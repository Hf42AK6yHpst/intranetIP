<?php
## Using By : 

#############################################
#
#   Date:   2020-07-09 Tommy
#           - modifide Get_Clear_Data_Confirm_Table(), for $sys_custom['eEnrollment']['setTargetForm'] to get selected form record
#
#   Date:   2020-06-24 Tommy
#           - modified Get_Management_Club_Attendance_Info_Table(), changed date checking for helper to change attendance
#
#   Date:   2020-06-12 Tommy
#           - modified Get_Clear_Unhandled_Data_Confirm_Table() --> $ClubInfoArr
#
#   Date:   2020-03-10 Henry
#           - modified Copy_Club_Step1_UI(), Get_Category_Setting_UI() to use libinterface GET_WARNING_TABLE()
#
#   Date:   2020-02-25 Henry
#           - modified Get_Enrollment_Application_Quota_Settings_Table() to control the max selection of Enrol min and max by $sys_custom['eEnrolment']['EnrollSelectionMaxNumber']
#
#   Date:   2020-02-12 Henry
#           - modified Get_Enrollment_Application_Quota_Settings_Table() to change the selection box of ApplyMin and EnrollMin
#
#   Date:   2020-01-24 Henry
#           - modified Get_Enrollment_Application_Quota_Settings_Table() to support enroll min
#
#   Date:   2019-11-01 Tommy
#           - added output total amount of student that joined the activity in Get_Management_Activity_Attendance_Info_Table() and Get_Management_Club_Attendance_Info_Table()
#
#   Date:   2019-10-28 Henry
#           - Modified Get_Management_TransferToWebSAMS_Step1_UI() to support inactive member (ActiveMemberOnly = 2)
#
#   Date:   2019-09-10 Henry
#           - Modified Get_Management_Activity_Attendance_UI() for bug fixing
#
#   Date:   2019-07-24 Anna
#           - Modified Get_Club_Selection(), added category name for general deploy
#
#   Date:   2019-04-01 Anna
#           - modified Get_Management_Club_Attendance_Info_Table(), change take attendance logic [#158758]
#
#   Date:   2019-02-19 Cameron
#           - add parameter $intakeID to getCourseSelectionByClass()
#
#   Date:   2018-12-18 Cameron
#           - pass $intakeClassIDAry to getEditLessonLayout() so that it retrieve the courses of the intake only
#
#   Date:   2018-12-11 Cameron
#           - add parameter $showSelectAll to getIntakeClassList()
#           - pass $showSelectAll to getCourseScheduleTable() and getCourseScheduleRow()
#
#   Date:   2018-11-16 Cameron
#           - add hidden field HidClassCombination in getCombinedLessonTemplateInstructor() for save record purpose
#
#   Date:   2018-11-07 Cameron
#           - disable class time conflict checking in checkeBooking4LessonUpdate() as class can split into multiple lesson in different location
#
#   Date:   2018-10-26 Cameron
#           - show time conflict classes in checkeBooking4LessonUpdate()
#
#   Date:   2018-10-25 Cameron
#           - add function setLessonBy(), auto select LessonInputBy when focus on select list or input text
#
#   Date:   2018-10-24 Cameron
#           - fix: hide "Is combined lesson" if it's combined course in getLessonTitleList()
#
#   Date:   2018-10-23 Cameron
#           - add function getLessonSelectionByCourse()
#           - modify checkForm() in getAddEditLessonJs() to check LessonTitleSelect, add delete function
#
#   Date:   2018-10-16 Cameron
#           - modify getCourseSelectionByClass(), set classID optional
#           - add function getAddEditLessonForm(), getAddEditLessonJs(), getEditLessonLayout()
#
#   Date:   2018-10-15 Cameron
#           - add para $selected to getCourseSelectionByClass()
#
#   Date:   2018-10-10 Cameron
#           - add function getIntakeClassList()
#           - modify getCourseScheduleTable()
#
#   Date:   2018-10-08 Cameron
#           - fix: remove gridline for combined cells in getNonCombinedCourseTemplateTable() and getCombinedCourseTemplateTable()
#
#   Date:   2018-10-05 Cameron
#           - add funnction getCombinedLessonLayout(), getCombinedLessonTemplateInstructor(), getCombinedLessonTemplateTable()
#
#   Date:   2018-10-02 Cameron
#           - add IsCombinedLesson to getLessonTitleList()
#
#   Date:   2018-09-20 Cameron
#           - don't set default instructor for admin user in getAddLessonLayout()
#           - modify chooseInstructor(), don't allow to choose instructor if course is not chozen
#           - add function checkeBooking4LessonUpdate() in getAddLessonLayout()
#
#   Date:   2018-09-19 Cameron
#           - change error message to "Time conflict" in checkForm() in getAddLessonLayout()
#           - add parameter $onClick to getInstructorSelection(), add chooseInstructor()
#
#   Date:   2018-09-14 Cameron
#           - add function getAddLessonLayout()
#
#   Date:   2018-09-12 Cameron
#           - add function getCourseSelectionByClass()
#
#   Date:   2018-09-06 Cameron
#           - change interval from 5 to 1 in getMinuteSelection() and getCourseScheduleTable()
#           - modify getClassSelection(), change onChange function name
#
#   Date:   2018-09-04 Cameron
#           - add left instructor indicator
#
#   Date:   2018-08-29 Cameron
#           - add parameter $firstTitle to getIntakeSelection()
#
#   Date:   2018-08-28 Cameron
#           - add function getClassSelection()
#           - fix: selected in getIntakeSelection()
#
#   Date:   2018-08-20 Cameron
#           - fix: $lessonStartTime and $lessonEndTime should contain no date part in getCourseScheduleTable(), getCourseScheduleRow()
#
#   Date:   2018-08-10 Cameron
#           - modify getIntakeClassTable(), pass NumberOfStudent as capacity to getLocationSelection(), set LocationCol id
#
#   Date:   2018-08-08 Cameron
#           - add parameter $topPriorityLocationID in getLocationSelection()
#           - add function getCombinedCourseLayout() and getNonCombinedCourseLayout()
#
#   Date:   2018-08-07 Cameron
#           - add function getCourseScheduleRow()
#
#   Date:   2018-08-01 Cameron
#           - add parameter $RecordType to Get_Meeting_Date_Time_Mgmt_Table_Row()
#           - add case getTimeLimitSetting in Get_Meeting_Date_Time_Mgmt_Table()
#           - add function getStaffSelection()
#
#   Date:   2018-07-19 Cameron
#           add function getNonCombinedCourseTemplateTable()
#
#   Date:   2018-07-18 Cameron
#           add function getCombinedCourseTemplateTable(), getDateTimeSelection(), 
#
#   Date:   2018-07-17 Cameron
#           add function getIntakeSelection()
#
#   Date:   2018-07-12 Cameron
#           add function getLocationSelection(), getNumberOfStudentSelection(), getInstructorSelection(), getIntakeClassTable()
#           define NEW_LINE
#
#   Date:   2018-07-11 Cameron
#           add function getWeekSelection(), getDaySelection(), getHourSelection(), getMinuteSelection()
#
#   Date:   2018-07-06 Cameron
#           add function getNumberOfLessonSelection(), getLessonTitleList()
#
#	Date	2018-07-03 Pun [121822] [ip.2.5.9.7.1]
#			Modified Get_Management_Club_Attendance_Info_Table(), Get_Management_Activity_Attendance_Info_Table(), Added NCS cust
#   Date:   2018-03-19 Philips
#           Modified Get_Management_Club_Attendance_Chart() for Late Count & Exempt Count & EarlyLeave Count
#
#   Date:   2018-03-13 Omas [ip.2.5.9.3.1]
#           Modified Get_Management_Club_Attendance_Info_Table() for fix L136487 
#
#	Date:	2018-01-13 Pun [ip.2.5.9.3.1]
#           Modified Get_Management_Activity_Attendance_Info_Table(), added NCS cust
#
#	Date:	2017-12-14 Pun
#			modified Get_Management_Activity_Attendance_Info_Table(), added NCS cust
#
#	Date:	2017-11-22 (Simon) [B115479]
#			modified Get_Management_Club_Attendance_Info_Table()
# 			add one more logic checking $isClubPIC and $isClubAdmin Enable change the Attendance
#
#	Date：	2017-11-21 (Anna)
#			modified Get_Management_Club_Attendance_Info_Table(), Get_Management_Activity_Attendance_Info_Table - change user photo path 
#
#	Date:	2017-10-16 (Simon) [B115479]
#			add flag to ensable if the date is in the future
#			modified Get_Management_Club_Attendance_Info_Table()
#			add $sys_custom['eEnrolment']['enableTakeAttendanceFutureDate'] flag 
#			add $sys_custom['eEnrolment']['DisabledPicTakeAttendanceIfAdminTaken'] flag inside below function
#			update Get_Management_Club_Attendance_Info_Table()
#
#	Date:	2017-09-28 (Anna)
#			modified Display_Enrolment_Default_OLE_Setting() - add when enroleventID =''
#
#	Date:	2017-09-15 (Anna)
#			modified Get_Management_TransferToWebSAMS_Step1_UI() - added upload file method
#
#	Date:	2017-06-20 (Villa)
#			modified Get_Management_Club_Attendance_Info_Table() - hide gradient info btn if user is a student
#	Date:	2017-06-06 (Anna)
#			modified Get_Management_TransferToWebSAMS_Step1_UI() - added export type 
#
#	Date:	2017-04-27 (Villa) #W107649
#			modified Get_Meeting_Date_Time_Mgmt_Table()
#			modified Get_Meeting_Date_Time_Mgmt_Table_Row() 
#
#	Date:	2017-03-30 (Frankie)
#			modified Get_Management_Club_Attendance_Info_Table(), Get_Management_Activity_Attendance_Info_Table() - handle User's Date Range
#
#	Date:	2016-11-30 Villa
#			modified Get_Management_Club_Attendance_Info_Table() - add id for <a href> Guardian Information
#	Date:	2016-01-19 Kenneth
#			modified Get_Achievement_Comment_Bank_DBTable() - Add column 'WebSAMS Code' for Performance Comment Bank
#
#	Date:	2015-11-27 Kenneth
#			Added Gradian Information Floating box - Get_Management_Club_Attendance_Info_Table() and Get_Management_Activity_Attendance_Info_Table()
#
#	Date:	2015-09-24 (Omas)
#			Fixed last update info display wrong issue - Get_Management_Activity_Attendance_Info_Table()
#
#	Date:	2015-09-16 (Omas)
#			added excluding holiday in Get_Meeting_Date_Periodic_Date_Selection_Table()
#
#   Date:   2015-07-15 (Shan)
#           Updated Get_Achievement_Comment_Bank_DBTable()
#
#   Date:   2015-06-02 (Shan)
#           Updated Copy_Club_Step1_UI()  style        
#
#	Date:	2015-06-01	(Omas)
#			Added Get_Clear_Data_Confirm_Table() for data handling > clear data
#
# 	Date:	2015-03-23  (Omas)
# 			Customization : modified Get_Student_Enrolment_Report_PerYear(),Get_Student_Enrolment_Report() to include clubs data - Z42562
#
#	Date:	2014-11-13	(Omas)
#			Pass New Parameter - RecordType to Get_Achievement_Comment_Bank_DBTable()
#
#	Date:	2014-10-31	(Omas)
#			Modified wordings(select->apply) in function Get_Enrollment_Application_Quota_Settings_Table(),Get_Enrollment_Application_Quota_Settings_Table_Round()
#
#	Date:	2014-10-14 (Pun)
#			Add round report for SIS customization
#
#	Date:	2014-08-11 (Bill)
#			Modified Get_Activity_Student_List_Report() - to display correct academic year of each report
#
#	Date:	2014-08-04 (Bill)
#			Modified Get_Activity_Student_List_Report()
#
#	Date:	2014-07-18 (Bill)
#			Modified Get_Activity_Student_List_Report()
#
#	Date:	2014-06-19 (Bill)
#			Modified Get_Activity_Student_List_Report()
#
#	Date:	2013-11-05 (Henry)
#			Modified Get_Management_Club_Attendance_Info_Table to show the email absent student button
#			Modified Get_Management_Activity_Attendance_Info_Table to show the email absent student button
#
#	Date:	2013-08-09 (Cameron)
#			add function Get_Category_Type_Selection()
#
#	Date:	2013-08-02 (Cameron)
#			add parameter ApplyUserType to Get_Club_Selection()
#
#   Date:   2013-06-25 (Rita)
#			add Get_Management_TransferToWebSAMS_Step1_UI();
#
#	Date:	2013-05-02 (Rita)
#			add Get_Activity_Student_List_Report()
#
#	Date:	2013-04-30 (Rita)
#			add Get_Activity_Report_Option_Table()
#
#	Date:	2013-03-26 (Rita)
#			add Get_Achievement_Comment_Bank_DBTable();
#
#	Date:	2013-02-27 (Rita)
#			modified Get_Management_Club_Attendance_Info_Table(), Get_Management_Activity_Attendance_Info_Table()
#
#   Date:	2013-01-04 (Rita)
#			modified Get_Management_TransferToSP_Step1_UI(), Get_Management_TransferToSP_Step2_UI() add Data Name Option
#
#	Date:	2013-01-02 (Ivan)
#			modified Display_Enrolment_Default_OLE_Setting() to add OLE type option
#
#	Date: 	2012-12-31 (Rita Tin)
#			add Get_Merit_Record_Display_Table()
#
#	Date: 	2012-11-06 (Rita Tin)
#			modified Get_Attendance_Display() add case late   	
#
#	Date: 	2012-11-05 (Rita Tin)
#			modified Get_Student_Attendance_Selection(), add late option for customization
#
#	Date:	2012-11-01 (Rita Tin)
#			modified Get_Management_Club_Attendance_Info_Table(), Get_Management_Activity_Attendance_Info_Table() add attendance helper right control 
# 
#	Date: 	2012-10-31 (Rita Tin)
#			- modified 	Get_Management_Activity_Attendance_Info_Table(), Get_Management_Activity_Attendance_Chart()
#
#	Date: 	2012-10-30 (Rita Tin)
#			- modified Get_Management_Club_Attendance_Info_Table(), () remove exempt
#
#	Date:	2012-10-24	(Rita Tin)
#			- modified Get_Club_Selection(), Get_Activity_Selection() add multiple selection condition
#
#	Date:	2011-10-17	(Henry Chow)
#			- add Display_Enrolment_Default_OLE_Setting(), dchange "category" & "OLE Component" as optional
#
#	Date:	2011-08-29	(Henry Chow)
#			- add Display_Enrolment_Default_OLE_Setting(), display OLE Default setting
#
#	Date:	2011-08-29	(Henry Chow)
#			- add Get_Student_Enrolment_Report() & Get_Student_Enrolment_Report_PerYear(), display "Student Enrolment report" [CRM : 2011-0616-1516-53126] 
#
#	Date:	2011-07-21	Henry Chow
#			- modified Get_Management_Club_Attendance_Info_Table(), Get_Management_Activity_Attendance_UI(), Get_Management_Activity_Attendance_Info_Table(), Get_Activity_Selection(), Get_Management_TransferToSP_Step1_UI(), Get_Management_TransferToSP_Step2_UI()
#			i) added option to view clubs/activities of previous years
#			ii) transfer records to SP & OLE (revised workflow)
#
#	Date:	2011-07-08	Henry Chow
#			- update Get_Management_TransferToSP_Step1_UI(), add "Div" next to "Semester" filter in order to display "Last Transferred"
#
#	Date:	2011-06-04	YatWoon
#			- update Get_Student_Role_And_Status_Remarks(), add "withoutClass" parameter for print version
#
#	Date:	2011-06-03	YatWoon
#			Hidden chart - case #2011-0526-1008-21067 (requested by UCCKE)
#			- update Get_Management_Club_Attendance_UI(),Get_Management_Activity_Attendance_UI,  Hidden display statistics chart
#			- update Get_Management_Club_Attendance_Chart(), Get_Management_Activity_Attendance_Chart(), change to display in pop-up window
#
#	Date:	2011-01-31	YatWoon
#			update Get_Club_Selection(), add academic year id
#
#	Date:	2010-12-14 YatWoon
#			update Get_Club_Selection(), Get_Copy_Club_Step1_Club_Selection_Table(), Get_Copy_Club_Step2_Club_Selection_Table(), club name eng/chinese
#
#	Date:	2010-12-10	YatWoon
#			update Get_Management_Club_Attendance_Info_Table(), add "toggle photo" function for take attendance view
#			update Get_Management_Activity_Attendance_Info_Table(), add "toggle photo" function for take attendance view
#
#############################################

if(!defined("NEW_LINE")) {
    define("NEW_LINE", "\n");
}

class libclubsenrol_ui extends interface_html {
	
	function libclubsenrol_ui()
	{
		parent::interface_html();
	}
	
	function Copy_Club_Step1_UI($FromStep2=0, $FromAcademicYearID='', $ToAcademicYearID='', $TermMappingArr='', $CopyClubArr='', $CopyMemberArr='', $CopyActivityArr='')
	{
		global $Lang;
		include_once('form_class_manage.php');
		
		### Navigation
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "javascript:js_Go_Back()"); 
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherYear'], ""); 
		$PageNavigation = $this->GET_NAVIGATION($PAGE_NAVIGATION);
		
		### Step Information
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][0], 1);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][1], 0);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][2], 0);
		
		### Copy Warning
		$WarningMsg = implode('<br /><br />', $Lang['eEnrolment']['CopyClub']['CopyWarning']);
		$WarningMsgBox = $this->GET_WARNING_TABLE($WarningMsg);
//		$WarningMsgBox = $this->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Warning'].'</font>', $WarningMsg, $others="");
		
		### Get To Academic Year
		$ToAcademicYearID = ($FromStep2!=1)? Get_Current_Academic_Year_ID() : $FromAcademicYearID;
		
		### Get From Academic Year
		if ($FromStep2 != 1)
		{
			$libAcademicYear = new academic_year();
			$AcademicYearArr = $libAcademicYear->Get_All_Year_List($ParLang="", $YearType=2, $exceptAcademicYearIDArr=array($ToAcademicYearID));
			$LastIndex = count($AcademicYearArr) - 1;
			$FromAcademicYearID = $AcademicYearArr[$LastIndex]['AcademicYearID'];
		}
		
		### Get From Academic Year Selection
		$thisTag = ' onchange="js_Changed_FromAcademicYear_Selection(this.value);" ';
		//$FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $thisTag, $noFirst=1, $noPastYear=0, $FromAcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, array($ToAcademicYearID));
		$FromAcademicYearSelection = getSelectAcademicYear('FromAcademicYearID', $thisTag, $noFirst=1, $noPastYear=0, $FromAcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0);
		
		### Get To Academic Year Selection
		$thisTag = ' onchange="js_Changed_ToAcademicYear_Selection(this.value);" ';
		//$ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $thisTag, $noFirst=1, $noPastYear=1, $ToAcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, array($FromAcademicYearID));
		$ToAcademicYearSelection = getSelectAcademicYear('ToAcademicYearID', $thisTag, $noFirst=1, $noPastYear=1, $ToAcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0);
		
		$ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Submit_Form();");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
		$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
		$x .= '<tr><td colspan="2" class="navigation">'.$PageNavigation.'</td></tr>'."\n";
		$x .= '</table>'."\n";		
		$x .= '<br style="clear:both" />'."\n";
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
		$x .= '<tr>'."\n";
		$x .= '<td colspan="2">'.$this->GET_STEPS($STEPS_OBJ).'</td>'."\n";
		$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
			
		$x .= '<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">'."\n";
			   
		$x .= '<tr>'."\n";
		$x .= '<td colspan="2" align="center">'.$WarningMsgBox.'</td>'."\n";
		$x .= '</tr>'."\n";
			   
		$x .= '<tr>'."\n";
			$x .= '<td>'."\n";
			   
		       $x .= '<table width="100%" border="0"   cellspacing="0" cellpadding="5" align="center" class="form_table_v30">'."\n";
				  # From Academic Year
				 $x .= '<tr>'."\n";
				 $x .= '<td class="field_title" width="30%" align="left">'.$Lang['eEnrolment']['CopyClub']['FromAcademicYear'].'</td>'."\n";
				 $x .= '<td class="tabletext"><div id="FromAcademicYearSelection">'.$FromAcademicYearSelection.'</div></td>'."\n";
				 $x .= '</tr>'."\n";							
				  # To Academic Year
			     $x .= '<tr>'."\n";
				 $x .= '<td class="field_title" width="30%" align="left">'.$Lang['eEnrolment']['CopyClub']['ToAcademicYear'].'</td>'."\n";
		 		 $x .= '<td class="tabletext"><div id="ToAcademicYearSelection">'.$ToAcademicYearSelection.'</div></td>'."\n";
				 $x .= '</tr>';				 
			   $x .= '</table>'."\n";		
			   
			$x .= '</td>'."\n";
	  $x .= '</tr>'."\n";
	       
			  # Term Mapping Table
	  $x .= '<tr>'."\n";
		 $x .= '<td colspan="2">'."\n";
		 $x .= '<div id="TermMappingDiv">'."\n";
		 $x .= $this->Get_Copy_Club_Step1_Term_Mapping_Table($FromAcademicYearID, $ToAcademicYearID, $ViewOnly=0, $TermMappingArr, $FromStep2);
		 $x .= '</div>'."\n";
		 $x .= '</td>'."\n";
	  $x .= '</tr>'."\n";
					  	
		    # Club Selection Table
	  $x .= '<tr>'."\n";
			$x .= '<td colspan="2">'."\n";
			$x .= '<div id="ClubSelectionDiv">'."\n";
			$x .= $this->Get_Copy_Club_Step1_Club_Selection_Table($FromAcademicYearID, $ToAcademicYearID, $CopyClubArr, $CopyMemberArr, $CopyActivityArr, $FromStep2);
			$x .= '</div>'."\n";
			$x .= '</td>'."\n";
	  $x .= '</tr>'."\n";
							
		$x .= '</table>'."\n";	
			
		$x .= '<div class="edit_bottom">'."\n";
	    $x .= '<span></span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					$x .= $ContinueBtn."\n";
					$x .= $BackBtn."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>'."\n";
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Copy_Club_Step1_Term_Mapping_Table($FromAcademicYearID, $ToAcademicYearID, $ViewOnly=0, $TermMappingArr='', $FromStep2=0)
	{
		global $Lang, $libenroll;
		
		$SemesterBasedClubInfoArr = $libenroll->GET_GROUPINFO('', $FromAcademicYearID, $ClubType='S');
		$numOfSemesterBasedClub = count($SemesterBasedClubInfoArr);
		
		if ($numOfSemesterBasedClub > 0)
		{
			include_once('form_class_manage.php');
			include_once('subject_class_mapping.php');
			include_once('subject_class_mapping_ui.php');
			
			$scm_ui = new subject_class_mapping_ui();
			
			$FromAcademicYearObj = new academic_year($FromAcademicYearID);
			$FromAcademicYearName = $FromAcademicYearObj->Get_Academic_Year_Name();
			$FromTermInfoArr = $FromAcademicYearObj->Get_Term_List($returnAsso=0);
			$numFromTerm = count($FromTermInfoArr);
			
			$ToAcademicYearObj = new academic_year($ToAcademicYearID);
			$ToAcademicYearName = $ToAcademicYearObj->Get_Academic_Year_Name();
			$ToTermInfoArr = $ToAcademicYearObj->Get_Term_List($returnAsso=0);
			$numToTerm = count($ToTermInfoArr);
			
			$x = '';
			$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
				$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($Lang['eEnrolment']['CopyClub']['TermMapping']).'</td></tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div id="TermMappingTableDiv">'."\n";
							$x .= '<table class="common_table_list" style="width:100%">'."\n";
								$x .= '<col align="left" style="width:50%;" />'."\n";
								$x .= '<col align="left" style="width:50%;" />'."\n";
								### Academic Year Name
								$x .= '<thead>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<th class="sub_row_top">'.$FromAcademicYearName.'</th>'."\n";
										$x .= '<th class="sub_row_top">'.$ToAcademicYearName.'</th>'."\n";
									$x .= '</tr>'."\n";
									$x .= '<tr>'."\n";
										$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['FromTerm'].'</th>'."\n";
										$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['ToTerm'].'</th>'."\n";
									$x .= '</tr>'."\n";
								$x .= '</thead>'."\n";
								
								### Term Name and Selection
								$x .= '<tbody>'."\n";
									for ($i=0; $i<$numFromTerm; $i++)
									{
										$thisFromTermID = $FromTermInfoArr[$i]['YearTermID'];
										$thisFromTermName = $FromTermInfoArr[$i]['TermName'];
										
										if ($ViewOnly==0)
										{
											$thisToTermID = ($FromStep2!=1)? $ToTermInfoArr[$i]['YearTermID'] : $TermMappingArr[$thisFromTermID];
											$thisDisplay = $scm_ui->Get_Term_Selection('TermMappingArr['.$thisFromTermID.']', $ToAcademicYearID, $thisToTermID, $OnChange='', $NoFirst=1);
										}
										else
										{
											$thisToTermID = $TermMappingArr[$thisFromTermID];
											$thisToTermObj = new academic_year_term($thisToTermID);
											$thisDisplay = $thisToTermObj->Get_Year_Term_Name();
										}
										
										$x .= '<tr>'."\n";
											$x .= '<td>'.$thisFromTermName.'</td>'."\n";
											$x .= '<td>'.$thisDisplay.'</td>'."\n";
										$x .= '</tr>'."\n";
									}
								$x .= '</tbody>'."\n";
							$x .= '</table>';
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	function Get_Copy_Club_Step1_Club_Selection_Table($FromAcademicYearID, $ToAcademicYearID, $CopyClubArr='', $CopyMemberArr='', $CopyActivityArr='', $FromStep2=0, $FromYearTermID='', $ToYearTermID='')
	{
		global $Lang, $libenroll, $intranet_session_language;
		
		include_once('form_class_manage.php');
		$ToAcademicYearObj = new academic_year($ToAcademicYearID);
		$ToAcademicYearName = $ToAcademicYearObj->Get_Academic_Year_Name();
		$TitleDuplicationRemark = str_replace('<!--YearName-->', $ToAcademicYearName, $Lang['eEnrolment']['CopyClub']['ClubTitleDuplicationRemarks']);
			
		### Get All Clubs Info
		$ClubInfoArr = $libenroll->GET_GROUPINFO('', $FromAcademicYearID, '', $FromYearTermID);
		$numOfClub = count($ClubInfoArr);
		$ToClubInfoArr = $libenroll->GET_GROUPINFO('', $ToAcademicYearID, '', $ToYearTermID);
		$ToClubTitleArr = Get_Array_By_Key($ToClubInfoArr, 'Title');
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($Lang['eEnrolment']['CopyClub']['ClubSelection']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="ClubSelectionTableDiv">'."\n";
						$x .= '<table class="common_table_list" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:40%;" />'."\n";
							$x .= '<col align="left" style="width:20%;" />'."\n";
							$x .= '<col style="width:10%;" />'."\n";
							$x .= '<col style="width:10%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['Club'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['Term'].'</th>'."\n";
								
								# Copy
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyClubChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['Copy'];
									$x .= '<br />'."\n";
									$x .= $this->Get_Checkbox('CopyClub_Global', "Global_Arr[CopyClub]", $Value=1, $thisChecked=1, $Class='CopyClubChk_Global', $Display='', $thisOnclick);
								$x .= '</th>'."\n";
								
								# Copy Member
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyMemberChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubMember'];
									$x .= '<br />'."\n";
									$x .= $this->Get_Checkbox('CopyMember_Global', "Global_Arr[CopyMember]", $Value=1, $thisChecked=1, $Class='CopyMemberChk_Global', $Display='', $thisOnclick);
								$x .= '</th>'."\n";
								
								# Copy Activity
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyActivityChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubActivity'];
									$x .= '<br />'."\n";
									$x .= $this->Get_Checkbox('CopyActivity_Global', "Global_Arr[CopyActivity]", $Value=1, $thisChecked=1, $Class='CopyActivityChk_Global', $Display='', $thisOnclick);
								$x .= '</th>'."\n";
							$x .= '</tr>'."\n";
							
							if ($numOfClub > 0)
							{
								for ($i=0; $i<$numOfClub; $i++)
								{
									$thisEnrolGroupID 	= $ClubInfoArr[$i]['EnrolGroupID'];
									$thisClubTitleDisplay 		= $intranet_session_language=="en"?$ClubInfoArr[$i]['Title']:$ClubInfoArr[$i]['TitleChinese'];
									$thisClubTitle = $ClubInfoArr[$i]['Title'];
									$thisSemesterTitle	= $ClubInfoArr[$i]['SemesterTitle'];
									
									//$thisActivityArr = $libenroll->GET_EVENTINFO('', $thisEnrolGroupID); 
									//$numOfActivity = count($thisActivityArr);
									
									if (!in_array($thisClubTitle, $ToClubTitleArr))
									{
										### Club Title does not exist in the target Term => can copy
										$thisID = 'CopyClubChk_'.$thisEnrolGroupID;
										$thisName = 'CopyClubArr['.$thisEnrolGroupID.']';
										$thisClass = 'CopyClubChk';
										$thisOnclick = "js_Check_Uncheck_Global_Selection(this.checked, 'CopyClub_Global', '$thisEnrolGroupID');";
										$thisChecked = ($FromStep2!=1)? 1 : $CopyClubArr[$thisEnrolGroupID];
										$thisCopyClubDisplay = $this->Get_Checkbox($thisID, $thisName, 1, $thisChecked, $thisClass, $Display='', $thisOnclick);
										
										$thisID = 'CopyMemberChk_'.$thisEnrolGroupID;
										$thisName = 'CopyMemberArr['.$thisEnrolGroupID.']';
										$thisClass = 'CopyMemberChk';
										$thisOnclick = "js_Check_Uncheck_Global_Selection(this.checked, 'CopyMember_Global');";
										$thisChecked = ($FromStep2!=1)? 1 : $CopyMemberArr[$thisEnrolGroupID];
										$thisCopyMemberDisplay = $this->Get_Checkbox($thisID, $thisName, 1, $thisChecked, $thisClass, $Display='', $thisOnclick);
										
										$thisID = 'CopyActivityChk_'.$thisEnrolGroupID;
										$thisName = 'CopyActivityArr['.$thisEnrolGroupID.']';
										$thisClass = 'CopyActivityChk';
										//$thisDisabled = ($numOfActivity==0)? 1 : 0;
										//$thisChecked = ($thisDisabled==0)? 1 : 0;
										$thisOnclick = "js_Check_Uncheck_Global_Selection(this.checked, 'CopyActivity_Global');";
										$thisChecked = ($FromStep2!=1)? 1 : $CopyActivityArr[$thisEnrolGroupID];
										$thisCopyActivityDisplay = $this->Get_Checkbox($thisID, $thisName, 1, $thisChecked, $thisClass, $Display='', $thisOnclick, $thisDisabled);
									}
									
									### Show in red if title is duplicated in the target term
									if (in_array($thisClubTitle, $ToClubTitleArr))
									{
										$TextPrefix = '<font color="red">';
										$TextSuffix = '</font>';
									}
									else
									{
										$TextPrefix = '';
										$TextSuffix = '';
									}
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$TextPrefix.$thisClubTitleDisplay.$TextSuffix.'</td>'."\n";
										$x .= '<td>'.$TextPrefix.$thisSemesterTitle.$TextSuffix.'</td>'."\n";
										
										if (!in_array($thisClubTitle, $ToClubTitleArr))
										{
											$x .= '<td style="text-align:center">'.$thisCopyClubDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyMemberDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyActivityDisplay.'</td>'."\n";
										}
										else
										{
											### Group Name duplicated in the target Term
											$x .= '<td colspan="3" style="text-align:center">'.$TextPrefix.$TitleDuplicationRemark.$TextSuffix.'</font></td>'."\n";
										}
									$x .= '</tr>'."\n";
								}
							}
							else
							{
								$x .= '<tr><td colspan="5" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							
						$x .= '</table>'."\n";
						
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Copy_Club_Step2_UI($FromAcademicYearID, $ToAcademicYearID, $TermMappingArr, $CopyClubArr, $CopyMemberArr, $CopyActivityArr)
	{
		global $Lang;
		include_once('form_class_manage.php');
		
		### Navigation
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "javascript:js_Cancel()"); 
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClub'], ""); 
		$PageNavigation = $this->GET_NAVIGATION($PAGE_NAVIGATION);
		
		### Step Information
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][0], 0);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][1], 1);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][2], 0);
		
		### Get From Academic Year
		$FromAcademicYearObj = new academic_year($FromAcademicYearID);
		$FromAcademicYearName = $FromAcademicYearObj->Get_Academic_Year_Name();
		
		### Get To Academic Year
		$ToAcademicYearObj = new academic_year($ToAcademicYearID);
		$ToAcademicYearName = $ToAcademicYearObj->Get_Academic_Year_Name();
		
		$ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Submit_Form();");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
		$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr><td colspan="2" class="navigation">'.$PageNavigation.'</td></tr>'."\n";
			$x .= '<table>'."\n";
			$x .= '<br style="clear:both" />'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'.$this->GET_STEPS($STEPS_OBJ).'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'."\n";
							# From Academic Year
							$x .= '<tr>'."\n";
								$x .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['eEnrolment']['CopyClub']['FromAcademicYear'].'</td>'."\n";
								$x .= '<td class="tabletext">'.$FromAcademicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# To Academic Year
							$x .= '<tr>'."\n";
								$x .= '<td class="formfieldtitle" width="30%" align="left">'.$Lang['eEnrolment']['CopyClub']['ToAcademicYear'].'</td>'."\n";
								$x .= '<td class="tabletext">'.$ToAcademicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							$x .= '<tr><td colspan="2">&nbsp;</td></tr>'."\n";
							
							# Term Mapping Table
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= '<div id="TermMappingDiv">'."\n";
										$x .= $this->Get_Copy_Club_Step1_Term_Mapping_Table($FromAcademicYearID, $ToAcademicYearID, $ViewOnly=1, $TermMappingArr);
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
							# Club Selection Table
							$TmpArr = $this->Get_Copy_Club_Step2_Club_Selection_Table($FromAcademicYearID, $ToAcademicYearID, $CopyClubArr, $CopyMemberArr, $CopyActivityArr, $TermMappingArr);
							$ClubSelectionTable = $TmpArr['HTML'];
							$NumOfCannotCopyClub = $TmpArr['NumOfCannotCopyClub'];
							
							$x .= '<tr>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= '<div id="ClubSelectionDiv">'."\n";
										$x .= $ClubSelectionTable;
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				$x .= '<span></span>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				if ($NumOfCannotCopyClub == 0)
					$x .= $ContinueBtn."\n";
				$x .= $BackBtn."\n";
				$x .= $CancelBtn."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<input type="hidden" id="FromStep2" name="FromStep2" value="1">'."\n";
			$x .= '<input type="hidden" id="FromAcademicYearID" name="FromAcademicYearID" value="'.$FromAcademicYearID.'">'."\n";
			$x .= '<input type="hidden" id="ToAcademicYearID" name="ToAcademicYearID" value="'.$ToAcademicYearID.'">'."\n";
			$x .= '<input type="hidden" id="TermMappingArr" name="TermMappingArr" value="'.rawurlencode(serialize($TermMappingArr)).'">'."\n";
			$x .= '<input type="hidden" id="CopyClubArr" name="CopyClubArr" value="'.rawurlencode(serialize($CopyClubArr)).'">'."\n";
			$x .= '<input type="hidden" id="CopyMemberArr" name="CopyMemberArr" value="'.rawurlencode(serialize($CopyMemberArr)).'">'."\n";
			$x .= '<input type="hidden" id="CopyActivityArr" name="CopyActivityArr" value="'.rawurlencode(serialize($CopyActivityArr)).'">'."\n";
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		
		return $x;
	}
	
	function Get_Copy_Club_Step2_Club_Selection_Table($FromAcademicYearID, $ToAcademicYearID, $CopyClubArr, $CopyMemberArr, $CopyActivityArr, $TermMappingArr)
	{
		global $Lang, $libenroll, $i_ClubsEnrollment_WholeYear, $intranet_session_language;
		
		### check duplicated group title
		include_once('form_class_manage.php');
		$ToAcademicYearObj = new academic_year($ToAcademicYearID);
		$ToAcademicYearName = $ToAcademicYearObj->Get_Academic_Year_Name();
		$TitleDuplicationRemark = str_replace('<!--YearName-->', $ToAcademicYearName, $Lang['eEnrolment']['CopyClub']['ClubTitleDuplicationRemarks']);
			
		### Get To Term Info
		$ToAcademicYearObj = new academic_year($ToAcademicYearID);
		$ToAcademicYearName = $ToAcademicYearObj->Get_Academic_Year_Name();
		$ToTermInfoArr = $ToAcademicYearObj->Get_Term_List($returnAsso=1);
			
		### Get All Clubs Info
		$ClubInfoArr = $libenroll->GET_GROUPINFO('', $FromAcademicYearID);
		$numOfClub = count($ClubInfoArr);
		$ToClubInfoArr = $libenroll->GET_GROUPINFO('', $ToAcademicYearID);
		$ToClubTitleArr = Get_Array_By_Key($ToClubInfoArr, 'Title');
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($Lang['eEnrolment']['CopyClub']['ClubSelection']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="ClubSelectionTableDiv">'."\n";
						$x .= '<table class="common_table_list" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:30%;" />'."\n";
							$x .= '<col align="left" style="width:15%;" />'."\n";
							$x .= '<col align="left" style="width:15%;" />'."\n";
							$x .= '<col style="width:10%;" />'."\n";
							$x .= '<col style="width:10%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['Club'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['FromTerm'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['ToTerm'].'</th>'."\n";
								
								# Copy
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyClubChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['Copy'];
								$x .= '</th>'."\n";
								
								# Copy Member
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyMemberChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubMember'];
								$x .= '</th>'."\n";
								
								# Copy Activity
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyActivityChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubActivity'];
								$x .= '</th>'."\n";
							$x .= '</tr>'."\n";
							
							if ($numOfClub > 0)
							{
								### Check if two clubs from the same group is mapped to the same term
								$GroupTermMappingCount = array();		// $GroupTermMapping[$GroupID][$ToTermID] = 2;
								for ($i=0; $i<$numOfClub; $i++)
								{
									$thisEnrolGroupID 	= $ClubInfoArr[$i]['EnrolGroupID'];
									$thisGroupID 		= $ClubInfoArr[$i]['GroupID'];
									$thisFromSemesterID	= $ClubInfoArr[$i]['Semester'];
									$thisCopyClub 		= $CopyClubArr[$thisEnrolGroupID];
									
									# skip for year-based club and not copy club
									if ($thisFromSemesterID=='' || $thisFromSemesterID==0 || $thisCopyClub==0)
										continue;
										
									$thisToSemesterID	= $TermMappingArr[$thisFromSemesterID];
									$GroupTermMappingCount[$thisGroupID][$thisToSemesterID]++;
								}
								
								$CannotCopyClubArr = array();
								for ($i=0; $i<$numOfClub; $i++)
								{
									$thisEnrolGroupID 	= $ClubInfoArr[$i]['EnrolGroupID'];
									$thisGroupID 		= $ClubInfoArr[$i]['GroupID'];
									$thisClubTitle 		= $ClubInfoArr[$i]['Title'];
									$thisClubTitleDisplay = $intranet_session_language=="en" ? $ClubInfoArr[$i]['Title'] : $ClubInfoArr[$i]['TitleChinese'];
									$thisSemesterTitle	= $ClubInfoArr[$i]['SemesterTitle'];
									$thisSemester		= $ClubInfoArr[$i]['Semester'];
									$thisCopyClub 		= $CopyClubArr[$thisEnrolGroupID];
									$thisCopyMember 	= $CopyMemberArr[$thisEnrolGroupID];
									$thisCopyActivity	= $CopyActivityArr[$thisEnrolGroupID];
									
									$thisTitleDuplicated = in_array($thisClubTitle, $ToClubTitleArr);
									
									### Get the Target Term Info
									$thisToSemesterID	= $TermMappingArr[$thisSemester];
									if ($thisToSemesterID=='' || $thisToSemesterID==0)
										$thisToSemesterTitle = $i_ClubsEnrollment_WholeYear;
									else
										$thisToSemesterTitle = $ToTermInfoArr[$thisToSemesterID];
									
									
									### Check if the Club is valid to transfer
									$thisWarningArr = array();
									# Clubs from the same Group mapped to the same target Term
									if ($thisCopyClub && $GroupTermMappingCount[$thisGroupID][$thisToSemesterID] > 1)
									{
										$CannotCopyClubArr[] = $thisEnrolGroupID;
										$thisWarningArr[] = $Lang['eEnrolment']['CopyClub']['ClubCopyToTheSameTermRemarks'];
									}
									
									if (!$thisTitleDuplicated)
									{
										### Club Title does not exist in the target Term => can copy
										$thisCopyClubDisplay = ($thisCopyClub==1)? $Lang['General']['Yes'] : '<span class="tabletextremark">'.$Lang['General']['No'].'</span>';
										$thisCopyMemberDisplay = ($thisCopyMember==1)? $Lang['General']['Yes'] : '<span class="tabletextremark">'.$Lang['General']['No'].'</span>';
										$thisCopyActivityDisplay = ($thisCopyActivity==1)? $Lang['General']['Yes'] : '<span class="tabletextremark">'.$Lang['General']['No'].'</span>';
									}
									
									
									### Show in grey if title is duplicated in the target term
									$numOfWarning = count($thisWarningArr);
									if ($CopyClubArr[$thisEnrolGroupID]==1 && $numOfWarning==0)
									{
										$TextPrefix = '';
										$TextSuffix = '';
									}
									else if ($CopyClubArr[$thisEnrolGroupID]==0 || $thisTitleDuplicated)
									{
										$TextPrefix = '<span class="tabletextremark">';
										$TextSuffix = '</span>';
									}
									else if ($numOfWarning > 0)
									{
										$TextPrefix = '<font color="red">';
										$TextSuffix = '</font>';
									}
									
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$TextPrefix.$thisClubTitleDisplay.$TextSuffix.'</td>'."\n";
										$x .= '<td>'.$TextPrefix.$thisSemesterTitle.$TextSuffix.'</td>'."\n";
										$x .= '<td>'.$TextPrefix.$thisToSemesterTitle.$TextSuffix.'</td>'."\n";
										
										if ($thisTitleDuplicated)
										{
											$x .= '<td colspan="3" style="text-align:center">'.$TextPrefix.$TitleDuplicationRemark.$TextSuffix.'</font></td>'."\n";
										}
										else if (count($thisWarningArr)==0)
										{
											$x .= '<td style="text-align:center">'.$thisCopyClubDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyMemberDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyActivityDisplay.'</td>'."\n";
										}
										else
										{
											$thisWarningDisplay = implode('<br />', $thisWarningArr);
											$x .= '<td colspan="3" style="text-align:center">'.$TextPrefix.$thisWarningDisplay.$TextSuffix.'</font></td>'."\n";
										}
									$x .= '</tr>'."\n";
								}
							}
							else
							{
								$x .= '<tr><td colspan="4" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$NumOfCannotCopyClub = count($CannotCopyClubArr);
		return array("HTML" => $x, "NumOfCannotCopyClub" => $NumOfCannotCopyClub);
	}
	
	function Get_Student_Role_And_Status_Remarks($TypeArr=1,$withoutClass=0)
	{
		$x = '';
		
		if ($TypeArr==1 || (is_array($TypeArr) && in_array('GroupAdmin', $TypeArr)))
			$x .= $this->GroupAdminField($withoutClass);
		if($withoutClass)
			$x .= "<br>";		
		if ($TypeArr==1 || (is_array($TypeArr) && in_array('InactiveStudent', $TypeArr)))
			$x .= $this->InactiveStudentField($withoutClass);
		
		return $x;
	}
	
	function Get_Clear_Unhandled_Data_Confirm_UI($EnrolGroupIDArr)
	{
		global $Lang, $eEnrollment;
		
		### Navaigtion
		$PAGE_NAVIGATION[] = array($eEnrollment['member_selection_and_drawing'], "javascript:js_Go_Back();");
	 	$PAGE_NAVIGATION[] = array($eEnrollment['button']['next_round'], '', 1);
	 	
	 	### Warning Msg Box
	 	$WarningBox = $this->GET_WARNING_TABLE('', $Lang['eEnrolment']['ClearUnhandledData']['UnhandledData'], '');
		
		### Buttons
		$DeleteBtn = $this->GET_ACTION_BTN($Lang['Btn']['DeleteAll'], "button", "js_Go_Clear_Data();", "", "", "", "formbutton_alert");
		$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();", "", "", "", "formbutton_v30");
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->GET_NAVIGATION_IP25($PAGE_NAVIGATION)."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $WarningBox."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Clear_Unhandled_Data_Confirm_Table($EnrolGroupIDArr);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom_v30">';
				$x .= $DeleteBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>';
		
		$x .= '</form>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Clear_Unhandled_Data_Confirm_Table($EnrolGroupIDArr)
	{
		global $PATH_WRT_ROOT, $Lang, $eEnrollment, $libenroll;
		
		$ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info($EnrolGroupIDArr, '', 0, '', '', 0, 1, 1, 1, '', '', '', 0);
		$numOfClub = count($ClubInfoArr);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($Lang['eEnrolment']['CopyClub']['Club']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="SubjectGroupTableDiv">'."\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:25%;" />'."\n";
							$x .= '<col align="left" style="width:15%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$x .= '<th rowspan="2">'.$eEnrollment['club_name'].'</th>'."\n";
								$x .= '<th rowspan="2">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term'].'</th>'."\n";
								$x .= '<th rowspan="2">'.$eEnrollment['club_quota'].'</th>'."\n";
								$x .= '<th rowspan="2">'.$eEnrollment['MinimumQuota'].'</th>'."\n";
								$x .= '<th colspan="3" style="text-align:center;">'.$eEnrollment['number_of_applicants'].'</th>'."\n";
								$x .= '<th rowspan="2">'.$eEnrollment['quota_left'].'</th>'."\n";
							$x .= '</tr>'."\n";
							
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['EnrollmentStatus']['Unhandled'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['EnrollmentStatus']['Approved'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['EnrollmentStatus']['Rejected'].'</th>'."\n";
							$x .= '</tr>'."\n";
							
							if ($numOfClub > 0)
							{
								foreach((array)$ClubInfoArr as $thisEnrolGroupID => $thisClubInfoArr)
								{
									$thisClubName = $thisClubInfoArr['GroupTitle'];
									$thisTermName = $thisClubInfoArr['YearTermName'];
									$thisQuota = $thisClubInfoArr['QuotaDisplay'];
									$thisMinQuota = $thisClubInfoArr['MinQuotaDisplay'];
									$thisNumOfWaiting = count((array)$thisClubInfoArr['StatusStudentArr'][0]);
									$thisNumOfApproved = count((array)$thisClubInfoArr['StatusStudentArr'][2]);
									$thisNumOfRejected = count((array)$thisClubInfoArr['StatusStudentArr'][1]);
									$thisQuotaLeft = $thisClubInfoArr['QuotaLeftDisplay'];
									
									if ($thisNumOfWaiting > 0)
										$thisNumOfWaiting = '<span class="tabletextrequire">'.$thisNumOfWaiting.'</span>';
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$thisClubName.'</td>'."\n";
										$x .= '<td>'.$thisTermName.'</td>'."\n";
										$x .= '<td>'.$thisQuota.'</td>'."\n";
										$x .= '<td>'.$thisMinQuota.'</td>'."\n";
										$x .= '<td>'.$thisNumOfWaiting.'</td>'."\n";
										$x .= '<td>'.$thisNumOfApproved.'</td>'."\n";
										$x .= '<td>'.$thisNumOfRejected.'</td>'."\n";
										$x .= '<td>'.$thisQuotaLeft.'</td>'."\n";
									$x .= '</tr>'."\n";
								}
							}
							else
							{
								$x .= '<tr><td colspan="8" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<input type="hidden" id="EnrolGroupIDList" name="EnrolGroupIDList" value="'.implode(',', $EnrolGroupIDArr).'" />'."\n";
		
		return $x;
	}
	
	function Get_Clear_Data_Confirm_Table($Type, $targetForm)
	{
	    global $PATH_WRT_ROOT, $Lang, $eEnrollment, $libenroll, $sys_custom;
		
		$EnrolGroupIDArr = '';
		if($Type == 'C'){
		    if($sys_custom['eEnrollment']['setTargetForm']){
// 		        $ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info($EnrolGroupIDArr);
		        $ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info_Cust($EnrolGroupIDArr, '', $targetForm,  0, '', '', 0, 1, 1, 1, '', '', '', 0);
		    }else{
			     $ClubInfoArr = $libenroll->Get_Club_Student_Enrollment_Info($EnrolGroupIDArr);	
		    }
		}
		else{
			$ClubInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolGroupIDArr);	
		}

		$numOfClub = count($ClubInfoArr);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$displayTitle = ($Type =='C' )? $eEnrollment['club']:$eEnrollment['activity'];
			$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($displayTitle).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="SubjectGroupTableDiv">'."\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:25%;" />'."\n";
							$x .= '<col align="left" style="width:15%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$displayHeader = ($Type =='C' )?$eEnrollment['club_name']:$eEnrollment['add_activity']['act_name'];
								if($Type =='C'){
									$x .= '<th rowspan="2">'.$displayHeader.'</th>'."\n";
									$x .= '<th rowspan="2">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term'].'</th>'."\n";	
									$x .= '<th rowspan="2">'.$eEnrollment['club_quota'].'</th>'."\n";
									$x .= '<th rowspan="2">'.$eEnrollment['MinimumQuota'].'</th>'."\n";
								}
								else{
									$x .= '<th rowspan="2" colspan="2">'.$displayHeader.'</th>'."\n";
									$x .= '<th rowspan="2" colspan="2">'.$eEnrollment['club_quota'].'</th>'."\n";
								}
								
								$x .= '<th colspan="3" style="text-align:center;">'.$eEnrollment['number_of_applicants'].'</th>'."\n";
								$x .= '<th rowspan="2">'.$eEnrollment['quota_left'].'</th>'."\n";
							$x .= '</tr>'."\n";
							
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['EnrollmentStatus']['Unhandled'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['EnrollmentStatus']['Approved'].'</th>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['EnrollmentStatus']['Rejected'].'</th>'."\n";
							$x .= '</tr>'."\n";
							
							if ($numOfClub > 0)
							{
								foreach((array)$ClubInfoArr as $thisEnrolGroupID => $thisClubInfoArr)
								{
									if($Type == 'C'){
										$thisClubName = $thisClubInfoArr['GroupTitle'];
										$thisTermName = $thisClubInfoArr['YearTermName'];
										$thisQuota = $thisClubInfoArr['QuotaDisplay'];
										$thisMinQuota = $thisClubInfoArr['MinQuotaDisplay'];
										$thisNumOfWaiting = count((array)$thisClubInfoArr['StatusStudentArr'][0]);
										$thisNumOfApproved = count((array)$thisClubInfoArr['StatusStudentArr'][2]);
										$thisNumOfRejected = count((array)$thisClubInfoArr['StatusStudentArr'][1]);
										$thisQuotaLeft = $thisClubInfoArr['QuotaLeftDisplay'];	
									}
									else{
										$thisClubName = $thisClubInfoArr['EventTitle'];
										//$thisTermName = '---';
										$thisQuota = $thisClubInfoArr['Quota'];
										//$thisMinQuota = '???';
										$thisNumOfWaiting = count((array)$thisClubInfoArr['StatusStudentArr'][0]);
										$thisNumOfApproved = count((array)$thisClubInfoArr['StatusStudentArr'][2]);
										$thisNumOfRejected = count((array)$thisClubInfoArr['StatusStudentArr'][1]);
										$thisQuotaLeft = $thisClubInfoArr['Quota'] - $thisClubInfoArr['Approved'];
										
									}
									
									if($thisNumOfApproved == 0 && $thisNumOfWaiting == 0 && $thisNumOfRejected==0){
										continue;
									}
									
									if ($thisNumOfWaiting > 0)
										$thisNumOfWaiting = '<span class="tabletextrequire">'.$thisNumOfWaiting.'</span>';
									if ($thisNumOfApproved > 0)
										$thisNumOfApproved = '<span class="tabletextrequire">'.$thisNumOfApproved.'</span>';
									if ($thisNumOfRejected > 0)
										$thisNumOfRejected = '<span class="tabletextrequire">'.$thisNumOfRejected.'</span>';
									
									$y .= '<tr>'."\n";
										if($Type == 'C'){
											$y .= '<td>'.$thisClubName.'</td>'."\n";
											$y .= '<td>'.$thisTermName.'</td>'."\n";
											$y .= '<td>'.$thisQuota.'</td>'."\n";
											$y .= '<td>'.$thisMinQuota.'</td>'."\n";
										}
										else{
											$y .= '<td colspan="2">'.$thisClubName.'</td>'."\n";
											$y .= '<td colspan="2">'.$thisQuota.'</td>'."\n";
										}
										
										$y .= '<td>'.$thisNumOfWaiting.'</td>'."\n";
										$y .= '<td>'.$thisNumOfApproved.'</td>'."\n";
										$y .= '<td>'.$thisNumOfRejected.'</td>'."\n";
										$y .= '<td>'.$thisQuotaLeft.'</td>'."\n";
									$y .= '</tr>'."\n";
								}
								if($y ==''){
									$x .= '<tr><td colspan="8" style="text-align:center"><input type="hidden" id="notAllowSubmit" name="notAllowSubmit" value="1"/>'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else{
									$x .= $y;
								}
							}
							else
							{
								$x .= '<tr><td colspan="8" style="text-align:center"><input type="hidden" id="notAllowSubmit" name="notAllowSubmit" value="1"/>'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_TransferToSP_Step1_UI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr)
	{
		global $PATH_WRT_ROOT, $Lang, $eEnrollment;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		include_once($PATH_WRT_ROOT.'includes/libinterface.php');
		
		$linterface = new interface_html();
		
		$StepsDisplay = $this->GET_IMPORT_STEPS($CurrStep=1, $Lang['eEnrolment']['Transfer_to_SP']['StepArr']);
		
		$RequiredStar = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Required'].'</span>'."\n";

		$thisTag = 'onchange="js_Changed_Academic_Year_Selection(this.value);"';
		$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $thisTag, $noFirst=1, $noPastYear=0, $AcademicYearID);
		
		$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
		$numOfForm = count($FormArr);
		$NumOfFormInRow = 5;
		
		$FormChkTable = '';
		$FormChkTable .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			
			$thisChkID = "SelectAll_TargetFormChk";
			$thisChkOnClick = "Set_Checkbox_Value('YearIDArr[]', this.checked);";
			$FormChkTable .= '<tr><td style="border-bottom:0px;padding-bottom:0px;padding-top:0px;">'."\n";
				$thisChecked = ($YearIDArr=='')? true : false;
				$FormChkTable .= $this->Get_Checkbox($thisChkID, $thisChkName='', 1, $thisChecked, '', $eEnrollment['all'], $thisChkOnClick, $Disabled='');
			$FormChkTable .= '</td></tr>'."\n";
				
			for ($i=0; $i<$numOfForm; $i++)
			{
				if (($i % $NumOfFormInRow) == 0)
				{
					### row start
					$FormChkTable .= '<tr>'."\n";
					$PrefixSpace = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				else
					$PrefixSpace = '';
				
				### Build Checkboxes
				$thisYearID = $FormArr[$i]['YearID'];
				$thisYearName = $FormArr[$i]['YearName'];
				$thisChkID = 'YearID_'.$thisYearID;
				$thisChkName = 'YearIDArr[]';
				$thisChkClass = 'FormChk';
				$thisChkOnClick = "Uncheck_SelectAll('SelectAll_TargetFormChk', this.checked);";
				$thisChecked = ($YearIDArr=='' || in_array($thisYearID, (array)$YearIDArr))? true : false;
				
				$FormChkTable .= '<td style="border-bottom:0px;padding-bottom:0px;padding-top:0px;">'."\n";
					$FormChkTable .= $PrefixSpace.$this->Get_Checkbox($thisChkID, $thisChkName, $thisYearID, $thisChecked, $thisChkClass, $thisYearName, $thisChkOnClick, $Disabled='');
				$FormChkTable .= '</td>'."\n";
				
				if ( ( $i>0 && (($i % $NumOfFormInRow) == ($NumOfFormInRow-1)) ) || ($i==$numOfForm-1) )
				{
					### row end
					$FormChkTable .= '</tr>'."\n";
				}
			}
		$FormChkTable .= '</table>'."\n";
		
		
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="POST">';
			$x .= '<div class="table_board">'."\n";
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
					$x .= '<tr><td>'.$StepsDisplay.'</td></tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								### Year / Term Settings
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['General']['SchoolYear'].'</td>'."\n";
									$x .= '<td>'.$AcademicYearSelection.'</td>';
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['General']['Term'].'</td>'."\n";
									$x .= '<td><div id="YearTermSelectionDiv"></div><div id="LastTransferDiv"></div></td>';
								$x .= '</tr>'."\n";
								
								### Club / Activity
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['eEnrolment']['Transfer_to_SP']['DataSource'].'</td>'."\n";
									$x .= '<td>'.$this->Get_Data_Type_Checkbox_Table($DataTypeArr).'</td>';
								$x .= '</tr>'."\n";
								
								### Club Name Display Option
								$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$RequiredStar.$Lang['eEnrolment']['Transfer_to_SP']['DataNameLang'] . ' <span class="tabletextremark">' . $Lang['eEnrolment']['ForClubOnly']  . '</span>' .'</td>'."\n";
								$x .= '<td>';	
									$x .= $linterface->Get_Radio_Button('DataNameLang', 'DataNameLang', '1', '1', $Class="", $Lang['General']['English']);
									$x .= $linterface->Get_Radio_Button('DataNameLang', 'DataNameLang', '0', $isChecked=0, $Class="",$Lang['General']['Chinese']);
								$x .= '</td>';
								$x .= '</tr>'."\n";
								
								### Active Member
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$eEnrollment['transfer_student_setting'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<input type="radio" name="ActiveMemberOnly" value="0" id="ActiveMemberOnly_No" checked> <label for="ActiveMemberOnly_No">'.$eEnrollment['all_member'].'</label>'."\n";
										$x .= '&nbsp;&nbsp;'."\n";
										$x .= '<input type="radio" name="ActiveMemberOnly" value="1" id="ActiveMemberOnly_Yes"> <label for="ActiveMemberOnly_Yes">'.$eEnrollment['active_member_only'].'</label>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								### Form
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$eEnrollment['add_activity']['act_target'].'</td>'."\n";
									$x .= '<td>'.$FormChkTable.'</td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			$x .= $this->MandatoryField()."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n"; 
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";

		return $x;
	}
	
	function Get_Management_TransferToSP_Step2_UI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr, $DataNameLang='')
	{
		global $PATH_WRT_ROOT, $Lang, $eEnrollment;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		$StepsDisplay = $this->GET_IMPORT_STEPS($CurrStep=2, $Lang['eEnrolment']['Transfer_to_SP']['StepArr']);
		
		### Get Academic Year Info
		$AcademicYearObj = new academic_year($AcademicYearID);
		$AcademicYearName = $AcademicYearObj->Get_Academic_Year_Name();
		
		### Get Year Term Info
		if ($YearTermID == '' || $YearTermID == 0)
			$YearTermName = $Lang['eEnrolment']['WholeYear'];
		else
		{
			$YearTermObj = new academic_year_term($YearTermID);
			$YearTermName = $YearTermObj->Get_Academic_Year_Name();
		}
		
		### Get Data Source Info
		$DataTypeDisplayArr = array();
		$numOfDataType = count($DataTypeArr);
		for ($i=0; $i<$numOfDataType; $i++)
		{
			$thisDataType = $DataTypeArr[$i];
			$DataTypeDisplayArr[] = $Lang['eEnrolment'][$thisDataType];
		}
		$DataTypeDisplay = implode(', ', $DataTypeDisplayArr);
		
		### Active Member Only
		$ActiveMemberOnlyDisplay = ($ActiveMemberOnly==1)? $eEnrollment['active_member_only'] : $eEnrollment['all_member'];
		
		### Form Info		
		$libYear = new Year();
		$YearInfoArr = $libYear->Get_All_Year_List();
		$YearInfoArr = BuildMultiKeyAssoc($YearInfoArr, 'YearID');
		
		$YearDisplayArr = array();
		$numOfForm = count($YearIDArr);
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisYearID = $YearIDArr[$i];
			$YearDisplayArr[] = $YearInfoArr[$thisYearID]['YearName']; 
		}
		$YearDisplay = implode(', ', $YearDisplayArr);
		
		
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="POST">';
			$x .= '<div class="table_board">'."\n";
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
					$x .= '<tr><td>'.$StepsDisplay.'</td></tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								### Year / Term Settings
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['General']['SchoolYear'].'</td>'."\n";
									$x .= '<td>'.$AcademicYearName.'</td>';
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['General']['Term'].'</td>'."\n";
									$x .= '<td>'.$YearTermName.'</td>';
								$x .= '</tr>'."\n";
								
								### Club / Activity
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['eEnrolment']['Transfer_to_SP']['DataSource'].'</td>'."\n";
									$x .= '<td>'.$DataTypeDisplay.'</td>';
								$x .= '</tr>'."\n";
								
								### Active Member
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$eEnrollment['transfer_student_setting'].'</td>'."\n";
									$x .= '<td>'.$ActiveMemberOnlyDisplay.'</td>'."\n";
								$x .= '</tr>'."\n";
								
								### Form
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$eEnrollment['add_activity']['act_target'].'</td>'."\n";
									$x .= '<td>'.$YearDisplay.'</td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<tr><td>&nbsp;</td></tr>'."\n";
					$x .= '<tr><td>'.$this->GET_LNK_PRINT('javascript:js_Print_Details();', $Lang['eEnrolment']['Transfer_to_SP']['Button']['PrintDetailsData'], $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=0).'</td></tr>'."\n";
					$x .= '<tr><td>&nbsp;</td></tr>'."\n";
					
					if (in_array('Club', $DataTypeArr))
					{
						$x .= '<tr>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->Get_Management_TransferToSP_Step2_Club_Table_ViewMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr, $DataNameLang);
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr><td>&nbsp;</td></tr>'."\n";
					}
					
					if (in_array('Activity', $DataTypeArr) && ($YearTermID == '' || $YearTermID == 0))
					{
						$x .= '<tr>'."\n";
							$x .= '<td>'."\n";
								$x .= $this->Get_Management_TransferToSP_Step2_Activity_Table_ViewMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr);
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", "backBtn");
				$x .= '&nbsp;'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();", "backBtn");
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
			
			$x .= $this->GET_HIDDEN_INPUT('AcademicYearID', 'AcademicYearID', $AcademicYearID);
			$x .= $this->GET_HIDDEN_INPUT('YearTermID', 'YearTermID', $YearTermID);
			$x .= $this->GET_HIDDEN_INPUT('DataTypeArr', 'DataTypeArr', implode(',', $DataTypeArr));
			$x .= $this->GET_HIDDEN_INPUT('ActiveMemberOnly', 'ActiveMemberOnly', $ActiveMemberOnly);
			$x .= $this->GET_HIDDEN_INPUT('YearIDArr', 'YearIDArr', implode(',', $YearIDArr));
			$x .= $this->GET_HIDDEN_INPUT('DataNameLang', 'DataNameLang', $DataNameLang);
				
			### iFrame
			$x .= '<iframe id="TransferIFrame" name="TransferIFrame" style="width:100%;height:300px;display:none;"></iframe>'."\n";
			
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";

		return $x;
	}
	
	function Get_Management_TransferToSP_Step2_Club_Table_ViewMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr, $DataNameLang)
	{
		global $libenroll, $Lang, $eEnrollment;
		
		$ClubType = ($YearTermID == '' || $YearTermID == 0)? 'Y' : 'S';
		$ClubInfoArr = $libenroll->GET_GROUPINFO($ParEnrolGroupID='', $AcademicYearID, $ClubType, $YearTermID);
		$numOfClub = count($ClubInfoArr);
		
		$ClubMemberInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupIDArr='', $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('GroupAdmin'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=1,
																$YearTermID, $ClubKeyword='', $ClubCategoryArr='', $ActiveMemberOnly, $YearIDArr);
		
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td>'.$this->GET_NAVIGATION2($Lang['eEnrolment']['Club']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th>'.$Lang['eEnrolment']['ClubName'].'</th>'."\n";
									$x .= '<th>'.$eEnrollment['No_of_Student'].'</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							
							$x .= '<tbody>'."\n";
								if ($numOfClub > 0)
								{
									for ($i=0; $i<$numOfClub; $i++)
									{
										$thisEnrolGroupID = $ClubInfoArr[$i]['EnrolGroupID'];
										
										//2012-0628-1115-06071
										//$thisClubName = $ClubInfoArr[$i]['Title'];
										//$thisClubName = Get_Lang_Selection($ClubInfoArr[$i]['TitleChinese'], $ClubInfoArr[$i]['Title']);
										
										
										if($DataNameLang=='1'){
											$thisClubName = $ClubInfoArr[$i]['Title'];
										}else{
											$thisClubName = $ClubInfoArr[$i]['TitleChinese'];
										}	
										
										
										$thisNumOfMember = count((array)$ClubMemberInfoArr[$thisEnrolGroupID]);
																				
										# check any existing profile records are from Admin Console (if yes, now allow to sync to SP in order to avoid duplicate name of club)
										//$SpClubRecordNotFromModule = $libenroll->Get_SP_Club_Record_Not_From_Module($thisEnrolGroupID, $AcademicYearID, $YearTermID);
									
										$x .= '<tr>'."\n";
											$x .= '<td>'.$thisClubName.'</td>'."\n";
											$x .= '<td>'.$thisNumOfMember.'</td>'."\n";
										$x .= '</tr>'."\n";
									}
								}
								else
								{
									$x .= '<tr><td colspan="8" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_TransferToSP_Step2_Activity_Table_ViewMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr)
	{
		global $libenroll, $Lang, $eEnrollment;
		
		$ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr='', $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ActiveMemberOnly, $YearIDArr);
		$numOfActivity = count($ActivityInfoArr);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td>'.$this->GET_NAVIGATION2($Lang['eEnrolment']['Activity']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th>'.$Lang['eEnrolment']['ClubName'].'</th>'."\n";
									$x .= '<th>'.$eEnrollment['No_of_Student'].'</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							
							$x .= '<tbody>'."\n";
								if ($numOfActivity > 0)
								{
									foreach((array)$ActivityInfoArr as $thisEnrolEventID => $thisActivityInfoArr)
									{
										$thisActivityName = $thisActivityInfoArr['EventTitle'];
										
										$thisMemberInfoArr = $thisActivityInfoArr['StatusStudentArr'][2];
										$thisNumOfMember = count((array)$thisMemberInfoArr);
										
										//if ($thisNumOfMember == 0)
										//	continue;
											
										$x .= '<tr>'."\n";
											$x .= '<td>'.$thisActivityName.'</td>'."\n";
											$x .= '<td>'.$thisNumOfMember.'</td>'."\n";
										$x .= '</tr>'."\n";
									}
								}
								else
								{
									$x .= '<tr><td colspan="8" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_TransferToSP_Step2_PrintUI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr,$DataNameLang)
	{
		global $PATH_WRT_ROOT, $Lang, $eEnrollment, $eEnrollmentMenu;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		$StepsDisplay = $this->GET_IMPORT_STEPS($CurrStep=2, $Lang['eEnrolment']['Transfer_to_SP']['StepArr']);
		
		### Get Academic Year Info
		$AcademicYearObj = new academic_year($AcademicYearID);
		$AcademicYearName = $AcademicYearObj->Get_Academic_Year_Name();
		
		### Get Year Term Info
		if ($YearTermID == '' || $YearTermID == 0)
			$YearTermName = $Lang['eEnrolment']['WholeYear'];
		else
		{
			$YearTermObj = new academic_year_term($YearTermID);
			$YearTermName = $YearTermObj->Get_Academic_Year_Name();
		}
		
		### Get Data Source Info
		$DataTypeDisplayArr = array();
		$numOfDataType = count($DataTypeArr);
		for ($i=0; $i<$numOfDataType; $i++)
		{
			$thisDataType = $DataTypeArr[$i];
			$DataTypeDisplayArr[] = $Lang['eEnrolment'][$thisDataType];
		}
		$DataTypeDisplay = implode(', ', $DataTypeDisplayArr);
		
		### Active Member Only
		$ActiveMemberOnlyDisplay = ($ActiveMemberOnly==1)? $eEnrollment['active_member_only'] : $eEnrollment['all_member'];
		
		### Form Info		
		$libYear = new Year();
		$YearInfoArr = $libYear->Get_All_Year_List();
		$YearInfoArr = BuildMultiKeyAssoc($YearInfoArr, 'YearID');
		
		$YearDisplayArr = array();
		$numOfForm = count($YearIDArr);
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisYearID = $YearIDArr[$i];
			$YearDisplayArr[] = $YearInfoArr[$thisYearID]['YearName']; 
		}
		$YearDisplay = implode(', ', $YearDisplayArr);
		
		
		$x .= '<br />'."\n";
		$x .= '<div class="table_board">'."\n";
			$x .= '<table width="100%" align="center" class="print_hide" border="0">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td align="right">'.$this->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2").'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<br />'."\n";
			$x .= '<table border="0" width="100%" align="center">'."\n";
				$x .= '<tr><td align="center" class="report_title">'.$eEnrollmentMenu['data_handling_to_SP'].' '.$Lang['eEnrolment']['Transfer_to_SP']['Button']['PrintDetailsData'].'</td></tr>'."\n";
			$x .= '</table>'."\n";
			
			if (in_array('Club', $DataTypeArr))
				$x .= $this->Get_Management_TransferToSP_Step2_Club_Table_PrintMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr, $DataNameLang);
				
			if (in_array('Activity', $DataTypeArr) && ($YearTermID == '' || $YearTermID == 0))
				$x .= $this->Get_Management_TransferToSP_Step2_Activity_Table_PrintMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr);
			
			$x .= '<table align="left" border="0">'."\n";
				$x .= '<tr><td>'.$this->Get_Student_Role_And_Status_Remarks().'</td></tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		$x .= '<br />'."\n";

		return $x;
	}
	
	function Get_Management_TransferToSP_Step2_Club_Table_PrintMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr, $DataNameLang)
	{
		global $PATH_WRT_ROOT, $libenroll, $Lang, $eEnrollment;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$ObjAcademicYear = new academic_year($AcademicYearID);
		$AcademicYearName = $ObjAcademicYear->Get_Academic_Year_Name();
		
		$ClubType = ($YearTermID == '' || $YearTermID == 0)? 'Y' : 'S';
		$ClubInfoArr = $libenroll->GET_GROUPINFO($ParEnrolGroupID='', $AcademicYearID, $ClubType, $YearTermID);
		$numOfClub = count($ClubInfoArr);
		
		$ClubMemberInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupIDArr='', $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('GroupAdmin'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=1,
																$YearTermID, $ClubKeyword='', $ClubCategoryArr='', $ActiveMemberOnly, $YearIDArr);
		
		$DisplayedEnrolGroupIDArr = array();
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td>'.$this->GET_NAVIGATION2($Lang['eEnrolment']['Club']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						if ($numOfClub > 0)
						{
							for ($i=0; $i<$numOfClub; $i++)
							{
								$thisEnrolGroupID = $ClubInfoArr[$i]['EnrolGroupID'];
								if($DataNameLang=='1'){
									$thisClubName = $ClubInfoArr[$i]['Title'];
								}else{
									$thisClubName = $ClubInfoArr[$i]['TitleChinese'];
								}
								
								
								$thisMemberInfoArr = $ClubMemberInfoArr[$thisEnrolGroupID];
								$thisNumOfMember = count($thisMemberInfoArr);
								
								$x .= '<table class="common_table_list_v30" style="width:100%">'."\n";
									$x .= '<col align="left" style="width:10%;" />'."\n";
									$x .= '<col align="left" style="width:10%;" />'."\n";
									$x .= '<col align="left" style="width:20%;" />'."\n";
									$x .= '<col align="left" style="width:20%;" />'."\n";
									$x .= '<col align="left" style="width:30%;" />'."\n";
									$x .= '<col align="left" style="width:10%;" />'."\n";
									
									## Header
									$x .= '<thead>'."\n";
										$x .= '<tr>'."\n";
											$x .= '<th colspan="8" class="sub_row_top">'.$thisClubName.'</th>'."\n";
										$x .= '</tr>'."\n";
										$x .= '<tr>'."\n";
											$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Class'].' ('.$AcademicYearName.')</th>'."\n";
											$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].' ('.$AcademicYearName.')</th>'."\n";
											$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>'."\n";
											$x .= '<th>'.$Lang['eEnrolment']['Role'].'</th>'."\n";
											$x .= '<th>'.$eEnrollment['performance'].'</th>'."\n";
											$x .= '<th>'.$eEnrollment['active'].'/'.$eEnrollment['inactive'].'</th>'."\n";
										$x .= '</tr>'."\n";
									$x .= '</thead>'."\n";
									
									$x .= '<tbody>'."\n";
										if ($thisNumOfMember > 0)
										{
											for ($j=0; $j<$thisNumOfMember; $j++)
											{
												$thisClassName = $thisMemberInfoArr[$j]['ClassName'];
												$thisClassNumber = $thisMemberInfoArr[$j]['ClassNumber'];
												$thisStudentName = $thisMemberInfoArr[$j]['StudentName'];
												$thisRoleTitle = ($thisMemberInfoArr[$j]['RoleTitle']=='')? '&nbsp;' : $thisMemberInfoArr[$j]['RoleTitle'];
												$thisPerformance = ($thisMemberInfoArr[$j]['Performance']=='')? '&nbsp;' : $thisMemberInfoArr[$j]['Performance'];
												$thisComment = ($thisMemberInfoArr[$j]['Comment']=='')? '&nbsp;' : $thisMemberInfoArr[$j]['Comment'];
												$thisActiveMemberStatus = $thisMemberInfoArr[$j]['ActiveMemberStatus'];
												
												$x .= '<tr>'."\n";
													$x .= '<td>'.$thisClassName.'</td>'."\n";
													$x .= '<td>'.$thisClassNumber.'</td>'."\n";
													$x .= '<td>'.$thisStudentName.'</td>'."\n";
													$x .= '<td>'.$thisRoleTitle.'</td>'."\n";
													$x .= '<td>'.$thisPerformance.'</td>'."\n";
													$x .= '<td>'.$thisActiveMemberStatus.'</td>'."\n";
												$x .= '</tr>'."\n";
											}
										}
										else
										{
											$x .= '<tr><td colspan="6" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
										}
										
									$x .= '</tbody>'."\n";
								$x .= '</table>'."\n";
								$x .= '<br style="clear:both;" />'."\n";
								
								$DisplayedEnrolGroupIDArr[] = $thisEnrolGroupID;
							}
						}
						
						if ($numOfClub==0 || count($DisplayedEnrolGroupIDArr)==0)
						{
							$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
								$x .= '<tr><td colspan="8" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							$x .= '</table>'."\n";
						}
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_TransferToSP_Step2_Activity_Table_PrintMode($AcademicYearID, $YearTermID, $ActiveMemberOnly, $YearIDArr)
	{
		global $PATH_WRT_ROOT, $libenroll, $Lang, $eEnrollment;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$ObjAcademicYear = new academic_year($AcademicYearID);
		$AcademicYearName = $ObjAcademicYear->Get_Academic_Year_Name();
		
		$ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr='', $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ActiveMemberOnly, $YearIDArr);
		$numOfActivity = count($ActivityInfoArr);
							
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td>'.$this->GET_NAVIGATION2($Lang['eEnrolment']['Activity']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div>'."\n";
						if ($numOfActivity > 0)
						{
							foreach((array)$ActivityInfoArr as $thisEnrolEventID => $thisActivityInfoArr)
							{
								$thisActivityName = $thisActivityInfoArr['EventTitle'];
								$thisMemberInfoArr = $thisActivityInfoArr['StatusStudentArr'][2];
								$thisNumOfMember = count($thisMemberInfoArr);
								
								$x .= '<table class="common_table_list_v30" style="width:100%">'."\n";
									$x .= '<col align="left" style="width:10%;" />'."\n";
									$x .= '<col align="left" style="width:10%;" />'."\n";
									$x .= '<col align="left" style="width:20%;" />'."\n";
									$x .= '<col align="left" style="width:20%;" />'."\n";
									$x .= '<col align="left" style="width:30%;" />'."\n";
									$x .= '<col align="left" style="width:10%;" />'."\n";
									
									## Header
									$x .= '<thead>'."\n";
										$x .= '<tr>'."\n";
											$x .= '<th colspan="8" class="sub_row_top">'.$thisActivityName.'</th>'."\n";
										$x .= '</tr>'."\n";
										$x .= '<tr>'."\n";
											$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Class'].' ('.$AcademicYearName.')</th>'."\n";
											$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].' ('.$AcademicYearName.')</th>'."\n";
											$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>'."\n";
											$x .= '<th>'.$Lang['eEnrolment']['Role'].'</th>'."\n";
											$x .= '<th>'.$eEnrollment['performance'].'</th>'."\n";
											$x .= '<th>'.$eEnrollment['active'].'/'.$eEnrollment['inactive'].'</th>'."\n";
										$x .= '</tr>'."\n";
									$x .= '</thead>'."\n";
									
									$x .= '<tbody>'."\n";
										if ($thisNumOfMember > 0)
										{
											for ($j=0; $j<$thisNumOfMember; $j++)
											{
												$thisClassName = $thisMemberInfoArr[$j]['ClassName'];
												$thisClassNumber = $thisMemberInfoArr[$j]['ClassNumber'];
												$thisStudentName = $thisMemberInfoArr[$j]['StudentName'];
												$thisRoleTitle = ($thisMemberInfoArr[$j]['RoleTitle']=='')? '&nbsp;' : $thisMemberInfoArr[$j]['RoleTitle'];
												$thisPerformance = ($thisMemberInfoArr[$j]['Performance']=='')? '&nbsp;' : $thisMemberInfoArr[$j]['Performance'];
												$thisComment = ($thisMemberInfoArr[$j]['Comment']=='')? '&nbsp;' : $thisMemberInfoArr[$j]['Comment'];
												$thisActiveMemberStatus = $thisMemberInfoArr[$j]['ActiveMemberStatus'];
												
												$x .= '<tr>'."\n";
													$x .= '<td>'.$thisClassName.'</td>'."\n";
													$x .= '<td>'.$thisClassNumber.'</td>'."\n";
													$x .= '<td>'.$thisStudentName.'</td>'."\n";
													$x .= '<td>'.$thisRoleTitle.'</td>'."\n";
													$x .= '<td>'.$thisPerformance.'</td>'."\n";
													$x .= '<td>'.$thisActiveMemberStatus.'</td>'."\n";
												$x .= '</tr>'."\n";
											}
										}
										else
										{
											$x .= '<tr><td colspan="6" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
										}
									$x .= '</tbody>'."\n";
								$x .= '</table>'."\n";
								$x .= '<br style="clear:both;" />'."\n";
							}
						}
						else
						{
							$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
								$x .= '<tr><td colspan="8" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							$x .= '</table>'."\n";
						}
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Management_TransferToSP_Step3_UI($ReturnMsgKey)
	{
		global $Lang;
		
		$StepsDisplay = $this->GET_IMPORT_STEPS($CurrStep=3, $Lang['eEnrolment']['Transfer_to_SP']['StepArr']);
		$ReturnMsg = $Lang['eEnrolment']['Transfer_to_SP']['ReturnMsgArr']['DataTransferArr'][$ReturnMsgKey];
		
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="POST">';
			$x .= '<div class="table_board">'."\n";
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
					$x .= '<tr><td>'.$StepsDisplay.'</td></tr>'."\n";
					$x .= '<tr><td>&nbsp;</td></tr>'."\n";
					$x .= '<tr><td style="text-align:center;">'.$ReturnMsg.'</td></tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<br style="clear:both;" />'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['eEnrolment']['Transfer_to_SP']['Button']['TransferOtherRecords'], "button", "js_Go_Transfer_Other_Records();", "submitBtn");
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n"; 
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";

		return $x;
	}
	
	function Get_Data_Type_Checkbox_Table($DataTypeArr='')
	{
		global $PATH_WRT_ROOT, $Lang;
		$thisChecked = ($DataTypeArr=='')? true : false;
		$SelectAllCheckBox = $this->Get_Checkbox($ID="SelectAll_DataTypeChk", $Name='', $Value='', $thisChecked, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('DataTypeArr[]', this.checked);");
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="border-bottom:0px;">'."\n";
					$x .= $SelectAllCheckBox."\n";
					$x .= '<br />'."\n";
					$thisChecked = ($DataTypeArr=='' || in_array('Club', (array)$DataTypeArr))? true : false;
					$thisCheckbox = $this->Get_Checkbox('DataTypeArr_Club', 'DataTypeArr[]', 'Club', $thisChecked, $Class='DataTypeChk', $Lang['eEnrolment']['Club'], $Onclick="Uncheck_SelectAll('SelectAll_DataTypeChk', this.checked);");
					$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCheckbox."<br />\n";
					$thisChecked = ($DataTypeArr=='' || in_array('Activity', (array)$DataTypeArr))? true : false;
					$thisCheckbox = $this->Get_Checkbox('DataTypeArr_Activity', 'DataTypeArr[]', 'Activity', $thisChecked, $Class='DataTypeChk', $Lang['eEnrolment']['Activity'], $Onclick="Uncheck_SelectAll('SelectAll_DataTypeChk', this.checked);");
					$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCheckbox."<br />\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Student_Attendance_Selection($Id, $Name, $TargetStatus, $Class='', $Disabled=0)
	{
		global $Lang;
		include_once("libclubsenrol.php");
		$libenroll = new libclubsenrol();
		
		$OptionArr = array();
		$OptionArr[] = array(1, $Lang['eEnrolment']['Attendance']['Present']);
		$OptionArr[] = array(3, $Lang['eEnrolment']['Attendance']['Absent']);
		$OptionArr[] = array(2, $Lang['eEnrolment']['Attendance']['Exempt']);
			
		if ($libenroll->enableAttendanceLateStatusRight()){
			$OptionArr[] = array(ENROL_ATTENDANCE_LATE, $Lang['eEnrolment']['Attendance']['Late']);		
		}
		if ($libenroll->enableAttendanceEarlyLeaveStatusRight()){
			$OptionArr[] = array(ENROL_ATTENDANCE_EARLY_LEAVE, $Lang['eEnrolment']['Attendance']['EarlyLeave']);
		}
		//$OptionArr[] = array(4, $Lang['eEnrolment']['Attendance']['Late']);
		
		$tag = ' id="'.$Id.'" name="'.$Name.'" class="'.$Class.'" ';
		if ($Disabled == 1)
			$tag .= ' disabled ';
			
		return getSelectByArray($OptionArr, $tag, $TargetStatus, $all=0, $noFirst=0);
	}
	
	function Get_Club_Selection($ID, $Name, $TargetEnrolGroupIDAry, $CategoryID='', $IsAll=0, $NoFirst=0, $OnChange='',$AcademicYearID='', $IncludeEnrolGroupIdAry='', $Disabled=false, $Class='', $isMultiple=false, $ApplyUserType='', $ParDefaultSelectedAll=false)
	{
		global $Lang, $libenroll, $intranet_session_language, $plugin;
		
		$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
		
		if (!is_array($TargetEnrolGroupIDAry)) {
			$TargetEnrolGroupIDAry = array($TargetEnrolGroupIDAry);
		}
	
		if($plugin['SIS_eEnrollment']){
			$roundFilter = $libenroll->targetRound;
		}
		$ClubInfoArr = $libenroll->Get_All_Club_Info('', $AcademicYearID, $CategoryID, '', '', '', 'asc', $ApplyUserType, $roundFilter);
		$numOfClub = count($ClubInfoArr);
		
       	if ($OnChange != '') {
       		$onchange = ' onchange="'.$OnChange.'" ';
       	}
       	if ($Disabled) {
       		$disabled_tag = ' disabled ';
       	}
       	if ($Class != '') {
       		$class_tag = ' class="'.$Class.'" ';
       	}
       	
       	if ($isMultiple) {
       		$multiple_tag = ' multiple="multiple" size="10" ';
       	}
       	
       	$thisTag = ' id="'.$ID.'" name="'.$Name.'" '.$onchange.' '.$disabled_tag.' '.$class_tag.' '.$multiple_tag;
       	$thisTitle = Get_Selection_First_Title($Lang['eEnrolment']['SelectClub']);
       	
       	//return getSelectByArray($SelectionArr, $thisTag, $TargetEnrolGroupID, $IsAll, $NoFirst, $thisTitle);
       	
       	$x = '';
       	$x .= '<select '.$thisTag.'>'."\n";
       		if ($NoFirst == 0) {
				//$emptySelected = ($TargetEnrolGroupID == '')? ' selected="selected" ' : '';
       			$emptySelected = (empty($TargetEnrolGroupIDAry))? ' selected="selected" ' : '';
				
				$x .= '<option value="" '.$emptySelected.'>'.$thisTitle.'</option>'."\n";
			}
	
       		for($i=0; $i<$numOfClub; $i++) {
	       		// TBD: access right checking
	       		$_enrolGroupId = $ClubInfoArr[$i]['EnrolGroupID'];
	       		$_title = Get_Lang_Selection($ClubInfoArr[$i]['TitleChineseWithSemester'], $ClubInfoArr[$i]['TitleWithSemester']);
	       		$_yearTermId = $ClubInfoArr[$i]['YearTermID'];
	       		$_termNumber = $libenroll->Get_Term_Number($_yearTermId);
	       		$_categoryName = $ClubInfoArr[$i]['CategoryName']==''? '': ' ('.$ClubInfoArr[$i]['CategoryName'].')';
	       		$_categoryID = $ClubInfoArr[$i]['GroupCategory'];
	       		
	       		if (is_array($IncludeEnrolGroupIdAry) && !in_array($_enrolGroupId, $IncludeEnrolGroupIdAry)) {
	       			continue;
	       		}
	       		
	       		$_selectedAttr = '';
	       		//if ($_enrolGroupId == $TargetEnrolGroupID||$ParDefaultSelectedAll) {
	       		if (in_array($_enrolGroupId, $TargetEnrolGroupIDAry) || $ParDefaultSelectedAll) {
	       			$_selectedAttr = ' selected="selected" ';
	       		}
	       		
	       		$x .= '<option value="'.$_enrolGroupId.'" termnumber="'.$_termNumber.'" '.$_selectedAttr.'  id="'.$_categoryID.'">'.$_title.$_categoryName.'</option>'."\n";
	       	}
       	$x .= '</select>'."\n";
       	
       	return $x;
	}
	
	function Get_Activity_Selection($ID, $Name, $TargetEnrolEventID, $CategoryID='', $IsAll=0, $NoFirst=0, $OnChange='', $AcademicYearID='', $isMultiple=false)
	{
		global $Lang, $libenroll;
		//debug_pr($TargetEnrolEventID);
		if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
		
		$ActivityInfoArr = $libenroll->GET_ADMIN_EVENTINFO_LIST($CategoryID, "", $AcademicYearID);
		//debug_pr($ActivityInfoArr);
		$numOfActivity = count($ActivityInfoArr);
		
       	$SelectionArr = array();
       	for($i=0; $i<$numOfActivity; $i++) 
       	{
       		// TBD: access right checking
       		
	     	$SelectionArr[$i] = array($ActivityInfoArr[$i]['EnrolEventID'], $ActivityInfoArr[$i]['EventTitle']);
       	}
       	
       	if ($OnChange != '')
       		$onchange = ' onchange="'.$OnChange.'" ';
       	
       	if ($isMultiple) {
       		$multiple_tag = ' multiple="multiple" size="10" ';
       	}
       	
       	$thisTag = ' id="'.$ID.'" name="'.$Name.'" '.$onchange.' '.$multiple_tag;
       	$thisTitle = Get_Selection_First_Title($Lang['eEnrolment']['SelectClub']);
       	//debug_pr($TargetEnrolEventID);
       	
       	return getSelectByArray($SelectionArr, $thisTag, $TargetEnrolEventID, $IsAll, $NoFirst, $thisTitle);
	}
	
	function Get_Management_Club_Attendance_UI($EnrolGroupID, $CanEdit, $PageAction,$AcademicYearID='')
	{
		global $eEnrollment, $Lang, $image_path, $LAYOUT_SKIN, $eEnrollment;
		global $libenroll;
		
		$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
		
		### Get Club Info
		$ClubInfoArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
		$GroupID = $ClubInfoArr['GroupID'];
		
		### Get Club Meeting Date
		$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);
		$numOfDate = count($DateArr);

		### Get Club Attendance Records
		$ClubAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($EnrolGroupID,'',$AcademicYearID);
		$ClubAttendanceInfoArr = $ClubAttendanceInfoArr[$EnrolGroupID];
		
		$x = '';
		
		### Toolbar
		$type = 2;
		$export_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type";
		$type = "clubAttendance";
		$import_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&type=$type";
		
		// hide tools bar if no attendance record
		if ($numOfDate > 0 && $PageAction != "view")
		{
			$x .= '<div style="width:100%;">'."\n";
				if($libenroll->AllowToEditPreviousYearData) {
					$x .= $this->GET_LNK_IMPORT(($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "import.php?$import_parameters", '', '', '', '', '', 0);
					$x .= toolBarSpacer();
				}
				$x .= $this->GET_LNK_EXPORT("export.php?$export_parameters", '', '', '', '', '', 0);
				$x .= toolBarSpacer();
				$x .= $this->GET_LNK_PRINT("javascript:newWindow('print.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=clubAttendance', 24)", $Lang['Btn']['Print'], '', '', '', '', 0);
				$x .= toolBarSpacer();
				
				# Designer is busy for other that haven't provide the new IP25 standard coding, use old style first
                $x .= "<div style='float: left; padding-top: 2px; padding-bottom: 5px; margin-left: 3px; background: url(".$image_path."/".$LAYOUT_SKIN."/eOffice/icon_viewstat.gif); background-repeat: no-repeat; background-position: left center;'>";
                    $x .= "<a href='javascript:newWindow(\"view_stat.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=clubAttendance\", 8);' class='contenttool' style='font-family:\"Century Gothic\", \"Microsoft JhengHei\", Verdana, Arial, Helvetica, sans-serif, 新細明體, 細明體_HKSCS, mingliu; padding-left:20px; line-height: 20px;'>";
                        $x .= $eEnrollment['view_statistics'];
                    $x .= "</a>";
                $x .= "</div>";
				//$x .= "<a href='javascript:newWindow(\"view_stat.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=clubAttendance\", 8);' class='contenttool' style='width:320px;display:block; padding-top:4px; font-family:Verdana, Arial, Helvetica, sans-serif;'><img src='".$image_path."/".$LAYOUT_SKIN ."/eOffice/icon_viewstat.gif' border='0' align='absmiddle'>". $eEnrollment['view_statistics'] ."</a>";
				
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
			//$x .= '<br style="clear:both;" />'."\n";
		}
		
		/* 2011-06-03 YatWoon - Hidden chart - case #2011-0526-1008-21067 
		### Show Attendance Chart if the User can edit the Attendance
		if ($CanEdit && $PageAction != "view")
		{
			$x .= '<div style="width:100%;">'."\n";
				$x .= $this->Get_Management_Club_Attendance_Chart($EnrolGroupID, $ClubAttendanceInfoArr);
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
		}
		*/
		
		### Show Attendance Info Table
		$x .= $this->Get_Management_Club_Attendance_Info_Table($EnrolGroupID, $CanEdit, $PageAction, $ClubAttendanceInfoArr,'',$AcademicYearID);
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Management_Club_Attendance_Chart($EnrolGroupID, $ClubAttendanceInfoArr, $returnDataOnly=false)
	{
		global $eEnrollment, $eEnrollmentMenu, $libenroll;
		
		### Preparing Chart Data
		$ChartDataArr = array();
		foreach ((array)$ClubAttendanceInfoArr['MeetingDateArr'] as $thisGroupDateID => $thisDateAttendanceArr)
		{
			$thisActivityDateStart = $thisDateAttendanceArr['ActivityDateStart'];
			$thisActivityDateEnd = $thisDateAttendanceArr['ActivityDateEnd'];
			
			# Display Title
			$thisDateTitle = date("Y-m-d H:i", strtotime($thisActivityDateStart)).'-'.date("H:i", strtotime($thisActivityDateEnd));
			
			# Count Total Attendance of Student
			$thisAttendance = 0;
			$thisLate = 0;
			$thisExempt = 0;
			$thisEarlyLeave = 0;
			foreach ((array)$thisDateAttendanceArr['AttendanceDataArr'] as $thisRecordStatus => $thisAttendanceArr)
			{
			//	if ($thisRecordStatus == ENROL_ATTENDANCE_PRESENT || $thisRecordStatus == ENROL_ATTENDANCE_EXEMPT || $thisRecordStatus == ENROL_ATTENDANCE_LATE)
				/*
				if($libenroll->enableAttendanceLateStatusRight()){
					if ($thisRecordStatus == ENROL_ATTENDANCE_PRESENT || $thisRecordStatus == ENROL_ATTENDANCE_LATE){		
						$thisAttendance += $thisAttendanceArr['AttendanceCount'];
					}
				}else{
					if ($thisRecordStatus == ENROL_ATTENDANCE_PRESENT){		
						$thisAttendance += $thisAttendanceArr['AttendanceCount'];
					}
				} */
				/*
				 * if (
				 * $thisRecordStatus == ENROL_ATTENDANCE_PRESENT
				 * ||$thisRecordStatus == ENROL_ATTENDANCE_LATE && $libenroll->enableAttendanceLateStatusRight()
				 * ||$thisRecordStatus == ENROL_ATTENDANCE_EARLY_LEAVE && $libenroll->enableAttendanceEarlyLeaveStatusRight()
				 * ){
				 *   $thisAttendance += $thisAttendanceArr['AttendanceCount'];
				 * }
				 * 
				 */
				if ($thisRecordStatus == ENROL_ATTENDANCE_PRESENT){
					$thisAttendance += $thisAttendanceArr['AttendanceCount'];
				} else if ($thisRecordStatus == ENROL_ATTENDANCE_EXEMPT){
					$thisAttendance += $thisAttendanceArr['AttendanceCount'];
					$thisExempt += $thisAttendanceArr['AttendanceCount'];
				} else if ($thisRecordStatus == ENROL_ATTENDANCE_LATE && $libenroll->enableAttendanceLateStatusRight()){
					$thisAttendance += $thisAttendanceArr['AttendanceCount'];
					$thisLate += $thisAttendanceArr['AttendanceCount'];
				} else if ($thisRecordStatus == ENROL_ATTENDANCE_EARLY_LEAVE && $libenroll->enableAttendanceEarlyLeaveStatusRight()){
					$thisAttendance += $thisAttendanceArr['AttendanceCount'];
					$thisEarlyLeave += $thisAttendanceArr['AttendanceCount'];
				}
			}
			if(!$libenroll->enableAttendanceLateStatusRight())
				$thisLate = null;
			if(!$libenroll->enableAttendanceEarlyLeaveStatusRight())
				$thisEarlyLeave = null;
				
			$ChartDataArr[] = array($thisDateTitle, $thisAttendance, $thisExempt, $thisLate, $thisEarlyLeave);
		}
		
		if ($returnDataOnly == true) {
			return $ChartDataArr;
		}
		else {

			$ConfArr['IsHorizontal'] = 1;
			$ConfArr['ChartX'] = $eEnrollmentMenu['act_date_time'];
			$ConfArr['ChartY'] = $eEnrollmentMenu['head_count'];
			$ConfArr['MaxWidth'] = 500;
			
			$x = '';
			//$x .= $this->GET_NAVIGATION2($eEnrollment['view_statistics']);
			//$x .= '<br />'."\n";
			$x .= $this->GEN_CHART($ConfArr, $ChartDataArr);
			
			return $x;
		}
		
		
	}
	
	
	function Get_Management_Club_Attendance_Info_Table($EnrolGroupID, $CanEdit, $PageAction, $ClubAttendanceInfoArr=array(), $DisplayPhoto=0, $AcademicYearID='')
	{
		global $eEnrollment, $Lang, $image_path, $LAYOUT_SKIN, $libenroll, $PATH_WRT_ROOT, $DisplayAllPhoto, $HiddenAllPhoto, $sys_custom;
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		include_once($PATH_WRT_ROOT."includes/user_right_target.php");
		
		$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
		$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
		$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
		$isClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club");
		$isClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolGroupID, "Club");
		
		##### 
		if(empty($ClubAttendanceInfoArr))
		{
			### Get Club Attendance Records
			$ClubAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($EnrolGroupID);
			$ClubAttendanceInfoArr = $ClubAttendanceInfoArr[$EnrolGroupID];	
		}
		
		### Get Club Meeting Dates
		$GroupDateIDArr = array_keys((array)$ClubAttendanceInfoArr['MeetingDateArr']);
		
		$numOfMeetingDate = count($GroupDateIDArr);
		
		### Special checking for HELPER - disable the attendance updating if the 
		$disabledArr = array();
		
		if ($isClubHelper)
		{
			$StaffRole = "H";	// 'H' for Helper
			
			# Control Attendance Helper's Right
// 			debug_pr($libenroll->disableHelperModificationRightLimitation);
			if($libenroll->disableHelperModificationRightLimitation !=true){
			// check for each Date
				for ($i=0; $i<$numOfMeetingDate; $i++)
				{
					$tempGroupDateID = $GroupDateIDArr[$i];
					
					// check if there are any attendance records in DB
					$sql = "SELECT GroupDateID, DateModified, LastModifiedRole,LastModifiedBy FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE EnrolGroupID = '$EnrolGroupID' AND GroupDateID = '$tempGroupDateID'";
					$result = $libenroll->returnArray($sql,2);
					
					$sql = "SELECT ActivityDateStart, ActivityDateEnd FROM INTRANET_ENROL_GROUP_DATE WHERE GroupDateID = ". $result[0]["GroupDateID"];
					$activityDate = $libenroll->returnArray($sql,2);
					
					# no attendance record yet => enable checkboxes for edit					
			
					# otherwise, do checking
					if (count($result) > 0)
					{
// 						if ($result[0]['LastModifiedRole'] == "A")
// 						{
// 							$disabledArr[$tempGroupDateID] = "DISABLED";	
// 						}
// 						else
// 						{
                        $disabledArr[$tempGroupDateID] = "DISABLED";	
							# disable checkboxes if last modified date is not today
//                        if (date("Y-m-d") == date("Y-m-d", strtotime($result[0]['DateModified'])) && ($result[0]['LastModifiedBy'] == $_SESSION['UserID']))
                        if (date("Y-m-d") == date("Y-m-d", strtotime($activityDate[0]['ActivityDateStart'])) && date("Y-m-d") == date("Y-m-d", strtotime($activityDate[0]['ActivityDateEnd'])))                        {
							$disabledArr[$tempGroupDateID] = "ENABLED";	
						}
// 						} 
					}
				}	
			}
		}
		else if($isClubPIC){
			$StaffRole = ""; // update $StaffRole when PIC status
		}
		else if($isEnrolAdmin || $isEnrolMaster)
		{
			$StaffRole = "A"; // update $StaffRole when Admin status
		}
		
		
		### Display the Student / Children only if it is in view mode
		$DisplayStudentIDArr = array();
		if ($PageAction == "view")
		{
			if ($_SESSION['UserType'] == USERTYPE_STUDENT)
			{
				$DisplayStudentIDArr[] = $_SESSION['UserID'];
			}
			else if ($_SESSION['UserType'] == USERTYPE_PARENT)
			{
				$libuser = new libuser($_SESSION['UserID']);
				$ChildArr = $libuser->getChildrenList();
				$DisplayStudentIDArr = Get_Array_By_Key($ChildArr, 'StudentID');
			}
		}
		
		
		### Get Club Members
		$ClubMemberInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0);
		
		$StudentIDArr = Get_Array_By_Key($ClubMemberInfoArr, 'StudentID');
		
		if ($libenroll->enableUserJoinDateRange()) {
			$enrolAvailDateArr = $libenroll->Get_Enrol_AvailDate($EnrolGroupID, $StudentIDArr, "Club");
		}
		
		$numOfMember = count($StudentIDArr);
		
		$x = '
			<div id="div_Club_AttendanceTable">
			<script language="javascript">
			function toggle_photo()
			{ 
				Reload_Club_Attendance_Info_Table2("'.$EnrolGroupID.'","'.$CanEdit.'","'.$PageAction.'", "'. ($DisplayPhoto==1 ? "0" : "1") .'");
			}
			</script>
		';
		
		### Get Member Attendance Records
		$MemberAttendanceInfoArr = $libenroll->Get_Student_Club_Attendance_Info($StudentIDArr, array($EnrolGroupID));
		
		### Navigation
		if ($CanEdit && $PageAction != 'view') {
			$x .= '<br />';
			$x .= $this->GET_NAVIGATION2($eEnrollment['take_attendance']);
		}
		
		if ($numOfMember==0)
		{
			$x .= '<br />'."\n";
			$x .= $Lang['eEnrolment']['Warning']['ClubHasNoMember'];
			$x .= '<br />'."\n";
			$x .= '<br />'."\n";
		}
		else
		{
			$DisplayPhotoBtn .= "<a href=\"javascript:toggle_photo();\" class=\"tabletool\"><span id=\"DisplayAllPhotoTxt\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo_". ($DisplayPhoto? "zoomout" : "zoomin") .".gif\" border=\"0\" align=\"absmiddle\" alt=\"$DisplayAllPhoto\"></span></a>";

			$x .= '<table id="AttendanceTable">'."\n";
				### Table Header
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						# Class
						$x .= '<th align="center" class="tableTitle tabletopnolink" width="50">'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>'."\n";
						
						# Class No.
						$x .= '<th align="center" class="tableTitle tabletopnolink" width="50">'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\n";
						
						# Student Name
						$x .= '<th class="tableTitle" nowrap style="border-right-width:5px"><span class="tabletopnolink">'.$eEnrollment['student_name'].'</span> '. $DisplayPhotoBtn .'</th>'."\n";
						
						if ($libenroll->enableUserJoinDateRange() && $CanEdit) {
							$x .= '<th class="tableTitle" colspan="2" align="center"><span class="tabletopnolink">'. $Lang['eEnrolment']['AvailiableDate'] .'</span></th>'."\n";
						}
						# Meeting Dates
						for ($i=0; $i<$numOfMeetingDate; $i++)
						{
							$thisGroupDateID = $GroupDateIDArr[$i];
							$thisActivityDateStart = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateStart'];
							$thisActivityDateEnd = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateEnd'];
							$thisDateTimeDisplay = date("Y-m-d", strtotime($thisActivityDateStart)).'<br />'.date("H:i", strtotime($thisActivityDateStart)).'-'.date("H:i", strtotime($thisActivityDateEnd));
							
							if($libenroll->enableTakeAttendanceFutureDate() && ($isEnrolAdmin == 1 || $isClubPIC == 1 || $isEnrolMaster == 1)){
								$disabledArr[$thisGroupDateID] = "ENABLED";
							}else{
								# disable if the date is in the future
								if (date("Y-m-d") < date("Y-m-d", strtotime($thisActivityDateStart)))
									$disabledArr[$thisGroupDateID] = "DISABLED";
							}
							
							# disable if the user is a normal student only or is in view mode
							if (!$CanEdit || $PageAction == "view")
								$disabledArr[$thisGroupDateID] = "DISABLED";
								
							$disable = $disabledArr[$thisGroupDateID] == "DISABLED"? "1":"0";

							$x .= '<th class="tableTitle tabletopnolink" align="center" nowrap>'."\n";
								$x .= '<span>'.$thisDateTimeDisplay.'</span>'."\n";
								if ($PageAction != "view")
								{
									if (!$libenroll->enableUserJoinDateRange()) {
										### Apply All Selection
										$thisSelection = $this->Get_Student_Attendance_Selection("Header_Col".$i, "Header_Col".$i, $thisAttendanceStatus='', "Header_Col".$i, $disable);
                    					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
                    					    $thisSelection .= '<br /><input class="Header_Col'.$i.'" placeholder="'.$Lang['General']['Remark'].'">';
                    					}
                    					
										$thisApplyAllIcon = $this->Get_Apply_All_Icon("javascript:CheckAll('".$i."');");
									}
									//--- Henry added 20131101 [start]
									$thisSendMailLink = "";
									$thisSendMailLink .= '<div style="float:left;width:30%">';
									$thisSendMailLink .= '<a id="sendMailThickboxLink" href="javascript:load_dyn_size_thickbox_ip(\''.$thisActivityDateStart.' '.$Lang['eEnrolment']['AbsentNotification'].'\', \'onloadThickBox('.$thisGroupDateID.','.$EnrolGroupID.');\', inlineID=\'\', defaultHeight=600, defaultWidth=800);" class="contenttool">';
									$thisSendMailLink .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/iMail/icon_compose.gif\"  border=\"0\" align=\"absmiddle\"> ";	
									$thisSendMailLink .= "</a>";
									$thisSendMailLink .= '</div>';
									//--- Henry added 20131101 [end]
									$x .= '<br />'."\n";
									$x .= '<div style="width:100%;text-align:center;">'.$thisSelection.'</div>'."\n";
									
									if ($libenroll->enableAttendanceScheduleDetail() || $libenroll->enableUserJoinDateRange()) {
										$thisActivityAttendanceDetailIcon = "<a class='icon_batch_assign' href=\"javascript:Reload_CLUB_Attendance_Info_Schedule_Detail_Table('".$thisGroupDateID."');\">" . $this->Get_View_Image() . "</a>";
										$x .= '<div style="width:12%;float:left;">&nbsp;</div>'.$thisActivityAttendanceDetailIcon.' &nbsp; '.$thisApplyAllIcon."\n";
									} else {
										$x .= '<div style="width:40%;float:left;">&nbsp;</div>'.$thisApplyAllIcon."\n";
									}
									
									//--- Henry added 20131101 [start]
									if($sys_custom['eEnrolment']['AttendanceSendEmailToAbsentStudent'] && !$libenroll->IS_CLUB_HELPER($EnrolGroupID)){
										$x .= $thisSendMailLink."\n";
									}
									//--- Henry added 20131101 [end]
								}
							$x .= '</th>'."\n";
						}
						
						# Attendance %
						$x .= '<th class="tableTitle" nowrap align="center">'."\n";
							$x .= '<span class="tabletopnolink">'."\n";
								$x .= $eEnrollment['attendance_rate']."\n";
								$x .= '<br />'."\n";
								$x .= '('.$eEnrollment['up_to'].' '.date("Y-m-d").')'."\n";
							$x .= '</span>'."\n";
						$x .= '</th>'."\n";
						
						# Attended Hours
						$x .= '<th class="tableTitle" nowrap align="center">'."\n";
							$x .= '<span class="tabletopnolink">'."\n";
								$x .= $Lang['eEnrolment']['AttendanceHour']."\n";
								$x .= '<br />'."\n";
								$x .= '('.$eEnrollment['up_to'].' '.date("Y-m-d").')'."\n";
							$x .= '</span>'."\n";
						$x .= '</th>'."\n";
						
// 						if($sys_custom['eEnrolment']['showGuardianInfo']){
						# guardian info
						if($_SESSION['UserType']!='2'){
							$x .= '<th class="tableTitle" nowrap align="center">'."\n";
								$x .= '<span class="tabletopnolink">'."\n";
									$x .= $Lang['StudentRegistry']['GuardianInfo'];
								$x .= '</span>'."\n";
							$x .= '</th>'."\n";
// 						}
						}
						
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
			
				
				### Table Content
				$libuserObj = new libuser("", "", $StudentIDArr);
				$x .= '<tbody>'."\n";
				//debug_pr($ClubMemberInfoArr);
					for ($i=0; $i < $numOfMember; $i++)
					{
						$thisStudentID = $ClubMemberInfoArr[$i]['StudentID'];
						$thisStudentName = $ClubMemberInfoArr[$i]['StudentName'];
						$thisClassName = $ClubMemberInfoArr[$i]['ClassName'];
						$thisClassNumber = $ClubMemberInfoArr[$i]['ClassNumber'];
						$thisWebSAMSRegNo = $ClubMemberInfoArr[$i]['WebSAMSRegNo'];
						$thisUserId = $ClubMemberInfoArr[$i]['UserID'];
						$thisStudentUserLogin = $ClubMemberInfoArr[$i]['UserLogin'];
						
						### Show user record only in view mode
						if ($PageAction == "view" && !in_array($thisStudentID, $DisplayStudentIDArr))
							continue;
							
						### Get Student Object
						$libuserObj->LoadUserData($thisStudentID);
						
						### Student Name is a Link to update Student Performance & Comment for Enrollment Admin, Master and Club PIC
						$thisStudentNameDisplay = '';
						$thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isClubPIC)? '<a class="tablelink" href="group_student_comment.php?AcademicYearID='.$AcademicYearID.'&StudentID='.$thisStudentID.'&EnrolGroupID='.$EnrolGroupID.'">' : '';
						$thisStudentNameDisplay .= $thisStudentName;
						$thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isClubPIC)? '</a>' : '';
		
						### Load Student Photos
// 						list($thisPhotoFilePath, $thisPhotoURL) = $libuserObj->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
						list($thisPhotoFilePath, $thisPhotoURL) = $libuserObj->GET_USER_PHOTO($thisStudentUserLogin);
						
						if (file_exists($thisPhotoFilePath))
						{
							if($DisplayPhoto==1)
							{
								$thisPhotoIconSpan="";
															
								list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
								$thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
								$thisPhotoSpan = "<div id=\"photo_". $thisStudentID ."\" style=\"display:inline\">";
								$thisPhotoSpan .= $thisImgTag;
								$thisPhotoSpan .= "</div>\n";
							}
							else
							{
								$thisPhotoIconSpan = "<div id=\"photo_". $thisStudentID ."_icon\" style=\"display:inline\">";
								$thisPhotoIconSpan .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\">";
								$thisPhotoIconSpan .= "</div>\n";
	
								$thisPhotoSpan = "";
							}
						}
						else
						{
							$thisPhotoIconSpan = "";
							$thisPhotoSpan = "";
						}
						
						$thisClass = 'tablegreenrow';
						$thisClass .= ($i % 2) + 1;
						$x .= '<tr class="'.$thisClass.'">'."\n";
							$x .= '<td align="center" class="tabletext">'.$thisClassName.'</td>'."\n";
							$x .= '<td align="center" class="tabletext">'.$thisClassNumber.'</td>'."\n";
							$x .= '<td align="left" nowrap style="border-right-width:5px">'."\n";
								$x .= '<span class="tabletext">'.$thisPhotoIconSpan. " ". $thisStudentNameDisplay.'</span>'."\n";
								$x .= '<br />'."\n";
								$x .= $thisPhotoSpan."\n";
							$x .= '</td>'."\n";
							if ($libenroll->enableUserJoinDateRange()) {
								if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"])
									&& !empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"])) {
									$AvailDateFrom = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"]));
								} else {
									$AvailDateFrom = "-";
								}
								if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"])
									&& !empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"])) {
									$AvailDateTo = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"]));
								} else {
									$AvailDateTo = "-";
								}
								if ($CanEdit) {
									$x .= '<td align="center">'. $AvailDateFrom  .'</td>'."\n";
									$x .= '<td align="center" nowrap>'. $AvailDateTo . '</td>'."\n";
								}
							}
							
							for ($j=0; $j < $numOfMeetingDate; $j++)
							{
								$thisGroupDateID = $GroupDateIDArr[$j];
								$thisActivityDateStart = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateStart'];
								$thisAttendanceStatus = $MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID][$thisGroupDateID]['RecordStatus'];
								$thisAttendanceRemark = $MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID][$thisGroupDateID]['Remark'];

								if ($libenroll->enableUserJoinDateRange()) {
									$isInAvailRange = false;
									$checkDate = strtotime(date("Y-m-d", strtotime($thisActivityDateStart)));
									$checkDateStart = strtotime($AvailDateFrom);
									$checkDateEnd = strtotime($AvailDateTo);
									if (($checkDate >= $checkDateStart || $AvailDateFrom == "-") && ($checkDate <= $checkDateEnd || $AvailDateTo == "-")) {
										$isInAvailRange = true;
									}
								}
								# Default Attendance Customization
								if($libenroll->enableClubDefaultAttendaceSelectionControl()){	
									if($thisAttendanceStatus=='0' || $thisAttendanceStatus==''){		
									 	$thisAttendanceStatus = $libenroll->defaultAttendanceStatus;
									}						
								}
								
								# $sys_custom['eEnrolment']['enableTakeAttendanceFutureDate']
								if (!$libenroll->enableTakeAttendanceFutureDate() && $libenroll->Is_Future_Date($thisActivityDateStart) == true)
								{
									$thisDisplay = $Lang['General']['EmptySymbol'];
								}
								else
								{
									if ($PageAction == 'view') {
										$thisDisplay = $this->Get_Attendance_Icon($thisAttendanceStatus);
									}
									else {
										// 										$thisID = $thisGroupDateID."[".$thisStudentID."]";
										$thisID = "attendanceArr[".$thisGroupDateID."]"."[".$thisStudentID."]";
										$thisIDRemark = "attendanceRemarkArr[".$thisGroupDateID."]"."[".$thisStudentID."]";
										// 										$thisName = $thisGroupDateID."[".$thisStudentID."]";
										$thisName = "attendanceArr[".$thisGroupDateID."]"."[".$thisStudentID."]";
										$thisNameRemark = "attendanceRemarkArr[".$thisGroupDateID."]"."[".$thisStudentID."]";
										$thisClass = "Col".$j;
										if ($libenroll->enableUserJoinDateRange()) {
											// $thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
											switch ($thisAttendanceStatus) {
												case ENROL_ATTENDANCE_PRESENT:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Present'];
													break;
												case ENROL_ATTENDANCE_EXEMPT:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Exempt'];
													break;
												case ENROL_ATTENDANCE_ABSENT:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Absent'];
													break;
												case ENROL_ATTENDANCE_LATE:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Late'];
													break;
												case ENROL_ATTENDANCE_EARLY_LEAVE:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['EarlyLeave'];
													break;
												default:
													$thisDisplay = "-";
													break;
											}
											
										} else {
											$thisDisabled = ($disabledArr[$thisGroupDateID] == "DISABLED")? true : false;
											
											if ($libenroll->enableTakeAttendanceFutureDate() && ($isEnrolAdmin == 1 || $isClubPIC == 1 || $isEnrolMaster == 1)) {
												$thisDisabled = false;
											}
											
											if($libenroll->disabledPicTakeAttendanceIfAdminTaken()){
											    $LastModifiedInfoArr = $libenroll->Get_Student_Attendance_Last_Modified_Info($EnrolGroupID, $thisStudentID, $thisGroupDateID, 'Club');
												$LastModifiedUserRole = $LastModifiedInfoArr[0]['LastModifiedRole'];
											 
												# is the last modified user id equal to admin
												if( $isClubPIC == 1 ){
													if( !empty($thisAttendanceStatus) && $LastModifiedUserRole == 'A' ){
														$thisDisabled = 1; // disabled
													}
												}
												# add one more logic checking $isClubPIC and $isClubAdmin Enable change the Attendance
												if( $isClubPIC == 1 && $isEnrolAdmin == 1 ){
													if( !empty($thisAttendanceStatus) && $LastModifiedUserRole == 'A' ){
														$thisDisabled = 0; // enabled
													}
												}
											}
											$thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
                        					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
                        					    $thisDisplay .= '<br /><input id="'.$thisIDRemark.'" name="'.$thisNameRemark.'" class="Col'.$j.'" value="'.$thisAttendanceRemark.'" placeholder="'.$Lang['General']['Remark'].'">';
                        					}
										}
									}
								}
								
								
								if ($libenroll->enableUserJoinDateRange()) {
									if ($isInAvailRange) {
										$x .= '<td align="center" class="tabletext">'.$thisDisplay.'</td>'."\n";
									} else {
										if ($thisDisplay != "-") {
											$x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;"><span class="tabletextrequire">x</span> ('. $thisDisplay.')</td>'."\n";
										} else {
											$x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>'."\n";
										}
									}
								} else {
									$x .= '<td align="center" class="tabletext">'.$thisDisplay.'</td>'."\n";
								}
// 								
							}
							if (isset($MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID]['AttendancePercentageRounded'])) {
								$x .= '<td align="center" class="tabletext">'. $MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID]['AttendancePercentageRounded'].'%</td>'."\n";
							} else {
								$x .= '<td align="center" class="tabletext">0.00%</td>'."\n";
							}
							if (isset($MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID]['Hours'])) {
								$x .= '<td align="center">'. $MemberAttendanceInfoArr[$thisStudentID][$EnrolGroupID]['Hours'] .'</td>'."\n";
							} else {
								$x .= '<td align="center">0</td>'."\n";
							}
// 							if($sys_custom['eEnrolment']['showGuardianInfo']){
							if($_SESSION['UserType']!='2'){
								$x .= '<td align="center">';
								$x .= "<a id=\"$thisUserId\" \" onclick=\"retrieveGuardianInfo($thisUserId, event);\" onblur=\"closeLayer('ToolMenu2');\"  href=\"javascript: void(0)\">";
								$x .= "<img src=\"$image_path/icons_guardian_info.gif\" border=\"0\" alt=\"$button_select\">";
								$x .= '</a></td>'."\n";
							}
// 							}
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
				
				### Show Attendance Statistics
				if ($PageAction != "view")
				{
					$x .= '<tfoot>'."\n";
						$x .= '<tr class="tablegreenbottom">'."\n";
							$x .= '<td align="center" colspan="3"><span class="tabletext">'.$eEnrollment['act_attendence'].'</span></td>'."\n";
							if ($libenroll->enableUserJoinDateRange()) {
								$x .= '<td align="center" colspan="2">&nbsp;</td>';
							}
							for ($i=0; $i<$numOfMeetingDate; $i++)
							{
								$thisGroupDateID = $GroupDateIDArr[$i];
								$thisActivityDateStart = $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['ActivityDateStart'];
								
								# $sys_custom['eEnrolment']['EnableTakeAttendanceFutureDate']
								if($libenroll->enableTakeAttendanceFutureDate() && ($isEnrolAdmin == 1 || $isClubPIC == 1 || $isEnrolMaster == 1)){
									$thisDisplay =  $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['AttendancePercentageRounded'].'%';
								}else{
									if ($libenroll->Is_Future_Date($thisActivityDateStart) == true)
										$thisDisplay = $Lang['General']['EmptySymbol'];
									else
										$thisDisplay =  $ClubAttendanceInfoArr['MeetingDateArr'][$thisGroupDateID]['AttendancePercentageRounded'].'%';
								}
								$x .= '<td align="center"><span class="tabletext">'.$thisDisplay.'</span></td>'."\n";
							}
							
							$thisAverageAttendance = $ClubAttendanceInfoArr['AttendancePercentageRounded'].'%';
							$x .= '<td align="center"><span class="tabletextrequire">*</span>'.$thisAverageAttendance.'</td>'."\n";
							
							$x .= '<td align="center" >&nbsp;</td>'."\n";
							
							// if($sys_custom['eEnrolment']['showGuardianInfo']){
							if($_SESSION['UserType']!='2'){
								$x .= '<td align="center" >&nbsp;</td>'."\n";
							}
							// }
						$x .= '</tr>'."\n";
						$x .= '<tr >';
						$x .= '<td align="center" colspan="3" style="border-right-width:5px">'.$eEnrollment['TotalStudent'].' &nbsp;</td>'."\n";
						
						$result = array();
						for ($i=0; $i<$numOfMeetingDate; $i++)
						{
						    $count = 0;
						    $thisGroupDateID = $GroupDateIDArr[$i];
						    for ($k=0; $k<$numOfMember; $k++)
						    {
						        $thisStudentID = $ClubMemberInfoArr[$k]['StudentID'];
						        $result[] = $libenroll->GetClubStudentStatusNumber($EnrolGroupID, $thisGroupDateID, $thisStudentID);
						        if($result[$k][0]['RecordStatus'] != 3 && $result[$k][0]['RecordStatus'] != ''){
						            $count++;
						        }
						    }
						    $x .= '<td align="center" colspan="1">'.$count.' / '.$numOfMember.'</td>';
						    unset($result);
						}
						$x .= '</tr>'."\n";
					$x .= '</tfoot>'."\n";
				}			
			$x .= '</table>'."\n";
			
			
			
			### Footer Remarks
			if ($PageAction == 'view')
			{
//				$x .= '<div>'."\n";
//					$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
//					$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//					$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
//					$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//					$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
//					if ($this->enableAttendanceLateStatusRight()) {
//						$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//						$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_LATE).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Late'].'</span>'."\n";
//					}
//				$x .= '</div>'."\n";
				$x .= $this->Get_Attendance_Icon_Remarks();
			}
			else
			{
				# Last Modified Remarks
				$LastModifiedInfoArr = $libenroll->Get_Attendance_Last_Modified_Info($EnrolGroupID, 'Club');
				$LastModifiedDate = $LastModifiedInfoArr[0]['LastDateModified'];
				$LastModifiedUserID = $LastModifiedInfoArr[0]['LastModifiedBy'];
				if (count($LastModifiedInfoArr) > 0)
					$x .= '<div class="tabletextremark">'.Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedUserID).'</div>';
				$x .= '<br style="clear:both;" />'."\n";
				
				# Attendance Overall Remarks
				$x .= '<div class="tabletextremark"><span class="tabletextrequire">*</span> '.$eEnrollment['act_most_update_avg_attendance'].'</span></div>'."\n";
			}
			
			# Deleted Student Remarks
			$x .= $this->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
		}
		
		//debug_pr($thisDisabled);
		
		### Buttons
		$x .= '<div class="edit_bottom_v30">';
		
			if ($PageAction == 'view')
			{
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "self.location='".$PATH_WRT_ROOT."home/eService/enrollment/'");
			}
			else
			{ 
				$thisDisabled = ($numOfMember==0)? true : false;
				if($libenroll->AllowToEditPreviousYearData) {
					if (!$libenroll->enableUserJoinDateRange()) {
						$x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:this.form.submit();", '', '', $thisDisabled);
						$x .= '&nbsp;'."\n";
						$x .= $this->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", '', '', '', $thisDisabled);
					}
					$x .= '&nbsp;'."\n";
				}
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:js_Cancel();");
			}
		$x .= '</div>';
		
		$x .= '</div>';
		
		### Hidden Fields for updating Attendance
		$x .= '<input type="hidden" name="StaffRole" value="'.$StaffRole.'" />'."\n";
		$x .= '<input type="hidden" name="disabledArr" value="'.rawurlencode(serialize($disabledArr)).'" />'."\n";
		
		return $x;
	}
	
	function Get_Management_Activity_Attendance_UI($EnrolEventID, $CanEdit, $PageAction, $AcademicYearID="")
	{
		global $eEnrollment, $Lang, $i_Notice_ViewStat, $image_path, $LAYOUT_SKIN;
		global $libenroll;
		
		### Get Club Info
		$EventInfo = $libenroll->GET_EVENTINFO($EnrolEventID);
		$ActivityEnrolGroupID = $EventInfo['EnrolGroupID'];
		$IsActivityClubPIC = $libenroll->IS_CLUB_PIC($ActivityEnrolGroupID);
		//if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
		if ($AcademicYearID == '') {
			$activityInfoAssoAry = $libenroll->Get_Activity_Info_By_Id($EnrolEventID);
			$AcademicYearID = $activityInfoAssoAry[$EnrolEventID]['AcademicYearID'];
		}
		
		### Get Club Meeting Date
		$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID);
		$numOfDate = count($DateArr);

		### Get Club Attendance Records
		$ActivityAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($EnrolEventID, '', $AcademicYearID);
		$ActivityAttendanceInfoArr = $ActivityAttendanceInfoArr[$EnrolEventID];
		
		
		$x = '';
		
		### Toolbar
		$type = 3;
		$export_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type";
		$type = "eventAttendance";
		$import_parameters = "AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&type=$type";
		
		// hide tools bar if no attendance record
		if ($numOfDate > 0 && $PageAction != "view")
		{
			$x .= '<div style="width:100%;">'."\n";
				if($libenroll->AllowToEditPreviousYearData) {
					$x .= $this->GET_LNK_IMPORT(($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "import.php?$import_parameters", '', '', '', '', '', 0);
					$x .= toolBarSpacer();
				}
				$x .= $this->GET_LNK_EXPORT("export.php?$export_parameters", '', '', '', '', '', 0);
				$x .= toolBarSpacer();
				$x .= $this->GET_LNK_PRINT("javascript:newWindow('print.php?AcademicYearID=$AcademicYearID&type=activityAttendance&EnrolEventID=$EnrolEventID',24);", $Lang['Btn']['Print'], '', '', '', '', 0);
				$x .= toolBarSpacer();
				
				# Designer is busy for other that haven't provide the new IP25 standard coding, use old style first
				$x .= "<div style='float: left; padding-top: 2px; padding-bottom: 5px; margin-left: 3px; background: url(".$image_path."/".$LAYOUT_SKIN."/eOffice/icon_viewstat.gif); background-repeat: no-repeat; background-position: left center;'>";
                    $x .= "<a href='javascript:newWindow(\"view_stat.php?EnrolEventID=$EnrolEventID\", 8);' class='contenttool' style='font-family:\"Century Gothic\", \"Microsoft JhengHei\", Verdana, Arial, Helvetica, sans-serif, 新細明體, 細明體_HKSCS, mingliu; padding-left:20px; line-height: 20px;'>";
                        //$x .= "<img src='".$image_path."/".$LAYOUT_SKIN ."/eOffice/icon_viewstat.gif' border='0' align='absmiddle'>";
                        $x .= $i_Notice_ViewStat;
                    $x .= "</a>";
                $x .= "</div>";

            $x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
			//$x .= '<br style="clear:both;" />'."\n";
		}
		
		/* 2011-06-03 YatWoon - Hidden chart - case #2011-0526-1008-21067 
		### Show Attendance Chart if the User can edit the Attendance
		if ($CanEdit && $PageAction != "view")
		{
			$x .= '<div style="width:100%;">'."\n";
				$x .= $this->Get_Management_Activity_Attendance_Chart($EnrolEventID, $ActivityAttendanceInfoArr);
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
		}
		*/
		### Show Attendance Info Table
		
		$x .= $this->Get_Management_Activity_Attendance_Info_Table($EnrolEventID, $CanEdit, $PageAction, $ActivityAttendanceInfoArr, 0, $AcademicYearID);
		
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Management_Activity_Attendance_Chart($EnrolEventID, $ActivityAttendanceInfoArr,$returnDataOnly= false)
	{
		global $eEnrollment, $eEnrollmentMenu, $libenroll;
		
		### Preparing Chart Data
		$ChartDataArr = array();
		foreach ((array)$ActivityAttendanceInfoArr['MeetingDateArr'] as $thisEventDateID => $thisDateAttendanceArr)
		{
			$thisActivityDateStart = $thisDateAttendanceArr['ActivityDateStart'];
			$thisActivityDateEnd = $thisDateAttendanceArr['ActivityDateEnd'];
			
			# Display Title
			$thisDateTitle = date("Y-m-d H:i", strtotime($thisActivityDateStart)).'-'.date("H:i", strtotime($thisActivityDateEnd));
			
			# Count Total Attendance of Student
			$thisAttendance = 0;
			foreach ((array)$thisDateAttendanceArr['AttendanceDataArr'] as $thisRecordStatus => $thisAttendanceArr)
			{
				if (
					$thisRecordStatus == ENROL_ATTENDANCE_PRESENT
					|| ($thisRecordStatus == ENROL_ATTENDANCE_LATE && $libenroll->enableAttendanceLateStatusRight())
					|| ($thisRecordStatus == ENROL_ATTENDANCE_EARLY_LEAVE && $libenroll->enableAttendanceEarlyLeaveStatusRight())
				){
					$thisAttendance += $thisAttendanceArr['AttendanceCount'];
				}
			}
				
			$ChartDataArr[] = array($thisDateTitle, $thisAttendance);
		}
		if($returnDataOnly == true){
			return $ChartDataArr;
		}
		else{
			$ConfArr['IsHorizontal'] = 1;
			$ConfArr['ChartX'] = $eEnrollmentMenu['act_date_time'];
			$ConfArr['ChartY'] = $eEnrollmentMenu['head_count'];
			$ConfArr['MaxWidth'] = 500;		
			
			$x = '';
	
			$x .= $this->GEN_CHART($ConfArr, $ChartDataArr);
			
			return $x;
		}
	}
	function Get_Management_Activity_Attendance_Info_Table($EnrolEventID, $CanEdit, $PageAction, $ActivityAttendanceInfoArr=array(), $DisplayPhoto=0, $AcademicYearID="")
	{		
		
		global $eEnrollment, $Lang, $image_path, $LAYOUT_SKIN, $libenroll, $PATH_WRT_ROOT, $DisplayAllPhoto, $HiddenAllPhoto, $sys_custom;
		
		//debug_pr($AcademicYearID);
		//debug_pr($EnrolEventID);
		
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		
		$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
		$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
		$isActivityPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Activity");
		$isAcitivtyHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolEventID, "Activity");
		if ($AcademicYearID == '') {
			$activityInfoAssoAry = $libenroll->Get_Activity_Info_By_Id($EnrolEventID);
			$AcademicYearID = $activityInfoAssoAry[$EnrolEventID]['AcademicYearID'];
		}
		
		if(empty($ActivityAttendanceInfoArr)) {
			### Get Activity Attendance Records
			//echo $AcademicYearID.';';
			$ActivityAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($EnrolEventID, "", $AcademicYearID);
			//debug_pr($ActivityAttendanceInfoArr);
			$ActivityAttendanceInfoArr = $ActivityAttendanceInfoArr[$EnrolEventID];
		}
		
		### Get Activity Meeting Dates
		$EventDateIDArr = array_keys((array)$ActivityAttendanceInfoArr['MeetingDateArr']);
		
		$numOfMeetingDate = count($EventDateIDArr);
		
// 		debug_pr($numOfMeetingDate);
		
		### Special checking for HELPER - disable the attendance updating if the 
		$disabledArr = array();
		if ($isAcitivtyHelper)
		{
			$StaffRole = "H";
			
			# Control Attendance Helper's Right
			if($libenroll->Event_DisableHelperModificationRightLimitation !=true){
				// check for each Date
				for ($i=0; $i<$numOfMeetingDate; $i++)
				{
					$thisEventDateID = $EventDateIDArr[$i];
					
					// check if there are any attendance records in DB
					$sql = "SELECT DateModified, LastModifiedRole FROM INTRANET_ENROL_EVENT_ATTENDANCE WHERE EnrolEventID = '$EnrolEventID' AND EventDateID = '$thisEventDateID'";
					$result = $libenroll->returnArray($sql,2);
					
					# no attendance record yet => enable checkboxes for edit						
					# otherwise, do checking
					if (count($result) > 0)
					{
						if ($result[0]['LastModifiedRole'] == "A")
						{
							$disabledArr[$thisEventDateID] = "DISABLED";	
						}
						else
						{
							# disable checkboxes if last modified date is not today
							if (date("Y-m-d") != date("Y-m-d", strtotime($result[0]['DateModified'])))
							{
								$disabledArr[$thisEventDateID] = "DISABLED";	
							}
						} 
					}
				}
			}
		}
		else
		{
			$StaffRole = "A";
		}
		
		
		### Display the Student / Children only if it is in view mode
		$DisplayStudentIDArr = array();
		if ($PageAction == "view")
		{
			if ($_SESSION['UserType'] == USERTYPE_STUDENT)
			{
				$DisplayStudentIDArr[] = $_SESSION['UserID'];
			}
			else if ($_SESSION['UserType'] == USERTYPE_PARENT)
			{
				$libuser = new libuser($_SESSION['UserID']);
				$ChildArr = $libuser->getChildrenList();
				$DisplayStudentIDArr = Get_Array_By_Key($ChildArr, 'StudentID');
			}
		}
		
		
		### Get Club Members
		$ActivityMemberInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventID, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ActiveMemberOnly=0, $FormIDArr='');
		$ActivityMemberInfoArr = (array)$ActivityMemberInfoArr[$EnrolEventID]['StatusStudentArr'][2];
		
		$StudentIDArr = Get_Array_By_Key($ActivityMemberInfoArr, 'StudentID');
		
		if ($libenroll->enableUserJoinDateRange()) {
			$enrolAvailDateArr = $libenroll->Get_Enrol_AvailDate($EnrolEventID, $StudentIDArr, "Activity");
		}
		
		$numOfMember = count($StudentIDArr);
		
		$x = '
			<div id="div_Activity_AttendanceTable">
			<script language="javascript">
			function toggle_photo()
			{ 
				Reload_Activity_Attendance_Info_Table2("'.$EnrolEventID.'","'.$CanEdit.'","'.$PageAction.'", "'. ($DisplayPhoto==1 ? "0" : "1") .'");
			}
			</script>
		';

		### Get Member Attendance Records
		$MemberAttendanceInfoArr = $libenroll->Get_Student_Activity_Attendance_Info($StudentIDArr, array($EnrolEventID));
		### Navigation
		if ($CanEdit && $PageAction != 'view') {
			$x .= '<br />';
			$x .= $this->GET_NAVIGATION2($eEnrollment['take_attendance']);
		}
			
		if ($numOfMember==0) {
			$x .= '<br />'."\n";
			$x .= $Lang['eEnrolment']['Warning']['ActivityHasNoParticipant'];
			$x .= '<br />'."\n";
			$x .= '<br />'."\n";
		}
		else {
			$DisplayPhotoBtn .= "<a href=\"javascript:toggle_photo();\" class=\"tabletool\"><span id=\"DisplayAllPhotoTxt\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo_". ($DisplayPhoto? "zoomout" : "zoomin") .".gif\" border=\"0\" align=\"absmiddle\" alt=\"$DisplayAllPhoto\"></span></a>";

			$x .= '<table id="AttendanceTable">'."\n";
				### Table Header
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						# UserLogin
    					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    						$x .= '<th align="center" class="tableTitle tabletopnolink" width="50">'.$Lang['General']['UserLogin'].'</th>'."\n";
    					}
    					
						# Class
						$x .= '<th align="center" class="tableTitle tabletopnolink" width="50">'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>'."\n";
						
						# Class No.
						$x .= '<th align="center" class="tableTitle tabletopnolink" width="50">'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\n";
						
						# Student Name
						$x .= '<th class="tableTitle" nowrap style="border-right-width:5px"><span class="tabletopnolink">'.$eEnrollment['student_name'].'</span> '. $DisplayPhotoBtn .'</th>'."\n";
						
						if ($libenroll->enableUserJoinDateRange() && $CanEdit) {
							$x .= '<th class="tableTitle" colspan="2" align="center"><span class="tabletopnolink">'. $Lang['eEnrolment']['AvailiableDate'] .'</span></th>'."\n";
						}
						# Meeting Dates
						for ($i=0; $i<$numOfMeetingDate; $i++)
						{
							$thisEventDateID = $EventDateIDArr[$i];
							$thisActivityDateStart = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisEventDateID]['ActivityDateStart'];
							$thisActivityDateEnd = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisEventDateID]['ActivityDateEnd'];
							$thisDateTimeDisplay = date("Y-m-d", strtotime($thisActivityDateStart)).'<br />'.date("H:i", strtotime($thisActivityDateStart)).'-'.date("H:i", strtotime($thisActivityDateEnd));
							
							# disable if the date is in the future
							if (date("Y-m-d") < date("Y-m-d", strtotime($thisActivityDateStart)))
								$disabledArr[$thisEventDateID] = "DISABLED";	
							
							# disable if the user is a normal student only or is in view mode
							if (!$CanEdit || $PageAction == "view")
								$disabledArr[$thisEventDateID] = "DISABLED";
								
							
							$x .= '<th class="tableTitle tabletopnolink" align="center" nowrap>'."\n";
								$x .= '<span>'.$thisDateTimeDisplay.'</span>'."\n";
								if ($PageAction != "view")
								{
									if (!$libenroll->enableUserJoinDateRange()) {
										### Apply All Selection
										$thisSelection = $this->Get_Student_Attendance_Selection("Header_Col".$i, "Header_Col".$i, $thisAttendanceStatus='', "Header_Col".$i);
                    					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
                    					    $thisSelection .= '<br /><input class="Header_Col'.$i.'" placeholder="'.$Lang['General']['Remark'].'">';
                    					}
										$thisApplyAllIcon = $this->Get_Apply_All_Icon("javascript:CheckAll('".$i."');");
									}
									//--- Henry added 20131101 [start]
									$thisSendMailLink = "";
									$thisSendMailLink .= '<div style="float:left;width:30%">';
									$thisSendMailLink .= '<a id="sendMailThickboxLink" href="javascript:load_dyn_size_thickbox_ip(\''.$thisActivityDateStart.' '.$Lang['eEnrolment']['AbsentNotification'].'\', \'onloadThickBox('.$thisEventDateID.','.$EnrolEventID.');\', inlineID=\'\', defaultHeight=600, defaultWidth=800);" class="contenttool">';								
									$thisSendMailLink .= "<img src=\"".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/iMail/icon_compose.gif\" border=\"0\" align=\"absmiddle\"> ";	
									$thisSendMailLink .= "</a>";
									$thisSendMailLink .= '</div>';
									//--- Henry added 20131101 [end]
									$x .= '<br />'."\n";
									$x .= '<div style="width:100%;text-align:center;">'.$thisSelection.'</div>'."\n";

									if ($libenroll->enableAttendanceScheduleDetail() || $libenroll->enableUserJoinDateRange()) {
										$thisActivityAttendanceDetailIcon = "<a class='icon_batch_assign' href=\"javascript:Reload_Activity_Attendance_Schedule_Detail_Table('".$thisEventDateID."');\">" . $this->Get_View_Image() . "</a>";
										$x .= '<div style="width:12%;float:left;">&nbsp;</div>'.$thisActivityAttendanceDetailIcon.' &nbsp; '.$thisApplyAllIcon."\n";
									} else {
										$x .= '<div style="width:40%;float:left;">&nbsp;</div>'.$thisApplyAllIcon."\n";
									}
									
									//--- Henry added 20131101 [start]
									if($sys_custom['eEnrolment']['AttendanceSendEmailToAbsentStudent'] && !$libenroll->IS_EVENT_HELPER($EnrolEventID)){
										$x .= $thisSendMailLink."\n";
									}
									//--- Henry added 20131101 [end]
								}
							$x .= '</th>'."\n";
						}
						
						# Attendance %
						$x .= '<th class="tableTitle" nowrap align="center">'."\n";
							$x .= '<span class="tabletopnolink">'."\n";
								$x .= $eEnrollment['attendance_rate']."\n";
								$x .= '<br />'."\n";
								$x .= '('.$eEnrollment['up_to'].' '.date("Y-m-d").')'."\n";
							$x .= '</span>'."\n";
						$x .= '</th>'."\n";
						
						# Attended Hours
						$x .= '<th class="tableTitle" nowrap align="center">'."\n";
							$x .= '<span class="tabletopnolink">'."\n";
								$x .= $Lang['eEnrolment']['AttendanceHour']."\n";
								$x .= '<br />'."\n";
								$x .= '('.$eEnrollment['up_to'].' '.date("Y-m-d").')'."\n";
							$x .= '</span>'."\n";
						$x .= '</th>'."\n";
						
// 						if($sys_custom['eEnrolment']['showGuardianInfo']){
						# guardian info
						if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role'] == ""){
    						$x .= '<th class="tableTitle" nowrap align="center">'."\n";
    							$x .= '<span class="tabletopnolink">'."\n";
    								$x .= $Lang['StudentRegistry']['GuardianInfo'];
    							$x .= '</span>'."\n";
    						$x .= '</th>'."\n";
						}
// 						}
						
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
			
			
				### Table Content
				
				$libuserObj = new libuser("", "", $StudentIDArr);
				$x .= '<tbody>'."\n";
					for ($i=0; $i<$numOfMember; $i++)
					{
						$thisStudentID = $ActivityMemberInfoArr[$i]['StudentID'];
						$thisStudentName = $ActivityMemberInfoArr[$i]['StudentName'];
						$thisClassName = $ActivityMemberInfoArr[$i]['ClassName'];
						$thisClassNumber = $ActivityMemberInfoArr[$i]['ClassNumber'];
					//	$thisWebSAMSRegNo = $ActivityMemberInfoArr[$i]['WebSAMSRegNo'];
						$thisUserId = $ActivityMemberInfoArr[$i]['UserID'];
						$thisStudentUserLogin = $ActivityMemberInfoArr[$i]['UserLogin'];
						
						### Show user record only in view mode
						if ($PageAction == "view" && !in_array($thisStudentID, $DisplayStudentIDArr))
							continue;
							
						### Get Student Object
						$libuserObj->LoadUserData($thisStudentID);
						
						### Student Name is a Link to update Student Performance & Comment for Enrollment Admin, Master and Club PIC
						$thisStudentNameDisplay = '';
						$thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isActivityPIC)? '<a class="tablelink" href="event_student_comment.php?AcademicYearID='.$AcademicYearID.'&StudentID='.$thisStudentID.'&EnrolEventID='.$EnrolEventID.'">' : '';
						$thisStudentNameDisplay .= $thisStudentName;
						$thisStudentNameDisplay .= ($isEnrolAdmin || $isEnrolMaster || $isActivityPIC)? '</a>' : '';
		
						### Load Student Photos
					//	list($thisPhotoFilePath, $thisPhotoURL) = $libuserObj->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
						list($thisPhotoFilePath, $thisPhotoURL) = $libuserObj->GET_USER_PHOTO($thisStudentUserLogin);
						/*
						if (file_exists($thisPhotoFilePath))
						{
							list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
							$thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
							$thisPhotoSpan = "<div id=\"photo_". $thisStudentID ."\" style=\"display:inline\">";
								$thisPhotoSpan .= $thisImgTag;
							$thisPhotoSpan .= "</div>\n";
							
	//						$thisPhotoIconSpan = "<div id=\"photo_". $thisStudentID ."_icon\" class=\"tabletext\" style=\"display:inline\">";
	//							$thisPhotoIconSpan .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\">";
	//						$thisPhotoIconSpan .= "</div>\n";
	// 						
	// 						$thisPhotoSpan2 = "<div id=\"photo_". $thisStudentID ."_tmp\" style=\"display:none\">";
	// 							$thisPhotoSpan2 .= $thisImgTag;
	// 						$thisPhotoSpan2 .= "</div>\n";
						}
						else
						{
							$thisPhotoSpan = "";
							$thisPhotoIconSpan="";
	// 						$thisPhotoSpan2 = "";
						}
						*/
						
						if (file_exists($thisPhotoFilePath))
						{
							if($DisplayPhoto==1)
							{
								$thisPhotoIconSpan="";
															
								list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
								$thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
								$thisPhotoSpan = "<div id=\"photo_". $thisStudentID ."\" style=\"display:inline\">";
								$thisPhotoSpan .= $thisImgTag;
								$thisPhotoSpan .= "</div>\n";
							}
							else
							{
								$thisPhotoIconSpan = "<div id=\"photo_". $thisStudentID ."_icon\" style=\"display:inline\">";
								$thisPhotoIconSpan .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_photo.gif\">";
								$thisPhotoIconSpan .= "</div>\n";
	
								$thisPhotoSpan = "";
							}
						}
						else
						{
							$thisPhotoIconSpan = "";
							$thisPhotoSpan = "";
						}
						
						$thisClass = 'tablegreenrow';
						$thisClass .= ($i % 2) + 1;
						$x .= '<tr class="'.$thisClass.'">'."\n";
    						if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    							$x .= '<td align="center" class="tabletext">'.$thisStudentUserLogin.'</td>'."\n";
    						}
							$x .= '<td align="center" class="tabletext">'.$thisClassName.'</td>'."\n";
							$x .= '<td align="center" class="tabletext">'.$thisClassNumber.'</td>'."\n";
							$x .= '<td align="left" nowrap style="border-right-width:5px">'."\n";
								$x .= '<span class="tabletext">'.$thisPhotoIconSpan. " ".$thisStudentNameDisplay.'</span>'."\n";
								$x .= '<br />'."\n";
								$x .= $thisPhotoSpan."\n";
							$x .= '</td>'."\n";
							if ($libenroll->enableUserJoinDateRange()) {
								if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"])
										&& !empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"])) {
									$AvailDateFrom = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateStart"]));
								} else {
									$AvailDateFrom = "-";
								}
								if (isset($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"])
									&& !empty($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"])) {
									$AvailDateTo = date("Y-m-d", strtotime($enrolAvailDateArr[$thisStudentID]["EnrolAvailiableDateEnd"]));
								} else {
									$AvailDateTo = "-";
								}
								if ($CanEdit) {
									$x .= '<td align="center">'. $AvailDateFrom  .'</td>'."\n";
									$x .= '<td align="center" nowrap>'. $AvailDateTo . '</td>'."\n";
								}
							}
							for ($j=0; $j<$numOfMeetingDate; $j++)
							{
								$thisEventDateID = $EventDateIDArr[$j];
								$thisActivityDateStart = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisEventDateID]['ActivityDateStart'];
								$thisAttendanceStatus = $MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID][$thisEventDateID]['RecordStatus'];
								$thisAttendanceRemark = $MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID][$thisEventDateID]['Remark'];
								
								if ($libenroll->enableUserJoinDateRange()) {
									$isInAvailRange = false;
									$checkDate = strtotime(date("Y-m-d", strtotime($thisActivityDateStart)));
									$checkDateStart = strtotime($AvailDateFrom);
									$checkDateEnd = strtotime($AvailDateTo);
									if (($checkDate >= $checkDateStart || $AvailDateFrom == "-") && ($checkDate <= $checkDateEnd || $AvailDateTo == "-")) {
										$isInAvailRange = true;
									}
								}
								
								# Default Attendance Customization
								if($libenroll->enableActivityDefaultAttendaceSelectionControl()){
									if($thisAttendanceStatus=='0' || $thisAttendanceStatus==''){
										$thisAttendanceStatus = $libenroll->Event_DefaultAttendanceStatus;
									}
								}
								
								if ($libenroll->Is_Future_Date($thisActivityDateStart) == true)
								{
									$thisDisplay = $Lang['General']['EmptySymbol'];
								}
								else
								{
									if ($PageAction == 'view')
										$thisDisplay = $this->Get_Attendance_Icon($thisAttendanceStatus);
									else
									{
										$thisID = "attendanceArr[".$thisEventDateID."]"."[".$thisStudentID."]";
// 										$thisID = $thisEventDateID."[".$thisStudentID."]";
										$thisIDRemark = "attendanceRemarkArr[".$thisEventDateID."]"."[".$thisStudentID."]";
										$thisName = "attendanceArr[".$thisEventDateID."]"."[".$thisStudentID."]";
// 										$thisName = $thisEventDateID."[".$thisStudentID."]";
										$thisNameRemark = "attendanceRemarkArr[".$thisEventDateID."]"."[".$thisStudentID."]";
										$thisClass = "Col".$j;
										
										if ($libenroll->enableUserJoinDateRange()) {
											// $thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
											switch ($thisAttendanceStatus) {
												case ENROL_ATTENDANCE_PRESENT:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Present'];
													break;
												case ENROL_ATTENDANCE_EXEMPT:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Exempt'];
													break;
												case ENROL_ATTENDANCE_ABSENT:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Absent'];
													break;
												case ENROL_ATTENDANCE_LATE:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['Late'];
													break;
												case ENROL_ATTENDANCE_EARLY_LEAVE:
													$thisDisplay = $Lang['eEnrolment']['Attendance']['EarlyLeave'];
													break;
												default:
													$thisDisplay = "-";
													break;
											}
										}  else {
											$thisDisabled = ($disabledArr[$thisEventDateID] == "DISABLED")? true : false;
											$thisDisplay = $this->Get_Student_Attendance_Selection($thisID, $thisName, $thisAttendanceStatus, $thisClass, $thisDisabled);
                        					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
                        					    $thisDisplay .= '<br /><input id="'.$thisIDRemark.'" name="'.$thisNameRemark.'" class="Col'.$j.'" value="'.$thisAttendanceRemark.'" placeholder="'.$Lang['General']['Remark'].'">';
                        					}
										}
									}
								}
								if ($libenroll->enableUserJoinDateRange()) {
									if ($isInAvailRange) {
										$x .= '<td align="center" class="tabletext">'.$thisDisplay.'</td>'."\n";
									} else {
										if ($thisDisplay != "-") {
											$x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;"><span class="tabletextrequire">x</span> ('. $thisDisplay.')</td>'."\n";
										} else {
											$x .= '<td align="center" class="tabletext" style="background-color:#dfdfdf;">&nbsp;</td>'."\n";
										}
									}
								} else {
									$x .= '<td align="center" class="tabletext">';
									$x .= $thisDisplay;
									/*if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
									    $x .= '<textarea style="width: 200px;height: 50px;resize: none;">TODO: cust</textarea>';
									}*/
									$x .= '</td>'."\n";
								}
							}
							if (isset($MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID]['AttendancePercentageRounded'])) {
								$x .= '<td align="center" class="tabletext">'.$MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID]['AttendancePercentageRounded'].'%</td>'."\n";
							} else {
								$x .= '<td align="center" class="tabletext">0.00%</td>'."\n";
							}
							if (isset($MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID]['Hours'])) {
								$x .= '<td align="center">'. $MemberAttendanceInfoArr[$thisStudentID][$EnrolEventID]['Hours'] .'</td>'."\n";
							} else {
								$x .= '<td align="center">0</td>'."\n";
							}
// 							if($sys_custom['eEnrolment']['showGuardianInfo']){
    						if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role'] == ""){
    							$x .= '<td align="center">';
    							$x .= "<a \" onclick=\"retrieveGuardianInfo($thisUserId, event);\" onblur=\"closeLayer('ToolMenu2');\" href=\"javascript: void(0)\">";
    							$x .= "<img src=\"$image_path/icons_guardian_info.gif\" border=\"0\" alt=\"$button_select\">";
    							$x .= '</a></td>'."\n";
    						}
// 							}
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
				
				### Show Attendance Statistics
				if ($PageAction != "view")
				{
				    $colspan = 3;
				    if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    				    $colspan = 4;
				    }
					$x .= '<tfoot>'."\n";
						$x .= '<tr class="tablegreenbottom">'."\n";
							$x .= '<td align="center" colspan="'.$colspan.'" style="border-right-width:5px"><span class="tabletext">'.$eEnrollment['act_attendence'].'</span></td>'."\n";
							if ($libenroll->enableUserJoinDateRange()) {
								$x .= '<td align="center" colspan="2">&nbsp;</td>';
							}
							for ($i=0; $i<$numOfMeetingDate; $i++)
							{
								$thisEventDateID = $EventDateIDArr[$i];
								$thisActivityDateStart = $ActivityAttendanceInfoArr['MeetingDateArr'][$thisEventDateID]['ActivityDateStart'];
								
								if ($libenroll->Is_Future_Date($thisActivityDateStart) == true)
									$thisDisplay = $Lang['General']['EmptySymbol'];
								else
									$thisDisplay =  $ActivityAttendanceInfoArr['MeetingDateArr'][$thisEventDateID]['AttendancePercentageRounded'].'%';
									
								$x .= '<td align="center"><span class="tabletext">'.$thisDisplay.'</span></td>'."\n";
							}
							
							$thisAverageAttendance = $ActivityAttendanceInfoArr['AttendancePercentageRounded'].'%';
							$x .= '<td align="center"><span class="tabletextrequire">*</span>'.$thisAverageAttendance.'</td>'."\n";
							
							$x .= '<td align="center">&nbsp;</td>'."\n";
							// if($sys_custom['eEnrolment']['showGuardianInfo']){
							$x .= '<td align="center" >&nbsp;</td>'."\n";
							// }
						$x .= '</tr>'."\n";
						$x .= '<tr >';
						$x .= '<td align="center" colspan="3" style="border-right-width:5px">'.$eEnrollment['TotalStudent'].' &nbsp;</td>'."\n";
						
						$result = array();
						for ($i=0; $i<$numOfMeetingDate; $i++)
						{
						    $count = 0;
						    $thisEventDateID = $EventDateIDArr[$i];
						    for ($k=0; $k<$numOfMember; $k++)
						    {
						        $thisStudentID = $ActivityMemberInfoArr[$k]['StudentID'];
						        $result[] = $libenroll->GetActStudentStatus($EnrolEventID, $thisEventDateID, $thisStudentID);
						        if($result[$k][0]['RecordStatus'] != 3 && $result[$k][0]['RecordStatus'] != ''){
						            $count++;
						        }
						    }
						    $x .= '<td align="center" colspan="1">'.$count.' / '.$numOfMember.'</td>';
						    unset($result);
						}
						$x .= '</tr>'."\n";
					$x .= '</tfoot>'."\n";

				}			
			$x .= '</table>'."\n";
			
			### Footer Remarks
			if ($PageAction == 'view')
			{
//				$x .= '<div>'."\n";
//					$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
//					$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//					$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
//					$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
//					$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
//				$x .= '</div>'."\n";
				$x .= $this->Get_Attendance_Icon_Remarks();
			}
			else
			{
				# Last Modified Remarks
				//$LastModifiedInfoArr = $libenroll->Get_Attendance_Last_Modified_Info($EnrolEventID, 'Club');
				$LastModifiedInfoArr = $libenroll->Get_Attendance_Last_Modified_Info($EnrolEventID, 'Event');
				$LastModifiedDate = $LastModifiedInfoArr[0]['LastDateModified'];
				$LastModifiedUserID = $LastModifiedInfoArr[0]['LastModifiedBy'];
				if (count($LastModifiedInfoArr) > 0)
					$x .= '<div class="tabletextremark">'.Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedUserID).'</div>';
				$x .= '<br style="clear:both;" />'."\n";
				
				# Attendance Overall Remarks
				$x .= '<div class="tabletextremark"><span class="tabletextrequire">*</span> '.$eEnrollment['act_most_update_avg_attendance'].'</span></div>'."\n";
			}
			
			# Deleted Student Remarks
			$x .= $this->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
		}
		
		
		### Buttons
		$x .= '<div class="edit_bottom_v30">';
			if ($PageAction == 'view')
			{
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "self.location='".$PATH_WRT_ROOT."home/eService/enrollment/event_index.php?AcademicYearID=$AcademicYearID'");
			}
			else
			{
				$thisDisabled = ($numOfMember==0)? true : false;
				if($libenroll->AllowToEditPreviousYearData) {
					if (!$libenroll->enableUserJoinDateRange()) {
						$x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:this.form.submit();", '', '', $thisDisabled);
						$x .= '&nbsp;'."\n";
						$x .= $this->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", '', '', '', $thisDisabled);
					}
					$x .= '&nbsp;'."\n";
				}
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:js_Cancel();");
			}
		$x .= '</div>';
		$x .= '</div>';
		
		### Hidden Fields for updating Attendance
		$x .= '<input type="hidden" name="StaffRole" value="'.$StaffRole.'" />'."\n";
		$x .= '<input type="hidden" name="disabledArr" value="'.rawurlencode(serialize($disabledArr)).'" />'."\n";
		//debug_pr($x);
		return $x;
	}
	
	
	function Get_Attendance_Icon($Status, $BW=0)
	{
		global $Lang, $image_path, $LAYOUT_SKIN;
		
		if ($BW == 1)
			$IconSuffix = '_bw';
		
		if ($Status == ENROL_ATTENDANCE_PRESENT)
			$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_present'.$IconSuffix.'.gif" width="18" height="18" border="0">';
		else if ($Status == ENROL_ATTENDANCE_EXEMPT)
			$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_exempt.gif" width="18" height="18" border="0">';
		else if ($Status == ENROL_ATTENDANCE_ABSENT)
			$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_absent'.$IconSuffix.'.gif" width="18" height="18" border="0">';
		else if ($Status == ENROL_ATTENDANCE_LATE) {
			if ($BW) {
				$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/attendance/icon_late_bw.gif" width="15" height="15" border="0">';
			}
			else {
				$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/attendance/icon_late_s.png" width="20" height="20" border="0">';
			}
		}
		else if ($Status == ENROL_ATTENDANCE_EARLY_LEAVE) {
			if ($BW) {
				$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/attendance/icon_early_leave_bw.gif" width="15" height="15" border="0">';
			}
			else {
				$Icon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/attendance/icon_early_leave_s.png" width="20" height="20" border="0">';
			}
		}
		else
			$Icon = $Lang['General']['EmptySymbol'];
		
		return $Icon;
	}
	
	function Get_Attendance_Icon_Remarks($BW=0) {
		global $Lang, $libenroll;
		
		$x = '';
		$x .= '<div>'."\n";
			$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT, $BW).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
			$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
			$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT, $BW).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
			$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
			$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT, $BW).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
			if ($libenroll->enableAttendanceLateStatusRight()) {
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
				$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_LATE, $BW).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Late'].'</span>'."\n";
			}
			if ($libenroll->enableAttendanceEarlyLeaveStatusRight()) {
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
				$x .= $this->Get_Attendance_Icon(ENROL_ATTENDANCE_EARLY_LEAVE, $BW).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['EarlyLeave'].'</span>'."\n";
			}
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Category_Setting_UI($Type='', $CategoryID='',$CategoryTypeID=0)
	{
		global $Lang, $i_ClubsEnrollment_NoLimit, $libenroll, $eEnrollment, $enrolConfigAry;
		
		## Category Selection
		$CatSelection = $libenroll->GET_CATEGORY_SEL($CategoryID, $CategoryID, 'this.form.submit();', $Lang['General']['PleaseSelect'], 0, $CategoryTypeID );

		if($Type == "Event")
		{
			$UseCategorySetting = $libenroll->Event_UseCategorySetting;
	        $defaultMin = $libenroll->Event_defaultMin;
			$defaultMax = $libenroll->Event_defaultMax;
			$EnrollMax = $libenroll->Event_EnrollMax;
			
			$TypeLang = "act";
			$action = "category_setting_event.php";
			$recordType = $enrolConfigAry['Activity'];
			$Disabled = $libenroll->Has_Any_Student_Enrolled_Event();
			
			$min_numbers = array (0,1,2,3,4,5,6,7,8,9);
			$max_numbers = array (array(0,$i_ClubsEnrollment_NoLimit));
			for ($i=1; $i<10; $i++) {
			     $max_numbers[] = array($i,$i);
			}
			
			$minSelection = getSelectByValue($min_numbers,"name='defaultMin' class='selection' ",$defaultMin);
			$maxSelection = getSelectByArray($max_numbers,"name='defaultMax' class='selection' ",$defaultMax);
			$enrollMaxSelection = getSelectByArray($max_numbers,"name='EnrollMax' class='selection' ",$EnrollMax);
		}
		else
		{
			$UseCategorySetting = $libenroll->UseCategorySetting;
//	        $defaultMin = $libenroll->defaultMin;
//			$defaultMax = $libenroll->defaultMax;
//			$EnrollMax = $libenroll->EnrollMax;
			
			$TypeLang = "club";
			$action = "category_setting_club.php";
			$recordType = $enrolConfigAry['Club'];
			$Disabled = $libenroll->Has_Any_Student_Enrolled_Club();
			
			$quotaSettingsType = $libenroll->quotaSettingsType;
		}
		if($Disabled)
		{
			$Instruction = $this->GET_WARNING_TABLE($Lang['eEnrolment']['CategorySettingInstruction']);
		}		
		$UseCategorySetting==1?$isEnable=1:$isDisable=1;
		
		## Application Quota Table
		$htmlAry['quotaSettingsYearBaseRadio'] = $this->Get_Radio_Button('quotaSettingsType_YearBase', 'quotaSettingsType', 'YearBase', ($quotaSettingsType=='YearBase'), $Class="", $eEnrollment['YearBased'], "clickedQuotaSettingsType(this.value);", $isDisabled=0);
		$htmlAry['quotaSettingsTermBaseRadio'] = $this->Get_Radio_Button('quotaSettingsType_TermBase', 'quotaSettingsType', 'TermBase', ($quotaSettingsType=='TermBase'), $Class="", $eEnrollment['SemesterBased'], "clickedQuotaSettingsType(this.value);", $isDisabled=0);
		$htmlAry['applicationQuotaTable'] = $this->Get_Enrollment_Application_Quota_Settings_Table($recordType, $CategoryID);
		
		
		$x .= '<br><br>'."\n";
		$x .= '<form id="form1" name="form1" method="POST" action="'.$action.'">'."\n";
		$x .= $Instruction."\n";
		$x .= '<table cellpadding="0" cellspacing="0" border="0" class="form_table_v30"> '."\n";
			$x .= '<tbody id="StatusTable">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['eEnrolment']['UseCategorySetting'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Radio_Button("CategorySettingStatus_1", "CategorySettingStatus", 1, $isEnable, $Class='CategorySettingStatus', $Lang['eEnrolment']['Enable'], 'js_Toggle_Status()',$Disabled)."&nbsp;";
						$x .= $this->Get_Radio_Button("CategorySettingStatus_2", "CategorySettingStatus", 0, $isDisable, $Class='CategorySettingStatus', $Lang['eEnrolment']['Disable'], 'js_Toggle_Status()',$Disabled);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		if($UseCategorySetting)
		{
			$x .= '<table cellpadding="0" cellspacing="0" border="0" class="form_table_v30"> '."\n";
				$x .= '<tbody id="CategoryTable">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td class="field_title">'.$Lang['eEnrolment']['Category'].'</td>'."\n";
						$x .= '<td>'.$CatSelection.'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</tbody>'."\n";	
				if($CategoryID!='')
				{
					$x .= '<tbody id="SettingTable">'."\n";
						if($Type == "Event") {
							//Default minimum number of applications
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$eEnrollment['default_min_'.$TypeLang].'</td>'."\n";
								$x .= '<td>'.$minSelection.'</td>'."\n";
							$x .= '</tr>'."\n";
							//Default maximum number of applications
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$eEnrollment['default_max_'.$TypeLang].'</td>'."\n";
								$x .= '<td>'.$maxSelection.'</td>'."\n";
							$x .= '</tr>'."\n";
							//Default maximum number of enrolled activities
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$eEnrollment['default_enroll_max_'.$TypeLang].'</td>'."\n";
								$x .= '<td>'.$enrollMaxSelection.'</td>'."\n";
							$x .= '</tr>'."\n";
						}
						else {
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eEnrolment']['ApplicationQuota'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $htmlAry['quotaSettingsYearBaseRadio'].' '.$htmlAry['quotaSettingsTermBaseRadio']."\n";
									$x .= '<br />'."\n";
									$x .= $htmlAry['applicationQuotaTable']."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
					$x .= '</tbody>'."\n";
				}
			$x .= '</table>'."\n";
		}
			$SaveBtn = $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "checkform(document.form1)");
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SaveBtn;
			$x .= '</div>'."\n";	
				
		$x .= '</form>'."\n";

		return $x; 	
	}
	
	function Get_Student_Enrolment_Report($data, $print=0)
	{
		global $Lang, $i_general_name, $i_general_class, $i_ClassNumber, $i_UserGender, $intranet_session_language, $i_ClassLevel, $i_no_record_exists_msg;
		
		include_once("libclubsenrol.php");
		include_once("libclass.php");
		//include_once("libuser.php");
		include_once("form_class_manage.php");
		$libenroll = new libclubsenrol();
		$lc = new libclass();
		$fcm = new form_class_manage();
		
		$NoOfTarget = count($data['rankTargetDetail']);
		
		# UserID of students
		if($data['rankTarget']=="form") {
			$studentID = array();
			
			for($i=0; $i<$NoOfTarget; $i++) {
			
				$temp = $lc->storeStudent(0,$data['rankTargetDetail'][$i], Get_Current_Academic_Year_ID());
				
				if(count($temp)>0)
					$studentID = array_merge($studentID, $temp);
			}
			
		} else if($data['rankTarget']=="class") {
		
			$studentID = array();
			for($i=0; $i<$NoOfTarget; $i++) {
				$temp = $lc->storeStudent(0,"::".$data['rankTargetDetail'][$i], Get_Current_Academic_Year_ID());
				if(count($temp)>0)
					$studentID = array_merge($studentID, $temp);
			}
			
		} else if($data['rankTarget']=="student") {
		
			$studentID = $data['studentID'];
			
		}
		
		# grade
		$grade = $libenroll->Get_Performance_Grade();
		//debug_pr($grade);
		# Category Array
		$EventCatArr = $libenroll->GET_CATEGORY_LIST();
		$NoOfCat = count($EventCatArr);
		
		# activity detail & class info of students
		$activityDetail = array();
		if(count($studentID > 0)) {	
			$activityDetail = $libenroll->TKOGSS_Get_Activity_Details($studentID, $data['selectYear']);
			$clubDetail = $libenroll->TKOGSS_Get_Club_Details($studentID, $data['selectYear']);
			$classData = $fcm->Get_Student_Class_Info_In_AcademicYear($studentID, Get_Current_Academic_Year_ID());
		}
		$NoOfClassData = count($classData);
		
		$returnContent = "";
		# prepare class info (table header)
		for($i=0;$i<$NoOfClassData; $i++) {
			list($uid, $classEn, $classB5, $classNo, $yearClassID, $yearID, $nameB5, $nameEn, $gender, $dob, $strn, $hkid) = $classData[$i];
			$dob = substr($dob,0,10);
			if($intranet_session_language=="b5") $gender = ($gender=="M" ? $Lang['eEnrolment']['tkogss_male'] : $Lang['eEnrolment']['tkogss_female']);
			
			$classInfo[$uid] = array(
									"Name" => $nameB5." (".$nameEn.")",
									"Class" => ($intranet_session_language=="en"?$classEn:$classB5),
									"ClassNo" => $classNo,
									"STRN" => $strn,
									"HKID" => $hkid,
									"DOB" => substr($dob,8,2)."/".substr($dob,5,2)."/".substr($dob,0,4),
									"Gender" => $gender
								);
		}
		
		if($print!=1) $moreDelimiter = "　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊";
		
		$NoOfStudent = count($studentID);
		for($i=0; $i<$NoOfStudent;$i++) {
			$sid = $studentID[$i];
			//$stdInfo = new libuser($sid);
			//debug_pr($stdInfo);
			//$name = ($intranet_session_lang=="en") ? $stdInfo->EnglishName : $stdInfo->ChineseName;
			//$name = ($intranet_session_lang=="en") ? $nameEn : $nameB5;
			
			# student info
			$returnContent .= "<table width='100%'>
					<tr>
						<td colspan='4' align='center' height='40'><h4>".$data['title']."</h4></td>
					</tr>
				</table>
				<table width='85%'>
					<tr>
						<td><span>".$i_general_name." : <u>".$classInfo[$sid]['Name']."</u></span></td>
						<td><span>".$Lang['eEnrolment']['tkogss_regNo']." : <u>".$classInfo[$sid]['STRN']."</u></span></td>
						<td><span>".$i_general_class.": <u>".$classInfo[$sid]['Class']."</u></span></td>
						<td align='right'><span>".$i_ClassNumber." : <u>".$classInfo[$sid]['ClassNo']."</u></span></td>
					</tr>
				</table>
				<table width='85%'>
					<tr>
						<td><span>".$Lang['eEnrolment']['tkogss_hkid']." : <u>".$classInfo[$sid]['HKID']."</u></span></td>
						<td><span>".$Lang['eEnrolment']['tkogss_dob']." : <u>".$classInfo[$sid]['DOB']."</u></span></td>
						<td><span>".$i_UserGender.": <u>".$classInfo[$sid]['Gender']."</u></span></td>
						<td align='right'><span>".$Lang['eEnrolment']['tkogss_date']." : <u>".$data['signDate']."</u></span></td>
					</tr>
				</table>
				<table width='100%'><tr><td style='border-top:2px solid #000000;'>&nbsp;</td></tr></table>";
			
			# report content (loop for each category)
			for($a=0; $a<$NoOfCat; $a++) {
				$catId = $EventCatArr[$a]['CategoryID'];
				
				$totalPerformance = 0;
				$tempTable = "";
				
				# prepare activity content on each category - // Omas - case#Z42562
				//$NoOfRecord = sizeof($activityDetail[$sid][$catId]);

				# Group Same name & category's club & activities
				$mergeClubActivityAry = $libenroll->TKOGSS_Merge_Club_Activities_Details($clubDetail,$activityDetail,$sid,$catId);
				$NoOfRecord = count($mergeClubActivityAry);
				
				for($x=0; $x<$NoOfRecord; $x++) {
// Omas - case#Z42562				
//					$tempTable .= "
//						<tr>
//							<td>".$activityDetail[$sid][$catId][$x]['YearForm']."</td>
//							<td>".$activityDetail[$sid][$catId][$x]['Title']."</td>
//							<td>".$activityDetail[$sid][$catId][$x]['Role']."</td>
//							<td>&nbsp;</td>
//						</tr>
//					";
//					$totalPerformance += $activityDetail[$sid][$catId][$x]['Performance'];
					$tempTable .= "<tr>
							<td>".$mergeClubActivityAry[$x]['Year']."</td>
							<td>".$mergeClubActivityAry[$x]['Title']."</td>
							<td>".$mergeClubActivityAry[$x]['Role']."</td>
							<td>&nbsp;</td>
						</tr>";//$mergeClubActivityAry[$x]['Performance']
					$totalPerformance += $mergeClubActivityAry[$x]['Performance'];
				}
				if($NoOfRecord==0) {
					$tempTable = "<tr><td colspan='4'>".$i_no_record_exists_msg."</td></tr>";
				}
				
				# Calculate Performance
				$preformance = "-";
				if($NoOfRecord>0) {
					for($p=0; $p<count($grade[$catId]); $p++) {
						if($totalPerformance>=$grade[$catId][$p]['MinScore'] && $totalPerformance<=$grade[$catId][$p]['MaxScore']) {
							$preformance = $grade[$catId][$p]['GradeChar'];
							break;
						}
					}
				}else{// Omas - case #Z42562 
					for($p=0; $p<count($grade[$catId]); $p++) {
						if($grade[$catId][$p]['MinScore']==0) {
							$preformance = $grade[$catId][$p]['GradeChar'];
							break;
						}
					}
				}
				
				
				
				# generate table content
				$returnContent .= "<table width='100%'>
						<tr>
							<td colspan='100%'>".$EventCatArr[$a]['CategoryName']."</td>
						</tr>
						<tr>
							<td width='10%'><u>".$Lang['General']['SchoolYear']."</u></td>
							<td width='45%'><u>".$Lang['eEnrolment']['tkogss_Item']."</u></td>
							<td width='35%'><u>".$Lang['eEnrolment']['tkogss_details']."</u></td>
							<td width='10%'>".$Lang['eEnrolment']['tkogss_grade']." : <u>".$preformance."</u></td>
						</tr>
						".$tempTable."
						<tr>
							<td>&nbsp;</td>
							<td colspan=2 align='center'>＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊".$moreDelimiter."</td>
							<td>&nbsp;</td>
					</table>";
				
			}
			
			$returnContent .= "<br style='clear:both'>";
			$returnContent .= "<br style='clear:both'>";
			//echo $i.'/'.$NoOfStudent.'<br>';
			if($print==1 && $i<$NoOfStudent-1) {
					$returnContent .= "<p style='page-break-after:always'>&nbsp;</p>";
			}
			# student activity record
			
			
		}
		
		
		return $returnContent;
	}
	
	function Get_Student_Enrolment_Report_PerYear($data, $print=0)
	{
		global $Lang, $i_general_name, $i_general_class, $i_ClassNumber, $i_UserGender, $intranet_session_language, $i_ClassLevel, $i_no_record_exists_msg;
		
		include_once("libclubsenrol.php");
		include_once("libclass.php");
		//include_once("libuser.php");
		include_once("form_class_manage.php");
		$libenroll = new libclubsenrol();
		$lc = new libclass();
		$fcm = new form_class_manage();
		
		$NoOfTarget = count($data['rankTargetDetail']);
		
		# UserID of students
		if($data['rankTarget']=="form") {
			$studentID = array();
			
			for($i=0; $i<$NoOfTarget; $i++) {
			
				$temp = $lc->storeStudent(0,$data['rankTargetDetail'][$i], Get_Current_Academic_Year_ID());
				
				if(count($temp)>0)
					$studentID = array_merge($studentID, $temp);
			}
			
		} else if($data['rankTarget']=="class") {
		
			$studentID = array();
			for($i=0; $i<$NoOfTarget; $i++) {
				$temp = $lc->storeStudent(0,"::".$data['rankTargetDetail'][$i], Get_Current_Academic_Year_ID());
				if(count($temp)>0)
					$studentID = array_merge($studentID, $temp);
			}
			
		} else if($data['rankTarget']=="student") {
		
			$studentID = $data['studentID'];
			
		}
		
		# Category Array 
		$EventCatArr = $libenroll->GET_CATEGORY_LIST();
		$NoOfCat = count($EventCatArr);
		
		# Grade case#Z42562
		$grade = $libenroll->Get_Performance_Grade();
		
		# activity detail & class info of students
		$activityDetail = array();
		if(count($studentID > 0)) {	
			$activityDetail = $libenroll->TKOGSS_Get_Activity_Details_PerYear($studentID, $data['selectYear']);
			$clubDetail = $libenroll->TKOGSS_Get_Club_Details($studentID, $data['selectYear']);
			$classData = $fcm->Get_Student_Class_Info_In_AcademicYear($studentID, Get_Current_Academic_Year_ID());
		}
		$NoOfClassData = count($classData);
		
		$returnContent = "";
		# prepare class info (table header)
		for($i=0;$i<$NoOfClassData; $i++) {
			list($uid, $classEn, $classB5, $classNo, $yearClassID, $yearID, $nameB5, $nameEn, $gender, $dob, $strn, $hkid) = $classData[$i];
			$dob = substr($dob,0,10);
			if($intranet_session_language=="b5") $gender = ($gender=="M" ? $Lang['eEnrolment']['tkogss_male'] : $Lang['eEnrolment']['tkogss_female']);
			
			$classInfo[$uid] = array(
									"Name" => $nameB5." (".$nameEn.")",
									"Class" => ($intranet_session_language=="en"?$classEn:$classB5),
									"ClassNo" => $classNo,
									"STRN" => $strn,
									"HKID" => $hkid,
									"DOB" => substr($dob,8,2)."/".substr($dob,5,2)."/".substr($dob,0,4),
									"Gender" => $gender
								);
		}

		# prepare Footer table
		for($m=0;$m<50;$m++) $space .= "&nbsp;";
		$footerTable = "<table cellpadding='0' cellspacing='0' border='0' width='100%'>
							<tr>
								<td>".$Lang['eEnrolment']['tkogss_supplementary']."</td>
							</tr>
							<tr>
								<td>
									<table class='common_table_list_v30'>
										<tr>
											<td rowspan='2' align='center' width='30%'>".$Lang['eEnrolment']['tkogss_Item']."</td>
											<td rowspan='2' align='center' width='20%'>".$Lang['eEnrolment']['tkogss_role_award']."</td>
											<td colspan='3' align='center' width='15%'>".$Lang['eEnrolment']['tkogss_award']."</td>
											<td rowspan='2' align='center' width='15%'>".$Lang['eEnrolment']['tkogss_teacher']."</td>
											<td rowspan='2' align='center' width='20%'>".$Lang['eEnrolment']['tkogss_teacher_signature']."</td>
										</tr>
										<tr>
											<td align='center'>".$Lang['eEnrolment']['tkogss_eca']."</td>
											<td align='center'>".$Lang['eEnrolment']['tkogss_ser']."</td>
											<td align='center'>".$Lang['eEnrolment']['tkogss_cs']."</td>
										</tr>
										<tr>
											<td align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
											<td colspan='3' align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
										</tr>
										<tr>
											<td align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
											<td colspan='3' align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
										</tr>
										<tr>
											<td align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
											<td colspan='3' align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
											<td align='center'>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align='right' height='60' vlaign='bottom'>".$Lang['eEnrolment']['tkogss_student_signature']." : <u>".$space."</u></td>
							</tr>
						</table>
						<br style='clear:both'>
						
						";
		
		
		$NoOfStudent = count($studentID);
		for($i=0; $i<$NoOfStudent;$i++) {
			$sid = $studentID[$i];
			
			//$stdInfo = new libuser($sid);
			//debug_pr($stdInfo);
			//$name = ($intranet_session_lang=="en") ? $stdInfo->EnglishName : $stdInfo->ChineseName;
			//$name = ($intranet_session_lang=="en") ? $nameEn : $nameB5;
			
			# student info
			$returnContent .= "
				<table width='100%'>
					<tr>
						<td colspan='4' align='center' height='40'><h4>".$data['title']."</h4></td>
					</tr>
				</table>
				<table width='90%'>
					<tr>
						<td>".$i_general_name." : ".$classInfo[$sid]['Name']."</td>
						<td>".$Lang['eEnrolment']['tkogss_regNo']." : ".$classInfo[$sid]['STRN']."</td>
						<td>".$i_general_class.": ".$classInfo[$sid]['Class']."</u></td>
						<td align='right'>".$i_ClassNumber." : ".$classInfo[$sid]['ClassNo']."</td>
					</tr>
				</table>
				<table width='100%'><tr><td style='border-top:2px solid #000000;'>&nbsp;</td></tr></table>
				
			";
			
			# report content (loop for each category)
			for($a=0; $a<$NoOfCat; $a++) {
				$catId = $EventCatArr[$a]['CategoryID'];
				
				$totalPerformance = 0;
				$tempTable = "";
				
				# prepare activity content on each category 
				//$NoOfRecord = sizeof($activityDetail[$sid][$catId]);
				
				# Group Same name & category's club & activities
				$mergeClubActivityAry = $libenroll->TKOGSS_Merge_Club_Activities_Details($clubDetail,$activityDetail,$sid,$catId);
				$NoOfRecord = count($mergeClubActivityAry);
				
				$NoOfRecord = count($mergeClubActivityAry);
				for($x=0; $x<$NoOfRecord; $x++) {
				
// Omas - case#Z42562
//					$tempTable .= "
//						<tr>
//							<td>".$activityDetail[$sid][$catId][$x]['Year']."</td>
//							<td>".$activityDetail[$sid][$catId][$x]['Title']."</td>
//							<td>".$activityDetail[$sid][$catId][$x]['Role']."</td>
//							<td>".$activityDetail[$sid][$catId][$x]['Performance']."</td>
//						</tr>
//					";
//					$totalPerformance += $activityDetail[$sid][$catId][$x]['Performance'];
					$tempTable .= "
						<tr>
							<td>".$mergeClubActivityAry[$x]['Year']."</td>
							<td>".$mergeClubActivityAry[$x]['Title']."</td>
							<td>".$mergeClubActivityAry[$x]['Role']."</td>
							<td>".($mergeClubActivityAry[$x]['Performance']+0)."</td>
						</tr>
					";
					$totalPerformance += $mergeClubActivityAry[$x]['Performance'];
				}
				if($NoOfRecord==0) {
					$tempTable = "<tr><td colspan='4'>".$i_no_record_exists_msg."</td></tr>";
				}
				
				
				# Perfance total marks -> grade Omas case#Z42562
				$preformance = "-";
				if($NoOfRecord>0) {
					for($p=0; $p<count($grade[$catId]); $p++) {
						if($totalPerformance>=$grade[$catId][$p]['MinScore'] && $totalPerformance<=$grade[$catId][$p]['MaxScore']) {
							$preformance = $grade[$catId][$p]['GradeChar'];
							break;
						}
					}
				}else{// Omas - case #Z42562 
					for($p=0; $p<count($grade[$catId]); $p++) {
						if($grade[$catId][$p]['MinScore']==0) {
							$preformance = $grade[$catId][$p]['GradeChar'];
							break;
						}
					}
				}
				
				
				# generate table content
				$returnContent .= "
					<table width='100%'>
						<tr>
							<td colspan='100%'>".$EventCatArr[$a]['CategoryName']."</td>
						</tr>
						<tr>
							<td width='10%'><u>".$Lang['General']['SchoolYear']."</u></td>
							<td width='45%'><u>".$Lang['eEnrolment']['tkogss_Item']."</u></td>
							<td width='35%'><u>".$Lang['eEnrolment']['tkogss_details']."</u></td>
							<td width='10%'>".$preformance."</td>
						</tr>
						".$tempTable."
						<tr>
							<td colspan=4 align='center'>&nbsp;</td>
						</tr>
					</table>
				";
				
			}
			
			$returnContent .= "<br style='clear:both'>";
			$returnContent .= $footerTable;
			
			if($print==1 && $i<$NoOfStudent-1) {
					$returnContent .= "<p style='page-break-after:always'>&nbsp;</p>";
			}
			
		}
		
		return $returnContent;
	}
	
	function Display_Enrolment_Default_OLE_Setting($RecordType, $EnrolmentIdAry=array())
	{
		global $linterface, $eEnrollment, $ec_iPortfolio, $LibPortfolio, $i_alert_pleaseselect, $Lang, $enrolConfigAry, $iPort, $PATH_WRT_ROOT, $intranet_session_language;
		
		include_once("libportfolio.php");
		include_once("libclubsenrol.php");
		include_once("libpf-slp.php");
		include_once("libfilesystem.php");
		//include_once("libwordtemplates_ipf.php");
		include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
		
		$libenrol = new libclubsenrol();
		$LibPortfolio = new libpf_slp();
		$DefaultELEArray = $LibPortfolio->GET_ELE();
		//$LibWord = new libwordtemplates_ipf(1);
		//$file_array = $LibWord->file_array;
		//$file_array = array_merge(array(array("","-- ".$i_alert_pleaseselect." --","")), $file_array);
		
		// $RecordType = 'club' or 'activity'
		if($EnrolmentIdAry[0] != ''){
			$data = $libenrol->Get_Enrolment_Default_OLE_Setting($RecordType, $EnrolmentIdAry);
		}else{
			$data = array();
		}
		$returnStr = '
		<tr>
			<td><br>'.$linterface->GET_NAVIGATION2($Lang['eEnrolment']['DefaultOleSetting']).'<br/>
			</td>
		</tr>';
		
		
			$relatedEnrolId = $data[0]['RelatedEnrolID'];
			$catId = $data[0]['CategoryID'];
			$ole_component = $data[0]['OLE_Component'];
			$organization = $data[0]['Organization'];
			$details = $data[0]['Details'];
			$remark = $data[0]['SchoolRemark'];
			$titleLang = $data[0]['TitleLang'];
			$intExt = $data[0]['IntExt'];
		
			# OLE Component
			$ELEList = "";
			$tempELEAry = explode(',',$ole_component);
			foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
			{
				$checked = "";
				
				if (is_array($tempELEAry))
				{
					$checked = in_array($ELE_ID, $tempELEAry) ? "CHECKED" : "";
				}
				$ELEList .= "<INPUT type='checkbox' name='ele_global[]' value='".$ELE_ID."' id='".$ELE_ID."_". $relatedEnrolId."' $checked class='eleChk'><label id='ELE_labal_".$ELE_ID."_". $relatedEnrolId."' for='".$ELE_ID."_". $relatedEnrolId."'>".$ELE_Name."</label><br />";
			}
			
			if ($titleLang == $enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['Chinese']) {
				$checkedTitleEnglish = '';
				$checkedTitleChinese = 'checked';
			}
			else {
				$checkedTitleEnglish = 'checked';
				$checkedTitleChinese = '';
			}
			
			if ($intExt == $enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['IntExt']['External']) {
				$checkedInt = '';
				$checkedExt = 'checked';
			}
			else {
				$checkedInt = 'checked';
				$checkedExt = '';
			}
			
			
			$returnStr .= '
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['RecordType'].' </td>
						<td>
							<input type="radio" id="intExt_INT" name="intExt" value="'.$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['IntExt']['Internal'].'" '.$checkedInt.' onclick="clickedIntExtRadio();" /><label for="intExt_INT">'.$iPort["internal_record"].'</label>
							<input type="radio" id="intExt_EXT" name="intExt" value="'.$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['IntExt']['External'].'" '.$checkedExt.' onclick="clickedIntExtRadio();" /><label for="intExt_EXT">'.$iPort["external_record"].'</label>
						</td>
					</tr>';
					
			if ($RecordType == 'club') {
				$returnStr .= '
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title" width="30%">'.$Lang['eEnrolment']['TargetNameLang'].' </td>
						<td>
							<input type="radio" id="titleLang_english" name="titleLang" value="'.$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['English'].'" '.$checkedTitleEnglish.' /><label for="titleLang_english">'.$Lang['General']['English'].'</label>
							<input type="radio" id="titleLang_chinese" name="titleLang" value="'.$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['Chinese'].'" '.$checkedTitleChinese.' /><label for="titleLang_chinese">'.$Lang['General']['Chinese'].'</label>
						</td>
					</tr>';
			}
			
			$returnStr .= '
			<tr>
				<td valign="top" nowrap="nowrap" class="field_title" width="30%">'.$eEnrollment['OLE_Category'].' </td>
				<td>
					'.$this->Get_Ole_Category_Selection('category_'.$relatedEnrolId, 'category', $catId).'
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="field_title" width="30%">'.$eEnrollment['OLE_Components'].' </td>
				<td>
					'.$ELEList.'
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="field_title" width="30%">'.$ec_iPortfolio['organization'].'</td>
				<td>
					<input id="organization_'.$relatedEnrolId.'" name="organization" type="text" value="'.intranet_htmlspecialchars($organization).'" maxlength="256" class="textboxtext">
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="field_title" width="30%">'.$ec_iPortfolio['details'].'</td>
				<td>
					'.$linterface->GET_TEXTAREA("details", $details).'
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="field_title" width="30%">'.$ec_iPortfolio['school_remarks'].'</td>
				<td>
					'.$linterface->GET_TEXTAREA("SchoolRemarks", $remark).'
				</td>
			</tr>
			';
		
		
		return $returnStr;
	}
	
	function Copy_Club_By_Term_Step1_UI($AcademicYearID, $Semester)
	{
		global $Lang;
		include_once('form_class_manage.php');
		include_once('subject_class_mapping_ui.php');
		
		$libSCM_ui = new subject_class_mapping_ui();
		
		### Navigation
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "javascript:js_Go_Back()"); 
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherTerm'], ""); 
		$PageNavigation = $this->GET_NAVIGATION($PAGE_NAVIGATION);
		
		### Step Information
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][0], 1);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][1], 0);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][2], 0);
		
		### Get Academic Year Name
		$objAcademicYear = new academic_year($AcademicYearID);
		$academicYearName = $objAcademicYear->Get_Academic_Year_Name();
		
		### Generate From Term Selection
		$FromTermSelection = $libSCM_ui->Get_Term_Selection('FromTermID', $AcademicYearID, $Semester, 'js_Changed_From_Term_Selection();', $NoFirst=1);
		
		$ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Submit_Form();");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr><td colspan="2" class="navigation">'.$PageNavigation.'</td></tr>'."\n";
			$x .= '<table>'."\n";
			$x .= '<br style="clear:both" />'."\n";
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'.$this->GET_STEPS($STEPS_OBJ).'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<table class="form_table_v30">'."\n";
							# Target Academic Year
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>'."\n";
								$x .= '<td>'.$academicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# From Term
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eEnrolment']['CopyClub']['FromTerm'].'</td>'."\n";
								$x .= '<td>'.$FromTermSelection.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# To Term
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eEnrolment']['CopyClub']['ToTerm'].'</td>'."\n";
								$x .= '<td><div id="ToTermSelDiv"></div></td>'."\n";
							$x .= '</tr>'."\n";
							
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br style="clear:both" />'."\n";
			
			$x .= '<div id="ClubInfoDiv">'."\n";
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both" />'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				$x .= '<span></span>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $ContinueBtn."\n";
				$x .= $BackBtn."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
			
			$x .= $this->GET_HIDDEN_INPUT('AcademicYearID', 'AcademicYearID', $AcademicYearID);
		$x .= '</form>'."\n";
		
		return $x;
	}
	
	function Get_Copy_Club_By_Term_Step1_Club_Selection_Table($AcademicYearID, $FromYearTermID='', $ToYearTermID='')
	{
		global $Lang, $libenroll, $intranet_session_language;
		
		include_once('form_class_manage.php');
		$ToTermObj = new academic_year_term($ToYearTermID);
		$ToTermName = $ToTermObj->Get_Year_Term_Name();
		$ClubExistedRemark = str_replace('<!--TermName-->', $ToTermName, $Lang['eEnrolment']['CopyClub']['ClubExistedInTermAlready']);
			
		### Get All Clubs Info
		$FromClubInfoArr = $libenroll->GET_GROUPINFO('', $AcademicYearID, '', $FromYearTermID);
		$numOfClub = count($FromClubInfoArr);
		$ToClubInfoArr = $libenroll->GET_GROUPINFO('', $AcademicYearID, '', $ToYearTermID);
		$ToClubTitleArr = Get_Array_By_Key($ToClubInfoArr, 'Title');
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($Lang['eEnrolment']['CopyClub']['ClubSelection']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="ClubSelectionTableDiv">'."\n";
						$x .= '<table class="common_table_list" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:40%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['Club'].'</th>'."\n";
								
								# Copy
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyClubChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['Copy'];
									$x .= '<br />'."\n";
									$x .= $this->Get_Checkbox('CopyClub_Global', "Global_Arr[CopyClub]", $Value=1, $thisChecked=1, $Class='CopyClubChk_Global', $Display='', $thisOnclick);
								$x .= '</th>'."\n";
								
								# Copy Member
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyMemberChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubMember'];
									$x .= '<br />'."\n";
									$x .= $this->Get_Checkbox('CopyMember_Global', "Global_Arr[CopyMember]", $Value=1, $thisChecked=1, $Class='CopyMemberChk_Global', $Display='', $thisOnclick);
								$x .= '</th>'."\n";
								
								# Copy Activity
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyActivityChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubActivity'];
									$x .= '<br />'."\n";
									$x .= $this->Get_Checkbox('CopyActivity_Global', "Global_Arr[CopyActivity]", $Value=1, $thisChecked=1, $Class='CopyActivityChk_Global', $Display='', $thisOnclick);
								$x .= '</th>'."\n";
							$x .= '</tr>'."\n";
							
							if ($numOfClub > 0)
							{
								for ($i=0; $i<$numOfClub; $i++)
								{
									$thisEnrolGroupID = $FromClubInfoArr[$i]['EnrolGroupID'];
									$thisClubTitleDisplay = Get_Lang_Selection($FromClubInfoArr[$i]['TitleChinese'], $FromClubInfoArr[$i]['Title']);
									$thisClubTitle = $FromClubInfoArr[$i]['Title'];
									$thisSemesterTitle	= $FromClubInfoArr[$i]['SemesterTitle'];
									
									//$thisActivityArr = $libenroll->GET_EVENTINFO('', $thisEnrolGroupID); 
									//$numOfActivity = count($thisActivityArr);
									
									if (!in_array($thisClubTitle, $ToClubTitleArr))
									{
										### Club Title does not exist in the target Term => can copy
										$thisID = 'CopyClubChk_'.$thisEnrolGroupID;
										$thisName = 'CopyClubArr['.$thisEnrolGroupID.']';
										$thisClass = 'CopyClubChk';
										$thisOnclick = "js_Check_Uncheck_Global_Selection(this.checked, 'CopyClub_Global', '$thisEnrolGroupID');";
										$thisChecked = ($FromStep2!=1)? 1 : $CopyClubArr[$thisEnrolGroupID];
										$thisCopyClubDisplay = $this->Get_Checkbox($thisID, $thisName, 1, $thisChecked, $thisClass, $Display='', $thisOnclick);
										
										$thisID = 'CopyMemberChk_'.$thisEnrolGroupID;
										$thisName = 'CopyMemberArr['.$thisEnrolGroupID.']';
										$thisClass = 'CopyMemberChk';
										$thisOnclick = "js_Check_Uncheck_Global_Selection(this.checked, 'CopyMember_Global');";
										$thisChecked = ($FromStep2!=1)? 1 : $CopyMemberArr[$thisEnrolGroupID];
										$thisCopyMemberDisplay = $this->Get_Checkbox($thisID, $thisName, 1, $thisChecked, $thisClass, $Display='', $thisOnclick);
										
										$thisID = 'CopyActivityChk_'.$thisEnrolGroupID;
										$thisName = 'CopyActivityArr['.$thisEnrolGroupID.']';
										$thisClass = 'CopyActivityChk';
										//$thisDisabled = ($numOfActivity==0)? 1 : 0;
										//$thisChecked = ($thisDisabled==0)? 1 : 0;
										$thisOnclick = "js_Check_Uncheck_Global_Selection(this.checked, 'CopyActivity_Global');";
										$thisChecked = ($FromStep2!=1)? 1 : $CopyActivityArr[$thisEnrolGroupID];
										$thisCopyActivityDisplay = $this->Get_Checkbox($thisID, $thisName, 1, $thisChecked, $thisClass, $Display='', $thisOnclick, $thisDisabled);
									}
									
									### Show in red if title is duplicated in the target term
									if (in_array($thisClubTitle, $ToClubTitleArr))
									{
										$TextPrefix = '<font color="red">';
										$TextSuffix = '</font>';
									}
									else
									{
										$TextPrefix = '';
										$TextSuffix = '';
									}
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$TextPrefix.$thisClubTitleDisplay.$TextSuffix.'</td>'."\n";
										
										if (!in_array($thisClubTitle, $ToClubTitleArr)) {
											$x .= '<td style="text-align:center">'.$thisCopyClubDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyMemberDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyActivityDisplay.'</td>'."\n";
										}
										else {
											### Group Name duplicated in the target Term
											$x .= '<td colspan="3" style="text-align:center">'.$TextPrefix.$ClubExistedRemark.$TextSuffix.'</font></td>'."\n";
										}
									$x .= '</tr>'."\n";
								}
							}
							else
							{
								$x .= '<tr><td colspan="5" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							
						$x .= '</table>'."\n";
						
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Copy_Club_By_Term_Step2_UI($AcademicYearID, $FromYearTermID, $ToYearTermID, $CopyClubArr, $CopyMemberArr, $CopyActivityArr) {
		global $Lang;
		include_once('form_class_manage.php');
		
		### Navigation
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'], "javascript:js_Cancel()"); 
		$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClubFromOtherTerm'], ""); 
		$PageNavigation = $this->GET_NAVIGATION($PAGE_NAVIGATION);
		
		### Step Information
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][0], 0);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][1], 1);
		$STEPS_OBJ[] = array($Lang['eEnrolment']['CopyClub']['StepArr'][2], 0);
		
		### Get Academic Year Name
		$objAcademicYear = new academic_year($AcademicYearID);
		$academicYearName = $objAcademicYear->Get_Academic_Year_Name();
		
		$FromTermObj = new academic_year_term($FromYearTermID);
		$FromTermName = $FromTermObj->Get_Year_Term_Name();
		
		$ToTermObj = new academic_year_term($ToYearTermID);
		$ToTermName = $ToTermObj->Get_Year_Term_Name();
		
		$ContinueBtn = $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Submit_Form();");
		$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
		$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");
		
		$x = '';
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">'."\n";
				$x .= '<tr><td colspan="2" class="navigation">'.$PageNavigation.'</td></tr>'."\n";
			$x .= '<table>'."\n";
			$x .= '<br style="clear:both" />'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td colspan="2">'.$this->GET_STEPS($STEPS_OBJ).'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<table class="form_table_v30">'."\n";
							# Target Academic Year
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>'."\n";
								$x .= '<td>'.$academicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# From Term
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eEnrolment']['CopyClub']['FromTerm'].'</td>'."\n";
								$x .= '<td>'.$FromTermName.'</td>'."\n";
							$x .= '</tr>'."\n";
							
							# To Term
							$x .= '<tr>'."\n";
								$x .= '<td class="field_title">'.$Lang['eEnrolment']['CopyClub']['ToTerm'].'</td>'."\n";
								$x .= '<td>'.$ToTermName.'</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
						$tableInfoAry = $this->Get_Copy_Club_By_Term_Step2_Club_Selection_Table($AcademicYearID, $FromYearTermID, $ToYearTermID, $CopyClubArr, $CopyMemberArr, $CopyActivityArr);
						$tableHtml = $tableInfoAry['HTML'];
						$NumOfCannotCopyClub = $tableInfoAry['NumOfCannotCopyClub'];
						$x .= $tableHtml."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				$x .= '<span></span>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				if ($NumOfCannotCopyClub == 0)
					$x .= $ContinueBtn."\n";
				$x .= $BackBtn."\n";
				$x .= $CancelBtn."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<input type="hidden" id="FromStep2" name="FromStep2" value="1">'."\n";
			$x .= '<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="'.$AcademicYearID.'">'."\n";
			$x .= '<input type="hidden" id="FromYearTermID" name="FromYearTermID" value="'.$FromYearTermID.'">'."\n";
			$x .= '<input type="hidden" id="ToYearTermID" name="ToYearTermID" value="'.$ToYearTermID.'">'."\n";
			$x .= '<input type="hidden" id="CopyClubArr" name="CopyClubArr" value="'.rawurlencode(serialize($CopyClubArr)).'">'."\n";
			$x .= '<input type="hidden" id="CopyMemberArr" name="CopyMemberArr" value="'.rawurlencode(serialize($CopyMemberArr)).'">'."\n";
			$x .= '<input type="hidden" id="CopyActivityArr" name="CopyActivityArr" value="'.rawurlencode(serialize($CopyActivityArr)).'">'."\n";
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
		
		return $x;
	}
	
	
	function Get_Copy_Club_By_Term_Step2_Club_Selection_Table($AcademicYearID, $FromYearTermID, $ToYearTermID, $CopyClubArr, $CopyMemberArr, $CopyActivityArr)
	{
		global $Lang, $libenroll, $i_ClubsEnrollment_WholeYear, $intranet_session_language;
		
		### check duplicated group title
		include_once('form_class_manage.php');
		$ToTermObj = new academic_year_term($ToYearTermID);
		$ToTermName = $ToTermObj->Get_Year_Term_Name();
		$ClubExistedRemark = str_replace('<!--TermName-->', $ToTermName, $Lang['eEnrolment']['CopyClub']['ClubExistedInTermAlready']);
		
		### To Year Term Name
		$ToTermObj = new academic_year_term($ToYearTermID);
		$ToTermName = $ToTermObj->Get_Year_Term_Name();
			
		### Get All Clubs Info
		$FromClubInfoArr = $libenroll->GET_GROUPINFO('', $AcademicYearID, '', $FromYearTermID);
		$numOfClub = count($FromClubInfoArr);
		$ToClubInfoArr = $libenroll->GET_GROUPINFO('', $AcademicYearID, '', $ToYearTermID);
		$ToClubTitleArr = Get_Array_By_Key($ToClubInfoArr, 'Title');
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td class="tabletext">'.$this->GET_NAVIGATION2($Lang['eEnrolment']['CopyClub']['ClubSelection']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="ClubSelectionTableDiv">'."\n";
						$x .= '<table class="common_table_list" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:40%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							$x .= '<col style="width:20%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['eEnrolment']['CopyClub']['Club'].'</th>'."\n";
								
								# Copy
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyClubChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['Copy'];
								$x .= '</th>'."\n";
								
								# Copy Member
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyMemberChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubMember'];
								$x .= '</th>'."\n";
								
								# Copy Activity
								$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyActivityChk');";
								$x .= '<th style="text-align:center">'."\n";
									$x .= $Lang['eEnrolment']['CopyClub']['ClubActivity'];
								$x .= '</th>'."\n";
							$x .= '</tr>'."\n";
							
							if ($numOfClub > 0)
							{
								$CannotCopyClubArr = array();
								for ($i=0; $i<$numOfClub; $i++)
								{
									$thisEnrolGroupID 		= $FromClubInfoArr[$i]['EnrolGroupID'];
									$thisGroupID 			= $FromClubInfoArr[$i]['GroupID'];
									$thisClubTitle 			= $FromClubInfoArr[$i]['Title'];
									$thisClubTitleDisplay 	= Get_Lang_Selection($FromClubInfoArr[$i]['TitleChinese'], $FromClubInfoArr[$i]['Title']);
									$thisSemesterTitle		= $FromClubInfoArr[$i]['SemesterTitle'];
									$thisSemester			= $FromClubInfoArr[$i]['Semester'];
									$thisCopyClub 			= $CopyClubArr[$thisEnrolGroupID];
									$thisCopyMember 		= $CopyMemberArr[$thisEnrolGroupID];
									$thisCopyActivity		= $CopyActivityArr[$thisEnrolGroupID];
									
									$thisTitleDuplicated = in_array($thisClubTitle, $ToClubTitleArr);
									
									### Check if the Club is valid to transfer
									$thisWarningArr = array();
									if (!$thisTitleDuplicated) {
										### Club Title does not exist in the target Term => can copy
										$thisCopyClubDisplay = ($thisCopyClub==1)? $Lang['General']['Yes'] : '<span class="tabletextremark">'.$Lang['General']['No'].'</span>';
										$thisCopyMemberDisplay = ($thisCopyMember==1)? $Lang['General']['Yes'] : '<span class="tabletextremark">'.$Lang['General']['No'].'</span>';
										$thisCopyActivityDisplay = ($thisCopyActivity==1)? $Lang['General']['Yes'] : '<span class="tabletextremark">'.$Lang['General']['No'].'</span>';
									}
									
									
									### Show in grey if title is duplicated in the target term
									$numOfWarning = count($thisWarningArr);
									if ($CopyClubArr[$thisEnrolGroupID]==1 && $numOfWarning==0) {
										$TextPrefix = '';
										$TextSuffix = '';
									}
									else if ($CopyClubArr[$thisEnrolGroupID]==0 || $thisTitleDuplicated) {
										$TextPrefix = '<span class="tabletextremark">';
										$TextSuffix = '</span>';
									}
									else if ($numOfWarning > 0) {
										$TextPrefix = '<font color="red">';
										$TextSuffix = '</font>';
									}
									
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$TextPrefix.$thisClubTitleDisplay.$TextSuffix.'</td>'."\n";
										
										if ($thisTitleDuplicated) {
											$x .= '<td colspan="3" style="text-align:center">'.$TextPrefix.$ClubExistedRemark.$TextSuffix.'</font></td>'."\n";
										}
										else if (count($thisWarningArr)==0) {
											$x .= '<td style="text-align:center">'.$thisCopyClubDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyMemberDisplay.'</td>'."\n";
											$x .= '<td style="text-align:center">'.$thisCopyActivityDisplay.'</td>'."\n";
										}
										else {
											$thisWarningDisplay = implode('<br />', $thisWarningArr);
											$x .= '<td colspan="3" style="text-align:center">'.$TextPrefix.$thisWarningDisplay.$TextSuffix.'</font></td>'."\n";
										}
									$x .= '</tr>'."\n";
								}
							}
							else {
								$x .= '<tr><td colspan="4" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$NumOfCannotCopyClub = count($CannotCopyClubArr);
		return array("HTML" => $x, "NumOfCannotCopyClub" => $NumOfCannotCopyClub);
	}
	
	function Get_Enrollment_Status_Display($statusCode) {
		global $enrolConfigAry, $Lang;
		
		$statusDisplay = '';
		switch ($statusCode) {
			case '':
				$statusDisplay = $Lang['General']['EmptySymbol'];
				break;
			case $enrolConfigAry['EnrolmentRecordStatus_Waiting']:
				$statusDisplay = $Lang['eEnrolment']['EnrollmentStatus']['Waiting'];
				break;
			case $enrolConfigAry['EnrolmentRecordStatus_Rejected']:
				$statusDisplay = $Lang['eEnrolment']['EnrollmentStatus']['Rejected'];
				break;
			case $enrolConfigAry['EnrolmentRecordStatus_Approved']:
				$statusDisplay = $Lang['eEnrolment']['EnrollmentStatus']['Approved'];
				break;
			default:
				$statusDisplay = $Lang['General']['EmptySymbol'];
		}
		
		return $statusDisplay;
	}
	
	function Get_Student_Application_Meeting_Date_Display($applicantId, $enrolGroupId, $choiceNumber, $clubInfoAry='', $meetingDateInfoAry='') {
		global $Lang, $libenroll;
		
		if ($clubInfoAry == '' && $applicantId != '' && $enrolGroupId != '') {
			$clubInfoAry = $libenroll->Get_All_Club_Info($enrolGroupId);
			$clubInfoAry = $clubInfoAry[0];
		}
				
		if ($meetingDateInfoAry == '' && $applicantId != '' && $enrolGroupId != '') {
			$meetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($enrolGroupId);
			if (count($meetingDateInfoAry) > 0) {
				if ($libenroll->enableUserJoinDateRange()) {
					$enrolAvailArr = $libenroll->Get_Enrol_AvailDate($enrolGroupId, array($applicantId), "Club");
					if (count($enrolAvailArr) > 0) {
						foreach ($meetingDateInfoAry as $kk => $vv) {
							$checkStart = strtotime(date("Y-m-d", strtotime($vv["ActivityDateStart"])));
							if (empty($enrolAvailArr[$applicantId]["EnrolAvailiableDateStart"])) $availStart = ""; 
							else $availStart = strtotime(date("Y-m-d", strtotime($enrolAvailArr[$applicantId]["EnrolAvailiableDateStart"])));
							if (empty($enrolAvailArr[$applicantId]["EnrolAvailiableDateEnd"])) $availEnd = "";
							else $availEnd = strtotime(date("Y-m-d", strtotime($enrolAvailArr[$applicantId]["EnrolAvailiableDateEnd"])));
		
							if ((empty($availStart) || $checkStart >= $availStart) && (empty($availEnd) || $checkStart <= $availEnd)) {
								/* in available date range */
							} else {
								unset($meetingDateInfoAry[$kk]);
							}
						}
						if (count($meetingDateInfoAry) > 0) {
							$meetingDateInfoAry = array_values($meetingDateInfoAry);
						}
					}
				}
			}
		}
		
		$timeDesc = nl2br(trim($clubInfoAry['TimeDescription']));
		$numOfMeetingDate = count($meetingDateInfoAry);
		
		$timeDescDisplay = '';
		$dateDisplayStyle = '';
		$showDetailsLinkStyle = '';
		$hideDetailsLinkStyle = '';
		if($timeDesc != '') {
			$dateDisplayStyle = 'display:none;';
			$showDetailsLinkStyle = '';
			$hideDetailsLinkStyle = 'display:none;';
		}
		else {
			$dateDisplayStyle = '';
			$showDetailsLinkStyle = 'display:none;';
			$hideDetailsLinkStyle = '';
		}
		
		$x = '';
		$x .= $timeDesc;
		if ($numOfMeetingDate > 0) {
			$x .= '<div id="ViewDetailLink'.$choiceNumber.'" style="'.$showDetailsLinkStyle.'">[<a class="tablelink showDateDetailsBtn" href="javascript:void(0);" onclick="ShowDateDetail(\''.$choiceNumber.'\');">'.$Lang['eEnrolment']['ViewDetailedTimePeriod'].'</a>]</div>';
			$x .= '<div id="HideDetailLink'.$choiceNumber.'" style="'.$hideDetailsLinkStyle.'">[<a class="tablelink hideDateDetailsBtn" href="javascript:void(0);" onclick="HideDateDetail(\''.$choiceNumber.'\');">'.$Lang['eEnrolment']['HideDetailedTimePeriod'].'</a>]</div>';
			$x .= '<div id="DateDetailDiv'.$choiceNumber.'" style="width:100%; '.$dateDisplayStyle.'">';
				for ($i=0; $i<$numOfMeetingDate; $i++) {
					$_meetingDateStart = $meetingDateInfoAry[$i]['ActivityDateStart'];
					$_meetingDateEndStart = $meetingDateInfoAry[$i]['ActivityDateEnd'];
					
					$x .= date('Y-m-d', strtotime($_meetingDateStart)).' ('.date('D', strtotime($_meetingDateStart)).') '.date('H:i', strtotime($_meetingDateStart)).'-'.date('H:i', strtotime($_meetingDateEndStart));
					$x .= '<br />';
				}
			$x .= '</div>';
		}
		if ($x == '') {
			$x = $Lang['General']['EmptySymbol'];
		}
		
		return $x;
	}
	
	function Get_Student_Application_Fee_Display($applicantId, $enrolGroupId, $clubInfoAry='') {
		global $Lang, $eEnrollment, $libenroll;
		
		if ($clubInfoAry == '' && $applicantId != '' && $enrolGroupId != '') {
			$clubInfoAry = $libenroll->Get_All_Club_Info($enrolGroupId);
			$clubInfoAry = $clubInfoAry[0];
		}
		
		$fee = $clubInfoAry['PaymentAmount'];
		$tbc = $clubInfoAry['isAmountTBC'];
		
		$x = '';
		if ($tbc) {
			$x .= $eEnrollment['ToBeConfirmed'];
		}
		else {
			if ($fee === '' || $fee == null) {
				$x .= $Lang['General']['EmptySymbol'];
			}
			else {
				$x .= $fee;
			}
		}
		
		return $x;
	}
	
	function Get_Student_Application_Status_Display($applicantId, $enrolGroupId, $appliedStatus='') {
		global $libenroll;
		
		if ($appliedStatus=='' && $applicantId != '' && $enrolGroupId != '') {
			$studentClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($applicantId, $enrolGroupId);
			$appliedStatus = $studentClubInfoAry[0]['RecordStatus'];
		}
		
		return $this->Get_Enrollment_Status_Display($appliedStatus);
	}
	
	function Get_Student_Application_Position_Display($applicantId, $enrolGroupId) {
		global $libenroll;
		
		return $libenroll->GET_STUDENT_GROUP_POSITION($enrolGroupId, $applicantId);
	}
	
	function Get_Student_Application_Application_Date_Time_Display($applicantId, $enrolGroupId, $applicationDateTime='') {
		global $libenroll, $Lang;
		
		if ($applicationDateTime=='' && $applicantId != '' && $enrolGroupId != '') {
			$studentClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($applicantId, $enrolGroupId);
			$applicationDateTime = $studentClubInfoAry[0]['DateInput'];
		}
		
		return ($applicationDateTime=='')? $Lang['General']['EmptySymbol'] : $applicationDateTime;
	}
	
	function Get_Student_Application_Role_Display($applicantId, $enrolGroupId, $roleTitle='') {
		global $libenroll, $Lang;
		
		if ($roleTitle=='' && $applicantId != '' && $enrolGroupId != '') {
			$enrolledClubInfoAry = $libenroll->Get_Student_Club_Info($applicantId, $enrolGroupId);
			$roleTitle = $enrolledClubInfoAry[0]['RoleTitle'];
		}
		
		return ($roleTitle=='')? $Lang['General']['EmptySymbol'] : $roleTitle;
	}
	
	function Get_Student_Application_Attendance_Display($applicantId, $enrolGroupId, $choiceNumber, $applyStatus='') {
		global $enrolConfigAry, $Lang, $libenroll;
		
		if ($applyStatus=='' && $applicantId != '' && $enrolGroupId != '') {
			$studentClubInfoAry = $libenroll->Get_Student_Applied_Club_Info($applicantId, $enrolGroupId);
			$appliedStatus = $studentClubInfoAry[0]['RecordStatus'];
		}
		
		$x = '';
		if ($applyStatus == $enrolConfigAry['EnrolmentRecordStatus_Approved']) {
			$x .= '<a href="javascript:void(0);" onclick="viewAttendance('.$choiceNumber.');" class="tablelink">'.$Lang['Btn']['View'].'</a>';
		}
		else {
			$x .= $Lang['General']['EmptySymbol'];
		}
		
		return $x;
	}
	
	function Get_Processing_Enrolment_Message() {
		global $libenroll, $Lang;
		
		$msg = $Lang['eEnrolment']['WithinEnrolmentProcessPeriod'];
		$msg = str_replace('<!--date-->', $libenroll->AnnounceEnrolmentResultDate, $msg);
		$msg = str_replace('<!--time-->', $libenroll->AnnounceEnrolmentResultHour.':'.$libenroll->AnnounceEnrolmentResultMin, $msg);
		
		return $msg;
	}
	
	function Get_Meeting_Date_Selection_Calendar($RecordType, $RecordId, $Year='', $Month='') {
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
		
		include_once($PATH_WRT_ROOT."includes/libcal.php");
		include_once($PATH_WRT_ROOT."includes/libcalevent.php");
		include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
		include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
		
		$lcal = new libcalevent2007();
		$lcycleperiods = new libcycleperiods();
		
		### Get the current year and month if not provided
		$Year = ($Year=='')? date("Y") : $Year;
		$Month = ($Month=='')? date("n") : $Month;
		
		$calendarDateTimestampAry = $lcal->returnCalendarArray($Month, $Year);
		$numOfDate = count($calendarDateTimestampAry);
        $dateCycleInfoAry = $lcycleperiods->getProductionCycleInfo($Year, $Month);
        
        $x = '';
        $x .='<table border="0" align="center" cellpadding="0" cellspacing="0">';
			$x .='<tr>';
				$x .='<td width="21"><a href="javascript:void(0);" onclick="changeMonth(-1)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" /></a></td>';
				$x .='<td align="center">'.date("F Y",strtotime("$Year-$Month-01")).'</td>';
				$x .='<td width="21"><a href="javascript:void(0);" onclick="changeMonth(1)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" /></a></td>';
			$x .='</tr>';
		$x .='</table>';
		$x .='<table border="0" cellspacing="0" cellpadding="0" class="booking_table_small_cal">';
			$x .='<tr>';
				$x .='<td class="icalendar_weektitle">S</td>';
				$x .='<td class="icalendar_weektitle">M</td>';
				$x .='<td class="icalendar_weektitle">T</td>';
				$x .='<td class="icalendar_weektitle">W</td>';
				$x .='<td class="icalendar_weektitle">T</td>';
				$x .='<td class="icalendar_weektitle">F</td>';
				$x .='<td class="icalendar_weektitle">S</td>';
			$x .='</tr>';
			# Show Dates
	        for($i=0; $i<$numOfDate; $i++) {
	        	$_timestamp = $calendarDateTimestampAry[$i];
	        	
	        	$_dateText = '';
	        	if($_timestamp != '') {
	           		$_dateText = date("Y-m-d", $_timestamp);
	        	}
	        	
	        	$_cycleInfoAry = $dateCycleInfoAry[$_dateText];
	          	$_cycleText = $_cycleInfoAry[2];
	        	
	        	$x .= ($i % 7 == 0) ? "<tr>\n" : "";
	          		$x .= $this->Get_Meeting_Date_Selection_Calendar_Date_Display($_timestamp, $_cycleText);
	          	$x .= ($i % 7 == 6) ? "</tr>\n" : "";
	        }
        $x .='</table>';
        
        return $x;
	}
	
	function Get_Meeting_Date_Selection_Calendar_Date_Display($ts='', $cycle='', $available=true) {
		if ($ts=='') {
        	$x = "<td class=\"small_cal_blank\">&nbsp;</td>";
        }
        else if ($available) {
			$datestr = date("Y-m-d",$ts);
			$day = date("D",$ts); 
        	$x = "	<td class=\"small_cal_normal_day bookable\">
						<a href=\"javascript:void(0);\" class=\"ebooking_choose\" onclick=\"addSingleDate('$datestr');\" style=\"cursor:pointer;\">
							<span>
								".date("j",$ts)."
								<br>
								<em>".$cycle."</em>
							</span>
						</a>
					</td>";
		}
		else {
			$x = "<td class=\"small_cal_normal_day non_bookable\" ><span>".date("j",$ts)."<br><em>".$cycle."</em></span></td>";
		}
		
		return $x;
	}
	
	function Get_Meeting_Date_Time_Mgmt_Table($RecordType, $RecordId) {
		global $Lang, $eEnrollment, $enrolConfigAry, $libenroll, $sys_custom;
		
		if ($RecordType == $enrolConfigAry['Club']) {
			$meetingInfoAry = $libenroll->GET_ENROL_GROUP_DATE($RecordId);
			$dateIdKey = 'GroupDateID';
		}
		else if ($RecordType == $enrolConfigAry['Activity']) {
			$meetingInfoAry = $libenroll->GET_ENROL_EVENT_DATE($RecordId);
			$dateIdKey = 'EventDateID';
		}
		else if ($RecordType == $enrolConfigAry['Setting']['TimeLimit']) {
		    $meetingInfoAry = $libenroll->getTimeLimitSetting($RecordId);
		    $dateIdKey = 'RecordID';
		}
		
		$numOfMeeting = count($meetingInfoAry);
		
		$x = '';
		$x .= '<table id="dateTimeSettingTable" class="common_table_list_v30 edit_table_list_v30">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th style="width:25px;">#</th>'."\n";
					$x .= '<th style="width:15%;">'.$Lang['General']['Date'].'</th>'."\n";
					$x .= '<th>'.$eEnrollment['add_activity']['act_time'].'</th>'."\n";
					if($sys_custom['eEnrolment']['EnableLocation_tlgcCust'] && ($RecordType != $enrolConfigAry['Setting']['TimeLimit'])){
						$x .= '<th style="width:45%;">'.$Lang['eEnrolment']['Location'].'</th>'."\n";
					}
					$x .= '<th style="width:25px;">&nbsp;</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr class="edit_table_head_bulk">'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
					$x .= '<th>'."\n";
						$x .= $this->Get_Time_Selection_Box('globalHourStart', 'hour', 0);
						$x .= ' : '; 
						$x .= $this->Get_Time_Selection_Box('globalMinStart', 'min', 0, $others_tab='', $interval=5);
						$x .= ' '.$eEnrollment['to'].' ';
						$x .= $this->Get_Time_Selection_Box('globalHourEnd', 'hour', 0);
						$x .= ' : ';
						$x .= $this->Get_Time_Selection_Box('globalMinEnd', 'min', 0, $others_tab='', $interval=5);
						$x .= ' ';
						$x .= $this->GET_SMALL_BTN($eEnrollment['add_activity']['set_all'], "button", "javascript:applyTimeToDate(0);");
						$x .= ' '; 
						$x .= $this->GET_SMALL_BTN($eEnrollment['add_activity']['set_select_period'], "button", "javascript:applyTimeToDate(1);");
						$x .= $this->Get_Form_Warning_Msg('global_invalidTimeRangeWarningDiv', $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');
					$x .= '</th>'."\n";
					if($sys_custom['eEnrolment']['EnableLocation_tlgcCust'] && ($RecordType != $enrolConfigAry['Setting']['TimeLimit'])){
						$x .= '<th>'.$this->GET_TEXTBOX('globalLocation', 'globalLocation', '', $OtherClass='', $OtherPar=array('size'=>'20')).$this->Get_Apply_All_Icon("JavaScript: PasteToAllLocation()").'</th>'."\n";
					}
					$x .= '<th><input type="checkbox" id="globalDateChk" onclick="Check_All_Options_By_Class(\'dateChk\', this.checked);"></th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$h_hiddenField = '';
			$x .= '<tbody>'."\n";
				// date time content
				for ($i=0; $i<$numOfMeeting; $i++) {
					$_rowNum = $i + 1;
					
					$_dateId = $meetingInfoAry[$i][$dateIdKey];
					$_dateTimeStart = $meetingInfoAry[$i]['ActivityDateStart'];
					$_dateTimeEnd = $meetingInfoAry[$i]['ActivityDateEnd'];
					$_dateText = getDateByDateTime($_dateTimeStart);
					
					if($sys_custom['eEnrolment']['EnableLocation_tlgcCust'] && ($RecordType != $enrolConfigAry['Setting']['TimeLimit'])){
						$_location = $meetingInfoAry[$i]['Location'];
						$x .= $this->Get_Meeting_Date_Time_Mgmt_Table_Row($_rowNum, $_dateTimeStart, $_dateTimeEnd, $_location, $RecordType);
					}else{
						$x .= $this->Get_Meeting_Date_Time_Mgmt_Table_Row($_rowNum, $_dateTimeStart, $_dateTimeEnd, '', $RecordType);
					}
					$h_hiddenField .= $this->GET_HIDDEN_INPUT('dateRecordId_'.$_rowNum, 'dateInfoAry['.$_rowNum.'][dateRecordId]', $_dateId);
				}
				
				// no record display row
				$displayPara = ($numOfMeeting==0)? '' : 'display:none;';
				$x .= '<tr id="noRecordTr" style="'.$displayPara.'"><td colspan="4" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		$x .= $h_hiddenField;
		
		$x .= '<textarea id="dateTimeTrTemplate" style="display:none;">'."\n";
			$x .= $this->Get_Meeting_Date_Time_Mgmt_Table_Row($enrolConfigAry['MeetingDateNewRowNumTempCode'], $enrolConfigAry['MeetingDateNewRowNumTempDateStart'].' 00:00:00', $enrolConfigAry['MeetingDateNewRowNumTempDateEnd'].' 00:00:00', '', $RecordType);
		$x .= '</textarea>'."\n"; 
		
		return $x;
	}
	
	function Get_Meeting_Date_Time_Mgmt_Table_Row($RowNumber, $StartDateTime='', $EndDateTime='', $Location='', $RecordType='') {
		global $eEnrollment, $Lang, $sys_custom;
		
		$dateStart = getDateByDateTime($StartDateTime);
		$hourStart = getHourByDateTime($StartDateTime);
		$minStart = getMinuteByDateTime($StartDateTime);
		$hourEnd = getHourByDateTime($EndDateTime);
		$minEnd = getMinuteByDateTime($EndDateTime);
		
		$hourStartSelection = $this->Get_Time_Selection_Box('dateInfoAry['.$RowNumber.'][hourStart]', 'hour', $hourStart, $others_tab='', $interval=1, $id='hourStart_'.$RowNumber);
		$minStartSelection = $this->Get_Time_Selection_Box('dateInfoAry['.$RowNumber.'][minStart]', 'min', $minStart, $others_tab='', $interval=5, $id='minStart_'.$RowNumber);
		$hourEndSelection = $this->Get_Time_Selection_Box('dateInfoAry['.$RowNumber.'][hourEnd]', 'hour', $hourEnd, $others_tab='', $interval=1, $id='hourEnd_'.$RowNumber);
		$minEndSelection = $this->Get_Time_Selection_Box('dateInfoAry['.$RowNumber.'][minEnd]', 'min', $minEnd, $others_tab='', $interval=5, $id='minEnd_'.$RowNumber);
		
		$checkboxId = '';
		$checkbox = $this->Get_Checkbox('dateChk_'.$RowNumber, 'dateChk_'.$RowNumber, $Value=1, $isChecked=0, $Class="dateChk", $Display="", $Onclick="Uncheck_SelectAll('globalDateChk', this.checked);");
		
		$x = '';
		$x .= '<tr id="dateTimeTr_'.$RowNumber.'" class="dateTimeTr">'."\n";
			$x .= '<td><div id="rowNumDiv_'.$RowNumber.'">'.$RowNumber.'</div></td>'."\n";
			$x .= '<td>'."\n";
				$x .= $dateStart."\n";
				$x .= $this->GET_HIDDEN_INPUT('dateTextHidden_'.$RowNumber, 'dateInfoAry['.$RowNumber.'][dateText]', $dateStart)."\n";
			$x .= '</td>'."\n";
			$x .= '<td>'."\n";
				$x .= $hourStartSelection.' : '.$minStartSelection.' '.$eEnrollment['to'].' '.$hourEndSelection.' : '.$minEndSelection;
				$x .= '<br />';
				$x .= $this->Get_Form_Warning_Msg('invalidTimeRangeWarningDiv_'.$RowNumber, $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');
				$x .= $this->Get_Form_Warning_Msg('timeOverlapWarningDiv_'.$RowNumber, $Lang['eEnrolment']['Warning']['TimeOverlap'], 'warningDiv');
			$x .= '</td>'."\n";
			#Location #W107649
			if($sys_custom['eEnrolment']['EnableLocation_tlgcCust'] && ($RecordType != $enrolConfigAry['Setting']['TimeLimit'])){
				$x .= "<td>".$this->GET_TEXTBOX('Location_'.$RowNumber, 'dateInfoAry['.$RowNumber.'][Location]', $Location, $OtherClass='', $OtherPar=array('size'=>'20'))."</td>"."\n";
			}
			$x .= '<td>'.$checkbox.'</td>'."\n";
		$x .= '</tr>'."\n";
		
		return $x;
	}
	
	function Get_Meeting_Date_Periodic_Date_Selection_Table() {
		global $Lang, $eEnrollment;
		
		$x .= '<table border="0" cellpadding="5" cellspacing="0" id="SpecificTimeRepetitiveTable">';
			$x .= '<tr>';
				$x .= '<td style="padding:5px; background-color:#dbedff"  valign="top"  height="50%">';
					$x .= '<div style="background-color:#f7f7f7">'."\n";
						$x .= '<table border=0 width="520">'."\n";
							$x .= '<col width=110>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['General']['StartDate'].'</td>'."\n";
								//$x .= '<td>'.$this->Get_Date_Picker("StartDate", $StartDate,'','','','','','checkDate("StartDate",this, dateText, inst)').'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $this->Get_Date_Picker("StartDate", $StartDate)."\n";
									$x .= '<span class="tabletextremark">(<span id="periodicDate_academicYearStartDateSpan"></span>)</span>';
									$x .= $this->Get_Form_Warning_Msg('invalidDateRangeWarningDiv', $Lang['eEnrolment']['Warning']['DateRangeInvalid'], 'warningDiv');
									$x .= $this->Get_Form_Warning_Msg('invalidStartDateWarningDiv', $Lang['eEnrolment']['Warning']['StartDateInvalid'], 'warningDiv');
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";											
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['General']['EndDate'].'</td>'."\n";
								//$x .= '<td>'.$this->Get_Date_Picker("EndDate", $EndDate,'','','','','','checkDate("EndDate",this, dateText, inst)').'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $this->Get_Date_Picker("EndDate", $EndDate)."\n";
									$x .= '<span class="tabletextremark">(<span id="periodicDate_academicYearEndDateSpan"></span>)</span>';
									$x .= $this->Get_Form_Warning_Msg('invalidEndDateWarningDiv', $Lang['eEnrolment']['Warning']['EndDateInvalid'], 'warningDiv');
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Period / Cycle
							$x .= '<tr valign="top">'."\n";
							     $x .= '<td>'.$Lang['eEnrolment']['MeetingDateCycle']['Choice'].'</td>'."\n";
							     $x .= '<td>'."\n";
							         $x .= "<input type='radio' name='rcChoice' id='rcChoice_p' value='period' onchange='checkRCChoice();' checked/>";
							         $x .= "<label for='rcChoice_p'>{$Lang['eEnrolment']['MeetingDateCycle']['Period']}</label>";
							         $x .= "<input type='radio' name='rcChoice' id='rcChoice_c' value='cycle' onchange='checkRCChoice();' />";
							         $x .= "<label for='rcChoice_c'>{$Lang['eEnrolment']['MeetingDateCycle']['Cycle']}</label>";
							     $x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Cycle
							$x .= '<tr valign="top" id="cycleTr" hidden>'."\n";
							     $x .= '<td>'.$Lang['eEnrolment']['MeetingDateCycle']['Cycle'].'</td>'."\n";
							     $x .= '<td>'."\n";
							     $x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Period
							$x .= '<tr valign="top" id="periodTr">'."\n";
								$x .= '<td>'.$Lang['eEnrolment']['OnEvery'].'</td>'."\n";
								$x .= '<td>'."\n";
									# Week Day
//									($StoredValueForBack['RepeatType']==1 || !$StoredValueForBack['RepeatType'])?$WeekDayChecked = 1:$CyclyDayChecked = 1;
//									$x .= '<div>'."\n";
//										$x .= $this->Get_Radio_Button("RepeatTypeWeekDay", "RepeatType", 1, $WeekDayChecked, "RepeatType", "", "js_Toggle_Repeat_Type()").$Lang['eBooking']['WeekDay']."<br>";
//									$x .= '</div>'."\n";
									$x .= '<div>'."\n";
										for($i=0 ;$i<7; $i++) {
											$x .= $this->Get_Checkbox("WeekDay$i", "SelectedWeekDay[]", $i, false, 'weekdayChk', '', $Onclick='', $Disabled='');
											$x .= "<label for='WeekDay$i'>".$Lang['eEnrolment']['MeetingDatePeriod']['ShortWeekday'][$i]."&nbsp;"."</label>";
										}
									$x .= '</div>'."\n";
									$x .= $this->Get_Form_Warning_Msg('selectWeekdayWarningDiv', $Lang['eEnrolment']['Warning']['SelectWeekday'], 'warningDiv');
//									
//									# Cycle Day
//									include_once("libcycleperiods.php");
//									$lcycleperiods = new libcycleperiods();
//									$CurrentTimezone = $lcycleperiods->getCurrentCyclePeriod();
//									
//									if($CurrentTimezone[0]['CycleType']!=0)
//									{
//										$CycleDayArr = $lcycleperiods->Get_Cycle_Day_By_TimeZone($CurrentTimezone[0]['PeriodID']);
//										
//										$x .= '<div>'."\n";
//											$x .= $this->Get_Radio_Button("RepeatTypeCycleDay", "RepeatType", 2, $CyclyDayChecked, "RepeatType", "", "js_Toggle_Repeat_Type()").$Lang['eBooking']['eService']['Cycle']."<br>";
//										$x .= '</div>'."\n";
//										$x .= '<div style="padding-left:20px;">'."\n";
//											$SizeOfCycle = sizeof($CycleDayArr);
//											for($i=1 ;$i<=$SizeOfCycle; $i++)
//											{
//												$x .= $this->Get_Checkbox("CycleDay$i", "SelectedCycleDay[]", $i, in_array($i, (array)$StoredValueForBack['SelectedCycleDay']), 'CycleDay', '', $Onclick='', $Disabled='');
//												$x .= $CycleDayArr[$i]."&nbsp;";
//											}
//										$x .= '</div>'."\n";
//									}	
									
									
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr valign="top">'."\n";
								$x .= '<td>'.$eEnrollment['add_activity']['act_time'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div>'."\n";
										$x .= $this->Get_Time_Selection_Box('globalPeriodicHourStart', 'hour', 0);
										$x .= ' : '; 
										$x .= $this->Get_Time_Selection_Box('globalPeriodicMinStart', 'min', 0, $others_tab='', $interval=5);
										$x .= ' '.$eEnrollment['to'].' ';
										$x .= $this->Get_Time_Selection_Box('globalPeriodicHourEnd', 'hour', 0);
										$x .= ' : ';
										$x .= $this->Get_Time_Selection_Box('globalPeriodicMinEnd', 'min', 0, $others_tab='', $interval=5);
									$x .= '</div>'."\n";
									$x .= $this->Get_Form_Warning_Msg('periodicDate_global_invalidTimeRangeWarningDiv', $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr valign="top">'."\n";
								$x .= '<td>'.$Lang['eEnrolment']['excludeHoliday'].'</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div>'."\n";
										$x .= $this->Get_Checkbox("excludeHoliday", "excludeHoliday", 1);
									$x .= '</div>'."\n";
									$x .= $this->Get_Form_Warning_Msg('periodicDate_global_invalidTimeRangeWarningDiv', $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr valign="top">'."\n";
								$x .= '<td>&nbsp;</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div>'."\n";
										$x .= $this->GET_SMALL_BTN($Lang['Btn']['Add'], 'button', 'addPeriodicDate();');
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>';
			$x .= '</tr>';
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Attendance_Display($Status) {
		global $Lang;
		include_once("libclubsenrol.php");
		$libenroll = new libclubsenrol();
		
		$statusText = '';
		/*
		if($libenroll->enableAttendanceLateStatusRight()){
			switch ($Status) {
				case ENROL_ATTENDANCE_PRESENT:
					$statusText = $Lang['eEnrolment']['Attendance']['Present'];
					break;
				case ENROL_ATTENDANCE_ABSENT:
					$statusText = $Lang['eEnrolment']['Attendance']['Absent'];
					break;
				case ENROL_ATTENDANCE_EXEMPT:
					$statusText = $Lang['eEnrolment']['Attendance']['Exempt'];
					break;
				case ENROL_ATTENDANCE_LATE:
					$statusText = $Lang['eEnrolment']['Attendance']['Late'];
				break;
				default:
					$statusText = $Lang['General']['EmptySymbol'];
			}
		}
		else{
			switch ($Status) {
				case ENROL_ATTENDANCE_PRESENT:
					$statusText = $Lang['eEnrolment']['Attendance']['Present'];
					break;
				case ENROL_ATTENDANCE_ABSENT:
					$statusText = $Lang['eEnrolment']['Attendance']['Absent'];
					break;
				case ENROL_ATTENDANCE_EXEMPT:
					$statusText = $Lang['eEnrolment']['Attendance']['Exempt'];
					break;
				default:
					$statusText = $Lang['General']['EmptySymbol'];
			}
		}*/
		switch ($Status) {
			case ENROL_ATTENDANCE_PRESENT:
				$statusText = $Lang['eEnrolment']['Attendance']['Present'];
				break;
			case ENROL_ATTENDANCE_ABSENT:
				$statusText = $Lang['eEnrolment']['Attendance']['Absent'];
				break;
			case ENROL_ATTENDANCE_EXEMPT:
				$statusText = $Lang['eEnrolment']['Attendance']['Exempt'];
				break;
			default:
				if ($libenroll->enableAttendanceLateStatusRight() && $Status == ENROL_ATTENDANCE_LATE) {
					$statusText = $Lang['eEnrolment']['Attendance']['Late'];
				} else if ($libenroll->enableAttendanceEarlyLeaveStatusRight() && $Status == ENROL_ATTENDANCE_EARLY_LEAVE) {
					$statusText = $Lang['eEnrolment']['Attendance']['EarlyLeave'];
				} else {
					$statusText = $Lang['General']['EmptySymbol'];
				}
		}
		
		return $statusText;
	}
	
	function Get_Enrollment_Application_Quota_Settings_Table($RecordType, $CategoryID='', $ViewMode=false, $QuotaTypeAry='', $IncludeYearTermIdAry='', $WithQuotaTypeCol=true, $StudentID='') {
		global $PATH_WRT_ROOT, $Lang, $eEnrollment, $libenroll, $i_ClubsEnrollment_NoLimit, $sys_custom;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$libfcm = new form_class_manage();
		
		$termAry = $libfcm->Get_Academic_Year_Term_List(Get_Current_Academic_Year_ID());
		$numOfTerm = count($termAry);
		$termAry[$numOfTerm]['YearTermID'] = 0;
		$numOfTerm = count($termAry);
		
		$includeYearTermNumAry = array();
		if ($IncludeYearTermIdAry !== '') {
			if (!is_array($IncludeYearTermIdAry)) {
				$IncludeYearTermIdAry = array($IncludeYearTermIdAry);
			}
			$numOfIncludeTerm = count($IncludeYearTermIdAry);
			
			for ($i=0; $i<$numOfIncludeTerm; $i++) {
				$_yearTermId = $IncludeYearTermIdAry[$i];
				
				$_termObj = new academic_year_term($_yearTermId);
				$includeYearTermNumAry[] = $_termObj->Get_Term_Number();
			}
		}
		
		$quotaSettingsAssoAry = array();
		if ($StudentID != '' ){
			$tempTermNumAry = array();
			for ($i=0; $i<$numOfTerm; $i++) {
				$tempTermNumAry[] = $i;
			}
			$studentQuotaAry = $libenroll->getMinMaxOfStudent($StudentID, $RecordType, $CategoryID, $tempTermNumAry);
			//debug_pr($studentQuotaAry);
			foreach((array)$studentQuotaAry as $_termNum => $_quotaInfoAry) {
				foreach((array)$_quotaInfoAry as $__settingsName => $__settingsValue) {
					$quotaSettingsAssoAry[$__settingsName][$_termNum] = $__settingsValue;
				}
			}
		}
		else {
			$quotaSettingsAry = $libenroll->Get_Application_Quota_Settings($RecordType, $CategoryID);
			$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);
		}
		
		if (isset($quotaSettingsAssoAry['ApplyMin'][-1])) {
			$x = $quotaSettingsAssoAry['ApplyMin'][-1];		// form settings
		}
		else {
			$quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();
			$numOfQuotaType = count($quotaTypeAry);
			
			$termAry = $libfcm->Get_Academic_Year_Term_List(Get_Current_Academic_Year_ID());
			$numOfTerm = count($termAry);
			$termAry[$numOfTerm]['YearTermID'] = 0;
			$numOfTerm = count($termAry);
			
			$widthTitle = 45;
			$widthTerm = floor((100 - $widthTitle) / ($numOfTerm + 1));
			
			
			$min_numbers = array(0,1,2,3,4,5,6,7,8,9);
			$max_numbers = array(array(0, $i_ClubsEnrollment_NoLimit));
			$enrollSelectionMaxNumber = $sys_custom['eEnrolment']['EnrollSelectionMaxNumber']?$sys_custom['eEnrolment']['EnrollSelectionMaxNumber']:9;
			for ($i=1; $i<=$enrollSelectionMaxNumber; $i++) {
			     $max_numbers[] = array($i,$i);
			}
			
			$x = '';
			$x .= '<table class="common_table_list_v30">'."\n";
				$x .= '<thead>';
					$x .= '<tr>';
					
						if ($WithQuotaTypeCol) {
							$x .= '<th style="width:'.$widthTitle.'%;">&nbsp;</th>';
						}
						
						for ($i=0; $i<$numOfTerm; $i++) {
							$_yearTermId = $termAry[$i]['YearTermID'];
							$_yearTermNameEn = $termAry[$i]['YearTermNameEN'];
							$_yearTermNameCh = $termAry[$i]['YearTermNameB5'];
							
							$_termObj = new academic_year_term($_yearTermId);
							$_yearTermNum = $_termObj->Get_Term_Number();
							
							if ($_yearTermId > 0 && count($includeYearTermNumAry) > 0 && !in_array($_yearTermNum, $includeYearTermNumAry)) {
								continue;
							}
							
							if ($_yearTermId == 0) {
								$_termName = $Lang['eEnrolment']['WholeYearClub'];
								$_class = 'wholeYearCol';
							}
							else {
								$_termName = Get_Lang_Selection($_yearTermNameCh, $_yearTermNameEn).' '.$Lang['eEnrolment']['Club'];
								$_class = 'termCol';
							}
							
							$x .= '<th class="'.$_class.'" style="width:'.$widthTerm.'%;"><span id="quotaSettingsTermNameSpan_'.$_yearTermId.'">'.$_termName.'</span></th>';
						}
					$x .= '</tr>';
				$x .= '</thead>';
				
				$x .= '<tbody>';
					for ($i=0; $i<$numOfQuotaType; $i++) {
						$_quotaType = $quotaTypeAry[$i];
						
						if ($QuotaTypeAry != '' && !in_array($_quotaType, (array)$QuotaTypeAry)) {
							continue;
						}
						
						if ($_quotaType == 'ApplyMin') {
							$_title = $Lang['eEnrolment']['MinStudentApply'];
						}
						else if ($_quotaType == 'ApplyMax') {
							$_title = $Lang['eEnrolment']['MaxStudentApply'];
						}
						else if ($_quotaType == 'EnrollMax') {
							$_title = $eEnrollment['default_enroll_max_club'];
						}
						else if ($_quotaType == 'EnrollMin') {
							$_title = $Lang['eEnrolment']['MinStudentEnrol'];
						}
						
						$x .= '<tr>';
							if ($WithQuotaTypeCol) {
								$x .= '<td>'.$_title.'</td>';
							}
							for ($j=0; $j<$numOfTerm; $j++) {
								$__yearTermId = $termAry[$j]['YearTermID'];
								
								$__termObj = new academic_year_term($__yearTermId);
								$__yearTermNum = $__termObj->Get_Term_Number();
								if ($__yearTermId > 0 && count($includeYearTermNumAry) > 0 && !in_array($__yearTermNum, $includeYearTermNumAry)) {
									continue;
								}
								
								if ($__yearTermId == 0) {
									$__termNum = 0;
									$__colClass = 'wholeYearCol';
								}
								else {
									$__termNum = $j + 1;
									$__colClass = 'termCol';
								}
								$__quotaTypeValue = $quotaSettingsAssoAry[$_quotaType][$__termNum];
								
								if ($ViewMode) {
									$__display = $__quotaTypeValue;
								}
								else {
									$__id = $_quotaType.'_'.$__termNum.'_'.$CategoryID.'_Sel';
									$__name = 'appQuotaAry['.$_quotaType.']['.$__termNum.']['.$CategoryID.']';
									$__class = $_quotaType.'Sel';
									
//									if ($_quotaType == 'ApplyMin' || $_quotaType == 'EnrollMin') {
//										$__display = getSelectByValue($min_numbers, 'id="'.$__id.'" name="'.$__name.'" class="'.$__class.'"', $__quotaTypeValue);
//									}
//									else {
										$__display = getSelectByArray($max_numbers, 'id="'.$__id.'" name="'.$__name.'" class="'.$__class.'"', $__quotaTypeValue);
//									}
								}
								$x .= '<td class="'.$__colClass.'">'.$__display.'</td>';
							}
						$x .= '</tr>';	
					}
				$x .= '</tbody>';
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	// Get_Enrollment_Application_Quota_Settings_Table_Round() is for SIS customization
	function Get_Enrollment_Application_Quota_Settings_Table_Round($RecordType, $CategoryID='', $ViewMode=false, $QuotaTypeAry='', $IncludeYearTermIdAry='', $WithQuotaTypeCol=true, $StudentID='') {
		global $PATH_WRT_ROOT, $Lang, $eEnrollment, $libenroll, $i_ClubsEnrollment_NoLimit, $sis_eEnrollmentConfig;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$libfcm = new form_class_manage();
		
		$includeYearTermNumAry = array();
		if ($IncludeYearTermIdAry !== '') {
			if (!is_array($IncludeYearTermIdAry)) {
				$IncludeYearTermIdAry = array($IncludeYearTermIdAry);
			}
			$numOfIncludeTerm = count($IncludeYearTermIdAry);
			
			for ($i=0; $i<$numOfIncludeTerm; $i++) {
				$_yearTermId = $IncludeYearTermIdAry[$i];
				
				$_termObj = new academic_year_term($_yearTermId);
				$includeYearTermNumAry[] = $_termObj->Get_Term_Number();
			}
		}
		
		$quotaSettingsAssoAry = array();
		if ($StudentID != '' ){
			$tempTermNumAry = array();
			for ($i=0; $i<$numOfTerm; $i++) {
				$tempTermNumAry[] = $i;
			}
			$studentQuotaAry = $libenroll->getMinMaxOfStudent($StudentID, $RecordType, $CategoryID, $tempTermNumAry);
			foreach((array)$studentQuotaAry as $_termNum => $_quotaInfoAry) {
				foreach((array)$_quotaInfoAry as $__settingsName => $__settingsValue) {
					$quotaSettingsAssoAry[$__settingsName][$_termNum] = $__settingsValue;
				}
			}
		}
		else {
			$quotaSettingsAry = $libenroll->Get_Application_Quota_Settings($RecordType, $CategoryID);
			$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('SettingName', 'TermNumber'), $IncludedDBField=array('SettingValue'), $SingleValue=1);
		}
		
		if (isset($quotaSettingsAssoAry['ApplyMin'][-1])) {
			$x = $quotaSettingsAssoAry['ApplyMin'][-1];		// form settings
		}
		else {
			$quotaTypeAry = $libenroll->Get_Application_Quota_Settings_Type();
			$numOfQuotaType = count($quotaTypeAry);
			
			$numOfTerm = $sis_eEnrollmentConfig['totalRound'];
			
			$widthTitle = 45;
			$widthTerm = floor((100 - $widthTitle) / ($numOfTerm + 1));
			
			
			$min_numbers = array(0,1,2,3,4,5,6,7,8,9);
			$max_numbers = array(array(0, $i_ClubsEnrollment_NoLimit));
			for ($i=1; $i<10; $i++) {
			     $max_numbers[] = array($i,$i);
			}
			
			$x = '';
			$x .= '<table class="common_table_list_v30">'."\n";
				$x .= '<thead>';
					$x .= '<tr>';
					
						if ($WithQuotaTypeCol) {
							$x .= '<th style="width:'.$widthTitle.'%;">&nbsp;</th>';
						}
						
						for ($i=0; $i<$numOfTerm; $i++) {
							$_yearTermId = $i+1;
							
							
								$_termName = $Lang['SIS_eEnrollment']['editCCA']['Round'][$i+1];
								$_class = 'termCol';
							
							$x .= '<th class="'.$_class.'" style="width:'.$widthTerm.'%;">'.$_termName.'</th>';
						}
					$x .= '</tr>';
				$x .= '</thead>';
				
				$x .= '<tbody>';
					for ($i=0; $i<$numOfQuotaType; $i++) {
						$_quotaType = $quotaTypeAry[$i];
						
						if ($QuotaTypeAry != '' && !in_array($_quotaType, (array)$QuotaTypeAry)) {
							continue;
						}
						
						if ($_quotaType == 'ApplyMin') {
							$_title = $Lang['eEnrolment']['MinStudentApply'];
						}
						else if ($_quotaType == 'ApplyMax') {
							$_title = $Lang['eEnrolment']['MaxStudentApply'];
						}
						else if ($_quotaType == 'EnrollMax') {
							$_title = $eEnrollment['default_enroll_max_club'];
						}
						
						$x .= '<tr>';
							if ($WithQuotaTypeCol) {
								$x .= '<td>'.$_title.'</td>';
							}
							for ($j=0; $j<$numOfTerm; $j++) {
								$__yearTermId = $j;
								
								
									$__termNum = $j + 1;
									$__colClass = 'termCol';
									
								$__quotaTypeValue = $quotaSettingsAssoAry[$_quotaType][$__termNum];
								
								if ($ViewMode) {
									$__display = $__quotaTypeValue;
								}
								else {
									$__id = $_quotaType.'_'.$__termNum.'_'.$CategoryID.'_Sel';
									$__name = 'appQuotaAry['.$_quotaType.']['.$__termNum.']['.$CategoryID.']';
									$__class = $_quotaType.'Sel';
									
									if ($_quotaType == 'ApplyMin') {
										$__display = getSelectByValue($min_numbers, 'id="'.$__id.'" name="'.$__name.'" class="'.$__class.'"', $__quotaTypeValue);
									}
									else {
										$__display = getSelectByArray($max_numbers, 'id="'.$__id.'" name="'.$__name.'" class="'.$__class.'"', $__quotaTypeValue);
									}
								}
								$x .= '<td class="'.$__colClass.'">'.$__display.'</td>';
							}
						$x .= '</tr>';	
					}
				$x .= '</tbody>';
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	function Get_Merit_Record_Display_Table($studentID='',$enrolGroupID='',$meritDataArr=''){
		global $PATH_WRT_ROOT, $Lang, $eDiscipline;
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		
		$ldiscipline = new libdisciplinev12();
		$libuser = new libuser();
		$linterface = new interface_html("popup.html");
		
		# Get Student Name
		$userNameInfoArr = $libuser->getNameWithClassNumber($studentID);

		# Get Existing Merit Record
	//	$meritRecordArr = $libuser->Get_Merit_Record($studentID,$enrolGroupID);
		$numOfMeritRecordArr = count($meritRecordArr);
		
		
		// Couter, please amend later
		$meritAppendCounter = 0;

		# Merit Count selection
		$select_merit_num = '<SELECT name="MeritNum" id="MeritNum">';
		$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
		for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
		{
		     $select_merit_num .= '<OPTION value="'.$i.'">'.$i.'</OPTION>';
		}
		$select_merit_num .= '</SELECT>';
		
		$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
		$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);


		# Merit Main Type Selection 
		$ParData =array();
		$ParData[]= array('-1', $Lang['eDiscipline']['sdbnsm_award_B']); 
		$ParData[]= array('1', $Lang['eDiscipline']['sdbnsm_punish_A']);
		$ParTags = 'class="mainMeritType" name="mainMeritType"';
		$meritTypeSelection = $linterface->GET_SELECTION_BOX($ParData, $ParTags, $ParDefault, $ParSelected="");
		
		# Sub Type Selection
		$meritParTags = 'class="subMeritType" name="subMeritType"';
		$meritTypeMeritSelection =  $linterface->GET_SELECTION_BOX($merit_record_type, $meritParTags, $meritParDefault, $meritParSelected="");
		$meritTypeDemeritSelection =  $linterface->GET_SELECTION_BOX($demerit_record_type, $demeritParTags, $demeritParDefault, $demeritParSelected="");
		
		# For Append Merit Options
		$meritAdd = $meritTypeSelection.$select_merit_num.$meritTypeMeritSelection . '<br />';
//		$meritAdd .= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "js_Submit_Append_Item();", "", "", "", "formsmallbuttonon");
//		$meritAdd .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "", "", "", "", "formsmallbuttonon");
//		$meritAdd .= '<br />';
		
		for($i=0;$i<$numOfMeritRecordArr;$i++){
			$thisSubMeritType = $meritRecordArr['MeritType'];
			
			if($thisSubMeritType>0){		
				$thisMainMeritType = '1';						
			}else{
				$thisMainMeritType = '-1';	
			}
			
			
			$exitingMeridRecordDisplay = '';
			$exitingMeridRecordDisplay .= '';
			
		
		}
		
						
		# Display Table
		$meritFormDisplay = '';
		$meritFormDisplay .= '<table  class="form_table_v30" width="100%" align="center" border="0">';
			
			$meritFormDisplay .= '<tr >';
			$meritFormDisplay .= '<td class="field_title">' . $Lang['AccountMgmt']['StudentName'] . '</td>';
			$meritFormDisplay .= '<td>' . $userNameInfoArr . '</td>';
			$meritFormDisplay .= '</tr>';
			
			$meritFormDisplay .= '<tr >';
			$meritFormDisplay .= '<td class="field_title" width="10%">' . $eDiscipline['MeritDemerit']. '</td>';
			
			$meritFormDisplay .= '<td>';
			$meritFormDisplay .= '<div style="display:none;">';
			$meritFormDisplay .= '<div id="meritAppendItem">' . $meritAdd . '</div>';
			$meritFormDisplay .= '</div>';
			$meritFormDisplay .= '<div id="meritAppendLayer"></div>';
			$meritFormDisplay .= '<div class="table_row_tool row_content_tool">';
			$meritFormDisplay .= '<a href="#" onclick="Add_Form_Row(); return false;" class="add_dim" title="'.$Lang['SysMgr']['FormClassMapping']['NewForm'].'"></a>';
			$meritFormDisplay .= '</div>';
			$meritFormDisplay .= '<input type="hidden" id="meritAppendCounter" value="'.$meritAppendCounter.'"></input>';
			
			$meritFormDisplay .= '</td>';
			
			$meritFormDisplay .= '</tr>';
		
		
		$meritFormDisplay .= '</table>';
	
	 	return $meritFormDisplay;
	}
	
	function Get_Ole_Category_Selection($id, $name, $selectedCateId) {
		global $linterface, $Lang, $libenroll;
		
		$file_array = $libenroll->Get_Ole_Category_Array();
		$file_array = array_merge(array(array("", Get_Selection_First_Title($Lang['General']['PleaseSelect']),"")), $file_array);

		return $linterface->GET_SELECTION_BOX($file_array, " id='".$id."' name='".$name."'", "", $selectedCateId);
	}
	
	function Get_Category_Type_Selection($id, $name, $selectedCateId) {
		global $linterface, $Lang, $libenroll;

		$file_array = $libenroll->Get_CategoryType_Array();
		$file_array = array_merge(array(array("", Get_Selection_First_Title($Lang['General']['PleaseSelect']),"")), $file_array);

		return $linterface->GET_SELECTION_BOX($file_array, " id='".$id."' name='".$name."'", "", $selectedCateId);
	}

	function Get_Member_Approve_By_Selection($recordType, $id, $name, $selectedType, $onChange='') {
		global $linterface, $Lang, $enrolConfigAry;
		
		$selAry = array();
		if ($recordType==$enrolConfigAry['Club']) {
			$selAry[0] = array('', $Lang['eEnrolment']['AllMembers']);
			$selAry[1] = array(1, $Lang['eEnrolment']['MembersApprovedByYou']);
		}
		else if ($recordType==$enrolConfigAry['Activity']) {
			$selAry[0] = array('', $Lang['eEnrolment']['AllParticipants']);
			$selAry[1] = array(1, $Lang['eEnrolment']['ParticipantsApprovedByYou']);
		}
		
		$onchange = ' onchange="'.$onChange.'" ';
		$attr = " id='".$id."' name='".$name."'".$onchange;
		
		return $linterface->GET_SELECTION_BOX($selAry, $attr, "", $selectedType);
	}
	
	function Get_Achievement_Comment_Bank_DBTable($Keyword='', $PageNumber='', $PageSize='', $Order='', $SortField='', $RecordType='')
	{

		
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $image_path, $libenroll;
		global $ck_eEnrol_Setting_Achievement_Comment_Bank_page_size, $ck_eEnrol_Setting_Achievement_Comment_Bank_page_number;
		global $ck_eEnrol_Setting_Achievement_Comment_Bank_page_order, $ck_eEnrol_Setting_Achievement_Comment_Bank_page_field;
		
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libinterface.php");

		$linterface = new interface_html();
		$Keyword = trim($Keyword);
		
		
		if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_size) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_size!=''){
			$PageSize = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_size;
		}
		
		if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_number) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_number!=''){
			$PageNumber = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_number;
		}
		
		if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_order) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_order!=''){
			$Order = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_order;
		}
		
		if(isset($ck_eEnrol_Setting_Achievement_Comment_Bank_page_field) && $ck_eEnrol_Setting_Achievement_Comment_Bank_page_field!=''){
			$SortField = $ck_eEnrol_Setting_Achievement_Comment_Bank_page_field;
		}
		
		# Default Table Settings
		$PageNumber = ($PageNumber == '')? 1 : $PageNumber;
		$PageSize = ($PageSize == '')? 20 : $PageSize;
		$Order = ($Order == '')? 1 : $Order;
		$SortField = ($SortField == '')? 0 : $SortField;
		
		
		
		# TABLE INFO
		$libdbtable = new libdbtable2007($SortField, $Order, $PageNumber);
		
		
		$libdbtable->sql = $libenroll->Get_Achievement_Comment_Sql('','', $RecordType,$Keyword);
		
		
		$libdbtable->IsColOff =  "IP25_table";
		
		
		$libdbtable->field_array = array("CommentCode", "Comment",
										 "CheckBox", "WebSAMS"
										 );
		
		$libdbtable->no_col = sizeof($libdbtable->field_array) + 2;		// 2 means column #, Checkbox
		$libdbtable->title = "";
		$libdbtable->column_array = array(0,18,0);
		$libdbtable->wrap_array = array(0,0,0);
		
		$libdbtable->page_size = $PageSize;
		$libdbtable->fieldorder2 = " ,CommentCodeDisplay, CommentDisplay";
		
		
		# Table Action Button
		$BtnArr = array();
		$BtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'CommentID[]\',\'new.php\');');
		$BtnArr[] = array('delete', 'javascript:checkRemove(document.form1,\'CommentID[]\',\'remove.php\');');
		$table_tool = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);
	
		
		# Table Column
		$pos = 0;
		
		$libdbtable->column_list .= "<th width='4%' class=''>#</td>\n";
		if($RecordType=='P'){
			$libdbtable->column_list .= "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['General']['Code'])."</td>\n";
			$libdbtable->column_list .= "<th width='35%'>".$libdbtable->column_IP25($pos++, $Lang['Group']['Comment'])."</td>\n";
			$libdbtable->column_list .= "<th width='35%'>".$libdbtable->column_IP25($pos++, $Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'])."</td>\n";
		}else{
			$libdbtable->column_list .= "<th width='20%'>".$libdbtable->column_IP25($pos++, $Lang['General']['Code'])."</td>\n";
			$libdbtable->column_list .= "<th width='70%'>".$libdbtable->column_IP25($pos++, $Lang['Group']['Comment'])."</td>\n";
		}
		$libdbtable->column_list .= "<th class='tableTitle'>".$libdbtable->check("CommentID[]")."</th>\n";
		$libdbtable->no_col = $pos + 2;
		
		$DBTable = '';
		$DBTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
			$DBTable .= '<tr>'."\n";
				$DBTable .= '	<td align="right" valign="bottom">'.$table_tool.'</td>'."\n";
			$DBTable .= '</tr>'."\n";
			$DBTable .= '<tr>'."\n";
				$DBTable .= '	<td align="center" valign="top">'.$libdbtable->display().'</td>'."\n";
			$DBTable .= '</tr>'."\n";
		$DBTable .= '</table>'."\n";
		
	
		
		$DBTable .= '<input type="hidden" name="pageNo" value="'.$libdbtable->pageNo.'" />';
		$DBTable .= '<input type="hidden" name="order" value="'.$libdbtable->order.'" />';
		$DBTable .= '<input type="hidden" name="field" value="'.$libdbtable->field.'" />';
		$DBTable .= '<input type="hidden" name="page_size_change" value="" />';
		$DBTable .= '<input type="hidden" name="numPerPage" value="'.$libdbtable->page_size.'" />';
		return $DBTable;
	}
	
	
	function Get_Activity_Report_Option_Table($sel_category, $Period, $AcademicYearID, $formType=''){
		global $PATH_WRT_ROOT, $Lang, $eEnrollment, $button_select_all, $i_From, $i_To, $textFromDate, $textToDate, $i_Discipline_Generate_Update_Statistic;
		global $i_Discipline_Form, $i_Discipline_Class, $i_UserStudentName;
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
		
		$linterface = new interface_html();
		$libenroll = new libclubsenrol();
		
		# Get Current Academic Year
		$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
		
		# School year
		$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
		$years = $libenroll->returnAllYearsSelectionArray();
		$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);
		
		# Semester
		$currentSemester = getCurrentSemester();
		$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
		$selectSemesterHTML .= "</select>";
		
		# Category Selection
		$category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "getActivityCategory()", $button_select_all);
		$activityCategoryHTMLBtn .= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('activityCategory'));return false;", "selectAllBtnActCat");
				
		$optionTable = '';
		
		$optionTable .= '<form name="form1" method="post" action="print.php" target="_blank" onSubmit="return checkForm();">';
			$optionTable .= '<table class="form_table_v30">';
	 		if($formType!='ApplicationForm'){
				// Target
				$optionTable .= '<tr valign="top">';
				$optionTable .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]. '</td>';
				$optionTable .= '<td>';
				$optionTable .= '<table class="inside_form_table">';
				$optionTable .= '<tr>';
				$optionTable .= '<td valign="top">';
				$optionTable .= '<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,\'\')">';
				// '". if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";}. "'
				$optionTable .= '<option value="form">'  . $i_Discipline_Form .'</option>';
				$optionTable .= '<option value="class">' . $i_Discipline_Class .'</option>';
				
				if($formType=='ParentNotice'){
					$optionTable .= '<option value="student">' . $i_UserStudentName . '</option>';
				}
				
				$optionTable .= '</select>';
				$optionTable .= '</td>';
	 		
	 		
				$optionTable .= '<td valign="top" nowrap>';
		 		
				// Form / Class 
				$optionTable .= '<span id="rankTargetDetail">';
				$optionTable .= '<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="7"></select>';
				$optionTable .= '</span>';
				$optionTable .=  $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01");
												
				// Student 
				$optionTable .= '<span id="spanStudent" style="">';
				$optionTable .= '<select name="studentID[]" multiple size="7" id="studentID[]"></select>';
				$optionTable .= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;");
				$optionTable .= '</span>';
				$optionTable .= '</td>';
				$optionTable .= '</tr>';
				$optionTable .= '<tr><td colspan="3" class="tabletextremark">'.$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"].'</td></tr>';
				$optionTable .= '</table>';
				$optionTable .= '<span id="div_Target_err_msg"></span>';
				$optionTable .= '</td>';
				$optionTable .= '</tr>';
	 		}

	 		$optionTable .= '<tr valign="top">';
	 		
			$optionTable .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"].'</td>';
			$optionTable .= '<td>';
			$optionTable .= '<table class="inside_form_table">';
			$optionTable .= '<tr>';
			$optionTable .= '<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true; getActivityCategory();">';
			$optionTable .= '<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" '.($Period!="DATE"?" checked":"") .'  onClick="getActivityCategory();">';
			$optionTable .= $Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"];
			$optionTable .= $selectSchoolYearHTML;
			$optionTable .= $Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"];
			$optionTable .= '<span id="spanSemester">'.$selectSemesterHTML.'</span>';
			$optionTable .= '</td>';
			$optionTable .= '</tr>';
			$optionTable .= '<tr>';
			$optionTable .= '<td onClick="document.form1.radioPeriod_Date.checked=true; getActivityCategory();"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" '.($Period=="DATE"?" checked":"") .' onClick="getActivityCategory();">'.$i_From. '</td>';
			$optionTable .= '<td onClick="changeRadioSelection(\'DATE\')" onFocus="changeRadioSelection(\'DATE\')">' . $linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='getActivityCategory();' ");
			$optionTable .= '<br><span id=\'div_DateEnd_err_msg\'></span></td>';
			$optionTable .= '<td>'. $i_To . '</td>';
			$optionTable .= '<td onClick="changeRadioSelection(\'DATE\')" onFocus="changeRadioSelection(\'DATE\')">' . $linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='getActivityCategory();' ");
			$optionTable .= '</tr>';
			$optionTable .= '</table>';
			$optionTable .= '</td>';
			$optionTable .= '</tr>';
			
			# Category 
			$optionTable .= '<tr>';
			$optionTable .= '<td class="field_title"><span class="tabletextrequire">*</span>'. $eEnrollment['add_activity']['act_category'].'</td>';
			$optionTable .= '<td>';
			$optionTable .= '<table class="inside_form_table" width="100%">';
			$optionTable .= '<tr>';
			$optionTable .= '<td>' . $category_selection . '</td>';
			$optionTable .= '</tr>';
			$optionTable .= '<tr>';
			$optionTable .= '<td>';
			$optionTable .= '<span id="CategoryElements"></span>' . $activityCategoryHTMLBtn;
			$optionTable .= '</td>';
			$optionTable .= '</tr>';
			$optionTable .= '</table>';
			$optionTable .= '<span id=\'div_Category_err_msg\'></span>';
			$optionTable .= '</td>';
			$optionTable .= '</tr>';
			
//			# Sort By 
//			$optionTable .= '<tr>';
//			$optionTable .= '<td class="field_title">' . $Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"] . '</td>';
//			$optionTable .= '<td>';
//			$optionTable .= $linterface->Get_Radio_Button("sortByClassNameNumber", "sortBy", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"] . " / " . $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"]);
//			$optionTable .= '<br />';
//			$optionTable .= $linterface->Get_Radio_Button("sortByOrderAsc", "sortByOrder", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["AscendingOrder"]);
//			$optionTable .= $linterface->Get_Radio_Button("sortByOrderDesc", "sortByOrder", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["DescendingOrder"]);				
//			$optionTable .= '</td>';
//			$optionTable .= '</tr>';
			$optionTable .= '</table>';
			$optionTable .= $linterface->MandatoryField();
			$optionTable .= '<div class="edit_bottom_v30">';
			$optionTable .= '<p class="spacer"></p>';
			$optionTable .=  $linterface->GET_ACTION_BTN($Lang['Btn']['Generate'] , "submit");
			$optionTable .= '<p class="spacer"></p>';
			$optionTable .= '</div>';
			$optionTable .= '<input type="hidden" name="studentFlag" id="studentFlag" value="0">';
		$optionTable .= '</form>';
		
		return $optionTable;
	}
	
	function Get_Activity_Student_List_Report($enrolEventIDArr, $reportType, $SQL_startdate, $SQL_enddate, $rankTarget, $rankTargetDetail, $sel_category, $selectYear)
	{
		global $PATH_WRT_ROOT, $Lang;
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
		$libuser = new libuser();
		$libenroll = new libclubsenrol();
		
		// $Lang['General']['EmptySymbol']
		
		$formIDArr = array();
		$studentIDArr = array();
		
		if($rankTarget == 'form'){
			$formIDArr = $rankTargetDetail;
		}elseif($rankTarget =='class'){
			$studentIDArr = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
		}elseif($rankTarget =='student'){
			$studentIDArr = $rankTargetDetail;
		}
		
		# Get Corresponding Activity Event Date
		$thisActivityDateInfoArr = $libenroll->GET_ENROL_EVENT_DATE($enrolEventIDArr, $SQL_startdate,$SQL_enddate);
		$thisActivityDateInfoIDArrAssoc = BuildMultiKeyAssoc($thisActivityDateInfoArr, array('EnrolEventID','EventDateID'));
		$thisActivityDateInfoIDArr = array_keys($thisActivityDateInfoIDArrAssoc);	
					
		# Get ALL Activity Info
		$activityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($thisActivityDateInfoIDArr,'',$selectYear,0,1,1,0,$formIDArr,'',true,$studentIDArr, $sel_category);
		//$activityInfoAssoArr = BuildMultiKeyAssoc($activityInfoArr, 'EnrolEventID');
								
		#### Other OLE Settings
//		$oleSettingsArr = $libenroll->Get_Enrolment_Default_OLE_Setting('activity', $thisActivityDateInfoIDArr);
//		debug_pr($oleSettingsArr);
//		$a = $libenroll->Get_Ole_Category_Array();
//		debug_pr($a); 
		
		$data_ary = array();
		$display = "";
		
		##### Loop Each Event Date's Activity
		foreach ((array)$activityInfoArr as $thisActivityID => $thisActivityInfo){
							
			#### Activity Info
			$thisActivityID = $thisActivityInfo['EnrolEventID'];
			
			$thisActivityName = $thisActivityInfo['EventTitle']? $thisActivityInfo['EventTitle']: $Lang['General']['EmptySymbol'];
			$thisActivityContent = removeHTMLtags($thisActivityInfo['Description'])? removeHTMLtags($thisActivityInfo['Description']):$Lang['General']['EmptySymbol'];
			$thisGroupId = $thisActivityInfo['EnrolGroupID'];
			
			$thisActivityNature = $thisActivityInfo['SchoolActivity'];				
			$thisActivityNatureDisplay = '';
			if($thisActivityNature=='0'){			
				$thisActivityNatureDisplay = '校外';
			}elseif($thisActivityNature=='1'){
				$thisActivityNatureDisplay = '校內';
			}elseif($thisActivityNature=='2'){
				$thisActivityNatureDisplay = '混合';
			}

			#### Below to Which Club	
			$thisClubInfo = $libenroll->getGroupInfoList('','',(array)$thisGroupId);
			$thisClubTitle = $thisClubInfo[0]['Title']? $thisClubInfo[0]['Title']:$Lang['General']['EmptySymbol'];
			
			#### Staff Info
			$activityStaffInfo = $libenroll->GET_EVENTSTAFF($thisActivityID);
			$numOfActivityStaffInfo = count($activityStaffInfo);
					
			$thisPicIDArr = array();
			$thisHelperIDArr = array();
			for($i=0;$i<$numOfActivityStaffInfo;$i++){
				$thisStaffType = $activityStaffInfo[$i]['StaffType'];	
				if($thisStaffType == 'PIC'){
					$thisPicIDArr[]= $activityStaffInfo[$i]['UserID'];
				}
				elseif($thisStaffType == 'HELPER'){
					$thisHelperIDArr[]= $activityStaffInfo[$i]['UserID'];
				}	
			}
			
			### PIC
			$thisUserInfoArr =  $libuser->returnUser('','',$thisPicIDArr);
			$numOfUserInfoArr = count($thisUserInfoArr);
			$thisUserNameArr = array();
			for($i=0;$i<$numOfUserInfoArr;$i++){
				$thisUserChineseName = $thisUserInfoArr[$i]['ChineseName'];
				$thisUserEnglishName = $thisUserInfoArr[$i]['EnglishName'];
				$thisUserName = $thisUserChineseName?$thisUserChineseName:$thisUserEnglishName;
				$thisUserNameArr[]= $thisUserName;
			}
			// Modified by Bill
			$thisPICArrString = implode("、",$thisUserNameArr)?implode("、",$thisUserNameArr):$Lang['General']['EmptySymbol'];
		
			## Helper
			$thisHelperUserInfoArr =  $libuser->returnUser('','',$thisHelperIDArr);
			$numOfHelperUserInfoArr = count($thisHelperUserInfoArr);
			$thisHelperUserNameArr = array();
			for($i=0;$i<$numOfHelperUserInfoArr;$i++){
				$thisHelperUserChineseName = $thisHelperUserInfoArr[$i]['ChineseName'];
				$thisHelperUserEnglishName = $thisHelperUserInfoArr[$i]['EnglishName'];
				$thisHelperUserName = $thisHelperUserChineseName?$thisHelperUserChineseName:$thisHelperUserEnglishName;
				$thisHelperUserNameArr[]= $thisHelperUserName;
			}
			$thisHelperArrString = implode("、",$thisHelperUserNameArr)? implode("、",$thisHelperUserNameArr):$Lang['General']['EmptySymbol'];
							
			#### Student Who Joined this Activity 
			$thisActivityStudentInfo = $thisActivityInfo['StatusStudentArr'];
			
			#### Payment Amount
			$thisPaymentAmount = $thisActivityInfo['PaymentAmount']?$thisActivityInfo['PaymentAmount']:$Lang['General']['EmptySymbol'];
			
			## Location
			$thisLocation = $thisActivityInfo['Location']?$thisActivityInfo['Location']:$Lang['General']['EmptySymbol'];
	
			## Transportation
			$thisTransportation = $thisActivityInfo['Transport']?$thisActivityInfo['Transport']:$Lang['General']['EmptySymbol'];
	
			## Organizer
			$thisOrganizer = $thisActivityInfo['Organizer']?$thisActivityInfo['Organizer']:$Lang['General']['EmptySymbol'];
	
			## RewardOrCert
			$thisRewardOrCert = $thisActivityInfo['RewardOrCert']?$thisActivityInfo['RewardOrCert']:$Lang['General']['EmptySymbol'];
	
			## OLE 
			$thisOLEInfoArr = $libenroll->Get_Enrolment_Default_OLE_Setting('activity', array($thisActivityID));
		
			include_once("libportfolio.php");
			include_once("libpf-slp.php");
			
			$LibPortfolio = new libpf_slp();
			$DefaultELEArray = $LibPortfolio->GET_ELE(false, 'b5');
			$ELEList = "";
			$ole_component = $thisOLEInfoArr[0]['OLE_Component'];
			$tempELEAry = explode(',',$ole_component);
	
			$thisOLEComponentArray = array();
			$thisOLEComponentString = '';
			foreach($DefaultELEArray as $ELE_ID => $ELE_Name)
			{
				if(in_array($ELE_ID, $tempELEAry)){
					$thisOLEComponentArray[]= $ELE_Name;	
				}
			}
			$thisOLEComponentString = implode(',', $thisOLEComponentArray);
			$thisOLEComponentString = $thisOLEComponentString?$thisOLEComponentString:$Lang['General']['EmptySymbol'];
			
			## Loop Event Date
			$thisEventInfoArr = $thisActivityDateInfoIDArrAssoc[$thisActivityID];	
			
			foreach ((array)$thisEventInfoArr as $thisEventDateID=>$thisEventDateInfo){

				$thisActivityDateStart = $thisEventDateInfo['ActivityDateStart'];
				$thisActivityDateEnd = $thisEventDateInfo['ActivityDateEnd']; 
				
				// Get academic year of each activities
				$academicInfoAry = getAcademicYearAndYearTermByDate($thisActivityDateStart);
				$academicYearId = $academicInfoAry['AcademicYearID'];
				if(!$academicYearObj || $academicYearObj->AcademicYearID != $academicYearId){
					$academicYearObj = new academic_year($academicYearId);
					$academicYearName = $academicYearObj->Get_Academic_Year_Name();
				}
				
				$thisActivityDateStartDateTimeArr = explode(' ', $thisActivityDateStart);
				$thisActivityDateEndDateTimeArr = explode(' ', $thisActivityDateEnd);
				
				if($thisActivityDateStartDateTimeArr[0]==$thisActivityDateEndDateTimeArr[0])
				{
					$thisActivityDate = $thisActivityDateStartDateTimeArr[0];
				}else{
					$thisActivityDate = $thisActivityDateStartDateTimeArr[0] . ' 至 ' . $thisActivityDateEndDateTimeArr[0];	
				}
				
				$thisActivityTime = substr($thisActivityDateStartDateTimeArr[1], 0, 5) . ' 至 ' . substr($thisActivityDateEndDateTimeArr[1], 0, 5);				
				
				$PageBreakIdentifier = '';
				$PageCounter = 0;
				#### Loop Students
				foreach ((array)$thisActivityStudentInfo as $thisRecordStatus=>$thisStudentInfoArr){				
					$RecordCounter = 0; // Record Already Shown		
						//debug_pr($thisStudentInfoArr);
					if($thisRecordStatus=='2'){ // Approved Activity Member
						$numOfStudentInfoArr = count($thisStudentInfoArr);	//Num Of Students of this RecordStatus						
						$fixedDisplayRow = 20;	//Fixed Num Of Row To Display
//						$fixedDisplayRow = ($reportType=='ConfirmationReport')? 20 : 15;						
						$numOfAvaliablePage = $numOfStudentInfoArr/$fixedDisplayRow; // For Page Check						
						$numOfRecordForDisplay = 0; // No. Of Each Row
						
						# Check If Reach The Last Page				
						while($numOfAvaliablePage>0){							
							$PageBreakIdentifier++;		
							$PageCounter++;										
							#######################################  Display Table Start #########################################							
							# Page Break for Printing
							if($PageBreakIdentifier>0)
							{
								$pageBreakCss = "page-break-after: always;";
							}
									
							########   Header Start   ########
							$display .= "<center><table width='1000px' style='" . $pageBreakCss . " border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '>";				
								# Form Number
								$display .= "<tr>";
								if($reportType=='ConfirmationReport'){
									$display .= "<td align='right' style='font-size:16px; colspan='2'>表格編組： N/LWL01(4)</td>";
								}else{
									$display .= "<td align='right' style='font-size:16px; colspan='2'>表格編組： N/LWL01(3)</td>";
								}
								$display .= "</tr>";
								
								# School Name
								$display .= "<tr>";
									$display .= "<td colspan='2' style='line-height:24px;' align='center'>";
									if($reportType=='ConfirmationReport'){
										$display .= " 青松侯寶垣中學 <br />".$academicYearName." <br />培育委員會<br />全方位學習組 <br />校　外　活　動　學　生　名　單　確　認　表<br />";
									}else{
										$display .= " 青松侯寶垣中學 <br />".$academicYearName." <br />培育委員會<br />全方位學習組 <br />校　外　活　動　學　生　名　單<br />";
									}
									$display .= "</td>";
								$display .= "</tr>";
								
								# Page Num Display
								$display .= "<tr>";
									$display .= "<td colspan='2' align='right' style='line-height:25px;float: right; padding-right: 30px;'>";
									$display .= "<span style='display: table-cell; width:800px; '>頁：</span><span style='width:100px; display:table-cell; border-bottom: 1px solid #000; text-align:center'>".$PageCounter."</span>";
									$display .= "</td>";
								$display .= "</tr>";				
							########   Header End   ########	
							
							######## Table Content Start ########	
								$display .= "<tr>";
									$display .= "<td align='center'>";
										$display .= "<table cellspacing='0' cellpadding='3' width='95%' style='border: 1px solid #000; margin: 0 auto; font-size: 22px; font-family: 標楷體;' >";
											$display .= "<tr>";
												$display .= "<td width='50%' rowspan='2' valign='top' style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>";
												$display .= "<span>活動名稱：</span>";
												$display .=  $thisActivityName;	
												$display .= "</td>";
										
												$display .= "<td width='50%' style='border-bottom: 1px solid #000;'>";
												$display .= "<span>負責老師：</span>";
												$display .= $thisPICArrString;
												$display .= "</td>";
											$display .= "</tr>";
											
											$display .= "<tr>";
												$display .= "<td style='border-bottom: 1px solid #000;'>";
												$display .= "<span>協助老師：</span>";
												$display .= $thisHelperArrString;
												$display .= "</td>";
											$display .= "<tr>";
											
											$display .= "<tr>";
												$display .= "<td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>";
												$display .= "<span>性質：</span>";
												$display .= $thisActivityNatureDisplay;	
												$display .= "</td>";
										
												$display .= "<td style='border-bottom: 1px solid #000;'>";
												$display .= "<span>舉辦單位：</span>";
												$display .= $thisOrganizer;
												$display .= "</td>";
											$display .= "</tr>";						
											
											$display .= "<tr>";
												$display .= "<td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>";
												$display .= "<span>日期：</span>";	
												$display .= date("d-m-Y",strtotime($thisActivityDate));
												$display .= "</td>";
										
												$display .= "<td style='border-bottom: 1px solid #000;'>";
												$display .= "<span>時間：</span>";
												$display .= $thisActivityTime;
												$display .= "</td>";
											$display .= "</tr>";	
											
											$display .= "<tr>";
												$display .= "<td style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>";
												$display .= "<span>地區：</span>";	
												$display .= $thisLocation;
												$display .= "</td>";
										
												$display .= "<td style='border-bottom: 1px solid #000;'>";
												$display .= "<span>交通：</span>";
												$display .= $thisTransportation;
												$display .= "</td>";
											$display .= "</tr>";	
											
											$display .= "<tr>";
												$display .= "<td style='border-right: 1px solid #000;' valign='top'>";
												//$display .= "<span>其他學習經歷種類：　德育及公民教育、社會服務、學工作有關的經驗、藝術發展、體育發展（請刪去不適用）</span>";	
												$display .= "<span>其他學習經歷種類：</span>";
												$display .= $thisOLEComponentString;
												$display .= "</td>";
										
												$display .= "<td valign='top' >";
												$display .= "<span>獎項或證書（中英對照）（如有）：</span><br />";
												$display .= $thisRewardOrCert;
												$display .= "</td>";
											$display .= "</tr>";								
																	
										$display .= "</table>";	
									$display .= "</td>";
								$display .= "</tr>";		
								
								$display .= "<tr>";
									$display .= "<td>";
										$display .= "　";
									$display .= "</td>";
								$display .= "<tr>";
								
								$display .= "<tr>";
									$display .= "<td>";
										$display .= "<span>學生名單</span>";
									$display .= "</td>";
								$display .= "<tr>";
											
							    $display .= "<tr>";
								    $display .= "<td align='center'>";	
								    
								    	$display .= "<table width='95%' cellspacing='0' cellpadding='2' style='border: 1px solid #000; margin: 0 auto; font-size: 22px; font-family: 標楷體;' >";	
								     	  $display .= "<tr>";
								     	   	$display .= "<td rowspan='2' width='5%' align='center' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>數目</span>";
								     	   	$display .= "</td>";	
								     	   	$display .= "<td colspan='2' width='35%' align='center' style='border-bottom: 1px solid #000; border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>姓名</span>";
								     	   	$display .= "</td>";	
								     	   	$display .= "<td rowspan='2' align='center' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>性別</span>";
								     	   	$display .= "</td>";	
								     	   	 $display .= "<td rowspan='2' align='center' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>班別</span>";
								     	   	$display .= "</td>";
								     	   	$display .= "<td rowspan='2' align='center' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>班號</span>";
								     	   	$display .= "</td>";
								     	   	// Modified by bill
								     	   	$display .= "<td rowspan='2' align='center' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>緊急聯絡電話</span>";
								     	   	$display .= "</td>";
								     	   	$display .= "<td rowspan='2' align='center' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>學生電話</span>";
								     	   	$display .= "</td>";
								     	   	
								     	   	if($reportType=='ConfirmationReport'){
								     	   		$borderRight = "style='border-right: 1px solid #000;'";
								     	   	}
								     	   	
								     	   	$display .= "<td rowspan='2' align='center' width='12%' $borderRight >";	
								     		   	$display .= "<span>參與角色</span>";
								     	   	$display .= "</td>";
								     	   	
								     	   	if($reportType=='ConfirmationReport'){
									     	   	$display .= "<td rowspan='2' width='15%' align='center' '>";	
									     		   	$display .= "<span>出席者（&#10003;）</span><br />";
									     		   	$display .= "<span>缺席者（&#10007;）</span>";
									     	   	$display .= "</td>";
								     	   	}
								     	  $display .= "</tr>";
								     	  
								     	  $display .= "<tr>";
								     	    $display .= "<td align='center' width='20%' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>英文</span>";
								     	   	$display .= "</td>";
								     	   	$display .= "<td align='center' width='15%' style='border-right: 1px solid #000;'>";	
								     		   	$display .= "<span>中文</span>";
								     	   	$display .= "</td>";
								     	  $display .= "</tr>";
								     	
								     	
								     	//debug_pr($thisStudentInfoArr);
										
										########  Student List Start  ########
									//	$numOfStudentInfoArr = count($thisStudentInfoArr);
										
										$rowCounter = 0;
										
										//debug_pr($RecordCounter . '-' . $numOfStudentInfoArr . '-' . $thisRecordStatus );
										while($RecordCounter<$numOfStudentInfoArr && $rowCounter<$fixedDisplayRow){						
											$rowCounter++;
											$numOfRecordForDisplay++;
											
									 		$thisStudentId = $thisStudentInfoArr[$RecordCounter]['StudentID'];	
									 		$thisStudentChineseName = $thisStudentInfoArr[$RecordCounter]['ChineseName']?$thisStudentInfoArr[$RecordCounter]['ChineseName']:$Lang['General']['EmptySymbol'];
									 		$thisStudentEnglishName = $thisStudentInfoArr[$RecordCounter]['EnglishName']?$thisStudentInfoArr[$RecordCounter]['EnglishName']:$Lang['General']['EmptySymbol'];
											$thisStudentClassName = $thisStudentInfoArr[$RecordCounter]['ClassName']?$thisStudentInfoArr[$RecordCounter]['ClassName']:$Lang['General']['EmptySymbol'];
											$thisStudentClassNum = $thisStudentInfoArr[$RecordCounter]['ClassNumber']?$thisStudentInfoArr[$RecordCounter]['ClassNumber']:$Lang['General']['EmptySymbol'];
											$thisStudentRole = $thisStudentInfoArr[$RecordCounter]['RoleTitle']?$thisStudentInfoArr[$RecordCounter]['RoleTitle']:$Lang['General']['EmptySymbol'];
											$thisStudentGender = $thisStudentInfoArr[$RecordCounter]['Gender']?$thisStudentInfoArr[$RecordCounter]['Gender']:$Lang['General']['EmptySymbol'];
											$thisStudentMobileTelNo = $thisStudentInfoArr[$RecordCounter]['MobileTelNo']?$thisStudentInfoArr[$RecordCounter]['MobileTelNo']:$Lang['General']['EmptySymbol'];
											$thisStudentHomeTelNo = $thisStudentInfoArr[$RecordCounter]['HomeTelNo']?$thisStudentInfoArr[$RecordCounter]['HomeTelNo']:$Lang['General']['EmptySymbol'];
											//$thisStudentHomeTelNo = $thisStudentInfoArr[$RecordCounter]['HomeTelNo'];
																						
										     	  $display .= "<tr>";
										     	    $display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$numOfRecordForDisplay."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='left' style='border-top: 1px solid #000;border-right: 1px solid #000; padding-left:5px;'>";	
										     		   	$display .= "<span>".$thisStudentEnglishName."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$thisStudentChineseName."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$thisStudentGender."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$thisStudentClassName."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$thisStudentClassNum."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$thisStudentHomeTelNo."</span>";
										     	   	$display .= "</td>";
										     	   	// Modified by Bill
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$thisStudentMobileTelNo."</span>";
										     	   	$display .= "</td>";
										     	   	
										     	   	$roleRowCss ='';
										     	   	if($reportType=='ConfirmationReport'){
										     	   		$roleRowCss = "style='border-top: 1px solid #000; border-right: 1px solid #000;'";
										     	   	}else{
										     	   		$roleRowCss = "style='border-top: 1px solid #000;'";
										     	   	}								     	   
										     	   	$display .= "<td align='center' $roleRowCss>";	
										     		   	$display .= "<span>".$thisStudentRole."</span>";
										     	   	$display .= "</td>";
										     	   
										     	   if($reportType=='ConfirmationReport'){
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";	
										     	   }									     	   											     	   	
										     	  $display .= "</tr>";
										    
										    $RecordCounter++;
								     	 }
								     	 
								     	 for($j=$rowCounter;$j<$fixedDisplayRow;$j++){
								     	 	$rowCounter++;
								     	 	$numOfRecordForDisplay++;
								     	 		$display .= "<tr>";
										     	    $display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>".$numOfRecordForDisplay."</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	// Modified by Bill
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;border-right: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	   	
										     	   	if($reportType=='ConfirmationReport'){
											     	   	$display .= "<td align='center' style='border-top: 1px solid #000; border-right: 1px solid #000;'>";	
											     		   	$display .= "<span>　</span>";
											     	   	$display .= "</td>";
										     	   	}
										     	   	
										     	   	$display .= "<td align='center' style='border-top: 1px solid #000;'>";	
										     		   	$display .= "<span>　</span>";
										     	   	$display .= "</td>";
										     	  $display .= "</tr>";
								     	 }
								     	 
								     	########  Student List End  ########
								     	     	 
								    	$display .= "</table>";	
								    $display .= "</td>";		
								$display .= "</tr>";	
											
								$display .= "<tr>";
									$display .= "<td>";
									if($reportType=='ConfirmationReport'){
										$display .= "*請於活動後（一星期內）將本表格交其他學習經歷教學助理，並將檔案儲存在內聯網的指定位置。<br />";
										$display .= "*若活動有頒發獎項及證書，請將正本及此表格交校務處文員處理。 <br />";
										$display .= "*請負責老師校對清楚活動名稱（中英文版）及出席者名單，並於下方簽署確認資料。 <br />";
									}else{
										$display .= "*請於出發前將本表格交校務處職員，並將檔案儲存在內聯網全方位學習組的指定位置。 <br />";
									}
									
									$display .= "</td>";
								$display .= "</tr>";
								
								$display .= "<tr><td>&nbsp;</td></tr>";
								
								$display .= "<tr>";
									$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 200px;'>負責老師簽署：</span>	
													 <span style='display:table-cell; border-bottom: 1px solid #000; width:200px;'>&nbsp;</span>
													 <span style='display: table-cell; width: 200px;'></span>
													 <span style='display: table-cell; width: 100px;'>日期：</span>	
													 <span style='width:200px; display:table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
									$display .= "</td>";
								$display .= "</tr>";
				
								$display .= "</table></center>";							
								$display .= "<br /><br /><br />";
													
								$numOfAvaliablePage--;
							
						} // End Of while($numOfAvaliablePage>0){
					}  // End Of if($thisRecordStatus=='2'){ // Approved Activity Member
				}//	foreach ((array)$thisActivityStudentInfo as $thisRecordStatus=>$thisStudentInfoArr){
			} // foreach event date info
		} // End Of foreach ($activityInfo as $thisEventID=>$thisEventInfo){
		
		if($display==''){
			$display .= '<center>';
			$display .= "<table width='1000px' style=' border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '>";
			$display .= '<tr><td align="center">';
			$display .= '沒有活動紀錄。';
			$display .= '</td></tr></table></center>';
		}		
		return $display;
	}
	
	
	function Get_Management_TransferToWebSAMS_Step1_UI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr)
	{
		global $PATH_WRT_ROOT, $Lang, $eEnrollment, $i_general_clickheredownloadsample, $libenroll;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		include_once($PATH_WRT_ROOT.'includes/libinterface.php');
		include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	
		
		$linterface = new interface_html();
		$libgs = new libgeneralsettings();
		
		$settingAry = array();
		$settingAry[] = '\'Generate_WebSAMS_Excel\'';
		$lastGeneratedDate = $libgs->Get_General_Setting($libenroll->ModuleTitle, $settingAry);
		$lastGeneratedDate = ($lastGeneratedDate['Generate_WebSAMS_Excel'])? $lastGeneratedDate['Generate_WebSAMS_Excel'] : $Lang['General']['EmptySymbol'];
		
		$StepsDisplay = $this->GET_IMPORT_STEPS($CurrStep=1, $Lang['eEnrolment']['Transfer_to_SP']['StepArr']);
		
		$RequiredStar = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Required'].'</span>'."\n";

		$thisTag = 'onchange="js_Changed_Academic_Year_Selection(this.value);"';
		$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $thisTag, $noFirst=1, $noPastYear=0, $AcademicYearID);
		
		$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
		$numOfForm = count($FormArr);
		$NumOfFormInRow = 5;
		
		$FormChkTable = '';
		$FormChkTable .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			
			$thisChkID = "SelectAll_TargetFormChk";
			$thisChkOnClick = "Set_Checkbox_Value('YearIDArr[]', this.checked);";
			$FormChkTable .= '<tr><td style="border-bottom:0px;padding-bottom:0px;padding-top:0px;">'."\n";
				$thisChecked = ($YearIDArr=='')? true : false;
				$FormChkTable .= $this->Get_Checkbox($thisChkID, $thisChkName='', 1, $thisChecked, '', $eEnrollment['all'], $thisChkOnClick, $Disabled='');
			$FormChkTable .= '</td></tr>'."\n";
				
			for ($i=0; $i<$numOfForm; $i++)
			{
				if (($i % $NumOfFormInRow) == 0)
				{
					### row start
					$FormChkTable .= '<tr>'."\n";
					$PrefixSpace = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				else
					$PrefixSpace = '';
				
				### Build Checkboxes
				$thisYearID = $FormArr[$i]['YearID'];
				$thisYearName = $FormArr[$i]['YearName'];
				$thisChkID = 'YearID_'.$thisYearID;
				$thisChkName = 'YearIDArr[]';
				$thisChkClass = 'FormChk';
				$thisChkOnClick = "Uncheck_SelectAll('SelectAll_TargetFormChk', this.checked);";
				$thisChecked = ($YearIDArr=='' || in_array($thisYearID, (array)$YearIDArr))? true : false;
				
				$FormChkTable .= '<td style="border-bottom:0px;padding-bottom:0px;padding-top:0px;">'."\n";
					$FormChkTable .= $PrefixSpace.$this->Get_Checkbox($thisChkID, $thisChkName, $thisYearID, $thisChecked, $thisChkClass, $thisYearName, $thisChkOnClick, $Disabled='');
				$FormChkTable .= '</td>'."\n";
				
				if ( ( $i>0 && (($i % $NumOfFormInRow) == ($NumOfFormInRow-1)) ) || ($i==$numOfForm-1) )
				{
					### row end
					$FormChkTable .= '</tr>'."\n";
				}
			}
		$FormChkTable .= '</table>'."\n";
		
		
		$x .= '<br />'."\n";
		$x .= '<form id="form1" name="form1" method="POST" enctype="multipart/form-data" >';
			$x .= '<div class="table_board">'."\n";
				$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
					$x .= '<tr><td>'.$StepsDisplay.'</td></tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								### Year / Term Settings
								$x .= $this->GET_NAVIGATION2_IP25($Lang['eEnrolment']['TransferToWebSAMS']['SelectCriteria']);
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['General']['SchoolYear'].'</td>'."\n";
									$x .= '<td>'.$AcademicYearSelection.'</td>';
								$x .= '</tr>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['General']['Term'].'<span class="tabletextremark"> '.$Lang['eEnrolment']['ForClubOnly'].'</span></td>'."\n";
									//$x .= '<td><div id="YearTermSelectionDiv"></div><div id="LastTransferDiv"></div></td>';
									$x .= '<td><div id="YearTermSelectionDiv"></div></td>';
								$x .= '</tr>'."\n";
								
								### Club / Activity
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['eEnrolment']['Transfer_to_SP']['DataSource'].'</td>'."\n";
									$x .= '<td>'.$this->Get_Data_Type_Checkbox_Table($DataTypeArr).'</td>';
								$x .= '</tr>'."\n";
								
//								### Club Name Display Option
//								$x .= '<tr>'."\n";
//								$x .= '<td class="field_title">'.$RequiredStar.$Lang['eEnrolment']['Transfer_to_SP']['DataNameLang'] . ' <span class="tabletextremark">' . $Lang['eEnrolment']['ForClubOnly']  . '</span>' .'</td>'."\n";
//								$x .= '<td>';	
//									$x .= $linterface->Get_Radio_Button('DataNameLang', 'DataNameLang', '1', '1', $Class="", $Lang['General']['English']);
//									$x .= $linterface->Get_Radio_Button('DataNameLang', 'DataNameLang', '0', $isChecked=0, $Class="",$Lang['General']['Chinese']);
//								$x .= '</td>';
//								$x .= '</tr>'."\n";
								
								### Active Member
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$eEnrollment['transfer_student_setting'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<input type="radio" name="ActiveMemberOnly" value="0" id="ActiveMemberOnly_No" checked> <label for="ActiveMemberOnly_No">'.$eEnrollment['all_member'].'</label>'."\n";
										$x .= '&nbsp;&nbsp;'."\n";
										$x .= '<input type="radio" name="ActiveMemberOnly" value="1" id="ActiveMemberOnly_Yes"> <label for="ActiveMemberOnly_Yes">'.$eEnrollment['active_member_only'].'</label>'."\n";
										$x .= '&nbsp;&nbsp;'."\n";
										$x .= '<input type="radio" name="ActiveMemberOnly" value="2" id="ActiveMemberOnly_Inactive"> <label for="ActiveMemberOnly_Inactive">'.$Lang['eEnrolment']['InactiveMemberOnly'].'</label>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								### Form
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$eEnrollment['add_activity']['act_target'].'</td>'."\n";
									$x .= '<td>'.$FormChkTable.'</td>'."\n";									
								$x .= '</tr>'."\n";
								
								
								### reportCardReadableIndicator
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['eEnrolment']['TransferToWebSAMS']['reportCardReadableIndicator'] .'</td>'."\n";
									$x .= '</td>'."\n";
									$x .= '<td>'."\n";
									
									$x .= '<input type="radio" name="reportCardReadableIndicator" value="Y" id="reportCardReadableIndicator_Yes"  checked> <label for="reportCardReadableIndicator_Yes">'.$Lang['eEnrolment']['TransferToWebSAMS']['Yes'].'</label>'."\n";
									$x .= '&nbsp;&nbsp;'."\n";
									$x .= '<input type="radio" name="reportCardReadableIndicator" value="N" id="reportCardReadableIndicator_No"> <label for="reportCardReadableIndicator_No">'.$Lang['eEnrolment']['TransferToWebSAMS']['No'].'</label>'."\n";
									
									$x .= '</td>'."\n";							
								$x .= '</tr>'."\n";
								
								### ExportType Option
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicator'] .'</td>'."\n";
									$x .= '</td>'."\n";
									$x .= '<td>'."\n";
									
									$x .= '<input type="radio" name="ExportIndicator" value="Single" id="ExportIndicator_Single"  checked> <label for="ExportIndicator_Single">'.$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicatorBySingle'].'</label>'."\n";
									$x .= '&nbsp;&nbsp;'."\n";
									$x .= '<input type="radio" name="ExportIndicator" value="All" id="ExportIndicator_All"> <label for="ExportIndicator_All">'.$Lang['eEnrolment']['TransferToWebSAMS']['WebSAMSExportTypeIndicatorByAll'].'</label>'."\n";
									
									$x .= '</td>'."\n";							
								$x .= '</tr>'."\n";
								
								
								########## mapping mode
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$RequiredStar.$eEnrollment['WebSAMSSTA']['MappingMode'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $linterface->Get_Radio_Button('uploadDirectly', 'uploadMethod', 'directly', $isChecked=1, $Class="", $eEnrollment['WebSAMSSTA']['UseWebSAMSCode'], 'changedUploadMethod(this.value);')."\n";
										$x .= '&nbsp;';
										$x .= $linterface->Get_Radio_Button('uploadMappingFiles', 'uploadMethod', 'mappingfile', $isChecked=0, $Class="", $eEnrollment['WebSAMSSTA']['UseMappingFile'], 'changedUploadMethod(this.value);')."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								
								
								
								#### Required field remark
								$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= "<span class='tabletextremark'>".Get_Last_Modified_Remark('', '', '', $Lang['eEnrolment']['LastGeneratedDate'].': '.$lastGeneratedDate)."</span>";
									$x .= '<br />'."\n";
									$x .= $this->MandatoryField()."\n";	
								$x .= '</td>'."\n";	
								$x .= '</tr>'."\n";
																										
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					$x .= '<br />';
					$x .= '<tr>';
					$x .= '<td>';
					$x .= '<br />';
						$x .='<div id="uploadFile" name="uploadFile">';
						
							$x .= '<table>'."\n";
								$x .= $this->GET_NAVIGATION2_IP25($Lang['eEnrolment']['TransferToWebSAMS']['UploadMappingFiles']);
						
								$x .= '<tr>';
								$x .=$linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eEnrolment']['TransferToWebSAMS']['Instruction']);
								$x .= '</tr>';
									
								$x .= '<tr>';
									$x .= '<td nowrap="nowrap" colspan="2">';
										$x .= $i_general_clickheredownloadsample . ':' ;
									$x .= '</td>';
									
									$x .= '<td nowrap="nowrap" colspan="2">';
										$x .= '<a  href="javascript:void(0);" onclick="javacript:js_export_sample(\'export_club_activity_sample.php\');" class="tablelink">';
										$x .= '<img src="'.$PATH_WRT_ROOT.'images/2009a/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"></img>';
										$x .= 'club_activity_mapping.csv';
										$x .= '</a>';
									$x .= '</td>';
									
									# post_mapping.csv
									$x .= '<td nowrap="nowrap" colspan="2">';
										$x .= '<a  href="javascript:void(0);" onclick="javacript:js_export_sample(\'export_post_sample.php\');" class="tablelink">';
										$x .= '<img src="'.$PATH_WRT_ROOT.'images/2009a/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">';
										$x .= 'post_mapping.csv';
										$x .= '</a>';
									$x .= '</td>';
									
									# performance_mapping.csv
									$x .= '<td nowrap="nowrap" colspan="2">';
										$x .= '<a href="javascript:void(0);" onclick="javacript:js_export_sample(\'export_performance_sample.php\');" class="tablelink">';
										$x .= '<img src="'.$PATH_WRT_ROOT.'images/2009a/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">';
										$x .= 'performance_mapping.csv';
										$x .= '</a>';
									$x .= '</td>';
								$x .= '</tr>';
								
							$x .= '</table>'."\n";		
				
						###### Upload Files ######
							$x .= '<table width="100%" class="form_table_v30">';
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['eEnrolment']['TransferToWebSAMS']['Club&ActivityMapping'].'</td>'."\n";
									$x .= '<td class="tabletext">';
									$x .= '<input id="clubAndActivityCSV" class="file" type="file" name="clubCSV">';
									$x .= '</td>';													
								$x .= '</tr>'."\n";
								
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['eEnrolment']['TransferToWebSAMS']['PostMapping'].'</td>'."\n";
									$x .= '<td class="tabletext">';
									$x .= '<input id="postCSV" class="file" type="file" name="postCSV">';
									$x .= '</td>';								
								$x .= '</tr>'."\n";
								
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['eEnrolment']['TransferToWebSAMS']['PerformanceMapping'].'</td>'."\n";
									$x .= '<td class="tabletext">';
									$x .= '<input id="performanceCSV" class="file" type="file" name="performanceCSV">';
									$x .= '</td>';								
								$x .= '</tr>'."\n";
							$x .= '</table>';
						$x .= '</div>'."\n";
					
					$x .= '</td>';
					$x .= '</tr>';
					
					$x .='</div>';
										
				$x .= '</table>'."\n";
			
										
			$x .= '<br />';		
		
					
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $this->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Check_Form();", "submitBtn");
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>'."\n"; 
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";

		return $x;
	}
	
	function Get_Term_Checkbox($AcademicYearID='', $defaultCheck=false) {
		global $i_ClubsEnrollment_WholeYear, $Lang, $linterface;
		
		if ($linterface==null) {
			$linterface = new interface_html();
		}
		
		if ($AcademicYearID=='') {
			$AcademicYearID = Get_Current_Academic_Year_ID();
		}
		$semester_data = getSemesters($AcademicYearID);
		
		$checkboxAry = array();
		$checkboxAry[] = array('YearTermID_0', 'YearTermIDAry[]', 0, $defaultCheck, 'YearTermIDChk', $i_ClubsEnrollment_WholeYear);
		foreach ($semester_data as $YearTermID => $YearTermTitle) {
			$checkboxAry[] = array('YearTermID_'.$YearTermID, 'YearTermIDAry[]', $YearTermID, $defaultCheck, 'YearTermIDChk', $YearTermTitle);
		}
		
		return $linterface->Get_Checkbox_Table('checkboxGroup', $checkboxAry, $itemPerRow=5, $defaultCheck);
	}
	
	function getDaysSelect($numDay=1,$selectedValue=''){
		
		if(($numDay+0)>0){
			global $Lang;
			$optionArr[0] = $Lang['eEnrolment']['AttendanceReminderToday'];
			
			for($x=1; $x< ($numDay+1); $x++){
				$optionArr[$x] = $x.' '.$Lang['eEnrolment']['AttendanceReminderDayAfter'];
			}
			
			return getSelectByAssoArray($optionArr,'id="AttendanceReminderDaysBefore" name="AttendanceReminderDaysBefore"',$selectedValue,0,1);
		}
		else{
			return false;
		} 
	}
	
	// used in curriculum template > course
	function getNumberOfLessonSelection($selectedNumber) 
	{
	    global $enrolConfigAry;
	    
	    $maxNumberOfLesson = $enrolConfigAry['CurriculumTemplate']['MaxNumberOfLesson'] ? $enrolConfigAry['CurriculumTemplate']['MaxNumberOfLesson'] : 30;
	    $data = array();
	    for ($i=1; $i<=$maxNumberOfLesson; $i++) {
	        $data[] = array($i,$i);
	    }
	    $x = getSelectByArray($data, "name='NumberOfLesson' id='NumberOfLesson'", $selectedNumber);
	    return $x;
	}

	function getWeekSelection($selectedWeek, $sequence=0)
	{
	    global $enrolConfigAry;
	    
	    $maxNumberOfWeek = $enrolConfigAry['CurriculumTemplate']['MaxNumberOfWeek'] ? $enrolConfigAry['CurriculumTemplate']['MaxNumberOfWeek'] : 52;
	    $data = array();
	    for ($i=1; $i<=$maxNumberOfWeek; $i++) {
	        $data[] = array($i,$i);
	    }
	    
        $name = 'WeekNumber[]';
        $id = 'WeekNumber_'.$sequence;
	    $x = getSelectByArray($data, "name='$name' id='$id'", $selectedWeek, $all=0, $noFirst=1);
	    return $x;
	}

	function getDaySelection($selectedDay, $sequence=0)
	{
	    global $enrolConfigAry;
	    
	    $daysPerWeek = $enrolConfigAry['CurriculumTemplate']['DaysPerWeek'] ? $enrolConfigAry['CurriculumTemplate']['DaysPerWeek'] : 7;
	    $data = array();
	    for ($i=1; $i<=$daysPerWeek; $i++) {
	        $data[] = array($i,$i);
	    }
	    
        $name = 'DayNumber[]';
        $id = 'DayNumber_'.$sequence;
	    $x = getSelectByArray($data, "name='$name' id='$id'", $selectedDay, $all=0, $noFirst=1);
	    return $x;
	}

	function getHourSelection($prefix='StartHour', $selectedHour, $sequence=0, $courseID=null, $onChange='')
	{
	    if(is_null($courseID)) {
    	    $name = $prefix.'[]';
    	    $id = $prefix.'_'.$sequence;
	    }
	    else {
	        $name = $prefix."[$courseID][$sequence]";
	        $id = $name;
	    }
	    if ($onChange) {
	        $onChange = "onChange=changeDateTimeSelect(this)";
	    }
	    $x = $this->Get_Time_Selection_Box($name, $type='hour', $selectedHour, "class='dateTimeSelect' $onChange", $interval=1, $id);
	    return $x;
	}	

	function getMinuteSelection($prefix='StartMinute', $selectedMinute, $sequence=0, $courseID=null, $onChange='')
	{
	    if(is_null($courseID)) {
    	    $name = $prefix.'[]';
    	    $id = $prefix.'_'.$sequence;
	    }
	    else {
	        $name = $prefix."[$courseID][$sequence]";
	        $id = $name;
	    }
	    if ($onChange) {
	        $onChange = "onChange=changeDateTimeSelect(this)";
	    }	    
	    $x = $this->Get_Time_Selection_Box($name, $type='min', $selectedMinute, "class='dateTimeSelect' $onChange", $interval=1, $id);
	    return $x;
	}
	
	function getLessonTitleList($lessonAry=array(), $isShowSchedule=false) 
	{
	    global $enrolConfigAry, $Lang;
	    
	    $x = '';
	    $maxNumberOfLesson = $enrolConfigAry['CurriculumTemplate']['MaxNumberOfLesson'] ? $enrolConfigAry['CurriculumTemplate']['MaxNumberOfLesson'] : 30;
	    for ($i=0; $i<$maxNumberOfLesson; $i++) {
	        $j = $i + 1;
	        if (($i == 0) || $lessonAry[$i]['Title']) {
	            $lesson = intranet_htmlspecialchars($lessonAry[$i]['Title']);
	            $class = 'show';
	        }
	        else {
	            $lesson = '';
	            $class = 'hide';
	        }
	        
	        if ($isShowSchedule) {
	            $weekDayClass = 'show';
	            $combinedLessonClass = 'hide';
	        }
	        else {
	            $weekDayClass = 'hide';
	            $combinedLessonClass = 'show';
	        }
	        $x .= '<div id="divLesson_'.$i.'" class="'.$class.'">'.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].' '. $j.': ';
	        $x .= '<input type="text" name="LessonTitle[]" id="LessonTitle_'.$i.'" class="textboxtext" style="width:50%" value="'.$lesson.'">';
	        $x .= '<span class="error_msg_hide" id="ErrLessonTitle_'.$i.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';

	        $x .= '<span class="isCombinedSpan '.$combinedLessonClass.'">';
	        $x .= '<input type="checkbox" name="IsCombinedLesson_'.$i.'" id="IsCombinedLesson_'.$i.'" value="1" '.($lessonAry[$i]['IsCombined']?' checked' : '').'><label for="IsCombinedLesson_'.$i.'">'. $Lang['eEnrolment']['course']['isCombinedLesson'].'</label>';
 	        $x .= '</span>';
 	        
            $weekSelection = $this->getWeekSelection($lessonAry[$i]['WeekNumber'], $i);
            $daySelection = $this->getDaySelection($lessonAry[$i]['DayNumber'], $i);
            
            if (empty($lessonAry[$i]['StartTime'])) {
                $startHourSelected = '0';
                $startMinuteSelected = '00';
            }
            else {
                $startHourSelected = date('G', strtotime($lessonAry[$i]['StartTime']));
                $startMinuteSelected = intval(date('i', strtotime($lessonAry[$i]['StartTime'])));
            }
            $startHourSelection = $this->getHourSelection('StartHour', $startHourSelected, $i);
            $startMinuteSelection = $this->getMinuteSelection('StartMinute', $startMinuteSelected, $i);

            if (empty($lessonAry[$i]['EndTime'])) {
                $endHourSelected = '0';
                $endMinuteSelected = '00';
            }
            else {
                $endHourSelected = date('G', strtotime($lessonAry[$i]['EndTime']));
                $endMinuteSelected = intval(date('i', strtotime($lessonAry[$i]['EndTime'])));
            }
            $endHourSelection = $this->getHourSelection('EndHour', $endHourSelected, $i);
            $endMinuteSelection = $this->getMinuteSelection('EndMinute', $endMinuteSelected, $i);
            
            $x .= '<br><div class="'.$weekDayClass.'" style="margin-top: 5px; padding-left:20px;"> '.$Lang['eEnrolment']['curriculumTemplate']['weekNumber'].': '. $weekSelection;
            $x .= ' ' .$Lang['eEnrolment']['curriculumTemplate']['dayNumber'].': '.$daySelection;
            $x .= ' '. $Lang['General']['Time'].': '. $startHourSelection.' : '. $startMinuteSelection;
            $x .= $Lang['eEnrolment']['curriculumTemplate']['to'].' ' . $endHourSelection . ' : ' . $endMinuteSelection;
            $x .= '<span class="error_msg_hide" id="ErrTimeLogic_'.$i.'">'.$Lang['eEnrolment']['Warning']['TimeRangeInvalid'].'</span>';
            $x .= '</div><br>';
	        
	        $x .= '</div>';
	    }
	    return $x;
	}
	
	/*
	 *     $bookingInfo = array('Date'=>'','StartTime'=>'','EndTime'=>'')
	 */
	function getLocationSelection($prefix='Location', $selectedLocation='', $row=0, $courseID=null, $showCapacity=false, $capacity=0, $bookingInfo=array(), $categoryID='', $topPriorityLocationID='')
	{
	    global $libenroll, $Lang;
	    
	    if ($showCapacity) {
	        $locationList = $libenroll->getLocationListWithCapacity($capacity, $bookingInfo, $selectedLocation, $categoryID, $topPriorityLocationID);
	        $errHints = $Lang['eEnrolment']['curriculumTemplate']['warning']['locationIsBookedByOthers'];
	    }
	    else {
	        $locationList = $libenroll->getLocationList();
	        $errHints = $Lang['eEnrolment']['curriculumTemplate']['warning']['selectLocation'];
	    }
	    
	    $data = array();
	    foreach ($locationList as $_locationID=>$_location) {
	        $data[] = array($_locationID, $_location['Location']);
	    }
	    
	    if(is_null($courseID)) {
	        $name = $prefix.'[]';
	        $id = $prefix.'_'.$row;
	    }
	    else {
	        $name = $prefix."[$courseID][$row]";
	        $id = $name;
	    }
	    $errID = 'Err'.$id;
	    
	    if ((count($data) == 0) && $showCapacity) {
	        $firstTitle = $Lang['eEnrolment']['curriculumTemplate']['warning']['noAvailableLocation'];
	        $x = getSelectByArray($data, "name='$name' id='$id'", $selectedLocation, $all=0, $noFirst=0, $firstTitle);
	    }
	    else {
	       $x = getSelectByArray($data, "name='$name' id='$id'", $selectedLocation);
	    }
	    $x .= '<span class="error_msg_hide" id="'.$errID.'">'.$errHints.'</span>';
	    return $x;
	}
	
	function getNumberOfStudentSelection($prefix='NumberOfStudent', $selectedNumber='1', $row=0)
	{
	    global $enrolConfigAry, $Lang;
	    
	    $maxNumberOfClassStudent = $enrolConfigAry['CurriculumTemplate']['MaxNumberOfClassStudent'] ? $enrolConfigAry['CurriculumTemplate']['MaxNumberOfClassStudent'] : 99;
	    $data = array();
	    for ($i=1; $i<=$maxNumberOfClassStudent; $i++) {
	        $data[] = array($i,$i);
	    }
	    
	    $name = $prefix.'[]';
	    $id = $prefix.'_'.$row;
	    $errID = 'Err'.$id;
	    $x = getSelectByArray($data, "name='$name' id='$id' onChange='changeNumberOfStudent(this)'", $selectedNumber, $all=0, $noFirst=1);
	    $x .= '<span class="error_msg_hide" id="'.$errID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['selectNumberOfStudent'].'</span>';
	    return $x;
	}
	
	/*
	 *  $instructorAry is for existing record.
	 *  $instructorAry = array('1'=>'Chan Tai Man');
	 *  $row is the row index, start from 0
	 *  $categoryID - enrolment category, used to filter instructor
	 */
	function getInstructorSelection($instructorAry=array(), $row=0, $courseID=null, $tableClass='', $classID='', $height='40px', $categoryID='', $parFieldID='', $parFieldName='', $parFieldClass='', $onClick='')
	{
	    global $button_remove, $button_select, $Lang;
	    
	    $nrInstructor = count($instructorAry);
	    
	    if ($parFieldID == '' && $parFieldName == '') {
	        if (is_null($courseID)) {
	            $fieldIDInstructor = "Instructor_$row";
	            $fieldNameInstructor = "Instructor[$row][]";
	        }
	        else {
	            if ($classID) {
	                $fieldIDInstructor = "ClassInstructor[$classID][$courseID][$row]";
	                $fieldNameInstructor = "ClassInstructor[$classID][$courseID][$row][]";
	            }
	            else {
	                $fieldIDInstructor = "Instructor[$courseID][$row]";
	                $fieldNameInstructor = "Instructor[$courseID][$row][]";
	            }
	        }
	    }
	    else {
	        $fieldIDInstructor = $parFieldID;
	        $fieldNameInstructor = $parFieldName;
	    }
	    
	    $instructorSel = '<select name="'.$fieldNameInstructor.'" id="'.$fieldIDInstructor.'" class="'.$parFieldClass.'" style="min-width:150px; height:'.$height.';" multiple="multiple">'.NEW_LINE;
	    
	    foreach((array)$instructorAry as $_instructorID => $_instructor) {
	        $instructorSel .= '<option value="'.$_instructorID.'">'.$_instructor.'</option>'.NEW_LINE;
	    }
	    $instructorSel .= "</select>";
	    
	    $button_remove_html = $this->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['{$fieldNameInstructor}'])");
	    
	    $x = '<table border="0" cellpadding="0" cellspacing="0" class="'.$tableClass.'" style="margin-top:-4.5px;">'.NEW_LINE;
	    $x .= '<tr>'.NEW_LINE;
	    $x .= '<td>'.$instructorSel.'<span class="error_msg_hide" id="Err'.$fieldIDInstructor.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['selectInstructor'].'</span></td>'.NEW_LINE;
	    $x .= '<td valign="bottom">';
	    if ($onClick == '') {
	        $onClick = "javascript:newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname={$fieldNameInstructor}&ppl_type=pic&permitted_type=1&categoryID={$categoryID}', 9)";
	    }
	    $x .= $this->GET_SMALL_BTN($button_select, "button", $onClick).'';
	    $x .= '<br>'.$button_remove_html;
	    $x .= '</td>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    $x .= '</table>'.NEW_LINE;
	    
	    return $x;
	}
	
    /*
     *  $intakeAry is for existing record.  showone class info only
     *  $intakeAry = array(
     *      'ClassID'=>'',
     *      'ClassName'=>'',
     *      'InstructorAry'=>array('id'=>'','name'=>''),
     *      'NumberOfStudent'=>'',
     *      'LocationID'=>'');
	 *  $row is the row index, start from 0      
     */	
	function getIntakeClassTable($intakeAry=array(), $row=0)
	{
	    global $Lang;
	    
	    $x = '<table class="common_table_list">'.NEW_LINE;
	    
	       $x .= '<tr>'.NEW_LINE;
	           $x .= '<th width="29%" style="min-width:200px;">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['className'].'</th>'.NEW_LINE;
	           $x .= '<th width="29%">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'].'</th>'.NEW_LINE;
	           $x .= '<th width="10%">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfStudent'].'</th>'.NEW_LINE;
	           $x .= '<th width="29%">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['locationCapacity'].'</th>'.NEW_LINE;
	           $x .= '<th width="3%">&nbsp;</th>'.NEW_LINE;
	       $x .= '</tr>'.NEW_LINE;
	       
	       if (count($intakeAry)) {
	           $classID = $intakeAry['ClassID'];
	           $className = intranet_htmlspecialchars($intakeAry['ClassName']);
	           $instructorAry = $intakeAry['InstructorAry'];
	           $numberOfStudent = $intakeAry['NumberOfStudent'];
	           $locationID = $intakeAry['LocationID'];	           
	       }
	       else {
	           $classID = '';
	           $className = '';
	           $instructorAry = array();
	           $numberOfStudent = 1;
	           $locationID = '';
	       }
	       $fieldNameClassName = 'ClassName[]';
	       $fieldIDClassName = 'ClassName_'.$row;
	       $prefixNumberOfStudent = 'NumberOfStudent';
	       $prefixLocation = 'Location';
	       $locationCol = 'LocationCol_'.$row;
	       
	       $x .= '<tr>'.NEW_LINE;
	           $x .= '<td class="tabletext"><input type="hidden" name="ClassID[]" value="'.$classID.'">';
	           $x .= '<input type="text" name="'.$fieldNameClassName.'" id="'.$fieldIDClassName.'" class="textboxtext" value="'.$className.'">';
	           $x .= '<span class="error_msg_hide" id="Err'.$fieldIDClassName.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputClassName'].'</span></td>'.NEW_LINE;
	           $x .= '<td class="tabletext">'.$this->getInstructorSelection($intakeAry['InstructorAry'], $row).'</td>'.NEW_LINE;
	           $x .= '<td class="tabletext">'.$this->getNumberOfStudentSelection($prefixNumberOfStudent, $numberOfStudent, $row).'</td>'.NEW_LINE;
	           $x .= '<td class="tabletext" id="'.$locationCol.'">'.$this->getLocationSelection($prefixLocation, $locationID, $row, $courseID=null, $showCapacity=true, $numberOfStudent).'</td>'.NEW_LINE;
	           $x .= '<td><span class="table_row_tool"><a class="delete" title="' . $Lang['eEnrolment']['curriculumTemplate']['intake']['removeClass']. '" onClick="removeClass(this)" data-ClassID="'.$classID.'"></a></span></td>';
	       $x .= '</tr>'.NEW_LINE;
	       
	    $x .='</table>'.NEW_LINE;
	    
	    return $x;
	}
	
	function getIntakeSelection($name='Intake', $selected='', $firstTitle='')
	{
	    global $libenroll, $Lang;
	    
	    $intakeList = $libenroll->getIntakeList();
	    
	    $data = array();
	    foreach ((array)$intakeList as $_intake) {
	        $key = $_intake['IntakeID'];
	        $val = $_intake['Title'];
	        $data[] = array($key, $val);
	    }
	    
	    if (count($data) && $selected == '') {
	       $selected = $data[0][0];        // select the first one
	    }
	    
	    $errID = 'Err'.$name;	    
	    $x = getSelectByArray($data, "name='$name' id='$name'", $selected, $all=0, $noFirst=0, $firstTitle);
	    $x .= '<span class="error_msg_hide" id="'.$errID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['selectIntake'].'</span>';
	    return $x;
	}
	
	// $field : both / date / time
	function getDateTimeSelection($courseID, $row, $lessonDate, $lessonStartTime, $lessonEndTime, $fields='both', $setOnChange=false)
	{
	    global $Lang;
	    
	    $lessonDateName = "LessonDate_{$courseID}_{$row}";
	    $errLessonDateID = 'Err'.$lessonDateName;

	    $x = '';
	    if (($fields == 'both') || ($fields == 'date')) {
    	    $x = '<span>'.$this->GET_DATE_PICKER($lessonDateName, $lessonDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeLessonDate(this)"); 
    	    $x .= '<span class="error_msg_hide" id="'.$errLessonDateID.'">'.$Lang['General']['WarningArr']['InputDate'].'</span>';
    		$x .= '</span><br>';
	    }
	    
	    if (($fields == 'both') || ($fields == 'time')) {
    		if (empty($lessonStartTime)) {
    	        $startHourSelected = '0';
    	        $startMinuteSelected = '00';
    	    }
    	    else {
    	        $startHourSelected = date('G', strtotime($lessonStartTime));
    	        $startMinuteSelected = intval(date('i', strtotime($lessonStartTime)));
    	    }
    	    
    	    $startHourSelection = $this->getHourSelection('StartHour', $startHourSelected, $row, $courseID, $setOnChange);
    	    $startMinuteSelection = $this->getMinuteSelection('StartMinute', $startMinuteSelected, $row, $courseID, $setOnChange);
    	    
    	    if (empty($lessonEndTime)) {
    	        $endHourSelected = '0';
    	        $endMinuteSelected = '00';
    	    }
    	    else {
    	        $endHourSelected = date('G', strtotime($lessonEndTime));
    	        $endMinuteSelected = intval(date('i', strtotime($lessonEndTime)));
    	    }

    	    $endHourSelection = $this->getHourSelection('EndHour', $endHourSelected, $row, $courseID, $setOnChange);
    	    $endMinuteSelection = $this->getMinuteSelection('EndMinute', $endMinuteSelected, $row, $courseID, $setOnChange);
    	    
    	    $x .= $startHourSelection.' : '. $startMinuteSelection;
    	    $x .= '<span style="white-space: nowrap;">'.$Lang['eEnrolment']['curriculumTemplate']['to'].$endHourSelection . ' : ' . $endMinuteSelection.'</span>';
	    }
	    if ($fields != 'date') {
    	    $errTimeLogicID = "ErrTimeLogic_{$courseID}_{$row}";
    	    $x .= '<span class="error_msg_hide" id="'.$errTimeLogicID.'">'.$Lang['eEnrolment']['Warning']['TimeRangeInvalid'].'</span>';
	    }
	    
	    return $x;
	}
	
	function getCombinedCourseTemplateTable($templateID, $intakeID='')
	{
	    global $libenroll, $Lang, $i_no_record_exists_msg;
	    
	    if ($intakeID) {
    	    $intakeAssoc = $libenroll->getNumberOfIntakeStudents($intakeID);
    	    $capacity = $intakeAssoc[$intakeID];
	    }
	    else {
	        $capacity = 0;
	    }
	    
	    $courseAssoc = $libenroll->getTemplateCourseDetails($templateID, $isCombinedCourse='1', $intakeID);
	    $nrCourse = count($courseAssoc);
        $tableClass = 'no_bottom_right_border';
        $setOnChange = true;    // set onChange function
        
        $locationListHints = $this->Get_Warning_Message_Box($Lang['eEnrolment']['course']['locationList']['title'], $Lang['eEnrolment']['course']['locationList']['combinedCourseDescription']);
        
        $x = '<table class="common_table_list">'.NEW_LINE;
    	    $x .= '<tr>'.NEW_LINE;
        	    $x .= '<th width="16%" style="min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['title'].'</th>'.NEW_LINE;
        	    $x .= '<th width="10%" style="min-width:50px;">'.$Lang['eEnrolment']['curriculumTemplate']['course']['code'].'</th>'.NEW_LINE;
        	    $x .= '<th width="16%" style="min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].'</th>'.NEW_LINE;
        	    $x .= '<th width="20%">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'].'</th>'.NEW_LINE;
        	    $x .= '<th width="22%">'.$Lang['eEnrolment']['curriculumTemplate']['course']['dateTime'].'</th>'.NEW_LINE;
        	    $x .= '<th width="16%">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['locationCapacity'].'<br>'.$locationListHints.'</th>'.NEW_LINE;
    	    $x .= '</tr>'.NEW_LINE;
	    
	    if ($nrCourse) {
	        foreach((array)$courseAssoc as $_course) {
	            $courseTitle = intranet_htmlspecialchars($_course['Title']);
                $courseID = $_course['CourseID'];
                $categoryID = $_course['CategoryID'];
                $courseTitleName = 'CourseTitle['.$courseID.']';
                $courseTitleID = $courseTitleName;
                $courseCodeName = 'CourseCode['.$courseID.']';
                $courseCodeID = $courseCodeName;
                $isCombinedCourseName = 'IsCombinedCourse['.$courseID.']';
                $courseDescriptionName = 'CourseDescription['.$courseID.']';
                $courseDescription = intranet_htmlspecialchars($_course['Description']);
                
                $lessonAssoc = $_course['LessonAry'];
                $nrLesson = count($lessonAssoc);
                $i = 0;
                foreach((array)$lessonAssoc as $__lesson) {
                    if ($i == 0) {
                        $courseTitleField = '<input type="text" name="'.$courseTitleName.'" id="'.$courseTitleID.'" class="textboxtext" value="'.$courseTitle.'">';
                        $courseTitleField .= '<span class="error_msg_hide" id="Err'.$courseTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseTitle'].'</span>';
                        $courseCodeField = '<input type="text" name="'.$courseCodeName.'" id="'.$courseCodeID.'" class="textboxtext" value="">';
                        $courseCodeField .= '<span class="error_msg_hide" id="Err'.$courseCodeID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseCode'].'</span>';
                        $isCombinedCourseField = '<input type="hidden" name="'.$isCombinedCourseName.'" value="1">';
                        $courseDescriptionField = '<input type="hidden" name="'.$courseDescriptionName.'" value="'.$courseDescription.'">';
                    }
                    else {
                        $courseTitleField = '';
                        $courseCodeField = '';
                        $isCombinedCourseField = '';
                        $courseDescriptionField = '';
                    }
                
                    $lessonID = $__lesson['LessonID'];
                    $lessonTitle = intranet_htmlspecialchars($__lesson['Title']);
                    $lessonTitleName = 'LessonTitle['.$courseID.'][]';
                    $lessonTitleID = 'LessonTitle['.$courseID.']['.$lessonID.']';
                    
                    $instructorAry = array();
                    
                    $lessonDate = $__lesson['LessonDate'];
                    $lessonStartTime = $__lesson['StartTime'];
                    $lessonEndTime = $__lesson['EndTime'];
                    
                    $prefixLocation = 'Location';
                    $selectedLocationID = '';
                    $bookingInfo = array('Date'=>$lessonDate, 'StartTime'=>$lessonStartTime, 'EndTime'=>$lessonEndTime);
                    $locationCol = 'LocationCol_'.$courseID.'_'.$i;
                    
    	            $x .= '<tr>'.NEW_LINE;
    	            if ($i == 0) {
        	            $x .= '<td class="tabletext" rowspan="'.$nrLesson.'">'.$courseTitleField;
        	               $x .= $isCombinedCourseField;
        	               $x .= $courseDescriptionField;
        	            $x .= '</td>'.NEW_LINE;
        	            $x .= '<td class="tabletext" rowspan="'.$nrLesson.'">'.$courseCodeField.'</td>'.NEW_LINE;
    	            }
    	            
        	            $x .= '<td class="tabletext">';
        	                $x .= '<input type="text" name="'.$lessonTitleName.'" id="'.$lessonTitleID.'" class="textboxtext" value="'.$lessonTitle.'">';
        	                $x .= '<span class="error_msg_hide" id="Err'.$lessonTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';
        	            $x .= '</td>'.NEW_LINE;
    	            
        	            $x .= '<td class="tabletext">'.$this->getInstructorSelection($instructorAry, $i, $courseID, $tableClass, '', '40px', $categoryID).'</td>'.NEW_LINE;
        	            $x .= '<td class="tabletext">'.$this->getDateTimeSelection($courseID, $i, $lessonDate, $lessonStartTime, $lessonEndTime, 'both', $setOnChange).'</td>'.NEW_LINE;
        	            $x .= '<td class="tabletext" id="'.$locationCol.'">'.$this->getLocationSelection($prefixLocation, $selectedLocationID, $i, $courseID, $showCapacity=true, $capacity, $bookingInfo, $categoryID).'</td>'.NEW_LINE;
    	            $x .= '</tr>'.NEW_LINE;
    	            
    	            $i++;
                }   // end lesson loop	            
	        }  // end course loop
	    }      // nrOfCourse > 0
	    else {
	        $x .= '<tr><td class="tableContent" align="center" colspan="6"><br>'.$i_no_record_exists_msg.'<br></td></tr>'.NEW_LINE;
	    }
	    
	    $x .='</table>'.NEW_LINE;
	    
	    return array('layout'=>$x, 'capacity'=>$capacity);
	}
	
	function getInstructorApplyToAllShortCutLayout($intakeID) {
	    global $Lang;
	    
	    $table = $this->getInstructorApplyToAllTable($intakeID);
	    
	    $x = '<tr>'.NEW_LINE;
	    $x .= '<td><br>'.NEW_LINE;
	    $x .= $this->GET_NAVIGATION2($Lang['eEnrolment']['curriculumTemplate']['categoryClassInstructor']);
	    $x .= '</td>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    $x .= '<tr>'.NEW_LINE;
	    $x .= '<td><br>'.NEW_LINE;
	    $x .= '<span id="InstructorApplyAllTable">'.$table.'</span>';
	    $x .= '</td>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    return $x;
	}
	
	function getInstructorApplyToAllTable($intakeID) {
	    global $Lang, $libenroll;
	    
	    $categoryAry = $libenroll->GET_CATEGORY_LIST();
	    $numOfCategory = count($categoryAry);
	    $intakeClassAry = $libenroll->getIntakeClass($intakeID);
	    $numOfIntakeClass = count($intakeClassAry);
	    
	    $x = '';
	    $x .= '<table id="ContentTable" class="common_table_list_v30">'.NEW_LINE;
	    $x .= '<thead>'.NEW_LINE;
	    $x .= '<tr>'.NEW_LINE;
	    $x .= '<th style="width:3%;">#</th>'.NEW_LINE;
	    $x .= '<th style="width:12%;">'.$Lang['eEnrolment']['curriculumTemplate']['course']['category'].'</th>'.NEW_LINE;
	    $x .= '<th style="width:23%;">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['class'].'</th>'.NEW_LINE;
	    $x .= '<th style="width:62%;">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'].'</th>'.NEW_LINE;
	    $x .= '</tr.'.NEW_LINE;
	    $x .= '</thead>'.NEW_LINE;
	    $x .= '<tbody>'.NEW_LINE;
	    for ($i=0; $i<$numOfCategory; $i++) {
	        $_categoryID = $categoryAry[$i]['CategoryID'];
	        $_categoryName = $categoryAry[$i]['CategoryName'];
	        
	        $x .= '<tr>'.NEW_LINE;
	        $x .= '<td rowspan="'.$numOfIntakeClass.'">'.($i+1).'</td>'.NEW_LINE;
	        $x .= '<td rowspan="'.$numOfIntakeClass.'">'.$_categoryName.'</td>'.NEW_LINE;
	        
	        $_intakeClassCount = 0;
	        foreach((array)$intakeClassAry as $__classID => $__intakeClass) {
	            $__className = $__intakeClass['ClassName'];
	            
	            if ($_intakeClassCount > 0) {
	                $x .= '<tr>'.NEW_LINE;
	            }
	            
	            $x .= '<td>'.$__className.'</td>'.NEW_LINE;
	            $x .= '<td>'.$this->getInstructorSelection($instructorAry=array(), $row=0, $courseID=null, $tableClass='no_bottom_right_border', $__classID, $height='40px', $_categoryID, 'defaultCategoryClassInstructorSel_'.$_categoryID.'_'.$__classID, 'defaultCategoryClassInstructorSel['.$_categoryID.']['.$__classID.']', 'defaultCategoryClassInstructorSel').'</td>'.NEW_LINE;
	            $x .= '</tr>'.NEW_LINE;
	        }
	    }
	    
	    $x .= '<tr><td colspan="4" align="center">'.NEW_LINE;
	    $x .= $this->GET_BTN($Lang['Btn']['ApplyToAll'], "button", "applyCategoryClassInstructorToAllCourses();","btnApplyToAll", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn").NEW_LINE;
	    $x .= '</td></tr>'.NEW_LINE;
	    $x .= '</tbody>'.NEW_LINE;
	    $x .= '</table>'.NEW_LINE;
	    $x .= '<br>'.NEW_LINE;
	    
	    return $x;
	}

	function getCombinedCourseLayout($templateID, $intakeID='') 
	{
	    global $Lang;
	    
	    $combinedCourseTable = $this->getCombinedCourseTemplateTable($templateID, $intakeID);
	    
	    $x = '<tr>'.NEW_LINE;
	        $x .= '<td><br>'.NEW_LINE;
	            $x .= $this->GET_NAVIGATION2($Lang['eEnrolment']['curriculumTemplate']['course']['combinedCourse']);
	            $x .= '&nbsp; ('. $Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfStudent'].': <span id="IntakeCapacity">'. $combinedCourseTable['capacity'].'</span>)';
	        $x .= '</td>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    $x .= '<tr>'.NEW_LINE;
	        $x .= '<td><br>'.NEW_LINE;
	            $x .= '<span id="CombinedCourseTable">'.$combinedCourseTable['layout'].'</span>';
	        $x .= '</td>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    return $x;
	}
	
	function getNonCombinedCourseTemplateTable($templateID, $intakeID='', $classID='')
	{
	    global $libenroll, $Lang, $i_no_record_exists_msg;
	    
	    $courseAssoc = $libenroll->getTemplateCourseDetails($templateID, $isCombinedCourse='0', $intakeID);
	    $nrCourse = count($courseAssoc);
	    $combinedLessonAssoc = $libenroll->getCombinedLessonTemplate($templateID);
	    
	    $tableClass = 'no_bottom_right_border';
	    
	    $x = '<table class="common_table_list">'.NEW_LINE;
	    $x .= '<tr>'.NEW_LINE;
	    $x .= '<th width="25%" style="min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['title'].'</th>'.NEW_LINE;
	    $x .= '<th width="20%" style="min-width:50px;">'.$Lang['eEnrolment']['curriculumTemplate']['course']['code'].'</th>'.NEW_LINE;
	    $x .= '<th width="25%" style="min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].'</th>'.NEW_LINE;
	    $x .= '<th width="25%">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'].'</th>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    if (($nrCourse) && ($classID)) {
	        foreach((array)$courseAssoc as $_course) {
	            $courseTitle = intranet_htmlspecialchars($_course['Title']);
	            $courseID = $_course['CourseID'];
	            $categoryID = $_course['CategoryID'];
	            $courseTitleName = 'ClassCourseTitle['.$classID.']['.$courseID.']';
	            $courseTitleID = $courseTitleName;
	            $courseCodeName = 'ClassCourseCode['.$classID.']['.$courseID.']';
	            $courseCodeID = $courseCodeName;
	            $courseDescriptionName = 'ClassCourseDescription['.$classID.']['.$courseID.']';
	            $courseDescription = intranet_htmlspecialchars($_course['Description']);
	            
	            $lessonAssoc = $_course['LessonAry'];
	            $nrLesson = count($lessonAssoc);
	            $i = 0;
	            foreach((array)$lessonAssoc as $__lesson) {
	                if ($i == 0) {
	                    if (count($combinedLessonAssoc[$courseID])) {  // disable input if it's already shown in combined lesson
	                        $courseTitleField = '<input type="text" name="dis_'.$courseTitleName.'" id="dis_'.$courseTitleID.'" class="textboxtext" value="'.$courseTitle.'" disabled>';
	                        $courseTitleField .= '<input type="hidden" name="'.$courseTitleName.'" id="'.$courseTitleID.'" class="textboxtext" value="'.$courseTitle.'">';
	                        $courseCodeField = '<input type="text" name="dis_'.$courseCodeName.'" id="dis_'.$courseCodeID.'" class="textboxtext" value="" disabled>';
	                        $courseCodeField .= '<input type="hidden" name="'.$courseCodeName.'" id="'.$courseCodeID.'" class="textboxtext" value="">';
	                    }
	                    else {
    	                    $courseTitleField = '<input type="text" name="'.$courseTitleName.'" id="'.$courseTitleID.'" class="textboxtext" value="'.$courseTitle.'">';
    	                    $courseTitleField .= '<span class="error_msg_hide" id="Err'.$courseTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseTitle'].'</span>';
    	                    $courseCodeField = '<input type="text" name="'.$courseCodeName.'" id="'.$courseCodeID.'" class="textboxtext" value="">';
    	                    $courseCodeField .= '<span class="error_msg_hide" id="Err'.$courseCodeID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseCode'].'</span>';
	                    }
	                    $courseDescriptionField = '<input type="hidden" name="'.$courseDescriptionName.'" value="'.$courseDescription.'">';
	                }
	                else {
	                    $courseTitleField = '';
	                    $courseCodeField = '';
	                    $courseDescriptionField = '';
	                }
	                
	                $lessonID = $__lesson['LessonID'];
	                $lessonTitle = intranet_htmlspecialchars($__lesson['Title']);
	                $lessonTitleName = 'ClassLessonTitle['.$classID.']['.$courseID.'][]';
	                $lessonTitleID = 'ClassLessonTitle['.$classID.']['.$courseID.']['.$lessonID.']';
	                
	                $instructorAry = array();
	                
	                $x .= '<tr>'.NEW_LINE;
	                if ($i == 0) {
    	                $x .= '<td class="tabletext" rowspan="'.$nrLesson.'">'.$courseTitleField . $courseDescriptionField . '</td>'.NEW_LINE;
    	                $x .= '<td class="tabletext" rowspan="'.$nrLesson.'">'.$courseCodeField.'</td>'.NEW_LINE;
	                }
	                $x .= '<td class="tabletext">';
	                $x .= '<input type="text" name="'.$lessonTitleName.'" id="'.$lessonTitleID.'" class="textboxtext" value="'.$lessonTitle.'">';
	                $x .= '<span class="error_msg_hide" id="Err'.$lessonTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';
	                $x .= '</td>'.NEW_LINE;
	                
	                $instructorSelClass = 'ClassInstructorSel_'.$categoryID.'_'.$classID;
	                $x .= '<td class="tabletext">'.$this->getInstructorSelection($instructorAry, $i, $courseID, $tableClass, $classID, '40px', $categoryID, $parFieldID='', $parFieldName='', $instructorSelClass).'</td>'.NEW_LINE;
	                $x .= '</tr>'.NEW_LINE;
	                
	                $i++;
	            }   // end lesson loop
	        }  // end course loop
	    }      // nrOfCourse > 0
	    else {
	        $x .= '<tr><td class="tableContent" align="center" colspan="4"><br>'.$i_no_record_exists_msg.'<br></td></tr>'.NEW_LINE;
	    }
	    
	    $x .='</table>'.NEW_LINE;
	    
	    return $x;
	}

	function getNonCombinedCourseLayout($templateID, $intakeID='')
	{
	    global $libenroll, $Lang;
	    
	    $x = '';
	    $intakeClassAry = $libenroll->getIntakeClass($intakeID);
	    foreach((array)$intakeClassAry as $_classID => $_intakeClass) {
	        $className = $_intakeClass['ClassName'];
	        $classCapacity = $_intakeClass['NumberOfStudent'];
	        
	        $nonCombinedCourseTable = $this->getNonCombinedCourseTemplateTable($templateID, $intakeID, $_classID);
	        
	        $x .= '<tr><td><br>'.$this->GET_NAVIGATION2($className);
	        $x .= '&nbsp; ('. $Lang['eEnrolment']['curriculumTemplate']['intake']['numberOfStudent'].': <span>'.$classCapacity.'</span>)';
	        $x .= '</td></tr>'.NEW_LINE;
	        $x .= '<tr><td><br>'.$nonCombinedCourseTable.'</td></tr>'.NEW_LINE;
	    }
	    
	    return $x;
	}
	
	function getCourseScheduleTable($enrolEventID) {
	    global $Lang, $eEnrollment, $enrolConfigAry, $libenroll, $sys_custom;
	    
	    $eventInfoAry = $libenroll->GET_EVENTINFO($enrolEventID);                  // course info
        $categoryID = count($eventInfoAry) ? $eventInfoAry['EventCategory'] : 0;
        
	    $courseScheduleAry = $libenroll->GET_ENROL_EVENT_DATE($enrolEventID);      // lesson info
	    $nrLesson = count($courseScheduleAry);
	    
// 	    $intakeClassID = $libenroll->getIntakeClassIDByEnrolEventID($enrolEventID);
	    
        $intakeID = $libenroll->getIntakeIDByEnrolEventID($enrolEventID);
        $intakeClassAssoc = $libenroll->getIntakeClass($intakeID);
        
//        $isAllowedInstructor = $libenroll->checkIsAllowedInstructor($enrolEventID, $_SESSION['UserID']);
        $checkIsCourseCategoryInstructorExist = $libenroll->checkIsCourseCategoryInstructorExist($enrolEventID);
        
        $i = 1;
        $classList = '';
        foreach((array)$intakeClassAssoc as $_class) {
            $_className = $_class['ClassName'];
            $_numberOfStudent = $_class['NumberOfStudent'];
            if ($i > 1) {
                $classList .= '<br>';
            }
            $classList .= $i . '. '. $_className .' ('.$_numberOfStudent.')';
            $i++;
        }
        unset($i);
        
//         $classLocationID = '';  // top priority location
//         if ($intakeID) {
//             if ($intakeClassID) {  
//                 $intakeClassAry = $libenroll->getIntakeClass($intakeID);
//                 if (count($intakeClassAry)) {
//                     $capacity = $intakeClassAry[$intakeClassID]['NumberOfStudent'];
//                     $classLocationID = $intakeClassAry[$intakeClassID]['LocationID'];
//                 }
//                 else {
//                     $capacity = 0;
//                 }
//             }
//             else {  // combined course
//                 $intakeAssoc = $libenroll->getNumberOfIntakeStudents($intakeID);
//                 $capacity = $intakeAssoc[$intakeID];
//             }
//         }
//         else {
//             $capacity = 0;
//         }
        
        $isInstructor = $libenroll->isInstructor();
        
        $locationListHints = $this->Get_Warning_Message_Box($Lang['eEnrolment']['course']['locationList']['title'], $Lang['eEnrolment']['course']['locationList']['description']);
        
	    $x = '';
	    $x .= '<table id="dateTimeSettingTable" class="common_table_list_v30 edit_table_list_v30">'.NEW_LINE;
	        $x .= '<thead>'.NEW_LINE;
	            $x .= '<tr>'.NEW_LINE;
	                $x .= '<th style="width:25px;">#</th>'.NEW_LINE;
	                $x .= '<th style="width:20%; min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].'</th>'.NEW_LINE;
            	    $x .= '<th style="width:13%;">'.$Lang['General']['Date'].'</th>'.NEW_LINE;
            	    $x .= '<th style="width:15%;">'.$Lang['General']['Time'].'</th>'.NEW_LINE;
            	    $x .= '<th style="width:15%;">'.$Lang['eEnrolment']['curriculumTemplate']['course']['class'].'</th>'.NEW_LINE;
            	    $x .= '<th style="width:15%;">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'].'</th>'.NEW_LINE;
            	    $x .= '<th style="width:17%;">'.$Lang['eEnrolment']['curriculumTemplate']['intake']['locationCapacity'].'</th>'.NEW_LINE;
            	    $x .= '<th style="width:25px;">&nbsp;</th>'."\n";
        	    $x .= '</tr>'.NEW_LINE;
        	    
        	    $x .= '<tr class="edit_table_head_bulk">'.NEW_LINE;
        	        $x .= '<th>&nbsp;</th>'.NEW_LINE;
        	        $x .= '<th>&nbsp;</th>'.NEW_LINE;
        	        $x .= '<th>&nbsp;</th>'.NEW_LINE;
        	        $x .= '<th>'.NEW_LINE;
                	    $x .= $this->Get_Time_Selection_Box('globalHourStart', 'hour', 0);
                	    $x .= ' : ';
                	    $x .= $this->Get_Time_Selection_Box('globalMinStart', 'min', 0, $others_tab='', $interval=1);
                	    $x .= ' '.$eEnrollment['to'].' ';
                	    $x .= $this->Get_Time_Selection_Box('globalHourEnd', 'hour', 0);
                	    $x .= ' : ';
                	    $x .= $this->Get_Time_Selection_Box('globalMinEnd', 'min', 0, $others_tab='', $interval=1);
                	    $x .= ' ';
                	    $x .= $this->GET_SMALL_BTN($eEnrollment['add_activity']['set_all'], "button", "javascript:applyTimeToDate(0);");
                	    $x .= ' ';
                	    $x .= $this->GET_SMALL_BTN($eEnrollment['add_activity']['set_select_period'], "button", "javascript:applyTimeToDate(1);");
                	    $x .= $this->Get_Form_Warning_Msg('global_invalidTimeRangeWarningDiv', $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');
            	    $x .= '</th>'.NEW_LINE;
            	    $x .= '<th>'.$classList.'</th>'.NEW_LINE;
            	    $x .= '<th>'.$Lang['eEnrolment']['instructor']['left'].'</th>'.NEW_LINE;
            	    $x .= '<th>'.$locationListHints.'</th>'.NEW_LINE;
	                $x .= '<th><input type="checkbox" id="globalDateChk" onclick="Check_All_Options_By_Class(\'dateChk\', this.checked);"></th>'.NEW_LINE;
	            $x .= '</tr>'.NEW_LINE;
	        $x .= '</thead>'.NEW_LINE;
	    
	    $h_hiddenField = '';
	    $x .= '<tbody>'.NEW_LINE;
	    
	    // date time content
	    $i = 0;
	    $tableClass = 'no_bottom_right_border';
	    $prefixLocation = 'Location';
	    foreach((array)$courseScheduleAry as $_lessonAry) {
	        $rowNum = $i + 1;
	        
	        $eventDateID = $_lessonAry['EventDateID'];
	        $lessonTitle = intranet_htmlspecialchars($_lessonAry['Lesson']);
	        $lessonDateStart = $_lessonAry['ActivityDateStart'];
	        $lessonStartTime = date('H:i:s', strtotime($lessonDateStart));
	        $lessonDateEnd = $_lessonAry['ActivityDateEnd'];
	        $lessonEndTime = date('H:i:s', strtotime($lessonDateEnd));
	        $lessonDate = date('Y-m-d',strtotime($lessonDateStart));
	        $selectedLocationID = $_lessonAry['LocationID'];
	        $lessonTitleName = 'LessonTitle['.$eventDateID.']';
	        $lessonTitleID = 'LessonTitle['.$eventDateID.']';
	        
	        $capacity = $libenroll->getNumberOfStudentOfLessonClass($eventDateID);
	        $lessonClassAssoc = $libenroll->getLessonClass($eventDateID);
	         
	        if (count($lessonClassAssoc) == 1) {
	            $classInfo = array_values($lessonClassAssoc);
	            $classLocationID = $classInfo[0]['LocationID'];
	        }
	        else {
	            $classLocationID = '';
	        }
	        
//debug_pr($lessonClassAssoc);	        
	        $intakeClassList = $this->getIntakeClassList($intakeClassAssoc, $lessonClassAssoc, $eventDateID, $isNew=false, $isShowCapacity=false, $showSelectAll=true);
	        
// 	        $enrolBookingRelationAry = $libenroll->GetEnrolEbookingRelation($eventDateID);
//          $bookingID = count($enrolBookingRelationAry) ? $enrolBookingRelationAry['BookingRecordID'] : '';
	            
// 	        if ($lessonStartTime == $lessonEndTime && date('H:i:s',strtotime($lessonStartTime)) == '00:00:00') {
// 	            $bookingInfo = array();        // don't filter with booking condition if time is not selected
// 	        }
// 	        else {
	            $bookingInfo = array('Date'=>$lessonDate, 'StartTime'=>$lessonStartTime, 'EndTime'=>$lessonEndTime);
//	        }
	        $locationCol = 'LocationCol_'.$eventDateID.'_0';

	        $trainerAry = $libenroll->getLessonTrainer($eventDateID);
	        
	        $checkbox = $this->Get_Checkbox('dateChk_'.$eventDateID, 'dateChk_'.$eventDateID, $Value=1, $isChecked=0, $Class="dateChk", $Display="", $Onclick="Uncheck_SelectAll('globalDateChk', this.checked);");
	        
//	        if ($isInstructor && !in_array($_SESSION['UserID'], array_keys($trainerAry))) {
	        if ($isInstructor && !in_array($_SESSION['UserID'], array_keys($trainerAry)) && !$checkIsCourseCategoryInstructorExist) {
	           // script other trainer record    
	        }
	        else {
    	        $x .= '<tr>'.NEW_LINE;
    	        $x .= '<td class="tabletext rowNum">'.$rowNum.'</td>'.NEW_LINE;
    	        $x .= '<td class="tabletext">';
    	        $x .= '<input type="text" name="'.$lessonTitleName.'" id="'.$lessonTitleID.'" class="textboxtext" value="'.$lessonTitle.'">';
    	        $x .= '<span class="error_msg_hide" id="Err'.$lessonTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';
    	        $x .= '</td>'.NEW_LINE;
    	        
    	        $x .= '<td class="tabletext">'.$this->getDateTimeSelection($eventDateID, 0, $lessonDate, $lessonStartTime, $lessonEndTime, 'date').'</td>'.NEW_LINE;
    	        $x .= '<td class="tabletext">'.$this->getDateTimeSelection($eventDateID, 0, $lessonDate, $lessonStartTime, $lessonEndTime, 'time').'</td>'.NEW_LINE;
    	        
    	        $x .= '<td class="tabletext">'.$intakeClassList.'</td>'.NEW_LINE;    	        
    	        $x .= '<td class="tabletext">'.$this->getInstructorSelection($trainerAry, 0, $eventDateID, $tableClass, '', '40px', $categoryID).'</td>'.NEW_LINE;

    	        $x .= '<td class="tabletext" id="'.$locationCol.'">'.$this->getLocationSelection($prefixLocation, $selectedLocationID, 0, $eventDateID, $showCapacity=true, $capacity, $bookingInfo, $categoryID, $classLocationID).'</td>'.NEW_LINE;
    	        
    	        $x .= '<td>'.$checkbox.'</td>'.NEW_LINE;
    	        
    	        $x .= '</tr>'.NEW_LINE;
    	        
    	        
    	        $h_hiddenField .= $this->GET_HIDDEN_INPUT('EventDateID_'.$eventDateID, 'EventDateID[]', $eventDateID);
    	        $h_hiddenField .= $this->GET_HIDDEN_INPUT('OriginalLocationID_'.$eventDateID, 'OriginalLocationID[]', $selectedLocationID);    // for showing selected location when change date/time
	        
// 	        $h_hiddenField .= $this->GET_HIDDEN_INPUT('BookingID_'.$i, 'BookingID[]', $bookingID);
	        
	           $i++;
	        }
	    }
//	    $h_hiddenField .= $this->GET_HIDDEN_INPUT('Capacity', 'Capacity', $capacity);
	    
	    // no record display row
	    $displayPara = ($nrLesson==0)? '' : 'display:none;';
	    $x .= '<tr id="noRecordTr" style="'.$displayPara.'"><td colspan="8" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'.NEW_LINE;
	    $x .= '</tbody>'.NEW_LINE;
	    $x .= '</table>'.NEW_LINE;
	    $x .= $h_hiddenField;
	    
	    return $x;
	}

	// $rowCount - row index counter for new lesson
	// $rowNum - row number to be shown
	function getCourseScheduleRow($rowCount, $rowNum, $categoryID=0, $capacity=0, $intakeStartDate='', $intakeID=0) {
	    global $Lang, $eEnrollment, $enrolConfigAry, $libenroll, $sys_custom;
	    
	    $x = '';

	    $tableClass = 'no_bottom_right_border';
	    $prefixLocation = 'Location';
        $eventDateID = 0;      // dummy eventDateID
        $lessonTitle = '';
        $lessonDate = $intakeStartDate;
        $lessonStartTime = '00:00:00';
        $lessonEndTime = $lessonStartTime;
        $selectedLocationID = '';
        $lessonTitleName = 'NewLessonTitle_'.$rowCount;
        $lessonTitleID = $lessonTitleName;
        $bookingInfo = array('Date'=>$lessonDate, 'StartTime'=>$lessonStartTime, 'EndTime'=>$lessonEndTime);
        $locationCol = 'LocationCol_'.$eventDateID.'_'.$rowCount;
        $trainerAry = array();
        $dateChkID = 'dateChk_'.$eventDateID.'_'.$rowCount;
        $checkbox = $this->Get_Checkbox($dateChkID, $dateChkID, $Value=1, $isChecked=0, $Class="dateChk", $Display="", $Onclick="Uncheck_SelectAll('globalDateChk', this.checked);");
        $setOnChange = true;    // set onChange function
        
        $lessonClassAssoc = array();
        $intakeClassAssoc = $libenroll->getIntakeClass($intakeID);
        $intakeClassList = $this->getIntakeClassList($intakeClassAssoc, $lessonClassAssoc, $rowCount, $isNew=true, $isShowCapacity=false, $showSelectAll=true);

        $x .= '<tr>'.NEW_LINE;
            $x .= '<td class="tabletext rowNum">'.$rowNum.'</td>'.NEW_LINE;
            $x .= '<td class="tabletext">';
                $x .= '<input type="text" name="'.$lessonTitleName.'" id="'.$lessonTitleID.'" class="textboxtext" value="'.$lessonTitle.'">';
                $x .= '<span class="error_msg_hide" id="Err'.$lessonTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';
            $x .= '</td>'.NEW_LINE;
        
            $x .= '<td class="tabletext">'.$this->getDateTimeSelection($eventDateID, $rowCount, $lessonDate, $lessonStartTime, $lessonEndTime, 'date', $setOnChange).'</td>'.NEW_LINE;
            $x .= '<td class="tabletext">'.$this->getDateTimeSelection($eventDateID, $rowCount, $lessonDate, $lessonStartTime, $lessonEndTime, 'time', $setOnChange).'</td>'.NEW_LINE;
            
            $x .= '<td class="tabletext">'.$intakeClassList.'</td>'.NEW_LINE;
            $x .= '<td class="tabletext">'.$this->getInstructorSelection($trainerAry, $rowCount, $eventDateID, $tableClass, '', '40px', $categoryID).'</td>'.NEW_LINE;
            
            $x .= '<td class="tabletext" id="'.$locationCol.'">'.$this->getLocationSelection($prefixLocation, $selectedLocationID, $rowCount, $eventDateID, $showCapacity=true, $capacity, $bookingInfo, $categoryID).'</td>'.NEW_LINE;
            
            $x .= '<td>'.$checkbox.'</td>'.NEW_LINE;
        
        $x .= '</tr>'.NEW_LINE;
	        
	    return $x;
	}
	
	// $staffType = 1 => teaching staff
	function getStaffSelection($staffType=1, $onChange='changeStaff', $selected='')
	{
	    global $libenroll;
	    
	    $staffAry = $libenroll->getStaffList($staffType);
	    $x = getSelectByArray($staffAry, "name='StaffID' id='StaffID' onChange='{$onChange}(this)'", $selected);
	    return $x;
	}
	
	function getClassSelection($intakeID, $classID='', $name='IntakeClass', $firstTitle='')
	{
	    global $libenroll, $Lang;
	    
	    if ($intakeID) {
	        $classList = $libenroll->getIntakeClass($intakeID);
	    
    	    $data = array();
    	    foreach ((array)$classList as $_class) {
    	        $key = $_class['ClassID'];
    	        $val = $_class['ClassName'];
    	        $data[] = array($key, $val);
    	    }
    	    
    	    if (count($data) && $classID=='') {
    	        $selected = $data[0][0];        // select the first one
    	    }
    	    else {
    	        $selected = $classID;
    	    }
    	    
    	    $errID = 'Err'.$name;    	    
    	    $x = getSelectByArray($data, "name='$name' id='$name' onChange=\"changeClass(this)\"", $selected, $all=0, $noFirst=0, $firstTitle);
    	    $x .= '<span class="error_msg_hide" id="'.$errID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['selectIntake'].'</span>';
	    }
	    else {
	        $x = '';
	    }
	    return $x;
	}
	
	function getCourseSelectionByClass($classID='', $selected='', $intakeID='')
	{
	    global $libenroll, $Lang;
	    
	    $courseList = $libenroll->getIntakeCourseByClass($classID, $intakeID);

        $data = array();
        foreach ((array)$courseList as $_course) {
            $key = $_course['EnrolEventID'];
            $val = $_course['EventTitle'];
            $data[] = array($key, $val);
        }
        
        $name = 'Course';
        $errID = 'Err'.$name;
        $x = getSelectByArray($data, "name='$name' id='$name' onChange='changeCourse(this)' ", $selected, $all=0, $noFirst=0, $firstTitle);
        $x .= '<span class="error_msg_hide" id="'.$errID.'">'.$Lang['eEnrolment']['course']['warning']['selectCourse'].'</span>';
	    return $x;
	}
	
	function getAddLessonLayout($intakeClassID, $lessonDate, $startTime, $targetEndTime, $maxEndTime) {
	    global $Lang, $libenroll;
	    
	    if ($intakeClassID) {
	        $intakeClassAry = $libenroll->getIntakeClass('',$intakeClassID);
	        if (count($intakeClassAry)) {
	            $classLocationID = $intakeClassAry[$intakeClassID]['LocationID'];
	            $intakeID = $intakeClassAry[$intakeClassID]['IntakeID'];
	            $capacity = $intakeClassAry[$intakeClassID]['NumberOfStudent'];
	            
	            $intakeAry = $libenroll->getIntake($intakeID);
	            $intakeStartDate = $intakeAry[$intakeID]['StartDate'];
	            
	            $intakeClassAssoc = $libenroll->getIntakeClass($intakeID);	
	            $lessonClassAssoc[$intakeClassID] = true;
	            $intakeClassList = $this->getIntakeClassList($intakeClassAssoc, $lessonClassAssoc, 0, false, true);
	            
	        }
	        else {
	            $classLocationID = '';
	            $intakeID = '';
	            $capacity = 0;
	            $intakeStartDate = '';
	        }
	    }
	    else {
	        $classLocationID = '';
	        $intakeID = '';
	        $capacity = 0;
	        $intakeStartDate = '';
	    }

	    // default trainer is the login user for instructor
	    $isInstructor = $libenroll->isInstructor();
	    if ($isInstructor) {
	        $trainerAry = array($_SESSION['UserID'] => Get_Lang_Selection($_SESSION['ChineseName'],$_SESSION['EnglishName']));
	    }
	    else {
	        $trainerAry = array();
	    }
	    $tableClass = 'no_bottom_right_border';
	    $categoryID = 0;
	    $prefixLocation = 'Location';
	    $selectedLocationID = '';
	    $bookingInfo = array('Date'=>$lessonDate, 'StartTime'=>$startTime, 'EndTime'=>$targetEndTime);
	    
	    $courseSelection = $this->getCourseSelectionByClass($intakeClassID, '', $intakeID);
	    
	    $para = array();
	    $para['CourseSelection'] = $courseSelection;
	    $lessonTitle = '';
	    $para['SelectLessonTitle'] = $this->getLessonSelectionByCourse($courseID='', $lessonTitle);
	    $para['IntakeClassList'] = $intakeClassList;
	    $para['LessonDate'] = $lessonDate;
	    $para['LessonTime'] = $this->getDateTimeSelection(0, 0, $lessonDate, $startTime, $targetEndTime, 'time', $setOnChange=true); 
	    $para['Instructor'] = $this->getInstructorSelection($trainerAry, 0, 0, $tableClass, '', '40px', $categoryID, $parFieldID='', $parFieldName='', $parFieldClass='', $onClick='chooseInstructor()');

	    $para['Location'] = $this->getLocationSelection($prefixLocation, $selectedLocationID, 0, 0, $showCapacity=true, $capacity, $bookingInfo, $categoryID, $classLocationID);
	    $para['HidLessonDate'] = $lessonDate;
	    $para['HidMinStartTime'] = $startTime;
	    $para['HidMaxEndTime'] = $maxEndTime;
	    $para['HidOriginalLocationID'] = $selectedLocationID;
	    $para['HidSaveButton'] = 'add';

	    $form = $this->getAddEditLessonForm($para);
	    $js = $this->getAddEditLessonJs('add');
	    
	    return $form . $js;
	}
	
	function getCombinedLessonTemplateTable($templateID, $intakeID='')
	{
	    global $libenroll, $Lang, $i_no_record_exists_msg;
	    
	    $courseAssoc = $libenroll->getCombinedLessonTemplate($templateID);
	    $classAssoc = $libenroll->getIntakeClass($intakeID);
	    $nrCourse = count($courseAssoc);
	    
	    $x = '<table class="common_table_list">'.NEW_LINE;
	    $x .= '<tr>'.NEW_LINE;
	    $x .= '<th width="25%" style="min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['title'].'</th>'.NEW_LINE;
	    $x .= '<th width="20%" style="min-width:50px;">'.$Lang['eEnrolment']['curriculumTemplate']['course']['code'].'</th>'.NEW_LINE;
	    $x .= '<th width="25%" style="min-width:200px;"><span class="tabletextrequire">*</span> '.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].'</th>'.NEW_LINE;
	    $x .= '<th width="25%">'.$Lang['eEnrolment']['curriculumTemplate']['course']['classCombinationAndInstructor'].'</th>'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    if ($nrCourse) {
	        foreach((array)$courseAssoc as $_courseID=>$_course) {
	            $_courseTitle = intranet_htmlspecialchars($_course['CourseTitle']);
	            $_categoryID = $_course['CategoryID'];
	            $_courseTitleName = 'CombinedLessonCourseTitle['.$_courseID.']';
	            $_courseTitleID = $_courseTitleName;
	            $_courseCodeName = 'CombinedLessonCourseCode['.$_courseID.']';
	            $_courseCodeID = $_courseCodeName;
	            $_courseDescriptionName = 'CombinedLessonCourseDescription['.$_courseID.']';
	            $_courseDescription = intranet_htmlspecialchars($_course['Description']);
	            
	            $_lessonAssoc = $_course['LessonAssoc'];
	            $_nrLesson = count($_lessonAssoc);
	            
	            $i = 0;
	            foreach((array)$_lessonAssoc as $__lessonID=>$__lesson) {
	                if ($i == 0) {
	                    $__courseTitleField = '<input type="text" name="'.$_courseTitleName.'" id="'.$_courseTitleID.'" class="textboxtext" value="'.$_courseTitle.'">';
	                    $__courseTitleField.= '<span class="error_msg_hide" id="Err'.$_courseTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseTitle'].'</span>';
	                    $__courseCodeField = '<input type="text" name="'.$_courseCodeName.'" id="'.$_courseCodeID.'" class="textboxtext" value="">';
	                    $__courseCodeField .= '<span class="error_msg_hide" id="Err'.$_courseCodeID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseCode'].'</span>';
	                    $__courseDescriptionField = '<input type="hidden" name="'.$_courseDescriptionName.'" value="'.$_courseDescription.'">';
	                }
	                else {
	                    $__courseTitleField = '';
	                    $__courseCodeField = '';
	                    $__courseDescriptionField = '';
	                }
	                
	                $__lessonTitle = intranet_htmlspecialchars($__lesson['LessonTitle']);
	                $__lessonTitleName = 'CombinedLessonTitle['.$_courseID.']['.$__lessonID.']';
	                $__lessonTitleID = 'CombinedLessonTitle['.$_courseID.']['.$__lessonID.']';
	                
	                $x .= '<tr>'.NEW_LINE;
	                if ($i == 0) {
	                    $x .= '<td class="tabletext" rowspan="'.$_nrLesson.'">'.$__courseTitleField. $__courseDescriptionField . '</td>'.NEW_LINE;
    	                $x .= '<td class="tabletext" rowspan="'.$_nrLesson.'">'.$__courseCodeField.'</td>'.NEW_LINE;
	                }
	                
	                $x .= '<td class="tabletext">';
	                $x .= '<input type="text" name="'.$__lessonTitleName.'" id="'.$__lessonTitleID.'" class="textboxtext" value="'.$__lessonTitle.'">';
	                $x .= '<span class="error_msg_hide" id="Err'.$__lessonTitleID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';
	                $x .= '</td>'.NEW_LINE;
	                
	                $__courseLessonInstructor = $this->getCombinedLessonTemplateInstructor($_courseID, $__lessonID, $rowID=0, $_categoryID, $classAssoc);
	                $__courseLessonID = $_courseID.'_'.$__lessonID;
	                $__courseLessonDivID = 'CourseLessonDiv_'.$__courseLessonID;
	                $__courseLessonCounter = 'CourseLessonCounter_'.$__courseLessonID;
	                
	                $hiddenCounter = '<input type="hidden" name="'.$__courseLessonCounter.'" id="'.$__courseLessonCounter.'" value="1">';
	                $addBtn = '<div><span class="table_row_tool"><a class="newBtn add" onclick="javascript:addCombinedLessonClassInstructor(this)" title="'.$Lang['eEnrolment']['curriculumTemplate']['course']['addClassCombinationAndInstructor'].'" data-CourseLessonID="'.$__courseLessonID.'"></a><br></span>';
	                $addBtn .= '<span class="error_msg_hide" id="ErrClassAddBtn">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['selectAtLeastOneClass'].'</span>';
	                $addBtn .= '<span class="error_msg_hide" id="ErrCourseLesson_'.$__courseLessonID.'">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['duplicateClassSelection'].'</span></div>';
	                
	                $x .= '<td class="tabletext"><div id="'.$__courseLessonDivID.'">'.$__courseLessonInstructor.'</div>'.$addBtn.$hiddenCounter.'</td>'.NEW_LINE;
	                $x .= '</tr>'.NEW_LINE;
	                
	                $i++;
	            }   // end lesson loop
 	        }  // end course loop
	    }      // nrOfCourse > 0
	    else {
	        $x .= '<tr><td class="tableContent" align="center" colspan="4"><br>'.$i_no_record_exists_msg.'<br></td></tr>'.NEW_LINE;
	    }
	    
	    $x .= '</table>'.NEW_LINE;
	    
	    return $x;
	}
	
	function getCombinedLessonTemplateInstructor($courseID, $lessonID, $rowID, $categoryID, $classAssoc)
	{
	    global $Lang;
	    
	    $classInstructorID = $courseID.'_'.$lessonID.'_'.$rowID;
	    
	    $classList = '';
	    if (is_array($classAssoc)) {
	        $i = 0;
	        foreach((array)$classAssoc as $_class) {
	            $_classID = $_class['ClassID'];
	            $_className = $_class['ClassName'];
	            $_fieldID = 'CombinedLessonClass_'.$classInstructorID.'_'.$_classID;
	            $_fieldName = 'CombinedLessonClass['.$courseID.']['.$lessonID.']['.$rowID.']['.$_classID.']';
	            
	            if ($i > 0) {
	                $classList .= '<br>';
	            }
	            $classList .= '<input type="checkbox" name="'.$_fieldName.'" id="'.$_fieldID.'" class="CombinedLessonClass" value="1"><label for="'.$_fieldID.'">'. $_className.'</label>';
	            $i++;
	        }
	    }
	    
	    $tableClass = 'no_bottom_right_border';
	    
	    $instructorAry = array();
	    $instructorSelClass = 'CombinedLessonInstructorSel_'.$categoryID;
	    $parFieldID = 'CombinedLessonInstructor_'.$classInstructorID;
	    $parFieldName = 'CombinedLessonInstructor['.$courseID.']['.$lessonID.']['.$rowID.'][]';
	    $hidFieldName = 'HidClassCombination['.$courseID.']['.$lessonID.']['.$rowID.']';
	    $x = '<div id="DivCourseLessonRow_'.$classInstructorID.'">'.NEW_LINE;
	    $x .= '<table class="'.$tableClass.'">'.NEW_LINE;
	    $x .= '<tr><td rowspan="2">'.$classList.'</td>'.NEW_LINE;
	    $x .= '<td class="tabletext">'.$this->getInstructorSelection($instructorAry, $rowID, $courseID, $tableClass, $classID='', '40px', $categoryID, $parFieldID, $parFieldName, $instructorSelClass).'</td>'.NEW_LINE;
	    $x .= '<input type="hidden" name="'.$hidFieldName.'" id="'.$hidFieldName.'" value="1">'.NEW_LINE;
	    $x .= '</tr>'.NEW_LINE;
	    
	    $x .= '<tr><td><span class="table_row_tool"><a class="delete" title="' . $Lang['eEnrolment']['curriculumTemplate']['course']['removeClassAndInstructor']. '" onClick="removeCombinedLessonInstructor(this)" data-ClassInstructorID="'.$classInstructorID.'"></a></span></td></tr>'.NEW_LINE;
	    $x .= '</table>'.NEW_LINE;
	    $x .= '<hr style="width: 100%;" >'.NEW_LINE;
	    $x .= '</div>';
	    return $x;
	}
	
	function getCombinedLessonLayout($templateID, $intakeID='')
	{
	    global $Lang;
	        
        $combinedLessonTable = $this->getCombinedLessonTemplateTable($templateID, $intakeID);
        
        $x = '<tr>'.NEW_LINE;
        $x .= '<td><br>'.NEW_LINE;
        $x .= $this->GET_NAVIGATION2($Lang['eEnrolment']['curriculumTemplate']['course']['combinedLesson']);
        $x .= '</td>'.NEW_LINE;
        $x .= '</tr>'.NEW_LINE;
        
        $x .= '<tr>'.NEW_LINE;
        $x .= '<td><br>'.NEW_LINE;
        $x .= '<span id="CombinedCourseTable">'.$combinedLessonTable .'</span>';
        $x .= '</td>'.NEW_LINE;
        $x .= '</tr>'.NEW_LINE;
        
        return $x;
	}
	
	function getIntakeClassList($intakeClassAssoc, $lessonClassAssoc, $eventDateID, $isNew=false, $isShowCapacity=false, $showSelectAll=false)
	{
	    global $Lang;
	    
	    $classList = '';
	    if (is_array($intakeClassAssoc)) {
	        $i = 0;
	        $prefix = $isNew ? 'New' : ''; 
	        foreach((array)$intakeClassAssoc as $_class) {
	            $_classID = $_class['ClassID'];
	            $_className = $_class['ClassName'];	            
	            $_capacity = $_class['NumberOfStudent'];
	            if ($isShowCapacity) {
	                $_className .= ' ('.$_capacity.')';   
	            }
	            $_locationID = $_class['LocationID'];
	            $_fieldID = $prefix.'LessonClass_'.$eventDateID.'_'.$_classID;
	            $_fieldName = $prefix.'LessonClass['.$eventDateID.']['.$_classID.']';
	            $_checked = $lessonClassAssoc[$_classID] ? 'checked' : '';
	            
	            if ($i > 0) {
	                $classList .= '<br>';
	            }
	            
	            if ($showSelectAll && $i==0) {
	                $_allID = $prefix.'LessonClassGroup_'.$eventDateID.'_0';
	                $_allName = $prefix.'LessonClassGroup['.$eventDateID.'][0]';
	                
	                $classList .= '<div style="float:right"><input type="checkbox" name="'.$_allName.'" id="'.$_allID.'" class="LessonClassGroup" value="1" onChange="changeLessonClassGroup(this)"><label for="'.$_allID.'">'. $Lang['Btn']['All'].'</label></div>';
	            }
	            
	            $classList .= '<input type="checkbox" name="'.$_fieldName.'" id="'.$_fieldID.'" class="LessonClass" value="1" data-capacity="'.$_capacity.'" data-locationID="'.$_locationID.'" '.$_checked.' onChange="changeLessonClass(this)"><label for="'.$_fieldID.'">'. $_className.'</label>';
	            $i++;
	        }
	        $errorID = 'Err'.$prefix.'LessonClass_'.$eventDateID;
	        $classList .= '<br><span class="error_msg_hide" id="'.$errorID.'"></span>';
	    }
	    return $classList;
	}
	
	function getAddEditLessonForm($para)
	{
	    global $Lang;

	    $x  = '<div id="divAddLessonThickBox">';
	    $x .= '<form id="form1" name="form1">';
	    $x .= '<br>';
	    $x .= '<table width="90%" class="form_table_v30">';
	    $x .= '<tr>';
	    $x .= '<td class="field_title" width="30%"><span class="tabletextrequire">*</span>' . $Lang['eEnrolment']['curriculumTemplate']['course']['title']. '</td>';
	    $x .= '<td width="70%">'.$para['CourseSelection'].'</td>';
	    $x .= '</tr>';
	    
	    $x .= '<tr>';
	    $x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['eEnrolment']['curriculumTemplate']['course']['lesson']. '</td>';
	    $x .= '<td class="tabletext">';
	    $x .= '<input type="radio" name="LessonInputBy" id="LessonInputBySelect" value="1" checked>'.$para['SelectLessonTitle'];
	    $x .= '<br>';
		$x .= '<div style="white-space:nowrap;"><input type="radio" name="LessonInputBy" id="LessonInputByText" value="0">';
 	    $x .= '<input type="text" name="LessonTitle" id="LessonTitle" class="textboxtext" value=""></div>';
	    $x .= '<span class="error_msg_hide" id="ErrLessonTitle">'.$Lang['eEnrolment']['curriculumTemplate']['warning']['inputLessonTitle'].'</span>';
	    $x .= '</td>';
	    $x .= '</tr>';
	    
	    $x .= '<tr>';
	    $x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['eEnrolment']['curriculumTemplate']['course']['class'] . '</td>';
	    $x .= '<td class="tabletext">'.$para['IntakeClassList'].'</td>';
	    $x .= '</tr>';
	    
	    $x .= '<tr>';
	    $x .= '<td class="field_title">'.(($para['HidSaveButton'] == 'edit') ? '<span class="tabletextrequire">*</span>' : ''). $Lang['General']['Date']. '</td>';
	    $x .= '<td class="tabletext">'.$para['LessonDate'].'</td>';
	    $x .= '</tr>';
	    
	    $x .= '<tr>';
	    $x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['General']['Time']. '</td>';
	    $x .= '<td class="tabletext">'.$para['LessonTime'].'</td>';
	    $x .= '</tr>';
	    
	    $x .= '<tr>';
	    $x .= '<td class="field_title">' . $Lang['eEnrolment']['curriculumTemplate']['intake']['instructor']. '</td>';
	    $x .= '<td class="tabletext">'.$para['Instructor'].'</td>';
	    $x .= '</tr>';
	    
	    $x .= '<tr>';
	    $x .= '<td class="field_title">' . $Lang['eEnrolment']['curriculumTemplate']['intake']['locationCapacity']. '</td>';
	    $x .= '<td id="LocationCol_0_0">' . $para['Location']. '</td>';
	    $x .= '</tr>';
	    
	    $x .= '</table>';
	    $x .= '<br/>';
	    $x .= '<div align="center">';
	    
	    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "", "btnSaveLesson", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
	    $x .= "&nbsp;&nbsp;";
	    $x .= $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "tb_remove();", "btnCancelLesson", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
	    
	    if ($para['HidSaveButton'] == 'add') {
	        $x .= $this->GET_HIDDEN_INPUT('MinStartTime', 'MinStartTime', $para['HidMinStartTime']);
	        $x .= $this->GET_HIDDEN_INPUT('MaxEndTime', 'MaxEndTime', $para['HidMaxEndTime']);
	        $x .= $this->GET_HIDDEN_INPUT('LessonDate_0_0', 'LessonDate_0_0', $para['HidLessonDate']);
	    }
	    else {
	        $x .= "&nbsp;&nbsp;";
	        $x .= $this->GET_ACTION_BTN($Lang['Btn']['Delete'], "button", "", "btnDeleteLesson", $ParOtherAttribute="", $ParDisabled=0, $ParClass="formbutton_alert", $ParExtraClass="actionBtn");
	        
    	    $x .= $this->GET_HIDDEN_INPUT('EventDateID', 'EventDateID', $para['HidEventDateID']);
    	    $x .= $this->GET_HIDDEN_INPUT('IntakeStartDate', 'IntakeStartDate', $para['HidIntakeStartDate']);
    	    $x .= $this->GET_HIDDEN_INPUT('IntakeEndDate', 'IntakeEndDate', $para['HidIntakeEndDate']);
	    }
	    
	    $x .= $this->GET_HIDDEN_INPUT('OriginalLocationID_0', 'OriginalLocationID[]', $para['HidOriginalLocationID']);     // for showing selected location when change date/time
	    $x .= $this->GET_HIDDEN_INPUT('CourseCategoryID', 'CourseCategoryID', $para['HidCourseCategoryID']);
	    $x .= "&nbsp;&nbsp;";
	    $x .= '</div>';
	    $x .= '</form></div>';
	    
	    return $x;
	}
	
	
	function getAddEditLessonJs($actionType, $intakeStartDate='', $intakeEndDate='')
	{
	    global $Lang;
	    
	    ob_start();
?>
<script>
function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
	if ($.trim($('select#Course').val()) == '') {
 		error++;
 		if (focusField == '') focusField = 'Course';
		$('#ErrCourse').addClass('error_msg_show').removeClass('error_msg_hide');
	} 	

	if ($('input#LessonInputBySelect').is(':checked') && $('#LessonTitleSelect').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'LessonTitleSelect';
		$('#ErrLessonTitleSelect').addClass('error_msg_show').removeClass('error_msg_hide');
	}

	if ($('input#LessonInputByText').is(':checked') && $.trim($('input#LessonTitle').val()) == '') { 	
 		error++;
 		if (focusField == '') focusField = 'LessonTitle';
		$('#ErrLessonTitle').addClass('error_msg_show').removeClass('error_msg_hide');
	} 	

	if ($('input.LessonClass:checked').length < 1) {
 		error++;
 		if (focusField == '') focusField = $('input.LessonClass:first').attr('id');
		$('#ErrLessonClass_0').addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['selectAtLeastOneClass'];?>');
	}

	var eventDateID = 0;		// this is different from the actual EventDateID
	var row = 0;
	var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();

	if (startHour == 0 && startMinute == 0 && endHour == 0 && endMinute == 0) {
 		error++;
 		if (focusField == '') {
 	 		focusField = 'StartHour\\\['+eventDateID+'\\\]\\\['+row+'\\\]';
 		}
 		$('#ErrTimeLogic_' + eventDateID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['report']['warning']['selectTimeRange'];?>');
	}
	
	if (!validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
 		error++;
 		if (focusField == '') {
 	 		focusField = 'StartHour\\\['+eventDateID+'\\\]\\\['+row+'\\\]';
 		}
 		$('#ErrTimeLogic_' + eventDateID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
	}
	
<?php if ($actionType == 'add'):?>
	var minStartTime = $('input#MinStartTime').val();
	var minStartHour = minStartTime.substr(0,2);
	var minStartMinute = minStartTime.substr(3,2);
	
	if (!validateTimeLogic(minStartHour, minStartMinute, startHour, startMinute)) {
 		error++;
 		if (focusField == '') {
 	 		focusField = 'StartHour\\\['+eventDateID+'\\\]\\\['+row+'\\\]';
 		}
		$('#ErrTimeLogic_' + eventDateID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['startTimeOutbound'];?>');
	} 

	var maxEndTime = $('input#MaxEndTime').val();
	var maxEndHour = maxEndTime.substr(0,2);
	var maxEndMinute = maxEndTime.substr(3,2);
	
	if (!validateTimeLogic(endHour, endMinute, maxEndHour, maxEndMinute)) {
 		error++;
 		if (focusField == '') {
 	 		focusField = 'StartHour\\\['+eventDateID+'\\\]\\\['+row+'\\\]';
 		}
		$('#ErrTimeLogic_' + eventDateID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['timeConflict'];?>');
	} 

<?php else:?>
	var lessonDate = $.trim($('input#LessonDate_0_0').val());
	var lessonID = $('input#LessonDate_0_0').attr('id'); 
    var intakeStartDate = '<?php echo $intakeStartDate;?>';
    var intakeEndDate = '<?php echo $intakeEndDate;?>';

	if (lessonDate == '') {
 		error++;
 		if (focusField == '') focusField = 'LessonDate_0_0';
//		$('#ErrLessonDate_0_0').addClass('error_msg_show').removeClass('error_msg_hide');
	} 	
    
	if (compareDate(lessonDate, intakeStartDate) == '-1') {
		error++;
		if (focusField == '') focusField = lessonID;
		$('#Err' + lessonID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['beforeStartDate'];?>');
	}
	else if (compareDate(intakeEndDate, lessonDate) == '-1') {
		error++;
		if (focusField == '') focusField = lessonID;
		$('#Err' + lessonID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['afterEndDate'];?>');
	} 	

<?php endif;?>

	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {
		return true;
	}
}

function validateTimeLogic(startHour, startMinute, endHour, endMinute)
{
	var hourStart = (startHour=='')? '00' : str_pad(startHour, 2);
	var minStart = (startMinute=='')? '00' : str_pad(startMinute, 2);
	var timeTextStart = hourStart + minStart;
	
	var hourEnd = (endHour=='')? '00' : str_pad(endHour, 2);
	var minEnd = (endMinute=='')? '00' : str_pad(endMinute, 2);
	var timeTextEnd = hourEnd + minEnd;
	if (timeTextStart > timeTextEnd) {
		return false;
	}
	else {
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID)
{
	var capacity = 0;
	var classLocationID = '';
	var numberOfSelectedClass = 0;
	
	$('input:checkbox[id^="LessonClass_' + eventDateID + '"]:checked').each(function(){
		capacity += parseInt($(this).attr('data-capacity'));
		classLocationID = $(this).attr('data-locationID');
		numberOfSelectedClass++;
	});
	if (numberOfSelectedClass > 1) {
		classLocationID = '';
	}

	$.ajax({
		dataType: "json",
		async: true,			
		type: "POST",
		url: '../../curriculum_template/ajax.php',
		data : {
			'action': 'getAvailableLocationListInSchedule',
			'lessonDate': lessonDate,
			'startHour': startHour,
			'startMinute': startMinute,
			'endHour': endHour,
			'endMinute': endMinute,
			'eventDateID': eventDateID,
			'row': row,
			'capacity': capacity,
			'originalLocationID': originalLocationID,
			'classLocationID': classLocationID,
			'categoryID': $('input#CourseCategoryID').val()
		},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#LocationCol_'+eventDateID+'_'+row).html(ajaxReturn.html);
				isLoading = false;
			}
		},
		error: show_ajax_error
	});
}

function changeDateTimeSelect(obj)
{
	$this = $(obj);	
	
	var eventDateID = 0;	
	var row = 0;
	var lessonDate = $('input#LessonDate_0_0').val();
	var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var originalLocationID = $('#OriginalLocationID_'+eventDateID).val();
	
	var errMsgObj = $('#ErrTimeLogic_' + eventDateID + '_' + row);
	
	if ($('input#CourseCategoryID').val() == '') {
		$('span#ErrCourse').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	else if (validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
        isLoading = true;
		$('#LocationCol_'+eventDateID+'_'+row).html(loadingImg);
 	 		
		$('[id^="ErrTimeLogic_"]').addClass('error_msg_hide').removeClass('error_msg_show');
		getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID);
	}
	else {
		errMsgObj.addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
	}
}

function changeLessonDate(obj)
{
	var $this = $(obj);
	var lessonDateID = $this.attr('id');
	var str = lessonDateID.split('_');
	var prefix = str[0];
	var eventDateID = str[1];
	var row = str[2];
	var lessonDate = $this.val();
	var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var originalLocationID = $('#OriginalLocationID_'+eventDateID).val();
	var intakeStartDate = '<?php echo $intakeStartDate;?>';
	var intakeEndDate = '<?php echo $intakeEndDate;?>';
	
	if (compareDate(lessonDate, intakeStartDate) == '-1') {
		$('#Err' + lessonDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['beforeStartDate'];?>');
	}
	else if (compareDate(intakeEndDate, lessonDate) == '-1') {
		$('#Err' + lessonDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['afterEndDate'];?>');
	}
	else { 	
		$('#Err' + lessonDateID).addClass('error_msg_hide').removeClass('error_msg_show');
	}

    isLoading = true;
    $('#LocationCol_'+eventDateID+'_'+row).html(loadingImg);
	getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID);
}

function changeLessonClass(obj)
{
	$this = $(obj);
	var id = $this.attr('id');
	var ary = id.split('_');	// 2D array
	var name = ary[0];
	var eventDateID = ary[1];
	var row = 0;
	var originalLocationID = $('#OriginalLocationID_'+eventDateID).val();
	var lessonDate = $('#LessonDate_'+eventDateID+'_'+row).val();
	var startHour = $('#StartHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var startMinute = $('#StartMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endHour = $('#EndHour\\['+eventDateID+'\\]\\['+row+'\\]').val();
	var endMinute = $('#EndMinute\\['+eventDateID+'\\]\\['+row+'\\]').val();
	
	var errMsgObj = $('#ErrTimeLogic_' + eventDateID + '_' + row);
	if (validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
        isLoading = true;
		$('#LocationCol_'+eventDateID+'_'+row).html(loadingImg);
 	 		
		$('[id^="ErrTimeLogic_"]').addClass('error_msg_hide').removeClass('error_msg_show');
		getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, eventDateID, row, originalLocationID);
	}
	else {
		errMsgObj.addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
	}
}

function addLessonUpdate()
{
	$.ajax({
		dataType: "json",
		async: true,			
		type: "POST",
		url: 'add_lesson_update.php',
		data : $('#form1').serialize(),
		success: function(ajaxReturn){
			if (ajaxReturn != null){
				var result = ajaxReturn.updateResult;
				isLoading = false;
				if (result == 'AddSuccess') {
					refreshReport();
					Get_Return_Message("<?php echo $Lang['General']['ReturnMessage']['AddSuccess'];?>");
				}
				else {
					Get_Return_Message("<?php echo $Lang['General']['ReturnMessage']['AddUnsuccess'];?>");
				}
			}
		},
		error: show_ajax_error
	});
}

function editLessonUpdate()
{
	$.ajax({
		dataType: "json",
		async: true,			
		type: "POST",
		url: 'edit_lesson_update.php',
		data : $('#form1').serialize(),
		success: function(ajaxReturn){
			if (ajaxReturn != null){
				var result = ajaxReturn.updateResult;
				isLoading = false;
				if (result == 'UpdateSuccess') {
					refreshReport();
					Get_Return_Message("<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>");
				}
				else {
					Get_Return_Message("<?php echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>");
				}
			}
		},
		error: show_ajax_error
	});
}

function chooseInstructor()
{
	if ($('input#CourseCategoryID').val() == '') {
		$('span#ErrCourse').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	else {	
		newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname=Instructor[0][0][]&ppl_type=pic&permitted_type=1&categoryID=' + $('input#CourseCategoryID').val(), 9);
	}
}

function checkeBooking4LessonUpdate()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '../../curriculum_template/ajax.php?action=checkeBooking4OneLessonUpdate',
		data : $('#form1').serialize(),
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				
				var isRoomBooked = ajaxReturn.html;
				var timeConflict = ajaxReturn.html2;
				var error = ajaxReturn.error;
				var row, ary;
				var isBooked = false;
				var isTimeConflict = false;
				var errorAry, errorDescription, i;
				
				$(':input[name^="Location"]').each(function(){
				    var thisID = $(this).attr('id');
				    thisID = thisID.replace(/\[/g,"\\\[");
				    thisID = thisID.replace(/\]/g,"\\\]");
					ary = $(this).attr('id').split('\]\[');	// 2D array
			 		eventDateID = ary[0].replace('Location\[','');
			 		row = ary[1].replace('\]','');

			 		if ((isRoomBooked != '') && (typeof isRoomBooked != 'undefined') && (isRoomBooked == true)) {
						errorAry = error[eventDateID];
						errorDescription = '';
						for(i=0; i<errorAry.length;i++) {
							switch (errorAry[i]) {
								case '101':
									errorDescription += '<?php echo $Lang['eEnrolment']['booking']['unavailablePeriod'];?>';
									break;
								case '301':
									errorDescription += '<?php echo $Lang['eEnrolment']['booking']['violateBookingRule'];?>';
									break;
								case '201':
								default:	
									errorDescription += '<?php echo $Lang['eEnrolment']['booking']['roomBooked'];?>';
									break;
							}
							if (i != error[eventDateID].length - 1) {
								errorDescription += ',';
							}
							
						}
						
						isBooked = true;
						//$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
						$('#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide').html(errorDescription);
						$('input.actionBtn').attr('disabled', '');
					}

			 		if ((timeConflict != '') && (typeof timeConflict[0][0] != 'undefined')) {
			 			for (i=0; i<timeConflict.length;i++) {
							switch (timeConflict[i][0]) {
    							case '102':
    								$('#ErrInstructor\\['+eventDateID+'\\]\\[0\\]').addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable'];?>');
    								break;
//     							case '202':
//    								$('#ErrLessonClass_'+eventDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictClass'];?>' + timeConflict[i][1]);
//     								break;
    							default:
    								$('#ErrNewLessonClass_'+eventDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictUnknown'];?>');
    								break;
							}
						}
			 			
			 			isTimeConflict = true;    					 			
						$('input.actionBtn').attr('disabled', '');
					}
					
				});

				if (!isBooked && !isTimeConflict) {
					if ($('input#EventDateID').val() > 0) {
						editLessonUpdate();
					}
					else {
						addLessonUpdate();
					}
					$('input.actionBtn').attr('disabled', '');
					tb_remove();
        		}
			}
			else {
				$('input.actionBtn').attr('disabled', '');
			}
		},
		error: function(){
			$('input.actionBtn').attr('disabled', '');
			show_ajax_error();
		}
	});
}

function setLessonBy()
{
	$('input#LessonInputBySelect').attr('checked',true);
}

$(document).ready(function(){

	$('input#btnSaveLesson').click(function(e){
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

	    $(':select[name*="Instructor"]').each(function(){
		    var thisID = $(this).attr('id');
		    thisID = thisID.replace(/\[/g,"\\\[");
		    thisID = thisID.replace(/\]/g,"\\\]");
			$('#' + thisID + ' option').attr('selected',true);
	    });
	    
		if (checkForm(document.form1)) {
			if ($('select#Instructor\\[0\\]\\[0\\]').val() == '' || $('select#Instructor\\[0\\]\\[0\\]').val() == null) {
				checkeBooking4LessonUpdate();
			}
			else {
    			$.ajax({
    				dataType: "json",
    				type: "POST",
    				url: '../../curriculum_template/ajax.php?action=checkIsAllowedInstructor',
    				data : {
    					'Course': $('select#Course').val(),
    					'Instructor[]': $('select#Instructor\\[0\\]\\[0\\]').val()		
    				},
    				success: function(ajaxReturn){
    					if (ajaxReturn != null && ajaxReturn.success){
    						var isInstructorAllowed = ajaxReturn.html;
    						if (isInstructorAllowed == true) {	
    							checkeBooking4LessonUpdate();
    						}
    						else {
    							$('span#ErrInstructor\\[0\\]\\[0\\]').addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['mixupCourseInstructor'];?>');
    							$('input.actionBtn').attr('disabled', '');
    						}
    					}
    					else {
    						$('input.actionBtn').attr('disabled', '');
    					}
    				},
        			error: function(){
        				$('input.actionBtn').attr('disabled', '');
        				show_ajax_error();
        			}
    			});
			}
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});


	$('input#btnDeleteLesson').click(function(e){
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    if(confirm("<?php echo $Lang['eEnrolment']['course']['warning']['confirmDeleteLesson'];?>")){
	    	$.ajax({
	    		dataType: "json",
	    		async: true,			
	    		type: "POST",
	    		url: 'delete_lesson.php',
	    		data : $('#form1').serialize(),
	    		success: function(ajaxReturn){
	    			if (ajaxReturn != null){
	    				var result = ajaxReturn.deleteResult;
	    				isLoading = false;
	    				if (result == 'DeleteSuccess') {
	    					refreshReport();
	    					Get_Return_Message("<?php echo $Lang['General']['ReturnMessage']['DeleteSuccess'];?>");
	    				}
	    				else {
	    					Get_Return_Message("<?php echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];?>");
	    				}
	    				tb_remove();
	    			}
	    		},
	    		error: show_ajax_error
	    	});
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});


	$('input#LessonTitle').focus(function(){
		$('input#LessonInputByText').attr('checked',true);
	});

});

</script>	    

<?php 	    
	    $x = ob_get_contents();
	    ob_end_clean();
	    
	    return $x;
	    
	}      // end getAddEditLessonJs()
	
	
	// 2018-12-18: void $intakeClassID as it can select multiple classes  
	function getEditLessonLayout($eventDateID, $intakeClassID) {
	    global $Lang, $libenroll;
	    
	    // set default values
	    $classLocationID = '';
	    $intakeID = '';
	    $intakeClassList = '';
	    $capacity = 0;	    
	    
	    $enrolEventID = '';
	    $lessonStartTime = '';
	    $lessonEndTime = '';
	    $lessonTitle = '';
	    $selectedLocationID = '';
	    $categoryID = 0;
	    $lessonDate = '';
	    $intakeStartDate = '';
	    $intakeEndDate = '';
	    $intakeClassIDAry = array();
	    
	    if ($eventDateID) {
	        $eventDateAry = $libenroll->GET_ENROL_EVENT_DATE('', '', '', $eventDateID);
	        if (count($eventDateAry)) {
	            $eventDateAry = $eventDateAry[0];
	            $enrolEventID = $eventDateAry['EnrolEventID'];
	            $lessonDateStart = $eventDateAry['ActivityDateStart'];
	            $lessonDateEnd = $eventDateAry['ActivityDateEnd'];
	            $lessonTitle = $eventDateAry['Lesson'];
	            $selectedLocationID = $eventDateAry['LocationID'];
	            $lessonStartTime = date('H:i:s', strtotime($lessonDateStart));
	            $lessonEndTime = date('H:i:s', strtotime($lessonDateEnd));
	            $lessonDate = date('Y-m-d',strtotime($lessonDateStart));
	            
	            $eventInfoAry = $libenroll->GET_EVENTINFO($enrolEventID);                  // course info
	            $categoryID = count($eventInfoAry) ? $eventInfoAry['EventCategory'] : 0;
	            
	            $intakeID = $libenroll->getIntakeIDByEnrolEventID($enrolEventID);
	            $intakeAry = $libenroll->getIntake($intakeID, $getClassAndInstructor=false);
	            $intakeStartDate = $intakeAry[$intakeID]['StartDate'];
	            $intakeEndDate = $intakeAry[$intakeID]['EndDate'];
	            
	            $intakeClassAssoc = $libenroll->getIntakeClass($intakeID);
	            $intakeClassIDAry = array_keys($intakeClassAssoc);
	            $lessonClassAssoc = $libenroll->getLessonClass($eventDateID);
	            $intakeClassList = $this->getIntakeClassList($intakeClassAssoc, $lessonClassAssoc, 0, false, true);
	            
	            if (count($lessonClassAssoc) == 1) {
	                $classInfo = array_values($lessonClassAssoc);
	                $classLocationID = $classInfo[0]['LocationID'];
	            }
	        }
	        else {
	            // use dfault values
	        }
	    }
	    else {
	        // use dfault values
	    }
	    
	    $trainerAry = $libenroll->getLessonTrainer($eventDateID);
	    
	    $tableClass = 'no_bottom_right_border';
	    $prefixLocation = 'Location';
	    $bookingInfo = array('Date'=>$lessonDate, 'StartTime'=>$lessonStartTime, 'EndTime'=>$lessonEndTime);
	    
	    $courseSelection = $this->getCourseSelectionByClass($intakeClassIDAry, $enrolEventID, $intakeID);        // show all courses of the intake
	    
	    $para = array();
	    $para['CourseSelection'] = $courseSelection;
	    $para['SelectLessonTitle'] = $this->getLessonSelectionByCourse($enrolEventID, $lessonTitle);
	    $para['IntakeClassList'] = $intakeClassList;
	    $para['LessonDate'] = $this->getDateTimeSelection(0, 0, $lessonDate, $lessonStartTime, $lessonEndTime, 'date');
//	    $para['LessonDate'] = $lessonDate;
	    $para['LessonTime'] = $this->getDateTimeSelection(0, 0, $lessonDate, $lessonStartTime, $lessonEndTime, 'time', $setOnChange=true);
	    $para['Instructor'] = $this->getInstructorSelection($trainerAry, 0, 0, $tableClass, '', '40px', $categoryID, $parFieldID='', $parFieldName='', $parFieldClass='', $onClick='chooseInstructor()');
	    $para['Location'] = $this->getLocationSelection($prefixLocation, $selectedLocationID, 0, 0, $showCapacity=true, $capacity, $bookingInfo, $categoryID, $classLocationID);
	    $para['HidEventDateID'] = $eventDateID;
	    $para['HidIntakeStartDate'] = $intakeStartDate;
	    $para['HidIntakeEndDate'] = $intakeEndDate;
	    $para['HidCourseCategoryID'] = $categoryID;
	    $para['HidOriginalLocationID'] = $selectedLocationID;
	    $para['HidSaveButton'] = 'edit';
	    
	    $form = $this->getAddEditLessonForm($para);
	    $js = $this->getAddEditLessonJs('edit', $intakeStartDate, $intakeEndDate);
	    
	    return $form . $js;
	    
	}
	
	function getLessonSelectionByCourse($courseID='', $selectedLesson='')
	{
	    global $libenroll, $Lang;

	    $data = array();
	    $selected = '';
	    if ($courseID) {
	        $lessonList = $libenroll->getCourseLesson($courseID);
    	    foreach ((array)$lessonList as $_lesson) {
    	        $lesson = $_lesson['Lesson'];
    	        $data[] = array($lesson, $lesson);

    	        if ($selectedLesson == $lesson) {
    	            $selected = $lesson;   
    	        }
    	    }
	    }
	    
	    $name = 'LessonTitleSelect';
	    $errID = 'Err'.$name;
	    $x = getSelectByArray($data, "name='$name' id='$name' onFocus='setLessonBy()'", $selected, $all=0, $noFirst=0, $firstTitle='');
	    $x .= '<span class="error_msg_hide" id="'.$errID.'">'.$Lang['eEnrolment']['course']['warning']['selectLesson'].'</span>';
        return $x;
	}
	
}
?>