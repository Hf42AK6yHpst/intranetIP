<?php
// using : ronald
if (!defined("LIBWORDTEMPLATES_DEFINED"))         // Preprocessor directives
{

 define("LIBWORDTEMPLATES_DEFINED",true);
 
 include_once("libfilesystem.php");
 class libwordtemplates extends libfilesystem{
       var $file_array;
       var $word_array;
       var $path;

       function libwordtemplates($type=0)
       {
	       global $eReportCardWordTemplate;
	       if ($type == 0) {
                global $i_wordtemplates_attendance,$i_wordtemplates_merit,$i_wordtemplates_demerit,$i_wordtemplates_activity,$i_wordtemplates_performance;
                global $i_wordtemplates_service,$i_wordtemplates_service_role,$i_wordtemplates_activity_role;
                global $i_wordtemplates_award_name;
                $this->libfilesystem();
                $this->file_array = array("attend.txt","merit.txt","demerit.txt","activityname.txt",
                "activity_role.txt","service.txt","service_role.txt",
                "awardname.txt", "performance.txt");
                $this->word_array = array ($i_wordtemplates_attendance,$i_wordtemplates_merit,
                                                                        $i_wordtemplates_demerit,
                                    $i_wordtemplates_activity,$i_wordtemplates_activity_role,
                                    $i_wordtemplates_service,$i_wordtemplates_service_role
                                    ,$i_wordtemplates_award_name
                                    ,$i_wordtemplates_performance
                                    );
                $this->path = "file/templates";
            } else if ($type == 1) {
	            global $eReportCard;
                $this->libfilesystem();
                $this->file_array = array("ClassTeacherComment.txt","SubjectTeacherComment.txt");
                $this->word_array = array ($eReportCard['ClassPresetComment'],$eReportCard['SubjectPresetComment']);
                $this->path = "file/templates";
            }
       }
       
       function getWordList($type)
       {
                if ($type < 0 || $type > sizeof($this->file_array) ) return array();
                global $intranet_root;
                $content = trim($this->file_read("$intranet_root/".$this->path."/".$this->file_array[$type]));
                
                if ($content != "")
                {
                    $result = explode("\n",$content);
                    for ($i=0; $i<sizeof($result); $i++)
                    {
                         $result[$i] = trim($result[$i]);
                    } 
                    
                    return $result;
                }
                else return array();
       }
       
       # get websams attendance word list
       function getWebSamsAttendanceWordList($attendance_type=0){
		       global $special_feature;
		       if($special_feature["websams_attendance_export"]){
		       		include_once('libdb.php');
		       		$li = new libdb();
		       		$where = $attendance_type==0?"": " WHERE ReasonType='$attendance_type' ";
			       $sql ="SELECT DISTINCT TRIM(ReasonText) FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE $where ORDER BY CodeID ";
			       return $li->returnVector($sql);
			   }
			   return array();
	   }
	   
       function getWordListAttendance($attendance_type=0)
       {

		       global $special_feature;
		       
		       if($special_feature["websams_attendance_export"]==true){ # use WebSams Attendance word list
		       		include_once('libdb.php');
		       		$li = new libdb();
			       $sql = "SELECT COUNT(*) FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE";
			       $temp = $li->returnVector($sql);
			       if($temp[0]+0 > 0)
		       			return $this->getWebSamsAttendanceWordList($attendance_type);
		       }
               return $this->getWordList(0);
       }
       function getWordListMerit()
       {
                return $this->Getwordlist(1);
       }
           function getWordListDemerit()
       {
                return $this->Getwordlist(2);
       }
       function getWordListActivityName()
       {
                return $this->getWordList(3);
       }
       function getWordListActivityRole()
       {
                return $this->getWordList(4);
       }
       function getWordListServiceName()
       {
                return $this->getWordList(5);
       }
       function getWordListServiceRole()
       {
                return $this->getWordList(6);
       }
       function getWordListAwardName()
       {
                return $this->getWordList(7);
       }
       function getWordListPerformance()
       {
                return $this->getWordList(8);
       }
       function getSelect($list,$tags)
       {
                if (sizeof($list)==0) return "";
                global $button_select;
                $x = "<SELECT $tags>\n";
                $x .= "<OPTION value=''> -- $button_select -- </OPTION>\n";
                for ($i=0; $i<sizeof($list); $i++)
                {
                     $item = trim($list[$i]);
                     $item = intranet_htmlspecialchars($item);
                     $x .= "<OPTION value='$item'>$item</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;

       }

       function getSelectAttendance($tags)
       {
                $list = $this->getWordListAttendance();
                return $this->getSelect($list,$tags);
       }
       function getSelectMerit($tags, $mtype=0)
       {
                $list = $this->getWordListMerit();
                                // load demerit if necessary
                                if (sizeof($list)==0 || $mtype<0)
                                {
                                        $nlist = $this->getWordListDemerit();
                                        if (sizeof($nlist)>0){
                                                $list = ($mtype>=0) ? Array("") : $nlist;
                                        }
                                }
                                return $this->getSelect($list,$tags);
       }
       function getSelectActivityName($tags)
       {
                $list = $this->getWordListActivityName();
                return $this->getSelect($list,$tags);
       }
       function getSelectPerformance($tags)
       {
                $list = $this->getWordListPerformance();
                return $this->getSelect($list,$tags);
       }
       function getSelectServiceName($tags)
       {
                $list = $this->getWordListServiceName();
                return $this->getSelect($list,$tags);
       }
       function getSelectServiceRole($tags)
       {
                $list = $this->getWordListServiceRole();
                return $this->getSelect($list,$tags);
       }
       function getSelectActivityRole($tags)
       {
                $list = $this->getWordListActivityRole();
                return $this->getSelect($list,$tags);
       }
       function getSelectAwardName ($tags)
       {
                $list = $this->getWordListAwardName();
                return $this->getSelect($list,$tags);
       }

	   function getWordListHWComment()
       {
                return $this->getWordList(10);
       }
              
	   function getSelectHWComment($tags)
       {
                $list = $this->getWordListHWComment();
                return $this->getSelect($list,$tags);
       }	
       
	   function getSelectHWTitle($tags)
       {
                $list = $this->getWordListHWTitle();
                return $this->getSelect($list,$tags);
       }
	   function getWordListHWTitle()
       {
                return $this->getWordList(9);
       }
 }


} // End of directives
?>