<?php
// Modifing by :  Paul

## Change Log ##
/*
 * 
 * 
*/

class pagamo_install extends libdb{
	

	function pagamo_install(){		
		$this->libdb();		
	}
	
	#######################################
	## GET FUNCTIONS
	
	function retrieve_quotation_install_info(){
		global $sys_custom;
		$sql = "SELECT q.QuotationName, q.SchoolName, q.ClassLevel as YearName, q.StdQuota, q.TchQuota, q.Startdate, q.Enddate, q.Inputdate, q.QuotationID 
				FROM PAGAMO_QUOTATION  q";
		$result = $this->returnArray($sql);
		if(sizeof($result) <=0 ){
			$html .= '<tr>
                      <td style="width: 600px;" colspan="5">No quotation found in this moment.</td></tr>';
		}
		for ($i=0; $i<sizeof($result); $i++){
			$PeriodFrom =  (date($result[$i]["Startdate"]))== "0000-00-00 00:00:00" ? "--":date("Y-m-d",strtotime($result[$i]["Startdate"]));
			$PeriodTo =  (date($result[$i]["Enddate"]))== "0000-00-00 00:00:00" ? "--":date("Y-m-d",strtotime($result[$i]["Enddate"]));
			$sql = "SELECT RecordID FROM PAGAMO_USER WHERE QuotationID='".$result[$i]["QuotationID"]."'";
			$hasRecord = $this->returnVector($sql);
			$IsActivated = (!empty($hasRecord))?true:false;
							
			if($IsActivated){
				$quotation_color = "style='color: red'";
			}else{
				$quotation_color = "";
			}
			if($i%2){
				$background_color = "style='background-color:#dfe3ea'";
			}else $background_color = "";
			
			$classLevelDisplay = $result[$i]["YearName"];
			if($sys_custom['non_eclass_PaGamO']==true){
				$classLevelDisplay = $result[$i]["SchoolName"]." - ".$result[$i]["YearName"];
			}
			$html .= '<tr '.$background_color.'>
						<td '.$quotation_color.'>'.($i+1).'</td>
                      	<td '.$quotation_color.'>'.$result[$i]["QuotationName"].'</td>
                      	<td '.$quotation_color.'>'.$classLevelDisplay.'</td>
                      	<td>'.$result[$i]["StdQuota"].'</td>
                      	<td>'.$result[$i]["TchQuota"].'</td>
                      	<td>'.$PeriodFrom.'</td>
                      	<td>'.$PeriodTo.'</td>
                      	<td>'.date("Y-m-d h:i:s",strtotime($result[$i]["Inputdate"])).'</td>';
			if($IsActivated){
				$html .= '<td style="color: #ff0058;"></td>';
			}else{	
            	$html .= '<td style="color: #ff0058;"><a class="delete_btn" style="cursor: pointer;" value="'.$result[$i]["QuotationID"].'" >X</a></td>';
			}
            $html .= '</tr>';
		}
		return $html;
	}

	function install_new_quotation($quotation_number, $school_code, $school_name, $class_level, $std_quota, $tch_quota, $period_from, $period_to){
		$sql = "INSERT INTO PAGAMO_QUOTATION 
				(QuotationName, SchoolCode, SchoolName, ClassLevel, StdQuota, TchQuota, Startdate, Enddate, Inputdate) VALUES(
				'".$quotation_number."','".$school_code."','".$school_name."','".$class_level."','".$std_quota."','".$tch_quota."','".$period_from."','".$period_to."', NOW()
				)";
		return $this->db_db_query($sql);
	}
	
	function uninstall_new_quotation($QuotationID){
		$sql = "DELETE FROM PAGAMO_QUOTATION WHERE QuotationID='".$QuotationID."'";
		$this->db_db_query($sql);
	}
	
	function getClassLevelOptions(){
		global $PATH_WRT_ROOT, $Lang;
		$html = "";
		/*
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$fcm = new form_class_manage();
		$fcmArr = $fcm->Get_Current_Academic_Year_And_Year_Term();
		list($currentYearID,$currentYearNameEn,$currentYearNameB5,$Sequence, $currentTermID, $currentTermNameEn, $currentTermB5, $TermStart, $TermEnd) = $fcmArr[0];
		
		$sql = "SELECT y.YearName, y.YearID FROM YEAR y INNER JOIN 
				YEAR_CLASS yc ON y.YearID=yc.YearID AND yc.AcademicYearID='".$currentYearID."'
				GROUP BY y.YearID
				ORDER BY y.Sequence ASC, y.YearID ASC";
		$year_list = $this->returnArray($sql);
		foreach($year_list as $year){
			$html .= "<option value='".$year['YearID']."'>".$year['YearName']."</option>";
		}*/
		$pagamoYearList = array("P4","P5","P6","S1","S2","S3");
		foreach($pagamoYearList as $year){
			$html .= "<option value='".$year."'>".$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList'][$year]."";
		}
		return $html;
	}
	
	function get_quotation_exists_by_quotation_number($quotation_number){
		$sql = "SELECT QuotationID FROM PAGAMO_QUOTATION WHERE QuotationName='".$quotation_number."'";
		$result = current($this->returnVector($sql));
		return (!empty($result));
	}
	
	function getLicenseArray($quotation_id, $UserType){
		$sql = "SELECT COUNT(RecordID) FROM PAGAMO_USER WHERE QuotationID='".$quotation_id."' AND UserType='".$UserType."'";
		$used = current($this->returnVector($sql));
		if($UserType=='S'){
			$sql = "SELECT StdQuota FROM PAGAMO_QUOTATION WHERE QuotationID='".$quotation_id."'";
			$total_quota = current($this->returnVector($sql));			
			$left = $total_quota - $used;
			return array($total_quota, $left, $used);
		}else if($UserType=='T'){
			$sql = "SELECT TchQuota FROM PAGAMO_QUOTATION WHERE QuotationID='".$quotation_id."'";
			$total_quota = current($this->returnVector($sql));
			$left = $total_quota - $used;
			return array($total_quota, $left, $used);
		}
		return array(false,false,false);
	}
	
	function hasUsedUpAllLicense($quotation_id, $UserType){
		$license = $this->getLicenseArray($quotation_id, $UserType);
		if($license[1]==0)
			return true;
		else
			return false;
	}
	
	function isInLicencePeriod($quotation_id){
		$sql = "SELECT Enddate FROM PAGAMO_QUOTATION WHERE QuotationID='".$quotation_id."'";
		$Expiry = current($this->returnVector($sql));
		if(strtotime($Expiry) > time()){
			return true;
		}
		return false;
	}	
}

?>