<?php
// Modifing by :  Paul

## Change Log ##
/*
 * 
 * 
*/

class pagamo extends libdb{	
	var $pagamo_launchUrl;
	var $pagamo_launchKey;
	var $pagamo_launchSecret;
	
	function pagamo(){
		$this->libdb();		
	}
	
	function setUpKeySecretUrl($schoolCode){
		global $UserID, $config_school_code, $sys_custom;
		$this->libdb();
		if($sys_custom['non_eclass_PaGamO'] && $schoolCode==""){
			$sql = "Select DISTINCT SchoolCode From PAGAMO_USER pu
					INNER JOIN PAGAMO_QUOTATION pq ON pu.QuotationID=pq.QuotationID
					WHERE pu.UserID='".$UserID."'";
			$schoolCode = current($this->returnVector($sql));
		}
		$sql = "SELECT LaunchURL, LaunchKey, LaunchSecret FROM LTI_LOGIN_INFO WHERE SchoolCode='".$schoolCode."' AND AppName='PaGamO'";
		list($this->pagamo_launchUrl, $this->pagamo_launchKey, $this->pagamo_launchSecret) = current($this->returnArray($sql));
		return $schoolCode;
	}
	
	function hasPagamoAccess(){
		global $UserID;
		if($this->isGeneralAdmin())return true;
		$sql = "SELECT RecordID FROM PAGAMO_USER pu INNER JOIN
				PAGAMO_QUOTATION pq ON pu.QuotationID=pq.QuotationID
				WHERE pu.UserID='".$UserID."' AND pq.Startdate < NOW() AND pq.Enddate > NOW()";
		$hasRecord = $this->returnVector($sql);
		if(empty($hasRecord)){
			return false;
		}
		return true;
	}
	
	function getPagamoClass($schoolCode){
		global $UserID, $UserType, $isTeaching, $PATH_WRT_ROOT, $CurrentSchoolYearInfo, $intranet_session_language, $sys_custom;
		$classData = array();
		if($this->isGeneralAdmin()){
			if($sys_custom['PaGamO']['ReleaseClassLimit']==true){
				$sql = "SELECT DISTINCT y.WEBSAMSCode as ClassLevel, y.YearID FROM PAGAMO_QUOTATION pq 
						INNER JOIN PAGAMO_USER pu ON pq.QuotationID = pu.QuotationID
						INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=pu.UserID
						INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
						INNER JOIN YEAR y ON yc.YearID=y.YearID
						WHERE pq.SchoolCode='".$schoolCode."' AND pq.Startdate < NOW() AND pq.Enddate > NOW() AND yc.AcademicYearID='".IntegerSafe($_SESSION['CurrentSchoolYearID'])."'";
			}else{
				$sql = "Select q.ClassLevel, q.YearID From PAGAMO_QUOTATION q WHERE SchoolCode='".$schoolCode."' AND q.Startdate < NOW() AND q.Enddate > NOW()";
			}
			$classLevels = $this->returnArray($sql);
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$scm = new subject_class_mapping();
			$classList = array();
			$noOfClasses = 0;
			foreach($classLevels as $cl){
				$nameSql = ($intranet_session_language=="b5")?"ClassTitleB5":"ClassTitleEN";
				$sql = "SELECT $nameSql as ClassName, YearClassID as ClassID FROM YEAR_CLASS WHERE YearID='".$cl['YearID']."' AND AcademicYearID='".IntegerSafe($_SESSION['CurrentSchoolYearID'])."'";
				$tchClasses = $this->returnArray($sql);
				foreach($tchClasses as $tc){
					$classList[$cl['ClassLevel']][] = array("ClassName"=>$tc['ClassName'], "ClassID"=>$tc['ClassID']);
					$noOfClasses++;
				}
			}
			$classData['classLevelNo'] = count($classLevels);
			$classData['classNo'] = $noOfClasses;
			$classData['classList'] = $classList;
		}else if($UserType==2){
			$nameSql = ($intranet_session_language=="b5")?"yc.ClassTitleB5":"yc.ClassTitleEN";
			$sql = "SELECT y.WEBSAMSCode as ClassLevel, $nameSql, yc.YearClassID FROM PAGAMO_QUOTATION pq 
					INNER JOIN PAGAMO_USER pu ON pq.QuotationID = pu.QuotationID
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=pu.UserID
					INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
					INNER JOIN YEAR y ON yc.YearID=y.YearID
					WHERE pu.UserID='".$UserID."' AND pq.Startdate < NOW() AND pq.Enddate > NOW() AND yc.AcademicYearID='".IntegerSafe($_SESSION['CurrentSchoolYearID'])."'";
			list($classLevel, $className, $classID) = current($this->returnArray($sql));
			$classList[$classLevel][] = array("ClassName"=>$className, "ClassID"=>$classID);
			$classData['classLevelNo'] = 1;
			$classData['classNo'] = 1;
			$classData['classList'] = $classList;
		}else if($UserType==1){
			$sql = "Select DISTINCT y.WEBSAMSCode as ClassLevel, y.YearID From PAGAMO_USER u
					INNER JOIN PAGAMO_QUOTATION q ON u.QuotationID=q.QuotationID 
					INNER JOIN PAGAMO_USER pu ON pu.QuotationID=q.QuotationID 
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=pu.UserID
					INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
					INNER JOIN YEAR y ON yc.YearID=y.YearID
					WHERE u.UserID='".$UserID."' AND pu.UserType='S' AND q.Startdate < NOW() AND q.Enddate > NOW() AND yc.AcademicYearID='".IntegerSafe($_SESSION['CurrentSchoolYearID'])."'";
			$classLevels = $this->returnArray($sql);
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$scm = new subject_class_mapping();
			$classList = array();
			$noOfClasses = 0;
			foreach($classLevels as $cl){
				$tchClasses = $scm->returnSubjectTeacherClass($UserID, $cl['YearID']);
				$nameSql = ($intranet_session_language=="b5")?"yc.ClassTitleB5":"yc.ClassTitleEN";
				$sql = "SELECT yc.YearClassID as ClassID, $nameSql as ClassName FROM YEAR_CLASS_TEACHER yct
						INNER JOIN YEAR_CLASS yc ON yct.YearClassID=yc.YearClassID
						INNER JOIN YEAR y ON yc.YearID=y.YearID
						WHERE yct.UserID=$UserID  AND yc.AcademicYearID='".IntegerSafe($_SESSION['CurrentSchoolYearID'])."'";
				$classTeacherClasses = $this->returnArray($sql);
				if(empty($tchClasses)){
					$tchClasses = $classTeacherClasses;
				}else{
					foreach($classTeacherClasses as $idx=>$ct){
						foreach($tchClasses as $tc){
							if($tc['ClassID']==$ct['ClassID']){
								unset($classTeacherClasses[$idx]);
								break;
							}
						}
					}
					if(!empty($classTeacherClasses)){
						foreach($classTeacherClasses as $idx=>$ct){
							array_push($tchClasses, $ct);
						}
					}
				}
				foreach($tchClasses as $tc){
					$classList[$cl['ClassLevel']][] = array("ClassName"=>$tc['ClassName'], "ClassID"=>$tc['ClassID']);
					$noOfClasses++;
				}
			}
			$classData['classLevelNo'] = count($classLevels);
			$classData['classNo'] = $noOfClasses;
			$classData['classList'] = $classList;
		}
		return $classData;
	}
	
	function generateLaunchData($schoolCode){
		global $UserID, $UserType, $config_school_code, $intranet_session_language, $sys_custom;
		$protocol = (strstr($_SERVER['HTTP_REFERER'],"https"))?"https":"http";
		if($sys_custom['non_eclass_PaGamO']){
			if($this->isGeneralAdmin()){
				$config_school_code = $schoolCode;
			}else{
				$sql = "SELECT pq.SchoolCode FROM PAGAMO_USER pu INNER JOIN
						PAGAMO_QUOTATION pq ON pu.QuotationID=pq.QuotationID
						WHERE pu.UserID='".$UserID."' AND pq.Startdate < NOW() AND pq.Enddate > NOW() LIMIT 1";
				$config_school_code = current($this->returnVector($sql));
			}
		}
		$sql = "SELECT 
					EnglishName, 
					ChineseName,
					LastName,
					FirstName,
					(CASE
						WHEN RecordType=1 THEN 'Instructor'
						WHEN RecordType=2 THEN 'Learner'
						ELSE '--'
					END) as Role,
					UserEmail					
				FROM INTRANET_USER
				WHERE UserID='".$UserID."'";
		$userInfo = current($this->returnArray($sql));
		$sql = "SELECT pq.SchoolName FROM PAGAMO_USER pu INNER JOIN
					PAGAMO_QUOTATION pq ON pu.QuotationID=pq.QuotationID
					WHERE pu.UserID='".$UserID."' AND pq.Startdate < NOW() AND pq.Enddate > NOW() LIMIT 1";
		$schoolName = current($this->returnVector($sql));;
		
		$launch_data = array(
				"user_id" => $config_school_code."-".$UserID,
				"roles" => $userInfo['Role'],
				"ext_lms" => "eClass",
				"ext_submit" => "Press to launch this activity",
				"lti_version" => "LTI-1p0",
				"basic-lti-launch-request" => "basic-lti-launch-request",
				"launch_presentation_locale" => $intranet_session_language,
				"launch_presentation_return_url" => $protocol."://".$_SERVER['HTTP_HOST']."/home/eLearning/LTI/Pagamo/get_return_message.php",
				"resource_link_id" => "eClass_".$config_school_code,
				"resource_link_title" => $schoolName,
				"resource_link_description" => $schoolName,
				"lis_person_name_full" => $userInfo['EnglishName'],
				"lis_person_name_family" => $userInfo['LastName'],
				"lis_person_name_given" => $userInfo['FirstName'],
				"lis_person_contact_email_primary" => $userInfo['UserEmail'],
				"lis_person_sourcedid" => $userInfo['Role'],
				"context_id" => "eClass_".$config_school_code,
				"context_title" => "Pagamo",
				"launch_presentation_locale" => $intranet_session_language,
				"custom_eclass_school_code" => $config_school_code,
				"custom_eclass_user_chinese_name" => $userInfo['ChineseName']
		);
		$classData = $this->getPagamoClass($schoolCode);
		# CUSTOM DATA CONFIGURATION
		$launch_data["custom_no_of_class"] = $classData['classNo'];
		$launch_data["custom_no_of_class_level"] = $classData['classLevelNo'];
		$idx=0;
		$loopIdx=0;
		foreach($classData['classList'] as $classLevel=>$classes){
			$launch_data['custom_eclass_class_level_'.$idx]=$classLevel;
			$idx++;
			foreach($classes as $class){
				$launch_data['custom_eclass_class_'.$loopIdx]=$class['ClassName'];
				$launch_data['custom_eclass_class_id_'.$loopIdx]=$class['ClassID'];
				$loopIdx++;
			}
		}
		
		$key = $this->pagamo_launchKey;
		$secret = $this->pagamo_launchSecret;
		# Basic LTI uses OAuth to sign requests
		# OAuth Core 1.0 spec: http://oauth.net/core/1.0/
		$launch_data["oauth_callback"] = "about:blank";
		$launch_data["oauth_consumer_key"] = $key;
		$launch_data["oauth_version"] = "1.0";
		$launch_data["oauth_nonce"] = str_replace('.','',uniqid('', true));
		$launch_data["oauth_timestamp"] = time();
		$launch_data["oauth_signature_method"] = "HMAC-SHA1";
		
		return $launch_data;
	}
		
	function generateSignature($launch_data){
		
		# In OAuth, request parameters must be sorted by name
		$launch_data_keys = array_keys($launch_data);
		sort($launch_data_keys);
		$launch_params = array();
		foreach ($launch_data_keys as $key) {
			array_push($launch_params, $key . "=" . rawurlencode($launch_data[$key]));
		}
		$base_string = "POST&" . urlencode($this->pagamo_launchUrl) . "&" . rawurlencode(implode("&", $launch_params));
		$secret = urlencode($this->pagamo_launchSecret) . "&";
		$signature = base64_encode(hash_hmac("sha1", $base_string, $secret, true));
		
		return $signature;
	}
	
	function testConnection($launch_data, $signature, $class_list){
		$launch_url = $this->getLaunchUrl();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $launch_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		$postvar = "";
		foreach ($launch_data as $k => $v ) {
			$postvar.= $k."=".$v."&";
		}
		$postvar .= "oauth_signature=".$signature;		
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postvar);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpCode=='403'){
			return false;
		}else{
			return true;
		}
	}
	
	function addToLog($code, $msg, $UserID){
		if(!isset($UserID)){
			global $UserID;
		}
		$sql = "INSERT INTO PAGAMO_LOG (UserID, Code, Action, DateInput) VALUES ('".$UserID."','".$code."','".$msg."',NOW())";
		$result = $this->db_db_query($sql);
	}
	
	function updateStatus($action, $UserID){
		if(!isset($UserID)){
			global $UserID;
		}
		switch($action){
			case 'AccCreation':
				$sql = "UPDATE PAGAMO_USER SET IsActivated=1,LastLogin=NOW() WHERE UserID='".$UserID."'";
				break;
			case 'LogIn':
				$sql = "UPDATE PAGAMO_USER SET LastLogin=NOW() WHERE UserID='".$UserID."'";
				break;
		}
		$this->db_db_query($sql);
	}	

	function getLaunchUrl(){
		return $this->pagamo_launchUrl;
	}
	
	function isGeneralAdmin(){
		global $UserID;
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='broadlearning'";
		$thisUserID = current($this->returnVector($sql));
		return ($UserID==$thisUserID);
	}
	
	function getSchoolList(){
		$sql = "SELECT DISTINCT SchoolCode, SchoolName FROM PAGAMO_QUOTATION ORDER BY QuotationID DESC";
		$schools = $this->returnArray($sql);
		$schoolData = array();
		foreach($schools as $sch){
			$schoolData[$sch['SchoolCode']]['SchoolName'] = $sch['SchoolName'];
		}
		return $schoolData;
	}
	
	function hasQuotation(){
		global $UserID;
		if($this->isGeneralAdmin()){
			return true;
		}else{
			$sql = "SELECT pq.QuotationID FROM PAGAMO_USER pu
					INNER JOIN PAGAMO_QUOTATION pq ON pu.QuotationID=pq.QuotationID
					WHERE pu.UserID='".$UserID."' AND pq.Startdate < NOW() AND pq.Enddate > NOW()";
			$result = $this->returnVector($sql);
			return (!empty($result));
		}
	}
}