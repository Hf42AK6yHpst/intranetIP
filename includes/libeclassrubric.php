<?php
/*
 * Modifing by Siuwan 
 * 
 * 2015-06-29 (Siuwan) [ip.2.5.6.7.1]
 *  - modified function getStandardRubricSet() to add orderby field 
 * 
 */


include_once("$intranet_root/includes/libeclass40.php");

class libeclassrubric extends libdb {
	
	public $objClassRoom = null;

	public function libeclassrubric($cid)
	{
		$this->libdb();

		//F: classNamingDB() exist in libeclass40.php
		$this->db = classNamingDB($cid);
	}


	/**
	* RETURN STANDARD RUBRIC SET 
	* @owner : Fai (20101013)
	* @param : INTEGER / ARRAY integer or array of integer of a Standard Rubric Set ID
	* @return : Information of the request standard rubric set ID
	* 
	*/
	public function getStandardRubricSet($standardRubricSetID = NULL,$orderByField="")
	{
		$idQueryString = "";

		$idQueryString = libies_static::concatSQLIntValue($standardRubricSetID);

		$sqlQuery = (trim($idQueryString) == "")? "":" where std_rubric_set_id in ({$idQueryString})";
		$sql = "SELECT ";
    $sql .= "std_rubric_set_id as 'STD_RUBRIC_SET_ID', ";
    $sql .= "set_title as 'SET_TITLE', ";
    $sql .= "description as 'DESCRIPTION', ";
    $sql .= "set_type as 'SET_TYPE', ";
    $sql .= "createdby  as 'CREATEDBY', ";
    $sql .= "inputdate as 'INPUTDATE', ";
    $sql .= "modified  as 'MODIFIED' ";
    $sql .= "FROM standard_rubric_set ";
    $sql .= $idQueryString;
	
	if(!empty($orderByField)){
		$sql .= " ORDER BY ".$orderByField;
	}	
		$result = $this->returnArray($sql);
		return $result;
	}


	/**
	* RETURN STANDARD RUBRIC 
	* @owner : Eric (20101015)
	* @param : INTEGER / ARRAY integer or array of integer of a Standard Rubric Set ID	
	* @param : INTEGER / ARRAY integer or array of integer of a Standard Rubric ID
	* @return : Information of the request standard rubric ID
	* 
	*/
	public function getStandardRubric($standardRubricSetID = NULL, $standardRubricID = NULL)
	{
		$idQueryString = "";
		if($standardRubricSetID != NULL){	
			//$standRubricSetID with value / not empty
			if(is_array($standardRubricSetID)){
				$idQueryString = implode(',',$standardRubricSetID);
			}
			else
			{
        $idQueryString = $standardRubricSetID;
      }
		}
		$sqlQuery = (trim($idQueryString) == "")? "":" AND std_rubric_set_id in ({$idQueryString})";
		
		$idQueryString = "";
		if($standardRubricID != NULL){	
			//$standRubricSetID with value / not empty
			if(is_array($standardRubricID)){
				$idQueryString = implode(',',$standardRubricID);
			}
			else
			{
        $idQueryString = $standardRubricID;
      }
		}
		$sqlQuery .= (trim($idQueryString) == "")? "":" AND std_rubric_id in ({$idQueryString})";
		
		$sql = "SELECT "; 
    $sql .= "std_rubric_id as 'STD_RUBRIC_ID', "; 
    $sql .= "title as 'TITLE', "; 
    $sql .= "showorder as 'SHOWORDER', ";
    $sql .= "createdby  as 'CREATEDBY', ";
    $sql .= "inputdate as 'INPUTDATE', ";
    $sql .= "modified  as 'MODIFIED' ";
    $sql .= "FROM standard_rubric ";
    $sql .= "WHERE 1";
    $sql .= $sqlQuery;
		
		$result = $this->returnArray($sql);
		return $result;
	}
	
	/**
	* COPY RUBRICS FROM STANDARD TO TASK 
	* @owner : Eric (20101015)
	* @param : INTEGER integer of a phase ID   (stage)
	* @param : INTEGER integer of a task ID    (scheme)
	* @param : INTEGER integer of a standard rubric ID	
	* @return : ID of task rubric just copied
	* 
	*/
	public function stdRubric2taskRubric($phaseID, $taskID, $stdRubricID)
	{
    $sql = "INSERT INTO task_rubric ";
    $sql .= "(task_id, phase_id, assessment_id, title, showorder, createdby, inputdate) ";
    $sql .= "SELECT {$taskID}, {$phaseID}, 1, title, showorder, createdby, now() ";
    $sql .= "FROM standard_rubric ";
    $sql .= "WHERE std_rubric_id = {$stdRubricID}";
    $this->db_db_query($sql);
    
    $task_rubric_id = $this->db_insert_id();
    
    $sql = "INSERT INTO task_rubric_detail ";
    $sql .= "(rubric_id, task_id, phase_id, assessment_id, score, score_from, score_to, description, createdby, inputdate) ";
    $sql .= "SELECT {$task_rubric_id}, {$taskID}, {$phaseID}, 1, score, score_from, score_to, description, createdby, now() ";
    $sql .= "FROM standard_rubric_detail ";
    $sql .= "WHERE std_rubric_id = {$stdRubricID}";
    $this->db_db_query($sql);
    
    return $task_rubric_id;
  }
	
	public function getTaskRubricByTaskID($taskID)
	{
		return $this->getTaskRubric(NULL,$taskID);
	}
	public function getTaskRubricByRubricID($rubricID)
	{
		return $this->getTaskRubric($rubricID,NULL);
	}
	
	private function getTaskRubric($rubricID = NULL,$taskID = NULL)
	{
//		$sql = "select rubric_detail_id as 'rubric_detail_id', rubric_id as 'rubric_id',  task_id as 'task_id' from task_rubric where task_id = 1";
		$conds = empty($rubricID) ? "" : " AND rubric_id = {$rubricID}";
		$conds .= empty($taskID) ? "" : " AND task_id = {$taskID}";

		$sql = "SELECT * FROM task_rubric WHERE 1 {$conds}";
		$result = $this->returnArray($sql);
		//debug_r($sql);
		//DEBUG(mysql_error());
		return $result;
	}
	
	public function getRubricDetail($rubricID)
	{
    $sql = "SELECT * FROM task_rubric_detail WHERE rubric_id = {$rubricID}";
    $result = $this->returnArray($sql);
    
    return $result;
  }
  
  public function getRubricMaxScore($rubricID)
  {
		$sql = "SELECT MAX(score_to) FROM task_rubric_detail WHERE rubric_id = {$rubricID}";
		$maxScore = current($this->returnVector($sql));
    
    return $maxScore;
	}
	
  public function getRubricMinScore($rubricID)
  {
		$sql = "SELECT MIN(score_from) FROM task_rubric_detail WHERE rubric_id = {$rubricID}";
		$minScore = current($this->returnVector($sql));
    
    return $minScore;
	}

	public function InsertTaskRubric($data)
	{

	}

}

?>
