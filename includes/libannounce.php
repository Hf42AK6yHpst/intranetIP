<?php
# using: 
  
######## Change Log [Start] #########
#
#   Date:   2020-03-05 Ray
#			added website code
#
#   Date:   2019-12-04 Bill [2019-1128-1046-36235]
#           Modified returnGroupsAnnouncement_Web_Service(), to return EndDate and onTop fields
#
#   Date:   2019-10-14 Tommy
#           Modified returnGroupsAnnouncement_Web_Service() -> add "a.onTop DESC" to orderBy
#
#   Date:   2019-08-27 Tommy
#           Modified returnALLSchoolAnnouncement(), returnSchoolAnnouncement(), returnGroupsAnnouncement(), returnGroupAnnouncement(), returnAllGroupsAnnouncement()
#           - add "a.onTop DESC" to orderBy
#
#   Date:   2019-01-09 Anna
#           Modified returnAnnouncement(), added TitleEng and DescriptionEng
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-09-12 (Anna) [Case#A146339]
#           modified returnSchoolAnnouncement(),returnAllSchoolAnnouncement()- change order by AnnouncementID DESC 
#
#   Date:   2018-08-08 (Bill) [2018-0807-1648-12073]
#           modified libannounce(), returnAnnouncement(), getApprovalStatus(), fixed sql injection
#
#	Date:	2017-12-22 (Cameron)
#			Hide $i_AnnouncementTargetGroup for Oaks in display2()
#
#	Date:	2017-11-28 (Anna)[P131539]
#			modified displayAnnouncementList() - added endDate display
#
#   Date:   2017-09-08 (Anna) [P123107]
#           modified displayAnnouncementList() - change AnnouncementTitle a href class to tablelink
#
#	Date:	2016-05-13 (Bill)
#			modified returnGroupsAnnouncement_Web_Service(), support Setting - Allow users to view past news	[2016-0513-1440-15206]
#
#	Date:	2016-03-04 (Siuwan) [ip.2.5.7.3.1] 
#			modified function displayAttachmentEdit() and display2(), use get_file_basename() instead of basename() for Chinese file name
#
#	Date:	2016-01-12 (Cameron)
#			Hide $i_AnnouncementTargetGroup for Amway in display2() [Case#P91044]
#
#	Date:	2016-01-07 (Bill) [2016-0107-1147-21066] [ip.2.5.7.1.1]
#			modified returnSchoolAnnouncement(), returnAllSchoolAnnouncement(), returnGroupAnnouncement(), returnGroupsAnnouncement(), returnAllGroupsAnnouncement()
#			- Announcement sorting - Start Date Descending > Issue Order Ascending
#
#	Date:	2015-12-11 (Paul) [ip.2.5.7.1.1]
#			modified display2() for new powerspeech expiration policy
# 
#	Date:	2015-11-05 (Omas) [E86087] [ip.2.5.7.1.1]
#			modified start date -> issue date using new ip25 lang
# 
#	Date:	2015-10-27 (Ivan) [P87700] [ip.2.5.7.1.1]
#			modified returnAllSchoolAnnouncement(), returnAllGroupsAnnouncement(), displayAnnouncementList() to add "allowUserToViewPastNews" logic
#
#	Date:	2015-02-10 (Bill)
#			modified display2(), displayAttachmentEdit() - prevent displaying Listen Now button if without Power Voice plugin 
#
#	Date:	2013-01-30 (YatWoon)
#			update returnTargetGroups(), check with lang to display eng/chinese group title	[Case#2013-0125-0934-10073]
#
#	Date:	2012-07-06 (YatWoon)
#			update display2(), displayAttachmentEdit(), download_attachment.php with encrypt logic
#
#	Date:	2012-05-15 (YatWoon)
#			modify display2(), no need to call intranet_handle_url() for using download_attachment.php (missing image part)
#
#	Date :	2012-04-17 (YatWoon)
#			update displayAttachmentEdit(), no need to call intranet_handle_url() for using download_attachment.php
#
#	Date :	2011-11-10 (YatWoon)
#			modify display2(), changed to use download_attachment.php to download the attachment. (missing image part)
#
#	Date : 	2011-10-20 (Jason)
#			modify displayAttachmentEdit() to fix the problem of failure of playing mp3 file in attachment field
#
#	Date:	2011-10-03 (Henry Chow)
#			modified returnAllSchoolAnnouncement(), add parameter $StartDate, $EndDate in order to retrieve announcement within specific date range
#
#	Date:	2010-08-13 (Ronald)
#			displayAttachmentEdit(), changed to use download_attachment.php to download the attachment.
#
#	Date:	2010-07-28	YatWoon
#			udpate display2(), due to the news desciption is using html-editor so that no need to use nl2br to cater line break
#			limitation: old content whish is using textarea will be affected.
#
#	Date:	2010-06-09	YatWoon
#			display description don't use function convertAllLinks2, just use function intranet_undo_htmlspecialchars, otherwise cannot display url with image correctly
#
#	Date:	2010-04-15 YatWoon
#			add function returnGroupAnnouncement_unread()
#			return the unread announcement number (within group), used for eCommunity index page
#
######## Change Log [End] #########

class libannounce extends libdb{

        var $Announcement;
        var $AnnouncementID;
        var $Title;
        var $Description;
        var $AnnouncementDate;
        var $RecordType;
        var $RecordStatus;
        var $DateInput;
        var $DateModified;
        var $EndDate;
        var $Attachment;
        var $OwnerGroupID;
        var $OwnerUserID;
        var $Internal;
        var $PublicStatus;        
        var $onTop;        
        var $VoiceFile;        
        var $WebsiteCode;
        

        ######################################################################################

        function libannounce($AnnouncementID=""){
                $this->libdb();
              
                $AnnouncementID = IntegerSafe($AnnouncementID);
                if($AnnouncementID <> "") {
                    global $sys_custom,$plugin;
              
                        $this->Announcement = $this->returnAnnouncement($AnnouncementID);
                        $this->AnnouncementID = $this->Announcement[0][0];
                        $this->Title = $this->Announcement[0][1];
                        $this->Description = $this->Announcement[0][2];
                        $this->AnnouncementDate = $this->Announcement[0][3];
                        $this->RecordType = $this->Announcement[0][4];
                        $this->RecordStatus = $this->Announcement[0][5];
                        $this->DateInput = $this->Announcement[0][6];
                        $this->DateModified = $this->Announcement[0][7];
                        $this->EndDate = $this->Announcement[0][8];
                        $this->Attachment = $this->Announcement[0][9];
                        $this->OwnerGroupID = $this->Announcement[0][10];
                        $this->OwnerUserID = $this->Announcement[0][11];
                        $this->Internal = $this->Announcement[0][12];
                        $this->PublicStatus = $this->Announcement[0][13];
                        $this->onTop = $this->Announcement[0][14];
                        if ($plugin['power_voice'])
                        {
                            $this->VoiceFile = $this->Announcement[0][15];
                        }                       
                        if( $sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
                            $this->TitleEng = $this->Announcement[0]['TitleEng'];
                            $this->DescriptionEng = $this->Announcement[0]['DescriptionEng'];
                        }
                        if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
							$this->WebsiteCode = $this->Announcement[0]['WebsiteCode'];
						}
                }
        }
        function returnReadCount()
        {
                 $sql = "SELECT TRIM(ReadFlag) FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
                 $temp = $this->returnVector($sql);
                 $flag = $temp[0];
                 if ($flag == "")
                 {
                     return 0;
                 }
                 else
                 {
	                 $flag = substr($flag,1,strlen($flag)-2);
                     $ids = explode(";;",$flag);
                     $ids = array_unique($ids);
                     $list = implode(",",$ids);
                     
					# check the readers are in the groups or not
                    $sql = "SELECT GroupID FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
					$temp = $this->returnVector($sql);
					
					if(!empty($temp))
					{
						global $PATH_WRT_ROOT;
						if($PATH_WRT_ROOT=="")	$PATH_WRT_ROOT = "../../";
						include_once($PATH_WRT_ROOT."includes/libgrouping.php");
						$g = new libgrouping();
						
						$group_users = "";
						foreach($temp as $key=>$groupid)
						{
							$t = $g->returnGroupUsers($groupid);
							foreach($t as $k=>$u)
								$group_users .= ";" . $u['UserID'] .";";
						}
						
						$final_list = "";
	                     $list_ary = explode(",",$list);
	                     foreach($list_ary as $key=>$data)
	                     {
							$check_userid = ";".$data.";";
							if(strpos($group_users, $check_userid) !== false)
								$final_list .= $data .",";
	                     }
	                     
	                     $list = substr($final_list,0, strlen($final_list)-1);
                     }
                     
                     $sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordStatus = 1 AND UserID IN ($list)";
                     $temp = $this->returnVector($sql);
                     return $temp[0]+0;
                 }
        }
        function returnTargetCount()
        {
                 $sql = "SELECT GroupID FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
                 $temp = $this->returnVector($sql);
                 if (sizeof($temp)==0)       # School Announcement
                 {
                     $sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordStatus = 1";
                     $temp2 = $this->returnVector($sql);
                     return $temp2[0];
                 }
                 else       # Group Announcement
                 {
                     $list = implode(",",$temp);
                     $sql = "SELECT COUNT(a.UserID) FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                             WHERE a.UserID = b.UserID AND a.RecordStatus = 1 AND b.GroupID IN ($list)";
                     $temp2 = $this->returnVector($sql);
                     return $temp2[0];
                 }
        }
        function returnReadList()
        {
                 $sql = "SELECT TRIM(ReadFlag) FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
                 $temp = $this->returnVector($sql);
                 $flag = $temp[0];
                 if ($flag == "")
                 {
                     return array();
                 }
                 else
                 {
                     $flag = substr($flag,1,strlen($flag)-2);
                     $ids = explode(";;",$flag);
                     $ids = array_unique($ids);
                     $list = implode(",",$ids);
                     
                     # check the readers are in the groups or not
					$sql = "SELECT GroupID FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
					$temp = $this->returnVector($sql);
					
					if(!empty($temp))
					{
						global $PATH_WRT_ROOT;
						if($PATH_WRT_ROOT=="")	$PATH_WRT_ROOT = "../../";
						include_once($PATH_WRT_ROOT."includes/libgrouping.php");
						$g = new libgrouping();
						
						$group_users = "";
						foreach($temp as $key=>$groupid)
						{
							$t = $g->returnGroupUsers($groupid);
							foreach($t as $k=>$u)
								$group_users .= ";" . $u['UserID'] .";";
						}
						
						$final_list = "";
	                     $list_ary = explode(",",$list);
	                     foreach($list_ary as $key=>$data)
	                     {
							$check_userid = ";".$data.";";
							if(strpos($group_users, $check_userid) !== false)
								$final_list .= $data .",";
	                     }
	                      $list = substr($final_list,0, strlen($final_list)-1);
                      }
                     
                     $name_field = getNameFieldWithClassNumberByLang();
                     $sql = "SELECT UserID, $name_field, RecordType FROM INTRANET_USER WHERE RecordStatus = 1 AND UserID IN ($list) ORDER BY ClassName,ClassNumber,EnglishName";
                     $temp = $this->returnArray($sql,3);
                     return $temp;
                 }
        }
        function returnUnreadList()
        {
                 $sql = "SELECT TRIM(ReadFlag) FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
                 $temp = $this->returnVector($sql);
                 $flag = $temp[0];

                 $sql = "SELECT GroupID FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
                 $temp = $this->returnVector($sql);
                 if (sizeof($temp)==0)       # School Announcement
                 {
                     $conds = "";
                     if ($flag != "")
                     {
                         $flag = substr($flag,1,strlen($flag)-2);
                         $ids = explode(";;",$flag);
                         $ids = array_unique($ids);
                         $list = implode(",",$ids);
                         $conds = "AND UserID NOT IN ($list)";
                     }
                     $name_field = getNameFieldWithClassNumberByLang();
                     $sql = "SELECT UserID, $name_field, RecordType FROM INTRANET_USER WHERE RecordStatus = 1 $conds ORDER BY ClassName,ClassNumber,EnglishName";
                     $temp = $this->returnArray($sql,3);
                     return $temp;
                 }
                 else       # Group Announcement
                 {
                     $grouplist = implode(",",$temp);
                     $sql = "SELECT a.UserID FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                             WHERE a.UserID = b.UserID AND a.RecordStatus = 1 AND b.GroupID IN ($grouplist)";
                     $temp2 = $this->returnVector($sql);
                     $userlist = implode(",",$temp2);

                     $conds = "";
                     if ($flag != "")
                     {
                         $flag = substr($flag,1,strlen($flag)-2);
                         $ids = explode(";;",$flag);
                         $ids = array_unique($ids);
                         $list = implode(",",$ids);
                         $conds = "AND UserID NOT IN ($list)";
                     }
                     $name_field = getNameFieldWithClassNumberByLang();
                     $sql = "SELECT UserID, $name_field, RecordType FROM INTRANET_USER WHERE RecordStatus = 1 AND UserID IN ($userlist) $conds ORDER BY ClassName,ClassNumber,EnglishName";
                     $temp = $this->returnArray($sql,3);
                     return $temp;
                 }
        }
        function getApprovalStatus($aid="")
        {
                 global $special_feature;
                 if (!$special_feature['announcement_approval']) return;
                 
                 $aid = IntegerSafe($aid);
                 if ($aid == "") $aid = $this->AnnouncementID;
                 $name_field = getNameFieldWithClassNumberByLang("b.");
                 $sql = "SELECT a.RecordStatus, a.UserID,
                         IF(b.UserID IS NULL,CONCAT('<I>',a.ApprovalUserName,'</I>'),$name_field)
                         ,IF(a.DateModified=a.DateInput,'',a.DateModified)
                         , a.Remark, a.Reason
                         FROM INTRANET_ANNOUNCEMENT_APPROVAL as a
                         LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.AnnouncementID = '$aid'";
                 $temp = $this->returnArray($sql,6);
                 if (sizeof($temp)==0 || $temp[0][0]=="") return false;
                 else return $temp[0];
        }
        function returnAnnouncerName()
        {
                 global $i_AnnouncementSystemAdmin;
                 if ($this->OwnerUserID == "") return $i_AnnouncementSystemAdmin;
                 $name_field = getNameFieldWithClassNumberByLang();
                 $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '".$this->OwnerUserID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnOwnerGroup()
        {
                 if ($this->OwnerGroupID == "") return "";
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '".$this->OwnerGroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnTargetGroups()
        {
	        global $intranet_session_language;
	        
	        $group_title = $intranet_session_language=="en" ? "a.Title" : "a.TitleChinese";
	        
                 if ($this->AnnouncementID == "") return array();
                 $sql = "SELECT DISTINCT $group_title 
                         FROM INTRANET_GROUPANNOUNCEMENT as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE AnnouncementID = ".$this->AnnouncementID;
                 return $this->returnVector($sql);
        }
        function returnTargetGroupsID()
        {
                 if ($this->AnnouncementID == "") return array();
                 $sql = "SELECT DISTINCT a.GroupID
                         FROM INTRANET_GROUPANNOUNCEMENT as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE AnnouncementID = ".$this->AnnouncementID;
                 return $this->returnVector($sql);
        }

        function isSchoolAnnouncement()
        {
                 $sql = "SELECT COUNT(GroupAnnouncementID) FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = '".$this->AnnouncementID."'";
                 $result = $this->returnVector($sql);
                 if ($result[0]==0)
                 {
                     return 1;
                 }
                 else
                 {
                     return 0;
                 }
        }

        function returnAnnouncement($AnnouncementID)
        {
            $AnnouncementID = IntegerSafe($AnnouncementID);
            
	        global $plugin;

	        if ($plugin['power_voice'])
	        {
                $sql = "SELECT AnnouncementID, Title, Description, DATE_FORMAT(AnnouncementDate, '%Y-%m-%d'), RecordType, RecordStatus, DateInput, DateModified, DATE_FORMAT(EndDate, '%Y-%m-%d'), Attachment, OwnerGroupID, UserID, Internal, PublicStatus, onTop, VoiceFile,TitleEng, DescriptionEng, WebsiteCode FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '$AnnouncementID'";
                return $this->returnArray($sql,17);
	        }
	        else
	        {
                $sql = "SELECT AnnouncementID, Title, Description, DATE_FORMAT(AnnouncementDate, '%Y-%m-%d'), RecordType, RecordStatus, DateInput, DateModified, DATE_FORMAT(EndDate, '%Y-%m-%d'), Attachment, OwnerGroupID, UserID, Internal, PublicStatus, onTop,TitleEng, DescriptionEng, WebsiteCode FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = '$AnnouncementID'";
                return $this->returnArray($sql,16);
            }
        }

        ######################################################################################

        function display($colortype=0){
                global $i_AnnouncementDate, $i_AnnouncementDateModified, $i_AnnouncementAttachment, $file_path, $image_path;
                global $intranet_httppath,$i_AnnouncementWholeSchool,$i_AnnouncementOwner,$i_AnnouncementTargetGroup;
                global $i_AnnouncementDescription;
                $announcer = $this->returnAnnouncerName();
                $ownerGroup = $this->returnOwnerGroup();
                $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer ($ownerGroup)";
                }
                if (sizeof($targetGroups)==0)
                    $target = $i_AnnouncementWholeSchool;
                else
                    $target = implode(", ",$targetGroups);

                if ($colortype==0)
                {
                    $bordercolordark = "#DBD6C4";
                    $bordercolorlight = "#FCF7E5";
                    $bgcolortag = "";
                }
                else if ($colortype==1)
                {
                     $bordercolorlight = "#FCF2CF";
                     $bordercolordark = "#D0C68F";
                     $bgcolortag="bgcolor=#FFE99E";
                }

                $x .= "<table width=80% border=1 bordercolordark=$bordercolordark bordercolorlight=$bordercolorlight cellpadding=2 cellspacing=1 $bgcolortag>\n";
                $x .= "<tr><td colspan=2 class=event_title>".intranet_wordwrap($this->Title,25,"\n",1)."</td></tr>\n";
                $x .= "<tr><td width=30% align=right class=event_content>$i_AnnouncementDate:</td><td class=event_content>".$this->AnnouncementDate."</td></tr>\n";
                $x .= "<tr><td align=right class=event_content>$i_AnnouncementOwner:</td><td class=event_content>".intranet_wordwrap($displayName,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right class=event_content>$i_AnnouncementTargetGroup:</td><td class=event_content>".intranet_wordwrap($target,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right class=event_content>$i_AnnouncementDescription:</td><td class=event_content>".nl2br($this->convertAllLinks2($this->Description,30))."</td></tr>\n";
                if ($this->Attachment != "")     # Display images
                {
                    $hasImage = false;
                    $displayImage .= "<tr><td align=left colspan=2>";
//                    $x .= "<tr><td class=event_content>$i_AnnouncementAttachment</td><td>\n";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);
                        /*
                        $displayImage .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayImage .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayImage .= " (".ceil($files[$key][1]/1000)."Kb)";
                        */
                        $size = GetImageSize($target_filepath);
                        list($width, $height, $type, $attr) = $size;

                        /*if ($width > 412 || $height > 550)
                        {
                            $image_html_tag = "width=412 height=550";
                        }
                        else
                        {
                            $image_html_tag = "";
                        }*/
                        
                        # modified by Kelvin Ho 2008-11-10
                        if ($width > 412 || $height > 550)
                        {
	                        if($height>$width)
                        		$image_html_tag = " height=550";
                        	elseif($height<$width)
                        		$image_html_tag = "width=412";
                        }else
                        {
                            $image_html_tag = "";
                        }


                        $displayImage .= "<a href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\"><img border=0 src=\"$intranet_httppath$url\" $image_html_tag></a>";

#                        $displayImage .= "<img src=\"$url\"> ";
                        $displayImage .= "<br>\n";
                        $hasImage = true;
                           }
                    }
                    $displayImage .= "</td></tr>\n";
                    if ($hasImage)
                    {
                        $x .= $displayImage;
                    }

                }
                if ($this->Attachment != "")
                {
                    $hasOther = false;
                    $displayOther .= "<tr><td align=left colspan=2>";
//                    $x .= "<tr><td class=event_content>$i_AnnouncementAttachment</td><td>\n";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (!isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);

                        $displayOther .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayOther .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
                        $displayOther .= "<br>\n";
                        $hasOther = true;
                        }
                    }
                    $displayOther .= "</td></tr>";
                    if ($hasOther)
                    {
                        $x .= $displayOther;
                    }

                }
                $x .= "</table>\n";
                return $x;
        }

        function display2($colortype=0)
        {
                global $i_AnnouncementDate, $i_AnnouncementDateModified, $i_AnnouncementAttachment, $file_path, $image_path, $LAYOUT_SKIN;
                global $intranet_httppath,$i_AnnouncementWholeSchool,$i_AnnouncementOwner,$i_AnnouncementTargetGroup;
                global $i_AnnouncementDescription,$plugin, $iPowerVoice;
                global $eclass40_httppath;
                global $Lang, $UserID;
                global $ss_intranet_plugin, $intranet_root, $sys_custom;
				
                $announcer = $this->returnAnnouncerName();
                $ownerGroup = $this->returnOwnerGroup();
                $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer ($ownerGroup)";
                }
                if (sizeof($targetGroups)==0)
                    $target = $i_AnnouncementWholeSchool;
                else
                    $target = implode(", ",$targetGroups);

				$titlecss = ($colortype==0) ? "indexnewslisttitle" : "indexnewslistgrouptitle";

                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>";
                $x .= "<tr>";
                $x .= "	<td>";
                $x .= "        	<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                $x .= "		<tr>";
                $x .= "	                <td align='left' class='indexpopsubtitle $titlecss'>".$this->Title."</td>";
                $x .= "                </tr>";
                $x .= "		</table>";
                $x .= "	</td>";
                $x .= "</tr>";
                $x .= "<tr>";
                $x .= "	<td>";
                $x .= "        	<table width='100%' border='0' cellspacing='4' cellpadding='3'>";
                $x .= "		<tr>";
                $x .= "			<td width='30%' valign='top' nowrap class='tabletext'>".$Lang['SysMgr']['SchoolNews']['StartDate']."</td>";
                $x .= "			<td bgcolor='#FFFFFF' class='tabletext' align='left' >". $this->AnnouncementDate ."</td>";
                $x .= "		</tr>";
                $x .= "		<tr>";
                $x .= "			<td valign='top' nowrap class='tabletext'>". $i_AnnouncementOwner ."<br></td>";
                $x .= "                	<td bgcolor='#FFFFFF' class='tabletext' align='left'>". intranet_wordwrap($displayName,30,"\n",1) ."</td>";
                $x .= "                </tr>";
                
                if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	                $x .= "                <tr>";
	                $x .= "                        <td valign='top' nowrap class='tabletext'>". $i_AnnouncementTargetGroup ."</td>";
	                $x .= "                        <td bgcolor='#FFFFFF' class='tabletext' align='left'>". intranet_wordwrap($target,30,"\n",1) ."</td>";
	                $x .= "                </tr>";
                }
                $x .= "                <tr>";
                $x .= "                        <td valign='top' nowrap class='tabletext'>". $i_AnnouncementDescription ."</td>";
                //$x .= "                        <td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'>"."<div id=announcement_id>". nl2br($this->convertAllLinks2($this->Description,30))."<div>"."</td>";
                //$x .= "                        <td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'>"."<div id=announcement_id>". nl2br(intranet_undo_htmlspecialchars($this->Description))."<div>"."</td>";
                //$x .= "                        <td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'>"."<div id=announcement_id>". intranet_undo_htmlspecialchars($this->Description)."<div>"."</td>";
                //$x .= "                        <td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'>"."<div id=announcement_id>". htmlspecialchars_decode($this->Description)."<div>"."</td>";
                //$x .= "                </tr>";
                $x .= "                        <td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'><div id='announce_msg'>". htmlspecialchars_decode($this->Description)."</div>";

                if($ss_intranet_plugin['power_speech'] && trim($this->Description)!="&amp;nbsp;"  && check_powerspeech_is_expired()!=2){             
//                	debug_r(generate_texttospeech_ui_IP('announce_msg'));  	
                	$x .= generate_texttospeech_ui_IP('announce_msg');
			/*	$x .= "                <tr>";
               	$x .= "                        <td valign='top' nowrap class='tabletext'></td>";
                $x .= "                        <td class='tabletext indexpop' align='left'><div class='TTS_PlayButton' ttsTarget='announcement_id' ttsPlayer='divPlayer'></div><div id='divPlayer'></div></td>";
                $x .= "                </tr>";*/
                }
                
                $x .= "</td></tr>";
                if ($this->VoiceFile != "")                                                                                                                                                       
                {  	                
	                //basename and dirname may work for UTF8.
	                //$VoiceFileName = basename($this->VoiceFile);    
	                //$VoiceDirName1 = dirname($this->VoiceFile);
	                //$VoiceDirName2 = basename($VoiceDirName1);
	                $VoiceFileName = get_file_basename($this->VoiceFile);

	                $VoiceDirName1 = substr($this->VoiceFile, 0, strrpos($this->VoiceFile, '/') === false ? strlen($this->VoiceFile) : strrpos($this->VoiceFile, '/'));
	                $VoiceDirName2 = get_file_basename($VoiceDirName1);;
	                
	                $path = "$file_path/file/announcement/".$VoiceDirName2;           

	                $target_filepath = $path."/".$VoiceFileName;
	                	                
					$url = str_replace($file_path, "", $path)."/".$VoiceFileName;
 					$url = intranet_handle_url($url);
	         	                
					$VoiceClip = " <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif\" border=\"0\" align=\"absmiddle\" /> ";
					//$displayHtml .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$VoiceFileName."';return true;\" onMouseOut=\"window.status='';return true;\">".$VoiceFileName."</a>";	                
	                $displayHtml .= "<a class=\"indexpoplink\" href=\"javascript:listenPVoice('$intranet_httppath$url')\" onMouseOver=\"window.status='".$VoiceFileName."';return true;\" onMouseOut=\"window.status='';return true;\">".$VoiceClip.$VoiceFileName."</a>";
					
					
                	$x .= "                <tr>";
                	$x .= "                        <td valign='top' nowrap class='tabletext'>".$iPowerVoice['sound_recording']."</td>";
                	$x .= "                        <td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'>". $displayHtml."</td>";
                	$x .= "                </tr>"; 
                }            
               
                $attNameDisplay = true;
                if ($this->Attachment != "") 	
                { 
                        $hasOther = false;
                    	$path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    	$real_path = $this->Attachment.$this->AnnouncementID;
                    	$templf = new libfiletable("", $path,0,0,"");
                    	$files = $templf->files;

                    	while (list($key, $value) = each($files)) 
                        {
                			if ($this->VoiceFile != "") 	
                			{
	                			if (get_file_basename($this->VoiceFile)==$files[$key][0])  	                
	                			{
		                			continue;
	                			}
                			}
	                        
							$target_filepath = "$path/".$files[$key][0];
                           	if (!isImage($target_filepath))
                           	{
								$url = str_replace($file_path, "", $real_path)."/".$files[$key][0];
								//$url = intranet_handle_url($url);
// 								$url = $url;
								//$url = rawurlencode($file_path."/file/announcement/".$url);
								$url = $file_path."/file/announcement/".$url;
								$displayOther .= "<tr>";
                        		$displayOther .= "<td valign='top' nowrap class='tabletext'>". ($attNameDisplay ? $i_AnnouncementAttachment : "") ."</td>";
                        		$displayOther .= "<td bgcolor='#FFFFFF' class='tabletext'>";
                                $displayOther .= "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                                //$displayOther .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                                $displayOther .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($url)."\" >".$files[$key][0]."</a>";
                                $displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
                                $displayImage .= "</td></tr>\n";
								
								$audio_url = str_replace($file_path, "", $path)."/".$files[$key][0];
								$audio_url = intranet_handle_url($audio_url);
								$listen = strpos($audio_url,".mp3");
								// hidden Listen Now button if without power voice plugin [2015-0209-1120-00066]
								if ($plugin['power_voice'] && $listen != FALSE){
									$displayOther .= "
									<INPUT type='button' class='formbutton' valign = 'center' value='Listen Now' onClick=\"window.open('http://$eclass40_httppath/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location=$audio_url','mywindow','width=400,height=200')\">
									";
								}

								$hasOther = true;
								$attNameDisplay = false;
							}
						}
                    	
                    	if ($hasOther)
                    	{
                        	$x .= $displayOther;
                    	}
                        
                        ### img file
                        $hasImage = false;
			
                    	$path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    	$templf = new libfiletable("", $path,0,0,"");
                    	$files = $templf->files;

                    	while (list($key, $value) = each($files)) 
                        {
							$target_filepath = "$path/".$files[$key][0];
                           	if (isImage($target_filepath))
                           	{
								$url = str_replace($file_path, "", $path)."/".$files[$key][0];
// 								$url = intranet_handle_url($url);

                        		$size = GetImageSize($target_filepath);
                        		list($width, $height, $type, $attr) = $size;
     					
								$image_html_tag = "";
								$href_start	= "<a class=\"indexpoplink\" href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">";
								$href_end	= "</a>";
                                                
                                if($width > 300)
                                	$image_html_tag = "style='width: 100%'";
                                        
                                $displayImage .= "<tr>";
                        		$displayImage .= "<td valign='top' nowrap class='tabletext'>". ($attNameDisplay ? $i_AnnouncementAttachment : "") ."</td>";
                        		$displayImage .= "<td bgcolor='#FFFFFF' class='tabletext' align='left'>";
                                $displayImage .= "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle>";
//                                 $displayImage .= $href_start . $files[$key][0] . "<br \>";
// 								$url = str_replace($file_path, "", $path)."/".$files[$key][0];
// 	                        	$url_download = rawurlencode($file_path.$url);
// 								$displayImage .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target=".$url_download."\" >".$files[$key][0]."</a><br \>";
								$displayImage .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($file_path.$url)."\" >".$files[$key][0]."</a><br \>";
								$displayImage .= "<img border=0 src=\"$url\" $image_html_tag>".$href_end;
                                $displayImage .= "</td></tr>\n";
                                
                        		$hasImage = true;
								$attNameDisplay = false;
                           	}                                
                    	}
                    	
                    	if ($hasImage)
                    	{
                        	$x .= $displayImage;
                    	}
                        
                        $x .= "</td></tr>";
                }
				if($this->RecordStatus!=1){
					$sql = "SELECT RecordStatus FROM INTRANET_ANNOUNCEMENT_APPROVAL WHERE AnnouncementID = '".$this->AnnouncementID."' AND UserID = '".$UserID."'";
					$result = $this->returnVector($sql);
					$RecordStatus = $result[0];
					
					if($RecordStatus!=""){
						$linterface = new interface_html();
						
						$x .= "<tr>";
						$x .= "<td valign='top' nowrap class='tabletext'>".$Lang['SysMgr']['SchoolNews']['Approval']."</td>";
						$x .= "<td bgcolor='#FFFFFF' class='tabletext indexpop' align='left'>";
						$x .= $linterface->GET_BTN($Lang['SysMgr']['SchoolNews']['Approve'], "button", "javascript:Approval(".$this->AnnouncementID.", 1)")."&nbsp;";
						if($RecordStatus!=3)
							$x .= $linterface->GET_BTN($Lang['SysMgr']['SchoolNews']['Reject'], "button", "javascript:Approval(".$this->AnnouncementID.", 3)");
						$x .= "</td>";
						$x .= "</tr>";
					}
				}
                
                $x .= "		</table></td>";
                $x .= "</tr>";  
                $x .= "</table>"; 
                return $x;
        }
 		
        function display_portal_popup(){
                global $i_AnnouncementDate, $i_AnnouncementDateModified, $i_AnnouncementAttachment, $file_path, $image_path;
                global $intranet_httppath,$i_AnnouncementWholeSchool,$i_AnnouncementOwner,$i_AnnouncementTargetGroup;
                global $i_AnnouncementDescription;
                $announcer = $this->returnAnnouncerName();
                $ownerGroup = $this->returnOwnerGroup();
                $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer ($ownerGroup)";
                }
                if (sizeof($targetGroups)==0)
                    $target = $i_AnnouncementWholeSchool;
                else
                    $target = implode(", ",$targetGroups);

                global $intranet_php_style;

                if ($intranet_php_style['announcement_popup_table']=="")
                {
                    $bordercolordark = "#D1C89D";
                    $bordercolorlight = "#FFFFFF";
                    $bgcolortag = "bgcolor=#FCEA88";
                    $table_attr = "$bgcolortag bordercolordark=$bordercolordark bordercolorlight=$bordercolorlight";
                }
                else
                {
                    $table_attr = $intranet_php_style['announcement_popup_table'];
                }
                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 class=body $table_attr>\n";
                $x .= "<tr class=announcement_title><td colspan=2 class=announcement_title><STRONG>".intranet_wordwrap($this->Title,25,"\n",1)."</STRONG></td></tr>\n";
                $x .= "<tr><td width=20% align=right >$i_AnnouncementDate:</td><td >".$this->AnnouncementDate."</td></tr>\n";
                $x .= "<tr><td align=right>$i_AnnouncementOwner:</td><td >".intranet_wordwrap($displayName,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right >$i_AnnouncementTargetGroup:</td><td>".intranet_wordwrap($target,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right >$i_AnnouncementDescription:</td><td>".nl2br($this->convertAllLinks2($this->Description,30))."</td></tr>\n";
                if ($this->Attachment != "")     # Display images
                {
                    $hasImage = false;
                    $displayImage .= "<tr><td align=left colspan=2>";
//                    $x .= "<tr><td class=event_content>$i_AnnouncementAttachment</td><td>\n";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);
                        /*
                        $displayImage .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayImage .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayImage .= " (".ceil($files[$key][1]/1000)."Kb)";
                        */
                        $size = GetImageSize($target_filepath);
                        list($width, $height, $type, $attr) = $size;
                        /*if ($width > 412 || $height > 550)
                        {
                            $image_html_tag = "width=412 height=550";
                        }
                        else
                        {
                            $image_html_tag = "";
                        }*/
                        # modified by Kelvin Ho 2008-11-10
                        if ($width > 412 || $height > 550)
                        {
	                        if($height>$width)
                        		$image_html_tag = " height=550";
                        	elseif($height<$width)
                        		$image_html_tag = "width=412 ";
                        }else
                        {
                            $image_html_tag = "";
                        }


                        $displayImage .= "<a href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\"><img border=0 src=\"$intranet_httppath$url\" $image_html_tag></a>";
                        $displayImage .= "<br>\n";
                        $hasImage = true;
                           }
                    }
                    $displayImage .= "</td></tr>\n";
                    if ($hasImage)
                    {
                        $x .= $displayImage;
                    }

                }
                if ($this->Attachment != "")
                {
                    $hasOther = false;
                    $displayOther .= "<tr><td align=left colspan=2>";
//                    $x .= "<tr><td class=event_content>$i_AnnouncementAttachment</td><td>\n";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (!isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);

                        $displayOther .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayOther .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
                        $displayOther .= "<br>\n";
                        $hasOther = true;
                        }
                    }
                    $displayOther .= "</td></tr>";
                    if ($hasOther)
                    {
                        $x .= $displayOther;
                    }

                }
                $x .= "</table>\n";
                return $x;
        }

        function displayAttachmentEdit($name)	
        {
			global $file_path, $image_path, $eclass40_httppath, $plugin;
			
			//$url = str_replace($file_path, "", $path)."/".$files[$key][0];
	        //$url = rawurlencode($file_path.$url);
			//$x .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target=$url\" >".$files[$key][0]."</a>";
			
			$path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			$x = "";
			
			while (list($key, $value) = each($files)) 
			{	                 
				if ($this->VoiceFile != "") 	
    			{
        			if (get_file_basename($this->VoiceFile)==$files[$key][0])  	                
        			{
            			continue;
        			}
    			}
	                 
               	$url = str_replace($file_path, "", $path)."/".$files[$key][0];
//                	$url = intranet_handle_url($url);
				
				$listen = strpos($url,".mp3");
               	
                #$url = addslashes($url);
                $x .= "<tr><td width=\"1\"><input type=\"checkbox\" name=\"$name\" value=\"".urlencode($files[$key][0])."\"  onClick=\"setRemoveFile($key,this.checked)\"></td>";
                $x .="<td id=\"a_".$key."\" class=\"tabletext\"><img src=\"$image_path/file.gif\" hspace=\"2\" vspace=\"2\" border=\"0\" align=\"absmiddle\">";
                //$x .= "<a class=\"tablelink\" target=\"_blank\" href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\" >".$files[$key][0]."</a>";
                
                $dl_url = str_replace($file_path, "", $path)."/".$files[$key][0];
	        	//$dl_url = rawurlencode($file_path.$url);
	        	$dl_url = $file_path.$url;
	        	
				//$x .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target=$dl_url\" >".$files[$key][0]."</a>";
				$x .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target_e=".getEncryptedText($dl_url)."\" >".$files[$key][0]."</a>";
				
                
                $x .= " (".ceil($files[$key][1]/1000)."Kb)";
				
				// hidden Listen Now button if without power voice plugin [2015-0209-1120-00066]
				if ($plugin['power_voice'] && $listen != FALSE){
					$x .= "
					<INPUT type='button' class='formbutton' value='Listen Now' onClick=\"window.open('http://$eclass40_httppath/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location=$url','mywindow','width=400,height=200')\">
					</td>";
				}
			}
			$x .= "</tr>\n";
             return $x;
        
		}

        ######################################################################################

        function returnSchoolAnnouncement($UserID)
        {
	        global $plugin;
	        
            if ($plugin['power_voice'])
            {
            	$ExtraField = ", a.VoiceFile";
            	$ArrayNum = 8;
        	}
        	else
        	{
            	$ExtraField = "";
            	$ArrayNum = 7;
        	}

	        $orderBy = " a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID DESC, a.Title ASC";
// 	        $orderBy = "a.AnnouncementDate DESC, a.AnnouncementID ASC ";
	        
			$name_field = getNameFieldWithClassNumberByLang("b.");
            $sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
            $AnnouncementIDs = $this->db_sub_select($sql);
            $conds = "a.AnnouncementID NOT IN ($AnnouncementIDs) AND a.RecordStatus = '1' AND (a.EndDate is null OR (CURDATE()>=a.AnnouncementDate AND CURDATE() <= a.EndDate))";
            $sql2 = "	SELECT 
            				a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), 
            				a.Attachment, $name_field, c.Title {$ExtraField}
                    	FROM 
                    		INTRANET_ANNOUNCEMENT as a
								LEFT OUTER JOIN 
							INTRANET_USER as b 
								ON a.UserID = b.UserID
								LEFT OUTER JOIN 
							INTRANET_GROUP as c 
								ON a.OwnerGroupID = c.GroupID
						WHERE 
							$conds 
						ORDER BY 
							$orderBy
						";
// 							debug_pr($sql);
            return $this->returnArray($sql2,$ArrayNum);
        }
        function returnAllSchoolAnnouncement($UserID, $StartDate="", $EndDate="")
        {
	        global $plugin;
	        
            if ($plugin['power_voice'])
            {
            	$ExtraField = ", a.VoiceFile";
            	$ArrayNum = 9;
        	}
        	else
        	{
            	$ExtraField = "";
            	$ArrayNum = 8;
        	}
	        
	        $orderBy = "a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID DESC";
// 	        $orderBy = "a.AnnouncementDate DESC, a.AnnouncementID ASC";
	        
			$name_field = getNameFieldWithClassNumberByLang("b.");
            $sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
            $AnnouncementIDs = $this->db_sub_select($sql);
            $conds = "a.AnnouncementID NOT IN ($AnnouncementIDs) AND a.RecordStatus = '1'";
            
            if($StartDate!="" && $EndDate!="") {
            	$conds .= " AND (a.AnnouncementDate BETWEEN '$StartDate' AND '$EndDate')";	
            } else {
	            $conds .= " AND (a.EndDate is null OR CURDATE()>=a.AnnouncementDate)";
            }
            
            $sql = "	SELECT 
            				a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), 
            				a.Attachment, $name_field, c.Title, a.Description {$ExtraField}, a.EndDate
                    	FROM 
                    		INTRANET_ANNOUNCEMENT as a
								LEFT OUTER JOIN 
							INTRANET_USER as b 
								ON a.UserID = b.UserID
                         		LEFT OUTER JOIN 
                         	INTRANET_GROUP as c 
                         		ON a.OwnerGroupID = c.GroupID
                         	WHERE 
                         		$conds 
                         	ORDER BY 
                         		$orderBy
                         ";
            return $this->returnArray($sql,$ArrayNum);
        }

        function returnPublicSchoolAnnouncement(){
                $sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
                $AnnouncementIDs = $this->db_sub_select($sql);
                $conds = "AnnouncementID NOT IN ($AnnouncementIDs) AND RecordStatus = '1' AND (EndDate is null OR (CURDATE()>=AnnouncementDate AND CURDATE() <= EndDate)) AND RecordType = 1";
                $sql = "SELECT AnnouncementID, DATE_FORMAT(AnnouncementDate, '%Y-%m-%d'), Title, Attachment FROM INTRANET_ANNOUNCEMENT WHERE $conds ORDER BY AnnouncementDate DESC, AnnouncementID ASC";
                return $this->returnArray($sql,4);
        }

        function displaySchoolAnnouncement($UserID){
                global $image_path,$intranet_session_language;
                global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                $row = $this->returnSchoolAnnouncement($UserID);
                $x = "<br><img src=$image_path/index/announcement_bullet.gif> <span class=h1>$i_AnnouncementPublic</span><br>\n";
                #$x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#FFFFFF>\n";
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#FCEA88 bordercolordark=#FFFFFF bgcolor=#FCEA88>
                    <tr>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementTitle</strong></td>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        #$x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#FFFFFF>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    else $displayName = $announcer;
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top class=body>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td width=30>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top class=bodylink>$clip<a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top class=bodylink>$displayName</td>\n";
                                $x .= "</tr>\n";
/*
                                $x .= "<td align=center class=body width=21%>$AnnouncementDate</td>\n";
                                $x .= "<td class=bodylink width=79%><a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a>".(($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "")." $clip</td>\n";
*/
                        }
                }
                $x .= "</table>\n";
                $x .= "<table width=495 border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td align=right><a href=javascript:moreNews(0)><img src=\"$image_path/btn_more_$intranet_session_language.gif\" border=0></a></td>
                    </tr></table>";
                return $x;
        }

        ######################################################################################

        function returnGroupAnnouncement($GroupID, $limit, $top_record='-1'){
                global $UserID;
                $GroupID = IntegerSafe($GroupID);
                $name_field = getNameFieldWithClassNumberByLang("c.");
                $sql = "
									SELECT a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), a.Attachment,$name_field,d.Title, a.PublicStatus, a.onTop  
									FROM 
										INTRANET_ANNOUNCEMENT AS a
                    LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
                    LEFT OUTER JOIN INTRANET_GROUP as d ON a.OwnerGroupID = d.GroupID
                    , INTRANET_GROUPANNOUNCEMENT AS b
                WHERE b.GroupID = '$GroupID' AND b.AnnouncementID = a.AnnouncementID
									AND a.AnnouncementDate <= CURDATE() AND a.EndDate >= CURDATE()
									AND a.RecordStatus = '1' ";
									
                if($top_record==1)	 	$sql .= " and a.onTop=1 ";   
                if($top_record==0)	 	$sql .= " and a.onTop=0 ";   
                
                //$sql.= " ORDER BY a.AnnouncementDate DESC , a.AnnouncementID DESC";
		        $sql .= " ORDER BY a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID ASC";   
                
                if($limit) $sql .= " limit $limit";
                
/*
	
                $sql = "SELECT a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), a.Attachment, $name_field, c.Title
                        FROM INTRANET_ANNOUNCEMENT as a
                             LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as c ON a.OwnerGroupID = c.GroupID
                             WHERE $conds ORDER BY a.AnnouncementDate DESC";
*/
	//hdebug_r($sql);
                return $this->returnArray($sql,8);
        }
        function returnGroupsAnnouncement($Groups)
        {
                global $UserID, $plugin;
                
                if (is_array($Groups) && count($Groups)>0)
                {
                	$groupList = implode(",",IntegerSafe($Groups));
            	}
            	else
            	{
	            	return array();
            	}
            	
                $name_field = getNameFieldWithClassNumberByLang("c.");
                if ($plugin['power_voice'])
                {
                	$ExtraField = ", a.VoiceFile";
                	$ArrayNum = 8;
            	}
            	else
            	{
	            	$ExtraField = "";
	            	$ArrayNum = 7;
            	}            	
	        
		        //$orderBy = "a.AnnouncementDate DESC, a.AnnouncementID DESC";
		        $orderBy = " a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID ASC";   
                
                $sql = "
                			SELECT 
                				DISTINCT a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), 
                				a.Attachment,$name_field,d.Title {$ExtraField}
                			FROM 
                					INTRANET_ANNOUNCEMENT AS a
                     			LEFT OUTER JOIN 
                     				INTRANET_USER as c 
                     			ON 
                     				a.UserID = c.UserID
                     			LEFT OUTER JOIN 
                     				INTRANET_GROUP as d 
                     			ON 
                     				a.OwnerGroupID = d.GroupID
                     				, INTRANET_GROUPANNOUNCEMENT AS b
                				WHERE 
                					b.GroupID IN ($groupList) AND b.AnnouncementID = a.AnnouncementID
                					AND a.AnnouncementDate <= CURDATE() AND a.EndDate >= CURDATE()
                					AND a.RecordStatus = '1' ORDER BY $orderBy
                			";
                			
                			//debug_pr($sql);

/*
                $sql = "SELECT a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), a.Attachment, $name_field, c.Title
                        FROM INTRANET_ANNOUNCEMENT as a
                             LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             LEFT OUTER JOIN INTRANET_GROUP as c ON a.OwnerGroupID = c.GroupID
                             WHERE $conds ORDER BY a.AnnouncementDate DESC";
*/
                return $this->returnArray($sql,$ArrayNum);
        }
        
        
        function returnGroupsAnnouncement_Web_Service($Groups, $fromeClassApp=false)
        {
        		// Parent Apps (called from /includes/libeclass_ws.php)
                global $PATH_WRT_ROOT, $UserID, $special_feature, $plugin;
				
				include_once($PATH_WRT_ROOT.'includes/libschoolnews.php');
				$lschoolnews = new libschoolnews();

				if(sizeof($Groups)!=0)
				{
					$groupList = implode(",",IntegerSafe($Groups));
					$name_field = getNameFieldWithClassNumberByLang("c.");
					// JR20: main page
					$start_date_sql = ($special_feature['portal']) ? format_date_no_year_sql("a.AnnouncementDate") : format_date_sql("a.AnnouncementDate");
					
					if(!$special_feature['sch_announcement_display_future'])
					{
						$check_shart_date = "CURDATE()>=a.AnnouncementDate AND";
					}
					// [2016-0513-1440-15206] add checking for Setting - Allow users to view past news
					$check_end_date = $lschoolnews->allowUserToViewPastNews? " 1 " : " a.EndDate >= CURDATE() ";
				
					$order =  $special_feature['sch_announcement_display_sorting_asc'] ? "ASC" : "DESC";
					
					if ($fromeClassApp) {
						$cond_date = " And DATE(a.AnnouncementDate) <= DATE(now()) AND ";
						// [2016-0513-1440-15206] add checking for Setting - Allow users to view past news
						$cond_date .= $lschoolnews->allowUserToViewPastNews? " 1 " : " DATE(now()) <= DATE(a.EndDate) "; 
					}
					
					if ($plugin['power_voice'])                        
					{
						$sql = "	SELECT 
										a.AnnouncementID,
										a.Title,
										a.Description,
										a.AnnouncementDate,
										$name_field,
										'S',
										IF(a.Attachment IS NOT NULL, 'with attachment', ''),
										UNIX_TIMESTAMP(AnnouncementDate),
										a.ReadFlag,
										a.VoiceFile,
										a.Attachment,
                                        a.EndDate,
                                        a.onTop
									FROM
										INTRANET_ANNOUNCEMENT AS a
                                        LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
                                        LEFT OUTER JOIN INTRANET_GROUP as d ON a.OwnerGroupID = d.GroupID
							 			, INTRANET_GROUPANNOUNCEMENT AS b
									WHERE 
										b.GroupID IN ($groupList) 
										AND b.AnnouncementID = a.AnnouncementID
										AND ". $check_shart_date ." ".$check_end_date."
										AND a.RecordStatus = '1' 
										$cond_date 
									GROUP BY
										a.AnnouncementID
									ORDER BY 
										a.onTop DESC, 
										a.AnnouncementDate ". $order .", 
										a.Title ASC
								";
							return $this->returnArray($sql,8);						
					}
					else
					{								
						$sql = "	SELECT 
										a.AnnouncementID,
										a.Title,
										a.Description,
										a.AnnouncementDate,
										$name_field,
										'S',
										IF(a.Attachment IS NOT NULL, 'with attachment', ''),
										UNIX_TIMESTAMP(AnnouncementDate),
										a.ReadFlag,
										a.Attachment,
                                        a.EndDate,
                                        a.onTop
									FROM
										INTRANET_ANNOUNCEMENT AS a
                                        LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
                                        LEFT OUTER JOIN INTRANET_GROUP as d ON a.OwnerGroupID = d.GroupID
							 			, INTRANET_GROUPANNOUNCEMENT AS b
									WHERE 
										b.GroupID IN ($groupList) 
										AND b.AnnouncementID = a.AnnouncementID
										AND ". $check_shart_date ." ".$check_end_date."
										AND a.RecordStatus = '1' 
										$cond_date 
									GROUP BY
										a.AnnouncementID
									ORDER BY 
										a.onTop DESC, 
										a.AnnouncementDate ". $order .", 
										a.Title ASC
								";
							return $this->returnArray($sql,8);
					}
				}
				else
					return NULL;
        }
        
        function returnAllGroupsAnnouncement($Groups, $type='1'){
                global $UserID;
                $groupList = implode(",",IntegerSafe($Groups));
                $name_field = getNameFieldWithClassNumberByLang("c.");
	        
		        //$orderBy = "a.AnnouncementDate DESC, a.AnnouncementID DESC";
		        $orderBy = "a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID ASC";
	        
		        if ($type == "2") {
		        	$sql = "SELECT DISTINCT a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), a.Attachment,$name_field,d.Title,a.EndDate
		        	FROM INTRANET_ANNOUNCEMENT AS a
		        	LEFT JOIN INTRANET_USER as c ON a.UserID = c.UserID
		        	LEFT JOIN INTRANET_GROUP as d ON a.OwnerGroupID = d.GroupID
		        	LEFT JOIN INTRANET_GROUPANNOUNCEMENT AS b ON (b.AnnouncementID = a.AnnouncementID)
		        	WHERE ((b.GroupID IN ($groupList)) OR (b.GroupAnnouncementID IS NULL)) 
		        	AND a.AnnouncementDate <= CURDATE()
		        	AND a.RecordStatus = '1' GROUP BY a.AnnouncementID ORDER BY $orderBy";
		        } else {
	                $sql = "SELECT DISTINCT a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), a.Title, IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), a.Attachment,$name_field,d.Title,a.EndDate
	                FROM INTRANET_ANNOUNCEMENT AS a
	                     LEFT OUTER JOIN INTRANET_USER as c ON a.UserID = c.UserID
	                     LEFT OUTER JOIN INTRANET_GROUP as d ON a.OwnerGroupID = d.GroupID
	                     , INTRANET_GROUPANNOUNCEMENT AS b
	                WHERE b.GroupID IN ($groupList) AND b.AnnouncementID = a.AnnouncementID
	                AND a.AnnouncementDate <= CURDATE() 
	                AND a.RecordStatus = '1' ORDER BY $orderBy";
                }
                return $this->returnArray($sql,7);
        }

        function displayGroupAnnounce($Groups){
                 global $image_path,$intranet_session_language;
                 global $i_iconAttachment,$i_AnnouncementGroup,
                $i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                #$GroupID = $Groups[0];
                #$Title = intranet_wordwrap( $Groups[1],40,"\n",1);
                $row = $this->returnGroupsAnnouncement($Groups);
                $x = "<br><img src=/images/index/announcement_bullet.gif> <span class=h1>$i_AnnouncementGroup</span><br>\n";
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#FCEA88 bordercolordark=#FFFFFF bgcolor=#FCEA88>
                    <tr>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementTitle</strong></td>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        #$x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#FFFFFF>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    else $displayName = $announcer;
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top class=body>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td width=30>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top class=bodylink><a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top class=bodylink>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td>$displayName</td>
                                             <td width=18 align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
/*
                                $x .= "<td align=center class=body width=21%>$AnnouncementDate</td>\n";
                                $x .= "<td class=bodylink width=79%><a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a>".(($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "")." $clip</td>\n";
*/
                        }
                }
                $x .= "</table>\n";
                
                
                $x .= "<table width=495 border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td align=right><a href=javascript:moreNews(1)><img src=\"$image_path/btn_more_$intranet_session_language.gif\" border=0></a></td>
                    </tr></table>";
                return $x;
                /*
                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#FFFFFF>\n";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center class=body width=21%>--</td><td class=body width=79%>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);
//                                $AnnouncementTitle = $row[$i][2];
                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $clip = ($Attachment == ""? "": " $i_iconAttachment");
                                $x .= "<tr>\n";
                                $x .= "<td align=center class=body width=21%>$AnnouncementDate</td>\n";
                                $x .= "<td class=bodylink width=79%><a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a>".(($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "")."$clip</td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
                */
        }
        function displayGroupAnnounceInGroup($Groups){
                 global $i_iconAttachment,$i_AnnouncementGroup,
                 $i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                $row = $this->returnGroupsAnnouncement($Groups);
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#FCEA88 bordercolordark=#FFFFFF bgcolor=#FCEA88>
                    <tr>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementTitle</strong></td>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    else $displayName = $announcer;
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top class=body>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td width=30>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top class=bodylink><a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top class=bodylink>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td>$displayName</td>
                                             <td width=18 align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                global $image_path,$intranet_session_language;
                $x .= "<table width=495 border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td align=right><a href=javascript:moreNews(1)><img src=\"$image_path/btn_more_$intranet_session_language.gif\" border=0></a></td>
                    </tr></table>";
                return $x;
        }

        
        function displayAllAnnouncement($type=0,$Groups=array())
        {
                 global $UserID;
                 if ($type==0)
                 {
                     $row = $this->returnAllSchoolAnnouncement($UserID);
                 }
                 else
                 {
	                 if(sizeof($Groups)==1 && $Groups[0]=="" || $type == 2){
                     	$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID'";
                     	$Groups = $this->returnVector($sql);
                     }
                     $row = $this->returnAllGroupsAnnouncement($Groups, $type);
                 }
                 return $this->displayAnnouncementList($row);
        }
        
        /*
        function displayAnnouncementList($row){
                global $image_path,$intranet_session_language;
                global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#FCEA88 bordercolordark=#FFFFFF bgcolor=#FCEA88>
                    <tr>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementTitle</strong></td>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                    {
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    }
                                    else
                                    {
                                        $displayName = $announcer;
                                    }
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top class=body>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td width=30>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top class=bodylink><a class=bodylink href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top class=bodylink>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td>$displayName</td>
                                             <td width=18 align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }
        */
        
        function displayAnnouncementList($row){
                global $image_path,$intranet_session_language;
                global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin, $image_path, $LAYOUT_SKIN, $intranet_root;
                    
					$x .= "<table width='100%' border='0' cellspacing='4' cellpadding='3'>";
			        $x .= "<tr>";
			        $x .= "<td width='120' valign='top' nowrap class='tabletop tabletopnolink'>$i_AnnouncementDisplayDate</td>";
			        $x .= "<td class='tabletop tabletopnolink'>$i_AnnouncementTitle</td>";
			        $x .= "<td width='120' class='tabletop tabletopnolink'>$i_AnnouncementOwner</td>";
			        $x .= "</tr>";

                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
			        	$x .= "<tr>";
						$x .= "<td align='center' colspan='3' valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>".$i_no_record_exists_msg."</td>";
		                $x .= "</tr>";
			                
                }else{
                		include_once($intranet_root.'/includes/libschoolnews.php');
                		$lschoolnews = new libschoolnews();
                	
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);
								$AnnouncementEndDate = date('Y-m-d', strtotime($row[$i]['EndDate']));
								
								if (!$lschoolnews->allowUserToViewPastNews && (date('Y-m-d') > $AnnouncementEndDate)) {
									continue;
								}
                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                    {
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    }
                                    else
                                    {
                                        $displayName = $announcer;
                                    }
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " <img src=\"{$image_path}/{$LAYOUT_SKIN}/index/whatnews/icon_attachment.gif\" /> ");
                				$newFlag = (($IsNew) ? "<img src='{$image_path}/{$LAYOUT_SKIN}/alert_new.gif' align='absmiddle' border='0' />" : "&nbsp;");

                                $x .= "<tr>";
                                $x .= "<td width='30%' valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>".$AnnouncementDate." ~ ".$AnnouncementEndDate." ". $newFlag. "</td>";
		                        $x .= "<td valign='top' bgcolor='#FFFFFF' class='tabletext'>$clip<a href='javascript:fe_view_announcement($AnnouncementID)' class=' $title_css tablelink'>".$AnnouncementTitle." </a></td>";
		                        $x .= "<td width='30%' valign='top' nowrap bgcolor='#FFFFFF' class='tabletext'>$displayName</td>";
		                        $x .= "</tr>";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }
        
        

        ######################################################################################

        function displaySchoolAnnouncement_portlet($UserID){
                global $image_path,$intranet_session_language;
                global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                $row = $this->returnSchoolAnnouncement($UserID);
                $x = "<br><img src=$image_path/index/announcement_bullet.gif> <span>$i_AnnouncementPublic</span><br>\n";
                $x .= "<table width=90% border=1 cellpadding=2 cellspacing=0>
                    <tr>
                      <td width=25% align=left><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left><strong>$i_AnnouncementTitle</strong></td>
                      <td width=30% align=left><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    else $displayName = $announcer;
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top>
                                         <table width=100% border=0 cellpadding=0 cellspacing=0>
                                           <tr>
                                             <td width=30%>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top><a href=javascript:new_fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top>
                                         <table width=100% border=0 cellpadding=0 cellspacing=0>
                                           <tr>
                                             <td>$displayName</td>
                                             <td width=18% align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                $x .= "<table width=90% border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td align=right><a href=javascript:moreNews(0)><img src=\"$image_path/btn_more_$intranet_session_language.gif\" border=0></a></td>
                    </tr></table>";
                return $x;
        }

        function displayGroupAnnounce_portlet($Groups){
                 global $image_path,$intranet_session_language;
                 global $i_iconAttachment,$i_AnnouncementGroup,
                $i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                $row = $this->returnGroupsAnnouncement($Groups);
                $x = "<br><img src=/images/index/announcement_bullet.gif> <span>$i_AnnouncementGroup</span><br>\n";
                $x .= "<table width=90% border=1 cellpadding=2 cellspacing=0>
                    <tr>
                      <td width=25% align=left><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left><strong>$i_AnnouncementTitle</strong></td>
                      <td width=30% align=left><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    else $displayName = $announcer;
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top>
                                         <table width=100% border=0 cellpadding=0 cellspacing=0>
                                           <tr>
                                             <td width=30%>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top><a href=javascript:fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top>
                                         <table width=100% border=0 cellpadding=0 cellspacing=0>
                                           <tr>
                                             <td>$displayName</td>
                                             <td width=18% align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                $x .= "<table width=90% border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td align=right><a href=javascript:moreNews(1)><img src=\"$image_path/btn_more_$intranet_session_language.gif\" border=0></a></td>
                    </tr></table>";
                return $x;
        }

        ########################################################################################

        function displaySchoolAnnouncement_portal($UserID){
                global $image_path,$intranet_session_language;
                global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin,$i_general_more;
                $row = $this->returnSchoolAnnouncement($UserID);
                $x .= "<table width=100% border=0 cellpadding=2 cellspacing=0>\n";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=2 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    if ($announcer == "")
                                        $displayName = "$i_AnnouncementSystemAdmin";
                                    else $displayName = $announcer;
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=\"$image_path/portal/icon_new.gif\" hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<tr>
                                         <td width=15 class=news-left><img src=\"$image_path/portal/index/bullet_arrow.gif\" width=12 height=12></td>
                                         <td width=90 class=news-date>$AnnouncementDate</td>
                                         <td class=news-date><a href=javascript:new_fe_view_announcement($AnnouncementID)>$AnnouncementTitle</a>$clip $newFlag</td>
                                         <td class=news-right>&nbsp;</td>
                                       ";
                                $x .= "</tr>\n";

                      }
                }
                $x .= "<tr><td colspan=4 align=right><a href=javascript:moreNews(0)>$i_general_more ...</a></td></tr>\n";
                $x .= "</table>\n";
                /*
                $x .= "<table width=100% border=0 cellpadding=0 cellspacing=0>
                    <tr>
                      <td align=right><a href=javascript:moreNews(0)><img src=\"$image_path/btn_more_$intranet_session_language.gif\" border=0></a></td>
                    </tr></table>";
                    */
                return $x;
        }

                # for alumni
                # --- index.php
        function returnSchoolAnnouncement2($GroupID)
        {
                 global $i_general_sysadmin;
                 
                 $GroupID = IntegerSafe($GroupID);
                 
                 $user_field = getNameFieldWithClassNumberByLang("c.");
                 $table_ext = "LEFT OUTER JOIN INTRANET_ANNOUNCEMENT_APPROVAL as approval ON a.AnnouncementID = approval.AnnouncementID";
                 
                 $sql  = "SELECT
                                DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),
                                DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
                                CONCAT('<a href=javascript:alumni_view_announcement(', a.AnnouncementID, ')><span class=13-blue>', a.Title, '</span></a> '),
                                IF (a.UserID IS NOT NULL AND a.UserID != 0,$user_field,'$i_general_sysadmin') as Username
                          FROM
                              INTRANET_ANNOUNCEMENT as a LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.UserID
                                                    LEFT OUTER JOIN INTRANET_GROUPANNOUNCEMENT AS b ON a.AnnouncementID = b.AnnouncementID AND b.GroupID = '$GroupID' 
                                                    $table_ext

                              WHERE
                                   a.RecordStatus = 1 AND
                                   a.AnnouncementDate <= CURDATE() AND
                                   b.GroupAnnouncementID IS NOT NULL
                                   ORDER BY a.AnnouncementDate DESC, a.DateModified  DESC
                                   LIMIT 0, 5
                           ORDER BY 
                                a.AnnouncementDate DESC
                              ";
                return $this->returnArray($sql,4);
        }
                # for alumni
                # --- index.php
        function displaySchoolAnnouncement2($UserID){
                global $image_path,$intranet_session_language;
                $row = $this->returnSchoolAnnouncement2($UserID);
                                $x ="";

                                if(sizeof($row)==0){
                                                        global $i_no_record_exists_msg;
                                                        $x .= "<span class='13-blue'>$i_no_record_exists_msg</span>\n";
                                }
                                else{
                                for($i=0; $i<sizeof($row); $i++){

                                $AnnouncementDate = $row[$i][0];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);
                                $announcer = $row[$i][3];

                                                                $x .=        "<table width='100%' border='0' cellspacing='0' cellpadding='0'>\n";
                                                                $x .=        "<tr>\n";
                                                                $x .=        "<td width='25' valign='top'><img src='$image_path/bullet.gif' width='25' height='17' align='absmiddle'></td>\n";
                                                                $x .=         "<td class='13-blue'>" . $AnnouncementTitle ."</td>\n";
                                                                $x .=        "</tr>\n";
                                                                $x .=        "<tr>\n";
                                                                $x .=        "<td>&nbsp;</td>\n";
                                                                $x .=        "<td class='11-grey'>" . $AnnouncementDate . " " . $announcer  ."</td>\n";
                                                                $x .=        "</tr>\n";
                                                                $x .=        "<tr>\n";
                                                                $x .=        "<td>&nbsp;</td>\n";
                                                                $x .=        "<td class='11-grey'>&nbsp;</td>\n";
                                                                $x .=        "</tr>\n";
                                                                $x .=        "</table>\n";

                                                        }
                                }

                return $x;
        }
                # for alumni
                # --- index.php
        function displayAllAnnouncement2($type=0)
        {
                 global $UserID;
                 if ($type==0)
                 {
                     $row = $this->returnAllSchoolAnnouncement($UserID);
                 }
                 else
                 {
                     $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID'";
                     $Groups = $this->returnVector($sql);
                     $row = $this->returnAllGroupsAnnouncement($Groups);
                 }
                 return $this->displayAnnouncementList2($row);
        }
        function displayAnnouncementList2($row){
                global $image_path,$intranet_session_language;
                global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
                       ,$i_AnnouncementSystemAdmin;
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#5DA5C9 bordercolordark=#FFFFFF bgcolor=#FFFFFF class=13-black>
                    <tr bgcolor=#E3DB9C>
                      <td width=120 align=left  class=13-black-bold><strong>$i_AnnouncementDisplayDate</strong></td>
                      <td align=left class=13-black-bold><strong>$i_AnnouncementTitle</strong></td>
                      <td width=120 align=left  class=13-black-bold><strong>$i_AnnouncementOwner</strong></td>
                    </tr>";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                	
                        for($i=0; $i<sizeof($row); $i++){
                                $AnnouncementID = $row[$i][0];
                                $AnnouncementDate = $row[$i][1];
                                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                                $IsNew = $row[$i][3];
                                $Attachment = $row[$i][4];
                                $announcer = $row[$i][5];
                                $ownerGroup = $row[$i][6];

                                if ($ownerGroup == "")
                                {
                                    $displayName = "$i_AnnouncementSystemAdmin";
                                }
                                else
                                {
                                    $displayName = "$announcer <br> $ownerGroup";
                                }

                                $clip = ($Attachment == ""? "&nbsp;": " $i_iconAttachment");
                                $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                                $x .= "<tr>\n";
                                $x .= "<td valign=top class=body>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td width=30>$newFlag</td>
                                             <td>$AnnouncementDate</td>
                                           </tr>
                                         </table>
                                       </td>
                                       <td align=left valign=top class=bodylink><a class=bodylink href=javascript:alumni_view_announcement($AnnouncementID)>$AnnouncementTitle</a></td>
                                       <td valign=top class=bodylink>
                                         <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                           <tr>
                                             <td>$displayName</td>
                                             <td width=18 align=right>$clip</td>
                                           </tr>
                                         </table>
                                       </td>\n";
                                $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }
        function display3($colortype=0){
                global $i_AnnouncementDate, $i_AnnouncementDateModified, $i_AnnouncementAttachment, $file_path, $image_path;
                global $intranet_httppath,$i_AnnouncementWholeSchool,$i_AnnouncementOwner,$i_AnnouncementTargetGroup;
                global $i_AnnouncementDescription;
                $announcer = $this->returnAnnouncerName();
                $ownerGroup = $this->returnOwnerGroup();
                $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer ($ownerGroup)";
                }
                if (sizeof($targetGroups)==0)
                    $target = $i_AnnouncementWholeSchool;
                else
                    $target = implode(", ",$targetGroups);

                if ($colortype==0)
                {
                    $bordercolordark = "#ffffff";
                    $bordercolorlight = "#5DA5C9";
                    $bgcolortag = "bgcolor=#ffffff";
                }
                else if ($colortype==1)
                {
                     $bordercolorlight = "#FCF2CF";
                     $bordercolordark = "#D0C68F";
                     $bgcolortag="bgcolor=#FFE99E";
                }

                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 $bgcolortag class='13-black' bordercolordark=$bordercolordark bordercolorlight=$bordercolorlight>\n";
                $x .= "<tr bgcolor=#E3DB9C><td colspan=2 align=center class='13-black-bold'>".intranet_wordwrap($this->Title,25,"\n",1)."</td></tr>\n";
                $x .= "<tr><td width=20% align=right >$i_AnnouncementDate:</td><td >".$this->AnnouncementDate."</td></tr>\n";
                $x .= "<tr><td align=right>$i_AnnouncementOwner:</td><td >".intranet_wordwrap($displayName,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right >$i_AnnouncementTargetGroup:</td><td>".intranet_wordwrap($target,30,"\n",1)."</td></tr>\n";
                $x .= "<tr><td align=right >$i_AnnouncementDescription:</td><td>".nl2br($this->convertAllLinks2($this->Description,30))."</td></tr>\n";
                if ($this->Attachment != "")     # Display images
                {
                    $hasImage = false;
                    $displayImage .= "<tr><td align=left colspan=2>";
//                    $x .= "<tr><td class=event_content>$i_AnnouncementAttachment</td><td>\n";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);

                        /*
                        $displayImage .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayImage .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayImage .= " (".ceil($files[$key][1]/1000)."Kb)";
                        */
                        $size = GetImageSize($target_filepath);
                        list($width, $height, $type, $attr) = $size;
                        /*if ($width > 412 || $height > 550)
                        {
                            $image_html_tag = "width=412 height=550";
                        }
                        else
                        {
                            $image_html_tag = "";
                        }*/
                        # modified by Kelvin Ho 2008-11-10
                        if ($width > 412 || $height > 550)
                        {
	                        if($height>$width)
                        		$image_html_tag = " height=550";
                        	elseif($height<$width)
                        		$image_html_tag = "width=412 ";
                        }else
                        {
                            $image_html_tag = "";
                        }


                        $displayImage .= "<a href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\"><img border=0 src=\"$intranet_httppath$url\" $image_html_tag></a>";
                        $displayImage .= "<br>\n";
                        $hasImage = true;
                           }
                    }
                    $displayImage .= "</td></tr>\n";
                    if ($hasImage)
                    {
                        $x .= $displayImage;
                    }

                }
                if ($this->Attachment != "")
                {
                    $hasOther = false;
                    $displayOther .= "<tr><td align=left colspan=2>";
//                    $x .= "<tr><td class=event_content>$i_AnnouncementAttachment</td><td>\n";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (!isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);

                        $displayOther .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                        //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayOther .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                        $displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
                        $displayOther .= "<br>\n";
                        $hasOther = true;
                        }
                    }
                    $displayOther .= "</td></tr>";
                    if ($hasOther)
                    {
                        $x .= $displayOther;
                    }

                }
                $x .= "</table>\n";
                return $x;
        }
        
        function hasRightToView($UID=null)
        {
                 global $UserID;
                 if ($this->isSchoolAnnouncement()) return true;
                 $targetGroups = $this->returnTargetGroupsID();

				$user_id = ($UID!=NULL && $UID>0) ? $UID : $UserID;
				
                 $sql = "SELECT DISTINCT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$user_id'";
                 $groups = $this->returnVector($sql);

                 for ($i=0; $i<sizeof($groups); $i++)
                 {
                      $target_gID = $groups[$i];
                      if (in_array($target_gID,$targetGroups))
                      {
                          return true;
                      }
                 }
                 return false;

        }

        function GET_ADMIN_USER()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/announcement/admin_user.txt"));
		
		return $AdminUser;
	}
        
        function hasAccessRight($UserID)
	{
		$AdminUser = $this->GET_ADMIN_USER();
                return (in_array($UserID, explode(",",$AdminUser)));

	}
	
	function returnAnnouncementAdminUsers()
	{
		$namefield=getNameFieldByLang("d.");
		$sql = "	SELECT
						DISTINCT d.UserID, $namefield
					FROM 
						ROLE_RIGHT a 
						INNER JOIN ROLE b ON a.RoleID = b.RoleID
						INNER JOIN ROLE_MEMBER c ON c.RoleID = b.RoleID
						INNER JOIN INTRANET_USER d ON c.UserID = d.UserID
					WHERE
						a.FunctionName = 'other-schoolNews' 
		";
		
		return $this->returnArray($sql); 
		
		
	}
	
	function returnGroupAnnouncement_unread($GroupID){
                global $UserID;
                
                $GroupID = IntegerSafe($GroupID);
                
                $sql = "
					SELECT 
						count(*)
					FROM 
						INTRANET_ANNOUNCEMENT AS a
						left join INTRANET_GROUPANNOUNCEMENT AS b on (b.AnnouncementID = a.AnnouncementID)
					WHERE 
						b.GroupID = '$GroupID' AND 
						a.RecordStatus = '1' and 
						a.ReadFlag not like '%;". $UserID .";%'
					";
				$result = $this->returnVector($sql);
                return $result[0];
        }
        
     function returnUnreadGroupAnnouncementArr($GroupIDArr=array()){
                global $UserID;
                
                if(!empty($GroupIDArr))
                	$cond_GroupIDArr = " AND b.GroupID IN (".implode(',',IntegerSafe($GroupIDArr)).") ";
                
                $sql = "
					SELECT 
						b.GroupID,
						count(*) as unread
					FROM 
						INTRANET_ANNOUNCEMENT AS a
						left join INTRANET_GROUPANNOUNCEMENT AS b on (b.AnnouncementID = a.AnnouncementID)
					WHERE 
						a.RecordStatus = '1' and 
						a.ReadFlag not like '%;". $UserID .";%'
						$cond_GroupIDArr
					GROUP BY
						b.GroupID
					";
					
				$result = BuildMultiKeyAssoc($this->returnArray($sql),"GroupID","unread",1);
				
                return $result;
    }
    
    function returnSchoolAnnouncementByDateRange($StartDate, $EndDate)
    {
    	
    }

}
?>