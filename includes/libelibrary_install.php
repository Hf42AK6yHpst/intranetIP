<?php
// Modifing by :

## Change Log ##
/*
#
# 2020-10-19 [Cameron]
#               - modify check_if_readable(), apply IsShow to filter condition, disable Temporary setting in testing site [case #Y190573]
#
# 2020-10-15 [Cameron]
#               - add function get_show_or_hide_eBook_sql(), updateShowHideOfeBook(), checkBookIsShow() [case #Y190573]
#               - apply checkBookIsShow() in check_book_is_readable()
#
# 2020-10-14 [Cameron]
#               - modify retrieve_quotation_info(), change link page according to status [case #Y190573]
#
# 2019-05-13 [Paul]
#				- Add quote to query having variable not quoted
# 2017-12-01 [Pun]
#				- modify check_book_is_readable(), Added $special_feature['eBookAuthorAccountLimit']
#
# 2016-10-27 [Paul]
#				- modify retrieve_quotation_info(), retrieve_quotation_info_importside(), fix the problem of CentOS 7 server returns a different value when calling strtotime towards 0000-00-00 00:00:00
#
# 2016-01-21 [Paul]
#				- customise function check_if_readable(), pass true if it set  $sys_custom['ebook']['disableRemovalEbook'] = true;
#
# 2015-12-15 [Paul]
#				- modify function check_if_readable(), pass true if 'isCompulsory'='1' when corresponding quotation is not expired
#
# 2015-10-16 [Cameron]
#				- add function check_if_ebook_exists()
#
# 2015-09-02 [Cameron]
#				- modify update_ebook_installation(), update INTRANET_ELIB_BOOK_INSTALLATION.IsEnable based on IsSlected flag
#				- add get_available_eBook_sql(), get_enabled_eBook_sql()
#
# 2015-08-31 [Cameron]
#				- Add function get_quotation_info(), update_ebook_selection(), get_compulsory_and_selected_book_count(), get_selected_eBook_sql()
#
# 2015-08-27 [Cameron]
#				- Add keyword search function to retrieve_particular_quotation_info()
#
# 2015-08-26 [Cameron]
#				- Add book cover image before title in retrieve_particular_quotation_info()
#				- Add function get_column_title_in_ebook_license(), get_column_titles_in_ebook_license() <-- void after 2015-09-02
#				- set column to be sortable in retrieve_eLib_license_ebooks_by_quotationID_html()
#				- add parameter for sorting function in retrieve_particular_quotation_info()
#
# 2013-07-05 [Charles Ma] - update20130705
# Detail	:	eBook Licence

*/

class elibrary_install extends libdb
{

	var $BookPublishers = array();
	var $BookAuthorIDs = array();

	function elibrary_install(){
		$this->libdb();
	}

	#######################################
	## GET FUNCTIONS
	function get_book_license_list($currUserID){
		global $eLib, $i_QB_LangSelectEnglish, $i_QB_LangSelectChinese;

		  $sql = "
			SELECT
				SUBSTRING((b.BookID+100000000), 2),
				b.Title,
				b.Author,
				b.Publisher,
				IF (b.Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."') ,
				IF (b.Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."'),
				b.DateModified,
				IF(tmp_bl.ct = '' or tmp_bl.ct IS NULL, '-', tmp_bl.ct) as assigned_quota,
				SUM( bl.NumberOfCopy) as quota,
				bl.BookLicenseID,
				b.BookID
			FROM
				INTRANET_ELIB_BOOK as b
			INNER JOIN
				INTRANET_ELIB_BOOK_LICENSE as bl
			ON
				b.BookID = bl.BookID
			LEFT JOIN
				INTRANET_ELIB_TEMP_BookLicense_{$currUserID} as tmp_bl
			ON
				bl.BookID = tmp_bl.BookID
			GROUP BY
			    b.BookID
			HAVING
				SUM( bl.NumberOfCopy) > 0
		  ";
		  return $sql;

	}

	//Obsolete : Not in use anymore
	/**
	 * Count the number of students assigned to each book (Assigned Quota)
	 */
	function get_student_license_count(){
		$aryCount = array();

		$sql = "SELECT BookLicenseID, count(*) as ct FROM INTRANET_ELIB_BOOK_STUDENT GROUP BY BookLicenseID";
		$aryRs = $this->returnArray($sql);

		if($aryRs == array()){
			return;
		}

		foreach($aryRs as $i => $info){
			$aryCount[$info['BookLicenseID']] = $info['ct'];
		}
		return $aryCount;
	}

	function create_tmp_student_license_count($currUserID){
		$aryCount = array();

		$sql = "CREATE TABLE INTRANET_ELIB_TEMP_BookLicense_{$currUserID}
				SELECT BookID, count(*) as ct FROM INTRANET_ELIB_BOOK_STUDENT GROUP BY BookID";
		$this->db_db_query($sql);
	}

	function drop_tmp_student_license_count($currUserID){
		$sql = "drop table IF EXISTS INTRANET_ELIB_TEMP_BookLicense_{$currUserID}";
		$this->db_db_query($sql);
	}

	function get_student_license_list($BookID){
		$sql = "SELECT * FROM INTRANET_ELIB_BOOK_STUDENT WHERE BookID = '".$BookID."' ORDER BY InputDate";
		return $this->returnArray($sql);
	}

	function get_studentID_license_list($BookID){
		$sql = "SELECT StudentID FROM INTRANET_ELIB_BOOK_STUDENT WHERE BookID = '".$BookID."' ORDER BY InputDate";
		return $this->returnVector($sql);
	}

	function get_student_with_name_license_list($BookID, $getSqlOnly="true"){
		$name = getNameFieldWithLoginByLang();

		$sql = "SELECT
					bs.InputDate,
					IF(u.ClassNumber IS NULL OR TRIM(u.ClassNumber)= '', '--', u.ClassNumber) as ClassNumber,
					IF(u.ClassName IS NULL OR TRIM(u.ClassName) = '', '--', u.ClassName) as ClassName,
					".$name.",
					IF (DATE_ADD(bs.InputDate, INTERVAL 2 DAY) > NOW(), CONCAT('<input type=\"checkbox\" name=\"BookStudentID[]\" value=\"',bs.BookStudentID,'\"'), '')
				FROM
					INTRANET_ELIB_BOOK_STUDENT as bs
				INNER JOIN
					INTRANET_USER as u
				ON
					bs.StudentID = u.UserID
				WHERE
					BookID = '".$BookID."'
				";

		return ($getSqlOnly)? $sql : $this->returnArray($sql);
	}

	function get_readable_book_info($curUserID){
		global $plugin, $UserType;

		if($plugin['eLib_license']==1){
			## Site license
			$sql = "SELECT
				   		be.BookID,
				   		b.Title
				   FROM
				   		INTRANET_ELIB_BOOK_ENABLED as be
				   INNER JOIN
				   	 	INTRANET_ELIB_BOOK b
				   ON
				   		be.BookID = b.BookID
				   ORDER BY
					   		b.Title
				   ";
		}else{
			## Student license
			if($UserType == 1){
				$sql = "SELECT
							bs.BookID,
							b.Title
						FROM
							INTRANET_ELIB_BOOK_LICENSE as bs
						INNER JOIN
							INTRANET_ELIB_BOOK as b
						ON
							bs.BookID = b.BookID
						GROUP BY
							bs.BookID
						HAVING
							SUM( bs.NumberOfCopy) > 0
						ORDER BY
					   		b.Title
					";
			}else{
				$sql = "SELECT
					   		bs.BookID,
					   		b.Title
					   FROM
					   		INTRANET_ELIB_BOOK_LICENSE as be
					   INNER JOIN
					   		INTRANET_ELIB_BOOK_STUDENT as bs
					   ON
					   		(be.BookID = bs.BookID AND
					   		bs.StudentID = '".$curUserID."')
					   INNER JOIN
					   	 	INTRANET_ELIB_BOOK as b
					   ON
					   		bs.BookID = b.BookID
					   GROUP BY
					   		bs.BookID
					    ORDER BY
					   		b.Title
					   ";
			}
		}

		return $this->returnArray($sql);
	}

	function get_year_class_user($YearClassID, $strUserID="", $notIn=true){

		if($notIn)
			$condition = (!empty($strUserID))? 'AND u.UserID NOT IN ('.$strUserID.')' : "";
		else
			$condition = (!empty($strUserID))? 'AND u.UserID IN ('.$strUserID.')' : "";

		$sql = 'select
						  ycu.UserID,
						  yc.ClassTitleEN,
						  yc.ClassTitleB5,
						  ycu.ClassNumber
						From
						  YEAR_CLASS_USER as ycu
						  inner join
						  INTRANET_USER as u
						  on ycu.YearClassID = \''.$YearClassID.'\' AND ycu.UserID = u.UserID '.$condition.'
						 inner join
						  YEAR_CLASS as yc
						  on ycu.YearClassID = yc.YearClassID ';

		return $this->returnArray($sql);
	}

	/**
	 * Get the maximum quota allowd for book
	 */
	function get_book_quota($BookID){

		$sql = "select
					SUM( bl.NumberOfCopy) as quota
				FROM
					INTRANET_ELIB_BOOK_LICENSE as bl
				WHERE
					bl.BookID = '".$BookID."'";
		$aryRs = $this->returnVector($sql);
		$quota = $aryRs[0];

		return $quota;
	}

	/**
	 * Get Available quota
	 */
	function get_available_quota($BookID){

		## Get Quota
		$quota = $this->get_book_quota($BookID);


		## Get number of students assigned to book
		$sql = "SELECT count(*) FROM INTRANET_ELIB_BOOK_STUDENT WHERE BookID = '".$BookID."' GROUP BY BookID";
		$aryRs2  = $this->returnVector($sql);
		$assigned = $aryRs2[0];

		$available = $quota - $assigned;

		$aryOriginal = $this->check_book_license_quota_is_original($BookID);
		foreach($aryOriginal as $i=> $orig_quota){
			if($orig_quota != "TRUE" && is_numeric($orig_quota)){
				$invalid_quota += $orig_quota;
			}
		}

		$available = ($invalid_quota > $available)? 0 : $available -$invalid_quota;


		return $available;
	}

	#############################################################
	## Checkings
	function check_book_enabled($BookID){		//2013-07-09 Charles Ma
		//$sql = "SELECT InputDate FROM INTRANET_ELIB_BOOK_ENABLED WHERE BookID = $BookID";
		//$rs  = $this->returnVector($sql);

		$new_method_checking = $this->check_if_readable($BookID);	//2013-07-10 Charles Ma
		//return $rs[0];
		return $new_method_checking;
	}

	function check_book_license_exists($BookID){
		$sql = "SELECT BookLicenseID FROM INTRANET_ELIB_BOOK_LICENSE WHERE BookID = '".$BookID."'";
		$rs  = $this->returnVector($sql);
		return $rs[0];
	}

	function check_book_student_exists($BookID, $StudentID){
		$sql = "SELECT BookStudentID FROM INTRANET_ELIB_BOOK_STUDENT WHERE BookID='".$BookID."' AND StudentID='".$StudentID."'";
		$rs = $this->returnVector($sql);

		if($rs[0]!="")
			return true;
		else
			return false;
	}


	function GetBookPublisher()
	{
		if (sizeof($this->BookPublishers)<=0)
		{
			$sql = "SELECT BookID, Publisher FROM INTRANET_ELIB_BOOK ORDER BY BookID";
			$rs  = $this->returnArray($sql);

			for ($i=0; $i<sizeof($rs); $i++)
			{
				$BookPublishers[$rs[$i]["BookID"]] = trim($rs[$i]["Publisher"]);
			}
			$this->BookPublishers = $BookPublishers;
		}

		return $this->BookPublishers;
	}

	function GetBookAuthorID()
	{
		if (sizeof($this->BookAuthorIDs)<=0)
		{
			$sql = "SELECT BookID, AuthorID FROM INTRANET_ELIB_BOOK ORDER BY BookID";
			$rs  = $this->returnArray($sql);

			for ($i=0; $i<sizeof($rs); $i++)
			{
				$BookPublishers[$rs[$i]["BookID"]] = trim($rs[$i]["AuthorID"]);
			}
			$this->BookAuthorIDs = $BookPublishers;
		}

		return $this->BookAuthorIDs;
	}



	function check_book_is_readable($BookID, $curUserID)
	{
		global $plugin, $UserType, $special_feature;

		$PublisherControl = $special_feature['eBookUserAccountLimit'][$curUserID];
		$AuthorControl = $special_feature['eBookAuthorAccountLimit'][$curUserID];

		if (is_array($PublisherControl))
		{
			$BookPublishers = $this->GetBookPublisher();

			# check if it is allowed publisher
			if ($BookPublishers[$BookID]=="" || !in_array($BookPublishers[$BookID], $PublisherControl))
			{
				//debug($BookPublishers[$BookID]." [false] ");
				return false;
			}
		}else if(is_array($AuthorControl)){
			$BookAuthorIds = $this->GetBookAuthorID();

			# check if it is allowed publisher
			if ($BookAuthorIds[$BookID]=="" || !in_array($BookAuthorIds[$BookID], $AuthorControl))
			{
				//debug($BookPublishers[$BookID]." [false] ");
				return false;
			}
		}

		if (!$this->checkBookIsShow($BookID)) {
		    return false;
        }

		if($plugin['eLib_license']==1)
		{
			## Site license
			if (is_array($plugin['eLib_book_for_teacher_only']) && sizeof($plugin['eLib_book_for_teacher_only'])>0 && in_array($BookID, $plugin['eLib_book_for_teacher_only']))
			{
				# if identity is teacher, fine
				return ($_SESSION["UserType"]==1);
			} else
			{
				$rs = $this->check_book_enabled($BookID);
				if($rs!="")
				{
					return true;
				}
			}
		} else
		{
		## Student license
			if($UserType ==1)
			{
				## User is Teacher - check quota is assigned to book
				$quota = $this->get_book_quota($BookID);
				return ($quota > 0);
				//return true;
			}
			$BookLicenseID = $this->check_book_license_exists($BookID);
			if($BookLicenseID != "")
			{
				$rs = $this->check_book_student_exists($BookID, $curUserID);
				if($rs!="")
					return true;
			}
		}
		return false;
	}

	/**
	 * check whether any quota has been compromised
	 */
	function check_book_license_quota_is_original($BookID){
		$aryRs_isValid 	= array();
		$quota 	= 0;

		$sql = "SELECT * FROM INTRANET_ELIB_BOOK_LICENSE WHERE BookID = '".$BookID."'";
		$aryBook = $this->returnArray($sql);

		foreach($aryBook as $i=>$data){
			$key = md5($data['BookID']."_".$data['NumberOfCopy']."_".$data['InputDate']);
			$aryRs_isValid[$data['BookLicenseID']] =($key == $data['CheckKey'])? "TRUE" : (($data['NumberOfCopy'] > 0)? $data['NumberOfCopy'] : "FALSE");

			$quota +=($key == $data['CheckKey'])? 0  : (($data['NumberOfCopy'] > 0)? $data['NumberOfCopy'] : 0);
		}

		return array($aryRs_isValid,$quota);
	}

	/**
	 * check wheather any Book license entry has been modified or not
	 */
	function check_book_quota_is_original($BookID){
		$aryBook = $this->check_book_license_quota_is_original($BookID);
		$isValid = $aryBook[0];
		$quota = $aryBook[1];

		if(in_array(false, $aryBook)){

			return array(false, "");
		}

		return array(true, $quota);
	}

	##############################################################
	## INSERT / DELETE

	function enable_site_book_license($BookID, $currUserID){

		if ($currUserID=="")
		{
			$currUserID = 0;
		}
		##Check book is already enabled
		if(trim($this->check_book_enabled($BookID))!=""){
			return 2;
		}

		## Enabled book
		$sql = "INSERT INTO INTRANET_ELIB_BOOK_ENABLED (BookID, UserID, InputDate) VALUES ('$BookID', '$currUserID', now())";

		return $this->db_db_query($sql);
	}

	function disable_site_book_license($BookID){
		$sql = "DELETE FROM INTRANET_ELIB_BOOK_ENABLED WHERE BookID = '$BookID'";
		return $this->db_db_query($sql);
	}

	function add_book_license_quota($BookID, $quota){

		if($BookID=="")
			return;

		$timestamp = date("Y-m-d H:i:s");
		$CheckKey = md5($BookID."_".$quota."_".$timestamp);
		$sql = "INSERT INTO
					INTRANET_ELIB_BOOK_LICENSE (BookID, NumberOfCopy, CheckKey, InputDate)
				VALUES
					('$BookID', '$quota', '$CheckKey', '$timestamp')";

		if($this->db_db_query($sql)){
			return $this->db_insert_id();
		}else{
			return;
		}
	}

	function add_book_student($BookLicenseID, $BookID, $StudentID){

		if($BookLicenseID=="" || $BookID == "" ||$StudentID=="" )
			return;

		## Check student book record exits already ?
		if($this->check_book_student_exists($BookID, $StudentID))
			return true;

		## Insert book student record
		$sql = "INSERT INTO
				INTRANET_ELIB_BOOK_STUDENT (BookID, BookLicenseID, StudentID, InputDate)
				VALUES('$BookID','$BookLicenseID', '$StudentID', NOW())";


		return $this->db_db_query($sql);

	}

	function delete_book_student($strBookStudentID){
		if($strBookStudentID=="")
			return;

		$sql = "DELETE FROM INTRANET_ELIB_BOOK_STUDENT WHERE BookStudentID IN (".$strBookStudentID.")";
		return $this->db_db_query($sql);
	}


	############################################################################################
	#######################     UI

	/**
	 * Given the list of success & failed updates/inserts , display the results.
	 *
	 * $extra_info_header : set this field to display extra information.
	 * $extra_info : must have $extra_info_header set to display extra info.
	 *
	 *  Called in files:
	 * install_quota_import_update.php
	 * install_status_import_update.php
	 * ajax.php
	 * admin_book_license_assign_update.php
	 */
	//function gen_result_ui($FirstHeading="", $arySucc, $aryFail, $reload_parent=true, $extra_info_header="", $extra_info="", $tb_width="628"){
	function gen_result_ui($arySucc, $aryFail, $reload_parent=true,$tb_width="628"){
		global $eLib;

		$rs = "";

		$rs .='<table width="'.$tb_width.'px" height="490px"><tr><td>';
		//$rs .='<table width="100%" height="490px"><tr><td>';
		$rs .='<div style="height:490px;background:white;">';

		########################################
		### Result UI
		$rs .='<div style="height:464px;overflow-y:auto;">';

		## Success list UI
		if(count($arySucc) > 0){
			$rs .= $this->get_ary_result_ui("Success", $arySucc);
		}

		## Fail list UI
		if(count($aryFail) > 0){
			$rs .= $this->get_ary_result_ui("Fail",$aryFail);
		}

		$rs .= '</div>';

		########################################
		## Button panel UI
		$rs .= '<div id="btn_panel" style="height:40px;background:#EEE;text-align:center;">';
		$rs .= '<table width="100%" height="100%"><tr><td align="center" valign="center">';

		## Reload parent page + close thickbox OR Close thickbox only
		$close_js = ($reload_parent)? 'window.parent.location.reload(); window.parent.tb_remove();' : 'window.parent.tb_remove();';

		$rs .= '<input type="button" name="close_btn" id="close_btn" value="'.$eLib["html"]["Close"].'" onClick="'.$close_js.'" />';
		$rs .= '</td></tr><table>';
		$rs .= '</div>';
		########################################

		$rs .= '</div>';
		$rs .= '</td></tr></table>';
		return $rs;
	}

	/**
	 *
	 */
	//function get_ary_result_ui($type, $FirstHeading, $aryRs, $extra_info_header, $extra_info){
	function get_ary_result_ui($type, $aryRs){
		global $eLib;

		$bgColor = "#EEE";
		$rs = "";
		$curBg = "";

		$header_color = ($type=="Success")? "green" : "red";
		$status = ($type=="Success")? $eLib["html"]["Success"] : $eLib["html"]["Fail"];

		## Heading
		//$rs .= '<BR />';
		$rs .= '<div style="height:45px;">';
		$rs .= '<table width="100%">';
		$rs .= '<tr style="font-weight:bold;"><td align="right">';
		$rs .= '<div class="systemmsg" style="width:100px;text-align:center;color:'.$header_color.';">'.$status.'<div>';
		$rs .= '</td></tr>';
		$rs .= '<tr><td align="right">'.$eLib["html"]['Total'].': '.count($aryRs).'</td></tr></table>';
		$rs .= '</div>';

		## Main
		$rs .= '<div id="display_content" style="">';
		$rs .= '<table width="100%" cellpadding="2" cellspacing="0">';


		$rs .= '<tr style="font-weight:bold;height:30px;background:#CCCCCC;">';
		$rs .= '<td width="5%">#</td>';

		$aryHeading = array_keys($aryRs[0]);
		$wdth = 70/count($aryHeading);
		foreach($aryHeading as $e=> $info){
			$rs .= '<td width="'.$wdth.'%">'.$info.'</td>';
		}
		$rs .= '</tr>';

		################################
		## List success
		foreach($aryRs as $i => $data){
			$index = $i+1;
			/*$rs .= '<tr style="background:'.$curBg.';"><td>'.$index.'</td><td>'.$default_data.'</td>';

			## Display extra info , if any
			if(!empty($extra_info_header) && is_array($extra_info_header) && $extra_info_header != array()){
				foreach($extra_info_header as $e=> $info){
					$rs .= '<td>'.$extra_info[$e][$i].'</td>';
				}
			}*/
			$rs .= '<tr style="background:'.$curBg.';">';
			$rs .= '<td>'.$index.'</td>';

			foreach($data as $j=>$info){
				$rs .= '<td>'.$info.'</td>';
			}

			$rs .= '</tr>';
			$curBg = $curBg==""? $bgColor : "";
		}
		$rs .= '</table>';
		$rs .= '</div>';

		return $rs;
	}

	function gen_year_class_user_ui($select_name="StudentID", $YearClassID,$strUserID=""){
		$hidden_list = "";
		$UserList = $this->get_year_class_user($YearClassID, $strUserID, true);
		$x .= '<select name="'.$select_name.'" id="'.$select_name.'" size="18" style="height:height:250px;width:280px" multiple="true">';
		for ($i=0; $i< sizeof($UserList); $i++) {
			$TempUser = new libuser($UserList[$i]['UserID']);
			$tmpUsername = $TempUser->UserNameLang().' ('.Get_Lang_Selection($UserList[$i]['ClassTitleB5'],$UserList[$i]['ClassTitleEN']).'-'.$UserList[$i]['ClassNumber'].')';

			$x .= '<option value="'.$UserList[$i]['UserID'].'">';
			$x .= $tmpUsername;
			$x .= '</option>';
			$hidden_list .= '<input type="hidden" id="hidden_user_list_'.$UserList[$i]['UserID'].'" value="'.$tmpUsername.'" />';
			unset($TempUser);
		}
		$x .= '</select>';
		$x .= $hidden_list;
		return $x;
	}


	function get_book_reader_link($BookID, $BookFormat="", $IsTLF="", $user_id=0, $click_on="COVER")
	{
		# where $click_on = "COVER" or "TITLE"
		if ($click_on=="COVER")
		{
			return $this->get_book_reader($BookID, $BookFormat, $IsTLF, $user_id);
		} else
		{
			# i.e. "TITLE"
			return "MM_openBrWindowFull('/home/eLearning/elibrary/book_detail.php?BookID={$BookID}','bookdetail','scrollbars=yes')";
		}
	}


	function get_book_reader($BookID, $BookFormat, $IsTLF, $user_id)
	{
		global $eLib;

		if($this->check_book_is_readable($BookID, $user_id))
		{
			$has_book_access = true;
			if ($BookFormat==BOOK_FORMAT_EPUB2)
			{
				$read_link = "newWindow('/home/eLearning/ebook_reader/".$BookID."/reader/', 31)";
			} elseif ($IsTLF)
				$read_link = "newWindow('/home/eLearning/elibrary_dev/tool/index.php?BookID=".$BookID."', 31)";
			else
				$read_link = "newWindow('/home/eLearning/elibrary/tool/index.php?BookID=".$BookID."', 31)";
		} else
		{
			$read_link = "alert('".$eLib['SystemMsg']['Err_NoReadRight']."')";
			$has_book_access = false;
		}

		return $read_link;
	}

	function get_book_action_title($click_on="COVER")
	{
		global $eLib;

		if ($click_on=="COVER")
			return $eLib["action"]['open_book'];
		else
			return $eLib["action"]['view_details'];
	}

	/////////////////////////////////////////
	/////////////////update20130705 Charles Ma
	/////////////////////////////////////////

	////////////////update20130819 Charles Ma
	//////////////// 20131127-eBook-licenseimprove
	function retrive_license_enable(){
		if(isset($_SESSION['ebook_license_enable'])){
		}else{
			$sql = " SELECT QuotationID FROM INTRANET_ELIB_BOOK_QUOTATION ";
			$sql2 = " SELECT BookID FROM INTRANET_ELIB_BOOK WHERE Publish = 1 ";
			$_SESSION['ebook_license_enable'] = sizeof($this->returnArray($sql)) > 0 || sizeof($this->returnArray($sql2)) > 0  ? true: false;
		}
		return $_SESSION['ebook_license_enable'];
	}
	////////////////update20130819 END

	function retrive_if_quotation_exist($QuotationNumber){
		$sql = " SELECT * FROM INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationNumber = '$QuotationNumber' ";
		return sizeof($this->returnVector($sql));
	}

	function insert_new_quotation($data,$QuotationNumber,$InstallationType, $PeriodFrom, $PeriodTo, $QuotationBookCount, $InputDate){

		if($InstallationType == 2)
			$IsActivated = 'NULL';
		else $IsActivated = "'1'";


		$sql = " INSERT INTO INTRANET_ELIB_BOOK_QUOTATION " .
				" (QuotationNumber, PeriodFrom, PeriodTo, InputDate, Type,QuotationBookCount, IsActivated) " .
				" VALUES ('$QuotationNumber', '$PeriodFrom', '$PeriodTo', NOW(), '$InstallationType', '$QuotationBookCount', $IsActivated) ";
		$success = $this->db_db_query($sql);
		$QuotationID = mysql_insert_id();
		$current_ebook_sql = "";
		$BookID_arr = array();
		if($success){
			for($i=0; $i<sizeof($data); $i++){
				if($InstallationType == 2){
					list($BookID,$IsCompulsory) = explode(",",$data[$i][0]);
					if($IsCompulsory == 1 || $IsCompulsory == "Y") $IsCompulsory = 1;
					else $IsCompulsory = "";
				}else{
					list($BookID,$IsEnable) = explode(",",$data[$i][0]);
					if($IsEnable == 1 || $IsEnable == "Y") $IsEnable = 1;
					else $IsEnable = "";
				}

				if(in_array($BookID,$BookID_arr)){
					continue;
				}else{
					array_push($BookID_arr,$BookID);
				}

				if($current_ebook_sql == "")
				$current_ebook_sql .= " ('$BookID','$IsCompulsory', '$IsEnable','$QuotationID',NOW()) ";
				else $current_ebook_sql .= ", ('$BookID','$IsCompulsory', '$IsEnable','$QuotationID',NOW()) ";
			}

			$sql = " INSERT INTO INTRANET_ELIB_BOOK_INSTALLATION " .
					" (BookID,IsCompulsory,IsEnable,QuotationID,InputDate) " .
					" VALUES ".$current_ebook_sql ;
			$success = $this->db_db_query($sql);
			$this->set_publish();
		}
		return $success;
	}

	function install_new_quotation($filepath, $QuotationNumber,$InstallationType, $PeriodFrom, $PeriodTo, $QuotationBookCount, $InputDate){
		include_once('libimporttext.php');
		$limport 	= new libimporttext();
		$data = $limport->GET_IMPORT_TXT($filepath);

		$errors = array("Quotation already exist - Quotation Number : ", "Quotation insertion failed : Query failed - Quotation Number :", "Quotation insertion failed : Available Book Count > Total Book Number  - Quotation Number :", "Upload file error - Quotation Number :");
		$error_arr = array(false, "");

		if(sizeof($data)>0){
			list($BookID,$IsCompulsory) = explode(",",$data[0][0]);
			for($i=0; $i<sizeof($data); $i++){
				list($BookID,$IsCompulsory) = explode(",",$data[$i][0]);
				$error_arr = (int)($BookID) <= 0 ? array(true, $errors[3].$QuotationNumber) : array(false, "");
				if((int)($BookID) <= 0)break;
			}
			if($error_arr[0]){
				return $error_arr[1];
			}
		}else{
			$error_arr = array(true, $errors[3].$QuotationNumber);
			return $error_arr[1];
		}

		$isQuotationExist = $this->retrive_if_quotation_exist($QuotationNumber);
		if($isQuotationExist){
			$error_arr = array(true, $errors[0].$QuotationNumber);
		}else{
			if($InstallationType == 2 && sizeof($data) < $QuotationBookCount){
				$error_arr = array(true, $errors[2].$QuotationNumber);
				return $error_arr[1];
			}
			$success = $this->insert_new_quotation($data,$QuotationNumber,$InstallationType, $PeriodFrom, $PeriodTo, $QuotationBookCount, $InputDate);
			if($success){

			}else{
				$error_arr = array(true, $errors[1]);
			}
		}

		if($error_arr[0]){
			return $error_arr[1];
		}else{
			return "Installation Success - Quotation Number: $QuotationNumber.";
		}
	}

	function uninstall_new_quotation($QuotationID){

		$sql = " Select QuotationID FROM INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationID = '$QuotationID' ";
		$result = current($this->returnVector($sql));

		if($result !=""){
					$sql = " DELETE FROM INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationID = '$QuotationID' ";
					$this->db_db_query($sql);
					$sql = " DELETE FROM INTRANET_ELIB_BOOK_INSTALLATION WHERE QuotationID = '$QuotationID' ";
					$this->db_db_query($sql);
					$this->set_publish();
		}

	}

//	function update_ebook_installation($QuotationID, $BookID_arr){
//		$sql = " SELECT QuotationBookCount FROM INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationID = $QuotationID ";
//		$QuotationBookCount = current($this->returnVector($sql));
//
//		if(count($BookID_arr) == $QuotationBookCount){
//			$sql = " UPDATE INTRANET_ELIB_BOOK_INSTALLATION " .
//				" SET IsEnable = '1' WHERE BookID IN ( ".implode(",", $BookID_arr)." ) AND QuotationID = $QuotationID ";
//			$this->db_db_query($sql);
//			$sql =  " UPDATE INTRANET_ELIB_BOOK_QUOTATION " .
//					" SET IsActivated = '1' WHERE QuotationID = $QuotationID ";
//			$this->db_db_query($sql);
//
//			$this->set_publish();
//		}
//	}

	function update_ebook_installation($QuotationID){
		if ($QuotationID > 0) {
			$sql = " SELECT QuotationBookCount FROM INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationID = '{$QuotationID}'";
			$QuotationBookCount = current($this->returnVector($sql));
			$check_count = $this->get_compulsory_and_selected_book_count($QuotationID);
			if($check_count == $QuotationBookCount){
				$sql = " UPDATE INTRANET_ELIB_BOOK_INSTALLATION " .
					" SET IsEnable = '1' WHERE QuotationID = '{$QuotationID}' AND IsSelected=1";
				$this->db_db_query($sql);
				$sql =  " UPDATE INTRANET_ELIB_BOOK_QUOTATION " .
						" SET IsActivated = '1' WHERE QuotationID = '{$QuotationID}' ";
				$this->db_db_query($sql);

				$this->set_publish();
			}
		}
	}

	function retrieve_compulsory_book_count_quotation($QuotationID){
		$sql = " Select COUNT(IsCompulsory) " .
				" From INTRANET_ELIB_BOOK_QUOTATION AS q  " .
				" INNER JOIN INTRANET_ELIB_BOOK_INSTALLATION AS i  " .
				" ON q.QuotationID = i.QuotationID WHERE q.QuotationID = '$QuotationID' AND IsCompulsory = '1' GROUP BY q.QuotationID ";
		$cbook_count = current($this->returnVector($sql)) ? current($this->returnVector($sql)) : 0 ;
		return $cbook_count;
	}

	function retrieve_book_count_quotation($QuotationID){
		$sql = " Select QuotationBookCount
				  From INTRANET_ELIB_BOOK_QUOTATION AS q WHERE q.QuotationID = '$QuotationID' ";
		$book_count = current($this->returnVector($sql));

		return $book_count;
	}

	function retrieve_total_book_count_quotation($QuotationID){
		$sql = " Select COUNT(i.BookID) AS BookCount
				  From INTRANET_ELIB_BOOK_QUOTATION AS q
				 INNER JOIN INTRANET_ELIB_BOOK_INSTALLATION AS i
				 ON q.QuotationID = i.QuotationID WHERE q.QuotationID = '$QuotationID' GROUP BY q.QuotationID  ";
		$book_count = current($this->returnVector($sql));

		return $book_count;
	}

	function retrieve_quotation_info_importside(){
		$sql = " Select q.QuotationID, q.QuotationNumber, q.PeriodFrom, q.PeriodTo, q.InputDate, q.Type, q.IsActivated, COUNT(i.BookID) AS BookCount, q.QuotationBookCount
				  From INTRANET_ELIB_BOOK_QUOTATION AS q
				 INNER JOIN INTRANET_ELIB_BOOK_INSTALLATION AS i
				 ON q.QuotationID = i.QuotationID GROUP BY q.QuotationID ";

		$result = $this->returnArray($sql);
		if(sizeof($result) <=0 ){
			$html .= '<tr>
                      <td style="width: 600px;" colspan="5">No quotation found in this moment.</td></tr>';
		}
		for ($i=0; $i<sizeof($result); $i++){
			$PeriodFrom =  (date($result[$i]["PeriodFrom"]))== "0000-00-00 00:00:00" ? "--":date("Y-m-d",strtotime($result[$i]["PeriodFrom"]));
			$PeriodTo =  (date($result[$i]["PeriodTo"]))== "0000-00-00 00:00:00" ? "--":date("Y-m-d",strtotime($result[$i]["PeriodTo"]));
			$IsActivated = $result[$i]["IsActivated"];
			if($result[$i]["Type"] == 2){
				$BookCount = $result[$i]["QuotationBookCount"];
			}else{
				$BookCount = $result[$i]["BookCount"];
			}

			if($IsActivated){
				$quotation_color = "style='color: red'";
			}else{
				$quotation_color = "";
			}
			if($i%2){
				$background_color = "style='background-color:#dfe3ea'";
			}else $background_color = "";
			$html .= '<tr '.$background_color.'>
                      <td '.$quotation_color.'>'.$result[$i]["QuotationNumber"].'</td>
                      <td>'.$BookCount.'</td>
                      <td>'.$PeriodFrom.'</td>
                      <td>'.$PeriodTo.'</td>
                      <td>'.date("Y-m-d",strtotime($result[$i]["InputDate"])).'</td>
                      <td style="color: #ff0058;"><a class="delete_btn" style="cursor: pointer;" value="'.$result[$i]["QuotationID"].'" >X</a></td></tr>';
		}
		return $html;
	}

	function retrieve_quotation_info(){
		global $Lang;
		$sql = " Select q.QuotationID, q.QuotationNumber, q.PeriodFrom, q.PeriodTo, q.InputDate, q.Type, q.IsActivated, COUNT(i.BookID) AS BookCount, q.QuotationBookCount
				  From INTRANET_ELIB_BOOK_QUOTATION AS q
				 INNER JOIN INTRANET_ELIB_BOOK_INSTALLATION AS i
				 ON q.QuotationID = i.QuotationID GROUP BY q.QuotationID ";

		$result = $this->returnArray($sql);
		if(sizeof($result)<=0){
			$html .= ' <td colspan="4">'.$Lang["libms"]["pre_label"]["no_match"].' </td>';
		}
		for ($i=0; $i<sizeof($result); $i++){
			$PeriodFrom =  date($result[$i]["PeriodFrom"]);
			$PeriodTo = date($result[$i]["PeriodTo"]);
			$currenttime = date('Y-m-d h:m:s');
			if($result[$i]["Type"] == 2 && $result[$i]["IsActivated"] == ""){
				$status_style = "style= 'color:red'";
				$status = " ".$Lang["ebook_license"]["pending_for_book_selection"]." ";
                $linkPage = 'elib_setting_license_ebooks.php';
				$edit_btn = '<label  for="Quotation'.$result[$i]["QuotationID"].'" class="table_row_tool"><a href="'.$linkPage.'?QuotationID='.$result[$i]["QuotationID"].'" class="edit_dim"></a></label>';
			}else{
				if($PeriodFrom == "0000-00-00 00:00:00" && $PeriodTo == "0000-00-00 00:00:00"){
					$status_style = "style= 'color:green'";
					$status = " ".$Lang["ebook_license"]["permanent_use"] ." ";
                    $linkPage = 'elib_setting_show_hide_ebooks.php';
				}
				else if($currenttime < $PeriodFrom){
					$from = date("Y-m-d",strtotime($PeriodFrom));
					$status_style = "style= 'color:green'";
					$status = " ".$Lang["ebook_license"]["activate_from"]." $from ".$Lang["ebook_license"]["activate_from_end"];
                    $linkPage = 'elib_setting_show_hide_ebooks.php';
				}else if( $currenttime > $PeriodFrom && $currenttime < $PeriodTo){
					$to = date("Y-m-d",strtotime($PeriodTo));
					$status_style = "style= 'color:green'";
					$status = " ".$Lang["ebook_license"]["activate_until"]." $to ".$Lang["ebook_license"]["activate_until_end"];
                    $linkPage = 'elib_setting_show_hide_ebooks.php';
				}else if($currenttime >= $PeriodTo){
					$status_style = "style= 'color:grey'";
					$status = " ".$Lang["ebook_license"]["expired"]." ";
                    $linkPage = 'elib_setting_license_ebooks.php';
				}
				$edit_btn = "";
			}

			if($result[$i]["Type"] == 2){
				$BookCount = $result[$i]["QuotationBookCount"];
			}else{
				$BookCount = $result[$i]["BookCount"];
			}

			$html .= '<tr>
                      <td>'.$result[$i]["QuotationNumber"].'</td>
                      <td><a style=" float: left;" id="Quotation'.$result[$i]["QuotationID"].'" href="'.$linkPage.'?QuotationID='.$result[$i]["QuotationID"].'">'.$BookCount.'</a>'.$edit_btn.'</td>
                      <td '.$status_style.'>'.$status.'</td>
                      <td>'.date("Y-m-d",strtotime($result[$i]["InputDate"])).'</td></tr>';
		}
		return $html;
	}

	function retrieve_elib_license_setting_html(){
		global $eLib, $Lang;

		$html = '<table class="common_table_list">
                              <thead>
                                <tr>
                                  <th>'.$Lang["ebook_license"]["quotation_number"].'</th>
                                  <th>'.$Lang["ebook_license"]["number_of_books"].'</th>
                                  <th>'.$Lang["ebook_license"]["status"].'</th>
                                  <th>'.$Lang["ebook_license"]["installation_date"].'</th>
                                </tr></thead><tbody>';
		$html .= $this->retrieve_quotation_info();
        $html .= '
                              </tbody>
                            </table>';

		return $html;
	}

	function check_if_readable($BookID){
		global $sys_custom;
		if($sys_custom['ebook']['disableRemovalEbook']){
			return true;
		}
		$sql= " Select i.BookID,i.IsCompulsory,i.IsEnable, q.IsActivated, q.PeriodFrom, q.PeriodTo, q.InputDate  From  INTRANET_ELIB_BOOK_INSTALLATION AS i
			INNER JOIN INTRANET_ELIB_BOOK_QUOTATION AS q ON i.QuotationID = q.QuotationID
			    WHERE 
			        i.BookID = '$BookID'
			        AND i.IsShow=1  
			        ORDER BY PeriodTo ";
		$result = $this->returnArray($sql);

		$LastExpiredPeriodTo = date("0000-00-00 00:00:00");
		$isReadable = "";
		$currenttime = date('Y-m-d h:m:s');
		for($i =0; $i < count($result); $i++){
			$PeriodFrom =  date($result[$i]["PeriodFrom"]);
			$PeriodTo =  date($result[$i]["PeriodTo"]);
			$IsActivated =  $result[$i]["IsActivated"];
			$IsEnable =  $result[$i]["IsEnable"];
			$IsCompulsory = $result[$i]["IsCompulsory"];
			$InputDate =  date($result[$i]["InputDate"]);
			if(($IsEnable || $IsCompulsory) && $IsActivated
			&& ($currenttime <= $PeriodTo && (($currenttime >= $PeriodFrom) || ($InputDate < $LastExpiredPeriodTo && strtotime($LastExpiredPeriodTo) - strtotime($PeriodFrom) >= -8035200)))
			|| ($PeriodFrom == "0000-00-00 00:00:00" && $PeriodTo == "0000-00-00 00:00:00")  ){
				$isReadable = "T";
				break;
			}
			$LastExpiredPeriodTo = $PeriodTo;
		}

		/////////// Temporary setting in testing site
//		if(count($result)<= 0){
//			$isReadable = "T";
//		}
		/////////// Temporary setting in testing site
		return $isReadable;
	}

	function retrieve_particular_quotation_info($QuotationID, $type, $IsActivated,$cur_sort_field=0,$cur_order=1,$keyword=''){
		global $eLib, $Lang, $PATH_WRT_ROOT;
		if($IsActivated){
			$enable_sql.=" AND i.IsEnable = 1 ";
		}
		if ($keyword != '') {
			$keyword_str = mysql_real_escape_string($keyword);
			$cond_sql = " AND (b.Title like '%$keyword_str%' OR b.Author like '%$keyword_str%' OR b.Publisher like '%$keyword_str%') ";
		}
		else {
			$cond_sql = "";
		}
		$sort = ($cur_order == 1) ? "ASC" : "DESC";
		$orderByField = $cur_sort_field + 1;

		$sql = " Select b.Title, b.Author, b.Publisher, b.Language,
 				i.QuotationID, q.QuotationNumber, i.BookID , i.IsCompulsory From INTRANET_ELIB_BOOK_INSTALLATION AS i
				INNER JOIN INTRANET_ELIB_BOOK_QUOTATION AS q ON q.QuotationID = i.QuotationID
				LEFT JOIN INTRANET_ELIB_BOOK AS b ON  i.BookID = b.BookID
				WHERE i.QuotationID = '$QuotationID' ".$enable_sql. $cond_sql .
			  " ORDER BY ". $orderByField. " " . $sort;

		$result = $this->returnArray($sql);
		$no_selected = 0;
		$no_available = 0;
		for ($i=0; $i<sizeof($result); $i++){
			$Title =  $result[$i]["Title"];
			$Author = $result[$i]["Author"];
			$Publisher = $result[$i]["Publisher"];
//			$Language = $result[$i]["Language"] == "chi" ? $Lang['General']['Chinese'] : $Lang['General']['English'] ;
			switch ($result[$i]["Language"]) {
				case "chi":
					$Language = $Lang['General']['Chinese'];
					break;
				case "eng":
					$Language = $Lang['General']['English'];
					break;
				default:
					$Language = '';
					break;
			}
			$BookID = $result[$i]["BookID"];

			$image = '/file/elibrary/content/'.$BookID.'/image/cover.jpg';
			$dispImage = file_exists($PATH_WRT_ROOT.$image) ? "<img style=\"height:100px; border:0;\" src=\"".$image."\"> " : "";

			if($type == 2 && $IsActivated == "")
				$checkbox_html = $result[$i]["IsCompulsory"] != "" ? "<td><input type=\"hidden\" name=\"BookID[]\" value=\"$BookID\"/>".$Lang["ebook_license"]["compulsory"]."</td>" :"<td><input type=\"checkbox\" name=\"AvailableBookID[]\" value=\"$BookID\"><input type=\"hidden\" name=\"\" value=\"$BookID\"/></td>";
			else $checkbox_html = '';
			$isReadable = $this->check_if_readable($BookID);

//			$class = $result[$i]["IsCompulsory"] != "" && $IsActivated == ""  ? "class='SelectedItem'" : "class='AvailableItem'" ;
//			$current_index = $i + 1;

			if ($result[$i]["IsCompulsory"] != "" && $IsActivated == "") {
				$class = "class='SelectedItem'";
				$no_selected++;
				$current_index = $no_selected;
			}
			else {
				$class = "class='AvailableItem'";
				$no_available++;
				$current_index = $no_available;
			}

			$html .= "<tr $class><td style='width: 10px;' class='row_no'>$current_index</td>
                      <td>".$dispImage . (($Title != '') ? $Title : '&nbsp;')."</td>
                      <td>".(($Author != '') ? $Author : '&nbsp;')."</td>
                      <td>".(($Publisher != '') ? $Publisher : '&nbsp;')."</td>
                      <td>$Language</td>$checkbox_html</tr>";
		}
		return $html;
	}

	function retrieve_eLib_license_ebooks_by_quotationID_html($QuotationID,$cur_sort_field=0,$cur_order=1,$list_type='available',$keyword=''){
		global $eLib, $Lang;

		$sql = " Select * From INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationID = '$QuotationID' ";
		$result = $this->returnArray($sql);
		$IsActivated = $result[0]["IsActivated"];
		$QuotationNumber = $result[0]["QuotationNumber"];
		$PeriodFrom =  date('Y-m-d',strtotime($result[0]["PeriodFrom"]));
		$PeriodTo =  date('Y-m-d',strtotime($result[0]["PeriodTo"]));
		$type = $result[0]["Type"];

		if($type == 2 && $IsActivated == ""){
//			$btn_html = '<input type="hidden" name="QuotationID" value="'.$QuotationID.'"></input>' .
//					'<input type="button" id="available_books" class="book_btn selected" value="'.$Lang["ebook_license"]["available_books"].'"></input>
//								<input type="button" id="selected_books"  class="book_btn" value="'.$Lang["ebook_license"]["selected_books"].'"></input>' .
//										'<input type="button" id="select" class="select_btn selected2" style=" float: right;"  value="'.$Lang["ebook_license"]["select"].'"></input>' .
//										'<input type="button" id="deselect"   class="select_btn" style=" float: right;"  value="'.$Lang["ebook_license"]["deselect"].'"></input>' .
//										'<input type="submit" id="finish" class="select_btn" style=" float: right;"  value="'.$Lang["ebook_license"]["finish"].'"></input>';
			$btn_html ='<input type="hidden" name="QuotationID" value="'.$QuotationID.'"></input>
						<input type="hidden" name="list_type" id="list_type" value="'.$list_type.'"></input>
						  <div class="shadetabs" style="float:left;">
							<ul>
							  <li class="book_btn_li current_li">
								<a><input type="button" id="available_books" class="book_btn selected" value="'.$Lang["ebook_license"]["available_books"].'"></input></a>
							  </li>
							  <li class="book_btn_li">
								<a><input type="button" id="selected_books"  class="book_btn" value="'.$Lang["ebook_license"]["selected_books"].'"></input></a>
							  </li>';
			$btn_html .= '  </ul>';
			$btn_html .= '</div>';
			$btn_html .= '<div style="float:right;">
							<input type="button" id="select" class="formbutton_v30  select_btn selected2" style=" float: right;"  value="'.$Lang["ebook_license"]["select"].'"></input>
							<input type="button" id="deselect"   class="formbutton_v30  select_btn" style=" float: right;"  value="'.$Lang["ebook_license"]["deselect"].'"></input>
							<input type="submit" id="finish" class="formbutton_v30  select_btn" style=" float: right;"  value="'.$Lang["ebook_license"]["finish"].'"></input>
							<div class="Conntent_search">
								<input autocomplete="off" name="keyword" id="keyword" type="text" value="'.$keyword.'" />
							</div>
						  </div>';

			$checkBox_a = '<th ><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'AvailableBookID[]\'):setChecked(0,this.form,\'AvailableBookID[]\')"></th>';
			$checkBox_s = ' <th ><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'SelectedBookID[]\'):setChecked(0,this.form,\'SelectedBookID[]\')"></th>	';
		}
		else {
			$btn_html = '<div style="float:right;">
							<div class="Conntent_search">
								<input autocomplete="off" name="keyword" id="keyword" type="text" value="'.$keyword.'" />
							</div>
						  </div>';
		}
		if($result[0]["PeriodFrom"] == "0000-00-00 00:00:00" && $result[0]["PeriodTo"] == "0000-00-00 00:00:00"){
			$html .= "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : ".$Lang["ebook_license"]["permanent_use"].")<br/><br/>";
		}else{
			$html .= "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : $PeriodFrom - $PeriodTo)<br/><br/>";
		}

		$html .= '<div class="content_top_tool">
							<div class="ebook_license">
								'.$btn_html.'
							</div>
						</div>

                          <div class="table_board">';

		$html .= '<table class="common_table_list">
                              <thead>
                                <tr class="AvailableItem">'.
									$this->get_column_titles_in_ebook_license($cur_sort_field, $cur_order).
                                 	$checkBox_a.'
                                </tr>
                                <tr class="SelectedItem">'.
                                	$this->get_column_titles_in_ebook_license($cur_sort_field, $cur_order).
                                 	$checkBox_s.'
                                </tr></thead><tbody>';

		$html .= $this->retrieve_particular_quotation_info($QuotationID, $type, $IsActivated, $cur_sort_field, $cur_order, $keyword);
        $html .= '
                              </tbody>
                            </table>';
        $html .= '</div>';

		return $html;
	}
	/////////////////////////////////////////
	/////////////////update20130705 Charles Ma END
	/////////////////////////////////////////

	/////////////////////////////////////////
	/////////////////update20130809 Charles Ma - emag
	/////////////////////////////////////////

	function retrive_emag_license_html(){
		$sql = " SELECT SettingID, SchoolCode, SchoolName, Package, StartDate, EndDate FROM ELIB_MAGAZINE_SETTING ";
		$result = $this->returnArray($sql);
		$html = "";
		for($i = 0; $i < count($result) ; $i++){
			$html .= "<tr><td>".$result[$i]['SchoolCode']."</td><td>".$result[$i]['SchoolName']."</td>" .
					"<td>".date("Y-m-d",strtotime($result[$i]["StartDate"]))."</td><td>".date("Y-m-d",strtotime($result[$i]["EndDate"]))."</td>" .
					"<td style=\"color: #ff0058;\"><a class=\"delete_btn\" style=\"cursor: pointer;\" value=\"".$result[$i]['SettingID']."\">X</a></td></tr>";
		}
		return $html;
	}

	function install_emag_license($SchoolCode, $SchoolName, $StartDate, $EndDate){
		////////////////////// check if schoolcode

		$sql = " SELECT SettingID FROM ELIB_MAGAZINE_SETTING WHERE SchoolCode = '$SchoolCode' ";
		$SettingID = current($this->returnVector($sql));

		if($SettingID == ""){
			$sql = " INSERT INTO ELIB_MAGAZINE_SETTING (SchoolCode, SchoolName, StartDate, EndDate) " .
					" Values ('$SchoolCode','$SchoolName','$StartDate','$EndDate')  ";
			$result = $this->db_db_query($sql);
			if($result) return " Query insertion completed. ";
			else " Query insertion failed ";
		}else{
			$sql = " UPDATE ELIB_MAGAZINE_SETTING Set StartDate = '$StartDate' , EndDate = '$EndDate' Where SchoolCode = '$SchoolCode'  ";
			$result = $this->db_db_query($sql);
			if($result) return " Query update completed. ";
			else " Query update failed ";
		}
		return " Unexpected END ";
	}

	function unstall_emag_license($QuotationID){
		if($QuotationID){
			$sql = " DELETE FROM ELIB_MAGAZINE_SETTING WHERE SettingID = '$QuotationID'  ";
			$result = $this->db_db_query($sql);
		}
	}

	function set_publish(){

		$sql = " UPDATE INTRANET_ELIB_BOOK Set Publish = '0' Where BookID NOT IN " .
				" (Select i.BookID AS BookID From INTRANET_ELIB_BOOK_QUOTATION AS q " .
				" INNER JOIN INTRANET_ELIB_BOOK_INSTALLATION as i ON q.QuotationID = i.QuotationID " .
				" WHERE ( i.IsCompulsory = 1 OR i.IsEnable = 1 ) AND IsActivated = 1 ) AND BookID < 10000000 ";
		$result = $this->db_db_query($sql);

		$sql = " UPDATE INTRANET_ELIB_BOOK Set Publish = '1' Where BookID IN " .
				" (Select i.BookID AS BookID From INTRANET_ELIB_BOOK_QUOTATION AS q " .
				" INNER JOIN INTRANET_ELIB_BOOK_INSTALLATION as i ON q.QuotationID = i.QuotationID " .
				" WHERE ( i.IsCompulsory = 1 OR i.IsEnable = 1 ) AND IsActivated = 1 ) AND BookID < 10000000 ";
		$result = $this->db_db_query($sql);

		$sql = " UPDATE INTRANET_ELIB_BOOK AS b SET b.HitRate = 0 WHERE 1 AND BookID < 10000000  ";
		$result = $this->db_db_query($sql);

		$sql = " UPDATE INTRANET_ELIB_BOOK AS b
				 INNER JOIN    ( Select BookID,  COUNT(DISTINCT HistoryID) AS HitRate_h  From INTRANET_ELIB_BOOK_HISTORY  GROUP BY BookID ) AS h ON b.BookID = h.BookID
				SET b.HitRate =  h.HitRate_h WHERE 1 AND BookID < 10000000  ";
		$result = $this->db_db_query($sql);

		// 20131209-elibplus-announcement
//		$sql = " DELETE FROM INTRANET_ELIB_BOOK_PAGE WHERE BookID = -1 ";
//		$result = $this->db_db_query($sql);

	}

	/////////////////////////////////////////
	/////////////////update20130809 END
	/////////////////////////////////////////


	/*
	 * 	@param:	$cur_sort_field	-- current sort by field
	 * 			$cur_order		-- current asc / desc order
	 * 			$field_index	-- column index
	 * 			$field_name		-- column field name to display
	 */
	function get_column_title_in_ebook_license($cur_sort_field, $cur_order, $field_index, $field_name){
	    global $image_path, $Lang, $LAYOUT_SKIN;
	    $x = "";
	    if($cur_sort_field==$field_index){
	    	if($cur_order==1) $x .= "<a href=\"javascript:sort_ebook_license_page(0,$field_index)\" onMouseOver=\"window.status='".$Lang["libms"]["report"]["sortby"]." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_a_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$Lang["libms"]["report"]["sortby"]." $field_name\">";
	        if($cur_order==0) $x .= "<a href=\"javascript:sort_ebook_license_page(1,$field_index)\" onMouseOver=\"window.status='".$Lang["libms"]["report"]["sortby"]." $field_name';MM_swapImage('sort_icon','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1);return true;\" onMouseOut=\"window.status='';MM_swapImgRestore();return true;\" title=\"".$Lang["libms"]["report"]["sortby"]." $field_name\">";
	    }else{
	        $x .= "<a href=\"javascript:sort_ebook_license_page($cur_order,$field_index)\" onMouseOver=\"window.status='".$Lang["libms"]["report"]["sortby"]." $field_name';return true;\" onMouseOut=\"window.status='';return true;\" title=\"".$Lang["libms"]["report"]["sortby"]." $field_name\">";
	    }
	    $x .= str_replace("_", " ", $field_name);
	    if($cur_sort_field==$field_index)
	    {
			//Change arrow display
            if($cur_order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
            if($cur_order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
	    }
	    $x .= "</a>";
	    return $x;
	}

	function get_column_titles_in_ebook_license($cur_sort_field, $cur_order) {
		global $Lang;
		$pos = 0;
		$x = '<th>#</th>
			  <th>'.$this->get_column_title_in_ebook_license($cur_sort_field, $cur_order, $pos++, $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']).'</th>
	          <th>'.$this->get_column_title_in_ebook_license($cur_sort_field, $cur_order, $pos++, $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']).'</th>
	          <th>'.$this->get_column_title_in_ebook_license($cur_sort_field, $cur_order, $pos++, $Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher']).'</th>
	          <th>'.$this->get_column_title_in_ebook_license($cur_sort_field, $cur_order, $pos++, $Lang["ebook_license"]["language"]).' </th>';
		return $x;
	}

	function get_available_eBook_sql($QuotationID, $keyword=''){
		global $Lang;
		if ($keyword != '') {
			$keyword_str = mysql_real_escape_string($keyword);
			$cond_sql = " AND (b.Title like '%$keyword_str%' OR b.Author like '%$keyword_str%' OR b.Publisher like '%$keyword_str%') ";
		}
		else {
			$cond_sql = "";
		}

		$sql = "Select 	CONCAT('<img style=\'height:100px; border:0; vertical-align:top;\' src=\'/file/elibrary/content/',i .`BookID`,'/image/cover.jpg\'> ',b.Title) as Title,
						b.Author, b.Publisher,
						CASE b.Language
							WHEN 'chi' THEN '".$Lang['General']['Chinese']."'
							WHEN 'eng' THEN '".$Lang['General']['English']."'
							ELSE ''
						END as Language,
						IF(q.Type=2 AND (q.IsActivated IS NULL or q.IsActivated=''),
							(
								IF(i.IsCompulsory='1','".$Lang["ebook_license"]["compulsory"]."',
									CONCAT('<input type=\'checkbox\' name=\'AvailableBookID[]\' id=\'AvailableBookID[]\' value=\'', i.`BookID` ,'\' />')
								)
							),'') as Selection, i.BookID, b.Title as TitleOnly
				From INTRANET_ELIB_BOOK_INSTALLATION AS i
				INNER JOIN INTRANET_ELIB_BOOK_QUOTATION AS q ON q.QuotationID = i.QuotationID
				LEFT JOIN INTRANET_ELIB_BOOK AS b ON  i.BookID = b.BookID
				WHERE i.QuotationID = '$QuotationID' AND i.IsCompulsory<>'1' AND i.IsSelected<>1 ".$cond_sql;
		return $sql;
	}

	function get_selected_eBook_sql($QuotationID, $keyword=''){
		global $Lang;
		if ($keyword != '') {
			$keyword_str = mysql_real_escape_string($keyword);
			$cond_sql = " AND (b.Title like '%$keyword_str%' OR b.Author like '%$keyword_str%' OR b.Publisher like '%$keyword_str%') ";
		}
		else {
			$cond_sql = "";
		}

		$sql = "Select 	CONCAT('<img style=\'height:100px; border:0; vertical-align:top;\' src=\'/file/elibrary/content/',i.`BookID`,'/image/cover.jpg\'> ',b.Title) as Title,
						b.Author, b.Publisher,
						CASE b.Language
							WHEN 'chi' THEN '".$Lang['General']['Chinese']."'
							WHEN 'eng' THEN '".$Lang['General']['English']."'
							ELSE ''
						END as Language,
						IF(q.Type=2 AND (q.IsActivated IS NULL or q.IsActivated=''),
							(
								IF(i.IsCompulsory='1','".$Lang["ebook_license"]["compulsory"]."',
									CONCAT('<input type=\'checkbox\' name=\'SelectedBookID[]\' id=\'SelectedBookID[]\' value=\'', i.`BookID` ,'\' />')
								)
							),'') as Selection, i.BookID, b.Title as TitleOnly
				From INTRANET_ELIB_BOOK_INSTALLATION AS i
				INNER JOIN INTRANET_ELIB_BOOK_QUOTATION AS q ON q.QuotationID = i.QuotationID
				LEFT JOIN INTRANET_ELIB_BOOK AS b ON  i.BookID = b.BookID
				WHERE i.QuotationID = '$QuotationID' AND (i.IsCompulsory='1' OR i.IsSelected=1) ".$cond_sql;
		return $sql;
	}

	function get_enabled_eBook_sql($QuotationID, $keyword=''){
		global $Lang;
		if ($keyword != '') {
			$keyword_str = mysql_real_escape_string($keyword);
			$cond_sql = " AND (b.Title like '%$keyword_str%' OR b.Author like '%$keyword_str%' OR b.Publisher like '%$keyword_str%') ";
		}
		else {
			$cond_sql = "";
		}

		$sql = "Select 	CONCAT('<img style=\'height:100px; border:0; vertical-align:top;\' src=\'/file/elibrary/content/',i.`BookID`,'/image/cover.jpg\'> ',b.Title) as Title,
						b.Author, b.Publisher,
						CASE b.Language
							WHEN 'chi' THEN '".$Lang['General']['Chinese']."'
							WHEN 'eng' THEN '".$Lang['General']['English']."'
							ELSE ''
						END as Language, i.BookID, b.Title as TitleOnly
				From INTRANET_ELIB_BOOK_INSTALLATION AS i
				INNER JOIN INTRANET_ELIB_BOOK_QUOTATION AS q ON q.QuotationID = i.QuotationID
				LEFT JOIN INTRANET_ELIB_BOOK AS b ON  i.BookID = b.BookID
				WHERE i.QuotationID = '$QuotationID' AND (i.IsCompulsory = '1' or i.IsEnable='1') ".$cond_sql;
		return $sql;
	}

	function get_show_or_hide_eBook_sql($QuotationID, $keyword='', $isShow=''){
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		if ($keyword != '') {
			$keyword_str = mysql_real_escape_string($keyword);
			$cond_sql = " AND (b.Title like '%$keyword_str%' OR b.Author like '%$keyword_str%' OR b.Publisher like '%$keyword_str%') ";
		}
		else {
			$cond_sql = "";
		}
		if ($isShow != '') {
		    $cond_sql .= " AND i.IsShow='".$isShow."' ";
        }

		$sql = "Select 	CONCAT('<img style=\'height:100px; border:0; vertical-align:top;\' src=\'/file/elibrary/content/',i.`BookID`,'/image/cover.jpg\'> ',b.Title) as Title,
						b.Author, b.Publisher,
						CASE b.Language
							WHEN 'chi' THEN '".$Lang['General']['Chinese']."'
							WHEN 'eng' THEN '".$Lang['General']['English']."'
							ELSE ''
						END as Language, 
						IF (i.IsShow = 1, '<img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_tick_green.gif\">','<img src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_norecord_bw.gif\">') as IsShow, 
						CONCAT('<input type=\'checkbox\' name=\'BookID[]\' id=\'BookID[]\' value=\'', i.`BookID` ,'\' />') as Selection, 
						i.BookID, b.Title as TitleOnly
				From INTRANET_ELIB_BOOK_INSTALLATION AS i
				INNER JOIN INTRANET_ELIB_BOOK_QUOTATION AS q ON q.QuotationID = i.QuotationID
				LEFT JOIN INTRANET_ELIB_BOOK AS b ON  i.BookID = b.BookID
				WHERE i.QuotationID = '$QuotationID' AND (i.IsCompulsory = '1' or i.IsEnable='1') ".$cond_sql;
		return $sql;
	}

	function get_quotation_info($QuotationID) {
		$sql = "SELECT * FROM INTRANET_ELIB_BOOK_QUOTATION WHERE QuotationID='".$QuotationID."'";
		$rs = $this->returnArray($sql);
		if (count($rs) > 0) {
			$rs = current($rs);
		}
		else {
			$rs = false;
		}
		return $rs;
	}

	function update_ebook_selection($QuotationID, $bookID, $task) {
		$bookID = is_array($bookID) ? $bookID : array($bookID);
		$bookIDStr = implode("','",$bookID);
		$isSelected = ($task=='select') ? '1' : '0';
		$sql = "UPDATE INTRANET_ELIB_BOOK_INSTALLATION " .
			" SET IsSelected = '".$isSelected."' WHERE QuotationID='".$QuotationID."' AND BookID IN ( '".$bookIDStr."' )";

		$ret = $this->db_db_query($sql);
		return $ret;
	}

	function get_compulsory_and_selected_book_count($QuotationID){
		$sql = "select
						COUNT(BookID) as NumOfBooks
				from
						INTRANET_ELIB_BOOK_QUOTATION q,
						INTRANET_ELIB_BOOK_INSTALLATION i
				where
						q.QuotationID = i.QuotationID
				and
						q.QuotationID = '".$QuotationID."'
				and
						(i.IsCompulsory = '1' or i.IsSelected=1)";
		$rs = $this->returnVector($sql);
		$ret = (count($rs) > 0) ? $rs[0] : 0;
		return $ret;
	}

	## check if at least one ebook exist, return true if exist and false otherwise
	function check_if_ebook_exists(){
		$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_INSTALLATION ORDER BY BookID DESC LIMIT 1";
		$rs  = $this->returnVector($sql);
		return count($rs) > 0 ? true : false;
	}

	function updateShowHideOfeBook($quotationID, $bookIDAry, $isShow)
    {
        $result = array();
        $this->Start_Trans();
        $sql = "UPDATE INTRANET_ELIB_BOOK_INSTALLATION i 
                INNER JOIN INTRANET_ELIB_BOOK_QUOTATION q ON q.QuotationID = i.QuotationID
                SET i.IsShow='".$isShow."' 
                WHERE i.BookID IN ('".implode("','",(array)$bookIDAry)."')
                AND q.QuotationId='".$quotationID."'";
        $result[] = $this->db_db_query($sql);

        $sql = "UPDATE INTRANET_ELIB_BOOK SET Publish='".$isShow."' WHERE BookID IN ('".implode("','",(array)$bookIDAry)."')";
        $result[] = $this->db_db_query($sql);

        if (!in_array(false,$result)) {
            $this->Commit_Trans();
            return true;
        }
        else {
            $this->RollBack_Trans();
            return false;
        }
    }

    function checkBookIsShow($bookID){
        global $sys_custom;
        if($sys_custom['ebook']['disableRemovalEbook']){
            return true;
        }
        $sql = "SELECT 
                    i.BookID 
                FROM 
                    INTRANET_ELIB_BOOK_INSTALLATION i 
                    INNER JOIN INTRANET_ELIB_BOOK_QUOTATION q ON i.QuotationID=q.QuotationID AND (q.PeriodTo > NOW() OR q.PeriodTo = '0000-00-00 00:00:00') AND q.IsActivated='1'
				WHERE 
				    (i.IsEnable='1' OR IsCompulsory='1') 
				    AND i.IsShow=1
				    AND i.BookID='".$bookID."'";
        $rs = $this->returnResultSet($sql);
        return count($rs) ? true : false;
    }

}

?>