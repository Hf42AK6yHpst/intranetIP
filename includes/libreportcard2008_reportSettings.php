<?php
// editing by 
 
/********************
 * 2014-11-06 Ivan [ip.2.5.5.12.1]: modified setDefaultSettings() to add default value for eAttendance tap card settings
 ********************/
if (!defined("LIBREPORTCARD2008_REPORTSETTINGS_DEFINED")) {
	define("LIBREPORTCARD2008_REPORTSETTINGS_DEFINED", true);
	
	class libreportcard_reportSettings extends libreportcard {
		private $settingsAssoAry;
		private $reportId;
		
		public function __construct($parReportId) {
			$this->libreportcard();
			$this->reportId = $parReportId;
		}
		
		private function getReportTableName() {
			return $this->DBName.'.RC_REPORT_SETTING';
		}
		
		public function saveSettings($parSettingsAssoAry) {
			if (count($parSettingsAssoAry)==0 || !is_array($parSettingsAssoAry)) return false;
			
			$RC_REPORT_SETTING = $this->getReportTableName();
			
			$this->Start_Trans();
			$successAry = array();
			
			$sql = "Select SettingsID, SettingName, SettingValue From $RC_REPORT_SETTING Where ReportID = '".$this->reportId."'";
			$curSettingsAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'SettingName');
			
			$insertAry = array();
			foreach ((array)$parSettingsAssoAry as $_settingsName => $_settingsValue) {
				if (isset($curSettingsAssoAry[$_settingsName])) {
					// update
					$sql = "Update $RC_REPORT_SETTING 
							Set SettingValue = '".$this->Get_Safe_Sql_Query($_settingsValue)."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' 
							Where SettingsID = '".$curSettingsAssoAry[$_settingsName]['SettingsID']."' And ReportID = '".$this->reportId."'";
					$successAry[$_settingsName] = $this->db_db_query($sql);
				}
				else {
					// insert
					$insertAry[] = " ('".$this->reportId."', '".$this->Get_Safe_Sql_Query($_settingsName)."', '".$this->Get_Safe_Sql_Query($_settingsValue)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
				}
			}
			
			if (count($insertAry) > 0) {
				$sql = "INSERT INTO $RC_REPORT_SETTING (ReportID, SettingName, SettingValue, DateInput, InputBy, DateModified, ModifiedBy)
						Values ".implode(', ', (array)$insertAry);
				$successAry['insert'] = $this->db_db_query($sql);
			}
			
			if(in_array(false, $successAry)) {
				$this->RollBack_Trans();
				return false;
			}
			else {
				$this->Commit_Trans();
				return true;
			}
		}
	 
		private function loadSettings($forceReload=false)
		{
			if ($forceReload || $this->settingsAssoAry == null) {
				$RC_REPORT_SETTING = $this->getReportTableName();
				
				$sql = "SELECT SettingName, SettingValue, DateModified FROM $RC_REPORT_SETTING Where ReportID = '".$this->reportId."'";
				$settingsAry = $this->returnResultSet($sql);
				$this->settingsAssoAry = BuildMultiKeyAssoc($settingsAry, "SettingName");
				$this->setDefaultSettings();
			}
		}
		
		private function setDefaultSettings(){
			
		}
		
		public function getSettingsValue($settingsName) {
			$this->loadSettings();
			return $this->settingsAssoAry[$settingsName]['SettingValue'];
		}
		
		public function getSettingsLastModifiedDate($settingsName) {
			$this->loadSettings();
			return $this->settingsAssoAry[$settingsName]['DateModified'];
		}
	}
}
?>