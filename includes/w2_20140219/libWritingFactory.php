<?
// using: 

class libWritingFactory {
	public function __construct() {
		
    }
   
    public static function createWriting($objSource, $writingId=null, $contentCode=null, $schemeCode=null) {
    	global $w2_cfg;
    	
    	switch ($objSource) {
    		case $w2_cfg["writingObjectSource"]["db"];
    			return new libWritingDb($writingId);
    			break;
    		case $w2_cfg["writingObjectSource"]["xml"];
    			return new libWritingXml($contentCode, $schemeCode);
    			break;
    	}
    }
}
?>