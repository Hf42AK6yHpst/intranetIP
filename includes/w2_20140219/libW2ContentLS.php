<?
// using: ivan
class libW2ContentLS extends libW2Content  {

	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setContentCode($w2_cfg["contentArr"]["ls"]["contentCode"]);
	}

	public function getConceptMapStepCode(){
		return 'c3';
	}
	public function getConceptMapTitle(){
		return 'W2 LS POWERCONCEPT';
	}
	
	public function getConceptMapInstruction(){
		return 'W2 LS POWERCONCEPT INSTRUCTION';
	}
	
	
	public function getUnitTitle() {
		return '個案';
	}
	
//	public function getUnitName() {
//		return '市區重建';
//	}
	
	public function getSchemeTitle() {
		return '題目';
	}
	
//	public function getSchemeName() {
//		return '中環嘉咸街重建項目';
//	}
	
	public function getWritingTaskStepCode() {
		return 'c5';
	}
}
?>