<?php
include_once('w2ShareContentConfig.inc.php');

$schoolPrefix=strtolower($w2_cfg["schoolCodeCurrent"]."_");
$filePath = '../../../home/eLearning/w2/content/'.$schoolPrefix.'w2_contentConfig.inc.php' ;
if( file_exists($filePath) )
{
	//Siuwan 20140115 Select from DB directly	
	$allInputContentAry = getContentListFromDB();
	$aryCnt = count($allInputContentAry);
	if($aryCnt>0){
		foreach($allInputContentAry as $_returnAry){
			$_ary = array();
			$_contentCode = $_returnAry['r_contentCode']; //eng
			$_schemeCode = $_returnAry['schemeNum'];
			$_schoolCode = $_returnAry['schoolCode']; //111111
			$_settingKey = $schoolPrefix.$_contentCode;
			$_settingSchemeKey =  $schoolPrefix.'scheme'.$_schemeCode;
			$_ary['type_super'] = $_returnAry['type_super'];
			$_ary['type'] = $_returnAry['type'];
			$_ary['name'] = $_returnAry['name'];
			$_ary['introduction'] = $_returnAry['introduction'];
			$_ary['dateModified'] = $_returnAry['dateModified'];
			$_ary['fullDateModified'] = $_returnAry['fullDateModified'];
			$_ary['grade'] = $_returnAry['grade'];
			$_ary['schoolCode'] = $_schoolCode;
			$w2_cfg_contentSetting[$_settingKey][$_settingSchemeKey] = $_ary;
		}		
	}

	
	//include_once($filePath);
	foreach( (array)$w2_cfg["contentCodeList"] as $subjCode)
	{
		$w2_cfg_contentSetting[$subjCode] = array_merge((array)$w2_cfg_contentSetting[$subjCode],(array)$w2_cfg_contentSetting[$schoolPrefix.$subjCode]);					
	}
}
function getContentListFromDB($grade=''){
		global $Lang,$intranet_db;
		$libdb = new libdb();
		
		$gradeCond = '';
		$sql = 'SELECT 
					topicName as name,
					topicIntro as introduction,
					level as type_super,
					category as type, 
					r_contentCode,
					grade,
					schemeNum,
					DATE_FORMAT(dateModified,"%Y-%m-%d") as fullDateModified,
					dateModified,
					schoolCode
				FROM
					'.$intranet_db.'.W2_CONTENT_DATA
				WHERE status = 1 
		';
		return $libdb->returnArray($sql);
					
	}	
/*
// don't delete , may be open for future use Fai 20131028
GLOBAL $w2_cfg;
foreach( (array)$w2_cfg["schoolCode"] as $schoolList )
{
		$schoolPrefix=strtolower($schoolList["shortDesc"]."_");
		$filePath = '../../../home/eLearning/w2/content/'.$schoolPrefix.'w2_contentConfig.inc.php' ;
		
		if( file_exists($filePath) )
		{
			include_once($filePath);
			foreach( (array)$w2_cfg["contentCodeList"] as $subjCode)
			{
					$w2_cfg_contentSetting[$subjCode] = array_merge((array)$w2_cfg_contentSetting[$subjCode],(array)$w2_cfg_contentSetting[$schoolPrefix.$subjCode]);					
			}
		}
}
// end don't delelete
*/
?>
<?php
//include_once('w2ShareContentConfig.inc.php');
//
//GLOBAL $w2_cfg;
//foreach( (array)$w2_cfg["schoolCode"] as $schoolList )
//{
//		$schoolPrefix=strtolower($schoolList["shortDesc"]."_");
//		$filePath = '../../../home/eLearning/w2/content/'.$schoolPrefix.'w2_contentConfig.inc.php' ;
//		
//		if( file_exists($filePath) )
//		{
//			include_once($filePath);
//			foreach( (array)$w2_cfg["contentCodeList"] as $subjCode)
//			{
//				foreach( (array)$w2_cfg_contentSetting[$schoolPrefix.$subjCode] as $key=>$schemeContent)
//				{
//					if(file_exists('../../../home/eLearning/w2/content/'.$subjCode.'/'.$key))
//					{
//						$temp =array($key=>$w2_cfg_contentSetting[$schoolPrefix.$subjCode][$key]);
//						$w2_cfg_contentSetting[$subjCode] = array_merge((array)$w2_cfg_contentSetting[$subjCode],(array)$temp);	
//					}
//				}
//				
//			}
//		}
//}
?>