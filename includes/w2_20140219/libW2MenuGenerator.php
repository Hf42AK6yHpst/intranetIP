<?php
/*
 * Editing by  
 * 
 * Modification Log: 
 * 2013-11-15 (Jason)
 * 		- edit getTemplateMenu() to fix the problem of not doing mysql escape on topicName
 */
$RelativePath = '../../../';
include_once ('w2ShareContentConfig.inc.php');

include_once ($RelativePath.'includes/libuser.php');
class W2TemplateMenuGenerator
{
	private $lang;
	private $schNameLang;
	function __construct($intranet_hardcode_lang)
	{
		$this->lang = $intranet_hardcode_lang;
		$this->schNameLang = ($this->lang =='en')?'engName':'chiName';
	}
	function getTemplateMenu($r_contentCode)
	{
		GLOBAL $w2_cfg, $Lang;
		$objDB = new libdb();
		$sql = "SELECT cid, topicName, category,
				DATE_FORMAT(dateInput, '%Y-%m-%d') as dateInput,
				DATE_FORMAT(dateModified, '%Y-%m-%d') as dateModified,
				DATE_FORMAT(dateGenerated, '%Y-%m-%d') as dateGenerated,
				(dateGenerated >= dateModified ) as LatestGenerated,
				createdBy, schoolCode, schemeNum, status, level FROM W2_CONTENT_DATA
				WHERE r_contentCode = '$r_contentCode'; ";
				
		$templateList = $objDB->returnResultSet($sql);
		foreach( (array)$templateList as $rs)
		{
			$objUser = new libuser($rs['createdBy']);
			if($rs['status']!=W2_DELETED)
			{
				
				$className='';
				$schoolCode = '';
				$schemeNum  = '';
				$generateHTML="<span>".$Lang['W2']['published']."</span>";
				
				if($rs['status']==W2_GENERATED || $rs['status']==W2_PUBLISHED)
				{
					$schoolCode = $rs['schoolCode'];
					$schemeNum  = $rs['schemeNum'];

					# Topic Name
					$topicNameHTML =$objDB->Get_Safe_Sql_Query("<a href='/home/eLearning/w2/index.php?mod=handin&task=viewHandIn&r_action=PREVIEW&r_contentCode=${r_contentCode}&r_schemeCode=${schoolCode}_scheme${schemeNum}'>");
					$topicNameHTML .=$objDB->Get_Safe_Sql_Query($rs['topicName']);
					$topicNameHTML .=$objDB->Get_Safe_Sql_Query("</a>");
					
					if($rs['LatestGenerated']==='0')
					{
						# Generate Button
						$dateModifiedHTML = $objDB->Get_Safe_Sql_Query($rs['dateGenerated']);
										
						$generateHTML = $objDB->Get_Safe_Sql_Query("<span class='table_row_tool_text'><a id='republish_${rs[cid]}' onmouseout=\"hideFloatLayer();\" onmouseover='displayFloatLayer(\"republish_${rs[cid]}\", \"layer_ex_brief\", \"$dateModifiedHTML\")' href='/home/eLearning/w2/index.php?mod=admin&task=generateEngScheme&r_contentCode=${r_contentCode}&cid=${rs[cid]}' class='tool_publish'><em>&nbsp;</em><span>");
						$generateHTML .= $objDB->Get_Safe_Sql_Query($Lang['W2']['publish']);
						$generateHTML .= $objDB->Get_Safe_Sql_Query("</span></a></span>");
		
		
//						$generateHTML .= $objDB->Get_Safe_Sql_Query("<div id='".$rs['cid']."' hidden>");
//						$generateHTML .= $objDB->Get_Safe_Sql_Query("<b>".$Lang['W2']['datePublished']."</b><br/>");
//						$generateHTML .= $objDB->Get_Safe_Sql_Query($rs['datePublished']."<br />");
//						$generateHTML .= $objDB->Get_Safe_Sql_Query("<span style='color:red;' >");
//							
//						$generateHTML .='Not latest scheme';
//						$generateHTML .= $objDB->Get_Safe_Sql_Query("</span>");
//						$generateHTML .= $objDB->Get_Safe_Sql_Query("</div>");
					}
					
	
				}
				else
				{
					$topicNameHTML =$objDB->Get_Safe_Sql_Query($rs['topicName']);
					$generateHTML = $objDB->Get_Safe_Sql_Query("<span class='table_row_tool_text'><a href='/home/eLearning/w2/index.php?mod=admin&task=generateEngScheme&r_contentCode=$r_contentCode&cid=$rs[cid]' class='tool_publish'><em>&nbsp;</em><span>");
					$generateHTML .= $objDB->Get_Safe_Sql_Query($Lang['W2']['publish']);				
					$generateHTML .= $objDB->Get_Safe_Sql_Query("</span></a></span>");
				}
	
				if($rs['status']==W2_PUBLISHED)
				{
					# Edit Button
					$editHTML =$objDB->Get_Safe_Sql_Query("Edit");
					# Delete Button
					$deleteHTML =$objDB->Get_Safe_Sql_Query("Delete");
				}
				else
				{
					# Edit Button
					$editHTML =$objDB->Get_Safe_Sql_Query("<span title='Edit' class='table_row_tool_text'><a class='tool_edit' href='/home/eLearning/w2/index.php?mod=admin&task=newTemplateWriting&r_contentCode=$r_contentCode&cid=$rs[cid]'><em>&nbsp;</em><span>Edit</span></a></span>");
					# Delete Button
					$deleteHTML =$objDB->Get_Safe_Sql_Query("<span class='table_row_tool'><a onclick = 'getConfirm(this.href);' href='/home/eLearning/w2/index.php?mod=admin&task=deleteTemplate&r_contentCode=$r_contentCode&cid=$rs[cid]' class='tool_delete_dim' title='Delete'>&nbsp;</a></span>");
					
				}
				//$upperSchoolCode = strToUpper($rs['schoolCode']);
				
				$parseData[] = array(
				'topicName' => $objDB->Get_Safe_Sql_Query($rs['topicName']),
				'topicName_link' => $topicNameHTML,
				'category' => $rs['category'],
				'dateInput' => $rs['dateInput'],
				'level' => $rs['level'],
				//'schoolCode' => $w2_cfg["schoolCode"][$upperSchoolCode][$this->schNameLang],
				'createdBy' => $objUser->UserName(),
				'dateModified' => $rs['dateModified'],
				'status' => $rs['status'],
				'delimitor' => '|',
				'generate' => $generateHTML,
				'edit' => $editHTML,
				'delete' => $deleteHTML,
				);
			}
		}
		return $parseData;
	}
	function getEngConfigDataList($r_contentCode)
	{
		GLOBAL $w2_cfg_contentSetting, $w2_cfg;
		$objDB = new libdb();
		foreach( (array)$w2_cfg_contentSetting[$r_contentCode] as $key=>$schemeList)
		{
			$upperSchoolCode = strToUpper($schemeList["schoolCode"]);

			if($upperSchoolCode != $w2_cfg["schoolCodeCurrent"])
			{
				# Topic Name
				$topicNameHTML =$objDB->Get_Safe_Sql_Query("<a href='/home/eLearning/w2/index.php?mod=handin&task=viewHandIn&r_action=PREVIEW&r_contentCode=${r_contentCode}&r_schemeCode=$key'>");
				$topicNameHTML .=$objDB->Get_Safe_Sql_Query($schemeList['name']);
				$topicNameHTML .=$objDB->Get_Safe_Sql_Query("</a>");
				
				$parseData[] = array(
				'topicName' => $objDB->Get_Safe_Sql_Query($schemeList['name']),
				'topicName_link' => $topicNameHTML,
				'category' => $objDB->Get_Safe_Sql_Query($schemeList['type']),
				'dateInput' => '---',
				'level' => $objDB->Get_Safe_Sql_Query($schemeList['type_super']),
				//'schoolCode' => ($upperSchoolCode=='')?'---':$w2_cfg["schoolCode"][$upperSchoolCode][$this->schNameLang],
				'createdBy' => '---',
				'dateModified' => $schemeList['dateModified']==''?'---':$schemeList['dateModified'],
				'status' => -1,
				'delimitor' => '',
				'generate' =>'',
				'edit' => '',			
				'delete' => ''			
				);
			}
		}
		return $parseData;
	}
}
?>