<?php
include_once ('w2ShareContentConfig.inc.php');
class W2PublishMenuGenerator
{
	function getPublishMenu($r_contentCode, $mainPublishMenu=true,$CIdList='')
	{
		
		GLOBAL $w2_cfg, $Lang;
		$objDB = new libdb();		
		$isSent = '';
		if($CIdList !='')
		{
			$condsCId = " And cid In ('".implode("','", (array)$CIdList)."') ";
		}

		//1. Get scheme information one by one
		$sqlInfo =
		"
			Select
				cid,
				topicName,
				category, 
				level,
				r_contentCode,
				schemeNum,
				schoolCode,
				DATE_FORMAT( dateModified, '%Y-%m-%d') as dateModified
			From
				W2_CONTENT_DATA
			Where
				status <>". W2_TEMP." And r_contentCode='${r_contentCode}' $condsCId
			Order By
				topicName
		";
		//2. Check if the shared record is present group by cid 
		foreach( (array)$w2_cfg["schoolCode"] as $schoolDetail)
		{
			//Present by Sch, group by cid			
			$shortDesc = strtolower($schoolDetail["shortDesc"]);
			$isSent.= "GROUP_CONCAT(targetSchoolCode) LIKE '%${shortDesc}%' as ${shortDesc}Sent,"."\n\r";	
		}
		$sqlSchemeSharedRecord=
		"
			Select
				${isSent}
				cid
			From 
				W2_SHARED_RECORD
			Where
				status = ".W2_SHARED_SUCCEED." And r_contentCode = '${r_contentCode}'
			Group By 
				cid
		";
		
		$sql =
		"
		Select
			*, wcd.cid
		From 
			( ${sqlInfo} ) wcd
		Left Join
			( ${sqlSchemeSharedRecord} ) wsr
		On
			wsr.cid = wcd.cid;
		";
		$publishList = $objDB->returnResultSet($sql);
		
		if($mainPublishMenu)
		{
			for( $i=0; $i < count($publishList); $i++)
			{
				$cid = $publishList[$i]["cid"];
				$topicNameHTML =$objDB->Get_Safe_Sql_Query("<a href='/home/eLearning/w2/index.php?mod=handin&task=viewHandIn&r_action=PREVIEW&r_contentCode=${r_contentCode}&r_schemeCode=".$publishList[$i]['schoolCode']."_scheme".$publishList[$i]['schemeNum']."'>");
				$topicNameHTML .=$objDB->Get_Safe_Sql_Query($publishList[$i]['topicName']);
				$topicNameHTML .=$objDB->Get_Safe_Sql_Query("</a>");
				
				$parseData[$i] = array(
				'topicName' => $publishList[$i]["topicName"],
				'topicName_link' => $topicNameHTML,
				'category' => $publishList[$i]["category"],
				'level' => $publishList[$i]["level"],
				);
				
//				$completeCount =0;	
				foreach( $w2_cfg["schoolCode"] as $schoolDetail)
				{

					$shortDesc = strtolower($schoolDetail["shortDesc"]);
					if ( $publishList[$i]["${shortDesc}Sent"] == true)
					{
						$parseData[$i]["${shortDesc}Sent"] ="OK";
//						$completeCount++;
					}
					else
					{
						$parseData[$i]["${shortDesc}Sent"] ="Not Yet";
					}
				}
				$parseData[$i]['dateModified'] =$publishList[$i]['dateModified']."\n\r";
				
				
//				//if it is published to all school, no 'click all' checkbox can be selected
//				if(count( $w2_cfg["schoolCode"] ) != ($completeCount+1) )
//				{
					$parseData[$i]['checkboxForAllSch'] =$objDB->Get_Safe_Sql_Query("<input type='checkbox' class='contentSelected' name='allSch[]' id='allSch[]' value='${cid}' />"."\n\r");
//				}
//				else
//				{
//					$parseData[$i]['checkboxForAllSch'] =$objDB->Get_Safe_Sql_Query("<span></span>"."\n\r");
//				}
			}
		}
		else
		{
			$parseData = $publishList;
		}
		return $parseData;		
	}
}
?>