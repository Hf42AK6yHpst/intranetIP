<?php
	$w2_cfg_contentSetting['eng'] = array(
		'scheme1' => array(
				'type_super' => 'Advanced',
				'type' => 'Diary',
				'name' => 'An unforgettable day (S1)',  //020h
				'introduction' => 'You spent an unforgettable day with your family members. Write in your diary what happened and how you felt. Use around 120 words.',
			),		
		'scheme2' => array(
				'type_super' => 'Basic',
				'type' => 'Blog',
				'name' => 'An unforgettable sports day (S1)',  //001e
				'introduction' => 'You had an unforgettable sports day. Write in your blog what happened and how you felt. Use around 150 words.',
			),		
		'scheme3' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog',
				'name' => 'An unforgettable sports day (S1)', //001h
				'introduction' => 'You had an unforgettable sports day. Write in your blog what happened and how you felt. Use around 200 words.',
			),		
		'scheme4' => array(
				'type_super' => 'Basic',
				'type' => 'Blog Entry',
				'name' => 'The International Food Festival in Macau (S1)',  //002e
				'introduction' => 'Write a blog entry about the International Food Festival in Macau you attended. Use around 100 - 150 words.',
		),		
		'scheme5' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog Entry',
				'name' => 'The International Food Festival in Macau (S1)',  //002h
				'introduction' => 'Write a blog entry about the International Food Festival in Macau you attended. Use around 150 - 200 words.',
		),		
		'scheme6' => array(
				'type_super' => 'Basic',
				'type' => 'Email',
				'name' => 'Food and travel (S1)',  //003e
				'introduction' => 'You visited Hong Kong and tried various Cantonese cuisines last week. Write an email to your friend about what you ate and how you felt.
Use around 100- 150 words.',
		),		
		'scheme7' => array(
				'type_super' => 'Advanced',
				'type' => 'Email',
				'name' => 'Food and travel (S1)',  //003h
				'introduction' => 'You visited Hong Kong and tried various Cantonese cuisines last week. Write an email to your friend about what you ate and how you felt.
Use around 150 - 200 words.',
		),		

		'scheme8' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'Pollution in Hong Kong (S1)',  //008e
				'introduction' => 'Write an article about pollution in Hong Kong.  Use around 150 - 220 words.',
		),		
		'scheme9' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'Pollution in Hong Kong (S1)',  //008h
				'introduction' => 'Write an article about pollution in Hong Kong. Use around 200 - 250 words.',
		),		


		'scheme10' => array(
				'type_super' => 'Basic',
				'type' => 'Proposal',
				'name' => 'Proposal to promote sports at school (S1)',  //013e
				'introduction' => 'You are the chairperson of a sports club. Write a proposal to promote sports at school during sports week.  Use around 150 - 200 words.',
		),		
		'scheme11' => array(
				'type_super' => 'Advanced',
				'type' => 'Proposal',
				'name' => 'Proposal to promote sports at school (S1)',  //013h
				'introduction' => 'You are the chairperson of a sports club. Write a proposal to promote sports at school during sports week.  Use around 200 - 250 words.',
		),	

		'scheme12' => array(
				'type_super' => 'Basic',
				'type' => 'Blog Entry',
				'name' => 'Disagreement with my parents (S1)',  //004e
				'introduction' => 'You had a disagreement with your parents last week. Write a blog entry about the argument and how you felt.  Use around 150 - 200 words.',
		),		
		'scheme13' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog Entry',
				'name' => 'Disagreement with my parents (S1)',  //004h
				'introduction' => 'You had a disagreement with your parents last week. Write a blog entry about the argument and how you felt.  Use around 200 - 250 words.',
		),	
		
		'scheme14' => array(
				'type_super' => 'Basic',
				'type' => 'Recount',
				'name' => 'A memorable experience (S1)',  //005e
				'introduction' => 'Describe the most memorable experience you have had with your family.  Write a recount of happened and how you felt.  Use around 150 - 200 words.',
		),		
		'scheme15' => array(
				'type_super' => 'Advanced',
				'type' => 'Recount',
				'name' => 'A memorable experience (S1)',  //005h
				'introduction' => 'Describe the most memorable experience you have had with your family.  Write a recount of happened and how you felt.  Use around 200 - 250 words.',
		),
		
		'scheme16' => array(
				'type_super' => 'Basic',
				'type' => 'Email',
				'name' => 'My father overseas (S1)',  //006e
				'introduction' => 'Your father is working overseas. Write him an email about what happened in your family last weekend.  Use around 150 - 200 words.',
		),		
		'scheme17' => array(
				'type_super' => 'Advanced',
				'type' => 'Email',
				'name' => 'My father overseas (S1)',  //006h
				'introduction' => 'Your father is working overseas. Write him an email about what happened in your family last weekend.  Use around 200 - 250 words.',
		),
		
		'scheme18' => array(
				'type_super' => 'Basic',
				'type' => 'Fictional Biography',
				'name' => 'My role model (1) (S1)',  //007e
				'introduction' => 'Describe a role model and why you like him/her.  Use around 150 - 200 words.',
		),		
		'scheme19' => array(
				'type_super' => 'Advanced',
				'type' => 'Fictional Biography',
				'name' => 'My role model (1) (S1)',  //007h
				'introduction' => 'Describe a role model and why you like him/her.  Use around 170 - 250 words.',
		),		
		
		
		'scheme20' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'The 4R&rsquo;s (S1)',  //009e
				'introduction' => 'Write an article about the 4R’s relating to environmental management in Hong Kong.  Use around 150 - 220 words.',
		),		
		'scheme21' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'The 4R&rsquo;s (S1)',  //009h
				'introduction' => 'Write an article about the 4R’s relating to environmental management in Hong Kong.  Use around 180 - 250 words.',
		),		
		
		'scheme22' => array(
				'type_super' => 'Basic',
				'type' => 'Leaflet',
				'name' => 'School environmental conservation club (S1)',  //0010e
				'introduction' => 'Imagine you are the chairperson of the environmental conservation club. Write a leaflet to promote the club. Use around 150 - 200 words.',
		),		
		'scheme23' => array(
				'type_super' => 'Advanced',
				'type' => 'Leaflet',
				'name' => 'School environmental conservation club (S1)',  //0010h
				'introduction' => 'Imagine you are the chairperson of the environmental conservation club. Write a leaflet to promote the club. Use around 150 - 220 words.',
		),
		
				
		'scheme24' => array(
				'type_super' => 'Basic',
				'type' => 'Argumentative essay',
				'name' => 'Wearing a school uniform (S1)',  //0011e
				'introduction' => 'Write an argumentative essay discussing the advantages and disadvantages of wearing a school uniform. Use around 150 - 200 words.',
		),		
		'scheme25' => array(
				'type_super' => 'Advanced',
				'type' => 'Argumentative essay',
				'name' => 'Wearing a school uniform (S1)',  //0011h
				'introduction' => 'Write an argumentative essay discussing the advantages and disadvantages of wearing a school uniform. Use around 200 – 250 words.',
		),
		

		'scheme26' => array(
				'type_super' => 'Basic',
				'type' => 'Letter of advice',
				'name' => 'Teen problems (S1)',  //0012e
				'introduction' => 'Write a letter of advice to your cousin who is worried about her weight problems and acne. Use around 150 - 200 words.',
		),		
		'scheme27' => array(
				'type_super' => 'Advanced',
				'type' => 'Letter of advice',
				'name' => 'Teen problems (S1)',  //0012h
				'introduction' => 'Write a letter of advice to your cousin who is worried about her weight problems and acne. Use around 200 - 250 words.',
		),



		'scheme28' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'My favourite athlete (S1)',  //0014e
				'introduction' => 'Write an article about your favourite athlete. Use around 150 - 200 words.',
		),		
		'scheme29' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'My favourite athlete (S1)',  //0014h
				'introduction' => 'Write an article about your favourite athlete. Use around 200 - 250 words.',
		),



		'scheme30' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'Using documentaries in class (S1)',  //0015e
				'introduction' => 'Write an article on ‘How teachers can use documentaries in class.’ Use around 150 - 200 words.',
		),
		'scheme31' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'Using documentaries in class (S1)',  //0015h
				'introduction' => 'Write an article on ‘How teachers can use documentaries in class.’ Use around 200 - 250 words.',
		),



		'scheme32' => array(
				'type_super' => 'Basic',
				'type' => 'Blog',
				'name' => 'A documentary I recently watched (S1)',  //0016e
				'introduction' => 'Write a blog review about a documentary you have watched.  Use around 150 - 200 words.',
		),
		'scheme33' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog',
				'name' => 'A documentary I recently watched (S1)',  //0016h
				'introduction' => 'Write a blog review about a documentary you have watched. Use around 180 - 250 words.',
		),
		


		'scheme34' => array(
				'type_super' => 'Basic',
				'type' => 'Blog',
				'name' => 'Documentaries I like to watch (S1)',  //0017e
				'introduction' => 'Write a blog about the documentaries you like to watch. Use around 150 - 200 words.',
		),		
		'scheme35' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog',
				'name' => 'Documentaries I like to watch (S1)',  //0017h
				'introduction' => 'Write a blog about the documentaries you like to watch. Use around 200 - 250 words.',
		),


		'scheme36' => array(
				'type_super' => 'Basic',
				'type' => 'Recipe',
				'name' => 'A recipe (S1)',  //0019e
				'introduction' => 'Write a recipe for Double Chocolate Chip Brownies. Use around 150 - 200 words.',
		),		
		'scheme37' => array(
				'type_super' => 'Advanced',
				'type' => 'Recipe',
				'name' => 'A recipe (S1)',  //0019h
				'introduction' => 'Write a recipe for Double Chocolate Chip Brownies. Use around 200 - 250 words.',
		),


		'scheme38' => array(
				'type_super' => 'Basic',
				'type' => 'Personal Letter',
				'name' => 'My role model (2) (S1)',  //0018e
				'introduction' => 'Write a letter to your pen-friend, tell them about your role model. Use around 150 - 200 words.',
		),		
		'scheme39' => array(
				'type_super' => 'Advanced',
				'type' => 'Personal Letter',
				'name' => 'My role model (2) (S1)',  //0018h
				'introduction' => 'Write a letter to your pen-friend, tell them about your role model. Use around 170 - 250 words.',
		),		

		'scheme40' => array(
				'type_super' => 'Basic',
				'type' => 'Diary',
				'name' => 'An unforgettable day (S1)',  //0020e
				'introduction' => 'You spent an unforgettable day with your family. Write in your diary what happened and how you felt. Use around 120 words.',
		),		
		'scheme41' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'Introducing an interesting animal (S2)',  //0021h
				'introduction' => 'Introducing an interesting animal. Use around 250 words.',
		),		

		'scheme42' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'Introducing an interesting animal (S2)',  //0021e
				'introduction' => 'Introducing an interesting animal. Use around 200 words.',
		),		
		'scheme43' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'Write an article about your role model (S2)',  //0022h
				'introduction' => 'Write an article about your role model. Use around 250 words.',
		),		

		'scheme44' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'Write an article about your role model (S2)',  //0022e
				'introduction' => 'Write an article about your role model. Use around 200 words.',
		),		

		'scheme45' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'Write an article for your school newspaper about what you can do to protect the environment (S2)',  //0023h
				'introduction' => 'What you can do to protect the environment. Use around 250 words.',
		),		

		'scheme46' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'Write an article for your school newspaper about what you can do to protect the environment (S2)',  //0023e
				'introduction' => 'What you can do to protect the environment. Use around 200 words.',
		),		

		'scheme47' => array(
				'type_super' => 'Advanced',
				'type' => 'Film review',
				'name' => 'Write a film review about a film you have recently watched (S2)',  //0024h
				'introduction' => 'Write a film review. Use around 250 words.',
		),		

		'scheme48' => array(
				'type_super' => 'Basic',
				'type' => 'Film review',
				'name' => 'Write a film review about a film you have recently watched (S2)',  //0024e
				'introduction' => 'Write a film review. Use around 200 words.',
		),		

		'scheme49' => array(
				'type_super' => 'Advanced',
				'type' => 'Two sided argument',
				'name' => '‘Buying clothes is a waste of money’(S2)',  //0025h
				'introduction' => '‘Buying clothes is a waste of money.’ Use around 250 words.',
		),		

		'scheme50' => array(
				'type_super' => 'Basic',
				'type' => 'Two sided argument',
				'name' => '‘Buying clothes is a waste of money’(S2)',  //0025e
				'introduction' => '‘Buying clothes is a waste of money’. Use around 200 words.',
		),	

		'scheme51' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog',
				'name' => 'Write a blog entry about your holiday (S2)',  //0026h
				'introduction' => 'A blog entry about your holiday. Use around 250 words',
		),		

		'scheme52' => array(
				'type_super' => 'Basic',
				'type' => 'Blog',
				'name' => 'Write a blog entry about your holiday (S2)',  //0026e
				'introduction' => 'A blog entry about your holiday. Use around 200 words.',
		),
		'scheme53' => array(
				'type_super' => 'Advanced',
				'type' => 'Letter',
				'name' => 'Write a letter of invitation to an event that is being held (S2)',  //0027h
				'introduction' => 'A letter of invitation. Use around 250 words.',
		),		

		'scheme54' => array(
				'type_super' => 'Basic',
				'type' => 'Letter',
				'name' => 'Write a letter of invitation to an event that is being held (S2)',  //0027e
				'introduction' => 'A letter of invitation. Use around 200 words.',
		),
		//by Siena
		'scheme55' => array(
				'type_super' => 'Advanced',
				'type' => 'Blog',
				'name' => 'Write about your favourite genre of film (S2)',  //0028h
				'introduction' => 'My favourite genre of film. Use around 250 words.',
		),		

		'scheme56' => array(
				'type_super' => 'Basic',
				'type' => 'Blog',
				'name' => 'Write about your favourite genre of film. (S2)',  //0028e
				'introduction' => 'My favourite genre of film. Use around 200 words.',
		),
			//end by Siena
		'scheme57' => array(
				'type_super' => 'Advanced',
				'type' => 'Letter',
				'name' => 'Write a letter to your pen pal and give a personal description of yourself (S2)',  //0029h
				'introduction' => 'Letter to a pen pal describing yourself. Use around 250 words.',
		),
        'scheme58' => array(
				'type_super' => 'Basic',
				'type' => 'Letter',
				'name' => 'Write a letter to your pen pal and give a personal description of yourself (S2)',  //0029e
				'introduction' => 'Letter to a pen pal describing yourself. Use around 200 words.',
		),
		'scheme59' => array(
				'type_super' => 'Advanced',
				'type' => 'One-sided argumentative essay',
				'name' => '“Should we keep pets at school?” (S2)',  //0030h
				'introduction' => 'Should we keep pets at school.  Write a one-sided argumentative essay on this topic. Use around 250 words.',
		),
	
	     'scheme60' => array(
				'type_super' => 'Basic',
				'type' => 'One-sided argumentative essay',
				'name' => '“Should we keep pets at school?”  (S2)',  //0030e
				'introduction' => 'Should we keep pets at school. Write a one-sided argumentative essay on this topic. Use around 200 words.',
		),
		 'scheme61' => array(
				'type_super' => 'Advanced',
				'type' => 'Leaflet',
				'name' => 'Grooming services of your local pet shop (S2)',  //0031h
				'introduction' => 'Write a leaflet for the local pet shop about the grooming services they provide. Give reasons within the leaflet why people should use these services. Use around 250 words.',
		),
		 'scheme62' => array(
				'type_super' => 'Basic',
				'type' => 'Leaflet',
				'name' => 'Grooming services of your local pet shop (S2)',  //0031e
				'introduction' => 'Write a leaflet for the local pet shop about the grooming services they provide. Give reasons within the leaflet why people should use these services. Use around 200 words.',
		),
		 'scheme63' => array(
				'type_super' => 'Advanced',
				'type' => 'Email',
				'name' => 'An email to your friend about where to go shopping (S2)',  //0032h
				'introduction' => 'Your friend is visiting Hong Kong. Write an email to her telling her the best places where she can go shopping. Use around 250 words.',
		),
		 'scheme64' => array(
				'type_super' => 'Basic',
				'type' => 'Email',
				'name' => 'An email to your friend about where to go shopping (S2)',  //0032e
				'introduction' => 'Your friend is visiting Hong Kong. Write an email to her telling her the best places where she can go shopping. Use around 200 words.',
		),
		'scheme65' => array(
				'type_super' => 'Advanced',
				'type' => 'Leaflet',
				'name' => 'Charity leaflet asking for donations (S2)',  //0033h
				'introduction' => 'Write a leaflet asking people for donations (money, toys, books, etc). Explain what you want them to donate, why and when. Use around 250 words.',
		),
		'scheme66' => array(
				'type_super' => 'Basic',
				'type' => 'Leaflet',
				'name' => 'Charity leaflet asking for donations (S2)',  //0033e
				'introduction' => 'Write a leaflet asking people for donations (money, toys, books, etc). Explain what you want them to donate, why and when. Use around 200 words.',
		),
		'scheme67' => array(
				'type_super' => 'Advanced',
				'type' => 'Proposal',
				'name' => 'How can we make Hong Kong a cleaner place? (S2)',  //0034h
				'introduction' => 'Write a proposal to your teacher on something you can do at school to make Hong Kong a cleaner place. Use around 250 words.',
		),
		'scheme68' => array(
				'type_super' => 'Basic',
				'type' => 'Proposal',
				'name' => 'How can we make Hong Kong a cleaner place? (S2)',  //0034e
				'introduction' => 'Write a proposal to your teacher on something you can do at school to make Hong Kong a cleaner place. Use around 200 words.',
		),
		'scheme69' => array(
				'type_super' => 'Advanced',
				'type' => 'Letter of advice',
				'name' => 'A letter of advice to a new student (S2)',  //0035h
				'introduction' => 'Write a letter of advice to a new student on how they can adapt to life at your school.  Use around 250 words.',
		),
		'scheme70' => array(
				'type_super' => 'Basic',
				'type' => 'Letter of advice',
				'name' => 'A letter of advice to a new student (S2)',  //0035e
				'introduction' => 'Write a letter of advice to a new student on how they can adapt to life at your school. Use around 200 words.',
		),
		'scheme71' => array(
				'type_super' => 'Advanced',
				'type' => 'Descriptive review',
				'name' => 'Film Review (S2)',  //0036h
				'introduction' => 'Write a film review about a film you have recently watched. Explain why you liked it / didn’t like it. Use around 250 words.',
		),
		'scheme72' => array(
				'type_super' => 'Basic',
				'type' => 'Descriptive review',
				'name' => 'Film Review (S2)',  //0036e
				'introduction' => 'Write a film review about a film you have recently watched. Explain why you liked it / didn’t like it. Use around 200 words.',
		),
		'scheme73' => array(
				'type_super' => 'Advanced',
				'type' => 'Personal Letter',
				'name' => 'A letter to my friend about my role model (S2)',  //0037h
				'introduction' => 'Write a letter to your friend about your role model. Explain who she / he is and why she/he is your role model. Use around 250 words.',
		),
		'scheme74' => array(
				'type_super' => 'Basic',
				'type' => 'Personal Letter',
				'name' => 'A letter to my friend about my role model (S2)',  //0037e
				'introduction' => 'Write a letter to your friend about your role model. Explain who she / he is and why she/he is your role model. Use around 200 words.',
		),
		'scheme75' => array(
				'type_super' => 'Advanced',
				'type' => 'Article',
				'name' => 'My best holiday experience ever (S2)',  //0038h
				'introduction' => 'Write an article about your best holiday experience ever. Include information about where you went, what you did, and why it was so good. Use around 250 words.',
		),
		'scheme76' => array(
				'type_super' => 'Basic',
				'type' => 'Article',
				'name' => 'My best holiday experience ever (S2)',  //0038e
				'introduction' => 'Write an article about your best holiday experience ever. Include information about where you went, what you did, and why it was so good. Use around 200 words.',
		),
		'scheme77' => array(
				'type_super' => 'Advanced',
				'type' => 'Expository',
				'name' => 'What can you do at school to save the environment? (S2)',  //0039h
				'introduction' => 'Write an expository text on ‘What can you do at school to save the environment?’ Include explanations on why it is important to take action. Use around 250 words.',
		),
		'scheme78' => array(
				'type_super' => 'Basic',
				'type' => 'Expository',
				'name' => 'What can you do at school to save the environment? (S2)',  //0039e
				'introduction' => 'Write an expository text on ‘What can you do at school to save the environment?’ Include explanations on why it is important to take action. Use around 200 words.',
		),
		'scheme79' => array(
				'type_super' => 'Advanced',
				'type' => 'Argumentative',
				'name' => 'Should junk food be banned? (S2)',  //0040h
				'introduction' => '‘Should junk food be banned?’ Write a one-sided argumentative piece on why you think it should/shouldn’t be banned. Use around 250 words.',
		),
		'scheme80' => array(
				'type_super' => 'Basic',
				'type' => 'Argumentative',
				'name' => 'Should junk food be banned? (S2)',  //0040e
				'introduction' => '‘Should junk food be banned?’ Write a one-sided argumentative piece on why you think it should/shouldn’t be banned.  Use around 200 words.',
		),	
		'scheme81' => array(
				'type_super' => 'Advanced',
				'type' => 'News article',
				'name' => 'Crime (S3)',  //0040e
				'introduction' => 'Write a news article about a recent incident of crime that has happened in your area. Use around 300 words.',
		),	
		'scheme82' => array(
				'type_super' => 'Basic',
				'type' => 'News article',
				'name' => 'Crime (S3)',  //0040e
				'introduction' => 'Write a news article about a recent incident of crime that has happened in your area. Use around 250 words.',
		),	
	);


	$w2_cfg_contentSetting['chi'] = array(
			'scheme1' => array(
				'type' => '描寫文',
				'name' => '茶樓眾生相 (S1)',
				'introduction' => '以「茶樓眾生相」為題，寫作一篇不少於300字的描寫文。',
			),
			'scheme2' => array(
				'type' => '遊記',
				'name' => '留在我心底裏的風景 (S1)',
				'introduction' => '以「留在我心底裏的風景」為題，寫作一篇不少於400字的遊記。',
			),
			'scheme3' => array(
				'type' => '記敍文',
				'name' => '一次意外事故的經歷和感受 (S1)',
				'introduction' => '以「一次意外事故的經歷和感受」為題，寫作一篇不少於300字的記敍文。',
			),
			'scheme4' => array(
				'type' => '描寫文',
				'name' => '逛超級市場 (S1)',
				'introduction' => '以「逛超級市場」為題，寫作一篇不少於300字的描寫文。',
			),

			'scheme5' => array(
				'type' => '議論文',
				'name' => '談食物安全的重要性 (S1)',
				'introduction' => '以「談食物安全的重要性」為題，寫作一篇不少於500字的議論文。',
			),

			'scheme6' => array(
				'type' => '描寫文',
				'name' => '驟雨下的街頭 (S1)',
				'introduction' => '以「驟雨下的街頭」為題，寫作一篇不少於300字的描寫文。',
			),

			'scheme7' => array(
				'type' => '說明文',
				'name' => '讀書的苦與樂 (S1)',
				'introduction' => '以「讀書的苦與樂」為題，寫作一篇不少於400字的說明文。',
			),

			'scheme8' => array(
				'type' => '記敍文',
				'name' => '他，令我心悅誠服 (S1)',
				'introduction' => '以「他，令我心悅誠服」為題，寫作一篇不少於400字的作文。',
			),
			
			'scheme9' => array(
				'type' => '記敍文',
				'name' => '考試成績發下來 (S2)',
				'introduction' => '以「考試成績發下來」為題，寫作一篇不少於400字的記敍文。',
			),
			
			'scheme10' => array(
				'type' => '記敍文',
				'name' => '當____遇上____ (S2)',
				'introduction' => '以「當____遇上____」為題，寫作一篇不少於400字的記敍文。',
			),
			
			'scheme11' => array(
				'type' => '描寫文',
				'name' => '一位（形容詞）的（人物）(S2)',
				'introduction' => "以「一位（形容詞）* 的（人物）*」為題，寫作一篇不少於400字的描寫文。\n*括號內的字詞自訂",
			),
			
			'scheme12' => array(
				'type' => '描寫文',
				'name' => '電話 (S2)',
				'introduction' => '以「電話」為題，寫作一篇不少於400字的借物抒情文。',
			),

			'scheme13' => array(
				'type' => '描寫文',
				'name' => '我最敬畏的人 (S2)',
				'introduction' => '以「我最敬畏的人」為題，寫作一篇不少於400字的描寫文。',
			),
			'scheme14' => array(
				'type' => '議論文',
				'name' => '論孝道 (S2)',
				'introduction' => '以「論孝道」為題，寫作一篇不少於400字的議論文。',
			),
			'scheme15' => array(
				'type' => '記敍文',
				'name' => '那一次我哭了 (S2)',
				'introduction' => '以「那一次我哭了」為題，寫作一篇不少於400字的記敍文。',
			),
			'scheme16' => array(
				'type' => '議論文',
				'name' => '論交友之道 (S2)',
				'introduction' => '以「論交友之道」為題，寫作一篇不少於400字的議論文。',
			),
			'scheme17' => array(
				'type' => '抒情文',
				'name' => '珍貴的____(S3)',
				'introduction' => '以「珍貴的____」為題，寫作一篇不少於400字的抒情文。',
			),
			'scheme18' => array(
				'type' => '說明文',
				'name' => '我理想的____(S3)',
				'introduction' => '以「我理想的____」為題，寫作一篇不少於400字的抒情文。',
			),
			'scheme19' => array(
				'type' => '記敍文',
				'name' => '明天(S3)',
				'introduction' => '以「明天」為題，寫作一篇不少於400字的記敘文。',
			),
			'scheme20' => array(
				'type' => '記敍文',
				'name' => '電話(S3)',
				'introduction' => '以「電話」為題，寫作一篇不少於400字的記敍文。',
			),
			'scheme21' => array(
				'type' => '記敍文',
				'name' => '遇見(S3)',
				'introduction' => '以「遇見」為題，寫作一篇不少於400字的記敍文。',
			),
			'scheme22' => array(
				'type' => '描寫文',
				'name' => '眼鏡眼鏡(S3)',
				'introduction' => '以「眼鏡眼鏡」為題，寫作一篇不少於400字的描寫文。',
			),
			'scheme23' => array(
				'type' => '議論文',
				'name' => '手提電話對中學生是否重要(S3)',
				'introduction' => '以「手提電話對中學生是否重要」為題，寫作一篇不少於400字的議論文。',
			),
			'scheme24' => array(
				'type' => '議論文',
				'name' => '學校應否設立校規(S3)',
				'introduction' => '以「學校應否設立校規」為題，寫作一篇不少於400字的議論文。',
			),
	);

	//ls --> default chi
	$w2_cfg_contentSetting['ls'] = array(
			'scheme1' => array(
				'type' => '市區重建',
				'name' => '中環嘉咸街重建項目 (S1)',
				'introduction' => '市區重建對嘉咸街的影響是正面還是負面？',
			),
			'scheme2' => array(
				'type' => '土地利用',
				'name' => '探討填海和發展岩洞的利與弊 (S1)',
				'introduction' => '如何增加和善用香港的土地資源？',
			),
			'scheme3' => array(
				'type' => '屏風效應',
				'name' => '九龍區屏風樓 (S1)',
				'introduction' => '屏風樓損害居住環境和市民健康？',
			),
			'scheme4' => array(
				'type' => '香港經濟',
				'name' => '內地珠三角與香港的經濟關係 (S1)',
				'introduction' => '香港與內地珠三角的經濟合作和競爭',
			),
			'scheme5' => array(
				'type' => '農民工',
				'name' => '富士康 (S2)',
				'introduction' => '國家經濟發展，對農民工利多於弊？',
			),
			'scheme6' => array(
				'type' => '中國軟實力',
				'name' => '世界博覽會 (S2)',
				'introduction' => '舉辦上海世博，贏了國際形象，輸了社會和諧？',
			),
			'scheme7' => array(
				'type' => '香港污染問題',
				'name' => '香港環保政策 (S2)',
				'introduction' => '教育比徵稅更有效減少廢物？',
			),
			'scheme8' => array(
  			'type' => '全球暖化',
  			'name' => '中國、美國 (S2)',
  			'introduction' => '減緩全球暖化，發展中國家的責任較大？',
  			),
			'scheme9' => array(
  			'type' => '全球化：經濟範疇',
  			'name' => '成衣業 (S3)',
  			'introduction' => '全球化下，利潤跑進誰的口袋？',
  			),
			'scheme10' => array(
  			'type' => '全球化：文化範疇',
  			'name' => '麥當勞 (S3)',
  			'introduction' => '全球化下，傳統文化逐漸消失？',
  			),
			'scheme11' => array(
  			'type' => '全球化：政府範疇',
  			'name' => '金融海嘯 (S3)',
  			'introduction' => '全球化加強各國合作還是擴大分歧？',
  			),
			'scheme12' => array(
  			'type' => '全球化：非政府範疇',
  			'name' => '國際環保組織 (S3)',
  			'introduction' => '國際非政府組織在政治全球化中扮演甚麼角色？',
  			),
  			
	);

	$w2_cfg_contentSetting['ls_eng'] = array(
			'scheme1' => array(
				'type' => 'Urban renewal',
				'name' => 'Redevelopment project of Graham Street, Central (S1)',
				'introduction' => 'Does urban renewal have positive or negative impacts on Graham Street?',
			),
			'scheme2' => array(
				'type' => 'Land use',
				'name' => 'To explore the pros and cons of land reclamation and rock cavern development (S1)',
				'introduction' => 'How to increase and use land resources in Hong Kong properly?',
			),
			'scheme3' => array(
				'type' => 'Wall effect',
				'name' => 'Screen-like buildings in Kowloon (S1)',
				'introduction' => 'Do screen-like buildings affect the living environment and people\'s health?',
			),
			'scheme4' => array(
				'type' => 'Hong Kong\'s economy',
				'name' => 'The economic relationship between Hong Kong and the mainland\'s Pearl River Delta Region (S1)',
				'introduction' => 'The economic cooperation and competition between Hong Kong and the mainland\'s Pearl River Delta Region',
			),
			'scheme5' => array(
				'type' => 'Migrant worker',
				'name' => 'Foxconn (S2)',
				'introduction' => 'Does China’s economic development have more pros than cons for peasant-labourers?',
			),
			'scheme6' => array(
				'type' => 'China\'s soft power',
				'name' => 'The Shanghai World Expo (S2)',
				'introduction' => 'Did the Shanghai World Expo promote China\'s international image at the expense of social harmony?',
			),
			'scheme7' => array(
			    'type' => 'The pollution problem in Hong Kong',
				'name' => 'Hong Kong\'s environmental policies (S2)',
			    'introduction' => 'Is education more effective than taxation in reducing waste?',
			),
			 'scheme8' => array(
				'type' => 'Global warming',
				 'name' => 'China, the US (S2)',
				 'introduction' => 'Do developing countries have greater responsibility for alleviating global warming?',
			),
			'scheme9' => array(
				'type' => 'Globalization: Economic aspect',
				 'name' => 'Garment industry(S3)',
				 'introduction' => 'Who can benefit from globalization?',
			),
			'scheme10' => array(
				'type' => 'Globalization: Cultural aspect',
				 'name' => 'McDonald’s(S3)',
				 'introduction' => 'Are traditional cultures disappearing under globalization?',
			),	
			'scheme11' => array(
				'type' => 'Globalization: Governmental aspect',
				 'name' => 'Financial tsunami(S3)',
				 'introduction' => 'Has globalization strengthened international cooperation or intensified disputes between countries?',
			),	
			'scheme12' => array(
				'type' => 'Globalization: Non-governmental aspect',
				 'name' => 'International environmental groups(S3)',
				 'introduction' => 'What roles do international non-governmental organizations play under political globalization?',
			),				
	);

	$w2_cfg_contentSetting['sci'] = array(
			'scheme1' => array(
				'type' => 'Environmental',
				'name' => 'Renewable Energy (S1)',
				'introduction' => 'The following are two articles about the development of renewable energy. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),
			'scheme2' => array(
				'type' => 'Environmental',
				'name' => 'Water Pollution (S1)',
				'introduction' => 'Following are three articles about water purification. Read and write an essay with 300 words to compare and contrast different aspects of the proper or improper uses of water.',
			),
			'scheme3' => array(
				'type' => 'Others',
				'name' => 'Animal Species (S1)',
				'introduction' => 'The following are two articles about the animal species in Hong Kong. Read them and then write a record of 300 words to describe your observations.',
			),
			'scheme4' => array(
				'type' => 'Others',
				'name' => 'Biotech (S1)',
				'introduction' => 'The following are two articles about the biotechnology of reproduction. Read them and then write a proposal of 300 words to seek funding from the Government to support advanced research in biotechnology.',
			),
			'scheme5' => array(
				'type' => 'Others',
				'name' => 'Use of Solar Energy found in space (S2)',
				'introduction' => 'The following are two articles about the use of solar energy which is found in space. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),
			'scheme6' => array(
				'type' => 'Environmental',
				'name' => 'Growth of plants in response to environmental stresses (S2)',
				'introduction' => 'The following are two articles about the growth of plants in response to various environmental stresses. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),
			'scheme7' => array(
				'type' => 'Others',
				'name' => 'Tissue Culture (S2)',
				'introduction' => 'The following are two articles about the development in animal and plant tissue cell culture. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),
			'scheme8' => array(
				'type' => 'Others',
				'name' => 'What has the Government done to help with neurodegenerative diseases (S2)',
				'introduction' => 'The following are two articles about how Hong Kong supports those with neurodegenerative diseases. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),
			'scheme9' => array(
				'type' => 'Others',
				'name' => 'The Discovery of DNA (S3)',
				'introduction' => 'The following are two articles about the development of DNA. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),	
			'scheme10' => array(
				'type' => 'Others',
				'name' => 'The Discovery of Atoms (S3)',
				'introduction' => 'The following are two articles about the discovery of atoms. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),
			'scheme11' => array(
				'type' => 'Others',
				'name' => 'Ethanol (S3)',
				'introduction' => 'The following are two articles about the making of ethanol. Read them and then write an essay of 300 words to summarize findings and opinions.',
			),	
			'scheme12' => array(
				'type' => 'Others',
				'name' => 'The Discovery of the Telescope (S3)',
				'introduction' => 'The following are two articles about the discovery of the telescope. Read them and then write an essay of 300 words to summarize findings and discoveries.',
			),	
	);

	include_once('w2_contentConfigContainer.php');
?>