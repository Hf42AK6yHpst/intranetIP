<?
// using: 

class libWritingXml extends libWriting {
	
	public function __construct($contentCode, $schemeCode) {
		parent::__construct();
		
		$this->setContentCode($contentCode);
		$this->setSchemeCode($schemeCode);
		$this->loadRecordFromStorage();
    }
    
    public function loadRecordFromStorage() {
    	return false;
    }
    
    public function save(){
		return false;	
	}
	
	public function delete(){
		return false;
	}
	
	public function findStep() {
		//step.STEP_ID, step.CODE,step.SEQUENCE
		$stepInfoAry = $this->objW2->getStepInfoByContent($this->getContentCode());
		$numOfStep = count($stepInfoAry);
		
		$returnAry = array();
		for ($i=0; $i<$numOfStep; $i++) {
			$returnAry[$i]['STEP_ID'] = '';
			$returnAry[$i]['STEP_TITLE_ENG'] = $stepInfoAry[$i]['titleEng'];
			$returnAry[$i]['STEP_TITLE_CHI'] = $stepInfoAry[$i]['titleChi'];
			$returnAry[$i]['STEP_CODE'] = $stepInfoAry[$i]['code'];
			$returnAry[$i]['STEP_SEQUENCE'] = $stepInfoAry[$i]['sequence'];
			$returnAry[$i]['WRITING_ID'] = '';
		}
		
		return $returnAry;
	}
}
?>