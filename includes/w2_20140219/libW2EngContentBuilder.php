<?php

include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");


class libEngContentBuilder
	{
		
		### XML Eng builder
		function buildGeneralInfoInsert($topicIntro, $topicName, $level, $grade='')
		{
			$returnStr=" '$topicIntro', '$topicName', '$level', '$grade' ";
			return array( 'field'=>'topicIntro, topicName, level, grade', 'content'=>$returnStr); 
		}
//		function buildGeneralInfoInsert($topicIntro, $topicName, $level, $category)
//		{
//			$returnStr=" '$topicIntro', '$topicName', '$level', '$category' ";
//			return array( 'field'=>'topicIntro, topicName, level, category', 'content'=>$returnStr); 
//		}
		function buildGeneralInfoUpdate($topicIntro, $topicName, $level, $grade='')
		{
			$fieldName = array('topicIntro','topicName', 'level', 'grade');
			$fieldValue = array("'$topicIntro'", "'$topicName'", "'$level'", "'$grade'");
			$updateArray=array();
			for ($i=0;$i<count($fieldName);$i++)
			{
				$updateArray[] = $fieldName[$i]." = ".$fieldValue[$i];
			}
			return implode(",", (array)$updateArray);  
		}
//		function buildGeneralInfoUpdate($topicIntro, $topicName, $level, $category)
//		{
//			$fieldName = array('topicIntro','topicName', 'level', 'category');
//			$fieldValue = array("'$topicIntro'", "'$topicName'", "'$level'", "'$category'");
//			$updateArray=array();
//			for ($i=0;$i<count($fieldName);$i++)
//			{
//				$updateArray[] = $fieldName[$i]." = ".$fieldValue[$i];
//			}
//			return implode(",", (array)$updateArray);  
//		}
		
		function buildStep1($step1Int,$step1TextType,$step1Purpose,$step1Content)
		{

			$returnStr='';	
			
			$returnStr.='<step1>';	
			$returnStr.='<step1Int>'.'<![CDATA['.$step1Int.']]>'.'</step1Int>';	
			$returnStr.='<step1TextType>'.'<![CDATA['.$step1TextType.']]>'.'</step1TextType>';
			$returnStr.='<step1Purpose>'.'<![CDATA['.$step1Purpose.']]>'.'</step1Purpose>';		
			$returnStr.='<step1Content>'.'<![CDATA['.$step1Content.']]>'.'</step1Content>';		
			$returnStr.='</step1>';	
			return array( 'field'=>'step1Content', 'content'=>"'$returnStr'" );
		}
		function buildStep2($step2Int)
		{
			$returnStr='';	
			$returnStr.='<step2>';	
			$returnStr.='<step2Int>'.'<![CDATA['.$step2Int.']]>'.'</step2Int>';	
			$returnStr.='</step2>';	
			return array( 'field'=>'step2Content', 'content'=>"'$returnStr'");
		}
		
		function buildStep3($step3Int, $step3Sample, $step3Vocab, $step3Grammar)
		{
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';	
			$returnStr.='<step3SampleWriting>'.'<![CDATA['.$step3Sample.']]>'.'</step3SampleWriting>';	
			$returnStr.='<step3Vocab>'.'<![CDATA['.$step3Vocab.']]>'.'</step3Vocab>';	
			$returnStr.='<step3Grammar>'.'<![CDATA['.$step3Grammar.']]>'.'</step3Grammar>';	
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}
		
		function buildStep3New($step3Int, $step3ParagraphAry, $step3RemarkAry, $step3Vocab, $step3Grammar)
		{
			global $w2_cfg;
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';
			$returnStr.='<step3ParagraphAry>';
			$paragraphCnt = count($step3ParagraphAry);
			for($i=0;$i<$paragraphCnt;$i++){
				$returnStr.='<paragraphItem'.($i+1).'>';
				foreach($step3ParagraphAry[$i] as $_key => $_value){
					$returnStr.='<'.$_key.'>';
					$returnStr.='<![CDATA['.$_value.']]>';
					$returnStr.='</'.$_key.'>';
				}
				$returnStr.='</paragraphItem'.($i+1).'>';
			}			
			$returnStr.='</step3ParagraphAry>';	
			$returnStr.='<step3RemarkAry>';
			for($i=0;$i<$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
				if(count($step3RemarkAry[$i])>0){
					$returnStr.='<remarkItem'.($i+1).'>';
					foreach($step3RemarkAry[$i] as $_key => $_value){
							$returnStr.='<'.$_key.'>';
							$returnStr.='<![CDATA['.$_value.']]>';
							$returnStr.='</'.$_key.'>';
					}
					$returnStr.='</remarkItem'.($i+1).'>';
				}
			}	
			$returnStr.='</step3RemarkAry>';		
			$returnStr.='<step3Vocab>'.'<![CDATA['.$step3Vocab.']]>'.'</step3Vocab>';	
			$returnStr.='<step3Grammar>'.'<![CDATA['.$step3Grammar.']]>'.'</step3Grammar>';	
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}
				
		function buildStep4($step4Int, $step4DraftQ)
		{
			$returnStr='';	
			$returnStr.='<step4>';	
			$returnStr.='<step4Int>'.'<![CDATA['.$step4Int.']]>'.'</step4Int>';	
			$returnStr.='<step4DraftQList>';
		
			foreach( (array)$step4DraftQ as $step4DraftEachQ)
			{
				$returnStr.='<step4DraftQ>'.'<![CDATA['.trim(stripslashes($step4DraftEachQ)).']]>'.'</step4DraftQ>';	
			}
			$returnStr.='</step4DraftQList>';	
			$returnStr.='</step4>';	
			return array( 'field'=>'step4Content', 'content'=>"'$returnStr'");
		
		}
		
		function buildStep5($step5Int, $step5DefaultWriting)
		{
			$returnStr='';	
			$returnStr.='<step5>';	
			$returnStr.='<step5Int>'.'<![CDATA['.$step5Int.']]>'.'</step5Int>';			
			$returnStr.='<step5DefaultWriting>'.'<![CDATA['.$step5DefaultWriting.']]>'.'</step5DefaultWriting>';			
			$returnStr.='</step5>';	
			
			return array( 'field'=>'step5Content', 'content'=>"'$returnStr'");
		}
		
		function buildResource($refObject)
		{
			$i=1;
			$returnStr='';			
			$returnStr.='<resource>';
				
			foreach((array)$refObject as $refItem)
			{
				$returnStr.='<resourceItem'.$i.'>';
					$returnStr.='<resourceItemName>';
						$returnStr.='<![CDATA['.$refItem['resourceName'].']]>';
					$returnStr.='</resourceItemName>';
					$returnStr.='<resourceItemWebsite>';
						$returnStr.='<![CDATA['.$refItem['resourceWebsite'].']]>';
					$returnStr.='</resourceItemWebsite>';
					
					
					$returnStr.='<resourceItemChecked>';	
						foreach((array)$refItem['checked'] as $key => $value)
						{
							$returnStr.='<'.$value.'>';	
							$returnStr.='<![CDATA[yes]]>';
							$returnStr.='</'.$value.'>';	
						}
					$returnStr.='</resourceItemChecked>';
				$returnStr.='</resourceItem'.$i.'>';	
				++$i;
			}
			$returnStr.='</resource>';				
			return array( 'field'=>'resource', 'content'=>"'{$returnStr}'" );
		}
		function buildTeacherAttachment($attachObject)
		{
			$returnStr='<TeacherAttachment>';			
			
			foreach((array)$attachObject as $_step => $_attachItem)
			{	
				$returnStr.='<teacherAttachment_'.$_step.'>';
				$i=1;
				foreach((array)$_attachItem as $__attachAry)
				{
					$returnStr.='<teacherAttachmentItem'.$i.'>';
						$returnStr.='<teacherAttachmentItemName>';
							$returnStr.='<![CDATA['.$__attachAry['filename'].']]>';
						$returnStr.='</teacherAttachmentItemName>';
						$returnStr.='<teacherAttachmentItemFile>';
							$returnStr.='<![CDATA['.$__attachAry['filepath'].']]>';
						$returnStr.='</teacherAttachmentItemFile>';
					$returnStr.='</teacherAttachmentItem'.$i.'>';				
					$i++;
				}
				$returnStr.='</teacherAttachment_'.$_step.'>';
			}				
			$returnStr.='</TeacherAttachment>';
			
			return array( 'field'=>'teacherAttachment', 'content'=>"'{$returnStr}'" );
		}		
		### XML Eng Builder End
		
		### DB Data	Change
		function buildDBInfoInsert($r_contentCode,$schemeNum)
		{
			GLOBAL $w2_cfg;
			$status=$w2_cfg["contentWriting"]["W2_TEMP"];
			$schoolCode = $w2_cfg["schoolCodeCurrent"];
			$returnStr=" '$r_contentCode',$status , now(), $_SESSION[UserID], now(), $_SESSION[UserID], $schemeNum,  '".strtolower($schoolCode)."'";
			
			return array( 'field'=>'r_contentCode, 
							status, 
							dateInput, 
							createdBy,
							dateModified, 
							modifiedBy,
							schemeNum,
							schoolCode'
							,
						 	'content'=>$returnStr);	
		}
		
		function buildDBInfoUpdate($r_contentCode)
		{
			
			return array( "r_contentCode = '${r_contentCode}'",
							"dateModified = now()", 
							"modifiedBy =  ${_SESSION[UserID]}"
							);	
		}
		### DB Data	Change End
		
		### Other helper functions
		function getNextSchemeNum($r_contentCode)
		{
			$objDB = new libdb();
			$sql="SELECT (MAX(schemeNum)+1) as nextNum FROM W2_CONTENT_DATA WHERE r_contentCode = '$r_contentCode'; ";
			$nextSchemeNum = $objDB->returnResultSet($sql);
			return ($nextSchemeNum[0]['nextNum']==''?STARTING_SCHEME_NUM:$nextSchemeNum[0]['nextNum']);
		}
		### Other helper functions End
			
		
		
	}
?>