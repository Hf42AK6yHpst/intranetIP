<?php
## Using By : 
/*
###########################################
## Modification Log:
## 2015-01-22 (Carlos): [ip2.5.6.3.1] Modified getActivitySummaryReportColumnArray() - redefine the fields
## 2014-07-23 (Carlos): Modified getActivitySummaryReportColumnArray() - added joint activity fields
## 2014-04-16 (Carlos): TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
##						Added getActivitySummaryGroupReportRecords(), getClubRolesAndCommentsRecords(), getActivitySummaryReportColumnArray()
###########################################
*/
if (!defined("LIBCLUBSENROL_CUST_DEFINED"))                     // Preprocessor directive
{

	class libclubsenrol_cust extends libclubsenrol 
	{
		function libclubsenrol_cust ()
		{
			$this->libdb();
		}
		
		################ ���l�尪���Ǯ� Yeo Chei Man Senior Secondary School [Start]
		### 1. Club / Activity performance is calculated by attendance times x 10 (score)
		### 2. Student Total Performance = club & activity total performance + manually adjustment score
		### 3. function "generate club / activity performance" - re-calculate all the club / activity performance, and need update student total performance
		#############################################################################
		function returnClubPerformanceByUserIDs($UserID_str='', $AcademicYearID='')
		{
			$result = array();
			
			if($UserID_str)
			{
				$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
				
				$sql = "
					select
						a.UserID,
						sum(a.Performance) as performance
					from 
						INTRANET_USERGROUP as a
						inner join INTRANET_GROUP as b on (b.GroupID=a.GroupID and b.RecordType=5 and b.AcademicYearID='$AcademicYearID' )
					where 
						a.UserID in ($UserID_str)
					group by a.UserID
					";
				$result = $this->returnArray($sql);	
			}
			
			return $result;
		}
		
		function returnActivityPerformanceByUserIDs($UserID_str='', $AcademicYearID='')
		{
			$result = array();
			
			if($UserID_str)
			{
				$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
				
				$sql ="
					select
						a.StudentID,
						sum(a.Performance) as performance
					from
						INTRANET_ENROL_EVENTSTUDENT as a
						inner join INTRANET_ENROL_EVENTINFO as b on (b.EnrolEventID = a.EnrolEventID and b.AcademicYearID='$AcademicYearID')
					where
						a.StudentID in ($UserID_str)
					group by a.StudentID
				";
				$result = $this->returnArray($sql);	
			}
			
			return $result;
		}
		
		function InsertUpdateStudentTotalPerformance($dataAry=array(),$AcademicYearID='')
		{
			if(!empty($dataAry))
			{
				global $UserID;
				
				$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
				
				$this_UserID = $dataAry['UserID'];
				$this_Adjustment = $dataAry['this_Adjustment'];
				$this_total = $dataAry['this_total'];
				
				# insert? update?
				$sql="select RecordID from ENROLMENT_ADJUSTMENT_PERFORMNAMCE_CUST where UserID='$this_UserID' and AcademicYearID='$AcademicYearID'";
				$result = $this->returnVector($sql);
				$thisRecord = $result[0];
				if(empty($thisRecord))
				{
					$sql = "insert into ENROLMENT_ADJUSTMENT_PERFORMNAMCE_CUST (UserID, AcademicYearID, Adjustment, TotalPerformance, DateInput, InputBy, DateModified, ModifyBy)
					values ($this_UserID, $AcademicYearID, $this_Adjustment, $this_total, now(), $UserID, now(), $UserID)";
				}
				else
				{
					$sql = "update ENROLMENT_ADJUSTMENT_PERFORMNAMCE_CUST set 
							Adjustment = '$this_Adjustment',
							TotalPerformance = '$this_total',
							DateModified = now(),
							ModifyBy = '$UserID'
							where RecordID='$thisRecord'";
				}
				$this->db_db_query($sql);
			}
		}
		
		function returnAdjustmentByUserIDs($UserID_str='', $AcademicYearID='')
		{
			$result = array();
			
			if($UserID_str)
			{
				$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
				
				$sql = "select UserID, Adjustment,DateModified from ENROLMENT_ADJUSTMENT_PERFORMNAMCE_CUST 
					where UserID in ($UserID_str)
					and AcademicYearID='$AcademicYearID'";
				$result = $this->returnArray($sql);		
			}
			
			return $result;
		}
		
		function countClubAttendance_AllClubs($AcademicYearID='')
		{
			$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
			
			$sql = "
				select 
					c.UserID, c.EnrolGroupID, count(c.EnrolGroupID)
				from 
					INTRANET_USERGROUP as c
					inner join INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
					inner join INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5 and b.AcademicYearID='$AcademicYearID' )
					inner join INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID and (e.RecordStatus Is Null Or e.RecordStatus = 1))
					inner join INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
				group by 
					d.UserID, c.EnrolGroupID
				";
			$result = $this->returnArray($sql);
			return $result;	
		}
		
		function countActivityAttendance_AllActivities($AcademicYearID='')
		{
			$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
			
			$sql = "
				select 
					a.StudentID, a.EnrolEventID, count(a.EnrolEventID)
				from 
					INTRANET_ENROL_EVENTSTUDENT as a
					inner join INTRANET_USER as d on (d.UserID=a.StudentID and d.RecordType=2)
					inner join INTRANET_ENROL_EVENTINFO as b on (b.EnrolEventID=a.EnrolEventID and b.AcademicYearID='$AcademicYearID')
					inner join INTRANET_ENROL_EVENT_DATE as c on (c.EnrolEventID=a.EnrolEventID and (c.RecordStatus Is Null Or c.RecordStatus = 1))
					inner join INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=c.EventDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
				group by 
					a.StudentID, a.EnrolEventID
			";
			$result = $this->returnArray($sql);	
			return $result;	
		}
		
		function clearClubPerformance($AcademicYearID='')
		{
			$result = array();
			
			$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
			
			$sql = "
				select 
					distinct(a.EnrolGroupID)
				from 
					INTRANET_USERGROUP as a
					inner join INTRANET_GROUP as b on (b.GroupID=a.GroupID and b.AcademicYearID='$AcademicYearID')
				where 
					a.EnrolGroupID>0
				";
			$result = $this->returnVector($sql);	
			if(!empty($result))
			{
				$IDs = implode(",",$result);
				$sql = "update INTRANET_USERGROUP set Performance=0 where EnrolGroupID in ($IDs)";
				$this->db_db_query($sql);
			}
		}
		
		function clearActivityPerformance($AcademicYearID='')
		{
			$result = array();
			
			$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
			
			$sql ="
					select
						distinct(a.EnrolEventID)
					from
						INTRANET_ENROL_EVENTSTUDENT as a
						inner join INTRANET_ENROL_EVENTINFO as b on (b.EnrolEventID = a.EnrolEventID and b.AcademicYearID='$AcademicYearID')
				";
			$result = $this->returnVector($sql);	
			if(!empty($result))
			{
				$IDs = implode(",",$result);
				$sql = "update INTRANET_ENROL_EVENTSTUDENT set Performance=0 where EnrolEventID in ($IDs)";
				$this->db_db_query($sql);
			}
		}
		
		
		################ ���l�尪���Ǯ� Yeo Chei Man Senior Secondary School [End]
		
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getActivitySummaryGroupReportRecords($options)
		{
			global $Lang;
			
			$LibPortfolio = new libpf_slp();
			
			$by_class_level = $options['ReportType'] == 'ClassLevel';
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$sel_category = $options['sel_category']; // array expected
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				//$selectSemester = getCurrentSemesterID();
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			
			if($by_class_level){
				//$year_cond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				$target_year_id = $options['TargetYearID'];
			}
			
			$YearTermIDArr = $selectSemester;
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr))
					$conds_YearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
				else
					$conds_YearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
			}
			
			$SemesterFieldName = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			$PIC_name_field = getNameFieldByLang("u.");
			
			//$OleCategoryAssoc = $this->Get_Ole_Category_Array(true);
			$DefaultELEArray = $LibPortfolio->GET_ELE();
			
			$sql = "SELECT 
						etos.OLE_Component,
						ieei.EnrolEventID,
						ieei.EventTitle,
						IF (iegi.Semester Is Null Or iegi.Semester = '',
							Concat(ig.TitleChinese, ' (".$Lang['eEnrolment']['WholeYear'].")'),
							Concat(ig.TitleChinese, ' (', $SemesterFieldName, ')') 
						) as GroupTitle,
						GROUP_CONCAT(DISTINCT $PIC_name_field SEPARATOR ', ') as PIC,
						GROUP_CONCAT(DISTINCT y.YearName ORDER BY y.Sequence SEPARATOR ', ') as Target,
						GROUP_CONCAT(DISTINCT y.YearID) as YearID  
					FROM INTRANET_ENROL_EVENTINFO as ieei 
					INNER JOIN ENROLMENT_TO_OLE_SETTING as etos ON etos.RelatedEnrolID=ieei.EnrolEventID AND etos.RecordType='activity' 
					$join_date_table 
					LEFT JOIN INTRANET_ENROL_GROUPINFO as iegi ON iegi.EnrolGroupID=ieei.EnrolGroupID 
					LEFT JOIN INTRANET_GROUP as ig ON (ig.GroupID = iegi.GroupID) 
					LEFT JOIN ACADEMIC_YEAR_TERM as ayt On (iegi.Semester = ayt.YearTermID) 
					INNER JOIN INTRANET_ENROL_EVENTSTAFF as iee ON iee.EnrolEventID=ieei.EnrolEventID AND iee.StaffType='PIC' 
					LEFT JOIN INTRANET_USER as u ON u.UserID=iee.UserID 
					INNER JOIN INTRANET_ENROL_EVENTCLASSLEVEL as ieec ON ieec.EnrolEventID=ieei.EnrolEventID 
					LEFT JOIN YEAR as y ON y.YearID=ieec.ClassLevelID 
					WHERE TRIM(etos.OLE_Component)<>'' AND ieei.AcademicYearID='$selectYear' $conds_YearTermIDArr $year_cond $date_cond
					GROUP BY ieei.EnrolEventID";
			$records = $this->returnArray($sql);
			$record_count = count($records);
			//debug_r($sql);
			$returnAry = array();
			$OleToRecord = array();
			for($i=0;$i<$record_count;$i++){
				$ole_components = explode(",",$records[$i]['OLE_Component']);
				$ole_components_count = count($ole_components);
				if(!$by_class_level){
					$in_catogory = false;
					for($j=0;$j<$ole_components_count;$j++){
						if(in_array($ole_components[$j],$sel_category)){
							$in_category = true;
						}
					}
					if(!$in_category) continue;
				}
				
				$in_year_class = true;
				if($by_class_level){
					$year_id_ary = explode(",",$records[$i]['YearID']);
					if(!in_array($target_year_id,$year_id_ary)){
						$in_year_class = false;
						continue;
					}
				}
				
				$sql = "SELECT SUM(TIME_TO_SEC(IF(TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) < TIME('00:00:00'), '00:00:00', TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart))))/3600 
						FROM INTRANET_ENROL_EVENT_DATE as e 
						WHERE e.EnrolEventID='".$records[$i]['EnrolEventID']."' AND (e.RecordStatus IS NULL OR e.RecordStatus = 1) and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d')>= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate')";
				$tmp = $this->returnVector($sql);
				$records[$i]['ActivityTotalTime'] = sprintf("%.2f",$tmp[0]);
				
				for($j=0;$j<$ole_components_count;$j++){
					$ole_type = $ole_components[$j];
					if(!$by_class_level && !in_array($ole_type,$sel_category))
					{
						continue;
					}
					if(!isset($OleToRecord[$ole_type])){
						$OleToRecord[$ole_type] = array();
					}
					$records[$i]['CategoryName'] = $DefaultELEArray[$ole_type];
					$OleToRecord[$ole_type][] = $records[$i];
				}
			}
			
			return $OleToRecord;
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getClubRolesAndCommentsRecords($options)
		{
			global $Lang;
			
			//debug_r($options);
			$format = $options['format'];
			$rankTarget = $options['rankTarget'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$sel_record_type = $options['sel_record_type']; // '': All; '1': with comments; '2': without comments
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				//$selectSemester = getCurrentSemesterID();
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			$YearTermIDArr = $selectSemester;
			
			if($rankTarget == 'form'){
				$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'class'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'student'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
			}
			
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr))
					$conds_YearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
				else
					$conds_YearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
			}
			
			$record_type_cond = "";
			if($sel_record_type == '1'){
				$record_type_cond = " AND (TRIM(iees.CommentStudent)<>'' OR TRIM(iees.Performance)<>'') ";
			}else if($sel_record_type == '2'){
				$record_type_cond = " AND (TRIM(iees.CommentStudent)='' OR TRIM(iees.Performance)='') ";
			}
			
			if($format == 'csv'){
				$EmptySymbol = '';
				$StylePrefix = '';
				$StyleSuffix = '';
			}else{
				$EmptySymbol = $Lang['General']['EmptySymbol'];
				$StylePrefix = '<span class="tabletextrequire">';
				$StyleSuffix = '</span>';
			}
			
			$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
			$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
			$StudentNameField = getNameFieldByLang("iu.");
			
			$clsName = $YearClassNameField;
			$sql = "SELECT 
						$AcademicYearNameField as YearName,
						IF(ayt.YearTermID IS NULL,'".$Lang['eEnrolment']['WholeYear']."',$SemesterNameField) as Semester,
						$YearClassNameField as ClassName,
						ycu.ClassNumber,
					/*	CONCAT(	IF(iug.RecordType = 'A', '".$StylePrefix."*".$StyleSuffix."', ''),
									IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
									$StudentNameField
							) as StudentName, */
						$StudentNameField as StudentName,
						IF (ieei.YearTermID Is Null Or ieei.YearTermID = 0,
							Concat(ieei.EventTitle, ' (".$Lang['eEnrolment']['WholeYear'].")'),
							Concat(ieei.EventTitle, ' (', $SemesterNameField, ')') 
						) as Activity,
						IF(TRIM(iees.RoleTitle)='', '".$EmptySymbol."', iees.RoleTitle) as Role,
		                IF(iees.Performance IS NULL Or iees.Performance = '', '".$EmptySymbol."', iees.Performance) as Performance,
		                IF(iees.CommentStudent IS NULL Or iees.CommentStudent = '', '".$EmptySymbol."', iees.CommentStudent) as Comment 
					FROM INTRANET_ENROL_EVENTINFO as ieei
					$join_date_table 
					INNER Join INTRANET_ENROL_EVENTSTUDENT as iees On (iees.EnrolEventID = ieei.EnrolEventID)
					INNER Join INTRANET_USER as iu On (iees.StudentID = iu.UserID And iu.RecordType = 2) 
					/*Left Join INTRANET_ROLE as ir ON iug.RoleID = ir.RoleID */
					Left Join ACADEMIC_YEAR_TERM as ayt On (ieei.YearTermID = ayt.YearTermID) 
					LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=ieei.AcademicYearID  
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
					LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=ieei.AcademicYearID 
					LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
					WHERE 
					ieei.AcademicYearID = '".$selectYear."' 
					 $yearIdCond $classIdCond $studentIdCond $conds_YearTermIDArr $date_cond $record_type_cond
					GROUP BY ieei.EnrolEventID,iees.EventStudentID 
					ORDER BY yc.ClassTitleEN,ycu.ClassNumber+0,ieei.ActivityCode";
			$records = $this->returnArray($sql);
			//debug_r($sql);
			return $records;
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getActivitySummaryReportColumnArray()
		{
			global $Lang, $eEnrollment, $sys_custom;
			$column_list = array();
			// each entry is in format [FieldName,DisplayFieldName,EmptySymbol]
			$column_list[] = array('ActivityCode',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityCode'],null);
			$column_list[] = array('EventTitle',$eEnrollment['act_name'],null);
			$column_list[] = array('GroupTitle',$Lang['eEnrolment']['ActivitySummaryReport']['GroupOrClub'],null);
			$column_list[] = array('InternalOrExternal',$Lang['eEnrolment']['ActivitySummaryReport']['InternalOrExternal'],null);
			$column_list[] = array('ActivityExtLocation',$Lang['eEnrolment']['ExternalLocation'],null); // added on 2015-01-22
			$column_list[] = array('PersonInCharge',$Lang['eEnrolment']['ActivitySummaryReport']['PersonInCharge'],null);
			$column_list[] = array('SupportTeachers',$Lang['eEnrolment']['ActivitySummaryReport']['SupportTeachers'],null);
			$column_list[] = array('ActivityGoal',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityGoal'],null);
			$column_list[] = array('ActivityNature',$Lang['eEnrolment']['Nature'],null);
			$column_list[] = array('OLEType',$Lang['eEnrolment']['ActivitySummaryReport']['OLEType'],null);
			$column_list[] = array('Target',$eEnrollment['Target'],null);
			$column_list[] = array('IsVolunteerService',$Lang['eEnrolment']['VolunteerService'],null);
			//$column_list[] = array('ActivityStartDate',$Lang['eEnrolment']['StartDate'],null);
			//$column_list[] = array('ActivityEndDate',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityEndDate'],null);
			$column_list[] = array('ActivityDates',$Lang['eEnrolment']['ActivityDates'],null); // added on 2015-01-22
			$column_list[] = array('NumberOfActivity',$Lang['eEnrolment']['ActivitySummaryReport']['NumberOfActivity'],null);
			$column_list[] = array('NumberOfParticipants',$Lang['eEnrolment']['ActivitySummaryReport']['NumberOfParticipants'],null);
			$column_list[] = array('ActivityTotalTime',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityTotalHour'],null);
			
			if($sys_custom['eEnrolment']['TWGHCYMA']){
				$column_list[] = array('JointAct',$Lang['eEnrolment']['ActivitySummaryReport']['JointActivity'],'&nbsp;');
				$column_list[] = array('JointActType',$Lang['eEnrolment']['ActivitySummaryReport']['SchoolOrganizedActivity'].'/'.$Lang['eEnrolment']['ActivitySummaryReport']['ExternalOrganizationOrganizedActivity'],'&nbsp;');
				$column_list[] = array('JointActOrganiser',$Lang['eEnrolment']['ActivitySummaryReport']['Organiser'],'&nbsp;');
				$column_list[] = array('JointActOrganisedGroup',$Lang['eEnrolment']['ActivitySummaryReport']['OrganisedGroupOrganizationName'],'&nbsp;');
				$column_list[] = array('ActivityFee',$Lang['eEnrolment']['ActivityFee'],'&nbsp;'); // added on 2015-01-22
				$column_list[] = array('JointActAwarded',$Lang['eEnrolment']['ActivitySummaryReport']['AwardedInActivity'],'&nbsp;');
				$column_list[] = array('JointActAwardType',$Lang['eEnrolment']['ActivitySummaryReport']['AwardType'],'&nbsp;');
				$column_list[] = array('JointActChiAuthority',$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAuthorityName'],'&nbsp;');
				$column_list[] = array('JointActEngAuthority',$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAuthorityName'],'&nbsp;');
				$column_list[] = array('JointActChiAwardName',$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAwardName'],'&nbsp;');
				$column_list[] = array('JointActEngAwardName',$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAwardName'],'&nbsp;');
				$column_list[] = array('AwardWinners',$Lang['eEnrolment']['ActivitySummaryReport']['AwardWinners'],'&nbsp;');
			}
			
			return $column_list;
		}
	}
}
?>