<?php
// Editing by 
/*
 * 2015-12-10 (Carlos): Applied IntegerSafe() to integer type parameters to prevent SQL injection.
 */
if (!defined("LIBCAMPUSTV_DEFINED"))                     // Preprocessor directive
{
  define("LIBCAMPUSTV_DEFINED", true);

  class libcampustv extends libdb
  {

        # Settings
        var $setting_file;
        var $live_file;
        var $programme_file;
        var $publicAllowed;
        var $bulletinDisabled;
        var $pollingDisabled;
        var $LiveURL;
        var $programmeList;
		# Portal settings
        var $live_portal_file;
        var $live_width;
        var $live_height;
        var $live_auto_play;
        var $live_show;
                

        function libcampustv()
        {
                 global $intranet_root;
                 $this->libdb();
                 $this->setting_file = "$intranet_root/file/settings_campustv.txt";
                 $this->live_file = "$intranet_root/file/campustv_live.txt";
                 # Portal settings
                 $this->live_portal_file = "$intranet_root/file/campustv_portal.txt";
                 $this->programme_file = "$intranet_root/file/campustv_programme.txt";
                 if (is_file($this->setting_file))
                 {
                     $file_content = get_file_content($this->setting_file);
                     $data = explode("\n",$file_content);
                     list($this->publicAllowed,
                          $this->bulletinDisabled,
                          $this->pollingDisabled
                          ) = $data;
                 }
                 else
                 {
                     $this->publicAllowed = false;
                     $this->bulletinDisabled = false;
                     $this->pollingDisabled = false;
                 }
                 if (is_file($this->live_file))
                 {
                     $file_content = get_file_content($this->live_file);
                     $this->LiveURL = trim($file_content);
                 }
                 else
                 {
                     $this->LiveURL = "";
                 }

                 if (is_file($this->programme_file))
                 {
                     $file_content = get_file_content($this->programme_file);
                     $data = explode("\n",$file_content);
                     for ($i=0; $i<sizeof($data); $i++)
                     {
                          $list[] = explode(":::",$data[$i]);
                     }
                     $this->programmeList = $list;
                 }
                 else
                 {
                     $this->programmeList = array();
                 }
                 
                 if (is_file($this->live_portal_file))
                 {
                     $file_content = get_file_content($this->live_portal_file);
                     $data = explode("\n",$file_content);
                     list($this->live_show,
                     	  $this->live_width,
                          $this->live_height
                          ) = $data;
                 }
                 if ($this->live_width=="")
                 {
                 		$this->live_width = "320";
            	}
                 if ($this->live_height=="")
                 {
                 		$this->live_height = "250";
            	}
                 
        }
        function returnMaxChannelOrder()
        {
                 $sql = "SELECT MAX(DisplayOrder) FROM INTRANET_TV_CHANNEL";
                 $result = $this->returnVector($sql);
                 return ($result[0]+0);
        }
        function retrieveChannelInfo($ChannelID)
        {
        		 $ChannelID = IntegerSafe($ChannelID);
                 $sql = "SELECT Title, Description, DisplayOrder FROM INTRANET_TV_CHANNEL WHERE ChannelID = '$ChannelID'";
                 $result = $this->returnArray($sql,3);
                 return $result[0];
        }
        function retrieveClipInfo($ClipID)
        {
        		 $ClipID = IntegerSafe($ClipID);
                 $sql = "SELECT Title, Description,ClipURL, Recommended,isFile,UserID, RecordType, RecordStatus, IsSecure FROM INTRANET_TV_MOVIECLIP WHERE ClipID = '$ClipID'";
                 $result = $this->returnArray($sql,9);
                 return $result[0];
        }

        function returnChannels()
        {
                 $sql = "SELECT ChannelID, Title FROM INTRANET_TV_CHANNEL ORDER BY DisplayOrder";
                 return $this->returnArray($sql,2);
        }
        function returnClips($ChannelID="")
        {
        		 $ChannelID = IntegerSafe($ChannelID);
                 $conds = ($ChannelID==""?"":" AND RecordType = '$ChannelID' ");
                 $sql = "SELECT ClipID, Title, Description, Recommended
                         FROM INTRANET_TV_MOVIECLIP
                         WHERE RecordStatus = 2 $conds
                         ORDER BY Title";
                 return $this->returnArray($sql,4);
        }
        function returnRecommendList($num=10)
        {
        		 $num = IntegerSafe($num);
                 $sql = "SELECT a.ClipID,a.Title,a.Description,b.Title,a.ClipURL
                         FROM INTRANET_TV_MOVIECLIP as a
                              LEFT OUTER JOIN INTRANET_TV_CHANNEL as b ON a.RecordType = b.ChannelID
                              WHERE a.Recommended = 2
                              ORDER BY a.DateModified DESC
                              LIMIT 0,$num";
                 $result = $this->returnArray($sql,5);
                 return $result;
        }
        function returnAllClips()
        {
                 $sql = "SELECT ClipID, Title, ClipURL FROM INTRANET_TV_MOVIECLIP";
                 return $this->returnArray($sql,3);
        }

        # Return array with
        #              key = URL of clip
        #              array (times of view, duration of view)
        function getMovieStat()
        {
                 $log = $this->getLog();
                 $data = explode("\n",$log);
                 for ($i=0; $i<sizeof($data); $i++)
                 {
                      $line = $data[$i];
                      if ($line[0]=="#") continue;

                      $item = explode(" ",$line);
                      list($vip, $vdate, $vtime,$vdns,$vlink,$vstart,$vduration) = $item;
                      if ($vduration == "-") continue;
                      $inPos = in_array_col($vlink,$stat,0);
                      if ($inPos === false)
                      {
                          $stat[] = array($vlink,1,$vduration);
                      }
                      else
                      {
                          $stat[$inPos][1] = $stat[$inPos][1]+1;
                          $stat[$inPos][2] = $stat[$inPos][2]+$vduration;
                      }
                 }
                 return $stat;
        }

        # Get raw log file
        function getLog()
        {

                 global $tv_log_user,$tv_log_passwd,$tv_log_file, $tv_ftp_host;
                 global $tv_host_type;
                 $lftp = new libftp();

			    $lftp->host = $tv_ftp_host;
			    $lftp->host_type = ($tv_host_type==""? "Windows":$tv_host_type);
			
			    if (!$lftp->connect($tv_log_user,$tv_log_passwd))
			    {
			         return "";
			    }
			    $pwd = $lftp->pwd();
			    $log = $lftp->download2variable($pwd,$tv_log_file);
                 return $log;


                 /*
                 $path = "ftp://$tv_log_user:$tv_log_passwd@$tv_ftp_host/$tv_log_file";
                 echo $path;
                 $log = fopen($path,"r");
                 */
/*
                 $data = "#StartDate: 2003-09-24 02:42:35
#EndDate: YYYY-MM-DD HH:MM:SS
#TimeFormat: UTC
#EncodingFormat: UTF-8
#ServerName: vcast-server
#ServerIP: 10.45.224.70
#Software: Windows Media Services
#Version: 9.00.00.3372
#PublishingPoint: Video
#Fields: c-ip date time c-dns cs-uri-stem c-starttime x-duration ------------- (more fields) ----------------
192.168.98.3 2003-09-24 02:43:57 - /videotest/yy.wmv 0 3
192.168.98.3 2003-09-24 02:44:06 - /videotest/yy.wmv 0 1
192.168.98.3 2003-09-24 02:44:08 - /videotest/tcc.wmv 0 10
192.168.98.3 2003-09-24 02:44:33 - /videotest/tcc.wmv 0 1
218.103.24.100 2003-09-24 02:44:36 - /videotest/yy.wmv 0 6
218.103.24.100 2003-09-24 02:44:44 - /videotest/tcc.wmv 0 3
192.168.98.3 2003-09-24 02:44:47 - /videotest/yy.wmv 0 1
192.168.98.3 2003-09-24 02:44:49 - /videotest/tsaam.wmv 0 1
192.168.98.3 2003-09-24 02:44:52 - /videotest/islamic.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /videotest/wscpm.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /videotest/wscpm.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /videotest/wscpm.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /videotest/wscpm.wmv 0 2
218.103.24.100 2003-09-24 02:45:02 - /videotest/hsk.wmv 0 100
218.103.24.100 2003-09-24 02:45:02 - /videotest/wscpm.wmv 0 14 ";
return $data;
*/
/*
#For demo server
                 $data = "#StartDate: 2003-09-24 02:42:35
#EndDate: YYYY-MM-DD HH:MM:SS
#TimeFormat: UTC
#EncodingFormat: UTF-8
#ServerName: vcast-server
#ServerIP: 10.45.224.70
#Software: Windows Media Services
#Version: 9.00.00.3372
#PublishingPoint: Video
#Fields: c-ip date time c-dns cs-uri-stem c-starttime x-duration ------------- (more fields) ----------------
192.168.98.3 2003-09-24 02:43:57 - /broadlearn/b2e2df33/wsk.wmv 0 3
192.168.98.3 2003-09-24 02:44:06 - /broadlearn/b2e2df33/wsk.wmv 0 1
192.168.98.3 2003-09-24 02:44:08 - /broadlearn/b2e2df33/wsk.wmv 0 10
192.168.98.3 2003-09-24 02:44:33 - /broadlearn/4f818875/hsn.wmv 0 1
218.103.24.100 2003-09-24 02:44:36 - /broadlearn/4f818875/hsn.wmv 0 6
218.103.24.100 2003-09-24 02:44:44 - /broadlearn/4f818875/hsn.wmv 0 3
192.168.98.3 2003-09-24 02:44:47 - /broadlearn/4f818875/hsn.wmv 0 1
192.168.98.3 2003-09-24 02:44:49 - /broadlearn/4f818875/hsn.wmv 0 1
192.168.98.3 2003-09-24 02:44:52 - /broadlearn/7cc5fd2f/islamic.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /broadlearn/7cc5fd2f/islamic.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /broadlearn/7cc5fd2f/islamic.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /broadlearn/7cc5fd2f/islamic.wmv 0 2
192.168.98.3 2003-09-24 02:44:56 - /broadlearn/7cc5fd2f/islamic.wmv 0 2
218.103.24.100 2003-09-24 02:45:02 - /broadlearn/7cc5fd2f/islamic.wmv 0 100
218.103.24.100 2003-10-24 03:45:02 - /broadlearn/89d8337c/tcc.wmv 0 10
218.103.24.100 2003-10-24 04:45:02 - /broadlearn/89d8337c/tcc.wmv 0 140
218.103.24.100 2003-10-24 05:45:02 - /broadlearn/89d8337c/tcc.wmv 0 14
218.103.24.100 2003-10-24 06:45:02 - /broadlearn/89d8337c/tcc.wmv 0 12
218.103.24.100 2003-10-24 07:45:02 - /broadlearn/89d8337c/tcc.wmv 0 14
218.103.24.100 2003-10-24 10:45:02 - /broadlearn/89d8337c/tcc.wmv 0 14
218.103.24.100 2003-11-24 02:45:02 - /broadlearn/2e870e50/wscam.wmv 0 14
218.103.24.100 2003-11-24 05:45:02 - /broadlearn/2e870e50/wscam.wmv 0 14
218.103.24.100 2003-11-24 06:45:02 - /broadlearn/2e870e50/wscam.wmv 0 44
218.103.24.100 2003-11-24 07:45:02 - /broadlearn/2e870e50/wscam.wmv 0 74
218.103.24.100 2003-11-24 08:45:02 - /broadlearn/fead91dd/ccypm.wmv 0 14
218.103.24.100 2003-11-24 09:45:02 - /broadlearn/fead91dd/ccypm.wmv 0 14
218.103.24.100 2003-11-26 02:45:02 - /broadlearn/fead91dd/ccypm.wmv 0 14
218.103.24.100 2004-01-02 04:45:02 - /broadlearn/4360c3cb/yy.wmv 0 14
218.103.24.100 2004-01-02 05:45:02 - /broadlearn/4360c3cb/yy.wmv 0 14
218.103.24.100 2004-01-02 07:45:02 - /broadlearn/4360c3cb/yy.wmv 0 4
218.103.24.100 2004-01-02 09:45:02 - /broadlearn/c2472223/ccckl.wmv 0 14
218.103.24.100 2004-01-02 10:45:02 - /broadlearn/c2472223/ccckl.wmv 0 14
218.103.24.100 2004-01-02 12:45:02 - /broadlearn/3588f9b2/tmg.wmv 0 140
218.103.24.100 2004-01-02 22:45:02 - /broadlearn/3588f9b2/tmg.wmv 0 140
218.103.24.100 2004-01-12 02:45:02 - /broadlearn/3588f9b2/tmg.wmv 0 140
218.103.24.100 2004-01-14 02:45:02 - /broadlearn/91ef77d9/ccyam.wmv 0 104
218.103.24.100 2004-01-20 02:45:02 - /broadlearn/91ef77d9/ccyam.wmv 0 14
218.103.24.100 2004-01-24 02:45:02 - /broadlearn/91ef77d9/ccyam.wmv 0 14
";
return $data;
*/

        }

        function getTopHits($num=10)
        {
                 global $tv_webhost;
                 #$tv_webhost = "mms://video2.vcase.net";
                 $stat = $this->getMovieStat();
                 $sorted = sortByColumn($stat,1,0);                # Sort Times by descending
                 $count = 0;
                 $clips = $this->returnAllClips();
                 #print_r($stat);
                 #print_r($clips);
                 /*
                 while (list($key, $value) = each ($sorted))
                 */
                 for ($i=0; $i<sizeof($sorted); $i++)
                 {
                      $value = $sorted[$i];
                        list($vlink, $times, $duration) = $value;
                        $vlink = trim("$tv_webhost$vlink");
                        $pos = in_array_col($vlink,$clips,2);
                        if ($pos===false) continue;
                        list($clipID, $clipName, $clipURL) = $clips[$pos];
                        $result[] = array($clipID,$clipName,$times,$clipURL);
                        $count++;
                        if ($count>=$num) break;
                 }
                 return $result;
        }

        function getTopDuration($num=10)
        {
                 global $tv_webhost;
				 
				 $num = IntegerSafe($num);
                 # Create Temp table
                 $sql = "CREATE TEMPORARY TABLE TEMP_TV_DURATION
                         ( ChannelID int(11), Duration int(11) default 0 )";
                 $this->db_db_query($sql);
                 $sql = "INSERT INTO TEMP_TV_DURATION (ChannelID)
                         SELECT ChannelID FROM INTRANET_TV_CHANNEL
                         ";
                 $this->db_db_query($sql);

                 $stat = $this->getMovieStat();
                 for ($i=0; $i<sizeof($stat); $i++)
                 {
                        list($vlink, $times, $duration) = $stat[$i];
                        $vlink = "$tv_webhost$vlink";
                        $sql = "SELECT RecordType FROM INTRANET_TV_MOVIECLIP WHERE ClipURL = '$vlink'";
                        $channel = $this->returnVector($sql);
                        $channelID = $channel[0];
                        if ($channelID != "")
                        {
                            $sql = "UPDATE TEMP_TV_DURATION SET Duration = Duration + $duration WHERE ChannelID = '$channelID'";
                            $this->db_db_query($sql);
                        }
                 }
                 $sql = "SELECT a.ChannelID, a.Title, b.Duration FROM INTRANET_TV_CHANNEL as a
                         LEFT OUTER JOIN TEMP_TV_DURATION as b ON a.ChannelID = b.ChannelID
                         WHERE b.Duration <> 0
                         ORDER BY b.Duration DESC LIMIT 0,$num";
                 return $this->returnArray($sql,3);
        }
        function getCurrentPolls($ParUserID = "")
        {
        		 $ParUserID = IntegerSafe($ParUserID);
                 if ($ParUserID != "")
                 {
                     $sql = "SELECT a.PollingID, a.Question, a.DateStart, a.DateEnd, a.Reference
                             FROM INTRANET_TV_POLLING as a
                                  LEFT OUTER JOIN INTRANET_TV_POLLING_RESULT as b ON a.PollingID = b.PollingID AND b.UserID = '$ParUserID' 
                             WHERE a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()
                             AND b.PollingResultID IS NULL";
                 }
                 else
                 {
                     $sql = "SELECT a.PollingID, a.Question, a.DateStart, a.DateEnd, a.Reference
                             FROM INTRANET_TV_POLLING as a
                             WHERE a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()
                             ";
                 }
                 return $this->returnArray($sql,5);
        }
        function getPastPolls($ParUserID="")
        {
        		 $ParUserID = IntegerSafe($ParUserID);
                 if ($ParUserID != "")
                 {
                     $sql = "SELECT a.PollingID, a.Question, a.DateStart, a.DateEnd, a.Reference
                             FROM INTRANET_TV_POLLING as a
                                  LEFT OUTER JOIN INTRANET_TV_POLLING_RESULT as b ON a.PollingID = b.PollingID AND b.UserID = '$ParUserID' 
                             WHERE a.DateEnd <= CURDATE() OR b.PollingResultID IS NOT NULL";
                 }
                 else
                 {
                     $sql = "SELECT a.PollingID, a.Question, a.DateStart, a.DateEnd, a.Reference
                             FROM INTRANET_TV_POLLING as a
                             WHERE a.DateEnd <= CURDATE()
                             ";
                 }
                 return $this->returnArray($sql,5);
        }
        function returnPolling($PollingID,$ParUserID="")
        {
        		 $ParUserID = IntegerSafe($ParUserID);
        		 $PollingID = IntegerSafe($PollingID);
                 if ($ParUserID != "")
                 {
                     $conds = "AND b.UserID = '$ParUserID' ";
                 }
                 else
                 {}
                 $sql = "SELECT a.PollingID, a.Question, a.DateStart, a.DateEnd, a.Reference, b.AnswerOptionOrder
                         FROM INTRANET_TV_POLLING as a
                              LEFT OUTER JOIN INTRANET_TV_POLLING_RESULT as b ON a.PollingID = b.PollingID $conds
                         WHERE a.PollingID = '$PollingID'";
                 $result = $this->returnArray($sql,6);
                 if ($result[0][0]=="") return;

                 $sql = "SELECT a.OptionOrder, a.OptionText, b.ClipURL, b.Title
                         FROM INTRANET_TV_POLLING_OPTION as a
                              LEFT OUTER JOIN INTRANET_TV_MOVIECLIP as b ON a.OptionText = b.ClipID
                          WHERE a.PollingID = '$PollingID' ORDER BY a.OptionOrder";
                 $options = $this->returnArray($sql,4);
                 list ($id,$que,$start,$end,$ref,$answer) = $result[0];
                 return array($id,$que,$start,$end,$ref,$answer,$options);
        }
        function returnPollingResult($PollingID)
        {
        		 $PollingID = IntegerSafe($PollingID);
                 $sql = "SELECT a.OptionOrder, COUNT(b.PollingResultID), c.ClipURL, c.Title, c.ClipID
                         FROM INTRANET_TV_POLLING_OPTION as a
                              LEFT OUTER JOIN INTRANET_TV_POLLING_RESULT as b ON a.PollingID = b.PollingID AND a.OptionOrder = b.AnswerOptionOrder
                              LEFT OUTER JOIN INTRANET_TV_MOVIECLIP as c ON a.OptionText = c.ClipID
                         WHERE a.PollingID = '$PollingID' 
                         GROUP BY a.OptionOrder
                         ORDER BY a.OptionOrder";
                 return $this->returnArray($sql,5);
        }

  }
}        // End of directive
?>