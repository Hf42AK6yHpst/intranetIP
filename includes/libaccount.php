<?php
// Modifing by : 
/**
 * Log :
 * 
 *  2020-04-30	Henry
 * 		added is_access_system_security
 * 
 *  2019-04-30	Henry
 * 		Command injection fix
 * 
 *  2018-12-13	Henry
 * 		Modified the logic of is_access_eclass_backup [Case#W154185]
 * 
 *  2017-10-09	Paul
 * 		Added PaGamO setting with $plugin['PaGamO']
 * 
 *  2017-8-01	Ryan
 * 		Removed $plugin['HKSSF_CONNECTION'] checking on HKSSF Connection settings is_access_hkssf_connection 
 * 
 * 	2017-05-25	Paul
 * 		Added PowerPad Lite Connection
 * 		Deploy: ip.2.5.8.7.1
 * 
 *  	2016-11-24	Ryan
 * 		Added HKSSF Connection settings is_access_hkssf_connection $plugin['HKSSF_CONNECTION']
 * 
 * 	2014-10-08	YatWoon
 * 		Requested by Solution, check eClassBackup and eClassUpdate with flag (maybe override in lib.php)
 * 		Deploy: ip.2.5.5.10.1
 * 
 * 	2014-06-03	YatWoon
 * 		Requested by Tony, display the eClassBackup menu and they will add checking in the coding.
 * 
 *  2013-06-06  Jason
 * 		modify display_menu_setting() to hide the eLibrary setting in function setting
 * 
 *  2013-03-06	Carlos
 * 		add intranet_eclass_api_settings [80]
 * 
 *	2012-02-21	YatWoon
 *		hide "User Account Mgmt"
 *
 *	2011-03-04	YatWoon
 *		add checking of flag $plugin['DisableResourcesBookingOnRequest']
 *
 *	2010-09-02 Kelvin:
 *		modified display_menu_eclass change eclass image to special room image
 *	2010-08-24 YatWoon
 *		hide "User Info Setting" (move to front-end)
 *	
 * 	2010-04-14 Sandy : 
 * 		display_menu_intranet() & is_access_function() - reopen student promotion option (Case request) 
 */
 
class libaccount extends libfilesystem {

        var $Path;
        var $PathAuthUserFile;
        var $PathAuthGroupFile;
        var $is_access_email;
        var $is_access_password;
        var $is_access_account;
        var $is_access_url;
        var $is_access_campuslink;
        var $is_access_cycle;
        var $is_access_rbps;
        var $is_access_language;
        var $is_access_filesystem;
        var $is_access_user;
        //var $is_access_group;
        var $is_access_role;
        var $is_access_motd;
        //var $is_access_polling;
        //var $is_access_announcement;
        //var $is_access_event;
        var $is_access_campusmail;
        var $is_access_timetable;
        //var $is_access_groupbulletin;
        //var $is_access_groupfiles;
        //var $is_access_grouplinks;
        var $is_access_resource;
        var $is_access_booking;
        var $is_access_eclass;
        var $is_access_system_admin;
        var $is_access_system_configuration;
        var $is_access_system_files;
        var $is_access_account_mgmt;
        var $is_access_info_mgmt;
        var $is_access_resource_mgmt;
        //var $is_access_homework;
        var $is_access_campusmail_set;
        var $is_access_tmpfiles;
        var $is_access_resource_set;
        var $is_access_function_set;
        var $is_access_booking_periodic;
        var $is_access_basic_settings;
        var $is_access_campustv;
        var $is_access_plugins;
        var $is_access_library_file;
        var $is_access_qb;
        var $is_access_school_settings;
        var $is_access_wordtemplates;
        var $is_access_admin_mgmt;
        var $is_access_academic;
        var $is_access_attendance;
        var $is_access_merit;
        var $is_access_service;
        var $is_access_activity;
        var $is_access_award;
        var $is_access_assessment;
        var $is_access_usage;
        var $is_access_teaching;
        //var $is_access_groupcategory;
        //var $is_access_groupsettings;
        //var $is_access_groupmgmt;
        //var $is_access_groupfunction;
        var $is_access_clubs_enrollment;
        var $is_access_enrollment_approval;
        var $is_access_student_files;
        //var $is_access_notice;
        var $is_access_quota_mail;
        var $is_access_quota_file;
        var $is_access_sms;
        var $is_access_adminjob;
        //var $is_access_student_attendance;
        //var $is_access_student_promotion;
        var $is_access_websams;
        //var $is_access_payment;
        //var $is_access_circular;
        var $is_access_profile_set;
        //var $is_access_student_attendance2;
        //var $is_access_discipline;
        //var $is_access_sports;
        var $is_access_album;
		//var $is_access_user_info_settings;

        var $is_access_service_mgmt_system;
        var $is_access_qualied_starpicking;

        var $is_access_guardian;
        var $is_access_patrol_system;
        //var $is_access_inventory;
		var $is_access_student_registry;
		var $is_access_elibrary;
		var $is_access_award_scheme;

		var $is_access_lunchbox;
		var $is_access_eclass_update;
		var $is_access_eclass_backup;
		var $is_access_lslp;
		var $is_access_ksk;
		var $is_access_eclass_api_settings;
		var $is_access_hkssf_connection;
		var $is_access_pagamo;

        var $notUseShell;
		
		var $is_access_system_security;
		
        # ------------------------------------------------------------------------------------

        function libaccount(){
                global $intranet_root;
                $this->Path = "$intranet_root/file";
                $this->PathAuthUserFile = $this->Path."/.htpasswd";
                $this->PathAuthGroupFile = $this->Path."/.htgroup";
                $this->notUseShell = true;
        }

        # ------------------------------------------------------------------------------------

        function make_htpasswd(){ # make .htpasswd
                 $login = "admin";
                 $pass = "9sheungyuetroad";
                 if (!$this->notUseShell)
                 {
                      exec("htpasswd -bc ".$this->PathAuthUserFile." $login $pass");
                 }
                 else
                 {
                     $this->phpAddUser($login,$pass);
                 }
        }

        function make_htgroup(){ # make .htgroup
                $x = " intranet_email:
 intranet_password:
 intranet_account:
 intranet_url:
 intranet_campuslink:
 intranet_cycle:
 intranet_rbps:
 intranet_language:
 intranet_filesystem:
 intranet_user:
 intranet_group:
 intranet_role:
 intranet_motd:
 intranet_polling:
 intranet_announcement:
 intranet_event:
 intranet_campusmail:
 intranet_timetable:
 intranet_groupbulletin:
 intranet_groupfiles:
 intranet_grouplinks:
 intranet_resource:
 intranet_booking:
 intranet_eclass:
 intranet_homework:
 intranet_campusmail_set:
 intranet_tmpfiles:
 intranet_resource_set:
 intranet_basic_settings:
 intranet_campustv:
 intranet_booking_periodic:
 intranet_qb:
 intranet_school_settings:
 intranet_wordtemplates:
 intranet_attendance:
 intranet_merit:
 intranet_service:
 intranet_activity:
 intranet_award:
 intranet_assessment:
 intranet_usage:
 intranet_teaching:
 intranet_groupcategory:
 intranet_clubs_enrollment:
 intranet_enrollment_approval:
 intranet_student_files:
 intranet_notice:
 intranet_quota:
 intranet_sms:
 intranet_adminjob:
 intranet_quota_mail:
 intranet_quota_file:
 intranet_student_attendance:
 intranet_student_promotion:
 intranet_websams:
 intranet_payment:
 intranet_circular:
 intranet_profile_set:
 intranet_student_attendance2:
 intranet_discipline:
 intranet_sports:
 intranet_album:
 intranet_service_mgmt_system:
 intranet_qualied_star_picking:
 intranet_reportcard:
 intranet_guardian:
 intranet_patrol_system:
 intranet_inventory:
 intranet_student_registry:
 intranet_elibrary:
 intranet_user_info_settings:
 intranet_swimming_gala:
 intranet_lunchbox:
 intranet_file_quota:
 intranet_library:
 intranet_eclass_update:
 intranet_lslp:
 intranet_eclass_backup:
 intranet_award_scheme:
 intranet_ksk:
 intranet_eclass_api_settings:
 intranet_hkssf_connection:
";
                $this->file_write($x, $this->PathAuthGroupFile);
        }

        function phpAddUser($login,$password)
        {
                 $found = false;
                 $users = $this->return_users();
                 $password = crypt($password);
                 $str = "";
                 for ($i=0; $i<sizeof($users); $i++)
                 {
                      if ($login == $users[$i][0])
                      {
                          $str .= "$login:$password\n";
                          $found = true;
                      }
                      else
                      {
                          $str .= $users[$i][0]."".$users[$i][1]."\n";
                      }
                 }
                 if (!$found)
                 {
                      $str .= "$login:$password\n";
                 }
                 $this->file_write(trim($str),$this->PathAuthUserFile);
        }

        # ------------------------------------------------------------------------------------

        function return_htgroup(){
                return $this->file_read($this->PathAuthGroupFile);
        }

        function return_htpasswd(){
                return $this->file_read($this->PathAuthUserFile);
        }

        # ------------------------------------------------------------------------------------

        function return_username($x){
                return substr($x, 0, strpos($x,":"));
        }

        function return_password($x){
                return substr($x, strpos($x,":"));
        }

        function return_user_password($x){
	        $password = "";
	        $row = $this->return_users();
	        //debug_r($row);
	        for($i=0; $i<sizeof($row); $i++){
		        if($row[$i][0] == $x)
		        {
			       $password = $row[$i][1];
			       break;
		        }
	        }

	        return $password;
        }

        function return_users(){
                $users = explode("\n", $this->return_htpasswd());
                for($i=0; $i<sizeof($users); $i++){
                        if(strlen(trim($users[$i]))!=0){
                        $row[$i][0] = $this->return_username($users[$i]);
                        $row[$i][1] = $this->return_password($users[$i]);
                        }
                }
                return $row;
        }

        # ------------------------------------------------------------------------------------

        function return_groupname($x){
                return substr($x, 0, strpos($x,":"));
        }

        function return_groupuser($x){
                return substr($x, strpos($x,":"));
        }

        function return_rights(){
                $groups = explode("\n", $this->return_htgroup());
                for($i=0; $i<sizeof($groups); $i++){
                        if(strlen(trim($groups[$i]))!=0){
                        $row[$i][0] = $this->return_groupname($groups[$i]);
                        $row[$i][1] = $this->return_groupuser($groups[$i]);
                        }
                }
                return $row;
        }

        # ------------------------------------------------------------------------------------

        function add_user($AdminLogin, $AdminPassword, $rights){
        		$AdminLogin = OsCommandSafe($AdminLogin);
        		$AdminPassword = OsCommandSafe($AdminPassword);

	        	$OrgAdminPassword = $AdminPassword;
                 if($AdminPassword == "")
                 {
                   	//$AdminPassword = $this->return_password($AdminLogin);
                    $AdminPassword = $this->return_user_password($AdminLogin);
                 }

                 if (!$this->notUseShell)
                 {
                    exec("htpasswd -b ".$this->PathAuthUserFile." $AdminLogin $AdminPassword");
                 }
                 else
                 {
	                 if($OrgAdminPassword != "")
	                 {
                     	$this->phpAddUser($AdminLogin,$AdminPassword);
                 	 }
                 }

                $htgroup = $this->return_htgroup();
                $htgroup = str_replace (" $AdminLogin ", " ", $htgroup);
                for($i=0; $i<sizeof($rights); $i++)
                $htgroup = str_replace ("intranet_".$rights[$i].":", "intranet_".$rights[$i].": $AdminLogin ", $htgroup);
                $this->file_write($htgroup, $this->PathAuthGroupFile);
        }

        function del_user($AdminLogin){
                $htgroup = $this->return_htgroup();
                $htgroup = str_replace (" $AdminLogin ", " ", $htgroup);
                $this->file_write($htgroup, $this->PathAuthGroupFile);
                $row = $this->return_users();
                for($i=0; $i<sizeof($row); $i++)
                $htpasswd .= ($AdminLogin==$row[$i][0]) ? "" : $row[$i][0].$row[$i][1]."\n";
                $this->file_write($htpasswd, $this->PathAuthUserFile);
        }

        function update_user($OldPassword, $NewPassword){
                global $PHP_AUTH_USER, $PHP_AUTH_PW;
                
                $PHP_AUTH_USER = OsCommandSafe($PHP_AUTH_USER);
                $NewPassword = OsCommandSafe($NewPassword);
                
                if($PHP_AUTH_PW == $OldPassword){
                   if (!$this->notUseShell)
                   {
                        exec("htpasswd -b ".$this->PathAuthUserFile." $PHP_AUTH_USER $NewPassword");
                   }
                   else
                   {
                       $this->phpAddUser($PHP_AUTH_USER,$NewPassword);
                   }
                   return 1;

                }
                return 0;
        }

        function list_user(){
                global $button_select;
                $row = $this->return_users();
                $x .= "<select name=AdminLoginUser>\n";
                $x .= "<option value=''>- $button_select -</option>\n";
                for($i=1; $i<sizeof($row); $i++)
                $x .= "<option value=".$row[$i][0].">".$row[$i][0]."</option>\n";
                $x .= "</select>\n";
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function is_access($AdminLogin, $htgroupRightsStr){
                return ($AdminLogin=="admin") ? 1 : strpos(" $htgroupRightsStr ", " $AdminLogin ");
        }

        function is_access_function($AdminLogin){
                 global $plugin,$special_feature,$sys_custom;
                 global $personalfile_type,$personalfile_account_management,$webmail_SystemType;
                $htgroupRights = $this->return_rights();
                $this->is_access_email = $this->is_access($AdminLogin, $htgroupRights[0][1]);
                $this->is_access_password = $this->is_access($AdminLogin, $htgroupRights[1][1]);
                $this->is_access_system_security = $this->is_access($AdminLogin, $htgroupRights[0][1]);
                $this->is_access_account = $this->is_access($AdminLogin, $htgroupRights[2][1]);
                $this->is_access_url = $this->is_access($AdminLogin, $htgroupRights[3][1]);
                $this->is_access_campuslink = $this->is_access($AdminLogin, $htgroupRights[4][1]);
                $this->is_access_cycle = $this->is_access($AdminLogin, $htgroupRights[5][1]);
                $this->is_access_rbps = $this->is_access($AdminLogin, $htgroupRights[6][1]);
                $this->is_access_language = $this->is_access($AdminLogin, $htgroupRights[7][1]);
                $this->is_access_filesystem = $this->is_access($AdminLogin, $htgroupRights[8][1]);
                $this->is_access_user = $this->is_access($AdminLogin, $htgroupRights[9][1]);
                //$this->is_access_group = $this->is_access($AdminLogin, $htgroupRights[10][1]);
                $this->is_access_role = $this->is_access($AdminLogin, $htgroupRights[11][1]);
                $this->is_access_motd = $this->is_access($AdminLogin, $htgroupRights[12][1]);
                //$this->is_access_polling = $this->is_access($AdminLogin, $htgroupRights[13][1]);
                //$this->is_access_announcement = $this->is_access($AdminLogin, $htgroupRights[14][1]);
                //$this->is_access_event = $this->is_access($AdminLogin, $htgroupRights[15][1]);
                $this->is_access_campusmail = $this->is_access($AdminLogin, $htgroupRights[16][1]);
                $this->is_access_timetable = $this->is_access($AdminLogin, $htgroupRights[17][1]);
                //$this->is_access_groupbulletin = $this->is_access($AdminLogin, $htgroupRights[18][1]);
                //$this->is_access_groupfiles = $this->is_access($AdminLogin, $htgroupRights[19][1]);
                //$this->is_access_grouplinks = $this->is_access($AdminLogin, $htgroupRights[20][1]);
                $this->is_access_resource = !$plugin['DisableResourcesBookingOnRequest'] && $this->is_access($AdminLogin, $htgroupRights[21][1]);
                $this->is_access_booking = !$plugin['DisableResourcesBookingOnRequest'] && $this->is_access($AdminLogin, $htgroupRights[22][1]);
                $this->is_access_eclass = $this->is_access($AdminLogin, $htgroupRights[23][1]);
                //$this->is_access_homework = $this->is_access($AdminLogin, $htgroupRights[24][1]);
                /* no need for iMail Setting */
                $this->is_access_campusmail_set = $this->is_access($AdminLogin,$htgroupRights[25][1]);

                $this->is_access_tmpfiles = $this->is_access($AdminLogin, $htgroupRights[26][1]);
                $this->is_access_resource_set = !$plugin['DisableResourcesBookingOnRequest'] && $this->is_access($AdminLogin, $htgroupRights[27][1]);
                $this->is_access_basic_settings = $this->is_access($AdminLogin, $htgroupRights[28][1]);
                $this->is_access_campustv = $plugin['tv'] && $this->is_access($AdminLogin, $htgroupRights[29][1]);
                #$this->is_access_campustv = $plugin['tv'] && ($this->is_access($AdminLogin, $htgroupRights[29][1]) + $AdminLogin=='campustv');
                $this->is_access_booking_periodic = !$plugin['DisableResourcesBookingOnRequest'] && $this->is_access($AdminLogin, $htgroupRights[30][1]);
                $this->is_access_qb = $plugin['QB'] &&  $this->is_access($AdminLogin, $htgroupRights[31][1]);
                $this->is_access_school_settings = $this->is_access($AdminLogin, $htgroupRights[32][1]);
                $this->is_access_wordtemplates = $this->is_access($AdminLogin, $htgroupRights[33][1]);
                $this->is_access_attendance = $this->is_access($AdminLogin, $htgroupRights[34][1]);
                $this->is_access_merit = $this->is_access($AdminLogin, $htgroupRights[35][1]);
                $this->is_access_service = $this->is_access($AdminLogin, $htgroupRights[36][1]);
                $this->is_access_activity = $this->is_access($AdminLogin, $htgroupRights[37][1]);
                $this->is_access_award = $this->is_access($AdminLogin, $htgroupRights[38][1]);
                $this->is_access_assessment = $this->is_access($AdminLogin, $htgroupRights[39][1]);
                $this->is_access_usage = $this->is_access($AdminLogin, $htgroupRights[40][1]);
                /*
                # no need for IP25
                $this->is_access_teaching = $this->is_access($AdminLogin, $htgroupRights[41][1]);
                */
                //$this->is_access_groupcategory = $this->is_access($AdminLogin, $htgroupRights[42][1]);
                /* no need for IP25
                $this->is_access_clubs_enrollment = isset($plugin['enrollment']) && $plugin['enrollment'] && $this->is_access($AdminLogin, $htgroupRights[43][1]);
                */
                $this->is_access_enrollment_approval = isset($plugin['enrollment']) && $plugin['enrollment'] && $this->is_access($AdminLogin, $htgroupRights[44][1]);
                $this->is_access_student_files = $this->is_access($AdminLogin, $htgroupRights[45][1]);
                //$this->is_access_notice = isset($plugin['notice']) && $plugin['notice'] && $this->is_access($AdminLogin, $htgroupRights[46][1]);

                #$fileControlNeeded = (isset($plugin['personalfile']) && $plugin['personalfile'] && $personalfile_type == 'LOCAL_FTP' && $personalfile_account_management);
                #$mailControlNeeded = (isset($plugin['webmail']) && $plugin['webmail'] && ($webmail_SystemType==3 || $webmail_SystemType==3) );
                /*
                if ($fileControlNeeded || $mailControlNeeded)
                {
                    $this->is_access_quota = $this->is_access($AdminLogin, $htgroupRights[47][1]);
                }
                */
                $this->is_access_quota = 0;
                $this->is_access_sms = isset($plugin['sms']) && $plugin['sms'] && $this->is_access($AdminLogin, $htgroupRights[48][1]);
                
                /*
                # no need for IP25
                $this->is_access_adminjob = $this->is_access($AdminLogin, $htgroupRights[49][1]);
                */
                $this->is_access_quota_mail = 0; #(isset($plugin['webmail']) && $plugin['webmail'] && ($webmail_SystemType==3 || $webmail_SystemType==3) ) && $this->is_access($AdminLogin, $htgroupRights[50][1]);
                $this->is_access_quota_file = (isset($plugin['personalfile']) && $plugin['personalfile'] && $personalfile_account_management) && $this->is_access($AdminLogin, $htgroupRights[51][1]);
                //$this->is_access_student_attendance = (isset($plugin['attendancestudent']) && $plugin['attendancestudent']) && $this->is_access($AdminLogin, $htgroupRights[52][1]);
                
                ##2010-01-14 - open again
                $this->is_access_student_promotion = (isset($special_feature['studentpromotion']) && $special_feature['studentpromotion']) && $this->is_access($AdminLogin,$htgroupRights[53][1]);
                
                
                $this->is_access_websams = ( (isset($plugin['WebSAMS']) && $plugin['WebSAMS']) || (isset($plugin['iPortfolio']) && $plugin['iPortfolio']) ) && $this->is_access($AdminLogin,$htgroupRights[54][1]);
                //$this->is_access_payment = (isset($plugin['payment']) && $plugin['payment']) && $this->is_access($AdminLogin,$htgroupRights[55][1]);
                //$this->is_access_circular = (isset($special_feature['circular']) && $special_feature['circular']) && $this->is_access($AdminLogin,$htgroupRights[56][1]);
                $this->is_access_profile_set = (isset($plugin['studentprofile']) && $plugin['studentprofile']) && $this->is_access($AdminLogin,$htgroupRights[57][1]);
                //$this->is_access_student_attendance2 = (isset($plugin['attendancestudent']) && $plugin['attendancestudent']) && $this->is_access($AdminLogin, $htgroupRights[58][1]);
                //$this->is_access_discipline =((isset($plugin['Discipline']) && $plugin['Discipline']) || (isset($plugin['Disciplinev12']) && $plugin['Disciplinev12'])) && $this->is_access($AdminLogin, $htgroupRights[59][1]);
                //$this->is_access_sports = (isset($plugin['Sports']) && $plugin['Sports']) && $this->is_access($AdminLogin, $htgroupRights[60][1]);
                $this->is_access_album = $this->is_access($AdminLogin , $htgroupRights[61][1]);
                $this->is_access_service_mgmt_system = (isset($plugin['ServiceMgmt']) && $plugin['ServiceMgmt']) && $this->is_access($AdminLogin, $htgroupRights[62][1]);
                $this->is_access_qualied_starpicking = (isset($plugin['QualiEd_StarPicking']) && $plugin['QualiEd_StarPicking']) && $this->is_access($AdminLogin, $htgroupRights[63][1]);
                $this->is_access_reportcard =((isset($plugin['ReportCard']) && $plugin['ReportCard']) || (isset($plugin['ReportCard2008']) && $plugin['ReportCard2008'])) && $this->is_access($AdminLogin, $htgroupRights[64][1]);
                $this->is_access_guardian = $this->is_access($AdminLogin, $htgroupRights[65][1]);
                $this->is_access_patrol_system = (isset($plugin['PatrolSystem']) && $plugin['PatrolSystem']) && $this->is_access($AdminLogin, $htgroupRights[66][1]);
                //$this->is_access_inventory = (isset($plugin['Inventory']) && $plugin['Inventory']) && $this->is_access($AdminLogin, $htgroupRights[67][1]);
				$this->is_access_student_registry = (isset($plugin['StudentRegistry']) && $plugin['StudentRegistry']) && $this->is_access($AdminLogin, $htgroupRights[68][1]);
				//debug($this->is_access($AdminLogin, $htgroupRights[69][1]))
				$this->is_access_elibrary =(isset($plugin['eLib']) && $plugin['eLib']) && $this->is_access($AdminLogin, $htgroupRights[69][1]) && !$plugin['library_management_system'];

				//$this->is_access_user_info_settings = $this->is_access($AdminLogin, $htgroupRights[70][1]);
				
                
				$this->is_access_lunchbox = (isset($plugin['Lunchbox']) && $plugin['Lunchbox']) && $this->is_access($AdminLogin, $htgroupRights[72][1]);

				//$this->is_access_eclass_update = (isset($special_feature['eclass_update']) && $special_feature['eclass_update']);
				//$this->is_access_eclass_update = (isset($special_feature['eclass_update']) && $special_feature['eclass_update']) && $this->is_access($AdminLogin, $htgroupRights[75][1]);
				//$this->is_access_eclass_update = $this->is_access($AdminLogin, $htgroupRights[75][1]);		// 2014-06-03
				$this->is_access_eclass_update = ($special_feature['eclass_update']) && $this->is_access($AdminLogin, $htgroupRights[75][1]);
				$this->is_access_lslp = (isset($plugin['lslp']) && $plugin['lslp']) && $this->is_access($AdminLogin, $htgroupRights[76][1]);
				//$this->is_access_eclass_backup = (isset($special_feature['eclass_backup']) && $special_feature['eclass_backup']) && $this->is_access($AdminLogin, $htgroupRights[77][1]);
				$this->is_access_eclass_backup = ($special_feature['eclass_backup'] || get_client_region()=="zh_HK" && !isset($special_feature['eclass_backup'])) && $this->is_access($AdminLogin, $htgroupRights[77][1]);
				//$this->is_access_eclass_backup = $this->is_access($AdminLogin, $htgroupRights[77][1]);	# 2014-06-03
				$this->is_access_award_scheme = isset($sys_custom['chuen_yuen_award_scheme']) && $sys_custom['chuen_yuen_award_scheme'] && $this->is_access($AdminLogin, $htgroupRights[78][1]);
				$this->is_access_ksk = (isset($plugin['kskgsmath']) && $plugin['kskgsmath']);
				$this->is_access_eclass_api_settings = isset($plugin['eClassAPI']) && $plugin['eClassAPI'] && $this->is_access($AdminLogin, $htgroupRights[80][1]);
				$this->is_access_hkssf_connection = !isKIS() && $this->is_access($AdminLogin, $htgroupRights[81][1]);
				$this->is_access_powerpad_lite = isset($plugin['PowerPad_Lite']) && $plugin['PowerPad_Lite'] && ($plugin['PowerPad_Lite_Quota'] > 0) && ($plugin['PowerPad_Lite_Quota']!=99999);
				$this->is_access_pagamo = isset($plugin['PaGamO']) && $plugin['PaGamO'];
				
                ###########################
                # Access Group
                $this->is_access_academic = (isset($plugin['studentprofile']) && $plugin['studentprofile'] && ($this->is_access_attendance+$this->is_access_merit+$this->is_access_service+$this->is_access_activity+$this->is_access_award+$this->is_access_student_files));
                $this->is_access_library_file = $plugin['library'] && $this->is_access($AdminLogin, "");
                $this->is_access_system_admin = ($this->is_access_email + $this->is_access_password + $this->is_access_account);
                //$this->is_access_system_configuration = ($this->is_access_url + $this->is_access_campuslink + $this->is_access_cycle + $this->is_access_rbps + $this->is_access_language + $this->is_access_groupcategory + $this->is_access_teaching + $this->is_access_adminjob);
                $this->is_access_system_configuration = ($this->is_access_url + $this->is_access_campuslink + $this->is_access_cycle + $this->is_access_rbps + $this->is_access_language + $this->is_access_teaching + $this->is_access_adminjob);
                $this->is_access_system_files = ($this->is_access_filesystem);
                //$this->is_access_account_mgmt = ($this->is_access_user + $this->is_access_student_promotion + $this->is_access_usage);
                $this->is_access_account_mgmt = ($this->is_access_user + $this->is_access_usage);
                //$this->is_access_admin_mgmt = ($this->is_access_academic + $this->is_access_enrollment_approval + $this->is_access_event);
                $this->is_access_admin_mgmt = ($this->is_access_academic );
                //$this->is_access_info_mgmt = ($this->is_access_motd + $this->is_access_polling + $this->is_access_announcement + $this->is_access_campusmail + $this->is_access_album);
                $this->is_access_info_mgmt = $this->is_access_motd;
                $this->is_access_resource_mgmt = !$plugin['DisableResourcesBookingOnRequest'] && ($this->is_access_resource + $this->is_access_booking + $this->is_access_booking_periodic);
                //$this->is_access_function_set = ($this->is_access_homework + $this->is_access_campusmail_set + $this->is_access_resource_set + $this->is_access_notice + $this->is_access_clubs_enrollment + $this->is_access_quota + $this->is_access_circular);
                $this->is_access_function_set = ( $this->is_access_campusmail_set + $this->is_access_resource_set + $this->is_access_notice + $this->is_access_clubs_enrollment + $this->is_access_quota + $this->is_access_circular);
                //$this->is_access_plugins = ($this->is_access_library_file + $this->is_access_qb + $this->is_access_campustv + $this->is_access_sms + $this->is_access_student_attendance + $this->is_access_websams + $this->is_access_payment);
                $this->is_access_plugins = ($this->is_access_library_file + $this->is_access_qb + $this->is_access_campustv + $this->is_access_sms + $this->is_access_student_attendance + $this->is_access_websams + $this->is_access_payment  + $this->is_access_lunchbox);
                //$this->is_access_groupsettings = ($this->is_access_groupcategory + $this->is_access_role);
                //$this->is_access_groupfunction = ($this->is_access_timetable + $this->is_access_groupbulletin + $this->is_access_groupfiles + $this->is_access_grouplinks);
                //$this->is_access_groupmgmt = ($this->is_access_group + $this->is_access_groupfunction);
        }

        # ------------------------------------------------------------------------------------
        /*
        function display_menu($AdminLogin){
                global $i_adminmenu_usermgmt, $i_adminmenu_user, $i_adminmenu_group, $i_adminmenu_role, $i_adminmenu_infomgmt, $i_adminmenu_motd, $i_adminmenu_announcement, $i_adminmenu_event, $i_adminmenu_polling, $i_adminmenu_timetable, $i_adminmenu_resourcemgmt, $i_adminmenu_resource, $i_adminmenu_booking, $i_adminmenu_eclassmgmt, $i_adminmenu_eclass, $i_adminmenu_referencefiles, $i_adminmenu_sysmgmt, $i_adminmenu_email_setting, $i_adminmenu_admin_account, $i_adminmenu_rbps;
                $this->is_access_function($AdminLogin);
                $x .= ($this->is_access_menu_usermgmt) ? "<p><div class=head>$i_adminmenu_usermgmt</div>\n" : "";
                $x .= ($this->is_access_user) ? "<li><a href=/admin/user/>$i_adminmenu_user</a>\n" : "";
                $x .= ($this->is_access_group) ? "<li><a href=/admin/group/>$i_adminmenu_group</a>\n" : "";
                $x .= ($this->is_access_role) ? "<li><a href=/admin/role/>$i_adminmenu_role</a>\n" : "";
                $x .= ($this->is_access_menu_infomgmt) ? "<p><div class=head>$i_adminmenu_infomgmt</div>\n" : "";
                $x .= ($this->is_access_motd) ? "<li><a href=/admin/motd/>$i_adminmenu_motd</a>\n" : "";
                $x .= ($this->is_access_announcement) ? "<li><a href=/admin/announcement/>$i_adminmenu_announcement</a>\n" : "";
                $x .= ($this->is_access_event) ? "<li><a href=/admin/event/>$i_adminmenu_event</a>\n" : "";
                $x .= ($this->is_access_polling) ? "<li><a href=/admin/polling/>$i_adminmenu_polling</a>\n" : "";
                $x .= ($this->is_access_timetable) ? "<li><a href=/admin/timetable/>$i_adminmenu_timetable</a>\n" : "";
                $x .= ($this->is_access_menu_resourcemgmt) ? "<p><div class=head>$i_adminmenu_resourcemgmt</div>\n" : "";
                $x .= ($this->is_access_resource) ? "<li><a href=/admin/resource/>$i_adminmenu_resource</a>\n" : "";
                $x .= ($this->is_access_booking) ? "<li><a href=/admin/booking/>$i_adminmenu_booking</a>\n" : "";
                $x .= ($this->is_access_menu_eclass) ? "<p><div class=head>$i_adminmenu_eclassmgmt</div>\n" : "";
                $x .= ($this->is_access_eclass) ? "<li><a href=/admin/eclass/>$i_adminmenu_eclass</a>\n" : "";
                $x .= ($this->is_access_menu_filesystem) ? "<p><div class=head>$i_adminmenu_referencefiles</div>\n" : "";
                $x .= ($this->is_access_filesystem) ? "<li><a href=/admin/filesystem/>$i_adminmenu_referencefiles</a>\n" : "";
                $x .= ($this->is_access_menu_sysmgmt) ? "<p><div class=head>$i_adminmenu_sysmgmt</div>\n" : "";
                $x .= ($this->is_access_email) ? "<li><a href=/admin/email/>$i_adminmenu_email_setting</a>\n" : "";
                $x .= ($this->is_access_account) ? "<li><a href=/admin/account/>$i_adminmenu_admin_account</a>\n" : "";
                $x .= ($this->is_access_rbps) ? "<li><a href=/admin/rbps/>$i_adminmenu_rbps</a>\n" : "";
                return $x;
        }
        */
        # ------------------------------------------------------------------------------------

        function display_level2($title, $imageUsed, $link=""){
                global $image_path;

                                $x = ($link=="") ? "<tr><td colspan='2'><img src='$image_path/admin/menu/$imageUsed' border=0 width=175 alt='$title'></td></tr>\n" : "<tr><td><a href=\"$link\"<img src='$image_path/admin/menu/$imageUsed' border=0 width=175 alt='$title'></td></tr>\n";
                                $x .= "<tr><td colspan='2'><img src=$image_path/space.gif border=0 width=1 height=6></td></tr>\n";

                return $x;
        }

        function display_level3($title, $link=""){
                global $image_path;

                                $x = "<tr><td width='25' align='right'><img src='$image_path/admin/menu/menu_bullet.gif' border='0' width='22' height='13'></td>\n";
                                $x .= ($link=="") ? "<td width='150'><span class=admin_level3_link_new>$title</span></td></tr>\n" : "<td width='150'><a class=admin_level3_link_new href=$link>$title</a></td></tr>\n";
                                $x .= "<tr><td colspan='2'><img src=$image_path/space.gif border=0 width=1 height=10></td></tr>\n";

                return $x;
        }

        # ------------------------------------------------------------------------------------

        function display_menu_setting($AdminLogin){
                global $i_adminmenu_sa, $i_adminmenu_sa_email, $i_adminmenu_sa_password, $i_adminmenu_sa_helper, $i_adminmenu_sc, $i_adminmenu_sc_globalsetting, $i_adminmenu_sc_resourcebooking, $i_adminmenu_sc_campuslink, $i_adminmenu_sf, $i_admintitle_timetable, $i_admintitle_resource, $i_adminmenu_sc_url, $i_adminmenu_sc_cycle, $i_adminmenu_sc_period, $i_adminmenu_sc_language, $i_adminmenu_sc_webmail, $i_adminmenu_sc_campusmail,$i_adminmenu_sc_homework,$i_admintitle_tmpfiles;
                global $i_adminmenu_fs, $i_adminmenu_fs_homework, $i_adminmenu_fs_campusmail, $i_adminmenu_fs_resource_set,$i_adminmenu_sc_basic_settings,$i_adminmenu_sc_user_info_settings;
                global $plugin,$i_adminmenu_sc_campustv,$i_adminmenu_plugin,$i_adminmenu_plugin_sls_library,$i_adminmenu_plugin_qb;
                global $i_adminmenu_sc_school_settings,$i_adminmenu_sc_wordtemplates,$i_adminmenu_sc_group_settings,$i_adminmenu_sc_clubs_enrollment_settings;
                global $i_Notice_ElectronicNoticeSettings,$i_adminmenu_adm_teaching,$i_LinuxAccountQuotaSetting;
                global $i_LinuxAccount_Folder_QuotaSetting,$i_LinuxAccount_Webmail_QuotaSetting;
                global $i_SMS_SMS,$i_StudentAttendance_System,$i_WebSAMS_Integration,$i_Payment_System;
                global $i_adminmenu_adm_adminjob;
                global $intranet_session_language;
                global $i_CampusMail_New_iMail_Settings,$i_Circular_Settings,$i_Profile_settings,$i_Discipline_System,$i_Sports_System,$i_ServiceMgmt_System_Settings, $i_ReportCard_System, $i_eLibrary_System;
                global $i_QualiEd_StarPicking;
                global $special_feature, $i_Polling_Settings, $i_eNews_Settings;
                global $i_InventorySystem;
                global $intranet_version,$i_SmartCard_Lunchbox_System;
                global $i_Calendar_Admin_Setting;
                global $i_ChuenYuen_Award_Scheme,$ip20TopMenu, $Lang;

                $this->is_access_function($AdminLogin);
                $x .= "<table width=175 border=0 cellpadding=0 cellspacing=0>\n";
                if($this->is_access_system_admin) $x .= $this->display_level2($i_adminmenu_sa, "sys_admin_$intranet_session_language.gif");
                if($this->is_access_email) $x .= $this->display_level3($i_adminmenu_sa_email,"/admin/email/");
                if($this->is_access_password) $x .= $this->display_level3($i_adminmenu_sa_password,"/admin/password/");
                if($this->is_access_system_security) $x .= $this->display_level3($Lang['SysMgr']['Security']['Title'],"/admin/security/index.php");
                if($this->is_access_account) $x .= $this->display_level3($i_adminmenu_sa_helper,"/admin/account/");
                if($this->is_access_eclass_update) $x .= $this->display_level3($Lang['Header']['Admin']['eClassUpdate'],"/admin/eclassupdate/");
                if($this->is_access_eclass_backup) $x .= $this->display_level3($Lang['Header']['Admin']['eClassBackup'],"/admin/eclassbackup/");

                if($this->is_access_system_configuration) $x .= $this->display_level2($i_adminmenu_sc, "sys_setting_$intranet_session_language.gif");
                if($this->is_access_basic_settings) $x .= $this->display_level3($i_adminmenu_sc_basic_settings,"/admin/basic_settings/");
                if($this->is_access_school_settings) $x .= $this->display_level3($i_adminmenu_sc_school_settings,"/admin/school_settings/");
                if($this->is_access_teaching) $x .= $this->display_level3($i_adminmenu_adm_teaching,"/admin/teaching/");
                if($this->is_access_adminjob) $x .= $this->display_level3($i_adminmenu_adm_adminjob,"/admin/adminjob/");
                //if($this->is_access_groupsettings) $x .= $this->display_level3($i_adminmenu_sc_group_settings,"/admin/group_settings/");
//                 if($this->is_access_user_info_settings) $x .= $this->display_level3($i_adminmenu_sc_user_info_settings,"/admin/user_info_settings/");
                // if($this->is_access_url) $x .= $this->display_level3($i_adminmenu_sc_url,"/admin/url/");
                #if($this->is_access_cycle) $x .= $this->display_level3($i_adminmenu_sc_cycle,"/admin/cycle/");
                #if($this->is_access_rbps) $x .= $this->display_level3($i_adminmenu_sc_period,"/admin/rbps/");
                // if($this->is_access_language) $x .= $this->display_level3($i_adminmenu_sc_language,"/admin/language/");
                #if($this->is_access_wordtemplates) $x .= $this->display_level3($i_adminmenu_sc_wordtemplates,"/admin/wordtemplates/");
                if($this->is_access_function_set) $x .= $this->display_level2($i_adminmenu_fs, "sys_function_setting_$intranet_session_language.gif");
                if($this->is_access_profile_set) $x .= $this->display_level3($i_Profile_settings, "/admin/profile_set/");
                //if($this->is_access_homework) $x .= $this->display_level3($i_adminmenu_fs_homework,"/admin/homework/");

                if ($special_feature['imail'])
                    $mail_set_title = $i_CampusMail_New_iMail_Settings;
                else $mail_set_title = $i_adminmenu_fs_campusmail;
                if($this->is_access_campusmail_set) $x .= $this->display_level3($mail_set_title,"/admin/campusmail_set/");
                ######### Temp menu item for iCalendar ##########
                #$x .= $this->display_level3($i_Calendar_Admin_Setting,"/admin/icalendar/");
                if($this->is_access_clubs_enrollment) $x .= $this->display_level3($i_adminmenu_sc_clubs_enrollment_settings,"/admin/clubs_enrollment/");
                //if($this->is_access_notice) $x .= $this->display_level3($i_Notice_ElectronicNoticeSettings,"/admin/notice/");

				//if($this->is_access_polling) $x .= $this->display_level3($i_Polling_Settings,"/admin/polling/settings.php");
				//if($this->is_access_announcement) $x .= $this->display_level3($i_eNews_Settings,"/admin/announcement/settings.php");

                #if($this->is_access_quota) $x .= $this->display_level3($i_LinuxAccountQuotaSetting,"/admin/quota/");
                #if($this->is_access_quota_mail) $x .= $this->display_level3($i_LinuxAccount_Webmail_QuotaSetting,"/admin/mail_quota/");
                if($this->is_access_quota_file) $x .= $this->display_level3($i_LinuxAccount_Folder_QuotaSetting,"/admin/file_quota/");
                if($this->is_access_resource_set) $x .= $this->display_level3($i_adminmenu_fs_resource_set,"/admin/resource_set/");
                //if($this->is_access_circular) $x .= $this->display_level3($i_Circular_Settings, "/admin/circular/");
                //if($this->is_access_discipline) $x .= $this->display_level3($i_Discipline_System, "/admin/discipline/");
                //if($this->is_access_reportcard) $x .= $this->display_level3($i_ReportCard_System, "/admin/reportcard/");

                if($this->is_access_elibrary) $x .= $this->display_level3($i_eLibrary_System, "/admin/elibrary/");
                if($this->is_access_lslp)
                {
	                global $intranet_root;
	                include_once($intranet_root."/includes/libdb.php");
	                include_once($intranet_root."/includes/liblslp.php");
	                $ls = new lslp();
	                if($ls->isInLicencePeriod())
	                {
	                	$x .= $this->display_level3($ip20TopMenu['LSLP'], "/admin/lslp/");
                	}
	            }
				if($this->is_access_ksk) $x .= $this->display_level3($Lang['kskgsmath']['kskgsmath'], "/admin/ksk/");
				if($this->is_access_powerpad_lite) $x .= $this->display_level3("PowerPad Lite","/admin/powerpad_lite/");
                //if($this->is_access_sports) $x .= $this->display_level3($i_Sports_System, "/admin/sports/");
                if($this->is_access_service_mgmt_system) $x .= $this->display_level3($i_ServiceMgmt_System_Settings, "/admin/service_mgmt/");
                if($this->is_access_qualied_starpicking) $x .= $this->display_level3($i_QualiEd_StarPicking, "/admin/qualied_starpicking/");
                //if($this->is_access_inventory) $x .= $this->display_level3($i_InventorySystem['InventorySystemSetting'], "/admin/inventory/");
                if($this->is_access_award_scheme) $x .= $this->display_level3($i_ChuenYuen_Award_Scheme, "/admin/award_scheme/settings.php");

                if($this->is_access_plugins) $x .= $this->display_level2($i_adminmenu_plugin, "sys_integrate_$intranet_session_language.gif");
                if($this->is_access_qb) $x .= $this->display_level3($i_adminmenu_plugin_qb,"/admin/qb/");
                if($this->is_access_library_file) $x .= $this->display_level3($i_adminmenu_plugin_sls_library,"/admin/library/");
                if($this->is_access_campustv) $x .= $this->display_level3($i_adminmenu_sc_campustv,"/admin/campustv/");
                if($this->is_access_sms) $x .= $this->display_level3($i_SMS_SMS,"/admin/sms/");
                //if($this->is_access_student_attendance2) $x .= $this->display_level3($i_StudentAttendance_System,"/admin/student_attendance2/");
                #if($this->is_access_student_attendance) $x .= $this->display_level3("(old)".$i_StudentAttendance_System,"/admin/student_attendance/");
                //if($this->is_access_payment) $x .= $this->display_level3($i_Payment_System,"/admin/payment/");
                if($this->is_access_websams) $x .= $this->display_level3($i_WebSAMS_Integration,"/admin/websams/attendance_code/");
                if($this->is_access_lunchbox) $x .= $this->display_level3($i_SmartCard_Lunchbox_System,"/admin/lunchbox/");
                if($this->is_access_patrol_system) $x .= $this->display_level3($i_SmartCard_Patrol_System['Patrol_System'],"/admin/patrol_system/");
				if($this->is_access_eclass_api_settings) $x .= $this->display_level3($Lang['eClassAPI']['eClassApiSettings'],"/admin/eclass_api_settings/");
				if($this->is_access_hkssf_connection) $x .= $this->display_level3($Lang['HKSSF']['module'],"/admin/hkssf_connection/");
				if($this->is_access_pagamo) $x .= $this->display_level3($Lang['PaGamO']['admin_module'],"/admin/pagamo/");
                                                
                $x .= "</table>\n";
                return $x;
                # $x .= $this->display_level3($i_adminmenu_sc_campusmail);
                # $x .= $this->display_level3($i_adminmenu_sc_webmail);
        }

        function display_menu_intranet($AdminLogin){
                global $i_adminmenu_am, $i_adminmenu_am_user, $i_adminmenu_im, $i_adminmenu_im_motd, $i_adminmenu_im_announcement, $i_adminmenu_im_event, $i_adminmenu_im_timetable, $i_adminmenu_im_campusmail, $i_adminmenu_im_polling, $i_adminmenu_rm, $i_adminmenu_rm_item, $i_adminmenu_rm_record, $i_adminmenu_us, $i_adminmenu_us_user, $i_adminmenu_us_group, $i_adminmenu_im_group_bulletin, $i_adminmenu_im_group_links, $i_adminmenu_im_group_files,$i_adminmenu_rm_record_periodic;
                global $i_adminmenu_adm,$i_adminmenu_adm_academic_record,$i_adminmenu_am_usage,$i_adminmenu_adm_teaching,$i_adminmenu_adm_enrollment_approval;
                global $i_adminmenu_gm,$i_adminmenu_gm_group,$i_adminmenu_gm_groupfunction;
                global $i_adminmenu_sf,$i_admintitle_resource,$i_admintitle_tmpfiles;
                global $i_StudentGuardian;
                global $i_StudentPromotion;
 				global $i_StudentRegistry;
                global $intranet_session_language;
                $this->is_access_function($AdminLogin);
                $x .= "<table width=163 border=0 cellpadding=0 cellspacing=0>\n";
                if($this->is_access_account_mgmt) $x .= $this->display_level2($i_adminmenu_am, "mgt_account_$intranet_session_language.gif");
                
                ### Hide user management centre [YatWoon]
//                 if($this->is_access_user) $x .= $this->display_level3($i_adminmenu_am_user,"/admin/user/");
                if($this->is_access_usage) $x .= $this->display_level3($i_adminmenu_am_usage,"/admin/usage/");
                
                ##2010-04-12 - Open again
                if($this->is_access_student_promotion) $x .= $this->display_level3($i_StudentPromotion,"/admin/student_promotion/");
                
                
//                if($this->is_access_role) $x .= $this->display_level3($i_adminmenu_am_role,"/admin/role/");
                if($this->is_access_admin_mgmt) $x .= $this->display_level2($i_adminmenu_adm, "mgt_admin_$intranet_session_language.gif");
                //if($this->is_access_event) $x .= $this->display_level3($i_adminmenu_im_event,"/admin/event/");
                //if($this->is_access_info_mgmt) $x .= $this->display_level3($i_adminmenu_im, "/admin/info/");
                if($this->is_access_academic) $x .= $this->display_level3($i_adminmenu_adm_academic_record,"/admin/academic/");
                if($this->is_access_guardian) $x .= $this->display_level3($i_StudentGuardian['MenuInfo'],"/admin/guardian/");
                //if($this->is_access_enrollment_approval) $x .= $this->display_level3($i_adminmenu_adm_enrollment_approval,"/admin/enrollment_approval/");
				if($this->is_access_student_registry) $x .= $this->display_level3($i_StudentRegistry['System'],"/admin/student_registry/");

				/*
                if($this->is_access_groupmgmt) $x .= $this->display_level2($i_adminmenu_gm, "mgt_group_$intranet_session_language.gif");
                if($this->is_access_group) $x .= $this->display_level3($i_adminmenu_gm_group,"/admin/group/");
                if($this->is_access_groupfunction) $x .= $this->display_level3($i_adminmenu_gm_groupfunction,"/admin/groupfunction/");
                */

//                if($this->is_access_motd) $x .= $this->display_level3($i_adminmenu_im_motd,"/admin/motd/");
                
                /*
                if($this->is_access_motd) $x .= $this->display_level3($i_adminmenu_im_motd,"/admin/motd/");
                if($this->is_access_polling) $x .= $this->display_level3($i_adminmenu_im_polling,"/admin/polling/");
                if($this->is_access_announcement) $x .= $this->display_level3($i_adminmenu_im_announcement,"/admin/announcement/");
                if($this->is_access_campusmail) $x .= $this->display_level3($i_adminmenu_im_campusmail,"/admin/campusmail/");
                */
#                if($this->is_access_timetable) $x .= $this->display_level3($i_adminmenu_im_timetable,"/admin/timetable/");
#                if($this->is_access_groupbulletin) $x .= $this->display_level3($i_adminmenu_im_group_bulletin,"/admin/groupbulletin/");
#                if($this->is_access_groupfiles) $x .= $this->display_level3($i_adminmenu_im_group_links,"/admin/grouplinks/");
#                if($this->is_access_grouplinks) $x .= $this->display_level3($i_adminmenu_im_group_files,"/admin/groupfiles/");
                if($this->is_access_resource_mgmt) $x .= $this->display_level2($i_adminmenu_rm, "mgt_resource_$intranet_session_language.gif");
                if($this->is_access_resource) $x .= $this->display_level3($i_adminmenu_rm_item,"/admin/resource/");
                if($this->is_access_booking) $x .= $this->display_level3($i_adminmenu_rm_record,"/admin/booking/");
                if($this->is_access_booking_periodic) $x .= $this->display_level3($i_adminmenu_rm_record_periodic,"/admin/booking_periodic/");
                # $x .= $this->display_level2($i_adminmenu_us);
                # $x .= $this->display_level3($i_adminmenu_us_user);
                # $x .= $this->display_level3($i_adminmenu_us_group);
                if($this->is_access_system_files) $x .= $this->display_level2($i_adminmenu_sf, "mgt_sys_file_$intranet_session_language.gif");
                //if($this->is_access_filesystem) $x .= $this->display_level3($i_admintitle_timetable,"/admin/filesystem/?folderID=0");
                if($this->is_access_filesystem) $x .= $this->display_level3($i_admintitle_resource,"/admin/filesystem/?folderID=1");
                if($this->is_access_tmpfiles) $x .= $this->display_level3($i_admintitle_tmpfiles,"/admin/tmpfiles/");
                $x .= "</table>\n";
                return $x;
        }
/*
        function display_menu_eclass($AdminLogin){
                 global $intranet_session_language;
                 global $i_adminmenu_sf,$i_admintitle_resource;
                 global $i_eClass_Admin_MgmtCenter,$i_eClass_Admin_Settings,$i_eClass_Admin_Stats;
                 $this->is_access_function($AdminLogin);

                 if ($this->is_access_eclass)
                 {
                     $x .= $this->display_level2($i_adminmenu_sf, "mgt_sys_file_$intranet_session_language.gif");
                     $x .= $this->display_level3($i_eClass_Admin_MgmtCenter,"/admin/eclass/");
                     $x .= $this->display_level3($i_eClass_Admin_Settings,"/admin/eclass/settings/");
                     $x .= $this->display_level3($i_eClass_Admin_Stats,"\"javascript:alert('Not Implemented')\"");
                 }
                 return $x;
        }
*/
        function display_menu_eclass($AdminLogin){
                 global $intranet_session_language;
                 global $i_adminmenu_sf,$i_admintitle_resource;
                 global $i_eClass_Admin_MgmtCenter,$i_eClass_Admin_Settings,$i_eClass_Admin_Stats, $i_eClass_Admin_Shared_Files, $eclass_lite_license_only;
                 $this->is_access_function($AdminLogin);

                 if ($this->is_access_eclass)
                 {
                     //$x .= $this->display_level2($i_adminmenu_sf, "eclass_$intranet_session_language.gif");
                     $x .= $this->display_level2($i_adminmenu_sf, "specialroom_$intranet_session_language.gif");
                     $x .= $this->display_level3($i_eClass_Admin_MgmtCenter,"/admin/eclass/");
                     /*
                     if (!$eclass_lite_license_only)
                     {
                             $x .= $this->display_level3($i_eClass_Admin_Settings,"/admin/eclass/settings/");
                             $x .= $this->display_level3($i_eClass_Admin_Stats,"/admin/eclass/stats/");
//                             $x .= $this->display_level3($i_eClass_Admin_Shared_Files,"/admin/eclass/shared_files/");
                     }
                     */
                 }
                 return $x;
        }

        function display_menu_network($AdminLogin){
                $this->is_access_function($AdminLogin);
                return $x;
        }

        # ------------------------------------------------------------------------------------

}
?>