<?php
// using:
/*
 * Change Log:
 */
 
if (!defined("LIBDOORACCESS_FACTORY_DEFINED")) {
	define("LIBDOORACCESS_FACTORY_DEFINED", true);
	
	class libdooraccess_factory {
		public function __construct() {
			
	    }
	   
	    public static function createObject() {
	    	global $PATH_WRT_ROOT;
	    	
	    	// include config file before initiating the class
	    	$door_access = array();
			$doorAccessConfigFile = $PATH_WRT_ROOT.'plugins/door_access_conf.php'; 
			if (file_exists($doorAccessConfigFile)) {
				include($doorAccessConfigFile);
			}
	    	
	    	$providerName = strtolower($door_access['provider']);
	    	$objectClassName = 'libdooraccess_'.$providerName;
	    	$libFile = $PATH_WRT_ROOT.'includes/'.$objectClassName.'.php';
	    	
			if (file_exists($libFile)) {
				include_once($libFile);
				return new $objectClassName($door_access['host'], $door_access['port'], $door_access['api_folder'], $door_access['key']);
			}
			else {
				return null;
			}
	    }
	}
}
?>