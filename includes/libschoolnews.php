<?php
# using: 

#########################################
#
#   Date    2019-06-06 Cameron
#           add eService > School News in GET_MODULE_OBJ_ARR() [case #P161545]
#
#   Date    2018-11-12  Philips     [2018-1105-1041-02206]
#           modified returnEmailNotificationData(), added __TITLE__ in email subject
#
#	Date	2017-07-31 Carlos
#			modified returnPushMessageNotificationData(), replace __TITLE__ at title text for DHL.
#
#	Date	2016-11-25 Villa
#			modified returnEmailNotificationData() - add space after the url link  
#
#	Date	2016-09-23 (Villa)
#			modifly	libschoolnews() loadDefaultSettings - add push msg
#			add to module setting
#			-"default using eclass App to notify"
#
#	Date:	2015-10-27 (Ivan) [P87700] [ip.2.5.7.1.1]
#			added function loadDefaultSettings()
#			added allowUserToViewPastNews settings logic
#
#	Date:	2014-10-14	Roy
#			add function returnPushMessageNotificationData(), return push message title and content
#
#	Date:	2011-03-25	YatWoon
#			add function returnEmailNotificationData(), return notification email subject & content
#
#	Date:	2011-03-11 (Henry Chow)
#			modified GET_MODULE_OBJ_ARR(), allow System Admin can view menu
#
#	Date:	2010-11-03	YatWoon
#			Add School News > Settings
#
#########################################

	class libschoolnews extends libdb{
		
		var $defaultNumDays;
	
		function libschoolnews(){
// 			$this->libdb();
		global $intranet_root,$intranet_version,$_SESSION;
             
             $this->libdb();
             $this->ModuleName = "SchoolNews";

			 $this->loadDefaultSettings();
             if (!isset($_SESSION["SSV_PRIVILEGE"]["SchoolNews"]))
         	{
				include_once("libgeneralsettings.php");
				$lgeneralsettings = new libgeneralsettings();
				
				$settings_ary = $lgeneralsettings->Get_General_Setting($this->ModuleName);
				
				if(!empty($settings_ary))
				{
					foreach($settings_ary as $key=>$data)
					{
						$_SESSION["SSV_PRIVILEGE"]["SchoolNews"][$key] = $data;
						$this->$key = $data; 
					}
				}
//				else
//				{
//                 	$this->defaultNumDays = 0;
//				}
			}
			else
			{
				$this->defaultNumDays = $_SESSION["SSV_PRIVILEGE"]["SchoolNews"]["defaultNumDays"];
				if (isset($_SESSION["SSV_PRIVILEGE"]["SchoolNews"]["allowUserToViewPastNews"])) {
					$this->allowUserToViewPastNews = $_SESSION["SSV_PRIVILEGE"]["SchoolNews"]["allowUserToViewPastNews"];
				}
				if (isset($_SESSION["SSV_PRIVILEGE"]["SchoolNews"]["sendPushMsg"])) {
					$this->sendPushMsg = $_SESSION["SSV_PRIVILEGE"]["SchoolNews"]["sendPushMsg"];
				}
				
             }
		}
		
		function loadDefaultSettings() {
			$this->defaultNumDays = 0;
			$this->allowUserToViewPastNews = 1;
			$this->sendPushMsg = 0;
		}
		
        /* Get MODULE_OBJ array
        */
        function GET_MODULE_OBJ_ARR()
        {
                global $UserID, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root;
                
                ### wordings 
                global  $Lang;
                
                # Current Page Information init
                $PageSchoolNews = 0;
                $PageSettings = 0;
                
                switch ($CurrentPage) {
                    case "SchoolNews":
                    	$PageSchoolNews = 1;
                    	break;
                    case "SchoolNewsSettings":
                    	$PageSettings = 1;
                    	break;
                    case "eServiceSchoolNews":
                        $PageSchoolNews = 1;
                        break;
                }
                
                if($CurrentPageArr['schoolNews'])
                {
	                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/schoolnews/";
	                
					if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION['SSV_USER_ACCESS']['other-schoolNews'])
					{	
						$MenuArr["schoolNews"] 	= array($Lang['SysMgr']['SchoolNews']['SchoolNews'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/schoolnews/index.php", $PageSchoolNews);
						$MenuArr["Settings"] 	= array($Lang['SysMgr']['SchoolNews']['Settings'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/schoolnews/settings/index.php", $PageSettings);
					}
				}
				
				else if ($CurrentPageArr['eServiceSchoolNews']) {
				    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/schoolnews/";
				    if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
			            $MenuArr["schoolNews"] 	= array($Lang['SysMgr']['SchoolNews']['SchoolNews'], $PATH_WRT_ROOT."home/eService/schoolnews/index.php", $PageSchoolNews);
				    }
				}

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass School News";'."\n";
            $js.= '</script>'."\n";

                ### module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['schoolNews'].$js;
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eoffice.gif";
                $MODULE_OBJ['menu'] = $MenuArr;
				
                return $MODULE_OBJ;
        }
		
		function getStatus($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($id, $name) = $result[$i];
				$selected_str = ($id==$selected? "SELECTED":"");
				$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}
		
		function returnPushMessageNotificationData($date, $title, $type='', $groups='')
		{
			global $Lang;

	        $website = get_website();
	        $webmaster = get_webmaster();
	        
//	        $email_subject = $Lang['AppNotifyMessage']['SchoolNews']['Subject'];
//	        $email_subject = str_replace("__TYPE__", $type, $email_subject);
//	        $email_subject = str_replace("__DATE__", $date, $email_subject);
	        
	        if(empty($groups))
	        {
	        	$email_subject = $Lang['AppNotifyMessage']['SchoolNews']['Title']['SCHOOL'];
		        $email_content = $Lang['AppNotifyMessage']['SchoolNews']['Content']['SCHOOL'];
	        }
	        else
	        {
	        	$email_subject = $Lang['AppNotifyMessage']['SchoolNews']['Title']['GROUP'];
	        	$email_content = $Lang['AppNotifyMessage']['SchoolNews']['Content']['GROUP'];
	        	$list = implode(",",$groups);
	        	$email_content = str_replace("__LIST__", $list, $email_content);
        	}
        	
        	$email_subject = str_replace("__TITLE__", $title, $email_subject);
	        $email_content = str_replace("__TITLE__", $title, $email_content);
	        $email_content = str_replace("__DATE__", $date, $email_content);
	        $email_content = str_replace("__WEBSITE__", $website, $email_content);
	        
			return array($email_subject, $email_content);
		}
		
		function returnEmailNotificationData($date, $title, $type='', $groups='')
        {
	        global $Lang;

	        $website = get_website();
	        $webmaster = get_webmaster();
	        
	        $email_subject = $Lang['EmailNotification']['SchoolNews']['Subject'];
	        $email_subject = str_replace("__TYPE__", $type, $email_subject);
	        $email_subject = str_replace("__DATE__", $date, $email_subject);
	        $email_subject = str_replace("__TITLE__", $title, $email_subject);
	        
	        if(empty($groups))
	        {
		        $email_content = $Lang['EmailNotification']['SchoolNews']['Content']['SCHOOL'];
	        }
	        else
	        {
	        	$email_content = $Lang['EmailNotification']['SchoolNews']['Content']['GROUP'];
	        	$list = implode(",",$groups);
	        	$email_content = str_replace("__LIST__", $list, $email_content);
        	}
        	
	        $email_content = str_replace("__TITLE__", $title, $email_content);
	        $email_content = str_replace("__DATE__", $date, $email_content);
	        $email_content = str_replace("__WEBSITE__", $website."  ", $email_content);
	        
	        $email_footer = $Lang['EmailNotification']['Footer'];
			$email_footer = str_replace("__WEBSITE__", $website."  ", $email_footer);
			$email_footer = str_replace("__WEBMASTER__", $webmaster."  ", $email_footer);
	        $email_content .= $email_footer;
	        
			return array($email_subject, $email_content);
        }
	}
?>