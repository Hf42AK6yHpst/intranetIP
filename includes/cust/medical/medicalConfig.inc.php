<?php
//update by :  
//$cfg_medical[''] = '';
include_once($intranet_root.'/includes/cust/medical/medicalConfigAccess.inc.php');


$medical_cfg['uploadFile_root'] = $intranet_root.'/file/medical/';

//stay option
$medical_cfg['sleep_status']['StayIn'] = array('value'=>1,
											 'lang'=>$Lang['medical']['report_meal']['sleepOption']['StayIn'],
											);
$medical_cfg['sleep_status']['StayOut'] = array('value'=>0,
											 'lang'=>$Lang['medical']['report_meal']['sleepOption']['StayOut'],
											);


$w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["breakfast"] = 1;
$w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["lunch"] = 2;
$w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["dinner"] = 3;


$w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] = 'MEDICAL_STUDENT_DAILY_MEAL_LOG_';
$w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] = 'CARD_STUDENT_DAILY_LOG_';
$w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] = 'MEDICAL_STUDENT_BOWEL_LOG_';
$medical_cfg['general']['status']['InUse'] = array('value'=>1,
											 'lang'=>$Lang['medical']['general']['StatusOption']['InUse']
											);
$medical_cfg['general']['status']['Suspend'] = array('value'=>0,
											 'lang'=>$Lang['medical']['general']['StatusOption']['Suspend']
											);
$medical_cfg['studentLog']['level2']['specialStatus'] = '抽搐';
$medical_cfg['studentSleep']['level1']['comments'] = '補充';

$medical_cfg['studentSleep']['sleepWell'] = '安睡';
$medical_cfg['studentSleep']['healthProblem'] = '健康問題';
$medical_cfg['studentSleep']['emotionProblem'] = '情緒行為問題';
$medical_cfg['studentSleep']['otherProblem'] = '其他問題';
$medical_cfg['studentSleep']['sleepWellID'] = 0;
$medical_cfg['studentSleep']['healthProblemID'] = 1;
$medical_cfg['studentSleep']['emotionProblemID'] = 2;
$medical_cfg['studentSleep']['otherProblemID'] = 3;

$medical_cfg['general']['all'] = $Lang['medical']['General']['All'] ? $Lang['medical']['General']['All'] : "ALL";

# search menu title
$medical_cfg['general']['report']['searchMenu']['date'] 		= $Lang['medical']['report_general']['searchMenu']['date'] ? $Lang['medical']['report_general']['searchMenu']['date'] : 'Date';
$medical_cfg['general']['report']['searchMenu']['form'] 		= $Lang['medical']['report_general']['searchMenu']['form'] ? $Lang['medical']['report_general']['searchMenu']['form'] : 'Class Name';
$medical_cfg['general']['report']['searchMenu']['group'] 		= $Lang['medical']['report_general']['searchMenu']['group'] ? $Lang['medical']['report_general']['searchMenu']['group'] : 'Group';
$medical_cfg['general']['report']['searchMenu']['sleep'] 		= $Lang['medical']['report_general']['searchMenu']['sleep'] ? $Lang['medical']['report_general']['searchMenu']['sleep'] : 'Stay/ Not Stay';
$medical_cfg['general']['report']['searchMenu']['gender'] 		= $Lang['medical']['report_general']['searchMenu']['gender'] ? $Lang['medical']['report_general']['searchMenu']['gender'] : 'Gender';
//$medical_cfg['general']['report']['searchMenu']['timePeriod'] 	= $Lang['medical']['report_general']['searchMenu']['timePeriod'] ? $Lang['medical']['report_general']['searchMenu']['timePeriod'] : 'Time Slot';
$medical_cfg['general']['report']['searchMenu']['studentName'] 	= $Lang['medical']['report_general']['searchMenu']['studentName'] ? $Lang['medical']['report_general']['searchMenu']['studentName'] : 'Student Name';
$medical_cfg['general']['report']['searchMenu']['item'] 		= $Lang['medical']['report_general']['searchMenu']['item'] ? $Lang['medical']['report_general']['searchMenu']['item'] : 'Item';

$medical_cfg['general']['report']['search']['sleepOption'] = count($Lang['medical']['report_general']['search']['sleepOption'])>0 ? $Lang['medical']['report_general']['search']['sleepOption'] : array($medical_cfg['general']['all']);
$medical_cfg['general']['report']['search']['genderOption'] = count($Lang['medical']['report_general']['search']['genderOption'])>0 ? $Lang['medical']['report_general']['search']['genderOption'] : array($medical_cfg['general']['all']);

$medical_cfg['general']['report']['search']['itemCheckbox']['all'] = $medical_cfg['general']['all'];
$medical_cfg['general']['report']['search']['itemCheckbox']['remarks'] = $Lang['medical']['report_general']['search']['itemCheckbox']['remarks'] ? $Lang['medical']['report_general']['search']['itemCheckbox']['remarks'] : 'Remarks';


# Sleep Config
$medical_cfg['general']['student_sleep']['maximumRecordCount'] =10;
$medical_cfg['general']['student_sleep']['frequency'] =10;

# Bowel Config
$medical_cfg['general']['bowel']['maximumRecordCount'] = 50;
$medical_cfg['general']['bowel']['import']['deleteRecordCode'] = 'R001';


$medical_cfg['CARD_STATUS_PRESENT'] = 0;
$medical_cfg['CARD_STATUS_ABSENT'] = 1;
$medical_cfg['CARD_STATUS_LATE'] = 2;
$medical_cfg['CARD_STATUS_OUTING'] = 3;

$medical_cfg['general']['module']['meal']='meal';
$medical_cfg['general']['module']['bowel']='bowel';
$medical_cfg['general']['module']['studentlog']='studentlog';
$medical_cfg['general']['module']['sleep']='sleep';


$medical_cfg['report']['convlusion']['rowPerPage']=14;
$medical_cfg['general']['MaxFileSize']='100';		// 100M
?>