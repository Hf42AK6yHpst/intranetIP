<?php
//using:

if (!defined("LIBPROBLEMRECORD_DEFINED"))                     // Preprocessor directive
{
        define("LIBPROBLEMRECORD_DEFINED", true);

		abstract class libProblemRecordBase{
			public $YearSeq;
			public $YearName;
			public $ClassSeq;
			public $ClassName;
			public $ClassNumber;
			public $UserID;
			public $RecordID;
			public $StudentName;
			public $ModifyByName;
			public $DateModified;
		}
		
		class libProblemRecordMeal extends libProblemRecordBase
		{
			public $PeriodStatus;
			public $MealStatus;
			public $Remarks;
			public $Date;
			
			static function cmp_obj($a, $b){
				if($a->YearSeq != $b->YearSeq){
					return $a->YearSeq - $b->YearSeq;
				}
				if($a->YearName != $b->YearName){
					return $a->YearName - $b->YearName;
				}
				if($a->ClassSeq != $b->ClassSeq){
					return $a->ClassSeq - $b->ClassSeq;
				}
				if($a->ClassName != $b->ClassName){
					return $a->ClassName - $b->ClassName;
				}
				if($a->ClassNumber != $b->ClassNumber){
					return $a->ClassNumber - $b->ClassNumber;
				}
				if($a->Date != $b->Date){			
					return (strtotime($a->Date) - strtotime($b->Date)) > 0;
				}
				return $a->PeriodStatus - $b->PeriodStatus;
			}
		}
		
		class libProblemRecordBowel extends libProblemRecordBase
		{
			public $RecordTime;
			public $BowelID;
			public $Remarks;
			
			static function cmp_obj($a, $b){
				if($a->YearSeq != $b->YearSeq){
					return $a->YearSeq - $b->YearSeq;
				}
				if($a->YearName != $b->YearName){
					return $a->YearName - $b->YearName;
				}
				if($a->ClassSeq != $b->ClassSeq){
					return $a->ClassSeq - $b->ClassSeq;
				}
				if($a->ClassName != $b->ClassName){
					return $a->ClassName - $b->ClassName;
				}
				if($a->ClassNumber != $b->ClassNumber){
					return $a->ClassNumber - $b->ClassNumber;
				}				
				return strcmp($a->RecordTime, $b->RecordTime);
			}
		}
		
		class libProblemRecordSleep extends libProblemRecordBase
		{
			public $RecordTime;
			public $SleepID;
			public $ReasonID;
			public $Frequency;
			public $Remarks;
			
			static function cmp_obj($a, $b){
				if($a->YearSeq != $b->YearSeq){
					return $a->YearSeq - $b->YearSeq;
				}
				if($a->YearName != $b->YearName){
					return $a->YearName - $b->YearName;
				}
				if($a->ClassSeq != $b->ClassSeq){
					return $a->ClassSeq - $b->ClassSeq;
				}
				if($a->ClassName != $b->ClassName){
					return $a->ClassName - $b->ClassName;
				}
				if($a->ClassNumber != $b->ClassNumber){
					return $a->ClassNumber - $b->ClassNumber;
				}				
				return strcmp($a->RecordTime, $b->RecordTime);
			}
		}
		
		class libProblemRecordStudentLog extends libProblemRecordBase
		{
			public $RecordTime;
			public $SleepID;
			public $ReasonID;
			public $Frequency;
			public $Remarks;
			
			static function cmp_obj($a, $b){
				if($a->YearSeq != $b->YearSeq){
					return $a->YearSeq - $b->YearSeq;
				}
				if($a->YearName != $b->YearName){
					return $a->YearName - $b->YearName;
				}
				if($a->ClassSeq != $b->ClassSeq){
					return $a->ClassSeq - $b->ClassSeq;
				}
				if($a->ClassName != $b->ClassName){
					return $a->ClassName - $b->ClassName;
				}
				if($a->ClassNumber != $b->ClassNumber){
					return $a->ClassNumber - $b->ClassNumber;
				}				
				return strcmp($a->RecordTime, $b->RecordTime);
			}
		}
}
?>