<?php
//updated by : 
/*
 * 	Log
 * 
 *
 *  2018-05-29 [Cameron]
 *      - apply stripslashes to StatusName
 *      	 
 * 	Date:	2013-12-18 [Cameron] 
 */
if (!defined("LIBSTUDENTSLEEPLEV2_DEFINED"))                     // Preprocessor directive
{
        define("LIBSTUDENTSLEEPLEV2_DEFINED", true);
		class studentSleepLev2
        {
                private $objDB;

				private $ReasonID;
				private $StatusID;
				private $ReasonName;
				private $ReasonCode;
				private $IsDefault;
				private $RecordStatus;
				private $DeletedFlag;
				private $DeletedBy;
				private $DeletedDate;
				private $InputBy;
				private $DateInput;
				private $DateModified;
				private $LastModifiedBy;


				public function studentSleepLev2($reasonID = NULL){
					$this->objDB = new libdb();

					if($reasonID != ''){
						$this->setReasonID($reasonID);
						$this->loadDataFormStorage();
					}
				}

				public function setReasonID($val){$this->ReasonID=$val;}
				public function getReasonID(){return $this->ReasonID;}

				public function setStatusID($val){$this->StatusID=$val;}
				public function getStatusID(){return $this->StatusID;}

				public function setLev2Code($val){$this->Lev2Code=$val;}
				public function getLev2Code(){return $this->Lev2Code;}

				public function setReasonName($val){$this->ReasonName=$val;}
				public function getReasonName(){return $this->ReasonName;}

				public function setReasonCode($val){$this->ReasonCode=$val;}
				public function getReasonCode(){return $this->ReasonCode;}

				public function setIsDefault($val){$this->IsDefault=$val;}
				public function getIsDefault(){return $this->IsDefault;}

				public function setRecordStatus($val){$this->RecordStatus=$val;}
				public function getRecordStatus(){return $this->RecordStatus;}

				public function setDeletedFlag($val){$this->DeletedFlag=$val;}
				public function getDeletedFlag(){return $this->DeletedFlag;}

				public function setDeletedBy($val){$this->DeletedBy=$val;}
				public function getDeletedBy(){return $this->DeletedBy;}

				public function setDeletedDate($val){$this->DeletedDate=$val;}
				public function getDeletedDate(){return $this->DeletedDate;}

				public function setInputBy($val){$this->InputBy=$val;}
				public function getInputBy(){return $this->InputBy;}
				
				public function setDateInput($val){$this->DateInput=$val;}
				public function getDateInput(){return $this->DateInput;}

				public function setDateModified($val){$this->DateModified=$val;}
				public function getDateModified(){return $this->DateModified;}

				public function setLastModifiedBy($val){$this->LastModifiedBy=$val;}
				public function getLastModifiedBy(){return $this->LastModifiedBy;}

				private function loadDataFormStorage($loadObject=true, $whereCriteria= NULL,$orderCriteria = NULL)
				{

					$conds = '';
					if($loadObject){
						$conds = 'and ReasonID = '.$this->getReasonID();
					}

					if($whereCriteria !== NULL){
							$conds .= ' and ('.$whereCriteria.')';
					}
					
					$orderBy = '';
					if ($orderCriteria !== NULL){
						$orderBy = ' ORDER BY ' . $orderCriteria;
					}

					$sql='select 
							  ReasonID,
							  StatusID,
							  ReasonName,
							  ReasonCode,
							  IsDefault,
							  RecordStatus,
							  DeletedFlag,
							  DeletedBy,
							  DeletedDate,
							  InputBy,
							  DateInput,
							  DateModified,
							  LastModifiedBy 
						  from MEDICAL_SLEEP_STATUS_REASON where 1=1 '.$conds . $orderBy;
//debug_r($sql);
					$rs = $this->objDB->returnResultSet($sql);
//debug_r($rs);
					if($loadObject){
						if(count($rs) > 1){
							//since the statusid is the key  ,return resultset cannot more than 1
							  $errMsg = 'SQL Result! support return zero not or one record but failed ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
							  return false;
						}
						
						if(count($rs) ==1){
							$rs = current($rs);
							$this->setReasonID($rs['ReasonID']);
							$this->setStatusID($rs['StatusID']);
							$this->setReasonName($rs['ReasonName']);
							$this->setReasonCode($rs['ReasonCode']);
							$this->setIsDefault($rs['IsDefault']);
							$this->setRecordStatus($rs['RecordStatus']);
							$this->setDeletedFlag($rs['DeletedFlag']);
							$this->setDeletedBy($rs['DeletedBy']);
							$this->setDeletedDate($rs['DeletedDate']);
							$this->setInputBy($rs['InputBy']);
							$this->setDateInput($rs['DateInput']);
							$this->setDateModified($rs['DateModified']);
							$this->setLastModifiedBy($rs['LastModifiedBy']);	
							return $this;
						}else{
							return NULL;
						}
					}
					else{
						return $rs;	
					}
				}

				public function save()
				{
					if($this->getReasonID()>0){
						$ret = $this->updateRecord();
					}else{
						$ret = $this->insertRecord();
					}
					return $ret;
				}
				
			   /**
				* Return a array with all active status, default is order by status code desc
				* @owner : Pun (20140113)
				* @param : String $orderCriteria (default order by status code desc)
				* @param : Integer $levelOneId (default = NULL)
				* @return : Resultset DB array for all active status , depend on the LevelOneId if have any
				* 
				*/
				public function getActiveStatus($orderCriteria = '',$levelOneId = NULL)
				{
					$orderCriteria = ($orderCriteria == '') ? ' ReasonCode' :$orderCriteria;

					if($levelOneId > 0){
						return $this->getAllStatus($whereCriteria = ' StatusID = '.$levelOneId.' and recordstatus = 1 and DeletedFlag = 0 ',$orderCriteria);
					}
					return $this->getAllStatus($whereCriteria = ' recordstatus = 1 and DeletedFlag = 0 ',$orderCriteria);
				}

				public function getAllStatus($whereCriteria = NULL,$orderCriteria = NULL){
					$rs = $this->loadDataFormStorage($loadObject=false, $whereCriteria,$orderCriteria);
					return $rs;
				}

				public function getHTMLSelection($htmlName, $selectedValue = NULL, $levelOneId = NULL, $otherAttribute = NULL){
					global $Lang;

					$rs = $this->getActiveStatus($orderCriteria = ' ReasonCode ASC ',$levelOneId);
//return print_r($rs,true);
					$html = '<select name="'.$htmlName.'" id="'.$htmlName.'" '.$otherAttribute.'>'."\n";
					if(count($rs) >0)
					{
						$withDefaultSelected = false;
						for($i = 0,$iMax = count($rs);$i < $iMax; $i++)
						{	
							$_selected = '';
							//without user default value
							if($selectedValue == NULL)
							{
								//have not set selected before
								if(!$withDefaultSelected)
								{
									if($rs[$i]['IsDefault'] == 1)
									{
										$_selected = ' SELECTED ';
										$withDefaultSelected = true;
									}
								}
							}else{
								$_selected = ($rs[$i]['ReasonID'] == $selectedValue) ? ' SELECTED ' :'';
							}
							
							$html .= '<option value= "'.$rs[$i]['ReasonID'].'" '.$_selected.' >'.stripslashes($rs[$i]['ReasonName']).'</option>'."\n";
						}	
					}else{
						$html .= '<option value= "" '.$_selected.'>'.$Lang['General']['NoRecordFound'].'</option>'."\n";
					}
					
					$html .= '</select>';
//					debug_r($html);
					return $html;
				}

				/**
				 * Checking for duplicate ['Lev2Name'] and ['Lev2Code']
				 * @owner : Pun (20140110)
				 * @param : String $testName (Name to test for duplicate)
				 * @param : String $testCode (Code to test for duplicate)
				 * @param : String $Lev1ID (Record to check, for holding old record)
				 * @return : 0 if the checking pass(no duplicated record), 1 if Lev1Name duplicated, 2 if Lev1Code duplicated, 3 if both duplicated
				 * 
				 */
				public function recordDuplicateChecking($testName, $testCode, $StatusID, $ReasonID='')
				{
					if($testName == '' && $testCode == '')
					{
						return true;
					}
					
					$checkingResult = 0;
					$addtionSQL = '';
					if($StatusID != '')
					{
						$addtionSQL .= ' AND StatusID = \'' . $StatusID . '\'';
					}
					if($ReasonID != '')
					{
						$addtionSQL .= ' AND ReasonID != \'' . $ReasonID . '\'';
					}
					
					$sql = 'SELECT COUNT(*) FROM MEDICAL_SLEEP_STATUS_REASON WHERE DeletedFlag = 0 AND ReasonName=\'' . trim($testName,'\'') . '\'' . $addtionSQL;
					$rs = $this->objDB->returnResultSet($sql);
					if($rs[0]['COUNT(*)'] > 0)
					{
						$checkingResult += 1;
					}
//error_log("\n\n\n---------------\n", 3, "/tmp/debug_a.txt");
//error_log('$sql -->'.print_r($sql,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
//error_log('$rs -->'.print_r($rs,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
//error_log('$checkingResult -->'.print_r($checkingResult,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
					if($testCode !='' && $testCode !='\'\''){
						$sql = 'SELECT COUNT(*) FROM MEDICAL_SLEEP_STATUS_REASON WHERE DeletedFlag = 0 AND ReasonCode=\'' . trim($testCode,'\'') . '\'' . $addtionSQL;
						$rs = $this->objDB->returnResultSet($sql);
						if($rs[0]['COUNT(*)'] > 0)
						{
							$checkingResult += 2;
						}
					}
								
					return $checkingResult;
				}

				/*
				 * Add duplicate checking - by Pun
				 */
				private function insertRecord(){
					global $cfg_medical;
					$DataArr = array();
					
					if($this->recordDuplicateChecking($this->getReasonName(),$this->getReasonCode(),$this->getStatusID()))
					{
						return false;
					}

					$DataArr['ReasonName'] = $this->objDB->pack_value($this->getReasonName(),'str');
					$DataArr['ReasonCode'] = $this->objDB->pack_value($this->getReasonCode(),'str');
					$DataArr['StatusID'] = $this->objDB->pack_value($this->getStatusID(),'int');
					$DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(),'int');
					$DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(),'int');
					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
					$DataArr['DateInput'] = $this->objDB->pack_value('now()','date');
//					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
//					$DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');

					$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

					$fieldStr= $sqlStrAry['sqlField'];
					$valueStr= $sqlStrAry['sqlValue'];


					$sql = 'Insert Into MEDICAL_SLEEP_STATUS_REASON ('.$fieldStr.') Values ('.$valueStr.')';
					
					$success = $this->objDB->db_db_query($sql);
					if($success != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}
					
					$RecordID = $this->objDB->db_insert_id();
					$this->setReasonID($RecordID);
					return $RecordID;
				}

				
				/*
				 * Add duplicate checking - by Pun
				 */
				private function updateRecord(){
					global $cfg_medical;

					$DataArr = array();

					$DataArr['ReasonName'] = $this->objDB->pack_value($this->getReasonName(),'str');
					$DataArr['ReasonCode'] = $this->objDB->pack_value($this->getReasonCode(),'str');
					$DataArr['StatusID'] = $this->objDB->pack_value($this->getStatusID(),'int');
//					$DataArr['ItemQty'] = $this->objDB->pack_value($this->getItemQty(),'str');
					$DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(),'int');
					$DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(),'int');
//					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
					$DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');
				
					if($this->recordDuplicateChecking($DataArr['ReasonName'], $DataArr['ReasonCode'], $this->getStatusID(), $this->getReasonID()))
					{
						return false;
					}
					
					foreach ($DataArr as $fieldName => $data)
					{
						$updateDetails .= $fieldName."=".$data.",";
					}

					//REMOVE LAST OCCURRENCE OF ",";
					$updateDetails = substr($updateDetails,0,-1);

					$sql = "update MEDICAL_SLEEP_STATUS_REASON set ".$updateDetails." where ReasonID = ".$this->getReasonID();
//error_log("\n\nupdate sql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
					$result = $this->objDB->db_db_query($sql);
								
					if($result != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}

					return $result;

				}
				
				// Check if IsDefault been set in amoung all active records
				public function checkIsDefault($StatusID){
					$sql = "SELECT ReasonID FROM MEDICAL_SLEEP_STATUS_REASON WHERE StatusID=".$StatusID." AND IsDefault=1 AND DeletedFlag=0 LIMIT 1";
					$rs = $this->objDB->returnResultSet($sql);
					$ret = (count($rs) > 0) ? true : false;
					return $ret; 					
					
				}
				
				public function sumItemQty($StatusID)
				{
					if ($StatusID)
					{
						// Deleted not count
						$sql = "SELECT COUNT(1) AS ItemQty FROM MEDICAL_SLEEP_STATUS_REASON WHERE StatusID=".$StatusID. " and DeletedFlag = 0";
//error_log("\n\nupdate sql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");						
						$rs = $this->objDB->returnResultSet($sql);
//error_log("\n\nrs -->".print_r($rs,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");						
						if (count($rs)==1)
						{
							return $rs[0]["ItemQty"];
						}
						else
						{
						  $errMsg = 'sumItemQty Error: mutiple records! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						}
					}
					else
					{
						  $errMsg = "sumItemQty Error: empty lev1ID  f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					}
					return false;	
				}
				
		}

}
?>