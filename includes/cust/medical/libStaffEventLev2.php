<?php
// updated by :
/*
 * Log
 * Date: 2018-03-26 [Cameron]
 */
if (! defined("LIBSTAFFEVENTLEV2_DEFINED")) // Preprocessor directive
{
    define("LIBSTAFFEVENTLEV2_DEFINED", true);

    class StaffEventLev2
    {

        private $objDB;

        private $EventTypeLev2ID;

        private $EventTypeID;

        private $EventTypeLev2Name;

        private $IsDefault;

        private $RecordStatus;

        private $DeletedFlag;

        private $DeletedBy;

        private $DeletedDate;

        private $DateInput;

        private $InputBy;

        private $DateModified;

        private $LastModifiedBy;

        public function StaffEventLev2($eventTypeLev2ID = NULL)
        {
            $this->objDB = new libdb();
            
            if ($eventTypeLev2ID != '') {
                $this->setEventTypeLev2ID($eventTypeLev2ID);
                $this->loadDataFormStorage();
            }
        }

        public function setEventTypeLev2ID($val)
        {
            $this->EventTypeLev2ID = $val;
        }

        public function getEventTypeLev2ID()
        {
            return $this->EventTypeLev2ID;
        }

        public function setEventTypeID($val)
        {
            $this->EventTypeID = $val;
        }

        public function getEventTypeID()
        {
            return $this->EventTypeID;
        }

        public function setEventTypeLev2Name($val)
        {
            $this->EventTypeLev2Name = $val;
        }

        public function getEventTypeLev2Name()
        {
            return $this->EventTypeLev2Name;
        }

        public function setIsDefault($val)
        {
            $this->IsDefault = $val;
        }

        public function getIsDefault()
        {
            return $this->IsDefault;
        }

        public function setRecordStatus($val)
        {
            $this->RecordStatus = $val;
        }

        public function getRecordStatus()
        {
            return $this->RecordStatus;
        }

        public function setDeletedFlag($val)
        {
            $this->DeletedFlag = $val;
        }

        public function getDeletedFlag()
        {
            return $this->DeletedFlag;
        }

        public function setDeletedBy($val)
        {
            $this->DeletedBy = $val;
        }

        public function getDeletedBy()
        {
            return $this->DeletedBy;
        }

        public function setDeletedDate($val)
        {
            $this->DeletedDate = $val;
        }

        public function getDeletedDate()
        {
            return $this->DeletedDate;
        }

        public function setDateInput($val)
        {
            $this->DateInput = $val;
        }

        public function getDateInput()
        {
            return $this->DateInput;
        }

        public function setInputBy($val)
        {
            $this->InputBy = $val;
        }

        public function getInputBy()
        {
            return $this->InputBy;
        }

        public function setDateModified($val)
        {
            $this->DateModified = $val;
        }

        public function getDateModified()
        {
            return $this->DateModified;
        }

        public function setLastModifiedBy($val)
        {
            $this->LastModifiedBy = $val;
        }

        public function getLastModifiedBy()
        {
            return $this->LastModifiedBy;
        }

        private function loadDataFormStorage($loadObject = true, $whereCriteria = NULL, $orderCriteria = NULL)
        {
            $conds = '';
            if ($loadObject) {
                $conds = 'AND EventTypeLev2ID = ' . $this->getEventTypeLev2ID();
            }
            
            if ($whereCriteria !== NULL) {
                $conds .= ' AND (' . $whereCriteria . ')';
            }
            
            $orderBy = '';
            if ($orderCriteria !== NULL) {
                $orderBy = ' ORDER BY ' . $orderCriteria;
            }
            
            $sql = 'SELECT
                              EventTypeLev2ID,
							  EventTypeID,
							  EventTypeLev2Name,
                              IsDefault,
							  RecordStatus,
							  DeletedFlag,
							  DeletedBy,
							  DeletedDate,
							  DateInput,
							  InputBy,
							  DateModified,
							  LastModifiedBy
						  FROM MEDICAL_STAFF_EVENT_TYPE_LEV2 WHERE 1=1 ' . $conds . $orderBy;
            
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($loadObject) {
                if (count($rs) > 1) {
                    return false;
                }
                
                if (count($rs) == 1) {
                    $rs = current($rs);
                    $this->setEventTypeLev2ID($rs['EventTypeLev2ID']);
                    $this->setEventTypeID($rs['EventTypeID']);
                    $this->setEventTypeLev2Name($rs['EventTypeLev2Name']);
                    $this->setIsDefault($rs['IsDefault']);
                    $this->setRecordStatus($rs['RecordStatus']);
                    $this->setDeletedFlag($rs['DeletedFlag']);
                    $this->setDeletedBy($rs['DeletedBy']);
                    $this->setDeletedDate($rs['DeletedDate']);
                    $this->setDateInput($rs['DateInput']);
                    $this->setInputBy($rs['InputBy']);
                    $this->setDateModified($rs['DateModified']);
                    $this->setLastModifiedBy($rs['LastModifiedBy']);
                    return $this;
                } else {
                    return NULL;
                }
            } else {
                return $rs;
            }
        }

        public function save()
        {
            if ($this->getEventTypeLev2ID() > 0) {
                $ret = $this->updateRecord();
            } else {
                $ret = $this->insertRecord();
            }
            return $ret;
        }

        public function getActiveEventType($orderCriteria = '', $eventTypeId = NULL)
        {
            $orderCriteria = ($orderCriteria == NULL) ? ' EventTypeLev2Name ' : $orderCriteria;
            
            if ($eventTypeId > 0) {
                return $this->getAllEventType($whereCriteria = ' EventTypeID = ' . $eventTypeId . ' and recordstatus = 1 and DeletedFlag = 0 ', $orderCriteria);
            }
            return $this->getAllEventType($whereCriteria = ' recordstatus = 1 and DeletedFlag = 0 ', $orderCriteria);
        }

        public function getAllEventType($whereCriteria = NULL, $orderCriteria = NULL)
        {
            $rs = $this->loadDataFormStorage($loadObject = false, $whereCriteria, $orderCriteria);
            return $rs;
        }

        public function getHTMLSelection($htmlName, $selectedValue = NULL, $levelOneId = NULL, $otherAttribute = NULL)
        {
            global $Lang;
            
            $html = '<select name="' . $htmlName . '" id="' . $htmlName . '" ' . $otherAttribute . '>' . "\n";
            
            if ($levelOneId == - 1) {
                $html .= '<option value= "" SELECTED>' . $Lang['General']['PleaseSelect'] . '</option>' . "\n";
            } else {
                $rs = $this->getActiveStatus($orderCriteria = 'EventTypeLev2ID asc ', $levelOneId);
                if (count($rs) > 0) {
                    $withDefaultSelected = false;
                    for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                        $_selected = '';
                        // without user default value
                        if ($selectedValue == NULL) {
                            // have not set selected before
                            if (! $withDefaultSelected) {
                                if ($rs[$i]['IsDefault'] == 1) {
                                    $_selected = ' SELECTED ';
                                    $withDefaultSelected = true;
                                }
                            }
                        } else {
                            $_selected = ($rs[$i]['EventTypeLev2ID'] == $selectedValue) ? ' SELECTED ' : '';
                        }
                        
                        $html .= '<option value= "' . $rs[$i]['EventTypeLev2ID'] . '" ' . $_selected . ' >' . $rs[$i]['EventTypeLev2Name'] . '</option>' . "\n";
                    }
                } else {
                    $html .= '<option value= "" ' . $_selected . '>' . $Lang['General']['NoRecordFound'] . '</option>' . "\n";
                }
            }
            $html .= '</select>';
            return $html;
        }

        // Check if IsDefault been set in among all active records
        public function checkIsDefault($eventTypeID)
        {
            if ($eventTypeID) {
                $sql = "SELECT EventTypeID FROM MEDICAL_STAFF_EVENT_TYPE_LEV2 WHERE EventTypeID='" . $eventTypeID . "' AND IsDefault=1 AND DeletedFlag=0 LIMIT 1";
                $rs = $this->objDB->returnResultSet($sql);
                
                $ret = (count($rs) > 0) ? true : false;
            } else {
                $ret = false;
            }
            return $ret;
        }

        public function recordDuplicateChecking($testName, $eventTypeID, $eventTypeLev2ID = '')
        {
            if ($testName == '') {
                return true;
            }
            
            $checkingResult = 0;
            $addtionSQL = '';
            if ($eventTypeID != '') {
                $addtionSQL .= ' AND EventTypeID = \'' . $eventTypeID . '\'';
            }
            if ($eventTypeLev2ID != '') {
                $addtionSQL .= ' AND EventTypeLev2ID != \'' . $eventTypeLev2ID . '\'';
            }
            
            $sql = 'SELECT COUNT(*) AS NumRecord FROM MEDICAL_STAFF_EVENT_TYPE_LEV2 WHERE DeletedFlag = 0 AND EventTypeLev2Name=\'' . $this->objDB->Get_Safe_Sql_Query(trim($testName), '\'') . '\'' . $addtionSQL;
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($rs[0]['NumRecord'] > 0) {
                $checkingResult += 1;
            }
            
            return $checkingResult;
        }

        private function insertRecord()
        {
            global $cfg_medical;
            $DataArr = array();
            if ($this->recordDuplicateChecking($this->getEventTypeLev2Name(), $this->getEventTypeID())) {
                return false;
            }
            
            $DataArr['EventTypeLev2Name'] = $this->objDB->pack_value($this->getEventTypeLev2Name(), 'str');
            $DataArr['EventTypeID'] = $this->objDB->pack_value($this->getEventTypeID(), 'int');
            $DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(), 'int');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            $DataArr['DateInput'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(), 'int');
            
            $sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);
            
            $fieldStr = $sqlStrAry['sqlField'];
            $valueStr = $sqlStrAry['sqlValue'];
            
            $sql = 'INSERT INTO MEDICAL_STAFF_EVENT_TYPE_LEV2 (' . $fieldStr . ') VALUES (' . $valueStr . ')';
            
            $success = $this->objDB->db_db_query($sql);
            
            if ($success != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            $eventTypeLev2ID = $this->objDB->db_insert_id();
            $this->setEventTypeLev2ID($eventTypeLev2ID);
            return $eventTypeLev2ID;
        }

        private function updateRecord()
        {
            global $cfg_medical;
            
            $DataArr = array();
            
            $DataArr['EventTypeLev2Name'] = $this->objDB->pack_value($this->getEventTypeLev2Name(), 'str');
            $DataArr['EventTypeID'] = $this->objDB->pack_value($this->getEventTypeID(), 'int');
            $DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(), 'int');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            $DataArr['DateModified'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(), 'int');
            
            if ($this->recordDuplicateChecking($DataArr['EventTypeLev2Name'], $this->getEventTypeID(), $this->getEventTypeLev2ID())) {
                return false;
            }
            
            foreach ($DataArr as $fieldName => $data) {
                $updateDetails .= $fieldName . "=" . $data . ",";
            }
            
            // REMOVE LAST OCCURRENCE OF ",";
            $updateDetails = substr($updateDetails, 0, - 1);
            
            $sql = "UPDATE MEDICAL_STAFF_EVENT_TYPE_LEV2 SET " . $updateDetails . " where EventTypeLev2ID = " . $this->getEventTypeLev2ID();
            $result = $this->objDB->db_db_query($sql);
            
            if ($result != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            return $result;
        }

        public function sumItemQty($eventTypeID)
        {
            if ($eventTypeID) {
                // Deleted not count
                $sql = "SELECT COUNT(1) AS ItemQty FROM MEDICAL_STAFF_EVENT_TYPE_LEV2 WHERE Lev1ID=" . $eventTypeID . " and DeletedFlag = 0";
                $rs = $this->objDB->returnResultSet($sql);
                
                if (count($rs) == 1) {
                    return $rs[0]["ItemQty"];
                } else {
                    $errMsg = 'sumItemQty Error: mutiple records! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                    alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                }
            } else {
                $errMsg = "sumItemQty Error: empty eventTypeID  f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
            }
            return false;
        }
    }
}
?>