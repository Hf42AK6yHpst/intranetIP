<?php
// using by :
/*
 * Log
 *
 * 2018-03-26 Cameron - create this file
 *
 */
if (! defined("LIBSTAFFEVENTLEV1_DEFINED")) // Preprocessor directive
{
    define("LIBSTAFFEVENTLEV1_DEFINED", true);

    class StaffEventLev1
    {

        private $objDB;

        private $EventTypeID;

        private $EventTypeName;

        private $IsDefault;

        private $RecordStatus;

        private $DeletedFlag;

        private $DeletedBy;

        private $DeletedDate;

        private $DateInput;

        private $InputBy;

        private $DateModified;

        private $LastModifiedBy;

        private $NrSubCategory;

        public function StaffEventLev1($eventTypeID = NULL)
        {
            $this->objDB = new libdb();
            
            if ($eventTypeID != '') {
                $this->setEventTypeID($eventTypeID);
                $this->loadDataFormStorage();
            }
        }

        public function setEventTypeID($val)
        {
            $this->EventTypeID = $val;
        }

        public function getEventTypeID()
        {
            return $this->EventTypeID;
        }

        public function setEventTypeName($val)
        {
            $this->EventTypeName = $val;
        }

        public function getEventTypeName()
        {
            return $this->EventTypeName;
        }

        public function setIsDefault($val)
        {
            $this->IsDefault = $val;
        }

        public function getIsDefault()
        {
            return $this->IsDefault;
        }

        public function setRecordStatus($val)
        {
            $this->RecordStatus = $val;
        }

        public function getRecordStatus()
        {
            return $this->RecordStatus;
        }

        public function setDeletedFlag($val)
        {
            $this->DeletedFlag = $val;
        }

        public function getDeletedFlag()
        {
            return $this->DeletedFlag;
        }

        public function setDeletedBy($val)
        {
            $this->DeletedBy = $val;
        }

        public function getDeletedBy()
        {
            return $this->DeletedBy;
        }

        public function setDeletedDate($val)
        {
            $this->DeletedDate = $val;
        }

        public function getDeletedDate()
        {
            return $this->DeletedDate;
        }

        public function setDateInput($val)
        {
            $this->DateInput = $val;
        }

        public function getDateInput()
        {
            return $this->DateInput;
        }

        public function setInputBy($val)
        {
            $this->InputBy = $val;
        }

        public function getInputBy()
        {
            return $this->InputBy;
        }

        public function setDateModified($val)
        {
            $this->DateModified = $val;
        }

        public function getDateModified()
        {
            return $this->DateModified;
        }

        public function setLastModifiedBy($val)
        {
            $this->LastModifiedBy = $val;
        }

        public function getLastModifiedBy()
        {
            return $this->LastModifiedBy;
        }

        public function setNrSubCategory($val)
        {
            $this->NrSubCategory = $val;
        }

        public function getNrSubCategory()
        {
            $sql = "SELECT COUNT(*) AS NrSubCategory FROM MEDICAL_STAFF_EVENT_TYPE_LEV2 WHERE EventTypeID='" . $this->EventTypeID . "'";
            $rs = $this->objDB->returnResultSet($sql);
            return count($rs) ? $rs[0]['NrSubCategory'] : 0;
        }

        private function loadDataFormStorage($loadObject = true, $whereCriteria = NULL, $orderCriteria = NULL)
        {
            $conds = '';
            if ($loadObject) {
                $conds = 'AND t.EventTypeID = ' . $this->getEventTypeID();
            }
            
            if ($whereCriteria !== NULL) {
                $conds .= ' AND (' . $whereCriteria . ')';
            }
            
            $orderBy = '';
            if ($orderCriteria !== NULL) {
                $orderBy = ' ORDER BY ' . $orderCriteria;
            }
            
            $sql = 'SELECT 
							  t.EventTypeID,
							  t.EventTypeName,
                              t.IsDefault,
							  t.RecordStatus,
							  t.DeletedFlag,
							  t.DeletedBy,
							  t.DeletedDate,
							  t.DateInput,
							  t.InputBy,
							  t.DateModified,
							  t.LastModifiedBy,
                              IFNULL(s.NrSubCategory,0) AS NrSubCategory
				    FROM 
                              MEDICAL_STAFF_EVENT_TYPE t
                    LEFT JOIN 
                              (SELECT 
                                        EventTypeID, COUNT(*) AS NrSubCategory 
                               FROM 
                                        MEDICAL_STAFF_EVENT_TYPE_LEV2 
                               WHERE  
                                        DeletedFlag=0 
                               GROUP BY EventTypeID) s ON s.EventTypeID=t.EventTypeID
                     WHERE 1=1 ' . $conds . $orderBy;
            
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($loadObject) {
                if (count($rs) > 1) {
                    return false;
                }
                
                if (count($rs) == 1) {
                    $rs = current($rs);
                    $this->setEventTypeID($rs['EventTypeID']);
                    $this->setEventTypeName($rs['EventTypeName']);
                    $this->setIsDefault($rs['IsDefault']);
                    $this->setRecordStatus($rs['RecordStatus']);
                    $this->setDeletedFlag($rs['DeletedFlag']);
                    $this->setDeletedBy($rs['DeletedBy']);
                    $this->setDeletedDate($rs['DeletedDate']);
                    $this->setDateInput($rs['DateInput']);
                    $this->setInputBy($rs['InputBy']);
                    $this->setDateModified($rs['DateModified']);
                    $this->setLastModifiedBy($rs['LastModifiedBy']);
                    $this->setNrSubCategory($this->getNrSubCategory());
                    return $this;
                } else {
                    return NULL;
                }
            } else {
                return $rs;
            }
        }

        public function save()
        {
            if ($this->getEventTypeID() > 0) {
                $ret = $this->updateRecord();
            } else {
                $ret = $this->insertRecord();
            }
            return $ret;
        }

        public function getActiveEventType($orderCriteria = NULL)
        {
            $orderCriteria = ($orderCriteria == NULL) ? ' EventTypeName ' : $orderCriteria;
            return $this->getAllEventType($whereCriteria = ' RecordStatus = 1 AND DeletedFlag = 0 ', $orderCriteria);
        }

        public function getAllEventType($whereCriteria = NULL, $orderCriteria = NULL)
        {
            $rs = $this->loadDataFormStorage($loadObject = false, $whereCriteria, $orderCriteria);
            return $rs;
        }

        // Check if IsDefault been set in among all active records
        public function checkIsDefault($eventTypeID)
        {
            if ($eventTypeID) {
                $sql = "SELECT EventTypeID FROM MEDICAL_STAFF_EVENT_TYPE WHERE EventTypeID<>'" . $eventTypeID . "' AND IsDefault=1 AND DeletedFlag=0 LIMIT 1";
                $rs = $this->objDB->returnResultSet($sql);
                $ret = (count($rs) > 0) ? true : false;
            }
            else {
                $ret = false;
            }
            return $ret;
        }

        public function recordDuplicateChecking($testName, $eventTypeID = '')
        {
            if ($testName == '') {
                return true;
            }
            
            $checkingResult = 0;
            $addtionSQL = '';
            if ($eventTypeID != '') {
                $addtionSQL = ' AND EventTypeID != \'' . $eventTypeID . '\'';
            }
            
            $sql = 'SELECT COUNT(*) FROM MEDICAL_STAFF_EVENT_TYPE WHERE DeletedFlag = 0 AND EventTypeName=\'' . $this->objDB->Get_Safe_Sql_Query(trim($testName), '\'') . '\'' . $addtionSQL;
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($rs[0]['COUNT(*)'] > 0) {
                $checkingResult += 1;
            }
            
            return $checkingResult;
        }

        private function insertRecord()
        {
            global $cfg_medical;
            $DataArr = array();
            
            $DataArr['EventTypeName'] = $this->objDB->pack_value($this->getEventTypeName(), 'str');
            $DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(), 'int');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            $DataArr['DateInput'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(), 'int');
            
            $sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);
            
            $fieldStr = $sqlStrAry['sqlField'];
            $valueStr = $sqlStrAry['sqlValue'];
            
            $sql = 'INSERT INTO MEDICAL_STAFF_EVENT_TYPE (' . $fieldStr . ') VALUES (' . $valueStr . ')';
            
            $success = $this->objDB->db_db_query($sql);
            if ($success != 1) {
                return false;
            }
            
            $RecordID = $this->objDB->db_insert_id();
            $this->setEventTypeID($RecordID);
            return $RecordID;
        }

        private function updateRecord()
        {
            global $cfg_medical;
            
            $DataArr = array();
            
            $DataArr['EventTypeName'] = $this->objDB->pack_value($this->getEventTypeName(), 'str');
            $DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(), 'int');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            $DataArr['DateModified'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(), 'int');
            
            foreach ($DataArr as $fieldName => $data) {
                $updateDetails .= $fieldName . "=" . $data . ",";
            }
            
            // REMOVE LAST OCCURRENCE OF ",";
            $updateDetails = substr($updateDetails, 0, - 1);
            
            $sql = "UPDATE MEDICAL_STAFF_EVENT_TYPE set " . $updateDetails . " WHERE EventTypeID = " . $this->getEventTypeID();
            
            $result = $this->objDB->db_db_query($sql);
            
            if ($result != 1) {
                return false;
            }
            
            return $result;
        }
    }
}
?>