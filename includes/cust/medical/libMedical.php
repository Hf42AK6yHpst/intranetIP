<?php
// updating : 
/*
 * 2020-06-29 [Cameron]
 * - fix StudentID filter in getStudentEventReport() [case #F188374]
 *
 * 2019-09-19 [Tommy]
 * - change $selfOnly sql in getStudentLogList() and getStudentLogReport2() for PIC to view their cases
 * 
 * 2019-07-08 [Cameron]
 * - add function getHandoverAttachment(), getHandoverPicList()
 * - initialize $pageReportHandover in GET_MODULE_OBJ_ARR()
 *
 * 2019-07-05 [Cameron]
 * - add function getHandoverStudent() [case #P150622]
 * - modify getHandoverRecordAttachment(), set recordID to array()
 *
 * 2019-06-20 [Cameron]
 * - fix: typo: $recordID instead of $record in getHandoverRecord()
 * 
 * 2019-06-12 [Philips]
 * - Added getStudentRevisitReport() to for medical report with $sys_custom['medical']['MedicalReport']
 * 
 * 2019-06-03 [Philips]
 * - Modified getStudentLogList() and getStudentLogReport2(), added STUDENTLOG_VIEWALL access right checking
 * 
 * 2019-01-04 [Pun] [157819]
 * - Modified getEventLogInputUI(), added AllowAdminEditDeleteRecordOnly cust
 * 
 * 2019-01-04 [Cameron]
 * - add function getStaffEventRowForStudentEvent(), getStaffEventListOfStudentEvent()
 * 
  * 2019-01-03 [Cameron]
 * - add function getStudentEventReport, getStudentEventReportLayout()
 * 
 * 2019-01-02 [Cameron]
 * - add file sequence number in file list in getAttachFileList()
 * - add function getAllStudentEventTypes()
 * 
 * 2018-12-31 [Cameron]
 * - add function getMultipleSelectStaff(), getAllStaffEventTypes(), getStaffEventReport(), getStaffEventReportLayout()
 * 
 * 2018-12-27 [Cameron]
 * - add function isStaffEventStaff()
 * - modify isMedicalUser(), return true if isStaffEventStaff($UserID) 
 * 
 * 2018-12-21 [Cameron]
 * - add case staff_event in getCurrentAttachmentLayout()
 * - add function getStaffEventStudentEvent(), isRequiredCheckSelfStaffEvent()
 * 
 * 2018-12-20 [Cameron]
 * - add case staffEvent to handleUploadFile()
 * - add function getStaffEvents(), getStaffEventAttachment(), getStudentEventListOfStaffEvent()
 * 
 * 2018-12-19 [Cameron]
 * - add function getStaffSelection(), getEventRowForStaffEvent()
 * 
 * 2018-12-14 [Cameron]
 * - add parameter $eventTypeID and $idAndNameOnly to getStaffEventTypeLev2()
 * 
 * 2018-06-05 [Cameron]
 * - retrieve InputBy in getStudentLogList(), getMealDailyLogList(), getBowelDailyLogList(), getSleepLogList() [case #F133828]
 * 
 * 2018-05-29 [Cameron]
 * - fix: apply stripslashes and htmlspecialchars_decode to StatusName in header column in getMealReport(), getSleepReport(), getSleepReport3()
 * - remove nl2br for remarks field in getStudentLogReport2()
 * 
 * 2018-05-28 [Cameron]
 * - filter approved student only in getStudentIDByBarcode()
 * 
 * 2018-05-10 [Cameron]
 * - add function getBowelTime4Db(), getStudentIDByBarcode(), getBowelTime2Show()
 * - add parameter $withTitle to getUserNameByID()
 * 
 * 2018-04-27 [Cameron]
 * - apply stripslashes to EventType field in getEventRowForCase(), getCaseEventList(), getEventRowForStudentLog(), getStudentLogEventList()
 *
 * 2018-04-24 [Cameron]
 * - add parameter $userID to getSleepLogList()
 *
 * 2018-04-03 [Cameron]
 * - add function getEventRowForStudentLog(), deleteStudentLogEvent()
 * - handle delete student log - event record in deleteStudentLogRecords()
 *
 * 2018-03-29 [Cameron]
 * - add function getStudentLogInfo(), getStudentLogRowForEvent(), getEventStudentLog(), getEventStudentLogList(),
 * getStudentLogEventLayout(), getStudentLogEventList()
 * - show 'Item' if index=-1 in getEventLogInputUI()
 *
 * 2018-03-27 [Cameron]
 * - add staff event functions, e.g. getStaffEventType(), getStaffEventTypeLev2(), getTeacherOptionsByType()
 *
 * 2018-03-23 [Cameron]
 * - add function getEventCase(), getEventCaseList(), getCaseRowForEvent()
 *
 * 2018-03-19 [Cameron]
 * - add search by group in getStudentSelection()
 *
 * 2018-02-27 [Cameron]
 * - add function isRequiredCheckSelfEvent(), isRequiredCheckSelfCase(), isRequiredCheckSelfAwardScheme(), isRequiredCheckSelfRevisit()
 * - don't show delete button in event follow-up (getEventFollowupView) if the Login User is not the creator and not super-admin [case #F135176]
 *
 * 2018-02-01 [Cameron]
 * - retrieve GroupID in getSchemeStudentTarget() [case #F135093]
 *
 * 2018-01-24 [Cameron]
 * - change logo according to cust flag $sys_custom['medical']['CaringSystem'] in GET_MODULE_OBJ_ARR() [case #F121742]
 *
 * 2018-01-15 [Cameron]
 * - add parameter $userID to getStudentLogList()
 *
 * 2018-01-12 [Cameron]
 * - modify deleteStudentLogAttachment(), remove extra forwardslash (but not affect the result)
 * - fix deleteStudentLogRecords(), should also delete body parts and syndrome records when delete student log record
 *
 * 2018-01-08 [Cameron]
 * - add function getPersonInfo()
 *
 * 2018-01-03 [Cameron]
 * - modify deleteStudentLogRecords(), check if pass-in parameter is empty or not before process
 * - modify getEventLogInputUI(), don't show attachment which StudentLogID is 0
 *
 * 2017-12-08 [Cameron]
 * - add function getLev2List()
 *
 * 2017-12-07 [Cameron]
 * - add parameter $userID to getBowelDailyLogList()
 *
 * 2017-11-24 [Cameron]
 * - modify getEventLogInputUI() to set readonly for user who is not (superadmin or record creator or the last update person)
 * (only allow to edit/delete his/her own record) [case #P120778]
 * - retrieve InputBy field in getStudentLogList()
 *
 * 2017-10-13 [Cameron]
 * - add parameter $firstOption to getGroupSelection()
 * - add function getStudentNameListWClassNumberByGroupID()
 * - break $plugin['medical_module']['discipline'] into $plugin['medical_module']['discipline'] and $plugin['medical_module']['awardscheme']
 *
 * 2017-10-06 [Cameron]
 * - fix: don't filter by current academic year in getGroupByGroupID()
 *
 * 2017-09-19 [Cameron]
 * - add function getClassByStudentID(), add parameter $includeUserIdAry to getStudentNameListWClassNumberByClassName()
 * - modify getStudentLogEventContent(), don't show body parts and duration if they are empty
 *
 * 2017-09-18 [Cameron] [case #P115711]
 * - return last revisit date in getStudentLogReport2()
 * - add $email2pic_checkbox to getEventLogInputUI()
 * - add function getStudentLogEventContent()
 *
 * 2017-09-04 [Cameron]
 * - return last drug in getStudentLogReport2()
 *
 * 2017-08-29 [Cameron]
 * - add revisit in menu in GET_MODULE_OBJ_ARR()
 * - add function getMedicalGeneralSetting()
 * - add AuthenticationSetting and apply in GET_MODULE_OBJ_ARR()
 *
 * 2017-07-28 [Cameron]
 * - remove empty row in selection option list in getStudentSelection(), getAffectedPersonSelection(), getTeacherSelection(), getSchemeGroupSelection()
 * - set min-width and height for those selection box
 * 2017-07-27 [Cameron]
 * - change $plugin['medical_module']['discipline'] to $sys_custom['medical']['swapBowelAndSleep']
 * 2017-07-25 [Cameron]
 * - add parameter gender to getMealDailyLogList()
 * - add argument $append=1 to GetPresetText() in getEventLogInputUI()
 * - change getBowelTimeInterval(), add interval of 15 & 20 [case#P120984]
 * - add getAllActiveBowelStatus(), getUserIDByBarcode()
 * 2017-07-17 [Cameron]
 * - hide Bowel Report for $plugin['medical_module']['discipline'] = true. [case #F116540]
 * 2017-07-06 [Cameron]
 * - fix bug: not allow to select group when add PIC in getEventLogInputUI()
 * 2017-06-09 [Cameron]
 * - add event, case, award in management menu and event type in setting menu in GET_MODULE_OBJ_ARR()
 * 2017-03-22 [Cameron]
 * - change "New" button to "Select" button in getEventLogInputUI()
 * - set default pic to current login user [case #F114961]
 * 2016-01-07 [Tiffany] add function getAttachmentHTML_App()
 * 2015-08-27 [Pun] Fix cannot display group if no chinese name, getMedicalGroup()
 * 2015-01-22 [Cameron] Use flag $plugin['medical_module']['AlwaysShowBodyPart'] to control whether to show body parts.
 */
if (! defined("LIBMEDICAL_DEFINED")) // Preprocessor directive
{
    define("LIBMEDICAL_DEFINED", true);

    class libMedical
    {

        protected $objDB;

        private $AuthenticationSetting;

        public function libMedical()
        {
            $this->objDB = new libdb();
            
            $this->AuthenticationSetting = 0;
            $settings = $this->getGeneralSettings();
            if ($settings['AuthenticationSetting']) {
                $this->AuthenticationSetting = 1;
            }
        }

        function GET_MODULE_OBJ_ARR()
        {
            global $PATH_WRT_ROOT, $Lang, $CurrentPage, $LAYOUT_SKIN, $plugin, $intranet_root, $special_feature, $sys_custom;
            
            $rs = $this->getMedicalGeneralSetting("AllowAccessIP");
            if (! empty($rs)) {
                $allowed_IPs = trim($rs['SettingValue']);
                $ip_addresses = explode("\n", $allowed_IPs);
                
                checkCurrentIP($ip_addresses, $Lang['medical']['module']);
            }
            if ($this->AuthenticationSetting && $_SESSION['module_session_login']['Medical'] != 1) {
                $x = "";
                $x .= "<form id='form1' name='form1' method='post' action='$PATH_WRT_ROOT/home/module_access_checking.php'>";
                $x .= "<input type='hidden' name='module' value='Medical' />";
                $x .= "<input type='hidden' name='module_path' value='" . $_SERVER["REQUEST_URI"] . "' />";
                $x .= "</form>";
                $x .= "<script language='javascript'>";
                $x .= "document.form1.submit();";
                $x .= "</script>";
                echo $x;
                
                exit();
            } else if (! isset($_SESSION['module_session_login']['Medical'])) {
                // no need authentication - set session value
                $_SESSION['module_session_login']['Medical'] = 1;
            }
            
            // Reminder
            $pageMessage = 0;
            
            // MANAGEMENT
            $pageManagement = 0;
            $pageManagementMeal = 0;
            $pageManagementHandover = 0;
            
            // REPORT
            $pageReport = 0;
            $pageReportMeal = 0;
            $pageReportBowel = 0;
            $pageReportConvulsion = 0;
            $pageReportSleep = 0;
            $pageReportStudentEvent = 0;
            $pageReportStaffEvent = 0;
            $pageReportRevisit = 0;
            $pageReportHandover = 0;
            
            // SETTINGS
            $pageSettings = 0;
            $pageSettingsMeal = 0;
            $pageSettingsMealStatus = 0;
            $pageSettingsStudentLog = 0;
            $pageSettingsStudentSleep = 0;
            $pageSettingsHandover = 0;
            
            // PARENT INPUT
            $pageParentInput = 0;
            $pageParentBowelLog = 0;
            
            if (! is_array($CurrentPage)) {
                switch ($CurrentPage) {
                    case "RemainderWrite":
                        $pageRemainder = 1;
                        $pageRemainderWrite = 1;
                        break;
                    case "RemainderRead":
                        $pageRemainder = 1;
                        $pageRemainderReceive = 1;
                        break;
                    case "RemainderSent":
                        $pageRemainder = 1;
                        $pageRemainderSent = 1;
                        break;
                    
                    case "ManagementMeal":
                        $pageManagement = 1;
                        $pageManagementMeal = 1;
                        break;
                    
                    case "ManagementBowel":
                        $pageManagement = 1;
                        $pageManagementBowel = 1;
                        break;
                    
                    case "ManagementStudentLog":
                        $pageManagement = 1;
                        $pageManagementStudentLog = 1;
                        break;
                    
                    case "ManagementStudentSleep":
                        $pageManagement = 1;
                        $pageManagementStudentSleep = 1;
                        break;
                    
                    case "ManagementProblemRecord":
                        $pageManagement = 1;
                        $pageManagementProblemRecord = 1;
                        break;
                    
                    case "ManagementEvent":
                        $pageManagement = 1;
                        $pageManagementEvent = 1;
                        break;
                    
                    case "ManagementCase":
                        $pageManagement = 1;
                        $pageManagementCase = 1;
                        break;
                    
                    case "ManagementAward":
                        $pageManagement = 1;
                        $pageManagementAward = 1;
                        break;
                    
                    case "ManagementRevisit":
                        $pageManagement = 1;
                        $pageManagementRevisit = 1;
                        break;
                    
                    case "ManagementStaffEvent":
                        $pageManagement = 1;
                        $pageManagementStaffEvent = 1;
                        break;
                    case "ManagementHandover":
                        $pageManagement = 1;
                        $pageManagementHandover = 1;
                        break;
                    case "ParentBowelLog":
                        $pageParentInput = 1;
                        $pageParentBowelLog = 1;
                        break;
                    case "ParentStudentLogLog":
                        $pageParentInput = 1;
                        $pageParentStudentLogLog = 1;
                        break;
                    
                    case "ReportMeal":
                        $pageReport = 1;
                        $pageReportMeal = 1;
                        break;
                    
                    case "ReportBowel":
                        $pageReport = 1;
                        $pageReportBowel = 1;
                        break;
                    case "ReportConvulsion":
                        $pageReport = 1;
                        $pageReportConvulsion = 1;
                        break;
                    
                    case "ReportSleep":
                        $pageReport = 1;
                        $pageReportSleep = 1;
                        break;

                    case "ReportStudentEvent":
                        $pageReport = 1;
                        $pageReportStudentEvent = 1;
                        break;

                    case "ReportStaffEvent":
                        $pageReport = 1;
                        $pageReportStaffEvent = 1;
                        break;
                    case "ReportRevisit":
                        $pageReport = 1;
                        $pageReportRevisit = 1;
                        break;
                    case "ReportHandover":
                        $pageReport = 1;
                        $pageReportHandover = 1;
                        break;
                        
                    case "SettingsMeal":
                        $pageSettings = 1;
                        $pageSettingsMeal = 1;
                        break;
                    
                    case "SettingsMealStatus":
                        $pageSettings = 1;
                        $pageSettingsMealStatus = 1;
                        break;
                    
                    case "SettingsBowelStatus":
                        $pageSettings = 1;
                        $pageSettingsBowelStatus = 1;
                        break;
                    
                    case "SettingsStudentLog":
                        $pageSettings = 1;
                        $pageSettingsStudentLog = 1;
                        break;
                    
                    case "SettingsStudentSleep":
                        $pageSettings = 1;
                        $pageSettingsStudentSleep = 1;
                        break;
                    
                    case "SettingsNotice":
                        $pageSettings = 1;
                        $pageSettingsNotice = 1;
                        break;
                    
                    case "SettingsDefaultRemarks":
                        $pageSettings = 1;
                        $pageSettingsDefaultRemarks = 1;
                        break;
                    
                    case "SettingsGroupFilter":
                        $pageSettings = 1;
                        $pageSettingsGroupFilter = 1;
                        break;
                    
                    case "SettingsEventType":
                        $pageSettings = 1;
                        $pageSettingsEventType = 1;
                        break;
                    
                    case "SettingsStaffEventType":
                        $pageSettings = 1;
                        $pageSettingsStaffEventType = 1;
                        break;
                    
                    case "SettingsAccessRights":
                        $pageSettings = 1;
                        $pageSettingsAccessRights = 1;
                        break;
                    
                    case "SettingsSystemSetting":
                        $pageSettings = 1;
                        $pageSettingsSystemSetting = 1;
                        break;
                    
                    case "SettingsAllowedIPSetting":
                        $pageSettings = 1;
                        $pageSettingsAllowedIPSetting = 1;
                        break;
                    case "SettingsHandover":
                        $pageSettings = 1;
                        $pageSettingsHandover = 1;
                    
                    /*
                     * case "":
                     * break;
                     */
                }
            }
            $MenuArr = array();
            $MedicalPath = $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/medical/";
            
            // $hasModule = ( ($plugin['medical_module']['meal']) || ($plugin['medical_module']['bowel']) || ($plugin['medical_module']['studentLog']) || ($plugin['medical_module']['sleep']) );
            $hasModule = in_array(true, $plugin['medical_module'], true);
            
            // ###########
            // Menu Reminder
            // ###########
            
            $moduleRight = $this->checkAccessRight($_SESSION['UserID'], $pageAccess = '_NOTICE');
            if (($plugin['medical_module']['message']) && ($moduleRight)) {
                $MenuArr["Remainder"] = array(
                    $Lang['medical']['menu']['message'],
                    "",
                    $pageRemainder
                );
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SEND_NOTICE')) {
                    $MenuArr["Remainder"]["Child"]["Write"] = array(
                        $Lang['medical']['menu']['writeMessage'],
                        "/home/eAdmin/StudentMgmt/medical/?t=message.writeMessage",
                        $pageRemainderWrite
                    );
                }
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'RECEIVE_NOTICE')) {
                    $MenuArr["Remainder"]["Child"]["Read"] = array(
                        $Lang['medical']['menu']['readMessage'],
                        "/home/eAdmin/StudentMgmt/medical/?t=message.readReceiveMessage",
                        $pageRemainderReceive
                    );
                }
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SEND_NOTICE')) {
                    $MenuArr["Remainder"]["Child"]["Sent"] = array(
                        $Lang['medical']['menu']['sendMessage'],
                        "/home/eAdmin/StudentMgmt/medical/?t=message.readSendMessage",
                        $pageRemainderSent
                    );
                }
            }
            
            // ###########
            // Menu Management
            // ###########
            
            $moduleRight = $this->checkAccessRight($_SESSION['UserID'], $pageAccess = '_MANAGEMENT');
            if (($plugin['medical_module']['staffEvents']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW') || $this->isStaffEventStaff($_SESSION['UserID']))) {
                $bShowManagement = true;
            }
            else {
                $bShowManagement = false;
            }
            
            if (($moduleRight || $bShowManagement) && $hasModule) {
                $MenuArr["Management"] = array(
                    $Lang['medical']['menu']['management'],
                    "",
                    $pageManagement
                );
            }
            if (($plugin['medical_module']['meal']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'MEAL_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["Meal"] = array(
                    $Lang['medical']['menu']['meal'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.meal",
                    $pageManagementMeal
                );
            }
            if (($plugin['medical_module']['bowel']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["Bowel"] = array(
                    $Lang['medical']['menu']['bowel'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.bowel",
                    $pageManagementBowel
                );
            }
            if (($plugin['medical_module']['studentLog']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["StudentLog"] = array(
                    $Lang['medical']['menu']['studentLog'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.studentLog",
                    $pageManagementStudentLog
                );
            }
            if (($plugin['medical_module']['sleep']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["StudentSleep"] = array(
                    $Lang['medical']['menu']['studentSleep'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.sleep",
                    $pageManagementStudentSleep
                );
            }
            
            if ((($plugin['medical_module']['meal']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'MEAL_MANAGEMENT')) && (! libMedicalShowInput::absentAllowedInput('meal'))) || (($plugin['medical_module']['bowel']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_MANAGEMENT')) && (! libMedicalShowInput::absentAllowedInput('bowel'))) || (($plugin['medical_module']['studentLog']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_MANAGEMENT')) && (! libMedicalShowInput::absentAllowedInput('studentLog'))) || (($plugin['medical_module']['sleep']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_MANAGEMENT')) && (! libMedicalShowInput::absentAllowedInput('sleep')))) {
                $MenuArr["Management"]["Child"]["ProblemRecord"] = array(
                    $Lang['medical']['menu']['problemRecord'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.problemRecord",
                    $pageManagementProblemRecord
                );
            }
            
            if (($plugin['medical_module']['discipline']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["Event"] = array(
                    $Lang['medical']['menu']['event'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.event_list&clearCoo=1",
                    $pageManagementEvent
                );
            }
            if (($plugin['medical_module']['discipline']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'CASE_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["Case"] = array(
                    $Lang['medical']['menu']['case'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.case_list&clearCoo=1",
                    $pageManagementCase
                );
            }
            if (($plugin['medical_module']['awardscheme']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["Award"] = array(
                    $Lang['medical']['menu']['award'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.award_list&clearCoo=1",
                    $pageManagementAward
                );
            }
            if (($plugin['medical_module']['revisit']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_MANAGEMENT'))) {
                $MenuArr["Management"]["Child"]["Revisit"] = array(
                    $Lang['medical']['menu']['revisit'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.revisit_list&clearCoo=1",
                    $pageManagementRevisit
                );
            }
            
            if (($plugin['medical_module']['staffEvents']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW') || $this->isStaffEventStaff($_SESSION['UserID']))) {
                $MenuArr["Management"]["Child"]["StaffEvent"] = array(
                    $Lang['medical']['menu']['staffEvent'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.staffEventList&clearCoo=1",
                    $pageManagementStaffEvent
                );
            }
            
            if (($sys_custom['medical']['Handover']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_MANAGEMENT'))){
                $MenuArr["Management"]["Child"]["Handover"] = array(
                    $Lang['medical']['menu']['handover'],
                    "/home/eAdmin/StudentMgmt/medical/?t=management.handover_list",
                    $pageManagementHandover
                );
            }
            
            // ###########
            // Menu Report
            // ###########
            $moduleRight = $this->checkAccessRight($_SESSION['UserID'], $pageAccess = '_REPORT');
            if ($moduleRight && $hasModule) {
                $MenuArr["Report"] = array(
                    $Lang['medical']['menu']['report'],
                    "",
                    $pageReport
                );
            }
            if (($plugin['medical_module']['meal']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'MEAL_REPORT'))) {
                // $MenuArr["Report"]["Child"]["Meal"] = array($Lang['medical']['menu']['report_meal'], "/home/eAdmin/StudentMgmt/medical/?t=reports.mealReport", $pageReportMeal);
                $MenuArr["Report"]["Child"]["Meal"] = array(
                    $Lang['medical']['menu']['report_meal'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.mealReport2",
                    $pageReportMeal
                );
            }
            if (($plugin['medical_module']['bowel']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_REPORT'))) {
                $report = $sys_custom['medical']['swapBowelAndSleep'] ? 'bowelReport1' : 'bowelReport2';
                $MenuArr["Report"]["Child"]["Bowel"] = array(
                    $Lang['medical']['menu']['report_bowel'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports." . $report,
                    $pageReportBowel
                );
            }
            if (($plugin['medical_module']['studentLog']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_REPORT'))) {
                $MenuArr["Report"]["Child"]["Convulsion"] = array(
                    $Lang['medical']['menu']['report_convulsion'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.convulsionReport2",
                    $pageReportConvulsion
                );
            }
            if (($plugin['medical_module']['sleep']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_REPORT'))) {
                $MenuArr["Report"]["Child"]["Sleep"] = array(
                    $Lang['medical']['menu']['report_sleep'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.sleepReport1",
                    $pageReportSleep
                );
            }

            if (($plugin['medical_module']['discipline']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTEVENT_REPORT'))) {
                $MenuArr["Report"]["Child"]["StudentEvent"] = array(
                    $Lang['medical']['menu']['report_student_event'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.studentEventReport",
                    $pageReportStudentEvent
                );
            }

            if (($plugin['medical_module']['staffEvents']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_REPORT'))) {
                $MenuArr["Report"]["Child"]["StaffEvent"] = array(
                    $Lang['medical']['menu']['report_staff_event'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.staffEventReport",
                    $pageReportStaffEvent
                );
            }
            
            if ($sys_custom['medical']['MedicalReport'] && ($plugin['medical_module']['revisit']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_REPORT'))) {
                $MenuArr["Report"]["Child"]["Revisit"] = array(
                    $Lang['medical']['menu']['report_revisit'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.revisitReport",
                    $pageReportRevisit
                );
            }
            
            if ($sys_custom['medical']['Handover'] && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_REPORT'))){
                $MenuArr['Report']['Child']['Handover'] = array(
                    $Lang['medical']['menu']['report_handover'],
                    "/home/eAdmin/StudentMgmt/medical/?t=reports.handoverReport",
                    $pageReportHandover
                );
            }
            
            // ###########
            // Menu Settings
            // ###########
            $moduleRight = $this->checkAccessRight($_SESSION['UserID'], $pageAccess = '_SETTINGS');
            if (($hasModule) && ($moduleRight || $this->isSuperAdmin($_SESSION['UserID']))) {
                $MenuArr["Settings"] = array(
                    $Lang['medical']['menu']['settings'],
                    "",
                    $pageSettings
                );
            }
            
            if (($plugin['medical_module']['meal']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'MEAL_SETTINGS'))) {
                $MenuArr["Settings"]["Child"]["Meal"] = array(
                    $Lang['medical']['menu']['settings_meal'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.mealStatus",
                    $pageSettingsMealStatus
                );
            }
            
            if (($plugin['medical_module']['bowel']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_SETTINGS'))) {
                $MenuArr["Settings"]["Child"]["Bowel"] = array(
                    $Lang['medical']['menu']['settings_bowel'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.bowelStatus",
                    $pageSettingsBowelStatus
                );
            }
            
            if (($plugin['medical_module']['studentLog']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_SETTINGS'))) {
                $MenuArr["Settings"]["Child"]["StudentLog"] = array(
                    $Lang['medical']['menu']['settings_studentlog'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.studentLog",
                    $pageSettingsStudentLog
                );
            }
            
            if (($plugin['medical_module']['sleep']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_SETTINGS'))) {
                $MenuArr["Settings"]["Child"]["StudentSleep"] = array(
                    $Lang['medical']['menu']['settings_studentsleep'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.studentSleep",
                    $pageSettingsStudentSleep
                );
            }
            
            if (($plugin['medical_module']['message']) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'NOTICE_SETTINGS'))) {
                $MenuArr["Settings"]["Child"]["Notice"] = array(
                    $Lang['medical']['menu']['settings_noticeRemarks'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.messageDefaultTemplate",
                    $pageSettingsNotice
                );
            }
            
            if (($hasModule) && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'MEAL_SETTINGS') || $this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_SETTINGS') || $this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_SETTINGS') || $this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_SETTINGS'))) {
                $MenuArr["Settings"]["Child"]["DefaultRemarks"] = array(
                    $Lang['medical']['menu']['settings_defaultRemarks'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.defaultRemarks",
                    $pageSettingsDefaultRemarks
                );
            }
            
            if ($plugin['medical_module']['discipline'] && $this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_SETTINGS')) {
                $MenuArr["Settings"]["Child"]["EventType"] = array(
                    $Lang['medical']['menu']['settings_event_type'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.eventType",
                    $pageSettingsEventType
                );
            }
            
            if ($plugin['medical_module']['staffEvents'] && $this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_SETTINGS')) {
                $MenuArr["Settings"]["Child"]["StaffEventType"] = array(
                    $Lang['medical']['menu']['settings_staff_event_type'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.staffEventType",
                    $pageSettingsStaffEventType
                );
            }
            
            if (($hasModule) && ($this->isSuperAdmin($_SESSION['UserID']))) {
                $MenuArr["Settings"]["Child"]["GroupFilter"] = array(
                    $Lang['medical']['menu']['settings_groupFilter'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.groupFilterSetting",
                    $pageSettingsGroupFilter
                );
                $MenuArr["Settings"]["Child"]["AccessRights"] = array(
                    $Lang['medical']['menu']['settings_accessRight'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.accessRightList",
                    $pageSettingsAccessRights
                );
            }
            // $MenuArr["Settings"]["Child"]["AccessRights"] = array($Lang['medical']['menu']['settings_accessRight'], $MedicalPath."/home/eAdmin/StudentMgmt/medical/?t=settings.accessRight", $pageSettingsAccessRights);
            
            if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SYSTEM_SETTING')) {
                $MenuArr["Settings"]["Child"]["SystemSetting"] = array(
                    $Lang['medical']['menu']['settings_system_setting'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.system_setting",
                    $pageSettingsSystemSetting
                );
            }
            
            if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'ALLOWED_IP_SETTING')) {
                $MenuArr["Settings"]["Child"]["AllowedIPSetting"] = array(
                    $Lang['medical']['menu']['settings_allowed_ip'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.allowed_ip_setting",
                    $pageSettingsAllowedIPSetting
                );
            }
            if ($sys_custom['medical']['Handover'] && ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_SETTINGS'))){
                $MenuArr["Settings"]["Child"]["Handover"] = array(
                    $Lang['medical']['menu']['settings_handover'],
                    "/home/eAdmin/StudentMgmt/medical/?t=settings.handoverList",
                    $pageSettingsHandover
                );
            }
            
            // ###########
            // Menu Parent Input
            // ###########
            if (($this->getParentViewAccess('meal')) || ($this->getParentViewAccess('bowel')) || ($this->getParentViewAccess('studentLog')) || ($this->getParentViewAccess('sleep'))) {
                $MenuArr["Parent"] = array(
                    $Lang['medical']['menu']['input'],
                    "",
                    $pageParentInput
                );
            }
            if ($this->getParentViewAccess('bowel')) { // Login User is parent
                $MenuArr["Parent"]["Child"]["Bowel"] = array(
                    $Lang['medical']['menu']['bowel'],
                    "/home/eAdmin/StudentMgmt/medical/?t=parent.bowelRec",
                    $pageParentBowelLog
                );
            }
            if ($this->getParentViewAccess('studentLog')) { // Login User is parent
                $MenuArr["Parent"]["Child"]["StudentLog"] = array(
                    $Lang['medical']['menu']['studentLog'],
                    "/home/eAdmin/StudentMgmt/medical/?t=parent.studentLogRec",
                    $pageParentStudentLogLog
                );
            }
            
            // ####################
            // module information
            // ####################
            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Medical Caring System";'."\n";
            $js.= '</script>'."\n";
            
            $MODULE_OBJ['title'] = $Lang['medical']['module'].$js;
            $MODULE_OBJ['title_css'] = "menu_opened";
            $logo = $sys_custom['medical']['CaringSystem'] ? "icon_medical_caring.png" : "icon_medical.png";
            $MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/{$logo}";
            // $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_medical.gif";
            $MODULE_OBJ['menu'] = $MenuArr;
            
            return $MODULE_OBJ;
        }

        function getParentViewAccess($module)
        {
            global $plugin;
            
            if ((! $plugin['medical']) || ($_SESSION["UserType"] != USERTYPE_PARENT)) {
                return false;
            }
            
            $module = strtoupper($module);
            switch ($module) {
                case 'MEAL':
                    return ($plugin['medical_module']['meal'] && ! $plugin['medical_module']['blockParent']['meal']);
                case 'BOWEL':
                    return ($plugin['medical_module']['bowel'] && ! $plugin['medical_module']['blockParent']['bowel']);
                case 'STUDENTLOG':
                    return ($plugin['medical_module']['studentLog'] && ! $plugin['medical_module']['blockParent']['studentLog']);
                case 'SLEEP':
                    return ($plugin['medical_module']['sleep'] && ! $plugin['medical_module']['blockParent']['sleep']);
            }
            return false;
        }

        function getMedicalGroupCategory()
        {
            $sql = "SELECT 
				MGNF.GroupID,
				MGNF.CategoryName,
				IGC.GroupCategoryID
			FROM 
				MEDICAL_GROUP_NAME_FILTER MGNF
			LEFT JOIN
				INTRANET_GROUP_CATEGORY IGC
			ON
				IGC.CategoryName = MGNF.CategoryName
			WHERE
				MGNF.DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getMedicalGroup()
        {
            $currentAcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT DISTINCT IGC.CategoryName, IG.GroupID, IG.Title, IG.TitleChinese FROM
					INTRANET_GROUP_CATEGORY IGC
				INNER JOIN
					INTRANET_GROUP IG
				ON
					IGC.GroupCategoryID = IG.RecordType
				INNER JOIN
					MEDICAL_GROUP_NAME_FILTER MGNF
				ON
					IGC.CategoryName = MGNF.CategoryName
				WHERE
					MGNF.DeletedFlag = 0
				And
					IG.AcademicYearID = '{$currentAcademicYearID}'
				ORDER BY
					IGC.CategoryName,IG.GroupID;
			";
            $rs = $this->objDB->returnResultSet($sql);
            
            for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                $rs[$i]['Title'] = Get_Lang_Selection($rs[$i]['TitleChinese'], $rs[$i]['Title']);
            }
            
            return $rs;
        }

        function getMedicalGroupStudent($groupList)
        {
            if (is_array($groupList)) {
                $groupList = implode(',', $groupList);
            }
            $sql = "SELECT IUG.UserID FROM
						INTRANET_USERGROUP IUG
					INNER JOIN
						INTRANET_USER IU
					ON
						IU.UserID = IUG.UserID
					WHERE 
						IUG.GroupID IN ({$groupList})";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getGroupSelection($name, $defaultValue = '', $extraAttr = '', $firstOption = '')
        {
            global $Lang;
            $group = $this->getMedicalGroup();
            $select = "<select id='$name' name='$name' {$extraAttr}>";
            $select .= "<option value=''>" . ($firstOption ? $firstOption : $Lang['medical']['general']['allStudent']) . "</option>";
            
            $categoryName = '';
            $optionHTML = '';
            foreach ($group as $g) {
                if ($g['CategoryName'] != $categoryName) {
                    $categoryName = $g['CategoryName'];
                    $optionHTML .= "</optgroup><optgroup label='{$categoryName}'>";
                }
                $selected = '';
                if ($defaultValue != '' && $defaultValue == $g['GroupID']) {
                    $selected = ' selected="selected"';
                }
                $optionHTML .= "<option value='{$g['GroupID']}' {$selected}>{$g['Title']}</option>";
            }
            $optionHTML .= '</optgroup>';
            $optionHTML = substr($optionHTML, 11); // trim the first </optgroup>
            
            $select .= $optionHTML;
            $select .= "</select>";
            return $select;
        }

        function getMealDailyLogList($date, $timePeriod, $sleep, $classID = '', $gender = '')
        {
            global $intranet_session_language, $w2_cfg, $medical_cfg;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $conds = '';
            $thatDate = date('Y-m-d', strtotime($date));
            if ($classID != '') {
                $conds .= "And YC.YearClassID = '{$classID}'";
            }
            
            if ($gender != '') {
                $conds .= (" And IU.gender ='" . $gender . "'");
            }
            
            if ($sleep != 1) {
                if ($sleep == 2) {
                    $sleepConds = ' and IUPS.stayOverNight = ' . $medical_cfg['sleep_status']['StayIn']['value'] . ' ';
                } else {
                    $sleepConds = ' and (IUPS.stayOverNight = ' . $medical_cfg['sleep_status']['StayOut']['value'] . ' or IUPS.stayOverNight is null) ';
                }
                // $sleep = ($sleep =='1')?'1':'0';
                
                $conds .= $sleepConds;
            }
            $sleepTable .= "left Join
									INTRANET_USER_PERSONAL_SETTINGS IUPS
								On
									IUPS.UserID = IU.UserID 
							";
            
            $thatDateDay = date('d', strtotime($date));
            $thatDateMonth = date('m', strtotime($date));
            $thatDateYear = date('Y', strtotime($date));
            
            // The time period will affect the attendance table we use
            // 1. Breakfast - PM status that day before
            // 2. Lunch - AM status that day
            // 3. Dinner - PM status that day
            if ($timePeriod == $w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["breakfast"]) {
                $theDayBefore = date('Y-m-d', strtotime('-1 day', strtotime($thatDate)));
                $theDayBeforeMonth = date('m', strtotime('-1 day', strtotime($thatDate)));
                $theDayBeforeYear = date('Y', strtotime('-1 day', strtotime($thatDate)));
                
                $selectedDay = date('d', strtotime('-1 day', strtotime($thatDate)));
                $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$theDayBeforeYear}_{$theDayBeforeMonth}";
            } else {
                $selectedDay = $thatDateDay;
                $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$thatDateYear}_{$thatDateMonth}";
            }
            
            $MealTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . "{$thatDateYear}_{$thatDateMonth}";
            
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            $mealTableExists = hasDailyLogTableByName($MealTableName);
            
            $fieldList = array();
            if ($attendanceTableExists) {
                $sessionSelected = ($timePeriod == $w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["breakfast"] || $timePeriod == $w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["dinner"]) ? 'CSDL.PMStatus' : 'CSDL.AMStatus';
                $fieldList[] = $sessionSelected;
                
                // For attedance status field, 0= present and 2 = late, both mean the student @ school
                $fieldList[] = "({$sessionSelected} = '0' Or {$sessionSelected} = '2') as Present";
                
                $attendanceJoinTable = "
				Left Join
					(Select * From {$TableName} Where DayNumber = '{$selectedDay}' ) CSDL
				On
					CSDL.UserID = IU.UserID";
                
                $attendanceOrderBy = 'Present DESC, ';
            }
            if ($mealTableExists) {
                $fieldList[] = 'MSDML.Remarks';
                $fieldList[] = 'MSDML.MealStatus';
                $fieldList[] = getNameFieldByLang2('CB.') . ' As CreatedBy';
                $fieldList[] = 'IUM.ConfirmedName';
                $fieldList[] = 'MSDML.DateModified';
                $fieldList[] = 'MSDML.RecordID';
                $mealJoinTable = "
					Left Join
						(Select * From {$MealTableName} Where RecordDay = '{$thatDateDay}' And PeriodStatus = '{$timePeriod}') MSDML
					On
						 MSDML.UserID = IU.UserID
					Left Join
						(Select 
							" . getNameFieldByLang2() . " As ConfirmedName, UserID
						From
							INTRANET_USER
						) IUM
					On
						IUM.UserID = MSDML.ConfirmedUserID
                    LEFT JOIN INTRANET_USER CB ON CB.UserID=MSDML.InputBy	 		
					";
            }
            $fieldList[] = 'IU.UserID';
            
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
            $fieldList[] = 'IUPS.stayOverNight as Stay';
            
            $fieldSelected = implode(", ", $fieldList);
            
            $sql = "
					Select
						{$fieldSelected}
					From
						INTRANET_USER IU
					Inner Join
						YEAR_CLASS_USER YCU
					On
						IU.UserID = YCU.UserID
					Inner Join
						YEAR_CLASS YC
					On
						YC.YearClassID = YCU.YearClassID
					Inner Join
						YEAR Y
					On
						Y.YearID = YC.YearID
					{$sleepTable}
					{$attendanceJoinTable}
					{$mealJoinTable}
					Where
						YC.AcademicYearID = '{$AcademicYearID}' And IU.RecordStatus = '1'
						{$conds}
					Order By {$attendanceOrderBy} Y.Sequence, Y.YearName, YC.Sequence, ClassName, YCU.ClassNumber
				;
				";
            // {$attendanceOrderBy} YC.Sequence,ClassName, YCU.ClassNumber
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getBowelDailyLogList($date, $gender, $sleep, $classID = '', $userID = '')
        {
            global $intranet_session_language, $w2_cfg, $medical_cfg;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $conds = '';
            $thatDate = date('Y-m-d', strtotime($date));
            if ($classID != '') {
                $conds .= "And YC.YearClassID = '{$classID}'";
            }
            
            if ($sleep != 1) {
                if ($sleep == 2) {
                    $sleepConds = ' and IUPS.stayOverNight = ' . $medical_cfg['sleep_status']['StayIn']['value'] . ' ';
                } else {
                    $sleepConds = ' and (IUPS.stayOverNight = ' . $medical_cfg['sleep_status']['StayOut']['value'] . ' or IUPS.stayOverNight is null) ';
                }
                // $sleep = ($sleep =='1')?'1':'0';
                
                $conds .= $sleepConds;
            }
            $sleepTable .= "left Join
									INTRANET_USER_PERSONAL_SETTINGS IUPS
								On
									IUPS.UserID = IU.UserID 
							";
            
            if ($gender != '') {
                $conds .= (" And IU.gender ='" . $gender . "'");
            }
            
            if ($userID) {
                $conds .= (" And IU.UserID ='" . $userID . "'");
            }
            
            $thatDateDay = date('d', strtotime($date));
            $thatDateMonth = date('m', strtotime($date));
            $thatDateYear = date('Y', strtotime($date));
            
            $selectedDay = $thatDateDay;
            $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$thatDateYear}_{$thatDateMonth}";
            
            $BowelTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] . "{$thatDateYear}_{$thatDateMonth}";
            
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            $bowelTableExists = hasDailyLogTableByName($BowelTableName);
            
            $fieldList = array();
            if ($attendanceTableExists) {
                // For attedance status field, 0= present and 2 = late, both mean the student @ school
                // $fieldList [] = "(CSDL.AMStatus = '0' Or CSDL.AMStatus = '2') as Present"; // Should add PMStatus
                $fieldList[] = "(CSDL.AMStatus = '{$medical_cfg['CARD_STATUS_PRESENT']}' Or 
								CSDL.AMStatus = '{$medical_cfg['CARD_STATUS_LATE']}' Or
								CSDL.PMStatus = '{$medical_cfg['CARD_STATUS_PRESENT']}' Or 
								CSDL.PMStatus = '{$medical_cfg['CARD_STATUS_LATE']}'
								) as Present";
                
                $attendanceJoinTable = "
				Left Join
					(Select * From {$TableName} Where DayNumber = '{$selectedDay}' ) CSDL
				On
					CSDL.UserID = IU.UserID";
                
                $attendanceOrderBy = 'Present DESC, ';
            }
            if ($bowelTableExists) {
                $fieldList[] = 'MSDBL.RecordID';
                $fieldList[] = 'MSDBL.RecordTime';
                $fieldList[] = 'MSDBL.BowelID';
                $fieldList[] = 'MSDBL.Remarks';
                $fieldList[] = getNameFieldByLang2('CB.') . ' As CreatedBy';
                $fieldList[] = 'IUM.ModifyBy AS ModifyBy';
                $fieldList[] = 'MSDBL.DateModified';
                $bowelJoinTable = "
					Left Join
						(Select * From {$BowelTableName} Where RecordTime >= '{$thatDate} 00:00:00' AND RecordTime <= '{$thatDate} 23:59:59') MSDBL
					On
						 MSDBL.UserID = IU.UserID
					Left Join
						(Select 
							" . getNameFieldByLang2() . " As ModifyBy, UserID
						From
							INTRANET_USER
						) IUM
					On
						IUM.UserID = MSDBL.ModifyBy
                    LEFT JOIN INTRANET_USER CB ON CB.UserID=MSDBL.InputBy
					";
                $bowelOrder = ',MSDBL.RecordTime';
            }
            $fieldList[] = 'IU.UserID';
            
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
            $fieldList[] = 'IUPS.stayOverNight as Stay';
            
            $fieldSelected = implode(", ", $fieldList);
            
            $sql = "
					Select
						{$fieldSelected}
					From
						INTRANET_USER IU
					Inner Join
						YEAR_CLASS_USER YCU
					On
						IU.UserID = YCU.UserID
					Inner Join
						YEAR_CLASS YC
					On
						YC.YearClassID = YCU.YearClassID
					Inner Join
						YEAR Y
					On
						Y.YearID = YC.YearID
					{$sleepTable}
					{$attendanceJoinTable}
					{$bowelJoinTable}
					Where
						YC.AcademicYearID = '{$AcademicYearID}' And IU.RecordStatus = '1'
						{$conds}
					Order By {$attendanceOrderBy} Y.Sequence, Y.YearName, YC.Sequence, ClassName, YCU.ClassNumber $bowelOrder
				;
				";
            // return print_r($sql,true);
            // {$attendanceOrderBy} YC.Sequence,ClassName, YCU.ClassNumber
            
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAccessRightCount()
        {
            $sql = '
			Select
				MAG.GroupID, GroupTitle, count(*) As TotalNum
			From
				MEDICAL_ACCESS_GROUP MAG
			Inner Join
				MEDICAL_ACCESS_GROUP_MEMBER MAGM
			On
				MAG.GroupID = MAGM.GroupID
			INNER Join
				INTRANET_USER IU
			ON
				MAGM.UserID = IU.UserID
			Where
				MAG.RecordStatus = 1
			Group By
				MAG.GroupID, GroupTitle
		';
            $rs = $this->objDB->returnResultSet($sql);
            // debug_r($sql);
            return $rs;
        }

        // prefix is the table name/alias with "." , e.g. INTRANET_USER.
        function getClassNameByLang2($prefix = "", $displayLang = "")
        {
            global $intranet_session_language;
            
            $displayLang = $displayLang ? $displayLang : $intranet_session_language;
            $chi = ($displayLang == "b5" || $displayLang == "gb");
            if ($chi) {
                $firstChoice = "ClassTitleB5";
                $altChoice = "ClassTitleEN";
            } else {
                $firstChoice = "ClassTitleEN";
                $altChoice = "ClassTitleB5";
            }
            $username_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
            return $username_field;
        }

        function deleteAcessRightGroup($groupIDList)
        {
            $this->objDB->Start_Trans();
            if ($groupIDList) {
                $conds = ' And GroupID In ("' . implode('","', (array) $groupIDList) . '")';
            }
            $sql = "
			Delete From
				MEDICAL_ACCESS_GROUP
			Where
				1=1 $conds
			";
            $result[] = $this->objDB->db_db_query($sql);
            $sql = "
			Delete From
				MEDICAL_ACCESS_GROUP_MEMBER
			Where
				1=1 $conds
			";
            $result[] = $this->objDB->db_db_query($sql);
            
            $finalResult = (! in_array(false, (array) $result));
            if ($finalResult) {
                $this->objDB->Commit_Trans();
            } else {
                $this->objDB->RollBack_Trans();
            }
            
            return $finalResult;
        }

        function getAcessRightGroupDetail($groupID)
        {
            if ($groupID) {
                $conds = " And GroupID = '{$groupID}'";
            }
            $sql = "
			Select
				GroupID,
				GroupTitle,
				GroupDescription,
				GroupAccessRight,
				RecordStatus
			From
				MEDICAL_ACCESS_GROUP
			Where
				1=1 $conds
			";
            $groupDetail = $this->objDB->returnResultSet($sql);
            
            $sql = "
			Select
				IU.UserID, " . getNameFieldByLang2('IU.') . " As Name
			From
				MEDICAL_ACCESS_GROUP_MEMBER MAGM
			Inner Join
				INTRANET_USER IU
			On
				IU.UserID = MAGM.UserID 
			Where
				1=1 $conds
			";
            $groupUserIDList = $this->objDB->returnResultSet($sql);
            return array(
                $groupDetail[0],
                $groupUserIDList
            );
        }

        function updateAcessRightGroup($groupID, $groupTitle, $acessRightList, $userIDList)
        {
            $this->objDB->Start_Trans();
            
            $sql = "
			Update
				MEDICAL_ACCESS_GROUP
			SET
				GroupTitle = '" . $this->objDB->Get_Safe_Sql_Query($groupTitle) . "',
				GroupAccessRight = '" . implode(':', (array) $acessRightList) . "',
				DateModified = now(),
				LastModifiedBy = now()
			Where
				GroupID = '{$groupID}' And RecordStatus = 1
			;";
            
            $result[] = $this->objDB->db_db_query($sql);
            
            $sql = "
			Delete From
				MEDICAL_ACCESS_GROUP_MEMBER
			Where
				GroupID ='{$groupID}';
			";
            
            $result[] = $this->objDB->db_db_query($sql);
            foreach ($userIDList as $userID) {
                $sql = "
				Insert Into
					MEDICAL_ACCESS_GROUP_MEMBER
					(GroupID, UserID, DateInput)
				Values
					('{$groupID}','{$userID}', now())
				";
                $result[] = $this->objDB->db_db_query($sql);
            }
            
            $finalResult = (! in_array(false, (array) $result));
            if ($finalResult) {
                $this->objDB->Commit_Trans();
            } else {
                $this->objDB->RollBack_Trans();
            }
            
            return $finalResult;
        }

        function insertAcessRightGroup($groupTitle, $acessRightList, $userIDList)
        {
            $this->objDB->Start_Trans();
            
            $sql = "
				Insert Into
					MEDICAL_ACCESS_GROUP
				(GroupTitle, GroupAccessRight, DateInput, DateModified, LastModifiedBy)
				Values
				( '{$groupTitle}', '" . implode(':', (array) $acessRightList) . "', now(), now(), now() )
			";
            $result[] = $this->objDB->db_db_query($sql);
            $groupID = $this->objDB->db_insert_id();
            foreach ($userIDList as $userID) {
                $sql = "
				Insert Into
					MEDICAL_ACCESS_GROUP_MEMBER
					(GroupID, UserID, DateInput)
				Values
					('{$groupID}','{$userID}', now())
				";
                $result[] = $this->objDB->db_db_query($sql);
            }
            
            $finalResult = (! in_array(false, (array) $result));
            if ($finalResult) {
                $this->objDB->Commit_Trans();
            } else {
                $this->objDB->RollBack_Trans();
            }
            
            return $finalResult;
        }

        function getStudentList($studentIDList, $sleep = '', $genderNum = '', $showFullDetail = false)
        {
            global $medical_cfg;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $join = '';
            $conds = '';
            
            $conds .= " And IU.UserID In ('" . implode("','", $studentIDList) . "' )";
            if ($sleep != '' || $showFullDetail) {
                if ($sleep == $medical_cfg['sleep_status']['StayIn']['value']) {
                    $sleepConds = ' and IUPS.stayOverNight = ' . $medical_cfg['sleep_status']['StayIn']['value'];
                } else {
                    $sleepConds = ' and (IUPS.stayOverNight = ' . $medical_cfg['sleep_status']['StayOut']['value'] . ' or IUPS.stayOverNight is null)';
                }
                $sleep = ($sleep == '1') ? '1' : '0';
                $join .= "left Join
									INTRANET_USER_PERSONAL_SETTINGS IUPS
								On
									IUPS.UserID = IU.UserID
							";
                // $conds .= " And IUPS.stayOverNight = '{$sleep}'";
                if (! $showFullDetail) {
                    $conds .= $sleepConds;
                }
            }
            if ($genderNum != '1' && $genderNum != '') {
                if ($genderNum == '2') {
                    $gender = 'M';
                } elseif ($genderNum == '3') {
                    $gender = 'F';
                }
                $conds .= " And IU.gender = '{$gender}'";
            }
            
            $fieldList = array();
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = 'IU.UserID';
            
            if ($showFullDetail) {
                $fieldList[] = 'IUPS.stayOverNight';
                $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
            }
            $fieldSelected = implode(", ", $fieldList);
            $sql = "
			Select {$fieldSelected}
			From
				INTRANET_USER IU
			Inner Join
				YEAR_CLASS_USER YCU
			On
				IU.UserID = YCU.UserID
			Inner Join
				YEAR_CLASS YC
			On
				YC.YearClassID = YCU.YearClassID
			Inner Join
				YEAR Y
			On
				Y.YearID = YC.YearID
				{$join}
			Where
				YC.AcademicYearID = '{$AcademicYearID}'
				{$conds}
			Order By
				Y.Sequence, Y.YearName, YC.Sequence, YCU.ClassNumber				
			";
            return $this->objDB->returnResultSet($sql);
        }

        function isMedicalUser($userID)
        {
            global $plugin;
            
            // super admin set in the ROLE Management
            if ($this->isSuperAdmin($userID)) {
                return true;
            }
            
            $objDB = new libdb();
            $sql = "
				Select
					count(*) as numOfRecord
				From
					MEDICAL_ACCESS_GROUP MAG
				Inner Join
					MEDICAL_ACCESS_GROUP_MEMBER MAGM
				On
					MAG.GroupID = MAGM.GroupID
				Where
					MAGM.UserID = '{$userID}'
				;";
            
            $result = $objDB->returnResultSet($sql);
            $result = current($result);
            if ($result['numOfRecord'] == 0) {
                if ($plugin['medical_module']['staffEvents'] && $this->isStaffEventStaff($userID)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        }

        function isSuperAdmin($userID)
        {
            if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-medical"]) {
                return true;
            } else {
                return false;
            }
        }

        /*
         * Purpose: Check if a user is the child of a parent
         * @param : $parentID - INTRANET_USER.UserID of parent
         * @param : $childID - INTRANET_USER.UserID of child
         * @return : true if it's the child or false othwerwise
         */
        function isChildOfTheParent($parentID, $childID)
        {
            $sql = "SELECT 1 FROM INTRANET_PARENTRELATION WHERE ParentID = '{$parentID}' AND StudentID='{$childID}'";
            $rs = $this->objDB->returnVector($sql);
            if (count($rs) == 1) {
                return true;
            } else {
                return false;
            }
        }

        function checkAccessRight($userID, $pageAccess = '')
        {
            if ($this->isSuperAdmin($userID)) {
                return true;
            }
            if ($_SESSION['AccessRightInMedicalModule'] == '') {
                $objDB = new libdb();
                $sql = "
				Select
					GroupAccessRight 
				From
					MEDICAL_ACCESS_GROUP MAG
				Inner Join
					MEDICAL_ACCESS_GROUP_MEMBER MAGM
				On
					MAG.GroupID = MAGM.GroupID
				Where
					MAGM.UserID = '{$userID}'
				;";
                
                $result = $objDB->returnResultSet($sql);
                $accessRightList = array();
                foreach ((array) $result as $resultItem) {
                    
                    $tmpList = explode(':', $resultItem['GroupAccessRight']);
                    foreach ((array) $tmpList as $tmpItem) {
                        // $accessRightList[$tmpItem]=true;
                        list ($option, $module) = explode('_', $tmpItem);
                        $accessRightList[$module][$option] = true;
                    }
                }
                $_SESSION['AccessRightInMedicalModule'] = $accessRightList;
            }
            
            $module = '';
            $option = '';
            list ($option, $module) = explode('_', $pageAccess);
            
            if ($option == '') {
                // for menu
                return isset($_SESSION['AccessRightInMedicalModule'][$module]);
            } else {
                // for separate page
                return $_SESSION['AccessRightInMedicalModule'][$module][$option];
            }
            
            // return $_SESSION['AccessRightInMedicalModule'][$pageAccess];
        }

        function getAccessRightUserIdList($accessRight = '')
        {
            $userIdList = array();
            $objDB = new libdb();
            
            // # Super Admin START ##
            $sql = "SELECT
				DISTINCT RM.UserID
			FROM
				ROLE_MEMBER RM 
			LEFT JOIN 
				ROLE_RIGHT RR 
			ON 
				RM.RoleID=RR.RoleID 
			WHERE 
				RR.FunctionName='eAdmin-medical'";
            $rs = $objDB->returnResultSet($sql);
            foreach ($rs as $r) {
                $userIdList[] = $r['UserID'];
            }
            // # Super Admin END ##
            
            // # Medical access right START ##
            if ($accessRight == '') {
                $sql = "SELECT DISTINCT UserID FROM MEDICAL_ACCESS_GROUP_MEMBER";
                $rs = $objDB->returnResultSet($sql);
            } else {
                $sql = "SELECT 
					DISTINCT MAGG.UserID
				FROM 
					MEDICAL_ACCESS_GROUP_MEMBER MAGG
				INNER JOIN
					MEDICAL_ACCESS_GROUP MAG
				ON
					MAGG.GroupID=MAG.GroupID
				WHERE 
					MAG.GroupAccessRight LIKE '%{$accessRight}%'
				ORDER BY
					MAGG.UserID";
                $rs = $objDB->returnResultSet($sql);
            }
            foreach ($rs as $r) {
                if (! in_array($r['UserID'], $userIdList)) {
                    $userIdList[] = $r['UserID'];
                }
            }
            // # Medical access right END ##
            return $userIdList;
        }

        function getStudentLogPartsSettings()
        {
            $objDB = new libdb();
            $sql = 'select third.Lev3ID,third.Lev3Name,third.Lev3Code,fourth.Lev4ID,fourth.Lev4Name,fourth.Lev4Code,fourth.Lev3ID as parentId from MEDICAL_STUDENT_LOG_LEV3 as third left join MEDICAL_STUDENT_LOG_LEV4 as fourth on third.Lev3ID = fourth.Lev3ID';
            // debug_r($sql);
            $rs = $objDB->returnResultSet($sql);
            
            if (count($rs) == 0) {
                return array();
            }
            
            $thirdLevel = array();
            for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                // GROUP THE RESULT BY Lev3ID , Lev3Id is a primate KEY in table MEDICAL_STUDENT_LOG_LEV3
                $thirdLevel[$rs[$i]['Lev3ID']]['details'] = array(
                    'name' => $rs[$i]['Lev3Name'],
                    'id' => $rs[$i]['Lev3ID'],
                    'code' => $rs[$i]['Lev3Code']
                );
            }
            for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                if ($rs[$i]['parentId'] > 0) {
                    $thirdLevel[$rs[$i]['Lev3ID']]['child'][] = array(
                        'name' => $rs[$i]['Lev4Name'],
                        'id' => $rs[$i]['Lev4ID'],
                        'code' => $rs[$i]['Lev4Code']
                    );
                }
            }
            return $thirdLevel;
        }

        function getLev2List($level1ID = '')
        {
            $sql = "Select v2.Lev2ID, v2.Lev2Name From MEDICAL_STUDENT_LOG_LEV2 v2 where recordstatus = 1 and DeletedFlag = 0 ";
            if ($level1ID) {
                $sql .= "and Lev1ID='" . $level1ID . "'";
            }
            return $this->objDB->returnResultSet($sql);
        }

        function getLev3List($ConvulsionOnly = true)
        {
            global $medical_cfg;
            $sql = "Select v3.Lev3ID, v3.Lev3Name From MEDICAL_STUDENT_LOG_LEV3 v3 ";
            // if ($ConvulsionOnly)
            // {
            // $sql .= "INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID=v3.Lev2ID WHERE v2.Lev2Name='" . $medical_cfg['studentLog']['level2']['specialStatus'] . "' ";
            // }
            
            return $this->objDB->returnResultSet($sql);
        }

        function getLev4List($Lev3ID = '')
        {
            $sql = "Select Lev4ID, Lev4Name From MEDICAL_STUDENT_LOG_LEV4 Where Lev3ID = '{$Lev3ID}'";
            return $this->objDB->returnResultSet($sql);
        }

        function getLev2IDByName($Lev2Name)
        {
            $sql = "Select Lev2ID From MEDICAL_STUDENT_LOG_LEV2 Where Lev2Name = '{$Lev2Name}'";
            $rs = $this->objDB->returnResultSet($sql);
            
            $tempArray = array();
            foreach ((array) $rs as $item) {
                $tempArray[] = $item['Lev2ID'];
            }
            $rs = $tempArray;
            return $rs;
        }

        function getStudentLogList($date, $gender = '', $sleep = '', $classID = '', $userID = '')
        {
            global $intranet_session_language, $w2_cfg, $medical_cfg, $_SESSION, $sys_custom;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $conds = '';
            $fieldList = array();
            
            $thatDate = date('Y-m-d', strtotime($date));
            if ($classID != '') {
                $conds .= "And YC.YearClassID = '{$classID}'";
            }
            
            if ($userID) {
                $conds .= " And IU.UserID ='" . $userID . "'";
            }
            
            if ($sleep != '') {
                global $medical_cfg;
                if ($sleep == '1') {
                    $conds .= " And IUPS.stayOverNight = '1'";
                } else {
                    $conds .= " And (IUPS.stayOverNight = " . $medical_cfg['sleep_status']['StayOut']['value'] . " or IUPS.stayOverNight is null)";
                }
            }
            $sleepTable = "
				Left Join
					INTRANET_USER_PERSONAL_SETTINGS IUPS
				On
					IUPS.UserID = IU.UserID 
				";
            
            if ($gender != '') {
                $conds .= (" And IU.gender ='" . $gender . "'");
            }
            
            $thatDateDay = date('d', strtotime($date));
            $thatDateMonth = date('m', strtotime($date));
            $thatDateYear = date('Y', strtotime($date));
            
            $selectedDay = $thatDateDay;
            $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$thatDateYear}_{$thatDateMonth}";
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            if ($attendanceTableExists) {
                // For attendance status field, 0= present and 2 = late, both mean the student @ school
                $fieldList[] = "( (CSDL.AMStatus = '0' Or CSDL.AMStatus = '2') || (CSDL.PMStatus = '0' Or CSDL.PMStatus = '2') ) as Present";
                
                $attendanceJoinTable = "
				Left Join
					(Select * From {$TableName} Where DayNumber = '{$selectedDay}' ) CSDL
				On
					CSDL.UserID = IU.UserID";
                
                $attendanceOrderBy = 'Present DESC, ';
            }
            
            $fieldList[] = 'IU.UserID';
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
            $fieldList[] = 'IUPS.stayOverNight as Stay';
            $fieldList[] = 'MSL.RecordID';
            $fieldList[] = 'MSL.RecordTime';
            $fieldList[] = 'MSL.Duration';
            $fieldList[] = 'MSL.Remarks';
            $fieldList[] = 'MSL.PIC';
            $fieldList[] = 'MSL.DateModified';
            $fieldList[] = 'MSL.ModifyBy';
            $fieldList[] = 'MSL.InputBy';
            // $fieldList [] = 'MSLP.StudentLogID';
            $fieldList[] = 'MSLL1.Lev1Name';
            $fieldList[] = 'MSLL2.Lev2Name';
            // $fieldList [] = 'MSLL3.Lev3ID';
            // $fieldList [] = 'MSLL3.Lev3Name';
            // $fieldList [] = 'MSLL4.Lev4Name';
            $fieldList[] = 'IUM.ConfirmedName';
            $fieldList[] = getNameFieldByLang2('CB.') . ' As CreatedBy';
            
            $fieldSelected = implode(", ", $fieldList);
            
            // Access Group
            if($sys_custom['medical']['accessRight']['ViewAll'] && !($this->isSuperAdmin($_SESSION['UserID']) || $this->checkAccessRight($_SESSION['UserID'] ,'STUDENTLOG_VIEWALL'))){
//                $selfOnly = " AND InputBy = '{$_SESSION['UserID']}' ";
                $selfOnly = " AND (PIC REGEXP \"(^|,){$_SESSION['UserID']}(,|$)\" OR InputBy = '{$_SESSION['UserID']}') ";
            }
            $sql = "
					Select
						{$fieldSelected}
					From
						YEAR_CLASS_USER YCU
					Inner Join
						INTRANET_USER IU
					On
						IU.UserID = YCU.UserID
					Inner Join
						YEAR_CLASS YC
					On
						YC.YearClassID = YCU.YearClassID
					Inner Join
						YEAR Y
					On
						Y.YearID = YC.YearID
					Left Join
						(Select
							*
						From
							MEDICAL_STUDENT_LOG
						Where
							recordtime >=  '{$date} 00:00:00'
						And
							recordtime <= '{$date} 23:59:59'
                            {$selfOnly}
						) As MSL
					On
						MSL.UserID = IU.UserID

					Left Join
						MEDICAL_STUDENT_LOG_LEV1 MSLL1
					On 
						MSL.BehaviourOne = MSLL1.Lev1ID
					Left Join
						MEDICAL_STUDENT_LOG_LEV2 MSLL2
					On 
						MSL.BehaviourTwo = MSLL2.Lev2ID
					Left Join
						(Select 
							" . getNameFieldByLang2() . " As ConfirmedName, UserID
						From
							INTRANET_USER
						) IUM
					On
						IUM.UserID = MSL.ModifyBy
                    LEFT JOIN INTRANET_USER CB ON CB.UserID=MSL.InputBy
					{$sleepTable}
					{$attendanceJoinTable}
					Where
						YC.AcademicYearID = '{$AcademicYearID}' And IU.RecordStatus = '1'
						{$conds}
					Order By 
						{$attendanceOrderBy} Y.Sequence, Y.YearName, YC.Sequence, ClassName, YCU.ClassNumber, MSL.RecordTime
				;
				";
            // Left Join
            // MEDICAL_STUDENT_LOG_PARTS MSLP
            // On
            // MSL.RecordID = MSLP.StudentLogID
            // Left Join
            // MEDICAL_STUDENT_LOG_LEV3 MSLL3
            // On
            // MSLP.Level3ID = MSLL3.Lev3ID
            // Left Join
            // MEDICAL_STUDENT_LOG_LEV4 MSLL4
            // On
            // MSLP.Level4ID = MSLL4.Lev4ID
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getStudentLogInfo($studentLogID)
        {
            $cond = '';
            if (is_array($studentLogID)) {
                $cond .= " AND e.RecordID IN ('" . implode("','", $studentLogID) . "')";
            } else if ($studentLogID) {
                $cond .= " AND e.RecordID='" . $studentLogID . "'";
            }
            
            if ($studentLogID) {
                $sql = "SELECT  e.RecordID AS StudentLogID,
                                DATE_FORMAT(e.RecordTime,'%Y-%m-%d') AS RecordDate,
                                DATE_FORMAT(e.RecordTime,'%H:%i') AS RecordTime,
                                e.UserID,
                                lev1.Lev1Name,
                                lev2.Lev2Name,
                                e.Duration,
                                e.Remarks,
                                e.PIC
                        FROM
                                MEDICAL_STUDENT_LOG e
                        INNER JOIN 
                                INTRANET_USER u ON u.UserID=e.UserID
                        LEFT JOIN
                                MEDICAL_STUDENT_LOG_LEV1 lev1 ON lev1.Lev1ID=e.BehaviourOne
                        LEFT JOIN
                                MEDICAL_STUDENT_LOG_LEV2 lev2 ON lev2.Lev2ID=e.BehaviourTwo
                        WHERE 1 " . $cond;
                
                $rs = $this->objDB->returnResultSet($sql);
                
                return $rs;
            } else {
                return false;
            }
        }

        public function getStudentLogRowForEvent($studentLogID, $eventID = '')
        {
            global $Lang;
            
            $studentLogAry = $this->getStudentLogInfo($studentLogID);
            
            $x = '';
            for ($i = 0, $iMax = count($studentLogAry); $i < $iMax; $i ++) {
                $rs = $studentLogAry[$i];
                $studentInfoAry = $this->getStudentInfoByID($rs['UserID']);
                
                if (count($studentInfoAry)) {
                    $studentName = $studentInfoAry['Name'];
                    if (! empty($studentInfoAry['ClassName']) && ! empty($studentInfoAry['ClassNumber'])) {
                        $studentName .= '(' . $studentInfoAry['ClassName'] . '-' . $studentInfoAry['ClassNumber'] . ')';
                    }
                } else {
                    $studentName = '';
                }
                
                $lev1Type = $rs['Lev1Name'];
                $lev2Item = $rs['Lev2Name'];
                
                // no new line character here, for javascript string
                $x .= '<tr id="eventStudentLogRow' . $rs['StudentLogID'] . '">';
                $x .= '<td style="white-space:nowrap;"><a href="#" class="viewStudentLog" data-StudentLogID="' . $studentLogID . '">' . $rs['RecordDate'] . '</a></td>';
                $x .= '<td>' . $rs['RecordTime'] . '</td>';
                $x .= '<td>' . $studentName . '</td>';
                $x .= '<td>' . $rs['Lev1Name'] . '</td>';
                $x .= '<td>' . $rs['Lev2Name'] . '</td>';
                $x .= '<td>' . nl2br($rs['Remarks']) . '</td>';
                $x .= '<td><span class="table_row_tool"><a class="delete removeStudentLog" title="' . $Lang['medical']['studentLog']['RemoveStudentLog'] . '" data-StudentLogID="' . $rs['StudentLogID'] . '" data-EventID="' . $eventID . '"></a></span>';
                $x .= '<input type="hidden" name="selStudentLogID[]" class="selectStudentLog" value="' . $rs['StudentLogID'] . '"></td>';
                $x .= '</tr>';
            }
            return $x;
        }

        function getSleepLogList($date, $gender = '', $sleep = '', $classID = '', $userID = '')
        {
            global $intranet_session_language, $w2_cfg, $medical_cfg;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $conds = '';
            $fieldList = array();
            
            $thatDate = date('Y-m-d', strtotime($date));
            if ($classID != '') {
                $conds .= "And YC.YearClassID = '{$classID}'";
            }
            
            if ($sleep != '1') {
                global $medical_cfg;
                if ($sleep == '2') {
                    $conds .= " And IUPS.stayOverNight = '1'";
                } else {
                    $conds .= " And (IUPS.stayOverNight = " . $medical_cfg['sleep_status']['StayOut']['value'] . " or IUPS.stayOverNight is null)";
                }
            }
            $sleepTable = "
				Left Join
					INTRANET_USER_PERSONAL_SETTINGS IUPS
				On
					IUPS.UserID = IU.UserID 
				";
            
            if ($gender != '1') {
                $gender = ($gender == '2') ? 'M' : 'F';
                $conds .= (" And IU.gender ='" . $gender . "'");
            }
            
            if ($userID) {
                $conds .= (" And IU.UserID ='" . $userID . "'");
            }
            
            $thatDateDay = date('d', strtotime($date));
            $thatDateMonth = date('m', strtotime($date));
            $thatDateYear = date('Y', strtotime($date));
            
            $selectedDay = $thatDateDay;
            $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$thatDateYear}_{$thatDateMonth}";
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            if ($attendanceTableExists) {
                // For attendance status field, 0= present and 2 = late, both mean the student @ school
                // $fieldList [] = "(CSDL.AMStatus = '0' Or CSDL.AMStatus = '2') as Present"; // Just Handel PM
                $fieldList[] = "(CSDL.PMStatus = '{$medical_cfg['CARD_STATUS_PRESENT']}' Or CSDL.PMStatus = '{$medical_cfg['CARD_STATUS_LATE']}') as Present";
                
                $attendanceJoinTable = "
				Left Join
					(Select * From {$TableName} Where DayNumber = '{$selectedDay}' ) CSDL
				On
					CSDL.UserID = IU.UserID";
                
                $attendanceOrderBy = 'Present DESC, ';
            }
            
            $fieldList[] = 'IU.UserID';
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
            $fieldList[] = 'IUPS.stayOverNight as Stay';
            $fieldList[] = 'MSS.RecordID';
            $fieldList[] = 'MSS.SleepID';
            $fieldList[] = 'MSS.ReasonID';
            $fieldList[] = 'MSS.Frequency';
            $fieldList[] = 'MSS.Remarks';
            $fieldList[] = 'MSS.DateModified';
            $fieldList[] = 'IUM.ModifyByName';
            $fieldList[] = getNameFieldByLang2('CB.') . ' As CreatedBy';
            
            $fieldSelected = implode(", ", $fieldList);
            
            $sql = "
					Select
						{$fieldSelected}
					From
						YEAR_CLASS_USER YCU
					Inner Join
						INTRANET_USER IU
					On
						IU.UserID = YCU.UserID
					Inner Join
						YEAR_CLASS YC
					On
						YC.YearClassID = YCU.YearClassID
					Inner Join
						YEAR Y
					On
						Y.YearID = YC.YearID
					Left Join
						(Select
							*
						From
							MEDICAL_STUDENT_SLEEP
						Where
							recordtime >=  '{$date} 00:00:00'
						And
							recordtime <= '{$date} 23:59:59'
						) As MSS
					On
						MSS.UserID = IU.UserID
					Left Join
						(Select 
							" . getNameFieldByLang2() . " As ModifyByName, UserID
						From
							INTRANET_USER
						) IUM
					On
						IUM.UserID = MSS.ModifyBy
                    LEFT JOIN INTRANET_USER CB ON CB.UserID=MSS.InputBy

					{$sleepTable}
					{$attendanceJoinTable}
					Where
						YC.AcademicYearID = '{$AcademicYearID}' And IU.RecordStatus = '1'
						{$conds}
					Order By 
						{$attendanceOrderBy} Y.Sequence, Y.YearName, YC.Sequence, ClassName, YCU.ClassNumber
				;
				";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getForStudentLog($userID)
        {
            $fieldList = array();
            $fieldList[] = 'IU.UserID';
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
            $fieldList[] = 'YCU.ClassNumber';
            
            $fieldSelected = implode(", ", $fieldList);
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $sql = "
					Select
						{$fieldSelected}
					From
						INTRANET_USER IU
					Inner Join
						YEAR_CLASS_USER YCU
					On
						IU.UserID = YCU.UserID
					Inner Join
						YEAR_CLASS YC
					On
						YC.YearClassID = YCU.YearClassID
					Where
						YC.AcademicYearID = {$AcademicYearID} And IU.UserID = {$userID} Limit 1
					";
            
            $rs = $this->objDB->returnResultSet($sql);
            return $rs[0];
        }

        function getPICInfo($picList)
        {
            $conds .= "And UserID In ({$picList})";
            
            $sql = "
			Select UserID, " . getNameFieldByLang2('') . " As Name
			From
				INTRANET_USER
			Where
				1
				{$conds}
			";
            $rs = $this->objDB->returnResultSet($sql);
            $resultSet = array();
            foreach ((array) $rs as $item) {
                $resultSet[] = array(
                    $item['UserID'],
                    $item['Name']
                );
                ;
            }
            
            return $resultSet;
        }

        function getSelectionBox($id, $name, $valueArray, $valueSelected = "", $class = "", $otherMembers = "")
        {
            $returnStr = '';
            $returnStr .= "<select class='{$class}' id='{$id}' name='{$name}' {$otherMembers}>";
            foreach ((array) $valueArray as $key => $valueItem) {
                $selected = '';
                if ($key == $valueSelected) {
                    $selected = 'selected';
                }
                $returnStr .= "<option $selected value=\"{$key}\">{$valueItem}</option>";
            }
            $returnStr .= "</select>";
            
            return $returnStr;
        }

        function getLevel3ListByID($Id)
        {
            $sql = "Select DISTINCT Level3ID From MEDICAL_STUDENT_LOG_PARTS Where StudentLogID = '{$Id}';";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getLev4SelectedList($Id, $Lev3ID)
        {
            $sql = "Select RecordID, Level3ID, Level4ID From MEDICAL_STUDENT_LOG_PARTS Where StudentLogID = '{$Id}' And Level3ID = '{$Lev3ID}';";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAttachmentDetail($IdList)
        {
            $IdListForSQL = implode("','", (array) $IdList);
            $sql = "Select RecordID, RenameFile, OrgFileName From MEDICAL_STUDENT_LOG_DOCUMENT Where StudentLogID In ('{$IdListForSQL}');";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getCheckBox($id, $name, $fieldName, $value, $valueSelected = '', $class = "", $otherMemebers = "")
        {
            $returnStr .= "<input type='checkbox' name='{$name}' value='{$value}' id='{$id}' class='{$class}' {$otherMemebers}/><label for='{$id}'>{$fieldName}</label>&nbsp;&nbsp;";
            return $returnStr;
        }

        function getFileDetailInfo($recordID)
        {
            $sql = "Select RecordID, RenameFile, OrgFileName,FileType,FolderPath From MEDICAL_STUDENT_LOG_DOCUMENT Where RecordID = '{$recordID}';";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs[0];
        }

        function getFileDetailUserID($recordID)
        {
            $sql = "Select
				MSL.UserID
			From 
				MEDICAL_STUDENT_LOG_DOCUMENT MSLD 
			LEFT JOIN
				MEDICAL_STUDENT_LOG MSL
			ON
				MSLD.StudentLogID = MSL.RecordID
			Where 
				MSLD.RecordID = '{$recordID}'";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs[0]['UserID'];
        }

        function hasAttachment($recordID)
        {
            $sql = "Select count(*) As RS From MEDICAL_STUDENT_LOG_DOCUMENT Where StudentLogID = '{$recordID}';";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs[0]['RS'];
        }

        function getAttachmentHTML($Id, $attachmentDetailList, $deleteBtn = true)
        {
            global $Lang, $image_path;
            
            $attachmentCounter = 0;
            $uploadedFilesHTML = '';
            foreach ((array) $attachmentDetailList as $attachmentDetail) {
                if ($deleteBtn) {
                    $deleteAttachmentBtn = "<span class='table_row_tool'><a class='deleteSavedAttachmentBtn delete' data-Id='{$Id}' data-attachmentId='{$attachmentDetail['RecordID']}' title='" . $Lang['Btn']['Delete'] . "'></a></span>";
                }
                ++ $attachmentCounter;
                $uploadedFilesHTML .= "<input type='hidden' name='event[{$Id}][isfileUploadedPresent][{$attachmentDetail['RecordID']}]' id='event[{$Id}][isfileUploadedPresent][{$attachmentDetail['RecordID']}]' value='1'/>" . "<span  class='event[{$Id}][fileUploaded][]'>{$attachmentCounter}) " . "<img src='{$image_path}/icon/attachment_blue.gif' border=0 /><a href='?t=management.fileDownload&RecordID={$attachmentDetail['RecordID']}' target='_blank' >{$attachmentDetail['OrgFileName']}</a>" . "</span>{$deleteAttachmentBtn}<br />";
            }
            
            return $uploadedFilesHTML;
        }

        function getAttachmentHTML_App($Id, $attachmentDetailList, $deleteBtn = true)
        {
            global $Lang, $image_path;
            
            $attachmentCounter = 0;
            $uploadedFilesHTML = '';
            foreach ((array) $attachmentDetailList as $attachmentDetail) {
                if ($deleteBtn) {
                    $deleteAttachmentBtn = "<span class='table_row_tool'><a class='deleteSavedAttachmentBtn delete' data-Id='{$Id}' data-attachmentId='{$attachmentDetail['RecordID']}' title='" . $Lang['Btn']['Delete'] . "' href='#myModal'>";
                    $deleteAttachmentBtn .= '<img src="/home/eClassApp/common/web_module/assets/img/icon_trash.png" class="img_target" width="20px">';
                    $deleteAttachmentBtn .= "</a></span>";
                    $attachmentRowID = "id='AttachmentRow_{$attachmentDetail['RecordID']}'";
                } else {
                    $attachmentRowID = "";
                }
                ++ $attachmentCounter;
                // $uploadedFilesHTML .= "<input type='hidden' name='event[{$Id}][isfileUploadedPresent][{$attachmentDetail['RecordID']}]' id='event[{$Id}][isfileUploadedPresent][{$attachmentDetail['RecordID']}]' value='1'/>".
                $uploadedFilesHTML .= "<span {$attachmentRowID}><span  class='event[{$Id}][fileUploaded][]'><span class='attachment_counter'>{$attachmentCounter})</span> " . "<img src='{$image_path}/icon/attachment_blue.gif' border=0 /><a href='fileDownload.php?RecordID={$attachmentDetail['RecordID']}' target='_blank' >{$attachmentDetail['OrgFileName']}</a>" . "</span> {$deleteAttachmentBtn}<br /></span>";
            }
            
            return $uploadedFilesHTML;
        }

        function geConvulatonID()
        {
            global $medical_cfg;
            
            $specialStatus = $medical_cfg['studentLog']['level2']['specialStatus'];
            $sql = "Select Lev2ID From MEDICAL_STUDENT_LOG_LEV2 Where Lev2Name ='{$specialStatus}'";
            $rs = $this->objDB->returnResultSet($sql);
            
            $tempArray = array();
            foreach ($rs as $item) {
                $tempArray[] = $item['Lev2ID'];
            }
            return $tempArray;
        }

        function getSleepLevel2List($reasonIDList)
        {
            if ($reasonIDList) {
                $whereReasonID = "ReasonID In ('" . implode("','", (array) $reasonIDList) . "')";
            }
            $sql = "
			Select
				MSSR.ReasonID, MSSR.ReasonName, (MSS.StatusName = '補充') as isComment
			From
				MEDICAL_SLEEP_STATUS_REASON MSSR
			Inner Join
				MEDICAL_SLEEP_STATUS MSS
			On
				MSS.StatusID = MSSR.StatusID
			Where
				{$whereReasonID}
			Order By
				isComment, MSS.StatusID, ReasonID
			;";
            $rs = $this->objDB->returnResultSet($sql);
            $tempList = array();
            foreach ((array) $rs as $item) {
                $tempList[$item['ReasonID']]['ReasonName'] = $item['ReasonName'];
                $tempList[$item['ReasonID']]['isComment'] = $item['isComment'];
            }
            return $tempList;
        }

        function getSleepCountDayByStudentID($userIDList, $startDate, $endDate)
        {
            if ($userIDList) {
                $whereUserID = "And MSS.UserID In ('" . implode("','", (array) $userIDList) . "')";
            }
            $fieldList = array();
            $fieldSelected = implode(", ", $fieldList);
            
            $sql = "
			Select
				DISTINCT recordtime, UserID
			From
				MEDICAL_STUDENT_SLEEP MSS
			Where
				MSS.recordtime >=  '{$startDate} 00:00:00'
			And
				MSS.recordtime <= '{$endDate} 23:59:59'
				{$whereUserID}
			";
            
            $sql = "
				Select count(*) As Count, UserID From ({$sql}) RS Group By UserID
				";
            
            $rs = $this->objDB->returnResultSet($sql);
            
            $tempArray = array();
            foreach ($rs as $item) {
                $tempArray[$item['UserID']] = $item['Count'];
            }
            return $tempArray;
        }

        /*
         * For $forReport3=true, it returns,
         * array => (
         * [year] => (
         * [month] => (
         * [date] => (
         * [reasonID] => (
         * array => (
         * ["ReasonName"]
         * ["Frequency"]
         * ["Color"]
         * ["Remarks"]
         * ),
         * array => (
         * ["ReasonName"]
         * ["Frequency"]
         * ["Color"]
         * ["Remarks"]
         * )
         * )
         * )
         * )
         * )
         * )
         */
        function getSleepReportData($reasonIDList, $userIDList, $startDate, $endDate, $getReasonName = false, $forReport3 = false)
        {
            global $medical_cfg;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $fieldList = array();
            
            if ($reasonIDList) {
                $whereReasonID = "And MSS.ReasonID In ('" . implode("','", (array) $reasonIDList) . "')";
            }
            if ($userIDList) {
                $whereUserID = "And IU.UserID In ('" . implode("','", (array) $userIDList) . "')";
            }
            if ($getReasonName) {
                $fieldList[] = "MSSR.ReasonName";
                $joinReasonName = "
								Left Join
									MEDICAL_SLEEP_STATUS_REASON MSSR
								On
									MSSR.ReasonID = MSS.ReasonID
								";
            }
            
            $fieldList[] = getNameFieldByLang2('IU.') . ' as Name ';
            $fieldList[] = 'IU.UserID';
            $fieldList[] = 'MSS.ReasonID';
            $fieldList[] = 'MSS.recordtime';
            $fieldList[] = 'MSS.Frequency';
            $fieldList[] = 'MSStatus.StatusName';
            $fieldList[] = "CASE 
								WHEN MSStatus.StatusName ='{$medical_cfg['studentSleep']['level1']['comments']}' THEN MSS.ReasonID
								ELSE '-1'
							END As Filter";
            
            $fieldList[] = 'MSS.Remarks';
            if ($forReport3) {
                $fieldList[] = 'MSStatus.Color';
            }
            
            $fieldSelected = implode(", ", $fieldList);
            
            $sql = "
			Select
				{$fieldSelected}
			From
				INTRANET_USER IU
			Inner Join
				YEAR_CLASS_USER YCU
			On
				IU.UserID = YCU.UserID
			Inner Join
				YEAR_CLASS YC
			On
				YC.YearClassID = YCU.YearClassID
			Inner Join
				YEAR Y
			On
				Y.YearID = YC.YearID
			Left Join
				MEDICAL_STUDENT_SLEEP MSS
			On
				MSS.UserID = IU.UserID
			Left Join
				MEDICAL_SLEEP_STATUS MSStatus
			On
				MSStatus.StatusID = MSS.SleepID
			{$joinReasonName}
			Where
				MSS.recordtime >=  '{$startDate} 00:00:00'
			And
				MSS.recordtime <= '{$endDate} 23:59:59'
			And
				YC.AcademicYearID = {$AcademicYearID}
				{$whereUserID}
				{$whereReasonID}
			Order By 
				Y.Sequence, Y.YearName, YC.Sequence, ClassName, YCU.ClassNumber";
            // extended SQL for handling group by DATE And Filter Remarks features
            // $sql = "
            // Select *
            // From ({$sql}) As RS Group By RS.recordtime, RS.UserID, RS.Filter
            // ";
            $tempArray = array();
            $rs = $this->objDB->returnResultSet($sql);
            // Data Structure For Report 3
            if ($forReport3) {
                
                foreach ((array) $rs as $item) {
                    $year = date('Y', strtotime($item['recordtime']));
                    $month = date('m', strtotime($item['recordtime']));
                    $day = date('d', strtotime($item['recordtime']));
                    $tempArray[$year][$month][$day][$item['ReasonID']][] = array(
                        'ReasonName' => $item['ReasonName'],
                        'Frequency' => $item['Frequency'],
                        'Color' => $item['Color'],
                        'Remarks' => $item['Remarks']
                    );
                }
            } // Data Structure For Report 2
else if ($getReasonName) {
                foreach ((array) $rs as $item) {
                    $tempArray[$item['UserID']]['Name'] = $item['Name'];
                    if (strpos($item['ReasonName'], $medical_cfg['studentSleep']['sleepWell']) !== false) {
                        ++ $tempArray[$item['UserID']]['sleepWell'];
                        ++ $tempArray[$item['UserID']]['totalCount'];
                    } else if (strpos($item['ReasonName'], $medical_cfg['studentSleep']['healthProblem']) !== false) {
                        ++ $tempArray[$item['UserID']]['healthProblem'];
                        ++ $tempArray[$item['UserID']]['totalCount'];
                    } else if (strpos($item['ReasonName'], $medical_cfg['studentSleep']['emotionProblem']) !== false) {
                        ++ $tempArray[$item['UserID']]['emotionProblem'];
                        ++ $tempArray[$item['UserID']]['totalCount'];
                    } else if (strpos($item['ReasonName'], $medical_cfg['studentSleep']['otherProblem']) !== false) {
                        ++ $tempArray[$item['UserID']]['otherProblem'];
                        ++ $tempArray[$item['UserID']]['totalCount'];
                    }
                }
            } // Data Structure For Report 1
else {
                foreach ((array) $rs as $item) {
                    $tempArray[$item['UserID']]['Name'] = $item['Name'];
                    // ++$tempArray[$item['UserID']]['reasonIDList'][$item['ReasonID']]; // 2014-04-28 Change date count to times count
                    $tempArray[$item['UserID']]['reasonIDList'][$item['ReasonID']] += $item['Frequency'];
                    
                    $date = date("Y-m-d", strtotime($item['recordtime']));
                    if (trim($item['Remarks']) != '') {
                        $tempArray['Remarks'][$item['UserID']][$date][] = trim($item['Remarks']);
                    }
                }
            }
            return $tempArray;
        }

        function getEventLogInputUI($Id, $linterface, $eventDetail = '', $index = 0, $isReadOnly = false)
        {
            global $Lang, $eEnrollment, $medical_cfg, $plugin, $sys_custom;
            
            if ($eventDetail == '') {
                $eventDetail = new StudentLog();
                $pic = $_SESSION['UserID']; // set default pic to current login user
            } else {
                if ($eventDetail->getPIC() != '') {
                    $pic = $eventDetail->getPIC();
                } else {
                    $pic = $_SESSION['UserID']; // set default pic to current login user
                }
            }
            // //////////////////////// UI Items ////////////////////////////////////////////////
            // Time Hour
            $valueArray = array();
            $hourList = range(0, 23);
            foreach ($hourList as $hour) {
                $valueArray['time_hour'][$hour] = str_pad($hour, 2, "0", STR_PAD_LEFT);
                ;
            }
            
            // Time Minute
            $minList = range(0, 59);
            foreach ($minList as $min) {
                $valueArray['time_min'][$min] = str_pad($min, 2, "0", STR_PAD_LEFT);
                ;
            }
            
            // Time Second
            $secList = range(0, 59);
            foreach ($secList as $sec) {
                $valueArray['time_sec'][$sec] = str_pad($sec, 2, "0", STR_PAD_LEFT);
                ;
            }
            
            // Level 3 List
            $Lev3List = $this->getLev3List();
            foreach ((array) $Lev3List as $Lev3) {
                $valueArray['lev3List'][$Lev3['Lev3ID']] = $Lev3['Lev3Name'];
            }
            // //////////////////////////////////////////////////////////////////////////////////
            
            $objLogLev1 = new studentLogLev1();
            // get the first one ID of Level 1 in the list, then push to object Level 2
            if ($eventDetail->getBehaviourOne() == '') {
                $Lev1List = $objLogLev1->getActiveStatus($orderCriteria = 'Lev1Code');
                $eventDetail->setBehaviourOne($Lev1List[0]['Lev1ID']);
            }
            $objLogLev2 = new studentLogLev2($eventDetail->getBehaviourOne());
            
            $timeLasted = explode(':', $valueSelected = $eventDetail->getDuration());
            
            $objLogLev3 = $this->getLevel3ListByID($Id);
            
            $picList = array();
            $picList = $this->getPICInfo($pic);
            
            if ($eventDetail->getRecordTime() != '') {
                $time_hour_Selected = (date('G', strtotime($eventDetail->getRecordTime())));
                $time_min_Selected = intval(date('i', strtotime($eventDetail->getRecordTime())));
            } else {
                $time_hour_Selected = date('G');
                $time_min_Selected = intval(date('i'));
            }
            if ($eventDetail->getBehaviourTwo() == '') {
                $level2Selected = NULL;
            } else {
                $level2Selected = $eventDetail->getBehaviourTwo();
            }
            
            if(
                $eventDetail->getRecordID() &&  
                ! $this->isSuperAdmin($_SESSION['UserID']) &&
                (
                    (
                        $sys_custom['medical']['StudentLog']['AllowEditDeleteOwnRecordOnly'] &&
                        ($eventDetail->getInputBy() != $_SESSION['UserID']) && 
                        ($eventDetail->getModifyBy() != $_SESSION['UserID'])
                    ) || (
                        $sys_custom['medical']['StudentLog']['AllowAdminEditDeleteRecordOnly']
                    )
                )
            ){
                $isReadOnly = true;
            }
            
            
            
            if ($plugin['medical_module']['discipline']) {
                $relevantEvents = $this->getStudentLogEventList($Id, $isReadOnly);
                $studentLogEventLayout = $this->getStudentLogEventLayout($Id, $relevantEvents, $isReadOnly);
            } else {
                $studentLogEventLayout = '';
            }
            
            if ($isReadOnly) {
                $time_h = str_pad($time_hour_Selected, 2, '0', STR_PAD_LEFT);
                $time_m = str_pad($time_min_Selected, 2, '0', STR_PAD_LEFT);
                $deleteEventBtn = '';
                $deleteBodyPartsBtn = '';
                $_lev1 = new studentLogLev1($eventDetail->getBehaviourOne());
                $lev1_Sel = $_lev1->getLev1Name();
                $_lev2 = new studentLogLev2($level2Selected);
                $lev2_Sel = '-&nbsp;' . $_lev2->getLev2Name();
            } else {
                $time_h = $this->getSelectionBox($id = "event[" . $Id . "][time_hour]", $name = "event[" . $Id . "][time_hour]", $valueArray['time_hour'], $valueSelected = $time_hour_Selected, $class = "");
                $time_m = $this->getSelectionBox($id = "event[" . $Id . "][time_min]", $name = "event[" . $Id . "][time_min]", $valueArray['time_min'], $valueSelected = $time_min_Selected, $class = "");
                $deleteEventBtn = "<span class='table_row_tool delete_event_div'><a class='deleteEventBtn delete' title='" . $Lang['medical']['studentLog']['deleteEvent'] . "' ></a></span>";
                $deleteBodyPartsBtn = "<span class='table_row_tool'><a class='deletePartsBtn delete' title='" . $Lang['Btn']['Delete'] . "'></a></span>";
                $lev1_Sel = $objLogLev1->getHTMLSelection("event[" . $Id . "][level1]", $eventDetail->getBehaviourOne(), 'class="level1"');
                $lev2_Sel = $objLogLev2->getHTMLSelection("event[" . $Id . "][level2]", $level2Selected, $levelOneId = $eventDetail->getBehaviourOne(), 'class="level2"');
            }
            
            $lev3_Sel = '';
            foreach ($objLogLev3 as $item) {
                $Lev4List = $this->getLev4List($item['Level3ID']);
                $Lev4SelectedList = $this->getLev4SelectedList($Id, $item['Level3ID']);
                $html = '';
                
                if ($isReadOnly) {
                    $lev3_Sel .= $valueArray['lev3List'][$item['Level3ID']];
                    $lev3_Sel .= " - <span class='lev4ListArea'>";
                    $lev4Str = '';
                    foreach ((array) $Lev4List as $Lev4) {
                        foreach ((array) $Lev4SelectedList as $Lev4Selected) {
                            if ($Lev4['Lev4ID'] == $Lev4Selected['Level4ID']) {
                                $lev4Str .= $Lev4['Lev4Name'] . ', ';
                                break;
                            }
                        }
                    }
                    $lev3_Sel .= trim($lev4Str, ', ');
                    $lev3_Sel .= "</span>";
                    $lev3_Sel .= "<br />";
                } else {
                    foreach ((array) $Lev4List as $Lev4) {
                        
                        foreach ((array) $Lev4SelectedList as $Lev4Selected) {
                            $selected = '';
                            if ($Lev4['Lev4ID'] == $Lev4Selected['Level4ID']) {
                                $selected = 'checked';
                                break;
                            }
                        }
                        // $valueSelected no use
                        $html .= $this->getCheckBox($id = "event[" . $Id . "][level4][" . $item['Level3ID'] . "][" . $Lev4['Lev4ID'] . "]", $name = "event[" . $Id . "][level4][" . $item['Level3ID'] . "][" . $Lev4['Lev4ID'] . "]", $name = $Lev4['Lev4Name'], $value = $Lev4['Lev4ID'], $valueSelected = '', // no use
$class = "level4", $OtherMembers = "$selected");
                    }
                    $lev3_Sel .= $this->getSelectionBox($id = "event[" . $Id . "][level3][]", $name = "event[" . $Id . "][level3][]", $valueArray['lev3List'], $valueSelected = $item['Level3ID'], $class = "level3", $otherMembers = "data-Id='{$Id}'");
                    $lev3_Sel .= "<span class='lev4ListArea'>";
                    $lev3_Sel .= $html;
                    $lev3_Sel .= "</span>";
                    $lev3_Sel .= $deleteBodyPartsBtn;
                    $lev3_Sel .= "<br />";
                }
            }
            
            if ($isReadOnly) {
                $time_min = intval($timeLasted[1]) + intval($timeLasted[0] * 24);
                $time_second = intval($timeLasted[2]);
                $time_min = str_pad($time_min, 2, '0', STR_PAD_LEFT);
                $time_second = str_pad($time_second, 2, '0', STR_PAD_LEFT);
                
                $addBodyParts_HTML = '';
                
                $attachmentDetailList = is_numeric($Id) ? $this->getAttachmentDetail($Id) : array();
                
                if (count($attachmentDetailList) == 0) {
                    $uploadedFilesHTML = '--';
                } else {
                    $uploadedFilesHTML = $this->getAttachmentHTML($Id, $attachmentDetailList, $deleteBtn = false);
                }
                $addAttachmentHTML = '';
                $isRemarkReadOnly = 'readonly';
                $defaultRemarksHTML = '';
                $addBtn = '';
                $button_remove_html = '';
                $PIC_SelRemark = '';
                $saveBtn = '';
                $email2pic_checkbox = '';
                $picReadOnly = 'readonly';
            } else {
                $time_min = $this->getSelectionBox($id = "event[" . $Id . "][timelasted_min]", $name = "event[" . $Id . "][timelasted_min]", $valueArray['time_min'], intval($timeLasted[1]) + intval($timeLasted[0] * 24), $class = "");
                $time_second = $this->getSelectionBox($id = "event[" . $Id . "][timelasted_sec]", $name = "event[" . $Id . "][timelasted_sec]", $valueArray['time_sec'], intval($timeLasted[2]), $class = "");
                
                $addBodyParts_HTML = "<span class='table_row_tool addBodyParts' data-Id='{$Id}' ><a class='add' title='{$Lang['medical']['studentLog']['addBodyParts']}'></a></span>";
                
                $attachmentDetailList = is_numeric($Id) ? $this->getAttachmentDetail($Id) : array();
                $uploadedFilesHTML = $this->getAttachmentHTML($Id, $attachmentDetailList);
                $addAttachmentHTML = "<span class='table_row_tool addAttachment' data-Id='{$Id}'><a class='add' title='{$Lang['medical']['studentLog']['addAttachment']}'></a></span>";
                $addAttachmentHTML .= "<span class='form_sep_title'>{$Lang['medical']['studentLog']['attachmentRemarks']}</span>";
                $isRemarkReadOnly = '';
                if (strpos($Id, 'n') !== FALSE) {
                    $defaultRemarksID = - 1 * ((int) str_replace('n', '', $Id));
                    $defaultRemarksHTML = '<span class="defaultRemarkSpan" style="margin-left:5px;">' . GetPresetText("jRemarksArr", $defaultRemarksID, "event[{$Id}][remarks]", "", "", "", "1") . '</span>';
                } else {
                    $defaultRemarksHTML = '<span class="defaultRemarkSpan" style="margin-left:5px;">' . GetPresetText("jRemarksArr", $Id, "event[{$Id}][remarks]", "", "", "", "1") . '</span>';
                }
                $addBtn = $linterface->GET_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=event[{$Id}][PIC][]&DisplayGroupCategory=1&OpenerFormName=form_{$Id}&page_title=SelectMembers&permitted_type=1&excluded_type=4&Disable_AddGroup_Button=1', 16)");
                $button_remove_html = $linterface->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:checkOptionRemove(document.getElementById('event[{$Id}][PIC][]'))");
                $PIC_SelRemark = "<span class='form_sep_title'>{$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']}</span>";
                $email2pic_checkbox = "<br><input type='checkbox' name=\"event[{$Id}][email2pic]\" id=\"event[{$Id}][email2pic]\" value='1'><label for=\"event[{$Id}][email2pic]\">" . $Lang['medical']['studentLog']['email2pic'] . "</label>";
                $picReadOnly = '';
                $saveBtn = <<<SAVE_HTML
					<tr>
						<td colspan="2" align="center">
							<input type="button" name="SaveIndividual" class="formbutton_v30 print_hide SaveIndividual" value="{$Lang['medical']['meal']['button']['save']}" data-recordid="$Id">
						</td>		
					</tr>
SAVE_HTML;
            }
            $PIC_Sel = $linterface->GET_SELECTION_BOX($picList, "name='event[{$Id}][PIC][]' id='event[{$Id}][PIC][]' class='select_studentlist {$picReadOnly}' size='6' style='width:300px' multiple='multiple'", "");
            $remarksHTML = stripslashes($eventDetail->getRemarks());
            
            $fieldSetName = '';
            if ($index == - 1) {
                $fieldSetName = '<legend class="oldEvent">' . $Lang['medical']['problemRecord']['studentLog']['tableHeader']['Item'] . '</legend>';
            } else {
                if (strpos($Id, 'n') !== FALSE) {
                    $fieldSetName = '<legend class="newEvent">' . $Lang['medical']['studentLog']['newEventDesc'] . ' #' . ltrim($Id, 'n') . '</legend>';
                } else {
                    $fieldSetName = '<legend class="oldEvent">' . $Lang['medical']['studentLog']['oldEventDesc'] . ' #' . $index . '</legend>';
                }
            }
            
            $convulationIDList = $this->geConvulatonID();
            $isDisplayed = (in_array($eventDetail->getBehaviourTwo(), $convulationIDList) || $plugin['medical_module']['AlwaysShowBodyPart']) ? '' : 'display:none;';
            echo <<<END
				<span>
					<form method="post" action="index.php" id="event_$Id" name="form_$Id" enctype="multipart/form-data">
						<fieldset>
						{$fieldSetName}
						<input type="hidden" name="recordID" value="$Id"/>
						<input type="hidden" name="action" value="edit"/>
						<input type="hidden" class="hasSave" value="1"/>
						<table style="width:100%" border="0" cellpadding="5" cellspacing="0" class="eventClass" >
							<colgroup>
							<col style="width:20%"/>
							<col style="width:80%"/>
							</colgroup>
							<tr class="saveMsg"><td colspan="2" style="text-align: center;"><span></span></td></tr>
							<tr>
								<td>{$Lang['medical']['studentLog']['time']}</td>
								<td>{$time_h}:{$time_m} {$deleteEventBtn}
								</td>
							</tr>
							<tr>
								<td>{$Lang['medical']['studentLog']['behaviour']}</td>
								<td>{$lev1_Sel}&nbsp;{$lev2_Sel}</td>
							</tr>
							<tr class="bodyPartsDiv" style="{$isDisplayed}">
								<td>{$Lang['medical']['studentLog']['bodyParts']}</td>
								<td>
									<span class="bodyPartsField">{$lev3_Sel}</span>
									{$addBodyParts_HTML}
								</td>
								
							</tr>
							<tr class="bodyPartsDiv" style="{$isDisplayed}">
								<td>{$Lang['medical']['studentLog']['timelasted']}</td>
								<td>{$time_min} {$Lang['medical']['studentLog']['minute']} {$time_second} {$Lang['medical']['studentLog']['second']}</td>
							</tr>				
							<tr>
								<td>{$Lang['SysMgr']['Homework']['Attachment']}</td>
								<td>
									{$uploadedFilesHTML}
									{$addAttachmentHTML}
								</td>
							</tr>
							<tr>
								<td>{$Lang['General']['Remark']}</td>
								<td><textarea id="event[{$Id}][remarks]" name="event[{$Id}][remarks]" style="width:300px;" rows="4" {$isRemarkReadOnly}>{$remarksHTML}</textarea>{$defaultRemarksHTML}</td>
							</tr>
							<tr>
								<td>{$eEnrollment['All_Member_Import_FileDescription_PICRemarks']}</td>
								<td>
									<span style="float:left;">{$PIC_Sel}</span>
									{$addBtn}
									<br/>
									{$button_remove_html}
									<br style="clear:both;" / >
									<br />
									{$PIC_SelRemark}
									{$email2pic_checkbox}
								</td>		
							</tr>
                            {$studentLogEventLayout}
							{$saveBtn}
						</table>
						<!--script>
						\$('.level1').change(function(){
							\$objList = \$(this).next();
							\$.ajax({
								url : "?t=management.ajax.getStudentLogLevelList",
								type : "POST",
								data : 'data='+$(this).val()+'&action=2',
								success : function(msg) {
									\$objList.html(msg);
									\$('.level2').change();
								}
							});	
						});
						\$('.level2').change(function(){
							if ((\$(this).find('option:selected').html()=="{$medical_cfg['studentLog']['level2']['specialStatus']}") || ({$plugin['medical_module']['AlwaysShowBodyPart']} == true)){	

								\$(this).parent().parent().parent().find('.bodyPartsDiv').show();			
							}
							else{
								\$(this).parent().parent().parent().find('.bodyPartsDiv').hide();
							}
						});
						</script-->
					</fieldset>
					</form>
					
					<br />
					<br />
				</span>
END;
        }

        function deleteStudentLogAttachment($filesIDDeletedList)
        {
            global $medical_cfg;
            $IdListForSQL = array();
            foreach ((array) $filesIDDeletedList as $Id) {
                $uploadFile_root = $medical_cfg['uploadFile_root'];
                if (! empty($uploadFile_root) && substr($uploadFile_root, - 1) != "/") {
                    $uploadFile_root .= "/";
                }
                $sql = "Select RenameFile, FolderPath From MEDICAL_STUDENT_LOG_DOCUMENT Where RecordID = '{$Id}' ";
                $fileNameList = $this->objDB->returnResultSet($sql);
                foreach ((array) $fileNameList as $fileDetail) {
                    $result = unlink($uploadFile_root . $fileDetail['FolderPath'] . '/' . $fileDetail['RenameFile']);
                    if ($result) {
                        $IdListForSQL[] = $Id;
                    }
                }
            }
            $result = array();
            $IdListForSQL = implode("','", $IdListForSQL);
            $sql = "Delete From MEDICAL_STUDENT_LOG_DOCUMENT Where RecordID In ('{$IdListForSQL}') ";
            
            $result[] = $this->objDB->db_db_query($sql);
            
            if (in_array(0, $result)) {
                $this->objDB->RollBack_Trans();
                return false;
            } else {
                $this->objDB->Commit_Trans();
                return true;
            }
        }

        function deleteStudentLogRecords($eventIDArray)
        {
            if (count($eventIDArray)) {
                if ((count($eventIDArray) == 1) && (empty($eventIDArray[0]))) {
                    // do nothing
                    return false;
                } else {
                    $IdListForSQL = implode("','", (array) $eventIDArray);
                    $filesIDDeletedList = array();
                    
                    $this->objDB->Start_Trans();
                    
                    $sql = "Delete From MEDICAL_STUDENT_LOG Where RecordID In ('{$IdListForSQL}') ";
                    $result[] = $this->objDB->db_db_query($sql);
                    
                    $sql = "Delete From MEDICAL_STUDENT_LOG_PARTS Where StudentLogID In ('{$IdListForSQL}') ";
                    $result[] = $this->objDB->db_db_query($sql);

                    $sql = "DELETE FROM MEDICAL_EVENT_STUDENT_LOG WHERE StudentLogID IN ('{$IdListForSQL}') ";
                    $result[] = $this->objDB->db_db_query($sql);
                    
                    $sql = "Select RecordID From MEDICAL_STUDENT_LOG_DOCUMENT Where StudentLogID In ('{$IdListForSQL}') ";
                    $rs = $this->objDB->returnResultSet($sql);
                    foreach ($rs as $item) {
                        $filesIDDeletedList[] = $item['RecordID'];
                    }
                    
                    $this->deleteStudentLogAttachment($filesIDDeletedList);
                    
                    if (in_array(0, $result)) {
                        $this->objDB->RollBack_Trans();
                        return false;
                    } else {
                        $this->objDB->Commit_Trans();
                        return true;
                    }
                }
            } else {
                return false;
            }
        }

        function deleteStudentLogEvent($eventIDAry = '')
        {
            $result = true;
            if (is_array($eventIDAry) && count($eventIDAry)) {
                $sql = "DELETE FROM MEDICAL_EVENT_STUDENT_LOG WHERE EventID IN ('" . implode("','", $eventIDAry) . "')";
                $result = $this->objDB->db_db_query($sql);
            }
            return $result;
        }

        function getStudentLogReport2($date, $studentList, $lev2IDList = '', $LastDrug = '')
        {
            global $medical_cfg, $sys_custom;
            
            $AcademicYearID = Get_Current_Academic_Year_ID();
            
            // # Class Filter
            // if($classID !=''){
            // $conds .= "And YC.YearClassID = '{$classID}'";
            // }
            // StudentList Filter
            if ($studentList != '') {
                $conds .= " And IU.UserID In ('" . implode("','", $studentList) . "')";
            }
            
            // Level 2 Filter
            
            if ($lev2IDList != '') {
                $conds .= " And MSL.BehaviourTwo In ('" . implode("','", $lev2IDList) . "')";
            }
            
            // # Stay School Filter
            // if($sleep !=''){
            // global $medical_cfg;
            // if($sleep =='1'){
            // $conds .= " And IUPS.stayOverNight = '1'";
            // }
            // else{
            // $conds .= " And (IUPS.stayOverNight = ".$medical_cfg['sleep_status']['StayOut']['value']." or IUPS.stayOverNight is null)";
            // }
            // $sleepTable = "
            // Left Join
            // INTRANET_USER_PERSONAL_SETTINGS IUPS
            // On
            // IUPS.UserID = IU.UserID
            // ";
            //
            // }
            
            // # Gender Filter
            // if($gender !=''){
            // $conds .= (" And IU.gender ='".$gender."'");
            // }
            
            $fieldList[] = 'IU.UserID';
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = 'MSL.RecordID';
            $fieldList[] = 'MSL.RecordTime';
            $fieldList[] = 'MSL.Duration';
            $fieldList[] = 'MSL.Remarks';
            $fieldList[] = 'MSL.DateModified';
            $fieldList[] = 'MSL.ModifyBy';
            // $fieldList [] = 'MSLP.StudentLogID';
            $fieldList[] = 'MSLL1.Lev1Name';
            $fieldList[] = 'MSLL2.Lev2Name';
            // $fieldList [] = 'MSLL3.Lev3ID';
            // $fieldList [] = 'MSLL3.Lev3Name';
            // $fieldList [] = 'MSLL4.Lev4Name';
            $fieldList[] = 'IUM.ConfirmedName';
            $fieldList[] = 'MSLD.StudentLogID';
            
            if($sys_custom['medical']['accessRight']['ViewAll'] && !($this->isSuperAdmin($_SESSION['UserID']) || $this->checkAccessRight($_SESSION['UserID'] ,'STUDENTLOG_VIEWALL'))){
//                $selfOnly = " AND InputBy = '{$_SESSION['UserID']}' ";
                $selfOnly = " AND (PIC REGEXP \"(^|,){$_SESSION['UserID']}(,|$)\" OR InputBy = '{$_SESSION['UserID']}') ";
            }
            
            $fieldSelected = implode(", ", $fieldList);
            
            $sql = "
				Select
					{$fieldSelected}
				From
					INTRANET_USER IU
				Inner Join
					YEAR_CLASS_USER YCU
				On
					IU.UserID = YCU.UserID
				Inner Join
					YEAR_CLASS YC
				On
					YC.YearClassID = YCU.YearClassID
				Inner Join
					(Select
						*
					From
						MEDICAL_STUDENT_LOG
					Where
						recordtime >=  '{$date} 00:00:00'
					And
						recordtime <= '{$date} 23:59:59'
                        {$selfOnly}
					) As MSL
				On
					MSL.UserID = IU.UserID
				Left Join
					(Select 
						" . getNameFieldByLang2() . " As ConfirmedName, UserID
					From
						INTRANET_USER
					) IUM
				On
					IUM.UserID = MSL.ModifyBy
				Left Join
					MEDICAL_STUDENT_LOG_LEV1 MSLL1
				On 
					MSL.BehaviourOne = MSLL1.Lev1ID
				Left Join
					MEDICAL_STUDENT_LOG_LEV2 MSLL2
				On 
					MSL.BehaviourTwo = MSLL2.Lev2ID
				LEFT JOIN
					MEDICAL_STUDENT_LOG_DOCUMENT MSLD
				ON
					MSL.RecordID = MSLD.StudentLogID
					{$sleepTable}
				Where
					YC.AcademicYearID = '{$AcademicYearID}' And IU.RecordStatus = '1'
					{$conds}
				GROUP BY
					MSL.RECORDID
				ORDER BY
					FIELD(IU.userID, '" . implode("','", (array) $studentList) . "')
			";
            $rs = $this->objDB->returnResultSet($sql);
            
            // Inner Join
            // MEDICAL_STUDENT_LOG_PARTS MSLP
            // On
            // MSL.RecordID = MSLP.StudentLogID
            // Inner Join
            // MEDICAL_STUDENT_LOG_LEV3 MSLL3
            // On
            // MSLP.Level3ID = MSLL3.Lev3ID
            // Inner Join
            // MEDICAL_STUDENT_LOG_LEV4 MSLL4
            // On
            // MSLP.Level4ID = MSLL4.Lev4ID
            
            $tempArray = array();
            
            $i = 0;
            foreach ($rs as $item) {
                $userID = $item['UserID'];
                $recordID = $item['RecordID'];
                
                if ($rs[$i]['RecordID'] != $rs[$i + 1]['RecordID']) {
                    // if( $rs[$i]['UserID'] != $rs[$i+1]['UserID']){
                    // $tempArray[$userID][$recordID]['Name']=$item['Name'];
                    // }
                    if ($rs[$i]['UserID'] != $rs[$i + 1]['UserID']) {
                        $tempArray[$userID]['Name'] = $item['Name'];
                        
                        $tempArray[$userID]['LastDrug'] = '';
                        if ($LastDrug) {
                            $revisitID = $this->getLastRevisitID($userID);
                            if ($revisitID) {
                                $drug_rs = $this->getRevisitDrug($revisitID);
                                $drugs = '';
                                $j = 1;
                                foreach ((array) $drug_rs as $drs) {
                                    $drugs .= ($j ++) . '. ' . $drs['Drug'];
                                    if ($drs['Dosage'] > 0) {
                                        $drugs .= ' ' . $drs['Dosage'];
                                    }
                                    if (! empty($drs['Unit'])) {
                                        $drugs .= ' ' . $drs['Unit'];
                                    }
                                    $drugs .= "\n";
                                }
                                $tempArray[$userID]['LastDrug'] = $drugs ? substr($drugs, 0, - 1) : '';
                                $revisit = $this->getRevisit($revisitID);
                                $tempArray[$userID]['LastRevisitDate'] = $revisit[0]['RevisitDate'];
                            }
                        }
                    }
                    // $tempArray[$userID][$recordID]['Duration']=$item['Duration'];
                    $tempArray[$userID]['recordDetail'][$recordID]['RecordID'] = $recordID;
                    $tempArray[$userID]['recordDetail'][$recordID]['StudentLogID'] = $item['StudentLogID'];
                    $tempArray[$userID]['recordDetail'][$recordID]['Remarks'] = stripslashes($item['Remarks']);
                    $tempArray[$userID]['recordDetail'][$recordID]['RecordTime'] = $item['RecordTime'];
                    $tempArray[$userID]['recordDetail'][$recordID]['Lev1Name'] = $item['Lev1Name'];
                    $tempArray[$userID]['recordDetail'][$recordID]['Lev2Name'] = $item['Lev2Name'];
                    $tempArray[$userID]['recordDetail'][$recordID]['ConfirmedName'] = $item['ConfirmedName'];
                    $tempArray[$userID]['recordDetail'][$recordID]['DateModified'] = $item['DateModified'];
                }
                
                // $tempArray[$userID][$recordID]['Lev3Array'][$item['Lev3ID']]=$item['Lev3Name'];
                // $tempArray[$userID][$recordID]['Lev4Array'][$item['Lev3ID']][]=$item['Lev4Name'];
                // $tempArray[$userID][$recordID]['counter'] = count($tempArray[$userID][$recordID]['Lev3Array']);
                
                ++ $i;
            }
            // debug_r($tempArray);
            return $tempArray;
        }

        function getClassNameClassNumber()
        {
            $sql = 'SELECT ClassName, ClassNumber FROM INTRANET_USER where RecordType = 2 And RecordStatus = 1';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getStudentLogAllActiveLev1()
        {
            $sql = 'SELECT * FROM MEDICAL_STUDENT_LOG_LEV1 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getStudentLogAllActiveLev2()
        {
            $sql = 'SELECT * FROM MEDICAL_STUDENT_LOG_LEV2 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveLev1Code()
        {
            $sql = 'SELECT Lev1Code FROM MEDICAL_STUDENT_LOG_LEV1 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveLev2Code()
        {
            $sql = 'SELECT Lev2Code FROM MEDICAL_STUDENT_LOG_LEV2 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveLev3Code()
        {
            $sql = 'SELECT Lev3Code FROM MEDICAL_STUDENT_LOG_LEV3 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveLev4Code()
        {
            $sql = 'SELECT Lev4Code FROM MEDICAL_STUDENT_LOG_LEV4 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveLev4CodeLev3ID()
        {
            $sql = 'SELECT Lev4Code,Lev3ID FROM MEDICAL_STUDENT_LOG_LEV4 WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllMealStatus()
        {
            $sql = 'SELECT * FROM MEDICAL_MEAL_STATUS WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllTeacherLoginID()
        {
            $sql = 'SELECT UserLogin FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1'; // Staff
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getLev2NameByCode($Lev2Code)
        {
            if ($Lev2Code == "") {
                return false;
            }
            $sql = "Select Lev2Name From MEDICAL_STUDENT_LOG_LEV2 Where Lev2Code = '{$Lev2Code}' AND RecordStatus='1' AND DeletedFlag='0'";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs[0]["Lev2Name"];
        }

        function getUserIDByClassNameAndNumber($className, $classNumber)
        {
            if ($className == '' || $classNumber == '') {
                return false;
            }
            
            $AcademicYearID = Get_Current_Academic_Year_ID();
            // $sql = "SELECT UserID FROM INTRANET_USER where RecordType = 2 And RecordStatus = 1 AND ClassName='$className' AND ClassNumber='$classNumber'";
            // Added Academic Year checking
            $sql = "SELECT
					IU.UserID
				FROM
					INTRANET_USER IU
				INNER JOIN
					YEAR_CLASS_USER YCU
				ON
					IU.UserID = YCU.UserID
				INNER JOIN
					YEAR_CLASS YC
				ON
					YC.YearClassID = YCU.YearClassID
				WHERE
					IU.RecordType = 2
				AND
					IU.RecordStatus = 1
				AND
					IU.ClassName='{$className}'
				AND
					IU.ClassNumber='{$classNumber}'
				AND
					YC.AcademicYearID='{$AcademicYearID}'";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                return $rs[0]["UserID"];
            } else {
                return false;
            }
        }

        // Add active-checking to the SQL, by Pun
        function getLev1IDByCode($code) // to libmedical.php
        {
            if ($code == "") {
                return false;
            }
            $sql = "SELECT Lev1ID FROM MEDICAL_STUDENT_LOG_LEV1 WHERE Lev1Code='$code' AND recordstatus = 1 AND DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                return $rs[0]["Lev1ID"];
            } else {
                return false;
            }
        }

        // Add active-checking to the SQL, by Pun
        function getLev2IDByCode($code, $lev1ID = '') // to libmedical.php
        {
            if ($code == "") {
                return false;
            }
            if ($lev1ID != '') {
                $condition = ' AND Lev1ID = "' . $lev1ID . '" ';
            }
            
            $sql = "SELECT Lev2ID FROM MEDICAL_STUDENT_LOG_LEV2 WHERE Lev2Code='$code' $condition AND recordstatus = 1 AND DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            // debug_r($sql);
            // debug_r($rs);
            if (count($rs) == 1) {
                return $rs[0]["Lev2ID"];
            } else {
                return false;
            }
        }

        // Add active-checking to the SQL, by Pun
        function getLev3IDByCode($code) // to libmedical.php
        {
            if ($code == "") {
                return false;
            }
            
            $sql = "SELECT Lev3ID FROM MEDICAL_STUDENT_LOG_LEV3 WHERE Lev3Code='$code' AND recordstatus = 1 AND DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                return $rs[0]["Lev3ID"];
            } else {
                return false;
            }
        }

        // Add active-checking to the SQL, by Pun
        function getLev4IDByCode($code) // to libmedical.php
        {
            if ($code == "") {
                return false;
            }
            
            $sql = "SELECT Lev4ID FROM MEDICAL_STUDENT_LOG_LEV4 WHERE Lev4Code='$code' AND recordstatus = 1 AND DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                return $rs[0]["Lev4ID"];
            } else {
                return false;
            }
        }

        function getStudentLogBodyPartsList($recordIDList)
        {
            $recordIDList = implode("','", (array) $recordIDList); // String Style
            
            $fieldList[] = 'MSLP.RecordID'; // RecordID in MSLP is NOT EQUAL TO THAT in MSL
            $fieldList[] = 'MSLP.StudentLogID';
            $fieldList[] = 'MSLL3.Lev3Name';
            $fieldList[] = 'MSLL4.Lev4Name';
            
            $fieldSelected = implode(", ", (array) $fieldList);
            $sql = "
				Select
					{$fieldSelected}
				From
					MEDICAL_STUDENT_LOG_PARTS MSLP
				Inner Join
					MEDICAL_STUDENT_LOG_LEV3 MSLL3
				On
					MSLP.Level3ID = MSLL3.Lev3ID
				Inner Join
					MEDICAL_STUDENT_LOG_LEV4 MSLL4
				On
					MSLP.Level4ID = MSLL4.Lev4ID
				Where
					MSLP.StudentLogID In ('{$recordIDList}') Order By MSLP.Level3ID
			";
            $rs = $this->objDB->returnResultSet($sql);
            $bodyPartsList = array();
            foreach ($rs as $row) {
                // $bodyPartsList[$row['StudentLogID']][$row['RecordID']][$row['Lev3Name']][] = $row['Lev4Name'];
                $bodyPartsList[$row['StudentLogID']][$row['Lev3Name']][] = $row['Lev4Name'];
            }
            
            return $bodyPartsList;
        }

        function getSleepIDByCode($code)
        {
            if ($code == "") {
                return false;
            }
            
            $sql = "SELECT StatusID FROM MEDICAL_SLEEP_STATUS WHERE StatusCode='$code' AND recordstatus = 1 AND DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                return $rs[0]["StatusID"];
            } else {
                return false;
            }
        }

        // function getReasonIDByCode($code)
        function getReasonIDByCode($code, $statusID)
        {
            if ($code == "") {
                return false;
            }
            
            // $sql = "SELECT ReasonID FROM MEDICAL_SLEEP_STATUS_REASON WHERE ReasonCode='$code' AND recordstatus = 1 AND DeletedFlag = 0";
            $sql = "SELECT ReasonID FROM MEDICAL_SLEEP_STATUS_REASON WHERE ReasonCode='$code' AND StatusID='$statusID' AND recordstatus = 1 AND DeletedFlag = 0";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                return $rs[0]["ReasonID"];
            } else {
                return false;
            }
        }

        function getAllActiveSleepStatus()
        {
            $sql = 'SELECT StatusCode FROM MEDICAL_SLEEP_STATUS WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveSleepStatusReason()
        {
            $sql = 'SELECT ReasonCode FROM MEDICAL_SLEEP_STATUS_REASON WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveSleepStatusID_Code()
        {
            $sql = 'SELECT StatusID,StatusCode FROM MEDICAL_SLEEP_STATUS WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveSleepStatusID_ReasonCode()
        {
            $sql = 'SELECT StatusID,ReasonCode FROM MEDICAL_SLEEP_STATUS_REASON WHERE recordstatus = 1 AND DeletedFlag = 0';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getAllActiveBowelStatus()
        {
            $sql = 'SELECT StatusID, BarCode FROM MEDICAL_BOWEL_STATUS WHERE RecordStatus = 1 AND DeletedFlag = 0 AND BarCode IS NOT NULL AND BarCode<>\'\'';
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getUserIDByBarcode($barcode)
        {
            if (is_array($barcode)) {
                $barcodeStr = implode("','", $this->objDB->Get_Safe_Sql_Query($barcode));
            } else {
                $barcodeStr = $this->objDB->Get_Safe_Sql_Query($barcode);
            }
            if ($barcodeStr) {
                $sql = "SELECT UserID, Barcode FROM INTRANET_USER WHERE Barcode IN ('" . $barcodeStr . "') ORDER BY Barcode";
                $rs = $this->objDB->returnResultSet($sql);
                return $rs;
            } else {
                return false;
            }
        }

        // $UserLogin is in comman separated format, e.g.: khwong2,peter_ppp,winnie_nt
        function getUserIDByUserLogin($UserLogin)
        {
            if ($UserLogin) {
                $UserLoginList = str_replace(",", "','", $this->objDB->Get_Safe_Sql_Query($UserLogin));
                
                $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin IN ('" . $UserLoginList . "')";
                $rs = $this->objDB->returnVector($sql);
                return $rs;
            } else {
                return false;
            }
        }

        function nullReplacer($string, $text = '-')
        {
            return ($string) ? $string : $text;
        }

        function getDefaultRemarks($remarkType)
        {
            global $medical_cfg;
            $sql = "SELECT Remarks FROM MEDICAL_DEFAULT_REMARKS WHERE RemarksType = '{$remarkType}'";
            $rs = $this->objDB->returnVector($sql);
            return $rs;
        }

        function getDefaultRemarksArray($remarkType)
        {
            $defaultRemark = $this->getDefaultRemarks($remarkType);
            $defaultRemarkArr = explode("\n", $defaultRemark[0]);
            $defaultRemark = '[';
            foreach ($defaultRemarkArr as $dr) {
                $remark = trim($dr);
                $defaultRemark .= "['{$remark}'],";
            }
            $defaultRemark = trim($defaultRemark, ',') . ']';
            return $defaultRemark;
        }

        function getBowelTimeInterval()
        {
            global $plugin;
            if ($plugin['medical_module']['bowelTimeInterval'] == '') {
                return 30;
            } else {
                if ($plugin['medical_module']['bowelTimeInterval'] == 5 || $plugin['medical_module']['bowelTimeInterval'] == 10 || $plugin['medical_module']['bowelTimeInterval'] == 15 || $plugin['medical_module']['bowelTimeInterval'] == 20 || $plugin['medical_module']['bowelTimeInterval'] == 30) {
                    return $plugin['medical_module']['bowelTimeInterval'];
                } else {
                    $errMsg = "bowelTimeInterval wrong setting. f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                    alert_error_log($projectName = $cfg_medical['module_code'], $errorMsg = $errMsg, $errorType = '');
                    return 30;
                }
            }
        }

        // return time used to stored in db
        function getBowelTime4Db($dateTime='')
        {
            if ($dateTime == '') {
                $dateTime = date('Y-m-d H:i:00');
            }
            $retTime = $dateTime;
            $time_MM = date( 'i', strtotime($dateTime));
            $timeInterval = $this->getBowelTimeInterval();
            
            for($i=0;$i<59;$i+=$timeInterval){
                $min = $i;
                $max = $i + $timeInterval- 1;
                if ($time_MM >= $min && $time_MM <= $max) {
                    $retTime = substr($dateTime,0,14).$max;
                    break;
                }
            }
            return $retTime;
        }

        // $dateTime is in yyyy-mm-dd HH:mm:00 format 
        function getBowelTime2Show($dateTime)
        {
            $time_HH = date( 'H', strtotime($dateTime));
            $time_MM = date( 'i', strtotime($dateTime));
            $timeInterval = $this->getBowelTimeInterval();
            $start = $time_MM + 1 - $timeInterval;
            $start = $start < 0 ? '00' : str_pad($start, 2, '0', STR_PAD_LEFT);
            $end = str_pad($time_MM, 2, '0', STR_PAD_LEFT);
            return $time_HH.' : '.$start.'-'.$end;
        }
        
        function getSleepReport($sleepStudentData, $sleepAttendanceList, $reasonIDList, $displayRemarks = 0, $outputFormat = 'html')
        {
            global $Lang, $medical_cfg, $linterface;
            
            if (($outputFormat == 'html') || ($outputFormat == 'print')) {
                $styleHTML = <<<HTML
				<style>
					.totalCount{
						background-color: lightgray !important;
					}
					.common_table_list_v30 th{
						text-align: center !important;
					}
					.common_table_list_v30 td{
						text-align: center !important;
					}
				</style>
HTML;
                echo $styleHTML;
                echo '<table border="0" class="common_table_list_v30">';
                echo '<thead>';
                echo '<tr class="tabletop">';
                echo '<th style="min-width:70px !important;text-align: left !important;">' . $Lang['medical']['report_meal']['tableHeader']['name'] . '</th>';
                foreach ($reasonIDList as $reasonData) {
                    if ($reasonData['isComment'] != 1) {
                        echo '<th>' . stripslashes($reasonData['ReasonName']) . '</th>';
                    }
                }
                echo '<th>' . $Lang['medical']['report_sleep']['tableHeader']['stayForDays'] . '</th>';
                foreach ($reasonIDList as $reasonData) {
                    if ($reasonData['isComment'] == 1) {
                        echo '<th>' . $medical_cfg['studentSleep']['level1']['comments'] . ': ' . stripslashes($reasonData['ReasonName']) . '</th>';
                    }
                }
                
                if ($displayRemarks == 1) {
                    echo '<th>' . $Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'] . '</th>';
                }
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                foreach ($sleepStudentData as $key => $sleepStudentList) {
                    if ($key == 'Remarks') {
                        continue;
                    }
                    echo '<tr>';
                    echo '<td style="text-align: left !important;">' . $sleepStudentList['Name'] . '</td>';
                    // $totalDaysCount = 0;
                    
                    foreach ($reasonIDList as $reasonID => $reasonData) {
                        if ($reasonData['isComment'] != 1) {
                            if ($outputFormat == 'html') {
                                if ($sleepStudentList['reasonIDList'][$reasonID] == '') {
                                    echo '<td>-</td>';
                                } else {
                                    echo '<td><a href="#" data-reasonID="' . $reasonID . '" data-studentid="' . $key . '" class="dateDetail">' . $sleepStudentList['reasonIDList'][$reasonID] . '</a></td>';
                                }
                            } else {
                                echo '<td>' . $this->nullReplacer($sleepStudentList['reasonIDList'][$reasonID]) . '</td>';
                            }
                            // $totalDaysCount +=$sleepStudentList['reasonIDList'][$reasonID];
                        }
                    }
                    echo '<td class="totalCount">' . $sleepAttendanceList[$key] . '</td>';
                    foreach ($reasonIDList as $reasonID => $reasonData) {
                        if ($reasonData['isComment'] == 1) {
                            echo '<td>' . $this->nullReplacer($sleepStudentList['reasonIDList'][$reasonID]) . '</td>';
                        }
                    }
                    
                    if ($displayRemarks == 1) {
                        $remarksDetails = $sleepStudentData['Remarks'][$key];
                        echo '<td style="text-align:left !important">';
                        if ($remarksDetails != NULL) {
                            ksort($remarksDetails);
                            foreach ($remarksDetails as $date => $remarks) {
                                $displayDate = date("j/n", strtotime($date));
                                foreach ($remarks as $remark) {
                                    if ($remark != "") {
                                        $remark = stripslashes($remark);
                                        echo "$displayDate: $remark<br/>";
                                    }
                                }
                            }
                        }
                        echo '</td>';
                    }
                    
                    echo '</tr>';
                }
                echo '</tbody>';
                echo '</table>';
            } else if ($outputFormat == 'csv') {
                global $PATH_WRT_ROOT;
                include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
                $lexport = new libexporttext();
                $exportColumn = array();
                
                $header[] = $Lang['medical']['report_meal']['tableHeader']['name'];
                foreach ($reasonIDList as $reasonData) {
                    if ($reasonData['isComment'] != 1) {
                        $header[] = htmlspecialchars_decode(stripslashes($reasonData['ReasonName']), ENT_QUOTES);
                    }
                }
                $header[] = $Lang['medical']['report_sleep']['tableHeader']['stayForDays'];
                foreach ($reasonIDList as $reasonData) {
                    if ($reasonData['isComment'] == 1) {
                        $header[] = htmlspecialchars_decode(stripslashes($reasonData['ReasonName']), ENT_QUOTES);
                    }
                }
                
                if ($displayRemarks == 1) {
                    $header[] = $Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'];
                }
                $exportColumn = $header;
                
                $Rows = array();
                foreach ($sleepStudentData as $key => $sleepStudentList) {
                    if ($key == 'Remarks') {
                        continue;
                    }
                    
                    // $totalDaysCount = 0;
                    $RowsDetail = array();
                    $RowsDetail[] = $sleepStudentList['Name'];
                    foreach ($reasonIDList as $reasonID => $reasonData) {
                        if ($reasonData['isComment'] != 1) {
                            $RowsDetail[] = $sleepStudentList['reasonIDList'][$reasonID];
                            // $totalDaysCount +=$sleepStudentList['reasonIDList'][$reasonID];
                        }
                    }
                    $RowsDetail[] = $sleepAttendanceList[$key];
                    foreach ($reasonIDList as $reasonID => $reasonData) {
                        if ($reasonData['isComment'] == 1) {
                            $RowsDetail[] = $sleepStudentList['reasonIDList'][$reasonID];
                        }
                    }
                    
                    if ($displayRemarks == 1) {
                        $remarksRow = "";
                        $remarksDetails = $sleepStudentData['Remarks'][$key];
                        if ($remarksDetails != NULL) {
                            ksort($remarksDetails);
                            foreach ($remarksDetails as $date => $remarks) {
                                $displayDate = date("j/n", strtotime($date));
                                foreach ($remarks as $remark) {
                                    $remarksRow .= "$displayDate: $remark\n";
                                }
                            }
                        }
                        $RowsDetail[] = trim(stripslashes($remarksRow));
                    }
                    $Rows[] = $RowsDetail;
                }
                
                $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11", $includeLineBreak = 1);
                $lexport->EXPORT_FILE('export.csv', $exportContent);
            }
        }

        function getSleepReport2($sleepStudentData, $sleepAttendanceList, $outputFormat = 'html')
        {
            global $Lang, $medical_cfg, $linterface;
            
            $headerRow = array();
            $headerRow[] = '';
            $headerRow[] = $Lang['medical']['report_sleep']['tableHeader']['sleepWell'];
            $headerRow[] = $Lang['medical']['report_meal']['tableSubHeader']['ratio'];
            $headerRow[] = $Lang['medical']['report_sleep']['tableHeader']['healthProblem'];
            $headerRow[] = $Lang['medical']['report_meal']['tableSubHeader']['ratio'];
            $headerRow[] = $Lang['medical']['report_sleep']['tableHeader']['emotionProblem'];
            $headerRow[] = $Lang['medical']['report_meal']['tableSubHeader']['ratio'];
            $headerRow[] = $Lang['medical']['report_sleep']['tableHeader']['otherProblem'];
            $headerRow[] = $Lang['medical']['report_meal']['tableSubHeader']['ratio'];
            $headerRow[] = '';
            
            if (($outputFormat == 'html') || ($outputFormat == 'print')) {
                $styleHTML = <<<HTML
				<style>
					.totalCount{
						background-color: lightgray !important;
					}
					.common_table_list_v30 th{
						text-align: center !important;
					}
					.common_table_list_v30 td{
						text-align: center !important;
					}
				</style>
HTML;
                echo $styleHTML;
                echo '<table border="0" class="common_table_list_v30">';
                echo '<thead>';
                echo '<tr class="tabletop">';
                echo '<th style="min-width:70px !important; text-align: left !important;">' . $Lang['medical']['report_meal']['tableHeader']['name'] . '</th>';
                echo '<th colspan="2">' . $Lang['medical']['report_sleep']['tableHeader']['normal'] . '</th>';
                echo '<th colspan="6">' . $Lang['medical']['report_sleep']['tableHeader']['abnormal'] . '</th>';
                echo '<th>' . $Lang['medical']['report_sleep']['tableHeader']['stayForDays'] . '</th>';
                echo '</tr>';
                echo '<tr class="tabletop">';
                foreach ($headerRow as $headerItem) {
                    echo '<th>' . $headerItem . '</th>';
                }
                echo '</tr>';
                echo '</thead>';
                
                echo '<tbody>';
                foreach ($sleepStudentData as $key => $sleepStudentList) {
                    if (is_numeric($sleepStudentList['totalCount']) && $sleepStudentList['totalCount'] > 0) {
                        $percent['sleepWell'] = (number_format($sleepStudentList['sleepWell'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                        $percent['healthProblem'] = (number_format($sleepStudentList['healthProblem'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                        $percent['emotionProblem'] = (number_format($sleepStudentList['emotionProblem'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                        $percent['otherProblem'] = (number_format($sleepStudentList['otherProblem'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                    } else {
                        $percent['sleepWell'] = '0%';
                        $percent['healthProblem'] = '0%';
                        $percent['emotionProblem'] = '0%';
                        $percent['otherProblem'] = '0%';
                        $sleepStudentList['totalCount'] = 0;
                    }
                    echo '<tr>';
                    echo '<td style="text-align: left !important;">' . $sleepStudentList['Name'] . '</td>';
                    
                    if ($outputFormat == 'html') {
                        if ($sleepStudentList['sleepWell'] == '') {
                            echo '<td>-</td>';
                        } else {
                            echo '<td><a href="#" data-typeID="' . $medical_cfg['studentSleep']['sleepWellID'] . '" data-studentid="' . $key . '" class="dateDetail">' . $sleepStudentList['sleepWell'] . '</a></td>';
                        }
                    } else {
                        echo '<td>' . $this->nullReplacer($sleepStudentList['sleepWell']) . '</td>';
                    }
                    echo '<td>' . $percent['sleepWell'] . '</td>';
                    
                    if ($outputFormat == 'html') {
                        if ($sleepStudentList['healthProblem'] == '') {
                            echo '<td>-</td>';
                        } else {
                            echo '<td><a href="#" data-typeID="' . $medical_cfg['studentSleep']['healthProblemID'] . '" data-studentid="' . $key . '" class="dateDetail">' . $sleepStudentList['healthProblem'] . '</a></td>';
                        }
                    } else {
                        echo '<td>' . $this->nullReplacer($sleepStudentList['healthProblem']) . '</td>';
                    }
                    echo '<td>' . $percent['healthProblem'] . '</td>';
                    
                    if ($outputFormat == 'html') {
                        if ($sleepStudentList['emotionProblem'] == '') {
                            echo '<td>-</td>';
                        } else {
                            echo '<td><a href="#" data-typeID="' . $medical_cfg['studentSleep']['emotionProblemID'] . '" data-studentid="' . $key . '" class="dateDetail">' . $sleepStudentList['emotionProblem'] . '</a></td>';
                        }
                    } else {
                        echo '<td>' . $this->nullReplacer($sleepStudentList['emotionProblem']) . '</td>';
                    }
                    echo '<td>' . $percent['emotionProblem'] . '</td>';
                    
                    if ($outputFormat == 'html') {
                        if ($sleepStudentList['otherProblem'] == '') {
                            echo '<td>-</td>';
                        } else {
                            echo '<td><a href="#" data-typeID="' . $medical_cfg['studentSleep']['otherProblemID'] . '" data-studentid="' . $key . '" class="dateDetail">' . $sleepStudentList['otherProblem'] . '</a></td>';
                        }
                    } else {
                        echo '<td>' . $this->nullReplacer($sleepStudentList['otherProblem']) . '</td>';
                    }
                    echo '<td>' . $percent['otherProblem'] . '</td>';
                    
                    echo '<td class="totalCount">' . $sleepAttendanceList[$key] . '</td>';
                    echo '</tr>';
                }
                echo '</tbody>';
                echo '</table>';
            } else if ($outputFormat == 'csv') {
                global $PATH_WRT_ROOT;
                include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
                $lexport = new libexporttext();
                $exportColumn = array();
                $Rows = array();
                
                $header[] = $Lang['medical']['report_meal']['tableHeader']['name'];
                $header[] = $Lang['medical']['report_sleep']['tableHeader']['normal'];
                $header[] = '';
                $header[] = $Lang['medical']['report_sleep']['tableHeader']['abnormal'];
                $header[] = '';
                $header[] = '';
                $header[] = '';
                $header[] = '';
                $header[] = '';
                $header[] = $Lang['medical']['report_sleep']['tableHeader']['stayForDays'];
                $exportColumn[] = $header;
                $exportColumn[] = $headerRow;
                
                foreach ($sleepStudentData as $key => $sleepStudentList) {
                    $RowsDetail = array();
                    if (is_numeric($sleepStudentList['totalCount']) && $sleepStudentList['totalCount'] > 0) {
                        $percent['sleepWell'] = (number_format($sleepStudentList['sleepWell'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                        $percent['healthProblem'] = (number_format($sleepStudentList['healthProblem'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                        $percent['emotionProblem'] = (number_format($sleepStudentList['emotionProblem'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                        $percent['otherProblem'] = (number_format($sleepStudentList['otherProblem'] / $sleepStudentList['totalCount'] * 100, 2)) . '%';
                    } else {
                        $percent['sleepWell'] = '0%';
                        $percent['healthProblem'] = '0%';
                        $percent['emotionProblem'] = '0%';
                        $percent['otherProblem'] = '0%';
                        $sleepStudentList['totalCount'] = 0;
                    }
                    $RowsDetail[] = $sleepStudentList['Name'];
                    $RowsDetail[] = $this->nullReplacer($sleepStudentList['sleepWell']);
                    $RowsDetail[] = $percent['sleepWell'];
                    $RowsDetail[] = $this->nullReplacer($sleepStudentList['healthProblem']);
                    $RowsDetail[] = $percent['healthProblem'];
                    $RowsDetail[] = $this->nullReplacer($sleepStudentList['emotionProblem']);
                    $RowsDetail[] = $percent['emotionProblem'];
                    $RowsDetail[] = $this->nullReplacer($sleepStudentList['otherProblem']);
                    $RowsDetail[] = $percent['otherProblem'];
                    $RowsDetail[] = $sleepAttendanceList[$key];
                    
                    $Rows[] = $RowsDetail;
                }
                $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
                $lexport->EXPORT_FILE('export.csv', $exportContent);
            }
        }

        function getSleepReport3($sleepStudentData, $studentList, $startDate, $endDate, $outputFormat = 'html')
        {
            global $Lang, $medical_cfg;
            
            $startYear = date("Y", strtotime($startDate));
            $endYear = date("Y", strtotime($endDate));
            $startMonth = date("m", strtotime($startDate));
            $endMonth = date("m", strtotime($endDate));
            $startDay = date("d", strtotime($startDate));
            $endDay = date("d", strtotime($endDate));
            
            $yearList = range($startYear, $endYear);
            
            if (count($yearList) == 1) {
                $monthList = range($startMonth, $endMonth);
                $lastYear = true;
                if (count($monthList) == 1) {
                    $dayList = range($startDay, $endDay);
                    $lastMonth = true;
                }
            }
            $firstYear = true;
            $firstMonth = true;
            
            if ($outputFormat == 'html') {
                $styleHTML = <<<HTML
				<style>
					.nonSelectedDayField{
						background-color:lightgray !important;
					}
					.common_table_list_v30 tr td{
						height: 80px;
						min-height: 80px;
					}
					.dayContent tr td{
						clear: both;
						width: 100px;
						height: 40px;
						max-height: 20px;

						border: none;
					}
					.dayContent th{
						background-color: #DAECC1 !important;
					}
					.dayContent th a{
						color: black !important;
						font-weight: bold !important;
						cursor: pointer;
					}
					.dateDetail{
						cursor:pointer;
					}
				</style>
HTML;
                echo $styleHTML;
                foreach ($yearList as $year) {
                    
                    $_startMonth = 1;
                    $_lastMonth = 12;
                    if ($firstYear) {
                        $_startMonth = $startMonth;
                        $firstYear = false;
                    }
                    if ($year == $endYear) {
                        $_lastMonth = $endMonth;
                    }
                    $monthList = range($_startMonth, $_lastMonth);
                    
                    foreach ($monthList as $month) {
                        echo '<br /><br />';
                        
                        $monthWeekDayCount = 0;
                        
                        $_startDay = 1;
                        
                        $startDateStamp = $year . '-' . $month . '-1';
                        $_startWeekDay = date("w", strtotime($startDateStamp));
                        $_lastDay = date("t", strtotime($startDateStamp));
                        if ($firstMonth) {
                            $_startDay = $startDay;
                            $firstMonth = false;
                        }
                        if ($lastMonth) {
                            $_lastDay = $endDay;
                        }
                        $dayList = range($_startDay, $_lastDay);
                        
                        $nonCurrentMonthDayList = $_startDay + $_startWeekDay - 1;
                        $isFirstMonth = false;
                        
                        $monthCalenderHTML = <<<HTML
						<table style="margin: 0 auto;" class="common_table_list_v30">
							<colgroup>
								<col  style="width:120px;"/>
								<col  style="width:120px;"/>
								<col  style="width:120px;"/>
								<col  style="width:120px;"/>
								<col  style="width:120px;"/>
								<col  style="width:120px;"/>
								<col  style="width:120px;"/>
							</colgroup>
							<thead>
								<caption>{$year} - {$month}</caption>
								<tr class="tabletop">
									<th style="text-align:center !important">{$Lang['General']['DayType4'][0]}</th>
									<th style="text-align:center !important">{$Lang['General']['DayType4'][1]}</th>
									<th style="text-align:center !important">{$Lang['General']['DayType4'][2]}</th>
									<th style="text-align:center !important">{$Lang['General']['DayType4'][3]}</th>
									<th style="text-align:center !important">{$Lang['General']['DayType4'][4]}</th>
									<th style="text-align:center !important">{$Lang['General']['DayType4'][5]}</th>
									<th style="text-align:center !important">{$Lang['General']['DayType4'][6]}</th>
								<tr>
							</thead>
							<tbody>
HTML;
                        echo $monthCalenderHTML;
                        
                        for ($i = 0; $i < $nonCurrentMonthDayList; ++ $i) {
                            ++ $monthWeekDayCount;
                            
                            if ($monthWeekDayCount % 7 == 1) {
                                echo '<tr>';
                            }
                            
                            echo '<td class="nonSelectedDayField"></td>';
                            
                            if ($monthWeekDayCount % 7 == 0) {
                                echo '</tr>';
                            }
                        }
                        foreach ((array) $dayList as $day) {
                            ++ $monthWeekDayCount;
                            
                            if ($monthWeekDayCount % 7 == 1) {
                                echo '<tr>';
                            }
                            
                            $dayDetail = <<<HTML
								<td>
									<table class="dayContent" style="width:99%">
										<tr><th><a data-date="{$year}-{$month}-{$day}" class="dateDetail">{$day}</a></th></tr>
										<tr><td>
HTML;
                            
                            foreach ((array) $sleepStudentData[$year][str_pad($month, 2, "0", STR_PAD_LEFT)][str_pad($day, 2, "0", STR_PAD_LEFT)] as $dayReasonListDetail) {
                                foreach ((array) $dayReasonListDetail as $dayReasonDetail) {
                                    $dayDetail .= "<p style='border: 1px solid black; padding: 0.5em; margin: 0.1em'>";
                                    $dayDetail .= "<span class='colorBoxStyle' style='background-color:{$dayReasonDetail['Color']}'>";
                                    $dayDetail .= "&nbsp;&nbsp;&nbsp;";
                                    $dayDetail .= "</span>";
                                    $dayDetail .= "&nbsp;";
                                    $dayDetail .= "<a data-date=\"{$year}-{$month}-{$day}\" class=\"dateDetail\">" . stripslashes($dayReasonDetail['ReasonName']) . "</a>";
                                    $dayDetail .= '<br />';
                                    $dayDetail .= $dayReasonDetail['Frequency'] . $Lang['medical']['report_sleep']['tableContent']['frequency'];
                                    $dayDetail .= '<br />';
                                    $dayDetail .= "</p>";
                                }
                            }
                            
                            $dayDetail .= <<<HTML
											</td>
										</tr>
									</table>
								
								</td>
HTML;
                            echo $dayDetail;
                            
                            if ($monthWeekDayCount % 7 == 0) {
                                echo '</tr>';
                            }
                        }
                        while (++ $monthWeekDayCount % 7 != 1) {
                            echo '<td class="nonSelectedDayField"></td>';
                            $extraPad = true;
                        }
                        if ($extraPad && $monthWeekDayCount % 7 == 1) {
                            echo '</tr>';
                        }
                        
                        $monthCalenderHTML = <<<HTML
							</td>
						</table>
HTML;
                        echo $monthCalenderHTML;
                    }
                }
                echo "<input type='hidden' value='{$studentList}' name='userID' />";
            } else if ($outputFormat == 'csv') {
                global $PATH_WRT_ROOT;
                include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
                $lexport = new libexporttext();
                
                $objUser = new libuser($studentList);
                $exportColumn = array();
                $Rows = array();
                
                $Rows[] = array(
                    $Lang['medical']['report_meal']['tableHeader']['name'],
                    $objUser->UserNameLang()
                );
                $Rows[] = array(
                    $Lang['medical']['meal']['searchMenu']['date'],
                    $Lang['medical']['report_meal']['searchMenu']['item'],
                    $Lang['medical']['sleep']['tableHeader']['Frequency'],
                    $Lang['medical']['sleep']['tableHeader']['Remarks']
                );
                
                foreach ($sleepStudentData as $year => $monthData) {
                    foreach ($monthData as $month => $dayData) {
                        foreach ($dayData as $day => $dataList) {
                            $firstDayItem = true;
                            
                            foreach ($dataList as $dataRows) {
                                foreach ($dataRows as $data) {
                                    $RowsDetail = array();
                                    
                                    if ($firstDayItem) {
                                        $RowsDetail[] = $year . '-' . $month . '-' . $day;
                                        $firstDayItem = false;
                                    } else {
                                        $RowsDetail[] = '';
                                    }
                                    $RowsDetail[] = htmlspecialchars_decode(stripslashes($data['ReasonName']), ENT_QUOTES);
                                    $RowsDetail[] = $data['Frequency'];
                                    $RowsDetail[] = htmlspecialchars_decode(stripslashes($data['Remarks']), ENT_QUOTES);
                                    
                                    $Rows[] = $RowsDetail;
                                }
                            }
                        }
                    }
                }
                $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
                $lexport->EXPORT_FILE('export.csv', $exportContent);
            }
        }

        /*
         * Purpose: Get children info by ParentID
         * @param : $parentUserID - INTRANET_USER.UserID of parent
         * @return : student info: UserID, Name, ClassName, ClassNumber, SleepingType
         */
        function getStudentInfoByParent($parentUserID)
        {
            if ($parentUserID) {
                $fieldList = array();
                $fieldList[] = 'IU.UserID';
                $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
                $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
                $fieldList[] = 'YCU.ClassNumber';
                $fieldList[] = 'IUPS.stayOverNight';
                
                $fieldSelected = implode(", ", $fieldList);
                $AcademicYearID = Get_Current_Academic_Year_ID();
                
                $sql = "
						Select
							{$fieldSelected}
						From INTRANET_USER IU
						Inner Join YEAR_CLASS_USER YCU On	IU.UserID = YCU.UserID
						Inner Join YEAR_CLASS YC On YC.YearClassID = YCU.YearClassID
						Inner Join INTRANET_PARENTRELATION p On p.StudentID=IU.UserID
						Inner Join INTRANET_USER u On u.UserID=p.ParentID					
						Left Join	INTRANET_USER_PERSONAL_SETTINGS IUPS On	IUPS.UserID = IU.UserID 
						Where YC.AcademicYearID = {$AcademicYearID}             
				            And IU.RecordType=2
				            And u.RecordType=3
				            And u.UserID = {$parentUserID}";
                
                $rs = $this->objDB->returnResultSet($sql);
                
                return $rs;
            } else {
                return false;
            }
        }

        /*
         * Purpose: Get attendant status of a user by UserID and date
         * @param : $userID - INTRANET_USER.UserID
         * $date - specific date in format (YYYY-MM-DD)
         * $timeSection - specify AM or PM attendance
         * @return : return status 1 - present or late
         * 0 - absent
         */
        function getAttendanceStatusOfUser($userID, $date, $timeSection = 'AM')
        {
            $timeSection = strtoupper($timeSection);
            if ($timeSection == 'AM') {
                return $this->getAmAttendanceStatusOfUser($userID, $date);
            } else if ($timeSection == 'PM') {
                return $this->getPmAttendanceStatusOfUser($userID, $date);
            } else if ($timeSection == 'AMPM') {
                return $this->getAmAttendanceStatusOfUser($userID, $date) & $this->getPmAttendanceStatusOfUser($userID, $date);
            }
            return $this->getAmAttendanceStatusOfUser($userID, $date) | $this->getPmAttendanceStatusOfUser($userID, $date);
        }

        /*
         * Purpose: Get Morning attendant status of a user by UserID and date
         * @param : $userID - INTRANET_USER.UserID
         * $date - specific date in format (YYYY-MM-DD)
         * @return : return status 1 - present or late
         * 0 - absent
         */
        function getAmAttendanceStatusOfUser($userID, $date)
        {
            global $w2_cfg;
            
            if ((! $userID) || (! $date)) {
                return false;
            }
            
            $datePart = "";
            $day = 0;
            if (strlen($date) == 10) {
                $datePart = substr($date, 0, 4) . "_" . substr($date, 5, 2);
                $day = intval(substr($date, 8, 2));
            } else {
                return false;
            }
            $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$datePart}";
            
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            
            if ($attendanceTableExists) {
                // For attedance status field, 0= present and 2 = late, both mean the student @ school
                $sql = "SELECT (c.AMStatus = '0' Or c.AMStatus = '2') as Attendance
						FROM {$TableName} c
						WHERE c.UserID='$userID'
							AND c.DayNumber=$day";
                
                $rs = $this->objDB->returnResultSet($sql);
                $ret = 0;
                if (count($rs) == 1) {
                    $ret = $rs[0]["Attendance"];
                }
                // debug_r($sql);
                // debug_r($rs);
                
                return $ret;
            } else {
                return false;
            }
        }

        /*
         * Purpose: Get Afternoon attendant status of a user by UserID and date
         * @param : $userID - INTRANET_USER.UserID
         * $date - specific date in format (YYYY-MM-DD)
         * @return : return status 1 - present or late
         * 0 - absent
         */
        function getPmAttendanceStatusOfUser($userID, $date)
        {
            global $w2_cfg;
            
            if ((! $userID) || (! $date)) {
                return false;
            }
            
            $datePart = "";
            $day = 0;
            if (strlen($date) == 10) {
                $datePart = substr($date, 0, 4) . "_" . substr($date, 5, 2);
                $day = intval(substr($date, 8, 2));
            } else {
                return false;
            }
            $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$datePart}";
            
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            
            if ($attendanceTableExists) {
                // For attedance status field, 0= present and 2 = late, both mean the student @ school
                $sql = "SELECT (c.PMStatus = '0' Or c.PMStatus = '2') as Attendance
						FROM {$TableName} c
						WHERE c.UserID='$userID'
							AND c.DayNumber=$day";
                
                $rs = $this->objDB->returnResultSet($sql);
                $ret = 0;
                if (count($rs) == 1) {
                    $ret = $rs[0]["Attendance"];
                }
                // debug_r($sql);
                // debug_r($rs);
                
                return $ret;
            } else {
                return false;
            }
        }

        /*
         * Purpose: Get all year and month value from STUDENT_DAILY_BOWEL_LOG tables
         * @return : return associate array that contains year and month
         * @example:
         * e.g. {
         * ["Year"]=> {
         * [1970]=> "1970"
         * [2014]=> "2014"
         * }
         * ["Month"]=> {
         * ["01"]=> "01"
         * }
         * }
         */
        function getYearMonthFromBowelTable()
        {
            global $w2_cfg;
            
            $sql = "SHOW TABLES LIKE '" . $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] . "%'";
            $rs = $this->objDB->returnResultSet($sql);
            $nrRec = count($rs);
            $year = array();
            $month = array();
            for ($i = 0; $i < $nrRec; $i ++) {
                foreach ($rs[$i] as $name) {
                    if (strlen($name) >= 7) {
                        $year[] = substr($name, - 7, 4);
                        $month[] = substr($name, - 2);
                    }
                }
            }
            // Remove duplicate
            $year = array_unique($year);
            $month = array_unique($month);
            // Reindex
            sort($year, SORT_STRING);
            sort($month, SORT_STRING);
            $asso_year = array();
            if (count($year) > 0) {
                foreach ($year as $y) {
                    $asso_year[$y] = $y;
                }
            }
            $asso_month = array();
            if (count($month) > 0) {
                foreach ($month as $m) {
                    $asso_month[$m] = $m;
                }
            }
            
            $ret = array(
                "Year" => $asso_year,
                "Month" => $asso_month
            );
            return $ret;
        }

        /**
         * Get the bowel table year-month that contains record with student
         *
         * @param
         *            studentID The student ID for search year-month
         * @return array( [Year] => array(
         *         [Month],
         *         [Month],
         *         ...
         *         )
         *         )
         */
        function getYearMonthFromBowelTableByStudentID($studentID)
        {
            global $w2_cfg;
            $studentID = IntegerSafe($studentID);
            
            $yearMonthList = array();
            $sql = "SHOW TABLES LIKE '{$w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"]}%'";
            $rs = $this->objDB->returnArray($sql);
            foreach ($rs as $r) {
                $tableName = $r[0];
                $sql = "SELECT COUNT(*) AS TotalRecordCount FROM {$tableName} WHERE UserID IN ({$studentID})";
                $rs2 = $this->objDB->returnResultSet($sql);
                foreach ($rs2 as $r2) {
                    if ($r2['TotalRecordCount'] > 0) {
                        $yearMonth = trim($tableName, $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"]);
                        $year = substr($yearMonth, 0, 4);
                        $month = substr($yearMonth, 5);
                        $yearMonthList[$year][] = $month;
                    }
                }
            }
            return $yearMonthList;
        }

        /**
         * Get the studentLog table year-month that contains record with student
         *
         * @param
         *            studentID The student ID for search year-month
         * @return array( [Year] => array(
         *         [Month],
         *         [Month],
         *         ...
         *         )
         *         )
         */
        function getYearMonthFromStudentLogTableByStudentID($studentID)
        {
            global $w2_cfg;
            $studentID = IntegerSafe($studentID);
            
            $yearMonthList = array();
            $sql = "SELECT 
				DISTINCT DATE_FORMAT(RecordTime, '%Y-%m') AS YearMonth
			FROM 
				MEDICAL_STUDENT_LOG
			WHERE
				UserID IN ({$studentID})
			";
            $rs = $this->objDB->returnResultSet($sql);
            
            foreach ($rs as $r) {
                $year = substr($r['YearMonth'], 0, 4);
                $month = substr($r['YearMonth'], 5);
                $yearMonthList[$year][] = $month;
            }
            return $yearMonthList;
        }

        /*
         * Purpose: Get student info by UserID
         * @param : $sid - INTRANET_USER.UserID
         * @return : student info: UserID, Name, ClassName, ClassNumber, SleepingType
         */
        function getStudentInfoByID($sid)
        {
            if ($sid) {
                $fieldList = array();
                $fieldList[] = 'IU.UserID';
                $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
                $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
                $fieldList[] = 'YCU.ClassNumber';
                $fieldList[] = 'IUPS.stayOverNight';
                
                $fieldSelected = implode(", ", $fieldList);
                $AcademicYearID = Get_Current_Academic_Year_ID();
                
                $sql = "
						Select
							{$fieldSelected}
						From INTRANET_USER IU
						Inner Join YEAR_CLASS_USER YCU On	IU.UserID = YCU.UserID
						Inner Join YEAR_CLASS YC On YC.YearClassID = YCU.YearClassID
						Left Join	INTRANET_USER_PERSONAL_SETTINGS IUPS On	IUPS.UserID = IU.UserID 
						Where YC.AcademicYearID = {$AcademicYearID}             
				            And IU.RecordType=2
				            And IU.UserID = {$sid}";
                
                $rs = $this->objDB->returnResultSet($sql);
                
                if (count($rs) == 1) {
                    return $rs[0];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        function getStudentIDByBarcode($barcode)
        {
            if ($barcode) {
                $sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='".$this->objDB->Get_Safe_Sql_Query($barcode)."' AND RecordType = 2 AND RecordStatus = 1";
                $rs = $this->objDB->returnResultSet($sql);
                return (count($rs) == 1) ? $rs[0]['UserID'] : '';
            } else {
                return '';
            }
        }
        
        /*
         * Purpose: Get StudentLogList of a month by student
         * @param : $userID - INTRANET_USER.UserID
         * $filterYear - year value (yyyy)
         * $filterMonth - month value (mm)
         * @return : associate array: $ret[Date][Time][Seq][]
         */
        function getStudentLogListByParent($userID, $filterYear, $filterMonth)
        {
            global $w2_cfg, $Lang;
            
            $userID = IntegerSafe($userID);
            $filterYear = IntegerSafe($filterYear);
            $filterMonth = str_pad(IntegerSafe($filterMonth), 2, '0', STR_PAD_LEFT);
            if ((! $userID) || (! $filterYear) || (! $filterMonth)) {
                return false;
            }
            
            $startDay = "{$filterYear}-{$filterMonth}-01";
            $endDay = date("Y-m-t", strtotime($startDay)); // last day of the month
            studentAttendanceToTempTable($startDay, $endDay);
            
            $modifyBy = getNameFieldByLang2('IU.');
            $sql = "SELECT
				MSL.RecordID,
				MSL.UserID,
				MSL.RecordTime,
				MSL.BehaviourOne,
				MSL.BehaviourTwo,
				MSL.Duration,
				MSL.Remarks,
				MSL.DateModified,
				{$modifyBy} AS ModifyBy,
				IU.RecordType AS ModifyByRecordType,
				(
					(TSA.AMStatus = '0' OR TSA.AMStatus = '2') OR 
					(TSA.PMStatus = '0' OR TSA.PMStatus = '2')
				) AS Attendance
			FROM
				MEDICAL_STUDENT_LOG MSL
			LEFT JOIN
				TEMP_STUDENT_ATTENDANCE TSA
			ON
				MSL.UserID = TSA.UserID
			AND
				DATE(MSL.RecordTime) = DATE(TSA.RECORD_DAY)
			LEFT JOIN
				INTRANET_USER IU
			ON
				MSL.ModifyBy = IU.UserID
			WHERE
				MSL.UserID = '{$userID}'
			AND
				DATE_FORMAT(MSL.RecordTime, '%Y-%m') = '{$filterYear}-{$filterMonth}'
			ORDER BY
				DATE(MSL.RecordTime) DESC, MSL.RecordTime ASC
			";
            $rs = $this->objDB->returnResultSet($sql);
            
            $recordList = array();
            foreach ($rs as $r) {
                $date = date('Y-m-d', strtotime($r['RecordTime']));
                $time = date('H:i', strtotime($r['RecordTime']));
                if ($r['ModifyByRecordType'] == USERTYPE_PARENT) {
                    $r['ModifyBy'] .= " ({$Lang['medical']['general']['parent']})";
                }
                $recordList[$date][$time][] = $r;
            }
            
            return $recordList;
        }

        /*
         * Purpose: Get BowelLogList of a month by student
         * @param : $userID - INTRANET_USER.UserID
         * $filterYear - year value (yyyy)
         * $filterMonth - month value (mm)
         * @return : associate array: $ret[Date][Time][Seq][]
         */
        function getBowelLogListByParent($userID, $filterYear, $filterMonth)
        {
            global $w2_cfg, $Lang;
            
            $userID = IntegerSafe($userID);
            $filterYear = IntegerSafe($filterYear);
            $filterMonth = str_pad(IntegerSafe($filterMonth), 2, '0', STR_PAD_LEFT);
            if ((! $userID) || (! $filterYear) || (! $filterMonth)) {
                return false;
            }
            
            $TableName = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . "{$filterYear}_{$filterMonth}";
            
            $BowelTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] . "{$filterYear}_{$filterMonth}";
            
            $attendanceTableExists = hasDailyLogTableByName($TableName);
            $bowelTableExists = hasDailyLogTableByName($BowelTableName);
            if ($bowelTableExists) {
                $fieldList = array();
                if ($attendanceTableExists) {
                    // For attedance status field, 0= present and 2 = late, both mean the student @ school
                    $fieldList[] = "(c.AMStatus = '0' Or c.AMStatus = '2') as Attendance";
                    $attendanceJoinTable = "LEFT JOIN {$TableName} c ON c.UserID=b.UserID and c.DayNumber=day(b.RecordTime)";
                }
                
                $fieldList[] = 'b.RecordID';
                $fieldList[] = 'date(b.RecordTime) as RecordDate';
                $fieldList[] = 'substring(time(b.RecordTime),1,5) as RecordTime';
                $fieldList[] = 'b.Remarks';
                $fieldList[] = 's.StatusName';
                $fieldList[] = 's.Color';
                $fieldList[] = "case when u.RecordType=" . USERTYPE_PARENT . " then concat(u.LastUpdatedBy,' (" . $Lang['medical']['general']['parent'] . ")') else u.LastUpdatedBy end as LastUpdatedBy";
                $fieldList[] = 'b.DateModified as LastUpdatedOn';
                
                $fieldSelected = implode(", ", $fieldList);
                $sql = "SELECT {$fieldSelected}
						FROM {$BowelTableName} b
						LEFT JOIN MEDICAL_BOWEL_STATUS s ON b.BowelID=s.StatusID
							{$attendanceJoinTable}
						LEFT JOIN (SELECT " . getNameFieldByLang2() . " As LastUpdatedBy, RecordType, UserID
									FROM INTRANET_USER
								  ) u ON u.UserID=b.ModifyBy
						WHERE b.UserID='$userID'
						ORDER BY date(b.RecordTime) DESC, b.RecordTime";
                
                $rs = $this->objDB->returnResultSet($sql);
                // debug_r($sql);
                // debug_r($rs);
                $ret = array();
                $prevDate = "";
                $prevTime = "";
                
                $nrRec = count($rs);
                if ($nrRec > 0) {
                    $i = 0;
                    foreach ($rs as $r) {
                        $currDate = $r["RecordDate"];
                        $currTime = $r["RecordTime"];
                        
                        if (($currDate == $prevDate) && ($currTime == $prevTime)) {
                            $i ++;
                        } else {
                            $i = 0;
                        }
                        
                        foreach ($r as $k => $v) {
                            $ret[$currDate][$currTime][$i][$k] = $v;
                        }
                        $prevDate = $currDate;
                        $prevTime = $currTime;
                    }
                }
                return $ret;
            } else {
                return false;
            }
        }

        /*
         * Purpose: Get BowelLogDetails by recordID for parent
         * @param : $recordID - BowelLogID
         * $linterface - common UserInterface
         * $bowelLogDetail - bowelLog Details array
         * @return : UI
         */
        function getBowelLogInputUIByParent($recordID, $bowelLogDetail = '')
        {
            global $Lang, $medical_currentUserId;
            
            $useDefaultPIC = false;
            if ($bowelLogDetail == '') {
                $bowelLogDetail = new BowelLog('');
                $useDefaultPIC = true;
            }
            // debug_r($bowelLogDetail);
            $selectedBowelID = $bowelLogDetail->getBowelID();
            $inputByUserID = $bowelLogDetail->getInputBy();
            $picUserID = ($useDefaultPIC) ? $medical_currentUserId : $inputByUserID;
            // debug_r($picUserID);
            // print "inputUserID=$inputByUserID and UserType=" . USERTYPE_PARENT . " and LoginUser=$medical_currentUserId<br>";
            $picInfo = getUserInfoByLang($picUserID);
            
            $disableEdit = "";
            if (($picInfo) && count($picInfo) > 0) {
                $disPic = $picInfo["UserName"];
                if ($picInfo["RecordType"] == USERTYPE_PARENT) // Parent
{
                    $disPicExp = "&nbsp;(" . $Lang['medical']['general']['parent'] . ")";
                } else {
                    $disPicExp = "";
                }
                
                if (($_SESSION["UserType"] == USERTYPE_PARENT) && ($inputByUserID != "") && ($inputByUserID != $medical_currentUserId)) // LoginUser is parent, inputBy is not login user
{
                    $disableEdit = "disabled";
                }
            } else {
                $disPic = "";
                $disPicExp = "";
            }
            
            // //////////////////////// UI Items ////////////////////////////////////////////////
            // Time Hour
            $valueArray = array();
            $hourList = range(0, 23);
            foreach ($hourList as $hour) {
                $valueArray['time_hour'][$hour] = str_pad($hour, 2, "0", STR_PAD_LEFT);
                ;
            }
            
            // Time Minute
            /*
             * Change 00-59 to '00-29' and '30-59'
             * $minList = range(0, 59, 1);
             * foreach($minList as $min){
             * $valueArray['time_min'][$min] = str_pad($min, 2, "0", STR_PAD_LEFT);;
             * }
             */
            // //////////////////////////////////////////////////////////////////////////////////
            
            $objBowelStatus = new bowelStatus();
            $bowelStatusList = $objBowelStatus->getActiveStatus($whereCriteria = 'recordstatus = 1', $orderCriteria = '');
            $bowelStatusItems = array();
            foreach ((array) $bowelStatusList as $obj) {
                $bowelStatusItems[$obj["StatusID"]] = $obj["StatusName"];
            }
            $disStatusList = getCommonSelectionBox("event[" . $recordID . "][bowelID]", "event[" . $recordID . "][bowelID]", $bowelStatusItems, $selectedBowelID, $class = "ClsBowel", $otherFeatures = $disableEdit);
            // debug_r($bowelStatusList);
            // debug_r($bowelStatusItems);
            
            if ($bowelLogDetail->getRecordTime() != '') {
                $time_hour_Selected = (date('G', strtotime($bowelLogDetail->getRecordTime())));
                $time_min_Selected = intval(date('i', strtotime($bowelLogDetail->getRecordTime())));
            }
            $time_h = $this->getSelectionBox($id = "event[" . $recordID . "][time_hour]", $name = "event[" . $recordID . "][time_hour]", $valueArray['time_hour'], $valueSelected = $time_hour_Selected, $class = "", $otherMembers = $disableEdit);
            // $time_m = $this->getSelectionBox($id="event[".$recordID."][time_min]", $name="event[".$recordID."][time_min]", $valueArray['time_min'], $valueSelected= $time_min_Selected, $class="",$otherMembers=$disableEdit);
            if ($time_min_Selected <= 29) {
                $selected29 = 'selected="selected"';
            } else {
                $selected59 = 'selected="selected"';
            }
            $time_m = <<<HTML
				<select id="event[{$recordID}][time_min]" name="event[{$recordID}][time_min]" {$disableEdit}>
					<option value="29" {$selected29}>00-29</option>
					<option value="59" {$selected59}>30-59</option>
				</select>
HTML;
            
            $deleteEventBtn = ($disableEdit == "") ? "<span class='table_row_tool delete_event_div'><a class='deleteEventBtn delete_dim' title='" . $Lang['medical']['studentLog']['deleteEvent'] . "' ></a></span>" : "";
            
            $remarksHTML = stripslashes($bowelLogDetail->getRemarks());
            
            $fieldSetName = '';
            if (strpos($recordID, 'n') !== FALSE) {
                $fieldSetName = '<legend>' . $Lang['medical']['studentLog']['newEventDesc'] . ltrim($recordID, 'n') . '</legend>';
            }
            
            echo <<<END
				<fieldset>
					{$fieldSetName}
					<table style="width:100%" border="0" cellpadding="5" cellspacing="0" class="eventClass" >
						<colgroup>
						<col style="width:20%"/>
						<col style="width:80%"/>
						</colgroup>
						<tr>
							<td>{$Lang['medical']['bowel']['parent']['tableHeader']['BowelTime']}</td>
							<td>{$time_h}:{$time_m} {$deleteEventBtn}
							</td>
						</tr>
						<tr>
							<td>{$Lang['medical']['bowel']['parent']['tableHeader']['BowelCondition']}</td>
							<td>{$disStatusList}</td>
						</tr>
						<tr>
							<td>{$Lang['General']['Remark']}</td>
							<td><textarea id="event[{$recordID}][remarks]" name="event[{$recordID}][remarks]" style="width:300px;" rows="4" $disableEdit>{$remarksHTML}</textarea></td>
						</tr>
						<tr>
							<td>{$Lang['medical']['general']['pic']}</td>
							<td>{$disPic}{$disPicExp}</td>
						</tr>
					</table>
				</fieldset>
				
				<br />
				<br />
END;
        }

        /*
         * Purpose: Delete BowelLog record in $eventIDArray
         * @param : $eventIDArray - MEDICAL_STUDENT_BOWEL_LOG_YYYY_MM
         * $date - Specific date
         * @return : true if success and false otherwise
         */
        public function deleteBowelLogRecords($eventIDArray, $date, $byParent = true)
        {
            global $w2_cfg, $medical_currentUserId;
            if (! is_array($eventIDArray)) {
                return false;
            } else if (count($eventIDArray) > 0) {
                $datePart = substr($date, 0, 4) . "_" . substr($date, 5, 2);
                
                $bowelLogTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] . "{$datePart}";
                $bowelLogTableExists = hasDailyLogTableByName($bowelLogTableName);
                if ($bowelLogTableExists) {
                    $ids = implode(",", $eventIDArray);
                    if ($ids != "") {
                        $cond = "";
                        if ($byParent) {
                            $cond = " AND InputBy=$medical_currentUserId"; // Parent cannot delete record input by others
                        }
                        $sql = "DELETE FROM $bowelLogTableName WHERE RecordID IN (" . $ids . ")" . $cond;
                        $ret = $this->objDB->db_db_query($sql);
                        return $ret;
                    }
                } else {
                    return false;
                }
            }
        }

        public function getProblemRecordMeal($startDate/* YYYY-MM-DD */,$endDate)
        {
            global $w2_cfg;
            
            $endday = strtotime($endDate);
            $startday = strtotime($startDate);
            
            // ### Get all student attendance START ####
            $beforeStartDate = strtotime('-1 day', $startday);
            studentAttendanceToTempTable(date('Y-m-d', $beforeStartDate), date('Y-m-d', $endday));
            // ### Get all attendance END ####
            
            // ### Get all problem meal record START ####
            $mealProblemRecord = array();
            $mealYearList = returnListOfYearMonth(date('Y-m-d', $startday), date('Y-m-d', $endday));
            
            foreach ($mealYearList as $yearMonth) {
                $year = substr($yearMonth, 0, 4); // Get the year
                $month = substr($yearMonth, 5); // Get the month
                $_TableMeal = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $yearMonth;
                
                // ### Breakfast START ####
                $mealSql = "SELECT
					MSDML.RecordID,
					MSDML.UserID,
					MSDML.RecordDay,
					MSDML.PeriodStatus,
					MSDML.Remarks,
					MSDML.MealStatus,
					MSDML.ModifyBy,
					MSDML.DateModified
				FROM
					{$_TableMeal} MSDML
				LEFT JOIN
					TEMP_STUDENT_ATTENDANCE TSA
				ON
					MSDML.UserID = TSA.UserID
				AND
					STR_TO_DATE(CONCAT('{$year}-{$month}-', MSDML.RecordDay), '%Y-%m-%d') = CAST(DATE_ADD(TSA.RECORD_DAY, INTERVAL 1 DAY) AS CHAR)
				WHERE
					MSDML.PeriodStatus = {$w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["breakfast"]}
				AND
					( (TSA.PMStatus <> '0' AND TSA.PMStatus <> '2') OR (TSA.PMStatus IS NULL) )
				";
                $rs = $this->objDB->returnResultSet($mealSql);
                foreach ($rs as $r) {
                    $day = strtotime("{$year}-{$month}-{$r['RecordDay']}");
                    if ($startday <= $day && $day <= $endday) {
                        $r['Date'] = date('Y-m-d', $day);
                        $mealProblemRecord[] = $r;
                    }
                }
                // ### Breakfast END ####
                
                // ### Lunch START ####
                $mealSql = "SELECT
					MSDML.RecordID,
					MSDML.UserID,
					MSDML.RecordDay,
					MSDML.PeriodStatus,
					MSDML.Remarks,
					MSDML.MealStatus,
					MSDML.ModifyBy,
					MSDML.DateModified
				FROM
					{$_TableMeal} MSDML
				INNER JOIN
					TEMP_STUDENT_ATTENDANCE TSA
				ON
					MSDML.UserID = TSA.UserID
				AND
					STR_TO_DATE(CONCAT('{$year}-{$month}-', MSDML.RecordDay), '%Y-%m-%d') = CAST(TSA.RECORD_DAY AS CHAR)
				WHERE
					MSDML.PeriodStatus = {$w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["lunch"]}
				AND
					( (TSA.AMStatus <> '0' AND TSA.AMStatus <> '2') OR (TSA.AMStatus IS NULL) )
				";
                $rs = $this->objDB->returnResultSet($mealSql);
                
                foreach ($rs as $r) {
                    $day = strtotime("{$year}-{$month}-{$r['RecordDay']}");
                    if ($startday <= $day && $day <= $endday) {
                        $r['Date'] = date('Y-m-d', $day);
                        $mealProblemRecord[] = $r;
                    }
                }
                // ### Lunch END ####
                
                // ### Dinner START ####
                $mealSql = "SELECT
					MSDML.RecordID,
					MSDML.UserID,
					MSDML.RecordDay,
					MSDML.PeriodStatus,
					MSDML.Remarks,
					MSDML.MealStatus,
					MSDML.ModifyBy,
					MSDML.DateModified
				FROM
					{$_TableMeal} MSDML
				LEFT JOIN
					TEMP_STUDENT_ATTENDANCE TSA
				ON
					MSDML.UserID = TSA.UserID
				AND
					STR_TO_DATE(CONCAT('{$year}-{$month}-', MSDML.RecordDay), '%Y-%m-%d') = CAST(TSA.RECORD_DAY AS CHAR)
				WHERE
					MSDML.PeriodStatus = {$w2_cfg["DB_MEDICAL_STUDENT_DAILY_MEAL_LOG_PERIODSTATUS"]["dinner"]}
				AND
					( (TSA.PMStatus <> '0' AND TSA.PMStatus <> '2') OR (TSA.PMStatus IS NULL) )
				";
                $rs = $this->objDB->returnResultSet($mealSql);
                
                foreach ($rs as $r) {
                    $day = strtotime("{$year}-{$month}-{$r['RecordDay']}");
                    if ($startday <= $day && $day <= $endday) {
                        $r['Date'] = date('Y-m-d', $day);
                        $mealProblemRecord[] = $r;
                    }
                }
                // ### Dinner END ####
            }
            // ### Get all problem meal record END ####
            
            // ### Get all releated User information START ####
            
            $userIdArr = array();
            foreach ($mealProblemRecord as $record) {
                $userIdArr[] = $record['UserID'];
                $userIdArr[] = $record['ModifyBy'];
            }
            $userInformation = $this->getUserInformation($userIdArr);
            /**
             * $userIdArr = array();
             * $userInformation = array();
             * foreach ($mealProblemRecord as $record) {
             * $userIdArr[] = $record['UserID'];
             * $userIdArr[] = $record['ModifyBy'];
             * }
             * $userIdSQL = implode(',', $userIdArr);
             * $userSQL = "SELECT
             * IU.UserID,
             * {$classNameField},
             * {$nameField},
             * Y.Sequence AS YearSeq,
             * Y.YearName AS YearName,
             * YC.Sequence AS ClassSeq,
             * YCU.ClassNumber
             * FROM
             * INTRANET_USER IU
             * LEFT Join
             * YEAR_CLASS_USER YCU
             * On
             * IU.UserID = YCU.UserID
             * LEFT Join
             * YEAR_CLASS YC
             * On
             * YC.YearClassID = YCU.YearClassID
             * LEFT Join
             * YEAR Y
             * On
             * Y.YearID = YC.YearID
             * WHERE
             * IU.UserID IN ({$userIdSQL})
             * ";
             * $rs = $this->objDB->returnResultSet($userSQL);
             * foreach ($rs as $r) {
             * $userInformation[$r['UserID']] = $r;
             * }
             */
            // ### Get all releated User information END ####
            
            // ### Generate Problem Record Object START ####
            $objProblemRecordArr = array();
            foreach ($mealProblemRecord as $record) {
                $recordUserID = $record['UserID'];
                $recordModifyByUserID = $record['ModifyBy'];
                $objProblemRecord = $record;
                $objProblemRecord['ClassName'] = $userInformation[$recordUserID]['ClassName'];
                $objProblemRecord['Name'] = $userInformation[$recordUserID]['Name'];
                $objProblemRecord['YearSeq'] = $userInformation[$recordUserID]['YearSeq'];
                $objProblemRecord['YearName'] = $userInformation[$recordUserID]['YearName'];
                $objProblemRecord['ClassSeq'] = $userInformation[$recordUserID]['ClassSeq'];
                $objProblemRecord['ClassNumber'] = $userInformation[$recordUserID]['ClassNumber'];
                $objProblemRecord['ModifyByName'] = $userInformation[$recordModifyByUserID]['Name'];
                $objProblemRecordArr[] = $this->_createMealProblemRecord($objProblemRecord);
            }
            // ### Generate Problem Record Object END ####
            
            return $objProblemRecordArr;
        }

        private function _createMealProblemRecord($data)
        {
            $objData = new libProblemRecordMeal();
            $objData->UserID = $data['UserID'];
            $objData->RecordID = $data['RecordID'];
            $objData->ClassName = $data['ClassName'];
            $objData->StudentName = $data['Name'];
            $objData->PeriodStatus = $data['PeriodStatus'];
            $objData->Remarks = $data['Remarks'];
            $objData->MealStatus = $data['MealStatus'];
            $objData->ModifyByName = $data['ModifyByName'];
            $objData->DateModified = $data['DateModified'];
            $objData->YearSeq = $data['YearSeq'];
            $objData->YearName = $data['YearName'];
            $objData->ClassSeq = $data['ClassSeq'];
            $objData->ClassNumber = $data['ClassNumber'];
            $objData->Date = $data['Date'];
            return $objData;
        }

        public function getProblemRecordBowel($startDate/* YYYY-MM-DD */,$endDate)
        {
            global $w2_cfg;
            
            $endday = strtotime($endDate . ' 23:59:59');
            $startday = strtotime($startDate);
            
            studentAttendanceToTempTable($startDate, $endDate);
            
            // ### Get problem record START ####
            $problemRecord = array();
            $totalYearMonth = returnListOfYearMonth($startDate, $endDate);
            foreach ($totalYearMonth as $yearMonth) {
                $year = substr($yearMonth, 0, 4); // Get the year
                $month = substr($yearMonth, 5); // Get the month
                $_TableBowel = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] . $yearMonth;
                
                $sql = "SELECT 
					MSBL.UserID,
					MSBL.RecordID,
					MSBL.RecordTime,
					MSBL.BowelID,
					MSBL.Remarks,
					MSBL.ModifyBy,
					MSBL.DateModified
				FROM    
					{$_TableBowel} MSBL
				LEFT JOIN
					TEMP_STUDENT_ATTENDANCE TSA
				ON
					MSBL.UserID = TSA.UserID
				AND
					DATE(MSBL.RecordTime) = DATE(TSA.RECORD_DAY)
				WHERE
					(TSA.AMStatus <> '0' AND TSA.AMStatus <> '2' OR TSA.AMStatus IS NULL) 
				AND 
					(TSA.PMStatus <> '0' AND TSA.PMStatus <> '2' OR TSA.PMStatus IS NULL)
				";
                $rs = $this->objDB->returnResultSet($sql);
                foreach ($rs as $r) {
                    $day = strtotime($r['RecordTime']);
                    if ($startday <= $day && $day <= $endday) {
                        $problemRecord[] = $r;
                    }
                }
            }
            // ### Get problem record END ####
            
            // ### Get all releated User information START ####
            $userIdArr = array();
            foreach ($problemRecord as $record) {
                $userIdArr[] = $record['UserID'];
                $userIdArr[] = $record['ModifyBy'];
            }
            $userInformation = $this->getUserInformation($userIdArr);
            // ### Get all releated User information END ####
            
            // ### Generate Problem Record Object START ####
            $problemRecordArray = array();
            foreach ($problemRecord as $record) {
                $recordUserID = $record['UserID'];
                $recordModifyByUserID = $record['ModifyBy'];
                $record['ClassName'] = $userInformation[$recordUserID]['ClassName'];
                $record['Name'] = $userInformation[$recordUserID]['Name'];
                $record['YearSeq'] = $userInformation[$recordUserID]['YearSeq'];
                $record['YearName'] = $userInformation[$recordUserID]['YearName'];
                $record['ClassSeq'] = $userInformation[$recordUserID]['ClassSeq'];
                $record['ClassNumber'] = $userInformation[$recordUserID]['ClassNumber'];
                $record['ModifyByName'] = $userInformation[$recordModifyByUserID]['Name'];
                $problemRecordArray[] = $this->_createBowelProblemRecord($record);
            }
            
            // ### Generate Problem Record Object END ####
            return $problemRecordArray;
        }

        private function _createBowelProblemRecord($data)
        {
            $objData = new libProblemRecordBowel();
            $objData->UserID = $data['UserID'];
            $objData->RecordID = $data['RecordID'];
            $objData->ClassName = $data['ClassName'];
            $objData->StudentName = $data['Name'];
            $objData->ModifyByName = $data['ModifyByName'];
            $objData->DateModified = $data['DateModified'];
            $objData->YearSeq = $data['YearSeq'];
            $objData->YearName = $data['YearName'];
            $objData->ClassSeq = $data['ClassSeq'];
            $objData->ClassNumber = $data['ClassNumber'];
            
            $objData->RecordTime = $data['RecordTime'];
            $objData->Remarks = $data['Remarks'];
            $objData->BowelID = $data['BowelID'];
            return $objData;
        }

        public function getProblemRecordStudentLog($startDate/* YYYY-MM-DD */,$endDate)
        {
            global $w2_cfg;
            
            studentAttendanceToTempTable($startDate, $endDate);
            
            // ### Get problem record START ####
            $problemRecord = array();
            $sql = "SELECT 
				MSL.UserID,
				MSL.RecordID,
				MSL.RecordTime,
				MSL.BehaviourOne,
				MSL.BehaviourTwo,
				MSL.Duration,
				MSL.Remarks,
				MSL.ModifyBy,
				MSL.DateModified
			FROM    
				MEDICAL_STUDENT_LOG MSL
			LEFT JOIN
				TEMP_STUDENT_ATTENDANCE TSA
			ON
				MSL.UserID = TSA.UserID
			AND
				DATE(MSL.RecordTime) = DATE(TSA.RECORD_DAY)
			WHERE
				MSL.RecordTime >= '{$startDate} 00:00:00' AND MSL.RecordTime <= '{$endDate} 23:59:59'
			AND
				(TSA.AMStatus <> '0' AND TSA.AMStatus <> '2' OR TSA.AMStatus IS NULL) 
			AND 
				(TSA.PMStatus <> '0' AND TSA.PMStatus <> '2' OR TSA.PMStatus IS NULL)";
            $rs = $this->objDB->returnResultSet($sql);
            foreach ($rs as $r) {
                $problemRecord[] = $r;
            }
            // ### Get problem record END ####
            
            // ### Get all releated User information START ####
            $userIdArr = array();
            foreach ($problemRecord as $record) {
                $userIdArr[] = $record['UserID'];
                $userIdArr[] = $record['ModifyBy'];
            }
            $userInformation = $this->getUserInformation($userIdArr);
            // ### Get all releated User information END ####
            
            // ### Generate Problem Record Object START ####
            $problemRecordArray = array();
            foreach ($problemRecord as $record) {
                $recordUserID = $record['UserID'];
                $recordModifyByUserID = $record['ModifyBy'];
                $record['ClassName'] = $userInformation[$recordUserID]['ClassName'];
                $record['Name'] = $userInformation[$recordUserID]['Name'];
                $record['YearSeq'] = $userInformation[$recordUserID]['YearSeq'];
                $record['YearName'] = $userInformation[$recordUserID]['YearName'];
                $record['ClassSeq'] = $userInformation[$recordUserID]['ClassSeq'];
                $record['ClassNumber'] = $userInformation[$recordUserID]['ClassNumber'];
                $record['ModifyByName'] = $userInformation[$recordModifyByUserID]['Name'];
                $problemRecordArray[] = $this->_createStudentLogProblemRecord($record);
            }
            // ### Generate Problem Record Object END ####
            return $problemRecordArray;
        }

        private function _createStudentLogProblemRecord($data)
        {
            $objData = new libProblemRecordStudentLog();
            $objData->UserID = $data['UserID'];
            $objData->RecordID = $data['RecordID'];
            $objData->ClassName = $data['ClassName'];
            $objData->StudentName = $data['Name'];
            $objData->ModifyByName = $data['ModifyByName'];
            $objData->DateModified = $data['DateModified'];
            $objData->YearSeq = $data['YearSeq'];
            $objData->YearName = $data['YearName'];
            $objData->ClassSeq = $data['ClassSeq'];
            $objData->ClassNumber = $data['ClassNumber'];
            
            $objData->RecordTime = $data['RecordTime'];
            $objData->Remarks = $data['Remarks'];
            $objData->BehaviourOne = $data['BehaviourOne'];
            $objData->BehaviourTwo = $data['BehaviourTwo'];
            $objData->Duration = $data['Duration'];
            return $objData;
        }

        public function getProblemRecordSleep($startDate/* YYYY-MM-DD */,$endDate)
        {
            global $w2_cfg;
            
            studentAttendanceToTempTable($startDate, $endDate);
            // ### Get problem record START ####
            $problemRecord = array();
            $sql = "SELECT 
				MSS.UserID,
				MSS.RecordID,
				MSS.RecordTime,
				MSS.SleepID,
				MSS.ReasonID,
				MSS.Frequency,
				MSS.Remarks,
				MSS.ModifyBy,
				MSS.DateModified
			FROM    
				MEDICAL_STUDENT_SLEEP MSS
			LEFT JOIN
				TEMP_STUDENT_ATTENDANCE TSA
			ON
				MSS.UserID = TSA.UserID
			AND
				DATE(MSS.RecordTime) = DATE(TSA.RECORD_DAY)
			WHERE
				MSS.RecordTime >= '{$startDate} 00:00:00' AND MSS.RecordTime <= '{$endDate} 23:59:59'
			AND
				(TSA.PMStatus <> '0' AND TSA.PMStatus <> '2' OR TSA.PMStatus IS NULL)";
            $rs = $this->objDB->returnResultSet($sql);
            foreach ($rs as $r) {
                $problemRecord[] = $r;
            }
            // ### Get problem record END ####
            
            // ### Get all releated User information START ####
            $userIdArr = array();
            foreach ($problemRecord as $record) {
                $userIdArr[] = $record['UserID'];
                $userIdArr[] = $record['ModifyBy'];
            }
            $userInformation = $this->getUserInformation($userIdArr);
            // ### Get all releated User information END ####
            
            // ### Generate Problem Record Object START ####
            $problemRecordArray = array();
            foreach ($problemRecord as $record) {
                $recordUserID = $record['UserID'];
                $recordModifyByUserID = $record['ModifyBy'];
                $record['ClassName'] = $userInformation[$recordUserID]['ClassName'];
                $record['Name'] = $userInformation[$recordUserID]['Name'];
                $record['YearSeq'] = $userInformation[$recordUserID]['YearSeq'];
                $record['YearName'] = $userInformation[$recordUserID]['YearName'];
                $record['ClassSeq'] = $userInformation[$recordUserID]['ClassSeq'];
                $record['ClassNumber'] = $userInformation[$recordUserID]['ClassNumber'];
                $record['ModifyByName'] = $userInformation[$recordModifyByUserID]['Name'];
                $problemRecordArray[] = $this->_createSleepProblemRecord($record);
            }
            // ### Generate Problem Record Object END ####
            return $problemRecordArray;
            /*
             * $sql = array();
             * for($i = 0,$iMax=count($totalYearMonth);$i < $iMax;$i++)
             * {
             * $_Table = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"].$totalYearMonth[$i];
             * $sql[] = "SELECT
             * IU.UserID,
             * MSS.RecordID,
             * {$classNameField},
             * {$nameField},
             * MSS.RecordTime,
             * MSS.SleepID,
             * MSS.ReasonID,
             * MSS.Frequency,
             * MSS.Remarks,
             * MSS.ModifyBy,
             * {$modifedByNameField},
             * MSS.DateModified,
             * Y.Sequence AS YearSeq,
             * Y.YearName AS YearName,
             * YC.Sequence AS ClassSeq,
             * YCU.ClassNumber
             * FROM
             * MEDICAL_STUDENT_SLEEP MSS
             * INNER JOIN
             * INTRANET_USER IU
             * ON
             * IU.UserID = MSS.UserID
             * LEFT JOIN
             * {$_Table} CSDL
             * ON
             * CSDL.UserID = MSS.UserID
             * AND
             * EXTRACT(DAY FROM MSS.RecordTime) = CSDL.DayNumber
             * Inner Join
             * YEAR_CLASS_USER YCU
             * On
             * IU.UserID = YCU.UserID
             * Inner Join
             * YEAR_CLASS YC
             * On
             * YC.YearClassID = YCU.YearClassID
             * Inner Join
             * YEAR Y
             * On
             * Y.YearID = YC.YearID
             * INNER JOIN
             * INTRANET_USER IUMOD
             * ON
             * IUMOD.UserID = MSS.ModifyBy
             * WHERE
             * MSS.RecordTime >= '{$startDate} 00:00:00' AND MSS.RecordTime <= '{$endDate} 00:00:00'
             * AND
             * (CSDL.PMStatus <> '0' AND CSDL.PMStatus <> '2' OR CSDL.PMStatus IS NULL)";
             * }
             * $sql = implode( ' UNION ', $sql );
             * $sql .= ' ORDER BY YearSeq, YearName, ClassSeq, ClassName, ClassNumber';
             * $rs = $this->objDB->returnResultSet($sql);
             * return $rs;/*
             */
        }

        private function _createSleepProblemRecord($data)
        {
            $objData = new libProblemRecordSleep();
            $objData->UserID = $data['UserID'];
            $objData->RecordID = $data['RecordID'];
            $objData->ClassName = $data['ClassName'];
            $objData->StudentName = $data['Name'];
            $objData->ModifyByName = $data['ModifyByName'];
            $objData->DateModified = $data['DateModified'];
            $objData->YearSeq = $data['YearSeq'];
            $objData->YearName = $data['YearName'];
            $objData->ClassSeq = $data['ClassSeq'];
            $objData->ClassNumber = $data['ClassNumber'];
            
            $objData->RecordTime = $data['RecordTime'];
            $objData->Remarks = $data['Remarks'];
            $objData->SleepID = $data['SleepID'];
            $objData->ReasonID = $data['ReasonID'];
            $objData->Frequency = $data['Frequency'];
            return $objData;
        }

        function getUserInformation($UserIdArr)
        {
            $nameField = getNameFieldByLang2('IU.') . ' As Name';
            $classNameField = $this->getClassNameByLang2($prefix = "YC.");
            $UserIdArr = array_unique($UserIdArr);
            $userIdSQL = implode(',', $UserIdArr);
            $userInformation = array();
            $currentYear = Get_Current_Academic_Year_ID();
            $userSQL = "SELECT
				IU.UserID,
				{$classNameField} AS ClassName,
				{$nameField},
				Y.Sequence AS YearSeq,
				Y.YearName AS YearName,
				YC.Sequence AS ClassSeq,
				YCU.ClassNumber
			FROM
				INTRANET_USER IU
			LEFT Join
				YEAR_CLASS_USER YCU
			On
				IU.UserID = YCU.UserID
			LEFT Join
				YEAR_CLASS YC
			On
				YC.YearClassID = YCU.YearClassID
			LEFT Join
				YEAR Y
			On
				Y.YearID = YC.YearID
			WHERE
				IU.UserID IN ({$userIdSQL})
			AND
				(YC.AcademicYearID = {$currentYear} OR {$classNameField} IS NULL)
			";
            $rs = $this->objDB->returnResultSet($userSQL);
            foreach ($rs as $r) {
                $userInformation[$r['UserID']] = $r;
            }
            return $userInformation;
        }

        // This function supports Four Data Structures
        // 1. Bowel Report 1
        // 2. Bowel Report 1 (Thickbox)
        // 3. Bowel Report 2
        // 4. Bowel Report 2 (Thickbox)
        function getBowelReportData($reasonIDList, $userIDList, $startDate, $endDate, $sleep = '', $report = 1, $fromThickBox = false, $startTime = '00:00:00', $endTime = '23:59:59')
        {
            global $medical_cfg, $w2_cfg;
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $logTable = array();
            $resultList = array();
            
            $fieldList = array();
            if ($reasonIDList) {
                $whereReasonID = "And MSBL.BowelID In ('" . implode("','", (array) $reasonIDList) . "')";
            }
            if ($userIDList) {
                $whereUserID = "And MSBL.UserID In ('" . implode("','", (array) $userIDList) . "')";
            }
            
            $fieldList[] = 'IU.UserID';
            $fieldList[] = getNameFieldByLang2('IU.') . ' As Name';
            $fieldList[] = 'MSBL.Remarks';
            $fieldList[] = 'MSBL.RecordTime';
            
            if ($report == 1) {
                $fieldList[] = 'MBS.StatusName';
                $fieldList[] = 'MSBL.BowelID';
            } else if ($report == 2) {
                $fieldList[] = 'MBS.StatusName';
                $fieldList[] = 'MBS.StatusCode';
                $fieldList[] = $this->getClassNameByLang2($prefix = "YC.") . ' as ClassName';
                $fieldList[] = 'IUPS.stayOverNight as Stay';
                
                if ($sleep != '1') {
                    if ($sleep == '2') {
                        $whereSleep = " And IUPS.stayOverNight = '1'";
                    } else {
                        $whereSleep = " And (IUPS.stayOverNight = " . $medical_cfg['sleep_status']['StayOut']['value'] . " or IUPS.stayOverNight is null)";
                    }
                }
                $sleepTable = "
					Left Join ( Select 	IUPS.UserID, IUPS.stayOverNight 
				                From  	INTRANET_USER_PERSONAL_SETTINGS IUPS
				                Where 	1=1 " . $whereSleep . ") IUPS
					On
						IUPS.UserID = IU.UserID 
					";
            }
            
            $fieldSelected = implode(", ", $fieldList);
            
            // LIST OUT THERE IS HOW MANY MONTH BETWEEN START DATE AND END DATE
            $totalYearMonth = returnListOfYearMonth($startDate, $endDate);
            
            for ($i = 0, $iMax = count($totalYearMonth); $i < $iMax; $i ++) {
                $_Table = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] . $totalYearMonth[$i];
                if (hasDailyLogTableByName($_Table)) {
                    $logTable[] = $_Table;
                }
            }
            
            for ($i = 0, $iMax = count($logTable); $i < $iMax; ++ $i) {
                
                $datelist = explode('_', $logTable[$i]);
                $year = $datelist[4];
                $month = $datelist[5];
                
                $startDateStamp = $year . '-' . $month . '-1';
                $_startDay = date("d", strtotime($startDateStamp));
                $_lastDay = date("t", strtotime($startDateStamp));
                $_startTime = '00:00:00';
                $_endTime = '23:59:59';
                if ($i == 0) {
                    $_startDay = date("d", strtotime($startDate));
                    $_startTime = $startTime;
                }
                if (($i + 1) == $iMax) {
                    $_lastDay = date("d", strtotime($endDate));
                    $_endTime = $endTime;
                }
                
                // error_log('$_startTime -->'.print_r($_startTime,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
                // error_log('$_endTime -->'.print_r($_endTime,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
                $whereTime = "
					And
						MSBL.RecordTime >=  '{$year}-{$month}-{$_startDay} {$_startTime}'
					And
						MSBL.RecordTime <= '{$year}-{$month}-{$_lastDay} {$_endTime}'
				";
                $sql = "
				Select
					{$fieldSelected}
				From 
					{$logTable[$i]} MSBL
				Left Join
					MEDICAL_BOWEL_STATUS MBS 
				On
					MSBL.BowelID = MBS.StatusID
				Inner Join
					INTRANET_USER IU
				On
					IU.UserID = MSBL.UserID
				Inner Join
					YEAR_CLASS_USER YCU
				On
					IU.UserID = YCU.UserID
				Inner Join
					YEAR_CLASS YC
				On
					YC.YearClassID = YCU.YearClassID
				Inner Join
					YEAR Y
				On
					Y.YearID = YC.YearID
					{$sleepTable}
				Where
					YC.AcademicYearID = '{$AcademicYearID}' And IU.RecordStatus = '1'
					{$whereReasonID}
					{$whereUserID}
					{$whereTime}					
				";
                // error_log('$sql -->'.print_r($sql,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
                $rs = $this->objDB->returnResultSet($sql);
                if ($report == 1) {
                    foreach ($rs as $item) {
                        $date = $item['RecordTime'];
                        
                        if ($fromThickBox) {
                            $recordDate = date("Y-m-d", strtotime($date));
                            $time = date("H:i:s", strtotime($date));
                            
                            if (($startTime <= $time) && ($endTime >= $time)) {
                                $resultList[$item['UserID']][$recordDate][] = array(
                                    'Time' => $time,
                                    'Remarks' => $item['Remarks']
                                );
                            }
                        } else {
                            $minute = intval(date("i", strtotime($date)));
                            $hour = intval(date("H", strtotime($date)));
                            
                            // Calculate the start period //
                            $startTimeHour = intval(date("H", strtotime($startTime)));
                            $startTimeMinute = intval(date("i", strtotime($startTime)));
                            
                            /*
                             * Change to use getBowelTimeInterval() instead of 30 minutes
                             * // $shiftTimePeriod = ($startTimeHour * 2) + ( ($startTimeMinute >=0 && $startTimeMinute <= 29)? 0 : 1 );
                             * $shiftTimePeriod = 0;
                             * if($minute >=0 && $minute <=29){
                             * ++$resultList[$item['BowelID']][$hour*2 - $shiftTimePeriod];
                             * }
                             * else if($minute >=30 && $minute <=59){
                             * ++$resultList[$item['BowelID']][$hour*2+1 - $shiftTimePeriod];
                             * }
                             */
                            ++ $resultList[$item['BowelID']][($hour * 60) + $minute];
                        }
                    }
                } else if ($report == 2) {
                    
                    $j = 0;
                    foreach ($rs as $item) {
                        // $weekday = date("w", strtotime($date));
                        if ($fromThickBox) {
                            $time = date("H:i", strtotime($item['RecordTime']));
                            $resultList[$item['UserID']][$j]['Time'] = $time;
                            $resultList[$item['UserID']][$j]['StatusCode'] = $item['StatusCode'];
                            $resultList[$item['UserID']][$j]['StatusName'] = $item['StatusName'];
                            $resultList[$item['UserID']][$j]['Remarks'] = $item['Remarks'];
                            ++ $j;
                        } else {
                            $date = date("Y-m-d", strtotime($item['RecordTime']));
                            ++ $resultList[$item['UserID']][$date][$item['StatusCode']];
                            $resultList['Remarks'][$item['UserID']][$date][] = $item['Remarks'];
                        }
                    }
                }
            }
            return $resultList;
        }

        function getBowelReport1($bowelStudentData, $reasonIDList, $startTime = '00:00:00', $endTime = '00:00:00', $outputFormat = 'html', $print = false)
        {
            global $Lang, $medical_cfg;
            
            $timeFormat = $this->getBowelTimeInterval();
            $bowelStatus = new bowelStatus();
            $bowelStatusList = $bowelStatus->getAllStatus($whereCriteria = "DeletedFlag = 0 And StatusID In ('" . implode("','", (array) $reasonIDList) . "')", $orderCriteria = 'StatusCode');
            
            $statusColorList = array();
            foreach ($bowelStatusList as $bowelStatus) {
                $statusColorList[] = $bowelStatus['Color'];
            }
            
            // $startHour = 0;
            // $endTime = '00:00';
            // $currentTime = "$startHour:00:00";
            
            $startTimeHour = intval(date("H", strtotime($startTime)));
            $startTimeMinute = intval(date("i", strtotime($startTime)));
            // $shiftPeriod = ($startTimeHour * 2) + ( ($startTimeMinute >=0 && $startTimeMinute <= 29)? 0 : 1 );
            $shiftPeriod = - 1 + $timeFormat + $startTimeHour * 60 + $startTimeMinute;
            $endTime = date($endTime);
            
            $currentTime = $startTime;
            $timeSlotList = array();
            
            $safeWhile = 0;
            do {
                if (++ $safeWhile > 1500)
                    break; // Escape from endless loop (avoid logical error)
                $timeUpper = date('H:i', strtotime($currentTime));
                $timeLower = date('H:i', strtotime('+' . ($timeFormat - 1) . ' minutes', strtotime($currentTime)));
                $nextSlot = date('H:i:s', strtotime('+' . $timeFormat . ' minutes', strtotime($currentTime)));
                $timeSlotList[] = "{$timeUpper}<br/>|<br/>{$timeLower}";
                $currentTime = $nextSlot;
                $checkEnd = date('H:i:s', strtotime($currentTime) - 1);
            } while (date('H:i:s', strtotime($currentTime) - 1) != $endTime);
            
            if ($outputFormat == 'html') {
                // Report Start
                $reportHTML = <<<HTML
			<div id="chartArea">
				<table id="medical_line_graph">
					<caption>次數</caption>
					<thead>
						<tr>
							<td></td>
HTML;
                echo $reportHTML;
                foreach ((array) $timeSlotList as $timeSlot) {
                    echo "<th scope='col'>{$timeSlot}</th>";
                }
                $reportHTML = <<<HTML
						</tr>
					</thead>
					<tbody>
HTML;
                echo $reportHTML;
                foreach ($bowelStatusList as $bowelStatus) {
                    $status = trim(htmlentities(stripslashes($bowelStatus['StatusName']), ENT_QUOTES, 'UTF-8'));
                    $reportHTML = <<<HTML
						<tr>
							<th scope="row">{$status}</th>
HTML;
                    echo $reportHTML;
                    
                    foreach ((array) $timeSlotList as $key => $timeSlot) {
                        // $index is for select the specific peroid
                        // $index = ($key + $shiftPeriod > ($countPeriod-1))?$key - ($countPeriod - $shiftPeriod):$key + $shiftPeriod;
                        $index = $key * $timeFormat + $shiftPeriod;
                        $result = $this->nullReplacer($bowelStudentData[$bowelStatus['StatusID']][$index], '0');
                        echo "<td>{$result}</td>";
                    }
                    
                    $reportHTML = <<<HTML
						</tr>
HTML;
                    echo $reportHTML;
                }
                
                $reportHTML = <<<HTML
					</tbody>
				</table>
			</div>
HTML;
                echo $reportHTML;
                echo '<br />';
                
                // Report End
                
                $reportHTML = <<<HTML
			<table class="common_table_list_v30" border="0">
				<thead>
					<tr class="tabletop">
						<th style="width:60px !important;">{$Lang['medical']['report_bowel']['tableHeader']['timeVerusPeople']}</th>
HTML;
                echo $reportHTML;
                foreach ((array) $timeSlotList as $timeSlot) {
                    echo "<th style='text-align:center !important;width:40px !important;'>{$timeSlot}</th>";
                }
                $reportHTML = <<<HTML
					</tr>
				</thead>
				<tbody>
HTML;
                echo $reportHTML;
                
                foreach ((array) $bowelStatusList as $bowelStatus) {
                    $status = stripslashes($bowelStatus['StatusName']);
                    echo "<tr>";
                    echo "<td>{$status}</td>";
                    
                    foreach ((array) $timeSlotList as $key => $timeSlot) {
                        // $index is for select the specific peroid
                        // $index = ($key + $shiftPeriod > ($countPeriod-1))?$key - ($countPeriod - $shiftPeriod):$key + $shiftPeriod;
                        $index = $key * $timeFormat + $shiftPeriod;
                        $result = '';
                        if ($bowelStudentData[$bowelStatus['StatusID']][$index] != '' && ! $print) {
                            $statusID = $bowelStatus['StatusID'];
                            $period = strip_tags($timeSlot);
                            $result .= "<a href='#' class='detailDescription' data-reasonID='{$statusID}' data-period='{$period}' style='text-align:center !important;width:40px !important;'>";
                        }
                        $result .= $this->nullReplacer($bowelStudentData[$bowelStatus['StatusID']][$index]);
                        if ($bowelStudentData[$bowelStatus['StatusID']][$index] != '' && ! $print) {
                            $result .= "</a>";
                        }
                        
                        echo "<td style='text-align:center !important;width:40px !important;'>{$result}</td>";
                    }
                    echo "</tr>";
                }
                $reportHTML = <<<HTML
				</tbody>
			</table>
			<input type='hidden' name='reasonIDThickBox' id='reasonIDThickBox' value='' />
			<input type='hidden' name='period' id='period' value='' />
HTML;
                echo $reportHTML;
                $statusColorString = implode("\",\"", (array) $statusColorList);
                
                $garphWidth = count($timeSlotList) * 67; // 67 is the vector of the width
                $reportHTML = <<<HTML
			<script>
			$(document).ready( function() {
				var line_graph_colors = ["{$statusColorString}"];
				$('#chartArea').html($('#medical_line_graph').visualize({type: 'line', width: '{$garphWidth}px', height : '200px', lineWeight:'5',colors : line_graph_colors}) );
			});
			</script>
HTML;
                echo $reportHTML;
            } else if ($outputFormat == 'csv') {
                global $PATH_WRT_ROOT;
                include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
                $lexport = new libexporttext();
                $exportColumn = array();
                $Rows = array();
                
                $exportColumn[] = $Lang['medical']['report_bowel']['tableHeader']['timeVerusPeople'];
                
                foreach ((array) $timeSlotList as $timeSlot) {
                    $exportColumn[] = str_replace("|", " - ", strip_tags($timeSlot));
                }
                
                foreach ((array) $bowelStatusList as $bowelStatus) {
                    
                    $RowsDetail = array();
                    $RowsDetail[] = stripslashes($bowelStatus['StatusName']);
                    foreach ((array) $timeSlotList as $key => $timeSlot) {
                        // $index is for select the specific peroid
                        // $index = ($key + $shiftPeriod > ($countPeriod-1))?$key - ($countPeriod - $shiftPeriod):$key + $shiftPeriod;
                        $index = $key * $timeFormat + $shiftPeriod;
                        $RowsDetail[] = $this->nullReplacer($bowelStudentData[$bowelStatus['StatusID']][$index]);
                    }
                    $Rows[] = $RowsDetail;
                }
                $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
                $lexport->EXPORT_FILE('export.csv', $exportContent);
            }
        }

        // ##########################################################################################
        // # eDiscipline Start
        public function getSelectedStudent($table, $field, $value, $cond = '', $orderBy = 'Name')
        {
            global $junior_mck;
            
            $name_field = getNameFieldWithClassNumberByLang('u.');
            $sql = "SELECT 	u.UserID, 
							{$name_field} AS Name 
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							{$table} s ON s.StudentID=u.UserID
					WHERE 
							s.{$field}='{$value}' {$cond}
					ORDER BY " . $orderBy;
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['Name'] = convert2unicode($rs[$i]['Name'], 1);
                }
            }
            
            return $rs;
        }

        public function getSelectedTeacher($table, $field, $value, $cond = '')
        {
            global $junior_mck;
            
            $name_field = getNameFieldByLang('u.');
            
            $sql = "SELECT 	u.UserID, 
							{$name_field} AS Name,
							t.{$field}
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							{$table} t ON t.TeacherID=u.UserID
					WHERE 
							t.{$field}='{$value}' {$cond}
					ORDER BY Name";
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['Name'] = convert2unicode($rs[$i]['Name'], 1);
                }
            }
            
            return $rs;
        }

        public function getAllStudents($academicYearID)
        {
            global $junior_mck;
            
            $name_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
            if ($junior_mck) {
                $sql = "SELECT 		u.UserID, 
									CONCAT({$name_field}, ' (', u.ClassName, '-', u.ClassNumber, ')') AS Name 
						FROM 
									INTRANET_USER u 
						WHERE 
									u.RecordType=2
						AND 		u.RecordStatus IN (0,1,2)								
						ORDER BY 
									u.ClassName, u.ClassNumber";
                $rs = $this->objDB->returnArray($sql);
                
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i][1] = convert2unicode($rs[$i][1], 1);
                    $rs[$i]['Name'] = convert2unicode($rs[$i]['Name'], 1);
                }
            } else {
                $sql = "SELECT 		u.UserID, 
									CONCAT({$name_field}, ' (', yc.ClassTitleEN, '-', ycu.ClassNumber, ')') AS Name 
						FROM 
									INTRANET_USER u 
						INNER JOIN 
									YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						WHERE 
									yc.AcademicYearID='" . $academicYearID . "'
						AND			u.RecordType=2
						AND 		u.RecordStatus IN (0,1,2)								
						ORDER BY 
									yc.ClassTitleEN, ycu.ClassNumber";
                $rs = $this->objDB->returnArray($sql);
            }
            
            return $rs;
        }

        public function getEvents($eventID = '')
        {
            $sql = "SELECT * FROM MEDICAL_EVENT WHERE 1 ";
            if (is_array($eventID)) {
                $sql .= " AND EventID IN ('" . implode("','", $eventID) . "')";
            } else if ($eventID) {
                $sql .= " AND EventID='" . $eventID . "'";
            }
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getEventStudent($eventID, $orderBy = 'Name')
        {
            return $this->getSelectedStudent('MEDICAL_EVENT_STUDENT', 'EventID', $eventID, '', $orderBy);
        }

        public function getEventAffectedPerson($eventID, $orderBy = 'Name')
        {
            global $junior_mck;
            
            $name_field_s = getNameFieldWithClassNumberByLang('u.');
            $name_field_t = getNameFieldByLang('u.');
            $sql = "SELECT 	u.UserID, 
							IF(u.RecordType=1,{$name_field_t},{$name_field_s}) AS Name 
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							MEDICAL_EVENT_AFFECTED_PERSON p ON p.AffectedPersonID=u.UserID
					WHERE 
							p.EventID='$eventID' 
					ORDER BY " . $orderBy;
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['Name'] = convert2unicode($rs[$i]['Name'], 1);
                }
            }
            
            return $rs;
        }

        public function getUserNameByID($userID, $withTitle = true)
        {
            global $junior_mck;
            
            $name_field = $withTitle ? getNameFieldByLang() : getNameFieldByLang2();
            $sql = "SELECT 	{$name_field} AS Name FROM INTRANET_USER WHERE UserID='" . $userID . "'";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs)) {
                $rs = current($rs);
                if ($junior_mck) {
                    $rs['Name'] = convert2unicode($rs['Name'], 1);
                }
            } else {
                $rs['Name'] = '';
            }
            
            return $rs['Name'];
        }

        public function getEventAttachment($eventID)
        {
            $cond = (is_array($eventID)) ? "EventID IN ('" . implode("','", (array) $eventID) . "')" : "EventID='" . $eventID . "'";
            $sql = "SELECT * FROM MEDICAL_EVENT_ATTACHMENT WHERE {$cond} ORDER BY FileName";
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function getEventFollowup($eventID = '', $followupID = '')
        {
            $sql = "SELECT * FROM MEDICAL_EVENT_FOLLOWUP WHERE 1";
            if (is_array($eventID) && $eventID != '') {
                $sql .= " AND EventID IN ('" . implode("','", (array) $eventID) . "')";
            } else if ($eventID) {
                $sql .= " AND EventID='$eventID'";
            }
            
            if ($followupID) {
                $sql .= " AND FollowupID='$followupID'";
            }
            $sql .= " ORDER BY EventID, FollowupID";
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function getEventFollowupAttachment($followupID)
        {
            $cond = (is_array($followupID)) ? "FollowupID IN ('" . implode("','", (array) $followupID) . "')" : "FollowupID='" . $followupID . "'";
            $sql = "SELECT * FROM MEDICAL_EVENT_FOLLOWUP_ATTACHMENT WHERE {$cond} ORDER BY FileName";
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function getEventCase($eventID, $caseID = '')
        {
            $sql = "SELECT * FROM MEDICAL_CASE_EVENT WHERE 1 ";
            if (is_array($eventID)) {
                $sql .= " AND EventID IN ('" . implode("','", $eventID) . "')";
            } else if ($eventID) {
                $sql .= " AND EventID='" . $eventID . "'";
            }
            if ($caseID) {
                $sql .= " AND CaseID='" . $caseID . "'";
            }
            $sql .= "ORDER BY CaseID";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getEventCaseList($eventID, $viewMode = '')
        {
            global $Lang;
            
            if (is_array($eventID)) {
                $cond = " AND ce.EventID IN ('" . implode("','", $eventID) . "')";
            } else if ($eventID) {
                $cond = " AND ce.EventID='$eventID'";
            } else {
                $cond = '';
            }
            
            if ($cond != '') {
                $sql = "SELECT 	ce.EventID,
                                c.CaseID,
                                c.CaseNo,
                                c.StudentID,
                                c.StartDate,
								c.Summary
						FROM
								MEDICAL_CASE c
						INNER JOIN
								MEDICAL_CASE_EVENT ce ON ce.CaseID=c.CaseID
								WHERE 1" . $cond . "
						ORDER BY c.CaseNo";
                $caseAry = $this->objDB->returnResultSet($sql);
            } else {
                return '';
            }
            
            $x = '';
            for ($i = 0, $iMax = count($caseAry); $i < $iMax; $i ++) {
                $rs = $caseAry[$i];
                
                $studentInfoAry = $this->getStudentInfoByID($rs['StudentID']);
                
                if (count($studentInfoAry)) {
                    $studentName = $studentInfoAry['Name'];
                    if (! empty($studentInfoAry['ClassName']) && ! empty($studentInfoAry['ClassNumber'])) {
                        $studentName .= '(' . $studentInfoAry['ClassName'] . '-' . $studentInfoAry['ClassNumber'] . ')';
                    }
                } else {
                    $studentName = '';
                }
                
                $x .= '<tr id="event_case_row_' . $rs['CaseID'] . '">';
                $x .= '<td style="white-space:nowrap;"><a href="#" class="viewCase" data-CaseID="' . $rs['CaseID'] . '">' . $rs['CaseNo'] . '</a></td>';
                $x .= '<td>' . $studentName . '</td>';
                $x .= '<td>' . $rs['StartDate'] . '</td>';
                $x .= '<td>' . nl2br($rs['Summary']) . '</td>';
                if (! $viewMode) {
                    $x .= '<td><span class="table_row_tool"><a class="delete removeCase" title="' . $Lang['medical']['event']['RemoveCase'] . '" data-CaseID="' . $rs['CaseID'] . '" data-EventID="' . $rs['EventID'] . '"></a></span>';
                    $x .= '<input type="hidden" name="SelCaseID[]" class="selectCase" value="' . $rs['CaseID'] . '"></td>';
                }
                $x .= '</tr>';
            }
            return $x;
        }

        public function getEventStudentLog($eventID = '', $studentLogID = '')
        {
            if (empty($eventID) && empty($studentLogID)) {
                return false;
            } else {
                $orderBy = "EventID";
                $sql = "SELECT * FROM MEDICAL_EVENT_STUDENT_LOG WHERE 1 ";
                if (is_array($eventID)) {
                    $sql .= " AND EventID IN ('" . implode("','", $eventID) . "')";
                    $orderBy = "EventID, StudentLogID";
                } else if ($eventID) {
                    $sql .= " AND EventID='" . $eventID . "'";
                    $orderBy = "StudentLogID";
                }
                
                if (is_array($studentLogID)) {
                    $sql .= " AND StudentLogID IN ('" . implode("','", $studentLogID) . "')";
                    $orderBy = "StudentLogID, EventID";
                } else if ($studentLogID) {
                    $sql .= " AND StudentLogID='" . $studentLogID . "'";
                    $orderBy = "EventID";
                }
                $sql .= "ORDER BY " . $orderBy;
                $rs = $this->objDB->returnResultSet($sql);
                return $rs;
            }
        }

        public function getEventStudentLogList($eventID, $viewMode = '')
        {
            global $Lang;
            
            if (is_array($eventID)) {
                $cond = " AND es.EventID IN ('" . implode("','", $eventID) . "')";
            } else if ($eventID) {
                $cond = " AND es.EventID='$eventID'";
            } else {
                $cond = '';
            }
            
            if ($cond != '') {
                $sql = "SELECT      e.RecordID AS StudentLogID,
                                    DATE_FORMAT(e.RecordTime,'%Y-%m-%d') AS RecordDate,
                                    DATE_FORMAT(e.RecordTime,'%H:%i') AS RecordTime,
                                    e.UserID,
                                    lev1.Lev1Name,
                                    lev2.Lev2Name,
                                    e.Remarks,
                                    es.EventID
                        FROM
                                    MEDICAL_STUDENT_LOG e
                        INNER JOIN
                                    INTRANET_USER u ON u.UserID=e.UserID
                        INNER JOIN  
                                    MEDICAL_EVENT_STUDENT_LOG es ON es.StudentLogID=e.RecordID
                        LEFT JOIN
                                    MEDICAL_STUDENT_LOG_LEV1 lev1 ON lev1.Lev1ID=e.BehaviourOne
                        LEFT JOIN
                                    MEDICAL_STUDENT_LOG_LEV2 lev2 ON lev2.Lev2ID=e.BehaviourTwo
                        WHERE 1 " . $cond . " ORDER BY e.RecordTime";
                $studentLogAry = $this->objDB->returnResultSet($sql);
            } else {
                return '';
            }
            
            $x = '';
            for ($i = 0, $iMax = count($studentLogAry); $i < $iMax; $i ++) {
                $rs = $studentLogAry[$i];
                
                $studentInfoAry = $this->getStudentInfoByID($rs['UserID']);
                
                if (count($studentInfoAry)) {
                    $studentName = $studentInfoAry['Name'];
                    if (! empty($studentInfoAry['ClassName']) && ! empty($studentInfoAry['ClassNumber'])) {
                        $studentName .= '(' . $studentInfoAry['ClassName'] . '-' . $studentInfoAry['ClassNumber'] . ')';
                    }
                } else {
                    $studentName = '';
                }
                
                $lev1Type = $rs['Lev1Name'];
                $lev2Item = $rs['Lev2Name'];
                
                // no new line character here, for javascript string
                $x .= '<tr id="eventStudentLogRow' . $rs['StudentLogID'] . '">';
                $x .= '<td style="white-space:nowrap;"><a href="#" class="viewStudentLog" data-StudentLogID="' . $rs['StudentLogID'] . '">' . $rs['RecordDate'] . '</a></td>';
                $x .= '<td>' . $rs['RecordTime'] . '</td>';
                $x .= '<td>' . $studentName . '</td>';
                $x .= '<td>' . $rs['Lev1Name'] . '</td>';
                $x .= '<td>' . $rs['Lev2Name'] . '</td>';
                $x .= '<td>' . nl2br($rs['Remarks']) . '</td>';
                if (! $viewMode) {
                    $x .= '<td><span class="table_row_tool"><a class="delete removeStudentLog" title="' . $Lang['medical']['studentLog']['RemoveStudentLog'] . '" data-StudentLogID="' . $rs['StudentLogID'] . '" data-EventID="' . $rs['EventID'] . '"></a></span>';
                    $x .= '<input type="hidden" name="selStudentLogID[]" class="selectStudentLog" value="' . $rs['StudentLogID'] . '"></td>';
                }
                $x .= '</tr>';
            }
            return $x;
        }

        public function getCaseMinuteAttachment($minuteID)
        {
            $cond = (is_array($minuteID)) ? "MinuteID IN ('" . implode("','", (array) $minuteID) . "')" : "MinuteID='" . $minuteID . "'";
            $sql = "SELECT * FROM MEDICAL_CASE_MINUTE_ATTACHMENT WHERE {$cond} ORDER BY FileName";
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function setNextCaseNo($academicYearID, $yearPart, $currentNo, $prefix = 'D')
        {
            if ($currentNo == 0) {
                $nextNo = 1;
            } else if ($currentNo == '9999') {
                $nextNo = 0; // overflow error
            } else {
                $nextNo = (int) ($currentNo) + 1;
            }
            
            if ($nextNo != 0) {
                $sql = "SELECT NextNo FROM MEDICAL_CASE_NO WHERE AcademicYearID='" . $academicYearID . "' AND Prefix='" . $prefix . "'";
                $rs = $this->objDB->returnResultSet($sql);
                if (count($rs) == 1) {
                    $sql = "UPDATE MEDICAL_CASE_NO SET NextNo='" . $nextNo . "' WHERE AcademicYearID='" . $academicYearID . "' AND Prefix='" . $prefix . "'";
                } else {
                    $sql = "INSERT INTO MEDICAL_CASE_NO (AcademicYearID, YearPart, Prefix, NextNo) VALUES (
								'" . $academicYearID . "',
								'" . $yearPart . "',
								'" . $prefix . "',
								'" . $nextNo . "')";
                }
                $result = $this->objDB->db_db_query($sql);
                return $result;
            } else {
                return false;
            }
        }

        // get next case number
        public function getNextCaseNo($prefix = 'D')
        {
            $academicYearID = Get_Current_Academic_Year_ID();
            $startDate = getStartDateOfAcademicYear($academicYearID);
            $endDate = getEndDateOfAcademicYear($academicYearID);
            $yearPart = substr($startDate, 0, 5) . substr($endDate, 2, 2);
            
            $sql = "SELECT YearPart, Prefix, NextNo FROM MEDICAL_CASE_NO WHERE AcademicYearID='" . $academicYearID . "' AND Prefix='" . $prefix . "'";
            $rs = $this->objDB->returnResultSet($sql);
            if (count($rs) == 1) {
                $nextNo = $rs[0]['YearPart'] . '/' . $prefix . '/' . $rs[0]['NextNo'];
            } else {
                $nextNo = $yearPart . '/' . $prefix . '/' . '0001';
                $this->setNextCaseNo($academicYearID, $yearPart, $currentNo = 0, $prefix);
            }
            return $nextNo;
        }

        public function getCaseMinute($caseID = '', $minuteID = '')
        {
            $sql = "SELECT * FROM MEDICAL_CASE_MINUTE WHERE 1";
            if (is_array($caseID)) {
                $sql .= " AND CaseID IN ('" . implode("','", $caseID) . "')";
            } else if ($caseID) {
                $sql .= " AND CaseID='$caseID'";
            }
            
            if ($minuteID) {
                $sql .= " AND MinuteID='$minuteID'";
            }
            $sql .= " ORDER BY CaseID, MeetingDate, MinuteID";
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function getCases($caseID = '')
        {
            $sql = "SELECT * FROM MEDICAL_CASE WHERE 1 ";
            if (is_array($caseID)) {
                $sql .= " AND CaseID IN ('" . implode("','", $caseID) . "')";
            } else if ($caseID) {
                $sql .= " AND CaseID='" . $caseID . "'";
            }
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getCasePIC($caseID)
        {
            if (is_array($caseID)) {
                $caseIDSql = implode("','", $caseID);
                $cond = " OR CaseID IN ('{$caseIDSql}')";
                
                return $this->getSelectedTeacher('MEDICAL_CASE_PIC', 'CaseID', '-1', $cond);
            }
            return $this->getSelectedTeacher('MEDICAL_CASE_PIC', 'CaseID', $caseID);
        }

        public function getCaseEvent($caseID, $eventID = '')
        {
            $sql = "SELECT * FROM MEDICAL_CASE_EVENT WHERE 1 ";
            if (is_array($caseID)) {
                $sql .= " AND CaseID IN ('" . implode("','", $caseID) . "')";
            } else if ($caseID) {
                $sql .= " AND CaseID='" . $caseID . "'";
            }
            if ($eventID) {
                $sql .= " AND EventID='" . $eventID . "'";
            }
            $sql .= "ORDER BY EventID";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getCaseEventList($caseID, $viewMode = '')
        {
            global $Lang;
            $eventTypeAry = $this->getEventType();
            $eventTypeAry = BuildMultiKeyAssoc($eventTypeAry, 'EventTypeID', $IncludedDBField = array(
                'EventTypeName'
            ), $SingleValue = 1);
            
            if (is_array($caseID)) {
                $cond = " AND ce.CaseID IN ('" . implode("','", $caseID) . "')";
            } else if ($caseID) {
                $cond = " AND ce.CaseID='$caseID'";
            } else {
                $cond = '';
            }
            
            if ($cond != '') {
                $sql = "SELECT 	ce.CaseID,
								e.EventID,
								e.EventDate,
								DATE_FORMAT(e.StartTime,'%k:%i') as StartTime,
								e.EventTypeID,
								e.Summary
						FROM
								MEDICAL_EVENT e
						INNER JOIN
								MEDICAL_CASE_EVENT ce ON ce.EventID=e.EventID
								WHERE 1" . $cond . "
						ORDER BY e.EventDate, e.StartTime";
                $event = $this->objDB->returnResultSet($sql);
            } else {
                return '';
            }
            
            $x = '';
            for ($i = 0, $iMax = count($event); $i < $iMax; $i ++) {
                $rs = $event[$i];
                
                $x .= '<tr id="case_event_row_' . $rs['EventID'] . '">
							<td style="white-space:nowrap;"><a href="javascript:view_event(\'' . $rs['EventID'] . '\')">' . $rs['EventDate'] . '</a></td>
							<td>' . $rs['StartTime'] . '</td>
							<td>' . stripslashes($eventTypeAry[$rs['EventTypeID']]) . '</td>
							<td>' . nl2br($rs['Summary']) . '</td>' . ($viewMode ? '' : '
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['case']['RemoveEvent'] . '" onClick="remove_case_event(' . $rs['CaseID'] . ',' . $rs['EventID'] . ')"></a></span>
							<input type="hidden" name="SelEventID[]" value="' . $rs['EventID'] . '"></td>') . '
						</tr>';
            }
            return $x;
        }

        public function getCaseMinuteList($caseID, $mode = 'view', $viewMode = '')
        {
            global $Lang;
            
            $case_minute = $this->getCaseMinute($caseID);
            $x = '';
            
            for ($i = 0, $iMax = count($case_minute); $i < $iMax; $i ++) {
                $cm = $case_minute[$i];
                $attachment = $this->getCurrentAttachmentLayout('minute', $cm['MinuteID'], $mode);
                $x .= '<tr id="case_minute_row_' . $cm['MinuteID'] . '">
							<td class="row_num">' . ($i + 1) . '</td>
							<td style="white-space:nowrap;">' . ($viewMode ? $cm['MeetingDate'] : '<a class="MeetingDate" href="javascript:edit_minute(\'' . $cm['MinuteID'] . '\')">' . $cm['MeetingDate'] . '</a>') . '</td>
							<td>' . $attachment . '</td>' . ($viewMode ? '' : '
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['case']['RemoveMinutes'] . '" onClick="removeMinute(' . $cm['MinuteID'] . ')"></a></span>
								<input type="hidden" name="SelMinuteID[]" value="' . $cm['MinuteID'] . '"></td>') . '
						</tr>';
            }
            return $x;
        }

        public function getCaseMinuteRow($minuteID, $rowNo = '')
        {
            global $Lang;
            
            $minute = $this->getCaseMinute($caseID = '', $minuteID);
            $x = '';
            if (count($minute) == 1) {
                $cm = $minute[0];
                $attachment = $this->getCurrentAttachmentLayout('minute', $cm['MinuteID'], $mode = 'view');
                $x .= '<tr id="case_minute_row_' . $cm['MinuteID'] . '">
							<td class="row_num">' . $rowNo . '</td>
							<td style="white-space:nowrap;"><a class="MeetingDate" href="javascript:edit_minute(\'' . $cm['MinuteID'] . '\')">' . $cm['MeetingDate'] . '</a></td>
							<td>' . $attachment . '</td>
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['case']['RemoveMinutes'] . '" onClick="removeMinute(' . $cm['MinuteID'] . ')"></a></span>
								<input type="hidden" name="SelMinuteID[]" value="' . $cm['MinuteID'] . '"></td>
						</tr>';
            }
            return $x;
        }

        // void after 2018-03-23
        public function getCaseRef4Event($caseID, $viewMode = '', $returnID = false)
        {
            global $Lang;
            
            $case = $this->getCases($caseID);
            $x = '';
            if (count($case) == 1) {
                $case = current($case);
                if ($viewMode) {
                    $x = $case['CaseNo'];
                    if ($returnID) {
                        $x .= '<input type="hidden" name="CaseID" id="CaseID" value="' . $case['CaseID'] . '">';
                    }
                } else {
                    $x = '<a href="javascript:view_case(\'' . $case['CaseID'] . '\')">' . $case['CaseNo'] . '</a>';
                    if ($returnID) {
                        $x .= '<input type="hidden" name="CaseID" id="CaseID" value="' . $case['CaseID'] . '">';
                    }
                }
            }
            return $x;
        }

        public function getCaseRowForEvent($caseID, $eventID = '')
        {
            global $Lang;
            
            $caseAry = $this->getCases($caseID);
            
            $x = '';
            for ($i = 0, $iMax = count($caseAry); $i < $iMax; $i ++) {
                $rs = $caseAry[$i];
                $studentInfoAry = $this->getStudentInfoByID($rs['StudentID']);
                
                if (count($studentInfoAry)) {
                    $studentName = $studentInfoAry['Name'];
                    if (! empty($studentInfoAry['ClassName']) && ! empty($studentInfoAry['ClassNumber'])) {
                        $studentName .= '(' . $studentInfoAry['ClassName'] . '-' . $studentInfoAry['ClassNumber'] . ')';
                    }
                } else {
                    $studentName = '';
                }
                
                // no new line character here, for javascript string
                $x .= '<tr id="event_case_row_' . $rs['CaseID'] . '">';
                $x .= '<td style="white-space:nowrap;"><a href="#" class="viewCase" data-CaseID="' . $rs['CaseID'] . '">' . $rs['CaseNo'] . '</a></td>';
                $x .= '<td>' . $studentName . '</td>';
                $x .= '<td>' . $rs['StartDate'] . '</td>';
                $x .= '<td>' . nl2br($rs['Summary']) . '</td>';
                $x .= '<td><span class="table_row_tool"><a class="delete removeCase" title="' . $Lang['medical']['event']['RemoveCase'] . '" data-CaseID="' . $rs['CaseID'] . '" data-EventID="' . $eventID . '"></a></span>';
                $x .= '<input type="hidden" name="SelCaseID[]" class="selectCase" value="' . $rs['CaseID'] . '"></td>';
                $x .= '</tr>';
            }
            return $x;
        }

        // $groupID is array
        public function getGroupByGroupID($groupID, $orderBy = '')
        {
            $name_field = Get_Lang_Selection('g.TitleChinese', 'g.Title');
            $currentAcademicYearID = Get_Current_Academic_Year_ID();
            if (! is_array($groupID) && $groupID != '') {
                $groupID = array(
                    $groupID
                );
            }
            $cond .= " AND g.GroupID IN ('" . implode("','", (array) $groupID) . "')";
            
            if (is_array($groupID)) {
                $sql = "SELECT 
									g.GroupID,
									$name_field as GroupTitle
						FROM 
									INTRANET_GROUP g
						WHERE 		1
	 								$cond 
						ORDER BY 	$name_field";
                
                $rs = $this->objDB->returnArray($sql);
                
                if ($orderBy == 'physical') { // input sequence
                    $ret = BuildMultiKeyAssoc($rs, 'GroupID', $IncludedDBField = array(
                        'GroupTitle'
                    ), $SingleValue = 1);
                    for ($i = 0, $iMax = count($groupID); $i < $iMax; $i ++) {
                        $rs[$i]['GroupID'] = $groupID[$i];
                        $rs[$i]['GroupTitle'] = $ret[$groupID[$i]];
                    }
                }
            } else {
                $rs = '';
            }
            
            return $rs;
        }

        // for new add row
        public function getTargetGroupRow($groupID, $rowID)
        {
            global $Lang;
            
            $group = $this->getGroupByGroupID($groupID, $orderBy = 'physical');
            $x = '';
            if (count($group)) {
                $x .= '<tr id="target_row_n' . $rowID . '" class="target_row">
							<td class="TargetName"><input type="text" autocomplete="off" name="target_name_n[]" id="target_id_n' . $rowID . '" value=""><span class="error_msg_hide" id="ErrTarget_' . $rowID . '">' . $Lang['medical']['award']['Warning']['InputSuggestion'] . '</span></td>';
                foreach ((array) $group as $k => $v) {
                    $x .= '<td class="Group_' . $v['GroupID'] . '"><input type="checkbox" name="target_group_n[' . $v['GroupID'] . '][' . $rowID . ']" value="1"></td>';
                }
                $x .= '<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['award']['RemoveTargetSuggestion'] . '" onClick="removeTarget(\'' . $rowID . '\')"></a></span></td>
						</tr>';
            }
            return $x;
        }

        public function getTargetGroupTitle($groupID, $group = '', $mode = 'edit', $schemeID = '')
        {
            global $Lang, $LAYOUT_SKIN;
            
            if ($group == '') {
                $group = $this->getGroupByGroupID($groupID, $orderBy = 'physical');
            }
            $x = '';
            if (count($group)) {
                $x .= '<tr id="target_row_title" class="tabletop">
							<th>' . $Lang['medical']['award']['Target'] . '</th>';
                if (($mode == 'view') && ($schemeID)) {
                    foreach ((array) $group as $k => $v) {
                        $isSchemeGroupTeacher = $this->isSchemeGroupTeacher($_SESSION['UserID'], $v['GroupID']);
                        $isSchemePIC = $this->getSelectedTeacher('MEDICAL_AWARD_SCHEME_PIC', 'SchemeID', $schemeID, $cond = " AND t.TeacherID='" . $_SESSION['UserID'] . "'");
                        $isSchemePIC = count($isSchemePIC) ? true : false;
                        
                        // GroupPIC, group member teacher or medical superadmin can edit the performance page
                        if ($isSchemeGroupTeacher || $isSchemePIC || $_SESSION['SSV_USER_ACCESS']['eAdmin-medical']) {
                            $x .= '<th class="Group_' . $v['GroupID'] . '"><a href="#TB_inline?height=500&width=800&inlineId=FakeLayer" onClick="edit_performance(\'' . $schemeID . '\',\'' . $v['GroupID'] . '\')" title="' . $Lang['medical']['award']['EditPerformance'] . '">';
                            $x .= '<div id="EditScheme_' . $schemeID . '" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);">' . $v['GroupTitle'] . '</div></a></th>';
                        } else {
                            $x .= '<th class="Group_' . $v['GroupID'] . '">' . $v['GroupTitle'] . '</th>';
                        }
                    }
                } else {
                    foreach ((array) $group as $k => $v) {
                        $x .= '<th class="Group_' . $v['GroupID'] . '">' . $v['GroupTitle'] . '</th>';
                    }
                    
                    if ($mode == 'edit') {
                        $x .= '<th width="2%">&nbsp;</th>';
                    }
                }
                $x .= '</tr>';
            }
            return $x;
        }

        public function getSchemeTarget($schemeID = '', $targetID = '', $groupID = '')
        {
            global $junior_mck;
            
            $sql = "SELECT 	SchemeID,
							TargetID,
							Target,
							GroupID
					FROM
							MEDICAL_AWARD_SCHEME_TARGET
					WHERE   1";
            if ($schemeID) {
                $sql .= " AND SchemeID='" . $schemeID . "'";
            }
            if ($targetID) {
                $sql .= " AND TargetID='" . $targetID . "'";
            }
            if ($groupID) {
                $sql .= " AND CONCAT(',',GroupID,',') LIKE '%," . $groupID . ",%'";
            }
            $sql .= "							
					ORDER BY 
							TargetID";
            
            $target = $this->objDB->returnResultSet($sql);
            if (count($target)) {
                foreach ((array) $target as $k => $v) {
                    $group = $this->getGroupByGroupID(array(
                        $v['GroupID']
                    ));
                    $target[$k]['Group'] = $group;
                }
            }
            
            return $target;
        }

        public function getSchemeTargetList($schemeID, $mode = 'edit')
        {
            global $Lang, $image_path, $LAYOUT_SKIN;
            
            $image_tick = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif' width='20' height='20' align='absmiddle'>";
            $image_blank = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' align='absmiddle'>";
            
            $x = '';
            $scheme = $this->getSchemes($schemeID);
            if (count($scheme) == 1) {
                $scheme = current($scheme);
                $schemeGroupID = explode(",", $scheme['GroupID']);
                $group = $this->getGroupByGroupID($schemeGroupID, $orderBy = 'physical');
                $schemeTarget = $this->getSchemeTarget($schemeID);
                
                $x .= $this->getTargetGroupTitle($schemeGroupID, $group, $mode, $schemeID);
                
                for ($i = 0, $iMax = count($schemeTarget); $i < $iMax; $i ++) {
                    $st = $schemeTarget[$i];
                    $targetGroupID = $st['GroupID'];
                    $targetGroupIDAry = explode(",", $targetGroupID);
                    
                    $x .= '<tr id="target_row_e' . $st['TargetID'] . '" class="target_row">
								<td class="TargetName">';
                    if ($mode == 'edit') { // edit
                        $x .= '<input type="hidden" name="TargetID[]" value="' . $st['TargetID'] . '">';
                        $x .= '<input type="text" autocomplete="off" name="target_name_e[]" id="target_id_e' . $st['TargetID'] . '" value="' . intranet_htmlspecialchars($st['Target']) . '"><span class="error_msg_hide" id="ErrTarget_' . $st['TargetID'] . '">' . $Lang['medical']['award']['Warning']['InputSuggestion'] . '</span></td>';
                        foreach ((array) $group as $k => $v) {
                            $x .= '<td class="Group_' . $v['GroupID'] . '"><input type="checkbox" name="target_group_e[' . $v['GroupID'] . '][' . $st['TargetID'] . ']" value="1"' . (in_array($v['GroupID'], (array) $targetGroupIDAry) ? ' checked' : '') . '></td>';
                        }
                        $x .= '<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['award']['RemoveTargetSuggestion'] . '" onClick="removeTarget(\'R' . $st['TargetID'] . '\')"></a></span></td>';
                    } else { // view
                        $x .= $st['Target'] . '</td>';
                        foreach ((array) $group as $k => $v) {
                            $x .= '<td class="Group_' . $v['GroupID'] . '">' . (in_array($v['GroupID'], (array) $targetGroupIDAry) ? $image_tick : $image_blank) . '</td>';
                        }
                    }
                    $x .= '</tr>';
                }
            }
            return $x;
        }

        public function getSchemes($schemeID = '')
        {
            $sql = "SELECT * FROM MEDICAL_AWARD_SCHEME WHERE 1 ";
            if (is_array($schemeID)) {
                $sql .= " AND SchemeID IN ('" . implode("','", $schemeID) . "')";
            } else if ($schemeID) {
                $sql .= " AND SchemeID='" . $schemeID . "'";
            }
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getSchemePIC($schemeID)
        {
            if (is_array($schemeID)) {
                $schemeIDSql = implode("','", $schemeID);
                $cond = " OR SchemeID IN ('{$schemeIDSql}')";
                
                return $this->getSelectedTeacher('MEDICAL_AWARD_SCHEME_PIC', 'SchemeID', '-1', $cond);
            }
            return $this->getSelectedTeacher('MEDICAL_AWARD_SCHEME_PIC', 'SchemeID', $schemeID);
        }

        public function getSchemeListByStudent($studentID)
        {
            if ($studentID != '') {
                $sql = "SELECT  
									s.SchemeID,
									s.SchemeName,
									s.StartDate,
									s.EndDate
						FROM 	
									MEDICAL_AWARD_SCHEME s
						INNER JOIN	
									MEDICAL_AWARD_SCHEME_STUDENT_TARGET t ON t.SchemeID=s.SchemeID 
						WHERE 
									t.StudentID='" . $studentID . "'
						GROUP BY 	s.SchemeID
						ORDER BY 	s.StartDate";
                
                $rs = $this->objDB->returnResultSet($sql);
                return $rs;
            } else {
                return '';
            }
        }

        public function checkPerformanceExistByGroupID($schemeID, $groupID)
        {
            $ret = false; // default not exist
            if (! is_array($groupID) && $groupID != '') {
                $groupID = array(
                    $groupID
                );
            }
            if (is_array($groupID)) {
                
                $sql = "SELECT  
									s.StudentTargetID
						FROM 	
									MEDICAL_AWARD_SCHEME_STUDENT_TARGET s
						WHERE 
									s.SchemeID='" . $schemeID . "'
						AND 		s.GroupID IN ('" . implode("','", $groupID) . "') LIMIT 1";
                
                $rs = $this->objDB->returnResultSet($sql);
                
                if (count($rs)) {
                    $ret = true;
                }
            }
            return $ret;
        }

        // return next Monday by specific date (yyyy-mm-dd)
        public function getNextMonday($date)
        {
            $ts = mktime(0, 0, 0, (int) (substr($date, 5, 2)), (int) substr($date, 8, 2), substr($date, 0, 4));
            $dateInfo = getdate($ts);
            
            switch ($dateInfo['wday']) { // day of the week
                case 0:
                    $offset = 1;
                    break;
                case 1:
                    $offset = 0;
                    break;
                case 2:
                    $offset = 6;
                    break;
                case 3:
                    $offset = 5;
                    break;
                case 4:
                    $offset = 4;
                    break;
                case 5:
                    $offset = 3;
                    break;
                case 6:
                    $offset = 2;
                    break;
            }
            $Monday = date('Y-m-d', mktime(0, 0, 0, (int) (substr($date, 5, 2)), (int) substr($date, 8, 2) + $offset, substr($date, 0, 4)));
            return $Monday;
        }

        // return array of all Monday within the date range
        // pass in date format YYYY-MM-DD
        public function getSchemeDateAry($startDate, $endDate)
        {
            $ret = array();
            $Monday = $this->getNextMonday($startDate);
            if ($Monday >= $startDate && $Monday <= $endDate) {
                
                $limit = 261; // max 5 years
                $count = 1;
                while ($Monday <= $endDate && $count <= $limit) {
                    $ret[] = $Monday;
                    $Monday = date('Y-m-d', mktime(0, 0, 0, (int) (substr($Monday, 5, 2)), (int) substr($Monday, 8, 2) + 7, substr($Monday, 0, 4)));
                    $count ++;
                }
            }
            
            return $ret;
        }

        // check if pass in user is the staff member of the group
        public function isSchemeGroupTeacher($userID, $groupID)
        {
            $ret = false;
            $name_field = getNameFieldByLang('u.');
            if ($groupID) {
                $sql = "SELECT 		u.UserID, 
									{$name_field} AS StaffName
						FROM 
									INTRANET_USER u
						INNER JOIN
									INTRANET_USERGROUP ug ON ug.UserID=u.UserID AND ug.GroupID='" . $groupID . "'
						WHERE 
									u.UserID='" . $userID . "'
						AND			u.RecordType='1'
						AND			u.RecordStatus<>'3'";
                
                $rs = $this->objDB->returnResultSet($sql);
                if (count($rs)) {
                    $ret = true;
                }
            }
            return $ret;
        }

        // get list of student (UserID & StudentName) by GroupID
        public function getSchemeStudentByGroup($groupID)
        {
            $rs = '';
            $name_field = getNameFieldByLang('u.');
            if ($groupID) {
                $sql = "SELECT 		u.UserID, 
									{$name_field} AS StudentName
						FROM 
									INTRANET_USER u
						INNER JOIN
									INTRANET_USERGROUP ug ON ug.UserID=u.UserID AND ug.GroupID='" . $groupID . "'
						WHERE 
									u.RecordType='2'
						AND			u.RecordStatus<>'3'
						ORDER BY 	u.EnglishName";
                
                $rs = $this->objDB->returnResultSet($sql);
            }
            return $rs;
        }

        public function getSchemeStudentTarget($schemeID, $groupID = '', $studentID = array())
        {
            $rs = '';
            if ($schemeID) {
                $cond = "";
                if (! is_array($studentID)) {
                    $studentID = array(
                        $studentID
                    );
                }
                if (is_array($studentID) && count($studentID) > 0) {
                    $cond .= " AND t.StudentID IN ('" . implode("','", $studentID) . "')";
                }
                if ($groupID) {
                    $cond .= " AND t.GroupID='$groupID'";
                }
                $sql = "SELECT 		t.StudentID,
									t.StudentTargetID,
									t.Target,
									t.Performance,
									t.GroupID
						FROM 
									MEDICAL_AWARD_SCHEME_STUDENT_TARGET t
						WHERE 
									t.SchemeID='$schemeID'
									{$cond}
						ORDER BY 	t.StudentID, t.StudentTargetID";
                $rs = $this->objDB->returnResultSet($sql);
            }
            return $rs;
        }

        public function getSchemePerformanceList($schemeID, $groupID)
        {
            global $Lang, $LAYOUT_SKIN;
            
            $nextWeek = $this->getNextMonday(date('Y-m-d'));
            $lastMonth = date('Y-m-d', strtotime("-1 month"));
            $scheme = $this->getSchemes($schemeID);
            $x = '';
            $perf = array();
            if (count($scheme) == 1) {
                $rs_scheme = current($scheme);
                
                $schemeDate = $this->getSchemeDateAry($rs_scheme['StartDate'], $rs_scheme['EndDate']);
                $nrDate = count($schemeDate);
                
                $student = $this->getSchemeStudentByGroup($groupID);
                
                $x .= '<table width="100%" id="PerformanceTable" class="common_table_list">
						<tr valign="top" class="tabletop">
							<th width="1%">#</th>
							<th>' . $Lang['Identity']['Student'] . '</th>
							<th>' . $Lang['medical']['award']['Target'] . '</th>';
                $pastIdx = - 1;
                $futureIdx = - 1;
                for ($i = 0; $i < $nrDate; $i ++) {
                    if ($schemeDate[$i] >= $nextWeek) {
                        $colClass = ' non_recent_col';
                        $selector = 'non_recent';
                        if ($futureIdx == - 1) { // set only once
                            $futureIdx = $i;
                        }
                    } else if ($schemeDate[$i] < $lastMonth) {
                        $colClass = ' non_recent_col';
                        $selector = 'non_recent';
                        $pastIdx = $i;
                    } else {
                        $colClass = '';
                        $selector = 'recent';
                    }
                    $x .= '<th class="' . $selector . $colClass . '">' . (($schemeDate[$i] && $schemeDate[$i] != '0000-00-00') ? ltrim(substr($schemeDate[$i], 8, 2), '0') . '/' . ltrim(substr($schemeDate[$i], 5, 2), '0') : '-') . '</th>';
                }
                $x .= '<th width="1%">
								<a href="javascript:toggleColumn()" title="' . $Lang['medical']['award']['ShowAllDate'] . '"><img id="icon_more" src="/images/' . $LAYOUT_SKIN . '/icon_and_more_on.gif"></a>
						   </th>';
                $x .= '</tr>';
                
                $performanceExist = $this->checkPerformanceExistByGroupID($schemeID, $groupID);
                if (! $performanceExist) {
                    $templateTarget = $this->getSchemeTarget($schemeID, $targetID = '', $groupID);
                    $nrTarget = count($templateTarget);
                }
                
                if ($performanceExist) {
                    // for each student
                    for ($i = 0, $iMax = count($student); $i < $iMax; $i ++) {
                        $studentID = $student[$i]['UserID'];
                        $studentTarget = $this->getSchemeStudentTarget($schemeID, $groupID, $studentID);
                        $nrTarget = count($studentTarget);
                        
                        if ($nrTarget > 0) {
                            $x .= '<tr class="edit_' . $studentID . '" id="edit_' . $studentID . '_1">';
                            $x .= '<td rowspan="' . $nrTarget . '" class="student_seq">' . ($i + 1) . '</td>
									<td rowspan="' . $nrTarget . '">' . $student[$i]['StudentName'] . '<br><span class="table_row_tool"><a class="newBtn add" onclick="javascript:add_target(\'' . $studentID . '\',\'' . $nrDate . '\',\'' . $pastIdx . '\',\'' . $futureIdx . '\')" title="' . $Lang['Btn']['Add'] . '"></a></span></td>';
                            for ($j = 0; $j < $nrTarget; $j ++) {
                                unset($perf);
                                $studentTargetID = $studentTarget[$j]['StudentTargetID'];
                                $performance = explode("^~", $studentTarget[$j]['Performance']);
                                foreach ((array) $performance as $val) {
                                    list ($date, $score) = explode(",", $val);
                                    $perf[$date] = $score;
                                }
                                if ($j > 0) {
                                    $x .= '<tr class="edit_' . $studentID . '" id="edit_' . $studentID . '_' . ($j + 1) . '">';
                                }
                                $x .= '<td class="tabletext"><input type="text" name="Target[' . $studentTargetID . ']" id="Target[' . $studentTargetID . ']" class="textboxtext PerfTargetName" style="min-width:100px;" value="' . intranet_htmlspecialchars($studentTarget[$j]['Target']) . '">
										<span class="error_msg_hide" id="ErrTarget[' . $studentTargetID . ']">' . $Lang['medical']['award']['Warning']['InputSuggestion'] . '</span></td>';
                                for ($h = 0; $h < $nrDate; $h ++) {
                                    if ($schemeDate[$h] >= $nextWeek) {
                                        $colClass = ' non_recent_col';
                                        $selector = 'non_recent';
                                    } else if ($schemeDate[$h] < $lastMonth) {
                                        $colClass = ' non_recent_col';
                                        $selector = 'non_recent';
                                    } else {
                                        $colClass = '';
                                        $selector = 'recent';
                                    }
                                    $x .= '<td class="tabletext ' . $selector . $colClass . '"><input class="positiveint" type="text" size="4" maxlength="3" name="Score[' . $studentTargetID . '][' . ($h + 1) . ']" id="Score[' . $studentTargetID . '][' . ($h + 1) . ']" value="' . $perf[$schemeDate[$h]] . '">
										<span class="error_msg_hide" id="ErrScore[' . $studentTargetID . '][' . ($h + 1) . ']">' . $Lang['medical']['award']['Warning']['InputInteger'] . '</span></td>';
                                }
                                $x .= '<td width="1%"><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['award']['RemoveTarget'] . '" onClick="remove_target(\'' . $studentTargetID . '\')"></a></span></td>';
                                $x .= '</tr>';
                            }
                        }
                    }
                } else { // from template
                         // for each student
                    for ($i = 0, $iMax = count($student); $i < $iMax; $i ++) {
                        $studentID = $student[$i]['UserID'];
                        
                        $x .= '<tr class="new_' . $studentID . '" id="new_' . $studentID . '_1">';
                        $x .= '<td rowspan="' . $nrTarget . '" class="student_seq">' . ($i + 1) . '</td>
								<td rowspan="' . $nrTarget . '">' . $student[$i]['StudentName'] . '<br><span class="table_row_tool"><a class="newBtn add" onclick="javascript:add_target(\'' . $studentID . '\',\'' . $nrDate . '\',\'' . $pastIdx . '\',\'' . $futureIdx . '\')" title="' . $Lang['Btn']['Add'] . '"></a></span></td>';
                        for ($j = 0; $j < $nrTarget; $j ++) {
                            if ($j > 0) {
                                $x .= '<tr class="new_' . $studentID . '" id="new_' . $studentID . '_' . ($j + 1) . '">';
                            }
                            $x .= '<td class="tabletext"><input type="text" name="NewTarget[' . $studentID . '][' . ($j + 1) . ']" id="NewTarget[' . $studentID . '][' . ($j + 1) . ']" class="textboxtext PerfTargetName" style="min-width:100px;" value="' . intranet_htmlspecialchars($templateTarget[$j]['Target']) . '">
									<span class="error_msg_hide" id="ErrTarget[' . $studentID . '][' . ($j + 1) . ']">' . $Lang['medical']['award']['Warning']['InputSuggestion'] . '</span></td>';
                            for ($h = 0; $h < $nrDate; $h ++) {
                                if ($schemeDate[$h] >= $nextWeek) {
                                    $colClass = ' non_recent_col';
                                    $selector = 'non_recent';
                                } else if ($schemeDate[$h] < $lastMonth) {
                                    $colClass = ' non_recent_col';
                                    $selector = 'non_recent';
                                } else {
                                    $colClass = '';
                                    $selector = 'recent';
                                }
                                $x .= '<td class="tabletext ' . $selector . $colClass . '"><input class="positiveint" type="text" size="4" maxlength="3" name="NewScore[' . $studentID . '][' . ($j + 1) . '][' . ($h + 1) . ']" id="NewScore[' . $studentID . '][' . ($j + 1) . '][' . ($h + 1) . ']" value="">
									<span class="error_msg_hide" id="ErrScore[' . $studentID . '][' . ($j + 1) . '][' . ($h + 1) . ']">' . $Lang['medical']['award']['Warning']['InputInteger'] . '</span></td>';
                            }
                            $x .= '<td width="1%"><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['award']['RemoveTarget'] . '" onClick="remove_new_target(\'' . $studentID . '\',\'' . ($j + 1) . '\')"></a></span></td>';
                            $x .= '</tr>';
                        }
                    }
                }
                
                if (count($student) == 0) {
                    $x .= '<tr><td colspan=' . ($nrDate + 4) . ' style="text-align:center">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>';
                }
                $x .= '</table>';
            }
            return $x;
        }

        public function checkPerformanceExistByDate($schemeID, $date)
        {
            $ret = false; // default not exist
            $sql = "SELECT  
								s.StudentTargetID
					FROM 	
								MEDICAL_AWARD_SCHEME_STUDENT_TARGET s
					WHERE 
								s.SchemeID='" . $schemeID . "'
					AND 		s.Performance LIKE '%" . $date . "%'";
            
            $rs = $this->objDB->returnResultSet($sql);
            $nrRec = count($rs);
            if ($nrRec) {
                for ($i = 0; $i < $nrRec; $i ++) {
                    $performance = explode("^~", $rs[$i]['Performance']);
                    foreach ((array) $performance as $val) {
                        list ($date, $score) = explode(",", $val);
                        if ($score !== '') {
                            return true;
                        }
                    }
                }
            }
            return $ret;
        }

        public function getPerformanceByDate($schemeID, $date = array())
        {
            if (is_array($date)) {
                $cond .= " AND (";
                $i = 0;
                foreach ((array) $date as $d) {
                    $cond .= $i > 0 ? " OR" : "";
                    $cond .= " Performance LIKE '%" . $d . "%'";
                    $i ++;
                }
                $cond .= ")";
            }
            $sql = "SELECT  
								StudentTargetID,
								Performance
					FROM 	
								MEDICAL_AWARD_SCHEME_STUDENT_TARGET 
					WHERE 
								SchemeID='" . $schemeID . "'" . $cond;
            
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function checkSchemeDateRangeChange($schemeID, $StartDate, $EndDate)
        {
            $fillBack = false;
            $deleteRec = false;
            
            $scheme = $this->getSchemes($schemeID);
            if (count($scheme) == 1) {
                $rs_scheme = current($scheme);
                
                $oldShemeDate = $this->getSchemeDateAry($rs_scheme['StartDate'], $rs_scheme['EndDate']);
                $newShemeDate = $this->getSchemeDateAry($StartDate, $EndDate);
                $date_to_add = array_diff($newShemeDate, $oldShemeDate);
                $date_to_delete = array_diff($oldShemeDate, $newShemeDate);
                
                if (count($date_to_add)) {
                    foreach ((array) $date_to_add as $date) {
                        
                        if ($date < $rs_scheme['StartDate']) {
                            $fillBack = true;
                            break;
                        }
                    }
                }
                
                if (count($date_to_delete)) {
                    foreach ((array) $date_to_delete as $date) {
                        $deleteRec = $this->checkPerformanceExistByDate($schemeID, $date);
                        break;
                    }
                }
            }
            
            return array(
                $fillBack,
                $deleteRec
            );
        }

        public function getAttachmentByID($table, $fileID)
        {
            $sql = "SELECT  *
					FROM 	
							{$table}
					WHERE 
						FileID='" . $fileID . "'";
            
            $rs = $this->objDB->returnResultSet($sql);
            
            return $rs;
        }

        public function deleteAttachmentByID($table, $fileID)
        {
            $sql = "DELETE FROM {$table} WHERE FileID='" . $fileID . "'";
            $result = $this->objDB->db_db_query($sql);
            
            return $result;
        }

        public function getAttachFile($table, $row, $script = false, $mode = 'edit')
        {
            global $Lang, $file_path, $PATH_WRT_ROOT;
            
            $medical_path = "$file_path/file/medical/edisc/";
            $fileSize = convert_size($row['SizeInBytes']);
            
            $x = '<span id="' . $mode . '_row_' . $row['FileID'] . '">';
            $x .= '<a class="tabletool" href="/home/download_attachment.php?target_e=' . getEncryptedText($medical_path . $row['FolderPath'] . '/' . $row['FileHashName']) . '&filename_e=' . getEncryptedText($row['FileName']) . '">';
            $x .= (($script == true) ? str_replace("'", "\'", $row['FileName']) : $row['FileName']) . ' (' . $fileSize . ')';
            $x .= '</a>';
            if ($table == 'minute') {
                $extraParaScript = ",\\\'" . $row['MinuteID'] . "\\\'";
                $extraPara = ",'" . $row['MinuteID'] . "'";
            } else {
                $extraParaScript = '';
                $extraPara = '';
            }
            if ($mode == 'edit') {
                if ($script == true) {
                    $x .= ' [<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\\\'' . $table . '\\\', \\\'' . $row['FileID'] . '\\\'' . $extraParaScript . ')">' . $Lang['Btn']['Delete'] . '</a>]';
                } else {
                    $x .= ' [<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\'' . $table . '\', \'' . $row['FileID'] . '\'' . $extraPara . ')">' . $Lang['Btn']['Delete'] . '</a>]';
                }
            }
            $x .= '</span>';
            return $x;
        }

        public function getAttachFileList($table, $attachList, $script = false, $mode = 'edit')
        {
            $x = '';
            for ($i = 0, $iMax = count($attachList); $i < $iMax; $i ++) {
                $j = $i+1;
                $row = $attachList[$i];
                $x .= $j . '. '. $this->getAttachFile($table, $row, $script, $mode);
                if ($i < $iMax - 1) {
                    $x .= '<br>';
                }
            }
            return $x;
        }

        public function getCurrentAttachmentLayout($table, $keyID, $mode = 'edit')
        {
            $x = '<div id="current_attachment_' . $table . '_' . $mode . '" ' . ($keyID ? '' : 'style="display:none"') . '>';
            if ($keyID) {
                switch ($table) {
                    case 'event':
                        $rs = $this->getEventAttachment($keyID);
                        break;
                    case 'event_followup':
                        $rs = $this->getEventFollowupAttachment($keyID);
                        break;
                    case 'minute':
                        $rs = $this->getCaseMinuteAttachment($keyID);
                        break;
                    case 'staff_event':
                        $rs = $this->getStaffEventAttachment($keyID);
                        break;
                }
                if (count($rs) == 0) {
                    return '';
                }
                $x .= $this->getAttachFileList($table, $rs, false, $mode);
            }
            $x .= '</div>';
            
            return $x;
        }

        /*
         * $mode - view / edit
         * $plupload = array( $use_plupload,
         * $pluploadContainerId,
         * $pluploadButtonId,
         * $pluploadDropTargetId,
         * $pluploadFileListDivId)
         */
        public function getAttachmentLayout($table, $keyID, $mode = 'edit', $plupload = array())
        {
            global $Lang, $linterface;
            $a = $this->getCurrentAttachmentLayout($table, $keyID, $mode);
            
            if (! empty($plupload)) {
                if ($plupload['use_plupload']) {
                    $uploadLayout = '<div id="' . $plupload['pluploadContainerId'] . '"></div>' . $linterface->GET_SMALL_BTN($Lang['medical']['event']['SelectFile'], "button", "", $plupload['pluploadButtonId'], $ParOtherAttribute = "", $OtherClass = "", $ParTitle = '') . '<div id="' . $plupload['pluploadDropTargetId'] . '" style="display:none;" class="DropFileArea">' . $Lang['medical']['upload']['OrDragAndDropFilesHere'] . '</div>
										 <div id="' . $plupload['pluploadFileListDivId'] . '"></div>' . $linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv_' . $table, '', 'FileWarnDiv');
                } else {
                    $uploadLayout = '<input type="file" name="File" id="File" class="textbox"/>';
                }
                $uploadLayout .= '<span id="div_err_File"></span>';
            } else {
                $uploadLayout = '';
            }
            
            $x = '<tr>';
            $x .= '<td valign="top" nowrap="nowrap" class="field_title" width="20%" >' . $Lang['medical']['event']['Attachment'] . '</td>';
            $x .= '<td class="tabletext">';
            $x .= $mode == 'edit' ? $uploadLayout . $a : $a;
            $x .= '</td>';
            $x .= '</tr>';
            return $x;
        }

        // $followup is 1D assoc array
        public function getEventFollowupView($followup = array(), $viewMode = '')
        {
            global $Lang;
            
            if (empty($followup)) {
                return '';
            }
            $followupInfo = sprintf($Lang['medical']['event']['FollowupActionInfo'], $this->getUserNameByID($followup['LastModifiedBy']), $followup['DateModified']);
            if (! $viewMode) {
                $isRequiredCheckSelfEvent = $this->isRequiredCheckSelfEvent();
                $action_tool = '<a href="#TB_inline?height=500&width=800&inlineId=FakeLayer" onClick="edit_followup(' . $followup['FollowupID'] . ')" class="tool_edit">' . $Lang['Btn']['Edit'] . '</a>';
                if ($isRequiredCheckSelfEvent && $followup['InputBy'] != $_SESSION['UserID']) {
                    // do nothing
                } else {
                    $action_tool .= '&nbsp;&nbsp;<a href="#" onClick="delete_followup(' . $followup['FollowupID'] . ')" class="tool_delete">' . $Lang['Btn']['Delete'] . '</a>';
                }
            } else {
                $action_tool = '';
            }
            
            $x = '<div id="ef_' . $followup['FollowupID'] . '">';
            $x .= '<table width="100%" class="common_table_list">';
            $x .= '<tr class="followup_list_header">';
            $x .= '<th colspan="2"><div style="width:69%;float:left;">' . $followupInfo . '</div>';
            $x .= '<div style="width:30%;float:left;text-align:right">' . $action_tool . '</div></th>';
            $x .= '</tr>';
            
            if (! empty($followup['Handling'])) {
                $x .= '<tr>
								<td width="15%">' . $Lang['medical']['event']['Handling'] . ':</td>
								<td>' . nl2br($followup['Handling']) . '</td>
							</tr>';
            }
            
            if (! empty($followup['StudentResponse'])) {
                $x .= '<tr>
								<td width="15%">' . $Lang['medical']['event']['StudentResponse'] . ':</td>
								<td>' . nl2br($followup['StudentResponse']) . '</td>
							</tr>';
            }
            
            if (! empty($followup['FollowupAction'])) {
                $x .= '<tr>
								<td width="15%">' . $Lang['medical']['event']['FollowupAction'] . ':</td>
								<td>' . nl2br($followup['FollowupAction']) . '</td>
							</tr>';
            }
            
            if (! empty($followup['Other'])) {
                $x .= '<tr>
								<td width="15%">' . $Lang['medical']['event']['Other'] . ':</td>
								<td>' . nl2br($followup['Other']) . '</td>
							</tr>';
            }
            
            $attachment = $this->getCurrentAttachmentLayout($table = 'event_followup', $followup['FollowupID'], $mode = 'view');
            if (! empty($attachment)) {
                $x .= '<tr>
								<td width="15%">' . $Lang['medical']['event']['Attachment'] . ':</td>
								<td>' . $attachment . '</td>
							</tr>';
            }
            
            $x .= '</table>';
            $x .= '</div>';
            return $x;
        }

        public function getEventFollowupList($eventID, $viewMode = '')
        {
            $followup = $this->getEventFollowup($eventID);
            $x = '';
            for ($i = 0, $iMax = count($followup); $i < $iMax; $i ++) {
                $x .= $this->getEventFollowupView($followup[$i], $viewMode);
            }
            return $x;
        }

        // three columns: selected students, buttons, student to be selected
        public function getStudentSelection($selectedStudent = array())
        {
            global $PATH_WRT_ROOT, $Lang, $linterface, $junior_mck;
            
            include_once ($PATH_WRT_ROOT . "includes/libclass.php");
            
            $lclass = new libclass();
            if ($junior_mck) {
                $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ", $selected = "", $optionSelect = "", $optionFirst = "", $AcademicYearID = '', $OptionNoClassStudent = false, $convert2unicode = true);
            } else {
                $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
            }
            
            $groupSelection = $this->getGroupSelection("GroupID", "", "style=\"display:none\"", "-- " . $Lang['Btn']['Select'] . " --");
            
            $nrStudent = count($selectedStudent);
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=StudentID[] id=StudentID style="min-width:200px; height:171px;" multiple>';
            $StudentID = array();
            for ($i = 0; $i < $nrStudent; $i ++) {
                $studentID = $selectedStudent[$i]['UserID'];
                $StudentID[] = $studentID;
                $studentName = $selectedStudent[$i]['Name'];
                $x .= '<option value="' . $studentID . '">' . $studentName . '</option>';
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $linterface->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableStudentID[]'],this.form.elements['StudentID[]']);return false;", "submit11") . "<br /><br />";
            $x .= $linterface->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['StudentID[]'],this.form.elements['AvailableStudentID[]']);return false;", "submit12");
            $x .= '</td>';
            
            $x .= '<td>';
            $x .= '<span>
                        <select name="ByMethod" id="ByMethod">
			               <option value="ByClass">' . $Lang['Header']['Menu']['Class'] . '</option>
					       <option value="ByGroup">' . $Lang['medical']['meal']['searchMenu']['group'] . '</option>
				        </select>
                    </span>';
            $x .= '<span id="ByMethodSpan">' . $classSelection . $groupSelection . '</span> &nbsp;<br>';
            $x .= '<span id="StudentNameSpan"><select name=AvailableStudentID[] id=AvailableStudentID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        // three columns: selected person, buttons, student to be selected
        public function getAffectedPersonSelection($selectedAffectedPerson = array())
        {
            global $PATH_WRT_ROOT, $Lang, $linterface, $junior_mck;
            
            include_once ($PATH_WRT_ROOT . "includes/libclass.php");
            
            $lclass = new libclass();
            if ($junior_mck) {
                $classSelection = $lclass->getSelectClass("name='AffectedPersonClassName' id='AffectedPersonClassName' ", $selected = "", $optionSelect = "", $optionFirst = "", $AcademicYearID = '', $OptionNoClassStudent = false, $convert2unicode = true);
                // $staffLabel = convert2unicode($Lang['Identity']['Staff'], 1);
                $teachingStaffLabel = convert2unicode($Lang['Identity']['TeachingStaff'], 1);
                $nonTeachingStaffLabel = convert2unicode($Lang['Identity']['NonTeachingStaff'], 1);
            } else {
                $classSelection = $lclass->getSelectClass("name='AffectedPersonClassName' id='AffectedPersonClassName' ");
                // $staffLabel = $Lang['Identity']['Staff'];
                $teachingStaffLabel = $Lang['Identity']['TeachingStaff'];
                $nonTeachingStaffLabel = $Lang['Identity']['NonTeachingStaff'];
            }
            // $classSelection = str_replace("</select>", "", $classSelection);
            // $classSelection .= '<optgroup label="' . $staffLabel . '">';
            // $classSelection .= '<option value="TeachingStaff">' . $teachingStaffLabel . '</option>';
            // $classSelection .= '<option value="NonTeachingStaff">' . $nonTeachingStaffLabel . '</option>';
            // $classSelection .= '</optgroup>';
            // $classSelection .= '</select>';
            
            $groupSelection = $this->getGroupSelection("AffectedPersonGroupID", "", "style=\"display:none\"", "-- " . $Lang['Btn']['Select'] . " --");
            
            $nrAffectedPerson = count($selectedAffectedPerson);
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=AffectedPersonID[] id=AffectedPersonID style="min-width:200px; height:171px;" multiple>';
            $AffectedPersonID = array();
            for ($i = 0; $i < $nrAffectedPerson; $i ++) {
                $studentID = $selectedAffectedPerson[$i]['UserID'];
                $AffectedPersonID[] = $studentID;
                $studentName = $selectedAffectedPerson[$i]['Name'];
                $x .= '<option value="' . $studentID . '">' . $studentName . '</option>';
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $linterface->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableAffectedPersonID[]'],this.form.elements['AffectedPersonID[]']);return false;", "submit13") . "<br /><br />";
            $x .= $linterface->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['AffectedPersonID[]'],this.form.elements['AvailableAffectedPersonID[]']);return false;", "submit14");
            $x .= '</td>';
            
            $x .= '<td>';
            
            $x .= '<span>
                        <select name="AffectedPersonByMethod" id="AffectedPersonByMethod">
			               <option value="ByClass">' . $Lang['Header']['Menu']['Class'] . '</option>
					       <option value="ByGroup">' . $Lang['medical']['meal']['searchMenu']['group'] . '</option>
                           <option value="TeachingStaff">' . $teachingStaffLabel . '</option>
                           <option value="NonTeachingStaff">' . $nonTeachingStaffLabel . '</option>
				        </select>
                    </span>';
            $x .= '<span id="AffectedPersonByMethodSpan">' . $classSelection . $groupSelection . '</span> &nbsp;<br>';
            
            // $x .= $classSelection . '<br>';
            $x .= '<span id="AffectedPersonNameSpan"><select name=AvailableAffectedPersonID[] id=AvailableAffectedPersonID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        // $teacherType = -1 denotes all teachers
        // get approved (RecordStatus=1) teacher only
        public function getTeacher($teacherType = "-1", $conds = "")
        {
            global $junior_mck;
            
            if ($teacherType == 1)
                $conds .= " AND Teaching=1";
            else if ($teacherType == 0)
                $conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
            
            $list = array();
            $name_field = getNameFieldByLang();
            $sql = "SELECT UserID, $name_field as Name FROM INTRANET_USER WHERE RecordType=1 AND RecordStatus='1' $conds ORDER BY EnglishName";
            
            $rs = $this->objDB->returnArray($sql);
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i][1] = convert2unicode($rs[$i][1], 1);
                    $rs[$i]['Name'] = convert2unicode($rs[$i]['Name'], 1);
                }
            }
            
            return $rs;
        }

        public function getTeacherTypeSelection($selectedType = '')
        {
            global $Lang;
            
            $x = '<select name=TeacherType id=TeacherType>';
            $x .= '<option value="" ' . (($selectedType == '') ? 'SELECTED' : '') . '>-- ' . $Lang['Btn']['Select'] . ' --</option>';
            $x .= '<option value="1" ' . (($selectedType == 1) ? 'SELECTED' : '') . '>' . $Lang['Identity']['TeachingStaff'] . '</option>';
            $x .= '<option value="0" ' . (($selectedType === 0) ? 'SELECTED' : '') . '>' . $Lang['Identity']['NonTeachingStaff'] . '</option>';
            $x .= '</select>';
            
            return $x;
        }

        // three columns: selected teachers, buttons, teacher to be selected
        public function getTeacherSelection($selectedTeacher = array())
        {
            global $Lang, $linterface;
            
            $nrTeacher = count($selectedTeacher);
            $selectedTeacherID = array();
            $teacherTypeSelection = $this->getTeacherTypeSelection();
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=TeacherID[] id=TeacherID style="min-width:200px; height:171px;" multiple>';
            for ($i = 0; $i < $nrTeacher; $i ++) {
                $teacherID = $selectedTeacher[$i]['UserID'];
                $teacherName = $selectedTeacher[$i]['Name'];
                $x .= '<option value="' . $teacherID . '">' . $teacherName . '</option>';
                $selectedTeacherID[] = $teacherID;
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $linterface->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableTeacherID[]'],this.form.elements['TeacherID[]']);return false;", "submit11") . "<br /><br />";
            $x .= $linterface->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['TeacherID[]'],this.form.elements['AvailableTeacherID[]']);return false;", "submit12");
            $x .= '</td>';
            
            $x .= '<td>';
            $x .= $teacherTypeSelection . '<br>';
            $x .= '<span id="TeacherSelectionSpan"><select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        public function getAvailableGroupByCategory($category = '', $excludedGroup = '')
        {
            $name_field = Get_Lang_Selection('g.TitleChinese', 'g.Title');
            $currentAcademicYearID = Get_Current_Academic_Year_ID();
            
            $cond = '';
            if ($category) {
                $cond .= " AND g.RecordType='" . $category . "'";
            }
            if ($excludedGroup) {
                $cond .= " AND g.GroupID NOT IN ('" . implode("','", (array) $excludedGroup) . "')";
            }
            $sql = "SELECT 
								g.GroupID,
								$name_field as GroupTitle
					FROM 
								INTRANET_GROUP g
					LEFT JOIN	
								INTRANET_GROUP_CATEGORY c ON c.GroupCategoryID=g.RecordType
					WHERE 
								(g.AcademicYearID='" . $currentAcademicYearID . "' or g.AcademicYearID is NULL)
 								$cond 
					ORDER BY c.GroupCategoryID, $name_field";
            
            $rs = $this->objDB->returnArray($sql);
            
            return $rs;
        }

        // three columns: selected groups, buttons, group to be selected
        public function getSchemeGroupSelection($selectedGroup = array())
        {
            global $Lang, $linterface, $PATH_WRT_ROOT;
            
            include_once ($PATH_WRT_ROOT . "includes/libgroupcategory.php");
            
            $nrGroup = count($selectedGroup);
            $selectedGroupID = array();
            
            $lc = new libgroupcategory();
            $groupCategory = $lc->returnAllCat();
            $groupCategorySelection = getSelectByArray($groupCategory, "name='GroupCategory' id='GroupCategory'");
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=GroupID[] id=GroupID style="min-width:200px; height:171px;" multiple>';
            if (is_array($selectedGroup)) {
                for ($i = 0; $i < $nrGroup; $i ++) {
                    $groupID = $selectedGroup[$i]['GroupID'];
                    $groupTitle = $selectedGroup[$i]['GroupTitle'];
                    $x .= '<option value="' . $groupID . '">' . $groupTitle . '</option>';
                    $selectedGroupID[] = $groupID;
                }
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $linterface->GET_BTN("<< " . $Lang['Btn']['Add'], "button", "checkOptionTransfer(this.form.elements['AvailableGroupID[]'],this.form.elements['GroupID[]']); checkTargetGroup(); return false;", "submit15") . "<br /><br />";
            $x .= $linterface->GET_BTN($Lang['Btn']['Delete'] . " >>", "button", "removeTargetGroup(); return false;", "submit16");
            $x .= '</td>';
            
            $x .= '<td>';
            $x .= $groupCategorySelection . '<br>';
            $x .= '<span id="GroupSelectionSpan"><select name=AvailableGroupID[] id=AvailableGroupID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        public function getEventType($eventTypeID = '')
        {
            $sql = "SELECT * FROM MEDICAL_EVENT_TYPE WHERE RecordStatus=1 AND DeletedFlag=0";
            if ($eventTypeID) {
                $sql .= " AND EventTypeID='" . $eventTypeID . "'";
            }
            $sql .= " ORDER BY EventTypeName";
            $rs = $this->objDB->returnArray($sql);
            return $rs;
        }

        function getInventoryBuildingArray($buildingID = '')
        {
            global $junior_mck;
            $sql = "SELECT BuildingID, " . Get_Lang_Selection("NameChi", "NameEng") . " as BuildingName FROM INVENTORY_LOCATION_BUILDING WHERE RecordStatus=1";
            if ($buildingID) {
                $sql .= " AND BuildingID='" . $buildingID . "'";
            }
            $sql .= " ORDER BY DisplayOrder";
            
            $rs = $this->objDB->returnArray($sql);
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i][1] = convert2unicode($rs[$i][1], 1);
                    $rs[$i]['BuildingName'] = convert2unicode($rs[$i]['BuildingName'], 1);
                }
            }
            return $rs;
        }

        function getInventoryLevelArray($BuildingID = '', $LocationLevelID = '')
        {
            global $junior_mck;
            
            $sql = "SELECT 		
								lvl.LocationLevelID, " . Get_Lang_Selection("lvl.NameChi", "lvl.NameEng") . " as LevelName, 
								lvl.BuildingID 
					FROM 
								INVENTORY_LOCATION_LEVEL lvl 
					INNER JOIN 
								INVENTORY_LOCATION_BUILDING b ON (lvl.BuildingID=b.BuildingID AND lvl.RecordStatus=1) 
					WHERE 
								lvl.RecordStatus=1";
            
            if ($BuildingID != "") {
                $sql .= " AND lvl.BuildingID='$BuildingID'";
            }
            if ($LocationLevelID) {
                $sql .= " AND lvl.LocationLevelID='$LocationLevelID'";
            }
            
            $sql .= " ORDER BY b.DisplayOrder, lvl.DisplayOrder";
            
            $rs = $this->objDB->returnArray($sql);
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i][1] = convert2unicode($rs[$i][1], 1);
                    $rs[$i]['LevelName'] = convert2unicode($rs[$i]['LevelName'], 1);
                }
            }
            return $rs;
        }

        function getInventoryLocationArray($LocationLevelID = '', $LocationID = '')
        {
            global $junior_mck;
            
            $sql = "SELECT 
								loc.LocationID, " . Get_Lang_Selection("loc.NameChi", "loc.NameEng") . " as LocationName, 
								loc.LocationLevelID 
					FROM 
								INVENTORY_LOCATION loc 
					INNER JOIN 
								INVENTORY_LOCATION_LEVEL lvl ON (lvl.LocationLevelID=loc.LocationLevelID) 
					INNER JOIN 
								INVENTORY_LOCATION_BUILDING b ON (b.BuildingID=lvl.BuildingID) 
					WHERE 
								loc.RecordStatus=1 
					AND 
								lvl.RecordStatus=1 
					AND 
								b.RecordStatus=1";
            
            if ($LocationLevelID != "") {
                $sql .= " AND loc.LocationLevelID='$LocationLevelID'";
            }
            
            if ($LocationID != "") {
                $sql .= " AND loc.LocationID='$LocationID'";
            }
            
            $sql .= " ORDER BY b.DisplayOrder, lvl.DisplayOrder, loc.DisplayOrder";
            
            $rs = $this->objDB->returnArray($sql);
            if ($junior_mck) {
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i][1] = convert2unicode($rs[$i][1], 1);
                    $rs[$i]['LocationName'] = convert2unicode($rs[$i]['LocationName'], 1);
                }
            }
            return $rs;
        }

        // for IP, compare against ClassTitleEN, exclude left student
        public function getStudentNameListWClassNumberByClassName($className, $includeUserIdAry = '', $excludeUserIdAry = '', $assoc = false)
        {
            global $junior_mck;
            
            $conds_userId = '';
            $recordstatus = "'0','1','2'";
            
            if ($includeUserIdAry != '') {
                $conds_userId .= " AND u.UserID IN ('" . implode("','", (array) $includeUserIdAry) . "') ";
            }
            
            if ($excludeUserIdAry != '') {
                $conds_userId .= " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            
            if ($junior_mck) {
                $name_field = getNameFieldWithClassNumberByLang('u.');
                $sql = "SELECT 
								u.UserID, 
								$name_field as StudentName  
						FROM 
								INTRANET_USER u 
						WHERE 
								u.RecordType = 2 
						AND 
								u.RecordStatus IN ($recordstatus) 
						AND 
								u.ClassName = '" . $this->objDB->Get_Safe_Sql_Query($className) . "' 
								$conds_userId 
						ORDER BY u.ClassNumber+0";
                $rs = $assoc ? $this->objDB->returnResultSet($sql) : $this->objDB->returnArray($sql); // need returnArray for getSelectByArray function
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['StudentName'] = convert2unicode($rs[$i]['StudentName'], 1);
                    if (! $assoc) {
                        $rs[$i][1] = $rs[$i]['StudentName'];
                    }
                }
            } else {
                $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
                $username_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
                $classNameField = 'ClassTitleEN'; // use English ClassName
                $student_name = "CONCAT($username_field,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";
                
                $sql = "SELECT 	u.UserID, 
								{$student_name} AS StudentName							 
						FROM 
								INTRANET_USER u 
						INNER JOIN 
								YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN 
								YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID 
							AND yc.AcademicYearID='" . $CurrentAcademicYearID . "' 
							AND yc.ClassTitleEN='" . $this->objDB->Get_Safe_Sql_Query($className) . "' 
						WHERE 
								u.RecordType=2
							AND u.RecordStatus IN ($recordstatus) 
							{$conds_userId}
						ORDER BY ycu.ClassNumber+0";
                $rs = $assoc ? $this->objDB->returnResultSet($sql) : $this->objDB->returnArray($sql); // need returnArray for getSelectByArray function
            }
            
            return $rs;
        }

        public function getStudentNameListWClassNumberByGroupID($groupID, $includeUserIdAry = '', $excludeUserIdAry = '', $assoc = false)
        {
            global $junior_mck;
            
            $conds_userId = '';
            $recordstatus = "'0','1','2'";
            
            if ($includeUserIdAry != '') {
                $conds_userId .= " AND u.UserID IN ('" . implode("','", (array) $includeUserIdAry) . "') ";
            }
            
            if ($excludeUserIdAry != '') {
                $conds_userId .= " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            
            if ($junior_mck) {
                $name_field = getNameFieldWithClassNumberByLang('u.');
                $sql = "SELECT 
								u.UserID, 
								$name_field as StudentName  
						FROM 
								INTRANET_USER u
						INNER JOIN 
								INTRANET_USERGROUP g ON g.UserID=u.UserID
						WHERE 
								u.RecordType = 2 
						AND 
								u.RecordStatus IN ($recordstatus) 
						AND 
								g.GroupID = '" . $groupID . "' 
								$conds_userId 
						ORDER BY u.ClassNumber+0";
                $rs = $assoc ? $this->objDB->returnResultSet($sql) : $this->objDB->returnArray($sql); // need returnArray for getSelectByArray function
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['StudentName'] = convert2unicode($rs[$i]['StudentName'], 1);
                    if (! $assoc) {
                        $rs[$i][1] = $rs[$i]['StudentName'];
                    }
                }
            } else {
                $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
                $username_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
                $classNameField = 'ClassTitleEN'; // use English ClassName
                $student_name = "CONCAT($username_field,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";
                
                $sql = "SELECT 	u.UserID, 
								{$student_name} AS StudentName							 
						FROM 
								INTRANET_USER u
						INNER JOIN 
								INTRANET_USERGROUP g ON g.UserID=u.UserID
						INNER JOIN 
								YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN 
								YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID 
							AND yc.AcademicYearID='" . $CurrentAcademicYearID . "' 
						WHERE 
								u.RecordType=2
							AND u.RecordStatus IN ($recordstatus)
					 		AND g.GroupID = '" . $groupID . "'
							{$conds_userId}
						ORDER BY ycu.ClassNumber+0";
                $rs = $assoc ? $this->objDB->returnResultSet($sql) : $this->objDB->returnArray($sql); // need returnArray for getSelectByArray function
            }
            
            return $rs;
        }

        public function getStudentNameByClassWithFilter($className, $excludeUserIdAry = array())
        {
            global $junior_mck;
            
            if (count($excludeUserIdAry) > 0) {
                $conds_userId = " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            if ($junior_mck) {
                $name_field = getNameFieldWithClassNumberByLang();
                $sql = "SELECT 	u.UserID, 
								$name_field AS StudentName 
						FROM 
								INTRANET_USER u 
						WHERE 
								RecordType=2 
							AND RecordStatus IN ('0','1','2') 
							AND ClassName = '" . $this->objDB->Get_Safe_Sql_Query($className) . "' 
							$conds_userId 
						ORDER BY ClassNumber"; // exclude left student
                
                $rs = $this->objDB->returnResultSet($sql);
                
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['StudentName'] = convert2unicode($rs[$i]['StudentName'], 1);
                }
            } else {
                $rs = $this->getStudentNameListWClassNumberByClassName($className, '', $excludeUserIdAry, true);
            }
            
            return $rs;
        }

        /*
         * $data = array([0]=>array( ['UserID'] => 1,
         * ['Name'] => 'demo user 1')
         * [1]=>array( ['UserID'] => 2,
         * ['Name'] => 'demo user 2')...)
         * return user name with line break
         */
        public function getUserNameList($data)
        {
            $user_list = array();
            for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
                $user_list[] = $data[$i]['Name'];
            }
            return implode("<br>", (array) $user_list);
        }

        public function getEventRowForCase($eventID)
        {
            global $Lang;
            $eventTypeAry = $this->getEventType();
            $eventTypeAry = BuildMultiKeyAssoc($eventTypeAry, 'EventTypeID', $IncludedDBField = array(
                'EventTypeName'
            ), $SingleValue = 1);
            
            $event = $this->getEvents($eventID);
            
            $x = '';
            for ($i = 0, $iMax = count($event); $i < $iMax; $i ++) {
                $rs = $event[$i];
                
                $x .= '<tr id="event_row_' . $rs['EventID'] . '">
							<td style="white-space:nowrap;"><a href="javascript:view_event(\'' . $rs['EventID'] . '\')">' . $rs['EventDate'] . '</a></td>
							<td>' . $rs['StartTime'] . '</td>
							<td>' . stripslashes($eventTypeAry[$rs['EventTypeID']]) . '</td>
							<td>' . nl2br($rs['Summary']) . '</td>
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['case']['RemoveEvent'] . '" onClick="removeEvent(' . $rs['EventID'] . ')"></a></span>
								<input type="hidden" name="SelEventID[]" value="' . $rs['EventID'] . '"></td>
						</tr>';
            }
            return $x;
        }

        public function getEventRowForStaffEvent($eventID)
        {
            global $Lang, $junior_mck;
            $eventTypeAry = $this->getEventType();
            $eventTypeAry = BuildMultiKeyAssoc($eventTypeAry, 'EventTypeID', $IncludedDBField = array(
                'EventTypeName'
            ), $SingleValue = 1);

            $event = $this->getEvents($eventID);
            
            $x = '';
            for ($i = 0, $iMax = count($event); $i < $iMax; $i ++) {
                $rs = $event[$i];
                $studentAry = $this->getEventStudent($rs['EventID']);
                $studentAry = BuildMultiKeyAssoc($studentAry, 'UserID', array('Name'), $SingleValue = 1);
                $studentList = implode(", ", $studentAry);
                
                $x .= '<tr id="event_row_' . $rs['EventID'] . '">
							<td style="white-space:nowrap;"><a href="javascript:view_event(\'' . $rs['EventID'] . '\')">' . $rs['EventDate'] . '</a></td>
							<td>' . $rs['StartTime'] . '</td>
                            <td>' . $studentList . '</td>
							<td>' . stripslashes($eventTypeAry[$rs['EventTypeID']]) . '</td>
							<td>' . nl2br($rs['Summary']) . '</td>
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['case']['RemoveEvent'] . '" onClick="removeEvent(' . $rs['EventID'] . ')"></a></span>
								<input type="hidden" name="SelEventID[]" value="' . $rs['EventID'] . '"></td>
						</tr>';
            }
            return $x;
        }
        
        public function getEventRowForStudentLog($eventID, $formId)
        {
            global $Lang;
            $eventTypeAry = $this->getEventType();
            $eventTypeAry = BuildMultiKeyAssoc($eventTypeAry, 'EventTypeID', $IncludedDBField = array(
                'EventTypeName'
            ), $SingleValue = 1);
            
            $event = $this->getEvents($eventID);
            
            $x = '';
            for ($i = 0, $iMax = count($event); $i < $iMax; $i ++) {
                $rs = $event[$i];
                
                $x .= '<tr class="eventRow' . $rs['EventID'] . '">
							<td style="white-space:nowrap;"><a href="#" class="viewEvent" data-EventID="' . $rs['EventID'] . '">' . $rs['EventDate'] . '</a></td>
							<td>' . $rs['StartTime'] . '</td>
							<td>' . stripslashes($eventTypeAry[$rs['EventTypeID']]) . '</td>
							<td>' . nl2br($rs['Summary']) . '</td>
							<td><span class="table_row_tool"><a class="delete removeEvent" title="' . $Lang['medical']['case']['RemoveEvent'] . '" data-EventID="' . $rs['EventID'] . '"></a></span>
								<input type="hidden" name="event[' . $formId . '][event][]" value="' . $rs['EventID'] . '"></td>
						</tr>';
            }
            return $x;
        }

        public function is_use_plupload()
        {
            global $userBrowser;
            $is_below_IE8 = ($userBrowser->browsertype == "MSIE" && intval($userBrowser->version) < 8);
            $is_older_firefox = ($userBrowser->browsertype == "Firefox" && intval($userBrowser->version) < 4);
            $is_mobile_tablet_platform = ($userBrowser->platform == "iPad" || $userBrowser->platform == "Andriod");
            $is_chrome = $userBrowser->isChrome();
            $use_plupload = ! $is_mobile_tablet_platform && ! $is_below_IE8 && ! $is_older_firefox;
            
            return $use_plupload;
        }

        public function handleTempUploadFolder()
        {
            global $PATH_WRT_ROOT;
            $ret = array();
            $ret['TempFolderPath'] = '';
            if ($this->is_use_plupload()) {
                
                include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
                
                $lfs = new libfilesystem();
                
                $tempFolderPath = '/u' . $_SESSION['UserID'] . '_' . time();
                $ret['TempFolderPath'] = $tempFolderPath;
                
                // Clean old temp folders
                $sql = "SELECT * FROM MEDICAL_TEMP_UPLOAD_FOLDER WHERE UserID='" . $_SESSION['UserID'] . "' AND TIMESTAMPDIFF(SECOND,InputDate,NOW()) > 86400";
                $tempFolderAry = $this->objDB->returnResultSet($sql);
                $folderCount = count($tempFolderAry);
                
                $folderIdToRemove = array();
                for ($i = 0; $i < $folderCount; $i ++) {
                    $tempFolder = $tempFolderAry[$i]['Folder'];
                    
                    $tempDeleteFolderPath = str_replace('//', '/', $PATH_WRT_ROOT . "file/medical/temp/" . $tempFolder);
                    if (file_exists($tempDeleteFolderPath)) {
                        $lfs->deleteDirectory($tempDeleteFolderPath);
                    }
                    $folderIdToRemove[] = $tempFolderAry[$i]['FolderID'];
                }
                if (count($folderIdToRemove) > 0) {
                    $sql = "DELETE FROM MEDICAL_TEMP_UPLOAD_FOLDER WHERE UserID='" . $_SESSION['UserID'] . "' AND FolderID IN (" . implode(",", $folderIdToRemove) . ")";
                    $this->objDB->db_db_query($sql);
                }
                
                // Store the new temp folder
                $sql = "INSERT INTO MEDICAL_TEMP_UPLOAD_FOLDER (UserID,Folder,InputDate) VALUES ('" . $_SESSION['UserID'] . "','" . $tempFolderPath . "',NOW())";
                $this->objDB->db_db_query($sql);
            }
            return $ret;
        }

        // default module eDiscipline
        public function handleUploadFile($TargetFolder, $recordID, $table, $module = 'eDiscipline')
        {
            global $PATH_WRT_ROOT, $medical_cfg;
            include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
            
            $result = array();
            $returnMsg = '';
            $result[] = $recordID;
            $dataAry = array();
            switch ($table) {
                case 'event':
                    $table = 'MEDICAL_EVENT_ATTACHMENT';
                    $keyField = 'EventID';
                    break;
                case 'event_followup':
                    $table = 'MEDICAL_EVENT_FOLLOWUP_ATTACHMENT';
                    $keyField = 'FollowupID';
                    break;
                case 'minute':
                    $table = 'MEDICAL_CASE_MINUTE_ATTACHMENT';
                    $keyField = 'MinuteID';
                    break;
                case 'staff_event':
                    $table = 'MEDICAL_STAFF_EVENT_ATTACHMENT';
                    $keyField = 'StaffEventID';
                    break;
            }
            
            // handle file directory
            if (! empty($TargetFolder) || ! empty($_FILES)) {
                $libfilesystem = new libfilesystem();
                
                $MaxFileSize = $medical_cfg['general']['MaxFileSize']; // MB
                $RestrictFileSize = is_numeric($MaxFileSize) && $MaxFileSize > 0;
                $MaxFileSizeInBytes = floatval($MaxFileSize) * 1024 * 1024;
                
                // create folder
                $today = date('Ymd');
                $targetPath = "/medical/";
                $targetPath_2ndLevel = "/edisc/";
                $target1stPath = str_replace('//', '/', $PATH_WRT_ROOT . "file/" . $targetPath);
                
                if (! file_exists($target1stPath) || ! is_dir($target1stPath)) {
                    $resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
                }
                
                $target2ndPath = str_replace('//', '/', $PATH_WRT_ROOT . "file/" . $targetPath . $targetPath_2ndLevel);
                if (! file_exists($target2ndPath) || ! is_dir($target2ndPath)) {
                    $resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
                }
                
                $target3rdPath = str_replace('//', '/', $PATH_WRT_ROOT . "file/" . $targetPath . $targetPath_2ndLevel . "/" . $today . "/");
                if (! file_exists($target3rdPath) || ! is_dir($target3rdPath)) {
                    $resultCreatedFolder = $libfilesystem->folder_new($target3rdPath);
                }
                
                $targetFullPath = $target3rdPath;
                chmod($targetFullPath, 0777);
            }
            
            // handle uploaded files
            if (! empty($TargetFolder)) { // by plupload method
                
                $tempPath = str_replace('//', '/', $PATH_WRT_ROOT . "file/medical/temp/" . $TargetFolder);
                
                $tempFileAry = $libfilesystem->return_folderlist($tempPath);
                
                $numOfFiles = count($tempFileAry);
                
                for ($i = 0; $i < $numOfFiles; $i ++) {
                    
                    // get file name
                    $origFileName = $libfilesystem->get_file_basename($tempFileAry[$i]);
                    if (empty($origFileName))
                        continue;
                    
                    $fileSize = filesize($tempFileAry[$i]);
                    
                    // Check file size
                    if ($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
                        $result[] = false;
                        $returnMsg = 'FileSizeExceedLimit';
                        continue;
                    }
                    
                    // encode file name
                    $targetFileHashName = session_id() . time() . uniqid();
                    $targetFile = $targetFullPath . $targetFileHashName;
                    
                    // upload file
                    $uploadResult = $libfilesystem->lfs_copy($tempFileAry[$i], $targetFile);
                    if (file_exists($targetFile)) {
                        chmod($targetFile, 0777);
                    }
                    
                    // save to db
                    unset($dataAry);
                    $dataAry[$keyField] = $recordID;
                    $dataAry['FileName'] = $origFileName;
                    $dataAry['FileHashName'] = $targetFileHashName;
                    $dataAry['SizeInBytes'] = $fileSize;
                    $dataAry['FolderPath'] = $today;
                    $dataAry['UploadTime'] = 'now()';
                    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                    $fileID = $this->INSERT2TABLE($table, $dataAry, $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = false);
                }
                
                // Do clean up
                $libfilesystem->deleteDirectory($tempPath);
                $sql = "DELETE FROM MEDICAL_TEMP_UPLOAD_FOLDER WHERE UserID='" . $_SESSION['UserID'] . "' AND Folder='" . $TargetFolder . "'";
                $this->objDB->db_db_query($sql);
            } else if (! empty($_FILES)) { // traditional method
                
                $numOfFiles = count($_FILES['File']['name']);
                
                // get file name
                $tempFile = $_FILES['File']['tmp_name'];
                
                $origFileName = $_FILES['File']['name'];
                if (empty($origFileName))
                    continue;
                
                $fileSize = $_FILES['File']['size'];
                
                // Check file size
                if ($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
                    $result[] = false;
                    $returnMsg = 'FileSizeExceedLimit';
                }
                
                // encode file name
                $targetFileHashName = session_id() . time() . uniqid();
                $targetFile = $targetFullPath . $targetFileHashName;
                
                // upload file
                $uploadResult = move_uploaded_file($tempFile, $targetFile);
                
                chmod($targetFile, 0777);
                
                // save to db
                unset($dataAry);
                $dataAry[$keyField] = $recordID;
                $dataAry['FileName'] = $origFileName;
                $dataAry['FileHashName'] = $targetFileHashName;
                $dataAry['SizeInBytes'] = $fileSize;
                $dataAry['FolderPath'] = $today;
                $dataAry['UploadTime'] = 'now()';
                $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                $fileID = $this->INSERT2TABLE($table, $dataAry, $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = false);
            }
            
            if ($returnMsg != '') {
                $msg = $returnMsg;
            } else if (! in_array(false, $result)) {
                $msg = "AddSuccess";
            } else {
                $msg = "AddUnsuccess";
            }
            
            return $msg;
        }

        // Add record to a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function INSERT2TABLE($table, $dataAry = array(), $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = true)
        {
            global $is_debug;
            
            $result = false;
            $recordID = 0;
            if ($insertIgnore)
                $IGNORE = " IGNORE ";
            
            $sql = "INSERT {$IGNORE} INTO `{$table}` SET ";
            
            foreach ($dataAry as $field => $value) {
                switch ($value) {
                    case "now()":
                        $sql .= "`" . $field . "`= now(),";
                        break;
                    case "null()":
                        $sql .= "`" . $field . " = null,";
                        break;
                    default:
                        $sql .= "`" . $field . "`= '" . $this->objDB->Get_Safe_Sql_Query($value) . "',";
                        break;
                }
            }
            
            if ($updateDateModified) {
                $sql .= "`DateModified`=now(),";
            }
            $sql = substr($sql, 0, - 1); // remove last comma
            
            if (! empty($condition)) {
                foreach ($condition as $field => $value)
                    $tmp_cond[] = "`{$field}` = '" . $this->objDB->Get_Safe_Sql_Query($value) . "'";
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            if ($is_debug) {
                echo $sql . '<br>';
                return;
            } else {
                if ($run) {
                    $result = $this->objDB->db_db_query($sql);
                    if ($result) {
                        $recordID = $this->objDB->db_insert_id();
                    }
                    return $recordID ? $recordID : $result;
                } else {
                    return $sql;
                }
            }
        }

        // Update a table
        // $table = "table_name" ;
        // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
        // $condition = array( 'field1' => 'data1', field2 => 'data2');
        //
        function UPDATE2TABLE($table, $dataAry = array(), $condition = array(), $run = true, $updateDateModified = true)
        {
            global $UserID;
            if (! is_array($dataAry)) {
                return false;
            }
            
            $sql = "UPDATE `{$table}` SET ";
            
            foreach ($dataAry as $field => $value) {
                switch ($value) {
                    case "now()":
                        $sql .= "`" . $field . "`= now(),";
                        break;
                    case "null()":
                        $sql .= "`" . $field . " = null,";
                        break;
                    default:
                        $sql .= "`" . $field . "`= '" . $this->objDB->Get_Safe_Sql_Query($value) . "',";
                        break;
                }
            }
            
            if ($updateDateModified) {
                $sql .= "`DateModified`=now(),";
            }
            $sql = substr($sql, 0, - 1); // remove last comma
            
            if (! empty($condition)) {
                foreach ($condition as $field => $value) {
                    $tmp_cond[] = "`{$field}` = '" . $this->objDB->Get_Safe_Sql_Query($value) . "'";
                }
                $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            }
            if ($run) {
                $result = $this->objDB->db_db_query($sql);
                return $result;
            } else {
                return $sql;
            }
        }

        // # eDiscipline End
        public function getMedicalGeneralSetting($name)
        {
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE SettingName='$name' AND Module='Medical'";
            $rs = $this->objDB->returnResultSet($sql);
            if ($rs) {
                $rs = current($rs);
                return $rs;
            } else {
                return '';
            }
        }

        public function getGeneralSettings()
        {
            $rs = $this->getMedicalGeneralSetting('GeneralSettings'); // in this format: k1^:v1^~k2^:v2^~...kn^:vn
            $ret = array();
            if ($rs) {
                $generalSettingsAry = explode("^~", $rs['SettingValue']);
                foreach ($generalSettingsAry as $ary) {
                    list ($k, $v) = explode("^:", $ary);
                    $ret[$k] = $v;
                }
            }
            return $ret;
        }

        // ********* Start Revisit
        public function getHospital()
        {
            $sql = "SELECT DISTINCT Hospital FROM MEDICAL_REVISIT WHERE Hospital<>'' AND Hospital IS NOT NULL ORDER BY Hospital";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getDivision()
        {
            $sql = "SELECT DISTINCT Division FROM MEDICAL_REVISIT WHERE Division<>'' AND Division IS NOT NULL ORDER BY Division";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getRevisit($revisitID = '')
        {
            $sql = "SELECT * FROM MEDICAL_REVISIT WHERE 1 ";
            if (is_array($revisitID)) {
                $sql .= " AND RevisitID IN ('" . implode("','", $revisitID) . "')";
            } else if ($revisitID) {
                $sql .= " AND RevisitID='" . $revisitID . "'";
            }
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getRevisitDrug($revisitID, $drugID = '')
        {
            $sql = "SELECT * FROM MEDICAL_REVISIT_DRUG WHERE 1 ";
            if (is_array($revisitID)) {
                $sql .= " AND RevisitID IN ('" . implode("','", $revisitID) . "')";
            } else if ($revisitID) {
                $sql .= " AND RevisitID='" . $revisitID . "'";
            }
            if ($drugID) {
                $sql .= " AND DrugID='" . $drugID . "'";
            }
            $sql .= "ORDER BY RevisitID,Drug";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        public function getLastRevisitID($studentID)
        {
            if ($studentID) {
                $sql = "SELECT RevisitID FROM MEDICAL_REVISIT WHERE StudentID='" . $studentID . "' ORDER BY RevisitDate DESC LIMIT 1";
                $rs = $this->objDB->returnResultSet($sql);
                $revisitID = count($rs) ? $rs[0]['RevisitID'] : '';
                return $revisitID;
            } else {
                return '';
            }
        }

        public function getLastDrugByStudent($studentID)
        {
            global $Lang;
            
            $revisitID = $this->getLastRevisitID($studentID);
            if ($revisitID) {
                $rs = $this->getRevisitDrug($revisitID);
                return $this->getDrugTableView($rs);
            }
            return '<div>' . $Lang['General']['NoRecordAtThisMoment'] . '</div>';
        }

        // for new add row
        public function getDrugRow($rowID, $rs = array(), $disabled = '')
        {
            global $Lang;
            
            $x = '<tr id="drug_row_' . $rowID . '" class="drug_row">
						<td class="DrugName"><input type="text" autocomplete="off" class="textboxtext" name="drug_name[]" id="drug_name_' . $rowID . '" value="' . intranet_htmlspecialchars($rs['Drug']) . '" ' . $disabled . '><span class="error_msg_hide" id="ErrDrugName_' . $rowID . '">' . $Lang['medical']['revisit']['Warning']['InputDrugName'] . '</span>
						</td>
						<td class="Dosage"><input type="text" name="dosage[]" id="dosage_' . $rowID . '" value="' . ($rs['Dosage'] > 0 ? $rs['Dosage'] : '') . '" style="width:50px;" ' . $disabled . '><span class="error_msg_hide" id="ErrDosage_' . $rowID . '">' . $Lang['medical']['revisit']['Warning']['InputDosage'] . '</span>
							<input type="radio" name="drug_unit[' . $rowID . ']" id="drug_unit_' . $rowID . '_mg" value="mg" ' . ($rs['Unit'] == 'mg' ? 'checked' : '') . ' ' . $disabled . '><label id="label_unit_' . $rowID . '_mg" for="drug_unit_' . $rowID . '_mg">' . $Lang['medical']['revisit']['DosageUnit']['mg'] . '</label>
							<input type="radio" name="drug_unit[' . $rowID . ']" id="drug_unit_' . $rowID . '_ml" value="ml" ' . ($rs['Unit'] == 'ml' ? 'checked' : '') . ' ' . $disabled . '><label id="label_unit_' . $rowID . '_ml" for="drug_unit_' . $rowID . '_ml">' . $Lang['medical']['revisit']['DosageUnit']['ml'] . '</label>
							<input type="radio" name="drug_unit[' . $rowID . ']" id="drug_unit_' . $rowID . '_prn" value="prn" ' . ($rs['Unit'] == 'prn' ? 'checked' : '') . ' ' . $disabled . '><label id="label_unit_' . $rowID . '_prn" for="drug_unit_' . $rowID . '_prn">' . $Lang['medical']['revisit']['DosageUnit']['prn'] . '</label>
							<span class="error_msg_hide" id="ErrUnit_' . $rowID . '">' . $Lang['medical']['revisit']['Warning']['SelectUnit'] . '</span>
						</td>';
            if ($disabled == '') {
                $x .= '<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['revisit']['RemoveDrug'] . '" onClick="remove_drug(\'' . $rowID . '\')" ' . $disabled . '></a></span></td>';
            }
            $x .= '</tr>';
            
            // if ($disabled == '') {
            // $x .= '<script> $("#drug_name_'.$rowID.'").inputselect({image_path: "/images/2009a/", ajax_script: "?t=management.ajax.ajax_get_selection_list&ma=1",js_lang_alert: {"no_records" : "'.$Lang['General']['NoRecordAtThisMoment'].'" }});</script>';
            // }
            return $x;
        }

        public function getDrugTable($data = array(), $disabled = '')
        {
            $x = '';
            for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
                $x .= $this->getDrugRow($i, $data[$i], $disabled);
            }
            return $x;
        }

        public function getDrugTableView($data = array())
        {
            global $Lang;
            
            $x = '<table id="LastDrugTable" class="common_table_list">
					<tr class="tabletop">
						<th width="55%">' . $Lang['medical']['revisit']['Drug'] . '</th>
						<th width="40%">' . $Lang['medical']['revisit']['Dosage'] . '</th>
					</tr>';
            
            for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
                $rs = $data[$i];
                $x .= '<tr>
							<td><input type="text" class="textboxtext" value="' . intranet_htmlspecialchars($rs['Drug']) . '" disabled></td>
							<td><input type="text" value="' . ($rs['Dosage'] > 0 ? $rs['Dosage'] : '') . '" style="width:50px;" disabled>
								<input type="radio" value="mg" ' . ($rs['Unit'] == 'mg' ? 'checked' : '') . ' disabled><label>' . $Lang['medical']['revisit']['DosageUnit']['mg'] . '</label>
								<input type="radio" value="ml" ' . ($rs['Unit'] == 'ml' ? 'checked' : '') . ' disabled><label>' . $Lang['medical']['revisit']['DosageUnit']['ml'] . '</label>
								<input type="radio" value="prn" ' . ($rs['Unit'] == 'prn' ? 'checked' : '') . ' disabled><label>' . $Lang['medical']['revisit']['DosageUnit']['prn'] . '</label>
							</td>
						</tr>';
            }
            $x .= '</table>';
            return $x;
        }

        public function getStudentRevisitReport($StudentID, $optionArr = array()){
            $table = "MEDICAL_REVISIT c";
            
            $cols ="IF(c.VisitDate = '0000-00-00', '-', c.VisitDate) AS VisitDate,
                    c.Hospital AS Hospital,
                    c.Division AS Division,
                    c.Diagnosis AS Diagnosis,
                    c.CuringStatus AS CuringStatus,
                    IF(c.RevisitDate = '0000-00-00', '-', c.RevisitDate) AS RevisitDate,
                    c.RevisitID AS RevisitID";
            $cond = " AND c.StudentID = ('$StudentID')";
            $cond .= " AND c.VisitDate >= '{$optionArr['StartDate']}'";
            $cond .= " AND c.VisitDate <= '{$optionArr['EndDate']}'";
            $itemCondArr = array();
            if($optionArr['Normal']){
                $itemCondArr[] = "(c.AccidentInjure = 0 AND c.DischargedAdmittion = 0)";
            }
            if($optionArr['AccidentInjure']){
                $itemCondArr[] = "(c.AccidentInjure = 1)";
            }
            if($optionArr['DischargedAdmittion']){
                $itemCondArr[] = "(c.DischargedAdmittion = 1)";
            }
            $cond .= " AND (" . implode(' OR ', $itemCondArr) . ")";
            if($optionArr['DisplayOrder']){
                $order = "c.VisitDate {$optionArr['DisplayOrder']}";
            } else {
                $order = "c.VisitDate ASC";
            }
            
            $sql = "SELECT $cols
                    FROM $table
                    WHERE 1
                         $cond
                    ORDER BY
                         $order";
//             debug_pr($sql);die();
            $result = $this->objDB->returnArray($sql);
            return $result;
        }
        // ********* End Revisit
        
        // ********* Start Staff Event
        public function getStaffEventType($eventTypeID='')
        {
            $sql = "SELECT * FROM MEDICAL_STAFF_EVENT_TYPE WHERE RecordStatus=1 AND DeletedFlag=0";
            if ($eventTypeID) {
                $sql .= " AND EventTypeID='" . $eventTypeID . "'";
            }
            $sql .= " ORDER BY EventTypeName";
            $rs = $this->objDB->returnArray($sql);
            return $rs;
        }

        public function getStaffEventTypeLev2($eventTypeLev2ID='', $eventTypeID='',  $idAndNameOnly=false)
        {
            $fields = $idAndNameOnly ? "EventTypeLev2ID, EventTypeLev2Name" : "*";
            
            $sql = "SELECT {$fields} FROM MEDICAL_STAFF_EVENT_TYPE_LEV2 WHERE RecordStatus=1 AND DeletedFlag=0";
            if ($eventTypeLev2ID) {
                $sql .= " AND EventTypeLev2ID='" . $eventTypeLev2ID . "'";
            }
            if ($eventTypeID) {
                $sql .= " AND EventTypeID='" . $eventTypeID . "'";
            }
            $sql .= " ORDER BY EventTypeLev2Name";
            $rs = $this->objDB->returnArray($sql);
            return $rs;
        }

        public function getTeacherOptionsByType($selected = '')
        {
            global $Lang;
            
            $x = '';
            
            $teachingStaffAry = $this->getTeacher($teacherType = '1');
            $teachingStaffAry = build_assoc_array($teachingStaffAry);
            $nonTeachingStaffAry = $this->getTeacher($teacherType = '0');
            $nonTeachingStaffAry = build_assoc_array($nonTeachingStaffAry);
            $data = array(
                $Lang['Identity']['TeachingStaff'] => $teachingStaffAry,
                $Lang['Identity']['NonTeachingStaff'] => $nonTeachingStaffAry
            );
            $firstTitle = $Lang['medical']['event']['AllTeacher'];
            $x .= getSelectByAssoArray($data, "name='StaffID' id='StaffID'", $selected, $all = 0, $noFirst = 0, $firstTitle);
            
            return $x;
        }

        // ********* End Staff Event
        
        // used for email notification
        public function getStudentLogEventContent($sid, $eventInfo)
        {
            global $Lang;
            
            $studentInfo = $this->getForStudentLog($sid);
            
            // use concat method because: (1) email program auto convert nl2br if use ob_start + ob_get_contents (2) must use in-line style to suit client site
            $x = '';
            $x .= '<html>';
            $x .= '<META http-equiv="Content-Type" content="text/html" Charset="UTF-8" />';
            $x .= '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">';
            $x .= '<tr>';
            $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['medical']['studentLog']['eventDate'] . '</td>';
            $x .= '<td>' . $eventInfo['EventDate'] . '</td>';
            $x .= '</tr>';
            
            $x .= '<tr>';
            $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['Header']['Menu']['Class'] . '</td>';
            $x .= '<td>' . $studentInfo['ClassName'] . '</td>';
            $x .= '</tr>';
            
            $x .= '<tr>';
            $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['Identity']['Student'] . '</td>';
            $x .= '<td>' . $studentInfo['Name'] . '(' . $studentInfo['ClassNumber'] . ')</td>';
            $x .= '</tr>';
            
            $x .= '<tr>';
            $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['medical']['studentLog']['time'] . '</td>';
            $x .= '<td>' . $eventInfo['EventTime'] . '</td>';
            $x .= '</tr>';
            
            $x .= '<tr>';
            $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['medical']['studentLog']['behaviour'] . '</td>';
            $x .= '<td>' . $eventInfo['Lev1Name'] . ' - ' . $eventInfo['Lev2Name'] . '</td>';
            $x .= '</tr>';
            
            $bodyParts = '';
            if (count($eventInfo['Lev3Name'])) {
                $bodyParts .= '<table border="0">';
                foreach ((array) $eventInfo['Lev3Name'] as $lev3ID => $lev3Name) {
                    $bodyParts .= '<tr><td width="30%">' . $lev3Name . ': </td><td>';
                    $i = 0;
                    foreach ((array) $eventInfo['Lev4Name'][$lev3ID] as $lev4ID => $lev4Name) {
                        $bodyParts .= ($i > 0 ? '<br>' : '') . $lev4Name;
                        $i ++;
                    }
                    $bodyParts .= '</td></tr>';
                }
                $bodyParts .= '</table>';
                
                $x .= '<tr>';
                $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['medical']['studentLog']['bodyParts'] . '</td>';
                $x .= '<td>' . $bodyParts . '</td>';
                $x .= '</tr>';
            }
            
            if ($eventInfo['TimeLasted_min'] > 0 || $eventInfo['TimeLasted_sec'] > 0) {
                $x .= '<tr>';
                $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['medical']['studentLog']['timelasted'] . '</td>';
                $x .= '<td>' . $eventInfo['TimeLasted_min'] . ' ' . $Lang['medical']['studentLog']['minute'] . ' ' . $eventInfo['TimeLasted_sec'] . ' ' . $Lang['medical']['studentLog']['second'] . '</td>';
                $x .= '</tr>';
            }
            
            $x .= '<tr>';
            $x .= '<td style="border-bottom: 1px solid #FFFFFF;width: 20%;background: #f3f3f3;padding-left: 2px; padding-right: 2px;">' . $Lang['General']['Remark'] . '</td>';
            $x .= '<td>' . nl2br(stripslashes(intranet_htmlspecialchars($eventInfo['Remarks']))) . '</td>';
            $x .= '</tr>';
            $x .= '</table>';
            $x .= '</html>';
            
            return $x;
        }

        // table layout of events for student log
        function getStudentLogEventLayout($studentLogID, $relevantEvents = '', $isReadOnly = false)
        {
            global $Lang;
            $x = '<tr valign="top">';
            $x .= '<td valign="top" nowrap="nowrap" class="field_title">' . $Lang['medical']['studentLog']['RelevantEvent'] . '</td>';
            $x .= '<td class="tabletext">';
            $x .= '<table width="100%" data-Id="' . $studentLogID . '" class="common_table_list eventTable' . $studentLogID . '" style="display:' . ($relevantEvents ? '' : 'none') . '">';
            $x .= '<tr>';
            $x .= '<th width="15%">' . $Lang['medical']['event']['EventDate'] . '</th>';
            $x .= '<th width="10%">' . $Lang['medical']['event']['tableHeader']['StartTime'] . '</th>';
            $x .= '<th width="20%">' . $Lang['medical']['event']['EventType'] . '</th>';
            $x .= '<th width="52%">' . $Lang['medical']['event']['Summary'] . '</th>';
            if (! $isReadOnly) {
                $x .= '<th>&nbsp;</th>';
            }
            $x .= '</tr>';
            $x .= $relevantEvents;
            $x .= '</table>';
            
            if (! $isReadOnly) {
                $x .= '<div>';
                $x .= '<span class="table_row_tool"><a class="add addEventBtn" data-Id="' . $studentLogID . '" title="' . $Lang['medical']['studentLog']['LinkRelevantEvent'] . '"></a></span>';
                $x .= '</div>';
            }
            $x .= '</td>';
            $x .= '</tr>';
            return $x;
        }

        // all event list related to the student log
        function getStudentLogEventList($studentLogID, $viewMode = '')
        {
            global $Lang;
            $eventTypeAry = $this->getEventType();
            $eventTypeAry = BuildMultiKeyAssoc($eventTypeAry, 'EventTypeID', $IncludedDBField = array(
                'EventTypeName'
            ), $SingleValue = 1);
            
            if (is_array($studentLogID)) {
                $cond = " AND es.StudentLogID IN ('" . implode("','", $studentLogID) . "')";
            } else if ($studentLogID) {
                $cond = " AND es.StudentLogID='$studentLogID'";
            } else {
                $cond = '';
            }
            
            if ($cond != '') {
                $sql = "SELECT 	es.StudentLogID,
								e.EventID,
								e.EventDate,
								DATE_FORMAT(e.StartTime,'%k:%i') as StartTime,
								e.EventTypeID,
								e.Summary
						FROM
								MEDICAL_EVENT e
						INNER JOIN
								MEDICAL_EVENT_STUDENT_LOG es ON es.EventID=e.EventID
								WHERE 1" . $cond . "
						ORDER BY e.EventDate, e.StartTime";
                $event = $this->objDB->returnResultSet($sql);
            } else {
                return '';
            }
            
            $x = '';
            for ($i = 0, $iMax = count($event); $i < $iMax; $i ++) {
                $rs = $event[$i];
                
                $x .= '<tr id="eventRow' . $rs['EventID'] . '">
							<td style="white-space:nowrap;">' . ($viewMode ? $rs['EventDate'] : '<a href="#" class="viewEvent" data-EventID="' . $rs['EventID'] . '">' . $rs['EventDate'] . '</a>') . '</td>
							<td>' . $rs['StartTime'] . '</td>
							<td>' . stripslashes($eventTypeAry[$rs['EventTypeID']]) . '</td>
							<td>' . nl2br($rs['Summary']) . '</td>' . ($viewMode ? '' : '
							<td><span class="table_row_tool"><a class="delete removeEvent" title="' . $Lang['medical']['case']['RemoveEvent'] . '" data-StudentLogID="' . $studentLogID . '" data-EventID="' . $rs['EventID'] . '"></a></span>
							<input type="hidden" name="event[' . $studentLogID . '][event][]" value="' . $rs['EventID'] . '"></td>') . '
						</tr>';
            }
            return $x;
        }

        public function getClassByStudentID($studentID)
        {
            global $junior_mck;
            
            $rs = array();
            $name_field = getNameFieldByLang2('u.');
            if ($junior_mck) {
                $sql = "SELECT 	
								u.ClassName
						FROM 
								INTRANET_USER u 
						WHERE 
								UserID='" . $studentID . "'";
                $rs = $this->objDB->returnResultSet($sql);
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $rs[$i]['ClassName'] = convert2unicode($rs[$i]['ClassName'], 1);
                }
            } else {
                $academicYearID = Get_Current_Academic_Year_ID();
                $sql = "SELECT 		
									yc.ClassTitleEN AS ClassName 
						FROM 
									INTRANET_USER u 
						INNER JOIN 
									YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
						INNER JOIN 
									YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						WHERE 
									yc.AcademicYearID='" . $academicYearID . "'
						AND u.UserID='" . $studentID . "'";
                $rs = $this->objDB->returnResultSet($sql);
            }
            
            if (count($rs)) {
                $rs = $rs[0]['ClassName'];
            } else {
                $rs = '';
            }
            return $rs;
        }

        // get user name and type, used for showing last update person for bowel record in eClassApp webview > bowel record details
        function getPersonInfo($userID)
        {
            $rs = "";
            if ($userID) {
                $sql = "SELECT 	UserID, " . getNameFieldByLang2('') . " As Name, 
								RecordType
						FROM
								INTRANET_USER
						WHERE
								UserID='" . $userID . "'";
                $rs = $this->objDB->returnResultSet($sql);
                
                if (count($rs)) {
                    $rs = current($rs);
                }
            }
            return $rs;
        }

        function isRequiredCheckSelfEvent()
        {
            if ($this->isSuperAdmin($_SESSION['UserID'])) {
                return false;
            } else {
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_DELETESELFRECORD')) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        function isRequiredCheckSelfCase()
        {
            if ($this->isSuperAdmin($_SESSION['UserID'])) {
                return false;
            } else {
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'CASE_DELETESELFRECORD')) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        function isRequiredCheckSelfAwardScheme()
        {
            if ($this->isSuperAdmin($_SESSION['UserID'])) {
                return false;
            } else {
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_DELETESELFRECORD')) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        function isRequiredCheckSelfRevisit()
        {
            if ($this->isSuperAdmin($_SESSION['UserID'])) {
                return false;
            } else {
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_DELETESELFRECORD')) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        
        function isRequiredCheckSelfStaffEvent()
        {
            if ($this->isSuperAdmin($_SESSION['UserID'])) {
                return false;
            } else {
                if ($this->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_DELETESELF')) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        
        function getStaffSelection($selectedType='', $selectedStaffID='')
        {
            global $Lang, $linterface;
            
            $nrStaff = count($selectedStaff);
            $staffTypeSelection = $this->getTeacherTypeSelection();
            $teacherType = ($selectedType) ? 1 : 0;
            $staffList = $this->getTeacher($teacherType);
            
            $x = '<select name=TeacherType id=TeacherType>';
            $x .= '<option value="1" ' . (($teacherType == 1) ? 'SELECTED' : '') . '>' . $Lang['Identity']['TeachingStaff'] . '</option>';
            $x .= '<option value="0" ' . (($teacherType == 0) ? 'SELECTED' : '') . '>' . $Lang['Identity']['NonTeachingStaff'] . '</option>';
            $x .= '</select>';
            $x .= '<span id="StaffIDSpan">';
            $x .= getSelectByArray($staffList, 'Name="StaffID" ID="StaffID"', $selectedStaffID);
            $x .= '</span>';
            
            return $x;
        }
        
        function getStaffEvents($staffEventID = '')
        {
            $staffName = getNameFieldByLang('s.');
            $sql = "SELECT s.Teaching, ".$staffName." AS StaffName, e.* 
                    FROM 
                            MEDICAL_STAFF_EVENT e
                            LEFT JOIN INTRANET_USER s ON s.UserID=e.StaffID
                    WHERE 1 ";
            if (is_array($staffEventID)) {
                $sql .= " AND StaffEventID IN ('" . implode("','", $staffEventID) . "')";
            } else if ($staffEventID) {
                $sql .= " AND StaffEventID='" . $staffEventID . "'";
            }
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }
        
        function getStaffEventAttachment($staffEventID)
        {
            $cond = (is_array($staffEventID)) ? "StaffEventID IN ('" . implode("','", (array) $staffEventID) . "')" : "StaffEventID='" . $staffEventID . "'";
            $sql = "SELECT * FROM MEDICAL_STAFF_EVENT_ATTACHMENT WHERE {$cond} ORDER BY FileName";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }
        
        function getStudentEventListOfStaffEvent($staffEventID, $viewMode = '', $linkEventDetails = true)
        {
            global $Lang;
            $eventTypeAry = $this->getEventType();
            $eventTypeAry = BuildMultiKeyAssoc($eventTypeAry, 'EventTypeID', $IncludedDBField = array(
                'EventTypeName'
            ), $SingleValue = 1);
            
            $studentName = getNameFieldWithClassNumberByLang("u.");
            $auStudentName = getNameFieldWithClassNumberByLang("au.");
            
            if (is_array($staffEventID)) {
                $cond = " AND se.StaffEventID IN ('" . implode("','", $staffEventID) . "')";
            } else if ($staffEventID) {
                $cond = " AND se.StaffEventID='" . $staffEventID . "'";
            } else {
                $cond = '';
            }
            
            if ($cond != '') {
                $sql = "SELECT 	e.EventID,
								e.EventDate,
								DATE_FORMAT(e.StartTime,'%k:%i') as StartTime,
								GROUP_CONCAT(IFNULL($studentName,$auStudentName) ORDER BY IFNULL($studentName,$auStudentName) SEPARATOR ', ') AS StudentName,
								e.EventTypeID,
								e.Summary
						FROM
								MEDICAL_EVENT e
						INNER JOIN
								MEDICAL_EVENT_STAFF_EVENT se ON se.EventID=e.EventID
                        INNER JOIN 
                                MEDICAL_EVENT_STUDENT s ON s.EventID=e.EventID
                        LEFT JOIN
                                INTRANET_USER u ON u.UserID=s.StudentID
                        LEFT JOIN 
                                INTRANET_ARCHIVE_USER au ON au.UserID=s.StudentID
						WHERE 1" . $cond . "
                        GROUP BY e.EventID
						        ORDER BY e.EventDate, e.StartTime";
                $event = $this->objDB->returnResultSet($sql);
            } else {
                return '';
            }
            
            $x = '';
            for ($i = 0, $iMax = count($event); $i < $iMax; $i ++) {
                $rs = $event[$i];
                
                $x .= '<tr id="student_event_row_' . $rs['EventID'] . '">
							<td style="white-space:nowrap;">'.($linkEventDetails ? '<a href="javascript:view_event(\'' . $rs['EventID'] . '\')">' . $rs['EventDate'] . '</a>' : $rs['EventDate']) . '</td>
							<td>' . $rs['StartTime'] . '</td>
                            <td>' . $rs['StudentName'] . '</td>
							<td>' . stripslashes($eventTypeAry[$rs['EventTypeID']]) . '</td>
							<td>' . nl2br($rs['Summary']) . '</td>' . ($viewMode ? '' : '
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['staffEvent']['RemoveEvent']. '" onClick="remove_student_event_of_staff_event(' . $staffEventID . ',' . $rs['EventID'] . ')"></a></span>
							<input type="hidden" name="SelEventID[]" value="' . $rs['EventID'] . '"></td>') . '
						</tr>';
            }
            return $x;
        }

        function getStaffEventStudentEvent($staffEventID = '', $studentEventID = '')
        {
            $sql = "SELECT * FROM MEDICAL_EVENT_STAFF_EVENT WHERE 1 ";
            if (is_array($staffEventID)) {
                $sql .= " AND StaffEventID IN ('" . implode("','", $staffEventID) . "')";
            } elseif ($staffEventID) {
                $sql .= " AND StaffEventID='" . $staffEventID . "'";
            }
            
            if (is_array($studentEventID)) {
                $sql .= " AND EventID IN ('" . implode("','", $studentEventID) . "')";
            } elseif ($studentEventID) {
                $sql .= " AND EventID='" . $studentEventID . "'";
            }
            $sql .= "ORDER BY EventID";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }
        
        // check if a user is the staff of STAFF_EVENT
        function isStaffEventStaff($staffID, $staffEventID='')
        {
            $sql = "SELECT StaffID FROM MEDICAL_STAFF_EVENT WHERE StaffID='".$staffID."'";
            if ($staffEventID) {
                $sql .= " AND StaffEventID='".$staffEventID."'";
            }
            $sql .= " LIMIT 1";
            $rs = $this->objDB->returnResultSet($sql);
            
            return count($rs) ? true : false;
        }
        
        // $staffType='-1' denotes all staff
        function getMultipleSelectStaff($staffType='-1')
        {
            $x = '<select name="staffID[]" id="staffID" style="min-width:200px; height:156px;" multiple>';
            $data = $this->getTeacher($staffType);
            for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
                $staffID = $data[$i]['UserID'];
                $staffName = $data[$i]['Name'];
                $x .= '<option value="' . $staffID. '">' . $staffName. '</option>';
            }
            $x .= '</select>';
            return $x;
        }
        
        // list by checkbox in two levels, used in report
        function getAllStaffEventTypes(){
            global $medical_cfg;
            
            $staffEventTypeAry = $this->getStaffEventType();
            $staffEventTypeLev2Ary = $this->getStaffEventTypeLev2();
            $staffEventTypeLev2Assoc = BuildMultiKeyAssoc($staffEventTypeLev2Ary, array('EventTypeID','EventTypeLev2ID'), array('EventTypeLev2Name'), $SingleValue = 1);
            
            $checkboxHTML  = '<table border=0 cellspacing=0 cellpadding=0>';
            $checkboxHTML .= '<tr><td colspan=2><input type ="checkbox" id="checkAllStaffEventType"/><label for="checkAllStaffEventType">'.$medical_cfg['general']['report']['search']['itemCheckbox']['all'].'</label></td></tr>'."\r\n";
            $i = 0;
            foreach((array)$staffEventTypeAry as $_idx => $_staffEventTypeAry)
            {
                $_eventTypeID = $_staffEventTypeAry["EventTypeID"];
                $_eventTypeName = $_staffEventTypeAry["EventTypeName"];
                
                $checkboxHTML .= '<tr><td width=120 class="staffEventTypeTr"><input type ="checkbox" class="level1 sub_item1" name="eventTypeID[]" data-Lev1ID ="'.$_eventTypeID.'" value="'.$_eventTypeID.'" id="level1_'.$_eventTypeID.'"/><label for="level1_'.$_eventTypeID.'">'.$_eventTypeName.'&nbsp;:</label></td>'."\r\n";
                $checkboxHTML .= '<td>';
                
                $j = 0;
                foreach((array)$staffEventTypeLev2Assoc[$_eventTypeID] as $__eventTypeLev2ID => $__eventTypeLev2Name)
                {
                    $checkboxHTML .= '<input type ="checkbox" class="level2 sub_item2 level2_'.$_eventTypeID.'" name="eventTypeLev2ID[]" value="'.$__eventTypeLev2ID.'" data-Lev1ID ="'.$_eventTypeID.'" id="level2'.$i.'_'.$j.'"/><label for="level2'.$i.'_'.$j.'">'.$__eventTypeLev2Name.'</label>'."\r\n";
                    if(++$j%6==0){
                        $checkboxHTML .= '<br />'."\r\n";
                    }
                }
                $checkboxHTML .= '</td></tr>';
                $i++;
            }
            $checkboxHTML .= '</table>';
            
            return	$checkboxHTML;
        }
        
        // $filter = array($startDate, $endDate, $staffID, $staffEventTypeID, $staffEventTypeLev2ID, $order)
        function getStaffEventReport($filter) 
        {
            $cond = '';
            
            if (count($filter['staffID'])) {
                $cond .= " AND e.StaffID IN ('" . implode("','", $filter['staffID']) . "')";
            }

            if (count($filter['staffEventTypeID'])) {
                $cond .= " AND e.EventTypeID IN ('" . implode("','", $filter['staffEventTypeID']) . "')";
            }
            
            if (count($filter['staffEventTypeLev2ID'])) {
                $cond .= " AND e.EventTypeLev2ID IN ('" . implode("','", $filter['staffEventTypeLev2ID']) . "')";
            }
            
            if ($filter['startDate']) {
                $cond .= " AND e.EventDate>='".$filter['startDate']."'";
            }

            if ($filter['endDate']) {
                $cond .= " AND e.EventDate<='".$filter['endDate']."'";
            }
            
            $staffName = getNameFieldByLang('s.');
            $updatedBy = getNameFieldByLang2('ub.');
            $orderBy = $filter['displayOrder'] == 'desc' ? 'DESC' : 'ASC';
            
            $sql = "SELECT
        				" . $staffName. " AS StaffName,
                        e.EventDate,
                        e.StaffEventID,
        				DATE_FORMAT(e.StartTime,'%k:%i') AS StartTime,
                        CASE
                            WHEN e.EventTypeID=0 AND e.EventTypeLev2ID=0 THEN '-'
                            WHEN e.EventTypeID=0 AND e.EventTypeLev2ID>0 THEN et2.EventTypeLev2Name
                            WHEN e.EventTypeID>0 AND e.EventTypeLev2ID=0 THEN et.EventTypeName
                            ELSE CONCAT(et.EventTypeName,' > ', et2.EventTypeLev2Name)
                        END AS EventType,
        				e.IsReportedInjury,
                        e.InjuryLeave,
                        e.SickLeave,
                        e.InjuryLeave + e.SickLeave AS TotalLeave,
                        e.Remark,        				
                        " . $updatedBy. " as UpdatedBy,
                        e.DateModified,
                        m.StaffEventID AS AttachmentStaffEventID
        		FROM
        				MEDICAL_STAFF_EVENT e
        		INNER JOIN
        				INTRANET_USER s ON s.UserID=e.StaffID
        		LEFT JOIN MEDICAL_STAFF_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
                LEFT JOIN MEDICAL_STAFF_EVENT_TYPE_LEV2 et2 ON et2.EventTypeLev2ID=e.EventTypeLev2ID
                LEFT JOIN INTRANET_USER ub ON ub.UserID=e.LastModifiedBy
                LEFT JOIN MEDICAL_STAFF_EVENT_ATTACHMENT m ON m.StaffEventID=e.StaffEventID
        		WHERE 1 " . $cond. " GROUP BY e.StaffEventID ORDER BY e.EventDate " . $orderBy . ",s.EnglishName";
            $rs = $this->objDB->returnResultSet($sql);
//debug_pr($sql);            
            return $rs;
        }
        
        function getStaffEventReportLayout($filter, $format='html')
        {
            global $Lang, $image_path;
            
            $staffEventAry = $this->getStaffEventReport($filter);
            if (count($staffEventAry) == 0) {
                $x = "<table class=\"common_table_list_v30 view_table_list_v30\">";
                $x .= '<tr><td style="text-align: center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
                $x .= '</table>';
            }
            else {
                $staffEventAssoc = BuildMultiKeyAssoc($staffEventAry, array('EventDate', 'StaffEventID'));
//debug_pr($staffEventAssoc);                
                foreach((array)$staffEventAssoc as $eventDate => $_staffEventAssoc) {
//debug_pr($_staffEventAssoc);
                    if ($tableCounter) {
                        $x .= "<br><br>";
                    }
                    $tableCounter++;
                    
                    $x .= '<div class="reportSection" style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.$Lang['medical']['menu']['staffEvent'].': ( '.$eventDate.' )'.'</div><br />';
                    
                    $x .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
                        $x .= "<thead>";
                            $x .= "<tr>";
                                $x .= "<th width='10%'>".$Lang['medical']['staffEvent']['tableHeader']['StaffName']."</th>";
                                $x .= "<th width='6%'>".$Lang['General']['Time']."</th>";
                                $x .= "<th width='12%'>".$Lang['medical']['staffEvent']['tableHeader']['EventType']."</th>";
                                $x .= "<th width='6%'>".$Lang['medical']['staffEvent']['HasReportedInjury']."</th>";
                                $x .= "<th width='7%'>".$Lang['medical']['staffEvent']['DaysOfInjuryLeave']."</th>";
                                $x .= "<th width='7%'>".$Lang['medical']['staffEvent']['DaysOfSickLeave']."</th>";
                                $x .= "<th width='7%'>".$Lang['medical']['reportStaffEvent']['TotalLeave']."</th>";
                                $x .= "<th width='19%'>".$Lang['medical']['staffEvent']['remark']."</th>";
                                $x .= "<th width='10%'>".$Lang['medical']['general']['lastModifiedBy']."</th>";
                                $x .= "<th width='10%'>".$Lang['medical']['general']['dateModified']."</th>";
                                $x .= "<th width='6%'>".$Lang['medical']['event']['Attachment']."</th>";
                            $x .= "</tr>";
                        $x .= "</thead>";
                    
                        $x .= "<tbody>";
                    
                        foreach((array)$_staffEventAssoc as $__eventID => $__staffEventAssoc) {
//debug_pr($__staffEventAssoc);
                            $disReportedInjury = $__staffEventAssoc['IsReportedInjury'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
                            if ($__staffEventAssoc['AttachmentStaffEventID']) {
                                if ($format == 'print') {
                                    $attachment = $Lang['General']['Yes2'];
                                }
                                else {
                                    $attachment = "<a id='fixedSizeThickboxLink' class='print_hide tablelink fixedSizeThickboxLink' href='javascript:void(0);' data-Id='{$__staffEventAssoc['AttachmentStaffEventID']}' title='".$Lang['SysMgr']['Homework']['Attachment']."' ><img src='{$image_path}/icon/attachment_blue.gif' border=0 /></a> ";
                                }
                            }
                            else {
                                $attachment = '-';
                            }
                            $x .= "<tr>";
                                $x .= "<td>".$__staffEventAssoc['StaffName']."</td>";
                                $x .= "<td>".$__staffEventAssoc['StartTime']."</td>";
                                $x .= "<td>".$__staffEventAssoc['EventType']."</td>";
                                $x .= "<td>".$disReportedInjury."</td>";
                                $x .= "<td>".$__staffEventAssoc['InjuryLeave']."</td>";
                                $x .= "<td>".$__staffEventAssoc['SickLeave']."</td>";
                                $x .= "<td>".$__staffEventAssoc['TotalLeave']."</td>";
                                $x .= "<td>".nl2br($__staffEventAssoc['Remark'])."</td>";
                                $x .= "<td>".$__staffEventAssoc['UpdatedBy']."</td>";
                                $x .= "<td>".$__staffEventAssoc['DateModified']."</td>";
                                $x .= "<td>".$attachment."</td>";
                            $x .= "</tr>";
                        }
                        $x .= "</tbody>";
                    $x .= "</table>";
                }
            }
            return $x;
        }
        
        // list by checkbox, used in report
        function getAllStudentEventTypes(){
            global $medical_cfg;
            
            $studentEventTypeAry = $this->getEventType();
            
            $checkboxHTML  = '<table border=0 cellspacing=0 cellpadding=0>';
            $checkboxHTML .= '<tr><td colspan=2><input type ="checkbox" id="checkAllStudentEventType"/><label for="checkAllStudentEventType">'.$medical_cfg['general']['report']['search']['itemCheckbox']['all'].'</label></td></tr>'."\r\n";
            $i = 0;
            foreach((array)$studentEventTypeAry as $_idx => $_studentEventTypeAry)
            {
                $_eventTypeID = $_studentEventTypeAry["EventTypeID"];
                $_eventTypeName = stripslashes($_studentEventTypeAry["EventTypeName"]);
                $checkboxHTML .= '<tr><td class="StudentEventTypeTr"><input type ="checkbox" class="level1" name="eventTypeID[]" data-Lev1ID ="'.$_eventTypeID.'" value="'.$_eventTypeID.'" id="level1_'.$_eventTypeID.'"/><label for="level1_'.$_eventTypeID.'">'.$_eventTypeName.'&nbsp;</label></td></tr>'."\r\n";
            }
            $checkboxHTML .= '</table>';
            
            return	$checkboxHTML;
        }
        
        // $filter = array($startDate, $endDate, $studentID, $eventTypeID, $order)
        // $displayType - html / csv
        function getStudentEventReport($filter)
        {
            global $Lang;
            
            $cond = '';
            $studentCond = '';
            if (count($filter['studentList'])) {
                $studentCond .= " WHERE s.StudentID IN ('" . implode("','", $filter['studentList']) . "')";
            }
            
            if (count($filter['eventTypeID'])) {
                $cond .= " AND e.EventTypeID IN ('" . implode("','", $filter['eventTypeID']) . "')";
            }
            
            if ($filter['startDate']) {
                $cond .= " AND e.EventDate>='".$filter['startDate']."'";
            }
            
            if ($filter['endDate']) {
                $cond .= " AND e.EventDate<='".$filter['endDate']."'";
            }
            
            $AcademicYearID = Get_Current_Academic_Year_ID();
            $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
            $classNameEng = "yc.ClassTitleEN";
            $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "' ";
            
            $locationField = Get_Lang_Selection("NameChi", "NameEng");
            $student_name = getNameFieldByLang2('u.');
            $affected_name = getNameFieldByLang2('u.');
            $updatedBy = getNameFieldByLang2('ub.');
            $orderBy = $filter['displayOrder'] == 'desc' ? 'DESC' : 'ASC';
            
            $sql = "SELECT
        				s.Student,
                        e.EventDate,
                        e.EventID,
        				DATE_FORMAT(e.StartTime,'%k:%i') AS StartTime,
        				et.EventTypeName,
        				CASE 
        					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN '-'
        					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN loc." . $locationField . "
        					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN lvl." . $locationField . "
        					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID<>0 THEN CONCAT(lvl." . $locationField . ",' > ',loc." . $locationField . ")
        					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN b." . $locationField . "
        					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN CONCAT(b." . $locationField . ",' > ',loc." . $locationField . ")
        					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ")
        					ELSE CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ",' > ',loc." . $locationField . ")
        				END as Location,
                        e.Summary,
        				IF(e.AffectedNonAccountHolder IS NULL OR e.AffectedNonAccountHolder='', IF(a.AffectedPerson IS NULL OR a.AffectedPerson='', '-', a.AffectedPerson), CONCAT(IF(a.AffectedPerson IS NULL,'',CONCAT(a.AffectedPerson,',<br>')), e.AffectedNonAccountHolder)) as AffectedPerson, 
                        " . $updatedBy. " as UpdatedBy,
                        e.DateModified,
                        m.EventID AS AttachmentEventID
        		FROM
        				MEDICAL_EVENT e
        		INNER JOIN (
        				SELECT 	EventID, 
        						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as StudentID,
        						GROUP_CONCAT(DISTINCT CONCAT('^',$classNameEng,'~') ORDER BY $classNameEng SEPARATOR ',') as ClassName, 
        						GROUP_CONCAT(DISTINCT CONCAT(" . $student_name . ",' ('," . $classNameField . ",IF(p.stayOverNight=1,'" . $Lang['medical']['general']['sleepOption']['StayIn'] . "','" . $Lang['medical']['general']['sleepOption']['StayOut'] . "'),')') ORDER BY u.ClassName, u.ClassNumber SEPARATOR ',<br>') as Student
        				FROM 
        						MEDICAL_EVENT_STUDENT s
        				INNER JOIN 
        						INTRANET_USER u ON u.UserID=s.StudentID
        				" . $joinClass . "
        				LEFT JOIN 
        						INTRANET_USER_PERSONAL_SETTINGS p ON p.UserID=u.UserID
                        " . $studentCond . "
        				GROUP BY s.EventID
        			) AS s ON s.EventID=e.EventID
            		LEFT JOIN MEDICAL_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
            		LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=e.LocationBuildingID
            		LEFT JOIN INVENTORY_LOCATION_LEVEL lvl ON lvl.LocationLevelID=e.LocationLevelID
            		LEFT JOIN INVENTORY_LOCATION loc ON loc.LocationID=e.LocationID
            		LEFT JOIN (
            				SELECT 	EventID, 
            						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as AffectedPersonID,
            						GROUP_CONCAT(DISTINCT " . $affected_name . " ORDER BY " . $affected_name . " SEPARATOR ',<br>') AS AffectedPerson
            				FROM 
            						MEDICAL_EVENT_AFFECTED_PERSON p
            				INNER JOIN 
            						INTRANET_USER u ON u.UserID=p.AffectedPersonID
            				GROUP BY EventID
            			) AS a ON a.EventID=e.EventID
                LEFT JOIN INTRANET_USER ub ON ub.UserID=e.LastModifiedBy
                LEFT JOIN MEDICAL_EVENT_ATTACHMENT m ON m.EventID=e.EventID
        		WHERE 1 " . $cond. " GROUP BY e.EventID ORDER BY e.EventDate " . $orderBy;
            $rs = $this->objDB->returnResultSet($sql);
//            debug_pr($sql);
            return $rs;
        }
        
        function getStudentEventReportLayout($filter, $format='html')
        {
            global $Lang, $image_path;
            
            $studentEventAry = $this->getStudentEventReport($filter);
            if (count($studentEventAry) == 0) {
                $x = "<table class=\"common_table_list_v30 view_table_list_v30\">";
                $x .= '<tr><td style="text-align: center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
                $x .= '</table>';
            }
            else {
//                 $eventIDAry = Get_Array_By_Key($studentEventAry,'EventID');
//                 $eventFollowupAry = $this->getEventFollowup($eventIDAry);
//                 $eventFollowupAssoc = BuildMultiKeyAssoc($eventFollowupAry, array('EventID', 'FollowupID'));
                
                $tableCounter = 0;
                $studentEventAssoc = BuildMultiKeyAssoc($studentEventAry, array('EventDate', 'EventID'));
                //debug_pr($studentEventAssoc);
                foreach((array)$studentEventAssoc as $eventDate => $_studentEventAssoc) {
                    //debug_pr($_studentEventAssoc);
                    if ($tableCounter) {
                        $x .= "<br><br>";
                    }
                    $tableCounter++;
                    
                    $x .= '<div class="reportSection" style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.$Lang['medical']['menu']['event'].': ( '.$eventDate.' )'.'</div><br />';
                    
                    $x .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
                    $x .= "<thead>";
                    $x .= "<tr>";
                    $x .= "<th width='13%'>".$Lang['medical']['event']['tableHeader']['StudentName']."</th>";
                    $x .= "<th width='7%'>".$Lang['medical']['event']['tableHeader']['StartTime']."</th>";
                    $x .= "<th width='10%'>".$Lang['medical']['event']['tableHeader']['EventType']."</th>";
                    $x .= "<th width='13%'>".$Lang['medical']['event']['tableHeader']['Place']."</th>";
                    $x .= "<th width='17%'>".$Lang['medical']['event']['Summary']."</th>";
                    $x .= "<th width='13%'>".$Lang['medical']['event']['tableHeader']['AffectedPerson']."</th>";
                    $x .= "<th width='10%'>".$Lang['medical']['general']['lastModifiedBy']."</th>";
                    $x .= "<th width='10%'>".$Lang['medical']['general']['dateModified']."</th>";
                    $x .= "<th width='6%'>".$Lang['medical']['event']['Attachment']."</th>";
                    $x .= "</tr>";
                    $x .= "</thead>";
                    
                    $x .= "<tbody>";
                    
                    foreach((array)$_studentEventAssoc as $__eventID => $__studentEventAssoc) {
                        //debug_pr($__studentEventAssoc);
                        
                        if ($__studentEventAssoc['AttachmentEventID']) {
                            if ($format == 'print') {
                                $attachment = $Lang['General']['Yes2'];
                            }
                            else {
                                $attachment = "<a id='fixedSizeThickboxLink' class='print_hide tablelink fixedSizeThickboxLink' href='javascript:void(0);' data-Id='{$__studentEventAssoc['AttachmentEventID']}' title='".$Lang['SysMgr']['Homework']['Attachment']."' ><img src='{$image_path}/icon/attachment_blue.gif' border=0 /></a> ";
                            }
                        }
                        else {
                            $attachment = "-";
                        }
                        
                        $x .= "<tr>";
                        $x .= "<td>".$__studentEventAssoc['Student']."</td>";
                        $x .= "<td>".$__studentEventAssoc['StartTime']."</td>";
                        $x .= "<td>".stripslashes($__studentEventAssoc['EventTypeName'])."</td>";
                        $x .= "<td>".$__studentEventAssoc['Location']."</td>";
                        $x .= "<td>".nl2br($__studentEventAssoc['Summary'])."</td>";
                        $x .= "<td>".$__studentEventAssoc['AffectedPerson']."</td>";
                        $x .= "<td>".$__studentEventAssoc['UpdatedBy']."</td>";
                        $x .= "<td>".$__studentEventAssoc['DateModified']."</td>";
                        $x .= "<td>".$attachment."</td>";
                        $x .= "</tr>";
                    }
                    $x .= "</tbody>";
                    $x .= "</table>";
                }
            }
            return $x;
        }

        public function getStaffEventRowForStudentEvent($staffEventIDAry)
        {
            global $Lang;
            
            $staffEventTypeAry = $this->getStaffEventType();
            $staffEventTypeAssoc = BuildMultiKeyAssoc($staffEventTypeAry, array('EventTypeID'), array('EventTypeName'), $SingleValue = 1);
            $staffEventTypeLev2Ary = $this->getStaffEventTypeLev2();
            $staffEventTypeLev2Assoc = BuildMultiKeyAssoc($staffEventTypeLev2Ary, array('EventTypeID','EventTypeLev2ID'), array('EventTypeLev2Name'), $SingleValue = 1);
            unset($staffEventTypeAry);
            unset($staffEventTypeLev2Ary);
            
            $staffEventAry = $this->getStaffEvents($staffEventIDAry);
            
            $x = '';
            for ($i = 0, $iMax = count($staffEventAry); $i < $iMax; $i ++) {
                $_rs = $staffEventAry[$i];
                $_eventTypeID = $_rs['EventTypeID'];
                $_eventTypeLev2ID= $_rs['EventTypeLev2ID'];
                if ($_eventTypeID && $_eventTypeLev2ID) {
                    $dispEventType = $staffEventTypeAssoc[$_eventTypeID] . ' > ' . $staffEventTypeLev2Assoc[$_eventTypeID][$_eventTypeLev2ID]; 
                }elseif ($_eventTypeID && !$_eventTypeLev2ID) {
                    $dispEventType = $staffEventTypeAssoc[$_eventTypeID];
                }elseif (!$_eventTypeID && $_eventTypeLev2ID) {
                    $dispEventType = $staffEventTypeLev2Assoc[$_eventTypeID][$_eventTypeLev2ID];
                }
                else {
                    $dispEventType = '-';
                }
                
                $x .= '<tr id="staff_event_row_' . $_rs['StaffEventID'] . '">
							<td style="white-space:nowrap;"><a href="javascript:view_staff_event(\'' . $_rs['StaffEventID'] . '\')">' . $_rs['EventDate'] . '</a></td>
							<td>' . $_rs['StartTime'] . '</td>
                            <td>' . $_rs['StaffName']. '</td>
							<td>' . $dispEventType . '</td>
							<td>' . nl2br($_rs['Remark']) . '</td>
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['event']['RemoveStaffEvent']. '" onClick="removeStaffEvent(' . $_rs['StaffEventID'] . ')"></a></span>
								<input type="hidden" name="SelStaffEventID[]" value="' . $_rs['StaffEventID'] . '"></td>
						</tr>';
            }
            return $x;
        }
     
        function getStaffEventListOfStudentEvent($studentEventID, $viewMode = '', $linkStaffEventDetails = true)
        {
            global $Lang;
            
            $staffEventTypeAry = $this->getStaffEventType();
            $staffEventTypeAssoc = BuildMultiKeyAssoc($staffEventTypeAry, array('EventTypeID'), array('EventTypeName'), $SingleValue = 1);
            $staffEventTypeLev2Ary = $this->getStaffEventTypeLev2();
            $staffEventTypeLev2Assoc = BuildMultiKeyAssoc($staffEventTypeLev2Ary, array('EventTypeID','EventTypeLev2ID'), array('EventTypeLev2Name'), $SingleValue = 1);
            unset($staffEventTypeAry);
            unset($staffEventTypeLev2Ary);
            
            if (is_array($studentEventID)) {
                $cond = " AND se.EventID IN ('" . implode("','", $studentEventID) . "')";
            } else if ($studentEventID) {
                $cond = " AND se.EventID='" . $studentEventID . "'";
            } else {
                $cond = '';
            }
            
            $staffName = getNameFieldByLang('s.');
            $auStaffName = getNameFieldByLang('au.');
            if ($cond != '') {
                $sql = "SELECT 	e.StaffEventID,
                                e.EventDate,
                                DATE_FORMAT(e.StartTime,'%k:%i') as StartTime,
                                IFNULL($staffName,$auStaffName) AS StaffName,
                                e.EventTypeID,
                                e.EventTypeLev2ID,
                                e.Remark
                        FROM
                                MEDICAL_STAFF_EVENT e
                        INNER JOIN
                                MEDICAL_EVENT_STAFF_EVENT se ON se.StaffEventID=e.StaffEventID
                        LEFT JOIN
                                INTRANET_USER s ON s.UserID=e.StaffID
                        LEFT JOIN
                                INTRANET_ARCHIVE_USER au ON au.UserID=e.StaffID
                        WHERE 1" . $cond . "
				        ORDER BY e.EventDate, e.StartTime";
                $staffEvent = $this->objDB->returnResultSet($sql);
            } else {
                return '';
            }
            
            $x = '';
            for ($i = 0, $iMax = count($staffEvent); $i < $iMax; $i ++) {
                $_rs = $staffEvent[$i];
                $_eventTypeID = $_rs['EventTypeID'];
                $_eventTypeLev2ID= $_rs['EventTypeLev2ID'];
                if ($_eventTypeID && $_eventTypeLev2ID) {
                    $dispEventType = $staffEventTypeAssoc[$_eventTypeID] . ' > ' . $staffEventTypeLev2Assoc[$_eventTypeID][$_eventTypeLev2ID];
                }elseif ($_eventTypeID && !$_eventTypeLev2ID) {
                    $dispEventType = $staffEventTypeAssoc[$_eventTypeID];
                }elseif (!$_eventTypeID && $_eventTypeLev2ID) {
                    $dispEventType = $staffEventTypeLev2Assoc[$_eventTypeID][$_eventTypeLev2ID];
                }
                else {
                    $dispEventType = '-';
                }
                
                $x .= '<tr id="staff_event_row_' . $_rs['StaffEventID'] . '">
							<td style="white-space:nowrap;">'.($linkStaffEventDetails? '<a href="javascript:view_staff_event(\'' . $_rs['StaffEventID'] . '\')">' . $_rs['EventDate'] . '</a>' : $_rs['EventDate']) . '</td>
							<td>' . $_rs['StartTime'] . '</td>
                            <td>' . $_rs['StaffName'] . '</td>
							<td>' . $dispEventType. '</td>
							<td>' . nl2br($_rs['Remark']) . '</td>' . ($viewMode ? '' : '
							<td><span class="table_row_tool"><a class="delete" title="' . $Lang['medical']['event']['RemoveStaffEvent']. '" onClick="remove_staff_event_of_student_event(' . $studentEventID . ',' . $_rs['StaffEventID'] . ')"></a></span>
							<input type="hidden" name="SelStaffEventID[]" value="' . $_rs['StaffEventID'] . '"></td>') . '
						</tr>';
            }
            return $x;
        }
        
        # Handover 
        
        function getHandoverRecord($recordID = ''){
            $cols = "RecordID, RecordDate, RecordItemID, Remark";
            $table = "MEDICAL_HANDOVER";
            $cond = " 1 ";
            if($recordID != ''){
                $cond .= "AND RecordID = '$recordID'";
            } else {
                $cond .= "AND isDeleted = '0'";
            }
            $sql = "SELECT $cols FROM $table WHERE $cond";
            $result = $this->objDB->returnArray($sql);
            return $result;
        }
        
        function getHandoverRecordAttachment($recordID){
            if (!is_array($recordID)) {
                $recordID = array($recordID);
            }
            $cols = "AttachmentID, RecordID, AttachmentLink";
            $table = "MEDICAL_HANDOVER_ATTACHMENT";
            $conds = " 1 ";
            $conds .= "AND RecordID IN ('".implode("','", (array)$recordID)."')";
            $sql = "SELECT $cols FROM $table WHERE $conds";
            $result = $this->objDB->returnArray($sql);
            return $result;
        }

        function getHandoverRecordPIC($recordID){
            $firstField = 'iu.'.Get_Lang_Selection('ChineseName','EnglishName');
            $secondField = 'iu.'.Get_Lang_Selection('EnglishName','ChineseName');
            $cols = "iu.UserID, IF($firstField IS NULL OR $firstField = '', $secondField, $firstField) AS UserName";
            $conds = "mhp.RecordID = '{$recordID}'";
            $table = "MEDICAL_HANDOVER_PIC mhp";
            $joinTable = "INNER JOIN INTRANET_USER iu ON mhp.UserID = iu.UserID";
            $sql = "SELECT $cols FROM $table $joinTable WHERE $conds";
//             debug_pr($sql);die();
            $pics = $this->objDB->returnArray($sql);
            return $pics;
        }

        function getHandoverItem($itemID){
            $table = "MEDICAL_HANDOVER_ITEM";
            $cond = "ItemID = '$itemID'";
            
            $sql = "SELECT * FROM $table WHERE $cond";
            $result = $this->objDB->returnArray($sql);
            return $result;
        }
        
        function getHandoverItemByCode($itemCode){
            $table = "MEDICAL_HANDOVER_ITEM";
            $cond = "ItemCode = '$itemCode' AND isDeleted = '0'";
            
            $sql = "SELECT * FROM $table WHERE $cond";
            $result = $this->objDB->returnArray($sql);
            return $result;
        }
        
        function getHandoverItemByName($itemName){
            $table = "MEDICAL_HANDOVER_ITEM";
            $cond = "ItemName = '$itemName' AND isDeleted = '0'";
            
            $sql = "SELECT * FROM $table WHERE $cond";
            $result = $this->objDB->returnArray($sql);
            return $result;
        }
        
        function getHandoverItemList($activate = true){
            $table = "MEDICAL_HANDOVER_ITEM";
            $cond  = " 1 ";
            if($activate){
                $cond .= "AND ItemStatus = '1' ";
            }
            $cond .= "AND isDeleted = '0'";
            $sql = "SELECT * FROM $table WHERE $cond";
            $result = $this->objDB->returnArray($sql);
            return $result;
        }

        function getHandoverStudent($recordID)
        {
            $name_field_s = getNameFieldWithClassNumberByLang('u.');
            $sql = "SELECT 	u.UserID, 
							{$name_field_s} AS UserName 
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							MEDICAL_HANDOVER_STUDENT s ON s.StudentID=u.UserID
					WHERE 
							s.RecordID='$recordID' 
					ORDER BY UserName";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        function getHandoverAttachment($attachmentID)
        {
            $name_field_s = getNameFieldWithClassNumberByLang('u.');
            $sql = "SELECT * FROM MEDICAL_HANDOVER_ATTACHMENT WHERE AttachmentID='$attachmentID'";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        // sort in by EnglishName
        function getHandoverPicList()
        {
            $userName = getNameFieldByLang('u.');
            $sql = "SELECT 	u.UserID, 
							{$userName} AS UserName 
					FROM 
							INTRANET_USER u 
					INNER JOIN 
							MEDICAL_HANDOVER_PIC p ON p.UserID=u.UserID
					GROUP BY u.UserID
					ORDER BY u.EnglishName";
            $rs = $this->objDB->returnResultSet($sql);
            return $rs;
        }

        # End Of Handover
    }

    // close class libMedical
    // /////////////////////////////////////////////////////////////////////////////////////////////
    
    // reportType = 1 => without percentage , 2 => with percentage
    // withRemark == "" don't show remark
    function getMealReport($startDate, $endDate, $studentList, $user_mealStatus, $timePeriod, 
        // $gender, $sleep,
        $reportType = 1, $outputFormat = 'html', $withRemark = "")
    {
        $css = '<style>
					.medica_align_center{
						text-align: center !important ;
					}
				</style>';
        
        global $PATH_WRT_ROOT, $w2_cfg, $Lang, $objMedical;
        $objDB = new libdb();
        
        include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
        $lexport = new libexporttext();
        $Rows = array(); // data rows
        $Details = array(); // one data row
        $HeaderColumn = array(); // header columns
                                 
        // LIST OUT THERE IS HOW MANY MONTH BETWEEN START DATE AND END DATE
        $totalYearMonth = returnListOfYearMonth($startDate, $endDate);
        
        // GET THE RELATED TABLE AND IT MUST BE EXIST
        $logTable = array();
        $recordAry = array(); // store the meal record for all student
        $recordAttendanceAry = array(); // store the attendance record for all student
        if ($withRemark) {
            $remarkAry = array(); // remark for students
        }
        
        for ($i = 0, $iMax = count($totalYearMonth); $i < $iMax; $i ++) {
            $_Table = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $totalYearMonth[$i];
            if (hasDailyLogTableByName($_Table)) {
                $logTable[] = $_Table;
            }
            $_Table = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . $totalYearMonth[$i];
            if (hasDailyLogTableByName($_Table)) {
                $attendanceTable[] = $_Table;
            }
        }
        
        for ($i = 0, $iMax = count($logTable); $i < $iMax; $i ++) {
            $sqlWhere = '';
            $sqlAttendanceWhere = '';
            $rs = array();
            
            $_table_sufix = str_replace($w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"], '', $logTable[$i]);
            
            // change the $_table_sufix from 2013_11 to 2013-11
            $_tmp = str_replace('_', '-', $_table_sufix);
            
            // HANDLE THE SQL FIT WITH THE DATE RANGE
            if (strcmp($_tmp, substr($startDate, 0, 7)) == 0) {
                // the log table match with the reqeust start month
                if (count($totalYearMonth) == 1) {
                    
                    // only involve only one log table , use between , this sql only execute once
                    $_startDay = intval(substr($startDate, 8, 2));
                    $_endDay = intval(substr($endDate, 8, 2));
                    $sqlWhere = ' and (RecordDay >= ' . $_startDay . ' and RecordDay <=' . $_endDay . ')';
                    $sqlAttendanceWhere = ' and (DayNumber >= ' . $_startDay . ' and DayNumber <=' . $_endDay . ')';
                } else {
                    $_startDay = intval(substr($startDate, 8, 2));
                    $sqlWhere = ' and (RecordDay >= ' . $_startDay . ')';
                    $sqlAttendanceWhere = ' and (DayNumber >= ' . $_startDay . ')';
                }
            } else if (strcmp($_tmp, substr($endDate, 0, 7)) == 0) {
                // the log table match with the reqeust end month
                $_endDay = intval(substr($endDate, 8, 2));
                $sqlWhere = ' and (RecordDay <= ' . $_endDay . ')';
                $sqlAttendanceWhere = ' and (DayNumber <= ' . $_endDay . ')';
            }
            $sqlWhere .= ' and MealStatus In (\'' . implode("','", (array) $user_mealStatus) . '\')';
            $sqlWhere .= ' and UserID In (\'' . implode("','", (array) $studentList) . '\')';
            if ($timePeriod != 1) {
                $sqlWhere .= ' and PeriodStatus =\'' . ($timePeriod - 1) . '\'';
            }
            $sqlAttendanceWhere .= ' and UserID In (\'' . implode("','", (array) $studentList) . '\')';
            
            $sql = 'select count(*) as totalRecord, userid, MealStatus from ' . $logTable[$i] . ' where 1 = 1 ' . $sqlWhere . ' group by userid, MealStatus';
            
            $rs = $objDB->returnResultSet($sql);
            
            for ($r = 0, $rMax = count($rs); $r < $rMax; $r ++) {
                $_userId = $rs[$r]['userid'];
                $_mealStatus = $rs[$r]['MealStatus'];
                $_totalRecord = $rs[$r]['totalRecord'];
                $recordAry[$_userId][$_mealStatus] += $_totalRecord;
            }
            
            // FOR ATTENDANCE
            $_attendanceTable = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . $_table_sufix;
            if (hasDailyLogTableByName($_attendanceTable)) {
                $rs = array();
                $sql2 = 'select count(*) as totalRecord, userid from ' . $_attendanceTable . ' where 1 = 1 ' . $sqlAttendanceWhere . ' and (amstatus = 0 or amstatus = 2)  group by userid';
                
                $rs = $objDB->returnResultSet($sql2);
                for ($r = 0, $rMax = count($rs); $r < $rMax; $r ++) {
                    $_userId = $rs[$r]['userid'];
                    $_totalRecord = $rs[$r]['totalRecord'];
                    $recordAttendanceAry[$_userId] += $_totalRecord;
                }
            }
            // END FOR ATTENDANCE
            
            if ($withRemark) {
                // Start getting remarks
                $sql = "SELECT UserID,Remarks,PeriodStatus,RecordDay FROM " . $logTable[$i] . " where 1 = 1 " . $sqlWhere . " AND Remarks is not null AND Remarks<>'' order by UserID";
                
                $rm = $objDB->returnResultSet($sql);
                $jMax = count($rm);
                if ($jMax > 0) {
                    for ($j = 0; $j < $jMax; $j ++) {
                        $_userId = $rm[$j]["UserID"];
                        if (isset($remarkAry[$_userId])) {
                            $prevIndex = count($remarkAry[$_userId]);
                        } else {
                            $prevIndex = 0;
                        }
                        $remarks = $rm[$j]["Remarks"];
                        $day = $rm[$j]["RecordDay"];
                        preg_match("/[0-9]*_0?(\d)*/", $totalYearMonth[$i], $month);
                        $month = $month[1];
                        $period = $rm[$j]["PeriodStatus"];
                        $remarkAry[$_userId][$prevIndex ++] = $day . '/' . $month . '(' . $Lang['medical']['report_general']['PeriodStatus'][$period] . ') : ' . $remarks;
                    }
                }
                // End getting remarks
            }
        }
        
        // $sql = 'select '.getNameFieldByLang2().' as userName ,userId from INTRANET_USER where userid between 1971 and 4102 and recordtype = 2';
        // if($gender != 1){
        // $gender = ($gender=='2')?'M':'F';
        // $conds = ' And gender =\''.$gender.'\'';
        // }
        
        // $sql = 'select '.getNameFieldByLang2('IU.').' as userName ,IU.userId from INTRANET_USER IU Inner Join INTRANET_USER_PERSONAL_SETTINGS IUPS On IUPS.UserID = IU.UserID where IU.userid In (\''. implode("','", (array)$studentList ).'\') And stayOverNight ="'.$sleep.'"'.$conds;
        $sql = 'select 
			' . getNameFieldByLang2('IU.') . ' as userName,
			IU.userId 
		from 
			INTRANET_USER IU 
		where 
			IU.userid In (\'' . implode("','", (array) $studentList) . '\') 
		ORDER BY
			FIELD(IU.userID, \'' . implode("','", (array) $studentList) . '\')';
        $studentList = $objDB->returnResultSet($sql);
        // debug_r($sql);
        $objMealStatus = new mealStatus();
        $mealStatus = $objMealStatus->getAllStatus($whereCriteria = 'recordstatus = 1 And StatusID In ("' . implode('","', $user_mealStatus) . '")');
        
        // ////////////////////////
        // START HANDLE OUTPUT THE RESULT
        // ////////////////////////
        
        // HEADER
        $hHeader = '<tr class="tabletop"><th>' . $Lang['medical']['report_meal']['tableHeader']['name'] . '</th>';
        $HeaderColumn[] = $Lang['medical']['report_meal']['tableHeader']['name'];
        for ($s = 0, $sMax = count($mealStatus); $s < $sMax; $s ++) {
            
            if ($reportType == 2) {
                $_colSpan = ' colspan="2"';
            }
            $hHeader .= '<th class="medica_align_center" ' . $_colSpan . '>' . stripslashes($mealStatus[$s]['StatusName']) . '</th>';
            $HeaderColumn[] = htmlspecialchars_decode(stripslashes($mealStatus[$s]['StatusName']), ENT_QUOTES);
            if ($reportType == 2) {
                $HeaderColumn[] = $Lang['medical']['report_meal']['tableSubHeader']['ratio'];
            }
        }
        
        $hHeader .= '<th class="medica_align_center">' . $Lang['medical']['report_meal']['tableHeader']['totalDayOfPresent'] . '</th>';
        if ($withRemark) {
            $hHeader .= '<th class="medica_align_center">' . $Lang['medical']['report_meal']['tableHeader']['remarks'] . '</th>';
        }
        $hHeader .= '</tr>';
        $HeaderColumn[] = $Lang['medical']['report_meal']['tableHeader']['totalDayOfPresent'];
        if ($withRemark) {
            $HeaderColumn[] = $Lang['medical']['report_meal']['tableHeader']['remarks'];
        }
        
        if ($reportType == 2) {
            $hHeader .= '<tr class="tabletop"><th>&nbsp;</th>';
            for ($s = 0, $sMax = count($mealStatus); $s < $sMax; $s ++) {
                $hHeader .= '<th class="medica_align_center">' . $Lang['medical']['report_meal']['tableSubHeader']['noOfMeal'] . '</th>';
                $hHeader .= '<th class="medica_align_center">' . $Lang['medical']['report_meal']['tableSubHeader']['ratio'] . '</th>';
            }
            
            $hHeader .= '<th>&nbsp;</th>';
            if ($withRemark) {
                $hHeader .= '<th>&nbsp;</th>';
            }
            $hHeader .= '</tr>';
        }
        
        $exportColumn[0] = $HeaderColumn;
        $h = '<table border = "0" class="common_table_list_v30">';
        $h .= $hHeader;
        for ($i = 0, $iMax = count($studentList); $i < $iMax; $i ++) {
            $Details = array();
            // LOOP FOR EACH STUDENT
            $_studentAllRecord = 0;
            
            $h .= '<tr>';
            $h .= '<td>' . $studentList[$i]['userName'] . '</td>';
            $_userId = $studentList[$i]['userId'];
            $Details[] = $studentList[$i]['userName'];
            if (count($recordAry[$_userId]) > 0) {
                $_studentAllRecord = array_sum($recordAry[$_userId]);
            }
            
            // LOOP FOR EACH MEAL STATUS
            for ($s = 0, $sMax = count($mealStatus); $s < $sMax; $s ++) {
                $_percentage = '0';
                $_mealStatus = $mealStatus[$s]['StatusID'];
                $_TotalNumber = ($recordAry[$_userId][$_mealStatus] == '') ? '-' : $recordAry[$_userId][$_mealStatus];
                
                if ($_studentAllRecord != '' || $_studentAllRecord != 0) {
                    $_percentage = $_TotalNumber / $_studentAllRecord * 100;
                }
                $_percentage = sprintf('%0.2f', $_percentage);
                if ($outputFormat == 'html') {
                    if ($_TotalNumber == '-') {
                        $h .= '<td align="center">-</td>';
                    } else {
                        $h .= '<td align="center"><a href="#" data-statusID="' . $_mealStatus . '" data-studentid="' . $_userId . '" data-timePeriod="' . $timePeriod . '"class="dateDetail">' . $_TotalNumber . '</a></td>';
                    }
                } else {
                    $h .= '<td align="center">' . $_TotalNumber . '</td>';
                }
                $Details[] = $_TotalNumber;
                
                if ($reportType == 2) {
                    $h .= '<td align="center">' . $_percentage . '%</td>';
                    $Details[] = $_percentage . '%';
                }
            }
            
            $_attendanceRecord = ($recordAttendanceAry[$_userId] == '') ? '-' : $recordAttendanceAry[$_userId];
            $Details[] = $_attendanceRecord;
            
            $h .= '<td align="center" class="bgColorr">' . $_attendanceRecord . '</td>';
            
            if ($withRemark) {
                $jMax = count($remarkAry[$_userId]);
                $_remarks = "";
                if ($jMax > 0) {
                    // debug_r($remarkAry[$_userId]);
                    for ($j = 0; $j < $jMax; $j ++) {
                        $_remarks .= stripslashes($remarkAry[$_userId][$j]) . "\n";
                    }
                    if (substr($_remarks, - 1) == "\n") {
                        $_remarks = substr($_remarks, 0, strlen($_remarks) - 1);
                    }
                } else {
                    $_remarks = "-";
                }
                $Details[] = $_remarks;
                $h .= '<td align="left" class="bgColorr">' . nl2br($_remarks) . '</td>';
            }
            $h .= '</tr>' . "\n";
            
            $Rows[] = $Details;
        }
        $h .= '</table>';
        
        switch ($outputFormat) {
            case "html":
            case "print":
                return $css . $h;
                break;
            
            default:
            case "csv":
                $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11", 1);
                $lexport->EXPORT_FILE('export.csv', $exportContent);
                return true;
                break;
        }
    }

    // end of function
    function getMealReport2($startDate, $endDate, $studentList, $user_mealStatus, $timePeriod, 
        // $gender, $sleep,
        $reportType = 1, $outputFormat = 'html', $withRemark = "")
    {
        $css = '<style>
					.medica_align_center{
						text-align: center !important ;
					}
				</style>';
        global $PATH_WRT_ROOT, $w2_cfg, $Lang, $linterface, $objMedical;
        $objDB = new libdb();
        
        include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
        $lexport = new libexporttext();
        $Rows = array(); // data rows
        $Details = array(); // one data row
        $HeaderColumn = array(); // header columns
        if ($withRemark) {
            $remarkAry = array(); // remark for students
        }
        
        // LIST OUT THERE IS HOW MANY MONTH BETWEEN START DATE AND END DATE
        $totalYearMonth = returnListOfYearMonth($startDate, $endDate);
        
        // GET THE RELATED TABLE AND IT MUST BE EXIST
        $logTable = array();
        $recordAry = array(); // store the meal record for all student
        $recordAttendanceAry = array(); // store the attendance record for all student
        
        for ($i = 0, $iMax = count($totalYearMonth); $i < $iMax; $i ++) {
            $_Table = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $totalYearMonth[$i];
            if (hasDailyLogTableByName($_Table)) {
                $logTable[] = $_Table;
            }
            $_Table = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"] . $totalYearMonth[$i];
            if (hasDailyLogTableByName($_Table)) {
                $attendanceTable[] = $_Table;
            }
        }
        
        for ($i = 0, $iMax = count($logTable); $i < $iMax; $i ++) {
            $sqlWhere = '';
            $sqlAttendanceWhere = '';
            $rs = array();
            
            $_table_sufix = str_replace($w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"], '', $logTable[$i]);
            
            // change the $_table_sufix from 2013_11 to 2013-11
            $_tmp = str_replace('_', '-', $_table_sufix);
            
            // HANDLE THE SQL FIT WITH THE DATE RANGE
            if (strcmp($_tmp, substr($startDate, 0, 7)) == 0) {
                // the log table match with the reqeust start month
                if (count($totalYearMonth) == 1) {
                    
                    // only involve only one log table , use between , this sql only execute once
                    $_startDay = intval(substr($startDate, 8, 2));
                    $_endDay = intval(substr($endDate, 8, 2));
                    $sqlWhere = ' and (RecordDay >= ' . $_startDay . ' and RecordDay <=' . $_endDay . ')';
                    $sqlAttendanceWhere = ' and (DayNumber >= ' . $_startDay . ' and DayNumber <=' . $_endDay . ')';
                } else {
                    $_startDay = intval(substr($startDate, 8, 2));
                    $sqlWhere = ' and (RecordDay >= ' . $_startDay . ')';
                    $sqlAttendanceWhere = ' and (DayNumber >= ' . $_startDay . ')';
                }
            } else if (strcmp($_tmp, substr($endDate, 0, 7)) == 0) {
                // the log table match with the reqeust end month
                $_endDay = intval(substr($endDate, 8, 2));
                $sqlWhere = ' and (RecordDay <= ' . $_endDay . ')';
                $sqlAttendanceWhere = ' and (DayNumber <= ' . $_endDay . ')';
            }
            $sqlWhere .= ' and MealStatus In (\'' . implode("','", (array) $user_mealStatus) . '\')';
            $sqlWhere .= ' and UserID In (\'' . implode("','", (array) $studentList) . '\')';
            if ($timePeriod != 1) {
                $sqlWhere .= ' and PeriodStatus =\'' . ($timePeriod - 1) . '\'';
            }
            $sqlAttendanceWhere .= ' and UserID In (\'' . implode("','", (array) $studentList) . '\')';
            
            $sql = 'select count(*) as totalRecord, userid, MealStatus from ' . $logTable[$i] . ' where 1 = 1 ' . $sqlWhere . ' group by userid, MealStatus';
            
            $rs = $objDB->returnResultSet($sql);
            
            for ($r = 0, $rMax = count($rs); $r < $rMax; $r ++) {
                $_userId = $rs[$r]['userid'];
                $_mealStatus = $rs[$r]['MealStatus'];
                $_totalRecord = $rs[$r]['totalRecord'];
                $recordAry[$_userId][$_mealStatus] += $_totalRecord;
            }
            
            // FOR ATTENDANCE
            /*
             * $_attendanceTable = $w2_cfg["PREFIX_CARD_STUDENT_DAILY_LOG"].$_table_sufix;
             * if(hasDailyLogTableByName($_attendanceTable))
             * {
             * $rs = array();
             * $sql2 = 'select count(*) as totalRecord, userid from '.$_attendanceTable.' where 1 = 1 '.$sqlAttendanceWhere.' and (amstatus = 0 or amstatus = 2) group by userid';
             *
             * $rs = $objDB->returnResultSet($sql2);
             * for($r = 0,$rMax = count($rs);$r<$rMax;$r++)
             * {
             * $_userId = $rs[$r]['userid'];
             * $_totalRecord = $rs[$r]['totalRecord'];
             * $recordAttendanceAry[$_userId] += $_totalRecord;
             * }
             * }
             */
            // END FOR ATTENDANCE
            
            if ($withRemark) {
                // Start getting remarks
                $sql = "SELECT UserID,Remarks,PeriodStatus,RecordDay FROM " . $logTable[$i] . " where 1 = 1 " . $sqlWhere . " AND Remarks is not null AND Remarks<>'' order by UserID";
                $rm = $objDB->returnResultSet($sql);
                $jMax = count($rm);
                if ($jMax > 0) {
                    for ($j = 0; $j < $jMax; $j ++) {
                        $_userId = $rm[$j]["UserID"];
                        if (isset($remarkAry[$_userId][$period])) {
                            $prevIndex = count($remarkAry[$_userId][$period]);
                        } else {
                            $prevIndex = 0;
                        }
                        $remarks = $rm[$j]["Remarks"];
                        $day = $rm[$j]["RecordDay"];
                        preg_match("/[0-9]*_0?(\d)*/", $totalYearMonth[$i], $month);
                        $month = $month[1];
                        $period = $rm[$j]["PeriodStatus"];
                        $remarkAry[$_userId][$period][] = $day . '/' . $month . ':' . /*'(' . $Lang['medical']['report_general']['PeriodStatus'][$period] . ') ' .*/ $remarks;
                        // $remarkAry[$_userId][$period][$prevIndex++] = $day . '/' . $month . ':' . /*'(' . $Lang['medical']['report_general']['PeriodStatus'][$period] . ') ' .*/ $remarks;
                    }
                }
                // End getting remarks
            }
        }
        
        /*
         * Get the record between start date and end date (include start/end date)
         * e.g. Get the record between ( 15-02-2014 , 14-03-2014 )
         * 1) get the data from MEDICAL_STUDENT_DAILY_MEAL_LOG_2014_02
         * WHERE RecordDay >= 15 AND RecordDay <= 28
         * 2) get the data from MEDICAL_STUDENT_DAILY_MEAL_LOG_2014_03
         * WHERE RecordDay >= 01 AND RecordDay <= 14
         * return
         * $recordsArr => [
         * "StudentID" => [
         * "PeriodStatus" => [ // PeriodStatus: 1=Breakfast,2=Lunch,3=Dinner
         * "month" => [
         * [date] => MealStatus, // MealStatus = MEDICAL_MEAL_STATUS.StatusID
         * [date] => MealStatus,
         * [date] => MealStatus,
         * ...
         * ]
         * ]
         * ]
         * ]
         */
        // ## Get record start ###
        $recordsArr = array();
        $recordsIdArr = array();
        $monthFirstDay = array();
        $monthLastDay = array();
        for ($i = 0, $iMax = count($logTable); $i < $iMax; $i ++) {
            // suppose $totalYearMonth[$i] with format YYYY_MM
            $thisMonth = intval(substr($logTable[$i], - 2));
            
            if ($i == 0) { // Start date of this month
                           // $startDate with format YYYY-MM-DD
                $thisMonthStartDate = substr($startDate, - 2); // Start date is in this month
            } else {
                $thisMonthStartDate = '01'; // Start date is NOT in this month
            }
            if ($i == $iMax - 1) { // End date of this month
                                   // $endDate with format YYYY-MM-DD
                $thisMonthEndDate = substr($endDate, - 2); // End date is in this month
            } else {
                $d = str_replace('_', '-', $logTable[$i]) . '-1';
                $thisMonthEndDate = date('t', strtotime($d)); // End date is NOT in this month
            }
            $monthFirstDay[] = $thisMonthStartDate;
            $monthLastDay[] = $thisMonthEndDate;
            
            $sql = "SELECT MSDML.RecordID, MSDML.UserID,MSDML.RecordDay,MSDML.PeriodStatus,MSDML.MealStatus FROM {$logTable[$i]} MSDML
				WHERE RecordDay >= '{$thisMonthStartDate}' AND RecordDay <= '{$thisMonthEndDate}'";
            $rs = $objDB->returnResultSet($sql);
            foreach ($rs as $record) {
                $recordsArr[$record['UserID']][$record['PeriodStatus']][$thisMonth][$record['RecordDay']] = $record['MealStatus'];
                $recordsIdArr[$record['UserID']][$record['PeriodStatus']][$thisMonth][$record['RecordDay']] = $record['RecordID'];
            }
        }
        ;
        // ## Get Record End ###
        $AcademicYearID = Get_Current_Academic_Year_ID();
        
        $sql = 'select ' . getNameFieldByLang2('IU.') . ' as userName  ,IU.userId, ' . $objMedical->getClassNameByLang2($prefix = "YC.") . ' as ClassName, IUPS.stayOverNight from 
			INTRANET_USER IU 
		Inner Join
			INTRANET_USER_PERSONAL_SETTINGS IUPS 
		On 
			IUPS.UserID = IU.UserID 
		Inner Join
			YEAR_CLASS_USER YCU
		On
			IU.UserID = YCU.UserID
		Inner Join
			YEAR_CLASS YC
		On
			YC.YearClassID = YCU.YearClassID
		WHERE 
			IU.userid In (\'' . implode("','", (array) $studentList) . '\') 
		AND
			YC.AcademicYearID = \'' . $AcademicYearID . '\'
		ORDER BY
			FIELD(IU.userID, \'' . implode("','", (array) $studentList) . '\')';
        $studentList = $objDB->returnResultSet($sql);
        
        $objMealStatus = new mealStatus();
        $mealStatus = array();
        $allMealStatus = $objMealStatus->getAllStatus($whereCriteria = 'recordstatus = 1 And StatusID In ("' . implode('","', $user_mealStatus) . '")');
        foreach ($allMealStatus as $status) {
            $mealStatus[$status['StatusID']] = $status['StatusCode'];
        }
        
        // ////////////////////////
        // START HANDLE OUTPUT THE RESULT
        // ////////////////////////
        $h = '<table border = "0" class="common_table_list_v30">';
        $h .= '<colgroup>
			<col style="width:7%">
			<col style="width:7%">
			<col style="width:4%">
			<col style="width:4%">
			</colgroup>';
        
        // # HEADER ##
        $dateArray = array();
        $monthArray = array();
        $dayArray = array();
        $hHeader = '<tr class="tabletop">';
        foreach ($Lang['medical']['report_meal2']['tableHeader'] as $header) {
            $HeaderColumn[] = $header;
            $hHeader .= '<th class="medica_align_center" nowrap="nowrap">' . $header . '</th>';
        }
        
        // loop date header START
        $currentTime = strtotime($startDate);
        $stopTime = strtotime($endDate);
        while ($currentTime <= $stopTime) {
            $HeaderColumn[] = date('j/n', $currentTime);
            $hHeader .= '<th class="medica_align_center" nowrap="nowrap">' . date('j/n', $currentTime) . '</th>';
            
            $dateArray[] = date('Y-m-d', $currentTime);
            $monthArray[] = date('n', $currentTime);
            $dayArray[] = date('j', $currentTime);
            
            $currentTime = strtotime('+1 day', date($currentTime));
        }
        // loop date header END
        
        if ($withRemark) {
            $hHeader .= '<th class="medica_align_center" nowrap="nowrap">' . $Lang['medical']['report_meal']['tableHeader']['remarks'] . '</th>';
            $HeaderColumn[] = $Lang['medical']['report_meal']['tableHeader']['remarks'];
        }
        
        $hHeader .= '</tr>';
        $h .= $hHeader;
        $exportColumn[0] = $HeaderColumn;
        
        // # DATA ##
        for ($i = 0, $iMax = count($studentList); $i < $iMax; $i ++) {
            // LOOP FOR EACH STUDENT
            $Details = array();
            $h .= '<tr>';
            $h .= '<td>' . $studentList[$i]['ClassName'] . '</td>';
            $Details[] = $studentList[$i]['ClassName'];
            $h .= '<td>' . $studentList[$i]['userName'] . '</td>';
            $Details[] = $studentList[$i]['userName'];
            if ($studentList[$i]['stayOverNight'] == 0) {
                $h .= '<td>' . $Lang['medical']['report_meal']['sleepOption']['StayOut'] . '</td>';
                $Details[] = $Lang['medical']['report_meal']['sleepOption']['StayOut'];
            } else {
                $h .= '<td>' . $Lang['medical']['report_meal']['sleepOption']['StayIn'] . '</td>';
                $Details[] = $Lang['medical']['report_meal']['sleepOption']['StayIn'];
            }
            $_userId = $studentList[$i]['userId'];
            
            if ($timePeriod == 1) { // Display all time period
                                    // Loop FOR timePeriod
                for ($period = 0; $period < 3; $period ++) { // 0=>Breakfast, 1=>Lunch, 2=>Dinner
                    if ($period > 0) {
                        // Start new peroid
                        $h .= '</tr><tr><td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>';
                        $Rows[] = $Details;
                        $Details = array();
                        $Details[] = '';
                        $Details[] = '';
                        $Details[] = '';
                    }
                    $periodStr = $Lang['medical']['meal']['search']['timePeriodOption'][$period];
                    $h .= '<td align="center">' . $periodStr . '</td>';
                    $Details[] = $periodStr;
                    
                    // Loop FOR Day
                    for ($currentDay = 0, $currentDayMax = count($dayArray); $currentDay < $currentDayMax; $currentDay ++) {
                        $data = $mealStatus[$recordsArr[$_userId][$period + 1][$monthArray[$currentDay]][$dayArray[$currentDay]]];
                        $dataID = $recordsIdArr[$_userId][$period + 1][$monthArray[$currentDay]][$dayArray[$currentDay]];
                        $Details[] = $data;
                        if ($data != NULL) {
                            if ($outputFormat == 'html') {
                                $h .= '<td align="center"><a href="#" data-ID="' . $dataID . '"data-date="' . $dateArray[$currentDay] . '" data-studentid="' . $_userId . '" class="dateDetail">' . $data . '</a></td>';
                            } else {
                                $h .= '<td align="center">' . $data . '</td>';
                            }
                        } else {
                            $h .= '<td>&nbsp;</td>';
                        }
                    }
                    
                    if ($withRemark) {
                        $jMax = count($remarkAry[$_userId][$period + 1]);
                        $_remarks = "";
                        if ($jMax > 0) {
                            for ($j = 0; $j < $jMax; $j ++) {
                                $_remarks .= stripslashes($remarkAry[$_userId][$period + 1][$j]) . "\n";
                            }
                            if (substr($_remarks, - 1) == "\n") {
                                $_remarks = substr($_remarks, 0, strlen($_remarks) - 1);
                            }
                        } else {
                            $_remarks = "-";
                        }
                        $Details[] = $_remarks;
                        $h .= '<td align="left" class="bgColorr">' . nl2br($_remarks) . '</td>';
                    }
                    
                    $h .= '</tr>'; // End period
                }
                $Rows[] = $Details;
                $Rows[] = array(
                    ''
                ); // Fill blank line
                
                if ($i < $iMax - 1) { // Fill blank line if it is not the last record
                    $h .= '<tr> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>';
                    for ($currentDay = 0, $currentDayMax = count($dayArray); $currentDay < $currentDayMax; $currentDay ++) {
                        $h .= '<td>&nbsp;</td>';
                    }
                    if ($withRemark) {
                        $h .= '<td>&nbsp;</td>';
                    }
                    $h .= '</tr>';
                }
            } else { // Display single period
                     // $timePeriod = 1 : Error
                     // $timePeriod = 2 : Display Breakfast
                     // $timePeriod = 3 : Display Lunch
                     // $timePeriod = 4 : Display Dinner
                $periodStr = $Lang['medical']['meal']['search']['timePeriodOption'][$timePeriod - 2];
                $h .= '<td align="center">' . $periodStr . '</td>';
                $Details[] = $periodStr;
                
                // Loop FOR Day
                for ($currentDay = 0, $currentDayMax = count($dayArray); $currentDay < $currentDayMax; $currentDay ++) {
                    $data = $mealStatus[$recordsArr[$_userId][$timePeriod - 1][$monthArray[$currentDay]][$dayArray[$currentDay]]];
                    $dataID = $recordsIdArr[$_userId][$timePeriod - 1][$monthArray[$currentDay]][$dayArray[$currentDay]];
                    $Details[] = $data;
                    if ($data != NULL) {
                        if ($outputFormat == 'html') {
                            $h .= '<td align="center"><a href="#" data-ID="' . $dataID . '"data-date="' . $dateArray[$currentDay] . '" data-studentid="' . $_userId . '" class="dateDetail">' . $data . '</a></td>';
                        } else {
                            $h .= '<td align="center">' . $data . '</td>';
                        }
                    } else {
                        $h .= '<td>&nbsp;</td>';
                    }
                }
                if ($withRemark) {
                    $jMax = count($remarkAry[$_userId][$timePeriod - 1]);
                    $_remarks = "";
                    if ($jMax > 0) {
                        for ($j = 0; $j < $jMax; $j ++) {
                            $_remarks .= stripslashes($remarkAry[$_userId][$timePeriod - 1][$j]) . "\n";
                        }
                        if (substr($_remarks, - 1) == "\n") {
                            $_remarks = substr($_remarks, 0, strlen($_remarks) - 1);
                        }
                    } else {
                        $_remarks = "-";
                    }
                    $Details[] = $_remarks;
                    $h .= '<td align="left" class="bgColorr">' . nl2br($_remarks) . '</td>';
                }
                $Rows[] = $Details;
            }
        }
        
        $h .= '</tr>' . "\n";
        $h .= '</table>';
        
        $thickboxHTML = $linterface->Include_Thickbox_JS_CSS();
        $thickboxHTML .= <<<SCRIPT
		<script>
		$('a.dateDetail').click(function(){
				
				selectedDate = $(this).attr('data-date');
				selectedID = $(this).attr('data-ID');
				selectedStudentId = $(this).attr('data-studentid');
				load_dyn_size_thickbox_ip("{$Lang['medical']['bowel']['tableHeader']['Detail']}", 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=800);
		});
		function onloadThickBox() {
//			alert(parDate + ' == '+ parReasonList + ' == '+parStudentId);
			$('div#TB_ajaxContent').load(
				"?t=reports.ajax.getThickBoxForMealReport2",{ 
					'r_studentId': selectedStudentId,
					'r_date': selectedDate,
					'r_ID': selectedID
				},
				function(ReturnData) {
					
				}
			);
		}
		</script>
SCRIPT;
        switch ($outputFormat) {
            case "html":
                return $css . $thickboxHTML . $h;
            case "print":
                return $css . $h;
                break;
            
            default:
            case "csv":
                $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11", 1);
                $lexport->EXPORT_FILE('export.csv', $exportContent);
                return true;
                break;
        }
    }

    // end of function
    
    /*
     * return array => (
     * [Year] => (
     * [Month] => (
     * [Day] => (
     * [Period] = [Remarks]
     * ),
     * [Day] => (
     * [Period] = [Remarks]
     * )
     * )
     * )
     * )
     */
    function getMealReport2ThickboxData($startDate, $endDate, $studentId, $user_mealStatus, $timePeriod)
    {
        global $PATH_WRT_ROOT, $w2_cfg, $Lang, $linterface, $objMedical;
        $objDB = new libdb();
        
        // LIST OUT THERE IS HOW MANY MONTH BETWEEN START DATE AND END DATE
        $totalYearMonth = returnListOfYearMonth($startDate, $endDate);
        
        // GET THE RELATED TABLE AND IT MUST BE EXIST
        $logTable = array();
        $recordAry = array(); // store the meal record for all student
        
        for ($i = 0, $iMax = count($totalYearMonth); $i < $iMax; $i ++) {
            $_Table = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $totalYearMonth[$i];
            if (hasDailyLogTableByName($_Table)) {
                $logTable[] = $_Table;
            }
        }
        
        $data = array();
        for ($i = 0, $iMax = count($logTable); $i < $iMax; $i ++) {
            $sqlWhere = '';
            $rs = array();
            
            $_table_sufix = str_replace($w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"], '', $logTable[$i]);
            
            // change the $_table_sufix from 2013_11 to 2013-11
            $_tmp = str_replace('_', '-', $_table_sufix);
            
            // HANDLE THE SQL FIT WITH THE DATE RANGE
            if (strcmp($_tmp, substr($startDate, 0, 7)) == 0) {
                // the log table match with the reqeust start month
                if (count($totalYearMonth) == 1) {
                    
                    // only involve only one log table , use between , this sql only execute once
                    $_startDay = intval(substr($startDate, 8, 2));
                    $_endDay = intval(substr($endDate, 8, 2));
                    $sqlWhere = ' and (RecordDay >= ' . $_startDay . ' and RecordDay <=' . $_endDay . ')';
                } else {
                    $_startDay = intval(substr($startDate, 8, 2));
                    $sqlWhere = ' and (RecordDay >= ' . $_startDay . ')';
                }
            } else if (strcmp($_tmp, substr($endDate, 0, 7)) == 0) {
                // the log table match with the reqeust end month
                $_endDay = intval(substr($endDate, 8, 2));
                $sqlWhere = ' and (RecordDay <= ' . $_endDay . ')';
            }
            $sqlWhere .= ' and MealStatus In (\'' . $user_mealStatus . '\')';
            $sqlWhere .= ' and UserID In (\'' . $studentId . '\')';
            
            if ($timePeriod > 1) {
                $sqlWhere .= ' and PeriodStatus = \'' . ($timePeriod - 1) . '\'';
            }
            
            // Start getting remarks
            $sql = "SELECT
						UserID,
						Remarks,
						PeriodStatus,
						RecordDay
					FROM
						{$logTable[$i]}
					WHERE 1 = 1 
						{$sqlWhere}
					ORDER BY
						RecordDay,PeriodStatus";
            $rm = $objDB->returnResultSet($sql);
            
            preg_match("/([0-9]*)_([0-9]*)/", $totalYearMonth[$i], $date);
            $year = $date[1];
            $month = $date[2];
            
            $jMax = count($rm);
            if ($jMax > 0) {
                for ($j = 0; $j < $jMax; $j ++) {
                    $remarks = $rm[$j]["Remarks"];
                    $day = str_pad($rm[$j]["RecordDay"], 2, '0', STR_PAD_LEFT);
                    $period = $rm[$j]["PeriodStatus"];
                    
                    $data[$year][$month][$day][$period] = $remarks;
                }
            }
            // End getting remarks
        }
        return $data;
    }

    // End function getMealReport2ThickboxData()
    function getSleepReport($sleepStudentData, $reasonIDList, $outputFormat = 'html')
    {
        global $Lang, $medical_cfg;
        
        if (($outputFormat == 'html') || ($outputFormat == 'print')) {
            $styleHTML = <<<HTML
				<style>
					.totalCount{
						background-color: lightgray !important;
					}
				</style>
HTML;
            echo $styleHTML;
            echo '<table border="0" class="common_table_list_v30">';
            echo '<thead>';
            echo '<tr class="tabletop">';
            echo '<th>' . $Lang['medical']['bowel']['tableHeader']['ClassName'] . '</th>';
            echo '<th>' . $Lang['medical']['bowel']['tableHeader']['name'] . '</th>';
            echo '<th>' . $Lang['medical']['bowel']['tableHeader']['StayOrNot'] . '</th>';
            foreach ($reasonIDList as $reasonData) {
                if ($reasonData['isComment'] != 1) {
                    echo '<th>' . $reasonData['ReasonName'] . '</th>';
                }
            }
            echo '<th>' . $Lang['medical']['report_sleep']['tableHeader']['stayForDays'] . '</th>';
            foreach ($reasonIDList as $reasonData) {
                if ($reasonData['isComment'] == 1) {
                    echo '<th>' . $medical_cfg['studentSleep']['level1']['comments'] . ': ' . $reasonData['ReasonName'] . '</th>';
                }
            }
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach ($sleepStudentData as $key => $sleepStudentList) {
                echo '<tr>';
                echo '<td>' . $sleepStudentList['Name'] . '</td>';
                $totalDaysCount = 0;
                
                foreach ($reasonIDList as $reasonID => $reasonData) {
                    if ($reasonData['isComment'] != 1) {
                        echo '<td>' . $this->nullReplacer($sleepStudentList['reasonIDList'][$reasonID]) . '</td>';
                        $totalDaysCount += $sleepStudentList['reasonIDList'][$reasonID];
                    }
                }
                /*
                 * if($outputFormat =='html') {
                 * echo '<td class="totalCount"><a href="#">'.$totalDaysCount.'</a></td>';
                 * }else{
                 * echo '<td class="totalCount">'.$totalDaysCount.'</td>';
                 * }
                 */
                echo '<td class="totalCount">' . $totalDaysCount . '</td>';
                foreach ($reasonIDList as $reasonID => $reasonData) {
                    if ($reasonData['isComment'] == 1) {
                        echo '<td>' . $this->nullReplacer($sleepStudentList['reasonIDList'][$reasonID]) . '</td>';
                    }
                }
                
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
        } else if ($outputFormat == 'csv') {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
            $lexport = new libexporttext();
            $exportColumn = array();
            
            $header[] = $Lang['medical']['report_meal']['tableHeader']['name'];
            foreach ($reasonIDList as $reasonData) {
                if ($reasonData['isComment'] != 1) {
                    $header[] = $reasonData['ReasonName'];
                }
            }
            $header[] = $Lang['medical']['report_sleep']['tableHeader']['stayForDays'];
            foreach ($reasonIDList as $reasonData) {
                if ($reasonData['isComment'] == 1) {
                    $header[] = $reasonData['ReasonName'];
                }
            }
            $exportColumn = $header;
            
            $Rows = array();
            foreach ($sleepStudentData as $key => $sleepStudentList) {
                $totalDaysCount = 0;
                $RowsDetail = array();
                $RowsDetail[] = $sleepStudentList['Name'];
                foreach ($reasonIDList as $reasonID => $reasonData) {
                    if ($reasonData['isComment'] != 1) {
                        $RowsDetail[] = $sleepStudentList['reasonIDList'][$reasonID];
                        $totalDaysCount += $sleepStudentList['reasonIDList'][$reasonID];
                    }
                }
                $RowsDetail[] = $totalDaysCount;
                foreach ($reasonIDList as $reasonID => $reasonData) {
                    if ($reasonData['isComment'] == 1) {
                        $RowsDetail[] = $sleepStudentList['reasonIDList'][$reasonID];
                    }
                }
                $Rows[] = $RowsDetail;
            }
            
            $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
            $lexport->EXPORT_FILE('export.csv', $exportContent);
        }
    }
}
?>