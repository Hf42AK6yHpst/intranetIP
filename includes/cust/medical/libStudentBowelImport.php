<?php
/*
 * 	Modified:
 * 	Log
 * 	 
 * 	Date: 2017-07-25 [Cameron] create this file
 */
 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libimport.php");
//include_once($PATH_WRT_ROOT."includes/libftp.php");


if (!defined("LIBSTUDENTBOWELIMPORT_DEFINED"))                     // Preprocessor directive
{
	define("LIBSTUDENTBOWELIMPORT_DEFINED", true);
	class libStudentBowelImport
	{

		function ReadStudentBowelImportData($TargetFilePath)
		{
			global $sys_custom;
			
			if (file_exists($TargetFilePath))
			{
				$libimport = new libimporttext();
				$barcodeTitle = $sys_custom['medical']['swapBowelAndSleep'] ? 'SleepBarcode' : 'BowelBarcode';
				$DefaultCsvHeaderArr= array('StudentBarcode', $barcodeTitle, 'RecordDate', 'RecordTime');
				$ColumnPropertyArr = array(1,1,1,1);	// 0 - Ref Field, not for actual import				
				
				$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);

				$CsvHeaderArr = array_shift($CsvData);
				$numOfCsvData = count($CsvData);
				$CsvHeaderWrong = false;
				for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
					if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
						$CsvHeaderWrong = true;
						break;
					}
				}
				$ret = array();		
				$ret["HeaderError"] = $CsvHeaderWrong;
				$ret["NumOfData"] 	= $numOfCsvData;
				$ret["Data"] = $this->ValidateStudentBowelImportData($CsvData);
				return $ret;		
			}
			else
			{
				return false;
			}
		}

		function ValidateStudentBowelImportData($CsvData)
		{
			global $Lang, $medical_cfg;	
			$objMedical = new libMedical();
					
			$ret = array();
			$bowelRecAry = array();	// for checking duplicate record
			$data = array();
			
			$bowelRS = $objMedical->getAllActiveBowelStatus();
			$bowelRS = BuildMultiKeyAssoc($bowelRS, 'BarCode', array('StatusID'), $SingleValue=1);
			
			$AllBowelBarCode = array_keys($bowelRS);
			
			$studentBarcodeAry = array();
			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
			{
				$studentBarcodeAry[]	= $CsvData[$i][0]; // column 0=> Student Barcode
			}
			if (count($studentBarcodeAry)) {
				$studentRS = $objMedical->getUserIDByBarcode($studentBarcodeAry);
				$studentRS = BuildMultiKeyAssoc($studentRS, 'Barcode', array('UserID'), $SingleValue=1);
			}
			else {
				$studentRS = '';
			}
				
			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
			{
				$studentBarcode	= $CsvData[$i][0]; // column 0=> Student Barcode
				$bowelBarcode	= $CsvData[$i][1]; // column 1=> Bowel Barcode
				$recordDate		= $CsvData[$i][2]; // column 2=> Record Date
				$recordTime		= $CsvData[$i][3]; // column 3=> Record Time
				
				$error = array(); // For holding error message in each row
				$validStudent = true;
				$studentID = '';
				
				if (!$studentRS[$studentBarcode]) {
					$validStudent = false;
					array_push($error, $Lang['medical']['studentBowel']['importError']['barcodeNotExist']);
				}
				else {
					$studentID = $studentRS[$studentBarcode];
				}				
				
				////////////////
				//check bowel barcode
				////////////////
				
				if(!in_array($bowelBarcode, $AllBowelBarCode))
				{
					array_push($error, $Lang['medical']['studentBowel']['importError']['bowelBarcodeNotExist']);
				}
				
				////////////////
				//check date 
				////////////////
				$recordDate = $this->convertDateFormat($recordDate);
				if(!$this->checkDateFormat($recordDate))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidDate']);
				}
			
				////////////////
				//check time 
				////////////////
				if($recordTime != ''){
					$cRecordTime = $this->convertTimeFormat($recordTime);
				}
				if(!$this->checkTimeFormat($cRecordTime))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidTime']);
				}
			
				/////////////////
				//check student attendance
				/////////////////
				if(!libMedicalShowInput::absentAllowedInput('bowel')){
					if($validStudent && $objMedical->getAttendanceStatusOfUser($studentID,$recordDate,'AM or PM') == 0)
					{
						$validStudent = false;
						array_push($error, $Lang['medical']['bowel']['import']['invalidAttendance']);
					}
				}
				
				// check duplicate
				if (in_array($studentBarcode.'_'.$bowelBarcode.'_'.$recordDate.'_'.$cRecordTime, $bowelRecAry)) {
					array_push($error, $Lang['medical']['bowel']['import']['duplicateRecord']);
				}
				else {
					$bowelRecAry[] = $studentBarcode.'_'.$bowelBarcode.'_'.$recordDate.'_'.$cRecordTime;
				}
				
				// check if duplicate record in db
				unset($data);
				$data['StudentID'] = $studentID;
				$data['BowelID'] = $bowelRS[$bowelBarcode];
				$data['RecordDate'] = $recordDate;
				$data['RecordTime'] = $cRecordTime.":00";
				$bowelLog = new BowelLog($recordDate.' '.$cRecordTime);
				if ($bowelLog->isBowelLogExist($data)) {
					array_push($error, $Lang['medical']['studentBowel']['importError']['duplicateRecord']);
				}
				
				$pass = !count($error);
				$ret[$i]["pass"] 						= $pass;
				$ret[$i]["rawData"]["StudentID"] 		= $studentID;
				$ret[$i]["rawData"]["BowelID"] 			= $bowelRS[$bowelBarcode];
				$ret[$i]["rawData"]["StudentBarcode"] 	= $studentBarcode;
				$ret[$i]["rawData"]["BowelBarcode"]		= $bowelBarcode;
				$ret[$i]["rawData"]["RecordDate"] 		= $recordDate;
				$ret[$i]["rawData"]["RecordTime"] 		= $cRecordTime;		// converted record time
				$ret[$i]["rawData"]["oRecordTime"] 		= $recordTime;		// original record time
				$ret[$i]["rawData"]["Error"] 			= $this->errorToRawHTML($error);
				
			}
			return $ret;	
		}
	
		function convertDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($a, $b, $c) = explode('-',$RecordDate);
			else 
				list($a, $b, $c) = explode('/',$RecordDate);
			
			if(strlen($a) == 4){
				return sprintf("%04d/%02d/%02d",$a,$b,$c);
			}
				return sprintf("%04d/%02d/%02d",$c,$b,$a);
		}
		
		function checkDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($year, $month, $day) = explode('-',$RecordDate);
			else 
				list($year, $month, $day) = explode('/',$RecordDate);
								
			if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
			{
				if(checkdate($month,$day,$year))
				{
					return true;
				}
			}
			return false;
		}
		
		function convertTimeFormat($RecordTime)
		{
			list($a, $b) = explode(':',$RecordTime);
			if (!ctype_digit($a) || !ctype_digit($b)) {
				return $RecordTime;
			}
			
			$objMedical = new libMedical();
			$interval = $objMedical->getBowelTimeInterval();
			if ($interval != '' && $interval != 0) {
				if ($b % $interval == 0) {
					$minute = $interval * ((int)($b / $interval) + 1)  - 1; 
				}
				else {
					$minute = $interval * (int)ceil($b / $interval) - 1;
				}
				return sprintf("%02d:%02d",$a,$minute);
			}
			else {
				return '';
			}
		}
	
		function checkTimeFormat($RecordTime)
		{
			list($hour, $minutes) = explode(':',$RecordTime);
			if(strlen($hour)==2 && strlen($minutes)==2)
			{
				if($hour>=0 && $hour<=23 && $minutes>=0 && $minutes<=59)
				{
					return true;
				}
			}
			return false;
		}
		
		
		function errorToRawHTML($errorArray)
		{
			if(empty($errorArray))
			{
				return '';
			}
			
			$raw = '*)' . $errorArray[0];
			for($i = 1,$iMax = count($errorArray); $i < $iMax; $i++)
			{
				$raw .= '<br />*)' . $errorArray[$i];
			}
			return $raw;
		}
		
	}//close class
}

?>