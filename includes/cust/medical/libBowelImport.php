<?php
/*
 * 	Modified: 
 * 	Log
 * 
 * 	Date: 2016-07-20 [Cameron]
 * 		- replace split by explode to support php 5.4+ 
 */
 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
//$TargetFilePath = $_POST["TargetFilePath"];


///////////////////////////////
//////////////new file here
///////////////////////////////

if (!defined("LIBBOWELIMPORT_DEFINED"))                     // Preprocessor directive
{
	define("LIBBOWELIMPORT_DEFINED", true);
	class libBowelImport
	{

		function ReadBowelImportData($TargetFilePath)
		{
			if (file_exists($TargetFilePath))
			{
				$libfs = new libfilesystem();
				$libImport = new libimporttext();
				$data = $libfs->file_read_csv($TargetFilePath);
				
				$data = $this->RemoveLastEmptyLines($data);
//debug_r($data);
				if($data === NULL){
					return NULL;
				}
				$cleanData = $this->ProcessRemoveRecord($data);
//debug_r($cleanData);
				
				$dataBlockArr = $this->DivGroup($cleanData);
				$dataArr = $this->RecordMapping($dataBlockArr);
				
				
				$ret["NumOfData"] 	= count($dataArr);
				$ret["Data"] = $this->ValidateStudentSleepImportData($dataArr);
//debug_r($ret);
				return $ret;
			}
			else
			{
				return false;
			}
		}
		
		private function RemoveLastEmptyLines($data)
		{
			while(count($data)>0){
				if($data[count($data)-1] == array()){ // Remove last Empty Records
					unset($data[count($data)-1]);
				}else{
					break;
				}
			}
			if($data == array()){
				return NULL;
			}
			return $data;
		}
		
		private function ProcessRemoveRecord($data)
		{
			global $medical_cfg;
			
			if(count($data)<2){
				return NULL;
			}
			
			$cleanData = array();
			array_push($cleanData, $data[0]);
			$isLastRecordCancel = false;
			for($i=1,$iMax=count($data);$i<$iMax;$i++){
				if($data[$i][0] == $medical_cfg['general']['bowel']['import']['deleteRecordCode']){
					if(!$isLastRecordCancel){
						array_pop($cleanData);
						$isLastRecordCancel = true;
					}
				}else{
					array_push($cleanData, $data[$i]);
					$isLastRecordCancel = false;
				}
			}
			return $cleanData;
		}
		
		private function DivGroup($cleanData)
		{
			$dataBlockArr = array();
			$dataBlock = array();
			
			$dataBlock[] = $cleanData[0];
			$dataLineCount = count($cleanData);
			for($i=1; $i<$dataLineCount; $i++){
				if($this->isStudentBarCodeCorrect($cleanData[$i][0])){
					$dataBlockArr[] = $dataBlock;
					$dataBlock = array();
				}
				
				$dataBlock[] = $cleanData[$i];
			}
			$dataBlockArr[] = $dataBlock;
			return $dataBlockArr;
		}
		
		private function RecordMapping($dataBlockArr)
		{
			$dataSet = array();
			for($i=0,$iMax=count($dataBlockArr);$i<$iMax;$i++){
				$block = $dataBlockArr[$i];
				if($this->isStudentBarCodeCorrect($block[0][0]) && $this->isBowelBarCodeCorrect($block[1][0])){
					// Normal case
					$dataSet[] = array(
						'StudentBarCode' => $block[0][0], 
						'DateTime' => $block[0][2], 
						'Data' => $block[1][0]
					);
					unset($block[0]);
					unset($block[1]);
				}/*else if(!$this->isStudentBarCodeCorrect($block[0][0]) && !$this->isBowelBarCodeCorrect($block[0][0])){
					// All wrong case
					$dataSet[] = array(
						'StudentBarCode' => $block[0][0], 
						'DateTime' => $block[0][2], 
						'Data' => NULL
					);
					unset($block[0]);
				}*/else if($this->isStudentBarCodeCorrect($block[0][0])){
					// Only Student correct case
					$dataSet[] = array(
						'StudentBarCode' => $block[0][0], 
						'DateTime' => $block[0][2], 
						'Data' => $block[1][0]
					);
					unset($block[0]);
					unset($block[1]);
				}
				foreach($block as $b){
					if(!$this->isStudentBarCodeCorrect($b[0]) && !$this->isBowelBarCodeCorrect($b[0])){
						// All wrong case
						$dataSet[] = array(
							'StudentBarCode' => $b[0], 
							'DateTime' => $b[2], 
							'Data' => NULL
						);
					}else{
						$dataSet[] = array(
							'StudentBarCode' => NULL, 
							'DateTime' => NULL, 
							'Data' => $b[0]
						);
					}
				}
				
			}
//debug_r($dataSet);
			return $dataSet;
		}
		
		function ValidateStudentSleepImportData($recordData)
		{
			global $Lang, $medical_cfg;	
			$objMedical = new libMedical();	
			
			$studentDateTimeArray = array(); // For duplicate checking
			$ret = array();
			
			for($i = 0,$iMax = count($recordData);$i < $iMax; $i++)
			{
				$studentBarCode	= $recordData[$i]['StudentBarCode'];
				$dateTime		= $recordData[$i]['DateTime'];
				$data			= $recordData[$i]['Data'];
				$student		= $this->getStudentByStudentBarCode($studentBarCode);
				$bowel			= $this->getBowelByBarCode($data);
				$statusID		= $bowel[0]["StatusID"];
				$statusName		= $bowel[0]["StatusName"];
				$statusCode		= $bowel[0]["StatusCode"];
				$error = array();			

				$dateTime = $this->convertMinutesToPerid($dateTime);
				$date = '20' . substr($dateTime, 0, strpos( $dateTime, ' ') );
				$date = str_replace('/','-',$date);
				$time = substr($dateTime, strpos( $dateTime, ' ')+1 );
				
				/////////////////
				//check student barcode
				/////////////////
				$validStudent = true;
				if($studentBarCode == '' || count($student)==0)
				{
					$validStudent = false;
					array_push($error, $Lang['medical']['bowel']['import']['invalidBarCode']);
				}
				
				/////////////////
				//check date
				/////////////////
				if($date == '' || $time == '' || !$this->checkDateFormat($date) || !$this->checkTimeFormat($time))
				{
					$validStudent = false;
					array_push($error, $Lang['medical']['bowel']['import']['invalidDateTime']);
				}
				
				/////////////////
				//check student attendance
				/////////////////
				if($validStudent && $objMedical->getAttendanceStatusOfUser($student[0]['UserID'],$date,'AM or PM') == 0)
				{
					$validStudent = false;
					array_push($error, $Lang['medical']['bowel']['import']['invalidAttendance']);
				}
				
				/////////////////
				//check data
				/////////////////
				if($data == '' || count($statusName)==0)
				{
					array_push($error, $Lang['medical']['bowel']['import']['invalidRecordBarCode']);
				}
				
				/////////////////
				//check duplicate
				/////////////////
				if( ($studentBarCode != '') && (in_array($studentBarCode.'_'.$data.'_'.$dateTime,$studentDateTimeArray)) )
				{
					array_push($error, $Lang['medical']['bowel']['import']['duplicateRecord']);
				}else{
					$studentDateTimeArray[] = $studentBarCode.'_'.$data.'_'.$dateTime;
				}
				
				
				
				$pass = !count($error);
				$ret[$i]["pass"] 						= $pass;
				$ret[$i]["reason"] 						= $error;
				$ret[$i]["rawData"]["StudentBarCode"]	= $studentBarCode;
				$ret[$i]["rawData"]["DateTimeOriginal"]	= $recordData[$i]['DateTime'];
				$ret[$i]["rawData"]["DateTime"]			= $dateTime;
				$ret[$i]["rawData"]["Date"]				= $date;
				$ret[$i]["rawData"]["Time"]				= $time;
				$ret[$i]["rawData"]["RecordData"] 		= $data;
				$ret[$i]["rawData"]["Error"] 			= $this->errorToRawHTML($error);
				
				
				$ret[$i]["rawData"]["StudentID"]		= $student[0]['UserID'];
				$ret[$i]["rawData"]["StudentName"]		= $student[0]['ChineseName'];
				$ret[$i]["rawData"]["Class"]			= $student[0]['ClassName'];
				$ret[$i]["rawData"]["ClassNumber"]		= $student[0]['ClassNumber'];
				$ret[$i]["rawData"]["RecordDataName"]	= ($statusCode=='')? '' : $statusCode . '-' . $statusName;
				$ret[$i]["rawData"]["BowelID"]			= $statusID;
			}
			return $ret;	
		}
		
		function convertMinutesToPerid($dateTime){
			list($something, $minutes) = explode(':',$dateTime);
			if(!ctype_digit($minutes))
			{
				return $dateTime;
			}
			if($minutes>=0 && $minutes<=29){
				$minutes = 29;
			}else if($minutes>=30 && $minutes <=59){
				$minutes = 59;
			}else{
				return $dateTime;
			}
			return $something . ':' . $minutes;
		}
		
		function checkDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($year, $month, $day) = explode('-',$RecordDate);
			else 
				list($year, $month, $day) = explode('/',$RecordDate);
			if(!ctype_digit($year) || !ctype_digit($month) || !ctype_digit($day))
			{
				return false;
			}
			if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
			{
				/*if(mktime(0,0,0,$month,$day,$year)>time())
				{
					return false;
				}*/
				if(checkdate($month,$day,$year))
				{
					return true;
				}
			}
			return false;
		}
		function checkTimeFormat($RecordTime)
		{
			list($hour, $minutes) = explode(':',$RecordTime);
		
			if(!ctype_digit($hour) || !ctype_digit($minutes))
			{
				return false;
			}
			if(strlen($hour)==2 && strlen($minutes)==2)
			{
			
				/*if(mktime($hour,$minutes,59)>time())
				{
					return false;
				}*/
				if($hour>=0 && $hour<=23 && $minutes>=0 && $minutes<=59)
				{
					return true;
				}
			}
			return false;
		}
		
		function errorToRawHTML($errorArray)
		{
			if(empty($errorArray))
			{
				return '';
			}
			
			$raw = '*)' . $errorArray[0];
			for($i = 1,$iMax = count($errorArray); $i < $iMax; $i++)
			{
				$raw .= '<br />*)' . $errorArray[$i];
			}
			return $raw;
		}
	
		function isStudentBarCodeCorrect($barCode){
			return count($this->getStudentByStudentBarCode($barCode));
		}
		
		function isBowelBarCodeCorrect($barCode){
			return count($this->getBowelByBarCode($barCode));
		}
		
		function getStudentByStudentBarCode($barCode){
			if ($barCode == "")
			{
				return false;
			}
			
			$sql = "Select * From INTRANET_USER WHERE Barcode='$barCode' AND RecordStatus='1'";
			$objdb = new libdb();
			$rs = $objdb->returnResultSet($sql);
//			$rs = $this->objDB->returnResultSet($sql);
			return $rs;
		}
		
		function getBowelByBarCode($barCode){
			if ($barCode == "")
			{
				return false;
			}
			
			$sql = "Select * From MEDICAL_BOWEL_STATUS WHERE BarCode='$barCode' AND RecordStatus='1' AND DeletedFlag='0'";
			$objdb = new libdb();
			$rs = $objdb->returnResultSet($sql);
//			$rs = $this->objDB->returnResultSet($sql);
			return $rs;
		}
		
	}//close class
}


		/**
		 * Groupping the data to an array(High Speed, difficult to maintain)
		function GroupRecord($rawData)
		{
			$dataArr = array();
			
			for($i=0,$iMax=count($rawData);$i<$iMax;){
				$isStudentLine = $this->isStudentBarCodeCorrect($rawData[$i][0]);
				$isBowelLine = $this->isBowelBarCodeCorrect($rawData[$i][0]);
				$isNextStudentLine = $this->isStudentBarCodeCorrect($rawData[$i+1][0]);
				$isNextBowelLine = $this->isBowelBarCodeCorrect($rawData[$i+1][0]);
				
				if($isStudentLine && $isNextBowelLine){
					// This record is student, next record is bowel. 
					$studentBarCode = $rawData[$i][0];
					$dateTime = $rawData[$i][2];
					$data = $rawData[$i+1][0];
					$validState = 'Valid';
					$i+=2;
				}else if($isStudentLine && $isNextStudentLine){
					// This record is student, next record is student. 
					$studentBarCode = $rawData[$i][0];
					$dateTime = $rawData[$i][2];
					$data = '';
					$validState = 'Bowel Empty';
					$i+=1;
				}else if($isStudentLine && !$isNextBowelLine && !$isNextStudentLine){
					// This record is student, next record is ?. 
					$studentBarCode = $rawData[$i][0];
					$dateTime = $rawData[$i][2];
					$data = $rawData[$i+1][0];
					$validState = 'Bowel wrong';
					$i+=2;
				}else if($isBowelLine){
					 // This record is bowel, next record is (don't care). 
					$studentBarCode = '';
					$dateTime = $rawData[$i][2];
					$data = $rawData[$i][0];
					$validState = 'Student Empty';
					$i+=1;
				}else if(!$isStudentLine && !$isBowelLine && $isNextStudentLine){
					 // This record is ?, next record is student. 
					$studentBarCode = $rawData[$i][0];
					$dateTime = $rawData[$i][2];
					$data = '';
					$validState = 'Student wrong,Bowel empty';
					$i+=1;
				}else if(!$isStudentLine && !$isBowelLine && $isNextBowelLine){
					 // This record is ?, next record is student. 
					$studentBarCode = $rawData[$i][0];
					$dateTime = $rawData[$i][2];
					$data = $rawData[$i+1][0];
					$validState = 'Student wrong';
					$i+=2;
				}else{
					 // This record is ?, next record is ?. 
					$studentBarCode = $rawData[$i][0];
					$dateTime = $rawData[$i][2];
					$data = $rawData[$i+1][0];
					$validState = 'Student wrong,Bowel wrong';
					$i+=2;
				}
				
				
				array_push($dataArr,array(
					'StudentBarCode'=>$studentBarCode, 
					'DateTime'=>$dateTime, 
					'Data'=>$data,
					'ValidState'=>$validState
				));
			}
			return $dataArr;
		}
		*/

?>