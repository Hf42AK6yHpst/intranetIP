<?php
//updated by : Pun
/*
 * 	Log
 * 	Date:
 */
if (!defined("LIBMESSAGE_DEFINED"))                     // Preprocessor directive
{
        define("LIBMESSAGE_DEFINED", true);
		class MedicalMessage
        {
			private $objDB;

			public function MedicalMessage(){
				$this->objDB = new libdb();
			}
			
			public function sendMessage($sender, $receiverArr, $message='&nbsp;'){
				$_safeSender = IntegerSafe($sender,'');
				
				$_safeReceiverArr = IntegerSafe($receiverArr);
				$_safeReceiverList = implode(',',$_safeReceiverArr);
				
				$sql = "INSERT INTO MEDICAL_MESSAGE_SEND (Message,Sender,ReceiverList,SendTime)
					VALUES ('{$message}','{$_safeSender}','{$_safeReceiverList}', now())";
				$this->objDB->db_db_query($sql);
				
				foreach($_safeReceiverArr as $r){
					$sql = "INSERT INTO MEDICAL_MESSAGE_RECEIVE (Message,Sender,Receiver,ReceiveTime)
						VALUES ('{$message}','{$_safeSender}','{$r}', now())";
					$this->objDB->db_db_query($sql);
				}
				return true;
			}
			
			public function deleteSendMessage($sendID){
				if(is_array($sendID)){
					$_safeSendID = IntegerSafe($sendID,'');
					$sqlSendIdArr = implode(',',$_safeSendID);
					$sql = "DELETE FROM MEDICAL_MESSAGE_SEND WHERE SendID IN ({$sqlSendIdArr})";
				}else{
					$_safeSendID = IntegerSafe($sendID,',');
					$sql = "DELETE FROM MEDICAL_MESSAGE_SEND WHERE SendID='{$_safeSendID}'";
				}
				return $this->objDB->db_db_query($sql);
			}
			
			public function deleteReceiveMessage($receiveID){
				if(is_array($receiveID)){
					$_safeReceivedIdArr = IntegerSafe($receiveID,'');
					$sqlReceivedId = implode(',',$_safeReceivedIdArr);
					$sql = "DELETE FROM MEDICAL_MESSAGE_RECEIVE WHERE ReceiveID IN ({$sqlReceivedId})";
				}else{
					$_safeReceiveID = IntegerSafe($receiveID,'');
					$sql = "DELETE FROM MEDICAL_MESSAGE_RECEIVE WHERE ReceiveID='{$_safeReceiveID}'";
				}
				return $this->objDB->db_db_query($sql);
			}
			
			/*
			 * Checking for delete send message.
			 * @return true, if user own all the message in $sendIdArr
			 */
			public function checkOwnSendMessage($userID,$sendIdArr){
				$_safeUserID = IntegerSafe($userID,'');
				$_safeSendIdArr = IntegerSafe($sendIdArr);
				$sqlSendId = implode(',',$_safeSendIdArr);
				
				$sql = "SELECT COUNT(SendID) AS TotalCount FROM MEDICAL_MESSAGE_SEND WHERE SendID IN ({$sqlSendId}) AND Sender='$_safeUserID'";
				$rs = $this->objDB->returnResultSet($sql);
				return (count($_safeSendIdArr) == $rs[0]['TotalCount']);
			}
			
			/*
			 * Checking for delete received message.
			 * @return true, if user own all the message in $sendIdArr
			 */
			public function checkOwnReceiveMessage($userID,$receivedIdArr){
				$_safeUserID = IntegerSafe($userID,'');
				$_safeReceivedIdArr = IntegerSafe($receivedIdArr);
				$sqlReceivedId = implode(',',$_safeReceivedIdArr);
				
				$sql = "SELECT COUNT(ReceiveID) AS TotalCount FROM MEDICAL_MESSAGE_RECEIVE WHERE ReceiveID IN ({$sqlReceivedId}) AND Receiver='$_safeUserID'";
				$rs = $this->objDB->returnResultSet($sql);
				return (count($_safeReceivedIdArr) == $rs[0]['TotalCount']);
			}
			
			/*
			 * Count for unread Receive message
			 * @return the number of unread Receive message. 
			 */
			public function getUnreadMessageCount($userID = ''){
				if($userID == ''){
					$userID = $_SESSION['UserID'];
				}
				$sql = "SELECT COUNT(*) AS total FROM MEDICAL_MESSAGE_RECEIVE WHERE Receiver = '{$userID}' AND HasRead = '0'";
				$rs = $this->objDB->returnResultSet($sql);
				return $rs[0]['total'];
			}
		}

}
?>