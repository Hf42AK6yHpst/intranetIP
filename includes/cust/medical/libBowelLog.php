<?php
//updated by : 
/*
 * 	Log
 * 	
 * 	Date:	2018-01-09 [Cameron]
 * 		- add RecordID checking in isBowelLogExist() to check the update content is duplicate to other records in the same table or not
 *  
 * 	Date:	2017-07-26 [Cameron]
 * 		- add function isBowelLogExist()
 * 	
 */
if (!defined("LIBBOWELLOG_DEFINED"))                     // Preprocessor directive
{
        define("LIBBOWELLOG_DEFINED", true);
		class BowelLog
        {
                private $objDB;

				private $RecordID;
				private $UserID;
				private $RecordTime;
				private $BowelID;
				private $Remarks;

				private $InputBy;
				private $DateInput;
				private $DateModified;
				private $ModifiedBy;

				private $OldRecordTime;
				private $OldBowelID;
				private $OldRemarks;

				public function BowelLog($recordTime, $recordID = NULL){
					$this->objDB = new libdb();

					$this->setRecordTime($recordTime);
					if($recordID != ''){
						$this->setRecordID($recordID);
						$this->loadDataFormStorage();
					}
				}

				private function setRecordID($val){$this->RecordID=$val;} // You are not allow to set this variable, use constructor instead.
				public function getRecordID(){return $this->RecordID;}

				public function setUserID($val){$this->UserID=$val;}
				public function getUserID(){return $this->UserID;}

				public function setRecordTime($val){$this->RecordTime=$val;}
				public function getRecordTime(){return $this->RecordTime;}

				public function setBowelID($val){$this->BowelID=$val;}
				public function getBowelID(){return $this->BowelID;}

				public function setRemarks($val){$this->Remarks=$val;}
				public function getRemarks(){return $this->Remarks;}

				public function setDeletedDate($val){$this->DeletedDate=$val;}
				public function getDeletedDate(){return $this->DeletedDate;}

				public function setInputBy($val){$this->InputBy=$val;}
				public function getInputBy(){return $this->InputBy;}
				
				public function setDateInput($val){$this->DateInput=$val;}
				public function getDateInput(){return $this->DateInput;}

				public function setDateModified($val){$this->DateModified=$val;}
				public function getDateModified(){return $this->DateModified;}

				public function setModifiedBy($val){$this->ModifiedBy=$val;}
				public function getModifiedBy(){return $this->ModifiedBy;}

				/* Removed at 09/04/2014, because it is not active-record.
				public function addRecord($userID, $date, 
											$BowelID,$Remarks,
											$InputBy, 
											$recordID=''){
					$this->setUserID($userID);
					$this->setRecordTime($date);
					$this->setBowelID($BowelID);
					$this->setRemarks($Remarks);
					
					$this->setInputBy($InputBy);
					$this->setModifiedBy($InputBy);

					$this->setRecordID($recordID);
				}
				/**/
        
				private function setOldRecordTime($val){$this->OldRecordTime=$val;}
				private function getOldRecordTime(){return $this->OldRecordTime;}

				private function setOldBowelID($val){$this->OldBowelID=$val;}
				private function getOldBowelID(){return $this->OldBowelID;}

				private function setOldRemarks($val){$this->OldRemarks=$val;}
				private function getOldRemarks(){return $this->OldRemarks;}
				
				private function loadDataFormStorage($loadObject=true, $whereCriteria= NULL,$orderCriteria = NULL)
				{
					global $w2_cfg, $cfg_medical;
					$conds = '';
					if($loadObject){
						$conds = 'and RecordID = '.$this->getRecordID();
					}

					if($whereCriteria !== NULL){
							$conds .= ' and ('.$whereCriteria.')';
					}
					
					$orderBy = '';
					if ($orderCriteria !== NULL){
						$orderBy = ' ORDER BY ' . $orderCriteria;
					}

					$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"].date('Y', strtotime($this->getRecordTime())).'_'.date('m', strtotime($this->getRecordTime()));

					$sql = 'Show tables like "'.$tableName.'"';
					
					$rs = $this->objDB->returnResultSet($sql);
			
					if($rs[0] !=''){
						$sql="select 
									RecordID,
									UserID,
									RecordTime,
									Remarks,
									BowelID,
									InputBy,
									DateInput,
									DateModified,
									ModifyBy 
								  from {$tableName} where 1=1 ".$conds . $orderBy;

						$rs = $this->objDB->returnResultSet($sql);
						
					}
					if(count($rs) > 1){
						//since the statusid is the key  ,return resultset cannot more than 1
						  $errMsg = 'SQL Result! support return zero not or one record but failed ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}

					
//					$rs = $this->objDB->returnResultSet($sql);

					if($loadObject){
						if(count($rs) > 1){
							//since the statusid is the key  ,return resultset cannot more than 1
							  $errMsg = 'SQL Result! support return zero not or one record but failed ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
							  return false;
						}
						
						if(count($rs) ==1){
							$rs = current($rs);
							$this->setRecordID($rs['RecordID']);
							$this->setUserID($rs['UserID']);
							$this->setRecordTime($rs['RecordTime']);
							$this->setRemarks($rs['Remarks']);
							$this->setBowelID($rs['BowelID']);
							$this->setInputBy($rs['InputBy']);
							$this->setDateInput($rs['DateInput']);
							$this->setDateModified($rs['DateModified']);
							$this->setModifiedBy($rs['ModifyBy']);	
							
							$this->setOldRecordTime($rs['RecordTime']);
							$this->setOldBowelID($rs['BowelID']);
							$this->setOldRemarks($rs['Remarks']);
							
							return $this;
						}else{
							return NULL;
						}
					}
					else{
						return $rs;	
					}
				}

				public function save($createTable=true)
				{
					if($this->getRecordID()>0){
						$ret = $this->updateRecord();
					}else{
						$ret = $this->insertRecord($createTable);
					}
					return $ret;
				}
				
	
			   /** Unused
				* Return a array with all active status, default is order by status code desc
				* @owner : Fai (20131212)
				* @param : String $orderCriteria (default order by status code desc)
				* @return : Resultset DB array for all active status (not deleted)
				* 
				*
				public function printStudentSleepLogReport($startDate, $endDate, $userIDList='', $BOWELCriteria=''){
					
					if($userIDList!=''){
						$userIDCconds = " UserID In ('".implode("','",$userIDList)."')";
					}
					if($BOWELCriteria!=''){
						$BOWELCconds = " BowelID In ('".implode("','",$BOWELCriteria)."')";
					}
					$whereCriteria= ".
							recordtime >=  '{$startDate} 00:00:00'
						And
							recordtime <= '{$endDate} 23:59:59'
						{$userIDCconds}
						{$BOWELCconds}
					";
					$orderCriteria = 'RecordTime, UserID';
					$rs = $this->loadDataFormStorage($loadObject=false, $whereCriteria,$orderCriteria);
					return $rs;
				}
				/**/
				
				private function insertRecord($createTable){
					global $cfg_medical, $w2_cfg;
					
					$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"].date('Y', strtotime($this->getRecordTime())).'_'.date('m', strtotime($this->getRecordTime()));

					if($createTable){
						$sql = $this->getCreateDatabaseSQL($tableName);
						$result = $this->objDB->db_db_query($sql);
						if($result != 1){
							$errMsg = 'SQL Error! Datebase cannot be created. '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
							alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
							return false;
						}
					}
					
					$DataArr = array();
			
					$DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(),'int');
					$DataArr['RecordTime'] = $this->objDB->pack_value($this->getRecordTime(),'date');
					$DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(),'str');
					$DataArr['BowelID'] = $this->objDB->pack_value($this->getBowelID(),'int');

					if($this->getDateInput() != ''){
						$DataArr['DateInput'] = $this->objDB->pack_value($this->getDateInput(),'date');
					}else{
						$DataArr['DateInput'] = $this->objDB->pack_value('now()','date');
					}					
					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
					$DataArr['ModifyBy'] = $this->objDB->pack_value($this->getModifiedBy(),'int');
					
					$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

					$fieldStr= $sqlStrAry['sqlField'];
					$valueStr= $sqlStrAry['sqlValue'];

					$sql = "Insert Into {$tableName} (".$fieldStr.') Values ('.$valueStr.')';
//debug_r($sql);					
					$success = $this->objDB->db_db_query($sql);
					if($success != 1){
						if(strpos(mysql_error(),'Duplicate entry') !== 0){
							$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
							alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						}
						return false;
					}
					
					$RecordID = $this->objDB->db_insert_id();
					$this->setRecordID($RecordID);
					return $RecordID;
				}

				private function updateRecord(){
					global $cfg_medical,$w2_cfg;

					if( // Update the modified recored only.
							$this->getOldRecordTime() == $this->getRecordTime() &&
							$this->getOldBowelID() == $this->getBowelID() &&
							$this->getOldRemarks() == $this->getRemarks()
					){
						return true;
					}
					
					$DataArr = array();

					$DataArr['RecordID'] = $this->objDB->pack_value($this->getRecordID(),'int');
					$DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(),'int');
					$DataArr['RecordTime'] = $this->objDB->pack_value($this->getRecordTime(),'date');
					$DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(),'str');
					$DataArr['BowelID'] = $this->objDB->pack_value($this->getBowelID(),'int');

					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
					$DataArr['ModifyBy'] = $this->objDB->pack_value($this->getModifiedBy(),'int');

					foreach ($DataArr as $fieldName => $data)
					{
						$updateDetails .= $fieldName."=".$data.",";
					}

					//REMOVE LAST OCCURRENCE OF ",";
					$updateDetails = substr($updateDetails,0,-1);
					
					$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"].date('Y', strtotime($this->getRecordTime())).'_'.date('m', strtotime($this->getRecordTime()));
					
					$sql = "update {$tableName} set ".$updateDetails." where RecordID = ".$this->getRecordID();

					$result = $this->objDB->db_db_query($sql);
								
					if($result != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}

					return $result;

				}
				public function deleteRecord($Date, $RecordID){
					global $cfg_medical, $w2_cfg;
					$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"].date('Y', strtotime($Date)).'_'.date('m', strtotime($Date));
					$sql = "Delete From
								{$tableName}
							Where 
								RecordID = ".$RecordID;
					
					$result = $this->objDB->db_db_query($sql);
								
					if($result != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}

					return $result;					
				}
	
				/*	Purpose:	Get BowelLog of specified date by StudentID
				 * 	@param : 	$sid 		- INTRANET_USER.UserID
				 * 				$date		- Specific date
				 * 				$filter		- Filter without leading "AND"
				 * 				$orderBy	- Sorting
				 * 	@return : 	array of BowelLog object
				 */
				public function getBowelLogByStudentID($sid,$date,$filter=null,$orderBy = "RecordTime")
				{
					global $w2_cfg;
					if ( (!$sid) || (!$date) || (strlen($date) != 10) )
					{
						return false;
					}	
					$datePart = substr($date,0,4) . "_" . substr($date,5,2);
					
					$bowelLogTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] ."{$datePart}";
					$bowelLogTableExists = hasDailyLogTableByName($bowelLogTableName);
					if ($bowelLogTableExists)
					{
						$sql = "SELECT * FROM $bowelLogTableName WHERE UserID=" . $sid;
						$sql .= " AND date(RecordTime)='" . $date . "'";
						if ($filter)
						{
							$sql .= " AND " . $filter;
						}	
						$sql .= " ORDER BY " . $orderBy;
						$rs = $this->objDB->returnResultSet($sql);
						
						$ret = array();
						$nrRec = count($rs);
						if ($nrRec > 0)
						{
							foreach ($rs as $r)
							{
								$bowelObj = new BowelLog($r['RecordTime'], $r['RecordID']);
								$bowelObj->setUserID($r['UserID']);
								$bowelObj->setRecordTime($r['RecordTime']);
								$bowelObj->setRemarks($r['Remarks']);
								$bowelObj->setBowelID($r['BowelID']);
								$bowelObj->setInputBy($r['InputBy']);
								$bowelObj->setDateInput($r['DateInput']);
								$bowelObj->setDateModified($r['DateModified']);
								$bowelObj->setModifiedBy($r['ModifyBy']);
								
								$ret[] = $bowelObj;
								
							}
						}
						return $ret;
					}
					else
					{
						return false;
					}					
				}			
        /* Don't use Temp table any more
				public function createTempTable($tableName,$date){
					$sql = $this->getCreateDatabaseSQL($tableName,true,$date);
//debug_r($sql);
					$result = $this->objDB->db_db_query($sql);
					return $result;
				}
				public function dropTempTable($tableName){
					$sql = "DROP TABLE TEMP_{$tableName}";
					$result = $this->objDB->db_db_query($sql);
					return $result;
				}
				public function getDataFromTempTable($tableName,$recordID){
					$sql = "SELECT * FROM TEMP_{$tableName} WHERE RecordID = '{$recordID}'";
//debug_r($sql);
					$result = $this->objDB->returnResultSet($sql);
					return $result;
				}
		/**/
				public function removeDateRecord($tableName,$date){
					$sql = "DELETE FROM {$tableName} WHERE 
							RecordTime >=  '{$date} 00:00:00'
						And
							RecordTime <= '{$date} 23:59:59'"; 
					$result = $this->objDB->db_db_query($sql);
					return $result;
				}
				
				public function getCreateDatabaseSQL($tableName/*,$isTemporary = false, $date = ''*/)
				{
					/* Don't use Temp table any more
					if($isTemporary){
						return 
						"CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_{$tableName} AS 
							(SELECT * FROM {$tableName} WHERE 
							RecordTime >=  '{$date} 00:00:00'
						And
							RecordTime <= '{$date} 23:59:59')"; 
//							ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
					}else{
					*/
						return 
						"CREATE TABLE IF NOT EXISTS {$tableName} (
							RecordID int(11) NOT NULL auto_increment,
							UserID int(11) NOT NULL,
							
							RecordTime datetime default NULL,
							BowelID int(11) default NULL,
							Remarks mediumtext,

							DateInput datetime default NULL,
							InputBy int(11) default NULL,							
							DateModified datetime default NULL,
							ModifyBy int(11) default NULL,

							PRIMARY KEY (RecordID),
							UNIQUE KEY UserID_RecordTime_BowelID (UserID,RecordTime,BowelID)
						) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
					//}
				}
				
				// $para: $data = array ('StudentID','BowelID', 'RecordDate', 'RecordTime')
				// return true if record exist and false otherwise
				// if pass RecordID, check if it duplicate to other records
				public function isBowelLogExist($data)
				{
					global $w2_cfg;
					if ( (!$data['StudentID']) || (!$data['BowelID']) || (!$data['RecordDate']) || (!$data['RecordTime']) )
					{
						return false;
					}	
					$datePart = substr($data['RecordDate'],0,4) . "_" . substr($data['RecordDate'],5,2);
					
					$bowelLogTableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"] ."{$datePart}";
					$bowelLogTableExists = hasDailyLogTableByName($bowelLogTableName);
					
					if ($bowelLogTableExists)
					{
						$cond = $data['RecordID'] ? " AND RecordID<>'".$data['RecordID']."'" : "";
						
						$sql = "SELECT RecordID FROM $bowelLogTableName WHERE UserID=" . $data['StudentID'];
						$sql .= " AND RecordTime='" . $data['RecordDate'] . " ".$data['RecordTime']."'";
						$sql .= " AND BowelID='".$data['BowelID']."'".$cond;
						$rs = $this->objDB->returnResultSet($sql);
						
						return count($rs) ? true : false;
					}
					else
					{
						return false;
					}					
				}			
				
		}

}
?>