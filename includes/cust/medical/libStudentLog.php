<?php
//using:   
/*
 * 		Log
 *
 *		2017-09-18 [Cameron]
 *			- fix typo in convertLev3ToName() 
 *
 * 		2015-12-04 [Cameron]
 * 			- remove pass by reference in save() function to support php 5.4
 * 			- return StudentLogID in insertRecord()
 *  	
 * 		2015-09-18 [Cameron]
 * 			- fix typing error $cfg_medical in StudentLogMapper->insertRecord() & updateRecord()
 * 			- add checking if StudentLogID, Level3ID and Level4ID are null in StudentLogPartsMapper->save() function
 */

if (!defined("LIBSTUDENTLOG_DEFINED"))                     // Preprocessor directive
{
        define("LIBSTUDENTLOG_DEFINED", true);

		class StudentLogMapper
		{
			private $objDB;

			public function StudentLogMapper()
			{
				$this->objDB = new libdb();
			}

			public function findByStudentId($studentId , $parStartDate = null,$parEndDate = null,$orderBy = null)
			{
			    global $sys_custom;
				$whereCriteria = '';
				
				$orderBy = ($orderBy == '') ? ' recordtime ' : $orderBy;

				if($studentId > 0 && $studentId != '')
				{
					
					if($parStartDate != ''){
						$parEndDate = ($parEndDate == '')?$parStartDate:$parEndDate;

//						$whereCriteria .= '  and (recordtime between( \''.$parStartDate.' 00:00:00\' and \''.$parEndDate.' 23:59:59\' ))';
						$whereCriteria .= "  and (recordtime >= '{$parStartDate} 00:00:00' and recordtime <= '{$parEndDate} 23:59:59')";
					}
					include_once ('libMedical.php');
					$objMedical = new libMedical();
					if($sys_custom['medical']['accessRight']['ViewAll'] && !($objMedical->isSuperAdmin($_SESSION['UserID']) || $objMedical->checkAccessRight($_SESSION['UserID'] ,'STUDENTLOG_VIEWALL'))){
					    $whereCriteria .= " AND InputBy = '{$_SESSION['UserID']}' ";
					}
					
					$sql = 'select RecordID,UserID,RecordTime,BehaviourOne,BehaviourTwo,Duration,Remarks,PIC,DateInput,InputBy,DateModified,ModifyBy from MEDICAL_STUDENT_LOG where UserID = '.$studentId.' '.$whereCriteria.' order by '.$orderBy;
					$rs = $this->objDB->returnResultSet($sql);
					if(count($rs) > 0)
					{
						$returnAry = array();
						for($i = 0,$iMax = count($rs);$i < $iMax; $i++)
						{

							$returnAry[] = $this->createObject($rs[$i]);
						}
						return $returnAry;
					}
					else
					{
						return NULL;
					}
				}
				else
				{
					return NULL;
				}

			}
			//must with "&" before $objStudentLog (pass  by reference), otherwise , $objStudentLog cannot get back the record id
			public function save(StudentLog $objStudentLog)
			{
				$id = $objStudentLog->getRecordID();

				if($id != '' && $id > 0)
				{
					//with a id before , update record with "recordId"
					$result = $this->updateRecord($objStudentLog);
				}else
				{
					$result = $this->insertRecord($objStudentLog);
				}

				return $result;
			}
			private function updateRecord(StudentLog $objStudentLog)
			{
				global $cfg_medical;
				$DataArr = array();

				$DataArr['UserID'] = $this->objDB->pack_value($objStudentLog->getUserID(),'int');
				$DataArr['RecordTime'] = $this->objDB->pack_value($objStudentLog->getRecordTime(),'date');
				$DataArr['BehaviourOne'] = $this->objDB->pack_value($objStudentLog->getBehaviourOne(),'int');
				$DataArr['BehaviourTwo'] = $this->objDB->pack_value($objStudentLog->getBehaviourTwo(),'int');
				$DataArr['Duration'] = $this->objDB->pack_value($objStudentLog->getDuration(),'date');
				$DataArr['Remarks'] = $this->objDB->pack_value($objStudentLog->getRemarks(),'str');
				$DataArr['PIC'] = $this->objDB->pack_value($objStudentLog->getPIC(),'str');
				$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
				$DataArr['ModifyBy'] = $this->objDB->pack_value($objStudentLog->getModifyBy(),'int');


				foreach ($DataArr as $fieldName => $data)
				{
					$updateDetails .= $fieldName."=".$data.",";
				}

				//REMOVE LAST OCCURRENCE OF ",";
				$updateDetails = substr($updateDetails,0,-1);

				$sql = "update MEDICAL_STUDENT_LOG set ".$updateDetails." where RecordID = ".$objStudentLog->getRecordID();

				$success = $this->objDB->db_db_query($sql);
				if($success != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			        alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					 return false;
				}

				return $success;

			}
			private function insertRecord(StudentLog $objStudentLog)
			{
				global $cfg_medical;
				$DataArr = array();

				$DataArr['UserID'] = $this->objDB->pack_value($objStudentLog->getUserID(),'int');
				$DataArr['RecordTime'] = $this->objDB->pack_value($objStudentLog->getRecordTime(),'date');
				$DataArr['BehaviourOne'] = $this->objDB->pack_value($objStudentLog->getBehaviourOne(),'int');
				$DataArr['BehaviourTwo'] = $this->objDB->pack_value($objStudentLog->getBehaviourTwo(),'int');
				$DataArr['Duration'] = $this->objDB->pack_value($objStudentLog->getDuration(),'date');
				$DataArr['Remarks'] = $this->objDB->pack_value($objStudentLog->getRemarks(),'str');
				$DataArr['PIC'] = $this->objDB->pack_value($objStudentLog->getPIC(),'str');
				$DataArr['DateInput'] = $this->objDB->pack_value('now()','date');
				$DataArr['InputBy'] = $this->objDB->pack_value($objStudentLog->getInputBy(),'int');
				$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
				$DataArr['ModifyBy'] = $this->objDB->pack_value($objStudentLog->getModifyBy(),'int');
			
				$sql = 'Insert Into MEDICAL_STUDENT_LOG ('.implode(',',array_keys($DataArr)).') Values ('.implode(',',array_values($DataArr)).')';

				$success = $this->objDB->db_db_query($sql);

				if($success != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			        alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					 return false;
				}
									
				$RecordID = $this->objDB->db_insert_id();
				$objStudentLog->setRecordID($RecordID);
				
				return $RecordID ? $RecordID : $success;
			}

			private function createObject($ary)
			{
				$objStudentLog = new StudentLog();
				$objStudentLog->setRecordID($ary['RecordID']);
				$objStudentLog->setUserID($ary['UserID']);
				$objStudentLog->setRecordTime($ary['RecordTime']);
				$objStudentLog->setBehaviourOne($ary['BehaviourOne']);
				$objStudentLog->setBehaviourTwo($ary['BehaviourTwo']);
				$objStudentLog->setDuration($ary['Duration']);
				$objStudentLog->setRemarks($ary['Remarks']);
				$objStudentLog->setPIC($ary['PIC']);
				$objStudentLog->setDateInput($ary['DateInput']);
				$objStudentLog->setInputBy($ary['InputBy']);
				$objStudentLog->setDateModified($ary['DateModified']);
				$objStudentLog->setModifyBy($ary['ModifyBy`']);
				return $objStudentLog;
			}
		}

		class StudentLog
        {
			private $RecordID;
			private $UserID;
			private $RecordTime;
			private $BehaviourOne;
			private $BehaviourTwo;
			private $Duration;
			private $Remarks;
			private $PIC;
			private $DateInput;
			private $InputBy;
			private $DateModified;
			private $ModifyBy;

			
			public function StudentLog($recordID = '')
			{
				if($recordID == ''){
					return;
				}
				$objdb =  new libdb();
				$sql = 'select RecordID,UserID,RecordTime,BehaviourOne,BehaviourTwo,Duration,Remarks,PIC,DateInput,InputBy,DateModified,ModifyBy from MEDICAL_STUDENT_LOG where RecordID = '.$recordID;
				$rs = $objdb->returnResultSet($sql);
				$ary = $rs[0];
				
				$this->setRecordID($ary['RecordID']);
				$this->setUserID($ary['UserID']);
				$this->setRecordTime($ary['RecordTime']);
				$this->setBehaviourOne($ary['BehaviourOne']);
				$this->setBehaviourTwo($ary['BehaviourTwo']);
				$this->setDuration($ary['Duration']);
				$this->setRemarks($ary['Remarks']);
				$this->setPIC($ary['PIC']);
				$this->setDateInput($ary['DateInput']);
				$this->setInputBy($ary['InputBy']);
				$this->setDateModified($ary['DateModified']);
				$this->setModifyBy($ary['ModifyBy']);
			}

		
			public function setRecordID($val){$this->RecordID=$val;} 
			public function getRecordID(){return $this->RecordID;}

			public function setUserID($val){$this->UserID=$val;} 
			public function getUserID(){return $this->UserID;}

			public function setRecordTime($val){$this->RecordTime=$val;} 
			public function getRecordTime(){return $this->RecordTime;}

			public function setBehaviourOne($val){$this->BehaviourOne=$val;} 
			public function getBehaviourOne(){return $this->BehaviourOne;}

			public function setBehaviourTwo($val){$this->BehaviourTwo=$val;} 
			public function getBehaviourTwo(){return $this->BehaviourTwo;}

			public function setDuration($val){$this->Duration=$val;} 
			public function getDuration(){return $this->Duration;}

			public function setRemarks($val){$this->Remarks=$val;} 
			public function getRemarks(){return $this->Remarks;}

			public function setPIC($val){$this->PIC=$val;} 
			public function getPIC(){return $this->PIC;}

			public function setDateInput($val){$this->DateInput=$val;} 
			public function getDateInput(){return $this->DateInput;}

			public function setInputBy($val){$this->InputBy=$val;} 
			public function getInputBy(){return $this->InputBy;}

			public function setDateModified($val){$this->DateModified=$val;} 
			public function getDateModified(){return $this->DateModified;}

			public function setModifyBy($val){$this->ModifyBy=$val;} 
			public function getModifyBy(){return $this->ModifyBy;}
			
			public static function convertBehaviourOneToName($val){
				$sql = 
				"Select Lev1Name From MEDICAL_STUDENT_LOG_LEV1 Where Lev1ID = '{$val}'";
				return $this->objDB->returnResultSet($sql);
			}
			public static function convertBehaviourTwoToName($val){
				$sql = 
				"Select Lev2Name From MEDICAL_STUDENT_LOG_LEV2 Where Lev2ID = '{$val}'";
				return $this->objDB->returnResultSet($sql);
			}
		}

		class StudentLogPartsMapper
		{
			private $objDB;
			private $partsAry; // array of object StudentLogParts
			private $StudentLogID; // Reference to table : MEDICAL_STUDENT_LOG.RecordID
			
			public function StudentLogPartsMapper($studentLogId)
			{
				$this->objDB = new libdb();
				$this->setStudentLogID($studentLogId); 
			}

			public function setStudentLogID($val){$this->StudentLogID=$val;} 
			public function getStudentLogID(){return $this->StudentLogID;}

			
			public function addParts(StudentLogParts $objparts)
			{
				$this->partsAry[] = $objparts;
			}

			public function save()
			{
				global $cfg_medical;
				$error = 0;
				if($this->getStudentLogID() > 0){
					//for safe , to the follow only if studentLogID is integer 

					$this->objDB->Start_Trans();

					$sql = 'delete from  MEDICAL_STUDENT_LOG_PARTS where StudentLogID = '.$this->getStudentLogID();

					$success = $this->objDB->db_db_query($sql);
					if($success != 1){
						$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
						alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						++$error;
					}
					
					$sqlInsertAry = null;
					$success = null;
					for($i =0,$iMax = count($this->partsAry);$i < $iMax; $i++)
					{
						//must init this value
						$insertDataAry = null;
						
						$_objStudentLogParts = $this->partsAry[$i];

						// 2015-09-18: add checking if StudentLogID, Level3ID and Level4ID are null 		
						if ($this->getStudentLogID() && $_objStudentLogParts->getLevel3ID() && $_objStudentLogParts->getLevel4ID()) {
							$insertDataAry[] = $this->getStudentLogID();
							$insertDataAry[] = $_objStudentLogParts->getLevel3ID();
							$insertDataAry[] = $_objStudentLogParts->getLevel4ID();
							$insertDataAry[] = $_objStudentLogParts->getDateInput();
							$insertDataAry[] = $_objStudentLogParts->getInputBy();
							$insertDataAry[] = $_objStudentLogParts->getDateModified();
							$insertDataAry[] = $_objStudentLogParts->getModifyBy();
							
							//concat a sql insert string eg. (12, now(),234,...)
							$sqlInsertAry[] = ' ('.implode(',',$insertDataAry).') ';
						}
					}

					if(count($sqlInsertAry) >0)
					{
					
						$sql = 'INSERT INTO `MEDICAL_STUDENT_LOG_PARTS`
								(
								`StudentLogID`,
								`Level3ID`,
								`Level4ID`,
								`DateInput`,
								`InputBy`,
								`DateModified`,
								`ModifyBy`)
								VALUES'.implode(',',$sqlInsertAry);

						$success = $this->objDB->db_db_query($sql);
						
						if($success != 1){
							$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
							alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
							++$error;
						}
					}
					
					if ( $error > 0 )
					{
						$this->objDB->RollBack_Trans();
						return false;
					}
					else {
						$this->objDB->Commit_Trans();
						return true;
					}
				}else{
					return false;
				}

				
			}

		}

		class StudentLogParts
		{
			private $objDB;

			private $RecordID;
			private $StudentLogID; 
			private $Level3ID;
			private $Level4ID;
			private $DateInput;
			private $InputBy;
			private $DateModified;
			private $ModifyBy;

			public function StudentLogParts(){
				$this->objDB = new libdb();
			}
			public function setRecordID($val){$this->RecordID=$val;} 
			public function getRecordID(){return $this->RecordID;}

			public function setStudentLogID($val){$this->StudentLogID=$val;} 
			public function getStudentLogID(){return $this->StudentLogID;}

			public function setLevel3ID($val){$this->Level3ID=$val;} 
			public function getLevel3ID(){return $this->Level3ID;}

			public function setLevel4ID($val){$this->Level4ID=$val;} 
			public function getLevel4ID(){return $this->Level4ID;}

			public function setDateInput($val){$this->DateInput=$val;} 
			public function getDateInput(){return $this->DateInput;}

			public function setInputBy($val){$this->InputBy=$val;} 
			public function getInputBy(){return $this->InputBy;}

			public function setDateModified($val){$this->DateModified=$val;} 
			public function getDateModified(){return $this->DateModified;}

			public function setModifyBy($val){$this->ModifyBy=$val;} 
			public function getModifyBy(){return $this->ModifyBy;}

			public function convertLev3ToName($val){
				$sql = 
				"Select Lev3Name From MEDICAL_STUDENT_LOG_LEV3 Where Lev3ID = '{$val}'";
				return $this->objDB->returnResultSet($sql);
			}
			public function convertLev4ToName($val){
				$sql = 
				"Select Lev4Name From MEDICAL_STUDENT_LOG_LEV4 Where Lev4ID = '{$val}'";
				return $this->objDB->returnResultSet($sql);
			}

		}
}
?>