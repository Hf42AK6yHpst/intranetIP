<?php
// updated by :
/*
 * Log
 *
 * Date: 2018-05-02 [Cameron]
 * - fix: use lang for ej in getHTMLSelection()
 * 
 * Date: 2018-04-27 [Cameron]
 * - fix: apply stripslashes() to Lev2Name in getHTMLSelection()
 *
 * Date: 2017-12-08 [Cameron]
 * - show "Please Select" when $levelOneId=-1 in getHTMLSelection()
 *
 * Date: 2013-12-18 [Cameron]
 */
if (! defined("LIBSTUDENTLOGLEV2_DEFINED")) // Preprocessor directive
{
    define("LIBSTUDENTLOGLEV2_DEFINED", true);

    class studentLogLev2
    {

        private $objDB;

        private $Lev2ID;

        private $Lev2Name;

        private $Lev2Code;

        private $Lev1ID;

        private $ItemQty;

        private $IsDefault;

        private $RecordStatus;

        private $DeletedFlag;

        private $DeletedBy;

        private $DeletedDate;

        private $InputBy;

        private $DateInput;

        private $DateModified;

        private $LastModifiedBy;

        public function studentLogLev2($lev2ID = NULL)
        {
            $this->objDB = new libdb();
            
            if ($lev2ID != '') {
                $this->setLev2ID($lev2ID);
                $this->loadDataFormStorage();
            }
        }

        public function setLev2ID($val)
        {
            $this->Lev2ID = $val;
        }

        public function getLev2ID()
        {
            return $this->Lev2ID;
        }

        public function setLev2Name($val)
        {
            $this->Lev2Name = $val;
        }

        public function getLev2Name()
        {
            return $this->Lev2Name;
        }

        public function setLev2Code($val)
        {
            $this->Lev2Code = $val;
        }

        public function getLev2Code()
        {
            return $this->Lev2Code;
        }

        public function setLev1ID($val)
        {
            $this->Lev1ID = $val;
        }

        public function getLev1ID()
        {
            return $this->Lev1ID;
        }

        public function setItemQty($val)
        {
            $this->ItemQty = $val;
        }

        public function getItemQty()
        {
            return $this->ItemQty;
        }

        public function setIsDefault($val)
        {
            $this->IsDefault = $val;
        }

        public function getIsDefault()
        {
            return $this->IsDefault;
        }

        public function setRecordStatus($val)
        {
            $this->RecordStatus = $val;
        }

        public function getRecordStatus()
        {
            return $this->RecordStatus;
        }

        public function setDeletedFlag($val)
        {
            $this->DeletedFlag = $val;
        }

        public function getDeletedFlag()
        {
            return $this->DeletedFlag;
        }

        public function setDeletedBy($val)
        {
            $this->DeletedBy = $val;
        }

        public function getDeletedBy()
        {
            return $this->DeletedBy;
        }

        public function setDeletedDate($val)
        {
            $this->DeletedDate = $val;
        }

        public function getDeletedDate()
        {
            return $this->DeletedDate;
        }

        public function setInputBy($val)
        {
            $this->InputBy = $val;
        }

        public function getInputBy()
        {
            return $this->InputBy;
        }

        public function setDateInput($val)
        {
            $this->DateInput = $val;
        }

        public function getDateInput()
        {
            return $this->DateInput;
        }

        public function setDateModified($val)
        {
            $this->DateModified = $val;
        }

        public function getDateModified()
        {
            return $this->DateModified;
        }

        public function setLastModifiedBy($val)
        {
            $this->LastModifiedBy = $val;
        }

        public function getLastModifiedBy()
        {
            return $this->LastModifiedBy;
        }

        private function loadDataFormStorage($loadObject = true, $whereCriteria = NULL, $orderCriteria = NULL)
        {
            $conds = '';
            if ($loadObject) {
                $conds = 'and Lev2ID = ' . $this->getLev2ID();
            }
            
            if ($whereCriteria !== NULL) {
                $conds .= ' and (' . $whereCriteria . ')';
            }
            
            $orderBy = '';
            if ($orderCriteria !== NULL) {
                $orderBy = ' ORDER BY ' . $orderCriteria;
            }
            
            $sql = 'select 
							  Lev2ID,
							  Lev2Name,
							  Lev2Code,
							  Lev1ID,
							  ItemQty,
							  IsDefault,
							  RecordStatus,
							  DeletedFlag,
							  DeletedBy,
							  DeletedDate,
							  InputBy,
							  DateInput,
							  DateModified,
							  LastModifiedBy 
						  from MEDICAL_STUDENT_LOG_LEV2 where 1=1 ' . $conds . $orderBy;
            $rs = $this->objDB->returnResultSet($sql);
            // debug_r($rs);
            if ($loadObject) {
                if (count($rs) > 1) {
                    // since the statusid is the key ,return resultset cannot more than 1
                    $errMsg = 'SQL Result! support return zero not or one record but failed [' . count($rs) . '] ' . $sql . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                    alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                    return false;
                }
                
                if (count($rs) == 1) {
                    $rs = current($rs);
                    $this->setLev2ID($rs['Lev2ID']);
                    $this->setLev2Name($rs['Lev2Name']);
                    $this->setLev2Code($rs['Lev2Code']);
                    $this->setLev1ID($rs['Lev1ID']);
                    $this->setItemQty($rs['ItemQty']);
                    $this->setIsDefault($rs['IsDefault']);
                    $this->setRecordStatus($rs['RecordStatus']);
                    $this->setDeletedFlag($rs['DeletedFlag']);
                    $this->setDeletedBy($rs['DeletedBy']);
                    $this->setDeletedDate($rs['DeletedDate']);
                    $this->setInputBy($rs['InputBy']);
                    $this->setDateInput($rs['DateInput']);
                    $this->setDateModified($rs['DateModified']);
                    $this->setLastModifiedBy($rs['LastModifiedBy']);
                    return $this;
                } else {
                    return NULL;
                }
            } else {
                return $rs;
            }
        }

        public function save()
        {
            if ($this->getLev2ID() > 0) {
                $ret = $this->updateRecord();
            } else {
                $ret = $this->insertRecord();
            }
            return $ret;
        }

        /**
         * Return a array with all active status, default is order by status code desc
         * @owner : Fai (20131212)
         * 
         * @param
         *            : String $orderCriteria (default order by status code desc)
         * @param
         *            : Integer $levelOneId (default = NULL)
         * @return : Resultset DB array for all active status , depend on the LevelOneId if have any
         *        
         */
        public function getActiveStatus($orderCriteria = '', $levelOneId = NULL)
        {
            $orderCriteria = ($orderCriteria == '') ? ' Lev2Code desc ' : $orderCriteria;
            
            if ($levelOneId > 0) {
                return $this->getAllStatus($whereCriteria = ' Lev1ID = ' . $levelOneId . ' and recordstatus = 1 and DeletedFlag = 0 ', $orderCriteria);
            }
            return $this->getAllStatus($whereCriteria = ' recordstatus = 1 and DeletedFlag = 0 ', $orderCriteria);
        }

        public function getAllStatus($whereCriteria = NULL, $orderCriteria = NULL)
        {
            $rs = $this->loadDataFormStorage($loadObject = false, $whereCriteria, $orderCriteria);
            return $rs;
        }

        public function getHTMLSelection($htmlName, $selectedValue = NULL, $levelOneId = NULL, $otherAttribute = NULL)
        {
            global $Lang, $junior_mck;
            
            $html = '<select name="' . $htmlName . '" id="' . $htmlName . '" ' . $otherAttribute . '>' . "\n";
            
            if ($levelOneId == - 1) {
                $html .= '<option value= "" SELECTED>' . ($junior_mck ? convert2unicode($Lang['General']['PleaseSelect'],1,1) : $Lang['General']['PleaseSelect']) . '</option>' . "\n";
            } else {
                $rs = $this->getActiveStatus($orderCriteria = 'Lev2Code asc ', $levelOneId);
                if (count($rs) > 0) {
                    $withDefaultSelected = false;
                    for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                        $_selected = '';
                        // without user default value
                        if ($selectedValue == NULL) {
                            // have not set selected before
                            if (! $withDefaultSelected) {
                                if ($rs[$i]['IsDefault'] == 1) {
                                    $_selected = ' SELECTED ';
                                    $withDefaultSelected = true;
                                }
                            }
                        } else {
                            $_selected = ($rs[$i]['Lev2ID'] == $selectedValue) ? ' SELECTED ' : '';
                        }
                        
                        $html .= '<option value= "' . $rs[$i]['Lev2ID'] . '" ' . $_selected . ' >' . stripslashes($rs[$i]['Lev2Name']) . '</option>' . "\n";
                    }
                } else {
                    $html .= '<option value= "" ' . $_selected . '>' . ($junior_mck ? convert2unicode($Lang['General']['NoRecordFound'],1,1) : $Lang['General']['NoRecordFound']) . '</option>' . "\n";
                }
            }
            $html .= '</select>';
            return $html;
        }

        /**
         * Checking for duplicate ['Lev2Name'] and ['Lev2Code']
         * @owner : Pun (20140110)
         * 
         * @param
         *            : String $testName (Name to test for duplicate)
         * @param
         *            : String $testCode (Code to test for duplicate)
         * @param
         *            : String $Lev1ID (Record to check, for holding old record)
         * @return : 0 if the checking pass(no duplicated record), 1 if Lev1Name duplicated, 2 if Lev1Code duplicated, 3 if both duplicated
         *        
         */
        public function recordDuplicateChecking($testName, $testCode = '', $Lev1ID, $Lev2ID = '')
        {
            if ($testName == '' && $testCode == '') {
                return true;
            }
            
            $checkingResult = 0;
            $addtionSQL = '';
            if ($Lev1ID != '') {
                $addtionSQL .= ' AND Lev1ID = \'' . $Lev1ID . '\'';
            }
            if ($Lev2ID != '') {
                $addtionSQL .= ' AND Lev2ID != \'' . $Lev2ID . '\'';
            }
            
            $sql = 'SELECT COUNT(*) FROM MEDICAL_STUDENT_LOG_LEV2 WHERE DeletedFlag = 0 AND Lev2Name=\'' . trim($testName, '\'') . '\'' . $addtionSQL;
            $rs = $this->objDB->returnResultSet($sql);
            if ($rs[0]['COUNT(*)'] > 0) {
                $checkingResult += 1;
            }
            // error_log("\n\n\n---------------\n", 3, "/tmp/debug_a.txt");
            // error_log('$sql -->'.print_r($sql,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
            // error_log('$rs -->'.print_r($rs,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
            // error_log('$checkingResult -->'.print_r($checkingResult,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
            if ($testCode != '' && $testCode != '\'\'') {
                $sql = 'SELECT COUNT(*) FROM MEDICAL_STUDENT_LOG_LEV2 WHERE DeletedFlag = 0 AND Lev2Code=\'' . trim($testCode, '\'') . '\'' . $addtionSQL;
                $rs = $this->objDB->returnResultSet($sql);
                if ($rs[0]['COUNT(*)'] > 0) {
                    $checkingResult += 2;
                }
            }
            
            return $checkingResult;
        }

        /*
         * Add duplicate checking - by Pun
         */
        private function insertRecord()
        {
            global $cfg_medical;
            $DataArr = array();
            
            if ($this->recordDuplicateChecking($this->getLev2Name(), $this->getLev2Code(), $this->getLev1ID())) {
                return false;
            }
            
            $DataArr['Lev2Name'] = $this->objDB->pack_value($this->getLev2Name(), 'str');
            $DataArr['Lev2Code'] = $this->objDB->pack_value($this->getLev2Code(), 'str');
            $DataArr['Lev1ID'] = $this->objDB->pack_value($this->getLev1ID(), 'int');
            $DataArr['ItemQty'] = $this->objDB->pack_value(0, 'str'); // default no item
            $DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(), 'int');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(), 'int');
            $DataArr['DateInput'] = $this->objDB->pack_value('now()', 'date');
            // $DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
            // $DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');
            
            $sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);
            
            $fieldStr = $sqlStrAry['sqlField'];
            $valueStr = $sqlStrAry['sqlValue'];
            
            $sql = 'Insert Into MEDICAL_STUDENT_LOG_LEV2 (' . $fieldStr . ') Values (' . $valueStr . ')';
            
            $success = $this->objDB->db_db_query($sql);
            if ($success != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            $RecordID = $this->objDB->db_insert_id();
            $this->setLev2ID($RecordID);
            return $RecordID;
        }

        /*
         * Add duplicate checking - by Pun
         */
        private function updateRecord()
        {
            global $cfg_medical;
            
            $DataArr = array();
            
            $DataArr['Lev2Name'] = $this->objDB->pack_value($this->getLev2Name(), 'str');
            $DataArr['Lev2Code'] = $this->objDB->pack_value($this->getLev2Code(), 'str');
            $DataArr['Lev1ID'] = $this->objDB->pack_value($this->getLev1ID(), 'int');
            // $DataArr['ItemQty'] = $this->objDB->pack_value($this->getItemQty(),'str');
            $DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(), 'int');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            // $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
            $DataArr['DateModified'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(), 'int');
            
            if ($this->recordDuplicateChecking($DataArr['Lev2Name'], $DataArr['Lev2Code'], $this->getLev1ID(), $this->getLev2ID())) {
                return false;
            }
            
            foreach ($DataArr as $fieldName => $data) {
                $updateDetails .= $fieldName . "=" . $data . ",";
            }
            
            // REMOVE LAST OCCURRENCE OF ",";
            $updateDetails = substr($updateDetails, 0, - 1);
            
            $sql = "update MEDICAL_STUDENT_LOG_LEV2 set " . $updateDetails . " where Lev2ID = " . $this->getLev2ID();
            // error_log("\n\nupdate sql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
            $result = $this->objDB->db_db_query($sql);
            
            if ($result != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            return $result;
        }

        // Check if IsDefault been set in amoung all active records
        public function checkIsDefault($lev1ID)
        {
            $sql = "SELECT Lev2ID FROM MEDICAL_STUDENT_LOG_LEV2 WHERE Lev1ID=" . $lev1ID . " AND IsDefault=1 AND DeletedFlag=0 LIMIT 1";
            $rs = $this->objDB->returnResultSet($sql);
            $ret = (count($rs) > 0) ? true : false;
            return $ret;
        }

        public function sumItemQty($lev1ID)
        {
            if ($lev1ID) {
                // Deleted not count
                $sql = "SELECT COUNT(1) AS ItemQty FROM MEDICAL_STUDENT_LOG_LEV2 WHERE Lev1ID=" . $lev1ID . " and DeletedFlag = 0";
                // error_log("\n\nupdate sql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
                $rs = $this->objDB->returnResultSet($sql);
                // error_log("\n\nrs -->".print_r($rs,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
                if (count($rs) == 1) {
                    return $rs[0]["ItemQty"];
                } else {
                    $errMsg = 'sumItemQty Error: mutiple records! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                    alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                }
            } else {
                $errMsg = "sumItemQty Error: empty lev1ID  f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
            }
            return false;
        }
    }
}
?>