<?php
$cfg_medical['module_code'] = 'medical';

if($plugin['medical_module']['meal']){
	$cfg_medical['ACCESS_RIGHT']['MEAL']['MANAGEMENT'] = array('RIGHT'=>'MEAL_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['MEAL']['REPORT'] = array('RIGHT'=>'MEAL_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
	if($sys_custom['medical']['accessRight']['PrintAndExport']){
    	$cfg_medical['ACCESS_RIGHT']['MEAL']['REPORTPRINT'] = array('RIGHT'=>'MEAL_REPORTPRINT','LANG'=>$Lang['medical']['accessRight']['Option']['Print']);
    	$cfg_medical['ACCESS_RIGHT']['MEAL']['REPORTEXPORT'] = array('RIGHT'=>'MEAL_REPORTEXPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Export']);
	}
	$cfg_medical['ACCESS_RIGHT']['MEAL']['SETTINGS'] = array('RIGHT'=>'MEAL_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
}

if($plugin['medical_module']['bowel']){
	$cfg_medical['ACCESS_RIGHT']['BOWEL']['MANAGEMENT'] = array('RIGHT'=>'BOWEL_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['BOWEL']['REPORT'] = array('RIGHT'=>'BOWEL_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
	if($sys_custom['medical']['accessRight']['PrintAndExport']){
    	$cfg_medical['ACCESS_RIGHT']['BOWEL']['REPORTPRINT'] = array('RIGHT'=>'BOWEL_REPORTPRINT','LANG'=>$Lang['medical']['accessRight']['Option']['Print']);
    	$cfg_medical['ACCESS_RIGHT']['BOWEL']['REPORTEXPORT'] = array('RIGHT'=>'BOWEL_REPORTEXPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Export']);
	}
	$cfg_medical['ACCESS_RIGHT']['BOWEL']['SETTINGS'] = array('RIGHT'=>'BOWEL_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
}

if($plugin['medical_module']['studentLog']){
	$cfg_medical['ACCESS_RIGHT']['STUDENTLOG']['MANAGEMENT'] = array('RIGHT'=>'STUDENTLOG_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['STUDENTLOG']['REPORT'] = array('RIGHT'=>'STUDENTLOG_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
	if($sys_custom['medical']['accessRight']['PrintAndExport']){
    	$cfg_medical['ACCESS_RIGHT']['STUDENTLOG']['REPORTPRINT'] = array('RIGHT'=>'STUDENTLOG_REPORTPRINT','LANG'=>$Lang['medical']['accessRight']['Option']['Print']);
    	$cfg_medical['ACCESS_RIGHT']['STUDENTLOG']['REPORTEXPORT'] = array('RIGHT'=>'STUDENTLOG_REPORTEXPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Export']);
	}
	$cfg_medical['ACCESS_RIGHT']['STUDENTLOG']['SETTINGS'] = array('RIGHT'=>'STUDENTLOG_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
	if($sys_custom['medical']['accessRight']['ViewAll']){
	   $cfg_medical['ACCESS_RIGHT']['STUDENTLOG']['VIEWALL'] = array('RIGHT'=>'STUDENTLOG_VIEWALL','LANG'=>$Lang['medical']['accessRight']['Option']['ViewAll']);
	}
}

if($plugin['medical_module']['sleep']){
	$cfg_medical['ACCESS_RIGHT']['SLEEP']['MANAGEMENT'] = array('RIGHT'=>'SLEEP_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['SLEEP']['REPORT'] = array('RIGHT'=>'SLEEP_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
	if($sys_custom['medical']['accessRight']['PrintAndExport']){
    	$cfg_medical['ACCESS_RIGHT']['SLEEP']['REPORTPRINT'] = array('RIGHT'=>'SLEEP_REPORTPRINT','LANG'=>$Lang['medical']['accessRight']['Option']['Print']);
    	$cfg_medical['ACCESS_RIGHT']['SLEEP']['REPORTEXPORT'] = array('RIGHT'=>'SLEEP_REPORTEXPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Export']);
	}
	$cfg_medical['ACCESS_RIGHT']['SLEEP']['SETTINGS'] = array('RIGHT'=>'SLEEP_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
}

if($plugin['medical_module']['message']){
	$cfg_medical['ACCESS_RIGHT']['NOTICE']['SEND'] = array('RIGHT'=>'SEND_NOTICE','LANG'=>$Lang['medical']['accessRight']['Option']['Send']);
	$cfg_medical['ACCESS_RIGHT']['NOTICE']['RECEIVE'] = array('RIGHT'=>'RECEIVE_NOTICE','LANG'=>$Lang['medical']['accessRight']['Option']['Receive']);
	$cfg_medical['ACCESS_RIGHT']['NOTICE']['SETTINGS'] = array('RIGHT'=>'NOTICE_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
}

if($plugin['medical_module']['discipline']) {
	$cfg_medical['ACCESS_RIGHT']['EVENT']['MANAGEMENT'] = array('RIGHT'=>'EVENT_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['EVENT']['DELETESELFRECORD'] = array('RIGHT'=>'EVENT_DELETESELFRECORD','LANG'=>$Lang['medical']['accessRight']['Option']['DeleteSelfRecord']);
	$cfg_medical['ACCESS_RIGHT']['EVENT']['REPORT'] = array('RIGHT'=>'EVENT_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
	$cfg_medical['ACCESS_RIGHT']['EVENT']['SETTINGS'] = array('RIGHT'=>'EVENT_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
	
	$cfg_medical['ACCESS_RIGHT']['CASE']['MANAGEMENT'] = array('RIGHT'=>'CASE_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['CASE']['DELETESELFRECORD'] = array('RIGHT'=>'CASE_DELETESELFRECORD','LANG'=>$Lang['medical']['accessRight']['Option']['DeleteSelfRecord']);
}

if($plugin['medical_module']['awardscheme']) {
	$cfg_medical['ACCESS_RIGHT']['AWARDSCHEME']['MANAGEMENT'] = array('RIGHT'=>'AWARDSCHEME_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['AWARDSCHEME']['DELETESELFRECORD'] = array('RIGHT'=>'AWARDSCHEME_DELETESELFRECORD','LANG'=>$Lang['medical']['accessRight']['Option']['DeleteSelfRecord']);
}

if($plugin['medical_module']['revisit']){
	$cfg_medical['ACCESS_RIGHT']['REVISIT']['MANAGEMENT'] = array('RIGHT'=>'REVISIT_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
	$cfg_medical['ACCESS_RIGHT']['REVISIT']['DELETESELFRECORD'] = array('RIGHT'=>'REVISIT_DELETESELFRECORD','LANG'=>$Lang['medical']['accessRight']['Option']['DeleteSelfRecord']);
	$cfg_medical['ACCESS_RIGHT']['REVISIT']['REPORT'] = array('RIGHT'=>'REVISIT_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
}

if ($plugin['medical_module']['staffEvents']) {
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['VIEW'] = array('RIGHT'=>'STAFFEVENT_VIEW','LANG'=>$Lang['medical']['accessRight']['Option']['View']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['ADD'] = array('RIGHT'=>'STAFFEVENT_ADD','LANG'=>$Lang['medical']['accessRight']['Option']['Add']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['EDIT'] = array('RIGHT'=>'STAFFEVENT_EDIT','LANG'=>$Lang['medical']['accessRight']['Option']['Edit']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['DELETE'] = array('RIGHT'=>'STAFFEVENT_DELETE','LANG'=>$Lang['medical']['accessRight']['Option']['Delete']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['DELETESELF'] = array('RIGHT'=>'STAFFEVENT_DELETESELF','LANG'=>$Lang['medical']['accessRight']['Option']['Self']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['DELETEALL'] = array('RIGHT'=>'STAFFEVENT_DELETEALL','LANG'=>$Lang['medical']['accessRight']['Option']['All']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['REPORT'] = array('RIGHT'=>'STAFFEVENT_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
    $cfg_medical['ACCESS_RIGHT']['STAFFEVENT']['SETTINGS'] = array('RIGHT'=>'STAFFEVENT_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
}

if ($sys_custom['medical']['Handover']){
    $cfg_medical['ACCESS_RIGHT']['HANDOVER']['MANAGEMENT'] = array('RIGHT'=>'HANDOVER_MANAGEMENT','LANG'=>$Lang['medical']['accessRight']['Option']['Management']);
    $cfg_medical['ACCESS_RIGHT']['HANDOVER']['REPORT'] = array('RIGHT'=>'HANDOVER_REPORT','LANG'=>$Lang['medical']['accessRight']['Option']['Report']);
    $cfg_medical['ACCESS_RIGHT']['HANDOVER']['SETTINGS'] = array('RIGHT'=>'HANDOVER_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
}
//if(
//	$plugin['medical_module']['meal'] ||
//	$plugin['medical_module']['bowel'] ||
//	$plugin['medical_module']['studentLog'] ||
//	$plugin['medical_module']['sleep']
//){
//	$cfg_medical['ACCESS_RIGHT']['DEFAULTREMARK']['SETTINGS'] = array('RIGHT'=>'DEFAULTREMARK_SETTINGS','LANG'=>$Lang['medical']['accessRight']['Option']['Settings']);
//}
?>