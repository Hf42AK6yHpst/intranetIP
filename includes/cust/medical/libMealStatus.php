<?php
//updated by : 
/*
 * 	Log
 * 
 *  Date:   2018-05-29 [Cameron] apply stripslashes to StatusName in getHTMLSelection()
 *  
 * 	Date:	2013-12-13 [Cameron] Add function checkIsDefault()
 */
if (!defined("LIBMEALSTATUS_DEFINED"))                     // Preprocessor directive
{
        define("LIBMEALSTATUS_DEFINED", true);
		class mealStatus
        {
                private $objDB;

				private $StatusID;
				private $StatusName;
				private $StatusCode;
				private $Color;
				private $IsDefault;
				private $RecordStatus;
				private $DeletedFlag;
				private $DeletedBy;
				private $DeletedDate;
				private $InputBy;
				private $DateInput;
				private $DateModified;
				private $LastModifiedBy;


				public function mealStatus($statusID = NULL){
					$this->objDB = new libdb();

					if($statusID != ''){
						$this->setStatusID($statusID);
						$this->loadDataFormStorage();
					}
				}

				public function setStatusID($val){$this->StatusID=$val;}
				public function getStatusID(){return $this->StatusID;}

				public function setStatusName($val){$this->StatusName=$val;}
				public function getStatusName(){return $this->StatusName;}

				public function setStatusCode($val){$this->StatusCode=$val;}
				public function getStatusCode(){return $this->StatusCode;}

				public function setColor($val){$this->Color=$val;}
				public function getColor(){return $this->Color;}

				public function setIsDefault($val){$this->IsDefault=$val;}
				public function getIsDefault(){return $this->IsDefault;}

				public function setRecordStatus($val){$this->RecordStatus=$val;}
				public function getRecordStatus(){return $this->RecordStatus;}

				public function setDeletedFlag($val){$this->DeletedFlag=$val;}
				public function getDeletedFlag(){return $this->DeletedFlag;}

				public function setDeletedBy($val){$this->DeletedBy=$val;}
				public function getDeletedBy(){return $this->DeletedBy;}

				public function setDeletedDate($val){$this->DeletedDate=$val;}
				public function getDeletedDate(){return $this->DeletedDate;}

				public function setInputBy($val){$this->InputBy=$val;}
				public function getInputBy(){return $this->InputBy;}
				
				public function setDateInput($val){$this->DateInput=$val;}
				public function getDateInput(){return $this->DateInput;}

				public function setDateModified($val){$this->DateModified=$val;}
				public function getDateModified(){return $this->DateModified;}

				public function setLastModifiedBy($val){$this->LastModifiedBy=$val;}
				public function getLastModifiedBy(){return $this->LastModifiedBy;}

				private function loadDataFormStorage($loadObject=true, $whereCriteria= NULL,$orderCriteria = NULL)
				{

					$conds = '';
					if($loadObject){
						$conds = 'and StatusID = '.$this->getStatusID();
					}

					if($whereCriteria !== NULL){
							$conds .= ' and ('.$whereCriteria.')';
					}
					
					$orderBy = '';
					if ($orderCriteria !== NULL){
						$orderBy = ' ORDER BY ' . $orderCriteria;
					}

					$sql='select 
							  StatusID,
							  StatusName,
							  StatusCode,
							  Color,
							  IsDefault,
							  RecordStatus,
							  DeletedFlag,
							  DeletedBy,
							  DeletedDate,
							  InputBy,
							  DateInput,
							  DateModified,
							  LastModifiedBy 
						  from MEDICAL_MEAL_STATUS where 1=1 '.$conds . $orderBy;

					$rs = $this->objDB->returnResultSet($sql);

					if($loadObject){
						if(count($rs) > 1){
							//since the statusid is the key  ,return resultset cannot more than 1
							  $errMsg = 'SQL Result! support return zero not or one record but failed ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
							  return false;
						}
						
						if(count($rs) ==1){
							$rs = current($rs);
							$this->setStatusID($rs['StatusID']);
							$this->setStatusName($rs['StatusName']);
							$this->setStatusCode($rs['StatusCode']);
							$this->setColor($rs['Color']);
							$this->setIsDefault($rs['IsDefault']);
							$this->setRecordStatus($rs['RecordStatus']);
							$this->setDeletedFlag($rs['DeletedFlag']);
							$this->setDeletedBy($rs['DeletedBy']);
							$this->setDeletedDate($rs['DeletedDate']);
							$this->setInputBy($rs['InputBy']);
							$this->setDateInput($rs['DateInput']);
							$this->setDateModified($rs['DateModified']);
							$this->setLastModifiedBy($rs['LastModifiedBy']);	
							return $this;
						}else{
							return NULL;
						}
					}
					else{
						return $rs;	
					}
				}

				public function save()
				{
					if($this->getStatusID()>0){
						$ret = $this->updateRecord();
					}else{
						$ret = $this->insertRecord();
					}
					return $ret;
				}
				
	
			   /**
				* Return a array with all active status, default is order by status code desc
				* @owner : Fai (20131212)
				* @param : String $orderCriteria (default order by status code desc)
				* @return : Resultset DB array for all active status (not deleted)
				* 
				*/
				public function getActiveStatus($orderCriteria = NULL)
				{
					$orderCriteria = ($orderCriteria == NULL) ? ' StatusCode desc ' :$orderCriteria;
					return $this->getAllStatus($whereCriteria = ' recordstatus = 1 and DeletedFlag = 0 ',$orderCriteria);
				}
				public function getAllStatus($whereCriteria = NULL,$orderCriteria = NULL){
					$rs = $this->loadDataFormStorage($loadObject=false, $whereCriteria,$orderCriteria);
					return $rs;
				}

				public function getHTMLSelection($htmlName, $selectedValue = NULL, $otherAttribute = NULL){
					global $Lang;

					//$sqlCriteria  = ' recordstatus = 1 ';
//					$orderCriteria = ' StatusCode desc ';
					
//					$rs = $this->getAllStatus($sqlCriteria ,$orderCriteria);
					$rs = $this->getActiveStatus(' StatusCode ASC ');

					$html = '<span style="white-space:nowrap;"><span class="colorBoxStyle">&nbsp;&nbsp;&nbsp;</span>&nbsp;'.'<select name="'.$htmlName.'" id="'.$htmlName.'" '.$otherAttribute.'>'."\n";
					if(count($rs) >0)
					{
						$withDefaultSelected = false;
						for($i = 0,$iMax = count($rs);$i < $iMax; $i++)
						{	
							$_selected = '';
							//without user default value
							if($selectedValue == NULL)
							{
								//have not set selected before
								if(!$withDefaultSelected)
								{
									if($rs[$i]['IsDefault'] == 1)
									{
										$_selected = ' SELECTED ';
										$withDefaultSelected = true;
									}
								}
							}else{
								$_selected = ($rs[$i]['StatusID'] == $selectedValue) ? ' SELECTED ' :'';
							}

							$html .= '<option value= "'.$rs[$i]['StatusID'].'" '.$_selected.'  data-color="'.$rs[$i]['Color'].'">'.stripslashes($rs[$i]['StatusName']).'</option>'."\n";
						}	
					}else{
						$html .= '<option value= "" '.$_selected.'>'.$Lang['General']['NoRecordFound'].'</option>'."\n";
					}
					
					$html .= '</select></span>';
					return $html;
				}
				
     		   /**
				 * Checking for duplicate ['StatusName'] and ['StatusCode']
				 * @owner : Pun (20140127)
				 * @param : String $testName (Name to test for duplicate)
				 * @param : String $testCode (Code to test for duplicate)
				 * @return : 0 if the checking pass(no duplicated record)
				 * 
				 */
				public function recordDuplicateChecking($testName, $testCode, $StatusID='')
				{
					if($testName == '' && $testCode == '')
					{
						return true;
					}
					
					$checkingResult = 0;
					$addtionSQL = '';
					if($StatusID != '')
					{
						$addtionSQL = ' AND StatusID != \'' . $StatusID . '\'';
					}
					
					$sql = 'SELECT COUNT(*) FROM MEDICAL_MEAL_STATUS WHERE DeletedFlag = 0 AND StatusName=\'' . trim($testName,'\'') . '\'' . $addtionSQL;
					$rs = $this->objDB->returnResultSet($sql);
					if($rs[0]['COUNT(*)'] > 0)
					{
						$checkingResult += 1;
					}
					
					$sql = 'SELECT COUNT(*) FROM MEDICAL_MEAL_STATUS WHERE DeletedFlag = 0 AND StatusCode=\'' . trim($testCode,'\'') . '\'' . $addtionSQL;
					$rs = $this->objDB->returnResultSet($sql);
					if($rs[0]['COUNT(*)'] > 0)
					{
						$checkingResult += 2;
					}
					
//error_log("\n\n\n---------------\n", 3, "/tmp/debug_a.txt");
//error_log('$sql -->'.print_r($sql,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
//error_log('$rs -->'.print_r($rs,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
//error_log('$checkingResult -->'.print_r($checkingResult,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
								
					return $checkingResult;
				}
				
				private function insertRecord(){
					global $cfg_medical;
					$DataArr = array();

					$DataArr['StatusName'] = $this->objDB->pack_value($this->getStatusName(),'str');
					$DataArr['StatusCode'] = $this->objDB->pack_value($this->getStatusCode(),'str');
					$DataArr['Color'] = $this->objDB->pack_value($this->getColor(),'str');
					$DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(),'int');
					$DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(),'int');
					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
					$DataArr['DateInput'] = $this->objDB->pack_value('now()','date');
//					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
//					$DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');

					$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

					$fieldStr= $sqlStrAry['sqlField'];
					$valueStr= $sqlStrAry['sqlValue'];


					$sql = 'Insert Into MEDICAL_MEAL_STATUS ('.$fieldStr.') Values ('.$valueStr.')';
					
					$success = $this->objDB->db_db_query($sql);
					if($success != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}
					
					$RecordID = $this->objDB->db_insert_id();
					$this->setStatusID($RecordID);
					return $RecordID;
				}

				private function updateRecord(){
					global $cfg_medical;

					$DataArr = array();

					$DataArr['StatusName'] = $this->objDB->pack_value($this->getStatusName(),'str');
					$DataArr['StatusCode'] = $this->objDB->pack_value($this->getStatusCode(),'str');
					$DataArr['Color'] = $this->objDB->pack_value($this->getColor(),'str');
					$DataArr['IsDefault'] = $this->objDB->pack_value($this->getIsDefault(),'int');
					$DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(),'int');
//					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
					$DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');

					foreach ($DataArr as $fieldName => $data)
					{
						$updateDetails .= $fieldName."=".$data.",";
					}

					//REMOVE LAST OCCURRENCE OF ",";
					$updateDetails = substr($updateDetails,0,-1);

					$sql = "update MEDICAL_MEAL_STATUS set ".$updateDetails." where StatusID = ".$this->getStatusID();

					$result = $this->objDB->db_db_query($sql);
								
					if($result != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}

					return $result;

				}
				
				// Check if IsDefault been set in amoung all active records
				public function checkIsDefault(){
					$sql = "SELECT StatusID FROM MEDICAL_MEAL_STATUS WHERE IsDefault=1 AND DeletedFlag=0 LIMIT 1";
					$rs = $this->objDB->returnResultSet($sql);
					$ret = (count($rs) > 0) ? true : false;
					return $ret; 					
					
				}
		}

}
?>