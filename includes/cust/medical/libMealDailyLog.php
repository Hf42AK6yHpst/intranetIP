<?php
// using: Adam


if (!defined("LIBMEALDAILYLOG_DEFINED"))                     // Preprocessor directive
{
    define("LIBMEALDAILYLOG_DEFINED", true);
	class mealDailyLog
    {
    	private $objDB;
    	private $intranet_db;
		private	$RecordID;
		private	$UserID;
		private	$RecordDay; // Event Date format: DD
		private	$PeriodStatus;
		private	$Remarks;
		private	$ConfirmedUserID;
		private	$MealStatus;
		private	$DateInput;
		private	$InputBy;
		private	$DateModified;
		private	$ModifyBy;
		
		private $FullRecordDay; // Event Date format: YYYY-MM-DD
		public function mealDailyLog($recordDay,$recordID = NULL){
			global $intranet_db;
			$this->objDB = new libdb();
			$this->intranet_db = $intranet_db;
			
			$this->setRecordDay(date('d', strtotime($recordDay)));
			$this->setFullRecordDay($recordDay);
			if($recordID != ''){
				$this->setRecordID($recordID);
				$this->loadDataFormStorage();
			}
		}
    		
		public function setUserID($val){$this->UserID=$val;}
		public function getUserID(){return $this->UserID;}
		
		public function setPeriodStatus($val){$this->PeriodStatus=$val;}
		public function getPeriodStatus(){return $this->PeriodStatus;}
		
		public function setRemarks($val){$this->Remarks=$val;}
		public function getRemarks(){return $this->Remarks;}
		
		public function setConfirmedUserID($val){$this->ConfirmedUserID=$val;}
		public function getConfirmedUserID(){return $this->ConfirmedUserID;}
		
		public function setMealStatus($val){$this->MealStatus=$val;}
		public function getMealStatus(){return $this->MealStatus;}
		
		public function setDateInput($val){$this->DateInput=$val;}
		public function getDateInput(){return $this->DateInput;}
		
		public function setInputBy($val){$this->InputBy=$val;}
		public function getInputBy(){return $this->InputBy;}
		
		public function setDateModified($val){$this->DateModified=$val;}
		public function getDateModified(){return $this->DateModified;}
		
		public function setModifyBy($val){$this->ModifyBy=$val;}
		public function getModifyBy(){return $this->ModifyBy;}

		private function setOldMealStatus($val){$this->OldMealStatus=$val;}
		private function getOldMealStatus(){return $this->OldMealStatus;}
		
		private function setOldRemarks($val){$this->OldRemarks=$val;}
		private function getOldRemarks(){return $this->OldRemarks;}
    
    
		private function setRecordID($val){$this->RecordID=$val;} // You are not allow to set this variable, use constructor instead.
		public function getRecordID(){return $this->RecordID;}
		
		private function setRecordDay($val){$this->RecordDay=$val;} // You are not allow to set this variable, use constructor instead.
		public function getRecordDay(){return $this->RecordDay;}
		
		private function setFullRecordDay($val){$this->FullRecordDay=$val;} // You are not allow to set this variable, use constructor instead. 
		public function getFullRecordDay(){return $this->FullRecordDay;}
		
		private function loadDataFormStorage()
		{
			global $cfg_medical, $w2_cfg;
			$conds = 'And RecordID = '.$this->getRecordID();
//			$tableName = 'MEDICAL_STUDENT_DAILY_MEAL_LOG_'.date('Y', strtotime($dateLog)).'_'.date('m', strtotime($dateLog));
			$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"].date('Y', strtotime($this->getFullRecordDay())).'_'.date('m', strtotime($this->getFullRecordDay()));

			$sql = 'Show tables like "'.$tableName.'"';
			$rs = $this->objDB->returnResultSet($sql);

			if($rs[0] !=''){
				$sql='Select
					RecordID,
					UserID,
					RecordDay,
					PeriodStatus,
					Remarks,
					ConfirmedUserID,
					MealStatus,
					DateInput,
					InputBy,
					DateModified,
					ModifyBy	
				From
					'.$tableName.'
				Where
					1=1 '.$conds;

				$rs = $this->objDB->returnResultSet($sql);
			
				if(count($rs) > 1){
					//since the statusid is the key  ,return resultset cannot more than 1
					  $errMsg = 'SQL Result! support return zero not or one record but failed ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
		              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					  return false;
				}
					
				if(count($rs) ==1){
					$rs = current($rs);
					$this->setRecordID($rs['RecordID']);
					$this->setUserID($rs['UserID']);
					$this->setRecordDay($rs['RecordDay']);
					$this->setPeriodStatus($rs['PeriodStatus']);
					$this->setRemarks($rs['Remarks']);
					$this->setConfirmedUserID($rs['ConfirmedUserID']);
					$this->setMealStatus($rs['MealStatus']);
					$this->setDateInput($rs['DateInput']);
					$this->setInputBy($rs['InputBy']);
					$this->setDateModified($rs['DateModified']);
					$this->setModifyBy($rs['ModifyBy']);	
					
					$this->setOldMealStatus($rs['MealStatus']);
					$this->setOldRemarks($rs['Remarks']);
					return $this;
				}
				else{
					return NULL;
				}
			}
			else{
				//since the statusid is the key  ,return resultset cannot more than 1
				$errMsg = 'SQL Result! Database Does Not Exist ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				return false;				
			}
		}
		public function Start_Trans(){
			$this->objDB->Start_Trans();
		}
		public function Commit_Trans(){
			$this->objDB->Commit_Trans();
		}
		public function RollBack_Trans(){
			$this->objDB->RollBack_Trans();
		}
		
		/* Removed at 09/04/2014, because it is not active-record.
		public function addRecord($userID, $mealStatus, $remarks,$date,
									$timePeriod,$modifiedBy, $recordID=''){
			$this->FullRecordDay = $date;
			
			$this->setRecordID($recordID);
			$this->setUserID($userID);
			$this->setMealStatus($mealStatus);
			$this->setRemarks($remarks);
			$this->setRecordDay(date('d', strtotime($date)));
			$this->setPeriodStatus($timePeriod);
			$this->setModifyBy($modifiedBy);
			$this->setConfirmedUserID($modifiedBy);
			$this->setInputBy($modifiedBy);
		}
		*/
		
		public function save()
		{
			if($this->getRecordID()>0){
				$result =  $this->updateRecord();
			}else{
				$result =  intval($this->insertRecord())>0;
			}
			return $result;
		}
		
		private function insertRecord(){
			global $cfg_medical, $w2_cfg;
			$DataArr = array();

			$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"].date('Y', strtotime($this->FullRecordDay)).'_'.date('m', strtotime($this->FullRecordDay));

			$sql = "
				CREATE TABLE IF NOT EXISTS {$tableName} (
				RecordID int(11) NOT NULL auto_increment,
				UserID int(11) NOT NULL,
				RecordDay int(11) NOT NULL,  -- 2013-12-01
				PeriodStatus int(11) default NULL,  -- (breakfast , lunch , dinner)
				Remarks mediumtext default NULL,
				ConfirmedUserID int(11) default NULL,
				MealStatus int(11) default NULL, -- (table meal status, id) 
				DateInput datetime default NULL,
				InputBy int(11) default NULL,
				DateModified datetime default NULL,
				ModifyBy int(11) default NULL, 
				PRIMARY KEY  (RecordID),
				UNIQUE KEY `User_RecordDay_PeriodStatus` (`UserID`,`RecordDay`,`PeriodStatus`)
				) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
			";
			$result = $this->objDB->db_db_query($sql);

			if($result != 1){
				$errMsg = 'SQL Error! Datebase cannot be created. '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				return false;
			}
			$DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(),'int');
			$DataArr['RecordDay'] = $this->objDB->pack_value($this->getRecordDay(),'int');
			$DataArr['PeriodStatus'] = $this->objDB->pack_value($this->getPeriodStatus(),'int');
			$DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(),'str');
			$DataArr['ConfirmedUserID'] = $this->objDB->pack_value($this->getConfirmedUserID(),'int');
			$DataArr['MealStatus'] = $this->objDB->pack_value($this->getMealStatus(),'int');
			$DataArr['DateInput'] = $this->objDB->pack_value( 'now()','date');
			$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
			$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
			$DataArr['ModifyBy'] = $this->objDB->pack_value($this->getModifyBy(),'int');
			
			$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

			$fieldStr= $sqlStrAry['sqlField'];
			$valueStr= $sqlStrAry['sqlValue'];

				
			$sql = 'Insert Into '.$this->intranet_db.'.'.$tableName.' ('.$fieldStr.') Values ('.$valueStr.')';
			
			$success = $this->objDB->db_db_query($sql);
			if($success != 1){
				  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
	              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				  return false;
			}
			
			$RecordID = $this->objDB->db_insert_id();
			$this->setRecordID($RecordID);
			return $RecordID;
		}

		private function updateRecord(){
			global $cfg_medical, $w2_cfg;

			if( // Update the modified recored only.
					$this->getOldMealStatus() == $this->getMealStatus() &&
					$this->getOldRemarks() == $this->getRemarks()
			){
				return true;
			}
			
//			$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"].date('Y', strtotime($this->getRecordDay())).'_'.date('m', strtotime($this->getRecordDay()));
			$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"].date('Y', strtotime($this->FullRecordDay)).'_'.date('m', strtotime($this->FullRecordDay));

			$sql = 'Show tables like "'.$tableName.'"';
			$rs = $this->objDB->returnResultSet($sql);			
			
			if( count($rs[0]) >0 ){
				$DataArr = array();
	
				$DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(),'str');
				$DataArr['ConfirmedUserID'] = $this->objDB->pack_value($this->getConfirmedUserID(),'int');
				$DataArr['MealStatus'] = $this->objDB->pack_value($this->getMealStatus(),'int');
				$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
				$DataArr['ModifyBy'] = $this->objDB->pack_value($this->getModifyBy(),'int');
				
				foreach ($DataArr as $fieldName => $data)
				{
					$updateDetails .= $fieldName."=".$data.",";
				}
	
				//REMOVE LAST OCCURRENCE OF ",";
				$updateDetails = substr($updateDetails,0,-1);
	
				$sql = "update ".$this->intranet_db.'.'.$tableName." set ".$updateDetails. " Where RecordID = '".$this->getRecordID()."'";
				$result = $this->objDB->db_db_query($sql);
				if($result != 1){
					  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
		              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					  return false;
				}

				return $result;
			}
			else{
				$errMsg = 'Update SQL Error! Database does not exist. '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				return false;
			}
		}
		
		public function deleteRecord($Date,$recordID){
			global $cfg_medical, $w2_cfg;
			$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"].date('Y', strtotime($Date)).'_'.date('m', strtotime($Date));
			$sql = "Delete From
						{$tableName}
					Where 
						RecordID = ".$recordID;
			$result = $this->objDB->db_db_query($sql);
			if($result != 1){
				  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
//	              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				  return false;
			}

			return $result;			
		}
	}
}
?>