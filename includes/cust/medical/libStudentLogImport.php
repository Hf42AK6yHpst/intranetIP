<?php
/*
 * 	Modified:
 * 	Log
 * 	
 * 	Date: 2016-05-10 [Cameron] replace split with explode to support php 5.4
 * 
 * 	Date: 2015-01-23 [Cameron] Add control flag $plugin['medical_module']['AlwaysShowBodyPart'] 
 */
 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");

//$TargetFilePath = $_POST["TargetFilePath"];


///////////////////////////////
//////////////new file here
///////////////////////////////

if (!defined("LIBSTUDENTLOGIMPORT_DEFINED"))                     // Preprocessor directive
{
	define("LIBSTUDENTLOGIMPORT_DEFINED", true);
	class libStudentLogImport
	{

		function ReadStudentLogImportData($TargetFilePath)
		{
			if (file_exists($TargetFilePath))
			{
		//		$libfs = new libfilesystem();
				$libimport = new libimporttext();
				
				$DefaultCsvHeaderArr= array('Class Name','Class Number','Date','Time','Log Type Code','Item Code','Body Parts','Duration','Remarks','PIC');
				$ColumnPropertyArr = array(1,1,1,1,1,1,1,1,1,1);	// 0 - Ref Field, not for actual import				
				
				$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
				$CsvHeaderArr = array_shift($CsvData);
				$numOfCsvData = count($CsvData);
				$CsvHeaderWrong = false;
				for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
					if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
						$CsvHeaderWrong = true;
						break;
					}
				}
//debug_r($CsvHeaderArr);
//debug_r($DefaultCsvHeaderArr);
//print "header wrong = $CsvHeaderWrong<br>";
				$ret = array();		
//				if ($CsvHeaderWrong)
				$ret["HeaderError"] = $CsvHeaderWrong;
				$ret["NumOfData"] 	= $numOfCsvData;
		//		if($CsvHeaderWrong || $numOfCsvData==0)
		//		{
		//			$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
		//			intranet_closedb();
		//			header('location: '.$ReturnPath.'&ReturnMsgKey='.$ReturnMsgKey);
		//			exit();
		//		}
						
				$ret["Data"] = $this->ValidateStudentLogImportData($CsvData);
				return $ret;		
			}
			else
			{
				return false;
			}
		}

		function ValidateStudentLogImportData($CsvData)
		{
			global $Lang, $medical_cfg, $plugin;
			$objMedical = new libMedical();		
			$ret = array();
			$AllLevCode1 = $objMedical->getAllActiveLev1Code();
			$AllLevCode2 = $objMedical->getAllActiveLev2Code();
			$AllLevCode3 = $objMedical->getAllActiveLev3Code();
			$AllLevCode4 = $objMedical->getAllActiveLev4Code();
			$AllLoginID = $objMedical->getAllTeacherLoginID();
			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
			{
				$classname 		= $CsvData[$i][0]; // column 0=> ClassName
				$classnum 		= $CsvData[$i][1]; // column 1=> ClassNumber
				$date 			= $CsvData[$i][2]; // column 2=> Date
				$time 			= $CsvData[$i][3]; // column 3=> Time
				$behaviour1 	= $CsvData[$i][4]; // column 4=> Behaviour1
				$behaviour2		= $CsvData[$i][5]; // column 5=> Behaviour2
				$parts 			= $CsvData[$i][6]; // column 6=> Parts
				$duration 		= $CsvData[$i][7]; // column 8=> Duration
				$remarks 		= $CsvData[$i][8]; // column 9=> Remarks
				$loginIDList 	= $CsvData[$i][9]; // column 10=> PIC
			
				$classRs = $objMedical->getClassNameClassNumber(); // All ClassName, ClassNumber (RecordType = 2 And RecordStatus = 1)
				$error = array(); // For holding error message in each row
				
				$date = str_replace('/','-',$date);
				
				//////////////////
				//check name exist
				//////////////////
				$nameExist = false;
				for($j = 0, $jMax = count($classRs); $j < $jMax; $j++)
				{
					if($classRs[$j]['ClassName'] == $classname){
						$nameExist = true;
						break;
					}
				}
				if(!$nameExist)
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidClassName']);
				}
			
				////////////////
				//check classnum 
				////////////////
				$validStudent = true;
				$studentID = $objMedical->getUserIDByClassNameAndNumber($classname, $classnum);
				if($classnum == '' || !$studentID)
				{
					$validStudent = false;
					array_push($error, $Lang['medical']['studentLog']['import']['invalidClassNum']);
				}
				
				////////////////
				//check date 
				////////////////
				$date = $this->convertDateFormat($date);
				if(!$this->checkDateFormat($date))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidDate']);
				}
				
				/////////////////
				//check student attendance
				/////////////////
				if(
					$validStudent && 
					($objMedical->getAttendanceStatusOfUser($studentID,$date, 'AM or PM') == 0) &&
					(!libMedicalShowInput::absentAllowedInput('studentLog'))
				){
					$validStudent = false;
					array_push($error, $Lang['medical']['bowel']['import']['invalidAttendance']);
				}
			
				////////////////
				//check time 
				////////////////
				if($time != ''){
					$time = $this->convertTimeFormat($time);
				}
				if(!$this->checkTimeFormat($time))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidTime']);
				}
				
				////////////////
				//check Behaviour1 
				////////////////
				if($behaviour1 == '')
				{
					array_push($error, $Lang['medical']['studentLog']['import']['emptyBehaviour1']);
				}
				else if(!in_array(array('Lev1Code' => $behaviour1), $AllLevCode1))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidBehaviour1']);
				}
				
				////////////////
				//check Behaviour2 
				////////////////
				$lev1ID = $objMedical->getLev1IDByCode($behaviour1);
				$lev1ID2 = $this->getLev1IDByLev2Code($behaviour2);
				$isBehaviour2Pass = false;
				if(is_array($lev1ID2)){
					foreach($lev1ID2 as $id){
						if($lev1ID == $id['Lev1ID']){
							$isBehaviour2Pass = true;
							break;
						}
					}
				}
				if($behaviour2 == '')
				{
					array_push($error, $Lang['medical']['studentLog']['import']['emptyBehaviour2']);
				}
				else if(!$isBehaviour2Pass)
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidBehaviour2']);
				}
				
				////////////////
				//check Parts
				////////////////
				if ($plugin['medical_module']['AlwaysShowBodyPart']) {
					$requiredParts = false;
				}
				else {
					$requiredParts = ($objMedical->getLev2NameByCode($behaviour2) == $medical_cfg['studentLog']['level2']['specialStatus']);
				}
				$partsArr = explode(',', $parts);
				
				if($parts == '' && $requiredParts)
				{
					array_push($error, $Lang['medical']['studentLog']['import']['emptyParts']);
				}
				else
				{
					foreach($partsArr as $part)
					{
						if($part != '' && !in_array(array('Lev4Code' => $part), $AllLevCode4))
						{
							array_push($error, $Lang['medical']['studentLog']['import']['invalidParts']);
							break;
						}
					}
				}
				
				/*
				 * Removed field
				////////////////
				//check Parts1 
				////////////////
				$requiredParts = ($objMedical->getLev2NameByCode($behaviour2) == $medical_cfg['studentLog']['level2']['specialStatus']);
				if($parts1 == '' && $requiredParts)
				{
					array_push($error, $Lang['medical']['studentLog']['import']['emptyParts1']);
				}
				else if($parts1 != '' && !in_array(array('Lev3Code' => $parts1), $AllLevCode3))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidParts1']);
				}
								
				///////////////
				//check Parts2
				////////////////
				if($parts2 == '' && $requiredParts)
				{
					array_push($error, $Lang['medical']['studentLog']['import']['emptyParts2']);
				}
				else if($parts1 != '' && !in_array(array('Lev4Code' => $parts2), $AllLevCode4))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidParts2']);
				}
				*/
				
				////////////////
				//check Duration
				////////////////
				$duration = $this->convertTimeFormat($duration);
				if($duration != '' && !$this->checkDurationFormat($duration))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidDuration']);
				}
				
				////////////////
				//check Remarks
				////////////////
				/*
				if($remarks == '')
				{
					array_push($error, 'Empty Remarks');
				}
				*/
				
				////////////////
				//check LoginID
				////////////////
				if($loginIDList == '')
				{
					//array_push($error, $Lang['medical']['studentLog']['import']['emptyPIC']);
				}
				else
				{
					$pic = explode(',', $loginIDList);
					foreach($pic as $p)
					{
						if(!in_array(array('UserLogin' => $p), $AllLoginID))
						{
							array_push($error, $Lang['medical']['studentLog']['import']['invalidPIC']);
							break;
						}
					}
				}
				
				
				
				$pass = !count($error);
				$ret[$i]["pass"] 					= $pass;
				$ret[$i]["reason"] 					= $error;
				$ret[$i]["rawData"]["ClassName"] 	= $classname;
				$ret[$i]["rawData"]["ClassNum"]		= $classnum;
				$ret[$i]["rawData"]["Date"] 		= $date;
				$ret[$i]["rawData"]["Time"] 		= $time;
				$ret[$i]["rawData"]["Behaviour1"] 	= $behaviour1;
				$ret[$i]["rawData"]["Behaviour2"] 	= $behaviour2;
				$ret[$i]["rawData"]["Parts"] 		= $parts;
				$ret[$i]["rawData"]["Duration"] 	= $duration;
				$ret[$i]["rawData"]["Remarks"] 		= $remarks;
				$ret[$i]["rawData"]["Pic"] 			= $loginIDList;
				$ret[$i]["rawData"]["Error"] 		= $this->errorToRawHTML($error);
				
			}
			return $ret;	
		}
		
		function convertDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($a, $b, $c) = explode('-',$RecordDate);
			else 
				list($a, $b, $c) = explode('/',$RecordDate);
			
			if(strlen($a) == 4){
				return sprintf("%04d/%02d/%02d",$a,$b,$c);
			}
			return sprintf("%04d/%02d/%02d",$c,$b,$a);
		}
		
		function checkDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($year, $month, $day) = explode('-',$RecordDate);
			else 
				list($year, $month, $day) = explode('/',$RecordDate);
								
			if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
			{
				if(checkdate($month,$day,$year))
				{
					return true;
				}
			}
			return false;
		}
		
		function convertTimeFormat($RecordTime)
		{
			list($a, $b) = explode(':',$RecordTime);
			return sprintf("%02d:%02d",$a,$b);
		}
	
		function checkTimeFormat($RecordTime)
		{
			list($hour, $minutes) = explode(':',$RecordTime);
			if(strlen($hour)==2 && strlen($minutes)==2)
			{
				if($hour>=0 && $hour<=23 && $minutes>=0 && $minutes<=59)
				{
					return true;
				}
			}
			return false;
		}
		
		function checkDurationFormat($RecordTime)
		{
			list($minutes, $second) = explode(':',$RecordTime);
			if(strlen($minutes)==2 && strlen($second)==2)
			{
				if($minutes>=0 && $minutes<=59 && $second>=0 && $second<=59)
				{
					return true;
				}
			}
			return false;
		}
		
		function errorToRawHTML($errorArray)
		{
			if(empty($errorArray))
			{
				return '';
			}
			
			$raw = '*)' . $errorArray[0];
			for($i = 1,$iMax = count($errorArray); $i < $iMax; $i++)
			{
				$raw .= '<br />*)' . $errorArray[$i];
			}
			return $raw;
		}

		
	
		// Add active-checking to the SQL, by Pun
		function getLev1IDByLev2Code($code) // to libmedical.php
		{
			if ($code == "")
			{
				return false;
			}
			
			$sql = "SELECT Lev1ID FROM MEDICAL_STUDENT_LOG_LEV2 WHERE Lev2Code='$code' AND recordstatus = 1 AND DeletedFlag = 0";
			$objdb = new libdb();
			$rs = $objdb->returnResultSet($sql);
//			$rs = $this->objDB->returnResultSet($sql);
			return $rs;
		}
		
		
		
	}//close class
}



?>