<?php
// using by : 
/*
 * 	Log
 * 	
 * 	2017-06-15 Cameron	- create this file
 * 
 */
if (!defined("LIBEVENTTYPE_DEFINED"))                     // Preprocessor directive
{
        define("LIBEVENTTYPE_DEFINED", true);
		class eventType
        {
                private $objDB;

				private $EventTypeID;
				private $EventTypeName;
				private $RecordStatus;
				private $DeletedFlag;
				private $DeletedBy;
				private $DeletedDate;				
				private $DateInput;
				private $InputBy;
				private $DateModified;
				private $LastModifiedBy;

				public function eventType($eventTypeID = NULL){
					$this->objDB = new libdb();

					if($eventTypeID != ''){
						$this->setEventTypeID($eventTypeID);
						$this->loadDataFormStorage();
					}
				}

				public function setEventTypeID($val){$this->EventTypeID=$val;}
				public function getEventTypeID(){return $this->EventTypeID;}

				public function setEventTypeName($val){$this->EventTypeName=$val;}
				public function getEventTypeName(){return $this->EventTypeName;}

				public function setRecordStatus($val){$this->RecordStatus=$val;}
				public function getRecordStatus(){return $this->RecordStatus;}

				public function setDeletedFlag($val){$this->DeletedFlag=$val;}
				public function getDeletedFlag(){return $this->DeletedFlag;}

				public function setDeletedBy($val){$this->DeletedBy=$val;}
				public function getDeletedBy(){return $this->DeletedBy;}

				public function setDeletedDate($val){$this->DeletedDate=$val;}
				public function getDeletedDate(){return $this->DeletedDate;}

				public function setDateInput($val){$this->DateInput=$val;}
				public function getDateInput(){return $this->DateInput;}

				public function setInputBy($val){$this->InputBy=$val;}
				public function getInputBy(){return $this->InputBy;}

				public function setDateModified($val){$this->DateModified=$val;}
				public function getDateModified(){return $this->DateModified;}

				public function setLastModifiedBy($val){$this->LastModifiedBy=$val;}
				public function getLastModifiedBy(){return $this->LastModifiedBy;}

				private function loadDataFormStorage($loadObject=true, $whereCriteria= NULL,$orderCriteria = NULL)
				{

					$conds = '';
					if($loadObject){
						$conds = 'and EventTypeID = '.$this->getEventTypeID();
					}

					if($whereCriteria !== NULL){
							$conds .= ' and ('.$whereCriteria.')';
					}
					
					$orderBy = '';
					if ($orderCriteria !== NULL){
						$orderBy = ' ORDER BY ' . $orderCriteria;
					}

					$sql='select 
							  EventTypeID,
							  EventTypeName,
							  RecordStatus,
							  DeletedFlag,
							  DeletedBy,
							  DeletedDate,
							  DateInput,
							  InputBy,
							  DateModified,
							  LastModifiedBy 
						  from MEDICAL_EVENT_TYPE where 1=1 '.$conds . $orderBy;

					$rs = $this->objDB->returnResultSet($sql);

					if($loadObject){
						if(count($rs) > 1){
							  return false;
						}
						
						if(count($rs) ==1){
							$rs = current($rs);
							$this->setEventTypeID($rs['EventTypeID']);
							$this->setEventTypeName($rs['EventTypeName']);
							$this->setRecordStatus($rs['RecordStatus']);
							$this->setDeletedFlag($rs['DeletedFlag']);
							$this->setDeletedBy($rs['DeletedBy']);
							$this->setDeletedDate($rs['DeletedDate']);
							$this->setDateInput($rs['DateInput']);
							$this->setInputBy($rs['InputBy']);
							$this->setDateModified($rs['DateModified']);
							$this->setLastModifiedBy($rs['LastModifiedBy']);	
							return $this;
						}else{
							return NULL;
						}
					}
					else{
						return $rs;	
					}
				}

				public function save()
				{
					if($this->getEventTypeID()>0){
						$ret = $this->updateRecord();
					}else{
						$ret = $this->insertRecord();
					}
					return $ret;
				}
				
	
				public function getActiveStatus($orderCriteria = NULL)
				{
					$orderCriteria = ($orderCriteria == NULL) ? ' EventTypeName ' :$orderCriteria;
					return $this->getAllStatus($whereCriteria = ' recordstatus = 1 and DeletedFlag = 0 ',$orderCriteria);
				}

				public function getAllStatus($whereCriteria = NULL,$orderCriteria = NULL){
					$rs = $this->loadDataFormStorage($loadObject=false, $whereCriteria,$orderCriteria);
					return $rs;
				}

				
				public function recordDuplicateChecking($testName, $EventTypeID='')
				{
					if($testName == '')
					{
						return true;
					}
					
					$checkingResult = 0;
					$addtionSQL = '';
					if($EventTypeID != '')
					{
						$addtionSQL = ' AND EventTypeID != \'' . $EventTypeID . '\'';
					}
					
					$sql = 'SELECT COUNT(*) FROM MEDICAL_EVENT_TYPE WHERE DeletedFlag = 0 AND EventTypeName=\'' . trim($testName,'\'') . '\'' . $addtionSQL;
					$rs = $this->objDB->returnResultSet($sql);
					if($rs[0]['COUNT(*)'] > 0)
					{
						$checkingResult += 1;
					}
					
					return $checkingResult;
				}
				
				private function insertRecord(){
					global $cfg_medical;
					$DataArr = array();

					$DataArr['EventTypeName'] = $this->objDB->pack_value($this->getEventTypeName(),'str');
					$DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(),'int');
					$DataArr['DateInput'] = $this->objDB->pack_value('now()','date');
					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');

					$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

					$fieldStr= $sqlStrAry['sqlField'];
					$valueStr= $sqlStrAry['sqlValue'];


					$sql = 'Insert Into MEDICAL_EVENT_TYPE ('.$fieldStr.') Values ('.$valueStr.')';
					
					$success = $this->objDB->db_db_query($sql);
					if($success != 1){
						  return false;
					}
					
					$RecordID = $this->objDB->db_insert_id();
					$this->setEventTypeID($RecordID);
					return $RecordID;
				}

				private function updateRecord(){
					global $cfg_medical;

					$DataArr = array();

					$DataArr['EventTypeName'] = $this->objDB->pack_value($this->getEventTypeName(),'str');
					$DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(),'int');
					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
					$DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');

					foreach ($DataArr as $fieldName => $data)
					{
						$updateDetails .= $fieldName."=".$data.",";
					}

					//REMOVE LAST OCCURRENCE OF ",";
					$updateDetails = substr($updateDetails,0,-1);

					$sql = "update MEDICAL_EVENT_TYPE set ".$updateDetails." where EventTypeID = ".$this->getEventTypeID();

					$result = $this->objDB->db_db_query($sql);
								
					if($result != 1){
						  return false;
					}

					return $result;

				}
				
		}

}
?>