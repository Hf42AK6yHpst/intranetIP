<?php
// updating : 
/*
 * 	2017-09-20 [Cameron]
 * 		- add function presentInputNotShowDefault()
 * 
 */
if (!defined("LIBMEDICALSHOWINPUT_DEFINED"))                     // Preprocessor directive
{
	define("LIBMEDICALSHOWINPUT_DEFINED", true);
	class libMedicalShowInput{
		
		private static function _getModuleName($module){
			switch(strtoupper($module)){
				case 'MEAL':
					return 'meal';
				case 'BOWEL':
					return 'bowel';
				case 'STUDENTLOG':
					return 'studentLog';
				case 'SLEEP':
					return 'sleep';
			}
			return '';
		}
		
		public static function attendanceDependence($module){
			global $cfg_medical,$plugin;
			
			
			$moduleName = self::_getModuleName($module);
			if($moduleName == ''){
				$errMsg = 'Wrong Argment in '.__FUNCTION__.":\n".print_r(debug_backtrace(),true);
				alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				return false;
			}
			
			return $plugin['medical_module']['LinkAttendance'][$moduleName] && $plugin['attendancestudent'];
//			return $plugin['attendancestudent'];
		}
		
		public static function absentAllowedInput($module){
			global $cfg_medical,$plugin;
			
			$moduleName = self::_getModuleName($module);
			if($moduleName == ''){
				$errMsg = 'Wrong Argment in '.__FUNCTION__.":\n".print_r(debug_backtrace(),true);
				alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
				return false;
			}
			
			if(!self::attendanceDependence($module)){
				return true;
			}
			return $plugin['medical_module']['AbsentAllowedInput'][$moduleName];
		}
		
		public static function absentAllowedInputShowDefault($module){
			global $cfg_medical,$plugin;
			switch(strtoupper($module)){
				case 'MEAL':
					$module = 'meal';
					break;
				case 'SLEEP':
					$module = 'sleep';
					break;
				default:
					$errMsg = 'Wrong Argment in '.__FUNCTION__.":\n".print_r(debug_backtrace(),true);
					alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					return false;
			}
			return $plugin['medical_module']['AbsentAllowedInputShowDefault'][$module];
		}
		
		public static function presentInputNotShowDefault($module){
			global $cfg_medical,$plugin;
			switch(strtoupper($module)){
				case 'SLEEP':
					$module = 'sleep';
					break;
				default:
					$errMsg = 'Wrong Argment in '.__FUNCTION__.":\n".print_r(debug_backtrace(),true);
					alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					return false;
			}
			return $plugin['medical_module']['PresentInputNotShowDefault'][$module];
		}
		
		
	}
}
?>