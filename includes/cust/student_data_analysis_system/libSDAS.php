<?php
// Using :
// ################ Change Log [Start] #####
//  2020-08-06 (Philips)
//  Modified ComputeSDMean(), $TermAssessment should be in string formrat when compare
//  Date:2019-05-27 [Philips]
//  Modified checkAccessRight(), added access group rights checking for subject teacher, class teacher, subject panel and MonitoringPIC
//  Added getAccessGroup()
//  Added getAccessGroupRights()
//  Added getAccessGroupMemberRights()
//  Added getAccessGroupMember()
//  Added insertAccessGroup()
//  Added insertAccessGroupRight()
//  Added updateAccessGroup()
//  Added updateAccessGroupRight()
//  Added updateAccessGroupMember()
//  Added insertAccessGroupMember()
//  Added deleteAccessGroupMember()
//  Added deleteAccessGroup()
//  Added deleteAccessGroupRight()
//
//  Date:2019-05-15 [Anna]
//  Hide monthly report for admin
//
// Date: 2019-04-03 [Anna]
// Modified getMonitoringGroupMember(), added yearid for case 155830
//
// Date: 2019-02-19 [Philips]
// Modified checkAccessRight() to allow Monthly Report Section PIC to use MonthlyReport
//
// Date: 2019-02-13 [Philips]
// Added getMonthlyReportSectionPIC() and getMonthlyReportSectionRight(), to get Details of Monthly Report Access Right
//
// Date: 2018-07-27 [Bill]
// - modified GET_MODULE_OBJ_ARR(), to add Report Comparison to side menu
//
// Date: 2018-03-12 [Omas]
// - modified ComputeSDMean() - add academicYear condition for getting YearClassID from YEAR_CLASS
//
// Date: 2018-01-23 [Pun]
// - modified GET_MODULE_OBJ_ARR(), added internal_value_added cust report for Maryknoll
//
// Date: 2018-01-22 [Anna]
// - added MonthlyReportPIC accessright to monthly report
//
// Date: 2018-01-11 [Omas] #F133979
// - modified ComputeSDMean(), only count result with socre > -1
//
// Date: 2017-12-12 [Pun]
// - modified Get_Subject_Selection(), added $firstTitle
//
// Date: 2017-12-12 [Pun]
// - modified ComputeSDMean(), added cust T1A3 hardcode full mark
//
// Date: 2017-11-23 [Omas]
// - added monthly report for TW group
//
// Date: 2017-08-25 [Pun]
// modified GET_MODULE_OBJ_ARR() - added cust report valueAddedDataReport
//
// Date: 2017-06-27 [Villa]
// modified checkAccessRight() - add monitoring group PIC access right for the report of Class Performance/ Academic Progress
//
// Date: 2017-06-23 [Villa]
// modified getAssessmentStatReportAccessRight() - add monitorgroupPIC
// addd getMonitoringGroupMember(), getMonitoringGroupMember()
//
// Date: 2017-06-02 [Villa] #X116199
// modified ComputeSDMean() - skip case score='-1' && grade = '*'
//
// Date: 2017-05-26 [Villa]
// modified GET_MODULE_OBJ_ARR() - added form passing rate
//
// Date: 2017-05-25 [Pun]
// modified GET_MODULE_OBJ_ARR() - added Learn and Teach report
//
// Date: 2017-04-21 [Villa]
// modified Get_Subject_Selection() - filter the subject deleted
//
// Date: 2017-04-20 [Villa]
// add ComputeSDMean(), sd(), sd_square
//
// Date: 2017-03-13 [Villa]
// modified Get_Subject_Selection() - add para subjectIdArr allow showing the subject in the Arr only FOR SubjectArr is not NULL
//
// Date: 2017-03-06 [Omas]
// revised GET_MODULE_OBJ_ARR() $pagesArr
//
// DAte: 2017-02-17 [Omas]
// added jupas to the menu
//
// Date: 2017-01-10 [Villa]
// add Get_Subject_Selection() - generate selection box to select subject including subject component group by category
//
// Date: 2016-12-13 [Villa]
// add checkZeroDot
//
// Date: 2016-12-09 [Villa]
// modified access right - subject_class_distribution
//
// Date: 2016-12-07 [Villa]
// modified GET_FILTER_TABLE() - set default to hide the option
//
// Date: 2016-12-06 [Omas]
// modified access right - allow subject teacher to academic_progress for catholic
//
// Date: 2016-11-28 [Villa]
// modified GET_MODULE_OBJ_ARR[] - add form student performance
//
// Date: 2016-11-23 [Villa]
// add GET_FILTER_TABLE() - output advance filter search
// modified GET_MODULE_OBJ_ARR[] - add class subject distribation
//
// Date: 2016-08-23 [Omas] [ip.2.5.7.10.1]
// modified GET_MODULE_OBJ_ARR() and checkAccessRight() - added dse confirmation for tungwah
//
// Date: 2016-07-08 [Omas] [ip.2.5.7.7.1]
// modified GET_MODULE_OBJ_ARR() - add flag $plugin['SDAS']['hideDsePrediction'] for hiding dse prediction
//
// Date: 2016-06-08 [Omas] [ip.2.5.7.7.1]
// Modified GET_MODULE_OBJ_ARR(), added DSE prediction report, added allow F.6 class teacher only to DSE prediction
//
// Date: 2016-04-07 [Pun] [ip.2.5.7.4.1]
// Modified GET_MODULE_OBJ_ARR(), added general skin for Student Analysis Data System
//
// Date: 2016-02-29 Pun
// Modified checkAccessRight(), added block subject panel, class teacher, subject teacher flag
//
// Date: 2016-02-26 Pun
// Modified GET_MODULE_OBJ_ARR(), change name for tungwah
//
// Date: 2016-02-25 Omas
// Modified getAssessmentStatReportAccessRight() - added a temp arr to store access right , avoid same sql rerun
//
// Date: 2016-02-19 Pun
// Modified checkAccessRight(), add checking for AcademicProgress
//
// Date: 2016-02-19 Pun
// Modified GET_MODULE_OBJ_ARR(), change name and re-order all reports
//
// Date: 2016-01-21 Omas
// Added "management > submitting to CEES" Section - tungwah only $plugin['StudentDataAnalysisSystem_Style']
//
// Date: 2016-02-17 Pun
// Modified checkAccessRight(), isSuperAdmin(), added SDAS role in Role Management
// Modified GET_MODULE_OBJ_ARR(), changed access right menu lang
//
// Date: 2016-02-12 Pun
// Modified getAssessmentStatReportAccessRight() fixed code duplicate with libportfolio.php
//
// Date: 2016-01-26 Pun
// Added "Percentile Setting" Section
//
// Date: 2016-01-25 Pun
// Modified checkAccessRight() add subject panel access right control by $plugin['SDAS_module']['accessRight']['subjectPanel']
//
// Date: 2016-01-21 Omas
// Added "management" Section
//
// ################# Change Log [End] ######
if (! defined("LIBSDAS_DEFINED")) // Preprocessor directives
{
    define("LIBSDAS_DEFINED", true);

    class libSDAS extends libdb
    {

        /**
         * Get the Left Sidebar, please set this variable before calling this.
         * $PATH_WRT_ROOT: The relative path to base
         * $CurrentPage: The current page (For highlighting)
         */
        function GET_MODULE_OBJ_ARR()
        {
            global $PATH_WRT_ROOT, $Lang, $CurrentPage, $LAYOUT_SKIN, $plugin, $sys_custom, $intranet_root;
            global $ec_iPortfolio;
            
            // ### Init START ####
            $ModulePath = $PATH_WRT_ROOT . "home/student_data_analysis_system/";
            $user = new libuser($_SESSION['UserID']);
            // ### Init END ####
        
            // ### Define Page START ####
            // academic
            $pagesArr['academic'][] = array(
                'id' => 'academic_progress',
                'lang' => $Lang['SDAS']['menu']['AcademicProgress'],
                'path' => $ModulePath . '?t=academic.academic_progress.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'subject_performance_statistic',
                'lang' => $Lang['SDAS']['menu']['ClassSubjectComparison'],
                'path' => $ModulePath . '?t=academic.subject_performance_statistic.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'class_subject_performance_summary',
                'lang' => $Lang['SDAS']['menu']['ClassPerformance'],
                'path' => $ModulePath . '?t=academic.class_subject_performance_summary.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'subject_class_distribution',
                'lang' => $Lang['SDAS']['menu']['SubjectClassDistribution'],
                'path' => $ModulePath . '?t=academic.subject_class_distribution.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'dse_stats',
                'lang' => $Lang['SDAS']['menu']['DseAnalysis'],
                'path' => $ModulePath . '?t=academic.dse_stats.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'dse_prediction',
                'lang' => $Lang['SDAS']['menu']['DsePrediction'],
                'path' => $ModulePath . '?t=academic.dse_prediction.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'form_student_performance',
                'lang' => $Lang['SDAS']['menu']['FormStudentPerformance'],
                'path' => $ModulePath . '?t=academic.form_student_performance.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'jupas_analysis',
                'lang' => $Lang['SDAS']['JupasAnalysis'],
                'path' => $ModulePath . '?t=academic.jupas.basic_stat'
            );
            $pagesArr['academic'][] = array(
                'id' => 'passing_stats_by_teacher',
                'lang' => $Lang['SDAS']['menu']['PassingRateStats'],
                'path' => $ModulePath . '?t=academic.passing_stats_by_teacher.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'passing_stats_by_teacher',
                'lang' => $Lang['SDAS']['menu']['PassingRateStats'],
                'path' => $ModulePath . '?t=academic.passing_stats_by_teacher.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'student_crossyear_result',
                'lang' => $Lang['SDAS']['menu']['StudentCrossYearPerformance'],
                'path' => $ModulePath . '?t=academic.student_crossyear_result.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'student_marksheets',
                'lang' => $Lang['SDAS']['menu']['StudentPerformanceTracking'],
                'path' => $ModulePath . '?t=academic.student_marksheets.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'student_marksheets',
                'lang' => $Lang['SDAS']['menu']['StudentPerformanceTracking'],
                'path' => $ModulePath . '?t=academic.student_marksheets.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'subject_improvements',
                'lang' => $Lang['SDAS']['menu']['SubjectImprovementStats'],
                'path' => $ModulePath . '?t=academic.subject_improvements.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'subject_statistics',
                'lang' => $Lang['SDAS']['menu']['SubjectStats'],
                'path' => $ModulePath . '?t=academic.subject_statistics.search'
            );
            $pagesArr['academic'][] = array(
                'id' => 'improvement_stats_by_teacher',
                'lang' => $Lang['SDAS']['menu']['TeachingImprovementStats'],
                'path' => $ModulePath . '?t=academic.improvement_stats_by_teacher.search'
            );
            
            $pagesArr['academic'][] = array(
                'id' => 'pres1_analysis',
                'lang' => $Lang['SDAS']['menu']['PreS1Analysis'],
                'path' => $ModulePath . '?t=academic.pres1_analysis.search'
            );
            // custom Report
            if ($plugin['SDAS_module']['customReport']['promotionAssessment']) {
                $pagesArr['customReport'][] = array(
                    'id' => 'report_promotion',
                    'lang' => $Lang['SDAS']['menu']['ClassPromotionAssessment'],
                    'path' => $ModulePath . '?t=reports.promotion.index'
                );
            }
            if ($plugin['SDAS_module']['customReport']['kcmc_form_passing_rate']) {
                $pagesArr['academic'][] = array(
                    'id' => 'form_passing_rate',
                    'lang' => $Lang['SDAS']['menu']['FormPassingRate'],
                    'path' => $ModulePath . '?t=academic.form_passing_rate.form_passing_rate'
                );
            }
            if ($sys_custom['iPf']['learnAndTeachReport']) {
                $pagesArr['academic'][] = array(
                    'id' => 'learn_and_teach',
                    'lang' => $Lang['SDAS']['menu']['LearnAndTeach'],
                    'path' => $ModulePath . '?t=academic.learn_and_teach.search'
                );
            }
            
            if ($sys_custom['iPf']['valueAddedDataReport']) {
                $pagesArr['academic'][] = array(
                    'id' => 'value_added_data',
                    'lang' => $Lang['SDAS']['menu']['ValueAddedData'],
                    'path' => $ModulePath . '?t=academic.value_added_data.search'
                );
            }
            
            if ($sys_custom['iPf']['Report']['internal_value_added']['MSS']) { // Maryknoll cust report
                $pagesArr['academic'][] = array(
                    'id' => 'internal_value_added_index',
                    'lang' => $Lang['iPortfolio']['InternalValueAdded'],
                    'path' => $ModulePath . '?t=academic.internal_value_added_index.search'
                );
            }
            
            if ($sys_custom['iPf']['Report']['report_comparsion']) {     // HKUGAC cust report
                $pagesArr['academic'][] = array(
                    'id' => 'report_comparsion',
                    'lang' => $Lang['SDAS']['menu']['ReportComparsion'],
                    'path' => $ModulePath . '?t=academic.report_comparsion.search'
                );
            }
            
            // management
            $pagesArr['management'][] = array(
                'id' => 'management.importData',
                'lang' => $Lang['SDAS']['menu']['examMgmt'],
                'path' => $ModulePath . '?t=management.data_import.index'
            )
            ;
            // $pagesArr['management'][] = array(
            // 'id' => 'management.importDataNew',
            // 'lang' => $Lang['SDAS']['menu']['examMgmt'].' (Please do not deploy this, thx)',
            // 'path' => $ModulePath.'?t=management.data_import.index_new'
            // );
            if ($plugin['StudentDataAnalysisSystem_Style'] == "tungwah") {
                $pagesArr['management'][] = array(
                    'id' => 'management.CEES',
                    'lang' => $Lang['SDAS']['menu']['CEES'],
                    'path' => $ModulePath . '?t=management.to_cees.index'
                )
                ;
                $pagesArr['management'][] = array(
                    'id' => 'management.Endorse',
                    'lang' => $Lang['SDAS']['toCEES']['PrincipalConfirm'],
                    'path' => $ModulePath . '?t=management.to_cees.endorse'
                )
                ;
                global $_SESSION;
                $pagesArr['management'][] = array(
                    'id' => 'management.MonthlyReport',
                    'lang' => $Lang['SDAS']['CEES']['MonthlyReport'],
                    'path' => $PATH_WRT_ROOT . 'home/cees/monthlyreport/',
                    'extraAttr' => 'target="_blank"'
                );
            }
            // settings
            $pagesArr['settings'][] = array(
                'id' => 'settings.assessmentStatReport',
                'lang' => $Lang['SDAS']['menu']['accessRight'],
                'path' => $ModulePath . '?t=settings.assessmentStatReport.accessRightConfigAdmin'
            )
            ;
            $pagesArr['settings'][] = array(
                'id' => 'settings.percentileSetting',
                'lang' => $Lang['SDAS']['menu']['percentile'],
                'path' => $ModulePath . '?t=settings.percentileSetting.setting'
            );
            $pagesArr['settings'][] = array(
                'id' => 'settings.fullmarkSetting',
                'lang' => $Lang['iPortfolio']['SubjectFullMark'],
                'path' => $ModulePath . '?t=settings.fullmarkSetting.subject_full_mark'
            );
            if ($plugin['SDAS_module']['customReport']['promotionAssessment']) {
                $pagesArr['settings'][] = array(
                    'id' => 'settings.customReport',
                    'lang' => $Lang['SDAS']['menu']['SchBaseReportConfig'],
                    'path' => $ModulePath . '?t=reports.promotion.config'
                )
                ;
            }
            $pagesArr['settings'][] = array(
                'id' => 'settings.monitoringGroup',
                'lang' => $Lang['SDAS']['menu']['MonitoringGroupSetting'],
                'path' => $ModulePath . '?t=settings.monitoringGroupSetting.list'
            );
            // ### Define Page END ####
            
            // ### Initialize START ####
            // Set variable to zero, just for safe.
            foreach ($pagesArr as $pageGroup => $pages) {
                $$pageGroup = 0;
                foreach ($pages as $page) {
                    ${$page['id']} = 0;
                }
            }
            // ### Initialize END ####
            
            // ### Highlight the Current Page START ####
            // Set the releative group and page to 1 (It will Highlight the hyperlink)
            foreach ($pagesArr as $pageGroup => $pages) {
                foreach ($pages as $page) {
                    if ($page['id'] == $CurrentPage) {
                        $$pageGroup = 1;
                        ${$page['id']} = 1;
                        break 2;
                    }
                }
            }
            // ### Highlight the Current Page END ####
        
            // ### Menu Display START ####
            $MenuArr = array();
            foreach ($pagesArr as $pageGroup => $pages) {
                if ($this->checkAccessRight($pageGroup)) {
                    // # Check The group has sub-item or not START ##
                    $hasSubItem = false;
                    foreach ($pages as $p) {
                        if ($this->checkAccessRight($pageGroup, $p['id'])) {
                            $hasSubItem = true;
                            break;
                        }
                    }
                    // # Check The group has sub-item or not END ##
                    
                    // # Show The group START ##
                    if ($hasSubItem) {
                        $MenuArr[$pageGroup] = array(
                            $Lang['SDAS']['menu'][$pageGroup],
                            "",
                            $$pageGroup
                        );
                    }
                    // # Show The group END ##
                    
                    // # Show The sub-item START ##
                    if ($hasSubItem) {
                        foreach ($pages as $p) {
                            if ($this->checkAccessRight($pageGroup, $p['id'])) {
                                // 3 is for customize icon on menu
                                $MenuArr[$pageGroup]["Child"][$p['id']] = array(
                                    $p['lang'],
                                    $p['path'],
                                    ${$p['id']},
                                    "",
                                    $p['extraAttr']
                                );
                            }
                        }
                    }
                    // # Show The sub-item END ##
                }
            }
            
            // ### Menu Display END ####
            
            // ### Module Information START ####
            $sdasSkin = ($plugin['StudentDataAnalysisSystem_Style']) ? $plugin['StudentDataAnalysisSystem_Style'] : 'general';
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentDataAnalysisSystem'][$sdasSkin];
            $MODULE_OBJ['title_css'] = "menu_opened";
            if ($plugin['StudentDataAnalysisSystem_Style_User'][$user->UserLogin]) {
                $iconFolder = $plugin['StudentDataAnalysisSystem_Style_User'][$user->UserLogin];
                $iconPath = "{$PATH_WRT_ROOT}/images/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$iconFolder}/";
            } else 
                if ($sdasSkin) {
                    $iconPath = "{$PATH_WRT_ROOT}/images/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$sdasSkin}/";
                } else {
                    $iconPath = "{$PATH_WRT_ROOT}/images/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/general/";
                }
            $MODULE_OBJ['logo'] = "{$iconPath}/icon_student.png";
            $MODULE_OBJ['selectedMenuIcon'] = "{$iconPath}/icon_sub_on.gif";
            $MODULE_OBJ['menu'] = $MenuArr;
            // ### Module Information END ####
            return $MODULE_OBJ;
        }
 // End GET_MODULE_OBJ_ARR()
        function isSuperAdmin()
        {
            return $_SESSION["SSV_USER_ACCESS"]["other-SDAS"];
        }
 // End isSuperAdmin()
        
        /**
         * This function is for checking the access right.
         */
        function checkAccessRight($module = '', $page = '', $subpage = '')
        {
            global $plugin, $sys_custom, $intranet_db;
            if (! $plugin['StudentDataAnalysisSystem']) {
                return false;
            }
            
            if ($sys_custom['SDAS']['CEES']['ExcludeSDASfunctions']) {
                if ($module == 'academic') {
                    return false;
                } elseif ($module == 'management') {
                    if ($page != 'management.MonthlyReport' && $page != '')
                        return false;
                } elseif ($module == 'settings') {
                    if ($page != 'assessmentStatReport' && $page != 'settings.assessmentStatReport' && $page != '')
                        return false;
                } else {
                    return false;
                }
            }
            
            if ($module == 'ajax') {
                return true;
            }
            
            $accessRight = $this->getAssessmentStatReportAccessRight();

            // ### Cust Module START ####
            // ### SKH TST cust START#######
            if (isset($sys_custom['SDAS']['SKHTST']['FullAccessReport']) && $module == 'academic') {
                if (in_array($page, $sys_custom['SDAS']['SKHTST']['FullAccessReport'])) {
                    return true;
                }
            }
            if (isset($sys_custom['SDAS']['ClassTeacher']['BlockAccessReports']) && $accessRight['admin'] != 1 && ! empty($accessRight['classTeacher']) && $module == 'academic') {
                if (in_array($page, $sys_custom['SDAS']['ClassTeacher']['BlockAccessReports'])) {
                    return false;
                }
            }
            // ### SKH TST cust END #######
            
            if ( // Hide academic_progress report if this flag does not set
! $sys_custom['iPf']['Report']['AssessmentStatisticReport']['AcademicProgress'] && $module == 'academic' && $page == 'academic_progress') {
                return false;
            }
            
            if ( // DSE only for advance mode
! $plugin['SDAS_module']['advanceMode'] && $page == 'dse_stats') 
            // ($page == 'dse_stats' || $page == 'dse_prediction')
            {
                return false;
            }
            
            if (($plugin['SDAS']['hideDsePrediction'] || ! $plugin['SDAS_module']['advanceMode']) && $page == 'dse_prediction') {
                return false;
            }
            
            if (! $plugin['SDAS_module']['advanceMode'] && $page == 'jupas_analysis') {
                return false;
            }
            
            if ($plugin['StudentDataAnalysisSystem_Style'] != "tungwah" && $page == 'to_cees') {
                return false;
            }
            if ($page == 'to_cees' && ! ($this->isSuperAdmin())) {
                if ($subpage == 'index' || ($subpage == 'endorse' && ! $accessRight['admin'])) {
                    return false;
                }
            }
            if ($page == 'promotion') {
                if (! $plugin['SDAS_module']['customReport']['promotionAssessment'] || ! $this->isSuperAdmin()) {
                    return false;
                }
            }
            // ### Cust Module END ####

            // ### Admin AccessRight START ####
            if ($this->isSuperAdmin() && $plugin['StudentDataAnalysisSystem_Style'] != "tungwah") {
                return true;
            }
         
            if ($module == 'academic' && $accessRight['admin']) { // SDAS PIC can see all reports only
                return true;
            }
            // ### Admin AccessRight END ####
            
            // ### Subject Panel AccessRight START ####
            if ($module == 'academic' && ! $plugin['SDAS_module']['accessRight']['blockSubjectPanel'] && $accessRight['subjectPanel']) {
                if (isset($plugin['SDAS_module']['accessRight']['subjectPanel'])) {
                    if (in_array($page, $plugin['SDAS_module']['accessRight']['subjectPanel'])) {
                        return true;
                    }
                } else {
                    // Default these reports are show for subject panel 
                    if ($page == 'subject_statistics' || $page == 'subject_improvements' || $page == 'internal_value_added_index' || 
                        $page == 'dse_stats' || ($page == 'learn_and_teach' && $sys_custom['iPf']['learnAndTeachReport']) || ($page == 'report_comparsion' && $sys_custom['iPf']['Report']['report_comparsion'])) {
                        return true;
                    }
                }
            }
            // ### Subject Panel AccessRight END ####
            
            // ### Class/Subject Teacher/ MonitoringGroupPIC AccessRight START ####
            if ($module == 'academic') {
                global $_SESSION;
                $rights = $this->getAccessGroupMemberRights($_SESSION['UserID']);
                if(sizeof($rights) > 0){
                    foreach($rights as $right){
                        if($page == 'jupas'){
                            $page = 'jupas_analysis';
                        }
                        if($page == $right['AcademicStatisticName']){
                            return true;
                        }
                    }
                }
                switch ($page) {
                    case '':
                        return true;
                    
                    case 'class_subject_performance_summary':
                        return ! $plugin['SDAS_module']['accessRight']['blockClassTeacher'] && ($accessRight['classTeacher'] || $accessRight['subjectTeacher'] || $accessRight['subjectPanel'] || $accessRight['MonitoringGroupPIC']);
                    case 'subject_performance_statistic':
                        return ! $plugin['SDAS_module']['accessRight']['blockClassTeacher'] && $accessRight['classTeacher'];
                    case 'class_marksheets':
                        return ! $plugin['SDAS_module']['accessRight']['blockClassTeacher'] && ($accessRight['classTeacher'] || $accessRight['subjectTeacher'] || $accessRight['subjectPanel'] || $accessRight['MonitoringGroupPIC']);
                    case 'student_marksheets':
                        return ! $plugin['SDAS_module']['accessRight']['blockClassTeacher'] && $accessRight['classTeacher'];
                    case 'subject_class_distribution':
                        return $accessRight['admin'] || $accessRight['subjectPanel'];
                    case 'form_student_performance':
                        return $accessRight['admin'] || $accessRight['MonitoringGroupPIC'] || (isset($sys_custom['SDAS']['SKHTST']) && $accessRight['classTeacher']);
                    case 'student_crossyear_result':
                        return $accessRight['admin'] || $accessRight['classTeacher'];
                    case 'passing_stats_by_teacher':
                    case 'improvement_stats_by_teacher':
                        return ! $plugin['SDAS_module']['accessRight']['blockSubjectTeacher'] && $accessRight['subjectTeacher'];
                    case 'pres1_analysis':
                        return $accessRight['admin'];
                    case 'academic_progress':
                        return ($plugin['StudentDataAnalysisSystem_Style'] == 'catholic' || false || $accessRight['MonitoringGroupPIC']);
                    case 'dse_stats':
                        return ($sys_custom['SDAS']['allTeacherAccessDSE'] || false); // $accessRight['classTeacher'] || $accessRight['subjectTeacher'];
                    case 'dse_prediction':
                        $sql = "Select YearID From $intranet_db.YEAR Where WEBSAMSCode REGEXP '4|5|6'";
                        $formSixYearIDs = $this->returnVector($sql);
                        $classTeacherYearIDs = Get_Array_By_Key($accessRight['classTeacher'], 'YearID');
                        $tempArr = array_intersect((array) $formSixYearIDs, (array) $classTeacherYearIDs);
                        $allow = (! empty($tempArr));
                        return $sys_custom['SDAS']['allTeacherAccessDSE'] || (! $plugin['SDAS_module']['accessRight']['blockClassTeacher'] && $allow && ! $plugin['SDAS']['hideDsePrediction'] && ! $sys_custom['SDAS']['ClassTeacher']['HideDSEPrediction']); // F.6 teacher
                    case 'learn_and_teach':
                        return ($sys_custom['iPf']['learnAndTeachReport']);
                    case 'report_comparsion':
                        return $sys_custom['iPf']['Report']['report_comparsion'];
                }
            }
            // ### Class/Subject Teacher AccessRight START ####
            
            // ### Management AccessRight START ####
            // principal ?
           
            if ($plugin['StudentDataAnalysisSystem_Style'] == "tungwah") {
                if ($module == 'management' && $this->isSuperAdmin()) {
                    if ($page != '') {
                        switch ($page) {                          
                            //                             case 'management.importData':
                            //                             case 'management.CEES':
                            case 'management.MonthlyReport':
                                if($this->isMonthlyReportPIC($_SESSION['UserID'])){
                                    return true;
                                    break;
                                }else{
                                    return false;
                                    break;
                                }
                            case 'management.importData':
                            case 'management.CEES':
                            case 'management.Endorse':
                            case 'to_cees':
                            case 'data_import':
                                return true;
                                break;
                            default:
                                return false;
                        }
                    }
                    return true;
                }
            
                if ($module == 'management' && $accessRight['admin']) {

                    if ($page != '') {
                        switch ($page) {
//                             case 'management.importData':
//                             case 'management.CEES':
                            case 'management.MonthlyReport':
                                if($this->isMonthlyReportPIC($_SESSION['UserID'])){
                                    return true;
                                    break;
                                }else{
                                    return false;
                                    break;
                                }                               
                            case 'management.importData':
                            case 'management.CEES':
                            case 'management.Endorse':                           
                            case 'to_cees':
                            case 'data_import':
                                return true;
                                break;
                            default:
                                return false;
                        }
                    }
                    return true;
                }               
                
//                 if ($module == 'management' && $accessRight['admin']) {
//                     if ($page != '') {
//                         switch ($page) {
//                             case 'management.importData':
//                             case 'management.CEES':
//                                 return false;
//                                 break;
//                             case 'management.Endorse':
//                             case 'management.MonthlyReport':
//                             case 'to_cees':
//                                 return true;
//                                 break;
//                             default:
//                                 return false;
//                         }
//                     }
//                     return true;
//                 }
               
//                 if ($module == 'management' && $accessRight['MonthlyReportPIC']) {
//                     if ($page != '') {
//                         switch ($page) {
//                             case 'management.importData':
//                             case 'management.CEES':
//                             case 'management.Endorse':
//                             case 'to_cees':
//                                 return false;
//                                 break;
//                             case 'management.MonthlyReport':
//                                 return true;
//                                 break;
//                             default:
//                                 return false;
//                         }
//                     }
//                     return true;
//                 }
                
                //global $_SESSION;
                if ($module == 'management' && $this->isMonthlyReportPIC($_SESSION['UserID'])) {
                    if ($page != '') {
                        switch ($page) {
                            case 'management.importData':
                            case 'management.CEES':
                            case 'management.Endorse':
                            case 'to_cees':
                                return false;
                                break;
                            case 'management.MonthlyReport':
                                return true;
                                break;
                            default:
                                return false;
                        }
                    }
                    return true;
                }
            }
            if ($this->isSuperAdmin() && $plugin['StudentDataAnalysisSystem_Style'] == "tungwah") {
                return true;
            }
            if ($module == 'management') {
                return false; // Only super admin can access this page
            }
            // ### Management AccessRight END ####
            
            // ### Setting AccessRight START ####
            if ($module == 'settings') {
                return false; // Only super admin can access this page
            }
            // ### Setting AccessRight END ####
            
            return false;
        }
 // End checkAccessRight()
        private $accessRightUserArr;

        function getAssessmentStatReportAccessRight($userID = '')
        {
            if (isset($this->accessRightUserArr[$userID])) {
                $accessRight = $this->accessRightUserArr[$userID];
            } else {
                global $PATH_WRT_ROOT;
                include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
                
                $lpf = new libportfolio();
                $accessRight = $lpf->getAssessmentStatReportAccessRight($userID);
                
                $this->accessRightUserArr[$userID] = $accessRight;
                // Monitoring Group
                $rs = $this->getMonitoringGroupMember('1', '');
                foreach ((array) $rs as $_rs) {
                    $MonitorArr[$_rs['UserID']][$_rs['GroupID']] = $_rs['GroupID'];
                    $MonitorArr['Admin'][$_rs['GroupID']] = $_rs['GroupID'];
                }
                if ($this->accessRightUserArr[$userID]['admin']) {
                    $this->accessRightUserArr[$userID]['MonitoringGroupPIC'] = $MonitorArr['Admin'];
                } else {
                    $this->accessRightUserArr[$userID]['MonitoringGroupPIC'] = $MonitorArr[$_SESSION['UserID']];
                }
                $accessRight = $this->accessRightUserArr[$userID];
            }
            
            return $accessRight;
            /*
             * Remove duplicate code with libportfolio.php * /
             * if($userID == ''){
             * $userID = $_SESSION['UserID'];
             * }
             * $academicYearID = Get_Current_Academic_Year_ID();
             *
             * $accessRight = array();
             *
             * ######### Get access right data START #########
             * $sql = "SELECT
             * RecordID,
             * UserID,
             * AccessType,
             * SubjectID,
             * YearID
             * FROM
             * ASSESSMENT_ACCESS_RIGHT
             * WHERE
             * UserID = '{$userID}'";
             * $rs = $this->returnResultSet($sql);
             * $rs = BuildMultiKeyAssoc($rs, array('UserID', 'AccessType', 'SubjectID', 'YearID') , array('RecordID'), $SingleValue=1);
             * ######### Get access right data END #########
             *
             *
             * ######### Get Admin Access Right START #########
             * if(count($rs[$userID]['0']) > 0){
             * $accessRight['admin'] = true;
             * }
             * ######### Get Admin Access Right END #########
             *
             *
             * ######### Get Subject Panel Access Right START #########
             * $accessRight['subjectPanel'] = $rs[$userID]['1']; // $accessRight['subjectPanel'][SubjectID][YearID]
             * ######### Get Subject Panel Access Right END #########
             *
             *
             * ######### Get Teaching Access Right START #########
             * $sql = "SELECT DISTINCT
             * ST.SubjectID
             * FROM
             * SUBJECT_TERM_CLASS_TEACHER STCT
             * INNER JOIN
             * SUBJECT_TERM ST
             * ON
             * STCT.SubjectGroupID = ST.SubjectGroupID
             * AND
             * STCT.UserID = '{$userID}'
             * INNER JOIN
             * ACADEMIC_YEAR_TERM AYT
             * ON
             * ST.YearTermID = AYT.YearTermID
             * AND
             * AYT.AcademicYearID = '{$academicYearID}'";
             * $rs = $this->returnVector($sql);
             * $accessRight['subjectTeacher'] = $rs;
             * ######### Get Teaching Access Right END #########
             *
             *
             * ######### Get Class Teacher Access Right START #########
             * $sql = "SELECT
             * YC.*
             * FROM
             * YEAR_CLASS_TEACHER YCT
             * INNER JOIN
             * YEAR_CLASS YC
             * ON
             * YCT.YearClassID = YC.YearClassID
             * INNER JOIN
             * YEAR Y
             * ON
             * YC.YearID = Y.YearID
             * WHERE
             * YCT.UserID = '{$userID}'
             * AND
             * YC.AcademicYearID = '{$academicYearID}'
             * ORDER BY
             * Y.Sequence, YC.Sequence";
             * $rs = $this->returnResultSet($sql);
             * $accessRight['classTeacher'] = $rs;
             * ######### Get Class Teacher Access Right END #########
             *
             * return $accessRight;
             * /*
             */
        }
 // End getAssessmentStatReportAccessRight()
        public function GET_FILTER_TABLE()
        {
            global $PATH_WRT_ROOT, $Lang, $CurrentPage, $LAYOUT_SKIN, $plugin, $intranet_root;
            global $ec_iPortfolio, $iPort;
            
            include_once ($PATH_WRT_ROOT . "/includes/libinterface.php");
            $linterface = new interface_html();
            // ##### Filter Table START ######
            $filterAry = $iPort['filter'];
            $i = 0;
            foreach ($filterAry as $filterAry_name) {
                $filterArr[$i][0] = $filterAry_name;
                $filterArr[$i][1] = 'SEN_filter[]';
                $filterArr[$i][2] = $filterAry_name;
                $filterArr[$i][5] = $filterAry_name;
                $i ++;
            }
            $filterCheckBox = $linterface->Get_Checkbox_Table('SENfilter', $filterArr, 5, 0);
            $filterTable = "";
            $filterTable .= "<table style='width: 824px;'>";
            $filterTable .= "<tr>";
            // $filterTable .= "<td style='width: 20%;'>";
            // $filterTable .= $iPort['AdvancedSearch'];
            // $filterTable .= "</td>";
            $filterTable .= "<td style='width: 100%;' colspan ='2'>";
            $filterTable .= "<input type='radio' id='Advanced_Search_y' name='Advanced_Search' value='1' onclick='AdvancedSearch(this)' ><label for='Advanced_Search_y'>" . $ec_iPortfolio['SLP']['Yes'] . "</label>";
            $filterTable .= "<input type='radio' id='Advanced_Search_n' name='Advanced_Search' value='0' onclick='AdvancedSearch(this)'checked><label for='Advanced_Search_n'>" . $ec_iPortfolio['SLP']['No'] . "</label>";
            $filterTable .= "</td>";
            $filterTable .= "</tr>";
            $filterTable .= "<tr class='advanceFilter' style='display:none'>";
            $filterTable .= "<td>";
            $filterTable .= $iPort['Gender'];
            $filterTable .= ": </td>";
            $filterTable .= "<td>";
            $filterTable .= "<input type='radio' id='gender_L' name='gender' class='gender' value='L' checked><label for='gender_L'>" . $iPort['NotLimit'] . "</label>";
            $filterTable .= "<input type='radio' id='gender_M' name='gender' class='gender' value='M'><label for='gender_M'>" . $iPort['GenderM'] . "</label>";
            $filterTable .= "<input type='radio' id='gender_F' name='gender' class='gender' value='F'><label for='gender_F'>" . $iPort['GenderF'] . "</label>";
            $filterTable .= "</td>";
            $filterTable .= "</tr>";
            $filterTable .= "<tr class='advanceFilter' style='display:none'>";
            $filterTable .= "<td>";
            $filterTable .= $iPort['NonChinese'];
            $filterTable .= ": </td>";
            $filterTable .= "<td>";
            $filterTable .= "<input type='radio' id='non_chinese_l' name='non_chinese' value='2' checked><label for='non_chinese_l'>" . $iPort['NotLimit'] . "</label>";
            $filterTable .= "<input type='radio' id='non_chinese_y' name='non_chinese' value='1'><label for='non_chinese_y'>" . $ec_iPortfolio['SLP']['Yes'] . "</label>";
            $filterTable .= "<input type='radio' id='non_chinese_n' name='non_chinese' value='0'><label for='non_chinese_n'>" . $ec_iPortfolio['SLP']['No'] . "</label>";
            $filterTable .= "</td>";
            $filterTable .= "</tr>";
            $filterTable .= "<tr class='advanceFilter' style='display:none'>";
            $filterTable .= "<td>";
            $filterTable .= $iPort['ComeFromChina'];
            $filterTable .= ": </td>";
            $filterTable .= "<td>";
            $filterTable .= "<input type='radio' id='come_from_china_l' name='come_from_china' value='2' checked><label for='come_from_china_l'>" . $iPort['NotLimit'] . "</label>";
            $filterTable .= "<input type='radio' id='come_from_china_y' name='come_from_china' value='1'><label for='come_from_china_y'>" . $ec_iPortfolio['SLP']['Yes'] . "</label>";
            $filterTable .= "<input type='radio' id='come_from_china_n' name='come_from_china' value='0'><label for='come_from_china_n'>" . $ec_iPortfolio['SLP']['No'] . "</label>";
            $filterTable .= "</td>";
            $filterTable .= "</tr>";
            $filterTable .= "<tr class='advanceFilter' style='display:none'>";
            $filterTable .= "<td>";
            $filterTable .= $iPort['CrossBoundary'];
            $filterTable .= ": </td>";
            $filterTable .= "<td>";
            $filterTable .= "<input type='radio' id='cross_boundary_l' name='cross_boundary' value='2' checked><label for='cross_boundary_l'>" . $iPort['NotLimit'] . "</label>";
            $filterTable .= "<input type='radio' id='cross_boundary_y' name='cross_boundary' value='1'><label for='cross_boundary_y'>" . $ec_iPortfolio['SLP']['Yes'] . "</label>";
            $filterTable .= "<input type='radio' id='cross_boundary_n' name='cross_boundary' value='0'><label for='cross_boundary_n'>" . $ec_iPortfolio['SLP']['No'] . "</label>";
            $filterTable .= "</td>";
            $filterTable .= "</tr>";
            $filterTable .= "<tr class='advanceFilter' style='display:none'>";
            $filterTable .= "<td>";
            $filterTable .= $Lang['SDAS']['SEN'];
            $filterTable .= ": </td>";
            $filterTable .= "<td>";
            $filterTable .= $filterCheckBox;
            $filterTable .= "</td>";
            $filterTable .= "</tr>";
            $filterTable .= "</table>";
            // ##### Filter Table END ######
            
            echo $filterTable;
        }

        function checkZeroDot($value)
        {
            $x = explode('.', $value);
            if (count($x > 0)) {
                if ($x[1] > 0) {
                    return $value; // 100.1
                } else {
                    return $x[0]; // 100.0
                }
            } else {
                return $value;
            }
        }

        function Get_Subject_Selection($SubjectIdArr = '', $firstTitle = '')
        {
            global $Lang, $eclass_db;
            $sql = "
			SELECT
			    assr.SubjectID, assr.SubjectComponentID
			From
			    {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
			WHERE
			    assr.SubjectID>0
			GROUP BY
			    assr.SubjectID, assr.SubjectComponentID
			";
            $temp1 = $this->returnResultSet($sql); // all subject and subject com in record
            
            $sql = "
					SELECT
						ass.RecordID, ass.EN_DES, ass.CH_DES, lc.LearningCategoryID, lc.NameChi, lc.NameEng
					FROM
						ASSESSMENT_SUBJECT ass
					LEFT JOIN
						LEARNING_CATEGORY lc ON lc.LearningCategoryID = ass.LearningCategoryID
					WHERE
						ass.RecordStatus = '1'
					";
            $temp = $this->returnResultSet($sql); // subject name and subject category
            foreach ((array) $temp as $_temp) {
                $name[$_temp['RecordID']]['SubjectName'] = Get_Lang_Selection($_temp['CH_DES'], $_temp['EN_DES']);
                $name[$_temp['RecordID']]['CategoryID'] = $_temp['LearningCategoryID'] ? $_temp['LearningCategoryID'] : '0';
                $CategoryName = Get_Lang_Selection($_temp['NameChi'], $_temp['NameEng']);
                $name[$_temp['RecordID']]['CategoryName'] = $CategoryName ? $CategoryName : "No Category";
                $ExistSubjectID[] = $_temp['RecordID'];
            }
            
            // reshape the arr
            foreach ((array) $temp1 as $_temp1) {
                if (empty($_temp1['SubjectComponentID'])) {
                    $_temp1['SubjectComponentID'] = '0';
                }
                if ($SubjectIdArr == '' || in_array($_temp1['SubjectID'], (array) $SubjectIdArr)) {
                    if ((in_array($_temp1['SubjectID'], (array) $ExistSubjectID) && in_array($_temp1['SubjectComponentID'], (array) $ExistSubjectID)) || ($_temp1['SubjectComponentID'] === '0' && in_array($_temp1['SubjectID'], (array) $ExistSubjectID))) {
                        $value = $_temp1['SubjectID'] . "_" . $_temp1['SubjectComponentID'];
                        $subject[$name[$_temp1['SubjectID']]['CategoryID']]['CategoryName'] = $name[$_temp1['SubjectID']]['CategoryName'];
                        $subject[$name[$_temp1['SubjectID']]['CategoryID']]['Subject'][$value]['value'] = $value;
                        if (empty($_temp1['SubjectComponentID'])) {
                            $subject[$name[$_temp1['SubjectID']]['CategoryID']]['Subject'][$value]['SubjectName'] = $name[$_temp1['SubjectID']]['SubjectName'];
                        } else {
                            $subject[$name[$_temp1['SubjectID']]['CategoryID']]['Subject'][$value]['SubjectName'] = $name[$_temp1['SubjectID']]['SubjectName'] . "-" . $name[$_temp1['SubjectComponentID']]['SubjectName'];
                        }
                    }
                }
            }
            // start ui
            $x = "";
            $x .= "<select id='FromSubjectID' name='FromSubjectID'>";
            if ($firstTitle) {
                $x .= "<option value=''>{$firstTitle}</option>";
            }
            foreach ((array) $subject as $_subject) { // category
                $x .= "<optgroup label='" . $_subject['CategoryName'] . "'>";
                foreach ($_subject['Subject'] as $__subject) {
                    $x .= "<option value='{$__subject['value']}'>";
                    $x .= $__subject['SubjectName'];
                    $x .= "</option>";
                }
                $x .= "</optgroup>";
            }
            $x .= "</select>";
            // end ui
            return $x;
        }

        function GetPromotionConfig()
        {
            $sql = "Select 
						SettingValue
					From 
						GENERAL_SETTING 
					WHERE 
						Module = 'SDAS' 
					AND 
						SettingName = 'Promotion_Config' ";
            $rs = $this->returnVector($sql);
            return $rs[0];
        }

        /**
         * $data => array(
         * array( $average1, $weight1 ),
         * array( $average2, $weight2 ),
         * ...
         * )
         */
        function CalculateWeightedArithmeticMean($data)
        {
            $dataWeight = 0;
            $totalWeight = 0;
            foreach ($data as $d) {
                $dataWeight += ($d[0] * $d[1]);
                $totalWeight += $d[1];
            }
            
            if ($totalWeight == 0) {
                return 0;
            }
            
            $weightedArithmeticMean = $dataWeight / $totalWeight;
            return $weightedArithmeticMean;
        }

        function ComputeSDMean($YearID, $AcademicYearID, $YearTermID, $YearClassID = 0)
        {
            global $intranet_db, $eclass_db, $intranet_root, $sys_custom;
            
            if ($YearClassID == 0) {
                $sql = "SELECT YearClassID FROM {$intranet_db}.YEAR_CLASS WHERE YearID='$YearID' AND AcademicYearID = '$AcademicYearID'";
                $ClassIDArr = $this->returnVector($sql);
                if (sizeof($ClassIDArr) < 1) {
                    // no class in that form / year
                    return false;
                }
            } else {
                $ClassIDArr = array(
                    $YearClassID
                );
            }
            $ClassIDinSQL = implode(",", $ClassIDArr);
            
            // remove previous data
            $sql = "DELETE FROM {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN " . "WHERE AcademicYearID='{$AcademicYearID}' AND YearTermID='{$YearTermID}' AND ClassLevelID='{$YearID}' AND YearClassID='{$YearClassID}' ";
            $this->db_db_query($sql);
            // ### Get PassMarks START ####
            $sql = "SELECT SubjectID, FullMarkInt, PassMarkInt FROM {$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK " . "WHERE AcademicYearID='{$AcademicYearID}' AND YearID='{$YearID}' ";
            $SubectFullMarkArr = $this->returnResultSet($sql);
            for ($i = 0; $i < sizeof($SubectFullMarkArr); $i ++) {
                $SubectFullMarks[$SubectFullMarkArr[$i]["SubjectID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
                $passMark = ($SubectFullMarkArr[$i]["PassMarkInt"]) ? $SubectFullMarkArr[$i]["PassMarkInt"] : ((int) $SubectFullMarkArr[$i]["FullMarkInt"]) / 2;
                $SubectPassMarks[$SubectFullMarkArr[$i]["SubjectID"]] = $passMark;
            }
            // ### Get PassMarks END ####
            // get the total number of students
            $StudentToal = 0;
            
            $lib_year = new Year($YearID);
            if ($YearClassID == 0) {
                $AllClasses = $lib_year->Get_All_Classes("", $AcademicYearID);
                // debug_r($AllClasses);
                for ($i = 0; $i < sizeof($AllClasses); $i ++) {
                    $this->ComputeSDMean($YearID, $AcademicYearID, $YearTermID, $AllClasses[$i]["YearClassID"]);
                    // debug($AllClasses[$i]["YearClassID"]);
                    $lib_year_class = new year_class($AllClasses[$i]["YearClassID"], false, false, true);
                    $StudentTotal += sizeof($lib_year_class->ClassStudentList);
                }
            } else {
                $lib_year_class = new year_class($YearClassID, false, false, true);
                $StudentTotal += sizeof($lib_year_class->ClassStudentList);
            }
            
            // for class
            // 1: subjects
            // 2: overall (Main)
            
            // ASSESSMENT_SUBJECT START
            $sql_yeartermID = ($YearTermID == NULL || $YearTermID == "" || $YearTermID == "NULL") ? " (YearTermID IS NULL OR YearTermID = 0) " : " YearTermID='{$YearTermID}' ";
            
            $IsMain = "0";
            // get the raw data
            $sql = "SELECT Score, Grade, SubjectID, SubjectComponentID, TermAssessment, IsAnnual FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score > -1 AND " . "AcademicYearID='{$AcademicYearID}' AND  {$sql_yeartermID}  " . "AND YearClassID IN ($ClassIDinSQL)  " . "ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual "; // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
            $SubjectMarks = $this->ReturnResultSet($sql);
            foreach ((array) $SubjectMarks as $_SubjectMarks) {
                $SubjectID = $_SubjectMarks['SubjectID'];
                $SubjectComponentID = $_SubjectMarks['SubjectComponentID'] ? $_SubjectMarks['SubjectComponentID'] : 0;
                $TermAssessment = $_SubjectMarks['TermAssessment'] ? $_SubjectMarks['TermAssessment'] : 0;
                if ($_SubjectMarks['Score'] == '-1' && $_SubjectMarks['Grade'] == '*') {
                    // Do nth to filter the Grade =='* and score = -1'
                } else {
                    $SubjectMarksArr[$SubjectID][$SubjectComponentID][$_SubjectMarks['IsAnnual']][$TermAssessment][] = $_SubjectMarks['Score'];
                }
            }
            // $SubjectMarksArr => Structure: SubjectID CMPID IsAnnual TermAssessment
            foreach ((array) $SubjectMarksArr as $SubjectID => $_SubjectMarksArr) {
                foreach ((array) $_SubjectMarksArr as $CMPID => $__SubjectMarksArr) {
                    foreach ((array) $__SubjectMarksArr as $IsAnnual => $___SubjectMarksArr) {
                        foreach ((array) $___SubjectMarksArr as $TermAssessment => $AllScores) {
                            if (! empty($AllScores)) {
                                $Stats['Min'] = min($AllScores);
                                $Stats['Max'] = max($AllScores);
                                $Stats['Num'] = count($AllScores);
                                $Stats['SD'] = (count($AllScores) - 1) ? $this->sd($AllScores) : 0; // SD=0 if and only if number of data = 1
                                $Stats['Mean'] = round(array_sum($AllScores) / sizeof($AllScores), 3);
                                
                                $PassMarkSubjectID = $CMPID ? $CMPID : $SubjectID;
                                $Stats['PassTotal'] = 0;
                                foreach ($AllScores as $_AllScores) {
                                	$passMark = $SubectPassMarks[$PassMarkSubjectID];
                                    if ($sys_custom['SDAS']['SKHTST']['CustFullMarks']) {
                                    	// 2020-08-06 (Philips) - $TermAssessment should be in string formrat when compare
                                    	if ((strpos($lib_year->YearName, '6') !== false && (string)$TermAssessment == 'T1A3') || ((string)$TermAssessment == 'T2A3')) {
                                            $passMark = 50;
                                        }
                                    }
                                    if ($_AllScores >= $passMark) {
                                        $Stats['PassTotal'] ++;
                                    }
                                }
                                $sql = "INSERT INTO
								{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
								(SubjectID, SubjectComponentID, AcademicYearID, YearTermID, TermAssessment, " . "IsAnnual, IsMain, ClassLevelID, YearClassID,  SD, MEAN, HighestMark, LowestMark, PassTotal, AttemptTotal, StudentTotal, InputDate) " . "values
								('{$SubjectID}', '{$CMPID}', '{$AcademicYearID}', '{$YearTermID}', '{$TermAssessment}', " . "'{$IsAnnual}', '{$IsMain}', '{$YearID}', '{$YearClassID}', '{$Stats['SD']}', '{$Stats['Mean']}', '" . $Stats['Max'] . "', '" . $Stats['Min'] . "', '" . $Stats["PassTotal"] . "', '" . $Stats['Num'] . "', '{$StudentTotal}', now()) ";
                                $this->db_db_query($sql);
                            }
                        }
                    }
                }
            }
            unset($Stats);
            // ASSESSMENT_SUBJECT END
            
            // ISMAIN START
            $IsMain = "1";
            $sql = "SELECT Score, TermAssessment, IsAnnual FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " . "AcademicYearID='{$AcademicYearID}' AND  {$sql_yeartermID}  " . "AND YearClassID IN ($ClassIDinSQL)  " . "ORDER BY TermAssessment, IsAnnual "; // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
            $MainMarks = $this->ReturnResultSet($sql);
            
            $SubjectID = 0;
            $SubjectComponentID = 0;
            foreach ((array) $MainMarks as $_MainMarks) {
                $IsAnnual = $_MainMarks['IsAnnual'];
                $TermAssessment = ($_MainMarks["TermAssessment"]) ? $_MainMarks["TermAssessment"] : "0";
                $MainMarksArr[$IsAnnual][$TermAssessment][] = $_MainMarks['Score'];
            }
            foreach ((array) $MainMarksArr as $IsAnnual => $_MainMarksArr) {
                foreach ((array) $_MainMarksArr as $TermAssessment => $AllScores) {
                    if (! empty($AllScores)) {
                        $Stats['SD'] = (count($AllScores) - 1) ? round($this->sd($AllScores), 3) : 0; // SD=0 if and only if number of data = 1;
                        $Stats['Mean'] = round(array_sum($AllScores) / sizeof($AllScores), 3);
                        $sql = "INSERT INTO {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN (SubjectID, SubjectComponentID, AcademicYearID,  YearTermID, TermAssessment, " . "IsAnnual, IsMain, ClassLevelID, YearClassID, SD, MEAN, InputDate) " . "values ('0', '0', '{$AcademicYearID}', '{$YearTermID}', '{$TermAssessment}', " . "'{$IsAnnual}', '{$IsMain}', '{$YearID}', '{$YearClassID}', '{$Stats['SD']}', '{$Stats['Mean']}', now()) ";
                        $this->db_db_query($sql);
                    }
                }
            }
            // ISMAIN END
        }

        function sd($array)
        {
            // Function to calculate standard deviation (uses sd_square)
            // square root of sum of squares devided by N-1
            return sqrt(array_sum(array_map("sd_square_SDAS", $array, array_fill(0, count($array), (array_sum($array) / count($array))))) / (count($array) - 1));
        }

        function getMonitoringGroup($GroupID, $AcademicYearID)
        {
            global $intranet_db, $eclass_db, $intranet_root;
            if ($GroupID) {
                $GroupIDCond = ' AND mg.GroupID = "' . $GroupID . '"';
            }
            if ($AcademicYearID) {
                $AcademicYearIDCond = ' AND mg.AcademicYearID = "' . $AcademicYearID . '" ';
            }
            $sql = 'Select
						mg.GroupID,
						mg.AcademicYearID,
						mg.GroupNameEN,
						mg.GroupNameCH,
						mg.DateModified,
						ay.YearNameEN,
						ay.YearNameB5
					FROM
						MONITORING_GROUP mg
					INNER JOIN 
						ACADEMIC_YEAR ay 
							ON ay.AcademicYearID = mg.AcademicYearID
					WHERE
						mg.Isdeleted = "0"
						' . $GroupIDCond . $AcademicYearIDCond . '
					Order by 
						ay.Sequence
					';
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        function getMonitoringGroupMember($RecordType, $GroupID)
        {
            global $intranet_db, $eclass_db, $intranet_root;
            
            if ($RecordType) {
                $RecordTypeCond = ' AND mgm.RecordType = "' . $RecordType . '" ';
            }
            if ($GroupID) {
                $GroupIDCond = ' AND mgm.GroupID = "' . $GroupID . '" ';
            }
            
            $sql = 'SELECT
				Distinct
				iu.UserID, iu.EnglishName, iu.ChineseName, mgm.GroupID, mgm.RecordType, mgm.UserID, mgm.DateModified, ycycu.ClassTitleEN, ycycu.ClassTitleB5, ycycu.ClassNumber, ycycu.YearID
			FROM
				MONITORING_GROUP_MEMBER mgm
			INNER JOIN
				MONITORING_GROUP mg
					ON mgm.GroupID = mg.GroupID
			INNER JOIN
				INTRANET_USER iu
					ON	iu.UserID = mgm.UserID
			LEFT JOIN
				(SELECT
					ycu.UserID, yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber, yc.AcademicYearID, yc.YearID
				FROM
					YEAR_CLASS_USER ycu
					LEFT JOIN
						YEAR_CLASS yc
							ON yc.YearClassID = ycu.YearClassID) ycycu
				ON ycycu.UserID = mgm.UserID  and ycycu.AcademicYearID = mg.AcademicYearID
					
			Where
				mgm.IsDeleted = "0"
				' . $RecordTypeCond . '
				' . $GroupIDCond . '
			ORDER BY
				ycycu.ClassTitleEN desc, ycycu.ClassNumber asc
			';
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }
        
        function getMonthlyReportPIC(){
            $sql = "select SettingValue from GENERAL_SETTING where SettingName = 'MonthlyReportPIC'";
            $TeacherList =  $this->returnVector($sql);
            if($TeacherList[0] == '' ){
                $TeacherCond = '0';
            } else {
                $TeacherCond = str_replace(",", "','", $TeacherList[0]);
            }
            
            $sql = "SELECT UserID, ChineseName, EnglishName
                    FROM INTRANET_USER
                    WHERE 1
                    AND UserID IN ('$TeacherCond')
                    ";
            $result = $this->returnArray($sql);
            $resultArr = array();
            foreach($result as $rs){
                $resultArr[$rs['UserID']] = $rs;
            }
            return $resultArr;
        }
        
        function isMonthlyReportPIC($id){
            $PICArr = $this->getMonthlyReportPIC();
            $SectionArr = $this->getMonthlyReportSectionRight($id);
            return !(empty($PICArr[$id])) || !(empty($SectionArr));
        }
        
        function getMonthlyReportSectionPIC($section=''){
//             $namefield = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');
            $namefield= "  If (iu.ChineseName IS NULL Or iu.ChineseName = '', iu.EnglishName, ".Get_Lang_Selection('iu.ChineseName','iu.EnglishName').") ";
                        
            $table = "CEES_SCHOOL_MONTHLY_REPORT_USER_ACCESS_RIGHT cees";
            $join1 = "INTRANET_USER iu
                        ON iu.UserID = cees.UserID";
            $cols = "cees.RecordID, cees.SectionName, cees.UserID, $namefield AS Name";
            if($section!=''){
                $conds = "WHERE cees.SectionName = '$section'";
            }
            
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                        INNER JOIN $join1
                    $conds";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getMonthlyReportSectionRight($userID){
            $table = "CEES_SCHOOL_MONTHLY_REPORT_USER_ACCESS_RIGHT";
            $cols = "SectionName";
            $conds = "UserID = '$userID'";
            
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                    WHERE
                        $conds
                    ";
            $result = $this->returnVector($sql);
            return $result;
        }
        
        // Start of Access Group
        
        function getAccessGroup($groupid){
            $cols = "GroupID, Name_ch, Name_en, Description";
            $table = "ASSESSMENT_ACCESS_GROUP";
            $cond = "isDeleted = '0'";
            $cond .= " AND GroupID = '$groupid'";
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                    WHERE
                        $cond
                    ";
//             debug_pr($sql);die();
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getAccessGroupRights($groupid){
            $cols = "aggr.AcademicStatisticName, aggr.RightType";
            $table = "ASSESSMENT_ACCESS_GROUP AS aag";
            $joinTable1 = "ASSESSMENT_ACCESS_GROUP_RIGHT AS aggr";
            $joinCond1 = "aag.GroupID = aggr.GroupID";
            $cond = "aag.isDeleted = '0'";
            $cond .= " AND aag.GroupID = '$groupid'";
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                    INNER JOIN
                        $joinTable1
                        ON $joinCond1
                    WHERE
                        $cond
                    ";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getAccessGroupMemberRights($uid = ''){
            if($uid == ''){
                global $_SESSION;
                $uid = $_SESSION['UserID'];
            }
            $cols = "aagr.AcademicStatisticName, aagr.RightType";
            $table = "ASSESSMENT_ACCESS_GROUP_MEMBER AS aagm";
            $joinTable1 = "ASSESSMENT_ACCESS_GROUP_RIGHT AS aagr";
            $joinCond1 = "aagm.GroupID = aagr.GroupID";
            $joinTable2 = "ASSESSMENT_ACCESS_GROUP AS aag";
            $joinCond2 = "aagm.GroupID = aag.GroupID";
            $cond = "aag.isDeleted = '0'";
            $cond .= " AND aagm.UserID = '$uid'";
            $cond .= " AND aagr.RightType = '1'";
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                        INNER JOIN $joinTable1
                        ON $joinCond1
                        INNER JOIN $joinTable2
                        ON $joinCond2
                    WHERE
                        $cond
                    ";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function isAccessGroupMember($uid = ''){
            if($uid == ''){
                global $_SESSION;
                $uid = $_SESSION['UserID'];
            }
            $rights = $this->getAccessGroupMemberRights($uid);
            return sizeof($rights) > 0;
        }
        
        function getAccessGroupMember($groupid){
            $namefield = 'iu.' . Get_Lang_Selection('ChineseName', 'EnglishName');
            $backupfield = 'iu.' . Get_Lang_Selection('EnglishName', 'ChineseName');
            $cols = "aag.GroupID, IF($namefield = '', $backupfield, $namefield) as Name, iu.UserID";
            $table = "ASSESSMENT_ACCESS_GROUP AS aag";
            $joinTable1 = "ASSESSMENT_ACCESS_GROUP_MEMBER AS aggm";
            $joinCond1 = "aag.GroupID = aggm.GroupID";
            $joinTable2 = "INTRANET_USER iu";
            $joinCond2 = "aggm.userID = iu.userID";
            $cond = "aag.isDeleted = '0'";
            $cond .= " AND aag.GroupID = '$groupid'";
            $sql = "SELECT 
                        $cols
                    FROM
                        $table
                    INNER JOIN
                        $joinTable1
                        ON $joinCond1
                    INNER JOIN
                        $joinTable2
                        ON $joinCond2
                    WHERE
                        $cond";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function insertAccessGroup($arr){
            global $_SESSION;
            $table = "ASSESSMENT_ACCESS_GROUP";
            $cols = array('Name_ch', 'Name_en', 'Description', 'InputDate', 'InputBy');
            $values = array($arr['Name_ch'], $arr['Name_en'], $arr['Description'], 'NOW()', $_SESSION['UserID']);
            
            $sql = "INSERT INTO $table(" . implode(",", $cols).") VALUES ('" . implode("','", $values)."')";
            $sql = str_replace("'NOW()'", "NOW()", $sql);
            $this->db_db_query($sql);
            return $this->db_insert_id();
        }
        
        function insertAccessGroupRight($arr, $groupID){
            $table = "ASSESSMENT_ACCESS_GROUP_RIGHT";
            $cols = "GroupID, AcademicStatisticName, RightType";
            $values = array();
            foreach($arr as $name => $right){
                $values[] = "('$groupID', '$name', '$right')";
            }
            $sql = "INSERT INTO $table($cols) VALUES " . implode(',', $values);
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function updateAccessGroup($arr){
            global $_SESSION;
            $table = "ASSESSMENT_ACCESS_GROUP";
            $cols = array('Name_ch', 'Name_en', 'Description', 'ModifiedDate', 'ModifiedBy');
            $values = array($arr['Name_ch'], $arr['Name_en'], $arr['Description'], 'NOW()', $_SESSION['UserID']);
            $update = "";
            for($i=0;$i<sizeof($cols);$i++){
                $update .= "$cols[$i] = '$values[$i]',";
            }
            $update = substr($update, 0, -1);
            $sql = "UPDATE $table
                    SET
                        $update
                    WHERE
                        GroupID = '$arr[GroupID]'";
            $sql = str_replace("'NOW()'", "NOW()", $sql);
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function updateAccessGroupRight($arr, $groupID){
            $this->deleteAccessGroupRight($groupID);
            $result = $this->insertAccessGroupRight($arr, $groupID);
            return $result;
        }
        
        function updateAccessGroupMember($arr, $groupID){
            $curMemberArr = $this->getAccessGroupMember($groupID);
            $curMemberArr = Get_Array_By_Key($curMemberArr, 'UserID');
            
            $insertArr = array_diff($arr, $curMemberArr);
            $deleteArr = array_diff($curMemberArr, $arr);
            
            $result = array(
                'insert' => $this->insertAccessGroupMember($insertArr, $groupID),
                'delete' => $this->deleteAccessGroupMember($deleteArr, $groupID)
            );
            return $result;
        }
        
        
        function insertAccessGroupMember($arr, $groupID){
            if(sizeof($arr) == 0)
                return true;
            $table = "ASSESSMENT_ACCESS_GROUP_MEMBER";
            $cols = "GroupID, UserID";
            $values = "";
            foreach($arr as $member){
                $values .= "('$groupID', '$member'),";
            }
            $values = substr($values, 0, -1);
            $sql = "INSERT INTO $table($cols) VALUES$values";
            $result = $this->db_db_query($sql);
//             debug_pr($sql);die();
            return $result;
        }
        
        function deleteAccessGroupMember($arr, $groupID){
            if(sizeof($arr) == 0)
                return true;
            $table = "ASSESSMENT_ACCESS_GROUP_MEMBER";
            $conds = "GroupID = '$groupID' ";
            $conds .= "AND UserID IN ('" . implode("','", $arr) . "')";
            $sql = "DELETE FROM $table WHERE $conds";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function deleteAccessGroup($groupIDArr){
            $table = "ASSESSMENT_ACCESS_GROUP";
            $sql = "UPDATE $table SET isDeleted = '1' Where GroupID IN ('".implode("','", $groupIDArr)."')";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function deleteAccessGroupRight($groupID){
            $table = "ASSESSMENT_ACCESS_GROUP_RIGHT";
            $sql = "DELETE FROM $table WHERE GroupID = '$groupID'";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        // End of Access Group
    }
 // End Class
    function sd_square_SDAS($x, $mean)
    {
        // Function to calculate square of value - mean
        return pow($x - $mean, 2);
    }
}

?>