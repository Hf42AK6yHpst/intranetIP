<?php 

class libPopupInterface extends interface_html{
	
	function __construct($ParTemplateFile = "default.html"){
		parent::__construct($ParTemplateFile);
	}
	
	
	
	function MODULE_MENU(){
		global $MODULE_OBJ, $contentLeftPos, $UserID, $PATH_WRT_ROOT, $i_general_expand_all, $i_general_collapse_all;
		global $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;
		global $customLeftMenu, $CurrentPageArr;
		global $LAYOUT_SKIN;

		if (sizeof($MODULE_OBJ) == 1) {
			return;
		}

		// customize left menu
		if ((isset($customLeftMenu) && $customLeftMenu != "")||$MODULE_OBJ['CustomLogo']!= "") {
			echo $customLeftMenu;
			return;
		}

		// determine module id by logo image path
		$ModuleID = md5(str_replace("../", "", $MODULE_OBJ["logo"]))."_".$UserID;

		// get the key array
		if (sizeof($MODULE_OBJ['menu']) > 0)
		foreach ($MODULE_OBJ['menu'] as $key => $MenuSection)
		{
			if (!is_array($KeyFlag) || !$KeyFlag[$key])
			{
				$KeyArrayJS .= "MenuKeyArray[MenuKeyArray.length] = \"{$key}\";\n";
				if(!empty($MenuSection['Child']))
					$ParentArrayJS .= "ParentMenuKeyArray[ParentMenuKeyArray.length] = \"{$key}\";\n";
				$KeyFlag[$key] = true;
			}
		}

		# generate
		$rx = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$rx .= "<tr>\n";
		$rx .= "<td width=\"148\" valign=\"top\" class=\"module_icon\">\n";
		$rx .= "<table width=\"148\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$rx .= "<tr>\n";

		$rx .= "<td height=\"100\" align=\"center\" valign=\"bottom\"><a href=\"{$MODULE_OBJ['root_path']}\"><img src=\"{$MODULE_OBJ['logo']}\" border=\"0\"></a></td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td bgcolor=\"#FFFFFF\">\n";
		$rx .= "<div id=\"hideall\"><a href=\"javascript:void(0)\" onClick=\"jDISPLAY_ALL_MENU_ITEM(ParentMenuKeyArray)\" ></a></div>";


		if (sizeof($MODULE_OBJ['menu']) > 0)
		foreach ($MODULE_OBJ['menu'] as $key => $MenuSection)
		{
			$TempArr = $MenuSection['Child'];

			$HasChild = (!empty($TempArr)) ? true : false;

			$Selected = $MenuSection[2];

			if ($Selected)
			{
				$tbClass = "section_btn_selected";
				$menuClass = "menuon";
			} else
			{
				$tbClass = "section_btn";
				$menuClass = "menu";
			}

			####### section ##############################################################

			$rx .= "<table class=\"submenuTable\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
			$rx .= "<tr>\n";
			$rx .= "<td>";

			for ($i = 0; $i < 2; $i ++)
			{

				if ($i != 0)
				{
					// expand
					if ($Selected)
					{
						$normalImg = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off_catholic.gif";
					} else
					{
						$normalImg = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off2_catholic.gif";
					}
					$mouseOverImg = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off_catholic.gif";
					$DisplayStyle = " style=\"display: block\" ";
					$TableIDExt = "Expand";
				} else
				{
					// close
					if ($Selected)
					{
						$normalImg = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off_catholic.gif";
					} else
					{
						$normalImg = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off_catholic.gif";
					}
					$mouseOverImg = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off_catholic.gif";
					$DisplayStyle = " style=\"display: none\" ";
					$TableIDExt = "Close";
				}

				if ($MenuSection[3] != "") {
					$frontImg = $MenuSection[3];
					$paddingstyle = "style=\"margin-left: 5px; margin-right: 5px\"";
				} else {
					$frontImg = "$PATH_WRT_ROOT/images/".$LAYOUT_SKIN."/10x10.gif";
					$paddingstyle = "";
				}

				/*
				if (!is_array($KeyFlag) || !$KeyFlag[$key])
				{
					$KeyArrayJS .= "MenuKeyArray[MenuKeyArray.length] = \"{$key}\";\n";
					$KeyFlag[$key] = true;
				}
				*/

				$rx .= "<table width=\"100%\" id=\"Table{$key}{$TableIDExt}\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"$tbClass\" onMouseOver=\"this.className='menubgon'\" onMouseOut=\"this.className='$tbClass'\" $DisplayStyle>\n";
				$rx .= "<tr>\n";
				$rx .= "<td width=\"10\" height=\"30\"> <img src=\"$frontImg\" align=\"absmiddle\" $paddingstyle></td>\n";


				$rx .= ($HasChild) ? "<td width=\"123\"><a href=\"javascript:void(0)\" class=\"$menuClass\"" : "<td><a href=\"".$MenuSection[1]."\" class=\"\"";
				if ($HasChild)
				{
					### onClick action ###
					$rx .= " onClick=\"jDISPLAY_MENU_ITEM('{$key}')\"";
					### onClick action ###
				}

				$rx .= ">\n";
				$rx .= $MenuSection[0];
				$rx .= "</a></td>\n";


				$rx .= "<td width=\"15\">";

				if($HasChild)
				{
					### OnClick Menu Section ####
					$rx .= "<a href=\"javascript:void(0)\" class=\"\">";
					$rx .= "<img src=\"{$normalImg}\" name=\"lb{$key}\" width=\"15\" height=\"30\" border=\"0\" id=\"lb{$key}{$TableIDExt}\" onMouseOver=\"MM_swapImage('lb{$key}{$TableIDExt}','','{$mouseOverImg}',1)\" onMouseOut=\"MM_swapImgRestore()\"";
					$rx .= " onClick=\"jDISPLAY_MENU_ITEM('{$key}')\" />";
					$rx .= "</a>";
					### OnClick Menu Section ####
				}

				$rx .= "</td>\n";
				$rx .= "</tr></table>";
			}


			$rx .= "</td>\n";
			$rx .= "</tr>\n";

			if($HasChild)
			{
				$rx .= "<tr id=\"ItemList{$key}\" $DisplayStyle>\n";
				$rx .= "<td>\n";

				foreach ($TempArr as $MenuItem) {
					//hdebug_r($MenuItem);
					if ($MenuItem[2]) {
						// Selected Style
						$icon = $MODULE_OBJ['selectedMenuIcon'];//"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/icon_sub_on_catholic.gif";
						$height = "15";
						$tdclass = "menuon";
					} else {
						// Normal Style
						$icon = "{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif";
						$height = "20";
						$tdclass = "submenu";
					}

					if ($MenuItem[3] != "") $icon = $MenuItem[3];

					####### Expand content ##############################################################
					$rx .= "<table>\n";
					$rx .= "<tr>\n";
					$rx .= "<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";

					
					if($MenuItem[1]=="lite")
					{
						# for eEnrolment Lite version 2011-09-07 YatWoon (maybe can use in other module lite version in future)
						$item_link = "<font color='#cccccc'><i>".$MenuItem[0]."</i></font>";
					}
					else
					{
						// extraAttr
						$extraAttr = '';
						if($MenuItem[4] != ''){
							$extraAttr = $MenuItem[4];
						}
						$item_link = "<a href=\"".$MenuItem[1]."\" class=\"$tdclass\" $extraAttr>".$MenuItem[0]."</a>";
						
					}
					#### Item Line ####
					$rx .= "<tr>\n";
					$rx .= "<td>";
					$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
					$rx .= "<tr>\n";
					$rx .= "<td width=\"6\"><img src=\"{$PATH_WRT_ROOT}images/".$LAYOUT_SKIN."/10x10.gif\" width=\"6\" height=\"20\"></td>\n";
					$rx .= "<td width=\"10\" align=\"center\" valign=\"top\">\n";
					$rx .= "<img src=\"$icon\" ></td>\n";
					$rx .= "<td width=\"124\">". $item_link ."</td>\n";
					$rx .= "</tr>\n";


					$rx .= "</table>";
					$rx .= "</td>\n";
					$rx .= "</tr>\n";
					
					/*
						if have child, expand here
					*/
					if (sizeof($MenuItem["child"]) > 0)
					{
						$rx .= "<tr><td >";
						foreach ($MenuItem["child"] as $ThirdChildItem)
						{

							if ($ThirdChildItem[3] != "") $childicon = $ThirdChildItem[3];


							if ($ThirdChildItem[2]) {
								// Selected Style
								$childtdclass = "menuon";
							} else {
								// Normal Style
								$childtdclass = "submenu";
							}

							### Third Level Child ###
							$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
							$rx .= "<tr>\n";
							$rx .= "<td width=\"6\"><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"30\" height=\"15\"></td>\n";
							$rx .= "<td width=\"10\" align=\"center\" valign=\"top\">\n";
							$rx .= "<img src=\"$childicon\" ></td>\n";
							$rx .= "<td width=\"200\"><a href=\"".$ThirdChildItem[1]."\" class=\"$childtdclass\">\n";
							$rx .= $ThirdChildItem[0];
							$rx .= "</a></td>\n";
							$rx .= "</tr>\n";
							$rx .= "</table>";
							### Third Level Child ###

						}
						$rx .= "</td></tr>";
					}

					#### Item Line ####
					if ($MenuItem[4]) {
						//$SubMenuArr['Settings']['Calculation']['name']
						/*
						for ($SubMenuCount = 0; $SubMenuCount < sizeof(); $SubMenuCount++) {
							$rx .= "<tr>\n";
							$rx .= "<td>";
							$rx .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
							$rx .= "<tr>\n";
							$rx .= "<td width=\"6\"><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"6\" height=\"20\"></td>\n";
							$rx .= "<td width=\"10\" align=\"center\" valign=\"top\">\n";
							$rx .= "<img src=\"$icon\" width=\"10\" height=\"$height\"></td>\n";
							$rx .= "<td width=\"124\"><a href=\"".$MenuItem[1]."\" class=\"$tdclass\">\n";
							$rx .= $MenuItem[0];
							$rx .= "</a></td>\n";
							$rx .= "</tr>\n";
							$rx .= "</table>";
							$rx .= "</td>\n";
							$rx .= "</tr>\n";
						}
						*/
					}


					$rx .= "</table></td>\n";
					$rx .= "</tr>\n";
					$rx .= "</table>\n";
					####### Expand content ##############################################################
				}

				$rx .= "</td>\n";
				$rx .= "</tr>\n";
			}

			$rx .= "</table>\n";
			####### section ##############################################################
		}

        if ($MODULE_OBJ['module'] == 'SDAS') {
            $icon_hide_arrow_on = "icon_hide_arrow_on_catholic.gif";
            $icon_hide_arrow_off = "icon_hide_arrow_off_catholic.gif";
            $icon_hide_bottom = "hide_bottom_catholic.png";
            $icon_bottom = "bottom_catholic.png";
        }
        else {
            $icon_hide_arrow_on = 'icon_hide_arrow_on.gif';
            $icon_hide_arrow_off = "icon_hide_arrow_off.gif";
            $icon_hide_bottom = "hide_bottom.gif";
            $icon_bottom = "bottom.gif";
        }
		$rx .= "</td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td bgcolor=\"#FFFFFF\"><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"30\"></td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table>\n";
		$rx .= "</td>\n";
		$rx .= "<td width=\"14\" valign=\"top\" background=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/hide_bg_catholic.png\">\n";
		$rx .= "<table width=\"14\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
		$rx .= "<tr>\n";
		$rx .= "<td width=\"14\"><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/10x10.gif\" width=\"14\" height=\"47\"></td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td width=\"14\" height=\"104\" align=\"center\" id=\"showhideLinkText\">";

		$rx .= "<a href=\"javascript:void(0)\" class=\"hidemenu\" onMouseOver=\"MM_swapImage('arrow_hide','','{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/{$icon_hide_arrow_on}',1)\" onMouseOut=\"MM_swapImgRestore()\" ";
		$rx .= " onClick=\"(isShow) ? HideLeftMenu(0, -{$contentLeftPos}):ShowLeftMenu(-{$contentLeftPos} ,0); \"";
		$rx .= "><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/{$icon_hide_arrow_off}\" name=\"arrow_hide\" width=\"14\" height=\"11\" border=\"0\" id=\"arrow_hide\"><br>\n";
		$rx .= "&nbsp;<br />\n";
		$rx .= "&nbsp;<br />\n";
		$rx .= "</a>";

		$rx .= "</td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table></td>\n";
		$rx .= "</tr>\n";
		$rx .= "<tr>\n";
		$rx .= "<td background=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/{$icon_bottom}\"><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/{$icon_bottom}\" width=\"148\" height=\"13\"></td>\n";
		$rx .= "<td><img src=\"{$PATH_WRT_ROOT}/images/".$LAYOUT_SKIN."/leftmenu/{$icon_hide_bottom}\" width=\"14\" height=\"13\"></td>\n";
		$rx .= "</tr>\n";
		$rx .= "</table>\n";

		if ($KeyArrayJS!="")
		{
			$rx .= "<script language='javascript'>\n var MenuKeyArray = Array();\n" . $KeyArrayJS . "var MenuID = \"{$ModuleID}\";\n jINITIAL_MENU_OPEN();\n</script>\n";
		}
		$rx .= "<script language='javascript'>\n var ParentMenuKeyArray = Array();\n " . $ParentArrayJS . "\n</script>\n";

		echo $rx;
		
	} // End MODULE_MENU()
	
} // End Class