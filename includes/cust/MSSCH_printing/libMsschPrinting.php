<?php
// updating : Pun
/**
 * Change Log:
 * 2020-08-26 (Pun) [193753]
 *  - Modified getYearMonthSelection(), added support for import last year (hidden function)
 * 2016-12-06 (Pun)
 *        - Modified resetUserOldUsage(), added backup table
 *      - Modified getYearMonthSelection(), fixed infinite loop if current month is December
 * 2016-02-01 (Pun) [92157]
 *        - Modified getFormUsage(), fixed export did not remove HTML tag for quit student
 * 2016-01-18 (Pun)
 *        - Added getYearMonthSelection(), for Year-Month Selection
 *        - Added getFormUsage(), for ePayment export
 * 2015-12-18 (Pun)
 *        - Modified getAllClassReportData(), fixed cannot read Past Usage data
 */
if (!defined("LIBMsschPRINTING_DEFINED"))                     // Preprocessor directive
{
    define("LIBMsschPRINTING_DEFINED", true);

    class libMsschPrinting extends libdb
    {
        public function GET_MODULE_OBJ_ARR()
        {
            global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPage, $Lang, $cfg_msschPrint;

            if (!is_array($CurrentPage)) {
                switch ($CurrentPage) {
                    case "ManagementPrint":
                        $pageManagement = 1;
                        $pageManagementPrint = 1;
                        break;

                    case "ManagementDeposit":
                        $pageManagement = 1;
                        $pageManagementDeposit = 1;
                        break;

                    case "ReportPrintGroupSummary":
                        $pageReport = 1;
                        $pageReportPrintGroupSummary = 1;
                        break;

                    case "ReportPrintClassSummary":
                        $pageReport = 1;
                        $pageReportPrintClassSummary = 1;
                        break;

                    case "ReportPrintUserSummary":
                        $pageReport = 1;
                        $pageReportPrintUserSummary = 1;
                        break;

                    case "SettingGroup":
                        $pageSetting = 1;
                        $pageSettingGroup = 1;
                        break;

                    case "SettingPrinting":
                        $pageSetting = 1;
                        $pageSettingPrinting = 1;
                        break;

                }
            }
            $MenuArr = array();

            ############
            # Management
            ############
            if (
                libMsschPrintingAccessRight::checkPrintImportAccessRight() ||
                libMsschPrintingAccessRight::checkDepositAccessRight()
            ) {
                $MenuArr["Management"] = array($Lang['MsschPrint']['menu']['management'], "", $pageManagement);
                if (libMsschPrintingAccessRight::checkPrintImportAccessRight()) {
                    $MenuArr["Management"]["Child"]["Print"] = array($Lang['MsschPrint']['menu']['print'], "{$cfg_MsschPrint['path']}?t=management.print&clearCoo=1", $pageManagementPrint);
                }
                if (libMsschPrintingAccessRight::checkDepositAccessRight()) {
                    $MenuArr["Management"]["Child"]["Deposit"] = array($Lang['MsschPrint']['menu']['deposit'], "{$cfg_MsschPrint['path']}?t=management.deposit&clearCoo=1", $pageManagementDeposit);
                }
            }

            ############
            # Reports
            ############
            if (libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
                $MenuArr["Report"] = array($Lang['MsschPrint']['menu']['report'], "", $pageReport);
                $MenuArr["Report"]["Child"]['printingGroupSummary'] = array($Lang['MsschPrint']['menu']['printingGroupSummary'], "{$cfg_MsschPrint['path']}?t=report.printingGroupSummary", $pageReportPrintGroupSummary);
                $MenuArr["Report"]["Child"]['printingClassSummary'] = array($Lang['MsschPrint']['menu']['printingClassSummary'], "{$cfg_MsschPrint['path']}?t=report.printingClassSummary", $pageReportPrintClassSummary);
                $MenuArr["Report"]["Child"]['printingUserSummary'] = array($Lang['MsschPrint']['menu']['printingUserSummary'], "{$cfg_MsschPrint['path']}?t=report.printingUserSummary", $pageReportPrintUserSummary);
            }

            ############
            # Settings
            ############
            if (libMsschPrintingAccessRight::checkGroupSettingAccessRight()) {
                $MenuArr["Setting"] = array($Lang['MsschPrint']['menu']['setting'], "", $pageSetting);
                $MenuArr["Setting"]["Child"]['Group'] = array($Lang['MsschPrint']['menu']['group'], "{$cfg_MsschPrint['path']}?t=setting.group&clearCoo=1", $pageSettingGroup);
                $MenuArr["Setting"]["Child"]['Printing'] = array($Lang['MsschPrint']['menu']['printSetting'], "{$cfg_MsschPrint['path']}?t=setting.printing&clearCoo=1", $pageSettingPrinting);
            }

            #####################
            # module information
            #####################
            $MODULE_OBJ['title'] = $Lang['MsschPrint']['module'];
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_ePayment.gif";
            $MODULE_OBJ['menu'] = $MenuArr;

            return $MODULE_OBJ;
        }

        public function getYearMonthArr($academicYearId = '')
        {
            $academicYearId = ($academicYearId) ? $academicYearId : Get_Current_Academic_Year_ID();
            $startYear = substr(getStartDateOfAcademicYear($academicYearId), 0, 4);
            $nextYear = $startYear+1;
            $yearMonthArr = array(
                "{$startYear}-08",
                "{$startYear}-09",
                "{$startYear}-10",
                "{$startYear}-11",
                "{$startYear}-12",
                "{$nextYear}-01",
                "{$nextYear}-02",
                "{$nextYear}-03",
                "{$nextYear}-04",
                "{$nextYear}-05",
                "{$nextYear}-06",
                "{$nextYear}-07",
            );

            return $yearMonthArr;
        }

        /**
         * Get the Year-Month Selection box HTML
         * @param string $htmlName The id and name for selection box
         * @param string $startMonth Format: YYYY-MM
         * @param string $endMonth Format: YYYY-MM
         * @param string $defaultSelected Default selected <option>, Format: YYYY-MM
         */
        public function getYearMonthSelection($htmlName = 'selectMonth', $academicYearId = '')
        {
            $yearMonthArr = $this->getYearMonthArr($academicYearId);

            $selectMonthHTML = getSelectByValue($yearMonthArr, 'id="' . $htmlName . '" name="' . $htmlName . '"', 0, 0, 1);

            return $selectMonthHTML;
        }

        /**
         * Get all Group MFP code
         * Return: array of MFP_CODE
         */
        public function getCostByAcademicYearId($AcademicYearID = '')
        {
            $AcademicYearID = ($AcademicYearID) ? $AcademicYearID : Get_Current_Academic_Year_ID();
            $sql = "SELECT 
                PC.* 
            FROM 
                PRINTING_COST PC
            WHERE
                PC.AcademicYearID = '{$AcademicYearID}'
            ORDER BY
                PC.YearMonth,
                PC.Type
            ";
            $rs = $this->returnResultSet($sql);

            return $rs;
        }

        /**
         * Get all Group MFP code
         * Return: array of MFP_CODE
         */
        public function getAllGroupMfpCode($AcademicYearID = '')
        {
            if ($AcademicYearID) {
                $yearFilter = "AND AcademicYearID = '{$AcademicYearID}'";
            }
            $sql = "SELECT 
				MFP_CODE
			FROM 
				PRINTING_GROUP
			WHERE 
				1=1
				{$yearFilter}
			";
            $rs = $this->returnVector($sql);

            $mfpArr = array_values($rs);
            return $mfpArr;
        }

        public function getGroupIdByMfpCode($MfpCode, $AcademicYearID = '')
        {
            if (empty($AcademicYearID)) {
                $AcademicYearID = Get_Current_Academic_Year_ID();
            }
            $MfpCode = trim(htmlentities($MfpCode, ENT_QUOTES, 'UTF-8'));

            $sql = "SELECT 
				GROUP_ID
			FROM 
				PRINTING_GROUP
			WHERE 
				MFP_CODE = _utf8 '{$MfpCode}' collate utf8_bin
			AND
				AcademicYearID = '{$AcademicYearID}'";
            $rs = $this->returnVector($sql);
            return $rs[0];
        }

        /**
         * Get all group name by $AcademicYearIdList
         * return array[ $groupID ] = 'groupName'
         */
        public function getAllGroupNameArr($AcademicYearIdList = array())
        {
            $AcademicYearIdList = (array)IntegerSafe($AcademicYearIdList);

            if ($AcademicYearIdList) {
                $AcademicYearIdArr = implode("','", $AcademicYearIdList);
                $yearSql = "AND AcademicYearID IN ('$AcademicYearIdArr')";
            }
            $sql = "SELECT
				GROUP_ID,
				NAME
			FROM 
				PRINTING_GROUP
			WHERE
				1=1
				{$yearSql}";
            $rs = $this->returnResultSet($sql);
            $groupNameArr = BuildMultiKeyAssoc($rs, array('GROUP_ID'), $IncludedDBField = array('NAME'), $SingleValue = 1);
            return $groupNameArr;
        }

        public function getAllGroupMfpCodeArr($AcademicYearIdList = array())
        {
            $AcademicYearIdList = (array)IntegerSafe($AcademicYearIdList);

            if ($AcademicYearIdList) {
                $AcademicYearIdArr = implode("','", $AcademicYearIdList);
                $yearSql = "AND AcademicYearID IN ('$AcademicYearIdArr')";
            }
            $sql = "SELECT
				GROUP_ID,
				MFP_CODE
			FROM 
				PRINTING_GROUP
			WHERE
				1=1
				{$yearSql}";
            $rs = $this->returnResultSet($sql);
            $groupNameArr = BuildMultiKeyAssoc($rs, array('GROUP_ID'), $IncludedDBField = array('MFP_CODE'), $SingleValue = 1);
            return $groupNameArr;
        }

        public function getAllGroupUserCountArr($AcademicYearIdList = array())
        {
            $AcademicYearIdList = (array)IntegerSafe($AcademicYearIdList);

            if ($AcademicYearIdList) {
                $AcademicYearIdArr = implode("','", $AcademicYearIdList);
                $yearSql = "AND PG.AcademicYearID IN ('$AcademicYearIdArr')";
            }
            $sql = "SELECT
				PG.MFP_CODE,
				COUNT(*) AS UserCount
			FROM 
				PRINTING_GROUP_MEMBER PGM
			INNER JOIN
				PRINTING_GROUP PG
			ON
				PG.GROUP_ID = PGM.GROUP_ID
			WHERE
				1=1
				{$yearSql}
			GROUP BY
				PG.GROUP_ID";
            $rs = $this->returnResultSet($sql);
            $groupNameArr = BuildMultiKeyAssoc($rs, array('MFP_CODE'), $IncludedDBField = array('UserCount'), $SingleValue = 1);
            return $groupNameArr;
        }

        public function getAllGroupUserQuitTimeArr($AcademicYearID, $MfpCode)
        {
            global $objDB, $cfg_msschPrint;

            $mfpCodeArr = array();
            if (is_array($MfpCode)) {
                foreach ($MfpCode as $code) {
                    $mfpCodeArr[] = trim(htmlentities($code, ENT_QUOTES, 'UTF-8'));
                }
            } else {
                $mfpCodeArr[] = trim(htmlentities($MfpCode, ENT_QUOTES, 'UTF-8'));
            }
            $mfpCodeSql = implode("','", $mfpCodeArr);

            $sql = "SELECT 
				PGM.UserID,
                PG.MFP_CODE,
                IUPS.QuitTime
			FROM 
				PRINTING_GROUP_MEMBER PGM
            INNER JOIN
                PRINTING_GROUP PG
            USING
                (GROUP_ID)
            INNER JOIN
                INTRANET_USER_PERSONAL_SETTINGS IUPS 
            USING
                (UserID)
			WHERE
				PGM.MEMBER_TYPE = '{$cfg_msschPrint['group']['memberType']['Member']}'
            AND
				PG.MFP_CODE IN ('{$mfpCodeSql}')
			AND 
			    PG.AcademicYearID = '{$AcademicYearID}'
            ";

            $rs = $objDB->returnResultSet($sql);
            return BuildMultiKeyAssoc($rs, array('MFP_CODE', 'UserID'), array('QuitTime'), true);
        }

        public function getPrintingUsage($userID, $AcademicYearIdList = array())
        {
            $userID = IntegerSafe($userID);
            $AcademicYearIdList = (array)IntegerSafe($AcademicYearIdList);

            if ($AcademicYearIdList) {
                $AcademicYearIdArr = implode("','", $AcademicYearIdList);
                $yearSql = "AND AcademicYearID IN ('$AcademicYearIdArr')";
            }
            $sql = "SELECT 
				SUM(AMOUNT)
			FROM 
				PRINTING_MEMBER_USAGE
			WHERE
				UserID = '{$userID}'
				{$yearSql}";
            $rs = $this->returnVector($sql);
            return ($rs[0] == null) ? 0 : $rs[0];
        }

        /**
         * This function is to get the usage by formID with specific peroid
         * @param string $startDate Format: YYYY-MM
         * @param string $endDate Format: YYYY-MM
         * @param string $formIdArr The formID, it can be an integer or array
         */
        public function getFormUsage($startDate, $endDate, $formIdArr)
        {
            $formIdArr = (array)IntegerSafe($formIdArr);
            $startDate = $startDate . '-01';
            $endDate = $endDate . '-01';

            ######## Get all Class/Student START ########
            $allStudentIdArr = array();
            $allStudentArr = array();
            $allClassNameArr = array();
            foreach ($formIdArr as $formId) {
                $year = new Year($formId);
                $studentArr = $year->Get_All_Student();
                foreach ($studentArr as $student) {
                    $student['StudentName'] = strip_tags($student['StudentName']);
                    $allStudentArr[$formId][] = $student;
                    $allStudentIdArr[] = $student['UserID'];
                }

                /* Not use class name/number
                $classArr = $year->Get_All_Classes();
                foreach($classArr as $class){
                    $allClassNameArr[ $class['YearClassID'] ] = $class['ClassTitleEN'];
                }
                */
            }
            $allStudentIdList = implode("','", $allStudentIdArr);
            ######## Get all Class/Student END ########

            ######## Get all Student Login START ########
            $sql = "SELECT
				UserID,
				UserLogin
			FROM 
				INTRANET_USER 
			WHERE
				UserID IN ('{$allStudentIdList}')";
            $rs = $this->returnResultSet($sql);
            $allUserLogin = BuildMultiKeyAssoc($rs, array('UserID'), array('UserLogin'), $SingleValue = 1);
            ######## Get all Student Login END ########

            ######## Get all printing record START ########
            $sql = "SELECT
				PMU.UserID,
				SUM(PMU.AMOUNT) AS TOTAL_AMOUNT
			FROM
				PRINTING_MEMBER_USAGE PMU
			INNER JOIN
				PRINTING_GROUP_USAGE PGU
			ON
				PMU.RECORD_ID = PGU.RECORD_ID
			WHERE
				PMU.UserID IN ('{$allStudentIdList}')
			AND
				'{$startDate}' <= PGU.RECORD_DATE
			AND
				PGU.RECORD_DATE <= '{$endDate}'
			GROUP BY 
				PMU.UserID";
            $rs = $this->returnResultSet($sql);
            $recordArr = BuildMultiKeyAssoc($rs, array('UserID'), array('TOTAL_AMOUNT'), $SingleValue = 1);
            $recordStudentIdArr = array_keys($recordArr);
            ######## Get all printing record END ########

            ######## Pack data START ########
            $result = array();
            foreach ($allStudentArr as $studentArr) {
                foreach ($studentArr as $student) {
                    $userID = $student['UserID'];
                    if (in_array($userID, $recordStudentIdArr)) {
                        $result[] = array(
                            'UserLogin' => $allUserLogin[$userID],
                            //'ClassName' => $allClassNameArr[ $student['YearClassID'] ],
                            //'ClassNumber' => $student['ClassNumber'],
                            'StudentName' => $student['StudentName'],
                            'TOTAL_AMOUNT' => $recordArr[$userID],
                        );
                    }
                }
            }
            ######## Pack data END ########

            return $result;
        }

        public function getAllGroupUsage($AcademicYearIdList = array(), $RECORD_DATE = '')
        {
            $AcademicYearIdList = (array)IntegerSafe($AcademicYearIdList);
            $RECORD_DATE = trim(htmlentities($RECORD_DATE, ENT_QUOTES, 'UTF-8'));

            if ($AcademicYearIdList) {
                $AcademicYearIdArr = implode("','", $AcademicYearIdList);
                $yearSql = "AND PGU.AcademicYearID IN ('$AcademicYearIdArr')";
            }
            $sql = "SELECT 
				PG.MFP_CODE,
				SUM(PGU.TOTAL_AMOUNT) AS AMOUNT
			FROM 
				PRINTING_GROUP_USAGE PGU
			INNER JOIN
				PRINTING_GROUP PG
			ON
				PGU.GROUP_ID = PG.GROUP_ID
			WHERE
				PGU.RECORD_DATE < '{$RECORD_DATE}'
				{$yearSql}
			GROUP BY
				PG.GROUP_ID";
            $rs = $this->returnResultSet($sql);

            $amount = BuildMultiKeyAssoc($rs, array('MFP_CODE'), array('AMOUNT'), $SingleValue = 1);
            return $amount;
        }

        public function getPrintingDeposit($userID, $AcademicYearIdList = array())
        {
            $userID = IntegerSafe($userID);
            $AcademicYearIdList = (array)IntegerSafe($AcademicYearIdList);

            if ($AcademicYearIdList) {
                $AcademicYearIdArr = implode("','", $AcademicYearIdList);
                $yearSql = "AND AcademicYearID IN ('$AcademicYearIdArr')";
            }
            $sql = "SELECT 
				SUM(AMOUNT)
			FROM 
				PRINTING_USER_DEPOSIT
			WHERE
				UserID = '{$userID}'
				{$yearSql}";
            $rs = $this->returnVector($sql);
            return ($rs[0] == null) ? 0 : $rs[0];
        }

        public function getBatchDepositDataByBatchID($batchID)
        {
            $batchID = IntegerSafe($batchID);
            $sql = "SELECT 
				*
			FROM 
				PRINTING_BATCH_DEPOSIT
			WHERE 
				BATCH_ID = '{$batchID}'";
            $rs = $this->returnResultSet($sql);
            return $rs[0];
        }

        ######################### Report START #########################
        public function getGroupReportData($formID)
        {
            $formID = IntegerSafe($formID);
            $currentAcademicYear = Get_Current_Academic_Year_ID();

            $Year = new Year($formID);
            $allStudent = $Year->Get_All_Student();

            $allStudentIdArr = array();
            foreach ($allStudent as $studnet) {
                $allStudentIdArr[] = $studnet['UserID'];
            }
            $allStudentIdList = implode("','", $allStudentIdArr);

            //// Get All Deposit START ////
            $sql = "SELECT 
				UserID,
				SUM(AMOUNT) as TOTAL_AMOUNT
			FROM 
				PRINTING_USER_DEPOSIT
			WHERE
				UserID IN ('{$allStudentIdList}')
			GROUP BY
				UserID";
            $rs = $this->returnResultSet($sql);
            $totalDeposit = BuildMultiKeyAssoc($rs, array('UserID'), $IncludedDBField = array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get All Deposit END ////

            //// Get Past Usage START ////
            $sql = "SELECT 
				UserID,
				SUM(AMOUNT) as TOTAL_AMOUNT
			FROM 
				PRINTING_MEMBER_USAGE
			WHERE
				AcademicYearID <> '{$currentAcademicYear}'
			AND
				UserID IN ('{$allStudentIdList}')
			GROUP BY
				UserID";
            $rs = $this->returnResultSet($sql);
            $pastTotalUsage = BuildMultiKeyAssoc($rs, array('UserID'), $IncludedDBField = array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get Past Usage END ////

            //// Calculate Past Balance START ////
            $balance = $totalDeposit;
            foreach ($pastTotalUsage as $userID => $usage) {
                $balance[$userID] -= $usage;
            }
            //// Calculate Past Balance END ////


            //// Calculate Special Deposit START ////
            /* This part is for delete the old record in report */
            $specialDeposit = $this->getUserOldUsage($allStudentIdArr);

            foreach ($balance as $userID => $b) {
                $balance[$userID] += $specialDeposit[$userID];
                $balance[$userID] = round($balance[$userID], 2);
                if ($balance[$userID] == -0) {
                    $balance[$userID] = 0;
                }
            }
            //// Calculate Special Deposit END ////


            //// Get current Usage by group START ////
            $sql = "SELECT 
				PGU.GROUP_ID,
				PMU.UserID,
				SUM(PMU.AMOUNT) AS TOTAL_AMOUNT
			FROM 
				PRINTING_MEMBER_USAGE PMU
			INNER JOIN
				PRINTING_GROUP_USAGE PGU
			ON
				PMU.RECORD_ID = PGU.RECORD_ID
			WHERE
				PMU.AcademicYearID = '{$currentAcademicYear}'
			AND
				UserID IN ('{$allStudentIdList}')
			GROUP BY
				PGU.GROUP_ID,
				PMU.UserID";
            $rs = $this->returnResultSet($sql);
            $currentYearUsage = BuildMultiKeyAssoc($rs, array('GROUP_ID', 'UserID'), $IncludedDBField = array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get current Usage by group END ////

            return array(
                'TotalBalance' => $balance,
                'CurrentYearUsage' => $currentYearUsage,
                'StudentList' => $allStudent
            );
        }

        public function getGroupReportData_v2($formID)
        {
            $formID = IntegerSafe($formID);
            $currentAcademicYear = Get_Current_Academic_Year_ID();

            $Year = new Year($formID);
            $allStudent = $Year->Get_All_Student();
            $allStudentMapping = BuildMultiKeyAssoc($allStudent, 'UserID');
            $allStudentIdArr = Get_Array_By_Key($allStudent, 'UserID');
            $allStudentIdList = implode("','", $allStudentIdArr);

            #### Get student usage data START ####
            $sql = "SELECT 
                PMU.UserID,
                PMU.RECORD_DATE,
                PMU.AMOUNT,
                PMU.GROUP_ID,
                PG.NAME AS GROUP_NAME
            FROM 
                PRINTING_MEMBER_USAGE_V2 PMU
            INNER JOIN
                PRINTING_GROUP PG
            USING
                (GROUP_ID)
            WHERE
                PMU.UserID IN ('{$allStudentIdList}')
            AND 
                PG.AcademicYearID = '{$currentAcademicYear}'
            ORDER BY
                PG.NAME,
                FIELD(PMU.UserID, '{$allStudentIdList}')
            ";
            $rs = $this->returnResultSet($sql);
            #### Get student usage data END ####

            #### Pack student name START ####
            foreach ($rs as $index => $r) {
                $rs[$index]['ClassName'] = $allStudentMapping[$r['UserID']]['ClassName'];
                $rs[$index]['ClassNumber'] = $allStudentMapping[$r['UserID']]['ClassNumber'];
                $rs[$index]['StudentName'] = $allStudentMapping[$r['UserID']]['StudentName'];
            }
            #### Pack student name END ####

            return $rs;
        }

        public function getAllClassReportData_v2($YearClassIdArr)
        {
            $YearClassIdArr = (array)IntegerSafe($YearClassIdArr);
            $YearClassIdList = implode("','", $YearClassIdArr);
            $currentAcademicYear = Get_Current_Academic_Year_ID();

            #### Get student usage data START ####
            $sql = "SELECT 
                PMU.UserID,
                PMU.RECORD_DATE,
                PMU.AMOUNT,
                YCU.YearClassID
            FROM 
                PRINTING_MEMBER_USAGE_V2 PMU
            INNER JOIN
                YEAR_CLASS_USER YCU
            USING
                (UserID)
            INNER JOIN
                YEAR_CLASS YC
            USING
                (YearClassID)
            WHERE
                PMU.AcademicYearID = '{$currentAcademicYear}'
            AND 
                YCU.YearClassID IN ('{$YearClassIdList}')
            ORDER BY
                YC.Sequence
            ";
            $rs = $this->returnResultSet($sql);
            #### Get student usage data END ####

            return $rs;
        }

        public function getAllClassReportData($YearClassIdArr, $firstRecordDate = '')
        {
            $YearClassIdArr = (array)IntegerSafe($YearClassIdArr);
            $YearClassIdList = implode("','", $YearClassIdArr);
            $currentAcademicYear = Get_Current_Academic_Year_ID();

            //// Get All Deposit START ////
            $sql = "SELECT 
				YCU.YearClassID, 
				SUM(PUD.AMOUNT) AS TOTAL_AMOUNT
			FROM 
				PRINTING_USER_DEPOSIT PUD
			INNER JOIN 
				YEAR_CLASS_USER YCU
			ON
				PUD.UserID = YCU.UserID
			AND
				YCU.YearClassID IN ('{$YearClassIdList}')
			GROUP BY
				YCU.YearClassID";
            $rs = $this->returnResultSet($sql);
            $totalDeposit = BuildMultiKeyAssoc($rs, array('YearClassID'), array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get All Deposit END ////

            //// Get Past Usage START ////
            $sql = "SELECT 
				YCU.YearClassID,
				SUM(PMU.AMOUNT) as TOTAL_AMOUNT
			FROM 
				PRINTING_MEMBER_USAGE PMU
			INNER JOIN 
				YEAR_CLASS_USER YCU
			ON
				PMU.UserID = YCU.UserID
			AND
				YCU.YearClassID IN ('{$YearClassIdList}')
			WHERE
				PMU.AcademicYearID <> '{$currentAcademicYear}'
			GROUP BY
				YCU.YearClassID";
            $rs = $this->returnResultSet($sql);
            $pastTotalUsage = BuildMultiKeyAssoc($rs, array('YearClassID'), $IncludedDBField = array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get Past Usage END ////

            //// Calculate Past Balance START ////
            $balance = $totalDeposit;
            foreach ($pastTotalUsage as $yearClassID => $usage) {
                $balance[$yearClassID] -= $usage;
            }
            //// Calculate Past Balance END ////


            //// Calculate Special Deposit START ////
            /* This part is for delete the old record in report */
            $specialDeposit = $this->getClassOldUsage($YearClassIdArr);

            foreach ($balance as $yearClassID => $b) {
                $balance[$yearClassID] += $specialDeposit[$yearClassID];
                $balance[$yearClassID] = round($balance[$yearClassID], 2);
                if ($balance[$yearClassID] == -0) {
                    $balance[$yearClassID] = 0;
                }
            }
            //// Calculate Special Deposit END ////


            //// Get Current Year Usage START ////
            $sql = "SELECT
				YCU.YearClassID,
				PGU.RECORD_DATE,
				SUM(PMU.AMOUNT) AS TOTAL_AMOUNT
			FROM 
				PRINTING_MEMBER_USAGE PMU
			INNER JOIN
				PRINTING_GROUP_USAGE PGU
			ON
				PMU.RECORD_ID = PGU.RECORD_ID
			INNER JOIN
				YEAR_CLASS_USER YCU
			ON
				PMU.UserID = YCU.UserID
			AND
				YCU.YearClassID IN ('{$YearClassIdList}')
			WHERE 
				PMU.AcademicYearID = '{$currentAcademicYear}'
			GROUP BY
				PGU.RECORD_DATE, YCU.YearClassID";
            $currentYearUsage = $this->returnResultSet($sql);
            //// Get Current Year Usage END ////

            return array(
                'OldBalance' => $balance,
                'CurrentYearUsage' => $currentYearUsage
            );
        }

        public function getSingleClassReportData_v2($UserIdArr)
        {
            $UserIdArr = (array)IntegerSafe($UserIdArr);
            $UserIdList = implode("','", $UserIdArr);
            $currentAcademicYear = Get_Current_Academic_Year_ID();

            #### Get student usage data START ####
            $sql = "SELECT
                UserID,
                RECORD_DATE,
                AMOUNT
            FROM
                PRINTING_MEMBER_USAGE_V2
            WHERE
                AcademicYearID = '{$currentAcademicYear}'
            AND   
                UserID IN ('{$UserIdList}')
            ";
            $rs = $this->returnResultSet($sql);
            #### Get student usage data END ####

            return $rs;
        }

        public function getSingleClassReportData($UserIdArr, $firstRecordDate = '')
        {
            $UserIdArr = (array)IntegerSafe($UserIdArr);
            $UserIdList = implode("','", $UserIdArr);
            $currentAcademicYear = Get_Current_Academic_Year_ID();


            //// Get All Deposit START ////
            $sql = "SELECT 
				PUD.UserID,
				SUM(PUD.AMOUNT) AS TOTAL_AMOUNT
			FROM 
				PRINTING_USER_DEPOSIT PUD
			WHERE
				PUD.UserID IN ('{$UserIdList}')
			GROUP BY
				PUD.UserID";
            $rs = $this->returnResultSet($sql);
            $totalDeposit = BuildMultiKeyAssoc($rs, array('UserID'), array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get All Deposit END ////

            //// Get Past Usage START ////
            $sql = "SELECT 
				PMU.UserID,
				SUM(PMU.AMOUNT) AS TOTAL_AMOUNT
			FROM 
				PRINTING_MEMBER_USAGE PMU
			WHERE
				PMU.AcademicYearID <> '{$currentAcademicYear}'
			AND
				PMU.UserID IN ('{$UserIdList}')
			GROUP BY
				PMU.UserID";
            $rs = $this->returnResultSet($sql);
            $pastTotalUsage = BuildMultiKeyAssoc($rs, array('UserID'), $IncludedDBField = array('TOTAL_AMOUNT'), $SingleValue = 1);
            //// Get Past Usage END ////

            //// Calculate Past Balance START ////
            $balance = $totalDeposit;
            foreach ($pastTotalUsage as $userID => $usage) {
                $balance[$userID] -= $usage;
            }
            //// Calculate Past Balance END ////


            //// Calculate Special Deposit START ////
            /* This part is for delete the old record in report */
            $specialDeposit = $this->getUserOldUsage($UserIdArr);

            foreach ($balance as $userID => $b) {
                $balance[$userID] += $specialDeposit[$userID];
                $balance[$userID] = round($balance[$userID], 2);
                if ($balance[$userID] == -0) {
                    $balance[$userID] = 0;
                }
            }
            //// Calculate Special Deposit END ////


            //// Get Current Year Usage START ////
            $sql = "SELECT
				PMU.UserID,
				PGU.RECORD_DATE,
				SUM(PMU.AMOUNT) AS TOTAL_AMOUNT
			FROM 
				PRINTING_MEMBER_USAGE PMU
			INNER JOIN
				PRINTING_GROUP_USAGE PGU
			ON
				PMU.RECORD_ID = PGU.RECORD_ID
			WHERE 
				PMU.AcademicYearID = '{$currentAcademicYear}'
			AND
				PMU.UserID IN ('{$UserIdList}')
			GROUP BY
				PGU.RECORD_DATE, PMU.UserID";
            $currentYearUsage = $this->returnResultSet($sql);
            //// Get Current Year Usage END ////

            return array(
                'OldBalance' => $balance,
                'CurrentYearUsage' => $currentYearUsage
            );
        }

        public function getSingleUserReportData_v2($userId){
            $userId = IntegerSafe($userId);

            #### Get student usage data START ####
            $sql = "SELECT 
                PMU.UserID,
                PMU.RECORD_DATE,
                PMU.AMOUNT,
                PMU.GROUP_ID,
                PG.NAME AS GROUP_NAME,
                PMU.AcademicYearID
            FROM 
                PRINTING_MEMBER_USAGE_V2 PMU
            INNER JOIN
                PRINTING_GROUP PG
            USING
                (GROUP_ID)
            WHERE
                PMU.UserID IN ('{$userId}')
            ORDER BY
                PG.NAME
            ";
            $rs = $this->returnResultSet($sql);
            #### Get student usage data END ####

            return $rs;
        }
        ######################### Report END #########################

        ######################### Special Deposit START #########################
        /* These function is for clearing the old deposit record while generate report. */

        public function resetUserOldUsage($UserIdArr, $onlyDeleteDebt = false)
        {
            $UserIdArr = (array)IntegerSafe($UserIdArr);
            $UserIdList = implode("','", $UserIdArr);

            $currentAcademicYear = Get_Current_Academic_Year_ID();

            ######## Get old usage START ########
            $sql = "SELECT 
            	PMU.UserID,
            	SUM(PMU.AMOUNT) AS TOTAL_AMOUNT
            FROM 
            	PRINTING_MEMBER_USAGE PMU
            WHERE
            	PMU.AcademicYearID <> '{$currentAcademicYear}'
            GROUP BY
            	PMU.UserID";
            $rs = $this->returnResultSet($sql);
            ######## Get old usage END ########

            ######## Backup special deposit START ########
            $date = date('Y_m_d_H_i_s');
            $sql = "CREATE TABLE PRINTING_SPECIAL_DEPOSIT_{$date} SELECT * FROM PRINTING_SPECIAL_DEPOSIT";
            $this->db_db_query($sql);
            ######## Backup special deposit END ########

            ######## Remove old special deposit START ########
            $sql = "DELETE FROM PRINTING_SPECIAL_DEPOSIT";
            $this->db_db_query($sql);
            ######## Remove old special deposit END ########

            ######## Insert into special deposit START ########
            #### Calculate adding amount START ####
            $addingArr = array();
            foreach ($rs as $r) {
                $addingArr[$r['UserID']] = $r['TOTAL_AMOUNT'] * -1;
            }
            #### Calculate adding amount END ####

            #### Prepare insert SQL START ####
            $insertSQL = '';
            foreach ($addingArr as $id => $amount) {
                $amount = -$amount;
                $insertSQL .= "('{$currentAcademicYear}', '{$id}', '{$amount}', NOW(), '{$_SESSION['UserID']}'),";
            }
            $insertSQL = trim($insertSQL, ',');
            #### Prepare insert SQL END ####

            #### Save to DB START ####
            $sql = "INSERT INTO 
				PRINTING_SPECIAL_DEPOSIT
			(
				AcademicYearID,
				UserID,
				AMOUNT,
				DATE_INPUT,
				INPUT_BY
			) VALUES 
				{$insertSQL}
			";
            $result = $this->db_db_query($sql);
            #### Save to DB END ####
            ######## Insert into special deposit END ########

            return $result;
        }

        public function getUserOldUsage($UserIdArr)
        {
            $UserIdArr = (array)IntegerSafe($UserIdArr);
            $UserIdList = implode("','", $UserIdArr);

            #### Get data START ####
            $sql = "SELECT 
				UserID,
				SUM(AMOUNT) AS TOTAL_AMOUNT
			FROM
				PRINTING_SPECIAL_DEPOSIT
			WHERE
				UserID IN ('{$UserIdList}')
			GROUP BY
				UserID";
            $rs = $this->returnResultSet($sql);
            $rs = BuildMultiKeyAssoc($rs, array('UserID'), array('TOTAL_AMOUNT'), $SingleValue = 1);
            #### Get data END ####

            return $rs;
        }

        public function getClassOldUsage($YearClassIdArr)
        {
            $YearClassIdArr = (array)IntegerSafe($YearClassIdArr);
            $YearClassIdList = implode("','", $YearClassIdArr);

            #### Get data START ####
            $sql = "SELECT 
				YCU.YearClassID,
				SUM(PSD.AMOUNT) AS TOTAL_AMOUNT
			FROM
				PRINTING_SPECIAL_DEPOSIT PSD
			INNER JOIN 
				YEAR_CLASS_USER YCU
			ON
				PSD.UserID = YCU.UserID
			AND
				YCU.YearClassID IN ('{$YearClassIdList}')
			GROUP BY
				YCU.YearClassID";

            $rs = $this->returnResultSet($sql);
            $rs = BuildMultiKeyAssoc($rs, array('YearClassID'), array('TOTAL_AMOUNT'), $SingleValue = 1);
            #### Get data END ####

            return $rs;
        }
        ######################### Special Deposit END #########################


    } // End Class
}
