<?php
// updating : Pun
if (!defined("LIB_PRINTING_GROUP_USAGE_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_GROUP_USAGE_DEFINED", true);
	class PRINTING_GROUP_USAGE{
		
		public static function insertUsage($RECORD_DATE, $MfpCode, $TOTAL_AMOUNT, $memberCount){
			global $objDB, $cfg_msschPrint;
			
			$MfpCode = trim( htmlentities( $MfpCode , ENT_QUOTES, 'UTF-8'));
			$TOTAL_AMOUNT = trim( htmlentities( $TOTAL_AMOUNT , ENT_QUOTES, 'UTF-8'));
			$RECORD_DATE = trim( htmlentities( $RECORD_DATE , ENT_QUOTES, 'UTF-8'));
			$AcademicYearID = Get_Current_Academic_Year_ID();
			
			//////// Get GROUP_ID and count the member START ////////
			$sql = "SELECT 
				PG.GROUP_ID AS GROUP_ID
			FROM 
				PRINTING_GROUP PG
			WHERE
				PG.MFP_CODE = _utf8 '{$MfpCode}' collate utf8_bin
			AND
				PG.AcademicYearID = '{$AcademicYearID}'
            ";
			$rs = $objDB->returnResultSet($sql);
			
			if(count($rs) != 1){
				$errMsg = 'Result Error! [count($rs) != 1]<br />'.$sql.'<br /> error['.print_r($rs, true).']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			$GROUP_ID = $rs[0]['GROUP_ID'];
			$MEMBER_COUNT = $memberCount;
			//////// Get GROUP_ID and count the member END ////////
			
			//////// Save data START ////////
			$sql = "INSERT INTO
				PRINTING_GROUP_USAGE
			(
				GROUP_ID,
				MEMBER_COUNT,
				TOTAL_AMOUNT,
				RECORD_DATE,
				DATE_INPUT,
				INPUT_BY,
				DATE_MODIFIED,
				MODIFIED_BY,
				AcademicYearID
			) VALUES (
				'{$GROUP_ID}',
				'{$MEMBER_COUNT}',
				'{$TOTAL_AMOUNT}',
				'{$RECORD_DATE}',
				NOW(),
				'{$_SESSION['UserID']}',
				NOW(),
				'{$_SESSION['UserID']}',
				'{$AcademicYearID}'
			) ON DUPLICATE KEY UPDATE 
				TOTAL_AMOUNT = '{$TOTAL_AMOUNT}',
				DATE_MODIFIED = NOW(),
				MODIFIED_BY = '{$_SESSION['UserID']}'";
			$result = $objDB->db_db_query($sql);

			if($result != 1){
				$errMsg = 'SQL Error! <br />'.$sql.'<br /> error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			return $objDB->db_insert_id();
		}
		
		public static function deleteAllGroupUsage($RECORD_ID){
			global $objDB, $cfg_msschPrint;
			
			$RECORD_ID = IntegerSafe($RECORD_ID);
			$RECORD_ID = array_filter($RECORD_ID); // Remove all '0' elements
			$RECORD_ID = implode("','",(array)$RECORD_ID);
			
			$sql = "DELETE FROM 
				PRINTING_GROUP_USAGE
			WHERE
				RECORD_ID IN ('{$RECORD_ID}')";
			
			$result = $objDB->db_db_query($sql);

			if($result != 1){
				$errMsg = 'SQL Error! <br />'.$sql.'<br /> error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			return $result;
		}
		
		
	} // End class
	
}