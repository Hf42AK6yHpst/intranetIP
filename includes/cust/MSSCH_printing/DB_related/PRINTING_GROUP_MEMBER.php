<?php
// updating : Pun
if (!defined("LIB_PRINTING_GROUP_MEMBER_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_GROUP_MEMBER_DEFINED", true);
	class PRINTING_GROUP_MEMBER{
		
		public static function getUserIDsByGroupIdMemberType($groupID, $memberType){
			global $objDB;
			
			$groupID = IntegerSafe($groupID);
			$memberType = IntegerSafe($memberType);
			$groupIDArr = implode("','",(array)$groupID);
			$memberTypeArr = implode("','",(array)$memberType);
			
			$userArr = array();
			$sql = "SELECT UserID FROM PRINTING_GROUP_MEMBER WHERE GROUP_ID IN ('$groupIDArr') AND MEMBER_TYPE IN ('{$memberTypeArr}')";
			$rs = $objDB->returnResultSet($sql);
			foreach($rs as $r){
				$userArr[] = $r['UserID'];
			}
			return $userArr;
		}
		
		public static function countGroupMember($GROUP_ID, $memberType){
			global $objDB;
			
			$GROUP_ID = IntegerSafe($GROUP_ID);
			$memberType = IntegerSafe($memberType);

			$sql = "SELECT 
				COUNT(*) AS MEMBER_COUNT
			FROM 
				PRINTING_GROUP_MEMBER
			WHERE
				MEMBER_TYPE = '{$memberType}'
			GROUP BY
				GROUP_ID
			HAVING
				GROUP_ID = '{$GROUP_ID}'";
			$rs = $objDB->returnVector($sql);
			if(count($rs) == 0){
				return 0;
			}
			return $rs[0];
		}
		
		public static function insertMember($GROUP_ID, $userIdArr, $memberType){
			global $objDB, $cfg_msschPrint;
			
			$GROUP_ID = IntegerSafe($GROUP_ID);
			$userIdArr = (array)IntegerSafe($userIdArr);
			$userIdArr = array_filter($userIdArr); // Remove all '0' elements
			$memberType = IntegerSafe($memberType);
			$allSuccess = true;
			$errSQL = array();
			
			foreach((array)$userIdArr as $user){
				$sql = "INSERT INTO PRINTING_GROUP_MEMBER (
					GROUP_ID,
					UserID,
					MEMBER_TYPE,
					DATE_INPUT,
					INPUT_BY
				) VALUES (
					'{$GROUP_ID}',
					'{$user}',
					'{$memberType}',
					NOW(),
					'{$_SESSION['UserID']}'
				)";
				$result = $objDB->db_db_query($sql);
				if($result != 1){
					$allSuccess = false;
					$errSQL[] = $sql;
				}
			}
			if(!$allSuccess){
					$errSQL = implode('<br />', $errSQL);
					$errMsg = 'SQL Error! <br /><br />'.$errSQL.'<br /><br /> error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			return $allSuccess;
		}
		public static function deleteAllMember($GROUP_ID, $memberType){
			global $objDB, $cfg_msschPrint;
			
			$GROUP_ID = IntegerSafe($GROUP_ID);
			$memberType = IntegerSafe($memberType);
			$groupIdStr = implode("','", (array)$GROUP_ID);
			
			$sql = "DELETE FROM 
				PRINTING_GROUP_MEMBER 
			WHERE 
				GROUP_ID IN ('{$groupIdStr}') 
			AND 
				MEMBER_TYPE = '{$memberType}'
			";
			$result = $objDB->db_db_query($sql);
			if($result != 1){
				$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			return $result;
		}
		
		
		
	} // End class
	
}