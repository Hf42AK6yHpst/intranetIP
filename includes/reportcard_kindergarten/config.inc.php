<?php
// editing by 

### General configs
$ercKindergartenConfig = array();
$ercKindergartenConfig['moduleCode'] = 'eReportCardKG';
$ercKindergartenConfig['eAdminPath'] = 'home/eAdmin/StudentMgmt/reportcard_kindergarten';
$ercKindergartenConfig['taskSeparator'] = '.';

$ercKindergartenConfig['jsFilePath'] = '/templates/2009a/js/reportcard_kindergarten';
$ercKindergartenConfig['cssFilePath'] = '/templates/2009a/css/reportcard_kindergarten';
$ercKindergartenConfig['imageFilePath'] = '/images/2009a/reportcard_kindergarten';
$ercKindergartenConfig['fontFilePath'] = '/templates/2009a/font/reportcard_kindergarten';

$ercKindergartenConfig['formLevel'] = array('K1', 'K2', 'K3');
//$ercKindergartenConfig['subjectCode'] = array('165','JCHI01');
$ercKindergartenConfig['subjectCode'] = array('S10','S08');
$ercKindergartenConfig['indicatorsCatCode'] = array('EN','PTH');
//$ercKindergartenConfig['subjectIndicatorsCodeMap'] = array('165'=>'EN','JCHI01'=>'PTH');
$ercKindergartenConfig['subjectIndicatorsCodeMap'] = array('S10'=>'EN','S08'=>'PTH');

$ercKindergartenConfig['awardType'] = array('Generate' => 'G', 'Input' => 'I');
?>