<?php
// editing by 
/*
 * 2013-01-08 (Carlos): add field $sizeInBytes
 */

if (!defined("LIBDOCROUTING_FILE_DEFINED")) {
	define("LIBDOCROUTING_FILE_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	
	class libDocRouting_file extends libdbobject {
		var $fileId;
		var $filePath;
		var $fileName;
		var $linkToType;
		var $linkToId;
		var $sizeInBytes;
		var $dateInput;
		var $inputBy;
		var $dateModified;
		var $modifiedBy;
		
		var $fileTmpPath;
		
		function libDocRouting_file($objectId='') {
			parent::__construct('INTRANET_DR_FILE', 'FileID', $this->returnFieldMappingAry(), $objectId);
		}
		
		function setFileId($val) {
			$this->fileId = $val;
		}
		function getFileId() {
			return $this->fileId;
		}
		
		function setFilePath($val) {
			$this->filePath = $val;
		}
		function getFilePath() {
			return $this->filePath;
		}
		
		function setFileName($val) {
			$this->fileName = $val;
		}
		function getFileName() {
			return $this->fileName;
		}
		
		function setLinkToType($val) {
			$this->linkToType = $val;
		}
		function getLinkToType() {
			return $this->linkToType;
		}
		
		function setLinkToId($val) {
			$this->linkToId = $val;
		}
		function getLinkToId() {
			return $this->linkToId;
		}
		
		function getSizeInBytes() {
			return $this->sizeInBytes;
		}
		function setSizeInBytes($val) {
			$this->sizeInBytes = $val;
		}
		
		function setDateInput($val) {
			$this->dateInput = $val;
		}
		function getDateInput() {
			return $this->dateInput;
		}
		
		function setInputBy($val) {
			$this->inputBy = $val;
		}
		function getInputBy() {
			return $this->inputBy;
		}
		
		function setDateModified($val) {
			$this->dateModified = $val;
		}
		function getDateModified() {
			return $this->dateModified;
		}
		
		function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('FileID', 'int', 'setFileId', 'getFileId');
			$fieldMappingAry[] = array('FilePath', 'str', 'setFilePath', 'getFilePath');
			$fieldMappingAry[] = array('FileName', 'str', 'setFileName', 'getFileName');
			$fieldMappingAry[] = array('LinkToType', 'str', 'setLinkToType', 'getLinkToType');
			$fieldMappingAry[] = array('LinkToID', 'int', 'setLinkToId', 'getLinkToId');
			$fieldMappingAry[] = array('SizeInBytes','int','setSizeInBytes', 'getSizeInBytes');
			$fieldMappingAry[] = array('DateInput', 'date', 'setDateInput', 'getDateInput');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('DateModified', 'date', 'setDateModified', 'getDateModified');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		function setFileTmpPath($val) {
			$this->fileTmpPath = $val;
		}
		function getFileTmpPath() {
			return $this->fileTmpPath;
		}
		
		function newRecordBeforeHandling() {
			global $indexVar;
			
			$this->setDateInput('now()');
			$this->setInputBy($indexVar['drUserId']);
			$this->setDateModified('now()');
			$this->setModifiedBy($indexVar['drUserId']);
			return true;
		}
		
		function updateRecordBeforeHandling() {
			global $indexVar;
			
			$this->setDateModified('now()');
			$this->setModifiedBy($indexVar['drUserId']);
			return true;
		}
		
		function returnFolderDivisionNum() {
			return ceil($this->getFileId() / 1500);
		}
	}
}
?>