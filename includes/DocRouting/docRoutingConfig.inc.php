<?php
// editing by 

$docRoutingConfig = array();
$docRoutingConfig['moduleCode'] = 'DocRouting';
$docRoutingConfig['eAdminPath'] = 'home/eAdmin/ResourcesMgmt/DocRouting';
$docRoutingConfig['eServicePath'] = 'home/eService/DocRouting';
if(isKIS()){
	$docRoutingConfig['filePath'] = $PATH_WRT_ROOT.'../'.$intranet_db.'data/DocRouting/attachment';
}else{
	$docRoutingConfig['filePath'] = $PATH_WRT_ROOT.'../intranetdata/DocRouting/attachment';
}
$docRoutingConfig['dbFilePath'] = '/DocRouting/attachment';
$docRoutingConfig['encryptKey'] = 'fj5#$G66yw';
$docRoutingConfig['urlPathParamSeparator'] = '||';
$docRoutingConfig['urlVariableSeparator'] = '/';
$docRoutingConfig['urlVariableKeyValueSeparator'] = '-';

$docRoutingConfig['replySlip']['linkToType']['route'] = 'route';
$docRoutingConfig['maxNumOfTags'] = 10;

$docRoutingConfig['maxLength']['RoutePhysicalLocation'] = 255;
$docRoutingConfig['routeLockTimeout'] = 30; // minute

$docRoutingConfig['archiveToDigitalArchiveTempPath'] = $intranet_root."/file/DocRouting/tmp_archive";

### Route Status
$docRoutingConfig['routeStatus']['notStarted'] = 1;
$docRoutingConfig['routeStatus']['inProgress'] = 2;
$docRoutingConfig['routeStatus']['completed'] = 3;


### Database settings
// $docRoutingConfig[tableName][fieldName][meaning] = dbValue;
$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active'] = 1;
$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'] = 2;
$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'] = 3;
$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'] = 4;
$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted'] = 5;

$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'] = 1;
$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location'] = 2;
$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['NA'] = 3;
$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA'] = 4;

$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['SignConfirm'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'] = 2;
$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'] = 3;
$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'] = 4;
$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'] = 5;
$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'] = 6;

$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'] = 2;
// Same meaning for INTRANET_DR_ROUTE_TARGET.AccessRight 

$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished'] = 2;

$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Active'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Deleted'] = 2;

$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'] = 'document';
$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Route'] = 'route';
$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'] = 'feedback';
$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['DigitalArchive'] = 'DigitalArchive';

$docRoutingConfig['INTRANET_DR_FILE']['RecordType']['Attachment'] = 1; // default
$docRoutingConfig['INTRANET_DR_FILE']['RecordType']['PowerVoice'] = 2;

$docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['DocumentInstruction'] = 1;
$docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'] = 2;
$docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['AllowAccessIP'] = 3;

$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All'] = 1;
$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self'] = 2;
$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Custom'] = 3;

//$docRoutingConfig['INTRANET_DR_ROUTE_FEEDBACK']['RecordStatus']['NotViewedYet'] = 1;
//$docRoutingConfig['INTRANET_DR_ROUTE_FEEDBACK']['RecordStatus']['InProgress'] = 2;
//$docRoutingConfig['INTRANET_DR_ROUTE_FEEDBACK']['RecordStatus']['Completed'] = 3;
$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'] = 2;
$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'] = 3;

$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View'] = 2;


### Index filtering
$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['StarRemarked'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotStarRemarked'] = 2;

$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['CreatedByYou'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotCreatedByYou'] = 2;

$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'] = 2;

$docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['ExpiredNotFinished'] = 1;

### Page View ###
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings'] = 2;
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'] = 3;
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings'] = 4; // signed all routes but document entry not completed
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings'] = 5;

$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Settings']['PresetInstruction'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Settings']['PresetNote'] = 2;

$docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView'] = 2;
$docRoutingConfig['INTRANET_DR_ROUTE_Page']['NonAdminView'] = 3;

### System properties
$docRoutingConfig['settings']['emailNotification_newRouteReceived']['enable'] = 1;
$docRoutingConfig['settings']['emailNotification_newRouteReceived']['disable'] = 0;
$docRoutingConfig['settings']['emailNotification_newRouteReceived']['defaultVal'] = $docRoutingConfig['settings']['emailNotification_newRouteReceived']['disable'];

$docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['enable'] = 1;
$docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['disable'] = 0;
$docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['defaultVal'] = $docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['disable'];

$docRoutingConfig['settings']['AuthenticationSetting']['enable'] = 1;
$docRoutingConfig['settings']['AuthenticationSetting']['disable'] = 0;
$docRoutingConfig['settings']['AuthenticationSetting']['defaultVal'] = $docRoutingConfig['settings']['AuthenticationSetting']['disable'];

### Online form ###
$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseAnytime'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterSubmission'] = 2;
$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'] = 3;
$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseNever'] = 4;

### Route Type
$docRoutingConfig['INTRANET_DR_ROUTES']['RouteType']['NewRoute'] = 0;
$docRoutingConfig['INTRANET_DR_ROUTES']['RouteType']['AddHocRoute'] = 1;

### Reply Slip Type
$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['List'] = 1;
$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table'] = 2;

?>