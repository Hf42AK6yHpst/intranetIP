<?php
// editing by 
 
/********************
 * Date: 2013-02-04 (Rita)
 * Details: modified getPresetData() add variables - 
 ********************/
if (!defined("LIBDOCROUTING_PRESET_DEFINED")) {
	define("LIBDOCROUTING_PRESET_DEFINED", true);
	
	class libDocRouting_preset extends libdb {
		function libDocRouting_preset() {
			$this->libdb();
		}
		
		
		function getPresetRecords($PresetType, $PresetID='', $targetUser=''){
			global $indexVar, $docRoutingConfig, $Lang;
			
			$inputUserID = $indexVar['drUserId'];
			
			if ($PresetType!=''){		
				$con_presetType =" AND PresetType = '".$PresetType."' "; 	
			}
			
			if($PresetID!=''){
				$con_presetID = " AND PresetID = '".$PresetID."' "; 	
			}
			
			if(!isset($targetUser) || $targetUser ==''){
				$con_accessRight ='';
			}
			if(isset($targetUser) && $targetUser!= $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){
				$con_accessRight = " AND InputBy = '".$inputUserID."' ";
				$con_targetType = "AND TargetType = '".$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self']."' ";	
			 
			}
			elseif(isset($targetUser) && $targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){	
				$con_targetType = "AND TargetType = '".$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All']."' ";	
			}
						
			
			$sql = "Select 
						CONCAT('<a href=\"javascript:js_Go_Update_Preset_Record(', PresetID, ')\" class=\"tablelink\" >', Title, '</a>'),
						/*
						CASE TargetType
								WHEN 1 THEN '".$Lang['DocRouting']['TargetType']['ForAllUsers']."'
								WHEN 2 THEN '".$Lang['DocRouting']['TargetType']['ForMySelf']."'
						END AS TargetTypeDisplay,
						*/
						CONCAT('<input type=\"checkbox\" name=\"PresetID[]\" class=\"PresetID\"  value=\"', PresetID, '\">') as CheckBox,
						Title,
						Contents,
						TargetType
					FROM 
						INTRANET_DR_PRESET
					WHERE 1
					$con_presetType
					$con_presetID
					$con_accessRight
					$con_targetType
					"; 
				
			//debug_pr($sql);
			
			return $sql; 
		}
		
		function getPresetData($PresetIDAry='',$presetType='',$targetUserId='', $orderBy='', $targetType='') {
			global $docRoutingConfig;
			
			if($PresetIDAry!=''){			
				$presetIdCond = " AND PresetID IN ('".implode("','", (array)$PresetIDAry)."') ";
			}
			
			if($presetType!=''){
				$presetTypeCond = " AND PresetType = '".$presetType."' ";				
			}
			
			if($targetUserId!=''){
				$targetUserIdCond =	"AND InputBy = '".$targetUserId."' ";
			}
			
			if($targetType!=''){
				$TargetTypeCond = " AND TargetType = '".$targetType."' ";	
			}
			
			
			
			if($orderBy!=''){
				
				$orderByCond = " Order By " . $orderBy; 
							
			}
			$sql = "Select
							PresetID,
							Title,
							Contents
					From
							INTRANET_DR_PRESET
					Where 	1
							$presetIdCond
							$presetTypeCond
							$targetUserIdCond
							$TargetTypeCond
					$orderByCond

					";
			return $this->returnResultSet($sql);
		
		}
		
		function getUserPresetRecords($presetType, $targetUserId) {
			global $docRoutingConfig;
			
			$sql = "Select
							PresetID,
							Title,
							TargetType
					From
							INTRANET_DR_PRESET
					Where
							PresetType = '".$presetType."'
							And (
									TargetType = '".$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All']."'
									Or (TargetType = '".$docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self']."' And InputBy = '".$targetUserId."')
								)
					Order By
							Title
					";
			return $this->returnResultSet($sql);
		}
		
		
		function addNewPresetRecord($PresetType, $presetTitle, $presetContent,$targetType)
		{
			global $indexVar;
			$presetTitle = $this->Get_Safe_Sql_Query($presetTitle);
			$presetContent = $this->Get_Safe_Sql_Query($presetContent);
			$inputUserID = $indexVar['drUserId'];
			
						
			$sql = "INSERT INTO 
							INTRANET_DR_PRESET (UserID, PresetType, Title, Contents, TargetType, DateInput, DateModified, InputBy, ModifiedBy) 
							VALUES ('$inputUserID.','$PresetType','$presetTitle','$presetContent','$targetType',NOW(),NOW(),'$inputUserID','$inputUserID')";
			
			$result = $this->db_db_query($sql);
			return $result;
		}
		
		
		function updatePresetRecord($PresetID, $PresetType, $presetTitle, $presetContent,$targetType)
		{			
			global $docRoutingConfig, $indexVar;
			$presetTitle = $this->Get_Safe_Sql_Query($presetTitle);
			$presetContent = $this->Get_Safe_Sql_Query($presetContent);
			
			$inputUserID = $indexVar['drUserId'];
			
			$sql = "UPDATE INTRANET_DR_PRESET
					SET	UserID ='".$inputUserID."' , 
						PresetType ='".$PresetType."', 
						Title ='".$presetTitle."', 
						Contents ='".$presetContent."', 
						TargetType ='".$targetType."', 
						DateModified = NOW(), 
						ModifiedBy ='".$inputUserID."'
						
					WHERE PresetID='".$PresetID."'";
			
			
			$result = $this->db_db_query($sql);
		 return $result;
		}
		
		
		function removePresetRecord($PresetID){
					
			$sql = "DELETE FROM INTRANET_DR_PRESET WHERE PresetID='".$PresetID."'";
			return $this->db_db_query($sql);
		
		}
		
		function getAccessRightTagControl($PresetType, $targetUser){
			global $docRoutingConfig, $indexVar,$Lang; 
			
			//$targetUser = $indexVar['paramAry']['targetUser'];
			if($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView']){
				$personalTag = 1;	
				$publicTag =  0;	
			}elseif($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){
				$publicTag =  1;
				$personalTag = 0;	
			}
			
			if($PresetType == $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['DocumentInstruction']){
				$directToLink = 'preset_doc_instruction';
			}else{
				$directToLink = 'preset_routing_note';
			}
			
			$personalRecordsActionLink =  $indexVar['libDocRouting']->getEncryptedParameter('settings/'.$directToLink.'', 'index', array('targetUser' => $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView'], 'clearCoo' => '1'));
			$allRecordsActionLink =  $indexVar['libDocRouting']->getEncryptedParameter('settings/'.$directToLink.'', 'index', array('targetUser' => $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView'], 'clearCoo' => '1'));	
			
			if($personalTag =='' && $publicTag=='' ){
				$personalTag = 1;
				$targetUser = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView'];		
			}	
			
			# Add New Preset Instruction
			$newPresetInstructionLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/'.$directToLink.'', 'new', array('targetUser' => $targetUser));		
			$submitUpdateLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/'.$directToLink.'', 'new_update', array('targetUser' => $targetUser));
			$goBackLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/'.$directToLink.'', 'index', array('targetUser' => $targetUser));	
			
			$TAGS_OBJ[] = array($Lang['DocRouting']['Personal'], "?pe=$personalRecordsActionLink",$personalTag);
			$TAGS_OBJ[] = array($Lang['DocRouting']['Public'],"?pe=$allRecordsActionLink", $publicTag );
				
			return array($targetUser, $newPresetInstructionLink, $TAGS_OBJ, $goBackLink, $submitUpdateLink);
		}
		
		
	}
}
?>