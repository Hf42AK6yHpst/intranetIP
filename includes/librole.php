<?php 
# modify by : 

### Modification Log ###
#
#   Date    :   2019-05-13 Cameron
#               fix potential sql injection problem by enclosing var with apostrophe
#
#	Date	:	2012-12-06 (YatWoon)
#				add returnRoleAdminUserID()
#
#	Date	:	2010-11-19 (Henry Chow)
#				add getRoleInfoByUserID(), retrieve Role info by UserID (for "Student Mgmt")
#
#	Date	:	2010-04-29 (Henry)
#				modified displayRoleSelection(), edit Role selection in "eAdmin > Account Management > Staff Management"
#
#	Date	:	2010-04-29 (Henry)
#				add displayRoleSelection(), display Role selection
#
###### End of Log ######
class librole extends libdb {

	var $Role;
	var $RoleID;
	var $Title;
	var $Description;
	var $RecordType;
	var $RecordStatus;
	var $DateInput;
	var $DateModified;

	function librole($RoleID=""){
		$this->libdb();
		if($RoleID<>""){
			$this->Role = $this->returnRole($RoleID);
			$this->RoleID = $this->Role[0][0];
			$this->Title = $this->Role[0][1];
			$this->Description = $this->Role[0][2];
			$this->RecordType = $this->Role[0][3];
			$this->RecordStatus = $this->Role[0][4];
			$this->DateInput = $this->Role[0][5];
			$this->DateModified = $this->Role[0][6];
		}
	}

	function returnRole($RoleID){
		$sql = "SELECT RoleID, Title, Description, RecordType, RecordStatus, DateInput, DateModified FROM INTRANET_ROLE WHERE RoleID = '$RoleID'";
		return $this->returnArray($sql,7);
	}

	function returnDefaultRole(){
		$sql = "SELECT RoleID FROM INTRANET_ROLE WHERE RecordStatus = 1 AND RecordType = '".$this->RecordType."'";
		$row = $this->returnArray($sql,1);
		return $row[0][0];
	}

	function returnSetDefaultRole($RecordType){
		$sql  = "SELECT RoleID, Title, RecordType, RecordStatus FROM INTRANET_ROLE WHERE RecordType = '$RecordType' ORDER BY Title ASC";
		return $this->returnArray($sql,4);
	}

	function displaySetDefaultRole(){
		global $i_GroupRole;
		for($i=0; $i<sizeof($i_GroupRole); $i++){
			$row = $this->returnSetDefaultRole($i);
			$x .= "<p><b>".$i_GroupRole[$i]."</b><br>\n";
			for($j=0; $j<sizeof($row); $j++){
				$RoleID = $row[$j][0];
				$Title = $row[$j][1];
				$RecordType = $row[$j][2];
				$RecordStatus = $row[$j][3];
				$x .= "<input type=radio name=RoleID".$RecordType." value=$RoleID".(($RecordStatus==1) ? " CHECKED" : "")."> $Title <br>\n";
			}
			$x .= "</p>\n";
		}
		return $x;
	}
	
	function displayRoleSelection($ID=0)
	{
		global $PATH_WRT_ROOT, $i_eNews_AddTo, $i_eNews_DeleteTo;
		
		//if($ID!=0)	$conds = " WHERE UserID=$ID";
		
		//$sql = "SELECT r.RoleID, r.RoleName FROM ROLE r LEFT OUTER JOIN ROLE_MEMBER rm ON (rm.RoleID=r.RoleID) GROUP BY r.RoleID ORDER BY r.RoleName";
		$sql = "SELECT RoleID, RoleName FROM ROLE ORDER BY RoleName";
		$row = $this->returnArray($sql, 2);
		
		$x .= "<table width=100% border=0 cellpadding=5 cellspacing=0>\n";
		$x .= "<tr>\n";
		$x .= "<td class=tableContent width=50%>\n";
		
		$joinedRole = array();
		if($ID!=0) {
			$sql = "SELECT RoleID FROM ROLE_MEMBER WHERE UserID='$ID'";
			$joinedRole = $this->returnVector($sql);
		}
		
		$x .= "<select name=RoleID[] id=RoleID size=10 multiple>\n";
		for($i=0; $i<sizeof($row); $i++)
		{
			$RoleID = $row[$i][0];
			$RoleName = $row[$i][1];
			$IsCheck = (in_array($RoleID, $joinedRole)) ? 1 : 0;
			
			$x .= ($IsCheck) ? "<option value=$RoleID>$RoleName</option>\n" : "";
		}
		$x .= "<option>\n";
		for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
		$x .= "</option>\n";
		$x .= "</select>\n";
		$x .= "</td>\n";
		$x .= "<td class=tableContent align=center>\n";
		
		//if($PATH_WRT_ROOT=="")	$PATH_WRT_ROOT = "../../";
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface 	= new interface_html();
		
		$x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "submit", "checkOptionTransfer(this.form.elements['AvailableRoleID[]'],this.form.elements['RoleID[]']);return false;", "submit11") . "<br /><br />";
		$x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "submit", "checkOptionTransfer(this.form.elements['RoleID[]'],this.form.elements['AvailableRoleID[]']);return false;", "submit12");
		
		$x .= "</td>\n";
		$x .= "<td class=tableContent width=50%>\n";
		$x .= "<select name=AvailableRoleID[] id=AvailableRoleID size=10 multiple>\n";
		for($i=0; $i<sizeof($row); $i++)
		{			
			$RoleID = $row[$i][0];
			$RoleName = $row[$i][1];
			$IsCheck = (in_array($RoleID, $joinedRole)) ? 1 : 0;
			$x .= ($IsCheck) ? "" : "<option value=$RoleID>$RoleName</option>\n";
		}
		$x .= "</select>\n";
		$x .= "</td>\n";
		$x .= "</tr>\n";
		$x .= "</table>\n";
		
		return $x;
	}
	
	function getRoleInfoByUserID($userid) {
		$sql = "SELECT r.RoleName, r.RoleID FROM ROLE r LEFT OUTER JOIN ROLE_MEMBER rm ON (rm.RoleID=r.RoleID) WHERE rm.UserID='$userid'";
		return $this->returnArray($sql, 2);
	}
	
	function returnRoleAdminUserID($Module='')
	{
		$UserIDs = array();
		if($Module)
		{
			$sql = "SELECT DISTINCT(rm.UserID) FROM ROLE_RIGHT rr LEFT OUTER JOIN ROLE_MEMBER rm ON (rm.RoleID=rr.RoleID) WHERE rr.FunctionName='eAdmin-". $Module."' order by rm.UserID";
			$teacherID = $this->returnVector($sql);
			
			foreach($teacherID as $t)
			{
				$UserIDs[] = $t;	
			}
			$UserIDs = array_values(array_unique($UserIDs));
		}
		return $UserIDs;
	}	

}
?>