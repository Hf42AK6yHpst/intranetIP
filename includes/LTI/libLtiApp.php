<?php


class libLtiApp
{

    /**
     * @var string
     */
    public $resourceLinkId;

    /**
     * @var string
     */
    public $contextId;

    /**
     * @var string
     */
    public $launchUrl;

    /**
     * @var string
     */
    public $consumerKey;

    /**
     * @var string
     */
    public $consumerSecret;

    /**
     * @var string
     */
    public $toolConsumerInstanceGuid;

    /**
     * @var string
     */
    public $toolConsumerInstanceName;

    /**
     * @var string
     */
    public $toolConsumerInstanceUrl;

    /**
     * @var string
     */
    public $toolConsumerInfoProductFamilyCode;

    /**
     * @var string
     */
    public $toolConsumerInfoVersion;

    /**
     * @var string
     */
    public $oauthSignatureMethod = 'HMAC-SHA1';
    
    /**
     * @var boolean
     */
    public $debug = false;

    /**
     * libLtiApp constructor.
     *
     * @param string $resourceLinkId
     * @param string $contextId
     * @param string $launchUrl
     * @param string $consumerKey
     * @param string $consumerSecret
     */
    public function __construct($resourceLinkId, $contextId, $launchUrl, $consumerKey, $consumerSecret)
    {
        global $intranet_root, $BroadlearningClientName;

        $this->setResourceLinkId($resourceLinkId);
        $this->setContextId($contextId);
        $this->setLaunchUrl($launchUrl);
        $this->setConsumerKey($consumerKey);
        $this->setConsumerSecret($consumerSecret);

        $intranetUrl = curPageURL(false, false);

        $this->setToolConsumerInstanceGuid(parse_url($intranetUrl, PHP_URL_HOST));
        $this->setToolConsumerInstanceName($BroadlearningClientName);
        $this->setToolConsumerInstanceUrl($intranetUrl);

        if (file_exists($intranet_root . '/ver.php')) {
            $JustWantVersionData = true;
            require_once($intranet_root . '/ver.php');
            $this->setToolConsumerInfoVersion($versions[0][0]);
            $this->setToolConsumerInfoProductFamilyCode(
                current(explode('.', strtoupper($this->getToolConsumerInfoVersion()), 2))
            );
        } else {
            $this->setToolConsumerInfoVersion('');
            $this->setToolConsumerInfoProductFamilyCode('');
        }
    }

    /**
     * @return string
     */
    public function getResourceLinkId()
    {
        return $this->resourceLinkId;
    }

    /**
     * @param string $resourceLinkId
     */
    public function setResourceLinkId($resourceLinkId)
    {
        $this->resourceLinkId = $resourceLinkId;
    }

    /**
     * @return string
     */
    public function getContextId()
    {
        return $this->contextId;
    }

    /**
     * @param string $contextId
     */
    public function setContextId($contextId)
    {
        $this->contextId = $contextId;
    }

    /**
     * @return string
     */
    public function getLaunchUrl()
    {
        return $this->launchUrl;
    }

    /**
     * @param string $launchUrl
     */
    public function setLaunchUrl($launchUrl)
    {
        $this->launchUrl = $launchUrl;
    }

    /**
     * @return string
     */
    public function getConsumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * @param string $consumerKey
     */
    public function setConsumerKey($consumerKey)
    {
        $this->consumerKey = $consumerKey;
    }

    /**
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->consumerSecret;
    }

    /**
     * @param string $consumerSecret
     */
    public function setConsumerSecret($consumerSecret)
    {
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * @return string
     */
    public function getToolConsumerInstanceGuid()
    {
        return $this->toolConsumerInstanceGuid;
    }

    /**
     * @param string $toolConsumerInstanceGuid
     */
    public function setToolConsumerInstanceGuid($toolConsumerInstanceGuid)
    {
        $this->toolConsumerInstanceGuid = $toolConsumerInstanceGuid;
    }

    /**
     * @return string
     */
    public function getToolConsumerInstanceName()
    {
        return $this->toolConsumerInstanceName;
    }

    /**
     * @param string $toolConsumerInstanceName
     */
    public function setToolConsumerInstanceName($toolConsumerInstanceName)
    {
        $this->toolConsumerInstanceName = $toolConsumerInstanceName;
    }

    /**
     * @return string
     */
    public function getToolConsumerInstanceUrl()
    {
        return $this->toolConsumerInstanceUrl;
    }

    /**
     * @param string $toolConsumerInstanceUrl
     */
    public function setToolConsumerInstanceUrl($toolConsumerInstanceUrl)
    {
        $this->toolConsumerInstanceUrl = $toolConsumerInstanceUrl;
    }

    /**
     * @return string
     */
    public function getToolConsumerInfoProductFamilyCode()
    {
        return $this->toolConsumerInfoProductFamilyCode;
    }

    /**
     * @param string $toolConsumerInfoProductFamilyCode
     */
    public function setToolConsumerInfoProductFamilyCode($toolConsumerInfoProductFamilyCode)
    {
        $this->toolConsumerInfoProductFamilyCode = $toolConsumerInfoProductFamilyCode;
    }

    /**
     * @return string
     */
    public function getToolConsumerInfoVersion()
    {
        return $this->toolConsumerInfoVersion;
    }

    /**
     * @param string $toolConsumerInfoVersion
     */
    public function setToolConsumerInfoVersion($toolConsumerInfoVersion)
    {
        $this->toolConsumerInfoVersion = $toolConsumerInfoVersion;
    }

    /**
     * @return string
     */
    public function getOauthSignatureMethod()
    {
        return $this->oauthSignatureMethod;
    }

    /**
     * @param string $oauthSignatureMethod
     */
    public function setOauthSignatureMethod($oauthSignatureMethod)
    {
        $this->oauthSignatureMethod = $oauthSignatureMethod;
    }
    
    /**
     * @return boolean
     */
    public function isDebug()
    {
        return $this->debug;
    }
    
    /**
     * @param boolean $debug
     */
    public function setDebug($debug)
    {
        $this->debug = !!$debug;
    }

    /**
     * @param array $launchData
     *
     * @return string
     * @throws Exception
     */
    public function getOauthSignature(array $launchData)
    {
        switch ($this->getOauthSignatureMethod()) {
            case 'HMAC-SHA1':
                $algo      = 'sha1';
                $data      = 'POST&' . urlencode($this->getLaunchUrl()) . '&' . rawurlencode(http_build_query($launchData));
                $key       = urlencode($this->getConsumerSecret()) . "&";
                $rawOutput = true;
                return base64_encode(hash_hmac($algo, $data, $key, $rawOutput));
                break;
            default:
                throw new Exception('Unknown oauth signature method.');
        }
    }

    /**
     * @return array
     */
    protected function getLaunchData()
    {
        $launchData = array(
            'lti_message_type' => 'basic-lti-launch-request',
            'lti_version'      => 'LTI-1p0',

            'resource_link_id' => $this->getResourceLinkId(),

            'context_id' => $this->getContextId(),

            'tool_consumer_instance_guid'            => $this->getToolConsumerInstanceGuid(),
            'tool_consumer_instance_name'            => $this->getToolConsumerInstanceName(),
            'tool_consumer_instance_url'             => $this->getToolConsumerInstanceUrl(),
            'tool_consumer_info_product_family_code' => $this->getToolConsumerInfoProductFamilyCode(),
            'tool_consumer_info_version'             => $this->getToolConsumerInfoVersion(),

            'oauth_callback'         => 'about:blank',
            'oauth_consumer_key'     => $this->getConsumerKey(),
            'oauth_version'          => '1.0',
            'oauth_signature_method' => $this->getOauthSignatureMethod()
        );

        return $launchData;
    }

    /**
     * @param libLtiUser $user
     * @param string     $locale
     * @param string     $documentTarget
     * @param string     $returnUrl
     *
     * @throws Exception
     */
    public function launch(libLtiUser $user, $locale, $documentTarget, $returnUrl)
    {
        $launchData = array_merge(
            $this->getLaunchData(),
            array(
                'user_id'                          => $user->getId(),
                'user_image'                       => $user->getImage(),
                'roles'                            => $user->getRole(),
                'lis_person_name_given'            => $user->getGivenName(),
                'lis_person_name_family'           => $user->getFamilyName(),
                'lis_person_name_full'             => $user->getFullName(),
                'lis_person_contact_email_primary' => $user->getEmail(),

                'launch_presentation_locale'          => $locale,
                'launch_presentation_document_target' => $documentTarget,
                'launch_presentation_return_url'      => $returnUrl,

                'oauth_nonce'     => uniqid('', true),
                'oauth_timestamp' => time()
            )
        );
        ksort($launchData);

        $formData = array_merge(
            $launchData,
            array(
                'oauth_signature' => $this->getOauthSignature($launchData)
            )
        );

        echo '<html>';
        echo '    <head>';
        echo '        <meta charset="UTF-8">';
        echo '    </head>';
        echo '    <body ' . ($this->isDebug()? '' : 'onload="document.ltiLaunchForm.submit();"') . '>';
        echo "        <form id='ltiLaunchForm' name='ltiLaunchForm' method='POST' action='{$this->launchUrl}'>";

        foreach ($formData as $key => $value) {
            echo "        <input type='hidden' name='{$key}' value='{$value}'/>";
        }

        echo '        </form>';
        echo '    </body>';
        echo '</html>';
    }
}