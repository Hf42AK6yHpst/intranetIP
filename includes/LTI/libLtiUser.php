<?php


class libLtiUser
{

    const ROLE_INSTRUCTOR = 'Instructor';

    const ROLE_LEARNER = 'Learner';

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $image;

    /**
     * @var string
     */
    public $role;

    /**
     * @var string
     */
    public $givenName;

    /**
     * @var string
     */
    public $familyName;

    /**
     * @var string
     */
    public $fullName;

    /**
     * @var string
     */
    public $email;

    /**
     * libLtiUser constructor.
     *
     * @param string $id
     * @param string $image
     * @param string $role
     * @param string $givenName
     * @param string $familyName
     * @param string $fullName
     * @param string $email
     */
    public function __construct($id, $image, $role, $givenName, $familyName, $fullName, $email)
    {
        $this->setId($id);
        $this->setImage($image);
        $this->setRole($role);
        $this->setGivenName($givenName);
        $this->setFamilyName($familyName);
        $this->setFullName($fullName);
        $this->setEmail($email);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getGivenName()
    {
        return $this->givenName;
    }

    /**
     * @param string $givenName
     */
    public function setGivenName($givenName)
    {
        $this->givenName = $givenName;
    }

    /**
     * @return string
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * @param string $familyName
     */
    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param      $user
     * @param bool $needIconv
     *
     * @return libLtiUser
     */
    public static function createByLibUser($user, $needIconv = false)
    {
        global $intranet_default_lang;

        $userPhotoAry = $user->GET_USER_PHOTO($user->UserLogin);
        if (is_array($userPhotoAry) && isset($userPhotoAry[1])) {
            $userPhoto = rtrim(curPageURL(false, false), '/') . '/' . ltrim($userPhotoAry[1], '/');
        } else {
            $userPhoto = '';
        }

        if ($needIconv) {
            $encoding      = isset($intranet_default_lang) && $intranet_default_lang === 'gb' ? 'GB2312' : 'BIG5-HKSCS';
            $userFirstName = iconv($encoding, 'UTF-8//TRANSLIT//IGNORE', $user->FirstName);
            $userLastName  = iconv($encoding, 'UTF-8//TRANSLIT//IGNORE', $user->LastName);
            $userFullName  = iconv($encoding, 'UTF-8//TRANSLIT//IGNORE', $user->EnglishName);
        } else {
            $userFirstName = $user->FirstName;
            $userLastName  = $user->LastName;
            $userFullName  = $user->EnglishName;
        }

        return new self(
            $user->UserID,
            $userPhoto,
            intval($user->RecordType) === 1 ? self::ROLE_INSTRUCTOR : self::ROLE_LEARNER,
            $userFirstName,
            $userLastName,
            $userFullName,
            $user->UserEmail
        );
    }

    /**
     * @return libLtiUser
     */
    public static function createDummyInstructor()
    {
        return new self(
            self::ROLE_INSTRUCTOR,
            '',
            self::ROLE_INSTRUCTOR,
            'Dummy',
            'Instructor',
            'Dummy Instructor',
            'dummyInstructor@broadlearning.com'
        );
    }

    /**
     * @return libLtiUser
     */
    public static function createDummyLearner()
    {
        return new self(
            self::ROLE_LEARNER,
            '',
            self::ROLE_LEARNER,
            'Dummy',
            'Learner',
            'Dummy Learner',
            'dummyLearner@broadlearning.com'
        );
    }
}