<?php

# being modified by: 

/********************** Change Log ***********************/
#   Date:   2018-11-27  Isaac
#           Added flag $sys_custom['DisableeCommunityMemberPhoto'] to hide the all mambers' photo. 
#
#   Date    2018-11-12  Bill     [2018-1105-1041-02206]
#           modified returnEmailNotificationData(), added __TITLE__ in email subject
#
#	Date:	2016-07-14	Bill
#			modified getOnlineUserID(), updateOnlineStatus(), change split() to explode() for PHP 5.4
#
#	Date:	2014-03-26	YatWoon
#			modified FolderThumbnail() [Case#A59952]
#
#	Date:	2012-02-09	YatWoon
#			add UpdateLastAccess(), for keep track the last accessed group id
#
#	Date:	2011-09-15	YatWoon
#			update IndexViewMode0, 1, 2, 4(), add remark
#
#	Date:	2011-05-03	YatWoon
#			update ShareArea(), Forum(), add alt / title for the icon
#
#	Date:	2011-04-04	YatWoon
#			add function returnEmailNotificationData(), return notification email subject & content
#
#	Date	:	2010-12-01 [YatWoon]
#				add CreateTmpFolder(), if there is no album/folder/category for add photo/video/file/website, then system will create a default album automatically
#
#	Date	:	2010-10-28 [Yuen]
#				modified hasAnyGroupAdminRight() to optimize performance by avoiding to being run twice
#
#	Date	:	2010-10-12 [Yuen]
#				modified member_online_list() to optimize performance by reducing the number of queries
#
#	Date	:	2010-10-04 [Marcus]
#				modified AnnouncementTable, show more btn even if no "existing" (all announcement expired) announcement.
#
#	Date	:	2010-09-10 [YatWoonn]
#				update display PersonalPhotoLink instead of PhotoLink
#
#	Date	:	2010-08-04 [Thomas]
#	Details	:	Added function resize_photo
#
# 	Date	:	2010-06-24 [Yuen]
#	Details	:	improved DisplayPhoto() to show image no larger than given width or height respectively
#
#	Date	:	2010-01-04 [Max] (200912311437)
#	Details	:	- Set condition for chatroom link display in function[member_online_list()]
#				- Hide photo, video, website and file if they are set disabled in function[RightViewMenu()] and in function [RightAddMenu()]
#
# 	Date	:	2009-12-10 [Yuen]
#	Details	:	hide "All" and "Online" tags in eCommunity Member List in order not to confuse user with instant messager
#
/******************* End Of Change Log *******************/


if (!defined("LIBEGROUP_DEFINED"))                     // Preprocessor directive
{
define("LIBEGROUP_DEFINED", true);

class libegroup extends libgroup
{
	
	var $AnyAdminAccessRightArr;

      function libegroup($GroupID='', $GroupsIDs="", $PreloadAllGroupOfUser="")
      {
			global $UserID, $PATH_WRT_ROOT, $LAYOUT_SKIN, $lu2007;
			$this->libgroup($GroupID,$GroupsIDs,$PreloadAllGroupOfUser);
			$this->retrieveAdminRights($UserID);

			//member online status
			//Check the user is group member or not
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
			include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
			if (!isset($lu2007))
			{
				$lu2007 = new libuser2007($UserID);
			}
			if($GroupID and $lu2007->isInGroup($GroupID))
				$this->updateOnlineStatus();
      }

        function GET_MODULE_OBJ_ARR()
        {
                global $UserID, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $GroupID;

                ### wordings
                global $i_frontpage_schoolinfo_groupinfo_egroup, $eComm;

                //check user has any group admin rights
                //$hasanyGroupAdminRights = sizeof($this->hasAnyGroupAdminRight());

                $CurrentPageArr['eGroups'] = 1;

                # Current Page Information init
                $MyGroups		= 0;
                $OtherGroups	= 0;
                $MyPage			= 0;
                $Settings		= 0;
                $Settings_GroupSettings	= 0;
				$Settings_AnnouncementSettings	= 0;
				$Settings_CalendarSettings	= 0;
				$Settings_SharingSettings	= 0;
				$Settings_ForumSettings	= 0;
				$Settings_MemberSettings	= 0;
				$Settings_AnnouncementSettings = 0;

                switch ($CurrentPage) {
                	case "MyGroups":
                    	$MyGroups = 1;
                    	break;
					case "OtherGroups":
                    	$OtherGroups = 1;
                    	break;
					case "MyPage":
                    	$MyPage = 1;
                    	break;
                    case "Settings":
                    	$Settings = 1;
                    	break;
                	case "Settings_GroupSettings":
						$Settings = 1;
						$Settings_GroupSettings = 1;
						break;
					case "Settings_ForumSettings":
						$Settings = 1;
						$Settings_ForumSettings = 1;
						break;
					case "Settings_MemberSettings":
						$Settings = 1;
						$Settings_MemberSettings = 1;
						break;
					case "Settings_AnnouncementSettings":
						$Settings = 1;
						$Settings_AnnouncementSettings = 1;
						break;
					case "Settings_SharingSettings":
						$Settings = 1;
						$Settings_SharingSettings = 1;
						break;
					case "Settings_CalendarSettings":
						$Settings = 1;
						$Settings_CalendarSettings = 1;
						break;
                }

                # Menu information
				$MenuArr["MyGroups"] 	= array($eComm['MyGroups'], $PATH_WRT_ROOT."home/eCommunity/index.php", $MyGroups);
				$MenuArr["OtherGroups"] = array($eComm['OtherGroups'], $PATH_WRT_ROOT."home/eCommunity/otherGroupindex.php", $OtherGroups);
				//$MenuArr["MyPage"] 		= array($eComm['MyPage'], $PATH_WRT_ROOT."home/eCommunity/mypage.php", $MyPage);
				
				$lg = new libegroup($GroupID,'',1);
//				$lg->LoadGroupData($GroupID);

				if(in_array($GroupID,$lg->hasAnyGroupAdminRight()))
				{
					
					if($GroupID || $lg->AdminAccessRight!="NULL" || $lg->AdminAccessRight=='ALL')
					{
						
						$MenuArr["Settings"] 	= array($eComm['Settings'], "#", $Settings);
						
						if ($lg->hasAdminBasicInfo($UserID))
							$MenuArr["Settings"]["Child"]["GroupSettings"] 		= array($eComm['GroupSettings'], 		$PATH_WRT_ROOT."home/eCommunity/group/settings.php?GroupID=".$GroupID, $Settings_GroupSettings);
						if ($lg->hasAdminInternalAnnounce($UserID) || $lg->hasAdminAllAnnounce($UserID))
							$MenuArr["Settings"]["Child"]["Announcement"] 		= array($eComm['AnnouncementSettings'], $PATH_WRT_ROOT."home/eCommunity/announce/settings.php?GroupID=".$GroupID, $Settings_AnnouncementSettings);
						if ($lg->hasAdminFiles($UserID))
							$MenuArr["Settings"]["Child"]["Sharing"] 			= array($eComm['SharingSettings'], 		$PATH_WRT_ROOT."home/eCommunity/files/settings_folder.php?GroupID=".$GroupID, $Settings_SharingSettings);
						if ($lg->hasAdminBulletin($UserID))
							$MenuArr["Settings"]["Child"]["Forum"] 				= array($eComm['ForumSettings'], 		$PATH_WRT_ROOT."home/eCommunity/bulletin/setting.php?GroupID=".$GroupID, $Settings_ForumSettings);
						if ($lg->hasAdminMemberList($UserID))
							$MenuArr["Settings"]["Child"]["Members"] 			= array($eComm['MemberSettings'], 		$PATH_WRT_ROOT."home/eCommunity/members/setting.php?GroupID=".$GroupID, $Settings_MemberSettings);
						
							
					}
				}

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass eCommunity";'."\n";
            $js.= '</script>'."\n";

                # module information
				$MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['title'] = $eComm['eCommunity'].$js;
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_egroup.gif";
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eCommunity/index.php";
                $MODULE_OBJ['menu'] = $MenuArr;

                return $MODULE_OBJ;
        }

		function FolderThumbnail($FolderID, $currentFolderID='')
		{
			global $LAYOUT_SKIN, $image_path, $eComm, $PATH_WRT_ROOT;
			
			if(!is_object($FolderID))
			{
				include_once($PATH_WRT_ROOT."includes/libfolder.php");
				$lfolder = new libfolder($FolderID);
			}
			else
			{
				$lfolder = $FolderID;
				$FolderID = $currentFolderID ? $currentFolderID : $lfolder->FolderID;
			}	
			switch($lfolder->FolderType)
			{
				case "P":
				case "V":
					$img = "album";
					$inner_color = "#6fb4ba";
					break;
				case "W":
					$img = "category";
					break;
				case "F":
				default:
					$img = "folder";
					$inner_color = "#ead76d";
					break;

			}

			$CoverFileID = $lfolder->CoverFile();

					$x ="
						 <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                          <tr>
                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_01.gif\" width=\"16\" height=\"16\"></td>
                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02a.gif\" width=\"111\" height=\"16\"></td>
                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_03.gif\" width=\"22\" height=\"16\"></td>
                          </tr>
                          <tr>
                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\" width=\"16\" height=\"13\"></td>
                            <td bgcolor=\"{$inner_color}\" align=\"center\" ><div id=\"photo_border\"><a href=\"javascript:viewFolder(".$FolderID.");\"><div >". $this->DisplayPhoto($CoverFileID, 0, 165) ."</div></a></div></td>
                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06a.gif\" width=\"22\" height=\"110\"></td>
                          </tr>
                          <tr>
                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_07.gif\" width=\"16\" height=\"9\"></td>
                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\" width=\"16\" height=\"9\"></td>
                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_09.gif\" width=\"22\" height=\"9\"></td>
                          </tr>
                        </table>
					";

			return $x;
		}


		function DisplayPhoto($FileID, $width='0', $height='0', $in_file_view=0, $link='')
		{
			global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;

			if(!is_object($FileID))
			{
				include_once($PATH_WRT_ROOT."includes/libfile.php");
				$li = new libfile($FileID);
			}
			else
			{
				$li = $FileID;
				$FileID = $li->FileID;
			}
			$newWidth = $width;
			$newHeight = $height;

			$newFilename = $li->Location;
			$FileLocation = $PATH_WRT_ROOT."file/group/g".$li->GroupID."/".$newFilename;
			$FilePhsical = $intranet_root."/file/group/g".$li->GroupID."/".$newFilename;

			if (isImage($FilePhsical))
			{
// 				debug_pr($FileLocation);
				list($w, $h) = getimagesize($FilePhsical);
				
				# if both width and height are input, resize image in aspect ratio and not bigger than width or height
				if ($width>0 && $height>0)
				{
					if ($width>=$height)
					{
						if ($w>$width)
						{
							$new_str = "width: {$newWidth}px";
						} elseif ($h>$height)
						{
							$new_str = "height: {$newHeight}px";
						} 
					} elseif ($width<$height)
					{
						if ($h>$height)
						{
							$new_str = "height: {$newHeight}px";
						} elseif ($w>$width)
						{
							$new_str = "width: {$newWidth}px";
						}
					} 
				} elseif ($width)
				{
					$newWidth = $w>$width ? $width : $w;
					$new_str = "width: {$newWidth}px";
				}
				else
				{
					$newHeight = $h>$height ? $height : $h;
					$new_str = "height: {$newHeight}px";
				}

				$x = $link."<img src='".$FileLocation."' style='{$new_str}' border='0'>".($link?"</a>":"");
			}
			else
			{
				if($width)
					$new_str = "width: ". $newWidth ."px";
				else
					$new_str = "height: ". $newHeight ."px";

				if(!$in_file_view)
				{

					switch($li->FileType)
					{
						case "W":
							$x = $link."<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/img_website_dummy.gif' style='{$new_str}' border='0'>".($link?"</a>":"");
							break;
						case "V":
							$x = $link."<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/img_video_dummy.gif' style='{$new_str}' border='0'>".($link?"</a>":"");
							break;
						case "F":
						default:
							$x = $link."<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/img_file_dummy.gif' style='{$new_str}' border='0'>".($link?"</a>":"");
							break;
					}
				}
				else
				{
					switch($li->FileType)
					{
						case "W":
							$x = $link."<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/img_website_dummy.gif' width='187' height='187' border='0'>".($link?"</a>":"");
							break;
						case "V":
							switch(get_file_ext($li->Location))
							{
								case ".mpg":
								case ".avi":
									$player_id = "MediaPlayer";
									$player_clsid = 'classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95" ';
									$plyaer_codebase = 'codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112" ';
									$player_mime = 'type="application/x-mplayer2" ';
									$image_size = "";
									break;
								case ".mov":
								case ".3gp":
									$player_id = "QuickTime";
									$player_clsid = 'classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" ';
									$plyaer_codebase = 'codebase="http://www.apple.com/qtactivex/qtplugin.cab" ';
									$player_mime = 'type="video/x-quicktime" ';
									$image_size = 'width="500" height="300" ';
									break;
								case ".rm":
									$x = '<a href="#">
									<OBJECT ID=RVOCX CLASSID="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" WIDTH=500 HEIGHT=300>
									<PARAM NAME="SRC" VALUE="'. $FileLocation .'">
									<PARAM NAME="CONTROLS" VALUE="ImageWindow">
									<PARAM NAME="CONSOLE" VALUE="one">
									<PARAM NAME="AUTOSTART" VALUE="false">
									<EMBED SRC="'. $FileLocation .'" WIDTH=500 HEIGHT=300 NOJAVA=true CONTROLS=ImageWindow CONSOLE=one AUTOSTART=true>
									</OBJECT><br>
									<OBJECT ID=RVOCX CLASSID="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" WIDTH=500 HEIGHT=36>
									<PARAM NAME="CONTROLS" VALUE="All">
									<PARAM NAME="CONSOLE" VALUE="one">
									<EMBED SRC="plugin.rpm" WIDTH=500 HEIGHT=36 NOJAVA=true CONTROLS=All CONSOLE=one>
									</OBJECT>
									</a>';
									break;
								default:
									$player_id = "";
									$x = $link."<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/img_video_dummy.gif' width='187' height='187' border='0'>".($link?"</a>":"");
									break;
							}


							$autostart = 0;
							$player  = '<object id="'.$player_id.'" '.$player_clsid.$player_codebase.$player_mime.$image_size.'>';
					        $player .= "<param name=\"autoplay\" value=\"$autostart\" />";
					        $player .= "<param name=\"src\" value=\"". $FileLocation . "\" />";
					        $player .= $other_settings;
					        $player .= '<embed '.$image_size.' src="'. $FileLocation . '" autoplay="'.$autostart.'" '.$player_mime.'></embed>';
					        $player .= "</object>\n";

					        if($player_id)	$x = "<a href='#'>" . $player . "</a>";
							break;
						case "F":
						default:
							$x = $link."<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/img_file_dummy.gif'  border='0'>".($link?"</a>":"");
							break;
					}
				}

			}

			return $x;
		}

		function DisplayFileListTable($FileID, $FolderID, $FileType)
		{
			global $image_path, $LAYOUT_SKIN, $eComm;

			switch($FileType)
			{
				case "P":
				case "V":
					$img = "album";
					break;
				case "W":
					$img = "category";
					break;
				case "F":
				default:
					$img = "folder";
					break;

			}

			$content = $this->DisplayFileList($FileID, $FolderID);

			if($content)
			{
				$x = "
				  <tr>
				    <td>
				    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id='albumareatable'>
				      <tr>
				        <td width=\"16\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_01.gif\" width=\"16\" height=\"16\"></td>
				        <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02a.gif\" width=\"111\" height=\"16\"></td>
				        <td width=\"22\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_03.gif\" width=\"22\" height=\"16\"></td>
				      </tr>
				      <tr>
				        <td width=\"16\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"16\" height=\"180\"></td>
				        <td valign=\"top\" bgcolor=\"#FFFFFF\">

						<div id='albumarea' style=\"position:absolute; width:600px; height:180px; visibility: visible; overflow: auto;\">
						<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">
						  <tr>
				";

				$x .= $content;

				$x .= "
					</tr>
					</table>
						</div>
						</td>
				        <td width=\"22\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06a.gif\" width=\"22\" height=\"110\"></td>
				      </tr>
				      <tr>
				        <td width=\"16\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_07.gif\" width=\"16\" height=\"9\"></td>
				        <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\" width=\"16\" height=\"9\"></td>
				        <td width=\"22\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_09.gif\" width=\"22\" height=\"9\"></td>
				      </tr>
				    </table>
				    </td>
				  </tr>
				";
			}

			return $x;
		}

		function DisplayFileList($FileID, $FolderID)
		{
			global $PATH_WRT_ROOT;

			$this->libdb();
			$sql = "select * from INTRANET_FILE where FolderID=$FolderID and Approved=1 order by onTop desc , UserTitle";
			$x = $this->returnArray($sql,1);
			$r = "";

			include_once($PATH_WRT_ROOT."includes/libfile.php");
	        $rr = array();
	        //re-order the list (check private and hidden)
	        for($i=0;$i<sizeof($x);$i++)
	        {
				$lf 			= new libfile($x[$i]['FileID']);
				if(!$lf->viewFileRight())	continue;
				array_push($rr, $x[$i]);
	        }
			
			$FileID_List = "<td><form id=\"FileID_List\">";
			
			for($i=0;$i<sizeof($rr);$i++)
			{
				$FileID_List .= "<input type=\"hidden\" name=\"item_".$i."\" value=\"".$rr[$i]['FileID']."\">";
				$title = $rr[$i]['UserTitle'] ? $rr[$i]['UserTitle'] : $rr[$i]['Title'];
				$r .= "
					<td id=\"img_".$rr[$i]['FileID']."\" align=\"center\" valign=\"middle\" width=\"150\" class=\"". ($FileID==$rr[$i]['FileID']?"current_select_item":"" )."\" onMouseOver=\"this.className='mouseover_item'\" onMouseOut=\"this.className='". ($FileID==$rr[$i]['FileID']?"current_select_item":"" )."'\">
						<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\" width=\"120\">
						<tr>
							<td height=\"120\">
								<div id=\"photo_border\"><center>
								<a href=\"javascript:viewfile(". $rr[$i]['FileID'] .")\"><span>". $this->DisplayPhoto($rr[$i]['FileID'], 0, 100) ."</span></a>
								</center>
								</div>
							</td>
						</tr>
						<tr>
							<td align=\"center\"><a href=\"javascript:viewfile(". $rr[$i]['FileID'] .")\" class=\"share_file_title\"><strong>". $title ." </strong></a></td>
						</tr>
						</table>
					</td>
				";
				if($i==0) $File_start = "<input type=\"hidden\" id=\"FileStart\" value=\"".$rr[0]['FileID']."\">\n";
				if($i==sizeof($rr)-1) $File_start .= "<input type=\"hidden\" id=\"FileEnd\" value=\"".$rr[$i]['FileID']."\">";
			}
			
			$r = $r."<td>".$File_start."</td>".$FileID_List."</form></td>";
			
			return $r;
		}

		function DisplayComments($FileID)
		{
			global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;

			include_once($PATH_WRT_ROOT."includes/libuser.php");

			$this->libdb();
			$sql = "select * from INTRANET_FILE_COMMENT where FileID=$FileID order by DateInput desc";
			$x = $this->returnArray($sql,1);
			$r = "";

			for($i=0;$i<sizeof($x);$i++)
			{
				$lu = new libuser($x[$i]['UserID']);
				$photo = (is_file($intranet_root.$lu->PersonalPhotoLink)) ? $lu->PersonalPhotoLink  : "/images/myaccount_personalinfo/samplephoto.gif";

				$r .= "
					<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
			        <tr>
			          <td width=\"100\">
			          <table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
			            <tr>
			              <td width=\"120\" align=\"center\">
							<table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
							<tr>
								<td><div id=\"photo_border_nolink\"><span><img src=\"". $photo ."\" style=\"width:45px\" border=\"0\"></span></div></td>
							</tr>
							</table>
						</td>
						</tr>
			            <tr>
			              <td width=\"120\" align=\"center\"><span class=\"tabletext\">". $x[$i]['UserName'] ." </span></td>
			              </tr>
			          </table></td>
			          <td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			            <tr>
			              <td width=\"25\" height=\"7\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_01.gif\" width=\"25\" height=\"7\"></td>
			              <td height=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_02.gif\" width=\"12\" height=\"7\"></td>
			              <td width=\"9\" height=\"7\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_03.gif\" width=\"9\" height=\"7\"></td>
			            </tr>
			            <tr>
			              <td width=\"25\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_04b.gif\" width=\"25\" height=\"40\"></td>
			              <td bgcolor=\"#ffffe9\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
			                <tr>
			                  <td align=\"right\" class=\"tabletext\">". $x[$i]['DateInput'] ."</td>
			                </tr>
			                <tr>
			                  <td class=\"tabletext\">". nl2br($x[$i]['Comment']) ."</td>
			                </tr>
			                <tr>
			                  <td height=\"10\" class=\"tabletext\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"></td>
			                </tr>
			              </table></td>
			              <td width=\"9\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_06.gif\" width=\"9\" height=\"13\"></td>
			            </tr>
			            <tr>
			              <td width=\"25\" height=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_07.gif\" width=\"25\" height=\"10\"></td>
			              <td height=\"10\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_08.gif\" width=\"9\" height=\"10\"></td>
			              <td width=\"9\" height=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/comment_board_09.gif\" width=\"9\" height=\"10\"></td>
			            </tr>
			          </table></td>
			        </tr>
			      </table>

			      <br>
				";
			}

			return $r;
		}

		function RightViewMenu($indexpage='')
		{
			global $image_path, $LAYOUT_SKIN, $eComm;
			global $GroupID, $DisplayType, $FileType, $currentMode;

			$rightMenu = "<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
			$rightMenu .= "<tr><td align=\"left\">";
			if($indexpage)
				$rightMenu .= "<span id=\"ft_span_\" class=\"share_sub_list_current\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, -1, 0, '')\" class=\"share_sub_list\">";
			else
				$rightMenu .= ($FileType=='' && !$currentMode)? "<span class=\"share_sub_list_current\">":"<a href=\"../files/index.php?GroupID={$GroupID}&FileType=&DisplayType={$DisplayType}\" class=\"share_sub_list\">";
			$rightMenu .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_shares.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
			$rightMenu .= $eComm['LatestShare'];
			$rightMenu .= ($FileType=='' && !$currentMode)? "</span>":"</a>";
			$rightMenu .= "</td></tr>";

			if ($this->AllowedImageTypes == "DISABLED") {
				// do nothing
			} else {
				$rightMenu .= "<tr><td align=\"left\">";
				if($indexpage)
					$rightMenu .= "<span id=\"ft_span_p\" class=\"share_sub_list\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, -1, 0, 'P')\" class=\"share_sub_list\">";
				else
					$rightMenu .= ($FileType=='P' && !$currentMode)? "<span class=\"share_sub_list_current\">":"<a href=\"../files/index2.php?GroupID={$GroupID}&FileType=P&DisplayType={$DisplayType}\" class=\"share_sub_list\">";
				$rightMenu .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_photo.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
				$rightMenu .= $eComm['Photo'];
				$rightMenu .= ($FileType=='P' && !$currentMode)? "</span>":"</a>";
				$rightMenu .= "</td></tr>";
			}
			
			if ($this->AllowedVideoTypes == "DISABLED") {
				// do nothing
			} else {
				$rightMenu .= "<tr><td align=\"left\">";
				if($indexpage)
					$rightMenu .= "<span id=\"ft_span_v\" class=\"share_sub_list\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, -1, 0, 'V')\" class=\"share_sub_list\">";
				else
					$rightMenu .= ($FileType=='V' && !$currentMode)? "<span class=\"share_sub_list_current\">":"<a href=\"../files/index2.php?GroupID={$GroupID}&FileType=V&DisplayType={$DisplayType}\" class=\"share_sub_list\">";
				$rightMenu .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_video.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
				$rightMenu .= $eComm['Video'];
				$rightMenu .= ($FileType=='V' && !$currentMode)? "</span>":"</a>";
				$rightMenu .= "</td></tr>";
			}
			
			if ($this->AllowedWebsite) {
				$rightMenu .= "<tr><td align=\"left\">";
				if($indexpage)
					$rightMenu .= "<span id=\"ft_span_w\" class=\"share_sub_list\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, -1, 0, 'W')\" class=\"share_sub_list\">";
				else
					$rightMenu .= ($FileType=='W' && !$currentMode)? "<span class=\"share_sub_list_current\">":"<a href=\"../files/index2.php?GroupID={$GroupID}&FileType=W&DisplayType={$DisplayType}\" class=\"share_sub_list\">";
				$rightMenu .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_website.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
				$rightMenu .= $eComm['Website'];
				$rightMenu .= ($FileType=='W' && !$currentMode)? "</span>":"</a>";
				$rightMenu .= "</td></tr>";
			} else {
				// do nothing
			}
			
			if ($this->AllowedFileTypes == "DISABLED") {
				// do nothing
			} else {
				$rightMenu .= "<tr><td align=\"left\">";
				if($indexpage)
					$rightMenu .= "<span id=\"ft_span_f\" class=\"share_sub_list\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, -1, 0, 'F')\" class=\"share_sub_list\">";
				else
					$rightMenu .= ($FileType=='F' && !$currentMode)? "<span class=\"share_sub_list_current\">":"<a href=\"../files/index2.php?GroupID={$GroupID}&FileType=F&DisplayType={$DisplayType}\" class=\"share_sub_list\">";
				$rightMenu .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_file.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
				$rightMenu .= $eComm['File'];
				$rightMenu .= ($FileType=='F' && !$currentMode)? "</span>":"</a>";
				$rightMenu .= "</td></tr>";
			}
			
			$rightMenu .= "<tr><td align=\"left\">";
			if($indexpage)
				$rightMenu .= "<span id=\"ft_span_wa\" class=\"share_sub_list\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, -1, 0, 'WA')\" class=\"share_sub_list\">";
			else
				$rightMenu .= ($FileType=='WA' && !$currentMode)? "<span class=\"share_sub_list_current\">":"<a href=\"../files/index.php?GroupID={$GroupID}&FileType=WA&DisplayType={$DisplayType}\" class=\"share_sub_list\">";
			$rightMenu .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_file.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
			$rightMenu .= $eComm['WaitingRejectList'];
			$rightMenu .= ($FileType=='WA' && !$currentMode)? "</span>":"</a>";
			$rightMenu .= "</td></tr>";

			$rightMenu .= "</table>";

			return $rightMenu;
		}

        function RightAddMenu()
        {
	        global $image_path, $LAYOUT_SKIN, $eComm;
			global $GroupID, $DisplayType, $FileType, $currentMode;

	     	$x = "<table width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";

			if ($this->AllowedImageTypes == "DISABLED") {
				// do nothing
			} else {
		     	$x .= "<tr>";
				$x .= "<td align=\"left\">";
				$x .= ($FileType=='P' && $currentMode=='add')? "<span class=\"share_sub_list_add_current\">":"<a href=\"javascript:newFile('photo');\" class=\"share_sub_list_add\">";
				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_photo_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ". $eComm['Photo'];
				$x .= ($FileType=='P' && $currentMode=='add')? "</span>":"</a>";
				$x .= "</td></tr>";
			}
			
			if ($this->AllowedVideoTypes == "DISABLED") {
				// do nothing
			} else {
				$x .= "<tr>";
				$x .= "<td align=\"left\">";
				$x .= ($FileType=='V' && $currentMode=='add')? "<span class=\"share_sub_list_add_current\">":"<a href=\"javascript:newFile('video');\" class=\"share_sub_list_add\">";
				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_video_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ". $eComm['Video'];
				$x .= ($FileType=='V' && $currentMode=='add')? "</span>":"</a>";
				$x .= "</td></tr>";
			}
			
			if ($this->AllowedWebsite) {
				$x .= "<tr>";
				$x .= "<td align=\"left\">";
				$x .= ($FileType=='W' && $currentMode=='add')? "<span class=\"share_sub_list_add_current\">":"<a href=\"javascript:newFile('website');\" class=\"share_sub_list_add\">";
				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_website_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ". $eComm['Website'];
				$x .= ($FileType=='W' && $currentMode=='add')? "</span>":"</a>";
				$x .= "</td></tr>";
			} else {
				// do nothing
			}
			
			if ($this->AllowedFileTypes == "DISABLED") {
				// do nothing
			} else {
				$x .= "<tr>";
				$x .= "<td align=\"left\">";
				$x .= ($FileType=='F' && $currentMode=='add')? "<span class=\"share_sub_list_add_current\">":"<a href=\"javascript:newFile('file');\" class=\"share_sub_list_add\">";
				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_file_add.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> ". $eComm['File'];
				$x .= ($FileType=='F' && $currentMode=='add')? "</span>":"</a>";
				$x .= "</td></tr>";
			}
			$x .= "</table>";

			//<span class="share_sub_list_add_current">

              return $x;
        }

        function GroupLogo()
        {
	        global $image_path, $LAYOUT_SKIN, $intranet_root;

	        $logo_link = "{$image_path}/{$LAYOUT_SKIN}/ecomm/sample_group_logo.gif";
			if (is_file($intranet_root.$this->GroupLogoLink))
			    $logo_link = $this->GroupLogoLink;

	        $x = "
		        <table height=\"200\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		        <tr>
		          <td width=\"7\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_01.gif\" width=\"7\" height=\"8\"></td>
		          <td height=\"8\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_02.gif\" width=\"8\" height=\"8\"></td>
		          <td width=\"11\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_03.gif\" width=\"11\" height=\"8\"></td>
		        </tr>
		        <tr>
		          <td width=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_04.gif\" width=\"7\" height=\"9\"></td>
		          <td><img src=\"{$logo_link}\" width=\"225\" height=\"170\"></td>
		          <td width=\"11\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_06.gif\" width=\"11\" height=\"11\"></td>
		        </tr>
		        <tr>
		          <td width=\"7\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_07.gif\" width=\"7\" height=\"8\"></td>
		          <td height=\"8\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_08.gif\" width=\"11\" height=\"8\"></td>
		          <td width=\"11\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/group_photo_bg_09.gif\" width=\"11\" height=\"8\"></td>
		        </tr>
		      </table>
	        ";

	        return $x;
        }

        function AnnouncementTable($announcement_list_ontop, $announcement_list_not_ontop, $limit=5)
        {
	        global $image_path, $LAYOUT_SKIN, $i_general_Poster, $i_adminmenu_announcement, $i_general_more, $i_no_record_exists_msg, $eComm;

	        $x = "
				<table width=\"100%\" height=\"200\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		        <tr>
		          <td width=\"5\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_01.gif\" width=\"5\" height=\"28\"></td>
		          <td height=\"28\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_02.gif\"><table width=\"100%\" height=\"28\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		            <tr>
		              <td align=\"left\"><table height=\"28\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		                  <tr>
		                    <td width=\"9\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_title_01.gif\" width=\"9\" height=\"28\"></td>
		                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_title_02.gif\" class=\"ecomm_box_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_announ.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> ". $i_adminmenu_announcement ."</td>
		                    <td width=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_title_03.gif\" width=\"10\" height=\"28\"></td>
		                  </tr>
		              </table></td>
		              <td align=\"right\" valign=\"top\"></td>
		            </tr>
		          </table></td>
		          <td width=\"7\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_03.gif\" width=\"7\" height=\"28\"></td>
		        </tr>
		        <tr>
		          <td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_04.gif\" width=\"5\" height=\"8\"></td>
		          <td height=\"164\" valign=\"top\">
		          <table width=\"100%\" border=\"0\" height=\"164\" cellspacing=\"0\" cellpadding=\"0\">

		          <tr><td  height=\"144\" valign=\"top\">
		          <div id=\"announcement_div\" style=\" width:100%; height:144px; visibility: visible; overflow: auto;\">
		            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">

		              <tr>
		                <td height=\"3\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"3\"></td>
		              </tr>
				";

				## display on top records
				$cur_no = 0;
				for($i=0;($i<sizeof($announcement_list_ontop) && $cur_no<$limit);$i++)
				{
					if($announcement_list_ontop[$i]['onTop'])
					{
						$x .= "
							<tr>
				                <td align=\"left\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				                    <tr>
				                      <td width=\"12\">". ($announcement_list_ontop[$i]['Attachment']?"<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/whatnews/icon_attachment.gif\" width=\"12\" height=\"18\">":"") ."</td>
				                      <td nowrap><img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_ontop.gif' align='absmiddle' title='". $eComm['Top'] ."' alt='". $eComm['Top'] ."'>".
				                      ($announcement_list_ontop[$i]['PublicStatus']?"":"<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif' align='absmiddle' title='". $eComm['Private'] ."' alt='". $eComm['Private'] ."'>") .
				                      "<a href=\"javascript:fe_view_announcement(".$announcement_list_ontop[$i]['AnnouncementID'].", 1);\" class=\"indexnewslistgroup\" >". $announcement_list_ontop[$i][2] ."</a> ". ($announcement_list_ontop[$i][3]?"<img src=\"{$image_path}/{$LAYOUT_SKIN}/alert_new.gif\" width=\"28\" height=\"18\" align=\"absmiddle\">":"") ."</td>
				                    </tr>
				                    <tr>
				                      <td>&nbsp;</td>
				                      <td class=\"indexnewsdesc\">(". $announcement_list_ontop[$i][1] .") ". $i_general_Poster ." ". $announcement_list_ontop[$i][5] ."</td>
				                    </tr>
				                </table></td>
				              </tr>
				              <tr>
				                <td height=\"2\" align=\"left\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
				              </tr>
						";
						$cur_no++;
					}
				}

				## display NOT on top records
				for($i=0;($i<sizeof($announcement_list_not_ontop) && $cur_no<$limit);$i++)
				{
					if(!$announcement_list_not_ontop[$i]['onTop'])
					{
						$x .= "
							<tr>
				                <td align=\"left\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
				                    <tr>
				                      <td width=\"12\">". ($announcement_list_not_ontop[$i]['Attachment']?"<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/whatnews/icon_attachment.gif\" width=\"12\" height=\"18\">":"") ."</td>
				                      <td nowrap>".
				                      ($announcement_list_not_ontop[$i]['PublicStatus']?"":"<img src='{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_private.gif' align='absmiddle' title='". $eComm['Private'] ."' alt='". $eComm['Private'] ."'>") .
				                      "<a href=\"javascript:fe_view_announcement(".$announcement_list_not_ontop[$i]['AnnouncementID'].",1);\" class=\"indexnewslistgroup\">". $announcement_list_not_ontop[$i][2] ."</a> ". ($announcement_list_not_ontop[$i][3]?"<img src=\"{$image_path}/{$LAYOUT_SKIN}/alert_new.gif\" width=\"28\" height=\"18\" align=\"absmiddle\">":"") ."</td>
				                    </tr>
				                    <tr>
				                      <td>&nbsp;</td>
				                      <td class=\"indexnewsdesc\">(". $announcement_list_not_ontop[$i][1] .") ". $i_general_Poster ." ". $announcement_list_not_ontop[$i][5] ."</td>
				                    </tr>
				                </table></td>
				              </tr>
				              <tr>
				                <td height=\"2\" align=\"left\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
				              </tr>
						";
						$cur_no++;
					}
				}

				if(!$cur_no)
				{
					$x .= "
						<tr><td height=\"100\" align=\"center\" class=\"tabletext\">$i_no_record_exists_msg</td></tr>
					";
		        }

				$x .= "
		            </table>
		          </div>
		          </td></tr>
		          ";

//		          if($cur_no)
//		          {
			          $x .= "
			          <tr><td height=\"20\" align=\"right\">
			          <a href=\"#\" onclick=\"javascript:moreNews(1);\" class=\"box_more\">{$i_general_more}...</a>
			          </td></tr>
			          ";
//		          }

		          $x .= "</table>
		          </td>
		          <td width=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_06.gif\" width=\"7\" height=\"20\"></td>
		        </tr>
		        <tr>
		          <td width=\"5\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_07.gif\" width=\"5\" height=\"8\"></td>
		          <td height=\"8\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_08.gif\" width=\"9\" height=\"8\"></td>
		          <td width=\"7\" height=\"8\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/announ_board_09.gif\" width=\"7\" height=\"8\"></td>
		        </tr>
		      </table>

	        ";

	        return $x;
        }

        function calendar($CalID)
        {
	        global $image_path, $LAYOUT_SKIN, $lcal;

	        $x = "
	        <table width=\"200\" height=\"200\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		        <tr>
		          <td width=\"2\" height=\"2\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_01.gif\" width=\"2\" height=\"2\"></td>
		          <td height=\"2\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_02.gif\" width=\"2\" height=\"2\"></td>
		          <td width=\"4\" height=\"2\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_03.gif\" width=\"4\" height=\"2\"></td>
		        </tr>
		        <tr>
		          <td width=\"2\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_04.gif\" width=\"2\" height=\"4\"></td>
		          <td height=\"192\" valign=\"top\">


	        	<span id=\"CalContent\"  >
					<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\">
					<tr>
						<td align=\"left\" valign=\"top\" >
						<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
						<tr>
							<td width=\"12\"><p>". $lcal->displayPrevMonth() ."</p></td>
							<td align=\"center\" class=\"indexcalendarmonth\">". $lcal->displayMonthText() ."</td>
							<td width=\"12\"><p>". $lcal->displayNextMonth() ."</p></td>
						</tr>
						</table>
						</td>
					</tr>
					<tr height=\"100%\" valign=\"top\">
						<td>
						<span id=\"CalContentDiv\"  > ". $lcal->displayeCommCalendar(date("m"), date("Y"), $CalID) ."</span>
						</td>
					</tr>
					</table>
					</span>


					</td>
		          <td width=\"4\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_06a.gif\" width=\"4\" height=\"6\"></td>
		        </tr>
		        <tr>
		          <td width=\"2\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_07.gif\" width=\"2\" height=\"4\"></td>
		          <td height=\"4\" align=\"left\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_08a.gif\" width=\"5\" height=\"4\"></td>
		          <td width=\"4\" height=\"4\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/calendar_board_09.gif\" width=\"4\" height=\"4\"></td>
		        </tr>
		      </table>
	        ";

	     	return $x;
        }

		function member_list($list_type='all')
		{
	        global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root, $eComm, $GroupID;
	        $members = $this->returnGroupUser();

	     	$x = "
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		          <tr>
		            <td width=\"5\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_01.gif\" width=\"5\" height=\"28\"></td>
		            <td height=\"28\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_02.gif\"><table width=\"100%\" height=\"28\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		              <tr>
		                <td width=\"9\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_title_01.gif\" width=\"9\" height=\"28\"></td>
		                <td align=\"left\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_title_02.gif\" class=\"ecomm_box_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_egroup.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> ". $eComm['Member'] ." (". sizeof($members) .") </td>
		                <td width=\"9\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_title_03.gif\" width=\"9\" height=\"28\"></td>
		              </tr>
		            </table></td>
		            <td width=\"7\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_03.gif\" width=\"7\" height=\"28\"></td>
		          </tr>
		          <tr>
		            <td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_04.gif\" width=\"5\" height=\"24\"></td>
		            <td valign=\"top\" bgcolor=\"#FFFFFF\">

		            <div id=\"member_list_div\" style=\" width:100%; height:540px; visibility: visible; overflow:auto; \">
				";


			$x .= $this->member_online_list($list_type);

			$x .= "
				</div>
				</td>
		            <td width=\"7\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_06.gif\" width=\"7\" height=\"540\"></td>
		          </tr>
		          <tr>
		            <td width=\"5\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_07.gif\" width=\"5\" height=\"6\"></td>
		            <td height=\"6\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_08.gif\" width=\"9\" height=\"6\"></td>
		            <td width=\"7\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/member_board_09.gif\" width=\"7\" height=\"6\"></td>
		          </tr>
		        </table>
	     	";
	     	return $x   ;
        }

        function member_online_list($list_type='online')
        {
            global $image_path, $LAYOUT_SKIN, $lu, $PATH_WRT_ROOT, $intranet_root, $eComm, $GroupID, $sys_custom;
	        include_once($PATH_WRT_ROOT."includes/libuser.php");

	        $online_id = explode(",", $this->getOnlineUserID());
	        $members = $this->returnGroupUser();

	        ## online user
			/* START ONLINE members */
				$online_x .="<!--START ONLINE members//-->";

/* not in used since 2010-10-12
		  	for($m=0;$m<sizeof($online_id);$m++)
			{
				if(trim($online_id[$m])=="")	continue;
				$lu = new libuser($online_id[$m]);

				$photo = (is_file($intranet_root.$lu->PersonalPhotoLink)) ? $lu->PersonalPhotoLink  : "/images/myaccount_personalinfo/samplephoto.gif";

				$online_x .= "
					<tr>
					<td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\" class=\"member_status_online\" onMouseOver=\"this.className='member_status_selected'\" onMouseOut=\"this.className='member_status_online'\">
					<tr>
					<td width=\"35\"><img src=\"". $photo ."\" style=\"width:35px\" border=\"0\"></td>
					<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
					<td height=\"30\" align=\"left\" class=\"member_name\">". $lu->getNameWithClassNumber($online_id[$m]) ." </td>
					</tr>
					<tr>
					<td align=\"left\" class=\"status_online_link\">Online</td>
					</tr>

				";


				$online_x .="
				</table></td>
				</tr>
				</table></td>
				</tr>
				";
			}
			*/
			for($m=0;$m<sizeof($members);$m++)
		  {
			  if ($members[$m]['UserID']<>"" && $members[$m]['UserID']>0)
			  	$AllMemberIdArray[] = $members[$m]['UserID'];
		  }
			$lu = new libuser("", "", $AllMemberIdArray);
			for($m=0;$m<sizeof($online_id);$m++)
			{
				if(trim($online_id[$m])=="")	continue;
				# load corresponding user 
				$lu->LoadUserData($online_id[$m]);

				$photo = (is_file($intranet_root.$lu->PersonalPhotoLink)) ? $lu->PersonalPhotoLink  : "/images/myaccount_personalinfo/samplephoto.gif";

				$online_x .= "
					<tr>
					<td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\" class=\"member_status_online\" onMouseOver=\"this.className='member_status_selected'\" onMouseOut=\"this.className='member_status_online'\">
					<tr>
					".($sys_custom['DisableeCommunityMemberPhoto']?"":"<td width=\"35\"><img src=\"". $photo ."\" style=\"width:35px\" border=\"0\"></td>").
					"<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
					<td height=\"30\" align=\"left\" class=\"member_name\">". $lu->StandardDisplayName ." </td>
					</tr>
					<tr>
					<td align=\"left\" class=\"status_online_link\">Online</td>
					</tr>

				";


				$online_x .="
				</table></td>
				</tr>
				</table></td>
				</tr>
				";
			}
			
			$online_x .="<!--END ONLINE members//-->";

			/* END ONLINE members */


			/* START all OFFLINE members */
		  $offline_x .= "
				<!--START OFFLINE members//-->
			";
		  for($m=0;$m<sizeof($members);$m++)
		  {
			  ### filter online members
			  if(in_array($members[$m]['UserID'], $online_id))	continue;

			  //$lu = new libuser($members[$m]['UserID']);
			  
				$lu->LoadUserData($members[$m]['UserID']);
				
			  $photo = (is_file($intranet_root.$lu->PersonalPhotoLink)) ? $lu->PersonalPhotoLink  : "/images/myaccount_personalinfo/samplephoto.gif";

			  $offline_x .= "
			  <tr>
			    <td><table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\" class=\"member_status_offline\" onMouseOver=\"this.className='member_status_selected'\" onMouseOut=\"this.className='member_status_offline'\">
			      <tr>
			        ".($sys_custom['DisableeCommunityMemberPhoto']?"":"<td width=\"35\"><img src=\"". $photo ."\" style=\"width:35px\" border=\"0\"></td>").
			        "<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			            <tr>";
			   // $offline_x .= "          <td height=\"30\" align=\"left\" class=\"member_name\">". $lu->getNameWithClassNumber($members[$m]['UserID']) ." </td>";
			    $offline_x .= "          <td height=\"30\" align=\"left\" class=\"member_name\">". $lu->StandardDisplayName ." </td>";
 				$offline_x .= " </tr>
				        </table>
			        </td>
			      </tr>
			    </table></td>
			  </tr>
			  ";
		  }
			$offline_x .="<!--END OFFLINE members//-->";

		  /* END all OFFLINE members */


	     	$x = "
					  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
				";

			/* START all / online select bar */
			/*
			$x .= "
			<!-- START all / online select bar //-->
			  <tr>
			    <td><div id=\"changestatus\">
						<ul>
							<li id=\"". ($list_type=='all'? "current" : "") ."\"><a href=\"javascript:jAJAX_MEMBER_LIST(document.memberlistform, 'all');\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_status_all.gif\" width=\"12\" height=\"15\" border=\"0\" align=\"absmiddle\">All</span></a></li>
							<li id=\"". ($list_type=='online'? "current" : "") ."\"><a href=\"javascript:jAJAX_MEMBER_LIST(document.memberlistform, 'online');\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_status_online.gif\" width=\"12\" height=\"15\" border=\"0\" align=\"absmiddle\">Online</span></a></li>
						</ul>
						</div>			</td>
			  </tr>
			<!-- END all / online select bar //-->
			";
			*/
			/* END all / online select bar */

//			// chat room link
//			$x .= " <!-- chat room link //-->
//				<tr>
//					<td align=\"right\">
//						<a href=\"../chat/index.php?GroupID={$GroupID}\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $eComm['chatroom'] ."</a>
//					</td>
//				</tr>";
			// chat room link
			### Chat Room Function Access ###
			# Group tools settings
			# Field : FunctionAccess
			# - Bit-1 (LSB) : timetable
			# - Bit-2 : chat
			# - Bit-3 : bulletin
			# - Bit-4 : shared links
			# - Bit-5 : shared files
			# - Bit-6 : question bank
			# - Bit-7 : Photo Album
			# - Bit-8 (MSB) : Survey
			define("FUNC_ACCESS_CHATROOM_BIT_POS", 1);
			if ($this->isAccessTool(FUNC_ACCESS_CHATROOM_BIT_POS) == 1) {
				$x .= " <!-- chat room link //-->
					<tr>
						<td align=\"right\">
							<a href=\"../chat/index.php?GroupID={$GroupID}\" class=\"contenttool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $eComm['chatroom'] ."</a>
						</td>
					</tr>";
			}
			$x .= $online_x;

			if($list_type=="all")	$x .= $offline_x;

			$x .= "
				</table>
	     	";
	     	return $x   ;
        }

        function ShareArea()
        {
	        global $image_path, $LAYOUT_SKIN, $eComm;
	        global $rightMenu, $Lang;

//			if(!$this->isAccessFiles() && !$this->isAccessPhotoAlbum() && !$this->isAccessLinks())
//				return '';
	        $x = "
			        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		          <tr>
		            <td width=\"5\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_01.gif\" width=\"5\" height=\"28\"></td>
		            <td height=\"28\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_02.gif\"><table width=\"100%\" height=\"28\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		                <tr>
		                  <td align=\"left\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		                    <tr>
		                      <td width=\"9\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_title_01.gif\" width=\"9\" height=\"28\"></td>
		                      <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_title_02.gif\"><span id=\"share_title\" class=\"ecomm_box_title\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_shares.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> ". $eComm['LatestShare'] ."</span></td>
		                      <td width=\"9\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_title_03.gif\" width=\"9\" height=\"28\"></td>
		                    </tr>
		                  </table></td>
		                  <td align=\"right\" valign=\"top\"><a href=\"javascript:maxShareArea();\" alt=\"". $Lang['eComm']['Manage'] ."\" title=\"". $Lang['eComm']['Manage'] ."\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/btn_enlarge_off.gif\" name=\"a_enlarge1\" width=\"16\" height=\"16\" border=\"0\" id=\"a_enlarge1\" onMouseOver=\"MM_swapImage('a_enlarge1','','{$image_path}/{$LAYOUT_SKIN}/ecomm/btn_enlarge_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>
		                </tr>
		              </table></td>
		            <td width=\"6\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_03.gif\" width=\"6\" height=\"28\"></td>
		            <td width=\"130\" height=\"28\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"130\" height=\"10\"></td>
		            <td width=\"13\">&nbsp;</td>
		          </tr>
		          <tr>
		            <td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_04.gif\" width=\"5\" height=\"240\"></td>
		            <td valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		              <tr>
		                <td align=\"right\">

		                <div id=\"shareviewmode_div\">". $this->ViewModeSelect($this->DefaultViewMode) ."<div>

		                </td>
		              </tr>
		              <tr>
		                <td>


		                <div id=\"share_div\" style=\"width:100%; height:220px; visibility: visible; overflow: auto; \">
						";

//		                $x .= $this->IndexViewMode($this->DefaultViewMode,0);
						$x .= "<!-- Load by ajax-->"; 

		                $x .= "
		                </div>
		                </td>
		              </tr>
		            </table>
		              </td>
		            <td width=\"6\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_06.gif\" width=\"6\" height=\"17\"></td>


		            <td width=\"130\" align=\"right\" valign=\"top\" bgcolor=\"#eff9e0\" style=\"background-image:url({$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_middle_01.gif); background-repeat:no-repeat; background-position:bottom left;\">
			            <div id=\"div_right_menu\">{$rightMenu}</div>
	    	        </td>



		            <td width=\"13\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_middle_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_top_view_02.gif\" width=\"13\" height=\"32\" id=\"rightmenuimg\"></td>
		          </tr>
		          <tr>
		            <td width=\"5\" height=\"29\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_07.gif\" width=\"5\" height=\"29\"></td>
		            <td height=\"29\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_08.gif\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		              <tr>
		                <td align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_08a.gif\" width=\"19\" height=\"29\"></td>
		                <td align=\"right\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_08b.gif\" width=\"261\" height=\"29\"></td>
		              </tr>
		            </table></td>
		            <td width=\"6\" height=\"29\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_09.gif\" width=\"6\" height=\"29\"></td>
		            <td width=\"130\" height=\"29\" align=\"left\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_bottom_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_bottom_01.gif\" width=\"124\" height=\"29\"></td>
		            <td width=\"13\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/share_board_sub_bottom_03.gif\" width=\"13\" height=\"29\"></td>
		          </tr>
		        </table>
	        ";

	        return $x;
        }

        function Forum($forum_list, $limit=5)
        {
	        global $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $GroupID, $eComm, $Lang;
	        global $eComm_Field_Topic, $eComm_Field_Createdby, $eComm_Field_LastUpdated, $eComm_Field_ReplyNo;

	        include_once($PATH_WRT_ROOT."includes/libbulletin.php");

	        $x = "
		        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" id=\"forumtable\" >
				<tr>
				  <td valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				    </tr><tr>
				      <td width=\"12\" height=\"25\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_01.gif\" width=\"12\" height=\"25\"></td>
				      <td width=\"13\" height=\"25\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_02.gif\" width=\"13\" height=\"25\"></td>
				      <td height=\"25\" align=\"right\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_02.gif\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				          <tr>
				            <td align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_forum.gif\" width=\"25\" height=\"20\" align=\"absmiddle\"><span class=\"ecomm_forum_title\">". $eComm['Forum'] ."</span></td>
				            <td align=\"right\"><a href=\"javascript:maxForum();\" alt=\"". $Lang['eComm']['Manage'] ."\" title=\"". $Lang['eComm']['Manage'] ."\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/btn_enlarge_off.gif\" name=\"a_enlarge2\" width=\"16\" height=\"16\" border=\"0\" id=\"a_enlarge2\" onMouseOver=\"MM_swapImage('a_enlarge2','','{$image_path}/{$LAYOUT_SKIN}/ecomm/btn_enlarge_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_03b.gif\" width=\"6\" height=\"25\" align=\"absmiddle\"></td>
				          </tr>
				      </table></td>
				      <td width=\"18\" height=\"25\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"18\" height=\"25\"></td>
				    </tr>
				    <tr>
				      <td width=\"12\" height=\"28\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_05.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_05.gif\" width=\"12\" height=\"28\"></td>
				      <td width=\"13\" height=\"28\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_10.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_06.gif\" width=\"13\" height=\"28\"></td>
				      <td height=\"28\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_07.gif\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">

				      </table></td>
				      <td width=\"18\" height=\"28\" valign=\"top\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_12.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_08.gif\" width=\"18\" height=\"28\"></td>
				    </tr>
				    <tr>
				      <td width=\"12\" valign=\"bottom\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_05.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_09b.gif\" width=\"12\" height=\"19\"></td>
				      <td width=\"13\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_10.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_10.gif\" width=\"13\" height=\"200\"></td>
				      <td bgcolor=\"#fffeed\" valign=\"top\" >

				      <div id=\"forum_div\" style=\"  width:100%; height:200px; visibility: visible;\">
				      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
				          <tr class=\"forumtabletop forumtabletoptext\">
				            <td width=\"18\" align=\"left\">&nbsp;</td>
				            <td align=\"left\">". $eComm_Field_Topic ."</td>
				            <td width=\"20\" align=\"left\">&nbsp;</td>
				            <td align=\"left\">". $eComm_Field_Createdby ."</td>
				            <td width=\"80\" align=\"center\" nowrap>". $eComm_Field_ReplyNo ."</td>
				            <td width=\"100\" align=\"left\">". $eComm_Field_LastUpdated ."</td>
				          </tr>
				";

				/*$lb = new libbulletin();
				$b = $lb->returnLatestTopic($GroupID, 5);*/

				for($i=0;($i<sizeof($forum_list));$i++)
				{
					if($forum_list[$i]['onTop'])
					{
					$x .="
					          <tr class=\"forumtablerow\">
					            <td align=\"left\" class=\"forumtablerow tabletext\">". $forum_list[$i][0]."</td>
					            <td width=\"50%\" align=\"left\"  class=\"forumtablerow \"><table class=\"tbclip\"><tr><td>". $forum_list[$i]['topic']."</td></tr></table></td>
					            <td align=\"left\" class=\"forumtablerow tabletext\">". $forum_list[$i][2]."</td>
					            <td align=\"left\"  class=\"forumtablerow tabletext\"> ". $forum_list[$i]['author']." </td>
					            <td align=\"center\" class=\"forumtablerow tabletext\"> ". $forum_list[$i][5]." </td>
					            <td align=\"left\" class=\"forumtablerow tabletext\">". $forum_list[$i]['postdate']." </td>
					          </tr>
		          	";
		          	}
	          	}
	          	for($i=0;($i<sizeof($forum_list));$i++)
				{
					if(!$forum_list[$i]['onTop'])
					{
					$x .="
					          <tr class=\"forumtablerow\">
					            <td align=\"left\" class=\"forumtablerow tabletext\">". $forum_list[$i][0]."</td>
					            <td width=\"50%\" align=\"left\"  class=\"forumtablerow\"><table class=\"tbclip\"><tr><td>". $forum_list[$i]['topic']."</td></tr></table></td>
					            <td align=\"left\" class=\"forumtablerow tabletext\">". $forum_list[$i][2]."</td>
					            <td align=\"left\"  class=\"forumtablerow tabletext\"> ". $forum_list[$i]['author']." </td>
					            <td align=\"center\" class=\"forumtablerow tabletext\"> ". $forum_list[$i][5]." </td>
					            <td align=\"left\" class=\"forumtablerow tabletext\">". $forum_list[$i]['postdate']." </td>
					          </tr>
		          	";
		          	}
	          	}
	          	$x .= "
				      </table>
				      </div>
				      </td>
				      <td width=\"18\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_12.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_12.gif\" width=\"18\" height=\"19\"></td>
				    </tr>
				    <tr>
				      <td width=\"12\" height=\"31\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"12\" height=\"31\"></td>
				      <td width=\"13\" height=\"31\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_14.gif\" width=\"13\" height=\"31\"></td>
				      <td height=\"31\" align=\"right\" background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_15.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_15b.gif\" width=\"170\" height=\"31\"></td>
				      <td width=\"18\" height=\"31\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/forum_board_16.gif\" width=\"18\" height=\"31\"></td>
				    </tr>
				  </table></td>
				</tr>
				</table>
	        ";

	        return $x;
        }

        function IndexViewMode($type, $start=0, $filetype='')
        {
	     	   switch ($type)
	     	   {
		     		default:
					case 0:		//list
		     			$x = $this->IndexViewMode0($filetype);
		     			break;
					case 1:		//1
		     			$x = $this->IndexViewMode1($filetype, $start);
		     			break;
	     			case 2:		//2
		     			$x = $this->IndexViewMode2($filetype, $start);
		     			break;
	     			case 4:		//4
		     			$x = $this->IndexViewMode4($filetype, $start);
		     			break;

	     	   }

	     	   return $x;
        }

        function IndexViewMode0($filetype)
        {
	        global $image_path, $LAYOUT_SKIN, $eComm, $PATH_WRT_ROOT;
	        global $eComm_Field_LastUpdated, $i_no_record_exists_msg, $Lang;

	        include_once($PATH_WRT_ROOT."includes/libfile.php");
	        $r = $this->getLatestRecords($filetype);
	        $fileIDArr = Get_Array_By_Key($r,"FileID");
	        $lf 			= new libfile(0,$fileIDArr);
	        
	        $rr = array();
	        //re-order the list (check private and hidden)
	        for($i=0;$i<sizeof($r);$i++)
	        {
				$lf->LoadFileData($r[$i]['FileID']);
				if(!$lf->viewFileRight())	continue;
				array_push($rr, $r[$i]);
	        }
			if($filetype=="WA")
			{
	        	$x .= $Lang['eComm']['Waiting_Reject_Remark']; 
        	}
            $x .="
		    		<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" class=\"tbclip\">
	                  <tr class=\"albumtabletop albumtabletoptext\">
	                    <td width=\"40\">&nbsp;</td>
	                    <td width=\"22%\">". $eComm['Share'] ."</td>
	        ";
            if($filetype=="WA")
            {
                $x.= "
		                    <td width=\"20%\" nowrap >". $eComm['ApprovalStatus'] ."</td>
		         ";
            }
            else
            {
                $x .= "
		       			<td width=\"10%\">". $eComm['By'] ."</td>
	                    <td align=\"center\" width=\"10%\">". $eComm['Comment'] ."</td>
		         ";
            }
	         $x .= "
	                    <td width=\"10%\">". $eComm_Field_LastUpdated ."</td>
	                  </tr>
			";

			if(sizeof($rr)==0)
			{
				$x .= "
					<tr class=\"forumtablerow\">
	                    <td class=\"forumtablerow tabletext\" colspan=\"5\" align=\"center\" height=\"150\" valign=\"middle\">$i_no_record_exists_msg</td>
	                  </tr>
				";
			}

			for($i=0;$i<sizeof($rr);$i++)
			{
//				$lf 			= new libfile($rr[$i]['FileID']);
				$lf->LoadFileData($rr[$i]['FileID']);
				
				$title			= $rr[$i]['UserTitle'] ? $rr[$i]['UserTitle'] : $rr[$i]['Title'];
				
				// 	                    <td  class=\"forumtablerow tabletext\"> ". $rr[$i]['UserName']." </td>
				//<td align=\"center\" class=\"forumtablerow tabletext\"> ". $lf->getCommentNumber() ." </td>
				$x .= "
	                  <tr class=\"forumtablerow\">
	                    <td class=\"forumtablerow tabletext\" nowrap>". $lf->FileTypeIcon() . $lf->StatusImg() ."</td>
	                    <td  class=\"forumtablerow\"><a href=\"javascript:viewfile(". $rr[$i]['FileID'] .");\" class=\"tablelink\">". $title."</a></td>
	                    ";
	            if($filetype=="WA")
	            {
		            $x .="
		                    <td  class=\"forumtablerow tabletext\"> ". ($rr[$i]['Approved']==0 ? $eComm['WaitingApproval'] : $eComm['Rejected']) ." </td>
		                 ";
            	}
            	else
            	{
	            	$x .= "
	            			<td  class=\"forumtablerow tabletext\"> ". $rr[$i]['UserName']." </td>
	                    	<td align=\"center\" class=\"forumtablerow tabletext\"> ". $lf->getCommentNumber() ." </td>
							";
            	}
            
	            $x .= "
	                    <td class=\"forumtablerow tabletext\">". substr($rr[$i]['DateModified'],0,10) ." </td>
	                  </tr>
				";
				
			}

            $x .= " </table> ";

	    	return $x;
        }

        function IndexViewMode1($filetype='', $start=0)
        {
	     	global $image_path, $LAYOUT_SKIN, $eComm, $PATH_WRT_ROOT;
	        global $eComm_Field_LastUpdated, $i_no_record_exists_msg, $Lang;

	        include_once($PATH_WRT_ROOT."includes/libfile.php");
	        include_once($PATH_WRT_ROOT."includes/libfolder.php");

	        $r = $this->getLatestRecords($filetype);
	        $fileIDArr = Get_Array_By_Key($r,"FileID");
	        $lf = new libfile(0,$fileIDArr);
	        
	        $rr = array();
	        //re-order the list (check private and hidden)
	        for($i=0;$i<sizeof($r);$i++)
	        {
				$lf->LoadFileData($r[$i]['FileID']);
				if(!$lf->viewFileRight())	continue;
				array_push($rr, $r[$i]);
	        }

	        if(sizeof($rr)==0)
			{
				$x .= "
					<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr class=\"forumtablerow\">
	                    <td class=\"tabletext\" colspan=\"5\" align=\"center\" height=\"150\" valign=\"middle\">$i_no_record_exists_msg</td>
	                  </tr>
	                  </table>
				";
			}
			else
			{
		        $pre = $start-1;
		        $next = (($start+1) >= sizeof($rr)) ? "" : $start+1;

		        $row = $rr[$start];
//		        $lf 			= new libfile($row['FileID']);
// 				$lf->LoadFileData($rr[$i]['FileID']);
		        $lfo 			= new libfolder($lf->FolderID);
		        $title			= $row['UserTitle'] ? $row['UserTitle'] : $row['Title'];
		        
		        switch($lfo->FolderType)
				{
					case "P":
					case "V":
						$img = "album";
						$inner_color = "#6fb4ba";
						break;
					case "W":
						$img = "category";
						break;
					case "F":
					default:
						$img = "folder";
						$inner_color = "#ead76d";
						break;
				}

				if($filetype=="WA")
				{
		        	$x .= $Lang['eComm']['Waiting_Reject_Remark']; 
	        	}
        	
		        $x .= "
			        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		                  <tr>
		                    <td width=\"20\">
	            ";
	            if($start)
	            	$x .= "<div id=\"btn_large\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 1, $pre, document.shareform.FileType.value);\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_prev.gif\" border=\"0\"></span></a></div>";
	            $x .= "</td>
		                    <td align=\"center\" valign=\"top\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
		                      <tr>
		                        <td width=\"260\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		                          <tr>
		                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_01.gif\" width=\"16\" height=\"16\"></td>
		                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02a.gif\" width=\"111\" height=\"16\"></td>
		                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_03.gif\" width=\"22\" height=\"16\"></td>
		                          </tr>
		                          <tr>
		                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\" width=\"16\" height=\"13\"></td>
		                            <td bgcolor=\"{$inner_color}\" align=\"center\"><div id=\"photo_border\"><a href=\"javascript:viewfile(". $row['FileID'] .");\"><span>". $this->DisplayPhoto($row['FileID'], 0,165) ."</span></a></div></td>
		                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06a.gif\" width=\"22\" height=\"110\"></td>
		                          </tr>
		                          <tr>
		                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_07.gif\" width=\"16\" height=\"9\"></td>
		                            <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\" width=\"16\" height=\"9\"></td>
		                            <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_09.gif\" width=\"22\" height=\"9\"></td>
		                          </tr>
		                        </table></td>
		                        <td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class=\"tbclip\">
		                          <tr>
		                            <td align=\"left\" class=\"share_file_title\">". $lf->FileTypeIcon() ." <a href=\"javascript:viewfolder(". $row['FolderID'] .");\">". $lfo->Title ."</a> (". $lfo->countFile() ."): </td>
		                          </tr>
		                          <tr>
		                            <td align=\"left\" class=\"share_file_title\"><a href=\"javascript:viewfile(". $row['FileID'] .");\"><strong>". $title ." </strong></a>". $lf->StatusImg() ."</td>
		                          </tr>
		                          <tr>
		                            <td height=\"2\" align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
		                          </tr>
		                          ";
		                          
		         if($filetype=="WA")
                  {
	                  $x .= "       
	                  			<tr>
		                            <td align=\"left\" class=\"tabletext\">". $eComm['ApprovalStatus'] .": </td>
		                          </tr>
		                          <tr>
		                            <td align=\"left\" class=\"tabletext\">". ($rr[$i]['Approved']==-1 ? $eComm['Rejected'] : $eComm['WaitingApproval'] ) ."</td>
		                          </tr>
                                ";
                  }
                  else
                  {
		    		    $x .= "
		                          <tr>
		                            <td align=\"left\" class=\"tabletext\">". $eComm['CreatedBy'] .": </td>
		                          </tr>
		                          <tr>
		                            <td align=\"left\" class=\"tabletext\"><strong>". $row['UserName'] ." </strong></td>
		                          </tr>
		                          <tr>
		                            <td height=\"2\" align=\"left\" class=\"tabletext\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
		                          </tr>
		                          <tr>
		                            <td align=\"left\" class=\"tabletext\">". $eComm_Field_LastUpdated .":</td>
		                          </tr>
		                          <tr>
		                            <td align=\"left\" class=\"tabletext\">". substr($r[$i]['DateModified'], 0,10) ."</td>
		                          </tr>
		                          <tr>
		                            <td height=\"2\" align=\"left\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
		                          </tr>
		                          <tr>
		                            <td align=\"left\" class=\"tablebluelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $lf->getCommentNumber()." ". $eComm['Comment'] ." </td>
		                          </tr>
		                  ";
	                  }
		              $x .= "         </table></td>
		                      </tr>
		                    </table></td>
		                    <td width=\"20\">
	            ";
	            if($next)
	            	$x .= "<div id=\"btn_large\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 1, $next, document.shareform.FileType.value);\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_next.gif\" border=\"0\"></span></a></div>";
	            $x .= "</td>
		                  </tr>
		                </table>
		        ";
        	}

	        return $x;
        }
        function IndexViewMode2($filetype='', $start=0)
        {
	        global $image_path, $LAYOUT_SKIN, $eComm, $PATH_WRT_ROOT;
	        global $eComm_Field_LastUpdated, $i_no_record_exists_msg, $Lang;

	        include_once($PATH_WRT_ROOT."includes/libfile.php");
	        include_once($PATH_WRT_ROOT."includes/libfolder.php");

	        $r = $this->getLatestRecords($filetype);
	        $fileIDArr = Get_Array_By_Key($r,"FileID");
	        $lf 			= new libfile(0,$fileIDArr);
	        
	        $rr = array();
	        //re-order the list (check private and hidden)
	        for($i=0;$i<sizeof($r);$i++)
	        {
				$lf->LoadFileData($r[$i]['FileID']);
				if(!$lf->viewFileRight())	continue;
				array_push($rr, $r[$i]);
	        }


	        $pre = $start-2;
	        $next = (($start+2) >= sizeof($rr)) ? "" : $start+2;

	        if($filetype=="WA")
			{
	        	$x .= $Lang['eComm']['Waiting_Reject_Remark']; 
        	}
        	
	     	$x .="
		     	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                  <tr>
	                    <td width=\"20\">
            ";
	          if($start)
            	$x .= "<div id=\"btn_large\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 2, $pre, document.shareform.FileType.value);\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_prev.gif\" border=\"0\"></span></a></div>";

	          $x .= "</td>
	                    <td align=\"center\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	                        <tr>
	        ";

	        if(sizeof($rr)==0)
			{
				$x .= "
					<tr class=\"forumtablerow\">
	                    <td class=\"tabletext\" colspan=\"5\" align=\"center\" height=\"150\" valign=\"middle\">$i_no_record_exists_msg</td>
	                  </tr>
				";
			}

	        $tr=0;

			for($i=$start;($i<sizeof($rr) and $tr<2);$i++)
	        {
	        	
//		        $lf 	= new libfile($rr[$i]['FileID']);
				$lf->LoadFileData($rr[$i]['FileID']);
		        $lfo 	= new libfolder($lf->FolderID);
		        $title	= $rr[$i]['UserTitle'] ? $rr[$i]['UserTitle'] : $rr[$i]['Title'];

		        //check private
				if(!$lf->viewFileRight())	continue;

		        switch($lfo->FolderType)
				{
					case "P":
					case "V":
						$img = "album";
						$inner_color = "#6fb4ba";
						break;
					case "W":
						$img = "category";
						break;
					case "F":
					default:
						$img = "folder";
						$inner_color = "#ead76d";
						break;
				}

		        $tr++;
		        $x .= "
	                        <td width=\"50%\" align=\"left\" valign=\"top\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                            <tr>
	                              <td align=\"center\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                                  <tr>
	                                    <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_01.gif\" width=\"16\" height=\"16\"></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02a.gif\" width=\"111\" height=\"16\"></td>
	                                    <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_03.gif\" width=\"22\" height=\"16\"></td>
	                                  </tr>
	                                  <tr>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\" width=\"16\" height=\"13\"></td>
	                                    <td bgcolor=\"{$inner_color}\" align=\"center\"><div id=\"photo_border\"><a href=\"javascript:viewfile(". $rr[$i]['FileID'] .");\"><span>". $this->DisplayPhoto($rr[$i]['FileID'], 0, 100) ."</span></a></div></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06a.gif\" width=\"22\" height=\"110\"></td>
	                                  </tr>
	                                  <tr>
	                                    <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_07.gif\" width=\"16\" height=\"9\"></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\" width=\"16\" height=\"9\"></td>
	                                    <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_09.gif\" width=\"22\" height=\"9\"></td>
	                                  </tr>
	                              </table></td>
	                              </tr>
	                            <tr>
	                              <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"tbclip\">
	                                <tr>
	                                  <td align=\"left\" class=\"share_file_title\"> <a href=\"javascript:viewfolder(". $rr[$i]['FolderID'] .");\">". $lfo->Title ."</a> (". $lfo->countFile() ."): <a href=\"javascript:viewfile(". $rr[$i]['FileID'] .");\"><strong> ". $title." </strong></a>". $lf->StatusImg() ."</td>
	                                </tr>
	                                ";
						if($filetype=="WA")
						{
		                  	$x .= "       <tr>
	                                  <td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['ApprovalStatus'] .":"  . ($rr[$i]['Approved']==-1 ? $eComm['Rejected'] : $eComm['WaitingApproval'] ) ." </span></td>
	                                </tr>
	                                ";
						}
						else
						{
							$x .= "
	                                <tr>
	                                  <td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['By'] .": <strong>". $rr[$i]['UserName']." </strong>(". substr($rr[$i]['DateModified'], 0,10) .")</span></td>
	                                </tr>
	                                 <tr>
	                                  <td align=\"left\" class=\"tablebluelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $lf->getCommentNumber() ." ". $eComm['Comment']." </td>
	                                </tr>
	                            ";
                        }
	                    $x .= "
	                              </table></td>
	                              </tr>
	                          </table></td>
				";
				
              }


	           $x .= "
	                        </tr>
	                      </table></td>
	                    <td width=\"20\">
                ";
                if($next)
                	$x .= "<div id=\"btn_large\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 2, $next, document.shareform.FileType.value);\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_next.gif\" border=\"0\"></span></a></div>";
                $x .= "</td>
	                  </tr>
	                </table>

	     	";
	     	return $x;
        }

        function IndexViewMode4($filetype='', $start=0)
        {
	        global $image_path, $LAYOUT_SKIN, $eComm, $PATH_WRT_ROOT;
	        global $eComm_Field_LastUpdated, $i_no_record_exists_msg, $Lang;

	        include_once($PATH_WRT_ROOT."includes/libfile.php");
	        include_once($PATH_WRT_ROOT."includes/libfolder.php");

   	        $r = $this->getLatestRecords($filetype);
	        $fileIDArr = Get_Array_By_Key($r,"FileID");
	        $lf 			= new libfile(0,$fileIDArr);
	        
	        $rr = array();
	        //re-order the list (check private and hidden)
	        for($i=0;$i<sizeof($r);$i++)
	        {
				$lf->LoadFileData($r[$i]['FileID']);
				if(!$lf->viewFileRight())	continue;
				array_push($rr, $r[$i]);
	        }
	        $pre = $start-4;
	        $next = (($start+4) >= sizeof($rr)) ? "" : $start+4;
	        
	        if($filetype=="WA")
			{
	        	$x .= $Lang['eComm']['Waiting_Reject_Remark']; 
        	}
        	
		    $x .= "
		     		<table width=\"100%\" height=\"220\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                  <tr>
	                    <td width=\"20\">
            ";
            if($start)
            	$x .= "<div id=\"btn_large\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 4, $pre, document.shareform.FileType.value);\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_prev.gif\" border=\"0\"></span></a></div>";
	        $x .= "
	        			</td>
	                    <td align=\"center\" valign=\"top\">
	                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
	        ";

	        $tr=0;

	        if(sizeof($rr)==0)
			{
				$x .= "
					<tr class=\"forumtablerow\">
	                    <td class=\"tabletext\" colspan=\"5\" align=\"center\" height=\"150\" valign=\"middle\">$i_no_record_exists_msg</td>
	                  </tr>
				";
			}


			for($i=$start;($i<sizeof($rr) and $tr<4);$i++)
	        {
//		        $lf 	= new libfile($rr[$i]['FileID']);
				$lf->LoadFileData($rr[$i]['FileID']);
		        $lfo 	= new libfolder($lf->FolderID);
		        $title			= $rr[$i]['UserTitle'] ? $rr[$i]['UserTitle'] : $rr[$i]['Title'];

		        //check private
				if(!$lf->viewFileRight())	continue;

		        switch($lfo->FolderType)
				{
					case "P":
					case "V":
						$img = "album";
						$inner_color = "#6fb4ba";
						break;
					case "W":
						$img = "category";
						break;
					case "F":
					default:
						$img = "folder";
						$inner_color = "#ead76d";
						break;
				}

		        $tr++;
	     		$x .= ($tr%2) ? "<tr>":"";
		        $x .= "
	                        <td width=\"50%\" align=\"left\" valign=\"top\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                            <tr>
	                              <td align=\"center\" valign=\"top\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	                                  <tr>
	                                    <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_01.gif\" width=\"16\" height=\"16\"></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_02a.gif\" width=\"80\" height=\"16\"></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_03.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"12\" height=\"16\"></td>
	                                  </tr>
	                                  <tr>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_04.gif\" width=\"16\" height=\"13\"></td>
	                                    <td bgcolor=\"{$inner_color}\" align=\"center\"><div id=\"photo_border\"><a href=\"javascript:viewfile(". $rr[$i]['FileID'] .");\"><span>". $this->DisplayPhoto($rr[$i]['FileID'], 0, 60) ."</span></a></div></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"12\" height=\"16\"></td>
	                                  </tr>
	                                  <tr>
	                                    <td><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_07.gif\" width=\"16\" height=\"9\"></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_08.gif\" width=\"16\" height=\"9\"></td>
	                                    <td background=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/{$img}/{$img}_1_09.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"12\" height=\"9\"></td>
	                                  </tr>
	                              </table></td>
	                              <td align=\"center\" valign=\"top\"><br><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"tbclip\">

	                                <tr>
	                                  <td align=\"left\" class=\"share_file_title\"><a href=\"javascript:viewfile(". $rr[$i]['FileID'] .");\"><strong> ". $title ." </strong></a>". $lf->StatusImg() ."</td>
	                                </tr>
	                                ";
	                                
	                  if($filetype=="WA")
	                  {
		                  $x .= "       <tr>
	                                  <td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['ApprovalStatus'] .":"  . ($rr[$i]['Approved']==-1 ? $eComm['Rejected'] : $eComm['WaitingApproval'] ) ." </span></td>
	                                </tr>
	                                ";
	                  }
	                  else
	                  {              
	                 	 $x .= "       <tr>
	                                  <td align=\"left\" class=\"share_file_title\"><span class=\"tabletext\">". $eComm['By'] ." <strong>". $rr[$i]['UserName'] ."</strong></span></td>
	                                </tr>
	                                <tr>
	                                  <td align=\"left\"><span class=\"tabletext\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"> ". $lf->getCommentNumber() ." </span></td>
	                                </tr>
	                                ";
                      }
						$x .= "
	                              </table></td>
	                            </tr>
	                          </table></td>
					";
					$x .= ($tr%2) ? "":"</tr>";
				}
	           $x .= "
	           			</table>

	                      </td>
	                    <td width=\"20\">";

				if($next)
					$x .= "<div id=\"btn_large\"><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 4, $next, document.shareform.FileType.value);\"><span><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_next.gif\" border=\"0\"></span></a></div>";

				$x .= "
						</td>
	                  </tr>
	                </table>
	     	";

	     	return $x;
        }

        function getLatestRecords($type='', $limit=10, $Approved=1)
        {
	        global $GroupID, $UserID, $PATH_WRT_ROOT;

	        ### find out the latest record#
	        include_once($PATH_WRT_ROOT."includes/libgroup.php");
			$lg = new libgroup($GroupID);
	        $limit_t = $lg->SharingLatestNo;
	        $limit = $limit_t ? $limit_t : $limit;
			
			$exclude_FileType = array();
			if(!$lg->isAccessPhoto())
				$exclude_FileType[] = "P";
			if(!$lg->isAccessVideo())
				$exclude_FileType[] = "V";
			if(!$lg->isAccessLinks())
				$exclude_FileType[] = "W";
			if(!$lg->isAccessFiles())
				$exclude_FileType[] = "F";
			
			$exclude_FileTypeSql = "'".implode("','",$exclude_FileType)."'";	
			if(count($exclude_FileType)>0)
				$cond .= " AND a.FileType NOT IN ($exclude_FileTypeSql)";	
				
			$sql = "select ";
			$sql .= "a.FileID, a.GroupID, a.UserID, a.UserName, a.Title, a.Location, a.DateInput, a.DateModified, a.FileType, a.PublicStatus, a.FolderID, a.CoverImg, a.UserTitle, a.Approved ";
			$sql .= "from INTRANET_FILE a ";
			$sql .= "where (a.FolderID != '' or a.FolderID is not null)";
			$sql .= "and a.GroupID=$GroupID ";
			if($type=='WA')
			{
				$sql .= "and a.Approved != 1 ";
				$sql .= "and a.UserID=$UserID ";
			}
			else
			{
				$sql .= "and a.Approved=$Approved ";
				if($type) $sql .= "and a.FileType='$type' ";
			}
			$sql .=	$cond;
			$sql .= "order by a.DateInput Desc limit $limit";

			return $this->returnArray($sql);
        }

        function ViewModeSelect($t)
        {
	        global $image_path, $LAYOUT_SKIN, $DisplayType;

			$x = "
				<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
                  <tr>
                    <td><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 0, 0, document.shareform.FileType.value);\" onMouseOver=\"MM_swapImage('view_list','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_list". ($t==0?"_on":"") .".gif\" name=\"view_list\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list\"></a></td>
                    <td><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 4, 0, document.shareform.FileType.value);\" onMouseOver=\"MM_swapImage('view_4','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_4". ($t==4?"_on":"") .".gif\" name=\"view_4\" width=\"24\" height=\"20\" border=\"0\" id=\"view_4\"></a></td>
                    <td><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 2, 0, document.shareform.FileType.value);\" onMouseOver=\"MM_swapImage('view_2','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_2_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_2". ($t==2?"_on":"") .".gif\" name=\"view_2\" width=\"24\" height=\"20\" border=\"0\" id=\"view_2\"></a></td>
                    <td><a href=\"javascript:jAJAX_SHARE_VIEW_MODE(document.shareform, 1, 0, document.shareform.FileType.value);\" onMouseOver=\"MM_swapImage('view_1','','{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_1_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/ecomm/icon_view_1". ($t==1?"_on":"") .".gif\" name=\"view_1\" width=\"24\" height=\"20\" border=\"0\" id=\"view_1\"></a></td>
                  </tr>
                </table>
			";
			return $x;
        }

        function hasAnyGroupAdminRight()
        {
	        global $UserID;
	        
	        if (!(isset($this->AnyAdminAccessRightArr[$UserID]) && sizeof($this->AnyAdminAccessRightArr[$UserID])>0))
	        {
		        $groups = $this->getGroupsID();
	
		        $has = array();
		         for($i=0;$i<sizeof($groups);$i++)
		        {
					$groupsIDARR[] = $groups[$i]['GroupID'];
		        }
		        if (sizeof($groupsIDARR)>0 && sizeof($groups)>0)
		        {
			        $groupsIDs = implode(",", $groupsIDARR);
//			        $n = new libgroup("", $groupsIDs);
		        }
		        
		        $oriGroupID = $this->GroupID;
		        for($i=0;$i<sizeof($groups);$i++)
		        {
			        $this_GroupID = $groups[$i]['GroupID'];
			    	$this->LoadGroupData($this_GroupID);
	
					if ($this->isGroupAdmin($UserID)) array_push($has, $this_GroupID);
					if ($this->hasAllAdminRights($UserID)) array_push($has, $this_GroupID);
	
					# 0 - Basic Info *
					# 1 - Member List *
					# 2 - Internal Announcement *
					# 3 - All Announcement *
					# 4 - Internal Event
					# 5 - All Event
					# 6 - Timetable
					# 7 - Bulletin *
					# 8 - Links *
					# 9 - Files *
					# 10- QB
					# 11- Internal Survey
					# 12- All Survey
					# 13- Photo Album
					$check_groups = array(0, 1, 2, 3, 7, 8, 9);
	
					for($c=0;$c<sizeof($check_groups);$c++)
					{
						$s = substr($n->AdminAccessRight,$check_groups[$c],1);
						if($s == "1") array_push($has, $this_GroupID);
					}
		        }
		        
		        $this->AnyAdminAccessRightArr[$UserID] = array_unique($has);
        	}

			$this->LoadGroupData($oriGroupID);
			
	        return $this->AnyAdminAccessRightArr[$UserID];

        }

         function targetUserHasAnyGroupAdminRight($targetUserID)
        {
	        $groups = $this->getTargetGroupsID($targetUserID);
	        $has = array();

	        for($i=0;$i<sizeof($groups);$i++)
	        {
		        $this_GroupID = $groups[$i]['GroupID'];
		    	$n = new libgroup($this_GroupID);

				if ($n->isGroupAdmin($targetUserID)) array_push($has, $this_GroupID);
				if ($n->hasAllAdminRights($targetUserID)) array_push($has, $this_GroupID);

				# 0 - Basic Info *
				# 1 - Member List *
				# 2 - Internal Announcement *
				# 3 - All Announcement *
				# 4 - Internal Event
				# 5 - All Event
				# 6 - Timetable
				# 7 - Bulletin *
				# 8 - Links *
				# 9 - Files *
				# 10- QB
				# 11- Internal Survey
				# 12- All Survey
				# 13- Photo Album
				$check_groups = array(0, 1, 2, 3, 7, 8, 9);

				for($c=0;$c<sizeof($check_groups);$c++)
				{
					$s = substr($n->AdminAccessRight,$check_groups[$c],1);
					if($s == "1") array_push($has, $this_GroupID);
				}
	        }

	        return array_unique($has);

        }

        function updateOnlineStatus()
        {
	        global $UserID, $intranet_root;

	        include_once("libfilesystem.php");
			    $lf = new libfilesystem();

		        $logFile = $this->GroupID . ".txt";
		        //$logFileFullPath = $PATH_WRT_ROOT."home/eCommunity/onlineLog/".$logFile;

		        # check folder exists
		        $logFileFolder = "$intranet_root/file/eCommunity";
		        //$lf->folder_remove_recursive($logFileFolder);
		        if(!file_exists($logFileFolder))
		        {
			     	include_once("libfilesystem.php");
			     	$lf = new libfilesystem();
			     	$t = $lf->folder_new($logFileFolder);
			     	$lf->chmod_R($logFileFolder, 0777);
		        }

		        $logFileFolder .= "/onlineLog";

		        if(!file_exists($logFileFolder))
		        {
			     	$lf->folder_new($logFileFolder);
			     	$lf->chmod_R($logFileFolder, 0777);
		        }

		        $logFileFullPath = $logFileFolder."/".$logFile;

		        $str = $UserID . "," . time()."\n";

		        if(file_exists($logFileFullPath))				## check log file exists
		        {
			     	$logfile = fopen($logFileFullPath, "r");
			     	if ($logfile)
			     	{
					    while (!feof($logfile))
					    {
					        $buffer = fgets($logfile, 4096);
					        list($id, $time) = explode(",",$buffer);
					        if($id=="" or $id==$UserID)	continue;
					        $str .= $buffer;
					    }
					}
					$logfile = fopen($logFileFullPath, "w+");
				}
		        else
		        {
			        ## create log file
		        	$logfile = fopen($logFileFullPath, "w+");
	        	}

	        	## insert record
				fwrite($logfile, $str);
		        fclose($logfile);
        }

        function getOnlineUserID()
        {
			global $PATH_WRT_ROOT;

			$latestSec = 5*60;
			$cur_time = time();
			$online_str = "";
			$logFile = $this->GroupID . ".txt";
	        $logFileFullPath = $PATH_WRT_ROOT."home/eCommunity/onlineLog/".$logFile;

	        if(file_exists($logFileFullPath))
	        {
		        $logfile = fopen($logFileFullPath, "r");
		     	if ($logfile)
		     	{
				    while (!feof($logfile))
				    {
				        $buffer = fgets($logfile, 4096);
				        list($id, $time) = explode(",",$buffer);
				        if($id=="")	continue;

				        // check time
				        if($cur_time-$time > $latestSec)	continue;

				        $online_str .= $id .",";
				    }
				}
				fclose($logfile);
			}

			return substr($online_str, 0, -1);

        }


        function CalEventDisplay($ts)
        {
	        global $i_EventDate, $i_EventDescription, $iCalendar_NewEvent_Location, $iCalendar_NewEvent_Link;

	        $result = $this->getCalEvent($ts);

	        $css ="indexnewslistgrouptitle";

                for($i=0;$i<sizeof($result);$i++)
                {
	                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='10'>";
	                $x .= "<tr>";
	                $x .= "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	                $x .= "<tr>";
	                $x .= "<td class='indexpopsubtitle $css'>". $result[$i]['Title'] ."</td>";
	                $x .= "</tr>";
	                $x .= "</table></td></tr>";
	                $x .= "<tr>";
	                $x .= "<td><table width='100%' border='0' cellspacing='4' cellpadding='3'>";

	                $x .= "<tr>";
	                $x .= "<td width='30%' valign='top' nowrap class='tabletext'>$i_EventDate</td>";
	                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". $result[$i]['EventDate'] ."</td>";
	                $x .= "</tr>";

	                if(trim($result[$i]['Location']))
	                {
		                $x .= "<tr>";
		                $x .= "<td width='30%' valign='top' nowrap class='tabletext'>$iCalendar_NewEvent_Location</td>";
		                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". $result[$i]['Location'] ."</td>";
		                $x .= "</tr>";
					}

					if(trim($result[$i]['Url']))
	                {
		                $x .= "<tr>";
		                $x .= "<td width='30%' valign='top' nowrap class='tabletext'>$iCalendar_NewEvent_Link</td>";
		                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>". $result[$i]['Url'] ."</td>";
		                $x .= "</tr>";
	                }


	                if(trim($result[$i]['Description']))
	                {
						$x .= "<tr>";
		                $x .= "<td valign='top' nowrap class='tabletext'>$i_EventDescription</td>";
		                $x .= "<td bgcolor='#FFFFFF' class='tabletext'>".  nl2br($result[$i]['Description']) ."</td>";
		                $x .= "</tr>";
	                }

	                $x .= "</table></td>";
	                $x .= "</tr>";
	                $x .= "</table>";
                }

                return $x;
        }

        function getCalEvent($ts)
        {
			$CalID = $this->CalID;

			$sql = "select Title, date_format(EventDate,'%Y-%m-%d') as EventDate, Description, Location, Url from CALENDAR_EVENT_ENTRY where CalID = $CalID and UNIX_TIMESTAMP(EventDate)=$ts ";
			return $this->returnArray($sql,5);
        }

        function getCalAdminUser()
		{
			$CalID = $this->CalID;

			$sql = "select UserID from CALENDAR_CALENDAR_VIEWER where CalID = $CalID and Access='A'";
			$temp = $this->returnVector($sql);

			return implode(",",$temp);
		}

		######################################
		## Added by Thomas on 2010-08-04
		## - Function to resize uploaded photo
        ######################################
        
		function resize_photo($srcFile, $dest, $max_width, $max_height)
		{			
			list($width, $height) = getimagesize($srcFile);
			
			$imgRatio = $width / $height;
			$maxRatio = $max_width / $max_height;
		
			if($imgRatio < $maxRatio)
			{
				$new_height = $max_height;
				$new_width  = $new_height * $imgRatio;
			}
			else if($imgRatio > $maxRatio)
			{
				$new_width  = $max_width;
				$new_height = $new_width / $imgRatio;
			}
			else
			{
				$new_height = $max_height;
				$new_width  = $max_width;
			}
			
			$tmp_img = imagecreatetruecolor($new_width, $new_height);
		
			$ext = strtolower(get_file_ext($srcFile));
			$ext = substr($ext, 1, strlen($ext)-1);

			switch ($ext)
			{
			case 'gif':
				$src_img = imagecreatefromgif($srcFile);
				imagecopyresampled($tmp_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagegif($tmp_img, $dest);
				break;
			case 'png':
				$src_img = imagecreatefrompng($srcFile);
				imagecopyresampled($tmp_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagepng($tmp_img, $dest);
				break;
			case 'jpg':
				$src_img = imagecreatefromjpeg($srcFile);
				imagecopyresampled($tmp_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($tmp_img, $dest, 100);
				break;
			case 'jpe':
				$src_img = imagecreatefromjpeg($srcFile);
				imagecopyresampled($tmp_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($tmp_img, $dest, 100);
				break;
			case 'jpeg':
				$src_img = imagecreatefromjpeg($srcFile);
				imagecopyresampled($tmp_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($tmp_img, $dest, 100);
				break;
			default:
				break;
			}
			imagedestroy($tmp_img);
		}
		
		function CreateTmpFolder($GroupID='', $FolderType='P', $tmpUserName='')
		{
			global $UserID;
			
			if($GroupID)
			{
				if($FolderType=="P" or $FolderType=="V")
					$Title = "Default Album";
				if($FolderType=="W")
					$Title = "Default Category";
				if($FolderType=="F")
					$Title = "Default Folder";
				
				$PublicStatus = 1;
				$onTop = 0;
				$PhotoResize = 0;
				$DemensionX = "NULL";
				$DemensionY = "NULL";
				
				$sql = "
					INSERT INTO INTRANET_FILE_FOLDER 
					(GroupID, FolderType, UserID, UserName, Title, PublicStatus, DateInput, onTop, PhotoResize, DemensionX, DemensionY) 
					VALUES
					('$GroupID', '$FolderType','$UserID','$tmpUserName','$Title','$PublicStatus', now(), $onTop, $PhotoResize, $DemensionX, $DemensionY)
				";
				$this->db_db_query($sql);	
			}
		}
		
		function returnEmailNotificationData($date, $title, $type='', $groups='')
        {
	        global $Lang;

	        $website = get_website();
	        $webmaster = get_webmaster();
	        
	        $email_subject = $Lang['EmailNotification']['SchoolNews']['Subject'];
	        $email_subject = str_replace("__TYPE__", $type, $email_subject);
	        $email_subject = str_replace("__DATE__", $date, $email_subject);
	        $email_subject = str_replace("__TITLE__", $title, $email_subject);
	        
	        if(empty($groups))
	        {
		        $email_content = $Lang['EmailNotification']['SchoolNews']['Content']['SCHOOL'];
	        }
	        else
	        {
	        	$email_content = $Lang['EmailNotification']['SchoolNews']['Content']['GROUP'];
	        	$list = implode(",",$groups);
	        	$email_content = str_replace("__LIST__", $list, $email_content);
        	}
        	
	        $email_content = str_replace("__TITLE__", $title, $email_content);
	        $email_content = str_replace("__DATE__", $date, $email_content);
	        $email_content = str_replace("__WEBSITE__", $website, $email_content);
	        
	        $email_footer = $Lang['EmailNotification']['Footer'];
			$email_footer = str_replace("__WEBSITE__", $website, $email_footer);
			$email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
			
	        $email_content .= $email_footer;
			return array($email_subject, $email_content);
        }
        
        function UpdateLastAccess($GroupID)
        {
	        if($GroupID)
	        {
		        global $UserID;
		        
		     	include_once("libuserpersonalsettings.php");
				$lps = new libuserpersonalsettings();
				
				$SettingList['eComm_LastAccessGroupID'] = $GroupID;
				
				$lps->Save_Setting($UserID,$SettingList);   
	        }
        }
}
}        // End of directive
?>
