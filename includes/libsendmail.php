<?php

// editing by 
/******************************* Change log *********************************
 * 2017-04-25 (Carlos): modified send_email(), $sys_custom['DHL'] hardcode "DHL Administator" for DHL.
 * 2013-11-11 (Yuen) : extended send_email() to support header par.
 * 2012-06-19 (Yuen) : extended build_html_message() to support attachments
 ****************************************************************************/
 
 
include_once ("$intranet_root/includes/liborgsendmail.php");
function split_array($a, $count)
{
         if ($count == 0) return array($a);

         $i = 0;
         while (list ($key, $val) = each ($a))
         {
                $temp[] = $val;
                $i++;
                if ($i == $count)
                {
                    $result[] = $temp;
                    unset($temp);
                    $i = 0;
                }
         }
         if (sizeof($temp)!=0) $result[] = $temp;
         return $result;
}

class libsendmail {

     var $sendmail_path = "/usr/lib/sendmail";
     var $mail_count;
     function libsendmail()
     {
              global $intranet_root;
              $this->mail_count = trim(get_file_content("$intranet_root/file/mail_count.txt"));
              $this->mail_count += 0;
     }

     function do_sendmail($sender_email, $sender_name, $recipient_email, $recipient_name, $title, $body, $charset="", $attachment=""){
	     
          $subject = $title;
          $message = $body;
          $from_name = $sender_name;
          $from_address = $sender_email;
          $reply_name = $from_name;
          $reply_address = $from_address;
          $reply_address = $from_address;
          $error_delivery_name = $from_name;
          $error_delivery_address = $from_address;
          $to_name = $from_name;
          $to_address = $from_address;

          # Divide the address
          $bcc_name = $recipient_name;
          $bcc_address = $recipient_email;

          $address_array = explode(",",$bcc_address);

          $toSend = split_array($address_array, $this->mail_count);


          for ($i=0; $i<sizeof($toSend); $i++)
          {
               $client_address = implode(",",$toSend[$i]);
               $lu = new liborgsendmail();
               $response = $lu->do_sendmail($sender_email, $sender_name, $client_address, $recipient_name, $title, $body, $charset);
          }

           return $response;
     }
     function SendMail($to,$subject,$body,$headers){
              $lu = new liborgsendmail();
              $lu->SendMail($to,$subject,$body,$headers);
     }
     function send_email($to,$subject,$body,$sender="",$cc="",$bcc="", $attachment="", $header="", $sender_name="")
     {
              global $webmaster, $intranet_root, $sys_custom;
              if ($sender == "")
              {
                  $sender = $webmaster;
                  $sender_name = $sys_custom['DHL']? "DHL Administrator" : "eClass Administrator";
              }
              $lu = new liborgsendmail();
              //write_file_content($attachment, $intranet_root."/file/mailmerge_attachment/sendloglib3.txt");
              return $lu->do_sendmail_nonbcc("$sender","$sender_name",$to,"",$subject,$body, $attachment, $header);

              /*
              $headers = "";
              $headers .= "To: $to\r\n";
              if ($sender != "")
              {
                  $headers .= "From: $sender\r\n";
              }
              if ($cc != "")
              {
                  $headers .= "Cc: $cc\r\n";
              }
              if ($bcc != "")
              {
                  $headers .= "Bcc: $bcc\r\n";
              }
              echo "header: $headers \n";
              return mail($to,$subject,$body,$headers);
              */
     }
}
?>