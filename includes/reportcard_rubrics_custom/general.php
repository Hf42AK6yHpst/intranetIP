<?php
# Editing by
/*
 *	Date : 2018-03-29 (Bill)    [2017-1102-1135-27054]
 *          modified Get_Cover_Page(), to fix PHP 5.4 problem of split()
 *          modified Get_Subject_Mark_Table(), Get_Subject_Topic_Mark_Row(), Get_Performance_Mark_Table(), Get_OtherInfo_Table(), to fix table broken in Chrome
 */

class libreportcardrubrics_custom extends libreportcardrubrics {

	function libreportcardrubrics_custom() {
		$this->libreportcardrubrics();
		$this->EmptySymbol = '&nbsp;';
	}
	
	function Get_Student_Report_Card($ReportID, $StudentID)
	{
		global $Global_DisplaySettingsArr, $lreportcard_ui;		// from /reports/reportcard_generation/print.php
		
		$x = '';
		
		### Cover Page
		if ($Global_DisplaySettingsArr['DisplayCoverPage'])
		{
			$x .= '<div class="container" style="page-break-after:always;">'."\n";
				$x .= $this->Get_Cover_Page($ReportID, $StudentID);
			$x .= '</div>'."\n";
			
			$NextPageBreak = '';
		}
		else
		{
			//$NextPageBreak = 'page-break-after:always;';
			$NextPageBreak = '';
		}
		
//		### Report Title
//		$x .= '<div style="vertical-align:top;width:100%;'.$NextPageBreak.'">'."\n";
//			$x .= $this->Get_Title_Table()."\n";
//		$x .= '</div>'."\n";
//		
//		# Empty Row
//		$x .= $lreportcard_ui->Get_Empty_Image(5);
//		
//		### Header Table
//		$x .= '<div style="vertical-align:top;width:100%;">'."\n";
//			$x .= $this->Get_Header_Info_Table($StudentID)."\n";
//		$x .= '</div>'."\n";
//		
//		# Empty Row
//		$x .= $lreportcard_ui->Get_Empty_Image(10);
		
		### Mark Table
		$x .= '<div style="vertical-align:top;width:100%;">'."\n";
			$x .= $this->Get_Mark_Table($ReportID, $StudentID)."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	
	function Get_Cover_Page($ReportID, $StudentID)
	{
		global $Global_StudentInfoArr, $Global_DisplaySettingsArr, $lreportcard_ui, $Lang;
		global $PATH_WRT_ROOT, $HTTP_SERVER_VARS;
		
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$lfs = new libfilesystem();
		
		$SchoolLogo = $lreportcard_ui->Get_School_Logo('250');
		$CoverTitleArr = $Global_DisplaySettingsArr['CoverTitleArr'];
		$numOfCoverTitle = count($CoverTitleArr);
		
		### Get School Info
		$school_website = $lfs->file_read($PATH_WRT_ROOT."/file/email_website.txt");
		$school_website = ($school_website=="") ? "http://".$HTTP_SERVER_VARS["HTTP_HOST"] : $school_website;
		//$school_email = trim($lfs->file_read($PATH_WRT_ROOT."/file/email_webmaster.txt"));
		//$school_email = ($school_email=="") ? $HTTP_SERVER_VARS["SERVER_ADMIN"] : $school_email;
		//$school_email = "swsps@hkt.salvationarmy.org";
		$school_email = "swsps@hkm.salvationarmy.org";        // [2019-0318-1404-33235]
		
		$school_data = explode("\n", get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$school_name = $school_data[0];
		$school_org = $school_data[1];
		$school_phone = $school_data[2];
		for($i=3; $i<count($school_data); $i++)
			$school_address .= $school_data[$i]."\n";
		
		// 2012-0130-0915-19069
		//$x .= $lreportcard_ui->Get_Empty_Image('180px');
		$x .= $lreportcard_ui->Get_Empty_Image('142px');
		
		### Logo
		$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
			//$x .= '<tr><td style="text-align:center;">'.$SchoolLogo.'</td></tr>'."\n";
			//2016-0606-1159-50235 - show logo again
			//$x .= '<tr><td style="text-align:center; height:250px;">&nbsp;</td></tr>'."\n";
			$x .= '<tr><td style="text-align:center;">'.$SchoolLogo.'</td></tr>'."\n";
		$x .= '</table>'."\n";
		
		// 2012-0130-0915-19069
		//$x .= $lreportcard_ui->Get_Empty_Image('50px');
		$x .= $lreportcard_ui->Get_Empty_Image('88px');
		
		### Cover Title
		$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
			for ($i=0; $i<$numOfCoverTitle; $i++)
			{
				$thisTitle = $CoverTitleArr[$i];
				$x .= '<tr><td class="cover_title" style="text-align:center;">'.$thisTitle.'</td></tr>'."\n";
			}
		$x .= '</table>'."\n";
		
		$x .= $lreportcard_ui->Get_Empty_Image('100px');
		
		### Student Info & Photo
		$x .= '<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="height:300px;vertical-align:top;">'."\n";
					$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td style="width:55%;vertical-align:top;">'."\n";
								$x .= '<table border="0" cellpadding="3" cellspacing="0" align="center" style="width:60%;vertical-align:top;">'."\n";
									# Student Name
									$x .= '<tr class="cover_title">'."\n";
										$x .= '<td style="white-space:nowrap;width:30%;">'.$lreportcard_ui->Get_Customized_Lang('HeaderData', 'StudentName', 'Ch').'</td>'."\n";
										$x .= '<td style="width:1%"> : </td>'."\n";
										$x .= '<td class="border_bottom" style="text-align:center;" nowrap>'.$Global_StudentInfoArr[$StudentID]['StudentName'].'</td>'."\n";
									$x .= '</tr>'."\n";
									# Form Name
									$x .= '<tr class="cover_title">'."\n";
										$x .= '<td style="white-space:nowrap;">'.$lreportcard_ui->Get_Customized_Lang('HeaderData', 'FormName', 'Ch').'</td>'."\n";
										$x .= '<td style="width:1%"> : </td>'."\n";
										$x .= '<td class="border_bottom" style="text-align:center;">'.$Global_StudentInfoArr[$StudentID]['YearName'].'</td>'."\n";
									$x .= '</tr>'."\n";
								$x .= '</table>'."\n";
							$x .= '</td>'."\n";
							
							if ($Global_DisplaySettingsArr['DisplayStudentPhotoFrame'])
							{
								$x .= '<td style="width:5%;">&nbsp;</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<table class="photo_frame" style="width:140px;height:180px;vertical-align:top;">'."\n";
										$x .= '<tr><td>&nbsp</td></tr>'."\n";
									$x .= '</table>'."\n";
								$x .= '</td>'."\n";
							}
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					
					// 2012-0130-0915-19069
					//$x .= $lreportcard_ui->Get_Empty_Image('80px');
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";	
			
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					### School Info
					$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
						$x .= '<tr><td class="school_info" style="text-align:center;">'.$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Addresss'].': '.$school_address.'</td></tr>'."\n";
					$x .= '</table>'."\n";
					$x .= $lreportcard_ui->Get_Empty_Image('10px');
					$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
						$x .= '<tr>'."\n";
							$numOfPhone = count($Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone']);
							for ($i=0; $i<$numOfPhone; $i++)
							{
								$x .= '<td class="school_info" style="text-align:center;">'.$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Phone'][$i].'</td>'."\n";
							}
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
					$x .= $lreportcard_ui->Get_Empty_Image('10px');
					$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="school_info" style="text-align:center;">'.$Lang['eRC_Rubrics']['Template']['SchoolInfo']['WebSite'].': '.$school_website.'</td>'."\n";
							$x .= '<td class="school_info" style="text-align:center;">'.$Lang['eRC_Rubrics']['Template']['SchoolInfo']['Email'].': '.$school_email.'</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";	
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	
	function Get_Title_Table()
	{
		global $Global_DisplaySettingsArr, $lreportcard_ui;
		
		$SchoolNameDisplay 	= $Global_DisplaySettingsArr['SchoolNameDisplay'];
		$ReportTitleArr 	= $Global_DisplaySettingsArr['ReportTitleArr'];
		$numOfTitle = count($ReportTitleArr);
		
		$x = '';
		
		if ($SchoolNameDisplay)
		{
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td style="width:50%;">&nbsp;</td>';
					$x .= '<td class="border_bottom font_16px" style="text-align:center;" nowrap>'.GET_SCHOOL_NAME().'</td>'."\n";
					$x .= '<td style="width:50%;">&nbsp;</td>';
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			if ($numOfTitle > 0)
				$x .= $lreportcard_ui->Get_Empty_Image(5);
		}
		
		for ($i=0; $i<$numOfTitle; $i++)
		{
			$thisTitle = $ReportTitleArr[$i];
			
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td style="width:50%;">&nbsp;</td>';
					$x .= '<td class="border_bottom font_16px" style="text-align:center;" nowrap>'.$thisTitle.'</td>'."\n";
					$x .= '<td style="width:50%;">&nbsp;</td>';
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			// no empty row for the last title
			if ($i < $numOfTitle - 1)
			{
				$x .= $lreportcard_ui->Get_Empty_Image(5);
			}
		}
		return $x;
	}
	
	function Get_Header_Info_Table($StudentID)
	{
		global $Global_DisplaySettingsArr, $Global_StudentInfoArr, $Global_ReportInfoArr, $Lang, $lreportcard_ui;
		
		$numOfHeaderDataPerRow = $Global_DisplaySettingsArr['NumOfHeaderDataPerRow'];
		$HeaderDataFieldArr = $Global_DisplaySettingsArr['HeaderDataFieldArr'];
		$numOfHeaderData = count($HeaderDataFieldArr);		
		
		# Student Info
		$HeaderData['StudentName'] = $Global_StudentInfoArr[$StudentID]['StudentName'];
		$HeaderData['ClassName'] = Get_Lang_Selection($Global_StudentInfoArr[$StudentID]['ClassTitleCh'], $Global_StudentInfoArr[$StudentID]['ClassTitleEn']);
		
		# Module Name
		$ReportModuleInfoArr = $Global_ReportInfoArr['ModuleInfo'];
		$ReportModuleNameArr = Get_Array_By_Key($ReportModuleInfoArr, Get_Lang_Selection('ModuleNameCh', 'ModuleNameEn'));
		$HeaderData['ModuleName'] = implode(', ', $ReportModuleNameArr);
		
		# Module Date Range
		$numOfModule = count($ReportModuleInfoArr);
		$StartDate = $ReportModuleInfoArr[0]['StartDate'];
		$EndDate = $ReportModuleInfoArr[$numOfModule-1]['EndDate'];
		
		$HeaderData['ModuleDateRange'] = '';
		if ($StartDate != '' && $EndDate != '')
		{
			$StartDateArr = explode('-', $StartDate);
			$EndDateArr = explode('-', $EndDate);
			$ModuleRangeDisplay = $Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['DateRangeDisplay'];
			$ModuleRangeDisplay = str_replace('<!--StartYear-->', $StartDateArr[0], $ModuleRangeDisplay);
			$ModuleRangeDisplay = str_replace('<!--StartMonth-->', $StartDateArr[1], $ModuleRangeDisplay);
			$ModuleRangeDisplay = str_replace('<!--StartDate-->', $StartDateArr[2], $ModuleRangeDisplay);
			$ModuleRangeDisplay = str_replace('<!--EndYear-->', $EndDateArr[0], $ModuleRangeDisplay);
			$ModuleRangeDisplay = str_replace('<!--EndMonth-->', $EndDateArr[1], $ModuleRangeDisplay);
			$ModuleRangeDisplay = str_replace('<!--EndDate-->', $EndDateArr[2], $ModuleRangeDisplay);
			$HeaderData['ModuleDateRange'] = $ModuleRangeDisplay;
		}
		
		$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" valign="top">'."\n";
			for ($i=0; $i<$numOfHeaderDataPerRow; $i++)
			{
				# Title
				$x .= '<col style="width:5%;text-align:left;vertical-align:bottom;">'."\n";
				# ":"
				$x .= '<col style="width:1%;text-align:left;vertical-align:bottom;">'."\n";
				# Content
				$x .= '<col style="width:5%;text-align:center;vertical-align:bottom;">'."\n";
				
				if ($i < $numOfHeaderDataPerRow-1)
				{
					# separator
					$x .= '<col align="left">'."\n";
				}
			}
			
			for ($i=0; $i<$numOfHeaderData; $i++)
			{
				$thisHeaderKey = $HeaderDataFieldArr[$i];
				$thisTitle = $lreportcard_ui->Get_Customized_Lang('HeaderData', $thisHeaderKey);
				$thisContent = $HeaderData[$thisHeaderKey];
				
				if ($i % $numOfHeaderDataPerRow == 0)
				{
					// Start a new row
					$x .= '<tr>'."\n";
				}
				
//				if ($Global_DisplaySettingsArr['ReportType'] == 'GeneralReport' && $thisHeaderKey == 'ModuleDateRange')
//				{
//					// no title for the module date range
//					$x .= '<td colspan="3" class="border_bottom" nowrap>'.$thisContent.'</td>'."\n";
//				}
//				else
//				{
					//2016-0606-1201-09235
					$_centerCss = '';
					if ($thisHeaderKey == 'ClassName') {
						$_centerCss = ' style="text-align:center;" ';
					}
					
					$x .= '<td nowrap>'.$thisTitle.'</td>'."\n";
					$x .= '<td>: </td>'."\n";
					$x .= '<td class="border_bottom" '.$_centerCss.' nowrap>'.$thisContent.'</td>'."\n";
//				}
				
				if (($i % $numOfHeaderDataPerRow) == ($numOfHeaderDataPerRow - 1))
				{
					// Last cell of the row => End the row
					$x .= '</tr>'."\n";
				}
				else
				{
					// Add a separator between each column
					$x .= '<td>&nbsp;</td>'."\n";
				}
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Mark_Table($ReportID, $StudentID)
	{
		global $Global_DisplaySettingsArr, $Global_SubjectRubricsInfoArr;
		global $PATH_WRT_ROOT, $lreportcard_ui;
		
		$YearID = $Global_DisplaySettingsArr['YearID'];
		$DisplayClassTeacherComment = $Global_DisplaySettingsArr['DisplayClassTeacherComment'];
		$SubjectIDArr = $Global_DisplaySettingsArr['SubjectIDArr'];
		$numOfSubject = count($SubjectIDArr);
		
		### Separate Subject for Different Rubrics Scheme
		$PerformanceSubjectID = $this->Get_Performance_SubjectID();
		$SubjectRubricsArr = array();
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $SubjectIDArr[$i];
			if ($thisSubjectID == $PerformanceSubjectID)
				continue;
			
			foreach ((array)$Global_SubjectRubricsInfoArr[$YearID][$thisSubjectID] as $thisRubricsID => $thisRubricsInfoArr)
			{
				(array)$SubjectRubricsArr[$thisRubricsID][] = $thisSubjectID;
			}
		}
		
		### Build Subject Mark Table
		$x = '';
		$NumOfTable = count($SubjectRubricsArr);
		$TableCounter = 0;
		$TableHTMLArr = array();  
        
		foreach ((array)$SubjectRubricsArr as $thisRubricsID => $thisSubjectIDArr)
		{
			$TableCounter++;
			if ($TableCounter < $NumOfTable) {
				$thisNeedPageBreak = 1;
			}
			else {
				$thisNeedPageBreak = 0;		
			}
			
			$thisIsFirst = ($TableCounter==1)? true : false;
            
			$thisSubjectTable = $this->Get_Subject_Mark_Table($ReportID, $StudentID, $thisSubjectIDArr, $thisRubricsID, $thisNeedPageBreak, $thisIsFirst);
			if ($thisSubjectTable != '')
				$TableHTMLArr[] = $thisSubjectTable;
		}
		
		$numOfTable = count($TableHTMLArr);
		for ($i=0; $i<$numOfTable; $i++)
		{
			$x .= $TableHTMLArr[$i];
			
			//if ($i < $numOfTable - 1)
				//$x .= '<div style="page-break-after:always;"></div>'."\n";
		}
		
		
		### Build "Performance" Table
		$x .= $this->Get_Performance_Mark_Table($ReportID, $StudentID, $PerformanceSubjectID);
		
		### Build ECA Table
		$x .= $this->Get_ECA_Table($ReportID, $StudentID);
		
		### Build Other Info Table
		$x .= $this->Get_OtherInfo_Table($ReportID, $StudentID);
		
		### Build Class Teacher Comment Table
		if ($DisplayClassTeacherComment)
			$x .= $this->Get_ClassTeacherComment_Table($ReportID, $StudentID);
		
		### Build Signature Table
		$x .= $this->Get_Signature_Table($ReportID, $StudentID);
		
		return $x;
	}
	
	function Get_Subject_Mark_Table($ReportID, $StudentID, $SubjectIDArr, $RubricsID, $ApplyPageBreak, $IsFirst)
	{
		global $PATH_WRT_ROOT, $lreportcard_ui;
		global $Global_DisplaySettingsArr, $Global_ReportInfoArr, $Global_SubjectInfoArr, $Global_SubjectRubricsInfoArr;	// from print.php
		global $Global_StudentMarksheetScoreArr, $Global_SubjectTopicTreeArr, $Global_TopicStatisticsArr;	// from print.php
		global $MarkTableRubricsInfoArr, $TopicDisplayIndexArr, $SubjectMarkDisplayArr, $StudentStudyTopicInfoArr;	// for result row generation
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_topic.php');
		include_once($PATH_WRT_ROOT.'includes/libreportcardrubrics_rubrics.php');
		$lreportcard_topic = new libreportcardrubrics_topic();
		
		$SubjectPerPage = 4; //when you want to show 3 subjects in a page, just change it to 3;
		
		### Get Rubrics Info
		$lreportcard_rubrics = new libreportcardrubrics_rubrics();
		$RubricsInfoArr = $lreportcard_rubrics->Get_Rubics_Info($RubricsID);
		
		### Get Student Study Topic Info
		// $StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = StudyTopicInfoArr
		$ReportModuleIDArr = Get_Array_By_Key($Global_ReportInfoArr['ModuleInfo'], 'ModuleID');
		$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ReportModuleIDArr, '', '', $StudentID);
		
		$YearID						= $Global_DisplaySettingsArr['YearID'];
		$SubjectDisplay 			= $Global_DisplaySettingsArr['SubjectDisplay'];
		$SubjectGroupDisplay 		= $Global_DisplaySettingsArr['SubjectGroupDisplay'];
		$MarkDisplayOption			= $Global_DisplaySettingsArr['MarkDisplayOption'];
		$TopicLevelArr 				= $Global_DisplaySettingsArr['TopicLevelArr'];
		$TopicNeedNotStudyDisplay	= $Global_DisplaySettingsArr['TopicNeedNotStudyDisplay'];
		$MarksheetRemarksDisplay	= $Global_DisplaySettingsArr['MarksheetRemarksDisplay'];
		$ReportModuleIDArr			= Get_Array_By_Key($Global_ReportInfoArr['ModuleInfo'], 'ModuleID');
		
		$ShowSubjectColumn = ($SubjectDisplay != 'Hidden' || $SubjectGroupDisplay == 1);
		
		$numOfSubject = count($SubjectIDArr);
		$numOfTopicLevel = count($TopicLevelArr);
		$numOfModule = count($Global_ReportInfoArr['ModuleInfo']);
		
		### Get the Rubrics Info of the 1st Subject
		$FirstSubjectID = $SubjectIDArr[0];
		$MarkTableRubricsInfoArr = $Global_SubjectRubricsInfoArr[$YearID][$FirstSubjectID][$RubricsID];
		$numOfRubricsItem = count($MarkTableRubricsInfoArr);
		
		### For empty topic title cell determination
		$TopicLevelArr	= $Global_DisplaySettingsArr['TopicLevelArr'];
		$numOfTopicLevel = count($TopicLevelArr);
		$LastTopicLevel = $TopicLevelArr[$numOfTopicLevel-1];
		
		### Calculate Column Width
		$SubjectColumnWidth = 15;
		$RemarksColumnWidth = 20;
		$RubricsItemWidth = 4;
		$RubricsItemTotalWidth = 4 * $numOfRubricsItem;
		$AverageTopicColumnWidth = floor((100 - $SubjectColumnWidth - $RemarksColumnWidth - $RubricsItemTotalWidth) / $numOfTopicLevel);
		$TopicWidthDifference = 5;
		
		$PageBreak_Style = ($ApplyPageBreak==1)? 'style="page-break-after:always;"' : '';
		$x = '';
		
			# Subject
			if ($ShowSubjectColumn)
				$x .= '<col style="text-align:left;vertical-align:top;width:'.$SubjectColumnWidth.'%;" />'."\n";
			
			# Topic Level Title (Col Width)
			for ($i=0; $i<$numOfTopicLevel; $i++)
			{
				$thisWidthAdjustmentRatio = ($i + 1) - ceil($numOfTopicLevel / 2);
				$thisWidth = floor($AverageTopicColumnWidth + ($TopicWidthDifference * $thisWidthAdjustmentRatio));
				
				$x .= '<col style="text-align:left;vertical-align:top;width:'.$thisWidth.'%" />'."\n";
			}
			
			# Remarks	
			if ($MarksheetRemarksDisplay)
				$x .= '<col style="text-align:left;vertical-align:top;width:'.$RemarksColumnWidth.'%;" />'."\n";
			
			# Rubrics Items
//			for ($i=0; $i<$numOfRubricsItem; $i++)
//				$x .= '<col style="text-align:center;vertical-align:top;width:'.$RubricsItemWidth.'%;" />'."\n";
			if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'W' && $MarkDisplayOption == 'ModuleBased')
			{
				// Display Modules
				for ($i=0; $i<$numOfModule; $i++) { 
					$x .= '<col style="text-align:center;vertical-align:top;width:'.$RubricsItemWidth.'%;" />'."\n";
				}
			}
			else
			{
				// Display Rubrics Items
				for ($i=0; $i<$numOfRubricsItem; $i++) {
					$x .= '<col style="text-align:center;vertical-align:top;width:'.$RubricsItemWidth.'%;" />'."\n";
				}
			}	
			
//			if ($MarksheetRemarksDisplay)
//				$x .= '<col style="text-align:left;vertical-align:top;width:15%;" />'."\n";
			
			### Header
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					# Subject
					if ($ShowSubjectColumn)
					{
						$thisTitle = ($IsFirst)? $lreportcard_ui->Get_Customized_Lang('General', 'Subject') : '&nbsp;';
						$x .= '<td rowspan="2" style="text-align:center;">'.$thisTitle.'</td>';
					}
					
					# Topic Level Title
					switch ($Global_DisplaySettingsArr['ReportType']) {
						case 'GeneralReport':
							$thisTopicType = 'TopicLevel';
							break;
						case 'TherapyReport':
							$thisTopicType = 'TherapyTopicLevel';
							break;
						case 'LearningReport':
							$thisTopicType = 'LearningReportLevel';
							break;
					}
					for ($i=0; $i<$numOfTopicLevel; $i++)
					{
						$thisLevel = $TopicLevelArr[$i];
						$border_left = ($i==0 && $ShowSubjectColumn==false)? '' : 'border_left';
						$thisTitle = ($IsFirst)? $lreportcard_ui->Get_Customized_Lang($thisTopicType, $thisLevel) : '&nbsp;';
						
						$x .= '<td rowspan="2" class="'.$border_left.'" style="text-align:center;">'.$thisTitle.'</td>';
					}
					
					# Remarks
					if ($MarksheetRemarksDisplay)
					{
						if ($Global_DisplaySettingsArr['ReportType'] == 'GeneralReport') {
							$thisTitle = $lreportcard_ui->Get_Customized_Lang('General', 'TopicDetailsRemarks');
						}
						else {
							$thisTitle = $lreportcard_ui->Get_Customized_Lang('General', 'Remarks');
						}
						$x .= '<td rowspan="2" class="'.$border_left.'" style="text-align:center;">'.$thisTitle.'</td>';
					}
					
					# Result
					if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'T')
					{
						switch ($Global_DisplaySettingsArr['ReportType']) {
							case 'GeneralReport':
								$thisTitleKey = 'Result';
								break;
							case 'TherapyReport':
								$thisTitleKey = 'StudentPerformance';
								break;
							case 'LearningReport':
								$thisTitleKey = 'LearningPerformance';
								break;
						}
						$x .= '<td colspan="'.$numOfRubricsItem.'" class="border_left" style="text-align:center;">'.$lreportcard_ui->Get_Customized_Lang('General', $thisTitleKey).'</td>';
					}
					else
					{
						if ($MarkDisplayOption == 'ModuleBased')
						{
							### Modules Title
							for ($i=0; $i<$numOfModule; $i++)
							{
								$thisModuleNameEn 	= $Global_ReportInfoArr['ModuleInfo'][$i]['ModuleNameEn'];
								$thisModuleNameCh 	= $Global_ReportInfoArr['ModuleInfo'][$i]['ModuleNameCh'];
								$thisModuleName 	= Get_Lang_Selection($thisModuleNameCh, $thisModuleNameEn);
								
								$x .= '<td rowspan="2" class="border_left" style="text-align:center;width:4%;"><div style="width:100%;word-wrap:break-word;text-align:center;">'.$thisModuleName.'</div></td>';
							}
						}
						else if ($MarkDisplayOption == 'Consolidated_%' || $MarkDisplayOption == 'Consolidated_Fraction')
						{
							//$thisTitle = Get_Lang_Selection($RubricsInfoArr[0]['RubricsCh'], $RubricsInfoArr[0]['RubricsEn']);
							$thisTitle = $lreportcard_ui->Get_Customized_Lang('General', 'StudentPerformanceRatio');
							$x .= '<td colspan="'.$numOfRubricsItem.'" class="border_left" style="text-align:center;">'.$thisTitle.'</td>';
						}
					}	
					
					# Remarks
//					if ($MarksheetRemarksDisplay)
//					{
//						$thisTitle = $lreportcard_ui->Get_Customized_Lang('General', 'Remarks');
//						$x .= '<td rowspan="2" class="'.$border_left.'" style="text-align:center;">'.$thisTitle.'</td>';
//					}
				$x .= '</tr>'."\n";
				
				### Rubrics Item Title
				if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'T' || $MarkDisplayOption != 'ModuleBased')
				{
					$x .= '<tr>'."\n";
						foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
						{
							$thisItemName = Get_Lang_Selection($thisRubricsItemInfoArr['LevelNameCh'], $thisRubricsItemInfoArr['LevelNameEn']);
							$x .= '<td class="border_left border_top" style="text-align:center;">'.$thisItemName.'</td>';
						}
					$x .= '</tr>'."\n";
				}
			$x .= '</thead>'."\n";
			
			### Content
			$html = '';
			$NumOfDisplayedSubject = 0;
			$IndexOfDisplayedSubject = 0;
			
			$PageBreak_Style= 'style="page-break-after:always;"';
			
			# Report Title
			$HeaderInfo = '<div style="vertical-align:top;width:100%;">'."\n";
				$HeaderInfo .= $this->Get_Title_Table()."\n";
			$HeaderInfo .= '</div>'."\n";
			
			# Empty Row
			$HeaderInfo .= $lreportcard_ui->Get_Empty_Image(5);
			
			# Header Table
			$HeaderInfo .= '<div style="vertical-align:top;width:100%;">'."\n";
				$HeaderInfo .= $this->Get_Header_Info_Table($StudentID)."\n";
			$HeaderInfo .= '</div>'."\n";
			
			# Empty Row
			$HeaderInfo .= $lreportcard_ui->Get_Empty_Image(10);
			$HeaderInfo .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" valign="top" class="report_border" '.$PageBreak_Style.'>'."\n";	
			$HeaderInfo .= $x;
			
			$tableBottom_html = '</table>'."\n";	
			
			### to count the maximum number of Displayed Subjects
			$NumOfDisplayedSubject_MAX=0;
			for ($i=0; $i<$numOfSubject; $i++)
			{				
				$TopicDisplayIndexArr = array();
				$thisSubjectID = $SubjectIDArr[$i];
				### Get Report Subject Topic Display Tree
				if ($TopicNeedNotStudyDisplay == 1)
					$thisSubjectTopicTreeArr = $Global_SubjectTopicTreeArr[$thisSubjectID];
				else
					$thisSubjectTopicTreeArr = $this->Get_Student_Study_Topic_Tree($StudentID, $thisSubjectID, $ReportModuleIDArr);			
		
				### Skip the not study Subject   //aaaa
				if (count($thisSubjectTopicTreeArr) == 0)
					continue;
					
				$NumOfDisplayedSubject_MAX++;
			}
			
			### to display subjects
			
				$SetHeader=false;
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$html_content='';
					$html_content .= '<tbody>'."\n";
					
					$TopicDisplayIndexArr = array();
					$thisSubjectID = $SubjectIDArr[$i];
					
					### Get Report Subject Topic Display Tree
					if ($TopicNeedNotStudyDisplay == 1)
						$thisSubjectTopicTreeArr = $Global_SubjectTopicTreeArr[$thisSubjectID];
					else
						$thisSubjectTopicTreeArr = $this->Get_Student_Study_Topic_Tree($StudentID, $thisSubjectID, $ReportModuleIDArr);
					$thisNumOfRow = $this->Count_Number_Of_Row_Of_The_Subject($thisSubjectTopicTreeArr, $LastTopicLevel);
					
			
					### Skip the not study Subject   //aaaa
					if (count($thisSubjectTopicTreeArr) == 0)
						continue;
					
					# Subject Name
					if ($ShowSubjectColumn)
					{
						$thisSubjectDisplayArr = array();
						if ($SubjectDisplay != 'Hidden')
						{
							$thisSubjectInfoArr = $Global_SubjectInfoArr[$thisSubjectID];
							
							switch ($SubjectDisplay)
							{
								case 'Desc':
									$thisSubjectDisplayArr[] = Get_Lang_Selection($thisSubjectInfoArr['CH_DES'], $thisSubjectInfoArr['EN_DES']);
								break;
								
								case 'Abbr':
									$thisSubjectDisplayArr[] = Get_Lang_Selection($thisSubjectInfoArr['CH_ABBR'], $thisSubjectInfoArr['EN_ABBR']);
								break;
								
								case 'ShortForm':
									$thisSubjectDisplayArr[] = Get_Lang_Selection($thisSubjectInfoArr['CH_SNAME'], $thisSubjectInfoArr['EN_SNAME']);
								break;
							}
						}
						
						# Subject Group Name
						if ($SubjectGroupDisplay == 1)
						{
							$thisYearTermID = $Global_ReportInfoArr['ModuleInfo'][0]['YearTermID'];
							$thisSubjectGroupID = $this->Get_Student_Studying_Subject_Group($thisYearTermID, $StudentID, $thisSubjectID);
							
							$thisSGObj = new subject_term_class($thisSubjectGroupID);
							$thisSubjectDisplayArr[] = $thisSGObj->Get_Class_Title();
						}
						
						$thisSubjectDisplay = implode('<br />', $thisSubjectDisplayArr);
						$thisSubjectRowSpan = $thisNumOfRow ? $thisNumOfRow : "";
						
						$html_content .= '<tr >'."\n";
                            $html_content .= '<td class="border_top" rowspan="'.$thisSubjectRowSpan.'" style="vertical-align:top;text-align:center;">'.$thisSubjectDisplay.'</td>';
					}
				    
					$html_content .= $this->Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $thisSubjectID, $RubricsID, $IsFirst=1, $numOfRubricsItem, $thisSubjectTopicTreeArr);
					$html_content .= '</tbody>'."\n";
					
					### 4 subjects in one page	(set $SubjectPerPage as 4)
					
					if($NumOfDisplayedSubject==0)  // if it is 1st subject (first subject), add the header
					{
						$header_html=$HeaderInfo;
						
						if($SubjectPerPage<=1)  // if $SubjectPerPage is equal or small than one 
						{
							$bottom_html = $tableBottom_html;
						}
						else if($NumOfDisplayedSubject_MAX<=1)
						{
							$bottom_html = $tableBottom_html;
						}
					}
					else
					{
						$header_html='';						
							
						if(($NumOfDisplayedSubject+1)%$SubjectPerPage==0)   // if it is  4th or 8th or 12th ....subject (last subject), add </table> 
						{
							$bottom_html = $tableBottom_html;
							$SetHeader=true;
						}
						else if($SetHeader==true) // if it is 5th or 9th or 13th....subject (first subject), add the header
						{
							$header_html=$HeaderInfo;
							$SetHeader=false;
						}
						if($SubjectPerPage<=1)  // if $SubjectPerPage is equal or small than one 
						{
							$header_html=$HeaderInfo;
							$bottom_html = $tableBottom_html;
						}
					}
					if($NumOfDisplayedSubject >= $NumOfDisplayedSubject_MAX-1)  // if it is the last subject , add </table> 
					{
						$bottom_html = $tableBottom_html;
					}
	
					$html .= $header_html; 		
					$html .= $html_content; // it is the subject content			
					$html .= $bottom_html;
					
					$header_html='';
					$bottom_html='';

					
					$NumOfDisplayedSubject++;
				}
		
		if ($NumOfDisplayedSubject > 0)
			return $html;
	}
	
	/*
	function Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $SubjectID, $RubricsID, $IsFirst, $NumOfRubricsItem, $SubjectTopicTreeArr)
	{
		global $eRC_Rubrics_ConfigArr, $Global_TopicInfoArr, $Global_DisplaySettingsArr, $Global_SubjectRubricsInfoArr, $Global_StudentMarksheetScoreArr, $Global_ReportInfoArr, $lreportcard_ui;
		global $MarkTableRubricsInfoArr, $TopicDisplayIndexArr, $SubjectMarkDisplayArr, $StudentStudyTopicInfoArr;	// from function Get_Subject_Mark_Table()
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		# Border Left determination
		$YearID						= $Global_DisplaySettingsArr['YearID'];
		$SubjectDisplay				= $Global_DisplaySettingsArr['SubjectDisplay'];
		$TopicNeedNotStudyDisplay	= $Global_DisplaySettingsArr['TopicNeedNotStudyDisplay'];
		$MarkDisplayOption			= $Global_DisplaySettingsArr['MarkDisplayOption'];
		$SubjectGroupDisplay 		= $Global_DisplaySettingsArr['SubjectGroupDisplay'];
		$ShowSubjectColumn 			= ($SubjectDisplay != 'Hidden' || $SubjectGroupDisplay == 1);
		
		$numOfModule = count($Global_ReportInfoArr['ModuleInfo']);
		
		# For empty topic title cell determination
		$TopicLevelArr	= $Global_DisplaySettingsArr['TopicLevelArr'];
		$numOfTopicLevel = count($TopicLevelArr);
		$FirstTopicLevel = $TopicLevelArr[0];
		$SecondLastTopicLevel = $TopicLevelArr[$numOfTopicLevel-2];
		$LastTopicLevel = $TopicLevelArr[$numOfTopicLevel-1];
		
		$x = '';
		$thisTopicCounter = 0;
		foreach ($SubjectTopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisTopicCounter++;
			
			$thisLevel 					= $Global_TopicInfoArr[$thisTopicID]['Level'];
			$thisDisplayOrder 			= $Global_TopicInfoArr[$thisTopicID]['DisplayOrder'];
			$thisParentTopicID			= $Global_TopicInfoArr[$thisTopicID]['ParentTopicID'];
			$thisPreviousLevelTopicID	= $Global_TopicInfoArr[$thisTopicID]['PreviousLevelTopicID'];
			$thisNameEn 				= $Global_TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh 				= $Global_TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 					= Get_Lang_Selection($thisNameCh, $thisNameEn);
			$thisNumOfNextLevelTopic 	= count($thisLinkedTopicIDArr);
			
			if ($thisLevel > $MaxLevel || !in_array($thisLevel, $TopicLevelArr))
			{
				### skip the display of topic if it is not selected for display
				$x .= $this->Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $SubjectID, $RubricsID, $IsFirst, $NumOfRubricsItem, $thisLinkedTopicIDArr);
				$IsFirst = 0;
				continue;
			}
				
				
			### Style
			$border_top = ($IsFirst == 1)? 'border_top' : '';	
			$first_border_left = ($ShowSubjectColumn == true)? 'border_left' : '';
			
			$StartNewLine = false;
			if ($thisLevel == $MaxLevel && count($TopicLevelArr) == 1 && $thisTopicCounter == 1)
			{
				// show last level only => do not open a new line
				$StartNewLine = false;
			}
			else
			{
				// show other levels => open a new line 
				if ($thisLevel == $FirstTopicLevel && ($ShowSubjectColumn==false || $ShowSubjectColumn==true && $IsFirst != 1))
					$StartNewLine = true;
				if ($thisLevel == $MaxLevel)
					$StartNewLine = true;
			}
			
			
			if ($StartNewLine)
				$x .= '<tr>'."\n";
			
			### Add empty cells before the title
			if ($thisTopicCounter > 1 || $thisLevel == $MaxLevel)
			{
				for ($i=0; $i<$numOfTopicLevel; $i++)
				{
					if ($TopicLevelArr[$i] >= $thisLevel)
						continue;
					
					$this_border_left = ($i==0)? $first_border_left : 'border_left';
					$x .= '<td class="'.$this_border_left.' '.$border_top.'">&nbsp;</td>'."\n";
					
				}
			}
			
			
			### Topic Name Display
			$this_border_left = ($thisLevel == $FirstTopicLevel)? $first_border_left : 'border_left';
			
			# Reset Counter if starting a new level
			if ($thisLevel < $MaxLevel)
				$this->Reset_Topic_Display_Index($thisLevel + 1);
				
			$TopicDisplayIndexArr[$thisLevel]++;
			$thisIndex = $this->Get_Topic_Display_Index($SubjectID, $thisLevel);
			$x .= '<td class="'.$this_border_left.' '.$border_top.'" style="vertical-align:top;">'.$thisIndex.' '.$thisName.'</td>'."\n";
			
			
			### Add empty cells after the title
			if ($thisNumOfNextLevelTopic == 0 || $thisLevel == $SecondLastTopicLevel || $thisLevel == $LastTopicLevel)
			{
				for ($i=0; $i<$numOfTopicLevel; $i++)
				{
					if ($TopicLevelArr[$i] <= $thisLevel)
						continue;
						
					$x .= '<td class="border_left '.$border_top.'">&nbsp;</td>'."\n";
				}
			}
			
			
			if ($thisNumOfNextLevelTopic > 0)
			{
				// Next line to display the Last Topic Level Info
				if ($thisLevel == $SecondLastTopicLevel)
				{
						# Empty cells for "Result" if the topic is not the last level and without any last level topic mapping
						$x .= $this->Get_Empty_Mark_Row($Global_ReportInfoArr['BasicInfo'][0]['ReportType'], $MarkDisplayOption, $border_top, $numOfModule, $NumOfRubricsItem);
					$x .= '</tr>'."\n";
					
					$IsFirst = 0;
				}
					
				$x .= $this->Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $SubjectID, $RubricsID, $IsFirst, $NumOfRubricsItem, $thisLinkedTopicIDArr);
			}
			else
			{
				if ($thisLevel == $MaxLevel)
				{
					# Student Topic Result
					if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'T')
					{
						### Term Report
						$thisModuleID = $Global_ReportInfoArr['ModuleInfo'][0]['ModuleID'];
						$thisStudentRubricsItemID = $Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
						
						foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
						{
							$thisDisplay = ($thisStudentRubricsItemID == $thisRubricsItemID)? $lreportcard_ui->Get_Tick_Image() : "&nbsp;";
							$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
						}
					}
					else if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'W')
					{
						### Consolidated Report
						if ($MarkDisplayOption == 'ModuleBased')
						{
							### Display Mark of Different Modules
							for ($i=0; $i<$numOfModule; $i++)
							{
								$thisModuleID 	= $Global_ReportInfoArr['ModuleInfo'][$i]['ModuleID'];
								
								$thisIsStudy = isset($StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$SubjectID][$thisTopicID]);
								if ($thisIsStudy)
								{
									$thisRubricsItemID = $Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
									
									if ($thisRubricsItemID > 0)
									{
										# Display Mark Name
										$thisRubricsItemNameEn = $Global_SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$thisRubricsItemID]['LevelNameEn'];
										$thisRubricsItemNameCh = $Global_SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$thisRubricsItemID]['LevelNameCh'];
										$thisDisplay = Get_Lang_Selection($thisRubricsItemNameCh, $thisRubricsItemNameEn);
									}
									else
									{
										# Not yet input mark
										$thisDisplay = '&nbsp;';
									}
								}
								else
								{
									# No need Study
									$thisDisplay = '&nbsp;';
								}
								
								$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
							}
						}
						else if ($MarkDisplayOption == 'Consolidated_%')
						{
							### Display Consolidated Mark Info
							$thisNumOfStudiedModule = $SubjectMarkDisplayArr[$thisTopicID]['NumOfStudiedModule'];
							
							foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
							{
								$thisRubricsItemCount = $SubjectMarkDisplayArr[$thisTopicID]['MarkInfoArr'][$thisRubricsItemID]['Count'];
								if ($thisRubricsItemCount == 0 || $thisNumOfStudiedModule == 0)
								{
									$thisDisplay = '&nbsp;';
								}
								else
								{
									$thisDisplay = my_round(($thisRubricsItemCount / $thisNumOfStudiedModule) * 100, 1);
									$thisDisplay .= '%';
								}
								$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
							}
						}
						else if ($MarkDisplayOption == 'Consolidated_Fraction')
						{
							$thisNumOfStudiedModule = $SubjectMarkDisplayArr[$thisTopicID]['NumOfStudiedModule'];
							
							foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
							{
								$thisRubricsItemCount = $SubjectMarkDisplayArr[$thisTopicID]['MarkInfoArr'][$thisRubricsItemID]['Count'];
								if ($thisRubricsItemCount == 0 || $thisNumOfStudiedModule == 0)
								{
									$thisDisplay = '&nbsp;';
								}
								else
								{
									$thisDisplay = $thisRubricsItemCount.'/'.$thisNumOfStudiedModule;
								}
								$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
							}
						}
					}
				}
				else
				{
					# Empty cells for "Result" if the topic is not the last level and without any last level topic mapping
					$x .= $this->Get_Empty_Mark_Row($Global_ReportInfoArr['BasicInfo'][0]['ReportType'], $MarkDisplayOption, $border_top, $numOfModule, $NumOfRubricsItem);
				}
				
				$x .= '</tr>'."\n";
			}
			$IsFirst = 0;
			
		}	// End of foreach ($SubjectTopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		
		return $x;
	}
	*/
	
	function Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $SubjectID, $RubricsID, $IsFirst, $NumOfRubricsItem, $SubjectTopicTreeArr)
	{
		global $eRC_Rubrics_ConfigArr, $Global_TopicInfoArr, $Global_DisplaySettingsArr, $Global_SubjectRubricsInfoArr, $Global_StudentMarksheetScoreArr, $Global_ReportInfoArr, $Global_TopicStatisticsArr;
		global $lreportcard, $lreportcard_ui;
		global $MarkTableRubricsInfoArr, $TopicDisplayIndexArr, $SubjectMarkDisplayArr, $StudentStudyTopicInfoArr;	// from function Get_Subject_Mark_Table()
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		# Border Left determination
		$YearID						= $Global_DisplaySettingsArr['YearID'];
		$SubjectDisplay				= $Global_DisplaySettingsArr['SubjectDisplay'];
		$TopicNeedNotStudyDisplay	= $Global_DisplaySettingsArr['TopicNeedNotStudyDisplay'];
		$MarkDisplayOption			= $Global_DisplaySettingsArr['MarkDisplayOption'];
		$SubjectGroupDisplay 		= $Global_DisplaySettingsArr['SubjectGroupDisplay'];
		$MarksheetRemarksDisplay	= $Global_DisplaySettingsArr['MarksheetRemarksDisplay'];
		$ShowSubjectColumn 			= ($SubjectDisplay != 'Hidden' || $SubjectGroupDisplay == 1);
		
		$numOfModule = count($Global_ReportInfoArr['ModuleInfo']);
		
		# For empty topic title cell determination
		$TopicLevelArr	= $Global_DisplaySettingsArr['TopicLevelArr'];
		$numOfTopicLevel = count($TopicLevelArr);
		$FirstTopicLevel = $TopicLevelArr[0];
		$SecondLastTopicLevel = $TopicLevelArr[$numOfTopicLevel-2];
		$LastTopicLevel = $TopicLevelArr[$numOfTopicLevel-1];
		
		$x = '';
		$thisTopicCounter = 0;
		foreach ((array)$SubjectTopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisTopicCounter++;
			
			$thisLevel 					= $Global_TopicInfoArr[$thisTopicID]['Level'];
			$thisNameEn 				= $Global_TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisNameCh 				= $Global_TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisName 					= Get_Lang_Selection($thisNameCh, $thisNameEn);
			$thisNumOfNextLevelTopic 	= count($thisLinkedTopicIDArr);
			
			if ($thisLevel > $MaxLevel || !in_array($thisLevel, $TopicLevelArr))
			{
				### skip the display of topic if it is not selected for display
				$x .= $this->Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $SubjectID, $RubricsID, $IsFirst, $NumOfRubricsItem, $thisLinkedTopicIDArr);
				$IsFirst = 0;
				continue;
			}
				
				
			### Style
			$border_top = ($IsFirst == 1)? 'border_top' : '';	
			$first_border_left = ($ShowSubjectColumn == true)? 'border_left' : '';
			
			### Add empty cells before the title
			/*
			if ($thisLevel != $FirstTopicLevel && $thisLevel != $LastTopicLevel && $TopicDisplayIndexArr[$thisLevel] > 0)
			{
				for ($i=0; $i<$numOfTopicLevel; $i++)
				{
					if ($TopicLevelArr[$i] >= $thisLevel)
						continue;
					
					$this_border_left = ($i==0)? $first_border_left : 'border_left';
					$x .= '<td class="'.$this_border_left.' '.$border_top.'">&nbsp;</td>'."\n";
					
				}
			}
			*/
			
			
			### Topic Name Display
			$this_border_left = ($thisLevel == $FirstTopicLevel)? $first_border_left : 'border_left';
			
			# Reset Counter if starting a new level
			if ($thisLevel < $LastTopicLevel)
				$this->Reset_Topic_Display_Index($thisLevel + 1);
				
			$TopicDisplayIndexArr[$thisLevel]++;
			$thisIndex = $this->Get_Topic_Display_Index($SubjectID, $thisLevel);
			
			$thisRowSpan = $lreportcard->Get_Number_Of_Last_Level_Topic($thisLinkedTopicIDArr, $LastTopicLevel, $Global_TopicInfoArr, $IncludeEmptyNonLastLevel=1, $TopicLevelArr);
			$thisRowSpan = $thisRowSpan ? $thisRowSpan : "";
			
			$x .= '<td class="'.$this_border_left.' '.$border_top.'" style="vertical-align:top;" rowspan="'.$thisRowSpan.'">'."\n";
				$x .= '<table cellpadding="0" cellspacing="0" style="width:100%;">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td style="vertical-align:top;width:1%;white-space:nowrap;">'.$thisIndex.'</td>'."\n";
						$x .= '<td style="vertical-align:top;">'.$thisName.'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			$x .= '</td>'."\n";
			
			
			### Add empty cells after the title
			if ($thisNumOfNextLevelTopic == 0 || $thisLevel == $LastTopicLevel)
			{
				for ($i=0; $i<$numOfTopicLevel; $i++)
				{
					if ($TopicLevelArr[$i] <= $thisLevel)
						continue;
						
					$x .= '<td class="border_left '.$border_top.'">&nbsp;</td>'."\n";
				}
			}
			
			if ($thisNumOfNextLevelTopic > 0 && $thisLevel != $LastTopicLevel)
			{
				// Next line to display the Last Topic Level Info
				/*
				if ($thisLevel == $SecondLastTopicLevel)
				{
						# Empty cells for "Result" if the topic is not the last level and without any last level topic mapping
						$x .= $this->Get_Empty_Mark_Row($Global_ReportInfoArr['BasicInfo'][0]['ReportType'], $MarkDisplayOption, $border_top, $numOfModule, $NumOfRubricsItem);
					$x .= '</tr>'."\n";
					
					$IsFirst = 0;
				}
				*/
					
				$x .= $this->Get_Subject_Topic_Mark_Row($ReportID, $StudentID, $SubjectID, $RubricsID, $IsFirst, $NumOfRubricsItem, $thisLinkedTopicIDArr);
			}
			else
			{
				if ($thisLevel == $LastTopicLevel)
				{
					# Remarks
					if ($MarksheetRemarksDisplay)
					{
						$thisModuleID = $Global_ReportInfoArr['ModuleInfo'][0]['ModuleID'];
						
						$thisStudentMarksheetRemarks = trim($Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['Remarks']);
						$thisStudentMarksheetRemarks = ($thisStudentMarksheetRemarks=='')? $this->EmptySymbol : $thisStudentMarksheetRemarks;
						$x .= '<td class="'.$border_top.' border_left">'.$thisStudentMarksheetRemarks.'</td>'."\n";
					}
				
					# Student Topic Result
					if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'T')
					{
						### Term Report
						$thisModuleID = $Global_ReportInfoArr['ModuleInfo'][0]['ModuleID'];
						$thisStudentRubricsItemID = $Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
						
						foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
						{
							$thisDisplay = ($thisStudentRubricsItemID == $thisRubricsItemID)? $lreportcard_ui->Get_Tick_Image() : "&nbsp;";
							$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
						}
					}
					else if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'W')
					{
						### Consolidated Report
						if ($MarkDisplayOption == 'ModuleBased')
						{
							### Display Mark of Different Modules
							for ($i=0; $i<$numOfModule; $i++)
							{
								$thisModuleID 	= $Global_ReportInfoArr['ModuleInfo'][$i]['ModuleID'];
								
								$thisIsStudy = isset($StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$SubjectID][$thisTopicID]);
								if ($thisIsStudy)
								{
									$thisRubricsItemID = $Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
									
									if ($thisRubricsItemID > 0)
									{
										# Display Mark Name
										$thisRubricsItemNameEn = $Global_SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$thisRubricsItemID]['LevelNameEn'];
										$thisRubricsItemNameCh = $Global_SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$thisRubricsItemID]['LevelNameCh'];
										$thisDisplay = Get_Lang_Selection($thisRubricsItemNameCh, $thisRubricsItemNameEn);
									}
									else
									{
										# Not yet input mark
										$thisDisplay = '&nbsp;';
									}
								}
								else
								{
									# No need Study
									$thisDisplay = '&nbsp;';
								}
								
								$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
							}
						}
						else if ($MarkDisplayOption == 'Consolidated_%')
						{
							### Display Consolidated Mark Info
							// $Global_TopicStatisticsArr[$StudentID][$SubjectID][$TopicID]['NumOfStudiedModule'] = value;
							// $Global_TopicStatisticsArr[$StudentID][$SubjectID][$TopicID]['MarkInfoArr'][$RubricsItemID]['Count'] = value;
							$thisNumOfStudiedModule = $Global_TopicStatisticsArr[$StudentID][$SubjectID][$thisTopicID]['NumOfStudiedModule'];
							
							foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
							{
								$thisRubricsItemCount = $Global_TopicStatisticsArr[$StudentID][$SubjectID][$thisTopicID]['MarkInfoArr'][$thisRubricsItemID]['Count'];
								if ($thisRubricsItemCount == 0 || $thisNumOfStudiedModule == 0)
								{
									$thisDisplay = '&nbsp;';
								}
								else
								{
									$thisDisplay = my_round(($thisRubricsItemCount / $thisNumOfStudiedModule) * 100, 1);
									$thisDisplay .= '%';
								}
								$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
							}
						}
						else if ($MarkDisplayOption == 'Consolidated_Fraction')
						{
							// $Global_TopicStatisticsArr[$StudentID][$SubjectID][$TopicID]['NumOfStudiedModule'] = value;
							// $Global_TopicStatisticsArr[$StudentID][$SubjectID][$TopicID]['MarkInfoArr'][$RubricsItemID]['Count'] = value;
							$thisNumOfStudiedModule = $Global_TopicStatisticsArr[$StudentID][$SubjectID][$thisTopicID]['NumOfStudiedModule'];
							
							foreach ((array)$MarkTableRubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
							{
								$thisRubricsItemCount = $Global_TopicStatisticsArr[$StudentID][$SubjectID][$thisTopicID]['MarkInfoArr'][$thisRubricsItemID]['Count'];
								if ($thisRubricsItemCount == 0 || $thisNumOfStudiedModule == 0)
								{
									$thisDisplay = '&nbsp;';
								}
								else
								{
									$thisDisplay = $thisRubricsItemCount.'/'.$thisNumOfStudiedModule;
								}
								$x .= '<td class="border_left '.$border_top.'" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
							}
						}
					}
					
					# Remarks
//					if ($MarksheetRemarksDisplay)
//					{
//						$thisStudentMarksheetRemarks = trim($Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['Remarks']);
//						$thisStudentMarksheetRemarks = ($thisStudentMarksheetRemarks=='')? $this->EmptySymbol : $thisStudentMarksheetRemarks;
//						$x .= '<td class="'.$border_top.' border_left">'.$thisStudentMarksheetRemarks.'</td>'."\n";
//					}
				}
				else
				{
					# Empty cells for "Result" if the topic is not the last level and without any last level topic mapping
					$x .= $this->Get_Empty_Mark_Row($Global_ReportInfoArr['BasicInfo'][0]['ReportType'], $MarkDisplayOption, $border_top, $numOfModule, $NumOfRubricsItem, $MarksheetRemarksDisplay);
				}
				
				$x .= '</tr>'."\n";
			}
			$IsFirst = 0;
			
		}	// End of foreach ($SubjectTopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		
		return $x;
	}
	
	function Get_Performance_Mark_Table($ReportID, $StudentID, $SubjectID)
	{
		global $Global_DisplaySettingsArr, $Global_SubjectRubricsInfoArr, $Global_ReportInfoArr, $Global_SubjectInfoArr, $Global_TopicInfoArr, $Global_StudentMarksheetScoreArr;
		global $eRC_Rubrics_ConfigArr;
		global $lreportcard_topic, $lreportcard_ui;
		
		$YearID					= $Global_DisplaySettingsArr['YearID'];
		$SubjectDisplay 		= $Global_DisplaySettingsArr['SubjectDisplay'];
		$ReportModuleIDArr		= Get_Array_By_Key($Global_ReportInfoArr['ModuleInfo'], 'ModuleID');
		$SubjectGroupDisplay 	= $Global_DisplaySettingsArr['SubjectGroupDisplay'];
		$ShowSubjectColumn 		= ($SubjectDisplay != 'Hidden' || $SubjectGroupDisplay == 1);
		
		$TmpArr = array_keys((array)$Global_SubjectRubricsInfoArr[$YearID][$SubjectID]);
		$RubricsID = $TmpArr[0];
		
		if ($RubricsID != '' || $RubricsID != 0)
		{
			$RubricsInfoArr = $Global_SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID];
			$numOfRubricsItem = count($RubricsInfoArr);
			
			
			/*****Last Page header*****/
			$PageBreak_Style= 'style="page-break-after:always;"';
			$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" valign="top" class="">'."\n";
				$x .='<tr>';
					$x .='<td>';
					
					### Report Title
					$x .= '<div style="vertical-align:top;width:100%;">'."\n";
						$x .= $this->Get_Title_Table()."\n";
					$x .= '</div>'."\n";
					
					# Empty Row
					$x .= $lreportcard_ui->Get_Empty_Image(5);
					
					### Header Table
					$x .= '<div style="vertical-align:top;width:100%;">'."\n";
						$x .= $this->Get_Header_Info_Table($StudentID)."\n";
					$x .= '</div>'."\n";
					
					# Empty Row
					$x .= $lreportcard_ui->Get_Empty_Image(10);
					
					$x .='</td>';
				$x .='</tr>';
			$x .= '</table>';
			/*****End Last Page header*****/
			
			
			
			$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" valign="top" class="report_border">'."\n";
				# Subject
				$x .= '<col style="text-align:left;vertical-align:top;width:15%;" />'."\n";
					
				# Subject Topic
				$x .= '<col style="text-align:left;vertical-align:top;" />'."\n";
				
				# Rubrics Items
				for ($i=0; $i<$numOfRubricsItem; $i++)
					$x .= '<col style="text-align:center;vertical-align:top;width:4%;" />'."\n";
					
				### Header
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						# Subject
						$x .= '<td style="text-align:center;">&nbsp;</td>';
						
						# Topic Level Title
						$x .= '<td class="border_left" style="text-align:center;">&nbsp;</td>';
						
						# Rubrics Item Title
						foreach ((array)$RubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
						{
							$thisItemName = Get_Lang_Selection($thisRubricsItemInfoArr['LevelNameCh'], $thisRubricsItemInfoArr['LevelNameEn']);
							$x .= '<td class="border_left" style="text-align:center;">'.$thisItemName.'</td>';
						}
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				### Content
				$x .= '<tbody>'."\n";
					### Get Report Subject Topic Display Tree
					$thisTopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID, '', $eRC_Rubrics_ConfigArr['MaxLevel'], '', '', '', $ReturnAsso=0);
					$thisNumOfTopic = count($thisTopicInfoArr);
					$thisNumOfRow = $thisNumOfTopic;
					
					# Subject Name
					$thisSubjectDisplayArr = array();
					if ($SubjectDisplay != 'Hidden')
					{
						$thisSubjectInfoArr = $Global_SubjectInfoArr[$SubjectID];
						
						switch ($SubjectDisplay)
						{
							case 'Desc':
								$thisSubjectDisplayArr[] = Get_Lang_Selection($thisSubjectInfoArr['CH_DES'], $thisSubjectInfoArr['EN_DES']);
							break;
							
							case 'Abbr':
							$thisSubjectDisplayArr[] = Get_Lang_Selection($thisSubjectInfoArr['CH_ABBR'], $thisSubjectInfoArr['EN_ABBR']);
							break;
							
							case 'ShortForm':
								$thisSubjectDisplayArr[] = Get_Lang_Selection($thisSubjectInfoArr['CH_SNAME'], $thisSubjectInfoArr['EN_SNAME']);
							break;
						}
					}
					
					# Subject Group Name
					if ($SubjectGroupDisplay == 1)
					{
						$thisYearTermID = $Global_ReportInfoArr['ModuleInfo'][0]['YearTermID'];
						$thisSubjectGroupID = $this->Get_Student_Studying_Subject_Group($thisYearTermID, $StudentID, $SubjectID);
						
						$thisSGObj = new subject_term_class($thisSubjectGroupID);
						$thisSubjectDisplayArr[] = $thisSGObj->Get_Class_Title();
					}
					
					$thisSubjectDisplay = implode('<br />', $thisSubjectDisplayArr);
					$thisSubjectRowSpan = $thisNumOfRow ? $thisNumOfRow : "";
					
					$x .= '<tr>'."\n";
					   $x .= '<td class="border_top" rowspan="'.$thisSubjectRowSpan.'" style="vertical-align:top;">'.$thisSubjectDisplay.'</td>';
						
					for ($i=0; $i<$thisNumOfTopic; $i++)
					{
						$thisTopicID = $thisTopicInfoArr[$i]['TopicID'];
						
						$thisNameEn = $Global_TopicInfoArr[$thisTopicID]['TopicNameEn'];
						$thisNameCh = $Global_TopicInfoArr[$thisTopicID]['TopicNameCh'];
						$thisName 	= Get_Lang_Selection($thisNameCh, $thisNameEn);
						
						$thisModuleID = $Global_ReportInfoArr['ModuleInfo'][0]['ModuleID'];
						$thisStudentRubricsItemID = $Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
						
						if ($i > 0)
							$x .= '<tr>'."\n";
							
							$x .= '<td class="border_left border_top" style="vertical-align:top;">'."\n";
								$x .= $thisName."\n";
							$x .= '</td>'."\n";
							
						foreach ((array)$RubricsInfoArr as $thisRubricsItemID => $thisRubricsItemInfoArr)
						{
							$thisDisplay = ($thisStudentRubricsItemID == $thisRubricsItemID)? $lreportcard_ui->Get_Tick_Image() : "&nbsp;";
							$x .= '<td class="border_left border_top" style="text-align:center;vertical-align:top;">'.$thisDisplay.'</td>';
						}
						
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	function Count_Number_Of_Row_Of_The_Subject($SubjectTopicMappingArr, $LastTopicLevel, $TotalNumOfRow=0)
	{
		global $eRC_Rubrics_ConfigArr, $Global_TopicInfoArr, $Global_DisplaySettingsArr;
		
		$TopicLevelArr	= $Global_DisplaySettingsArr['TopicLevelArr'];
		$numOfDisplayLevel = count($TopicLevelArr);
		
		foreach ((array)$SubjectTopicMappingArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisLevel = $Global_TopicInfoArr[$thisTopicID]['Level'];
			$numOfLinkedTopic = count($thisLinkedTopicIDArr);
			
			if ($thisLevel > $LastTopicLevel)
				continue;
				
			if (($numOfLinkedTopic == 0 || $thisLevel == $LastTopicLevel) && in_array($thisLevel, $TopicLevelArr))
			{
				// No linked Topic or Last Level Topic => a new row
				$TotalNumOfRow++;
			}
			else if ($numOfLinkedTopic > 0 && $thisLevel < $LastTopicLevel && in_array($thisLevel, $TopicLevelArr))
			{
				// Have Linked Topics and it is not the Last Level => keep looping the next Topic Level
				$TotalNumOfRow = $this->Count_Number_Of_Row_Of_The_Subject($thisLinkedTopicIDArr, $LastTopicLevel, $TotalNumOfRow);
			}
			else if (!in_array($thisLevel, $TopicLevelArr) && $thisLevel < $LastTopicLevel)
			{
				// If the Topic Level is not included but not yet reaching the Last Level, do not count this Level and keep looping the next Topic Level
				$TotalNumOfRow = $this->Count_Number_Of_Row_Of_The_Subject($thisLinkedTopicIDArr, $LastTopicLevel, $TotalNumOfRow);
			}
		}
		
		return $TotalNumOfRow;
	}
	
	function Get_Topic_Display_Index($SubjectID, $TopicLevel)
	{
		global $TopicDisplayIndexArr;
		
		$IndexDisplay = '';
		for ($i=1; $i<=$TopicLevel; $i++)
		{
			$thisLevelDisplayedTopic = $TopicDisplayIndexArr[$i];
			if ($thisLevelDisplayedTopic > 0)
				$IndexDisplay .= $thisLevelDisplayedTopic.'.';
		}
		
		return $IndexDisplay;
	}
	
	function Reset_Topic_Display_Index($TopicLevel)
	{
		global $TopicDisplayIndexArr, $eRC_Rubrics_ConfigArr;
		
		$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		for ($i=1; $i<=$MaxLevel; $i++)
		{
			if ($i >= $TopicLevel)
				$TopicDisplayIndexArr[$i] = 0;
		}
	}
	
	function Get_Empty_Mark_Row($ReportType, $MarkDisplayOption, $border_top, $numOfModule, $NumOfRubricsItem, $MarksheetRemarksDisplay)
	{
		$x = '';
		
		if ($ReportType == 'W' && $MarkDisplayOption == 'ModuleBased')
		{
			# Loop Modules
			for ($i=0; $i<$numOfModule; $i++)
			{
				$x .= '<td class="'.$border_top.' border_left no_mark_td">&nbsp;</td>'."\n";
			}
		}
		else
		{
			# Loop Rubrics Items
			for ($i=0; $i<$NumOfRubricsItem; $i++)
			{
				$x .= '<td class="'.$border_top.' border_left no_mark_td">&nbsp;</td>'."\n";
			}
		}
		
		if ($MarksheetRemarksDisplay)
			$x .= '<td class="'.$border_top.' border_left no_mark_td">&nbsp;</td>'."\n";
		
		return $x;
	}
	
	/*
	function Get_ECA_Table($ReportID, $StudentID)
	{
		global $PATH_WRT_ROOT, $Global_DisplaySettingsArr, $lreportcard_eca, $lreportcard_ui;
		global $Global_EcaCategoryInfoArr, $Global_EcaSubCategoryInfoArr, $Global_EcaCatSubCatItemMappingArr, $Global_StudentEcaMappingArr;		// ECA related Data from print.php
		
		$DispalyEcaCategoryIDArr = $Global_DisplaySettingsArr['ECACategoryIDArr'];
		
		$numOfEcaCategory = count($Global_EcaCategoryInfoArr);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" valign="top" class="report_border">'."\n";
			### Category Title
			$x .= '<tr>'."\n";
				$CatDisplayCount = 0;
				$FirstDisplayedCatID = '';
				foreach ($Global_EcaCategoryInfoArr as $thisEcaCategoryID => $thisEcaCategoryInfoArr)
				{
					if (!in_array($thisEcaCategoryID, $DispalyEcaCategoryIDArr))
						continue;
					
					$numOfSubCat = count($Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID]);
					$numOfColspan = $numOfSubCat * 2;	// Item Name and Item "Tick" for each SubCat
					
					if ($numOfSubCat == 0)
						continue;
					
					$thisCatNameEn = $Global_EcaCategoryInfoArr[$thisEcaCategoryID]['EcaCategoryNameEn'];
					$thisCatNameCh = $Global_EcaCategoryInfoArr[$thisEcaCategoryID]['EcaCategoryNameCh'];
					$thisCatName = Get_Lang_Selection($thisCatNameCh, $thisCatNameEn);
					
					$border_left = ($CatDisplayCount==0)? '' : 'border_left';
					$FirstDisplayedCatID = ($CatDisplayCount==0)? $thisEcaCategoryID : $FirstDisplayedCatID;
					$x .= '<td class="'.$border_left.'" colspan="'.$numOfColspan.'" style="text-align:center;">'.$thisCatName.'</td>'."\n";
					
					$CatDisplayCount++;
					
				}
			$x .= '</tr>'."\n";
			
			### Sub-Category Title
			$x .= '<tr>'."\n";
				$isFirstSubCat = 1;
				foreach ($Global_EcaCategoryInfoArr as $thisEcaCategoryID => $thisEcaCategoryInfoArr)
				{
					$thisNumOfSubCat = count((array)$Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID]);
					
					if (!in_array($thisEcaCategoryID, $DispalyEcaCategoryIDArr))
						continue;
					
					foreach((array)$Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID] as $thisEcaSubCategoryID => $thisSubCategoryInfoArr)
					{
						$thisSubCatNameEn = $Global_EcaSubCategoryInfoArr[$thisEcaCategoryID][$thisEcaSubCategoryID]['EcaSubCategoryNameEn'];
						$thisSubCatNameCh = $Global_EcaSubCategoryInfoArr[$thisEcaCategoryID][$thisEcaSubCategoryID]['EcaSubCategoryNameCh'];
						$thisSubCatName = Get_Lang_Selection($thisSubCatNameCh, $thisSubCatNameEn);
						
						$border_left = ($isFirstSubCat==1)? '' : 'border_left';
						
						// Do not show Sub Cat name if there are only 1 Sub Cat
						if ($thisNumOfSubCat > 1)
							$x .= '<td colspan="2" class="border_top '.$border_left.'" style="text-align:center;">'.$thisSubCatName.'</td>'."\n";
						else
							$x .= '<td colspan="2" class="'.$border_left.'" style="text-align:center;">&nbsp;</td>'."\n";
						
						$isFirstSubCat = 0;
					}
					
				}
			$x .= '</tr>'."\n";
			
			### Items
			// $SubCatItemDisplayArr[$SubCatID][0,1,2] = Td HTML Code
			$SubCatItemDisplayArr = array();
			$SubCatItemCountArr = array();
			
			# Generate the Item Td HTML first
			$isFirstSubCat = 1;
			foreach ($Global_EcaCategoryInfoArr as $thisEcaCategoryID => $thisEcaCategoryInfoArr)
			{
				if (!in_array($thisEcaCategoryID, $DispalyEcaCategoryIDArr))
					continue;
				
				foreach((array)$Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID] as $thisEcaSubCategoryID => $thisSubCategoryInfoArr)
				{
					$SubCatItemCountArr[$thisEcaSubCategoryID] = count($thisSubCategoryInfoArr);
					
					foreach((array)$thisSubCategoryInfoArr as $thisEcaItemID => $thisItemInfoArr)
					{
						$thisEcaItemNameEn = $thisItemInfoArr['EcaItemNameEn'];
						$thisEcaItemNameCh = $thisItemInfoArr['EcaItemNameCh'];
						$thisEcaItemName = Get_Lang_Selection($thisEcaItemNameCh, $thisEcaItemNameEn);
						
						$border_left = ($isFirstSubCat==1)? '' : 'border_left';
						
						# Check if Student has enrolled in this ECA Item
						$thisHasEnrolled = in_array($thisEcaItemID, (array)$Global_StudentEcaMappingArr[$StudentID]);
						$thisDisplay = ($thisHasEnrolled)? $lreportcard_ui->Get_Tick_Image() : '&nbsp;';
						
						$thisItemHTML = '';
						$thisItemHTML .= '<td class="border_top '.$border_left.'" style="text-align:center;vertical-align:top;">'.$thisEcaItemName.'</td>'."\n";
						$thisItemHTML .= '<td class="border_top border_left" style="text-align:center;width:20px;">'.$thisDisplay.'</td>'."\n";
						
						(array)$SubCatItemDisplayArr[$thisEcaSubCategoryID][] = $thisItemHTML;
					}
					
					$isFirstSubCat = 0;
				}	
			}
			
			# Re-order the Item HTML Display Order
			$MaxNumOfItem = max($SubCatItemCountArr);
			for ($i=0; $i<$MaxNumOfItem; $i++)
			{
				$FirstItem = 1;
				$x .= '<tr>'."\n";
					// Loop Cat
					foreach ($Global_EcaCategoryInfoArr as $thisEcaCategoryID => $thisEcaCategoryInfoArr)
					{
						if (!in_array($thisEcaCategoryID, $DispalyEcaCategoryIDArr))
							continue;
						
						// Loop SubCat
						foreach((array)$Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID] as $thisEcaSubCategoryID => $thisSubCategoryInfoArr)	
						{
							// Display the Item Info
							if ($SubCatItemDisplayArr[$thisEcaSubCategoryID][$i] == '')
							{
								// insert empty cells
								$border_left = ($thisEcaCategoryID==$FirstDisplayedCatID && $FirstItem==1)? '' : 'border_left';
								$x .= '<td class="border_top '.$border_left.'">&nbsp;</td>'."\n";
								$x .= '<td class="border_top border_left">&nbsp;</td>'."\n";
							}
							else
							{
								$x .= $SubCatItemDisplayArr[$thisEcaSubCategoryID][$i];
							}
							
							$FirstItem = 0;
						}
					}
				$x .= '</tr>'."\n";
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	*/
	
	function Get_ECA_Table($ReportID, $StudentID)
	{
		global $PATH_WRT_ROOT, $Global_DisplaySettingsArr, $lreportcard_eca, $lreportcard_ui;
		global $Global_EcaCategoryInfoArr, $Global_EcaSubCategoryInfoArr, $Global_EcaCatSubCatItemMappingArr, $Global_StudentEcaMappingArr;		// ECA related Data from print.php
		
		$DispalyEcaCategoryIDArr = $Global_DisplaySettingsArr['ECACategoryIDArr'];
		
		$numOfEcaCategory = count($Global_EcaCategoryInfoArr);
		$numOfItemPerRow = 4;
		
		$x = '';
		$x .= '<table border="0" cellpadding="3" cellspacing="0" class="report_border" style="width:100%;vertical-align:top;">'."\n";
			### Category Title
			$isFirstCategory = true;
			foreach ($Global_EcaCategoryInfoArr as $thisEcaCategoryID => $thisEcaCategoryInfoArr)
			{
				if (!in_array($thisEcaCategoryID, (array)$DispalyEcaCategoryIDArr))
					continue;
				
				$numOfSubCat = count($Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID]);
				
				if ($numOfSubCat == 0)
					continue;
					
				$thisCatNameEn = $Global_EcaCategoryInfoArr[$thisEcaCategoryID]['EcaCategoryNameEn'];
				$thisCatNameCh = $Global_EcaCategoryInfoArr[$thisEcaCategoryID]['EcaCategoryNameCh'];
				$thisCatName = Get_Lang_Selection($thisCatNameCh, $thisCatNameEn);
					
				$numOfColspan = $numOfItemPerRow;
				$border_top = ($isFirstCategory)? '' : 'border_top';
				
				$x .= '<tr>'."\n";
					$x .= '<td class="'.$border_top.'" colspan="'.$numOfColspan.'" style="text-align:center;">'.$thisCatName.'</td>'."\n";
				$x .= '</tr>'."\n";
				
				$isFirstCategory = false;
				
				foreach((array)$Global_EcaCatSubCatItemMappingArr[$thisEcaCategoryID] as $thisEcaSubCategoryID => $thisSubCategoryInfoArr)
				{
					$thisSubCatNameEn = $Global_EcaSubCategoryInfoArr[$thisEcaCategoryID][$thisEcaSubCategoryID]['EcaSubCategoryNameEn'];
					$thisSubCatNameCh = $Global_EcaSubCategoryInfoArr[$thisEcaCategoryID][$thisEcaSubCategoryID]['EcaSubCategoryNameCh'];
					$thisSubCatName = Get_Lang_Selection($thisSubCatNameCh, $thisSubCatNameEn);
					
					$x .= '<tr>'."\n";
						$x .= '<td class="border_top" colspan="'.$numOfColspan.'">'.$thisSubCatName.'</td>'."\n";
					$x .= '</tr>'."\n";
						
					$ItemCounter = 0;
					foreach((array)$thisSubCategoryInfoArr as $thisEcaItemID => $thisItemInfoArr)
					{
						$thisEcaItemNameEn = $thisItemInfoArr['EcaItemNameEn'];
						$thisEcaItemNameCh = $thisItemInfoArr['EcaItemNameCh'];
						$thisEcaItemName = Get_Lang_Selection($thisEcaItemNameCh, $thisEcaItemNameEn);
						
						$thisHasEnrolled = in_array($thisEcaItemID, (array)$Global_StudentEcaMappingArr[$StudentID]);
						$thisDisplay = ($thisHasEnrolled)? $lreportcard_ui->Get_Tick_Image() : $lreportcard_ui->Get_Empty_Image('10px');
						
						if ($ItemCounter % $numOfItemPerRow == 0)
						{
							$x .= '<tr>'."\n";
						}
							
							$x .= '<td>'."\n";
								$x .= '<table border="0" cellpadding="3" cellspacing="3" style="width:100%;height:100%;vertical-align:top;">'."\n";
									$x .= '<tr>'."\n";
										$x .= '<td style="width:15px;height:100%;text-align:left;vertical-align:top;">'."\n";
											$x .= '<div class="border_top border_bottom border_left border_right" style="width:15px;height:15px;text-align:center;vertical-align:top;">'."\n";
												$x .= $thisDisplay;
											$x .= '</td>'."\n"; 
										$x .= '</td>'."\n";
										$x .= '<td>'.$thisEcaItemName.'</td>'."\n";
									$x .= '</tr>'."\n";
								$x .= '</table>'."\n";
							$x .= '</td>'."\n";
							
						if ($ItemCounter % $numOfItemPerRow == $numOfItemPerRow - 1)
							$x .= '</tr>'."\n";
							
						$ItemCounter++;
					}
				}
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_OtherInfo_Table($ReportID, $StudentID)
	{
		global $Global_DisplaySettingsArr, $Global_OtherInfoCategoryArr, $Global_OtherInfoItemArr, $Global_StudentOtherInfoArr, $lreportcard_ui;
		
		$OtherInfoArr = $Global_DisplaySettingsArr['OtherInfoItemIDArr'];
		foreach ((array)$OtherInfoArr as $thisCategoryID => $thisItemIDArr)
		{
			$thisCategoryCode = $Global_OtherInfoCategoryArr[$thisCategoryID]['CategoryCode'];
			$thisCategoryName = $lreportcard_ui->Get_Customized_Lang('OtherInfoCategory', $thisCategoryCode);
			
			
			if ($thisCategoryCode == 'award')
			{
				
				### Award
				$x .= '<table border="0" cellpadding="3" cellspacing="0" class="report_border" style="width:100%;height:60px;vertical-align:top;">'."\n";
					# Title
					$x .= '<tr><td style="height:1%;vertical-align:top;">'.$thisCategoryName.':</td></tr>'."\n";
					
					# Info
					foreach ((array)$thisItemIDArr as $thisItemID => $thisDisplay)
					{
						$thisStudentOtherInfoArr = $Global_StudentOtherInfoArr[$StudentID][$thisCategoryID][$thisItemID];
						
						foreach((array)$thisStudentOtherInfoArr as $thisRecordID => $thisInfoArr)
						{
							$thisDisplay = $thisInfoArr['Information'];
							$x .= '<tr><td style="vertical-align:top;">'.$thisDisplay.'</td></tr>'."\n";
						}
					}
				$x .= '</table>'."\n";
			}
			else if ($thisCategoryCode == 'attendance')
			{
				### Attendance
				$numOfItem = count((array)$thisItemIDArr);
				$numOfRowspan = $numOfItem + 1;
				$numOfRowspan = $numOfRowspan ? $numOfRowspan : "";
				
				$thisCategoryName = $lreportcard_ui->Get_Customized_Lang('General', 'ThisTerm');
				
				$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" valign="top" class="report_border">'."\n";
					# Title
					$x .= '<tr><td rowspan="'.$numOfRowspan.'" colspan="4" style="vertical-align:top;">'.$thisCategoryName.':</td></tr>'."\n";
					
					# Info
					foreach ((array)$thisItemIDArr as $thisItemID => $thisDisplay)
					{
						# Item Name
						$thisItemCode = $Global_OtherInfoItemArr[$thisItemID]['ItemCode'];
						$thisItemName = $lreportcard_ui->Get_Customized_Lang('OtherInfoItem', $thisItemCode);
						$thisDayName = $lreportcard_ui->Get_Customized_Lang('General', 'Day');
						
						# Show Student Info
						$x .= '<tr>'."\n";
							$x .= '<td style="vertical-align:top;width:30px;">'.$thisItemName.'</td>'."\n";
							
							$thisStudentOtherInfoArr = $Global_StudentOtherInfoArr[$StudentID][$thisCategoryID][$thisItemID];
							$numOfRecord = count($thisStudentOtherInfoArr);
							if ($numOfRecord == 0)
							{
								$thisDisplay = 0;
							}
							else
							{
								foreach((array)$thisStudentOtherInfoArr as $thisRecordID => $thisInfoArr)
								{
									$thisDisplay = $thisInfoArr['Information'];
									$thisDisplay = ($thisDisplay=='')? 0 : $thisDisplay;
								}
							}
							$x .= '<td style="vertical-align:top;width:30px;">'.$thisDisplay.'</td>'."\n";
							
							$x .= '<td style="vertical-align:top;width:81%;">'.$thisDayName.'</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</table>'."\n";
			}
			
		}
		
		return $x;
	}
	
	function Get_ClassTeacherComment_Table($ReportID, $StudentID)
	{
		global $Global_ReportInfoArr, $Global_StudentClassTeacherCommentArr, $lreportcard_ui;
		
		$StudentComment = $Global_StudentClassTeacherCommentArr[$StudentID]['Comment'];
		
		if ($Global_ReportInfoArr['BasicInfo'][0]['ReportType'] == 'T')
			$CommentName = $lreportcard_ui->Get_Customized_Lang('General', 'Remarks');
		else
			$CommentName = $lreportcard_ui->Get_Customized_Lang('General', 'ClassTeacherComment');
			
		$x = '';
		$x .= '<table border="0" cellpadding="3" cellspacing="0" class="report_border" style="width:100%;height:80px;vertical-align:top;">'."\n";
			$x .= '<tr><td style="vertical-align:top;height:1%;">'.$CommentName.':</td></tr>'."\n";
			$x .= '<tr><td style="vertical-align:top;">'.nl2br($StudentComment).'</td></tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Signature_Table($ReportID, $StudentID)
	{
		global $Global_DisplaySettingsArr, $lreportcard_ui, $Lang;
		
		$SignatureFieldArr = $Global_DisplaySettingsArr['SignatureFieldArr'];
		$numOfField = count($SignatureFieldArr);
		
		$x = '';
		if ($Global_DisplaySettingsArr['ReportType'] == 'GeneralReport')
		{
			if ($numOfField > 0)
			{
				$ColumnWidth = floor(100 / $numOfField);
				
				$x .= '<table border="0" cellpadding="3" cellspacing="0" class="report_border" style="width:100%;vertical-align:top;">'."\n";
					### Title
					$x .= '<tr>'."\n";
						for ($i=0; $i<$numOfField; $i++)
						{
							$thisField = $SignatureFieldArr[$i];
							$thisFieldDisplay = $lreportcard_ui->Get_Customized_Lang('Signature', $thisField);
							
							$border_left = ($i==0)? '' : 'border_left';
							$x .= '<td class="'.$border_left.'" style="text-align:center;width:'.$ColumnWidth.'%;">'.$thisFieldDisplay.'</td>'."\n";
						}
					$x .= '</tr>'."\n";
					
					### Empty Space
					$x .= '<tr>'."\n";
						for ($i=0; $i<$numOfField; $i++)
						{
							$border_left = ($i==0)? '' : 'border_left';
							$x .= '<td class="border_top '.$border_left.'" style="height:80px;">&nbsp;</td>'."\n";
						}
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			}
		}
		else if ($Global_DisplaySettingsArr['ReportType'] == 'TherapyReport' || $Global_DisplaySettingsArr['ReportType'] == 'LearningReport')
		{
			$RemarksArr = $Lang['eRC_Rubrics']['Template']['RemarksArr'];
			
			$x .= '<table border="0" cellpadding="3" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
				$x .= '<tr><td colspan="2">'.$lreportcard_ui->Get_Customized_Lang('General', 'Remarks').':</td></tr>'."\n";
				$x .= '<tr><td colspan="2">'.$lreportcard_ui->Get_Empty_Image('2px').'</td></tr>'."\n";
				$x .= '<tr>'."\n";
					# Remarks Table
					$x .= '<td style="width:60%;">'."\n";
						$x .= '<table border="0" cellpadding="1" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
							foreach((array)$RemarksArr as $thisKey => $thisRemarks)
							{
								$x .= '<tr><td>'.$thisRemarks.'</td></tr>'."\n";
							}
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
					
					# Signature Table
					$thisTeacherSignatureTitle = ($Global_DisplaySettingsArr['ReportType'] == 'TherapyReport')? $lreportcard_ui->Get_Customized_Lang('Signature', 'LinguisticsTeacher') : $lreportcard_ui->Get_Customized_Lang('Signature', 'TeacherInCharge');
					$x .= '<td style="width:40%;">'."\n";
						$x .= '<table border="0" cellpadding="1" cellspacing="0" style="width:100%;vertical-align:top;">'."\n";
							$x .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
							
							# Teacher
							if (in_array('ClassTeacher', (array)$SignatureFieldArr))
							{
								$x .= '<tr>'."\n";
									$x .= '<td style="width:43%; text-align:right;">'.$thisTeacherSignatureTitle.'</td>'."\n";
									$x .= '<td> : </td>'."\n";
									$x .= '<td class="border_bottom" style="width:53%;">&nbsp;</td>'."\n";
								$x .= '</tr>'."\n";
							}
							else
							{
								$x .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
							}
							
							$x .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
							
							# Parent
							if (in_array('ClassTeacher', (array)$SignatureFieldArr))
							{
								$x .= '<tr>'."\n";
									$x .= '<td style="width:43%; text-align:right;">'.$lreportcard_ui->Get_Customized_Lang('Signature', 'Parent').'</td>'."\n";
									$x .= '<td> : </td>'."\n";
									$x .= '<td class="border_bottom" style="width:53%;">&nbsp;</td>'."\n";
								$x .= '</tr>'."\n";
							}
							else
							{
								$x .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
							}
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	function Get_Performance_SubjectID()
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		$libSubject = new subject();
		$SubjectInfoArr = $libSubject->Get_Subject_By_SubjectCode('BH');
		
		return $SubjectInfoArr[0]['RecordID'];
	}
}
?>