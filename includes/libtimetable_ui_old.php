<?php
include_once('libtimetable_old.php');
include_once('libinterface.php');

if (!defined("LIBTIMETABLE_UI_OLD_DEFINED"))         // Preprocessor directives
{
	define("LIBTIMETABLE_UI_OLD_DEFINED",true);
	
	class libtimetable_ui extends libtimetable {
		var $thickBoxWidth;
		var $thickBoxHeight;
		
		function libtimetable_ui() {
			parent:: libtimetable();
			
			$this->thickBoxWidth = 750;
			$this->thickBoxHeight = 450;
			$this->EmptyString = '---';
		}
		
		function Include_JS_CSS()
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN;
			
			$x = '
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
				
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
				';
				
			return $x;
		}
		
		function Get_Allocation_Filter_Table($PeriodID="", $CycleDays="", $SelectedSubjectID="", $SelectedSubjectGroup=array(), 
												$SelectedBuildingID="", $SelectedLocationLevelID="", $SelectedLocationID="", $DisplayOption=array())
		{
			global $Lang, $PATH_WRT_ROOT;
			
			# Acadermic Year Filter
			$thisOnchange = 'onchange="js_Update_Period_Cycle_Semester_Selection()"';
			$acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID", $thisOnchange, 0, 1);
			
			# Term Filter
			include_once("subject_class_mapping_ui.php");
			$subject_class_mapping_ui = new subject_class_mapping_ui();
			//$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID", '', '', "js_Update_Subject_Selection()");
			$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID", '', '', "MM_showHideLayers('SubjectGroupSelectionDiv','','hide');");
			
			# Cycle Days Filter
			include_once("libcycleperiods_ui.php");
			$libcycleperiods_ui = new libcycleperiods_ui();
			$cycleDaysSelection = $libcycleperiods_ui->getCycleDaysSelection("SelectedCycleDays","SelectedCycleDays", $CycleDays,'','',0);
			
			# Period Filter
			$periodSelection = $this->Get_Period_Selection();
			
			# Subject Filter
			include_once("subject_class_mapping_ui.php");
			$libsubject_ui = new subject_class_mapping_ui();
			$subjectSelection = $libsubject_ui->Get_Subject_Selection("SelectedSubjectID", $SelectedSubjectID, 
								"js_Show_Hide_Subject_Group_Selection_Div()", 0, $Lang['SysMgr']['SubjectClassMapping']['All']['Subject'], '', 
								"js_Show_Hide_Subject_Group_Selection_Div()");
								
			# Building Filter
			include_once("liblocation_ui.php");
			$liblocation_ui = new liblocation_ui();
			//$buildingSelection = $liblocation_ui->Get_Building_Selection($SelectedBuildingID, 'SelectedBuildingID', 
			//					"js_Change_Floor_Selection('SelectedBuildingID', 'FloorSelectionDiv', 'SelectedLocationLevelID', 'RoomSelectionDiv', 'SelectedLocationID', '')");
			$buildingSelection = $liblocation_ui->Get_Building_Selection(
									$SelectedBuildingID, 
									'SelectedBuildingID', 
									"js_Change_Floor_Selection(	'$PATH_WRT_ROOT',
																'SelectedBuildingID', 
																'',
																'',
																'FloorSelectionDiv',
																'SelectedLocationLevelID',
																'',
																'',
																'RoomSelectionDiv',
																'SelectedLocationID',
																''
																 )
									",
									$noFirst = 0,
									$WithOthersOpt = 0,
									$ParFirstTitle = $Lang['SysMgr']['Location']['All']['Building']
								);
								
			# Refresh Button
			include_once("libinterface.php");
			$libinterface = new interface_html();
			$refreshBtn = $libinterface->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "js_Reload_Timetable()", "RefreshBtn");
			
			$table = '';
			$table .= '<table id="FilterTable">';
				$table .= '<tr>';
					# Acadermic Year
					$table .= '<td style="vertical-align:top">'.$acadermicYearSelection.'</td>';
					# Term
					$table .= '<td style="vertical-align:top">';
						$table .= '<div id="TermSelectionDiv">';
							$table .= $termSelection;
						$table .= '</div>';
					$table .= '</td>';
					# Cycle Days
					$table .= '<td style="vertical-align:top">';
						$table .= '<div id="CycleSelectionDiv">';
							$table .= $cycleDaysSelection;
						$table .= '</div>';
					$table .= '</td>';
					# Period
					$table .= '<td style="vertical-align:top">';
						$table .= '<div id="PeriodSelectionDiv">';
							$table .= $periodSelection;
						$table .= '</div>';
					$table .= '</td>';
					# Subject
					$table .= '<td style="vertical-align:top;" align="left">';
						$table .= '<div id="SubjectSelectionDiv">';
							$table .= $subjectSelection;
						$table .= '</div>';
						$table .= '<div id="SubjectGroupSelectionDiv" class="selectbox_layer">aaa</div>';
					$table .= '</td>';
					/*
					# Subject Group
					$table .= '<td style="vertical-align:top">';
						$table .= '<div id="SubjectGroupSelectionDiv" style="display:none;"></div>';
					$table .= '</td>';
					*/
					# Refresh button
					$table .= '<td style="vertical-align:top">';
						$table .= $refreshBtn;
					$table .= '</td>';
				$table .= '</tr>';
			$table .= '</table>';
				
			$table .= '<table id="FilterTable2" align="left">';
				$table .= '<tr>';
					# Building
					$table .= '<td style="vertical-align:top">'.$buildingSelection.'</td>';
					# Floor
					$table .= '<td style="vertical-align:top">';
						$table .= '<div id="FloorSelectionDiv" style="display:none;"></div>';
					$table .= '</td>';
					# Room
					$table .= '<td style="vertical-align:top">';
						$table .= '<div id="RoomSelectionDiv" style="display:none;"></div>';
					$table .= '</td>';
				$table .= '</tr>';
			$table .= '</table>';
			
			return $table;
		}
		
		function Get_Subject_Group_Checkbox_Selection_Div($SubjectID, $YearTermID='')
		{
			global $Lang, $image_path, $LAYOUT_SKIN;
			include_once("subject_class_mapping_ui.php");
			include_once("libinterface.php");
			
			$libinterface = new interface_html();
			
			if ($YearTermID == '')
			{
				$YearTermArr = getCurrentAcademicYearAndYearTerm();
				$YearTermID = $YearTermArr['YearTermID'];
			}
			
			$libSCM = new subject_class_mapping();
			$SubjectGroupInfoArr = $libSCM->Get_Subject_Group_List($YearTermID,$SubjectID);
			$SubjectGroupListArr = $SubjectGroupInfoArr['SubjectGroupList'];
			$numOfSubjectGroup = count($SubjectGroupListArr);
			
			$div = '';
			$div .= '<div>';
				$div .= '<table>';
					# All Selection
					$div .= '<tr>';
						$div .= '<td style="text-align:left;vertical-align:top">';
							$div .= $libinterface->Get_Checkbox("SubjectGroupID_All", "SubjectGroupID[]", "", 1, "SubjectGroupChkBox", $Lang['General']['All'], "js_Select_All_None_Subject_Group()");
						$div .= '</td>';
					$div .= '</tr>';
					
					if ($numOfSubjectGroup > 0)
					{
						# Selection subject Group
						foreach ($SubjectGroupListArr as $thisSubjectGroupID => $thisSubjectGroupInfoArr)
						{
							$thisClassTitleEN = $thisSubjectGroupInfoArr['ClassTitleEN'];
							$thisClassTitleB5 = $thisSubjectGroupInfoArr['ClassTitleB5'];
							$thisSubjectGroupName = Get_Lang_Selection($thisClassTitleB5, $thisClassTitleEN);
							$thisCheckBoxID = "SubjectGroupID_".$thisSubjectGroupID;
							
							$div .= '<tr>';
								$div .= '<td style="text-align:left;vertical-align:top">';
									$div .= $libinterface->Get_Checkbox($thisCheckBoxID, "SubjectGroupID[]", $thisSubjectGroupID, 1, "SubjectGroupChkBox", $thisSubjectGroupName, "js_Unselect_All_Subject_Group('$thisCheckBoxID')");
								$div .= '</td>';
							$div .= '</tr>';
						}
					}
					
					$div .= '<tr>';
						$div .= '<td align="left" valign="top" class="dotline" height="2"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" height="2"></td>';
					$div .= '</tr>';
					$div .= '<tr>';
						$div .= '<td align="center" class="tabletext">';
							$div .= $libinterface->GET_BTN($Lang['Btn']['Close'], "button", "javascript: onClick=MM_showHideLayers('SubjectGroupSelectionDiv','','hide');");
						$div .= '</td>';
					$div .= '</tr>';
				$div .= '</table>';
			$div .= '</div>';
			
			return $div;
		}
		
		function Get_Timetable_Table($PeriodID, $CycleDays='', $CyclePeriodID='', $SubjectGroupIDDisplayArr=array(), $LocationID='', $DisplayOptionArr=array(), $isViewMode=0)
		{
			global $Lang;
			include_once("liblocation.php");
			include_once("subject_class_mapping.php");
			
			$libinterface = new interface_html();
			
			$PeriodArr = $this->Get_Period_Info($PeriodID);
			$numOfPeriod = count($PeriodArr);
			
			if ($CycleDays != '')
			{
				$numOfCycle = $CycleDays;
			}
			else
			{
				$CycleArr = $this->Get_Cycle_Info($CyclePeriodID);
				$numOfCycle = count($CycleArr);
				$CycleDays = $numOfCycle;
			}
			
			# Get Subject GroupInfo
			$libSCM = new subject_class_mapping();
			$SubjectGroupArr = $libSCM->Get_Subject_Group_Info();
			
			# Get Subject - SujectGroup Mapping
			$SubjectGroupMapping = $libSCM->Get_SubjectGroup_Subject_Mapping();
			
			# Get Room Allocation Info
			$libRoomAllocation = new Room_Allocation();
			$RoomAllocationArr = $libRoomAllocation->Get_All_Room_Allocation($PeriodID, $CycleDays, $SubjectGroupID=array(), $LocationID='');
			
			$liblocation = new liblocation();
			$LocationInfoArr = $liblocation->Get_Building_Floor_Room_Name_Arr();
			
			$table = '';
			$table .= '<table class="common_table_list" id="ContentTable">'."\n";
			
				if ($numOfPeriod == 0)
				{
					$table .= '<tr><td align="center">'.$Lang['SysMgr']['Timetable']['Warning']['NoPeriodSetUpYet'].'</td></tr>'."\n";
				}
				else if ($numOfCycle == 0)
				{
					$table .= '<tr><td align="center">'.$Lang['SysMgr']['Timetable']['Warning']['NoCycleSetUpYet'].'</td></tr>'."\n";
				}
				else
				{
					if ($numOfCycle != 0)
						$colWidth = floor(90 / $numOfCycle)."%";
					else
						$colWidth = "100%";
			
					## Standardize the display of IE and Firefox
					$table .= '<col align="left"></col>'."\n";
					for ($i=0; $i<$numOfCycle; $i++)
					{
						$table .= '<col align="left" width="'.$colWidth.'"></col>'."\n";
					}
	
					## Header
					$table .= '<thead>'."\n";
						# Cycle Title Display
						$table .= '<tr>'."\n";
							$table .= '<th>&nbsp;</th>'."\n";
							for ($i=0; $i<$numOfCycle; $i++)
							{
								if ($isViewMode)
								{
									// Display Day1, Day2, Day3...
									$thisTitle = $CycleArr[$i];
									$table .= '<th>'.$thisTitle.'</th>'."\n";
								}
								else
								{
									// Display 1,2,3...
									$thisDay = $i + 1;
									$table .= '<th>'.$thisDay.'</th>'."\n";
								}
							}
						$table .= '</tr>'."\n";
					$table .= '</thead>'."\n";
					
					$table .= '<tbody>'."\n";
						# Loop Period
						foreach ($PeriodArr as $thisTimeSessionID => $thisTimeSessionArr)
						{
							$thisTitle = $thisTimeSessionArr['Title'];
							$thisTimeSlot = $thisTimeSessionArr['TimeSlot'];
							
							$table .= '<tr>'."\n";
								$table .= '<td>'.$thisTitle.'<br />'.$thisTimeSlot.'</td>'."\n";
								
								# Loop Cycle
								for ($j=0; $j<$numOfCycle; $j++)
								{
									$thisDay = $j + 1;
									$thisRoomAllocationArr = $RoomAllocationArr[$thisTimeSessionID][$thisDay];
									
									$thisAddBtnDivID = 'AddBtnDiv_'.$thisTimeSessionID.'_'.$thisDay;
									$thisDetailTableDivID = 'DetailTableDiv_'.$thisTimeSessionID.'_'.$thisDay;
									
									$thisActionTag = '';
									$thisAddBtnDiv = '';
									if (!$isViewMode)
									{
										$thisActionTag = 'onmouseover="js_Show_Hide_Btn_Div(\''.$thisAddBtnDivID.'\', 1)" onmouseout="js_Show_Hide_Btn_Div(\''.$thisAddBtnDivID.'\', 0)"';
										# Add Room Allocation Icon
										$thisAddBtnDiv .= '<div id="'.$thisAddBtnDivID.'">'."\n";
											$thisAddBtnDiv .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
														$Lang['SysMgr']['Timetable']['Add']['RoomAllocation'], 
														"js_Get_Add_Edit_Room_Allocation_Layer('$thisTimeSessionID', '$CycleDays', '$thisDay', '', '$CycleDays'); return false;")."\n";
										$thisAddBtnDiv .= '</div>'."\n";
										$thisAddBtnDiv .= '<br style="clear:both" />'."\n";
									}
									
									$table .= '<td '.$thisActionTag.'>'."\n";
										$table .= $thisAddBtnDiv."\n";
										
										$table .= '<div id="'.$thisDetailTableDivID.'">'."\n";
											$table .= $this->Get_Room_Allocation_Detail_Table($thisTimeSessionID, $CycleDays, $thisDay, $thisRoomAllocationArr, $SubjectGroupArr, 
																								$SubjectGroupMapping, $LocationInfoArr, $SubjectGroupIDDisplayArr, $isViewMode)."\n";
										$table .= '</div>'."\n";
										
									$table .= '</td>'."\n";
								}	// End Loop Cycle
							$table .= '</tr>'."\n";
						}	// End Loop Period
					$table .= '</tbody>'."\n";
				}
			$table .= '</table>'."\n";
			
			$table .= '<div class="FakeLayer"></div>'."\n";
			
			return $table;
		}
		
		function Get_Room_Allocation_Detail_Table($TimeSessionID, $CycleDays, $Day, $thisRoomAllocationArr, $SubjectGroupArr, $SubjectGroupMapping, $LocationInfoArr, $SubjectGroupIDDisplayArr, $isViewMode)
		{
			global $Lang;
			$libinterface = new interface_html();
			
			$ContentTableID = 'ContentTable_'.$TimeSessionID.'_'.$Day;
			$numOfRoomAllocation = count($thisRoomAllocationArr);
			
			$table = '';
			$table .= '<table id="'.$ContentTableID.'" class="common_table_list" style="width:100%;">';
				# Loop each Room Allocation
				for ($k=0; $k<$numOfRoomAllocation; $k++)
				{
					$thisRoomAllocationID = $thisRoomAllocationArr[$k]['RoomAllocationID'];
					$thisSubjectGroupID = $thisRoomAllocationArr[$k]['SubjectGroupID'];
					$thisLocationID = $thisRoomAllocationArr[$k]['LocationID'];
					$thisOthersLocation = $thisRoomAllocationArr[$k]['OthersLocation'];
					
					if (!in_array($thisSubjectGroupID, $SubjectGroupIDDisplayArr))
						continue;
					
					# Subject Info
					$SubjectInfoArr = $SubjectGroupMapping[$thisSubjectGroupID];
					$thisSubjectNameEN = $SubjectInfoArr['SubjectDescEN'];
					$thisSubjectNameB5 = $SubjectInfoArr['SubjectDescB5'];
					$thisSubjectName = Get_Lang_Selection($thisSubjectNameB5, $thisSubjectNameEN);
					
					# Subject Group Info
					$thisSubjectGroupInfoArr = $SubjectGroupArr[$thisSubjectGroupID];
					$thisSubjectGroupNameEN = $thisSubjectGroupInfoArr['ClassTitleEN'];
					$thisSubjectGroupNameB5 = $thisSubjectGroupInfoArr['ClassTitleB5'];
					$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameB5, $thisSubjectGroupNameEN);
					$thisNumOfStudents = $thisSubjectGroupInfoArr['Total'];
					
					$thisSubjectGroupDisplay = $thisSubjectName." - ".$thisSubjectGroupName;
					
					# Teachers Info
					$thisStaffNameArr = $thisSubjectGroupInfoArr['StaffName'];
					$thisStaffNameDisplay = implode("<br />", $thisStaffNameArr);
					
					# Location Info
					if ($thisLocationID != 0)
					{
						$thisLocationInfoArr = $LocationInfoArr[$thisLocationID];
						$thisBuildingName = Get_Lang_Selection($thisLocationInfoArr['BuildingNameChi'], $thisLocationInfoArr['BuildingNameEng']);
						$thisFloorName = Get_Lang_Selection($thisLocationInfoArr['FloorNameChi'], $thisLocationInfoArr['FloorNameEng']);
						$thisRoomName = Get_Lang_Selection($thisLocationInfoArr['RoomNameChi'], $thisLocationInfoArr['RoomNameEng']);
						$thisLocationDisplay = $thisBuildingName." > ".$thisFloorName." > ".$thisRoomName;
					}
					else
					{
						$thisLocationDisplay = $thisOthersLocation;
					}
					
					# Delete Icon
					$thisDeleteBtnDivID = 'DeleteBtnDiv_'.$thisRoomAllocationID;
					
					if ($isViewMode)
					{
						# no edit link
						$thisRoomAllocationDisplay = "$thisSubjectGroupDisplay ($thisNumOfStudents)";
					}
					else
					{
						# edit room allocation link
						$thisRoomAllocationDisplay = $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "", $Lang['SysMgr']['Timetable']['Edit']['RoomAllocation'], 
														"js_Get_Add_Edit_Room_Allocation_Layer('$TimeSessionID', '$CycleDays', '$Day', '$thisRoomAllocationID'); return false;", 
														"FakeLayer", "$thisSubjectGroupDisplay ($thisNumOfStudents)");
					}
					
					$table .= '<tr onmouseover="js_Show_Hide_Btn_Div(\''.$thisDeleteBtnDivID.'\', 1)" onmouseout="js_Show_Hide_Btn_Div(\''.$thisDeleteBtnDivID.'\', 0)">';
						$table .= '<td>';
							# Room Allocation Content
							$table .= '<div>';
								$table .= $thisRoomAllocationDisplay;
								$table .= '<br />';
								$table .= $thisLocationDisplay;
								$table .= '<br />';
								$table .= $thisStaffNameDisplay;
							$table .= '</div>';
						$table .= '</td>';
						
						# Show delete room allocation icon if not in view mode
						if (!$isViewMode)
						{
							$table .= '<td width="20px">';
								$table .= '<div id="'.$thisDeleteBtnDivID.'">';
									$table .= '<a href="#" class="delete_dim" onclick="js_Delete_Room_Allocation(\''.$thisRoomAllocationID.'\'); return false;" title="'.$Lang['SysMgr']['Timetable']['Delete']['RoomAllocation'].'"></a>';
								$table .= '</div>';
								$table .= '<br style="clear:both" />';
							$table .= '</td>';
						}
					$table .= '</tr>';
				}	// End Loop Room Allocation
			$table .= '</table>';
			
			return $table;
		}
		
		function Get_Add_Edit_Room_Allocation_Layer($TimeSessionID, $CycleDays, $Day, $YearTermID, $RoomAllocationID='')
		{
			global $Lang, $PATH_WRT_ROOT, $intranet_session_language;
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$libinterface = new interface_html();
			
			# Add / Edit
			$isEdit = ($RoomAllocationID=='')? false : true;
			
			# Title
			$thisTitle = ($isEdit)? $Lang['SysMgr']['Timetable']['Edit']['RoomAllocation'] : $Lang['SysMgr']['Timetable']['Add']['RoomAllocation'];
			
			# Reset
			$btn_Reset = $libinterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "button", 
								$onclick="document.RoomAllocationForm.reset(); 
											$('#CodeWarningDiv').hide('fast'); 
											js_Change_Subject_Group_Selection(); 
											js_Change_Floor_Selection( 'SelectedBuildingID_InAddEditLayer', 
												'FloorSelectionDiv_InAddEditLayer', 'SelectedLocationLevelID_InAddEditLayer',
												'RoomSelectionDiv_InAddEditLayer', 'SelectedLocationID_InAddEditLayer',
												'---')
										", $id="Btn_Reset");
			# Cancel
			$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
								$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
			
			if ($isEdit)
			{
				$js_RoomAllocationID = $RoomAllocationID;
				
				# Button Submit
				$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
									$onclick="js_Insert_Edit_Room_Allocation('$RoomAllocationID', '$TimeSessionID', '$CycleDays', '$Day', 0)", $id="Btn_Edit");
			}
			else
			{
				$js_RoomAllocationID = 'null';
				
				# Button Add & Finish
				$btn_Add_Finish = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['Location']['Button']['Add&Finish'], "button", 
									$onclick="js_Insert_Edit_Room_Allocation('', '$TimeSessionID', '$CycleDays', '$Day', 0)", $id="Btn_AddFinish");
				# Button Add & Add More Location
				$btn_Add_AddMore = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['Timetable']['Button']['Add&AddMoreRoomAllocation'], "button", 
									$onclick="js_Insert_Edit_Room_Allocation('', '$TimeSessionID', '$CycleDays', '$Day', 1)", $id="Btn_AddFinish");
			}
			
			# Preset Filters for edit mode
			if ($isEdit)
			{
				$objRoomAllocation = new Room_Allocation($RoomAllocationID);
				$thisSubjectGroupID = $objRoomAllocation->SubjectGroupID;
				
				include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
				$objSubjectGroup = new subject_term_class($thisSubjectGroupID);
				$thisSubjectID = $objSubjectGroup->SubjectID;
				
				$thisLocationID = $objRoomAllocation->LocationID;
				
				if ($thisLocationID == 0)
				{
					# Others Location
					$thisOthersLocation = intranet_htmlspecialchars($objRoomAllocation->OthersLocation);
					
					$thisBuildingID = "-1";
				}
				else
				{
					include_once($PATH_WRT_ROOT."includes/liblocation.php");
					$objRoom = new Room($thisLocationID);
					$thisLocationLevelID = $objRoom->LocationLevelID;
					
					$objFloor = new Floor($thisLocationLevelID);
					$thisBuildingID = $objFloor->BuildingID;
				}
			}
			else
			{
				$thisSubjectID = '';
				$thisLocationID = '';
				$thisLocationLevelID = '';
				$thisBuildingID = '';
			}
			
			
			# Subject Filter
			include_once("subject_class_mapping_ui.php");
			if ($SelectedSubjectID == '')
			{
				$libSubject = new subject();
				$SubjectArr = $libSubject->Get_Subject_List();
				$SelectedSubjectID = $SubjectArr[0]['SubjectID'];
			}
			$libsubject_ui = new subject_class_mapping_ui();
			$subjectSelection = $libsubject_ui->Get_Subject_Selection("SelectedSubjectID_InAddEditLayer", $thisSubjectID, 
								"js_Change_Subject_Group_Selection($TimeSessionID, $CycleDays, $Day, $js_RoomAllocationID, $YearTermID)", 
								0, $Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']);
			
			# Subject Group Filter
			if ($RoomAllocationID != '')
			{
				$subjectGroupSelDisplay = '';
				$subjectGroupFilter = $libsubject_ui->Get_Subject_Group_Selection($thisSubjectID, 
																					"SelectedSubjectGroupID", 
																					$thisSubjectGroupID,
																					"js_Validate_Subject_Group_Overlap($TimeSessionID, $CycleDays, $Day, $js_RoomAllocationID)",
																					$YearTermID
																					);
			}
			else
			{
				$subjectGroupSelDisplay = ' style="display:none" ';
			}
			
			# Building Filter
			include_once("liblocation_ui.php");
			$liblocation_ui = new liblocation_ui();
			$buildingSelection = $liblocation_ui->Get_Building_Selection(
									$thisBuildingID, 
									'SelectedBuildingID_InAddEditLayer', 
									"js_Change_Floor_Selection(	'$PATH_WRT_ROOT',
																'SelectedBuildingID_InAddEditLayer', 
																'OthersLocationTb',
																'FloorSelectionTr_InAddEditLayer',
																'FloorSelectionDiv_InAddEditLayer',
																'SelectedFloorID_InAddEditLayer',
																'',
																'RoomSelectionTr_InAddEditLayer',
																'RoomSelectionDiv_InAddEditLayer',
																'SelectedRoomID_InAddEditLayer',
																'',
																'js_Validate_Room_Vacancy($TimeSessionID, $CycleDays, $Day, $js_RoomAllocationID)'
																 );
									",
									$noFirst = 0,
									$withOthersOption = 1
								);
			
			if ($isEdit && $thisLocationID != 0)
			{
				# Floor Filter
				$floorSelDisplay = '';
				$floorFilter = $liblocation_ui->Get_Floor_Selection($thisBuildingID, $thisLocationLevelID, 'SelectedFloorID_InAddEditLayer', 
																	"js_Change_Room_Selection(	'SelectedFloorID_InAddEditLayer', 
																		'RoomSelectionDiv_InAddEditLayer', 'SelectedRoomID_InAddEditLayer')", 
																	'---',
																	1);
																	
				# Room Filter
				$roomSelDisplay = '';
				$roomFilter = $liblocation_ui->Get_Room_Selection($thisLocationLevelID, $thisLocationID, "SelectedRoomID_InAddEditLayer", "", 1);
			}
			else
			{
				$floorSelDisplay = ' style="display:none" ';
				$roomSelDisplay = ' style="display:none" ';
			}
			
			# Others location textbox
			if ($RoomAllocationID != '' && $thisLocationID == 0)
			{
				$othersLocationTb = '<input id="OthersLocationTb" name="OthersLocation" type="text" size="20" value="'.$thisOthersLocation.'" />';
			}
			else
			{
				$othersLocationTb = '<input id="OthersLocationTb" name="OthersLocation" type="text" size="20" style="display:none" />';
			}
			
			$TimeSessionInfoArr = $this->Get_Time_Session_Info($TimeSessionID);
			$thisTitle = $TimeSessionInfoArr['Title'];
			$thisTimeSlot = $TimeSessionInfoArr['TimeSlot'];
			$TimeSessionDisplay = $thisTitle.' '.$thisTimeSlot;
			
			$x = '';
			$x .= '<div id="debugArea"></div>';
			$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
				$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
				$x .= '<h1> '.$Lang['SysMgr']['Timetable']['Timetable'].' &gt; <span>'.$thisTitle.'</span></h1>'."\n";
				$x .= '<div class="edit_pop_board_write">'."\n";
					$x .= '<form id="RoomAllocationForm" name="RoomAllocationForm">'."\n";
						$x .= '<table class="form_table">'."\n";
							$x .= '<col class="field_title" />'."\n";
							$x .= '<col class="field_c" />'."\n";
							
							# Cycle Name display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['CycleDay']['Day'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'.$Day.'</td>'."\n"; 
							$x .= '</tr>'."\n";
							# Period Name display
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Timetable']['TimeSession'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'.$TimeSessionDisplay.'</td>'."\n"; 
							$x .= '</tr>'."\n";
							# Subject Selection
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= $subjectSelection;
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Subject Group Selection
							$x .= '<tr id="SubjectGroupSelectionTr_InAddEditLayer" '.$subjectGroupSelDisplay.'>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="SubjectGroupSelectionDiv_InAddEditLayer">';
										$x .= $subjectGroupFilter;
									$x .= '</div>';
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("SubjectGroupWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Building Selection
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Location']['Building'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<span>'.$buildingSelection.'</span>&nbsp;&nbsp;';
									$x .= '<span>'.$othersLocationTb.'</span>';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Floor Selection
							$x .= '<tr id="FloorSelectionTr_InAddEditLayer" '.$floorSelDisplay.'>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Location']['Floor'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="FloorSelectionDiv_InAddEditLayer">';
										$x .= $floorFilter;
									$x .= '</div>';
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
							# Location Selection
							$x .= '<tr id="RoomSelectionTr_InAddEditLayer" '.$roomSelDisplay.'>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['Location']['Room'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="RoomSelectionDiv_InAddEditLayer">';
										$x .= $roomFilter;
									$x .= '</div>';
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("LocationWarningDiv");
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</form>'."\n";
				$x .= '</div>'."\n";
				
				$x .= '<div class="edit_bottom">'."\n";
					$x .= '<span></span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
					if ($isEdit)
					{
						$x .= $btn_Edit."\n";
					}
					else
					{
						$x .= $btn_Add_Finish."\n";
						$x .= $btn_Add_AddMore."\n";
					}
					$x .= $btn_Reset."\n";
					$x .= $btn_Cancel."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>';
			$x .= '</div>';
			
			return $x;
		}
		
		function Get_Period_Selection()
		{
			global $Lang;
			
			$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['Period']);
			$PeriodInfoArr = array("1" => "Normal Period", "2"=>"Summer Period");
			
			$periodSelection = getSelectByAssoArray($PeriodInfoArr, "id='SelectedPeriodID'", 1, $all=0, $noFirst=0, $firstTitle);
			
			return $periodSelection;
		}
		
		function Get_View_Settings_Table($PeriodID="", $CycleDays="", $ViewMode="", $Identity="")
		{
			global $Lang;
			
			# Acadermic Year Filter
			$thisOnchange = 'onchange="js_Update_Period_Cycle_Semester_Selection()"';
			$acadermicYearSelection = getSelectAcademicYear("SelectedAcademicYearID", $thisOnchange, 1);
			
			# Term Filter
			include_once("subject_class_mapping_ui.php");
			$subject_class_mapping_ui = new subject_class_mapping_ui();
			$termSelection = $subject_class_mapping_ui->Get_Term_Selection("SelectedYearTermID", '', '', '', 1);
			
			# Period Filter
			$periodSelection = $this->Get_Period_Selection();
			
			# Cycle Filter
			include_once("libcycleperiods_ui.php");
			$libcycleperiods_ui = new libcycleperiods_ui();
			$cycleDaysSelection = $libcycleperiods_ui->getCyclePeriodSelection("SelectedCyclePeriodID","SelectedCyclePeriodID", $CycleDays, "");
			
			# View Mode Filter
			$viewModeSelection = $this->Get_View_Mode_Selection("SelectedViewMode", "SelectedViewMode", $ViewMode, "js_Reload_Secondary_Selection()");
			
			$table = '';
			$table .= '<table id="FilterTable" class="form_table">'."\n";
				$table .= '<col class="field_title" style="vertical-align:top"/>'."\n";
				$table .= '<col class="field_c" style="vertical-align:top"/>'."\n";
				
				# Acadermic Year
				$table .= '<tr>';
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['AcademicYear'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'.$acadermicYearSelection.'</td>'."\n";
				$table .= '</tr>';
				# Term
				$table .= '<tr>';
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['Term'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>';
						$table .= '<div id="TermSelectionDiv">';
							$table .= $termSelection;
						$table .= '</div>';
					$table .= '</td>'."\n";
				$table .= '</tr>';
				# Cycle
				$table .= '<tr>';
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['Cycle'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>';
						$table .= '<div id="CycleSelectionDiv">';
							$table .= $cycleDaysSelection;
						$table .= '</div>';
					$table .= '</td>'."\n";
				$table .= '</tr>';
				# Period
				$table .= '<tr>';
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['Period'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>';
						$table .= '<div id="PeriodSelectionDiv">';
							$table .= $periodSelection;
						$table .= '</div>';
					$table .= '</td>'."\n";
				$table .= '</tr>';
				# View Mode
				$table .= '<tr>';
					$table .= '<td>'.$Lang['SysMgr']['Timetable']['ViewMode'].'</td>'."\n";
					$table .= '<td>:</td>'."\n";
					$table .= '<td>'.$viewModeSelection.'</td>'."\n";
				$table .= '</tr>';
			$table .= '</table>';
			
			return $table;
		}
		
		function Get_View_Secondary_Settings_Table($ViewMode)
		{
			global $Lang;
			include_once("libinterface.php");
			$libinterface = new interface_html();
			
			$table = '';
			$table .= '<table id="SecondaryFilterTable" class="form_table">'."\n";
				$table .= '<col class="field_title" style="vertical-align:top"/>'."\n";
				$table .= '<col class="field_c" style="vertical-align:top"/>'."\n";
				
				if ($ViewMode == "Class")
				{
					include_once("form_class_manage_ui.php");
					$form_class_manage_ui = new form_class_manage_ui();
					$formSelection = $form_class_manage_ui->Get_Form_Selection('SelectedYearID', "", "js_Reload_Class_Selection(1)", 0);
	
					# Form Selection
					$table .= '<tr id="FormSelectionTr" >';
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="FormSelectionDiv">'.$formSelection.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Class Multiple Selection
					$table .= '<tr id="ClassSelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="ClassSelectionDiv">'.$this->EmptyString.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($ViewMode == "SubjectGroup")
				{
					include_once("subject_class_mapping_ui.php");
					$libsubject_ui = new subject_class_mapping_ui();
					$subjectSelection = $libsubject_ui->Get_Subject_Selection('SelectedSubjectID', '', 
																				"js_Reload_Subject_Group_Selection('', '', '', '', '')", 0, $Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']);
					
					# Subject Selection
					$table .= '<tr id="SubjectSelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Subject'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="SubjectSelectionDiv">'.$subjectSelection.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Subject Group Multiple Selection
					$table .= '<tr id="SubjectGroupSelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="SubjectGroupSelectionDiv">'.$this->EmptyString.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($ViewMode == "Personal")
				{
					$identitySelection = $this->Get_Identity_Selection('SelectedIdentity', 'SelectedIdentity', "", "js_Show_Hide_Form_Class_Selection()");
					
					include_once("form_class_manage_ui.php");
					$form_class_manage_ui = new form_class_manage_ui();
					$formSelection = $form_class_manage_ui->Get_Form_Selection('SelectedYearID', "", "js_Reload_Class_Selection(0)", 0);
					
					# Identity Selection
					$table .= '<tr id="IdentitySelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['Timetable']['Identity'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="IdentitySelectionDiv">'.$identitySelection.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Form Selection
					$table .= '<tr id="FormSelectionTr" style="display:none">';
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="FormSelectionDiv">'.$formSelection.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Class Selection
					$table .= '<tr id="ClassSelectionTr" style="display:none">';
						$table .= '<td>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="ClassSelectionDiv">'.$this->EmptyString.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Target Multiple Selection
					/*
					if ($Identity == "TeachingStaff")
						$thisTitle = $Lang['SysMgr']['RoleManagement']['TeachingStaff'];
					else if ($Identity == "NonTeachingStaff")
						$thisTitle = $Lang['SysMgr']['RoleManagement']['SupportStaff'];
					else if ($Identity == "Student")
						$thisTitle = $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'];
					*/
					
					$table .= '<tr id="PersonSelectionTr" style="display:none">';
						$table .= '<td>'.$Lang['SysMgr']['Timetable']['Target'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="PersonSelectionDiv">'.$this->EmptyString.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				else if ($ViewMode == "Room")
				{
					# Building Filter
					include_once("liblocation_ui.php");
					$liblocation_ui = new liblocation_ui();
					$buildingSelection = $liblocation_ui->Get_Building_Selection($SelectedBuildingID, 'SelectedBuildingID', 
											"js_Change_Floor_Selection( 'SelectedBuildingID', 
																		'FloorSelectionDiv', 'SelectedLocationLevelID',
																		'RoomSelectionDiv', 'SelectedLocationID',
																		'---')"  );
		
					# Building Selection
					$table .= '<tr id="BuildingSelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['Location']['Building'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="BuildingSelectionDiv">'.$buildingSelection.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Floor Selection
					$table .= '<tr id="FloorSelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['Location']['Floor'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="FloorSelectionDiv">'.$this->EmptyString.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
					
					# Room Multiple Selection
					$table .= '<tr id="RoomSelectionTr">';
						$table .= '<td>'.$Lang['SysMgr']['Location']['Room'].'</td>'."\n";
						$table .= '<td>:</td>'."\n";
						$table .= '<td>';
							$table .= '<div id="RoomSelectionDiv">'.$this->EmptyString.'</div>';
						$table .= '</td>'."\n";
					$table .= '</tr>'."\n";
				}
				
				# Empty Row
				$table .= '<tr><td colspan="3">&nbsp;</td></tr>'."\n";
			$table .= '</table>'."\n";
			
			# Button Submit
			$btn_Submit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "Generate_Timetable()", "GenerateBtn");
			$table .= '<div class="edit_bottom">'."\n";
				$table .= '<span></span>'."\n";
				$table .= '<p class="spacer"></p>'."\n";
					$table .= $btn_Submit."\n";
				$table .= '<p class="spacer"></p>'."\n";
			$table .= '</div>';
			
			return $table;
		}
		
		function Get_View_Mode_Selection($ID, $Name, $SelectedMode="", $OnChange="")
		{
			global $Lang;
			
			$selectionArr['Class'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Class'];
			$selectionArr['SubjectGroup'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['SubjectGroup'];
			$selectionArr['Personal'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Personal'];
			$selectionArr['Room'] = $Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
				
			$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['ViewMode']);
			
			$selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedMode, $all=0, $noFirst=0, $firstTitle);
			
			return $selection;
		}
		
		function Get_Identity_Selection($ID, $Name, $SelectedIdentity="", $OnChange="")
		{
			global $Lang;
			
			$selectionArr['TeachingStaff'] = $Lang['Identity']['TeachingStaff'];
			$selectionArr['NonTeachingStaff'] = $Lang['Identity']['NonTeachingStaff'];
			$selectionArr['Student'] = $Lang['Identity']['Student'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
			
			$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['Identity']);
			
			$selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedMode, $all=0, $noFirst=0, $firstTitle);
			
			return $selection;
		}
		
		/*
		function Get_User_Multiple_Selection_Box($YearClassID='', $SubjectGroupID='', $ShowStudent=1)
		{
			include_once("form_class_manage.php");
			if ($YearClassID == '')
			{
				$libYearClass = new year_class($YearClassID,0,0,0);
				$FormClass_TeacherList = $libYearClass->Get_All_Class_Teacher_List();
				$FormClass_StudentList = $libYearClass->Get_All_Class_Teacher_List();
			}
			else
			{
				$libYearClass = new year_class($YearClassID,0,1,1);
				$FormClass_TeacherList = $libYearClass->ClassTeacherList;
				$FormClass_StudentList = $libYearClass->ClassStudentList;
			}
			
			include_once("subject_class_mapping.php");
			if ($SubjectGroupID == '')
			{
				$libSubjectGroup = new subject_term_class($SubjectGroupID,0,0,0);
				$FormClass_TeacherList = $libSubjectGroup->Get_All_Class_Teacher_List();
				$FormClass_StudentList = $libSubjectGroup->Get_All_Class_Teacher_List();
			}
			else
			{
				$libSubjectGroup = new year_class($SubjectGroupID,0,1,1);
				$FormClass_TeacherList = $libSubjectGroup->ClassTeacherList;
				$FormClass_StudentList = $libSubjectGroup->ClassStudentList;
			}
			
			$SubjectGroupID = 47;
			$libSCM = new subject_term_class($SubjectGroupID, 1, 1);
		}
		*/
	}

} // End of directives
?>

