<?php
# using: 

/******************************************
 *  20130124 Rita
 * 		- modified retrieveRecord(), add return
 * 	20100719 Marcus:
 * 		- modified returnSelectCategory, returnCats, excluded ECA if client use eEnrolment
 * 
 * ****************************************/
 
if (!defined("LIBGROUPCATEGORY_DEFINED"))         // Preprocessor directives
{

 define("LIBGROUPCATEGORY_DEFINED",true);

 class libgroupcategory extends libdb{
       var $GroupCategoryID;
       var $CategoryName;
       var $recordType;
       var $recordStatus;

       function libgroupcategory($cid="")
       {
                $this->libdb();
                if ($cid != "")
                {
                    $this->retrieveRecord($cid);
                }
       }
       function retrieveRecord($cid)
       {
                $sql = "SELECT CategoryName,RecordType,RecordStatus
                        FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID = $cid";
                $result = $this->returnArray($sql,3);
                
                list ($this->CategoryName, $this->recordType,
                      $this->recordStatus) = $result[0];
                $this->GroupCategoryID = $cid;
	   		return $result;
       }
       function returnAllCatEx($exids)   # return all categories other than list in $exids
       {
                $list = implode(",",$exids);
                $sql = "SELECT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY
                        WHERE GroupCategoryID NOT IN ($list) ORDER BY CategoryName";
                return $this->returnArray($sql,2);
       }
       function returnAllCat()
       {
                $sql = "SELECT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY
                        ORDER BY CategoryName";
                return $this->returnArray($sql,2);
       }

       function returnSelectCategory2($tag, $selected="")
       {
                $cats = $this->returnAllCatEx(array(0));
                return getSelectByArray($cats,$tag,$selected);
       }
       function returnSelectAllCategory($tag, $selected="")
       {
                $cats = $this->returnAllCat();
                return getSelectByArray($cats,$tag,$selected,1);
       }
       function returnCats($noID=false, $except=array())
       {
                if ($noID)
                {
                    $conds = " AND GroupCategoryID != 0";
                }
                
                if(!empty($except))
                {
                	$exceptCatStr = implode(",",(array)$except);
                	$conds.= " AND GroupCategoryID NOT IN ($exceptCatStr) ";	
                }
                
                $sql = "SELECT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY
                        WHERE 1 $conds ORDER BY GroupCategoryID";
                $result = $this->returnArray($sql,2);
                
				return $result;
       }
       function returnCatsForOrPage($noID=false)
       {
                if ($noID)
                {
                    $conds = "WHERE GroupCategoryID != 0";
                }
                $sql = "SELECT b.RecordType, COUNT(b.GroupID)
                          FROM INTRANET_GROUP as b
                               LEFT OUTER JOIN INTRANET_ORPAGE_GROUP as a ON a.GroupID = b.GroupID
                               WHERE a.RecordStatus <> 1 OR a.GroupID IS NULL
                               GROUP BY b.RecordType
                ";
                $cats = $this->returnArray($sql,2);
                #print_r($cats);
                $delimiter = "";
                $list = "";
                for ($i=0; $i<sizeof($cats); $i++)
                {
                     list($catID,$count) = $cats[$i];
                     $list .= "$delimiter $catID";
                     $delimiter = ",";
                }

                if ($list != "")
                {
                    if ($conds != "")
                    {
                        $conds .= " AND GroupCategoryID IN ($list)";
                    }
                    else
                    {
                        $conds .= "WHERE GroupCategoryID IN ($list)";
                    }
                }

                /*
                $cats = $this->returnVector($sql);
                if (sizeof($cats)!=0)
                {
                    $list = implode(",",$cats);
                    $conds .= " AND GroupCategoryID IN ($list)";
                }
                */
                $sql = "SELECT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY
                        $conds ORDER BY GroupCategoryID";
                return $this->returnArray($sql,2);
       }
       function returnSelectCategory($tag,$noID=false, $all=0, $selected="", $except = array())
       {
       			global $plugin;

                $cats = $this->returnCats($noID, $except);
                
                return getSelectByArray($cats,$tag,$selected,$all);
       }
       function returnSelectCategoryForOrPage($tag,$noID=false, $all=0, $selected="")
       {
                $cats = $this->returnCatsForOrPage($noID);
                return getSelectByArray($cats,$tag,$selected,$all);
       }
       
       function returnGroupCategoryDefaultRoleID($GroupCategory='')
       {
	       if($GroupCategory)
	       {
                $sql = "SELECT RoleID FROM INTRANET_ROLE WHERE RecordType = $GroupCategory and RecordStatus=1";
                $result = $this->returnVector($sql);
                return $result[0];
            }
        }
        
         function returnGroupCategoryDefaultRole($GroupCategory='')
        {
       		if($GroupCategory=='')
	       		$GroupCategory = $this->GroupCategoryID;
	       	 	
	   		if($GroupCategory)
			{
			     $sql = "SELECT RoleID, Title FROM INTRANET_ROLE WHERE RecordType = $GroupCategory and RecordStatus=1";
			     $result = $this->returnArray($sql);
			
			     return $result[0];
			}
        }


 }


} // End of directives
?>