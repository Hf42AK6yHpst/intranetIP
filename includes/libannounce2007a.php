<?php
# using:  
/*
 * Modification Log:
 * 
 * 	20151210 (Cameron)
 * 	- add function displayCustAnnouncement()
 * 
 * 	20151015 (Ivan) [ip.2.5.6.12.1]
 * 	- modified function display() to sync parameter with libannonuce.php to solve the PHP5.4 "Strict Standards" error
 *
 */

class libannounce2007 extends libannounce
{
	function libannounce2007($AnnouncementID="")
	{
		$this->libannounce($AnnouncementID);		
	}

	function displaySchoolAnnouncement($UserID)
	{
		global $image_path,$intranet_session_language, $LAYOUT_SKIN;;
		global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
				,$i_AnnouncementSystemAdmin, $i_general_more;
		global $plugin;				
		
		$row = $this->returnSchoolAnnouncement($UserID);
		
		$x .= "
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" >
				";
		if(sizeof($row)==0)
		{
			global $i_no_record_exists_msg;
			
			$x .= "<tr><td align='center' colspan='3' class='indextabclassiconoff'>$i_no_record_exists_msg</td></tr>\n";
		} 
		else 
		{
			for($i=0; $i<sizeof($row); $i++)
			{
				$AnnouncementID = $row[$i][0];
				$AnnouncementDate = $row[$i][1];
				$AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                $IsNew = $row[$i][3];
                $Attachment = $row[$i][4];
                $announcer = $row[$i][5];
                $ownerGroup = $row[$i][6];
                if ($plugin['power_voice'])
                {
	                $VoiceFile = $row[$i][7];
	                if ($VoiceFile == "")
	                {
		                $VoiceClip = "";
		                $ClipWidth= "12";
	                }
	                else
	                {
		                $VoiceClip = " <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif\" border=\"0\" align=\"absmiddle\"  /> ";
		                $ClipWidth= "38";
	                }
                }                                                

                if ($ownerGroup == "")
                {
                    if ($announcer == "")
                        $displayName = "$i_AnnouncementSystemAdmin";
                    else 
                    	$displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer <br> $ownerGroup";
                }

                $clip = ($Attachment == ""? "&nbsp;": " <img src=\"{$image_path}/{$LAYOUT_SKIN}/index/whatnews/icon_attachment.gif\" /> ");
                $newFlag = (($IsNew) ? "<img src='{$image_path}/{$LAYOUT_SKIN}/alert_new.gif' align='absmiddle' border='0' />" : "&nbsp;");
                                
                $x .= "	
						<tr>
							<td>
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
							<tr>
								<td width=\"{$ClipWidth}\" align=\"left\" >{$clip}{$VoiceClip}</td>
								<td align='left'><a href=\"javascript:void(0)\" onclick=\"javascript:fe_view_announcement($AnnouncementID, 0)\" class=\"indexnewslist\">{$AnnouncementTitle}</a>{$newFlag}</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td align='left' class=\"indexnewsdesc\">({$AnnouncementDate}) {$i_AnnouncementOwner} {$displayName}</td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td height=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
						</tr>                       		
                       	";
        	}
		}
        
        if(sizeof($row)==0)
		{
			$x .= "
			<tr>
					<td height=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
				</tr> 
				";
		}
		
		$x .= "</table>\n";
        
        $x .= "
        		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" >
            	<tr>
              		<td align=right><a href='#' onclick='javascript:moreNews(0)' class=\"indexnewslist\">$i_general_more...</a></td>
           		</tr>
           		</table>
            	";
    
        return $x;        
	}
		
	
	function displayGroupAnnounce($Groups)
	{
		global $image_path,$intranet_session_language, $LAYOUT_SKIN;
		global $i_iconAttachment,$i_AnnouncementGroup, $i_general_more, 
				 $i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner, $i_AnnouncementSystemAdmin;
		global $plugin;
				 
		$row = $this->returnGroupsAnnouncement($Groups);

		$x .= "
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" >
				";
		if(sizeof($row)==0)
		{
			global $i_no_record_exists_msg;
			
			$x .= "<tr><td align='center' colspan='3' class='indextabclassiconoff' >$i_no_record_exists_msg</td></tr>\n";
		} 
		else 
		{
			for($i=0; $i<sizeof($row); $i++)
			{
                $AnnouncementID = $row[$i][0];
                $AnnouncementDate = $row[$i][1];
                $AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                $IsNew = $row[$i][3];
                $Attachment = $row[$i][4];
                $announcer = $row[$i][5];
                $ownerGroup = $row[$i][6];

                if ($ownerGroup == "")
                {
                    if ($announcer == "")
                        $displayName = "$i_AnnouncementSystemAdmin";
                    else $displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer <br> $ownerGroup";
                }
                $clip = ($Attachment == ""? "&nbsp;": " <img src=\"{$image_path}/{$LAYOUT_SKIN}/index/whatnews/icon_attachment.gif\" /> ");
                $newFlag = (($IsNew) ? "<img src='{$image_path}/{$LAYOUT_SKIN}/alert_new.gif' align='absmiddle' border='0' />" : "&nbsp;");

                if ($plugin['power_voice'])
                {
	                $VoiceFile = $row[$i][7];
	                $VoiceClip = ($VoiceFile == ""? "": " <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif\" border=\"0\" align=\"absmiddle\"  /> ");
                }                                

                if ($VoiceClip =="")
                {
	                $ClipWidth= "12";
                }
                else
                {
	                $ClipWidth= "38";
                }                                

                $x .= "	
						<tr>
							<td>
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
							<tr>
								<td width=\"{$ClipWidth}\">{$clip}{$VoiceClip}</td>
								<td valign=\"top\" align=left><a href=\"javascript:void(0)\" onclick=\"javascript:fe_view_announcement($AnnouncementID, 1)\" class=\"indexnewslistgroup\">{$AnnouncementTitle}</a> {$newFlag}</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td valign=\"top\" class=\"indexnewsdesc\" align=left>({$AnnouncementDate}) {$i_AnnouncementOwner} {$displayName}</td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td height=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
						</tr>                       		
                       	";
                        }
                }
			
			if(sizeof($row)==0)
			{
				$x .= "
					<tr>
							<td height=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"2\"></td>
						</tr> 
						";
			}
			
			$x .= "</table>\n";
			
				$group_str = implode(",",$Groups);
				
		        $x .= "
		        		<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" >
		            	<tr>
		              		<td align=right>
		              		<td align=right><a href='#' onclick='javascript:moreGroupNews(1, \"". $group_str ."\")' class=\"indexnewslistgroup\">$i_general_more...</a></td>
		           		</tr>
		           		</table>
		            	";
		            	
			return $x;
        }
	
	
	function display($colortype=0)
	{
                global $i_AnnouncementTitle, $i_AnnouncementDate, $i_AnnouncementDateModified, $i_AnnouncementAttachment, $file_path, $image_path, $LAYOUT_SKIN;
                global $intranet_httppath,$i_AnnouncementWholeSchool,$i_AnnouncementOwner,$i_AnnouncementTargetGroup;
                global $i_AnnouncementDescription;
                $announcer = $this->returnAnnouncerName();
                $ownerGroup = $this->returnOwnerGroup();
                $targetGroups = $this->returnTargetGroups();
                if ($ownerGroup == "")
                {
                    $displayName = $announcer;
                }
                else
                {
                    $displayName = "$announcer ($ownerGroup)";
                }
                if (sizeof($targetGroups)==0)
                    $target = $i_AnnouncementWholeSchool;
                else
                    $target = implode(", ",$targetGroups);

				$x = "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
				$x .= "<tr valign=\"top\">";
				$x .= "		<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">$i_AnnouncementTitle </span></td>";
				$x .= "		<td class=\"tabletext\">". $this->Title ."</td>";
				$x .= "</tr>";

				$x .= "<tr valign=\"top\">";
				$x .= "		<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">$i_AnnouncementDate </span></td>";
				$x .= "		<td class=\"tabletext\">". $this->AnnouncementDate ."</td>";
				$x .= "</tr>";
				
				$x .= "<tr valign=\"top\">";
				$x .= "		<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">$i_AnnouncementOwner </span></td>";
				$x .= "		<td class=\"tabletext\">". intranet_wordwrap($displayName,30,"\n",1) ."</td>";
				$x .= "</tr>";
				
				$x .= "<tr valign=\"top\">";
				$x .= "		<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">$i_AnnouncementTargetGroup </span></td>";
				$x .= "		<td class=\"tabletext\">". intranet_wordwrap($target,30,"\n",1) ."</td>";
				$x .= "</tr>";
				
				$x .= "<tr valign=\"top\">";
				$x .= "		<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">$i_AnnouncementDescription </span></td>";
				$x .= "		<td class=\"tabletext\">". nl2br($this->convertAllLinks2($this->Description,30)) ."</td>";
				$x .= "</tr>";
				
				$attNameDisplay = true;
				if ($this->Attachment != "")	#non-image
				{
					
                    $hasOther = false;
                    $displayOther .= "<tr><td align=left colspan=2>";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) 
                    {
                           $target_filepath = "$path/".$files[$key][0];
                           if (!isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);
                               
                               	$displayOther .= "<tr>";
								$displayOther .= "<td valign='top' nowrap class='tabletext'>". ($attNameDisplay ? $i_AnnouncementAttachment : "") ."</td>";
                        		$displayOther .= "<td class='tabletext'>";
								$displayOther .= "<img src='$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif' hspace='2' vspace='2' border='0' align='absmiddle'>";
                                $displayOther .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                                $displayOther .= " (".ceil($files[$key][1]/1000)."Kb)";
                                $displayImage .= "</td></tr>\n";
                                $hasOther = true;
                                $attNameDisplay = false;
                        }
                    }
                    $displayOther .= "</td></tr>";
                    if ($hasOther)
                    {
                        $x .= $displayOther;
                    }
                    
                    
                    #Display image
                    $hasImage = false;
                    $displayImage .= "<tr><td align=left colspan=2>";
                    $path = "$file_path/file/announcement/".$this->Attachment.$this->AnnouncementID;
                    $templf = new libfiletable("", $path,0,0,"");
                    $files = $templf->files;

                    while (list($key, $value) = each($files)) {
                           $target_filepath = "$path/".$files[$key][0];
                           if (isImage($target_filepath))
                           {
                               $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                               $url = intranet_handle_url($url);
                        		$size = GetImageSize($target_filepath);
                        		list($width, $height, $type, $attr) = $size;
								
                        		$image_html_tag = "";
                            	$href_start	= "<a class=\"indexpoplink\" href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">";
								$href_end	= "</a>";
                        
								if($width > 300)
									$image_html_tag = "style='width: 300px'";
						
                        $displayImage .= "<tr>";
                        $displayImage .= "<td valign='top' nowrap class='tabletext'>". ($attNameDisplay ? $i_AnnouncementAttachment : "") ."</td>";
                        $displayImage .= "<td bgcolor='#FFFFFF' class='tabletext'>";
                        $displayImage .= "<img src='$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif' hspace='2' vspace='2' border='0' align='absmiddle'>";
                        $displayImage .= $href_start . $files[$key][0] . "<br \>";
                        $displayImage .= "<img border=0 src=\"$intranet_httppath$url\" $image_html_tag>".$href_end;
                        $displayImage .= "</td></tr>\n";
                        $hasImage = true;
                        $attNameDisplay = false;
                           }
                    }
                    $displayImage .= "</td></tr>\n";
                    if ($hasImage)
                    {
                        $x .= $displayImage;
                    }
				}
				
				$x .= "</table>";
				
                return $x;
        }	
        
        /*
        # moved to libportal.php
        function getWaitingAnnouncement($targetUserID)
        {
	        
	        $sql = "SELECT count(*) FROM INTRANET_ANNOUNCEMENT_APPROVAL WHERE UserID = '$targetUserID' and RecordStatus = 2";
			$temp = $this->returnVector($sql);
			return $temp[0]+0;
    	}
    	*/

	#########################################################################################
	# Amway Customized Announcement  [Start]
	#########################################################################################

	function displayCustAnnouncement($UserID)
	{
		global $image_path,$intranet_session_language, $LAYOUT_SKIN;;
		global $i_AnnouncementPublic,$i_iconAttachment,$i_AnnouncementDisplayDate,$i_AnnouncementTitle,$i_AnnouncementOwner
				,$i_AnnouncementSystemAdmin, $i_general_more;
		global $plugin, $Lang;				
		
		$row = $this->returnSchoolAnnouncement($UserID);
		
		$x = "";
		if(sizeof($row)==0)
		{
			global $i_no_record_exists_msg;
			$x .= "<div class=\"portal_module_list indextabclassiconoff\">$i_no_record_exists_msg</div>\n";
		} 
		else 
		{
			for($i=0, $iMax=count($row); $i<$iMax; $i++)
			{
				$AnnouncementID = $row[$i][0];
				$AnnouncementDate = $row[$i][1];
				$AnnouncementTitle = intranet_wordwrap($row[$i][2],40,"\n",1);

                $IsNew = $row[$i][3];
                $Attachment = $row[$i][4];
                $announcer = $row[$i][5];
                $ownerGroup = $row[$i][6];
                if ($plugin['power_voice'])
                {
	                $VoiceFile = $row[$i][7];
	                if ($VoiceFile == "")
	                {
		                $VoiceClip = "";
	                }
	                else
	                {
		                $VoiceClip = " <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif\" border=\"0\" align=\"absmiddle\"  /> ";
	                }
                }                                                
         		$clip = ($Attachment == ""? "":"<span class=\"icon_attachment\"></span>");
         		$detail = $this->returnAnnouncement($AnnouncementID);
         		$description = $detail[0]['Description'];
         		$images = getImageFromText($description);

                $x .= "  <div class=\"portal_module_list".($IsNew?" portal_module_list_new":"")."\">
                            <span class=\"module_icon icon_annoucement\"></span>
                            <div class=\"module_group\">";
				if ($IsNew) {                            
                	$x .= "	<div class=\"module_group_right\"><span class=\"icon_new\"></span></div>";
				}
				$x .= "<div class=\"module_name\"><a href=\"javascript:void(0)\" onclick=\"javascript:fe_view_announcement($AnnouncementID, 0)\">{$AnnouncementTitle}{$clip}{$VoiceClip}</a><em>".$Lang['Portal']['Announcement']['IssuedOn'].": {$AnnouncementDate}</em></div>";
                                
				if (count($images) > 0) {
					foreach((array)$images as $image) {
						$x .= "<a href=\"javascript:fe_view_announcement($AnnouncementID, 0)\" class=\"module_photo\">{$image}</a>";
					}
				}
				$x .= "     </div>
                            <p class=\"spacer\"></p>
                    	</div>";
        	}
			$x .= "<div class=\"portal_module_list_more\"><a href=\"#\" onclick='javascript:moreNews(0)'>".$Lang['Portal']['Announcement']['DisplayAll']."</a></div>";
		}
        return $x;        
	}

	#########################################################################################
	# Amway Customized Announcement  [End]
	#########################################################################################
	
}

?>
