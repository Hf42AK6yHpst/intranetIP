<?php
// using : Frankie
/*
 * Change Log:
 * 2018-07-04 Frankie SSO from DSI
 * 2018-06-14 Frankie SSO for DSI initial
 */

if (!defined("LIBSSODSI_UI_DEFINED"))
{
	define("LIBSSODSI_UI_DEFINED", true);
	
	class libSsoDsi_ui extends interface_html
	{
		
	    public function libSsoDsi_ui($parTemplate='')
	    {
			$template = ($parTemplate=='')? 'default.html' : $parTemplate;
			$this->interface_html($template);
		}
	}
}