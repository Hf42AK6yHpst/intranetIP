<?php
// using : Frankie
/*
 * Change Log:
 * 2018-07-04 Frankie SSO from DSI
 * 2018-06-14 Frankie SSO for DSI initial
 */
if (!defined("LIBSSODSI_DEFINED"))
{
	define("LIBSSODSI_DEFINED", true);
	
	class libSsoDsi extends libdb
	{
		var $ModuleTitle;
		
		public function libSsoDsi()
		{
		    global $singlesignonservice, $ssoservice, $junior_mck, $sys_custom;

		    $this->libdb();
		    $this->valid = $ssoservice["dsi"]["Valid"];
			$this->config = $singlesignonservice['dsi'];
			$this->config['taskSeparator'] = ".";
			$this->config['hex_key'] = bin2hex('vTb0F2vrWUepa9KMJUI8');
			$this->config['pack_key'] = "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3";
			
			$this->config['protocol'] = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
			$this->config['client_host'] = $this->config['protocol'] . "://" . $_SERVER['HTTP_HOST'];
			$this->config['auth_link'] = $this->config['client_host'] . "/sso/dsi";
			$this->config['receive_toekn'] = $this->config['client_host'] . "/sso/dsi?task=auth.token";
			$this->config['token_login'] = $this->config['client_host'] . "/sso/dsi?task=auth.login";

			$current_link_info = parse_url($this->config['auth_link']);
			
            /*****************************************************/		
	        // $this->debugMode = 'dev';
	        /*****************************************************/
			
		    if (file_exists(dirname(dirname(__FILE__)) . "/json.php")) {
		        require_once(dirname(dirname(__FILE__)) . "/json.php");
		        $this->json = new JSON_obj();
		    }

		    $this->isEJ = (isset($junior_mck) && $junior_mck > 0);
		    
		    if (!empty($sys_custom['INTRANET_USER']))
		    {
		        $this->userTable = $sys_custom['INTRANET_USER'];
		    }
		    else
		    {
		        $this->userTable = 'INTRANET_USER';
		    }
		    
		    $this->tokenTable = "INTRANET_SSO_DSI";
		    
		    $this->dateArr = array(
		        'Created_at', 'Modified_at'
		    );
		}
		
		
		/**
		 * Verify referer info
		 * 
		 * @param string $referer
		 * @return boolean
		 */
		public function verifyReferer($referer = '')
		{
		    $referer_host = parse_url($referer);
		    $my_config = parse_url($this->config['url']);
		    
		    if ($referer_host["scheme"] == $my_config["scheme"] &&
		        $referer_host["host"] == $my_config["host"])
		    {
		        return true;
		    } else {
		        return false;
		    }
		}
		
		/**
		 * JSON Encode
		 * 
		 * @param string $string
		 * @return string
		 */
		public function jsonEncode($string = "")
		{
		    return $this->json->encode($string);
		}
		
		/**
		 * JSON Decode
		 * 
		 * @param string $string
		 * @return string
		 */
		public function jsonDecode($string = "")
		{
		    return $this->json->decode($string);
		}
		
		/**
		 * get SSO Dsi Config by Key
		 * 
		 * @param string $key
		 * @return unknown|string
		 */
		public function getConfig($key='')
		{
		    if ($key == "all")
		    {
		        return $this->config;
		    } else {
		        return isset($this->config[$key]) ? $this->config[$key] : "";
		    }
		}
		
		/**
		 * Check SSO DSI is enabled
		 * 
		 * @return boolean
		 */
		public function SsoDsiStatus()
		{
		    return $this->valid;
		}
		
		/**
		 * eClass Default Check Access
		 */
		public function checkAccessRight()
		{
		    global $plugin;
		    $canAccess = true;
		    if (!isset($this->config['passport_client_id']))
		    {
		        $canAccess = false;
		    }
		    if (!$this->valid)
		    {
		        No_Access_Right_Pop_Up();
		    }
		    
		    if (!in_array($_SESSION['UserType'], array(1))) {
		        No_Access_Right_Pop_Up();
		    }
		    
		    if (!$canAccess) {
		        No_Access_Right_Pop_Up();
		    }
		    if (empty($this->config['url']))
		    {
		        No_Access_Right_Pop_Up();
		    }
		}
		
		/**
		 * library initial Setting
		 */
		public function setDefaultSettings()
		{
		    
		}
		
		/**
		 * Get Real Client IP
		 * 
		 * @param unknown $default
		 * @param number $filter_options
		 * @return string|null
		 */
	    public function get_client_real_ip($default = NULL, $filter_options = 12582912)
	    {
	        $ip = null;
	        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	        {
	            $ip=$_SERVER['HTTP_CLIENT_IP'];
	        }
	        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	        {
	            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	        }
	        elseif (isset($_SERVER['REMOTE_ADDR']) && ($_SERVER['REMOTE_ADDR'] != '') && ($_SERVER['REMOTE_ADDR'] != NULL))
	        {
	            $ip = $_SERVER['REMOTE_ADDR'];
	        }
	        return $ip;
	    }
		
	    /**
	     * MySql AES Encrypt / Decrypt
	     * 
	     * @param string $type
	     * @param string $string
	     * @param string $key
	     * @param string $isLocal
	     * @return string
	     */
		public function db_encrypt_decrypt($type='encrypt', $string='', $key='', $isLocal = false)
		{
		    if ($isLocal)
		    {
		        $serverPublicCode = bin2hex('local');
		        $clientPublicCode = bin2hex('local');
		    } else {
		        $serverPublicCode = bin2hex('DSI2018');
		        $clientPublicCode = bin2hex('eClass2018');
		    }
	        if ($type == 'encrypt')
	        {
	            $sql = "SELECT AES_ENCRYPT('" . $string . "', UNHEX('" . $key . $clientPublicCode . "')) as code";
	            $result = $this->returnResultSet($sql);
	            
	            return  base64_encode($result[0]['code']);
	        } else {
	            $sql = "SELECT AES_DECRYPT('" . addslashes(base64_decode($string)) . "', UNHEX('" . $key . $serverPublicCode . "')) as code";
	            $result = $this->returnResultSet($sql);
	            return  $result[0]['code'];
	        }
		}
		
		/**
		 * get Token Information by User ID
		 * 
		 * @param string $user_id
		 * @return array|NULL
		 */
		public function getTokenByUserID($user_id = '')
		{
		    $sql = "SELECT 
                        AuthType, ExpiresIn, AToken, RToken, Created_at, Modified_at 
                    FROM " . $this->tokenTable . " AS tt
                    JOIN " . $this->userTable . " AS ut ON (tt.UserID=ut.UserID and ut.RecordStatus='1' and ut.RecordType='1')
                    where tt.UserID='" . $user_id . "'";
		    $result = $this->returnResultSet($sql);
		    if (!empty($result) && count($result) > 0)
		    {
		        return $result[0];
		    } else {
		        return null;
		    }
		}
		
		/**
		 * build Insert SQL by Param
		 * 
		 * @param string $table
		 * @param array $param
		 * @return boolean|string
		 */
		public function buildInsertSQL($table = "", $param = array())
		{
		    $sql = "INSERT INTO " . $table . " (" . implode(", ", array_keys($param)) . ") VALUES (";
		    $value_sql = "";
		    if (count($param) > 0)
		    {
		        foreach ($param as $key => $val)
		        {
		            if (!empty($value_sql))
		            {
		                $value_sql .= ", ";
		            }
		            if (empty($val))
		            {
		                return false;
		            }
		            switch ($val)
		            {
		                case "now":
		                    if (in_array($key, $this->dateArr))
		                    {
		                        $value_sql .= "NOW()";
		                    } else {
		                        $value_sql .= "'" . $val . "'";
		                    }
		                    break;
		                default:
		                    $value_sql .= "'" . $val . "'";
		                    break;
		            }
		        }
		    } else {
		        return false;
		    }
		    $sql .= $value_sql . ")";
		    return $sql;
		}
		
		/**
		 * build Update SQL by Param
		 * 
		 * @param string $table
		 * @param array $param
		 * @param string $condition
		 * @return boolean|string
		 */
		public function buildUpdateSQL($table = "", $param = array(), $condition = '')
		{
		    $sql = "UPDATE " . $table . " SET ";
		    $value_sql = "";
		    if (count($param) > 0)
		    {
		        foreach ($param as $key => $val)
		        {
		            if (!empty($value_sql))
		            {
		                $value_sql .= ", ";
		            }
		            if (empty($val))
		            {
		                return false;
		            }
		            switch ($val)
		            {
		                case "now":
		                    if (in_array($key, $this->dateArr))
		                    {
		                        $value_sql .= $key ."=NOW()";
		                    } else {
		                        $value_sql .= $key . "='" . $val . "'";
		                    }
		                    break;
		                default:
		                    $value_sql .= $key . "='" . $val . "'";
		                    break;
		            }
		        }
		    } else {
		        return false;
		    }
		    $sql .= $value_sql . " WHERE " . $condition;
		    return $sql;
		}
		
		
		/**
		 * Insert or Update Token Information
		 * 
		 * @param string $user_id
		 * @param array $jsonArr
		 * @return query_result|boolean
		 */
		public function insertOrUpdateTokenInfo($user_id = '', $jsonArr = array())
		{
		    
		    $existingInfo = $this->getTokenByUserID($user_id);
		    if ($existingInfo == null) {
		        $param = array(
		            "UserID" => $user_id,
		            "AuthType" => $jsonArr['token_type'],
		            "ExpiresIn" => $jsonArr['expires_in'],
		            "AToken" => $jsonArr['access_token'],
		            "RToken" => $jsonArr['refresh_token'],
		            "Created_at" => 'now',
		            "Modified_at" => 'now'
		        );
		        $sql = $this->buildInsertSQL($this->tokenTable, $param);
		    } else {
		        $param = array(
		            "AuthType" => $jsonArr['token_type'],
		            "ExpiresIn" => $jsonArr['expires_in'],
		            "AToken" => $jsonArr['access_token'],
		            "RToken" => $jsonArr['refresh_token'],
		            "Modified_at" => 'now'
		        );
		        $sql = $this->buildUpdateSQL($this->tokenTable, $param, "UserID='" . $user_id . "'");
		    }
		    if ($sql !== false) {
		        return $this->db_db_query($sql);
		    } else {
		        return false;
		    }
		}
	}
}