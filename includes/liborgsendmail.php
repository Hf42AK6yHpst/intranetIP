<?php

// editing by 
/******************************* Change log *********************************
 * 2016-09-29 (Carlos): Modified do_sendmail_nonbcc() added return email.
 * 2013-11-11 (Yuen) : extended do_sendmail_nonbcc() to header par
 * 2012-06-19 (Yuen) : extended build_html_message() to support attachments
 * 2011-05-06 (Carlos) : added build_html_message(), modified do_sendmail_nonbcc
 * 2011-04-04 (Carlos) : modified SendMail() to send html message
 ****************************************************************************/
 
 
class liborgsendmail extends libemail {

     var $sendmail_path = "/usr/lib/sendmail";
	
     function do_sendmail($sender_email, $sender_name, $recipient_email, $recipient_name, $title, $body, $charset="", $attachment=""){
          $subject = $title;
          $message = $body;
          $from_name = $sender_name;
          $from_address = $sender_email;
          $reply_name = $from_name;
          $reply_address = $from_address;
          $reply_address = $from_address;
          $error_delivery_name = $from_name;
          $error_delivery_address = $from_address;
          $to_name = $from_name;
          $to_address = $from_address;

          # Divide the address
          $bcc_name = $recipient_name;
          $bcc_address = $recipient_email;
          
          ## check if the recipient name contain comma (,) ##
          if(strpos($to_name,",")>0)
          	$to_name = '"'.$to_name.'"';
          	
          if(strpos($from_name,",")>0)
          	$from_name = '"'.$from_name.'"';
          	
          if(strpos($reply_name,",")>0)
          	$reply_name = '"'.$reply_name.'"';
          	
          if(strpos($error_delivery_name,",")>0)
          	$error_delivery_name = '"'.$error_delivery_name.'"';

          if(     !strcmp($error=$this->SetEncodedEmailHeader("To",$to_address,$to_name),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("Bcc",$bcc_address,$bcc_name),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("From",$from_address,$from_name),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("Reply-To",$reply_address,$reply_name),"")
               && !strcmp($error=$this->SetHeader("Return-Path",$error_delivery_address),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("Errors-To",$error_delivery_address,$error_delivery_name),"")
               && !strcmp($error=$this->SetEncodedHeader("Subject",$subject,$charset),"")
               && !strcmp($this->AddQuotedPrintableTextPart($this->WrapText($message),$charset),""))
			{
				/*
				  
				 if ($attachment!="" && file_exists($attachment))
				{
					$this->AddFilePart($attachment);
				}
				*/
               $error=$this->Send();
			}

           return $error;
     }

     function SendMail($to,$subject,$body,$headers){
          $command=$this->sendmail_path." -t";
          if(IsSet($this->delivery["Headers"])){
               $headers_values=$this->delivery["Headers"];
               for($return_path="",$header=0,Reset($headers_values);$header<count($headers_values);$header++,Next($headers_values)){
                    if(strtolower(Key($headers_values))=="return-path"){
                         $return_path=$headers_values[Key($headers_values)];
                         break;
                    }
               }
               if(strcmp($return_path,"")) $command.=" -f $return_path";
          }
          #global $webmaster;
          #$from = "eClass Intranet Administrator <$webmaster>";
		  
		  $message = $this->format_html_message($to,$from,$subject,$headers,$body);			  
          if(!($pipe=popen($command,"w")))
               return("it was not possible to open sendmail input pipe");

          //if(!fputs($pipe,"To: $to\n") || !fputs($pipe,"From: $from\n") || !fputs($pipe,"Subject: $subject\n") || ($headers!="" && !fputs($pipe,"$headers\n")) || !fputs($pipe,"\n$body"))
          //     return("it was not possible to write sendmail input pipe");
		  
		  if(!fputs($pipe,$message))
               return("it was not possible to write sendmail input pipe");
		  
          pclose($pipe);
          return("");
     }
/*
     function do_sendmail_nonbcc($sender_email, $sender_name, $recipient_email, $recipient_name, $title, $body){
     #echo "Sending nonbcc\n";
          $subject = $title;
          $message = $body;
          $from_name = $sender_name;
          $from_address = $sender_email;
          #$reply_name = $from_name;
          #$reply_address = $from_address;
          #$reply_address = $from_address;
          $error_delivery_name = $from_name;
          $error_delivery_address = $from_address;
          $to_name = $recipient_name;
          $to_address = $recipient_email;

          if(     !strcmp($error=$this->SetEncodedEmailHeader("To",$to_address,$to_name),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("Bcc",$bcc_address,$bcc_name),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("From",$from_address,$from_name),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("Reply-To",$reply_address,$reply_name),"")
               && !strcmp($error=$this->SetHeader("Return-Path",$error_delivery_address),"")
               && !strcmp($error=$this->SetEncodedEmailHeader("Errors-To",$error_delivery_address,$error_delivery_name),"")
               && !strcmp($error=$this->SetEncodedHeader("Subject",$subject),"")
               && !strcmp($this->AddQuotedPrintableTextPart($this->WrapText($message)),""))
               $error=$this->Send();

           return $error;
     }
*/

     function do_sendmail_nonbcc($sender_email, $sender_name, $recipient_email, $recipient_name, $title, $body, $attachment="", $header=""){
     	global $intranet_root;
          /*
          $subject = $title;
          $message = $body;
          $from_name = $sender_name;
          $from_address = $sender_email;
          $error_delivery_name = $from_name;
          $error_delivery_address = $from_address;
          $to_name = $recipient_name;
          $to_address = $recipient_email;
          $headers = "From: $from_address\r\n";
			*/	
		  $mime = $this->build_html_message($recipient_email,$sender_email,$sender_name,$title,$header,$body, "UTF-8", $attachment);
		  $to_address = $mime['to'];
		  $subject = $mime['subject'];
		  $message = $mime['body'];
		  $headers = $mime['headers'];
			//write_file_content($message, $intranet_root."/file/mailmerge_attachment/sendloglib4.txt");
          return mail($to_address,$subject,$message,"$headers", "-f $sender_email");
     }
     
     function format_mail_part($string, $encoding='UTF-8')
     {
		$string = base64_encode($string);
	  	$string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?B?", $string) ;
		$encoded_string = '=?'.$encoding.'?B?'.$string.'?=';
	  	return $encoded_string;
	 }
	 
	 function format_html_message($to,$from,$subject,$headers,$body,$encoding="UTF-8")
	 {
	 	$lbr = chr(10);
	 	$mime = "";
	 	
	 	$mime .= "To: $to".$lbr;
	 	$mime .= "From: $from".$lbr;
	 	$mime .= "Subject: ".$this->format_mail_part($subject,$encoding).$lbr;
	 	if(trim($headers)!="") $mime .= trim($headers).$lbr;
	 	$mime .= "MIME-Version: 1.0".$lbr;
		$mime .= "Content-Type: text/html;".$lbr;
	    $mime .= "\tcharset=\"$encoding\"".$lbr;
	    $mime .= "Content-Transfer-Encoding: base64".$lbr;
	 	$mime .= $lbr;
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$body."</BODY></HTML>\n"));
        $mime .= $lbr.$lbr;
        
        return $mime;
	 }
	 
	 function build_html_message($to,$from,$sender_name,$subject,$headers,$body,$encoding="UTF-8", $files="")
	 {
	 	global $intranet_root;
	 	
	 	$lbr = chr(10);
	 	
	 	$mail_to = (is_array($to) && sizeof($to)>0)?implode(", ",$to):$to;
	 	
	 	$mail_subject = $this->format_mail_part($subject,$encoding);
	 	
	 	
	    $semi_rand = md5(time()); 
	    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
	 	
	 	$sender = (trim($sender_name)!="" && stristr($from,"@")!=false)?($this->format_mail_part($sender_name,$encoding)."<".$from.">"):$from;
	 	$mail_headers .= "From: $sender".$lbr;
	 	if(trim($headers)!="") $mail_headers .= trim($headers).$lbr;
	 	$mail_headers .= "MIME-Version: 1.0".$lbr;
	 	$mail_headers .= "Content-Type: multipart/mixed;" . " boundary=\"{$mime_boundary}\"";
	 	
	 	
	    $mail_body = "--{$mime_boundary}".$lbr;
		$mail_body .= "Content-Type: text/html; ";
	    $mail_body .= "charset=\"$encoding\"".$lbr;
	    $mail_body .= "Content-Transfer-Encoding: base64".$lbr.$lbr;
	 	$mail_body .= chunk_split(base64_encode("<HTML><BODY>\n".$body."</BODY></HTML>\n")).$lbr;
	 	
	 	// preparing attachments
	 	if (!is_array($files) && $files!="")
	 	{
	 		$tmpFiles[] = $files;
	 		$files = $tmpFiles;
	 	}
	 	// boundary 
	 	
	 	$msg_attachment = "";
	    for ($i=0;$i<count($files);$i++)
	    {
	        if(is_file($files[$i]))
	        {
	            $msg_attachment .= "--{$mime_boundary}\n";
	            $data = chunk_split(base64_encode(get_file_content($files[$i])));
	            $msg_attachment .= "Content-Type: application/octet-stream; name=\"".basename($files[$i])."\"\n" . 
	         //   "Content-Description: ".basename($files[$i])."\n" .
	            "Content-Disposition: attachment;\n" . " filename=\"".basename($files[$i])."\"; size=".filesize($files[$i]).";\n" . 
	            "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
	        }
	    }
	    $msg_attachment .= "--{$mime_boundary}--";

	 	
	 	//write_file_content($message, $intranet_root."/file/mailmerge_attachment/sendloglib5.txt");
	 	return array("to"=>$mail_to,
					"subject"=>$mail_subject,
					"headers"=>$mail_headers,
					"body"=>$mail_body.$msg_attachment);
	 }
};
?>
