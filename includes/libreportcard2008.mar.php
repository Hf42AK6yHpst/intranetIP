<?php
#  Editing by Connie
/* ***************************************************
 * Modification Log
 * 20111108 Connie: 
 * 		- added  Get_eDiscipline_Conduct_Data()
 * 20111104 Connie:  Get_eDiscipline_Conduct_Data
 * 		- modified Get_Term_Selection() added para $isMultiple
 * 20111028 Ivan:
 * 		- modified GET_FORM_NUMBER() added para $ByWebSAMSCode to retrieve the Form Number by WebSAMS Code since some client may use Chinese as the Form Name
 * 20111024 Connie:
 * 		- added Convert_ClassOfForm_ToInt($StudentID) to return Class of Form Number (1,2,3......) 
 * 20111018 Ivan:
 * 		- added Get_Scheme_Range_Info($SchemeRangeID) to return the Grade 
 * 20111012 Marcus:
 * 		- fixed Get_Manual_Adjustment, do not return Associative array only as list cannot retrieve data from assoc array 
 * 20111007 Ivan:
 * 		- Modified returnSubjectwOrderNoL added para $SubjectFieldLang
 * 20110921 Connie:
 * 		- Added Get_Student_Trial_Promotion_Status
 * 20110920 Ivan:
 * 		- added Get_Subject_WebSAMSCode_Config_Array(), Get_Subject_WebSAMSCode_From_Config() to get the hardcoded WebSAMS Code of a Specific Subject
 * 20110919 Ivan:
 * 		- modified returnSubjectTeacherCommentByBatch(), added AdditionalComment in the return array
 * 20110829 Ivan:
 * 		- added marksheet estimated score related functions
 * 20110818 Ivan:
 * 		- modified GET_TEACHER_COMMENT(), added Update_Teacher_Comment() and Insert_Teacher_Comment() for adding Additional Comment for Class Teacher Comment
 * 20110726 Marcus:
 * 		- modified getPersonalCharacteristicsData, add return value comment
 * 20110706 Marcus:
 * 		- fixed Convert_MarksArr_Mark_To_Grade, for TopPercentage = 2, excluded "unrank" student from number of passed student
 * 		- modified CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME, CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS, CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS cast Marks to float to avoid comparing problem   
 * 20110705 Ivan:
 * 		- added Preload related functions (Get_Preload_Result, Check_PreloadInfoArr_Exist, Set_Preload_Result, Get_PreloadInfoArrVariableStr)
 * 		- modified Get_Manual_Adjustment, getMarksFromMarksheet, returnReportTemplateSubjectWeightData to apply the preload logic
 * 		- modified GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED to improve performance
 * 20110629 Ivan:
 * 		- fixed CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS and CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS, skip scheme range if the scheme do not have Upper Limit
 * 20110628 Marcus:
 * 		- fixed CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME, for TopPercentage = 2, excluded "unrank" student from number of passed student   
 * 20110621 Marcus:
 * 		- modified checkSubjectTeacher, the original function was designed for old erc 
 * 20110620 Ivan:
 * 		- added getPersonalCharacteristicsOptionTitleByID() to get the display of the Personal char
 * 20110617 Ivan:
 * 		- modified Load_Batch_Subject_Form_Grading_Scheme() to retrieve the Grand Subject at last which is same as the UI display
 * 20110613 Marcus:
 * 		- modified returnSubjectTeacherClass(), add UserIDCond
 * 20110609 Ivan:
 * 		- modified getOtherInfoData(), added Form-based csv info logic
 * 20110609 Marcus:
 *		- modified INSERT_GRADING_SCHEME_RANGE, UPDATE_GRADING_SCHEME_RANGE, cater grading scheme grade description 
 * 20110609 Ivan:
 * 		- added getMarksFromMarksheet() to simulate the getMarks function by getting the marks from marksheet
 * 20110609 Marcus:
 * 		- modified Load_Batch_Grading_Scheme_Range_Info, added description for a grade
 * 20110607 Ivan:
 * 		- modified Load_Batch_Student_Class_ClassLevel_Info() added HKID, DateOfBirth, Gender info
 * 		- added Load_Batch_Student_House_Info, Get_Student_House_Info() to retrieve students' House info
 * 20110527 Ivan:
 * 		- modified getPersonalCharacteristicsData() since the data here may contains some last year records, extra coding is added to get the current year student info only
 * 20110524 Ivan:
 * 		- modified getReportResultScore() to add batch load logic
 * 		- modified getMarks and getReportResultScore to add ordering logic
 * 		- modified getPersonalCharacteristicsProcessedDataByBatch() added para $ReturnGradeCode, $ReturnCharID to return the GradeCode and CharID instead of the Name
 * 		- added Add_SubjectID_In_GrandMarkArr() to syn the array Format of getReportResultScore() with getMarks() for easier looping 
 * 		- added Check_MarkNature_Is_Pass()
 * 20110520 Marcus:
 * 		- added returnSubjectTeacherCommentByBatch, getPersonalCharacteristicsProcessedDataByBatch, Get_Semester_Seq_Number, Get_Student_Selection_By_Class 
 * 		- modified getPersonalCharacteristicsData, support get data by batch
 * 		- Copy function from Rubrics, Get_User_Accessible_Form, Get_User_Accessible_Class, Get_User_Accessible_Subject,  Get_User_Teaching_Subject 
 * 20110519 Ivan:
 * 		- modified Get_Form_Grading_Scheme() to get the Subject FullMark info also
 * 20110503 Ivan [CRM:2011-0503-0804-14067]:
 * 		- modified returnReportTemplateTerms() to get the "whole year" term also
 * 20110404 Marcus:
 * 		- modified GET_MARKSHEET_SCORE, GET_MARKSHEET_OVERALL_SCORE, Add param to Skip Subject Group Checking for marksheet_edit 
 * 20110401 Marcus:
 * 		- modified getMarks, manual adjusted OrderMeritSubjectGroup value from OrderMeritClass to OrderMeritSubjectGroup
 * 20110329 Ivan:
 * 		- modified Get_Score_Display_HTML(), will not round the "Numeric Grade" now 
 * 20110328 Marcus:
 * 		- fixed isMSComplete, return false if no progress record.  
 * 	20110310 Ivan:
 * 		- fixed returnSubjectwOrder(), wrong Subject Component ordering
 *  20110310 Marcus:
 * 		- fixed COPY_CLASSLEVEL_SETTINGS, cater copying subject ordeing. 
 *  20110309 Marcus:
 * 		- fixed Get_Student_Subject_Mark_Nature_Arr, use mark to return nature if ScaleDisplay=="M", even if Grade exist. 
 * 20110223 Ivan:
 * 		- fixed returnMarkNature - retrieve Subject Column weight instead of Default Column weight
 * 20110222 Ivan:
 * 		- fixed returnSubjectwOrder fixed wrong subject ordering
 * 20110221 Marcus:
 * 		- fixed returnMarkNature check mark with weighted pass mark only if $UseWeightedMark = 1
 * 
 * 20110209 Marcus:
 * 		- modified Get_Academic_Progress, Get_Academic_Progress_View_Sql, Get Progress Report with SubjectID < 0 for grand marks  
 * 		- modified Get_Order_Criteria_Selection, added GrandSDScore for school whose position are determined by WeightedSD
 * 20110114 Ivan:
 * 		- modified GET_COMMENT_BANK_SUGGEST(), get Parent Subject Comment Bank if the Subject is a Component
 * 20110106 Ivan:
 * 		- modified returnSubjectTeacherClassInfo() to correct the SQL statement of checking if the user is a Subject Teacher
 * 20110105 Ivan:
 * 		- added Get_Student_Class_Teacher_Info() to retrieve Student's Class Teacher Info
 * 20110103 Ivan:
 * 		- modified GET_SUBJECT_NAME_LANG(), if $ParLang = 'ch', display Chinese Subject Name also
 * 20101221 Ivan:
 * 		- modified GET_ALL_SUBJECTS(), fixed the wrong subject order in Settings > Subject & Class Level
 * 20101217 Marcus:
 * 		- modified CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS, CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS
 * 		- add checking of whether FormPosition >0 if  SpecialGradeConvertingForSmallClassGroup is on.
 * 20101216 Ivan:
 * 		- modified ReportTemplateTermCopy(), fixed cannot copy Main Term Report if the Form has an Extra Term Report
 * 20101213 Ivan:
 * 		- modified returnClassTeacherForm(), fixed cannot get the correct teaching Form problem
 * 20101209 Marcus:
 * 		- modified getMinimum, getMaximun, added array_values to avoid miscalculation due to uncontinuous key value. 
 * 20101208 Marcus:
 * 		- modified Get_Subject_Group_List_Of_Report, join SUBJECT_TERM_CLASS_YEAR_RELATION
 * 		- modified Get_Scheme_Grade_Statistics_Info, filter students of other forms 
 * 20101202 Ivan:
 * 		- modified getReportResultScore(), getMarks(), added Get_Subject_Passing_Mark(), fixed should compare the passing mark instead of the average mark (for the Subject Position Customization only)
 * 20101201 Ivan:
 * 		- modified getReportResultScore(), fixed getting the display of Form/Class/Stream Position, system wrongly compare the Position with the GrandAverage (for the Subject Position Customization only)
 * 20101109 Ivan:
 * 		- modified getOtherInfoTabObjArr(), use the sequence in $this->configFilesType to display the tag in Other Info (modified all customized library already)
 * 20101020 Marcus:
 * 		- modified DELETE_MARKSHEET_SCORE, add 2 params $SubjectID, $StudentID
 * 		- modified Get_Subject_Selection, add $ExceptWithoutSubjectGroup
 * 20101020 Marcus:
 * 		- modified Get_Subject_Selection, add param $ParentSubjectAsOptGroup, use GET_SELECTION_BOX_WITH_OPTGROUP instead of getSelectByAssoArray
 * 20101004 Marcus:
 * 		- modified Get_Order_Criteria_Selection, default selected Get_Grand_Position_Determine_Field
 * 20101004 Marcus:
 * 		- modified GET_STUDENT_BY_CLASSLEVEL, change Inner join INTRANET_USER to LEFT JOIN.
 * 20100906 Ivan:
 * 		- modified getReportOtherInfoData, added para $ArrayWithType for St. Stephen College Template Migration
 * 
 * 20100802 Marcus:
 * 		- modified CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME, CONVERT_MARK_TO_GRADE_POINT_FROM_GRADING_SCHEME, Convert_MarksArr_Mark_To_Grade
 * 			- added flag $eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup'] for Munsang
 * 		- Added CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS, CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS
 * 			- to syn CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME, Convert_MarksArr_Mark_To_Grade Convert to grade logic.
 * 20100802 Marcus:
 * 		- modified getReportOtherInfoData (pass $ClassLevelID to getOtherInfoData), getOtherInfoData (add param $ClassLevelID, Cater new csv format)
 * 20100730 Marcus:
 * 		- modified GET_STUDENT_BY_CLASSLEVEL, Add return value UserLogin
 * 20100720 Marcus:
 * 		- modified getReportOtherInfoData, added Get_Student_Profile_Attendance_Data, for munsang, get attendance record from student profile in stead of csv
 * 20100719 Marcus:
 * 		- modified getFormSubjectSDAndAverage , getMarks, Get_Scheme_Grade_Statistics_Info, Convert_MarksArr_Mark_To_Grade 
 * 		- to improve performance in grade distribution 
 * 20100708 Ivan:
 * 		- modified getReportResultScore(), added the case of Stream Position
 * 		- modified CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME(), add Round in the SQL for float comparison
 * 20100706 Marcus:
 * 		- modified getPromotionStatus(), skip SpecialCase Subject
 * 20100702 Marcus:
 * 		- modified Convert_MarksArr_Mark_To_Grade, if original grade is grade  Get_Exclude_Ranking_Special_Case(), do not convert
 * 20100625 Marcus:
 * 		- fixed Get_Related_TermReport_Of_Consolidated_Report, function consider ReportColumnID == '' means get All ReportColumn, actually  ReportColumnID == '' means overall column
 *			now ReportColumn == '' or ReportColumn==0 will get overall column
 * 		- modified returnReportTemplateBasicInfo , change $isMainReport default to 1
 * 20100609 Marcus:
 * 		- add Master Report to GET_MODULE_OBJ_ARR
 * 20100603 Marcus:
 * 		- modified getPassingRate Subject Selection (Add Param includeGrandMark, OtherTagInfo)
 * 20100603 Marcus:
 * 		- modified getPassingRate,getPassingNumber to return fail number/rate
 * 		- added  Get_Student_Grand_Mark_Nature_Arr, Get_SubjectID_Grand_Field_Arr, Sort_UserID_By_Position, Convert_MarksArr_Mark_To_Grade
 * 20100602 Marcus:
 * 		- added Get_Display_Mark_By_Grading_Scheme() 
 * 20100531 Ivan:
 * 		- added Get_User_Array_From_Selection(), Get_Available_User_To_Add_Comment() and Append_Class_Teacher_Comment() for class teacher comment input
 * 20100531 Marcus:
 * 		- fixed getMarks , order by DisplayOrder instead of b.DisplayOrder
 * 20100514 Ivan:
 * 		- added function Get_Exclude_Ranking_Special_Case()
 * 20100513 Ivan:
 * 		- added Student Academic Progress related functions
 * 		- added function Get_Related_TermReport_Of_Consolidated_Report
 * 		- modified function getCorespondingTermReportID
 * 20100510 Ivan:
 * 		- added function Get_Mark_Statistics_Info and Get_Student_Subject_Mark_Nature_Arr
 * 20100505 Marcus:
 * 		- fixed GET_COMPONENT_SUBJECT, if pass a cmp subject id to the function, the sql wrongly return the other cmp subject of its parent subject.
 * 20100407 Marcus:
 * 		- modified GET_ALL_SUB_MARKSHEET_COLUMN add 1 more return value "FullMark", also added in DB 
 * 20100303 Marcus:
 * 		- modified getPromotionStatus(), change PromotionStatus to value(1,2) instead of text(Promoted...)
 * 20100302 Marcus:
 * 		- added GetPromotionGenerationDate()
 * 20100218 Ivan (Handle student who has deleted from the subject group):
 * 		- modified GET_MARKSHEET_SCORE(), GET_PARENT_SUBJECT_MARKSHEET_SCORE(), GET_MARKSHEET_OVERALL_SCORE(), Is_Student_In_Subject_Group_Of_Subject()
 *		- added Get_Student_In_Current_Subject_Group_Of_Subject()
 * 20100218 Marcus:
 * 		- modified getPassingRate (excluded NA student)
 * 		- added GET_SPECIAL_CASE_SET1_STRING_ARR() (return assoc array of Possible special character)
 *  	- added GET_SPECIAL_CASE_SET2_STRING_ARR()
 * 		- added CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($CheckVal) (Check if converted marks is specail case)
 *  	- added CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($CheckVal)
 * 20100211 Marcus:
 * 		- modified returnMarkNature (add param $ConvertBy)
 * 20100210 Marcus: 
 *		- modified getReportResultScore (add return value GrandSDScore )
 * 20100210 Marcus:
 *		- modified getFormSubjectSDAndAverage (add Param $ReportColumnID )
 * 20100204 Marcus:
 *		- modified getOtherInfoTabObjArr (add "others" tag for csv import)
 * 20100129 Marcus:
 * 		- CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME (For Percentage Range, cater convert from GPA, GrandTotal in addition to GrandAverage)
 * 20100118 Marcus:
 *		- modified checkSpCase (add conditions to check whether $thisMark is special case)
 * 20100114 Ivan:
 *		- modified returnReportTemplateBasicInfo (add field ShowSubjectComponent)
 * 20100111 Marcus:
 * 		- modified returnPersonalCharSettingData, change condition if($SubjectID) to if($SubjectID || $SubjectID!='') so that to get overall setting correctly
 * 		- add 2 function about personal characteristic getPersonalCharacteristicsProcessedData and returnPersonalCharTitleByID
 * 20100107 Ivan:
 * 		- modified GET_COMPONENT_SUBJECT - added Order By b.DisplayOrder
 * 20100107 Marcus:
 * 		- modified returnSubjectwOrder, GET_COMPONENT_SUBJECT add $ParDisplayType to return Subject Display type Abbr/Desc/ShortName 
 * 20100106 Marcus:
 *		- added param $HideNonGenerated and $OtherTags to Get_Report_Selection
 * 20100104 Ivan:
 *		- added Subject Criteria and Personal Characteristics related functions
 * 20100104 Marcus:
 *		- added FormatDate - simple date formating function
 * 20091231 Ivan:
 *		- moved GenReportList, GenReportList_To_iPortfolio, GenSubMarksheetReportList, GenAssessmentReportList to the ui library
 * 20091230 Ivan:
 *		- added Position Display Settings related functions
 *		- added Get_Report_Average_Mark, Get_Grand_Position_Determine_Field, Get_Grand_Field_SubjectID
 *		- modified getMarks() to support position display settings
 * 20091223 Marcus:
 * 		- modified Get_Scheme_Grade_Statistics_Info (add Param $forSubjectGroup to get Stat of SubjectGroup)
 * 		- modified getVariance (return 0 if array contain 1 element only)
 * 		- modified getSD, getAverage (also round 0 to given decimal point)
 * 20091221 Marcus:
 * 		- CHANGE_FROM_SUBJECT_GRADING, UPDATE_SUBJECT_FORM_GRADING, INSERT_SUBJECT_FORM_GRADING, GET_SUBJECT_FORM_GRADING (add langdisplay & DisplaySubjectGroup)
 * 20091216 Marcus:
 * 		- returnSubjectFullMark (add parm $ParScaleInput to get Fullmark accordding to ScaleInput instead of ScaleDisplay)
 * 20091216 Marcus:
 * 		- Get_Subject_Mark_Nature_Arr (fixed can't get Nature of Grade by add checking to $ScaleDisplay)
 * 20091215 Ivan:
 * 		- CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME (For Percentage Range, skip students whose grade is 'N.A.' in the grading calculation)
 * 20091215 Marcus:
 *    	- Added function getFormSubjectSDAndAverage ($ReportID, $SubjectID)  
 * 20091211 Kit:
 *    - getPromotionCriteria(add selected field (DateModified,LastModifiedBy,UserName))  
 * 20091208 Ivan: 
 * 		- Added Get_Student_Studying_Subject_Group (for subject group ranking)
 * 20091208 Marcus: 
 * 		- Delete GetClassInfoOfUser (Function Duplicated)
 * 		- GET_GRADING_SCHEME_RANGE_INFO (order by DisplayOrder, GradingSchemeRangeID instead of GradingSchemeRangeID)
 * 20091207 Ivan: 
 * 		- GET_MODULE_OBJ_ARR (add reports Mark-Grade Conversion, Mark-Position Conversion)
 * 20091204 Marcus:
 * 		- Get_Scheme_Grade_Statistics_Info (add Nature of each grade in which ClassID is 0(Form))
 * 			for Counting Pass Student
 * 20091203 Marcus: 
 * 		- GET_MODULE_OBJ_ARR (add reports ExaminationSummary, SubjectSummarySheet, GradeDistribution)
 * 		- GET_SUBJECT_NAME_LANG (add Parameter to get SNAME instead of DES)
 * 		- Get_GrandMS_CSS (add summary_header to stylesheet (for Report Examination Summary))
 * ***************************************************/

# In case of not setting $ReportCardCustomSchoolName in includes/settings.php, set it to "general" by default
if ($_GET['CustomSchoolCode'] != '') {
	$ReportCardCustomSchoolName = $_GET['CustomSchoolCode'];
}
else {
	if (!isset($ReportCardCustomSchoolName)) {
		$ReportCardCustomSchoolName = "general";
	}
}
if (!$NoLangWordings) {
	include_once($intranet_root."/lang/reportcard2008_lang.$intranet_session_language.php");
	include_once($intranet_root."/lang/reportcard_custom/$ReportCardCustomSchoolName.$intranet_session_language.php");
}
if (is_file($intranet_root."/includes/eRCConfig.php")) {
	include_once($intranet_root."/includes/eRCConfig.php");
}

//if (is_file($intranet_root."/addon/install/other_info_config.php")) 
//{
//	include_once($intranet_root."/addon/install/other_info_config.php");
//}
class libreportcard extends libdb {
	/**
	 * Constructor
	 */
	# variable to store batch record once  
	var $Subject_Form_Grading_Scheme;
	var $Grading_Scheme_Main_Info;
	var $Grading_Scheme_Range_Info; 
	var $PreloadInfoArr;
	
//	public static $OTHER_INFO_ITEM_TYPE_STR = 1;
//	public static $OTHER_INFO_ITEM_TYPE_NUM = 2;
//	public static $OTHER_INFO_ITEM_TYPE_GRADE = 3;
	  
	private $MS_EstimateScoreSymbol;
	 
	function libreportcard() {
		global $intranet_root, $ReportCardCustomSchoolName, $PATH_WRT_ROOT;
		
		$this->libdb();
		$this->uid = $_SESSION['UserID'];
		
		$this->schoolYear = $this->GET_ACTIVE_YEAR();
		$this->schoolYearID = $this->GET_ACTIVE_YEAR_ID();
		$this->DBName = $this->GET_DATABASE_NAME($this->schoolYear);
		
		// For standardlize form validation
		$this->textAreaMaxChar = 255;
		$this->textAreaMaxChar_SubjectTeacherComment = 255;
		
		// Two sets of special cases symbols
		$this->specialCasesSet1 = array("+", "-", "*", "/", "N.A.");
		$this->specialCasesSet2 = array("abs", "*", "/", "N.A.");
		$this->specialCasesSetExcludeRanking = array("-", "abs", "*", "/", "N.A.");
		
		// Other Info definition, Type & CSV Path 
		$this->configFilesPath = $intranet_root."/home/eAdmin/StudentMgmt/eReportCard/management/other_info/".$ReportCardCustomSchoolName."/";
		$this->configFilesType = array("summary","award","merit","eca","remark","interschool","schoolservice","attendance");
		$this->dataFilesPath = $intranet_root."/file/reportcard2008/".$this->schoolYear."/";
		
		// System Settings definition, for error checking & in-code reference
		$this->SettingDefinition["System"] = array("SchoolYear");
		$this->SettingDefinition["HighLight"] = array("Fail", "Pass", "Distinction");
		$this->SettingDefinition["Storage&Display"] = array("SubjectScore", "SubjectTotal", "GrandAverage", "GrandTotal");
		$this->SettingDefinition["Absent&Exempt"] = array("Absent", "Exempt");
		$this->SettingDefinition["Calculation"] = array("UseWeightedMark", "AdjustToFullMarkFirst", "OrderTerm", "OrderFullYear");
		$this->SettingDefinition["AccessSettings"] = array("AllowClassTeacherUploadCSV", "AllowTeacherAccessGrandMS");
		$this->SettingDefinition["Ranking"] = array();
		
		// For marksheet edit page
		$this->MS_MinimunColumn = 5;
		
		// Archive Report Card css path
		$this->ArchiveCSSPath = $PATH_WRT_ROOT."file/reportcard2008/".$this->schoolYear."/archive_css/";
		
		//Default Other Info In GrandMS
		$this->OtherInfoInGrandMS = array("Conduct");
		
		// Preload Info Array
		$this->PreloadInfoArr = array();
		
		// MS Estimate Score Symbol
		$this->MS_EstimateScoreSymbol = '#';
		
	}
	
	# function to load data in batch (for better performance)
	function Load_Batch_Grading_Scheme_Main_Info()
	{
		if(empty($this->Grading_Scheme_Main_Info)) {
			$table = $this->DBName.".RC_GRADING_SCHEME";
			$sql  = "SELECT SchemeID, SchemeTitle, SchemeType, FullMark, PassMark, TopPercentage, Pass, Fail ";
			$sql .= "FROM $table ";
			$sql .= "ORDER BY SchemeTitle";
			
			$this->Grading_Scheme_Main_Info = BuildMultiKeyAssoc((array)$this->returnArray($sql),"SchemeID");
		}
	}  
	
	function Load_Batch_Grading_Scheme_Range_Info() {
		if (empty($this->Grading_Scheme_Range_Info))	{
			 // Get grade mark Info
			$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
			
			$sql = "SELECT 
						GradingSchemeRangeID, 
						SchemeID, 
						Nature, 
						LowerLimit, 
						UpperLimit, 
						Grade, 
						GradePoint, 
						AdditionalCriteria,
						Description,
						CASE Nature
							WHEN 'D' THEN 1
							WHEN 'P' THEN 2
							WHEN 'F' THEN 3
						END AS DisplayOrder 
					FROM 
						$table 
					ORDER BY 
						DisplayOrder ASC , LowerLimit DESC, GradingSchemeRangeID ASC
					";
			$result = $this->returnArray($sql);
			$size = count($result);
			for($i=0; $i<$size; $i++) {
				$this->Grading_Scheme_Range_Info[$result[$i]["SchemeID"]][]= $result[$i];
			}
		}
	}
	
	function Load_Batch_Subject_Form_Grading_Scheme($ClassLevelID) {
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		if(empty($this->Subject_Form_Grading_Scheme[$ClassLevelID])) {
			$sql = "SELECT 
						SchemeID, DisplayOrder, ScaleInput, ScaleDisplay, SubjectID, LangDisplay, DisplaySubjectGroup, ClassLevelID, ReportID,
						If (SubjectID > 0, 1, 2) as SubjectType /* to make the Grand Subject Retrieve last which is the same as the UI */
					FROM 
						$table 
					WHERE
						ClassLevelID = $ClassLevelID
					ORDER BY SubjectType, DisplayOrder";
			$this->Subject_Form_Grading_Scheme[$ClassLevelID] = $this->returnArray($sql);
		}
	}
	
	function Load_ReportColumnID_ReportID_Mapping() {
		if(empty($this->ReportColumnID_ReportID_Mapping)) {
			$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
			$sql = "Select ReportColumnID, ReportID From $table ";
			$result = $this->returnArray($sql);
			$this->ReportColumnID_ReportID_Mapping =  build_assoc_array($result);
		}
	}
	
	function Load_Report_Template_Basic_Info($ReportID) {
		if(empty($this->Report_Template_Basic_Info[$ReportID]))	{
			$table = $this->DBName.".RC_REPORT_TEMPLATE";
			$sql = "SELECT 
						ReportID,
						ReportTitle,
						ClassLevelID,
						Semester,
						Description,
						PercentageOnColumnWeight,
						HeaderHeight,
						Footer,
						LineHeight,
						AllowClassTeacherComment,
						AllowSubjectTeacherComment,
						DisplaySettings,
						LastGenerated,
						MarksheetSubmissionEnd,
						MarksheetVerificationEnd,
						ShowSubjectFullMark,
						ShowSubjectOverall, 
						ShowOverallPositionClass,
						ShowOverallPositionForm,
						ShowGrandTotal,
						ShowGrandAvg,
						Issued,
						ShowNumOfStudentClass,
						ShowNumOfStudentForm,
						OverallPositionRangeClass,
						OverallPositionRangeForm,
						isMainReport,
						GreaterThanAverageClass,
						GreaterThanAverageForm,
						ShowSubjectComponent,
						TermStartDate,
						TermEndDate,
						LastGeneratedAcademicProgress,
						AttendanceDays,
						LastGeneratedAward,
						LastGeneratedBy
					FROM 
						$table 
					WHERE
						ReportID = '$ReportID'
					";
				$x = $this->returnArray($sql);	
				if (count($x) > 0 && $x[0]['Semester'] != 'F' && (is_date_empty($x[0]['TermStartDate']) || is_date_empty($x[0]['TermEndDate']))) {
					global $PATH_WRT_ROOT;
					include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
					
					$YearTermID = $x[0]['Semester'];
					$objYearTerm = new academic_year_term($YearTermID);
					
					$x[0]['TermStartDate'] = substr($objYearTerm->TermStart, 0, 10);
					$x[0]['TermEndDate'] = substr($objYearTerm->TermEnd, 0, 10);
				}
			$this->Report_Template_Basic_Info[$ReportID] = $x[0];
		}	
	}
	
	function Load_Batch_Student_Class_ClassLevel_Info() {
		if(empty($this->Student_Class_ClassLevel_Info))	{
			$sql = "SELECT
							yc.YearID as ClassLevelID,
							y.YearName as ClassLevelName,
							yc.YearClassID as ClassID,
							yc.ClassTitleEN as ClassName,
							yc.ClassTitleB5 as ClassNameCh,
							ycu.ClassNumber,
							ycu.UserID,
							iu.EnglishName,
							iu.ChineseName,
							iu.HKID,
							iu.DateOfBirth,
							iu.Gender,
							iu.UserLogin,
							iu.STRN,
							iu.WebSAMSRegNo
					FROM
							YEAR_CLASS_USER as ycu
							INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."')
							INNER JOIN YEAR as y On (yc.YearID = y.YearID)
							Inner Join INTRANET_USER as iu On (ycu.UserID = iu.UserID)
					";
			$resultSet = $this->returnArray($sql, null, $ReturnAssoOnly=1);
			$this->Student_Class_ClassLevel_Info = BuildMultiKeyAssoc($resultSet, "UserID");
		}
	}	
	
	function Load_Batch_Student_House_Info() {
		if(empty($this->PreloadInfoArr['Student_House_Info'])) {
			$sql = "Select
							iu.UserID as UserID,
							ig.GroupID,
							ig.Title as HouseTitleEn,
							ig.TitleChinese as HouseTitleCh
					From
							INTRANET_USERGROUP as iu
							Inner Join
							INTRANET_GROUP as ig On (iu.GroupID = ig.GroupID)
					Where
							ig.RecordType = '4'
							And ig.AcademicYearID = '".$this->schoolYearID."'
					";
			$resultSet = $this->returnArray($sql, $ReturnAssoOnly=1);
			$this->PreloadInfoArr['Student_House_Info'] = BuildMultiKeyAssoc($resultSet, "UserID");
		}
	}	
	
	function Preload_Setting() {
		if(empty($this->General_Setting)) {
			$table = $this->DBName.".RC_SETTINGS";
			$sql  = "SELECT SettingCategory, SettingKey, SettingValue FROM $table ";
			$row = $this->returnArray($sql);
			
			# default
			$setting['Absent&Exempt']['Absent'] = "ExWeight";
			$setting['Absent&Exempt']['Exempt'] = "ExWeight";
			$setting['Storage&Display']['SubjectScore'] = 0;
			$setting['Storage&Display']['SubjectTotal'] = 0;
			$setting['Storage&Display']['GrandAverage'] = 0;
			$setting['Storage&Display']['GrandTotal'] = 0;
			$setting['Calculation']['UseWeightedMark'] = 0;
			$setting['Calculation']['AdjustToFullMarkFirst'] = 0;
			$setting['Calculation']['OrderTerm'] = 1;
			$setting['Calculation']['OrderFullYear'] = 1;	
			
			if(count($row)>0) {
				foreach($row as $rec) {
					$setting[$rec['SettingCategory']][$rec['SettingKey']] = $rec['SettingValue'];
				}
			}
			$this->General_Setting = $setting;
		}
	}
	
	/**
	 * Get active year from /file/reportcard2008/db_year.txt
	 *
	 * @param string $format To create a formatted year string with it as the middle symbol
	 * @return string
	 */
	function GET_ACTIVE_YEAR($format="") {
		global $PATH_WRT_ROOT, $intranet_root;
		$activeYear = get_file_content($intranet_root."/file/reportcard2008/db_year.txt");
		if ($format != "" && get_client_region() != 'zh_MY') {
			$activeYear .= $format.($activeYear + 1);
		}
		return $activeYear;
	}
	
	function GET_ACTIVE_YEAR_ID() {
		global $intranet_root;
		$activeYear = get_file_content($intranet_root."/file/reportcard2008/db_year.txt");
		
		include_once("form_class_manage.php");
		$lib_AcademicYear = new academic_year();
		$AcademicYearInfoArr = $lib_AcademicYear->Get_Academic_Year_Info_By_YearName($activeYear);
		$activeYearID = $AcademicYearInfoArr[0]['AcademicYearID'];
		
		return $activeYearID;
	}
	
	function GET_ACTIVE_YEAR_NAME() {
		include_once('form_class_manage.php');
		$ObjAcademicYear = new academic_year($this->schoolYearID);
		return $ObjAcademicYear->Get_Academic_Year_Name();
	}
	
	/**
	 * Update active year to /file/reportcard2008/db_year.txt
	 *
	 * @param int $year New year to be updated
	 * @return boolean
	 */
	function UPDATE_ACTIVE_YEAR($year) {
		global $intranet_root;
		include_once($intranet_root."/includes/libfilesystem.php");
		$lo = new libfilesystem();
		$file = $intranet_root."/file/reportcard2008/db_year.txt";
		$success = $lo->file_write($year, $file);
		return $success;
	}
	
	/**
	 * IP20: Get current academic year from /file/academic_yr.txt which is set by eClass Admin Console
	 * IP25: Get from school settings > academic year
	 *
	 * @param string $format To create a formatted year string with it as the middle symbol
	 * @return string
	 */
	function GET_ACADEMIC_YEAR($format="") {
		global $intranet_root, $PATH_WRT_ROOT, $eRC_Addon_PATH_WRT_ROOT;
		
		//$academicYearLong = get_file_content($intranet_root."/file/academic_yr.txt");
		if (isset($eRC_Addon_PATH_WRT_ROOT) && $eRC_Addon_PATH_WRT_ROOT!='') {
			include_once($eRC_Addon_PATH_WRT_ROOT."includes/form_class_manage.php");
		}
		else {
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		}
		
		$obj_AcademicYear = new academic_year(Get_Current_Academic_Year_ID());
		$academicYearLong = $obj_AcademicYear->YearNameEN;
		
		if (!empty($academicYearLong)) {
			// Try to separate it if its format is like "2007-2008"
			$academicYearPiece = explode("-", $academicYearLong);
			if (is_numeric($academicYearPiece[0])) {
				// Handle year in 4 digits and 2 digits
				if (strlen($academicYearPiece[0]) == 4) {
					$academicYear = $academicYearPiece[0];
				} else if (strlen($academicYearPiece[0]) == 2) {
					$academicYear = substr(date("Y"), 0, 2).$academicYearPiece[0];
				}
			} else {
				// Cannot separate, just get the first 4 or 2 numbers and assume it is the year
				if (is_numeric(substr($academicYearLong, 0, 4))) {
					$academicYear = substr($academicYearLong, 0, 4);
				} else if (is_numeric(substr($academicYearLong, 0, 2))) {
					$academicYear = (substr(date("Y"), 0, 2)).(substr($academicYearLong, 0, 2));
				}
			}
		}
		if ($format != "" && get_client_region() != 'zh_MY') {
			$academicYear .= $format.($academicYear + 1);
		}
		return $academicYear;
	}
	
	function Get_Academic_Year_Long_Name($AcademicYear) {
		global $PATH_WRT_ROOT;
		
		$x = '';
		if (get_client_region() == 'zh_MY') {
			$x = $AcademicYear;
		}
		else {
			$x = $AcademicYear."-".($AcademicYear + 1);
		}
		return $x;
	}
	
	/**
	 * Get eReportCard database name
	 *
	 * @param int $year Suffix of the DB name
	 * @return string
	 */
	function GET_DATABASE_NAME($year) {
		global $intranet_db;
		$prefix = $intranet_db."_DB_REPORT_CARD_";
		return $prefix.$year;
	}
	
	/**
	 * Get the content in /file/reportcard2008/admin_user.txt
	 *
	 * @return string Comma-separated UserID of eReportCard admins
	 */
	function GET_ADMIN_USER() {
		$sql = "SELECT
						DISTINCT(rm.UserID) 
				FROM
						(SELECT RoleID FROM	ROLE_RIGHT WHERE FunctionName = 'eAdmin-eReportCard' AND RightFlag = 1) as a
						INNER JOIN ROLE_MEMBER as rm on a.RoleID = rm.RoleID
				";
		$adminArr = $this->returnVector($sql);
		
		if (count($adminArr) > 0)
			$adminList = implode(", ", $adminArr);
		else
			$adminList = '';

		return $adminList;
	}
	
	/**
	 * Check if the given UserID is an eReportCard admin
	 *
	 * @param int $ParUserID UserID to check if it is an admin
	 * @return boolean
	 */
	function IS_ADMIN_USER($ParUserID) {
		return $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"];
	}
	
	/**
	 * Check various conditions to see if user have access right to a page
	 *
	 * @use $PageRight Defined in the browsing page, specifying who can view it
	 * @use $intranet_reportcard_admin Whether user is an eReportCard admin
	 * @use $intranet_reportcard_usertype Type of user
	 * @return boolean
	 */
	function hasAccessRight() {
		global $PageRight, $ck_ReportCard_UserType, $PATH_WRT_ROOT, $plugin;
		
		if (!is_array($PageRight))
			$PageRightArr = array($PageRight);
		else
			$PageRightArr = $PageRight;
		
		if(in_array("ADMIN", $PageRightArr) && count($PageRightArr)==1) {
			# Admin Only
			$HaveRight = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"]==1) ? true : false;
		} else {
			# User Type which in the PageRightArr or Admin can access the page
			$HaveRight = (in_array($ck_ReportCard_UserType, $PageRightArr) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"]==1) ? true : false;
		}
		
		if (!$plugin['ReportCard2008'] || $HaveRight == false) {
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
		
		return $HaveRight;
	}
	
	/**
	 * Get the class name of the class in which user is the class teacher
	 *
	 * @return string
	 */
	function GET_TEACHING_CLASS() {
		$sql = "SELECT
					yc.YearClassID,
					yc.ClassTitleEN,
					yc.ClassTitleB5,
					yc.YearID
				FROM
					YEAR_CLASS_TEACHER as yct
					INNER JOIN
					YEAR_CLASS as yc
					ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' )
				WHERE
					yct.UserID='".$this->uid."'
				";
		return $this->returnArray($sql);
	}
	
	/**
	 * Update a Datetime field in RC_REPORT_TEMPLATE to current time
	 *
	 * @param int $ReportID
	 * @param string $DateName Name of the field to be updated
	 * @return boolean
	 */
	function UPDATE_REPORT_LAST_DATE($ReportID, $DateName, $isReset=0, $ModifiedByFieldName='') {
		// $DateName = LastArchived, LastPrinted,	LastAdjusted, LastGenerated, LastMarksheetInput, DateInput or DateModified
		$TargetDate = ($isReset==0)? 'NOW()' : 'NULL';
		
		if ($ModifiedByFieldName != '') {
			$SetModifiedBy = " , ".$ModifiedByFieldName." = '".$_SESSION['UserID']."' ";
		}
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "UPDATE $table SET $DateName = $TargetDate $SetModifiedBy WHERE ReportID = '$ReportID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * Get MODULE_OBJ array for generation of left menu
	 *
	 * @return array Array contains the required info for generating left menu
	 */
	function GET_MODULE_OBJ_ARR(){

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPage, $eReportCard, $CurrentPageArr;
		global $i_ReportCard_System, $ck_ReportCard_UserType, $top_menu_mode;
		global $plugin, $UserID, $ReportCardCustomSchoolName, $sys_custom, $eRCTemplateSetting;
		
		//include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateNo = $eRCTemplateSetting['CusTemplate'];
		
		if(isset($top_menu_mode) && $top_menu_mode != "") {
			if($top_menu_mode==0) {
				$ck_ReportCard_UserType = $ck_ReportCard_UserType=="ADMIN"?"TEACHER":$ck_ReportCard_UserType;
				$CurrentPageArr['eReportCard'] = 0;
				$CurrentPageArr['eServiceeReportCard'] = 1;
			} else {
				$CurrentPageArr['eReportCard'] = 1;
				$CurrentPageArr['eServiceeReportCard'] = 0;
			}
		} else {
			if($ck_ReportCard_UserType=="TEACHER" || $ck_ReportCard_UserType=="PARENT" || $ck_ReportCard_UserType=="STUDENT") {
				$CurrentPageArr['eReportCard'] = 0;
				$CurrentPageArr['eServiceeReportCard'] = 1;
			} else if($ck_ReportCard_UserType=="ADMIN") {
				$CurrentPageArr['eReportCard'] = 1;
				$CurrentPageArr['eServiceeReportCard'] = 0;
			}
		}

		if($ck_ReportCard_UserType=="ADMIN") {
			// Logo link
			$MODULE_OBJ['root_path'] = "/home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php";
			
			$PageManagement = 0;
			$PageManagement_Schedule = 0;
			$PageManagement_Progress = 0;
			$PageManagement_MarkSheetRevision = 0;
			$PageManagement_ClassTeacherComment = 0;
			$PageManagement_OtherInfo = 0;
			$PageManagement_DataHandling = 0;
			$PageManagement_ReportArchive = 0;
			$PageManagement_Promotion = 0;
			
			$PageReports = 0;
			$PageReports_GenerateReport = 0;
			$PageReports_GrandMarksheet = 0;
			
			$PageStatistics = 0;
			
			$PageSettings = 0;
			$PageSettings_BasicSettings = 0;
			$PageSettings_CommentBank = 0;
			$PageSettings_SubjectsAndForms = 0;
			$PageSettings_ReportCardTemplates = 0;
			$PageSettings_ExcludeStudents = 0;
			$PageSettings_PromotionCriteria = 0;
			
			switch ($CurrentPage) {
				case "Management_Schedule":
					$PageManagement = 1;
					$PageManagement_Schedule = 1;
					break;
				case "Management_Progress":
					$PageManagement = 1;
					$PageManagement_Progress = 1;
					break;
				case "Management_MarkSheetRevision":
					$PageManagement = 1;
					$PageManagement_MarkSheetRevision = 1;
					break;
				case "Management_ClassTeacherComment":
					$PageManagement = 1;
					$PageManagement_ClassTeacherComment = 1;
					break;
				case "Management_OtherInfo":
					$PageManagement = 1;
					$PageManagement_OtherInfo = 1;
					break;
				case "Management_OtherStudentInfo":	// for SIS use only
					$PageManagement = 1;
					$PageManagement_OtherStudentInfo = 1;
					break;
				case "Management_DataHandling":
					$PageManagement = 1;
					$PageManagement_DataHandling = 1;
					break;
				case "Management_PersonalCharacteristics":		// for st. stephen only
					$PageManagement = 1;
					$PageManagement_PersonalCharacteristics = 1;
					break;
				case "Management_ReExam":		// for escola_tong_nam only
					$PageManagement = 1;
					$PageManagement_ReExam = 1;
					break;
				case "Management_SubjectConductMark":		// for li_sing_tai_hang only
					$PageManagement = 1;
					$PageManagement_SubjectConductMark = 1;
					break;
				case "Management_ReportArchive":
					$PageManagement = 1;
					$PageManagement_ReportArchive = 1;
					break;
				case "Management_Promotion":
					$PageManagement = 1;
					$PageManagement_Promotion = 1;
					break;
				case "Management_AcademicProgress";
					$PageManagement = 1;
					$PageManagement_AcademicProgress = 1;
					break;
										
					
				case "Reports_GenerateReport":
					$PageReports = 1;
					$PageReports_GenerateReport = 1;
					break;
				case "Reports_SubMarksheetReport":
					$PageReports = 1;
					$PageReports_SubMarksheetReport = 1;
					break;
				case "Reports_AssessmentReport":
					$PageReports = 1;
					$PageReports_AssessmentReport = 1;
					break;
				case "Reports_GrandMarksheet":
					$PageReports = 1;
					$PageReports_GrandMarksheet = 1;
					break;
				case "Reports_ClassHonourReport":
					$PageReports = 1;
					$PageReports_ClassHonourReport = 1;
					break;
				case "Reports_SubjectPrizeReport":
					$PageReports = 1;
					$PageReports_SubjectPrizeReport = 1;
					break;
				case "Reports_ConductAwardReport":
					$PageReports = 1;
					$PageReports_ConductAwardReport = 1;
					break;
				case "Reports_ConductProgressReport":
					$PageReports = 1;
					$PageReports_ConductProgressReport = 1;
					break;
				case "Reports_GradeDistribution":
					$PageReports = 1;
					$PageReports_GradeDistribution = 1;
					break;
				case "Reports_GPATop3Rank":
					$PageReports = 1;
					$PageReports_GPATop3Rank = 1;
					break;
				case "Reports_ExaminationSummary":
					$PageReports = 1;
					$PageReports_ExaminationSummary = 1;
					break;
				case "Reports_StudentResultSummary":
					$PageReports = 1;
					$PageReports_StudentResultSummary = 1;
					break;
					
				case "Reports_SubjectSummarySheet":
					$PageReports = 1;
					$PageReports_SubjectSummarySheet = 1;
					break;
				case "Reports_MarkGradeConversion":
					$PageReports = 1;
					$PageReports_MarkGradeConversion = 1;
					break;
				case "Reports_MarkPositionConversion":
					$PageReports = 1;
					$PageReports_MarkPositionConversion = 1;
					break;
				case "Reports_RawMarkDistribution":
					$PageReports = 1;
					$PageReports_RawMarkDistribution = 1;
					break;
				case "Reports_RemarksReport":
					$PageReports = 1;
					$PageReports_RemarksReport = 1;
					break;
				case "Reports_SubjectPercentagePassReport":
					$PageReports = 1;
					$PageReports_SubjectPercentagePassReport = 1;
					break;
				case "Reports_MasterReport":
					$PageReports = 1;
					$PageReports_MasterReport = 1;
					break;
				case "Reports_TrialPromotion":
					$PageReports = 1;
					$PageReports_TrialPromotion = 1;
					break;
				case "Reports_GradeList":
					$PageReports = 1;
					$PageReports_GradeList = 1;
					break;
				case "Reports_HKUGAGradeList":
					$PageReports = 1;
					$PageReports_HKUGAGradeList = 1;
					break;
				case "Reports_AwardList":
					$PageReports = 1;
					$PageReports_AwardList = 1;
					break;
				case "Reports_SubjectAcademicResult":
					$PageReports = 1;
					$PageReports_SubjectAcademicResult = 1;
					break;
				
				
				case "Statistics":
					$PageStatistics = 1;
					break;
					
					
				case "Settings_BasicSettings":
					$PageSettings = 1;
					$PageSettings_BasicSettings = 1;
					break;
				case "Settings_CommentBank":
					$PageSettings = 1;
					$PageSettings_CommentBank = 1;
					break;
				case "Settings_SubjectsAndForms":
					$PageSettings = 1;
					$PageSettings_SubjectsAndForms = 1;
					break;
				case "Settings_ReportCardTemplates":
					$PageSettings = 1;
					$PageSettings_ReportCardTemplates = 1;
					break;
				case "Settings_ExcludeStudents":
					$PageSettings = 1;
					$PageSettings_ExcludeStudents = 1;
					break;
				case "Curriculum_Expectation":
					$PageSettings = 1;
					$PageSettings_Curriculum_Expectation = 1;
					break;
				case "PersonalCharacteristics":		// for st. stephen only
					$PageSettings = 1;
					$PageSettings_PersonalCharacteristics = 1;
					break;
				case "PromotionCriteria":		
					$PageSettings = 1;
					$PageSettings_PromotionCriteria = 1;
					break;
			}
			
			// Menu information
			$MenuArr["Management"] = array($eReportCard['Management'], "", $PageManagement);
			$MenuArr["Management"]["Child"]["Schedule"] = array($eReportCard['Management_Schedule'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/schedule/schedule.php", $PageManagement_Schedule);
			$MenuArr["Management"]["Child"]["Progress"] = array($eReportCard['Management_Progress'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/progress/submission.php?clearCoo=1", $PageManagement_Progress);
			
			if ($eRCTemplateSetting['HideMenu']['Management']['ClassTeacherComment'] == false) {
				$MenuArr["Management"]["Child"]["ClassTeacherComment"] = array($eReportCard['Management_ClassTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/class_teacher_comment/index.php", $PageManagement_ClassTeacherComment);
			}
			
			$MenuArr["Management"]["Child"]["MarksheetRevision"] = array($eReportCard['Management_MarksheetRevision'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/marksheet_revision/marksheet_revision.php", $PageManagement_MarkSheetRevision);
			
			if($sys_custom['eRC']['Settings']['PersonalCharacteristics'] && !$eRCTemplateSetting['HideMenu']['Management']['PersonalCharacteristics']) {
				$MenuArr["Management"]["Child"]["PersonalCharacteristics"] = array($eReportCard['PersonalCharacteristics'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/personal_characteristics/index.php", $PageManagement_PersonalCharacteristics);
			}
			
			$MenuArr["Management"]["Child"]["OtherInfo"] = array($eReportCard['Management_OtherInfo'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php", $PageManagement_OtherInfo);
			
			if($sys_custom['eRC']['Management']['AcademicProgress'])
				$MenuArr["Management"]["Child"]["AcademicProgress"] = array($eReportCard['Management_AcademicProgress'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/academic_progress/index.php", $PageManagement_AcademicProgress);
			
			if($sys_custom['eRC']['Management']['ReExam'])
				$MenuArr["Management"]["Child"]["ReExam"] = array($eReportCard['ReExam'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/re_exam/index.php", $PageManagement_ReExam);
			
			if($sys_custom['eRC']['Settings']['PromotionCriteria'])
				$MenuArr["Management"]["Child"]["Promotion"] = array($eReportCard['Management_Promotion'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/promotion/index.php", $PageManagement_Promotion);
			
			$MenuArr["Management"]["Child"]["ReportArchive"] = array($eReportCard['Management_ReportArchive'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/archive_report_card/index.php", $PageManagement_ReportArchive);
			$MenuArr["Management"]["Child"]["DataHandling"] = array($eReportCard['Management_DataHandling'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/data_handling/transition.php", $PageManagement_DataHandling);
			
				
			
			$MenuArr["Reports"] = array($eReportCard['Reports'], "", $PageReports);
			$MenuArr["Reports"]["Child"]["GenerateReport"] = array($eReportCard['Reports_GenerateReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/generate_reports/index.php?clearCoo=1", $PageReports_GenerateReport);
			
			if ($sys_custom['eRC']['Report']['SubMarksheetReport'])
				$MenuArr["Reports"]["Child"]["SubMarksheetReport"] = array($eReportCard['Reports_SubMarksheetReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/sub_marksheet_reports/index.php", $PageReports_SubMarksheetReport);
				
			if ($sys_custom['eRC']['Report']['AssessmentReport'])
				$MenuArr["Reports"]["Child"]["AssessmentReport"] = array($eReportCard['Reports_AssessmentReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/assessment_reports/index.php", $PageReports_AssessmentReport);
			
			$MenuArr["Reports"]["Child"]["GrandMarksheet"] = array($eReportCard['Reports_GrandMarksheet'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/grand_marksheet/index.php", $PageReports_GrandMarksheet);
						
			if ($sys_custom['eRC']['ExtraReport'] || $sys_custom['eRC']['Report']['HonourReport'])
				$MenuArr["Reports"]["Child"]["ClassHonourReport"] = array($eReportCard['Reports_ClassHonourReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/class_honour/index.php", $PageReports_ClassHonourReport);
				
			if ($sys_custom['eRC']['ExtraReport'] || $sys_custom['eRC']['Report']['SubjectPrizeReport'])
				$MenuArr["Reports"]["Child"]["SubjectPrizeReport"] = array($eReportCard['Reports_SubjectPrizeReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/subject_prize/index.php", $PageReports_SubjectPrizeReport);
				
			if ($sys_custom['eRC']['ExtraReport'] || $sys_custom['eRC']['Report']['RankingReport'])
				$MenuArr["Reports"]["Child"]["RankingReport"] = array($eReportCard['Reports_GPATop3Rank'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/gpa_top3/index.php", $PageReports_GPATop3Rank);
			
			if($this->ConductAry || $ReportCardCustomSchoolName == "ucc_ke")
			{
				if ($sys_custom['eRC']['ExtraReport'] || $MenuArr["Reports"]["Child"]["ConductAwardReport"])
					$MenuArr["Reports"]["Child"]["ConductAwardReport"] = array($eReportCard['Reports_ConductAwardReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/conduct_award/index.php", $PageReports_ConductAwardReport);
				
				if ($sys_custom['eRC']['ExtraReport'] || $MenuArr["Reports"]["Child"]["ConductProgressReport"])
					$MenuArr["Reports"]["Child"]["ConductProgressReport"] = array($eReportCard['Reports_ConductProgressReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/conduct_progress/index.php", $PageReports_ConductProgressReport);
			}
			
			if ($sys_custom['eRC']['Report']['ExaminationSummary'])
				$MenuArr["Reports"]["Child"]["ExaminationSummary"] = array($eReportCard['Reports_ExaminationSummary'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/examination_summary/index.php", $PageReports_ExaminationSummary);

			if ($sys_custom['eRC']['Report']['StudentResultSummary'])
				$MenuArr["Reports"]["Child"]["StudentResultSummary"] = array($eReportCard['Reports_StudentResultSummary'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/student_result_summary/index.php", $PageReports_StudentResultSummary);
			
			if ($sys_custom['eRC']['Report']['SubjectSummarySheet'])
				$MenuArr["Reports"]["Child"]["SubjectSummarySheet"] = array($eReportCard['Reports_SubjectSummarySheet'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/subject_summary_sheet/index.php", $PageReports_SubjectSummarySheet);
			
			if ($sys_custom['eRC']['Report']['GradeDistribution'])
				$MenuArr["Reports"]["Child"]["GradeDistribution"] = array($eReportCard['Reports_GradeDistribution'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/grade_distribution/index.php", $PageReports_GradeDistribution);
			
			if ($sys_custom['eRC']['Report']['MarkGradeConversion'])
				$MenuArr["Reports"]["Child"]["MarkGradeConversion"] = array($eReportCard['Reports_MarkGradeConversion'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/mark_grade_conversion/index.php", $PageReports_MarkGradeConversion);
			
			if ($sys_custom['eRC']['Report']['MarkPositionConversion'])
				$MenuArr["Reports"]["Child"]["MarkPositionConversion"] = array($eReportCard['Reports_MarkPositionConversion'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/mark_position_conversion/index.php", $PageReports_MarkPositionConversion);
			
			if ($sys_custom['eRC']['Report']['RawMarkDistribution'])
				$MenuArr["Reports"]["Child"]["RawMarkDistribution"] = array($eReportCard['Reports_RawMarkDistribution'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/raw_mark_distribution/index.php", $PageReports_RawMarkDistribution);
				
			if ($sys_custom['eRC']['Report']['RemarksReport'])
				$MenuArr["Reports"]["Child"]["RemarksReport"] = array($eReportCard['Reports_RemarksReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/remarks_report/index.php", $PageReports_RemarksReport);
			
			if ($sys_custom['eRC']['Report']['SubjectPercentagePassReport'])
				$MenuArr["Reports"]["Child"]["SubjectPercentagePassReport"] = array($eReportCard['Reports_SubjectPercentagePassReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/subject_percentage_pass/index.php", $PageReports_SubjectPercentagePassReport);
			
			$MenuArr["Reports"]["Child"]["MasterReport"] = array($eReportCard['Reports_MasterReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/master_report/index.php", $PageReports_MasterReport);
			
			if ($sys_custom['eRC']['Report']['TrialPromotion'])
				$MenuArr["Reports"]["Child"]["TrialPromotion"] = array($eReportCard['Reports_TrialPromotion'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/trial_promotion/index.php", $PageReports_TrialPromotion);
	
			//if (false)	// for prototyping only
			//	$MenuArr["Reports"]["Child"]["GradeList"] = array("Grade List", $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/grade_list/index.php", $PageReports_GradeList);
			
			if ($sys_custom['eRC']['Report']['HKUGAGradeList'])
				$MenuArr["Reports"]["Child"]["HKUGAGradeList"] = array($eReportCard['Reports_HKUGAGradeList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/hkuga_grade_list/index.php", $PageReports_HKUGAGradeList);
				
			if ($eRCTemplateSetting['Report']['AwardList'])
				$MenuArr["Reports"]["Child"]["AwardList"] = array($eReportCard['Reports_AwardList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/award_list/index.php", $PageReports_AwardList);
			
			if ($eRCTemplateSetting['Report']['SubjectAcademicResult'])
				$MenuArr["Reports"]["Child"]["SubjectAcademicResult"] = array($eReportCard['Reports_SubjectAcademicResult'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/subject_academic_result/index.php", $PageReports_SubjectAcademicResult);
			
			
			$MenuArr["Statistics"] = array($eReportCard['Statistics'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/statistics/index.php", $PageStatistics);
			
			
			$MenuArr["Settings"] = array($eReportCard['Settings'], "", $PageSettings);
			$MenuArr["Settings"]["Child"]["BasicSettings"] = array($eReportCard['Settings_BasicSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", $PageSettings_BasicSettings);
			
			if ($eRCTemplateSetting['HideMenu']['Settings']['CommentBank'] == false)
				$MenuArr["Settings"]["Child"]["CommentBank"] = array($eReportCard['Settings_CommentBank'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php", $PageSettings_CommentBank);
			
			$MenuArr["Settings"]["Child"]["SubjectsAndForms"] = array($eReportCard['Settings_SubjectsAndForm'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php", $PageSettings_SubjectsAndForms);
			$MenuArr["Settings"]["Child"]["ReportCardTemplates"] = array($eReportCard['Settings_ReportCardTemplates'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/reportcard_templates/index.php", $PageSettings_ReportCardTemplates);
			$MenuArr["Settings"]["Child"]["ExcludeStudents"] = array($eReportCard['Settings_ExcludeStudents'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/exclude_students/index.php", $PageSettings_ExcludeStudents);
			
			# Curriculum Expectation
			if($sys_custom['eRC']['Settings']['CurriculumExpectation'])
				$MenuArr["Settings"]["Child"]["CurriculumExpectation"] = array($eReportCard['Settings_CurriculumExpectation'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/curriculum_expectation/index.php", $PageSettings_Curriculum_Expectation);
			
			# Personal Characteristics
			if($sys_custom['eRC']['Settings']['PersonalCharacteristics'])
				$MenuArr["Settings"]["Child"]["PersonalCharacteristics"] = array($eReportCard['PersonalCharacteristics'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/personal_characteristics/index.php", $PageSettings_PersonalCharacteristics);
			
			# Promotion Criteria (Munsang College)
			if($sys_custom['eRC']['Settings']['PromotionCriteria'])
				$MenuArr["Settings"]["Child"]["PromotionCriteria"] = array($eReportCard['Settings_PromotionCriteria'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/promotion/index.php", $PageSettings_PromotionCriteria);
			
		} else if($ck_ReportCard_UserType=="TEACHER") {
			# Logo link
			$MODULE_OBJ['root_path'] = "/home/eAdmin/StudentMgmt/eReportCard/management/marksheet_revision/marksheet_revision.php";
		
			$SettingCategory	= "AccessSettings";
			$AccessSettings = $this->LOAD_SETTING($SettingCategory);
			
			$PageManagement = 0;
			$PageManagement_MarkSheetRevision = 0;
			$PageManagement_ClassTeacherComment = 0;
			$PageManagement_OtherInfo = 0;
		
			switch ($CurrentPage) 
			{
				case "Management_MarkSheetRevision":
					$PageManagement = 1;
					$PageManagement_MarkSheetRevision = 1;
					break;
				case "Management_ClassTeacherComment":
					$PageManagement = 1;
					$PageManagement_ClassTeacherComment = 1;
					break;
				case "Management_OtherInfo":
					$PageManagement = 1;
					$PageManagement_OtherInfo = 1;
					break;
				case "Reports_GrandMarksheet":
					$PageReports = 1;
					$PageReports_GrandMarksheet = 1;
					break;
				case "Reports_HKUGAGradeList":
					$PageReports = 1;
					$PageReports_HKUGAGradeList = 1;
					break;
				case "Management_PersonalCharacteristics":		// for st. stephen only
					$PageManagement = 1;
					$PageManagement_PersonalCharacteristics = 1;
					break;
				case "Management_ReExam":		// for escola_tong_nam only
					$PageManagement = 1;
					$PageManagement_ReExam = 1;
					break;
				case "Reports_RemarksReport": 	// for hkuga_college
					$PageReports = 1;
					$PageReports_RemarksReport = 1;
					break;
			}
		
			$MenuArr["Management"] = array($eReportCard['Management'], "", $PageManagement);
			$MenuArr["Management"]["Child"]["MarksheetSubmission"] = array($eReportCard['Management_MarksheetSubmission'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/marksheet_revision/marksheet_revision.php", $PageManagement_MarkSheetRevision);
			
			//$ClassTeacherClassInfoArr = $this->returnClassTeacherClassInfo($_SESSION['UserID']);
			//if ($ClassTeacherClassInfoArr || $this->returnSubjectTeacherClassInfo($UserID))
			if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"] || $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_teacher"])
			{
				# class teacher only
				//if ($ClassTeacherClassInfoArr && $eRCTemplateSetting['HideMenu']['Management']['ClassTeacherComment'] == false)
				if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"] && $eRCTemplateSetting['HideMenu']['Management']['ClassTeacherComment'] == false)
					$MenuArr["Management"]["Child"]["ClassTeacherComment"] = array($eReportCard['Management_ClassTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/class_teacher_comment/index.php", $PageManagement_ClassTeacherComment);
				
				# both class teacher and subject teacher
				if ($sys_custom['eRC']['Settings']['PersonalCharacteristics'] && !$eRCTemplateSetting['HideMenu']['Management']['PersonalCharacteristics'])
					$MenuArr["Management"]["Child"]["PersonalCharacteristics"] = array($eReportCard['PersonalCharacteristics'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/personal_characteristics/index.php", $PageManagement_PersonalCharacteristics);
				
				# class teacher only
				//if ($ClassTeacherClassInfoArr) {
				if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"]) {
					if ($ReportCardCustomSchoolName == "escola_tong_nam") {
						$MenuArr["Management"]["Child"]["ReExam"] = array($eReportCard['ReExam'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/re_exam/index.php", $PageManagement_ReExam);
					}
					
					if($AccessSettings['AllowClassTeacherUploadCSV'])
						$MenuArr["Management"]["Child"]["OtherInfo"] = array($eReportCard['Management_OtherInfo'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php", $PageManagement_OtherInfo);
				}
			}
		
			if ($AccessSettings['AllowTeacherAccessGrandMS'] || $sys_custom['eRC']['Report']['HKUGAGradeList']) {
				$MenuArr["Reports"] = array($eReportCard['Reports'], "", $PageReports);
			}
			
			if($AccessSettings['AllowTeacherAccessGrandMS']) {
				$MenuArr["Reports"]["Child"]["GrandMarksheet"] = array($eReportCard['Reports_GrandMarksheet'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/grand_marksheet/index.php", $PageReports_GrandMarksheet);
			}
			
			if ($sys_custom['eRC']['Report']['HKUGAGradeList'])
				$MenuArr["Reports"]["Child"]["HKUGAGradeList"] = array($eReportCard['Reports_HKUGAGradeList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/hkuga_grade_list/index.php", $PageReports_HKUGAGradeList);
			
			# class teacher only
			//if ($ClassTeacherClassInfoArr)
			if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"]) {
				if ($sys_custom['eRC']['Report']['RemarksReport'])
					$MenuArr["Reports"]["Child"]["RemarksReport"] = array($eReportCard['Reports_RemarksReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/remarks_report/index.php", $PageReports_RemarksReport);
			}
						
		} else if($ck_ReportCard_UserType=="STUDENT" || $ck_ReportCard_UserType=="PARENT") {
			// Logo link
			$MODULE_OBJ['root_path'] = "home/eAdmin/StudentMgmt/eReportCard/management/marksheet_feedback/index.php";
		
			// Current Page Information init
			$PageManagement = 0;
			$PageVerifyAssessmentResult = 0;

			switch ($CurrentPage) {
				case "Management_VerifyAssessmentResult":
					$PageManagement = 1;
					$PageVerifyAssessmentResult = 1;
					break;
			}

			// Menu information
			$MenuArr["Management"] = array($eReportCard['Management'], "", $PageManagement);
			$MenuArr["Management"]["Child"]["MarksheetVerification"] = array($eReportCard['Management_MarksheetVerification'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/marksheet_feedback/index.php", $PageVerifyAssessmentResult);
		}

		// module information
		$MODULE_OBJ['title'] = $i_ReportCard_System;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reportcardsystem.gif";
		$MODULE_OBJ['menu'] = $MenuArr;
		
		return $MODULE_OBJ;
	}
	
	##################################################################
	######################## Settings Functions Start ###########################
	
	########## Basic Settings ###########
	/**
	 * Enclose the text with a styled <span> HTML tag according to setting
	 *
	 * @param string $text Text to styled
	 * @param string $SettingCategory
	 * @param string $style Specify which style to add (optional)
	 * @return string HTML to display
	 */
	function ReturnTextwithStyle($text, $SettingCategory='Highlight', $style='') {
		if ($text == '') {
			return ($this->EmptySymbol == '')? "&nbsp" : $this->EmptySymbol;
		}
		else {
			$setting = $this->LOAD_SETTING($SettingCategory, $style);
			//Format of Setting:  Color|bold|underline|italic|LeftSymbol|RightSymbol
			list($c, $b, $u, $i, $l, $r) = explode("|",$setting[$style]);
			return "<span style='color:$c; font-weight:$b; text-decoration:$u; font-style:$i'>".$l.$text.$r."</span>";
		}
	}
	
	/**
	 * Declare default style selection - Colors
	 *
	 * @return Array array of color definition in Hex format
	 */
	function returnHighlightColorSelection()
	{
		$color_ary = array(
			'FF0000','FF9900','FFDF00','FFFF00',
			'009900','99CC00','66CCCC','CCFF00',
			'6699CC','0066CC','33CCFF','9933CC',
			'996600','999999','FF99CC','000000'
		);	
		return $color_ary;
	}
	
	
	/**
	 * Declare style selection - enclosing symbol(left)
	 *
	 * @return Array array of symbols
	 */
	function returnHighlightSymbolSelection1()
	{
		$sym1_ary = array(
			' ','(','[',
			'{','*','#',
			'^'
		);	
		return $sym1_ary;
	}
	
	/**
	 * Declare style selection - enclosing symbol(right)
	 *
	 * @return Array array of symbols
	 */
	function returnHighlightSymbolSelection2()
	{
		$sym2_ary = array(
			' ',')',']',
			'}','*','#',
			'^'
		);	
		return $sym2_ary;
	}
	
	/**
	 * Load the setting in a category to an array
	 *
	 * @param string $category Category of setting to load, e.g. "Hightlight", "Calculation"
	 * @param string $key Name of the actual setting in a category, e.g. "Fail", "Pass" in "Hightlight"
	 * @return array Associated array with key|value pair containing all the settings of the given category
	 */

	 
	function LOAD_SETTING($category, $key='', $returnDBValue=0) {
		if (!array_key_exists($category, $this->SettingDefinition))
			return false;
		
		if($returnDBValue)
		{
			$table = $this->DBName.".RC_SETTINGS";
			$sql  = "SELECT SettingKey, SettingValue ";
			$sql .= "FROM $table ";
			$sql .= "WHERE SettingCategory = '$category'";
			if($key)	$sql .= " and SettingKey = '$key'";
			$row = $this->returnArray($sql);
		
			# load settings
			$setting = array();
			for ($i=0; $i<sizeof($row); $i++) {
				$setting[$row[$i]["SettingKey"]] = $row[$i]["SettingValue"];
			}
		}
		else
		{	
			$this->Preload_Setting();
			$raw_setting = $this->General_Setting[$category];
			
			if($key)
				$setting[$key] = $raw_setting[$key];
			else
				$setting = $raw_setting;
		}
		
			
//		$table = $this->DBName.".RC_SETTINGS";
//		$sql  = "SELECT SettingKey, SettingValue ";
//		$sql .= "FROM $table ";
//		$sql .= "WHERE SettingCategory = '$category'";
//		if($key)	$sql .= " and SettingKey = '$key'";
//		$row = $this->returnArray($sql, 2);
//		
//		if ($returnDBValue || $row != NULL)
//		{
//			# load settings
//			$setting = array();
//			for ($i=0; $i<sizeof($row); $i++) {
//				$setting[$row[$i]["SettingKey"]] = $row[$i]["SettingValue"];
//			}
//		}
//		else
//		{
//			# return default settings
//			if ($category == "Absent&Exempt")
//			{
//				$setting['Absent'] = "ExWeight";
//				$setting['Exempt'] = "ExWeight";
//			}
//			else if ($category == "Storage&Display")
//			{
//				$setting['SubjectScore'] = 0;
//				$setting['SubjectTotal'] = 0;
//				$setting['GrandAverage'] = 0;
//				$setting['GrandTotal'] = 0;
//			}
//			else if ($category == "Calculation")
//			{
//				$setting['UseWeightedMark'] = 0;
//				$setting['AdjustToFullMarkFirst'] = 0;
//				$setting['OrderTerm'] = 1;
//				$setting['OrderFullYear'] = 1;
//			}
//		}
		return $setting;
	}
	
	/**
	 * Update 1 or more setting(s) in a category
	 *
	 * @param string $category Category of setting to update, e.g. "Hightlight", "Calculation"
	 * @param array $setting Associated array of new setting key/value pair
	 * @return boolean
	 */
	function UPDATE_SETTING($category, $setting) {
		// Check if it is defined in the constructor
		if (!array_key_exists($category, $this->SettingDefinition))
			return false;
		$result = false;
		$table = $this->DBName.".RC_SETTINGS";
		$sqlUpdate  = "UPDATE $table SET";
		foreach ($setting as $key => $value) {
			if (in_array($key, $this->SettingDefinition[$category])) {
				$column = "SettingValue = '$value', DateModified = NOW()";
				$cond = "WHERE SettingKey = '$key' AND SettingCategory = '$category'";
				$sql = "$sqlUpdate $column $cond";
				$result = $this->db_db_query($sql);
			}
		}
		
		return $result;
	}
	
	/**
	 * Insert 1 or more setting(s) in a category
	 *
	 * @param string $category Category of setting to insert, e.g. "Hightlight", "Calculation"
	 * @param array $setting Associated array of new setting key/value pair
	 * @return boolean
	 */
	function INSERT_SETTING($category, $setting) {
		// Check if it is defined in the constructor
		if (sizeof($setting) == 0 || !array_key_exists($category, $this->SettingDefinition))
			return false;
		$table = $this->DBName.".RC_SETTINGS";
		$insertSQL = "INSERT INTO $table VALUES ";
		
		foreach ($setting as $key => $value) {
			if (in_array($key, $this->SettingDefinition[$category])) {
				$insertItem[] = "(NULL, '$category', '$key', '$value', NOW(), NULL)";
			}
		}
		
		$result = false;
		if (isset($insertItem) && sizeof($insertItem) > 0) {
			$insertSQL .= implode(",", $insertItem);
			$result = $this->db_db_query($insertSQL);
		}
		
		return $result;
	}
	
	########## Comment Bank ###########
	
	/**
	 * Get all the comment categories
	 *
	 * @param int $SubjectID Only retreived comment categories of a subject
	 * @return array Array of comment categories
	 */
	function GET_COMMENT_CAT($SubjectID = "") {
		$table = $this->DBName.".RC_COMMENT_BANK";
		$sql = "SELECT DISTINCT CommentCategory FROM $table WHERE CommentCategory IS NOT NULL AND CommentCategory != ''";
		if ($SubjectID != "") {
			$sql .= "AND SubjectID = '$SubjectID' ";
		}
		$sql .= "ORDER BY CommentCategory ASC";
		$result = $this->returnVector($sql);
		return $result;
	}
	
	/**
	 * Delete a comment category (assign NULL to the category field in all matching records)
	 *
	 * @param string $Comment Comment category
	 * @return boolean
	 */
	function DELETE_COMMENT_CAT($Comment) {
		$table = $this->DBName.".RC_COMMENT_BANK";
		$sql = "UPDATE $table SET CommentCategory = NULL WHERE CommentCategory = '$Comment'";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	########### Subjects & Form (Grading Scheme) ###########
	
	/**
	 * Get the main info of a grading scheme or all grading schemes
	 *
	 * @param int $SchemeID Provide this to get info of one scheme only
	 * @return array Detail of the grading scheme(s)
	 */
	function GET_GRADING_SCHEME_MAIN_INFO($SchemeID = "ALL") {
		# Get the main info of the grading scheme
		$this->Load_Batch_Grading_Scheme_Main_Info();
		
		if($SchemeID != "ALL")
			$result =  $this->Grading_Scheme_Main_Info[$SchemeID];
		else
			$result =  array_values($this->Grading_Scheme_Main_Info);
			
		return $result;
//		$table = $this->DBName.".RC_GRADING_SCHEME";
//		$sql  = "SELECT SchemeID, SchemeTitle, SchemeType, FullMark, PassMark, TopPercentage, Pass, Fail ";
//		$sql .= "FROM $table ";
//		$sql .= ($SchemeID != "ALL") ? "WHERE SchemeID = $SchemeID " : "";
//		$sql .= "ORDER BY SchemeTitle";
//		
//		$result = $this->returnArray($sql, 8);
//		if($SchemeID != "ALL")
//			$mainInfo = $result[0];
//		else
//			$mainInfo = $result;
//		return $mainInfo;
	}
	
	/**
	 * Get ranges of a grading scheme
	 *
	 * @param int $SchemeID
	 * @param string $Nature Provide this to get ranges of a type only ("D","P","F")
	 * @return array Info of ranges
	 * modified by Marcus 20091208, OrderBy DisplayOrder,GradingSchemeRangeID instead of GradingSchemeRangeID
	 */
	function GET_GRADING_SCHEME_RANGE_INFO($SchemeID, $Nature="ALL") {
		$returnArr = array();
		$gradeRangeInfo = array();
		
		$this->Load_Batch_Grading_Scheme_Range_Info();
		$gradeRangeInfo = $this->Grading_Scheme_Range_Info[$SchemeID];
		// Get grade mark Info
//		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
//		
//		$sql  = "
//			SELECT 
//				GradingSchemeRangeID, 
//				SchemeID, 
//				Nature, 
//				LowerLimit, 
//				UpperLimit, 
//				Grade, 
//				GradePoint, 
//				AdditionalCriteria,
//				CASE Nature
//					WHEN 'D' THEN 1
//					WHEN 'P' THEN 2
//					WHEN 'F' THEN 3
//				END AS DisplayOrder 
//			FROM 
//				$table 
//			WHERE 
//				SchemeID = $SchemeID 
//			ORDER BY 
//				DisplayOrder ASC , GradingSchemeRangeID ASC";
//		
//		/*$sql  = "SELECT GradingSchemeRangeID, SchemeID, Nature, LowerLimit, UpperLimit, Grade, GradePoint,AdditionalCriteria ";
//		$sql .= "FROM $table ";
//		$sql .= "WHERE SchemeID = $SchemeID ";
//		$sql .= "ORDER BY GradingSchemeRangeID";*/
//		$gradeRangeInfo = $this->returnArray($sql, 7);
		
		// Get grade mark Info by Nature
		if($Nature != "ALL"){
			if(count($gradeRangeInfo) > 0){
				for($i=0 ; $i<count($gradeRangeInfo) ; $i++){
					if($gradeRangeInfo[$i]['Nature'] == $Nature)
						$returnArr[] = $gradeRangeInfo[$i];
				}
			}
		}
		else {
			$returnArr = $gradeRangeInfo;
		}
		return $returnArr;
	}
	
	
	/**
	 * Get info of a grading scheme as well as info of its ranges
	 *
	 * @param int $SchemeID
	 * @return array Info of grading scheme and its ranges
	 */
	function GET_GRADING_SCHEME_INFO($SchemeID) {
		$mainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
		$gradeRangeArr = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
		return array($mainInfo, $gradeRangeArr);
	}
	
	/**
	 * Insert a new grading scheme (and its ranges if its SchemeType="H" (honor-based))
	 *
	 * @param array $SchemeInfo Definition of the new scheme
	 * @param array $RangeInfo Definition of the ranges of the new scheme
	 * @return boolean
	 */
	function INSERT_GRADING_SCHEME_AND_RANGE($SchemeInfo, $RangeInfo=array()) {
		$table = $this->DBName.".RC_GRADING_SCHEME";
		$sqlScheme = "INSERT INTO $table VALUES (NULL, 
				'".$SchemeInfo["SchemeTitle"]."',
				'".$SchemeInfo["SchemeType"]."',
				'".$SchemeInfo["FullMark"]."',
				'".$SchemeInfo["PassMark"]."',
				'".$SchemeInfo["TopPercentage"]."',
				'".$SchemeInfo["Pass"]."',
				'".$SchemeInfo["Fail"]."',
				NOW(), NOW() )";
		$result = $this->db_db_query($sqlScheme);

		$insertedSchemeID = mysql_insert_id();
		if ($result) {
			// only need to insert Ranges if its type is honor-based 
			if ($SchemeInfo["SchemeType"] == "H" && sizeof($RangeInfo) > 0) {
				// update the last inserted SchemeID into all elements of the Range Info array
				for($i=0; $i<sizeof($RangeInfo); $i++) {
					$RangeInfo[$i]["SchemeID"] = $insertedSchemeID;
				}
				$result = $this->INSERT_GRADING_SCHEME_RANGE($RangeInfo);
			}
			return $result;
		} else {
			return false;
		}
	}
	
	/**
	 * Delete a grading scheme (and its ranges if its SchemeType="H" (honor-based))
	 *
	 * @param int $SchemeID
	 * @return boolean
	 */
	function DELETE_GRADING_SCHEME_AND_RANGE($SchemeID){
		$table = $this->DBName.".RC_GRADING_SCHEME";
		$sql = "DELETE FROM $table WHERE SchemeID = $SchemeID";
		$success[] = $this->db_db_query($sql);
		$success[] = $this->DELETE_GRADING_SCHEME_RANGE($SchemeID);
		
		// update the subject form info if SchemeID no longer exists
		if(!in_array(false, $success)){
			$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
			$sql = "UPDATE $table SET SchemeID = '' WHERE SchemeID = $SchemeID";
			$success[] = $this->db_db_query($sql);
		}
		
		return in_array(false, $success);
	}
	
	/**
	 * Update a grading scheme (and its ranges if its SchemeType="H" (honor-based))
	 * Need to check if some GradingSchemeRangeID is empty() to determine INSERT/UPDATE/DELETE
	 *
	 * @param array $SchemeInfo Definition of the scheme
	 * @param array $RangeInfo Definition of the ranges of the scheme
	 * @return boolean
	 */
	function UPDATE_GRADING_SCHEME_AND_RANGE($SchemeInfo, $RangeInfo) {
		$table = $this->DBName.".RC_GRADING_SCHEME";
		// update scheme info first
		$sqlScheme = "UPDATE $table SET 
						SchemeTitle = '".$SchemeInfo["SchemeTitle"]."',
						SchemeType = '".$SchemeInfo["SchemeType"]."',
						FullMark = ".$SchemeInfo["FullMark"].",
						PassMark = ".$SchemeInfo["PassMark"].",
						TopPercentage = ".$SchemeInfo["TopPercentage"].",
						Pass = '".$SchemeInfo["Pass"]."',
						Fail = '".$SchemeInfo["Fail"]."', 
						DateModified = NOW() 
					WHERE SchemeID = ".$SchemeInfo["SchemeID"];
		$success = $this->db_db_query($sqlScheme);
		if (!success) return false;
		
		// delete all ranges of the scheme if SchemeType is set to "PF" (Pass/Fail)
		if ($SchemeInfo["SchemeType"] == "PF") {
			$success = $this->DELETE_GRADING_SCHEME_RANGE($SchemeInfo["SchemeID"]);
			return $success;
			// update END Here for this case
		}
		
		// get the ID of existing ranges for counter checking
		$existRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeInfo["SchemeID"]);
		if (sizeof($existRangeInfo) > 0) {
			for($i=0; $i<sizeof($existRangeInfo); $i++) {
				$existRangeID[] = $existRangeInfo[$i]["GradingSchemeRangeID"];
			}
		} else {
			$existRangeID = array();
		}
		
		$success = array();
		
		// check the GradingSchemeRangeID to divide the RangeInfo array for DB operation 
		if (sizeof($RangeInfo) > 0) {
			for($i=0; $i<sizeof($RangeInfo); $i++) {
				$GradingSchemeRangeID = $RangeInfo[$i]["GradingSchemeRangeID"];
				// No GradingSchemeRangeID is provided, new range => INSERT
				if (empty($GradingSchemeRangeID)) {
					$newRangeInfo[] = $RangeInfo[$i];
				} //  GradingSchemeRangeID is exist in DB, edit range => UPDATE
				else if (in_array($GradingSchemeRangeID, $existRangeID)) {
					// remove the matched GradingSchemeRangeID from existRangeID,
					// it can be used to detemine which ranges need to be DELETE
					$keyMatch = array_search($GradingSchemeRangeID, $existRangeID);
					unset($existRangeID[$keyMatch]);
					// UPDATE matched range
					$success[] = $this->UPDATE_GRADING_SCHEME_RANGE($GradingSchemeRangeID, $RangeInfo[$i]);
				}
			}
			
			// INSERT new ranges
			$success[] = $this->INSERT_GRADING_SCHEME_RANGE($newRangeInfo);
		}
		
		// DELETE removed ranges
		if (sizeof($existRangeID) > 0) {
			foreach($existRangeID as $value) {
				$success[] = $this->DELETE_GRADING_SCHEME_RANGE("", $existRangeID[$i]);
			}
		}
		
		return in_array(false, $success);
	}
	
	/**
	 * Get all semesters
	 *
	 * @param int $SemID Provide this will make this function return a semester name only
	 * @param boolean $haveReportTemplate Passing "true" will only return semester with reportcard only
	 * @return boolean
	 */
	function GET_ALL_SEMESTERS($SemID='-1', $haveReportTemplate=false) {
		// semester definition retrieve by the function getSemesters() in lib.php
		// getSemesters()  read the text file: /file/semester.txt
		$sem_ary = getSemesters($this->schoolYearID);
		
		if ($SemID == '-1')
		{
			// only return those semesters which have one or more report templates created
			if ($haveReportTemplate) {
				$result = array();
				$table = $this->DBName.".RC_REPORT_TEMPLATE";
				$sql = "SELECT DISTINCT Semester 
						FROM $table 
						WHERE Semester IS NOT NULL AND Semester != 'F'";
				$result = $this->returnVector($sql);
				if (sizeof($result)>0) {
					foreach($sem_ary as $key => $val) {
						if (!in_array($key, $result))
							unset($sem_ary[$key]);
					}
				}
			}
			return $sem_ary;
		}
		else
		{
			$sem_name = $sem_ary[$SemID];
			return $sem_name;
		}
	}
	
	/**
	 * Get all class level
	 *
	 * @param boolean $haveReportTemplate Passing "true" will only return class level with reportcard only
	 * @return array Array contain ClassLevelID and Name of all class levels
	 */
	 # 27 Jun 2009 - Changed the order from ClassLevelID to LevelName
	function GET_ALL_FORMS($haveReportTemplate=false) {
		if ($haveReportTemplate) {
			$table = $this->DBName.".RC_REPORT_TEMPLATE";
			$sql  = "SELECT DISTINCT 
						a.YearID as ClassLevelID, a.YearName as LevelName
					FROM 
						YEAR AS a, $table AS b
					WHERE 
						a.YearID = b.ClassLevelID 
					ORDER BY 
						a.Sequence";
		} else {
			$sql = 'SELECT 
							YearID as ClassLevelID, 
							YearName as LevelName
					FROM 
							YEAR 
					ORDER BY 
							Sequence
					';
		}
		
		$row = $this->returnArray($sql, 2);
		
		return $row;
	}
	
	/**
	 * Get an array mapping SubjectID to CODEID of all subjects
	 *
	 * @return array The mapping array
	 */
	function GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=0) {
		$cmp_code_conds = '';
		if ($withComponent == 0)
			$cmp_code_conds = " AND (CMP_CODEID IS NULL Or CMP_CODEID = '') ";
			
		$sql = "SELECT DISTINCT
						RecordID,
						CODEID,
						CMP_CODEID
					FROM
						ASSESSMENT_SUBJECT 
					WHERE
						EN_DES IS NOT NULL
						AND 
						RecordStatus = 1
						$cmp_code_conds
					ORDER BY
						DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 2);
		
		$ReturnArr = array();
		
		if (sizeof($SubjectArr) > 0) {
			if ($withComponent == 0)
			{
				if ($mapByCode)
				{
					for($i=0; $i<sizeof($SubjectArr); $i++) {
						$ReturnArr[$SubjectArr[$i]["CODEID"]] = $SubjectArr[$i]["RecordID"];
					}
				}
				else
				{
					for($i=0; $i<sizeof($SubjectArr); $i++) {
						$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
					}
				}
			}
			else
			{
				if ($mapByCode)
				{
					for($i=0; $i<sizeof($SubjectArr); $i++) {
						$thisKey = $SubjectArr[$i]["CODEID"].'_'.$SubjectArr[$i]["CMP_CODEID"];
						$ReturnArr[$thisKey] = $SubjectArr[$i]["RecordID"];
					}
				}
				else
				{
					for($i=0; $i<sizeof($SubjectArr); $i++) {
						$ReturnArr[$SubjectArr[$i]["RecordID"]]['CODEID'] = $SubjectArr[$i]["CODEID"];
						$ReturnArr[$SubjectArr[$i]["RecordID"]]['CMP_CODEID'] = $SubjectArr[$i]["CMP_CODEID"];
					}
				}
			}
		}
		return $ReturnArr;
	}
	
	/**
	 * Get IDs and names of all subjects
	 *
	 * @param boolean $ParForSelection True will result in different format ("<br>" or "()") of subject name
	 * @return array Associated array (two level) contain ID/Name pair
	 */
	function GET_ALL_SUBJECTS($ParForSelection=0) {
		
		$PreloadArrKey = 'Get_All_Subjects_Arr';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		if ($ParForSelection==1)
			$SubjectField = "IF(a.CH_DES='' OR a.CH_DES IS NULL, a.EN_DES, CONCAT(a.EN_DES, ' (', a.CH_DES, ')'))";
		else
			$SubjectField = "IF(a.CH_DES='' OR a.CH_DES IS NULL, a.EN_DES, CONCAT(a.EN_DES, '<br />', a.CH_DES))";
		
		if ($ParForSelection==1)
			$CmpSubjectField = "IF(b.CH_DES='' OR b.CH_DES IS NULL, b.EN_DES, CONCAT(b.EN_DES, ' (', b.CH_DES, ')'))";
		else
			$CmpSubjectField = "IF(b.CH_DES='' OR b.CH_DES IS NULL, b.EN_DES, CONCAT(b.EN_DES, '<br />', b.CH_DES))";
		#$SubjectField = ($ParForSelection==1) ? "CONCAT(a.EN_DES, ' (', a.CH_DES, ')')" : "CONCAT(a.EN_DES, '<br />', a.CH_DES)";
		#$CmpSubjectField = ($ParForSelection==1) ? "CONCAT(b.EN_DES, ' (', b.CH_DES, ')')" : "CONCAT(b.EN_DES, '<br />', b.CH_DES)";
		$sql = "SELECT DISTINCT
						a.RecordID,
						$SubjectField,
						b.RecordID,
						$CmpSubjectField,
						If (b.CMP_CODEID IS NULL Or b.CMP_CODEID='', 1, 2) as ParentCmpDisplayOrder
					FROM
						ASSESSMENT_SUBJECT AS a
						/* LEFT JOIN ASSESSMENT_SUBJECT AS b ON a.CODEID = b.CODEID */
						Inner Join ASSESSMENT_SUBJECT AS b ON a.CODEID = b.CODEID
					WHERE
						a.EN_SNAME IS NOT NULL
						AND (a.CMP_CODEID IS NULL Or a.CMP_CODEID='')
						AND a.RecordStatus = 1
						AND b.RecordStatus = 1
					ORDER BY
						ParentCmpDisplayOrder,
						a.DisplayOrder,
						b.DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 4);
		
		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($SubjectID, $SubjectName, $CmpSubjectID, $CmpSubjectName) = $SubjectArr[$i];
			if($SubjectID==$CmpSubjectID)
			{
				$CmpSubjectID = 0;
			}
			else
			{
				$SubjectName = $CmpSubjectName;
			}
			$ReturnArr[$SubjectID][$CmpSubjectID] = $SubjectName;
			//ksort($ReturnArr[$SubjectID]);
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
		return $ReturnArr;
	}
	
	function GET_PARENT_SUBJECT_ID($CmpSubjectID){
		$PreloadArrKey = 'GET_PARENT_SUBJECT_ID';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	global $PATH_WRT_ROOT;
    	
		$returnVal = '';
		if($CmpSubjectID != "" && $CmpSubjectID != null){
			//include_once("subject_class_mapping.php");
			//$objSubject = new subject($CmpSubjectID);
			//if ($objSubject->Is_Component_Subject())
			//{
				//$objSubjectComponent = new Subject_Component($CmpSubjectID);
				//$ParentInfoArr = $objSubjectComponent->Get_Parent_Subject();
				//$ParSubjectID = $ParentInfoArr['RecordID'];
				
				
			//}
			//$returnVal = $ParSubjectID;
			
			if ($this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($CmpSubjectID))
			{
				if (!isset($this->PreloadInfoArr['GET_PARENT_SUBJECT_ID_SubjectInfoArr'])) {
					include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
					$libscm = new subject_class_mapping();
					$SubjectInfoArr = $libscm->Get_Subject_List_With_Component();
					
					$this->PreloadInfoArr['GET_PARENT_SUBJECT_ID_SubjectInfoArr'] = BuildMultiKeyAssoc($SubjectInfoArr, 'SubjectID', array('ParentSubjectID'), $SingleValue=1);
					unset($SubjectInfoArr);
				}
				$ParentSubjectID = $this->PreloadInfoArr['GET_PARENT_SUBJECT_ID_SubjectInfoArr'][$CmpSubjectID];
			}
			$returnVal = $ParentSubjectID;
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnVal);
		return $returnVal;
	}
	
	/**
	 * Get an array mapping SubjectID/DisplayOrder of assessment subject
	 *
	 * @param array $subjectIDArr Array of SubjectID
	 * @return array Associated array mapping SubjectID/DisplayOrder
	 */
	function GET_SUBJECTS_DISPLAY_ORDER($subjectIDArr) {
		
		
		$subjectID = implode(",", $subjectIDArr);
		$sql  = "SELECT RecordID, DisplayOrder FROM ASSESSMENT_SUBJECT ";
		$sql .= "WHERE RecordID IN ($subjectID) AND RecordStatus = 1 ORDER BY RecordID";
		
		$result = $this->returnArray($sql);
		if ($result) {
			for($i=0; $i<sizeof($result); $i++) {
				$returnArr[$result[$i]["RecordID"]] = $result[$i]["DisplayOrder"];
			}
			return $returnArr;
		} else {
			return false;
		}
	}
	
	/**
	 * Delete entry in RC_SUBJECT_FORM_GRADING according to the exist add-on subjects in ASSESSMENT_SUBJECT
	 * Prevent data corruption if add-on subjects are deleted in admin-console
	 *
	 * @param int $ClassLevelID
	 * @return boolean
	 */
	function DELETE_NOT_EXIST_SUBJECT_FORM_GRADING($ClassLevelID) {
		
		
		$sql  = "SELECT RecordID FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1";
		$result = $this->returnVector($sql);
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		if (sizeof($result) > 0) {
			$existSubjectIDList = implode(",", $result);
			$sql = "DELETE FROM $table WHERE SubjectID NOT IN ($existSubjectIDList) AND ClassLevelID = '$ClassLevelID'";
		} else {
			$sql = "DELETE FROM $table WHERE ClassLevelID = '$ClassLevelID'";
		}
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * Construct the array return by GET_FROM_SUBJECT_GRADING_SCHEME() according to their Subject <> Sub-subjects relation
	 * defined in ASSESSMENT_SUBJECT
	 *
	 * @assumption: $SGSArr is ordered by DisplayOrder (mixed with sub-subjects) but parent subjects will be on proper order after removing all sub-subjects
	 * @remark: A parameter $setReturnOneArr is used to control the format of return Array
	 * $setReturnOneArr : 1 -- return single array with sorting all subjects and sub-subjects order 
	 *                    0 -- return 2 arrays (ParentSubject Array & Sub-subject Array)
	 *
	 * @param array $SGSArr Return by GET_FROM_SUBJECT_GRADING_SCHEME
	 * @param int $setReturnOneArr
	 * @return boolean
	 */
	function CONSTRUCT_FROM_SUBJECT_GRADING_SCHEME($SGSArr=array(), $setReturnOneArr=1){
		
		
		// get all subject and their relations defined in term of CODEID & CMP_CODEID
		$sql = "SELECT RecordID, CODEID, CMP_CODEID FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1";
		$result = $this->returnArray($sql);
		
		if ($result) {
			// create an array to map CODEID to RecordID (=Subject ID) for parent subjects
			for($i=0; $i<sizeof($result); $i++) {
				if ($result[$i]["CMP_CODEID"] == "") {
					$subjectCodeMap[$result[$i]["CODEID"]] = $result[$i]["RecordID"];
				}
			}
			
			// create an array using the ID of sub-subject as key and the ID of parent subject as value
			// indicating which sub-subject belongs to which parent subject	
			$subSubjectRelation = array();
			for($i=0; $i<sizeof($result); $i++) {
				// CMP_CODEID != "" is the condition indicating it is a sub-subject
				if ($result[$i]["CMP_CODEID"] != "") {
					$subSubjectRelation[$result[$i]["RecordID"]] = $subjectCodeMap[$result[$i]["CODEID"]];
				}
			}
			
			// create two array, 
			//	$subSubjects to holds all sub-subjects belong to a parent (using parent's subject ID as the key)
			//	$parentSubjects to hold all parent subjects excluding all sub-subjects
			for($i=0; $i<sizeof($SGSArr); $i++) {
				if (array_key_exists($SGSArr[$i]["SubjectID"], $subSubjectRelation)) {
					$subSubjects[$subSubjectRelation[$SGSArr[$i]["SubjectID"]]][] = $SGSArr[$i];
				} else {
					$parentSubjects[] = $SGSArr[$i];
				}
			}
			
			// default to generate a array with sorting all parents subjects and their sub-subjects in proper order
			// the control parameter is set to default (1) 
			if($setReturnOneArr){
				$cnt = 0;
				$result = array();
				for($i=0 ; $i<count($parentSubjects) ; $i++){
					$result[$cnt] = $parentSubjects[$i];
					$cnt++;
					if(count($subSubjects) > 0){
						foreach($subSubjects as $key => $data){
							if($key == $parentSubjects[$i]['SubjectID']){
								for($j=0 ; $j<count($data) ; $j++){
									$result[$cnt] = $data[$j];
									$cnt++;
								}
							}
						}
					}
				}
				return $result;
			} else {
				return array($parentSubjects, $subSubjects);
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Check if subject grading is enable for the subjects of a Class Level
	 *
	 * @param int $ClassLevelID
	 * @return array List of all subjects with value 1 or 0 indicate if it has scheme
	 */
	function CHECK_FROM_SUBJECT_GRADING($ClassLevelID, $ReportID='') {
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		# Marcus 20101011 (template based grading scheme) 
		if(trim($ReportID)=='')
			$cond_ReportID = " AND ReportID = 0 ";
		else
			$cond_ReportID = " AND ReportID = '$ReportID' ";
		
		$sql  = "SELECT SubjectID ";
		$sql .= "FROM $table ";
		$sql .= "WHERE ClassLevelID = $ClassLevelID $cond_ReportID";
		$result = $this->returnVector($sql);
		$allSubjects = $this->GET_ALL_SUBJECTS();
		
		$returnArr = array();
		if (sizeof($allSubjects) > 0) {
			foreach($allSubjects as $key => $value) {
				if (in_array($key, $result))
					$returnArr[$key] = 1;
				else
					$returnArr[$key] = 0;
				if (sizeof($value) > 1) {
					foreach ($value as $innerKey => $innerValue) {
						if ($innerKey != 0) {
							if (in_array($innerKey, $result))
								$returnArr[$innerKey] = 1;
							else
								$returnArr[$innerKey] = 0;
						}
					}
				}
			}
		}
		return $returnArr;
	}
	
	/**
	 * Compare the updated subject grading setting with the existing setting
	 * Do INSERT (newly checked subjects) or DELETE (unchecked subjects)
	 *
	 * @param int $ClassLevelID
	 * @param array $updatedArr Updated subject grading setting of the class level
	 * @return boolean
	 */
	 # Modified by Marcus 20101011 (for template based grading scheme) 
	function CHANGE_FROM_SUBJECT_GRADING($ClassLevelID, $updatedArr) {
		
		
		$result = array();

		$FormReportList = $this->Get_Report_List($ClassLevelID);
		$FormReportIDList = Get_Array_By_Key($FormReportList, "ReportID");
		$FormReportIDList[] = 0;
		foreach($FormReportIDList as $thisReportID)
		{
			$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
			$insertSQL = "	INSERT IGNORE INTO $table 
							(SubjectFormGradingID,SubjectID,ClassLevelID,DisplayOrder,SchemeID,ScaleInput,ScaleDisplay,LangDisplay,DisplaySubjectGroup,DateInput,DateModified,ReportID)
						VALUES 
						";
			
			$existArr = $this->CHECK_FROM_SUBJECT_GRADING($ClassLevelID, $thisReportID);
			$diffSubj = array_diff_assoc($updatedArr, $existArr);
			$subjects = array_keys($diffSubj);
			$defaultOrder = $this->GET_SUBJECTS_DISPLAY_ORDER($subjects);
			
			foreach ($diffSubj as $SubjectID => $value) {
				if ($value == 1) {
					// (SubjectFormGradingID, SubjectID, ClassLevelID, DisplayOrder,SchemeID,ScaleInput,ScaleDisplay,LangDisplay,DisplaySubjectGroup,DateInput,DateModified)
						$insertItem[] = "(NULL, $SubjectID, $ClassLevelID, ".$defaultOrder[$SubjectID].", NULL, NULL, NULL, NULL, NULL, NOW(), NOW(),'$thisReportID')";
				} else if ($value == 0) {
					$deleteSQL = "DELETE FROM $table WHERE ClassLevelID = $ClassLevelID AND SubjectID = $SubjectID";
					$result[] = $this->db_db_query($deleteSQL);
				}
			}
			if (isset($insertItem) && sizeof($insertItem) > 0) {
				$insertSQL .= implode(",", $insertItem);
				$result[] = $this->db_db_query($insertSQL) or die($insertSQL.mysql_error());

			}
		}
		return !in_array(false, $result);
	}
		
	/**
	 * Insert new grading scheme range(s)
	 *
	 * @param array $RangeDetailArr Array contains new range info
	 * @return boolean
	 */
	function INSERT_GRADING_SCHEME_RANGE($RangeDetailArr) {
		if (sizeof($RangeDetailArr) == 0)
			return false;
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql = "INSERT INTO $table VALUES ";
		for($i=0; $i<sizeof($RangeDetailArr); $i++) {
			$rows[] = "(NULL, 
						".$RangeDetailArr[$i]["SchemeID"].",
						'".$RangeDetailArr[$i]["Nature"]."',
						".$RangeDetailArr[$i]["LowerLimit"].",
						".$RangeDetailArr[$i]["UpperLimit"].",
						'".$RangeDetailArr[$i]["Grade"]."',
						".$RangeDetailArr[$i]["GradePoint"].",
						NOW(), 
						NOW(),
						'".$RangeDetailArr[$i]["AdditionalCriteria"]."',
						'".$RangeDetailArr[$i]["Description"]."'
						)";
		}
		$sql .= implode(",", $rows);
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * Update the detail of a grading scheme range specify by GradingSchemeRangeID
	 * Only update ONE row at a time
	 *
	 * @param int $GradingSchemeRangeID
	 * @param array $RangeDetail Array contains updated range info
	 * @return boolean
	 */
	function UPDATE_GRADING_SCHEME_RANGE($GradingSchemeRangeID, $RangeDetail) {
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql = 	"UPDATE $table SET 
					LowerLimit = ".$RangeDetail["LowerLimit"].", 
					UpperLimit = ".$RangeDetail["UpperLimit"].",
					Grade = '".$RangeDetail["Grade"]."',
					GradePoint = ".$RangeDetail["GradePoint"].",
					AdditionalCriteria = ".$RangeDetail["AdditionalCriteria"].",
					Description = '".$RangeDetail["Description"]."',
					DateModified = NOW()
				WHERE GradingSchemeRangeID = $GradingSchemeRangeID
				";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	/**
	 * Delete grading scheme ranges specify by GradingSchemeRangeID or SchemeID
	 *
	 * @param int $SchemeID Delete all ranges of the grading scheme
	 * @param int $GradingSchemeRangeID Delete one range only when provided
	 * @return boolean
	 */
	function DELETE_GRADING_SCHEME_RANGE($SchemeID, $GradingSchemeRangeID="") {
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql = "DELETE FROM $table WHERE ";
		if ($GradingSchemeRangeID !="")
			$sql .= "GradingSchemeRangeID = $GradingSchemeRangeID";
		else
			$sql .= "SchemeID = $SchemeID";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * Get all subject grading schemes in a class level
	 * Since the data from query is not sorted properly with Sub-subjects
	 * it needs to sort data by CONSTRUCT_FROM_SUBJECT_GRADING_SCHEME
	 *
	 * @param int $ClassLevelID
	 * @return array Grading schemes info of subjects
	 */
	 # Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, $withGrandMark=0, $ReportID='')
	{
		$tmpResult = array();
		$result = array();
		
		
		if(trim($ReportID)=='')
			$cond_ReportID = " AND a.ReportID = 0 ";
		else
			$cond_ReportID = " AND a.ReportID = $ReportID ";
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$subjectTable = "ASSESSMENT_SUBJECT";
		$sql  = " SELECT a.SubjectID, a.SchemeID, a.DisplayOrder, a.ScaleInput, a.ScaleDisplay, LangDisplay, DisplaySubjectGroup ";
		$sql .= " FROM $table as a INNER JOIN $subjectTable as b ON a.SubjectID = b.RecordID ";
		$sql .= " WHERE a.ClassLevelID = $ClassLevelID $cond_ReportID AND b.RecordStatus = 1";
		$sql .= " ORDER BY a.DisplayOrder";
		$tmpResult = $this->returnArray($sql, 2);
		
		// re-generate the order including parent subject and its subjects in proper format
		if(count($tmpResult) > 0)
			$result = $this->CONSTRUCT_FROM_SUBJECT_GRADING_SCHEME($tmpResult, 1);
			
		
		$allSubjects = $this->GET_ALL_SUBJECTS();
		
		$returnArr = array();
		$is_cmpSubject = 0;
		$subjectName = '';
		$parentSubjectID = '';
		if(count($result) > 0 && count($allSubjects) > 0){
			for($i=0 ; $i<count($result) ; $i++){
				$flag = 0;
				foreach($allSubjects as $subjectID => $data){
					if(is_array($data)){
						$parentSubjectID = $subjectID;
						foreach($data as $cmpSubjectID => $subjectName){
							if($result[$i]['SubjectID'] == $cmpSubjectID || $result[$i]['SubjectID'] == $subjectID){
								$subjectID = ($cmpSubjectID==0) ? $subjectID : $cmpSubjectID;
								$is_cmpSubject = ($cmpSubjectID==0) ? 0 : 1;
								$flag = 1;
								break;
							}
						}
					}
					if($flag)
						break;
				}
				
				$returnArr[$result[$i]['SubjectID']]['schemeID'] = $result[$i]['SchemeID'];
				$returnArr[$result[$i]['SubjectID']]['displayOrder'] = $result[$i]['DisplayOrder'];
				$returnArr[$result[$i]['SubjectID']]['scaleInput'] = $result[$i]['ScaleInput'];
				$returnArr[$result[$i]['SubjectID']]['scaleDisplay'] = $result[$i]['ScaleDisplay'];
				$returnArr[$result[$i]['SubjectID']]['LangDisplay'] = $result[$i]['LangDisplay'];
				$returnArr[$result[$i]['SubjectID']]['DisplaySubjectGroup'] = $result[$i]['DisplaySubjectGroup'];
				$returnArr[$result[$i]['SubjectID']]['is_cmpSubject'] = $is_cmpSubject;
				$returnArr[$result[$i]['SubjectID']]['subjectName'] = $subjectName;
				$returnArr[$result[$i]['SubjectID']]['parentSubjectID'] = $parentSubjectID;
			}
		}
		
		if ($withGrandMark)
		{
			# Get Grand Mark Grading Scheme
			/*
			 *	SubjectID = -1 => GrandAverage
			 *	SubjectID = -2 => GrandTotal
			 *	SubjectID = -3 => GPA
			 */
			$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
			$sql  = " SELECT SubjectID, SchemeID, ScaleInput, ScaleDisplay ";
			$sql .= " FROM $table a";
			$sql .= " WHERE ClassLevelID = '$ClassLevelID' $cond_ReportID AND SubjectID < 0";
			$returnArrTemp = $this->returnArray($sql, 5);
			
			for ($i=0; $i<count($returnArrTemp); $i++)
			{
				$thisSubjectID = $returnArrTemp[$i]['SubjectID'];
				
				$returnArr[$thisSubjectID]['schemeID'] = $returnArrTemp[$i]['SchemeID'];
				$returnArr[$thisSubjectID]['scaleInput'] = $returnArrTemp[$i]['ScaleInput'];
				$returnArr[$thisSubjectID]['scaleDisplay'] = $returnArrTemp[$i]['ScaleDisplay'];
			}
		}
		
		return $returnArr;
	}
	
	function Get_Grand_Mark_Grading_Scheme_Index_Arr()
	{
		$GrandDisplayArr = array( 	'-1' => 'GrandAverage',
									'-2' => 'GrandTotal',
									'-3' => 'GPA'
								);
		return $GrandDisplayArr;
	}
	
	function Load_Batch_Grand_Mark_Grading_Scheme($ClassLevelID)
	{
		if(empty($this->Grand_Mark_Grading_Scheme[$ClassLevelID]))
		{
			$tmpResult = array();
			$result = array();
			$returnArr = array();
		
			 
//			if(trim($ReportID)=='')
//				$cond_ReportID = " AND ReportID = 0 ";
//			else
//				$cond_ReportID = " AND ReportID = '$ReportID' ";
			
			/*
			 *	SubjectID = -1 => GrandAverage
			 *	SubjectID = -2 => GrandTotal
			 *	SubjectID = -3 => GPA
			 */
			$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
			$sql  = " SELECT SubjectID, SchemeID, ScaleInput, ScaleDisplay, ReportID ";
			$sql .= " FROM $table ";
			$sql .= " WHERE ClassLevelID = '$ClassLevelID' AND SubjectID < 0";
			$returnArrTemp = $this->returnArray($sql, 5);
			
			for ($i=0; $i<count($returnArrTemp); $i++)
			{
				$thisSubjectID = $returnArrTemp[$i]['SubjectID'];
				$thisReportID = $returnArrTemp[$i]['ReportID'];
				
				$returnArr[$thisReportID][$thisSubjectID]['SchemeID'] = $returnArrTemp[$i]['SchemeID'];
				$returnArr[$thisReportID][$thisSubjectID]['ScaleInput'] = $returnArrTemp[$i]['ScaleInput'];
				$returnArr[$thisReportID][$thisSubjectID]['ScaleDisplay'] = $returnArrTemp[$i]['ScaleDisplay'];
			}
			$this->Grand_Mark_Grading_Scheme[$ClassLevelID] = $returnArr;
			//hdebug_pr($this->Grand_Mark_Grading_Scheme[$ClassLevelID]);
		}
	}
	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID='')
	{
//		$tmpResult = array();
//		$result = array();
//		$returnArr = array();
//	
//		 
//		if(trim($ReportID)=='')
//			$cond_ReportID = " AND ReportID = 0 ";
//		else
//			$cond_ReportID = " AND ReportID = '$ReportID' ";
//		
//		/*
//		 *	SubjectID = -1 => GrandAverage
//		 *	SubjectID = -2 => GrandTotal
//		 *	SubjectID = -3 => GPA
//		 */
//		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
//		$sql  = " SELECT SubjectID, SchemeID, ScaleInput, ScaleDisplay ";
//		$sql .= " FROM $table ";
//		$sql .= " WHERE ClassLevelID = '$ClassLevelID' $cond_ReportID AND SubjectID < 0";
//		$returnArrTemp = $this->returnArray($sql, 5);
//		
//		for ($i=0; $i<count($returnArrTemp); $i++)
//		{
//			$thisSubjectID = $returnArrTemp[$i]['SubjectID'];
//			
//			$returnArr[$thisSubjectID]['SchemeID'] = $returnArrTemp[$i]['SchemeID'];
//			$returnArr[$thisSubjectID]['ScaleInput'] = $returnArrTemp[$i]['ScaleInput'];
//			$returnArr[$thisSubjectID]['ScaleDisplay'] = $returnArrTemp[$i]['ScaleDisplay'];
//		}

		$ReportID = trim($ReportID)==''?0:$ReportID;
		$this->Load_Batch_Grand_Mark_Grading_Scheme($ClassLevelID);
		
		$returnArr = $this->Grand_Mark_Grading_Scheme[$ClassLevelID][$ReportID];
		
		return $returnArr;
	}
	
	/**
	 * Get All Grading Scheme ID
	 *
	 * @return array Array of SchemeID
	 */
	function GET_GRADING_SCHEME_ID() {
		# Get the main info of the grading scheme
		$table = $this->DBName.".RC_GRADING_SCHEME";
		$sql  = "SELECT SchemeID FROM $table ORDER BY SchemeID";
		
		$result = $this->returnVector($sql);
		return $result;
	}
	
	/**
	 * Update info about how the grading scheme of a subject in a class level behave 
	 *
	 * @param array $subjectInfo Updated behaviour of grading scheme
	 * @return boolean
	 */
	 # Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function UPDATE_SUBJECT_FORM_GRADING($subjectInfo){
		$success = false;
		$ReportID = trim($subjectInfo["ReportID"])==''?0:$subjectInfo["ReportID"];
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$sql  = "UPDATE $table SET  
					".((isset($subjectInfo["displayOrder"]) && $subjectInfo["displayOrder"] != "") ? "DisplayOrder = ".$subjectInfo["displayOrder"].", " : "")."  
					".(($subjectInfo["schemeID"] != "") ? "SchemeID = ".$subjectInfo["schemeID"].", " : "SchemeID = NULL, ")." 
					".((isset($subjectInfo["scaleInput"])) ? "ScaleInput = '".$subjectInfo["scaleInput"]."', " : "")." 
					".((isset($subjectInfo["scaleDisplay"])) ? "ScaleDisplay = '".$subjectInfo["scaleDisplay"]."', " : "")."
					".((isset($subjectInfo["LangDisplay"])) ? "LangDisplay = '".$subjectInfo["LangDisplay"]."', " : "")." 
					".((isset($subjectInfo["DisplaySubjectGroup"])) ? "DisplaySubjectGroup = '".$subjectInfo["DisplaySubjectGroup"]."', " : "")." 
					DateModified = NOW()
				WHERE
					ClassLevelID = ".$subjectInfo["classLevelID"]." AND
					ReportID = ".$ReportID." AND  
					SubjectID = ".$subjectInfo["subjectID"];
		$success = $this->db_db_query($sql);

		return $success;
	}
	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function INSERT_SUBJECT_FORM_GRADING($subjectInfo){
		$success = false;
		$ReportID = trim($subjectInfo["ReportID"])==''?0:$subjectInfo["ReportID"];
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$sql = "INSERT INTO $table
					(SubjectID, ClassLevelID, DisplayOrder, SchemeID, ScaleInput, ScaleDisplay, LangDisplay, DisplaySubjectGroup, DateInput, DateModified, ReportID)
				VALUES
					(	'".$subjectInfo['subjectID']."',
						'".$subjectInfo['classLevelID']."',
						'".$subjectInfo['displayOrder']."',
						'".$subjectInfo['schemeID']."',
						'".$subjectInfo['scaleInput']."',
						'".$subjectInfo['scaleDisplay']."',
						'".$subjectInfo['LangDisplay']."',
						'".$subjectInfo['DisplaySubjectGroup']."',
						now(),
						now(),
						'".$ReportID."'
					)
				";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	
	################ Report Card Templates ################
	/**
	 * Get/Display reportCard templates
	 *
	 * @param int $ClassLevelID Only display reportcards in a class level
	 * @param boolean $DisplayUI True return HTML for UI, False return an array
	 * @return array Array of reportCard template detail
	 * @return string HTML table rows of reportCard templates
	 */
	function ReportTemplateList($ClassLevelID='', $DisplayUI = 1)
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $button_preview, $button_remove, $button_copy, $eReportCard;
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		include_once('form_class_manage.php');
		
		//$lclass = new libclass();
		
		if($ClassLevelID)
			$conds_classLevel .= " and report.ClassLevelID = $ClassLevelID ";
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$RC_REPORT_TEMPLATE_COLUMN = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		$ReportTypeWithSemesterNameField = $this->Get_Template_Type_With_Semester_NameField('report.', 'ayt.', 'ayt_c.', '', ' ');
		
		/*$sql = "	
			SELECT 
				report.ReportID, 
				y.YearName as LevelName,
				report.ReportTitle, 
				report.DateModified,
				report.ClassLevelID, 
				report.Semester,
				report.isMainReport,
				$ReportTypeWithSemesterNameField as ReportTypeWithSemester
			FROM 
				$table as report
				Left Outer Join
				$RC_REPORT_TEMPLATE_COLUMN as rtc On (report.ReportID = rtc.ReportID)
				INNER Join
				YEAR as y On (report.ClassLevelID = y.YearID)
				Left Outer Join
				ACADEMIC_YEAR_TERM as ayt On (report.Semester = ayt.YearTermID)
				Left Outer Join
				ACADEMIC_YEAR_TERM as ayt_c On (rtc.SemesterNum = ayt_c.YearTermID)
			Group By
				report.ReportID
		";*/
		$sql = "	
			SELECT 
				report.ReportID, 
				y.YearName as LevelName,
				report.ReportTitle, 
				report.DateModified,
				report.ClassLevelID, 
				report.Semester,
				report.isMainReport,
				$ReportTypeWithSemesterNameField as ReportTypeWithSemester
			FROM 
				$table as report
				Left Join
				$RC_REPORT_TEMPLATE_COLUMN as rtc On (report.ReportID = rtc.ReportID)
				INNER Join
				YEAR as y On (report.ClassLevelID = y.YearID)
				Left Join
				ACADEMIC_YEAR_TERM as ayt On (report.Semester = ayt.YearTermID)
				Left Join
				ACADEMIC_YEAR_TERM as ayt_c On (rtc.SemesterNum = ayt_c.YearTermID)
			Where
				1
				$conds_classLevel
			Group By
				report.ReportID
			ORDER BY
				y.Sequence, report.Semester, ayt.TermStart, report.isMainReport Desc, report.ReportTitle
		";
		$result = $this->returnArray($sql);
		
		if(!$DisplayUI)		
		{
			return $result;	
			exit;
		}
		
		$data = array();
		$FormNameArr = array();
		for($i=0;$i<sizeof($result);$i++)
		{
			$thisClassLevelID = $result[$i]['ClassLevelID'];
			$thisReportID = $result[$i]['ReportID'];
			
			$data[$thisClassLevelID][$thisReportID]['ReportTitle'] = str_replace(":_:", "<br>",$result[$i]['ReportTitle']);
			$data[$thisClassLevelID][$thisReportID]['DateModified'] = $result[$i]['DateModified'];
			$data[$thisClassLevelID][$thisReportID]['Semester'] = $result[$i]['Semester'];
			$data[$thisClassLevelID][$thisReportID]['isMainReport'] = $result[$i]['isMainReport'];
			$data[$thisClassLevelID][$thisReportID]['ReportTypeWithSemester'] = $result[$i]['ReportTypeWithSemester'];
			$FormNameArr[$thisClassLevelID] = $result[$i]['LevelName'];
		}
		
		// Form Looping
		foreach($data as $ClassLevelID => $value1)
		{   
			$row .= "
				<tr class=\"resubtabletop\">
					<td width=\"80\" class=\"tabletext\"><strong>". $FormNameArr[$ClassLevelID] ."</strong></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td id=\"formTD{$ClassLevelID}\"><a href=\"javascript:copyForm(". $ClassLevelID .");\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/btn_copy.gif\" border=\"0\" alt=\"{$button_copy}\" title=\"{$button_copy}\"></a> </td>
				</tr>
			";
					
			// Report Template Looping
			$rowi = 0;
			foreach($value1 as $ReportID => $value2)
			{
				$ReportTitle = $value2['ReportTitle'];
				$ReportType = $value2['ReportTypeWithSemester'];
				
				## added the related term name for display by Ivan on 18 Aug 2009
				//$ReportType = $value2['Semester'] == "F" ? $eReportCard['WholeYearReport'] : $eReportCard['TermReport'];
				/*
				if ($value2['Semester'] == 'F')
				{
					$ReportType = $eReportCard['WholeYearReport'];
					
					$thisColumnData = $this->returnReportTemplateColumnData($ReportID);
					$numOfColumn = count($thisColumnData);
					if ($numOfColumn > 0)
					{
						$TermTitleArr = array();
						for ($i=0; $i<$numOfColumn; $i++)
						{
							$thisYearTermID = $thisColumnData[$i]['SemesterNum'];
							
							$ObjYearTerm = new academic_year_term($thisYearTermID);
							$TermTitleArr[] = $ObjYearTerm->Get_Year_Term_Name();
						}
						
						$ReportType .= ' ('.implode(', ', $TermTitleArr).')';
					}
					
				}
				else
				{
					$isMainTermReport = $value2['isMainReport'];
					$TermReportTypeText = ($isMainTermReport==1)? $eReportCard['Main'] : $eReportCard['Extra'];
					$ReportType = $TermReportTypeText.' '.$eReportCard['TermReport'];
					
					$ObjYearTerm = new academic_year_term($value2['Semester']);
					$ReportType .= ' ('.$ObjYearTerm->Get_Year_Term_Name().')';
				}
				*/
				
				$DateModified = ($value2['DateModified'] != '' && $value2['DateModified'] != '0000-00-00 00:00:00') ? $value2['DateModified'] : "---";
				if ($this->isReportTemplateComplete($ReportID)) {
					$remark = $eReportCard['SettingsCompleted'];
					$remarkStyle = "";
				} else {
					$remark = $eReportCard['SettingsNotCompleted'];
					$remarkStyle = "status_alert";
				}
				
				$row .= "
					<tr class=\"tablerow". ($rowi%2==1?1:2) ."\"> 
						<td align=\"right\" class=\"tabletext\"><a href=\"javascript:newWindow('preview.php?ReportID=$ReportID', '10');\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" alt=\"{$button_preview}\" title=\"{$button_preview}\"></a></td>
						<td class=\"tabletext\"><a href=\"new.php?ReportID={$ReportID}&ClassLevelID={$ClassLevelID}\" class=\"tablelink\">{$ReportTitle}</a></td>
						<td class=\"tabletext\">{$ReportType}</td>
						<td class=\"tabletext\">{$DateModified}</td>
						<td class=\"tabletext $remarkStyle\">". $remark ."</td>
						<td class=\"tabletext\" id=\"formTermTD{$ReportID}\" nowrap>
							<a href=\"javascript:copyTerm({$ReportID});\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"{$button_copy}\" title=\"{$button_copy}\"></a> 
							<a href=\"javascript:cfmDelete({$ReportID});\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"{$button_remove}\" title=\"{$button_remove}\"></a>
						</td>
					</tr>
				";
				$rowi++;
			}
		}	
		return $row;
	}
	
	/**
	 * INSERT a new report template column
	 *
	 * @param array $columnInfo Detail of the new column
	 * @return boolean
	 */
	function INSERT_REPORT_TEMPLATE_COLUMN($columnInfo) {
		$success = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		$sql  = "INSERT INTO $table VALUES (";
		$sql .= "NULL, 
				".$columnInfo["ReportID"].", 
				'".$columnInfo["ColumnTitle"]."', 
				".$columnInfo["DefaultWeight"].", 
				".$columnInfo["DisplayOrder"].", 
				".$columnInfo["ShowPosition"].", 
				'".$columnInfo["PositionRange"]."', 
				".$columnInfo["SemesterNum"].", 
				NOW(), NOW()
				)";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * UPDATE a report template column specify by ReportColumnID
	 *
	 * @param array $columnInfo Detail of the updated column with ID
	 * @return boolean
	 */
	function UPDATE_REPORT_TEMPLATE_COLUMN($columnInfo) {
		$success = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		$sql  = "UPDATE $table SET 
					ReportID = '".$columnInfo["ReportID"]."', 
					ColumnTitle = '".$columnInfo["ColumnTitle"]."', 
					DefaultWeight = ".$columnInfo["DefaultWeight"].", 
					DisplayOrder = ".$columnInfo["DisplayOrder"].", 
					ShowPosition = ".$columnInfo["ShowPosition"].", 
					PositionRange = '".$columnInfo["PositionRange"]."', 
					SemesterNum = ".$columnInfo["SemesterNum"].", 
					DateModified = NOW()
				WHERE
					ReportColumnID = ".$columnInfo["ReportColumnID"];
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * DELETE a report template column specify by table field name and its value
	 *
	 * @param string $column A table field name
	 * @param string $value Value for the field
	 * @return boolean
	 */
	function DELETE_REPORT_TEMPLATE_COLUMN($column, $value) {
		$success = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		if($column=="ReportColumnID")	$this->DELETE_MARKSHEET_SCORE($value);
		if($column=="ReportID")
		{
			$sql = "select ReportColumnID from $table where ReportID='$value'";	
			$x = $this->returnArray($sql);
			foreach($x as $key=>$data)
			{
				$this->DELETE_MARKSHEET_SCORE($data[0]);
			}
		}
		
		$sql = "DELETE FROM $table WHERE $column = '$value'";
		$success = $this->db_db_query($sql);
		return success;
	}

	/****** Sub-marksheet Related Functions ******/
	function INSERT_SUB_MARKSHEET_SCORE($columnInfo) {
		$success = false;
		$table = $this->DBName.".RC_SUB_MARKSHEET_SCORE";
		$sql  = "INSERT INTO $table 
				(ColumnID, StudentID, MarkRaw, TeacherID, DateInput, DateModified)
				VALUES (";
		$sql .= "
				".$columnInfo["ColumnID"].", 
				'".$columnInfo["StudentID"]."', 
				".$columnInfo["MarkRaw"].", 
				'".$columnInfo["TeacherID"]."', 
				NOW(), NOW()
				)";
		$success = $this->db_db_query($sql);
		
		if (!$success)
		{
			// may exist, need to update
			$sql  = "UPDATE $table SET 
					MarkRaw = ".$columnInfo["MarkRaw"].", 
					TeacherID = '".$columnInfo["TeacherID"]."',
					DateModified = NOW()
				WHERE
					ColumnID = ".$columnInfo["ColumnID"]." AND
					StudentID = ".$columnInfo["StudentID"];
			$success = $this->db_db_query($sql);
		}

		return $success;
	}
	
	
	function INSERT_SUB_MARKSHEET_COLUMN($columnInfo) {
		$success = false;
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		$sql  = "INSERT INTO $table ";
		$sql .= " (SubjectID, ReportColumnID, ColumnTitle, Ratio, Calculation, FullMark, DateInput, DateModified) ";
		$sql .= " VALUES (";
		$sql .= "
				'".$columnInfo["SubjectID"]."', 
				'".$columnInfo["ReportColumnID"]."', 
				'".$columnInfo["ColumnTitle"]."', 
				'".$columnInfo["Ratio"]."',
				'".$columnInfo["Calculation"]."',
				'".$columnInfo["FullMark"]."',
				NOW(), NOW()
				)";
				
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/**
	 * @return boolean
	 */
	function UPDATE_SUB_MARKSHEET_COLUMN($columnInfo) {
		$success = false;
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		# update title and ratio only
		$sql  = "UPDATE $table SET 
					ColumnTitle = '".$columnInfo["ColumnTitle"]."', 
					Ratio = '".$columnInfo["Ratio"]."', 
					FullMark = '".$columnInfo["FullMark"]."', 
					DateModified = NOW()
				WHERE
					ColumnID = ".$columnInfo["ColumnID"];
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function UPDATE_SUB_MARKSHEET_COLUMN_OVERALL_INFO($ColumnID, $Calculation, $TransferToMS) {
		$success = false;
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		
		$sql  = "UPDATE $table SET 
					Calculation = '".$Calculation."',
					TransferToMS = '".$TransferToMS."',
					DateModified = NOW()
				WHERE
					ColumnID = ".$ColumnID;
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	/*
	* Get Sub-Marksheet Column
	*/
	function GET_ALL_SUB_MARKSHEET_COLUMN($ParReportColumnID, $ParSubjectID)
	{
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		$sql = "SELECT
					ColumnID,
					ColumnTitle,
					Ratio,
					Calculation,
					TransferToMS,
					FullMark
				FROM
					{$table}
				WHERE
					ReportColumnID = '$ParReportColumnID'
					AND SubjectID = '$ParSubjectID'
				ORDER BY
					ColumnID
				";
		$row = $this->returnArray($sql, 4);

		return $row;
	}
	
	
	function DELETE_SUB_MARKSHEET_COLUMN($column, $value) {
		$success = false;
		
		# pending to remove scores of this assessment column (done)
		$table = $this->DBName.".RC_SUB_MARKSHEET_SCORE";
		$sql = "DELETE FROM $table WHERE $column = '$value'";
		$success = $this->db_db_query($sql);
		
		# remove sub-marksheet column
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		$sql = "DELETE FROM $table WHERE $column = '$value'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function GET_SUB_MARKSHEET_SCORE($ReportColumnID, $SubjectID) {
		
		# get all columns
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		$fields  = "ColumnID ";
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' ";
		$columnIDArr = $this->returnVector($sql);
		$columnIDList = implode(",", $columnIDArr);
		
		$table = $this->DBName.".RC_SUB_MARKSHEET_SCORE";
		$fields  = "ColumnID, StudentID, MarkRaw, TeacherID ";
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE ColumnID IN ($columnIDList) ";
		
		$result = $this->returnArray($sql,4);
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) {
			$returnArr[$result[$i]["StudentID"]][$result[$i]["ColumnID"]] = $result[$i]["MarkRaw"];
		}
		return $returnArr;
	}

	function GET_SUB_MARKSHEET_SCORE_BY_COLUMN_ID($ColumnID) {
		
		if(is_array($ColumnID))
			$ColumnIDList = implode(",",$ColumnID);
		else
			$ColumnIDList = $ColumnID;
		
		$table = $this->DBName.".RC_SUB_MARKSHEET_SCORE";
		$fields  = "ColumnID, StudentID, MarkRaw, TeacherID ";
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE ColumnID IN ($ColumnIDList) ";
		$result = $this->returnArray($sql,4);
		
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) {
			$thisMark = $result[$i]["MarkRaw"];
			
			if($thisMark < 0) 
				$thisMark = $this->specialCasesSet1[abs($thisMark)];
			
			$returnArr[$result[$i]["StudentID"]][$result[$i]["ColumnID"]] = $thisMark;
		}
		return $returnArr;
	}
		
	function GET_SUB_MARKSHEET_COLUMN_INFO($ColumnID)
	{
		$table = $this->DBName.".RC_SUB_MARKSHEET_COLUMN";
		$fields  = "ColumnID, SubjectID, ReportColumnID, ColumnTitle, Ratio, Calculation, FullMark ";
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE ColumnID = '$ColumnID' ";		
		$result = $this->returnArray($sql,6);
		
		$returnArr = array();
		$returnArr[$ColumnID]["SubjectID"] = $result[0]["SubjectID"];
		$returnArr[$ColumnID]["ReportColumnID"] = $result[0]["ReportColumnID"];
		$returnArr[$ColumnID]["ColumnTitle"] = $result[0]["ColumnTitle"];
		$returnArr[$ColumnID]["Ratio"] = $result[0]["Ratio"];
		$returnArr[$ColumnID]["Calculation"] = $result[0]["Calculation"];
		$returnArr[$ColumnID]["FullMark"] = $result[0]["FullMark"];
		
		return $returnArr;
		
	}
	
	function IS_SUB_MARKSHEET_SCORE_EXIST($ColumnID, $StudentID)
	{
		$table = $this->DBName.".RC_SUB_MARKSHEET_SCORE";
		$fields  = "count(ColumnID) ";
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE ColumnID = '$ColumnID' AND StudentID = '$StudentID' ";		
		$result = $this->returnArray($sql,1);
		
		if ($result[0][0] == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/****** End of Sub-marksheet Related Functions ******/

	# updated on 1st Dec 2008 by Ivan - added ShowNumOfStudentClass and ShowNumOfStudentForm in SQL
	# updated on 1st Jan 2009 by Ivan - added OverallPositionRangeClass and OverallPositionRangeForm in SQL
	function returnReportTemplateBasicInfo($ReportID='', $others='', $ClassLevelID='', $Semester='', $isMainReport=1, $excludeSemester='') {
		$args = func_get_args();
		if(count($args)==1 && trim($ReportID)!='') {
			$this->Load_Report_Template_Basic_Info($ReportID);
			return $this->Report_Template_Basic_Info[$ReportID];	
		}
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		if($ReportID)		$con[] = " ReportID = '$ReportID' ";
		if($ClassLevelID)	$con[] = " ClassLevelID = '$ClassLevelID' ";
		if($Semester)		$con[] = " Semester = '$Semester' ";
		if(!$ReportID)	$con[] = " isMainReport = '$isMainReport' ";
		if($excludeSemester)$con[] = (is_array($excludeSemester))? " Semester Not In ('".implode("','", $excludeSemester)."') " : " Semester != '$excludeSemester' ";
		if($others)			$con[] = $others;
		
		$cons = empty($con) ? "" : implode(" and ", $con);
		$sql = "SELECT 
					ReportID,
					ReportTitle,
					ClassLevelID,
					Semester,
					Description,
					PercentageOnColumnWeight,
					HeaderHeight,
					Footer,
					LineHeight,
					AllowClassTeacherComment,
					AllowSubjectTeacherComment,
					DisplaySettings,
					LastGenerated,
					MarksheetSubmissionEnd,
					MarksheetVerificationEnd,
					ShowSubjectFullMark,
					ShowSubjectOverall, 
					ShowOverallPositionClass,
					ShowOverallPositionForm,
					ShowGrandTotal,
					ShowGrandAvg,
					Issued,
					ShowNumOfStudentClass,
					ShowNumOfStudentForm,
					OverallPositionRangeClass,
					OverallPositionRangeForm,
					isMainReport,
					GreaterThanAverageClass,
					GreaterThanAverageForm,
					ShowSubjectComponent,
					TermStartDate,
					TermEndDate,
					LastGeneratedAcademicProgress,
					AttendanceDays,
					LastGeneratedAward,
					LastGeneratedBy
				FROM 
					$table 
				WHERE
					$cons
			";
		$x = $this->returnArray($sql);
		
		### Term Report Only - if has not input term range, use the default term date range
		if (count($x) > 0 && $x[0]['Semester'] != 'F' && (is_date_empty($x[0]['TermStartDate']) || is_date_empty($x[0]['TermEndDate']))) {
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			
			$YearTermID = $x[0]['Semester'];
			$objYearTerm = new academic_year_term($YearTermID);
			
			$x[0]['TermStartDate'] = substr($objYearTerm->TermStart, 0, 10);
			$x[0]['TermEndDate'] = substr($objYearTerm->TermEnd, 0, 10);
		}
		return $x[0];
	}
	
	function Get_Report_List($ClassLevelID='')
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$ClassLevelCond = '';
		if ($ClassLevelID != '')
			$ClassLevelCond = " AND ClassLevelID = '$ClassLevelID' ";
		
		$sql = "	
				SELECT 
						*
				FROM 
						$table 
				WHERE
						1
						$ClassLevelCond
				ORDER BY
						Semester
				";
		$x = $this->returnArray($sql);
		
		return $x;
	}
	
	function returnReportTemplateTerms($termReportOnly=0)
	{
		global $eReportCard;
		
		$semester_conds = '';
		if ($termReportOnly)
			$semester_conds = " And rt.Semester != 'F' ";
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
//		$sql = "select distinct(Semester) from $table $semester_conds order by Semester";
//		$x = $this->returnArray($sql);
//		
//		$ary = array();
//		foreach($x as $key => $val)
//		{
//			$s = $val['Semester'];
//			if($s == "F")
//				$ary[] = array('F', $eReportCard['WholeYearReport']);
//			else
//				$ary[] = array($s, $this->returnSemesters($s));
//		}
		
		$YearTermNameField = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');
		$sql = "Select
						rt.Semester,
						If (rt.Semester = 'F', '".$eReportCard['WholeYearReport']."', $YearTermNameField) as TermName,
						If (rt.Semester = 'F', 'W', 'T') as ReportType
				From
						$table as rt
						/* INNER JOIN <-- Inner Join failed to get the Whole Year result */
						Left Outer Join
						ACADEMIC_YEAR_TERM as ayt On (rt.Semester = ayt.YearTermID And ayt.AcademicYearID = '".$this->schoolYearID."')
				Where
						1
						$semester_conds
				Group By
						rt.Semester
				Order By
						ReportType, ayt.TermStart
				";
		$ary = $this->returnArray($sql);
		
		return $ary;
	}
	
	function returnReportTemplateColumnData($ReportID, $other="")
	{
		$PreloadArrKey = 'returnReportTemplateColumnData';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		$sql = "	
			SELECT 
				ReportColumnID,
				ColumnTitle,
				DefaultWeight,
				SemesterNum,
				IsDetails,
				ShowPosition,
				PositionRange,
				BlockMarksheet,
				DisplayOrder		
			FROM 
				$table 
			WHERE
				ReportID = $ReportID
		";
		if($other) $sql .= " and $other ";
		$sql .="		
			ORDER BY
				DisplayOrder
		";
		$x = $this->returnArray($sql);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $x);
		return $x;
	}
	
	function returnReportIDByReportColumnID($ReportColumnID)
	{
		$this->Load_ReportColumnID_ReportID_Mapping();
		
		return $this->ReportColumnID_ReportID_Mapping[$ReportColumnID];
//		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
//		
//		$sql = "Select ReportID From $table Where ReportColumnID = '".$ReportColumnID."'";
//		$resultSet = $this->returnVector($sql);
		
//		return $resultSet[0];
	}
	
	function checkTermReportExists($ClassLevelID, $semid)
	{
		$table_TEMPLATE = $this->DBName.".RC_REPORT_TEMPLATE";
		$table_TEMPLATE_COLUMN = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "
			select 
				count(b.ReportColumnID)
			from
				$table_TEMPLATE as a
				left join $table_TEMPLATE_COLUMN as b on (b.ReportID = a.ReportID)
			where 
				a.ClassLevelID=$ClassLevelID 
				and a.Semester=$semid 
				and b.ColumnTitle is not null
		";
		$x = $this->returnArray($sql);
		return $x[0][0];
	}
	
	function Load_Report_Involved_Sem()
	{
		if(empty($this->Load_Report_Involved_Sem))
		{

		}
	}
	
	
	function returnReportInvolvedSem($ReportID, $DisplayName=1)
	{
		if($this->TmpBatchArr['returnReportInvolvedSem']['ReportSemArr'][$ReportID])
			return $this->TmpBatchArr['returnReportInvolvedSem']['ReportSemArr'][$ReportID];
		
		$x = array();

//		$sql = "
//			SELECT 
//				IF(Semester= 'F', SemesterNum ,Semester)
//			FROM 
//				RC_REPORT_TEMPLATE rrt
//				INNER JOIN RC_REPORT_TEMPLATE_COLUMN rrtc ON rrtc.ReportID = rrt.ReportID
//			WHERE
//				rrt.ReportID =  $ReportID
//			ORDER BY 
//				rrt.ReportID, DisplayOrder
//		";
//		$result = $this->returnVector($sql);
//		foreach($result as $key => $semid)
//			$x[$semid] = $this->returnSemesters($semid);
			
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "SELECT Semester FROM $table where ReportID=$ReportID";
		$result = $this->returnVector($sql);
		$sem = $result[0];
		
		if($sem=="F")
		{
			$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
			$sql = "SELECT SemesterNum FROM $table where ReportID = $ReportID ORDER BY DisplayOrder";
			$result = $this->returnVector($sql);
			
			foreach($result as $key => $semid)
			{
				$x[$semid] = $this->returnSemesters($semid);
			}
		}
		else
		{
				$x[$sem] = $this->returnSemesters($sem);
		}
		
		$this->TmpBatchArr['returnReportInvolvedSem']['ReportSemArr'][$ReportID] = $x;
		return $x;
	}
	
	######################### Settings Functions End ###########################
	##################################################################
	
	##################################################################
	####################### Management Functions Start ##########################
	
	################ Other Info ################
	# check the available config files, compare with $this->configFilesType defined in the constructor
	// 20111121
	function getOtherInfoType() {
		global $intranet_root;
		$configFilesPath = $this->configFilesPath;
		$suffix = "_config.csv";
		
		global $lreportcard;
		if($lreportcard)
			$configFilesType = $lreportcard->configFilesType;
		else
			$configFilesType = $this->configFilesType;
		
		$returnArr = array();
		for($i=0; $i<sizeof($configFilesType); $i++) {
			$filename = $configFilesPath.$configFilesType[$i].$suffix;
			
			if (file_exists($filename)) {
				$returnArr[] = $configFilesType[$i];
			}
		}
		
		return $returnArr;
	}
	
	# Create the $TAGS_OBJ for display tabs on the Other Info upload page
	# New condition must be added for more categories of Other Info
	# 	$UploadType = currently selected tab
	# 	ref: /home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php
	function getOtherInfoTabObjArr($UploadType) {
		global $eReportCard, $PATH_WRT_ROOT;
		
		$otherInfoTypes = $this->getOtherInfoType();
		$numOfType = count($otherInfoTypes);
		
		$TAGS_OBJ = array();
		for ($i=0; $i<$numOfType; $i++)
		{
			$thisOtherInfoType = $otherInfoTypes[$i];
			
			$thisTitleDisplay = $eReportCard['OtherInfoArr'][$thisOtherInfoType];
			$thisURL = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=".$thisOtherInfoType;
			
			$thisSelected = ($thisOtherInfoType == $UploadType)? true : false;
			$thisSelected = ($i==0 && !isset($UploadType))? true : $thisSelected;
			
			$TAGS_OBJ[] = array($thisTitleDisplay, $thisURL, $thisSelected);
		}

		
		# tag information
//		if (in_array("summary", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['SummaryInfoUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=summary", ($UploadType=="summary" || !isset($UploadType))?1:0);
//		if (in_array("award", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['AwardRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=award", $UploadType=="award"?1:0);
//		if (in_array("merit", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['MeritRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=merit", $UploadType=="merit"?1:0);
//		if (in_array("eca", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['ECARecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=eca", $UploadType=="eca"?1:0);
//		if (in_array("remark", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['RemarkUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=remark", $UploadType=="remark"?1:0);
//		if (in_array("interschool", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['InterSchoolCompetitionUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=interschool", $UploadType=="interschool"?1:0);
//		if (in_array("schoolservice", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['SchoolServiceUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=schoolservice", $UploadType=="schoolservice"?1:0);
//		if (in_array("attendance", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['AttendanceUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=attendance", $UploadType=="attendance"?1:0);
//		if (in_array("dailyPerformance", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['DailyPerformanceUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=dailyPerformance", $UploadType=="dailyPerformance"?1:0);
//		if (in_array("demerit", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['DemeritUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=demerit", $UploadType=="demerit"?1:0);
//		if (in_array("OLE", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['OLEUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=OLE", $UploadType=="OLE"?1:0);
//		if (in_array("assessment", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['Assessment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=assessment", $UploadType=="assessment"?1:0);
//		if (in_array("post", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['Post'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=post", $UploadType=="post"?1:0);
//		if (in_array("subjectweight", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['SubjectWeight'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=subjectweight", $UploadType=="subjectweight"?1:0);
//		if (in_array("others", $otherInfoTypes))
//			$TAGS_OBJ[] = array($eReportCard['OthersUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/other_info/index.php?UploadType=others", $UploadType=="others"?1:0);
//		
		return $TAGS_OBJ;
	}
	
	# Get an array containing the definition of the upload types
	# For constructing the sample CSV & verifying user's uploaded files
	function getOtherInfoConfig($UploadType) {
		global $intranet_root, $PATH_WRT_ROOT;
		$configFilesType = $this->getOtherInfoType();

		if (!in_array($UploadType, $configFilesType)) {
			return false;
		}
		$configFilesPath = $this->configFilesPath;
		$suffix = "_config.csv";
		$fileName = $configFilesPath.$UploadType.$suffix;

		if (file_exists($fileName)) {
			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			$limport = new libimporttext();
			$data = $limport->GET_IMPORT_TXT($fileName);

			$header = array_shift($data);
			if (sizeof($data) > 0) {
				for($i=0; $i<sizeof($data); $i++) {
					for($j=0; $j<sizeof($header); $j++) {
						$info[$i][$header[$j]] = $data[$i][$j];
					}
				}
				return $info;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/*	Param: $ReportID, $StudentID, $ClassID, $UploadType (can be array or string)
	 *	return: $ReturnArr[$StudentID][$YearTermID][$UploadType] = Value
	 *	Added by Marcus 20/8/2009
	 */
	function getReportOtherInfoData($ReportID, $StudentID='', $ClassID='', $UploadType='', $ArrayWithType=0)
	{
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName;
		
		//set $UpdateType as Ary
		if(!empty($UploadType) && !is_array($UploadType))
			$UploadType = Array($UploadType);
			
		//$latestTerm = "";
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
		$TermStartDate = $ReportSetting['TermStartDate'];
		$TermEndDate = $ReportSetting['TermEndDate'];
		
		$sems = $this->returnReportInvolvedSem($ReportID);
		
		if($ReportType == "W")
			$sems[0] = "Consolidated Term";
		
		if ($ClassID=='' && $StudentID=='')
		{
			$StudentAry=$this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		}
		else
		{
			if($ClassID)		
				$StudentAry=$this->GET_STUDENT_BY_CLASS($ClassID);	
			else
				$StudentAry[] = Array("UserID"=>$StudentID);
		}
		
		
		# retrieve Student Class
		$returnAry = Array();
		//each student loop
		$StudentIDAry = Get_Array_By_Key($StudentAry,"UserID");
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$lu = new libuser('','',$StudentIDAry);
		
		
		foreach($StudentAry as $Student)
		{
			$StudentID = $Student['UserID'];
			if($StudentID)
			{
//				include_once($PATH_WRT_ROOT."includes/libuser.php");
//				$lu = new libuser($StudentID);
				$lu->LoadUserData($StudentID);
				if(!$ClassID)
				{
					$ClassInfo 		= $this->Get_Student_Class_ClassLevel_Info($StudentID);
					$thisClassID 	= $ClassInfo[0]['ClassID'];
				}
				else
				{
					$thisClassID = $ClassID;
				}
				$WebSamsRegNo 	= $lu->WebSamsRegNo;
				
			}
			
			$ary = array();
			$csvType = $this->getOtherInfoType();
			
			
			//each term loop
			foreach($sems as $TermID => $Term)
			{
				
				//for munsang
				if($ReportCardCustomSchoolName == "munsang_college")
				{
					$munsangeDisdata = $this->Get_eDiscipline_Data($TermID, $StudentID);

					//for munsang (attendance)
					global $GlobalAttendanceClassData;
					if(!isset($GlobalAttendanceClassData[$TermID][$thisClassID]))
					{
						$GlobalAttendanceClassData[$TermID][$thisClassID] = $this->Get_Student_Profile_Attendance_Data($TermID, $thisClassID, $TermStartDate, $TermEndDate);
					}
					$AttendanceData = $GlobalAttendanceClassData[$TermID][$thisClassID];
					
				}
				
				//each type loop
				foreach($csvType as $k => $Type)
				{
					if(!empty($UploadType) && !in_array($Type,$UploadType)) 
						continue;
					 
					$csvData = $this->getOtherInfoData($Type, $TermID, $thisClassID, $ClassLevelID);
					
					$config = $this->getOtherInfoConfig($Type);
					$ConfigTitleArr = Get_Array_By_Key($config, "EnglishTitle");
					
 					foreach((array)$ConfigTitleArr as $key)
 					{
 						$key = trim($key);
 						if(strtoupper($key) == "REGNO")
 							continue;
 						
 						$val = $csvData[$StudentID][$key];
 						
 						if(trim($val)!='') // always use csv data if csv data exist.
	 					{
	 						if ($ArrayWithType == 1)
 								$ary[$TermID][$Type][$key] = $val;
 							else
 								$ary[$TermID][$key] = $val;
	 					}
 						else if(trim($ary[$TermID][$key])=='') 
 						{
 							if(!empty($munsangeDisdata[$key])) // munsung eDis Data 
 							{
 								if ($ArrayWithType == 1)
	 								$ary[$TermID][$Type][$key] = $munsangeDisdata[$key];
	 							else
	 								$ary[$TermID][$key] = $munsangeDisdata[$key];
 							}
		 					else if(!empty($AttendanceData[$StudentID][$key])) // Student Profile Attendance Data
		 					{	
		 						if ($ArrayWithType == 1)
		 							$ary[$TermID][$Type][$key] = $AttendanceData[$StudentID][$key];
		 						else
		 							$ary[$TermID][$key] = $AttendanceData[$StudentID][$key];
		 					}		
 						}
 						
						if ($TermID != 0)
	 					{
	 						if ($ArrayWithType == 1)
	 						{
	 							if(is_numeric($ary[$TermID][$Type][$key]))
		 							$ary[0][$Type][$key] += $ary[$TermID][$Type][$key];
	 						}
	 						else
	 						{
	 							if(is_numeric($ary[$TermID][$key]))
		 							$ary[0][$key] += $ary[$TermID][$key];
	 						}
	 					}
 					}

				}// end type loop
				
			} // end term loop
			
			$returnAry[$StudentID] = $ary;
			
		}// end student loop
		
		
		if ($ReportCardCustomSchoolName == 'chung_hua_ma')
		{
			### Calculate the Annual Conduct Mark and Annual Conduct Grade
			
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			$objAcademicYear = new academic_year($this->schoolYearID);
			$YearTermInfoArr = $objAcademicYear->Get_Term_List($returnAsso=0);
			$YearTermIDArr = Get_Array_By_Key($YearTermInfoArr, 'YearTermID');
			
			foreach((array)$returnAry as $thisStudentID => $thisStudentOtherInfoArr)
			{
				$numOfTerm = 2;
				$ConductTotal = 0;
				for ($i=0; $i<$numOfTerm; $i++)
				{
					$thisYearTermID = $YearTermIDArr[$i];
					$thisConductMark = $thisStudentOtherInfoArr[$thisYearTermID]['Conduct'];
					
					if ($thisConductMark != '' && is_numeric($thisConductMark))
						$ConductTotal += $thisConductMark;
				}
				
				$AnnualConductMark = my_round($ConductTotal / $numOfTerm, 0);
				
				$AnnualConductGrade = '';
				$ConductGradeSchemeArr = $this->AnnualConductSchemeArr;
				foreach((array)$ConductGradeSchemeArr as $thisLowerLimit => $thisGrade)
				{
					if ($AnnualConductMark >= $thisLowerLimit)
					{
						$AnnualConductGrade = $thisGrade;
						break;
					}
				}
				
				$returnAry[$thisStudentID][0]['Conduct'] = $AnnualConductMark;
				$returnAry[$thisStudentID][0]['ConductGrade'] = $AnnualConductGrade;
			}
		}
		return $returnAry;
	}
	

//	function getReportOtherInfoData($ReportID, $StudentID='', $ClassID='', $UploadType='')
//	{
//		global $PATH_WRT_ROOT,$ReportCardCustomSchoolName;
//		
//		//set $UpdateType as Ary
//		if(!empty($UploadType) && !is_array($UploadType))
//			$UploadType = Array($UploadType);
//			
//		//$latestTerm = "";
//		
//		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
//		$ClassLevelID = $ReportSetting['ClassLevelID'];
//		$SemID = $ReportSetting['Semester'];
//		$ReportType = $SemID == "F" ? "W" : "T";
//		
//		$sems = $this->returnReportInvolvedSem($ReportID);
//		if($ReportType == "W")
//			$sems[0] = "Consolidated Term";
//		
//		if ($ClassID=='' && $StudentID=='')
//		{
//			$StudentAry=$this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
//		}
//		else
//		{
//			if($ClassID)		
//				$StudentAry=$this->GET_STUDENT_BY_CLASS($ClassID);	
//			else
//				$StudentAry[] = Array("UserID"=>$StudentID);
//		}
//		
//		# retrieve Student Class
//		$returnAry = Array();
//		//each student loop
//		foreach($StudentAry as $Student)
//		{
//			$StudentID = $Student['UserID'];
//			if($StudentID)
//			{
//				include_once($PATH_WRT_ROOT."includes/libuser.php");
//				$lu = new libuser($StudentID);
//				if(!$ClassID)
//				{
//					$ClassInfo 		= $this->Get_Student_Class_ClassLevel_Info($StudentID);
//					$thisClassID 	= $ClassInfo[0]['ClassID'];
//				}
//				else
//				{
//					$thisClassID = $ClassID;
//				}
//				$WebSamsRegNo 	= $lu->WebSamsRegNo;
//			}
//			
//			$ary = array();
//			$csvType = $this->getOtherInfoType();
//			
//			
//			//each term loop
//			
//			foreach($sems as $TermID => $Term)
//			{
//				//each type loop
//				foreach($csvType as $k => $Type)
//				{
//					if(!empty($UploadType) && !in_array($Type,$UploadType)) 
//						continue;
//					
//					$csvData = $this->getOtherInfoData($Type, $TermID, $thisClassID);	
//					if(!empty($csvData)) 
//					{
//						foreach($csvData as $RegNo=>$data)
//						{
//							if($RegNo == $WebSamsRegNo)
//							{
//			 					foreach($data as $key=>$val)
//			 					{
//				 					### do not overwrite the calculated value for consolidated report
//				 					if ($TermID != 0 || ($TermID == 0 && $ary[$TermID][$key]==''))
//				 					{
//				 						$ary[$TermID][$key] = $val;
//				 						if(!empty($munsangeDisdata[$key])) //replace csv data by edis data 
//				 							$ary[$TermID][$key] = $munsangeDisdata[$key];
//				 					}
//				 					
//				 					if($TermID != 0)
//					 					if(is_numeric($val))
//					 						$ary[0][$key] += $val;
//			 					}
//							}
//						}
//					}
//				}// end type loop
//				
//				//for munsang
//				if($ReportCardCustomSchoolName == "munsang_college")
//				{
//					$munsangeDisData = $this->Get_eDiscipline_Data($TermID, $StudentID);
//					foreach ((array)$munsangeDisData as $ConductField => $thisGrade)
//						$ary[$TermID][$ConductField] = $thisGrade;
//				}
//				
//			} // end term loop
//			
//			$returnAry[$StudentID] = $ary;
//			
//		}// end student loop
//		
//		return $returnAry;
//	}
	

	//modified by Marcus 20/8/2009 , Change parameter from ClassName to ClassID
	# Get the data store in CSV file, specifiy by Type, Term and Class
	//20111121
	function getOtherInfoData_old($UploadType, $Term, $ClassID, $ClassLevelID='') {
		global $intranet_root, $PATH_WRT_ROOT;
		
		# Verify if $UploadType is valid
		$configFilesType = $this->getOtherInfoType();
		if (!in_array($UploadType, $configFilesType)) {
			return false;
		}
		
		# old csv processing
//		include_once("libclass.php");
//		$libClass = new libclass();
//		$ClassName = $libClass->getClassName($ClassID);
		
		if(empty($this->TmpBatchArr['getOtherInfoData']['ClassObj'][$ClassID]))
		{
			include_once("libclass.php");
			$this->TmpBatchArr['getOtherInfoData']['ClassObj'][$ClassID] = new Year_Class($ClassID,false,false,false);
		}
		$ClassName = $this->TmpBatchArr['getOtherInfoData']['ClassObj'][$ClassID]->ClassTitleEN;
		
		$ClassName = str_replace(' ', '', $ClassName);
		$ClassName = str_replace('_', '', $ClassName); 
		$dataFilesPath = $this->dataFilesPath.$UploadType."/";
		
		$ClassLevelID = $ClassLevelID? $ClassLevelID : $this->Get_ClassLevel_By_ClassID($ClassID); 
		
		$fileName = $dataFilesPath.($this->schoolYear)."_".$Term."_".$ClassName."_".$ClassID."_unicode.csv";

		# Cater new csv file name format (each Form Format)
		if(!file_exists($fileName))
		{
			$fileName = $dataFilesPath.$Term."_".$ClassLevelID."_0_unicode.csv";
			$FormFormat = true;
		}

		# Cater new csv file name format (each Class Format)
		if(!file_exists($fileName))
			$fileName = $dataFilesPath.$Term."_".$ClassLevelID."_".$ClassID."_unicode.csv";

		if (file_exists($fileName)) {
			$returnArr = array();
			
			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			$limport = new libimporttext();
			$data = $limport->GET_IMPORT_TXT($fileName, 0, '<br />');
			
			
			// remove the header
			$header = array_shift($data);
			
			// remove empty item (line)
			$data = array_filter($data);
			
			if($header[0]=='REGNO') # old csv format
			{
				$StudentList = $this->GET_STUDENT_BY_CLASSLEVEL('', $ClassLevelID);
				$WebSAMSUserIDAssoc = BuildMultiKeyAssoc($StudentList,"WebSAMSRegNo","UserID",1);
				
				// Loop and build the associate array
				for($i=0; $i<sizeof($data); $i++) {
					// The first column is WebSAMSRegNo

					$WebSAMSRegNo = $data[$i][0];
					$thisUserID = $WebSAMSUserIDAssoc[$WebSAMSRegNo];
					for($j=1; $j<sizeof($header); $j++) {
						$returnArr[$thisUserID][$j] = $data[$i][$j];
						#$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] = $data[$i][$j];
						// if same REGNO have more than one entry, use an array to store all entries, if not, just store the one entry directly
						if (isset($returnArr[$thisUserID][$header[$j]])) {
							if (!is_array($returnArr[$thisUserID][$header[$j]])) {
								$tmpValue = $returnArr[$thisUserID][$header[$j]];
								$returnArr[$thisUserID][$header[$j]] = array();
								$returnArr[$thisUserID][$header[$j]][] = $tmpValue;
							}
							$returnArr[$thisUserID][$header[$j]][] = $data[$i][$j];
						} else {
							$returnArr[$thisUserID][$header[$j]] = $data[$i][$j];
						}
					}
				}
			}	
			else  # new csv format
			{
				# Get a list of WebSAMSRegNo of all student in the class for validating REGNO
				if ($FormFormat)
					$StudentList = $this->GET_STUDENT_BY_CLASSLEVEL('', $ClassLevelID);
				else
					$StudentList = $this->GET_STUDENT_BY_CLASS($ClassID);
				
				# $ClassNameNumberAssoc[$ClassName][$ClassNumber] = $UserID
				$ClassNameNumberAssoc = BuildMultiKeyAssoc($StudentList,array("ClassTitleEn","ClassNumber"),"UserID",1);
				
				# $UserLoginAssoc[$UserLogin] = $UserID
				$UserLoginAssoc = BuildMultiKeyAssoc($StudentList,array("UserLogin"),"UserID",1);
				
				# $UserLoginAssoc[$WebSAMSRegNo] = $UserID
				$WebSAMSRegNoAssoc = BuildMultiKeyAssoc($StudentList,array("WebSAMSRegNo"),"UserID",1);
				
				if ($header[0] == 'Class Name' && $header[1] == 'Class Number' && $header[2] == 'User Login' && $header[3] == 'WebSAMSRegNo') {
					// Loop and build the associate array
					for($i=1; $i<sizeof($data); $i++) {
						// get student data 
						list($ClassName, $ClassNumber,$UserLogin, $WebSAMSRegNo) = $data[$i];
						
						if($ClassName&&$ClassNumber)
							$thisUserID = $ClassNameNumberAssoc[$ClassName][$ClassNumber];
						else if($UserLogin)
							$thisUserID = $UserLoginAssoc[$UserLogin];
						else
							$thisUserID = $WebSAMSRegNoAssoc[$WebSAMSRegNo];
						
						for($j=5; $j<sizeof($header); $j++) {
							// if same REGNO have more than one entry, use an array to store all entries, if not, just store the one entry directly
							if (isset($returnArr[$thisUserID][$header[$j]])) {
								if (!is_array($returnArr[$thisUserID][$header[$j]])) {
									$tmpValue = $returnArr[$thisUserID][$header[$j]];
									$returnArr[$thisUserID][$header[$j]] = array();
									$returnArr[$thisUserID][$header[$j]][] = $tmpValue;
								}
								$returnArr[$thisUserID][$header[$j]][] = $data[$i][$j];
							} else {
								$returnArr[$thisUserID][$header[$j]] = $data[$i][$j];
							}
						}
					}
				}
				else {
					### Form-based csv info
					for($i=1; $i<sizeof($data); $i++) {
						for($j=0; $j<sizeof($header)-1; $j++) {
							// if same REGNO have more than one entry, use an array to store all entries, if not, just store the one entry directly
							$returnArr[$header[$j]] = $data[$i][$j];
						}
					}
				}
			}
			debug_pr($returnArr);
			return $returnArr;
		}
		else {
			return false;
		}

	}
	
	//20111121
	function getOtherInfoData($UploadType, $Term, $ClassID, $ClassLevelID='') {
		global $intranet_root, $PATH_WRT_ROOT;
		
		# Verify if $UploadType is valid
		$configFilesType = $this->getOtherInfoType();
		if (!in_array($UploadType, $configFilesType)) {
			return false;
		}
		
		$ClassLevelID = $ClassLevelID? $ClassLevelID : $this->Get_ClassLevel_By_ClassID($ClassID);
		 
		$StudentList = $this->GET_STUDENT_BY_CLASSLEVEL('', $ClassLevelID);
		
		$Data = $this->Get_Student_OtherInfo_Data($UploadType, $Term, $ClassID, NULL, NULL, $ClassLevelID);
		
		$returnArr = array();
		
		$SizeOfRec = sizeof($Data);
		for($i=0; $i<$SizeOfRec; $i++)
		{
			$varArr = explode("\n", $Data[$i]['Information']);
			if(count($varArr)>1)
				$returnArr[$Data[$i]['StudentID']][$Data[$i]['ItemCode']][] = $varArr;
			else
				$returnArr[$Data[$i]['StudentID']][$Data[$i]['ItemCode']] = $varArr[0];
		}
		
		return $returnArr;
	}
	
//	//modified by Marcus 20/8/2009 , Change parameter from ClassName to ClassID
//	# Get the data store in CSV file, specifiy by Type, Term and Class
//	function getOtherInfoData($UploadType, $Term, $ClassID) {
//		global $intranet_root, $PATH_WRT_ROOT;
//		
//		# Verify if $UploadType is valid
//		$configFilesType = $this->getOtherInfoType();
//		if (!in_array($UploadType, $configFilesType)) {
//			return false;
//		}
//		
//		include_once("libclass.php");
//		$libClass = new libclass();
//		$ClassName = $libClass->getClassName($ClassID);
//		
//		$ClassName = str_replace(' ', '', $ClassName);
//		$ClassName = str_replace('_', '', $ClassName); 
//		$dataFilesPath = $this->dataFilesPath.$UploadType."/";
//		
//		$fileName = $dataFilesPath.($this->schoolYear)."_".$Term."_".$ClassName."_".$ClassID."_unicode.csv";
//		if (file_exists($fileName)) {
//			$returnArr = array();
//			// Get the EnglishTitle from config file for creating associated array
//			$config = $this->getOtherInfoConfig($UploadType);
//			
//			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
//			$limport = new libimporttext();
//			$data = $limport->GET_IMPORT_TXT($fileName, 0, '<br />');
//			
//			// remove the header
//			$header = array_shift($data);
//			// remove empty item (line)
//			$data = array_filter($data);
//			// Loop and build the associate array
//			for($i=0; $i<sizeof($data); $i++) {
//				// The first column is WebSAMSRegNo
//				$WebSAMSRegNo = $data[$i][0];
//				for($j=1; $j<sizeof($header); $j++) {
//					$returnArr[$data[$i][0]][$j] = $data[$i][$j];
//					#$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] = $data[$i][$j];
//					// if same REGNO have more than one entry, use an array to store all entries, if not, just store the one entry directly
//					if (isset($returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]])) {
//						if (!is_array($returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]])) {
//							$tmpValue = $returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]];
//							$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] = array();
//							$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]][] = $tmpValue;
//						}
//						$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]][] = $data[$i][$j];
//					} else {
//						$returnArr[$data[$i][0]][$config[$j]["EnglishTitle"]] = $data[$i][$j];
//					}
//				}
//			}
//			return $returnArr;
//		} else {
//			return false;
//		}
//	}
	
	################ Marksheet Revision ################
	
	# Get Subject Name
	function GET_SUBJECT_NAME($ParSubjectID, $ShortName=0, $Bilingual=1) {
		global $intranet_session_language;
		
		$fields = "";
		if ($ShortName)
			$fields = " a.EN_SNAME, a.CH_SNAME, b.EN_SNAME, b.CH_SNAME ";
		else
			$fields = " a.EN_DES, a.CH_DES, b.EN_DES, b.CH_DES ";
		
		$sql = "SELECT
						$fields
				FROM
						ASSESSMENT_SUBJECT as a
						LEFT JOIN ASSESSMENT_SUBJECT as b 
						ON a.CODEID = b.CODEID AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
				WHERE
						a.RecordID = '$ParSubjectID'
					AND
						a.RecordStatus = 1
					AND 
						b.RecordStatus = 1
				";
		$row = $this->returnArray($sql, 4);
		
		list($SubjectEngName, $SubjectChiName, $ParentSubjectEngName, $ParentSubjectChiName) = $row[0];
		
		if ($SubjectEngName==$ParentSubjectEngName)
		{
			if ($Bilingual==1)
				$SubjectName = $SubjectEngName."&nbsp;".$SubjectChiName;
			else
			{
				if ($intranet_session_language=='en')
					$SubjectName = $SubjectEngName;
				else
					$SubjectName = $SubjectChiName;
			}
		}
		else
		{
			if ($Bilingual==1)
				$SubjectName = $ParentSubjectEngName." - ".$SubjectEngName."&nbsp;".$ParentSubjectChiName." - ".$SubjectChiName;
			else
			{
				if ($intranet_session_language=='en')
					$SubjectName = $ParentSubjectEngName." - ".$SubjectEngName;
				else
					$SubjectName = $ParentSubjectChiName." - ".$SubjectChiName;
			}
		}
		
		//$SubjectName = ($SubjectEngName==$ParentSubjectEngName) ? $SubjectEngName."&nbsp;".$SubjectChiName : $ParentSubjectEngName." - ".$SubjectEngName."&nbsp;".$ParentSubjectChiName." - ".$SubjectChiName;
		return $SubjectName;
	}
	
	function Load_Subject_Name()
	{
		if(empty($this->Batch_Subject_Name))
		{
			$sql = "
				SELECT
					EN_ABBR,CH_ABBR,EN_SNAME,CH_SNAME,EN_DES,CH_DES, RecordID
				FROM
					ASSESSMENT_SUBJECT
				WHERE
					RecordStatus = 1
				";
			$this->Batch_Subject_Name = BuildMultiKeyAssoc($this->returnArray($sql), "RecordID");
		}
	}
	
	# input: Lang = "EN", "CH"
	# modified by marcus 20091203 - add par $ShortName to select short name
	function GET_SUBJECT_NAME_LANG($SubjectID, $ParLang='', $ParShortName=0, $ParAbbrName=0) 
	{
		global $intranet_session_language;
		
		if(!$ParLang)
			$ParLang = ($intranet_session_language=="en") ? "EN" : "CH";
		else
		{
			if ($ParLang == 'en')
				$ParLang = 'EN';
			else if ($ParLang == 'b5' || $ParLang == 'ch')
				$ParLang = 'CH';
		}
		
		if ($ParAbbrName==1)
			$select = $ParLang."_ABBR";
		else if ($ParShortName==1)
			$select = $ParLang."_SNAME";
		else
			$select = $ParLang."_DES";
		$select = strtoupper($select);	
		
		$this->Load_Subject_Name();	
		return $this->Batch_Subject_Name[$SubjectID][$select];	
			
//		$sql = "SELECT
//					{$select}
//				FROM
//					ASSESSMENT_SUBJECT
//				WHERE
//					RecordID = '$SubjectID'
//					AND
//					RecordStatus = 1
//				";
//		$row = $this->returnVector($sql);
//		return $row[0];
	}
	
	# Get the marksheet score for s group of student
	function GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnID, $ConvertToNaIfZeroWeight=0, $ColumnWeight='', $SkipSubjectGroupCheckingForGetMarkSheet=0) 
	{
		$PreloadArrKey = 'GET_MARKSHEET_SCORE';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		global $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$checkStudnetInSG = false;
		if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup'] && $SkipSubjectGroupCheckingForGetMarkSheet!=1)
		{
			$checkStudnetInSG = true;
			$ReportID = $this->returnReportIDByReportColumnID($ReportColumnID);
			$SubjectGroupStudentIDArr = $this->Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $SubjectID);
		}
		
		$IsZeroWeightColumn = false;
		if ($ConvertToNaIfZeroWeight == 1)
		{
			if ($ColumnWeight == 0)
				$IsZeroWeightColumn = true;
		}
		
		
		$table = $this->DBName.".RC_MARKSHEET_SCORE";
		$fields  = "MarksheetScoreID, StudentID, SchemeID, MarkType, MarkRaw, ";
		$fields .= "MarkNonNum, LastModifiedUserID, IsEstimated ";
		$students = implode(",", $StudentIDArr);
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE StudentID IN ($students) AND SubjectID = '$SubjectID' AND ";
		$sql .= "ReportColumnID = '$ReportColumnID'";
		//$sql .= $conds_SubjectGroupStudent;
		
		$result = $this->returnArray($sql);
		
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) 
		{
			$thisStudentID = $result[$i]["StudentID"];
			
			$thisStudentNotInSG = false;
			if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']==true && $checkStudnetInSG==true && in_array($thisStudentID, $SubjectGroupStudentIDArr)==false)
				$thisStudentNotInSG = true;

			if ($thisStudentNotInSG || $IsZeroWeightColumn)
			{
				$returnArr[$thisStudentID] = 
				array(
					"MarksheetScoreID" =>  $result[$i]["MarksheetScoreID"],
					"SchemeID" =>  $result[$i]["SchemeID"],
					"MarkType" =>  'SC',
					"MarkRaw" =>  '-1',
					"MarkNonNum" =>  'N.A.',
					"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
					"IsEstimated" =>  '0',
				);
			}
			else
			{
				$returnArr[$thisStudentID] = 
				array(
					"MarksheetScoreID" =>  $result[$i]["MarksheetScoreID"],
					"SchemeID" =>  $result[$i]["SchemeID"],
					"MarkType" =>  $result[$i]["MarkType"],
					"MarkRaw" =>  $result[$i]["MarkRaw"],
					"MarkNonNum" =>  $result[$i]["MarkNonNum"],
					"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
					"IsEstimated" =>  $result[$i]["IsEstimated"],
				);
			}
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	# Get the marksheet score for group of student
	function GET_MARKSHEET_COLUMN_SCORE($StudentID, $ReportColumnID, $SubjectID='') {
		$PreloadArrKey = 'GET_MARKSHEET_COLUMN_SCORE';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		$table = $this->DBName.".RC_MARKSHEET_SCORE";
		$fields  = "MarksheetScoreID, SubjectID, StudentID, SchemeID, ";
		$fields .= "MarkType, MarkRaw, MarkNonNum, LastModifiedUserID, DateModified, IsEstimated ";
		if (is_array($StudentID)) {
			$students = implode(",", $StudentID);
			$students = "IN ($students)";
		} else {
			$students = "= $StudentID";
		}
		
		$subject_conds = '';
		if ($SubjectID != '')
			$subject_conds = " And SubjectID = '$SubjectID' ";
			
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE StudentID $students AND ";
		$sql .= "ReportColumnID = '$ReportColumnID'";
		$sql .= $subject_conds;
		
		$result = $this->returnArray($sql);
		$returnArr = array();
		
		for($i=0; $i<sizeof($result); $i++) {
			if ($SubjectID == '')
			{
				$returnArr[$result[$i]["StudentID"]][$result[$i]["SubjectID"]] = 
					array(
						"MarksheetScoreID" =>  $result[$i]["MarksheetScoreID"],
						"SchemeID" =>  $result[$i]["SchemeID"],
						"MarkType" =>  $result[$i]["MarkType"],
						"MarkRaw" =>  $result[$i]["MarkRaw"],
						"MarkNonNum" =>  $result[$i]["MarkNonNum"],
						"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
						"DateModified" =>  $result[$i]["DateModified"],
						"IsEstimated" =>  $result[$i]["IsEstimated"]
					);
			}
			else
			{
				$returnArr[$result[$i]["StudentID"]] = 
					array(
						"MarksheetScoreID" =>  $result[$i]["MarksheetScoreID"],
						"SchemeID" =>  $result[$i]["SchemeID"],
						"MarkType" =>  $result[$i]["MarkType"],
						"MarkRaw" =>  $result[$i]["MarkRaw"],
						"MarkNonNum" =>  $result[$i]["MarkNonNum"],
						"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
						"DateModified" =>  $result[$i]["DateModified"],
						"IsEstimated" =>  $result[$i]["IsEstimated"]
					);
			}
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	# Get the marksheet score for group of student forconsolidate report
	function GET_CONSOLIDATE_REPORT_COLUMN_SCORE($StudentIDArr, $ReportColumnID)
	{
		$PreloadArrKey = 'GET_CONSOLIDATE_REPORT_COLUMN_SCORE';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$fields  = "ReportResultScoreID, SubjectID, StudentID,  ";
		$fields .= " Mark, RawMark, Grade ";
		if (is_array($StudentIDArr)) {
			$students = implode(",", $StudentIDArr);
			$students = "IN ($students)";
		} else {
			$students = "= $StudentID";
		}
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE StudentID $students AND ";
		$sql .= "ReportColumnID = '$ReportColumnID'";

		$result = $this->returnArray($sql);
		$returnArr = array();
		
		for($i=0; $i<sizeof($result); $i++) {
			$returnArr[$result[$i]["StudentID"]][$result[$i]["SubjectID"]] = 
				array(
					"ReportResultScoreID" =>  $result[$i]["ReportResultScoreID"],
					"Mark" =>  $result[$i]["Mark"],
					"RawMark" =>  $result[$i]["RawMark"],
					"Grade" =>  $result[$i]["Grade"],
					"DateModified" =>  $result[$i]["DateModified"]
				);
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	# INSERT marksheet scores without raw mark record
	function INSERT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnID, $LastModifiedUserID="") {
		if ($LastModifiedUserID == "")
			$LastModifiedUserID = $this->uid;
		$existStudentID = array_keys($this->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnID));
		$StudentIDArr = array_diff($StudentIDArr, $existStudentID);
		sort($StudentIDArr);
		if (sizeof($StudentIDArr) > 0) {
			$table = $this->DBName.".RC_MARKSHEET_SCORE";
			$fields = "StudentID, SubjectID, ReportColumnID, LastModifiedUserID, DateInput, DateModified";
			$sql = "INSERT INTO $table ($fields) VALUES ";
			for($i=0; $i<sizeof($StudentIDArr); $i++) {
				$entries[] = "(".$StudentIDArr[$i].", '$SubjectID', '$ReportColumnID',  
								'$LastModifiedUserID', NOW(), NOW())";
			}
			$sql .= implode(",", $entries);
			$success = $this->db_db_query($sql);
			return $success;
		} else {
			return true;
		}
	}
	
	# UPDATE marksheet scores
	function UPDATE_MARKSHEET_SCORE($MarksheetScoreArr) {
		$table = $this->DBName.".RC_MARKSHEET_SCORE";
		$sqlUpdate = "UPDATE $table SET ";
		for($i=0; $i<sizeof($MarksheetScoreArr); $i++) {
			$sql = "SchemeID = '".$MarksheetScoreArr[$i]["SchemeID"]."', ";
			$sql .= "MarkType = '".$MarksheetScoreArr[$i]["MarkType"]."', ";
			$sql .= "MarkRaw = '".$MarksheetScoreArr[$i]["MarkRaw"]."', ";
			$sql .= "MarkNonNum = '".$MarksheetScoreArr[$i]["MarkNonNum"]."', ";
			$sql .= "IsEstimated = '".$MarksheetScoreArr[$i]["IsEstimated"]."', ";
			$sql .= "LastModifiedUserID = '".$MarksheetScoreArr[$i]["LastModifiedUserID"]."', ";
			$sql .= "DateModified = NOW() ";
			$sql .= "WHERE ";
			if (isset($MarksheetScoreArr[$i]["MarksheetScoreID"]) &&  $MarksheetScoreArr[$i]["MarksheetScoreID"] != "") {
				$sql .= "MarksheetScoreID = '".$MarksheetScoreArr[$i]["MarksheetScoreID"]."'";
			} else {
				$sql .= "StudentID = '".$MarksheetScoreArr[$i]["StudentID"]."' AND ";
				$sql .= "SubjectID = '".$MarksheetScoreArr[$i]["SubjectID"]."' AND ";
				$sql .= "ReportColumnID = '".$MarksheetScoreArr[$i]["ReportColumnID"]."'";
			}
			$sql = $sqlUpdate.$sql;
			
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
	}
	
	# DELETE marksheet scores specify by the ReportColumnID
	# This should only be executed when deleteing a Report Template or other delete operations which required cscading data
	function DELETE_MARKSHEET_SCORE($ReportColumnID, $StudentID='', $SubjectID = '') {
		$cond_StudentID = trim($StudentID)==''?"":" AND StudentID = '$StudentID' ";
		$cond_SubjectID = trim($SubjectID)==''?"":" AND SubjectID = '$SubjectID' ";
		
		$table = $this->DBName.".RC_MARKSHEET_SCORE";
		if (is_array($ReportColumnID)) {
			$ReportColumnIDList = implode(",", $ReportColumnID);
			$sql = "DELETE FROM $table WHERE ReportColumnID IN ($ReportColumnIDList)";
		} else {
			$sql = "DELETE FROM $table WHERE ReportColumnID = $ReportColumnID";
		}
		$sql .= $cond_StudentID;
		$sql .= $cond_SubjectID;
		
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	# Check Marksheet Status 
	# Status Mode: 1-Edit, 0-View
	# $ParCmpSubjectIDArr can be used for checking Subject which contains component subjects	
	function CHECK_MARKSHEET_STATUS($ParSubjectID, $ParClassLevelID, $ParReportID, $ParCmpSubjectIDArr=array()){
		$returnVal = 1;				// default as Edit Mode
		$hasDirectInputMark = 1;	// default as Edit Status
		$ColumnHasTemplateAry = array();
		
		if($ParSubjectID != "" && $ParClassLevelID != "" && $ParReportID != ""){
			# Get Grading Scheme Info of Parent / Common Subject
			$CommonSubjectFormGradingArr = array();
			# Modified by Marcus 20101011 (for template based grading scheme)
			$CommonSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ParClassLevelID, $ParSubjectID, 0, 0, $ParReportID);
			if(count($CommonSubjectFormGradingArr) > 0){
				$SchemeID = $CommonSubjectFormGradingArr['SchemeID'];
				$ScaleInput = $CommonSubjectFormGradingArr['ScaleInput'];
				$ScaleDisplay = $CommonSubjectFormGradingArr['ScaleDisplay'];
				
				$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$SchemeType = $SchemeMainInfo['SchemeType'];
			}
			
			# Check Whether the Scale Input of parent subject is Grade(G) / PassFail(PF) or not
			if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
				$hasDirectInputMark = 1;
			}
			else {
				# Case for The Scale Input of parent subject is Mark(M)
				$basic_data = $this->returnReportTemplateBasicInfo($ParReportID);			// Basic  Data
				$column_data = $this->returnReportTemplateColumnData($ParReportID); 		// Column Data
				$ReportType = (count($basic_data) > 0) ?  $basic_data['Semester'] : "";
				
				if(count($column_data) > 0){	
					for($i=0 ; $i<sizeof($column_data) ; $i++){
						$ReportColumnID = $column_data[$i]['ReportColumnID'];
						$SemesterNum = $column_data[$i]['SemesterNum'];
						
						# Whole Year Report Template use
						if($ReportType == "F"){
							# Case 1a: Check for any term report template is defined for the use of whole year report
							$ColumnHasTemplateAry[$ReportColumnID] = $this->CHECK_REPORT_TEMPLATE_FROM_COLUMN($SemesterNum, $ParClassLevelID);
	
							if($ColumnHasTemplateAry[$ReportColumnID]){
								$hasDirectInputMark = 0;
							} else if(!$ColumnHasTemplateAry[$ReportColumnID]){
								$hasDirectInputMark = 1;
								continue;
							}
							
							# Case 1b: When Terms in Whole Year report have not defined, check whether the component subjects can calculate mark or not 
						}
					}
					
					# Check for Any subject having component subjects &
					# Case 2: if all ScaleInput of Component Subjects are "G", allow to edit ($hasDirectInputMark = 1)
					# Case 3: if anyone ScaleInput of Component Subjects is "M", do not allow to edit ($hasDirectInputMark = 1)
					if(!empty($ParCmpSubjectIDArr)){
						$ScaleInputArr = array();
						for($j=0 ; $j<count($ParCmpSubjectIDArr) ; $j++){
							$SubjectFormGradingArr = array();
							# Modified by Marcus 20101011 (for template based grading scheme)
							$SubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ParClassLevelID, $ParCmpSubjectIDArr[$j], 0, 0, $ParReportID);
							
							if(count($SubjectFormGradingArr) > 0){
								$ScaleInputArr[] = $SubjectFormGradingArr['ScaleInput'];
							}
						}
						
						if(count($ScaleInputArr) > 0){
							if(in_array("M", $ScaleInputArr)){	// Set to "0" as at least one ScaleInput of Component Subject is Mark(M)
								$hasDirectInputMark = 0;
							}
							else {			// Set to "1" as no any ScaleInput of Component Subject is Mark(M)
								$hasDirectInputMark = 1;
							}
						}
					}
				}
			}
		}
		
		if(!$hasDirectInputMark)
			$returnVal = 0;
			
		return $returnVal;
	}
	
	# Get Last Modified Info of Parent Subject having Component Subjects
	function GET_CLASS_PARENT_MARKSHEET_LAST_MODIFIED($ParentSubjectID, $CmpSubjectIDArr, $ReportColumnIDList, $ClassID){
		$returnArr = array();
		
		if($ParentSubjectID != "" && count($CmpSubjectIDArr) > 0){
			$ParentLastModifiedArr = array();
			$ParentLastModifiedArr = $this->GET_CLASS_MARKSHEET_LAST_MODIFIED($ParentSubjectID, $ReportColumnIDList);
			
			$CmpLastModifiedArr = array();
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				$CmpLastModifiedArr[$CmpSubjectIDArr[$i]] = $this->GET_CLASS_MARKSHEET_LAST_MODIFIED($CmpSubjectIDArr[$i], $ReportColumnIDList);
			}
			
			
			# Compare the Last Modified Time among Component Subjects
			$tmpLastModifiedTime = array();
			$CmpLastModifiedTime = '';
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				if(isset($CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID])){
					$tmpTime = $CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID][0];
					if($i != 0 && strtotime($CmpLastModifiedTime) - strtotime($tmpTime) < 0){
						$CmpLastModifiedTime = $tmpTime;
						$tmpLastModifiedTime = $CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID];
					} 
					if($i == 0){
						$CmpLastModifiedTime = $tmpTime;
						$tmpLastModifiedTime = $CmpLastModifiedArr[$CmpSubjectIDArr[$i]][$ClassID];
					}
				}
			}
			
			if(isset($ParentLastModifiedArr[$ClassID]) && count($ParentLastModifiedArr[$ClassID]) > 0){
				$returnArr[$ClassID] = $ParentLastModifiedArr[$ClassID];
				$LastModifiedTime = $ParentLastModifiedArr[$ClassID][0];
					
				# Compare the Last Modified Time between Parent Subject & the lated Modified Time of Component Subject
				if($CmpLastModifiedTime != ""){
					if(strtotime($CmpLastModifiedTime) - strtotime($LastModifiedTime) > 0){
						$returnArr[$ClassID] = $tmpLastModifiedTime;
					}
				}
			} else if(isset($tmpLastModifiedTime)){
				$returnArr[$ClassID] = $tmpLastModifiedTime;
			}
		}
		
		return $returnArr;
	}
	
	function GET_SUBJECT_GROUP_PARENT_MARKSHEET_LAST_MODIFIED($ParentSubjectID, $CmpSubjectIDArr, $ReportColumnIDList, $SubjectGroupID, $YearTermID){
		$returnArr = array();
		
		if($ParentSubjectID != "" && count($CmpSubjectIDArr) > 0){
			$ParentLastModifiedArr = array();
			$ParentLastModifiedArr = $this->GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED($ParentSubjectID, $ReportColumnIDList, '', $YearTermID);
			
			$CmpLastModifiedArr = array();
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				// 2011-0217-1452-19069
				//$CmpLastModifiedArr[$CmpSubjectIDArr[$i]] = $this->GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED($CmpSubjectIDArr[$i], $ReportColumnIDList, '', $YearTermID);
				$CmpLastModifiedArr[$CmpSubjectIDArr[$i]] = $this->GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED($CmpSubjectIDArr[$i], $ReportColumnIDList, $SubjectGroupID, $YearTermID, $ParentSubjectID);
			}
			
			
			# Compare the Last Modified Time among Component Subjects
			$tmpLastModifiedTime = array();
			$CmpLastModifiedTime = '';
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				$thisCmpSubjectID = $CmpSubjectIDArr[$i];
				
				/*
				if(isset($CmpLastModifiedArr[$thisCmpSubjectID][$SubjectGroupID])){
					$tmpTime = $CmpLastModifiedArr[$thisCmpSubjectID][$SubjectGroupID][0];
					if($i != 0 && strtotime($CmpLastModifiedTime) - strtotime($tmpTime) < 0){
						$CmpLastModifiedTime = $tmpTime;
						$tmpLastModifiedTime = $CmpLastModifiedArr[$thisCmpSubjectID][$SubjectGroupID];
					} 
					if($i == 0){
						$CmpLastModifiedTime = $tmpTime;
						$tmpLastModifiedTime = $CmpLastModifiedArr[$thisCmpSubjectID][$SubjectGroupID];
					}
				}
				*/
				
				if (is_array($CmpLastModifiedArr[$thisCmpSubjectID]))
				{
					foreach ($CmpLastModifiedArr[$thisCmpSubjectID] as $thisSubjectGroupID => $thisLastModifiedInfoArr)
					{
						$tmpTime = $CmpLastModifiedArr[$thisCmpSubjectID][$thisSubjectGroupID][0];
						if($i != 0 && strtotime($CmpLastModifiedTime) - strtotime($tmpTime) < 0){
							$CmpLastModifiedTime = $tmpTime;
							$tmpLastModifiedTime = $CmpLastModifiedArr[$thisCmpSubjectID][$thisSubjectGroupID];
						} 
						if($i == 0){
							$CmpLastModifiedTime = $tmpTime;
							$tmpLastModifiedTime = $CmpLastModifiedArr[$thisCmpSubjectID][$thisSubjectGroupID];
						}
					}
				}
			}
			
			if(isset($ParentLastModifiedArr[$SubjectGroupID]) && count($ParentLastModifiedArr[$SubjectGroupID]) > 0){
				$returnArr[$SubjectGroupID] = $ParentLastModifiedArr[$SubjectGroupID];
				$LastModifiedTime = $ParentLastModifiedArr[$SubjectGroupID][0];
					
				# Compare the Last Modified Time between Parent Subject & the lated Modified Time of Component Subject
				if($CmpLastModifiedTime != ""){
					if(strtotime($CmpLastModifiedTime) - strtotime($LastModifiedTime) > 0){
						$returnArr[$SubjectGroupID] = $tmpLastModifiedTime;
					}
				}
			} else if(isset($tmpLastModifiedTime)){
				$returnArr[$SubjectGroupID] = $tmpLastModifiedTime;
			}
		}
		
		return $returnArr;
	}
	
	
	# Get Report Student Selection
	function GET_REPORT_STUDENT_SELECTION($ParClassID)
	{
		include_once("form_class_manage_ui.php");
		$lib_formClassUI = new form_class_manage_ui();
		$ReturnValue = $lib_formClassUI->Get_Student_Selection('TargetStudentID[]', $ParClassID, '', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0);

		return $ReturnValue;
	}
	
	
	# Compare current time with the Submission OR Verification Period of the ReportCard
	# $ParPeriod = "Submission" or "Verification"
	# Meaning of return values:
	#	"-1" - Period not set yet (NULL)
	#	"0" - before the Period
	#	"1" - within  the Period
	#	"2" - after the Period
	function COMPARE_REPORT_PERIOD($ReportID, $ParPeriod, $ParStartTS='', $ParEndTS='') {
		
		if ($ParStartTS=='' && $ParEndTS=='')
		{
			$table = $this->DBName.".RC_REPORT_TEMPLATE";
			$start = "Marksheet".$ParPeriod."Start";
			$end = "Marksheet".$ParPeriod."End";
			$sql = "SELECT UNIX_TIMESTAMP($start) AS StartTs, UNIX_TIMESTAMP($end) AS EndTs FROM $table WHERE ReportID = '$ReportID'";
			$result = $this->returnArray($sql);
			
			$result[0]["StartTs"] == "" ? $startTs = "" : $startTs = $result[0]["StartTs"];
			$result[0]["EndTs"] == "" ? $endTs = "" : $endTs = $result[0]["EndTs"];
		}
		else
		{
			$startTs = $ParStartTS;
			$endTs = $ParEndTS;
		}
		
		# Add on more day the End timestamp since the day end at 23:59
		$endTs = $endTs + 24*60*60;
		$currentTs = time();
		# not set yet
		if ($startTs == "" || $endTs == "") {
			return -1;
		}
		# before
		if ($currentTs < $startTs) {
			return 0;
		}
		# within
		if ($currentTs >= $startTs && $currentTs < $endTs) {
			return 1;
		}
		# after
		if ($currentTs >= $endTs) {
			return 2;
		}
	}
	
	function Insert_Teacher_Comment($DataArr) {
		if (!is_array($DataArr) || count($DataArr) == 0) {
			return false;
		}
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value) {
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# TeacherID
		$fieldArr[] = 'TeacherID';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		$RC_MARKSHEET_COMMENT = $this->DBName.".RC_MARKSHEET_COMMENT";
		$sql = "Insert Into $RC_MARKSHEET_COMMENT ($fieldText) Values ($valueText)";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Update_Teacher_Comment($CommentID, $DataArr) {
		if (!is_array($DataArr) || count($DataArr) == 0) {
			return false;
		}
		
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $field => $value) {
			$valueFieldArr[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
		}
		$valueFieldArr[] = " DateModified = now() ";
		
		$valueFieldText = implode(',', $valueFieldArr);
		
		$RC_MARKSHEET_COMMENT = $this->DBName.".RC_MARKSHEET_COMMENT";
		$sql = "Update $RC_MARKSHEET_COMMENT Set $valueFieldText Where CommentID = '$CommentID'";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	# Get a list of JavaScript Array for returning to the query page for auto complete
	function GET_COMMENT_BANK_SUGGEST($SubjectID="", $ArrayOnly=FALSE, $IsAdditional=0) {
		global $PATH_WRT_ROOT;
		
		// escape the keyword string
		$patterns = array('/\s+/', '/"+/', '/%+/');
		$replace = array('');
		$keyword = preg_replace($patterns, $replace, $keyword);
		// build the SQL query that gets the matching functions from the database
		$table = $this->DBName.".RC_COMMENT_BANK";
		$sql = "SELECT CommentCode, Comment, CommentEng FROM $table ";
		if ($SubjectID == "")
			$sql .=	"WHERE (SubjectID IS NULL || SubjectID = '') And IsAdditional = '".$IsAdditional."' ";
		else
		{
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$objSubject = new subject($SubjectID);
			if ($objSubject->Is_Component_Subject())
			{
				$objSubjectComponent = new Subject_Component($SubjectID);
				$ParentInfoArr = $objSubjectComponent->Get_Parent_Subject();
				$TargetSubjectID = $ParentInfoArr['RecordID'];
			}
			else
			{
				$TargetSubjectID = $SubjectID;
			}
		
			$sql .=	"WHERE SubjectID = '$TargetSubjectID'";
		}
		$sql .=	"ORDER BY CommentCode";
		
		// execute the SQL query
		$result = $this->returnArray($sql);
		
		if ($ArrayOnly)	return $result;
		
		// build the JS array
		// if we have results, loop through them and add them to the output
		$output = "";
		if(sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$comment = preg_replace('/\r\n|\r/', "<br>", $result[$i]['Comment']);
				$commentEng = preg_replace('/\r\n|\r/', "<br>", $result[$i]['CommentEng']);
				
				$comment = str_replace("\"", "\\\"", $comment);
				$commentEng = str_replace("\"", "\\\"", $commentEng);
				
				// Don't include it if it's empty
				if (trim($comment) != "")
					$entries[] = '["'.$result[$i]['CommentCode']." ".$comment.'", "'.$comment.'"]';
				if (trim($commentEng) != "")
					$entries[] = '["'.$result[$i]['CommentCode']." ".$commentEng.'", "'.$commentEng.'"]';
			}
			$output = implode(",", $entries);
		}
		// add the final closing tag
		// return the results
		return $output;
	}
	
	function Get_Comment_In_Comment_Bank($SubjectID='', $Keyword='', $CommentIDArr='', $IsAdditional=0)
	{
		$cond_SubjectID = '';
		if ($SubjectID == '')
			$cond_SubjectID = " And (SubjectID Is Null Or SubjectID = '') ";
		else
			$cond_SubjectID = " And SubjectID = '$SubjectID' ";
			
		$cond_Keyword = '';
		if ($Keyword != '')
		{
			$cond_Keyword = " 	And
								(
									CommentCode Like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
									Or
									CommentEng Like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
									Or
									Comment Like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
								)
							";
		}
		
		$cond_CommentID = '';
		if (is_array($CommentIDArr) && count($CommentIDArr) > 0)
			$cond_CommentID = " And CommentID In ( ".implode(',', $CommentIDArr)." ) ";
		
		$table = $this->DBName.".RC_COMMENT_BANK";
		$sql = "Select
						CommentID,
						CommentCode,
						CommentCategory,
						SubjectID,
						CommentEng,
						Comment
				From
						$table
				Where
						IsAdditional = '".$IsAdditional."'
						$cond_SubjectID
						$cond_Keyword
						$cond_CommentID
				Order By
						CommentCode Asc
				";
		return $this->returnArray($sql);
	}
	
	function Append_Class_Teacher_Comment($ReportID, $StudentIDArr, $CommentIDArr)
	{
		$CommentInfoArr = $this->Get_Comment_In_Comment_Bank($SubjectID=0, $Keyword='', $CommentIDArr);
		$numOfComment = count($CommentInfoArr);
		
		### get comment and check insert / update for each student
		$table = $this->DBName.".RC_MARKSHEET_COMMENT";
		$numOfStudent = count($StudentIDArr);
		$InsertStudentIDArr = array();
		$UpdateCommentIDArr = array();
		$StudentCommentArr = array();
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			$sql = "Select CommentID, Comment From $table Where ReportID='$ReportID' And SubjectID='0' And StudentID='$thisStudentID'";
			$CommentArr = $this->returnArray($sql);
			
			$StudentCommentArr[$thisStudentID] = $CommentArr[0]['Comment'];
			
			if ($CommentArr[0]['CommentID'] == '')
			{
				// insert
				$InsertStudentIDArr[] = $thisStudentID;
			}
			else
			{
				// update
				$UpdateCommentIDArr[$thisStudentID] = $CommentArr[0]['CommentID'];
			}
		}
			
		### ignore duplicated comments
		$AppendStudentCommentArr = array();
		for ($i=0; $i<$numOfComment; $i++)
		{
			$thisComment = Get_Lang_Selection($CommentInfoArr[$i]['Comment'], $CommentInfoArr[$i]['CommentEng']);
			
			for ($j=0; $j<$numOfStudent; $j++)
			{
				$thisStudentID = $StudentIDArr[$j];
				$thisStudentComment = $StudentCommentArr[$thisStudentID];
				
				if (strpos($thisStudentComment, $thisComment)===false)
					(array)$AppendStudentCommentArr[$thisStudentID][] = $thisComment;
			}
		}
		
		### insert / update student comment
		$SuccessArr = array();
		$InsertValuesArr = array();
		foreach ((array)$AppendStudentCommentArr as $thisStudentID => $thisCommentArr)
		{
			$thisInsertComment = implode("\r\n", $thisCommentArr);
			
			if (in_array($thisStudentID, $InsertStudentIDArr))
			{
				$InsertValuesArr[] = "('$thisStudentID', '0', '$ReportID', '".$this->Get_Safe_Sql_Query($thisInsertComment)."', '".$_SESSION['UserID']."', now(), now())";
			}
			else
			{
				$thisCommentID = $UpdateCommentIDArr[$thisStudentID];
				$sql_update = "	Update 
										$table
								Set 
										Comment = Concat(Comment, If (Comment Is Null Or Comment = '', '', CHAR(13)), '".$this->Get_Safe_Sql_Query($thisInsertComment)."'),
										TeacherID = '".$_SESSION['UserID']."',
										DateModified = now()
								Where	
										CommentID = '".$thisCommentID."'
							";
				$SuccessArr['UpdateComment'][$thisStudentID] = $this->db_db_query($sql_update);
			}
		}
		
		if (count($InsertValuesArr) > 0)
		{
			$sql_insert = " Insert Into $table
								(StudentID, SubjectID, ReportID, Comment, TeacherID, DateInput, DateModified)
							Values
								".implode(',', $InsertValuesArr)."
							";
			$SuccessArr['InsertComment'] = $this->db_db_query($sql_insert);
		}
		
		
		/* Old: Insert without duplication checking
		$InsertCommentArr = array();
		for ($i=0; $i<$numOfComment; $i++)
			$InsertCommentArr[] = Get_Lang_Selection($CommentInfoArr[$i]['Comment'], $CommentInfoArr[$i]['CommentEng']);
		$InsertComment = implode("\r\n", $InsertCommentArr);
		
		
		$table = $this->DBName.".RC_MARKSHEET_COMMENT";
		$InsertValuesArr = array();
		$UpdateCommentIDArr = array();
		$numOfStudent = count($StudentIDArr);
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			$sql = "Select CommentID From $table Where ReportID='$ReportID' And SubjectID='0' And StudentID='$thisStudentID'";
			$CommentArr = $this->returnArray($sql);
			
			if ($CommentArr[0]['CommentID'] == '')
			{
				// insert
				$InsertValuesArr[] = "('$thisStudentID', '0', '$ReportID', '".$this->Get_Safe_Sql_Query($InsertComment)."', '".$_SESSION['UserID']."', now(), now())";
			}
			else
			{
				// update
				$UpdateCommentIDArr[] = $CommentArr[0]['CommentID'];
			}
		}
		
		if (count($InsertValuesArr) > 0)
		{
			$sql_insert = " Insert Into $table
								(StudentID, SubjectID, ReportID, Comment, TeacherID, DateInput, DateModified)
							Values
								".implode(',', $InsertValuesArr)."
							";
			$SuccessArr['InsertComment'] = $this->db_db_query($sql_insert);
		}
		
		if (count($UpdateCommentIDArr) > 0)
		{
			$sql_update = "	Update 
									$table
							Set 
									Comment = Concat(Comment, If (Comment Is Null Or Comment = '', '', CHAR(13)), '".$this->Get_Safe_Sql_Query($InsertComment)."'),
									TeacherID = '".$_SESSION['UserID']."',
									DateModified = now()
							Where	
									CommentID In (".implode(',', $UpdateCommentIDArr).");
							";
			$SuccessArr['UpdateComment'] = $this->db_db_query($sql_update);
		}
		*/
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	//kkkk
	# Get class/subject teacher comment (if class teacher, SubjectID = "")
	function GET_TEACHER_COMMENT($StudentIDArr, $SubjectIDArr, $ReportID) {
		$returnArr = array();
		if (sizeof($StudentIDArr) < 1) {
			return $returnArr;
		}		

		$SubjectIDList_IsArr = is_array($SubjectIDArr);
		
		$table = $this->DBName.".RC_MARKSHEET_COMMENT";
		$sql = "SELECT CommentID, StudentID, Comment, AdditionalComment, SubjectID FROM $table ";
		$sql .= "WHERE SubjectID IN ('".implode("','", (array)$SubjectIDArr)."') AND ReportID = '$ReportID' ";
		$sql .= "AND StudentID IN ('".implode("','", (array)$StudentIDArr)."')";
		
		$result = $this->returnArray($sql);
		if(sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$thisStudentID = $result[$i]["StudentID"];
				$thisSubjectID = $result[$i]["SubjectID"];
				
				if ($SubjectIDList_IsArr) {
					$returnArr[$thisStudentID][$thisSubjectID]["CommentID"] = $result[$i]["CommentID"];
					$returnArr[$thisStudentID][$thisSubjectID]["Comment"] = $result[$i]["Comment"];
					$returnArr[$thisStudentID][$thisSubjectID]["AdditionalComment"] = $result[$i]["AdditionalComment"];
				}
				else {
					$returnArr[$thisStudentID]["CommentID"] = $result[$i]["CommentID"];
					$returnArr[$thisStudentID]["Comment"] = $result[$i]["Comment"];
					$returnArr[$thisStudentID]["AdditionalComment"] = $result[$i]["AdditionalComment"];
				}
			}
		}
		return $returnArr;
	}
	
	# Get extra student subject info from RC_EXTRA_SUBJECT_INFO table
	function GET_EXTRA_SUBJECT_INFO($StudentIDList, $SubjectID='', $ReportID='') {
		$returnArr = array();
		if (sizeof($StudentIDList) < 1) {
			return $returnArr;
		}
		
		$conds_SubjectID = '';
		if ($SubjectID != '') {
			$conds_SubjectID = " And SubjectID In ('".implode("','", (array)$SubjectID)."') ";
		}
		
		$conds_ReportID = '';
		if ($ReportID != '') {
			$conds_ReportID = " And ReportID In ('".implode("','", (array)$ReportID)."') ";
		}
		
		$StudentIDList = implode(",", $StudentIDList);
		$table = $this->DBName.".RC_EXTRA_SUBJECT_INFO";
		$sql = "SELECT 
						ExtraInfoID, StudentID, Info, SubjectID
				FROM
						$table
				WHERE 
						StudentID IN ($StudentIDList)
						$conds_SubjectID
						$conds_ReportID
				";
		$result = $this->returnArray($sql);

		if(sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$thisStudentID = $result[$i]["StudentID"];
				$thisSubjectID = $result[$i]["SubjectID"];
				$thisExtraInfoID = $result[$i]["ExtraInfoID"];
				$thisInfo = $result[$i]["Info"];
				
				if ($SubjectID == '' || (is_array($SubjectID))) {
					$returnArr[$thisStudentID][$thisSubjectID]["ExtraInfoID"] = $thisExtraInfoID;
					$returnArr[$thisStudentID][$thisSubjectID]["Info"] = $thisInfo;
				}
				else {
					$returnArr[$thisStudentID]["ExtraInfoID"] = $thisExtraInfoID;
					$returnArr[$thisStudentID]["Info"] = $thisInfo;
				}
				
			}
		}
		return $returnArr;
	}
	
	# added on 8 Dec 2008 by Ivan - for �װ�Ѱ| (�F�E�s) [CRM Ref No.: 2008-1205-1459]
	# Get extra student subject manual adjusted position from RC_REPORT_RESULT_SCORE_ARCHIVE table
	function GET_MANUAL_ADJUSTED_POSITION($StudentIDList, $SubjectID, $ReportID) {
		$returnArr = array();
		if (sizeof($StudentIDList) < 1) {
			return $returnArr;
		}
		if (is_array($StudentIDList))
		{
			$StudentIDList = implode(",", $StudentIDList);
		}
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE_ARCHIVE";
		$sql = "SELECT * FROM $table ";
		$sql .= "WHERE SubjectID = '$SubjectID' AND ReportID = '$ReportID' ";
		$sql .= "AND StudentID IN ($StudentIDList)";
		$sql .= "AND ReportColumnID = 0";
		$result = $this->returnArray($sql);
		if(sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$returnArr[$result[$i]["StudentID"]]["ExtraInfoID"] = $result[$i]["MarksheetConsolidatedID"];
				$returnArr[$result[$i]["StudentID"]]["Info"] = $result[$i]["OrderMeritClass"];
			}
		}
		return $returnArr;
	}
	
	################ Class Teacher's Comment ################
	
	# Get the status (completed or not) of Class teacher comment in a Class Level or a Class
	function GET_CLASS_TEACHER_COMMENT_PROGRESS($ReportID, $ParName, $ParID) {
		$table = $this->DBName.".RC_CLASS_COMMENT_PROGRESS";
		if ($ParName == "ClassLevelID") {
			$ClassArr = $this->GET_CLASSES_BY_FORM($ParID);
			
			if (sizeof($ClassArr) > 0) {
				for($i=0 ; $i<count($ClassArr) ;$i++){
					$ClassIDList[] = "'".$ClassArr[$i]["ClassID"]."'";
				}
				$ClassIDList = implode(",", $ClassIDList);
				$sql = "SELECT ClassCommentProgressID, ClassID, IsCompleted FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND ClassID IN ($ClassIDList)";
				
				$result = $this->returnArray($sql);
			} else {
				$result = array();
			}
			
			if (sizeof($result) > 0) {
				for($i=0; $i<sizeof($result); $i++) {
					$returnArr[$result[$i]["ClassID"]]["ClassCommentProgressID"] = $result[$i]["ClassCommentProgressID"];
					$returnArr[$result[$i]["ClassID"]]["IsCompleted"] = $result[$i]["IsCompleted"];
				}
			} else {
				$returnArr = array();
			}
		} else if ($ParName == "ClassID") {
			$sql = "SELECT ClassCommentProgressID, IsCompleted FROM $table ";
			$sql .= "WHERE ReportID = '$ReportID' AND ClassID = '$ParID'";
			
			$result = $this->returnArray($sql);
			if (sizeof($result) > 0) {
				$returnArr["ClassCommentProgressID"] = $result[0]["ClassCommentProgressID"];
				$returnArr["IsCompleted"] = $result[0]["IsCompleted"];
			} else {
				$returnArr = array();
			}
		}
		
		return $returnArr;
	}
	
	# Mark the Class Teacher Comment of a class in a term is completed
	function MARK_CLASS_TEACHER_COMMENT_COMPLETED($IsCompleted, $ReportID, $ClassID) {
		$commentStatus = $this->GET_CLASS_TEACHER_COMMENT_PROGRESS($ReportID, "ClassID", $ClassID);
		$success = false;
		$table = $this->DBName.".RC_CLASS_COMMENT_PROGRESS";
		
		# Already have record - UPDATE
		if (sizeof($commentStatus) > 0) {
			$sql = "UPDATE $table SET IsCompleted = '$IsCompleted', ";
			$sql .= "DateModified = NOW() ";
			$sql .= "WHERE ReportID = '$ReportID' AND ClassID = '$ClassID'";
			$success = $this->db_db_query($sql);
		# No record exist - INSERT
		} else {
			$field = "ReportID, ClassID, TeacherID, IsCompleted, DateInput, DateModified";
			$sql = "INSERT INTO $table ($field) values (
						'$ReportID', '$ClassID', '".$this->uid."', '$IsCompleted', 
						NOW(), NOW())";
			
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	###################### Progress ######################
	# Get the Submission OR Verification Period  & Dates of the ReportCard
	function GET_REPORT_DATE_PERIOD($Condition="", $ReportID="", $UnixTs=true) {
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$sql = "SELECT a.ReportID, a.ReportTitle, a.ClassLevelID, ";
		$fieldArr = array(
						"MarksheetSubmissionStart", "MarksheetSubmissionEnd", 
						"MarksheetVerificationStart", "MarksheetVerificationEnd", 
						"Issued", "LastPrinted", "LastAdjusted", "LastGenerated", 
						"LastMarksheetInput", "DateInput", "DateModified"
					);
		if ($UnixTs) {
			$sql .= "If (UNIX_TIMESTAMP(a.MarksheetSubmissionStart) = 0, '', UNIX_TIMESTAMP(a.MarksheetSubmissionStart)) AS SubStart, ";
			$sql .= "If (UNIX_TIMESTAMP(a.MarksheetSubmissionEnd) = 0, '', UNIX_TIMESTAMP(a.MarksheetSubmissionEnd)) AS SubEnd, ";
			$sql .= "If (UNIX_TIMESTAMP(a.MarksheetVerificationStart) = 0, '', UNIX_TIMESTAMP(a.MarksheetVerificationStart)) AS VerStart, ";
			$sql .= "If (UNIX_TIMESTAMP(a.MarksheetVerificationEnd) = 0, '', UNIX_TIMESTAMP(a.MarksheetVerificationEnd)) AS VerEnd, ";
			$sql .= "If (UNIX_TIMESTAMP(a.Issued) = 0, '', UNIX_TIMESTAMP(a.Issued)) AS Issued, ";
			$sql .= "If (UNIX_TIMESTAMP(a.PCSubmissionStart) = 0, '', UNIX_TIMESTAMP(a.PCSubmissionStart)) AS PCStart, ";
			$sql .= "If (UNIX_TIMESTAMP(a.PCSubmissionEnd) = 0, '', UNIX_TIMESTAMP(a.PCSubmissionEnd)) AS PCEnd ";
		} else {
			$sql .= "If (a.MarksheetSubmissionStart = '0000-00-00 00:00:00', '', a.MarksheetSubmissionStart) AS SubStart, ";
			$sql .= "If (a.MarksheetSubmissionEnd = '0000-00-00 00:00:00', '', a.MarksheetSubmissionEnd) AS SubEnd, ";
			$sql .= "If (a.MarksheetVerificationStart = '0000-00-00 00:00:00', '', a.MarksheetVerificationStart) AS VerStart, ";
			$sql .= "If (a.MarksheetVerificationEnd = '0000-00-00 00:00:00', '', a.MarksheetVerificationEnd) AS VerEnd, ";
			$sql .= "If (a.Issued AS Issued = '0000-00-00', '', a.Issued) ,";
			$sql .= "If (a.PCSubmissionStart = '0000-00-00 00:00:00', '', a.PCSubmissionStart) AS PCStart, ";
			$sql .= "If (a.PCSubmissionEnd = '0000-00-00 00:00:00', '', a.PCSubmissionEnd) AS PCEnd ";
		}
		$sql .= "FROM 
						$table as a
						INNER JOIN YEAR as b on a.ClassLevelID = b.YearID
				WHERE 
						a.ReportTitle IS NOT NULL 
				";
		if ($ReportID != "") {
			$sql .= "AND a.ReportID = '$ReportID' ";
		}
		if ($Condition != "") {
			$sql .= "AND $Condition ";
		}
		$sql .= "ORDER BY b.Sequence, a.Semester";
		$result = $this->returnArray($sql, 7);
		
		return $result;
	}
	
	# Check if the score of (all students in a class of) an Assessment Column of an subject have been filled in
	# Return "all" if no scores have been filled in the column
	# Return the number of filled scores otherwise
//	function IS_MARKSHEET_SCORE_EMPTY($StudentIDList, $SubjectID, $ReportColumnID) {
//		$StudentSize = sizeof($StudentIDList);
//		if ($StudentSize == 0)
//			return true;
//		$StudentIDList = implode(",", $StudentIDList);
//		$table = $this->DBName.".RC_MARKSHEET_SCORE";
//		$sql = "SELECT MarksheetScoreID FROM $table WHERE ";
//		$sql .= "SubjectID = '$SubjectID' AND ReportColumnID = '$ReportColumnID' AND ";
//		$sql .= "(MarkRaw <> '-1' OR MarkNonNum <> '') AND ";
//		$sql .= "StudentID IN ($StudentIDList)";
//		$result = $this->returnArray($sql, 1);
//		
//		if (sizeof($result) == 0) {
//			return "all";
//		} else if (sizeof($result) > 0) {
//			return sizeof($result);
//		}
//	}

	// overiding the function in libeRc2008
	# Case 1 occurs when $isOverallScore = 0  
	# Check if the score of (all students in a class of) an Assessment Column of an subject have been filled in
	#
	# Case 2 occurs when $isOverallScore = 1 
	# Check if the overall score of (all students in a class of) an overall term subject mark OR 
	# overall subject mark Column of an subject have been filled in when 
	# the scheme type is honor-based with Input Grade(G) OR The scheme type is PassFail(PF)
	# the Input variable $ReportColumnID regarded as $ReportID 
	#
	# Return "all" if no scores have been filled in the column
	# Return the number of filled scores otherwise
	function IS_MARKSHEET_SCORE_EMPTY($StudentIDList, $SubjectID, $ReportColumnID, $isOverallScore=0) {
		$StudentSize = sizeof($StudentIDList);
		if ($StudentSize == 0)
			return true;
		$StudentIDList = implode(",", $StudentIDList);
		$table = (!$isOverallScore) ? $this->DBName.".RC_MARKSHEET_SCORE" : $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		$sql = "SELECT ";
		$sql .= (!$isOverallScore) ? "MarksheetScoreID " : "MarksheetOverallScoreID ";
		$sql .= "FROM $table WHERE ";
		$sql .= "SubjectID = '$SubjectID' AND ";
		$sql .= (!$isOverallScore) ? "ReportColumnID = '$ReportColumnID' AND " : "ReportID = '$ReportColumnID' AND ";
		$sql .= (!$isOverallScore) ? " (MarkRaw <> '-1' OR MarkNonNum <> '') AND " : " (MarkNonNum <> '') AND ";
		$sql .= "StudentID IN ($StudentIDList)";
		$result = $this->returnArray($sql, 1);
		
		if (sizeof($result) == 0) {
			return "all";
		} else if (sizeof($result) > 0) {
			return sizeof($result);
		}
	}
	
	# Filling NA in the empty scores of Marksheet
	function FILL_EMPTY_SCORE_WITH_NA($StudentIDList, $SubjectID, $ReportColumnID, $isOverallScore=0, $ClassLevelID='', $UpdateLastModified=1) 
	{
		global $eRCTemplateSetting;
		
		$success = true;
		$StudentSize = sizeof($StudentIDList);
		if ($StudentSize == 0)
			return true;
		$StudentIDList = implode(",", $StudentIDList);
		$table = (!$isOverallScore) ? $this->DBName.".RC_MARKSHEET_SCORE" : $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		$sql = "SELECT ";
		$sql .= (!$isOverallScore) ? "MarksheetScoreID as MarksheetID" : "MarksheetOverallScoreID as MarksheetID";
		$sql .= " , StudentID, SchemeID ";
		$sql .= "FROM $table WHERE ";
		$sql .= "SubjectID = '$SubjectID' AND ";
		$sql .= (!$isOverallScore) ? "ReportColumnID = '$ReportColumnID' AND " : "ReportID = '$ReportColumnID' AND ";
		$sql .= (!$isOverallScore) ? "MarkRaw = '-1' AND " : "";
		$sql .= "MarkNonNum = '' AND ";
		$sql .= "StudentID IN ($StudentIDList)";
			
		$MarksheetScoreInfoArr = $this->returnArray($sql, 3);
		$numOfMS = count($MarksheetScoreInfoArr);
		
		$UpdateStudentIDArr = array();
		$MarksheetScoreIDArr = array();
		for ($i=0; $i<$numOfMS; $i++)
		{
			$thisStudentID = $MarksheetScoreInfoArr[$i]['StudentID'];
			$thisMarksheetID = $MarksheetScoreInfoArr[$i]['MarksheetID'];
			
			$UpdateStudentIDArr[] = $thisStudentID;
			$MarksheetScoreIDArr[] = $thisMarksheetID;
		}
		
		$success = array();
		// Update
		if(count($MarksheetScoreIDArr) > 0){
			# Fill in All Empty Scores in Marksheet By N.A.
			$lastModifiedFields = '';
			if ($UpdateLastModified)
			{
				$lastModifiedFields = ",LastModifiedUserID = ".$this->uid.", 
										DateModified = NOW()
									  ";
			}
			$MarksheetScoreIDList = implode(",", $MarksheetScoreIDArr);
			$sql = "UPDATE $table SET 
						MarkType = 'SC', 
						MarkNonNum = 'N.A.'
						$lastModifiedFields
					WHERE 
						".((!$isOverallScore) ? "MarksheetScoreID" : "MarksheetOverallScoreID")." IN ($MarksheetScoreIDList)";
			
			$success['Update'] = $this->db_db_query($sql);
		}
		
		if ($StudentIDList != '')
			$StudentIDArr = explode(',', trim($StudentIDList));
			
		// Insert
		if (count($UpdateStudentIDArr) < count($StudentIDArr))
		{
			# Get the Student who have mark already
			$table = (!$isOverallScore) ? $this->DBName.".RC_MARKSHEET_SCORE" : $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
			$sql = "SELECT ";
			$sql .= " DISTINCT(StudentID) ";
			$sql .= "FROM $table WHERE ";
			$sql .= "SubjectID = '$SubjectID' AND ";
			$sql .= (!$isOverallScore) ? "ReportColumnID = '$ReportColumnID' AND " : "ReportID = '$ReportColumnID' AND ";
			$sql .= "StudentID IN ($StudentIDList)";
			$StudentWithMarkList = $this->returnVector($sql);
			
			
			# Modified by Marcus 20101011 (for template based grading scheme) 
			$thieReportID = $this->returnReportIDByReportColumnID($ReportColumnID);
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$thieReportID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			
			$valueArr = array();
			$numOfStudent = count($StudentIDArr);
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$thisStudentID = $StudentIDArr[$i];
				
				if (in_array($thisStudentID, $UpdateStudentIDArr) || in_array($thisStudentID, $StudentWithMarkList))
					continue;
					
				$thisValue = " '$thisStudentID', '$SubjectID', '$ReportColumnID', '$SchemeID', 'SC', 'N.A.' ";
				if ($UpdateLastModified)
				{
					$thisValue .= " , '".$this->uid."', NOW(), NOW() ";
				}
				
				$valueArr[] = "( ".$thisValue." )";
			}
			
			if (count((array)$valueArr) > 0) {
				$reportField = (!$isOverallScore) ? "ReportColumnID" : "ReportID";
				$fields = " StudentID, SubjectID, $reportField, SchemeID, MarkType, MarkNonNum ";
				if ($UpdateLastModified)
				{
					$fields .= ' , LastModifiedUserID, DateInput, DateModified ';
				}
				$allValues = implode(', ', $valueArr);
				$sql = "INSERT INTO $table
						($fields)
						VALUES
						$allValues
						";
				$success['Insert'] = $this->db_db_query($sql);
			}
		}
		
		return !in_array(false, $success);
	}
	
	function FILL_ALL_EMPTY_SCORE_WITH_NA($ReportID, $UpdateLastModified=1)
	{
		# Report Info
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $reportBasicInfo['ClassLevelID'];
		
		# Report Column Info
		$reportColumnArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfReportColumn = count($reportColumnArr);
		
		# Student Info
		$studentArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		$numOfStudent = count($studentArr);
		$studentIDArr = array();
		for ($i=0; $i<$numOfStudent; $i++)
			$studentIDArr[] = $studentArr[$i]['UserID'];
		
		# Subject Info
		# Modified by Marcus 20101011 (for template based grading scheme) 
		$subjectGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID,'',0,0,$ReportID);
		$numOfSubject = count($subjectGradingArr);
		
//		$SubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$p]['SubjectID']);
		
		for ($i=0; $i<$numOfReportColumn; $i++)
		{
			$thisReportColumnID = $reportColumnArr[$i]['ReportColumnID'];
			for ($j=0; $j<$numOfSubject; $j++)
			{
				$thisSubjectID = $subjectGradingArr[$j]['SubjectID'];
				$successArr[$thisReportColumnID][$thisSubjectID]['NotOverall'] = $this->FILL_EMPTY_SCORE_WITH_NA($studentIDArr, $thisSubjectID, $thisReportColumnID, $isOverallScore=0, $ClassLevelID, $UpdateLastModified);
			}
		}
		
		# Overall Column
		for ($j=0; $j<$numOfSubject; $j++)
		{
			$thisSubjectID = $subjectGradingArr[$j]['SubjectID'];
			$SchemeID = $subjectGradingArr['SchemeID'];
			$ScaleInput = $subjectGradingArr['ScaleInput'];
			$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$SchemeType = $SchemeMainInfo['SchemeType'];
			
			if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")
				$successArr[0][$thisSubjectID] = $this->FILL_EMPTY_SCORE_WITH_NA($studentIDArr, $thisSubjectID, 0, $isOverallScore=1, $ClassLevelID, $UpdateLastModified);
		}
		
		if (in_array(false, $successArr))
			return false;
		else
			return true;
	}
	
	# Iterate a list of classes (* use StudentIDList instead of ClassID *), then a list of subjects, then a list of assessment columns
	# Run function according to different Mode inside the innest loop
	function ITERATE_MARKSHEET_SCORE($ClassIDList, $SubjectIDList, $ReportID, $Mode) {
		global $ck_ReportCard_UserType;
		$returnArr = array();
		$ReportColumnList = $this->returnReportTemplateColumnData($ReportID);
		
		$BasicInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $BasicInfoArr['Semester'];
		$ReportType = ($SemID=='F')? 'W' : 'T';
		
		for($i=0; $i<sizeof($ClassIDList); $i++) {
			$thisClassID = $ClassIDList[$i];
			
			$StudentList = $this->GET_STUDENT_BY_CLASS($ClassIDList[$i]);
			$StudentIDList = array();
			for($j=0; $j<sizeof($StudentList); $j++) {
				$StudentIDList[] = $StudentList[$j]["UserID"];
			}
			for($k=0; $k<sizeof($SubjectIDList); $k++) {
				$SubjectID = $SubjectIDList[$k];
				if($ck_ReportCard_UserType == "TEACHER") {
					$t = $this->checkSubjectTeacher($this->uid, $SubjectID, $ClassIDList[$i]);
					if(empty($t))
						continue;
				}
				for($l=0; $l<sizeof($ReportColumnList); $l++) {
					$ReportColumnID = $ReportColumnList[$l]["ReportColumnID"];
					
					/* 
					if ($ReportType == 'T')
					{
						$StudentArr = $this->GET_STUDENT_BY_SUBJECT($SubjectID, '', $thisClassID, "", $isShowBothLangs=0);
						$numOfStudent = count($StudentArr);
						
						$StudentIDList = array();
						for ($m=0; $m<$numOfStudent; $m++)
						{
							$StudentIDList[] = $StudentArr[$m]['UserID'];
						}
					}
					*/
					
					if ($Mode == "checkEmpty") {
						$empty = $this->IS_MARKSHEET_SCORE_EMPTY($StudentIDList, $SubjectID, $ReportColumnID);
						if ($empty == "all" && (!isset($returnArr[$ClassIDList[$i]][$SubjectID]) || $returnArr[$ClassIDList[$i]][$SubjectID] == "all")) {
							$returnArr[$ClassIDList[$i]][$SubjectID] = "all";
						} else {
							$returnArr[$ClassIDList[$i]][$SubjectID] = 1;
							break;
						}
					} else if ($Mode == "setNA") {
						
					}
				}
			}
		}
		return $returnArr;
	}
	
	######################## Management Functions End ##########################
	##################################################################
	
	######################## General Function Start ############################
	
	# Generate Input Text Box
	function GET_INPUT_TEXT($name, $id, $value='', $maxlength='', $otherPar=array()){
	    $returnVal = '<input type="text" ';
	    if($name){
	      $returnVal .= ' name="'.$name.'"';
	      //$returnVal .= ' id="'.str_replace('[','',str_replace(']','',$name)).'"';
	    }
	    if($id){
		    $returnVal .= ' id="'.$id.'"';
	    }
	    $returnVal .= ' value="'.$value.'"';
	    if($maxlength){
	      $returnVal .= ' maxlength="'.$maxlength.'"';
	    }
	    if(count($otherPar)>0){
	      foreach($otherPar as $parKey => $parVal){
	        $returnVal .= ' '.$parKey.'="'.$parVal.'"';
	      }
	    }
	    $returnVal .= '/>';
	    return $returnVal;
	}
	
	# Format date - $ParDateFormat compose of Y,M,D e.g. Y-M-D, D-M-Y, D/M/Y ..etc.
	# $ParSQLDate is the default format of mysql ,i.e. 2010-01-01 
	function FormatDate($ParSQLDate, $ParDateFormat)
	{
		if(empty($ParSQLDate)) return '';
		list($Y,$M,$D) = explode("-",$ParSQLDate);
		$ParDateFormat = str_replace("Y",$Y,$ParDateFormat);
		$ParDateFormat = str_replace("M",$M,$ParDateFormat);
		$ParDateFormat = str_replace("D",$D,$ParDateFormat);
		
		return $ParDateFormat;
	} 
	######################## End of General  ######################
	
	##################################################################
	######################## Reports Functions Start ############################
	
	# Get the next class level when promoting from a given class level
	# Now Hardcoded for SIS only
	function GET_NEXT_PROMOTED_CLASSLEVEL($ClassLevelName) {
		$ClassLevelOrder = array("P1", "P2", "P3", "P4", "P5", "P6", "S1", "S2", "S3");
		$newClassLevelKey = array_search($ClassLevelName, $ClassLevelOrder);
		// array_search() may return the Index "0", therefore must use "===" operator to test it
		if ($newClassLevelKey === FALSE || ($newClassLevelKey+1 > sizeof($ClassLevelOrder)))
			return "-";
		else
			return $ClassLevelOrder[$newClassLevelKey+1];
	}
	
	# Determine if student can be promoted to next Class Level, conditions are hardcoded
	function IS_PROMOTED($classLevelName, $overallPercent, $subjectMark=array()) {
		$promoteGroup1 = array("P1", "P2", "P3", "P4", "P5");
		$promoteGroup2 = array("P6", "S1", "S2", "S3");
		
		// minimum average percentage
		$promotePercent1 = 50;
		$promotePercent2 = 55;
		
		// minimum mark for required subject
		$promoteSubjMark = 55;
		
		if (in_array(trim($classLevelName), $promoteGroup1)) {
			return ($overallPercent >= $promotePercent1);
		}
		
		if (in_array(trim($classLevelName), $promoteGroup2)) {
			if (sizeof($subjectMark) > 0) {
				foreach ($subjectMark as $codeID => $mark) {
					if ($mark < $promoteSubjMark)
						return 0;
				}
			}
			return ($overallPercent >= $promotePercent2);
		}
		return "ERROR";
	}
	
	function GET_PROMOTION_INFO($ReportID, $StudentIDList) {
		global $eReportCard;
		
		$table = $this->DBName.".RC_REPORT_RESULT";
		$sql  = "SELECT ResultID, StudentID, Promotion, NewClassName from $table ";
		$sql .= "WHERE ReportID = '$ReportID' AND StudentID IN ($StudentIDList) ";
		$sql .= "AND (ReportColumnID = '' OR ReportColumnID IS NULL OR ReportColumnID = '0')";
		
		$result = $this->returnArray($sql, 2);
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) {
			$returnArr[$result[$i]["StudentID"]]["ResultID"] = $result[$i]["ResultID"];
			$returnArr[$result[$i]["StudentID"]]["Promotion"] = $result[$i]["Promotion"];
			$returnArr[$result[$i]["StudentID"]]["PromotionText"] = $eReportCard["PromotionStatus"][$result[$i]["Promotion"]];
			$returnArr[$result[$i]["StudentID"]]["NewClassName"] = $result[$i]["NewClassName"];
		}
		return $returnArr;
	}
	
	// Round the given mark
	// 3 types: SubjectScore, SubjectTotal, GrandAverage
	// 20080827: 4 types, plus GrandTotal
	function ROUND_MARK($mark, $type) {
		if (!is_numeric($mark))	return $mark;
		
		global $storageDisplaySettings;
		if (!isset($storageDisplaySettings)) {
			$storageDisplaySettings = $this->LOAD_SETTING("Storage&Display");
		}
		
		return my_round($mark, $storageDisplaySettings[$type]);
	}
	
	
	# Get the result score for s group of student
	function GET_RESULT_SCORE($StudentIDArr, $SubjectID, $ReportColumnID) {
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$fields  = "ReportResultScoreID, StudentID, Mark, Grade ";
		$students = implode(",", $StudentIDArr);
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE StudentID IN ($students) AND SubjectID = '$SubjectID' AND ";
		$sql .= "ReportColumnID = '$ReportColumnID'";
		$result = $this->returnArray($sql, 4);
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) {
			$returnArr[$result[$i]["StudentID"]] = 
				array(
					"ReportResultScoreID" =>  $result[$i]["ReportResultScoreID"],
					"Mark" =>  $result[$i]["Mark"],
					"Grade" =>  $result[$i]["Grade"]
				);
		}
		return $returnArr;
	}
	
	# Get the result score for s group of student, make the return array like GET_MARKSHEET_SCORE()
	function GET_RESULT_SCORE_FOR_SUBJECT_OVERALL($StudentIDArr, $SubjectID, $ReportColumnID, $ReportID) {
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$fields  = "ReportResultScoreID, StudentID, Mark, Grade, RawMark ";
		$students = implode(",", $StudentIDArr);
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE StudentID IN ($students) AND SubjectID = '$SubjectID' AND ";
		$sql .= "ReportColumnID = '$ReportColumnID' AND ReportID = '$ReportID'";
		
		
		$result = $this->returnArray($sql, 5);
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) {
			if (in_array($result[$i]["Grade"], $this->specialCasesSet1) || in_array($result[$i]["Grade"], $this->specialCasesSet2))
				$MarkType = "SC";
			else if (is_numeric($result[$i]["RawMark"]))
				$MarkType = "M";
			else 
				$MarkType = "G";
			/*
			if (is_numeric($result[$i]["RawMark"])) {
				$MarkType = "M";
			} else {
				if (in_array($result[$i]["Grade"], $this->specialCasesSet1) || in_array($result[$i]["Grade"], $this->specialCasesSet2))
					$MarkType = "SC";
				else
					$MarkType = "G";
			}
			*/
			
			$returnArr[$result[$i]["StudentID"]] = 
				array(
					"ReportResultScoreID" =>  $result[$i]["ReportResultScoreID"],
					"MarkRaw" =>  $result[$i]["RawMark"],
					"MarkType" =>  $MarkType,
					"MarkNonNum" =>  $result[$i]["Grade"]
				);
		}
		return $returnArr;
	}
	
	# Get new full mark
	function GET_NEW_FULLMARK($ReportID, $StudentID, $SubjectID, $ReportColumnID) {
		$PreloadArrKey = 'GET_NEW_FULLMARK';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	if ($StudentID !== '') {
    		$conds_StudentID = " And StudentID In ('".implode("','", (array)$StudentID)."') ";
    	}
    	
    	if ($ReportColumnID !== '') {
    		$conds_ReportColumnID = " And ReportColumnID In ('".implode("','", (array)$ReportColumnID)."') ";
    	}
    	
    	if ($SubjectID !== '') {
    		$conds_SubjectID = " And SubjectID In ('".implode("','", (array)$SubjectID)."') ";
    	}
    	
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$sql = "SELECT 
						ReportResultFullmarkID,
						StudentID,
						ReportID,
						ReportColumnID,
						SubjectID,
						FullMark,
						Semester
				FROM 
						$table 
				WHERE 
						ReportID = '$ReportID' 
						$conds_SubjectID
						$conds_StudentID 
						$conds_ReportColumnID
				";
		$result = $this->returnArray($sql);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $result);
		return $result;
	}
	
	######################## Reports Functions Start ############################
	##################################################################
	
	/* ******************************************* */
	// already defined by Jason
	
	# To Check whether the Full Mark of input special case is count for total full mark or not
	# $scString : string of special case
	function IS_COUNT_FULLMARK_OF_SPECIALCASE($scString, $absentExemptSettings){
		$isCount = 1;
		if($scString == "-"){
	    	$isCount = ($absentExemptSettings['Absent'] == "ExFullMark") ? 0 : 1;
		} else if($scString == "/"){
			$isCount = ($absentExemptSettings['Exempt'] == "ExFullMark") ? 0 : 1;
		} else if($scString == "*" || $scString == "N.A."){
    		$isCount = 0;
		}
		return $isCount;
	}
	
	# Check Whether The Input Character / String is Special Case Charactors or not For Set 1
	function CHECK_MARKSHEET_SPECIAL_CASE_SET1($testChar){
		$SpecialCase = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		return in_array($testChar, $SpecialCase);
	}
	
	# Check Whether The Input Character / String is Special Case Charactors or not For Set 2
	function CHECK_MARKSHEET_SPECIAL_CASE_SET2($testChar){
		$SpecialCase = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		return in_array($testChar, $SpecialCase);	
	}
	
	# To Check whether the weight of input special case is count for total weight or not
	# $scString : string of special case
	function IS_COUNT_WEIGHT_OF_SPECIALCASE($scString, $absentExemptSettings){
		$scMark = '';
		$isCount = 1;
		if($scString == "-"){
			$isCount = ($absentExemptSettings['Absent'] == "ExWeight" || $absentExemptSettings['Absent'] == "ExFullMark") ? 0 : 1;
		} else if($scString == "/"){
			$isCount = ($absentExemptSettings['Exempt'] == "ExWeight" || $absentExemptSettings['Exempt'] == "ExFullMark") ? 0 : 1;
		} else if($scString == "*" || $scString == "N.A."){
    		$isCount = 0;
		}
		return $isCount;
	}
	
	# To Handle the case of calculating Parent Subject Term Mark From Component Subjects
	# A Factor is created and is used by comparing the Full Mark of Parent Subject And Total Full Mark of All Component Subjects
	function GET_FULL_MARK_FACTOR($ParentFullMark, $TotalCmpFullMark){
		$returnVal = '';
		if($TotalCmpFullMark > 0){
			$returnVal = $ParentFullMark / $TotalCmpFullMark;
		}
		return $returnVal;
	}
	
	# Get Full Mark of Subject 
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function GET_SUBJECT_FULL_MARK($SubjectID, $ClassLevelID, $ReportID=''){
		$returnVal = '';
		if($SubjectID != "" && $ClassLevelID != ""){
			$SubjectFormGradingArr = array();
			
			# Get Subject Form Grading Info
			$SubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
			$SchemeID = (count($SubjectFormGradingArr) > 0) ? $SubjectFormGradingArr['SchemeID'] : '';
			
			# Get Subject Full Mark
			$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$FullMark = (count($SchemeMainInfo) > 0) ? $SchemeMainInfo['FullMark'] : '';
			$returnVal = $FullMark;
		}
		return $returnVal;
	}
	
	# Special Case for Honor Based - Mark or Grade
	# Modified by Andy 21/5/2008: The set of special cases are defined in the constructor
	function GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1(){
		return $this->specialCasesSet1;
	}
	
	function GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($case){
		global $eReportCard;
		$returnVal = '';
		switch ($case){
			case "+":
				$returnVal = $eReportCard['RemarkAbsentZeorMark'];
				break;
			case "-":
				$returnVal = $eReportCard['RemarkAbsentNotConsidered'];
				break;
			case "*":
				$returnVal = $eReportCard['RemarkDropped'];
				break;
			case "/":
				$returnVal = $eReportCard['RemarkExempted'];
				break;
			case "N.A.":
				$returnVal = $eReportCard['RemarkNotAssessed'];
				break;
			default:
				break;
		}
		return $returnVal;
	}
	
	# Special Case for PassFail Based - Pass or Fail
	function GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2(){
		return $this->specialCasesSet2;
	}
	
	function GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($case){
		global $eReportCard;
		$returnVal = '';
		switch ($case){
			case "abs":
				$returnVal = $eReportCard['RemarkAbsent'];
				break;
			case "*":
				$returnVal = $eReportCard['RemarkDropped'];
				break;
			case "/":
				$returnVal = $eReportCard['RemarkExempted'];
				break;
			case "N.A.":
				$returnVal = $eReportCard['RemarkNotAssessed'];
				break;
			default: 
				break;
		}
		return $returnVal;
	}
	
	function Get_Exclude_Ranking_Special_Case()
	{
		return $this->specialCasesSetExcludeRanking;
	}
	
	function GET_SPECIAL_CASE_SET1_STRING_ARR()
	{
		
		$Set = $this->specialCasesSet1;
		$returnAry = array();
		
		for($i=0; $i<sizeof($Set); $i++)
		{
			$returnAry[$Set[$i]]=$this->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($Set[$i]);
		}
	
		return $returnAry;
	}
	
	function GET_SPECIAL_CASE_SET2_STRING_ARR()
	{
		$Set = $this->specialCasesSet2;
		$returnAry = array();
		for($i=0; $i<sizeof($Set); $i++)
		{
			$returnAry[$Set[$i]]=$this->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($Set[$i]);
		}
		return $returnAry;
	}
	
	function CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($CheckVal)
	{
		$StrAry = $this->GET_SPECIAL_CASE_SET1_STRING_ARR();
		return in_array($CheckVal,$StrAry);
		
	}
	
	function CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($CheckVal)
	{
		$StrAry = $this->GET_SPECIAL_CASE_SET2_STRING_ARR();
		return in_array($CheckVal,$StrAry);
	}
	

	# Modified by Andy on 30/4/2008, add "isExFullMark" to the returnArr for checking if full mark is needed to be excluded
	# Before using this function, an Array with special case as a key must be created AND 
	# each Array with special case has an associated array with name "isCount", "Count" & "value"
	# Check for output of special case in marksheet 
	function DIFFERENTIATE_SPECIAL_CASE($specialcaseArr, $specialcaseString, $absentExemptSettings){
		$returnArr = $specialcaseArr;
		$sc = $specialcaseString;
		switch($sc){
			case "+":
			case "abs":
				$returnArr[$sc]['isCount'] = 1;
				$returnArr[$sc]['Value'] = 0;
				break;
			case "-":
				if($absentExemptSettings['Absent'] == "ExWeight"){
					$returnArr[$sc]['Count']++;
					$returnArr[$sc]['isCount'] = 0;
					$returnArr[$sc]['Value'] = '';
					$returnArr[$sc]['ExWeight'] = 1;
				} else if($absentExemptSettings['Absent'] == "ExFullMark"){
					$returnArr[$sc]['Count']++;
					$returnArr[$sc]['isCount'] = 0;
					$returnArr[$sc]['Value'] = '';
					$returnArr[$sc]['isExFullMark'] = 1;
				} else if($absentExemptSettings['Absent'] == "Mark0"){
					$returnArr[$sc]['isCount'] = 1;
					$returnArr[$sc]['Value'] = 0;
				} 
				break;
			case "/":
				if($absentExemptSettings['Exempt'] == "ExWeight"){
					$returnArr[$sc]['Count']++;
					$returnArr[$sc]['isCount'] = 0;
					$returnArr[$sc]['Value'] = '';
					$returnArr[$sc]['ExWeight'] = 1;
				} else if($absentExemptSettings['Exempt'] == "ExFullMark"){
					$returnArr[$sc]['Count']++;
					$returnArr[$sc]['isCount'] = 0;
					$returnArr[$sc]['Value'] = '';
					$returnArr[$sc]['isExFullMark'] = 1;
				} else if($absentExemptSettings['Exempt'] == "Mark0"){
					$returnArr[$sc]['isCount'] = 1;
					$returnArr[$sc]['Value'] = 0;
				} 
				break;
			case "*":
				$returnArr[$sc]['Count']++;
				$returnArr[$sc]['isCount'] = 0;
				$returnArr[$sc]['Value'] = '';
				$returnArr[$sc]['isExFullMark'] = 1;
				break;
			case "N.A.":
				$returnArr[$sc]['Count']++;
				$returnArr[$sc]['isCount'] = 0;
				$returnArr[$sc]['Value'] = '';
				$returnArr[$sc]['isExFullMark'] = 1;
				break;
		}
		return $returnArr;
	}
	
	# Get the marksheet overall score for s group of student for the cases
	# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
	# when the scheme type is PassFail-Based (PF)
	function GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet=0) 
	{
		global $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$CheckStudentInSG = false;		
		if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup'] && $SkipSubjectGroupCheckingForGetMarkSheet!=1)
		{
			$CheckStudentInSG = true;
			$SubjectGroupStudentIDArr = $this->Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $SubjectID);
		}
		
		$table = $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		$fields  = "MarksheetOverallScoreID, StudentID, SchemeID, MarkType, ";
		$fields .= "MarkNonNum, LastModifiedUserID, IsEstimated ";
		$students = implode(",", $StudentIDArr);
		$sql  = "SELECT $fields FROM $table ";
		$sql .= "WHERE StudentID IN ($students) AND SubjectID = '$SubjectID' AND ";
		$sql .= "ReportID = '$ReportID'";
		$result = $this->returnArray($sql,5);
		
		$returnArr = array();
		for($i=0; $i<sizeof($result); $i++) {
			// Only add item to array if MarkNonNum have value
			if ($result[$i]["MarkNonNum"] == '') continue;
			
			$thisStudentID = $result[$i]["StudentID"];
			
			if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']==true && $CheckStudentInSG==true && in_array($thisStudentID, $SubjectGroupStudentIDArr)==false)
			{
				$returnArr[$thisStudentID] = 
				array(
					"MarksheetOverallScoreID" =>  $result[$i]["MarksheetOverallScoreID"],
					"SchemeID" =>  $result[$i]["SchemeID"],
					"MarkType" =>  'SC',
					"MarkNonNum" =>  'N.A.',
					"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
					"IsEstimated" =>  0,
				);
			}
			else
			{
				$returnArr[$thisStudentID] = 
				array(
					"MarksheetOverallScoreID" =>  $result[$i]["MarksheetOverallScoreID"],
					"SchemeID" =>  $result[$i]["SchemeID"],
					"MarkType" =>  $result[$i]["MarkType"],
					"MarkNonNum" =>  $result[$i]["MarkNonNum"],
					"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
					"IsEstimated" =>  $result[$i]["IsEstimated"],
				);
			}
		}
		return $returnArr;
	}
	
	// 20111115
	function GET_CLASSES_BY_FORM($ParClassLevelID="", $ClassID="", $returnAssoName=0, $returnAssoInfo=0)
	{
		if ($_SESSION['intranet_session_language'] == 'en')
			$titleField = 'ClassTitleEN';
		else
			$titleField = 'ClassTitleB5';
			
		$formCond = '';
		if ($ParClassLevelID != '')
			$formCond = " AND YearID = '$ParClassLevelID' ";
			
		$classCond = '';
		if ($ClassID != '')
			$classCond = " AND YearClassID = '$ClassID' ";
		
		//add ClassGroupID by Marcus 20091127 (for Stream ordering)
		$sql= "SELECT 
						YearClassID as ClassID, 
						$titleField as ClassName,
						ClassTitleEN,
						ClassTitleB5,
						ClassGroupID,
						YearID ClassLevelID
				From 
						YEAR_CLASS
				WHERE 
						AcademicYearID = '".$this->schoolYearID."'
						$formCond
						$classCond
				ORDER BY
						Sequence
				";
		$return = $this->returnArray($sql, 2);
		
		$returnArr = array();
		if ($returnAssoName == 1)
			$returnArr = build_assoc_array($return, "---");
		else if ($returnAssoInfo == 1)
		{
			foreach ((array)$return as $key => $ValueArr)
			{
				$thisClassID = $ValueArr['ClassID'];
				$returnArr[$thisClassID] = $ValueArr;
			}
		}
		else
			$returnArr = $return;

		return $returnArr;
	}
	
	# Get Student List by ClassID
	//20111121
	function GET_STUDENT_BY_CLASS($ParClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=0)
	{
		$cond = ($ParStudentIDList!="") ? " AND u.UserID IN ($ParStudentIDList)" : "";
		
		if ($withClassNumber == 1)
		{
			$NameField = getNameFieldWithClassNumberByLang('u.');
			$ArchiveNameField = getNameFieldWithClassNumberByLang('au.');
		}
		else
		{
			if ($isShowBothLangs)
			{
				$NameField = "	CONCAT(	TRIM(u.EnglishName), 
										If (
											u.ChineseName Is Not Null Or u.ChineseName != '',
											Concat(' (', TRIM(u.ChineseName), ')'),
											''
										)
								)
							";
				$ArchiveNameField = "	CONCAT(	TRIM(au.EnglishName), 
												If (
													au.ChineseName Is Not Null Or au.ChineseName != '',
													Concat(' (', TRIM(au.ChineseName), ')'),
													''
												)
										)
									";
			}
			else
			{
				$NameField = getNameFieldByLang2("u.");
				$ArchiveNameField = getNameFieldByLang2("au.");
			}
			//$NameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("u.") : "CONCAT(TRIM(u.EnglishName), ' (', TRIM(u.ChineseName), ')')";
			//$ArchiveNameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("au.") : "CONCAT(TRIM(au.EnglishName), ' (', TRIM(au.ChineseName), ')')";
		}
		
		
		
		if ($isShowStyle==0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
			
		$sql = "SELECT 
						DISTINCT(ycu.UserID), 
						u.WebSAMSRegNo, 
						ycu.ClassNumber as ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
							ELSE ".$NameField." 
						END as StudentName,
						yc.ClassTitleEN as ClassName,
						u.EnglishName as StudentNameEn,
						u.ChineseName as StudentNameCh,
						yc.ClassTitleEN as ClassTitleEn,
						yc.ClassTitleB5 as ClassTitleCh,
						u.WebSAMSRegNo,
						u.UserLogin,
						yc.YearClassID
				FROM 
						YEAR_CLASS_USER as ycu
						INNER JOIN 
						YEAR_CLASS as yc
						ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."')
						INNER JOIN
						INTRANET_USER as u 
						ON (ycu.UserID = u.UserID)
						Left Join
						INTRANET_ARCHIVE_USER as au 
						ON (u.UserID = au.UserID) 
				WHERE 
						yc.YearClassID IN (".implode(",",(array)$ParClassID).")
						$cond
				ORDER BY 
						yc.Sequence,
						ycu.ClassNumber 
				";
		$row_student = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1)
		{
			foreach ($row_student as $key => $thisStudentInfoArr)
			{
				$thisStudentID = $thisStudentInfoArr['UserID'];
				$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
			}
		}
		else
		{
			$ReturnArr = $row_student;
		}
		
		return $ReturnArr;
	}
	
	# Get Student List by Subject Group
	function GET_STUDENT_BY_SUBJECT_GROUP($ParSubjectGroupIDArr, $ParClassLevelID='', $ParStudentIDList="", $isShowBothLangs=0, $isShowStyle=0, $returnAsso=0)
	{
		$student_cond = ($ParStudentIDList!="") ? " AND u.UserID IN ($ParStudentIDList) " : "";
		$classLevel_cond = ($ParClassLevelID!="") ? " AND yc.YearID = '$ParClassLevelID' " : "";
		
		//$NameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("u.") : " CONCAT(TRIM(u.EnglishName), ' (', TRIM(u.ChineseName), ')') ";
		//$ArchiveNameField =  ($isShowBothLangs == 0) ? getNameFieldByLang2("au.") : " CONCAT(TRIM(au.EnglishName), ' (', TRIM(au.ChineseName), ')') ";
		$ClassNumField = getClassNumberField("ycu.");
		
		if ($isShowBothLangs)
		{
			$NameField = "	CONCAT(	TRIM(u.EnglishName), 
									If (
										u.ChineseName Is Not Null Or u.ChineseName != '',
										Concat(' (', TRIM(u.ChineseName), ')'),
										''
									)
							)
						";
			$ArchiveNameField = "	CONCAT(	TRIM(au.EnglishName), 
											If (
												au.ChineseName Is Not Null Or au.ChineseName != '',
												Concat(' (', TRIM(au.ChineseName), ')'),
												''
											)
									)
								";
		}
		else
		{
			$NameField = getNameFieldByLang2("u.");
			$ArchiveNameField = getNameFieldByLang2("au.");
		}
		
		
		if ($isShowStyle==0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
		
		$sql = "SELECT 
						DISTINCT(u.UserID), 
						u.WebSAMSRegNo, 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
							ELSE ".$NameField." 
						END as StudentName,
						yc.ClassTitleEn as ClassName,
						u.UserLogin,
						stcu.SubjectGroupID,
						yc.ClassTitleEn,
						yc.ClassTitleB5 AS ClassTitleCh
				FROM 
						SUBJECT_TERM_CLASS_USER as stcu
						Inner Join
						INTRANET_USER as u ON (stcu.UserID = u.UserID AND stcu.SubjectGroupID In (".implode(',', (array)$ParSubjectGroupIDArr).") $student_cond)
						Left Join
						INTRANET_ARCHIVE_USER as au ON (u.UserID = au.UserID) 
						Inner Join
						YEAR_CLASS_USER as ycu ON (u.UserID = ycu.UserID)
						Inner Join
						YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' $classLevel_cond )
				ORDER BY
						yc.ClassTitleEn, 
						ycu.ClassNumber
				";
		$row = $this->returnArray($sql,4);
		
		$ReturnArray = array();
		if ($returnAsso == 1) {
			$ReturnArray = BuildMultiKeyAssoc($row, 'SubjectGroupID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		}
		else {
			$ReturnArray = $row;
		}
		
		return $ReturnArray;
	}
	
	
	// 20110520
	function GET_STUDENT_BY_SUBJECT($ParSubjectID, $ParClassLevelID='', $ParClassID='', $ParStudentIDList="", $isShowBothLangs=0)
	{
		$student_cond = ($ParStudentIDList!="") ? " AND u.UserID IN ($ParStudentIDList) " : "";
		$NameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("u.") : " CONCAT(TRIM(u.EnglishName), ' (', TRIM(u.ChineseName), ')') ";
		$classLevel_cond = ($ParClassLevelID!="") ? " AND yc.YearID = '$ParClassLevelID' " : "";
		$class_cond = ($ParClassID!="") ? " AND yc.YearClassID = '$ParClassID' " : "";
		
//		$ClassNumField = getClassNumberField("ycu.");
		$sql = "SELECT 
						DISTINCT(u.UserID), 
						u.WebSAMSRegNo, 
						ycu.ClassNumber as ClassNumber,
						$NameField as StudentName,
						yc.ClassTitleEn as ClassName,
						u.UserLogin,
						st.SubjectID,
						yc.YearClassID
				FROM 
						SUBJECT_TERM as st
					INNER JOIN
						SUBJECT_TERM_CLASS_USER as stcu ON (st.SubjectGroupID = stcu.SubjectGroupID )
					INNER JOIN 
						INTRANET_USER as u ON (stcu.UserID = u.UserID  $student_cond)
					INNER JOIN
						YEAR_CLASS_USER as ycu ON (u.UserID = ycu.UserID)
					INNER JOIN
						YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' $classLevel_cond $class_cond)
				WHERE
					1
					AND st.SubjectID IN (".implode(",",(array)$ParSubjectID).")
				ORDER BY
						st.SubjectID,
						u.ClassName, 
						ycu.ClassNumber 
				";
		$row = $this->returnArray($sql,4);
		return $row;
	}
	
	# Get Report Type (Period) of Report Templates of ClassLevelID (Form)
	function GET_REPORT_TYPES($ClassLevelID, $ReportID="", $Semester='', $MainReportOnly=0){
		$result = array();
		$returnArr = array();
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		if ($MainReportOnly)
			$conds_isMainReport = " And isMainReport = 1 ";
		
		$sql = "	
			SELECT 
				Semester, ReportID, ReportTitle  
			FROM 
				$table 
			WHERE 
				ClassLevelID = $ClassLevelID 
				".(($ReportID != "") ? "AND ReportID = '$ReportID'" : "")."
				".(($Semester != "") ? "AND Semester = '$Semester'" : "")."
				$conds_isMainReport
			ORDER BY 
				Semester 
		";
		$result = $this->returnArray($sql, 2);
		
		if(count($result) > 0){
			for($i=0 ; $i<count($result) ; $i++){
				if($ReportID == ""){
					$returnArr[] = $result[$i];
					//$returnArr[$i]['SemesterTitle'] = ($result[$i]['Semester'] != "F") ? $this->GET_ALL_SEMESTERS($result[$i]['Semester']) : $result[$i]['ReportTitle'];
					$returnArr[$i]['SemesterTitle'] = str_replace(":_:", " ", $result[$i]['ReportTitle']);
				} 
				else {
					$returnArr['ReportID'] = $result[$i]['ReportID'];
					$returnArr['Semester'] = $result[$i]['Semester'];
					//$returnArr['SemesterTitle'] = ($result[$i]['Semester'] != "F") ? $this->GET_ALL_SEMESTERS($result[$i]['Semester']) : $result[$i]['ReportTitle'];
					$returnArr['SemesterTitle'] = str_replace(":_:", " ", $result[$i]['ReportTitle']);
				}
			}
		}
		return $returnArr;		
	}
	
	/*
	* Get details from MarkSheet_Submission_Progress
	*/
	function GET_MARKSHEET_SUBMISSION_PROGRESS($ParSubjectID, $ParReportID, $ParClassID='', $ParSubjectGroupIDArr='', $ParReturnAsso=0){
		$returnArr = array();
		$row = array();
		
		//if($ParSubjectID != "" && $ParReportID != ""){
		if($ParReportID != ""){
			
			if ($ParSubjectID != '')
				$subject_cond = " AND SubjectID = '$ParSubjectID' ";
			
			if ($ParClassID != '')
				$class_cond = " AND ClassID = '$ParClassID' ";
				
			if ($ParSubjectGroupIDArr != '')
				$subject_group_cond = " AND SubjectGroupID In (".implode(',', (array)$ParSubjectGroupIDArr).") ";
				
			$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
			$sql = "
				SELECT 
					MarksheetSubmissionProgressID, IsCompleted, SubjectGroupID
				FROM 
					$table 
				WHERE 
					ReportID = $ParReportID
					$subject_cond
					$class_cond
					$subject_group_cond
			";
			$row = $this->returnArray($sql, 2);
		
			if(count($row) > 0){
				if ($ParReturnAsso == 1) {
					$numOfRow = count($row);
					for ($i=0; $i<$numOfRow; $i++) {
						$thisSubjectGroupID = $row[$i]['SubjectGroupID'];
						
						$returnArr[$thisSubjectGroupID]['MarksheetSubmissionProgressID'] = $row[$i]['MarksheetSubmissionProgressID'];
						$returnArr[$thisSubjectGroupID]['IsCompleted'] = $row[$i]['IsCompleted'];
					}
				}
				else {
					$returnArr['MarksheetSubmissionProgressID'] = $row[0]['MarksheetSubmissionProgressID'];
					$returnArr['IsCompleted'] = $row[0]['IsCompleted'];
				}
			}
		}
		return $returnArr;
	}
	
	/*
	* Get details from MarkSheet_Submission_Progress
	*/
	function GET_MARKSHEET_SUBMISSION_PROGRESS_OF_ALL_SUBJECT_GROUP($ParSubjectID, $ParReportID, $ParClassID){
		$returnArr = array();
		$row = array();
		
//		include_once("subject_class_mapping.php");
//		$objSubject = new subject($ParSubjectID);
//		if ($objSubject->Is_Component_Subject())
//		{
//			$objSubjectComponent = new Subject_Component($ParSubjectID);
//			$ParentInfoArr = $objSubjectComponent->Get_Parent_Subject();
//			$ParSubjectID = $ParentInfoArr['RecordID'];
//		}
		if ($this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($ParSubjectID)) {
			$ParSubjectID = $this->GET_PARENT_SUBJECT_ID($ParSubjectID);
		}
		
		
		# get the subject group list of the class in this report
		$SubjectGroupIDArr = $this->Get_Subject_Group_List_Of_Report($ParReportID, $ParClassID, $ParSubjectID);
		$numOfSubjectGroup = count($SubjectGroupIDArr);
		
		if ($numOfSubjectGroup > 0)
		{
			$SubjectGroupList = implode(',', $SubjectGroupIDArr);
			
			# check all marksheet is empty or not
			$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
			$sql = "SELECT
							IsCompleted
					FROM
							$table
					WHERE
							SubjectID = $ParSubjectID
						AND 
							ReportID = $ParReportID
						AND
							SubjectGroupID IN ($SubjectGroupList)
					";
			$isCompleteArr = $this->returnVector($sql);
			$numIsComplete = count($isCompleteArr);
			
			if ($numIsComplete == 0)
			{
				$returnArr['IsCompleted'] = 0;
			}
			else
			{
				$returnArr['IsCompleted'] = 1;
				for ($i=0; $i<$numIsComplete; $i++)
				{
					$thisIsComplete = $isCompleteArr[$i];
					
					if ($thisIsComplete == 0 || $thisIsComplete == '')
					{
						$returnArr['IsCompleted'] = 0;
						break;
					}
				}
			}
		}
		else
		{
			# treat as completed if the class does not have student studying the subject
			$returnArr['IsCompleted'] = 1;
		}
		
		return $returnArr;
	}
	
	function UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, $isComplete, $ParentSubjectID="", $SubjectGroupID=''){
		$returnVal = true;
		$result = array();
		
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportType = $ReportInfo['Semester'];
		
		$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		$CmpSubjectArr = ($isCmpSubject) ?  $this->GET_COMPONENT_SUBJECT($ParentSubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0) : $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0); 
		
		# Set to NotCompleted
		if($isComplete == 0){
			# For Term Report - need to consider the complete status of Whole Year Report
			if($ReportType != "F"){
				$ReportIDArr = $this->ReportTemplateList($ClassLevelID, 0);
				
				if(count($ReportIDArr) > 0){
					for($i=0 ; $i<count($ReportIDArr) ; $i++){
						$whole_year_report_id = $ReportIDArr[$i]['ReportID'];
						
						if($ReportIDArr[$i]['Semester'] == "F"){
							$isRelatedReportID = 0;
							$WholeYearReportInfo = $this->returnReportTemplateColumnData($whole_year_report_id);
							if(count($WholeYearReportInfo) > 0){
								for($k=0 ; $k<count($WholeYearReportInfo) ; $k++){
									if($ReportType == $WholeYearReportInfo[$k]['SemesterNum'])
										$isRelatedReportID = 1;
								}
							}
							
							if($isRelatedReportID){
								if($isCmpSubject || (!$isCmpSubject && !empty($CmpSubjectArr))){
									# For Whole Year Report AND Term Report
									# Get Complete Status of Parent Subject 
									if($isCmpSubject)
										if ($SubjectGroupID == '')
											$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $whole_year_report_id, $ClassID);
										else
											$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $whole_year_report_id, '', $SubjectGroupID);
									else 
										if ($SubjectGroupID == '')
											$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $whole_year_report_id, $ClassID);
										else
											$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $whole_year_report_id, '', $SubjectGroupID);
									
									if(count($ProgressArr) > 0 && $ProgressArr['IsCompleted'] == 1){
										if(!empty($CmpSubjectArr)){	
											for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
												$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
												
												# Get any existing record of Component Subject in Marksheet Submission Progress
												if ($SubjectGroupID == '')
													$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, $ClassID);
												else
													$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, '', $SubjectGroupID);
												
												if(count($CmpSubmissionProgressArr) > 0){	
													# UPDATE - if having existing records
													if ($SubjectGroupID == '')
														$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, $ClassID, 0);
													else
														$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, '', 0, $SubjectGroupID);
												} else {	
													# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
													if ($SubjectGroupID == '')
														$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, $ClassID, 0);
													else
														$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $whole_year_report_id, '', 0, $SubjectGroupID);
												}
											}
										}
										# UPDATE - parent subject
										if($isCmpSubject)
											if ($SubjectGroupID == '')
												$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $whole_year_report_id, $ClassID, 0);
											else
												$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $whole_year_report_id, '', 0, $SubjectGroupID);
										else 
											if ($SubjectGroupID == '')
												$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $whole_year_report_id, $ClassID, 0);
											else
												$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $whole_year_report_id, '', 0, $SubjectGroupID);
									}
								}
							} 
						}
					}
				}
			}
			
			//if($isCmpSubject || (!$isCmpSubject && !empty($CmpSubjectArr)) ){				
				# For Whole Year Report AND Term Report
				# Get Complete Status of Parent Subject 
				if($isCmpSubject)
					if ($SubjectGroupID == '')
						$SubProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $ReportID, $ClassID);
					else
						$SubProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $ReportID, '', $SubjectGroupID);
				else 
					if ($SubjectGroupID == '')
						$SubProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
					else
						$SubProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, '', $SubjectGroupID);
				
				# When Complete Status of Parent Subject is 1(Completed), 
				# reset Complete Stuats of itselt and that of All its Component Subject(s) to 0(NotCompleted)
				if(count($SubProgressArr) > 0 && $SubProgressArr['IsCompleted'] == 1){
					if(!empty($CmpSubjectArr)){	
						for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
							$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
							
							# Get any existing record of Component Subject in Marksheet Submission Progress
							if ($SubjectGroupID == '')
								$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
							else
								$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, '', $SubjectGroupID);
							
							if(count($CmpSubmissionProgressArr) > 0){	
								# UPDATE - if having existing records
								if ($SubjectGroupID == '')
									$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 0);
								else
									$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, '', 0, $SubjectGroupID);
							} else {	
								# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
								if ($SubjectGroupID == '')
									$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 0);
								else
									$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, '', 0, $SubjectGroupID);
							}
						}
					}
					# UPDATE - parent subject
					if($isCmpSubject)
						if ($SubjectGroupID == '')
							$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $ReportID, $ClassID, 0);
						else
							$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParentSubjectID, $ReportID, '', 0, $SubjectGroupID);
					else 
						if ($SubjectGroupID == '')
							$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0);
						else
							$result['update_parent_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, '', 0, $SubjectGroupID);
				} else {
					# Set its Complete Status to 0(Not Completed)
					if ($SubjectGroupID == '')
						$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0);
					else
						$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, '', 0, $SubjectGroupID);
				}
			//}
		} 
		# Set to Completed
		else {
			if(!empty($CmpSubjectArr)){	
				for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
					$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
					
					# Get any existing record of Component Subject in Marksheet Submission Progress
					if ($SubjectGroupID == '')
						$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
					else
						$CmpSubmissionProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, '', $SubjectGroupID);
					
					if(count($CmpSubmissionProgressArr) > 0){	
						# UPDATE - if having existing records
						if ($SubjectGroupID == '')
							$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 1);
						else
							$result['update_cmp_complete_'.$j] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, '', 1, $SubjectGroupID);
					} else {	
						# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
						if ($SubjectGroupID == '')
							$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID, 1);
						else
							$result['insert_cmp_complete_'.$j] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, '', 1, $SubjectGroupID);
					}
				}
			}
			
			# Get any existing record of Non-Component Subject in Marksheet Submission Progress
			if ($SubjectGroupID == '')
				$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
			else
				$ProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, '', $SubjectGroupID);
			
			if(count($ProgressArr) > 0){	
				# UPDATE - if having existing records
				if ($SubjectGroupID == '')
					$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 1);
				else
					$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, '', 1, $SubjectGroupID);
			} else {	
				# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
				if ($SubjectGroupID == '')
					$result['insert_complete'] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 1);
				else
					$result['insert_complete'] = $this->INSERT_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, '', 1, $SubjectGroupID);
			}
			
			//$result['update_complete'] = $this->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 1);
		}
		
		$returnVal = (!in_array(false, $result)) ? true : false;
		
		return $returnVal;
	}
	
	# Update the Flag [IsCompleted] to state the status of Marksheet in Marksheet Submission Progress  on 25 Mar 
	function UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ParSubjectID, $ParReportID, $ParClassID, $ParIsCompleted, $ParSubjectGroupID=''){
		$success = false;
		if($ParSubjectID != "" && $ParReportID != "" ){
			
			if ($ParClassID != '')
				$classID_cond = " AND ClassID = '$ParClassID' ";
				
			if ($ParSubjectGroupID != '')
				$subjectGroupID_cond = " AND SubjectGroupID = '$ParSubjectGroupID' ";
			
			$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
			$sql = "
				UPDATE 
					$table 
				SET 
					IsCompleted = $ParIsCompleted, 
					DateModified = NOW() 
				WHERE 
					SubjectID = $ParSubjectID AND 
					ReportID = $ParReportID
					$classID_cond
					$subjectGroupID_cond
			";
			$success = $this->db_db_query($sql);
		}
		return $success;
	}
	
	# Insert a new record for each subject term report of a class in Marksheet Submission Progress on 1 Apr
	function INSERT_MARKSHEET_SUBMISSION_PROGRESS($ParSubjectID, $ParReportID, $ParClassID, $ParIsCompleted, $ParSubjectGroupID=''){
		$success = false;
		if($ParSubjectID != "" && $ParReportID != ""){
			
			if ($ParClassID == '')
				$ParClassID = 'null';
				
			if ($ParSubjectGroupID == '')
				$ParSubjectGroupID = 'null';
			
			$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
			$sql = "
				INSERT INTO $table 
					(TeacherID, SubjectID, ClassID, IsCompleted, 
					 ReportID, SubjectGroupID, DateInput, DateModified) 
				VALUES 
					($this->uid, $ParSubjectID, $ParClassID, $ParIsCompleted, 
					 $ParReportID, $ParSubjectGroupID, NOW(), NOW())
			";
			$success = $this->db_db_query($sql);
		}
		return $success;
	}
	
	/*
	* Get Class Last Modified of Marksheet Submission
	*/
	function GET_CLASS_MARKSHEET_LAST_MODIFIED($ParSubjectID="", $ReportColumnIDList){
		$NameField = getNameFieldByLang2("d.");
		$table = $this->DBName.".RC_MARKSHEET_SCORE";
		$cond_columnID = ($ReportColumnIDList=="")? 1 : " a.ReportColumnID IN ($ReportColumnIDList) ";
		
		$sql = "
			SELECT 
				a.SubjectID, 
				yc.YearClassID, 
				MAX(a.DateModified), 
				$NameField 
			FROM 
				$table AS a 
				LEFT Outer JOIN 
				INTRANET_USER AS b 
				ON b.UserID = a.StudentID
				LEFT Outer JOIN 
				YEAR_CLASS_USER AS ycu 
				ON ycu.UserID = b.UserID
				Left Outer Join
				YEAR_CLASS as yc
				On (ycu.YearClassID = yc.YearClassID And yc.AcademicYearID = '".$this->schoolYearID."')
				LEFT Outer JOIN 
				INTRANET_USER AS d 
				ON d.UserID = a.LastModifiedUserID 
			WHERE 
				$cond_columnID
				".(($ParSubjectID != "") ? "AND a.SubjectID = $ParSubjectID" : "")."
			GROUP BY 
				a.SubjectID, yc.YearClassID 
			ORDER BY 
				a.SubjectID, yc.Sequence
		";
		$row = $this->returnArray($sql, 4);
		
		if(count($row) > 0){
			for($i=0; $i<sizeof($row); $i++){
				list($SubjectID, $ClassID, $LastModifiedDate, $LastModifiedBy) = $row[$i];
				if($ParSubjectID != "")
					$ReturnArray[$ClassID] = array($LastModifiedDate, $LastModifiedBy);
				else 
					$ReturnArray[$SubjectID][$ClassID] = array($LastModifiedDate, $LastModifiedBy);
			}
		}
		
		return $ReturnArray;
	}
	
	function GET_CLASS_MARKSHEET_OVERALL_LAST_MODIFIED($ParSubjectID="", $ReportID){
		$NameField = getNameFieldByLang2("d.");
		$table = $this->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		
		$sql = "
			SELECT 
				a.SubjectID, 
				yc.YearClassID, 
				MAX(a.DateModified), 
				$NameField 
			FROM 
				$table AS a 
				LEFT Outer JOIN 
				INTRANET_USER AS b 
				ON b.UserID = a.StudentID
				LEFT Outer JOIN 
				YEAR_CLASS_USER AS ycu 
				ON ycu.UserID = b.UserID
				Left Outer Join
				YEAR_CLASS as yc
				On (ycu.YearClassID = yc.YearClassID And yc.AcademicYearID = '".$this->schoolYearID."')
				LEFT Outer JOIN 
				INTRANET_USER AS d 
				ON d.UserID = a.LastModifiedUserID 
			WHERE 
				a.ReportID = '".$ReportID."'
				".(($ParSubjectID != "") ? "AND a.SubjectID = $ParSubjectID" : "")."
			GROUP BY 
				a.SubjectID, yc.YearClassID 
			ORDER BY 
				a.SubjectID, yc.Sequence
		";
		$row = $this->returnArray($sql, 4);
		
		if(count($row) > 0){
			for($i=0; $i<sizeof($row); $i++){
				list($SubjectID, $ClassID, $LastModifiedDate, $LastModifiedBy) = $row[$i];
				if($ParSubjectID != "")
					$ReturnArray[$ClassID] = array($LastModifiedDate, $LastModifiedBy);
				else 
					$ReturnArray[$SubjectID][$ClassID] = array($LastModifiedDate, $LastModifiedBy);
			}
		}
		
		return $ReturnArray;
	}
	
	function GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED($ParSubjectID="", $ReportColumnIDList="", $ParSubjectGroupID="", $ParYearTermID='', $ParParentSubjectID=''){
		$NameField = getNameFieldByLang2("teacher.");
		$table = $this->DBName.".RC_MARKSHEET_SCORE";
		
		$cond_columnID = ($ReportColumnIDList=="")? 1 : " ms.ReportColumnID IN ($ReportColumnIDList) ";
		
		### Get Report and Form Info
		$ReportColumnIDArr = explode(',', $ReportColumnIDList);
		$ReportID = $this->returnReportIDByReportColumnID($ReportColumnIDArr[0]);
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$conds_ClassLevelID = '';
		if ($ClassLevelID != '') {
			$conds_ClassLevelID = " And stcyr.YearID = '".$ClassLevelID."' ";
		}
		
		$cond_YearTermID = '';
		if ($ParYearTermID != '') {
			$cond_YearTermID = " And aty.YearTermID = '$ParYearTermID' ";
		}
			
		// 2011-0217-1452-19069: Failed to retrieve Cmp Last Modified Info
		if ($ParParentSubjectID != '') {
			$conds_JoinSubjectID = " And ((ms.SubjectID = '$ParSubjectID' And st.SubjectID = '$ParParentSubjectID') || ms.SubjectID = st.SubjectComponentID) ";
		}
		else {
			$conds_JoinSubjectID = " And (ms.SubjectID = st.SubjectID || ms.SubjectID = st.SubjectComponentID) ";
		}
			
//		$sql = "
//			SELECT 
//				If(st.SubjectComponentID Is Null Or st.SubjectComponentID = 0, a.SubjectID, st.SubjectComponentID) as TargetSubjectID,
//				stcu.SubjectGroupID,
//				MAX(a.DateModified), 
//				$NameField 
//			FROM 
//				$table AS a 
//				LEFT JOIN INTRANET_USER AS b ON 
//				b.UserID = a.StudentID
//				LEFT JOIN SUBJECT_TERM_CLASS_USER AS stcu ON
//				stcu.UserID = b.UserID
//				Left Outer Join SUBJECT_TERM As st On
//				stcu.SubjectGroupID = st.SubjectGroupID $conds_JoinSubjectID
//				Left Outer Join ACADEMIC_YEAR_TERM as aty On
//				st.YearTermID = aty.YearTermID
//				LEFT JOIN INTRANET_USER AS d ON 
//				d.UserID = a.LastModifiedUserID 
//			WHERE 
//				$cond_columnID
//				And aty.AcademicYearID = '".$this->schoolYearID."'
//				".(($ParSubjectID != "") ? "AND a.SubjectID = $ParSubjectID" : "")."
//				".(($ParSubjectGroupID != "") ? "AND stcu.SubjectGroupID = $ParSubjectGroupID" : "")."
//				$cond_YearTermID
//			GROUP BY 
//				TargetSubjectID, stcu.SubjectGroupID 
//			ORDER BY 
//				TargetSubjectID, stcu.SubjectGroupID
//		";

		$sql = "SELECT
				        If(st.SubjectComponentID Is Null Or st.SubjectComponentID = 0, ms.SubjectID, st.SubjectComponentID) as TargetSubjectID,
				        stc.SubjectGroupID,
				        MAX(ms.DateModified),
				        $NameField
				FROM
				        SUBJECT_TERM_CLASS as stc
				        Inner Join SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr On (stc.SubjectGroupID = stcyr.SubjectGroupID)
				        Inner Join SUBJECT_TERM as st On (stc.SubjectGroupID = st.SubjectGroupID)
				        Inner Join ACADEMIC_YEAR_TERM as aty On (st.YearTermID = aty.YearTermID)
				        Inner Join SUBJECT_TERM_CLASS_USER as stcu On (stc.SubjectGroupID = stcu.SubjectGroupID)
				        Inner Join $table as ms On (stcu.UserID = ms.StudentID $conds_JoinSubjectID)
				        Inner Join INTRANET_USER AS teacher ON (ms.LastModifiedUserID = teacher.UserID)
				WHERE
				        $cond_columnID
						$conds_ClassLevelID
						And aty.AcademicYearID = '".$this->schoolYearID."'
						".(($ParSubjectID != "") ? "AND ms.SubjectID = $ParSubjectID" : "")."
						".(($ParSubjectGroupID != "") ? "AND stc.SubjectGroupID = $ParSubjectGroupID" : "")."
						$cond_YearTermID
				GROUP BY
				        TargetSubjectID, stc.SubjectGroupID
				ORDER BY
				        TargetSubjectID, stc.SubjectGroupID
				";
		$row = $this->returnArray($sql, 4);
		
		if(count($row) > 0){
			for($i=0; $i<sizeof($row); $i++){
				list($SubjectID, $SubjectGroupID, $LastModifiedDate, $LastModifiedBy) = $row[$i];
				if($ParSubjectID != "")
					$ReturnArray[$SubjectGroupID] = array($LastModifiedDate, $LastModifiedBy);
				else 
					$ReturnArray[$SubjectID][$SubjectGroupID] = array($LastModifiedDate, $LastModifiedBy);
			}
		}
		
		return $ReturnArray;
	}
	
	# Check Whether it is the Component Subject
	function Load_Component_Subject_List()
	{
		if(empty($this->Component_Subject_List))
		{
			$sql = "
			SELECT 
				RecordID
			FROM 
				ASSESSMENT_SUBJECT 
			WHERE 
				RecordStatus = 1
				AND CMP_CODEID IS NOT NULL 
				AND TRIM(CMP_CODEID) <> ''";
				
			$this->Component_Subject_List = $this->returnVector($sql, 1);	
		}
	}
	
	function CHECK_SUBJECT_IS_COMPONENT_SUBJECT($ParSubjectID){
		
//		$returnVal = 0;
//		if($ParSubjectID != ""){
//			$sql = "SELECT 
//						CMP_CODEID 
//					FROM 
//						ASSESSMENT_SUBJECT 
//					WHERE 
//						RecordID = $ParSubjectID
//						AND
//						RecordStatus = 1
//					";
//			$returnArr = $this->returnVector($sql, 1);
//			if(count($returnArr) > 0){
//				if($returnArr[0] != NULL && $returnArr[0] != "")
//					$returnVal = 1;
//			}
//		}
		$this->Load_Component_Subject_List();
		$returnVal = in_array($ParSubjectID, $this->Component_Subject_List);
		return $returnVal;
	}
	
	# Get Component SubjectID in the report by parent SubjectID
	function GET_COMPONENT_SUBJECT($ParSubjectIDArr, $ParClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID='', $ReturnAsso=0)
	{
		$PreloadArrKey = 'GET_COMPONENT_SUBJECT';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		switch($ParDisplayType)
		{
			case "Desc": $DisplayForm = "DES"; break;
			case "Abbr": $DisplayForm = "ABBR"; break;
			case "ShortName": $DisplayForm = "SNAME"; break;
			default: $DisplayForm = "DES";
		}
		$chiName = "b.CH_".$DisplayForm;
		$engName = "b.EN_".$DisplayForm; 
		if ($ParLang == '' || $ParLang == 'both')
			$SubjectNameField = "CONCAT($engName, ' (', $chiName, ')')";
		else if ($ParLang == 'en')
			$SubjectNameField = $engName;
		else
			$SubjectNameField = $chiName;
			
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$returnArr = array();
		
		if($ParSubjectIDArr != "" && $ParClassLevelID != ""){
			/*$sql = "SELECT
						DISTINCT(b.RecordID) SubjectID,
						$SubjectNameField SubjectName 
					FROM
						ASSESSMENT_SUBJECT as a
						LEFT JOIN ASSESSMENT_SUBJECT as b ON 
						a.CODEID = b.CODEID AND 
						b.CMP_CODEID IS NOT NULL 
						INNER JOIN $table as c ON 
						c.SubjectID = b.RecordID
					WHERE
						a.RecordID = $ParSubjectID 
						AND 
						a.RecordID != b.RecordID
						AND
						c.ClassLevelID = $ParClassLevelID 
						AND
						a.RecordStatus = 1 
						AND
						b.RecordStatus = 1
					Order By
						c.DisplayOrder
					";*/
					
			# Modified by Marcus 20101011 (for template based grading scheme) 
			if(trim($ReportID)!='')
				$cond_ReportID = " AND c.ReportID = $ReportID ";
			else
				$cond_ReportID = " AND c.ReportID = 0 ";

			$sql = "
				SELECT 
					b.RecordID SubjectID,
					$SubjectNameField  SubjectName,
					a.RecordID ParentSubjectID
				FROM 
					ASSESSMENT_SUBJECT a 
					INNER JOIN ASSESSMENT_SUBJECT b ON 
						a.CODEID = b.CODEID AND 
						(a.CMP_CODEID IS NULL OR a.CMP_CODEID ='') AND 
						(b.CMP_CODEID IS NOT NULL AND b.CMP_CODEID <> '') AND
						a.RecordStatus =1 AND
						b.RecordStatus = 1
					INNER JOIN $table as c ON 
						c.SubjectID = b.RecordID
				WHERE
					c.ClassLevelID = $ParClassLevelID
					$cond_ReportID
					AND a.RecordID In (".implode(',', (array)$ParSubjectIDArr).")
				Group By
					b.RecordID
				Order By
					c.DisplayOrder
			";						
					
			$dataArr = $this->returnArray($sql);
			
			if ($ReturnAsso == 1) {
				$returnArr = BuildMultiKeyAssoc($dataArr, 'ParentSubjectID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			}
			else {
				$returnArr = $dataArr;
			}
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	function Convert_MarksArr_Mark_To_Grade($ReportID,$MarksAry=array())
	{
		global $eRCTemplateSetting;
		
		if(empty($MarksAry))
		$MarksAry = $this->getMarks($ReportID);
//		debug_pr($MarksAry);
		
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportBasicInfo["ClassLevelID"];
		# Modified by Marcus 20101011 (for template based grading scheme)
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);
		
		
		foreach(($MarksAry) as $StudentID => $SubjectColumnAry)
		{
			foreach(($SubjectColumnAry) as $SubjectID => $ColumnAry)
			{
				if($subjectList[$SubjectID]["scaleInput"]== 'G') //skip converting if ScaleInput is Grade
					continue;
				
				$schemeID = $subjectList[$SubjectID]["schemeID"];
				if(!isset($schemeInfoAry[$schemeID]))
				{
					$schemeInfoAry[$schemeID] = $this->GET_GRADING_SCHEME_INFO($schemeID);
					$RangeInfoAry[$schemeID] = $this->GET_GRADING_SCHEME_RANGE_INFO($schemeID);
				}
				
				$SchemeInfo = $schemeInfoAry[$schemeID][0];
				$RangeInfo = $RangeInfoAry[$schemeID];
				
				if(count($RangeInfo) == 0) continue;
				
				foreach((array)($ColumnAry) as $ReportColumnID => $thisMarksAry)
				{
					$Mark = $thisMarksAry["Mark"];
					$Grade = $thisMarksAry["Grade"];

					// case for Mark Range
					if($SchemeInfo['TopPercentage'] == 0)
					{	
					
						if (in_array($Grade,$this->Get_Exclude_Ranking_Special_Case()))
						{
							$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] = 'N.A.';
							continue;
						}
					
						$isStart = 1;
						for($i=0 ; $i<count($RangeInfo) ; $i++){
							
							$j = $i - 1;
							if($RangeInfo[$i]['LowerLimit'] != ""){
//								if( ($isStart == 1 && (float)$Mark >= (float)$RangeInfo[$i]['LowerLimit'] && (float)$Mark <= (float)$SchemeInfo['FullMark']) || 
//									($isStart == 0 && (float)$Mark >= (float)$RangeInfo[$i]['LowerLimit'] && (float)$Mark < $RangeInfo[$j]['LowerLimit']) ){
							if( ($isStart == 1 && floatcmp((float)$Mark, ">=", (float)$RangeInfo[$i]['LowerLimit']) && floatcmp((float)$Mark, "<=", (float)$SchemeInfo['FullMark'])) || 
								($isStart == 0 && floatcmp((float)$Mark, ">=", (float)$RangeInfo[$i]['LowerLimit']) && floatcmp((float)$Mark, "<", (float)$RangeInfo[$j]['LowerLimit'])) ){
										if($isStart == 1) $isStart = 0;
										$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] = $RangeInfo[$i]['Grade'];
										continue;
								}
							}
						}
					} // end if($SchemeInfo['TopPercentage'] == 0)
					else if ($SchemeInfo['TopPercentage'] == 1)
					{	// case for Percentage Range for all students
						# get scheme info
						$PassMark = $SchemeInfo['PassMark'];
						
						$LastPassingGrade = '';
						$TmpLastPassingGrade = '';
						foreach ((array)$RangeInfo as $key => $value)
						{
							
							if ($eRCTemplateSetting['HigherThanPassingMarkThenIsPassed'] == true)
							{
								### Record the last Passing Grade
								$thisNature = $RangeInfo[$key]['Nature'];
								
								if ($thisNature=='F' && $LastPassingGrade=='')
									$LastPassingGrade = $TmpLastPassingGrade;
									
								$TmpLastPassingGrade = $RangeInfo[$key]['Grade'];
							}
						}
											
						$tempArr = 	$MarksAry[$StudentID][$SubjectID][$ReportColumnID];
						
						$FormPosition = $tempArr["OrderMeritForm"];
						$FormTotalStudent = $tempArr["FormNoOfStudent"];
						$StudentMark = $tempArr["Mark"];
						$StudentGrade = $tempArr["Grade"];
	
						//if ($StudentGrade == 'N.A.')
						if (in_array($StudentGrade,$this->Get_Exclude_Ranking_Special_Case()))
						{
							$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] = 'N.A.';
							continue;
						}
																					
						# if position is less than zero, return the original marks
						if ($FormPosition <= 0) 
						{
							$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] =  $Mark;
							continue;
						}
						
						$FullMark = $SchemeInfo['FullMark'];
						
						$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] = $this->CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS($Mark,$FormPosition,$FullMark,$FormTotalStudent,$RangeInfo,$LastPassingGrade,$PassMark,$SubjectID);
						
					} // end if ($SchemeInfo['TopPercentage'] == 1)
					else if ($SchemeInfo['TopPercentage'] == 2)
					{	// case for Percentage Range for passed students
					
						# get the mark, poistion in Form and total number of students in the Form
						$tempArr = $MarksAry[$StudentID][$SubjectID][$ReportColumnID];
						$FormPosition = $tempArr["OrderMeritForm"];
						$FormTotalStudent = $tempArr["FormNoOfStudent"];
						$Mark = $tempArr["Mark"];
						$StudentGrade = $tempArr["Grade"];
						
						//if ($StudentGrade == 'N.A.')
						if (in_array($StudentGrade,$this->Get_Exclude_Ranking_Special_Case()))
						{
							$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] =  $StudentGrade;
						}
						
						# if position is less than zero, return the original marks
						if ($FormPosition <= 0) 
						{
							$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] =  $Mark;
							continue;
						}
						
						# get number of passing student
						$PassMark = $SchemeInfo['PassMark'];
						
						# get all student of the same class level
						$StudentArr = array_keys($MarksAry);
						$studentIDListSql = implode(",", $StudentArr);

						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$fieldToSelect = "COUNT(IF(Mark >= $PassMark,1,NULL)),COUNT(IF(Mark < $PassMark,1,NULL)) ";
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID IN ($studentIDListSql) ";
						$sql .= "AND SubjectID = '$SubjectID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$sql .= "AND OrderMeritForm <> -1";
						
						$resultArr = $this->returnArray($sql);

						$NumPassStudent = $resultArr[0][0];
						$NumFailStudent = $resultArr[0][1];
						
						# calculate student percentage
						$FilteredRangeInfo = array();
						$RangeInfoKeyArr = array();
						foreach ($RangeInfo as $key => $thisRangeInfo)
						{
							if ($thisRangeInfo['Nature'] == "D" || $thisRangeInfo['Nature'] == "P")
								$PassRangeInfo[] = $thisRangeInfo;
							else if ($thisRangeInfo['Nature'] == "F")
								$FailRangeInfo[] = $thisRangeInfo;
						}
						
//						if ((float)$Mark >= (float)$PassMark)
						if (floatcmp((float)$Mark, ">=", (float)$PassMark))
						{
							$FilteredRangeInfo = $PassRangeInfo;
							$FormTotalStudent = $NumPassStudent;												
						}
						else
						{
							$FilteredRangeInfo = $FailRangeInfo;
							$FormTotalStudent = $NumFailStudent;
							$FormPosition = $FormPosition - $NumPassStudent;
						}
	
						$FullMark = $SchemeInfo['FullMark'];
						
						$MarksAry[$StudentID][$SubjectID][$ReportColumnID]["Grade"] = $this->CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS($Mark,$FormPosition,$FullMark,$FormTotalStudent,$FilteredRangeInfo);
						
					} // end $TopPercentage == 2
					
				} // end loop column
				
			} // end loop subject 
			
		}// end loop user
		
		return $MarksAry;
	}
	
	/*********************************************************
	 * If you need to modify the logic of this function, 
	 * please check whether you need to modify Convert_MarksArr_Mark_To_Grade also
	 * *******************************************************/
	function CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $Mark, $ReportID="", $StudentID="", $SubjectID="", $ClassLevelID="", $ReportColumnID="", $Grade="", $returnGradePoint=0){
		
		
		global $eRCTemplateSetting;

//		$Mark =  (float) $Mark;
		$returnVal = '';
		$RangeInfo = array();
		$SchemeInfo = array();
		$GradeRange = array();
		$Returnfield = $returnGradePoint==1?"GradePoint":"Grade";
		
		if($SchemeID != "" && $SchemeID != null){
			
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$RangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
		
			if(count($RangeInfo) > 0){
				$isStart = 1;
				if($SchemeInfo['TopPercentage'] == 0)
				{	// case for Mark Range
					//if ($Grade == 'N.A.') {
					if ($this->Check_If_Grade_Is_SpecialCase($Grade)) {
						return $Grade;
					}
				
					for($i=0 ; $i<count($RangeInfo) ; $i++){
						
						$j = $i - 1;
						if($RangeInfo[$i]['LowerLimit'] != ""){

//							if( ($isStart == 1 && (float)(string)$Mark >= (float)(string)$RangeInfo[$i]['LowerLimit'] && (float)(string)$Mark <= (float)(string)$SchemeInfo['FullMark']) || 
//								($isStart == 0 && (float)(string)$Mark >= (float)(string)$RangeInfo[$i]['LowerLimit'] && (float)(string)$Mark < (float)(string)$RangeInfo[$j]['LowerLimit']) ){
							if( ($isStart == 1 && floatcmp((float)$Mark, ">=", (float)$RangeInfo[$i]['LowerLimit']) && floatcmp((float)$Mark, "<=", (float)$SchemeInfo['FullMark'])) || 
								($isStart == 0 && floatcmp((float)$Mark, ">=", (float)$RangeInfo[$i]['LowerLimit']) && floatcmp((float)$Mark, "<", (float)$RangeInfo[$j]['LowerLimit'])) ){
									if($isStart == 1) $isStart = 0;
									$returnVal = $RangeInfo[$i][$Returnfield];
									continue;
							}
						}
					}
				} 
				else if ($SchemeInfo['TopPercentage'] == 1)
				{	// case for Percentage Range for all students
					# get scheme info
					$PassMark = $SchemeInfo['PassMark'];
					
					$LastPassingGrade = '';
					$TmpLastPassingGrade = '';
					foreach ($RangeInfo as $key => $value)
					{
						if ($eRCTemplateSetting['HigherThanPassingMarkThenIsPassed'] == true)
						{
							### Record the last Passing Grade
							$thisNature = $RangeInfo[$key]['Nature'];
							
							if ($thisNature=='F' && $LastPassingGrade=='')
								$LastPassingGrade = $TmpLastPassingGrade;
								
							$TmpLastPassingGrade = $RangeInfo[$key]['Grade'];
						}
					}
										
					# get the poistion in Form and total number of students in the Form

					if($SubjectID < 0)
					{
						switch($SubjectID)
						{
							case -1: $GrandMark = "GrandAverage"; break;
							case -2: $GrandMark = "GrandTotal"; break;
							case -3: $GrandMark = "GrandGPA"; break;
						}
						
						$table = $this->DBName.".RC_REPORT_RESULT";
						$fieldToSelect = "OrderMeritForm, FormNoOfStudent, $GrandMark ";
						
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID = '$StudentID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$tempArr = $this->returnArray($sql,2);
						
					}
					else
					{
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$fieldToSelect = "OrderMeritForm, FormNoOfStudent, Mark, Grade ";
						
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID = '$StudentID' ";
						$sql .= "AND SubjectID = '$SubjectID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$tempArr = $this->returnArray($sql,2);
					}
					$FormPosition = $tempArr[0][0];
					$FormTotalStudent = $tempArr[0][1];
					$StudentMark = $tempArr[0][2];
					$StudentGrade = $tempArr[0][3];

					//if ($StudentGrade == 'N.A.') {
					if ($this->Check_If_Grade_Is_SpecialCase($StudentGrade)) {
						//return 'N.A.';
						return $StudentGrade;
					}
																				
					# if position is less than zero, return the original marks
					if ($FormPosition <= 0) 
					{
						return $Mark;
					}
					
					$FullMark = $SchemeInfo['FullMark'];
					
					$returnVal = $this->CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS($Mark,$FormPosition,$FullMark,$FormTotalStudent,$RangeInfo,$LastPassingGrade,$PassMark,$SubjectID,$returnGradePoint);
	
				}
				else if ($SchemeInfo['TopPercentage'] == 2)
				{	// case for Percentage Range for passed students
				
					# get the mark, poistion in Form and total number of students in the Form
					if($SubjectID < 0)
					{
						$table = $this->DBName.".RC_REPORT_RESULT";
						$fieldToSelect = "OrderMeritForm, FormNoOfStudent ";
						
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID = '$StudentID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$tempArr = $this->returnArray($sql,2);
						
					}
					else
					{
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$fieldToSelect = "OrderMeritForm, FormNoOfStudent, Mark, Grade ";
						
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID = '$StudentID' ";
						$sql .= "AND SubjectID = '$SubjectID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
					}
					$tempArr = $this->returnArray($sql,3);
					$FormPosition = $tempArr[0][0];
					$FormTotalStudent = $tempArr[0][1];
					
					if ($SubjectID > 0)
					{
						$Mark = $tempArr[0][2];
						$StudentGrade = $tempArr[0][3];
					}
					
					//if ($StudentGrade == 'N.A.') {
					if ($this->Check_If_Grade_Is_SpecialCase($StudentGrade)) {
						//return 'N.A.';
						return $StudentGrade;
					}
					
					# if position is less than zero, return the original marks
					if ($FormPosition <= 0) 
					{
						return $Mark;
					}
					
					# get number of passing student
					$PassMark = $SchemeInfo['PassMark'];
					
					# get all student of the same class level
					$StudentArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
					$StudentIDArr = array();
					for($i=0 ; $i<count($StudentArr) ; $i++)
					{
						$StudentIDArr[] = $StudentArr[$i][0];
					}
					$studentIDListSql = implode(",", $StudentIDArr);
					if($SubjectID < 0)
					{
						switch($SubjectID)
						{
							case -1: $GrandMark = "GrandAverage"; break;
							case -2: $GrandMark = "GrandTotal"; break;
							case -3: $GrandMark = "GrandGPA"; break;
						}
						$table = $this->DBName.".RC_REPORT_RESULT";
						$fieldToSelect = "COUNT(*) ";
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID IN ($studentIDListSql) ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$sql .= "AND Round($GrandMark, 5) >= Round(".$PassMark.", 5)";
						$sql .= "AND OrderMeritForm <> -1";
					}
					else
					{
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$fieldToSelect = "COUNT(*) ";
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID IN ($studentIDListSql) ";
						$sql .= "AND SubjectID = '$SubjectID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$sql .= "AND Round(Mark, 5) >= Round(".$PassMark.", 5)";
						$sql .= "AND OrderMeritForm <> -1";
					}
					$tempArr = $this->returnVector($sql);
					$NumPassStudent = $tempArr[0];
					# get number of fail student
					$studentIDListSql = implode(",", $StudentIDArr);
					if($SubjectID < 0)
					{
						switch($SubjectID)
						{
							case -1: $GrandMark = "GrandAverage"; break;
							case -2: $GrandMark = "GrandTotal"; break;
							case -3: $GrandMark = "GrandGPA"; break;
						}
						$table = $this->DBName.".RC_REPORT_RESULT";
						$fieldToSelect = "COUNT(*) ";
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID IN ($studentIDListSql) ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$sql .= "AND Round($GrandMark, 5) < Round(".$PassMark.", 5)";
						$sql .= "AND OrderMeritForm <> -1";
						
					}
					else
					{
						$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
						$fieldToSelect = "COUNT(*) ";
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID IN ($studentIDListSql) ";
						$sql .= "AND SubjectID = '$SubjectID' ";
						$sql .= "AND ReportColumnID = '$ReportColumnID' ";
						$sql .= "AND Round(Mark, 5) < Round(".$PassMark.", 5)";
						$sql .= "AND OrderMeritForm <> -1";
					}
					$tempArr = $this->returnVector($sql);
					$NumFailStudent = $tempArr[0];
					
					# calculate student percentage
					$FilteredRangeInfo = array();
					$RangeInfoKeyArr = array();
					foreach ($RangeInfo as $key => $thisRangeInfo)
					{
						if ($thisRangeInfo['Nature'] == "D" || $thisRangeInfo['Nature'] == "P")
							$PassRangeInfo[] = $thisRangeInfo;
						else if ($thisRangeInfo['Nature'] == "F")
							$FailRangeInfo[] = $thisRangeInfo;
					}
					
//					if ((float)$Mark >= (float)$PassMark)
					if (floatcmp((float)$Mark, ">=", (float)$PassMark))
					{
						$FilteredRangeInfo = $PassRangeInfo;
						$FormTotalStudent = $NumPassStudent;												
					}
					else
					{
						$FilteredRangeInfo = $FailRangeInfo;
						$FormTotalStudent = $NumFailStudent;
						$FormPosition = $FormPosition - $NumPassStudent;
					}

					$FullMark = $SchemeInfo['FullMark'];
					
					$returnVal = $this->CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS($Mark,$FormPosition,$FullMark,$FormTotalStudent,$FilteredRangeInfo,$returnGradePoint);

				}

			}
		}

		return $returnVal;
	}
	
	function CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_2_PROCESS($Mark,$FormPosition,$FullMark,$FormTotalStudent,$FilteredRangeInfo,$returnGradePoint=0)
	{
		global $eRCTemplateSetting;
		
		$ReturnField = $returnGradePoint==1?"GradePoint":"Grade";
		if($eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup']===true)
		{
			$StudentCount = 0;
			for($i=0 ; $i<count($FilteredRangeInfo) ; $i++)
			{
				$UpperLimit = $FilteredRangeInfo[$i]['UpperLimit'];
				if ($UpperLimit == '') {
					continue;
				}
				
				$GradeQuota = ceil($UpperLimit*$FormTotalStudent/100); 
				$StudentCount += $GradeQuota;
				
				if($eRCTemplateSetting['AdditionalCriteria'])
					$ACLowerMarkLimit = $FullMark*$FilteredRangeInfo[$i]['AdditionalCriteria']/100;
				
//				if ($FormPosition> 0 && $FormPosition <= $StudentCount && (float)$Mark >= (float)$ACLowerMarkLimit)
				if ($FormPosition> 0 && $FormPosition <= $StudentCount && floatcmp((float)$Mark, ">=", (float)$ACLowerMarkLimit))
				{
					$returnVal = $FilteredRangeInfo[$i][$ReturnField];
					break;
				}
			}
		}
		else
		{	
			if($eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup2']===true)
				$FormPosition = $FormPosition - 1;
			
			if ($FormTotalStudent == 0) {
				$FormPercentage = 0;
			}
			else {
				$FormPercentage = ($FormPosition / $FormTotalStudent) * 100;
			}
					
			for($i=0 ; $i<count($FilteredRangeInfo) ; $i++)
			{
				$UpperLimit = $FilteredRangeInfo[$i]['UpperLimit'];
				
				if ($UpperLimit == '') {
					continue;
				}
				
				if($eRCTemplateSetting['AdditionalCriteria'])
					$ACLowerMarkLimit = $FullMark*$FilteredRangeInfo[$i]['AdditionalCriteria']/100;
				
//				if ($FormPercentage <= $UpperLimit && (float)$Mark >= (float)$ACLowerMarkLimit)
				if ($FormPercentage <= $UpperLimit && floatcmp((float)$Mark, ">=", (float)$ACLowerMarkLimit))
				{
					$returnVal = $FilteredRangeInfo[$i][$ReturnField];
					break;
				}
				else
				{
					$FormPercentage -= $UpperLimit;
				}
			}
		}
		
		return $returnVal;
	}
	
	function CONVERT_MARK_TO_GRADE_TOP_PERCENTAGE_1_PROCESS($Mark,$FormPosition,$FullMark,$FormTotalStudent,$RangeInfo,$LastPassingGrade,$PassMark,$SubjectID='',$returnGradePoint=0)
	{
		global $eRCTemplateSetting;
		
		$ReturnField = $returnGradePoint==1?"GradePoint":"Grade";
		
		if($eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup']===true)
		{
			$StudentCount = 0;
			for($i=0 ; $i<count($RangeInfo) ; $i++)
			{
				$UpperLimit = $RangeInfo[$i]['UpperLimit'];
				
				if ($UpperLimit == '') {
					continue;
				}
				
				$GradeQuota = ceil($UpperLimit*$FormTotalStudent/100); 
				$StudentCount += $GradeQuota;
				
				if($eRCTemplateSetting['AdditionalCriteria'])
					$ACLowerMarkLimit = $FullMark*$RangeInfo[$i]['AdditionalCriteria']/100;
				
				
//				if ($FormPosition> 0 && $FormPosition <= $StudentCount && (float)$Mark >= (float)$ACLowerMarkLimit)
				if ($FormPosition> 0 && $FormPosition <= $StudentCount && floatcmp((float)$Mark, ">=", (float)$ACLowerMarkLimit))
				{
//					if ($eRCTemplateSetting['HigherThanPassingMarkThenIsPassed'] == true && (float)$Mark >= (float)$PassMark && $RangeInfo[$i]['Nature']=='F')
					if ($eRCTemplateSetting['HigherThanPassingMarkThenIsPassed'] == true && floatcmp((float)$Mark, ">=", (float)$PassMark) && $RangeInfo[$i]['Nature']=='F')
					{
						$returnVal = $LastPassingGrade;
					}
					else
					{
						$returnVal = $RangeInfo[$i][$ReturnField];
					}
					break;
				}
			}
		}
		else
		{
			if($eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup2']===true)
				$FormPosition = $FormPosition - 1;

			# calculate Form percentage of student
			if ($FormTotalStudent == 0) {
				$FormPercentage = 0;
			}
			else {
				$FormPercentage = ($FormPosition / $FormTotalStudent) * 100;
			}

			for($i=0 ; $i<count($RangeInfo) ; $i++)
			{
				$UpperLimit = $RangeInfo[$i]['UpperLimit'];
				
				if ($UpperLimit == '') {
					continue;
				}
				
				if($eRCTemplateSetting['AdditionalCriteria']&&$SubjectID>0)
					$ACLowerMarkLimit = $FullMark*$RangeInfo[$i]['AdditionalCriteria']/100;

//				if ($FormPercentage <= $UpperLimit && (float)$Mark>=(float)$ACLowerMarkLimit)
				if ($FormPercentage <= $UpperLimit && floatcmp((float)$Mark, ">=", (float)$ACLowerMarkLimit))
				{
//					if ($eRCTemplateSetting['HigherThanPassingMarkThenIsPassed'] == true && (float)$Mark >= (float)$PassMark && $RangeInfo[$i]['Nature']=='F')
					if ($eRCTemplateSetting['HigherThanPassingMarkThenIsPassed'] == true && floatcmp((float)$Mark, ">=", (float)$PassMark) && $RangeInfo[$i]['Nature']=='F')
						$returnVal = $LastPassingGrade;
					else
						$returnVal = $RangeInfo[$i][$ReturnField];

					break;
				}
				else
				{
					$FormPercentage -= $UpperLimit;
				}
			}
		}
		
		return $returnVal;
	}
	
	
	function CONVERT_MARK_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $Mark, $ReportID="", $StudentID="", $SubjectID="", $ClassLevelID="", $ReportColumnID="", $Grade="")
	{
		return $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $Mark, $ReportID, $StudentID, $SubjectID, $ClassLevelID, $ReportColumnID, $Grade, 1);
	}
	/*
	* Convert Grade Point from mark based on the Range of Mark setting in Grading Scheme
	*/
//	function CONVERT_MARK_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $Mark, $ReportID="", $StudentID="", $SubjectID="", $ClassLevelID="", $ReportColumnID="", $debug=0){
//		
//		global $eRCTemplateSetting;
//		
//		$returnVal = '';
//		$RangeInfo = array();
//		$SchemeInfo = array();
//		$GradeRange = array();
//		
//		if($SchemeID != "" && $SchemeID != null){
//			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
//			$RangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
//			if(count($RangeInfo) > 0){
//				$isStart = 1;
//				if($SchemeInfo['TopPercentage'] == 0) {	// case for Mark Range
//					for($i=0 ; $i<count($RangeInfo) ; $i++){
//						$j = $i - 1;
//						if($RangeInfo[$i]['LowerLimit'] != ""){
//							if( ($isStart == 1 && $Mark >= $RangeInfo[$i]['LowerLimit'] && $Mark <= $SchemeInfo['FullMark']) || 
//								($isStart == 0 && $Mark >= $RangeInfo[$i]['LowerLimit'] && $Mark < $RangeInfo[$j]['LowerLimit']) ){
//									if($isStart == 1) $isStart = 0;
//									$returnVal = $RangeInfo[$i]['GradePoint'];
//									continue;
//							}
//						}
//					}
//				}
//				else if ($SchemeInfo['TopPercentage'] == 1)
//				{	// case for Percentage Range for all students
//					
//					# get scheme info
//					$UpperLimitArr = array();
//					foreach ($RangeInfo as $key => $value)
//					{
//						$UpperLimitArr[] = $RangeInfo[$key]['UpperLimit'];
//					}
//										
//					# get the poistion in Form and total number of students in the Form
//					$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
//					$fieldToSelect = "OrderMeritForm, FormNoOfStudent ";
//					
//					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
//					$sql .= "ReportID = '$ReportID' ";
//					$sql .= "AND StudentID = '$StudentID' ";
//					$sql .= "AND SubjectID = '$SubjectID' ";
//					$sql .= "AND ReportColumnID = '$ReportColumnID' ";
//					$tempArr = $this->returnArray($sql,2);
//					$FormPosition = $tempArr[0][0];
//					$FormTotalStudent = $tempArr[0][1];
//					
//					# if position is less than zero, return the original marks
//					if ($FormPosition <= 0) 
//					{
//						return $Mark;
//					}
//					
//					# calculate Form percentage of student
//					$FormPercentage = ($FormPosition / $FormTotalStudent) * 100;
//									
//					for($i=0 ; $i<count($UpperLimitArr) ; $i++)
//					{
//						if($eRCTemplateSetting['AdditionalCriteria'])
//							$ACLowerMarkLimit = $SchemeInfo['FullMark']*$RangeInfo[$i]['AdditionalCriteria']/100;
//						
//						if ($FormPercentage <= $UpperLimitArr[$i] && $Mark >= $ACLowerMarkLimit)
//						{
//							/* Need not style in displaying rank
//							switch ($RangeInfo[$i]['Nature'])
//							{
//								case "D":
//									$thisNature = "Distinction";
//								  	break;
//								case "P":
//								  	$thisNature = "Pass";
//								  	break;
//								case "F":
//								  	$thisNature = "Fail";
//								  	break;
//						  	}
//
//							$thisMarkDisplay = $this->ReturnTextwithStyle($RangeInfo[$i]['Grade'], 'HighLight', $thisNature);
//							*/
//							$returnVal = $RangeInfo[$i]['GradePoint'];
//							break;
//						}
//						else
//						{
//							$FormPercentage -= $UpperLimitArr[$i];
//						}
//					}
//					
//				}
//				else if ($SchemeInfo['TopPercentage'] == 2)
//				{	// case for Percentage Range for passed students
//																	
//					# get the mark, poistion in Form and total number of students in the Form
//					$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
//					$fieldToSelect = "OrderMeritForm, FormNoOfStudent, Mark ";
//					
//					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
//					$sql .= "ReportID = '$ReportID' ";
//					$sql .= "AND StudentID = '$StudentID' ";
//					$sql .= "AND SubjectID = '$SubjectID' ";
//					$sql .= "AND ReportColumnID = '$ReportColumnID' ";
//					$tempArr = $this->returnArray($sql,3);
//					$FormPosition = $tempArr[0][0];
//					$FormTotalStudent = $tempArr[0][1];
//					$Mark = $tempArr[0][2];
//															
//					# if position is less than zero, return the original marks
//					if ($FormPosition <= 0) 
//					{
//						return $Mark;
//					}
//					
//					# get number of passing student
//					$PassMark = $SchemeInfo['PassMark'];
//					# get all student of the same class level
//					$StudentArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
//					$StudentIDArr = array();
//					for($i=0 ; $i<count($StudentArr) ; $i++)
//					{
//						$StudentIDArr[] = $StudentArr[$i][0];
//					}
//					$studentIDListSql = implode(",", $StudentIDArr);
//					$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
//					$fieldToSelect = "COUNT(*) ";
//					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
//					$sql .= "ReportID = '$ReportID' ";
//					$sql .= "AND StudentID IN ($studentIDListSql) ";
//					$sql .= "AND SubjectID = '$SubjectID' ";
//					$sql .= "AND ReportColumnID = '$ReportColumnID' ";
//					$sql .= "AND Mark >= $PassMark";
//					$tempArr = $this->returnVector($sql);
//					$NumPassStudent = $tempArr[0];
//					
//					# get number of fail student
//					$studentIDListSql = implode(",", $StudentIDArr);
//					$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
//					$fieldToSelect = "COUNT(*) ";
//					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
//					$sql .= "ReportID = '$ReportID' ";
//					$sql .= "AND StudentID IN ($studentIDListSql) ";
//					$sql .= "AND SubjectID = '$SubjectID' ";
//					$sql .= "AND ReportColumnID = '$ReportColumnID' ";
//					$sql .= "AND Mark < $PassMark";
//					$tempArr = $this->returnVector($sql);
//					$NumFailStudent = $tempArr[0];
//					
//					# calculate student percentage
//					$UpperLimitArr = array();
//					$RangeInfoKeyArr = array();
//					if ($Mark >= $PassMark)
//					{
//						# Pass
//						# get scheme info
//						foreach ($RangeInfo as $key => $value)
//						{
//							if ($RangeInfo[$key]['Nature'] == "D" || $RangeInfo[$key]['Nature'] == "P")
//							{
//								$UpperLimitArr[] = $RangeInfo[$key]['UpperLimit'];
//								$RangeInfoKeyArr[] = $key;
//							}
//						}
//						
//						$FormTotalStudent = $NumPassStudent;												
//					}
//					else
//					{
//						# Fail	
//						# get scheme info						
//						foreach ($RangeInfo as $key => $value)
//						{
//							if ($RangeInfo[$key]['Nature'] == "F")
//							{
//								$UpperLimitArr[] = $RangeInfo[$key]['UpperLimit'];
//								$RangeInfoKeyArr[] = $key;
//							}
//						}
//						
//						$FormTotalStudent = $NumFailStudent;
//						$FormPosition = $FormPosition - $NumPassStudent;
//					}
//					
//					$FormPercentage = ($FormPosition / $FormTotalStudent) * 100;
//											
//					for($i=0 ; $i<count($UpperLimitArr) ; $i++)
//					{
//						if($eRCTemplateSetting['AdditionalCriteria'])
//							$ACLowerMarkLimit = $SchemeInfo['FullMark']*$RangeInfo[$RangeInfoKeyArr[$i]]['AdditionalCriteria']/100;
//						
//						
//						if ($FormPercentage <= $UpperLimitArr[$i] && $Mark >= $ACLowerMarkLimit)
//						{
//							/* Need not style in displaying rank
//							switch ($RangeInfo[$i]['Nature'])
//							{
//								case "D":
//									$thisNature = "Distinction";
//								  	break;
//								case "P":
//								  	$thisNature = "Pass";
//								  	break;
//								case "F":
//								  	$thisNature = "Fail";
//								  	break;
//						  	}
//
//							$thisMarkDisplay = $this->ReturnTextwithStyle($RangeInfo[$i]['Grade'], 'HighLight', $thisNature);
//							*/
//							$returnVal = $RangeInfo[$RangeInfoKeyArr[$i]]['GradePoint'];
//							break;
//						}
//						else
//						{
//							$FormPercentage -= $UpperLimitArr[$i];
//						}
//					}
//				}
//			}
//		}
//		
//		return $returnVal;
//	}
	
	# Updated version by Jason
	function CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $Mark, $CalculationType=1, $AllColumnMarkArr=array()){
		$returnVal = '';
		$ReportInfo = array();
		$ReportWeightInfo = array();
		$WeightArr = array();
		$WeightTotal = 0;
		
		# Get Absent&Exempt Settings
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		$isCmpSubject = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$column_data = $this->returnReportTemplateColumnData($ReportID); 		// Column Data
		
		# Get Report Column Weight
		if($CalculationType == 2){
			if(count($ReportInfo) > 0){
				$OtherCondition = "SubjectID = $SubjectID AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
				$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
				
				$WeightArr[$ReportWeightInfo[0]['SubjectID']] = $ReportWeightInfo[0]['Weight'];
				
				$WeightTotal += $ReportWeightInfo[0]['Weight'];
			}
		} else {
			//$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$SubjectID);
			
			if ($SubjectID=='')
				$conds = "(SubjectID Is Null OR SubjectID = '')";
			else
				$conds = "SubjectID = '".$SubjectID."'";
			
			$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $conds);
				
			# Subject Weight Array
			if(count($ReportWeightInfo) > 0){
				for($i=0 ; $i<sizeof($ReportWeightInfo) ; $i++){
					$WeightArr[$ReportWeightInfo[$i]['ReportColumnID']] = $ReportWeightInfo[$i]['Weight'];
					
					if($ReportWeightInfo[$i]['ReportColumnID'] != ""){
						# Check for any weight will not be used to as a Total Weight
						$isCountWeight = 1;
						$isCountFullMark = 1;
						if(count($AllColumnMarkArr) > 0 && $AllColumnMarkArr[$ReportWeightInfo[$i]['ReportColumnID']] != ""){
							$tmpVal = $AllColumnMarkArr[$ReportWeightInfo[$i]['ReportColumnID']];
							$isSpecialCase = $this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($tmpVal);
							
							if($isSpecialCase){
								# Check whether it counts the weight or not if excludes full mark 
								$isCountFullMark = $this->IS_COUNT_FULLMARK_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
								
								# Check whether it counts the weight or not if excludes weight 
								$isCountWeight = $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
							}
						}
						
						$WeightTotal += ($isCountWeight == 0 && !($isCountFullMark == 0)) ? 0 : $ReportWeightInfo[$i]['Weight'];
					}
				}
			}
		}
		
		if(count($ReportInfo) > 0) {
			if($Mark !== "" && $Mark >= 0){
				$returnVal = ($WeightTotal > 0) ? ($Mark * $WeightArr[$ReportColumnID]) / $WeightTotal : $Mark;
			}
		}
		
		return $returnVal;
	}
	
	# Modified version by Andy, for calculating the weighted mark of component subject
	# Solely based on the weight of the subject itself, not using the column weight
	function CONVERT_CMP_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $Mark, $CalculationType=1, $AllColumnMarkArr=array()){
		$returnVal = '';
		$ReportInfo = array();
		$ReportWeightInfo = array();
		$WeightArr = array();
		$WeightTotal = 0;
		
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$column_data = $this->returnReportTemplateColumnData($ReportID); 		// Column Data
		
		# Get Report Column Weight
		$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = ".$SubjectID);
		
		# Subject Weight Array
		if(count($ReportWeightInfo) > 0) {
			for($i=0 ; $i<sizeof($ReportWeightInfo) ; $i++){
				if ($ReportWeightInfo[$i]['ReportColumnID'] == "")
					$WeightArr[0] = $ReportWeightInfo[$i]['Weight'];
				else
					$WeightArr[$ReportWeightInfo[$i]['ReportColumnID']] = $ReportWeightInfo[$i]['Weight'];
				if($CalculationType == 1){
					if($ReportWeightInfo[$i]['ReportColumnID'] != ""){
						# Check for any weight will not be used to as a Total Weight
						$isCountWeight = 1;
						$isCountFullMark = 1;
						if(count($AllColumnMarkArr) > 0 && $AllColumnMarkArr[$ReportWeightInfo[$i]['ReportColumnID']] != ""){
							$tmpVal = $AllColumnMarkArr[$ReportWeightInfo[$i]['ReportColumnID']];
							$isSpecialCase = $this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($tmpVal);
							
							if($isSpecialCase){
								# Check whether it counts the weight or not if excludes full mark 
								$isCountFullMark = $this->IS_COUNT_FULLMARK_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
								
								# Check whether it counts the weight or not if excludes weight 
								$isCountWeight = $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpVal, $absentExemptSettings);
							}
						}
						$WeightTotal += ($isCountWeight == 0 && !($isCountFullMark == 0)) ? 0 : $ReportWeightInfo[$i]['Weight'];
					}
				}
			}
		}
		
		if(count($ReportInfo) > 0) {
			if($Mark !== "" && $Mark >= 0) {
				//$returnVal = $Mark * $WeightArr[$ReportColumnID];
				//if ($ReportID==435 && $SubjectID==297) debug_r("($Mark * ".$WeightArr[$ReportColumnID].") / $WeightTotal");
				$returnVal = ($WeightTotal > 0) ? ($Mark * $WeightArr[$ReportColumnID]) / $WeightTotal : $Mark;
			}
		}
		
		return $returnVal;
	}
	
	/*
	* Get general details of Subject Form Grading
	*/
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID='', $withGrandResult=0, $returnAsso=0, $ReportID=''){
		$returnArr = array();
		$this->Load_Batch_Subject_Form_Grading_Scheme($ClassLevelID);
		$AllSchemeArr = $this->Subject_Form_Grading_Scheme[$ClassLevelID];
		
		$sizeOfScheme = count($AllSchemeArr);
		$result = array();
		
		
		for($i=0;$i<$sizeOfScheme; $i++)
		{
//			if ($AllSchemeArr[$i]["ClassLevelID"] != $ClassLevelID )
//				continue; 
				
			if (trim($SubjectID) != '' && $AllSchemeArr[$i]["SubjectID"] != $SubjectID )
				continue;

			if ($withGrandResult == 0 && $AllSchemeArr[$i]["SubjectID"] < 0)
				continue;
				
			if(trim($ReportID)=='' && $AllSchemeArr[$i]["ReportID"] != 0 )
				continue;
			else if(trim($ReportID)!='' && $AllSchemeArr[$i]["ReportID"] != $ReportID ) 
				continue;
			$result[] = $AllSchemeArr[$i];
		}		
		
//		$SubjectCond = '';
//		if ($SubjectID != '')
//			$SubjectCond = " AND SubjectID = '$SubjectID' ";
//			
//		$GrandCond = '';
//		if ($withGrandResult == 0)
//			$GrandCond = " AND SubjectID > '0' ";
//		
//		if(trim($ReportID)=='')
//			$cond_ReportID = " AND ReportID = 0 ";
//		else
//			$cond_ReportID = " AND ReportID = '$ReportID' ";
//			
//		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
//		$sql  = "SELECT 
//					SchemeID, DisplayOrder, ScaleInput, ScaleDisplay, SubjectID, LangDisplay, DisplaySubjectGroup
//				FROM 
//					$table 
//				WHERE 
//					ClassLevelID = $ClassLevelID 
//					$cond_ReportID
//					$SubjectCond
//					$GrandCond
//				ORDER BY DisplayOrder";
//		$result = $this->returnArray($sql, 4);

		if(count($result) > 0){
			
			if ($SubjectID != '')
			{
				$returnArr['SchemeID'] = $result[0]['SchemeID'];
				$returnArr['DisplayOrder'] = $result[0]['DisplayOrder'];
				$returnArr['ScaleInput'] = $result[0]['ScaleInput'];
				$returnArr['ScaleDisplay'] = $result[0]['ScaleDisplay'];
				$returnArr['LangDisplay'] = $result[0]['LangDisplay'];
				$returnArr['DisplaySubjectGroup'] = $result[0]['DisplaySubjectGroup'];
			}
			else
			{
				$numOfResult = count($result);
				$returnArr = array();
				for ($i=0; $i<$numOfResult; $i++)
				{
					if ($returnAsso==1)
					{
						$thisSubjectID = $result[$i]['SubjectID'];
						$returnArr[$thisSubjectID] = $result[$i];
					}
					else
					{
						$returnArr[] = $result[$i];
					}
				}
			}
		}
		
		return $returnArr;
	}
	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed) 
	function Get_Form_Grading_Scheme($ClassLevelID, $ReportID='')
	{
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		
		$schemeInfoArr = array();
		foreach($SubjectArray as $SubjectID => $SubjectName)
		{
			# Retrieve Subject Scheme ID & settings
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
			$schemeInfoArr[$SubjectID]['SchemeID'] = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($schemeInfoArr[$SubjectID]['SchemeID']);
			
			$schemeInfoArr[$SubjectID]['ScaleDisplay'] = $SubjectFormGradingSettings['ScaleDisplay'];
			$schemeInfoArr[$SubjectID]['ScaleInput'] = $SubjectFormGradingSettings['ScaleInput'];
			$schemeInfoArr[$SubjectID]['LangDisplay'] = $SubjectFormGradingSettings['LangDisplay'];
			$schemeInfoArr[$SubjectID]['FullMark'] = $SchemeInfo['FullMark'];
		}
		
		return $schemeInfoArr;
	}
	
	# Get Parent Subject Marksheet Score calculated by Marksheet Scores of Component Subjects 
	# Use when it contains Term Report Template (defined)
	# This function is used for Calculation Down-Right Method only
	# Pass StudentID Array, CmpSubjectID Array, ReportID, ReportColumnID
	# For each mark (M) of assessment(ReportColumn) of Parent Subject 
	# 	Formula: M = M1 x W1 x G1 + M2 x W2 x G2 + ... + Mn x Wn x Gn 
	# 	M1: Mark of Assessment of 1st Component Subject
	# 	W1: Weight of Assessment
	# 	G1: Full Mark Factor which is calculated by  Pf / total of (Cf x Wn)
	# 	Pf: Full Mark of Parent Subject
	# 	Cf: Full Mark of Component Subject
	# Modified by Andy on 2 Jun 2008, add the logic to deal with No DB Records
	# Modified by Andy on 23 July 2008, add $Mode argument for specifyingTerm/FullYear
	# Modified by Andy on 12 August 2008
	#	Add $IsSubjectOverall argument for specifying consolidated marks should be retrieved instead of marksheet score 
	#	for calculating subject overall of parent subject when calculation order is "1" (horizontal-to-vertical)
	function GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $ParentSubjectID, $CmpSubjectIDArr, $ReportID, $ReportColumnID, $Mode="Term", $IsSubjectOverall=0, $debug=0){
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName, $eRCTemplateSetting;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup'])
		{
			$SubjectGroupStudentIDArr = $this->Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $ParentSubjectID);
		}
		
		$returnArr = array();
		$MarksheetScoreArr = array();
		
		# Get Absent & Exempt Settings
		$absentExemptSettings = $this->LOAD_SETTING('Absent&Exempt');
		
		# Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$categorySettings = $this->LOAD_SETTING('Calculation');
		
		if ($Mode == "Term")
			$TermCalculationType = $categorySettings['OrderTerm'];
		else
			$TermCalculationType = $categorySettings['OrderFullYear'];
		
		$isAbjustFullMark = $categorySettings["AdjustToFullMarkFirst"];
			
		# Get Report Template Info
		$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);				// Basic Data
		$ClassLevelID = $ReportInfo['ClassLevelID'];
		$Semester = $ReportInfo['Semester'];
		$ReportType = ($Semester=='F')? 'W' : 'T';
		
		$ColumnInfo = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ColumnInfo);
		
		# Get Full Mark of Parent Subject
		$ParentFullMark = $this->GET_SUBJECT_FULL_MARK($ParentSubjectID, $ClassLevelID, $ReportID);
		
		# check if all component subjects are zero weight in order to retrieve marks from different table
		$isAllComponentZeroWeightArr = $this->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $ParentSubjectID);
		
		if(count($StudentIDArr) > 0 && count($CmpSubjectIDArr) > 0){			
			for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
				if ($IsSubjectOverall) {
					# Get Consolidated component subjects mark
					$MarksheetScoreArr[$CmpSubjectIDArr[$i]] = $this->GET_RESULT_SCORE_FOR_SUBJECT_OVERALL($StudentIDArr, $CmpSubjectIDArr[$i], $ReportColumnID, $ReportID);
				} else {
					# Get MarkSheet Score Data by Column in Student 
					$MarksheetScoreArr[$CmpSubjectIDArr[$i]] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $CmpSubjectIDArr[$i], $ReportColumnID);
				}
			}
			
			
			$TmpReportColumnID = ($ReportColumnID=='')? 0 : $ReportColumnID;
			$existSpFullMarkArr = $this->GET_NEW_FULLMARK($ReportID, '', '', $TmpReportColumnID);
			$existSpFullMarkAssoArr = BuildMultiKeyAssoc($existSpFullMarkArr, array('StudentID', 'SubjectID', 'ReportColumnID'));
			unset($existSpFullMarkArr);
			
			# Loop Students
			foreach($StudentIDArr as $StudentID){
				
				$TotalAssSubMark = 0;		// store the Total Assessment Subject Mark
				$notCount = 0;				// count for the happeness of Special Case for (/, *, N.A.)
				$notYetMark = 0;			// count for the Mark which is still not given
				$noRecord = 0;
				$totalWeight = 0;
				$excludeWeight = 0;
				$excludeCmpFullMark = 0;
				
				# Check for Combination of Special Case of Input Mark Among Component Subjects 
		        $SpecialCaseArr = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		        foreach($SpecialCaseArr as $key){
		        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
	        	}
				
				# Loop The Component Subjects
				for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
					$iCmpSubjectID = $CmpSubjectIDArr[$i];
					$TotalCmpSubjectWeight = 0;
					$AllCmpSubjectWeight = 0;
					$TotalCmpFullMark = 0;
					$AllCmpFullMark = 0;
					$totalDenomenator = 0;
					
					######## Added on 20080707 ########
					$TotalCmpFullMarkForInsert = 0;
					
					# Get Personal Weight among Component Subjects of each student
					if($TermCalculationType == 1){
						$OtherCondition = "SubjectID = ".$iCmpSubjectID." AND ReportColumnID IS NULL ";
						$CmpSubjectWeight[$iCmpSubjectID] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
					} else {
						$OtherCondition = "SubjectID = ".$iCmpSubjectID." AND ReportColumnID = $ReportColumnID";
						$CmpSubjectWeight[$iCmpSubjectID] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					}
					
					$AllCmpSubjectWeight += $CmpSubjectWeight[$iCmpSubjectID][0]['Weight'];
			    	$TotalCmpSubjectWeight += ($isCountWeight) ? $CmpSubjectWeight[$iCmpSubjectID][0]['Weight'] : 0;
			    	
			    	# Get Total Full Mark of All Component Subjects
					$CmpFullMark = $this->GET_SUBJECT_FULL_MARK($iCmpSubjectID, $ClassLevelID, $ReportID);
					//$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $StudentID, $iCmpSubjectID, $ReportColumnID);
					$existSpFullMark = $existSpFullMarkAssoArr[$StudentID][$iCmpSubjectID][$TmpReportColumnID];
					if (count((array)$existSpFullMark) > 0) {
						$iTmpCmpFullMark = $existSpFullMark["FullMark"] * $CmpSubjectWeight[$iCmpSubjectID][0]['Weight'];
					} else {
						$iTmpCmpFullMark = ($CmpFullMark != "") ? $CmpFullMark * $CmpSubjectWeight[$iCmpSubjectID][0]['Weight'] : 0;
					}
					
					if(count($MarksheetScoreArr[$CmpSubjectIDArr[$i]]) > 0){
						##################### By Andy: Skip if no Record #####################
						if (isset($MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID])) {
							for($j=0 ; $j<count($CmpSubjectIDArr) ; $j++){
								$thisCmpSubjectID = $CmpSubjectIDArr[$j];
								 
								# Get Mark Details for Temporary use only
								$tmpType = $MarksheetScoreArr[$thisCmpSubjectID][$StudentID]['MarkType'];
								$tmpNonNum = $MarksheetScoreArr[$thisCmpSubjectID][$StudentID]['MarkNonNum'];
								
								$isCountWeight = ($tmpType == "SC") ? $this->IS_COUNT_WEIGHT_OF_SPECIALCASE($tmpNonNum, $absentExemptSettings) : 1;
								
								# Get Personal Weight among Component Subjects of each student
								if($TermCalculationType == 1){
									$OtherCondition = "SubjectID = ".$thisCmpSubjectID." AND ReportColumnID IS NULL ";
									$CmpSubjectWeight[$thisCmpSubjectID] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
									
								} else {
									$OtherCondition = "SubjectID = ".$thisCmpSubjectID." AND ReportColumnID = $ReportColumnID";
									$CmpSubjectWeight[$thisCmpSubjectID] = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
								}
								$AllCmpSubjectWeight += $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'];
						    	$TotalCmpSubjectWeight += ($isCountWeight) ? $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'] : 0;
						    	
						    	# Get Total Full Mark of All Component Subjects
								$CmpFullMark = $this->GET_SUBJECT_FULL_MARK($thisCmpSubjectID, $ClassLevelID, $ReportID);
								//$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $StudentID, $thisCmpSubjectID, $ReportColumnID);
								$existSpFullMark = $existSpFullMarkAssoArr[$StudentID][$thisCmpSubjectID][$TmpReportColumnID];
								
								if (count((array)$existSpFullMark) > 0) {
									$tmpCmpFullMark = $existSpFullMark["FullMark"] * $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'];
								} else {
									$tmpCmpFullMark = ($CmpFullMark != "") ? $CmpFullMark * $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'] : 0;
								}
								$TotalCmpFullMark += ($isCountWeight) ? (($tmpCmpFullMark != "") ? $tmpCmpFullMark : 0) : 0;
								
								$AllCmpFullMark += ($tmpCmpFullMark != "") ? $tmpCmpFullMark : 0;
								
								# updated on 1 Jan 09 by Ivan - added the $isAbjustFullMark condition
								if($TermCalculationType == 1)
								{
									$totalDenomenator += $CmpFullMark * $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'];
								}
								else
								{
									if ($isAbjustFullMark)
									{
										$totalDenomenator += 100 * $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'];
									}
									else
									{
										$totalDenomenator += $CmpFullMark * $CmpSubjectWeight[$thisCmpSubjectID][0]['Weight'];
									}
								}
								
								############# Added on 20080707: Check if the component subject is excluded full mark ################
								if ($tmpType == "SC") {
									$tempSpecialCaseCountArr = array();
									$tempSpecialCaseCountArr = $this->DIFFERENTIATE_SPECIAL_CASE($tempSpecialCaseCountArr, $tmpNonNum, $absentExemptSettings);
									if ($tempSpecialCaseCountArr[$tmpNonNum]["isExFullMark"] == "1") {
										$TotalCmpFullMarkForInsert += 0;
									} else {
										// A factor ($ParentFullMark/$CmpFullMark) is multipy to fix the different of full mark betwenn parent subject and component subjects
										if ($tmpCmpFullMark != "") {
											$TotalCmpFullMarkForInsert += $tmpCmpFullMark*($ParentFullMark/$CmpFullMark);
										}
									}
								} else {
									if ($tmpCmpFullMark != "") {
										$TotalCmpFullMarkForInsert += $tmpCmpFullMark*($ParentFullMark/$CmpFullMark);
									}
								}
								
								##########################################################################
							}
							
							#Get Full Mark Factor for calculating the mark of Parent Subject 
							#$Factor = $this->GET_FULL_MARK_FACTOR($ParentFullMark, $TotalCmpFullMark);
							
							$Factor = $this->GET_FULL_MARK_FACTOR($ParentFullMark, $TotalCmpFullMarkForInsert);
							
							// if total weight > 1, need to reduce it to = 1 in order to make the total component full mark equal to parent full mark
							if ($TotalCmpSubjectWeight > 1) {
								if ($TotalCmpFullMarkForInsert == $AllCmpFullMark)
									$TotalCmpFullMarkForInsert = $TotalCmpFullMarkForInsert*(1/$AllCmpSubjectWeight);
								else if ($TotalCmpFullMarkForInsert < $AllCmpFullMark)
									$TotalCmpFullMarkForInsert = $TotalCmpFullMarkForInsert*(1/$TotalCmpSubjectWeight);
							}
							############# Added on 20080707: If new full mark need to be inserted ################
							############# Disabled on 20080909: No need to calculate, full mark will be the same
							/*
							if ($TotalCmpFullMarkForInsert != $ParentFullMark) {
								$existSpFullMark = $this->GET_NEW_FULLMARK($ReportID, $StudentID, $ParentSubjectID, $ReportColumnID);
								// Prevent duplicated insertion
								if (sizeof($existSpFullMark) == 0) {
									#$this->INSERT_NEW_FULLMARK($TotalCmpFullMarkForInsert, $ReportID, $StudentID, $ParentSubjectID, $ReportColumnID);
								}
							}
							*/
							##########################################################################
							
							# Get Mark Details
							$MarkType = $MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID]['MarkType'];
							$MarkRaw = $MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID]['MarkRaw'];
							$MarkNonNum = $MarksheetScoreArr[$CmpSubjectIDArr[$i]][$StudentID]['MarkNonNum'];
							
							$totalWeight += $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'];
							
							# Calculate the Weighted Mark of Parent Subject From the Mark of Component Subjects
							if($MarkType == "M" && $MarkRaw !== "" && $MarkRaw >= 0){
								//$TotalAssSubMark += ($TotalCmpSubjectWeight > 0) ? ($MarkRaw * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight']) / $TotalCmpSubjectWeight : "";
								//$TotalAssSubMark += ($Factor != "" && $Factor > 0) ? $MarkRaw * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'] * $Factor : 0;
								
								# updated on 1 Jan 09 by Ivan - added the $isAbjustFullMark condition
								if ($isAbjustFullMark)
								{
									$CmpFullMark = $this->GET_SUBJECT_FULL_MARK($CmpSubjectIDArr[$i], $ClassLevelID, $ReportID);
									$thisCmpMark = ($MarkRaw * (100 / $CmpFullMark)) * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'];
									
									/*
									if (!$eRCTemplateSetting['UseSumOfCmpMarkToCalculateParentSubjectMark'])
									{
										$thisCmpMark = $thisCmpMark * $Factor;
									}
									*/
									
									if ($eRCTemplateSetting['RoundWeightedSubjectMarkBeforeAddToOverallMark'])
									{
										$thisCmpMark = $this->ROUND_MARK($thisCmpMark, "SubjectScore");
									}
									
									$TotalAssSubMark += ($Factor != "" && $Factor > 0) ? $thisCmpMark : 0;
								}
								else
								{
									$thisCmpMark = $MarkRaw * $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'];
									
									/*
									if (!$eRCTemplateSetting['UseSumOfCmpMarkToCalculateParentSubjectMark'])
									{
										$thisCmpMark = $thisCmpMark * $Factor;
									}
									*/
									
									if ($eRCTemplateSetting['RoundWeightedSubjectMarkBeforeAddToOverallMark'])
									{
										$thisCmpMark = $this->ROUND_MARK($thisCmpMark, "SubjectScore");
									}
									
									$TotalAssSubMark += ($Factor != "" && $Factor > 0) ? $thisCmpMark : 0;
								}
								
							} else if($MarkType == "M" && ($MarkRaw == "" || $MarkRaw == -1) ){
								$notYetMark++;
							} else if($MarkType == "SC"){					// Special Case checking
								$SpecialCaseCountArr = $this->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $MarkNonNum, $absentExemptSettings);
								$scMark = $SpecialCaseCountArr[$MarkNonNum]['Value'];
								$notCount = $SpecialCaseCountArr[$MarkNonNum]['Count'];
								$isCount = $SpecialCaseCountArr[$MarkNonNum]['isCount'];
								$isExFullMark = $SpecialCaseCountArr[$MarkNonNum]['isExFullMark'];
								
								if($isCount)
									$TotalAssSubMark += $scMark;	// scMark : SpecialCaseMark
								if (!$isCount && !$isExFullMark)
									$excludeWeight += $CmpSubjectWeight[$CmpSubjectIDArr[$i]][0]['Weight'];
								if ($isExFullMark)
									$excludeCmpFullMark += $iTmpCmpFullMark;
									
								### If one of the column is N.A. or other special case, the overall column will be the special case also
								// Comment below if 2011-0120-1542-12096 said disable the customization
								if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true)
								{
									if ($this->Check_Copy_Special_Case($MarkNonNum, $ReportType))
									{
										$returnArr[$StudentID] = $MarkNonNum;
										break;
									}
								}
								// Comment above if 2011-0120-1542-12096 said disable the customization
							} else if($MarkType == "G"){
								if($MarkNonNum != "")
									$notCount++;
								else 
									$notYetMark++;
							}
						} else {
							# Counter for no record
							$noRecord++;
						}
					} else {
						$notYetMark++;
					}
				}
				
				# Checking for any combination of special cases without any Mark
				$isSetSpecialCase = 0;
				$isSetAllSpecialCase = 0;
				$totalSCCount = 0;
				foreach($SpecialCaseCountArr as $key => $scData){	// scData : SpecialCaseData
					if($scData['Count'] == count($CmpSubjectIDArr)){
						$isSetSpecialCase = 1;
						$setSpecialCaseValue = $key;
					}
					$totalSCCount += $scData['Count'];
				}
				if($totalSCCount == count($CmpSubjectIDArr)){
					$isSetAllSpecialCase = 1;
				}
				############# Added on 20080708: If all are special cases and at least one of the special case is treat as Zero mark, make it "0" anyway #############
				$scCondition = ($noRecord != count($CmpSubjectIDArr)) && ($notYetMark != count($CmpSubjectIDArr)) && 
								$isSetSpecialCase && ($notCount != count($CmpSubjectIDArr));
// 				$scCondition = ($noRecord != count($CmpSubjectIDArr)) && ($notYetMark != count($CmpSubjectIDArr)) && ($notCount != count($CmpSubjectIDArr));
				
				if (($scCondition) && 
					($SpecialCaseCountArr['+']['isCount'] > 0 || 
					($absentExemptSettings['Absent'] == "Mark0" && $SpecialCaseCountArr['-']['isCount'] > 0) || 
					($absentExemptSettings['Exempt'] == "Mark0" && $SpecialCaseCountArr['/']['isCount'] > 0)))
				{
					$isSetSpecialCase = 1;
					$setSpecialCaseValue = 0;
				}
				##############################################################################
				
				# Control the output of mark of each student if not assigned the marks yet
				if ($returnArr[$StudentID] == '')
				{
					if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']==true && in_array($StudentID, $SubjectGroupStudentIDArr)==false)
					{
						$returnArr[$StudentID] = "N.A.";
					}
					else if($noRecord == count($CmpSubjectIDArr))
					{
						# 1st condition ($noRecord) added by Andy
						$returnArr[$StudentID] = "";
					}
					else if($notYetMark == count($CmpSubjectIDArr))
					{
						if ($isAllComponentZeroWeightArr[$ReportColumnID] == 1)
						{
							### All weight zero => get parent subjetc score from marksheet directly
							$thisMarksheetScoreArr = $this->GET_MARKSHEET_COLUMN_SCORE($StudentID, $ReportColumnID, $ParentSubjectID);
							$thisRawMark = $thisMarksheetScoreArr[$StudentID]['MarkRaw'];
							$thisMarkNonNum = $thisMarksheetScoreArr[$StudentID]['MarkNonNum'];
							$thisScore = ($thisRawMark != -1) ? $thisRawMark : $thisMarkNonNum;
							
							$returnArr[$StudentID] = $thisScore;
						}
						else
						{
							$returnArr[$StudentID] = "";
						}
					}
					else if($isSetSpecialCase){
						$returnArr[$StudentID] = $setSpecialCaseValue;
					}
					else if($notCount == count($CmpSubjectIDArr))
					{
						$returnArr[$StudentID] = "N.A.";
					}
					else 
					{
						if ($totalWeight != 0)
						{
							$thisWeight = ($totalWeight - $excludeWeight) / $totalWeight;
							
							if ($thisWeight != 0)
								$thisScore = $TotalAssSubMark / $thisWeight;
							else
								$thisScore = 0;
								
								// Disabled: 2011-0217-1334-46128
//							if ($Semester!='F' && sizeof($existSpFullMark)>0)
//								$thisScore = $thisScore / $numOfColumn;							
						}
						else
							$thisScore = 0;
							
						# updated on 1 Jan 09 by Ivan - added the $isAbjustFullMark condition
						if ($TermCalculationType == 1)
						{
							# Full Mark Adjustment
							if ($AllCmpFullMark - $excludeCmpFullMark > 0)
								$fullMarkRatio = $AllCmpFullMark / ($AllCmpFullMark - $excludeCmpFullMark);
							else
								$fullMarkRatio = 1;
							
							if ($isAbjustFullMark)
							{
								//$ParentFullMark = $this->GET_SUBJECT_FULL_MARK($ParentSubjectID, $ClassLevelID);
								# commented on 22 Jun 2009
								# �����j�|�Ǯ� - eRC -- Wrong Calculation in all component subjects [CRM Ref No.: 2009-0619-1540]
								//$thisScore = $thisScore / $totalWeight;
								//$returnArr[$StudentID] = $thisScore * ($ParentFullMark / 100);
								
								$thisScore = $thisScore * $fullMarkRatio;
								
								if ($AllCmpFullMark > 0)
									$returnArr[$StudentID] = $thisScore * (100 / $AllCmpFullMark);
								else
									$returnArr[$StudentID] = $thisScore;
							}
							else
							{
								//$returnArr[$StudentID] = $thisScore;
								if ($totalDenomenator > 0) {
									$thisScore = $thisScore * ($ParentFullMark / $totalDenomenator);
								}
								
								if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
								{
									$returnArr[$StudentID] = $TotalAssSubMark;
								}
								else
								{
									$returnArr[$StudentID] = $thisScore * $fullMarkRatio;
								}
							}
						}
						else
						{
							if ($totalDenomenator == 0)
							{
								$returnArr[$StudentID] = $thisScore * $totalWeight;
							}
							else
							{
								if ($isAbjustFullMark)
								{
									$returnArr[$StudentID] = ($thisScore / $totalDenomenator) * 100 * $totalWeight;
								}
								else
								{
									//if ($eRCTemplateSetting['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark'])
									//{
									//	$tmpReturnMark = $thisScore / ($AllCmpFullMark - $excludeCmpFullMark);
									//	$returnArr[$StudentID] = $tmpReturnMark * $ParentFullMark;
									//}
									//else
									//{
										//$returnArr[$StudentID] = ($thisScore / $totalDenomenator) * $totalDenomenator * $totalWeight;
										
										if ($ParentFullMark > 0)
										{
											# Full Mark Adjustment
											$fullMarkRatio = ($AllCmpFullMark - $excludeCmpFullMark) / $ParentFullMark;
											
											# Exclusion Adjustment
											if ($fullMarkRatio > 0)
												$returnArr[$StudentID] = $thisScore / $fullMarkRatio;
										}
									//}
								}
							} 
						}
					}
				}	// End of "if ($returnArr[$StudentID] == '')"
			}	// End of "foreach($StudentIDArr as $StudentID)"
		}	// End of "if(count($StudentIDArr) > 0 && count($CmpSubjectIDArr) > 0)"
		return $returnArr;
	}
	
	/* ******************************************* */
	// already defined by Yat Woon
	
	function returnReportColoumnTitle($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "SELECT ReportColumnID, ColumnTitle, SemesterNum FROM $table where ReportID = $ReportID ORDER BY DisplayOrder";
		$result = $this->returnArray($sql);
		foreach($result as $key => $val)
		{
			list($ReportColumnID, $ColumnTitle, $SemesterNum) = $val;
			$x[$ReportColumnID] = $ColumnTitle ? $ColumnTitle : $this->returnSemesters($SemesterNum);
		}
		
		return $x;
	}
	
	function returnClassLevel($ClassLevelID)
	{
		$sql = "SELECT 
						YearName as LevelName
				FROM 
						YEAR 
				WHERE
						YearID = '$ClassLevelID'
				ORDER BY 
						Sequence
				";
		$result = $this->returnVector($sql);
		
		return $result[0];
	}
	
	# Param $ParDisplayType = "Abbr/Desc/ShortName", Default Desc
	function returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID='')
	{ 
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		if($TeacherID)
		{
			$TeahcerSubjects = array();
			$TeahcerSubjectAry = $this->returnSunjectTeacherSubject($TeacherID, $ClassLevelID);
			foreach($TeahcerSubjectAry as $key=>$val)
				$TeahcerSubjects[] = $val['SubjectID'];
		}
		
		switch($ParDisplayType)
		{
			case "Desc": $DisplayForm = "DES"; break;
			case "Abbr": $DisplayForm = "ABBR"; break;
			case "ShortName": $DisplayForm = "SNAME"; break;
			default: $DisplayForm = "DES";
		}
		$chiName = "CH_".$DisplayForm;
		$engName = "EN_".$DisplayForm; 
		
		$conds_ExcludeCmpSubject = '';
		if ($ExcludeCmpSubject == 1)
			$conds_ExcludeCmpSubject = " AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '') ";
		
		if ($SubjectFieldLang == "")
		{
			$SubjectField = ($ParForSelection==1) ? "CONCAT(a.$engName, ' (', a.$chiName, ')')" : "CONCAT(a.$engName, '<br />', a.$chiName)";
			$CmpSubjectField = ($ParForSelection==1) ? "CONCAT(b.$engName, ' (', b.$chiName, ')')" : "CONCAT(b.$engName, '<br />', b.$chiName)";
		}
		else
		{
			if ($SubjectFieldLang == "en") {
				$SubjectField = "a.$engName";
				$CmpSubjectField = "b.$engName";
			} else {
				$SubjectField = "a.$chiName";
				$CmpSubjectField = "b.$chiName";
			}
		}
		
		# Modified by Marcus 20101011 (for template based grading scheme) 
		if ($ReportID != '')
			$cond_ReportID = " And c.ReportID = '".$ReportID."' And d.ReportID = '".$ReportID."' ";
		else
			$cond_ReportID = " AND c.ReportID = 0 AND d.ReportID = 0 ";
		
		$sql = "SELECT DISTINCT
					a.RecordID,
					$SubjectField,
					b.RecordID,
					$CmpSubjectField
				FROM
					ASSESSMENT_SUBJECT AS a
					LEFT JOIN ASSESSMENT_SUBJECT AS b ON a.CODEID = b.CODEID
					LEFT JOIN $table as c on c.SubjectID=b.RecordID 
					LEFT JOIN $table as d on d.SubjectID=a.RecordID
				WHERE
					a.EN_SNAME IS NOT NULL
					AND (a.CMP_CODEID IS NULL Or a.CMP_CODEID = '')
					AND c.ClassLevelID=$ClassLevelID 
					AND d.ClassLevelID=$ClassLevelID 
					$cond_ReportID
					AND a.RecordStatus = 1
					AND b.RecordStatus = 1
					$conds_ExcludeCmpSubject
				ORDER BY
					d.DisplayOrder, c.DisplayOrder
			";
		$SubjectArr = $this->returnArray($sql);
		
		$ReturnArr = array();
		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($SubjectID, $SubjectName, $CmpSubjectID, $CmpSubjectName) = $SubjectArr[$i];
			
			if($TeacherID)
			{
				if(in_array($SubjectID, $TeahcerSubjects)===false)	continue;
			}
			
			if($SubjectID==$CmpSubjectID)
			{
				# Main Subject
				$CmpSubjectID = 0;
			}
			else
			{
				# Component Subject
				
				# Set the Main Subject first
				if ($ReturnArr[$SubjectID][0] == '')
					$ReturnArr[$SubjectID][0] = $SubjectName;
				
				$SubjectName = $CmpSubjectName;
			}
			$ReturnArr[$SubjectID][$CmpSubjectID] = $SubjectName;
			
			
			# updated on 18 Dec 2008 by Ivan
			# order by DisplayOrder, not by SubjectID
			//ksort($ReturnArr[$SubjectID]);
		}
		return $ReturnArr;
	}
	
	function returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID='', $SubjectFieldLang='')
	{
		$x = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection, $TeacherID='', $SubjectFieldLang, $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		$ary = array();
		if(is_array($x))
		{
			foreach($x as $PSubjectID => $data)
			{
				foreach($data as $SubjectID => $d)
				{
					$SubjID = ($SubjectID && !$MainSubjectOnly)? $SubjectID : $PSubjectID;
					$ary[$SubjID] = $d;
					if($MainSubjectOnly) break;
				}
			}
		}		
		return $ary;
	}
	
	function returnClassTeacherClassInfo($ParUserID)
	{
		$sql = "SELECT
						yct.YearClassID as ClassID
				FROM
						YEAR_CLASS_TEACHER as yct
					INNER JOIN
						YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."')
				WHERE
						yct.UserID = '$ParUserID'
				";
		//$sql ="select ClassID from INTRANET_CLASSTEACHER where UserID=$UserID";
		return $this->returnVector($sql);
	}
	
	function returnClassTeacherForm($ParUserID)
	{
		$sql = "SELECT
						DISTINCT(y.YearID) as ClassLevelID,
						y.YearName as LevelName
				FROM
						YEAR_CLASS_TEACHER as yct
						INNER JOIN
						YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."')
						INNER JOIN
						YEAR as y ON (yc.YearID = y.YearID)
				Where
						yct.UserID = '".$ParUserID."'
				Order By
						y.Sequence
				";
		return $this->returnArray($sql);
	}
	
	function returnClassTeacherClass($ParUserID, $ClassLevelID='', $ClassName='', $YearClassID='')
	{
		$con = $ClassLevelID != "" ? " and y.YearID = '$ClassLevelID' " : "";
		$con .= $ClassName != "" ? " and yc.ClassTitleEn = '$ClassName' " : "";
		$con .= $YearClassID != "" ? " and yc.YearClassID = '$YearClassID' " : "";
		
	 	$sql = "SELECT
	 					yc.YearClassID as ClassID,
	 					yc.ClassTitleEn as ClassName,
	 					y.YearID as ClassLevelID
	 			FROM
	 					YEAR_CLASS_TEACHER as yct
	 					INNER JOIN
	 					YEAR_CLASS as yc
	 					ON (yct.YearClassID = yc.YearClassID AND yct.UserID = '$ParUserID' AND yc.AcademicYearID = '".$this->schoolYearID."')
	 					INNER JOIN
	 					YEAR as y
	 					ON (yc.YearID = y.YearID)
	 			WHERE
	 					1
	 					$con
	 			";
		return $this->returnArray($sql);
	}
	
	function returnSubjectTeacherClassInfo($ParUserID)
	{
		//$sql ="select SubjectID from RC_SUBJECT_TEACHER where UserID=$UserID";
		$sql = "	SELECT
							st.SubjectID
					FROM
							SUBJECT_TERM_CLASS_TEACHER as stct
							INNER JOIN
							SUBJECT_TERM as st ON (stct.SubjectGroupID = st.SubjectGroupID AND stct.UserID = '$ParUserID')
							INNER JOIN
							ACADEMIC_YEAR_TERM  as ayt ON (st.YearTermID = ayt.YearTermID)
							INNER JOIN
							ACADEMIC_YEAR as ay ON (ayt.AcademicYearID = ay.AcademicYearID AND ay.AcademicYearID = '".$this->schoolYearID."')
				";
		return $this->returnVector($sql);
	}
	
	function returnSunjectTeacherForm($ParUserID, $YearTermID='')
	{
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		include_once('form_class_manage.php');
	 	$obj_AcademicYear = new academic_year($this->schoolYearID);
		$YearTermArr = $obj_AcademicYear->Get_Term_List(0);
		$numOfTerm = count($YearTermArr);
		
		# Build YearTermID Array and List
		$YearTermIDArr = array();
		for ($i=0; $i<$numOfTerm; $i++)
			$YearTermIDArr[] = $YearTermArr[$i]['YearTermID'];
		$YearTermIDList = implode(', ', $YearTermIDArr);
	 	
		# Modified by Marcus 20101011 (for template based grading scheme) 
		$cond_ReportID = " AND a.ReportID = 0 ";

	 	$sql = "	SELECT
	 							DISTINCT(y.YearID) as ClassLevelID,
	 							y.YearName as LevelName
	 				FROM
	 							SUBJECT_TERM_CLASS_TEACHER as stct
	 							INNER JOIN
	 							SUBJECT_TERM_CLASS_USER as stcu
	 							ON (stct.SubjectGroupID = stcu.SubjectGroupID AND stct.UserID = '$ParUserID')
	 							INNER JOIN
	 							SUBJECT_TERM as stc
	 							ON (stct.SubjectGroupID = stc.SubjectGroupID AND stc.YearTermID IN ($YearTermIDList) )
	 							INNER JOIN
	 							YEAR_CLASS_USER as ytcu
	 							ON (stcu.UserID = ytcu.UserID)
	 							INNER JOIN
	 							YEAR_CLASS as ytc
	 							ON (ytcu.YearClassID = ytc.YearClassID And ytc.AcademicYearID = '".$this->schoolYearID."')
	 							INNER JOIN
	 							YEAR as y
	 							ON (ytc.YearID = y.YearID)
	 							INNER JOIN
	 							$table as a
	 							ON (stc.SubjectID=a.SubjectID AND a.ClassLevelID=y.YearID $cond_ReportID)
	 				ORDER BY
	 							y.Sequence
	 			";
		return $this->returnArray($sql);
	}
	
//	function returnSubjectTeacherClass($ParUserID, $ClassLevelID)
//	{
//		global $intranet_session_language;
//		
////		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
//		$ClassNameField = ($intranet_session_language=='en')? 'ClassTitleEN' : 'ClassTitleB5';
//	 	
//	 	$ClassLevelCond = ($ClassLevelID != "")? " and yc.YearID = '$ClassLevelID'" : "";
//	 	$sql = "SELECT
//	 					DISTINCT(yc.YearClassID) as ClassID,
//	 					yc.$ClassNameField as ClassName,
//	 					yc.YearID as ClassLevelID
//	 			FROM
//	 					SUBJECT_TERM_CLASS_TEACHER as stct
//	 					INNER JOIN
//	 					SUBJECT_TERM_CLASS_USER as stcu
//	 					ON (stct.SubjectGroupID = stcu.SubjectGroupID)
//	 					INNER JOIN
//	 					YEAR_CLASS_USER as ycu
//	 					ON (stcu.UserID = ycu.UserID)
//	 					INNER JOIN
//	 					YEAR_CLASS as yc
//	 					ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' $ClassLevelCond)
//	 			ORDER BY
//	 				yc.Sequence
//	 			";
//		return $this->returnArray($sql);
//	}
	
	function returnSubjectTeacherClass($ParUserID, $ClassLevelID)
	{
		global $intranet_session_language;
		
//		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$ClassNameField = ($intranet_session_language=='en')? 'ClassTitleEN' : 'ClassTitleB5';
	 	
	 	$ClassLevelCond = ($ClassLevelID != "")? " and yc.YearID = '$ClassLevelID'" : "";
	 	$UserIDCond = ($ParUserID != "")? " and stct.UserID = '$ParUserID'" : "";
	 	$sql = "SELECT
	 					DISTINCT(yc.YearClassID) as ClassID,
	 					yc.$ClassNameField as ClassName,
	 					yc.YearID as ClassLevelID,
						yc.YearClassID,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						yc.YearID
	 			FROM
	 					SUBJECT_TERM_CLASS_TEACHER as stct
	 					INNER JOIN
	 					SUBJECT_TERM_CLASS_USER as stcu
	 					ON (stct.SubjectGroupID = stcu.SubjectGroupID)
	 					INNER JOIN
	 					YEAR_CLASS_USER as ycu
	 					ON (stcu.UserID = ycu.UserID)
	 					INNER JOIN
	 					YEAR_CLASS as yc
	 					ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' $ClassLevelCond)
				WHERE
						1
						$UserIDCond
	 			ORDER BY
	 				yc.Sequence
	 			";
	 			
		return $this->returnArray($sql);
	}	
	
	function checkSubjectTeacher($ParUserID, $SubjectID, $ClassID='', $SubjectGroupID='')
	{
		$con = $SubjectID != "" ? " and st.SubjectID = $SubjectID" : "";
		$con .= $ClassID != "" ? " and ycu.YearClassID = $ClassID" : "";
		$con .= $SubjectGroupID != "" ? " and stc.SubjectGroupID = $SubjectGroupID" : "";
		
		$sql = "
			SELECT 
				DISTINCT stc.SubjectGroupID
			FROM 
				SUBJECT_TERM_CLASS_TEACHER as stct
				INNER JOIN SUBJECT_TERM_CLASS as stc on (stc .SubjectGroupID = stct.SubjectGroupID)
				INNER JOIN SUBJECT_TERM as st on (st .SubjectGroupID = stc.SubjectGroupID)
				INNER JOIN SUBJECT_TERM_CLASS_USER as stcu on (stcu .SubjectGroupID = stc.SubjectGroupID)
				INNER JOIN YEAR_CLASS_USER as ycu ON (stcu.UserID = ycu.UserID)
			 	INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' $ClassLevelCond)
			WHERE
				stct.UserID = '$ParUserID'
				$con
		";
		
//		$con = $SubjectID != "" ? " and SubjectID = $SubjectID" : "";
//		$con .= $ClassID != "" ? " and ClassID = $ClassID" : "";
//		$con .= $SubjectGroupID != "" ? " and SubjectGroupID = $SubjectGroupID" : "";
//		
//		$sql = "
//			SELECT 
//					SubjectTeacherID
//			FROM 
//					RC_SUBJECT_TEACHER
//			WHERE 
//					UserID = $ParUserID
//					$con
//			";
		return $this->returnArray($sql);
		
	}
	
	function returnSemesters($SemID='-1',$lang='')
	{
		$sem_ary = getSemesters($this->schoolYearID,1,$lang);
		
		if ($SemID == '-1')
		{
			return $sem_ary;
		}
		else
		{
			return $sem_ary[$SemID];
		}
	}
	
	function isClassCommentComplete($ReportID, $ClassIDArr)
	{
		if (!is_array($ClassIDArr))
			$ClassIDArr = array($ClassIDArr);
		
		$table = $this->DBName.".RC_CLASS_COMMENT_PROGRESS";
		$sql = "select IsCompleted from $table where ReportID=$ReportID and ClassID in (".implode(',', $ClassIDArr).")";
		$x = $this->returnVector($sql);
		
		if (in_array(0, $x))
			return 0;
		else
			return 1;
	}
	
	function isMSComplete($ReportID, $ClassIDArr, $SubjectIDArr, $SubjectGroupIDArr='')
	{
		global $PATH_WRT_ROOT, $sys_custom;
		
		if (!is_array($SubjectIDArr))
			$SubjectIDArr = array($SubjectIDArr);
		
		$isAllColumnWeightZero = $this->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $SubjectIDArr);
		if($isAllColumnWeightZero)	return -1;
		
		$classID_conds = '';
		if ($ClassIDArr != '')
		{
			if (!is_array($ClassIDArr))
				$ClassIDArr = array($ClassIDArr);
			$classID_conds = " AND ClassID In (".implode(',', $ClassIDArr).") ";
		}
			
		$subjectGroupID_conds = '';
		if ($SubjectGroupIDArr != '')
		{
			if (!is_array($SubjectGroupIDArr))
				$SubjectGroupIDArr = array($SubjectGroupIDArr);
				
			$subjectGroupID_conds = " AND SubjectGroupID In (".implode(',', $SubjectGroupIDArr).") ";
			
			if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'])
			{
				include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
				$numOfSubjectGroup = count($SubjectGroupIDArr);
				for ($i=0; $i<$numOfSubjectGroup; $i++)
				{
					$thisSubjectGroupID = $SubjectGroupIDArr[$i];
					$ObjSG = new subject_term_class($thisSubjectGroupID);
					$thisSubjectComponentID = $ObjSG->SubjectComponentID;
					
					if ($thisSubjectComponentID!='' && $thisSubjectComponentID!=0)
						$SubjectIDArr[] = $thisSubjectComponentID;
				}
			}
		}
		
		if (!is_array($SubjectIDArr))
			$SubjectIDArr = array($SubjectIDArr);
		
		$table = $this->DBName.".RC_MARKSHEET_SUBMISSION_PROGRESS";
		$sql = "select IsCompleted from $table where ReportID=$ReportID and SubjectID in (".implode(',', $SubjectIDArr).") ";
		$sql .= $classID_conds;
		$sql .= $subjectGroupID_conds;
		
		$x = $this->returnVector($sql);
		
//		2011-0324-1519-32071 - 迦密愛禮信中學 - Fail to display correct mark/grade
//		if (in_array(0, $x))
		if (count($x)==0 || in_array(0, $x))
			return 0;
		else
			return 1;
		
		// 20080730 changed by Andy
		// no record -> return -1 will cause serious problem, reportcard can be generated even if NO marksheet is completed
		#return $x[0]=="" ? -1 : $x[0];
		//return $x[0];
	}
	
	function checkCanGenernateReport($ReportID)
	{
		global $eReportCard;
		
		$errAry = array();
		
		# Retrieve Basic Information of Report
		$BasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$AllowClassTeacherComment = $BasicInfo['AllowClassTeacherComment'];
		$SemID = $BasicInfo['Semester'];
		
		# Check Submission date and Verification date
		$SubmissionEnd = substr($BasicInfo['MarksheetSubmissionEnd'],0,10);
		if($SubmissionEnd=="" or $SubmissionEnd>=date("Y-m-d"))				$errAry[] = $eReportCard['SubmissionEndDateNotReached'];
		$VerificationEnd = $BasicInfo['MarksheetVerificationEnd'];
		if($VerificationEnd!="" and $VerificationEnd>=date("Y-m-d"))		$errAry[] = $eReportCard['VerificationEndDateNotReached'];
		
		# Other variables
		$ClassLevelID = $BasicInfo['ClassLevelID'];
		$ClassAry 	= $this->GET_CLASSES_BY_FORM($ClassLevelID);
		$SubjectAry = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		
		$ClassIDArr = Get_Array_By_Key($ClassAry, 'ClassID');
		if($AllowClassTeacherComment)
		{
			# Check is completed (RC_CLASS_COMMENT_PROGRESS)
			$C = $this->isClassCommentComplete($ReportID, $ClassIDArr);
			if(!$C)		$errAry[] = $eReportCard['ClassCommentNotCompleted'];
		}
		
		$SubjectIDArr = array_values(array_keys((array)$SubjectAry));
		
		//foreach($ClassAry as $key => $data)
		//{
			//$ClassID = $data['ClassID'];
			
			# Check is completed (RC_MARKSHEET_SUBMISSION_PROGRESS)
			//foreach($SubjectAry as $SubjectID => $SubjectName)
			//{
				if ($SemID == 'F')
				{
					$C = $this->isMSComplete($ReportID, $ClassIDArr, $SubjectIDArr);
					if(!$C)	$errAry[] = $eReportCard['MSNotCompleted'];
				}
				else
				{
					foreach($SubjectAry as $SubjectID => $SubjectName)
					{
						include_once("subject_class_mapping.php");
						$objSubject = new subject($SubjectID);
						$thisSubjectGroupArr = $objSubject->Get_Subject_Group_List($SemID, $ClassLevelID);
						$thisSubjectGroupIDArr = Get_Array_By_Key($thisSubjectGroupArr, 'SubjectGroupID');
						$numOfSubjectGroup = count($thisSubjectGroupIDArr);
						
						if ($numOfSubjectGroup > 0)
						{
							//for ($i=0; $i<$numOfSubjectGroup; $i++)
							//{
								//$thisSubjectGroupID = $SubjectGroupArr[$i]['SubjectGroupID'];
								$C = $this->isMSComplete($ReportID, '', $SubjectID, $thisSubjectGroupIDArr);
								
								if(!$C)
								{
									$errAry[] = $eReportCard['MSNotCompleted'];
									break;
								}
							//}
						}
					}
				}
			//}
		//}		
		
		return array_unique($errAry);
	}
	
	function returnReportTemplateSubjectWeightData($ReportID, $other_condition="", $SubjectIDArr='', $ReportColumnID='')
	{
		$FuncArgArr = get_defined_vars();
		$PreloadArrKey = 'SubjectWeightDataArr';
		$returnAry = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnAry !== false) {
    		return $returnAry;
    	}
    	
    	
		$cond_SubjectID = '';
		if ($SubjectIDArr != '')
		{
			if (!is_array($SubjectIDArr))
				$SubjectIDArr = array($SubjectIDArr);
			$cond_SubjectID = " And SubjectID In (".implode(',', $SubjectIDArr).") ";
		}
		
		$cond_ReportColumnID = '';
		if ($ReportColumnID !== '')
		{
			if ($ReportColumnID == 'null')
				$cond_ReportColumnID = " And ReportColumnID is Null ";
			else
				$cond_ReportColumnID = " And ReportColumnID = '$ReportColumnID' ";
		}
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
		
		$sql = "	
			SELECT 
				ReportColumnID,
				SubjectID,
				Weight,
				IsDisplay
			FROM 
				$table 
			WHERE
				ReportID = $ReportID
				$cond_SubjectID
				$cond_ReportColumnID
		";
		if($other_condition)	$sql .= " AND " . $other_condition;
		$x = $this->returnArray($sql);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $x);
		return $x;
	}
	
	function getSelectSemester($tags,$selected="")
	{
	         $semester_data = getSemesters($this->schoolYearID, 0);
	         $curYearInfo = getCurrentAcademicYearAndYearTerm();
	         $curYearTermID = $curYearInfo['YearTermID'];
	         
	         $x = "<SELECT $tags>\n";
	         for ($i=0; $i<sizeof($semester_data); $i++)
	         {
	              //$line = split("::",$semester_data[$i]);
	              //list ($name,$current) = $line;
	              list ($thisYearTermID, $thisYearTermName) = $semester_data[$i];
				  if (trim($thisYearTermName) == "") continue;
				  
	              $selected_str = ($selected==$thisYearTermName || ($thisYearTermID==$curYearTermID))? "SELECTED" : "";
	              $x .= "<OPTION value='$thisYearTermID' $selected_str>$thisYearTermName</OPTION>\n";
	         }
	         $x .= "</SELECT>\n";
	         return $x;
	}
	
	# Modified by Andy on 20080729
	# Retrieve two more fields: ClassNoOfStudent & FormNoOfStudent
	# Add GrandSDScore By Marcus 20100210
	function getReportResultScore($ReportID, $ReportColumnID=0, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=1, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='') 
	{
		global $eRCTemplateSetting;
		
		$args = get_defined_vars();
    	$GlobalGetGrandMarksArrVariableStr = '$GlobalGetGrandMarksArr';
    	foreach( (array)$args as  $arg_key => $arg_val)
    	{
    		$key_name = trim($arg_val);	
    		$GlobalGetGrandMarksArrVariableStr.="['".addslashes($key_name)."']";
    	}

    	global $GlobalGetGrandMarksArr;
    	global $eRCTemplateSetting;

    	# check if any identical getMarks function was called before.
    	$GlobalGrandMarksArrExist = false;
    	eval('$GlobalGrandMarksArrExist = isset('.$GlobalGetGrandMarksArrVariableStr.');');
    	
    	if($GlobalGrandMarksArrExist)
    	{
    		$returnAry = '';
    		eval('$returnAry = '.$GlobalGetGrandMarksArrVariableStr.';');
    		return $returnAry;
    	}
    	
		
		$table = $this->DBName.".RC_REPORT_RESULT";
		
		$studentIDCond = "";
		if ($StudentID!='')
			$studentIDCond = " AND StudentID = '$StudentID' ";
			
		$conds_ranking = '';
		if ($FilterNoRanking == 1)
			$conds_ranking = " And ClassNoOfStudent IS NOT NULL And FormNoOfStudent IS NOT NULL ";
			
		$numOfOrder = count((array)$OrderFieldArr);
		$OrderArr = array();
		$OrderStr = '';
		if ($OrderFieldArr != '' && is_array($OrderFieldArr) && $numOfOrder > 0) {
			for ($i=0; $i<$numOfOrder; $i++) {
				$thisOrderField = $OrderFieldArr[$i]['OrderField'];
				$thisOrder = $OrderFieldArr[$i]['Order'];
				
				$OrderArr[] = "$thisOrderField $thisOrder";
			}
			$OrderStr = 'Order By '.implode(',', (array)$OrderArr);
		}
			
		$sql = "
			select
				GrandTotal,
				Promotion,
				NewClassName, 
				Passed,
				GrandAverage,
				OrderMeritClass,
				OrderMeritForm,
				ClassNoOfStudent,
				FormNoOfStudent,
				GPA,
				StudentID,
				ReportColumnID,
				OrderMeritStream,
				StreamNoOfStudent,
				GrandSDScore				
			from 
				$table
			where
				ReportID = $ReportID 
				and ReportColumnID = $ReportColumnID
				$conds_ranking
				$studentIDCond
				$other_conds
			$OrderStr
		";
		$result = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		
		
		
		/*
		if ($StudentID!='')
		{
			$returnArr = $result[0];
			
			if (is_array($returnArr))
			{
				//debug_pr($ManualAdjustedArr);
				if($includeAdjustedMarks)
				{
					foreach($returnArr as $key => $data)
					{
						//debug(!empty($ManualAdjustedArr[$StudentID][$ReportColumnID][$key])." ".$ManualAdjustedArr[$StudentID][$ReportColumnID][$key]);
						if(!empty($ManualAdjustedArr[$StudentID][$ReportColumnID][0][$key]))
						{
							$returnArr[$key] = $ManualAdjustedArr[$StudentID][$ReportColumnID][0][$key];
						}
					}
				}
			}
		}
		else
		{
			foreach($result as $key=>$data)
			{
				$thisStudentID = $data['StudentID'];
				$thisReportColumnID = $data['ReportColumnID'];

				foreach ($data as $db_FieldName => $value)
				{
					if($includeAdjustedMarks&&!empty($ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$db_FieldName]))
						$returnArr[$thisStudentID][$thisReportColumnID][$db_FieldName] = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$db_FieldName];
					else
						$returnArr[$thisStudentID][$thisReportColumnID][$db_FieldName] = $value;
				}
			}
		}
		*/
		
		# Get Manual Adjusted Marks - Marcus 20090930
		if($includeAdjustedMarks)
			$ManualAdjustedArr = $this->Get_Manual_Adjustment($ReportID);	
		
		if ($CheckPositionDisplay == 1)
		{
			### Position Display Settings
			// $PositionDisplaySettingArr[$SubjectID][Class, Form] = array(values)
			$PositionDisplaySettingArr = $this->Get_Position_Display_Setting($ReportID, $SubjectID='-1');
			
			### Subject Average Mark
			// $SubjectAverageArr[$thisSubjectID][$thisReportColumnID] = Average
			$SubjectAverageArr = $this->Get_Report_Average_Mark($ReportID);
			
			### Get Position Determine Field
			$AverageCompareField = $this->Get_Grand_Position_Determine_Field();
			$thisGrandSubjectID = $this->Get_Grand_Field_SubjectID($AverageCompareField);
			
			### Get Passing Mark
			$PassingMark = $this->Get_Subject_Passing_Mark($thisGrandSubjectID, $ReportID);	
			
			### Get Grand Mark Statistics
			$StatisticsArr = $this->Get_Scheme_Grade_Statistics_Info($ReportID, $thisGrandSubjectID, $forSubjectGroup=0);
		}
		
		foreach($result as $key=>$data)
		{
			$thisStudentID = $data['StudentID'];
			$thisReportColumnID = $data['ReportColumnID'];
			$thisAverageCompareValue = $data[$AverageCompareField];
			
			foreach ($data as $db_FieldName => $value)
			{
				
				if($includeAdjustedMarks && !empty($ManualAdjustedArr[$thisStudentID][$thisReportColumnID][0][$db_FieldName]))
				{
					$returnArrTemp[$thisStudentID][$thisReportColumnID][$db_FieldName] = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][0][$db_FieldName];
				}
				else
				{
					if ($CheckPositionDisplay == 0)
					{
						$returnArrTemp[$thisStudentID][$thisReportColumnID][$db_FieldName] = $value;
					}
					else
					{
						if ($db_FieldName == 'OrderMeritClass' || $db_FieldName == 'OrderMeritForm' || $db_FieldName == 'OrderMeritStream')
						{
							### Class Checking
							if ($db_FieldName == 'OrderMeritClass')
							{
								$thisClassInfoArr = $this->Get_Student_Class_ClassLevel_Info($thisStudentID);
								$thisYearClassID = $thisClassInfoArr[0]['ClassID'];
								
								$thisRecordType = 'Class';
								$thisNumOfStudentTemp = $data['ClassNoOfStudent'];
							}
							else if ($db_FieldName == 'OrderMeritForm')
							{
								$thisYearClassID = 0;
								$thisRecordType = 'Form';
								$thisNumOfStudentTemp = $data['FormNoOfStudent'];
							}
							else if ($db_FieldName == 'OrderMeritStream')
							{
								$thisYearClassID = 0;
								$thisRecordType = 'Form';
								$thisNumOfStudentTemp = $data['StreamNoOfStudent'];
							}
							
							# Get Display Settings
							$thisSubjectID = '-1';
							$thisRangeType = $PositionDisplaySettingArr[$thisSubjectID][$thisRecordType]['RangeType'];
							$thisRangeType = ($thisRangeType=='')? 1 : $thisRangeType;
							$thisUpperLimit = $PositionDisplaySettingArr[$thisSubjectID][$thisRecordType]['UpperLimit'];
							$thisUpperLimit = ($thisUpperLimit=='')? 0 : $thisUpperLimit;
							$thisGreaterThanAverage = $PositionDisplaySettingArr[$thisSubjectID][$thisRecordType]['GreaterThanAverage'];
							$thisGreaterThanAverage = ($thisGreaterThanAverage=='')? 0 : $thisGreaterThanAverage;
							$thisShowPosition = $PositionDisplaySettingArr[$thisSubjectID][$thisRecordType]['ShowPosition'];
							$thisShowPosition = ($thisShowPosition=='')? 1 : $thisShowPosition;
							
							
							# Compare Average
							//$thisAverageMark = $SubjectAverageArr[$AverageCompareField][$ReportColumnID][$thisYearClassID];
							$thisAverageDifference = $thisAverageCompareValue - $PassingMark;
							
							$PassAverageChecking = true;
							if (($thisGreaterThanAverage != 0 && $thisAverageDifference < $thisGreaterThanAverage) || $thisShowPosition!=1)
							{
								$returnArrTemp[$thisStudentID][$thisReportColumnID][$db_FieldName] = '';
								$PassAverageChecking = false;
							}
							
							if ($PassAverageChecking == true)
							{
								# Position Range Checking
								if ($thisRangeType == 1)
								{
									# Position Range
									$thisCompareValue = $value;
									$numOfStudent = $thisNumOfStudentTemp;
								}
								else
								{
									if ($thisRangeType == 2)
									{
										# All students in class
										$numOfStudent = $thisNumOfStudentTemp;
									}
									else if ($thisRangeType == 3)
									{
										# All PASSED sutdents in class
										$numOfStudent = 0;
										foreach ((array)$StatisticsArr as $thisGrade => $thisStatArr)
										{
											$thisNature = $thisStatArr[$thisYearClassID]['Nature'];
											$thisNumOfStudent = $thisStatArr[$thisYearClassID]['NumOfStudent'];
											
											if ($thisNature == 'Distinction' || $thisNature == 'Pass')
												$numOfStudent += $thisNumOfStudent;
										}
									}
									
									if ($numOfStudent > 0)
										$thisPositionPercentage = ($value / $numOfStudent) * 100;
										
									$thisCompareValue = $thisPositionPercentage;
								}
								
								if ($thisUpperLimit != 0 && $thisCompareValue > $thisUpperLimit && $numOfStudent > 0)
									$returnArrTemp[$thisStudentID][$thisReportColumnID][$db_FieldName] = '';
								else
									$returnArrTemp[$thisStudentID][$thisReportColumnID][$db_FieldName] = $value;
							}
						}
						else
						{
							$returnArrTemp[$thisStudentID][$thisReportColumnID][$db_FieldName] = $value;
						}
					}
				}
			}
		}
		
		$returnArr = array();
		if ($StudentID != '')
		{
			$returnArr = $returnArrTemp[$StudentID][$ReportColumnID];
		}
		else
		{
			$returnArr = $returnArrTemp;
		}
		
		eval($GlobalGetGrandMarksArrVariableStr.' = $returnArr;');
		unset($PositionDisplaySettingArr);
		unset($SubjectAverageArr);
		unset($StatisticsArr);
		unset($ManualAdjustedArr);
		unset($returnArrTemp);
		
		
		//debug_pr($returnArr);
		return $returnArr;
	}
	
	
	/*
	Action of insert ReportCard Basic Information (Step 1)
	ref: /home/eAdmin/StudentMgmt/eReportCard/settings/reportcard_templates/new_update.php
	*/
	function InsertReportTemplateBasicInfo($data)
	{	
		//$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE";
 		$id = "";
 		
 		if(!empty($data))
 		{
	 		foreach ($data as $key => $value) {
				$k[] = $key;
				$v[] = "'".$value."'";
			}
			$k[] = "DateInput";
			$v[] = "NOW()";
			
			$sql = "INSERT INTO $table (";
	 		$sql .= implode(",", $k);
			$sql .= ") VALUES (";
			$sql .= implode(",", $v);
			$sql .= ")";
			
			$result = $this->db_db_query($sql);
			$id = $this->db_insert_id();
		}
		return $id;
		
	}
	
	function InsertReportTemplateColumn($data)
	{	
		$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
 		
 		$sql = "INSERT INTO $table VALUES ";
 		
 		foreach($data as $key => $ary)
 		{
	 		if($ary['SemesterNum'] != 0 )
			{
				// Check semester duplicate or not
				$dupsql = "SELECT ReportColumnID FROM $table where ReportID=". $ary['ReportID'] ." and SemesterNum=".$ary['SemesterNum'];
				$dupresult = $this->returnVector($dupsql);
				if($dupresult[0]) continue;
			}
			
			$ColumnTitle = $ary['ColumnTitle'] ? "'". $ary['ColumnTitle'] ."'" : "''";
			$SemesterNum = $ary['SemesterNum'] ? "'". $ary['SemesterNum'] ."'" : 0;
			$DefaultWeight = $ary['DefaultWeight'] ? "'". $ary['DefaultWeight'] ."'" : "NULL";
			$IsDetails = $ary['IsDetails'] ? "'". $ary['IsDetails'] ."'" : "NULL";
			
			$insertItem[] = "(
				NULL, 
				'". $ary['ReportID'] ."',
				". $ColumnTitle .",
				". $DefaultWeight .",
				'". $ary['DisplayOrder'] ."',
				'". $ary['ShowPosition'] ."',
				'". $ary['PositionRange'] ."',
				'". $ary['BlockMarksheet'] ."',
				". $SemesterNum .",
				". $IsDetails .",
				NOW(),
				NULL)
			";
 		}
 		
 		$id="";
 		if (isset($insertItem) && sizeof($insertItem) > 0) {
			$sql .= implode(",", $insertItem);
			$result = $this->db_db_query($sql);
			$id = $this->db_insert_id();
		}
 		return $id;
	}
	
	/*
	Action of update ReportCard Basic Information (Step 1)
	ref: /home/eAdmin/StudentMgmt/eReportCard/settings/reportcard_templates/new_update.php
	*/
	function UpdateReportTemplateBasicInfo($ReportID, $data)
	{	
		$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE";
 		
 		foreach ($data as $key => $value) {
	 		$temp[] .= $key ." = ". "'".$value."'";
		}

		$sql = "UPDATE $table set ";
 		$sql .= implode(",", $temp);
		$sql .= " WHERE ReportID=$ReportID";
		$result = $this->db_db_query($sql);
		
		return $result;
		
	}
	
	function RemoveReportTemplate($ReportID)
	{	
		$this->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportID', $ReportID);
		$this->DELETE_REPORT_TEMPLATE_COLUMN('ReportID', $ReportID);
 		$table = $this->DBName.".RC_REPORT_TEMPLATE";
 		$sql = "DELETE FROM $table WHERE ReportID=$ReportID";
		return $this->db_db_query($sql);
	}
	
	# DELETE a report template subject weight specify by column name and its value
	#. e.g. $column = "ReportColumnID", "ReportID"
	function DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT($column, $value, $other='') {
		$success = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
		$sql = "DELETE FROM $table WHERE $column = '$value'";
		$sql .= ($other) ? " AND ".$other : "";
		
		$success = $this->db_db_query($sql);
		return success;
	}
	
	function GET_EXCLUDE_ORDER_STUDENTS() {
		global $intranet_root;
		include_once($intranet_root."/includes/libfilesystem.php");
		$lf = new libfilesystem();
		$excludeStudents = trim($lf->file_read($intranet_root."/file/reportcard2008/exclude_order_students.txt"));
		$excludeStudentsArray = array();
		if(!empty($excludeStudents)) {
			$excludeStudentsArray = explode(",", $excludeStudents);
		}
		return $excludeStudentsArray;
	}
	
	function returnReportTemplateCalculation($ReportID)
	{
		$semAry = $this->returnReportTemplateBasicInfo($ReportID);
		$sem = $semAry['Semester'];
		$t = $this->LOAD_SETTING('Calculation');
		if($sem =="F")				// Whole Year
			return $t[OrderFullYear];	
		else						// Term 
			return $t[OrderTerm];
	}
	
	function returnSubjectIDwOrder($ClassLevelID, $ExcludeCmpSubject=0, $ReportID='')
	{
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject, $ReportID);
		
		$SubjectIDAry = array();
		if(!empty($SubjectArray))
		{
			foreach($SubjectArray as $key => $val)
			{
				$SubjectIDAry[] = $key;
				if(sizeof($val)>1)
				{
					foreach($val as $key1 => $val1)
					{
						if($key1)	$SubjectIDAry[] = $key1;
					}
				}
			}
		}
		
		return $SubjectIDAry;
		
	}
	
	function returnReportColoumnTitleWithOrder($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "SELECT DisplayOrder, ColumnTitle, SemesterNum FROM $table where ReportID = $ReportID ORDER BY DisplayOrder";
		$result = $this->returnArray($sql);
		
		foreach($result as $key => $val)
		{
			list($DisplayOrder, $ColumnTitle, $SemesterNum) = $val;
			$x[$DisplayOrder] = $ColumnTitle ? $ColumnTitle : $this->returnSemesters($SemesterNum);
		}
		
		return $x;
	}
	
	function UpdateReportTemplateColumn($data, $where)
	{	
		$result = false;
 		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
 		
 		foreach ($data as $key => $value) {
	 		$temp[] .= $key ." = ". "'".$value."'";
		}

		$sql = "UPDATE $table set ";
 		$sql .= implode(",", $temp);
		$sql .= " WHERE $where ";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function InsertReportTemplateSubjectWeight($ReportID, $data)
	{
		$result = false;
		$table = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";
		
		$sql = "INSERT INTO $table VALUES ";
		
		foreach ($data as $key => $value) 
		{
			$SubjID = $value['SubjectID'] ? "'". $value['SubjectID'] ."'" : "NULL";
			$ReportColumnID = $value['ReportColumnID'] ? "'". $value['ReportColumnID'] ."'" : "NULL";
			$IsDisplay = $value['IsDisplay'] ? "'". $value['IsDisplay'] ."'" : "NULL";
			
			$insertItem[] = "(NULL, '$ReportID', ". $ReportColumnID .", ". $SubjID .", ". $IsDisplay .", '". $value['Weight'] ."' ,NOW(), NULL)";
		}
		
		if (isset($insertItem) && sizeof($insertItem) > 0) {
			$sql .= implode(",", $insertItem);
			$result = $this->db_db_query($sql);
		}
		
		return $result;
	}
	
	function UpdateReportTemplateColumnOrder($ReportID, $curPos, $newPos)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		// find the curPos ID
		$sql = "select ReportColumnID from $table where ReportID=$ReportID and DisplayOrder=$curPos";
		$x = $this->returnArray($sql);
		$curColID = $x[0][0];

		$sql = "select ReportColumnID, DisplayOrder from $table where ReportID=$ReportID and ReportColumnID<>$curColID order by DisplayOrder";
		$x = $this->returnArray($sql);
		for($i=0;$i<sizeof($x);$i++)
		{
			if($x[$i]['DisplayOrder']<=$newPos)	continue;
			if($x[$i]['DisplayOrder']>$newPos)	
			{
				$sql1 = "update $table set DisplayOrder = ". ($x[$i]['DisplayOrder']+1) ." where ReportColumnID=".$x[$i]['ReportColumnID'];
				$this->db_db_query($sql1);
			}
		}
		$sql1 = "update $table set DisplayOrder = $newPos+1 where ReportColumnID=".$curColID;
		$this->db_db_query($sql1);
	}
	
	function ChangeReportTemplateColumnOrder($ReportID, $ColID, $Order)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		// find curPos and newPos
		$sql = "select ReportColumnID, DisplayOrder from $table where ReportID=$ReportID order by DisplayOrder";
		$x = $this->returnArray($sql);
		
		for($i=0;$i<sizeof($x);$i++)
		{
			if($ColID==$x[$i]['ReportColumnID'])	
			{	
				$data[0]['ReportColumnID'] = $x[$i]['ReportColumnID'];
				$data[0]['DisplayOrder'] = $x[$i]['DisplayOrder'];

				$data[1]['ReportColumnID'] = $x[($Order==1?$i+1:$i-1)]['ReportColumnID'];
				$data[1]['DisplayOrder'] = $x[($Order==1?$i+1:$i-1)]['DisplayOrder'];
				break;
			}
		}
		$this->SwapReportTemplateColumnOrder($ReportID, $data);
	}
	
	function SwapReportTemplateColumnOrder($ReportID, $data)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
		
		$sql = "update $table set DisplayOrder=". $data[1]['DisplayOrder'] ." where ReportColumnID=".$data[0]['ReportColumnID'];
		$this->db_db_query($sql);
		
		$sql = "update $table set DisplayOrder=". $data[0]['DisplayOrder'] ." where ReportColumnID=".$data[1]['ReportColumnID'];
		$this->db_db_query($sql);
	}	
	
	function isReportTemplateComplete($ReportID)
	{
 		$table_TEMPLATE					 = $this->DBName.".RC_REPORT_TEMPLATE";
 		$table_TEMPLATE_COLUMN			 = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";
 		$table_TEMPLATE_SUBJECT_WEIGHT	 = $this->DBName.".RC_REPORT_TEMPLATE_SUBJECT_WEIGHT";

		//check LineHeight
		//$sql = "select ClassLevelID, LineHeight from $table_TEMPLATE where ReportID=$ReportID";
		//$x = $this->returnArray($sql);
		//if($x[0]['LineHeight']=="")	return false;
		//$ClassLevelID = $x[0]['ClassLevelID'];
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		if($ReportInfoArr['LineHeight']=="")	return false;
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		//check column exists or not
		$sql = "select ReportColumnID from $table_TEMPLATE_COLUMN where ReportID=$ReportID";
		$ReportColumnIDArr = $this->returnVector($sql);
		if(empty($ReportColumnIDArr))		return false;
			
		$SubjectIDArr = $this->returnSubjectIDwOrder($ClassLevelID);
		  
		//check weight
//		for($i=0;$i<sizeof($x);$i++)
//		{
//			$ColID = $x[$i][0];
//			foreach($SubjectArray as $key => $SubjID)
//			{
//				$sql = "select count(*) from $table_TEMPLATE_SUBJECT_WEIGHT where ReportColumnID=$ColID and SubjectID=$SubjID";
//				$x = $this->returnArray($sql);
//				if($x[0][0]==0)
//				{	
//					if ($ReportID==98)
//					{
//						debug_pr('$ColID = '.$ColID);
//						debug_pr('$SubjID = '.$SubjID);
//					}
//					return false;
//				}
//			}			 
//		}	

		$sql = "Select
						ReportColumnID, SubjectID, count(*) as WeightCount
				From 
						$table_TEMPLATE_SUBJECT_WEIGHT 
				Where 
						ReportColumnID in (".implode(',', (array)$ReportColumnIDArr).") 
						And SubjectID in (".implode(',', (array)$SubjectIDArr).")
				Group By
						ReportColumnID, SubjectID
				";
		$SubjectColumnWeightArr = $this->returnArray($sql);
		$SubjectColumnWeightAssoArr = BuildMultiKeyAssoc($SubjectColumnWeightArr, array('ReportColumnID', 'SubjectID'), array('WeightCount'), $SingleValue=1);
		
		$numOfReportColumn = count($ReportColumnIDArr);
		$numOfSubject = count($SubjectIDArr);
		for ($i=0; $i<$numOfReportColumn; $i++)
		{
			$thisReportColumnID = $ReportColumnIDArr[$i];
			for ($j=0; $j<$numOfSubject; $j++)
			{
				$thisSubjectID = $SubjectIDArr[$j];
				$thisHasWeight = $SubjectColumnWeightAssoArr[$thisReportColumnID][$thisSubjectID];
				
				if ($thisHasWeight == '' || $thisHasWeight == 0)
					return false;
			}
		}
		
		
		//check ration 
		$cal = $this->returnReportTemplateCalculation($ReportID);
		if($cal==1)
			$sql = "select count(*) from $table_TEMPLATE_SUBJECT_WEIGHT where ReportID=$ReportID and ReportColumnID is null and SubjectID is not null";
		else
			$sql = "select count(*) from $table_TEMPLATE_SUBJECT_WEIGHT where ReportID=$ReportID and SubjectID is null and ReportColumnID is not null";			
		$x = $this->returnArray($sql);
		if($x[0][0]==0)	return false;
		
		return true;
	}
	
	function ReportTemplateFormCopy($frForm='', $toForm, $toTerm='', $frReportID='')
	{
		$table_TEMPLATE = $this->DBName.".RC_REPORT_TEMPLATE";
		$copyResult	= array();
		
		if($frReportID)		// single report copy
		{
			
			$sql = "select * from $table_TEMPLATE where ReportID = $frReportID";
			$template = $this->returnArray($sql);
			
			$copyResult[$frReportID] = $this->ReportTemplateTermCopy($toForm, $toTerm, $template[0], $frReportID);
			
		}
		else				// all reports copy
		{
			//retrieve RC_REPORT_TEMPLATE (frForm)
			$sql = "select * from $table_TEMPLATE where ClassLevelID = $frForm order by ReportTitle";
			$template = $this->returnArray($sql);
			
			for($i=0;$i<sizeof($template);$i++)
			{
				$frReportID = $template[$i][ReportID];
				$copyResult[$frReportID] = $this->ReportTemplateTermCopy($toForm, $toTerm, $template[$i], '');
			}
		}
		
		return $copyResult;
	}
	
	function ReportTemplateTermCopy($toForm, $toTerm='', $template, $frReportID='')
	{
		$table_TEMPLATE = $this->DBName.".RC_REPORT_TEMPLATE";
		
		if($frReportID)		// single report copy
		{
			$Semester = $template[Semester] == "F"? $template[Semester] : $toTerm;
		}
		else				// all reports copy
		{
			$Semester = $template[Semester];
			$CopyAllReport = 1;
		}
		
		$frReportID = $template[ReportID];
		$SubjMatched = 1;
		
		// Check if the report is a main report
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($frReportID);
		$isMainReport = $ReportInfoArr['isMainReport'];
		
		// Check template already exists
		if($Semester != "F" && $isMainReport == 1)
		{
			$dupStr = "select ReportID from $table_TEMPLATE where ClassLevelID = $toForm and Semester = '". $Semester ."' And isMainReport = 1";
			$dup = $this->returnArray($dupStr);
			if(sizeof($dup) > 0)
			{
				return -1;
				continue;
			}
		}
		
		// copy RC_REPORT_TEMPLATE
		$data = array();
		foreach ((array)$template as $field => $value)
		{
			// Date Input is defined in function InsertReportTemplateBasicInfo 
			// ReportID is the key => cannot duplicated
			if (is_numeric($field) == false && $field != 'DateInput' && $field != 'ReportID')
				$data[$field] = $this->Get_Safe_Sql_Query($value);
		}
		$data['Semester'] = $Semester;
		$data['ClassLevelID'] = $toForm;
		$ReportID = $this->InsertReportTemplateBasicInfo($data);
		
		$this->COPY_CLASSLEVEL_SETTINGS($toForm, $toForm, 0, $ReportID); // Copy grading scheme from global to newly added report first. (setup new set of report based grading scheme)
		if(!$CopyAllReport)
		{
			$this->COPY_CLASSLEVEL_SETTINGS("", $toForm, $frReportID, $ReportID);
		}
		
		//copy RC_REPORT_TEMPLATE_COLUMN
		$template_col_data = $this->returnReportTemplateColumnData($frReportID);
		foreach($template_col_data as $key=>$coldata)
		{
			$data = array();
			$data[0][ReportID] 		= $ReportID;
			$data[0][ColumnTitle] 	= $coldata[ColumnTitle];
			$data[0][DefaultWeight]	= $coldata[DefaultWeight];
			$data[0][DisplayOrder] 	= $coldata[DisplayOrder];
			$data[0][ShowPosition] 	= $coldata[ShowPosition];
			$data[0][PositionRange] = $coldata[PositionRange];
			$data[0][SemesterNum] 	= $coldata[SemesterNum];
			$data[0][IsDetails] 	= $coldata[IsDetails];
			$frReportColumnID		= $coldata[ReportColumnID];	
			$ReportColumnID = $this->InsertReportTemplateColumn($data);
							
			//copy RC_REPORT_TEMPLATE_SUBJECT_WEIGHT
			$SubjectArray = $this->returnSubjectIDwOrder($toForm);
			$SubjMatched = empty($SubjectArray) ? 0 : $SubjMatched;
			
			$sw = 0;
			$data = array();
			foreach($SubjectArray as $key=>$SubjID)
			{
				$other_condition = "ReportColumnID=$frReportColumnID and SubjectID=$SubjID";
				$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition);
				
				if(empty($weight_data_ary))	
				{
					$SubjMatched = 0;
					continue;	
				}
									
				$weight_data = $weight_data_ary[0];
				
				$data[$sw][ReportID] 		= $ReportID;
				$data[$sw][ReportColumnID] 	= $ReportColumnID;
				$data[$sw][SubjectID] 		= $SubjID;
				$data[$sw][IsDisplay] 		= $weight_data[IsDisplay];
				$data[$sw][Weight] 			= $weight_data[Weight];
				$sw++;
			}
			
			$this->InsertReportTemplateSubjectWeight($ReportID, $data);
			
			// Column data of !SubjID & ColID
			$data = array();
			$other_condition = "ReportColumnID = $frReportColumnID and SubjectID is NULL";
			$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition);
			
			if(!empty($weight_data_ary))	
			{
				$weight_data = $weight_data_ary[0];
				$data = array();
				$data[0][ReportID] 			= $ReportID;
				$data[0][ReportColumnID] 	= $ReportColumnID;
				$data[0][IsDisplay] 		= $weight_data[IsDisplay];
				$data[0][Weight] 			= $weight_data[Weight];
				$this->InsertReportTemplateSubjectWeight($ReportID, $data);
			}
		}
		
		$Cal = $this->returnReportTemplateCalculation($ReportID);
		if($Cal==1)
		{
			// Column data of SubjID & !ColID
			$data = array();
			$sw = 0;
			$SubjectArray = $this->returnSubjectIDwOrder($toForm);
			foreach($SubjectArray as $key=>$SubjID)
			{
				$other_condition = "ReportColumnID is NULL and SubjectID=$SubjID";
				$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition);
				
				if(empty($weight_data_ary))	
				{
					$SubjMatched = 0;
					continue;	
				}
				
				$weight_data = $weight_data_ary[0];
				
				$data[$sw][ReportID] 		= $ReportID;
				$data[$sw][SubjectID] 		= $SubjID;
				$data[$sw][IsDisplay] 		= $weight_data[IsDisplay];
				$data[$sw][Weight] 			= $weight_data[Weight];
				$sw++;
			}
			$this->InsertReportTemplateSubjectWeight($ReportID, $data);
		}
		
		if($Cal==2)
		{
			// !SubjID & !ColID
			$other_condition = "ReportColumnID is NULL and SubjectID is NULL";
			$weight_data_ary = $this->returnReportTemplateSubjectWeightData($frReportID, $other_condition);
			$data = array();
			$data[0][ReportID] 		= $ReportID;
			$data[0][IsDisplay] 	= $weight_data[IsDisplay];
			$data[0][Weight] 		= $weight_data[Weight];
			$this->InsertReportTemplateSubjectWeight($ReportID, $data);	
		}
		
		// Copy Position Display Settings
		$SuccessArr['CopyPositionDisplaySettings'] = $this->Copy_Position_Display_Setting($frReportID, $ReportID);
		
		return $SubjMatched;
	}
	
	function returnSunjectTeacherSubject($UserID, $ClassLevelID)
	{
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		
		# Modified by Marcus 20101011 (for template based grading scheme) 
			$cond_ReportID = " AND d.ReportID = 0 ";

	 	$sql = "SELECT
	 					DISTINCT(st.SubjectID)
	 			FROM
	 					SUBJECT_TERM_CLASS_TEACHER as stct
	 				INNER JOIN
	 					SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr
		 			ON (stct.SubjectGroupID = stcyr.SubjectGroupID
		 				AND stct.UserID = '$UserID'
		 				AND stcyr.YearID = '$ClassLevelID')
		 			INNER JOIN
		 				SUBJECT_TERM as st
		 			ON (st.SubjectGroupID = stct.SubjectGroupID)
		 			INNER JOIN
		 				ASSESSMENT_SUBJECT as s
		 			ON (st.SubjectID = s.RecordID)
		 			INNER JOIN
		 				ACADEMIC_YEAR_TERM as ayt
		 			ON (st.YearTermID = ayt.YearTermID 
		 				AND ayt.AcademicYearID = '".$this->schoolYearID."')
		 			inner join $table as d on (d.SubjectID=st.SubjectID and d.ClassLevelID=stcyr.YearID $cond_ReportID)
		 		ORDER BY 
						s.DisplayOrder
		 		";
		return $this->returnArray($sql);
	}
	
	function returnSubjectTeacherComment($ReportID, $StudentID='')
	{
		$table 	= $this->DBName.".RC_MARKSHEET_COMMENT";
		
		$x = array();
		if($StudentID)
		{
			$sql = "select SubjectID, Comment from $table where ReportID=$ReportID and StudentID=$StudentID";
			$result = $this->returnArray($sql);
			
			foreach($result as $k=>$data)
				$x[$data['SubjectID']] = stripslashes($data['Comment']);
		}
		
		return $x;
	}
	
	function returnSubjectTeacherCommentByBatch($ReportID, $StudentID='', $SubjectID='')
	{
		$table 	= $this->DBName.".RC_MARKSHEET_COMMENT";
		
		if(trim($StudentID)!='')
			$cond_StudentID = " and StudentID IN (".implode(",",(array)$StudentID).") ";

		if(trim($SubjectID)!='')
			$cond_SubjectID = " and SubjectID IN (".implode(",",(array)$SubjectID).") ";
		
		$x = array();
		$sql = "select SubjectID, Comment, AdditionalComment, StudentID from $table where ReportID=$ReportID $cond_StudentID $cond_SubjectID";
		$result = $this->returnResultSet($sql);
		
		$x = BuildMultiKeyAssoc($result, array("SubjectID","StudentID"), array("Comment", "AdditionalComment"), 1);
		
		return $x;
	}

	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function returnSubjectFullMark($ClassLevelID, $withSub=1, $ParSubjectIDArr=array(), $ParInputScale=0, $ReportID='')
	{
		$table_RC_SUBJECT_FORM_GRADING 	= $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$table_RC_GRADING_SCHEME 		= $this->DBName.".RC_GRADING_SCHEME";
		$table_RC_GRADING_SCHEME_RANGE	= $this->DBName.".RC_GRADING_SCHEME_RANGE";
		
		
		if (count($ParSubjectIDArr)==0)
		{
			if($withSub)
				$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
			else
				$SubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		}
		else
		{
			for ($i=0; $i<count($ParSubjectIDArr); $i++)
			{
				$thisSubjectID = $ParSubjectIDArr[$i];
				$SubjectArray[$thisSubjectID] = "";
			}
		}
		
		if(trim($ReportID)=='')
			$cond_ReportID = " AND ReportID = 0 ";
		else
			$cond_ReportID = " AND ReportID = '$ReportID' ";
		
		$x = array();
		if (is_array($SubjectArray))
		{
			foreach($SubjectArray as $SubjectID => $SubjectName)
			{
				# retrieve SchemeID & ScaleDisplay
				$sql = "SELECT SchemeID, ScaleDisplay, ScaleInput FROM $table_RC_SUBJECT_FORM_GRADING WHERE SubjectID=$SubjectID and ClassLevelID=$ClassLevelID $cond_ReportID";
				$result = $this->returnArray($sql);
				$SchemeID= $result[0]['SchemeID'];
				$ScaleDisplay = $ParInputScale?$result[0]['ScaleInput']:$result[0]['ScaleDisplay'];
				
				# retrieve SchemeType & FullMark
				$sql = "SELECT SchemeType, FullMark FROM $table_RC_GRADING_SCHEME WHERE SchemeID=$SchemeID";
				$result = $this->returnArray($sql);
				
				$SchemeType = $result[0]['SchemeType'];
				$FullMark = $result[0]['FullMark'];
				
				if($SchemeType=="H")
				{
					if($ScaleDisplay=="M")		
					{
						$x[$SubjectID] = $FullMark;
					}
					else
					{
//						$sql = "SELECT Grade FROM $table_RC_GRADING_SCHEME_RANGE WHERE SchemeID=$SchemeID and Grade!='' and Nature!='F' order by GradingSchemeRangeID";
						$sql = "SELECT Grade, IF(Nature = 'D',2,1) AS TmpNature  FROM $table_RC_GRADING_SCHEME_RANGE WHERE SchemeID=$SchemeID and Grade!='' and Nature!='F' order by TmpNature DESC, LowerLimit DESC, GradingSchemeRangeID ASC";
						$result = $this->returnVector($sql);
						$x[$SubjectID] = $result[0];
					}
				}
				else
				{
					$x[$SubjectID] = $this->EmptySymbol;
				}
			}
		}
		
		return $x;
		
	}
	
	function getCusTemplate()
	{
		global $PATH_WRT_ROOT, $intranet_root, $ReportCardCustomSchoolName;
		
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateNo = $eRCTemplateSetting['CusTemplate'];
		$TemplatePath = $PATH_WRT_ROOT."includes/libreportcard2008_".$TemplateNo.".php";
		if(file_exists($TemplatePath))
		{
			include_once($TemplatePath);
			$eRCtemplate = new libreportcardSIS();
			return $eRCtemplate;
		}
		else
		{
			return "";
		}
	}
	
	function getTemplateCSS()
	{
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName;
		
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateCSS = $eRCTemplateSetting['CSS'];
		
		$TemplateCSSPath = $PATH_WRT_ROOT."file/reportcard2008/templates/". $TemplateCSS;
		
		if($TemplateCSS != "" && file_exists($TemplateCSSPath))
			return $TemplateCSSPath;
		else
			return false;
	}
	
	function getSubMSTemplateCSS()
	{
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName,$eRCTemplateSetting;
		
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateCSS = $eRCTemplateSetting['SubMS']['CSS'];
		
		$TemplateCSSPath = $PATH_WRT_ROOT."file/reportcard2008/templates/". $TemplateCSS;
		
		if($TemplateCSS != "" && file_exists($TemplateCSSPath))
			return $TemplateCSSPath;
		else
			return false;
	}

	function getAssessmentTemplateCSS()
	{
		global $PATH_WRT_ROOT, $ReportCardCustomSchoolName,$eRCTemplateSetting;
		
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$TemplateCSS = $eRCTemplateSetting['AssessmentReport']['CSS'];
		
		$TemplateCSSPath = $PATH_WRT_ROOT."file/reportcard2008/templates/". $TemplateCSS;
		
		if($TemplateCSS != "" && file_exists($TemplateCSSPath))
			return $TemplateCSSPath;
		else
			return false;
	}
	
	
	function getFooter($ReportID)
	{
		global $eReportCard;
		$FooterRow = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Footer = $ReportSetting['Footer'];
			if ($Footer!="")
			{
				$FooterRow = "<tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n";
			}
			else
			{
				$FooterRow = "";
			}
		}
		
		return $FooterRow;
	}
	
	function GET_STUDENT_BY_CLASSLEVEL($ParReportID, $ParClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0)
	{
		if($ParStudentID!="") {
			$cond = " AND ycu.UserID IN ($ParStudentID)";
		} else if($ParClassID!="") {
			if(is_array($ParClassID))
			{
				$ClassIDStr = implode(",",$ParClassID);
				$cond = " AND yc.YearClassID IN ($ClassIDStr)";
			}
			else
				$cond = " AND yc.YearClassID = '$ParClassID'";
		}
		
		//$ClassNumField = getClassNumberField("ycu.");
		$NameField = getNameFieldByLang2('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		if ($isShowStyle==0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
			
		$sql = "SELECT 
						DISTINCT(ycu.UserID), 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
							ELSE ".$NameField." 
						END as StudentName,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML', au.EnglishName) 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML', u.EnglishName) 
							ELSE u.EnglishName
						END as StudentNameEn,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML', au.ChineseName) 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML', u.ChineseName) 
							ELSE u.ChineseName
						END as StudentNameCh,
						yc.ClassTitleEN as ClassTitleEn,
						yc.ClassTitleB5 as ClassTitleCh,
						yc.YearClassID,
						u.WebSAMSRegNo,
						u.UserLogin
				FROM 
						YEAR_CLASS as yc
						INNER JOIN
						YEAR_CLASS_USER as ycu 
						ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."')
						LEFT JOIN 
						INTRANET_USER as u 
						ON (ycu.UserID = u.UserID)
						Left Join
						INTRANET_ARCHIVE_USER as au 
						ON (ycu.UserID = au.UserID) 
				WHERE 
						yc.YearID = '$ParClassLevelID'
						$cond
				ORDER BY
						yc.Sequence, 
						ycu.ClassNumber
				";
		$row_student = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1)
		{
			foreach ($row_student as $key => $thisStudentInfoArr)
			{
				$thisStudentID = $thisStudentInfoArr['UserID'];
				$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
			}
		}
		else
		{
			$ReturnArr = $row_student;
		}
		
		return $ReturnArr;
	}
	
	# return Mark, Grade, OrderMeritClass
	function getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID='', $CheckPositionDisplay=0, $SubOrderArr='')
	{
    	$args = get_defined_vars();
    	$GlobalGetMarksArrVariableStr = '$GlobalGetMarksArr';
    	foreach( (array)$args as  $arg_key => $arg_val)
    	{
    		$key_name = trim($arg_val);	
    		$GlobalGetMarksArrVariableStr.="['".addslashes($key_name)."']";
    	}

    	global $GlobalGetMarksArr;
    	global $eRCTemplateSetting;

    	# check if any identical getMarks function was called before.
    	$GlobalMarksArrExist = false;
    	eval('$GlobalMarksArrExist = isset('.$GlobalGetMarksArrVariableStr.');');
    	
    	if($GlobalMarksArrExist)
    	{
    		$returnAry = '';
    		eval('$returnAry = '.$GlobalGetMarksArrVariableStr.';');
    		return $returnAry;
    	}
    	

    	$x = array();
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		
		$studentIDCond = "";
		if($StudentID !== '')
			$studentIDCond = " AND a.StudentID = '$StudentID' ";
			
		$subjectIDCond = "";
		if($SubjectID !== '')
			$subjectIDCond = " AND a.SubjectID = '$SubjectID' ";
			
		$reportColumnIDCond = "";
		if($ReportColumnID !== '')
			$reportColumnIDCond = " AND a.ReportColumnID = '$ReportColumnID' ";
		
		if ($OrderBy == '') {
			$OrderBy = ' IFNULL(b.DisplayOrder,100) ';
		}
		else if ($OrderBy == 'Mark Desc') {
			$OrderBy = 'a.Mark Desc';
		}
			
		$numOfSubOrder = count((array)$SubOrderArr);
		$SubOrderStr = '';
		if ($SubOrderArr != '' && is_array($SubOrderArr) && $numOfSubOrder > 0) {
			for ($i=0; $i<$numOfSubOrder; $i++) {
				$thisOrderField = $SubOrderArr[$i]['OrderField'];
				$thisOrder = $SubOrderArr[$i]['Order'];
				
				$SubOrderStr .= ", $thisOrderField $thisOrder";
			}
		}
		
		global $sys_custom;
		if($sys_custom['Class']['ClassGroupSettings'])
		{
			$StreamField = ", a.OrderMeritStream, a.StreamNoOfStudent ";
		}
			
		if($eRCTemplateSetting['OrderPositionMethod'])
		{
			$SDField = ", a.SDScore, a.RawSDScore";
		}	
			
		//$sql = "SELECT * FROM $table WHERE ReportID='$ReportID' $studentIDCond $cons ";
		//debug($sql);
		#modified by Marcus 20090924 - JOIN RC_REPORT_TEMPLATE_COLUMN , ORDER BY DisplayOrder
		# IFNULL(b.DisplayOrder,100) -> always put Column with reportColumnID = 0 at the end of the records
		$sql = "
			SELECT 
				a.ReportResultScoreID,
				a.StudentID,
			 	a.SubjectID,
			 	a.ReportID ,
				a.ReportColumnID,
			 	a.Mark,
			 	a.Grade,
			 	a.RawMark,
				a.OrderMeritClass,
			 	a.ClassNoOfStudent,
			 	a.OrderMeritForm,
			 	a.FormNoOfStudent,
			 	a.OrderMeritSubjectGroup,
			 	a.SubjectGroupNoOfStudent
				$StreamField
			 	$SDField
			FROM 
				".$this->DBName.".RC_REPORT_RESULT_SCORE  a
				LEFT JOIN ".$this->DBName.".RC_REPORT_TEMPLATE_COLUMN b ON a. ReportID = b.ReportID AND a.ReportColumnID = b.ReportColumnID
			WHERE 
				a.ReportID='$ReportID' 
				$studentIDCond 
				$subjectIDCond
				$reportColumnIDCond
				$cons
			ORDER BY 
				$OrderBy $SubOrderStr";
		$result = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 		= $ReportSetting['ClassLevelID'];
		
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		if(sizeof($MainSubjectArray) > 0)
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);

		/*
		$returnAry = array();
		if($StudentID)
		{
			foreach($result as $key=>$data)
			{
				$thisSubjectID = $data['SubjectID'];
				$thisReportColumnID = $data['ReportColumnID'];
				
				if($ParentSubjectOnly && !in_array($thisSubjectID, $MainSubjectIDArray))
					continue;

				#Get Manual Adjusted Value - Added By Marcus 20090915
				$ManualAdjustedArr = $this->Get_Manual_Adjustment($ReportID,$StudentID,$thisReportColumnID,$thisSubjectID);
				
				//debug("ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score']".$data['Mark'].",".$data['Grade']."vs".$ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score']);				
				if($includeAdjustedMarks&&!empty($ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score']))
				{
					$x[$thisSubjectID][$thisReportColumnID]['Mark'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score'];
					$x[$thisSubjectID][$thisReportColumnID]['Grade'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score'];
				}
				else
				{
					$x[$thisSubjectID][$thisReportColumnID]['Mark'] = $data['Mark'];
					$x[$thisSubjectID][$thisReportColumnID]['Grade'] = $data['Grade'];
				}
				
				$data['OrderMeritClass'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritClass']? $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritClass'] : $data['OrderMeritClass'];
				$data['OrderMeritForm'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritForm']? $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritForm'] : $data['OrderMeritForm'];
				$data['ClassNoOfStudent'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['ClassNoOfStudent']? $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['ClassNoOfStudent'] : $data['ClassNoOfStudent'];
				$data['FormNoOfStudent'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['FormNoOfStudent']? $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['FormNoOfStudent'] : $data['FormNoOfStudent'];
				
				$x[$thisSubjectID][$thisReportColumnID]['OrderMeritClass'] = $data['OrderMeritClass'];
				$x[$thisSubjectID][$thisReportColumnID]['OrderMeritForm'] = $data['OrderMeritForm'];
				$x[$thisSubjectID][$thisReportColumnID]['ClassNoOfStudent'] = $data['ClassNoOfStudent'];
				$x[$thisSubjectID][$thisReportColumnID]['FormNoOfStudent'] = $data['FormNoOfStudent'];

				# Sort Subject 
				foreach($MainSubjectArray as $parentSubjectID => $CmpSubjectArr)
				{
					foreach ($CmpSubjectArr as $CmpSubjectID => $CmpSubjectName)
					{
						$key = $CmpSubjectID==0?$parentSubjectID:$CmpSubjectID;
						$returnAry[$key] = $x[$key];
					}
				}

			}//debug_pr($returnAry);
		}
		else
		{
			foreach($result as $key=>$data)
			{
				$thisStudentID = $data['StudentID'];
				$thisSubjectID = $data['SubjectID'];
				$thisReportColumnID = $data['ReportColumnID'];
				
				if($ParentSubjectOnly && !in_array($thisSubjectID, $MainSubjectIDArray))
					continue;
				
				foreach ($data as $db_FieldName => $value)
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID][$db_FieldName] = $value;
					
				#Get Manual Adjusted Value - Added By Marcus 20090915
				if($includeAdjustedMarks)
				{
					$ManualAdjustedArr = $this->Get_Manual_Adjustment($ReportID,$StudentID,$thisReportColumnID,$thisSubjectID);
					
					if(!empty($ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['Score']))
					{
						$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score'];
						$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'] = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score'];
					}
				}
				
				foreach($MainSubjectArray as $parentSubjectID => $CmpSubjectArr)
				{
					foreach ($CmpSubjectArr as $CmpSubjectID => $CmpSubjectName)
					{
						$key = $CmpSubjectID==0? $parentSubjectID : $CmpSubjectID;
						$returnAry[$thisStudentID][$key] = $x[$thisStudentID][$key];
					}
				}
					
			}
		}
		//debug_pr($returnAry);
		*/
		
		if ($includeAdjustedMarks)
		{
			// $ManualAdjustedArr[$StudentID][$SubjectID][$ReportColumnID][Mark, Grade...]
			$ManualAdjustedArr = $this->Get_Manual_Adjustment($ReportID, $StudentID, $ReportColumnID, $SubjectID);
		}
			
		if ($CheckPositionDisplay == 1)
		{
			### Position Display Settings
			// $PositionDisplaySettingArr[$SubjectID][Class, Form] = array(values)
			$PositionDisplaySettingArr = $this->Get_Position_Display_Setting($ReportID);
			
			### Subject Average Mark
			// $SubjectAverageArr[$thisSubjectID][$thisReportColumnID] = Average
			$SubjectAverageArr = $this->Get_Report_Average_Mark($ReportID);
		}
		
		if($eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent'] )
		{
			$thisNumOfStudentInSubject = array();
			$thisNumOfClassStudentInSubject = array();
			foreach($result as $key=>$data)
			{
				$thisGrade = $data['Grade'];
				$thisSubjectID = $data['SubjectID'];
				$thisReportColumnID = $data['ReportColumnID'];
				$thisStudentID = $data['StudentID'];
				
				$ClassInfo[$thisStudentID] = $this->Get_Student_Class_ClassLevel_Info($thisStudentID);
				$thisClassID = $ClassInfo[$thisStudentID][0]['ClassID'];
				
				if(!in_array($thisGrade,$eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent']))
				{
					$thisNumOfStudentInSubject[$thisSubjectID][$thisReportColumnID] ++;
					$thisNumOfClassStudentInSubject[$thisClassID][$thisSubjectID][$thisReportColumnID] ++;
				}
			}

		}
		
		$returnAryTemp = array();
		foreach($result as $key=>$data)
		{
			$thisStudentID = $data['StudentID'];
			$thisSubjectID = $data['SubjectID'];
			$thisReportColumnID = $data['ReportColumnID'];
			
			if($ParentSubjectOnly && !in_array($thisSubjectID, $MainSubjectIDArray))
				continue;
			
			foreach ($data as $db_FieldName => $value)
				$x[$thisStudentID][$thisSubjectID][$thisReportColumnID][$db_FieldName] = $value;
				
			# Check Position Display Settings - added by ivan 20091230
			if ($CheckPositionDisplay == 1)
			{
				### Class Position Display Checking
				$thisClassRangeType = $PositionDisplaySettingArr[$thisSubjectID]['Class']['RangeType'];
				$thisClassRangeType = ($thisClassRangeType=='')? 1 : $thisClassRangeType;
				$thisClassUpperLimit = $PositionDisplaySettingArr[$thisSubjectID]['Class']['UpperLimit'];
				$thisClassUpperLimit = ($thisClassUpperLimit=='')? 0 : $thisClassUpperLimit;
				$thisClassGreaterThanPassing = $PositionDisplaySettingArr[$thisSubjectID]['Class']['GreaterThanAverage'];
				$thisClassGreaterThanPassing = ($thisClassGreaterThanPassing=='')? 0 : $thisClassGreaterThanPassing;
				$thisClassShowPosition = $PositionDisplaySettingArr[$thisSubjectID]['Class']['ShowPosition'];
				$thisClassShowPosition = ($thisClassShowPosition=='')? 1 : $thisClassShowPosition;
				
				### Class Info
				$thisClassInfoArr = $this->Get_Student_Class_ClassLevel_Info($thisStudentID);
				$thisYearClassID = $thisClassInfoArr[0]['ClassID'];
				
				$thisClassPosition = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritClass'];
				$thisClassNumOfStudent = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritClass'];
				$thisFormPosition = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritForm'];
				$thisFormNumOfStudent = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['FormNoOfStudent'];
				$thisStreamPosition = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritStream'];
				$thisStreamNumOfStudent = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['StreamNoOfStudent'];
				
				
				### Get Subject Statistics
				// $thisStatisticsArr[$SubjectID][$SchemeGrade][$ClassID][NumOfStudent, Nature] = value
				if (isset($thisStatisticsArr[$thisSubjectID]) == false) {
					$thisStatisticsArr[$thisSubjectID] = $this->Get_Scheme_Grade_Statistics_Info($ReportID, $thisSubjectID, $forSubjectGroup=0);
				}
					
				if (isset($SubjectPassingMarkArr[$thisSubjectID]) == false) {
					$SubjectPassingMarkArr[$thisSubjectID] = $this->Get_Subject_Passing_Mark($thisSubjectID, $ReportID);
				}
				
				
				# Class Average Checking
				$thisRawMark = $x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['RawMark'];
				//$thisSubjectAverageClass = $SubjectAverageArr[$thisSubjectID][$thisReportColumnID][$thisYearClassID];
				//$thisAverageDifferenceClass = $thisRawMark - $thisSubjectAverageClass;
				$thisSubjectPassingMark = $SubjectPassingMarkArr[$thisSubjectID];
				$thisPassingDifferenceClass = $thisRawMark - $thisSubjectPassingMark;
				
				$PassClassAverageMarkChecking = true;
				if (($thisClassGreaterThanPassing != 0 && $thisPassingDifferenceClass < $thisClassGreaterThanPassing) || $thisClassShowPosition != 1)
				{
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritClass'] = '';
					$PassClassAverageMarkChecking = false;
				}
				
				if ($PassClassAverageMarkChecking == true)
				{
					# Position Range Checking
					$numOfClassStudent = $thisClassNumOfStudent;
					if ($thisClassRangeType == 1)
					{
						# Position Range
						$thisCompareValue = $thisClassPosition;
					}
					else
					{
						if ($thisClassRangeType == 3)
						{
							# All PASSED sutdents in class
							$thisSubjectStatisticsArr = $thisStatisticsArr[$thisSubjectID];
							
							$numOfClassStudent = 0;
							foreach ((array)$thisSubjectStatisticsArr as $thisGrade => $thisStatArr)
							{
								$thisNature = $thisStatArr[$thisYearClassID]['Nature'];
								$thisNumOfStudent = $thisStatArr[$thisYearClassID]['NumOfStudent'];
								
								if ($thisNature == 'Distinction' || $thisNature == 'Pass')
									$numOfClassStudent += $thisNumOfStudent;
							}
						}
						
						if ($numOfClassStudent > 0)
							$thisClassPositionPercentage = ($thisClassPosition / $numOfClassStudent) * 100;
							
						$thisCompareValue = $thisClassPositionPercentage;
					}
					
					if ($thisClassUpperLimit != 0 && $thisCompareValue > $thisClassUpperLimit && $numOfClassStudent > 0)
						$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritClass'] = '';
				}

				
				
				
				### Form Position Setting Checking
				//$thisSubjectAverageForm = $SubjectAverageArr[$thisSubjectID][$thisReportColumnID][0];
				//$thisAverageDifferenceForm = $thisRawMark - $thisSubjectAverageForm;
				$thisPassingDifferenceForm = $thisRawMark - $thisSubjectPassingMark;
				
				$thisYearClassID = 0;
				$thisFormRangeType = $PositionDisplaySettingArr[$thisSubjectID]['Form']['RangeType'];
				$thisFormRangeType = ($thisFormRangeType=='')? 1 : $thisFormRangeType;
				$thisFormUpperLimit = $PositionDisplaySettingArr[$thisSubjectID]['Form']['UpperLimit'];
				$thisFormUpperLimit = ($thisFormUpperLimit=='')? 0 : $thisFormUpperLimit;
				$thisFormGreaterThanPassing = $PositionDisplaySettingArr[$thisSubjectID]['Form']['GreaterThanAverage'];
				$thisFormGreaterThanPassing = ($thisFormGreaterThanPassing=='')? 0 : $thisFormGreaterThanPassing;
				$thisFormShowPosition = $PositionDisplaySettingArr[$thisSubjectID]['Form']['ShowPosition'];
				$thisFormShowPosition = ($thisFormShowPosition=='')? 1 : $thisFormShowPosition;
				
				
				# Form Average Checking
				$PassFormAverageMarkChecking = true;
				$PassStreamAverageMarkChecking = true;
				if (($thisFormGreaterThanPassing != 0 && $thisPassingDifferenceForm < $thisFormGreaterThanPassing) || $thisFormShowPosition != 1)
				{
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritForm'] = '';
					$PassFormAverageMarkChecking = false;
					
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritStream'] = '';
					$PassStreamAverageMarkChecking = false;
				}
				
				if ($PassFormAverageMarkChecking == true)
				{
					$numOfFormStudent = $thisFormNumOfStudent;
					if ($thisFormRangeType == 1)
					{
						# Position Range
						$thisCompareValue = $thisFormPosition;
					}
					else
					{
						if ($thisFormRangeType == 3)
						{
							# All PASSED sutdents in form
							$thisSubjectStatisticsArr = $thisStatisticsArr[$thisSubjectID];
							
							$numOfFormStudent = 0;
							foreach ((array)$thisSubjectStatisticsArr as $thisGrade => $thisStatArr)
							{
								$thisNature = $thisStatArr[$thisYearClassID]['Nature'];
								$thisNumOfStudent = $thisStatArr[$thisYearClassID]['NumOfStudent'];
								
								if ($thisNature == 'Distinction' || $thisNature == 'Pass')
									$numOfFormStudent += $thisNumOfStudent;
							}
						}
						
						if ($numOfFormStudent > 0)
							$thisFormPositionPercentage = ($thisFormPosition / $numOfFormStudent) * 100;
							
						$thisCompareValue = $thisFormPositionPercentage;
					}
					
					if ($thisFormUpperLimit != 0 && $thisCompareValue > $thisFormUpperLimit && $numOfFormStudent > 0)
						$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritForm'] = '';
				}
				
				# Stream Position Checking
				if ($PassStreamAverageMarkChecking == true)
				{
					$numOfStreamStudent = $thisStreamNumOfStudent;
					if ($thisFormRangeType == 1)
					{
						# Position Range
						$thisCompareValue = $thisStreamPosition;
					}
					else
					{
						if ($thisFormRangeType == 3)
						{
							# All PASSED sutdents in stream
							$thisSubjectStatisticsArr = $thisStatisticsArr[$thisSubjectID];
							
							$numOfStreamStudent = 0;
							foreach ((array)$thisSubjectStatisticsArr as $thisGrade => $thisStatArr)
							{
								$thisNature = $thisStatArr[$thisYearClassID]['Nature'];
								$thisNumOfStudent = $thisStatArr[$thisYearClassID]['NumOfStudent'];
								
								if ($thisNature == 'Distinction' || $thisNature == 'Pass')
									$numOfStreamStudent += $thisNumOfStudent;
							}
						}
						
						if ($numOfStreamStudent > 0)
							$thisStreamPositionPercentage = ($thisStreamPosition / $numOfStreamStudent) * 100;
							
						$thisCompareValue = $thisStreamPositionPercentage;
					}
					
					if ($thisFormUpperLimit != 0 && $thisCompareValue > $thisFormUpperLimit && $numOfStreamStudent > 0)
						$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritStream'] = '';
				}
			}	// End if ($CheckPositionDisplay == 1)
			
			# Get Manual Adjusted Value - Added By Marcus 20090915
			if($includeAdjustedMarks)
			{
				# Adjust Mark
				if(!empty($ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['Score']))
				{
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Mark'] = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['Score'];
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['Grade'] = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['Score'];
				}
				
				# Adjust Position / Num of Student
				$thisAdjustedOrderMeritClass = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritClass'];
				$thisAdjustedClassNoOfStudent = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['ClassNoOfStudent'];
				$thisAdjustedOrderMeritForms = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritForm'];
				$thisAdjustedFormNoOfStudent = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['FormNoOfStudent'];
				$thisAdjustedOrderMeritSubjectGroup = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritSubjectGroup'];
				$thisAdjustedSubjectGroupNoOfStudent = $ManualAdjustedArr[$thisStudentID][$thisReportColumnID][$thisSubjectID]['SubjectGroupNoOfStudent'];
				
				if ($thisAdjustedOrderMeritClass != '')
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritClass'] = $thisAdjustedOrderMeritClass;
				if ($thisAdjustedClassNoOfStudent != '')
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['ClassNoOfStudent'] = $thisAdjustedClassNoOfStudent;
				
				if ($thisAdjustedOrderMeritForms != '')
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritForm'] = $thisAdjustedOrderMeritForms;
				if ($thisAdjustedFormNoOfStudent != '')
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['FormNoOfStudent'] = $thisAdjustedFormNoOfStudent;
					
				if ($thisAdjustedOrderMeritSubjectGroup != '')
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['OrderMeritSubjectGroup'] = $thisAdjustedOrderMeritSubjectGroup;
				if ($thisAdjustedSubjectGroupNoOfStudent != '')
					$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['SubjectGroupNoOfStudent'] = $thisAdjustedSubjectGroupNoOfStudent;
			}
			
			if(isset($eRCTemplateSetting['SpecialCaseThatExcludedFromNoOfStudent']) )
			{
				$thisClassID = $ClassInfo[$thisStudentID][0]['ClassID'];
				$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['ClassNoOfStudent']=$thisNumOfClassStudentInSubject[$thisClassID][$thisSubjectID][$thisReportColumnID];
				$x[$thisStudentID][$thisSubjectID][$thisReportColumnID]['FormNoOfStudent']=$thisNumOfStudentInSubject[$thisSubjectID][$thisReportColumnID];
			}
			
			### Order the Subject Score
			foreach($MainSubjectArray as $parentSubjectID => $CmpSubjectArr)
			{
				foreach ($CmpSubjectArr as $CmpSubjectID => $CmpSubjectName)
				{
					
					$key = $CmpSubjectID==0? $parentSubjectID : $CmpSubjectID;
					$returnAryTemp[$thisStudentID][$key] = $x[$thisStudentID][$key];
				}
			}
			
		}
		
		if ($StudentID)
			$returnAry = $returnAryTemp[$StudentID];
		else
			$returnAry = $returnAryTemp;
			
		
		unset($x);
		unset($returnAryTemp);
		unset($ManualAdjustedArr);
		unset($PositionDisplaySettingArr);
		unset($SubjectAverageArr);
		
		eval($GlobalGetMarksArrVariableStr.' = $returnAry;');
		
		return $returnAry;
	}
	
	function getMarksFromMarksheet($ReportID, $SubjectID) 
	{
		$FuncArgArr = get_defined_vars();
		$PreloadArrKey = 'GetMarksFromMarksheetArr';
		$returnAry = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnAry !== false) {
    		return $returnAry;
    	}
    	
    	
    	
    	# Get Report Info
    	$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID =  $ReportInfoArr['ClassLevelID'];
    	
    	# Get Student Info
    	$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
    	$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
    	$numOfStudent = count($StudentIDArr);
    	unset($StudentInfoArr);
    	
    	# Component Subject Name and Grade Image
		$ComponentSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
		$numOfComponent = count($ComponentSubjectArr);
    	
    	# Build the getMarks array from MS
		$CmpSubjectIDArr = Get_Array_By_Key($ComponentSubjectArr, 'SubjectID');
		$ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ReportColumnInfoArr);
		
		$SubjectColumnWeightArr = array();
		$ReportWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID);
		$ReportWeightInfo = BuildMultiKeyAssoc($ReportWeightInfo, array('SubjectID', 'ReportColumnID'));
		foreach ((array)$ReportWeightInfo as $thisSubjectID => $thisSubjectWeightInfoArr) {
			foreach ((array)$thisSubjectWeightInfoArr as $thisReportColumnID => $thisReportColumnWeightInfoArr) {
				if ($thisReportColumnID == '') {
					$ReportWeightInfo[$thisSubjectID][0] = $thisReportColumnWeightInfoArr;
				}
			}
		}
		
		# Get Marks from Marksheet
		$MarksheetInfoArr = array();
		for ($i=0; $i<$numOfComponent; $i++) {
			$thisCmpSubjectID = $CmpSubjectIDArr[$i];
			
			for ($j=0; $j<$numOfColumn; $j++) {
				$thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];
				$MarksheetInfoArr[$thisCmpSubjectID][$thisReportColumnID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $thisCmpSubjectID, $thisReportColumnID);
			}
			
			// Get Overall Marksheet
			$thisReportColumnID = 0;
			$MarksheetInfoArr[$thisCmpSubjectID][$thisReportColumnID] = $this->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $thisCmpSubjectID, $ReportID);
		}
		
		
		# Get Scheme Grade Mapping
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);
		$schemeRangeIDGradeMap = array();
		for ($i=0; $i<$numOfComponent; $i++) {
			$thisCmpSubjectID = $CmpSubjectIDArr[$i];
			$thisSchemeID = $subjectList[$thisCmpSubjectID]['schemeID'];
			
			$schemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($thisSchemeID);
			for($m=0; $m<sizeof($schemeRangeInfo); $m++) {
				$schemeRangeIDGradeMap[$thisSchemeID][$schemeRangeInfo[$m]["GradingSchemeRangeID"]] = $schemeRangeInfo[$m]["Grade"];
			}			
		}
		
		
		# Analyse the array to build an array same as getMarks function
    	for ($i=0; $i<$numOfStudent; $i++) {
			$thisStudentID = $StudentIDArr[$i];
	    	
	    	for ($j=0; $j<$numOfComponent; $j++) {
				$thisCmpSubjectID = $CmpSubjectIDArr[$j];
				
				$thisSchemeID = $subjectList[$thisCmpSubjectID]['schemeID'];
				$thisCmpOverallMarkType = $MarksheetInfoArr[$thisCmpSubjectID][0][$thisStudentID]['MarkType'];
				$thisCmpOverallMarkRaw = $MarksheetInfoArr[$thisCmpSubjectID][0][$thisStudentID]['MarkRaw'];
				$thisCmpOverallMarkNonNum = $MarksheetInfoArr[$thisCmpSubjectID][0][$thisStudentID]['MarkNonNum'];
				
				if ($thisCmpOverallMarkType == 'SC') {
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Mark'] = '';
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['RawMark'] = '';
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Grade'] = $MarksheetInfoArr[$thisCmpSubjectID][0][$thisStudentID]['MarkNonNum'];
				}
				else if ($thisCmpOverallMarkType == 'G') {
					$thisGradingSchemeRangeID = $thisCmpOverallMarkNonNum;
					$thisGrade = $schemeRangeIDGradeMap[$thisSchemeID][$thisGradingSchemeRangeID];
					
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Mark'] = '';
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['RawMark'] = '';
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Grade'] = $thisGrade;
				}
				else if ($thisCmpOverallMarkType == 'M') {
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Mark'] = $this->ROUND_MARK($thisCmpOverallMarkRaw, 'SubjectTotal');
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['RawMark'] = $thisCmpOverallMarkRaw;
					$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Grade'] = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisCmpOverallMarkRaw, $ReportID, $thisStudentID, $thisCmpSubjectID, $ClassLevelID, 0);
				}
				else if ($thisCmpOverallMarkType == '') {	// Without Overall Mark direct input
					// Calculate the Component Overall Mark from Report Columns
					$thisCmpSubjectMark = -1;
					$thisCmpSubjectTotalWeight = 0;
					$thisCmpSubjectExcludeWeight = 0;
					for ($k=0; $k<$numOfColumn; $k++) {
						$thisReportColumnID = $ReportColumnInfoArr[$k]['ReportColumnID'];
						
						$thisMarksheetArr = $MarksheetInfoArr[$thisCmpSubjectID][$thisReportColumnID];
						$thisMarkType = $thisMarksheetArr[$thisStudentID]['MarkType'];
						$thisMarkRaw = $thisMarksheetArr[$thisStudentID]['MarkRaw'];
						$thisMarkNonNum = $thisMarksheetArr[$thisStudentID]['MarkNonNum'];
						$thisSubjectColumnWeight = $ReportWeightInfo[$thisCmpSubjectID][$thisReportColumnID]['Weight'];
						$thisCmpSubjectTotalWeight += $thisSubjectColumnWeight;
						
						if ($thisMarkType == '') {		// $thisMarkType == '' means no mark
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['Mark'] = '';
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['RawMark'] = '';
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['Grade'] = 'N.A.';
						}
						else if ($thisMarkType == 'SC') {
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['Mark'] = '';
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['RawMark'] = '';
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['Grade'] = $thisMarkNonNum;
							
							$thisCmpSubjectExcludeWeight += $thisSubjectColumnWeight;
						}
						else {
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['Mark'] = $this->ROUND_MARK($thisMarkRaw, 'SubjectScore');
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['RawMark'] = $thisMarkRaw;
							$returnAry[$thisStudentID][$thisCmpSubjectID][$thisReportColumnID]['Grade'] = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMarkRaw, $ReportID, $thisStudentID, $thisCmpSubjectID, $ClassLevelID, 0);
							
							if ($thisCmpSubjectMark == -1) {
								$thisCmpSubjectMark = 0;
							}
							$thisCmpSubjectMark += $thisMarkRaw * $thisSubjectColumnWeight;
						}
					}
					
					
					// No ReportColumn Mark OR All weights are excluded => N.A. for the Overall Score
					if ($thisCmpSubjectMark == -1 || $thisCmpSubjectTotalWeight == $thisCmpSubjectExcludeWeight) {
						$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Mark'] = '';
						$returnAry[$thisStudentID][$thisCmpSubjectID][0]['RawMark'] = '';
						$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Grade'] = 'N.A.';
					}
					else {
						$thisCmpRemainingWeight = $thisCmpSubjectTotalWeight - $thisCmpSubjectExcludeWeight;
						
						if ($thisCmpRemainingWeight > 0) {
							$thisCmpSubjectMark = $thisCmpSubjectMark * ($thisCmpSubjectTotalWeight / $thisCmpRemainingWeight);
						}
						
						$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Mark'] = $this->ROUND_MARK($thisCmpSubjectMark, 'SubjectTotal');
						$returnAry[$thisStudentID][$thisCmpSubjectID][0]['RawMark'] = $thisCmpSubjectMark;
						$returnAry[$thisStudentID][$thisCmpSubjectID][0]['Grade'] = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisCmpSubjectMark, $ReportID, $thisStudentID, $thisCmpSubjectID, $ClassLevelID, 0);
					}
				}				
    		}
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnAry);
		return $returnAry;
	}
	
	function Get_Report_Average_Mark($ReportID, $SubjectID='', $ReportColumnID='')
	{
		$PreloadArrKey = 'ReportAverageMark';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		### Get Mark
		$MarkArr = $this->getMarks($ReportID, $StudentID='', $cons='', 0, $includeAdjustedMarks=1, '', $SubjectID, $ReportColumnID);
		
		# Get Student Info for Average Calculation
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, "", "", $ReturnAsso=1);
		
		### Consolidate Subject Mark
		$SubjectMarkArr = array();
		foreach ((array)$MarkArr as $thisStudentID => $StudentMarkArr)
		{
			$thisYearClassID = $StudentInfoArr[$thisStudentID]['YearClassID'];
			foreach ((array)$StudentMarkArr as $thisSubjectID => $StudentSubjectMarkArr)
			{
				foreach ((array)$StudentSubjectMarkArr as $thisReportColumnID => $StudentSubjectReportColumnMarkArr)
				{
					$thisMark = $StudentSubjectReportColumnMarkArr['RawMark'];
					$thisGrade = $StudentSubjectReportColumnMarkArr['Grade'];
					if (is_numeric($thisMark) && in_array($thisGrade, $this->specialCasesSet1)==false && in_array($thisGrade, $this->specialCasesSet2)==false)
					{
						(array)$SubjectMarkArr[$thisSubjectID][$thisReportColumnID][$thisYearClassID][] = $thisMark;
						(array)$SubjectMarkArr[$thisSubjectID][$thisReportColumnID][0][] = $thisMark;
					}
				}
			}
		}
		
		# Get Subject Average Mark
		$SubjectAverageArr = array();
		$ReportColumnIDArr = array();
		foreach ((array)$SubjectMarkArr as $thisSubjectID => $thisSubjectMarkArr)
		{
			foreach ((array)$thisSubjectMarkArr as $thisReportColumnID => $SubjectReportColumnMarkArr)
			{
				$ReportColumnIDArr[] = $thisReportColumnID;
				
				foreach ((array)$SubjectReportColumnMarkArr as $thisYearClassID => $SubjectReportColumnClassMarkArr)
				{
					if (count($SubjectReportColumnClassMarkArr) > 0)
						$SubjectAverageArr[$thisSubjectID][$thisReportColumnID][$thisYearClassID] = $this->getAverage($SubjectReportColumnClassMarkArr, 2);
					else
						$SubjectAverageArr[$thisSubjectID][$thisReportColumnID][$thisYearClassID] = 0;
				}
			}
		}
		
		### GrandTotal, GrandAverage, GPA Average
		$numOfReportColumnID = count($ReportColumnIDArr);
		for ($i=0; $i<$numOfReportColumnID; $i++)
		{
			$thisReportColumnID = $ReportColumnIDArr[$i];
			$GrandMarkArr = $this->getReportResultScore($ReportID, $thisReportColumnID);
			
			$GrandMarkTempArr = array();
			# Consolidate ReportColumn Grand Mark
			foreach ((array)$GrandMarkArr as $thisStudentID => $StudentGrandMarkArr)
			{
				$thisGrandTotal = $StudentGrandMarkArr[$thisReportColumnID]['GrandTotal'];
				$thisGrandAverage = $StudentGrandMarkArr[$thisReportColumnID]['GrandAverage'];
				$thisGPA = $StudentGrandMarkArr[$thisReportColumnID]['GPA'];
				
				$thisYearClassID = $StudentInfoArr[$thisStudentID]['YearClassID'];
				
				### Class Average
				if (is_numeric($thisGrandTotal))
					(array)$GrandMarkTempArr['GrandTotal'][$thisYearClassID][] = $thisGrandTotal;
				if (is_numeric($thisGrandAverage))
					(array)$GrandMarkTempArr['GrandAverage'][$thisYearClassID][] = $thisGrandAverage;
				if (is_numeric($thisGPA))
					(array)$GrandMarkTempArr['GPA'][$thisYearClassID][] = $thisGPA;
					
				### Form Average
				if (is_numeric($thisGrandTotal))
					(array)$GrandMarkTempArr['GrandTotal'][0][] = $thisGrandTotal;
				if (is_numeric($thisGrandAverage))
					(array)$GrandMarkTempArr['GrandAverage'][0][] = $thisGrandAverage;
				if (is_numeric($thisGPA))
					(array)$GrandMarkTempArr['GPA'][0][] = $thisGPA;
			}
			
			# Get ReportColumn Average Grand Mark
			foreach ($GrandMarkTempArr as $GrandMarkType => $thisGrandMarkArr)
			{
				foreach ($thisGrandMarkArr as $thisYearClassID => $thisClassGrandMarkArr)
				{
					if (count($thisClassGrandMarkArr) > 0)
						$SubjectAverageArr[$GrandMarkType][$thisReportColumnID][$thisYearClassID] = $this->getAverage($thisClassGrandMarkArr, 2);
					else
						$SubjectAverageArr[$GrandMarkType][$thisReportColumnID][$thisYearClassID] = 0;
				}
			}
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SubjectAverageArr);
		return $SubjectAverageArr;
	}
	
	function checkSpCase($ReportID, $SubjectID, $thisMark, $tempGrade='', $checkManualAdjust=1)
	{
		//For manual adjustment, Mark adjusted will have same mark and grade
//		if($checkManualAdjust && ($thisMark == $tempGrade) && !is_numeric($thisMark) && !in_array($thisMark,$this->specialCasesSet1)&& !in_array($thisMark,$this->specialCasesSet2))
//		{
//			return array($thisMark, 0);
//		}
		
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		# Modified by Marcus 20101011 (for template based grading scheme) 
 		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
 		$SchemeID = $SubjectFormGradingSettings[SchemeID];
 		$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];

		$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
		$SchemeType = $SchemeInfo['SchemeType'];
		
		if ($ScaleDisplay=="M" && $tempGrade == '')
			return array($thisMark, 1);
			
		switch($SchemeType)
		{
			case "H":
				$SpecialCaseArrH = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
				# check is the tempGrade in the SpecialCaseArrH
				if(in_array($tempGrade, $SpecialCaseArrH)===true)
				{
					$needStyle = ($tempGrade == '+')? 1 : 0;
					return array($this->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($tempGrade), $needStyle);
				}
				else
					return array($thisMark, 1);
				break;
			case "PF":
				$SpecialCaseArrPF = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
				# check is the tempGrade in the SpecialCaseArrH
				if(in_array($tempGrade, $SpecialCaseArrPF))
				{
					$needStyle = ($tempGrade == '+')? 1 : 0;
					return array($this->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($tempGrade), $needStyle);
				}
				else
					return array($thisMark, 1);
				break;
		}
	}
	
	# modified by Marcus 20100211, add $ConvertBy to force returnMarkNature to treat marks as grade
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed) 
	# can help to returnMarkNature of marks in scheme using percentage to classify nature.
	function returnMarkNature($ClassLevelID, $SubjectID, $thisMark='', $ReportColumnID='', $ConvertBy='',$ReportID='')
	{
		global $eRCTemplateSetting;
		
		$CalSetting = $this->LOAD_SETTING("Calculation");
		$UseWeightedMark = $CalSetting['UseWeightedMark'];
		
		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1, 0, $ReportID);
		$SchemeID = $SubjectFormGradingSettings['SchemeID'];
		$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
		
		$NatrueType = array("P"=>"Pass", "F"=>"Fail", "D"=>"Distinction");
		
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$tableRC_GRADING_SCHEME = $this->DBName.".RC_GRADING_SCHEME";
		
		if($ConvertBy!='')
			$ScaleDisplay=$ConvertBy;
			 
		switch($ScaleDisplay)
		{
			case "G":
//				$sql = "select Nature from $table where SchemeID=$SchemeID and Grade='$thisMark'";
//				$result = $this->returnVector($sql);
//				return $NatrueType[$result[0]];

				$this->Load_Batch_Grading_Scheme_Range_Info();
				if(empty($this->TmpBatchArr["returnMarkNature"]["GradeNature"][$SchemeID]))
				{
					$tmpResult = $this->Grading_Scheme_Range_Info[$SchemeID];
					$this->TmpBatchArr["returnMarkNature"]["GradeNature"][$SchemeID] = BuildMultiKeyAssoc($tmpResult, "Grade","Nature",1);
				}
				$nature = $this->TmpBatchArr["returnMarkNature"]["GradeNature"][$SchemeID][$thisMark];	

				return $NatrueType[$nature];
				break;
				
			case "M":
				# check fail
				# assume value is Passing Mark is exists
//				$sql = "select PassMark from $tableRC_GRADING_SCHEME where SchemeID=$SchemeID";
//				$result = $this->returnArray($sql);
				$this->Load_Batch_Grading_Scheme_Main_Info();
				$result = $this->Grading_Scheme_Main_Info[$SchemeID];

				if($result)
				{
					if ($ReportColumnID != '' && ($UseWeightedMark == 1 || $eRCTemplateSetting['InputWeightedMarkDirectly']))
					{
						$OtherCondition = " SubjectID = '$SubjectID' AND ReportColumnID = '$ReportColumnID' ";
						$SubjectWeightInfo = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						$SubjectWeight = $SubjectWeightInfo[0]['Weight'];
						
						$thisPassMark = $result['PassMark'] * $SubjectWeight;
					}
					else
					{
						$SubjectWeight = 1;
						$thisPassMark = $result['PassMark'];
					}
					
					
					
//					if ($ReportColumnID != '' && $UseWeightedMark == 1)
//					{
//						$thisReportID = $this->returnReportIDByReportColumnID($ReportColumnID);
//						$thisColumnData = $this->returnReportTemplateColumnData($thisReportID);
//						$thisColumnWeightMap = array();
//						for($j=0; $j<sizeof($thisColumnData); $j++) {
//							$thisColumnWeightMap[$thisColumnData[$j]["ReportColumnID"]] = $thisColumnData[$j]["DefaultWeight"];
//						}
//						
//						$thisPassMark = $result['PassMark'] * $thisColumnWeightMap[$ReportColumnID];
//					}
//					else
//					{
//						$thisPassMark = $result['PassMark'];
//					}

					// Exclusive, e.g. pass mark = 40 & mark = 40, it consider as passed.
					if($thisMark < $thisPassMark)
						return $NatrueType['F'];
				}
				 
                # check non
//                $sql = "select Nature, LowerLimit from $table where SchemeID=$SchemeID and LowerLimit!='' and Nature!='F' order by LowerLimit desc";
//                $result = $this->returnArray($sql);

				$this->Load_Batch_Grading_Scheme_Range_Info();
				if(empty($this->TmpBatchArr["returnMarkNature"]["MarkNature"][$SchemeID]))
				{
					$tmpResult = $this->Grading_Scheme_Range_Info[$SchemeID];
					$this->TmpBatchArr["returnMarkNature"]["MarkNature"][$SchemeID] = $tmpResult; //BuildMultiKeyAssoc($tmpResult, "Grade","Nature",1);
				}
				$result = $this->TmpBatchArr["returnMarkNature"]["MarkNature"][$SchemeID];

                if(!empty($result))
                {
                        foreach($result as $key=>$data)
                        {
                                $Nature = $data["Nature"];
                        		$LowerLimit = $data["LowerLimit"] * $SubjectWeight;
                        		
                                if(empty($LowerLimit))          continue;
                                if($thisMark >= $LowerLimit)    return $NatrueType[$Nature];
                        }
                        return "";
                }
                else
                	return "Pass";
                break;
                
            # updated on 24 Dec 2008 by Ivan - return nature of P/F Scheme also
            default:
//            	$sql = "select Pass, Fail from $tableRC_GRADING_SCHEME where SchemeID=$SchemeID";
//				$result = $this->returnArray($sql, 2);
				$this->Load_Batch_Grading_Scheme_Main_Info();
				$result = $this->Grading_Scheme_Main_Info[$SchemeID];
				
				if ($thisMark == $result['Pass'])
					return "Pass";
				else if ($thisMark == $result['Fail'])
					return "Fail";
				else
					return "";
				break;
		}
		
		
	}
	
	function returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID, $ColID)
	{
		$table = $this->DBName.".RC_REPORT_RESULT_FULLMARK";
		$sql = "select FullMark from $table where ReportID=$ReportID and SubjectID=$SubjectID and StudentID=$StudentID and ReportColumnID=$ColID";
		
		return $this->returnVector($sql);
		
	}
	
	function retrieveLastAdjustDateTime($ReportID, $StudentID='')
	{
		$x = array();
		
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
		$sql = "select StudentID, DateInput, DateModified from $table where ReportID=$ReportID";
		if($StudentID)	$sql .= " and StudentID=$StudentID";
		$result = $this->returnArray($sql);
		
		foreach($result as $key=>$data)
			$x[$data['StudentID']] = ($data['DateInput'] != $data['DateModified']) ? $data['DateModified'] : $x[$data['StudentID']];
		return $x;
	}
	
	function UPDATE_REPORT_RESULT_SCORE($ReportID, $StudentID, $ColID=0, $SubjectID, $Mark='', $Grade='', $ReportResultScoreID='')
	{
		$table = $this->DBName.".RC_REPORT_RESULT_SCORE";
	
		if ($ReportResultScoreID=='')
			$conds = "ReportID=$ReportID and StudentID=$StudentID and SubjectID=$SubjectID and ReportColumnID=$ColID";
		else
			$conds = "ReportResultScoreID = '".$ReportResultScoreID."'";
	
		$sql = "UPDATE $table set Mark='$Mark', Grade='$Grade', DateModified=NOW() WHERE $conds";
		$result = $this->db_db_query($sql);
		return $result;

	}
	
	// for SIS
	function IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $SubjectIDArr="", $ReportColumnID=""){
		$returnVal = '';
		$isAllColumnWeightZero = 1;
		
		# Load Settings - Calculation
		$categorySettings = $this->LOAD_SETTING('Calculation');
		
		# Get Report Info
		$ReportInfo = $this->returnReportTemplateColumnData($ReportID);
		
		$ReportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportType = $ReportBasicInfo['Semester'];
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $categorySettings['OrderFullYear'] : $categorySettings['OrderTerm'];
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if($ReportColumnID != ""){
				$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$ReportColumnID;
				$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						
				if($weight_data[0]['Weight'] > 0) 
					$isAllColumnWeightZero = 0;
			} else {
				if(count($ReportInfo) > 0){
			    	for($i=0 ; $i<sizeof($ReportInfo) ; $i++){
						$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$ReportInfo[$i]['ReportColumnID'];
						$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						
						if($weight_data[0]['Weight'] > 0){
							$isAllColumnWeightZero = 0;
							continue;
						}
					}
				}
			}
		}
		else {
			$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, '', $SubjectIDArr, $ReportColumnID);
			$num_of_weight_data = count($weight_data);
			
			for($i=0 ; $i<$num_of_weight_data ; $i++){
				
				if($weight_data[$i]['Weight'] > 0){
					if($ReportColumnID != ""){
						if($weight_data[$i]['ReportColumnID'] == $ReportColumnID){
							$isAllColumnWeightZero = 0;
							continue;
						}
					} else {
						$isAllColumnWeightZero = 0;
						continue;
					}
				}	
			}
		}
		
		$returnVal = $isAllColumnWeightZero;
		return $returnVal;
	}
	
	# Copy ClassLevel Settings [SchemeID, ScaleInput, ScaleDisplay]
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function COPY_CLASSLEVEL_SETTINGS($frClassLevelID='', $toClassLevelID='', $frReportID='', $toReportID=''){
		$resultCopy = array();
		
		if(trim($frClassLevelID)=='')
		{
			$frReportData = $this->returnReportTemplateBasicInfo($frReportID);
			$frClassLevelID = $frReportData["ClassLevelID"];
		}
		if(trim($toClassLevelID)=='')
		{
			$toReportData = $this->returnReportTemplateBasicInfo($toReportID);
			$toClassLevelID = $toReportData["ClassLevelID"];
		}
		
		$frReportID = trim($frReportID)==''?0:$frReportID;
		if(trim($toReportID)=='' || trim($toReportID)==0)
		{
			$ReportList = $this->Get_Report_List($toClassLevelID);
			$toReportIDArr = Get_Array_By_Key($ReportList, "ReportID");
			$toReportIDArr[] = 0;
		}	
		else
		{
			$toReportIDArr = array($toReportID);
		}

		# Get Subject Form Grading Data of Source
		$frSubjectDataArr = array();
		$frSubjectDataArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($frClassLevelID, 0, $frReportID);

		foreach($toReportIDArr as $toReportID)
		{		
			# Get Subject Form Grading Data of Target ClassLevel
			$toSubjectDataArr = array();
			$toSubjectDataArr = $this->GET_FROM_SUBJECT_GRADING_SCHEME($toClassLevelID, 0, $toReportID);
		
			if($frReportID == $toReportID && $frReportID != 0)
				continue;
			
			
//				$toSubjectDataArr = $frSubjectDataArr;
			
			if(count($frSubjectDataArr) > 0 ){
				$frSubjectIDArr = array_keys($frSubjectDataArr);
				
				if(count($toSubjectDataArr)>0)
					$toSubjectIDArr = array_keys($toSubjectDataArr);
				else if($frClassLevelID == $toClassLevelID)
					$toSubjectIDArr = array_keys($frSubjectDataArr);
				
				### Copy Grand Mark Scheme too
				$GrandSchemeIndexArr = $this->Get_Grand_Mark_Grading_Scheme_Index_Arr();
				foreach ($GrandSchemeIndexArr as $GrandMarkIndex => $GrandMarkType)
				{
					$frSubjectIDArr[] = $GrandMarkIndex;
					$toSubjectIDArr[] = $GrandMarkIndex;
				}
				
				# Get SubjectID which is matched among Source & Target ClassLevel
				$MatchedSubjectIDArr = array();
				for($i=0 ; $i<count($toSubjectIDArr) ; $i++){
					if(in_array($toSubjectIDArr[$i], $frSubjectIDArr))
						$MatchedSubjectIDArr[] = $toSubjectIDArr[$i];
				}
//				debug_pr($MatchedSubjectIDArr);
				if(count($MatchedSubjectIDArr) > 0){
					$UpdateSubjectDataArr = array();
					
					# Get Necessary Data for copying,[SchemeID, ScaleInput, ScaleDisplay]
					for($i=0 ; $i<count($MatchedSubjectIDArr) ; $i++){
						$SubjectID = $MatchedSubjectIDArr[$i];
						
						# Copy operates only when 
						# The Target ClassLevel Subject does not have assigned any Scheme AND 
						# The Source ClassLevel Subject contains a Grading Scheme
						
//						if( $frSubjectDataArr[$SubjectID]['schemeID'] != "" && $frSubjectDataArr[$SubjectID]['schemeID'] != null && 
//							($toSubjectDataArr[$SubjectID]['schemeID'] == "" || $toSubjectDataArr[$SubjectID]['schemeID'] == null) ){
//								$UpdateSubjectDataArr[$SubjectID]['schemeID'] = $frSubjectDataArr[$SubjectID]['schemeID'];
						if( $frSubjectDataArr[$SubjectID]['schemeID'] != "" && $frSubjectDataArr[$SubjectID]['schemeID'] != null  || empty($frSubjectDataArr[$SubjectID]['schemeID']) ){
								
								$UpdateSubjectDataArr[$SubjectID]['schemeID'] = $frSubjectDataArr[$SubjectID]['schemeID'];
								$UpdateSubjectDataArr[$SubjectID]['displayOrder'] = $frSubjectDataArr[$SubjectID]['displayOrder'];
							
							# Get Grading Scheme Info
							$SchemeMainInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($frSubjectDataArr[$SubjectID]['schemeID']);
							$SchemeType = $SchemeMainInfo['SchemeType'];
							
							if($SchemeType == "H"){
								/*if( $frSubjectDataArr[$SubjectID]['scaleInput'] != "" && $frSubjectDataArr[$SubjectID]['scaleInput'] != null &&
									($toSubjectDataArr[$SubjectID]['scaleInput'] == "" || $toSubjectDataArr[$SubjectID]['scaleInput'] == null) )
									$UpdateSubjectDataArr[$SubjectID]['scaleInput'] = $frSubjectDataArr[$SubjectID]['scaleInput'];
									
								if( $frSubjectDataArr[$SubjectID]['scaleDisplay'] != "" && $frSubjectDataArr[$SubjectID]['scaleDisplay'] != null &&
									($toSubjectDataArr[$SubjectID]['scaleDisplay'] == "" || $toSubjectDataArr[$SubjectID]['scaleDisplay'] == null) )
									$UpdateSubjectDataArr[$SubjectID]['scaleDisplay'] = $frSubjectDataArr[$SubjectID]['scaleDisplay'];
	
								if( $frSubjectDataArr[$SubjectID]['LangDisplay'] != "" && $frSubjectDataArr[$SubjectID]['LangDisplay'] != null &&
									($toSubjectDataArr[$SubjectID]['LangDisplay'] == "" || $toSubjectDataArr[$SubjectID]['LangDisplay'] == null) )
									$UpdateSubjectDataArr[$SubjectID]['LangDisplay'] = $frSubjectDataArr[$SubjectID]['LangDisplay'];
									
								if( $frSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] != "" && $frSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] != null &&
									($toSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] == "" || $toSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] == null) )
									$UpdateSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] = $frSubjectDataArr[$SubjectID]['DisplaySubjectGroup']; 
*/
								if( $frSubjectDataArr[$SubjectID]['scaleInput'] != "" && $frSubjectDataArr[$SubjectID]['scaleInput'] != null  || empty($frSubjectDataArr[$SubjectID]['scaleInput']))
									$UpdateSubjectDataArr[$SubjectID]['scaleInput'] = $frSubjectDataArr[$SubjectID]['scaleInput'];
									
								if( $frSubjectDataArr[$SubjectID]['scaleDisplay'] != "" && $frSubjectDataArr[$SubjectID]['scaleDisplay'] != null  || empty($frSubjectDataArr[$SubjectID]['scaleDisplay']))
									$UpdateSubjectDataArr[$SubjectID]['scaleDisplay'] = $frSubjectDataArr[$SubjectID]['scaleDisplay'];
									
								if( $frSubjectDataArr[$SubjectID]['LangDisplay'] != "" && $frSubjectDataArr[$SubjectID]['LangDisplay'] != null || empty($frSubjectDataArr[$SubjectID]['LangDisplay']))
									$UpdateSubjectDataArr[$SubjectID]['LangDisplay'] = $frSubjectDataArr[$SubjectID]['LangDisplay'];
									
								if( $frSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] != "" && $frSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] != null || empty($frSubjectDataArr[$SubjectID]['DisplaySubjectGroup']))
									$UpdateSubjectDataArr[$SubjectID]['DisplaySubjectGroup'] = $frSubjectDataArr[$SubjectID]['DisplaySubjectGroup'];
									 
							} else {
								# If SchemeType is PassFail(PF), set scaleInput & scaleDisplay for default value
								$UpdateSubjectDataArr[$SubjectID]['scaleInput'] = "";
								$UpdateSubjectDataArr[$SubjectID]['scaleDisplay'] = "";
							}
						} else {
							$resultCopy[$SubjectID.'_'.$toReportID] = 0;
						}
					}
					
					# Update ClassLevel Settings Info of Target ClassLevel - [Copy To Operation]
					if(count($UpdateSubjectDataArr) > 0){
						foreach($UpdateSubjectDataArr as $UpdateID => $data){
							$UpdateDataArr = array();
							
							foreach($data as $field => $value){
								$UpdateDataArr[$field] = $value;
							}
							$UpdateDataArr['subjectID'] = $UpdateID;
							$UpdateDataArr['classLevelID'] = $toClassLevelID;
							$UpdateDataArr['ReportID'] = $toReportID;
							
							if ($this->GET_SUBJECT_FORM_GRADING($toClassLevelID, $UpdateID, 0, 0, $toReportID) == NULL)
							{
								$result['insert_sub_form_grading_'.$UpdateID.'_'.$toReportID] = $this->INSERT_SUBJECT_FORM_GRADING($UpdateDataArr);
							}
							
							$resultCopy[$UpdateID.'_'.$toReportID] = $this->UPDATE_SUBJECT_FORM_GRADING($UpdateDataArr);
							
						}
					}
				} 
			}
		}
//debug_pr($result);
//debug_pr($resultCopy);
		return $resultCopy;
	}
	
	function CHECK_REPORT_TEMPLATE_FROM_COLUMN($ReportType, $ClassLevelID){
		$returnArr = array();
		$table1 = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "	
			SELECT 
				ReportID 
			FROM 
				$table1 
			WHERE 
				Semester = '$ReportType' AND 
				ClassLevelID = $ClassLevelID AND
				isMainReport = 1
		";
		$returnArr = $this->returnVector($sql);
		if(count($returnArr) > 0)
			return $returnArr[0];
		else 
			return false;
	}
	
	function getDisplayMarkdp($DisplayType='', $mark=0)
	{
		$dpAry = $this->LOAD_SETTING("Storage&Display", $DisplayType);
		$dp = $dpAry[$DisplayType];
		return my_round($mark, $dp);
	}
	
	function returnSubjectTeacher($ClassID, $SubjectID, $ReportID, $StudentID, $FullTeacherInfo=0)
	{
		/*
		$NameField = getNameFieldByLang("b.");
		
		$sql ="
			select 
				$NameField
			from 
				RC_SUBJECT_TEACHER as a
				left join INTRANET_USER as b on (a.UserID = b.UserID)
			where 
				a.ClassID=$ClassID and 
				a.SubjectID=$SubjectID
			";
		return $this->returnVector($sql);
		*/
		
		global $PATH_WRT_ROOT;
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$Semester = $ReportSetting['Semester'];
		$SubjectGroupID = $this->Get_Student_Studying_Subject_Group($Semester, $StudentID, $SubjectID);
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		$SubjectGroupObj = new subject_term_class($SubjectGroupID, $GetTeacherList=true);
		
		if ($FullTeacherInfo == 0)
			return Get_Array_By_Key($SubjectGroupObj->ClassTeacherList, 'TeacherName');
		else if ($FullTeacherInfo == 1)
			return $SubjectGroupObj->ClassTeacherList;
	}
	
	function IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID)
	{
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $this->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		$FullYearCalculationType = $categorySettings['OrderFullYear'];
		
		# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
		// Loading Report Template Data
		$basic_data = $this->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $this->returnReportTemplateColumnData($ReportID); 		// Column Data
		$ColumnSize = sizeof($column_data);
		$ColumnTitleAry = $this->returnReportColoumnTitle($ReportID);
		$ReportTitle = $basic_data['ReportTitle'];
		$ReportType = $basic_data['Semester'];
		$isPercentage = $basic_data['PercentageOnColumnWeight'];
		$ClassLevelID = $basic_data['ClassLevelID'];
		
		# Get Subject Components
		$CmpSubjectArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
		$CmpSubjectIDArr = array();
		if(!empty($CmpSubjectArr)){	
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
				
				# Modified by Marcus 20101011 (for template based grading scheme) 
				$CmpSubjectFormGradingArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
				if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
					$isCalculateByCmpSub = 1;
					
			}
		}
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if(count($column_data) > 0){
		    	for($i=0 ; $i<sizeof($column_data) ; $i++){
					//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					//$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					//$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
					
					// modified on 30 July 2008
		            for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
		            	$OtherCondition = "SubjectID = '".$CmpSubjectArr[$j]['SubjectID']."' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
						$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
		            	$WeightAry[$weight_data[0]['ReportColumnID']][$CmpSubjectArr[$j]['SubjectID']] = $weight_data[0]['Weight'];
		            }
				}
			}
		}
		else {
			$weight_data = $this->returnReportTemplateSubjectWeightData($ReportID);
			for($i=0 ; $i<sizeof($weight_data) ; $i++)
				$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
		}
		
		# check if all component weight is zero
		$isAllCmpSubjectWeightZeroArr = array();
		for ($i=0; $i<sizeof($column_data); $i++)
		{
			$isAllZero = 1;
			$thisReportColumnID = $column_data[$i]['ReportColumnID'];
			for ($j=0; $j<sizeof($CmpSubjectIDArr); $j++)
			{
				$thisCmpSubjectID = $CmpSubjectIDArr[$j];
				
				if ($WeightAry[$thisReportColumnID][$thisCmpSubjectID] != 0)
				{
					$isAllZero = 0;
					break;
				}
			}
			$isAllCmpSubjectWeightZeroArr[$thisReportColumnID] = $isAllZero;
		}
		
		/*
		$isAllCmpSubjectWeightZero = true;
		foreach ($isAllCmpSubjectWeightZeroArr as $thisReportColumnID => $isZero)
		{
			if ($isZero == false)
			{
				$isAllCmpSubjectWeightZero = false;
				break;
			}
		}
		*/
		
		return $isAllCmpSubjectWeightZeroArr;
	}
	
	/*
	* Get Grade & GradingSchemeRangeID From Grading Scheme Range Table
	* Default to return an Array in format of $returnArr[][] so as to generate a Selection Box
	*/
	function GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID='') {
		$PreloadArrKey = 'GET_GRADE_FROM_GRADING_SCHEME_RANGE';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		$returnArr = array();
		$gradeRangeInfo = array();
		
		if ($SchemeID != '') {
			$conds_SchemeID = " And SchemeID = '".$SchemeID."' "; 
		}
		
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql = "SELECT 
					GradingSchemeRangeID, 
					Grade,
					CASE Nature
						WHEN 'D' THEN 1
						WHEN 'P' THEN 2
						WHEN 'F' THEN 3
					END AS DisplayOrder 
				FROM 
					$table 
				WHERE 
					1
					$conds_SchemeID
				ORDER BY 
					DisplayOrder ASC , GradingSchemeRangeID ASC
				";
//		$sql  = "SELECT GradingSchemeRangeID, Grade FROM $table WHERE SchemeID = $SchemeID ORDER BY GradingSchemeRangeID";
		$gradeRangeInfo = $this->returnArray($sql);
		
		if(count($gradeRangeInfo) > 0){
			for($i=0 ; $i<count($gradeRangeInfo) ; $i++)
				$returnArr[$gradeRangeInfo[$i]['GradingSchemeRangeID']] = $gradeRangeInfo[$i]['Grade'];
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		unset($gradeRangeInfo);
		return $returnArr;
	}
	
	function CONVERT_GRADE_TO_GRADE_POINT_FROM_GRADING_SCHEME($SchemeID, $Mark, $ReportID="", $StudentID="", $SubjectID="", $ClassLevelID="", $ReportColumnID=""){
		$returnVal = '';
		$RangeInfo = array();
		$SchemeInfo = array();
		$GradeRange = array();
		
		if($SchemeID != "" && $SchemeID != null){
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$RangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
			
			if(count($RangeInfo) > 0){
				$isStart = 1;
				for($i=0 ; $i<count($RangeInfo) ; $i++){
					$j = $i - 1;
					if($RangeInfo[$i]['Grade'] != "" && $RangeInfo[$i]['GradePoint'] != ""){
						if ($Mark == $RangeInfo[$i]['Grade'])
						{
							return $RangeInfo[$i]['GradePoint'];
						}
					}
				}
			}
		}
		
		return 0;
	}
	
	### GrandMS Statistics Calculation functions ###
	
	function getAverage($array, $decimalPlace=""){
		$sizeArray = count($array);
		$validCount = 0;
		$sum = 0;
		for ($i=0; $i<$sizeArray; $i++)
		{
			$thisValue = $array[$i];
			if (is_numeric($thisValue))
			{
				$sum += $thisValue;
				$validCount++;
			}
		}
		if ($validCount==0)
		{
			$average = 0;
			return "--";
		}
		else
		{
			//debug_pr('sum = '.$sum);
			//debug_pr('validCount = '.$validCount);
			$average = $sum / $validCount;
		}
		
	    $returnValue = ($decimalPlace=="")? $average : my_round($average, $decimalPlace);
	    return $returnValue;
	}
	
	/*
	 *	Equations:
	 *	Variance = {Sum of [(value - mean)^2]} / (size of array - 1)
	 *	Deviation = sqrt(Variance)
	 */
	function getVariance($array, $decimalPlace=""){
		global $eRCTemplateSetting;
		
		if (!is_array($array) || count($array) <=1 )
			return my_round(0, $decimalPlace);
			
	    $avg = $this->getAverage($array);
	    if($avg=="--")
	    	return "--";
	    
	    foreach ($array as $value) {
	        $variance[] = pow($value-$avg, 2);
	    }
	    
	    if ($eRCTemplateSetting['DoNotMinusOneWhenCalculateSD'] == true)
	    	$denominator = count($variance);
	    else
	    	$denominator = count($variance) - 1;
	    
	    $variance = array_sum($variance) / $denominator;
	    $returnValue = ($decimalPlace=="")? $variance : my_round($variance, $decimalPlace);
	    return $returnValue;
	}
	
	function getSD($array, $decimalPlace=""){
		if (!is_array($array) || count($array) == 0)
			return "--";
			
	    $variance = $this->getVariance($array);
	    if($variance=="--")
	    	return "--";
	    	
	    $deviation = sqrt($variance);
	    $returnValue = ($decimalPlace=="")? $deviation : my_round($deviation, $decimalPlace);
	    return $returnValue;
	}
	
	# 20100210 Marcus add Param $ReportColumnID 
	function getFormSubjectSDAndAverage($ReportID, $SubjectID='', $ClassID='',$ReportColumnID=0)
	{ 
		global $eRCTemplateSetting
		;
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting["ClassLevelID"];
		
		# Get Students
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID, "", $ReturnAsso=1);
		
		# Get Exclude Order Student list
		$excludeStudentArr = $this->GET_EXCLUDE_ORDER_STUDENTS();
		
		# Get Marks
		$SpcialCase = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		$MarksAry = $this->getMarks($ReportID, '', '', 0, 1, '', '',$ReportColumnID);
		
		foreach((array)$MarksAry as $StudentID =>$SubjectDataArr)
		{
			# Exclude the student in the Exclude Order Student list in the SD and Average Calculation
			if (in_array($StudentID, $excludeStudentArr))
				continue;
				
			# If selected specific Class, skip the student if the student is not in that class
			if(!empty($ClassID))
			{
				//$thisClassInfo = $this->Get_Student_Class_ClassLevel_Info($StudentID);
				//if($ClassID!=$thisClassInfo[0]['ClassID'])
				
				if ($StudentInfoArr[$StudentID]['YearClassID'] != $ClassID)
					continue;
			}
			
			foreach((array) $SubjectDataArr as $thisSubjectID =>$ColumnDataArr)
			{
				if ($SubjectID != '' && $thisSubjectID != $SubjectID)
					continue;
					
				# Modified by Marcus 20101011 (for template based grading scheme) 
				if (isset($SubjectFormGradingSettings[$thisSubjectID]) == false)
					$SubjectFormGradingSettings[$thisSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 0,0, $ReportID);
				
				$SchemeID = $SubjectFormGradingSettings[$thisSubjectID]['SchemeID'];
				
				if (isset($SchemeInfo[$SchemeID]) == false)
					$SchemeInfo[$SchemeID] = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				foreach((array) $ColumnDataArr as $ColumnID =>$ColumnData)
				{
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $ColumnData['Grade']!='' )? $ColumnData['Grade'] : "";
					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $ColumnData['Mark'] : "";
					
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					if (!in_array($thisMark,$SpcialCase) && $thisMark!= -1)
						(array)$SubjectAllMarks[$thisSubjectID][] = $ColumnData["RawMark"];
					//debug_pr($ColumnData);
//					if(is_numeric($ColumnData["RawMark"]))
				}
			}
		}
		
		$GrandMarkArr = $this->getReportResultScore($ReportID, $ReportColumnID, '', '', 0, $includeAdjustedMarks=1, 0, 0);
		foreach((array)$GrandMarkArr as $thisStudentID => $thisStudentGrandMarkArr)
		{
			# Exclude the student in the Exclude Order Student list in the SD and Average Calculation
			if (in_array($thisStudentID, $excludeStudentArr))
				continue;
				
			if ($ClassID != '' && $StudentInfoArr[$thisStudentID]['YearClassID'] != $ClassID)
				continue;
					
			$thisStudentColumnGrandMarkArr = $thisStudentGrandMarkArr[$ReportColumnID];
			$thisGrandAverage = $thisStudentColumnGrandMarkArr['GrandAverage'];
			$thisGrandTotal = $thisStudentColumnGrandMarkArr['GrandTotal'];
			$thisGPA = $thisStudentColumnGrandMarkArr['GPA'];
			
			if ($thisGrandAverage != '-1')
				(array)$SubjectAllMarks['GrandAverage'][] = $thisGrandAverage;
				
			if ($thisGrandTotal != '-1')
				(array)$SubjectAllMarks['GrandTotal'][] = $thisGrandTotal;
				
			if ($thisGPA != '-1')
				(array)$SubjectAllMarks['GPA'][] = $thisGPA;
		}
		
		foreach((array)$SubjectAllMarks as $thisSubjectID => $thisMarksAry)
		{
			if ($eRCTemplateSetting['UseRawMarkToCalculateSDScore'])
				$Rounding = '';
			else
				$Rounding = 2;
				
			$SD[$thisSubjectID] = $this->getSD($thisMarksAry, $Rounding);
			$AVG[$thisSubjectID] = $this->getAverage($thisMarksAry, $Rounding);
			
			/*
			hdebug_pr('$ReportColumnID = '.$ReportColumnID);
			hdebug_pr('$thisSubjectID = '.$thisSubjectID);
			hdebug_pr($thisMarksAry);
			hdebug_pr('$AVG = '.$AVG[$thisSubjectID]);
			hdebug_pr('$SD = '.$SD[$thisSubjectID]);
			*/
		}
		
		if($SubjectID!='')
			return array($SD[$SubjectID],$AVG[$SubjectID]);
		else
			return array($SD,$AVG);
	}	
	function getPassingRate($array, $decimalPlace="", $withFailRate=0){
		if (!is_array($array) || count($array) == 0)
			return 0;
			
		$totalNum = count($array);
		
		$resultArr = @array_count_values($array);
		$totalNum = $totalNum-$resultArr["N.A."];
		if($totalNum<=0)
			return 0;
		
		$passingNum = $resultArr["Distinction"] + $resultArr["Pass"];
		$failNum = $resultArr["Fail"];
		$passingRate = ($passingNum / $totalNum) * 100;
		$failRate = ($failNum / $totalNum) * 100;
		$PassValue = (($decimalPlace=="")? $passingRate : my_round($passingRate, $decimalPlace))."%";
		$FailValue = (($decimalPlace=="")? $failRate : my_round($failRate, $decimalPlace))."%";
		
		$returnValue = $withFailRate==1?array($PassValue,$FailValue):$PassValue;

	    return $returnValue;
	}
	
	function getPassingNumber($array, $withFailNumber=0){
		if (!is_array($array) || count($array) == 0)
			return 0;
			
		$resultArr = @array_count_values($array);
		
		$passingNum = $resultArr["Distinction"] + $resultArr["Pass"];
		$passingNum = $passingNum?$passingNum:0;
		$failNum = $resultArr["Fail"]?$resultArr["Fail"]:0;
		
		$returnValue = $withFailNumber==1?array($passingNum,$failNum):$passingNum;
		
	    return $returnValue;
	}
	
	function getPassingRateByMarkArr($MarksArr, $PassingMark, $decimalPlace='')
	{
		$PassNumber = $this->getPassingNumberByMarkArr($MarksArr, $PassingMark);
		if($PassNumber == 0)
			return 0;
		
		list($pass, $fail, $total) = $PassNumber;
		
		$PassRate = ($pass/$total*100);
		$FailRate = ($fail/$total*100);
		
		$PassRate = ($decimalPlace==''?$PassRate:my_round($PassRate,$decimalPlace))."%";
		$FailRate = ($decimalPlace==''?$FailRate:my_round($FailRate,$decimalPlace))."%";
		
		return array($PassRate, $FailRate);
	}
	
	function getPassingNumberByMarkArr($MarksArr, $PassingMark)
	{
		$pass=$fail=$total=0;
		foreach($MarksArr as $Mark)
		{
			if($Mark=="N.A." || $Mark==-1 || $Mark=='')
				continue;
			
			if($Mark>=$PassingMark)
				$pass++;
			else
				$fail++;
			
			$total++;		
		}
		
		if($total==0)
			return 0;
		
		return array($pass, $fail, $total);
	}
	
	function getMaximum($array)
	{
		$max = "";
		$array=array_values((array)$array);
		$numOfElement = count($array);
		
		for ($i=0; $i<$numOfElement; $i++)
		{
			$thisValue = $array[$i];
			
			if (($thisValue > $max) && is_numeric($thisValue))
				$max = $thisValue;
		}
		
		return $max;
	}
	
	function getMinimum($array)
	{
		$min = "";
		$array=array_values((array)$array);
		$numOfElement = count($array);
		$isFirst = 1;
		for ($i=0; $i<$numOfElement; $i++)
		{
			$thisValue = $array[$i];
			
			if ($isFirst)
			{
				if (is_numeric($thisValue))
				{
					$min = $thisValue;
					$isFirst = 0;
				}
			}
			else
			{
				if (($thisValue < $min) && is_numeric($thisValue))
					$min = $thisValue;
			}
		}
		
		return $min;
	}

	### End of GrandMS Statistics Calculation functions ###
	
	function returnGrandTotalFullMark($ReportID, $StudentID='', $checkExemption=0, $excludeDisplayGradeSubject=0, $excludeSPFullChecking=0)
	{
		# Retrieve Display Settings
		$ReportSetting 	= $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		$SemID 			= $ReportSetting['Semester'];
 		$ReportType 	= $SemID == "F" ? "W" : "T";	
 		
 		# Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$categorySettings = $this->LOAD_SETTING('Calculation');
		if ($ReportType == "T")
			$TermCalculationType = $categorySettings['OrderTerm'];
		else
			$TermCalculationType = $categorySettings['OrderFullYear'];
		
		# check if the grand total is abnormal
		if ($excludeSPFullChecking == 0)
		{
			$gt = $this->returnStudentSubjectSPFullMark($ReportID, 0, $StudentID, 0);
			if($gt)	return $gt[0];
		}
				
		$MarksAry = $this->getMarks($ReportID, $StudentID);						# Student Marks

		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 0, array(), 1);
		//debug_r($SubjectFullMarkAry);
		$GrandTotalFullMark = 0;
		
		$SubjectGradingSchemeArr = array();
		if ($excludeDisplayGradeSubject == 1)
		{
			# Modified by Marcus 20101011 (for template based grading scheme)
			$SubjectGradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=1, $returnAsso=1, $ReportID);
		}
		
		foreach($SubjectFullMarkAry as $SubjectID=>$thisFullMark)
		{
			if ($excludeDisplayGradeSubject == 1 && $SubjectGradingSchemeArr[$SubjectID]['ScaleDisplay']=='G')
				continue;
			
			//$thisWeightAry = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$SubjectID and ReportColumnID is NULL");
			//$thisWeight = $thisWeightAry[0]['Weight'];
			
			if($TermCalculationType == 1){
				$OtherCondition = "SubjectID = ".$SubjectID." AND ReportColumnID IS NULL ";
			} else {
				$OtherCondition = "SubjectID = ".$SubjectID;
			}
			$thisWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
			$thisWeight = $thisWeightArr[0]['Weight'];
			
			if((!empty($MarksAry[$SubjectID]) or !$StudentID))	
			{
				if (!$checkExemption || (in_array($MarksAry[$SubjectID][0]['Grade'], $this->specialCasesSet1)==false && in_array($MarksAry[$SubjectID][0]['Grade'], $this->specialCasesSet2)==false))
				{
					$GrandTotalFullMark += $thisFullMark * $thisWeight;
				}
			}
	/*
			$SPFull = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID);
			
			if(!empty($SPFull))		
				$x += $SPFull[0];
			else
				if(!empty($MarksAry[$SubjectID]) or !$StudentID)	
					$x += $v;	
*/
		}
		return $GrandTotalFullMark;
	}
	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function isAllSubjectsDisplayGrade($ClassLevelID, $ReportID='')
	{
		
		$subjectList = $this->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);
		
		$isAllSubjectDisplayGrade = true;
		foreach ($subjectList as $subjectID => $subjectInfo) {
			// $schemeInfo[0] is main info, [1] is ranges info
			$schemeInfo = $this->GET_GRADING_SCHEME_INFO($subjectInfo["schemeID"]);
			$scaleDisplay = $subjectInfo["scaleDisplay"];
			
			if ($scaleDisplay != "G")
				$isAllSubjectDisplayGrade = false;
		}
		
		return $isAllSubjectDisplayGrade;
	}
	
	function GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=0) {
		$PreloadArrKey = 'GET_FORM_NUMBER';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		if ($ByWebSAMSCode) {
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			
			$objYear = new Year($ClassLevelID);
			$FormNumberDetermineText = $objYear->WEBSAMSCode;
		}
		else {
			$FormNumberDetermineText = $this->returnClassLevel($ClassLevelID);
		}
		$FormNumber = substr($FormNumberDetermineText, strlen($FormNumberDetermineText)-1, 1);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $FormNumber);
		return $FormNumber;
	}
	
	# Arrage the order of report display (for Tong Nam)
	function Get_Num_Of_Column($ReportID)
	{			
		$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";		
		$sql = "	
			SELECT 
				SemesterNum	
			FROM 
				$table 
			WHERE
				ReportID = $ReportID
		";
		$tempArr = $this->returnVector($sql);
		$NumCol = sizeof($tempArr);
		
		return $NumCol;
	}
	
	function Get_Consolidated_Report_List($ClassLevelID, $ReportID)
	{			
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$sql = "	
			SELECT 
					a.ReportID,
					a.Semester
			FROM 
					$table as a
				left join INTRANET_CLASSLEVEL as b on a.ClassLevelID = b.ClassLevelID
			WHERE 
					a.ClassLevelID = '$ClassLevelID'
				AND
					a.ReportID != '$ReportID'
				AND
					a.Semester = 'F'
		";
		$consolidatedReportIDArr = $this->returnVector($sql);
		
		# get column data of consolidated reports
		$SemesterArr = array();
		$SemesterReportIDMapArr = array();
		$NumColArr = array();
		for ($i=0; $i<sizeof($consolidatedReportIDArr); $i++)
		{
			$thisReportID = $consolidatedReportIDArr[$i];
			
			$table = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN";		
			$sql = "	
				SELECT 
					SemesterNum	
				FROM 
					$table 
				WHERE
					ReportID = $thisReportID
			";
			$tempArr = $this->returnVector($sql);
			
			$thisSemIDList = implode(",", $tempArr);
			$SemesterReportIDMapArr[$thisSemIDList] = $thisReportID;
			$SemesterArr[$i] = $thisSemIDList;
		}
		
		# sort consolidated reports. After sorting:
		# [0] = 1st Term Whole Term ReportID
		# [1] = 2nd Term Whole Term ReportID
		sort($SemesterArr);
		$sortedConsolidatedReportArr = array();			
		for ($i=0; $i<sizeof($SemesterArr); $i++)
		{
			$thisSemIDList = $SemesterArr[$i];
			$thisReportID = $SemesterReportIDMapArr[$thisSemIDList];
			$sortedConsolidatedReportArr[] = $thisReportID;
		}
		
		return $sortedConsolidatedReportArr;
	}
	
	function Get_Empty_Row($Height, $NumOfRow=1)
	{
		global $PATH_WRT_ROOT;
		
		$x = '';
		for ($i=0; $i<$NumOfRow; $i++)
			$x .= "<tr><td>".$this->Get_Empty_Image($Height)."</td></tr>";
		
		return $x;
	}
	
	function Get_Empty_Image_Div($Height)
	{
		return "<div>".$this->Get_Empty_Image($Height)."</div>";
	}
	
	function Get_Empty_Image($Height)
	{
		global $PATH_WRT_ROOT;
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		return "<img src='".$emptyImagePath."' height='".$Height."'>";
	}
	
	public function Get_Tick_Symbol() {
		return '<span style="font-family:Wingdings 2;">P</span>';
	}
	
	function Get_Student_Age($DateOfBirth, $DecimalPlace=0)
	{
		if ($DateOfBirth == '')
			return 0;
			
		# $DateOfBirth should be in the format yyyy-mm-dd
		$BirthArr = explode("-", $DateOfBirth);
		$BirthYear = $BirthArr[0];
		$BirthMonth = $BirthArr[1] + 0;
		$BirthDay = $BirthArr[2] + 0;
		
		$ageTime = mktime(0, 0, 0, $BirthMonth, $BirthDay, $BirthYear);
		$nowTime = time();
		$year = 60 * 60 * 24 * 365;
		
		$age = ($nowTime - $ageTime) / $year;
		
		if ($DecimalPlace==0)
			$returnAge = floor($age);
		else
			$returnAge = my_round($age, $DecimalPlace);
		
		return $returnAge;
	}
	
	function GET_CSV_FILE($ParFolderPath)
	{
		if (file_exists($ParFolderPath)) {
			$handle = opendir($ParFolderPath);
			
			while (($file = readdir($handle))!==false) {
				if($file!="." && $file!="..") {
					
					$SplitArr = explode(".", $file);
					
					# updated on 31 Dec by Ivan
					# the code below will not work with this file name because there are two "."
					# 2008_1_P.6 Fortitude.csv
					# $ext = substr(basename($file), (strpos(basename($file), ".")+1));
					
					# new code
					$ext = $SplitArr[count($SplitArr)-1];
					
					if($ext=="csv") {
						$FileArray[] = $file;
					}
				}
			}
			if (is_array($FileArray) && sizeof($FileArray)>0) {
				sort($FileArray);
			}
			
			closedir($handle);
		}
		return $FileArray;
	}
	
	function showWarningMsg($title, $content, $others="")
	{
		$showmsg =  "<fieldset class='instruction_box'>";
		$showmsg .=  "<legend class='instruction_title'>";
		$showmsg .=  $title;
		$showmsg .=  "</legend>";
		$showmsg .=  "<span $others>";
		$showmsg .=  $content;
		$showmsg .=  "</span>";
		$showmsg .=  "</fieldset>";

		return $showmsg;
	}
	
	/**
	 * Get max GPA
	 */
	function GET_MAX_GRADE_POINT() 
	{
		$table = $this->DBName.".RC_GRADING_SCHEME_RANGE";
		$sql  = " select max(GradePoint) from $table";
		return  $this->returnVector($sql);
	}
	
	function Get_GPA_Selection($Id, $Name, $SelectedGPA='', $Onchange='')
	{
		$MaxGPAAry = $this->GET_MAX_GRADE_POINT();
		
		if ($MaxGPAAry==NULL || count($MaxGPAAry)==0)
			return "";
		
		$MaxGPA = $MaxGPAAry[0]+0;
		$GPA_selectionAry = array();
		for ($i=$MaxGPA;$i>=0.1;$i=$i-0.1)
		{	
			$k = (strpos($i, ".")=== false) ? $i.".0" : $i;
			$GPA_selectionAry[] = array($k, $k);
		}
		$GPA_selectionAry[] = array("0.0", "0.0");
		
		$tags = 'name="'.$Name.'" id="'.$Id.'" onchange="'.$Onchange.'"';
		
		$GPA_selection = getSelectByArray($GPA_selectionAry, $tags, $SelectedGPA);
		
		return $GPA_selection;
	}
	
	function Get_Conduct_CheckBoxes($Name, $Id_Suffix='')
	{
		$ConductAry = $this->ConductAry;
		
		$Conduct_CheckBoxes = "";
		if(sizeof($ConductAry))
		{
			foreach($ConductAry as $key=>$data)
			{
				$Conduct_CheckBoxes .= "<input type='checkbox' name='".$Name."' value='$data' id='".$data.$Id_Suffix."'> <label for='".$data.$Id_Suffix."'>$data</label> ";
			}
		}
		
		return $Conduct_CheckBoxes;
	}
	
	function Get_Terms_CheckBoxes($Name, $hasConsolidate='')
	{
		global $eReportCard;
		
		$TermInfoArr = $this->returnReportTemplateTerms();
		$Terms_CheckBoxes = "<input id='AllTerm' type='checkbox' onclick='$(\"[name=$Name]\").attr(\"checked\",this.checked); '> <label for='AllTerm'>".$eReportCard['AllTerms']."</label>";
		$Terms_CheckBoxes .= "<br>"; 
		if(sizeof($TermInfoArr))
		{
			foreach($TermInfoArr as $key=>$data)
			{
				if(!$hasConsolidate) {if($data[0]=='F') continue;}
				$Terms_CheckBoxes .= "<input type='checkbox' name='".$Name."' value='".$data[0]."' id='Term".$data[0]."' onclick='var all=document.getElementById(\"AllTerm\"); if(!this.checked) {all.checked=false;}'> <label for='Term".$data[0]."'>".$data[1]."</label> ";
			}
		}
		
		return $Terms_CheckBoxes;
	}

	function Get_Honours_CheckBoxes($Name)
	{
		global $eReportCard;
		
		$Honour_CheckBoxes = "<input id='AllHonour' type='checkbox' onclick='$(\"[name=$Name]\").attr(\"checked\",this.checked); '> <label for='AllTerm'>".$eReportCard['AllHonour']."</label>";
		$Honour_CheckBoxes .= "<br>"; 
		if(sizeof($eReportCard['Honour']))
		{
			foreach($eReportCard['Honour'] as $key=>$data)
			{
				$Honour_CheckBoxes .= "<input type='checkbox' name='".$Name."' value='".$key."' id='Honour".$key."' onclick='var all=document.getElementById(\"AllHonour\"); if(!this.checked) {all.checked=false;}'> <label for='Honour".$key."'>".$data."</label> ";
			}
		}
		
		return $Honour_CheckBoxes;
	}

	
	# $MarkArr[$SubjectID][$ReportColumnID] = $MarkInfoArr
	function Get_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $MarkArr)
	{
		if (!is_array($MarkArr))
			return array();
		
		$returnNatureArr = array();
		foreach ((array)$MarkArr as $SubjectID => $ReportColumnDataArr)
		{
			# Modified by Marcus 20101011 (for template based grading scheme)
			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1 ,0 ,$ReportID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$SchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$TopPercentage = $SchemeInfoArr['TopPercentage'];
		
			foreach ((array)$ReportColumnDataArr as $ReportColumnID => $ReportColumnMarkArr)
			{
				$thisMark = $ReportColumnMarkArr['Mark'];
				$thisGrade = $ReportColumnMarkArr['Grade'];
				
				$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisGrade!='' )? $thisGrade : "";
				$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMark : "";
				
				$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
								
				# ignore special handling grade
				if (!$this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMark) && !$this->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMark))
				{
					$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark,'','',$ReportID);
					
					$returnNatureArr[$SubjectID] = $thisNature;
				}
			}
		}
		
		return $returnNatureArr;
	}
	
	# $MarkArr[$StudentID][$SubjectID][$ReportColumnID] = $MarkInfoArr
	function Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $MarkArr)
	{
		if (!is_array($MarkArr))
			return array();
		
		$returnNatureArr = array();
		foreach ((array)$MarkArr as $StudentID => $thisStudentMarkArr)
		{
			foreach ((array)$thisStudentMarkArr as $SubjectID => $ReportColumnDataArr)
			{
				# Modified by Marcus 20101011 (for template based grading scheme)
				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1, 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$SchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$TopPercentage = $SchemeInfoArr['TopPercentage'];
			
				foreach ((array)$ReportColumnDataArr as $ReportColumnID => $ReportColumnMarkArr)
				{
					$thisMark = $ReportColumnMarkArr['Mark'];
					$thisGrade = $ReportColumnMarkArr['Grade'];
					
					$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $thisGrade!='' )? $thisGrade : "";
//					$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $thisMark : "";
					$thisMark = ($ScaleDisplay=="M") ? $thisMark : "";
										
					$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
					
					# ignore special handling grade
					if (!$this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMark) && !$this->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMark))
					{
						if($ScaleDisplay == "M" && $TopPercentage > 0)
						{
							$thisStyleDetermineMarks = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $SubjectID, $ClassLevelID, $ReportColumnID);
							$ConvertBy = "G";
						}
						else
						{
							$thisStyleDetermineMarks = $thisMark;
							$ConvertBy = "";
						}
						
						$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisStyleDetermineMarks,'',$ConvertBy,$ReportID);
	  											
						$returnNatureArr[$StudentID][$SubjectID][$ReportColumnID] = $thisNature;
					}
				}
			}
		}
		return $returnNatureArr;
	}

	# $MarkArr[$StudentID][$SubjectID][$ReportColumnID] = $MarkInfoArr
	function Get_Student_Grand_Mark_Nature_Arr($ReportID, $ClassLevelID, $GrandMarkArr)
	{
		
		if (!is_array($GrandMarkArr))
			return array();
			
		$GrandMarkGradingScheme = $this->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);

		$returnNatureArr = array();
		$RankDetermineField = $this->Get_Grand_Position_Determine_Field();
		$RankDetermineFieldID = $this->Get_Grand_Field_SubjectID($RankDetermineField);
		
		foreach ((array)$GrandMarkArr as $StudentID => $thisStudentMarkArr)
		{
			foreach ((array)$thisStudentMarkArr as $thisReportColumnID => $ReportColumnData)
			{
				$SchemeID = $GrandMarkGradingScheme[$RankDetermineFieldID]['SchemeID'];
				$ScaleDisplay = $GrandMarkGradingScheme[$RankDetermineFieldID]['ScaleDisplay'];
				$SchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$TopPercentage = $SchemeInfoArr['TopPercentage'];
				
				$thisMark = $ReportColumnData[$RankDetermineField];
				$thisGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark, $ReportID, $StudentID, $RankDetermineFieldID, $ClassLevelID, $thisReportColumnID);
				$thisNature = $this->returnMarkNature($ClassLevelID, $RankDetermineFieldID, $thisGrade,'','G',$ReportID);
				$returnNatureArr[$StudentID][$thisReportColumnID][$RankDetermineField] = $thisNature;
			}
		}

		return $returnNatureArr;
	}
		
	# $MarkArr[$SubjectID][$ReportColumnID] = $MarkInfoArr
	function Get_Examed_Subject_Arr($ReportID, $MarkArr)
	{
		if (!is_array($MarkArr))
			return array();
			
		$returnExamedSubjectIDArr = array();
		foreach ((array)$MarkArr as $SubjectID => $ReportColumnDataArr)
		{
			foreach ((array)$ReportColumnDataArr as $ReportColumnID => $ReportColumnMarkArr)
			{
				$thisGrade = $ReportColumnMarkArr['Grade'];
				
				# ignore special handling grade
				if (!$this->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisGrade) && !$this->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisGrade))
				{
					$returnExamedSubjectIDArr[] = $SubjectID;
				}
			}
		}
		
		return $returnExamedSubjectIDArr;
	}
	
	function Get_ClassLevel_By_ClassID($ParClassID)
	{
		include_once("form_class_manage.php");
		$objYearClass = new year_class($ParClassID, true);
		
		return $objYearClass->YearID;
	}

	function Get_GrandMS_CSS()
	{
		$css = '<style type="text/css">
				<!--
				h1 {
					font-size: 20px;
					font-family: arial, "lucida console", sans-serif;
					margin: 0px;
					padding-top: 3px;
					padding-bottom: 10px;
				}
				.GrandMSContentTable {
					border-collapse: collapse;
					border-color: #000000;
				}
				.GrandMSContentTable thead {
					font-size: 12px;
					font-family: arial, "lucida console", sans-serif;
				}
				.GrandMSContentTable tbody {
					font-size: 12px;
					font-family: arial, "lucida console", sans-serif;
				}
				.footer {
					font-family: "Arial", "Lucida Console";	
					font-size: 12px;
				}
				.summary_header
				{
					border-top:1px solid #000;
					border-bottom:1px solid #000;
				}
				.border_bottom
				{
					border-bottom:1px solid #000;
				}
				.text_12px {
					font-family: "Arial", "Lucida Console";	
					font-size: 12px;
				}
				-->
				</style>';
		return $css;
	}
	
	function Get_Report_Header($title='')
	{
		$header = '';
		$header .= '<html>';
		$header .= '<head>';
		$header .= '<meta http-equiv="pragma" content="no-cache" />';
		$header .= '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
		
		if ($title != '')
		$header .= '<title>'.$title.'</title>';
		
		$header .= '</head>';
		
		return $header;
	}

	function Get_Report_Footer()
	{
		$footer = '';
		$footer .= '</html>';
		
		return $footer;
	}	
	
	function Get_Order_Criteria_Selection($Id_Name, $IncludeSDScore=0)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		$CriteriaArray = array();
		$CriteriaArray['GrandAverage'] = $eReportCard['Template']['GrandAverage'];
		$CriteriaArray['GrandTotal'] = $eReportCard['Template']['GrandTotal'];		
		$CriteriaArray['GPA'] = $eReportCard['GPA'];
		if($IncludeSDScore==1 && $eRCTemplateSetting['OrderPositionMethod']=="WeightedSD")
			$CriteriaArray['GrandSDScore'] = $eReportCard['GrandStandardScore'];	
		
		$selectionTags = ' id="'.$Id_Name.'" name="'.$Id_Name.'" ';
		$selected = $this->Get_Grand_Position_Determine_Field();
		$selection = getSelectByAssoArray($CriteriaArray, $selectionTags, $selected, $all=0, $noFirst=1);
			
		return $selection;
	}
	
	function Get_ClassLevel_Report_List($ClassLevelID, $TermReportOnly=0)
	{
		$cond_semester = '';
		if ($TermReportOnly)
			$cond_semester = " AND Semester != 'F' ";
		
		$conds = " ClassLevelID = '$ClassLevelID' $cond_semester ORDER BY Semester ";
		$ReportArr = $this->returnReportTemplateBasicInfo($ReportID='', $conds);
		
		return $ReportArr;
	}

	function Get_Term_Selection($ID_Name, $AcademicYearID='', $SelectedYearTermID='', $OnChange='', $NoFirst=0, $NoPastTerm=0, $withWholeYear=1,$isMultiple=0)
	{
		global $Lang, $eReportCard, $PATH_WRT_ROOT;
		
		# Set to active Academic Year if there is no target Academic Year
		if ($AcademicYearID == '')
			$AcademicYearID = $this->GET_ACTIVE_YEAR_ID();
		
		# Set to currect Semester if there is no target semester
		if ($SelectedYearTermID == '')
			$SelectedYearTermID = $CurrentYearTermArr['YearTermID'];
		
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$fcm = new form_class_manage();
		$YearTermArr = $fcm->Get_Academic_Year_Term_List($AcademicYearID, $NoPastTerm);
		$numOfYearTerm = count($YearTermArr);
	
		$selectArr = array();
		
		if ($withWholeYear)
			$selectArr['F'] = $eReportCard['WholeYear'];
			
		for ($i=0; $i<$numOfYearTerm; $i++)
		{
			$thisYearTermID = $YearTermArr[$i]['YearTermID'];
			$thisYearTermName = Get_Lang_Selection($YearTermArr[$i]['YearTermNameB5'],$YearTermArr[$i]['YearTermNameEN']);
			
			$selectArr[$thisYearTermID] = $thisYearTermName;
		}
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = 'onchange="'.$OnChange.'"';
				
		$multiple ='';
		if($isMultiple ==1)
		{
			$multiple = 'multiple';
		}
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$multiple;
		$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Term']);

		$semesterSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearTermID, $all=0, $NoFirst, $firstTitle);

		return $semesterSelection;
	}
	
	function Get_Student_ClassName($ParUserID)
	{
		$sql = "SELECT
						DISTINCT(yc.ClassTitleEN)
				FROM
						YEAR_CLASS_USER as ycu
						INNER JOIN
						YEAR_CLASS as yc 
						ON (ycu.YearClassID = yc.YearClassID AND ycu.UserID = '$ParUserID' AND yc.AcademicYearID = '".$this->schoolYearID."')
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet[0];
	}
	
	function Get_Student_Class_ClassLevel_Info($ParStudentID)
	{
		$this->Load_Batch_Student_Class_ClassLevel_Info();
		$resultSet[] = $this->Student_Class_ClassLevel_Info[$ParStudentID];
//		$sql = "SELECT
//						yc.YearID as ClassLevelID,
//						y.YearName as ClassLevelName,
//						yc.YearClassID as ClassID,
//						yc.ClassTitleEN as ClassName,
//						yc.ClassTitleB5 as ClassNameCh,
//						ycu.ClassNumber
//				FROM
//						YEAR_CLASS_USER as ycu
//						INNER JOIN
//						YEAR_CLASS as yc 
//						ON (ycu.YearClassID = yc.YearClassID AND ycu.UserID = '$ParStudentID' AND yc.AcademicYearID = '".$this->schoolYearID."')
//						INNER JOIN
//						YEAR as y
//						ON (yc.YearID = y.YearID)
//				";
//		$resultSet = $this->returnArray($sql);
		
		return $resultSet;
	}
	
	function Get_Student_House_Info($ParStudentID)
	{
		$this->Load_Batch_Student_House_Info();
		$resultSet[] = $this->PreloadInfoArr['Student_House_Info'][$ParStudentID];
		return $resultSet;
	}
	
	function Get_Student_Class_Teacher_Info($ParStudentID)
	{
		$name_field = getNameFieldByLang('iu.');
		$sql = "Select
						iu.EnglishName,
						iu.ChineseName,
						iu.TitleEnglish,
						iu.TitleChinese
				From
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join
						YEAR_CLASS_TEACHER as yct On (yc.YearClassID = yct.YearClassID)
						Inner Join
						INTRANET_USER as iu On (yct.UserID = iu.UserID)
				Where
						ycu.UserID = '".$ParStudentID."'
						And yc.AcademicYearID = '".$this->schoolYearID."'
				";
		return $this->returnArray($sql);
	}
	//aaaa
	function Get_Report_Selection($ClassLevelID, $ReportID, $ID_Name, $ParOnchange='', $ForVerification=0, $ForSubmission=0, $HideNonGenerated=0, $OtherTags='', $ForGradingSchemeSettings=0, $excludeReportID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
		$lreportcard_ui = new libreportcard_ui();
		
		# Preset the selected report as the current submission/verification report template if not specified
		if ($ReportID == '' && $ForGradingSchemeSettings!=1)
		{
			$Period_Conds = '';
			if ($ForVerification)
				$Period_Conds = ' AND NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ';
			else if ($ForSubmission)
				$Period_Conds = ' AND NOW() BETWEEN MarksheetSubmissionStart AND MarksheetSubmissionEnd ';
			
			$conds = " 	ClassLevelID = '$ClassLevelID' 
						$Period_Conds
					";
			$ReportInfoArr = $this->returnReportTemplateBasicInfo('', $conds);
			$ReportID = ($ReportInfoArr['ReportID'])? $ReportInfoArr['ReportID'] : '';
		}
	
		if($ForGradingSchemeSettings==1)
		{
			$all=1;
			$noFirst=0;
			$firstTitle = $eReportCard['AllReportCards'];
		}	
		else
		{
			$all=0;
			$noFirst=1;
		}
		# Get Report List of the ClassLevel
		$ReportInfoArr = $this->Get_Report_List($ClassLevelID);
		$numOfReport = count($ReportInfoArr);
		
		$selectionArr = array();
		for ($i=0; $i<$numOfReport; $i++)
		{
			if ($HideNonGenerated==1&&($ReportInfoArr[$i]["LastGenerated"] == "0000-00-00 00:00:00" || $ReportInfoArr[$i]["LastGenerated"] == "") || in_array($ReportInfoArr[$i]['ReportID'],(array)$excludeReportID))
				continue;
			$thisReportID = $ReportInfoArr[$i]['ReportID'];
			$thisReportTitle = trim(str_replace(':_:', ' ', $ReportInfoArr[$i]['ReportTitle']));
			
			$ReportType = $lreportcard_ui->Get_Report_Card_Type_Display($thisReportID, $ForSelection=1);
			
			$selectionArr[$thisReportID] = $thisReportTitle.' ('.$ReportType.')';
		}
		
		$onchange = '';
		if ($ParOnchange != "")
			$onchange = 'onchange="'.$ParOnchange.'"';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$OtherTags;
			
		$reportSelection = getSelectByAssoArray($selectionArr, $selectionTags, $ReportID, $all, $noFirst, $firstTitle);
		
		return $reportSelection;
	}
	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	/*function Get_Subject_Selection($ClassLevelID, $SubjectID, $ID_Name, $ParOnchange, $isMultiple=0, $includeComponent=0, $InputMarkOnly=0, $includeGrandMarks=0, $otherTagInfo='', $ReportID='')
	{
		global $eReportCard;
		
		$SubjectArr = $this->returnSubjectwOrder($ClassLevelID, 1);
		$numOfSubject = count($SubjectArr);
		
		$selectionArr = array();
		if ($numOfSubject > 0)
		{
			foreach ($SubjectArr as $thisSubjectID => $thisSubjectArr)
			{
				$thisMainSubjectID = $thisSubjectID;
				
				if ($includeComponent)
				{
					foreach ($thisSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
					{
						if ($thisCmpSubjectID == 0)
						{
							$thisTargetSubjectID = $thisMainSubjectID;
							$thisTargetSubjectName = $thisCmpSubjectName;
						}
						else
						{
							$thisTargetSubjectID = $thisCmpSubjectID;
							$thisTargetSubjectName = '&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCmpSubjectName;
						}
						
						$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisTargetSubjectID, $withGrandResult=0, $returnAsso=0, $ReportID);
						$ScaleInput = $GradingSchemeInfoArr['ScaleInput'];
						
						if ($InputMarkOnly==0 || ($InputMarkOnly==1 && $ScaleInput=='M'))
								$selectionArr[$thisTargetSubjectID] = $thisTargetSubjectName;
					}
				}
				else
				{
					$selectionArr[$thisMainSubjectID] = $thisSubjectArr[0];
				}
			}
		}
		
		if($includeGrandMarks==1)
		{
			$GrandMarkArr = $this->Get_SubjectID_Grand_Field_Arr();
			foreach((array)$GrandMarkArr as $GrandFieldID => $GrandField)
				$selectionArr[$GrandFieldID] = $eReportCard[$GrandField];
		}
			
		
		$onchange = '';
		if ($ParOnchange != "")
			$onchange = 'onchange="'.$ParOnchange.'"';
			
		$style = '';
		if ($isMultiple)
			$style = ' multiple="true" size="10"';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$style.' '.$otherTagInfo;
		
		$subjectSelection = getSelectByAssoArray($selectionArr, $selectionTags, $SubjectID, $all=0, $noFirst=1);
		
		return $subjectSelection;
	}*/
	
	function Get_Subject_Selection($ClassLevelID, $SubjectID, $ID_Name, $ParOnchange, $isMultiple=0, $includeComponent=0, $InputMarkOnly=0, $includeGrandMarks=0, $otherTagInfo='', $ReportID='', $ParentSubjectAsOptGroup=0, $ExcludeWithoutSubjectGroup=0, $TeachingOnly=0, $ExcludeSubjectIDArr='', $ParDefault='')
	{
		global $eReportCard, $linterface;
		
		if(!$linterface)
		{
			include_once("libinterface.php");
			$linterface = new interface_html();
		}
		
		$TeacherID = $TeachingOnly?$_SESSION['UserID']:'';
		
		$SubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=1, $TeacherID, $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		$numOfSubject = count($SubjectArr);
		
		if($ExcludeWithoutSubjectGroup==1)
		{
			$SubjectWithSubjectGroup = $this->Get_Subject_With_Subject_Group($ReportID);
		}
		
		if($includeComponent)
		{
			$GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=0, $ReportID);
			$GradingSchemeInfoArr = BuildMultiKeyAssoc($GradingSchemeInfoArr,"SubjectID");
		}
		
		$selectionArr = array();
		if ($numOfSubject > 0)
		{
			foreach ($SubjectArr as $thisSubjectID => $thisSubjectArr)
			{
				if($ExcludeWithoutSubjectGroup==1 && !in_array($thisSubjectID,$SubjectWithSubjectGroup)) {
					continue;
				}
				
				if ($ExcludeSubjectIDArr != '' && in_array($thisSubjectID, (array)$ExcludeSubjectIDArr)) {
					continue;
				}
				
				$thisMainSubjectID = $thisSubjectID;
				
				if ($includeComponent && count($thisSubjectArr)>1)
				{
					if($ParentSubjectAsOptGroup)
					{
						$ParentSubjectID = $thisMainSubjectID;
						$ParentSubjectName =  $thisSubjectArr[0];
					}
					foreach ($thisSubjectArr as $thisCmpSubjectID => $thisCmpSubjectName)
					{
						if ($thisCmpSubjectID == 0)
						{
							$thisTargetSubjectID = $thisMainSubjectID;
							$thisTargetSubjectName = $thisCmpSubjectName;
						}
						else
						{
							$thisTargetSubjectID = $thisCmpSubjectID;
							$thisTargetSubjectName = '&nbsp;&nbsp;&nbsp;&nbsp;'.$thisCmpSubjectName;
						}
						
						$thisGradingSchemeInfoArr = $GradingSchemeInfoArr[$thisTargetSubjectID];
						$ScaleInput = $thisGradingSchemeInfoArr['ScaleInput'];
						
						if (($InputMarkOnly==0 || ($InputMarkOnly==1 && $ScaleInput=='M')) && (!$ParentSubjectAsOptGroup || $thisCmpSubjectID!=0))
								$selectionArr[] = array($thisTargetSubjectID,$thisTargetSubjectName,$ParentSubjectName);
					}
				}
				else
				{
					$selectionArr[] = array($thisMainSubjectID,$thisSubjectArr[0]);
				}
			}
		}
		
		if($includeGrandMarks==1)
		{
			$GrandMarkArr = $this->Get_SubjectID_Grand_Field_Arr();
			foreach((array)$GrandMarkArr as $GrandFieldID => $GrandField)
				$selectionArr[] = array($GrandFieldID,$eReportCard[$GrandField]);
		}
			
		
		$onchange = '';
		if ($ParOnchange != "")
			$onchange = 'onchange="'.$ParOnchange.'"';
			
		$style = '';
		if ($isMultiple)
			$style = ' multiple="true" size="10"';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$style.' '.$otherTagInfo;
		
		$subjectSelection =  $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($selectionArr, $selectionTags, $ParDefault, $SubjectID);
		
		return $subjectSelection;
	}
	
	function Is_Teaching_Subject_Group($ParUserID, $SubjectGroupID)
	{
		$sql = "SELECT
						SubjectClassTeacherID
				FROM
						SUBJECT_TERM_CLASS_TEACHER
				WHERE
						UserID = '$ParUserID' AND SubjectGroupID = '$SubjectGroupID'
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	
	function Has_Class_Student_Enrolled_In_Subject_Group($YearClassID, $SubjectGroupID)
	{
		$sql = "SELECT
						ycu.YearClassUserID
				FROM
						YEAR_CLASS_USER as ycu
						INNER JOIN
						SUBJECT_TERM_CLASS_USER as stcu
						ON (ycu.UserID = stcu.UserID AND ycu.YearClassID = '$YearClassID' AND stcu.SubjectGroupID = '$SubjectGroupID')
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	
	function Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $ParUserID, $YearClassID, $SubjectID='')
	{
		$ColumnArr = $this->returnReportTemplateColumnData($ReportID);
		$numOfColumn = count($ColumnArr);
		
		$YearTermArr = array();
		for ($i=0; $i<$numOfColumn; $i++)
		{
			$thisYearTermID = $ColumnArr[$i]['SemesterNum'];
			$YearTermArr[] = $thisYearTermID;
		}
		$YearTermList = implode(',', $YearTermArr);
		
		$cond_Subject = '';
		if ($SubjectID != '')
			$cond_Subject = " AND st.SubjectID = '$SubjectID' ";
		
		$sql = "SELECT
						DISTINCT(ycu.UserID)
				FROM
						YEAR_CLASS_USER as ycu
						INNER JOIN
						SUBJECT_TERM_CLASS_USER as stcu
						ON (ycu.UserID = stcu.UserID AND ycu.YearClassID = '$YearClassID')
						INNER JOIN
						SUBJECT_TERM_CLASS_TEACHER as stct
						ON (stcu.SubjectGroupID = stct.SubjectGroupID AND stct.UserID = '$ParUserID')
						INNER JOIN
						SUBJECT_TERM as st
						ON ( stcu.SubjectGroupID = st.SubjectGroupID AND st.YearTermID IN ($YearTermList) $cond_Subject)
				";
		$resultSet = $this->returnVector($sql);
		
		return $resultSet;
	}
	
	function Is_Class_Teacher($ParUserID, $YearClassID='')
	{
		$Class_Cond = '';
		if ($YearClassID != '')
			$Class_Cond = " AND yc.YearClassID = '$YearClassID' ";
			
		$sql = "SELECT
						DISTINCT(yct.YearClassID)
				FROM
						YEAR_CLASS_TEACHER as yct
						INNER JOIN
						YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."')
				WHERE
						yct.UserID = '$ParUserID'
						$Class_Cond
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	
	function Is_Subject_Teacher($ParUserID)
	{
		$sql = "SELECT
						DISTINCT(stc.SubjectGroupID)
				FROM
						SUBJECT_TERM_CLASS_TEACHER as stct
						INNER JOIN
						SUBJECT_TERM_CLASS as stc
						ON (stct.SubjectGroupID = stc.SubjectGroupID AND stct.UserID = '$ParUserID')
						INNER JOIN
						SUBJECT_TERM as st ON (stc.SubjectGroupID = st.SubjectGroupID)
						INNER JOIN
						ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID AND ayt.AcademicYearID = '".$this->schoolYearID."')
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	
	function Get_Student_Studying_SubjectID($ReportID, $StudentID='', $includeComponent=1, $ClassID='')
	{
		$ReportBasicInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportBasicInfoArr['Semester'];
		$ReportType = ($SemID=='F')? 'W' : 'T';
		
		$YearTermIDList = '';
		if ($ReportType == 'W')
		{
			# Consolidated Report => Build the related Term List
			$ColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
			$numOfColumn = count($ColumnInfoArr);
			
			$YearTermIDArr = array();
			for ($i=0; $i<$numOfColumn; $i++)
			{
				$YearTermIDArr[] = $ColumnInfoArr[$i]['SemesterNum'];
			}
			$YearTermIDList = implode(',', $YearTermIDArr);
		}
		else
		{
			# Term Report => One Term Only
			$YearTermIDList = $SemID;
		}
		
		$cond_student = '';
		if ($StudentID != '')
			$cond_student = " AND stcu.UserID = '$StudentID' ";
		
		$sql = "SELECT
						DISTINCT(st.SubjectID)
				FROM
						SUBJECT_TERM_CLASS_USER as stcu
						INNER JOIN
						SUBJECT_TERM as st
						ON (stcu.SubjectGroupID = st.SubjectGroupID 
							AND st.YearTermID IN ($YearTermIDList)
							$cond_student)
				";
		if ($ClassID != '')
		{
			$sql .= "	INNER JOIN
						YEAR_CLASS_USER as ycu
						ON (stcu.UserID = ycu.UserID AND ycu.YearClassID = '$ClassID')
					";
		}
		$SubjectIDArr = $this->returnVector($sql);
		
		if (!$includeComponent)
		{
			return $SubjectIDArr;
		}
		else
		{
			include_once('subject_class_mapping.php');
			$numOfSubject = count($SubjectIDArr);
			$SubjectIDArr_WithComponent = array();
			for ($i=0; $i<$numOfSubject; $i++)
			{
				$thisSubjectID = $SubjectIDArr[$i];
				$thisSubjectObj = new subject($thisSubjectID);
				$SubjectIDArr_WithComponent[] = $thisSubjectID;
				
				$thisComponentArr = $thisSubjectObj->Get_Subject_Component();
				$numOfComponent = count($thisComponentArr);
				for ($j=0; $j<$numOfComponent; $j++)
				{
					$SubjectIDArr_WithComponent[] = $thisComponentArr[$j]['RecordID'];
				}
			}
			
			return $SubjectIDArr_WithComponent;
		}
	}
	
	function Get_Student_Info_By_ClassName_ClassNumber($ClassName, $ClassNumber)
	{
		$sql = "SELECT
						ycu.UserID, yc.ClassTitleEN as ClassName, ycu.ClassNumber, u.Gender, ycu.YearClassID
				FROM
						YEAR_CLASS_USER as ycu
						INNER JOIN
						YEAR_CLASS as yc
						LEFT JOIN
						INTRANET_USER as u ON ycu.UserID = u.UserID
				WHERE
						ycu.YearClassID = yc.YearClassID 
						AND 
						yc.AcademicYearID = '".$this->schoolYearID."'
						AND
						yc.ClassTitleEN = '".$this->Get_Safe_Sql_Query($ClassName)."'
						AND
						ycu.ClassNumber = '".$this->Get_Safe_Sql_Query($ClassNumber)."'
				";
		$resultSet = $this->returnArray($sql);
		
		return $resultSet;
	}
	
	function Get_Subject_Group_List_Of_Report($ReportID, $ClassID='', $SubjectID='', $IncludeSubject=false)
	{
		$BasicInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$YearTermID = $BasicInfoArr['Semester'];
		$ClassLevelID = $BasicInfoArr['ClassLevelID'];
		
		# search by form if not specified the class
		$cond_class = '';
		if ($ClassID == '')
			$cond_year = " AND stcyr.YearID = '$ClassLevelID' ";
		else
			$cond_class = " AND yc.YearClassID = '$ClassID' ";
			
		$cond_subject = '';
		if ($SubjectID != '')
			$cond_subject = " AND st.SubjectID = '$SubjectID' ";
		
		if($YearTermID != 'F')
			$cond_YearTermID = " AND st.YearTermID = '$YearTermID' ";
		
		# get the subject group list of the class in this report
		$sql = "SELECT 
						DISTINCT(stcu.SubjectGroupID) as SubjectGroupID,
						s.RecordID as SubjectID
				FROM
						YEAR_CLASS as yc
						INNER JOIN YEAR_CLASS_USER as ycu ON (yc.YearClassID = ycu.YearClassID  $cond_class)
						INNER JOIN SUBJECT_TERM_CLASS_USER as stcu ON (ycu.UserID = stcu.UserID)
						INNER JOIN SUBJECT_TERM as st ON (stcu.SubjectGroupID = st.SubjectGroupID  $cond_YearTermID $cond_subject)
						INNER JOIN SUBJECT_TERM_CLASS as stc ON (st.SubjectGroupID = stc.SubjectGroupID)
						INNER JOIN ACADEMIC_YEAR_TERM ayt ON (ayt.YearTermID = st.YearTermID AND ayt.AcademicYearID = ".$this->schoolYearID.")
						INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (st.SubjectGroupID = stcyr.SubjectGroupID $cond_year)
						Inner Join ASSESSMENT_SUBJECT as s On (st.SubjectID = s.RecordID)
						Left Outer Join	LEARNING_CATEGORY as lc On (s.LearningCategoryID = lc.LearningCategoryID)
				Order By
						lc.DisplayOrder, s.DisplayOrder, stc.ClassTitleEN
				";
		$ResultArr = $this->returnResultSet($sql);
		
		$ReturnArr = array();
		if ($IncludeSubject) {
			$ReturnArr = BuildMultiKeyAssoc($ResultArr, 'SubjectID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		}
		else {
			$ReturnArr = Get_Array_By_Key($ResultArr, 'SubjectGroupID');
		}
		
		return $ReturnArr;
	}
	
	function Get_Subject_With_Subject_Group($ReportID)
	{
		$BasicInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$YearTermID = $BasicInfoArr['Semester'];
		$ClassLevelID = $BasicInfoArr['ClassLevelID'];
		
		if($YearTermID != 'F')
			$cond_YearTermID = " AND st.YearTermID = '$YearTermID' ";
		
		# get the subject group list of the class in this report
		$sql = "SELECT 
						DISTINCT(st.SubjectID)
				FROM
						YEAR_CLASS as yc
						INNER JOIN
						YEAR_CLASS_USER as ycu
						ON (yc.YearClassID = ycu.YearClassID )
						INNER JOIN
						SUBJECT_TERM_CLASS_USER as stcu
						ON (ycu.UserID = stcu.UserID)
						INNER JOIN
						SUBJECT_TERM as st
						ON (stcu.SubjectGroupID = st.SubjectGroupID  $cond_YearTermID )
						INNER JOIN 
						ACADEMIC_YEAR_TERM ayt
						ON ayt.YearTermID = st.YearTermID AND ayt.AcademicYearID = ".$this->schoolYearID." 
				";
		$SubjectIDArr = $this->returnVector($sql);
		
		return $SubjectIDArr;
	}
	
	function Get_Report_Title($ParReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		$sql = "
			SELECT 
				ReportTitle
			FROM
				$table
			WHERE
				ReportID = '$ParReportID'
		";
		
		$temp = $this->returnVector($sql);

		return $temp[0];
	}
	
	
	############## Marksheet Verification ###############
	function Get_Marksheet_Feedback($ReportID, $SubjectID='', $StudentID='')
	{
		$cond_subject = '';
		if ($SubjectID != '')
			$cond_subject = " AND SubjectID IN (".implode(",",(array)$SubjectID).") ";
			
		$cond_studentid = '';
		if ($StudentID != '')
			$cond_studentid = " AND StudentID IN (".implode(",",(array)$StudentID).") ";
			
		$table = $this->DBName.".RC_MARKSHEET_FEEDBACK";
		$sql = "	SELECT
							*
					FROM
							$table
					WHERE
							ReportID = '".$ReportID."'
							$cond_subject
							$cond_studentid
				";
		$temp = $this->returnArray($sql);
		
		$returnArr = BuildMultiKeyAssoc($temp, array("StudentID","SubjectID"));
		return $returnArr;
	}
	
	// 20110408
	function Get_Marksheet_Feedback_Info($ReportID, $SubjectID='', $StudentID='')
	{
		$cond_subject = '';
		if ($SubjectID != '')
			$cond_subject = " AND a.SubjectID = '".$SubjectID."' ";
			
		$cond_studentid = '';
		if ($StudentID != '')
			$cond_studentid = " AND a.StudentID IN (".implode(",",(array)$StudentID).") ";
		$name = getNameFieldByLang2("b.");
			
		$table = $this->DBName.".RC_MARKSHEET_FEEDBACK";
		$sql = "	SELECT
							a.DateModified as DateModified,
							$name as LastModifiedBy,
							a.RecordStatus,
							a.StudentID,
							a.SubjectID
					FROM
							$table as a
							INNER JOIN INTRANET_USER as b on a.LastModifiedBy = b.UserID
					WHERE
							a.ReportID = '".$ReportID."'
							$cond_subject
							$cond_studentid

				";
				
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
		
	}
	
	function INSERT_FEEDBACK($ReportID,$SubjectID,$StudentID,$Comment,$FeedbackID='')
	{
		global $PATH_WRT_ROOT; 
		
		include $PATH_WRT_ROOT."includes/libuser.php";
		$UserID = $_SESSION['UserID'];
		$luser = new libuser($UserID);
		$CurrentTime = Date("Y-m-d h:i");
		
		# build comment
		$Comment = wordwrap2($Comment,60);
		
		$built_comment = "	<table>
								<tr>
									<td width=20% nowrap>
									".$luser->UserName().":
									</td>
									<td>
											($CurrentTime)
									</td>
								</tr>
								<tr>
									<td colspan=2>
										". htmlspecialchars($Comment)."
									</td>
								</tr>
							</table>";
		
		$table = $this->DBName.".RC_MARKSHEET_FEEDBACK"; 
		if(!$FeedbackID)
		{
		
			$sql = "	INSERT INTO 
							$table
								(`SubjectID`,`ReportID`,`StudentID`,`Comment`,`LastModifiedBy`,`RecordStatus`,`DateInput`,`DateModified`)
							VALUES
								('$SubjectID','$ReportID','$StudentID','$built_comment','$UserID',2,NOW(),NOW())				
							";
		}
		else
		{
			
			$sql = "	UPDATE $table
							SET
								Comment = IF(Comment IS NULL, '$built_comment',CONCAT(Comment,'<hr>$built_comment')),
								DateModified = NOW(),
								LastModifiedBy = '$UserID',
								RecordStatus = '2'
							WHERE
								FeedbackID = '$FeedbackID'						
							";
			
		}

		
		$Success = 	$this->db_db_query($sql) or die($sql." ".mysql_error());

		return 	$Success;
	
	} 
	
	// 20110408
	function Fetch_Feedback_Detail($FeedBack)
	{
		$tagname = "td";
		$pattern = "/<$tagname.*>\s*(.*?)\s*<\/$tagname>/";
	    preg_match_all($pattern, $FeedBack, $matches);
	    
	    $temp = $matches[1];
	    for($i=0 ; $i<sizeof($temp); $i+=3)
	    {
	    	$FeedbackArr['Name'] = $temp[$i+0];
	    	$FeedbackArr['DateInput'] = $temp[$i+1];
	    	$FeedbackArr['Comment'] = $temp[$i+2]; 
	    	$ReturnArr[] = $FeedbackArr;
	    }
	    
		return $ReturnArr;
	}
	
//	function Get_All_Marksheet_Feedback_Of_Subject_Group($ReportID='', $ClassLevelID='', $SemID='', $SubjectID='')
//	{
//		if(trim($ReportID)!='')
//			$cond_ReportID = " AND mf.ReportID = $ReportID ";
//		if(trim($ClassLevelID)!='')
//			$cond_ClassLevelID = " AND stcyr.YearID = $ClassLevelID ";
//		if(trim($SemID)!='')
//			$cond_SemID = " AND st.YearTermID = $SemID ";
//		if(trim($SubjectID)!='')
//			$cond_SubjectID = " AND st.SubjectID = $SubjectID ";
//			
//		$RC_MARKSHEET_FEEDBACK = $this->DBName.".RC_MARKSHEET_FEEDBACK";
//		
//		$sql = "
//			SELECT
//				DISTINCT (stcu.UserID),
//				stcu.SubjectGroupID,
//				mf.*
//			FROM
//				SUBJECT_TERM as st
//				INNER JOIN SUBJECT_TERM_CLASS_USER as stcu	On (stcu.SubjectGroupID = st.SubjectGroupID)
//				INNER JOIN SUBJECT_TERM_CLASS as stc On (st.SubjectGroupID = stc.SubjectGroupID)
//				INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON stcyr.SubjectGroupID = stc.SubjectGroupID
//				INNER JOIN INTRANET_USER as u ON (stcu.UserID = u.UserID)
//				Inner Join
//				YEAR_CLASS_USER as ycu ON (stcu.UserID = ycu.UserID)
//				Inner Join
//				YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->schoolYearID."' $classLevel_cond )
//				LEFT JOIN $RC_MARKSHEET_FEEDBACK mf ON mf.SubjectID = st.SubjectID AND stcu.UserID = mf.StudentID $cond_ReportID
//			WHERE 
//				1
//				$cond_SubjectID
//				$cond_ClassLevelID
//				$cond_SemID
//		";
//		
//		$result = $this->returnArray($sql);
//		
//		return $result;  
//	}
	########### End of marksheet Verification ###########
	
	

	########### For Manual Adjustment ##############
	
	# Get records of manual adjustment
	#****Param****: for Grand Marks or Ranking, $SubjectID =0; for General Marks of each subject, $OtherInfoName='Score'
	# output: $returnAry[$StudentID][$ReportColumnID][$SubjectID][$OtherInfoName] = $Score
	
	function Get_Manual_Adjustment($ReportID, $StudentID='', $ReportColumnID='', $SubjectID='', $OtherInfoName='')
	{
		$FuncArgArr = get_defined_vars();
		$PreloadArrKey = 'ManualAdjustmentArr';
		$returnAry = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnAry !== false) {
    		return $returnAry;
    	}

		$cond = !empty($StudentID)?" AND StudentID = '$StudentID' ":"";
		$cond .= !empty($SubjectID)||$SubjectID===0?" AND SubjectID = '$SubjectID' ":"";
		$cond .= !empty($OtherInfoName)?" AND OtherInfoName = '$OtherInfoName' ":"";
		$cond .= !empty($ReportColumnID)||$ReportColumnID===0?" AND ReportColumnID = '$ReportColumnID' ":"";
		
		$table = $this->DBName.".RC_MANUAL_ADJUSTMENT";
		
		$sql = "
			SELECT 
				StudentID, 
				ReportColumnID, 
				SubjectID ,
				OtherInfoName,
				AdjustedValue
			FROM 
				$table
			WHERE
				ReportID = '$ReportID'
				$cond

		";

		$temp = $this->returnArray($sql);
		
		$returnAry = array();
		for($i=0; $i<count($temp);$i++)
		{
			list($StudentID,$ReportColumnID,$SubjectID,$OtherInfoName,$AdjustedValue) = $temp[$i];
			$returnAry[$StudentID][$ReportColumnID][$SubjectID][$OtherInfoName]=$AdjustedValue;
		}
		
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnAry);
		return $returnAry;
	} 
	
	function retrieveLastManualAdjustDate($ReportID, $StudentID='')
	{
		$x = array();
		$table = $this->DBName.".RC_MANUAL_ADJUSTMENT AS rma";
		
		if($StudentID)	$cond .= " AND StudentID=$StudentID";
		
		$sql = "
			SELECT
				StudentID,  
				MAX(LastModified) AS LastModified, 
				u.ChineseName, 
				u.EnglishName  
			FROM 
				$table 
				INNER JOIN INTRANET_USER u ON rma.LastModifiedBy = u.UserID
			WHERE 
				ReportID=$ReportID
				$cond
			GROUP BY 
				ReportID , StudentID
			ORDER BY LastModified DESC
			
		";
		
		$result = $this->returnArray($sql);
		
		foreach($result as $key=>$data)
			$x[$data['StudentID']] = array( $data['LastModified'],Get_Lang_Selection($data['ChineseName'],$data['EnglishName']));
		return $x;
	}
	
	function Delete_Manual_Adjustment($ReportID)
	{
		$table = $this->DBName.".RC_MANUAL_ADJUSTMENT";
		
		$sql = "Delete From ".$table." Where ReportID = '".$ReportID."'";
		$success = $this->db_db_query($sql);
	}
	
	
	########### End of marksheet Verification ###########
	
	
	############## Report Card Archive ###############
	
	function Archive_CSS_File($SourceCSSPath, $TargetCSSPath)
	{
		global $PATH_WRT_ROOT;
		
		# Create css archive folder
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		$libfilesystem = new libfilesystem();

		$ArchiveFolderPath = $PATH_WRT_ROOT."file/reportcard2008/".$this->schoolYear;
		if (!file_exists($ArchiveFolderPath))
			$libfilesystem->folder_new($ArchiveFolderPath);
		$ArchiveFolderPath .= "/archive_css";
		$libfilesystem->folder_new($ArchiveFolderPath);
		
		# archive css file
		// rename the old css first if exist and is different from the new old
		if (file_exists($TargetCSSPath))
		{
			// Compare css files
			$SourceCSSContent = strtoupper(dechex(crc32(file_get_contents($SourceCSSPath))));
			$TargetCSSContent = strtoupper(dechex(crc32(file_get_contents($TargetCSSPath))));
			
			if ($SourceCSSContent != $TargetCSSContent)
			{
				// files not the same
				$thisDateTime = date('Y_m_d_H_i_s');
				$NewFileName = $TargetCSSPath."_".$thisDateTime;
				$successArr['Rename'] = $libfilesystem->file_rename($TargetCSSPath, $NewFileName);
			}
		}
		$successArr['Copy'] = $libfilesystem->file_copy($SourceCSSPath, $TargetCSSPath);
		
		if (in_array(false, $successArr))
			return false;
		else
			return true;
	}
	
	function Archive_Report_Card($ReportID, $StudentID, $ReportHTML)
	{
		# Get Semester
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$Semester = $ReportInfoArr['Semester'];
		$YearID = $ReportInfoArr['ClassLevelID'];
		
		# Get Student Form Class Info
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		$YearClassID = $StudentInfoArr[0]['ClassID'];
		
		$table = $this->DBName.".RC_REPORT_CARD_ARCHIVE";
		
		# delete old record
		$sql = "DELETE FROM ".$table." Where ReportID = '".$ReportID."' AND StudentID = '".$StudentID."'";
		$SuccessArr['Delete'] = $this->db_db_query($sql);
		
		# insert report card
		$sql = "INSERT INTO ".$table."
					(ReportID, Semester, YearID, YearClassID, StudentID, ReportCardHTML, DateInput, LastModifiedBy)
				VALUES
					(	'".$ReportID."', 
						'".$Semester."',
						'".$YearID."',
						'".$YearClassID."',
						'".$StudentID."',
						'".$this->Get_Safe_Sql_Query($ReportHTML)."',
						now(),
						'".$_SESSION['UserID']."'
					)
				";
		$SuccessArr['Insert'] = $this->db_db_query($sql);
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	
	function Get_Archived_Report($ReportID, $StudentID='')
	{
		$table = $this->DBName.".RC_REPORT_CARD_ARCHIVE";
		
		$cond_StudentID = '';
		if ($StudentID != '')
			$cond_StudentID = " AND StudentID = '".$StudentID."' ";
			
		$sql = "SELECT
						ReportCardHTML
				FROM
						$table
				WHERE
						ReportID = '".$ReportID."'
						$cond_StudentID
				";
		$ReportHTMLArr = $this->returnVector($sql);
		
		return $ReportHTMLArr;
	}
	
	function Delete_Archived_Report($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_CARD_ARCHIVE";
		
		$sql = "Delete From ".$table." Where ReportID = '".$ReportID."'";
		$success = $this->db_db_query($sql);
	}
	
	############## End Of Report Card Archive ###############
		
	function getCorespondingTermReportID($ReportID, $ReportColumnID, $MainReportOnly=0)
	{
		/* modified by Ivan on 2010-05-13: use the function Get_Related_TermReport_Of_Consolidated_Report to get the term report info
		$sql = "
			SELECT
				a.SemesterNum , b.ClassLevelID 
			FROM 
				".$this->DBName.".RC_REPORT_TEMPLATE_COLUMN a
				LEFT JOIN ".$this->DBName.".RC_REPORT_TEMPLATE b ON a.ReportID = b.ReportID
			WHERE
				a.ReportColumnID = '$ReportColumnID' AND a.ReportID = '$ReportID'";
			
			$TermData = $this->returnArray($sql);
			list($TermSem, $TermForm) = $TermData[0];

			$sql = "
				SELECT 
					ReportID
				FROM
					 ".$this->DBName.".RC_REPORT_TEMPLATE
				WHERE 
					Semester = '$TermSem'
				AND
					ClassLevelID = '$TermForm' 
				";
			
			$tmp = $this->returnVector($sql);
			
			return $tmp[0];
		*/
		$reportBasicInfo = $this->returnReportTemplateBasicInfo($ReportID);

		if ($reportBasicInfo['Semester'] != 'F')
		{
			return $ReportID;
		}
		else
		{
			$TermReportArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly, $ReportColumnID);
			return $TermReportArr[0]['ReportID'];
		}
	}
	
	function Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL")
	{
		$conds_MainReport = '';
		if ($MainReportOnly == 1)
			$conds_MainReport = " And TermReport.isMainReport = 1 ";
			
		$conds_ReportColumnID = '';
		if($ReportColumnID === 0 || $ReportColumnID === '')
		{
			$conds_ReportColumnID = " And (ReportColumn.ReportColumnID = 0 OR ReportColumn.ReportColumnID IS NULL) ";
		}
		else if($ReportColumnID!='' && $ReportColumnID!="ALL")
		{
			$conds_ReportColumnID = " And ReportColumn.ReportColumnID = '$ReportColumnID' ";
		}

		$tableReport = $this->DBName.".RC_REPORT_TEMPLATE ";
		$tableReportColumn = $this->DBName.".RC_REPORT_TEMPLATE_COLUMN ";
		
		$sql = "Select 
					TermReport.ReportID, TermReport.Semester, ReportColumn.ReportColumnID
				From
					$tableReport as ConReport
					Left Outer Join
					$tableReportColumn as ReportColumn
					On (ConReport.ReportID = ReportColumn.ReportID)
					Inner Join
					$tableReport as TermReport
					On (ReportColumn.SemesterNum = TermReport.Semester And ConReport.ClassLevelID = TermReport.ClassLevelID)
				Where
					ConReport.ReportID = '$ReportID'
					$conds_MainReport
					$conds_ReportColumnID
				Order By
					ReportColumn.DisplayOrder
				";
		return $this->returnArray($sql);	
	}
	
	function Get_Class_Max_Min_Info($ReportID, $ReportColumnID, $ClassID, $MarkField, $Order='asc', $includeAdjustedMarks=1)
	{
		# Get Class Student
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASS($ClassID);
		$numOfClassStudent = count($StudentInfoArr);
		if ($numOfClassStudent == 0)
			return false;
			
		$StudentIDArr = array();
		for ($i=0; $i<$numOfClassStudent; $i++)
			$StudentIDArr[] = $StudentInfoArr[$i]['UserID'];
		$StudentIDList = implode(',', $StudentIDArr);
		
		# Get Student Score
		$sql_conds = " And StudentID In (".$StudentIDList.") ";
		$ScoreInfoArr = $this->getReportResultScore($ReportID, $ReportColumnID, $StudentID='', $sql_conds, 0, $includeAdjustedMarks);
		$numOfScore = count($ScoreInfoArr);
		
		if ($numOfScore == 0)
			return false;
		
		# Consolidate Numeric Mark for sorting
		$targetScoreArr = array();
		foreach ($ScoreInfoArr as $thisStudentID => $thisScoreInfoArr)
		{
			$thisScore = $thisScoreInfoArr[$ReportColumnID][$MarkField];
			
			if (is_numeric($thisScore))
				$targetScoreArr[] = $thisScore;
		}
		
		# Sort Marks
		if ($Order == 'asc')
			sort($targetScoreArr);
		else
			rsort($targetScoreArr);
		
		return $targetScoreArr[0];
	}
	
	function Get_Score_Display_HTML($Score, $ReportID, $ClassLevelID, $SubjectID, $StyleDetermineScore='', $MarkType='', $StudentID='', $ReportColumnID='', $MarkNature='', $ConvertToGrade=0, $Rounding='', $ScaleDisplay='')
	{
		$ReportColumnID = ($ReportColumnID=='')? 0 : $ReportColumnID;
		$ScaleDisplay = ($ScaleDisplay=='')? 'M' : $ScaleDisplay;
		
		if ($MarkType == 'ClassPosition' || $MarkType == 'FormPosition')
		{
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$OverallPositionRangeClass 	= $ReportSetting['OverallPositionRangeClass'];
			$OverallPositionRangeForm 	= $ReportSetting['OverallPositionRangeForm'];
			
			$thisRange = ($MarkType == 'ClassPosition')? $OverallPositionRangeClass : $OverallPositionRangeForm;
			
			if ( ($Score == -1) || ($thisRange != 0 && $Score > $thisRange) || $Score == '')
				$thisScoreDisplay = $this->EmptySymbol;
			else
				$thisScoreDisplay = $Score;
		}
		else if ($MarkType == 'ClassNoOfStudent' || $MarkType == 'FormNoOfStudent')
		{
			if ($Score == -1 || $Score == '')
				$thisScoreDisplay = $this->EmptySymbol;
			else
				$thisScoreDisplay = $Score;
		}
		else if ($Score != '' && $Score != -1)
		{
			
			switch ($MarkType)
			{
				case 'GrandAverage':
					$SubjectID = '-1'; break;
				case 'GrandTotal':
					$SubjectID = '-2'; break;
				case 'GPA':
					$SubjectID = '-3'; break;
				default:
					$SubjectID = $SubjectID;
			} 
			
			if ($StyleDetermineScore == '')
				$StyleDetermineScore = $Score;
				
			$ConvertedToGrade = false;
			if ($MarkNature == '')
			{
				if ($StyleDetermineScore=='+')
					$thisNature = "Fail";
				else
				{
					# Modified by Marcus 20101011 (for template based grading scheme)
					$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1,0,$ReportID);
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					
					$SchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$TopPercentage = $SchemeInfoArr['TopPercentage'];

					if(!is_numeric($StyleDetermineScore))
					{
						$ConvertBy = "G";
					}
					else if($ScaleDisplay == "M" && $TopPercentage > 0 && $StudentID != '')
					{
						$StyleDetermineScore = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $Score, $ReportID, $StudentID, $SubjectID, $ClassLevelID, $ReportColumnID);
						$ConvertBy = "G";
						$ConvertedToGrade = true;
					}
					else
						$ConvertBy = "";
					
					$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $StyleDetermineScore, '', $ConvertBy,$ReportID);
					
				}
			}
			else
			{
				$thisNature = $MarkNature;
			}
			
			### Force the result to return grade 
			if ($ConvertToGrade == 1)
			{
				if ($ConvertedToGrade == true)
				{
					$Score = $StyleDetermineScore;
				}
				else
				{
					# Get grading scheme info if not get yet
					if ($SchemeID == '')
					{
						# Modified by Marcus 20101011 (for template based grading scheme)
						$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1,0,$ReportID);
						$SchemeID = $SubjectFormGradingSettings['SchemeID'];
						$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					}
					
					if ($ScaleDisplay == 'M')
					{
						
						$Score = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $Score, $ReportID, $StudentID, $SubjectID, $ClassLevelID, $ReportColumnID);
						
					}
				}
			}
			
			if ($Rounding!='' && is_numeric($Score) && $ScaleDisplay=='M')
				$Score = my_round($Score, $Rounding);
			
			if ($Score == '0')
				$thisNature = 'Fail';
			$thisScoreDisplay = $this->ReturnTextwithStyle($Score, 'HighLight', $thisNature);
			
			
		}
		else
		{
			$thisScoreDisplay = $this->EmptySymbol;
		}
		
		return $thisScoreDisplay;
	}
	
	function Get_Last_Semester_Of_Report($ReportID)
	{
		# retrieve the latest Term
		$latestTerm = "";
		$sems = $this->returnReportInvolvedSem($ReportID);
		foreach($sems as $TermID=>$TermName)
			$latestTerm = $TermID;
			
		return $latestTerm;
	}
	
	function Is_Student_In_Subject_Group_Of_Subject($ReportID, $StudentID, $SubjectID, $YearTermID='')
	{
		$resultSet = $this->Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $SubjectID, $StudentID, $YearTermID);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	
	function Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $SubjectID, $StudentID='', $YearTermIDArr='')
	{
		$PreloadArrKey = 'Get_Student_In_Current_Subject_Group_Of_Subject';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		if ($YearTermIDArr == '') {
			$YearTermIDArr = array();
			$sems = $this->returnReportInvolvedSem($ReportID);
			foreach($sems as $TermID=>$TermName)
				$YearTermIDArr[] = $TermID;
		}
		$YearTermList = implode(',', (array)$YearTermIDArr);
		
		$conds_studentID = '';
		if ($StudentID != '')
			$conds_studentID = " And stcu.UserID = '$StudentID' ";
			
		# Get Parent Subject if the Subject is a Component
//		$objSubject = new subject($SubjectID);
//		$isComponent = $objSubject->Is_Component_Subject();
//		if ($isComponent == true)
//		{
//			$objSubjectComponent = new Subject_Component($SubjectID);
//			$ParentSubjectInfo = $objSubjectComponent->Get_Parent_Subject();
//			$SubjectID = $ParentSubjectInfo['RecordID'];
//		}
		if ($this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID)) {
			$SubjectID = $this->GET_PARENT_SUBJECT_ID($SubjectID);
		} 
		
		$sql = "Select
						stcu.UserID
				From
						SUBJECT_TERM as st
						Inner Join
						SUBJECT_TERM_CLASS_USER as stcu
						On (stcu.SubjectGroupID = st.SubjectGroupID)
				Where
						st.SubjectID = '".$SubjectID."'
						And
						st.YearTermID In ($YearTermList)
						$conds_studentID
				";
		$resultSet = $this->returnVector($sql);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $resultSet);
		return $resultSet;
	}
	
	# add Nature By Marcus 20091204
	function Get_Scheme_Grade_Statistics_Info($ReportID, $SubjectID, $forSubjectGroup=0)
	{
		global $PATH_WRT_ROOT;
		
		$PreloadArrKey = 'SchemeGradeStatisticsInfoArr';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		
		$SchemeGradeStatInfoArr = array();
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Marks of the Subject
		# $MarkArr[$StudentID][$SubjectID][$ReportColumnID][Mark, Grade, etc...]
		$MarkArr = array();
		if ($SubjectID > 0)
		{
			# Normal Subject
			$MarkArr = $this->getMarks($ReportID, '', '', $ParentSubjectOnly=1, $includeAdjustedMarks=1, '', $SubjectID, 0);
			$MarkArr = $this->Convert_MarksArr_Mark_To_Grade($ReportID,$MarkArr);
			
		}
		else
		{
			# Grand Mark
			$MarkArrTemp = $this->getReportResultScore($ReportID);
			
			$AverageCompareField = $this->Get_Grand_Position_Determine_Field();
			foreach ($MarkArrTemp as $thisStudentID => $thisStudentMarkArr)
			{
				$MarkArr[$thisStudentID][$SubjectID][0]['Mark'] = $thisStudentMarkArr[0][$AverageCompareField];
			}
		}
		### Get Grading Scheme Info of the Subject
		# Modified by Marcus 20101011 (for template based grading scheme)
		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1, 0, $ReportID);
		$SchemeID = $SubjectFormGradingSettings['SchemeID'];
		$SchemeRangeInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
		$SchemeGradeArr = Get_Array_By_Key($SchemeRangeInfo, 'Grade');
		$numOfSchemeGrade = count($SchemeGradeArr);
		$SchemeNatureArr = Get_Array_By_Key($SchemeRangeInfo, 'Nature');
		if(!empty($SchemeGradeArr)&&!empty($SchemeNatureArr))
			$GradeNatureArr = array_combine($SchemeGradeArr,$SchemeNatureArr);
		
		### Get Student List
		if($forSubjectGroup == 0)
		{
			# Get Class List
			$YearClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
			$ClassGroupIDArr = Get_Array_By_Key($YearClassInfoArr, 'ClassID');
			$numOfClassGroup = count($ClassGroupIDArr);
			
			# Get Form Student List
			$FormStudentIDArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
			$FormStudentIDArr = Get_Array_By_Key($FormStudentIDArr, 'UserID');
			$numOfStudentInForm = count($FormStudentIDArr);
			
			
			$GroupStudentIDArr = array();
			for ($i=0; $i<$numOfClassGroup; $i++)
			{
				$thisYearClassID = $ClassGroupIDArr[$i];
				$thisClassStudentIDArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $thisYearClassID);
				$GroupStudentIDArr[$thisYearClassID] = Get_Array_By_Key($thisClassStudentIDArr, 'UserID');
			}
		}
		else
		{
			# Get Subj Group List
			$ClassGroupIDArr = $this->Get_Subject_Group_List_Of_Report($ReportID,'',$SubjectID);
			$numOfClassGroup = count($ClassGroupIDArr);
			
			# Get Subject Group Student List
			$GroupStudentIDArr = array();
			$totalNumOfStudent=0;
			foreach((array)$ClassGroupIDArr as $SubjectGroupID)
			{
				include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
				$scm = new subject_term_class($SubjectGroupID, 1, 1);
				//$GroupStudentIDArr[$SubjectGroupID]	=  $scm->ClassStudentList;
				$thisClassStudentList = array();
				foreach($scm->ClassStudentList as $StudentInfo)
				{
					if($StudentInfo['YearID'] == $ClassLevelID)
						$thisClassStudentList[] = $StudentInfo;
				}
				$GroupStudentIDArr[$SubjectGroupID] = Get_Array_By_Key($thisClassStudentList, 'UserID');
				$totalNumOfStudent += count($GroupStudentIDArr[$SubjectGroupID]);
			}
		}
		//StartTimer();
		
		### Get Form Grade Range
		# initialize mark info array
		$FormSchemeMarkArr = array();
		for ($i=0; $i<$numOfSchemeGrade; $i++)
		{
			$thisSchemeGrade = $SchemeGradeArr[$i];
			$FormSchemeMarkArr[$thisSchemeGrade] = array();
		}
		
		# Get the raw mark of each student in different grades
		for ($i=0; $i<$numOfStudentInForm; $i++)
		{
			$thisStudentID 	= $FormStudentIDArr[$i];
			$thisMarkRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['RawMark'];
			$thisGradeRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['Grade'];
			
//			if ($thisGradeRaw=="") {
//				$thisGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarkRaw, $ReportID, $thisStudentID, $SubjectID, $ClassLevelID, 0);
//			} else {
//				$thisGrade = $thisGradeRaw;
//			}
			$thisGrade = $thisGradeRaw;
			# exclude special grade
			if (in_array($thisGrade, $SchemeGradeArr))
			{
				$FormSchemeMarkArr[$thisGrade][] = $thisMarkRaw;
			}
			else
			{
				$FormSchemeMarkArr['SpecialCase'][] = $thisGrade;
			}
		}
		
		# Consolidate Form Mark Info in to the array
		$numOfSpecialMark = count($FormSchemeMarkArr['SpecialCase']);
		for ($i=0; $i<$numOfSchemeGrade; $i++)
		{
			$thisSchemeGrade = $SchemeGradeArr[$i];
			$thisSchemeMarkArr = $FormSchemeMarkArr[$thisSchemeGrade];
			
			$thisNumOfStudent = count($thisSchemeMarkArr);
			$thisTotalNumOfStudent = $forSubjectGroup!=1?$numOfStudentInForm:$totalNumOfStudent;
			$thisTotalNumOfStudent = $thisTotalNumOfStudent - $numOfSpecialMark;
			
			$thisPercentage = ($thisTotalNumOfStudent > 0)? ($thisNumOfStudent / $thisTotalNumOfStudent) * 100 : 0;
			$thisMin = ($thisNumOfStudent > 0)? min($thisSchemeMarkArr) : 0;
			$thisMax = ($thisNumOfStudent > 0)? max($thisSchemeMarkArr) : 0;
			
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['NumOfStudent'] = $thisNumOfStudent;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['PercentageRaw'] = $thisPercentage;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['Percentage'] = my_round($thisPercentage, 2);
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['MinMark'] = $thisMin;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['MaxMark'] = $thisMax;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['Nature'] = $GradeNatureArr[$thisSchemeGrade];
		}
		
		//hdebug("3".StopTimer());
		//StartTimer();
		
		### Get Class Grade Range
		for($i=0; $i<$numOfClassGroup; $i++)
		{
			$thisClassGroupID = $ClassGroupIDArr[$i];
			$thisClassStudentIDArr = $GroupStudentIDArr[$thisClassGroupID];
			$thisNumOfStudentInClass = count($thisClassStudentIDArr);
			
			# initialize mark info array
			$thisClassSchemeMarkArr = array();
			for ($j=0; $j<$numOfSchemeGrade; $j++)
			{
				$thisSchemeGrade = $SchemeGradeArr[$j];
				$thisClassSchemeMarkArr[$thisSchemeGrade] = array();
			}
			
			# Get the raw mark of each student in different grades
			for ($j=0; $j<$thisNumOfStudentInClass; $j++)
			{
				$thisStudentID 	= $thisClassStudentIDArr[$j];
				//debug("$thisStudentID,$SubjectID");
				$thisMarkRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['RawMark'];
				$thisGradeRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['Grade'];
				
//				if ($thisGradeRaw=="") {
//					$thisGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarkRaw, $ReportID, $thisStudentID, $SubjectID, $ClassLevelID, 0);
//				} else {
//					$thisGrade = $thisGradeRaw;
//				}
				$thisGrade = $thisGradeRaw;
				
				# exclude special grade
				if (in_array($thisGrade, $SchemeGradeArr))
				{
					$thisClassSchemeMarkArr[$thisGrade][] = $thisMarkRaw;
				}
				else
				{
					$thisClassSchemeMarkArr['SpecialCase'][] = $thisGrade;
				}
				//debug_pr($thisClassSchemeMarkArr);
			}
			
			# Consolidate Form Mark Info into an array
			$thisNumOfSpecialCaseMark = count($thisClassSchemeMarkArr['SpecialCase']);
			for ($j=0; $j<$numOfSchemeGrade; $j++)
			{
				$thisSchemeGrade = $SchemeGradeArr[$j];
				$thisSchemeMarkArr = $thisClassSchemeMarkArr[$thisSchemeGrade];
				
				$thisNumOfStudent = count($thisSchemeMarkArr);
				$thisNumOfCountMarkStudent = $thisNumOfStudentInClass - $thisNumOfSpecialCaseMark;
				$thisPercentage = ($thisNumOfCountMarkStudent > 0)? ($thisNumOfStudent / $thisNumOfCountMarkStudent) * 100 : 0;
				$thisMin = ($thisNumOfStudent > 0)? min($thisSchemeMarkArr) : 0;
				$thisMax = ($thisNumOfStudent > 0)? max($thisSchemeMarkArr) : 0;
				
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisClassGroupID]['NumOfStudent'] = $thisNumOfStudent;
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisClassGroupID]['PercentageRaw'] = $thisPercentage;
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisClassGroupID]['Percentage'] = my_round($thisPercentage, 2);
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisClassGroupID]['MinMark'] = $thisMin;
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisClassGroupID]['MaxMark'] = $thisMax;
			}
		}
		//hdebug("4".StopTimer());
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SchemeGradeStatInfoArr);
		
		return $SchemeGradeStatInfoArr;
	}
	
	function Get_Mark_Statistics_Info($ReportID, $SubjectIDArr, $Average_Rounding=2, $SD_Rounding=2, $Percentage_Rounding=2, $SubjectMarkRounding=0, $IncludeClassStat=1)
	{
		global $PATH_WRT_ROOT;
		
		$numOfSubject = count($SubjectIDArr);
		if ($numOfSubject==0)
			return array();
			
		### Get Form Student List
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormStudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=1, $isShowStyle=0);
		
		### Get excluded student list
		$excludeStudentArr = $this->GET_EXCLUDE_ORDER_STUDENTS();
		
		### Get Marks
		# Normal Subject
		$SubjectMarkArr = $this->getMarks($ReportID, '', '', $ParentSubjectOnly=0, $includeAdjustedMarks=1, 'Mark Desc', '', 0);
		$SubjectNatureArr = $this->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $SubjectMarkArr);
		
		# Grand Mark
		$MarkArrTemp = $this->getReportResultScore($ReportID);
		$AverageCompareField = $this->Get_Grand_Position_Determine_Field();
		$GrandSubjectID = $this->Get_Grand_Field_SubjectID($AverageCompareField);
		foreach ((array)$MarkArrTemp as $thisStudentID => $thisStudentMarkArr)
		{
			$SubjectMarkArr[$thisStudentID][$GrandSubjectID][0]['Mark'] = $thisStudentMarkArr[0][$AverageCompareField];
		}
		
		### Consolidate all marks of each subject
		$MarkStatisticsArr = array();
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $SubjectIDArr[$i];
			
			### Get Grading Scheme Info
			# Modified by Marcus 20101011 (for template based grading scheme)
			$GradingSchemeInfo[$thisSubjectID] = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, $withGrandResult=1, $returnAsso=0, $ReportID);
			$thisScaleInput = $GradingSchemeInfo[$thisSubjectID]['ScaleInput'];
			
			if ($thisScaleInput != 'M')
				continue;
			
			foreach((array)$SubjectMarkArr as $thisStudentID => $thisStudentSubjectMarkArr)
			{
				$thisYearClassID = $FormStudentInfoArr[$thisStudentID]['YearClassID'];
				$thisRawMark = $thisStudentSubjectMarkArr[$thisSubjectID][0]['RawMark'];
				$thisMark = $thisStudentSubjectMarkArr[$thisSubjectID][0]['Mark'];
				$thisGrade = $thisStudentSubjectMarkArr[$thisSubjectID][0]['Grade'];
				
				### Skip non-numeric score, special case, and excluded student
				if (is_numeric($thisRawMark) == false || $this->Check_If_Grade_Is_SpecialCase($thisGrade) || in_array($thisStudentID, $excludeStudentArr))
					continue;
				
				// If the subject is input Grade, record the Grade for statistics
				if ($thisScaleInput == 'G')
					$thisFinalMark = $thisGrade;
				else
					$thisFinalMark = ($thisMark==0)? my_round($thisRawMark, $SubjectMarkRounding) : my_round($thisMark, $SubjectMarkRounding);
				
				
				if ($IncludeClassStat == 1)
				{
					####### Class Statistics
					### Count the appearance of each mark
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MarkInfo'][$thisFinalMark]['Count']++;
					
					### Record for Average and SD calculation
					(array)$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['AllMarkArr'][] = $thisFinalMark;
					
					### Count Number of Student taken the subject
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['NumOfStudent']++;
					
					### Count the Mark Nature
					$thisMarkNature = $SubjectNatureArr[$thisStudentID][$thisSubjectID][0];
					if ($thisMarkNature == 'Distinction')
					{
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['Distinction']++;
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['TotalPass']++;
						
						// Record the lowest passing mark
						if ($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] == '' || $MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] > $thisFinalMark)
							$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] = $thisFinalMark;
					}
					else if ($thisMarkNature == 'Pass')
					{
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['Pass']++;
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['TotalPass']++;
						
						// Record the lowest passing mark
						if ($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] == '' || $MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] > $thisFinalMark)
							$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] = $thisFinalMark;
					}
					else if ($thisMarkNature == 'Fail')
					{
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['Fail']++;
					}
					
					### Count the Mark Nature for 50 marks for Holy Trinity College
					if ($this->HardCodePassMark != '')
					{
						if ($thisRawMark >= $this->HardCodePassMark)
						{
							$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['Pass']++;
							$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['TotalPass']++;
						}
						else
						{
							$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['Fail']++;							
						}
					}
				}
								
				
				
				####### Form Statistics
				$thisYearClassID = 0;
				
				### Count the appearance of each mark
				$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MarkInfo'][$thisFinalMark]['Count']++;
				
				### Record for Average and SD calculation
				(array)$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['AllMarkArr'][] = $thisFinalMark;
				
				### Count Number of Student taken the subject
				$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['NumOfStudent']++;
				
				### Count the Mark Nature
				$thisMarkNature = $SubjectNatureArr[$thisStudentID][$thisSubjectID][0];
				if ($thisMarkNature == 'Distinction')
				{
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['Distinction']++;
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['TotalPass']++;
					
					// Record the lowest passing mark
					if ($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] == '' || $MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] > $thisFinalMark)
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] = $thisFinalMark;
				}
				else if ($thisMarkNature == 'Pass')
				{
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['Pass']++;
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['TotalPass']++;
					
					// Record the lowest passing mark
					if ($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] == '' || $MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] > $thisFinalMark)
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['LowestPassingMark'] = $thisFinalMark;
				}
				else if ($thisMarkNature == 'Fail')
				{
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['Fail']++;
				}
				
				### Count the Mark Nature for 50 marks for Holy Trinity College
				if ($this->HardCodePassMark != '')
				{
					if ($thisRawMark >= $this->HardCodePassMark)
					{
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['Pass']++;
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['TotalPass']++;
					}
					else
					{
						$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['Fail']++;							
					}
				}
			}
		} 
		
		
		foreach((array)$MarkStatisticsArr as $thisSubjectID => $thisSubjectMarkArr)
		{
			foreach((array)$thisSubjectMarkArr as $thisYearClassID => $thisFormSubjectMarkArr)
			{
				if ($IncludeClassStat == 0 && $thisYearClassID != 0)
					continue;
					
				### Consolidate Max, Min, Average, SD of each subjects of each forms
				if ($GradingSchemeInfo[$thisSubjectID]['ScaleInput'] == 'G' || $thisFormSubjectMarkArr['NumOfStudent']==0)
				{
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MaxMark'] = '---';
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MinMark'] = '---';
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Average_Raw'] = '---';
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Average_Rounded'] = '---';
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['SD_Raw'] = '---';
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['SD_Rounded'] = '---';
				}
				else if ($thisFormSubjectMarkArr['NumOfStudent'] > 0)
				{
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MaxMark'] = max($thisFormSubjectMarkArr['AllMarkArr']);
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MinMark'] = min($thisFormSubjectMarkArr['AllMarkArr']);
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Average_Raw'] = $this->getAverage($thisFormSubjectMarkArr['AllMarkArr']);
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Average_Rounded'] = my_round($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Average_Raw'], $Average_Rounding);
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['SD_Raw'] = $this->getSD($thisFormSubjectMarkArr['AllMarkArr']);
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['SD_Rounded'] = my_round($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['SD_Raw'], $SD_Rounding);
				}
				unset($MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['AllMarkArr']);
				
				### Calculate the Mark Distribution
				$AccumulativeStudentCount = 0;
				$thisNumOfStudentTakenThisSubject = $thisFormSubjectMarkArr['NumOfStudent'];
				$thisMarkInfoArr = $thisFormSubjectMarkArr['MarkInfo'];
				
				if ($GradingSchemeInfo[$thisSubjectID]['ScaleInput'] == 'M')
					krsort($thisMarkInfoArr, SORT_NUMERIC);
				else
					krsort($thisMarkInfoArr);
					
				foreach ($thisMarkInfoArr as $thisMark => $thisMarkCountArr)
				{
					$thisNumOfStudent =  $thisMarkCountArr['Count'];
					$AccumulativeStudentCount += $thisNumOfStudent;
					
					$thisStudentNumPercentage = ($thisNumOfStudent / $thisNumOfStudentTakenThisSubject) * 100;
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MarkInfo'][$thisMark]['StudentNumPercentage'] = my_round($thisStudentNumPercentage, $Percentage_Rounding);
					
					$thisAccumulativeStudentNumPercentage = ($AccumulativeStudentCount / $thisNumOfStudentTakenThisSubject) * 100;
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['MarkInfo'][$thisMark]['AccumlativeStudentNumPercentage'] = my_round($thisAccumulativeStudentNumPercentage, $Percentage_Rounding);
				}
				
				### Calculate the passing percentage
				$thisPassingPercentage = ($thisFormSubjectMarkArr['Nature']['TotalPass'] / $thisFormSubjectMarkArr['NumOfStudent']) * 100;
				$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature']['TotalPassPercentage'] = my_round($thisPassingPercentage, $Percentage_Rounding);
				
				if ($this->HardCodePassMark != '')
				{
					$thisPassingPercentage = ($thisFormSubjectMarkArr['Nature_'.$this->HardCodePassMark]['TotalPass'] / $thisFormSubjectMarkArr['NumOfStudent']) * 100;
					$MarkStatisticsArr[$thisSubjectID][$thisYearClassID]['Nature_'.$this->HardCodePassMark]['TotalPassPercentage'] = my_round($thisPassingPercentage, $Percentage_Rounding);
				}
			}
		}
		
		return $MarkStatisticsArr;
	}
	
	function Get_Student_Studying_Subject_Group($YearTermID, $StudentID, $SubjectID)
	{
		$SubjectGroupInfoArr = $this->Get_Student_Studying_Subject_Group_Info($YearTermID, $StudentID, $SubjectID);
		
		return $SubjectGroupInfoArr[0]['SubjectGroupID'];
	}
	
	function Get_Student_Studying_Subject_Group_Info($YearTermID, $StudentID, $SubjectID)
	{
		$PreloadArrKey = 'Get_Student_Studying_Subject_Group_Info';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		
		//$SubjectObj = new subject($SubjectID);
		//if ($SubjectObj->Is_Component_Subject())
		if ($this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID)) {
			$cond_SubjectID = " And st.SubjectComponentID = '".$SubjectID."' ";
		}
		else {
			$cond_SubjectID = " And st.SubjectID = '".$SubjectID."' And (st.SubjectComponentID Is Null Or st.SubjectComponentID = 0)";
		}
		
		$sql = "Select
						stcu.SubjectGroupID,
						stc.ClassTitleEN,
						stc.ClassTitleB5,
						stcu.UserID
				From
						SUBJECT_TERM as st
						Inner Join
						SUBJECT_TERM_CLASS_USER as stcu	On (stcu.SubjectGroupID = st.SubjectGroupID)
						Inner Join
						SUBJECT_TERM_CLASS as stc On (st.SubjectGroupID = stc.SubjectGroupID)
				Where
						stcu.UserID = '".$StudentID."'
						And
						st.YearTermID = '".$YearTermID."'
						$cond_SubjectID
				";
		$resultSet = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $resultSet);
		return $resultSet;
	}
	
	
	############### Start of Promotion Criteria Functions #######################
	function insertPromotionCriteria($data)
	{
		global $UserID; 
		
		$ClassLevelID = $data['ClassLevelID'];
		$pass_average = ($data['pass_average'] == "1")? 1 : 0;
		$FailSubject_MAX1 = ($data['maxfail1'] == "1")? $data['FailSubject_MAX1'] : "NULL";
		$FailSubject_MAX2 = ($data['maxfail2'] == "1")? $data['FailSubject_MAX2'] : "NULL";
		$FailSubject_MAX3 = ($data['maxfail3'] == "1")? $data['FailSubject_MAX3'] : "NULL";
		$SubjectSelected1 = ($data['maxfail1'] == "1" && count($data['SubjectSelected1'])>0)? implode(",", $data['SubjectSelected1']) : "";
		$SubjectSelected2 = ($data['maxfail2'] == "1" && count($data['SubjectSelected2'])>0)? implode(",", $data['SubjectSelected2']) : "";
		$SubjectSelected3 = ($data['maxfail3'] == "1" && count($data['SubjectSelected3'])>0)? implode(",", $data['SubjectSelected3']) : "";
		$submit_ass = ($data['submit_ass'] == "1")? 1 : 0;
		$SubmitAss_MAX = ($data['submit_ass'] == "1")? $data['SubmitAss_MAX'] : "NULL";
		
		$table = $this->DBName.".RC_PROMOTION_CRITERIA";
		
		$fields = " (	ClassLevelID, PassGrandAverage, 
						MaxFailNumOfSubject1, SubjectList1, MaxFailNumOfSubject2, SubjectList2, MaxFailNumOfSubject3, SubjectList3, 
						NonSubmissionAssignment, MaxAssignment, DateInput, DateModified, LastModifiedBy) ";
		
		$values = " (	$ClassLevelID, $pass_average, 
						$FailSubject_MAX1, '$SubjectSelected1', $FailSubject_MAX2, '$SubjectSelected2', $FailSubject_MAX3, '$SubjectSelected3', 
						$submit_ass, $SubmitAss_MAX, NOW(), NOW(), '".$UserID."' ) ";
						
		$insertSQL = "INSERT INTO $table $fields VALUES $values ";
		
		$result = $this->db_db_query($insertSQL);
		$insertedID = mysql_insert_id();
		
		return $insertedID;
	}
	
	function updatePromotionCriteria($data)
	{
		global $UserID; 
		
		$RecordID = $data['RecordID'];
		$ClassLevelID = $data['ClassLevelID'];
		$pass_average = ($data['pass_average'] == "1")? 1 : 0;
		$FailSubject_MAX1 = ($data['maxfail1'] == "1")? $data['FailSubject_MAX1'] : "NULL";
		$FailSubject_MAX2 = ($data['maxfail2'] == "1")? $data['FailSubject_MAX2'] : "NULL";
		$FailSubject_MAX3 = ($data['maxfail3'] == "1")? $data['FailSubject_MAX3'] : "NULL";
		$SubjectSelected1 = ($data['maxfail1'] == "1" && count($data['SubjectSelected1'])>0)? implode(",", $data['SubjectSelected1']) : "";
		$SubjectSelected2 = ($data['maxfail2'] == "1" && count($data['SubjectSelected2'])>0)? implode(",", $data['SubjectSelected2']) : "";
		$SubjectSelected3 = ($data['maxfail3'] == "1" && count($data['SubjectSelected3'])>0)? implode(",", $data['SubjectSelected3']) : "";
		$submit_ass = ($data['submit_ass'] == "1")? 1 : 0;
		$SubmitAss_MAX = ($data['submit_ass'] == "1")? $data['SubmitAss_MAX'] : "NULL";
		
		$table = $this->DBName.".RC_PROMOTION_CRITERIA";
		
		$sql = "UPDATE $table SET 
						PassGrandAverage = $pass_average, 
						MaxFailNumOfSubject1 = $FailSubject_MAX1, 
						SubjectList1 = '$SubjectSelected1', 
						MaxFailNumOfSubject2 = $FailSubject_MAX2, 
						SubjectList2 = '$SubjectSelected2', 
						MaxFailNumOfSubject3 = $FailSubject_MAX3, 
						SubjectList3 = '$SubjectSelected3', 
						NonSubmissionAssignment = $submit_ass, 
						MaxAssignment = $SubmitAss_MAX, 
						DateModified = NOW(), 
						LastModifiedBy = '$UserID' 
				WHERE 
						ClassLevelID = '$ClassLevelID' 
						AND 
						PromotionCriteriaID = '$RecordID'
				";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
  
	function getPromotionCriteria($ClassLevelID)
	{
		$table = $this->DBName.".RC_PROMOTION_CRITERIA";
		
		$NameField = getNameFieldByLang2("b.");
		$sql = "SELECT 
						a.PromotionCriteriaID,
						a.ClassLevelID,
						a.PassGrandAverage,
						a.MaxFailNumOfSubject1,
						a.SubjectList1,
						a.MaxFailNumOfSubject2,
						a.SubjectList2,
						a.MaxFailNumOfSubject3,
						a.SubjectList3,
						a.NonSubmissionAssignment,
						a.MaxAssignment, 
						a.DateModified,
						a.LastModifiedBy, 
						$NameField as LastModifiedBy 
				FROM 
						$table as a 
						LEFT JOIN INTRANET_USER AS b ON a.LastModifiedBy = b.UserID 
				WHERE
						ClassLevelID = '$ClassLevelID'
		";
		$result = $this->returnArray($sql);
		
		return $result;
	}
  
	function getPromotionStatus($ReportID, $StudentID, $OtherInfoAry=array(), $LastestTermID=0)
	{
		# Set Data
		$ReportColumnID = 0;
		$ParentSubjectOnly = 1;
		$MaxfailGroup = 3;
		$MinConductGrade = "D";
		$NumOfNotSubmitHomework = $OtherInfoAry[0]['NumOfNotSubmitHomework'];
		$Diligence = $OtherInfoAry[$LastestTermID]['Diligence'];
		$Discipline = $OtherInfoAry[$LastestTermID]['Discipline'];
		$Manner = $OtherInfoAry[$LastestTermID]['Manner'];
		$Sociability = $OtherInfoAry[$LastestTermID]['Sociability'];
		//debug_r($OtherInfoAry);
		
		# Get ClassLevelID
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		# Load Promotion Criteria
		$pcResult = $this->getPromotionCriteria($ClassLevelID);
		if(count($pcResult) > 0)
		{
			$RecordID = $pcResult[0]['PromotionCriteriaID']; 		
			$pass_average = $pcResult[0]['PassGrandAverage']; 
			$submit_ass = $pcResult[0]['NonSubmissionAssignment']; 
			$SubmitAss_MAX = $pcResult[0]['MaxAssignment']; 
			
			$FailSubject_MAX1 = $pcResult[0]['MaxFailNumOfSubject1'];
			$FailSubject_MAX2 = $pcResult[0]['MaxFailNumOfSubject2'];
			$FailSubject_MAX3 = $pcResult[0]['MaxFailNumOfSubject3']; 
			$SelectedSubject1 = $pcResult[0]['SubjectList1']; 
			$SelectedSubjectArr1 = trim($SelectedSubject1)? explode(",", $SelectedSubject1) : array();
			$SelectedSubject2 = $pcResult[0]['SubjectList2']; 
			$SelectedSubjectArr2 = trim($SelectedSubject2)? explode(",", $SelectedSubject2) : array();
			$SelectedSubject3 = $pcResult[0]['SubjectList3'];
			$SelectedSubjectArr3 = trim($SelectedSubject3)? explode(",", $SelectedSubject3) : array();
			
			# 1. Check Pass in Grand Average
			if($pass_average)
			{
				$SubjectID = "-1";
				$ResultScoreArr = $this->getReportResultScore($ReportID, $ReportColumnID, $StudentID);
				$GrandAverage = $ResultScoreArr['GrandAverage'];
				$ga_nature = $this->returnMarkNature($ClassLevelID, $SubjectID, $GrandAverage,'','',$ReportID);
				$pass['GrandAverage'] = ($ga_nature != "Fail")? true : false;
			}
			
			# 2. Check Maximum not submitting assignment
			if($submit_ass)
			{
				$pass['NotSubmitAss'] = ($SubmitAss_MAX >= $NumOfNotSubmitHomework) ? true : false;
			}
			
			# 3. Check Maximum fail subjects
			// loop 3 groups 
			for($i=1; $i <= $MaxfailGroup; $i++)
			{
				unset($subj_arr);
				$subj_arr = ${SelectedSubjectArr.$i};
				$maxFail = ${FailSubject_MAX.$i};
				$failCount = 0;
				if(count($subj_arr) > 0)
				{        
					$pass['MaxfailSubjGroup'.$i] = true;
					foreach($subj_arr as $index=>$SubjectID)
					{
						# Modified by Marcus 20101011 (for template based grading scheme)
						$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
						$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
						$ResultMarkArr = $this->getMarks($ReportID, $StudentID, '', $ParentSubjectOnly);
						if($this->Check_If_Grade_Is_SpecialCase($ResultMarkArr[$SubjectID][0]['Grade']))
							continue;              
							
						$subjMarks = ($ScaleDisplay == "M")? $ResultMarkArr[$SubjectID][0]['Mark'] : $ResultMarkArr[$SubjectID][0]['Grade'];
						$maxgp_nature = $this->returnMarkNature($ClassLevelID, $SubjectID, $subjMarks,'','',$ReportID);
						
//						global $test;
//						$test[$StudentID]["Group$i"][$SubjectID] = $maxgp_nature;
						if($maxgp_nature == "Fail") 
							$failCount++;
					}
					
					if($failCount > $maxFail){
						$pass['MaxfailSubjGroup'.$i] = false;
					}       
				}        
			}
			
			# 4. Check Conduct Grade (diligence, discipline, manner, sociability)
			if(trim($Diligence)){
				$conduct['Diligence'] = (substr($Diligence, 0, 1) < $MinConductGrade)? true : false;
			}
			if(trim($Discipline)){
				$conduct['Discipline'] = (substr($Discipline, 0, 1) < $MinConductGrade)? true : false;
			}
			if(trim($Manner)){
				$conduct['Manner'] = (substr($Manner, 0, 1) < $MinConductGrade)? true : false;
			}
			if(trim($Sociability)){
				$conduct['Sociability'] = (substr($Sociability, 0, 1) < $MinConductGrade)? true : false;
			}
			
      $pass['Conduct'] = (in_array(true, $conduct))? true : false;
			
      //debug_pr($pass);
			# Get Promotion Status
			if(in_array(false, $pass))
				$PromotionStatus = "2";	
			else            
				$PromotionStatus = "1";
		}
		else
		{
			$PromotionStatus = "";
		}
		
		return $PromotionStatus;
	}
	############### End of Promotion Criteria Functions #######################
    
	############ Subject Display Lang 	
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function GetFormSubjectDisplayLang($SubjectID,$ClassLevelID,$ReportID='') 
	{
		if(trim($ReportID)=='')
			$cond_ReportID = " AND ReportID = 0 ";
		else
			$cond_ReportID = " AND ReportID = '$ReportID' ";
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$sql = "SELECT LangDisplay FROM $table WHERE ClassLevelID='$ClassLevelID' $cond_ReportID AND SubjectID='$SubjectID' ";
		
		$temp = $this->returnVector($sql);
		return $temp[0];
	}	 
	############ Subject Display Lang End 
	
	############ Display Subject Group
	# Modified by Marcus 20101011 (for template based grading scheme) (param changed)
	function GetDisplaySubjectGroup($SubjectID,$ClassLevelID,$ReportID='') 
	{
		
		if(trim($ReportID)=='')
			$cond_ReportID = " AND ReportID = 0 ";
		else
			$cond_ReportID = " AND ReportID = '$ReportID' ";
		
		$table = $this->DBName.".RC_SUBJECT_FORM_GRADING";
		$sql = "SELECT DisplaySubjectGroup FROM $table WHERE ClassLevelID='$ClassLevelID' $cond_ReportID AND SubjectID='$SubjectID' ";
		
		$temp = $this->returnVector($sql);
		return $temp[0];
	}	 
	############ Display Subject Group End 
	
	############ Position Display Lang 
	
	function Get_Position_Display_Setting($ReportID, $SubjectID='', $RecordType='', $returnAsso=1)
	{
		$PreloadArrKey = 'PositionDisplaySetting';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
		if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
		$subject_conds = '';
		if ($SubjectID != '')
			$subject_conds = " And SubjectID = '$SubjectID' ";
		
		$recordtype_conds = '';
		if ($RecordType != '')
			$recordtype_conds = " And RecordType = '$RecordType' ";
		
		$table = $this->DBName.".RC_POSITION_DISPLAY_SETTING";
		$sql = "SELECT 
						* 
				FROM 
						$table 
				WHERE 
						ReportID = '$ReportID' 
						$subject_conds
						$recordtype_conds
				";
		$tempArr = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		
		$returnArr = array();
		if ($returnAsso == 1)
		{
			$numOfResult = count($tempArr);
			for ($i=0; $i<$numOfResult; $i++)
			{
				$thisSubjectID = $tempArr[$i]['SubjectID'];
				$thisRecordType = $tempArr[$i]['RecordType'];
				
				$returnArr[$thisSubjectID][$thisRecordType] = $tempArr[$i];
			}
		}
		else
		{
			$returnArr = $tempArr;
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
		return $returnArr;
	}
	
	function Insert_Position_Display_Setting($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# DateModified
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = $_SESSION['UserID'];
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$table = $this->DBName.".RC_POSITION_DISPLAY_SETTING";
		$sql = '';
		$sql .= ' INSERT INTO '.$table;
		$sql .= ' ( '.$fieldText.' ) ';
		$sql .= ' VALUES ';
		$sql .= ' ( '.$valueText.' ) ';
		
		$success = $this->db_db_query($sql);
		if ($success == false)
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
	
	function Update_Position_Display_Setting($PositionDisplaySettingID, $DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModified = now() ';
		
		$table = $this->DBName.".RC_POSITION_DISPLAY_SETTING";
		$sql = '';
		$sql .= ' UPDATE '.$table;
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE PositionDisplaySettingID = \''.$PositionDisplaySettingID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	} 
	
	function Copy_Position_Display_Setting($fromReportID, $toReportID)
	{
		$SettingArr = $this->Get_Position_Display_Setting($fromReportID, '', '', $returnAsso=0);
		$numOfSetting = count($SettingArr);
		
		$SuccessArr = array();
		for ($i=0; $i<$numOfSetting; $i++)
		{
			$DataArr = array();
			$DataArr['ReportID'] = $toReportID;
			$DataArr['SubjectID'] = $SettingArr[$i]['SubjectID'];
			$DataArr['RangeType'] = $SettingArr[$i]['RangeType'];
			$DataArr['UpperLimit'] = $SettingArr[$i]['UpperLimit'];
			$DataArr['GreaterThanAverage'] = $SettingArr[$i]['GreaterThanAverage'];
			$DataArr['RecordType'] = $SettingArr[$i]['RecordType'];
			
			$SuccessArr[$SettingArr[$i]['RecordType']][$SettingArr[$i]['SubjectID']] = $this->Insert_Position_Display_Setting($DataArr);
		}
		
		if (in_array(false, $SuccessArr))
			return false;
		else
			return true;
	}
	############ Position Display Lang End 
    
	function Get_Grand_Position_Determine_Field()
	{
		global $eRCTemplateSetting, $intranet_root;
		include_once($intranet_root."/includes/eRCConfig.php");
		
		$AverageCompareField = ($eRCTemplateSetting['OrderingPositionBy']=='')? 'GrandAverage' : $eRCTemplateSetting['OrderingPositionBy'];
		return $AverageCompareField;
	}
	
	function Get_Grand_Field_SubjectID($GrandField)
	{
		if ($GrandField == 'GrandAverage')
			$ID = '-1';
		else if ($GrandField == 'GrandTotal')
			$ID = '-2';
		else if ($GrandField == 'GPA')
			$ID = '-3';
		else if ($GrandField == 'GrandSDScore')
			$ID = '-4';
			
		return $ID;
	}
	
	function Get_SubjectID_Grand_Field_Arr()
	{
		global $eRCTemplateSetting;
		
		$ReturnArr[-1] = "GrandAverage";
		$ReturnArr[-2] = "GrandTotal";
		$ReturnArr[-3] = "GPA";
		if($eRCTemplateSetting['OrderingPositionBy']=="WeightedSD")
			$ReturnArr[-4] = "GrandSDScore";
		
		return $ReturnArr;
	}
	
	############ Course Description Function
	function returnCurriculumExpectation($ClassLevelID, $SubjectID='', $TermID=0)
	{
		$table = $this->DBName.".RC_CURRICULUM_EXPECTATION";
		$sql = "select * from $table where ClassLevelID=$ClassLevelID ";
		
		$sql .= " and TermID='$TermID' ";
		
		if($SubjectID)
			$sql .= " and SubjectID='$SubjectID' ";
			
		$result = $this->returnArray($sql);
		foreach($result as $k=>$d)
			$Content[$d['SubjectID']] = $d['Content'];	
		return $Content;
	}
	
	function Insert_Course_Description($ClassLevelID, $SubjectID, $TermID, $Description)
	{
		$table = $this->DBName.".RC_CURRICULUM_EXPECTATION";
		
		$sql = "insert into 
					$table 
					(ClassLevelID, SubjectID, TermID, Content, DateInput, DateModified) 
				values 
					($ClassLevelID, $SubjectID, $TermID, '$Description', NOW(), NOW())
				";
		$result = $this->db_db_query($sql);
		
		return $result;
	}
	
	function Update_Course_Description($ClassLevelID, $SubjectID, $TermID, $Description)
	{
		$table = $this->DBName.".RC_CURRICULUM_EXPECTATION";
		
		$sql = "Update $table Set 
					Content = '".$Description."', 
					DateModified = now() 
				Where 
					ClassLevelID = '".$ClassLevelID."'
					And
					SubjectID = '".$SubjectID."'
					And
					TermID = '".$TermID."'
				";
		$result = $this->db_db_query($sql);
		
		return $result;
	}
	
	function Is_Course_Description_Exist($ClassLevelID, $SubjectID, $TermID)
	{
		$table = $this->DBName.".RC_CURRICULUM_EXPECTATION";
		
		$sql = "Select CurExpID From $table Where ClassLevelID = '$ClassLevelID' And SubjectID = '$SubjectID' And TermID = '$TermID'";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) > 0)
			return true;
		else
			return false;
	}
	############ End Course Description Function
	
	############ Personal Characteristics Function
	function insertPersonalCharItem($TitleEn, $TitleCh, $DisplayOrder)
	{
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS";
		
		$TitleEn = $this->Get_Safe_Sql_Query($TitleEn);
		$TitleCh = $this->Get_Safe_Sql_Query($TitleCh);
		
		$sql = "insert into 
					$table 
					(Title_EN, Title_CH, DisplayOrder, DateInput, DateModified)
				values 
					('$TitleEn', '$TitleCh', '$DisplayOrder', NOW(), NOW())
				";
		$result = $this->db_db_query($sql);
		
		return $result;
	}
	
	function updatePersonalCharItem($CharID, $TitleEn, $TitleCh, $DisplayOrder)
	{
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS";
		
		$TitleEn = $this->Get_Safe_Sql_Query($TitleEn);
		$TitleCh = $this->Get_Safe_Sql_Query($TitleCh);

		$sql = "update $table set Title_EN='$TitleEn', Title_CH='$TitleCh', DisplayOrder='$DisplayOrder', DateModified=NOW() where CharID=$CharID";
		$result = $this->db_db_query($sql);
		
		return $result;
	}
	
	function removePersonalCharItem($CharID)
	{
		if (is_array($CharID))
			$CharID = implode(",", $CharID);
		
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS";
		$sql = "DELETE FROM {$table} WHERE CharID IN (".$CharID.")";
		
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function returnPersonalCharData($id='')
	{
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS";
		
		$sql = "select * from {$table} ";
		if($id) $sql .= " where CharID in ('".implode("','", (array)$id)."') ";
		$sql .= " order by DisplayOrder, Title_EN";
		
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function returnPersonalCharSettingData($ClassLevelID, $SubjectID='', $GroupByCharID=0)
	{
		global $intranet_session_language;
		
		if ($GroupByCharID == 1) {
			$Group_By_CharID = ' Group By b.CharID ';
		}
		
		$table = $this->DBName.".RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS";
		$table2 = $this->DBName .".RC_PERSONAL_CHARACTERISTICS";
		$sql = "
			select 
				a.ClassLevelID, 
				a.SubjectID,
				b.CharID,
				b.Title_EN,
				b.Title_CH
			from 
				{$table} as a 
				inner join 
				{$table2} as b on (a.CharID=b.CharID)
			where 
				ClassLevelID=$ClassLevelID 
		";
		if($SubjectID || $SubjectID!='') $sql .= " and SubjectID=$SubjectID";
		//$sql .= " order by Title_". ($intranet_session_language=="en" ? "EN" : "CH");
		$sql .= $Group_By_CharID;
		$sql .= " order by b.DisplayOrder ";
		
		$result = $this->returnArray($sql);
		return $result;
	}
	
	# order: 0 - asc, 1-desc
	function returnPersonalCharOptions($br=0, $order='', $Lang_Par='', $ReturnNormalTeacherOnly=0)
	{
		global $intranet_session_language, $eReportCard;
		
		$PersonalCharGradeArr = $this->PersonalCharGradeArr;
		
		$x = array();
		if($br)
		{
			foreach ((array)$PersonalCharGradeArr as $index => $value)
			{
				if ($ReturnNormalTeacherOnly && in_array($index, (array)$this->PersonalCharAdminOnlyGradeArr)) {
					continue;
				}
				
				$x[$index] = $eReportCard[$value.'_en']."<br>".$eReportCard[$value.'_b5'];
			}
		}
		else
		{
			if ($Lang_Par == '')
				$Lang_Par = $intranet_session_language;
				
			foreach ((array)$PersonalCharGradeArr as $index => $value)
			{
				if ($ReturnNormalTeacherOnly && in_array($index, (array)$this->PersonalCharAdminOnlyGradeArr)) {
					continue;
				}
				
				$x[$index] = $eReportCard[$value.'_'.$Lang_Par];
			}
		}
		
		if($order)
			krsort($x);
		
		return $x;
	}
	
	function returnPersonalCharTitleByID($CharID,$ParLang='')
	{
		global $intranet_session_language;
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS";
		
		if($ParLang=='')	
			$ParLang=$intranet_session_language;
			
		if($ParLang=="en")	
			$Title = "Title_EN";
		else
			$Title = "Title_CH";
			
		$sql = "
			SELECT 
				$Title
			FROM
				$table
			WHERE
				CharID = '$CharID'
			Order By
				DisplayOrder
		";
		
		$result = $this->returnVector($sql);
		
		return $result[0];
		
	}
	
//	function getPersonalCharacteristicsData($StudentID='', $ReportID='', $SubjectID='')
//	{
//		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_DATA";
//		
//		$sql = "select 
//					CharData
//				from 
//					{$table} 
//				where 
//					StudentID = $StudentID and
//					ReportID = $ReportID and 
//					SubjectID = $SubjectID
//				";
//		
//		$result = $this->returnArray($sql);
//		return $result;
//	}
	
	// modified by marcus 20110520, support get by batch
	function getPersonalCharacteristicsData($StudentID='', $ReportID='', $SubjectID='')
	{
		$table = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_DATA";
		
		// 2011-05-27 (Ivan)
		// Get the Student of this year as the Personal Char data may include last year student info (caused by problem in the past, the problem has been fixed now)
		if (trim($ReportID)!='') {
			if (!is_array($ReportID)) {
				$ReportID = array($ReportID);
			}
			$numOfReport = count($ReportID);
			
			$ReportStudentIDArr = array();
			for ($i=0; $i<$numOfReport; $i++) {
				$thisReportID = $ReportID[$i];
				$thisReportInfoArr = $this->returnReportTemplateBasicInfo($thisReportID);
				$thisClassLevelID = $thisReportInfoArr['ClassLevelID'];
				
				$thisStudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID);
				$thisStudentIDArr = Get_Array_By_Key($thisStudentInfoArr, 'UserID');
				
				$ReportStudentIDArr = array_values(array_unique(array_merge($ReportStudentIDArr, $thisStudentIDArr)));
			}
			$conds_ReportStudentID = " AND StudentID IN (".implode(',',(array)$ReportStudentIDArr).") ";
		}
		
		if(trim($StudentID)!='')
			$cond_StudentID = " AND StudentID IN (".implode(',',(array)$StudentID).") ";
		if(trim($ReportID)!='')
			$cond_ReportID = " AND ReportID IN (".implode(',',(array)$ReportID).") ";
		if(trim($SubjectID)!='')
			$cond_SubjectID = " AND SubjectID IN (".implode(',',(array)$SubjectID).") ";
			
		$sql = "select 
					CharData,
					Comment,
					StudentID,
					ReportID,
					SubjectID
				from 
					{$table} 
				where
					1 
					$conds_ReportStudentID
					$cond_StudentID
					$cond_ReportID
					$cond_SubjectID
				";
		
		$result = $this->returnArray($sql);
		return $result;
	}

	
	function getPersonalCharacteristicsProcessedData($StudentID='', $ReportID='', $SubjectID='')
	{
		global $intranet_session_language;
		
		$result = $this->getPersonalCharacteristicsData($StudentID, $ReportID, $SubjectID);
		$CharOptions = $this->returnPersonalCharOptions(0, 0, $intranet_session_language);
		//debug_pr($result);
		if(empty($result)) return null;
		
		$rawData = $result[0][0];
		$token = explode(",",$rawData);
		
		foreach((array)$token as $eachChar)
		{
			list($CharID,$CharData) = explode(":",$eachChar);
			$CharName = $this->returnPersonalCharTitleByID($CharID);
			
			$returnAry[$CharName] = $CharOptions[$CharData];
		}
		
		return $returnAry;
		
	}
	
	function getPersonalCharacteristicsProcessedDataByBatch($StudentID, $ReportID, $SubjectID, $ReturnGradeCode=0, $ReturnCharID=0, $ReturnComment=0)
	{
		global $intranet_session_language;
		
		$result = $this->getPersonalCharacteristicsData($StudentID, $ReportID, $SubjectID);
		if(empty($result)) return null;
		
		$CharOptions = $this->returnPersonalCharOptions(0, 0, $intranet_session_language);
		
		$AssocArr = BuildMultiKeyAssoc($result, array("ReportID","SubjectID","StudentID"));
		
		foreach((array)$AssocArr as $ReportID =>$SubjectArr)
		{
			foreach((array)$SubjectArr as $SubjectID => $StudentArr)
			{
				foreach((array)$StudentArr as $StudentID =>$rawData)
				{
					$token = explode(",",$rawData['CharData']);
					
					foreach((array)$token as $eachChar)
					{
						list($CharID,$CharData) = explode(":",$eachChar);
						$CharName = $this->returnPersonalCharTitleByID($CharID);
						
						$thisKey = ($ReturnCharID==1)? $CharID : $CharName;
						$thisValue = ($ReturnGradeCode==1)? $CharData : $CharOptions[$CharData];
						
						$returnAry[$ReportID][$SubjectID][$StudentID][$thisKey] = $thisValue;
					}
					
					if ($ReturnComment) {
						$returnAry[$ReportID][$SubjectID][$StudentID]['comment'] = $rawData['Comment'];
					}
				}
			}
		}
			
		return $returnAry;
	}
	
	function getPersonalCharacteristicsOptionTitleByID($CharID, $ParLang='') {
		global $eReportCard, $intranet_session_language;
		
		$LangKey = $this->PersonalCharGradeArr[$CharID];
		$ParLang = ($ParLang=='')? $intranet_session_language : $ParLang;
		
		return $eReportCard[$LangKey.'_'.$ParLang];
	}
	
	function GET_CLASSLEVEL_CHARACTERISTICS($ParData, $ReverseOrder='')
	{
		global $intranet_session_language;
		
		if ($ReverseOrder == '')
			$ReverseOrder = $this->DisplayCharItemInReverseOrder;
			
		if ($ReverseOrder == 1)
			$cond_order = 'desc';
			
		$CharLangField = Get_Lang_Selection('a.Title_CH', 'a.Title_EN');
		
		$sql =  " SELECT a.CharID, $CharLangField, a.Title_EN as CharTitleEn, a.Title_CH as CharTitleCh ";
		$sql .= " FROM ". $this->DBName .".RC_PERSONAL_CHARACTERISTICS as a ";
		$sql .= " LEFT JOIN ". $this->DBName .".RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS as b ";
		$sql .= " ON a.CharID = b.CharID ";
		$sql .= " WHERE b.ClassLevelID = ". $ParData['ClassLevelID'] ." AND SubjectID = ". $ParData['SubjectID'];
		$sql .= " ORDER BY a.DisplayOrder $cond_order";
			
		$result = $this->returnArray($sql,2);
		
		return $result;
	}
	
	function IS_STUDENT_CHARACTERISTICS_RECORD_EXIST($ParDataArr)
	{
		$table = $this->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
		
		$sql = "SELECT 
						COUNT(CharRecordID) 
				FROM 
						$table
				WHERE 
						StudentID = '".$ParDataArr['StudentID']."'
						AND
						SubjectID = '".$ParDataArr['SubjectID']."'
						AND
						ReportID = '".$ParDataArr['ReportID']."'
				";
		$result = $this->returnVector($sql);
		
		return ($result[0] != 0);
	}
	
	function INSERT_STUDENT_CHARACTERISTICS_RECORD($ParDataArr)
	{
		global $UserID;
		$table = $this->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
		$sql = "INSERT INTO ".$table." 
				(StudentID, SubjectID, SubjectGroupID, ReportID, CharData, Comment, TeacherID, DateInput, DateModified)
				VALUES ( 
						 '".$ParDataArr['StudentID']."', 
						 '".$ParDataArr['SubjectID']."',
						 '".$ParDataArr['SubjectGroupID']."',
						 '".$ParDataArr['ReportID']."', 
						 '".$ParDataArr['CharData']."',
						'".$ParDataArr['Comment']."',  
						 '".$UserID."', NOW(), NOW()
						)
				";
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function UPDATE_STUDENT_CHARACTERISTICS_RECORD($ParDataArr)
	{
		global $UserID;
		$table = $this->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
		$sql = "UPDATE 
						$table
				SET 
						CharData = '".$ParDataArr['CharData']."',
						Comment = '".$ParDataArr['Comment']."',
						SubjectGroupID = '".$ParDataArr['SubjectGroupID']."',
						TeacherID = $UserID,
						DateModified = NOW()
				WHERE 
						StudentID = '".$ParDataArr['StudentID']."'
						AND
						SubjectID = '".$ParDataArr['SubjectID']."'
						AND
						ReportID = '".$ParDataArr['ReportID']."'
				";
			
		$result = $this->db_db_query($sql);
		return $result;
	}
	############ End Personal Characteristics Function
	
	# Used to gen empty in the statistic rows
	function GrandMarkSheetGenEmptySymbol($returnType,$NumberOfEmptySymbol)
	{
		if($returnType=='HTML')
		{
			for($i=0; $i<$NumberOfEmptySymbol;$i++)
				$markTable .= "<td align='center' >".$this->EmptySymbol."</td>";
			
		}
		else if($returnType=='CSV')
		{
			$markTable = array();
			for($i=0; $i<$NumberOfEmptySymbol;$i++)
				$markTable[] = $this->EmptySymbol;
				
		}
		
		return $markTable;
	}
	
	function GetPromotionGenerationDate($YearID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE ";
		$sql = "
			SELECT
				LastPromotionUpdate,
				LastGenerated
			FROM
				$table
			WHERE 
				ClassLevelID = '$YearID'
				AND Semester = 'F'
		";
		
		$returnValue = $this->returnArray($sql);
		return $returnValue[0];
	}
	
	function GetPromotionStatusList($YearID='', $YearClassID='', $StudentID='', $ReportID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		
		if ($ReportID == '')
		{
			$ReportSetting = $this->returnReportTemplateBasicInfo(''," ClassLevelID='$YearID' AND Semester='F' ");
			$ReportID = $ReportSetting["ReportID"];
		}
		
		# Get StudentList
		$cond_StudentID = '';
		if ($StudentID == '')
		{
			$StudentList= array();
			if($YearClassID != '' && $YearClassID != 0)
				$StudentList = $this->GET_STUDENT_BY_CLASS($YearClassID);
			else
				$StudentList = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $YearID);
				
			$StudentIDAry = Get_Array_By_Key($StudentList,"UserID");
			$StudentIDStr = implode(",",$StudentIDAry);
			
			$cond_StudentID = " And rrr.StudentID IN ($StudentIDStr) ";
		}
		else
		{
			$cond_StudentID = " And rrr.StudentID = '".$StudentID."' ";
		}		
		
		# Get Promotion Status
		$table = $this->DBName.".RC_REPORT_RESULT as rrr";
		$sql = "
			SELECT
				rrr.StudentID,
				rrr.Promotion,
				ycu.YearClassID
			FROM 
				$table
				Inner Join
				YEAR_CLASS_USER as ycu
				On rrr.StudentID = ycu.UserID
				Inner Join
				YEAR_CLASS as yc
				On ycu.YearClassID = yc.YearClassID
			WHERE
				rrr.ReportID = '$ReportID'
				And 
				(rrr.ReportColumnID = 0 OR rrr.ReportColumnID IS NULL)
				And 
				yc.AcademicYearID = '".$this->schoolYearID."'
				$cond_StudentID
			Order By
				yc.Sequence, ycu.ClassNumber
			";
		$StudentPromotionArr = $this->returnArray($sql);
		
		$OtherInfo = array();
		foreach((array)$StudentPromotionArr as $thisStudentPromotionArr)
		{
			$thisStudentID = $thisStudentPromotionArr["StudentID"];
			$thisYearClassID = $thisStudentPromotionArr["YearClassID"];
			$thisPromotionStatus = $thisStudentPromotionArr["Promotion"];
			$thisPromotionStatusText = $eReportCard["PromotionStatus"][$thisPromotionStatus];
			
			$user = new libuser($thisStudentID);
			$thisStudentName = $user->UserNameLang(0);
			
			if (is_array($OtherInfo[$thisYearClassID]) == false)
				$OtherInfo[$thisYearClassID] = $this->getReportOtherInfoData($ReportID, '', $thisYearClassID, "remark");
				
			$CSVPromotionText = isset($OtherInfo[$thisYearClassID][$thisStudentID][0]["Promotion"])? $OtherInfo[$thisYearClassID][$thisStudentID][0]["Promotion"] : '';
			$CSVPromotionText = is_array($CSVPromotionText)? $CSVPromotionText[0] : $CSVPromotionText;
			
			$returnAry[$thisStudentID]["GenerationStatus"] = $thisPromotionStatusText;
			
			if($CSVPromotionText=="empty")
				$returnAry[$thisStudentID]["FinalStatus"] = '';
			else
				$returnAry[$thisStudentID]["FinalStatus"] = $CSVPromotionText? $CSVPromotionText : $thisPromotionStatusText;
			$returnAry[$thisStudentID]["Promotion"] = $CSVPromotionText? 3 : $thisPromotionStatus;
		}
		
		return $returnAry; 
	}
	
	function UpdatePromotion($ReportID, $StudentID, $PromotionStatus)
	{
		$table = $this->DBName.".RC_REPORT_RESULT ";
		
		$sql = "
			UPDATE
				$table
			SET
				Promotion = '$PromotionStatus'
			WHERE
				ReportID = '$ReportID'
				AND (ReportColumnID = 0 OR ReportColumnID IS NULL)
				AND StudentID = '$StudentID'
		";
		
		return $this->db_db_query($sql);
	}
	
	function UpdatePromotionDate($ReportID)
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE ";
		
		$sql = "
			UPDATE
				$table
			SET 
				LastPromotionUpdate = NOW()
			WHERE 
				ReportID = '$ReportID'
		";
		
		return $this->db_db_query($sql);
	}
	
	
	/**********************************for munsang custom***********************************/
	function Get_eDiscipline_Data($YearTermID, $StudentID)
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
		
		$AcademicYearID = $this->GET_ACTIVE_YEAR_ID();
		
		$returnArr = array();
		$ldiscipline = new libdisciplinev12();

		/* 	20100211 Ivan 
			CS-WiseLee: henry �P�ڱN�N update conduct grade��, �N�|�X�f�Ѯv�쥻�Jo�ӭ�
			The below coding cannot handle the above case
		$eDisDataArr = $ldiscipline->getFinalConductGradeString($AcademicYearID, $YearTermID, $StudentID);
		$ConductArr = explode(',', $eDisDataArr['ConductString']);
		
		$returnArr['Conduct'] = $eDisDataArr['IntegratedConductGrade'];
		
		foreach ((array)$ConductArr as $key => $value)
		{
			$thisConductArr = explode(':', $value);
			$thisConductKey = $thisConductArr[0];
			$thisConductValue = $thisConductArr[1];
			
			if ($thisConductKey == 1)
				$returnArr['Discipline'] = $thisConductValue;
			else if ($thisConductKey == 2)
				$returnArr['Diligence'] = $thisConductValue;
			else if ($thisConductKey == 3)
				$returnArr['Manner'] = $thisConductValue;
			else if ($thisConductKey == 4)
				$returnArr['Sociability'] = $thisConductValue;
		}
		*/
		
		
		# Output :
		# Discipline:E, Diligence:C-, Manner:F, Sociability:F, Final Conduct Grade:E
		$eDisDataStr = $ldiscipline->getFinalGradeString($AcademicYearID, $YearTermID, $StudentID);
		$ConductArr = explode(', ', $eDisDataStr);
		$numOfConduct = count($ConductArr);
		for ($i=0; $i<$numOfConduct; $i++)
		{
			$thisValue = $ConductArr[$i];
			$thisValueArr = explode(':', $thisValue);
			$thisGrade = $thisValueArr[1];
			
			switch ($i) 
			{
				case 0:
					$returnArr['Discipline'] = $thisGrade;
					break;
				case 1:
					$returnArr['Diligence'] = $thisGrade;
					break;
				case 2:
					$returnArr['Manner'] = $thisGrade;
					break;
				case 3:
					$returnArr['Sociability'] = $thisGrade;
					break;
				case 4:
					$returnArr['Conduct'] = $thisGrade;
					break;
			}
		}
		
		return $returnArr;
	}
	
	function Get_eEnrolment_Data($StudentID, $YearTermIDArr='')
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
		
		$AcademicYearID = $this->GET_ACTIVE_YEAR_ID();
		$libenroll = new libclubsenrol();
		$ECA_Arr = $libenroll->Get_Student_Club_Info($StudentID, $EnrolGroupID='', $AcademicYearID, $YearTermIDArr);
		$numOfECA = count($ECA_Arr);
		
		$ReturnArr = array();
		for($i=0; $i<$numOfECA; $i++)
		{
			$ReturnArr[$i]['ClubTitle'] = $ECA_Arr[$i]['ClubTitle'];
			$ReturnArr[$i]['RoleTitle'] = $ECA_Arr[$i]['RoleTitle'];
			$ReturnArr[$i]['Performance'] = $ECA_Arr[$i]['Performance'];
		}
		
		return $ReturnArr;
	}
	
	function Get_Student_Profile_Data($ReportID, $StudentID, $MapByRecordType=false, $CheckRecordStatus=true,$returnTotalUnit=false)
	{
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$TermStartDate = $ReportInfoArr['TermStartDate'];
		$TermEndDate = $ReportInfoArr['TermEndDate'];
		
		$cond_checkRecordStatus='';
		if($CheckRecordStatus)
		{
			$cond_checkRecordStatus = "And RecordStatus = '1'";
		}
		
		$sql = "Select 
						RecordType,
						Count(StudentMeritID) as NumOfMerit,
						Sum(NumberOfUnit) as TotalUnit
				From 
						PROFILE_STUDENT_MERIT
				Where
						UserID = '".$StudentID."'
						$cond_checkRecordStatus
						And
						Date(MeritDate) Between '$TermStartDate' And '$TermEndDate'
				Group By
						RecordType
				";
		$DataArr = $this->returnArray($sql);
		$numOfData = count($DataArr);
		
		$ReturnArr = array();
		for ($i=0; $i<$numOfData; $i++)
		{
			$thisRecordType = $DataArr[$i]['RecordType'];
			$thisNumOfMerit = $DataArr[$i]['NumOfMerit'];
			$thisTotalUnit = $DataArr[$i]['TotalUnit'];
			
			$thisTitle = '';
			if ($MapByRecordType) {
				$thisTitle = $thisRecordType;
			}
			else {
				switch ($thisRecordType){
					case "1":
						$thisTitle = 'Merits';
						break;
					case "2":
						$thisTitle = 'MinorAchievement';
						break;
					case "3":
						$thisTitle = 'MajorAchievement';
						break;
					default:
						break;
				};
			}
			
		
			if ($thisTitle !== '')
			{
				if($returnTotalUnit)
				{
					$ReturnArr[$thisTitle] = (int)$thisTotalUnit;
				}
				else
				{
					$ReturnArr[$thisTitle] = $thisNumOfMerit;
				}
				
			}
				
		}

		return $ReturnArr;
	}
	
	function Get_Student_Profile_Attendance_Data($SemID, $ClassID, $StartDate='', $EndDate='')
	{
			include_once("libclass.php");
			include_once("libattendance.php");
			$lattend = new libattendance();
			include_once("form_class_manage.php");
			$objYearTerm = new academic_year_term($SemID);
			 
			if ($SemID!=0) {
				$StartDate = $objYearTerm->TermStart;
				$EndDate = $objYearTerm->TermEnd;
			}
			
			$class_name = $lattend->getClassName($ClassID);
			$result = $lattend->getAttendanceListByClass($class_name,$StartDate,$EndDate,$reason);
			
			$ReturnArr = array();
			for($i = 0; $i<sizeof($result); $i++)
			{
				list($id,$name,$classnumber,$absence,$late,$earlyleave) = $result[$i];
				$ReturnArr[$id]["Days Absent"] = $absence;
				$ReturnArr[$id]["Time Late"] = $late; 
			}
			
			return $ReturnArr;
 
	}
	/**********************************for munsang custom end***********************************/
	
	/**********************************for worker children  ***********************************/
	function Get_eDiscipline_Conduct_Data($ReportIDArr, $StudentIDArr)
	{
		//aaaa
		$AcademicYearID = $this->GET_ACTIVE_YEAR_ID();
		
		$ReportYearTermArr = array();
		if($ReportIDArr!='')
		{
			if (!is_array($ReportIDArr)) {
				$ReportIDArr = array($ReportIDArr);
			}
			
			for($i=0,$i_MAX=count($ReportIDArr);$i<$i_MAX;$i++)
			{
				$thisReportID = $ReportIDArr[$i];
				
				$thisReportInfoArr = $this->returnReportTemplateBasicInfo($ReportIDArr[$i]);
				$thisReportTermAssoArr = $this->returnReportInvolvedSem($thisReportID, $DisplayName=1);
				$thisReportTermIDArr = array_keys($thisReportTermAssoArr);
				
				$YearTermIDArr = array_merge((array)$YearTermIDArr, $thisReportTermIDArr);
			}
			
			$cond_YearTermID = "And YearTermID in ('".implode("','", (array)$YearTermIDArr)."')";				
		}
		
		if($StudentIDArr!='')
		{	
			$cond_StudentID = "And StudentID in ('".implode("','", (array)$StudentIDArr)."')";
		}
		
		
		$sql = "Select 
						StudentID,YearTermID,GradeChar
				From 
						DISCIPLINE_STUDENT_CONDUCT_BALANCE
				Where
						
						AcademicYearID = '$AcademicYearID'
						$cond_YearTermID
						$cond_StudentID
				";
		$DataArr = $this->returnArray($sql);
		
		$ReturnArr = array();
		for ($i=0,$i_MAX=count($DataArr); $i<$i_MAX; $i++)
		{
			$thisStudentID = $DataArr[$i]['StudentID'];
			$thisYearTerm = $DataArr[$i]['YearTermID'];	

			if ($thisStudentID != '' && $thisYearTerm!='')
			{
				 $ReturnArr[$thisStudentID][$thisYearTerm]['Conduct'] =  $DataArr[$i]['GradeChar'];
			}
				 
		}

		return $ReturnArr;
	}
	/**********************************for worker children end***********************************/
	
	
	function Check_If_Grade_Is_SpecialCase($Grade)
	{
		return (in_array($Grade,$this->specialCasesSet1) || in_array($Grade,$this->specialCasesSet2));
	}
	
	function Check_Copy_Special_Case($TargetSC, $ReportType)
	{
		global $eRCTemplateSetting;
		
		$needCopy = false;
		if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']==true)
		{
			$CopySCArr = $eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCaseArr'];
			if (is_array($CopySCArr)==false || (is_array($CopySCArr)==true && in_array($TargetSC, $CopySCArr)))
			{
				$thisReportTypeArr = $eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ReportTypeArr'][$TargetSC];
				if ((count((array)$thisReportTypeArr)==0) || (count((array)$thisReportTypeArr) > 0 && in_array($ReportType, $thisReportTypeArr)))
					$needCopy = true;
			}
		}
		return $needCopy;
	}
	
	function Delete_Academic_Progress($ReportID)
	{
		$table = $this->DBName.".RC_STUDENT_ACADEMIC_PROGRESS ";
		$sql = "Delete From $table Where ReportID = '$ReportID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	// $DataArr[$SubjectID][$StudentID][InfoField] = value
	function Insert_Academic_Progress($ReportID, $FromReportID, $ToReportID, $DataArr)
	{
		if (count($DataArr) == 0)
			return false;
		
		$ValueArr = array();
		foreach((array)$DataArr as $thisSubjectID => $thisSubjectDataArr)
		{
			foreach((array)$thisSubjectDataArr as $thisStudentID => $thisStudentDataArr)
			{
				$ValueArr[] = "	(
									'$ReportID', '$thisSubjectID', '$thisStudentID', '$FromReportID', '$ToReportID', 
									'".$thisStudentDataArr['FromScore']."', '".$thisStudentDataArr['ToScore']."', '".$thisStudentDataArr['ScoreDifference']."', 
									'".$thisStudentDataArr['OrderClassScoreDifference']."', '".$thisStudentDataArr['OrderFormScoreDifference']."',
									'".$thisStudentDataArr['AcademicProgressPrize']."',
									now(), '".$_SESSION['UserID']."'
								)
								";
			}
		}
		
		if (count($ValueArr) > 0)
		{
			# Insert Record
			$this->Start_Trans();
			
			$ValuesList = implode(',', $ValueArr);
			$table = $this->DBName.".RC_STUDENT_ACADEMIC_PROGRESS ";
			$sql = "Insert Into $table
						(
							ReportID, SubjectID, StudentID, FromReportID, ToReportID,
							FromScore, ToScore, ScoreDifference, 
							OrderClassScoreDifference, OrderFormScoreDifference,
							AcademicProgressPrize,
							DateModified, LastModifiedBy
						)
					Values
						$ValuesList
					";
			$SuccessArr['InsertProgressRecords'] = $this->db_db_query($sql);
			
			$SuccessArr['UpdateLastGenerateProgress'] = $this->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGeneratedAcademicProgress');
			if (in_array(false, $SuccessArr))
			{
				$this->RollBack_Trans();
				return false;
			}
			else
			{
				$this->Commit_Trans();
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	
	function Update_Academic_Progress($AcademicProgressIDArr, $PrizeStatus)
	{
		$table = $this->DBName.".RC_STUDENT_ACADEMIC_PROGRESS ";
		$sql = "Update $table Set AcademicProgressPrize = '$PrizeStatus' Where AcademicProgressID In (".implode(',', $AcademicProgressIDArr).")";
		return $this->db_db_query($sql);
	}
	
	function Get_Academic_Progress($ReportID, $SubjectIDArr=array(), $StudentIDArr=array())
	{
		$conds_SubjectID = '';
		if(!is_array($SubjectIDArr) && $SubjectIDArr < 0)
			$conds_SubjectID = " And SubjectID < 0 ";
		else if (count($SubjectIDArr) > 0)
			$conds_SubjectID = " And SubjectID In (".implode(',', (array)$SubjectIDArr).") ";
			
		$conds_StudentID = '';
		if (is_array($StudentIDArr) && count($StudentIDArr) > 0)
			$conds_StudentID = " And StudentID In (".implode(',', $StudentIDArr).") ";
		
		$table = $this->DBName.".RC_STUDENT_ACADEMIC_PROGRESS ";
		$sql = "Select
						StudentID,
						SubjectID,
						FromScore,
						ToScore,
						ScoreDifference,
						OrderClassScoreDifference,
						OrderFormScoreDifference,
						AcademicProgressPrize
				From
						$table
				Where
						ReportID = '$ReportID'
						$conds_SubjectID
						$conds_StudentID
				Order By
						SubjectID, ScoreDifference desc
				";
				
		$resultArr = $this->returnArray($sql);
		
		return BuildMultiKeyAssoc($resultArr, array("StudentID", "SubjectID"));
	}
	
	function Get_Academic_Progress_View_Sql($ReportID, $YearClassID='', $SubjectID='', $ShowStyle=1, $MaxClassPosition='', $MaxFormPosition='', $PrizeStatus='')
	{
		global $Lang;
		
		$conds_YearClassID = '';
		if ($YearClassID != '')
			$conds_YearClassID = " And yc.YearClassID = '$YearClassID' ";
			
		$conds_SubjectID = '';
		if ($SubjectID != '')
		{
			if ($SubjectID > 0)	
				$conds_SubjectID = " And Progress.SubjectID = '$SubjectID' ";
			else
				$conds_SubjectID = " And Progress.SubjectID < 0 ";
		}	
		$conds_ClassPosition = '';
		if ($MaxClassPosition != '')
			$conds_ClassPosition = " And Progress.OrderClassScoreDifference <= $MaxClassPosition ";
			
		$conds_FormPosition = '';
		if ($MaxFormPosition != '')
			$conds_FormPosition = " And Progress.OrderFormScoreDifference <= $MaxFormPosition ";
			
		$conds_PrizeStatus = '';
		if ($PrizeStatus !== '')	// '' means all, 1 means with prize, 0 means without prize => need to use "!=="
			$conds_PrizeStatus = " And Progress.AcademicProgressPrize = '$PrizeStatus' ";
			
		$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		$StudentNameField = getNameFieldByLang2('u.');
		$ArchiveStudentNameField = getNameFieldByLang2('au.');
		
		if ($ShowStyle == 0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
		
		$ProgressTable = $this->DBName.".RC_STUDENT_ACADEMIC_PROGRESS ";
		$sql = "Select
						$ClassNameField as ClassName,
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL Then CONCAT('$starHTML', ".$ArchiveStudentNameField.") 
							WHEN u.RecordStatus = 3 Then CONCAT('$starHTML', ".$StudentNameField.") 
							ELSE ".$StudentNameField." 
						END as StudentName,
						Progress.FromScore,
						Progress.ToScore,
						Progress.ScoreDifference,
						Progress.OrderClassScoreDifference,
						Progress.OrderFormScoreDifference,
						If (Progress.AcademicProgressPrize=1, '".$Lang['General']['Yes']."', '".$Lang['General']['No']."') as HavePrize,
						Concat('<input type=\"checkbox\" name=\"AcademicProgressIDArr[]\" value=\"', Progress.AcademicProgressID, '\" />')
				From
						$ProgressTable as Progress
						Inner Join YEAR_CLASS_USER as ycu On (Progress.StudentID = ycu.UserID) 
						Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join INTRANET_USER as u On (ycu.UserID = u.UserID)
						Left Outer Join INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
				Where
						yc.AcademicYearID = '".$this->schoolYearID."'
						And Progress.ReportID = '".$ReportID."'
						$conds_YearClassID
						$conds_SubjectID
						$conds_ClassPosition
						$conds_FormPosition
						$conds_PrizeStatus
				";
		return $sql;
	}
	
	function Get_User_Array_From_Selection($SelectionArr, $YearID)
    {
    	global $PATH_WRT_ROOT;
    	include_once($PATH_WRT_ROOT.'includes/libgrouping.php');
    	
    	$UserIDArr = array();
    	$libgrouping = new libgrouping();
    	$OwnClassStudentOnly = ($this->IS_ADMIN_USER($_SESSION['UserID']))? 0 : 1;
    	
    	foreach((array)$SelectionArr as $key => $value)
		{
			$type = substr($value, 0, 1);	// U: User, G: Group, C: Class 
			$type_id = substr($value, 1);
			switch($type)
			{
				case "U":
					$UserIDArr[] = $type_id;
					break;
				case "G":
				case "C":
					//$objGroup = new libgroup($type_id);
					//$GroupMemberID = $objGroup->Get_Group_User($UserType=2);
					//$temp = $libgrouping->returnGroupUsersInIdentity($groupIDAry, $PermittedUserTypeArr);
					
					$GroupMemberInfoArr = $libgrouping->returnGroupUsersInIdentity(array($type_id), array($PermittedUserType=2), $YearID, $OwnClassStudentOnly);
					foreach($GroupMemberInfoArr as $k1 => $d1)
						$UserIDArr[] = $d1['UserID'];
					
					break;
				default:
					$UserIDArr[] = $value;
					break;
			}
		}	
		
		$UserIDArr = array_unique($UserIDArr);
		return $UserIDArr;
    }
    
    function Get_Available_User_To_Add_Comment($YearID='', $ExcludeUserIDArr='', $UserLogin='', $ClassName='', $ClassNumber='', $StudentName='', $TeachingClassOnly=0)
    {
    	# Exclude Student condition
    	$ExcludeUserIDArr = array_remove_empty($ExcludeUserIDArr);
		if (is_array($ExcludeUserIDArr) && count($ExcludeUserIDArr) > 0)
		{
			$excludeUserList = implode(',', $ExcludeUserIDArr);
			$conds_excludeUserID = " And u.UserID Not In ($excludeUserList) ";
		}
		
		# Student form condition
		$conds_YearID = '';
		if ($YearID != '')
			$conds_YearID = " And yc.YearID = '".$YearID."' ";
			
		# Student Login condition
		$conds_UserLogin = '';
		if ($UserLogin != '')
			$conds_UserLogin = " And u.UserLogin Like '%".$this->Get_Safe_Sql_Like_Query($UserLogin)."%' ";
			
		# Student class condition
		$conds_ClassName = '';
		if ($ClassName != '')
			$conds_ClassName = " And	(
											yc.ClassTitleEN Like '%".$this->Get_Safe_Sql_Like_Query($ClassName)."%'
											Or
											yc.ClassTitleB5 Like '%".$this->Get_Safe_Sql_Like_Query($ClassName)."%' 
										)
								";
			
		# Student class number condition
		$conds_ClassNumber = '';
		if ($ClassNumber != '')
			$conds_ClassNumber = " And ycu.ClassNumber = '".$this->Get_Safe_Sql_Query($ClassNumber)."' ";
			
		# Student name condition
		$conds_StudentName = '';
		if ($StudentName != '')
			$conds_StudentName = " And	(
											u.EnglishName Like '%".$this->Get_Safe_Sql_Like_Query($StudentName)."%'
											Or
											u.ChineseName Like '%".$this->Get_Safe_Sql_Like_Query($StudentName)."%' 
										)
								";
			
		$conds_ClassTeacher = '';
		if ($TeachingClassOnly == 1)
		{
			$TeachingClassInfoArr = $this->GET_TEACHING_CLASS();
			$TeachingYearClassIDArr = Get_Array_By_Key($TeachingClassInfoArr, 'YearClassID');
			
			if (count($TeachingYearClassIDArr) > 0)
				$conds_ClassTeacher = " And yc.YearClassID In (".implode(',', $TeachingYearClassIDArr).") ";
			else
				$conds_ClassTeacher = " And 0 ";
		}
		
		$name_field = getNameFieldWithClassNumberByLang('u.');
    	$sql = "Select
						u.UserID,
						$name_field as UserName,
						u.UserLogin
				From
						INTRANET_USER as u
						Inner Join
						YEAR_CLASS_USER as ycu
						On (u.UserID = ycu.UserID)
						Inner Join
						YEAR_CLASS as yc
						On (ycu.YearClassID = yc.YearClassID)
				Where
						yc.AcademicYearID = '".$this->schoolYearID."'
						And
						u.RecordType = '2'
						$conds_YearID
						$conds_excludeUserID
						$conds_UserLogin
						$conds_ClassName
						$conds_ClassNumber
						$conds_StudentName
						$conds_ClassTeacher
				Order By
						yc.Sequence, ycu.ClassNumber
				";
		return $this->returnArray($sql);
		
    }
    
    function Get_Display_Mark_By_Grading_Scheme($Mark,$Grade,$GradingScheme)
    {
		$ScaleDisplay = $GradingScheme['ScaleDisplay'];
		
		$Grade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $Grade!='' )? $Grade : "";
		$Mark = ($ScaleDisplay=="M" && $Mark=='') ? $Mark : "";
		
		$Mark = ($ScaleDisplay=="M" && strlen($Mark)) ? $Mark : $Grade;

		return $Mark;
    }
    
    	# Sort Function for Master Report
	function Sort_UserID_By_Class_ClassNumber($StudentList)
	{
		# Build Assoc Array $ClassStudentList["YearClassID"]["UserID"] = $StudentInfo
		$ClassStudentList = BuildMultiKeyAssoc((array)$StudentList, array("YearClassID","UserID"));
		
		$SortedFormStudentList= array();
		foreach((array)$ClassStudentList as $ClassID => $thisStudentList)
		{
			sortByColumn2($thisStudentList,"ClassNumber");
			$SortedFormStudentList = array_merge($SortedFormStudentList,$thisStudentList);
		}
		$SortedUserIDList = Get_Array_By_Key($SortedFormStudentList,"UserID");
		
		return $SortedUserIDList;
	}
	
	function Sort_UserID_By_Position($FormOrClass,$StudentList,$MarksAry,$GrandMarksAry,$SubjectID)
	{
		$GrandFieldArr = $this->Get_SubjectID_Grand_Field_Arr();
		foreach((array)$StudentList as $StudentInfo)
		{
			$YearClassID = $StudentInfo["YearClassID"];
			$StudentID = $StudentInfo["UserID"];
			if($SubjectID>0) // subject mark 
			{
				if($MarksAry[$StudentID][$SubjectID][0]["OrderMerit$FormOrClass"]<0)
				{
					$ExcludedRankStudent[] = $StudentID;
					continue;
				}
				$UserSubjectOrderArr[$StudentID]["RankDetermineField"] = $MarksAry[$StudentID][$SubjectID][0]["OrderMerit$FormOrClass"];
				$UserSubjectOrderArr[$StudentID]["YearClassID"] = $YearClassID;
			}
			else
			{
				$GrandField = $GrandFieldArr[$SubjectID];

				if($GrandMarksAry[$StudentID][0][$GrandField]<0)
				{
					$ExcludedRankStudent[] = $StudentID;
					continue;
				}
				$UserSubjectOrderArr[$StudentID]["RankDetermineField"] = $GrandMarksAry[$StudentID][0][$GrandField];
				$UserSubjectOrderArr[$StudentID]["YearClassID"] = $YearClassID;
			}
		}
		
		if(count($UserSubjectOrderArr)>0)
		{
			# sort by class first
			sortByColumn2($UserSubjectOrderArr,"YearClassID",0,1);

			$reverse = $SubjectID>0?0:1;
			sortByColumn2($UserSubjectOrderArr,"RankDetermineField",$reverse,1);
		}
				
		$SortedUserIDList = array_keys((array)$UserSubjectOrderArr);
		$SortedUserIDList = array_merge((array)$SortedUserIDList,(array)$ExcludedRankStudent);
		
		return $SortedUserIDList;
		
	}

	# Get array (Below1Subject,Conduct,Late) for Trial Promotion
	function Get_Student_Trial_Promotion_Status($ParReportID,$ParStudentIDArr)
	{	
		$returnArray = array();
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ParReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$YearTermID = $ReportInfoArr['Semester'];
		$YearTermID = ($YearTermID=='F')? 0 : $YearTermID;

		$FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);		
		$WebSAMSCodeArr = $this->Get_Subject_WebSAMSCode_Config_Array();
		$SubjectCodeIDAssoArr = $this->GET_SUBJECTS_CODEID_MAP($withComponent=0, $mapByCode=1);		
		$MarkArr = $this->getMarks($ParReportID, '', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID=0);				
		$OtherInfoDataArr = $this->getReportOtherInfoData($ParReportID);
		
		for ($i=0,$i_MAX=count($ParStudentIDArr);$i<$i_MAX;$i++) {
			$thisStudentID = $ParStudentIDArr[$i];
			$thisConduct = $OtherInfoDataArr[$thisStudentID][$YearTermID]['Conduct'];
			$thisLate = $OtherInfoDataArr[$thisStudentID][$YearTermID]['Late']+$OtherInfoDataArr[$thisStudentID][$YearTermID]['Sick Leave']+$OtherInfoDataArr[$thisStudentID][$YearTermID]['Truant'];		
			
			$MarkNaturnArr = $this->Get_Subject_Mark_Nature_Arr($ParReportID, $ClassLevelID, $MarkArr[$thisStudentID]); 
			
			$thisConditionPassArr = array();
			if( $FormNumber == 1 || $FormNumber == 2 || $FormNumber == 3 ) {	//if Form 1, Form 2 and Form 3
				
			    $condition1_Count=0;			    
			    $condition2_Count=0;
			    $SubjBelowOneArr=array();
			    
			 
				foreach((array)$WebSAMSCodeArr as $_subjectName=>$thisWebSAMSCode)
				{				
					$thisSubjectID = $SubjectCodeIDAssoArr[$thisWebSAMSCode];
					$Grade=$MarkArr[$thisStudentID][$thisSubjectID][0]['Grade'];					
					
					if($Grade<=1 && $Grade!=null)
					{	
						if($Grade==1)	
						{
							if($_subjectName=='Chinese' ||$_subjectName=='English' ||$_subjectName=='Maths' ||$_subjectName=='LS')//conditon 1
								$condition1_Count++;						
						}								
						$SubjBelowOneArr[$_subjectName] =$thisSubjectID;
						$condition2_Count++;
					}
				}
				
				$thisConditionPassArr[] = $condition1_Count>=3; //condition 1: 中,英,數,通識四科中其中三科 1
				$thisConditionPassArr[] = $condition2_Count>=4; //condition 2: 任何四科 1或以下
				
			}
			else if( $FormNumber == 4) {	//if Form 4
				
				$EnglishBelowOne = false;
			    $condition2_Count=0;
			    $SubjBelowOneArr=array();
			    
				foreach((array)$WebSAMSCodeArr as $_subjectName=>$thisWebSAMSCode)
				{				
					$thisSubjectID = $SubjectCodeIDAssoArr[$thisWebSAMSCode];
					$Grade=$MarkArr[$thisStudentID][$thisSubjectID][0]['Grade'];					
					
					if($Grade<=1 && $Grade!=null)
					{			
						if($_subjectName=='English')	
						{
							$EnglishBelowOne=true;
						}	
						else if($_subjectName=='Chinese' ||$_subjectName=='Maths' ||$_subjectName=='LS')
						{
							$condition2_Count++;
						}											
						$SubjBelowOneArr[$_subjectName] =$thisSubjectID;
					}	
				}
				
				$thisConditionPassArr[] = $EnglishBelowOne==true; //condition 1: 英文1或以下
				$thisConditionPassArr[] = $condition2_Count>=2; //condition 2: 中,數,通識任何兩科 1或以下
				
			}
			else if( $FormNumber == 5  ) {	//if Form 5

			    $condition1_Count=0;
			    $condition2_Count=0;
			    $condition3_Count=0;
			    $SubjBelowOneArr=array();
			    
				foreach((array)$WebSAMSCodeArr as $_subjectName=>$thisWebSAMSCode)
				{				
					$thisSubjectID = $SubjectCodeIDAssoArr[$thisWebSAMSCode];
					$Grade=$MarkArr[$thisStudentID][$thisSubjectID][0]['Grade'];
					$thisMarkNature = $MarkNaturnArr[$thisSubjectID];
					$thisSubjIsPass = $this->Check_MarkNature_Is_Pass($thisMarkNature);
										
					
					$ChiPass=false;
					$EngPass=false;
					$MathPass=false;
					$LSPass=false;
					
					if ($_subjectName=='Chinese'&& ($thisSubjIsPass==true))
					{
						$ChiPass=true;
					}
					else if ($_subjectName=='English'&& ($thisSubjIsPass==true))
					{
						$EngPass=true;
					}
					
					else if ($_subjectName=='Maths'&& ($thisSubjIsPass==true))
					{
						$MathPass=true;
					}
					
					else if ($_subjectName=='LS'&& ($thisSubjIsPass==true))
					{
						$LSPass=true;
					}
					
					
					if($Grade<=1 && $Grade!=null)
					{			
						if($_subjectName=='English' || $_subjectName=='Chinese')//condition 1: 英,中任何一科取得1或以下	
						{						
							$condition1_Count++;
						}	
						if($_subjectName=='Maths' || $_subjectName=='LS')//condition 2: 數,通識均取1或以下	
						{						
							$condition2_Count++;
						}	
						else
						{
							$condition3_Count++; //condition 3: 選修兩科取1或以下
						}
						$SubjBelowOneArr[$_subjectName] =$thisSubjectID;																
					}	
				}
				$condition1_IsFulfill=false;
				if($condition1_Count>=1) //condition 1: 英,中任何一科取得1或以下
				{
					$condition1_IsFulfill=true;
				}
				
				$condition2_IsFulfill=false;
				if($ChiPass==true && $EngPass==true && $condition2_Count>=2) //condition 2: 中,英已合格,但數,通識兩均取1或以下
				{					
					$condition2_IsFulfill=true;
				}
				
				$condition3_IsFulfill=false;
				if($ChiPass==true && $EngPass==true && ($MathPass==true || $LSPass==true)&&$condition3_Count>=2) //condition 3:中,英已合格, 數,通識其中一科已合格, 但選修兩科均取1或以下
				{					
					$condition3_IsFulfill=true;
				}
				
				$thisConditionPassArr[] = $condition1_IsFulfill==true;//condition 1: 英,中任何一科取得1或以下
				$thisConditionPassArr[] = $condition2_IsFulfill==true;//condition 2: 中,英已合格,但數,通識兩均取1或以下
				$thisConditionPassArr[] = $condition3_IsFulfill==true;//condition 3:中,英已合格, 數,通識其中一科已合格, 但選修兩科均取1或以下
				

			}	// End if $FormNumber
			
			$thisConditionPassArr[] = strtoupper($thisConduct)=='C'||strtoupper($thisConduct)=='D'||strtoupper($thisConduct)=='E';//condition: 品行B或C
			$thisConditionPassArr[] = $thisLate>=15; //condition: 缺席,遲到,曠課次數達15次或以上
			
			if (in_array(true, (array)$thisConditionPassArr)) {
				
				$SubjNameBelowOneArr = array();
				foreach((array)$SubjBelowOneArr as $_subjectName=>$_subjectID)
				{
					$SubjNameBelowOneArr[] = $this->GET_SUBJECT_NAME_LANG($_subjectID);
				}
				
				$SubjectStr = implode(", ",(array)$SubjNameBelowOneArr);
			
				$returnArray[$thisStudentID]['Below1Subject']=$SubjectStr;
				$returnArray[$thisStudentID]['Conduct']=$thisConduct;
				$returnArray[$thisStudentID]['Late']=$thisLate;
			}
			
		}
		//debug_pr($returnArray);
		return $returnArray;
	}
    
    protected function Get_Trial_Promotion_Report_Header() {
    	global $eReportCard;
    	
    	$returnArray = array();
    	
    	$returnArray[] = array("Property" => 1,
								"En" => $eReportCard['TrialPromotion']['trialCSVheader']['En']['Class'],
								"Ch" => $eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Class']
								);
																	
		$returnArray[] = array("Property" => 1,
								"En" => $eReportCard['TrialPromotion']['trialCSVheader']['En']['ClassNo'],
								"Ch" => $eReportCard['TrialPromotion']['trialCSVheader']['Ch']['ClassNo']
								);
										
		$returnArray[] = array("Property" => 1,
								"En" => $eReportCard['TrialPromotion']['trialCSVheader']['En']['Student'],
								"Ch" => $eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Student']
								);
										
		$returnArray[] = array("Property" => 1,
								"En" => $eReportCard['TrialPromotion']['trialCSVheader']['En']['BelowOneSubject'],
								"Ch" => $eReportCard['TrialPromotion']['trialCSVheader']['Ch']['BelowOneSubject']
								);
		$returnArray[] = array("Property" => 1,
								"En" => $eReportCard['TrialPromotion']['trialCSVheader']['En']['Conduct'],
								"Ch" => $eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Conduct']
								);
											
		$returnArray[] = array("Property" => 1,
								"En" => $eReportCard['TrialPromotion']['trialCSVheader']['En']['LateAbsTruantTotal'],
								"Ch" => $eReportCard['TrialPromotion']['trialCSVheader']['Ch']['LateAbsTruantTotal']
								);
		return $returnArray;
    }

    function Get_Master_Report_Preset_List()
    {
    	$sql = "
			SELECT 
				PresetID,
				PresetName
			FROM
				".$this->DBName.".RC_MASTER_REPORT_PRESET
		";
		
		$result =$this->returnArray($sql);
		foreach($result as $row)
		{
			$returnArray[$row[0]] = $row[1]; 
		}
		 
		return $returnArray;
    }
    
    function Get_Master_Report_Preset_Value($PresetID)
    {
    	$sql = "
			SELECT 
				PresetValue
			FROM
				".$this->DBName.".RC_MASTER_REPORT_PRESET
			WHERE
				PresetID = '$PresetID'	
		";
		
		$returnArray = $this->returnVector($sql);
		
		return unserialize($returnArray[0]);
    }
    
    function Insert_Master_Report_Preset($PresetName,$ValueStr)
    {
    	$sql = "
			INSERT INTO 
				".$this->DBName.".RC_MASTER_REPORT_PRESET
				(
					PresetName,
					PresetValue,
					DateInput,
					DateModified
				)
				VALUES
				(
					'".$PresetName."',
					'".$ValueStr."',
					NOW(),
					NOW()
				)
			";	
			
		$result = $this->db_db_query($sql);
		return $result;
    }
    
    function Update_Master_Report_Preset($PresetID, $ValueStr)
    {
    	$sql = "
			UPDATE 
				".$this->DBName.".RC_MASTER_REPORT_PRESET
			SET
				PresetValue = '".$ValueStr."',
				DateModified = NOW()  			
			WHERE
				PresetID = '".$PresetID."'
		";
		$result = $this->db_db_query($sql);
		return $result;
    }
    
    function Delete_Master_Report_Preset($PresetID)
    {
    	$sql ="
			DELETE FROM
				".$this->DBName.".RC_MASTER_REPORT_PRESET
			WHERE
				PresetID = '".$PresetID."'
		";
		
		$result = $this->db_db_query($sql);
		return $result;
		
    }
    
    function Get_ReportColumn_Subject_Student_Number($ReportID)
    {
    	$SpecialCase1 = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
    	$SpecialCase2 = $this->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
    	$SpecialCaseRawAry = array_merge((array)$SpecialCase1,(array)$SpecialCase2);

    	foreach((array)$SpecialCaseRawAry as $SpecialCase)
    		$SpcialCaseAry[] = "'".$SpecialCase."'";
    		
    	$SpecialCaseSql = implode(",",(array)$SpcialCaseAry);	
    	
    	$sql ="
			SELECT
				ReportColumnID, 
				SubjectID, 
				COUNT(IF(Grade IN ($SpecialCaseSql),NULL,1)) as StudentNo
			FROM 
				".$this->DBName.".RC_REPORT_RESULT_SCORE 
			WHERE 
				ReportID = '$ReportID' 
			GROUP BY 
				ReportColumnID, SubjectID
		";
		
		$StudentNumberArr = $this->returnArray($sql);
		$returnArr = BuildMultiKeyAssoc($StudentNumberArr, array("SubjectID","ReportColumnID"), "StudentNo");
		return $returnArr; 
    }
    
    function CheckAnySubjectPositionDisplay($ReportID)
    {
    	global $intranet_session_language;
    	
    	$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
    	
    	$SubjectArr = $this->returnSubjectIDwOrder($ClassLevelID);
    	$SubjectStr = implode(",",(array)$SubjectArr);
    	
    	$sql = "
			SELECT 
				RecordType,
				MAX(ShowPosition) AS ShowPosition
			FROM
				".$this->DBName.".RC_POSITION_DISPLAY_SETTING
			WHERE
				SubjectID IN ($SubjectStr)
				AND ReportID = '$ReportID' 
			GROUP BY
				RecordType
		";
		
		
		$result = $this->returnArray($sql);
		$result = BuildMultiKeyAssoc($result,"RecordType");
		
		return array($result["Class"]["ShowPosition"],$result["Form"]["ShowPosition"]);
    }
    
    function Get_Template_Type_With_Semester_NameField($TemplateTablePrefix, $TermTablePrefix, $ConsolidateTermPrefix, $TermSeparator='', $InfoSeparator='')
    {
    	global $eReportCard;
    	
    	if ($TermSeparator=='')
    		$TermSeparator = ', ';
    		
    	if ($InfoSeparator=='')
    		$InfoSeparator = '<br />';
    		
    	$YearTermNameField = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
    	$x = "If (".$TemplateTablePrefix."Semester = 'F',
					Concat(	'".$eReportCard['WholeYearReport']."', 
							'".$InfoSeparator."(',
							GROUP_CONCAT(
								".$ConsolidateTermPrefix.$YearTermNameField."
								ORDER BY ayt_c.TermStart
								SEPARATOR '".$TermSeparator."'
							),
							')'
					),
					Concat(	If (".$TemplateTablePrefix."isMainReport = 1, '".$eReportCard['Main']."', '".$eReportCard['Extra']."'),
							' ".$eReportCard['TermReport']."',
							'".$InfoSeparator."(',
							".$TermTablePrefix.$YearTermNameField.",
							')'
					)
				)
				";
		return $x;
    }
    
    function Format_Mark_With_Style($ReportID, $SubjectID, $Mark, $Grade,$ClassLevelID='', $thisSubjectWeight='',$ScaleDisplay='',$UseWeightedMark='',$ReportColumnID='')
    {
    	
    	if(trim($ClassLevelID)=='')
    	{
    		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
    		$ClassLevelID = $ReportSetting['ClasslevelID'];
    	}
    	
    	if(trim($UseWeightedMark)=='')
    	{
//			global $CalSetting;
//			if(!isset($CalSetting))
			$CalSetting = $this->LOAD_SETTING("Calculation");
			$UseWeightedMark = $CalSetting['UseWeightedMark'];
    	}
    	
    	
    	if(trim($UseWeightedMark)=='')
    	{
    		global $GlobalSubjectWeightData;
    		if(!isset($GlobalSubjectWeightData[$ReportID]))
    		{
    			$AllSubjectWeightData = $this->returnReportTemplateSubjectWeightData($ReportID);
    			$GlobalSubjectWeightData[$ReportID] = BuildMultiKeyAssoc($AllSubjectWeightData, array("ReportColumnID","SubjectID"));
    		}
    		$thisSubjectWeightData = $GlobalSubjectWeightData[$ReportID][$ReportColumnID][$SubjectID];
    		$thisSubjectWeight = $thisSubjectWeightData['Weight'];
    	}
    	
    	if(trim($ScaleDisplay)=='')
    	{
    		$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0, $ReportID );
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
    	}
    	
    	list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $Mark, $Grade);
		
		if($needStyle)
		{
			if ($thisSubjectWeight>0 && $ScaleDisplay=="M")
			{
				$thisMarkTemp = ($UseWeightedMark && $thisSubjectWeight!=0) ? $thisMark/$thisSubjectWeight : $thisMark;
			}
			else
			{
				$thisMarkTemp = $thisMark;
			}
			
			$thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisMarkTemp);
		}
		else
		{
			$thisMarkDisplay = $thisMark;
		}
		
		return $thisMarkDisplay;
    }
    
    function Get_Subject_Passing_Mark($SubjectID, $ReportID)
    {
    	$PreloadArrKey = 'SubjectPassingMarkArr';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
    	$ReportInfo = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfo['ClassLevelID'];
		
		$GrandGradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, $withGrandResult=1, $returnAsso=0, $ReportID);
		$SchemeID = $GrandGradingSchemeArr['SchemeID'];
		
		$SchemeInfoArr = $this->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
		$TopPercentage = $SchemeInfoArr['TopPercentage'];
		
		if ($TopPercentage == 0)
		{
			# Mark Range
			$SchemeRangeInfoArr = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID, $Nature="P");
			$LowerLimitArr = Get_Array_By_Key((array)$SchemeRangeInfoArr, 'LowerLimit');
			$PassingMark = (count((array)$LowerLimitArr) > 0)? min((array)$LowerLimitArr) : 0;
		}
		else if ($TopPercentage == 1 || $TopPercentage == 2)
		{
			# Percentage Range
			$PassingMark = $SchemeInfoArr['PassMark'];
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $PassingMark);
		return $PassingMark;	
    }
    
    ### General => Class Teacher Comment
    ### Holy Trinity => Customized Comment Display
    function Get_Report_Remarks($ReportID, $StudentID, $IncludePromotion=1)
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID); 
		
		$RemarkArr = array();
		
		# teacher comment
		$CommentAry = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		$classteachercomment = $CommentAry[0];
		$RemarkArr[] = nl2br($classteachercomment);
		
		return $RemarkArr;
	}
    
    function Write_Log($row, $file='')
	{
		global $PATH_WRT_ROOT,$log_file;
		
		if(empty($log_file))
			$log_file = $PATH_WRT_ROOT."/file/reportcard2008/".date("Ymdhis").$file.".txt";
		
		$x = (($fd = fopen($log_file, "a+")) && (fputs($fd, $row."\n"))) ? 1 : 0;
	    fclose ($fd);
	    return $x;
	}
	
	//20110408
	function Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $ParUserID, $ClassID, $ParentSubjectID, $SubjectID)
	{
		if ($this->IS_ADMIN_USER($ParUserID) == true || $this->Is_Class_Teacher($ParUserID, $ClassID) == true)
		{
			# Admin / Class Teacher => Show all students
			$TaughtStudentList = '';
		}
		else
		{
			# Subject Teacher => Show taught students only
			$TargetSubjectID = ($ParentSubjectID != '')? $ParentSubjectID : $SubjectID;
			$TaughtStudentArr = $this->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $ParUserID, $ClassID, $TargetSubjectID);
			$TaughtStudentList = '';
			if (count($TaughtStudentArr > 0))
				$TaughtStudentList = implode(',', $TaughtStudentArr);
		}
		$StudentArr = $this->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1, 0, 1);
		
		return $StudentArr;
	}
	
	function Update_Marksheet_Feedback($ReportID, $SubjectID, $StatusArr)
	{
		if(empty($ReportID)||empty($SubjectID)||empty($StatusArr))
			return array();
			
		$StudentList = array_keys($StatusArr);
		$MarksheetFeedback = $this->Get_Marksheet_Feedback_Info($ReportID, $SubjectID, $StudentList);
		$ExistStudentFeedBack = Get_Array_By_Key($MarksheetFeedback,"StudentID");
		
		foreach($StudentList as $thisStudentID)
		{
			if(in_array($thisStudentID,$ExistStudentFeedBack))
				$UpdateStatus[$StatusArr[$thisStudentID]][] = $thisStudentID;
			else
				$InsertStatus[$StatusArr[$thisStudentID]][] = $thisStudentID;
		}
		
		$this->Start_Trans();
		# Update if record exist
		$RC_MARKSHEET_FEEDBACK = $this->DBName.".RC_MARKSHEET_FEEDBACK";
		if(count($UpdateStatus)> 0 )
		{
			foreach($UpdateStatus as $Status => $StudentIDArr)
			{
				$sql = "	
					UPDATE 
						$RC_MARKSHEET_FEEDBACK
					SET
						RecordStatus = '$Status',
						DateModified = NOW(),
						LastModifiedBy = '".$_SESSION['UserID']."'
					WHERE 
						ReportID = '$ReportID'
						AND SubjectID = '$SubjectID'
						AND StudentID IN (".implode(",",(array)$StudentIDArr).")
					";
					
					$Success['UpdateArr'][] = $this->db_db_query($sql); 
			}			
		}	
		
		# Update if record not exist
		if(count($InsertStatus)> 0 )
		{
			foreach($InsertStatus as $Status => $StudentIDArr)
			{
				
				$ValueArr = array();
				foreach($StudentIDArr as $thisStudentID)
				{
					$ValueArr[] = "('$SubjectID','$ReportID','$thisStudentID','$Status','".$_SESSION['UserID']."',NOW(),NOW())";
				}
				$ValueSql = implode(",",(array)$ValueArr);
				$sql = "	
					INSERT INTO $RC_MARKSHEET_FEEDBACK
						(SubjectID, ReportID , StudentID, RecordStatus, LastModifiedBy, DateInput, DateModified)
					VALUES
						$ValueSql				
					";
					
				$Success['InsertArr'][] = $this->db_db_query($sql); 
				
			}	
		}
		
		if(in_array(false,(array)$Success['UpdateArr']) || in_array(false,(array)$Success['InsertArr']))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
	}
 
 	function Get_User_Accessible_Form ()
	{
		global $ck_ReportCard_UserType, $UserID;
		
		$IsAdmin = $this->Is_Admin_User($UserID);
		
		if ($IsAdmin)
		{
			$FormArr = $this->GET_ALL_FORMS();
		}
		else if($ck_ReportCard_UserType=="TEACHER")
		{
			$FormArr = $this->returnClassTeacherForm($_SESSION['UserID']);
		}
		
		return $FormArr;
	}
	
	function Get_User_Accessible_Class ($ClassLevelID='')
	{
		global $ck_ReportCard_UserType, $UserID;
		
		$IsAdmin = $this->Is_Admin_User($UserID);
		if ($IsAdmin )
		{
			$ClassArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
		}
		else if($ck_ReportCard_UserType=="TEACHER")
		{
			$ClassArr = $this->returnClassTeacherClass($_SESSION['UserID'],$ClassLevelID);
		}
		
		return $ClassArr;
		
	}
	
	
	function Get_User_Accessible_Subject($YearTermIDArr='')
	{
		$PreloadArrKey = 'Get_User_Accessible_Subject';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		global $PATH_WRT_ROOT;
		
		$IsAdmin = $this->Is_Admin_User();
		if ($IsAdmin)
		{
			// Get all Subjects for Admin
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$libSubject = new subject();
			$SubjectArr = $libSubject->Get_Subject_List();
		}
		else
		{
			// Get Teaching Subject only for Normal Teacher
			$SubjectArr = $this->Get_User_Teaching_Subject($YearTermIDArr);
		}
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $SubjectArr);
		return $SubjectArr;
	}
    
	function Get_User_Teaching_Subject ($YearTermIDArr='')
	{
		$PreloadArrKey = 'Get_User_Teaching_Subject';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	
		if ($YearTermIDArr != '')
			$conds_YearTermID = " And ayt.YearTermID In (".implode(',', (array)$YearTermIDArr).") ";
			
		$sql = "Select
						Distinct(st.SubjectID)
				From
						SUBJECT_TERM_CLASS_TEACHER as stct
						Inner Join
						SUBJECT_TERM as st On (stct.SubjectGroupID = st.SubjectGroupID)
						Inner Join
						ACADEMIC_YEAR_TERM as ayt On (st.YearTermID = ayt.YearTermID)
				Where
						stct.UserID = '".$_SESSION['UserID']."'
						And
						ayt.AcademicYearID = '".$this->AcademicYearID."'
						$conds_YearTermID
				";
		$ReturnArr = $this->returnArray($sql, null, $ReturnAssoOnly=1);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
		return $ReturnArr;
	}
	
	function Get_Student_Selection_By_Class($ID_Name, $YearClassIDArr, $SelectedStudentID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=0, $ExcludedStudentIDArr='')
	{
		include_once("form_class_manage_ui.php");
		$lib_formClassUI = new form_class_manage_ui();
		$ReturnValue = $lib_formClassUI->Get_Student_Selection($ID_Name, $YearClassIDArr, $SelectedStudentID, $Onchange, $noFirst, $isMultiple, $isAll, $ExcludedStudentIDArr);
		
		return $ReturnValue;
	}
	
	function Get_Semester_Seq_Number($SemID, $AcademicYearID='')
	{
		include_once("form_class_manage.php");
		
		if(trim($AcademicYearID)=='')
			$AcademicYearID = $this->schoolYearID;
		
		$ay = new academic_year($AcademicYearID);
		$TermList = $ay->Get_Term_List();
		$TermIDList = array_keys($TermList);
		
		if(!in_array($SemID,$TermIDList))
			return false;
		else
			return array_search($SemID, $TermIDList)+1;
		
	}
	
	function Add_SubjectID_In_GrandMarkArr($GrandMarkArr) {
		$SubjectID = 0;
		$MarkArr = array();
		foreach ((array)$GrandMarkArr as $thisStudentID => $thisStudentMarkArr) {
			foreach ((array)$thisStudentMarkArr as $thisReportColumnID => $thisStudentColumnMarkArr) {
				$MarkArr[$thisStudentID][$SubjectID][$thisReportColumnID] = $thisStudentColumnMarkArr;
			}
		}
		return $MarkArr;
	}
	
	function Check_MarkNature_Is_Pass($MarkNature) {
		if ($MarkNature=='Distinction' || $MarkNature=='Pass') {
			$isPass = true;
		}
		else {
			$isPass = false;
		}
		return $isPass;
	}
	
	public function Get_Marksheet_Estimate_Score_Symbol() {
		return $this->MS_EstimateScoreSymbol;
	}
	
	public function Is_Estimated_Score($Score) {
		$SymbolPosition = stripos($Score, $this->Get_Marksheet_Estimate_Score_Symbol());
		
		return (is_numeric($SymbolPosition))? true : false;
	}
	
	public function Remove_Estimate_Score_Symbol($Score) {
		return str_replace($this->Get_Marksheet_Estimate_Score_Symbol(), '', $Score);
	}
	
	public function Get_Estimated_Score_Display($Score) {
		if ($Score == 'N.A.') {
			$EstimatedScore = $this->Get_Marksheet_Estimate_Score_Symbol();
		}
		else {
			$EstimatedScore = $Score.$this->Get_Marksheet_Estimate_Score_Symbol();
		}
		return $EstimatedScore;
	}
	
	public function Convert_Marksheet_UIScore_To_DBRawData($RawData) {
		$IsEstimated = ($this->Is_Estimated_Score($RawData))? 1 : 0;
		
		$RawDataDB = $this->Remove_Estimate_Score_Symbol($RawData);
		if ($IsEstimated == 1 && $RawDataDB == '') {
			$RawDataDB = 'N.A.';
		}
		
		return $RawDataDB;
	}
	
	private function Get_Subject_WebSAMSCode_Config_Array() {
		global $eRCTemplateSetting;
		return $eRCTemplateSetting['SubjectWebSAMSCodeArr'];
	}
	
	public function Get_Subject_WebSAMSCode_From_Config($SubjectName) {
		$SubjectWebSAMSCodeConfigArr = $this->Get_Subject_WebSAMSCode_Config_Array();
		return $SubjectWebSAMSCodeConfigArr[$SubjectName];
	}
	
	public function Get_Class_Teacher_Comment_MaxLength() {
		global $eRCTemplateSetting;
		
		$MaxLength = 255;
		if ($eRCTemplateSetting['UnlimitClassTeacherComment']) {
			$MaxLength = -1;
		}
		else if ($eRCTemplateSetting['TextAreaMaxChar']) {
			$MaxLength = $eRCTemplateSetting['TextAreaMaxChar'];
		}
		
		return $MaxLength;
	}
	
	public function Get_Subject_Teacher_Comment_MaxLength() {
		global $eRCTemplateSetting;
		
		$MaxLength = 255;
		if ($eRCTemplateSetting['UnlimitSubjectTeacherComment']) {
			$MaxLength = -1;
		}
		else if ($eRCTemplateSetting['TextAreaMaxChar_SubjectTeacherComment']) {
			$MaxLength = $this->textAreaMaxChar_SubjectTeacherComment;
		}
		else if ($eRCTemplateSetting['TextAreaMaxChar']) {
			$MaxLength = $this->textAreaMaxChar;
		}
		
		return $MaxLength;
	}
	
	public function Get_Principal_Info() {
		$PreloadArrKey = 'Get_Principal_Info';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
    	
    	global $intranet_db;
    	$sql = "Select UserID, EnglishName, ChineseName From $intranet_db.INTRANET_USER Where lower(TitleEnglish) = 'principal'";
    	$ReturnArr = $this->returnArray($sql);
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
		return $ReturnArr;
	}
	
	/***************************************************
	 * It converts the class of form(A, B, C.....)  into Integer (based on ASCII, and minus 64).Example, F.9A, A=65-64=1
	 * 
	 * @owner: Connie 20111026
	 * @param: string $StudentID
	 * @return:  $Class_of_Form_no  (1,2,3....)
	 * 
	 ***************************************************/
	public function Convert_ClassOfForm_ToInt($StudentID)
	{
		$StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
		
		$ClassNameEn = $StudentInfoArr[0]['ClassName'];
		$Class_of_Form = strtoupper(substr($ClassNameEn, strlen($ClassNameEn)-1, 1));
		$Class_of_Form_no = ord($Class_of_Form)-64;
		
		return $Class_of_Form_no;
					
	}
	
	protected function Get_Preload_Result($ArrayKey, $FunctionArgumentArr) {
		$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
    	$PreloadInfoArrExist = $this->Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr);
    	
    	$returnAry = array();
    	if ($PreloadInfoArrExist)
    	{
    		eval('$returnAry = '.$PreloadInfoArrVariableStr.';');
    		return $returnAry;
    	}
    	else {
    		return false;
    	}
	}
	
	protected function Set_Preload_Result($ArrayKey, $FunctionArgumentArr, $ResultArr) {
		$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
		eval($PreloadInfoArrVariableStr.' = $ResultArr;');
	}
	
	private function Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr) {
		$PreloadInfoArrVariableStr = '$this->PreloadInfoArr[\''.$ArrayKey.'\']';
    	foreach( (array)$FunctionArgumentArr as  $arg_key => $arg_val)
    	{
    		if (is_array($arg_val)) {
    			$key_name = implode(',', $arg_val);
    		}
    		else {
    			$key_name = trim($arg_val);
    		}
    		$PreloadInfoArrVariableStr .= "['".addslashes($key_name)."']";
    	}
    	
    	return $PreloadInfoArrVariableStr;
	}
	
	private function Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr) {
		$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
		
		$PreloadInfoArrExist = false;
    	eval('$PreloadInfoArrExist = isset('.$PreloadInfoArrVariableStr.');');
    	
    	return $PreloadInfoArrExist;
	}
	
	# Other Information
	function Update_Other_Info_Data($TermID, $UploadType, $OtherInfoArr)
	{
		$ClassID = array_keys($OtherInfoArr);
		
		$Success["Delete"] = $this->Delete_Student_Existing_OtherInfo($TermID, $UploadType, $ClassID);
		$Success["Insert"] = $this->Insert_Student_OtherInfo($TermID, $UploadType, $OtherInfoArr);
		
		return !in_array(false, (array)$Success);	
			
	}
	
	function Delete_Student_Existing_OtherInfo($TermID, $UploadType, $ClassID)
	{
		
		$RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
		$sql = "
			DELETE FROM 
				$RC_OTHER_INFO_STUDENT_RECORD
			WHERE
				TermID = '$TermID'
				AND UploadType = '".$this->Get_Safe_Sql_Query($UploadType)."'
				AND ClassID IN (".implode(",",(array)$ClassID).")
		";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Insert_Student_OtherInfo($TermID, $UploadType, $ClassOtherInfoArr)
	{
		foreach((array)$ClassOtherInfoArr as $ClassID => $OtherInfoArr)
		{
			foreach((array)$OtherInfoArr as $StudentID => $StudentOtherInfo)
			{
				foreach((array)$StudentOtherInfo as $ItemCode => $OtherInfoVal)
				{
					if(trim($OtherInfoVal)!='')
					{
						$RecordSqlArr[] = "('$TermID','$UploadType','$StudentID','".$this->Get_Safe_Sql_Query($ItemCode)."','".$this->Get_Safe_Sql_Query(trim($OtherInfoVal))."', '$ClassID', NOW(),'".$_SESSION['UserID']."')";
					}
				}
			}
		}
		
		if(count($RecordSqlArr)>0)
		{
			$RecordSql = implode(",\n",$RecordSqlArr);
			
			$RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
			$sql = "
				INSERT INTO
					$RC_OTHER_INFO_STUDENT_RECORD
					(TermID, UploadType, StudentID, ItemCode, Information, ClassID, DateInput, InputBy)
				VALUES
					$RecordSql		
				"; 
			
			$success = $this->db_db_query($sql);
		}
		else
			$success = true;
		
		return $success;
	}
	
	function Get_Student_OtherInfo_Data($UploadType='', $TermID='', $ClassID='', $StudentID='', $ItemCode='', $YearID='')
	{
		if(trim($TermID)!='')
			$cond_TermID = " AND oisr.TermID IN (".implode(",",(array)$TermID).") ";
		if(trim($UploadType)!='')
			$cond_UploadType= " AND oisr.UploadType IN ('".implode("','",(array)$UploadType)."') ";
		if(trim($StudentID)!='')
			$cond_StudentID = " AND oisr.StudentID IN (".implode(",",(array)$StudentID).") ";
		if(trim($ItemCode)!='')
			$cond_ItemCode = " AND oisr.ItemCode IN ('".implode("','",(array)$ItemCode)."') ";
		if(trim($ClassID)!='')
			$cond_ClassID = " AND oisr.ClassID IN (".implode(",",(array)$ClassID).") ";
		if(trim($YearID)!='')
		{
			$cond_YearID = " AND yc.YearID IN (".implode(",",(array)$YearID).") ";
			$LEFT_JOIN_YEAR_CLASS = "LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = oisr.ClassID";
			$OrderByClass = " ORDER BY yc.Sequence ";
		}
		
		$RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
		$sql = "
			SELECT
				oisr.TermID, 
				oisr.UploadType, 
				oisr.StudentID, 
				oisr.ItemCode, 
				oisr.Information
			FROM
				$RC_OTHER_INFO_STUDENT_RECORD oisr
				$LEFT_JOIN_YEAR_CLASS
			WHERE
				1
				$cond_ClassID
				$cond_YearID
				$cond_TermID
				$cond_UploadType
				$cond_StudentID
				$cond_ItemCode
			$OrderByClass
				
		";
		
		$result = $this->returnArray($sql);

		return $result;
	}
	
	function Get_OtherInfo_Last_Modified_Info($UploadType, $TermID, $ClassID='')
	{
		if(trim($ClassID) != '')
			$cond_ClassID = " AND oisr.ClassID IN (".implode(",",(array)$ClassID).") ";
		
		$RC_OTHER_INFO_STUDENT_RECORD = $this->DBName.".RC_OTHER_INFO_STUDENT_RECORD";
		$NameField = getNameFieldByLang("iu.");
		$sql = "
			SELECT
				oisr.ClassID,
				$NameField NameField,
				oisr.DateInput
			FROM
				$RC_OTHER_INFO_STUDENT_RECORD oisr
				LEFT JOIN INTRANET_USER iu ON iu.UserID = oisr.InputBy 
			WHERE
				oisr.UploadType  = '$UploadType'
				AND oisr.TermID = '$TermID'
				$cond_ClassID
			GROUP BY
				oisr.ClassID
		";
		$result = $this->returnArray($sql);

		return $result;
	}

	function Get_Student_OtherInfo_Export_Data($UploadType, $TermID, $ClassID, $YearID)
	{
		global $ReportCard_Rubrics_CustomSchoolName;
		
		list($ExportHeader,$ExportHeaderCh) = $this->Get_OtherInfo_CSV_Header($UploadType,1);

		$OtherInfoConfig = $this->getOtherInfoConfig($UploadType);
		$NumOfConfig = sizeof($OtherInfoConfig);
		
		$StudentOtherInfo = $this->Get_Student_OtherInfo_Data($UploadType, $TermID, $ClassID, NULL, NULL, $YearID);
		$StudentIDArr = Get_Array_By_Key($StudentOtherInfo, "StudentID"); 
		$StudentOtherInfoAssocArr = BuildMultiKeyAssoc($StudentOtherInfo, array("StudentID", "ItemCode"),"Information",1);

		$ExportData = array();
		$ExportData[] = $ExportHeaderCh;
		
		foreach((array)$StudentOtherInfoAssocArr as $thisStudentID => $thisOtherInfo)
		{
			$thisStudentInfo = $this->Get_Student_Class_ClassLevel_Info($thisStudentID);
			
			// find out how many rows are needed to show duplicated records
			$maxRow = 0;
			for($i=5; $i<$NumOfConfig; $i++)
			{
				$ItemCode = $OtherInfoConfig[$i]['EnglishTitle'];
				
				$tmpOtherInfoArr = explode("\n",$thisOtherInfo[$ItemCode]);
				$maxRow = max($maxRow,count($tmpOtherInfoArr));
				$thisOtherInfoArr[$ItemCode] = $tmpOtherInfoArr;
			}
			
			// loop rows per student
			for($j=0;$j<$maxRow; $j++)
			{
				$ExportRow = array();
				
				$ExportRow[] = $thisStudentInfo[0]['ClassName'];
				$ExportRow[] = $thisStudentInfo[0]['ClassNumber'];
				$ExportRow[] = $thisStudentInfo[0]['WebSAMSRegNo'];
				$ExportRow[] = $thisStudentInfo[0]['UserLogin'];
				$ExportRow[] = $thisStudentInfo[0]['EnglishName'];
				
				// loop otherinfo per rows
				for($i=5; $i<$NumOfConfig; $i++)
				{
					$ExportRow[] = $thisOtherInfoArr[$OtherInfoConfig[$i]['EnglishTitle']][$j];
				}
				
				$ExportData[] = $ExportRow;
			}
		}
		
		return array($ExportHeader,$ExportData);
	}	

	function Get_OtherInfo_CSV_Header($UploadType, $IncludeChineseRow=0)
	{
		global $Lang;
		
		$OtherInfoConfig = $this->getOtherInfoConfig($UploadType);
		$NumOfConfig = sizeof($OtherInfoConfig);
		
		$fieldName = array();
		$chineseRow = array();
		
		for($i=0; $i<$NumOfConfig; $i++) {
			
			$fieldName[] = $OtherInfoConfig[$i]["EnglishTitle"];
			$chineseRow[0][] =  $OtherInfoConfig[$i]["ChineseTitle"];
		}
		$chineseRow[0][] = $Lang['General']['ImportArr']['SecondRowWarn'];// append (do not remove this row)
		
		if($IncludeChineseRow==1)
			return array($fieldName,$chineseRow[0]);
		else
			return $fieldName;
	}
	
	function Get_OtherInfo_Export_Filename($UploadType, $TermID, $ClassID='', $YearID='')
	{
		if(empty($ClassID))
		{
			$sql = "SELECT YearName FROM YEAR WHERE YearID='$YearID' ";
		}
		else
		{
			$ClassID = substr($ClassID, 2);
			$sql = "SELECT ClassTitleEn FROM YEAR_CLASS WHERE YearClassID='$ClassID' ";
		}
		$tmp = $this->returnVector($sql);
		$ClassTitle = $tmp[0];
		
		if($TermID==0)
			$TermName = "Whole Year";
		else
		{
			$sql = "SELECT YearTermNameEN FROM ACADEMIC_YEAR_TERM WHERE YearTermID = $TermID";
			$tmp = $this->returnVector($sql);
			$TermName = $tmp[0];
		}
	
		return str_replace(" ","_",$TermName."_".$ClassTitle."_".$UploadType).".csv";	
	}
}

?>