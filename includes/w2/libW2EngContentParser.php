<?php
//editing : fai

include_once ($PATH_WRT_ROOT."includes/libxml.php");
class libW2EngContentParser
{
	function parseChiStepData($stepContent,$step)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$stepData = $libXML->XML_ToArray($stepContent);
	
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $stepData[$step];
	}	
	function parseStepData($stepContent, $tagName)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$xml = simplexml_load_string( $stepContent );
		$numOfCol = count($xml);
		foreach( (array)$tagName['stepTag'] as $tagCotentName)
		{
			$stepData[$tagCotentName] = (string)$xml->$tagCotentName ;
		}
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $stepData;
	}
	function parseStep4Data($stepContent, $tagName)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');
		
		
		$libXML = new libXML();
		$xml = simplexml_load_string( $stepContent );
		
		
		$xml = $libXML->XML_ToArray($stepContent);
		$stepData[$tagName['stepTag'][0]] = $xml[$tagName['stepName']][$tagName['stepTag'][0]] ;
		
		foreach( (array)$xml[$tagName['stepName']][$tagName['stepTag'][1]][$tagName['stepTag'][2]] as $value)
		{
			$stepData[$tagName['stepTag'][1]][] =  $value;
		}
		//restore settings
		
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $stepData;
	}
	function parseResource($resource)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$xml = simplexml_load_string( $resource );

		$resourceData = $libXML->XML_ToArray($resource);
		
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $resourceData;
	}
	function parseStep3Data($Data)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$xml = simplexml_load_string( $Data );
		$returnData = $libXML->XML_ToArray($Data);

		ini_set('zend.ze1_compatibility_mode', $mode);
		return $returnData['step3'];
	}	
	
	function parseTeacherAttachment($attachment){
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$xml = simplexml_load_string( $attachment );
		$attachmentData = $libXML->XML_ToArray($attachment);
	
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $attachmentData;
	}	
	
	function parseEngContent($cid,$data=array())
	{
		list($sch_code,$scheme_num,$content_code) = $data;
		if($cid == '' && ($sch_code=='' || $scheme_num=='' || $content_code=='')){
			return false;
		}

		$objDB = new libdb();
		if(!empty($sch_code)&&!empty($scheme_num)&&!empty($content_code)){
			$sql= "SELECT * FROM W2_CONTENT_DATA WHERE schoolCode = '".$sch_code."' AND schemeNum = '".$scheme_num."' AND r_contentCode = '".$content_code."' ";
		}else{
			$sql= "SELECT * FROM W2_CONTENT_DATA WHERE cid='$cid'; ";
		}
		
		$rs = $objDB->returnResultSet($sql);

		if(count($rs) != 1){
			//should return one and only one result only
			return false;
		}

		$rs = $rs[0];
		$cid = $rs['cid'];
		$topicName = $rs['topicName'];
		$topicIntro = $rs['topicIntro'];
		$level = $rs['level'];
		$category = $rs['category'];
		$step1Content = $rs['step1Content'];
		$step2Content = $rs['step2Content'];
		$step3Content = $rs['step3Content'];
		$step4Content = $rs['step4Content'];
		$step5Content = $rs['step5Content'];
		$step6Content = $rs['step6Content'];		
		$resource = $rs['resource'];
		$r_contentCode = $rs['r_contentCode'];
		$schoolCode = $rs['schoolCode'];
		$schemeNum = $rs['schemeNum'];
		$status = $rs['status'];
		$dataInput = $rs['dataInput'];
		$createdBy = $rs['createdBy'];
		$dataModified = $rs['dataModified'];
		$modifiedBy = $rs['modifiedBy'];
		$powerConceptID = $rs['powerConceptID'];
		$powerBoardID = $rs['powerBoardID'];
		$attachment = $rs['attachment'];
		$conceptType = $rs['conceptType'];		
		$resource = $rs['resource'];
		$grade = $rs['grade']; 
		$teacherAttachment = $rs['teacherAttachment']; 
		$teacherAttachmentData = $this->parseTeacherAttachment($teacherAttachment);
		$resourceData = $this->parseResource($resource);
		if($r_contentCode=='eng'){
			$tagName = array('stepName' =>'step1',
				'stepTag'=>array('step1Int', 'step1TextType', 'step1Purpose', 'step1Content')
			);
			$step1Data = $this->parseStepData($step1Content,$tagName);
			
			$tagName = array('stepName' =>'step2',
				'stepTag'=>array('step2Int')
			);
			$step2Data = $this->parseStepData($step2Content,$tagName);
				
	//		$tagName = array('stepName' =>'step3',
	//			'stepTag'=>array('step3Int', 'step3SampleWriting', 'step3Grammar','step3Vocab')
	//		);
	//		$step3Data = $this->parseStepData($step3Content,$tagName);
			$step3Data = $this->parseStep3Data($step3Content);
			
			$tagName = array('stepName' =>'step4',
				'stepTag'=>array('step4Int', 'step4DraftQList','step4DraftQ')
			);
			$step4Data = $this->parseStep4Data($step4Content,$tagName);
			
			
			$tagName = array('stepName' =>'step5',
				'stepTag'=>array('step5Int', 'step5DefaultWriting')
			);
			$step5Data = $this->parseStepData($step5Content,$tagName);
	
			
			$resultArray = array('cid'=>$cid,'topicName'=>$topicName, 'topicIntro'=>$topicIntro, 'level'=>$level, 'category'=>$category,
					'step1Data'=>$step1Data, 'step2Data'=>$step2Data, 'step3Data'=>$step3Data, 'step4Data'=>$step4Data, 'step5Data'=>$step5Data,
					'r_contentCode'=>$r_contentCode, 'schoolCode'=>$schoolCode, 'schemeNum'=>$schemeNum,  'conceptType'=>$conceptType, 
						'powerConceptID'=>$powerConceptID,  'powerBoardID'=>$powerBoardID,  'attachment'=>$attachment, 
					'resourceData' =>$resourceData, 'status' => $status, 'grade' =>$grade, 'teacherAttachmentData'=>$teacherAttachmentData['TeacherAttachment']
			);
		}else{
			$step1Data = $this->parseChiStepData($step1Content,'step1');
			$step2Data = $this->parseChiStepData($step2Content,'step2');
			$step3Data = $this->parseChiStepData($step3Content,'step3');
			$step4Data = $this->parseChiStepData($step4Content,'step4');
			$step5Data = $this->parseChiStepData($step5Content,'step5');
			$step6Data = $this->parseChiStepData($step6Content,'step6');
			$resultArray = array('cid'=>$cid,'topicName'=>$topicName, 'topicIntro'=>$topicIntro, 'level'=>$level, 'category'=>$category,
					'step1Data'=>$step1Data, 'step2Data'=>$step2Data, 'step3Data'=>$step3Data, 'step4Data'=>$step4Data, 'step5Data'=>$step5Data, 'step6Data'=>$step6Data,
					'r_contentCode'=>$r_contentCode, 'schoolCode'=>$schoolCode, 'schemeNum'=>$schemeNum,  'conceptType'=>$conceptType, 
						'powerConceptID'=>$powerConceptID,  'powerBoardID'=>$powerBoardID,  'attachment'=>$attachment, 
					'resourceData' =>$resourceData, 'status' => $status, 'grade' =>$grade, 'teacherAttachmentData'=>$teacherAttachmentData['TeacherAttachment']
			);		
		}		
		return $resultArray;
	}
	
	/* Copy From generateEngScheme.php */
	function getResouceArrayReparsed(&$parseData)
	{
		global $Lang;
		$resourceList = $parseData['resourceData']['resource'];
		$stepNum = $parseData['r_contentCode']=='sci'?6:5;
		
		foreach( (array) $resourceList as $resourceItem)
		{
			if(!empty($resourceItem['resourceItemName'])&&!empty($resourceItem['resourceItemWebsite'])){
				if (!preg_match('/^http:\/\//', $resourceItem['resourceItemWebsite'])) 
				{
					$resourceItem['resourceItemWebsite'] = 'http://'.$resourceItem['resourceItemWebsite'];
				}
				$resourceItemHTML = "<a target='_blank' href='{$resourceItem[resourceItemWebsite]}'>".$resourceItem['resourceItemName'].'</a><br />';
				
				for ($i=0; $i<$stepNum; ++$i)
				{
					if ( $resourceItem["resourceItemChecked"]['c'.($i+1)] )
					{
						$parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'] .= $resourceItemHTML;		
					}
				}
			}
			
		}
		for($i = 0; $i < $stepNum; ++$i)
		{
			if($parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'])
			{
				$parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'] = '<br />'.$Lang['W2']['refWebsite'].':<br />'.$parseData['step'.($i+1).'Data']['step'.($i+1).'Resource'];
			}
		}
	}
}
?>