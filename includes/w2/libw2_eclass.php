<?
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

class libw2_eclass {
	private static $objInstance; 
	private static $isClassRoomExist; 
	private static $classRoomID;
	private static $classRoomDB;

	private function __construct() {
		$this->checkClassRoomID();
    }

	public static function getInstance()
	{
		if (!self::$objInstance)
		{
			self::$objInstance = new libw2_eclass();
		}

		return self::$objInstance;
	}  

	private function checkClassRoomID(){
		global $w2_cfg,$eclass_prefix;
		if(isset($this->isClassRoomExist)){
			//do nothing 
		}else{
			$classRoomID = getEClassRoomID($w2_cfg['w2ClassRoomType']);
			if(is_numeric($classRoomID) && $classRoomID > 0){
				$this->isClassRoomExist = true;
				$this->classRoomID = $classRoomID;
				$this->classRoomDB = $eclass_prefix.'c'.$classRoomID;
			}else{
				$this->isClassRoomExist = false;
			}
		}		
	}
	function getIsClassRoomExist(){
		return $this->isClassRoomExist;
	}
	function getClassRoomID(){
		return $this->classRoomID;
	}
	function getClassRoomDB(){
		return $this->classRoomDB;
	}
}
?>
