<?
// using: 

include_once($intranet_root."/includes/w2/libW2Content.php");

class libW2ContentFactory {
	public function __construct() {
		
    }
   
    public static function createContent($strContent) {
    	global $w2_cfg, $intranet_root;
	    	
		$strContent = strtoupper($strContent) ;

		$ContentObj = 'libW2Content'.$strContent;
		$libFile = $intranet_root.'/includes/w2/'.$ContentObj.'.php';
		if(file_exists($libFile)){
			include_once($libFile);
			return new $ContentObj();
		}else{
			return null;
		}
    }
}
?>