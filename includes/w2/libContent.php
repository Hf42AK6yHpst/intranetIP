<?
// using: 

class libW2Content {
	private $code;
	private $css;
	
	public function __construct($code = NULL) {
		global $w2_cfg;
		
		if ($code == NULL) {
			// do nth
		}
		else {
			$this->setCode($w2_cfg['contentArr'][$code]['contentCode']);
			$this->setCss($w2_cfg['contentArr'][$code]['css']);
		}
    }
    
    // Start Get Set
    private function setCode($code) {
    	$this->code = $code;
    }
    public function getCode() {
    	return $this->code;
    }
    
    private function setCss($css) {
    	$this->css = $css;
    }
    public function getCss() {
    	return $this->css;
    }
    // End Get Set
}
?>