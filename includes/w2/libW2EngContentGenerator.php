<?php
/**
 * Change Log:
 * 2019-05-02 Pun
 *  - Prevent Command-injection
 *
 *  Date: 	2015-10-06 [Siuwan] [ip.2.5.7.1.1] (Case#S87239)
 * 			- modified function getMindMapGeneration(), use schoolCode retrieved in W2_CONTENT_DATA instead of config value.
 */

include_once('libW2SchemeConfigGenerator.php');
include_once('w2ShareContentConfig.inc.php');
define('STARTDELIMITER', '{{{');
define('ENDDELIMITER', '}}}');

class libW2GenerateEngContent{
	private $schemeTemplateFileName;
	private $schemeTemplatePath;
	private $destinationPath;
	private $configFilePath;
	private $configFileName;
	private $PATH_WRT_ROOT;
	private $schemePath;

	public function __construct($PATH_WRT_ROOT)
	{
		$this->schemePath = $PATH_WRT_ROOT."home/eLearning/w2/content/template/";
		$this->PATH_WRT_ROOT = $PATH_WRT_ROOT;
		$this->schemeTemplateFileName ="schemeTemplate_eng/";
		$this->schemeTemplatePath = $PATH_WRT_ROOT."home/eLearning/w2/content/template/".$this->schemeTemplateFileName;
		$this->destinationPath = $PATH_WRT_ROOT."home/eLearning/w2/content/eng/";
		$this->configFilePath = $PATH_WRT_ROOT."home/eLearning/w2/content/";
		$this->configFileName = "_w2_contentConfig.inc.php";
	}

	function generateEngContent($parseData,$w2_classRoomDB,$eclass_db){
		GLOBAL $w2_cfg;

		$schoolCode = strtolower($w2_cfg["schoolCodeCurrent"]);
		$schemeNum = $parseData['schemeNum'];
		$cid = $parseData['cid'];
		$conceptType = $parseData["conceptType"];
		$schemeName =$schoolCode.'_scheme'.$schemeNum ;
		$configFullPath = $this->configFilePath.$schoolCode.$this->configFileName;

		//	(testing)delete the folder in the destination path


		$path = OsCommandSafe($this->destinationPath.$schemeName);
		exec("rm -Rf {$path}  2>&1");
		try{
		    //copy the template folder
		    $path = OsCommandSafe("{$this->schemeTemplatePath} {$this->schemePath}{$schemeName}");
			exec( "cp -r {$path}  2>&1");

			if(!$this->fileValidation($this->schemePath.$schemeName) ){
			    echo "Incomplete Files!\n";
				exit;
			}
		}
		catch (Exception $e){
		    echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

		// Edit c1 ~ c5
		$this->getC1EngContentReplacement($parseData['step1Data'],$parseData['schoolCode'],$parseData['schemeNum'],$this->schemePath.$schemeName);
		$this->getC2EngContentReplacement($parseData['step2Data'],$parseData['schoolCode'],$parseData['schemeNum'],$this->schemePath.$schemeName);
		$this->getC3EngContentReplacement($parseData['step3Data'],$parseData['schoolCode'],$parseData['schemeNum'],$this->schemePath.$schemeName);
		$this->getC4EngContentReplacement($parseData['step4Data'],$parseData['schoolCode'],$parseData['schemeNum'],$this->schemePath.$schemeName);
		$this->getC5EngContentReplacement($parseData['step5Data'],$parseData['schoolCode'],$parseData['schemeNum'],$this->schemePath.$schemeName);
		// Edit c1 ~ c5 End

		// Edit model.php and reference
		$this->getReferenceAndModelEngContentReplacementData($parseData['step3Data'],$parseData['schoolCode'],$parseData['schemeNum'],$this->schemePath.$schemeName);
		// Edit model.php and reference End

		// Generate config

		### 20141024 For old convention files
		# Adam
//		if(file_exists($configFullPath)){
//			$objW2SchemeConfigGenerator = new libW2SchemeConfigGenerator();
//			$objW2SchemeConfigGenerator->generateConfigFile($configFullPath,$schoolCode);
//		}
		// Generate config End

		$path = OsCommandSafe("{$this->schemePath}{$schemeName} {$this->destinationPath}");
		exec( "mv {$path}");

		//delete existing folder in temp folder
		$path = OsCommandSafe($this->schemePath.$schemeName);
		exec( "rm -Rf {$path}  2>&1");

//		if($conceptType!=$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){
//			if($conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]){
//				$conceptMapID = $parseData['powerBoardID'];
//			}else{
//				$conceptMapID = $parseData['powerConceptID'];
//			}
//			$this->getMindMapGeneration($parseData['r_contentCode'], $schemeName, $conceptMapID,$w2_classRoomDB,$eclass_db, $conceptType);
//		}
	}

	function getTableUpdateToGenerated($cid){
		GLOBAL $w2_cfg;
		$objDB =new libdb();
		$sql = "Update W2_CONTENT_DATA Set status=".$w2_cfg['contentWriting']['W2_GENERATED'].", dateGenerated=now() Where cid=$cid;";
		$rs = $objDB->db_db_query($sql);
		return $rs;
	}
	function getContentOptionalBySubject($r_contentCode=''){
		if($r_contentCode){
			$condsSubj = "AND r_contentCode in ('".implode("','", (array)$r_contentCode );
		}
		$sql = "SELECT * FROM W2_CONTENT_DATA WHERE 1=1 $condsSubj";
		return $this->returnResultSet($sql);
	}
	function getC1OptionsCreated($textTypeAns){
		// Random two things
		// 1) the option of the answer
		// 2) the order of the answer

		global $w2_cfg_contentSetting;

		$optionHTML = '';
		$optionHTML .= '<option value="3" <?=$w2_libW2->getAnsSelected($w2_s_thisStepAns[$w2_m_ansCode[0]], 3)?>>';
		$optionHTML .= $textTypeAns;
		$optionHTML .= '</option>';
		$answerList[0] = $optionHTML;

		//get type list
		foreach ( (array)$w2_cfg_contentSetting['eng'] as $item){
			$typeList[] = $item['type'];
		}
		$uniqueTypeList = array_unique($typeList);
		$uniqueTypeList = array_diff($uniqueTypeList, array($textTypeAns));

		//shuffle the list
		shuffle($uniqueTypeList);

		//pick two options
		for($counter=0; $counter <2 ; ++$counter){
			$optionHTML ='';
			$optionHTML .= '<option value="'.($counter+1).'" <?=$w2_libW2->getAnsSelected($w2_s_thisStepAns[$w2_m_ansCode[0]], '.($counter+1).')?>>';
			$optionHTML .= $uniqueTypeList[$counter];
			$optionHTML .= '</option>';
			$answerList[] = $optionHTML;

			$uniqueTypeList = array_diff($uniqueTypeList, array($uniqueTypeList[$counter]));
		}

		//randomize the order of options
		shuffle($answerList);

		//print options
		return implode("", $answerList);
	}
#############################################################################################
	/*
	 * Step Content Replacement Helper ( Step 1 ~5 + Reference)
	 */
	function getC1EngContentReplacement($parseData,$schoolCode,$schemeNum,$schemeName){
		$folderName =$schemeName.'/c1/';

		$step1TexttypeOption = $this->getC1OptionsCreated($parseData['step1TextType']);
		// c1/model.php
		$contentFile='model.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

		// c1/view.php
		$contentFile='view.php';
		$findString = array(
					STARTDELIMITER.'step1Int'.ENDDELIMITER,
					STARTDELIMITER.'step1TextType'.ENDDELIMITER,
					STARTDELIMITER.'step1Purpose'.ENDDELIMITER,
					STARTDELIMITER.'step1Content'.ENDDELIMITER,
					STARTDELIMITER.'step1Resource'.ENDDELIMITER,
					STARTDELIMITER.'step1TexttypeOption'.ENDDELIMITER
					);
		$replaceString = array(
					$parseData['step1Int'],
					$parseData['step1TextType'],
					$parseData['step1Purpose'],
					$parseData['step1Content'],
					$parseData['step1Resource'],
					$step1TexttypeOption,
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));
	}
	function getC2EngContentReplacement($parseData,$schoolCode,$schemeNum,$schemeName){
		$folderName = $schemeName.'/c2/';
		// c2/model.php
		$contentFile='model.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

		// c2/view.php
		$contentFile='view.php';
		$findString = array(
					STARTDELIMITER.'step2Int'.ENDDELIMITER,
					STARTDELIMITER.'step2Resource'.ENDDELIMITER
					);
		$replaceString = array(
					$parseData['step2Int'],
					$parseData['step2Resource'],
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));
	}
	function getC3EngContentReplacement($parseData,$schoolCode,$schemeNum,$schemeName){
		$folderName = $schemeName.'/c3/';
		// c3/model.php
		$contentFile='model.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum,
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));


		// c3/view.php
		$contentFile='view.php';
		$findString = array(
					STARTDELIMITER.'step3Int'.ENDDELIMITER,
					STARTDELIMITER.'step3SampleWriting'.ENDDELIMITER,
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					STARTDELIMITER.'step3Resource'.ENDDELIMITER,
					);
		$replaceString = array(
					$parseData['step3Int'],
					$parseData['step3SampleWriting'],
					$schoolCode,
					$schemeNum,
					$parseData['step3Resource'],
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

	}
	function getC4EngContentReplacement($parseData,$schoolCode,$schemeNum,$schemeName){
		$folderName = $schemeName.'/c4/';
		// c4/model.php
		$contentFile='model.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum,
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

		// c4/view.php
		$contentFile='view.php';
		$findString = array(
					STARTDELIMITER.'step4Int'.ENDDELIMITER,
					STARTDELIMITER.'step4DraftQ'.ENDDELIMITER,
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					STARTDELIMITER.'step4Resource'.ENDDELIMITER,
					);
		$replaceString = array(
					$parseData['step4Int'],
					$parseData['step4DraftQList'],
					$schoolCode,
					$schemeNum,
					$parseData['step4Resource'],
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

	}
	function getC5EngContentReplacement($parseData,$schoolCode,$schemeNum,$schemeName){
		$folderName = $schemeName.'/c5/';
		// c5/model.php
		$contentFile='model.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					STARTDELIMITER.'step5DefaultWriting'.ENDDELIMITER,
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum,
					$parseData['step5DefaultWriting'],
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

		// c5/view.php
		$contentFile='view.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					STARTDELIMITER.'step5Int'.ENDDELIMITER,
					STARTDELIMITER.'step5Resource'.ENDDELIMITER,
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum,
					$parseData['step5Int'],
					$parseData['step5Resource'],
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));
	}

	function getReferenceAndModelEngContentReplacementData($parseData,$schoolCode,$schemeNum,$schemeName){
		$folderName = $schemeName.'/reference/';
		// reference/referenceGrammer.php
		$contentFile='referenceGrammer.php';
		$findString = array(
					STARTDELIMITER.'step3Grammar'.ENDDELIMITER,
					);
		$stepGrammar = !empty($parseData['step3Grammar'])?intranet_undo_htmlspecialchars($parseData['step3Grammar']):'';
		$replaceString = array(
					$stepGrammar,
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

		// reference/referenceVocab.php
		$contentFile='referenceVocab.php';
		$findString = array(
					STARTDELIMITER.'step3Vocab'.ENDDELIMITER,
					);
		$stepVocab = !empty($parseData['step3Vocab'])?intranet_undo_htmlspecialchars($parseData['step3Vocab']):'';
		$replaceString = array(
					$stepVocab,
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));

		// model.php
		$folderName = $schemeName.'/';
		$contentFile='model.php';
		$findString = array(
					STARTDELIMITER.'schPrefix'.ENDDELIMITER,
					STARTDELIMITER.'schemeNum'.ENDDELIMITER,
					);
		$replaceString = array(
					$schoolCode,
					$schemeNum,
					);
		file_put_contents($folderName.$contentFile,str_replace($findString, $replaceString, file_get_contents($folderName.$contentFile)));
	}

	function getMindMapGeneration($parseData, $w2_classRoomDB,$eclass_db){
		global $w2_cfg;
		$objDB =new libdb();

		$schoolCode = !empty($parseData["schoolCode"])?strtolower($parseData["schoolCode"]):strtolower($w2_cfg["schoolCodeCurrent"]);
		$schemeNum = $parseData['schemeNum'];
		$r_contentCode = $parseData['r_contentCode'];
		$schemeName =$schoolCode.'_scheme'.$schemeNum ;
		$conceptType = !empty($parseData['conceptType'])?$parseData['conceptType']:W2_CONCEPT_TYPE_POWERCONCEPT;
		if($conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]){
			$settingName = 'conceptMapContentPB';
			$conceptMapID = $parseData['powerBoardID'];
			$sql = "select DataContent from TOOLS_POWERBOARD Where PowerBoardID = '".$conceptMapID."'";
			$settingValue = current($objDB->returnVector($sql));
			$classroom_table = 'powerboard';
		}else{
			$settingName = 'conceptMapContent';
			$conceptMapID = $parseData['powerConceptID'];
			$sql = "select DataContent from TOOLS_POWERCONCEPT Where PowerConceptID = '".$conceptMapID."'";
			$settingValue = current($objDB->returnVector($sql));
			$classroom_table = 'powerconcept';
		}
		$sql =
		"
		Insert Ignore Into
			{$eclass_db}.W2_DEFAULT_CONTENT(SETTING_NAME , CONTENT_CODE ,SCHEME_CODE ,DATE_INPUT ,DATE_MODIFIED )
		Values
			('".$settingName."','${r_contentCode}','".$schemeName."',now(),now())
		;";
		$rs = $objDB->db_db_query($sql);

		if($rs){
			$sql =
			"
			Select
				SETTING_ID
			From
				{$eclass_db}.W2_DEFAULT_CONTENT
			Where
				SCHEME_CODE = '". $schemeName ."' AND SETTING_NAME = '".$settingName."' AND CONTENT_CODE = '".$r_contentCode."'
			;";
			$rs = $objDB->returnVector($sql);
			$settingID =$rs[0];
		}
		else{
			bl_error_log($projectName='W2 ENGINE',$errorMsg="WRONG INSERT1, {$sql}",$errorType='');
			exit();
		}

		$sql ="
			Update
				{$eclass_db}.W2_DEFAULT_CONTENT as c
			Set
				SETTING_VALUE = '".$settingValue."'
			Where c.SETTING_ID = ".$settingID."
		";

//		$sql =
//		"
//			Update
//				{$eclass_db}.W2_DEFAULT_CONTENT as c
//			Set
//				SETTING_VALUE = (select DataContent from TOOLS_POWERCONCEPT
//									Where PowerConceptID = ${PowerConceptID})
//			Where c.SETTING_ID = ${settingID};
//		";
		$rs = $objDB->db_db_query($sql);

		if($rs)
		{
			$sql = "SELECT 1 from ${w2_classRoomDB}.".$classroom_table."
					Where Name = 'W2_DEFAULT_".strtoupper( $r_contentCode )."_".strtoupper( $schemeName )."';";
			$rs = $objDB->returnVector($sql);
			$rs = $rs[0];
			if($rs =='')
			{
				$sql =
				"
					Insert Into
						${w2_classRoomDB}.".$classroom_table."(Name,DataContent,InputDate,ModifiedDate,user_id,group_id,assignment_id,plan_section_id,MarkedDataContent,MarkedDate)
					Values
						('W2_DEFAULT_".strtoupper( $r_contentCode )."_".strtoupper( $schemeName )."','',now(),now(),0,0,0,NULL,NULL,NULL);
				";
				$rs = $objDB->db_db_query($sql);
			}
			if($rs)
			{
				$sql =
					"
					Update
						".$w2_classRoomDB.".".$classroom_table."
					Set
						DataContent = (SELECT SETTING_VALUE from {$eclass_db}.W2_DEFAULT_CONTENT
										Where setting_name = '".$settingName."' and content_code = '".$r_contentCode."' and scheme_code = '".$schemeName."')
					Where Name = 'W2_DEFAULT_".strtoupper( $r_contentCode )."_".strtoupper( $schemeName )."';
					";
				$rs = $objDB->db_db_query($sql);

			}
			else
			{
				bl_error_log($projectName='W2 ENGINE',$errorMsg="WRONG UPDATE2, {$sql}",$errorType='');
				exit;
			}
		}
		else
		{
			bl_error_log($projectName='W2 ENGINE',$errorMsg="WRONG INSERT2, {$sql}",$errorType='');
			exit;
		}
		return $rs;
	}
	/*
	 *  Add to config data
	 */
//	function getConfigData($schemeName, $type_super, $type, $name, $introduction, $createdDate)
//	{
//		$returnStr="";
//		$returnStr.=" '$schemeName' => array( "."\n\r".
//				" 'type_user' =>'$type_user', "."\n\r".
//				" 'type' =>'$type', "."\n\r".
//				" 'name' =>'$name', "."\n\r".
//				" 'introduction' =>'$introduction', "."\n\r".
//				" 'createdDate' =>'$createdDate', "."\n\r".
//			"), "."\n\r";
//		$returnStr.="\/\/engNewTemplate"."\n\r";
//		return $returnStr;
//	}
#################################################################################################
	/*
	 *  Validate files' existence
	 */
	function fileValidation($destinationPath)
	{
		$dirList= array(
			'c1/'=>array('model.php', 'view.php'),
			'c2/'=>array('model.php', 'view.php'),
			'c3/'=>array('model.php', 'view.php'),
			'c4/'=>array('model.php', 'view.php'),
			'c5/'=>array('model.php', 'view.php'),
			'reference/'=>array('referenceGrammer.php', 'referenceVocab.php'),
			''=>array('model.php')
		);
		foreach((array)$dirList as $key => $fileList) //forreach directory
		{
			foreach( (array)$fileList as $file) //foreach files in a directory
			{

				if(!file_exists($destinationPath.'/'.$key.$file))
				{
					return false;
				}
			}
		}
		return true;
	}
}


?>