<?php
	/*
	 * Please Use UTF-8 to save this file
	 * Except status, all the information in Writing 2.0 content engine should be placed here
	 */

###########################!!!!!!!!!!!!!!!!!!!!!! IMPORTANT NOTE !!!!!!!!!!!!!!!!!!!!!!###########################
/* 20150211 added by Siuwan
 * 
 * 
 * For version ip.2.5.6.3.1.0 or later, please DO NOT EDIT $w2_cfg["schoolCodeCurrent"] ANYMORE!!!!!!!!!!!!!!!!!
 * If it is really neccessary to edit $w2_cfg["schoolCodeCurrent"], please DOUBLE CONFIRM it works fine in MANAGEMENT and CONTENT INPUT
 * since viewHandIn.php retrieves cid by using schemecode and schoolcode
 * DO NOT add UNDERSCORE in schoolcode!!!!!
 * 
 * 
*/
###########################!!!!!!!!!!!!!!!!!!!!!! IMPORTANT NOTE !!!!!!!!!!!!!!!!!!!!!!###########################

	define("STARTING_SCHEME_NUM", 1);
	
	### 201471027 Comment Start, schoolCode will be no more useful towards W2 
//	if(empty($config_school_code))	{
//		echo "System Config Missing for Writing 2.0.<br/>";
//		bl_error_log($projectName='W2 Engine ('.$server_ip.')',$errorMsg='Empty School Config Code',$errorType='Missing Value');
//		exit;
//	}
//	$w2_cfg["schoolCodeCurrent"]=$config_school_code;
	### 201471027 Comment End

	$w2_cfg["schoolCodeCurrent"]='bl_w2';
//	 array( Short Descritpion, School Full English Name, School Full Chinese Name )

if($system_env['ENV'] == 'dev'){
	$w2_cfg["schoolCode"]['111111'] = array(
			"shortDesc"=>'111111',
			"engName"=>"Broadlearning College 146",
			"chiName"=>"博文書院  146");
}
if($system_env['ENV'] == 'testing'){
	$w2_cfg["schoolCode"]['100000'] = array(
			"shortDesc"=>'100000',
			"engName"=>"Broadlearning College Spec27",
			"chiName"=>"博文書院  Spec27");
}
if($system_env['ENV'] == 'uat'){			
	$w2_cfg["schoolCode"]['ip20b5alpha'] = array(
			"shortDesc"=>'ip20b5alpha',
			"engName"=>"Broadlearning College 149",
			"chiName"=>"博文書院  149");
}
	$w2_cfg["schoolCode"]['558044'] = array(
			"shortDesc"=>'558044',
			"engName"=>"Po Leung Kuk Laws Foundation College",
			"chiName"=>"保良局羅氏基金中學");
			
	$w2_cfg["schoolCode"]['536857'] = array(
			"shortDesc"=>'536857',
			"engName"=>"Fukien Secondary School",
			"chiName"=>"福建中學");
			
	$w2_cfg["schoolCode"]['542105'] = array(
			"shortDesc"=>'542105',
			"engName"=>"St Margaret's Co-educational English Secondary and Primary School",
			"chiName"=>"聖瑪加利男女英文中小學");
			
	$w2_cfg["schoolCode"]['513369'] = array(
			"shortDesc"=>'513369',
			"engName"=>"Pui Kiu Middle School",
			"chiName"=>"培橋中學");
			
	$w2_cfg["schoolCode"]['214299'] = array(
			"shortDesc"=>'214299',
			"engName"=>"St Margaret’s Girl’s College, Hong Kong",
			"chiName"=>"香港聖瑪加利女書院");
			
	$w2_cfg["contentCodeList"] = array(
			'eng',
			'chi', 
			'ls', 
			'ls_eng', 
			'sci'
	);
	function bl_error_log($projectName='',$errorMsg='',$errorType=''){
	$mail = 'mfkhoe@g2.broadlearning.com';
	$mail = 'adamwan@g4.broadlearning.com';
	//mail($mail,"Project {$projectName} - ".date("F j, Y, g:i a"),$errorMsg);
	return true;
}
?>