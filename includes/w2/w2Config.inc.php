<?php
/** [Modification Log] Modifying By:Siuwan
 * *******************************************
 * *******************************************
 */




define("CFG_W2_MODULE_PATH", "/home/eLearning/w2");

define("W2_STATUS_ACTIVE",1);  // indicate a record is active or suspend, delete 
define("W2_STATUS_SUBMITTING", 2);
define("W2_STATUS_SUBMITTED", 3);	### edit /templates/2009a/js/w2/content/viewHandinHandler.js if you changed this value ###
define("W2_STATUS_APPROVED", 4);
define("W2_STATUS_TEACHER_READ", 5);
define("W2_STATUS_REDO", 6);
define("W2_STATUS_REJECT", 7);
define("W2_STATUS_DELETED", 8);
define("W2_STATUS_WAITING", 9);
define("W2_STATUS_PUBLIC", 10);
define("W2_STATUS_PRIVATE", 11);
define("W2_STATUS_DRAFT", 12);
define("W2_STATUS_ALL_WRITING", 13);
define("W2_STATUS_MY_WRITING", 14);
define("W2_STATUS_TEACHER_COMMENTED", 15);
define("W2_STATUS_STUDENT_READ", 16);
define("W2_STATUS_STUDENT_NOT_READ", 17);

define("W2_ACTION_MARKING", 'MARKING');
define("W2_ACTION_PEER_MARKING", 'PEER_MARKING');
define("W2_ACTION_PREVIEW", 'PREVIEW');
define("W2_ACTION_SHARING", 'SHARE');
define("W2_ACTION_REDO", 'REDO'); //added by siuwan for marking

define("W2_EDITOR_SOURCE_IP", 'IP');
define("W2_EDITOR_SOURCE_ECLASS", 'ECLASS');

define("W2_WRITING_OBJECT_SOURCE_DB", 'DB');
define("W2_WRITING_OBJECT_SOURCE_XML", 'XML');

define("W2_WRITING_WALKING_STEP_MODE_STEPWISE", 'stepWise');
define("W2_WRITING_WALKING_STEP_MODE_STEPFREE", 'stepFree');

define("W2_WRITING_DELIMITER_1", '@~@');

define("W2_REFERENCE_SOURCE_STEP_ANSWER", 'stepAns');
define("W2_REFERENCE_SOURCE_DEFAULT", 'default');
define("W2_REFERENCE_SOURCE_SELF_ADD", 'selfAdd');
define("W2_REFERENCE_SOURCE_CONTENT_INPUT", 'contentInput'); //Siuwan 20140411 for Sci input engine

define("W2_REFERENCE_DISPLAY_MODE_MANAGE", 'manage');
define("W2_REFERENCE_DISPLAY_MODE_VIEW", 'view');
define("W2_REFERENCE_DISPLAY_MODE_INPUT", 'input'); //Siuwan 20140410 for Sci input engine
define("W2_CONCEPT_TYPE_POWERCONCEPT", 'powerconcept');
define("W2_CONCEPT_TYPE_POWERBOARD", 'powerboard');
define("W2_CONCEPT_TYPE_ATTACHMENT", 'attachment');

$w2_cfg['w2ClassRoomType'] = 6; // w2 with class room backup , room type = 6
$w2_cfg['moduleCode'] = 'w2';
$w2_cfg['modulePath'] = CFG_W2_MODULE_PATH;

$w2_cfg["dbTableDefault"]["NumPerPage"] = 50;
$w2_cfg["dbTableDefault"]["sortColumnIndex"] = 0;
$w2_cfg["dbTableDefault"]["sortOrder"] = 0;

$w2_cfg["contentArr"]["chi"]["contentCode"] = "chi";
$w2_cfg["contentArr"]["chi"]["css"] = "chi";
$w2_cfg["contentArr"]["eng"]["contentCode"] = "eng";
$w2_cfg["contentArr"]["eng"]["css"] = "eng";
$w2_cfg["contentArr"]["ls"]["contentCode"] = "ls";
$w2_cfg["contentArr"]["ls"]["css"] = "ls";
$w2_cfg["contentArr"]["ls_eng"]["contentCode"] = "ls_eng";
$w2_cfg["contentArr"]["ls_eng"]["css"] = "ls_eng";
$w2_cfg["contentArr"]["sci"]["contentCode"] = "sci";
$w2_cfg["contentArr"]["sci"]["css"] = "sci";

$w2_cfg["actionArr"]["marking"] = W2_ACTION_MARKING;
$w2_cfg["actionArr"]["peerMarking"] = W2_ACTION_PEER_MARKING;
$w2_cfg["actionArr"]["preview"] = W2_ACTION_PREVIEW;
$w2_cfg["actionArr"]["sharing"] = W2_ACTION_SHARING;
$w2_cfg["actionArr"]["redo"] = W2_ACTION_REDO;

$w2_cfg["editorSource"]["ip"] = W2_EDITOR_SOURCE_IP;
$w2_cfg["editorSource"]["eclass"] = W2_EDITOR_SOURCE_ECLASS;

$w2_cfg["writingAccessStatus"]["allWriting"] = W2_STATUS_ALL_WRITING;
$w2_cfg["writingAccessStatus"]["myWriting"] = W2_STATUS_MY_WRITING;

$w2_cfg["writingObjectSource"]["db"] = W2_WRITING_OBJECT_SOURCE_DB;
$w2_cfg["writingObjectSource"]["xml"] = W2_WRITING_OBJECT_SOURCE_XML;

$w2_cfg["refCategoryObjectSource"]["stepAns"] = W2_REFERENCE_SOURCE_STEP_ANSWER;
$w2_cfg["refCategoryObjectSource"]["default"] = W2_REFERENCE_SOURCE_DEFAULT;
$w2_cfg["refCategoryObjectSource"]["selfAdd"] = W2_REFERENCE_SOURCE_SELF_ADD;
$w2_cfg["refCategoryObjectSource"]["contentInput"] = W2_REFERENCE_SOURCE_CONTENT_INPUT; //Siuwan 20140411 for Sci input engine

$w2_cfg["refItemObjectSource"]["stepAns"] = W2_REFERENCE_SOURCE_STEP_ANSWER;
$w2_cfg["refItemObjectSource"]["default"] = W2_REFERENCE_SOURCE_DEFAULT;
$w2_cfg["refItemObjectSource"]["selfAdd"] = W2_REFERENCE_SOURCE_SELF_ADD;
$w2_cfg["refItemObjectSource"]["contentInput"] = W2_REFERENCE_SOURCE_CONTENT_INPUT; //Siuwan 20140411 for Sci input engine

$w2_cfg["refDisplayMode"]["manage"] = W2_REFERENCE_DISPLAY_MODE_MANAGE;
$w2_cfg["refDisplayMode"]["view"] = W2_REFERENCE_DISPLAY_MODE_VIEW;
$w2_cfg["refDisplayMode"]["input"] = W2_REFERENCE_DISPLAY_MODE_INPUT; //Siuwan 20140410 for Sci input engine
$w2_cfg['refDefaultCssSet'] = 3;

######
# DB VALUE CONFIG
# FORM DB_table name_table field_meaning = value
######
//TABLE : W2_WRITING
$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

$w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["public"] = W2_STATUS_PUBLIC;
$w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["private"] = W2_STATUS_PRIVATE;

$w2_cfg["DB_W2_WRITING_WITH_DEFAULT_CONCEPTMAP"]["yes"] = 1;
$w2_cfg["DB_W2_WRITING_WITH_DEFAULT_CONCEPTMAP"]["no"] = 0;

$w2_cfg["DB_W2_WRITING_PEER_MARKING_TARGET_NUM"]["markEveryone"] = -1;
$w2_cfg["DB_W2_WRITING_PEER_MARKING_TARGET_NUM_default"] = $w2_cfg["DB_W2_WRITING_PEER_MARKING_TARGET_NUM"]["markEveryone"];

$w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING"]["allow"] = 1;
$w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING"]["notAllow"] = 0;
$w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING_default"] = $w2_cfg["DB_W2_WRITING_ALLOW_PEER_MARKING"]["notAllow"];

$w2_cfg["DB_W2_WRITING_SHOW_PEER_NAME"]["show"] = 1;
$w2_cfg["DB_W2_WRITING_SHOW_PEER_NAME"]["notShow"] = 0;
$w2_cfg["DB_W2_WRITING_SHOW_PEER_NAME_default"] = $w2_cfg["DB_W2_WRITING_SHOW_PEER_NAME"]["notShow"];

//TABLE : W2_WRITING_STUDENT
$w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"] = W2_STATUS_SUBMITTED;

$w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_WRITING_STUDENT_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

//TABLE : W2_STEP_STUDENT
$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"] = W2_STATUS_SUBMITTED;  //student has footprint in this step
$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["draft"] = W2_STATUS_DRAFT;

$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"] = W2_STATUS_WAITING;
$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"] = W2_STATUS_APPROVED;
$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"] = W2_STATUS_REDO;
//TABLE : W2_WRITING_TEACHER
$w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

//TABLE : W2_STEP_HANDIN
$w2_cfg["DB_W2_STEP_HANDIN_ANSWER"]["infoboxHasSubmitted"] = W2_STATUS_SUBMITTED;	### edit /templates/2009a/js/w2/content/viewHandinHandler.js if you changed this value ###

//TABLE : W2_STEP_STUDENT_COMMENT
$w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["teacherCommented"] = W2_STATUS_TEACHER_COMMENTED;
$w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["studentHasRead"] = W2_STATUS_STUDENT_READ;

//TABLE : W2_REF_CATEGORY
$w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_REF_CATEGORY_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

//TABLE : W2_REF_ITEM
$w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

//TABLE : W2_PEER_MARKING_GROUPING
$w2_cfg["DB_W2_PEER_MARKING_GROUPING_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_PEER_MARKING_GROUPING_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

//TABLE : W2_PEER_MARKING_DATA
$w2_cfg["DB_W2_PEER_MARKING_DATA_DELETE_STATUS"]["active"] = W2_STATUS_ACTIVE;
$w2_cfg["DB_W2_PEER_MARKING_DATA_DELETE_STATUS"]["isDeleted"] = W2_STATUS_DELETED;

$w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["viewed"] = W2_STATUS_STUDENT_READ;
$w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["notViewed"] = W2_STATUS_STUDENT_NOT_READ;

//TABLE : W2_STEP
$w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["YES"] = 1;
$w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["NO"] = 0;

//TABLE : W2_CONTENT_DATA
$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"] = W2_CONCEPT_TYPE_POWERCONCEPT;
$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"] = W2_CONCEPT_TYPE_POWERBOARD;
$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"] = W2_CONCEPT_TYPE_ATTACHMENT;
$w2_cfg["DB_W2_CONTENT_DATA_TEACHERATTACTMENT"]["defaultDisplayNum"] = 2;

//TABLE : W2_STEP_STUDENT_FILE
$w2_cfg["DB_W2_STEP_STUDENT_FILE_STUDENTATTACHMENT"]["defaultDisplayNum"] = 2;
######
# TOP TAB MENU CONFIG
######
//define("CFG_W2_TAB_IMAGE_PATH", "images/".$LAYOUT_SKIN."/w2/Resource/190_writing20/");
define("CFG_W2_IMAGE_PATH", "images/2020a/w2/Resource/190_writing20/");
$w2_cfg["imagePath"] = CFG_W2_IMAGE_PATH;

// Student
define("CFG_W2_TAB_STUDENT_INCOMPLETE_WRITING", 1);
define("CFG_W2_TAB_STUDENT_COMPLETEED_WRITING", 2);
define("CFG_W2_TAB_STUDENT_PEER_MARKING", 3);
define("CFG_W2_TAB_STUDENT_SHARE_WRITING", 4);
$w2_cfg["tabArr"][USERTYPE_STUDENT][CFG_W2_TAB_STUDENT_INCOMPLETE_WRITING] = array($Lang['W2']['incompleteWriting'], '?task=listWriting&mod=writing&r_contentCode='.$r_contentCode, 'icon_incomp_writing.gif', 0);
$w2_cfg["tabArr"][USERTYPE_STUDENT][CFG_W2_TAB_STUDENT_COMPLETEED_WRITING] = array($Lang['W2']['completedWriting'], '?task=completedWriting&mod=writing&r_contentCode='.$r_contentCode, 'icon_comp_writing.gif', 0);
$w2_cfg["tabArr"][USERTYPE_STUDENT][CFG_W2_TAB_STUDENT_PEER_MARKING] = array($Lang['W2']['peerMarking'], '?task=listMarkWriting&mod=peerMarking&r_contentCode='.$r_contentCode, 'icon_peer_mark.gif', 0);
$w2_cfg["tabArr"][USERTYPE_STUDENT][CFG_W2_TAB_STUDENT_SHARE_WRITING] = array($Lang['W2']['shareWriting'], '?task=listSharingWriting&mod=writing&r_contentCode='.$r_contentCode, 'icon_share_writing.gif', 0);
//$w2_cfg["tabArr"][USERTYPE_STUDENT][CFG_W2_TAB_STUDENT_SHARE_WRITING] = array($Lang['W2']['shareWriting'], '?task=shareWriting&mod=writing&r_contentCode='.$r_contentCode, 'icon_share_writing.gif', 0);


// Teacher
define("CFG_W2_TAB_TEACHER_MANAGEMENT", 1);
define("CFG_W2_TAB_TEACHER_REPORT", 2);
define("CFG_W2_TAB_TEACHER_TEMPLATE_MGMT", 3);
define("CFG_W2_TAB_TEACHER_PUBLISH", 4);
define("CFG_W2_TAB_TEACHER_SHARE_WRITING", 5);

$w2_cfg["tabArr"][USERTYPE_STAFF][CFG_W2_TAB_TEACHER_MANAGEMENT] = array($Lang['W2']['management'], '?task=writingMgmt&mod=writing&r_contentCode='.$r_contentCode.'&clearCoo=1', 'icon_mgmt.gif', 0);
$w2_cfg["tabArr"][USERTYPE_STAFF][CFG_W2_TAB_TEACHER_TEMPLATE_MGMT] = array($Lang['W2']['contentInput'], '?task=templateManagement&mod=admin&r_contentCode='.$r_contentCode, 'icon_content_input.gif', 0);
//$w2_cfg["tabArr"][USERTYPE_STAFF][CFG_W2_TAB_TEACHER_PUBLISH] = array($Lang['W2']['publish'], '?task=publish&mod=admin&r_contentCode='.$r_contentCode, 'icon_mgmt.gif', 0);
$w2_cfg["tabArr"][USERTYPE_STAFF][CFG_W2_TAB_TEACHER_SHARE_WRITING] = array($Lang['W2']['shareWriting'], '?task=listSharingWriting&mod=writing&r_contentCode='.$r_contentCode, 'icon_share_writing.gif', 0);
$w2_cfg["tabArr"][USERTYPE_STAFF][CFG_W2_TAB_TEACHER_REPORT] = array($Lang['W2']['report'], '?task=listWritingReport&mod=report&r_contentCode='.$r_contentCode.'&clearCoo=1', 'icon_report.gif', 0);
// Legend
define("CFG_W2_LEGEND_APPROVAL_IS_NEEDED", 'approvalIsNeeded');
define("CFG_W2_LEGEND_WAITING_FOR_APPROVAL", 'waitingForApproval');
define("CFG_W2_LEGEND_APPROVED", 'approved');
define("CFG_W2_LEGEND_NEED_TO_REDO", 'needToRedo');
define("CFG_W2_LEGEND_INCOMPLETE", 'incomplete');
define("CFG_W2_LEGEND_COMPLETED", 'completed');
define("CFG_W2_LEGEND_TEACHER_ALREADY_COMMENTED", 'teacherAlreadyCommented');
define("CFG_W2_LEGEND_TEACHER_NEED_PROVIDE_COMMENT", 'teacherNeedProvideComment');
define("CFG_W2_LEGEND_STUDENT_NEED_CHECK_COMMENT", 'studentNeedCheckComment');
define("CFG_W2_LEGEND_STUDENT_WAITING_TEACHER_COMMENT", 'studentWaitingTeacherComment');
define("CFG_W2_LEGEND_PEER_COMMENTED", 'peerCommented');
define("CFG_W2_LEGEND_SHARE_WRITING", 'shareWriting');
define("CFG_W2_LEGEND_SHARE_RUBRICS", 'shareRubrics');

$w2_cfg["legendArr"]["approvalIsNeeded"] = CFG_W2_LEGEND_APPROVAL_IS_NEEDED;
$w2_cfg["legendArr"]["waitingForApproval"] = CFG_W2_LEGEND_WAITING_FOR_APPROVAL;
$w2_cfg["legendArr"]["approved"] = CFG_W2_LEGEND_APPROVED;
$w2_cfg["legendArr"]["needToRedo"] = CFG_W2_LEGEND_NEED_TO_REDO;
$w2_cfg["legendArr"]["incomplete"] = CFG_W2_LEGEND_INCOMPLETE;
$w2_cfg["legendArr"]["completed"] = CFG_W2_LEGEND_COMPLETED;
$w2_cfg["legendArr"]["teacherAlreadyCommented"] = CFG_W2_LEGEND_TEACHER_ALREADY_COMMENTED;
$w2_cfg["legendArr"]["teacherNeedProvideComment"] = CFG_W2_LEGEND_TEACHER_NEED_PROVIDE_COMMENT;
$w2_cfg["legendArr"]["studentNeedCheckComment"] = CFG_W2_LEGEND_STUDENT_NEED_CHECK_COMMENT;
$w2_cfg["legendArr"]["studentWaitingTeacherComment"] = CFG_W2_LEGEND_STUDENT_WAITING_TEACHER_COMMENT;
$w2_cfg["legendArr"]["peerCommented"] = CFG_W2_LEGEND_PEER_COMMENTED;
$w2_cfg["legendArr"]["shareWriting"] = CFG_W2_LEGEND_SHARE_WRITING;
$w2_cfg["legendArr"]["shareRubrics"] = CFG_W2_LEGEND_SHARE_RUBRICS;



// Top right ball ball handling
$w2_cfg["walkingStepModeArr"]["stepWise"] = W2_WRITING_WALKING_STEP_MODE_STEPWISE;
$w2_cfg["walkingStepModeArr"]["stepFree"] = W2_WRITING_WALKING_STEP_MODE_STEPFREE;


$w2_cfg['studentHandInAllowAction']['edit'] = 'edit';
$w2_cfg['studentHandInAllowAction']['view'] = 'view';

// CONCEPT MAP DISPLAY MODE

$w2_cfg['conceptMapDisplayMode']['studentAns'] = 1; // this mode for display concept in student login for answer / view the concept
$w2_cfg['conceptMapDisplayMode']['teacherPreivewImage'] = 2; // this mode for teacher preview the step in teacher login, this show the image only

//student fileupload displayMode
$w2_cfg['uploadFileActionMode']['allowUpload'] = 1; // this mode for display file in student login with 'upload new' and 'delete action'
$w2_cfg['uploadFileActionMode']['doNotAllowUpload'] = 2; // this mode for display file in student login without 'upload new' and 'delete action'


// Delimiter
$w2_cfg['delimiter1'] = W2_WRITING_DELIMITER_1;

// Emtpy Data Display
$w2_cfg['emptyDataStyle1'] = '--';

// Writing Default Content Color
$w2_cfg['defaultWritingTextColor'] = 'blue';

// DB Content Writing Status 
define("W2_TEMP", 0);
define("W2_GENERATED", 1);
define("W2_PUBLISHED", 2);
define("W2_DELETED", 3);
define("W2_DISABLED", 4);

// DB Shared Record Status
define("W2_SHARED_FAIL", 0);
define("W2_SHARED_SUCCEED", 1);

// DB Received Record Status
define("W2_RECEIVED_FAIL", 0);
define("W2_RECEIVED_SUCCEED", 1);

// Content Upload Status
define("W2_UPLOAD_UPLOADING", 0);
define("W2_UPLOAD_SUCCEED", 1);
define("W2_UPLOAD_FAIL", 2);

$w2_cfg["contentWriting"]["W2_TEMP"] = W2_TEMP;
$w2_cfg["contentWriting"]["W2_GENERATED"] = W2_GENERATED;
$w2_cfg["contentWriting"]["W2_PUBLISHED"] = W2_PUBLISHED;

// Marking Method Mark Setting
define("W2_MARKING_FULL", 100);
define("W2_MARKING_PASS", 50);
define("W2_MARKING_LOWEST", 0);

define("W2_MARKING_TEACHER_WEIGHT", 100);
define("W2_MARKING_SELF_WEIGHT", NULL);
define("W2_MARKING_PEER_WEIGHT", NULL);

// Marking Sticker
$w2_cfg["contentWriting"]["MarkingSticker"] = array();
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_01";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_02";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_03";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_04";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_05";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_06";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_07";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_08";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_09";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_10";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_11";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_12";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_13";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_14";
$w2_cfg["contentWriting"]["MarkingSticker"][] = "sticker_15";


//ViewHandIn Step
$w2_cfg['HandinArr']['WritingTemplate']['chi']['c1'] = 'topicAndTheme';
$w2_cfg['HandinArr']['WritingTemplate']['chi']['c2'] = 'conceptMap';
$w2_cfg['HandinArr']['WritingTemplate']['chi']['c3'] = 'sampleWriting';
$w2_cfg['HandinArr']['WritingTemplate']['chi']['c4'] = 'writingApproach';
$w2_cfg['HandinArr']['WritingTemplate']['chi']['c5'] = 'doWriting';

$w2_cfg['HandinArr']['WritingTemplate']['eng']['c1'] = 'topicAndTheme';
$w2_cfg['HandinArr']['WritingTemplate']['eng']['c2'] = 'conceptMap';
$w2_cfg['HandinArr']['WritingTemplate']['eng']['c3'] = 'sampleWriting';
$w2_cfg['HandinArr']['WritingTemplate']['eng']['c4'] = 'writingApproach';
$w2_cfg['HandinArr']['WritingTemplate']['eng']['c5'] = 'doWriting';

$w2_cfg['HandinArr']['WritingTemplate']['ls_eng']['c1'] = 'topicAndTheme';
$w2_cfg['HandinArr']['WritingTemplate']['ls_eng']['c2'] = 'termsAndInformation';
$w2_cfg['HandinArr']['WritingTemplate']['ls_eng']['c3'] = 'conceptMap';
$w2_cfg['HandinArr']['WritingTemplate']['ls_eng']['c4'] = 'writingApproach';
$w2_cfg['HandinArr']['WritingTemplate']['ls_eng']['c5'] = 'doWriting';

$w2_cfg['HandinArr']['WritingTemplate']['ls']['c1'] = 'topicAndTheme';
$w2_cfg['HandinArr']['WritingTemplate']['ls']['c2'] = 'termsAndInformation';
$w2_cfg['HandinArr']['WritingTemplate']['ls']['c3'] = 'conceptMap';
$w2_cfg['HandinArr']['WritingTemplate']['ls']['c4'] = 'writingApproach';
$w2_cfg['HandinArr']['WritingTemplate']['ls']['c5'] = 'doWriting';


$w2_cfg['HandinArr']['WritingTemplate']['sci']['c1'] = 'topicAndTheme';
$w2_cfg['HandinArr']['WritingTemplate']['sci']['c2'] = 'referenceMaterials';
$w2_cfg['HandinArr']['WritingTemplate']['sci']['c3'] = 'conceptMap';
$w2_cfg['HandinArr']['WritingTemplate']['sci']['c4'] = 'sampleWriting';
$w2_cfg['HandinArr']['WritingTemplate']['sci']['c5'] = 'writingApproach';
$w2_cfg['HandinArr']['WritingTemplate']['sci']['c6'] = 'doWriting';

$w2_cfg['HandinArr']['withConceptMapStep']['chi'] = array('c2','c5');
$w2_cfg['HandinArr']['withConceptMapStep']['eng'] = array('c2','c4','c5');
$w2_cfg['HandinArr']['withConceptMapStep']['ls_eng'] = array('c3','c4','c5');
$w2_cfg['HandinArr']['withConceptMapStep']['ls'] = array('c3','c4','c5');
$w2_cfg['HandinArr']['withConceptMapStep']['sci'] = array('c3','c5','c6');

//Define Writing Step Config
$w2_cfg['HandinArr']['hideApprovalBtn']['chi'] 								= array('c1'=>true,'c2'=>true,'c3'=>true,'c4'=>true,'c5'=>true);
$w2_cfg['HandinArr']['hideTeacherCommentBtn']['chi'] 						= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disabledNextBtnIfNotClickedAllModelAnswer']['chi'] 	= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disableAnswerIfCompletedWriting']['chi'] 				= array('c1'=>true,'c2'=>true,'c3'=>true,'c4'=>true,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfSubmittedWriting']['chi'] 				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfCompletedWriting']['chi']				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfSubmittedStep']['chi'] 				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfCompletedWriting']['chi'] 			= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['requireInputAllAnswer']['chi'] 						= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['getStudentFileInThisStep']['chi'] 					= array('c1'=>false,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>true);

$w2_cfg['HandinArr']['hideApprovalBtn']['eng'] 								= array('c1'=>true,'c2'=>true,'c3'=>true,'c4'=>true,'c5'=>true);
$w2_cfg['HandinArr']['hideTeacherCommentBtn']['eng'] 						= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disabledNextBtnIfNotClickedAllModelAnswer']['eng'] 	= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disableAnswerIfCompletedWriting']['eng'] 				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>true,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfSubmittedWriting']['eng'] 				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfCompletedWriting']['eng']				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfSubmittedStep']['eng'] 				= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfCompletedWriting']['eng'] 			= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['requireInputAllAnswer']['eng'] 						= array('c1'=>true,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['getStudentFileInThisStep']['eng'] 					= array('c1'=>false,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>true);

$w2_cfg['HandinArr']['hideApprovalBtn']['ls'] 								= array('c1'=>true,'c2'=>true,'c3'=>true,'c4'=>true,'c5'=>true);
$w2_cfg['HandinArr']['hideTeacherCommentBtn']['ls'] 						= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disabledNextBtnIfNotClickedAllModelAnswer']['ls'] 	= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disableAnswerIfCompletedWriting']['ls'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfSubmittedWriting']['ls'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfCompletedWriting']['ls']				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfSubmittedStep'] ['ls'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfCompletedWriting']['ls'] 			= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['requireInputAllAnswer']['ls'] 						= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['getStudentFileInThisStep']['ls'] 						= array('c1'=>false,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>true);

$w2_cfg['HandinArr']['hideApprovalBtn']['ls_eng'] 								= array('c1'=>true,'c2'=>true,'c3'=>true,'c4'=>true,'c5'=>true);
$w2_cfg['HandinArr']['hideTeacherCommentBtn']['ls_eng'] 						= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disabledNextBtnIfNotClickedAllModelAnswer']['ls_eng'] 	= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['disableAnswerIfCompletedWriting']['ls_eng'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfSubmittedWriting']['ls_eng'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfCompletedWriting']['ls_eng']				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfSubmittedStep'] ['ls_eng'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfCompletedWriting']['ls_eng'] 			= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['requireInputAllAnswer']['ls_eng'] 						= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>false,'c5'=>false);
$w2_cfg['HandinArr']['getStudentFileInThisStep']['ls_eng'] 						= array('c1'=>false,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>true);

$w2_cfg['HandinArr']['hideApprovalBtn']['sci'] 								= array('c1'=>true,'c2'=>true,'c3'=>true,'c4'=>true,'c5'=>true,'c6'=>true);
$w2_cfg['HandinArr']['hideTeacherCommentBtn']['sci'] 						= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['disabledNextBtnIfNotClickedAllModelAnswer']['sci'] 	= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['disableAnswerIfCompletedWriting']['sci'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>true,'c6'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfSubmittedWriting']['sci'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['hideCheckBtnIfCompletedWriting']['sci']				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfSubmittedStep'] ['sci'] 				= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['showModelAnswerIfCompletedWriting']['sci'] 			= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['requireInputAllAnswer']['sci'] 						= array('c1'=>true,'c2'=>true,'c3'=>false,'c4'=>true,'c5'=>false,'c6'=>false);
$w2_cfg['HandinArr']['getStudentFileInThisStep']['sci'] 					= array('c1'=>false,'c2'=>false,'c3'=>false,'c4'=>false,'c5'=>false,'c6'=>true);

$w2_cfg['HandinArr']['maxRedoAttemptCount'] = 10;
include_once($intranet_root.'/includes/w2/w2ContentInputConfig.inc.php');
include_once($intranet_root.'/includes/w2/w2_contentConfig.inc.php');
/////////////////////////////////////////
//should not have any new line below this
/////////////////////////////////////////
?>