<?php

include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");


class libEngContentBuilder
	{
		
		### XML Eng builder
		##Eng
		function buildGeneralInfoInsert($topicIntro, $topicName, $level, $grade='')
		{
			$returnStr=" '$topicIntro', '$topicName', '$level', '$grade' ";
			return array( 'field'=>'topicIntro, topicName, level, grade', 'content'=>$returnStr); 
		}
		##Chi 
		function buildChiGeneralInfoInsert($topicIntro, $topicName, $category, $grade='')
		{
			$returnStr=" '$topicIntro', '$topicName', '$category', '$grade' ";
			return array( 'field'=>'topicIntro, topicName, category, grade', 'content'=>$returnStr); 
		}		
//		function buildGeneralInfoInsert($topicIntro, $topicName, $level, $category)
//		{
//			$returnStr=" '$topicIntro', '$topicName', '$level', '$category' ";
//			return array( 'field'=>'topicIntro, topicName, level, category', 'content'=>$returnStr); 
//		}
		##Eng
		function buildGeneralInfoUpdate($topicIntro, $topicName, $level, $grade='')
		{
			$fieldName = array('topicIntro','topicName', 'level', 'grade');
			$fieldValue = array("'$topicIntro'", "'$topicName'", "'$level'", "'$grade'");
			$updateArray=array();
			for ($i=0;$i<count($fieldName);$i++)
			{
				$updateArray[] = $fieldName[$i]." = ".$fieldValue[$i];
			}
			return implode(",", (array)$updateArray);  
		}
		##Chi
		function buildChiGeneralInfoUpdate($topicIntro, $topicName, $category, $grade='')
		{
			$fieldName = array('topicIntro','topicName', 'category', 'grade');
			$fieldValue = array("'$topicIntro'", "'$topicName'", "'$category'", "'$grade'");
			$updateArray=array();
			for ($i=0;$i<count($fieldName);$i++)
			{
				$updateArray[] = $fieldName[$i]." = ".$fieldValue[$i];
			}
			return implode(",", (array)$updateArray);  
		}
//		function buildGeneralInfoUpdate($topicIntro, $topicName, $level, $category)
//		{
//			$fieldName = array('topicIntro','topicName', 'level', 'category');
//			$fieldValue = array("'$topicIntro'", "'$topicName'", "'$level'", "'$category'");
//			$updateArray=array();
//			for ($i=0;$i<count($fieldName);$i++)
//			{
//				$updateArray[] = $fieldName[$i]." = ".$fieldValue[$i];
//			}
//			return implode(",", (array)$updateArray);  
//		}
		
		function buildStep1($step1Int,$step1TextType,$step1Purpose,$step1Content)
		{

			$returnStr='';	
			
			$returnStr.='<step1>';	
			$returnStr.='<step1Int>'.'<![CDATA['.$step1Int.']]>'.'</step1Int>';	
			$returnStr.='<step1TextType>'.'<![CDATA['.$step1TextType.']]>'.'</step1TextType>';
			$returnStr.='<step1Purpose>'.'<![CDATA['.$step1Purpose.']]>'.'</step1Purpose>';		
			$returnStr.='<step1Content>'.'<![CDATA['.$step1Content.']]>'.'</step1Content>';		
			$returnStr.='</step1>';	
			return array( 'field'=>'step1Content', 'content'=>"'$returnStr'" );
		}
		function buildChiStep1($step1Data)
		{
			$returnStr='';	
			$returnStr.='<step1>';	
			foreach((array)$step1Data as $_key => $_value){
				$_value = !empty($_value)?$_value:'';
				$returnStr.='<'.$_key.'>'.'<![CDATA['.$_value.']]>'.'</'.$_key.'>';	
			}
			$returnStr.='</step1>';	
			return array( 'field'=>'step1Content', 'content'=>"'$returnStr'" );
		}	
		function buildSciStep2($step2Int,$step2ArticleAry)
		{
			$returnStr='';	
			$returnStr.='<step2>';	
			$returnStr.='<step2Int>'.'<![CDATA['.$step2Int.']]>'.'</step2Int>';	
			$returnStr.='<step2ArticleAry>';
			foreach((array)$step2ArticleAry as $_key => $_ary){
				$returnStr.='<'.$_key.'>';
				foreach((array)$_ary as $__key => $__ary){
					$returnStr.='<'.$__key.'>';
					foreach((array)$__ary as $___key => $___value){
						$___value = !empty($___value)?$___value:'';
						$returnStr.='<'.$___key.'><![CDATA['.$___value.']]></'.$___key.'>';
					}
					$returnStr.='</'.$__key.'>';
				}
				$returnStr.='</'.$_key.'>';
			}
			$returnStr.='</step2ArticleAry>';
			$returnStr.='</step2>';	
			return array( 'field'=>'step2Content', 'content'=>"'$returnStr'");
		}					
		function buildStep2($step2Int)
		{
			$returnStr='';	
			$returnStr.='<step2>';	
			$returnStr.='<step2Int>'.'<![CDATA['.$step2Int.']]>'.'</step2Int>';	
			$returnStr.='</step2>';	
			return array( 'field'=>'step2Content', 'content'=>"'$returnStr'");
		}
		function buildChiStep2($step2Int,$step2Concept,$step2Reference)
		{
			global $w2_cfg;
			$step2Reference['Type'] = !empty($step2Reference['Type'])?$step2Reference['Type']:'normal';
			$step2Int = !empty($step2Int)?$step2Int:'';
			$returnStr='';	
			$returnStr.='<step2>';	
			$returnStr.='<step2Int>'.'<![CDATA['.$step2Int.']]>'.'</step2Int>';	
			$returnStr.='<step2ConceptAry>';
				$_key = 0;
				foreach((array)$step2Concept as $_value){
						$returnStr.='<question'.$_key.'>'.'<![CDATA['.(!empty($_value)?addslashes($_value):'').']]>'.'</question'.$_key.'>';
						$_key++;
				}	
			$returnStr.='</step2ConceptAry>';	
			$returnStr.='<step2ReferenceAry>';
			$returnStr.='<referenceTopic>'.'<![CDATA['.$step2Reference['referenceTopic'].']]>'.'</referenceTopic>';
			$returnStr.='<referenceType>'.'<![CDATA['.$step2Reference['referenceType'].']]>'.'</referenceType>';
			$returnStr.='<referenceData>';
			
			foreach((array)$step2Reference['referenceData'] as $_key => $_dataAry){
				$returnStr.='<'.$_key.'>';
				$i = 0;
				$returnStr.='<itemAry>';
				foreach((array)$_dataAry["itemAry"] as $__value){
					$__caption = !empty($__value['referenceCaption'])?$__value['referenceCaption']:'';
					$__path = !empty($__value['referencePath'])?$__value['referencePath']:'';
					$returnStr.='<referenceItem'.$i.'>';
					$returnStr.='<referenceCaption>'.'<![CDATA['.$__caption.']]>'.'</referenceCaption>';
					$returnStr.='<referencePath>'.'<![CDATA['.$__path.']]>'.'</referencePath>';
					$returnStr.='</referenceItem'.$i.'>';
					$i++;
				}
				$_title = !empty($_dataAry['itemTitle'])?$_dataAry['itemTitle']:'';
				$returnStr.='</itemAry>';
				$returnStr.='<itemTitle>'.'<![CDATA['.$_title.']]>'.'</itemTitle>';
				$returnStr.='</'.$_key.'>';
			}
			
			$returnStr.='</referenceData>';
			$returnStr.='</step2ReferenceAry>';	
			$returnStr.='</step2>';	
			return array( 'field'=>'step2Content', 'content'=>"'$returnStr'");
		}
		function buildLsStep2($step2Int,$step2Source,$updateFile=false){
			global $w2_cfg;
			$step2Int = !empty($step2Int)?$step2Int:'';			
			$step2SourceAry = !empty($step2SourceAry)?$step2SourceAry:'';
			$returnStr='';	
			$returnStr.='<step2>';	
			$returnStr.='<step2Int>'.'<![CDATA['.addslashes($step2Int).']]>'.'</step2Int>';	
			if($updateFile){
				$returnStr.='<step2SourceAry>';
					foreach((array)$step2Source as $_key => $_ary){
						$returnStr.='<'.$_key.'>';
							$returnStr.='<image>'.'<![CDATA['.(!empty($_ary['image'])?addslashes($_ary['image']):'').']]>'.'</image>';
							$returnStr.='<questionAry>';
							foreach((array)$_ary['questionAry'] as $__key => $__value){
								$returnStr.='<item'.$__key.'>';
								$returnStr.='<question>'.'<![CDATA['.(!empty($__value['question'])?addslashes($__value['question']):'').']]>'.'</question>';
								$returnStr.='<answer>'.'<![CDATA['.(!empty($__value['answer'])?addslashes($__value['answer']):'').']]>'.'</answer>';	
								$returnStr.='</item'.$__key.'>';							
							}
							$returnStr.='</questionAry>';
						$returnStr.='</'.$_key.'>';
					}	
				$returnStr.='</step2SourceAry>';					
			}
			$returnStr.='</step2>';	
			return array( 'field'=>'step2Content', 'content'=>"'$returnStr'");
		}	
		function buildStep3($step3Int, $step3Sample, $step3Vocab, $step3Grammar)
		{
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';	
			$returnStr.='<step3SampleWriting>'.'<![CDATA['.$step3Sample.']]>'.'</step3SampleWriting>';	
			$returnStr.='<step3Vocab>'.'<![CDATA['.$step3Vocab.']]>'.'</step3Vocab>';	
			$returnStr.='<step3Grammar>'.'<![CDATA['.$step3Grammar.']]>'.'</step3Grammar>';	
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}
		function buildChiStep3($step3Int,$step3Outline,$step3Writing,$step3RemarkAry){
			global $w2_cfg;
			$step3Int = !empty($step3Int)?$step3Int:'';
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';
			$returnStr.='<step3OutlineAry>';
				$_key = 0;
				foreach((array)$step3Outline as $_value){
						$returnStr.='<question'.$_key.'>'.'<![CDATA['.(!empty($_value)?addslashes($_value):'').']]>'.'</question'.$_key.'>';
						$_key++;
				}	
			$returnStr.='</step3OutlineAry>';	
			$returnStr.='<step3Writing>'.'<![CDATA['.$step3Writing.']]>'.'</step3Writing>';	
			$returnStr.='<step3RemarkAry>';
			foreach((array)$step3RemarkAry as $_key => $_value){
				$_title = !empty($_value['remarkTitle'])?$_value['remarkTitle']:'';
				$_content = !empty($_value['remarkContent'])?$_value['remarkContent']:'';
				if(!empty($_title)||!empty($_content)){
					$returnStr.='<'.$_key.'>';
					$returnStr.='<remarkTitle>'.'<![CDATA['.$_title.']]>'.'</remarkTitle>';
					$returnStr.='<remarkContent>'.'<![CDATA['.$_content.']]>'.'</remarkContent>';
					$returnStr.='</'.$_key.'>';
				}
			}
			$returnStr.='</step3RemarkAry>';					
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}	
		function buildSciStep3($step3Int,$step3RefItemAry)
		{
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';	
			$returnStr.='<step3RefItemAry>';	
				foreach((array)$step3RefItemAry as $_key => $_ary){
					$returnStr.='<'.$_key.'>';	
					foreach((array)$_ary as $__key => $__value){
						$__value = !empty($__value)?$__value:'';
						$returnStr.='<'.$__key.'><![CDATA['.$__value.']]></'.$__key.'>';
					}
					$returnStr.='</'.$_key.'>';		
				}
			$returnStr.='</step3RefItemAry>';	
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}
		function buildLsStep3($step3Int)
		{
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';	
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}			
		function buildChiStep4($step4Int,$step4Approach,$step4ExampleSentence,$step4Vocab,$step4Grammar){
			global $w2_cfg;
			$step4Int = !empty($step4Int)?$step4Int:'';
			$step4ExampleSentence = !empty($step4ExampleSentence)?$step4ExampleSentence:'';
			$step4Vocab = !empty($step4Vocab)?$step4Vocab:'';
			$step4Grammar = !empty($step4Grammar)?$step4Grammar:'';
			$returnStr='';	
			$returnStr.='<step4>';	
			$returnStr.='<step4Int>'.'<![CDATA['.$step4Int.']]>'.'</step4Int>';	
			$returnStr.='<step4ApproachAry>';
				$_key = 0;
				foreach((array)$step4Approach as $_value){
						$returnStr.='<question'.$_key.'>'.'<![CDATA['.(!empty($_value)?addslashes($_value):'').']]>'.'</question'.$_key.'>';
						$_key++;
				}	
			$returnStr.='</step4ApproachAry>';				
			$returnStr.='<step4Vocab>'.'<![CDATA['.$step4Vocab.']]>'.'</step4Vocab>';	
			$returnStr.='<step4Grammar>'.'<![CDATA['.$step4Grammar.']]>'.'</step4Grammar>';	
			$returnStr.='<step4ExampleSentence>'.'<![CDATA['.$step4ExampleSentence.']]>'.'</step4ExampleSentence>';	
			$returnStr.='</step4>';	

			return array( 'field'=>'step4Content', 'content'=>"'$returnStr'");					
		}
		function buildLsStep4($step4Int,$step4Outline){
			global $w2_cfg;
			$step4Int = !empty($step4Int)?$step4Int:'';
			$returnStr='';	
			$returnStr.='<step4>';	
			$returnStr.='<step4Int>'.'<![CDATA['.$step4Int.']]>'.'</step4Int>';	
			$returnStr.='<step4OutlineAry>';
				$_key = 0;
				foreach((array)$step4Outline as $_value){
						$returnStr.='<question'.$_key.'>'.'<![CDATA['.(!empty($_value)?addslashes($_value):'').']]>'.'</question'.$_key.'>';
						$_key++;
				}	
			$returnStr.='</step4OutlineAry>';	
			$returnStr.='</step4>';	

			return array( 'field'=>'step4Content', 'content'=>"'$returnStr'");					
		}		
		function buildSciStep4($step4Data,$saveHighlightWord=false)
		{
			global $w2_cfg;		
			$returnStr='';	
			$returnStr.='<step4>';	
			foreach((array)$step4Data as $_key => $_value){
				$returnStr .= '<'.$_key.'>';
				if($_key=='step4ParagraphAry'||$_key=='step4RemarkAry'){
					foreach((array)$_value as $__key => $__ary){
						$returnStr.='<'.$__key.'>';
						foreach((array)$__ary as $___key => $___value){
							$___value = !empty($___value)?$___value:'';
							if($saveHighlightWord){
								$___value = addslashes($___value);
							}
							$returnStr.='<'.$___key.'><![CDATA['.$___value.']]></'.$___key.'>';
						}
						$returnStr.='</'.$__key.'>';
					}
				}elseif($_key=='HightlightWordsAry'){
					$returnStr.='<vocabularies>';
					foreach((array)$_value['vocabularies'] as $__key => $__value){
						$__word = !empty($__value['word'])?$__value['word']:'';
						$__meaning = !empty($__value['meaning'])?$__value['meaning']:'';
						if(!$saveHighlightWord){
							$__word = addslashes($__word);
							$__meaning = addslashes($__meaning);
						}
						$returnStr.='<'.$__key.'>';
							$returnStr.='<word><![CDATA['.$__word.']]></word>';
							$returnStr.='<meaning><![CDATA['.$__meaning.']]></meaning>';
						$returnStr.='</'.$__key.'>';
					}	
					$returnStr.='</vocabularies>';		
					$returnStr.='<expressions>';
					foreach((array)$_value['expressions'] as $__key => $__value){
						$__word = !empty($__value['word'])?$__value['word']:'';
						$__meaning = !empty($__value['meaning'])?$__value['meaning']:'';
						if(!$saveHighlightWord){
							$__word = addslashes($__word);
							$__meaning = addslashes($__meaning);
						}
						$returnStr.='<'.$__key.'>';
							$returnStr.='<word><![CDATA['.$__word.']]></word>';
							$returnStr.='<meaning><![CDATA['.$__meaning.']]></meaning>';
						$returnStr.='</'.$__key.'>';
					}	
					$returnStr.='</expressions>';									
				}else{
					$_value = !empty($_value)?$_value:'';
					if($saveHighlightWord){
						$_value = addslashes($_value);
					}
					$returnStr.= '<![CDATA['.$_value.']]>';
				}
				$returnStr .= '</'.$_key.'>';
			}					
			$returnStr.='</step4>';	

			return array( 'field'=>'step4Content', 'content'=>"'$returnStr'");
		}	
		function buildSciStep5($step5Int,$step5Outline){
			global $w2_cfg;
			$step4Int = !empty($step5Int)?$step5Int:'';
			$returnStr='';	
			$returnStr.='<step5>';	
			$returnStr.='<step5Int>'.'<![CDATA['.$step5Int.']]>'.'</step5Int>';	
			$returnStr.='<step5OutlineAry>';
				$_key = 0;
				foreach((array)$step5Outline as $_value){
						$returnStr.='<question'.$_key.'>'.'<![CDATA['.(!empty($_value)?addslashes($_value):'').']]>'.'</question'.$_key.'>';
						$_key++;
				}	
			$returnStr.='</step5OutlineAry>';	
			$returnStr.='</step5>';	

			return array( 'field'=>'step5Content', 'content'=>"'$returnStr'");					
		}	
		function buildSciStep6($step6Int, $step6DefaultWriting)
		{
			$returnStr='';	
			$returnStr.='<step6>';	
			$returnStr.='<step6Int>'.'<![CDATA['.$step6Int.']]>'.'</step6Int>';			
			$returnStr.='<step6DefaultWriting>'.'<![CDATA['.$step6DefaultWriting.']]>'.'</step6DefaultWriting>';			
			$returnStr.='</step6>';	
			
			return array( 'field'=>'step6Content', 'content'=>"'$returnStr'");
		}						
		function buildStep3New($step3Int, $step3ParagraphAry, $step3RemarkAry, $step3Vocab, $step3Grammar)
		{
			global $w2_cfg;
			$returnStr='';	
			$returnStr.='<step3>';	
			$returnStr.='<step3Int>'.'<![CDATA['.$step3Int.']]>'.'</step3Int>';
			$returnStr.='<step3ParagraphAry>';
			$paragraphCnt = count($step3ParagraphAry);
			for($i=0;$i<$paragraphCnt;$i++){
				$returnStr.='<paragraphItem'.($i+1).'>';
				foreach((array)$step3ParagraphAry[$i] as $_key => $_value){
					$returnStr.='<'.$_key.'>';
					$returnStr.='<![CDATA['.$_value.']]>';
					$returnStr.='</'.$_key.'>';
				}
				$returnStr.='</paragraphItem'.($i+1).'>';
			}			
			$returnStr.='</step3ParagraphAry>';	
			$returnStr.='<step3RemarkAry>';
			for($i=0;$i<$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
				if(count($step3RemarkAry[$i])>0){
					$returnStr.='<remarkItem'.($i+1).'>';
					foreach((array)$step3RemarkAry[$i] as $_key => $_value){
							$returnStr.='<'.$_key.'>';
							$returnStr.='<![CDATA['.$_value.']]>';
							$returnStr.='</'.$_key.'>';
					}
					$returnStr.='</remarkItem'.($i+1).'>';
				}
			}	
			$returnStr.='</step3RemarkAry>';		
			$returnStr.='<step3Vocab>'.'<![CDATA['.$step3Vocab.']]>'.'</step3Vocab>';	
			$returnStr.='<step3Grammar>'.'<![CDATA['.$step3Grammar.']]>'.'</step3Grammar>';	
			$returnStr.='</step3>';	

			return array( 'field'=>'step3Content', 'content'=>"'$returnStr'");
		}
				
		function buildStep4($step4Int, $step4DraftQ)
		{
			$returnStr='';	
			$returnStr.='<step4>';	
			$returnStr.='<step4Int>'.'<![CDATA['.$step4Int.']]>'.'</step4Int>';	
			$returnStr.='<step4DraftQList>';
		
			foreach( (array)$step4DraftQ as $step4DraftEachQ)
			{
				$returnStr.='<step4DraftQ>'.'<![CDATA['.trim(stripslashes($step4DraftEachQ)).']]>'.'</step4DraftQ>';	
			}
			$returnStr.='</step4DraftQList>';	
			$returnStr.='</step4>';	
			return array( 'field'=>'step4Content', 'content'=>"'$returnStr'");
		
		}
		
		function buildStep5($step5Int, $step5DefaultWriting)
		{
			$returnStr='';	
			$returnStr.='<step5>';	
			$returnStr.='<step5Int>'.'<![CDATA['.$step5Int.']]>'.'</step5Int>';			
			$returnStr.='<step5DefaultWriting>'.'<![CDATA['.$step5DefaultWriting.']]>'.'</step5DefaultWriting>';			
			$returnStr.='</step5>';	
			
			return array( 'field'=>'step5Content', 'content'=>"'$returnStr'");
		}
		
		function buildResource($refObject)
		{
			$i=1;
			$returnStr='';			
			$returnStr.='<resource>';
				
			foreach((array)$refObject as $refItem)
			{
				if(!empty($refItem['resourceName'])&&!empty($refItem['resourceWebsite'])){
					$returnStr.='<resourceItem'.$i.'>';
						$returnStr.='<resourceItemName>';
							$returnStr.='<![CDATA['.$refItem['resourceName'].']]>';
						$returnStr.='</resourceItemName>';
						$returnStr.='<resourceItemWebsite>';
							$returnStr.='<![CDATA['.$refItem['resourceWebsite'].']]>';
						$returnStr.='</resourceItemWebsite>';
						
						
						$returnStr.='<resourceItemChecked>';	
							foreach((array)$refItem['checked'] as $key => $value)
							{
								$returnStr.='<'.$value.'>';	
								$returnStr.='<![CDATA[yes]]>';
								$returnStr.='</'.$value.'>';	
							}
						$returnStr.='</resourceItemChecked>';
					$returnStr.='</resourceItem'.$i.'>';	
					++$i;
				}
			}
			$returnStr.='</resource>';				
			return array( 'field'=>'resource', 'content'=>"'{$returnStr}'" );
		}
		function buildTeacherAttachment($attachObject)
		{
			$returnStr='<TeacherAttachment>';			
			
			foreach((array)$attachObject as $_step => $_attachItem)
			{	
				$returnStr.='<teacherAttachment_'.$_step.'>';
				$i=1;
				foreach((array)$_attachItem as $__attachAry)
				{
					$returnStr.='<teacherAttachmentItem'.$i.'>';
						$returnStr.='<teacherAttachmentItemName>';
							$returnStr.='<![CDATA['.$__attachAry['filename'].']]>';
						$returnStr.='</teacherAttachmentItemName>';
						$returnStr.='<teacherAttachmentItemFile>';
							$returnStr.='<![CDATA['.$__attachAry['filepath'].']]>';
						$returnStr.='</teacherAttachmentItemFile>';
					$returnStr.='</teacherAttachmentItem'.$i.'>';				
					$i++;
				}
				$returnStr.='</teacherAttachment_'.$_step.'>';
			}				
			$returnStr.='</TeacherAttachment>';
			
			return array( 'field'=>'teacherAttachment', 'content'=>"'{$returnStr}'" );
		}		
		### XML Eng Builder End
		
		### DB Data	Change
		function buildDBInfoInsert($r_contentCode,$schemeNum)
		{
			GLOBAL $w2_cfg;
			$status=$w2_cfg["contentWriting"]["W2_TEMP"];
			$schoolCode = $w2_cfg["schoolCodeCurrent"];
			$returnStr=" '$r_contentCode',$status , now(), $_SESSION[UserID], now(), $_SESSION[UserID], $schemeNum,  '".strtolower($schoolCode)."'";
			
			return array( 'field'=>'r_contentCode, 
							status, 
							dateInput, 
							createdBy,
							dateModified, 
							modifiedBy,
							schemeNum,
							schoolCode'
							,
						 	'content'=>$returnStr);	
		}
		
		function buildDBInfoUpdate($r_contentCode)
		{
			
			return array( "r_contentCode = '${r_contentCode}'",
							"dateModified = now()", 
							"modifiedBy =  ${_SESSION[UserID]}"
							);	
		}
		### DB Data	Change End
		
		### Other helper functions
		function getNextSchemeNum($r_contentCode)
		{
			$objDB = new libdb();
			$sql="SELECT (MAX(schemeNum)+1) as nextNum FROM W2_CONTENT_DATA WHERE r_contentCode = '$r_contentCode'; ";
			$nextSchemeNum = $objDB->returnResultSet($sql);
			return ($nextSchemeNum[0]['nextNum']==''?STARTING_SCHEME_NUM:$nextSchemeNum[0]['nextNum']);
		}
		### Other helper functions End
			
		
		
	}
?>