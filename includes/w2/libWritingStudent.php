<?
// using: fai

class libWritingStudent {
	private $objDB;
	private $tableName;
	private $primaryKeyName;
	
	private $writingStudentId;
	private $writingId;
	private $studentUserId;
	private $inputBy;
	private $handinSubmitStatus;
	private $handinSubmitStatusDate;
	private $otherInfo;
	private $dateInput;
	private $dateModified;
	private $deleteStatus;
	private $deletedBy;
	private $dateDelete;
	

	public function __construct($writingStudentId = NULL) {
		global $w2_cfg,$intranet_db;

		$this->objDb = new libdb();
		
		$this->setTableName('W2_WRITING_STUDENT');
		$this->setPrimaryKeyName('WRITING_STUDENT_ID');

		if(is_numeric($writingStudentId)){
			$this->setWritingStudentId($writingStudentId);
			$this->loadRecordFromStorage();
		}
    }
    
    /**
	* LOAD writing details from storage with a writing ID
	* @owner : Fai (201000922)
    * @input : NIL
	* @return : NIL
	* 
	*/
	private function loadRecordFromStorage(){
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		
		$sql = "select * from $tableName where $primaryKeyName = $primaryKeyValue";
		$result = current($this->objDb->returnResultSet($sql));

		$this->setWritingStudentId($result['WRITING_STUDENT_ID']);
		$this->setWritingId($result['WRITING_ID']);
		$this->setStudentUserId($result['USER_ID']);
		$this->setInputBy($result['INPUT_BY']);
		$this->setHandinSubmitStatus($result['HANDIN_SUBMIT_STATUS']);
		$this->setHandinSubmitStatusDate($result['HANDIN_SUBMIT_STATUS_DATE']);
		$this->setOtherInfo($result['OTHER_INFO']);
		$this->setDateInput($result['DATE_INPUT']);
		$this->setDateModified($result['DATE_MODIFIED']);
		$this->setDeleteStatus($result['DELETE_STATUS']);
		$this->setDeletedBy($result['DELETED_BY']);
		$this->setDateDelete($result['DATE_DELETE']);
	}

////START GET SET///
	private function setTableName($str) {
		$this->tableName = $str;
	}
	private function getTableName() {
		return $this->tableName;
	}
	
	private function setPrimaryKeyName($str) {
		$this->primaryKeyName = $str;
	}
	private function getPrimaryKeyName() {
		return $this->primaryKeyName;
	}
	
	private function getPrimaryKeyValue() {
		return $this->writingStudentId;
	}
	
	public function setWritingStudentId($int) {
		$this->writingStudentId = $int;
	}
	public function getWritingStudentId() {
		return $this->writingStudentId;
	}
	
	public function setWritingId($int) {
		$this->writingId = $int;
	}
	public function getWritingId() {
		return $this->writingId;
	}
	
	public function setStudentUserId($int) {
		$this->studentUserId = $int;
	}
	public function getStudentUserId() {
		return $this->studentUserId;
	}
	
	public function setInputBy($int) {
		$this->inputBy = $int;
	}
	public function getInputBy() {
		return $this->inputBy;
	}
	
	public function setHandinSubmitStatus($int) {
		$this->handinSubmitStatus = $int;
	}
	public function getHandinSubmitStatus() {
		return $this->handinSubmitStatus;
	}
	
	public function setHandinSubmitStatusDate($date) {
		$this->handinSubmitStatusDate = $date;
	}
	public function getHandinSubmitStatusDate() {
		return $this->handinSubmitStatusDate;
	}
	
	public function setOtherInfo($str) {
		$this->otherInfo = $str;
	}
	public function getOtherInfo() {
		return $this->otherInfo;
	}
	
	public function setDateInput($date) {
		$this->dateInput = $date;
	}
	public function getDateInput() {
		return $this->dateInput;
	}
	
	public function setDateModified($date) {
		$this->dateModified = $date;
	}
	public function getDateModified() {
		return $this->dateModified;
	}
	
	public function setDeleteStatus($int) {
		$this->deleteStatus = $int;
	}
	public function getDeleteStatus() {
		return $this->deleteStatus;
	}
	
	public function setDeletedBy($int) {
		$this->deletedBy = $int;
	}
	public function getDeletedBy() {
		return $this->deletedBy;
	}
	
	public function setDateDelete($date) {
		$this->dateDelete = $date;
	}
	public function getDateDelete() {
		return $this->dateDelete;
	}
	
	

////END GET SET///

	public function save() {
		$primaryKey = $this->getPrimaryKeyValue();
		if (is_numeric($primaryKey)) {
			$newPrimaryKey = $this->updateDb();
		}
		else {
			$newPrimaryKey = $this->insertDb();
		}
		
		return $newPrimaryKey;
	}
	
	private function updateDb() {
		# Prepare value for SQL update
		$DataArr = array();
		$DataArr["WRITING_ID"]					= $this->objDb->pack_value($this->getWritingId(), "int");
		$DataArr["USER_ID"]						= $this->objDb->pack_value($this->getStudentUserId(), "int");
		$DataArr["HANDIN_SUBMIT_STATUS"]		= $this->objDb->pack_value($this->getHandinSubmitStatus(), "int");
		$DataArr["HANDIN_SUBMIT_STATUS_DATE"]	= $this->objDb->pack_value($this->getHandinSubmitStatusDate(), "date");
		$DataArr["OTHER_INFO"]					= $this->objDb->pack_value($this->getOtherInfo(), "str");
		$DataArr["DATE_MODIFIED"]				= $this->objDb->pack_value($this->getDateModified(), "date");
		$DataArr["DELETE_STATUS"]				= $this->objDb->pack_value($this->getDeleteStatus(), "int");
		$DataArr["DELETED_BY"]					= $this->objDb->pack_value($this->getDeletedBy(), "int");
		$DataArr["DATE_DELETE"]					= $this->objDb->pack_value($this->getDateDelete(), "date");
		
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $_field => $_value)
		{
			$valueFieldArr[] = " $_field = $_value ";
		}
		$valueFieldText .= implode(',', $valueFieldArr);
		
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		$sql = "Update $tableName Set $valueFieldText Where $primaryKeyName = '$primaryKeyValue'";
		$success = $this->objDb->db_db_query($sql);
		
		$this->loadRecordFromStorage();
		return $this->getPrimaryKeyValue();
	}
	
	private function insertDb() {
		// the record should be inserted in libWriting.php function addStudentToWriting()
		return false;
	}
}
?>