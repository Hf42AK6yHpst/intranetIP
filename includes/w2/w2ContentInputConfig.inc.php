<?php
//Subject Which hv content input engine
$w2_cfg['contentInput']['allowContentCode'] = array('chi','eng','ls_eng','ls','sci');
$w2_cfg['contentInput']['mindMapStep'] = array('chi'=>'2','eng'=>'2','ls_eng'=>'3','ls'=>'3','sci'=>'3');
//templateName
$w2_cfg['contentInput']['templateName'] = array();
$w2_cfg['contentInput']['templateName']['eng'] = 'newTemplateWriting';
$w2_cfg['contentInput']['templateName']['chi'] = 'newChiTemplateWriting';
$w2_cfg['contentInput']['templateName']['ls_eng'] = 'newLsEngTemplateWriting';
$w2_cfg['contentInput']['templateName']['ls'] = 'newLsEngTemplateWriting';
$w2_cfg['contentInput']['templateName']['sci'] = 'newSciTemplateWriting';
//basic setting
$w2_cfg['contentInput']['defaultInputNum'] = 2;
$w2_cfg['contentInput']['maxInputQNum'] = 10;
$w2_cfg['contentInput']['SampleWritingDefaultParagraphNum'] = 3;
$w2_cfg['contentInput']['SampleWritingLeftBubbleNum'] = 8; //related to writing20_newlayout.css .bubble_01 - .bubble_08
$w2_cfg['contentInput']['SampleWritingRemarkNum'] = 10; //related to writing20_newlayout.css .remark_highlight_01 - .remark_highlight_10
$w2_cfg['contentInput']['SampleWritingAction']['new'] = 1;
$w2_cfg['contentInput']['SampleWritingAction']['edit'] = 2;

//Chi step2
$w2_cfg['contentInput']['step2ReferenceType'] = array('normal','compare');

// Sample Writing Highlight Mapping
$w2_cfg["SampleWriting"]["HighlightMapping"] = array();
$w2_cfg["SampleWriting"]["HighlightMapping"]['01'] = "red";
$w2_cfg["SampleWriting"]["HighlightMapping"]['02'] = "orange";
$w2_cfg["SampleWriting"]["HighlightMapping"]['03'] = "yellow";
$w2_cfg["SampleWriting"]["HighlightMapping"]['04'] = "lightgreen";
$w2_cfg["SampleWriting"]["HighlightMapping"]['05'] = "green";
$w2_cfg["SampleWriting"]["HighlightMapping"]['06'] = "lightblue";
$w2_cfg["SampleWriting"]["HighlightMapping"]['07'] = "blue";
$w2_cfg["SampleWriting"]["HighlightMapping"]['08'] = "darkblue";
$w2_cfg["SampleWriting"]["HighlightMapping"]['09'] = "purple";
$w2_cfg["SampleWriting"]["HighlightMapping"]['10'] = "pink";


?>