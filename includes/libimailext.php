<?php
if (!defined("LIBIMAILEXT_DEFINED"))         // Preprocessor directives
{

 define("LIBIMAILEXT_DEFINED",true);

class libimailext extends libdb{

     # ------------------------------------------------------------------------------------

     function libimailext(){
          $this->libdb();
     }

     function synAliasCount($AliasID,$type)
     {
              if ($type==1)
              {
                  $entrytable = "INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY";
                  $aliastable = "INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL";
              }
              else
              {
                  $entrytable = "INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY";
                  $aliastable = "INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL";
              }
              $sql = "SELECT COUNT(*) FROM $entrytable WHERE AliasID = '$AliasID'";
              $temp = $this->returnVector($sql);
              $count = $temp[0]+0;

              $sql = "UPDATE $aliastable SET NumberOfEntry = '$count' WHERE AliasID = '$AliasID'";
              $this->db_db_query($sql);
     }
     function removeAttachmentDBRecords($target_id)
     {
              if (!is_array($target_id) || sizeof($target_id)==0)
              {
                  return;
              }
              $list = implode(",",$target_id);
              /*
              $sql = "SELECT a.PartID FROM INTRANET_IMAIL_ATTACHMENT_PART as a
                             WHERE CampusMailID IN ($list)";
                             */
              $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE CampusMailID IN ($list)";
              $this->db_db_query($sql);
     }
     function clearAttachmentDBRecords()
     {
              $sql = "SELECT a.PartID FROM INTRANET_IMAIL_ATTACHMENT_PART as a
                             LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON a.CampusMailID = b.CampusMailID OR a.CampusMailID = b.CampusMailFromID
                      WHERE b.CampusMailID IS NULL";
              $unlink = $this->returnVector($sql);
              if (sizeof($unlink)!=0)
              {
                  $list = implode(",",$unlink);
                  $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE PartID IN ($list)";
                  $this->db_db_query($sql);
              }
     }
}

} // End of directives
?>