<?php
//	Using:
/**
 * Modification Log
 * 2018-11-22 (Pun) [149038] [ip.2.5.10.1.1]
 *      - Fix cannot read number after new line correctly.
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
/****************************************************************************
 *  Variables declared in this class.
 ***************************************************************************/
$ser_url = "servlet/BLECServlet";
$act_url = "action=tts&username=";
$voice_url = "&voice=".$_REQUEST["voice"]."&speed=".$_REQUEST["speed"];
$text_url = "&inputText=";
$in_str =  stripslashes($_REQUEST["inputText"]);
$limit_in_str="";
//$tts_groupid = "BLECUserGroup"; //test for cantonese

/****************************************************************************
 *	Check input text language type.
 *	We define the language type of the input text as Chinese if 
 *	there exits one or more Chinese character.
 *	Otherwise we define it as English. Both cases accept special
 *	characters. We limit the word count to 2500 for Chinese/English input.
 *  2500 is the suggested max no. of word the TTS engine can handle each time
****************************************************************************/
$tts_exclude_chinese = array("—", "©", "＼", "￡", "℃","℉", "＄", "￠", "㎡", "﹫", "㊣", "∮", "＊", "∞", "≠", "≧", "≦", "√", "÷", "≡", "﹢", "↑", "↓", "←", "→", "↖", "↗", "↙", "↘", "∥", "①", "②", "③", "④", "⑤", "⑥", "⑦", "⑧", "⑨", "⑩", "⑴", "⑵","⑶","⑷","⑸","⑹","⑺","⑻","⑼","⑽","［","］","ˆ","€","Œ","Š","Ž","“","”","•","˜˜˜˜˜™","š","›","œ","ž","Ÿ","¡","¢","£","¤","¥","¦","§","¨","ª","«","®","±","²","³","´","µ","¶","·","¸","¹","º","»","¼","½","¾","¿","Æ","♠","♣","♦","♥","‾","…","‰","‘","’","Ä","ñ","º","µ","↕","Þ","È","Ã","ℓ","ö","Ö",".","ò","ô","é","ã");
$tts_word_limit = 2500;

function string_to_array ($string) {

	$arrayInput = array();
		$strlen = mb_strlen($string);
		for($i=0; $i<$strlen; $i++) {
			 $arrayInput[] = mb_substr($string, $i, 1, 'UTF-8');
		}
		return $arrayInput;
}

if($_REQUEST["inputTextLang"] == "c" ){ 
		$lanURL = "/goChinese/";		
}else if($_REQUEST["inputTextLang"] == "e"){		
		$lanURL = "/goEnglish/";
}else{	
	$lanURL = "/goEnglish/";
		
	foreach(string_to_array ($in_str) as $chr){
		if(hexdec(sprintf("%02X", ord($chr)))>128 && !in_array($chr, $tts_exclude_chinese)) {	
			$lanURL = "/goChinese/";
			break;	
		}
	}
	
}

//Limit the data from input text
if($lanURL == "/goEnglish/")
{	
	# values of speed in English are different from Chinese! 
	$speed_value = ($_REQUEST["speed"]!=2) ? $_REQUEST["speed"]+1 : 5;
	$voice_url = "&voice=".$_REQUEST["voice"]."&speed=".$speed_value;
	
	$extraChar = "1234567890";
	$temp_str = array_keys(str_word_count($in_str, 2));
	(count($temp_str)> $tts_word_limit ? $limit_in_str =  substr($in_str, 0, $temp_str[$tts_word_limit]-1) : $limit_in_str = $in_str);

} else
{
	$limit_in_str = mb_substr($in_str, 0, $tts_word_limit,'UTF-8');
	
	//test for cantonese
	/*
	$tts_server_url = "http://202.123.82.43";
	$code1 = time();
	$code2 = MD5("BLECtts09".$tts_groupid.$tts_user.$code1);	
	$cantonese_param = "&groupId=$tts_groupid&code1=$code1&code2=$code2";
	*/
	//test for cantonese
}

$limit_in_str = str_replace(array("\r\n", "\n", "\r"), " ", $limit_in_str);
$rawdata = rawurlencode($limit_in_str);
//$post_data = $act_url.$tts_user.$voice_url.$text_url.$rawdata.$cantonese_param; //test for cantonese
$post_data = $act_url.$tts_user.$voice_url.$text_url.$rawdata; //test for cantonese

//--- curl ---
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $tts_server_url.$lanURL.$ser_url); 
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

//echo "0|=|".$tts_server_url.$lanURL.$ser_url."?".$post_data; exit;
$ReturnMessage = curl_exec($curl);
## Uncomment below to check whether license has expired.
//echo "0|=|".$ReturnMessage.$limit_in_str; die();

if( $ReturnMessage === false ){

    echo $Lang['TextToSpeech']['Error'];
      
}else{
	
	if((substr($ReturnMessage, -4) == ".mp3") && (substr($ReturnMessage, 0, 1) == "/" )){
		
		echo "1|=|".$tts_server_url.$ReturnMessage;
		
	}else{
		
		echo $Lang['TextToSpeech']['Error'];

	}
   
}
curl_close($curl);
echo '?';
echo $tts_server_url.$lanURL.$ser_url;
?>