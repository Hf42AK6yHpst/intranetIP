<?php
// Using : 

/*
 * ################### Change Log ###################
 * ##
 * ## Date: 2020-01-13 Tommy
 * ## - added getCancelledBooking() to show cancelled record
 * ##
 * ## Date: 2020-01-08 Cameron
 * ## - fix bug: should cast $RecipientUserIDArr as array before apply array_unique and implode in SendMailToManageGroup_Merged()
 * ## - fix bug: should keep TimeSlotID and TimeSlotName after apply Facility_Time_Range_Intersect() in Get_Facility_Available_Booking_Period_Of_User() for multiple items booking in EverydayTimetable
 * ##
 * ## Date: 2019-11-20 Cameron
 * ## - add function getTimeslotListOfTimetableWithMaxNumOfSlotsByDate(), getDisplayOrderByPeriodID()
 * ##
 * ## Date: 2019-11-07 Cameron
 * ## - add parameter $startDate and $endDate to Is_Facility_Booking_Period_Set()
 * ##
 * ## Date: 2019-11-06 Cameron
 * ## - modify Get_Facility_Available_Booking_Period_For_Booking(), should apply Special Booking Period for EverydayTimetable [case #M165759]
 * ## - add function getDisplayOrderByTimeSlotID()
 * ##
 * ## Date: 2019-10-18 Cameron
 * ## - add parameter $From_eAdmin to Check_If_Facility_Is_Available() and pass the argument to Get_Facility_Available_Booking_Period_Of_User() in it
 * ## - pass argument $From_eAdmin = 1 to Check_If_Facility_Is_Available() in New_Room_Reserve()
 * ##
 * ## Date: 2019-09-18 Tommy
 * ## - add "BookingApproveNotification" checking in SendEmailToFollowUpGroup_Merged
 * ##
 * ## Date: 2019-09-11 Cameron
 * ## - add parameter $includeUnbookable to getAllBookableCategoryItem() [case #K169643]
 * ##
 * ## Date: 2019-09-10 Cameron
 * ## - add function getUnbookableLocation(), getUnbookableItem()
 * ## - add parameter $includeUnbookable to getAllBookableLocationName() [case #K169643]
 * ##
 * ## Date: 2019-07-02 Cameron
 * ## - add function getBookingIDByDate()
 * ## - add function rejectBookingByDate()
 * ##
 * ## Date: 2019-06-26 Cameron
 * ## modify getSkipSchoolDates(), change facilityID to array 
 * ##
 * ## Date: 2019-06-25 Cameron
 * ## - fix: should use $_ts instead $ts in foreach loop in getAvailableBookingPeriodFromEverydayTimetable(), one of the parameters is $facilityType
 * ## - add function getSkipSchoolDates()
 * ## - by pass checking available booking period for user in Check_If_Facility_Is_Available() if $sys_custom['eBooking']['EverydayTimetable'] = true
 * ##
 * ## Date: 2019-06-20 Henry
 * ## - modified Get_Facility_Booking_Record() to fix the problem of item with same name [Case#R161059]
 * ##
 * ## Date: 2019-06-13 Cameron
 * ## - add function getBookingIDByTimeSlot(), rejectBookingByTimeSlot()
 * ##
 * ## Date: 2019-06-11 Cameron
 * ## - add function getActualTimeslotInfoByDefaltTimeslot(), updateBookingTimeslotInfo(), updateBookingTimeInfo()
 * ##
 * ## Date: 2019-06-04 Cameron
 * ## - add function getAvailableBookingPeriodFromEverydayTimetable()
 * ## - call getAvailableBookingPeriodFromEverydayTimetable in Get_Facility_Available_Booking_Period_For_Booking()
 * ##
 * ## Date: 2019-05-20 Cameron
 * ## - add TimeSlotID in New_Room_Reserve(), New_Room_Booking(), New_Item_Reserve(), New_Item_Booking() [case #M141869]
 * ##
 * ## Date: 2019-05-13 Cameron
 * ## - fix potential sql injection problem by enclosing var with apostrophe
 * ##
 * ## Date: 2019-04-15 Isaac
 * ## modified getMappingBookingIDtoTimeslot() roundup both $_StartTimeTimestamp and $_EndTimeTimestamp to $MappingInterval_Second
 * ## 
 * ## Date: 2019-01-24 Cameron
 * ## - return send email result in SendMailToManageGroup_Merged(), SendEmailToFollowUpGroup_Merged() 
 * ## - add $this->Load_General_Setting(); in SendMailToManageGroup_Merged()
 * ##
 * ## Date:	2018-01-17 Isaac
 * ## modify GET_MODULE_OBJ_ARR(), unset menu for ebooking report in PowerClass 
 * ## Date:	2018-01-08 Isaac
 * ## modify GET_MODULE_OBJ_ARR() to set Menu Items for PowerClass
 * ##
 * ## Date: 2018-12-12 Cameron
 * ## update ProcessDateTime field in Reject_Approved_Or_Pending_Booking_By_DateTimeArr()
 * ## add ProcessDateTime field in Insert_Room_Booking_Record(), Insert_Item_Booking_Record()
 * ##
 * ## Date: 2018-12-11 Cameron
 * ## retrieve ProcessDateTime in Get_All_Facility_Booking_Record() 
 * ##
 * ## Date: 2018-11-28 Isaac
 * ## removed $sys_custom['eBookingSendMailWithRemark'] to generalise including booking remarks in sys mails  
 * ##
 * ## Date: 2018-08-30 Cameron
 * ## change $PATH_WRT_ROOT to $intranet_root in Get_Facility_Booking_Record() to avoid error in eEnrolment-HKPF generate course
 * ##
 * ## Date: 2018-08-28 Isaac 
 * ## Fixed Get_Facility_Booking_Record() and Get_FollowUp_Page_Group_And_Display_Color() sql now return correct info.
 * ##
 * ## Date: 2018-28-02 Isaac
 * ## modified Get_Facility_Booking_Record(), set Mysql group_concat_max_len by SET SESSION group_concat_max_len befire running the result query
 * ##
 * ## Date : 2018-05-02 Henry
 * ## modified EventCalID() - support to update EventCalID
 * ##
 * ## Date : 2018-03-22 Omas
 * ## modified Get_Settings_ManagementGroup_MemberList_Sql(), Get_Settings_FollowUpGroup_MemberList_Sql() - display archived user
 * ##
 * ## Date : 2018-01-11 Isaac (2018-03-12 Isaac: rollbacked)
 * ## modified Get_All_Facility_Booking_Record(), added 2 stripslashes() function to $keyword
 * ##
 * ## Date : 2017-01-09 Omas #R133710
 * ## modified SendMailToManageGroup_Merged(). return true to avoid notification if turned off notification email
 * ##
 * ## Date : 2017-10-06 Simon #T126579
 * ## Add sys_custom flag to send email to SCL users
 * ## $sys_custom['eBooking']['SendEmailToSpecialUserLoginAry']
 * ##
 * ## Date : 2017-09-29 Omas #L127101
 * ## added Has_Any_Special_Timetable(), modified isSameTimeTable()
 * ## - continue for #L126525 fix for repeat booking - if same special timetable can book, if normal with special timetable then follow normal
 * ##
 * ## Date : 2017-09-19 Simon #Y124731
 * ## Update Get_DateRange_By_DateType function with new critieria "Current"
 * ##
 * ## Date : 2017-07-17 Villa #P100181 , #S123030
 * ## modified Insert_User_Booking_Rule() - add DisableBooking
 * ## modified Update_User_Booking_Rule() - add DisableBooking
 * ## modified Get_User_Booking_Rule() - add DisableBooking
 * ## modified getFacilityBookingAvailableDateRange_MultiItem() - support DisableBooking/ fix sql error with item user group
 * ##
 * ## Date : 2017-05-19 Villa #Z117075
 * ## modified New_Item_Booking() - optimize sending merge mail logic
 * ## modified SendMailToManageGroup_Merged - add sent by system
 * ##
 * ## Date : 2017-05-18 Villa #E117054
 * ## Modified getAllBookableCategoryItem() - fix trim warning
 * ##
 * ## Date : 2017-05-17 Villa #Z117075
 * ## Modified Email_Booking_Result_Merged() - handle the bookingstatus pending
 * ## Modified New_Item_Booking() - only send the email when there is approved item in that bookingID
 * ##
 * ## Date : 2017-05-09 Villa #E116736
 * ## Modified Get_Facility_With_Specific_Booking_Period - fix showing warning if there is no data in Arr1
 * ##
 * ## Date : 2017-04-10 Villa #G115068
 * ## Modified Email_Booking_Result_Merged() - improve detail data array structure: support multi-item
 * ## - improve sending email with item booking status/ rejected reason(if rejected)
 * ##
 * ## Date : 2017-03-23 Villa #W114642
 * ## Modified getAllBookableCategoryItem() - add sql cond to filter out AllowBookingIndependence = 0
 * ## Modified getAllBookableCategoryItemAssoc() - add pass in parameter
 * ##
 * ## Date : 2017-03-17 Villa #U114567
 * ## Modified Get_Facility_Booking_Record() - Prevent showing duplicated itme name/ sorting by item name en
 * ##
 * ## Date : 2017-03-10 Omas [ip.2.5.8.4.1]
 * ## Modified New_Item_Booking(), Insert_Item_Booking_Record() - fix #B114302
 * ##
 * ## Date : 2016-11-02 Omas [ip.2.5.8.1.1]
 * ## Add Get_Lastest_Date_With_BookingPeriod() - #J107052
 * ##
 * ## Date : 2016-09-26 Villa #J104488
 * ## Add isSameTimeTable() - check if the selected date is in same timetable or not
 * ##
 * ## Date : 2016-09-22 Omas
 * ## modified Get_Item_SubCategory() - comment out RecordStatus, as eBooking don't have such concept - #W102333
 * ##
 * ## Date : 2016-07-04 Omas
 * ## modified Get_Facility_Booking_Record(), Get_Special_Timetable_Date_Of_TimeZone(), Get_Timeslot_By_TimetableID(), Get_TimeZone_Available_Booking_Timetable(), getAllBookableLocationName() - fix trim array problem. change to use !empty()
 * ##
 * ## Date : 2016-06-16 Cara
 * ## modified Get_All_Facility_Booking_Record() to show responsible user
 * ##
 * ## Date : 2016-03-16 Kenneth
 * ## modified Get_Item_SubCategory() - get catID and cat Name, and add order by param
 * ## add getAllLocationInfo() - get all location info
 * ## add getAllManagementGroupInfo()/getAllFollowupGroupInfo - get all managment/followup group info
 * ## add Get_Item_Info_By_Item_Code();
 * ##
 * ## Date : 2016-01-26 Omas [ip2.5.7.3.1] -#E91810
 * ## added Format_Display_Date() - convert time str to yyyy-mm-dd (w)
 * ##
 * ## Date : 2015-08-05 (Omas)
 * ## modified getFacilityBookingAvailableDateRange_MultiItem() - fix cannot get setting "Allow Booking within x days" -#E81757
 * ##
 * ## Date : 2015-06-04 (Henry)
 * ## modified GET_MODULE_OBJ_ARR() to add navigation to eBooking Admin page if the site is KIS
 * ##
 * ## Date : 2015-05-12 (Omas)
 * ## Add functions for import booking
 * ## added getBookingRecordDetails(),deleteImportTempData(),getImportTempData(),getImportColumnProperty(),getImportHeader(),insertImportTempData(),getLocationNameByCode(),getLocationNameByID(),validateLocation(),getExistingLocationAssoAry(),setExistingLocationAssoAry()
 * ##
 * ## Date : 2015-04-30 (Omas)
 * ## Add Logic to add MyCalendar if user not yet have MyCalendar, avoid insert CalID = 0 record
 * ## Modified New_Room_Booking(),New_Item_Booking(),CreatePersonalCalenderEvent(),New_Room_Reserve(),New_Item_Reserve()
 * ##
 * ## Date : 2015-04-21 (Omas)
 * ## modified Update_Related_Booking_Detail() to update Responsible User
 * ##
 * ## Date : 2015-03-25 (Omas)
 * ## Improvement: Add plz do not reply in mail content
 * ## modified Email_Booking_Result(),Email_Booking_Result_Merged(),Email_Booking_Result(),SendMailToFollowUpGroup(),SendMailToManageGroup(),SendMailToManageGroup_Merged(),Send_Cancel_Booking_Email_Notification()
 * ##
 * ## Date : 2015-03-24 (Omas)
 * ## Fix : Merged follow-up group mail show approved/rejected for room
 * ##
 * ## Date : 2015-02-23 (Omas)
 * ## change all Start_Trans_For_Lock_Table_Procedure() to Start_Trans()
 * ##
 * ## Date : 2015-02-16 (Omas)
 * ## new SendMailToManageGroup_Merged()
 * ##
 * ## Date : 2015-02-04 (Omas)
 * ## modified ShowFollowUpDetail(), ShowItemBookingDetail(),ShowRoomBookingDetail() to show attachment
 * ##
 * ## Date : 2015-01-23 (Omas)
 * ## Room Booking Record Add Day View
 * ## -new getMappingBookingIDtoTimeslot(), getRoomBookingRecordByLocationID(),SaveDayViewSetting()
 * ##
 * ## Date : 2015-01-16 (Omas)
 * ## add new setting Booking Category
 * ## - modified Get_Related_Booking_Record_By_BookingID()
 * ## - new getBookingMapWithCategory(), Is_Category_Linked_Data(),Update_Category_Record_DisplayOrder(),Get_Update_Display_Order_Arr(),getBookingCategorySelectionBox(),getBookingCategorySQL(),getBookingCategoryByCategoryID()
 * ##
 * ## Date : 2015-01-14 (Omas)
 * ## add SendEmailToFollowUpGroup_Merged() to merge related booking send reminder to FollowUpGroup
 * ##
 * ## Date : 2014-10-06 (Bill)
 * ## Modified Get_Booking_Record_By_BookingID() to return booking records that sort with input time
 * ##
 * ## Date : 2014-09-29 (Bill)
 * ## Modified New_Room_Reserve(), New_Item_Reserve(), Get_All_Facility_Booking_Record(), Email_Booking_Result_Merged() to send email when booked resources are reserved
 * ##
 * ## Date : 2014-08-01 (Carlos)
 * ## Modified New_Room_Reserve(), New_Item_Reserve(), New_Room_Booking(), New_Item_Booking() added EventCalID for user selected calendar
 * ##
 * ## Date : 2014-07-04 (Bill)
 * ## Modified New_Room_Booking(), New_Item_Reserve() for approval notificaiton
 * ## Added SendMailToManageGroup() to send email to approval group
 * ##
 * ## Date : 2014-06-20 (Bill)
 * ## Modified Get_All_Allow_Booking_Item_Category_Array(), Get_All_Allow_Booking_Item_Sub_Category_Array() to filter unbookable selection
 * ##
 * ## Date : 2014-04-09 (Carlos)
 * ## Modified Create_Temp_Booking_Record_iCal(), added SettingArr['FromModule'] to distinguish temp booking records for different modules, e.g. eEnrolment, default module is iCalendar
 * ##
 * ## Date : 2014-03-19 (Pun)
 * ## Modified SendMailToFollowUpGroup() add remarks to email content for SIS cust
 * ##
 * ## Date : 2014-02-04 (Ivan) [V57936]
 * ## modified SendMailToFollowUpGroup() to improve email content
 * ##
 * ## Date : 2014-01-16 (Tiffany)
 * ## modified Get_Booking_Record_Display_Detail, can use with the specific date range.
 * ##
 * ## Date : 2014-01-15 (Tiffany)
 * ## modified Set_Default_Setting() that add default value to "WeekDayEnabled", "CycleEnabled", "DefaultPeriodicBookingMethod"
 * ##
 * ## Date : 2013-12-12 (Ivan) [V56658]
 * ## modified Get_Booking_Record_Display_Detail() to added $SortField param
 * ##
 * ## Date : 2013-11-15 (Ivan) [2013-1024-1127-39156]
 * ## modified Get_All_Facility_Booking_Record to add $FacilityID param
 * ##
 * ## Date : 2013-10-22 (Ivan)
 * ## modified Get_Facility_Booking_Record() added parameter $GroupByBookingRecord
 * ##
 * ## Date : 2013-08-23 (Ivan) [2013-0822-0943-36073]
 * ## modified Is_Facility_Booking_Period_Set() check the specific suspension period settings also
 * ##
 * ## Date : 2013-08-19 (Ivan) [2013-0819-0942-09073]
 * ## modified getFacilityBookingAvailableDateRange_MultiItem() to fix the date range checking if the room/item only applied customized group user booking rule
 * ##
 * ## Date : 2013-05-02 (Ivan) [2013-0430-1228-29073]
 * ## modified New_Room_Reserve(), New_Item_Reserve(), New_Room_Booking(), New_Item_Booking() to fix failed to add event in iCal if the user book multiple timeslot within one day
 * ##
 * ## Date : 2013-03-26 (Carlos)
 * ## modified Build_DateTimeArr() can append time period to date associated array
 * ##
 * ## Date : 2013-01-30 (Rita)
 * ## add Send_Cancel_Booking_Email_Notification() for sending cancel booking email notification
 * ##
 * ## Date : 2012-12-07 (Ivan) [2012-1207-1409-26054]
 * ## modified Check_If_Booking_Need_Approval() to change logic to if one of the User Booking Rule no need approval => no need approval
 * ##
 * ## Date : 2012-10-25 (YatWoon)
 * ## add setting option "ReceiveEmailNotificationFromeInventory"
 * ##
 * ## Date : 2012-10-24 (Ivan)
 * ## added eInventory API related functions
 * ##
 * ## Date : 2012-10-19 (Rita)
 * ## add SELECT Event on Get_Facility_Booking_Record's sql
 * ##
 * ## Date : 2012-10-15 (Rita)
 * ## change function name form Get_Monthly_Report_Result_Ary to Get_Usage_Report_Result_Ary
 * ##
 * ## Date : 2012-10-03 (Rita)
 * ## Details : define constant for event setting
 * ##
 * ## Date : 2012-09-21 (Ivan) [2012-0919-1617-15071]
 * ## Details : modified New_Room_Booking(), New_Item_Booking(), Create_Temp_Booking_Record_iCal(), New_Room_Reserve(), New_Item_Reserve() to avoid different user can book the same room in the same time if they submit records together
 * ##
 * ## Date : 2012-09-19 (Rita)
 * ## Details : add Check_Enable_Door_Access_Right() for door access customization
 * ##
 * ## Date : 2012-09-04 (Rita)
 * ## Details : Get_Monthly_Report_Result_Ary(), add Get_Month_in_Date_Range() for eBooking Monthly Usage Report.
 * ##
 * ## Date : 2012-08-24 (Rita)
 * ## Details : modified GET_MODULE_OBJ_ARR() add monthly usage report menu
 * ##
 * ## Date : 2012-08-22 (Ivan)
 * ## Details : modified New_Room_Reserve, New_Room_Booking, Update_Related_Booking_Detail to apply door access logic
 * ##
 * ## Date : 2012-06-28 (Ivan) [2012-0417-1542-49054]
 * ## Details : modified function Update_Related_Booking_Detail, New_Room_Reserve, New_Room_Booking, New_Item_Reserve, New_Item_Booking, Get_Related_Booking_Record_By_BookingID, Get_Booking_Record_Display_Detail, Get_All_Facility_Booking_Record to add "Event" for the customization
 * ##
 * ## Date : 2012-06-27 (Rita)
 * ## Details : modified Get_Facility_Booking_Record() - add search box result for Room and Item Booking Records
 * ##
 * ## Date : 2012-06-21 (Rita)
 * ## Details : modified function Set_Default_Setting()
 * ##
 * ## Date : 2011-11-24 (YatWoon)
 * ## update Get_Booking_Record_Display_Detail(), detect attachment remark icon display
 * ##
 * ## Date : 2011-11-14 (YatWoon)
 * ## update Update_Related_Booking_Detail(), New_Item_Booking(), New_Room_Booking(),
 * ## Get_Related_Booking_Record_By_BookingID(), Get_Booking_Record_By_BookingID(),
 * ## DisplayCustRemark(), Get_Booking_Record_Display_Detail()
 * ##
 * ## Date : 2011-04-18 (YAtWoon)
 * ## update Email_Booking_Result(), change email notification wordings
 * ##
 * ## Date : 2011-04-01 (Marcus)
 * ## Fix getFacilityBookingAvailableDateRange_MultiItem(), skip checking for admin user
 * ## direct call getFacilityBookingAvailableDateRange_MultiItem() in getFacilityBookingAvailableDateRange(), for better maintainance
 * ## Improved IS_ADMIN_USER, can pass userid to check admin right
 * ##
 * ## Date : 2011-03-15 (Carlos)
 * ## Fix New_Item_Reserve() by converting ItemID to array
 * ##
 * ## Date : 2011-03-09 (YatWoon)
 * ## update Get_All_Allow_Booking_Floor_Array(), add room.RecordStatus=1 checking
 * ##
 * ## Date : 2011-02-18 (Carlos)
 * ## Details : modified Get_Room_Item_Info() get Attachment;
 * ## added Upload_Room_Attachment() and Delete_Room_Attachment();
 * ##
 * ## Date : 2011-02-16 (Carlos)
 * ## Details : modified getAllBookableCategoryItemAssoc() to get Attachment
 * ##
 * ## Date : 2010-11-08 (Ronald)
 * ## Details : modified a function - Check_If_Facility_Is_Available()
 * ## changed the logic of getting all available booking period.
 * ##
 * ## Date : 2010-06-03 (Ronald)
 * ## Details : create a new function - CreatePersonalCalenderEvent()
 * ## which is used to creata a Calender Event when making a booking request.
 * ##
 * ## Date : 2010-07-15 (Ronald)
 * ## Details : modified Get_Room_Booking_In_One_Day_Of_Multiple_TimeSlot()
 * ## changed the Query "details.BookingStatus = '1'" to "details.BookingStatus IN ('1','999')"
 * ##
 * ## Date : 2010-07-15 (Ronald)
 * ## Details : modified Get_Item_Booking_In_One_Day_Of_Multiple_TimeSlot()
 * ## changed the Query "details.BookingStatus = '1'" to "details.BookingStatus IN ('1','999')"
 * ##
 * ## Date : 2010-07-15 (Ronald)
 * ## Details : modified Create_Temp_Booking_Record_iCal()
 * ## changed the Booking Status to LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY
 * ##
 * ############### End of Change Log ################
 */
if (! defined("LIBEBOOKING_DEFINED")) // Preprocessor directives
{
    include_once ($intranet_root . "/lang/ebooking_lang.$intranet_session_language.php");

    define("LIBEBOOKING_DEFINED", true);
    
    define("LIBEBOOKING_FACILITY_TYPE_ROOM", 1);
    define("LIBEBOOKING_FACILITY_TYPE_ITEM", 2);
    
    define("LIBEBOOKING_BOOKING_STATUS_REJECTED", - 1);
    define("LIBEBOOKING_BOOKING_STATUS_PENDING", 0);
    define("LIBEBOOKING_BOOKING_STATUS_APPROVED", 1);
    define("LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY", 999);
    
    define("LIBEBOOKING_BOOKING_Waiting", 0);
    define("LIBEBOOKING_BOOKING_CheckIn", 1);
    define("LIBEBOOKING_BOOKING_CheckOut", 2);
    
    define("LIBEBOOKING_ALLOW_BOOKING_STATUS_ALLOW", 1);
    define("LIBEBOOKING_ALLOW_BOOKING_STATUS_SUSPEND", 2);
    define("LIBEBOOKING_ALLOW_BOOKING_STATUS_SUSPEND_UNTIL", 3);
    
    define("LIBEBOOKING_DEFAULT_BOOKING_METHOD_PERIOD", 1);
    define("LIBEBOOKING_DEFAULT_BOOKING_METHOD_TIME", 2);
    
    // Tiffany added 20140115
    define("LIBEBOOKING_DEFAULT_PERIODIC_BOOKING_METHOD_WEEKDAY", 1);
    define("LIBEBOOKING_DEFAULT_PERIODIC_BOOKING_METHOD_CYCLE", 2);
    define("LIBEBOOKING_DEFAULT_PERIODIC_BOOKING_METHOD", 1);
    
    // Rita added 20120621
    define("LIBEBOOKING_DEFAULT_BOOKING_TYPE_SINGLE", 1);
    define("LIBEBOOKING_DEFAULT_BOOKING_TYPE_PERIODIC", 2);
    
    define("LIBEBOOKING_BOOKING_NOTIFICATION_ENABLE", 1);
    define("LIBEBOOKING_BOOKING_NOTIFICATION_DISABLE", 0);
    
    define("LIBEBOOKING_BOOKING_DEFAULT_DOOR_ACCESS_ENABLE", 1);
    define("LIBEBOOKING_BOOKING_DEFAULT_DOOR_ACCESS_DISABLE", 0);
    
    // Rita added 20121003
    define("LIBEBOOKING_BOOKING_EVENT_IN_ICALENDAR_YES", 1);
    define("LIBEBOOKING_BOOKING_EVENT_IN_ICALENDAR_NO", 0);
    
    define("LIBEBOOKING_ALLOW_TO_BOOK_DAMAGED_ITEM_YES", 1);
    define("LIBEBOOKING_ALLOW_TO_BOOK_DAMAGED_ITEM_NO", 0);
    define("LIBEBOOKING_ALLOW_TO_BOOK_REPAIRING_ITEM_YES", 1);
    define("LIBEBOOKING_ALLOW_TO_BOOK_REPAIRING_ITEM_NO", 0);
    define("LIBEBOOKING_RECEIVE_EMAIL_NOTIFICATION_FROM_EINVENTORY_YES", 1);
    define("LIBEBOOKING_RECEIVE_EMAIL_NOTIFICATION_FROM_EINVENTORY_NO", 0);
    define("LIBEBOOKING_RECEIVE_CANCEL_BOOKING_EMAIL_NOTIFICATION_YES", 1);
    define("LIBEBOOKING_RECEIVE_CANCEL_BOOKING_EMAIL_NOTIFICATION_NO", 0);
    
    // Bill added 20140623
    define("LIBEBOOKING_BOOKING_NOTIFICATION_APPROVAL_ENABLE", 1);
    define("LIBEBOOKING_BOOKING_NOTIFICATION_APPROVAL_DISABLE", 0);

    // Tommy added 20190918
    define("LIBEBOOKING_BOOKING_APPROVE_EMAIL_NOTIFICATION_ENABLE", 1);
    define("LIBEBOOKING_BOOKING_APPROVE_EMAIL_NOTIFICATION_DISABLE", 0);

    class libebooking extends libdb
    {

        var $libUserAry;

        function libebooking()
        {
            $this->libdb();
            $this->uid = $_SESSION['UserID'];
            $this->libUserAry = array();
        }
        
        // function db_db_query($sql)
        // {
        // global $DebugMode;
        //
        // if($DebugMode==1)
        // return parent::db_db_query($sql) or die($sql."<br>".mysql_error()."<br>");
        // else
        // return parent::db_db_query($sql);
        // }
        
        /*
         * Get MODULE_OBJ array
         */
        function GET_MODULE_OBJ_ARR($From_eService = 0)
        {
            global $PATH_WRT_ROOT, $i_InventorySystem, $CurrentPage, $LAYOUT_SKIN, $ip20TopMenu, $CurrentPageArr;
            global $Lang, $sys_custom;
            
            $MenuArr = array();
            if ($From_eService == 1) {
                $CurrentPageArr['eServiceeBooking'] = 1;
                
                // Current Page Information init
                $Page_eService = 0;
                $eService_MyBookingRecord = 0;
                $eService_CurrentRoomBookingRecord = 0;
                $eService_CurrentItemBookingRecord = 0;
                
                switch ($CurrentPage) {
                    case "eService_MyBookingRecord":
                        $Page_eService = 1;
                        $eService_MyBookingRecord = 1;
                        break;
                    
                    case "eService_CurrentRoomBookingRecord":
                        $Page_eService = 1;
                        $eService_CurrentRoomBookingRecord = 1;
                        break;
                    
                    case "eService_CurrentItemBookingRecord":
                        $Page_eService = 1;
                        $eService_CurrentItemBookingRecord = 1;
                        break;
                }
                
                /*
                 * structure:
                 * $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                 * $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
                 */
                
                // Menu information
                // Management
                $MenuArr["Management"] = array(
                    $Lang['eBooking']['Management']['FieldTitle']['Management'],
                    $PATH_WRT_ROOT . "",
                    $Page_eService
                );
                $MenuArr["Management"]["Child"]["BookingRequest"] = array(
                    $Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord'],
                    $PATH_WRT_ROOT . "home/eService/eBooking/index.php",
                    $eService_MyBookingRecord
                );
                $MenuArr["Management"]["Child"]["RoomBooking"] = array(
                    $Lang['eBooking']['Management']['FieldTitle']['RoomBooking'],
                    $PATH_WRT_ROOT . "home/eService/eBooking/room_booking_week.php?clearCoo=1",
                    $eService_CurrentRoomBookingRecord
                );
                $MenuArr["Management"]["Child"]["ItemBooking"] = array(
                    $Lang['eBooking']['Management']['FieldTitle']['ItemBooking'],
                    $PATH_WRT_ROOT . "home/eService/eBooking/item_booking_week.php?clearCoo=1",
                    $eService_CurrentItemBookingRecord
                );
                
                if ($_SESSION["platform"] == "KIS" && $this->IS_ADMIN_USER()) {
                    $MenuArr["Settings"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['Settings'],
                        $PATH_WRT_ROOT . "",
                        false
                    );
                    $MenuArr["Settings"]["Child"]["eBookingAdmin"] = array(
                        $Lang['Header']['Menu']['eBooking'] . " (" . $Lang['General']['Admin'] . ")",
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/",
                        false
                    );
                }
                
                if($sys_custom['PowerClass']){
                    if(isset($_SESSION['PowerClass_PAGE'])){
                        if($_SESSION['PowerClass_PAGE']=="app"){
                            $MenuArr = array();
                        }
                    }
                }
                
                // Root Path
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eService/eBooking/index_list.php?clearCoo=1";
            } else {
                // Current Page Information init
                $PageManagement_eBooking = 0;
                
                $PageManagement = 0;
                $PageManagement_BookingRequest = 0;
                $PageManagement_RoomBooking = 0;
                $PageManagement_ItemBooking = 0;
                $PageManagement_FollowUpWork = 0;
                
                $PageCheckInCheckOut = 0;
                $PageCheckInCheckOut_TodayBooking = 0;
                $PageCheckInCheckOut_CheckInRecord = 0;
                $PageCheckInCheckOut_CheckOutRecord = 0;
                
                $PageReports = 0;
                $PageReports_UsageReport = 0;
                
                $PageSettings = 0;
                $PageSettings_SystemPropertySetting = 0;
                $PageSettings_BookingPeriodSetting = 0;
                $PageSettings_GeneralPermission = 0;
                $PageSettings_BookingCategory = 0;
                $PageSettings_ManagementGroup = 0;
                $PageSettings_FollowUpGroup = 0;
                $PageSettings_UserBookingRule = 0;
                
                switch ($CurrentPage) {
                    // ## Management Section ###
                    case "PageManagement_BookingRequest":
                        $PageManagement = 1;
                        $PageManagement_BookingRequest = 1;
                        break;
                    case "PageManagement_RoomBooking":
                        $PageManagement = 1;
                        $PageManagement_RoomBooking = 1;
                        break;
                    case "PageManagement_ItemBooking":
                        $PageManagement = 1;
                        $PageManagement_ItemBooking = 1;
                        break;
                    case "PageManagement_FollowUpWork":
                        $PageManagement = 1;
                        $PageManagement_FollowUpWork = 1;
                        break;
                    
                    // ## Check-in Check-out Section ###
                    case "PageCheckInCheckOut_TodayBooking":
                        $PageManagement = 1;
                        $PageCheckInCheckOut_TodayBooking = 1;
                        break;
                    // case "PageCheckInCheckOut_CheckInRecord":
                    // $PageManagement = 1;
                    // $PageCheckInCheckOut_CheckInRecord = 1;
                    // break;
                    // case "PageCheckInCheckOut_CheckOutRecord":
                    // $PageManagement = 1;
                    // $PageCheckInCheckOut_CheckOutRecord = 1;
                    // break;
                    
                    // ## Report Section ###
                    case "PageReports_UsageReport":
                        $PageReports = 1;
                        $PageReports_UsageReport = 1;
                        break;
                    
                    // ## Setting Section ####
                    
                    case "Settings_DefaultSettings":
                        $PageSettings = 1;
                        $PageSettings_DefaultSettings = 1;
                        break;
                    case "Settings_SystemProperty":
                        $PageSettings = 1;
                        $PageSettings_SystemPropertySetting = 1;
                        break;
                    case "Settings_BookingCategory":
                        $PageSettings = 1;
                        $PageSettings_BookingCategory = 1;
                        break;
                    case "Settings_GeneralPermission":
                        $PageSettings = 1;
                        $PageSettings_GeneralPermission = 1;
                        break;
                    case "Settings_ManagementGroup":
                        $PageSettings = 1;
                        $PageSettings_ManagementGroup = 1;
                        break;
                    case "Settings_FollowUpGroup":
                        $PageSettings = 1;
                        $PageSettings_FollowUpGroup = 1;
                        break;
                    case "Settings_UserBookingRule":
                        $PageSettings = 1;
                        $PageSettings_UserBookingRule = 1;
                        break;
                    case "Settings_BookingPeriodSettings":
                        $PageSettings = 1;
                        $PageSettings_BookingPeriodSetting = 1;
                        break;
                    case "Settings_Category":
                        $PageSettings = 1;
                        $PageSettings_Category = 1;
                        break;
                    
                    case "Settings_DoorAccess":
                        $PageSettings = 1;
                        $PageSettings_DoorAccess = 1;
                        break;
                }
                
                $CurrentPageArr['eBooking'] = 1;
                
                /*
                 * structure:
                 * $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                 * $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
                 */
                
                // Menu information
                // Management
                $MenuArr["Management"] = array(
                    $Lang['eBooking']['Management']['FieldTitle']['Management'],
                    $PATH_WRT_ROOT . "",
                    $PageManagement
                );
                
                if ($this->IS_ADMIN_USER()) {
                    $MenuArr["Management"]["Child"]["TodayRecord"] = array(
                        $Lang['eBooking']['CheckInCheckOut']['FieldTitle']['TodayRecords'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/today_records.php?clearCoo=1",
                        $PageCheckInCheckOut_TodayBooking
                    );
                }
                
                if ($_SESSION["eBooking"]["role"] == "ADMIN" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER") {
                    $MenuArr["Management"]["Child"]["BookingRequest"] = array(
                        $Lang['eBooking']['Management']['FieldTitle']['BookingRequest'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/management/booking_request.php?clearCoo=1",
                        $PageManagement_BookingRequest
                    );
                }
                if ($_SESSION["eBooking"]["role"] == "ADMIN" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER") {
                    $MenuArr["Management"]["Child"]["RoomBooking"] = array(
                        $Lang['eBooking']['Management']['FieldTitle']['RoomBooking'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/management/room_booking_week.php?clearCoo=1",
                        $PageManagement_RoomBooking
                    );
                    $MenuArr["Management"]["Child"]["ItemBooking"] = array(
                        $Lang['eBooking']['Management']['FieldTitle']['ItemBooking'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/management/item_booking_week.php?clearCoo=1",
                        $PageManagement_ItemBooking
                    );
                }
                if ($_SESSION["eBooking"]["role"] == "ADMIN" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER") {
                    $MenuArr["Management"]["Child"]["FollowUpWork"] = array(
                        $Lang['eBooking']['Management']['FieldTitle']['FollowUpWork'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/management/follow_up_work.php?clearCoo=1",
                        $PageManagement_FollowUpWork
                    );
                }
                
                // Check-in & Check-out
                // if($this->IS_ADMIN_USER())
                // {
                // $MenuArr["CheckInCheckOut"] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckInAndCheckOut'], $PATH_WRT_ROOT."", $PageCheckInCheckOut);
                // $MenuArr["CheckInCheckOut"]["Child"]["TodayRecord"] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['TodayRecords'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/today_records.php?clearCoo=1", $PageCheckInCheckOut_TodayBooking);
                // $MenuArr["CheckInCheckOut"]["Child"]["CheckInRecord"] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckInRecords'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/check_in_records.php?clearCoo=1", $PageCheckInCheckOut_CheckInRecord);
                // $MenuArr["CheckInCheckOut"]["Child"]["CheckOutRecord"] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutRecords'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/check_out_records.php?clearCoo=1", $PageCheckInCheckOut_CheckOutRecord);
                // }
                
                // Reports
                if ($this->IS_ADMIN_USER()) {
                    $MenuArr["Reports"] = array(
                        $Lang['eBooking']['Reports']['FieldTitle']['Reports'],
                        $PATH_WRT_ROOT . "",
                        $PageReports
                    );
                    $MenuArr["Reports"]["Child"]["UsageReport"] = array(
                        $Lang['eBooking']['Reports']['FieldTitle']['UsageReport'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/reports/monthly_usage_report/index.php",
                        $PageReports_UsageReport
                    );
                }
                
                // Settings
                if ($this->IS_ADMIN_USER()) {
                    $MenuArr["Settings"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['Settings'],
                        $PATH_WRT_ROOT . "",
                        $PageSettings
                    );
                    $MenuArr["Settings"]["Child"]["SystemProperty"] = array(
                        $Lang['eBooking']['Settings']['SystemProperty']['MenuTitle'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/system_property/setting.php",
                        $PageSettings_SystemPropertySetting
                    );
                    $MenuArr["Settings"]["Child"]["GeneralPermission"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['GeneralPermission'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/general_permission/room/view.php?clearCoo=1",
                        $PageSettings_GeneralPermission
                    );
                    $MenuArr["Settings"]["Child"]["ManagementGroup"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/management_group/management_group.php?clearCoo=1",
                        $PageSettings_ManagementGroup
                    );
                    $MenuArr["Settings"]["Child"]["FollowUpGroup"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/follow_up_group/follow_up_group.php",
                        $PageSettings_FollowUpGroup
                    );
                    $MenuArr["Settings"]["Child"]["UserBookingRule"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/booking_rule/user_booking_rules.php?clearCoo=1",
                        $PageSettings_UserBookingRule
                    );
                    $MenuArr["Settings"]["Child"]["DefaultSettings"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/booking_period/general_available_period.php?clearCoo=1",
                        $PageSettings_BookingPeriodSetting
                    );
                    $MenuArr["Settings"]["Child"]["Category"] = array(
                        $Lang['eBooking']['Settings']['FieldTitle']['Category'],
                        $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/category/category_setting.php?clearCoo=1",
                        $PageSettings_Category
                    );
                    if ($sys_custom['eBooking_BookingCategory']) {
                        $MenuArr["Settings"]["Child"]["BookingCategory"] = array(
                            $Lang['eBooking']['Settings']['BookingCategory']['BookingCategory'],
                            $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/booking_category/index.php",
                            $PageSettings_BookingCategory
                        );
                    }
                    
                    if ($this->Check_Enable_Door_Access_Right() == true) {
                        $MenuArr["Settings"]["Child"]["DoorAccess"] = array(
                            $Lang['eBooking']['Settings']['FieldTitle']['DoorAccess'],
                            $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/settings/door_access/index.php?clearCoo=1",
                            $PageSettings_DoorAccess
                        );
                    }
                }
                
                if($sys_custom['PowerClass']){
                    if(isset($_SESSION['PowerClass_PAGE'])){
                        if($_SESSION['PowerClass_PAGE']=="app"){
                            $MenuArr = array("Management"=>$MenuArr["Management"]);
                        }elseif($_SESSION['PowerClass_PAGE']=="reports"){
                            unset($MenuArr);
                        }                        
                    }
                }
                
                // Root Path
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eBooking/management/booking_request.php?clearCoo=1";
                // for kis client go to eService ebooking page
                if ($_SESSION["platform"] == "KIS" && $this->IS_ADMIN_USER()) {
                    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT . "home/eService/eBooking/index_list.php?clearCoo=1";
                }
            }
            
            // module information
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['eBooking'];
            
            // for kis client go to eService ebooking page
            if ($From_eService != 1 && $_SESSION["platform"] == "KIS" && $this->IS_ADMIN_USER()) {
                $MODULE_OBJ['title'] .= " (" . $Lang['General']['Admin'] . ")";
            }

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass eBooking";'."\n";
            $js.= '</script>'."\n";
            $MODULE_OBJ['title'] .= $js;
            
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = $PATH_WRT_ROOT . "images/{$LAYOUT_SKIN}/leftmenu/icon_ebooking.gif";
            $MODULE_OBJ['menu'] = $MenuArr;
            $MODULE_OBJ['TitleBar_Style'] = 2;
            return $MODULE_OBJ;
        }

        function IS_ADMIN_USER($ParUserID = '')
        {
            if (trim($ParUserID) != '' && $ParUserID != $_SESSION['UserID']) {
                include_once ("user_right_target.php");
                $urt = new user_right_target();
                $Right = $urt->Load_User_Right($ParUserID);
                if ($Right['eAdmin-eBooking'] === 1)
                    return 1;
                else
                    return 0;
            } else 
                if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"]) {
                    return 1;
                } else {
                    return 0;
                }
        }

        function Check_Page_Permission($AdminOnly = 1)
        {
            global $plugin, $PATH_WRT_ROOT;
            
            if ($AdminOnly)
                $CanAccess = ($plugin['eBooking'] == true && $this->IS_ADMIN_USER() == true);
            else
                $CanAccess = $plugin['eBooking'];
            
            if ($CanAccess == false) {
                include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
                $laccessright = new libaccessright();
                $laccessright->NO_ACCESS_RIGHT_REDIRECT();
                intranet_closedb();
                exit();
            }
        }
        
        // Cache function
        // function Cache_Available_Booking_Date($FacilityType='', $FacilityID='')
        // {
        // if(!isset($this->Cache_Available_Booking_Date_Arr[$FacilityType][$FacilityID]))
        // {
        // if($FacilityType == "")
        // {
        // // general booking period
        // $sql = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedSubLocationID = 0 AND RelatedItemID = 0)";
        // }
        // else if($FacilityType == 1)
        // {
        // // locations booking period
        // $sql = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedSubLocationID = 0 AND RelatedItemID = 0) OR (RelatedSubLocationID = '$FacilityID') ";
        // }
        // else if($FacilityType == 2)
        // {
        // // items booking period
        // $sql = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedSubLocationID = 0 AND RelatedItemID = 0) OR (RelatedItemID = '$FacilityID') ";
        // }
        //
        // $DateArr = $this->returnVector($sql);
        // $this->Cache_Available_Booking_Date_Arr[$FacilityType][$FacilityID] = $DateArr;
        // $AllPeriod = $this->Get_Facility_Available_Booking_Period($FacilityType,$FacilityID);
        // $DateArr = array_keys($AllPeriod);
        // $this->Cache_Available_Booking_Date_Arr[$FacilityType][$FacilityID] = $DateArr;
        //
        // }
        //
        // }
        
        // Cache function end
        
        // function isAvaliableBookingPeriod($TargetDate, $FacilityType='', $FacilityID='')
        // {
        // if($FacilityType == "")
        // {
        // // general booking period
        // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RecordDate = '$TargetDate' AND (RelatedSubLocationID = 0 AND RelatedItemID = 0)";
        // }
        // else if($FacilityType == 1)
        // {
        // // locations booking period
        // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RecordDate = '$TargetDate' AND RelatedSubLocationID = 0 AND RelatedItemID = 0) OR (RecordDate = '$TargetDate' AND RelatedSubLocationID = '$FacilityID') ";
        // }
        // else if($FacilityType == 2)
        // {
        // // items booking period
        // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RecordDate = '$TargetDate' AND RelatedSubLocationID = 0 AND RelatedItemID = 0) OR (RecordDate = '$TargetDate' AND RelatedItemID = '$FacilityID') ";
        // }
        // else
        // {
        // }
        // $AvaliableBooking = $this->returnVector($sql);
        //
        // if($AvaliableBooking[0]!=0)
        // {
        // return 1;
        // }
        // else
        // {
        // return 0;
        // }
        // }
        
        // function isAvaliableBookingPeriod($TargetDate, $FacilityType='', $FacilityID='')
        // {
        // $this->Cache_Available_Booking_Date($FacilityType, $FacilityID);
        // if(in_array($TargetDate,$this->Cache_Available_Booking_Date_Arr[$FacilityType][$FacilityID]))
        // {
        // return 1;
        // }
        // else
        // {
        // return 0;
        // }
        // }
        function retriveAllAvailableBookingPeriodBySchoolYearID($AcademicYearID, $FacilityType = '', $FacilityID = '')
        {
            $AcademicYearStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($AcademicYearID)));
            $AcademicYearEnd = date("Y-m-d", strtotime(getEndDateOfAcademicYear($AcademicYearID)));
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $cond_FacilityID = " RelatedItemID = '$RelatedItemID' AND RelatedSubLocationID = '$RelatedSubLocationID' ";
            
            $sql = "select 
							PeriodID, RecordDate, StartTime, EndTime, RepeatType, RepeatValue, RelatedPeriodID
						FROM
							INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD
						WHERE
							$cond_FacilityID AND (RecordDate BETWEEN '$AcademicYearStart' AND '$AcademicYearEnd') ORDER BY RecordDate";
            $result = $this->returnArray($sql, 6);
            
            return $result;
        }

        function retriveAllDaySuspensionBySchoolYearID($AcademicYearID, $FacilityType = '', $FacilityID = '')
        {
            $AcademicYearStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($AcademicYearID)));
            $AcademicYearEnd = date("Y-m-d", strtotime(getEndDateOfAcademicYear($AcademicYearID)));
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $cond_FacilityID = " RelatedItemID = '$RelatedItemID' AND RelatedSubLocationID = '$RelatedSubLocationID' ";
            
            $sql = "select 
							PeriodID, RecordDate, RepeatType, RepeatValue, RelatedPeriodID
						FROM
							INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION
						WHERE
							$cond_FacilityID AND (RecordDate BETWEEN '$AcademicYearStart' AND '$AcademicYearEnd') ORDER BY RecordDate";
            $result = $this->returnArray($sql, 6);
            
            return $result;
        }

        function Get_Related_Available_Period_Info($PeriodID, $FacilityType = 0, $FacilityID = 0)
        {
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            
            $sql = "
					SELECT 
						a.PeriodID, 
						MIN(a.RecordDate) AS StartDate, 
						MAX(a.RecordDate) AS EndDate, 
						a.StartTime AS StartTime, 
						a.EndTime AS EndTime, 
						a.TimeSlotID, 
						a.RepeatType, 
						a.RepeatValue
					FROM 
						INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a 
					WHERE
						a.RelatedPeriodID = '$PeriodID' AND a.RelatedItemID = '$RelatedItemID' AND a.RelatedSubLocationID = '$RelatedSubLocationID'
					GROUP BY 
						a.RelatedPeriodID 
				";
            
            $arrExistRecord = $this->returnArray($sql, 8);
            
            return $arrExistRecord;
        }

        function Get_Related_Day_Suspension_Info($PeriodID, $FacilityType = 0, $FacilityID = 0)
        {
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            
            $sql = "
					SELECT 
						a.PeriodID, 
						MIN(a.RecordDate) AS StartDate, 
						MAX(a.RecordDate) AS EndDate, 
						a.RepeatType, 
						a.RepeatValue
					FROM 
						INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION AS a 
					WHERE
						a.RelatedPeriodID = '$PeriodID' AND a.RelatedItemID = '$RelatedItemID' AND a.RelatedSubLocationID = '$RelatedSubLocationID'
					GROUP BY 
						a.RelatedPeriodID 
				";
            $arrExistRecord = $this->returnArray($sql, 8);
            
            return $arrExistRecord;
        }

        function Insert_Update_Booking_Available_Period($FacilityType, $FacilityID, $DateArr, $arrFinalStartTime, $arrFinalEndTime, $arrFinalTimeSlotID, $RepeatOption, $final_repeat_value = '')
        {
            global $UserID;
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $NoOfTimePeriod = count($arrFinalStartTime);
            $NoOfDate = count($DateArr);
            
            $final_repeat_value = trim($final_repeat_value) == '' ? "NULL" : "'$final_repeat_value'";
            for ($i = 0; $i < $NoOfDate; $i ++) {
                $targetRecordDate = $DateArr[$i];
                for ($j = 0; $j < $NoOfTimePeriod; $j ++) {
                    $FinalStartTime = $arrFinalStartTime[$j];
                    $FinalEndTime = $arrFinalEndTime[$j];
                    if (sizeof($arrFinalTimeSlotID) > 0) {
                        $TimeSlotID = $arrFinalTimeSlotID[$j];
                    } else {
                        $TimeSlotID = "";
                    }
                    $sql = "INSERT INTO 
										INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD 
										(RecordDate, TimeSlotID, StartTime, EndTime, RepeatType, RepeatValue, RelatedPeriodID, RelatedItemID, RelatedSubLocationID, InputBy, ModifiedBy, RecordType, RecordStatus, DateInput, DateModified) 
									VALUES 
										('$targetRecordDate','$TimeSlotID','$FinalStartTime','$FinalEndTime','$RepeatOption',$final_repeat_value,-999,'$RelatedItemID','$RelatedSubLocationID','$UserID','$UserID',NULL,NULL,NOW(),NOW())";
                    $result['CreateBookingPeriod_' . $i . '_' . $j] = $this->db_db_query($sql);
                    
                    if ($i == 0 && $j == 0) {
                        $parent_period_id = $this->db_insert_id();
                        // $child_period_id = $this->db_insert_id();
                    } else {
                        // $child_period_id = $this->db_insert_id();
                    }
                }
            }
            $sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = '$parent_period_id' WHERE RelatedPeriodID = -999";
            $result['UpdateBookingPeriodRelation'] = $this->db_db_query($sql);
            
            return ! in_array(false, $result) ? 1 : 0;
        }

        function Insert_Update_Booking_Day_Suspension($FacilityType, $FacilityID, $DateArr, $RepeatOption, $final_repeat_value = '')
        {
            global $UserID;
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $NoOfDate = count($DateArr);
            
            $final_repeat_value = trim($final_repeat_value) == '' ? "NULL" : "'$final_repeat_value'";
            for ($i = 0; $i < $NoOfDate; $i ++) {
                $targetRecordDate = $DateArr[$i];
                $sql = "INSERT INTO 
									INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION 
									(RecordDate, RepeatType, RepeatValue, RelatedPeriodID, RelatedItemID, RelatedSubLocationID, InputBy, ModifiedBy, DateInput, DateModified) 
								VALUES 
									('$targetRecordDate','$RepeatOption',$final_repeat_value,-999,'$RelatedItemID','$RelatedSubLocationID','$UserID','$UserID',NOW(),NOW())";
                $result['CreateDaySuspension_' . $i] = $this->db_db_query($sql);
                
                if ($i == 0) {
                    $parent_period_id = $this->db_insert_id();
                }
            }
            $sql = "UPDATE INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION SET RelatedPeriodID = '$parent_period_id' WHERE RelatedPeriodID = -999";
            $result['UpdateDaySuspensionRelation'] = $this->db_db_query($sql);
            
            return ! in_array(false, $result) ? 1 : 0;
        }

        function Get_Settings_ManagementGroup_Sql($Keyword = '')
        {
            global $Lang;
            
            $cond_keyword = '';
            if ($Keyword != '') {
                $cond_keyword .= " 	And 
										(
											mg.GroupName Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
											Or
											mg.Description Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
										)
									";
            }
            
            $sql = "Select
								CONCAT('<a href=\"javascript:js_Go_Update_Group_Info(', mg.GroupID, ')\" class=\"tablelink\" title=\"{$Lang['eBooking']['Settings']['ManagementGroup']['EditGroup']}\">', mg.GroupName, '</a>'),
								mg.Description,
								CONCAT('<a href=\"javascript:js_Go_View_Member_List(', mg.GroupID, ')\" class=\"tablelink\" title=\"{$Lang['eBooking']['Settings']['ManagementGroup']['ViewMemberList']}\">', Count(mgm.UserID), '</a>'),
								CONCAT('<input type=\"checkbox\" name=\"GroupIDArr[]\" value=\"', mg.GroupID ,'\">') 
						From
								INTRANET_EBOOKING_MANAGEMENT_GROUP as mg
								/*LEFT Join
								INTRANET_USER as iu
								On mg.ModifiedBy = iu.UserID*/
								Left Join
								INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER as mgm
								On mg.GroupID = mgm.GroupID
						Where
								1
								$cond_keyword
						Group By
								mg.GroupID
	        			";
            return $sql;
        }

        function Get_Settings_ManagementGroup_MemberList_Sql($GroupID, $Keyword = '')
        {
            global $Lang;
            
            // $namefield = getNameFieldByLang("iu.");
            
            $cond_keyword = '';
            if ($Keyword != '') {
                $cond_keyword .= " 	And 
										(
											iu.EnglishName Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
											Or
											iu.ChineseName Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
										)
									";
            }
            
            $sql = "Select
								IF(iu.EnglishName IS NULL, CONCAT(iau.EnglishName,'<span class=\"red\">^</span>'), iu.EnglishName ) as EnglishName ,
	        	                IF(iu.ChineseName IS NULL, CONCAT(iau.ChineseName,'<span class=\"red\">^</span>'), iu.ChineseName ) as ChineseName ,
								CONCAT('<input type=\"checkbox\" name=\"DeleteUserIDArr[]\" value=\"', mgm.UserID ,'\">')
						From
								INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER as mgm
								LEFT JOIN
								INTRANET_USER as iu
								On mgm.UserID = iu.UserID
        	                    LEFT JOIN
								INTRANET_ARCHIVE_USER as iau
								On mgm.UserID = iau.UserID
						Where
								mgm.GroupID = '" . $GroupID . "'
								$cond_keyword
						Group By
								mgm.UserID
	        			";
            return $sql;
        }

        function Get_User_Array_From_Selection($SelectionArr)
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libgroup.php');
            
            $UserIDArr = array();
            foreach ((array) $SelectionArr as $key => $value) {
                $type = substr($value, 0, 1); // U: User, G: Group, C: Class
                $type_id = substr($value, 1);
                switch ($type) {
                    case "U":
                        $UserIDArr[] = $type_id;
                        break;
                    case "G":
                    case "C":
                        $objGroup = new libgroup($type_id);
                        $GroupMemberID = $objGroup->Get_Group_User($UserType = 1);
                        // $temp = $libgrouping->returnGroupUsersInIdentity($groupIDAry, $PermittedUserTypeArr);
                        foreach ($GroupMemberID as $k1 => $d1)
                            $UserIDArr[] = $d1['UserID'];
                        
                        break;
                    default:
                        $UserIDArr[] = $value;
                        break;
                }
            }
            
            $UserIDArr = array_unique($UserIDArr);
            return $UserIDArr;
        }

        function DeleteBookingPeriod($PeriodID)
        {
            if ($PeriodID != "") {
                $sql = "DELETE FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE PeriodID = '$PeriodID' OR RelatedPeriodID = '$PeriodID'";
                $result = $this->db_db_query($sql);
                return $result;
            }
            return false;
        }

        function DeleteBookingDaySuspension($PeriodID)
        {
            if ($PeriodID != "") {
                $sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION WHERE PeriodID = '$PeriodID' OR RelatedPeriodID = '$PeriodID'";
                $result = $this->db_db_query($sql);
                return $result;
            }
            return false;
        }

        function Get_All_Allow_Booking_Building_Array($AllowBooking = "")
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            
            if ($AllowBooking != "")
                $Cond_AllowBooking = " AND room.AllowBooking = '$AllowBooking' ";
            
            $sql = "
					SELECT DISTINCT 
						building.BuildingID, 
						" . $linventory->getInventoryNameByLang("building.") . " 
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID) 
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID) 
					WHERE 
						building.RecordStatus = 1 
						AND floor.RecordStatus = 1 
						AND room.RecordStatus = 1 
						$Cond_AllowBooking
					ORDER BY
						building.DisplayOrder ASC, floor.DisplayOrder ASC, room.DisplayOrder ASC
				";
            $arrBuilding = $this->returnArray($sql, 2);
            
            return $arrBuilding;
        }

        function Get_All_Allow_Booking_Floor_Array($targetBuilding, $AllowBooking = "")
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            
            if ($AllowBooking != "")
                $Cond_AllowBooking = " AND room.AllowBooking = '$AllowBooking' ";
            
            $sql = "
					SELECT DISTINCT 
						floor.LocationLevelID, " . $linventory->getInventoryNameByLang("floor.") . " 
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID) 
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID) 
					WHERE 
						building.RecordStatus = 1 
						AND floor.RecordStatus = 1 
						AND room.RecordStatus = 1  
						and building.BuildingID = '$targetBuilding' 
						$Cond_AllowBooking
					ORDER BY
						building.DisplayOrder ASC, floor.DisplayOrder ASC, room.DisplayOrder ASC
				";
            $arrFloor = $this->returnArray($sql, 2);
            
            return $arrFloor;
        }

        function Get_All_Allow_Booking_Room_Array($targetBuilding, $targetFloor, $AllowBooking = "")
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            
            if ($AllowBooking != "")
                $Cond_AllowBooking = " AND room.AllowBooking = '$AllowBooking' ";
            
            $sql = "
					SELECT DISTINCT 
						room.LocationID, 
						" . $linventory->getInventoryNameByLang("room.") . " 
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID) 
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID) 
					WHERE 
						building.RecordStatus = 1 
						AND floor.RecordStatus = 1 
						AND room.RecordStatus = 1 
						AND building.BuildingID = '$targetBuilding' 
						AND floor.LocationLevelID = '$targetFloor' 
						$Cond_AllowBooking 
					ORDER BY
						building.DisplayOrder ASC, floor.DisplayOrder ASC, room.DisplayOrder ASC
				";
            $arrRoom = $this->returnArray($sql, 2);
            
            return $arrRoom;
        }

        function Get_All_Allow_Booking_Item_Category_Array($hideNoItemCategory = false)
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            
            $hideNoItemCategory_sql = "";
            if ($hideNoItemCategory) {
                $hideNoItemCategory_sql = "	WHERE Item.AllowBooking = 1";
            }
            
            // $sql = "SELECT DISTINCT Cat.CategoryID, ".$linventory->getInventoryNameByLang("Cat.")." FROM INVENTORY_CATEGORY AS Cat INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS SubCat ON (Cat.CategoryID = SubCat.CategoryID) INNER JOIN INVENTORY_ITEM AS Item ON (Cat.CategoryID = Item.CategoryID AND SubCat.Category2ID = Item.Category2ID)";
            $sql = "
					SELECT DISTINCT 
						Cat.CategoryID, " . $linventory->getInventoryNameByLang("Cat.") . " 
					FROM 
						INVENTORY_CATEGORY AS Cat 
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS SubCat ON (Cat.CategoryID = SubCat.CategoryID) 
						INNER JOIN INTRANET_EBOOKING_ITEM AS Item ON (Cat.CategoryID = Item.CategoryID AND SubCat.Category2ID = Item.Category2ID)
					$hideNoItemCategory_sql
				";
            $arrCatList = $this->returnArray($sql, 2);
            return $arrCatList;
        }

        function Get_All_Allow_Booking_Item_Sub_Category_Array($ItemCategory, $hideNoItemSubCategory = false)
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            
            $hideNoItemSubCategory_sql = "";
            if ($hideNoItemSubCategory) {
                $hideNoItemSubCategory_sql = "	AND Item.AllowBooking = 1";
            }
            
            $sql = "
					SELECT DISTINCT 
						SubCat.Category2ID, " . $linventory->getInventoryNameByLang("SubCat.") . " 
					FROM INVENTORY_CATEGORY AS Cat 
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS SubCat ON (Cat.CategoryID = SubCat.CategoryID) 
						INNER JOIN INTRANET_EBOOKING_ITEM AS Item ON (Cat.CategoryID = Item.CategoryID AND SubCat.Category2ID = Item.Category2ID) 
					WHERE 
						Cat.CategoryID = '$ItemCategory'
						$hideNoItemSubCategory_sql
				";
            $arrSubCatList = $this->returnArray($sql, 2);
            
            return $arrSubCatList;
        }

        function Get_All_Allow_Booking_Item_Array($ItemCategory, $ItemSubCategory)
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            
            // $sql = "SELECT ItemID, CONCAT(ItemCode, ' - ', ".$linventory->getInventoryNameByLang().") FROM INTRANET_EBOOKING_ITEM WHERE CategoryID = $ItemCategory AND Category2ID = $ItemSubCategory AND RecordStatus = 1 ORDER BY ItemCode";
            $sql = "SELECT ItemID, CONCAT(ItemCode, ' - ', " . $linventory->getInventoryNameByLang() . ") FROM INTRANET_EBOOKING_ITEM WHERE CategoryID = '$ItemCategory' AND Category2ID = '$ItemSubCategory' AND RecordStatus = 1 ORDER BY ItemCode";
            $arrItemList = $this->returnArray($sql, 2);
            
            return $arrItemList;
        }

        function getAllLocationInfo($orderby = '')
        {
            if ($orderby != '') {
                $orderCond = " ORDER BY  $orderby";
            }
            $sql = "Select 
            				il.LocationID,
            				il.Code,
            				il.Barcode,
            				il.NameChi AS RoomNameChi,
            				il.NameEng AS RoomNameEng,
            				ill.LocationLevelID,
            				ill.NameChi AS LevelNameChi,
            				ill.NameEng AS LevelNameEng,
            				ilb.BuildingID,
            				ilb.NameChi AS BuildingNameChi,
            				ilb.NameEng AS BuildingNameEng																		 
            			From 
            				INVENTORY_LOCATION AS il
            				INNER JOIN INVENTORY_LOCATION_LEVEL AS ill ON (il.LocationLevelID=ill.LocationLevelID)
            				INNER JOIN INVENTORY_LOCATION_BUILDING AS ilb ON (ill.BuildingID=ilb.BuildingID)
            			WHERE ilb.RecordStatus = 1 and ill.RecordStatus = 1 and il.RecordStatus = 1 
            			$orderCond		";
            return $this->returnArray($sql);
        }

        function Get_Item_Category($CategoryID = '', $RecordStatus = '1', $orderBy = '')
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            $namefield = $linventory->getInventoryNameByLang();
            
            if ($orderBy != '') {
                $orderByCond = ' ORDER BY ' . $orderBy . ' ';
            }
            
            $conds_categoryID = '';
            if ($CategoryID != '')
                $conds_categoryID = " And CategoryID = '$CategoryID' ";
            
            $sql = "Select
								CategoryID,
								$namefield as CategoryName
						From
								INVENTORY_CATEGORY
						Where
								1 and RecordStatus in ($RecordStatus) 
								$conds_categoryID
								$orderByCond		
						";
            $returnArr = $this->returnArray($sql);
            return $returnArr;
        }

        function Get_Item_SubCategory($CategoryID = '', $Category2ID = '', $orderBy = '', $RecordStatus = '1')
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $linventory = new libinventory();
            // $namefield = $linventory->getInventoryNameByLang();
            $namefield = Get_Lang_Selection('NameChi', 'NameEng');
            
            $conds_categoryID = '';
            if ($CategoryID != '')
                $conds_categoryID = " And icl2.CategoryID = '$CategoryID' ";
            
            $conds_category2ID = '';
            if ($Category2ID != '')
                $conds_category2ID = " And Category2ID = '$Category2ID' ";
            
            if ($orderBy != '') {
                $orderCond = " ORDER BY " . $orderBy;
            }
            $sql = "Select
								Category2ID,
								icl2.$namefield as SubCategoryName,
								icl2.CategoryID,
								ic.$namefield as CategoryName			
						From
								INVENTORY_CATEGORY_LEVEL2 AS icl2
								INNER JOIN INVENTORY_CATEGORY AS ic ON (icl2.CategoryID = ic.CategoryID)
						Where
								1
								/*and icl2.RecordStatus in ($RecordStatus)*/ 
								$conds_categoryID
								$conds_category2ID
								$orderCond		
						";
            $returnArr = $this->returnArray($sql);
            return $returnArr;
        }

        function Get_User_Booking_Rule($RuleID = '', $ItemID = '', $RoomID = '', $ParUserID = '')
        {
            global $PATH_WRT_ROOT;
            
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);

            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            $conds_RuleID = '';
            if ($RuleID != '')
                $conds_RuleID = " And rule.RuleID = '$RuleID' ";
            
            $join_table = '';
            $conds_ItemID = '';
            if ($ItemID != '') {
                $join_table .= "	Inner Join
										INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE As item_mapping
										On (rule.RuleID = item_mapping.RuleID)
									";
                $conds_ItemID = " And item_mapping.ItemID = '$ItemID' ";
            }
            
            $conds_RoomID = '';
            if ($RoomID != '') {
                $join_table .= "	Inner Join
										INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION As room_mapping
										On (rule.RuleID = room_mapping.UserBookingRuleID)
									";
                $conds_RoomID = " And room_mapping.LocationID = '$RoomID' ";
            }
            
            $conds_User = '';
            if ($ParUserID != '') {
                // $join_table .= " Inner Join
                // INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER As ruleTagetUser
                // On (rule.RuleID = ruleTagetUser.RuleID)
                // ";
                //
                // include_once($PATH_WRT_ROOT."includes/libuser.php");
                // $ObjUser = new libuser($ParUserID);
                //
                // $conds_User .= " And ruleTagetUser.UserType = '".$ObjUser->RecordType."' ";
                // if ($ObjUser->isTeacherStaff())
                // {
                // # no need to distinguish teaching & non-teaching staff at this moment
                // //$conds_user .= " And RuleUser.IsTeaching = '".$ObjUser->teaching."' ";
                // }
                // else if ($ObjUser->isStudent())
                // {
                // $YearIDArr = Get_Array_By_Key($ObjUser->Get_User_Studying_Form(), 'YearID');
                // $conds_User .= " And ruleTagetUser.YearID In (".implode(',', $YearIDArr).") ";
                // }
                // else if ($ObjUser->isParent())
                // {
                // $YearIDArr = Get_Array_By_Key($ObjUser->Get_Children_Studying_Form(), 'YearID');
                // $conds_User .= " And ruleTagetUser.YearID In (".implode(',', $YearIDArr).") ";
                // }
                
                $UserRuleIDArr = $this->Get_User_Booking_Rule_Of_User($ParUserID);
                $conds_UserRuleID = " AND rule.RuleID IN (" . implode(",", (array) $UserRuleIDArr) . ")  ";
            }
            
            $sql = "SELECT 
							DISTINCT rule.RuleID, rule.RuleName, rule.NeedApproval, rule.DaysBeforeUse, rule.CanBookForOthers, rule.RecordType, rule.DisableBooking
						FROM 
							INTRANET_EBOOKING_USER_BOOKING_RULE AS rule
							$join_table
						Where
							1
							$conds_ItemID
							$conds_RoomID
							$conds_RuleID
							$conds_UserRuleID
						";
            
            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_User_Booking_Rule_Of_User($ParUserID)
        {
            global $PATH_WRT_ROOT;
            
            $RuleIDArr = array();
            
            // check Target Form
            // include_once($PATH_WRT_ROOT."includes/libuser.php");
            // $ObjUser = new libuser($ParUserID);
            $ObjUser = $this->getLibUserObj($ParUserID);
            
            if ($ObjUser->isTeacherStaff()) {
                $UserType = 1;
            } else 
                if ($ObjUser->isStudent()) {
                    $YearIDArr = Get_Array_By_Key($ObjUser->Get_User_Studying_Form(), 'YearID');
                    $UserType = 2;
                } else 
                    if ($ObjUser->isParent()) {
                        $YearIDArr = Get_Array_By_Key($ObjUser->Get_Children_Studying_Form(), 'YearID');
                        $UserType = 3;
                    }
            $RuleTargetUserMapping = $this->Get_User_Booking_Rule_Target_Form_Mapping(NULL, $YearIDArr, $UserType);
            $RuleIDArr1 = Get_Array_By_Key($RuleTargetUserMapping, "RuleID");
            
            // check Custom Group
            $UserRuleMapping = $this->Get_User_Booking_Rule_Custom_Group_User_Mapping(NULL, $ParUserID);
            $RuleIDArr2 = Get_Array_By_Key($UserRuleMapping, "RuleID");
            
            $RuleIDArr = array_unique(array_merge($RuleIDArr1, $RuleIDArr2));
            return $RuleIDArr;
        }

        function retriveAllAvailableBookingPeriodWithBasicPeriodBySchoolYearID($AcademicYearID, $FacilityType = '', $FacilityID = '')
        {
            $AcademicYearStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($AcademicYearID)));
            $AcademicYearEnd = date("Y-m-d", strtotime(getEndDateOfAcademicYear($AcademicYearID)));
            
            if ($FacilityType == "") {
                $sql = "SELECT 
								a.PeriodID, CONCAT(a.RecordDate,' ~ ',c.EndDate), CONCAT(a.StartTime,' - ',a.EndTime), a.RepeatType, a.RepeatValue, a.DateModified, a.ModifiedBy
							FROM 
								INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a 
								INNER JOIN 
								(SELECT b.PeriodID, b.RelatedPeriodID, MAX(b.RecordDate) AS EndDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS b WHERE (b.RelatedPeriodID IS NOT NULL OR b.RelatedPeriodID != '') AND b.RelatedItemID = 0 AND b.RelatedSubLocationID = 0 GROUP BY b.RelatedPeriodID) AS c ON (a.PeriodID = c.RelatedPeriodID) 
							WHERE 
								(a.RecordDate BETWEEN '$AcademicYearStart' AND '$AcademicYearEnd') AND a.RelatedItemID = 0 AND a.RelatedSubLocationID = 0
							GROUP BY 
								a.RelatedPeriodID 
							ORDER By 
								a.RecordDate ASC";
                
                $result = $this->returnArray($sql, 7);
            } else 
                if ($FacilityType == 1) {
                    // Locations booking coditions
                    
                    $sql = "SELECT 
								a.PeriodID, CONCAT(a.RecordDate,' ~ ',c.EndDate), CONCAT(a.StartTime,' - ',a.EndTime), a.RepeatType, a.RepeatValue, a.DateModified, a.ModifiedBy
							FROM 
								INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a 
								INNER JOIN 
								(SELECT b.PeriodID, b.RelatedPeriodID, MAX(b.RecordDate) AS EndDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS b WHERE (b.RelatedPeriodID IS NOT NULL OR b.RelatedPeriodID != '') AND (b.RelatedItemID = 0 AND b.RelatedSubLocationID = '$FacilityID') OR (b.RelatedItemID = 0 AND b.RelatedSubLocationID = 0) GROUP BY b.RelatedPeriodID) AS c ON (a.PeriodID = c.RelatedPeriodID) 
							WHERE 
								(a.RecordDate BETWEEN '$AcademicYearStart' AND '$AcademicYearEnd') AND a.RelatedItemID = 0 AND (a.RelatedSubLocationID = '$FacilityID' OR a.RelatedSubLocationID = 0)
							GROUP BY 
								a.RelatedPeriodID 
							ORDER By 
								a.RecordDate ASC";
                    
                    $result = $this->returnArray($sql, 7);
                } else 
                    if ($FacilityType == 2) {
                        // Item booking conditions
                        
                        $sql = "SELECT 
								a.PeriodID, CONCAT(a.RecordDate,' ~ ',c.EndDate), CONCAT(a.StartTime,' - ',a.EndTime), a.RepeatType, a.RepeatValue, a.DateModified, a.ModifiedBy
							FROM 
								INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a 
								INNER JOIN 
								(SELECT b.PeriodID, b.RelatedPeriodID, MAX(b.RecordDate) AS EndDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS b WHERE (b.RelatedPeriodID IS NOT NULL OR b.RelatedPeriodID != '') AND (b.RelatedItemID = '$FacilityID' AND b.RelatedSubLocationID = 0) OR (b.RelatedItemID = 0 AND b.RelatedSubLocationID = 0) GROUP BY b.RelatedPeriodID) AS c ON (a.PeriodID = c.RelatedPeriodID) 
							WHERE 
								(a.RecordDate BETWEEN '$AcademicYearStart' AND '$AcademicYearEnd') AND (a.RelatedItemID = '$FacilityID' OR a.RelatedItemID = 0)AND a.RelatedSubLocationID = 0
							GROUP BY 
								a.RelatedPeriodID 
							ORDER By 
								a.RecordDate ASC";
                        
                        $result = $this->returnArray($sql, 7);
                    } else {}
            return $result;
        }

        function Check_Approve_Room_Booking_Request_Right($RoomID)
        {
            global $UserID;
            
            if (! $this->IS_ADMIN_USER()) {
                $sql = "SELECT 
								COUNT(*) 
							FROM 
								INVENTORY_LOCATION AS a 
								INNER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS b ON (a.LocationID = b.LocationID)
								INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS c ON (b.GroupID = c.GroupID)
							WHERE 
								a.LocationID = '$RoomID' AND c.UserID = '$UserID'";
                
                $arrApproveRight = $this->returnVector($sql);
                $ApproveRight = $arrApproveRight[0];
            } else {
                $ApproveRight = 1;
            }
            return $ApproveRight;
        }

        function Check_Approve_Item_Booking_Request_Right($ItemID)
        {
            global $UserID;
            
            if (! $this->IS_ADMIN_USER()) {
                $sql = "SELECT 
								COUNT(*) 
							FROM 
								INTRANET_EBOOKING_ITEM AS a 
								INNER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS b ON (a.ItemID = b.ItemID)
								INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS c ON (b.GroupID = c.GroupID)
							WHERE 
								a.ItemID = '$ItemID' AND c.UserID = '$UserID'";
                
                $arrApproveRight = $this->returnVector($sql);
                $ApproveRight = $arrApproveRight[0];
            } else {
                $ApproveRight = 1;
            }
            return $ApproveRight;
        }

        /*
         * para: $DateTimeArr[$Date][0]['StartTime', 'EndTime'] = value;
         */
        function Get_Room_Booking_In_One_Day_Of_Multiple_TimeSlot($RoomIDArr, $DateTimeArr, $ReturnAsso = 0, $BookingStatus = array(1,999))
        {
            if (is_array($RoomIDArr) == false || count($RoomIDArr) == 0)
                return array();
            else
                $conds_RoomID = " And details.FacilityID In (" . implode(",", $RoomIDArr) . ") ";
            
            if (is_array($DateTimeArr) == false || count($DateTimeArr) == 0)
                return array();
            else {
                // ## condition to get the booking of the date in the timeslot
                $thisCondsDateTimeArr = array();
                foreach ($DateTimeArr as $thisDate => $thisTimeArr) {
                    $numOfTime = count($thisTimeArr);
                    $thisCondsTimeArr = array();
                    for ($i = 0; $i < $numOfTime; $i ++) {
                        $thisStartTime = $thisTimeArr[$i]['StartTime'];
                        $thisEndTime = $thisTimeArr[$i]['EndTime'];
                        
                        $thisCondsTimeArr[] = " (
														(record.StartTime >= '$thisStartTime' And record.StartTime < '$thisEndTime')
														Or
														(record.EndTime > '$thisStartTime' And record.EndTime <= '$thisEndTime')
														Or
														(record.StartTime <= '$thisStartTime' And record.EndTime >= '$thisEndTime')
													) ";
                    }
                    $thisCondsDateTimeArr[] = " (record.Date = '$thisDate' And ( " . implode(" Or ", $thisCondsTimeArr) . ")) ";
                }
                $conds_Date = " And (" . implode(" Or ", $thisCondsDateTimeArr) . ") ";
            }
            
            $RequestedBy_namefield = getNameFieldByLang("u1.");
            $ReponsibleUID_namefield = getNameFieldByLang("u2.");
            
            $sql = "Select
								record.BookingID,
								details.FacilityID,
								record.Date,
								$RequestedBy_namefield as RequestedByUserName,
								$ReponsibleUID_namefield as ReponsibleUserName,
								record.StartTime,
								record.EndTime,
								record.DeleteOtherRelatedIfReject,
								record.BookingStatus bs1,
								details.BookingStatus bs2 
						From
								INTRANET_EBOOKING_RECORD as record
								Inner Join
								INTRANET_EBOOKING_BOOKING_DETAILS as details
								On (record.BookingID = details.BookingID AND details.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ROOM . ")
								LEFT Join
								INTRANET_USER as u1
								On (record.RequestedBy = u1.UserID)
								LEFT Join
								INTRANET_USER as u2
								On (record.ResponsibleUID = u2.UserID)
						Where
								record.RecordStatus = 1 AND	
								details.BookingStatus IN ('" . implode("','", $BookingStatus) . "')
								$conds_RoomID
								$conds_Date
						Order By
								record.Date, record.StartTime
						";
            
            $BookingArr = $this->returnArray($sql);
            
            if ($ReturnAsso == 0)
                return $BookingArr;
            else {
                $ReturnArr = array();
                $numOfBooking = count($BookingArr);
                for ($i = 0; $i < $numOfBooking; $i ++) {
                    $thisRoomID = $BookingArr[$i]['FacilityID'];
                    $thisDate = $BookingArr[$i]['Date'];
                    (array) $ReturnArr[$thisRoomID][$thisDate][] = $BookingArr[$i];
                }
                return $ReturnArr;
            }
        }

        /*
         * para: $DateTimeArr[$Date][0]['StartTime', 'EndTime'] = value;
         */
        function Get_Item_Booking_In_One_Day_Of_Multiple_TimeSlot($ItemIDArr, $DateTimeArr, $ReturnAsso = 0, $BookingStatus = array(1,999))
        {
            if (is_array($ItemIDArr) == false || count($ItemIDArr) == 0)
                return array();
            else
                $conds_ItemID = " And details.FacilityID In (" . implode(",", $ItemIDArr) . ") ";
            
            if (is_array($DateTimeArr) == false || count($DateTimeArr) == 0)
                return array();
            else {
                $thisCondsDateTimeArr = array();
                foreach ($DateTimeArr as $thisDate => $thisTimeArr) {
                    $numOfTime = count($thisTimeArr);
                    $thisCondsTimeArr = array();
                    for ($i = 0; $i < $numOfTime; $i ++) {
                        $thisStartTime = $thisTimeArr[$i]['StartTime'];
                        $thisEndTime = $thisTimeArr[$i]['EndTime'];
                        
                        $thisCondsTimeArr[] = " (
														(record.StartTime >= '$thisStartTime' And record.StartTime < '$thisEndTime')
														Or
														(record.EndTime > '$thisStartTime' And record.EndTime <= '$thisEndTime')
														Or
														(record.StartTime <= '$thisStartTime' And record.EndTime >= '$thisEndTime')
													) ";
                    }
                    $thisCondsDateTimeArr[] = " (record.Date = '$thisDate' And ( " . implode(" Or ", $thisCondsTimeArr) . ")) ";
                }
                $conds_Date = " And (" . implode(" Or ", $thisCondsDateTimeArr) . ") ";
            }
            
            $RequestedBy_namefield = getNameFieldByLang("u1.");
            $ReponsibleUID_namefield = getNameFieldByLang("u2.");
            
            $sql = "Select
								record.BookingID,
								details.FacilityID,
								record.Date,
								$RequestedBy_namefield as RequestedByUserName,
								$ReponsibleUID_namefield as ReponsibleUserName,
								record.StartTime,
								record.EndTime,
								record.DeleteOtherRelatedIfReject
						From
								INTRANET_EBOOKING_RECORD as record
								Inner Join
								INTRANET_EBOOKING_BOOKING_DETAILS as details
								On (record.BookingID = details.BookingID AND details.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ITEM . ")
								LEFT Join
								INTRANET_USER as u1
								On (record.RequestedBy = u1.UserID)
								LEFT Join
								INTRANET_USER as u2
								On (record.ResponsibleUID = u2.UserID)
						Where
								record.RecordStatus = 1 AND
								details.BookingStatus IN (" . implode(",", (array) $BookingStatus) . ")
								$conds_ItemID
								$conds_Date
						Order By
								record.Date, record.StartTime
						";
            
            $BookingArr = $this->returnArray($sql);
            
            if ($ReturnAsso == 0)
                return $BookingArr;
            else {
                $ReturnArr = array();
                $numOfBooking = count($BookingArr);
                for ($i = 0; $i < $numOfBooking; $i ++) {
                    $thisItemID = $BookingArr[$i]['FacilityID'];
                    $thisDate = $BookingArr[$i]['Date'];
                    (array) $ReturnArr[$thisItemID][$thisDate][] = $BookingArr[$i];
                }
                return $ReturnArr;
            }
        }

        function Get_Room_Booking_In_A_TimeRange_Of_Multiple_Date($StartTime, $EndTime, $DateArr, $RoomIDArr, $ReturnAsso = 0, $BookingStatusArr = '')
        {
            if (is_array($RoomIDArr) == false || count($RoomIDArr) == 0)
                return array();
            else
                $conds_RoomID = " And details.FacilityID In (" . implode(",", $RoomIDArr) . ") ";
            
            if (is_array($DateArr) == false || count($DateArr) == 0)
                return array();
            else
                $conds_Date = " And record.Date In ('" . implode("','", $DateArr) . "') ";
            
            $conds_BookingStatus = '';
            if ($BookingStatusArr != '')
                $conds_BookingStatus = " And details.BookingStatus In (" . implode(',', $BookingStatusArr) . ") ";
            
            $RequestedBy_namefield = getNameFieldByLang("u1.");
            $ReponsibleUID_namefield = getNameFieldByLang("u2.");
            
            $sql = "Select
								record.BookingID,
								details.FacilityID,
								record.Date,
								$RequestedBy_namefield as RequestedByUserName,
								$ReponsibleUID_namefield as ReponsibleUserName,
								details.BookingStatus,
								record.StartTime,
								record.EndTime
						From
								INTRANET_EBOOKING_RECORD as record
								Inner Join
								INTRANET_EBOOKING_BOOKING_DETAILS as details
								On (record.BookingID = details.BookingID AND details.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ROOM . ")
								LEFT Join
								INTRANET_USER as u1
								On (record.RequestedBy = u1.UserID)
								LEFT Join
								INTRANET_USER as u2
								On (record.ResponsibleUID = u2.UserID)
						Where
								record.RecordStatus = 1 AND
								(
									(record.StartTime >= '$StartTime' And record.StartTime < '$EndTime')
									Or
									(record.EndTime > '$StartTime' And record.EndTime <= '$EndTime')
									Or
									(record.StartTime <= '$StartTime' And record.EndTime >= '$EndTime')
								)
								$conds_RoomID
								$conds_Date
								$conds_BookingStatus
						Order By
								record.Date, record.StartTime
						";
            $BookingArr = $this->returnArray($sql);
            
            if ($ReturnAsso == 0)
                return $BookingArr;
            else {
                $ReturnArr = array();
                $numOfBooking = count($BookingArr);
                for ($i = 0; $i < $numOfBooking; $i ++) {
                    $thisRoomID = $BookingArr[$i]['FacilityID'];
                    $thisDate = $BookingArr[$i]['Date'];
                    (array) $ReturnArr[$thisRoomID][$thisDate][] = $BookingArr[$i];
                }
                return $ReturnArr;
            }
        }

        function Get_Item_Booking_In_A_TimeRange_Of_Multiple_Date($StartTime, $EndTime, $DateArr, $ItemIDArr, $ReturnAsso = 0, $BookingStatusArr = '')
        {
            if (is_array($ItemIDArr) == false || count($ItemIDArr) == 0)
                return array();
            else
                $conds_ItemID = " And details.FacilityID In (" . implode(",", $ItemIDArr) . ") ";
            
            if (is_array($DateArr) == false || count($DateArr) == 0)
                return array();
            else
                $conds_Date = " And record.Date In ('" . implode("','", $DateArr) . "') ";
            
            $conds_BookingStatus = '';
            if ($BookingStatusArr != '')
                $conds_BookingStatus = " And details.BookingStatus In (" . implode(',', $BookingStatusArr) . ") ";
            
            $RequestedBy_namefield = getNameFieldByLang("u1.");
            $ReponsibleUID_namefield = getNameFieldByLang("u2.");
            
            $sql = "Select
								record.BookingID,
								details.FacilityID,
								record.Date,
								$RequestedBy_namefield as RequestedByUserName,
								$ReponsibleUID_namefield as ReponsibleUserName,
								record.StartTime,
								record.EndTime
						From
								INTRANET_EBOOKING_RECORD as record
								Inner Join
								INTRANET_EBOOKING_BOOKING_DETAILS as details
								On (record.BookingID = details.BookingID AND details.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ITEM . ")
								LEFT Join
								INTRANET_USER as u1
								On (record.RequestedBy = u1.UserID)
								LEFT Join
								INTRANET_USER as u2
								On (record.ResponsibleUID = u2.UserID)
						Where
								record.RecordStatus = 1 AND
								(
									(record.StartTime >= '$StartTime' And record.StartTime < '$EndTime')
									Or
									(record.EndTime > '$StartTime' And record.EndTime <= '$EndTime')
									Or
									(record.StartTime <= '$StartTime' And record.EndTime >= '$EndTime')
								)
								$conds_ItemID
								$conds_Date
								$conds_BookingStatus
						Order By
								record.Date, record.StartTime
						";
            $BookingArr = $this->returnArray($sql);
            
            if ($ReturnAsso == 0)
                return $BookingArr;
            else {
                $ReturnArr = array();
                $numOfBooking = count($BookingArr);
                for ($i = 0; $i < $numOfBooking; $i ++) {
                    $thisItemID = $BookingArr[$i]['FacilityID'];
                    $thisDate = $BookingArr[$i]['Date'];
                    
                    (array) $ReturnArr[$thisItemID][$thisDate][] = $BookingArr[$i];
                }
                return $ReturnArr;
            }
        }

        /*
         * If $ItemType=='', returns both bulk item and single item
         */
        function Get_Room_Item_Info($RoomID, $ItemType = '', $IncludedWhenBooking = '', $ShowForReference = '')
        {
            global $Lang, $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
            
            $linventory = new libinventory();
            
            $conds_bulk_IncludedWhenBooking = '';
            $conds_single_IncludedWhenBooking = '';
            if ($IncludedWhenBooking != '') {
                $conds_bulk_IncludedWhenBooking = " And relation.IncludedWhenBooking = '$IncludedWhenBooking' ";
                $conds_single_IncludedWhenBooking = " And item.IncludedWhenBooking = '$IncludedWhenBooking' ";
            }
            
            $conds_bulk_ShowForReference = '';
            $conds_single_ShowForReference = '';
            if ($ShowForReference != '') {
                $conds_bulk_ShowForReference = " And relation.ShowForReference = '$ShowForReference' ";
                $conds_single_ShowForReference = " And item.ShowForReference = '$ShowForReference' ";
            }
            
            if ($ItemType == '' || $ItemType == 2) {
                $sql_bulk = "
							SELECT 
									item.ItemID, 
									2 AS ItemType,
									" . $linventory->getInventoryNameByLang("item.") . " as ItemName, 
									CONCAT(" . $linventory->getInventoryNameByLang("cat.") . ",' > '," . $linventory->getInventoryNameByLang("sub_cat.") . ") as ItemCategory,
									SUM(bulk_location.Quantity) as Quantity,
									relation.IncludedWhenBooking,
									relation.ShowForReference, 
									NULL as Attachment,
									bulk_location.LocationID,
									NULL as eInventoryItemID
							FROM
									INVENTORY_ITEM AS item 
									INNER JOIN 
									INVENTORY_ITEM_BULK_LOCATION AS bulk_location 
									ON (item.ItemID = bulk_location.ItemID) 
									LEFT OUTER JOIN 
									INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION AS relation 
									ON (relation.LocationID = '$RoomID' AND item.ItemID = relation.BulkItemID) 
									INNER JOIN 
									INVENTORY_CATEGORY AS cat 
									ON (item.CategoryID = cat.CategoryID) 
									INNER JOIN 
									INVENTORY_CATEGORY_LEVEL2 AS sub_cat 
									ON (item.Category2ID = sub_cat.Category2ID) 
							WHERE
									item.ItemType = 2 
									AND item.RecordStatus = 1 
									AND bulk_location.LocationID = '$RoomID'
									AND bulk_location.Quantity >0
									$conds_bulk_IncludedWhenBooking
									$conds_bulk_ShowForReference
							Group By
									item.ItemID, bulk_location.LocationID
							";
            }
            
            if ($ItemType == '' || $ItemType == 1) {
                $sql_single = "SELECT 
											item.ItemID,
											1 AS ItemType,
											" . $linventory->getInventoryNameByLang("item.") . " as ItemName,
											CONCAT(" . $linventory->getInventoryNameByLang("cat.") . ", ' > ', " . $linventory->getInventoryNameByLang("sub_cat.") . ") as ItemCategory, 
											1 AS Quantity, 
											item.IncludedWhenBooking, 
											item.ShowForReference,
											item.Attachment,
											item.LocationID,
											item.eInventoryItemID
									FROM 
											INTRANET_EBOOKING_ITEM AS item 
											INNER JOIN 
											INVENTORY_CATEGORY AS cat 
											ON (item.CategoryID = cat.CategoryID)
											INNER JOIN 
											INVENTORY_CATEGORY_LEVEL2 AS sub_cat 
											ON (item.Category2ID = sub_cat.Category2ID) 
									WHERE 
											item.LocationID = '$RoomID'
											AND item.AllowBooking = '1'
											AND item.RecordStatus = 1
											$conds_single_IncludedWhenBooking
											$conds_single_ShowForReference
									";
            }
            
            if ($ItemType == 1)
                $sql = $sql_single;
            else 
                if ($ItemType == 2)
                    $sql = $sql_bulk;
                else 
                    if ($ItemType == '')
                        $sql = "($sql_bulk) Union ($sql_single)";
            $sql .= " Order By ItemType desc, ItemName asc ";
            
            $itemAry = $this->returnArray($sql);
            $numOfItem = count($itemAry);
            for ($i = 0; $i < $numOfItem; $i ++) {
                $_itemId = $itemAry[$i]['ItemID'];
                $_locationId = $itemAry[$i]['LocationID'];
                $_itemType = $itemAry[$i]['ItemType'];
                
                if ($_itemType == 2) {
                    // bulk item
                    $_quantity = $this->Get_eInventory_Bulk_Item_Normal_Quantity($_itemId, $_locationId);
                    
                    $itemAry[$i][4] = $_quantity;
                    $itemAry[$i]['Quantity'] = $_quantity;
                }
            }
            
            return $itemAry;
        }

        function Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly = 0, $BookingStatusArr = '')
        {
            $BookingDateTimeArr = array();
            $numOfDate = count($DateArr);
            $numOfTimeRange = count($StartTimeArr);
            
            // ## Get all time clash time range by getting the existing booking
            $TimeClash_Time_Arr = array();
            $TimeClash_Date_Arr = array();
            for ($i = 0; $i < $numOfTimeRange; $i ++) {
                $thisStartTime = $StartTimeArr[$i];
                $thisEndTime = $EndTimeArr[$i];
                
                $thisBookingArr = $this->Get_Room_Booking_In_A_TimeRange_Of_Multiple_Date($thisStartTime, $thisEndTime, $DateArr, array(
                    $RoomID
                ), $ReturnAsso = 1, $BookingStatusArr);
                
                if (count($thisBookingArr) > 0) {
                    foreach ($thisBookingArr as $thisRoomID => $thisRoomBookingArr) {
                        if (is_array($thisRoomBookingArr) && count($thisRoomBookingArr) > 0) {
                            $thisBookingDateArr = array_keys($thisRoomBookingArr);
                            $numOfBookingDate = count($thisBookingDateArr);
                            
                            for ($j = 0; $j < $numOfBookingDate; $j ++) {
                                $thisBookingDate = $thisBookingDateArr[$j];
                                $thisBookingInTheDateArr = $thisRoomBookingArr[$thisBookingDate];
                                $numOfBookingInTheDate = count($thisBookingInTheDateArr);
                                
                                for ($k = 0; $k < $numOfBookingInTheDate; $k ++) {
                                    $thisInfoArr = array(
                                        'BookingID' => $thisBookingInTheDateArr[$k]['BookingID'],
                                        'RequestedByUserName' => $thisBookingInTheDateArr[$k]['RequestedByUserName'],
                                        'ReponsibleUserName' => $thisBookingInTheDateArr[$k]['ReponsibleUserName'],
                                        'Date' => $thisBookingInTheDateArr[$k]['Date'],
                                        'StartTime' => $thisBookingInTheDateArr[$k]['StartTime'],
                                        'EndTime' => $thisBookingInTheDateArr[$k]['EndTime']
                                    );
                                    
                                    (array) $TimeClash_Date_Arr[$thisBookingDate][$thisStartTime][] = $thisInfoArr;
                                    (array) $TimeClash_Time_Arr[$thisStartTime][$thisBookingDate][] = $thisInfoArr;
                                }
                            }
                        }
                    }
                }
            }
            
            if ($TimeClashOnly == 1)
                return array(
                    $TimeClash_Date_Arr,
                    $TimeClash_Time_Arr
                );
            else {
                $BookingDateTimeArr = array();
                $BookingTimeDateArr = array();
                for ($i = 0; $i < $numOfDate; $i ++) {
                    $thisDate = $DateArr[$i];
                    
                    for ($j = 0; $j < $numOfTimeRange; $j ++) {
                        $thisStartTime = $StartTimeArr[$j];
                        $thisEndTime = $EndTimeArr[$j];
                        
                        $thisBookingArr = $TimeClash_Time_Arr[$thisStartTime][$thisDate];
                        if (is_array($thisBookingArr) == false || count($thisBookingArr) == 0) {
                            // ## no time clash
                            $tmpArr = array(
                                'StartTime' => $thisStartTime,
                                'EndTime' => $thisEndTime
                            );
                            $BookingDateTimeArr[$thisDate][] = $tmpArr;
                            
                            $tmpArr = array(
                                'Date' => $thisDate,
                                'EndTime' => $thisEndTime
                            );
                            $BookingTimeDateArr[$thisStartTime][] = $tmpArr;
                        }
                    }
                }
                
                return array(
                    $BookingDateTimeArr,
                    $BookingTimeDateArr,
                    $TimeClash_Date_Arr,
                    $TimeClash_Time_Arr
                );
            }
        }

        function Get_Room_DateTimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr, $BookingStatusArr = '')
        {
            $TmpArr = $this->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly = 1, $BookingStatusArr);
            return $TmpArr[0];
        }

        function Get_Room_TimeDateClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr, $BookingStatusArr = '')
        {
            $TmpArr = $this->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly = 1, $BookingStatusArr);
            return $TmpArr[1];
        }

        function Get_Item_Booking_DateTimeArray_And_TimeClashArray($ItemID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly = 0, $BookingStatusArr = '')
        {
            $BookingDateTimeArr = array();
            $numOfDate = count($DateArr);
            $numOfTimeRange = count($StartTimeArr);
            
            // ## Get all time clash time range by getting the existing booking
            $TimeClash_Time_Arr = array();
            $TimeClash_Date_Arr = array();
            for ($i = 0; $i < $numOfTimeRange; $i ++) {
                $thisStartTime = $StartTimeArr[$i];
                $thisEndTime = $EndTimeArr[$i];
                $arrItemID = explode(",", $ItemID);
                $thisBookingArr = $this->Get_Item_Booking_In_A_TimeRange_Of_Multiple_Date($thisStartTime, $thisEndTime, $DateArr, $arrItemID, $ReturnAsso = 1, $BookingStatusArr);
                if (count($thisBookingArr) > 0) {
                    foreach ($thisBookingArr as $thisItemID => $thisRoomBookingArr) {
                        if (is_array($thisRoomBookingArr) && count($thisRoomBookingArr) > 0) {
                            $thisBookingDateArr = array_keys($thisRoomBookingArr);
                            $numOfBookingDate = count($thisBookingDateArr);
                            
                            for ($j = 0; $j < $numOfBookingDate; $j ++) {
                                $thisBookingDate = $thisBookingDateArr[$j];
                                $thisBookingInTheDateArr = $thisRoomBookingArr[$thisBookingDate];
                                $numOfBookingInTheDate = count($thisBookingInTheDateArr);
                                
                                for ($k = 0; $k < $numOfBookingInTheDate; $k ++) {
                                    $thisInfoArr = array(
                                        'BookingID' => $thisBookingInTheDateArr[$k]['BookingID'],
                                        'RequestedByUserName' => $thisBookingInTheDateArr[$k]['RequestedByUserName'],
                                        'ReponsibleUserName' => $thisBookingInTheDateArr[$k]['ReponsibleUserName'],
                                        'Date' => $thisBookingInTheDateArr[$k]['Date'],
                                        'StartTime' => $thisBookingInTheDateArr[$k]['StartTime'],
                                        'EndTime' => $thisBookingInTheDateArr[$k]['EndTime']
                                    );
                                    
                                    (array) $TimeClash_Date_Arr[$thisBookingDate][$thisStartTime][] = $thisInfoArr;
                                    (array) $TimeClash_Time_Arr[$thisStartTime][$thisBookingDate][] = $thisInfoArr;
                                }
                            }
                        }
                    }
                }
            }
            
            if ($TimeClashOnly == 1)
                return array(
                    $TimeClash_Date_Arr,
                    $TimeClash_Time_Arr
                );
            else {
                $BookingDateTimeArr = array();
                $BookingTimeDateArr = array();
                
                for ($i = 0; $i < $numOfDate; $i ++) {
                    $thisDate = $DateArr[$i];
                    
                    for ($j = 0; $j < $numOfTimeRange; $j ++) {
                        $thisStartTime = $StartTimeArr[$j];
                        $thisEndTime = $EndTimeArr[$j];
                        
                        $thisBookingArr = $TimeClash_Time_Arr[$thisStartTime][$thisDate];
                        if (is_array($thisBookingArr) == false || count($thisBookingArr) == 0) {
                            // ## no time clash
                            $tmpArr = array(
                                'StartTime' => $thisStartTime,
                                'EndTime' => $thisEndTime
                            );
                            $BookingDateTimeArr[$thisDate][] = $tmpArr;
                            
                            $tmpArr = array(
                                'Date' => $thisDate,
                                'EndTime' => $thisEndTime
                            );
                            $BookingTimeDateArr[$thisStartTime][] = $tmpArr;
                        }
                    }
                }
                
                return array(
                    $BookingDateTimeArr,
                    $BookingTimeDateArr,
                    $TimeClash_Date_Arr,
                    $TimeClash_Time_Arr
                );
            }
        }

        function Get_Item_DateTimeClashArray($ItemID, $DateArr, $StartTimeArr, $EndTimeArr, $BookingStatusArr = '')
        {
            $TmpArr = $this->Get_Item_Booking_DateTimeArray_And_TimeClashArray($ItemID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly = 1, $BookingStatusArr);
            return $TmpArr[0];
        }

        function Get_Item_TimeDateClashArray($ItemID, $DateArr, $StartTimeArr, $EndTimeArr, $BookingStatusArr = '')
        {
            $TmpArr = $this->Get_Item_Booking_DateTimeArray_And_TimeClashArray($ItemID, $DateArr, $StartTimeArr, $EndTimeArr, $TimeClashOnly = 1, $BookingStatusArr);
            return $TmpArr[1];
        }

        /*
         * function Get_Room_Booking_DateTimeArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr)
         * {
         * $TmpArr = $this->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr);
         * return $TmpArr[0];
         * }
         *
         * function Get_Room_Booking_TimeDateArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr)
         * {
         * $TmpArr = $this->Get_Room_Booking_DateTimeArray_And_TimeClashArray($RoomID, $DateArr, $StartTimeArr, $EndTimeArr);
         * return $TmpArr[1];
         * }
         */
        function Get_StartTime_EndTime_TimeSlotID_Array_By_TimeRange_Array($TimeRangeArr)
        {
            $numOfTimeRange = count($TimeRangeArr);
            $StartTimeArr = array();
            $EndTimeArr = array();
            $StartTime_TimeSlotID_Mapping_Arr = array();
            
            for ($i = 0; $i < $numOfTimeRange; $i ++) {
                $thisTmpArr = explode('_', $TimeRangeArr[$i]);
                $thisStartTime = $thisTmpArr[0];
                $thisEndTime = $thisTmpArr[1];
                $thisTimeSlotID = $thisTmpArr[2];
                
                $StartTimeArr[] = $thisStartTime;
                $EndTimeArr[] = $thisEndTime;
                $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime] = $thisTimeSlotID;
            }
            
            return array(
                $StartTimeArr,
                $EndTimeArr,
                $StartTime_TimeSlotID_Mapping_Arr
            );
        }

        function New_Room_Booking($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr, $iCalEventArr, $FolderPath = '', $CustRemarkAry = array(), $BookingEvent = '', $ApplyDoorAccess = 0, $BookingCategoryID = '')
        {
            global $plugin, $PATH_WRT_ROOT;
            $this->Load_General_Setting();

            $SuccessArr = array();
            $numOfSingleItem = count($SingleItemIDArr);
            
            $CancelDayBookingIfOneItemIsNA = ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1) ? 1 : 0;
            
            // $this->Start_Trans_For_Lock_Table_Procedure();
            $this->Start_Trans();
            // Lock tables
            $sql = "LOCK TABLES 
								INTRANET_EBOOKING_RECORD WRITE
								INTRANET_EBOOKING_BOOKING_DETAILS WRITE
								";
            $this->db_db_query($sql);
            
            $ItemBookingDateClashArr = array();
            $ItemClashedDateTimeArr = array();
            
            // $TimeDateArr = $this->Convert_DateTimeArr_To_TimeDateArr($DateTimeArr);
            
            // ## Check if the Item is being booked already in the selected date and time slot
            $AvailableBookingSingleItemID = array();
            $ItemClashCheckArr = array();
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeRangeInfoArr) {
                $numOfTimeRange = count($thisTimeRangeInfoArr);
                for ($i = 0; $i < $numOfTimeRange; $i ++) {
                    $thisStartTime = $thisTimeRangeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeRangeInfoArr[$i]['EndTime'];
                    
                    for ($j = 0; $j < $numOfSingleItem; $j ++) {
                        $thisSingleItemID = $SingleItemIDArr[$j];
                        $ItemClashCheckArr[$thisDate][$thisStartTime][$thisSingleItemID] = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $thisSingleItemID, $thisDate, $thisStartTime, $thisEndTime);
                        
                        if ($ItemClashCheckArr[$thisDate][$thisStartTime][$thisSingleItemID] == 1)
                            $AvailableBookingSingleItemID[$thisDate][$thisStartTime][] = $thisSingleItemID;
                    }
                    
                    if (in_array(false, (array) $ItemClashCheckArr[$thisDate][$thisStartTime]))
                        $ItemClashedDateTimeArr[$thisDate][$thisStartTime] = true;
                }
            }
            
            $InsertBookingArr = array();
            $BookingStatusArr = array(
                1
            ); // check for approved booking records only
            
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeInfoArr[$i]['EndTime'];
                    $thisTimeSlotID = $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime];
                    $_thisTimeSlotID = $thisTimeInfoArr[$i]['TimeSlotID'];
                    
                    // ## skip the date booking if one of the item is N.A. on that date
                    if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)
                        continue;
                        
                        // ## Check if the timeslot is available for the room and items
                        // $thisCurRoomBooking = $this->Get_Room_Booking_In_A_TimeRange_Of_Multiple_Date($thisStartTime, $thisEndTime, array($thisDate), array($RoomID), $ReturnAsso=0, $BookingStatusArr);
                        // $numOfCurRoomBooking = count($thisCurRoomBooking);
                    $IsRoomAvailable = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $thisDate, $thisStartTime, $thisEndTime);
                    
                    // Checked in "if ($ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)" already
                    // $thisCurItemBooking = $this->Get_Item_Booking_In_A_TimeRange_Of_Multiple_Date($thisStartTime, $thisEndTime, array($thisDate), $SingleItemIDArr, $ReturnAsso=0, $BookingStatusArr);
                    // $numOfCurItemBooking = count($thisCurItemBooking);
                    
                    if ($IsRoomAvailable) {
                        $InsertBookingArr[] = "	(
														'$thisTimeSlotID', '$thisDate', '$thisStartTime', '$thisEndTime', '" . $this->Get_Safe_Sql_Query($BookingRemarks) . "', '$FolderPath',
														'" . $_SESSION['UserID'] . "', now(), '$ResponsibleUID', 1, '$CancelDayBookingIfOneItemIsNA', 1, '$ApplyDoorAccess',
														'" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now(), '" . $this->Get_Safe_Sql_Query($BookingEvent) . "','$_thisTimeSlotID'
													)";
                    }
                }
            }
            
            if (count($InsertBookingArr) == 0) {
                $this->RollBack_Trans();
                $sql = "UNLOCK TABLES";
                $this->db_db_query($sql);
                return false;
            }
            
            $InsertBookingList = implode(',', $InsertBookingArr);
            
            // ## Insert Booking Record
            $sql = "	Insert Into INTRANET_EBOOKING_RECORD		
									(
										PeriodID, Date, StartTime, EndTime, Remark, Attachment,
										RequestedBy, RequestDate, ResponsibleUID, BookingStatus, DeleteOtherRelatedIfReject, RecordStatus, ApplyDoorAccess,
										InputBy, ModifiedBy, DateInput, DateModified, Event, TimeSlotID
									)
									Values
									$InsertBookingList
								";
            $SuccessArr['Insert_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Update RelatedTo Field
            $FirstBookingID = $this->db_insert_id();
            $sql = "Select max(BookingID) From INTRANET_EBOOKING_RECORD";
            $tmpArr = $this->returnVector($sql);
            $LastBookingID = $tmpArr[0];
            
            /*
             * $sql = "Update INTRANET_EBOOKING_RECORD
             * Set RelatedTo = BookingID
             * Where BookingID Between '$FirstBookingID' And '$LastBookingID'
             * ";
             */
            $sql = "Update INTRANET_EBOOKING_RECORD 
							Set RelatedTo = '$FirstBookingID'
							Where BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            
            $SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Booking Category Relation
            
            if ($BookingCategoryID != '') {
                
                if ($FirstBookingID != $LastBookingID) {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID Between '$FirstBookingID' And '$LastBookingID'";
                } else {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID = '$FirstBookingID'";
                }
                $AddedBookingIDAry = $this->ReturnResultSet($sql);
                
                $insertRowsAry = array();
                foreach ($AddedBookingIDAry as $_BookingID) {
                    
                    $insertRowsAry[] .= "( '" . $BookingCategoryID . "', '" . $_BookingID['BookingID'] . "', now(),'" . $_SESSION['UserID'] . "',now(),'" . $_SESSION['UserID'] . "' )";
                }
                
                $sql = "INSERT INTO INTRANET_EBOOKING_RECORD_CATEGORY_RELATION 
								(CategoryID,BookingID,DateInput,InputBy,DateModified,ModifiedBy) 
							VALUES 
								" . implode(',', $insertRowsAry) . "
							";
                $this->db_db_query($sql);
            }
            
            // add cust remarks
            if ($SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] && ! empty($CustRemarkAry)) {
                $sql = "delete from INTRANET_EBOOKING_RECORD_CUST_REMARK where BookingID='$FirstBookingID'";
                $this->db_db_query($sql);
                
                foreach ($CustRemarkAry as $fields[] => $data[]);
                $fields_str = implode(",", $fields);
                foreach ($data as $k => $d)
                    $data_str .= ",'" . $this->Get_Safe_Sql_Query($d) . "'";
                
                $sql = "insert into INTRANET_EBOOKING_RECORD_CUST_REMARK ";
                $sql .= " (BookingID, $fields_str) values ";
                $sql .= "($FirstBookingID" . $data_str . ")";
                $this->db_db_query($sql);
            }
            
            // ## Insert Room Booking Details and Item Booking Details
            // Build "StartTime-Date BookingID" Mapping
            $sql = "Select 
								BookingID,
								Date,
								StartTime,
								EndTime
						From
								INTRANET_EBOOKING_RECORD
						Where
								BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $BookingInfoArr = $this->returnArray($sql);
            $numOfBooking = count($BookingInfoArr);
            
            $DateTime_BookingID_MappingArr = array();
            for ($i = 0; $i < $numOfBooking; $i ++) {
                $thisBookingID = $BookingInfoArr[$i]['BookingID'];
                $thisDate = $BookingInfoArr[$i]['Date'];
                $thisStartTime = $BookingInfoArr[$i]['StartTime'];
                
                // ## skip the date booking if one of the item is N.A. on that date
                if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)
                    continue;
                
                $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime] = $thisBookingID;
            }
            
            // ## Since the item / room may not book on ALL the inserted booking dates (due to time clash),
            // ## loop the prepared $DateTimeArr instead of $BookingInfoArr to avoid creating extra booking records
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    
                    // ## skip the date booking if one of the item is N.A. on that date
                    if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)
                        continue;
                        
                        // Book Room
                    $RoomNeedApproval[$thisBookingID] = $this->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $RoomID);
                    
                    if ($RoomNeedApproval[$thisBookingID])
                        $RoomBookingStatus = 0;
                    else
                        $RoomBookingStatus = 1;
                    $SuccessArr['Insert_Room_Booking_Record'][$thisBookingID] = $this->Insert_Room_Booking_Record($thisBookingID, $RoomID, $RoomBookingStatus);
                    
                    // Book Single Item
                    $thisCanBookSingleItemIDArr = $AvailableBookingSingleItemID[$thisDate][$thisStartTime];
                    if (is_array($thisCanBookSingleItemIDArr) && count($thisCanBookSingleItemIDArr) > 0)
                        $SuccessArr['Insert_Item_Booking_Record'][$thisBookingID] = $this->Insert_Item_Booking_Record($thisBookingID, $thisCanBookSingleItemIDArr, $BookingStatus = '', $RoomBookingStatus);
                        
                        // # check if we need to create a iCal event or not
                    if (sizeof($iCalEventArr) > 0) {
                        
                        include_once ("icalendar.php");
                        $iCal = new icalendar();
                        
                        $booking_date = $thisDate;
                        $start_time = $thisTimeInfoArr[$i]['StartTime'];
                        $end_time = $thisTimeInfoArr[$i]['EndTime'];
                        
                        $event_date_time = $booking_date . " " . $start_time;
                        
                        $start_timestamp = strtotime($booking_date . " " . $start_time) . "<BR>";
                        $end_timestamp = strtotime($booking_date . " " . $end_time);
                        
                        $duration = ($end_timestamp - $start_timestamp) / 60;
                        
                        $OwnerID = $iCalEventArr[0]['Owner'];
                        $iCalEventTitle = $iCalEventArr[0]['EventTitle'];
                        $BookingRemarks = $iCalEventArr[0]['EventDesc'];
                        $location = $iCalEventArr[0]['Location'];
                        $calID = $iCalEventArr[0]['EventCalID'];
                        
                        $isImportant = "";
                        $isAllDay = "";
                        $access = "P";
                        $url = "";
                        
                        if ($calID == "") {
                            // get the CalendarID for the targer user
                            $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                            $TargetCalendarID = $iCal->returnVector($sql);
                            $calID = $TargetCalendarID[0];
                            
                            // Insert My Calendar if user not yet have My Calendar
                            if ($calID == "" || $calID == 0) {
                                $iCal->insertMyCalendar();
                                $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                                $TargetCalendarID = $iCal->returnVector($sql);
                                $calID = $TargetCalendarID[0];
                            }
                        }
                        // # if the booking is auto approve, create the event directly
                        
                        $fieldname = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
                        $fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID";
                        
                        $fieldvalue = "'".$OwnerID . "', '$event_date_time', NOW(), NOW(), $duration, ";
                        $fieldvalue .= "'$isImportant', '$isAllDay', '$access', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventTitle)) . "', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($BookingRemarks)) . "', ";
                        $fieldvalue .= "'" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)) . "', '$url', '$calID'";
                        
                        $sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
                        $tempSuccess = $this->db_db_query($sql);
                        
                        if ($tempSuccess) {
                            $iCalEventID = $this->db_insert_id();
                            
                            global $schoolNameAbbrev;
                            $uid = $OwnerID . "-{$calID}-{$iCalEventID}@{$schoolNameAbbrev}.tg";
                            $sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$iCalEventID'";
                            $this->db_db_query($sql);
                            
                            $sql = "INSERT INTO INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (BookingID, EventID) VALUES ('$thisBookingID', '$iCalEventID')";
                            $this->db_db_query($sql);
                        }
                    }
                }
            }
            
            if ($plugin['door_access']) {
                include_once ($PATH_WRT_ROOT . "includes/libebooking_door_access.php");
                $lebooking_door_access = new libebooking_door_access();
                
                $SuccessArr['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($FirstBookingID);
            }
            
            // debug_pr($SuccessArr);
            
            if (! in_array(false, $SuccessArr)) {
                $this->Commit_Trans();
                $success = true;
            } else {
                $this->RollBack_Trans();
                $success = false;
            }
            
            $sql = "UNLOCK TABLES";
            $this->db_db_query($sql);
            
            // ## Send email if auto approved booking
            $emailBookingIdAry = array();
            $emailApproveIdAry = array();
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    if ($RoomNeedApproval[$thisBookingID] == false) {
                        // 2013-0910-1707-32073
                        // $SuccessArr['Send_eMail'][$thisBookingID] = $this->Email_Booking_Result($thisBookingID);
                        $emailBookingIdAry[] = $thisBookingID;
                        
                        // $SuccessArr['Send_eMail_FollowUp'][$thisBookingID] = $this->SendMailToFollowUpGroup($thisBookingID);
                        // Omas->SendEmailToFollowUpGroup_Merged
                    }                    // Added by bill: Send email to approval group if request approval
                    elseif ($this->SettingArr['ApprovalNotification']) {
                        $emailApproveIdAry[] = $thisBookingID;
                        // $SuccessArr['Send_eMail_Manage'][$thisBookingID] = $this->SendMailToManageGroup($thisBookingID);
                        // Omas->SendMailToManageGroup_Merged
                    }
                }
            }
            
            $emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
            if (count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->Email_Booking_Result_Merged($emailBookingIdAry);
            }
            
            if (count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->SendEmailToFollowUpGroup_Merged($emailBookingIdAry);
            }
            
            $emailApproveIdAry = array_values(array_unique($emailApproveIdAry));
            if (count($emailApproveIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->SendMailToManageGroup_Merged($emailApproveIdAry);
            }
            
            return $success;
        }

        function New_Item_Booking($ItemID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $BookingRemarks, $ResponsibleUID, $iCalEventArr, $FolderPath, $CustRemarkAry = array(), $BookingEvent = '', $BookingCategoryID = '')
        {
            global $plugin, $PATH_WRT_ROOT;
            $this->Load_General_Setting();
            
            $DateArr = array_keys($DateTimeArr);
            
            // $AvailableBookingPeriodArr = $this->getAllAvailablePeriodOfUser(LIBEBOOKING_FACILITY_TYPE_ITEM, $ItemID, $_SESSION['UserID'], $DateArr);
            // $AvailableBookingPeriodArr = BuildMultiKeyAssoc($AvailableBookingPeriodArr, array("RecordDate", "PeriodID"));
            // $AvailableBookingPeriodArr =$this->Get_Facility_Available_Booking_Period(LIBEBOOKING_FACILITY_TYPE_ITEM,$ItemID,$YearInfo[0],$Date, $Date);
            $arrItemID = explode(",", $ItemID);
            
            $SuccessArr = array();
            // $this->Start_Trans_For_Lock_Table_Procedure();
            $this->Start_Trans();
            
            // Lock tables
            $sql = "LOCK TABLES 
								INTRANET_EBOOKING_RECORD WRITE
								INTRANET_EBOOKING_BOOKING_DETAILS WRITE
								";
            $this->db_db_query($sql);
            
            $InsertBookingArr = array();
            $BookingStatusArr = array(
                1
            ); // check for approved booking records only
            
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeInfoArr[$i]['EndTime'];
                    $thisTimeSlotID = $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime];
                    $_thisTimeSlotID = $thisTimeInfoArr[$i]['TimeSlotID'];
                    
                    // ## Check if the item is current available in the selected timeslot
                    // if (!isset($thisCurItemBooking[$thisStartTime]))
                    // $thisCurItemBooking[$thisStartTime] = $this->Get_Item_Booking_In_A_TimeRange_Of_Multiple_Date($thisStartTime, $thisEndTime, $DateArr, $arrItemID, $ReturnAsso=0, $BookingStatusArr);
                    
                    // debug_pr($thisCurItemBooking);
                    // $thisCurBookingArr = $thisCurItemBooking[$thisStartTime][$ItemID][$thisDate];
                    // if ($thisCurBookingArr=='') // no booking records yet
                    // $thisCurBookingArr = array();
                    
                    $IsItemAvailable = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $arrItemID, $thisDate, $thisStartTime, $thisEndTime);
                    
                    // if ($numOfCurItemBooking == 0)
                    if ($IsItemAvailable) {
                        $InsertBookingArr[] = "	(
														'$thisTimeSlotID', '$thisDate', '$thisStartTime', '$thisEndTime', '" . $this->Get_Safe_Sql_Query($BookingRemarks) . "','$FolderPath',
														'" . $_SESSION['UserID'] . "', now(), '$ResponsibleUID', 1, 1,
														'" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now(), '" . $this->Get_Safe_Sql_Query($BookingEvent) . "', '$_thisTimeSlotID'
													)";
                    }
                }
            }
            
            if (count($InsertBookingArr) == 0) {
                $this->RollBack_Trans();
                $sql = "UNLOCK TABLES";
                $this->db_db_query($sql);
                return false;
            }
            
            $InsertBookingList = implode(',', $InsertBookingArr);
            
            // ## Insert Booking Record
            $sql = "	Insert Into INTRANET_EBOOKING_RECORD		
									(
										PeriodID, Date, StartTime, EndTime, Remark, Attachment,
										RequestedBy, RequestDate, ResponsibleUID, BookingStatus, RecordStatus,
										InputBy, ModifiedBy, DateInput, DateModified, Event, TimeSlotID
									)
									Values
									$InsertBookingList
								";
            $SuccessArr['Insert_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Update RelatedTo Field
            $FirstBookingID = $this->db_insert_id();
            $sql = "Select max(BookingID) From INTRANET_EBOOKING_RECORD";
            $tmpArr = $this->returnVector($sql);
            $LastBookingID = $tmpArr[0];
            
            /*
             * $sql = "Update INTRANET_EBOOKING_RECORD
             * Set RelatedTo = BookingID
             * Where BookingID Between '$FirstBookingID' And '$LastBookingID'
             * ";
             */
            $sql = "Update INTRANET_EBOOKING_RECORD 
							Set RelatedTo = '$FirstBookingID'
							Where BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Booking Category Relation
            
            if ($BookingCategoryID != '') {
                
                if ($FirstBookingID != $LastBookingID) {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID Between '$FirstBookingID' And '$LastBookingID'";
                } else {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID = '$FirstBookingID'";
                }
                $AddedBookingIDAry = $this->ReturnResultSet($sql);
                
                $insertRowsAry = array();
                foreach ($AddedBookingIDAry as $_BookingID) {
                    
                    $insertRowsAry[] .= "( '" . $BookingCategoryID . "', '" . $_BookingID['BookingID'] . "', now(),'" . $_SESSION['UserID'] . "',now(),'" . $_SESSION['UserID'] . "' )";
                }
                
                $sql = "INSERT INTO INTRANET_EBOOKING_RECORD_CATEGORY_RELATION 
								(CategoryID,BookingID,DateInput,InputBy,DateModified,ModifiedBy) 
							VALUES 
								" . implode(',', $insertRowsAry) . "
							";
                $this->db_db_query($sql);
            }
            
            // add cust remarks
            if ($SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] && ! empty($CustRemarkAry)) {
                $sql = "delete from INTRANET_EBOOKING_RECORD_CUST_REMARK where BookingID='$FirstBookingID'";
                $this->db_db_query($sql);
                
                foreach ($CustRemarkAry as $fields[] => $data[]);
                $fields_str = implode(",", $fields);
                foreach ($data as $k => $d)
                    $data_str .= ",'" . $this->Get_Safe_Sql_Query($d) . "'";
                
                $sql = "insert into INTRANET_EBOOKING_RECORD_CUST_REMARK ";
                $sql .= " (BookingID, $fields_str) values ";
                $sql .= "('$FirstBookingID'" . $data_str . ")";
                $this->db_db_query($sql);
            }
            
            // ## Insert Item Booking Details
            // Build "StartTime-Date BookingID" Mapping
            $sql = "Select 
								BookingID,
								Date,
								StartTime,
								EndTime
						From
								INTRANET_EBOOKING_RECORD
						Where
								BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $BookingInfoArr = $this->returnArray($sql);
            $numOfBooking = count($BookingInfoArr);
            
            $DateTime_BookingID_MappingArr = array();
            for ($i = 0; $i < $numOfBooking; $i ++) {
                $thisBookingID = $BookingInfoArr[$i]['BookingID'];
                $thisDate = $BookingInfoArr[$i]['Date'];
                $thisStartTime = $BookingInfoArr[$i]['StartTime'];
                
                $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime] = $thisBookingID;
            }
            
            // ## Since the item / room may not book on ALL the inserted booking dates (due to time clash),
            // ## loop the prepared $DateTimeArr instead of $BookingInfoArr to avoid creating extra booking records
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    
                    // Omas fix B114302 comment - already done in Insert_Item_Booking_Record
                    // $ItemNeedApprovalArr[$thisBookingID] = $this->Check_If_Booking_Need_Approval($_SESSION['UserID'], $ItemID, '');
                    // if ($ItemNeedApprovalArr[$thisBookingID])
                    // $ItemBookingStatus = 0;
                    // else
                    // $ItemBookingStatus = 1;
                    
                    // Book Single Item
                    $arrItemID = explode(",", $ItemID);
                    // $SuccessArr['Insert_Item_Booking_Record'][$thisBookingID] = $this->Insert_Item_Booking_Record($thisBookingID, $arrItemID, $ItemBookingStatus);
                    $SuccessArr['Insert_Item_Booking_Record'][$thisBookingID] = $this->Insert_Item_Booking_Record($thisBookingID, $arrItemID, '');
                    // Z117075 check if any item in this booking is autoapproved (will send email for this booking)
                    foreach ((array) $arrItemID as $thisItemID) {
                        if (! $this->Check_If_Booking_Need_Approval($_SESSION['UserID'], $thisItemID, '')) {
                            $AutoApproveArr[] = $thisBookingID;
                        }
                        if ($this->Check_If_Booking_Need_Approval($_SESSION['UserID'], $thisItemID, '')) {
                            $NeedApproveArr[] = $thisBookingID;
                        }
                    }
                    
                    // # check if we need to create a iCal event or not
                    if (sizeof($iCalEventArr) > 0) {
                        
                        include_once ("icalendar.php");
                        $iCal = new icalendar();
                        
                        $booking_date = $thisDate;
                        $start_time = $thisTimeInfoArr[$i]['StartTime'];
                        $end_time = $thisTimeInfoArr[$i]['EndTime'];
                        
                        $event_date_time = $booking_date . " " . $start_time;
                        
                        $start_timestamp = strtotime($booking_date . " " . $start_time) . "<BR>";
                        $end_timestamp = strtotime($booking_date . " " . $end_time);
                        
                        $duration = ($end_timestamp - $start_timestamp) / 60;
                        
                        $OwnerID = $iCalEventArr[0]['Owner'];
                        $iCalEventTitle = $iCalEventArr[0]['EventTitle'];
                        $BookingRemarks = $iCalEventArr[0]['EventDesc'];
                        $location = $iCalEventArr[0]['Location'];
                        $calID = $iCalEventArr[0]['EventCalID'];
                        
                        $isImportant = "";
                        $isAllDay = "";
                        $access = "P";
                        $url = "";
                        
                        if ($calID == "") {
                            // get the CalendarID for the targer user
                            $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                            $TargetCalendarID = $iCal->returnVector($sql);
                            $calID = $TargetCalendarID[0];
                            
                            // Insert My Calendar if user not yet have My Calendar
                            if ($calID == "" || $calID == 0) {
                                $iCal->insertMyCalendar();
                                $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                                $TargetCalendarID = $iCal->returnVector($sql);
                                $calID = $TargetCalendarID[0];
                            }
                        }
                        // # if the booking is auto approve, create the event directly
                        $fieldname = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
                        $fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID";
                        
                        $fieldvalue = "'".$OwnerID . "', '$event_date_time', NOW(), NOW(), $duration, ";
                        $fieldvalue .= "'$isImportant', '$isAllDay', '$access', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventTitle)) . "', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($BookingRemarks)) . "', ";
                        $fieldvalue .= "'" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)) . "', '$url', '$calID'";
                        
                        $sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
                        $tempSuccess = $this->db_db_query($sql);
                        
                        if ($tempSuccess) {
                            $iCalEventID = $this->db_insert_id();
                            
                            global $schoolNameAbbrev;
                            $uid = $OwnerID . "-{$calID}-{$iCalEventID}@{$schoolNameAbbrev}.tg";
                            $sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$iCalEventID'";
                            $iCal->db_db_query($sql);
                            
                            $sql = "INSERT INTO INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (BookingID, EventID) VALUES ('$thisBookingID', '$iCalEventID')";
                            $this->db_db_query($sql);
                        }
                    }
                }
            }
            
            if (! in_array(false, $SuccessArr)) {
                $this->Commit_Trans();
                $success = true;
            } else {
                $this->RollBack_Trans();
                $success = false;
            }
            
            $sql = "UNLOCK TABLES";
            $this->db_db_query($sql);
            // debug_pr($ItemNeedApprovalArr[$thisBookingID]);
            // debug_pr($this->SettingArr['ApprovalNotification']);
            // ## Send email if auto approved booking
            $emailBookingIdAry = array();
            $emailApproveIdAry = array();
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    // if ($ItemNeedApprovalArr[$thisBookingID] == false){
                    // Z117075
                    if (in_array($thisBookingID, (array) $AutoApproveArr)) {
                        // 2013-0910-1707-32073
                        // $SuccessArr['Send_eMail'][$thisBookingID] = $this->Email_Booking_Result($thisBookingID);
                        $emailBookingIdAry[] = $thisBookingID;
                        // $SuccessArr['Send_eMail_FollowUp'][$thisBookingID] = $this->SendMailToFollowUpGroup($thisBookingID);
                        // Omas->SendEmailToFollowUpGroup_Merged
                    }
                    // Appproval group notification
                    // elseif($this->SettingArr['ApprovalNotification']) {
                    if ($this->SettingArr['ApprovalNotification'] && in_array($thisBookingID, (array) $NeedApproveArr)) {
                        $emailApproveIdAry[] = $thisBookingID;
                        // $SuccessArr['Send_eMail_Manage'][$thisBookingID] = $this->SendMailToManageGroup($thisBookingID);
                        // Omas->SendMailToManageGroup_Merged
                    }
                }
            }
            
            $emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
            if (count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->Email_Booking_Result_Merged($emailBookingIdAry);
            }
            
            if (count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->SendEmailToFollowUpGroup_Merged($emailBookingIdAry);
            }
            
            $emailApproveIdAry = array_values(array_unique($emailApproveIdAry));
            if (count($emailApproveIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->SendMailToManageGroup_Merged($emailApproveIdAry);
            }
            
            return $success;
        }

        function Insert_Room_Booking_Record($BookingID, $RoomIDArr, $BookingStatus)
        {
            if ($BookingStatus == 1) {
                $ProcessDate = 'now()';
                $ProcessDateTime = 'now()';
            }
            else {
                $ProcessDate = 'Null';
                $ProcessDateTime = 'Null';
            }
            
            if (! is_array($RoomIDArr))
                $RoomIDArr = array(
                    $RoomIDArr
                );
            
            $numOfRoom = count($RoomIDArr);
            $InsertValueArr = array();
            for ($i = 0; $i < $numOfRoom; $i ++) {
                $thisRoomID = $RoomIDArr[$i];
                
                $InsertValueArr[] = " ('$BookingID', '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "', '$thisRoomID', '$BookingStatus', $ProcessDate, 1, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now(), $ProcessDateTime) ";
            }
            $InsertValueList = implode(', ', $InsertValueArr);
            
            $sql = "Insert Into INTRANET_EBOOKING_BOOKING_DETAILS
							(BookingID, FacilityType, FacilityID, BookingStatus, ProcessDate, RecordStatus, InputBy, ModifiedBy, DateInput, DateModified, ProcessDateTime)
						Values
							$InsertValueList
						";
            return $this->db_db_query($sql);
        }

        function Insert_Item_Booking_Record($BookingID, $ItemIDArr, $BookingStatus, $RoomBookingStatus = '')
        {
            if (! is_array($ItemIDArr))
                $ItemIDArr = array(
                    $ItemIDArr
                );
            
            $numOfItem = count($ItemIDArr);
            $InsertValueArr = array();
            for ($i = 0; $i < $numOfItem; $i ++) {
                $thisItemID = $ItemIDArr[$i];
                
                if ($BookingStatus == '') {
                    // If the room is pending, the included items are always pending also
                    // Omas fix B114302
                    // if ($RoomBookingStatus == 0)
                    if ($RoomBookingStatus === 0)
                        $thisBookingStatus = 0;
                    else {
                        $ItemNeedApproval = $this->Check_If_Booking_Need_Approval($_SESSION['UserID'], $thisItemID, '');
                        if ($ItemNeedApproval)
                            $thisBookingStatus = 0;
                        else
                            $thisBookingStatus = 1;
                    }
                } else {
                    $thisBookingStatus = $BookingStatus;
                }
                
                if ($thisBookingStatus == 1) {
                    $ProcessDate = 'now()';
                    $ProcessDateTime = 'now()';
                }
                else {
                    $ProcessDate = 'Null';
                    $ProcessDateTime = 'Null';
                }
                
                $InsertValueArr[] = " ('$BookingID', '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "', '$thisItemID', '$thisBookingStatus', $ProcessDate, 1, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now(), $ProcessDateTime) ";
            }
            $InsertValueList = implode(', ', $InsertValueArr);
            
            $sql = "Insert Into INTRANET_EBOOKING_BOOKING_DETAILS
							(BookingID, FacilityType, FacilityID, BookingStatus, ProcessDate, RecordStatus, InputBy, ModifiedBy, DateInput, DateModified, ProcessDateTime)
						Values
							$InsertValueList
						";
            return $this->db_db_query($sql);
        }

        function Convert_DateTimeArr_To_TimeDateArr($DateTimeArr)
        {
            $TimeDateArr = array();
            foreach ($DateTimeArr as $thisDate => $thisDateTimeArr) {
                $numOfTime = count($thisDateTimeArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisDateTimeArr[$i]['StartTime'];
                    $thisDateTimeArr[$i]['Date'] = $thisDate;
                    
                    (array) $TimeDateArr[$thisStartTime][] = $thisDateTimeArr[$i];
                }
            }
            
            return $TimeDateArr;
        }

        function Check_If_Booking_Need_Approval($ParUserID, $ItemID = '', $RoomID = '')
        {
            $RuleArr = $this->Get_User_Booking_Rule($RuleID = '', $ItemID, $RoomID, $ParUserID);
            $NeedApprovalArr = Get_Array_By_Key($RuleArr, 'NeedApproval');
            
            // 2012-1207-1409-26054: logic changed to if one of the User Booking Rule no need approval => no need approval
            // ## If one of the User Booking Rule Need Approval => the overall booking need approval
            // if (in_array('1', $NeedApprovalArr))
            // $OverallNeedApproval = 1;
            // else
            // $OverallNeedApproval = 0;
            if (in_array('0', $NeedApprovalArr)) {
                $OverallNeedApproval = 0;
            } else {
                $OverallNeedApproval = 1;
            }
            return $OverallNeedApproval;
        }

        function Check_Can_Book_For_Others($ParUserID, $ItemID = '', $RoomID = '')
        {
            $RuleArr = $this->Get_User_Booking_Rule($RuleID = '', $ItemID, $RoomID, $ParUserID);
            $CanBookForOthersArr = Get_Array_By_Key($RuleArr, 'CanBookForOthers');
            
            // ## If one of the User Booking Rule can book for others => the overall booking can book for others
            if (in_array('1', $CanBookForOthersArr))
                $CanBookForOthers = 1;
            else
                $CanBookForOthers = 0;
            
            return $CanBookForOthers;
        }
        
        // function Check_If_Facility_Is_Available($FacilityType, $FacilityID, $Date, $StartTime, $EndTime, $AvailableBookingPeriodArr='', $ExistingBookingArr='')
        // {
        // $RoomID = ($FacilityType==1)? $FacilityID : '';
        // $ItemID = ($FacilityType==2)? $FacilityID : '';
        //
        // # Check if the period is available for booking or not
        // if ($AvailableBookingPeriodArr == '')
        // {
        // $AvailableBookingPeriodArr = $this->getAllAvailablePeriodOfUser($FacilityType, $FacilityID, $_SESSION['UserID'], array($Date));
        // $AvailableBookingPeriodArr = BuildMultiKeyAssoc($AvailableBookingPeriodArr, array("RecordDate", "PeriodID"));
        // }
        //
        // $AvailableArr['WithinAvailableBookingPeriod'] = false;
        //
        // $AvailableTimeRange = array();
        // $InAvailablePeriod = false;
        // foreach((array)$AvailableBookingPeriodArr[$Date] as $thisDatePeriods)
        // {
        // $AvailableStartTimets = strtotime($thisDatePeriods['StartTime']);
        // $AvailableEndTimets = strtotime($thisDatePeriods['EndTime']);
        //
        // $BookStartTimets = strtotime($StartTimeArr[0]);
        // $BookEndTimets = strtotime($EndTimeArr[0]);
        //
        //
        // $AvailableTimeRange[] = Date("H:i",$AvailableStartTimets).'-'.Date("H:i",$AvailableEndTimets);
        // $AvailableStartTime[] = Date("H:i",$AvailableStartTimets);
        // $AvailableEndTime[] = Date("H:i",$AvailableEndTimets);
        // }
        // $FinalAvailableTimeRange = $this->Union_Time_Range($AvailableTimeRange);
        // foreach((array)$FinalAvailableTimeRange as $key=>$time_range)
        // {
        // $available_start_time = substr($time_range,0,strpos($time_range,'-'));
        // $available_end_time = substr($time_range,strpos($time_range,'-')+1,strlen($time_range));
        // $ts_available_start_time = strtotime($available_start_time);
        // $ts_available_end_time = strtotime($available_end_time);
        //
        // $BookStartTimets = strtotime($StartTime);
        // $BookEndTimets = strtotime($EndTime);
        //
        //
        // if($ts_available_start_time<=$BookStartTimets&&$BookStartTimets<=$ts_available_end_time&& $BookEndTimets>=$ts_available_start_time&&$BookEndTimets<=$ts_available_end_time)
        // {
        // $AvailableArr['WithinAvailableBookingPeriod'] = true;
        // break;
        // }
        // }
        //
        // # Check if there are any bookings in the timeslot already
        // if ($ExistingBookingArr == '')
        // {
        // $DateTimeArr = array();
        // $DateTimeArr[$Date][0]['StartTime'] = $StartTime;
        // $DateTimeArr[$Date][0]['EndTime'] = $EndTime;
        //
        // if ($FacilityType==1)
        // $ExistingBookingArr = $this->Get_Room_Booking_In_One_Day_Of_Multiple_TimeSlot(array($RoomID), $DateTimeArr, $ReturnAsso=0);
        // else if ($FacilityType==2)
        // $ExistingBookingArr = $this->Get_Item_Booking_In_One_Day_Of_Multiple_TimeSlot(array($ItemID), $DateTimeArr, $ReturnAsso=0);
        // }
        // $AvailableArr['NoBookingClash'] = (count($ExistingBookingArr)==0)? true : false;
        //
        // if (!in_array(false, $AvailableArr))
        // return true;
        // else
        // return false;
        // }
        function Get_AcademicYear_Term_Info_By_Date($Date)
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            $result = getAcademicYearInfoAndTermInfoByDate($Date);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Check_If_Facility_Is_Available($FacilityType, $FacilityID, $Date, $StartTime, $EndTime, $AvailableBookingPeriodArr = '', $ExistingBookingArr = '', $CheckBookingPeriodOnly = 0, $From_eAdmin = '')
        {
            global $sys_custom;

            if ($sys_custom['eBooking']['EverydayTimetable']) {
                $AvailableArr['WithinAvailableBookingPeriod'] = true;
            }
            else {
                // Get Available Period
                if (trim($AvailableBookingPeriodArr) == '') {
                    // $YearInfo = getAcademicYearInfoAndTermInfoByDate($Date);
                    $YearInfo = $this->Get_AcademicYear_Term_Info_By_Date($Date);
                    $AvailableBookingPeriodArr = $this->Get_Facility_Available_Booking_Period_Of_User($FacilityType, $FacilityID, $_SESSION['UserID'], $YearInfo[0], $Date, $Date, $From_eAdmin);
                }
                
                $AvailableBookingPeriodArr = $this->Union_Time_Range_Of_Date_Assoc($AvailableBookingPeriodArr);

                if (! $this->In_Time_Period($StartTime, $EndTime, $AvailableBookingPeriodArr[$Date]))
                    $AvailableArr['WithinAvailableBookingPeriod'] = false;
                else
                    $AvailableArr['WithinAvailableBookingPeriod'] = true;
            }

            if ($CheckBookingPeriodOnly != 1) {
                // Get Approved Booking Record within this week
                if (trim($ExistingBookingArr) == '')
                    $ExistingBookingArr = $this->Get_Facility_Booking_Record($FacilityType, $FacilityID, $Date, $Date, LIBEBOOKING_BOOKING_STATUS_APPROVED);
                foreach ($ExistingBookingArr as $thisBookingRecord) {
                    $StartTimestamp = strtotime($thisBookingRecord['StartTime']);
                    $EndTimestamp = strtotime($thisBookingRecord['EndTime']);
                    $DateBookingArr[$thisBookingRecord['Date']][$thisBookingRecord['BookingID']] = array(
                        Date("H:i", $StartTimestamp) . '-' . Date("H:i", $EndTimestamp)
                    );
                }
                
                $AvailableArr['NoBookingClash'] = true;
                foreach ((array) $DateBookingArr[$Date] as $thisBookingRecordPeriod) {
                    if ($this->Is_Time_Clash($StartTime, $EndTime, $thisBookingRecordPeriod)) {
                        $AvailableArr['NoBookingClash'] = false;
                        break;
                    }
                }
            }
            
            if (! in_array(false, $AvailableArr))
                return true;
            else
                return false;
        }
        
        // ########## marcus
        function getAllBookableLocationName($LocationID = '', $BuildingID = '', $includeUnbookable=false)
        {
            // if(trim($LocationID)!=''){
            if (! empty($LocationID)) {
                $cond_LocationID = " AND a.LocationID IN (" . implode(",", (array) $LocationID) . ") ";
            }
            
            if (trim($BuildingID) != '') {
                $cond_BuildingID = " AND c.BuildingID = '" . $BuildingID . "' ";
            }

            if ($includeUnbookable) {
                $unbookableLocationIDAry = $this->getUnbookableLocation();
                if (count($unbookableLocationIDAry)) {
                    $cond_Bookable = " AND (a.AllowBooking = 1 OR (a.AllowBooking = 0 AND a.LocationID IN (".implode(",",(array)$unbookableLocationIDAry)."))) ";
                }
                else {
                    $cond_Bookable = " AND a.AllowBooking = 1 ";
                }
            }
            else {
                $cond_Bookable = " AND a.AllowBooking = 1 ";
            }

            $sql = "
					SELECT 
						a.LocationID, a.NameChi AS RoomNameCH, a.NameEng AS RoomNameEN, 
						b.LocationLevelID, b.NameChi AS FloorNameCH, b.NameEng AS FloorNameEN, 
						c.BuildingID, c.NameChi AS BldgNameCH, c.NameEng AS BldgNameEN,
						CONCAT(c.NameEng, ' > ', b.NameEng, ' > ', a.NameEng) FullNameEN,
						CONCAT(c.NameChi, ' > ', b.NameChi, ' > ', a.NameChi) FullNameCH
					FROM
						INVENTORY_LOCATION a
						INNER JOIN INVENTORY_LOCATION_LEVEL b ON a.LocationLevelID = b.LocationLevelID 
						INNER JOIN INVENTORY_LOCATION_BUILDING c ON b.BuildingID = c.BuildingID
					WHERE
						1
						$cond_LocationID
						$cond_BuildingID
						AND	a.RecordStatus = 1
						$cond_Bookable
						AND b.RecordStatus = 1
						AND c.RecordStatus = 1
					Order By
						c.DisplayOrder, b.DisplayOrder, a.DisplayOrder ";
            return $this->returnArray($sql);
        }
        
        // function getAllAvailablePeriod($SinceDate='', $UntilDate='', $DateArr='')
        // {
        // if(!empty($SinceDate))
        // $Cond .= " AND RecordDate >= '$SinceDate' ";
        //
        // if(!empty($UntilDate))
        // $Cond .= " AND RecordDate <= '$UntilDate' ";
        //
        // if (is_array($DateArr) && count($DateArr)>0)
        // {
        // $StartDate = $DateArr[0];
        // $EndDate = $DateArr[count($DateArr)-1];
        //
        // $Cond .= " AND RecordDate Between '$StartDate' And '$EndDate' ";
        // }
        //
        // $sql = "
        // SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
        // WHERE
        // 1 $Cond
        // Order By a.RecordDate, a.StartTime, a.EndTime
        // ";
        // $result = $this->returnArray($sql);
        //
        // $returnAry = BuildMultiKeyAssoc($result,array("FacilityType","FacilityID","PeriodID"));
        //
        // return $returnAry;
        // }
        //
        // function getAllAvailablePeriodByFacility($FacilityType, $FacilityID, $SinceDate='', $UntilDate='', $DateArr='')
        // {
        // if(!empty($SinceDate))
        // $Cond .= " AND RecordDate >= '$SinceDate' ";
        //
        // if(!empty($UntilDate))
        // $Cond .= " AND RecordDate <= '$UntilDate' ";
        //
        // if (is_array($DateArr) && count($DateArr)>0)
        // {
        // $StartDate = $DateArr[0];
        // $EndDate = $DateArr[count($DateArr)-1];
        //
        // $Cond .= " AND RecordDate Between '$StartDate' And '$EndDate' ";
        // }
        //
        // if($FacilityType == 1){
        // $facility_cond = " AND ((RelatedItemID = 0 AND RelatedSubLocationID = $FacilityID) OR (RelatedItemID = 0 AND RelatedSubLocationID = 0)) ";
        // }else{
        // $facility_cond = " AND ((RelatedItemID = $FacilityID AND RelatedSubLocationID = 0) OR (RelatedItemID = 0 AND RelatedSubLocationID = 0)) ";
        // }
        //
        // $sql = "
        // SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
        // WHERE
        // 1 $facility_cond $Cond
        // Order By a.RecordDate, a.StartTime, a.EndTime
        // ";
        // $result = $this->returnArray($sql);
        //
        // $returnAry = BuildMultiKeyAssoc($result,array("FacilityType","FacilityID","PeriodID"));
        // return $returnAry;
        // }
        //
        // function getAllAvailablePeriodByFacility_MultiItem($FacilityType, $FacilityID, $SinceDate='', $UntilDate='', $DateArr='')
        // {
        // if(!empty($SinceDate))
        // $Cond .= " AND RecordDate >= '$SinceDate' ";
        //
        // if(!empty($UntilDate))
        // $Cond .= " AND RecordDate <= '$UntilDate' ";
        //
        // if (is_array($DateArr) && count($DateArr)>0)
        // {
        // $StartDate = $DateArr[0];
        // $EndDate = $DateArr[count($DateArr)-1];
        //
        // $Cond .= " AND RecordDate Between '$StartDate' And '$EndDate' ";
        // }
        // ### Get the Specify Booking Period ###
        // for($i=0; $i<sizeof($FacilityID); $i++){
        // if($facility_cond != "")
        // $facility_cond .= " OR ";
        // $facility_cond .= " (RelatedItemID = $FacilityID[$i] AND RelatedSubLocationID = 0) ";
        // }
        //
        // $sql = "SELECT RecordDate, StartTime, EndTime, COUNT(*) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $facility_cond $Cond GROUP BY RecordDate, StartTime, EndTime";
        // $arrSpecificBookingPeriod = $this->returnArray($sql,4);
        //
        // for($i=0; $i<sizeof($arrSpecificBookingPeriod); $i++){
        // list($record_date, $start_time, $end_time, $num_of_facility) = $arrSpecificBookingPeriod[$i];
        // if($num_of_facility == sizeof($FacilityID)){
        // $arrAvailableSpecificPeriod[] = array($record_date,$start_time,$end_time);
        //
        // if($temp_cond1 != "")
        // $temp_cond1 .= ",";
        // if($temp_cond2 != "")
        // $temp_cond2 .= ",";
        // if($temp_cond3 != "")
        // $temp_cond3 .= ",";
        //
        // $temp_cond1 .= "'".$record_date."'";
        // $temp_cond2 .= "'".$start_time."'";
        // $temp_cond3 .= "'".$end_time."'";
        // }
        // }
        //
        // $normal_sql = "SELECT PeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedItemID = 0 AND RelatedSubLocationID = 0) $Cond AND RecordDate NOT IN (SELECT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $facility_cond $Cond GROUP BY RecordDate, StartTime, EndTime) GROUP BY RecordDate, StartTime, EndTime";
        // if($temp_cond1 != "" && $temp_cond2 != "" && $temp_cond3 != ""){
        // $temp_cond1 = " RecordDate IN ($temp_cond1) ";
        // $temp_cond2 = " StartTime IN ($temp_cond2) ";
        // $temp_cond3 = " EndTime IN ($temp_cond3) ";
        //
        // $temp_cond = " AND (".$temp_cond1." AND ".$temp_cond2." AND ".$temp_cond3.")";
        // $specific_sql = "SELECT PeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE ($facility_cond) $temp_cond $Cond";
        // }
        // if($specific_sql != "")
        // $sql = "(".$normal_sql.") UNION (".$specific_sql.")";
        // else
        // $sql = "(".$normal_sql.")";
        //
        // $arrResult = $this->returnVector($sql);
        //
        // $targetPeriodID = implode(",",$arrResult);
        //
        // $final_sql = " SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
        // WHERE
        // PeriodID in ($targetPeriodID)
        // Order By RecordDate, StartTime, EndTime
        // ";
        // $result = $this->returnArray($final_sql);
        //
        // $returnAry = BuildMultiKeyAssoc($result,array("FacilityType","FacilityID","PeriodID"));
        // return $returnAry;
        // }
        
        // function getAllAvailablePeriodByFacility_MultiItem($FacilityType, $FacilityID, $SinceDate='', $UntilDate='', $DateArr='')
        // {
        // if(!empty($SinceDate))
        // $Cond .= " AND RecordDate >= '$SinceDate' ";
        //
        // if(!empty($UntilDate))
        // $Cond .= " AND RecordDate <= '$UntilDate' ";
        //
        // if (is_array($DateArr) && count($DateArr)>0)
        // {
        // $StartDate = $DateArr[0];
        // $EndDate = $DateArr[count($DateArr)-1];
        //
        // $Cond .= " AND RecordDate Between '$StartDate' And '$EndDate' ";
        // }
        // ### Get the Specify Booking Period ###
        // // for($i=0; $i<sizeof($FacilityID); $i++){
        // // if($facility_cond != "")
        // // $facility_cond .= " OR ";
        // // $facility_cond .= " (RelatedItemID = $FacilityID[$i] AND RelatedSubLocationID = 0) ";
        // // }
        // $facility_cond = " (RelatedItemID IN (".implode(",",(array)$FacilityID).") AND RelatedSubLocationID = 0) ";
        //
        // // $sql = "SELECT RecordDate, StartTime, EndTime, COUNT(*) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $facility_cond $Cond GROUP BY RecordDate, StartTime, EndTime";
        // $sql = "SELECT RecordDate, StartTime, EndTime, RelatedItemID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $facility_cond $Cond ORDER BY RecordDate, StartTime, EndTime";
        // $arrSpecificBookingPeriod = $this->returnArray($sql,4);
        //
        // $PeriodRangeAssoc = BuildMultiKeyAssoc($arrSpecificBookingPeriod, array("RelatedItemID"),array("StartTime","EndTime","RecordDate"),'',1);
        // $IntersectRangeArr = $this->Facility_Time_Range_Intersect($PeriodRangeAssoc);
        // //
        // for($i=0; $i<sizeof($arrSpecificBookingPeriod); $i++){
        // list($record_date, $start_time, $end_time, $num_of_facility) = $arrSpecificBookingPeriod[$i];
        // if($num_of_facility == sizeof($FacilityID)){
        // $arrAvailableSpecificPeriod[] = array($record_date,$start_time,$end_time);
        //
        // if($temp_cond1 != "")
        // $temp_cond1 .= ",";
        // if($temp_cond2 != "")
        // $temp_cond2 .= ",";
        // if($temp_cond3 != "")
        // $temp_cond3 .= ",";
        //
        // $temp_cond1 .= "'".$record_date."'";
        // $temp_cond2 .= "'".$start_time."'";
        // $temp_cond3 .= "'".$end_time."'";
        // }
        // }
        //
        // $normal_sql = "SELECT PeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedItemID = 0 AND RelatedSubLocationID = 0) $Cond AND RecordDate NOT IN (SELECT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $facility_cond $Cond GROUP BY RecordDate, StartTime, EndTime) GROUP BY RecordDate, StartTime, EndTime";
        // if($temp_cond1 != "" && $temp_cond2 != "" && $temp_cond3 != ""){
        // $temp_cond1 = " RecordDate IN ($temp_cond1) ";
        // $temp_cond2 = " StartTime IN ($temp_cond2) ";
        // $temp_cond3 = " EndTime IN ($temp_cond3) ";
        //
        // $temp_cond = " AND (".$temp_cond1." AND ".$temp_cond2." AND ".$temp_cond3.")";
        // $specific_sql = "SELECT PeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE ($facility_cond) $temp_cond $Cond";
        // }
        // if($specific_sql != "")
        // $sql = "(".$normal_sql.") UNION (".$specific_sql.")";
        // else
        // $sql = "(".$normal_sql.")";
        //
        // $arrResult = $this->returnVector($sql);
        //
        // $targetPeriodID = implode(",",$arrResult);
        //
        // $final_sql = " SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
        // WHERE
        // PeriodID in ($targetPeriodID)
        // Order By RecordDate, StartTime, EndTime
        // ";
        // $result = $this->returnArray($final_sql);
        //
        // $returnAry = BuildMultiKeyAssoc($result,array("FacilityType","FacilityID","PeriodID"));
        // return $returnAry;
        // }
        
        // function getAllAvailablePeriod_NEW($FacilityType, $FacilityID, $SinceDate='', $UntilDate='', $DateArr='')
        // {
        //
        // if(!empty($SinceDate))
        // $Cond .= " AND RecordDate >= '$SinceDate' ";
        //
        // if(!empty($UntilDate))
        // $Cond .= " AND RecordDate <= '$UntilDate' ";
        //
        // if (is_array($DateArr) && count($DateArr)>0)
        // {
        // $StartDate = $DateArr[0];
        // $EndDate = $DateArr[count($DateArr)-1];
        //
        // $Cond .= " AND RecordDate Between '$StartDate' And '$EndDate' ";
        // }
        //
        // if($FacilityType == 1){
        // $facility_cond = " RelatedItemID = 0 AND RelatedSubLocationID = $FacilityID ";
        // }else{
        // $facility_cond = " RelatedItemID = $FacilityID AND RelatedSubLocationID = 0 ";
        // }
        // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $facility_cond $Cond";
        //
        // $arrSpecifyPeriodExist = $this->returnVector($sql);
        //
        // if($arrSpecifyPeriodExist[0] > 0){
        // $sql = "
        // SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
        // WHERE
        // 1 AND $facility_cond $Cond
        // Order By a.RecordDate, a.StartTime, a.EndTime
        // ";
        // }else{
        // $sql = "
        // SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
        // WHERE
        // 1 AND RelatedItemID = 0 AND RelatedSubLocationID = 0 $Cond
        // Order By a.RecordDate, a.StartTime, a.EndTime
        // ";
        // }
        // $result = $this->returnArray($sql);
        //
        // $returnAry = BuildMultiKeyAssoc($result,array("FacilityType","FacilityID","PeriodID"));
        //
        // return $returnAry;
        // }
        //
        // function getAllAvailablePeriod_NEW2($FacilityType, $FacilityID, $SinceDate='', $UntilDate='', $DateArr='')
        // {
        // if(!empty($SinceDate))
        // $Cond .= " AND RecordDate >= '$SinceDate' ";
        //
        // if(!empty($UntilDate))
        // $Cond .= " AND RecordDate <= '$UntilDate' ";
        //
        // if (is_array($DateArr) && count($DateArr)>0)
        // {
        // $StartDate = $DateArr[0];
        // $EndDate = $DateArr[count($DateArr)-1];
        //
        // $Cond .= " AND RecordDate Between '$StartDate' And '$EndDate' ";
        // }
        //
        // if($FacilityType == 1){
        // $sql_date_with_special_period = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedItemID = 0 AND RelatedSubLocationID = $FacilityID) $Cond";
        // }else{
        // $sql_date_with_special_period = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedItemID = $FacilityID AND RelatedSubLocationID = 0) $Cond";
        // }
        // $arrTempSpecialPeriod = $this->returnVector($sql_date_with_special_period);
        // $arrSpecialPeriod = array();
        // for($i=0; $i<sizeof($arrTempSpecialPeriod); $i++){
        // $arrSpecialPeriod[] = "'".$arrTempSpecialPeriod[$i]."'";
        // }
        // $targetSpecialPeriod = implode(",",$arrSpecialPeriod);
        // if($targetSpecialPeriod != "")
        // $sql_date_with_normal_period = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedItemID = 0 AND RelatedSubLocationID = 0) AND RecordDate NOT IN ($targetSpecialPeriod) $Cond";
        // else
        // $sql_date_with_normal_period = "SELECT DISTINCT RecordDate FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE (RelatedItemID = 0 AND RelatedSubLocationID = 0) $Cond";
        //
        // $arrTempNormalPeriod = $this->returnVector($sql_date_with_special_period);
        // $arrNormalPeriod = array();
        // for($i=0; $i<sizeof($arrTempNormalPeriod); $i++){
        // $arrNormalPeriod[] = "'".$arrTempNormalPeriod[$i]."'";
        // }
        // $duplicate_date_cond = "";
        // if(sizeof($arrSpecialPeriod) == 0){
        // $targetNormalPeriod = implode(",",$arrNormalPeriod);
        // }else{
        // $arrDuplicatePeriod = array_intersect($arrSpecialPeriod, $arrNormalPeriod);
        // $duplicate_date_string = implode(",", $arrDuplicatePeriod);
        // $duplicate_date_cond = " AND (a.RecordDate NOT IN ($duplicate_date_string)) ";
        // //$targetNormalPeriod = implode(",",$final_arrNormalPeriod);
        // }
        //
        // if($FacilityType == 1){
        // if($targetSpecialPeriod != ""){
        // $sql1 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RecordDate IN (".$targetSpecialPeriod.") ) AND (a.RelatedItemID = 0 AND a.RelatedSubLocationID = '$FacilityID')
        // ORDER By
        // a.RecordDate ASC";
        // }else{
        // $sql1 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RelatedItemID = 0 AND a.RelatedSubLocationID = '$FacilityID')
        // ORDER By
        // a.RecordDate ASC";
        // }
        //
        // if($targetNormalPeriod != ""){
        // $sql2 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RecordDate IN (".$targetNormalPeriod.") ) $targetNormalPeriod AND (a.RelatedItemID = 0 AND a.RelatedSubLocationID = 0)
        // ORDER By
        // a.RecordDate ASC";
        // }else{
        // $sql2 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RelatedItemID = 0 AND a.RelatedSubLocationID = 0) $duplicate_date_cond
        // ORDER By
        // a.RecordDate ASC";
        // }
        // }else{
        // if($targetSpecialPeriod != ""){
        // $sql1 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RecordDate IN (".$targetSpecialPeriod.") ) AND (a.RelatedItemID = '$FacilityID' AND a.RelatedSubLocationID = 0)
        // ORDER By
        // a.RecordDate ASC";
        // }else{
        // $sql1 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RelatedItemID = '$FacilityID' AND a.RelatedSubLocationID = 0)
        // ORDER By
        // a.RecordDate ASC";
        // }
        //
        // if($targetNormalPeriod != ""){
        // $sql2 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RecordDate IN (".$targetNormalPeriod.") ) $targetNormalPeriod AND (a.RelatedItemID = 0 AND a.RelatedSubLocationID = 0)
        // ORDER By
        // a.RecordDate ASC";
        // }else{
        // $sql2 = "SELECT
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
        // IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,
        // a.*
        // FROM
        // INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD AS a
        // WHERE
        // (a.RelatedItemID = 0 AND a.RelatedSubLocationID = 0) $targetNormalPeriod
        // ORDER By
        // a.RecordDate ASC";
        // }
        //
        // }
        // $sql = "($sql1) UNION ($sql2) ORDER BY RecordDate, StartTime, EndTime";
        // $result = $this->returnArray($sql);
        //
        // $returnAry = BuildMultiKeyAssoc($result,array("FacilityType","FacilityID","PeriodID"));
        //
        // return $returnAry;
        // }
        function getAllBookableCategoryItem($Keyword = '', $ItemID = '', $AllowBookingIndependence = false, $includeUnbookable = false)
        {
            include_once ("libinventory.php");
            $linventory = new libinventory();

            if ($includeUnbookable) {
                $unbookableItemIDAry = $this->getUnbookableItem();
                if (count($unbookableItemIDAry)) {
                    $cond_Bookable = " AND (Item.AllowBooking = 1 OR (Item.AllowBooking = 0 AND Item.ItemID IN (".implode(",",(array)$unbookableItemIDAry)."))) ";
                }
                else {
                    $cond_Bookable = " AND Item.AllowBooking = 1 ";
                }
            }
            else {
                $cond_Bookable = " AND Item.AllowBooking = 1 ";
            }

            if ($Keyword) {
                $Keyword = addslashes(trim($Keyword));
                $Cond = " AND CONCAT(Item.ItemCode, ' - ', " . $linventory->getInventoryNameByLang("Item.") . ") LIKE '%$Keyword%' ";
            }
            
            if (! empty($ItemID))
                $cond_ItemID = " AND Item.ItemID IN (" . implode(",", (array) $ItemID) . ") ";
            if ($AllowBookingIndependence)
                $AllowBookingIndependence_sql = " AND Item.AllowBookingIndependence = 1 ";
            $sql = "
					SELECT
						 Cat.CategoryID, 
						" . $linventory->getInventoryNameByLang("Cat.") . " CategoryName,
						 SubCat.Category2ID, 
						" . $linventory->getInventoryNameByLang("SubCat.") . " Category2Name,
						Item.ItemID,  
						CONCAT(Item.ItemCode, ' - ', " . $linventory->getInventoryNameByLang("Item.") . ") ItemName,
						" . $linventory->getInventoryNameByLang("Item.") . " RawItemName,
						Item.Attachment,
						Item.eInventoryItemID
					FROM 
						INVENTORY_CATEGORY AS Cat 
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS SubCat ON (Cat.CategoryID = SubCat.CategoryID) 
						INNER JOIN INTRANET_EBOOKING_ITEM  AS Item ON (Cat.CategoryID = Item.CategoryID AND SubCat.Category2ID = Item.Category2ID)
						INNER JOIN INVENTORY_LOCATION as room On (Item.LocationID = room.LocationID)
						INNER JOIN INVENTORY_LOCATION_LEVEL as floor On (room.LocationLevelID = floor.LocationLevelID)
						INNER JOIN INVENTORY_LOCATION_BUILDING as building On (floor.BuildingID = building.BuildingID)
					WHERE
						Item.RecordStatus = 1 
						$cond_Bookable 
						$Cond
						$cond_ItemID
						$AllowBookingIndependence_sql
					ORDER BY 
						Item.ItemCode	
				";
            
            $result = $this->returnArray($sql);
            return $result;
        }

        function getAllBookableCategoryItemAssoc($Keyword = '', $AllowBookingIndependence = false)
        {
            $result = $this->getAllBookableCategoryItem($Keyword, '', $AllowBookingIndependence);
            
            return BuildMultiKeyAssoc($result, array(
                "CategoryID",
                "Category2ID",
                "ItemID"
            ));
        }

        function getFacilityBookingAvailableDateRange($FacilityType, $FacilityID, $ParUserID, $From_eAdmin = '')
        {
            return $this->getFacilityBookingAvailableDateRange_MultiItem($FacilityType, $FacilityID, $ParUserID, $From_eAdmin);
        }

        function getLibUserObj($ParUserID)
        {
            global $intranet_root;
            
            if (! isset($this->libUserAry[$ParUserID])) {
                include_once ($intranet_root . "/includes/libuser.php");
                $this->libUserAry[$ParUserID] = new libuser($ParUserID);
            }
            
            return $this->libUserAry[$ParUserID];
        }

        function getFacilityBookingAvailableDateRange_MultiItem($FacilityType, $FacilityID, $ParUserID, $From_eAdmin = '')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            if ($From_eAdmin) {
                $SinceDate = date("Y-m-d");
                $UntilDate = '';
                return array(
                    $SinceDate,
                    $UntilDate
                );
            }
            
            // include_once("libuser.php");
            // $libuser = new libuser($ParUserID);
            $libuser = $this->getLibUserObj($ParUserID);
            $uType = $libuser->RecordType;
            $IsTeaching = $libuser->teaching;
            $targetFacilityID = implode(",", (array) $FacilityID);
            
            if ($uType == 1) {
                $YearID = 0;
            } else 
                if ($uType == 2) {
                    $IsTeaching = 0;
                    $YearIDArr = $libuser->Get_User_Studying_Form();
                    $YearID = Get_Array_By_Key($YearIDArr, "YearID");
                } else 
                    if ($uType == 3) {
                        $IsTeaching = 0;
                        $YearIDArr = $libuser->Get_Children_Studying_Form();
                        $YearID = Get_Array_By_Key($YearIDArr, "YearID");
                    }
            
            if ($FacilityType == 2) // item
{
                $sql = "
						Select 
							MAX(a.BookingDayBeforehand) 
						FROM 
							INTRANET_EBOOKING_ITEM a   
						WHERE
							a.ItemID IN ($targetFacilityID)
					";
                $SinceDate = $this->returnVector($sql);
                
                $sql = "
						Select 
							b.DaysBeforeUse, b.DisableBooking
						FROM 
							INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE a   
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE b ON a.RuleID = b.RuleID
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER c ON b.RuleID = c.RuleID
						WHERE
							a.ItemID IN ($targetFacilityID)
							AND UserType = '$uType'
							AND YearID IN (" . implode(",", (array) $YearID) . ")
					";
                $UntilDate_temp = $this->returnResultSet($sql);
                
                $sql = "
						SELECT   
							ubr.DaysBeforeUse, ubr.DisableBooking
						FROM   
							INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE  fbr  
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE ubr ON fbr.RuleID = ubr.RuleID 
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER cgu ON ubr.RuleID = cgu.RuleID  
						WHERE  
							fbr.ItemID IN ($targetFacilityID)  
							AND cgu.UserID = '$ParUserID'
					";
                // $UntilDate = array_merge((array)$UntilDate, (array)$this->returnVector($sql));
                $UntilDateCustom_temp = $this->returnResultSet($sql);
            } else 
                if ($FacilityType == 1) // location
{
                    $sql = "
						SELECT
							MAX(b.DaysBeforeUse)
						FROM 
							INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION a 
							INNER JOIN INTRANET_EBOOKING_LOCATION_BOOKING_RULE b ON a.BookingRuleID= b.RuleID
						WHERE
							a.LocationID IN ($targetFacilityID)
					";
                    $SinceDate = $this->returnVector($sql);
                    
                    $sql = "
						SELECT
							b.DaysBeforeUse, b.DisableBooking
						FROM 
							INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION a 
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE b ON a.UserBookingRuleID = b.RuleID
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER c ON b.RuleID = c.RuleID
						WHERE
							a.LocationID IN ($targetFacilityID)
							AND UserType = '$uType'
							AND YearID IN (" . implode(",", (array) $YearID) . ")
					";
                    $UntilDate_temp = $this->returnResultSet($sql);
                    
                    $sql = "
						SELECT   
							ubr.DaysBeforeUse, ubr.DisableBooking
						FROM   
							INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION fbr  
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE ubr ON fbr.UserBookingRuleID = ubr.RuleID 
							INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER cgu ON ubr.RuleID = cgu.RuleID  
						WHERE  
							fbr.LocationID IN ($targetFacilityID)  
							AND cgu.UserID = '$ParUserID'
					";
                    // $UntilDate = array_merge((array)$UntilDate, (array)$this->returnVector($sql));
                    // $UntilDateCustom_temp = array_values(array_remove_empty($this->returnVector($sql)));
                    $UntilDateCustom_temp = $this->returnResultSet($sql);
                    // $UntilDate = array_merge($UntilDate, $UntilDateCustom);
                }
            
            // UntilDate Handling Start
            foreach ((array) $UntilDate_temp as $_UntilDate_temp) {
                if (! $_UntilDate_temp['DisableBooking']) {
                    $UntilDate_DisableBooking = $_UntilDate_temp['DisableBooking'];
                    if ($_UntilDate_temp['DaysBeforeUse'] === '0') {
                        $UntilDate = '0';
                    } elseif ($UntilDate === '0') {
                        // do nth
                    } elseif ($_UntilDate_temp['DaysBeforeUse'] > $UntilDate) {
                        $UntilDate = $_UntilDate_temp['DaysBeforeUse'];
                    }
                } else {
                    if (isset($UntilDate_DisableBooking)) {
                        // Do nth
                    } else {
                        $UntilDate_DisableBooking = $_UntilDate_temp['DisableBooking'];
                    }
                }
            }
            foreach ((array) $UntilDateCustom_temp as $_UntilDateCustom_temp) {
                
                if (! $_UntilDateCustom_temp['DisableBooking']) {
                    $UntilDate_DisableBooking = $_UntilDateCustom_temp['DisableBooking'];
                    if ($_UntilDateCustom_temp['DaysBeforeUse'] === '0') {
                        $UntilDate = '0';
                    } elseif ($UntilDate === '0') {
                        // do nth
                    } elseif ($_UntilDateCustom_temp['DaysBeforeUse'] > $UntilDate) {
                        $UntilDate = $_UntilDateCustom_temp['DaysBeforeUse'];
                    }
                } else {
                    if (isset($UntilDate_DisableBooking)) {
                        // Do nth
                    } else {
                        $UntilDate_DisableBooking = $_UntilDateCustom_temp['DisableBooking'];
                    }
                }
            }
            // UntilDate Handling END
            
            $time = time();
            
            $SinceDate = ! empty($SinceDate[0]) ? (date("Y-m-d", $time + ($SinceDate[0] * 24 * 60 * 60))) : (date("Y-m-d"));
            
            // 2012-1207-1409-26054: change to get the far-est
            // # $UntilDate may have more than 1 record, get the nearest
            // $UntilDate = array_unique($UntilDate);
            // if(count($UntilDate)==1&&$UntilDate[0]==0 ||count($UntilDate)==0)
            // {
            // $UntilDate = ''; //no limit
            // }
            // else
            // {
            // $UntilDate = array_diff((array)$UntilDate,array(0), array('')); // take away all 0 (unlimited)
            // if(count($UntilDate)!=0) {
            // $UntilDate = date("Y-m-d",$time+(min($UntilDate)*24*60*60)); // get the nearest date
            // }
            // }
            
            // 2013-0819-0942-09073
            // if (in_array(0, $UntilDate) || in_array('', $UntilDate) || count($UntilDate)==0) {
            // 2013-1121-1215-13073
            // if (in_array(0, $UntilDate, true) || count($UntilDate)==0) {
            // Omas 20150805 E81757
            // if (in_array(0, $UntilDate, true) || in_array('0', $UntilDate, true) || count((array)$UntilDate)==0 || (is_array($UntilDate) && $UntilDate[0]===null)) {
            if ($UntilDate_DisableBooking) {
                // if disable booking set untildate before today to disable all the day
                $UntilDate = date("Y-m-d", $time - ($SinceDate[0] * 24 * 60 * 60));
            } else {
                $haveDayLimit = 0;
                foreach ((array) $UntilDate as $day) {
                    if ($day > 0) {
                        $haveDayLimit = 1;
                    } else 
                        if ($day === '0') {
                            // 0 = No Limit & follow largest permission therefore break here
                            $haveDayLimit = 0;
                            break;
                        }
                }
                if ($haveDayLimit == 0) {
                    $UntilDate = ''; // no limit
                } else {
                    $UntilDate = array_diff((array) $UntilDate, array(
                        0
                    ), array(
                        ''
                    )); // take away all 0 (unlimited)
                    if (count($UntilDate) != 0) {
                        $UntilDate = date("Y-m-d", $time + (max($UntilDate) * 24 * 60 * 60)); // get the nearest date
                    }
                }
            }
            
            $result = array(
                $SinceDate,
                $UntilDate
            );
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_Item_Info_By_ItemID($ItemID = '', $RecordStatus = '1')
        {
            if ($ItemID != '' || $ItemID === 0) {
                $conds = " AND ItemID IN ($ItemID) ";
            }
            $sql = "
							SELECT 
								*
							FROM
								INTRANET_EBOOKING_ITEM
							WHERE
								1 AND RecordStatus = '$RecordStatus'  $conds
						";
            $result = $this->returnArray($sql);
            return $result;
        }

        function Get_Item_Info_By_Item_Code($ItemCode = '', $RecordStatus = '1')
        {
            if ($ItemCode != '') {
                $conds = " AND ItemCode = '$ItemCode' ";
            }
            $sql = "
							SELECT 
								*
							FROM
								INTRANET_EBOOKING_ITEM
							WHERE
								1 AND RecordStatus = '$RecordStatus'  $conds
						";
            $result = $this->returnArray($sql);
            return $result;
        }

        function Get_Booking_Record_Display_Detail($Keyword = '', $FacilityType = '', $BookingStatusList = '', $BookingDateStatus = '', $SelectedStartDate = '', $SelectedEndDate = '', $ManagementGroup = '', $TargetUserID = '', $SortField = '')
        {
            global $PATH_WRT_ROOT, $UserID, $Lang;
            include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
            
            $linventory = new libinventory();
            
            if ($BookingDateStatus != "") {
                $curr_date = date("Y-m-d");
                if ($BookingDateStatus == 3) {} else 
                    if ($BookingDateStatus == 2) {
                        $SelectedStartDate = $curr_date;
                        $SelectedEndDate = '';
                    } else {
                        $SelectedStartDate = '';
                        $SelectedEndDate = $curr_date;
                    }
            }
            
            if ($Keyword != "") {
                // $Keyword = addslashes(trim($Keyword));
                $Keyword = trim($Keyword);
            }
            
            if ($TargetUserID === '') {
                $TargetUserID = $UserID;
            } else 
                if ($TargetUserID == - 1) {
                    $TargetUserID = '';
                }
            
            $arrTempResult = $this->Get_All_Facility_Booking_Record('', $BookingStatusList, $SelectedStartDate, $SelectedEndDate, $ManagementGroup, $Keyword, '', $FacilityType, $TargetUserID, '', $SortField);
            
            if (sizeof($arrTempResult) > 0) {
                for ($i = 0; $i < sizeof($arrTempResult); $i ++) {
                    list ($booking_id, $period_id, $booking_remark, $booking_date_string, $booking_start_time, $booking_end_time, $requested_by, $request_date, $responsible_ppl, $room_id, $room_name, $room_PIC, $room_booking_process_date, $room_booking_status, $item_id, $item_name, $item_PIC, $item_booking_process_date, $item_booking_status, $is_reserve, $room_check_in_out_remarks, $item_check_in_out_remarks, $room_PICID, $item_PICID, $room_reject_reason, $item_reject_reason, $Attachment, $room_CurentStatus, $item_CurentStatus, $room_CheckOutTime, $item_CheckOutTime, $RelatedTo, $Event) = $arrTempResult[$i];
                    
                    $weekday = date("D", strtotime($booking_date_string));
                    
                    $arrBookingID[] = $booking_id;
                    $arrBookingDetails[$booking_id]['Date'] = $booking_date_string . ' (' . $weekday . ')';
                    $arrBookingDetails[$booking_id]['RelatedPeriod'] = $period_id;
                    $arrBookingDetails[$booking_id]['Remark'] = $booking_remark;
                    $arrBookingDetails[$booking_id]['StartTime'] = $booking_start_time;
                    $arrBookingDetails[$booking_id]['EndTime'] = $booking_end_time;
                    $arrBookingDetails[$booking_id]['RequestedBy'] = $requested_by;
                    $arrBookingDetails[$booking_id]['RequestedDate'] = $request_date;
                    $arrBookingDetails[$booking_id]['IsReserve'] = $is_reserve;
                    $arrBookingDetails[$booking_id]['Attachment'] = $Attachment;
                    $arrBookingDetails[$booking_id]['RelatedTo'] = $RelatedTo;
                    $arrBookingDetails[$booking_id]['Event'] = $Event;
                    
                    $curr_date = date("Y-m-d");
                    // $booking_day_before = floor((strtotime($curr_date)-strtotime($request_date))/86400);
                    // if($booking_day_before > 7) {
                    $booking_day_before = $request_date;
                    // } else {
                    // $booking_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
                    // }
                    $arrBookingDetails[$booking_id]['RequestedDayBefore'] = $booking_day_before;
                    
                    $arrBookingDetails[$booking_id]['ResponsiblePerson'] = $responsible_ppl;
                    if ($room_id != "") {
                        $arrBookingDetails[$booking_id]['RoomBooking'] = 1;
                        $arrBookingDetails[$booking_id]['RelatedRoom'][] = $room_id;
                        $arrBookingDetails[$booking_id][$room_id]['RoomName'] = $room_name;
                        $arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC'] = $room_PIC;
                        $arrBookingDetails[$booking_id][$room_id]['RoomProcessDate'] = $room_booking_process_date;
                        $arrBookingDetails[$booking_id][$room_id]['RejectReason'] = $room_reject_reason;
                        
                        $curr_date = date("Y-m-d");
                        // $room_booking_process_day_before = floor((strtotime($curr_date)-strtotime($room_booking_process_date))/86400);
                        // if($room_booking_process_day_before > 7) {
                        $room_booking_process_day_before = $room_booking_process_date;
                        // } else {
                        // $room_booking_process_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
                        // }
                        
                        $arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore'] = $room_booking_process_day_before;
                        
                        $arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
                    } else {
                        $arrBookingDetails[$booking_id]['RoomBooking'] = 0;
                        $arrBookingDetails[$booking_id]['RelatedRoom'][] = array();
                        $arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
                    }
                    
                    if ($item_id != "") {
                        $arrBookingDetails[$booking_id]['ItemBooking'] = 1;
                        $arrBookingDetails[$booking_id]['RelatedItem'][] = $item_id;
                        $arrBookingDetails[$booking_id][$item_id]['ItemName'] = $item_name;
                        $arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC'] = $item_PIC;
                        $arrBookingDetails[$booking_id][$item_id]['ItemProcessDate'] = $item_booking_process_date;
                        $arrBookingDetails[$booking_id][$item_id]['RejectReason'] = $item_reject_reason;
                        
                        $curr_date = date("Y-m-d");
                        // $item_booking_process_day_before = floor((strtotime($curr_date)-strtotime($item_booking_process_date))/86400);
                        // if($room_booking_process_day_before > 7) {
                        $item_booking_process_day_before = $item_booking_process_date;
                        // } else {
                        // $item_booking_process_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
                        // }
                        $arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore'] = $item_booking_process_day_before;
                        
                        $arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
                    } else {
                        $arrBookingDetails[$booking_id]['ItemBooking'] = 0;
                        $arrBookingDetails[$booking_id]['RelatedItem'][] = array();
                        $arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
                    }
                }
                $arrBookingID = array_unique($arrBookingID);
            }
            
            return array(
                $arrBookingID,
                $arrBookingDetails
            );
        }

        function Get_User_Related_Mgmt_Group_Array($uid)
        {
            if ($this->IS_ADMIN_USER()) {
                $sql = "SELECT GroupID, GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP ORDER BY GroupName";
            } else {
                $sql = "SELECT mgmt_group.GroupID, mgmt_group.GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP as mgmt_group INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER as member ON (mgmt_group.GroupID = member.GroupID) WHERE member.UserID = '$uid' ORDER BY mgmt_group.GroupName";
            }
            $arrMgmtGroup = $this->returnArray($sql, 2);
            return $arrMgmtGroup;
        }

        function getAllManagementGroupInfo($groupID = '')
        {
            if ($groupID != '') {
                $cond = " AND  GroupID = '" . $groupID . "' ";
            }
            $sql = "Select * From INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE 1 $cond";
            return $this->returnArray($sql);
        }

        function getAllFollowupGroupInfo($groupID = '')
        {
            if ($groupID != '') {
                $cond = " AND  GroupID = '" . $groupID . "' ";
            }
            $sql = "Select * From INTRANET_EBOOKING_FOLLOWUP_GROUP WHERE 1 $cond";
            return $this->returnArray($sql);
        }

        function Check_All_Related_Booking_Request_Is_Processed($booking_id)
        {
            $sql = "SELECT COUNT(*),COUNT(IF(BookingStatus != 0,1,NULL))  FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id'";
            $TotalNumOfBookingResource = $this->returnArray($sql);
            if ($TotalNumOfBookingResource[0][0] == $TotalNumOfBookingResource[0][1]) {
                return true;
            }
            
            return false;
        }

        function SendEmailToFollowUpGroup_Merged($booking_id)
        {
            $this->Load_General_Setting();
            
            if (! $this->SettingArr['BookingApproveNotification']) { // if notification is disabled.
                return true;
            }
            
            global $PATH_WRT_ROOT, $Lang, $intranet_root, $sys_custom;
            include_once ($PATH_WRT_ROOT . 'includes/libwebmail.php');
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            
            // ## Get Target Recipients (Location)
            $sql = "SELECT member.UserID, room_details.BookingID FROM INTRANET_EBOOKING_BOOKING_DETAILS as room_details INNER JOIN INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION AS relation ON (room_details.FacilityID = relation.LocationID AND room_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "') INNER JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE room_details.BookingID IN( \"" . implode('","', $booking_id) . "\" ) ";
            $arrLocationFollowUp = $this->returnResultSet($sql);
            
            // ## Get Target Recipients (Item)
            $sql = "SELECT member.UserID, item_details.BookingID FROM INTRANET_EBOOKING_BOOKING_DETAILS as item_details INNER JOIN INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP AS relation ON (item_details.FacilityID = relation.ItemID AND item_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "') INNER JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE item_details.BookingID IN( \"" . implode('","', $booking_id) . "\" )";
            $arrItemFollowUp = $this->returnResultSet($sql);
            
            $targetFollowUpAry = (! empty($arrLocationFollowUp)) ? $arrLocationFollowUp : $arrItemFollowUp;
            foreach ($targetFollowUpAry as $UserFollowUpAry) {
                $_userId = $UserFollowUpAry['UserID'];
                $_bookingId = $UserFollowUpAry['BookingID'];
                
                $UserFollowUpAssoAry[$_bookingId][] = $_userId;
            }
            
            // # get Booking Detail
            $sql = "SELECT Date, StartTime, EndTime, RequestedBy, ResponsibleUID, Remark, RelatedTo, BookingID FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN( \"" . implode('","', $booking_id) . "\" )";
            $arrBookingDetails = $this->returnResultSet($sql);
            $RecipientUserIDArr = array();
            foreach ($arrBookingDetails as $BookingIDInfoAry) {
                $GroupByRelatedBookingIDInfoAry[$BookingIDInfoAry['RelatedTo']][$BookingIDInfoAry['BookingID']] = $BookingIDInfoAry;
                $RecipientUserIDArr[] = $BookingIDInfoAry['RequestedBy'];
                $RecipientUserIDArr[] = $BookingIDInfoAry['ResponsibleUID'];
            }
            
            // ## Get Recipients Name Array
            $RecipientUserIDArr = array_unique($RecipientUserIDArr);
            $sql = " SELECT UserID, EnglishName, ChineseName FROM INTRANET_USER WHERE UserID IN ( '" . implode("','", $RecipientUserIDArr) . "' ) ";
            $RecipientNameArr = $this->ReturnResultSet($sql);
            $RecipientNameAssoArr = BuildMultiKeyAssoc($RecipientNameArr, 'UserID');
            
            $MapFollowupUserBookingAssoAry = array();
            foreach ((array) $GroupByRelatedBookingIDInfoAry as $_relatedToBookingId => $_bookingAssoAry) {
                foreach ((array) $_bookingAssoAry as $__bookingId => $__bookingInfoAry) {
                    $__followUpUserIdAry = array_values(array_unique((array) $UserFollowUpAssoAry[$__bookingId]));
                    $__numOfFollowUpUser = count($__followUpUserIdAry);
                    
                    for ($i = 0; $i < $__numOfFollowUpUser; $i ++) {
                        $___userId = $__followUpUserIdAry[$i];
                        
                        if (isset($GroupByRelatedBookingIDInfoAry[$_relatedToBookingId])) {
                            $MapFollowupUserBookingAssoAry[$___userId][$_relatedToBookingId] = $GroupByRelatedBookingIDInfoAry[$_relatedToBookingId];
                        }
                    }
                }
            }
            
            // # get room result
            // Z117075 Filter the Room/ Item only BookingStatus is Approved
            $sql = "SELECT 
							room.BookingID,
							" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . " as Location,
							room.BookingStatus as room_BookingStatus
						FROM 
							INTRANET_EBOOKING_BOOKING_DETAILS as room 
							INNER JOIN INVENTORY_LOCATION as location ON (room.FacilityID = location.LocationID  AND room.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "')
							INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (floor.LocationLevelID = location.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING as building ON (floor.BuildingID = building.BuildingID)
						WHERE
							room.BookingID IN( \"" . implode('","', $booking_id) . "\" )
						AND
							room.BookingStatus = '1'
						";
            $arrRoomResult = $this->returnResultSet($sql);
            $arrRoomResultAssoAry = BuildMultiKeyAssoc($arrRoomResult, 'BookingID', array(
                'Location',
                'room_BookingStatus'
            ));
            
            // # get item result
            $sql = "SELECT
							item.BookingID,
							" . $linventory->getInventoryNameByLang("ebooking_item.") . " as ItemName
						FROM
							INTRANET_EBOOKING_BOOKING_DETAILS as item
							INNER JOIN INTRANET_EBOOKING_ITEM as ebooking_item ON (item.FacilityID = ebooking_item.ItemID AND item.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "')
						WHERE
							item.BookingID IN( \"" . implode('","', $booking_id) . "\" )
						AND
							item.BookingStatus = '1'
						";
            $arrItemResult = $this->returnResultSet($sql);
            foreach ($arrItemResult as $itemBookingInfoAry) {
                $arrItemResultAssoAry[$itemBookingInfoAry['BookingID']][] = $itemBookingInfoAry['ItemName'];
            }
            
            // Build Mail and send
            foreach ($MapFollowupUserBookingAssoAry as $_FollowUserID => $_UserFollowUpAry) {
                // Merged Among different related booking
                $_MergedMessage = "";
                
                foreach ($_UserFollowUpAry as $__RelatedBookingID => $__RelatedBookingAry) { // email content build in this loop
                                                                                           // Build Message (Start)
                                                                                           // Merged Among different date time on same related booking
                    $__Message = "";
                    // initialize
                    $__Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['PleaseFollowItem'] . ":<BR><BR>";
                    foreach ($__RelatedBookingAry as $___BookingInfoAry) {
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . ": " . $___BookingInfoAry['Date'] . "<BR>";
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . ": " . $___BookingInfoAry['StartTime'] . "<BR>";
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . ": " . $___BookingInfoAry['EndTime'] . "<BR>";
                        
                        $remark = $___BookingInfoAry['Remark'];
                        $remark = ($remark == '') ? $Lang['General']['EmptySymbol'] : $remark;
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['Remark'] . ": " . $remark . "<BR>";
                    
                        
                        // Room Details
                        $__Message .= '<BR>';
                        if (! empty($arrRoomResultAssoAry)) {
                            $__Message .= $Lang['eBooking']['Settings']['SystemProperty']['Room'] . ": <BR>";
                            $__Message .= $arrRoomResultAssoAry[$___BookingInfoAry['BookingID']]['Location'] . "<br><br>";
                        }
                        
                        // Item Details
                        if (isset($arrItemResultAssoAry[$___BookingInfoAry['BookingID']])) {
                            $__Message .= $Lang['eBooking']['General']['FieldTitle']['Item'] . ": <BR>";
                            foreach ($arrItemResultAssoAry[$___BookingInfoAry['BookingID']] as $item_name) {
                                $__Message .= $item_name . "<BR>";
                            }
                            $__Message .= "<BR>";
                        }
                        $__Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['WillBeUsedBy'] . ": " . $RecipientNameAssoArr[$___BookingInfoAry['ResponsibleUID']]['EnglishName'] . "<BR>";
                        $__Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['BookedBy'] . ": " . $RecipientNameAssoArr[$___BookingInfoAry['RequestedBy']]['EnglishName'] . "<BR><BR>";
                        
                        $__targetBookingStatus = $arrRoomResultAssoAry[$___BookingInfoAry['BookingID']]['room_BookingStatus'];
                        if (! empty($arrRoomResultAssoAry)) {
                            if ($__targetBookingStatus == 1) {
                                $__Message .= "<font color='green'>" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] . "</font><br>";
                            } else {
                                $__Message .= "<font color='red'>" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] . "</font><br>";
                            }
                        }
                        $__Message .= "<br>";
                    }
                    $__Message .= '<br><hr><br>';
                    // Build Message (End)
                    $_MergedMessage .= $__Message;
                }
                $toAry[] = $_FollowUserID;
                $Subject = $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['emailTitle'];
                
                $successAry[] = $lwebmail->sendModuleMail($toAry, $Subject, $_MergedMessage, 1, '', 'User', true);
                // $successAry[] = $lwebmail->sendModuleMail($toAry,$Subject,$_MergedMessage,1,0,true);
                
                unset($toAry);
            }
            
            return ! in_array(false, (array) $successAry);
        }

        function Email_Booking_Result_Merged($bookingIdAry, $requestNofify = false)
        {
            $this->Load_General_Setting();
            
            if (! $this->SettingArr['BookingNotification']) { // if notification is disabled.
                return true;
            }
            
            global $PATH_WRT_ROOT, $Lang, $intranet_root;
            include_once ($PATH_WRT_ROOT . 'includes/libwebmail.php');
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            $successAry = array();
            
            if ($requestNofify) {
                include_once ($PATH_WRT_ROOT . 'includes/libuser.php');
                $reservedBy = new libuser($_SESSION['UserID']);
                $reservedName = Get_Lang_Selection($reservedBy->ChineseName, $reservedBy->EnglishName);
                $reservedName = str_replace("<--Name-->", $reservedName, $Lang['eBooking']['eMailContentAry']['reservedBy']);
            }
            
            if (! is_array($bookingIdAry)) {
                $bookingIdAry = array(
                    $bookingIdAry
                );
            }
            
            // find all users involved the bookings
            $bookingInfoAry = $this->Get_All_Facility_Booking_Record($bookingIdAry, $BookingStatus = '', $StartDate = '', $EndDate = '', $ManagementGroup = '', $keyword = '', $CheckInOutRecord = '', $FacilityType = '', $ParUserID = '', $FacilityIDAry = '', $SortField = 1);
            
            // G115068
            foreach ((array) $bookingInfoAry as $_bookingInfoAry) {
                if (! isset($bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']])) {
                    $itemIndex = 1;
                    $bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']] = $_bookingInfoAry;
                }
                if ($_bookingInfoAry['ItemID'] > 0) {
                    $bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']]['ItemID_' . $itemIndex] = $_bookingInfoAry['ItemID'];
                    $bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']]['ItemBookingStatus_' . $itemIndex] = $_bookingInfoAry['item_BookingStatus'];
                    $bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']]['ItemRejectReason_' . $itemIndex] = $_bookingInfoAry['item_RejectReason'];
                    $bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']]['ItemName_' . $itemIndex] = $_bookingInfoAry['ItemName'];
                    $bookingInfoAssoAry[$_bookingInfoAry['BookingDetailRecordID']]['NumberOfItem'] = $itemIndex;
                    $itemIndex ++;
                }
            }
            // $bookingInfoAssoAry = BuildMultiKeyAssoc($bookingInfoAry, 'BookingDetailRecordID');
            $numOfBooking = count($bookingInfoAry);
            
            $requestedByUserIdAry = Get_Array_By_Key($bookingInfoAry, 'RequestedBy');
            $responsibleUserIdAry = Get_Array_By_Key($bookingInfoAry, 'ResponsibleUID');
            $bookingUserIdAry = array_values(array_unique(array_merge($requestedByUserIdAry, $responsibleUserIdAry)));
            $numOfUser = count($bookingUserIdAry);
            
            $emailSubject = $Lang['eBooking']['Mail']['FieldTitle']['ResultOfYourBookigRequest'];
            for ($i = 0; $i < $numOfUser; $i ++) {
                $_userId = $bookingUserIdAry[$i];
                
                // find related bookings of each users to send email
                $_userRelatedBookingDetailsIdAry = array();
                for ($j = 0; $j < $numOfBooking; $j ++) {
                    $__bookingDetailsId = $bookingInfoAry[$j]['BookingDetailRecordID'];
                    $__requestedBy = $bookingInfoAry[$j]['RequestedBy'];
                    $__responsibleUserId = $bookingInfoAry[$j]['ResponsibleUID'];
                    
                    if ($__requestedBy == $_userId || $__responsibleUserId == $_userId) {
                        $_userRelatedBookingDetailsIdAry[] = $__bookingDetailsId;
                    }
                }
                $_numOfRelatedBookingDetails = count($_userRelatedBookingDetailsIdAry);
                
                // consolidate the booking info into an email content
                $_emailContent = '';
                $_emailContent .= "<p>";
                $_emailContent .= $Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam'] . "<BR><BR>";
                
                $_processedRoomIdAry = array();
                for ($j = 0; $j < $_numOfRelatedBookingDetails; $j ++) {
                    $__bookingDetailsId = $_userRelatedBookingDetailsIdAry[$j];
                    $__date = $bookingInfoAssoAry[$__bookingDetailsId]['DateString'];
                    $__startTime = $bookingInfoAssoAry[$__bookingDetailsId]['StartTime'];
                    $__endTime = $bookingInfoAssoAry[$__bookingDetailsId]['EndTime'];
                    $__relatedTo = $bookingInfoAssoAry[$__bookingDetailsId]['RelatedTo'];
                    $__roomId = $bookingInfoAssoAry[$__bookingDetailsId]['RoomID'];
                    $__itemId = $bookingInfoAssoAry[$__bookingDetailsId]['ItemID'];
                    $__NumberOfItem = $bookingInfoAssoAry[$__bookingDetailsId]['NumberOfItem'];
                    
                    if ($__roomId > 0 && isset($_processedRoomIdAry[$__roomId][$__date][$__startTime])) {
                        // avoid duplicate records if a room is booked with many items
                        continue;
                    } else {
                        $_processedRoomIdAry[$__roomId][$__date][$__startTime] = true;
                    }
                    
                    $__targetReason = '';
                    if ($__roomId > 0) {
                        $__targetName = $bookingInfoAssoAry[$__bookingDetailsId]['RoomName'];
                        $__targetBookingStatus = $bookingInfoAssoAry[$__bookingDetailsId]['room_BookingStatus'];
                        $__targetReason = $requestNofify ? $reservedName : $bookingInfoAssoAry[$__bookingDetailsId]['room_RejectReason'];
                    } else 
                        if ($__itemId > 0) {
                            $__targetName = $bookingInfoAssoAry[$__bookingDetailsId]['ItemName'];
                            $__targetBookingStatus = $bookingInfoAssoAry[$__bookingDetailsId]['item_BookingStatus'];
                            $__targetReason = $requestNofify ? $reservedName : $bookingInfoAssoAry[$__bookingDetailsId]['item_RejectReason'];
                        }
                    $__targetReason = trim($__targetReason);
                    
                    $_emailContent .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . " : " . $__date . "<BR>";
                    $_emailContent .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . " : " . $__startTime . "<BR>";
                    $_emailContent .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . " : " . $__endTime . "<BR>";
                    $_emailContent .= $Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] . " : ";
                    
                    // Z117075 handling the case of pending
                    if ($__targetBookingStatus == 1) {
                        $_emailContent .= "<font color='green'>" . $__targetName . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] . "</font><br>";
                    } elseif ($__targetBookingStatus == '0') {
                        $_emailContent .= $__targetName . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Pending'] . '<br>';
                    } else {
                        $_emailContent .= "<font color='red'>" . $__targetName . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] . "</font><br>";
                        if ($__targetReason != '') {
                            $_emailContent .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'] . " : " . $__targetReason . "<br/>";
                        }
                    }
                    // G115068 Villa
                    // Z117075 handling the case of pending
                    if ($__NumberOfItem > 0) {
                        $_emailContent .= "<br>";
                        for ($k = 1; $k < $__NumberOfItem + 1; $k ++) {
                            $itemName = $bookingInfoAssoAry[$__bookingDetailsId]['ItemName_' . $k];
                            $RejectReason = $bookingInfoAssoAry[$__bookingDetailsId]['ItemRejectReason_' . $k];
                            if ($bookingInfoAssoAry[$__bookingDetailsId]['ItemBookingStatus_' . $k] == '1') {
                                $ItemBookingStatus = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'];
                            } elseif ($bookingInfoAssoAry[$__bookingDetailsId]['ItemBookingStatus_' . $k] == '0') {
                                $ItemBookingStatus = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Pending'];
                            } else {
                                $ItemBookingStatus = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'];
                            }
                            // $ItemBookingStatus = ($bookingInfoAssoAry[$__bookingDetailsId]['ItemBookingStatus_'.$k]=='1')? $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']:$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'];
                            $_emailContent .= $Lang['eBooking']['General']['FieldTitle']['Item'] . $k . " : " . Get_String_Display($itemName) . " - " . $ItemBookingStatus;
                            if ($bookingInfoAssoAry[$__bookingDetailsId]['ItemBookingStatus_' . $k] == '1' || $bookingInfoAssoAry[$__bookingDetailsId]['ItemBookingStatus_' . $k] == '0') {
                                // Do Nothing
                            } else {
                                $_emailContent .= "<br>";
                                $_emailContent .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'] . " : " . Get_String_Display($RejectReason);
                            }
                            $_emailContent .= "<br>";
                        }
                    }
                    $_emailContent .= "<br>";
                }
                $_emailContent .= "<p>";
                
                // send mail
                $successAry[$_userId] = $lwebmail->sendModuleMail(array(
                    $_userId
                ), $emailSubject, $_emailContent, 1, '', 'User', true);
            }
            return ! in_array(false, (array) $successAry);
        }

        function Email_Booking_Result($booking_id_arr)
        {
            $this->Load_General_Setting();

            if (! $this->SettingArr['BookingNotification']) // if notification is disabled.
                return true;
            
            global $PATH_WRT_ROOT, $Lang, $intranet_root;
            include_once ('libwebmail.php');
            include_once ('libinventory.php');
            
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            
            $booking_id_arr = (array) $booking_id_arr;
            
            $SizeOfBooking = sizeof($booking_id_arr);
            for ($i = 0; $i < $SizeOfBooking; $i ++) {
                $booking_id = $booking_id_arr[$i];
                $sql = "SELECT RequestedBy FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$booking_id'";
                $arrRequestBy = $this->returnVector($sql);
                
                $sql = "SELECT ResponsibleUID FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$booking_id'";
                $arrResponsible = $this->returnVector($sql);
                
                unset($arrTo);
                $arrTo = array_merge($arrRequestBy, $arrResponsible);
                $arrTo = array_unique($arrTo);
                
                $Subject = $Lang['eBooking']['Mail']['FieldTitle']['ResultOfYourBookigRequest'];
                
                // # get Booking Detail
                $sql = "SELECT Date, StartTime, EndTime FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$booking_id'";
                $arrBookingDetails = $this->returnArray($sql, 3);
                
                // # get room result
                $sql = "SELECT 
								" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . ", 
								room.BookingStatus, room.RejectReason
							FROM 
								INTRANET_EBOOKING_BOOKING_DETAILS as room 
								INNER JOIN INVENTORY_LOCATION as location ON (room.FacilityID = location.LocationID)
								INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (floor.LocationLevelID = location.LocationLevelID)
								INNER JOIN INVENTORY_LOCATION_BUILDING as building ON (floor.BuildingID = building.BuildingID)
							WHERE
								room.BookingID = '$booking_id'
								AND room.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "'
							";
                
                $arrRoomResult = $this->returnArray($sql, 2);
                
                // # get item result
                $sql = "SELECT 
								" . $linventory->getInventoryNameByLang("ebooking_item.") . ", item.BookingStatus, item.RejectReason
							FROM
								INTRANET_EBOOKING_BOOKING_DETAILS as item
								INNER JOIN INTRANET_EBOOKING_ITEM as ebooking_item ON (item.FacilityID = ebooking_item.ItemID)
							WHERE
								item.BookingID = '$booking_id'
								AND item.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "'
						";
                $arrItemResult = $this->returnArray($sql, 2);
                
                if (sizeof($arrBookingDetails) > 0) {
                    list ($booking_date, $start_time, $end_time) = $arrBookingDetails[0];
                    
                    $Message = "<p>";
                    $Message .= $Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam'] . "<BR><BR>";
                    $Message .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . " : " . $booking_date . "<BR>";
                    $Message .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . " : " . $start_time . "<BR>";
                    $Message .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . " : " . $end_time . "<BR><BR>";
                    
                    if (sizeof($arrRoomResult) > 0) {
                        for ($j = 0; $j < sizeof($arrRoomResult); $j ++) {
                            list ($location_name, $result, $RejectReason) = $arrRoomResult[$j];
                            if ($j == 0)
                                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] . " : <BR>";
                            
                            if ($result == 1)
                                $Message .= "<font color='green'>" . $location_name . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] . "</font><br><br>";
                            else {
                                $Message .= "<font color='red'>" . $location_name . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] . "</font><br><br>";
                                if (trim($RejectReason) != '')
                                    $Message .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'] . ": <br/>" . $RejectReason . "<br/><br/>";
                            }
                        }
                    }
                    if (sizeof($arrItemResult) > 0) {
                        for ($j = 0; $j < sizeof($arrItemResult); $j ++) {
                            list ($item_name, $result, $RejectReason) = $arrItemResult[$j];
                            if ($j == 0)
                                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] . " : <BR>";
                            
                            if ($result == 1)
                                $Message .= "<font color='green'>" . $item_name . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] . "</font><br><br>";
                            else {
                                $Message .= "<font color='red'>" . $item_name . " - " . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] . "</font><br><br>";
                                if (trim($RejectReason) != '')
                                    $Message .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'] . ": <br/>" . $RejectReason . "<br/><br/>";
                            }
                        }
                    }
                    $Message .= "</p>";
                }
                
                $resultArr[] = $lwebmail->sendModuleMail($arrTo, $Subject, $Message, 1, '', 'User', true);
            }
            return ! in_array(false, (array) $resultArr);
        }

        function SendMailToFollowUpGroup($booking_id)
        {
            global $PATH_WRT_ROOT, $Lang, $intranet_root, $intranet_session_language, $sys_custom;
            include_once ('libwebmail.php');
            include_once ('libinventory.php');
            
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            
            // ## Get Target Recipients
            $sql = "SELECT member.UserID FROM INTRANET_EBOOKING_BOOKING_DETAILS as room_details INNER JOIN INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION AS relation ON (room_details.FacilityID = relation.LocationID AND room_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "') INNER JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE room_details.BookingID = '$booking_id'";
            $arrLocationFollowUp = $this->returnVector($sql);
            $sql = "SELECT member.UserID FROM INTRANET_EBOOKING_BOOKING_DETAILS as item_details INNER JOIN INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP AS relation ON (item_details.FacilityID = relation.ItemID AND item_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "') INNER JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE item_details.BookingID = '$booking_id'";
            $arrItemFollowUp = $this->returnVector($sql);
            
            unset($arrTo);
            $arrTo = array_merge($arrLocationFollowUp, $arrItemFollowUp);
            $arrTo = array_unique($arrTo);
            
            if (count($arrTo) == 0) {
                return true;
            }
            
            // $Subject = "eBooking - Follow-up Work";
            $Subject = $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['emailTitle'];
            
            // # get Booking Detail
            $sql = "SELECT Date, StartTime, EndTime, RequestedBy, ResponsibleUID, Remark FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$booking_id'";
            $arrBookingDetails = $this->returnArray($sql, 3);
            
            // # get room result
            $sql = "SELECT 
							" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . "
						FROM 
							INTRANET_EBOOKING_BOOKING_DETAILS as room 
							INNER JOIN INVENTORY_LOCATION as location ON (room.FacilityID = location.LocationID  AND room.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "')
							INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (floor.LocationLevelID = location.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING as building ON (floor.BuildingID = building.BuildingID)
						WHERE
							room.BookingID = '$booking_id' AND room.BookingStatus = " . LIBEBOOKING_BOOKING_STATUS_APPROVED;
            $arrRoomResult = $this->returnArray($sql, 1);
            
            // # get item result
            $sql = "SELECT 
							" . $linventory->getInventoryNameByLang("ebooking_item.") . "
						FROM
							INTRANET_EBOOKING_BOOKING_DETAILS as item
							INNER JOIN INTRANET_EBOOKING_ITEM as ebooking_item ON (item.FacilityID = ebooking_item.ItemID AND item.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "')
						WHERE
							item.BookingID = '$booking_id' AND item.BookingStatus = " . LIBEBOOKING_BOOKING_STATUS_APPROVED;
            $arrItemResult = $this->returnArray($sql, 2);
            
            if (sizeof($arrBookingDetails) > 0) {
                list ($booking_date, $start_time, $end_time, $requestBy, $responsibleBy, $remark) = $arrBookingDetails[0];
                
                $nameField = getNameFieldByLang();
                $sql = "Select UserID, $nameField as NameDisplay From INTRANET_USER Where UserID IN ('" . $requestBy . "', '" . $responsibleBy . "')";
                $nameAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID');
                
                $Message = "<p>";
                // $Message .= $Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam']."<BR><BR>";
                if ($intranet_session_language == 'en') {
                    $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['dearSir/Madam'] . "<BR><BR>";
                }
                $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['PleaseFollowItem'] . ":<BR><BR>";
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . ": " . $booking_date . "<BR>";
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . ": " . $start_time . "<BR>";
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . ": " . $end_time . "<BR>";
                
                
                $remark = ($remark == '') ? $Lang['General']['EmptySymbol'] : $remark;
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['Remark'] . ": " . $remark . "<BR>";
                
                $Message .= '<BR>';
                
                if (sizeof($arrRoomResult) > 0) {
                    for ($j = 0; $j < sizeof($arrRoomResult); $j ++) {
                        list ($location_name) = $arrRoomResult[$j];
                        if ($j == 0) {
                            // $Message .= "Please follow-up"." : <BR>";
                            $Message .= $Lang['eBooking']['Settings']['SystemProperty']['Room'] . ": <BR>";
                        }
                        
                        // if($result == 1)
                        // $Message .= "<font color='green'>".$location_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."</font><br><br>";
                        // else
                        // $Message .= "<font color='red'>".$location_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."</font><br><br>";
                        $Message .= $location_name . "<BR><BR>";
                    }
                }
                if (sizeof($arrItemResult) > 0) {
                    for ($j = 0; $j < sizeof($arrItemResult); $j ++) {
                        list ($item_name, $result) = $arrItemResult[$j];
                        if ($j == 0) {
                            // $Message .= "Please follow-up"." : <BR>";
                            $Message .= $Lang['eBooking']['General']['FieldTitle']['Item'] . ": <BR>";
                        }
                        // $Message .= $Lang['eBooking']['Mail']['FieldTitle']['RoomsResult']." : <BR>";
                        
                        // if($result == 1)
                        // $Message .= "<font color='green'>".$item_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."</font><br><br>";
                        // else
                        // $Message .= "<font color='red'>".$item_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."</font><br><br>";
                        $Message .= $item_name . "<BR>";
                    }
                    $Message .= "<BR>";
                }
                
                $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['WillBeUsedBy'] . ": " . $nameAssoAry[$responsibleBy]['NameDisplay'] . "<BR>";
                $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['BookedBy'] . ": " . $nameAssoAry[$requestBy]['NameDisplay'] . "<BR><BR>";
                
                $Message .= "</p>";
            }
            
            $result = $lwebmail->sendModuleMail($arrTo, $Subject, $Message, 1, '', 'User', true);
            return $result;
        }
        
        // Bill added: sending out email notification to approval group
        function SendMailToManageGroup($booking_id)
        {
            
            // debug_pr($booking_id);
            global $PATH_WRT_ROOT, $Lang, $intranet_root, $intranet_session_language, $sys_custom;
            include_once ('libwebmail.php');
            include_once ('libinventory.php');
            
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            
            // ## Get Target Recipients
            $sql = "SELECT member.UserID FROM INTRANET_EBOOKING_BOOKING_DETAILS as room_details INNER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS relation ON (room_details.FacilityID = relation.LocationID AND room_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "') INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE room_details.BookingID = '$booking_id'";
            $arrLocationApproval = $this->returnVector($sql);
            $sql = "SELECT member.UserID FROM INTRANET_EBOOKING_BOOKING_DETAILS as item_details INNER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS relation ON (item_details.FacilityID = relation.ItemID AND item_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "') INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE item_details.BookingID = '$booking_id'";
            $arrItemApproval = $this->returnVector($sql);
            // debug_pr($arrItemFollowUp);
            unset($arrTo);
            $arrTo = array_merge($arrLocationApproval, $arrItemApproval);
            $arrTo = array_unique($arrTo);
            
            if (count($arrTo) == 0) {
                return true;
            }
            
            // $Subject = "eBooking - Follow-up Work";
            $Subject = $Lang['eBooking']['Mail']['FieldTitle']['WaitForApprovalRequest'];
            
            // # get Booking Detail
            $sql = "SELECT Date, StartTime, EndTime, RequestedBy, ResponsibleUID, Remark FROM INTRANET_EBOOKING_RECORD WHERE BookingID = '$booking_id'";
            $arrBookingDetails = $this->returnArray($sql, 3);
            
            // # get room result
            $sql = "SELECT 
							" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . "
						FROM 
							INTRANET_EBOOKING_BOOKING_DETAILS as room 
							INNER JOIN INVENTORY_LOCATION as location ON (room.FacilityID = location.LocationID  AND room.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "')
							INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (floor.LocationLevelID = location.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING as building ON (floor.BuildingID = building.BuildingID)
						WHERE
							room.BookingID = '$booking_id' AND room.BookingStatus = 0";
            $arrRoomResult = $this->returnArray($sql, 1);
            
            // # get item result
            $sql = "SELECT 
							" . $linventory->getInventoryNameByLang("ebooking_item.") . "
						FROM
							INTRANET_EBOOKING_BOOKING_DETAILS as item
							INNER JOIN INTRANET_EBOOKING_ITEM as ebooking_item ON (item.FacilityID = ebooking_item.ItemID AND item.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "')
						WHERE
							item.BookingID = '$booking_id' AND item.BookingStatus = 0";
            $arrItemResult = $this->returnArray($sql, 2);
            
            if (sizeof($arrBookingDetails) > 0) {
                list ($booking_date, $start_time, $end_time, $requestBy, $responsibleBy, $remark) = $arrBookingDetails[0];
                
                $nameField = getNameFieldByLang();
                $sql = "Select UserID, $nameField as NameDisplay From INTRANET_USER Where UserID IN ('" . $requestBy . "', '" . $responsibleBy . "')";
                $nameAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID');
                
                $Message = "<p>";
                // $Message .= $Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam']."<BR><BR>";
                if ($intranet_session_language == 'en') {
                    $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['dearSir/Madam'] . "<BR><BR>";
                }
                $Message .= $Lang['eBooking']['eMailContentAry']['ManageWorkAry']['PleaseManageItem'] . ":<BR><BR>";
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . ": " . $booking_date . "<BR>";
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . ": " . $start_time . "<BR>";
                $Message .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . ": " . $end_time . "<BR>";
                
                /*
                 * if($sys_custom['eBookingSendMailWithRemark']){
                 * $remark = ($remark=='')? $Lang['General']['EmptySymbol'] : $remark;
                 * $Message .= $Lang['eBooking']['Mail']['FieldTitle']['Remark'].": ".$remark."<BR>";
                 * }
                 */
                
                $Message .= '<BR>';
                
                // debug_pr($arrRoomResult);
                // debug_pr($arrItemResult);
                if (sizeof($arrRoomResult) > 0) {
                    for ($j = 0; $j < sizeof($arrRoomResult); $j ++) {
                        list ($location_name) = $arrRoomResult[$j];
                        if ($j == 0) {
                            // $Message .= "Please follow-up"." : <BR>";
                            $Message .= $Lang['eBooking']['Settings']['SystemProperty']['Room'] . ": <BR>";
                        }
                        
                        // if($result == 1)
                        // $Message .= "<font color='green'>".$location_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."</font><br><br>";
                        // else
                        // $Message .= "<font color='red'>".$location_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."</font><br><br>";
                        $Message .= $location_name . "<BR><BR>";
                    }
                }
                if (sizeof($arrItemResult) > 0) {
                    for ($j = 0; $j < sizeof($arrItemResult); $j ++) {
                        list ($item_name, $result) = $arrItemResult[$j];
                        if ($j == 0) {
                            // $Message .= "Please follow-up"." : <BR>";
                            $Message .= $Lang['eBooking']['General']['FieldTitle']['Item'] . ": <BR>";
                        }
                        // $Message .= $Lang['eBooking']['Mail']['FieldTitle']['RoomsResult']." : <BR>";
                        
                        // if($result == 1)
                        // $Message .= "<font color='green'>".$item_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved']."</font><br><br>";
                        // else
                        // $Message .= "<font color='red'>".$item_name." - ".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected']."</font><br><br>";
                        $Message .= $item_name . "<BR>";
                    }
                    $Message .= "<BR>";
                }
                // debug_pr($Message);
                // debug_pr($arrTo);
                $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['WillBeUsedBy'] . ": " . $nameAssoAry[$responsibleBy]['NameDisplay'] . "<BR>";
                $Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['BookedBy'] . ": " . $nameAssoAry[$requestBy]['NameDisplay'] . "<BR><BR>";
                $Message .= $Lang['eBooking']['eMailContentAry']['ManageWorkAry']['Remarks'];
                $Message .= "</p>";
            }
            
            $result = $lwebmail->sendModuleMail($arrTo, $Subject, $Message, 1, '', 'User', true);
            return $result;
        }

        function SendMailToManageGroup_Merged($booking_id)
        {
            $this->Load_General_Setting();
            
            if (! $this->SettingArr['ApprovalNotification']) {
                return true;
            }
            
            global $PATH_WRT_ROOT, $Lang, $intranet_root, $sys_custom;
            include_once ($PATH_WRT_ROOT . 'includes/libwebmail.php');
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            
            // ## Get Target Recipients (Location)
            $sql = "SELECT member.UserID, room_details.BookingID FROM INTRANET_EBOOKING_BOOKING_DETAILS as room_details INNER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS relation ON (room_details.FacilityID = relation.LocationID AND room_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "') INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE room_details.BookingID IN( \"" . implode('","', $booking_id) . "\" ) ";
            $arrLocationFollowUp = $this->returnResultSet($sql);
            
            // ## Get Target Recipients (Item)
            $sql = "SELECT member.UserID, item_details.BookingID FROM INTRANET_EBOOKING_BOOKING_DETAILS as item_details INNER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS relation ON (item_details.FacilityID = relation.ItemID AND item_details.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "') INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS member ON (relation.GroupID = member.GroupID) WHERE item_details.BookingID IN( \"" . implode('","', $booking_id) . "\" )";
            $arrItemFollowUp = $this->returnResultSet($sql);
            
            $targetFollowUpAry = (! empty($arrLocationFollowUp)) ? $arrLocationFollowUp : $arrItemFollowUp;
            foreach ($targetFollowUpAry as $UserFollowUpAry) {
                $_userId = $UserFollowUpAry['UserID'];
                $_bookingId = $UserFollowUpAry['BookingID'];
                
                $UserFollowUpAssoAry[$_bookingId][] = $_userId;
            }
            
            // # get Booking Detail
            $sql = "SELECT Date, StartTime, EndTime, RequestedBy, ResponsibleUID, Remark, RelatedTo, BookingID FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN( \"" . implode('","', $booking_id) . "\" )";
            $arrBookingDetails = $this->returnResultSet($sql);
            foreach ($arrBookingDetails as $BookingIDInfoAry) {
                $GroupByRelatedBookingIDInfoAry[$BookingIDInfoAry['RelatedTo']][$BookingIDInfoAry['BookingID']] = $BookingIDInfoAry;
                $RecipientUserIDArr[] = $BookingIDInfoAry['RequestedBy'];
                $RecipientUserIDArr[] = $BookingIDInfoAry['ResponsibleUID'];
            }
            
            // ## Get Recipients Name Array
            $RecipientUserIDArr = array_unique((array)$RecipientUserIDArr);
            $sql = " SELECT UserID, EnglishName, ChineseName FROM INTRANET_USER WHERE UserID IN ( '" . implode("','", (array)$RecipientUserIDArr) . "' ) ";
            $RecipientNameArr = $this->ReturnResultSet($sql);
            $RecipientNameAssoArr = BuildMultiKeyAssoc($RecipientNameArr, 'UserID');
            
            $MapFollowupUserBookingAssoAry = array();
            foreach ((array) $GroupByRelatedBookingIDInfoAry as $_relatedToBookingId => $_bookingAssoAry) {
                foreach ((array) $_bookingAssoAry as $__bookingId => $__bookingInfoAry) {
                    $__followUpUserIdAry = array_values(array_unique((array) $UserFollowUpAssoAry[$__bookingId]));
                    $__numOfFollowUpUser = count($__followUpUserIdAry);
                    
                    for ($i = 0; $i < $__numOfFollowUpUser; $i ++) {
                        $___userId = $__followUpUserIdAry[$i];
                        
                        if (isset($GroupByRelatedBookingIDInfoAry[$_relatedToBookingId])) {
                            $MapFollowupUserBookingAssoAry[$___userId][$_relatedToBookingId] = $GroupByRelatedBookingIDInfoAry[$_relatedToBookingId];
                        }
                    }
                }
            }
            
            // # get room result
            $sql = "SELECT 
							room.BookingID,
							" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . " as Location
						FROM 
							INTRANET_EBOOKING_BOOKING_DETAILS as room 
							INNER JOIN INVENTORY_LOCATION as location ON (room.FacilityID = location.LocationID  AND room.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "')
							INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (floor.LocationLevelID = location.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING as building ON (floor.BuildingID = building.BuildingID)
						WHERE
							room.BookingID IN( \"" . implode('","', $booking_id) . "\" ) AND room.BookingStatus = " . LIBEBOOKING_BOOKING_STATUS_PENDING;
            $arrRoomResult = $this->returnResultSet($sql);
            $arrRoomResultAssoAry = BuildMultiKeyAssoc($arrRoomResult, 'BookingID', array(
                'Location'
            ), 1);
            
            // # get item result
            $sql = "SELECT
							item.BookingID,
							" . $linventory->getInventoryNameByLang("ebooking_item.") . " as ItemName
						FROM
							INTRANET_EBOOKING_BOOKING_DETAILS as item
							INNER JOIN INTRANET_EBOOKING_ITEM as ebooking_item ON (item.FacilityID = ebooking_item.ItemID AND item.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "')
						WHERE
							item.BookingID IN( \"" . implode('","', $booking_id) . "\" ) AND item.BookingStatus = " . LIBEBOOKING_BOOKING_STATUS_PENDING;
            $arrItemResult = $this->returnResultSet($sql);
            foreach ($arrItemResult as $itemBookingInfoAry) {
                $arrItemResultAssoAry[$itemBookingInfoAry['BookingID']][] = $itemBookingInfoAry['ItemName'];
            }
            
            // Build Mail and send
            foreach ($MapFollowupUserBookingAssoAry as $_FollowUserID => $_UserFollowUpAry) {
                // Merged Among different related booking
                $_MergedMessage = "";
                foreach ($_UserFollowUpAry as $__RelatedBookingID => $__RelatedBookingAry) { // email content build in this loop
                                                                                           // Build Message (Start)
                                                                                           // Merged Among different date time on same related booking
                    $__Message = "";
                    // initialize
                    $__Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['dearSir/Madam'] . "<BR><BR>";
                    $__Message .= $Lang['eBooking']['eMailContentAry']['ManageWorkAry']['PleaseManageItem'] . "<BR><BR>";
                    foreach ($__RelatedBookingAry as $___BookingInfoAry) {
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . ": " . $___BookingInfoAry['Date'] . "<BR>";
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . ": " . $___BookingInfoAry['StartTime'] . "<BR>";
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . ": " . $___BookingInfoAry['EndTime'] . "<BR>";
                        
                        $remark = $___BookingInfoAry['Remark'];
                        $remark = ($remark == '') ? $Lang['General']['EmptySymbol'] : $remark;
                        $__Message .= $Lang['eBooking']['Mail']['FieldTitle']['Remark'] . ": " . $remark . "<BR>";
                    
                        
                        // Room Details
                        $__Message .= '<BR>';
                        if (! empty($arrRoomResultAssoAry)) {
                            $__Message .= $Lang['eBooking']['Settings']['SystemProperty']['Room'] . ": <BR>";
                            $__Message .= $arrRoomResultAssoAry[$___BookingInfoAry['BookingID']] . "<br><br>";
                        }
                        
                        // Item Details
                        if (! empty($arrItemResultAssoAry[$___BookingInfoAry['BookingID']])) {
                            $__Message .= $Lang['eBooking']['General']['FieldTitle']['Item'] . ": <BR>";
                            foreach ($arrItemResultAssoAry[$___BookingInfoAry['BookingID']] as $item_name) {
                                $__Message .= $item_name . "<BR>";
                            }
                            $__Message .= "<BR>";
                        }
                        $__Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['WillBeUsedBy'] . ": " . $RecipientNameAssoArr[$___BookingInfoAry['ResponsibleUID']]['EnglishName'] . "<BR>";
                        $__Message .= $Lang['eBooking']['eMailContentAry']['followUpWorkAry']['BookedBy'] . ": " . $RecipientNameAssoArr[$___BookingInfoAry['RequestedBy']]['EnglishName'] . "<BR><BR>";
                    }
                    $__Message .= $Lang['eBooking']['eMailContentAry']['ManageWorkAry']['Remarks'];
                    $__Message .= '<br><hr><br>';
                    // Build Message (End)
                    $_MergedMessage .= $__Message;
                }
                $toAry[] = $_FollowUserID;
                $Subject = $Lang['eBooking']['Mail']['FieldTitle']['WaitForApprovalRequest'];
                $successAry[] = $lwebmail->sendModuleMail($toAry, $Subject, $_MergedMessage, 1, '', 'User', true);
                
                // $successAry[] = $lwebmail->sendModuleMail($toAry,$Subject,$_MergedMessage,1,0,true);
                unset($toAry);
            }
            
            return ! in_array(false, (array) $successAry);
        }

        function Send_Cancel_Booking_Email_Notification($targetBookingID)
        {
            global $Lang, $intranet_root, $sys_custom;
            
            $this->Load_General_Setting();
            // Settings - to be changed
            if (! $this->SettingArr['ReceiveCancelBookingEmailNotification']) { // if notification is disabled.
                return true;
            }
            
            include_once ('libwebmail.php');
            include_once ('libinventory.php');
            include_once ('librole.php');
            include_once ('libuser.php');
            
            $lwebmail = new libwebmail();
            $linventory = new libinventory();
            $librole = new librole();
            $libuser = new libuser();
            
            // # Get target receipients - eBooking Admin User ##
            $toAdminUserIDArr = $librole->returnRoleAdminUserID('eBooking');
            
            // add sys_custom flag for email to SCL users
            if ($sys_custom['eBooking']['SendEmailToSpecialUserLoginAry']) {
                $_userLoginArr = array();
                $userIDAry = array();
                $_userLoginArr = implode("','", $sys_custom['eBooking']['SendEmailToSpecialUserLoginAry']);
                $sql_cond = " IN ('" . $_userLoginArr . "') ";
                $sql = " SELECT UserID FROM INTRANET_USER WHERE UserLogin $sql_cond ";
                
                $userIDAry = $this->returnResultSet($sql);
                foreach ($userIDAry as $userId) {
                    $toAdminUserIDArr[] = $userId['UserID'];
                }
                $toAdminUserIDArr = array_values(array_unique($toAdminUserIDArr));
            }
            
            // # Get Booking Request Detail
            $sql = "SELECT BookingID, Date, StartTime, EndTime, InputBy FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($targetBookingID)";
            $bookingDetailsArr = $this->returnArray($sql, 3);
            
            // # Get Room Info
            $sql = "SELECT 
							room.BookingID,
							" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . " AS locationName				
						FROM 
							INTRANET_EBOOKING_BOOKING_DETAILS as room 
							INNER JOIN INVENTORY_LOCATION as location ON (room.FacilityID = location.LocationID)
							INNER JOIN INVENTORY_LOCATION_LEVEL as floor ON (floor.LocationLevelID = location.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING as building ON (floor.BuildingID = building.BuildingID)
						WHERE
							room.BookingID IN ($targetBookingID)
							AND room.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "'
						";
            
            $roomResultsArr = $this->returnArray($sql, 2);
            $roomResultsAssoc = BuildMultiKeyAssoc($roomResultsArr, 'BookingID');
            
            // # Get Item Info
            $sql = "SELECT 
							item.BookingID,
							" . $linventory->getInventoryNameByLang("ebooking_item.") . " As itemName
						FROM
							INTRANET_EBOOKING_BOOKING_DETAILS as item
							INNER JOIN INTRANET_EBOOKING_ITEM as ebooking_item ON (item.FacilityID = ebooking_item.ItemID)
						WHERE
							item.BookingID IN ($targetBookingID)
							AND item.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "'
						";
            $itemResultsArr = $this->returnArray($sql, 2);
            $itemResultsAssoc = BuildMultiKeyAssoc($itemResultsArr, 'BookingID', $IncludedDBField = array(
                'itemName'
            ), $SingleValue = 1, $BuildNumericArray = 1);
            
            // ##### Constructing Cancel/ Delete Booking Request Email ######
            // Email Subject
            $emailSubject = '';
            $emailSubject = $Lang['eBooking']['eService']['CancelBookingNotification'];
            
            $numOfBookingDetails = count($bookingDetailsArr);
            $resultArr = array();
            $emailMessage = '';
            $emailMessage .= "<p>";
            $emailMessage .= $Lang['eBooking']['eService']['PleaseNoteThatBelowBookingIsBeingCancelled'] . ": <BR><BR>";
            for ($i = 0; $i < $numOfBookingDetails; $i ++) {
                $thisBookingId = $bookingDetailsArr[$i]['BookingID'];
                $thisBookingDate = $bookingDetailsArr[$i]['Date'];
                $thisStartTime = $bookingDetailsArr[$i]['StartTime'];
                $thisEndTime = $bookingDetailsArr[$i]['EndTime'];
                $thisUserId = $bookingDetailsArr[$i]['InputBy'];
                
                // Request User's Name
                $thisUserInfoArr = $libuser->returnUser($thisUserId);
                $thisUserEngName = $thisUserInfoArr[0]['EnglishName'];
                $thisUserChiName = $thisUserInfoArr[0]['ChineseName'];
                
                $thisUserName = $thisUserEngName;
                if ($thisUserChiName != '') {
                    $thisUserName .= ' (' . $thisUserChiName . ')';
                }
                
                // Email Content
                // $emailMessage = '';
                // $emailMessage .= "<p>";
                // $emailMessage .= $Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam']."<BR><BR>";
                // $emailMessage .= $Lang['eBooking']['eService']['PleaseNoteThatBelowBookingIsBeingCancelled'] . ": <BR><BR>";
                $emailMessage .= $Lang['eBooking']['General']['Export']['FieldTitle']['BookedBy'] . " : " . $thisUserName . "<BR>";
                $emailMessage .= $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] . " : " . $thisBookingDate . "<BR>";
                $emailMessage .= $Lang['eBooking']['Mail']['FieldTitle']['StartTime'] . " : " . $thisStartTime . "<BR>";
                $emailMessage .= $Lang['eBooking']['Mail']['FieldTitle']['EndTime'] . " : " . $thisEndTime . "<BR>";
                
                // Check If Request Has Room Info
                $thisLocationName = $roomResultsAssoc[$thisBookingId]['locationName'];
                if ($thisLocationName != '') {
                    $emailMessage .= $Lang['eBooking']['General']['FieldTitle']['Room'] . " : " . $thisLocationName . "<BR>";
                }
                
                // Check If Request Has Item Info
                // $thisItemName = $itemResultsAssoc[$thisBookingId]['itemName'];
                $thisItemName = implode(', ', (array) $itemResultsAssoc[$thisBookingId]);
                if ($thisItemName != '') {
                    $emailMessage .= $Lang['eBooking']['General']['FieldTitle']['Item'] . " : " . $thisItemName . "<BR>";
                }
                
                $emailMessage .= "<BR>";
                
                // Send Email
                // 2013-0910-1707-32073
                // $resultArr[] = $lwebmail->sendModuleMail($toAdminUserIDArr,$emailSubject,$emailMessage);
            }
            
            $emailMessage .= "</p>";
            $resultArr[] = $lwebmail->sendModuleMail($toAdminUserIDArr, $emailSubject, $emailMessage, 1, '', 'User', true);
            
            return ! in_array(false, (array) $resultArr);
        }

        function IsManagementGroupMember($uid)
        {
            $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER WHERE UserID = '$uid'";
            $arrIsMgmtMemeber = $this->returnVector($sql);
            if ($arrIsMgmtMemeber[0] > 0) {
                return true;
            } else {
                return false;
            }
        }

        function IsFollowUpGroupMemeber($uid)
        {
            $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER WHERE UserID = '$uid'";
            $arrIsFollowUpGroupMemeber = $this->returnVector($sql);
            if ($arrIsFollowUpGroupMemeber[0] > 0) {
                return true;
            } else {
                return false;
            }
        }

        function Time_Slot_Validation($StartDate, $EndDate)
        {
            $sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$StartDate' BETWEEN PeriodStart AND PeriodEnd";
            $arrCheckStartDate = $this->returnVector($sql);
            
            $sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$EndDate' BETWEEN PeriodStart AND PeriodEnd";
            $arrCheckEndDate = $this->returnVector($sql);
            
            if ($arrCheckStartDate[0] != $arrCheckEndDate[0]) {
                return false;
            } else {
                return true;
            }
        }

        function isSameTimeTable($SortDateArr)
        { // 28-9-2016 Villa J104488 funtion to check if the selected date is in same timetable or not
                                                
            // Get the unique Peroid ID from the date selected
            for ($i = 0; $i < count($SortDateArr); $i ++) {
                $sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$SortDateArr[$i]' BETWEEN PeriodStart AND PeriodEnd";
                $PeriodID_2Darray[] = $this->returnVector($sql);
                $PeriodID[] = $PeriodID_2Darray[$i][0];
            }
            $PeriodID = array_unique($PeriodID);
            $PeriodID = implode(',', $PeriodID);
            
            // Get the TimetableID from the unique Peroid ID above
            $sql = "SELECT TimetableID FROM INTRANET_PERIOD_TIMETABLE_RELATION Where PeriodID in($PeriodID) GROUP BY TimetableID";
            $TimetableID = $this->returnVector($sql);
            
            // Get if the $TimetableID is the same
            $isSameTimeTable = count($TimetableID) == 1 ? true : false;
            
            // fix
            // check if all dates are from special timetable
            $sql = "SELECT SpecialTimetableSettingsID from INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE WHERE SpecialDate IN ('" . implode("','", $SortDateArr) . "')";
            $specialDateTimetableArr = $this->returnVector($sql);
            if (count($specialDateTimetableArr) > 0) {
                $allDateAreSpecial = count($specialDateTimetableArr) == count($SortDateArr);
                $onlyOneSpecialTimetable = count(array_unique($specialDateTimetableArr)) == 1;
                
                return $isSameTimeTable && ($allDateAreSpecial && $onlyOneSpecialTimetable);
            } else {
                return $isSameTimeTable;
            }
        }

        function Has_Any_Special_Timetable($SortDateArr)
        {
            $sql = "SELECT SpecialTimetableSettingsID from INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE WHERE SpecialDate IN ('" . implode("','", $SortDateArr) . "')";
            $specialDateTimetableArr = $this->returnVector($sql);
            return count($specialDateTimetableArr) > 0;
        }
        
        // input e.g. $TimeRangeArr = array("18:00-20:00","19:00-21:00","16:00-18:00","12:30-13:00");
        // output e.g. $newTimeRange = array("16:00-21:00","12:30-13:00");
        function Union_Time_Range($TimeRangeArr)
        {
            if (count($TimeRangeArr) <= 1)
                return $TimeRangeArr;
            
            $new_start = array();
            $new_end = array();
            foreach ($TimeRangeArr as $TimeRange) {
                list ($start, $end) = explode("-", $TimeRange);
                
                $start = strtotime($start);
                $end = strtotime($end);
                
                if (count($new_start) == 0) {
                    $new_start[] = $start;
                    $new_end[] = $end;
                } else {
                    
                    for ($i = 0; $i < count($new_start); $i ++) {
                        $InExistPeriod = true;
                        if ($start >= $new_start[$i] && $start <= $new_end[$i] && $end > $new_end[$i]) {
                            $new_end[$i] = $end;
                        } else 
                            if ($end >= $new_start[$i] && $end <= $new_end[$i] && $start < $new_start[$i]) {
                                $new_start[$i] = $start;
                            } else 
                                if ($start < $new_start[$i] && $end > $new_end[$i]) {
                                    $new_start[$i] = $start;
                                    $new_end[$i] = $end;
                                } else 
                                    if ($start >= $new_start[$i] && $end <= $new_end[$i]) {
                                        // do nothing
                                    } else
                                        $InExistPeriod = false;
                    }
                    if (! $InExistPeriod) {
                        $new_start[] = $start;
                        $new_end[] = $end;
                    }
                }
            }
            for ($i = 0; $i < count($new_start); $i ++) {
                $newTimeRange[] = date("H:i", $new_start[$i]) . "-" . date("H:i", $new_end[$i]);
            }
            
            return $newTimeRange;
        }

        function Union_Time_Range_Of_Date_Assoc($DateArr)
        {
            foreach ((array) $DateArr as $thisDate => $thisDatePeriodsArr) {
                $AvailableTimeRange = array();
                
                foreach ((array)$thisDatePeriodsArr as $thisDatePeriods) {
                    $AvailableStartTimets = strtotime($thisDatePeriods['StartTime']);
                    $AvailableEndTimets = strtotime($thisDatePeriods['EndTime']);
                    
                    $AvailableTimeRange[] = Date("H:i", $AvailableStartTimets) . '-' . Date("H:i", $AvailableEndTimets);
                }
                $ReturnArr[$thisDate] = $this->Union_Time_Range($AvailableTimeRange);
            }
            
            return $ReturnArr;
        }
        // ###### marcus
        function Check_Can_Delete_Category($CategoryID = '', $Category2ID = '')
        {
            $cond_CategoryID = '';
            if ($CategoryID != '')
                $cond_CategoryID = " And CategoryID = '$CategoryID' ";
            
            $cond_Category2ID = '';
            if ($Category2ID != '')
                $cond_Category2ID = " And Category2ID = '$Category2ID' ";
            
            $sql = "(	
							Select 
									ItemID
							From
									INTRANET_EBOOKING_ITEM
							Where
									RecordStatus = 1
									$cond_CategoryID
									$cond_Category2ID
						)
						Union
						(
							Select 
									ItemID
							From
									INVENTORY_ITEM
							Where
									RecordStatus = 1
									$cond_CategoryID
									$cond_Category2ID
						)";
            $ReturnArr = $this->returnArray($sql);
            
            if (count($ReturnArr) > 0)
                $canDelete = false;
            else
                $canDelete = true;
            
            return $canDelete;
        }

        function CreatePersonalCalenderEvent($OwnerID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $EventTitle, $EventDesc, $RoomID = "")
        {
            include_once ("icalendar.php");
            include_once ("libinventory.php");
            $iCal = new icalendar();
            $linventory = new libinventory();
            
            $this->Start_Trans();
            
            $isImportant = "";
            $isAllDay = "";
            $access = "P";
            $url = "";
            
            // get the CalendarID for the targer user
            $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
            $TargetCalendarID = $iCal->returnVector($sql);
            $calID = $TargetCalendarID[0];
            
            // Insert My Calendar if user not yet have My Calendar
            if ($calID == "" || $calID == 0) {
                $iCal->insertMyCalendar();
                $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                $TargetCalendarID = $iCal->returnVector($sql);
                $calID = $TargetCalendarID[0];
            }
            
            if ($RoomID != "") {
                $sql = "SELECT 
								" . $this->Get_Location_Name_Field("building.", "floor.", "room.") . "
							FROM
								INVENTORY_LOCATION_BUILDING AS building
								INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
								INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
							WHERE 
								room.LocationID = '$RoomID'";
                $arrLocation = $linventory->returnVector($sql);
                $location = $arrLocation[0];
            } else {
                $location = "";
            }
            
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeRangeInfoArr) {
                $booking_date = $thisDate;
                $start_time = $thisTimeRangeInfoArr[0]['StartTime'];
                $end_time = $thisTimeRangeInfoArr[0]['EndTime'];
                
                $event_date_time = $booking_date . " " . $start_time;
                
                $start_timestamp = strtotime($booking_date . " " . $start_time) . "<BR>";
                $end_timestamp = strtotime($booking_date . " " . $end_time);
                
                $duration = ($end_timestamp - $start_timestamp) / 60;
                
                $fieldname = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
                $fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID";
                
                $fieldvalue = "'".$OwnerID . "', '$event_date_time', NOW(), NOW(), $duration, ";
                // $fieldvalue .= "'$isImportant', '$isAllDay', '$access', '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($EventTitle))."', '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
                $fieldvalue .= "'$isImportant', '$isAllDay', '$access', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($EventTitle)) . "', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($EventDesc)) . "', ";
                $fieldvalue .= "'" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)) . "', '$url', '$calID'";
                
                $sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
                $result[] = $iCal->db_db_query($sql);
                $eventID = $iCal->db_insert_id();
                
                global $schoolNameAbbrev;
                $uid = $OwnerID . "-{$calID}-{$eventID}@{$schoolNameAbbrev}.tg";
                $sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$eventID'";
                $iCal->db_db_query($sql);
            }
            
            if (! in_array(false, $result)) {
                $this->Commit_Trans();
                return true;
            } else {
                $this->RollBack_Trans();
                return false;
            }
        }

        function Create_Temp_Booking_Record_iCal($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr)
        {
            global $Lang;
            $SuccessArr = array();
            $numOfSingleItem = count($SingleItemIDArr);
            
            $FromModule = $SettingsArr['FromModule'];
            
            $CancelDayBookingIfOneItemIsNA = ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1) ? 1 : 0;
            
            // $this->Start_Trans_For_Lock_Table_Procedure();
            $this->Start_Trans();
            
            // Lock tables
            $sql = "LOCK TABLES 
								INTRANET_EBOOKING_RECORD WRITE
								INTRANET_EBOOKING_BOOKING_DETAILS WRITE
								";
            $this->db_db_query($sql);
            
            $ItemBookingDateClashArr = array();
            $ItemClashedDateTimeArr = array();
            
            // $TimeDateArr = $this->Convert_DateTimeArr_To_TimeDateArr($DateTimeArr);
            
            // ## Check if the Item is being booked already in the selected date and time slot
            $AvailableBookingSingleItemID = array();
            $ItemClashCheckArr = array();
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeRangeInfoArr) {
                $numOfTimeRange = count($thisTimeRangeInfoArr);
                for ($i = 0; $i < $numOfTimeRange; $i ++) {
                    $thisStartTime = $thisTimeRangeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeRangeInfoArr[$i]['EndTime'];
                    
                    for ($j = 0; $j < $numOfSingleItem; $j ++) {
                        $thisSingleItemID = $SingleItemIDArr[$j];
                        $ItemClashCheckArr[$thisDate][$thisStartTime][$thisSingleItemID] = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $thisSingleItemID, $thisDate, $thisStartTime, $thisEndTime);
                        
                        if ($ItemClashCheckArr[$thisDate][$thisStartTime][$thisSingleItemID] == 1)
                            $AvailableBookingSingleItemID[$thisDate][$thisStartTime][] = $thisSingleItemID;
                    }
                    
                    if (in_array(false, (array) $ItemClashCheckArr[$thisDate][$thisStartTime]))
                        $ItemClashedDateTimeArr[$thisDate][$thisStartTime] = true;
                }
            }
            
            $InsertBookingArr = array();
            $BookingStatusArr = array(
                1
            ); // check for approved booking records only
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeInfoArr[$i]['EndTime'];
                    $thisTimeSlotID = $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime];
                    
                    // ## skip the date booking if one of the item is N.A. on that date
                    if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)
                        continue;
                        
                        // ## Check if the timeslot is available for the room and items
                    $IsRoomAvailable = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $thisDate, $thisStartTime, $thisEndTime);
                    
                    if ($IsRoomAvailable) {
                        $InsertBookingArr[] = "	(
														'$thisTimeSlotID', '$thisDate', '$thisStartTime', '$thisEndTime', '" . $this->Get_Safe_Sql_Query($BookingRemarks) . "',
														'" . $_SESSION['UserID'] . "', now(), '$ResponsibleUID', 1, '$CancelDayBookingIfOneItemIsNA', 1, 
														'" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now()
													)";
                    }
                }
            }
            
            if (count($InsertBookingArr) == 0) {
                $this->RollBack_Trans();
                $sql = "UNLOCK TABLES";
                $this->db_db_query($sql);
                return false;
            }
            
            $InsertBookingList = implode(',', $InsertBookingArr);
            
            // ## Insert Booking Record
            $sql = "	Insert Into INTRANET_EBOOKING_RECORD		
									(
										PeriodID, Date, StartTime, EndTime, Remark, 
										RequestedBy, RequestDate, ResponsibleUID, BookingStatus, DeleteOtherRelatedIfReject, RecordStatus,
										InputBy, ModifiedBy, DateInput, DateModified
									)
									Values
									$InsertBookingList
								";
            $SuccessArr['Insert_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Update RelatedTo Field
            $FirstBookingID = $this->db_insert_id();
            $sql = "Select max(BookingID) From INTRANET_EBOOKING_RECORD";
            $tmpArr = $this->returnVector($sql);
            $LastBookingID = $tmpArr[0];
            
            /*
             * $sql = "Update INTRANET_EBOOKING_RECORD
             * Set RelatedTo = BookingID
             * Where BookingID Between '$FirstBookingID' And '$LastBookingID'
             * ";
             */
            $sql = "Update INTRANET_EBOOKING_RECORD 
							Set RelatedTo = '$FirstBookingID'
							Where BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Insert Room Booking Details and Item Booking Details
            // Build "StartTime-Date BookingID" Mapping
            $sql = "Select 
								BookingID,
								Date,
								StartTime,
								EndTime
						From
								INTRANET_EBOOKING_RECORD
						Where
								BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $BookingInfoArr = $this->returnArray($sql);
            $numOfBooking = count($BookingInfoArr);
            
            $DateTime_BookingID_MappingArr = array();
            for ($i = 0; $i < $numOfBooking; $i ++) {
                $thisBookingID = $BookingInfoArr[$i]['BookingID'];
                $thisDate = $BookingInfoArr[$i]['Date'];
                $thisStartTime = $BookingInfoArr[$i]['StartTime'];
                
                // ## skip the date booking if one of the item is N.A. on that date
                if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)
                    continue;
                
                $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime] = $thisBookingID;
            }
            
            // ## Since the item / room may not book on ALL the inserted booking dates (due to time clash),
            // ## loop the prepared $DateTimeArr instead of $BookingInfoArr to avoid creating extra booking records
            $arrBookingID = array();
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    
                    // ## skip the date booking if one of the item is N.A. on that date
                    if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemClashedDateTimeArr[$thisDate][$thisStartTime] == true)
                        continue;
                        
                        // Book Room
                        // $RoomNeedApproval[$thisBookingID] = $this->Check_If_Booking_Need_Approval($_SESSION['UserID'], '', $RoomID);
                        // if ($RoomNeedApproval[$thisBookingID])
                    $RoomBookingStatus = LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY;
                    // else
                    // $RoomBookingStatus = 1;
                    $SuccessArr['Insert_Room_Booking_Record'][$thisBookingID] = $this->Insert_Room_Booking_Record($thisBookingID, $RoomID, $RoomBookingStatus);
                    
                    // Book Single Item
                    $thisCanBookSingleItemIDArr = $AvailableBookingSingleItemID[$thisDate][$thisStartTime];
                    if (is_array($thisCanBookSingleItemIDArr) && count($thisCanBookSingleItemIDArr) > 0)
                        $SuccessArr['Insert_Item_Booking_Record'][$thisBookingID] = $this->Insert_Item_Booking_Record($thisBookingID, $thisCanBookSingleItemIDArr, $BookingStatus = '', $RoomBookingStatus);
                        
                        // ## Create a temp booking record
                    if ($FromModule == 'eEnrolment') {
                        $sql = "INSERT INTO INTRANET_EBOOKING_ENROL_EVENT_RELATION (BookingID) VALUES ($thisBookingID)";
                        $this->db_db_query($sql);
                    } else { // Default module is iCalender
                        $sql = "INSERT INTO INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (BookingID) VALUES ($thisBookingID)";
                        $this->db_db_query($sql);
                    }
                    // ## Update the target Booking into invisiable (similar to "Cancel Booking")
                    // $sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($thisBookingID)";
                    // $this->db_db_query($sql);
                    
                    $arrBookingID[] = $thisBookingID;
                }
            }
            
            if (! in_array(false, $SuccessArr)) {
                $this->Commit_Trans();
                // $success = true;
                
                $PATH_WRT_ROOT = "../../";
                include_once ("libinventory.php");
                include_once ("libinterface.php");
                $linterface = new interface_html();
                $linventory = new libinventory();
                
                $strBookingIDs = implode(",", $arrBookingID);
                $sql = "SELECT 
								" . $this->Get_Location_Name_Field("building.", "floor.", "room.") . " 
							FROM 
								INVENTORY_LOCATION_BUILDING AS building 
								INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
								INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
							WHERE 
								building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1 
								AND room.LocationID = '$RoomID'";
                $location_name = $this->returnVector($sql);
                
                $cnt = 0;
                for ($i = 0; $i < sizeof($arrBookingID); $i ++) {
                    $result = $this->Check_All_Related_Booking_Request_Is_Apporved_iCal($arrBookingID[$i]);
                    if ($result) {
                        $cnt ++;
                    }
                }
                
                if ((sizeof($arrBookingID) == 1) && ($cnt == sizeof($arrBookingID))) {
                    global $image_path, $LAYOUT_SKIN;
                    $result_img = "<img border='0' src='" . $image_path . "/" . $LAYOUT_SKIN . "/icon_approve.gif'>" . $Lang['eBooking']['iCal']['FieldTitle']['Approved'];
                } else 
                    if ((sizeof($arrBookingID) == 1) && ($cnt != sizeof($arrBookingID))) {
                        global $image_path, $LAYOUT_SKIN;
                        $result_img = "<img border='0' src='" . $image_path . "/" . $LAYOUT_SKIN . "/icon_wait_approve.gif'>" . $Lang['eBooking']['iCal']['FieldTitle']['Pending'];
                    } else 
                        if ((sizeof($arrBookingID) > 1) && ($cnt == sizeof($arrBookingID))) {
                            global $image_path, $LAYOUT_SKIN;
                            $result_img = "<img border='0' src='" . $image_path . "/" . $LAYOUT_SKIN . "/icon_approve.gif'>" . $Lang['eBooking']['iCal']['FieldTitle']['SomeApproved'];
                        } else 
                            if ((sizeof($arrBookingID) > 1) && ($cnt != sizeof($arrBookingID))) {
                                global $image_path, $LAYOUT_SKIN;
                                $result_img = "<img border='0' src='" . $image_path . "/" . $LAYOUT_SKIN . "/icon_wait_approve.gif'>" . $Lang['eBooking']['iCal']['FieldTitle']['SomePending'];
                            }
                
                $edit_link = '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/script.js"></script>';
                $edit_link .= '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>';
                $edit_link .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />';
                $edit_link .= $linterface->Get_Thickbox_Link('500', '800', "", $Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'], "js_Change_Selected_Location(); return false;", "", $Lang['Btn']['Edit']);
                
                $return_string = $location_name[0] . " [ " . $edit_link . " | <a href='#' onClick='javascript:js_Cancel_Selected_Location()'>" . $Lang['Btn']['Clear'] . "</a> ]<br>" . $result_img . "||" . $strBookingIDs;
            } else {
                $this->RollBack_Trans();
                // $success = false;
                $return_string = "";
            }
            
            $sql = "UNLOCK TABLES";
            $this->db_db_query($sql);
            
            echo $return_string;
        }

        function checkUserCanBookTheFacilityWithTargetDate($FacilityType, $FacilityID, $ParUserID, $targetDate)
        {
            $ArrTemp = $this->getFacilityBookingAvailableDateRange($FacilityType, $FacilityID, $ParUserID);
            $SinceDate = $ArrTemp[0];
            $UntilDate = $ArrTemp[1];
            $ts_SinceDate = strtotime($SinceDate);
            $ts_UntilDate = strtotime($UntilDate);
            
            if ($ts_SinceDate <= strtotime($targetDate)) {
                if ($ts_UntilDate != "") {
                    if (strtotime($targetDate) <= $ts_UntilDate) {
                        $booking_date_pass = 1;
                    } else {
                        $booking_date_pass = 0;
                    }
                } else {
                    $booking_date_pass = 1;
                }
            } else {
                $booking_date_pass = 0;
            }
            
            return $booking_date_pass;
        }

        function Check_All_Related_Booking_Request_Is_Apporved_iCal($booking_id)
        {
            // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = $booking_id";
            // $arrTotalNumOfRoomDetail = $this->returnVector($sql);
            // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = $booking_id";
            // $arrTotalNumOfItemDetail = $this->returnVector($sql);
            // $TotalNumOfBookingResource = $arrTotalNumOfRoomDetail[0] + $arrTotalNumOfItemDetail[0];
            //
            // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_ROOM_BOOKING_DETAILS WHERE BookingID = $booking_id AND BookingStatus = ".LIBEBOOKING_BOOKING_STATUS_APPROVED;
            // $arrNumOfProcessedRoomBooking = $this->returnVector($sql);
            // $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID = $booking_id AND BookingStatus = ".LIBEBOOKING_BOOKING_STATUS_APPROVED;
            // $arrNumOfProcessedItemBooking = $this->returnVector($sql);
            // $TotalNumOfProcessedBooking = $arrNumOfProcessedRoomBooking[0] + $arrNumOfProcessedItemBooking[0];
            //
            // if($TotalNumOfBookingResource == $TotalNumOfProcessedBooking)
            // {
            // return true;
            // }
            $sql = "SELECT COUNT(*),COUNT(IF(BookingStatus = " . LIBEBOOKING_BOOKING_STATUS_APPROVED . ",1,NULL))  FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id'";
            $TotalNumOfBookingResource = $this->returnArray($sql);
            
            if ($TotalNumOfBookingResource[0][0] == $TotalNumOfBookingResource[0][1]) {
                return true;
            }
            
            return false;
        }

        function New_Room_Reserve($RoomID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr, $iCalEventArr, $FolderPath = '', $CustRemarkAry = array(), $BookingEvent = '', $ApplyDoorAccess = 0, $BookingCategoryID = '')
        {
            global $plugin, $PATH_WRT_ROOT;
            
            $SuccessArr = array();
            $numOfSingleItem = count($SingleItemIDArr);

            $CancelDayBookingIfOneItemIsNA = ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1) ? 1 : 0;
            
            // $this->Start_Trans_For_Lock_Table_Procedure();
            $this->Start_Trans();
            // Lock tables
            $sql = "LOCK TABLES 
								INTRANET_EBOOKING_RECORD WRITE
								INTRANET_EBOOKING_BOOKING_DETAILS WRITE
								";
            $this->db_db_query($sql);
            
            $ItemBookingDateClashArr = array();
            $ItemClashedDateTimeArr = array();
            
            // ## Check if the Item is being booked already in the selected date and time slot
            $AvailableBookingSingleItemID = array();
            $IsInAvailablePeriod = array();
            
            // ## check whether the period lie in available booking period
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeRangeInfoArr) {
                $numOfTimeRange = count($thisTimeRangeInfoArr);
                
                for ($i = 0; $i < $numOfTimeRange; $i ++) {
                    $thisStartTime = $thisTimeRangeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeRangeInfoArr[$i]['EndTime'];
                    
                    for ($j = 0; $j < $numOfSingleItem; $j ++) {
                        $thisSingleItemID = $SingleItemIDArr[$j];
                        
                        $IsInAvailablePeriod[$thisDate][$thisStartTime][$thisSingleItemID] = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $thisSingleItemID, $thisDate, $thisStartTime, $thisEndTime, '', '', 1, 1);
                        
                        if ($IsInAvailablePeriod[$thisDate][$thisStartTime][$thisSingleItemID] == 1)
                            $AvailableBookingSingleItemID[$thisDate][$thisStartTime][] = $thisSingleItemID;
                    }
                    
                    if (in_array(false, (array) $IsInAvailablePeriod[$thisDate][$thisStartTime]))
                        $ItemNotInAvailablePeriodArr[$thisDate][$thisStartTime] = true;
                }
            }

            $InsertBookingArr = array();
            $BookingStatusArr = array(
                1
            ); // check for approved booking records only
            
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeInfoArr[$i]['EndTime'];
                    $thisTimeSlotID = $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime];        // Is this still used?
                    $_thisTimeSlotID = $thisTimeInfoArr[$i]['TimeSlotID'];
                    
                    // ## skip the date booking if one of the item is N.A. on that date
                    if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemNotInAvailablePeriodArr[$thisDate][$thisStartTime] == true)
                        continue;
                        
                        // ### Reject existing booking
                        // # get the existing BookingID first
                        // $DateTimeArr = array();
                        // $DateTimeArr[$thisDate][0]['StartTime'] = $thisStartTime;
                        // $DateTimeArr[$thisDate][0]['EndTime'] = $thisEndTime;
                        //
                        // $ExistingRoomBookingArr = $this->Get_Room_Booking_In_One_Day_Of_Multiple_TimeSlot((array)$RoomID, $DateTimeArr, 1);
                        //
                        // $thisRoomBookingArr = $ExistingRoomBookingArr[$RoomID][$thisDate];
                        // $existingRoomBookingID = Get_Array_By_Key($thisRoomBookingArr, "BookingID");
                        //
                        // if(count($existingRoomBookingID))
                        // {
                        // $sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = -1 WHERE BookingID IN (".implode(",",(array)$existingRoomBookingID).")";
                        //
                        // $SuccessArr['RejectPreviousApprovedRoomBooking'] = $this->db_db_query($sql);
                        //
                        // ## Email User that the previous booking is rejected due to the reservation by eBooking Admin
                        // $this->Email_Booking_Result($existingRoomBookingID);
                        // }
                        // ### Reject existing booking end
                        
                    // Reject other overlapped booking record
                    $thisDateTimeArr = $this->Build_DateTimeArr($thisDate, $thisStartTime, $thisEndTime);
                    $result['RejectPreviousApprovedRoomBooking'] = $this->Reject_Approved_Or_Pending_Booking_By_DateTimeArr(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $thisDateTimeArr, $ExcludeBookingID = array(), $sendEmail = true, $returnEmailBookingIdAry = false, true);
                    
                    // ## Check if the timeslot is available for the room and items
                    // $IsRoomAvailable = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $RoomID, $thisDate, $thisStartTime, $thisEndTime);
                    $IsRoomAvailable = true; // # As Reserve is TOP priority, so default set true to pass the room available checking
                    if ($IsRoomAvailable) {
                        $InsertBookingArr[] = "	(
														'$thisTimeSlotID', '$thisDate', '$thisStartTime', '$thisEndTime', '" . $this->Get_Safe_Sql_Query($BookingRemarks) . "', '$FolderPath',
														'" . $_SESSION['UserID'] . "', now(), '$ResponsibleUID', 1, '$CancelDayBookingIfOneItemIsNA', 1, '$ApplyDoorAccess',
														1, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now(), '" . $this->Get_Safe_Sql_Query($BookingEvent) . "', '$_thisTimeSlotID'
													)";
                    }
                }
            }
            
            if (count($InsertBookingArr) == 0) {
                $this->RollBack_Trans();
                $sql = "UNLOCK TABLES";
                $this->db_db_query($sql);
                return false;
            }
            
            $InsertBookingList = implode(',', $InsertBookingArr);
            
            // ## Insert Booking Record
            $sql = "	Insert Into INTRANET_EBOOKING_RECORD		
									(
										PeriodID, Date, StartTime, EndTime, Remark, Attachment, 
										RequestedBy, RequestDate, ResponsibleUID, BookingStatus, DeleteOtherRelatedIfReject, IsReserve, ApplyDoorAccess, 
										RecordStatus, InputBy, ModifiedBy, DateInput, DateModified, Event, TimeSlotID
									)
									Values
									$InsertBookingList
								";
            $SuccessArr['Insert_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Update RelatedTo Field
            $FirstBookingID = $this->db_insert_id();
            $sql = "Select max(BookingID) From INTRANET_EBOOKING_RECORD";
            $tmpArr = $this->returnVector($sql);
            $LastBookingID = $tmpArr[0];
            
            $sql = "Update INTRANET_EBOOKING_RECORD 
							Set RelatedTo = '$FirstBookingID'
							Where BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Booking Category Relation
            
            if ($BookingCategoryID != '') {
                
                if ($FirstBookingID != $LastBookingID) {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID Between '$FirstBookingID' And '$LastBookingID'";
                } else {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID = '$FirstBookingID'";
                }
                $AddedBookingIDAry = $this->ReturnResultSet($sql);
                
                $insertRowsAry = array();
                foreach ($AddedBookingIDAry as $_BookingID) {
                    
                    $insertRowsAry[] .= "( '" . $BookingCategoryID . "', '" . $_BookingID['BookingID'] . "', now(),'" . $_SESSION['UserID'] . "',now(),'" . $_SESSION['UserID'] . "' )";
                }
                
                $sql = "INSERT INTO INTRANET_EBOOKING_RECORD_CATEGORY_RELATION 
								(CategoryID,BookingID,DateInput,InputBy,DateModified,ModifiedBy) 
							VALUES 
								" . implode(',', $insertRowsAry) . "
							";
                $this->db_db_query($sql);
            }
            
            // add cust remarks
            if ($SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] && ! empty($CustRemarkAry)) {
                $sql = "delete from INTRANET_EBOOKING_RECORD_CUST_REMARK where BookingID='$FirstBookingID'";
                $this->db_db_query($sql);
                
                foreach ($CustRemarkAry as $fields[] => $data[]);
                $fields_str = implode(",", $fields);
                foreach ($data as $k => $d)
                    $data_str .= ",'" . $this->Get_Safe_Sql_Query($d) . "'";
                
                $sql = "insert into INTRANET_EBOOKING_RECORD_CUST_REMARK ";
                $sql .= " (BookingID, $fields_str) values ";
                $sql .= "('$FirstBookingID'" . $data_str . ")";
                $this->db_db_query($sql);
            }
            
            // ## Insert Room Booking Details and Item Booking Details
            // Build "StartTime-Date BookingID" Mapping
            $sql = "Select 
								BookingID,
								Date,
								StartTime,
								EndTime
						From
								INTRANET_EBOOKING_RECORD
						Where
								BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $BookingInfoArr = $this->returnArray($sql);
            $numOfBooking = count($BookingInfoArr);
            
            $DateTime_BookingID_MappingArr = array();
            for ($i = 0; $i < $numOfBooking; $i ++) {
                $thisBookingID = $BookingInfoArr[$i]['BookingID'];
                $thisDate = $BookingInfoArr[$i]['Date'];
                $thisStartTime = $BookingInfoArr[$i]['StartTime'];
                
                // ## skip the date booking if one of the item is N.A. on that date
                if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemNotInAvailablePeriodArr[$thisDate][$thisStartTime] == true)
                    continue;
                
                $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime] = $thisBookingID;
            }
            
            // ## Since the item / room may not book on ALL the inserted booking dates (due to time clash),
            // ## loop the prepared $DateTimeArr instead of $BookingInfoArr to avoid creating extra booking records
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    
                    // ## skip the date booking if one of the item is N.A. on that date
                    if ($SettingsArr['CancelDayBookingIfOneItemIsNA'] == 1 && $ItemNotInAvailablePeriodArr[$thisDate][$thisStartTime] == true)
                        continue;
                        
                        // Book Room
                    $RoomBookingStatus = 1;
                    $SuccessArr['Insert_Room_Booking_Record'][$thisBookingID] = $this->Insert_Room_Booking_Record($thisBookingID, $RoomID, $RoomBookingStatus);
                    
                    // Book Single Item
                    $thisCanBookSingleItemIDArr = $AvailableBookingSingleItemID[$thisDate][$thisStartTime];
                    
                    if (is_array($thisCanBookSingleItemIDArr) && count($thisCanBookSingleItemIDArr) > 0) {
                        $SuccessArr['Insert_Item_Booking_Record'][$thisBookingID] = $this->Insert_Item_Booking_Record($thisBookingID, $thisCanBookSingleItemIDArr, $RoomBookingStatus);
                        
                        // # found existing approved item
                        // $DateTimeArr = array();
                        // $DateTimeArr[$thisDate][0]['StartTime'] = $thisTimeInfoArr[$i]['StartTime'];
                        // $DateTimeArr[$thisDate][0]['EndTime'] = $thisTimeInfoArr[$i]['EndTime'];
                        //
                        // $ExistingItemBookingArr = $this->Get_Item_Booking_In_One_Day_Of_Multiple_TimeSlot($thisCanBookSingleItemIDArr, $DateTimeArr, 1);
                        //
                        // foreach($ExistingItemBookingArr as $thisItemID => $DateBookingArr)
                        // {
                        // $thisBookingArr = $DateBookingArr[$thisDate];
                        // $existingBookingID = Get_Array_By_Key($thisBookingArr, "BookingID");
                        // $existingBookingID = array_diff($existingBookingID, (array)$thisBookingID); // take away current booking
                        //
                        // if(count($existingBookingID)==0) continue;
                        //
                        // $sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = -1 WHERE BookingID IN (".implode(",",(array)$existingBookingID).")";
                        //
                        // $SuccessArr['RejectPreviousApprovedItemBooking'] = $this->db_db_query($sql);
                        //
                        // ## Email User that the previous booking is rejected due to the reservation by eBooking Admin
                        // $this->Email_Booking_Result($existingBookingID);
                        // }
                        // Reject other overlapped booking record
                        $thisDateTimeArr = $this->Build_DateTimeArr($thisDate, $thisStartTime, $thisEndTime);
                        $result['RejectPreviousApprovedItemBooking'] = $this->Reject_Approved_Or_Pending_Booking_By_DateTimeArr(LIBEBOOKING_FACILITY_TYPE_ITEM, $thisCanBookSingleItemIDArr, $thisDateTimeArr, $thisBookingID, $sendEmail = true, $returnEmailBookingIdAry = false, true);
                    }
                    
                    // # check if we need to create a iCal event or not
                    if (sizeof($iCalEventArr) > 0) {
                        
                        include_once ("icalendar.php");
                        $iCal = new icalendar();
                        
                        $booking_date = $thisDate;
                        $start_time = $thisTimeInfoArr[$i]['StartTime'];
                        $end_time = $thisTimeInfoArr[$i]['EndTime'];
                        
                        $event_date_time = $booking_date . " " . $start_time;
                        
                        $start_timestamp = strtotime($booking_date . " " . $start_time) . "<BR>";
                        $end_timestamp = strtotime($booking_date . " " . $end_time);
                        
                        $duration = ($end_timestamp - $start_timestamp) / 60;
                        
                        $OwnerID = $iCalEventArr[0]['Owner'];
                        $iCalEventTitle = $iCalEventArr[0]['EventTitle'];
                        $BookingRemarks = $iCalEventArr[0]['EventDesc'];
                        $location = $iCalEventArr[0]['Location'];
                        $calID = $iCalEventArr[0]['EventCalID'];
                        
                        $isImportant = "";
                        $isAllDay = "";
                        $access = "P";
                        $url = "";
                        
                        if ($calID == '') {
                            // get the CalendarID for the targer user
                            $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                            $TargetCalendarID = $iCal->returnVector($sql);
                            $calID = $TargetCalendarID[0];
                            
                            // Insert My Calendar if user not yet have My Calendar
                            if ($calID == "" || $calID == 0) {
                                $iCal->insertMyCalendar();
                                $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                                $TargetCalendarID = $iCal->returnVector($sql);
                                $calID = $TargetCalendarID[0];
                            }
                        }
                        // # if the booking is auto approve, create the event directly
                        
                        $fieldname = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
                        $fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID";
                        
                        $fieldvalue = "'".$OwnerID . "', '$event_date_time', NOW(), NOW(), $duration, ";
                        $fieldvalue .= "'$isImportant', '$isAllDay', '$access', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventTitle)) . "', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($BookingRemarks)) . "', ";
                        $fieldvalue .= "'" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)) . "', '$url', '$calID'";
                        
                        $sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
                        $tempSuccess = $this->db_db_query($sql);
                        
                        if ($tempSuccess) {
                            $iCalEventID = $this->db_insert_id();
                            
                            global $schoolNameAbbrev;
                            $uid = $OwnerID . "-{$calID}-{$iCalEventID}@{$schoolNameAbbrev}.tg";
                            $sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$iCalEventID'";
                            $this->db_db_query($sql);
                            
                            $sql = "INSERT INTO INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (BookingID, EventID) VALUES ($thisBookingID, $iCalEventID)";
                            $this->db_db_query($sql);
                        }
                    }
                }
            }
            
            if ($plugin['door_access']) {
                include_once ($PATH_WRT_ROOT . "includes/libebooking_door_access.php");
                $lebooking_door_access = new libebooking_door_access();
                
                $successAry['updateDoorAccess'] = $lebooking_door_access->updateBookingDoorAccessRecord($FirstBookingID);
            }
            
            // $this->RollBack_Trans();
            // die();
            
            if (! in_multi_array(false, $SuccessArr)) {
                $this->Commit_Trans();
                $success = true;
            } else {
                $this->RollBack_Trans();
                $success = false;
            }
            
            $sql = "UNLOCK TABLES";
            $this->db_db_query($sql);
            
            return $success;
        }

        function New_Item_Reserve($ItemID, $DateTimeArr, $StartTime_TimeSlotID_Mapping_Arr, $BookingRemarks, $ResponsibleUID, $iCalEventArr, $FolderPath = '', $CustRemarkAry = array(), $BookingEvent = '', $BookingCategoryID = '')
        {
            $DateArr = array_keys($DateTimeArr);
            $ItemIDArr = (is_array($ItemID)) ? $ItemID : explode(",", $ItemID);
            // $AvailableBookingPeriodArr = $this->getAllAvailablePeriodOfUser(LIBEBOOKING_FACILITY_TYPE_ITEM, $ItemID, $_SESSION['UserID'], $DateArr);
            // $AvailableBookingPeriodArr = BuildMultiKeyAssoc($AvailableBookingPeriodArr, array("RecordDate", "PeriodID"));
            
            $SuccessArr = array();
            // $this->Start_Trans_For_Lock_Table_Procedure();
            $this->Start_Trans();
            // Lock tables
            $sql = "LOCK TABLES 
								INTRANET_EBOOKING_RECORD WRITE
								INTRANET_EBOOKING_BOOKING_DETAILS WRITE";
            $this->db_db_query($sql);
            
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeInfoArr[$i]['EndTime'];
                    $thisTimeSlotID = $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime];
                    
                    // $sql = "SELECT
                    // record.BookingID
                    // FROM
                    // INTRANET_EBOOKING_RECORD AS record
                    // INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS item_record ON (record.BookingID = item_record.BookingID AND item_record.FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."')
                    // WHERE
                    // record.Date = '$thisDate'
                    // AND (
                    // (record.StartTime >= '$thisStartTime' And record.StartTime < '$thisEndTime')
                    // Or
                    // (record.EndTime > '$thisStartTime' And record.EndTime <= '$thisEndTime')
                    // Or
                    // (record.StartTime <= '$thisStartTime' And record.EndTime >= '$thisEndTime')
                    // )
                    // AND item_record.FacilityID IN ( ".implode(",",$ItemIDArr)." )";
                    // $arrExistItemBooking = $this->returnVector($sql);
                    //
                    // if(sizeof($arrExistItemBooking) > 0)
                    // {
                    // $sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = -1 WHERE BookingID IN (".implode(",",$arrExistItemBooking).")";
                    // $this->db_db_query($sql);
                    //
                    // for($j=0; $j<sizeof($arrExistItemBooking); $j++)
                    // {
                    // ## Email User that the previous booking is rejected due to the reservation by eBooking Admin
                    // $this->Email_Booking_Result($arrExistItemBooking[$j]);
                    // }
                    // }
                    
                    $thisDateTimeArr = $this->Build_DateTimeArr($thisDate, $thisStartTime, $thisEndTime);
                    $SuccessArr['RejectPreviousApprovedItemBooking'] = $this->Reject_Approved_Or_Pending_Booking_By_DateTimeArr(LIBEBOOKING_FACILITY_TYPE_ITEM, $ItemIDArr, $thisDateTimeArr, $ExcludeBookingID = array(), $sendEmail = true, $returnEmailBookingIdAry = false, true);
                }
            }
            
            $InsertBookingArr = array();
            $BookingStatusArr = array(
                1
            ); // check for approved booking records only
            
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisEndTime = $thisTimeInfoArr[$i]['EndTime'];
                    $thisTimeSlotID = $StartTime_TimeSlotID_Mapping_Arr[$thisStartTime];
                    $_thisTimeSlotID = $thisTimeInfoArr[$i]['TimeSlotID'];
                    
                    // ## Check if the item is current available in the selected timeslot
                    // if (!isset($thisCurItemBooking[$thisStartTime]))
                    // $thisCurItemBooking[$thisStartTime] = $this->Get_Item_Booking_In_A_TimeRange_Of_Multiple_Date($thisStartTime, $thisEndTime, $DateArr, array($ItemID), $ReturnAsso=0, $BookingStatusArr);
                    // $thisCurBookingArr = $thisCurItemBooking[$thisStartTime][$ItemID][$thisDate];
                    // if ($thisCurBookingArr=='') // no booking records yet
                    // $thisCurBookingArr = array();
                    
                    // $IsItemAvailable = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $ItemID, $thisDate, $thisStartTime, $thisEndTime, $AvailableBookingPeriodArr, $thisCurBookingArr);
                    // $IsInAvailablePeriod[$thisDate][$thisStartTime][$thisSingleItemID] = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $thisSingleItemID, $thisDate, $thisStartTime, $thisEndTime, '', '', 1);
                    //
                    // if ($IsInAvailablePeriod[$thisDate][$thisStartTime][$thisSingleItemID] == 1)
                    // $AvailableBookingSingleItemID[$thisDate][$thisStartTime][] = $thisSingleItemID;
                    
                    // $IsItemAvailable = $this->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ITEM, $ItemID, $thisDate, $thisStartTime, $thisEndTime, '', '', 1);
                    $IsItemAvailable = true; // # As Reserve is TOP priority, so default set true to pass the item available checking
                                             
                    // if ($numOfCurItemBooking == 0)
                    if ($IsItemAvailable) {
                        $InsertBookingArr[] = "	(
														'$thisTimeSlotID', '$thisDate', '$thisStartTime', '$thisEndTime', '" . $this->Get_Safe_Sql_Query($BookingRemarks) . "','$FolderPath',
														'" . $_SESSION['UserID'] . "', now(), '$ResponsibleUID', 1, 1,
														1, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now(), '" . $this->Get_Safe_Sql_Query($BookingEvent) . "','$_thisTimeSlotID'
													)";
                    }
                }
            }
            
            if (count($InsertBookingArr) == 0) {
                $this->RollBack_Trans();
                $sql = "UNLOCK TABLES";
                $this->db_db_query($sql);
                return false;
            }
            
            $InsertBookingList = implode(',', $InsertBookingArr);
            
            // ## Insert Booking Record
            $sql = "Insert Into INTRANET_EBOOKING_RECORD		
								(
									PeriodID, Date, StartTime, EndTime, Remark, Attachment,
									RequestedBy, RequestDate, ResponsibleUID, BookingStatus, RecordStatus,
									IsReserve, InputBy, ModifiedBy, DateInput, DateModified, Event, TimeSlotID
								)
								Values
								$InsertBookingList";
            $SuccessArr['Insert_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Update RelatedTo Field
            $FirstBookingID = $this->db_insert_id();
            $sql = "Select max(BookingID) From INTRANET_EBOOKING_RECORD";
            $tmpArr = $this->returnVector($sql);
            $LastBookingID = $tmpArr[0];
            
            /*
             * $sql = "Update INTRANET_EBOOKING_RECORD
             * Set RelatedTo = BookingID
             * Where BookingID Between '$FirstBookingID' And '$LastBookingID'
             * ";
             */
            $sql = "Update INTRANET_EBOOKING_RECORD 
							Set RelatedTo = '$FirstBookingID'
							Where BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] = $this->db_db_query($sql);
            
            // ## Booking Category Relation
            
            if ($BookingCategoryID != '') {
                
                if ($FirstBookingID != $LastBookingID) {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID Between '$FirstBookingID' And '$LastBookingID'";
                } else {
                    $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD Where BookingID = '$FirstBookingID'";
                }
                $AddedBookingIDAry = $this->ReturnResultSet($sql);
                
                $insertRowsAry = array();
                foreach ($AddedBookingIDAry as $_BookingID) {
                    
                    $insertRowsAry[] .= "( '" . $BookingCategoryID . "', '" . $_BookingID['BookingID'] . "', now(),'" . $_SESSION['UserID'] . "',now(),'" . $_SESSION['UserID'] . "' )";
                }
                
                $sql = "INSERT INTO INTRANET_EBOOKING_RECORD_CATEGORY_RELATION 
								(CategoryID,BookingID,DateInput,InputBy,DateModified,ModifiedBy) 
							VALUES 
								" . implode(',', $insertRowsAry) . "
							";
                $this->db_db_query($sql);
            }
            
            // add cust remarks
            if ($SuccessArr['Update_RelatedTo_In_INTRANET_EBOOKING_RECORD'] && ! empty($CustRemarkAry)) {
                $sql = "delete from INTRANET_EBOOKING_RECORD_CUST_REMARK where BookingID='$FirstBookingID'";
                $this->db_db_query($sql);
                
                foreach ($CustRemarkAry as $fields[] => $data[]);
                $fields_str = implode(",", $fields);
                foreach ($data as $k => $d)
                    $data_str .= ",'" . $this->Get_Safe_Sql_Query($d) . "'";
                
                $sql = "insert into INTRANET_EBOOKING_RECORD_CUST_REMARK ";
                $sql .= " (BookingID, $fields_str) values ";
                $sql .= "('$FirstBookingID'" . $data_str . ")";
                $this->db_db_query($sql);
            }
            
            // ## Insert Item Booking Details
            // Build "StartTime-Date BookingID" Mapping
            $sql = "Select 
								BookingID,
								Date,
								StartTime,
								EndTime
						From
								INTRANET_EBOOKING_RECORD
						Where
								BookingID Between '$FirstBookingID' And '$LastBookingID'
						";
            $BookingInfoArr = $this->returnArray($sql);
            $numOfBooking = count($BookingInfoArr);
            
            $DateTime_BookingID_MappingArr = array();
            for ($i = 0; $i < $numOfBooking; $i ++) {
                $thisBookingID = $BookingInfoArr[$i]['BookingID'];
                $thisDate = $BookingInfoArr[$i]['Date'];
                $thisStartTime = $BookingInfoArr[$i]['StartTime'];
                
                $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime] = $thisBookingID;
            }
            
            // ## Since the item / room may not book on ALL the inserted booking dates (due to time clash),
            // ## loop the prepared $DateTimeArr instead of $BookingInfoArr to avoid creating extra booking records
            foreach ((array) $DateTimeArr as $thisDate => $thisTimeInfoArr) {
                $numOfTime = count($thisTimeInfoArr);
                for ($i = 0; $i < $numOfTime; $i ++) {
                    $thisStartTime = $thisTimeInfoArr[$i]['StartTime'];
                    $thisBookingID = $DateTime_BookingID_MappingArr[$thisDate][$thisStartTime];
                    
                    // $ItemNeedApprovalArr[$thisBookingID] = $this->Check_If_Booking_Need_Approval($_SESSION['UserID'], $ItemID, '');
                    // if ($ItemNeedApprovalArr[$thisBookingID])
                    // $ItemBookingStatus = 0;
                    // else
                    $ItemBookingStatus = 1;
                    
                    // Book Single Item
                    $SuccessArr['Insert_Item_Booking_Record'][$thisBookingID] = $this->Insert_Item_Booking_Record($thisBookingID, $ItemIDArr, $ItemBookingStatus);
                    
                    // # check if we need to create a iCal event or not
                    if (sizeof($iCalEventArr) > 0) {
                        
                        include_once ("icalendar.php");
                        $iCal = new icalendar();
                        
                        $booking_date = $thisDate;
                        $start_time = $thisTimeInfoArr[$i]['StartTime'];
                        $end_time = $thisTimeInfoArr[$i]['EndTime'];
                        
                        $event_date_time = $booking_date . " " . $start_time;
                        
                        $start_timestamp = strtotime($booking_date . " " . $start_time) . "<BR>";
                        $end_timestamp = strtotime($booking_date . " " . $end_time);
                        
                        $duration = ($end_timestamp - $start_timestamp) / 60;
                        
                        $OwnerID = $iCalEventArr[0]['Owner'];
                        $iCalEventTitle = $iCalEventArr[0]['EventTitle'];
                        $BookingRemarks = $iCalEventArr[0]['EventDesc'];
                        $location = $iCalEventArr[0]['Location'];
                        $calID = $iCalEventArr[0]['EventCalID'];
                        
                        $isImportant = "";
                        $isAllDay = "";
                        $access = "P";
                        $url = "";
                        
                        if ($calID == '') {
                            // get the CalendarID for the targer user
                            $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                            $TargetCalendarID = $iCal->returnVector($sql);
                            $calID = $TargetCalendarID[0];
                            
                            // Insert My Calendar if user not yet have My Calendar
                            if ($calID == "" || $calID == 0) {
                                $iCal->insertMyCalendar();
                                $sql = "SELECT MIN(CalID) FROM CALENDAR_CALENDAR WHERE Owner = '$OwnerID'";
                                $TargetCalendarID = $iCal->returnVector($sql);
                                $calID = $TargetCalendarID[0];
                            }
                        }
                        // # if the booking is auto approve, create the event directly
                        $fieldname = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
                        $fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID";
                        
                        $fieldvalue = "'".$OwnerID . "', '$event_date_time', NOW(), NOW(), $duration, ";
                        $fieldvalue .= "'$isImportant', '$isAllDay', '$access', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventTitle)) . "', '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($BookingRemarks)) . "', ";
                        $fieldvalue .= "'" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)) . "', '$url', '$calID'";
                        
                        $sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
                        $tempSuccess = $this->db_db_query($sql);
                        
                        if ($tempSuccess) {
                            $iCalEventID = $this->db_insert_id();
                            
                            global $schoolNameAbbrev;
                            $uid = $OwnerID . "-{$calID}-{$iCalEventID}@{$schoolNameAbbrev}.tg";
                            $sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$iCalEventID'";
                            $iCal->db_db_query($sql);
                            
                            $sql = "INSERT INTO INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (BookingID, EventID) VALUES ('$thisBookingID', '$iCalEventID')";
                            $this->db_db_query($sql);
                        }
                    }
                }
            }
            
            if (! in_multi_array(false, $SuccessArr)) {
                $this->Commit_Trans();
                $success = true;
            } else {
                $this->RollBack_Trans();
                $success = false;
            }
            
            $sql = "UNLOCK TABLES";
            $this->db_db_query($sql);
            
            return $success;
        }

        function Get_Settings_FollowUpGroup_Sql($Keyword = '')
        {
            global $Lang;
            $cond_keyword = '';
            if ($Keyword != '') {
                $cond_keyword .= " 	And 
										(
											follow_up.GroupName Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
											Or
											follow_up.Description Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
										)
									";
            }
            
            $sql = "Select
								CONCAT('<a href=\"javascript:js_Go_Update_Group_Info(', follow_up.GroupID, ')\" class=\"tablelink\" title=\"{$Lang['eBooking']['Settings']['ManagementGroup']['EditGroup']}\">', follow_up.GroupName, '</a>'),
								follow_up.Description, 
								CONCAT('<br><table border=\"0\" width=\"10px\" cellpadding=\"0\" cellspacing=\"0\"><tr height=\"10px\" ><td valign=\"center\" style=\"background-color:', follow_up.GroupColor, '\"</td></tr></table>'),
								CONCAT('<a href=\"javascript:js_Go_View_Member_List(', follow_up.GroupID, ')\" class=\"tablelink\" title=\"{$Lang['eBooking']['Settings']['ManagementGroup']['ViewMemberList']}\">', Count(fgm.UserID), '</a>'),
								CONCAT('<input type=\"checkbox\" name=\"GroupIDArr[]\" value=\"', follow_up.GroupID ,'\">') 
						From
								INTRANET_EBOOKING_FOLLOWUP_GROUP as follow_up
								/*LEFT Join
								INTRANET_USER as iu
								On follow_up.ModifiedBy = iu.UserID*/
								Left Join
								INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER as fgm
								On follow_up.GroupID = fgm.GroupID
						Where
								1
								$cond_keyword
						Group By
								follow_up.GroupID
	        			";
            return $sql;
        }

        function Is_Follow_Up_Group_Name_Valid($GroupName, $GroupID)
        {
            $cond_excludeObjectID = '';
            if ($GroupID != '')
                $cond_excludeObjectID = " And GroupID != '" . $GroupID . "' ";
            
            $sql = "Select
								GroupID
						From
								INTRANET_EBOOKING_FOLLOWUP_GROUP
						Where
								GroupName = '" . $this->Get_Safe_Sql_Query($GroupName) . "'
								$cond_excludeObjectID
						";
            $resultSet = $this->returnVector($sql);
            
            if (count($resultSet) == 0)
                return 1;
            else
                return 0;
        }

        function Get_Settings_FollowUpGroup_MemberList_Sql($GroupID, $Keyword = '')
        {
            global $Lang;
            
            // $namefield = getNameFieldByLang("iu.");
            
            $cond_keyword = '';
            if ($Keyword != '') {
                $cond_keyword .= " 	And 
										(
											iu.EnglishName Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
											Or
											iu.ChineseName Like '%" . $this->Get_Safe_Sql_Like_Query($Keyword) . "%'
										)
									";
            }
            
            $sql = "SELECT
								IF(iu.EnglishName IS NULL, CONCAT(iau.EnglishName,'<span class=\"red\">^</span>'), iu.EnglishName ) as EnglishName ,
	        	                IF(iu.ChineseName IS NULL, CONCAT(iau.ChineseName,'<span class=\"red\">^</span>'), iu.ChineseName ) as ChineseName ,
								CONCAT('<input type=\"checkbox\" name=\"DeleteUserIDArr[]\" value=\"', fgm.UserID ,'\">')
						FROM
								INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER AS fgm
								LEFT JOIN
								INTRANET_USER AS iu
								ON fgm.UserID = iu.UserID
            	        	    LEFT JOIN
								INTRANET_ARCHIVE_USER AS iau
								ON fgm.UserID = iau.UserID
						WHERE
								fgm.GroupID = '" . $GroupID . "'
								$cond_keyword
						GROUP BY
								fgm.UserID
	        			";
            return $sql;
        }

        function Get_Available_FollowUp_User($GroupID, $SearchUserLogin = '', $ExcludeUserIDArr = '')
        {
            if (! is_array($ExcludeUserIDArr))
                $ExcludeUserIDArr = array();
            
            $ExcludeUserIDArr = array_remove_empty($ExcludeUserIDArr);
            if (is_array($ExcludeUserIDArr) && count($ExcludeUserIDArr) > 0) {
                $excludeUserList = implode(',', $ExcludeUserIDArr);
                $conds_excludeUserID = " And UserID Not In ($excludeUserList) ";
            }
            
            // $MemberInfoArr = $this->Get_Member_List();
            $sql = "Select
								iu.UserID,
								" . getNameFieldWithClassNumberByLang('iu.') . "
						From
								INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER as fgm
								Inner Join
								INTRANET_USER as iu
								On (fgm.UserID = iu.UserID)
						Where
								fgm.GroupID = '$GroupID'";
            $MemberInfoArr = $this->returnArray($sql, 2);
            $MemberUserIDArr = Get_Array_By_Key($MemberInfoArr, 'UserID');
            if (count($MemberUserIDArr) > 0) {
                $MemberList = implode(',', $MemberUserIDArr);
                $conds_excludeMember = " And UserID Not In ($MemberList) ";
            }
            
            if ($SearchUserLogin != '')
                $conds_userlogin = " And UserLogin Like '%$SearchUserLogin%' ";
            
            $name_field = getNameFieldByLang();
            $sql = "Select
								UserID,
								$name_field as UserName,
								UserLogin
						From
								INTRANET_USER
						Where
								RecordStatus = 1
								And
								RecordType = 1
								$conds_excludeUserID
								$conds_excludeMember
								$conds_userlogin
						Order By
								EnglishName
						";
            $returnArr = $this->returnArray($sql);
            return $returnArr;
        }

        function Get_Invalidate_FollowUp_Member_Selection($GroupID, $SelectionList)
        {
            $InvalidSelectionArr = array();
            
            $SelectionArr = explode(',', $SelectionList);
            $SelectionArr = array_remove_empty($SelectionArr);
            $numOfSelection = count($SelectionArr);
            if ($numOfSelection > 0) {
                // $GroupMemberInfoArr = $this->Get_Member_List();
                $sql = "Select
								iu.UserID,
								" . getNameFieldWithClassNumberByLang('iu.') . "
						From
								INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER as fgm
								Inner Join
								INTRANET_USER as iu
								On (fgm.UserID = iu.UserID)
						Where
								fgm.GroupID = '$GroupID'";
                $GroupMemberInfoArr = $this->returnArray($sql, 2);
                
                $GroupMemberUserIDArr = Get_Array_By_Key($GroupMemberInfoArr, 'UserID');
                
                for ($i = 0; $i < $numOfSelection; $i ++) {
                    $thisSelection = $SelectionArr[$i];
                    $thisUserIDArr = $this->Get_User_Array_From_Selection(array(
                        $thisSelection
                    ));
                    
                    // $thisIsMemberUserIDArr contains the UserID which is member and is in the user selection => which is an invalid selection
                    $thisIsMemberUserIDArr = array_intersect($GroupMemberUserIDArr, $thisUserIDArr);
                    
                    if (count($thisIsMemberUserIDArr))
                        $InvalidSelectionArr[] = $thisSelection;
                }
            }
            
            $InvalidSelectionList = implode(',', $InvalidSelectionArr);
            return $InvalidSelectionList;
        }

        function Add_FollowUp_Member($GroupID, $TargetMemberArr)
        {
            $TargetMemberArr = array_remove_empty($TargetMemberArr);
            $UserIDArr = $this->Get_User_Array_From_Selection($TargetMemberArr);
            $numOfUser = count($UserIDArr);
            
            if ($numOfUser > 0) {
                $InsertValueArr = array();
                for ($i = 0; $i < $numOfUser; $i ++) {
                    $thisUserID = $UserIDArr[$i];
                    
                    $InsertValueArr[] = "('" . $thisUserID . "', '" . $GroupID . "', NULL, NULL, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', now(), now())";
                }
                
                $insertValueList = implode(', ', $InsertValueArr);
                
                $sql = "Insert Into INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER
								(UserID, GroupID, RecordType, RecordStatus, InputBy, ModifiedBy, DateInput, DateModified)
							Values
								$insertValueList
							";
                $success = $this->db_db_query($sql);
            }
            
            return $success;
        }

        function Get_All_FollowUp_Group($returnAsso = 0)
        {
            $sql = "Select
								GroupID,
								GroupName,
								Description
						From
								INTRANET_EBOOKING_FOLLOWUP_GROUP
						Order By
								GroupName
						";
            $resultArr = $this->returnArray($sql);
            
            if ($returnAsso == 0)
                $returnArr = $resultArr;
            else {
                foreach ((array) $resultArr as $key => $ValueArr) {
                    $thisGroupID = $ValueArr['GroupID'];
                    $returnArr[$thisGroupID] = $ValueArr;
                }
            }
            
            return $returnArr;
        }

        function ChangeDisplayWeek($WeekStartTimeStamp, $ChangeValue)
        {
            global $Lang;
            
            $tsNumOfDay = $ChangeValue * 24 * 60 * 60;
            $newWeekStartTimeStamp = $WeekStartTimeStamp + $tsNumOfDay;
            
            $curr_date = date("Y-m-d", $newWeekStartTimeStamp);
            $curr_year = substr($curr_date, 0, 4);
            $curr_month = substr($curr_date, 5, 2);
            $curr_day = substr($curr_date, 8, 2);
            
            $StartOfTheWeek = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday, $curr_year));
            $EndOfTheWeek = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday + 6, $curr_year));
            
            $StartYearOfTheWeek = substr($StartOfTheWeek, 0, 4);
            $StartMonthOfTheWeek = substr($StartOfTheWeek, 5, 2);
            $StartDayOfTheWeek = substr($StartOfTheWeek, 8, 2);
            
            $EndYearOfTheWeek = substr($EndOfTheWeek, 0, 4);
            $EndMonthOfTheWeek = substr($EndOfTheWeek, 5, 2);
            $EndDayOfTheWeek = substr($EndOfTheWeek, 8, 2);
            
            $DisplayStartOfTheWeek = $StartDayOfTheWeek . " " . $Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int) $StartMonthOfTheWeek] . "," . $StartYearOfTheWeek;
            $DisplayEndOfTheWeek = $EndDayOfTheWeek . " " . $Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int) $EndMonthOfTheWeek] . "," . $EndYearOfTheWeek;
            
            $returnString = $newWeekStartTimeStamp . ":::" . $DisplayStartOfTheWeek . " - " . $DisplayEndOfTheWeek;
            
            return $returnString;
        }

        function ChangeDisplayWeekByCal($WeekStartDate)
        {
            global $Lang;
            
            $selected_year = substr($WeekStartDate, 0, 4);
            $selected_month = substr($WeekStartDate, 5, 2);
            $selected_day = substr($WeekStartDate, 8, 2);
            $selected_weekday = date("w", mktime(0, 0, 0, $selected_month, $selected_day, $selected_year));
            
            $new_date = date("Y-m-d", mktime(0, 0, 0, $selected_month, $selected_day - $selected_weekday, $selected_year));
            $newWeekStartTimeStamp = strtotime($new_date);
            
            $curr_date = date("Y-m-d", $newWeekStartTimeStamp);
            $curr_year = substr($curr_date, 0, 4);
            $curr_month = substr($curr_date, 5, 2);
            $curr_day = substr($curr_date, 8, 2);
            
            $StartOfTheWeek = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday, $curr_year));
            $EndOfTheWeek = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday + 6, $curr_year));
            
            $StartYearOfTheWeek = substr($StartOfTheWeek, 0, 4);
            $StartMonthOfTheWeek = substr($StartOfTheWeek, 5, 2);
            $StartDayOfTheWeek = substr($StartOfTheWeek, 8, 2);
            
            $EndYearOfTheWeek = substr($EndOfTheWeek, 0, 4);
            $EndMonthOfTheWeek = substr($EndOfTheWeek, 5, 2);
            $EndDayOfTheWeek = substr($EndOfTheWeek, 8, 2);
            
            $DisplayStartOfTheWeek = $StartDayOfTheWeek . " " . $Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int) $StartMonthOfTheWeek] . "," . $StartYearOfTheWeek;
            $DisplayEndOfTheWeek = $EndDayOfTheWeek . " " . $Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int) $EndMonthOfTheWeek] . "," . $EndYearOfTheWeek;
            
            $returnString = $newWeekStartTimeStamp . ":::" . $DisplayStartOfTheWeek . " - " . $DisplayEndOfTheWeek;
            echo $returnString;
        }

        function Upload_Room_Attachment($LocationID, $AttachmentLocation, $AttachmentName)
        {
            global $PATH_WRT_ROOT, $intranet_root;
            
            include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
            $fs = new libfilesystem();
            
            // Create folder if not exist
            $folder_prefix = $intranet_root . "/file/ebooking";
            if (! file_exists($folder_prefix))
                $fs->folder_new($folder_prefix);
            $folder_prefix .= "/room";
            if (! file_exists($folder_prefix))
                $fs->folder_new($folder_prefix);
            $folder_prefix .= "/r" . $LocationID;
            if (! file_exists($folder_prefix))
                $fs->folder_new($folder_prefix);
            
            $Result['CopyFile'] = $fs->file_copy($AttachmentLocation, $folder_prefix . "/" . $AttachmentName);
            if ($Result['CopyFile']) {
                $sql = "UPDATE INVENTORY_LOCATION  
							SET Attachment = '" . $this->Get_Safe_Sql_Query($AttachmentName) . "'  
							WHERE LocationID = '" . $LocationID . "' ";
                $Result['UpdateDB'] = $this->db_db_query($sql);
            }
            return ! in_array(false, $Result);
        }

        function Delete_Room_Attachment($LocationID)
        {
            global $PATH_WRT_ROOT, $intranet_root;
            
            include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
            $fs = new libfilesystem();
            
            $sql = "SELECT Attachment FROM INVENTORY_LOCATION WHERE LocationID = '" . $LocationID . "' ";
            $res_vec = $this->returnVector($sql);
            
            if (trim($res_vec[0]) != '') {
                $Attachment = $res_vec[0];
                $location = $intranet_root . "/file/ebooking/room/r" . $LocationID . "/" . $Attachment;
                $Result['RemoveFile'] = $fs->file_remove($location);
                if ($Result['RemoveFile']) {
                    $sql = "UPDATE INVENTORY_LOCATION SET Attachment = NULL WHERE LocationID = '" . $LocationID . "' ";
                    $Result['UpdateDB'] = $this->db_db_query($sql);
                }
                return ! in_array(false, $Result);
            }
            return true;
        }

        function Get_TimeSlot_By_Period($StartDate = '', $EndDate = '', $PeriodID = '')
        {
            if (trim($StartDate) != '' && trim($EndDate) != '')
                $cond_Date = "AND (period.PeriodStart <= '$StartDate' AND '$EndDate' <= period.PeriodEnd)";
            else 
                if (trim($PeriodID) != '')
                    $cond_PeriodID = "AND period.PeriodID = '$PeriodID' ";
            
            $sql = "SELECT 
					TT.TimeSlotID,
					CONCAT(TT.TimeSlotName,' (',TT.StartTime,' - ',TT.EndTime,')'),
					CONCAT(TT.StartTime,' - ',TT.EndTime),
					period.PeriodID
				FROM 
					INTRANET_CYCLE_GENERATION_PERIOD AS period 
					INNER JOIN INTRANET_PERIOD_TIMETABLE_RELATION AS PT_relation ON (period.PeriodID = PT_relation.PeriodID) 
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION AS TT_relation ON (PT_relation.TimetableID = TT_relation.TimetableID) 
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT AS TT ON (TT_relation.TimeSlotID = TT.TimeSlotID) 
				WHERE 
					1
					$cond_PeriodID
					$cond_Date
				ORDER BY
					TT.DisplayOrder ASC
					";
            $result = $this->returnArray($sql);
            
            return $result;
        }

        function Get_Special_Timetable_TimeSlot($SpecialTimetableSettingsID = '', $TimezoneID = '')
        {
            if (trim($SpecialTimetableSettingsID) != '')
                $cond_SpecialTimetableSettingsID = "AND sts.SpecialTimetableSettingsID = '$SpecialTimetableSettingsID' ";
            if (trim($TimezoneID) != '')
                $cond_TimezoneID = "AND sts.TimezoneID = '$TimezoneID' ";
            
            $sql = "
				SELECT 
					TT.TimeSlotID,
					CONCAT(TT.TimeSlotName,' (',TT.StartTime,' - ',TT.EndTime,')'),
					CONCAT(TT.StartTime,' - ',TT.EndTime),
					sts.SpecialTimetableSettingsID,
					sts.TimezoneID
				FROM
					INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS sts 
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION AS TT_relation ON (sts.TimetableID = TT_relation.TimetableID) 
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT AS TT ON (TT_relation.TimeSlotID = TT.TimeSlotID) 
				WHERE 
					1
					$cond_SpecialTimetableSettingsID
					$cond_TimezoneID
				ORDER BY
					TT.DisplayOrder ASC
					";
            
            $result = $this->returnArray($sql);
            
            return $result;
        }

        function Get_Timeslot_By_TimetableID($TimetableID = '', $TimeSlotID = '')
        {
            // if(trim($TimetableID)!='')
            if (! empty($TimetableID))
                $cond_TimetableID = " AND TT_relation.TimetableID IN (" . implode(",", (array) $TimetableID) . ") ";
                // if(trim($TimeSlotID)!='')
            if (! empty($TimeSlotID))
                $cond_TimeSlotID = " AND TT.TimeSlotID IN (" . implode(",", (array) $TimeSlotID) . ") ";
            
            $sql = "
					SELECT 
						TT.TimeSlotID,
						TT.TimeSlotName,
						TT.StartTime,
						TT.EndTime,
						CONCAT(TT.TimeSlotName,' (',TT.StartTime,' - ',TT.EndTime,')') TimeslotDisplay,
						TT_relation.TimetableID
					FROM
						INTRANET_TIMETABLE_TIMESLOT_RELATION AS TT_relation 
						INNER JOIN INTRANET_TIMETABLE_TIMESLOT AS TT ON (TT_relation.TimeSlotID = TT.TimeSlotID) 
					WHERE 
						1
						$cond_TimetableID
						$cond_TimeSlotID
					ORDER BY
						TT.DisplayOrder ASC
				";
            
            $result = $this->returnArray($sql);
            
            return $result;
        }

        function Separate_Booking_Period_By_DateArr($TargetDateArr, $FacilityType, $FacilityID, $IsSuspension = 0)
        {
            if (! is_array($TargetDateArr) || count($TargetDateArr) == 0 || ($FacilityType === '' && $FacilityID === ''))
                return false;
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            
            $Table = $IsSuspension ? "INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION" : "INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD";
            
            $str_targetDate = "'" . implode("','", (array) $TargetDateArr) . "'"; // used to delete original crashed period
            
            $DateCrashedPeriod = $this->Get_Corresponding_Booking_Period_By_Date($TargetDateArr, $RelatedItemID, $RelatedSubLocationID, $IsSuspension);
            if (empty($DateCrashedPeriod)) // no crashed date, can insert date directly without overwrite
                return true;
            
            $DateCrashedPeriod = BuildMultiKeyAssoc($DateCrashedPeriod, "RelatedPeriodID", "RecordDate", 1, 1);
            
            foreach ($DateCrashedPeriod as $CrashedPeriod => $TargetDateArr) // loop crashed period
{
                for ($i = 0; $i <= sizeof($TargetDateArr); $i ++) // loop crashed on the same period
{
                    // skip successive dates
                    if ($TargetDateArr[$i - 1] && $TargetDateArr[$i] && abs(intranet_time_diff($TargetDateArr[$i - 1], $TargetDateArr[$i])) <= 1)
                        continue;
                    
                    $cond_Min = $cond_Max = '';
                    if ($TargetDateArr[$i - 1])
                        $cond_Min = "AND RecordDate > '" . $TargetDateArr[$i - 1] . "'";
                    if ($TargetDateArr[$i])
                        $cond_Max = "AND RecordDate < '" . $TargetDateArr[$i] . "'";
                    
                    $sql = "SELECT MIN(PeriodID), RelatedPeriodID FROM $Table WHERE RelatedItemID = '$RelatedItemID' AND RelatedSubLocationID = '$RelatedSubLocationID' $cond_Min $cond_Max AND RelatedPeriodID = '$CrashedPeriod' GROUP By RelatedPeriodID";
                    $arrResult = $this->returnArray($sql, 3);
                    // debug_pr($sql);
                    if (sizeof($arrResult) > 0) {
                        for ($j = 0; $j < sizeof($arrResult); $j ++) {
                            list ($new_related_periodID, $RelatedPeriodID) = $arrResult[$j];
                            
                            $sql = "
									UPDATE 
										$Table 
									SET 
										RelatedPeriodID = '$new_related_periodID' 
									WHERE 
										RelatedItemID = '$RelatedItemID' 
										AND RelatedSubLocationID = '$RelatedSubLocationID' 
										$cond_Min 
										$cond_Max
										AND RelatedPeriodID = '$RelatedPeriodID'
									";
                            
                            $Success["Update"] = $this->db_db_query($sql);
                            // debug_pr($sql);
                        }
                    }
                }
            }
            $sql = "DELETE FROM $Table WHERE RelatedItemID = '$RelatedItemID' AND RelatedSubLocationID = '$RelatedSubLocationID' AND RecordDate IN ($str_targetDate)";
            $Success["Delete"] = $this->db_db_query($sql);
            
            return in_array(false, $Success) ? false : true;
        }

        function Get_Corresponding_Booking_Period_By_Date($TargetDateArr, $RelatedItemID, $RelatedSubLocationID, $IsSuspension = 0)
        {
            $str_targetDate = "'" . implode("','", (array) $TargetDateArr) . "'";
            
            $Table = $IsSuspension ? "INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION" : "INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD";
            $sql = "
					SELECT 
						RelatedPeriodID,
						RecordDate						
					FROM
						$Table
					WHERE
						RelatedItemID = '$RelatedItemID' 
						AND RelatedSubLocationID = '$RelatedSubLocationID' 
						AND RecordDate IN ($str_targetDate)
				";
            $result = $this->returnArray($sql);
            
            return $result;
        }

        function Get_All_TimeZone_By_Academic_Year($AcademicYearID = '')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            if ($AcademicYearID != '')
                $cond_AcademicYearID = " AND ay.AcademicYearID IN (" . implode(",", (array) $AcademicYearID) . ") ";
            
            $sql = "
				SELECT
					tz.PeriodID,
					tz.PeriodStart,
					tz.PeriodEnd,
					tz.ColorCode,
					tz.PeriodType,
					ay.AcademicYearID 
				FROM
					ACADEMIC_YEAR ay 
					INNER JOIN ACADEMIC_YEAR_TERM ayt ON ayt.AcademicYearID = ay.AcademicYearID
					INNER JOIN INTRANET_CYCLE_GENERATION_PERIOD as tz ON (tz.PeriodStart Between ayt.TermStart AND ayt.TermEnd) AND tz.RecordStatus = 1
				WHERE
					1 
					$cond_AcademicYearID
				ORDER BY
					tz.PeriodStart ASC
			";
            
            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_Involved_Timezone_Timetable_In_Period($PeriodStart, $PeriodEnd)
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            $sql = "
				SELECT
					tz.PeriodID,
					tz.PeriodStart,
					tz.PeriodEnd,
					tz.ColorCode,
					tz.PeriodType,
					tzt.TimetableID
				FROM
					INTRANET_CYCLE_GENERATION_PERIOD tz
					INNER JOIN INTRANET_PERIOD_TIMETABLE_RELATION tzt ON tz.PeriodID = tzt.PeriodID
				WHERE
					tz.PeriodStart BETWEEN '$PeriodStart' AND '$PeriodEnd'
					OR tz.PeriodEnd BETWEEN '$PeriodStart' AND '$PeriodEnd'
					OR 
					(
						'$PeriodStart' BETWEEN tz.PeriodStart AND tz.PeriodEnd
						AND '$PeriodEnd' BETWEEN tz.PeriodStart AND tz.PeriodEnd 
					)
				ORDER BY
					tz.PeriodStart ASC
			";
            // debug_pr($sql);
            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_Special_Timetable_Date_Of_TimeZone($TimezoneID = '', $SpecialTimetableSettingsID = '', $WeekStartDate = '', $WeekEndDate = '')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            // if(trim($TimezoneID)!='')
            if (! empty($TimezoneID))
                $cond_TimezoneID = " AND st.TimezoneID IN (" . implode(",", (array) $TimezoneID) . ") ";
                // if(trim($SpecialTimetableSettingsID)!='')
            if (! empty($SpecialTimetableSettingsID))
                $cond_SpecialTimetableSettingsID = " AND st.SpecialTimetableSettingsID IN (" . implode(",", (array) $SpecialTimetableSettingsID) . ") ";
            if (trim($WeekStartDate) != '')
                $cond_WeekStartDate = " AND std.SpecialDate >= '$WeekStartDate' ";
            if (trim($WeekEndDate) != '')
                $cond_WeekEndDate = " AND std.SpecialDate <= '$WeekEndDate' ";
            
            $sql = "
				SELECT	
					st.TimezoneID,
					st.BgColor,
					st.RepeatType,
					st.TimetableID,
					std.SpecialTimetableSettingsID,
					std.SpecialDate
				FROM
					INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS st
					INNER JOIN INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE std ON st.SpecialTimetableSettingsID = std.SpecialTimetableSettingsID 
				WHERE
					1					
					$cond_TimezoneID
					$cond_SpecialTimetableSettingsID
					$cond_WeekStartDate
					$cond_WeekEndDate
			";
            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Has_Any_Special_Timetable_Set($TimezoneID = '')
        {
            if (trim($TimezoneID) != '')
                $cond_TimezoneID = " AND st.TimezoneID IN (" . implode(",", (array) $TimezoneID) . ") ";
            $sql = "
				SELECT	
					COUNT(*)
				FROM
					INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS st
				WHERE
					1
					$cond_TimezoneID
			";
            $result = $this->returnVector($sql);
            
            return $result[0] > 0 ? 1 : 0;
        }

        function Get_Special_Timetable_Repeat_Day($SpecialTimetableSettingsID)
        {
            $sql = "
				SELECT	
					strd.*
				FROM
					INTRANET_TIMEZONE_SPECIAL_TIMETABLE_REPEAT_DAY strd
				WHERE
					strd.SpecialTimetableSettingsID = '$SpecialTimetableSettingsID'
			";
            
            $result = $this->returnArray($sql);
            
            return $result;
        }

        function Get_TimeZone_Info($TimeZoneID)
        {
            $sql = "
				SELECT 
					TimeZone.PeriodID,
					TimeZone.PeriodStart,
					TimeZone.PeriodEnd,
					TimeZone.PeriodType,
					TimeZone.CycleType,
					TimeZone.PeriodDays
				FROM 
					INTRANET_CYCLE_GENERATION_PERIOD AS TimeZone
				WHERE
 					TimeZone.PeriodID = '$TimeZoneID'
				ORDER BY
					TimeZone.PeriodStart
				";
            
            return $this->returnArray($sql);
        }

        function Save_TimeZone_BookingPeriod($SelectedSlot, $TimezoneSettingID = '', $TimeZoneID = '', $FacilityType = '', $FacilityID = '', $SpecialTimetableSettingsID = '')
        {
            global $Lang;
            $this->Start_Trans();
            if (trim($TimezoneSettingID) != '') {
                $Success['UpdateTimetableInfo'] = $this->Update_TimeZone_BookingPeriod($TimezoneSettingID);
            } else {
                $Success['InsertTimetableInfo'] = ($TimezoneSettingID = $this->Insert_TimeZone_BookingPeriod($TimeZoneID, $FacilityType, $FacilityID, $SpecialTimetableSettingsID));
            }
            $Success['UpdateTimeslot'] = $this->Update_TimeZone_BookingPeriod_Timeslot($SelectedSlot, $TimezoneSettingID);
            
            if (in_array(false, $Success)) {
                $this->RollBack_Trans();
                return $Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodEditFailed'];
            } else {
                $this->Commit_Trans();
                return $Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodEditSuccessfully'];
            }
        }

        function Update_TimeZone_BookingPeriod($TimezoneSettingID)
        {
            $sql = "
				UPDATE 
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING
				SET
					DateModified = NOW(),
					ModifiedBy = '" . $_SESSION['UserID'] . "'
				WHERE
					TimezoneSettingID = '$TimezoneSettingID'
			";
            $Success = $this->db_db_query($sql);
            return $Success ? 1 : 0;
        }

        function Insert_TimeZone_BookingPeriod($TimeZoneID, $FacilityType, $FacilityID, $SpecialTimetableSettingsID)
        {
            $FacilityType = trim($FacilityType) == '' ? 0 : $FacilityType;
            $FacilityID = trim($FacilityID) == '' ? 0 : $FacilityID;
            $SpecialTimetableSettingsID = trim($SpecialTimetableSettingsID) == '' ? "0" : $SpecialTimetableSettingsID;
            
            $sql = "
				INSERT INTO INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING
					(TimeZoneID, FacilityType, FacilityID, SpecialTimetableSettingsID, ModifiedBy, DateModified)
				VALUES
					('$TimeZoneID', '$FacilityType', '$FacilityID', '$SpecialTimetableSettingsID', '" . $_SESSION['UserID'] . "', NOW())
			";
            
            $Success = $this->db_db_query($sql);
            
            return $Success ? mysql_insert_id() : 0;
        }

        function Get_Lastest_Date_With_BookingPeriod()
        {
            $sql = "SELECT MAX(PeriodEnd)
				FROM
				INTRANET_CYCLE_GENERATION_PERIOD as icgp
				INNER JOIN INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING as ieabts ON icgp.PeriodID = ieabts.TimeZoneID";
            $tempLatestDateWithTimezone = $this->ReturnVector($sql);
            return $tempLatestDateWithTimezone[0];
        }

        function Update_TimeZone_BookingPeriod_Timeslot($SelectedSlot, $TimezoneSettingID)
        {
            $Success['Delete'] = $this->Delete_TimeZone_BookingPeriod_Timeslot($TimezoneSettingID);
            if (! empty($SelectedSlot))
                $Success['Insert'] = $this->Insert_TimeZone_BookingPeriod_Timeslot($SelectedSlot, $TimezoneSettingID);
            
            return ! in_array(false, $Success) ? 1 : 0;
        }

        function Delete_TimeZone_BookingPeriod_Timeslot($TimezoneSettingID)
        {
            $sql = "
				DELETE FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT
				WHERE
					TimezoneSettingID = '$TimezoneSettingID'
			";
            
            $Success = $this->db_db_query($sql);
            return $Success;
        }

        function Insert_TimeZone_BookingPeriod_Timeslot($SelectedSlot, $TimezoneSettingID)
        {
            foreach ($SelectedSlot as $Day => $TimeslotIDArr) {
                foreach ($TimeslotIDArr as $TimeslotID)
                    $ValArr[] = "('$TimezoneSettingID', '$Day', '$TimeslotID')";
            }
            $ValStr = implode(",", (array) $ValArr);
            
            $sql = "
				INSERT INTO INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT
					(TimezoneSettingID, Day, TimeslotID)	
				VALUES
					$ValStr
			";
            
            $Success = $this->db_db_query($sql);
            return $Success;
        }

        function Get_TimeZone_Available_Booking_Timetable($FacilityType, $FacilityID, $TimeZoneID = '', $SpecialTimetableSettingsID = '')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            if (trim($SpecialTimetableSettingsID) != '')
                $cond_SpecialTimetableSettingsID = " AND tz.SpecialTimetableSettingsID = '$SpecialTimetableSettingsID' ";
                
                // if(trim($TimeZoneID)!='')
            if (! empty($TimeZoneID))
                $cond_TimezoneID = " AND tz.TimeZoneID IN (" . implode(",", (array) $TimeZoneID) . ") ";
            
            if (trim($FacilityType) == '')
                $FacilityType = 0;
            if (trim($FacilityID) == '')
                $FacilityID = 0;
            
            $sql = "
				SELECT
					tz.TimezoneSettingID, 
					tz.TimeZoneID,
					tz.FacilityType,
					tz.FacilityID,
					tz.SpecialTimetableSettingsID,
					COUNT(ts.TimezoneTimeslotID) as NumberOfTimeslot
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING tz
					LEFT JOIN INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ts ON tz.TimezoneSettingID = ts.TimezoneSettingID
				WHERE
					tz.FacilityType = '$FacilityType'
					AND tz.FacilityID = '$FacilityID'
					$cond_TimezoneID
					$cond_SpecialTimetableSettingsID
				GROUP BY
					tz.TimezoneSettingID
			";

            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_TimeZone_Available_Booking_Timetable_Timeslot($TimezoneSettingID)
        {
            if (empty($TimezoneSettingID))
                return array();
            
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            $sql = "
				SELECT
					ts.TimezoneSettingID, 
					ts.Day,
					ts.TimeslotID,
					its.StartTime,
					its.EndTime					
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ts   
					INNER JOIN INTRANET_TIMETABLE_TIMESLOT its ON ts.TimeslotID = its.TimeSlotID 
				WHERE
					ts.TimezoneSettingID IN (" . implode(",", (array) $TimezoneSettingID) . ")
				ORDER BY
					its.DisplayOrder ASC
			";
            $result = $this->returnArray($sql);
            // debug_pr($result);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_Facility_Available_Booking_Period_Of_User($FacilityType, $FacilityIDArr, $ParUserID, $AcademicYearID = '', $StartDate = '', $EndDate = '', $From_eAdmin = '')
        {
            global $sys_custom;

            list ($SinceDate, $UntilDate) = $this->getFacilityBookingAvailableDateRange($FacilityType, $FacilityIDArr, $ParUserID, $From_eAdmin);

            $SinceDate = $SinceDate && strtotime($SinceDate) > strtotime($StartDate) ? $SinceDate : $StartDate;
            $UntilDate = $UntilDate && strtotime($UntilDate) < strtotime($EndDate) ? $UntilDate : $EndDate;
            foreach ((array) $FacilityIDArr as $FacilityID) {
                $BookingPeriod[$FacilityID] = $this->Get_Facility_Available_Booking_Period_For_Booking($FacilityType, $FacilityID, $AcademicYearID, $SinceDate, $UntilDate);
            }
            
            if ($sys_custom['eBooking']['EverydayTimetable'] && $FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM && count($FacilityIDArr)>1) {
                $OriginalBookingPeriod = $BookingPeriod;
                $BookingPeriod = $this->Facility_Time_Range_Intersect($BookingPeriod);
                $lastFacilityBookingPeriod = array_pop($OriginalBookingPeriod);

                foreach((array)$lastFacilityBookingPeriod as $_date=>$_timeslotAry) {
                    foreach((array)$_timeslotAry as $__index=>$__timeslotAry) {
                        if ($BookingPeriod[$_date][$__index]) {
                            $lastFacilityBookingPeriod[$_date][$__index]['StartTime'] = $BookingPeriod[$_date][$__index]['StartTime'];
                            $lastFacilityBookingPeriod[$_date][$__index]['EndTime'] = $BookingPeriod[$_date][$__index]['EndTime'];
                        }
                    }
                }

                return $lastFacilityBookingPeriod;
            }
            else {
                $BookingPeriod = $this->Facility_Time_Range_Intersect($BookingPeriod);
            }
            return $BookingPeriod;
        }

        function Get_Facility_Available_Booking_Period_For_Booking($FacilityType, $FacilityID, $AcademicYearID = '', $StartDate = '', $EndDate = '')
        {
            global $sys_custom;
            
            if (empty($AcademicYearID) && (empty($StartDate) || empty($EndDate)))
                return false;
            
            if (empty($AcademicYearID)) // if no academic year provided, find academic year of EACH DAY in the period
            {
                $StartDateTS = strtotime($StartDate);
                $EndDateTS = strtotime($EndDate);
                for ($TS = $StartDateTS; $TS <= $EndDateTS; $TS = strtotime("+1 day", $TS)) {
                    $Date = date("Y-m-d", $TS);
                    $thisAcademicYear = $this->Get_AcademicYearID_By_Date($Date);
                    
                    $YearDateAssoc[$thisAcademicYear][] = $Date; // group date with the same academic year
                }
            } else {
                if (empty($StartDate) && empty($EndDate)) {
                    $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($AcademicYearID);
                    $StartDate = $AcademicYearInfo['StartDate'];
                    $EndDate = $AcademicYearInfo['EndDate'];
                }
                
                $YearDateAssoc[$AcademicYearID][] = $StartDate;
                $YearDateAssoc[$AcademicYearID][] = $EndDate;
            }
            
            foreach ((array) $YearDateAssoc as $thisAcademicYearID => $DateArr) // loop each academic Year
            {
                $thisStartDate = $DateArr[0];
                $thisEndDate = $DateArr[sizeof($DateArr) - 1];
                
                if ($sys_custom['eBooking']['EverydayTimetable']) {
                    if (($FacilityType == 0 && $FacilityID == 0) || ! $this->Is_Facility_Booking_Period_Set($thisAcademicYearID, $FacilityType, $FacilityID, $thisStartDate, $thisEndDate)) // if no setting on the facility this Academic Year, will use default setting
                    {
                        $specialBookingPeriodArr = $this->Get_Facility_Available_Booking_Period(0, 0, $thisAcademicYearID, $thisStartDate, $thisEndDate);
                    } else // otherwise use specific booking period
                    {
                        $specialBookingPeriodArr = $this->Get_Facility_Available_Booking_Period($FacilityType, $FacilityID, $thisAcademicYearID, $thisStartDate, $thisEndDate);
                    }

                    // change from index array to associated array using date -> DisplayOrder
                    if (count($specialBookingPeriodArr)) {
                        $specialBookingPeriodAssoc = array();
                        $specialBookingPeriodTimeSlotAry = array();
                        $specialBookingPeriodSpecificTimeAry = array();
                        foreach((array)$specialBookingPeriodArr as $_date => $_periodInfo) {
                            $timeSlotInfo = array();
                            foreach((array)$_periodInfo as $__idx=>$__periodInfo) {
                                $timeSlotID = $__periodInfo['TimeSlotID'];
                                $specialBookingPeriodTimeSlotAry[] = $timeSlotID;
                                if ($timeSlotID) {  // by lesson
                                    $timeSlotInfo[$timeSlotID] = $__periodInfo;
                                }
                                else {
                                    $specialBookingPeriodSpecificTimeAry[$_date][$__idx] = $__periodInfo;
                                }
                            }
                            if (count($timeSlotInfo)) {
                                $specialBookingPeriodAssoc[$_date] = $timeSlotInfo;
                            }
                        }
                        if (count($specialBookingPeriodTimeSlotAry)) {      // by lesson
                            $specialBookingPeriodTimeSlotAry = array_values(array_unique($specialBookingPeriodTimeSlotAry));
                            $timeSlotDisplayOrderAssoc = $this->getDisplayOrderByTimeSlotID($specialBookingPeriodTimeSlotAry);
                            $specialBookingPeriodDisplayOrderAssoc = array();
                            foreach ((array)$specialBookingPeriodAssoc as $_date => $_timeSlotInfo) {
                                foreach ((array)$_timeSlotInfo as $__timeSlotID => $__timeSlotInfo) {
                                    $__displayOrder = $timeSlotDisplayOrderAssoc[$__timeSlotID];
                                    $specialBookingPeriodDisplayOrderAssoc[$_date][$__displayOrder] = $__timeSlotInfo;      // only $__displayOrder is significant, $__timeSlotInfo here is insignificant
                                }
                            }
                            unset($specialBookingPeriodAssoc);
                            unset($timeSlotDisplayOrderAssoc);
                        }
                    }

                    $everydayPeriodArr = $this->getAvailableBookingPeriodFromEverydayTimetable($thisAcademicYearID, $thisStartDate, $thisEndDate, $FacilityType, $FacilityID);

                    if (count($everydayPeriodArr)) {        // has record in everydate timetable, i.e. non-skip school day record
                        foreach ((array)$everydayPeriodArr as $_date => $_everydayPeriodAry) {
                            if (isset($specialBookingPeriodDisplayOrderAssoc[$_date])) {                  // available timeslot are those set in special booking period where TimeSlotID>0
                                foreach ((array)$_everydayPeriodAry as $__idx => $__timeSlotInfo) {
                                    if (!isset($specialBookingPeriodDisplayOrderAssoc[$_date][$__idx])) {
                                        unset($everydayPeriodArr[$_date][$__idx]);          // set the timeslot to be unavailable
                                    }
                                }
                            }
                        }
                        $PeriodArr[] = $everydayPeriodArr;
                    }
                    else {
                        $PeriodArr[] = $specialBookingPeriodSpecificTimeAry;
                    }
                }       // end EverydayTimetable
                else {
                    if (($FacilityType == 0 && $FacilityID == 0) || ! $this->Is_Facility_Booking_Period_Set($thisAcademicYearID, $FacilityType, $FacilityID)) // if no setting on the facility this Academic Year, will use default setting
                    {
                        $PeriodArr[] = $this->Get_Facility_Available_Booking_Period(0, 0, $thisAcademicYearID, $thisStartDate, $thisEndDate);
                    } else // otherwise use specific booking period
                    {
                        $PeriodArr[] = $this->Get_Facility_Available_Booking_Period($FacilityType, $FacilityID, $thisAcademicYearID, $thisStartDate, $thisEndDate);
                    }
                 }
            }

            // merge the booking period of different academic years into 1 array
            $ReturnArr = array();
            foreach ((array) $PeriodArr as $thisPeriod) {
                $ReturnArr = array_merge($ReturnArr, (array)$thisPeriod);
            }

            return $ReturnArr;
        }

        function Get_Facility_Available_Booking_Period($FacilityType, $FacilityID, $AcademicYearID, $StartDate = '', $EndDate = '')
        {
            if (trim($StartDate) == '' && trim($EndDate) == '' && trim($AcademicYearID) != '') {
                $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($AcademicYearID);
                $StartDate = $AcademicYearInfo['StartDate'];
                $EndDate = $AcademicYearInfo['EndDate'];
            }
            
            global $a;
            // Timezone Setting
            if (! $a) {
                include_once ('libcycleperiods.php');
                $a = new libcycleperiods();
            }
            
            // Get all time zone of current academic year
            if (trim($StartDate) != '' && trim($EndDate) != '') {
                $TimeZoneArr = $this->Get_Involved_Timezone_Timetable_In_Period($StartDate, $EndDate);
            } else {
                $TimeZoneArr = $this->Get_All_TimeZone_By_Academic_Year($AcademicYearID);
            }
            $TimeZoneArr = BuildMultiKeyAssoc($TimeZoneArr, "PeriodID");
            $TimeZoneIDArr = array_keys($TimeZoneArr);

            // Get Special Timetable and Special Dates
            $TimezoneSpecialTimetableDateArr = $this->Get_Special_Timetable_Date_Of_TimeZone($TimeZoneIDArr);

            $SpecialTimetableInfoArr = BuildMultiKeyAssoc($TimezoneSpecialTimetableDateArr, array(
                "SpecialTimetableSettingsID"
            ), array(
                "RepeatType",
                "BgColor",
                "TimezoneID"
            ));
            $TimezoneSpecialTimetableDateArr = BuildMultiKeyAssoc($TimezoneSpecialTimetableDateArr, array(
                "TimezoneID",
                "SpecialTimetableSettingsID"
            ), '', 0, 1);
            $TimezoneSettingArr = $this->Get_TimeZone_Available_Booking_Timetable($FacilityType, $FacilityID, $TimeZoneIDArr);
            $TimezoneSettingAssoc = BuildMultiKeyAssoc($TimezoneSettingArr, array(
                "TimeZoneID",
                "SpecialTimetableSettingsID"
            ));

            $TimezoneSettingIDArr = array();
            // if(!empty($FacilityType) && !empty($FacilityID))
            // {
            // foreach($TimeZoneIDArr as $thisTimeZoneID)
            // {
            // $GeneralTimetableSetting = $TimezoneSettingAssoc[$thisTimeZoneID][0];
            // if(!$GeneralTimetableSetting || $GeneralTimetableSetting['NumberOfTimeslot']==0) // check whether general timetable was set
            // $TimetableWithoutSpecificTimezoneSetting[$thisTimeZoneID][] = 0;
            // else
            // $TimezoneSettingIDArr[] = $GeneralTimetableSetting['TimezoneSettingID'];
            //
            // foreach((array)$TimezoneSpecialTimetableDateArr[$thisTimeZoneID] as $SpecialTimetableSettingsID => $SpecialTimetableDateArr)
            // {
            // $SpecialTimetableSetting = $TimezoneSettingAssoc[$thisTimeZoneID][$SpecialTimetableSettingsID];
            //
            // if(!$SpecialTimetableSetting || $SpecialTimetableSetting['NumberOfTimeslot']==0) // check whether special timetable was set
            // $TimetableWithoutSpecificTimezoneSetting[$thisTimeZoneID][] = $SpecialTimetableSettingsID; // get general timezone setting if not set.
            // else
            // $TimezoneSettingIDArr[] = $SpecialTimetableSetting['TimezoneSettingID']; // append TimezoneSettingID to involved TimezoneSetting
            // }
            //
            // }
            //
            // # if specific time zone setting is empty, get from general settings
            // if(sizeof($TimetableWithoutSpecificTimezoneSetting)>0)
            // {
            // $thisTimetableTimezoneIDArr = array_keys($TimetableWithoutSpecificTimezoneSetting);
            // $GeneralTimezoneSettingArr = $this->Get_TimeZone_Available_Booking_Timetable(0, 0, $thisTimetableTimezoneIDArr);
            // $GeneralTimezoneSettingArr = BuildMultiKeyAssoc($GeneralTimezoneSettingArr, array("TimeZoneID","SpecialTimetableSettingsID"));
            //
            // foreach((array)$TimetableWithoutSpecificTimezoneSetting as $thisTimeZoneID => $thisTimezoneSettingArr)
            // {
            // foreach((array)$thisTimezoneSettingArr as $thisSpecialTimetableSettingsID)
            // {
            // if($GeneralTimezoneSettingArr[$thisTimeZoneID][$thisSpecialTimetableSettingsID])
            // {
            // $TimezoneSettingAssoc[$thisTimeZoneID][$thisSpecialTimetableSettingsID] = $GeneralTimezoneSettingArr[$thisTimeZoneID][$thisSpecialTimetableSettingsID];
            // $TimezoneSettingIDArr[] = $GeneralTimezoneSettingArr[$thisTimeZoneID][$thisSpecialTimetableSettingsID]['TimezoneSettingID'];
            // }
            //
            // }
            // }
            // }
            // }
            // else
            $TimezoneSettingIDArr = Get_Array_By_Key($TimezoneSettingArr, "TimezoneSettingID");

            $TimeslotArr = $this->Get_TimeZone_Available_Booking_Timetable_Timeslot($TimezoneSettingIDArr);
            $TimeslotIDArr = Get_Array_By_Key($TimeslotArr, "TimeslotID");
            $TimeslotArr = BuildMultiKeyAssoc($TimeslotArr, array(
                "TimezoneSettingID",
                "Day"
            ), array(
                "TimeslotID",
                "StartTime",
                "EndTime"
            ), 0, 1);
            
            $AvailableDateArr = array();
            
            foreach ((array) $TimeZoneArr as $thisTimeZoneID => $TimeZoneInfo) {
                // Build Array for general timetable
                $thisTimezoneSettingID = $TimezoneSettingAssoc[$thisTimeZoneID][0]['TimezoneSettingID'];
                
                $DateArr = $a->Get_Dates_By_TimeZone($thisTimeZoneID, '', 1);
                
                foreach ((array) $DateArr as $thisDate => $CycleDay) {
                    if (trim($StartDate) != '' && trim($EndDate) != '' && ! (strtotime($StartDate) <= strtotime($thisDate) && strtotime($thisDate) <= strtotime($EndDate))) {
                        continue;
                    }
                    
                    foreach ((array) $TimeslotArr[$thisTimezoneSettingID][$CycleDay] as $thisTimeslotInfo) {
                        $AvailableDateArr[$thisDate][] = $thisTimeslotInfo;
                    }
                }
                
                // Build Array for Special timetable
                foreach ((array) $TimezoneSpecialTimetableDateArr[$thisTimeZoneID] as $thisSpecialTimetableSettingsID => $SpecialTimetableDateArr) {
                    $RepeatType = $SpecialTimetableInfoArr[$thisSpecialTimetableSettingsID]['RepeatType'];
                    $DateArr = $a->Get_Dates_By_TimeZone($thisTimeZoneID, '', 1, $RepeatType);
                    
                    foreach ((array) $SpecialTimetableDateArr as $thisDateInfo) {
                        $thisDate = $thisDateInfo['SpecialDate'];
                        
                        if (trim($StartDate) != '' && trim($EndDate) != '' && ! (strtotime($StartDate) <= strtotime($thisDate) && strtotime($thisDate) <= strtotime($EndDate)))
                            continue;
                        
                        $CycleDay = $DateArr[$thisDate];
                        $thisTimezoneSettingID = $TimezoneSettingAssoc[$thisTimeZoneID][$thisSpecialTimetableSettingsID]['TimezoneSettingID'];
                        
                        if (! $thisTimezoneSettingID)
                            continue;
                        
                        $thisAvailableDatePeriodArr = array();
                        
                        foreach ((array) $TimeslotArr[$thisTimezoneSettingID][$CycleDay] as $thisTimeslotInfo) {
                            $thisAvailableDatePeriodArr[] = $thisTimeslotInfo;
                        }
                        
                        // if(!empty($thisAvailableDatePeriodArr)) // cover general timetable setting only if client selected any special timetable timeslot
                        $AvailableDateArr[$thisDate] = $thisAvailableDatePeriodArr;
                    }
                }
            }

            // Special Setting
            $SpecialPeriod = $this->Get_Special_Available_Booking_Period($FacilityType, $FacilityID, $StartDate, $EndDate);

            foreach ((array) $SpecialPeriod as $thisRecordDate => $BookingPeriodArr)
                $AvailableDateArr[$thisRecordDate] = $BookingPeriodArr;

                // Remove Suspended day
            $DaySuspension = $this->Get_Booking_Day_Suspension($FacilityType, $FacilityID, $StartDate, $EndDate);

            foreach ((array) $DaySuspension as $thisRecordDate)
                $AvailableDateArr[$thisRecordDate] = NULL;
            
            $AvailableDateArr = array_remove_empty($AvailableDateArr);

            ksort($AvailableDateArr);

            return $AvailableDateArr;
        }

        function Get_Special_Available_Booking_Period($FacilityType, $FacilityID, $StartDate = '', $EndDate = '')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $cond_RelatedID = "	AND (RelatedSubLocationID = '$RelatedSubLocationID' AND RelatedItemID = '$RelatedItemID')";
            
            if (trim($StartDate) != '')
                $cond_StartDate = " AND a.RecordDate >= '$StartDate' ";
            if (trim($EndDate) != '')
                $cond_EndDate = " AND a.RecordDate <= '$EndDate' ";
            
            $sql = "
					SELECT 
						/*IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) as FacilityType,
						IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,RelatedItemID,RelatedSubLocationID)) as FacilityID,*/
						a.RecordDate,
						a.TimeSlotID,
						a.StartTime,
						a.EndTime
					FROM
						INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD a
					WHERE 
						1  
						$cond_RelatedID
						$cond_StartDate
						$cond_EndDate	
					Order By 
						/*IF(RelatedSubLocationID = 0 AND RelatedItemID = 0,0,IF(RelatedItemID > 0 ,2 ,1)) ASC,*/ a.RecordDate, a.StartTime, a.EndTime 		
				";
            $Result = $this->returnArray($sql);

            $result = BuildMultiKeyAssoc((array) $Result, array(
                "RecordDate"
            ), array(
                "TimeSlotID",
                "StartTime",
                "EndTime"
            ), 0, 1);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
            
            // if(empty($FacilityType) && empty($FacilityID))
            // return $Result[0][0];
            // else
            // {
            // foreach((array)$Result[$FacilityType][$FacilityID] as $thisRecordDate => $BookingPeriod) // cover facility booking period to general booking period if facility booking period exist on a date.
            // $Result[0][0][$thisRecordDate] = $BookingPeriod;
            //
            // return $Result[0][0];
            // }
        }

        function Get_Booking_Day_Suspension($FacilityType, $FacilityID, $StartDate = '', $EndDate = '')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $cond_RelatedID = "	AND (RelatedSubLocationID = '$RelatedSubLocationID' AND RelatedItemID = '$RelatedItemID')";
            
            if (trim($StartDate) != '')
                $cond_StartDate = " AND a.RecordDate >= '$StartDate' ";
            if (trim($EndDate) != '')
                $cond_EndDate = " AND a.RecordDate <= '$EndDate' ";
            
            $sql = "
					SELECT 
						a.RecordDate
					FROM
						INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION a
					WHERE 
						1  
						$cond_RelatedID
						$cond_StartDate
						$cond_EndDate	
					Order By 
						a.RecordDate 		
				";
            $result = $this->returnVector($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Is_Facility_Booking_Period_Set($AcademicYearID, $FacilityType, $FacilityID, $startDate='', $endDate='')
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            if (empty($FacilityType) && empty($FacilityID))
                $cond_RelatedID = "	(RelatedSubLocationID = 0 AND RelatedItemID = 0)";
            else 
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM)
                    $cond_RelatedID = "	(RelatedSubLocationID = '$FacilityID' AND RelatedItemID = 0)";
                else
                    $cond_RelatedID = "	(RelatedSubLocationID = 0 AND RelatedItemID = '$FacilityID')";
            if (($startDate == '') && ($endDate == '')) {
                $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($AcademicYearID);
                $StartDate = $AcademicYearInfo['StartDate'];
                $EndDate = $AcademicYearInfo['EndDate'];
            }
            else {
                $StartDate = $startDate;
                $EndDate = $endDate;
            }
            $cond_Date = " AND RecordDate BETWEEN '$StartDate' AND '$EndDate' ";
            
            $sql = "
				SELECT 
					COUNT(*)
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD
				WHERE
					$cond_RelatedID
					$cond_Date
			";
            $exist = $this->returnVector($sql);
            if ($exist[0] > 0) {
                // return true;
                
                $result = true;
                $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
                return $result;
            }
            
            // 2013-0822-0943-36073
            $sql = "
				SELECT 
					COUNT(*)
				FROM
					INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION
				WHERE
					$cond_RelatedID
					$cond_Date
			";
            $exist = $this->returnVector($sql);
            if ($exist[0] > 0) {
                // return true;
                
                $result = true;
                $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
                return $result;
            }
            
            $TimezoneArr = $this->Get_All_TimeZone_By_Academic_Year($AcademicYearID);
            if (empty($TimezoneArr)) {
                // return false;
                
                $result = false;
                $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
                return $result;
            }
            
            $TimezoneIDArr = Get_Array_By_Key($TimezoneArr, "PeriodID");
            $cond_TimezoneID = " AND tz.TimezoneID IN (" . implode(",", (array) $TimezoneIDArr) . ") ";
            $sql = "
				SELECT
					COUNT(ts.TimezoneTimeslotID)
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING tz
					LEFT JOIN INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ts ON tz.TimezoneSettingID = ts.TimezoneSettingID
				WHERE
					tz.FacilityType = '$FacilityType'
					AND tz.FacilityID = '$FacilityID'
					$cond_TimezoneID

			";
            $exist = $this->returnVector($sql);
            
            // if ($exist[0]>0) {
            // return true;
            // }
            $result = ($exist[0] > 0) ? true : false;
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_Facility_With_Specific_Booking_Period($AcademicYearID)
        {
            $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($AcademicYearID);
            $StartDate = $AcademicYearInfo['StartDate'];
            $EndDate = $AcademicYearInfo['EndDate'];
            $cond_Date = " AND RecordDate BETWEEN '$StartDate' AND '$EndDate' ";
            
            $sql = "
				SELECT DISTINCT
					IF(RelatedSubLocationID = 0, 2, 1) FacilityType,
					IF(RelatedSubLocationID = 0, RelatedItemID, RelatedSubLocationID) FacilityID
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD
				WHERE
					(RelatedSubLocationID <> 0 OR RelatedItemID <> 0)
					$cond_Date
			";
            $FacilityArr1 = $this->returnArray($sql);
            
            $TimezoneArr = $this->Get_All_TimeZone_By_Academic_Year($AcademicYearID);
            if (empty($TimezoneArr))
                return array();
            
            $TimezoneIDArr = Get_Array_By_Key($TimezoneArr, "PeriodID");
            $cond_TimezoneID = " AND tz.TimezoneID IN (" . implode(",", (array) $TimezoneIDArr) . ") ";
            $sql = "
				SELECT DISTINCT
					tz.FacilityType,
					tz.FacilityID
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING tz
					INNER JOIN INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ts ON tz.TimezoneSettingID = ts.TimezoneSettingID
				WHERE
					tz.FacilityID <> 0
					$cond_TimezoneID
			";
            $FacilityArr2 = $this->returnArray($sql);
            
            $FacilityIDArr = array();
            foreach ($FacilityArr1 as $thisFacility) {
                $FacilityIDArr[$thisFacility['FacilityType']][] = $thisFacility['FacilityID'];
            }
            
            foreach ($FacilityArr2 as $thisFacility) {
                if (! in_array($thisFacility['FacilityID'], (array) $FacilityIDArr[$thisFacility['FacilityType']]))
                    $FacilityIDArr[$thisFacility['FacilityType']][] = $thisFacility['FacilityID'];
            }
            
            return $FacilityIDArr;
        }

        function Get_Facility_Booking_Record($FacilityType, $FacilityID = '', $StartDate = '', $EndDate = '', $BookingStatus = '', $BookingID = '', $WithName = 0, $WithUserName = 0, $FollowGroupID = '', $MgmtGroupID = '', $GroupByBookingRequest = 0, $OrderBy = '', $Keyword = '', $GroupByBookingRecord = 0)
        {
            global $PATH_WRT_ROOT, $intranet_root, $linventory, $Lang;
            
            if (! $linventory) {
                include_once ($intranet_root. '/includes/libinventory.php');
                $linventory = new libinventory();
            }
            
            // if(trim($BookingStatus)=='')
            if (empty($BookingStatus) || $BookingStatus == '') {
                $BookingStatus = array(
                    LIBEBOOKING_BOOKING_STATUS_REJECTED,
                    LIBEBOOKING_BOOKING_STATUS_PENDING,
                    LIBEBOOKING_BOOKING_STATUS_APPROVED,
                    LIBEBOOKING_BOOKING_STATUS_EBOOKING_TEMPORY
                );
            }
            $cond_BookingStatus = " AND bd.BookingStatus IN (" . implode(",", (array) $BookingStatus) . ") ";
            if (trim($StartDate) != '')
                $cond_StartDate = " AND br.Date >= '$StartDate' ";
            if (trim($EndDate) != '')
                $cond_EndDate = " AND br.Date <= '$EndDate' ";
                // if(trim($BookingID)!='')
            if (! empty($BookingID))
                $cond_BookingID = " AND br.BookingID IN (" . implode(",", (array) $BookingID) . ") ";
                // if(trim($FacilityID!=''))
            if (! empty($FacilityID))
                $cond_FacilityID = " AND bd.FacilityID IN (" . implode(",", (array) $FacilityID) . ") ";
            
            if ($WithName == 1) {
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $Join_Name_Table = "LEFT JOIN INTRANET_EBOOKING_ITEM AS item_name ON (bd.FacilityID = item_name.ItemID)";
                    if ($GroupByBookingRequest)
                        $FacilityNameField = ",GROUP_CONCAT(DISTINCT CONCAT(" . $linventory->getInventoryNameByLang("item_name.") . ", '<!--(', item_name.itemCode, ')-->') ORDER BY item_name.NameEng SEPARATOR'<br>')AS FacilityName"; // U114567
                                                                                                                                                                                                    // $FacilityNameField = ",GROUP_CONCAT( CONCAT(".$linventory->getInventoryNameByLang("item_name.").") SEPARATOR'<br>')AS FacilityName";
                    else
                        $FacilityNameField = ", CONCAT(" . $linventory->getInventoryNameByLang("item_name.") . ") AS FacilityName";
                } else {
                    $Join_Name_Table = "
						LEFT JOIN INVENTORY_LOCATION AS location ON (bd.FacilityID = location.LocationID)
						LEFT JOIN INVENTORY_LOCATION_LEVEL AS floor ON (location.LocationLevelID = floor.LocationLevelID)
						LEFT JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
					";
                    // $FacilityNameField = ", CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("location.").") AS FacilityName";
                    $FacilityNameField = ", " . $this->Get_Location_Name_Field("building.", "floor.", "location.") . " AS FacilityName"; 
                }
            }
            
            if ($WithUserName == 1) {
                $Join_User_Table = "
					LEFT JOIN INTRANET_USER req ON req.UserID = br.RequestedBy
					LEFT JOIN INTRANET_USER res ON res.UserID = br.ResponsibleUID
					LEFT JOIN INTRANET_USER AS pic ON (bd.PIC = pic.UserID)
				";
                
                $RequestedBy = getNameFieldByLang("req.");
                $ResponsibleUser = getNameFieldByLang("res.");
                $PIC = getNameFieldByLang("pic.");
                
                $UserNameField = "
					$RequestedBy RequestedByUserName,
					$ResponsibleUser ReponsibleUserName,
					IFNULL(" . $PIC . ",'" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System'] . "') PICUserName,
				";
            }
            
            if (! empty($FollowGroupID)) {
                $cond = '';
                foreach ((array) $FollowGroupID as $key => $follow_up_group_id) {
                    if ($cond != "") {
                        $cond .= " OR ";
                    }
                    if ($follow_up_group_id != "-999") {
                        if($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM){
                            $cond .= " item_follow_up.GroupID IN ($follow_up_group_id) ";
                        } else {
                            $cond .= " location_follow_up.GroupID IN ($follow_up_group_id) ";
                        }
                    } else {
                        $cond .= " fg.GroupID IS NULL ";
                    }
                }
                $fg_cond = " AND (" . $cond . ")";
                
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $Join_FollowUp_Group = "
						LEFT JOIN INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP as item_follow_up ON (bd.FacilityID = item_follow_up.ItemID AND bd.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ITEM . ") 
						LEFT JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP as fg ON (item_follow_up.GroupID = fg.GroupID) 
					";
                } else {
                    $Join_FollowUp_Group = "
						LEFT JOIN INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION as location_follow_up ON (bd.FacilityID = location_follow_up.LocationID  AND bd.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ROOM . ") 
						LEFT JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP as fg ON (location_follow_up.GroupID = fg.GroupID) 
					";
                }
            }
            
            if (! empty($MgmtGroupID)) {
                $cond = '';
                foreach ((array) $MgmtGroupID as $key => $mgmt_group_id) {
                    if ($cond != "") {
                        $cond .= " OR ";
                    }
                    if ($mgmt_group_id != "-999") {
                        $cond .= " mgmt_group.GroupID IN ($mgmt_group_id) ";
                    } else {
                        $cond .= " mgmt_group.GroupID IS NULL ";
                    }
                }
                $mg_cond = " AND (" . $cond . ")";
                
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $Join_FollowUp_Group = "
						LEFT JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (bd.FacilityID = item_group.ItemID)
						LEFT JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group ON (item_group.GroupID = mgmt_group.GroupID)
					";
                } else {
                    $Join_FollowUp_Group = "
						LEFT JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS room_group ON (bd.FacilityID = room_group.LocationID)
						LEFT JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group ON (room_group.GroupID = mgmt_group.GroupID)
					";
                }
            }
            
            if ($GroupByBookingRequest) {
                $GroupBy = " GROUP BY br.RelatedTo, br.StartTime, br.EndTime ";
                $Date = " GROUP_CONCAT(DISTINCT br.Date) Date ";
            } else 
                if ($GroupByBookingRecord) {
                    // $GroupBy = " GROUP BY br.BookingID ";
                    $GroupBy = " GROUP BY bd.RecordID ";
                    $Date = " br.Date ";
                } else {
                    $Date = " br.Date ";
                }
            
            if (trim($OrderBy) != '') {
                $OrderBy = " ORDER BY " . $OrderBy;
            }
            
            // Keyword condition for searchbox
            if ($Keyword != "") {
                $Keyword = $this->Get_Safe_Sql_Like_Query($Keyword);
                // # PIC / Request ppl / Responsible ppl ##
                $cond_keyword .= " (req.ChineseName like '%$Keyword%') OR (req.EnglishName like '%$Keyword%') OR ";
                $cond_keyword .= " (res.ChineseName like '%$Keyword%') OR (res.EnglishName like '%$Keyword%') OR ";
                // $cond_keyword .= " (room_pic.ChineseName like '%$keyword%') OR (room_pic.EnglishName like '%$keyword%') OR ";
                // $cond_keyword .= " (item_pic.ChineseName like '%$keyword%') OR (item_pic.EnglishName like '%$keyword%') OR ";
                
                // # Item name
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $cond_keyword .= " (item_name.NameChi like '%$Keyword%') OR (item_name.NameEng like '%$Keyword%') OR ";
                }                

                // # Room name / floor name / building name ##
                elseif ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM) {
                    $cond_keyword .= " (location.NameChi like '%$Keyword%') OR (location.NameEng like '%$Keyword%') OR ";
                    $cond_keyword .= " (floor.NameChi like '%$Keyword%') OR (floor.NameEng like '%$Keyword%') OR ";
                    $cond_keyword .= " (building.NameChi like '%$Keyword%') OR (building.NameEng like '%$Keyword%') OR ";
                }
                
                $cond_keyword .= " (br.Remark like '%$Keyword%') ";
                $cond_keyword = " AND (" . $cond_keyword . ")";
            }
            $sql ="SET SESSION group_concat_max_len = 1000000;";
            $this->db_db_query($sql);
            $sql = "SELECT
                   br.BookingID,
                   $Date,
                   br.StartTime,
                   br.EndTime,
                   br.RelatedTo,
                   br.Remark,
                   br.RequestedBy,
                   br.RequestDate,
                   br.ResponsibleUID,
                   br.PIC,
                   bd.ProcessDate,
                   br.IsReserve,
                   (UNIX_TIMESTAMP(CONCAT(br.Date, ' ',br.EndTime)) - UNIX_TIMESTAMP(CONCAT(br.Date,' ' ,br.StartTime))) AS Duration,
                   $UserNameField                    
                   bd.BookingStatus,
                   bd.FacilityID,
                   bd.FacilityType
                   $FacilityNameField,
				   br.Event,
				   br.Attachment
               FROM
                   INTRANET_EBOOKING_RECORD br
                   INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS bd ON br.BookingID = bd.BookingID
                   $Join_User_Table
                   $Join_Name_Table
                   $Join_FollowUp_Group

                   		
               WHERE
                   1
                   $cond_BookingID
                   AND br.RecordStatus = 1
                   AND bd.FacilityType = '$FacilityType'
                   $cond_FacilityID
                   $cond_BookingStatus
                   $cond_StartDate
                   $cond_EndDate
                   $fg_cond                
                   $mg_cond
				   $cond_keyword 
             
               $GroupBy
               $OrderBy
           ";
            return $this->returnArray($sql);
        }

        /*
         * list($booking_id,$period_id,$booking_remark,$booking_date_string,$booking_start_time,$booking_end_time,
         * $requested_by,$request_date,$responsible_ppl,
         * $room_id,$room_name,$room_PIC,$room_booking_process_date,$room_booking_status,
         * $item_id,$item_name,$item_PIC,$item_booking_process_date,$item_booking_status,
         * $is_reserve, $room_check_in_out_remarks, $item_check_in_out_remarks, $room_PICID, $item_PICID,
         * $room_reject_reason, $item_reject_reason, $Attachment, $room_current_status, $item_current_status,
         * $room_check_out_time, $item_check_out_time) = $arrTempResult[$i];
         *
         */
        function Get_All_Facility_Booking_Record($BookingID = '', $BookingStatus = '', $StartDate = '', $EndDate = '', $ManagementGroup = '', $keyword = '', $CheckInOutRecord = '', $FacilityType = '', $ParUserID = '', $FacilityIDAry = '', $SortField = '')
        {
            global $PATH_WRT_ROOT, $linventory, $Lang, $order, $field, $pageNo, $numPerPage;
            
            if (! $linventory) {
                include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
                $linventory = new libinventory();
            }
            
            if ($BookingStatus != "") {
                if ($BookingStatus != '-999') {
                    $cond_booking_status .= " AND (room.BookingStatus IN (" . implode(",", (array) $BookingStatus) . ") OR item.BookingStatus IN (" . implode(",", (array) $BookingStatus) . ")) ";
                }
            } else 
                if ($BookingStatus === 0 || $BookingStatus === '0') {
                    $cond_booking_status .= " AND (room.BookingStatus = '" . LIBEBOOKING_BOOKING_STATUS_PENDING . "' OR item.BookingStatus = '" . LIBEBOOKING_BOOKING_STATUS_PENDING . "') ";
                }
            
            if (trim($StartDate) != "") {
                $cond_booking_date = " AND booking_record.Date >= '$StartDate' ";
            }
            
            if (trim($EndDate) != "") {
                $cond_booking_date .= " AND booking_record.Date <= '$EndDate' ";
            }
            
            if ($ManagementGroup != "") {
                if ($ManagementGroup != "-999") {
                    $cond_mgmt_group .= " AND (mgmt_group1.GroupID = '$ManagementGroup' OR mgmt_group2.GroupID = '$ManagementGroup') ";
                } else {
                    $cond_mgmt_group .= " AND (mgmt_group1.GroupID IS NULL AND mgmt_group2.GroupID IS NULL) ";
                }
                
                $LEFT_JOIN_MGMT_GROUP = "
						LEFT JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item.FacilityID = item_group.ItemID)
						LEFT JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
						LEFT JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS room_group ON (room.FacilityID = room_group.LocationID)
						LEFT JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group2 ON (room_group.GroupID = mgmt_group2.GroupID)
				";
            }
            
            // if(trim($BookingID) != "")
            if (! empty($BookingID)) {
                $cond_BookingID = " AND booking_record.BookingID IN (" . implode(",", (array) $BookingID) . ") ";
            }
            
            if ($keyword != "") {
                // $keyword = stripslashes(stripslashes(($keyword)));
                $keyword = $this->Get_Safe_Sql_Like_Query($keyword);
                // # PIC / Request ppl / Responsible ppl ##
                $cond_keyword .= " (request_ppl.ChineseName like '%$keyword%') OR (request_ppl.EnglishName like '%$keyword%') OR ";
                $cond_keyword .= " (responsible_ppl.ChineseName like '%$keyword%') OR (responsible_ppl.EnglishName like '%$keyword%') OR ";
                $cond_keyword .= " (room_pic.ChineseName like '%$keyword%') OR (room_pic.EnglishName like '%$keyword%') OR ";
                $cond_keyword .= " (item_pic.ChineseName like '%$keyword%') OR (item_pic.EnglishName like '%$keyword%') OR ";
                // # Item Name ##
                $cond_keyword .= " (item_name.NameChi like '%$keyword%') OR (item_name.NameEng like '%$keyword%') OR ";
                $cond_keyword .= " (item_name.Barcode like '%$keyword%') OR ";
                
                // # Room name / floor name / building name ##
                $cond_keyword .= " (location.NameChi like '%$keyword%') OR (location.NameEng like '%$keyword%') OR ";
                $cond_keyword .= " (location.Barcode like '%$keyword%') OR ";
                $cond_keyword .= " (floor.NameChi like '%$keyword%') OR (floor.NameEng like '%$keyword%') OR ";
                $cond_keyword .= " (floor.Barcode like '%$keyword%') OR ";
                $cond_keyword .= " (building.NameChi like '%$keyword%') OR (building.NameEng like '%$keyword%') OR ";
                $cond_keyword .= " (building.Barcode like '%$keyword%') OR ";
                
                $cond_keyword .= " (booking_record.Remark like '%$keyword%') OR ";
                $cond_keyword .= " (booking_record.Event like '%$keyword%')";
                
                $cond_keyword = " AND (" . $cond_keyword . ")";
            }
            
            if ($CheckInOutRecord != "") {
                $cond_checkInOut = " AND (room.CurrentStatus IN (" . implode(",", (array) $CheckInOutRecord) . ") OR item.CurrentStatus IN (" . implode(",", (array) $CheckInOutRecord) . ")) ";
            }
            
            if ($FacilityType == 2) // item
{
                $cond_facility_type = " AND item.FacilityType='" . LIBEBOOKING_FACILITY_TYPE_ITEM . "' ";
                
                if ($FacilityIDAry != '') {
                    $cond_facility_id = " AND item.FacilityID IN ('" . implode("','", (array) $FacilityIDAry) . "') ";
                }
            } else 
                if ($FacilityType == 1) {
                    $cond_facility_type = " AND room.FacilityType='" . LIBEBOOKING_FACILITY_TYPE_ROOM . "' ";
                    
                    if ($FacilityIDAry != '') {
                        $cond_facility_id = " AND room.FacilityID IN ('" . implode("','", (array) $FacilityIDAry) . "') ";
                    }
                } else {}
            
            // $cond_user_id
            if (! empty($ParUserID))
                $cond_user_id = " AND (request_ppl.UserID = '$ParUserID' OR  responsible_ppl.UserID = '$ParUserID')";
                
                // order
            $Sort = $order == 1 ? "DESC" : "ASC";
            $SortField = ($SortField == '') ? $field : $SortField;
            if ($SortField == 0) {
                $OrderBy = "IF(RoomID IS NULL, ItemName, RoomName) $Sort, booking_record.Date DESC, booking_record.StartTime ASC, booking_record.EndTime ASC";
            } else {
                $OrderBy = "booking_record.Date $Sort, booking_record.StartTime ASC, booking_record.EndTime ASC";
            }
            
            // if($pageNo)
            // $n_start = ($pageNo-1)*$numPerPage;
            
            $sql = "SELECT 
						booking_record.BookingID, 
						booking_record.PeriodID, 
						booking_record.Remark,
						booking_record.Date AS DateString, 
						booking_record.StartTime, 
						booking_record.EndTime, 
						IFNULL(" . getNameFieldByLang("request_ppl.") . ", '" . $Lang['General']['UserAccountNotExists'] . "') AS RequestPerson, 
						booking_record.RequestDate, 
						IF(
							booking_record.RequestedBy <> booking_record.ResponsibleUID, 
							" . getNameFieldByLang("responsible_ppl.") . ", 
							IFNULL(" . getNameFieldByLang("request_ppl.") . ", '" . $Lang['General']['UserAccountNotExists'] . "')
						) AS ResponsiblePerson,
						room.FacilityID RoomID, 
						" . $this->Get_Location_Name_Field("building.", "floor.", "location.") . " RoomName, 
						IFNULL(" . getNameFieldByLang("room_pic.") . ",'" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System'] . "'), 
                        IFNULL(room.ProcessDateTime, DATE_FORMAT(room.ProcessDate, '%Y-%m-%d')), 
						room.BookingStatus as room_BookingStatus,
						item.FacilityID ItemID, 
						" . $linventory->getInventoryNameByLang("item_name.") . " ItemName, 
						IFNULL(" . getNameFieldByLang("item_pic.") . ",'" . $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System'] . "'), 
                        IFNULL(item.ProcessDateTime, DATE_FORMAT(item.ProcessDate, '%Y-%m-%d')), 
						item.BookingStatus as item_BookingStatus, 
						booking_record.IsReserve, 
						room.CheckInCheckOutRemark room_CheckInCheckOutRemark, 
						item.CheckInCheckOutRemark item_CheckInCheckOutRemark,
						room.PIC room_PIC,
						item.PIC item_PIC,
						room.RejectReason room_RejectReason,
						item.RejectReason item_RejectReason,
						booking_record.Attachment,
						room.CurrentStatus room_CurentStatus,
						item.CurrentStatus item_CurentStatus,
						room.CheckOutTime room_CheckOutTime,
						item.CheckOutTime item_CheckOutTime,
						booking_record.RelatedTo,
						booking_record.Event,
						booking_record.RequestedBy,
						booking_record.ResponsibleUID,
						IF (room.RecordID is null, item.RecordID, room.RecordID) BookingDetailRecordID,
						booking_record.Attachment
					FROM
						INTRANET_EBOOKING_RECORD AS booking_record
						LEFT JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room ON (booking_record.BookingID = room.BookingID AND room.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ROOM . ")
						LEFT JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS item ON (booking_record.BookingID = item.BookingID AND item.FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ITEM . ")
						LEFT JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
						LEFT JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
						LEFT JOIN INTRANET_USER AS room_pic ON (room.PIC = room_pic.UserID)
						LEFT JOIN INTRANET_USER AS item_pic ON (item.PIC = item_pic.UserID)
						LEFT JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item.FacilityID = item_name.ItemID)
						LEFT JOIN INVENTORY_LOCATION AS location ON (room.FacilityID = location.LocationID)
						LEFT JOIN INVENTORY_LOCATION_LEVEL AS floor ON (location.LocationLevelID = floor.LocationLevelID)
						LEFT JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
						$LEFT_JOIN_MGMT_GROUP
					WHERE
						booking_record.RecordStatus = 1
						$cond_BookingID
						$cond_booking_status
						$cond_mgmt_group 
						$cond_booking_date
						$cond_keyword
						$cond_checkInOut
						$cond_facility_type
						$cond_user_id
						$cond_facility_id
					ORDER BY
						/*booking_record.Date DESC, booking_record.StartTime ASC, booking_record.EndTime ASC*/
						$OrderBy
					";
            return $this->returnArray($sql);
        }

        function Get_Booking_Record_By_BookingID($BookingID, $OrderByTime = false)
        {
            $RequestedBy = getNameFieldByLang("req.");
            $ResponsibleUser = getNameFieldByLang("res.");
            $ordering = $OrderByTime ? ', br.DateInput DESC' : '';
            
            $sql = "
				SELECT
					br.BookingID,
					br.Date,
					br.StartTime,
					br.EndTime,
					br.RequestedBy,
					br.ResponsibleUID,
					$RequestedBy RequestedByUserName,
					$ResponsibleUser ReponsibleUserName,
					br.Remark,
					br.Attachment,
					br.RelatedTo
				FROM
					INTRANET_EBOOKING_RECORD br
					LEFT JOIN INTRANET_USER req ON req.UserID = br.RequestedBy
					LEFT JOIN INTRANET_USER res ON res.UserID = br.ResponsibleUID
				WHERE
					br.BookingID IN (" . implode(",", (array) $BookingID) . ")
				ORDER BY
					br.Date, br.StartTime $ordering
			";
            return $this->returnArray($sql);
        }

        function Get_Related_Booking_Record_By_BookingID($BookingID = '', $BookingStatus = '', $RecordStatusAry = 1, $dateRangeAry = '')
        {
            // $RequestedBy = getNameFieldByLang("req.");
            // $ResponsibleUser = getNameFieldByLang("res.");
            if ($BookingID != '') {
                $cond_BookingID = " AND br2.BookingID IN (" . implode(",", (array) $BookingID) . ") ";
            }
            
            if (trim($BookingStatus) != '')
                $cond_BookingStatus = " AND br.BookingStatus IN (" . implode(",", (array) $BookingStatus) . ") ";
            
            if ($RecordStatusAry === '') {
                // get records of all status
            } else {
                $cond_RecordStatus = " AND br.RecordStatus In ('" . implode("','", (array) $RecordStatusAry) . "') ";
            }
            
            if ($dateRangeAry != '') {
                $startDate = $dateRangeAry['StartDate'];
                $endDate = $dateRangeAry['EndDate'];
                
                $cond_Date = " AND '" . $startDate . "' <= br.Date AND br.Date <= '" . $endDate . "' ";
            }
            
            $sql = "
				SELECT
					br.BookingID,
					br.Date,
					br.StartTime,
					br.EndTime,
					br.RequestedBy,
					br.ResponsibleUID,
					br.Remark,
					br.Attachment,
					br.BookingStatus,
					br.DeleteOtherRelatedIfReject,
					br.RelatedTo,
					br.Event,
					br.ApplyDoorAccess,
					br.RecordStatus,
					iec.CategoryName
				FROM
					INTRANET_EBOOKING_RECORD br2
					INNER JOIN INTRANET_EBOOKING_RECORD br ON br.RelatedTo = br2.RelatedTo
					LEFT OUTER JOIN INTRANET_EBOOKING_RECORD_CATEGORY_RELATION iercr ON iercr.BookingID = br2.BookingID
					LEFT OUTER JOIN INTRANET_EBOOKING_CATEGORY iec ON iercr.CategoryID = iec.CategoryID 
				WHERE
					1
					$cond_BookingID
					$cond_BookingStatus	
					$cond_RecordStatus
					$cond_Date
				ORDER BY
					br.Date, br.StartTime
			";
            $Result = $this->returnArray($sql) or die(mysql_error());
            
            return $Result;
        }

        function In_Time_Period($StartTime, $EndTime, $TimePeroidArr)
        {
            foreach ((array) $TimePeroidArr as $key => $time_range) {
                $available_start_time = substr($time_range, 0, strpos($time_range, '-'));
                $available_end_time = substr($time_range, strpos($time_range, '-') + 1, strlen($time_range));
                $ts_available_start_time = strtotime($available_start_time);
                $ts_available_end_time = strtotime($available_end_time);
                
                $BookStartTimets = strtotime($StartTime);
                $BookEndTimets = strtotime($EndTime);
                
                if ($ts_available_start_time <= $BookStartTimets && $BookStartTimets <= $ts_available_end_time && $BookEndTimets >= $ts_available_start_time && $BookEndTimets <= $ts_available_end_time) {
                    return true;
                }
            }
            return false;
        }

        function Is_Time_Clash($StartTime, $EndTime, $TimePeroidArr)
        {
            foreach ((array) $TimePeroidArr as $key => $time_range) {
                $available_start_time = substr($time_range, 0, strpos($time_range, '-'));
                $available_end_time = substr($time_range, strpos($time_range, '-') + 1, strlen($time_range));
                $ts_available_start_time = strtotime($available_start_time);
                $ts_available_end_time = strtotime($available_end_time);
                
                $BookStartTimets = strtotime($StartTime);
                $BookEndTimets = strtotime($EndTime);
                
                if (($BookStartTimets >= $ts_available_start_time && $BookStartTimets < $ts_available_end_time) || ($BookEndTimets > $ts_available_start_time && $BookEndTimets <= $ts_available_end_time) || ($BookStartTimets <= $ts_available_start_time && $BookEndTimets >= $ts_available_end_time)) {
                    return true;
                }
            }
            return false;
        }

        function Log_Delete_Action($DeleteItemName)
        {
            $sql = "
				INSERT INTO	INTRANET_EBOOKING_DELETE_LOG
					(DeleteItemName, DeleteBy, DeleteDate)
				VALUES
					('$DeleteItemName', '" . $_SESSION['UserID'] . "', NOW())
			";
            
            $result = $this->db_db_query($sql);
            return $result;
        }

        function Remove_Facility_Booking_Period($AcademicYearID, $FacilityType, $FacilityID)
        {
            $Result['SpecialBookingPeriod'] = $this->Remove_Facility_Special_Booking_Period($AcademicYearID, $FacilityType, $FacilityID);
            $Result['TimezoneBookingPeriod'] = $this->Remove_Facility_Timezone_Booking_Period('', $AcademicYearID, $FacilityType, $FacilityID);
            $Result["Log"] = $this->Log_Delete_Action("Remove_Facility_Booking_Period");
            
            return ! in_array(false, $Result) ? true : false;
        }

        function Remove_Facility_Special_Booking_Period($AcademicYearID, $FacilityType, $FacilityID, $IsSuspension = 0)
        {
            $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($AcademicYearID);
            $StartDate = $AcademicYearInfo['StartDate'];
            $EndDate = $AcademicYearInfo['EndDate'];
            $date_cond = " (RecordDate Between '$StartDate' AND '$EndDate') ";
            
            list ($RelatedSubLocationID, $RelatedItemID) = $this->Process_FacilityID($FacilityType, $FacilityID);
            $facility_cond = " AND (RelatedSubLocationID = '$RelatedSubLocationID' AND RelatedItemID = '$RelatedItemID') ";
            
            $Table = $IsSuspension ? "INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION" : "INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD";
            
            $sql = "
				DELETE FROM 
					$Table 
				WHERE 
					$date_cond 
					$facility_cond
			";
            $result["Delete"] = $this->db_db_query($sql);
            $Log = ($IsSuspension ? "Remove_Facility_Suspension" : "Remove_Facility_Special_Booking_Period" . "\n");
            $Log .= "$StartDate,$EndDate,$RelatedSubLocationID,$RelatedItemID ";
            $result["Log"] = $this->Log_Delete_Action($Log);
            
            return ! in_array(false, $result) ? true : false;
        }

        function Remove_Facility_Timezone_Booking_Period($TimezoneSettingID = '', $AcademicYearID = '', $FacilityType = '', $FacilityID = '')
        {
            $result = array();
            if (trim($TimezoneSettingID) != '') {
                $TimezoneSettingIDArr = $TimezoneSettingID;
            } else 
                if (trim($AcademicYearID) != '' && trim($FacilityType) != '' && trim($FacilityID) != '') {
                    $TimezoneArr = $this->Get_All_TimeZone_By_Academic_Year($AcademicYearID);
                    if ($TimezoneArr) {
                        $TimezoneIDArr = Get_Array_By_Key($TimezoneArr, "PeriodID");
                        $cond_TimezoneID = " AND tz.TimezoneID IN (" . implode(",", (array) $TimezoneIDArr) . ") ";
                        $sql = "
						SELECT
							tz.TimezoneSettingID
						FROM
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING tz
						WHERE
							tz.FacilityType = '$FacilityType'
							AND tz.FacilityID = '$FacilityID'
							$cond_TimezoneID
					";
                        $TimezoneSettingIDArr = $this->returnVector($sql);
                    }
                } else
                    return false;
            
            if ($TimezoneSettingIDArr) {
                $sql = "
					DELETE FROM 
						INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING
					WHERE
						TimezoneSettingID IN (" . implode(",", (array) $TimezoneSettingIDArr) . ")
				";
                $result["DeleteTimezoneSetting"] = $this->db_db_query($sql);
                
                $sql = "
					DELETE FROM 
						INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT
					WHERE
						TimezoneSettingID IN (" . implode(",", (array) $TimezoneSettingIDArr) . ")
				";
                $result["DeleteTimezoneSettingTimeslot"] = $this->db_db_query($sql);
            }
            
            $result["Log"] = $this->Log_Delete_Action("Remove_Facility_Timezone_Booking_Period");
            
            return ! in_array(false, $result) ? true : false;
        }

        function Copy_Booking_Period_Setting($AcademicYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr)
        {
            $Result['CopySpecialBookingPeriod'] = $this->Copy_Special_Booking_Period_Setting($AcademicYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr);
            $Result['CopyBookingDaySuspension'] = $this->Copy_Booking_Day_Suspension_Setting($AcademicYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr);
            $Result['CopyTimezoneBookingPeriod'] = $this->Copy_Timezone_Booking_Period_Setting($AcademicYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr);
            $Result['DeleteLog'] = $this->Log_Delete_Action("Copy_Booking_Period_Setting");
            
            return ! in_array(false, $Result) ? true : false;
        }

        function Copy_Timezone_Booking_Period_Setting($SchoolYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr)
        {
            $TimezoneArr = $this->Get_All_TimeZone_By_Academic_Year($SchoolYearID);
            if (empty($TimezoneArr))
                return false;
            
            $TimezoneIDArr = Get_Array_By_Key($TimezoneArr, "PeriodID");
            $cond_TimezoneID = " AND tz.TimezoneID IN (" . implode(",", (array) $TimezoneIDArr) . ") ";
            $sql = "
				SELECT
					tz.TimezoneSettingID,
					tz.TimezoneID,
					tz.FacilityType,
					tz.FacilityID,
					tz.SpecialTimetableSettingsID
				FROM
					INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING tz
				WHERE
					tz.FacilityType = '$CopyFromType'
					AND tz.FacilityID = '$CopyFromFacilityID'
					$cond_TimezoneID
			";
            $TimezoneSettingArr = $this->returnArray($sql);
            
            $SizeOfCopyTo = sizeof($CopyToArr);
            for ($i = 0; $i < $SizeOfCopyTo; $i ++) {
                list ($thisFacilityType, $thisFacilityID) = $CopyToArr[$i];
                
                $Result["Remove_" . $thisFacilityType . "_" . $thisFacilityID] = $this->Remove_Facility_Timezone_Booking_Period('', $SchoolYearID, $thisFacilityType, $thisFacilityID);
                
                foreach ($TimezoneSettingArr as $thisSetting) {
                    list ($FromTimezoneSettingID, $TimezoneID, $FromFacilityType, $FromFacilityID, $SpecialTimetableSettingsID) = $thisSetting;
                    $sql = "
						INSERT INTO	INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING
							(TimezoneID, FacilityType, FacilityID, SpecialTimetableSettingsID, ModifiedBy, DateModified)
						SELECT
							TimezoneID, '$thisFacilityType', '$thisFacilityID', SpecialTimetableSettingsID, '" . $_SESSION['UserID'] . "', NOW()
						FROM
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING
						WHERE
							TimezoneSettingID = '$FromTimezoneSettingID'
					";
                    
                    $Result["CopyTimezoneSetting_" . $thisFacilityType . "_" . $thisFacilityID] = $this->db_db_query($sql);
                    $NewSettingID = mysql_insert_id();
                    
                    $sql = "
						INSERT INTO	INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT
							(TimezoneSettingID, Day, TimeslotID)
						SELECT
							'$NewSettingID', Day, TimeslotID
						FROM
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT
						WHERE
							TimezoneSettingID = '$FromTimezoneSettingID'
					";
                    
                    $Result["CopyTimezoneSettingTimeslot_" . $thisFacilityType . "_" . $thisFacilityID] = $this->db_db_query($sql);
                }
            }
            
            return ! in_array(false, (array) $Result) ? true : false;
        }

        function Copy_Special_Booking_Period_Setting($SchoolYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr)
        {
            $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($SchoolYearID);
            $SchoolYearStartDate = $AcademicYearInfo['StartDate'];
            $SchoolYearEndDate = $AcademicYearInfo['EndDate'];
            
            $date_cond = " (RecordDate Between '$SchoolYearStartDate' AND '$SchoolYearEndDate') ";
            
            $SizeOfCopyTo = sizeof($CopyToArr);
            for ($i = 0; $i < $SizeOfCopyTo; $i ++) {
                list ($thisFacilityType, $thisFacilityID) = $CopyToArr[$i];
                
                $Result["Remove_" . $thisFacilityType . "_" . $thisFacilityID] = $this->Remove_Facility_Special_Booking_Period($SchoolYearID, $thisFacilityType, $thisFacilityID);
                
                if ($CopyFromType == 1) {
                    $copy_cond = " AND ((RelatedSubLocationID = '$CopyFromFacilityID') AND (RelatedItemID = 0)) ";
                } else {
                    $copy_cond = " AND ((RelatedSubLocationID = 0) AND (RelatedItemID = '$CopyFromFacilityID')) ";
                }
                // # Copy
                $sql = "SELECT RecordDate, TimeSlotID, StartTime, EndTime, RepeatType, RepeatValue, RelatedPeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE $date_cond $copy_cond";
                $arrCopyInfo = $this->returnArray($sql, 7);
                $SizeOfCopyInfo = sizeof($arrCopyInfo);
                $insert_value = array();
                if ($SizeOfCopyInfo > 0) // copy only if record exist
{
                    for ($j = 0; $j < $SizeOfCopyInfo; $j ++) {
                        list ($record_date, $time_slot_id, $start_time, $end_time, $repeat_type, $repeat_value, $related_period_id) = $arrCopyInfo[$j];
                        if ($repeat_value == "") {
                            $repeat_value = "NULL";
                        }
                        if ($thisFacilityType == 1) {
                            $insert_value[] = "('$record_date', $time_slot_id, '$start_time', '$end_time', $repeat_type, '$repeat_value', '$related_period_id', 0, '$thisFacilityID', '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', NOW(), NOW())";
                        } else {
                            $insert_value[] = "('$record_date', $time_slot_id, '$start_time', '$end_time', $repeat_type, '$repeat_value', '$related_period_id', '$thisFacilityID', 0, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', NOW(), NOW())";
                        }
                    }
                    
                    $str_insert_value = implode(",", $insert_value);
                    
                    // # Parse
                    $sql = "INSERT INTO 
							INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD
							(RecordDate, TimeSlotID, StartTime, EndTime, RepeatType, RepeatValue, RelatedPeriodID, 
								RelatedItemID,RelatedSubLocationID,InputBy,ModifiedBy,DateInput,DateModified)
							VALUES $str_insert_value";
                    $Result['CopyAndParse_' . $thisFacilityType . "_" . $thisFacilityID] = $this->db_db_query($sql);
                    
                    // # relink the RelatedPeriodID
                    if ($thisFacilityType == 1) {
                        $sub_update_cond = " b.RelatedSubLocationID = '$thisFacilityID' ";
                        $update_cond = " a.RelatedSubLocationID = '$thisFacilityID' ";
                    } else {
                        $sub_update_cond = " b.RelatedItemID = '$thisFacilityID' ";
                        $update_cond = " a.RelatedItemID = '$thisFacilityID' ";
                    }
                    $sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD
							as a LEFT JOIN (SELECT 
												b.RelatedPeriodID, MIN(b.PeriodID) as NewRelatedPeriodID
											FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD as b
											WHERE 
												$sub_update_cond GROUP BY b.RelatedPeriodID) as c ON (a.RelatedPeriodID = c.RelatedPeriodID)
							SET a.RelatedPeriodID = c.NewRelatedPeriodID where $update_cond";
                    $Result['UpdateRelatedPeriodID' . $thisFacilityType . "_" . $thisFacilityID] = $this->db_db_query($sql);
                }
            }
            
            return ! in_array(false, (array) $Result) ? true : false;
        }

        function Copy_Booking_Day_Suspension_Setting($SchoolYearID, $CopyFromType, $CopyFromFacilityID, $CopyToArr)
        {
            $AcademicYearInfo = $this->Get_Period_Of_Academic_Year($SchoolYearID);
            $SchoolYearStartDate = $AcademicYearInfo['StartDate'];
            $SchoolYearEndDate = $AcademicYearInfo['EndDate'];
            
            $date_cond = " (RecordDate Between '$SchoolYearStartDate' AND '$SchoolYearEndDate') ";
            
            $SizeOfCopyTo = sizeof($CopyToArr);
            for ($i = 0; $i < $SizeOfCopyTo; $i ++) {
                list ($thisFacilityType, $thisFacilityID) = $CopyToArr[$i];
                
                $Result["Remove_" . $thisFacilityType . "_" . $thisFacilityID] = $this->Remove_Facility_Special_Booking_Period($SchoolYearID, $thisFacilityType, $thisFacilityID, 1);
                
                if ($CopyFromType == 1) {
                    $copy_cond = " AND ((RelatedSubLocationID = '$CopyFromFacilityID') AND (RelatedItemID = 0)) ";
                } else {
                    $copy_cond = " AND ((RelatedSubLocationID = 0) AND (RelatedItemID = '$CopyFromFacilityID')) ";
                }
                // # Copy
                $sql = "SELECT RecordDate, RepeatType, RepeatValue, RelatedPeriodID FROM INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION WHERE $date_cond $copy_cond";
                $arrCopyInfo = $this->returnArray($sql, 7);
                $SizeOfCopyInfo = sizeof($arrCopyInfo);
                $insert_value = array();
                if ($SizeOfCopyInfo > 0) // copy only if record exist
{
                    for ($j = 0; $j < $SizeOfCopyInfo; $j ++) {
                        list ($record_date, $repeat_type, $repeat_value, $related_period_id) = $arrCopyInfo[$j];
                        if ($repeat_value == "") {
                            $repeat_value = "NULL";
                        }
                        if ($thisFacilityType == 1) {
                            $insert_value[] = "('$record_date', $repeat_type, '$repeat_value', '$related_period_id', 0, '$thisFacilityID', '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', NOW(), NOW())";
                        } else {
                            $insert_value[] = "('$record_date', $repeat_type, '$repeat_value', '$related_period_id', '$thisFacilityID', 0, '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', NOW(), NOW())";
                        }
                    }
                    
                    $str_insert_value = implode(",", $insert_value);
                    
                    // # Parse
                    $sql = "INSERT INTO 
							INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION
							(RecordDate, RepeatType, RepeatValue, RelatedPeriodID, 
								RelatedItemID,RelatedSubLocationID,InputBy,ModifiedBy,DateInput,DateModified)
							VALUES $str_insert_value";
                    $Result['CopyAndParse_' . $thisFacilityType . "_" . $thisFacilityID] = $this->db_db_query($sql);
                    
                    // # relink the RelatedPeriodID
                    if ($thisFacilityType == 1) {
                        $sub_update_cond = " b.RelatedSubLocationID = '$thisFacilityID' ";
                        $update_cond = " a.RelatedSubLocationID = '$thisFacilityID' ";
                    } else {
                        $sub_update_cond = " b.RelatedItemID = '$thisFacilityID' ";
                        $update_cond = " a.RelatedItemID = '$thisFacilityID' ";
                    }
                    $sql = "UPDATE INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION
							as a LEFT JOIN (SELECT 
												b.RelatedPeriodID, MIN(b.PeriodID) as NewRelatedPeriodID
											FROM INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION as b
											WHERE 
												$sub_update_cond GROUP BY b.RelatedPeriodID) as c ON (a.RelatedPeriodID = c.RelatedPeriodID)
							SET a.RelatedPeriodID = c.NewRelatedPeriodID where $update_cond";
                    $Result['UpdateRelatedPeriodID' . $thisFacilityType . "_" . $thisFacilityID] = $this->db_db_query($sql);
                }
            }
            
            return ! in_array(false, (array) $Result) ? true : false;
        }

        function Get_Period_Of_Academic_Year($SchoolYearID)
        {
            if (! $this->Cache_Period_Of_Academic_Year_Arr[$SchoolYearID]) {
                $this->Cache_Period_Of_Academic_Year($SchoolYearID);
                // $AcademicYearInfo = getPeriodOfAcademicYear($SchoolYearID);
                // $this->Cache_Period_Of_Academic_Year_Arr[$SchoolYearID] = $AcademicYearInfo;
            }
            return $this->Cache_Period_Of_Academic_Year_Arr[$SchoolYearID];
        }

        function Cache_Period_Of_Academic_Year($SchoolYearID)
        {
            $AcademicYearInfo = getPeriodOfAcademicYear($SchoolYearID);
            $this->Cache_Period_Of_Academic_Year_Arr[$SchoolYearID] = $AcademicYearInfo;
        }

        function Get_AcademicYearID_By_Date($Date)
        {
            if (! $AcademicYearID = $this->Find_AcademicYearID_By_Date_In_Cache($Date)) {
                $YearInfo = getAcademicYearInfoAndTermInfoByDate($Date);
                $AcademicYearID = $YearInfo[0];
                $this->Cache_Period_Of_Academic_Year($AcademicYearID);
            }
            
            return $AcademicYearID;
        }

        function Find_AcademicYearID_By_Date_In_Cache($Date)
        {
            if (! isset($this->Cache_Period_Of_Academic_Year_Arr))
                return '';
            
            foreach ((array) $this->Cache_Period_Of_Academic_Year_Arr as $AcademicYearID => $AcademicYearInfo) {
                list ($StartDate, $EndDate) = $AcademicYearInfo;
                
                // if(compareDate($Date,$StartDate)>=0 && compareDate($Date,$EndDate)<=0)
                if (strtotime($Date) >= strtotime($StartDate) && strtotime($Date) <= strtotime($EndDate))
                    return $AcademicYearID;
            }
            return '';
        }

        function Format_Display_Date($DateString)
        {
            return Date("Y-m-d (D)", strtotime($DateString));
        }

        function Format_Display_Time_Period($DateArr)
        {
            $StartTime = Date("H:i", strtotime($DateArr['StartTime']));
            $EndTime = Date("H:i", strtotime($DateArr['EndTime']));
            
            return $StartTime . " - " . $EndTime;
        }

        function Facility_Time_Range_Intersect($DateRangeArr)
        {
            foreach ($DateRangeArr as $FacilityID => $DateArray) {
                // $GroupTimeRange = BuildMultiKeyAssoc($DateArray, array("RecordDate"),array("StartTime","EndTime"),'',1);
                $GroupTimeRange = $DateArray;
                if ($IntersectTimeArr) {
                    $IntersectTimeArr = Time_Range_Array_Intersect($IntersectTimeArr, $GroupTimeRange);
                    if (empty($IntersectTimeArr))
                        return array();
                } else 
                    if (! empty($GroupTimeRange)) // first facility time range
                        $IntersectTimeArr = $GroupTimeRange;
                    else
                        return array();
            }
            
            return $IntersectTimeArr;
        }

        function Get_FollowUp_Page_Group_And_Display_Color($LocationIDArr = '', $ItemIDArr = '', $FollowGroupArr = '')
        {
            if (! empty($FollowGroupArr)) {
                $cond = '';
                foreach ($FollowGroupArr as $key => $follow_up_group_id) {
                    if ($cond != "") {
                        $cond .= " OR ";
                    }
                    if ($follow_up_group_id != "-999") {
                        if(! empty($LocationIDArr)){
                            $cond .= " room_relation.GroupID IN ($follow_up_group_id) ";
                        } else if (! empty($ItemIDArr)){
                            $cond .= " item_relation.GroupID IN ($follow_up_group_id) ";
                        }
                    } else {
                        $cond .= " fg.GroupID IS NULL ";
                    }
                }
                $fg_cond = " AND (" . $cond . ")";
            }
            
            if (! empty($LocationIDArr)) {
                $JOIN_LOCATION_TABLE = "LEFT OUTER JOIN INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION as room_relation on 
					fg.GroupID = room_relation.GroupID
					AND room_relation.LocationID IN (" . implode(",", (array) $LocationIDArr) . ")
				";
            }
            
            if (! empty($ItemIDArr)) {
                $JOIN_ITEM_TABLE = "LEFT OUTER JOIN INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP as item_relation on 
					fg.GroupID = item_relation.GroupID
					AND item_relation.ItemID IN (" . implode(",", (array) $ItemIDArr) . ")
				";
            }
            
            $sql = "
				SELECT 
					Distinct fg.GroupColor, fg.GroupName
				FROM 
					INTRANET_EBOOKING_FOLLOWUP_GROUP as fg 
					$JOIN_LOCATION_TABLE
					$JOIN_ITEM_TABLE
				WHERE
					1 
					$fg_cond
				ORDER BY
					fg.GroupName
			";
            
            $Result = $this->returnArray($sql, '', 1);
            
            $color = $Result[0]['GroupColor'] ? $Result[0]['GroupColor'] : '#000000';
            $GroupArr = Get_Array_By_Key($Result, "GroupName");
            
            return array(
                $color,
                $GroupArr
            );
        }

        function Get_DateRange_By_DateType($BookingDateType)
        {
            $StartDate = $EndDate = '';
            $curr_date = date("Y-m-d");
            switch ($BookingDateType) {
                case "Future":
                    $StartDate = $curr_date;
                    break;
                
                case "Current":
                    $curr_year = substr($curr_date, 0, 4);
                    $curr_month = substr($curr_date, 5, 2);
                    $curr_day = substr($curr_date, 8, 2);
                    $StartDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day, $curr_year));
                    $EndDate = date("Y-m-d", mktime(23, 59, 59, $curr_month, $curr_day, $curr_year));
                    break;
                
                case "ThisWeek":
                    $curr_weekday = date("w");
                    
                    $curr_year = substr($curr_date, 0, 4);
                    $curr_month = substr($curr_date, 5, 2);
                    $curr_day = substr($curr_date, 8, 2);
                    
                    $StartDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday, $curr_year));
                    $EndDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday + 6, $curr_year));
                    break;
                
                case "NextWeek":
                    $curr_weekday = date("w");
                    
                    $curr_year = substr($curr_date, 0, 4);
                    $curr_month = substr($curr_date, 5, 2);
                    $curr_day = substr($curr_date, 8, 2);
                    
                    $StartDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday + 7, $curr_year));
                    $EndDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day - $curr_weekday + 6 + 7, $curr_year));
                    break;
                
                case "ThisMonth":
                    $curr_weekday = date("w");
                    
                    $curr_year = substr($curr_date, 0, 4);
                    $curr_month = substr($curr_date, 5, 2);
                    $curr_day = substr($curr_date, 8, 2);
                    
                    $last_day_of_month = date("t");
                    
                    $StartDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, 1, $curr_year));
                    $EndDate = date("Y-m-d", mktime(0, 0, 0, $curr_month, $last_day_of_month, $curr_year));
                    break;
                
                case "NextMonth":
                    $curr_weekday = date("w");
                    
                    $curr_year = substr($curr_date, 0, 4);
                    $curr_month = substr($curr_date, 5, 2);
                    $curr_day = substr($curr_date, 8, 2);
                    
                    $last_day_of_month = date("t", mktime(0, 0, 0, $curr_month + 1, 1, $curr_year));
                    
                    $StartDate = date("Y-m-d", mktime(0, 0, 0, $curr_month + 1, 1, $curr_year));
                    $EndDate = date("Y-m-d", mktime(0, 0, 0, $curr_month + 1, $last_day_of_month, $curr_year));
                    break;
                
                case "Past":
                    $EndDate = $curr_date;
                    break;
            }
            
            return array(
                $StartDate,
                $EndDate
            );
        }

        function Get_FacilityID_by_BookingID($BookingID)
        {
            $sql = "
				SELECT 
					FacilityType, 
					FacilityID 
				FROM 
					INTRANET_EBOOKING_BOOKING_DETAILS 
				WHERE 
				    BookingID = '$BookingID'
			";
            
            return $this->returnArray($sql);
        }

        function Update_Related_Booking_Detail($RelatedToID, $BookingRemarks, $FolderPath, $iCalEventUpdateArr = '', $CustRemarkAry = array(), $BookingEvent = '', $ApplyDoorAccess = 0, $ResponsibleUID = '')
        {
            if ($ResponsibleUID != '') {
                $ResponsibleUID_field = ", br.ResponsibleUID = '" . $ResponsibleUID . "' ";
            } else {
                $ResponsibleUID_field = "";
            }
            
            $sql = "
				UPDATE 
					INTRANET_EBOOKING_RECORD br
					INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS bd ON br.BookingID = bd.BookingID
				SET
					br.Remark = '" . $this->Get_Safe_Sql_Query($BookingRemarks) . "',
					br.Attachment = '" . $FolderPath . "',
					br.Event = '" . $this->Get_Safe_Sql_Query($BookingEvent) . "',
					br.ApplyDoorAccess = '" . $ApplyDoorAccess . "'
					$ResponsibleUID_field
				WHERE
					br.RelatedTo = '$RelatedToID' 
			";
            
            $Success['UpdateBookingDetail'] = $this->db_db_query($sql);
            if ($iCalEventUpdateArr)
                $Success['UpdateiCalEvent'] = $this->Update_iCal_Event_Detail($RelatedToID, $iCalEventUpdateArr);
                
                // add cust remarks
            if ($Success['UpdateBookingDetail'] && ! empty($CustRemarkAry)) {
                $sql = "delete from INTRANET_EBOOKING_RECORD_CUST_REMARK where BookingID='$RelatedToID'";
                $this->db_db_query($sql);
                
                foreach ($CustRemarkAry as $fields[] => $data[]);
                $fields_str = implode(",", $fields);
                foreach ($data as $k => $d)
                    $data_str .= ",'" . $this->Get_Safe_Sql_Query($d) . "'";
                
                $sql = "insert into INTRANET_EBOOKING_RECORD_CUST_REMARK ";
                $sql .= " (BookingID, $fields_str) values ";
                $sql .= "('$RelatedToID'" . $data_str . ")";
                $this->db_db_query($sql);
            }
            
            return ! in_array(false, $Success);
        }

        function Check_New_eInventory_Item()
        {
            $sql = "
				SELECT 
					COUNT(*)
				FROM
					INVENTORY_ITEM
				WHERE
					ItemType = 1
					AND TransferredToEbooking = 0
			";
            $Result = $this->returnVector($sql);
            
            return $Result[0];
        }

        function Save_General_Setting($SettingsNameValueArr)
        {
            if (count($SettingsNameValueArr) == 0 || ! is_array($SettingsNameValueArr))
                return false;
            
            $this->Start_Trans();
            
            $SettingsNameArr = array_keys($SettingsNameValueArr);
            
            $Success['Remove'] = $this->Remove_General_Settings($SettingsNameArr);
            $Success['Add'] = $this->Add_General_Settings($SettingsNameValueArr);
            
            if (in_array(false, $Success)) {
                $this->RollBack_Trans();
                return false;
            } else {
                $this->Commit_Trans();
                return true;
            }
        }

        function Remove_General_Settings($SettingsNameArr)
        {
            if (count($SettingsNameArr) == 0)
                return false;
            
            $SettingsNameSql = "'" . implode("','", (array) $SettingsNameArr) . "'";
            
            $sql = "
				DELETE FROM
					INTRANET_EBOOKING_SETTING	 
				WHERE
					SettingName IN (" . $SettingsNameSql . ")
			";
            
            $Success = $this->db_db_query($sql);
            
            return $Success;
        }

        function Add_General_Settings($SettingsNameValueArr)
        {
            if (count($SettingsNameValueArr) == 0 || ! is_array($SettingsNameValueArr))
                return false;
            
            foreach ((array) $SettingsNameValueArr as $SettingName => $SettingValue) {
                $InsertSqlArr[] = "('$SettingName','$SettingValue', '" . $_SESSION['UserID'] . "', NOW())";
            }
            
            if (count($InsertSqlArr) > 0) {
                $InsertSql = implode(',', $InsertSqlArr);
                
                $sql = "
					INSERT INTO	INTRANET_EBOOKING_SETTING
						(SettingName, SettingValue, LastModifiedBy, DateModified)	 
					VALUES
						$InsertSql
				";
                
                $Success = $this->db_db_query($sql);
                return $Success;
            } else
                return false;
        }

        function Load_General_Setting()
        {
            if (! $this->SettingArr) {
                $sql = "SELECT * FROM INTRANET_EBOOKING_SETTING";
                $Result = $this->returnArray($sql);
                $this->SettingArr = BuildMultiKeyAssoc($Result, "SettingName", "SettingValue", 1);
                
                // Update SettingArr if certain values has not been set yet
                $this->Set_Default_Setting();
            }
        }
        
        // Rita Modified 20120621
        function Set_Default_Setting()
        {
            // fix
            if (! isset($this->SettingArr['AllowBookingStatus']))
                $this->SettingArr['AllowBookingStatus'] = LIBEBOOKING_ALLOW_BOOKING_STATUS_ALLOW;
            
            if (! isset($this->SettingArr['bookingTypeSingleEnabled']) && ! isset($this->SettingArr['bookingTypePeriodicEnabled'])) {
                $this->SettingArr['bookingTypeSingleEnabled'] = 1;
                $this->SettingArr['bookingTypePeriodicEnabled'] = 1;
            }
            
            if (! isset($this->SettingArr['DefaultBookingType']))
                $this->SettingArr['DefaultBookingType'] = LIBEBOOKING_DEFAULT_BOOKING_TYPE_SINGLE;
            
            if (! isset($this->SettingArr['SpecificTimeEnabled']) && ! isset($this->SettingArr['SpecificPeriodEnabled'])) {
                $this->SettingArr['SpecificTimeEnabled'] = 1;
                $this->SettingArr['SpecificPeriodEnabled'] = 1;
            }
            
            if (! isset($this->SettingArr['DefaultBookingMethod']))
                $this->SettingArr['DefaultBookingMethod'] = LIBEBOOKING_DEFAULT_BOOKING_METHOD_PERIOD;
            
            if (! isset($this->SettingArr['WeekDayEnabled']) && ! isset($this->SettingArr['CycleEnabled'])) {
                $this->SettingArr['WeekDayEnabled'] = 1;
                $this->SettingArr['CycleEnabled'] = 1;
            }
            
            if (! isset($this->SettingArr['DefaultPeriodicBookingMethod']))
                $this->SettingArr['DefaultPeriodicBookingMethod'] = LIBEBOOKING_DEFAULT_PERIODIC_BOOKING_METHOD;
            
            if (! isset($this->SettingArr['DisplayBuildingName']))
                $this->SettingArr['DisplayBuildingName'] = 1;
            
            if (! isset($this->SettingArr['DisplayFloorName']))
                $this->SettingArr['DisplayFloorName'] = 1;
            
            if (! isset($this->SettingArr['BookingNotification']))
                $this->SettingArr['BookingNotification'] = 1;
            
            if (! isset($this->SettingArr['DefaultApplyDoorAccess']) || $this->SettingArr['DefaultApplyDoorAccess'] == '') {
                $this->SettingArr['DefaultApplyDoorAccess'] = LIBEBOOKING_BOOKING_DEFAULT_DOOR_ACCESS_DISABLE;
            }
            
            if (! isset($this->SettingArr['EventInICalendar'])) {
                $this->SettingArr['EventInICalendar'] = LIBEBOOKING_BOOKING_EVENT_IN_ICALENDAR_YES;
            }
            
            if (! isset($this->SettingArr['AllowToBookDamagedItem'])) {
                $this->SettingArr['AllowToBookDamagedItem'] = LIBEBOOKING_ALLOW_TO_BOOK_DAMAGED_ITEM_YES;
            }
            
            if (! isset($this->SettingArr['AllowToBookRepairingItem'])) {
                $this->SettingArr['AllowToBookRepairingItem'] = LIBEBOOKING_ALLOW_TO_BOOK_REPAIRING_ITEM_YES;
            }
            
            if (! isset($this->SettingArr['ReceiveEmailNotificationFromeInventory'])) {
                $this->SettingArr['ReceiveEmailNotificationFromeInventory'] = LIBEBOOKING_RECEIVE_EMAIL_NOTIFICATION_FROM_EINVENTORY_NO;
            }
            
            // Bill added: for email notification to approval group
            if (! isset($this->SettingArr['ApprovalNotification'])) {
                $this->SettingArr['ApprovalNotification'] = LIBEBOOKING_BOOKING_NOTIFICATION_APPROVAL_DISABLE;
            }
        }

        function Check_Allow_Booking_Status()
        {
            $this->Load_General_Setting();
            if ($this->SettingArr['AllowBookingStatus'] == LIBEBOOKING_ALLOW_BOOKING_STATUS_ALLOW)
                return true;
            else 
                if ($this->SettingArr['AllowBookingStatus'] == LIBEBOOKING_ALLOW_BOOKING_STATUS_SUSPEND)
                    return false;
                else 
                    if ($this->SettingArr['AllowBookingStatus'] == LIBEBOOKING_ALLOW_BOOKING_STATUS_SUSPEND_UNTIL) {
                        if (time() > strtotime($this->SettingArr['SuspendBookingUntil']))
                            return true;
                        else
                            return false;
                    }
        }

        function Process_FacilityID($FacilityType, $FacilityID)
        {
            $RelatedItemID = 0;
            $RelatedSubLocationID = 0;
            if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM) {
                $RelatedSubLocationID = $FacilityID;
            } else 
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $RelatedItemID = $FacilityID;
                }
            return array(
                $RelatedSubLocationID,
                $RelatedItemID
            );
        }

        function Get_Booking_Details_By_BookingID($BookingID, $FacilityType = '', $FacilityID = '')
        {
            if (trim($FacilityType) != '')
                $cond_FacilityType = " AND FacilityType = '$FacilityType' ";
            if (trim($FacilityID) != '')
                $cond_FacilityID = " AND FacilityID IN (" . implode(",", (array) $FacilityID) . ") ";
            
            $sql = "
				SELECT
					* 
				FROM
					INTRANET_EBOOKING_BOOKING_DETAILS
				WHERE
					BookingID IN (" . implode(",", (array) $BookingID) . ")	
					$cond_FacilityType		
					$cond_FacilityID
			";
            
            return $this->returnArray($sql);
        }

        function Update_iCal_Event_Detail($BookingID, $iCalEventUpdateArr)
        {
            $sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$BookingID'";
            $arrExistingEvent = $this->returnVector($sql);
            
            include_once ("icalendar.php");
            $iCal = new icalendar();
            
            if ($iCalEventUpdateArr['Owner'])
                $SetSql .= " UserID = '" . $iCalEventUpdateArr['Owner'] . "', ";
            if ($iCalEventUpdateArr['EventTitle'])
                $SetSql .= " Title = '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventUpdateArr['EventTitle'])) . "', ";
            if ($iCalEventUpdateArr['Description'])
                $SetSql .= " Description = '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventUpdateArr['EventDesc'])) . "', ";
            if ($iCalEventUpdateArr['Location'])
                $SetSql .= " Location = '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventUpdateArr['Location'])) . "', ";
            if ($iCalEventUpdateArr['EventCalID'])
                $SetSql .= " CalID = '" . $iCal->Get_Safe_Sql_Query($iCal->Get_Request($iCalEventUpdateArr['EventCalID'])) . "', ";
            
            $sql = "
					UPDATE
						CALENDAR_EVENT_ENTRY
					SET
						$SetSql
						ModifiedDate = NOW()
					WHERE
						EventID = '" . $arrExistingEvent[0] . "'
				";
            
            return $this->db_db_query($sql);
        }

        function Get_Booking_Record_iCal_Detail($BookingID)
        {
            $sql = "
				SELECT 
					cee.* 
				FROM 
					INTRANET_EBOOKING_CALENDAR_EVENT_RELATION cer
					LEFT JOIN CALENDAR_EVENT_ENTRY cee ON cer.EventID = cee.EventID
				WHERE 
					cer.BookingID = '$BookingID'";
            
            $EventArr = $this->returnArray($sql);
            return $EventArr;
        }

        function Break_Time_Range($TimeRange)
        {
            $SelectedTimeArr = explode("_", $TimeRange);
            
            list ($BookStartHour, $BookStartMin) = explode(":", $SelectedTimeArr[0]);
            list ($BookEndHour, $BookEndMin) = explode(":", $SelectedTimeArr[1]);
            
            return array(
                $BookStartHour,
                $BookStartMin,
                $BookEndHour,
                $BookEndMin
            );
        }

        function Build_DateTimeArr($DateArr, $StartTimeArr, $EndTimeArr)
        {
            $DateArr = (array) $DateArr;
            $StartTimeArr = (array) $StartTimeArr;
            $EndTimeArr = (array) $EndTimeArr;
            
            foreach ($DateArr as $k => $thisDate) {
                // $NumOfTime = count($StartTimeArr);
                
                $thisTimeArr = array();
                // for($i=0; $i<$NumOfTime; $i++)
                // {
                $thisTimeArr[$StartTimeArr[$k]]['StartTime'] = $StartTimeArr[$k];
                $thisTimeArr[$StartTimeArr[$k]]['EndTime'] = $EndTimeArr[$k];
                // }
                
                ksort($thisTimeArr);
                if (isset($DateTimeArr[$thisDate])) {
                    $DateTimeArr[$thisDate] = array_merge($DateTimeArr[$thisDate], array_values($thisTimeArr));
                } else {
                    $DateTimeArr[$thisDate] = array_values($thisTimeArr);
                }
            }
            ksort($DateTimeArr);
            
            return $DateTimeArr;
        }

        function Reject_Approved_Or_Pending_Booking_By_DateTimeArr($FacilityType, $FacilityIDArr, $DateTimeArr, $ExcludeBookingID = array(), $sendEmail = true, $returnEmailBookingIdAry = false, $emailNotify = false)
        {
            if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM) {
                $ExistingItemBookingArr = $this->Get_Room_Booking_In_One_Day_Of_Multiple_TimeSlot((array) $FacilityIDArr, $DateTimeArr, 1, array(
                    LIBEBOOKING_BOOKING_STATUS_APPROVED,
                    LIBEBOOKING_BOOKING_STATUS_PENDING
                ));
            } else 
                if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $ExistingItemBookingArr = $this->Get_Item_Booking_In_One_Day_Of_Multiple_TimeSlot((array) $FacilityIDArr, $DateTimeArr, 1, array(
                        LIBEBOOKING_BOOKING_STATUS_APPROVED,
                        LIBEBOOKING_BOOKING_STATUS_PENDING
                    ));
                } else
                    return false;
            
            $existingBookingID = array();
            $emailBookingIdAry = array();
            foreach ($ExistingItemBookingArr as $thisFacilityID => $DateBookingArr) {
                foreach ($DateBookingArr as $thisDate => $thisBookingArr) {
                    foreach ($thisBookingArr as $thisBookingRecord) {
                        $deleteOtherRelatedIfRecject = $thisBookingRecord['DeleteOtherRelatedIfReject'];
                        $thisBookingID = $thisBookingRecord['BookingID'];
                        
                        if (in_array($thisBookingID, (array) $ExcludeBookingID))
                            continue;
                        
                        $existingBookingID[] = $thisBookingID;
                        if ($deleteOtherRelatedIfRecject == 1 && $FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                            $deleteRelatedBookingID[] = $thisBookingID;
                        }
                    }
                    // If reject room, reject the room and all item request of the room
                    // If reject item, reject the item.
                    if (count($existingBookingID) > 0) {
                        if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM) {} else 
                            if ($FacilityType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                                $cond_DeleteItemOnly = " AND FacilityType = '$FacilityType' AND FacilityID = '$thisFacilityID' ";
                            }
                        
                        $sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = -1, PIC='" . $_SESSION['UserID'] . "', ProcessDate=NOW(), ProcessDateTime=NOW() WHERE BookingID IN (" . implode(",", (array) $existingBookingID) . ") $cond_DeleteItemOnly ";
                        $SuccessArr[] = $this->db_db_query($sql);
                    }
                    
                    // if $deleteOtherRelatedIfRecject is 1, delete all other room and item in the booking
                    if (count($deleteRelatedBookingID) > 0) {
                        $sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = -1, PIC='" . $_SESSION['UserID'] . "', ProcessDate=NOW(), ProcessDateTime=NOW() WHERE BookingID IN (" . implode(",", (array) $deleteRelatedBookingID) . ") ";
                        $SuccessArr[] = $this->db_db_query($sql);
                    }
                    
                    if (count($existingBookingID) > 0) {
                        
                        // # Email User that the previous booking is rejected due to the reservation by eBooking Admin
                        // 2013-0910-1707-32073
                        // $this->Email_Booking_Result($existingBookingID);
                        $emailBookingIdAry = array_merge((array) $emailBookingIdAry, (array) $existingBookingID);
                    }
                }
            }
            
            $emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
            if ($sendEmail && count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $this->Email_Booking_Result_Merged($emailBookingIdAry, $emailNotify);
            }
            
            if ($returnEmailBookingIdAry) {
                return $emailBookingIdAry;
            } else {
                return ! in_array(false, (array) $SuccessArr);
            }
        }
        
        // ################################################################################################################
        // Manipulate User Booking Rule
        function Insert_User_Booking_Rule($rule_title, $needApproval, $BookingWithin_Day, $canBookForOthers, $user_type, $disableBooking = '0')
        {
            global $UserID;
            
            // insert new rule recorde
            $sql = "INSERT INTO 
						INTRANET_EBOOKING_USER_BOOKING_RULE 
						(RuleName,disableBooking,NeedApproval,DaysBeforeUse,CanBookForOthers,InputBy,ModifiedBy,RecordType,RecordStatus,DateInput,DateModified)
					VALUES
						('$rule_title','$disableBooking','$needApproval','$BookingWithin_Day','$canBookForOthers','$UserID','$UserID','$user_type','',now(),now())";
            $result['InsertRule'] = $this->db_db_query($sql);
            
            return ! in_multi_array(false, $result);
        }

        function Update_User_Booking_Rule($targetRuleID, $rule_title, $needApproval, $BookingWithin_Day, $canBookForOthers, $user_type, $disableBooking = '0')
        {
            global $UserID;
            
            $sql = "UPDATE
					INTRANET_EBOOKING_USER_BOOKING_RULE 
				SET
					RuleName = '$rule_title',
					DisableBooking = '$disableBooking',
					NeedApproval = '$needApproval',
					DaysBeforeUse = '$BookingWithin_Day',
					CanBookForOthers = '$canBookForOthers',
					ModifiedBy = '$UserID',
					RecordType = '$user_type',
					RecordStatus = '',
					DateModified = now()
				WHERE
					RuleID = '$targetRuleID'";
            
            $result['UpdateBookingRule'] = $this->db_db_query($sql);
            
            return ! in_multi_array(false, $result);
        }

        function Update_User_Booking_Rule_Target_User($targetRuleID, $user_type, $targetForm)
        {
            $result['DeleteExistingTargetUser'] = $this->Delete_User_Booking_Rule_Target_User($targetRuleID);
            
            if ($user_type == 1) {
                $sql = "INSERT INTO INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER (RuleID,YearID,UserType, DateInput) VALUES ('$targetRuleID','','$user_type', NOW())";
                $result['UpdateTargetUser'] = $this->db_db_query($sql);
            } else 
                if ($user_type == 2 || $user_type == 3) {
                    for ($i = 0; $i < sizeof($targetForm); $i ++) {
                        $target_form = $targetForm[$i];
                        $ValArr[] = "('$targetRuleID','$target_form','$user_type', NOW())";
                    }
                    $ValStr = implode(",", (array) $ValArr);
                    $sql = "INSERT INTO INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER (RuleID,YearID,UserType, DateInput) VALUES " . $ValStr;
                    $result['UpdateTargetUser'] = $this->db_db_query($sql);
                }
            
            return ! in_multi_array(false, $result);
        }

        function Delete_User_Booking_Rule_Target_User($targetRuleID)
        {
            if (empty($targetRuleID))
                return false;
            
            $sql = "DELETE FROM INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER WHERE RuleID IN (" . implode(",", (array) $targetRuleID) . ") ";
            return $this->db_db_query($sql);
        }

        function Update_User_Booking_Rule_Custom_Group_User($targetRuleID, $targetUserArr = '')
        {
            $result['DeleteExistingTargetUser'] = $this->Delete_User_Booking_Rule_Custom_Group_User($targetRuleID);
            
            if (! empty($targetUserArr)) {
                for ($i = 0; $i < sizeof($targetUserArr); $i ++) {
                    $target_user = $targetUserArr[$i];
                    $ValArr[] = "('$targetRuleID','$target_user', NOW())";
                }
                $ValStr = implode(",", (array) $ValArr);
                $sql = "INSERT INTO INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER (RuleID, UserID, DateInput) VALUES " . $ValStr;
                $result['UpdateTargetUser'] = $this->db_db_query($sql);
            }
            
            return ! in_multi_array(false, $result);
        }

        function Delete_User_Booking_Rule_Custom_Group_User($targetRuleID)
        {
            if (empty($targetRuleID))
                return false;
            
            $sql = "DELETE FROM INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER WHERE RuleID IN (" . implode(",", (array) $targetRuleID) . ") ";
            return $this->db_db_query($sql);
        }

        function Delete_User_Booking_Rule($RuleID)
        {
            if (empty($RuleID))
                return false;
            
            $sql = "DELETE FROM INTRANET_EBOOKING_USER_BOOKING_RULE WHERE RuleID IN (" . implode(",", (array) $RuleID) . ")";
            $result['DeleteBookingRule'] = $this->db_db_query($sql);
            
            $result['DeleteBookingRuleTargetForm'] = $this->Delete_User_Booking_Rule_Target_User($RuleID);
            $result['DeleteBookingRuleCustomGroup'] = $this->Delete_User_Booking_Rule_Custom_Group_User($RuleID);
            
            return ! in_array(false, $result);
        }
        
        // Manipulate User Booking Rule
        // ################################################################################################################
        function Get_User_Booking_Rule_Target_Form_Mapping($RuleID = '', $YearID = '', $UserType = '', $withName = 0)
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            if ((is_array($RuleID) && count($RuleID)) > 0 || (! is_array($RuleID) && trim($RuleID) != ''))
                $cond_RuleID = " AND target_user.RuleID IN (" . implode(",", (array) $RuleID) . ") ";
            if ((is_array($YearID) && count($YearID)) > 0 || (! is_array($YearID) && trim($YearID) != ''))
                $cond_YearID = " AND target_user.YearID IN (" . implode(",", (array) $YearID) . ") ";
            if ((is_array($UserType) && count($UserType)) > 0 || (! is_array($UserType) && trim($UserType) != ''))
                $cond_UserType = " AND target_user.UserType IN (" . implode(",", (array) $UserType) . ") ";
            
            if ($withName == 1) {
                $JoinTable = " LEFT JOIN YEAR AS year on (target_user.YearID = year.YearID) ";
                $FormName = " ,year.YearName";
            }
            
            $sql = "
				SELECT
					target_user.RuleID,
					target_user.YearID,
					target_user.UserType
					$FormName YearName
				FROM
					INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER target_user
					$JoinTable
				WHERE
					1
					$cond_RuleID
					$cond_YearID
					$cond_UserType
			";
            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_User_Booking_Rule_Custom_Group_User_Mapping($RuleID = '', $ParUserID = '', $withName = 0)
        {
            $funcParamAry = get_defined_vars();
            $cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
            if ($cacheResultAry !== null) {
                return $cacheResultAry;
            }
            
            if ((is_array($RuleID) && count($RuleID)) > 0 || (! is_array($RuleID) && trim($RuleID) != ''))
                $cond_RuleID = " AND target_user.RuleID IN (" . implode(",", (array) $RuleID) . ") ";
            if ((is_array($ParUserID) && count($ParUserID)) > 0 || (! is_array($ParUserID) && trim($ParUserID) != ''))
                $cond_UserID = " AND target_user.UserID IN (" . implode(",", (array) $ParUserID) . ")  ";
            
            if ($withName == 1) {
                $JoinTable = " LEFT JOIN INTRANET_USER AS iu on (target_user.UserID = iu.UserID) ";
                $NameField = getNameFieldByLang("iu.");
                $UserName = " ,$NameField NameField";
            }
            
            $sql = "
				SELECT
					target_user.RuleID,
					target_user.UserID
					$UserName
				FROM
					INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER target_user
					$JoinTable
				WHERE
					1
					$cond_RuleID
					$cond_UserID
			";
            $result = $this->returnArray($sql);
            $this->setCacheResult(__FUNCTION__, $funcParamAry, $result);
            return $result;
        }

        function Get_Approval_Group_Item_By_User($ParUserID)
        {
            $sql = "
				SELECT 
					a.ItemID
				FROM 
					INTRANET_EBOOKING_ITEM AS a 
					INNER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS b ON (a.ItemID = b.ItemID)
					INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS c ON (b.GroupID = c.GroupID)
				WHERE 
					c.UserID = '$ParUserID'
			";
            return $this->returnVector($sql);
        }

        function Get_Approval_Group_Room_By_User($ParUserID)
        {
            $sql = "
				SELECT 
					a.LocationID
				FROM 
					INVENTORY_LOCATION AS a 
					INNER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS b ON (a.LocationID = b.LocationID)
					INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER AS c ON (b.GroupID = c.GroupID)
				WHERE 
					c.UserID = '$ParUserID'
			";
            return $this->returnVector($sql);
        }

        function Get_Location_Name_Field($BuildingTable, $FloorTable, $LocationTable)
        {
            global $PATH_WRT_ROOT;
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            $linventory = new libinventory();
            
            $this->Load_General_Setting();
            
            $sql = "CONCAT(";
            if ($this->SettingArr['DisplayBuildingName'])
                $sql .= $linventory->getInventoryNameByLang($BuildingTable) . ",' > ',";
            if ($this->SettingArr['DisplayFloorName'])
                $sql .= $linventory->getInventoryNameByLang($FloorTable) . ",' > ',";
            $sql .= $linventory->getInventoryNameByLang($LocationTable);
            $sql .= ")";
            
            return $sql;
        }

        function returnCustRemark($RelatedToID)
        {
            // if more client has cust remark, pls don't select *, update later if necessary
            
            // retrieve data
            $sql = "select * from INTRANET_EBOOKING_RECORD_CUST_REMARK where BookingID='$RelatedToID'";
            $result = $this->returnArray($sql, 1, 1);
            return $result[0];
        }

        function DisplayCustRemark($RelatedToID)
        {
            $cust_remark_data = $this->returnCustRemark($RelatedToID);
            
            if (! empty($cust_remark_data)) {
                $cust_remark .= "Activity Type: " . $cust_remark_data['ActivityType'] . "<br>";
                $cust_remark .= "Name of Activity: " . $cust_remark_data['ActivityName'] . "<br>";
                $cust_remark .= "Class / Expected No. of Participants: " . $cust_remark_data['Attendance'] . "<br>";
                $cust_remark .= "Person-in-charge: " . $cust_remark_data['PIC'] . "<br><br>";
                
                if ($cust_remark_data['IT_Teachnician'] || $cust_remark_data['Photographer'] || $cust_remark_data['Clerical'] || $cust_remark_data['Janitor']) {
                    $cust_remark .= "No. of Staff Required on Activity Day: <br>";
                    if ($cust_remark_data['IT_Teachnician'])
                        $cust_remark .= "IT Teachnician: " . $cust_remark_data['IT_Teachnician'] . "<br>";
                    if ($cust_remark_data['Photographer'])
                        $cust_remark .= "Photographer: " . $cust_remark_data['Photographer'] . "<br>";
                    if ($cust_remark_data['Clerical'])
                        $cust_remark .= "Clerical: " . $cust_remark_data['Clerical'] . "<br>";
                    if ($cust_remark_data['Janitor'])
                        $cust_remark .= "Janitor: " . $cust_remark_data['Janitor'] . "<br>";
                    
                    $cust_remark .= "<br>";
                }
                
                $cust_remark .= "REQUIREMENT: <br>";
                if ($cust_remark_data['PhotoTaking'])
                    $cust_remark .= "- PhotoTaking <br>";
                if ($cust_remark_data['VideoShooting'])
                    $cust_remark .= "- Video Shooting <br>";
                if ($cust_remark_data['Projector'])
                    $cust_remark .= "- Projector <br>";
                if ($cust_remark_data['Visualizer'])
                    $cust_remark .= "- Visualizer <br>";
                if ($cust_remark_data['Microphone'])
                    $cust_remark .= "- Microphone <br>";
                if ($cust_remark_data['MobilePASystem'])
                    $cust_remark .= "- MobilePASystem <br>";
                if ($cust_remark_data['Laptop'])
                    $cust_remark .= "- Laptop <br>";
                if ($cust_remark_data['Signage'])
                    $cust_remark .= "- Signage <br>";
                if ($cust_remark_data['Souvenir'])
                    $cust_remark .= "- Souvenir <br>";
                if ($cust_remark_data['Stationery'])
                    $cust_remark .= "- Stationery <br>";
                if ($cust_remark_data['Flower'])
                    $cust_remark .= "- Flower <br>";
                if ($cust_remark_data['CateringServicer'])
                    $cust_remark .= "- CateringServicer <br>";
                if ($cust_remark_data['Others'])
                    $cust_remark .= "- Others <br>";
                
                $cust_remark .= "<br>";
            }
            
            return $cust_remark;
        }

        function Insert_Update_Location_Booking_Rule($LocationID, $DayBeforeBooking)
        {
            $sql = "SELECT BookingRuleID FROM INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION WHERE LocationID = '$LocationID'";
            $arrExistRoomBookingRuleID = $this->returnVector($sql);
            
            if (sizeof($arrExistRoomBookingRuleID) > 0) {
                $sql = "UPDATE 
							INTRANET_EBOOKING_LOCATION_BOOKING_RULE 
						SET 
							DaysBeforeUse = '$DayBeforeBooking', 
							ModifiedBy = '" . $_SESSION['UserID'] . "', 
							DateModified = NOW() 
						WHERE 
							RuleID = '" . $arrExistRoomBookingRuleID[0] . "' ";
                $result["UpdateRoomBookingRule"] = $this->db_db_query($sql);
            } else {
                $sql = "INSERT INTO 
							INTRANET_EBOOKING_LOCATION_BOOKING_RULE 
							(DaysBeforeUse, InputBy, ModifiedBy, DateInput, DateModified)
						VALUES
							('$DayBeforeBooking', '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', NOW(), NOW())";
                $result["InsertRoomBookingRule"] = $this->db_db_query($sql);
                
                $RoomBookingRuleID = $this->db_insert_id();
                
                $sql = "INSERT INTO 
							INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION
							(LocationID, BookingRuleID, InputBy, ModifiedBy, DateInput, DateModified)
						VALUES
							('$LocationID', '$RoomBookingRuleID', '" . $_SESSION['UserID'] . "', '" . $_SESSION['UserID'] . "', NOW(), NOW())";
                $result["InsertRoomBookingRuleRelation"] = $this->db_db_query($sql);
            }
            
            return ! in_array(false, $result);
        }

        function Get_Booking_Record_Export_Header()
        {
            global $Lang, $sys_custom;
            
            $exportHeaderAry = array();
            $exportHeaderAry[] = '#';
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'];
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'];
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'];
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson'];
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy'];
            if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
                $exportHeaderAry[] = $Lang['eBooking']['Event'];
            }
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'];
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Reason'];
            $exportHeaderAry[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark'];
            
            return $exportHeaderAry;
        }

        function Get_Booking_Record_Export_Content($Keyword = '', $FacilityType = '', $BookingStatusList = '', $BookingDateStatus = '', $StartDate = '', $EndDate = '', $ManagementGroup = '', $TargetUserID = '')
        {
            list ($arrBookingID, $arrBookingDetails) = $this->Get_Booking_Record_Display_Detail($Keyword, $FacilityType, $BookingStatusList, $BookingDateStatus, $StartDate, $EndDate, $ManagementGroup, $TargetUserID);
            $start = 1;
            
            $exportDataAry = array();
            if (sizeof($arrBookingID) > 0) {
                foreach ($arrBookingID as $key => $booking_id) {
                    if ($booking_id != "")
                        $arrSortedBookingID[] = $booking_id;
                }
                
                // for($i = $start-1; $i < $end; $i++) {
                // $arrDisplayBookingID[] = $arrSortedBookingID[$i];
                // }
                $arrDisplayBookingID = $arrSortedBookingID;
                
                foreach ($arrDisplayBookingID as $key => $booking_id) {
                    $arrRoomBookingDetails = array();
                    $arrItemBookingDetails = array();
                    
                    if (is_array($arrBookingDetails[$booking_id]['RelatedRoom']))
                        $arrTempRoomBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedRoom']);
                    if (is_array($arrBookingDetails[$booking_id]['RelatedItem']))
                        $arrTempItemBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedItem']);
                    
                    if (sizeof($arrTempRoomBookingDetails) > 0) {
                        foreach ($arrTempRoomBookingDetails as $key => $val) {
                            if (($val != "") && ($val != "0")) {
                                $arrRoomBookingDetails[] = $val;
                            }
                        }
                    }
                    
                    if (sizeof($arrTempItemBookingDetails) > 0) {
                        foreach ($arrTempItemBookingDetails as $key => $val) {
                            if (($val != "") && ($val != "0")) {
                                $arrItemBookingDetails[] = $val;
                            }
                        }
                    }
                    $row_num = $start ++;
                    
                    if (($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1)) {
                        // # Book for room & item at the same time
                        $room_id = $arrRoomBookingDetails[0];
                        $exportDataAry[] = $this->Get_Booking_Record_Export_Row($row_num, LIBEBOOKING_FACILITY_TYPE_ROOM, $room_id, $arrBookingDetails[$booking_id]);
                        
                        for ($i = 0; $i < sizeof($arrItemBookingDetails); $i ++) {
                            $item_id = $arrItemBookingDetails[$i];
                            $exportDataAry[] = $this->Get_Booking_Record_Export_Row($row_num, LIBEBOOKING_FACILITY_TYPE_ITEM, $item_id, $arrBookingDetails[$booking_id], $itemInRoom = true);
                        }
                    } else 
                        if (($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 0)) {
                            // # Book for Room Only
                            $room_id = $arrRoomBookingDetails[0];
                            $exportDataAry[] = $this->Get_Booking_Record_Export_Row($row_num, LIBEBOOKING_FACILITY_TYPE_ROOM, $room_id, $arrBookingDetails[$booking_id]);
                        } else 
                            if (($arrBookingDetails[$booking_id]['RoomBooking'] == 0) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1)) {
                                // # Book for Item Only
                                for ($i = 0; $i < sizeof($arrItemBookingDetails); $i ++) {
                                    $item_id = $arrItemBookingDetails[$i];
                                    
                                    $itemInOtherItem = ($i == 0) ? false : true;
                                    $exportDataAry[] = $this->Get_Booking_Record_Export_Row($row_num, LIBEBOOKING_FACILITY_TYPE_ITEM, $item_id, $arrBookingDetails[$booking_id], $itemInRoom = false, $itemInOtherItem);
                                }
                            }
                }
            }
            
            return $exportDataAry;
        }

        function Get_Booking_Record_Export_Row($rowNum, $itemType, $id, $infoAry, $itemInRoom = false, $itemInOtherItem = false)
        {
            global $Lang, $sys_custom, $lebooking;
            // LIBEBOOKING_FACILITY_TYPE_ROOM
            // LIBEBOOKING_FACILITY_TYPE_ITEM
            
            if ($itemType == LIBEBOOKING_FACILITY_TYPE_ROOM) {
                $nameField = 'RoomName';
                $bookingStatusFieldName = 'RoomBookingStatus';
                $bookingPicFieldName = 'RoomBookingPIC';
                $processDayBeforeFieldName = 'RoomProcessDayBefore';
            } else 
                if ($itemType == LIBEBOOKING_FACILITY_TYPE_ITEM) {
                    $nameField = 'ItemName';
                    $bookingStatusFieldName = 'ItemBookingStatus';
                    $bookingPicFieldName = 'ItemBookingPIC';
                    $processDayBeforeFieldName = 'ItemProcessDayBefore';
                }
            switch ($infoAry[$id][$bookingStatusFieldName]) {
                case 0:
                    $img_status = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'];
                    break;
                case 1:
                    $img_status = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] . ' (' . $infoAry[$id][$bookingPicFieldName] . ' ' . $infoAry[$id][$processDayBeforeFieldName] . ')';
                    break;
                case - 1:
                    $img_status = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] . ' (' . $infoAry[$id][$bookingPicFieldName] . ' ' . $infoAry[$id][$processDayBeforeFieldName] . ')';
                    break;
                case 999:
                    $img_status = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory'];
                    break;
            }
            
            if ($sys_custom['eBooking_Cust_Remark']['WongKamFai']) {
                $relatedToId = $infoAry['RelatedTo'];
                $custRemark = str_replace('<br>', "\r\n", $this->DisplayCustRemark($relatedToId));
            }
            
            if ($itemInRoom) {
                $namePrefix = '& ';
            } else {
                $namePrefix = ($infoAry['IsReserve']) ? '* ' : '';
            }
            
            $rowInfoAry = array();
            $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : $rowNum;
            $rowInfoAry[] = $namePrefix . $infoAry[$id][$nameField];
            $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : $infoAry['Date'];
            $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : date("H:i", strtotime($infoAry['StartTime'])) . " - " . date("H:i", strtotime($infoAry['EndTime']));
            $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : $infoAry['ResponsiblePerson'];
            $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : $infoAry['RequestedBy'] . ' (' . $infoAry['RequestedDayBefore'] . ')';
            if ($sys_custom['eBooking_Booking_ExtraEvent_Input']) {
                $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : $infoAry['Event'];
            }
            $rowInfoAry[] = $img_status;
            $rowInfoAry[] = $infoAry[$id]['RejectReason'];
            $rowInfoAry[] = ($itemInRoom || $itemInOtherItem) ? '' : $custRemark . $infoAry['Remark'];
            
            return $rowInfoAry;
        }

        function Get_Usage_Report_Result_Ary($facilityType, $FacilityID, $targetFromDate, $targetToDate)
        {
            $result = $this->Get_Facility_Booking_Record($facilityType, $FacilityID, $targetFromDate, $targetToDate, $BookingStatus = LIBEBOOKING_BOOKING_STATUS_APPROVED, $BookingID = '', $WithName = 1, $WithUserName = 0, $FollowGroupID = '', $MgmtGroupID = '', $GroupByBookingRequest = 0, $OrderBy = '', $Keyword = '');
            
            return $result;
        }

        function Get_Month_in_Date_Range($targetFromDate, $targetToDate)
        {
            $date1 = strtotime($targetFromDate);
            $date2 = strtotime($targetToDate);
            if ($targetFromDate < $targetToDate) {
                $dates_range[] = $targetFromDate;
                while ($date1 != $date2) {
                    $month_ary[] = date('Y-m', $date1);
                    $date1 = mktime(0, 0, 0, date("m", $date1), date("d", $date1) + 1, date("Y", $date1));
                    $month_ary[] = date('Y-m', $date1);
                }
                
                foreach ($month_ary as $key => $item) {
                    $month_ary1[$item][] = $item;
                }
                foreach ($month_ary1 as $key => $item) {
                    $month_ary2[] = $key;
                }
            } elseif ($targetFromDate == $targetToDate) {
                
                $month_ary2[0] = date('Y-m', $date1);
            }
            
            return $month_ary2;
        }

        function Check_Enable_Door_Access_Right()
        {
            global $plugin;
            return $plugin['door_access'];
        }
        
        // #############################################################################
        // ######################### To eInventory API [Start] #########################
        // #############################################################################
        function returnIsItemCheckedIn($eInvItemIdAry)
        {
            $sql = "Select
							iei.eInventoryItemID
					From
							INTRANET_EBOOKING_ITEM as iei
							Inner Join INTRANET_EBOOKING_BOOKING_DETAILS as iebd On (iei.ItemID = iebd.FacilityID)
					Where
							iebd.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "'
							And CurrentStatus = '" . LIBEBOOKING_BOOKING_CheckIn . "'
							And iei.eInventoryItemID IN ('" . implode("','", (array) $eInvItemIdAry) . "')
					";
            $checkedInItemIdAry = Get_Array_By_Key($this->returnResultSet($sql), 'eInventoryItemID');
            
            $returnAssoAry = array();
            $numOfItem = count($eInvItemIdAry);
            for ($i = 0; $i < $numOfItem; $i ++) {
                $_itemId = $eInvItemIdAry[$i];
                
                if (in_array($_itemId, $checkedInItemIdAry)) {
                    $returnAssoAry[$_itemId] = 1;
                } else {
                    $returnAssoAry[$_itemId] = 0;
                }
            }
            
            return $returnAssoAry;
        }

        function returnItemBookingRecords($itemType, $eInvItemId, $locationId = '')
        {
            $selectField = " iebr.Date, iebr.StartTime, iebr.EndTime, iebr.RequestedBy, iebr.ResponsibleUID ";
            $condsTime = " And iebr.Date >= DATE(NOW()) ";
            
            if ($itemType == 1) {
                // single item
                $sql = "Select
								$selectField
						From
								INTRANET_EBOOKING_ITEM as iei
								Inner Join INTRANET_EBOOKING_BOOKING_DETAILS as iebd On (iei.ItemID = iebd.FacilityID)
								Inner Join INTRANET_EBOOKING_RECORD as iebr On (iebd.BookingID = iebr.BookingID)
						Where
								iei.eInventoryItemID = '" . $eInvItemId . "'
								And iebd.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ITEM . "'
								And iebd.BookingStatus = '" . LIBEBOOKING_BOOKING_STATUS_APPROVED . "'
								And iebr.RecordStatus = '1'
								$condsTime
						";
            } else 
                if ($itemType == 2) {
                    // bulk item
                    $sql = "Select
								$selectField
						From
								INTRANET_EBOOKING_BOOKING_DETAILS as iebd
								Inner Join INTRANET_EBOOKING_RECORD as iebr On (iebd.BookingID = iebr.BookingID)
						Where
								iebd.FacilityID = '" . $locationId . "'
								And iebd.FacilityType = '" . LIBEBOOKING_FACILITY_TYPE_ROOM . "'
								And iebd.BookingStatus = '" . LIBEBOOKING_BOOKING_STATUS_APPROVED . "'
								And iebr.RecordStatus = '1'
								$condsTime
						";
                }
            return $this->returnResultSet($sql);
        }

        function returnIsItemAllowBooking($itemType, $eInvItemId, $locationId = '')
        {
            if ($itemType == 1) {
                // single item
                $sql = "Select
								AllowBooking
						From
								INTRANET_EBOOKING_ITEM
						Where
								eInventoryItemID = '" . $eInvItemId . "'
								And RecordStatus = '1'
						";
            } else 
                if ($itemType == 2) {
                    // bulk item
                    $sql = "Select
								IncludedWhenBooking as AllowBooking
						From
								INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION
						Where
								LocationID = '" . $locationId . "'
								And BulkItemID = '" . $eInvItemId . "'
						";
                }
            
            $infoAry = $this->returnResultSet($sql);
            $allowBooking = ($infoAry[0]['AllowBooking'] == '') ? 0 : $infoAry[0]['AllowBooking'];
            
            return $allowBooking;
        }
        // #############################################################################
        // ########################## To eInventory API [End] ##########################
        // #############################################################################
        
        // #############################################################################
        // ######################## From eInventory API [Start] ########################
        // #############################################################################
        function Get_eInventory_Bulk_Item_Normal_Quantity($eInvItemId, $locationId)
        {
            global $PATH_WRT_ROOT;
            
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            $linventory = new libinventory();
            
            return $linventory->returnBulkItemNormalQty($eInvItemId, $locationId);
        }
        
        // 1-Normal, 2-Damaged, 3-Repairing, 4-Deleted, 5-Write-off
        // no need to cater "4-Deleted" for eBooking
        function Get_eInventory_Single_Item_Status($itemId)
        {
            global $PATH_WRT_ROOT;
            
            include_once ($PATH_WRT_ROOT . 'includes/libinventory.php');
            include_once ($PATH_WRT_ROOT . 'includes/libebooking_item.php');
            $linventory = new libinventory();
            $libebooking_item = new eBooking_Item();
            
            $itemInventoryItemIdAry = array_values(array_remove_empty(Get_Array_By_Key($libebooking_item->Get_All_Item($CategoryID = '', $Category2ID = '', $itemId), 'eInventoryItemID')));
            return $linventory->returnSingleItemStatus($itemInventoryItemIdAry);
        }

        function Get_Item_Status_Display($status)
        {
            global $Lang;
            
            switch ($status) {
                case 2:
                    $display = $Lang['eBooking']['Damaged'];
                    break;
                case 3:
                    $display = $Lang['eBooking']['Repairing'];
                    break;
                case 5:
                    $display = $Lang['eBooking']['WrittenOff'];
                    break;
                default:
                    $display = '';
            }
            
            return $display;
        }
        // #############################################################################
        // ######################### From eInventory API [End] #########################
        // #############################################################################
        
        // ##################### Booking Category[Start] ###############################
        function getBookingCategoryByCategoryID($CategoryIDAry, $returnAssoAry = 0)
        {
            if (is_array($CategoryIDAry)) {
                $conds_CatID = implode("','", $CategoryIDAry);
            } else {
                $conds_CatID = $CategoryIDAry;
            }
            
            $sql = "SELECT
							CategoryID as CategoryID,
							CategoryName as CategoryName
					FROM
							INTRANET_EBOOKING_CATEGORY
					WHERE
							RecordStatus = 1 AND
							CategoryID IN ( '" . $conds_CatID . "' )
					Order By
							DisplayOrder
					";
            $result = $this->ReturnResultSet($sql);
            
            if ($returnAssoAry > 0) {
                $resultAry = BuildMultiKeyAssoc($result, 'CategoryID');
            } else {
                $resultAry = $result;
            }
            
            return $resultAry;
        }

        function getBookingCategorySQL()
        {
            $sql = "SELECT
							CategoryID,
							CONCAT('<a href=\"new.php?CategoryID=', CategoryID ,'\" class=\"tablelink\">', CategoryName, '</a>') as CNameLink,
							CategoryName as CategoryName
					FROM
							INTRANET_EBOOKING_CATEGORY
					WHERE
							RecordStatus = 1
					Order By
							DisplayOrder
					";
            
            return $sql;
        }

        function getBookingCategorySelectionBox($BookingCategoryID = '', $FilterMode = 0, $onChangeTag = '')
        {
            global $Lang;
            
            $sql = $this->getBookingCategorySQL();
            
            $BookingCategoryAry = $this->returnResultSet($sql);
            $BookingCategoryAssoAry = BuildMultiKeyAssoc($BookingCategoryAry, 'CategoryID', array(
                'CategoryName'
            ), 1);
            
            if ($FilterMode) {
                $id = "id=\"BookingCategoryID\" name=\"BookingCategoryID\" onchange=\"" . $onChangeTag . "\"";
                $BookingCategorySelectionBox = getSelectByAssoArray($BookingCategoryAssoAry, $id, $BookingCategoryID, 0, 0, $Lang['eBooking']['Management']['Filter']['AllBookingCategory']);
            } else {
                $id = "id=\"BookingCategoryID\" name=\"BookingCategoryID\" ";
                $BookingCategorySelectionBox = getSelectByAssoArray($BookingCategoryAssoAry, $id, $BookingCategoryID);
            }
            
            return $BookingCategorySelectionBox;
        }

        function Get_Update_Display_Order_Arr($OrderText, $Separator = ",")
        {
            if ($OrderText == "")
                return false;
            $orderArr = explode($Separator, $OrderText);
            $orderArr = array_remove_empty($orderArr);
            $orderArr = Array_Trim($orderArr);
            $numOfOrder = count($orderArr);
            // display order starts from 1
            $counter = 1;
            $newOrderArr = array();
            for ($i = 0; $i < $numOfOrder; $i ++) {
                $thisID = $orderArr[$i];
                if (is_numeric($thisID)) {
                    $newOrderArr[$counter] = $thisID;
                    $counter ++;
                }
            }
            return $newOrderArr;
        }

        function Update_Category_Record_DisplayOrder($DisplayOrderArr = array())
        {
            if (count($DisplayOrderArr) == 0)
                return false;
            
            $this->Start_Trans();
            for ($i = 1; $i <= sizeof($DisplayOrderArr); $i ++) {
                $thisObjectID = $DisplayOrderArr[$i];
                
                $sql = 'UPDATE INTRANET_EBOOKING_CATEGORY SET 
							DisplayOrder = \'' . $i . '\' 
						WHERE 
							CategoryID = \'' . $thisObjectID . '\'';
                
                $Result['ReorderResult' . $i] = $this->db_db_query($sql);
            }
            
            if (in_array(false, $Result)) {
                $this->RollBack_Trans();
                return false;
            } else {
                $this->Commit_Trans();
                return true;
            }
        }

        function Is_Category_Linked_Data($CategoryIDArr)
        {
            $sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_RECORD_CATEGORY_RELATION WHERE CategoryID IN ( '" . implode('\',\'', $CategoryIDArr) . "')";
            $numRecordArr = $this->returnVector($sql);
            
            $numOfData = $numRecordArr[0];
            $returnVal = false;
            
            if ($numOfData > 0) {
                $returnVal = true;
            }
            
            return $returnVal;
        }

        function getBookingMapWithCategory($BookingIDAry, $MappingField)
        {
            $sql = "SELECT
					 	iercr.CategoryID,
						iercr.BookingID,
						iec.CategoryName
					FROM 
						INTRANET_EBOOKING_RECORD_CATEGORY_RELATION iercr 
						INNER JOIN INTRANET_EBOOKING_CATEGORY iec ON iec.CategoryID = iercr.CategoryID 
					WHERE 
						BOOKINGID IN ('" . implode('\',\'', (array) $BookingIDAry) . "')";
            
            $BookingCatRelationAry = $this->ReturnResultSet($sql);
            $BookingCatRelationAssoAry = BuildMultiKeyAssoc($BookingCatRelationAry, 'BookingID', array(
                $MappingField
            ), 1);
            
            return $BookingCatRelationAssoAry;
        }

        function insertUpdatDelete_BookingCategory($parAction, $parUserID, $parCategoryName = '', $parCategoryID = '')
        {
            $SafeparCategoryName = $this->Get_Safe_Sql_Query($parCategoryName);
            
            if ($parAction == 'INSERT') {
                // New
                $sql = "SELECT Max(DisplayOrder) from INTRANET_EBOOKING_CATEGORY where RecordStatus= 1";
                $numCat = $this->returnVector($sql);
                $newRecordOrder = $numCat[0] + 1;
                
                $sql = "INSERT INTO 
					INTRANET_EBOOKING_CATEGORY 
					(CategoryName,DisplayOrder,RecordStatus,DateInput,InputBy,DateModified,ModifiedBy) 
				Values 
					( '" . $SafeparCategoryName . "','$newRecordOrder',1,now(),'$parUserID',now(),'$parUserID')";
            } else 
                if ($parAction == 'UPDATE') {
                    // update
                    $sql = "UPDATE 
					INTRANET_EBOOKING_CATEGORY 
				SET 
					CategoryName = '" . $SafeparCategoryName . "',
					DateModified = now(), 
					ModifiedBy = '" . $parUserID . "'  
				WHERE 
					CategoryID = '" . $parCategoryID . "' ";
                } else 
                    if ($parAction == 'DELETE') {
                        // Delete
                        $sql = "UPDATE 
							INTRANET_EBOOKING_CATEGORY 
						SET 
							RecordStatus = 0,
							DateModified = now(), 
							ModifiedBy = '" . $parUserID . "'  
						WHERE 
							CategoryID IN ('" . $parCategoryID . "') ";
                    }
            return $this->db_db_query($sql);
        }
        // ###################### Booking Category[End] ################################
        
        // ####################################### Room Booking Record Day View [Start] ############################################################
        function getMappingBookingIDtoTimeslot($LocationID, $bookingInfoAry, $startTime, $timeRange)
        {
            $MappingInterval_Second = 300;
          
       
            $TimeIntervalArray = array();
            $numTimeslot = $timeRange / $MappingInterval_Second;
            
            // ######################### Get All the Start to End timeslot (Start) ##########################
            $_tempStartEndTimeslotAry = array();

            $maxRow = 1;
            $checkRowAry = array();
            foreach ($bookingInfoAry as $_oneBookingInfoAry) {
                if ($_oneBookingInfoAry['FacilityID'] == $LocationID) {
                    // debug_pr($_oneBookingInfoAry);
                    $_BookingID = $_oneBookingInfoAry['BookingID'];
                    $_StartTimeTimestamp = strtotime($_oneBookingInfoAry['StartTime']);
                    $_StartTimeTimestamp = $MappingInterval_Second*(ceil($_StartTimeTimestamp/$MappingInterval_Second)); //round up to nearest $MappingInterval_Second
                    $_EndTimeTimestamp = strtotime($_oneBookingInfoAry['EndTime']) - $MappingInterval_Second; // end time minus 1 time 
                    $_EndTimeTimestamp = $MappingInterval_Second*(ceil(($_EndTimeTimestamp)/$MappingInterval_Second));//round up to nearest $MappingInterval_Second
                    $_Duration = $_oneBookingInfoAry['Duration'] - $MappingInterval_Second;
                    
                    $i = 0;
                    while ($i < $maxRow) {  
                        
                        if (empty($_tempStartEndTimeslotAry[$_StartTimeTimestamp][$i]) && empty($_tempStartEndTimeslotAry[$_EndTimeTimestamp][$i])) {
                            $_tempStartEndTimeslotAry[$_StartTimeTimestamp][$i]['BookingID'] = $_BookingID;
                            $_tempStartEndTimeslotAry[$_StartTimeTimestamp][$i]['TimeType'] = 'Start';
                            for ($j = $MappingInterval_Second; $j < $_Duration; $j += $MappingInterval_Second) {
                                $_middleTimeTimestamp = $_StartTimeTimestamp + $j;
                                $_tempStartEndTimeslotAry[$_middleTimeTimestamp][$i]['BookingID'] = $_BookingID;
                                $_tempStartEndTimeslotAry[$_middleTimeTimestamp][$i]['TimeType'] = 'Start';
                            }
                            $_tempStartEndTimeslotAry[$_EndTimeTimestamp][$i]['BookingID'] = $_BookingID;
                            $_tempStartEndTimeslotAry[$_EndTimeTimestamp][$i]['TimeType'] = 'End'; 

                            break;
                        } else {
                            if (! in_array($i, $checkRowAry)) {
                                $maxRow ++;
                            }
                            $checkRowAry[] = $i;
                            $i ++;
                        }
                    }
                } else {
                    continue;
                }
            }//debug_pr($_tempStartEndTimeslotAry);
            // ########################## Get All the Start to End timeslot (End) ###########################
            
            // ########################## Get Empty Timeslot (Start) ###########################
            $TimeIntervalArray = array();
            for ($i = 0; $i < $numTimeslot; $i ++) {
                $_Timestamp = strtotime($startTime) + $MappingInterval_Second * $i;
                   
                if (! empty($_tempStartEndTimeslotAry[$_Timestamp])) {
                    $TimeIntervalArray[$_Timestamp] = $_tempStartEndTimeslotAry[$_Timestamp];
                } else {
                    $TimeIntervalArray[$_Timestamp] = 0;
                }
            }
            // ########################### Get Empty Timeslot (End) ############################
            /*
             * Timeslot Info array strcuture as follow
             * Empty timeslot - $Ary['TimeslotInfo'][9:00]
             * Occupy timeslot - $Ary['TimeslotInfo'][10:00][0][BookingID] = 12345
             * $Ary['TimeslotInfo'][10:00][0][TimeType] = Start/End
             */
            $resultAssoAry['TimeslotInfo'] = $TimeIntervalArray;
            $resultAssoAry['maxRow'] = $maxRow;
            return $resultAssoAry;
        }

        function getRoomBookingRecordByLocationID($LocationID)
        {
            if (is_array($LocationID)) {
                $LocationID_conds = " FacilityID IN ('" . implode("','", $LocationID) . "') ";
            } else {
                $LocationID_conds = " FacilityID = '" . $LocationID."'";
            }
            
            $sql = "
				SELECT
					* 
				FROM
					INTRANET_EBOOKING_BOOKING_DETAILS
				WHERE
					$LocationID_conds
					FacilityType = " . LIBEBOOKING_FACILITY_TYPE_ROOM . " 
					
			";
            $result = $this->ReturnResultSet($sql);
            
            return $result;
        }

        function SaveDayViewSetting($StartTime, $EndTime, $TimeInterval)
        {
            include_once ("libgeneralsettings.php");
            
            $libgs = new libgeneralsettings();
            $SettingAry['RoomBookingRecordDayViewStartTime'] = $StartTime;
            $SettingAry['RoomBookingRecordDayViewEndTime'] = $EndTime;
            $SettingAry['RoomBookingRecordDayViewTimeInterval'] = $TimeInterval;
            
            $result = $libgs->Save_General_Setting('eBooking', $SettingAry);
            return $result;
        }
        // ###################################### Room Booking Record Day View [End] ########################################
        
        // ###################################### New Function Start From Here ##############################################
        private $ExistingLocationAssoAry;

        private function setExistingLocationAssoAry()
        {
            $sql = "SELECT
						ilb.NameChi as BuildingChiName,
						ilb.NameEng as BuildingEngName,
						ilb.Code as BuildingCode,
						ill.NameChi as LocationLevelChiName,
						ill.NameEng as LocationLevelEngName,
						ill.Code as LocationLevelCode,
						il.NameChi as RoomChiName,
						il.NameEng as RoomEngName,
						il.Code as RoomCode,
						il.LocationID as LocationID,
						il.AllowBooking as isAllowBooking
					FROM 
						INVENTORY_LOCATION_BUILDING as ilb 
						LEFT JOIN INVENTORY_LOCATION_LEVEL as ill ON ilb.BuildingID = ill.BuildingID 
						LEFT JOIN INVENTORY_LOCATION as il ON ill.LocationLevelID = il.LocationLevelID
					WHERE 
						ilb.RecordStatus = 1 and ill.RecordStatus = 1 and il.RecordStatus = 1
					ORDER BY
						ilb.Code,ill.Code,il.Code";
            
            $result = $this->returnResultSet($sql);
            $this->ExistingLocationAssoAry = $result;
        }

        public function getExistingLocationAssoAry($assocKey = array())
        {
            if (! isset($this->ExistingLocationAssoAry)) {
                $this->setExistingLocationAssoAry();
            }
            
            $ExistingLocationAry = $this->ExistingLocationAssoAry;
            
            if (! empty($assocKey)) {
                $ExistingLocationAry = BuildMultiKeyAssoc($ExistingLocationAry, $assocKey);
            }
            
            return $ExistingLocationAry;
        }

        public function isLocationBookable($LocationID)
        {
            $LocationAssoAry = $this->getExistingLocationAssoAry(array(
                'LocationID'
            ));
            
            return $LocationAssoAry[$LocationID]['isAllowBooking'];
        }

        public function validateLocation($BuildingCode, $LocationLevelCode, $RoomCode, $returnLocationID = false)
        {
            $LocationAssoAry = $this->getExistingLocationAssoAry(array(
                'BuildingCode',
                'LocationLevelCode',
                'RoomCode'
            ));
            
            // if not empty mean the location exist
            if (empty($LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode])) {
                return false;
            } else {
                if ($returnLocationID) {
                    return $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['LocationID'];
                } else {
                    return true;
                }
            }
        }

        public function getLocationNameByCode($BuildingCode, $LocationLevelCode, $RoomCode)
        {
            $LocationAssoAry = $this->getExistingLocationAssoAry(array(
                'BuildingCode',
                'LocationLevelCode',
                'RoomCode'
            ));
            
            $BNameChi = $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['BuildingChiName'];
            $LNameChi = $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['LocationLevelChiName'];
            $RNameChi = $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['RoomChiName'];
            $LocationNameChi = $BNameChi . ' > ' . $LNameChi . ' > ' . $RNameChi;
            
            $BNameEng = $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['BuildingEngName'];
            $LNameEng = $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['LocationLevelEngName'];
            $RNameEng = $LocationAssoAry[$BuildingCode][$LocationLevelCode][$RoomCode]['RoomEngName'];
            $LocationNameEng = $BNameEng . ' > ' . $LNameEng . ' > ' . $RNameEng;
            return Get_Lang_Selection($LocationNameChi, $LocationNameEng);
        }

        public function getLocationNameByID($LocationID)
        {
            $LocationAssoAry = $this->getExistingLocationAssoAry(array(
                'LocationID'
            ));
            
            $BNameChi = $LocationAssoAry[$LocationID]['BuildingChiName'];
            $LNameChi = $LocationAssoAry[$LocationID]['LocationLevelChiName'];
            $RNameChi = $LocationAssoAry[$LocationID]['RoomChiName'];
            $LocationNameChi = $BNameChi . ' > ' . $LNameChi . ' > ' . $RNameChi;
            
            $BNameEng = $LocationAssoAry[$LocationID]['BuildingEngName'];
            $LNameEng = $LocationAssoAry[$LocationID]['LocationLevelEngName'];
            $RNameEng = $LocationAssoAry[$LocationID]['RoomEngName'];
            $LocationNameEng = $BNameEng . ' > ' . $LNameEng . ' > ' . $RNameEng;
            return Get_Lang_Selection($LocationNameChi, $LocationNameEng);
        }

        function isLocationExistByID($LocationID)
        {
            $sql = "SELECT
						ilb.NameChi as BuildingChiName,
						ilb.NameEng as BuildingEngName,
						ilb.Code as BuildingCode,
						ill.NameChi as LocationLevelChiName,
						ill.NameEng as LocationLevelEngName,
						ill.Code as LocationLevelCode,
						il.NameChi as RoomChiName,
						il.NameEng as RoomEngName,
						il.Code as RoomCode,
						il.LocationID as LocationID,
						il.AllowBooking as isAllowBooking
					FROM 
						INVENTORY_LOCATION_BUILDING as ilb 
						LEFT JOIN INVENTORY_LOCATION_LEVEL as ill ON ilb.BuildingID = ill.BuildingID 
						LEFT JOIN INVENTORY_LOCATION as il ON ill.LocationLevelID = il.LocationLevelID
					WHERE 
						ilb.RecordStatus = 1 and ill.RecordStatus = 1 and il.RecordStatus = 1 AND il.LocationID = '$LocationID'
					ORDER BY
						ilb.Code,ill.Code,il.Code";
            $result = $this->returnResultSet($sql);
            return $result;
        }

        public function insertImportTempData($ValueArr)
        {
            
            // ## clear old data
            $this->deleteImportTempData();
            
            // ## insert new temp data
            $table = "TEMP_EBOOKING_RECORD_IMPORT";
            $sql = "INSERT INTO 
						$table
					(UserID,RowNumber,LocationID,TargetDate,StartTime,EndTime,OccupiedBy,Remarks,DateInput) 
					Values 
					" . implode(',', $ValueArr) . "";
            $successAry[] = $this->db_db_query($sql);
            
            return ! in_array(false, $successAry);
        }

        public function getImportTempData()
        {
            $table = "TEMP_EBOOKING_RECORD_IMPORT";
            $sql = "Select * from $table Where UserID = '" . $_SESSION["UserID"] . "' ";
            return $this->returnResultSet($sql);
        }

        public function deleteImportTempData()
        {
            $successAry = array();
            $table = "TEMP_EBOOKING_RECORD_IMPORT";
            
            $sql = "Delete from $table Where UserID = '" . $_SESSION["UserID"] . "' ";
            $successAry['DeleteTempImportCsvData'] = $this->db_db_query($sql);
            
            return ! in_array(false, $successAry);
        }

        public function getImportHeader()
        {
            $exportHeaderAry['En'] = array(
                "Building Code",
                "Location Code",
                "SubLocation Code",
                "Date",
                "StartTime",
                "EndTime",
                "Remarks",
                "OccupiedBy"
            );
            $exportHeaderAry['Ch'] = array(
                "大樓代號",
                "位置代號",
                "子位置代號",
                "日期",
                "開始時間",
                "結束時間",
                "備註",
                "使用者"
            );
            
            return $exportHeaderAry;
        }

        public function getImportColumnProperty()
        {
            return array(
                1,
                1,
                1,
                1,
                1,
                1,
                1,
                1
            );
        }

        public function getBookingRecordDetails($BookingIDArr)
        {
            global $Lang;
            
            $sql = 'SELECT
						ier.BookingID,
						iebd.FacilityType,
						iebd.FacilityID,
						ier.Date,
						ier.StartTime,
						ier.EndTime,
						CASE iebd.BookingStatus
							WHEN "' . LIBEBOOKING_BOOKING_STATUS_APPROVED . '" THEN "' . $Lang['General']['Approved'] . '"
							WHEN "' . LIBEBOOKING_BOOKING_STATUS_REJECTED . '" THEN "' . $Lang['General']['Rejected'] . '"
							WHEN "' . LIBEBOOKING_BOOKING_STATUS_PENDING . '" THEN "' . $Lang['General']['Pending'] . '" 
						END	as BookingStatus,
						ier.ResponsibleUID
					FROM
						INTRANET_EBOOKING_RECORD as ier
						INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS  as iebd on ier.BookingID = iebd.BookingID
					WHERE
						ier.BookingID IN ("' . implode('","', $BookingIDArr) . '")
						AND ier.RecordStatus = 1';
            
            $result = $this->returnResultSet($sql);
            return $result;
        }
        
        function getAvailableBookingPeriodFromEverydayTimetable($academicYearID, $startDate, $endDate, $facilityType=0, $facilityID=0)
        {
            global $intranet_root;
            include_once ($intranet_root. '/includes/libtimetable.php');
            $libtimetable = new Timetable();
            $skipSchoolEventDateAssoc = $libtimetable->getSkipSchoolEventDateByYear($academicYearID);   // event of full year's
            $skipSchoolEventDateAry = array_keys($skipSchoolEventDateAssoc);
            foreach((array)$skipSchoolEventDateAry as $_idx=>$_ts) {
                $skipSchoolEventDateAry[$_idx] = date('Y-m-d', $_ts);
            }

            $timetableDateAssoc = $libtimetable->getEverydayTimetableByPeriod($startDate, $endDate, $ts=false); // RecordDate as key
            $timetableIDAry = array_unique(array_values($timetableDateAssoc));
            $timeslotInfoAssoc = $libtimetable->getAllTimeslotByTimetableID($timetableIDAry);
            $suspensionDateAry = $this->Get_Booking_Day_Suspension($facilityType, $facilityID, $startDate, $endDate);
            $ignoreDateAry = array_unique(array_merge($skipSchoolEventDateAry,$suspensionDateAry));
            
            $returnAssoc = array();
            foreach((array)$timetableDateAssoc as $_date=>$_timetableID) {
                if (in_array($_date, $ignoreDateAry)) {
                    continue;
                }
                $returnAssoc[$_date] = $timeslotInfoAssoc[$_timetableID];
            }
            return $returnAssoc;                
        }
        
        function getActualTimeslotInfoByDefaltTimeslot($defaultTimeSlotID, $date) 
        {
            $sql = "SELECT
                    	s.StartTime, s.EndTime, s.TimeSlotID 
                    FROM
                    	INTRANET_TIMETABLE_TIMESLOT s
                    INNER JOIN
                    	INTRANET_TIMETABLE_TIMESLOT_RELATION r ON r.TimeSlotID=s.TimeSlotID
                    INNER JOIN
                    	INTRANET_SCHOOL_TIMETABLE t ON t.TimetableID=r.TimetableID
                    INNER JOIN
                    	INTRANET_SCHOOL_EVERYDAY_TIMETABLE t1 ON IF(t1.TimetableID=0,t1.DefaulttimetableID,t1.TimetableID)=t.TimetableID
                    INNER JOIN
                        INTRANET_TIMETABLE_TIMESLOT s2 ON (s2.DisplayOrder=s.DisplayOrder AND s2.StartTime=s.StartTime AND s2.EndTime=s.EndTime)
                    WHERE 
                    	t1.RecordDate='".$date."'
                        AND s2.TimeSlotID='".$defaultTimeSlotID."'
                    ";
            $result = $this->returnResultSet($sql);
            if (count($result)) {
                $result = current($result);
            }
            return $result;
        }
        
        function getBookingIDByTimeSlot($timeSlotID,$date='') 
        {
            $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE TimeSlotID='".$timeSlotID."'";
            if ($date != '') {
                $sql .= " AND Date='".$date."'";
            }
            $sql .= " ORDER BY BookingID";
            $tmpBookingIDAry = $this->returnResultSet($sql);
            
            if (count($tmpBookingIDAry)) {
                $bookingIDAry = Get_Array_By_Key($tmpBookingIDAry, 'BookingID');
            }
            else {
                $bookingIDAry = array();
            }
            return $bookingIDAry;
        }

        function getBookingIDByDate($date)
        {
            $sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE Date='".$date."'";
            $sql .= " ORDER BY BookingID";
            $tmpBookingIDAry = $this->returnResultSet($sql);
            
            if (count($tmpBookingIDAry)) {
                $bookingIDAry = Get_Array_By_Key($tmpBookingIDAry, 'BookingID');
            }
            else {
                $bookingIDAry = array();
            }
            return $bookingIDAry;
        }
        
        // used in:
        // 1. changing timetable of specific date in Everyday Timetable
        // 2. delete special time table
        function updateBookingTimeslotInfo($oldTimeSlotID, $newTimeSlotID, $date, $startTime, $endTime)
        {
            $result = array();
            $changeInputDateEventIDAssoc = array();
            $bookingIDAry = $this->getBookingIDByTimeSlot($oldTimeSlotID, $date);
            if (count($bookingIDAry)) {
                foreach($bookingIDAry as $bookingID) {
            
                    // update eBooking
                    $sql = "UPDATE INTRANET_EBOOKING_RECORD SET
                                StartTime='".$startTime."', 
                                EndTime='".$endTime."',
                                TimeSlotID='".$newTimeSlotID."',
                                ModifiedBy='".$_SESSION['UserID']."',
                                DateModified=NOW()
                            WHERE BookingID='".$bookingID."'";
                    $result['UpdateBooking'] = $this->db_db_query($sql);

                    // update iCalendar Event
                    $eventDateTime = $date.' '.$startTime;
                    $startTimeStamp = strtotime($eventDateTime);
                    $endTimeStamp = strtotime($date.' '.$endTime);
                    $duration = ($endTimeStamp - $startTimeStamp)/60;
                    
                    $sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID='".$bookingID."'";
                    $eventIDAry = $this->returnResultSet($sql);
                    if (count($eventIDAry)) {
                        $eventIDAssoc = BuildMultiKeyAssoc($eventIDAry,array('EventID'), array('EventID'),1);
                        if (count($eventIDAssoc)) {
                            foreach($eventIDAssoc as $_eventID) {
                                /* 
                                 * As CALENDAR_EVENT_ENTRY contains UNIQUE KEY UserID_2 (UserID,EventDate,InputDate,Title,CalID), need to check unique key violation
                                 * change (shift) InputDate temporary to allow update EventDate
                                 */
                                $sql = "SELECT 
                                            e1.EventID
                                        FROM
                                            CALENDAR_EVENT_ENTRY e1
                                        INNER JOIN
                                            CALENDAR_EVENT_ENTRY e2 ON (e2.UserID=e1.UserID AND e2.CalID=e1.CalID AND e2.InputDate=e1.InputDate AND e2.Title=e1.Title)
                                        WHERE
                                            e1.EventID<>e2.EventID
                                            AND e1.EventDate='".$eventDateTime."'
                                            AND e2.EventID='".$_eventID."'";
                                $_changeInputDateEventIDAry = $this->returnResultSet($sql);
                                
                                $iMax = count($_changeInputDateEventIDAry);
                                if ($iMax) {
                                    for ($i=0;$i<$iMax;$i++) {
                                        $__eventID = $_changeInputDateEventIDAry[$i]['EventID'];
                                        $j = $i+1;
                                        $sql = "UPDATE CALENDAR_EVENT_ENTRY SET InputDate=DATE_ADD(InputDate,INTERVAL {$j} SECOND) WHERE EventID='".$__eventID."'";
                                        $result['ShiftInputDate_'.$__eventID] = $this->db_db_query($sql);
                                        $changeInputDateEventIDAssoc[$__eventID] = $j;
                                    }
                                }
    
                                // now update new EventDate and Duration
                                $sql = "UPDATE 
                                            CALENDAR_EVENT_ENTRY 
                                        SET 
                                            EventDate='".$eventDateTime."',
                                            Duration='".$duration."',
                                            ModifiedDate=NOW()
                                        WHERE
                                            EventID='".$_eventID."'";
                                $result['UpdateCalendarEvent'] = $this->db_db_query($sql);
                                
                            }
                        }
                    }
                }
            }
            
            $result = in_array(false,$result) ? false : true;
            $ret = array("result"=>$result, "changeInputDate"=>$changeInputDateEventIDAssoc);
            return $ret;
        }
 
        // used by changing time range (StartTime and EndTime) in Timetable > Manage of specific timeslot of a timetable
        function updateBookingTimeInfo($timeSlotID, $startTime, $endTime)
        {
            $result = array();
            $changeInputDateEventIDAssoc = array();
            $bookingIDAry = $this->getBookingIDByTimeSlot($timeSlotID);
            
            if (count($bookingIDAry)) {
                // update eBooking
                $sql = "UPDATE INTRANET_EBOOKING_RECORD SET
                            StartTime='".$startTime."',
                            EndTime='".$endTime."',
                            ModifiedBy='".$_SESSION['UserID']."',
                            DateModified=NOW()
                        WHERE 
                            TimeSlotID='".$timeSlotID."'";
                $result['UpdateBooking'] = $this->db_db_query($sql);
                
                // update iCalendar Event
                $startTimeStamp = strtotime($startTime);
                $endTimeStamp = strtotime($endTime);
                $duration = ($endTimeStamp - $startTimeStamp)/60;
                
                $sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID IN ('".implode("','",$bookingIDAry)."')";
                $tmpEventIDAry = $this->returnResultSet($sql);
                
                if (count($tmpEventIDAry)) {
                    $eventIDAry = Get_Array_By_Key($tmpEventIDAry,'EventID');
                    unset($tmpEventIDAry);
                    
                    // now update new EventDate and Duration
                    $sql = "UPDATE
                                CALENDAR_EVENT_ENTRY
                            SET
                                EventDate=REPLACE(EventDate,RIGHT(EventDate,8),'".$startTime."'),
                                Duration='".$duration."',
                                ModifiedDate=NOW()
                            WHERE
                                EventID IN ('".implode("','",$eventIDAry)."')";
                    $result['UpdateCalendarEvent'] = $this->db_db_query($sql);
                    
                }
            }
            
            $result = in_array(false,$result) ? false : true;
            
            return $result;
        }
        
        // used by: 
        // 1. libcycleperiods->createSchoolHolidayEvent 
        // 2. libcycleperiods->updateSchoolHolidayEvent 
        // 3. delete time slot in Timetable > Manage of specific timeslot of a timetable
        // 4. update special time table
        // 5. delete special time table
        // 6. delete time table
        function rejectBookingByTimeSlot($timeSlotID, $date='')
        {
            $emailBookingIdAry = array();
            $overallResult = array();
            
            $bookingIDAry = $this->getBookingIDByTimeSlot($timeSlotID, $date);
            
            foreach($bookingIDAry as $_bookingID) {
                $result = array();
                $this->Start_Trans();
                
                $sql = "UPDATE 
                            INTRANET_EBOOKING_BOOKING_DETAILS 
                        SET 
                            PIC = '".$_SESSION['UserID']."', 
                            RejectReason = 'Delete TimeSlot ".$timeSlotID."', 
                            ProcessDate = NOW(), 
                            ProcessDateTime = NOW(), 
                            BookingStatus = -1 
                        WHERE 
                        BookingID = '$_bookingID'";
            
                $result['UpdateRecordDetails_'.$_bookingID] = $this->db_db_query($sql);
                
                if ($this->Check_All_Related_Booking_Request_Is_Processed($_bookingID)) {
                    $emailBookingIdAry[] = $_bookingID;
                    $sql = "SELECT
                                COUNT(*), COUNT(IF(BookingStatus = -1,1,NULL))
                            FROM
                                INTRANET_EBOOKING_BOOKING_DETAILS
                            WHERE
                                BookingID = '$_bookingID'";
                    list($TotalNumOfBookingResource, $TotalNumOfRejectedBooking) =  current($this->returnArray($sql));
                    
                    if($TotalNumOfBookingResource == $TotalNumOfRejectedBooking) {
                        $sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$_bookingID'";
                        $targetEventID = $this->returnVector($sql);
                        
                        if ($targetEventID[0]) {
                            $sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$targetEventID[0]."'";
                            $result['DeleteCalendarEventEntry_'.$_bookingID] = $this->db_db_query($sql);
                        }
                        
                        $sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = 0 WHERE BookingID = '$_bookingID'";
                        $result['UpdateEventRelation_'.$_bookingID] = $this->db_db_query($sql);
                        
                    }
                }
                    
                
                if(!in_array(false,$result)){
                    $this->Commit_Trans();
                    $overallResult[] = true;
                }else{
                    $this->RollBack_Trans();
                    $overallResult[] = false;
                }
                
            }   // loop foreach
           
            $emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
            if (count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->Email_Booking_Result_Merged($emailBookingIdAry);
            }
            
            return in_array(false,$overallResult) ? false : true;
        }

        // used by:
        // 1. delete Holiday / Event where isSkipCycle=1, 2. change event from non "Skip School Day" to "Skip School Day"
        function rejectBookingByDate($date)
        {
            $emailBookingIdAry = array();
            $overallResult = array();
            
            $bookingIDAry = $this->getBookingIDByDate($date);
            
            foreach($bookingIDAry as $_bookingID) {
                $result = array();
                $this->Start_Trans();
                
                $sql = "UPDATE
                            INTRANET_EBOOKING_BOOKING_DETAILS
                        SET
                            PIC = '".$_SESSION['UserID']."',
                            RejectReason = 'Delete Holiday/Event on ".$date."',
                            ProcessDate = NOW(),
                            ProcessDateTime = NOW(),
                            BookingStatus = -1
                            WHERE
                            BookingID = '$_bookingID'";
                
                $result['UpdateRecordDetails_'.$_bookingID] = $this->db_db_query($sql);
                
                if ($this->Check_All_Related_Booking_Request_Is_Processed($_bookingID)) {
                    $emailBookingIdAry[] = $_bookingID;
                    $sql = "SELECT
                    COUNT(*), COUNT(IF(BookingStatus = -1,1,NULL))
                    FROM
                    INTRANET_EBOOKING_BOOKING_DETAILS
                    WHERE
                    BookingID = '$_bookingID'";
                    list($TotalNumOfBookingResource, $TotalNumOfRejectedBooking) =  current($this->returnArray($sql));
                    
                    if($TotalNumOfBookingResource == $TotalNumOfRejectedBooking) {
                        $sql = "SELECT EventID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID = '$_bookingID'";
                        $targetEventID = $this->returnVector($sql);
                        
                        if ($targetEventID[0]) {
                            $sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$targetEventID[0]."'";
                            $result['DeleteCalendarEventEntry_'.$_bookingID] = $this->db_db_query($sql);
                        }
                        
                        $sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = 0 WHERE BookingID = '$_bookingID'";
                        $result['UpdateEventRelation_'.$_bookingID] = $this->db_db_query($sql);
                        
                    }
                }
                
                
                if(!in_array(false,$result)){
                    $this->Commit_Trans();
                    $overallResult[] = true;
                }else{
                    $this->RollBack_Trans();
                    $overallResult[] = false;
                }
                
            }   // loop foreach
            
            $emailBookingIdAry = array_values(array_unique($emailBookingIdAry));
            if (count($emailBookingIdAry) > 0) {
                // return true even failed to send email
                $SuccessArr['Send_eMail'] = $this->Email_Booking_Result_Merged($emailBookingIdAry);
            }
            
            return in_array(false,$overallResult) ? false : true;
        }
        
        // today or date in future only
        function getSkipSchoolDates($academicYearID, $startDate, $endDate, $facilityType=0, $facilityID=array())
        {
            global $intranet_root;
            include_once ($intranet_root. '/includes/libtimetable.php');
            $libtimetable = new Timetable();
            $today = date('Y-m-d');

            if ($startDate<$today) {
                $startDate = $today;
            }
            $skipSchoolEventDateAssoc = $libtimetable->getSkipSchoolEventDateByYear($academicYearID, $startDate, $endDate);   // event within the specified date range
            $skipSchoolEventDateAry = array_keys($skipSchoolEventDateAssoc);
            foreach((array)$skipSchoolEventDateAry as $_idx=>$_ts) {
                $skipSchoolEventDateAry[$_idx] = date('Y-m-d', $_ts);
            }
            
            $totalSuspensionDateAry = $this->Get_Booking_Day_Suspension($facilityType, 0, $startDate, $endDate);     // if there's default suspension date, no need to check specific facility
            
            if (is_array($facilityID) && count($facilityID)) {
                foreach((array)$facilityID as $_facilityID) {
                    $suspensionDateAry = $this->Get_Booking_Day_Suspension($facilityType, $_facilityID, $startDate, $endDate);  // get suspension date by FacilityID
                    $totalSuspensionDateAry = array_merge($totalSuspensionDateAry, $suspensionDateAry);
                }
            }
            
            $totalSuspensionDateAry = array_unique($totalSuspensionDateAry);
            
            $returnAry = array_unique(array_diff($skipSchoolEventDateAry,$totalSuspensionDateAry));
            return $returnAry;
        }

        // get Location that have ever been booked but now it's set to un-bookable
        function getUnbookableLocation()
        {
            $sql = "SELECT DISTINCT loc.LocationID
                    FROM
                        INVENTORY_LOCATION loc
                        INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS bd ON (bd.FacilityType=1 AND bd.FacilityID=loc.LocationID)
                    WHERE
                        loc.AllowBooking=0
                        AND loc.RecordStatus=1
                        ORDER BY loc.LocationID";
            $locationIDAry = $this->returnVector($sql);
            return $locationIDAry;
        }

        // get item that have ever been booked but now it's set to un-bookable
        function getUnbookableItem()
        {
            $sql = "SELECT DISTINCT i.ItemID
                    FROM
                        INTRANET_EBOOKING_ITEM i
                        INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS bd ON (bd.FacilityType=2 AND bd.FacilityID=i.ItemID)
                    WHERE
                        i.AllowBooking=0
                        AND i.RecordStatus=1
                        ORDER BY i.ItemID";
            $itemIDAry = $this->returnVector($sql);
            return $itemIDAry;
        }

        function getDisplayOrderByTimeSlotID($timeSlotID)
        {
            if (!is_array($timeSlotID)) {
                $timeSlotID = array($timeSlotID);
            }

            // DisplayOrder-1 for compare index
            $sql = "SELECT 
                        TimeSlotID, 
                        (DisplayOrder-1) AS DisplayOrder
                    FROM
                        INTRANET_TIMETABLE_TIMESLOT 
                    WHERE
                        TimeSlotID IN ('".implode("','",$timeSlotID)."')
                        ORDER BY TimeSlotID";
            $resultAry = $this->returnResultSet($sql);
            $resultAssoc = BuildMultiKeyAssoc($resultAry,'TimeSlotID','DisplayOrder',1);
            return $resultAssoc;
        }

        // return timeslot list that has max number of timeslot in all timetables of the academic year that cover the given date
        function getTimeslotListOfTimetableWithMaxNumOfSlotsByDate($date)
        {
            global $Lang;
            $termAry = getAcademicYearAndYearTermByDate($date);
            $academicYearID = $termAry['AcademicYearID'];
            $sql = "SELECT
                        ist.TimetableID, COUNT(itt.TimeSlotID) AS NrTimeSlot
                    FROM 
                        INTRANET_TIMETABLE_TIMESLOT itt
                        INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION  ittr ON ittr.TimeSlotID=itt.TimeSlotID
                        INNER JOIN INTRANET_SCHOOL_TIMETABLE ist ON ist.TimetableID=ittr.TimetableID
                    WHERE 
                        ist.AcademicYearID='".$academicYearID."'
                    GROUP BY ist.TimetableID
                    ORDER BY NrTimeSlot DESC, ist.TimetableID LIMIT 1";
            $maxTimeSlotAry = $this->returnResultSet($sql);
            if (count($maxTimeSlotAry)) {
                $timetableID = $maxTimeSlotAry[0]['TimetableID'];
                $sql = "SELECT 
                                itt.TimeSlotID,
                                itt.DisplayOrder                                 
                        FROM 
                                INTRANET_TIMETABLE_TIMESLOT itt
                                INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION ittr ON ittr.TimeSlotID=itt.TimeSlotID
                        WHERE 
                                ittr.TimetableID='".$timetableID."'
                                ORDER BY itt.DisplayOrder";
                $timeSlotAry = $this->returnResultSet($sql);
                $nrTimeSlot = count($timeSlotAry);
                if ($nrTimeSlot) {
                    $resultAry = array();
                    for($i=0; $i<$nrTimeSlot; $i++) {
                        $resultAry[$i][0] = $timeSlotAry[$i]['TimeSlotID'];
                        $j = $i + 1;
                        $timeSlotTitle = str_replace("num", $j, $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['TimeSlotTitle']);
                        $resultAry[$i][1] = $timeSlotTitle;
                    }
                    return $resultAry;
                }
                return false;
            }
            return false;
        }

        function getDisplayOrderByPeriodID($periodID)
        {
            if (!is_array($periodID)) {
                $periodID = array($periodID);
            }

            // DisplayOrder-1 for compare index
            $sql = "SELECT 
                        DISTINCT
                        itt.TimeSlotID, 
                        itt.DisplayOrder
                    FROM
                        INTRANET_TIMETABLE_TIMESLOT itt
                        INNER JOIN INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD abp ON abp.TimeSlotID=itt.TimeSlotID 
                    WHERE
                        abp.PeriodID IN ('".implode("','",$periodID)."')
                        ORDER BY itt.DisplayOrder";
            $resultAry = $this->returnResultSet($sql);
            return $resultAry;
        }
        
        function getCancelledBooking($bookingId){
            $sql = "Select * From INTRANET_EBOOKING_BOOKING_DETAILS WHERE ProcessDate IS NOT NULL AND BookingStatus = 0 AND RecordStatus = 1 AND BookingID = ".$bookingId;
            $resultAry = $this->returnArray($sql);
            return $resultAry;
        }
        

    } // end of libebooking
}
?>