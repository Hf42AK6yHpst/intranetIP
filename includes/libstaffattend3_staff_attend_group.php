<?
// using by kenenth chung
include_once("libdb.php");

class staff_attend_group extends libdb {
	var $GroupID;
	var $GroupName;
	var $MemberList;
	var $SlotList;
	
	function staff_attend_group($GroupID="",$GetMember=false,$GetSlotInfo=false) {
		parent::libdb();
		
		if ($GroupID != "") {
			$sql = 'select 
								GroupID, 
								GroupName 
							From 
								CARD_STAFF_ATTENDANCE2_GROUP 
							where 
								GroupID = \''.$GroupID.'\'';
			$Result = $this->returnArray($sql);
			
			$this->GroupID = $Result[0]['GroupID'];		
			$this->GroupName = $Result[0]['GroupName'];
			
			if ($GetMember) {
				$sql = 'select 
									u.UserID 
								From 
									CARD_STAFF_ATTENDANCE_USERGROUP ug
									inner join 
									INTRANET_USER u
									on ug.GroupId = \''.$GroupID.'\' and ug.UserId = u.UserID and u.RecordStatus = \'1\' 
								Order by 
									u.EnglishName
								';
				//debug_r($sql);
				$this->MemberList = $this->returnVector($sql);
			}
			
			if ($GetSlotInfo) {
				$sql = 'select 
									SlotID, 
									SlotName,
									SlotStart,
									SlotEnd, 
									DutyCount,
									IF(InWavie=1,\'Y\',\'N\') as InWavie,
									IF(OutWavie=1,\'Y\',\'N\') as OutWavie
								From 
									CARD_STAFF_ATTENDANCE3_SLOT 
								Where 
									GroupID = \''.$GroupID.'\' 
							 ';
				$this->SlotList = $this->returnArray($sql);
			}
		}
	}
}
?>