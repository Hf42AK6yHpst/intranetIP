<?php
// Modifying by: 

// #################################### !!! IMPORTANT !!! #####################################
// #### If upload this file to client site before ip.2.5.6.10.1, #####
// #### please upload with the addon schema and update the schema in client site #####
// #################################### !!! IMPORTANT !!! #####################################

// ########### Change Log [start] ###############
//	Date: 2020-01-09 Philips [DM#3729]
//  modified getParentStudentMappingInfo(), allow filter parent status
//
//  Date: 2019-09-25 Tommy
//  added approvedUserinEnrol() to get enrol group user
//  added groupRecordExist() to check record already exist 
//
//  Date: 2019-09-19 Bill   [2019-0917-1522-01206]
//  Modified getParentStudentMappingInfo(), to add parms to skip student status checking
//
//  Date: 2019-05-31 Anna
//  Modified CheckPasswordSafe(), require update if have update period date and last modified is null
//
// Date: 2019-05-09 Carlos
//  Modified returnUsersType(), fetch UserLogin, RecordType, RecordStatus.
//
// Date: 2019-04-30 Henry
//  Modified prepareUserRemoval(), added OsCommandSafe for $command
// 
// Date 2018-10-12 Isaac
//  added isDisplayinEComunity() to check if the last group is allowed to be access in eComunity
//
// Date 2018-08-31 Philips
// added getAccountLog(), insertAccountLog()
//
// Date 2018-02-12 Omas [ip.2.5.9.3.1]
// modified GET_OFFICIAL_PHOTO_BY_USER_ID() - support archive user
//
// Date 2017-10-19 Ivan [ip.2.5.9.1.1]
// added returnDefaultOfficialPhotoPath(), returnDefaultPersonalPhotoPath()
// modified GET_USER_PHOTO(), LoadUserData() to support new official photo path
//
// Date 2017-07-12 Paul
// modified removeUsers(), createUsers() - NCS track action and log
//
// Date 2017-06-28 Icarus
// modified returnUsersByIdentity() - added $excludeUserIdAry
//
// Date 2017-05-24 Anna
// modified returnUsersByIdentity() - added $sys_custom['eEnrolment']['ShowStudentNickName']
//
// Date 2017-05-10 Siuwan
// modified GET_USER_PHOTO() - added parameter $UseSamplePhoto to decide whether return null or sample photo if user does not have official photo and personal photo
//
// Date 2017-04-20 Carlos
// modified prepareUserRemoval($list) - $sys_custom['DHL'] remove DHL related user records.
//
// Date 2017-03-13 Carlos
// modified prepareUserRemoval($list) - remove iMail db records that not handled before.
//
// Date 2016-09-09 Omas
// added getUserIDsByJupasNumArr()- for get student by jupas no
//
// Date 2016-08-16 Kenneth
// modified returnUsersType(), add select ChineseName and English Name
//
// Date 2016-08-09 Carlos
// $sys_custom['BIBA_AccountMgmtCust'] - aded HouseholdRegister for BIBA.
//
// Date 2016-06-14 Omas
// modified getUserIDsByWebSamsRegNoArr() - add parm for always return lowercase
//
// Date 2016-06-10 (Anna, Cara)
// add InputBy, DateInput, CreatedBy

// Date 2016-05-30 Siuwan
// modified function LoadUserData() and returnUser(), added field DeviceID
// Deploy: ip.2.5.7.7.1.0
//
// Date 2016-04-08 Omas
// added function getUserIDsByHKIDArr()
// Deploy: ip.2.5.7.4.1.0
//
// Date 2016-03-11 Ivan
// added function getStudentWithStudentUsingStudentApp()
// Deploy: ip.2.5.7.4.1.0
//
// Date 2016-02-26 Omas
// modified getUserIDsByWebSamsRegNoArr() , getUserIDsBySTRNArr() - add support to archived user
//
// Date 2016-01-15 Omas
// added getUserIDsByWebSamsRegNoArr() , getUserIDsBySTRNArr()
//
// Date: 2016-01-14 Carlos
// $sys_custom['StudentAccountAdditionalFields'] modified returnUser() and LoadUserData(), added INTRANET_USER.[NonChineseSpeaking,SpecialEducationNeeds,PrimarySchool,University,Programme].
//
// Date: 2015-12-14 Carlos
// modified prepareUserRemoval($list), remove radius server user records.
//
// Date: 2015-11-24 Kenneth
// modified returnUsersByIdentity(), added system flag control($sys_custom['hideTeacherTitle']) to hide teacher title
//
// Date: 2015-10-13 Yuen [2015-0922-1008-00071]
// fixed: improved synUserDataToModules() to syn. official photo (primary) or personal photo
//
// Date: 2015-09-30 (Ivan)
// modified returnUser(), LoadUserData() to return HKEdCityUserLogin
// Deploy: ip.2.5.6.10.1.0 ************ NEED TO UPLOAD WITH ADDON SCHEMA ************
//
// Date: 2015-09-29 (Bill)
// modified Get_User_By_HKEdCityUserLogin() to return UserID and UserLogin
// Deploy: ip.2.5.6.10.1.0
//
// Date: 2015-09-23 (Paul)
// modified Get_User_Studying_Form to return YearName field also
// Deploy: ip.2.5.6.10.1.0
//
// Date: 2015-09-09 (Ivan)
// modified returnGroups() to return TitelChinese field also
// Deploy: ip.2.5.6.10.1.0
//
// Date: 2015-07-23 (Roy)
// modified getTeacherWithTeacherUsingTeacherApp(), added parameter $includeLoggedOutTeacher=false for receiving push message while logged out feature in teacher app
// Deploy: ip.2.5.6.8.1.0
//
// Date: 2015-06-05 (Omas)
// Added getParentGuardianInfoAry(), modified getGuardianInfoAry()
// Deploy: ip.2.5.6.8.1.0
//
// Date: 2015-05-14 (Omas)
// add getUserInfoByUserLogin()
// Deploy: ip.2.5.6.8.1.0
//
// Date: 2015-04-01 (Carlos)
// modified prepareUserRemoval(), for iMail plus remove quota records and shared mailbox member records for deleted users.
// Deploy: ip.2.5.6.5.1.0
//
// Date: 2014-10-16 (Siuwan)
// modified function prepareUserRemoval() to add delete user log for user_course
// Deploy: ip.2.5.5.10.1.0
//
// Date: 2014-10-08 (Ivan)
// added function getParentUsingParentApp() to get parent list who is using parent app
// Deploy: ip.2.5.5.10.1.0
//
// Date: 2014-10-07 (Henry)
// modified synUserDataToModules() to assign the gender information to LIBMS_USER table
//
// Date: 2014-09-18 (Ivan)
// modified getTeacherWithTeacherUsingTeacherApp() and getStudentWithParentUsingParentApp() to return user with registered device only
//
// Date: 2014-09-10 (Carlos)
// modified prepareUserRemoval(), delete ACCESS_RIGHT_GROUP_MEMBER and DISCIPLINE_DETENTION_SESSION_PIC which have constraint with INTRANET_USER.UserID
//
// Date: 2014-09-08 (Bill)
// modified LoadUserData() to set photo link null when offical photo link is empty
//
// Date: 2014-08-18 (Bill)
// modified returnUserID() to get user ID from target year
//
// Date: 2014-07-10 (Ivan)
// Added getTeacherWithTeacherUsingTeacherApp() to get teacher app users only
// Deploy: ip.2.5.5.8.1.0
//
// Date: 2014-06-05 (YatWoon)
// Fixed: Incorrect db field name for INTRANET_CAMPUSMAIL_FOLDER
// Deploy: ip.2.5.5.7.1.0
//
// Date: 2014-04-11 (Ivan)
// modified getParent() and getStudentWithParentUsingParentApp() to check different log table for eClassApp
//
// Date: 2013-1-24 (Roy)
// modified getParent(), modified filterAppParent case: do not get student login record
//
// Date: 2013-12-11 (Carlos)
// Modified prepareUserRemoval(), cater iMail plus email for $sys_custom['iMailPlus']['EmailAliasName']
//
// Date: 2013-10-09 (Roy)
// modified getParent(), add $UserRecordStatus, $getAllParent parameter
//
// Date: 2013-09-27 (Ivan)
// modified returnNextUserId() to consider auto increment value of INTRANET_USER and INTRANET_ARCHIVE_USER when calculation the next userId
//
// Date: 2013-08-28 (Ivan)
// modified synUserDataToModules() to add user to default group of LMS if the user is newly added to LMS
//
// Date: 2013-07-17 (Ivan)
// added returnNextUserId() for creating user account
//
// Date: 2013-06-26 (YatWoon)
// added returnStudentClassNameClassNumber()
//
// Date: 2013-05-22 (Ivan)
// added getUserInfoByLogin() for auto-complete search user by user login
//
// Date: 2013-04-17 (Yuen)
// added AddOrUpdateAlumniYearGroup() for Alumni module
//
// Date: 2013-04-03 (YatWoon)
// modified returnUser(), LoadUserData(), add "ModifyBy"
//
// Date: 2013-02-04 (YatWoon)
// add returnParentList()
//
// Date: 2012-10-25 (Rita)
// copy getUserInfoByName(), getGuardianInfoAry() from EJ
//
// Date: 2012-09-19 (Ivan)
// updated synUserDataToModules() to syn user barcode to eLib+
//
// Date: 2012-09-11 (Carlos)
// updated prepareUserRemoval(), added missing $lldap->connect()
//
// Date: 2012-07-11 (Ivan)
// modified function returnUsersType2() to add param $userIdAry to return specific users' data
//
// Date: 2012-06-25 (Ivan)
// added function synUserDataToModules() to syn User Data to LMS (can add coding for different modules if the module need to syn the user data from INTRANET_USER)
//
// Date: 2012-05-24 (Henry Chow)
// update GET_USER_BY_CLASS_CLASSNO(), also return ClassTitleB5
//
// Date: 2012-02-13 (Marcus)
// update returnEmailNotificationData_HashedPw_ACK()
//
// Date: 2012-02-02 (YatWoon)
// update returnGroups(), missing retrieve alumni group
//
// Date: 2011-12-28 (Carlos)
// modified getParent(), add parameter $filterAppParent to select parents that have purchased/can access parent app
//
// Date: 2011-12-14 (YatWoon)
// update returnUsersByIdentity(), add parameter $UserRecordStatus
//
// Date: 2011-11-04 (Carlos)
// add isAlumni()
//
// Date: 2011-11-03 (YatWoon)
// updated setToAlumni(), add yearofleft and classname, classnumber
//
// Date: 2011-08-08 (Ivan)
// modified Get_User_Studying_Form() to add Form WebSAMS Code in the return array
//
// Date: 2011-08-01 (Henry Chow)
// modify getNameWithClassNumber(), add parameter $IncludeArchivedUser in order to retrieve archived user info (for display user info in enrolment club/activity PIC)
//
// Date: 2011-08-01 (Henry Chow)
// modify prepareUserRemoval(), archive of staff will not remove the PIC on Enrolment (club / activity)

// # Date: 2011-03-30 Marcus
// modify returnUserID, get user id by class name and class number from YEAR_CLASS_USER instead of getting from INTRANET_USER
//
// Date: 2011-03-28 YatWoon
// add function returnEmailNotificationData(), return notification email subject & content
//
// Date : 2011-02-06 [Marcus]
// modified getNameWithClassNumber(), cater retrieve by batch.
//
// Date : 2011-01-31 [Marcus]
// added getUserIDByWebSamsRegNo()
//
// Date : 2011-01-13 [YatWoon]
// update getChildrenList()
// - fixed pass the parent id to the function with new libuser($id)
// - set alias to $namefield
//
// Date : 2010-12-14 [Ivan]
// added getChildrenSelection() to get the children selection if the user is a parent
//
// Date : 2010-12-13 [YatWoon]
// update returnGroupsWithFunctionAdmin(), display group name according to lang session
//
// Date : 2010-11-17 [Marcus]
// modified LoadUserData,
// original logic: if Student data do not exist in BatchUser, return the first one.
// now: return empty array instead.
//
// Date : 2010-10-12 [Yuen]
// modified libuser() to preload multiple user data using one query
// added AssignToBatchUsers to set user data to $this->BatchUsers in associative array using user_id
// added LoadUserData to load user data to current object according to user_id
//
// Date : 2010-09-13 [Marcus]
// modified returnGroupIDs, returnGroups, get Group id of current school year only
//
// Date : 2010-09-13 [Henry Chow]
// modified getParent(), display more than 1 parent if student linked to more parents
//
// Date : 2010-09-10 YatWoon
// Add personal photo link
//
// Date : 2010-09-09 YatWoon
// update UserNameLang(), display teacher name with TitleEnglish / TitleChinese
//
// Date : 2010-09-08 Henry Chow
// modified getParent(), return 1 more field (student name)
//
// Date : 2010-09-06 Carlos
// Fix prepareUserRemoval(), which delete the Gamma Mail account when user account is deleted
//
// Date : 2010-09-03 YatWoon
// add "LastLang" for user
//
// Date : 2010-09-02 Ronald
// update prepareUserRemoval(), so now will delete the Gamma Mail account when user account is deleted
//
// Date : 2010-08-30 Marcus
// Modified returnGroupsWithFunctionAdmin, Set FunctionAccess to All if FuntionAccess = 0
//
// Date : 2010-08-24 YatWoon
// update GET_OFFICIAL_PHOTO(), GET_OFFICIAL_PHOTO_BY_USER_ID()
// add RetrieveUserInfoSetting()
//
// Date : 2010-08-16 (Henry Chow)
// Details : modified prepareUserRemoval(), delete Student(s) in detention session (since foreign is set)
//
// Date : 2010-08-11 (Marcus)
// Details : modified returnUsersByIdentity, check student existence by checking YEAR_CLASS_USER.ClassNumber , YEAR_CLASS.YearClassID instead of checking INTRANET_USER
//
// Date : 2010-08-11 (kenneth chung)
// Details : Modify constructor, returnUser functions to allow retrieve user info by UserLogin
//
// Date : 2010-06-21 (kenneth chung)
// Details : Modify GET_OFFICIAL_PHOTO, GET_USER_PHOTO function, return one more value $photolink
//
// Date : 2010-06-07 (kenneth chung)
// Details : add RFID field on object for eattendance purpose
//
// Date : 2010-05-17 (Henry Chow)
// Details : modified prepareUserRemoval(), add "Removal of activity"
//
// Date : 2010-05-17 (Henry Chow)
// Details : modified returnUsersType2(), add option of TeacherType (Teaching / non-teaching staff)
//
// Date : 2010-04-22 Ivan
// Details : added function Get_User_Studying_Form() and Get_Children_Studying_Form() to get the Form information of the user
//
// Date : 2010-04-15 Sandy
// Details : returnUsersType() , add param "$condition" [optional], EG. to exclude certain users
//
// Date : 2010-03-16 [YatWoonb]
// Details : update returnUsersType(), returnUsersType2()
// force to order by English name, Chinese name no matter the interface is English/Chinese
//
// - 20100308 Kenneth Chung:
// add StandardDisplayName for object to that call getNameFieldByLang common functions for the field
//
//
// - 20100301 Marcus:
// add ImapUserEmail in Object
//
// - 20100114 Marcus:
// add return field DisplayInCommunity in function returnGroupsWithFunctionAdmin
//
// - 2010-01-13 [Henry]
// add returnUserIDByClassIDClassNumber()
// retrieve UserID by ClassID & ClassNumber
//
// - 2009-12-22 [YatWoon]
// update returnGroupsWithFunctionAdmin()
// Distinct Group ID to prevent duplicate group
//
// - 2009-12-22 [YatWoon]
// Add function getParent()
// Retrieve parent info (array) with StudentIDStr
// ########### Change Log [end] #################
if (! defined("LIBUSER_DEFINED")) // Preprocessor directives
{
    
    define("LIBUSER_DEFINED", true);

    class libuser extends libdb
    {

        var $User;

        var $UserID;

        var $UserLogin;

        var $UserPassword;

        var $UserEmail;

        var $ImapUserEmail;

        var $FirstName;

        var $LastName;

        var $ChineseName;

        var $EnglishName;

        var $NickName;

        var $DisplayName;

        var $Title;

        var $Gender;

        var $DateOfBirth;

        var $HomeTelNo;

        var $OfficeTelNo;

        var $MobileTelNo;

        var $FaxNo;

        var $ICQNo;

        var $Address;

        var $Country;

        var $Info;

        var $Remark;

        var $ClassNumber;

        var $ClassName;

        var $ClassLevel;

        var $LastUsed;

        var $LastModifiedPwd;

        var $RecordType;

        var $RecordStatus;

        var $DateInput;

        var $DateModified;

        var $URL;

        var $PhotoLink;

        var $sessionKey;

        var $teaching;

        var $CardID;

        var $RFID;

        var $TitleChinese;

        var $TitleEnglish;

        var $WebSamsRegNo;

        var $HKID;

        var $STRN;

        var $StandardDisplayName;

        var $LastLang;

        var $PersonalPhotoLink;

        var $BatchUsers;

        var $ModifyBy;

        var $HKEdCityUserLogin;

        var $NonChineseSpeaking;
 // $sys_custom['StudentAccountAdditionalFields']
        var $SpecialEducationNeeds;
 // $sys_custom['StudentAccountAdditionalFields']
        var $PrimarySchool;
 // $sys_custom['StudentAccountAdditionalFields']
        var $University;
 // $sys_custom['StudentAccountAdditionalFields']
        var $Programme;
 // $sys_custom['StudentAccountAdditionalFields']
        var $DeviceID;

        var $HouseholdRegister;
 // $sys_custom['BIBA_AccountMgmtCust']
                                
        // #####################################################################################
        function libuser($uid = "", $UserLogin = "", $UserIDsArray = null)
        {
            global $special_feature, $intranet_root;
            
            /*
             * $is_new_user_ini = false;
             * if (is_array($LIB_USER_ID) && !in_array($uid, $LIB_USER_ID))
             * {
             * $LIB_USER_ID[] = $uid;
             * $is_new_user_ini = true;
             * }
             *
             */
            
            $this->libdb();
            
            /*
             * if (isset($lu) && $lu->UserID==$uid && $uid!="")
             * {
             * $this->UserID = $lu->UserID;
             * $this->UserLogin = $lu->UserLogin;
             * $this->UserPassword = $lu->UserPassword;
             * $this->UserEmail = $lu->UserEmail;
             * $this->FirstName = $lu->FirstName;
             *
             * $this->Title = $lu->Title;
             * $this->Gender = $lu->Gender;
             * $this->DateOfBirth = $lu->DateOfBirth;
             * debug ("same user id = $uid");
             * return;
             * }
             */
            
            if ($uid != "" || $UserLogin != "" || (is_array($UserIDsArray) && sizeof($UserIDsArray) > 0)) {
                $BatchUsers = $this->returnUser($uid, $UserLogin, $UserIDsArray);
                
                if (sizeof($BatchUsers) > 0) {
                    $this->AssignToBatchUsers($BatchUsers);
                }
                
                $this->User = $BatchUsers;
                // load this first user
                $this->LoadUserData($this->User[0][0]);
            }
            if ($uid == $_SESSION['UserID'] && $_SESSION['eclass_session_password'] != "") {
                $this->UserPassword = $_SESSION['eclass_session_password'];
            }
        }
        
        // added on 2010-10-12
        function LoadUserData($uid)
        {
            global $special_feature, $intranet_root, $sys_custom;
            
            if (is_array($this->BatchUsers[$uid]) && sizeof($this->BatchUsers[$uid]) > 0) {
                $userobj = $this->BatchUsers[$uid];
            } else {
                /*
                 * modified on 20101114, if student data do not exist in INTRANET_USER,
                 * system used to return nothing instead of the first Batch User(another user).
                 */
                // $userobj = $this->User[0];
                $userobj = array();
            }
            $this->UserID = $userobj[0];
            $this->UserLogin = $userobj[1];
            $this->UserPassword = $userobj[2];
            $this->UserEmail = $userobj[3];
            $this->FirstName = $userobj[4];
            $this->LastName = $userobj[5];
            $this->ChineseName = $userobj[6];
            $this->NickName = $userobj[7];
            $this->DisplayName = $userobj[8];
            $this->Title = $userobj[9];
            $this->Gender = $userobj[10];
            $this->DateOfBirth = $userobj[11];
            $this->HomeTelNo = $userobj[12];
            $this->OfficeTelNo = $userobj[13];
            $this->MobileTelNo = $userobj[14];
            $this->FaxNo = $userobj[15];
            $this->ICQNo = $userobj[16];
            $this->Address = $userobj[17];
            $this->Country = $userobj[18];
            $this->Info = $userobj[19];
            $this->Remark = $userobj[20];
            $this->ClassNumber = $userobj[21];
            $this->ClassName = $userobj[22];
            $this->ClassLevel = $userobj[23];
            $this->LastUsed = $userobj[24];
            $this->LastModifiedPwd = $userobj[25];
            $this->RecordType = $userobj[26];
            $this->RecordStatus = $userobj[27];
            $this->DateInput = $userobj[28];
            $this->DateModified = $userobj[29];
            $this->URL = $userobj[30];
            $this->EnglishName = $userobj[31];
            $this->PhotoLink = $userobj[32];
            // special handle if $this->PhotoLink not exist , get from GET_USER_PHOTO();
            if (empty($this->PhotoLink) || ! file_exists($intranet_root . $this->PhotoLink)) {
                // commented on 2014-09-08: display default photo instead of personal photo [2014-0906-1149-18071]
                // list(,,$this->PhotoLink) = $this->GET_USER_PHOTO($this->UserLogin, $this->UserID);
                // $this->PhotoLink = "/images/myaccount_personalinfo/samplephoto.gif";
                $this->PhotoLink = $this->returnDefaultOfficialPhotoPath();
            }
            
            $this->sessionKey = $userobj[33];
            $this->teaching = $userobj[34];
            $this->CardID = $userobj[35];
            $this->TitleChinese = $userobj[36];
            $this->TitleEnglish = $userobj[37];
            $this->WebSamsRegNo = $userobj[38];
            $this->HKID = $userobj[39];
            $this->ImapUserEmail = $userobj[40];
            $this->StandardDisplayName = $userobj[41];
            if ($special_feature['ava_strn']) {
                $this->STRN = $userobj[42];
                $this->RFID = $userobj['RFID']; // added by kenneth chung 2010-06-02
            } else {
                $this->RFID = $userobj['RFID']; // added by kenneth chung 2010-06-02
            }
            $this->LastLang = $userobj['LastLang'];
            $this->PersonalPhotoLink = $userobj['PersonalPhotoLink'];
            $this->ModifyBy = $userobj['ModifyBy'];
            $this->HKEdCityUserLogin = $userobj['HKEdCityUserLogin'];
            if ($sys_custom['StudentAccountAdditionalFields']) {
                $this->NonChineseSpeaking = $userobj['NonChineseSpeaking'];
                $this->SpecialEducationNeeds = $userobj['SpecialEducationNeeds'];
                $this->PrimarySchool = $userobj['PrimarySchool'];
                $this->University = $userobj['University'];
                $this->Programme = $userobj['Programme'];
            }
            $this->DeviceID = $userobj['DeviceID'];
            if ($sys_custom['BIBA_AccountMgmtCust']) {
                $this->HouseholdRegister = $userobj['HouseholdRegister'];
            }
            $this->InputBy = $userobj['InputBy'];
            $this->DateInput = $userobj['DateInput'];
        }

        function Get_Children_Selection($Id, $Name, $SelectedChildId, $ParentId = '', $OnChange = '')
        {
            $ChildArr = $this->getChildrenList($ParentId);
            
            if ($OnChange != '') {
                $onchange_para = ' onchange="' . $OnChange . '" ';
            }
            $para = ' id="' . $Id . '" name="' . $Name . '" ' . $onchange_para;
            
            return getSelectByArray($ChildArr, $para, $SelectedChildId, $all = 0, $noFirst = 1, $FirstTitle = "");
        }
        
        // added on 2010-10-12
        function AssignToBatchUsers($BatchUsers)
        {
            for ($i = 0; $i < sizeof($BatchUsers); $i ++) {
                // store using UserID
                $this->BatchUsers[$BatchUsers[$i][0]] = $BatchUsers[$i];
            }
        }

        function CheckPasswordSafe($password = "")
        {
        	global $sys_custom;
            $password = ($password != "") ? $password : $this->UserPassword;
            if ($password != "") {
                $thisUserType = $this->RecordType;
                
                $SettingNameArr[] = 'CanUpdatePassword_' . $thisUserType;
                $SettingNameArr[] = 'EnablePasswordPolicy_' . $thisUserType;
                $SettingNameArr[] = 'RequirePasswordPeriod_' . $thisUserType;
                
                $SettingArr = $this->RetrieveUserInfoSettingInArrary($SettingNameArr);
      
                if ($SettingArr['CanUpdatePassword_' . $thisUserType]) {
                    if ($this->UserLogin == $password) {
                        return - 1;
                    }
                    
                    // password policy
                    if ($SettingArr['EnablePasswordPolicy_' . $thisUserType] != "" && $SettingArr['EnablePasswordPolicy_' . $thisUserType] >= 1) {
                        $PasswordLength = $SettingArr['EnablePasswordPolicy_' . $thisUserType];
                        if ($PasswordLength < 6 && $PasswordLength > 0) {
                            // at least 6 by default
                            $PasswordLength = 6;
                        }
                        if($sys_custom['UseStrongPassword'] && ($PasswordLength == '' || $PasswordLength<6)){
							$PasswordLength = 6;
						}
                        if (strlen($password) < $PasswordLength) // at least characters
{
                            return - 2;
                        } elseif (! preg_match('`[A-Z]`', strtoupper($password))) // at least one upper case
{
                            return - 3;
                        } elseif (! preg_match('`[0-9]`', $password)) // at least one digit
{
                            return - 4;
                        }
                    }
              
                    $UpdatePeriodDay = $SettingArr['RequirePasswordPeriod_' . $thisUserType];
                    // $UpdatePeriodDay = 1;
                    if ($UpdatePeriodDay > 0 && $this->LastModifiedPwd != "") {
                      
                        // $UpdatePeriodDay = 0;
                        if ((time() > strtotime($this->LastModifiedPwd) + $UpdatePeriodDay * 3600 * 24) || $this->LastModifiedPwd == '') {
                            // require update if exceeds the period
                            return - 5;
                        }
                    }
                    else if ($this->LastModifiedPwd == "" && $UpdatePeriodDay > 0){                                       
                            // require update if have update period date and last modified is null
                            return - 5;                    
                    }
                }
            }
            
            return 1;
        }

        function isIdentity($target)
        {
            if ($this->UserID == "")
                return false;
            return ($this->RecordType == $target);
        }

        function isTeacherStaff()
        {
            return $this->isIdentity(1);
        }

        function isStudent()
        {
            return $this->isIdentity(2);
        }

        function isParent()
        {
            return $this->isIdentity(3);
        }

        function isAlumni()
        {
            return $this->isIdentity(4);
        }

        function returnUser($UserID = "", $UserLogin = "", $UserIDsArray = null)
        {
            global $special_feature, $sys_custom;
            
            $fieldname = "UserID, ";
            $fieldname .= "UserLogin, ";
            $fieldname .= "UserPassword, ";
            $fieldname .= "UserEmail, ";
            $fieldname .= "FirstName, ";
            $fieldname .= "LastName, ";
            $fieldname .= "ChineseName, ";
            $fieldname .= "NickName, ";
            $fieldname .= "DisplayName, ";
            $fieldname .= "Title, ";
            $fieldname .= "Gender, ";
            /*
             * $fieldname .= "IF(UNIX_TIMESTAMP(DateOfBirth)='0', '', DATE_FORMAT(DateOfBirth, '%Y-%m-%d')), ";
             */
            $fieldname .= "IF(DATE_FORMAT(DateOfBirth,'%Y')=0, '', DATE_FORMAT(DateOfBirth,'%Y-%m-%d')), ";
            
            $fieldname .= "HomeTelNo, ";
            $fieldname .= "OfficeTelNo, ";
            $fieldname .= "MobileTelNo, ";
            $fieldname .= "FaxNo, ";
            $fieldname .= "ICQNo, ";
            $fieldname .= "Address, ";
            $fieldname .= "Country, ";
            $fieldname .= "Info, ";
            $fieldname .= "Remark, ";
            $fieldname .= "ClassNumber, ";
            $fieldname .= "ClassName, ";
            $fieldname .= "ClassLevel, ";
            $fieldname .= "LastUsed, ";
            $fieldname .= "LastModifiedPwd, ";
            $fieldname .= "RecordType, ";
            $fieldname .= "RecordStatus, ";
            $fieldname .= "DateInput, ";
            $fieldname .= "DateModified,";
            $fieldname .= "URL";
            $fieldname .= ",EnglishName";
            $fieldname .= ",PhotoLink";
            $fieldname .= ",SessionKey";
            $fieldname .= ",Teaching";
            $fieldname .= ",CardID";
            $fieldname .= ",TitleChinese";
            $fieldname .= ",TitleEnglish";
            $fieldname .= ",WebSAMSRegNo";
            $fieldname .= ",HKID";
            $fieldname .= ",ImapUserEmail";
            $fieldname .= "," . getNameFieldByLang('INTRANET_USER.');
            if ($special_feature['ava_strn']) {
                $fieldname .= ",STRN";
            }
            $fieldname .= ", RFID"; // by kenneth chung on 2010-06-02
            $fieldname .= ", LastLang";
            $fieldname .= ", PersonalPhotoLink";
            $fieldname .= ", ModifyBy";
            $fieldname .= ", HKEdCityUserLogin";
            if ($sys_custom['StudentAccountAdditionalFields']) {
                $fieldname .= ",NonChineseSpeaking ";
                $fieldname .= ",SpecialEducationNeeds ";
                $fieldname .= ",PrimarySchool ";
                $fieldname .= ",University ";
                $fieldname .= ",Programme ";
            }
            if ($sys_custom['BIBA_AccountMgmtCust']) {
                $fieldname .= ",HouseholdRegister ";
            }
            $fieldname .= ",DeviceID ";
            $fieldname .= ",InputBy ";
            $fieldname .= ",DateInput ";
            
            $sql = "SELECT $fieldname 
                				FROM 
                					INTRANET_USER 
                				WHERE ";
            if ($UserID != "") {
                $sql .= "	UserID ='" . $UserID . "'";
            } elseif (is_array($UserIDsArray) && sizeof($UserIDsArray) > 0) {
                $sql .= " UserID IN ( " . implode(",", $UserIDsArray) . " ) ";
            } else { // by userlogin
                $sql .= " UserLogin ='" . $this->Get_Safe_SQL_Query($UserLogin) . "'";
            }
            
            return $this->returnArray($sql);
        }
        
        // #####################################################################################
        function display()
        {
            global $image_path;
            global $i_UserLogin, $i_UserEmail, $i_UserPassword, $i_UserFirstName, $i_UserLastName, $i_UserChineseName, $i_UserNickName, $i_UserDisplayName, $i_UserTitle, $i_UserGender, $i_UserDateOfBirth, $i_UserHomeTelNo, $i_UserOfficeTelNo, $i_UserMobileTelNo, $i_UserFaxNo, $i_UserICQNo, $i_UserAddress, $i_UserCountry, $i_UserInfo, $i_UserRemark, $i_UserClassNumber, $i_UserClassName, $i_UserClassLevel, $i_UserRecordStatus;
            global $i_title_mr, $i_title_miss, $i_title_mrs, $i_title_ms, $i_title_dr, $i_title_prof, $i_gender_male, $i_gender_female, $i_status_suspended, $i_status_approved, $i_status_pending;
            switch ($this->Title) {
                case 0:
                    $Title = $i_title_mr;
                    break;
                case 1:
                    $Title = $i_title_miss;
                    break;
                case 2:
                    $Title = $i_title_mrs;
                    break;
                case 3:
                    $Title = $i_title_ms;
                    break;
                case 4:
                    $Title = $i_title_dr;
                    break;
                case 5:
                    $Title = $i_title_prof;
                    break;
                default:
                    $Title = "";
                    break;
            }
            switch ($this->Gender) {
                case "M":
                    $Gender = $i_gender_male;
                    break;
                case "F":
                    $Gender = $i_gender_female;
                    break;
                default:
                    $Gender = "";
                    break;
            }
            switch ($this->RecordStatus) {
                case 0:
                    $RecordStatus = $i_status_suspended;
                    break;
                case 1:
                    $RecordStatus = $i_status_approved;
                    break;
                case 2:
                    $RecordStatus = $i_status_pending;
                    break;
                default:
                    $RecordStatus = "";
                    break;
            }
            $x = "<table width=500 border=0 cellpadding=2 cellspacing=1>\n";
            $x .= "<tr><td width=150>$i_UserLogin:</td><td>" . $this->UserLogin . "</td></tr>\n";
            $x .= "<tr><td>$i_UserEmail:</td><td>" . $this->convertAllLinks2($this->UserEmail) . "</td></tr>\n";
            // $x .= "<tr><td>$i_UserPassword:</td><td>".$this->UserPassword."</td></tr>\n";
            $x .= "<tr><td colspan=2><br></td></tr>\n";
            $x .= "<tr><td>$i_UserFirstName:</td><td>" . $this->FirstName . "</td></tr>\n";
            $x .= "<tr><td>$i_UserLastName:</td><td>" . $this->LastName . "</td></tr>\n";
            $x .= "<tr><td>$i_UserChineseName:</td><td>" . $this->ChineseName . "</td></tr>\n";
            $x .= "<tr><td>$i_UserNickName:</td><td>" . $this->NickName . "</td></tr>\n";
            $x .= "<tr><td>$i_UserDisplayName:</td><td>" . $this->DisplayName . "</td></tr>\n";
            $x .= "<tr><td>$i_UserTitle:</td><td>" . $Title . "</td></tr>\n";
            $x .= "<tr><td>$i_UserGender:</td><td>" . $Gender . "</td></tr>\n";
            $x .= "<tr><td>$i_UserDateOfBirth:</td><td>" . $this->DateOfBirth . " (yyyy-mm-dd)</td></tr>\n";
            $x .= "<tr><td>$i_UserHomeTelNo:</td><td>" . $this->HomeTelNo . "</td></tr>\n";
            $x .= "<tr><td>$i_UserOfficeTelNo:</td><td>" . $this->OfficeTelNo . "</td></tr>\n";
            $x .= "<tr><td>$i_UserMobileTelNo:</td><td>" . $this->MobileTelNo . "</td></tr>\n";
            $x .= "<tr><td>$i_UserFaxNo:</td><td>" . $this->FaxNo . "</td></tr>\n";
            $x .= "<tr><td>$i_UserICQNo:</td><td>" . $this->ICQNo . "</td></tr>\n";
            $x .= "<tr><td>$i_UserURL:</td><td>" . intranet_convertURLS($this->URL) . "</td></tr>\n";
            $x .= "<tr><td>$i_UserAddress:</td><td>" . $this->Address . "</td></tr>\n";
            $x .= "<tr><td>$i_UserCountry:</td><td>" . $this->Country . "</td></tr>\n";
            $x .= "<tr><td>$i_UserInfo:</td><td>" . nl2br($this->convertAllLinks2($this->Info)) . "</td></tr>\n";
            $x .= "<tr><td>$i_UserRemark:</td><td>" . nl2br($this->convertAllLinks2($this->Remark)) . "</td></tr>\n";
            $x .= "<tr><td>$i_UserClassNumber:</td><td>" . $this->ClassNumber . "</td></tr>\n";
            $x .= "<tr><td>$i_UserClassName:</td><td>" . $this->ClassName . "</td></tr>\n";
            $x .= "<tr><td>$i_UserClassLevel:</td><td>" . $this->ClassLevel . "</td></tr>\n";
            $x .= "<tr><td>$i_UserRecordStatus:</td><td>" . $RecordStatus . "</td></tr>\n";
            $x .= "</table>\n";
            return $x;
        }
        
        // #####################################################################################
        function returnGroups()
        {
            if (! isset($this->UserGroupList[$this->UserID])) {
                global $alumni_GroupID;
                
                if ($alumni_GroupID) {
                    $alumni_sql = " or a.GroupID=$alumni_GroupID";
                }
                $AcademicYearID = Get_Current_Academic_Year_ID();
                $sql = "
					SELECT 
						a.GroupID, 
						a.Title,
						a.TitleChinese
					FROM 
						INTRANET_GROUP AS a  
						INNER JOIN INTRANET_USERGROUP AS b ON a.GroupID = b.GroupID AND (a.GroupID<=4 OR a.AcademicYearID = '$AcademicYearID' $alumni_sql)
					WHERE 
						b.UserID = " . $this->UserID . " 
					ORDER BY 
						a.RecordType+0, a.Title
						";
                $result = $this->returnArray($sql, 2);
                
                $this->UserGroupList[$this->UserID] = $result;
            }
            
            return $this->UserGroupList[$this->UserID];
        }

        function returnGroupIDs()
        {
            // $AcademicYearID = Get_Current_Academic_Year_ID();
            // $sql = "
            // SELECT
            // a.GroupID
            // FROM
            // INTRANET_GROUP AS a
            // INNER JOIN INTRANET_USERGROUP AS b ON a.GroupID = b.GroupID AND (a.GroupID<=4 OR a.AcademicYearID = '$AcademicYearID')
            // WHERE
            // b.UserID = ".$this->UserID . "
            // ORDER BY
            // a.RecordType+0, a.Title
            // ";
            
            // return $this->returnVector($sql);
            $Groups = $this->returnGroups();
            $GroupIDs = Get_Array_By_Key($Groups, "GroupID");
            
            return $GroupIDs;
        }
        
        // modified by marcus 10/09/2009 - added parameter $ParAcademicYearID in order to get groups in specific Year
        function returnGroupsWithFunctionAdmin($ParAcademicYearID = '', $showDisplayIneCommOnly = 0)
        {
            /*
             * $sql = "SELECT
             * a.GroupID,
             * a.Title,
             * IF(a.FunctionAccess IS NULL,'ALL',REVERSE(BIN(a.FunctionAccess))),
             * b.RecordType,
             * a.RecordType,
             * a.PublicStatus
             * FROM
             * INTRANET_GROUP AS a,
             * INTRANET_USERGROUP AS b
             * WHERE
             * a.GroupID = b.GroupID
             * AND b.UserID = ".$this->UserID . "
             * AND a.GroupID = 27
             * ORDER BY
             * a.RecordType+0, a.Title
             * ";
             * $special = $this->returnArray($sql,6);
             *
             * $sql = "SELECT
             * a.GroupID,
             * a.Title,
             * IF(a.FunctionAccess IS NULL,'ALL',REVERSE(BIN(a.FunctionAccess))),
             * b.RecordType,
             * a.RecordType ,
             * a.PublicStatus
             * FROM
             * INTRANET_GROUP AS a,
             * INTRANET_USERGROUP AS b
             * WHERE
             * a.GroupID = b.GroupID
             * AND b.UserID = ".$this->UserID . "
             * AND a.GroupID != 27
             * ORDER BY
             * a.RecordType+0, a.Title
             * ";
             * $others = $this->returnArray($sql,6);
             * $result = array_merge($special,$others);
             * return $result;
             */
            global $intranet_session_language;
            
            // #### Related to AcademicYearID Groups ####
            if (! empty($ParAcademicYearID))
                $YearFilter = " AND a.AcademicYearID = '$ParAcademicYearID' ";
            if ($showDisplayIneCommOnly == 1)
                $cond_DisplayInCommunity = " AND a.DisplayInCommunity = '1' ";
            
            $identity_filter = " and a.GroupID>4 "; // not includes any identity groups
            
            $title_field = $intranet_session_language == "en" ? "a.Title" : "a.TitleChinese";
            $sql = "SELECT 
							distinct(a.GroupID), 
							" . $title_field . ", 
							IF(a.FunctionAccess IS NULL OR a.FunctionAccess = 0,'ALL',REVERSE(BIN(a.FunctionAccess))), 
							b.RecordType, 
							a.RecordType ,
							a.PublicStatus,
							a.DisplayInCommunity
						FROM 
							INTRANET_GROUP AS a, 
							INTRANET_USERGROUP AS b 
						WHERE 
							a.GroupID = b.GroupID 
							AND b.UserID = " . $this->UserID . " 
							$YearFilter
							$identity_filter
							$cond_DisplayInCommunity
						ORDER BY 
							a.RecordType+0, " . $title_field;
            
            $result1 = $this->returnArray($sql, 6);
            
            // #### Related to non-AcademicYearID Groups (identity groups) ####
            $identity_filter = " and a.GroupID<=4";
            $sql = "SELECT 
							distinct(a.GroupID), 
							" . $title_field . ", 
							IF(a.FunctionAccess IS NULL OR a.FunctionAccess = 0,'ALL',REVERSE(BIN(a.FunctionAccess))), 
							b.RecordType, 
							a.RecordType ,
							a.PublicStatus,
							a.DisplayInCommunity
						FROM 
							INTRANET_GROUP AS a, 
							INTRANET_USERGROUP AS b 
						WHERE 
							a.GroupID = b.GroupID 
							AND b.UserID = " . $this->UserID . " 
							$identity_filter
							$cond_DisplayInCommunity
						ORDER BY 
							a.RecordType+0, " . $title_field;
            $result2 = $this->returnArray($sql, 6);
            
            $result = array_merge((array) $result2, (array) $result1);
            return $result;
        }
        
        function approvedUserinEnrol($UserID){
            $sql = "SELECT * FROM INTRANET_ENROL_GROUPSTUDENT WHERE RecordStatus = 2 AND StudentID = ".$UserID;
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function groupRecordExist($UserID, $enrolGroupID){
            $sql = "SELECT * FROM INTRANET_USERGROUP AS g INNER JOIN INTRANET_ENROL_GROUPSTUDENT AS eg ON eg.EnrolGroupID= g.EnrolGroupID 
                WHERE eg.RecordStatus = 2 AND g.DateInput < eg.DateInput AND g.UserID = ".$UserID." AND g.GroupID = ".$enrolGroupID;
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function isDisplayinEComunity($groupId){
            $sql = "SELECT 
							DisplayInCommunity
						FROM 
							INTRANET_GROUP
						WHERE 
							GroupID = $groupId
							";
            $result = $this->returnArray($sql);
            return $result[0][DisplayInCommunity];
        }

        function returnPeriodSQL($Period, $field)
        {
            switch ($Period) {
                case 0:
                    $x = "";
                    break;
                case 1:
                    $x = " AND YEAR(now()) = YEAR($field) AND MONTH(now()) = MONTH($field)";
                    break;
                case 2:
                    $x = " AND YEAR(now()) = YEAR($field) AND WEEK(now()) = WEEK($field)";
                    break;
                case 3:
                    $x = " AND TO_DAYS(now()) = TO_DAYS($field)";
                    break;
                // Special request
                case 4:
                    $x = " AND $field >= CURDATE()";
                    break;
            }
            return $x;
        }

        function returnPeriodRangeSQL($Period, $start_field, $end_field)
        {
            switch ($Period) {
                case 0:
                    $x = "";
                    break;
                case 1:
                    $today = getdate();
                    $startstamp = mktime(0, 0, 0, $today['mon'], 1, $today['year']);
                    $start = date('Y-m-d', $startstamp);
                    $endstamp = mktime(0, 0, 0, $today['mon'] + 1, 1, $today['year']);
                    $end = date('Y-m-d', $endstamp);
                    $x = " AND $start_field < '$end' AND $end_field >= '$start'";
                    break;
                case 2:
                    $today = getdate();
                    $weekday = date('w');
                    $startstamp = mktime(0, 0, 0, $today['mon'], $today['mday'] - $weekday, $today['year']);
                    $start = date('Y-m-d', $startstamp);
                    $endstamp = mktime(0, 0, 0, $today['mon'], $today['mday'] - $weekday + 7, $today['year']);
                    $end = date('Y-m-d', $endstamp);
                    $x = " AND $start_field <= '$end' AND $end_field > '$start'";
                    break;
                case 3:
                    $x = " AND $start_field <= CURDATE() AND $end_field >= CURDATE()";
                    break;
                // Special request
                case 4:
                    $x = " AND $start_field <= CURDATE() AND $end_field >= CURDATE()";
                    break;
            }
            return $x;
        }

        function returnGroupInfo($Period, $InfoType, $GroupID)
        {
            $sql1 = "SELECT DISTINCT a.AnnouncementID, UNIX_TIMESTAMP(a.AnnouncementDate), 'A', a.Title, IF((LOCATE(';" . $this->UserID . ";',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0), Attachment
                                FROM INTRANET_ANNOUNCEMENT AS a, INTRANET_GROUPANNOUNCEMENT AS b
                                WHERE a.AnnouncementID = b.AnnouncementID AND a.RecordStatus = '1' AND b.GroupID = $GroupID
                                " . $this->returnPeriodRangeSQL($Period, "a.AnnouncementDate", "a.EndDate");
            $sql2 = "SELECT DISTINCT a.EventID, UNIX_TIMESTAMP(a.EventDate), 'E', a.Title, IF((LOCATE(';" . $this->UserID . ";',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0)
                                FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b
                                WHERE a.EventID = b.EventID  AND a.RecordStatus = '1' AND b.GroupID = $GroupID
                                " . $this->returnPeriodSQL($Period, "a.EventDate");
            if ($InfoType == 0) {
                $row = array_merge($this->returnArray($sql1, 6), $this->returnArray($sql2, 6));
            }
            if ($InfoType == 1) {
                $row = $this->returnArray($sql1, 6);
            }
            if ($InfoType == 2) {
                $row = $this->returnArray($sql2, 5);
            }
            return $row;
        }
        
        // #####################################################################################
        function displayGroupInfo($Period, $InfoType, $GroupID)
        {
            global $i_iconAttachment;
            $row = $this->returnGroupInfo($Period, $InfoType, $GroupID);
            $x .= "<table width=92% border=0 cellpadding=2 cellspacing=1 align=center>\n";
            if (sizeof($row) == 0) {
                global $i_no_record_exists_msg;
                $x .= "<tr><td>$i_no_record_exists_msg</td></tr>\n";
            } else {
                $j = 0;
                for ($i = 0; $i < sizeof($row); $i ++)
                    $datetime[$j ++] = $row[$i][1];
                arsort($datetime);
                while (list ($key, $value) = each($datetime)) {
                    $recID = $row[$key][0];
                    $recDate = date("Y-m-d", $row[$key][1]);
                    $recType = $row[$key][2];
                    $recTitle = intranet_wordwrap($row[$key][3], 30, "\n", 1);
                    $recReadFlag = $row[$key][4];
                    $recAttachment = $row[$key][5];
                    $clip = ($recAttachment == "" ? "" : " $i_iconAttachment");
                    $x .= "<tr>\n";
                    $x .= "<td width=18% align=left>$recDate</td>\n";
                    $x .= "<td width=10% align=right><img src=/images/" . (($recType == "A") ? "icon_announcement.gif" : "icon_event.gif") . " border=0></td>\n";
                    $x .= "<td width=72% align=left><a href=javascript:" . (($recType == "A") ? "fe_view_announcement($recID)" : "fe_view_event($recID)") . ">$recTitle</a>" . (($recReadFlag) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "") . " $clip</td>\n";
                    $x .= "</tr>\n";
                }
            }
            $x .= "</table>\n";
            return $x;
        }
        
        // #####################################################################################
        function IsGroup($GroupID)
        {
            $row = $this->returnGroups();
            for ($i = 0; $i < sizeof($row); $i ++) {
                if ($GroupID == $row[$i][0])
                    return 1;
            }
            return 0;
        }

        function UserName($withTitle = 1)
        {
            return $this->UserNameLang($withTitle);
        }

        /*
         * # This UserNameLang function will display:
         * # Mr / Ms => TitleEnglish/TitleChinese => (empty) = Teacher
         * function UserNameLang ($withTitle=1)
         * {
         * global $intranet_session_language,$i_general_Teacher;
         * global $lib_title_disabled, $UserID;
         *
         * if ($intranet_session_language == "en")
         * {
         * $name = $this->EnglishName;
         * $name = trim($name) ? $name : $this->ChineseName;
         * if ($lib_title_disabled)
         * {
         * return $name;
         * }
         *
         * if($withTitle)
         * {
         * if ($this->isTeacherStaff() || $this->isParent())
         * {
         * //$name = $this->getTitle()." $name";
         * if($this->isTeacherStaff())
         * {
         * $sql = "SELECT Teaching FROM INTRANET_USER WHERE UserID = $UserID";
         * $IsTeacher = $this->returnVector($sql);
         * if($IsTeacher[0] == 1)
         * {
         * if($this->TitleEnglish != "")
         * $name = $this->TitleEnglish." ".$name;
         * else
         * $name = $i_general_Teacher." ".$name;
         * }
         * else
         * {
         * $name = $this->getTitle()." $name";
         * }
         * }
         * else
         * {
         * $name = $this->getTitle()." $name";
         * }
         * }
         * }
         *
         * return $name;
         * }
         * else
         * {
         * $name = $this->ChineseName;
         * $name = trim($name) ? $name : $this->EnglishName;
         * if ($lib_title_disabled)
         * {
         * return $name;
         * }
         *
         * if ($this->isTeacherStaff() || $this->isParent())
         * {
         * //$name = "$name".$this->getTitle();
         * if($this->isTeacherStaff())
         * {
         * $sql = "SELECT Teaching FROM INTRANET_USER WHERE UserID = $UserID";
         * $IsTeacher = $this->returnVector($sql);
         * if($IsTeacher[0] == 1)
         * {
         * if($this->TitleChinese != "")
         * $name = $name.$this->TitleChinese;
         * else
         * $name = $name.$i_general_Teacher;
         * }
         * else
         * {
         * $name = $name.$this->getTitle();
         * }
         * }
         * else
         * {
         * $name = $name.$this->getTitle();
         * }
         * }
         * return $name;
         * }
         * }
         */
        
        // This UserNameLang function will display:
        // TitleEnglish/TitleChinese => (empty) = nothing
        function UserNameLang($withTitle = 1)
        {
            global $intranet_session_language;
            
            if ($intranet_session_language == "en") {
                $name = $this->EnglishName;
                $name = trim($name) ? $name : $this->ChineseName;
                $name = $this->TitleEnglish . " " . $name;
                return $name;
            } else {
                $name = $this->ChineseName;
                $name = trim($name) ? $name : $this->EnglishName;
                $name = $name . $this->TitleChinese;
                return $name;
            }
        }

        function UserNameClassNumber()
        {
            $class = "";
            if ($this->ClassNumber != "" && $this->ClassName != "") {
                $class = " (" . $this->ClassName . "-" . $this->ClassNumber . ")";
            }
            return $this->UserNameLang() . $class;
        }

        function NickNameClassNumber()
        {
            global $intranet_session_language;
            $class = "";
            if ($this->ClassNumber != "" && $this->ClassName != "") {
                $class = " (" . $this->ClassName . "-" . $this->ClassNumber . ")";
            }
            $nick = ($this->NickName == "" ? $this->UserNameLang() : $this->NickName);
            return $nick . $class;
        }
        
        // Modified by Simon on 21 Oct 2004
        // Return the name which is compatiable for chat room (in UTF8 encoding)
        // Space is replaced by underscore '_'
        function NickNameForChat()
        {
            $nickname = ($this->NickName == "") ? $this->EnglishName : $this->NickName;
            // $nickname = Big5ToUnicode($nickname);
            $nickname = str_replace(' ', '_', $nickname);
            
            return $nickname;
        }

        function getTitle()
        {
            global $i_title_mr, $i_title_miss, $i_title_mrs, $i_title_ms, $i_title_dr, $i_title_prof;
            global $intranet_session_langauge;
            if ($intranet_session_language == "en") {
                if ($this->TitleEnglish != '')
                    return $this->TitleEnglish;
            } else {
                if ($this->TitleChinese != '')
                    return $this->TitleChinese;
            }
            switch ($this->Title) {
                case 0:
                    $Title = $i_title_mr;
                    break;
                case 1:
                    $Title = $i_title_miss;
                    break;
                case 2:
                    $Title = $i_title_mrs;
                    break;
                case 3:
                    $Title = $i_title_ms;
                    break;
                case 4:
                    $Title = $i_title_dr;
                    break;
                case 5:
                    $Title = $i_title_prof;
                    break;
                default:
                    $Title = "";
                    break;
            }
            return $Title;
        }
        
        // Get Default Lang name
        function getNameForRecord()
        {
            global $lib_title_disabled;
            global $langID;
            $isEng = ($langID == 1);
            
            if ($isEng) {
                if ($this->EnglishName != '')
                    $name = $this->EnglishName;
                else
                    $name = $this->ChineseName;
            } else {
                if ($this->ChineseName != '')
                    $name = $this->ChineseName;
                else
                    $name = $this->EnglishName;
            }
            if ($this->ClassName != "") {
                $name .= " (" . $this->ClassName . "-" . $this->ClassNumber . ")";
            }
            if ($lib_title_disabled) {
                return $name;
            }
            if ($this->isTeacherStaff() || $this->isParent()) {
                $Title = '';
                if ($isEng) {
                    if ($this->TitleEnglish != '')
                        $Title = $this->TitleEnglish;
                } else {
                    if ($this->TitleChinese != '')
                        $Title = $this->TitleChinese;
                }
                if ($Title == '') {
                    switch ($this->Title) {
                        case 0:
                            $Title = ($isEng ? "Mr." : "..");
                            break;
                        case 1:
                            $Title = ($isEng ? "Miss" : "..");
                            break;
                        case 2:
                            $Title = ($isEng ? "Mrs." : "..");
                            break;
                        case 3:
                            $Title = ($isEng ? "Ms." : "..");
                            break;
                        case 4:
                            $Title = ($isEng ? "Dr." : "..");
                            break;
                        case 5:
                            $Title = ($isEng ? "Prof." : "..");
                            break;
                        default:
                            $Title = "";
                            break;
                    }
                }
                if ($isEng) {
                    return $Title . " " . $name;
                } else {
                    return $name . " " . $Title;
                }
            } else {
                return $name;
            }
        }

        function isEmailExists($target_email)
        {
            $sql = "SELECT COUNT(UserID) FROM INTRANET_USER WHERE UserEmail = '$target_email'";
            $result = $this->returnVector($sql);
            return ($result[0] == 0 ? false : true);
        }

        function returnUsersType($type, $condition = "")
        {
            $name = getNameFieldWithLoginByLang();
            $sql = "SELECT UserID, $name as UserName, ChineseName, EnglishName, UserLogin, RecordType, RecordStatus FROM INTRANET_USER WHERE RecordType = $type AND RecordStatus = 1 $condition ORDER BY EnglishName, ChineseName";
            return $this->returnArray($sql, 2);
        }

        function returnUsersType2($type, $teaching = "", $userIdAry = '', $excludeUserIdAry = '')
        {
            if ($teaching == - 1) {
                $conds = "";
            } else 
                if ($teaching == 1)
                    $conds = " AND Teaching=1";
                else 
                    if ($teaching == 0)
                        $conds = " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
            
            if ($userIdAry !== '') {
                $conds_userId = " And UserID In ('" . implode("','", (array) $userIdAry) . "') ";
            }
            
            if ($excludeUserIdAry !== '') {
                $conds_excludeUserId = " And UserID Not In ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            
            $name = getNameFieldWithLoginByLang2();
            $sql = "SELECT UserID, $name, UserLogin FROM INTRANET_USER WHERE RecordType = $type AND RecordStatus = 1 $conds $conds_userId $conds_excludeUserId ORDER BY EnglishName, ChineseName";
            return $this->returnArray($sql, 2);
        }

        function getSelectUsersType($type, $tag, $selected)
        {
            $result = $this->returnUsersType($type);
            return getSelectByArray($result, $tag, $selected);
        }

        function getSelectTeacherStaff($tag, $selected = "")
        {
            return $this->getSelectUsersType(1, $tag, $selected);
        }

        function getSelectStudent($tag, $selected = "")
        {
            return $this->getSelectUsersType(2, $tag, $selected);
        }

        function returnGroupsNewAnnounce($groupList = "")
        {
            $uid = $this->UserID;
            if ($groupList != "") {
                $conds = "AND a.GroupID IN ($groupList)";
            }
            $sql = "SELECT a.GroupID, COUNT(DISTINCT b.AnnouncementID)
                         FROM INTRANET_GROUPANNOUNCEMENT as b LEFT OUTER JOIN INTRANET_GROUP as a ON a.GroupID = b.GroupID
                              LEFT OUTER JOIN INTRANET_ANNOUNCEMENT as c ON b.AnnouncementID = c.AnnouncementID
                         WHERE c.EndDate IS NULL OR (CURDATE()>=c.AnnouncementDate AND CURDATE() <= c.EndDate) $conds
                         AND (c.ReadFlag IS NULL OR locate(';$uid;', c.ReadFlag)=0)
                         GROUP BY a.GroupID";
            $result = $this->returnArray($sql, 2);
            // echo "1. $sql\n";
            return build_assoc_array($result);
        }

        function returnGroupsNewBulletin($groupList = "")
        {
            if ($groupList != "") {
                $conds = "AND GroupID IN ($groupList)";
            }
            $uid = $this->UserID;
            $sql = "SELECT GroupID, COUNT(BulletinID) FROM INTRANET_BULLETIN
                          WHERE locate(';$uid;',ReadFlag)=0 $conds
                          GROUP BY GroupID";
            // echo "2. $sql\n";
            $result = $this->returnArray($sql, 2);
            return build_assoc_array($result);
        }

        function returnGroupsNewLink($groupList = "")
        {
            if ($groupList != "") {
                $conds = "AND GroupID IN ($groupList)";
            }
            $uid = $this->UserID;
            $sql = "SELECT GroupID, COUNT(LinkID) FROM INTRANET_LINK
                         WHERE locate(';$uid;',ReadFlag)=0 $conds
                         GROUP BY GroupID";
            // echo "3. $sql\n";
            $result = $this->returnArray($sql, 2);
            return build_assoc_array($result);
        }

        function returnGroupsNewFile($groupList = "")
        {
            if ($groupList != "") {
                $conds = "AND GroupID IN ($groupList)";
            }
            $conds .= " AND Approved = 1";
            $uid = $this->UserID;
            $sql = "SELECT GroupID, COUNT(FileID) FROM INTRANET_FILE
                         WHERE locate(';$uid;',ReadFlag)=0 $conds
                         GROUP BY GroupID";
            // echo "4. $sql\n";
            $result = $this->returnArray($sql, 2);
            return build_assoc_array($result);
        }
        // /home/school/index.php
        function displayGroupPage()
        {
            global $image_path;
            global $i_frontpage_schoolinfo_groupinfo_group_timetable, $i_frontpage_schoolinfo_groupinfo_group_chat, $i_frontpage_schoolinfo_groupinfo_group_bulletin, $i_frontpage_schoolinfo_groupinfo_group_links, $i_frontpage_schoolinfo_groupinfo_group_files, $i_adminmenu_plugin_qb, $i_frontpage_schoolinfo_groupinfo_group_settings;
            $groups = $this->returnGroupsWithFunctionAdmin();
            // print_r($groups);
            $groupList = "";
            $delimiter = "";
            for ($i = 0; $i < sizeof($groups); $i ++) {
                $groupList .= $delimiter . $groups[$i][0];
                $delimiter = ",";
            }
            $available = returnGroupAvailableFunctions();
            $announce = $this->returnGroupsNewAnnounce($groupList);
            $bulletin = $this->returnGroupsNewBulletin($groupList);
            $links = $this->returnGroupsNewLink($groupList);
            $files = $this->returnGroupsNewFile($groupList);
            
            $x = "<table width=\"550\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\">\n";
            $x .= "<tr>
          <td width=\"15\" class=groupboard_left><img src=\"$image_path/groupinfo/board_topleft.gif\" width=\"15\" height=\"15\"><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
          <td width=\"252\" class=groupboard_top><img src=\"$image_path/spacer.gif\" width=\"3\" height=\"15\"></td>
          <td width=\"1\" class=groupboard_top><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"15\"></td>
          <td width=\"263\" class=groupboard_top><img src=\"$image_path/spacer.gif\" width=\"3\" height=\"15\"></td>
          <td width=\"19\" class=groupboard_right><img src=\"$image_path/groupinfo/board_topright.gif\" width=\"19\" height=\"15\"></td>
        </tr>\n";
            for ($i = 0; $i < sizeof($groups); $i ++) {
                list ($id, $name, $function, $admin, $type) = $groups[$i];
                $grouplink = "<a href=javascript:openGroupPage($id)>$name</a>";
                $countBull = $bulletin[$id];
                $countLink = $links[$id];
                $countFile = $files[$id];
                $countBull = ($countBull == "" || $countBull == 0 ? "" : "($countBull)");
                $countLink = ($countLink == "" || $countLink == 0 ? "" : "($countLink)");
                $countFile = ($countFile == "" || $countFile == 0 ? "" : "($countFile)");
                // Field : FunctionAccess
                // - Bit-1 (LSB) : timetable
                // - Bit-2 : chat
                // - Bit-3 : bulletin
                // - Bit-4 : shared links
                // - Bit-5 : shared files
                // - Bit-6 : question bank
                // - Bit-7 : Photo Album
                // - Bit-8 (MSB) : Survey
                if ($function == "ALL") {
                    $isTimetable = ($available[0] != "");
                    $isChat = ($available[1] != "");
                    $isBulletin = ($available[2] != "");
                    $isLink = ($available[3] != "");
                    $isFile = ($available[4] != "");
                    $isQB = $type == 2 && ($available[5] != "");
                    $isPhoto = ($available[6] != "");
                    $isSurvey = ($available[7] != "");
                } else {
                    $isTimetable = (substr($function, 0, 1) == '1' && $available[0] != "");
                    $isChat = (substr($function, 1, 1) == '1' && $available[1] != "");
                    $isBulletin = (substr($function, 2, 1) == '1' && $available[2] != "");
                    $isLink = (substr($function, 3, 1) == '1' && $available[3] != "");
                    $isFile = (substr($function, 4, 1) == '1' && $available[4] != "");
                    $isQB = $type == 2 && (substr($function, 5, 1) == '1' && $available[5] != "");
                    $isPhoto = (substr($function, 6, 1) == '1' && $available[6] != "");
                    $isSurvey = (substr($function, 7, 1) == '1' && $available[7] != "");
                }
                $functionIcon = "";
                if ($isTimetable)
                    $functionIcon .= "<a href=javascript:fe_view_timetable($id)><img src=/images/icon_timetable.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_timetable'></a>\n";
                if ($isChat)
                    $functionIcon .= "<a href=javascript:fe_view_chat($id)><img src=/images/icon_chatroom.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_chat'></a>\n";
                if ($isBulletin)
                    $functionIcon .= "<a href=javascript:fe_view_bulletin($id)><img src=/images/icon_bulletin.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'>$countBull</a>\n";
                if ($isLink)
                    $functionIcon .= "<a href=javascript:fe_view_links($id)><img src=/images/icon_sharedlinks.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_links'>$countLink</a>\n";
                if ($isFile)
                    $functionIcon .= "<a href=javascript:fe_view_files($id)><img src=/images/icon_sharedfiles.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_files'>$countFile</a>\n";
                if ($isQB)
                    $functionIcon .= "<a href=javascript:fe_view_qb($id)><img src=/images/icon_schoolbasedqb.gif border=0 alt='$i_adminmenu_plugin_qb'></a>\n";
                if ($isPhoto)
                    $functionIcon .= ""; // Not yet implemented
                if ($isSurvey)
                    $functionIcon .= ""; // Not yet implemented
                if ($admin == "A")
                    $functionIcon .= "<a href=javascript:fe_view_settings($id)><img src=/images/icon_groupsetting.gif border=0 alt='$i_frontpage_schoolinfo_groupinfo_group_settings'></a>\n";
                
                $x .= "<tr>
          <td class=groupboard_left>&nbsp;</td>
          <td bgcolor=\"#FBFCE5\">$grouplink</td>
          <td bgcolor=\"#CCAD00\"><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
          <td bgcolor=\"#FBFCE5\">$functionIcon</td>
          <td class=groupboard_right>&nbsp;</td>
        </tr>\n";
                if ($i != sizeof($groups) - 1)
                    $x .= "<tr>
          <td class=groupboard_left><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
          <td bgcolor=\"#CCAD00\"><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
          <td bgcolor=\"#CCAD00\"><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
          <td bgcolor=\"#CCAD00\"><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
          <td class=groupboard_right><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"1\"></td>
        </tr>\n";
                // $x .= "<tr><td>$grouplink</td><td>$functionIcon</td></tr>\n";
                // $x .= "<tr><td align=center><hr width='95%'></td></tr>\n";
            }
            $x .= "<tr>
          <td><img src=\"$image_path/groupinfo/board_bottomleft.gif\" width=\"15\" height=\"19\"></td>
          <td class=groupboard_bottom>&nbsp;</td>
          <td class=groupboard_bottom><img src=\"$image_path/spacer.gif\" width=\"1\" height=\"19\"></td>
          <td class=groupboard_bottom>&nbsp;</td>
          <td><img src=\"$image_path/groupinfo/board_bottomright.gif\" width=\"19\" height=\"19\"></td>
        </tr>\n";
            $x .= "</table>\n";
            return $x;
        }
        
        // function getNameWithClassNumber($id)
        // {
        // $name_field = getNameFieldWithClassNumberByLang("");
        // $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = $id";
        // $result = $this->returnVector($sql);
        // return $result[0];
        // }
        function getNameWithClassNumber($id, $IncludeArcivedUser = 0)
        {
            $name_field = getNameFieldWithClassNumberByLang("");
            $sql = "SELECT $name_field as NameField, UserID FROM INTRANET_USER WHERE UserID IN (" . implode(',', (array) $id) . ")";
            $result = $this->returnArray($sql);
            
            if ($IncludeArcivedUser == 1 && sizeof($result) == 0) {
                $name_field = Get_Lang_Selection("ChineseName", "EnglishName");
                $sql = "SELECT $name_field as NameField, UserID FROM INTRANET_ARCHIVE_USER WHERE UserID IN (" . implode(',', (array) $id) . ")";
                $result = $this->returnArray($sql);
            }
            
            if (! is_array($id))
                return $result[0][0];
            else
                return BuildMultiKeyAssoc($result, "UserID", "NameField", 1);
        }

        function getChildren($id = "")
        {
            if ($id == "")
                $parentID = $this->UserID;
            
            if ($parentID == "")
                return array();
            $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$parentID'";
            return $this->returnVector($sql);
        }

        function getParent($StudentIDStr = "", $sortByClass = 0, $filterAppParent = 0, $UserRecordStatus = 0, $getAllParent = 0)
        {
            global $Lang, $plugin;
            
            if ($getAllParent == 0) {
                if ($StudentIDStr == "")
                    return array();
                $selectStudentCond = "AND a.StudentID in  ($StudentIDStr)";
            }
            
            $name_field = getNameFieldWithClassNumberByLang("b.");
            $name_field2 = getNameFieldByLang("c.");
            
            if ($filterAppParent) {
                // $join_api_log = " INNER JOIN INTRANET_API_REQUEST_LOG as r ON (r.UserID = a.ParentID OR r.UserID = a.StudentID) ";
                if ($plugin['eClassApp']) {
                    $logTable = 'APP_LOGIN_LOG';
                } else {
                    $logTable = 'INTRANET_API_REQUEST_LOG';
                }
                $join_api_log = " INNER JOIN $logTable as r ON (r.UserID = a.ParentID) ";
            }
            
            if ($UserRecordStatus) {
                $hideSuspendedCond = "AND b.RecordStatus = 1";
                $hideIfChildSuspendedCond = "AND c.RecordStatus = 1";
            }
            
            $sql = "
	                 SELECT distinct
						 (a.ParentID),
	                	 $name_field AS OutputName, 
						 $name_field2 AS StudentName,
						 c.ClassName, c.ClassNumber
	                 FROM 
	                 	INTRANET_PARENTRELATION as a
	                 	inner join INTRANET_USER as b on (b.UserID=a.ParentID)
						inner join INTRANET_USER as c on (c.UserID=a.StudentID) 
						$join_api_log
	                 WHERE 1
	                 	$selectStudentCond
						$hideSuspendedCond
						$hideIfChildSuspendedCond
					
                 ";
            if ($sortByClass == 1)
                $sql .= " ORDER BY c.ClassName, c.ClassNumber";
            else
                $sql .= " ORDER BY StudentName, OutputName";
                // echo $sql;
            
            return $this->returnArray($sql);
        }

        function returnParentList($sid)
        {
            $temp_ary = $this->getParent($sid);
            $parent_ary = array();
            if (! empty($temp_ary)) {
                foreach ($temp_ary as $k => $d) {
                    $parent_ary[] = $d['OutputName'];
                }
            }
            return implode(", ", $parent_ary);
        }

        function getChildrenList($id = "", $combineNameAndClassNumer = true)
        {
            $parentID = $id ? $id : $this->UserID;
            
            if ($parentID == "")
                return array();
            
            $namefield = ($combineNameAndClassNumer) ? getNameFieldWithClassNumberByLang("b.") : getNameFieldByLang("b.");
            $sql = "SELECT a.StudentID, $namefield as StudentName, b.ClassName, b.ClassNumber
                     FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
                     WHERE a.StudentID = b.UserID AND a.ParentID = '$parentID'";
            return $this->returnArray($sql, 4);
        }

        function getActiveChildrenList($id = "")
        {
            if ($id == "")
                $parentID = $this->UserID;
            if ($parentID == "")
                return array();
            
            $namefield = getNameFieldWithClassNumberByLang("b.");
            $sql = "SELECT a.StudentID, $namefield
                        FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
                        WHERE a.StudentID = b.UserID AND a.ParentID = $parentID AND b.ClassName IS NOT NULL AND b.ClassName!=''";
            
            return $this->returnArray($sql, 2);
        }
        
        // for archive student
        function ArchiveUserNameClassNumber($StudentID)
        {
            global $intranet_session_language;
            
            if ($intranet_session_language == "en")
                $sql = "SELECT EnglishName ";
            else 
                if ($intranet_session_language == "b5")
                    $sql = "SELECT ChineseName ";
            
            $sql .= " , ClassName, ClassNumber,UserLogin FROM INTRANET_ARCHIVE_USER WHERE UserID=$StudentID";
            $result = $this->returnArray($sql, 4);
            
            return ($result[0][0] . " " . " (" . $result[0][3] . " , " . $result[0][1] . "-" . $result[0][2] . ")");
        }

        function ReturnArchiveStudentInfo($StudentID)
        {
            global $intranet_session_language;
            
            if ($intranet_session_language == "en")
                $sql = "SELECT EnglishName ";
            else 
                if ($intranet_session_language == "b5")
                    $sql = "SELECT ChineseName ";
            
            $sql .= " , UserLogin, ClassName, ClassNumber, YearOfLeft FROM INTRANET_ARCHIVE_USER WHERE UserID=$StudentID";
            $result = $this->returnArray($sql, 5);
            return ($result[0]);
        }
        
        function ReturnArchiveUserCreateModifyInfo($UserID)
        {
            $sql = "SELECT ArchivedBy, DateArchive FROM INTRANET_ARCHIVE_USER WHERE UserID=$UserID";
            $result = $this->returnArray($sql);
            return ($result[0]);
        }
        // end of archive student part
        
        // Remove CampusMail and relative attachments for a list of users
        // Param : $list : list of UserID (comma separated, to put in SQL stmt)
        function removeUsersMail($list)
        {}
        
        // Remove and archive information of common users
        // Param : $list : list of UserID (comma separated, to put in SQL stmt)
        // Needs to include libclubsenrol.php, libwebmail.php, libftp.php, libldap.php, libfilesystem.php
        // Modified by key [2008-12-11]: update the sql query due to sql error
        function prepareUserRemoval($list)
        {
            global $file_path, $eclass_db, $eclass_prefix, $intranet_db, $intranet_root;
            global $intranet_authentication_method, $plugin, $SYS_CONFIG, $sys_custom;
            $namefield = getNameFieldForRecord2("b.");
            
            if ($plugin['radius_server']) {
                global $intranet_db_user, $intranet_db_pass, $radius_db_host, $radius_db_name, $radius_db_username, $radius_db_password;
                
                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN ($list)";
                $userlogin_list = $this->returnVector($sql);
                if (count($userlogin_list) > 0) {
                    intranet_closedb(); // close the intranet db
                    $this->opendb($radius_db_host, $radius_db_username, $radius_db_password, $radius_db_name); // open the radius server db
                    
                    $sql = "DELETE FROM " . $radius_db_name . ".radcheck WHERE username IN ('" . implode("','", $userlogin_list) . "')";
                    $this->db_db_query($sql);
                    $sql = "DELETE FROM " . $radius_db_name . ".radusergroup WHERE username IN ('" . implode("','", $userlogin_list) . "')";
                    $this->db_db_query($sql);
                    unset($userlogin_list);
                    
                    $this->closedb(); // close the radius server db
                    intranet_opendb(); // reopen the intranet db
                    $this->db = $intranet_db;
                }
            }
            
            if ($sys_custom['DHL']) {
                include_once ($intranet_root . "/includes/DHL/libdhl.php");
                
                $libdhl = new libdhl();
                $libdhl->deleteDepartmentUserRecord(array(), explode(",", $list));
            }
            
            // Archive Booking Records
            $sql = "INSERT INTO INTRANET_BOOKING_ARCHIVE
                         (BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
                         SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
                         $namefield,
                         d.ResourceCategory, d.Title, d.ResourceCode,
                         a.RecordStatus, a.TimeApplied, a.LastAction
                         FROM INTRANET_BOOKING_RECORD as a 
                         LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         LEFT OUTER JOIN INTRANET_RESOURCE as d ON a.ResourceID = d.ResourceID
                         LEFT OUTER JOIN INTRANET_SLOT as c ON c.BatchID = d.TimeSlotBatchID AND a.TimeSlot = c.SlotSeq
                         WHERE a.UserID IN ($list)";
            $this->db_db_query($sql);
            
            $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Archive Login Sessions
            /*
             * $sql = "INSERT IGNORE INTO INTRANET_ARCHIVE_LOGIN_SESSION
             * (UserName,ClassName,ClassNumber,LoginID,TimeIn,TimeOut,ClientHost)
             * SELECT $namefield, b.ClassName,b.ClassNumber,b.UserLogin,a.StartTime,a.DateModified
             * FROM INTRANET_LOGIN_SESSION as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
             * WHERE a.UserID IN ($list)";
             */
            $sql = "INSERT IGNORE INTO INTRANET_ARCHIVE_LOGIN_SESSION
                         (UserName,ClassName,ClassNumber,LoginID,TimeIn,TimeOut)
                         SELECT $namefield, b.ClassName,b.ClassNumber,b.UserLogin,a.StartTime,a.DateModified
                         FROM INTRANET_LOGIN_SESSION as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.UserID IN ($list)";
            
            $this->db_db_query($sql);
            
            $sql = "DELETE FROM INTRANET_LOGIN_SESSION WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove QB
            $sql = "DELETE FROM QB_USER_SCORE WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Parent Relation
            $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID IN ($list) OR StudentID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Enrolment records
            $sql = "SELECT DISTINCT GroupID FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID IN ($list)";
            $groups = $this->returnVector($sql);
            
            // Enrol_group student
            $sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT WHERE StudentID IN ($list)";
            $this->db_db_query($sql);
            
            // enrol student
            $sql = "DELETE FROM INTRANET_ENROL_STUDENT WHERE StudentID IN ($list)";
            $this->db_db_query($sql);
            // Fix approved count
            if (sizeof($groups) != 0) {
                $lc = new libclubsenrol();
                for ($i = 0; $i < sizeof($groups); $i ++) {
                    $lc->synApprovedCount($groups[$i]);
                }
            }
            
            // Remove Periodic Booking
            $sql = "DELETE FROM INTRANET_PERIODIC_BOOKING WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove groups
            $sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Activity
            /*
             * commented by Henry Chow on 20110801 (allow to view enrolment record in previous years)
             * $sql = "DELETE FROM INTRANET_ENROL_GROUPSTAFF WHERE UserID IN ($list)";
             * $this->db_db_query($sql);
             *
             * $sql = "DELETE FROM INTRANET_ENROL_EVENTSTAFF WHERE UserID IN ($list)";
             * $this->db_db_query($sql);
             */
            $sql = "DELETE FROM INTRANET_ENROL_EVENTSTUDENT WHERE StudentID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Admin User
            $sql = "DELETE FROM INTRANET_ADMIN_USER WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Class Teacher
            $sql = "DELETE FROM INTRANET_CLASSTEACHER WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove System Access
            $sql = "DELETE FROM INTRANET_SYSTEM_ACCESS WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Subject teacher
            $sql = "DELETE FROM INTRANET_SUBJECT_TEACHER WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Subject leader
            $sql = "DELETE FROM INTRANET_SUBJECT_LEADER WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Disciplinev12 Detention (DISCIPLINE_DETENTION_STUDENT_SESSION) - since foreign key is set
            $sql = "DELETE FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE StudentID IN ($list)";
            $this->db_db_query($sql);
            
            // Role Admin Group Member which has constraint with INTRANET_USER.UserID
            $sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove eDiscipline detention pic which has constraint with INTRANET_USER.UserID
            $sql = "DELETE FROM DISCIPLINE_DETENTION_SESSION_PIC WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            // Remove Campusmail
            /*
             * old method
             * $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($list)";
             * $campusmail_ids = $this->returnVector($sql);
             * if (sizeof($campusmail_ids)!=0)
             * {
             * $sql = "SELECT DISTINCT Attachment FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($list)";
             * $attachments = $this->returnVector($sql);
             * $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($list)";
             * $this->db_db_query($sql);
             * $campusmail_list = implode(",",$campusmail_ids);
             * $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN ($campusmail_list)";
             * $this->db_db_query($sql);
             * if (sizeof($attachments)!=0)
             * {
             * $attachment_list = "'".implode(",",$attachments)."'";
             * $sql = "SELECT DISTINCT Attachment FROM INTRANET_CAMPUSMAIL WHERE Attachment IN ($attachment_list)";
             * $left_attachments = $this->returnVector($sql);
             * $lf = new libfilesystem();
             * for ($i=0; $i<sizeof($attachments); $i++)
             * {
             * $target = $attachments[$i];
             * if (!in_array($target,$left_attachments) && $target != "")
             * {
             * $path = "$file_path/file/mail/$target";
             * $lf->lfs_remove($path);
             * }
             * }
             * }
             * }
             */
            
            /* New method */
            // Step 1: Extract valid mail IDs
            $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($list)";
            $targetID = $this->returnVector($sql);
            
            if (sizeof($targetID) != 0) {
                $CampusMailIDStr = implode(",", $targetID);
                
                // Step 2: Extract Attachment paths
                $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 1 AND IsAttachment = 1 AND Attachment != '' AND CampusMailID IN ($CampusMailIDStr)";
                $to_remove_attachmentpaths_int = $this->returnVector($sql);
                $sql = "SELECT Attachment FROM INTRANET_CAMPUSMAIL WHERE MailType = 2 AND IsAttachment = 1 AND Attachment != '' AND CampusMailID IN ($CampusMailIDStr)";
                $to_remove_attachmentpaths_ext = $this->returnVector($sql);
                
                // Step 3: Remove reply records
                // Remove the replies for notification mail sent
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID IN (" . $CampusMailIDStr . ")";
                $this->db_db_query($sql);
                
                // Step 4: Remove Mail DB records
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL WHERE CampusMailID IN ($CampusMailIDStr)";
                $this->db_db_query($sql);
                
                // Step 5: Update Notification
                $sql = "UPDATE INTRANET_CAMPUSMAIL SET Notification = 0 WHERE CampusMailFromID IN ($CampusMailIDStr)";
                $this->db_db_query($sql);
                
                // Step 6: Search for attachment path (Internal)
                $lf = new libfilesystem();
                unset($list_path_string_to_remove);
                $delim = "";
                for ($i = 0; $i < sizeof($to_remove_attachmentpaths_int); $i ++) {
                    $target_path = $to_remove_attachmentpaths_int[$i];
                    if ($target_path != "") {
                        $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE Attachment = '$target_path'";
                        $temp = $this->returnVector($sql);
                        if (is_array($temp) && sizeof($temp) == 1 && $temp[0] == 0) {
                            // Remove Database records
                            $list_path_string_to_remove .= "$delim'$target_path'";
                            $delim = ",";
                            
                            // Remove actual files
                            $full_path = "$file_path/file/mail/" . $target_path;
                            global $bug_tracing;
                            if ($bug_tracing['imail_remove_attachment']) {
                                $command = OsCommandSafe("mv $full_path " . $full_path . "_bak");
                                exec($command);
                            } else {
                                $lf->lfs_remove($full_path);
                            }
                        }
                    }
                }
                
                // Step 7: Remove Attachment path (External Mail)
                for ($i = 0; $i < sizeof($to_remove_attachmentpaths_ext); $i ++) {
                    $target_path = $to_remove_attachmentpaths_ext;
                    if ($target_path != "") {
                        // Remove Database records
                        $list_path_string_to_remove .= "$delim'$target_path'";
                        $delim = ",";
                        
                        // Remove actual files
                        $full_path = "$file_path/file/mail/" . $target_path;
                        if ($bug_tracing['imail_remove_attachment']) {
                            $command = OsCommandSafe("mv $full_path " . $full_path . "_bak");
                            exec($command);
                        } else {
                            $lf->lfs_remove($full_path);
                        }
                    }
                }
                
                // Remove Database Records
                if ($list_path_string_to_remove != "") {
                    $sql = "DELETE FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE AttachmentPath IN ($list_path_string_to_remove)";
                    $this->db_db_query($sql);
                }
                
                // Remove internet email raw message source
                $sql = "DELETE FROM INTRANET_RAWCAMPUSMAIL WHERE UserID IN ($list)";
                $this->db_db_query($sql);
                
                // Remove user folders
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID IN ($list) AND RecordType=1";
                $this->db_db_query($sql);
                
                // Remove used quota records
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_USED_STORAGE WHERE UserID IN ($list)";
                $this->db_db_query($sql);
                
                // Remove total quota records
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_USERQUOTA WHERE UserID IN ($list)";
                $this->db_db_query($sql);
                
                // Remove new mail counting
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID IN ($list)";
                $this->db_db_query($sql);
            }
            
            // Remove Payment accounts
            global $plugin;
            if ($plugin['payment']) {
                $sql = "DELETE FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
                $this->db_db_query($sql);
            }
            
            // Remove iMail Quota
            global $special_feature;
            if ($special_feature['imail']) {
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID IN ($list)";
                $this->db_db_query($sql);
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL WHERE OwnerID IN ($list)";
                $this->db_db_query($sql);
                $sql = "DELETE FROM INTRANET_CAMPUSMAIL_USERQUOTA WHERE UserID IN ($list)";
                $this->db_db_query($sql);
            }
            
            // Remove Role Membership
            $sql = "Delete From ROLE_MEMBER where UserID in ($list)";
            $this->db_db_query($sql);
            
            // Grab Logins and Emails
            // extract email list
            $sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN ($list)";
            $result = $this->returnVector($sql);
            if (sizeof($result) != 0) {
                $emailList = "";
                $delimiter = "";
                for ($i = 0; $i < sizeof($result); $i ++) {
                    $emailList .= "$delimiter'" . $result[$i] . "'";
                    $delimiter = ",";
                }
                
                // extract login list
                $sql = "SELECT UserLogin, UserID " . ($sys_custom['iMailPlus']['EmailAliasName'] ? ",ImapUserLogin" : "") . " FROM INTRANET_USER WHERE UserID IN ($list)";
                $logins = $this->returnArray($sql, 2);
                
                // Remove system A/Cs (LDAP, email, FTP)
                $lwebmail = new libwebmail();
                $lftp = new libftp();
                if ($intranet_authentication_method == 'LDAP') {
                    // Remove LDAP accounts
                    $lldap = new libldap();
                    if ($lldap->isAccountControlNeeded()) {
                        $lldap->connect();
                        for ($i = 0; $i < sizeof($logins); $i ++) {
                            $lldap->removeAccount($logins[$i]['UserLogin']);
                        }
                    }
                }
                for ($i = 0; $i < sizeof($logins); $i ++) {
                    // Remove Email accounts
                    if ($plugin['imail_gamma']) {
                        // Remove Gamma Mail Account
                        global $intranet_root;
                        include_once ("imap_gamma.php");
                        include_once ("libaccountmgmt.php");
                        $IMap = new imap_gamma(true);
                        $laccount = new libaccountmgmt();
                        if ($sys_custom['iMailPlus']['EmailAliasName'] && trim($logins[$i]['ImapUserLogin']) != "") {
                            $IMapUserEmail = strtolower($logins[$i]['ImapUserLogin']) . "@" . $SYS_CONFIG['Mail']['UserNameSubfix'];
                        } else {
                            $IMapUserEmail = strtolower($logins[$i]['UserLogin']) . "@" . $SYS_CONFIG['Mail']['UserNameSubfix'];
                        }
                        if ($IMap->delete_account($IMapUserEmail)) {
                            $laccount->setIMapUserEmail($logins[$i]['UserID'], "");
                            // $IMap->setIMapUserEmailQuota($logins[$i]['UserID'],0);
                            $IMap->removeQuotaAndSharedMailBoxMember($logins[$i]['UserID']);
                        }
                    } else {
                        // Remove webmail account
                        if ($lwebmail->has_webmail)
                            $lwebmail->delete_account($logins[$i]['UserLogin']);
                    }
                    
                    // Remove FTP accounts
                    if ($lftp->isFTP)
                        $lftp->removeAccount($logins[$i]['UserLogin']);
                }
                // Remove eClass
                include_once ($intranet_root . "/includes/liblog.php");
                $lg = new liblog();
                $this->db = $eclass_db;
                $sql = "SELECT DISTINCT course_id FROM user_course WHERE user_email IN ($emailList)";
                $target_courses = $this->returnVector($sql);
                for ($i = 0; $i < sizeof($target_courses); $i ++) {
                    $this->db = $eclass_prefix . "c" . $target_courses[$i];
                    
                    // Eric Yip(20090817): Check if db exists
                    $db_selected = mysql_select_db($this->db);
                    if ($db_selected) {
                        $sql = "SELECT user_id FROM usermaster WHERE user_email IN ($emailList)";
                        $user_ids = $this->returnVector($sql);
                        if (count($user_ids) > 0) {
                            $RecordDetailAry = array();
                            $RecordDetailAry['CourseID'] = $target_courses[$i];
                            $RecordDetailAry['DeletedUserID'] = implode(",", $user_ids);
                            $RecordDetailAry['SelectedEmail'] = $emailList;
                            $RecordDetailAry['UrlRequestFrom'] = $_SERVER["REQUEST_URI"];
                            
                            $lg->INSERT_LOG('Account Management', 'Student Account', $RecordDetailAry, 'user_course', "");
                            
                            $sql = "UPDATE usermaster SET status = 'deleted' WHERE user_email IN ($emailList)";
                            $this->db_db_query($sql);
                            $sql = "SELECT count(user_id) FROM usermaster WHERE (status is null OR status NOT IN ('deleted'))";
                            $result = $this->returnVector($sql);
                            $row = $result[0];
                            $this->db = $eclass_db;
                            $sql = "UPDATE course SET no_users = $row WHERE course_id = " . $target_courses[$i];
                            $this->db_db_query($sql);
                        }
                    }
                }
                $this->db = $eclass_db;
                $sql = "DELETE FROM user_course WHERE user_email IN ($emailList)";
                $this->db_db_query($sql);
            }
            
            $this->db = $intranet_db;
        }
        // Remove DB records in INTRANET_USER
        // Should be called after prepareUserRemoval()
        // Param: $list : list of UserID (comma separated, to put in SQL stmt)
        function removeUsers($list)
        {
            global $sys_custom, $intranet_root;
            if ($sys_custom['project']['NCS']) {
                $userSql = "SELECT EnglishName FROM INTRANET_USER WHERE UserID IN ($list)";
                $userList = $this->returnVector($userSql);
            }
            // CRUD LOG by Philips
            if(strpos($list,',')>-1){
                $ids = explode(',',$list);
                foreach($ids as $id){
                    $this->insertAccountLog($id, 'D');
                }
            } else {
                $this->insertAccountLog($list, 'D');
            }
            // CRUD LOG by Philips
            $sql = "DELETE FROM INTRANET_USER WHERE UserID IN ($list)";
            $this->db_db_query($sql);
            
            if ($sys_custom['project']['NCS']) {
                include_once ($intranet_root . "/includes/libcurdlog.php");
                $log = new libcurdlog();
                $log->addlog("User Settings", "DELETE", "INTRANET_USER", "UserID", $list, "", "REMOVED: " . implode(',', $userList), $sql);
                unset($log);
            }
        }
        
        // Set Students to Alumni
        function setToAlumni($list, $YearOfLeft = '')
        {
            global $alumni_GroupID;
            // Change User Type
            $sql = "UPDATE INTRANET_USER SET RecordType = 4, RecordStatus = 1 WHERE UserID IN ($list) AND RecordType = 2";
            $this->db_db_query($sql);
            // CRUD LOG by Philips
            if(strpos($list,',')>-1){
                $ids = explode(',',$list);
                foreach($ids as $id){
                    $this->insertAccountLog($id, 'U');
                }
            } else {
                $this->insertAccountLog($list, 'U');
            }
            // CRUD LOG by Philips
            // Add to alumni group
            $users = explode(",", $list);
            $delimiter = "";
            $values = "";
            for ($i = 0; $i < sizeof($users); $i ++) {
                $id = $users[$i];
                $values .= "$delimiter ($id,$alumni_GroupID)";
                $delimiter = ",";
            }
            $sql = "INSERT IGNORE INTO INTRANET_USERGROUP (UserID, GroupID) VALUES $values";
            $this->db_db_query($sql);
            $this->UpdateRole_UserGroup();
            
            // update the lastest class name and class number to alumni from ARCHIVE Class History
            for ($i = 0; $i < sizeof($users); $i ++) {
                $id = $users[$i];
                $sql = "SELECT ClassName, ClassNumber FROM PROFILE_ARCHIVE_CLASS_HISTORY 
                 		WHERE UserID='$id' ORDER BY AcademicYear DESC LIMIT 1";
                $result = $this->returnArray($sql);
                $this_classname = $result[0]['ClassName'];
                $this_classnumber = $result[0]['ClassNumber'];
                $sql = "UPDATE INTRANET_USER SET ClassName='$this_classname', ClassNumber='$this_classnumber', YearOfLeft='$YearOfLeft' WHERE UserID = $id";
                $this->db_db_query($sql);
            }
        }

        function returnLeftStudentIDList()
        {
            $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 3";
            return $this->returnVector($sql);
        }
        
        // Implemented in SchoolNet's case
        // Create User if not exists
        // Update User if exists
        // Param: login name, password, email, ename, cname, gender, title, mobile, usertype, teacher, classname, classnumber, childlogin
        function createUsers($userlogin, $password, $email, $ename, $cname, $gender, $title, $mobile, $usertype, $teacher, $classname, $classnumber, $childlogin)
        {
            global $HTTP_SERVER_VARS, $special_feature, $intranet_root, $intranet_email_generation_domain, $intranet_authentication_method, $intranet_password_salt, $sys_custom;
            // Check info
            if ($sys_custom['project']['NCS']) {
                include_once ($intranet_root . "/includes/libcurdlog.php");
                $log = new libcurdlog();
            }
            if ($userlogin == "")
                return false;
            if ($password == "")
                $password = intranet_random_passwd(8);
            if ($email == "") {
                $domain_name = ($intranet_email_generation_domain == "" ? $_SERVER["SERVER_NAME"] : $intranet_email_generation_domain);
                $email = "$userlogin@$domain_name";
            }
            if ($usertype != 1 || $usertype != 2 || $usertype != 3)
                return false;
                // Try insert
            if ($intranet_authentication_method == "HASH") {
                $password_field = ",HashedPass";
                $password_value = ",MD5('" . $userlogin . $password . $intranet_password_salt . "')";
                $update_password_field = ", HashedPass = MD5('" . $userlogin . $password . $intranet_password_salt . "')";
            } else {
                $password_field = ", UserPassword";
                $password_value = ", '$password'";
                $update_password_field = ", UserPassword = '$password'";
            }
            
            $fieldname = "UserLogin  $password_field, UserEmail, EnglishName, ChineseName, Gender, Title, MobileTelNo, RecordType, RecordStatus";
            $values = "'$userlogin' $password_value ,'$email','$ename','$cname','$gender','$title','$mobile','$usertype',1";
            $sql = "INSERT INTO INTRANET_USER ($fieldname) VALUES ($values)";
            $insert_success = $this->db_db_query($sql);
            // CRUD LOG by Philips
            $this->insertAccountLog($this->db_insert_id(), 'C');
            // CRUD LOG by Philips
            if ($sys_custom['project']['NCS']) {
                $log->addlog("User Settings", "NEW", "INTRANET_USER", "UserID", $this->db_insert_id(), "", "", $sql);
            }
            $sql = "SELECT UserID,UserEmail FROM INTRANET_USER WHERE UserLogin = '$userlogin'";
            $temp = $this->returnArray($sql, 2);
            $target_user_id = $temp[0][0];
            if ($target_user_id == "")
                return false;
            $target_user_oldemail = $temp[0][1];
            
            if (! $insert_success) {
                // Try Update
                $other_fields = "";
                if ($password != "") {
                    $other_fields .= $update_password_field; // ",UserPassword = '$password'";
                }
                if ($email != "") {
                    $other_fields .= ", UserEmail = '$email'";
                }
                $sql = "UPDATE INTRANET_USER SET EnglishName = '$ename',
                                     ChineseName = '$cname', Gender = '$gender', Title = '$title',
                                     MobileTelNo = '$mobile' $other_fields
                              WHERE UserID = '$target_user_id'";
                $this->db_db_query($sql);
                $leclass = new libeclass();
                $leclass->eClassUserUpdateInfoImport($target_user_oldemail, $title, $ename, $cname, "", "", $password, $classname, $classnumber, $email, $gender);
            } else {
                // Insert to ID group
                if ($usertype == 1) // Teacher/Staff
{
                    if ($teacher == 1) // Teaching staff -> Teacher group
{
                        $target_idgroup = 1;
                    } else // Non-teaching staff -> Admin staff group
{
                        $target_idgroup = 3;
                    }
                } else 
                    if ($usertype == 2) // Student -> Student group
{
                        $target_idgroup = 2;
                    } else 
                        if ($usertype == 3) // Parent -> Parent group
{
                            $target_idgroup = 4;
                        }
                $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID) VALUES ($target_idgroup, $target_user_id)";
                $this->db_db_query($sql);
            }
            
            // Update other information
            if ($usertype == 1) {
                $sql = "UPDATE INTRANET_USER SET Teaching = '$teacher' WHERE UserID = '$target_user_id'";
                $this->db_db_query($sql);
            } else 
                if ($usertype == 2) {
                    $sql = "UPDATE INTRANET_USER SET ClassName = '$classname', ClassNumber = '$classnumber'
                              WHERE UserID = '$target_user_id'";
                    $this->db_db_query($sql);
                    
                    $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ($target_user_id,0)";
                    $this->db_db_query($sql);
                } else 
                    if ($usertype == 3) {
                        // Add Child link
                        $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$childlogin' AND RecordType = 2";
                        $temp = $this->returnVector($sql);
                        $child_user_id = $temp[0];
                        if ($child_user_id != "") {
                            $sql = "INSERT INTO INTRANET_PARENTRELATION (ParentID, StudentID) VALUES ($target_user_id, $child_user_id)";
                            $this->db_db_query($sql);
                        }
                    }
            
            $this->UpdateRole_UserGroup();
            
            if ($special_feature['imail']) {
                // Get Default Quota
                $file_content = get_file_content($intranet_root . "/file/account_mail_quota.txt");
                if ($file_content == "") {
                    $quota = 10;
                } else {
                    $userquota = explode("\n", $file_content);
                    $quota = $userquota[$usertype - 1];
                }
                $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$target_user_id', '$quota')";
                $this->db_db_query($sql);
            }
            return true;
        }
        
        // ####################################################################################
        function displayGroupPage_portlet()
        {
            global $image_path;
            global $i_frontpage_schoolinfo_groupinfo_group_timetable, $i_frontpage_schoolinfo_groupinfo_group_chat, $i_frontpage_schoolinfo_groupinfo_group_bulletin, $i_frontpage_schoolinfo_groupinfo_group_links, $i_frontpage_schoolinfo_groupinfo_group_files, $i_adminmenu_plugin_qb, $i_frontpage_schoolinfo_groupinfo_group_settings;
            $groups = $this->returnGroupsWithFunctionAdmin();
            // print_r($groups);
            $groupList = "";
            $delimiter = "";
            for ($i = 0; $i < sizeof($groups); $i ++) {
                $groupList .= $delimiter . $groups[$i][0];
                $delimiter = ",";
            }
            $available = returnGroupAvailableFunctions();
            $announce = $this->returnGroupsNewAnnounce($groupList);
            $bulletin = $this->returnGroupsNewBulletin($groupList);
            $links = $this->returnGroupsNewLink($groupList);
            $files = $this->returnGroupsNewFile($groupList);
            
            $x = "<table width=100% border=0 cellpadding=2 cellspacing=0 class=12grey>\n";
            for ($i = 0; $i < sizeof($groups); $i ++) {
                list ($id, $name, $function, $admin, $type) = $groups[$i];
                $grouplink = "<a href=javascript:openGroupPage($id)>$name</a>";
                $countBull = $bulletin[$id];
                $countLink = $links[$id];
                $countFile = $files[$id];
                $countBull = ($countBull == "" || $countBull == 0 ? "" : "$countBull");
                $countLink = ($countLink == "" || $countLink == 0 ? "" : "$countLink");
                $countFile = ($countFile == "" || $countFile == 0 ? "" : "$countFile");
                // Field : FunctionAccess
                // - Bit-1 (LSB) : timetable
                // - Bit-2 : chat
                // - Bit-3 : bulletin
                // - Bit-4 : shared links
                // - Bit-5 : shared files
                // - Bit-6 : question bank
                // - Bit-7 : Photo Album
                // - Bit-8 (MSB) : Survey
                if ($function == "ALL") {
                    $isTimetable = ($available[0] != "");
                    $isChat = ($available[1] != "");
                    $isBulletin = ($available[2] != "");
                    $isLink = ($available[3] != "");
                    $isFile = ($available[4] != "");
                    $isQB = $type == 2 && ($available[5] != "");
                    $isPhoto = ($available[6] != "");
                    $isSurvey = ($available[7] != "");
                } else {
                    $isTimetable = (substr($function, 0, 1) == '1' && $available[0] != "");
                    $isChat = (substr($function, 1, 1) == '1' && $available[1] != "");
                    $isBulletin = (substr($function, 2, 1) == '1' && $available[2] != "");
                    $isLink = (substr($function, 3, 1) == '1' && $available[3] != "");
                    $isFile = (substr($function, 4, 1) == '1' && $available[4] != "");
                    $isQB = $type == 2 && (substr($function, 5, 1) == '1' && $available[5] != "");
                    $isPhoto = (substr($function, 6, 1) == '1' && $available[6] != "");
                    $isSurvey = (substr($function, 7, 1) == '1' && $available[7] != "");
                }
                $functionIcon = "";
                $num_announce = $announce[$id];
                $functionIcon .= "<a href=javascript:openGroupPage($id)><img src=\"$image_path/portal/index/icon_announcement.gif\" border=0 align=absmiddle alt=''>$num_announce</a>\n";
                if ($isTimetable)
                    $functionIcon .= "<a href=javascript:fe_view_timetable($id)><img src=\"$image_path/portal/index/icon_timetable.gif\" border=0 align=absmiddle alt='$i_frontpage_schoolinfo_groupinfo_group_timetable'></a>\n";
                if ($isChat)
                    $functionIcon .= "<a href=javascript:fe_view_chat($id)><img src=\"$image_path/portal/index/icon_chatroom.gif\" border=0 align=absmiddle alt='$i_frontpage_schoolinfo_groupinfo_group_chat'></a>\n";
                if ($isBulletin)
                    $functionIcon .= "<a href=javascript:fe_view_bulletin($id)><img src=\"$image_path/portal/index/icon_bulletin.gif\" border=0 align=absmiddle alt='$i_frontpage_schoolinfo_groupinfo_group_bulletin'>$countBull</a>\n";
                if ($isLink)
                    $functionIcon .= "<a href=javascript:fe_view_links($id)><img src=\"$image_path/portal/index/icon_sharedlinks.gif\" border=0 align=absmiddle alt='$i_frontpage_schoolinfo_groupinfo_group_links'>$countLink</a>\n";
                if ($isFile)
                    $functionIcon .= "<a href=javascript:fe_view_files($id)><img src=\"$image_path/portal/index/icon_sharedfiles.gif\" border=0 align=absmiddle alt='$i_frontpage_schoolinfo_groupinfo_group_files'>$countFile</a>\n";
                if ($isQB)
                    $functionIcon .= "<a href=javascript:fe_view_qb($id)><img src=\"$image_path/portal/index/icon_schoolbasedqb.gif\" border=0 align=absmiddle alt='$i_adminmenu_plugin_qb'></a>\n";
                if ($isPhoto)
                    $functionIcon .= ""; // Not yet implemented
                if ($isSurvey)
                    $functionIcon .= ""; // Not yet implemented
                if ($admin == "A")
                    $functionIcon .= "<a href=javascript:fe_view_settings($id)><img src=\"$image_path/portal/index/icon_groupsetting.gif\" border=0 align=absmiddle alt='$i_frontpage_schoolinfo_groupinfo_group_settings'></a>\n";
                $x .= "<tr valign=top>\n";
                // <img src="images/secportal/icon_news.gif" width="25" height="8">
                $x .= "<td width=15><img src=\"$image_path/portal/index/bullet_square_red.gif\" width=13 height=13 align=absmiddle></td>\n";
                $x .= "<td class=12grey>$grouplink </td>\n";
                $x .= "</tr>\n";
                $x .= "<tr valign=top>\n";
                $x .= "<td>&nbsp;</td>\n";
                $x .= "<td>$functionIcon</td>\n";
                $x .= "</tr>\n";
            }
            $x .= "</table>\n";
            return $x;
        }

        function synChildrenNames($studentlist = "")
        {
            if ($studentlist != "") {
                $conds = " AND ";
            }
            $sql = "SELECT UserID FROM INTRANET_USER WHERE ";
        }

        function user_login($user_id = "")
        {
            global $eclass_root, $image_path, $admin_url_path;
            global $intranet_db, $intranet_rel_path;
            
            // $this->db = $intranet_db;
            if ($user_id == "")
                $sql = "SELECT UserLogin FROM {$intranet_db}.INTRANET_USER WHERE UserEmail='" . $this->user_email . "'";
            else
                $sql = "SELECT UserLogin FROM {$intranet_db}.INTRANET_USER WHERE UserID='" . $user_id . "'";
            $result = $this->returnVector($sql);
            $x = $result[0];
            
            return $x;
        }

        function user_name($isNickname = 0)
        {
            $x = "";
            if (trim($this->class_number) != '')
                $x .= "<" . $this->class_number . "> ";
            if ($isNickname) {
                $x .= $this->nickname;
            } else {
                $x .= ($this->lastname != "") ? $this->lastname . " " . $this->firstname : $this->firstname;
            }
            return trim($x);
        }

        function returnUsersByIdentity($type, $teaching = "", $AcadmicYearID = "", $UserRecordStatus = 1, $YearOfLeft = "", $includeUserIdAry = '', $excludeUserIdAry = '')
        {
            global $intranet_session_language;
            global $sys_custom;
            // if($sys_custom['hideTeacherTitle']){
            // $name_field = getNameFieldWithClassNumberByLang("iu.",true);
            // }else{
            // $name_field = getNameFieldWithClassNumberByLang("iu.");
            // }
            $name_field = getNameFieldWithClassNumberByLang("iu.", $sys_custom['hideTeacherTitle'], $sys_custom['eEnrolment']['ShowStudentNickName']);
            
            if (trim($AcadmicYearID) == '')
                $AcadmicYearID = Get_Current_Academic_Year_ID();
            $userList = array(
                array(),
                array()
            );
            if ($type == "")
                return $userList;
                /*
             * if($teaching!=1)
             * $sql = "SELECT UserID,$name_field FROM INTRANET_USER WHERE RecordType=$type AND (Teaching!=1 OR Teaching IS NULL) AND RecordStatus=1 ORDER BY ClassName, ClassNumber, EnglishName";
             * else
             * $sql = "SELECT UserID,$name_field FROM INTRANET_USER WHERE RecordType=$type AND Teaching=1 AND RecordStatus=1 ORDER BY ClassName, ClassNumber, EnglishName";
             */
            
            $conds_userId = '';
            
            if ($includeUserIdAry != '') {
                $conds_userId .= " AND iu.UserID In ('" . implode("','", (array) $includeUserIdAry) . "') ";
            }
            
            if ($excludeUserIdAry != '') {
                $conds_userId .= " AND iu.UserID NOT In ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }
            
            if ($intranet_session_language == "en") {
                $OrderField = "iu.EnglishName";
            } else {
                $OrderField = "iu.EnglishName";
            }
            if ($type != "1") {
                $OrderField = "iu.ClassName, iu.ClassNumber, " . $OrderField;
            }
            if ($type == 4) {
                // alumni
                if ($YearOfLeft != "") {
                    $sql_cond = " AND YearOfLeft='{$YearOfLeft}' ";
                }
                
                $sql = "	SELECT 
								iu.UserID,$name_field AS OutputName
							FROM
								INTRANET_USER iu
							WHERE
								iu.RecordType=$type 
								AND RecordStatus in ($UserRecordStatus)
								{$sql_cond}
								$conds_userId
							ORDER BY
								iu.EnglishName, iu.ChineseName
						 ";
            } else 
                if ($teaching != 1) {
                    
                    $sql = "	SELECT 
								iu.UserID,$name_field AS OutputName 
							FROM
								INTRANET_USER iu
								LEFT JOIN YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
								LEFT JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcadmicYearID'
							WHERE
								iu.RecordType=$type AND (iu.Teaching!=1 OR iu.Teaching IS NULL) 
								AND RecordStatus in ($UserRecordStatus)
								AND (iu.RecordType <> 2 OR (ycu.ClassNumber <> '' AND yc.YearClassID <> '' AND ycu.ClassNumber IS NOT NULL AND yc.YearClassID IS NOT NULL))
								$conds_userId 
							ORDER BY
								{$OrderField}
						 ";
                } else {
                    $sql = "	SELECT 
								iu.UserID,$name_field AS OutputName 
							FROM 
								INTRANET_USER iu
								LEFT JOIN YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
								LEFT JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcadmicYearID'
							WHERE 
								iu.RecordType=$type AND iu.Teaching=1 AND iu.RecordStatus=1
								AND (iu.RecordType <> 2 OR (ycu.ClassNumber <> '' AND yc.YearClassID <> '' AND ycu.ClassNumber IS NOT NULL AND yc.YearClassID IS NOT NULL))
								$conds_userId 
							ORDER BY 
								{$OrderField}
							";
                }
            $userList = $this->returnArray($sql, 2);
            return $userList;
        }

        function returnPersonalPhoto()
        {}

        function returnUserID($ClassName = '', $ClassNumber = '', $RecordType = 2, $RecordStatus = '-1', $SchoolYear = '')
        {
            $cond_RecordStatus = ($RecordStatus == - 1) ? "" : " AND iu.RecordStatus IN (" . $RecordStatus . ")";
            $AcademicYearID = $SchoolYear == '' ? Get_Current_Academic_Year_ID() : $SchoolYear;
            $sql = "
				SELECT 
					iu.UserID 
				FROM 
					INTRANET_USER iu
					INNER JOIN YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$AcademicYearID'
				WHERE
					yc.ClassTitleEn = '$ClassName'
					AND ycu.ClassNumber = '$ClassNumber'
					AND RecordType = '$RecordType'
					$cond_RecordStatus 
			";
            
            // $sql = "select UserID from INTRANET_USER where ClassName='$ClassName' and ClassNumber='$ClassNumber' and RecordType=$RecordType ";
            // $sql .= ($RecordStatus==-1) ? "" : " and RecordStatus in (". $RecordStatus .")";
            $x = $this->returnArray($sql, 1);
            return $x[0][0];
        }

        function returnUserIDByClassIDClassNumber($classID = '', $classNumber = '')
        {
            $sql = "SELECT UserID FROM YEAR_CLASS_USER WHERE YearClassID=$classID AND ClassNumber=$classNumber";
            $x = $this->returnVector($sql);
            return $x[0];
        }

        function needAttendance()
        {
            $sql = "select 
								count(1)
							from 
								INTRANET_USER AS a
                INNER JOIN 
                CARD_STAFF_ATTENDANCE_USERGROUP as c 
                ON a.UserID = c.UserID AND a.UserID = " . $this->UserID . "
             ";
            $AttendGroup = $this->returnVector($sql);
            
            return ($AttendGroup[0] > 0) ? true : false;
        }

        function UserName2Lang($first = "en", $DisplayFormat = 1)
        {
            global $intranet_session_language;
            
            $en_name = $this->EnglishName;
            $ch_name = $this->ChineseName;
            
            $ary = array();
            $ary[] = $first == "en" ? $en_name : $ch_name;
            $ary[] = $first == "en" ? $ch_name : $en_name;
            
            switch ($DisplayFormat) {
                case 1:
                    $x = $ary[0] . ($ary[1] ? " (" . $ary[1] . ")" : "");
                    break;
                case 2:
                    $x = $ary[0] . ($ary[1] ? " " . $ary[1] . "" : "");
                    break;
            }
            
            return $x;
        }
        
        // Get the link of official photo of a student by User ID
        function GET_OFFICIAL_PHOTO_BY_USER_ID($ParUserID)
        {
            /*
             * # Get data of given users from db
             * $sql = "
             * SELECT
             * WebSAMSRegNo
             * FROM
             * INTRANET_USER
             * WHERE
             * UserID = '".$ParUserID."'
             * ";
             *
             * $Regno = $this->returnVector($sql);
             *
             * return $this->GET_OFFICIAL_PHOTO($Regno[0]);
             */
            
            // Get data of given users from db
            $sql = "
								SELECT
									UserLogin
								FROM
									INTRANET_USER
								WHERE
									UserID = '" . $ParUserID . "'
							";
            
            $result = $this->returnVector($sql);
            
            if (empty($result)) {
                $sql = "
								SELECT
									UserLogin
								FROM
									INTRANET_ARCHIVE_USER
								WHERE
									UserID = '" . $ParUserID . "'
							";
                
                $result = $this->returnVector($sql);
            }
            
            return $this->GET_USER_PHOTO($result[0]);
        }
        
        // Get the link of official photo of a student by Registion Number
        function GET_OFFICIAL_PHOTO($ParRegNo)
        {
            /*
             * global $intranet_rel_path, $intranet_root;
             *
             * $tmp_regno = trim(str_replace("#", "", $ParRegNo));
             * $photolink = "/file/official_photo/".$tmp_regno.".jpg";
             * $photo_filepath = $intranet_root.$photolink;
             * $photo_url = $intranet_rel_path.$photolink;
             *
             * return array($photo_filepath, $photo_url, $photolink);
             */
            
            // Get data of given users from db
            $sql = "
								SELECT
									UserLogin
								FROM
									INTRANET_USER
								WHERE
									WebSAMSRegNo = '" . $ParRegNo . "'
							";
            
            $result = $this->returnVector($sql);
            
            return $this->GET_USER_PHOTO($result[0]);
        }

        function GET_USER_PHOTO($UserLogin = '', $Uid = '', $UseSamplePhoto = true)
        {
            if ($UserLogin) {
                global $intranet_rel_path, $intranet_root, $intranet_rel_path;
                
                $photoname = $UserLogin . ".jpg";
                
                $photolink = "/file/user_photo/" . $photoname;
                
                $photo_filepath = $intranet_root . $photolink;
                $photo_url = $intranet_rel_path . $photolink;
                
                // # since in photo/personal, the photo is in term of [uid]##
                if ($Uid == '') {
                    $Uid = $this->getUserIDByUserLogin($UserLogin);
                }
                
                $photoname_uid = "p" . $Uid . ".jpg";
                
                $photolink_uid = "/file/photo/personal/" . $photoname_uid;
                $photo_url_uid = $intranet_rel_path . $photolink_uid;
                if (! file_exists($intranet_root . $photolink_uid)) {
                    $photoname_uid = "p" . $Uid . ".JPG";
                    $photolink_uid = "/file/photo/personal/" . $photoname_uid;
                    $photo_url_uid = $intranet_rel_path . $photolink_uid;
                }
                $photo_filepath_uid = $intranet_root . $photolink_uid;
                
                // ###
                
                if (file_exists($photo_filepath)) {
                    $photo_filepath = $photo_filepath;
                } else 
                    if (file_exists($photo_filepath_uid)) {
                        $photo_filepath = $photo_filepath_uid;
                        $photolink = $photolink_uid;
                        $photo_url = $photo_url_uid;
                    } else 
                        if ($UseSamplePhoto) {
                            // IF THE IMAGE NOT FOND, RETURN A SAMPLE IMAGE
                            // $photo_filepath = "/images/myaccount_personalinfo/samplephoto.gif";
                            // $photo_url = "/images/myaccount_personalinfo/samplephoto.gif";
                            // $photolink = "/images/myaccount_personalinfo/samplephoto.gif";
                            $photo_filepath = $this->returnDefaultOfficialPhotoPath();
                            $photo_url = $this->returnDefaultOfficialPhotoPath();
                            $photolink = $this->returnDefaultOfficialPhotoPath();
                        } else {
                            $photo_filepath = null;
                            $photo_url = null;
                            $photolink = null;
                        }
                
                return array(
                    $photo_filepath,
                    $photo_url,
                    $photolink
                );
            }
        }

        function GET_USER_BY_CLASS_CLASSNO($cname, $cno, $ay = '')
        {
            if (empty($ay)) {
                $ay = Get_Current_Academic_Year_ID();
            }
            
            $sql = "
					select 
						a.UserID, 
						 b.ClassTitleEN,
						 a.ClassNumber,
						 b.ClassTitleB5
						from 
						YEAR_CLASS_USER as a
						left join YEAR_CLASS as b on (b.YearClassID=a.YearClassID)
					where 
						b.AcademicYearID = $ay and
						b.ClassTitleEN = '$cname' and
						a.ClassNumber = $cno
					";
            $result = $this->returnArray($sql);
            return $result;
        }

        function Get_User_By_HKEdCityUserLogin($HKEdCityUserLogin)
        {
            $sql = " SELECT UserID, UserLogin FROM INTRANET_USER WHERE HKEdCityUserLogin = '" . trim($HKEdCityUserLogin) . "'";
            $result = $this->returnArray($sql);
            
            return $result[0];
        }

        function getUserIDByUserLogin($ParUserLogin)
        {
            if (is_array($this->BatchUsers) && sizeof($this->BatchUsers) > 0) {
                // try to search from preload data
                foreach ($this->BatchUsers as $uid => $userobj) {
                    if ($userobj[1] == $ParUserLogin) {
                        $tmp[] = $uid;
                        // debug("found");
                        break;
                    }
                }
            }
            
            if (sizeof($tmp) < 1 || ! isset($tmp)) {
                $sql = " SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$ParUserLogin'";
                $tmp = $this->returnVector($sql);
            }
            
            return $tmp[0];
        }

        function getUserIDsByWebSamsRegNoArr($WebSamsRegNoArr, $isArchived = 0, $alwaysReturnLowerCase = false)
        {
            if ($isArchived) {
                $tableName = 'INTRANET_ARCHIVE_USER';
            } else {
                $tableName = 'INTRANET_USER';
            }
            
            $regno_field = $alwaysReturnLowerCase ? "LOWER(WebSamsRegNo) as WebSamsRegNo" : "WebSamsRegNo";
            
            $sql = "SELECT UserID,$regno_field FROM $tableName WHERE WebSamsRegNo != '' AND WebSamsRegNo IN ('" . implode("','", (array) $WebSamsRegNoArr) . "')";
            return $this->returnResultSet($sql);
        }

        function getUserIDsByJupasNumArr($HKJApplNoArr, $isArchived = 0)
        {
            if ($isArchived) {
                $tableName = 'INTRANET_ARCHIVE_USER';
            } else {
                $tableName = 'INTRANET_USER';
            }
            $sql = "SELECT UserID, HKJApplNo AS jupasNo FROM $tableName WHERE HKJApplNo IN ('" . implode("','", (array) $HKJApplNoArr) . "')";
            
            return $this->returnResultSet($sql);
        }

        function getUserIDsBySTRNArr($STRNArr, $isArchived = 0)
        {
            if ($isArchived) {
                $tableName = 'INTRANET_ARCHIVE_USER';
            } else {
                $tableName = 'INTRANET_USER';
            }
            $sql = "SELECT UserID,STRN FROM $tableName WHERE STRN IN ('" . implode("','", (array) $STRNArr) . "')";
            
            return $this->returnResultSet($sql);
        }

        function getUserIDsByHKIDArr($HKIDArr, $isArchived = 0)
        {
            if ($isArchived) {
                $tableName = 'INTRANET_ARCHIVE_USER';
            } else {
                $tableName = 'INTRANET_USER';
            }
            $sql = "SELECT UserID,HKID FROM $tableName WHERE HKID IN ('" . implode("','", (array) $HKIDArr) . "')";
            
            return $this->returnResultSet($sql);
        }

        function getUserIDByWebSamsRegNo($ParWebSamsRegNo)
        {
            if (is_array($this->BatchUsers) && sizeof($this->BatchUsers) > 0) {
                // try to search from preload data
                foreach ($this->BatchUsers as $uid => $userobj) {
                    if ($userobj[38] == $ParWebSamsRegNo) {
                        $tmp[] = $uid;
                        // debug("found");
                        break;
                    }
                }
            }
            
            if (sizeof($tmp) < 1 || ! isset($tmp)) {
                $sql = " SELECT UserID FROM INTRANET_USER WHERE WebSamsRegNo = '$ParWebSamsRegNo'";
                $tmp = $this->returnVector($sql);
            }
            
            return $tmp[0];
        }

        function getUserGender($uid)
        {
            $sql = " SELECT Gender FROM INTRANET_USER WHERE UserID = '$uid'";
            $tmp = $this->returnVector($sql);
            
            return $tmp[0];
        }

        function Get_User_Studying_Form($uid = '', $AcademicYearID = '')
        {
            if ($uid == '')
                $uid = $this->UserID;
            
            if ($AcademicYearID == '')
                $AcademicYearID = Get_Current_Academic_Year_ID();
            
            $sql = "Select 
							yc.YearID, y.YearName AS YearName, 
							y.WEBSAMSCode as FormWebSAMSCode
					From
							YEAR_CLASS_USER as ycu
							Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
							Inner Join YEAR as y On (yc.YearID = y.YearID)
					Where
							ycu.UserID = '$uid'
							And
							yc.AcademicYearID = '" . $AcademicYearID . "'
					";
            $ReturnArr = $this->returnArray($sql);
            
            return $ReturnArr;
        }

        function Get_Children_Studying_Form($uid = '')
        {
            if ($uid == '')
                $uid = $this->UserID;
            
            $sql = "Select 
							ipr.StudentID, yc.YearID
					From
							INTRANET_PARENTRELATION as ipr
							Inner Join
							YEAR_CLASS_USER as ycu
							On (ipr.StudentID = ycu.UserID)
							Inner Join
							YEAR_CLASS as yc
							On (ycu.YearClassID = yc.YearClassID)
					Where
							ipr.ParentID = '$uid'
							And
							yc.AcademicYearID = '" . Get_Current_Academic_Year_ID() . "'
					";
            $ReturnArr = $this->returnArray($sql);
            
            return $ReturnArr;
        }

        function RetrieveUserInfoSetting($field_type = 'Display', $field = '', $ParUserType = '')
        {
            if ($field) {
                $thisUserType = trim($ParUserType) != '' ? $ParUserType : $_SESSION['UserType'];
                
                $sql_field = $field_type . $field . "_" . $thisUserType;
                
                $sql = "select SettingValue from GENERAL_SETTING where Module='UserInfoSettings' and SettingName='$sql_field '";
                $result = $this->returnVector($sql);
                return ($result[0] == 1);
            }
        }

        function RetrieveUserInfoSettingInArrary($SettingNameArr)
        {
            if (is_array($SettingNameArr) && sizeof($SettingNameArr) > 0) {
                
                $sql_field = '\'' . implode('\', \'', $SettingNameArr) . '\'';
                
                $sql = "select SettingName, SettingValue from GENERAL_SETTING where Module='UserInfoSettings' and SettingName IN ($sql_field) ";
                
                return build_assoc_array($this->returnArray($sql));
            }
        }

        function getChildrenSelection($ID, $Name, $OnChange = '', $ParentID = '', $SelectedChildrenID = '')
        {
            global $Lang;
            
            $onchange = '';
            if ($OnChange != "")
                $onchange = 'onchange="' . $OnChange . '"';
            $selectionTags = ' id="' . $ID . '" name="' . $Name . '" ' . $onchange;
            $FirstTitle = Get_Selection_First_Title($Lang['General']['SelectChildren']);
            
            return getSelectByArray($this->getChildrenList($ParentID), $selectionTags, $SelectedChildrenID, $all = 0, $noFirst = 0, $FirstTitle);
        }

        function getParentUsingParentApp($studentIdAry = '', $includeNoPushMessageUser = false)
        {
            global $plugin;
            
            if ($studentIdAry !== '') {
                $join_INTRANET_PARENTRELATION = " Inner Join INTRANET_PARENTRELATION as ipr ON (l.UserID = ipr.ParentID) ";
                $conds_studentId = " AND ipr.StudentID IN ('" . implode("','", (array) $studentIdAry) . "') ";
            }
            
            if ($includeNoPushMessageUser) {
                // do nth
            } else {
                $innerJoin_APP_USER_DEVICE = " Inner Join APP_USER_DEVICE as d ON (l.UserID = d.UserID) ";
                $conds_DeviceRecordStatus = " AND d.RecordStatus = 1 ";
            }
            
            $sql = "Select 
							l.UserID 
					From 
							APP_LOGIN_LOG as l
							$innerJoin_APP_USER_DEVICE
							$join_INTRANET_PARENTRELATION 
					Where
							1
							$conds_DeviceRecordStatus
							$conds_studentId
					Group By
							l.UserID
					";
            return $this->returnVector($sql);
        }

        function getStudentWithParentUsingParentApp($includeNoPushMessageUser = false)
        {
            global $plugin;
            
            if ($plugin['eClassApp']) {
                $logTable = 'APP_LOGIN_LOG';
            } else {
                $logTable = 'INTRANET_API_REQUEST_LOG';
            }
            
            if ($includeNoPushMessageUser) {
                // do nth
            } else {
                $innerJoin_APP_USER_DEVICE = " Inner Join APP_USER_DEVICE as d ON (l.UserID = d.UserID) ";
                $conds_DeviceRecordStatus = " AND d.RecordStatus = 1 ";
            }
            
            $sql = "Select 
							l.UserID 
					From 
							$logTable as l
							$innerJoin_APP_USER_DEVICE 
					Where
							1
							$conds_DeviceRecordStatus
					";
            $appUserIdAry = $this->returnVector($sql);
            
            $sql = "Select StudentID From INTRANET_PARENTRELATION Where ParentID IN ('" . implode("','", (array) $appUserIdAry) . "') Group By StudentID";
            return $this->returnVector($sql);
        }

        function getTeacherWithTeacherUsingTeacherApp($includeNoPushMessageUser = false, $includeLoggedOutTeacher = false)
        {
            if ($includeNoPushMessageUser) {
                // do nth
            } else {
                $innerJoin_APP_USER_DEVICE = " Inner Join APP_USER_DEVICE as d ON (l.UserID = d.UserID) ";
                
                if ($includeLoggedOutTeacher) {
                    $conds_DeviceRecordStatus = " AND (d.RecordStatus = 1 OR d.LoginStatus = 'out')";
                } else {
                    $conds_DeviceRecordStatus = " AND d.RecordStatus = 1 ";
                }
            }
            
            $sql = "Select 
							iu.UserID
					From
							APP_LOGIN_LOG as l
							Inner Join INTRANET_USER as iu ON (l.UserID = iu.UserID)
							$innerJoin_APP_USER_DEVICE
					Where
							iu.RecordType = 1
							And iu.RecordStatus = 1
							$conds_DeviceRecordStatus
					";
            return $this->returnVector($sql);
        }

        function getStudentWithStudentUsingStudentApp($includeNoPushMessageUser = false)
        {
            if ($includeNoPushMessageUser) {
                // do nth
            } else {
                $innerJoin_APP_USER_DEVICE = " Inner Join APP_USER_DEVICE as d ON (l.UserID = d.UserID) ";
                $conds_DeviceRecordStatus = " AND d.RecordStatus = 1 ";
            }
            
            $sql = "Select 
							iu.UserID
					From
							APP_LOGIN_LOG as l
							Inner Join INTRANET_USER as iu ON (l.UserID = iu.UserID)
							$innerJoin_APP_USER_DEVICE
					Where
							iu.RecordType = 2
							And iu.RecordStatus = 1
							$conds_DeviceRecordStatus
					";
            return $this->returnVector($sql);
        }

        function getParentStudentMappingInfo($studentIdAry = '', $parentIdAry = '', $skipStuStatusCheck=false, $parentStatusCheck=false)
        {
            if ($studentIdAry != '') {
                $conds_studentId = " And iu.UserID IN ('" . implode("','", (array) $studentIdAry) . "')";
            }
            if ($parentIdAry != '') {
                $conds_parentId = " And pr.ParentID IN ('" . implode("','", (array) $parentIdAry) . "')";
            }
            if ($skipStuStatusCheck) {
                $cond_stuStatus = " OR iu.RecordStatus IS NOT NULL ";
            }
            if ($parentStatusCheck) {
            	$joinParent = " Inner Join INTRANET_USER as iup ON (pr.ParentID = iup.UserID) ";
            	$cond_parentStatus = " AND iup.RecordStatus = '1' AND iup.RecordType = '" . USERTYPE_PARENT . "' ";
            }
            
            $studentNameField = getNameFieldWithClassNumberByLang('iu.');
            
            $sql = "Select 
							pr.ParentID, pr.StudentID, $studentNameField as StudentName
					From
							INTRANET_PARENTRELATION as pr
							Inner Join INTRANET_USER as iu ON (pr.StudentID = iu.UserID)
							$joinParent
					Where
							(iu.RecordStatus = 1 $cond_stuStatus)
							$conds_studentId
							$conds_parentId
							$cond_parentStatus
					Order By
							iu.ClassName, iu.ClassNumber
					";
            return $this->returnResultSet($sql);
        }

        function getStudentInfoByAcademicYear($academicYearId)
        {
            $sql = "SELECT
							yc.ClassTitleEN as ClassNameEn,
							yc.ClassTitleB5 as ClassNameCh,
							ycu.ClassNumber
					FROM
							YEAR_CLASS_USER as ycu
							INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '" . $academicYearId . "')
							/* INNER JOIN YEAR as y On (yc.YearID = y.YearID) */
					Where
							ycu.UserID = '" . $this->UserID . "'
					";
            return $this->returnResultSet($sql);
        }

        function returnEmailNotificationData($UserEmail, $UserLogin, $UserPassword, $EnglishName)
        {
            global $Lang;
            
            $website = get_website();
            $webmaster = get_webmaster();
            
            $email_subject = $Lang['EmailNotification']['ForgotPassword']['Subject'];
            
            $email_content = $Lang['EmailNotification']['ForgotPassword']['Content'];
            $email_content = str_replace("__NAME__", $EnglishName, $email_content);
            $email_content = str_replace("__WEBSITE__", $website, $email_content);
            $email_content = str_replace("__EMAIL__", $UserEmail, $email_content);
            $email_content = str_replace("__LOGIN__", $UserLogin, $email_content);
            $email_content = str_replace("__PASSWORD__", $UserPassword, $email_content);
            
            $email_footer = $Lang['EmailNotification']['Footer'];
            $email_footer = str_replace("__WEBSITE__", $website, $email_footer);
            $email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
            $email_content .= $email_footer;
            
            return array(
                $email_subject,
                $email_content
            );
        }

        function returnEmailNotificationData_ACK($UserEmail, $UserLogin)
        {
            global $Lang;
            
            $website = get_website();
            $webmaster = get_webmaster();
            
            $email_subject = $Lang['EmailNotification']['ForgotPassword_ACK']['Subject'];
            
            $email_content = $Lang['EmailNotification']['ForgotPassword_ACK']['Content'];
            $email_content = str_replace("__EMAIL__", $UserEmail, $email_content);
            $email_content = str_replace("__LOGIN__", $UserLogin, $email_content);
            
            $email_footer = $Lang['EmailNotification']['Footer'];
            $email_footer = str_replace("__WEBSITE__", $website, $email_footer);
            $email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
            $email_content .= $email_footer;
            
            return array(
                $email_subject,
                $email_content
            );
        }

        function returnEmailNotificationData_HashedPw($UserEmail, $UserLogin, $EnglishName, $Key)
        {
            global $Lang;
            
            $website = get_website();
            $webmaster = get_webmaster();
            
            $email_subject = $Lang['EmailNotification']['ForgotHashedPassword']['Subject'];
            
            $email_content = $Lang['EmailNotification']['ForgotHashedPassword']['Content'];
            $email_content = str_replace("__NAME__", $EnglishName, $email_content);
            $email_content = str_replace("__WEBSITE__", $website, $email_content);
            $email_content = str_replace("__RESET_PW_PATH__", "/home/iaccount/account/reset_password.php", $email_content);
            $email_content = str_replace("__EMAIL__", $UserEmail, $email_content);
            $email_content = str_replace("__LOGIN__", $UserLogin, $email_content);
            $email_content = str_replace("__RESETKEY__", urlencode($Key), $email_content);
            
            $email_footer = $Lang['EmailNotification']['Footer'];
            $email_footer = str_replace("__WEBSITE__", $website, $email_footer);
            $email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
            $email_content .= $email_footer;
            
            return array(
                $email_subject,
                $email_content
            );
        }

        function returnEmailNotificationData_HashedPw_ACK($UserEmail, $UserLogin)
        {
            global $Lang;
            
            $website = get_website();
            $webmaster = get_webmaster();
            
            $email_subject = $Lang['EmailNotification']['ForgotHashedPassword_ACK']['Subject'];
            
            $email_content = $Lang['EmailNotification']['ForgotHashedPassword_ACK']['Content'];
            $email_content = str_replace("__EMAIL__", $UserEmail, $email_content);
            $email_content = str_replace("__LOGIN__", $UserLogin, $email_content);
            
            $email_footer = $Lang['EmailNotification']['Footer'];
            $email_footer = str_replace("__WEBSITE__", $website, $email_footer);
            $email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
            $email_content .= $email_footer;
            
            return array(
                $email_subject,
                $email_content
            );
        }
        
        // ##########################################################
        // ####################### IMPORTANT ########################
        // ######## CANNOT directly copy this function to EJ ########
        // ########### (EJ has convert encoding coding) #############
        // ##########################################################
        function synUserDataToModules($userIdAry = '', $moduleAry = '')
        {
            global $PATH_WRT_ROOT, $plugin, $intranet_db, $intranet_root, $sys_custom;
            global $intranet_session_language; // for liblibrarymgmt_group_manage.php
            
            $successAry = array();
            
            // ## Sync data to app central server
            if (($plugin['eClassApp'] || $plugin['eClassTeacherApp']) && ($moduleAry == '' || in_array('app_central_server', $moduleAry))) {
                global $eclassAppConfig, $config_school_code;
                
                include_once ($PATH_WRT_ROOT . "includes/eClassApp/libeClassApp.php");
                include_once ($PATH_WRT_ROOT . "includes/eClassApp/groupMessage/libeClassApp_groupMessage.php");
                $leClassApp = new libeClassApp();
                $lgroupMessage_group = new libeClassApp_groupMessage();
                
                $groupIdAry = array_values(array_unique(Get_Array_By_Key($lgroupMessage_group->getGroupMemberList('', $userIdAry, $activeUserOnly=false), 'GroupID')));
                if (count($groupIdAry) > 0) {
                    $lgroupMessage_group->saveGroupMemberInCloud($groupIdAry);
                }
                
                $leClassApp->saveUserInfoToCloud($userIdAry);
            }
            
            // ## Syn data to LMS
            if ($plugin['library_management_system'] && ($moduleAry == '' || in_array('library_management_system', $moduleAry))) {
                include_once ($PATH_WRT_ROOT . "includes/liblibrarymgmt.php");
                include_once ($PATH_WRT_ROOT . "includes/liblibrarymgmt_group_manage.php");
                $liblms = new liblms();
                
                // Build ClassName-FormName mapping
                $sql = "Select 
								yc.ClassTitleEN, yc.ClassTitleB5, y.YearName
						From
								YEAR_CLASS as yc
								Inner Join YEAR as y On (yc.YearID = y.YearID)
						Where
								yc.AcademicYearID = '" . Get_Current_Academic_Year_ID() . "'
						";
                $classFormInfoAry = $this->returnResultSet($sql);
                $numOfInfo = count($classFormInfoAry);
                
                $classFormMappingAssoAry = array();
                for ($i = 0; $i < $numOfInfo; $i ++) {
                    $_classNameEn = trim($classFormInfoAry[$i]['ClassTitleEN']);
                    $_classNameCh = trim($classFormInfoAry[$i]['ClassTitleB5']);
                    $_formName = trim($classFormInfoAry[$i]['YearName']);
                    
                    $classFormMappingAssoAry[$_classNameEn] = $_formName;
                    $classFormMappingAssoAry[$_classNameCh] = $_formName;
                }
                
                // get related data from INTRANET_USER
                $INTRANET_USER = $intranet_db . '.INTRANET_USER';
                $LIBMS_USER = $liblms->db . '.LIBMS_USER';
                
                if ($userIdAry != '') {
                    $conds_iu_userId = " And iu.UserID In ('" . implode("','", (array) $userIdAry) . "') ";
                    $conds_lu_userId = " And lu.UserID In ('" . implode("','", (array) $userIdAry) . "') ";
                }
                
                $sql = "Select 
								iu.UserID, 
								iu.UserLogin, 
								iu.UserEmail, 
								iu.EnglishName, 
								iu.ChineseName, 
								iu.Gender,
								iu.RecordType, 
								iu.Teaching, 
								iu.ClassNumber, 
								iu.ClassName, 
								iu.RecordStatus,
								iu.HomeTelNo, 
								iu.MobileTelNo, 
								if( iu.PhotoLink='' OR iu.PhotoLink is NULL, iu.PersonalPhotoLink, iu.PhotoLink ) as PhotoLinkDetected, 
								iu.DateModified,
								iu.Barcode
						From 
								$INTRANET_USER as iu
						Where 
								1
								$conds_iu_userId
				";
                $intranetUserDataAry = $liblms->returnResultSet($sql);
                $intranetUserIdAry = Get_Array_By_Key($intranetUserDataAry, 'UserID');
                $numOfUser = count($intranetUserDataAry);
                
                // select users from LMS so as to determine running insert or update SQL statement
                $sql = "Select lu.UserID From $LIBMS_USER as lu Where 1 $conds_lu_userId";
                $lmsUserDataAry = $liblms->returnResultSet($sql);
                $lmsUserIdAry = Get_Array_By_Key($lmsUserDataAry, 'UserID');
                unset($lmsUserDataAry);
                
                $insertAry = array();
                $defaultGroupUserAssoAry = array();
                for ($i = 0; $i < $numOfUser; $i ++) {
                    $_userId = $intranetUserDataAry[$i]['UserID'];
                    $_userLogin = $intranetUserDataAry[$i]['UserLogin'];
                    $_userEmail = $intranetUserDataAry[$i]['UserEmail'];
                    $_englishName = trim($intranetUserDataAry[$i]['EnglishName']);
                    $_chineseName = trim($intranetUserDataAry[$i]['ChineseName']);
                    $_gender = trim($intranetUserDataAry[$i]['Gender']);
                    $_recordType = $intranetUserDataAry[$i]['RecordType'];
                    $_isTeaching = $intranetUserDataAry[$i]['Teaching'];
                    $_classNumber = $intranetUserDataAry[$i]['ClassNumber'];
                    $_className = trim($intranetUserDataAry[$i]['ClassName']);
                    $_recordStatus = $intranetUserDataAry[$i]['RecordStatus'];
                    $_homeTelNo = $intranetUserDataAry[$i]['HomeTelNo'];
                    $_mobileTelNo = $intranetUserDataAry[$i]['MobileTelNo'];
                    $_photoLink = $intranetUserDataAry[$i]['PhotoLinkDetected'];
                    $_dateModified = $intranetUserDataAry[$i]['DateModified'];
                    $_barcode = trim($intranetUserDataAry[$i]['Barcode']);
                    
                    $_formName = $classFormMappingAssoAry[$_className];
                    
                    $_userType = '';
                    switch ($_recordType) {
                        case USERTYPE_STAFF:
                            $_userType = ($_isTeaching == 1) ? 'T' : 'NT';
                            $_formName = '';
                            $_className = '';
                            $_classNumber = null;
                            break;
                        case USERTYPE_STUDENT:
                            $_userType = 'S';
                            if (! is_numeric($_classNumber)) {
                                $_classNumber = '';
                            }
                            break;
                        case USERTYPE_PARENT:
                            $_userType = 'P';
                            $_formName = '';
                            $_className = '';
                            $_classNumber = null;
                            break;
                        case USERTYPE_ALUMNI:
                            $_userType = 'A';
                            $_formName = '';
                            $_className = '';
                            $_classNumber = null;
                            break;
                        default:
                            $_userType = '';
                            $_formName = '';
                            $_className = '';
                            $_classNumber = null;
                            break;
                    }
                    $user_type_arr[$_userId] = $_userType;
                    
                    $_userIdDb = $liblms->pack_value($_userId, 'int');
                    $_userLoginDb = $liblms->pack_value($_userLogin, 'str');
                    $_userEmailDb = $liblms->pack_value($_userEmail, 'str');
                    $_englishNameDb = $liblms->pack_value($_englishName, 'str');
                    $_chineseNameDb = $liblms->pack_value($_chineseName, 'str');
                    $_genderDb = $liblms->pack_value($_gender, 'str');
                    $_userTypeDb = $liblms->pack_value($_userType, 'str');
                    $_classNumberDb = $liblms->pack_value($_classNumber, 'int');
                    $_classNameDb = $liblms->pack_value($_className, 'str');
                    $_formNameDb = $liblms->pack_value($_formName, 'str');
                    $_recordStatusDb = $liblms->pack_value($_recordStatus, 'int');
                    $_homeTelNoDb = $liblms->pack_value($_homeTelNo, 'str');
                    $_mobileTelNoDb = $liblms->pack_value($_mobileTelNo, 'str');
                    $_photoLinkDb = $liblms->pack_value($_photoLink, 'str');
                    $_dateModifiedDb = $liblms->pack_value($_dateModified, 'date');
                    $_barcodeDb = $liblms->pack_value($_barcode, 'str');
                    
                    if (in_array($_userId, (array) $lmsUserIdAry)) {
                        // update
                        $sql = "Update 
										$LIBMS_USER
								Set 
										UserLogin = " . $_userLoginDb . ",
										UserEmail = " . $_userEmailDb . ",
										EnglishName = " . $_englishNameDb . ",
										ChineseName = " . $_chineseNameDb . ",
										Gender = " . $_genderDb . ",
										UserType = " . $_userTypeDb . ",
										ClassNumber = " . $_classNumberDb . ",
										ClassName = " . $_classNameDb . ",
										ClassLevel = " . $_formNameDb . ",
										RecordStatus = " . $_recordStatusDb . ",
										HomeTelNo = " . $_homeTelNoDb . ",
										MobileTelNo = " . $_mobileTelNoDb . ",
										UserPhoto = " . $_photoLinkDb . ",
										DateModified = " . $_dateModifiedDb . ",
										BarCode = " . $_barcodeDb . "
								Where 
										UserID = '" . $_userId . "'
								";
                        $successAry['library_management_system']['update'][$_userId] = $liblms->db_db_query($sql);
                    } else {
                        // insert
                        $insertAry[] = " (	" . $_userIdDb . ", 
											" . $_userLoginDb . ", 
											" . $_userEmailDb . ",
											" . $_englishNameDb . ",
											" . $_chineseNameDb . ",
											" . $_genderDb . ",
											" . $_userTypeDb . ",
											" . $_classNumberDb . ",
											" . $_classNameDb . ",
											" . $_formNameDb . ",
											" . $_recordStatusDb . ",
											" . $_homeTelNoDb . ",
											" . $_mobileTelNoDb . ",
											" . $_photoLinkDb . ",
											" . $_dateModifiedDb . ",
											" . $_barcodeDb . "
										) ";
                        
                        $defaultGroupUserAssoAry[$_userType][] = $_userId;
                    }
                }
                
                // ## Handle deleted user in INTRANET_USER
                $deletedUserIdAry = array_values(array_diff($lmsUserIdAry, $intranetUserIdAry));
                $sql = "Update $LIBMS_USER Set RecordStatus = 0 Where UserID In ('" . implode("','", (array) $deletedUserIdAry) . "')";
                $successAry['library_management_system']['updateDeletedUserStatus'] = $liblms->db_db_query($sql);
                
                // ## Process insert statement by batch
                $numOfInsert = count($insertAry);
                if ($numOfInsert > 0) {
                    $numOfDataPerChunck = 500;
                    $numOfChunk = ceil($numOfInsert / $numOfDataPerChunck);
                    $insertChunkAry = array_chunk($insertAry, $numOfChunk);
                    foreach ((array) $insertChunkAry as $_insertValueAry) {
                        $_insertValueText = implode(", ", $_insertValueAry);
                        $sql = "INSERT INTO $LIBMS_USER
												(UserID, UserLogin, UserEmail, EnglishName, ChineseName, Gender, UserType, ClassNumber, ClassName, ClassLevel, RecordStatus, HomeTelNo, MobileTelNo, UserPhoto, DateModified, BarCode)
										VALUES
												$_insertValueText
										";
                        $result = $liblms->db_db_query($sql);
                        $successAry['library_management_system']['insert'][] = $result;
                        
                        // if ($result)
                        // {
                        // // $_recordType
                        // $NewUser[] = $_insertValueAry[0];
                        // }
                    }
                }
                
                // add to patron group of eLib plus
                // if (sizeof($NewUser)>0)
                // {
                // # get the student and teacher group from LIBMS_GROUP
                // # complete it later with default student and teacher group !!
                // /*
                // for ($i_user=0; $i_user<sizeof($NewUser); $i_user++)
                // {
                // if ($user_type_arr[$i_user]=="T" || $user_type_arr[$i_user]=="S")
                // {
                // // add to
                // }
                // }*/
                // }
                
                if (count($defaultGroupUserAssoAry) > 0) {
                    $lmsgm = new liblibrarymgmt_group_manage();
                    $groupAssoAry = BuildMultiKeyAssoc($lmsgm->Get_Group_List(), 'GroupType');
                    
                    foreach ((array) $defaultGroupUserAssoAry as $_groupType => $_groupUserIdAry) {
                        $_groupId = $groupAssoAry[$_groupType]['GroupID'];
                        
                        if ($_groupId > 0) {
                            $successAry['library_management_system']['addUserToDefaultGroup'][$_groupType] = $lmsgm->Add_Group_Member($_groupId, $_groupUserIdAry);
                        }
                    }
                }
            }
            
            return in_multi_array(false, (array) $successAry) ? false : true;
        }

        function getUserInfoByLogin($userLogin, $recordTypeAry = '')
        {
            if ($recordTypeAry !== '') {
                $conds_recordType = " And RecordType IN ('" . implode("','", (array) $recordTypeAry) . "') ";
            }
            
            $sql = "Select UserID From INTRANET_USER Where UserLogin Like '%" . $this->Get_Safe_Sql_Like_Query($userLogin) . "%' $conds_recordType";
            return $this->returnResultSet($sql);
        }

        function getUserInfoByUserLogin($userLogin, $recordTypeAry = '')
        {
            $NameField = getNameFieldByLang('u.');
            
            if ($recordTypeAry !== '') {
                $conds_recordType = " And RecordType IN ('" . implode("','", (array) $recordTypeAry) . "') ";
            }
            
            $sql = "Select UserID, UserLogin, " . $NameField . " as Name From INTRANET_USER as u
					Where UserLogin IN ( '" . implode("','", (array) $userLogin) . "' ) $conds_recordType";
            
            return $this->returnResultSet($sql);
        }

        function getUserInfoByName($name)
        {
            $sql = "Select 
						UserID 
					From 
						INTRANET_USER 
					Where 
							EnglishName Like '%" . $this->Get_Safe_Sql_Like_Query($name) . "%' or ChineseName Like '%" . $this->Get_Safe_Sql_Like_Query($name) . "%'";
            return $this->returnResultSet($sql);
        }

        function getGuardianInfoAry($studentIdAry, $isMainOnly = false)
        {
            global $eclass_db;
            
            if ($isMainOnly) {
                $conds_isMain = " and b.IsMain = 1 ";
            }
            
            $sql = "SELECT  
							b.UserID,
							b.ChName,
							b.EnName,
							b.EmPhone,
							b.Relation,
							b.Phone,
							b.IsSMS
				  	FROM 
							$eclass_db.GUARDIAN_STUDENT AS b 
					WHERE 
							b.UserID In ('" . implode("','", (array) $studentIdAry) . "')
							$conds_isMain
					";
            return $this->returnResultSet($sql);
        }

        function getParentGuardianInfoAry($studentIdAry = array(), $parentAccDataFirst = 0, $allStudent = 0)
        {
            
            // If no student is passing in, will get all student
            if (empty($studentIdAry) && $allStudent == 1) {
                $StudentInfoArr = $this->returnUsersType(2);
                $StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
                $studentIdAry = $StudentIDArr;
            }
            
            $studentAppArr = $this->getStudentWithParentUsingParentApp();
            $studentNoAppIDArr = array_diff($studentIdAry, $studentAppArr);
            
            if (! $parentAccDataFirst) {
                // Guardian data first
                $studentObj = new libuser('', '', $studentNoAppIDArr);
                $guardianInfo = $this->getGuardianInfoAry($studentNoAppIDArr);
                // # sort by isSMS if student have more then 1 guardian with use the one isSMS = 1
                sortByColumn2($guardianInfo, 'isSMS');
                $guardianAssoInfo = BuildMultiKeyAssoc($guardianInfo, 'UserID');
                
                foreach ((array) $studentNoAppIDArr as $_studentNoAppId) {
                    $studentObj->LoadUserData($_studentNoAppId);
                    
                    $_guardianInfoArr = $guardianAssoInfo[$_studentNoAppId];
                    $_outputName = Get_String_Display(Get_Lang_Selection($_guardianInfoArr['ChName'], $_guardianInfoArr['EnName']));
                    
                    $resultArr[$_studentNoAppId]['ClassName'] = $studentObj->ClassName;
                    $resultArr[$_studentNoAppId]['ClassNumber'] = $studentObj->ClassNumber;
                    $resultArr[$_studentNoAppId]['StudentName'] = $studentObj->StandardDisplayName;
                    $resultArr[$_studentNoAppId]['ParentName'] = $_outputName;
                    
                    if ($_guardianInfoArr['EmPhone'] != '') {
                        $resultArr[$_studentNoAppId]['ContactNumber'] = $_guardianInfoArr['EmPhone'];
                    } else {
                        $resultArr[$_studentNoAppId]['ContactNumber'] = $_guardianInfoArr['Phone'];
                    }
                    
                    if ($resultArr[$_studentNoAppId]['ContactNumber'] == '') {
                        $NeedParentArr[] = $_studentNoAppId;
                    } else {
                        continue;
                    }
                }
                
                if (count($NeedParentArr) > 0) {
                    $ParentNoAppArr = $this->getParentStudentMappingInfo($NeedParentArr);
                    $ParentNoAppIDAry = Get_Array_By_Key($ParentNoAppArr, 'ParentID');
                    $noAppStudent_Parent = BuildMultiKeyAssoc($ParentNoAppArr, 'StudentID');
                    
                    $parentObj = new libuser('', '', $ParentNoAppIDAry);
                    
                    foreach ((array) $NeedParentArr as $_studentId) {
                        $_parentID = $noAppStudent_Parent[$_studentId]['ParentID'];
                        $parentObj->LoadUserData($_parentID);
                        
                        $resultArr[$_studentId]['ParentName'] = $parentObj->StandardDisplayName;
                        $resultArr[$_studentId]['ContactNumber'] = $parentObj->MobileTelNo;
                    }
                }
            } else {
                // Parent Account data first
                $ParentNoAppArr = $this->getParentStudentMappingInfo($studentNoAppIDArr);
                $ParentNoAppIDAry = Get_Array_By_Key($ParentNoAppArr, 'ParentID');
                $noAppStudent_Parent = BuildMultiKeyAssoc($ParentNoAppArr, 'StudentID');
                
                $parentObj = new libuser('', '', $ParentNoAppIDAry);
                $studentObj = new libuser('', '', $studentNoAppIDArr);
                
                foreach ((array) $studentNoAppIDArr as $_studentNoAppId) {
                    
                    $_parentID = $noAppStudent_Parent[$_studentNoAppId]['ParentID'];
                    $studentObj->LoadUserData($_studentNoAppId);
                    $parentObj->LoadUserData($_parentID);
                    
                    $resultArr[$_studentNoAppId]['ClassName'] = $studentObj->ClassName;
                    $resultArr[$_studentNoAppId]['ClassNumber'] = $studentObj->ClassNumber;
                    $resultArr[$_studentNoAppId]['StudentName'] = $studentObj->StandardDisplayName;
                    $resultArr[$_studentNoAppId]['ParentName'] = $parentObj->StandardDisplayName;
                    $resultArr[$_studentNoAppId]['ContactNumber'] = $parentObj->MobileTelNo;
                    
                    if ($resultArr[$_studentNoAppId]['ContactNumber'] == '') {
                        $NeedGuardianArr[] = $_studentNoAppId;
                    } else {
                        continue;
                    }
                }
                
                if (count($NeedGuardianArr) > 0) {
                    
                    $guardianInfo = $this->getGuardianInfoAry($NeedGuardianArr);
                    sortByColumn2($guardianInfo, 'isSMS');
                    $guardianAssoInfo = BuildMultiKeyAssoc($guardianInfo, 'UserID');
                    
                    foreach ((array) $NeedGuardianArr as $_studentId) {
                        
                        $_guardianInfoArr = $guardianAssoInfo[$_studentId];
                        $_outputName = Get_String_Display($_guardianInfoArr['ChName'], $_guardianInfoArr['EnName']);
                        
                        $resultArr[$_studentId]['ParentName'] = $_outputName;
                        
                        if ($_guardianInfoArr['EmPhone'] != '') {
                            $resultArr[$_studentNoAppId]['ContactNumber'] = $_guardianInfoArr['EmPhone'];
                        } else {
                            $resultArr[$_studentNoAppId]['ContactNumber'] = $_guardianInfoArr['Phone'];
                        }
                    }
                }
            }
            
            return $resultArr;
        }

        function AddOrUpdateAlumniYearGroup($user_id, $YearOfLeft)
        {
            $alumni_code_prefix = "ALUMNI_";
            // find existing year group
            $sql = "SELECT iu.GroupID FROM INTRANET_USERGROUP AS iu, INTRANET_GROUP AS ig WHERE iu.UserID='{$user_id}' AND  ig.RecordType=0 AND ig.GroupID=iu.GroupID AND ig.GroupCode LIKE '{$alumni_code_prefix}%' ";
            $rows = $this->returnVector($sql);
            $previous_group_id = (sizeof($rows) > 1) ? implode(",", $rows) : $rows[0];
            
            if ($YearOfLeft != "") {
                $new_group_title = "Alumni " . $YearOfLeft;
                $new_group_code = $alumni_code_prefix . $YearOfLeft;
                
                // 1. find the group (groupID and name)
                $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType=0 AND RecordStatus=1 AND GroupCode='$new_group_code' ";
                $rows = $this->returnResultSet($sql);
                $target_group_id = $rows[0]["GroupID"];
                
                // 2. if group doesn't exist, create the group
                if ($target_group_id == "" || $target_group_id < 1) {
                    $sql = "INSERT INTO INTRANET_GROUP (Title, TitleChinese, RecordType, RecordStatus, DateInput, DateModified, GroupCode) " . "VALUES ('{$new_group_title}', '{$new_group_title}', 0, 1, now(), now(), '{$new_group_code}') ";
                    $this->db_db_query($sql);
                    
                    $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType=0 AND RecordStatus=1 AND GroupCode='$new_group_code' ";
                    $rows = $this->returnResultSet($sql);
                    $target_group_id = $rows[0]["GroupID"];
                }
                
                // 4a. if the user already in the same group, do nothing;
                if ($target_group_id == $previous_group_id) {} else {
                    // 4b. else, remove him/her from that group and then join the "new" group
                    $sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($target_group_id, $user_id, now(), now())";
                    $this->db_db_query($sql);
                    
                    $sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID in ($previous_group_id) AND UserID='$user_id' ";
                    $this->db_db_query($sql);
                }
            } else {
                // remove the user from any alumni year group
                $sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID in ($previous_group_id) AND UserID='$user_id' ";
                $this->db_db_query($sql);
            }
        }

        function returnStudentClassNameClassNumber($student_id)
        {
            $sql = "select ClassName, ClassNumber from INTRANET_USER where UserID=$student_id";
            $result = $this->returnArray($sql);
            return $result[0];
        }

        function returnNextUserId()
        {
            global $intranet_db;
            
            $sql = "Select MAX(UserID) as MaxUserID from INTRANET_USER";
            $intranetUserAry = $this->returnResultSet($sql);
            $intranetUserMaxUserId = ($intranetUserAry[0]['MaxUserID']) ? $intranetUserAry[0]['MaxUserID'] : 0;
            
            $sql = "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'INTRANET_USER' and TABLE_SCHEMA = '" . $intranet_db . "'";
            $intranetUserSchemaAry = $this->returnResultSet($sql);
            $intranetUserAutoIncrementVal = ($intranetUserSchemaAry[0]['AUTO_INCREMENT']) ? $intranetUserSchemaAry[0]['AUTO_INCREMENT'] : 0;
            
            $sql = "Select MAX(UserID) as MaxUserID from INTRANET_ARCHIVE_USER";
            $archiveUserAry = $this->returnResultSet($sql);
            $archiveUserMaxUserId = ($archiveUserAry[0]['MaxUserID']) ? $archiveUserAry[0]['MaxUserID'] : 0;
            
            $sql = "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'INTRANET_ARCHIVE_USER' and TABLE_SCHEMA = '" . $intranet_db . "'";
            $archiveUserSchemaAry = $this->returnResultSet($sql);
            $archiveUserAutoIncrementVal = ($archiveUserSchemaAry[0]['AUTO_INCREMENT']) ? $archiveUserSchemaAry[0]['AUTO_INCREMENT'] : 0;
            
            $nextUserID = max($intranetUserMaxUserId + 1, $archiveUserMaxUserId + 1, $intranetUserAutoIncrementVal, $archiveUserAutoIncrementVal);
            return $nextUserID;
        }

        function returnDefaultOfficialPhotoPath()
        {
            return '/images/myaccount_personalinfo/samplephoto_official.png';
        }

        function returnDefaultPersonalPhotoPath()
        {
            return '/images/myaccount_personalinfo/samplephoto_personal.png';
        }
        
        function insertAccountLog($targetID, $Action, $IPAdress = ''){
            
            
            $tableName = 'INTRANET_LOG_ACCOUNT';
            
            if($IPAddress==''){
                $IPAddress = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
            }
            if($Action == 'U' || $Action == 'D'){
                $sql = "SELECT EnglishName, ChineseName FROM INTRANET_USER WHERE UserID = '$targetID'";
                $user = $this->returnArray($sql);
                $nameCol = ',EnglishName, ChineseName';
                $user = $user[0];
                $nameValue = ",'$user[EnglishName]', '$user[ChineseName]'";
                $sql = "UPDATE $tableName
                        SET EnglishName = '$user[EnglishName]',
                            ChineseName = '$user[ChineseName]'
                        WHERE UserID = '$targetID'";
                $this->db_db_query($sql);
            }
            $col = 'UserID, Action, LogDate, LogTime, LogBy, IPAddress' .  $nameCol;
            $values = "'$targetID', '$Action', NOW(), NOW(), '$_SESSION[UserID]', '$IPAddress'";
            $sql = "INSERT INTO $tableName ($col) VALUES ($values $nameValue)";
            $result = array();
            $result['result'] = $this->db_db_query($sql);
            $result['id'] = $this->db_insert_id();
            return $result;
        }
        
        function getAccountLog($optionsArr = array(), $paging = array(), $sortArr = array()){
            
            $tableName = 'INTRANET_LOG_ACCOUNT la';
            $tableCol = 'la.LogID, la.UserID, la.Action, la.LogDate, la.LogTime, la.IPAddress, iu.Userlogin';
            
            $joinUser = 'INTRANET_USER iu';
            $joinUserCond = 'iu.UserID = la.UserID';
            $joinUserName = 'IF(iu.UserID IS NULL, la.EnglishName, iu.EnglishName) as EnglishName, IF(iu.UserID IS NULL, la.ChineseName, iu.ChineseName) as ChineseName';
            
            $joinUser2 = 'INTRANET_USER iu2';
            $joinUser2Cond = 'iu2.UserID = la.LogBy';
            $joinUser2Name = 'iu2.EnglishName as LogEnglishName, iu2.ChineseName as LogChineseName, iu2.UserLogin as LogBy';
            
            $joinUser3 = 'INTRANET_ARCHIVE_USER iau';
            $joinUser3Cond = 'iau.UserID = la.UserID';
            $joinUser3Name = 'IF(iu.UserLogin IS NULL, iau.UserLogin, iu.UserLogin) as UserLogin';
            
            if($optionsArr['name']){
                $fil_name = $optionsArr['name'];
                $nameCond = "(la.ChineseName LIKE '%$fil_name%' OR
                              la.EnglishName LIKE '%$fil_name%' OR
                              iu.ChineseName LIKE '%$fil_name%' OR
                              iu.EnglishName LIKE '%$fil_name%' OR
                              iu2.ChineseName LIKE '%$fil_name%' OR
                              iu2.EnglishName LIKE '%$fil_name%'
                            )";
            } else {
                $nameCond = '1';
            }
            if($optionsArr['action']){
                $typeCond = "(la.Action = '$optionsArr[action]')";
            } else {
                $typeCond = '1';
            }
            if($optionsArr['DateStart'] && $optionsArr['DateEnd']){
                $dateCond = "(la.LogDate BETWEEN '$optionsArr[DateStart]' AND '$optionsArr[DateEnd]')";
            } else {
                $dateCond = '1';
            }
            if(empty($paging)){
                $paging['pageNo'] = 1;
                $paging['pageSize'] = 50;
            }
            if(!$paging['all']){
                $pn = $paging['pageNo'];
                $ps = $paging['pageSize'];
                $limitCond = 'LIMIT ' . (($pn-1) * $ps ) . ', ' . $ps;
            } else {
                $limitCond = '';
            }
            if($sortArr['Field'] != ''){
                $sortOrder = ($sortArr['Order']== 0) ? 'asc' : 'desc';
                $sortCond = "ORDER BY $sortArr[Field] $sortOrder";
            } else {
                $sortCond = "ORDER BY la.LogDate DESC, la.LogTime DESC";
            }
            
            $col = "$tableCol, $joinUserName, $joinUser2Name, $joinUser3Name";
            $sql = "SELECT $col
                    FROM $tableName
                    LEFT OUTER JOIN $joinUser
                    ON $joinUserCond
                    INNER JOIN $joinUser2
                    ON $joinUser2Cond
                    LEFT OUTER JOIN $joinUser3
                    ON $joinUser3Cond
                    WHERE 1 
                    AND $nameCond
                    AND $typeCond
                    AND $dateCond
                    $sortCond
                    $limitCond                    
                    ";
            $result = $this->returnArray($sql);
            $sql = "SELECT COUNT(*)FROM $tableName
                    LEFT OUTER JOIN $joinUser
                    ON $joinUserCond
                    INNER JOIN $joinUser2
                    ON $joinUser2Cond
                    WHERE 1 
                    AND $nameCond
                    AND $typeCond
                    AND $dateCond";
            $rs = $this->returnArray($sql);
            $result['PAGEMAX'] = $rs[0][0];
            return $result;
        }
    }
} // End of directives
?>