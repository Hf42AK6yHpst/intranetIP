<?php
// editing by 
/*
 * A lib of mixture of UI, DB and File manipulation
 * -------------------------- Changes ----------------------------
 * 2016-03-03 (Carlos): Added Get_Can_Manage_Groups(), modified Get_DA_Group_Selection() and Get_Module_Files_Group_Folder_List_TB_Form(), 
 * 						only let user archive files to groups with management rights (either module admin, or group admin with manage own file right, or member with manage own file right).
 * 2014-02-18 (Tiffany):Add get_DA_for_DR() function.
 * 2013-03-05 (Carlos): Modified Get_Group_Folder_Selection_Folder_Hierarchy() as DA has changed the folder hierarchy approach
 */

include_once("libfilesystem.php");
include_once("libdigitalarchive.php");
include_once("libinterface.php");
include_once($intranet_root."/lang/digitalarchive_lang.$intranet_session_language.php");

class libdigitalarchive_moduleupload extends interface_html
{
	var $libdigitalarchive;
	var $libfilesystem;
	var $TBHeight;
	var $TBWidth;
	var $isPhysicalFileMode;
	var $isThickboxMode;
	var $cancelBtn;
	var $batchFileNameDefaultFormat;
	var $batchFileNameDefaultFormatDisplay;
	var $batchFileNameDefaultPrefix;
	var $defaultTagAry;
	
	function libdigitalarchive_moduleupload()
	{
		$this->libdigitalarchive = 0;
		$this->libfilesystem = 0;
		$this->TBHeight = "520";
		$this->TBWidth = "600";
		$this->isPhysicalFileMode = true;
		$this->isThickboxMode = true;
		$this->Reset_Default_Tag();
	}
	
	function Set_Is_Physical_File_Mode($val) {
		$this->isPhysicalFileMode = $val;
	}
	function Get_Is_Physical_File_Mode() {
		return $this->isPhysicalFileMode;
	}
	
	function Set_Is_Thickbox_Mode($val) {
		$this->isThickboxMode = $val;
	}
	function Get_Is_Thickbox_Mode() {
		return $this->isThickboxMode;
	}
	
	function Set_Cancel_Button($val) {
		$this->cancelBtn = $val;
	}
	function Get_Cancel_Button() {
		return $this->cancelBtn;
	}
	
	function Set_Batch_File_Name_Default_Format($val) {
		$this->batchFileNameDefaultFormat = $val;
	}
	function Get_Batch_File_Name_Default_Format() {
		return $this->batchFileNameDefaultFormat;
	}
	
	function Set_Batch_File_Name_Default_Format_Display($val) {
		$this->batchFileNameDefaultFormatDisplay = $val;
	}
	function Get_Batch_File_Name_Default_Format_Display() {
		return $this->batchFileNameDefaultFormatDisplay;
	}
	
	function Set_Batch_File_Name_Default_Prefix($val) {
		$this->batchFileNameDefaultPrefix = $val;
	}
	function Get_Batch_File_Name_Default_Prefix() {
		return $this->batchFileNameDefaultPrefix;
	}
	
	function Add_Default_Tag($val) {
		$this->defaultTagAry[] = $val;
	}
	function Reset_Default_Tag() {
		$this->defaultTagAry = array();
	}
	function Get_Default_Tag_Text() {
		return implode(', ', (array)$this->defaultTagAry);
	}
	
	function Get_libdigitalarchive()
	{
		if(!$this->libdigitalarchive){
			$this->libdigitalarchive = new libdigitalarchive();
		}
		return $this->libdigitalarchive;
	}
	
	function Get_libfilesystem()
	{
		if(!$this->libfilesystem){
			$this->libfilesystem = new libfilesystem();
		}
		return $this->libfilesystem;
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$x = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>';
		$x.= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
		$x.= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.scrollTo-min.js"></script>';
		$x.= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
		//$x.= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/content_25.css" type="text/css" media="screen" />';
		$x.= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/content_30.css" type="text/css" media="screen" />';
		
		return $x;
	}
	
	function User_Can_Archive_File()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $plugin;
		
		$result = false;
		if($plugin['digital_archive'] && $_SESSION['UserType'] == USERTYPE_STAFF) {
			$lda = $this->Get_libdigitalarchive();
			if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList())==0) {
				$result = false;
			}else{
				$result = true;
			}
		}
		return $result;
	}
	
	function Get_Can_Manage_Groups()
	{
		global $intranet_root;
		
		$libda = $this->Get_libdigitalarchive();
		$myGroupList = $libda->GetMyGroup();
		$numOfGroup = count($myGroupList);
		$returnGroupList = array();
		
		for($i=0;$i<$numOfGroup;$i++){
			$groupID = $myGroupList[$i][0];
			$groupTitle = $myGroupList[$i][1];
			
			if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']
				 || ($libda->IsAdminInGroup[$groupID] && $libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupID."-ADMIN_OWN-MANAGE")) 
				 || $libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupID."-MEMBER_OWN-MANAGE"))
			{
				$returnGroupList[] = $myGroupList[$i];
			}
		}
		
		return $returnGroupList;
	}
	
	function Get_DA_Group_Selection($TargetUserID, $SelectedGroupID='', $GroupDataList=array())
	{
		global $Lang, $LAYOUT_SKIN, $intranet_root, $image_path;
		
		if(count($GroupDataList)>0) {
			$myGroupList = $GroupDataList;
		}else{
			$libda = $this->Get_libdigitalarchive();
			//$myGroupList = $libda->GetMyGroup();
			$myGroupList = $this->Get_Can_Manage_Groups();
		}
		$numOfGroup = count($myGroupList);
		
		$x = '<select id="TargetGroupID" name="TargetGroupID" onchange="js_DA_Get_Group_Folder_Selection_Table(0);">'."\n";
		for($i=0;$i<$numOfGroup;$i++){
			$groupID = $myGroupList[$i][0];
			$groupTitle = $myGroupList[$i][1];
			$selected = $SelectedGroupID == $groupID? 'selected="selected"':'';
			$x .= '<option value="'.$groupID.'" '.$selected.'>'.intranet_htmlspecialchars($groupTitle).'</option>'."\n";
		}
		$x.= '</select>'."\n";
		
		return $x;
	}
	
	function Get_Group_Folder_Selection_Table($TargetUserID,$TargetGroupID,$SelectedDocumentID=0)
	{
		global $Lang, $LAYOUT_SKIN, $intranet_root, $image_path;
		
		$libda = $this->Get_libdigitalarchive();
		
		$myGroup = $libda->getGroupInfo($TargetGroupID);
		
		$x = '';
		
		$groupID = $TargetGroupID;
		$groupTitle = $myGroup['GroupTitle'];
		$indentLevel = 1;
		
		$can_manage_group = false;
		if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] 
			|| ($libda->IsAdminInGroup[$groupID] && $libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupID."-ADMIN_OWN-MANAGE"))
			|| $libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$groupID."-MEMBER_OWN-MANAGE"))
		{
			$can_manage_group = true;
		}
		
		$groupSelection = $this->Get_DA_Group_Selection($TargetUserID, $TargetGroupID);
		
		$x .= '<table class="common_table_list_v30">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>';
					$x .= '<th colspan="3">'.$groupSelection.'</th>';
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
			if($can_manage_group)
			{
				$x .= '<tr>'."\n";
					$x .= '<td style="width:80%">'."\n";
						$x .= '/';
					$x .= '</td>'."\n";
					$x .= '<td style="width:10%">'."\n";
						$x .= '<span class="table_row_tool">'."\n";
							$new_folder_title = str_replace("<!--FOLDER-->","/",$Lang['DigitalArchiveModuleUpload']['CreateNewFolder']);
							$x .= '<a class="add_dim" title="'.$new_folder_title.'" href="javascript:void(0);" onclick="js_DA_Insert_New_Folder_Row(this);"></a>'."\n";
						$x .= '</span>'."\n";
					$x .= '</td>'."\n";
					$x .= '<td style="width:10%">'."\n";
						$x .= '<input type="radio" name="DocumentID[]" value="0" '.($SelectedDocumentID==0?'checked':'').' />'."\n";
						$x .= '<input type="hidden" name="GroupID[]" id="GroupID['.$groupID.']" value="'.$groupID.'" />'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				$hierarchy = $libda->Get_Folder_Hierarchy_Array($groupID);
				$x .= $this->Get_Group_Folder_Selection_Folder_Hierarchy($hierarchy, $SelectedDocumentID, $indentLevel);
			}else{
				$x .= '<tr><td colspan="3" style="text-align:center">'.$Lang['DigitalArchive']['NoAvailableGroup'].'</td></tr>';
			}
		$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br />'."\n";
		
		return $x;
	}
	
	/*
	function Get_Group_Folder_Selection_Table($TargetUserID)
	{
		global $Lang, $LAYOUT_SKIN, $intranet_root, $image_path;
		
		$libda = $this->Get_libdigitalarchive();
		
		//$myGroupList = $libda->GetMyGroupList();
		$myGroupList = $libda->GetMyGroup();
		$numOfGroup = count($myGroupList);
		
		$x = '';
		
		for($i=0;$i<$numOfGroup;$i++) {
			$groupID = $myGroupList[$i][0];
			$groupTitle = $myGroupList[$i][1];
			$indentLevel = 1;
			
			$x .= '<table class="common_table_list_v30">'."\n";
				$x .= '<thead>'."\n";
					$x .= '<tr>';
						$x .= '<th colspan="3">'.$groupTitle.'</th>';
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				$x .= '<tbody>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td style="width:80%">'."\n";
							$x .= '/';
						$x .= '</td>'."\n";
						$x .= '<td style="width:10%">'."\n";
							$x .= '<span class="table_row_tool">'."\n";
								$new_folder_title = str_replace("<!--FOLDER-->","/",$Lang['DigitalArchiveModuleUpload']['CreateNewFolder']);
								$x .= '<a class="add_dim" title="'.$new_folder_title.'" href="javascript:void(0);" onclick="js_DA_Insert_New_Folder_Row(this);"></a>'."\n";
							$x .= '</span>'."\n";
						$x .= '</td>'."\n";
						$x .= '<td style="width:10%">'."\n";
							$x .= '<input type="radio" name="DocumentID[]" value="0" '.($i==0?'checked':'').' />'."\n";
							$x .= '<input type="hidden" name="GroupID[]" id="GroupID['.$groupID.']" value="'.$groupID.'" />'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
			$hierarchy = $libda->Get_Folder_Hierarchy_Array($groupID);
			
			$x .= $this->Get_Group_Folder_Selection_Folder_Hierarchy($hierarchy, $indentLevel);
			
			$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br />'."\n";
		}
		
		return $x;
	}
	*/
	function Get_Group_Folder_Selection_Folder_Hierarchy($array, $SelectedDocumentID=0, $indentLevel=0,$padSymbol='&nbsp;')
	{
		global $Lang;
		
		$x = '';
		if(count($array)>0){
			$indentLevel *= 2;
			foreach($array as $documentID => $subArray){
				if(is_array($subArray[0]) && count($subArray)>0) {
					for($i=0;$i<count($subArray);$i++){
						$x .= $this->Get_Group_Folder_Selection_Folder_Hierarchy($subArray[$i],$SelectedDocumentID,$indentLevel);
					}
				}else {
				
					$folderTitle = $subArray[0];
					//$subfolderArray = $subArray[1];
					$indentStr = '';
					for($i=0;$i<$indentLevel;$i++){
						$indentStr .= $padSymbol;
					}
					
					$x .= '<tr>'."\n";
						$x .= '<td style="width:80%">'."\n";
							$x .= $indentStr.$folderTitle;
						$x .= '</td>'."\n";
						$x .= '<td style="width:10%">'."\n";
								$x .= '<span class="table_row_tool">'."\n";
									$new_folder_title = str_replace("<!--FOLDER-->",$folderTitle,$Lang['DigitalArchiveModuleUpload']['CreateNewFolder']);
									$x .= '<a class="add_dim" title="'.$new_folder_title.'" href="javascript:void(0);" onclick="js_DA_Insert_New_Folder_Row(this);"></a>'."\n";
								$x .= '</span>'."\n";
							$x .= '</td>'."\n";
						$x .= '<td style="width:10%">'."\n";
							$checked = $SelectedDocumentID == $documentID? 'checked="checked"':'';
							$x .= '<input type="radio" name="DocumentID[]" value="'.$documentID.'" '.$checked.' />'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					
					if(count($subArray)>1){
						for($i=1;$i<count($subArray);$i++) {
							$x .= $this->Get_Group_Folder_Selection_Folder_Hierarchy($subArray[$i],$SelectedDocumentID,$indentLevel);
						}
					}
				}
			}
		}
		
		return $x;
	}
	
	function Get_Module_Files_List_Table($ModuleTmpFolder)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;
		
		$isPhysicalFileMode = $this->Get_Is_Physical_File_Mode();
		
		$libfilesystem = $this->Get_libfilesystem();
		clearstatcache();
		$files = $libfilesystem->return_folderlist($ModuleTmpFolder);
		
		$x .= '<table class="common_table_list_v30">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['DigitalArchiveModuleUpload']['FileName'].'</th>'."\n";
					if ($isPhysicalFileMode) {
						$x .= '<th>'.$Lang['DigitalArchiveModuleUpload']['FileSize'].'</th>'."\n";
						$x .= '<th width="1"><input type="checkbox" onclick="setChecked((this.checked?1:0),document.getElementById(\'TBForm\'),\'Files[]\');" name="checkmaster" id="checkmaster" /></th>'."\n";
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
		//clearstatcache();
		if ($isPhysicalFileMode) {
			for($i=0;$i<count($files);$i++) {
				if($files[$i] != '.' && $files[$i] != '..'){
					$base_name = $libfilesystem->get_file_basename($files[$i]);
					$target_e = getEncryptedText($files[$i]);
					$file_size = filesize($files[$i]);
					
					if($file_size > 1024*1024){
						$file_size = sprintf("%.2f MB",$file_size / 1024 / 1024);
					}else if($file_size > 1024){
						$file_size = sprintf("%.2f KB",$file_size / 1024);
					}else{
						$file_size = $file_size." Bytes";
					}
					$x .= '<tr>'."\n";
						$x .= '<td>';
							$x .= '<div id="FileLinkDiv'.$i.'">';
								$x .= '<a id="FileLink'.$i.'" href="/home/download_attachment.php?target_e='.$target_e.'" style="float:left;">'.$base_name.'</a>';
								$x .= '<span class="table_row_tool">'."\n";
									$x .= '<a class="edit_dim" onclick="js_DA_ShowHideRenameInput('.$i.');" href="javascript:void(0);" title="'.$Lang['DigitalArchiveModuleUpload']['RenameFile'].'"></a>'."\n";
								$x .= '</span>'."\n";
							$x .= '</div>'."\n";
							//$x .= $this->Get_Form_Warning_Msg("RenameFileWarningDiv".$i,'', "WarnMsgDiv");
							$x .= '<div class="tabletextrequire" style="display:none;" id="RenameFileWarningDiv'.$i.'"></div>';
						$x .= '</td>'."\n";
						if ($isPhysicalFileMode) {
							$x .= '<td>'.$file_size.'</td>'."\n";
							$x .= '<td><input type="checkbox" id="Files'.$i.'" name="Files[]" value="'.$target_e.'" checked="checked" /></td>'."\n";
						}
					$x .= '</tr>'."\n";
				}
			}
		}
		else {
			$batchFileNameDefaultFormat = $this->Get_Batch_File_Name_Default_Format();
			$batchFileNameDefaultFormatDisplay = $this->Get_Batch_File_Name_Default_Format_Display();
			
			$x .= '<tr>'."\n";
				$x .= '<td>';
					$x .= $this->GET_TEXTBOX_NAME('batchFileNamePrefixTb', 'batchFileNamePrefix', $this->Get_Batch_File_Name_Default_Prefix(), '', array('style' => 'width:150px;'));
					$x .= '&nbsp;'; 
					$x .= $batchFileNameDefaultFormatDisplay;
					$x .= '<div class="tabletextrequire" style="display:none;" id="batchFileNamePrefixWarningDiv"></div>';
					$x .= '<input type="hidden" id="batchFileNameFormat" name="batchFileNameFormat" value="'.$batchFileNameDefaultFormat.'" />';
				$x .= '</td>';
			$x .= '</tr>'."\n";
		}
				
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
				
		return $x;
	}
	
	function Get_Module_Files_Group_Folder_List_TB_Form($FromModule,$ModuleTmpFolder)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;
		
		$isPhysicalFileMode = $this->Get_Is_Physical_File_Mode();
		$isThickboxMode = $this->Get_Is_Thickbox_Mode();
		
		$libda = $this->Get_libdigitalarchive();
		//$myGroupList = $libda->GetMyGroup();
		$myGroupList = $this->Get_Can_Manage_Groups();
		$groupID = $myGroupList[0][0];
		
		if ($isThickboxMode) {
			$divStyleTag = 'style="height:'.($this->TBHeight - 120).'px; overflow-x:auto; overflow-y:auto;"';
			$tableClass = 'form_table_thickbox';
			$returnMessageLayer = $this->Get_Thickbox_Return_Message_Layer();
			
			$blockUiFunctionName = 'Block_Thickbox';
			$unBlockUiFunctionName = 'UnBlock_Thickbox';
			$closeThickboxFunction = 'window.tb_remove();';
		}
		else {
			$blockUiFunctionName = 'Block_Document';
			$unBlockUiFunctionName = 'UnBlock_Document';
		}
		
		
		$x = '';
		$x .= $returnMessageLayer;
		$x .= '<form id="TBForm" name="TBForm" action="" method="POST" onsubmit="js_DA_Check_TBForm();return false">'."\n";
		$x .= '<div id="TBFormDiv" '.$divStyleTag.'>'."\n";
			$x .= '<table width="100%" cellpadding="2" class="form_table_v30 '.$tableClass.'">'."\n";
				$x .= '<col class="field_title">'."\n";
				$x .= '<col class="field_c">'."\n";
				
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title" nowrap><span class="tabletextrequire">*</span>'.$Lang['DigitalArchiveModuleUpload']['SelectFiles'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= '<div id="FilesDiv">'."\n";
							$x .= $this->Get_Module_Files_List_Table($ModuleTmpFolder);
						$x .= '</div>'."\n";
						$x .= $this->Get_Form_Warning_Msg("FilesWarningDiv",$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectAtLeastOneFile'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title" nowrap>'.$Lang["DigitalArchive"]["Tag"].'</td>'."\n";
					$x .= '<td><input type="text" name="TagName" id="TagName" class="textbox" value="'.$this->Get_Default_Tag_Text().'" />'."\n";
					$x .= '<span class="tabletextremark">('.$Lang['StudentRegistry']['CommaAsSeparator'].", ".$Lang["DigitalArchive"]["Max10Words"].')</span></td>'."\n";
				$x .= '</tr>'."\n";
			
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title" nowrap><span class="tabletextrequire">*</span>'.$Lang['DigitalArchiveModuleUpload']['ToFolder'].'</td>'."\n";
					$x .= '<td>'."\n";
						//$x .= $this->Get_DA_Group_Selection($_SESSION['UserID'],$groupID);
						//$x .= '<br /><br />'."\n";
						$x .= '<div id="DAGroupFolderTableDiv">'."\n";
							$x .= $this->Get_Group_Folder_Selection_Table($_SESSION['UserID'],$groupID);
						$x .= '</div>'."\n";
					$x .= $this->Get_Form_Warning_Msg("ToFolderWarningDiv",$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectFolder'], "WarnMsgDiv");
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";	
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", 'submitArchiveBtn');
			$CancelBtn = $this->Get_Cancel_Button();
			if ($CancelBtn == '') {
				$CancelBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","js_DA_Remove_Temp_Folder(); ".$closeThickboxFunction);
			}
			$x .= $this->MandatoryField();
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$CancelBtn;
			$x .= '</div>'."\n";
			
			$x .= '<input type="hidden" name="FromModule" id="FromModule" value="'.htmlspecialchars($FromModule,ENT_QUOTES).'" />'."\n";
			$x .= '<input type="hidden" name="ModuleTmpFolder" id="ModuleTmpFolder" value="'.$ModuleTmpFolder.'" />'."\n";
			//$x .= '<iframe style="display:none;" name="TBIFrame" id="TBIFrame"></iframe>';
			
			$x .= '<div id="batchFileCheckboxDiv" style="display:none;"></div>'."\n";
		$x .= '</form>'."\n";
		
		$x .= '<script type="text/javascript" language="JavaScript">'."\n";
		$x .= 'function js_DA_Get_Selected_Files()'."\n";
		$x .= '{'."\n";
		$x .= '  var cbFiles = $(\'#TBForm input[name=Files[]]:checked\');'."\n";
		$x .= '  var checkedFiles = [];'."\n";
		$x .= '  for(var i=0;i<cbFiles.length;i++){'."\n";
		$x .= '    checkedFiles.push(cbFiles[i].value);'."\n";
		$x .= '  }'."\n";
		$x .= '  return checkedFiles;'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Check_TBForm()'."\n";
		$x .= '{'."\n";
		$x .= '  $("input#submitArchiveBtn").attr("disabled", "disabled")'."\n";
		
		$x .= '  if(typeof window.generateTempFile == \'function\') {'."\n";		
		$x .= '		generateTempFile();'."\n";
		$x .= '  }'."\n";
		
		$x .= '  var batchFilenamePrefix = js_DA_Get_Batch_Filename_Prefix();'."\n";
		$x .= '  var checkedFiles = js_DA_Get_Selected_Files();'."\n";
		$x .= '  var documentIDObj = $(\'#TBForm input[name=DocumentID[]]:checked\');'."\n";
		$x .= '  var groupID = 0;'."\n";
		$x .= '  var isValid = true;'."\n";
		
		if ($isPhysicalFileMode) {
			$x .= '  if(checkedFiles.length==0){'."\n";
			$x .= '    $(\'div#FilesWarningDiv\').show();'."\n";
			$x .= '    $("#TBForm input#checkmaster").focus();'."\n";
			$x .= '    isValid = false;'."\n";
			$x .= '  }else{'."\n";
			$x .= '    $(\'div#FilesWarningDiv\').hide();'."\n";
			$x .= '  }'."\n";
		}
		else {
			$x .= '  if (!js_DA_Check_Is_FileName_Valid(batchFilenamePrefix)) {'."\n";
			$x .= '      $(\'div#batchFileNamePrefixWarningDiv\').html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseDoNotUseForbiddenChars'].'\').show();'."\n";
			$x .= '      $(\'input#batchFileNamePrefixTb\').focus();'."\n";
			$x .= '      isValid = false;'."\n";
			$x .= '  }'."\n";
			$x .= '  else {'."\n";
			$x .= '  	$(\'div#batchFileNamePrefixWarningDiv\').html(\'\');'."\n";
			$x .= '  }'."\n";
		}
		$x .= '  if(documentIDObj.length==0){'."\n";
		$x .= '    $(\'div#ToFolderWarningDiv\').show();'."\n";
		$x .= '    $(\'#TBForm input[name=DocumentID[]]:first\').focus();'."\n";
		$x .= '    isValid = false;'."\n";
		$x .= '  }else{'."\n";
		$x .= '    groupID = documentIDObj.closest(\'table\').find(\'input[name=GroupID[]]\').val();'."\n";
		$x .= '    $(\'div#ToFolderWarningDiv\').hide();'."\n";
		$x .= '  }'."\n";
		
		$x .= '  if (isValid) {'."\n";
		$x .= '    '.$blockUiFunctionName.'();'."\n";
		$x .= '    $.post('."\n";
		$x .= '      "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '      {'."\n";
		$x .= '         "task":"Check_Is_Module_Files_Duplicated",'."\n";
		$x .= '         "ModuleTmpFolder":"'.$ModuleTmpFolder.'",'."\n";
		$x .= '         "Files[]":checkedFiles,'."\n";
		$x .= '         "GroupID":groupID,'."\n";
		$x .= '         "DocumentID":documentIDObj.val()'."\n";
		$x .= '      },'."\n";
		$x .= '      function(duplicatedFileNames){'."\n";
		$x .= '        if(duplicatedFileNames != ""){'."\n";
		$x .= '          var duplicatedNameArr = duplicatedFileNames.split("\n");';
		$x .= '          if(confirm("'.$Lang['DigitalArchiveModuleUpload']['ConfirmMsg']['ConfirmOverwriteDuplicatedFiles'].'\n" + duplicatedFileNames)){'."\n";
		$x .= '            js_DA_Upload_Files(1);'."\n";
		$x .= '          }else{'."\n";
		$x .= '            '.$unBlockUiFunctionName.'();'."\n";
		$x .= '          }'."\n";
		$x .= '        }else{'."\n";
		$x .= '            js_DA_Upload_Files(0);'."\n";
		$x .= '        }'."\n";
		$x .= '      }'."\n";
		$x .= '    );'."\n";
		$x .= '  }'."\n";
		$x .= '  $("input#submitArchiveBtn").attr("disabled", "")'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Upload_Files(updateVerionIfDuplicate)'."\n";
		$x .= '{'."\n";
		$x .= '  if(typeof window.generateBatchFile == \'function\') {'."\n";		
		$x .= '		generateBatchFile();'."\n";
		$x .= '  }'."\n";
		$x .= '  var checkedFiles = js_DA_Get_Selected_Files();'."\n";
		$x .= '  var documentIDObj = $(\'#TBForm input[name=DocumentID[]]:checked\');'."\n";
		$x .= '  var groupID = documentIDObj.closest(\'table\').find(\'input[name=GroupID[]]\').val();'."\n";
		$x .= '  $.post('."\n";
		$x .= '    "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '    {'."\n";
		$x .= '       "task":"Upload_Module_Files_To_Admin_Doc",'."\n";
		$x .= '       "FromModule":encodeURIComponent("'.$FromModule.'"),'."\n";
		$x .= '       "ModuleTmpFolder":"'.$ModuleTmpFolder.'",'."\n";
		$x .= '       "Files[]":checkedFiles,'."\n";
		$x .= '       "TagName":encodeURIComponent($.trim($(\'input#TagName\').val())),'."\n";
		$x .= '       "GroupID":groupID,'."\n";
		$x .= '       "DocumentID":documentIDObj.val(),'."\n";
		$x .= '       "IsDuplicateFlag":updateVerionIfDuplicate'."\n";
		$x .= '    },'."\n";
		$x .= '    function(data){'."\n";
		$x .= '       '.$unBlockUiFunctionName.'();'."\n";
		$x .= '       '.$closeThickboxFunction."\n";
		$x .= '       Get_Return_Message(data);'."\n";
		$x .= '       window.Scroll_To_Top();'."\n";
		$x .= '    });'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Remove_Temp_Folder()'."\n";
		$x .= '{'."\n";
		$x .= '  $.post('."\n";
		$x .= '    "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '    {'."\n";
		$x .= '      "task":"Remove_Temp_Folder",'."\n";
		$x .= '      "ModuleTmpFolder":encodeURIComponent($("#TBForm input#ModuleTmpFolder").val())'."\n";
		$x .= '    },'."\n";
		$x .= '    function(data){}'."\n";
		$x .= '  );'."\n";
		$x .= '}'."\n";
		
		$x .= 'function js_DA_Insert_New_Folder_Row(obj)'."\n";
		$x .= '{'."\n";
		$x .= '  if($(\'#NewFolderRow\').length > 0){'."\n";
		$x .= '    $(\'#NewFolderRow\').remove();'."\n";
		$x .= '  }'."\n";
		$x .= '  var parentFolderRow = $(obj).closest("tr");'."\n";
		$x .= '  var rowTemplate = \''.str_replace("'","\'",$this->Get_New_Folder_Table_Row_Template()).'\';'."\n";
		$x .= '  parentFolderRow.after(rowTemplate);'."\n";
		$x .= '  $("input#NewFolderName").focus();'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Cancel_Create_New_Folder()'."\n";
		$x .= '{'."\n";
		$x .= '   $(\'#NewFolderRow\').remove();'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Update_Create_New_Folder()'."\n";
		$x .= '{'."\n";
		$x .= '  var newFolderName = $.trim($(\'input#NewFolderName\').val());'."\n";
		$x .= '  var isValid = true;'."\n";
		$x .= '  if(newFolderName == ""){'."\n";
		$x .= '    $("#NewFolderRowWarningDiv").html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseInputFolderName'].'\').show();'."\n";
		$x .= '    $("input#NewFolderName").focus();'."\n";
		$x .= '    isValid = false;'."\n";
		$x .= '  }else{'."\n";
		$x .= '    $("#NewFolderRowWarningDiv").html(\'\').hide();'."\n";
		$x .= '  }'."\n";
		$x .= '  if(isValid){'."\n";
		$x .= '  var folderID = $("tr#NewFolderRow").prev("tr").find(\'input[name=DocumentID[]]\').val();'."\n";
		$x .= '  var groupID = $("tr#NewFolderRow").closest(\'table\').find(\'input[name=GroupID[]]\').val();'."\n";
		$x .= '  '.$blockUiFunctionName.'();'."\n";
		$x .= '  $.post('."\n";
		$x .= '    "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '    {'."\n";
		$x .= '      "task":"Check_Is_Folder_Name_Duplicated",'."\n";
		$x .= '      "FolderName":encodeURIComponent(newFolderName),'."\n";
		$x .= '      "GroupID":groupID,'."\n";
		$x .= '      "ParentFolderID":folderID'."\n";
		$x .= '    },'."\n";
		$x .= '    function(data){'."\n";
		$x .= '       if(data=="1"){'."\n";
		$x .= '         $("#NewFolderRowWarningDiv").html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['FolderExists'].'\').show();'."\n";
		$x .= '         $("input#NewFolderName").focus();'."\n";
		$x .= '         '.$unBlockUiFunctionName.'();'."\n";
		$x .= '       }else{'."\n";
		$x .= '         $("#NewFolderRowWarningDiv").html(\'\').hide();'."\n";
		$x .= '         $.post('."\n";
		$x .= '           "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '           {'."\n";
		$x .= '             "task":"Create_New_Admin_Folder",'."\n";
		$x .= '             "FolderName":encodeURIComponent(newFolderName),'."\n";
		$x .= '             "GroupID":groupID,'."\n";
		$x .= '             "ParentFolderID":folderID'."\n";
		$x .= '           },'."\n";
		$x .= '           function(newFolderID){'."\n";
		$x .= '              if(isInteger(newFolderID)){'."\n";
		$x .= '                Get_Return_Message(\''.$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['CreateFolderSuccess'].'\');'."\n";
		$x .= '                js_DA_Get_Group_Folder_Selection_Table(newFolderID);'."\n";
		$x .= '              }else{'."\n";
		$x .= '                Get_Return_Message(\''.$Lang['DigitalArchiveModuleUpload']['ReturnMsg']['CreateFolderUnsuccess'].'\');'."\n";
		$x .= '                js_DA_Get_Group_Folder_Selection_Table(0);'."\n";
		$x .= '              }'."\n";
		$x .= '           }'."\n";
		$x .= '         )'."\n";
		$x .= '       }'."\n";
		$x .= '    }'."\n";
		$x .= '  );'."\n";
		$x .= '  }'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Get_Group_Folder_Selection_Table(selectedDocumentID)'."\n";
		$x .= '{'."\n";
		$x .= '  '.$blockUiFunctionName.'();'."\n";
		$x .= '  if(selectedDocumentID != 0 && typeof(selectedDocumentID)=="undefined")'."\n";
		$x .= '    selectedDocumentID = $(\'#TBForm input[name=DocumentID[]]:checked\').val();'."\n";
		$x .= '  $("div#DAGroupFolderTableDiv").load('."\n";
		$x .= '    "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '    {'."\n";
		$x .= '      "task":"Get_Group_Folder_Selection_Table",'."\n";
		$x .= '      "TargetGroupID":$("#TBForm select#TargetGroupID").val(),'."\n";
		$x .= '      "SelectedDocumentID":selectedDocumentID'."\n";
		$x .= '    },'."\n";
		$x .= '    function(data){'."\n";
		$x .= '       '.$unBlockUiFunctionName.'();'."\n";
		$x .= '    }'."\n";
		$x .= '  );'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Get_Module_Files_List_Table()'."\n";
		$x .= '{'."\n";
		$x .= '  Block_Element("FilesDiv");'."\n";
		$x .= '  $("#TBForm div#FilesDiv").load('."\n";
		$x .= '    "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '    {'."\n";
		$x .= '      "task":"Get_Module_Files_List_Table",'."\n";
		$x .= '      "ModuleTmpFolder":encodeURIComponent($("#TBForm input#ModuleTmpFolder").val())'."\n";
		$x .= '    },'."\n";
		$x .= '    function(data){'."\n";
		$x .= '      UnBlock_Element("FilesDiv");'."\n";
		$x .= '    }'."\n";
		$x .= '  );'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_ShowHideRenameInput(index)'."\n";
		$x .= '{'."\n";
		$x .= '  var inputToolTemplate = \''.str_replace("'","\'",$this->Get_Rename_File_Input_Tool_Template()).'\';'."\n";
		$x .= '  inputToolTemplate = inputToolTemplate.replace(/{##INDEX##}/g,index);'."\n";
		$x .= '  var fileLinkDiv = $("#TBForm div#FileLinkDiv"+index);'."\n";
		$x .= '  var fileLink = $("#TBForm a#FileLink"+index);'."\n";
		$x .= '  fileLinkDiv.hide();'."\n";
		$x .= '  fileLinkDiv.before(inputToolTemplate);'."\n";
		$x .= '  var fileName = fileLink.text();'."\n";
		$x .= '  var fileNameNoExt = fileName.substr(0,fileName.lastIndexOf("."));';
		$x .= '  $("#TBForm input#NewFileName"+index).val(fileNameNoExt);'."\n";
		$x .= '  $("#TBForm input#TmpOldFileName"+index).val(fileName);'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Cancel_Rename_File(index)'."\n";
		$x .= '{'."\n";
		$x .= '  $("#TBForm div#RenameFileDiv"+index).remove();'."\n";
		$x .= '  $("#TBForm div#FileLinkDiv"+index).show();'."\n";
		$x .= '  $("div#RenameFileWarningDiv" + index).html("").hide();'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Update_Rename_File(index)'."\n";
		$x .= '{'."\n";
//		$x .= '  var forbiddenChars = ["\\\\","/","<",">","?",":","*","\\"","|"];'."\n";
		$x .= '  var newFileName = $.trim($("#TBForm input#NewFileName"+index).val());'."\n";
		$x .= '  var tmpOldFileName = $("#TBForm input#TmpOldFileName"+index).val();'."\n";
		$x .= '  var tmpOldFileNameNoExt = tmpOldFileName.substr(0,tmpOldFileName.lastIndexOf("."));';
		$x .= '  var tmpFileExt = tmpOldFileName.substr(tmpOldFileName.lastIndexOf("."));';
		$x .= '  var isValid = true;'."\n";
		$x .= '  var warnDiv = $("div#RenameFileWarningDiv" + index);'."\n";
		$x .= '  warnDiv.html("").hide();'."\n";
		$x .= '  if(newFileName == ""){'."\n";
		$x .= '    warnDiv.html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseInputNewFileName'].'\').show();'."\n";
		$x .= '    isValid = false;'."\n";
		$x .= '    return false;'."\n";
		$x .= '  }'."\n";
//		$x .= '  for(var i=0;i<forbiddenChars.length;i++){'."\n";
//		$x .= '    if(newFileName.indexOf(forbiddenChars[i]) != -1){'."\n";
//		$x .= '      warnDiv.html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseDoNotUseForbiddenChars'].'\').show();'."\n";
//		$x .= '      isValid = false;'."\n";
//		$x .= '      break;'."\n";
//		$x .= '    }'."\n";
//		$x .= '  }'."\n";
		//$x .= '  var regExp=/^.+\.[^\.\s]+$/;'."\n";
		//$x .= '  if(!regExp.test(newFileName)){'."\n";
		//$x .= '      warnDiv.html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['FileNameMustHaveExtension'].'\').show();'."\n";
		//$x .= '      isValid = false;'."\n";
		//$x .= '  }'."\n";
		$x .= '  if (!js_DA_Check_Is_FileName_Valid(newFileName)) {'."\n";
		$x .= '      warnDiv.html(\''.$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseDoNotUseForbiddenChars'].'\').show();'."\n";
		$x .= '      isValid = false;'."\n";
		$x .= '  }'."\n";
		$x .= '  if(newFileName == tmpOldFileNameNoExt){'."\n";
		$x .= '    js_DA_Cancel_Rename_File(index);'."\n";
		$x .= '    isValid = false;'."\n";
		$x .= '  }'."\n";
		$x .= '  if(isValid) {'."\n";
		$x .= '    Block_Element("FilesDiv");'."\n";
		$x .= '    $.post('."\n";
		$x .= '      "/home/eAdmin/ResourcesMgmt/DigitalArchive/module_upload/ajax_task.php",'."\n";
		$x .= '      {'."\n";
		$x .= '        "task":"Rename_Temp_File",'."\n";
		$x .= '        "newFileName":encodeURIComponent(newFileName + tmpFileExt),'."\n";
		$x .= '        "oldFilePath":$("#TBForm input#Files"+index).val()'."\n";
		$x .= '      },'."\n";
		$x .= '      function(data){'."\n";
		$x .= '        Get_Return_Message(data);'."\n";
		$x .= '        js_DA_Get_Module_Files_List_Table();'."\n";
		$x .= '      }'."\n";
		$x .= '    );'."\n";
		$x .= '  }'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Check_Is_FileName_Valid(fileName)'."\n";
		$x .= '{'."\n";
		$x .= '  var forbiddenChars = ["\\\\","/","<",">","?",":","*","\\"","|"];'."\n";
		$x .= '  var isValid = true;'."\n";
		$x .= '  for(var i=0;i<forbiddenChars.length;i++){'."\n";
		$x .= '    if(fileName.indexOf(forbiddenChars[i]) != -1){'."\n";
		$x .= '      isValid = false;'."\n";
		$x .= '      break;'."\n";
		$x .= '    }'."\n";
		$x .= '  }'."\n";
		$x .= '  return isValid'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Get_Batch_Filename_Format()'."\n";
		$x .= '{'."\n";
		$x .= '		return $("#batchFileNameFormat").val();'."\n";
		$x .= '}'."\n\n";
		
		$x .= 'function js_DA_Get_Batch_Filename_Prefix()'."\n";
		$x .= '{'."\n";
		$x .= '		return $("#batchFileNamePrefixTb").val();'."\n";
		$x .= '}'."\n\n";
		
		$x .= '$("#TB_ajaxContent").css("overflow","hidden");'."\n";
		$x .= '$("#TB_closeWindowButton").unbind("click").bind("click",function(){'."\n";
		$x .= '  js_DA_Remove_Temp_Folder();'."\n";
		$x .= '  '.$closeThickboxFunction."\n";
		$x .= '});'."\n";
		$x .= '</script>'."\n";
		
		return $x;
	}
	
	function Get_New_Folder_Table_Row_Template()
	{
		global $Lang;
		
		// insert to js string, should not have line break \n
		$x = '<tr id="NewFolderRow">';
			$x.= '<td colspan="3">';
				$x .= $this->GET_TEXTBOX_NAME('NewFolderName','NewFolderName','','',array('maxlength'=>255));
				$x .= '&nbsp;';
				$x .= $this->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "js_DA_Update_Create_New_Folder();", "NewFolderSubmitBtn");
				$x .= '&nbsp;';
				$x .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_DA_Cancel_Create_New_Folder();", "NewFolderCancelBtn");
				//$x .= $this->Get_Form_Warning_Msg("NewFolderRowWarningDiv",'', "WarnMsgDiv");
				$x .= '<div class="tabletextrequire" style="display:none;" id="NewFolderRowWarningDiv"></div>';
			$x.= '</td>';
		//	$x.= '<td style="width:10%">';
		//		$x.= '&nbsp;';
		//	$x.= '</td>';
		//	$x.= '<td style="width:10%">';
		//		$x.= '&nbsp;';
		//	$x.= '</td>';
		$x.= '</tr>';
		
		return $x;
	}
	
	function Get_Rename_File_Input_Tool_Template()
	{
		global $Lang;
		
		$x .= '<div id="RenameFileDiv{##INDEX##}">';
			$x .= $this->GET_TEXTBOX_NAME('NewFileName{##INDEX##}','NewFileName{##INDEX##}','','',array('maxlength'=>255));
			$x .= '&nbsp;';
			$x .= $this->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "js_DA_Update_Rename_File({##INDEX##});", "RenameFileSubmitBtn{##INDEX##}");
			$x .= '&nbsp;';
			$x .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_DA_Cancel_Rename_File({##INDEX##});", "RenameFileCancelBtn{##INDEX##}");
			$x .= '<input type="hidden" id="TmpOldFileName{##INDEX##}" name="TmpOldFileName{##INDEX##}" value="" />';
		$x .= '</div>';
		
		return $x;
	}
	
	function Check_Is_Module_Files_Duplicated($fileList,$GroupID,$DocumentID=0)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		$libda = $this->Get_libdigitalarchive();
		$libfilesystem = $this->Get_libfilesystem();
		
		$result = array();
		for($i=0;$i<count($fileList);$i++) {
			$file_title = $libda->Get_Safe_Sql_Query($libfilesystem->get_file_basename($fileList[$i]));
			$duplicated = $libda->Is_Duplicate_Admin_Document($GroupID,$DocumentID,$file_title);
			if($duplicated){
				$result[] = $file_title;
			}
		}
		
		return $result;
	}
	
	/*
	 * @param 1Darray $fileList : full paths of module files
	 * @param int $GroupID : DA Group ID
	 * @param int $DocumentID : DA Folder Record ID, 0 is root folder
	 * @return 2Darray $dataAry :
	 * 				Title
	 * 				FileFolderName,
	 * 				FileHashName,
	 * 				FileExtension,
	 * 				Description,
	 * 				VersionNo,
	 * 				IsLatestFile,
	 * 				SizeInBytes,
	 * 				ParentFolderID,
	 * 				GroupID 
	 */
	function Transform_Module_Files_To_Data_Array($fileList,$GroupID,$DocumentID=0,$FromModule='')
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;
		
		$libfilesystem = $this->Get_libfilesystem();
		$ParentFolderID = $DocumentID;
		$InputUserID = $_SESSION['UserID'];
		$timestamp = date('YmdHis');
		
		$dataAry = array();
		for($i=0;$i<count($fileList);$i++) {
			$file_name = $libfilesystem->get_file_basename($fileList[$i]);
			$file_size_bytes = filesize($fileList[$i]);
			$file_extension = strtolower(substr($libfilesystem->file_ext($fileList[$i]),1));
			$file_hashname = "u".$InputUserID."_".$timestamp.$i; // hashname is a bit different with DA's hash method
			$dataAry[] = array('Title'=>$file_name,
								'FileFolderName'=>$file_name,
								'FileHashName'=>$file_hashname,
								'FileExtension'=>$file_extension,
								'Description'=>'',
								'VersionNo'=>1,
								'IsLatestFile'=>1,
								'ParentFolderID'=>$ParentFolderID,
								'GroupID'=>$GroupID,
								'SizeInBytes'=>$file_size_bytes,
								'FromModule'=>$FromModule);
		}
		return $dataAry;
	}
	
	/* 
	 * @param string $tmpFolder : full path of tmp folder where holds the files for copying to DA
	 * @param 2Darray $dataAry :
	 * 				Title
	 * 				FileFolderName,
	 * 				FileHashName,
	 * 				FileExtension,
	 * 				Description,
	 * 				VersionNo,
	 * 				IsLatestFile,
	 * 				SizeInBytes,
	 * 				ParentFolderID,
	 * 				GroupID 
	 * @param string $tagName : comma separated tags
	 * @param bool $isDuplicateFlag : true to increase version number
	 */
	function Upload_Module_Files_To_Admin_Doc($tmpFolder,$dataAry,$tagName='',$isDuplicateFlag=false)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		$libda = $this->Get_libdigitalarchive();
		$libfilesystem = $this->Get_libfilesystem();
		
		$targetPath = "/digital_archive/";
		$targetPath_2ndLevel = "/admin_doc/";
		# create folder
	
		$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."../intranetdata/".$targetPath);
		
		if ( file_exists($target1stPath) ) {
			$resultCreatedFolder = true;
		} else {
			$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
		}
		
		$targetFullPath = str_replace('//', '/', $PATH_WRT_ROOT."../intranetdata/".$targetPath.$targetPath_2ndLevel);
		if ( file_exists($targetFullPath) ) {
			$resultCreatedFolder = true;
		} else {
			$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
		}
		
		chmod($targetFullPath, 0777);
		# end create folder;
		
		if(substr($tmpFolder,strlen($tmpFolder)-1) != '/'){
			$tmpFolder .= '/';
		}
		
		$result = array();
		$numOfFile = count($dataAry);
		for($i=0;$i<$numOfFile;$i++) {
			# step 1 - copy physical file
			$src_full_path = $tmpFolder.$dataAry[$i]['FileFolderName'];
			$dest_full_path = $targetFullPath.$dataAry[$i]['FileHashName'];
			
			$result['Copy'.$i] = $libfilesystem->file_copy($src_full_path, $dest_full_path);
			
			# step 2 - insert DB record
			if($result['Copy'.$i]){
				// escape text fields for query
				$dataAry[$i]['Title'] = $libda->Get_Safe_Sql_Query($dataAry[$i]['Title']);
				$dataAry[$i]['FileFolderName'] = $libda->Get_Safe_Sql_Query($dataAry[$i]['FileFolderName']);
				
				$GroupID = $dataAry[$i]['GroupID'];
				$FolderID = $dataAry[$i]['ParentFolderID'];
				$Title = $dataAry[$i]['Title'];
				$FileFolderName = $dataAry[$i]['FileFolderName'];
				
				$RecordID = $libda->insertAdminRecord($dataAry[$i]);
				$inHeritTag = 0;
				if($tagName != "") {
					$libda->Update_Admin_Doc_Tag_Relation($tagName, $RecordID, $libda->AdminModule);
				} else {
					$inHeritTag = 1;	
				}
				
				if($isDuplicateFlag) {
					// FileFolderName is redundant 
					$libda->Update_Duplicate_Record($RecordID, $GroupID, $FolderID, $Title, $FileFolderName, $inHeritTag);
				}
			}
		}
		
		# clean tmp folder
		//if(preg_match('/^.+\/file\/.+$/',$tmpFolder)) {
		//	$libfilesystem->folder_remove_recursive($tmpFolder);
		//}
		$this->Remove_Temp_Folder($tmpFolder);
		
		return !in_array(false,$result);
	}
	
	function Check_Is_Folder_Name_Duplicated($FolderName, $GroupID, $ParentFolderID='')
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		$libda = $this->Get_libdigitalarchive();
		
		$FolderName = $libda->Get_Safe_Sql_Query($FolderName);
		if($ParentFolderID==0){
			$ParentFolderID = '';
		}
		$record = $libda->Get_Folder_Info_By_Name($FolderName, $GroupID, $ParentFolderID);
		
		return count($record)>0; // true - already exist 
	}
	
	function Create_New_Admin_Folder($Title,$FileFolderName,$GroupID,$ParentFolderID=0)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		$libda = $this->Get_libdigitalarchive();
		$dataAry = array();
		$dataAry['Title'] = $libda->Get_Safe_Sql_Query($Title);
		$dataAry['FileFolderName'] = $libda->Get_Safe_Sql_Query($FileFolderName);
		$dataAry['GroupID'] = $GroupID;
		$dataAry['ParentFolderID'] = $ParentFolderID;
		
		$DocumentID = $libda->Create_Admin_Folder($dataAry);
		
		return $DocumentID;
	}
	
	function Rename_File($filename)
	{
		$new_filename = $filename;
		if( preg_match('/^.+\((\d+)\)\.[^\.]+$/', $filename, $matches) ){
			if(count($matches)>1){
				$num = intval($matches[count($matches)-1]);
				$new_filename = str_replace("(".$num.").","(".($num+1).").",$filename);
			}
		}else{
			$dot_pos = strrpos($filename,".",0);
			$suffix = substr($filename,$dot_pos);
			$new_filename = substr_replace($filename,"(1)".$suffix,$dot_pos);
		}
		return $new_filename;
	}
	
	function Rename_Temp_File($newFileName,$oldFilePath)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;
		
		$libfilesystem = $this->Get_libfilesystem();
		
		if(file_exists($oldFilePath)) {
			$newFilePath = substr($oldFilePath,0,strrpos($oldFilePath,"/"))."/".$newFileName; 
			$success = $libfilesystem->file_rename($oldFilePath,$newFilePath);
		}else{
			$success = false;
		}
		return $success;
	}
	
	function Remove_Temp_Folder($TmpFolderPath)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT, $intranet_root;
		
		$libfilesystem = $this->Get_libfilesystem();
		
		if(file_exists($TmpFolderPath) && preg_match('/^.+\/file\/.+$/',$TmpFolderPath)){
			$libfilesystem->folder_remove_recursive($TmpFolderPath);
		}
	}
	
	function Get_CSS_Link_Tags($html)
	{
		$link_arr = array(); // 0: <link> tag ; 1: href path
		$tmp_inner_head = $html;
		while( ($link_start = stripos($tmp_inner_head, '<link', 0)) !== FALSE )
		{
			$link_end = strpos($tmp_inner_head,'>',$link_start);
			$link_tag = substr($tmp_inner_head, $link_start, $link_end + 1 - $link_start);
			$tmp_inner_head = substr($tmp_inner_head, $link_end + 1, strlen($tmp_inner_head) - $link_end);
			if(preg_match("/href\s*=\s*['|\"]([^'\"]+)['|\"]/",$link_tag,$matches))
			{
				$link_href = $matches[1];
				$link_arr[] = array($link_tag,$link_href);
			}
		}
		return $link_arr;
	}
	
	function Get_Img_Tags($html)
	{
		$img_arr = array(); // 0: <img> tag ; 1: src path 
		$tmp_inner_html = $html;
		while( ($img_start = stripos($tmp_inner_html, '<img', 0)) !== FALSE )
		{
			$img_end = strpos($tmp_inner_html,'>',$img_start);
			$img_tag = substr($tmp_inner_html, $img_start, $img_end + 1 - $img_start);
			$tmp_inner_html = substr($tmp_inner_html, $img_end + 1, strlen($tmp_inner_html) - $img_end);
			//$img_arr[] = $img_tag;
			if(preg_match("/src\s*=\s*['|\"]([^'\"]+)['|\"]/",$img_tag,$matches))
			{
				$img_src = $matches[1];
				$img_arr[] = array($img_tag,$img_src);
			}
		}
		
		return $img_arr;
	}
	
	function Embed_Image_CSS_To_HTML($html)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		
		$libfilesystem = $this->Get_libfilesystem();
		$replaced_html = $html;
		
		$img_arr = $this->Get_Img_Tags($html);
		$link_arr = $this->Get_CSS_Link_Tags($html);
		//debug_pr($link_arr);
		for($i=0;$i<count($img_arr);$i++) {
			$img_tag_org = $img_arr[$i][0];
			$img_src_org = $img_arr[$i][1];
			
			$curDomain = curPageURL($withQueryString=0, $withPageSuffix=0).'/';
			$img_tag = str_replace($curDomain, '', $img_tag_org);
			$img_src = str_replace($curDomain, '', $img_src_org);
						
			// handle image path like "/file/reportcard2008/2009/imageSignature/1664.png?ts=1348192685"
			// to ignore the ts parameter
			$pos = strpos($img_src, '?');
			if ($pos !== false) {
				$img_src = substr($img_src, 0, strpos($img_src, '?'));
			}
			
			if($img_src[0]!='/') {
				$img_src_slashed = '/'.$img_src;
			}else{
				$img_src_slashed = $img_src;
			}
			$ext = substr($img_src, strrpos($img_src, ".")+1);
			$img_src_fullpath = $intranet_root.$img_src_slashed;
			$uri_data = 'data:image/'.$ext.';base64,';
			if(file_exists($img_src_fullpath)) {
				$base64content = base64_encode($libfilesystem->file_read($img_src_fullpath));
				$uri_data = $uri_data.$base64content;
				$replaced_html = str_replace($img_src_org,$uri_data,$replaced_html);
			}
		}
		
		for($i=0;$i<count($link_arr);$i++) {
			$link_tag = $link_arr[$i][0];
			$link_href = $link_arr[$i][1];
			//echo htmlspecialchars($link_tag,ENT_QUOTES)."<br />";
			//echo htmlspecialchars($link_href,ENT_QUOTES)."<br />";
			if($link_href[0] != '/'){
				$link_href_slashed = '/'.$link_href;
			}else{
				$link_href_slashed = $link_href;
			}
			$css_fullpath = $intranet_root.$link_href_slashed;
			//echo htmlspecialchars($css_fullpath,ENT_QUOTES)."<br />";
			if(file_exists($css_fullpath)) {
				$css = $libfilesystem->file_read($css_fullpath);
				$css = '<style type="text/css">'.$css.'</style>';
				//echo htmlspecialchars($link_tag,ENT_QUOTES)."<br />";
				$replaced_html = str_replace($link_tag,$css,$replaced_html);
			}
		}
		
		return $replaced_html;
	}
	function get_DA_for_DR($GroupID,$FolderID=""){
    
    global $_SESSION, $UserID;
    $libda = $this->Get_libdigitalarchive();
	if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {	# if not module ADMIN, then need to check access right
			if($libda->IsAdminInGroup[$GroupID]) {	# group admin
				if(!$libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-VIEW")) {	# cannot view others
					if($libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						$conds = " AND rec.InputBy='$UserID'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				}  else {		# can view others
					if($libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$UserID'";
					}					
				}
				
					
			} else {	# group member
			
				if(!$libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-VIEW")) {	# cannot view others
					if($libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						$conds = " AND rec.InputBy='$UserID'";
					} else {									# cannot view own
						$conds = " AND rec.InputBy='0'";
					}
				} else {	# can view others
					if($libda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-VIEW")) {	# can view own
						# nothing filter
					} else {									# cannot view own
						$conds = " AND rec.InputBy!='$UserID'";
					}	
				}
			}
		}
		
       $sql = "SELECT DocumentID, FileFolderName, IsFolder, ParentFolderID FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD rec WHERE ParentFolderID='$FolderID' AND GroupID='$GroupID' AND RecordStatus=1 $conds AND IsLatestFile=1 ORDER BY IsFolder,FileFolderName";
	   $result =  $libda->returnArray($sql);
	   return $result;
	}
}

?>