<?php
class libboxplot{
	var $minimum;
	var $maximum;
	var $q1;
	var $q2;
	var $q3;

	function libboxplot() {
		
	}
	
	function setMinimum($val) {
		$this->minimum = $val;
	}
	function getMinimum() {
		return $this->minimum; 
	}
	
	function setMaximum($val) {
		$this->maximum = $val;
	}
	function getMaximum() {
		return $this->maximum; 
	}
	
	function setQ1($val) {
		$this->q1 = $val;
	}
	function getQ1() {
		return $this->q1; 
	}
	
	function setQ2($val) {
		$this->q2 = $val;
	}
	function getQ2() {
		return $this->q2; 
	}
	
	function setQ3($val) {
		$this->q3 = $val;
	}
	function getQ3() {
		return $this->q3; 
	}
	
	function returnBoxPlotContentHtml($width="130px"){
		$width = $width * 0.9;  //The ratio of the title and the boxplot is 9:10
		$box='';
		$height = 7;
		$min = $this->getMinimum();
		$max = $this->getMaximum();
		$q1 = $this->getQ1();
		$q2 = $this->getQ2();
		$q3 = $this->getQ3();
		
		$minToq1 = $q1 - $min;
		$q1Toq2 =  $q2 - $q1;
		$q2Toq3 = $q3 - $q2;
		$q3Tomax = $max - $q3;
		$maxpad = 100 - $max;
		
		//Dataset with any invalid data (below 0 or greater than 100) will have no plot
		if ($min<0||$max<0||$q1<0||$q2<0||$q3<0|| $q1>100||$max>100||$q1>100||$q2>100||$q3>100){
			$box = "";
			return $box;
		}else{
			$box .= '<div align=center><table style="border:0px;"><tr><td style="border:0px;"><table width='.$width.' style="align:right;border:0px;border-collapse:collapse;"><tr>';
			if ($max == $min){
				$box .= '<td height='.($height*2).'px rowspan="2" style=" width:'.$min.'%; padding:0px; border:0px;border-right: 1px solid;"></td>
						<td height='.$height.'px rowspan="2" style="width:'.$maxpad.'%; padding:0;border:0px; border-left: 1px solid;"></td>';
			}else{
				if ($min!=0){
					$box.= '<td height='.$height.'px rowspan="2" style=" width:'.$min.'%; padding:0;border:0px;"></td>';
				}
				$box.= '<td height='.$height.'px style="width:'.$minToq1.'%; padding:0;border:0px;border-bottom: 1px solid;"></td>
					<td height='.$height.'px rowspan="2" style="width:'.$q1Toq2.'%; padding:0;border: 1px solid;"></td>
					
					<td height='.$height.'px rowspan="2" style="width:'.$q2Toq3.'%; padding:0;border: 1px solid;"></td>
					<td height='.$height.'px style="width:'.$q3Tomax.'%; padding:0;border:0px;border-bottom: 1px solid;"></td>
					<td height='.$height.'px rowspan="2" style="width:'.$maxpad .'%; padding:0;border:0px;"></td>
					</tr><tr><td height='.$height.'px style="border:0px;"></td><td height='.$height.'px style="border:0px;"></td>';
			}
			$box .= '</tr></table></td></tr></table></div>';
			return $box;
		}	
	}
	
	function returnBoxPlotHeaderHtml($headWidth="130px"){
		$arrowheight = 15;
		$arrow .= '<div align=center>Box Plot<br /><table style="border:0px;">
					<tr><td style="border:0px;"><table width='.$headWidth.' style="border:0px; border-collapse:collapse;">
					<tr><td colspan=3 style="text-align:left; border:0px;">0</td>
					<td colspan=6 style="border:0px;">50</td>
					<td colspan=3 style="text-align:right;border:0px;">100</td></tr>';
		
		$arrow .= '<tr><td style="height:4px;border:0px;"></td>
					<td colspan=5 style="height:4px;border:0px;border-left: 1px solid;"></td>
					<td colspan=5 style="height:4px;border:0px;border-left: 1px solid;"></td>
					<td style="height:7px;border:0px;border-left: 1px solid;"></td></tr>';
					
		
		$arrow .= '<tr><td style="width:5%;height:7px;border:0px;"></td>';
		for($i=0;$i<10;$i++){  
			//Title box ratio set here, now the axis takes 90% of the space
			$arrow .= '<td style="width:9%;border:0px;  border-left: 1px solid; border-top: 1px solid;"></td>';
		}
		$arrow .= '<td style="width:5%;border:0px; border-left: 1px solid;"></td></tr></table>';										
		$arrow .= '</td></tr></table></div>';
		return $arrow;
	}
	
}
?>