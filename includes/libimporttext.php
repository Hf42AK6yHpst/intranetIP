<?php
// Modifing by : 

## Change Log ##
/*
# 2017-09-14 [Carlos]
# Detail	: remove UTF-8 non-breaking space with hex value C2A0 at GET_IMPORT_TXT() and GET_IMPORT_TXT_WITH_REFERENCE().
#
# 2015-08-18 [Cameron]
# Detail	: fix bug on checking is_array before applying array_filter in GET_IMPORT_TXT_WITH_REFERENCE()	
#
# 2012-03-15 [Marcus]
# Detail	:	Fixed GET_IMPORT_TXT cannot parse ""{$Data}"" to "{$Data}"
#
# 2011-11-22 [Yuen]
# Detail	:	add GET_IMPORT_TXT_FORMAT() & DISPLAY_FORMAT_WARNING() for detecting whether the file is unicode text
#
# 2011-02-15 [Henry Chow]
# Detail	:	modified GET_IMPORT_TXT_WITH_REFERENCE()
# Purpose	:	return column name without the word [optional]
#	
# 2011-02-01 [Henry Chow]
# Detail	:	modified GET_IMPORT_TXT_WITH_REFERENCE()
# Purpose	:	return original column name for column checking 	
#
# 2010-07-07 Henry Chow :
# Detail	:	add GET_IMPORT_TXT_WITH_REFERENCE()
# Purpose	:	Apply new import standard to IP25	
#
# 2010-04-09 Max (201004091545)
# Modified function GET_IMPORT_TXT() to support recognizing ASCII and change to UTF-8
#
# 20091109 (Ivan) :
# Added parameter $lineBreakReplacement in function GET_IMPORT_TXT
# Purpose: eRC need to keep the line break for displaying by passing $lineBreakReplacement = "<br />"
#
# 20090827 (Ronald) :
# Purpose : remove the line break if it is occur inside the csv cell
#				$NewFileContent = preg_replace('/"([^"]+)(?:\n\r|\n)([^"]+)"/',"\"$1$2\"",$NewFileContent);
#
# 20090725 (Ronald) : 
# Purpose : convert the import csv charset to utf-8
#				$NewFileContent = iconv(mb_detect_encoding($Buffer), 'UTF-8//TRANSLIT', $Buffer);
*/

if (!defined("LIBIMPORTTEXT_DEFINED"))                     // Preprocessor directive
{
  define("LIBIMPORTTEXT_DEFINED", true);

  class libimporttext extends libdb
  {
	  	
		var $Records = array();
		var $CoreHeader = array();
		
		/*
		* Initialize
		*/
		function libimporttext()
		{
			global $intranet_root;
			$this->libdb();
		}
		
		/*
		* Check existence uploaded file
		*/
		function CHECK_FILE_UPLOADED($ParFile) {
			$ReturnVal = ($ParFile['error']==0 && file_exists($ParFile['tmp_name']));
			return $ReturnVal;
		}
		
		/*
		* Check Byte Order Mark
		*/
		function CHECK_BOM ($Buffer) {
			$charset[1] = substr($Buffer, 0, 1);
			$charset[2] = substr($Buffer, 1, 1);
			$charset[3] = substr($Buffer, 2, 1);  
			if (ord($charset[1]) == 255 && ord($charset[2]) == 254)
				return "UTF-16LE";
			else if (ord($charset[1]) == 254 && ord($charset[2]) == 255)
				return "UTF-16BE";
			else if (ord($charset[1]) == 239 && ord($charset[2]) == 187 && ord($charset[3]) == 191)
				return "UTF-8";
			else
				return "Unknown";
		}
		
		/*
		* Extract data from file
		*/
/*
remark:
shall add
$x = addslash($x);
if using the B5 mode, either at the calling side or here, which is now at the calling side
*/

		
		function GET_IMPORT_TXT_FORMAT($ArrData)
		{
			$row1 = $ArrData[0];
			$row2 = $ArrData[1];			
			if (sizeof($row1)<=1 && sizeof($row2)<=1)
			{
				return "OTHERS";
			} else
			{
				return "UNICODE_TEXT";
			}
		}
		
		
		function DISPLAY_FORMAT_WARNING($ArrData)
		{
			global $Lang;
			if ($this->GET_IMPORT_TXT_FORMAT($ArrData)!="UNICODE_TEXT")
			{
				return $Lang['Warning']['UnicodeText'];
			}
			
		}
		
		
		function GET_IMPORT_TXT($ParFilePath, $incluedEmptyRow=0, $lineBreakReplacement='') {
	
			global $intranet_default_lang, $import_coding, $g_encoding_unicode;			
			#$g_encoding_unicode = true;
			$Handle = @fopen($ParFilePath, 'r');
			$Buffer = fread($Handle, filesize($ParFilePath));
			$TempFile = tmpfile();
			//$BytesLength = 4096; // Modified by key(2008-10-22) - enlarge the bytes length - default 2048
			$BytesLength = 8192; // Modified by Bill(2014-07-24) - enlarge the bytes length (Reason: vietnamese lang file)
					
			/* Big5 encoding cannot be used in mb_convert_encoding in PHP version < 4.3.0
			if (function_exists(mb_convert_encoding)) {
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'UTF-16LE');
					$NewFileContent = substr($NewFileContent, 2);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'UTF-16BE');
					$NewFileContent = substr($NewFileContent, 2);
				} else {
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'auto');
				}
			} else
			*/
			
			// get file coding input from interface
			// if file coding is b5 / gb, change the coding to utf-8

			if (function_exists(iconv)) {
				
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16LE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16BE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-8") {
					$NewFileContent = substr($Buffer, 3);
				} else {
					if ($import_coding == "utf") {
						$NewFileContent = $Buffer;
					} else {
						if (mb_detect_encoding(substr($Buffer,0,1)) == "ASCII") {
							$NewFileContent = iconv("Big5", 'UTF-8//TRANSLIT', $Buffer);
						} else {
						/* Added By Ronald on 20090724 */
						## convert the $Buffer to UTF8 if the original encoding is in unknow format
						$NewFileContent = iconv(mb_detect_encoding($Buffer), 'UTF-8//TRANSLIT', $Buffer);						

						/* Disable by Ronald on 20090724 
						if ($intranet_default_lang == "b5") {
							$NewFileContent = iconv('BIG5', 'UTF-8', $Buffer);						
						} else {
							$NewFileContent = iconv('GB2312', 'UTF-8', $Buffer);
						}
						*/
						}
					}					
				}
			} else {
				$NewFileContent = $Buffer;
			}
			
			// Added by : Ronald (20090827)
			// Purpose : remove the line break if it is occur inside the csv cell //
			// Old Method (this one will have error, so do not use)
			//$NewFileContent = preg_replace('/"([^"]+)(?:\n\r|\n)([^"]+)"/','/"$1$2/"',$NewFileContent);
			// New method 
			/*
			$file = $NewFileContent;
			function removecrlf($match){
				return preg_replace('/\r\n/',' ',$match[0]);
			}
			$file=preg_replace_callback('/(?<!\\\\)".*?(?<!\\\\)"/s','removecrlf',$file);
			$NewFileContent = $file;
			*/
			
			$file = $NewFileContent;
			### Added $lineBreakReplacement by Ivan (20091109)
			### Purpose: eRC need to keep the line break for display by passing $lineBreakReplacement = "<br />"
			$lineBreakReplacement = ($lineBreakReplacement=='')? ' ' : $lineBreakReplacement;
			$file = preg_replace_callback(
							'/(?<!\\\\)".*?(?<!\\\\)"/s',
							create_function(
								'$match',
								'return preg_replace("/\n/", "'.$lineBreakReplacement.'", $match[0]);'
							),
							$file
						);
			$NewFileContent = $file;
			
			////////////////////////////////////////////////////////////////////////////
			// For imail address testing
			////////////////////////////////////////////////////////////////////////////
			//$NewFileContent = str_replace("\"","",$NewFileContent);
			////////////////////////////////////////////////////////////////////////////
			
			# remove first 3-byte used for Byte Order Mark
			#$NewFileContent = substr($NewFileContent, 3);
			//echo $NewFileContent;
			fwrite($TempFile, $NewFileContent);
			fseek($TempFile, 0);
			
			while (!feof($TempFile)) {
				//$Data = trim(fgets($TempFile, $BytesLength));		//tempory disable by Ronald
				$Data = fgets($TempFile, $BytesLength);
				$Data = str_replace("\xc2\xa0", '', $Data); // remove UTF-8 non-breaking space with hex value C2A0 
				$DataPieces = explode("\t", $Data);
				for ($i=0; $i<sizeof($DataPieces); $i++) {
					
					$DataPieces[$i] = $this->REPLACE_DQ_IN_STR($DataPieces[$i]);
					
					// check for pattern : ""{DATA}""
//					if(substr($DataPieces[$i],0) == "\"\"")
					if(substr($DataPieces[$i],0,2) == "\"\"")
						$DataPieces[$i] = str_replace("\"\"","\"",$DataPieces[$i]);
					
					// check for pattern : "{DATA_1""DATA_2}"
					if(strpos($DataPieces[$i],"\"\"")>0)
						$DataPieces[$i] = str_replace("\"\"","\"",$DataPieces[$i]);
					
					// check for pattern : "{DATA_1\rDATA_2}"
					if(strpos($DataPieces[$i],"\r"))
						$DataPieces[$i] = str_replace("\r","",$DataPieces[$i]);
				}
				
				if($incluedEmptyRow)	# Yat Woon: some function need dislay the empty row [20090427]
				{
					$ReturnArr[] = $DataPieces;
				}
				else
				{
					# Eric Yip : To get rid of empty row (20080905)
					if(count(array_filter($DataPieces)) > 0)
						$ReturnArr[] = $DataPieces;
				}
			}
			
			fclose($TempFile);
			fclose($Handle);

			return $ReturnArr;
		}
		
		function GET_IMPORT_TXT_WITH_REFERENCE($ParFilePath, $incluedEmptyRow=0, $lineBreakReplacement='', $fieldAry=array(), $flagAry=array()) {
	
			global $intranet_default_lang, $import_coding, $g_encoding_unicode, $Lang;			
			#$g_encoding_unicode = true;
			
			$Handle = @fopen($ParFilePath, 'r');
			$Buffer = fread($Handle, filesize($ParFilePath));
			$TempFile = tmpfile();
			$BytesLength = 4096; // Modified by key(2008-10-22) - enlarge the bytes length - default 2048
						
			// get file coding input from interface
			// if file coding is b5 / gb, change the coding to utf-8

			if (function_exists(iconv)) {
				
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16LE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16BE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-8") {
					$NewFileContent = substr($Buffer, 3);
				} else {
					if ($import_coding == "utf") {
						$NewFileContent = $Buffer;
					} else {
						if (mb_detect_encoding(substr($Buffer,0,1)) == "ASCII") {
							$NewFileContent = iconv("Big5", 'UTF-8//TRANSLIT', $Buffer);
						} else {
						/* Added By Ronald on 20090724 */
						## convert the $Buffer to UTF8 if the original encoding is in unknow format
						$NewFileContent = iconv(mb_detect_encoding($Buffer), 'UTF-8//TRANSLIT', $Buffer);						
						}
					}					
				}
			} else {
				$NewFileContent = $Buffer;
			}
						
			$file = $NewFileContent;
			
			### Added $lineBreakReplacement by Ivan (20091109)
			### Purpose: eRC need to keep the line break for display by passing $lineBreakReplacement = "<br />"
			$lineBreakReplacement = ($lineBreakReplacement=='')? ' ' : $lineBreakReplacement;
			$file = preg_replace_callback(
							'/(?<!\\\\)".*?(?<!\\\\)"/s',
							create_function(
								'$match',
								'return preg_replace("/\n/", "'.$lineBreakReplacement.'", $match[0]);'
							),
							$file
						);
			$NewFileContent = $file;
						
			# remove first 3-byte used for Byte Order Mark
			#$NewFileContent = substr($NewFileContent, 3);
			
			fwrite($TempFile, $NewFileContent);
			fseek($TempFile, 0);
			
			$c = 1;				# row number
			$pointer = 0;		# current pointer of the $fieldAry
			
			$optional = $Lang['General']['ImportArr']['Optional']['En'];
			$reference = $Lang['General']['ImportArr']['Reference']['En'];
			
			while (!feof($TempFile)) {	# loop for every row of record
				$cell = 0;				
				unset($DataPiecesNew);
				//$Data = trim(fgets($TempFile, $BytesLength));		//tempory disable by Ronald
				
				$Data = fgets($TempFile, $BytesLength);
				$Data = str_replace("\xc2\xa0", '', $Data); // remove UTF-8 non-breaking space with hex value C2A0 
				$DataPieces = explode("\t", $Data);
				
				if($c!=2) {		# ignore 2nd row of CSV file (BIG5 column name)
					for ($i=0; $i<sizeof($DataPieces); $i++) {
						//echo $DataPieces[$i].'/';
						if($c==1) {		# change column name in 1st row from user-defined name to php readable format (English column name)
							if($flagAry[$i] == 1) {
								//$DataPiecesNew[$cell] = $fieldAry[$pointer];
								$DataPiecesNew[$cell] = $this->REPLACE_DQ_IN_STR($DataPieces[$i]); # assign import column name for column checking
								$DataPiecesNew[$cell] = str_replace(" ".$optional,"",$DataPiecesNew[$cell]);
								$DataPiecesNew[$cell] = str_replace(" ".$reference,"",$DataPiecesNew[$cell]);
								
								$pointer++;
								$cell++;
								
							}
						} 
						else 
						{
							if($flagAry[$i] == 1) {
								
								$DataPiecesNew[$cell] = $this->REPLACE_DQ_IN_STR($DataPieces[$i]);
								
								// check for pattern : ""{DATA}""
//								if(substr($DataPiecesNew[$cell],0) == "\"\"") {
								if(substr($DataPiecesNew[$cell],0, 2) == "\"\"") {
									$DataPiecesNew[$cell] = str_replace("\"\"","\"",$DataPiecesNew[$cell]);
								}
									
								//debug_pr($DataPiecesNew);
								// check for pattern : "{DATA_1""DATA_2}"
								if(strpos($DataPiecesNew[$cell],"\"\"")>0) {
									$DataPiecesNew[$cell] = str_replace("\"\"","\"",$DataPiecesNew[$cell]);
								}
								
								//debug_pr($DataPiecesNew);
								// check for pattern : "{DATA_1\rDATA_2}"
								if(strpos($DataPiecesNew[$cell],"\r")) {
									$DataPiecesNew[$cell] = str_replace("\r","",$DataPiecesNew[$cell]);
								}
								
								//echo '<br>';
								
								$cell++;
							}
						}
					}
					
					if($incluedEmptyRow)	# Yat Woon: some function need dislay the empty row [20090427]
					{
						$ReturnArr[] = $DataPiecesNew;
					}
					else
					{
						# Eric Yip : To get rid of empty row (20080905)
						if(is_array($DataPiecesNew) && count(array_filter($DataPiecesNew)) > 0)
							$ReturnArr[] = $DataPiecesNew;
					}
				}
				$c++;
			}
			
			fclose($TempFile);
			fclose($Handle);
			
			//debug_pr($ReturnArr);

			return $ReturnArr;
		}
		
				
		/*
		* Replace Double quotation mark of the string (1st character & last character)
		*/
		function REPLACE_DQ_IN_STR($Str) {
			$StrLength = strlen($Str);
			$Str = trim($Str);
			if (substr($Str, 0, 1) == "\"" && (substr($Str, $StrLength-1) == "\"" || substr($Str, $StrLength-2, 1) == "\"" || substr($Str, $StrLength-3, 1) == "\"")) {
				$Str = substr($Str, 1);
				$Str = substr($Str, 0, strlen($Str)-1);
			}
			return $Str;
		}
		
	// new method to handle special characters of csv file content
	// copy from libfilesystem.php
	/*
	## Old Version - cannot handle if there is a Double Quote in the cell element
    function mb_csv_split($line, $delim = ',', $removeQuotes = true) {
    	global $g_encoding_unicode;
    
	    $fields = array();
	    $fldCount = 0;
	    $inQuotes = false;
	    
	    # Eric Yip (20081204) : set internal encoding for mb functions
	    if($g_encoding_unicode)
	    	mb_internal_encoding("UTF-8");
	    else
	    	mb_internal_encoding("pass");
	
	    for ($i = 0; $i < mb_strlen($line); $i++) {
	      if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
	      $tmp = mb_substr($line, $i, mb_strlen($delim));
		  if ($tmp === $delim && !$inQuotes) {
	        $fldCount++;
	        $i+= mb_strlen($delim) - 1;
	      }
	      else if ($fields[$fldCount] == "" && mb_substr($line, $i, 1) == '"' && !$inQuotes) {
	        if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
	        $inQuotes = true;
	      }
	      else if (mb_substr($line, $i, 1) == '"') {
          if (mb_substr($line, $i+1, 1) == '"') {
            $i++;
            $fields[$fldCount] .= mb_substr($line, $i, 1);
          } else {
            if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
            $inQuotes = false;
          }
	      }
	      else {
	      	$fields[$fldCount] .= mb_substr($line, $i, 1);
	      }
	    }
	    return $fields;
    }
    */
    ## Modified: by Ronald (20090331)
    ## Changed:	Now can handle if there is a double quote inside the cell element
    function mb_csv_split($line, $delim = ',', $removeQuotes = true) {
    	global $g_encoding_unicode;
    
	    $fields = array();
	    $fldCount = 0;
	    $inQuotes = false;
	    
	    # Eric Yip (20081204) : set internal encoding for mb functions
	    if($g_encoding_unicode)
	    	mb_internal_encoding("UTF-8");
	    else
	    	mb_internal_encoding("pass");
	
	    for ($i = 0; $i < mb_strlen($line); $i++) {
		    if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
		    $tmp = mb_substr($line, $i, mb_strlen($delim));
		    if ($tmp === $delim && !$inQuotes) {
			    $fldCount++;
			    $i+= mb_strlen($delim) - 1;
		    }
		    else if ($fields[$fldCount] == "" && mb_substr($line, $i, 1) == '"' && !$inQuotes) {
			    if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
			    //echo " A:".mb_substr($line, $i, 1);
			    $inQuotes = true;
		    }
		    else if (mb_substr($line, $i, 1) == '"') {
			    if (mb_substr($line, $i+1, 1) == '"') {
				    $i++;
				    $fields[$fldCount] .= mb_substr($line, $i, 1);
				    //echo " B:".mb_substr($line, $i, 1);
			    } else {
				    //if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
				    
				    if((mb_substr($line, $i+1, 1) != $delim) && ($i!=mb_strlen($line)-1)){
					    $fields[$fldCount] .= mb_substr($line, $i, 1);
					    $inQuotes = true;
				    }
					else
					{
						$inQuotes = false;
					}
			    }
		    }
		    else {
			    $fields[$fldCount] .= mb_substr($line, $i, 1);
		    }
	    }
		//print_r($fields);
	    return $fields;
    }
		
		/*
		* Get file extension
		*/
		function GET_FILE_EXT($ParFilePath){
            $file = basename($ParFilePath);
            return strtolower(substr($ParFilePath, strrpos($ParFilePath,".")));
        }
        
        /*
		* Check file extension
		* Allow extensions: TXT, CSV, LOG
		*/
		function CHECK_FILE_EXT($ParFilePath){
            $file = basename($ParFilePath);
            $ext = strtoupper(substr($ParFilePath, strrpos($ParFilePath,".")));
            if($ext == ".CSV" || $ext == ".TXT" || $ext == ".LOG") {
            	return true;
        	} else {
        		return false;
	        }
        }
		
		
		/*
		*
		*/
		function SET_CORE_HEADER($ParCoreHeader) {
			$this->CoreHeader = $ParCoreHeader;
		}
		
		/*
		* Validate data, see if any field is unset or empty
		* (to be tested)
		*/
		function VALIDATE_DATA($ParData) {
			$CoreHeader = $this->CoreHeader;
			$CheckArray = array();
			for($i=0; $i<sizeof($ParData); $i++) {
				for ($j=0; $j<sizeof($CoreHeader); $j++) {
					if (empty($ParData[$i][$j]) || !isset($ParData[$i][$j]))
						$CheckArray[$i][$j] = TRUE;
					else
						$CheckArray[$i][$j] = FALSE;
				}
			}
			
			return $CheckArray;
		}
		
		/*
		* Validate data header based on core headers
		*  - can be strict or loose (can have more field after core headers)
		*/
		function VALIDATE_HEADER($ParData, $ParExactlyCore) {
	
			$CoreHeader = $this->CoreHeader;
			$DataHeader = $ParData[0];
			
			if ($ParExactlyCore && sizeof($DataHeader)!=sizeof($CoreHeader))
			{
				# not exactly matched
				$ReturnVal = FALSE;
			} else
			{
				$ReturnVal = TRUE;
	
				# check each header field
				for ($i=0; $i<sizeof($CoreHeader); $i++)
				{
					if (trim($CoreHeader[$i])!=trim($DataHeader[$i]))
					{
						$ReturnVal = FALSE;
						break;
					}
				}
			}
			return $ReturnVal;
		}
		
		
		/*
		* Check whether any valid data to add or update
		*/
		function ALLOW_SUBMISSION() {
			$Records = $this->Records;
			$ReturnVal = (sizeof($Records['valid_add'])>0 || sizeof($Records['valid_update'])>0);
			return $ReturnVal;
		}
		
		/*
		 * Check existency
		 * 
		 */
		 function GET_EMPTY_COLUMN($DataArray,$existencyCheckingArray){
		 	$numOfData = count($DataArray);
		 	for($i=0;$i<$numOfData;$i++){
		 		if($existencyCheckingArray[$i]==1){
		 			//Check only when $existencyCheckingArray[$i]==1
		 			if(trim($DataArray[$i])==''){
		 				// NOT EXIST!!!!
		 				return $i;
		 			}
		 		}
		 	}
		 	return -1;
		 }
		 
		function CHECK_VALID_YES_NO($YesNoData){
			/**
			 * Return 1 if yes
			 * Return 0 if No
			 * Return -1 if invalid
			 */
			 if($YesNoData=='Y'||$YesNoData=='y'||$YesNoData=='1'){
			 	return 1;
			 }else if($YesNoData=='N'||$YesNoData=='n'||$YesNoData=='0'){
			 	return 0;
			 }else{
			 	return -1;
			 }
			 
		}

	}	// End of class definition

}        // End of directive
?>