<?php
include_once('libdb.php');
include_once('lib.php');

if (!defined("LIBTIMETABLE_OLD_DEFINED"))         // Preprocessor directives
{
	define("LIBTIMETABLE_OLD_DEFINED", true);
	
	class libtimetable extends libdb {
		
		function libtimetable(){
			parent:: libdb();
		}
		
		function Get_Cycle_Info($CyclePeriodID)
		{
			global $Lang;
			
			include_once("libcycleperiods.php");
			$libcycleperiods = new libcycleperiods();
			$CyclePeriodInfoArr = $libcycleperiods->returnPeriods_NEW($CyclePeriodID);
			
			$PeriodType = $CyclePeriodInfoArr[0]['PeriodType'];
			$CycleType = $CyclePeriodInfoArr[0]['CycleType'];
			$PeriodDays = $CyclePeriodInfoArr[0]['PeriodDays'];
			
			$CycleDisplayArr = array();
			if ($PeriodType == 0)
			{
				# Weekdays
				$CycleDisplayArr = $Lang['General']['DayType0'];
				array_shift($CycleDisplayArr);	// Discard Sunday
			}
			else if ($PeriodType == 1)
			{
				if ($CycleType == 0)
				{
					# Numbers (Day 1, Day 2...)
					$TargetArr = $libcycleperiods->array_numeric;
				}
				else if ($CycleType == 1)
				{
					# Characters (Day A, Day B...)
					$TargetArr = $libcycleperiods->array_alphabet;
				}
				else if ($CycleType == 2)
				{
					# Roman Numbers (Day I, Day II...)
					$TargetArr = $libcycleperiods->array_roman;
				}
				
				for ($i=0; $i<$PeriodDays; $i++)
				{
					$CycleDisplayArr[] = $Lang['SysMgr']['CycleDay']['Day'].' '.$TargetArr[$i];
				}
			}
			
			return $CycleDisplayArr;
		}
		
		function Get_Period_Info($PeriodID)
		{
			$ReturnArr = array();
			
			# $ReturnArr[$TimeSessionID] = $TimeSessionInfoArr
			$ReturnArr[1] = array("TimeSessionID" => 1, "Title" => "Morning Assembly", "TimeSlot" => "(8:00 - 8:15)");
			$ReturnArr[2] = array("TimeSessionID" => 2, "Title" => "Lesson 1", "TimeSlot" => "(8:15 - 8:55)");
			$ReturnArr[3] = array("TimeSessionID" => 3, "Title" => "Lesson 2", "TimeSlot" => "(8:55 - 9:35)");
			$ReturnArr[4] = array("TimeSessionID" => 4, "Title" => "Recess", "TimeSlot" => "(9:35 - 9:45)");
			$ReturnArr[5] = array("TimeSessionID" => 5, "Title" => "Lesson 3", "TimeSlot" => "(9:45 - 10:25)");
			$ReturnArr[6] = array("TimeSessionID" => 6, "Title" => "Lesson 4", "TimeSlot" => "(10:25 - 11:05)");
			$ReturnArr[7] = array("TimeSessionID" => 7, "Title" => "Recess", "TimeSlot" => "(11:05 - 11:15)");
			$ReturnArr[8] = array("TimeSessionID" => 8, "Title" => "Lesson 5", "TimeSlot" => "(11:15 - 11:55)");
			$ReturnArr[9] = array("TimeSessionID" => 9, "Title" => "Lesson 6", "TimeSlot" => "(11:55 - 12:35)");
			$ReturnArr[10] = array("TimeSessionID" => 10, "Title" => "Lunch Time", "TimeSlot" => "(12:35 - 13:45)");
			$ReturnArr[11] = array("TimeSessionID" => 11, "Title" => "Lesson 7", "TimeSlot" => "(13:45 - 14:25)");
			$ReturnArr[12] = array("TimeSessionID" => 12, "Title" => "Lesson 8", "TimeSlot" => "(14:25 - 15:00)");
			$ReturnArr[13] = array("TimeSessionID" => 13, "Title" => "Class Teacher Session", "TimeSlot" => "(15:00 - 15:15)");
			
			return $ReturnArr;
		}
		
		function Get_Time_Session_Info($TimeSessionID)
		{
			$ReturnArr = array();
			
			$PeriodArr = $this->Get_Period_Info($PeriodID);
			$ReturnArr = $PeriodArr[$TimeSessionID];
			
			return $ReturnArr;
		}
	}
	
	
	
	class Room_Allocation extends libtimetable {
		var $RoomAllocationID;
		
		var $TableName;
		var $ID_FieldName;
		
		function Room_Allocation($RoomAllocationID=""){
			parent:: libtimetable();
			
			$this->TableName = 'INTRANET_TIMETABLE_ROOM_ALLOCATION';
			$this->ID_FieldName = 'RoomAllocationID';
			
			if ($RoomAllocationID!="")
			{
				$this->RecordID = $RoomAllocationID;
				$this->RoomAllocationID = $RoomAllocationID;
				$this->Get_Self_Info();
			}
		}
		
		function Get_Self_Info()
		{
			$sql = "";
			$sql .= " SELECT * FROM ".$this->TableName;
			$sql .= " WHERE ".$this->ID_FieldName." = '".$this->RecordID."'";
			
			$resultSet = $this->returnArray($sql);
			$infoArr = $resultSet[0];
			
			foreach ($infoArr as $key => $value)
				$this->{$key} = $value;
		}
		
		function Insert_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
			
			# Last Modified By
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# Last Modified By
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = $LastModifiedBy;
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = '';
			$sql .= ' INSERT INTO '.$this->TableName;
			$sql .= ' ( '.$fieldText.' ) ';
			$sql .= ' VALUES ';
			$sql .= ' ( '.$valueText.' ) ';
			
			$success = $this->db_db_query($sql);
			if ($success == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
			return $success;
		}
		
		function Update_Record($DataArr=array())
		{
			if (count($DataArr) == 0)
				return false;
				
			
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
			}
			$valueFieldText .= ' DateModified = now(), ';
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
			
			$sql = '';
			$sql .= ' UPDATE '.$this->TableName;
			$sql .= ' SET '.$valueFieldText;
			$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->RecordID.'\' ';
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Delete_Record()
		{
			$sql = "DELETE FROM ".$this->TableName." WHERE RoomAllocationID = '".$this->RoomAllocationID."' ";
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		function Get_All_Room_Allocation($PeriodID, $CycleDays, $SubjectGroupIDArr=array(), $LocationID='')
		{
			/*
			$ReturnArr = array();
			$ReturnArr[0][0] = array();
			
			# $ReturnArr[$TimeSessionID][#CycleDay] = RoomAllocationInfoArr;
			$ReturnArr[1][0][] = array("RoomAllocationID" => 1, "SubjectGroupID" => 1, "LocationID" => 1);
			$ReturnArr[1][0][] = array("RoomAllocationID" => 2, "SubjectGroupID" => 2, "LocationID" => 2);
			
			$ReturnArr[1][3][] = array("RoomAllocationID" => 5, "SubjectGroupID" => 1, "LocationID" => 32);
			
			$ReturnArr[5][4][] = array("RoomAllocationID" => 3, "SubjectGroupID" => 1, "LocationID" => 3);
			$ReturnArr[5][4][] = array("RoomAllocationID" => 4, "SubjectGroupID" => 2, "LocationID" => 1);
			
			return $ReturnArr;
			*/
			
			$cond_subjectGroup = "";
			if (count($SubjectGroupIDArr) > 0)
			{
				$SubjectGroupIDList = implode(", ", $SubjectGroupIDArr);
				$cond_subjectGroup = " AND SubjectGroupID IN ($SubjectGroupIDList) ";
			}
			
			$cond_locationID = "";
			if ($LocationID != "")
				$cond_locationID = " AND LocationID = '".$LocationID."' ";
			
			$sql = "SELECT
							*
					FROM
							INTRANET_TIMETABLE_ROOM_ALLOCATION
					WHERE
							CycleDays = '".$CycleDays."'
						$cond_subjectGroup
						$cond_locationID
					";
			$RoomAllocationArr = $this->returnArray($sql);
			
			$numOfRoomAllocation = count($RoomAllocationArr);
			$ReturnArr = array();
			# $ReturnArr[$TimeSessionID][#Day][0,1,2...] = RoomAllocationInfoArr;
			for ($i=0; $i<$numOfRoomAllocation; $i++)
			{
				$thisTimeSessionID = $RoomAllocationArr[$i]['TimeSessionID'];
				$thisDay = $RoomAllocationArr[$i]['Day'];
				
				$ReturnArr[$thisTimeSessionID][$thisDay][] = $RoomAllocationArr[$i];
			}
			
			return $ReturnArr;
			
		}
		
		function Is_Room_Allocation_Overlapped($TimeSessionID, $CycleDays, $Day, $LocationID, $RoomAllocationID='')
		{
			$cond_RoomAllocationID = '';
			if ($RoomAllocationID != '')
				$cond_RoomAllocationID = " AND RoomAllocationID != '$RoomAllocationID' ";
			
			$sql = "	SELECT
								RoomAllocationID
						FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION
						WHERE
								TimeSessionID = '$TimeSessionID'
							AND
								CycleDays = '$CycleDays'
							AND
								Day = '$Day'
							AND
								LocationID = '$LocationID'
							$cond_RoomAllocationID
					";
			$resultSet = $this->returnArray($sql);
			
			if (count($resultSet) > 0)
				return 1;
			else
				return 0;
		}
		
		function Is_Subject_Group_Overlapped($TimeSessionID, $CycleDays, $Day, $SubjectGroupID, $RoomAllocationID='')
		{
			$cond_RoomAllocationID = '';
			if ($RoomAllocationID != '')
				$cond_RoomAllocationID = " AND RoomAllocationID != '$RoomAllocationID' ";
			
			$sql = "	SELECT
								RoomAllocationID
						FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION
						WHERE
								TimeSessionID = '$TimeSessionID'
							AND
								CycleDays = '$CycleDays'
							AND
								Day = '$Day'
							AND
								SubjectGroupID = '$SubjectGroupID'
							$cond_RoomAllocationID
					";
			$resultSet = $this->returnArray($sql);
			
			if (count($resultSet) > 0)
				return 1;
			else
				return 0;
		}
	}

} // End of directives
?>