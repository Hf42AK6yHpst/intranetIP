<?php
// page editing by: Michael Cheung
require_once('UserInterface.php');
// require_once('shared_mailbox.php');
// require_once('imap_address.php');
// require_once('user.php');

class imap_ui extends UserInterface {
	function imap_ui() {
		parent::UserInterface();
	}
	
	// Get Read email interface
	function Get_Read_Mail($Mail, $CacheFormat=false) {
		Global $Lang, $SYS_CONFIG;
		Global $Folder, $CurTag, $MessageID;	
		
		// get addressbook obj
		$AdrBook = new imap_address();
		$GroupList = $AdrBook->Get_User_Groups($_SESSION['SSV_USERID']);
		array_unshift($GroupList,array($Lang['email']['NoAddToGroup'],""));
		
		// Javascript to show and hide detail list of To/CC/Bcc/Attachment
		$x .= '<script type="text/javascript">
					function Expand_Receiver(HideSpan,ShowSpan) {
						HideSpan.style.display = \'none\';
						ShowSpan.style.display = \'inline\';
					}
					</script>';
					
		// JS to show the add contact form
		$x .= '<script type="text/javascript">
					function Display_AddContact(DisplayName,Address,event) {
						var Obj = document.getElementById(\'AddContactSubLayer\');
					';
		if ($CacheFormat) 
			$x .= '	document.getElementById(\'NewContactForm\').action = \'cache_add_contact.php\';';
		else
			$x .= '	document.getElementById(\'NewContactForm\').action = \'add_contact.php\';';
		
		$x .= '	document.getElementById(\'NewMailDisplay\').innerHTML = Address;
						document.getElementById(\'NewFirstName\').value = DisplayName;
						document.getElementById(\'NewEmail\').value = Address;
						
						Obj.style.top = (event.layerY-15) + \'px\';
						Obj.style.left = event.layerX + \'px\';
						
						Obj.style.display = \'inline\';
						Obj.style.visibility = \'visible\';
					}
					
					function Validate_Contact() {
						var FirstName = document.getElementById(\'NewFirstName\').value;
						var SurName = document.getElementById(\'NewSurName\').value;
						
						if (FirstName.Trim() != "" && SurName.Trim() != "") 
							document.getElementById(\'NewContactForm\').submit(); 
						else 
							document.getElementById(\'ErrMsg\').innerHTML = \''.$Lang['Warning']['ContactBothFirstSur'].'\'; 
					}
					</script>';
					
		// Layer for add contact personal address book
		$x .= $this->Get_Form_Open("NewContactForm","POST","add_contact.php");
		$x .= $this->Get_Div_Open("AddContactSubLayer",'style="display:none; position:absolute; top: 0px; left: -9999; visibility: hidden;"');
		$x .= $this->Get_Div();
		//$x .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$x .= $this->Get_Div_Open("",'style="width:100%;"');
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03"').$this->Get_Span_Open("",'CLASS="bubble_board_04"');
		$x .= $Lang['email']['AddContacts'].": \"";
		$x .= $this->Get_Span_Open("NewMailDisplay");
		$x .= $this->Get_Span_Close();
		$x .= "\"<br>";
		$x .= $this->Get_Span_Open("ErrMsg",'class="warning"');
		$x .= $this->Get_Span_Close();
		$x .= "<br>";
		$x .= $this->Get_Div_Open("",'style="width:90%"');
		$x .= $this->Get_Table_Open("","","",0,1,0);
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('','style="width: 80px; font-size: 11px; color: #000000;"');
		$x .= $Lang['email']['FirstName'];
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open();
		$x .= $this->Get_Input_Text("NewFirstName","",'class="textbox"');
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('','style="font-size: 11px; color: #000000;"');
		$x .= $Lang['email']['Surname'];
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open();
		$x .= $this->Get_Input_Text("NewSurName","",'class="textbox"')."<br>";
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('','style="font-size: 11px; color: #000000;"');
		$x .= $Lang['email']['AddToGroup'];
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open();
		$x .= $this->Get_Input_Select("ChooseGroupID","ChooseGroupID",$GroupList,"");
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_button("Submit","Submit",$Lang['btn_save'],'onclick="Validate_Contact();" class="button_main_act"')." ".$this->Get_Input_Button("","",$Lang['btn_cancel'],'OnClick="document.getElementById(\'AddContactSubLayer\').style.visibility = \'hidden\'; document.getElementById(\'AddContactSubLayer\').style.display = \'none\';" class="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Input_Hidden("NewEmail","NewEmail","");
		$x .= $this->Get_Input_Hidden("Folder","Folder",$Folder);
		$x .= $this->Get_Input_Hidden("CurTag","CurTag",$CurTag);
		$x .= $this->Get_Input_Hidden("MessageID","MessageID",$MessageID);
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		// end layer for add contact
		
		// Subject/ Receive Date
		$x .= $this->Get_Div_Open("commontabs_board",'class="imail_mail_content"');
		$x .= $this->Get_Span_Open("read_mail_subject").$Mail[0]["subject"].$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("read_mail_daytime").$Mail[0]["dateReceive"].$this->Get_Span_Close();
		$x .= "<br>";
		
		//from
		$fromList = $Mail[0]["fromDetail"];
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['From'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_content");
		for ($i=0; $i< sizeof($fromList); $i++) {
			if (!isset($fromList[$i]->personal)) $DisplayName = $fromList[$i]->mailbox."@".$fromList[$i]->host;
			else {
				$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
			}
			
			$Address = $fromList[$i]->mailbox."@".$fromList[$i]->host;
			
			if ($AdrBook->Check_Exists_Personal_Book($Address)) {
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.str_replace("'","\'",$DisplayName).'\',\''.$Address.'\',event);"');
				$x .= $DisplayName;
				$x .= $this->Get_HyperLink_Close();
			}
			else {
				$x .= $DisplayName;
			}
			
			$x .= ";";
		}
		$x .= $this->Get_Span_Close();
		$x .= "<br>";
		
		//to
		$toList = $Mail[0]["toDetail"];
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['To'];
		$x .= $this->Get_Span_Close();
		
		// short receiver list
		$x .= $this->Get_Span_Open("ToList",'style="display:none;"');	
		$x .= $this->Get_Span_Open("mail_form_label_content");
		for ($i=0; $i< 5; $i++) {
			if (!isset($toList[$i]->personal)) $DisplayName = $toList[$i]->mailbox."@".$toList[$i]->host;
			else {
				$DisplayObject = imap_mime_header_decode(imap_utf8($toList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
			}
			$Address = $toList[$i]->mailbox."@".$toList[$i]->host;
			
			if ($AdrBook->Check_Exists_Personal_Book($Address)) {
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.$DisplayName.'\',\''.$Address.'\',event);"');
				$x .= $DisplayName;
				$x .= $this->Get_HyperLink_Close();
			}
			else {
				$x .= $DisplayName;
			}
			$x .= "; ";
		}
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'ToList\'),document.getElementById(\'ToDetailList\'));"');
		$x .= " ...";
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_button");
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'ToList\'),document.getElementById(\'ToDetailList\'));"');
		$x .= $Lang['email']['ShowAll'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		
		// detail list
		$x .= $this->Get_Span_Open("ToDetailList",'style="display:none;"');	
		$x .= $this->Get_Span_Open("mail_form_label_content");
		for ($i=0; $i< sizeof($toList); $i++) {
			if (!isset($toList[$i]->personal)) $DisplayName = $toList[$i]->mailbox."@".$toList[$i]->host;
			else {
				$DisplayObject = imap_mime_header_decode(imap_utf8($toList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
			}
			$Address = $toList[$i]->mailbox."@".$toList[$i]->host;
			
			$FullList = $this->Get_Span_Open("mail_form_label_content");
			if ($AdrBook->Check_Exists_Personal_Book($Address)) {
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.$DisplayName.'\',\''.$Address.'\',event);"');
				$x .= $DisplayName;
				$x .= $this->Get_HyperLink_Close();
			}
			else {
				$x .= $DisplayName;
			}
			$x .= "; ";
		}
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_button");
		if (sizeof($toList) > 6) {
			$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'ToDetailList\'),document.getElementById(\'ToList\'));"');
			$x .= $Lang['email']['ShowShortList'];
			$x .= $this->Get_HyperLink_Close();
		}
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		
		if (sizeof($toList) > 6) {
			$x .= '
						<script>
							document.getElementById("ToList").style.display = "inline";
						</script>
						';
		}
		else {
			$x .= '
						<script>
							document.getElementById("ToDetailList").style.display = "inline";
						</script>
						';
		}
		$x .= '<br>';
		//end of to
		
		//CC
		$ccList = $Mail[0]["ccDetail"];
		if (sizeof($ccList) > 0) {
			$x .= $this->Get_Span_Open("mail_form_label");
			$x .= $Lang['email']['Cc'];
			$x .= $this->Get_Span_Close();
			// short receiver list
			$x .= $this->Get_Span_Open("CcList",'style="display:none;"');	
			$x .= $this->Get_Span_Open("mail_form_label_content");
			for ($i=0; $i< 5; $i++) {
				if (!isset($ccList[$i]->personal)) $DisplayName = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				else {
					$DisplayObject = imap_mime_header_decode(imap_utf8($ccList[$i]->personal));
					$DisplayName = $DisplayObject[0]->text;
				}
				$Address = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				
				if ($AdrBook->Check_Exists_Personal_Book($Address)) {
					$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.$DisplayName.'\',\''.$Address.'\',event);"');
					$x .= $DisplayName;
					$x .= $this->Get_HyperLink_Close();
				}
				else {
					$x .= $DisplayName;
				}
				$x .= "; ";
			}
			$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'CcList\'),document.getElementById(\'CcDetailList\'));"');
			$x .= " ...";
			$x .= $this->Get_HyperLink_Close();
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Open("mail_form_label_button");
			$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'CcList\'),document.getElementById(\'CcDetailList\'));"');
			$x .= $Lang['email']['ShowAll'];
			$x .= $this->Get_HyperLink_Close();
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Close();
			
			// detail list
			$x .= $this->Get_Span_Open("CcDetailList",'style="display:none;"');	
			$x .= $this->Get_Span_Open("mail_form_label_content");
			for ($i=0; $i< sizeof($ccList); $i++) {
				if (!isset($ccList[$i]->personal)) $DisplayName = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				else {
					$DisplayObject = imap_mime_header_decode(imap_utf8($ccList[$i]->personal));
					$DisplayName = $DisplayObject[0]->text;
				}
				$Address = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				
				$FullList = $this->Get_Span_Open("mail_form_label_content");
				if ($AdrBook->Check_Exists_Personal_Book($Address)) {
					$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.$DisplayName.'\',\''.$Address.'\',event);"');
					$x .= $DisplayName;
					$x .= $this->Get_HyperLink_Close();
				}
				else {
					$x .= $DisplayName;
				}
				$x .= "; ";
			}
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Open("mail_form_label_button");
			if (sizeof($ccList) > 6) {
				$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'CcDetailList\'),document.getElementById(\'CcList\'));"');
				$x .= $Lang['email']['ShowShortList'];
				$x .= $this->Get_HyperLink_Close();
			}
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Close();
			
			if (sizeof($ccList) > 6) {
				$x .= '
							<script>
								document.getElementById("CcList").style.display = "inline";
							</script>
							';
			}
			else {
				$x .= '
							<script>
								document.getElementById("CcDetailList").style.display = "inline";
							</script>
							';
			}
		}
		$x .= '<br>';
		//end of CC
		
		//BCC
		if ($Folder == $SYS_CONFIG['Mail']['FolderPrefix'].$SYS_CONFIG['Mail']['FolderDelimiter']."Sent") {
			$bccList = $Mail[0]["bccDetail"];
			if (sizeof($bccList) > 0) {
				$x .= $this->Get_Span_Open("mail_form_label");
				$x .= $Lang['email']['Bcc'];
				$x .= $this->Get_Span_Close();
				// short receiver list
				$x .= $this->Get_Span_Open("BccList",'style="display:none;"');	
				$x .= $this->Get_Span_Open("mail_form_label_content");
				for ($i=0; $i< 5; $i++) {
					if (!isset($bccList[$i]->personal)) $DisplayName = $bccList[$i]->mailbox."@".$bccList[$i]->host;
					else {
						$DisplayObject = imap_mime_header_decode(imap_utf8($bccList[$i]->personal));
						$DisplayName = $DisplayObject[0]->text;
					}
					$Address = $bccList[$i]->mailbox."@".$bccList[$i]->host;
					
					if ($AdrBook->Check_Exists_Personal_Book($Address)) {
						$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.$DisplayName.'\',\''.$Address.'\',event);"');
						$x .= $DisplayName;
						$x .= $this->Get_HyperLink_Close();
					}
					else {
						$x .= $DisplayName;
					}
					$x .= "; ";
				}
				$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'BccList\'),document.getElementById(\'BccDetailList\'));"');
				$x .= " ...";
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Open("mail_form_label_button");
				$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'BccList\'),document.getElementById(\'BccDetailList\'));"');
				$x .= $Lang['email']['ShowAll'];
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Close();
				
				// detail list
				$x .= $this->Get_Span_Open("BccDetailList",'style="display:none;"');	
				$x .= $this->Get_Span_Open("mail_form_label_content");
				for ($i=0; $i< sizeof($bccList); $i++) {
					if (!isset($bccList[$i]->personal)) $DisplayName = $bccList[$i]->mailbox."@".$bccList[$i]->host;
					else {
						$DisplayObject = imap_mime_header_decode(imap_utf8($bccList[$i]->personal));
						$DisplayName = $DisplayObject[0]->text;
					}
					$Address = $bccList[$i]->mailbox."@".$bccList[$i]->host;
					
					$FullList = $this->Get_Span_Open("mail_form_label_content");
					if ($AdrBook->Check_Exists_Personal_Book($Address)) {
						$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link" onclick="Display_AddContact(\''.$DisplayName.'\',\''.$Address.'\',event);"');
						$x .= $DisplayName;
						$x .= $this->Get_HyperLink_Close();
					}
					else {
						$x .= $DisplayName;
					}
					$x .= "; ";
				}
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Open("mail_form_label_button");
				if (sizeof($bccList) > 6) {
					$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'BccDetailList\'),document.getElementById(\'BccList\'));"');
					$x .= $Lang['email']['ShowShortList'];
					$x .= $this->Get_HyperLink_Close();
				}
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Close();
				
				if (sizeof($bccList) > 6) {
					$x .= '
								<script>
									document.getElementById("BccList").style.display = "inline";
								</script>
								';
				}
				else {
					$x .= '
								<script>
									document.getElementById("BccDetailList").style.display = "inline";
								</script>
								';
				}
			}
			$x .= '<br>';
		}
		//end of BCC
		
		//attachment
		$Attachment = $Mail[0]["attach_parts"];
		if (!(is_null($Attachment) || $Attachment == "")) {
			$x .= $this->Get_Span_Open("mail_form_attach");
			
			// short list	
			$x .= $this->Get_Span_Open("Attach_Short_List",'style="display:none;"');
			$x .= $this->Get_Span_Open("mail_form_label_content_line");
			for ($i=0; $i< 5; $i++) {
				$x .= $this->Get_HyperLink_Open("view_attachment.php?AttachNum=".$i."&Folder=".$Folder."&MessageID=".$MessageID,'CLASS="imail_entry_read_link"');
				$x .= $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'imail/icon_attachment.gif',12,18,'BORDER="0" ALIGN="absmiddle"');
				$x .= $Attachment[$i]['filename'];
				$x .= $this->Get_HyperLink_Close();
				$x .= "; ";
			}
			$x .= $this->Get_Span_Close();
			if (sizeof($Attachment) > 6) {
				$x .= $this->Get_Span_Open("mail_form_label_button");
				$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"  onclick="Expand_Receiver(document.getElementById(\'Attach_Short_List\'),document.getElementById(\'Attach_Detail_List\'));"');
				$x .= $Lang['email']['ShowAll'];
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();
				
			}
			$x .= $this->Get_Span_Close();
			
			// detail list	
			$x .= $this->Get_Span_Open("Attach_Detail_List",'style="display:none;"');
			$x .= $this->Get_Span_Open("mail_form_label_content_line");
			for ($i=0; $i< sizeof($Attachment); $i++) {
				$x .= $this->Get_HyperLink_Open("view_attachment.php?AttachNum=".$i."&Folder=".$Folder."&MessageID=".$MessageID,'CLASS="imail_entry_read_link"');
				$x .= $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'imail/icon_attachment.gif',12,18,'BORDER="0" ALIGN="absmiddle"');
				$x .= $Attachment[$i]['filename'];
				$x .= $this->Get_HyperLink_Close();
				$x .= "; ";
			}
			$x .= $this->Get_Span_Close();
			if (sizeof($Attachment) > 6) {
				$x .= $this->Get_Span_Open("mail_form_label_button");
				$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="Expand_Receiver(document.getElementById(\'Attach_Detail_List\'),document.getElementById(\'Attach_Short_List\'));"');
				$x .= $Lang['email']['ShowShortList'];;
				$x .= $this->Get_HyperLink_Close();
				$x .= $this->Get_Span_Close();
				
			}
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Close();
			
			if (sizeof($Attachment) > 5) {
				$x .= '
							<script>
								document.getElementById("Attach_Short_List").style.display = "inline";
							</script>
							';
			}
			else {
				$x .= '
							<script>
								document.getElementById("Attach_Detail_List").style.display = "inline";
							</script>
							';
			}
		}
		//$x .= $this->Get_Div_Close();
		
		// Mail Body
		$x .= $this->Get_Div_Open("mail_form_content_board",'style="width:98.2%; padding: 5px"');
		$x .= $this->Get_IFrame("0","MailBody","MailBody","auto","100%","100%","view_body.php?Folder=".urlencode($Folder)."&MessageID=".$MessageID,"0","0");
		$x .= $this->Get_Div_Close();
		
		$x .= $this->Get_Div_Close();

		return $x;
	}
	
	// Get Read email interface
	function Get_Mail_Print_Version($Mail) {
		Global $Lang, $SYS_CONFIG;
		Global $Folder, $MessageID;	
		
		// Subject/ Receive Date
		$x .= $this->Get_Div_Open("commontabs_board",'CLASS="imail_mail_content"');
		$x .= $this->Get_Table_Open("","100%","",0,0,0,'valign="top"');
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open("",'width="50%"');
		$x .= $this->Get_Div_Open("read_mail_subject").$Mail[0]["subject"].$this->Get_Div_Close();
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open("",'width="50%"');
		$x .= $this->Get_Div_Open("read_mail_daytime").$Mail[0]["dateReceive"].$this->Get_Div_Close();
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		$x .= "<br>";
		
		//from
		$fromList = $Mail[0]["fromDetail"];
		$x .= $this->Get_Table_Open("","100%","",0,0,0,'valign="top"');
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('','width="7%"');
		$x .= $this->Get_Div_Open("mail_form_label");
		$x .= $Lang['email']['From'];
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open();
		$x .= $this->Get_Div_Open("mail_form_label_content");
		for ($i=0; $i< sizeof($fromList); $i++) {
			if (!isset($fromList[$i]->personal)) $DisplayName = $fromList[$i]->mailbox."@".$fromList[$i]->host;
			else {
				$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
			}
			
			$Address = $fromList[$i]->mailbox."@".$fromList[$i]->host;
			
			//$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_unread_link"');
			$x .= $DisplayName;
			//$x .= $this->Get_HyperLink_Close();
			$x .= ";";
		}
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		
		//to
		$toList = $Mail[0]["toDetail"];
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open('','width="7%"');
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['To'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Table_Cell_Close();
		
		// detail list
		$x .= $this->Get_Table_Cell_Open();
		$x .= $this->Get_Span_Open("ToDetailList",'style="display:inline;"');	
		$x .= $this->Get_Span_Open("mail_form_label_content");
		for ($i=0; $i< sizeof($toList); $i++) {
			if (!isset($toList[$i]->personal)) $DisplayName = $toList[$i]->mailbox."@".$toList[$i]->host;
			else {
				$DisplayObject = imap_mime_header_decode(imap_utf8($toList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
			}
			$Address = $toList[$i]->mailbox."@".$toList[$i]->host;
			
			$FullList = $this->Get_Span_Open("mail_form_label_content");
			//$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
			$x .= $DisplayName;
			//$x .= $this->Get_HyperLink_Close();
			$x .= "; ";
		}
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_button");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		//end of to
		
		//CC
		$ccList = $Mail[0]["ccDetail"];
		if (sizeof($ccList) > 0) {
			$x .= $this->Get_Table_Row_Open();
			$x .= $this->Get_Table_Cell_Open('','width="7%"');
			$x .= $this->Get_Span_Open("mail_form_label");
			$x .= $Lang['email']['Cc'];
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Table_Cell_Close();
			
			// detail list
			$x .= $this->Get_Table_Cell_Open();
			$x .= $this->Get_Span_Open("CcDetailList",'style="display:inline;"');	
			$x .= $this->Get_Span_Open("mail_form_label_content");
			for ($i=0; $i< sizeof($ccList); $i++) {
				if (!isset($ccList[$i]->personal)) $DisplayName = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				else {
					$DisplayObject = imap_mime_header_decode(imap_utf8($ccList[$i]->personal));
					$DisplayName = $DisplayObject[0]->text;
				}
				$Address = $ccList[$i]->mailbox."@".$ccList[$i]->host;
				
				$FullList = $this->Get_Span_Open("mail_form_label_content");
				//$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
				$x .= $DisplayName;
				//$x .= $this->Get_HyperLink_Close();
				$x .= "; ";
			}
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Open("mail_form_label_button");
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Table_Cell_Close();
			$x .= $this->Get_Table_Row_Close();
		}
		//end of CC
		
		//BCC
		if ($Folder == $SYS_CONFIG['Mail']['FolderPrefix'].$SYS_CONFIG['Mail']['FolderDelimiter']."Sent") {
			$bccList = $Mail[0]["bccDetail"];
			if (sizeof($bccList) > 0) {
				$x .= $this->Get_Table_Row_Open();
				$x .= $this->Get_Table_Cell_Open('','width="7%"');
				$x .= $this->Get_Span_Open("mail_form_label");
				$x .= $Lang['email']['Bcc'];
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Table_Cell_Close();
				
				// detail list
				$x .= $this->Get_Table_Cell_Open();
				$x .= $this->Get_Span_Open("BccDetailList",'style="display:inline;"');	
				$x .= $this->Get_Span_Open("mail_form_label_content");
				for ($i=0; $i< sizeof($bccList); $i++) {
					if (!isset($bccList[$i]->personal)) $DisplayName = $bccList[$i]->mailbox."@".$bccList[$i]->host;
					else {
						$DisplayObject = imap_mime_header_decode(imap_utf8($bccList[$i]->personal));
						$DisplayName = $DisplayObject[0]->text;
					}
					$Address = $bccList[$i]->mailbox."@".$bccList[$i]->host;
					
					$FullList = $this->Get_Span_Open("mail_form_label_content");
					//$x .= $this->Get_HyperLink_Open("#",'CLASS="imail_entry_read_link"');
					$x .= $DisplayName;
					//$x .= $this->Get_HyperLink_Close();
					$x .= "; ";
				}
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Open("mail_form_label_button");
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Span_Close();
				$x .= $this->Get_Table_Cell_Close();
				$x .= $this->Get_Table_Row_Close();
			}
		}
		//end of BCC
		$x .= $this->Get_Table_Close();
		
		//attachment
		$Attachment = $Mail[0]["attach_parts"];
		if (!(is_null($Attachment) || $Attachment == "")) {
			$x .= $this->Get_Span_Open("mail_form_attach");
			$x .= $this->Get_Span_Open("mail_form_label_content_line");
			for ($i=0; $i< sizeof($Attachment); $i++) {
				$x .= $this->Get_HyperLink_Open("view_attachment.php?AttachNum=".$i."&Folder=".$Folder."&MessageID=".$MessageID,'CLASS="imail_entry_read_link"');
				$x .= $this->Get_Image($SYS_CONFIG["Image"]["Abs"].'imail/icon_attachment.gif',12,18,'BORDER="0" ALIGN="absmiddle"');
				$x .= $Attachment[$i]['filename'];
				$x .= $this->Get_HyperLink_Close();
				$x .= "; ";
			}
			$x .= $this->Get_Span_Close();
			$x .= $this->Get_Span_Close();
		}
		//$x .= $this->Get_Div_Close();
		
		// Mail Body
		$x .= $this->Get_Div_Open("mail_form_content_board",'style="border:1px;"');
		$x .= $this->Get_Table_Open("","100%","",1,0,2);
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open();
		if ($Mail[0]["is_html_message"] == (-1)) {
			$OutputContent = str_replace(chr(10),'<br>',$Mail[0]['text_message']);
			$x .= $OutputContent;
		}
		else {
			$x .= $Mail[0]['message'];
		}
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		$x .= $this->Get_Table_Close();
		$x .= $this->Get_Div_Close();
		
		$x .= $this->Get_Div_Close();
		/*$x .= '<center>';
		$x .= $this->Get_Input_Button("Go","button",$Lang['btn_close_window'],'class="button" onclick="window.close();"');
		$x .= '</center>';*/
		
		return $x;
	}
	
	// get left menu of mail subsystem
	function Get_Left_Sub_Mail_Menu($CurMenu="",$CurTag="",$IMap=NULL,$Folder="") {
		global $Lang, $PathRelative, $CurPage, $SYS_CONFIG;
		
		// Ajax form
		$x = '
					<!-- YUI Panel -->
					<link rel="stylesheet" type="text/css" href="'.$PathRelative.'src/include/js/ajax/build/container/assets/skins/sam/container.css">   
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/yahoo-dom-event/yahoo-dom-event.js"></script>  
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/dragdrop/dragdrop-min.js"></script>
					<script type="text/javascript" src="'.$PathRelative.'src/include/js/ajax/build/container/container-min.js"></script>  
				 ';
				 
		$x .= $this->Get_Div_Open("NewFolderPanel");
		$x .= $this->Get_Form_Open("NewFolderForm","POST","new_folder_update.php");
		$x .= $this->Get_Div();
		$x .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03"').$this->Get_Span_Open("",'CLASS="bubble_board_04"');
		$x .= $Lang['email']['NewFolderPanelHeader']."<br><br>";
		$x .= $this->Get_Div_Open("",'style="width:90%"');
		$x .= $this->Get_Input_Text("FolderName","",'class="textbox"')."<br>";
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_save'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myPanel.hide()" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Input_Hidden("CurFolder","CurFolder",$Folder);
		$x .= $this->Get_Input_Hidden("CurTag","CurTag",$CurTag);
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();
		
		$x .= '					
					<script>
						var myPanel = new YAHOO.widget.Panel("NewFolderPanel", { width:"240px", visible:false, constraintoviewport:true });
						
						myPanel.render(document.body);
								
					function showNewFolderPanel(obj) {
						var pos_left = getPostion(obj,"offsetLeft")+25;
						var pos_top  = getPostion(obj,"offsetTop")-20;
					
						myPanel.cfg.setProperty("x", pos_left);
						myPanel.cfg.setProperty("y", pos_top);
					
						myPanel.show();
					}
					</script>
					<!-- End of YUI Panel -->
				 ';
		// end of Ajax form
		
		/*$x .= $this->Get_Div_Open("",'style="visibility:hidden; display:none;"');
		//$x .= $this->Get_Div_Open("",'');
		$x .= $this->Get_IFrame("0","FetchCache","FetchCache","no","0","0","","0","0");
		//$x .= $this->Get_IFrame("0","FetchCache","FetchCache","no","600","100","","0","0");
		$x .= $this->Get_Div_Close();*/
		
		// top button
		$x .= $this->Get_Div_Open("imail_left");
		$x .= $this->Get_Div_Open("mail_list", 'class="imail_left_function"');
		$x .= $this->Get_Div_Open("imail_left_function_item");
		/*$x .= $this->Get_Span_Open("IMapStatus", 'style="text-align:center"');
		$x .= $Lang['email']['MailCacheStatusIdle'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("imail_left_function_item");*/
		$x .= $this->Get_Span_Open("", 'style="text-align:center"');
		$x .= $this->Get_Input_Button("button", "check_mail", $Lang['email']['CheckMail'], 'onclick="window.location = \''.$PathRelative.'src/module/email/imail_inbox.php\'" class="button_main_act button_imail_menu"').'&nbsp;';
		//$x .= $this->Get_Input_Button("button", "check_mail", $Lang['email']['CheckMail'], 'class="button_main_act button_imail_menu" onclick="document.getElementById(\'FetchCache\').src = \'get_mail.php\';"').'&nbsp;';
		$x .= $this->Get_Input_Button("button", "compose_mail", $Lang['email']['ComposeMail'], 'onclick="window.location = \''.$PathRelative.'src/module/email/compose_email.php?Folder='.$Folder.'&CurMenu='.$CurMenu.'&CurTag='.$CurTag.'\'" class="button_main_act button_imail_menu"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif', 150, 1);
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		
		// Search Form
		$x .= $this->Get_Div_Open("mail_list", ($CurMenu == '1')? 'class="imail_left_function_selected"':'class="imail_left_function_item"');
		$x .= $this->Get_Div_Open("imail_search");
		$x .= $this->Get_Form_Open("SearchForm","GET",$PathRelative."src/module/email/imail_search_result.php");
		$x .= $this->Get_Div_Open("",'style="width:75px;float:left;padding:2px; height: 22px;"');
		$x .= $this->Get_Input_Text("Keyword", "Search on Subject", 'class="textbox" onfocus="if (this.value == \'Search on Subject\') this.value=\'\';"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("",'style="width:30px;float:left;padding-bottom:5px; height: 22px; padding-top:3px"');
		$x .= $this->Get_Input_Button("Go", "button", $Lang['email']['Go'], 'class="button button_go" onclick="document.SearchForm.submit();"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Open("",'style="width:70px;float:left;padding:2px;padding-left:2px; padding-top:7px; height: 22px;"');
		$x .= $this->Get_Span_Open("",'CLASS="imail_menu_adv_search"');
		/*$x .= "|";
		$x .= $this->Get_HyperLink_Open($PathRelative."src/module/email/imail_advance_search.php");
		//$x .= $this->Get_HyperLink_Open("#", 'onclick="alert(\'' . $Lang['general']['UnderConstruction'] . '\')"');
		$x .= $Lang['email']['AdvSearch'];
		$x .= $this->Get_HyperLink_Close();*/
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		
		// mail box list
		$x .= $this->Get_Div_Open("imail_left_function_item",'style="width:185px"');
		$x .= $this->Get_Form_Open("ClearForm","GET");
		$x .= $this->Get_List_Open();
		$x .= $this->Get_List_Member($this->Get_Span_Open().$Lang['email']['MailFolder'].$this->Get_Span_Close(),  'ID="subtitle"');
		// get short info number of unseen message in folder
		$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($SYS_CONFIG['Mail']['FolderPrefix']);
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".$SYS_CONFIG['Mail']['FolderPrefix']."&Folder=".$SYS_CONFIG['Mail']['FolderPrefix'], 'class="imail_inbox"').$this->Get_Span_Open().$Lang['email']['Inbox']." ".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $SYS_CONFIG['Mail']['FolderPrefix'])? 'ID="current"':'');
		unset($DisplayUnseen);
		// get short info number of unseen message in folder
		//$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Sent");
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".$IMap->RootPrefix."Sent&Folder=".$IMap->RootPrefix."Sent",'CLASS="imail_sent"').$this->Get_Span_Open().$Lang['email']['Sent']." ".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $IMap->RootPrefix."Sent")? 'ID="current"':'');
		// get short info number of unseen message in folder
		//$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Draft");
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".$IMap->RootPrefix."Draft&Folder=".$IMap->RootPrefix."Draft",'CLASS="imail_draft"').$this->Get_Span_Open().$Lang['email']['Draft']." ".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $IMap->RootPrefix."Draft")? 'ID="current"':'');
		
		// get short info number of unseen message in folder
		//$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Spam");
		// $this->Get_Input_Button("Go","button",'['.$Lang['email']['Clear'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_clear" onclick="document.getElementById(\'SpamHyperLink\').href = \'#\'; Confirm_Submit_Page(\''.$Lang['Warning']['ClearSpamWarn'].'\',\'ClearForm\',\'clear_spam.php\');"').
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".$IMap->RootPrefix."Spam&Folder=".$IMap->RootPrefix."Spam",'CLASS="imail_spam" id="SpamHyperLink"').$this->Get_Span_Open().$Lang['email']['Spam']."&nbsp;&nbsp;".$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $IMap->RootPrefix."Spam")? 'ID="current"':'');
		// get short info number of unseen message in folder
		//$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($IMap->RootPrefix."Trash");
		// $this->Get_Input_Button("Go","button",'['.$Lang['email']['Clear'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_clear" onclick="document.getElementById(\'TrashHyperLink\').href = \'#\'; Confirm_Submit_Page(\''.$Lang['Warning']['ClearTrashWarn'].'\',\'ClearForm\',\'clear_trash.php\');"').
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".$IMap->RootPrefix."Trash&Folder=".$IMap->RootPrefix."Trash",'CLASS="imail_trash" id="TrashHyperLink"').$this->Get_Span_Open().$Lang['email']['Trash']." ".$DisplayUnseen."&nbsp;&nbsp;".$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $IMap->RootPrefix."Trash")? 'ID="current"':'');
		$x .= $this->Get_Input_Hidden("Folder","Folder",$Folder);
		
		//$x .= $this->Get_List_Member($this->Get_Span_Open().$Lang['email']['PersonalFolder']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->Get_Input_Button("Go","button",'['.$Lang['email']['FolderNew'], 'OnClick="showNewFolderPanel(this)" class="imail_menu_sub_btn imail_menu_sub_btn_new"')."|".$this->Get_Input_Button("Go","button",$Lang['email']['FolderManage'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_manage" onClick="MM_goToURL(\'parent\',\'imail_myfolder.php\');return document.MM_returnValue"').$this->Get_Span_Close(), 'id="subtitle"');
		$x .= $this->Get_List_Member($this->Get_Span_Open().$Lang['email']['PersonalFolder']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$this->Get_Input_Button("Go","button",'['.$Lang['email']['FolderNew'], 'OnClick="showNewFolderPanel(this)" class="imail_menu_sub_btn imail_menu_sub_btn_new"')."|".$this->Get_Input_Button("Go","button",$Lang['email']['FolderManage'].']', 'class="imail_menu_sub_btn imail_menu_sub_btn_manage" onclick="window.location = \''.$PathRelative.'src/module/email/manage_folder.php\';"').$this->Get_Span_Close(), 'id="subtitle"');
		
		$TabSpacing = 6;
		if ($IMap!="") {
			if (in_array($Folder,$IMap->DefaultFolderList)) 	
				$FolderList = $IMap->Get_Folder_List_In_Path($SYS_CONFIG['Mail']['FolderPrefix'],true);
			else 
				$FolderList = $IMap->Get_Folder_List_In_Path($Folder,true);
			if ($Folder != "") {
				$FolderPath = explode($IMap->folderDelimiter, $Folder);
			}
			
			// folder path
			if (!in_array($Folder,$IMap->DefaultFolderList)) {
				for ($i=0; $i< sizeof($FolderPath); $i++) {
					// get folder full path for back navigation
					if ($i == 0) {
						$FolderFullPath .= $FolderPath[$i];
						if ($Folder != $SYS_CONFIG['Mail']['FolderPrefix']) 
							$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=6&Folder=".$FolderFullPath,'class="imail_folder"').$this->Get_Span_Open().$this->Get_Image($SYS_CONFIG['Image']['Abs']."icon_folder_up.gif",14,14,'border="0"').$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && ($CurTag == 6 || $CurTag == ""))? 'id="current"':'');
					}
					else {
						$FolderFullPath .= $IMap->folderDelimiter.$FolderPath[$i];
					}
					
					// get short info number of unseen message in folder
					//$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($FolderFullPath);
					
					if ($i != 0) $x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".urlencode($FolderFullPath)."&Folder=".urlencode($FolderFullPath),'class="imail_folder" style="background-position:'.$TabSpacing.'px 2px"').$this->Get_Span_Open("",'style="padding-left:'.($TabSpacing+15).'px"').$FolderPath[$i]."..".$DisplayUnseen.$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $FolderFullPath)? 'ID="current"':'');
					
					$TabSpacing = $TabSpacing + 10;
				}
			}
			else {
				for ($i=0; $i< sizeof($FolderPath); $i++) {
					// get folder full path for back navigation
					if ($i == 0) {
						$FolderFullPath .= $FolderPath[$i];
					}
					else {
						$FolderFullPath .= $IMap->folderDelimiter.$FolderPath[$i];
					}
				}
				if (!in_array($Folder,$IMap->DefaultFolderList)) 
					$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=6&Folder=".urlencode($FolderFullPath),'class="imail_folder"').$this->Get_Span_Open().$this->Get_Image($SYS_CONFIG['Image']['Abs']."icon_folder_up.gif",14,14,'border="0"').$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '1' && ($CurTag == 6 || $CurTag == ""))? 'id="current"':'');
			}
			
			// folder list
			for ($i=0; $i< sizeof($FolderList); $i++) {
				$FolderDisplayName = $IMap->getMailDisplayFolder($FolderList[$i]);
				
				// get short info number of unseen message in folder
				//$DisplayUnseen = $IMap->Get_Unseen_Mail_Number($FolderList[$i]);
				
				$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_inbox.php?CurTag=".urlencode($FolderList[$i])."&Folder=".urlencode($FolderList[$i]),'class="imail_folder" style="background-position:'.$TabSpacing.'px 2px"').$this->Get_Span_Open("",'style="padding-left:'.($TabSpacing+15).'px"').$FolderDisplayName." ".$DisplayUnseen.$this->Get_Span_Close.$this->Get_HyperLink_Close(), ($CurMenu == '1' && $CurTag == $FolderList[$i])? 'ID="current"':'');
			}
		}
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif',150,1);
		$x .= $this->Get_Div_Close();
		
		//address book
		$x .= $this->Get_Div_Open("addressbook", ($CurMenu == '2')? 'class="imail_left_function_selected"':'class="imail_left_function"');
		$x .= $this->Get_Div_Open("imail_left_function_item");
		$x .= $this->Get_List_Open();
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/address_book/imail_address_book.php",'class="imail_address_book"').$this->Get_Span_Open().$Lang['email']['AddressBook'].$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '2')? 'ID="current"':'');
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif',150,1);
		$x .= $this->Get_Div_Close();
		
		// perference
		$x .= $this->Get_Div_Open("preference",($CurMenu == '3')? 'class="imail_left_function_selected"':'class="imail_left_function"');
		$x .= $this->Get_Div_Open("imail_left_function_item");
		$x .= $this->Get_List_Open();
		$x .= $this->Get_List_Member($this->Get_HyperLink_Open($PathRelative."src/module/email/imail_preference.php",'class="imail_preference"').$this->Get_Span_Open().$Lang['email']['Preference'].$this->Get_Span_Close().$this->Get_HyperLink_Close(), ($CurMenu == '3' && $CurTag == '1')? 'ID="current"':'');
		$x .= $this->Get_List_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs'].'spacer.gif',150,1);
		$x .= $this->Get_Div_Close();
		$x .= "&nbsp;";
		$x .= $this->Get_Div_Close();
					
		return $x;
	}
	
	// get tool button bar of Mail system
	function Get_Tool_Bar($LeftMenu=array(),$RightMenu=array(),$MainID="",$LeftID="",$RightID="") {
		$MainID = ($MainID=="")? "imail_toolbar":$MainID;
		$LeftID = ($LeftID=="")? "imail_toolbar_right":$LeftID;
		$RightID = ($RightID=="")? "imail_toolbar_left":$RightID;
		$x .= $this->Get_Div_Open($MainID);
		$x .= $this->Get_Div_Open($LeftID);
		for ($i=0; $i< sizeof($LeftMenu); $i++) {
			$x .= $LeftMenu[$i]." ";
		}
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open($RightID);
		for ($i=0; $i< sizeof($RightMenu); $i++) {
			$x .= $RightMenu[$i]." ";
		}
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		
		return $x;
	}
	
	function Get_Compose_Mail_Form($Mail="",$Action="",$Folder="",$Signature="") {
		global $SYS_CONFIG, $Lang, $PathRelative;
		
		$user = new user($_SESSION['SSV_USERID']);
		$AliasList = $user->Get_User_Alias_List();
		for ($i=0; $i< sizeof($AliasList); $i++) {
			$CheckMailList[] = $AliasList[$i][0];
		}
		$CheckMailList[] = $user->Email;
		
		// process data for Reply/ ReplyAll/ Forward
		$fromList = $Mail[0]['fromDetail'];
		for ($i=0; $i< sizeof($fromList); $i++) {			
			if (trim($fromList[$i]->personal) != "") {
				$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
				$MailBodyFrom .= "\"".$DisplayName."\" &lt; ".$fromList[$i]->mailbox."@".$fromList[$i]->host." &gt;; ";
				if (!in_array($fromList[$i]->mailbox."@".$fromList[$i]->host,$CheckMailList)) 
					$fromAddress .= "\"".$DisplayName."\" &lt;".$fromList[$i]->mailbox."@".$fromList[$i]->host."&gt;; ";
			}
			else {
				$MailBodyFrom .= $fromList[$i]->mailbox."@".$fromList[$i]->host."; ";
				if (!in_array($fromList[$i]->mailbox."@".$fromList[$i]->host,$CheckMailList)) 
					$fromAddress .= $fromList[$i]->mailbox."@".$fromList[$i]->host."; ";
			}
		}
		
		$toList = $Mail[0]['toDetail'];
		for ($i=0; $i< sizeof($toList); $i++) {			
			if (trim($toList[$i]->personal) != "") {
				$DisplayObject = imap_mime_header_decode(imap_utf8($toList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
				$MailBodyTo .= "\"".$DisplayName."\" &lt; ".$toList[$i]->mailbox."@".$toList[$i]->host." &gt;; ";
				if (!in_array($toList[$i]->mailbox."@".$toList[$i]->host,$CheckMailList)) 
					$toAddress .= "\"".$DisplayName."\" &lt;".$toList[$i]->mailbox."@".$toList[$i]->host."&gt;; ";
			}
			else {
				$MailBodyTo .= " ".$toList[$i]->mailbox."@".$toList[$i]->host."; ";
				if (!in_array($toList[$i]->mailbox."@".$toList[$i]->host,$CheckMailList)) 
					$toAddress .= " ".$toList[$i]->mailbox."@".$toList[$i]->host."; ";
			}
		}
		
		$ccList = $Mail[0]['ccDetail'];
		for ($i=0; $i< sizeof($ccList); $i++) {
			if (trim($ccList[$i]->personal) != "") {
				$DisplayObject = imap_mime_header_decode(imap_utf8($ccList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
				$MailBodyCc .= "\"".$DisplayName."\" &lt; ".$ccList[$i]->mailbox."@".$ccList[$i]->host." &gt;; ";
				if (!in_array($ccList[$i]->mailbox."@".$ccList[$i]->host,$CheckMailList)) 
					$ccAddress .= "\"".$DisplayName."\" &lt;".$ccList[$i]->mailbox."@".$ccList[$i]->host."&gt;; ";
			}
			else {
				$MailBodyCc .= " ".$ccList[$i]->mailbox."@".$ccList[$i]->host."; ";
				if (!in_array($ccList[$i]->mailbox."@".$ccList[$i]->host,$CheckMailList)) 
					$ccAddress .= " ".$ccList[$i]->mailbox."@".$ccList[$i]->host."; ";
			}
		}
		
		$bccList = $Mail[0]['bccDetail'];
		for ($i=0; $i< sizeof($bccList); $i++) {
			if (trim($ccList[$i]->personal) != "") {
				$DisplayObject = imap_mime_header_decode(imap_utf8($bccList[$i]->personal));
				$DisplayName = $DisplayObject[0]->text;
				$MailBodyBcc .= "\"".$bccList[$i]->personal."\" &lt; ".$bccList[$i]->mailbox."@".$bccList[$i]->host." &gt;; ";
				$bccAddress .= "\"".$bccList[$i]->personal."\" &lt;".$bccList[$i]->mailbox."@".$bccList[$i]->host."&gt;; ";
			}
			else {
				$MailBodyBcc .= " ".$bccList[$i]->mailbox."@".$bccList[$i]->host."; ";
				$bccAddress .= " ".$bccList[$i]->mailbox."@".$bccList[$i]->host."; ";
			}
		}
		
		$Subject = $Mail[0]['subject'];
		$Body = $Mail[0]['message'];
		$Priority = ($Mail[0]['Priority'] == 1)? true:false;
		$DateReceived = $Mail[0]['dateReceive'];
		
		// setup the email field by Draft/ Reply/ ReplyAll/ Forward
		switch ($Action) {
			case "Reply":
				$FinalTo = $fromAddress;
				$MailInfo = '<br>
										 <hr>
										 <b>From:</b> '.$MailBodyFrom.'<br>
										 <b>Sent:</b> '.$DateReceived.'<br>';
				$MailInfo .= ($MailBodyTo != "")? '<b>To:</b> '.$MailBodyTo.'<br>':'';
				$MailInfo .= ($MailBodyCc != "")? '<b>Cc:</b> '.$MailBodyCc.'<br>':'';
				$MailInfo .= '<b>Subject:</b> '.$Subject.'<br><br><br>';
				$Subject = "RE: ".$Subject;
				$FinalBody = $MailInfo.$Body;
				break;
			case "ReplyAll":
				$FinalTo = $fromAddress.$toAddress;
				$FinalCc = $ccAddress;
				$MailInfo = '<br>
										 <hr>
										 <b>From:</b> '.$MailBodyFrom.'<br>
										 <b>Sent:</b> '.$DateReceived.'<br>';
				$MailInfo .= ($MailBodyTo != "")? '<b>To:</b> '.$MailBodyTo.'<br>':'';
				$MailInfo .= ($MailBodyCc != "")? '<b>Cc:</b> '.$MailBodyCc.'<br>':'';
				$MailInfo .= '<b>Subject:</b> '.$Subject.'<br><br><br>';
				$Subject = "RE: ".$Subject;
				$FinalBody = $MailInfo.$Body;
				break;
			case "Forward":
				$MailInfo = '<br>
										 <hr>
										 <b>From:</b> '.$MailBodyFrom.'<br>
										 <b>Sent:</b> '.$DateReceived.'<br>';
				$MailInfo .= ($MailBodyTo != "")? '<b>To:</b> '.$MailBodyTo.'<br>':'';
				$MailInfo .= ($MailBodyCc != "")? '<b>Cc:</b> '.$MailBodyCc.'<br>':'';
				$MailInfo .= '<b>Subject:</b> '.$Subject.'<br><br><br>';
				$Subject = "FW: ".$Subject;
				$FinalBody = $MailInfo.$Body;
				break;
			default;
				$FinalTo = $toAddress;
				$FinalCc = $ccAddress;
				$FinalBcc = $bccAddress;
				$FinalBody = $Body;
				break;
		}
		$FinalBody = $Signature.$FinalBody;
		// end process		
		
		// include jquery for auto expand
		/*$x .= '<script type="text/javascript" src="'.$SYS_CONFIG['sys_web_root'].'src/include/js/jquery/jquery.js"></script>
					 <script type="text/javascript" src="'.$SYS_CONFIG['sys_web_root'].'src/include/js/jquery/txtarea_auto_expand.js"></script>
					 
					 <script type="text/javascript">
					 $(document).ready (function() {
						
						 $(\'textarea.expanding\').autogrow();
												
					 });
 					 </script>
 					 
 					 <style type="text/css">

						textarea.expanding {
							line-height: 11px;
							min-height:30px;
							width: 96%;
							padding-right: 3px;
							padding-left: 3px;
							padding-top: 3px;
							font-family: Verdana;
							font-size: 11px;
						}
						
						</style>
					 ';*/
		// end of include jquery
						
		$x .= $this->Get_Div_Open("commontabs_board",'class="imail_mail_content"');
		// To Field
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['To'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		//$x .= $this->Get_Input_Text("ToAddress",$FinalTo,'class="textarea_autoexpand" style="width:96%;" onblur="HideDiv(\'to_list\')" onkeydown="return NavigateAddress(\'ToEmailList\', event)" onkeyup="return AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'\', \'ToEmailList\');"');
		//$x .= $this->Get_Textarea_Open("ToAddress",'class="textarea_compose" col="5" style="width:96%;" onblur="HideDiv(\'to_list\')" onkeydown="return NavigateAddress(\'ToEmailList\', event)" onkeyup="return AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'\', \'ToEmailList\');"');
		$x .= $this->Get_Textarea_Open("ToAddress",'class="textarea_compose" col="5" style="width:96%;" onblur="HideDiv(\'to_list\')" onkeydown="return Check_Tab(event,\'\')" onkeyup="return AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'\', \'ToEmailList\');"');
		//$x .= $this->Get_Textarea_Open("ToAddress",'class="expanding" col="5" onblur="HideDiv(\'to_list\')" onkeydown="return Check_Tab(event,\'\')" onkeyup="return AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'\', \'ToEmailList\');"');
		$x .= $FinalTo;
		$x .= $this->Get_Textarea_Close();
		//$x .= $this->Get_HyperLink_Open("javascript:window.open('select_school.php?fromSrc=ToAddress','','scrollbars=yes,width=350,height=350')");
		$x .= $this->Get_HyperLink_Open("#", 'onclick="window.open(\'select_school.php?fromSrc=ToAddress\',\'\',\'scrollbars=yes,width=350,height=350\');return false"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0"');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Div_Open("to_list", 'class="added_item_ppl_list_board" onmouseover="EmailListFocus=true" onmouseout="EmailListFocus=false" style="display:none;"');
		$x .= $this->Get_Div_Open("added_item_ppl_list", '');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br();
		
		// Cc Field
		//$Display = ($FinalCc != "")? 'inline':'none';
		$Display = 'inline';
		$x .= $this->Get_Span_Open("CcSpan",'style="display:'.$Display.'"');
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['Cc'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		//$x .= $this->Get_Input_Text("CcAddress",$FinalCc,'class="textarea_autoexpand" style="width:96%;" onblur="HideDiv(\'cc_list\')" onkeydown="return NavigateAddress(\'CcEmailList\', event)" onkeyup="AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'CC\',\'CcEmailList\');"');
		$x .= $this->Get_Textarea_Open("CcAddress",'class="textarea_compose" cols="3" style="width:96%;" onblur="HideDiv(\'cc_list\')" onkeydown="return Check_Tab(event,\'CC\')" onkeyup="AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'CC\',\'CcEmailList\');"');
		//$x .= $this->Get_Textarea_Open("CcAddress",'class="expanding" col="5" onblur="HideDiv(\'cc_list\')" onkeydown="return Check_Tab(event,\'CC\')" onkeyup="AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'CC\',\'CcEmailList\');"');
		$x .= $FinalCc;
		$x .= $this->Get_Textarea_Close();
		//$x .= $this->Get_HyperLink_Open("javascript:window.open('select_school.php?fromSrc=CcAddress','','scrollbars=yes,width=350,height=350')");
		$x .= $this->Get_HyperLink_Open("#", 'onclick="window.open(\'select_school.php?fromSrc=CcAddress\',\'\',\'scrollbars=yes,width=350,height=350\');return false"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0"');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Div_Open("cc_list", 'class="added_item_ppl_list_board" onmouseover="EmailListFocus=true" onmouseout="EmailListFocus=false" style="display:none;"');
		$x .= $this->Get_Div_Open("added_item_ppl_list_CC");
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		
		// Bcc Field
		$Display = ($FinalBcc != "")? 'inline':'none';
		//$Display = 'inline';
		$x .= $this->Get_Span_Open("BccSpan",'style="display:'.$Display.'"');
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['Bcc'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		//$x .= $this->Get_Input_Text("BccAddress",$FinalBcc,'class="textarea_autoexpand" style="width:96%;" onblur="HideDiv(\'bcc_list\')" onkeydown="return NavigateAddress(\'BccEmailList\', event)" onkeyup="AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'BCC\',\'BccEmailList\');"');
		$x .= $this->Get_Textarea_Open("BccAddress",'class="textarea_compose" cols="3" style="width:96%;" onblur="HideDiv(\'bcc_list\')" onkeydown="return Check_Tab(event,\'BCC\')" onkeyup="AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'BCC\',\'BccEmailList\');"');
		//$x .= $this->Get_Textarea_Open("BccAddress",'class="expanding" col="5" onblur="HideDiv(\'bcc_list\')" onkeydown="return Check_Tab(event,\'BCC\')" onkeyup="AutoComplete_Contact(this.value, \'Key\', event, \''.$PathRelative.'src/module/email/address_autoComplete.php\',\'BCC\',\'BccEmailList\');"');
		$x .= $FinalBcc;
		$x .= $this->Get_Textarea_Close();
		//$x .= $this->Get_HyperLink_Open("javascript:window.open('select_school.php?fromSrc=BccAddress','','scrollbars=yes,width=350,height=350')");
		$x .= $this->Get_HyperLink_Open("#", 'onclick="window.open(\'select_school.php?fromSrc=BccAddress\',\'\',\'scrollbars=yes,width=350,height=350\');return false"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0"');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Div_Open("bcc_list", 'class="added_item_ppl_list_board" onmouseover="EmailListFocus=true" onmouseout="EmailListFocus=false" style="display:none;"');
		$x .= $this->Get_Div_Open("added_item_ppl_list_BCC");
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();

		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line", 'style="padding-left: 70px;"');
		//$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="document.getElementById(\'CcSpan\').style.display = \'inline\';"');
		//$x .= $Lang['email']['AddCc'];
		//$x .= $this->Get_HyperLink_Close();
		//$x .= " | ";
		$x .= $this->Get_HyperLink_Open("#",'id="AddBccLink" class="imail_entry_read_link" onclick="document.getElementById(\'BccSpan\').style.display = \'inline\'; document.getElementById(\'AddBccLink\').style.display = \'none\';"');
		$x .= $Lang['email']['AddBcc'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		/*
		$x .= $this->Get_Span_Open("OptionsSpan");
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_content_line");
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="document.getElementById(\'CcSpan\').style.display = \'inline\';"');
		$x .= $Lang['email']['AddCc'];
		$x .= $this->Get_HyperLink_Close();
		$x .= " | ";
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="document.getElementById(\'BccSpan\').style.display = \'inline\';"');
		$x .= $Lang['email']['AddBcc'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();*/
		/*$x .= $this->Get_Span_Open("mail_form_label_button");		
		$x .= $this->Get_HyperLink_Open("#");
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_address_book.gif",20,20,'border="0"');
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();*/
		//$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br('style="clear: both"');
		
		// Subject Field
		$x .= $this->Get_Span_Open("mail_form_label");
		$x .= $Lang['email']['Subject'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line");
		$x .= $this->Get_Input_Text("Subject",$Subject,'class="textbox" onKeyDown="if (event.keyCode==13) document.getElementById(\'MailBody\').contentWindow.focus();"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br();
		
		// hidden Iframe to process attachment list
		$x .= $this->Get_IFrame("0","AttachListRefresh","AttachListRefresh","no","0","0","attach_list_refresh.php?uid=".$Mail[0]['uid']."&MailAction=".$Action.'&Folder='.$Folder,"0","0",'style="visibility:visible;"');
		$x .= $this->Get_Span_Open("AttachListIFrame");
		$x .= $this->Get_Span_Close();
		
		// attachment part
		$x .= $this->Get_Span_Open("mail_form_attach");
		$x .= $this->Get_Span_Open("mail_form_label_content_whole2");
		/*$x .= $this->Get_Div_Open("added_item_attachment");
		$x .= $this->Get_Span_Open("AttachmentList");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();*/
		$x .= $this->Get_Div_Open("add_attachment");
		$x .= $this->Get_Span_Open("AttachmentList");
		$x .= $this->Get_Span_Close();
		//$x .= $this->Get_Br();
		$x .= $this->Get_Span_Open("FileInputList");
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="AddFileImport()"');
		//$x .= $Lang['email']['AttachAnotherFile'];
		$x .= $Lang['email']['AttachFile'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		
		$x .= '
					<script>
					<!--
					var FileCount=0;
					function AddFileImport() {
						var Obj = document.getElementById("FileInputList");
						
						Obj.innerHTML += \'<span id="File\'+FileCount+\'"><br><input type="file" name="userfile\'+FileCount+\'" id="userfile\'+FileCount+\'" onchange="FileAttachSubmit(\'+FileCount+\')" class="textbox" style="width:400px" /> <a href="#" onclick="FileInputDelete(\'+FileCount+\');" />'.$Lang['email']['RemoveAttach'].'</a></span>\';
						FileCount++;
					}
					
					function FileAttachSubmit(FileNumber) {
						var AttachIframeList = document.getElementById("AttachListIFrame");
						AttachIframeList.innerHTML += \'<iframe width="0" height="0" name="FileOperation\'+FileNumber+\'" id="FileOperation\'+FileNumber+\'" src="" style="visibility:hidden;" />\';
						
						var Obj = document.getElementById("NewMailForm");
						Obj.target = "FileOperation"+FileNumber; 
						Obj.encoding = "multipart/form-data";
						Obj.action = "attach_update.php?FileInputNumber="+FileNumber;
						Obj.submit();
						
						var FileList = document.getElementById("File"+FileNumber);
						FileList.innerHTML = \'<br>"\' + document.getElementById("userfile"+FileNumber).value + \'" uploading...<br>\';
					}
					
					function FileDeleteSubmit(FileID) {
						Obj = document.getElementById("NewMailForm");
						Obj.target = "AttachListRefresh"; 
						Obj.action = "attach_remove.php?FileID="+FileID;
						
						Obj.submit();
					}
					
					function FileInputDelete(FileNumber) {
						var FileList = document.getElementById("File"+FileNumber);
						FileList.innerHTML = \'\';
					}
					//-->
					</script>
					';
		
		/*$x .= $this->Get_Span_Open("mail_form_label_content_whole",'style="visibility:visible"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_attachment.gif",12,18,'border="0" align="absmiddle"');
		$x .= $this->Get_HyperLink_Open("#",'class="imail_entry_read_link" onclick="window.open(\'attach.php?uid='.$Mail[0]['uid'].'\')"');
		$x .= $Lang['email']['AttachFile'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();*/
		
		$x .= $this->Get_Br();
		
		/* function not ready, hide it first
		$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_event.gif",16,20,'border="0" align="absmiddle"');
		$x .= $this->Get_HyperLink_Open("imail_compose_mail_wrote.htm",'class="imail_entry_read_link"');
		$x .= $Lang['email']['AddEventInvitation'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		$x .= $this->Get_Div_Open("mail_form_content_board");
		*/
		//$x .= $this->Get_Div_Open("",'CLASS="mail_msg"');
		
		$x .= $this->Get_Br('style="clear: both"');
		//text editor of email body
		$x .= $this->Get_Word_Editor("MailBody","MailBody",$FinalBody,true,"100%");
		// for spell checker
		//$x .= $this->Get_Textarea_Open("SpellCheckBox",'style="display:none; visibility:hidden; width:0px; height:0px;"').$this->Get_Textarea_Close();
		
		//$x .= $this->Get_Div_Close();
		//$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br();
		
		// for spell checker
		/*$x .= '<!-- Source the JavaScript spellChecker object -->';
		$x .= '<script language="javascript" type="text/javascript" src="'.$PathRelative.'src/include/js/speller/spellChecker.js">';
		$x .= '</script>';
		
		$x .= '<!-- Call a function like this to handle the spell check command -->';
		$x .= '<script language="javascript" type="text/javascript">';
		$x .= 'function openSpellChecker() {';
		$x .= '	document.getElementById("SpellCheckBox").value = document.getElementById("MailBody").contentWindow.document.body.innerHTML;';
		$x .= ' var txt = document.getElementById("SpellCheckBox");';
		$x .= '	var speller = new spellChecker( txt );';
		$x .= '	speller.openChecker();';
		$x .= '}';
		$x .= '</script>';*/
		
		// for spell checker
		//$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		//$x .= $this->Get_Hyperlink_Open("#",'class="imail_entry_read_link" onclick="openSpellChecker()"');
		//$x .= $Lang['email']['SpellChecker'];
		//$x .= $this->Get_Hyperlink_Close();
		//$x .= $this->Get_Span_Close();
		//$x .= $this->Get_Br();
		
		$x .= $this->Get_Span_Open("mail_form_label_content_whole");
		$x .= $this->Get_Input_CheckBox("ImportantFlag","ImportantFlag",1,$Priority);
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_important.gif",9,16,'align="absmiddle"');
		$x .= $Lang['email']['Important'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
					
		return $x;
	}
	
	function Get_Advance_Search_Form($Keyword="",$Subject="",$From="",$To="",$Body="",$FolderStructure=array(),$SearchFolderList=array()) {
		Global $Lang, $SYS_CONFIG;
		Global $Folder, $MessageID, $PathRelative;	
		
		$x .= $this->Get_Div_Open("imail_right");
		$x .= $this->Get_Form_Open("NewMailForm","GET",$PathRelative."src/module/email/imail_search_result.php");
		$x .= $this->Get_Div_Open("commontabs_board",'class="imail_mail_content"');
		
		// Keyword
		$x .= $this->Get_Span_Open("mail_form_label",'style="width:15%"');
		$x .= $Lang['email']['SearchKeyword'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line",'style="width:80%"');
		$x .= $this->Get_Input_Text("Keyword",$Keyword,'class="textbox" style="width:96%;"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br('style="clear: both"');
		
		// Search by
		$SearchField[] = array($Lang['email']['From'],"From");
		$SearchField[] = array($Lang['email']['To'],"To");
		$SearchField[] = array($Lang['email']['Cc'],"Cc");
		$SearchField[] = array($Lang['email']['Subject'],"Subject");
		$SearchField[] = array($Lang['email']['Body'],"Body");
		
		$x .= $this->Get_Span_Open("mail_form_label",'style="width:15%"');
		$x .= $Lang['email']['SearchInField'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line",'style="width:80%"');
		$x .= $this->Get_Input_Select("SearchField[]","SearchField[]",$SearchField,"",'multiple="true" class="demotextbox" size="5"');
		$x .= $this->Get_Input_CheckBox("AllFieldCheck","AllFieldCheck","",false,'onclick="Select_All(document.getElementById(\'SearchField[]\'),document.getElementById(\'AllFieldCheck\'))"');
		$x .= $Lang['email']['All'];
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Span_Open("",'class="content"');
		$x .= $Lang['email']['MultiSelectMsg'];
    $x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br('style="clear: both"');
		
		// dot line
		$x .= $this->Get_Dot_Line();
		
		// Folder List		
		$x .= $this->Get_Span_Open("mail_form_label",'style="width:15%"');
		$x .= $Lang['email']['SearchInFolderList'];
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line",'style="width:80%"');
		$x .= $this->Get_Input_Select_With_Label("SearchFolder[]","SearchFolder[]",$FolderStructure,"",'multiple="true" class="demotextbox" size="10"');
		$x .= $this->Get_Input_CheckBox("AllFolderCheck","AllFolderCheck","",false,'onclick="Select_All(document.getElementById(\'SearchFolder[]\'),document.getElementById(\'AllFolderCheck\'))"');
		$x .= $Lang['email']['All'];
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Span_Open("",'class="content"');
		$x .= $Lang['email']['MultiSelectMsg'];
    $x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br('style="clear: both"');
		
		// dot line
		$x .= $this->Get_Dot_Line();
		
		// Date Range
		$x .= $this->Get_Span_Open("mail_form_label",'style="width:15%"');
		$x .= "Date Range";
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Open("mail_form_label_textbox_line",'style="width:80%"');
		$x .= $Lang['email']['FromDate'].'&nbsp;';
		$x .= $this->Get_Input_Text("FromDate","",'class="textbox" style="width:20%"').' (YYYY-MM-DD)';
		$x .= '&nbsp;';
		$x .= $Lang['email']['ToDate'].'&nbsp;';
		$x .= $this->Get_Input_Text("ToDate","",'class="textbox" style="width:20%"').' (YYYY-MM-DD)';
		$x .= $this->Get_Br('style="clear: both"');
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Br('style="clear: both"');
		
		$x .= $this->Get_Div_Open("", 'id="form_btn"');
		$x .= $this->Get_Input_Submit("btnSubmit", "btnSubmit", $Lang['btn_search'], 'class="button_main_act"');
		$x .= " ";
		$x .= $this->Get_Reset_Button("btnReset", "btnReset", $Lang['btn_reset'], 'class="button_main_act"');
		$x .= " ";
		$x .= $this->Get_Input_Button("btnCancel", "btnCancel", $Lang['btn_cancel'], 'class="button_main_act" onclick="window.location=\'imail_inbox.php\'"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();

		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		
		return $x;
	}
	
	function Get_Manage_Folder_Form($Folder,$IMap) {
		global $PathRelative, $SYS_CONFIG, $Lang;
		
		$FolderList = $IMap->Get_Folder_Structure(true,false);
		
		$x .= $this->Get_Div_Open("imail_right");
		$x .= $this->Get_Form_Open("form1","POST");
		$x .= $this->Get_Input_Hidden("CurFolder","CurFolder");
		$x .= $this->Get_Div_Open("sub_page_title");
		$x .= $Lang['email']['PersonalFolder'];
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("btn_link",'style="width:100px"');
		$x .= $this->Get_HyperLink_Open("#",'onclick="showNewFolderPanel2(this)"');
		$x .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."icon_add.gif",20,20,'border="0" align="absmiddle"');
		$x .= $Lang['email']['ManageFolderNew'];
		$x .= $this->Get_HyperLink_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Open("imail_mail_list");
		$x .= $this->Get_Table_Open("","100%","100%",0,0,0);
		$x .= $this->Get_Table_Row_Open();
		$x .= $this->Get_Table_Cell_Open("",'class="imail_entry_top" nowrap');
		$x .= $Lang['email']['Folder'];
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open("",'class="imail_entry_top" nowrap');
		$x .= $Lang['email']['TotalMailInFolder'];
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Cell_Open("",'class="imail_entry_top" nowrap');
		$x .= '&nbsp;';
		$x .= $this->Get_Table_Cell_Close();
		$x .= $this->Get_Table_Row_Close();
		
		// display folder List
		for ($i=0; $i< sizeof($FolderList); $i++) {
			$NumOfMail = $IMap->Check_Mail_Count($FolderList[$i][1]);
			$Temp = $FolderList[$i][0];
			for ($j=0;$j< strlen($Temp); $j+=strlen('&nbsp;&nbsp;')) {
				if (substr($Temp,$j,strlen('&nbsp;&nbsp;')) != '&nbsp;&nbsp;') {
					$DisplayFolderName = substr($Temp,0,$j);
					$DisplayFolderName .= $this->Get_Image($SYS_CONFIG['Image']['Abs']."imail/icon_folder.gif",20,20,'border="0" align="absmiddle"');
					$DisplayFolderName .= substr($Temp,$j,strlen($Temp));
					break;
				}
			}
			
			$x .= $this->Get_Table_Row_Open();
			$x .= $this->Get_Table_Cell_Open("",'class="imail_entry_unread" nowrap');
			$x .= $DisplayFolderName;
			$x .= $this->Get_Table_Cell_Close();
			$x .= $this->Get_Table_Cell_Open("",'class="imail_entry_unread" nowrap');
			$x .= $NumOfMail;
			$x .= $this->Get_Table_Cell_Close();
			$x .= $this->Get_Table_Cell_Open("",'align="right" nowrap class="imail_entry_unread sub_link"');
			$x .= '[ '.$this->Get_HyperLink_Open("javascript: showNewFolderPanel1(document.getElementById('New".$i."'),'".$FolderList[$i][1]."')",'id="New'.$i.'"').$Lang['email']['CreateSubFolder'].$this->Get_HyperLink_Close();
			$x .= ' | '.$this->Get_HyperLink_Open("javascript: showRenameFolderPanel(document.getElementById('Rename".$i."'),'".$FolderList[$i][1]."')",'id="Rename'.$i.'"').$Lang['email']['RenameFolder'].$this->Get_HyperLink_Close();
			$x .= ' | '.$this->Get_HyperLink_Open("#",'onclick="document.form1.CurFolder.value = \''.$FolderList[$i][1].'\'; Confirm_Submit_Page(\''.$Lang['Warning']['DeleteFolderWarn'].'\',\'form1\',\'delete_folder_update.php\');"').$Lang['email']['DeleteFolder'].$this->Get_HyperLink_Close().']';
			$x .= $this->Get_Table_Cell_Close();
			$x .= $this->Get_Table_Row_Close();
		}
		$x .= $this->Get_Table_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();
		
		// rename folder name form
		$x .= $this->Get_Div_Open("RenameFolderPanel");
		$x .= $this->Get_Form_Open("RenameFolderForm","POST","rename_folder_update.php");
		$x .= $this->Get_Div();
		$x .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03_b"').$this->Get_Span_Open("",'CLASS="bubble_board_04_b"');
		$x .= $Lang['email']['NewFolderPanelHeader']."<br><br>";
		$x .= $this->Get_Div_Open("",'style="width:90%"');
		$x .= $this->Get_Input_Text("FolderName","",'class="textbox"')."<br>";
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_save'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myPanel1.hide()" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Input_Hidden("CurFolder","CurFolder",$Folder);
		$x .= $this->Get_Input_Hidden("CurTag","CurTag",$CurTag);
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();
		
		// New Folder Form with right arrow
		$x .= $this->Get_Div_Open("NewFolderPanel1");
		$x .= $this->Get_Form_Open("NewFolderForm1","POST","new_folder_update.php");
		$x .= $this->Get_Div();
		$x .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03_b"').$this->Get_Span_Open("",'CLASS="bubble_board_04_b"');
		$x .= $Lang['email']['NewFolderPanelHeader']."<br><br>";
		$x .= $this->Get_Div_Open("",'style="width:90%"');
		$x .= $this->Get_Input_Text("FolderName","",'class="textbox"')."<br>";
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_save'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myPanel2.hide()" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Input_Hidden("CurFolder","CurFolder",$Folder);
		$x .= $this->Get_Input_Hidden("CurTag","CurTag",$CurTag);
		$x .= $this->Get_Input_Hidden("ReturnPage","ReturnPage","manage_folder.php");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();
		
		// NEW Folder form with left arrow
		$x .= $this->Get_Div_Open("NewFolderPanel2");
		$x .= $this->Get_Form_Open("NewFolderForm2","POST","new_folder_update.php");
		$x .= $this->Get_Div();
		$x .= $this->Get_Div_Open("sub_layer_imail_rename_folder");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_01"').$this->Get_Span_Open("",'CLASS="bubble_board_02"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_03"').$this->Get_Span_Open("",'CLASS="bubble_board_04"');
		$x .= $Lang['email']['NewFolderPanelHeader']."<br><br>";
		$x .= $this->Get_Div_Open("",'style="width:90%"');
		$x .= $this->Get_Input_Text("FolderName","",'class="textbox"')."<br>";
		$x .= $this->Get_Div_Open("form_btn");
		$x .= $this->Get_Input_Submit("Submit","Submit",$Lang['btn_save'],'class="button_main_act"')." ".$this->Get_Input_Button("Submit","Submit",$Lang['btn_cancel'],'OnClick="myPanel3.hide()" CLASS="button_main_act"');
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Span_Close();
		$x .= $this->Get_Input_Hidden("CurFolder","CurFolder",$Folder);
		$x .= $this->Get_Input_Hidden("CurTag","CurTag",$CurTag);
		$x .= $this->Get_Input_Hidden("ReturnPage","ReturnPage","manage_folder.php");
		$x .= $this->Get_Span_Open("",'CLASS="bubble_board_05"').$this->Get_Span_Open("",'CLASS="bubble_board_06"').$this->Get_Span_Close().$this->Get_Span_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Div_Close();
		$x .= $this->Get_Form_Close();
		$x .= $this->Get_Div_Close();

		// YUI script				
		$x .= '					
					<script>
						var myPanel1 = new YAHOO.widget.Panel("RenameFolderPanel", { width:"240px", visible:false, constraintoviewport:true });
						var myPanel2 = new YAHOO.widget.Panel("NewFolderPanel1", { width:"240px", visible:false, constraintoviewport:true });
						var myPanel3 = new YAHOO.widget.Panel("NewFolderPanel2", { width:"240px", visible:false, constraintoviewport:true });
						
						myPanel1.render(document.body);
						myPanel2.render(document.body);
						myPanel3.render(document.body);
								
					function showRenameFolderPanel(obj,FolderValue) {
						document.RenameFolderForm.CurFolder.value = FolderValue;
					
						var pos_left = getPostion(obj,"offsetLeft")-260;
						var pos_top  = getPostion(obj,"offsetTop")-22;
					
						myPanel1.cfg.setProperty("x", pos_left);
						myPanel1.cfg.setProperty("y", pos_top);
					
						myPanel1.show();
					}
								
					function showNewFolderPanel1(obj,FolderValue) {
						document.NewFolderForm1.CurFolder.value = FolderValue;
						
						var pos_left = getPostion(obj,"offsetLeft")-260;
						var pos_top  = getPostion(obj,"offsetTop")-22;
						
						
						myPanel2.cfg.setProperty("x", pos_left);
						myPanel2.cfg.setProperty("y", pos_top);
						
						myPanel2.show();
					}
					
					function showNewFolderPanel2(obj) {
						document.NewFolderForm2.CurFolder.value = "";
						
						var pos_left = getPostion(obj,"offsetLeft")+10;
						var pos_top  = getPostion(obj,"offsetTop")-15;
						
						
						myPanel3.cfg.setProperty("x", pos_left);
						myPanel3.cfg.setProperty("y", pos_top);
						
						myPanel3.show();
					}
					</script>
					<!-- End of YUI Panel -->';	
					
		return $x;
	}
	
	function Get_Switch_MailBox($UserID="") {
		global $Lang, $PathRelative;
		if ($UserID == "") $UserID = $_SESSION['SSV_USERID'];
		$User = new user($UserID);
		
		$UserPrimaryMailBox = $User->Get_User_Primary_Mail();
		$UserMailBoxList = $User->Get_User_Shared_MailBox();
		
		$MailBoxList = array_merge($UserPrimaryMailBox,$UserMailBoxList);	
		
		if (sizeof($MailBoxList) > 1) {
			$x .= $this->Get_Div_Open("module_content_header_sub");
			$x .= $this->Get_Form_Open("SwitchMailBoxForm","POST",$PathRelative."src/module/email/switch_mailbox.php");
			$x .= $Lang['email']['SwitchMailBox'];
			$x .= $this->Get_Input_Select("MailBoxName","MailBoxName",$MailBoxList,$_SESSION['SSV_EMAIL_LOGIN'],'onchange="document.getElementById(\'SwitchMailBoxForm\').submit();"');
			$x .= $this->Get_Form_Close();
			$x .= $this->Get_Div_Close();
		}
			
		return $x;					
	}
}
