<?php
/*
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

if (!defined("LIBPB_DEFINED"))                     // Preprocessor directive
{
define("LIBPB_DEFINED", true);
$rb_image_path = "/images/resourcesbooking";
class libpb extends libdb {

     var $UserID;
     var $PeriodicBookingID;
     var $ResourceID;
     var $AppliedUserID;
     var $BookingStartDate;
     var $BookingEndDate;
     var $TimeSlots;
     var $Remark;
     var $RecordType;
     var $TypeValues;
     var $RecordStatus;
     var $TimeApplied;
     var $LastAction;
     var $no_msg;

     ###############################################################################

     function libpb(){
          global $UserID,$i_no_record_exists_msg;
          $this->libdb();
          $this->UserID = $UserID;
          $this->no_msg = $i_no_record_exists_msg;
          $this->PeriodicBookingID = "";
     }

     function retrievePeriodicBooking($pid)
     {
              $this->PeriodicBookingID = $pid;
              $sql = "SELECT ResourceID,UserID,BookingStartDate,BookingEndDate,TimeSlots,Remark,RecordType,TypeValues,RecordStatus,TimeApplied,LastAction FROM INTRANET_PERIODIC_BOOKING WHERE PeriodicBookingID = '$pid'";
              $rs = $this->db_db_query($sql);
              if ($rs)
              {
                  $this->rs = $rs;
                  $result = $this->db_fetch_array();
                  list($this->ResourceID,$this->AppliedUserID,$this->BookingStartDate,$this->BookingEndDate,$this->TimeSlots,$this->Remark,$this->RecordType,$this->TypeValues,$this->RecordStatus,$this->TimeApplied,$this->LastAction) = $result;
              }
     }

     function retrieveDates()
     {
              if ($this->PeriodicBookingID == "") return array();
              $sql = "SELECT DISTINCT BookingDate FROM INTRANET_BOOKING_RECORD WHERE PeriodicBookingID = '".$this->PeriodicBookingID."' AND (RecordStatus NOT IN (5) OR RecordStatus = '".$this->RecordStatus."') ORDER BY BookingDate";
              return $this->returnVector($sql);
     }
     function retrieveDetailedInfo()
     {
              if ($this->PeriodicBookingID == "") return array();
              # Get Batch ID of this item
              $sql = "SELECT TimeSlotBatchID FROM INTRANET_RESOURCE WHERE ResourceID = '".$this->ResourceID."'";
              $temp = $this->returnVector($sql);
              $batchID = $temp[0];
              if ($batchID == "")
              {
                  return;
              }
              $sql = "SELECT DATE_FORMAT(a.BookingDate,'%Y-%m-%d'), b.Title, b.TimeRange, a.RecordStatus
                             FROM INTRANET_BOOKING_RECORD as a
                                  LEFT OUTER JOIN INTRANET_SLOT as b ON b.BatchID = '$batchID' AND b.SlotSeq = a.TimeSlot
                             WHERE a.PeriodicBookingID = '".$this->PeriodicBookingID."'
                             ORDER BY a.BookingDate, a.TimeSlot";
              return $this->returnArray($sql,4);
     }

     function listEditPeriodicTimeSlots($pid="")
     {
              global $rb_image_path, $i_BookingTimeSlots,$i_BookingAppliedByU,$i_BookingStatusArray,$i_BookingStatus;
              if ($pid != "") $this->retrievePeriodicBooking($pid);
              $time_slots = $this->TimeSlots;
              $rid = $this->ResourceID;
              $status = $this->RecordStatus;
              $sql = "SELECT b.SlotSeq, b.Title, b.TimeRange, IF (b.SlotSeq IN ($time_slots),1,0) FROM INTRANET_RESOURCE as a, INTRANET_SLOT as b WHERE a.TimeSlotBatchID = b.BatchID AND a.ResourceID = '$rid' ORDER BY b.SlotSeq";
              $slots = $this->returnArray($sql,4);

              $x .= "<table width=408 border=0 cellspacing=0 cellpadding=0 background='$rb_image_path/timeslotsbg2.gif' class=body>
                <tr>
                  <td align=center width=134 class=h1 style=\"background-image: url('$rb_image_path/timeslotstop1a.gif'); vertical-align: middle\">$i_BookingTimeSlots</td>
                  <td align=center width=200 style=\"background-image: url('$rb_image_path/timeslotstop1c.gif'); vertical-align: middle\" class=h1>$i_BookingStatus</td>
                  <td align=center width=74 style=\"background-image: url('$rb_image_path/timeslotstop1b.gif'); vertical-align: middle\">
                    <input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'slots[]'):setChecked(0,document.form1,'slots[]')>
                  </td>
                </tr>
                <tr valign=top>
                  <td align=center width=134>&nbsp;</td>
                  <td align=center width=200> </td>
                  <td align=center width=74>&nbsp;</td>
                </tr>";
              for ($i=0; $i<sizeof($slots); $i++)
              {
                   list ($seq, $title, $timerange,$owned) = $slots[$i];
                   $title = intranet_wordwrap ($title,12," ",1);
                   $timerange = intranet_wordwrap ($timerange,12," ",1);


                       $color_str = "#000099";
                       $checked = ($owned ==1? "CHECKED":"");
                       $status_str = ($owned==1? "<font color=#FF0000>".$i_BookingStatusArray[$status]."</font>":"");

                       $checkbox_str = "<input type=checkbox name=slots[] value=$seq $checked>";
                       if ($status == 0 || ($status==4 && $owned ==1))
                       {

                           $x .= "<tr valign=top>
                                   <td align=center width=134>$title<br>$timerange</td>
                                   <td align=center width=200>$status_str</td>
                                   <td align=center width=74>$checkbox_str</td>
                                 </tr>
                                 <tr valign=top>
                                   <td align=center width=134>&nbsp;</td>
                                   <td align=center width=200></td>
                                   <td align=center width=74>&nbsp;</td>
                                 </tr>";
                       }
              }
              $x .= "<tr valign=top>
                  <td align=center colspan=3><img src='$rb_image_path/timeslotsbottom2.gif'>
                  </td></tr></table>\n";
              return $x;

     }


}


}        // End of directive

?>