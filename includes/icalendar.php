<?php
//echo number_format((memory_get_usage())/1024)."KB A2 <br />";
# Editing by 

/********************** Change Log ***********************/
#
# Date  : 2020-06-04 (Tommy):
#               modified Print_Search_result(), Print_Agenda(), fix deleted user's record show 0 in created by row
#
# Date  : 2019-09-03 (Ray):
#				Added get_Event_detail SUMMARY(;.*), LOCATION(;.*)
#				Added Get_IMap_Calendar_Content
# Date	: 2019-05-29 (Carlos):
#				Added getEventDetail($CurrentUserID,$EventID,$CalType) to get event participants in batch.
#				Added convertToEventEndDatetime($startDatetime,$duration) to calculate event end datetime by start datetime and duration.
#				Added newEventUpdate(), editEventUpdate(), removeEventUpdate(), chooseUsers() for api to create/edit/remove event and choose users to event.
# Date	: 2019-05-09 (Carlos): 
#				Added class attribute $CurrentUserID and its getter/setter, constructor can assign the UserID to make all methods stick to the current user id.
#				Added function getEventDetail() for fetching one event detail for api.
# Date	: 2018-10-23 (Carlos)
#				modified Get_All_Related_Event(), sort events by IsAllDay first then event date.
#				added sortEventCompareMethod($e1, $e2) for usort(). 
#				modified Print_Agenda() to fix sorting issue when cross day events present.
#
# Date	: 2018-08-22 (Carlos)
#				modified Query_User_Related_Events(), removed the union query that search for personal notes.
#
# Date	: 2017-01-24 (Carlos)
#				added getEventTitleExtraInfo(), updateEventTitleWithExtraInfo(), sendPushMessageNotifyParticipants($eventId).
#				modified generateEventGuestDiv(), generateEventGroupGuestList(), Print_Agenda().
#
# Date	: 2015-07-03 (Carlos)
#				modified Print_Agenda(), Display_Cal_Event_New(), Print_Weekly_AllDay_Event(), Display_Weekly_Cal_Event(), Display_Daily_Cal_Event(), do not output hidden events in print view.
#
# Date	: 2015-06-24 (Carlos)
#				modified Print_Agenda(), Query_User_Related_Events() - added DateModified to be displayed.
#
# Date	: 2015-05-05 (Omas)
#				modifed getOwnCalendarSelection() to fix when substr Chinese will become garbage
#
# Date	: 2015-04-30 (Omas)
#				modified returnViewableCalendar(),insertMyCalendar() can pass userID into the function
#
# Date	: 2015-01-15 (Carlos)
#				modified icalEvent_to_db(), when sync new events set event owner id as calendar owner id, if calendar is shared calendar, event owner id is set to the creator user id.
#				update event do not modify event user id, left to be the creator id or calendar owner id. 
#
# Date	: 2014-08-01 (Carlos)
#				added getOwnCalendarSelection() for getting user own calendar html selection
#
# Date	: 2014-07-15 (Carlos)
#				fix getNthWeekdayOfMonth() if $nth is negative, should be decremented 
#
# Date	: 2014-07-07 [Carlos]
#				modified insertRepeatEventSeries(), fix repeat weekly couting problem
#
# Date	: 2013-10-23 [Carlos]
#				modified Display_Cal_Event_New() and Print_Weekly_AllDay_Event(), sort continue events by start time first, then end date
#
# Date	: 2013-06-28 [Carlos]
#				added getSkipDates($StartDate,$EndDate)
#
# Date	: 2013-03-14 [Carlos]
#				add optional parameter groupPath to insertCalenderViewer()
#				modified addCalendarViewerToGroup() to insert viewer with groupPath
#
# Date	: 2012-12-05 [Carlos]
#				fix generateMonthlyCalendar() monthly view new event date format to Y-m-d
#
# Date	: 2012-11-28 [Carlos]
#				modified Query_User_Related_Events() tuned search query 
#
# Date	: 2012-10-25 [Carlos]
#				fix displayed end day of agenda type in get_navigation()
#
# Date	: 2012-06-07 [Carlos]
#				added global function sort_event_cmp_func() for sorting event start datetime used at extract_icalEvent()
#
# Date	: 2011-10-17 [Carlos] 
#				modified Print_Agenda() display weekday string
#		
# Date	: 2011-10-04 [Carlos]
#				added addCalendarViewerToGroup() and removeCalendarViewerFromGroup()
#
# Date	: 2011-09-26 [Carlos]
#				1. added getNthWeekdayOfMonth() for easier finding the Nth weekday in a month
#				2. fixed getRepeatedEventEndDate() and get_all_repeated_day() for weekly and monthly repeat event
# Date	: 2011-07-29 [Carlos]
#				1. added getRepeatedEventEndDate() for determining repeat event end date
#				2. added logic to handle COUNT=N in RRULE of .ics file
#				3. modified get_icalendar_rule(),icalEvent_to_db(),insert_repeated_event(),get_all_repeated_day() for 1. and 2.
#
# Date	: 2011-07-26 [Carlos]
#				fix icalEvent_to_db() - lossing events problem
#				fix get_all_repeated_day() - convert startUTC, endUTC to standard datetime format with $this->fromUTC()
#
# Date	: 2011-06-01 [Carlos]
#				fix insertRepeatEventSeries() - when insert repeat event missing first day
#
#
# Date	: 2011-03-09 [Yuen]
#				1. fixed the problem that cannot show events which across days; Print_Agenda() is modified.
#
# Date	: 2010-12-24 [Yuen]
#				1. fixed the problem that cannot show layer when click on an event in weekly view
#				2. fixed the problem that some event owner names cannot be diplayed (if the event overlap in time)
# Date	: 2010-11-04 [Yuen]
#				1. show full title in weekly view and also searched result in monthly view
#				2. show event owner/creator name right after title (enabled by new setting - $plugin["iCalendarShowOwnerInTitle"] = true;)
#				3. improved to search particular user's created events
#
/******************* End Of Change Log *******************/

// $PATH_REL_ROOT="../";
// include_once($PATH_REL_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_REL_ROOT."lang/ical_lang.$intranet_session_language.php");
$IncludesFolder = $intranet_root."/";
if (!isset($intranet_root) || trim($intranet_root)=="")
{
	# detect relative path
	$RootRelativePath = "";
	for ($iii=0; $iii<10; $iii++)
	{
		if (file_exists($RootRelativePath."includes/icalendar_api.php"))
		{
			$IncludesFolder =  $RootRelativePath;
			break;
		} else
		{
			$RootRelativePath .= "../";
		}
	}
}

if (!isset($plugin["iCalendarEditByOwner"])){
	$plugin["iCalendarEditByOwner"]  = true;
}

include_once($IncludesFolder."/includes/ESF_ui.php");
include_once($IncludesFolder."/includes/icalendar_api.php");

define("SECONDINADAY", 86400);
define("PERSONAL_CALENDAR", 0);
define("SCHOOL_CALENDAR", 1);
// define("FOUNDATION_CALENDAR", 2);
// define("ENROL_CPD_CALENDAR", 3);
// define("ENROL_CAS_CALENDAR", 4);

include_once($IncludesFolder."/includes/libdb.php");
include_once($IncludesFolder."/includes/lib.php");
// include_once("icalendar_api.php");
/* Testing Tools */
/* function debug_r($arr) {
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
} */

// function hidden_div($str){
	// return '<div style="display:none">'.$str.'<br><br></div>';
// }

// function StartTimer() {
	// global $timestart;
	// $mtime = microtime();
	// $mtime = explode(" ",$mtime);
	// $mtime = $mtime[1] + $mtime[0];
	// $timestart = $mtime;
	// return true;
// }

// function StopTimer($precision=5) {
	// global $timestart;
	// $mtime = microtime();
	// $mtime = explode(" ",$mtime);
	// $mtime = $mtime[1] + $mtime[0];
	// $timeend = $mtime;
	// $timetotal = $timeend-$timestart;
	// $scripttime = number_format($timetotal,$precision);
	// return $scripttime;
// }
/* End of Testing Tools */

// for sorting import events 
function sort_event_cmp_func($event1,$event2)
{
	if($event1['start'] == $event2['start']) return 0;
	return ($event1['start'] < $event2['start'])? -1 : 1;
}

// $UserID = $_SESSION['UserID'];
$schoolNameAbbrev = $_SESSION['SchoolCode']; //school Name abbreviation

// if (!defined("LIBICALENDAR_DEFINED"))                     // Preprocessor directive
// {
// $UserID = $_SESSION['UserID'];
$lui = new ESF_ui();
class icalendar extends libdb {
	var $workingHourStart, $workingHourEnd;
	var $reminderPeriod, $reminderLimit;
	var $systemSettings;
	var $offsetTimeStamp;
	var $limitEventDisplay;
	
	# for Involve Event Settings use only 		added on 5 May 08
	var $involveSettings, $involveCalValid, $involveCalColor;
	var $involveCalColorID, $involveCalFontColor;
	
	
	var $otherSettings, $otherCalValid, $otherCalColor;
	var $otherCalColorID, $otherCalFontColor;
	
	
	# for other CalType Event use only			added on 3 Mar 09
	var $otherCalTypeMapping;
	
	
	var $is_magic_quotes_active = false;
	
	var $CurrentUserID;
	
	function icalendar($parUserID=null) {
		$this->libdb();
		
		$uid = $parUserID? $parUserID : $_SESSION["UserID"];
		$this->setCurrentUserID($uid);
		
		$this->is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
		
		$this->systemSettings = $this->getSystemSetting();
		$this->systemSettings["WorkingHoursStart"] = 0;
		$this->systemSettings["WorkingHoursEnd"] = 23;
		// in term of minutes, from 5 min to 7 days
		$this->reminderPeriod = array(5,10,15,20,25,30,45,60,120,180,1440,2880,10080);
		// setup working hours
		if ($this->systemSettings["WorkingHoursStart"])
			$this->workingHourStart = $this->systemSettings["WorkingHoursStart"] + 0;
		else
			$this->workingHourStart = 0;
		if ($this->systemSettings["WorkingHoursEnd"])
			$this->workingHourEnd = $this->systemSettings["WorkingHoursEnd"] + 1;
		else
			$this->workingHourEnd = 24;
		// maximum number of reminder in a single event
		$this->reminderLimit = 5;
		// if no settings were found in DB, apply default settings
		if (!($this->systemSettings["PreferredView"]))
			$this->systemSettings["PreferredView"] = "agenda";
		//if (!($this->systemSettings["TimeFormat"]))
			$this->systemSettings["TimeFormat"] = "24";
		if (!($this->systemSettings["DisableRepeat"]))
			$this->systemSettings["DisableRepeat"] = "no";
		if (!($this->systemSettings["DisableGuest"]))
			$this->systemSettings["DisableGuest"] = "no";
			
		$this->offsetTimeStamp = 0;
		$this->limitEventDisplay = 5;
		
		# Involve Calendar Settings
		$this->involveSettings = $this->returnInvolveCalendarSettings();
		$this->involveCalValid = $this->involveSettings['valid'];
		$this->involveCalColor = $this->involveSettings['color'];
		$this->involveCalColorID = $this->involveSettings['colorID'];
		$this->involveCalFontColor = $this->involveSettings['fontcolor'];
		
		#other invitaion calendar
		// $this->otherCalValid = $this->involveSettings['valid'];
		// $this->otherCalColor = $this->involveSettings['color'];
		// $this->otherCalColorID = $this->involveSettings['colorID'];
		// $this->otherCalFontColor = $this->involveSettings['fontcolor'];
		
		// $this->otherCalTypeMapping = array(ENROL_CPD_CALENDAR=>'enrolcpd', MY_CPD_CALENDAR=>'mycpd');
		// debug_r($this->systemSettings); 
	}
	
	function setCurrentUserID($parUserID)
	{
		$this->CurrentUserID = $parUserID;
	}
	
	function getCurrentUserID()
	{
		return $this->CurrentUserID? $this->CurrentUserID : $_SESSION['UserID'];
	}
	
	function getSystemSetting() {
		$sql = "SELECT * FROM CALENDAR_CONFIG";
		$result = $this->returnArray($sql);
		foreach ($result as $value) {
			$settings[$value["Setting"]] = $value["Value"];
		}
		return $settings;
	}
	
	function sqlDatetimeToTimestamp($d) {
		if ($d=="" || $d == -1 || $d == 0)
			return 0;
		return strtotime($d);
	}
	
	function TimestampTosqlDatetime($ts) {
		if ( $ts==0 || $ts=="")
			return 0;
		$d = date("Y-m-d H:i:s", $ts);
		return $d;
	}
	
	function returnViewableCalendar($own=false,$other=false, $write=false, $showOwner=true, $ParUserID='') {
		global $UserID,$plugin,$intranet_session_language;
		$SchoolYear=Get_Current_Academic_Year_ID();
		$targetUserId = $ParUserID!=''?$ParUserID : $this->getCurrentUserID();
		
		$sql = "select CalID from INTRANET_GROUP where (AcademicYearID is null or AcademicYearID = '$SchoolYear') and CalID is not null and CalID <> ''";
		
		$GroupCal = $this->returnVector($sql);// echo $sql;
		
		$sql  = "SELECT DISTINCT a.CalID, a.Owner, a.SyncURL, ";
		if ($showOwner)
		{
			$sql .= "CASE WHEN a.Name = 'My Calendar' and U.UserID is not null THEN concat(a.Name,' (',".getNameFieldByLang2('U.').",')') ";
			$sql .= "ELSE a.Name ";
			$sql .= "END as Name, ";
		}
		else
		{
			$sql .= "a.Name, ";
		} 
		$sql .= "a.DefaultEventAccess, a.CalType, b.GroupID, b.GroupType, b.Access, b.Color, b.Visible ";
		$sql .= "FROM CALENDAR_CALENDAR AS a ";
		$sql .= "INNER JOIN CALENDAR_CALENDAR_VIEWER AS b ON a.CalID = b.CalID ";
		$sql .= "Left JOIN INTRANET_USER as U on a.Owner = U.UserID ";
		$sql .= "WHERE b.UserID = '$targetUserId' ";
		$sql .= "AND (b.GroupType in ('E','T') and a.CalID in ('".implode("','",$GroupCal)."') or b.GroupType not in ('E','T') or b.GroupType is null) ";
		$sql .= "AND (b.Access IS NOT NULL AND b.Access != '' ";
		$sql .= (!($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF))? " AND (a.CalType = 0 || a.CalType = 2 || a.CalType = 3)":"";
		$sql .= ($write == true) ? "AND (b.Access = 'A' OR b.Access = 'W') " : "";
		$sql .= ") ";
		//$sql .= "AND (b.CalType <> 2 OR b.CalType IS NULL) ";	# added on 2009-3-18 by Jason for not displaying Foundation Calendar in Optional Calendar
		if($write == false){
			if ($own){ $sql .= "AND a.Owner = '$targetUserId' "; }
			if ($other){ $sql .= "AND a.Owner <> '$targetUserId' ";}
		}
		$sql .= "ORDER BY a.CalID, b.Access"; //echo $sql;
		//$sql .= "ORDER BY b.GroupType";
		$result = $this->returnArray($sql,10);
		
		
		// echo $sql;
		// debug_r($result);
		# work around approach for handling duplicated user records but with different permission
		//if($own == false){
			if(count($result) > 0){
				$tmpInfo = array();
				$cnt = 0;
				for($i=0 ; $i<count($result) ; $i++){
					$tmpInfo[$cnt] = $result[$i];
					for($j=$i+1 ; $j<count($result) ; $j++){
						if($result[$i]['CalID'] != $result[$j]['CalID']){
							break;
						}
						if(trim($result[$j]['Access']) == 'W'){
                                                        $tmpInfo[$cnt] = $result[$j];
                                                }
					}
					$cnt++;
					$i = $j - 1;
				}
				$result = $tmpInfo;
			}
		//}
		
		return $result;
	}
	/*
	function returnFoundationCalendar(){
		global $UserID;
		$isESF = true;
		
		if($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV'){
			$db = new database(false, false, true);
			$isESF = false;
		}
		$sql  = "SELECT 
					a.CalID, a.Owner, a.Name, a.DefaultEventAccess, a.CalType, 
					a.CalSharedType, b.Color 
				FROM 
					CALENDAR_CALENDAR AS a
				INNER JOIN CALENDAR_CALENDAR_VIEWER AS b ON 
					b.UserID = a.Owner AND 
					b.CalID = a.CalID 
				WHERE 
					a.CalType = 2 ";
		$sql .= ($_SESSION['SSV_USER_TYPE'] == 'T') ? "AND a.CalSharedType IN ('T', 'A') " : "";
		$sql .= ($_SESSION['SSV_USER_TYPE'] == 'S') ? "AND a.CalSharedType IN ('S', 'A') " : "";
		$sql .= ($isESF == true) ? "AND a.Owner != '$UserID' "  : "";
		$sql .= "ORDER BY a.CalID";
		//echo $sql;
		$result = ($isESF == true) ? $this->returnArray($sql) : $db->returnArray($sql);
		return $result;
	}
	*/
	function returnFoundationCalendar(){
		global $UserID;
		$isESF = true;
		
		$uid = $this->getCurrentUserID();
		
		$current_dbname = $this->db;
		if($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV'){
			$db = new database(false, false, true);
			$central_dbname = $db->db;
			$isESF = false;
		} else {
			$central_dbname = $this->db;
		}
		$sql  = "SELECT 
					a.CalID, a.Owner, a.Name, a.DefaultEventAccess, a.SyncURL, b.Access, a.CalType, 
					a.CalSharedType, CASE WHEN c.Color IS NULL THEN b.Color collate Latin1_General_CI_AS ELSE c.Color END as Color, 
					CASE WHEN c.Visible IS NULL THEN 1 ELSE c.Visible END as Visible 
				FROM 
					".$central_dbname.".dbo.CALENDAR_CALENDAR AS a 
				INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR_VIEWER AS b ON 
					b.UserID = a.Owner AND 
					b.CalID = a.CalID
				LEFT JOIN ".$current_dbname.".dbo.CALENDAR_CALENDAR_VIEWER AS c ON 
					c.CalID = a.CalID AND 
					c.UserID = '".$uid."' AND 
					c.CalType = 2
				WHERE 
					a.CalType = 2 ";
		$sql .= ($_SESSION['SSV_USER_TYPE'] == 'T') ? "AND a.CalSharedType IN ('T', 'A') " : "";
		$sql .= ($_SESSION['SSV_USER_TYPE'] == 'S') ? "AND a.CalSharedType IN ('S', 'A') " : "";
		$sql .= ($isESF == true) ? "AND a.Owner != '$uid' "  : "";
		$sql .= "ORDER BY a.CalID";
		//
		// echo $sql;
		// exit;
		$result = ($isESF == true) ? $this->returnArray($sql) : $db->returnArray($sql);
		return $result;
	}
	
	# Insert a default calendar for user if he have no calendar created before
	function insertMyCalendar($ParUserID='') {
		//global $UserID;
		if($ParUserID == ''){
			$user_id_par = $this->getCurrentUserID();
		}
		else{
			$user_id_par = $ParUserID;
		}
		
		$calendars = $this->returnViewableCalendar(true,false, false, true, $user_id_par);
		
		//$calDefaultName = $this->returnUserFullName($UserID);
		$calDefaultName = $this->returnUserFullName($user_id_par);
		
		if (sizeof($calendars) == 0) {
			$sql = "INSERT INTO CALENDAR_CALENDAR (Owner, Name, DefaultEventAccess) VALUES ('$user_id_par', '".(($calDefaultName != 0) ? $calDefaultName : "My Calendar")."', 'R')";
			
			$this->db_db_query($sql);
			$calID = $this->db_insert_id();
			
			$sql  = "INSERT INTO CALENDAR_CALENDAR_VIEWER ";
			$sql .= "(CalID, UserID, Access, Color) VALUES ('$calID', '$user_id_par', 'A', '2f75e9')";
			
			$this->db_db_query($sql);
			
			$sql  = "INSERT INTO CALENDAR_USER_PREF ";
			$sql .= "(UserID, Setting, Value) VALUES ";
			$sql .= "('$user_id_par', 'schoolCalVisible', '1'), ";
			$sql .= "('$user_id_par', 'involveCalVisible', '1') ";
			
			$this->db_db_query($sql);
			$_SESSION["iCalHaveCal"] = 1;
		}
	}
	
	function returnEventEntry($calID, $lower_bound="", $upper_bound="") {
		global $UserID;
		$uid = $this->getCurrentUserID();
		$conds = "";
		if ($lower_bound != "" && $upper_bound != "") {
			$conds = " AND UNIX_TIMESTAMP(EventDate) BETWEEN '$lower_bound' AND '$upper_bound'";
		}
		
		$sql  = "SELECT EventID, UNIX_TIMESTAMP(EventDate), Duration, Title FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE CalID = '$calID' AND (UserID  = '$uid' OR Access = \"D\" OR Access = \"P\") $conds";
		$result = $this->returnArray($sql);
		
		return $result;
	}

	function array_union($a, $b) {
		$union = array_merge($a, $b); // duplicates may still exist
		$union = array_unique($union);
		return $union;
	}

	# Convert the Array from events list into a format with Event Data as a key (Associated Array) for More function use 
	function Return_Events_List_in_Associated_Array($result){
		$returnArr = array();
		# Put it in an associated array with EventDate as a Key
		if(count($result) > 0){	
			$tmp_info = array();
			for($i=0 ; $i<count($result) ; $i++){
				$evt_timeslot = $result[$i]['EventDate'];
				$tmp_info[$evt_timeslot][] = $result[$i];
			}
			$returnArr = $tmp_info;
		}
		return $returnArr;
	}

	# Convert the Array from query events into a format with Event Data as a key (Associated Array)
	function Return_Query_Events_in_Associated_Array($result){
		$returnArr = array();
		# Put it in an associated array with EventDate as a Key
		if(count($result) > 0){	
			$tmp_info = array();
			for($i=0 ; $i<count($result) ; $i++){
				$evt_date = date("Y-m-d", strtotime($result[$i]['EventDate']));
								
				$tmp_info[$evt_date][] = $result[$i];
				for($j=$i+1 ; $j<count($result) ; $j++){
					$next_evt_dtime = strtotime($result[$j]['EventDate']);
					$next_evt_date = date("Y-m-d", $next_evt_dtime);
					if($evt_date == $next_evt_date){
						$tmp_info[$evt_date][] = $result[$j];
					} else {
						break;
					}
				}
				$i = $j - 1;
			}
			$returnArr = $tmp_info;
		}
		//echo "count: ".$countDate."<br>";
		return $returnArr;
	}

	# new approach of getting users related events
	# Query user related events either involved or non-involved events
	function Query_User_Related_Events($lower_bound="", $upper_bound="", $getInvolvedEvent=false, $idOnly=false, $associArrOnly=false, $getFoundationEvent=false, $getMultipleDayEvent = false,$searchCase='',$useTempTable=false){
		global $UserID, $sys_custom;
		
		$uid = $this->getCurrentUserID();
		
		$userEvent = array();		
		# Select event within a period
		$conds = "";
		//$upper_bound_YMD = date("Y-m-d H:i:s",$upper_bound);
		//$lower_bound_YMD = date("Y-m-d H:i:s",$lower_bound);
		//echo $upper_bound_YMD." ".$lower_bound_YMD."<br>";
		
		if ($lower_bound != "" && $upper_bound != "" && $getMultipleDayEvent){
			//$lowerBoundstr = date("Y-m-d H:i:s.000", $lower_bound);
			//$upperBoundstr = date("Y-m-d H:i:s.000", $upper_bound);
			$conds= " AND (UNIX_TIMESTAMP(a.EventDate) <  '$lower_bound' AND UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,if(a.IsAllDay=1,a.Duration-1440,a.Duration),a.EventDate)) > '$upper_bound' OR UNIX_TIMESTAMP(a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' OR UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,if(a.IsAllDay=1,a.Duration-1440,a.Duration),a.EventDate)) BETWEEN '$lower_bound' AND '$upper_bound') ";
			//$conds = " AND (DATEDIFF(s, a.EventDate, '$lowerBoundstr') >= 0 AND DATEDIFF(s, '$lowerBoundstr', DATEADD(minute, a.Duration, a.EventDate)) > 0 OR DATEDIFF(s, '19700101', a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' )";
		}
		elseif ($lower_bound != "" && $upper_bound != "") { 
			$conds = " AND UNIX_TIMESTAMP(a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' ";	
			//$conds = " AND DATEDIFF(s, '19700101', a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' ";
			//$conds = " AND a.EventDate BETWEEN '$lower_bound_YMD' AND '$upper_bound_YMD' ";
		}

		# Join table with calendar viewer setting
		$sub_sql = "";
		if ($associArrOnly){
			$sub_sql = " LEFT OUTER JOIN CALENDAR_CALENDAR_VIEWER AS c ON 
							c.CalID = a.CalID AND c.UserID = '$uid' ";
		}

		$eventSql = '';
		$isGo = true;
		if($getInvolvedEvent)
		{
			# Get all user involved event id
			$sql  = "SELECT EventID FROM CALENDAR_EVENT_USER WHERE UserID = '$uid' AND ";
			$sql .= "((InviteStatus = 0 AND Status IN ('A','M')) OR (InviteStatus = 1 AND Status IN ('A','M')) )";// AND FromSchoolCode is null AND ToSchoolCode is null";
			$eventSql = " a.EventID in (".$sql.") ";	
			//$involveEvent = $this->returnArray($sql);
			//$userEvent = $involveEvent;
		} 
		else 
		{
			# Get all own related event excluding involved event id
			# Find out user's viewable calendar
			$sql = "SELECT DISTINCT CalID FROM CALENDAR_CALENDAR_VIEWER WHERE UserID = '$uid' ";	 
			$viewableCalendar = $this->returnArray($sql);
			if (sizeof($viewableCalendar) > 0) {
				for ($i=0; $i<sizeof($viewableCalendar); $i++)
					$viewableCalendarSql[$i] = $viewableCalendar[$i][0];
				$viewableCalendarSql = implode(",", $viewableCalendarSql);
				$sql  = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE CalID IN ($viewableCalendarSql)";
				$eventSql = " a.CalID in ($viewableCalendarSql) ";
				//$ownEvent = $this->returnArray($sql);
			} else {
				//$ownEvent = array();
				$isGo = false;	
			}
			//$userEvent = $ownEvent;
		}

		# combine own event and particpated event
		/*$allEventSql = array();
		$allEvent = $userEvent;
		for ($i=0; $i<sizeof($allEvent); $i++){ $allEventSql[$i] = $allEvent[$i][0]; }
		$allEventSql = array_unique($allEventSql);
		sort($allEventSql);
		$allEventSql = implode(",", $allEventSql);*/
		
		if ($idOnly && !$associArrOnly) {
			$fieldname  = "a.EventID";
		} else {
			$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, a.EventDate, a.Title, ";
			$fieldname .= "a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url, ";
			$fieldname .= "pn.PersonalNote, ";
			$fieldname .= "b.Status, b.Access AS Permission ";
			$fieldname .= ($associArrOnly) ? ", c.Color, c.Visible, c.Access AS CalAccess, a.ModifiedDate as DateModified " : "";
		}
		$eclassConstraint ="AND (pn.CalType <> 1 AND pn.CalType <= 3 OR pn.CalType is null)";
		
		$colArray = Array('a.Description','a.Location','a.Title','pn.PersonalNote','a.Url','a.EventDate');
		$iCal_api = new icalendar_api($uid);
		$keySearchSql = $iCal_api->SearchSqlString($searchCase,$colArray);
		
		# involve searching of owner
		if (is_array($searchCase) && sizeof($searchCase)>0)
		{
			$kkkk = 0;
			$sql_user = "SELECT DISTINCT UserID FROM INTRANET_USER WHERE ";
			for ($jjj=0; $jjj<sizeof($searchCase); $jjj++)
			{
				if (trim($searchCase[$jjj][0][0])<>"")
				{
					$sql_user .= (($kkkk==0)? "" : " OR " ) . " ChineseName like '%".$searchCase[$jjj][0][0]."%' OR EnglishName like '%".$searchCase[$jjj][0][0]."%'";
					$kkkk ++;
				}
			}
			if ($kkkk>0)
			{
				$user_result = $this->returnVector($sql_user);
				$user_searched_ids = implode(",", $user_result);
				if ($user_searched_ids<>"")
				{
					$keySearchSql = "And ( ( a.UserID IN ({$user_searched_ids}) OR " . substr($keySearchSql, 9);
				}
			}
		}

	///	if (sizeof($allEvent) > 0) {
		if ($isGo) {
			/*	
			$sql = "SELECT $fieldname 
					FROM CALENDAR_EVENT_ENTRY AS a 
					".(($getInvolvedEvent) ? "INNER JOIN " : "LEFT JOIN ")." 
					CALENDAR_EVENT_USER AS b ON 
						a.EventID=b.EventID AND 
						b.UserID = '$UserID' 
						".(($getInvolvedEvent) ? "AND b.Access = 'R' " : "AND b.Access <> 'R' ")."
						AND b.Status <> 'R' 				
					$sub_sql 
					LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON 
						a.EventID = pn.EventID AND 
						pn.UserID = '$UserID' 
					WHERE 
						$eventSql
						$eclassConstraint
						$conds
						$keySearchSql
					ORDER BY a.EventDate, a.EventID ";
			*/
			$colArray = Array('a.Description','a.Location','a.Title','a.Url','a.EventDate');
			
			$keySearchEventSql = $iCal_api->SearchSqlString($searchCase,$colArray);
			//$colArray = Array('pn.PersonalNote');
			//$keySearchNoteSql = $iCal_api->SearchSqlString($searchCase,$colArray);
			
			$sql = "SELECT $fieldname 
					FROM CALENDAR_EVENT_ENTRY AS a 
					".(($getInvolvedEvent) ? "INNER JOIN " : "LEFT JOIN ")." 
					CALENDAR_EVENT_USER AS b ON 
						a.EventID=b.EventID AND 
						b.UserID = '$uid' 
						".(($getInvolvedEvent) ? "AND b.Access = 'R' " : "AND b.Access <> 'R' ")."
						AND b.Status <> 'R' 				
					$sub_sql 
					LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON 
						a.EventID = pn.EventID AND 
						pn.UserID = '$uid' 
					WHERE 
						$eventSql
						$eclassConstraint
						$conds
						$keySearchEventSql ";
			/*			
			$sql.= "UNION (
					SELECT $fieldname 
					FROM CALENDAR_EVENT_ENTRY AS a 
					".(($getInvolvedEvent) ? "INNER JOIN " : "LEFT JOIN ")." 
					CALENDAR_EVENT_USER AS b ON 
						a.EventID=b.EventID AND 
						b.UserID = '$UserID' 
						".(($getInvolvedEvent) ? "AND b.Access = 'R' " : "AND b.Access <> 'R' ")."
						AND b.Status <> 'R' 				
					$sub_sql 
					LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON 
						a.EventID = pn.EventID AND 
						pn.UserID = '$UserID' 
					WHERE 
						$eventSql
						$eclassConstraint
						$conds
						$keySearchNoteSql 
					)";
			*/
			$sql.= "ORDER BY EventDate,EventID ";
			//echo ($getInvolvedEvent) ? 'Involve<br>'.$sql : 'Non-involve<br>'.$sql;
			if ($useTempTable){
				$insertSql = " insert into TempEvent (EventID, UserID, CalID, RepeatID, EventDate, Title, Description, Duration, IsImportant, IsAllDay, Access, Location, Url, PersonalNote, Status, Permission ".(($associArrOnly) ? ", Color, Visible, CalAccess" : "").", DateModified) ".$sql;
				$r = $this->db_db_query($insertSql);
				return;
			}
			else{
				if ($idOnly && !$associArrOnly) {
					$result = $this->returnVector($sql);
				} else {
					$result = $this->returnArray($sql);
				}
			}
			# Remove the duplicated records
			if(count($result) > 0){
				$tmpInfo = array();
				$cnt = 0;
				for($i=0 ; $i<count($result) ; $i++){
					$tmpInfo[$cnt] = $result[$i];
					for($j=$i+1 ; $j<count($result) ; $j++){
						if($result[$i]['EventID'] != $result[$j]['EventID']){ break; }
						if(trim($result[$j]['Permission']) == 'W'){ $tmpInfo[$cnt] = $result[$j]; }
					}
					$cnt++;
					$i = $j - 1;
				}
				$result = $tmpInfo;
			}
		} else {
			$result = array();
		}
		// if ($getInvolvedEvent){
		// debug_r($result);
		// exit;
		// }
		
		return $result;
	}

	# new approach of getting users related events
	# Query user related events either involved or non-involved events
	function Query_User_Foundation_Events($lower_bound="", $upper_bound="", $idOnly=false, $associArrOnly=false, $getMultipleDayEvent=false){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$userEvent = array();
		$isESF = true;
		$result = array();
		
		# Checking for the current school is ESFC or not
		$current_dbname = $this->db;
		if($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV'){
			$db = new database(false, false, true);
			$isESF = false;
			$central_dbname = $db->db;
		}
		
		# Select event within a period
		$conds = "";
		if ($lower_bound != "" && $upper_bound != "" && $getMultipleDayEvent){
			$lowerBoundstr = date("Y-m-d H:i:s.000", $lower_bound);
			$upperBoundstr = date("Y-m-d H:i:s.000", $upper_bound);
			$conds = "  DATEDIFF(s, a.EventDate, '$lowerBoundstr') >= 0 AND DATEDIFF(s, '$lowerBoundstr', DATEADD(minute, a.Duration, a.EventDate)) > 0 OR DATEDIFF(s, '19700101', a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' ";
		}
		elseif ($lower_bound != "" && $upper_bound != "") {
			$conds = " DATEDIFF(s, '19700101', a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' ";
		}
		
		# Get Foundation Event ID
		/*
		$sql  = "SELECT a.EventID FROM CALENDAR_EVENT_USER AS a 
				 INNER JOIN CALENDAR_CALENDAR AS b ON 
					b.CalID = a.CalID AND b.CalType = 2 ";
		$sql .= ($_SESSION['SSV_USER_TYPE'] == 'T') ? "AND b.CalSharedType IN ('T', 'A') " : (($_SESSION['SSV_USER_TYPE'] == 'S') ? "AND b.CalSharedType IN ('S', 'A') " : "");
		$sql .= ($isESF == true) ? "AND b.Owner != '$UserID' "  : "";
		$foundationEvent = ($isESF == true) ? $this->returnArray($sql) : $db->returnArray($sql);
		echo $sql;
		*/
		
		if ($idOnly && !$associArrOnly) {
			$fieldname  = "a.EventID";
		} else {
			$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, CONVERT(VARCHAR(20), a.EventDate, 120) AS EventDate, a.Title, ";
			$fieldname .= "a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url, ";
			$fieldname .= "cast(cast(pn.PersonalNote as varbinary(max)) as image) as PersonalNote, ";
			$fieldname .= "b.CalType, b.Owner ";
			//$fieldname .= "b.Status, b.Access AS Permission ";
			if($associArrOnly){
				$fieldname .= ", CASE WHEN d.Color IS NULL THEN c.Color ELSE d.Color END AS Color, 
								CASE WHEN d.Visible IS NULL THEN c.Visible ELSE d.Visible END AS Visible, 
								c.Access AS CalAccess";
			}
		}
		
		# Get Details of Foundation EventID
		//if (sizeof($foundationEvent) > 0) {
			$sql = "SELECT $fieldname 
					FROM ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY AS a 
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR AS b ON 
						b.CalID = a.CalID AND 
						b.CalType = 2 
						".(($_SESSION['SSV_USER_TYPE'] == 'T') ? "AND b.CalSharedType IN ('T', 'A') " : (($_SESSION['SSV_USER_TYPE'] == 'S') ? "AND b.CalSharedType IN ('S', 'A') " : ""))." 
						".(($isESF == true) ? "AND b.Owner != '$uid' "  : "")." 
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR_VIEWER AS c ON 
						c.CalID = a.CalID AND 
						c.UserID = b.Owner 
					LEFT JOIN ".$current_dbname.".dbo.CALENDAR_CALENDAR_VIEWER AS d ON 
						d.CalID = a.CalID AND 
						d.UserID = '".$uid."' AND 
						d.CalType = 2 
					LEFT JOIN ".$current_dbname.".dbo.CALENDAR_EVENT_PERSONAL_NOTE AS pn with (nolock) ON 
						a.EventID = pn.EventID AND 
						pn.UserID = '$uid' AND 
						pn.CalType = 2 
					WHERE 
						$conds 
					ORDER BY a.EventDate, a.EventID";
			if ($idOnly && !$associArrOnly) {
				$result = ($isESF == true) ? $this->returnVector($sql) : $db->returnVector($sql);
			} else {
				$result = ($isESF == true) ? $this->returnArray($sql) : $db->returnArray($sql);
			}
// echo $sql;
// exit;
			# Remove the duplicated records
			if(count($result) > 0){
				$tmpInfo = array();
				$cnt = 0;
				for($i=0 ; $i<count($result) ; $i++){
					$tmpInfo[$cnt] = $result[$i];
					for($j=$i+1 ; $j<count($result) ; $j++){
						if($result[$i]['EventID'] != $result[$j]['EventID']){ break; }
						if(trim($result[$j]['Permission']) == 'W'){ $tmpInfo[$cnt] = $result[$j]; }
					}
					$cnt++;
					$i = $j - 1;
				}
				$result = $tmpInfo;
			}
		//} 
		
		return $result;
	}
	
	# Query all events that the user can view, bound by the lower and upper time limit
	function query_events($lower_bound="", $upper_bound="", $idOnly=false) {
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		# Select event within a period
		$conds = "";
		if ($lower_bound != "" && $upper_bound != "") {;
			$conds = " AND UNIX_TIMESTAMP(a.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' ";
		}
		
		# Find out user's viewable calendar
		$sql = "SELECT CalID FROM CALENDAR_CALENDAR_VIEWER WHERE UserID = '$uid'";
		$viewableCalendar = $this->returnArray($sql);
		if (sizeof($viewableCalendar) > 0) {
			for ($i=0; $i<sizeof($viewableCalendar); $i++)
				$viewableCalendarSql[$i] = $viewableCalendar[$i][0];
			$viewableCalendarSql = implode(",", $viewableCalendarSql);
			$sql  = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE CalID IN ($viewableCalendarSql)";
			// Filter private event set by owner
			//$sql .= " AND ((UserID != $UserID AND Access != 'R') OR (UserID = $UserID))";
			$ownEvent = $this->returnArray($sql);
		} else {
			$ownEvent = array();
		}
		
		//$sql = "SELECT EventID FROM CALENDAR_EVENT_USER WHERE UserID = $UserID AND Status IN ('A','R','W','M')";
		$sql = "SELECT EventID FROM CALENDAR_EVENT_USER WHERE UserID = '$uid' AND ((InviteStatus = 0 AND Status IN ('A','M')) OR (InviteStatus = 1 AND Status IN ('A','M')) )";
		$involveEvent = $this->returnArray($sql);
		
		# combine own event and particpated event
		$allEventSql = array();
		$allEvent = array_merge($ownEvent, $involveEvent);
		for ($i=0; $i<sizeof($allEvent); $i++)
			$allEventSql[$i] = $allEvent[$i][0];
		$allEventSql = array_unique($allEventSql);
		sort($allEventSql);
		$allEventSql = implode(",", $allEventSql);
		
		if ($idOnly) {
			$fieldname  = "a.EventID";
		} else {
			$fieldname  = "a.EventID, a.UserID, a.CalID, a.RepeatID, a.EventDate, a.Title, ";
			$fieldname .= "a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url, ";
			$fieldname .= "pn.PersonalNote, ";
			$fieldname .= "b.Status, b.Access AS Permission ";
		}
		
		if (sizeof($allEvent) > 0) {
			$eclassConstraint =" AND (pn.CalType <> 1 AND pn.CalType <= 3 OR pn.CalType is null) ";
			$sql  = "SELECT $fieldname FROM CALENDAR_EVENT_ENTRY AS a LEFT JOIN CALENDAR_EVENT_USER AS b ";
			$sql .= "ON a.EventID=b.EventID AND b.UserID = '$uid' ";
			$sql .= "AND b.Status <> 'R' ";					# control to show the event which the user decided not to attend
			$sql .= "LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE as pn ON a.EventID=pn.EventID and pn.UserID='$uid' ";
			$sql .= "WHERE a.EventID IN ($allEventSql) ".$eclassConstraint;
			$sql .= $conds;
			# now order the results by EventDate and by EventID.
			$sql .= "ORDER BY a.EventDate, a.EventID";
			
			/*
			$sql  = "SELECT $fieldname FROM CALENDAR_EVENT_ENTRY AS a LEFT JOIN CALENDAR_EVENT_USER AS b ";
			$sql .= "ON a.EventID=b.EventID AND b.UserID = $UserID WHERE ";
			$sql .= "a.EventID IN ($allEventSql)";
			$sql .= $conds;
			$sql .= "GROUP BY a.EventID ORDER BY MIN(a.EventDate)";
			*/
			if ($idOnly) {
				$result = $this->returnVector($sql);
			} else {
				$result = $this->returnArray($sql);
			}
		} else {
			$result = array();
		}
		return $result;
	}
	
	// $eventDate in "YYYY-mm-dd H:i:s" format
	function returnEventConflictArr($eventID, $eventTs, $endTs) {
		$lowerBound = $eventTs - 24*60*60;
		$upperBound = $endTs; 
		
		// retrieve user-viewable event
		$allEvents = $this->query_events($lowerBound, $upperBound, true);
		
		if (!empty($eventID) && $eventID != "" && is_numeric($eventID)) {
			$tempArr = array($eventID);
			$allEvents = array_diff($allEvents, $tempArr);
		}
		 
		if (sizeof($allEvents) > 0) {
			$cond = "EventID IN (".implode(",", $allEvents).") AND ";
			
			$sql  = "SELECT EventID, Title, EventDate, Duration ";
			$sql .= "FROM CALENDAR_EVENT_ENTRY WHERE ";
			$sql .= "IsAllDay != '1' AND ";
			$sql .= $cond;
			$sql .= "((UNIX_TIMESTAMP(EventDate) >= $eventTs AND UNIX_TIMESTAMP(EventDate) < $endTs) OR";
			$sql .= "((UNIX_TIMESTAMP(EventDate)+Duration*60) >= $eventTs AND (UNIX_TIMESTAMP(EventDate)+Duration*60) <= $endTs)) ";
			$sql .= "ORDER BY UNIX_TIMESTAMP(EventDate)";
			// $sql .= "((DATEDIFF(s, '19700101', EventDate) >= ".$eventTs." AND DATEDIFF(s, '19700101', EventDate) < ".$endTs.") OR";
			// $sql .= "((DATEDIFF(s, '19700101', EventDate)+Duration*60) >= ".$eventTs." AND (DATEDIFF(s, '19700101', EventDate)+Duration*60) <= ".$endTs.")) ";
			// $sql .= "ORDER BY DATEDIFF(s, '19700101', EventDate)";
			$result = $this->returnArray($sql, 4);
			
		} else {
			$result = array();
		}
		return $result;
	}
	
	function resetTimestampDayStart($ts) {
		$year = date("Y", $ts);
		$month = date("m", $ts);
		$day = date("d", $ts);
		return mktime(0, 0, 0, $month, $day, $year);
	}
	
	function displayHoliday($ts, $year, $month) {
		if (date("w", $ts) == 0) # Sunday
			return "holidayNumber";
		if (date("Y-m-d", $ts) == date("Y-m-d", time()))
			return "todayNumber";
		/*
		$SchoolEventArray = $this->returnSchoolEventRecord(mktime(0,0,0, $month, 1, $year), mktime (0,0,0, $month+1, 1, $year));
		for($i=0; $i<sizeof($SchoolEventArray); $i++) {
			if($this->resetTimestampDayStart($SchoolEventArray[$i][1])==$ts && $SchoolEventArray[$i][2] == 2) {
				return "holidayNumber";
			}
		}
		*/
		return "normalDayNumber";
	}

	function getCalBasicDetail($calID){
                $result = array();
                if($calID != ""){
                        $sql = "SELECT * FROM CALENDAR_CALENDAR WHERE CalID = '$calID'";
                        $result = $this->returnArray($sql);
                }
                return $result;
        }
	
	# $isESF  used for Foundation Calendar 
	function getCalDetail($calID, $isESF=false) {
		global $UserID;
		$uid = $this->getCurrentUserID();
		$sql = "SELECT * FROM CALENDAR_CALENDAR_VIEWER WHERE UserID = '$uid' AND CalID = '$calID'";
		// if($isESF == true){
			// $db = new database(false, false, true);
			// $result = $db->returnArray($sql);
		// } else {
			$result = $this->returnArray($sql);
		//}
		return $result;
	}
	
	function addTimeToTimestamp($ts, $year, $month, $day, $hour=0, $minute=0, $second=0) {
		$year = (int)date("Y", $ts) + $year;
		$month = (int)date("n", $ts) + $month;
		$day = (int)date("j", $ts) + $day;
		$hour = (int)date("H", $ts) + $hour;
		$minute = (int)date("i", $ts) + $minute;
		$second = (int)date("s", $ts) + $second;
		return mktime($hour, $minute, $second, $month, $day, $year);
	}
	
	function changeTimestamp($ts, $year, $month, $day, $hour="", $minute="", $second="") {
		if ($year == "")
			$year=date("Y", $ts);
		if ($month == "")
			$month=date("n", $ts);
		if ($day == "")
			$day=date("j", $ts);
		if ($hour == "")
			$hour=date("H", $ts);
		if ($minute == "")
			$minute=date("i", $ts);
		if ($second == "")
			$second=date("s", $ts);
		return mktime($hour, $minute, $second, $month, $day, $year);
	}
	
	function returnNextNthDayOfMonth($ts) {
		if (date("L", $ts)) {
			$daysofmonth = array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		} else {
			$daysofmonth = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		}
		
		$year = (int)date("Y", $ts);
		$month = (int)date("n", $ts);
		$day = (int)date("j", $ts);
		$hour = (int)date("H", $ts);
		$minute = (int)date("i", $ts);
		$second = (int)date("s", $ts);
		
		$new_month = $month;
		$new_year = $year;
		
		if ($month == 2 && $day == 29 && date("L", $ts)) {
			if (($year+4)%400 == 0) {
				$new_year = $year+4;
			} else if (($year+4)%100 == 0) {
				$new_year = $year+8;
			} else if (($year+4)%4 == 0) {
				$new_year = $year+4;
			} else {
				$new_year = $year+8;
			}
		} else {
			for ($i=$month; $i<($month+12); $i++) {
				if ($i >= 12) {
					$i = $i-12;
					$new_year_flag = true;
				}
				if ($day > $daysofmonth[$i]) {
					continue;
				} else {
					$new_month = $i+1;
					break;
				}
			}
			$new_year = $new_year_flag?($new_year+1):$new_year;
		}
		return mktime($hour, $minute, $second, $new_month, $day, $new_year);
	}

	# New approach of printing monthly date  enhanced on 1 Sept
	# using function Display_Cal_Event() once only as the date of user related events & user involved events 
	# are manipulated at the same time
	function Print_Monthly_Date ($ts, &$conEvent, &$associateDay, $print=false, $calViewMode='simple'){
		global $num, $numVisible,$iCalendar_more;
		$str = '';
		$date = '';
		$date_div = '';
		$num = 0;
		$numVisible = 0;
		
		$limit = $this->limitEventDisplay;

		if($print){
			//$date = $this->Display_Cal_Event($ts, true);
			$date = $this->Display_Cal_Event_New($ts,  $conEvent, $associateDay, true);			
			$date_div = ($num > $limit) ? "<div>" : "<div style='height:90px;'>";
		} else {
			
			$date = $this->Display_Cal_Event_New($ts,  $conEvent, $associateDay, false, ($calViewMode=='simple' ? true : false), 9, $calViewMode);
			//$date = $this->Display_Cal_Event($ts);
// 			debug_pr($this->Display_Cal_Event_New($ts,  $conEvent, $associateDay, false, ($calViewMode=='simple' ? true : false), 9, $calViewMode));
			
			$date_div = "<div id=\"day-".date("j",$ts)."\"style='height:90px;'>";
		}

		$more_btn = ''; 
		$current_date = date("Y-m-d",$ts);
		
		$num += count($conEvent[$current_date]);
		
		if($num > $limit && !$print && $calViewMode == 'simple'){
			$evt_year = date("Y", $ts);
			$evt_month = date("n", $ts);
			$evt_date = date("j", $ts);
			$evtDate = date("Y-n-j", $ts);
			
			//echo "$current_date ".count($conEvent[$current_date]);
			
			$total = $numVisible;
			
			$display = ($total > 5) ? "block" : "none";
			// if ($calViewMode == 'full') {
				// $display = 'block';
			// }
			
			$more_btn .= "<div id=\"more_$evt_date\" name=\"more[]\" align=\"right\" style=\"display:$display\">";
			$more_btn .= "<a class=\"sub_layer_link\" href=\"javascript:jGet_Event_Entry_List('$evtDate');\">$iCalendar_more...</a>";
			$more_btn .= "</div>";
		}

		$str = $date_div.$date.'</div>'.$more_btn;
		return $str;
	}
	
	# Old approach of printing monthly date
	function printMonthlyDate($ts, $print=false) {
		global $num, $numVisible;
		$date = "";
		$viewableCal = $this->returnViewableCalendar();
		$limit = $this->limitEventDisplay;
		
		$num = 0;
		$numVisible = 0;
        for ($i=0; $i<sizeof($viewableCal); $i++) {
	        if ($print){
            	$calEventDiv = $this->displayCalEvent($viewableCal[$i], $ts, true);
        	} else {
            	$calEventDiv = $this->displayCalEvent($viewableCal[$i], $ts);
        	}
            if ($calEventDiv != "") {
            	$date .= $calEventDiv;
    		}
    	}
    	
    	if ($print){
    		$involveEventDiv = $this->displayInvolveEvent($ts, true);
		} else {
    		$involveEventDiv = $this->displayInvolveEvent($ts);
		}
    	if ($involveEventDiv != "") {
        	$date .= $involveEventDiv;
		}
		$date .= "</div>";
		
		$more_evt = '';
		if(strtotime('2008-05-29 00:00:00') == $ts){
			var_dump($num);var_dump($limit);var_dump($numVisible);
		}
		if($num > $limit && !$print){
			$evt_year = date("Y", $ts);
			$evt_month = date("n", $ts);
			$evt_date = date("j", $ts);
			$evtDate = date("Y-n-j", $ts);
			$display = ($numVisible >= 5) ? "block" : "none";
			
			$date .= "<div id=\"more_$evt_date\" name=\"more[]\" align=\"right\" style=\"display:$display\">";
			$date .= "<a class=\"sub_layer_link\" href=\"javascript:jGet_Event_Entry_List('$evtDate');\">more...</a>";
			$date .= "</div>";
			//$date .= "<div id=\"more_$evt_date\" name=\"more[]\" align=\"right\" style=\"display:$display\"><a class=\"sub_layer_link\" href=\"javascript:jShow_More_Event_List($evt_year, $evt_month, $evt_date);\">more...</a></div>";
			//$more_evt .= $this->displayMoreCalEvent($ts, $print, $evt_year.'-'.$evt_month.'-'.$evt_date);
		}
		
		# set the standard height of each Monthly Cell
		$data_div = '';
		if($print){
			$date_div .= ($num > $limit) ? "<div>" : "<div style='height:90px;'>";
		} else {
			$date_div .= "<div style='height:90px;overflow:hidden'>";
		}
		
		# return
		$date = $more_evt.$date_div.$date;
    	return $date;
	}
	
	function returnUserPreferredName($UserID){
		$result_1 = array();
		$result_2 = array();
		$returnStr = '';
		
		# Get UserType & SureName
		$sql = "SELECT UserType, SurName FROM INTRANET_USER WHERE UserID = '$UserID'";
		$result_1 = $this->returnArray($sql);
				
		if(count($result_1) > 0){
			switch ($result_1[0]['UserType']){
				case 'T': $table = "STAFF"; break;
				case 'S': $table = "STUDENT"; break;
				default : break;
			}
						
			# Get Preferred Name
			$sql = "SELECT Pref_Name FROM $table WHERE UserID = '$UserID'";
			$result_2 = $this->returnArray($sql);
						
			if(count($result_2) > 0){
				$returnStr = $result_2[0]['Pref_Name'].' '.$result_1[0]['SurName'];
			} else {
				$returnStr = $result_1[0]['SurName'];
			}
		}
		
		return $returnStr;
	}
	
	function returnUserFullName($User_ID, $CalType='') {
		//$user = empty($user)?$_SESSION['UserID']:$UserID;
		global $events_ownernames;
		
		if ($events_ownernames[$User_ID]<>"")
		{
			$result[0]["name"] = $events_ownernames[$User_ID];
		} else
		{
			$sql  = "SELECT ".getNameFieldByLang()." AS name FROM INTRANET_USER WHERE UserID = '".$User_ID."' ";
			$result = $this->returnArray($sql);
		}
		if (sizeof($result) > 0)
			return $result[0]["name"];
		else
			return 0;
	}
	
	function AppendOnwerName($user_id_given) {
		
		global $iCalendar_systemAdmin, $events_ownernames, $plugin;
		
		if (!$plugin["iCalendarShowOwnerInTitle"])
		{
			return "";
		}
		
		if ($user_id_given==0)
		{
			# admin
			$owner_name = $iCalendar_systemAdmin;
		} elseif ($user_id_given>0)
		{
			$owner_name = $events_ownernames[$user_id_given];
		}
		if (trim($owner_name)!="")
		{
			return " [".$owner_name."]";
		} else
		{
			return "";
		}
	}
	
	
	function returnUsersFullNames($User_ID_Arr) {
		if (is_array($User_ID_Arr) && sizeof($User_ID_Arr)>0)
		{
			$UserIDs = implode(",", IntegerSafe($User_ID_Arr));
			$sql  = "SELECT ".getNameFieldByLang()." AS name, UserID FROM INTRANET_USER WHERE UserID in (".$UserIDs.")";
			$result = $this->returnArray($sql);
		}
		if (is_array($result) && sizeof($result) > 0)
		{
			for ($i=0; $i<sizeof($result); $i++)
			{
				$resultAss[$result[$i][1]] = $result[$i][0];
			}
			return $resultAss;
		} else
		{
			return Array();
		}
	}
	
	function returnClassName($ClassID=-1, $GroupID=-1) {
		if ($ClassID > 0) {
			$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID = '$ClassID'";
		} else if ($GroupID > 0) {
			$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE GroupID = '$GroupID'";
		}
		$result = $this->returnArray($sql);
		if (sizeof($result) > 0)
			return $result[0]["ClassName"];
		else
			return 0;
	}
	
	function returnCourseName($CourseID) {
		$sql = "SELECT CourseName FROM CALENDAR_COURSE WHERE CourseID = '$CourseID'";
		$result = $this->returnArray($sql);
		if (sizeof($result) > 0)
			return $result[0]["CourseName"];
		else
			return 0;
	}
	
	function returnGroupName($GroupID) {
		$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '$GroupID'";
		$result = $this->returnArray($sql);
		if (sizeof($result) > 0)
			return $result[0]["Title"];
		else
			return 0;
	}
	
	function returnClassStudentList($GroupID) {
		if (is_numeric($GroupID)) {
			$ClassName = $this->returnClassName(-1, $GroupID);
			$sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$ClassName'";
			$result = $this->returnVector($sql);
			return $result;
		} else {
			return 0;
		}
	}
	
	function returnCourseStudentList($GroupID) {
		if (is_numeric($GroupID)) {
			$sql = "SELECT UserID FROM CALENDAR_USER_COURSE WHERE CourseID = '$GroupID'";
			$result = $this->returnVector($sql);
			return $result;
		} else {
			return 0;
		}
	}
	
	function returnGroupMemberList($GroupID) {
		if (is_numeric($GroupID)) {
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID'";
			$result = $this->returnVector($sql);
			return $result;
		} else {
			return 0;
		}
	}
	
	function returnStudentParent($studentID) {
		if (is_numeric($studentID)) {
			$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID = '$studentID' ORDER BY ParentID";
			$result = $this->returnVector($sql);
			return $result;
		} else {
			return 0;
		}
	}
	
	function returnRepeatEventSeries($repeatID) {
		$sql  = "SELECT EventID, UID, EventDate FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE RepeatID=".$repeatID;
		return $this->returnArray($sql);
	}  
	
	function returnRepeatID($eventID) {
		$sql  = "SELECT RepeatID FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE EventID='".$eventID."' ";
		$result = $this->returnArray($sql);
		$repeatID = $result[0]["RepeatID"];
		return $repeatID;
	}
	
	function returnFirstEventDate($repeatID) {
		// use "TOP" in MS SQL instead of "LIMIT" in MySQL
		$sql  = "SELECT EventDate FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE RepeatID = '$repeatID' ";
		$sql .= "ORDER BY EventDate ASC LIMIT 1";
		$result = $this->returnArray($sql);
		return $result[0]["EventDate"];
	}
	
	function returnViewerID($eventID, $isLocalSchool = true) {
		$sql  = "SELECT * FROM CALENDAR_EVENT_USER ";
		$sql .= "WHERE EventID='".$eventID."' ";
		return $this->returnArray($sql);
	}
	
	function returnReminderID($userID, $eventID) {
		$uid = $this->getCurrentUserID();
		$sql  = "SELECT ReminderID FROM CALENDAR_REMINDER ";
		$sql .= "WHERE EventID='$eventID'";
		if ($userID>0)
			$sql .= " AND UserID='$userID'";
		else
			$sql .= " AND UserID<> '".$uid."' ";
		$result = $this->returnVector($sql);
		return $result;
	}
	
	function returnReminderDetail($eventID, $excludeOwn) {
		$uid = $this->getCurrentUserID();
		$sql  = "SELECT ReminderID, ReminderBefore FROM CALENDAR_REMINDER ";
		$sql .= "WHERE EventID='$eventID'";
		if ($excludeOwn)
			$sql .= " AND UserID<> '".$uid."' ";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function returnUserEventAccess($userID, $eventID) {
		$sql  = "SELECT Access FROM CALENDAR_EVENT_USER ";
		$sql .= "WHERE EventID='$eventID' AND UserID='$userID'";
		$result = $this->returnArray($sql);
		if (sizeof($result) > 0) {
			return $result[0]["Access"];
		} else {
			$sql  = "SELECT CalID FROM CALENDAR_EVENT_ENTRY ";
			$sql .= "WHERE EventID = '$eventID'";
			$result = $this->returnArray($sql);
			if (sizeof($result) > 0) {
				$calID = $result[0]["CalID"];
				
				$sql  = "SELECT Access FROM CALENDAR_CALENDAR_VIEWER ";
				$sql .= "WHERE UserID = '$userID' AND CalID = '$calID'";
				$result = $this->returnArray($sql);
				
				if (sizeof($result) > 0)
					return trim($result[0]["Access"]);
				else
					return 0;
			} else {
				return 0;
			}
		}
	}
	 
	function returnUserCalendarAccess($userID, $calID) {
		$sql  = "SELECT Access FROM CALENDAR_CALENDAR_VIEWER ";
		$sql .= "WHERE UserID='$userID' AND CalID='$calID'";
		$result = $this->returnArray($sql);
		if (sizeof($result) > 0){
			for($i=0 ; $i<count($result) ; $i++){
				if(trim($result[$i]["Access"]) == 'A') return trim($result[$i]["Access"]);
			}
			return trim($result[0]["Access"]);
			//return $result[0]["Access"];
		} else {
			return 0;
		}
	}
	
	function returnUserPref($userID, $setting) {
		if ($setting == "AllSetting") {
			$sql = "SELECT * FROM CALENDAR_USER_PREF WHERE UserID = '$userID'";
			$result = $this->returnArray($sql);
			for ($i=0; $i<sizeof($result); $i++) {
				$pref[$result[$i]["Setting"]] = $result[$i]["Value"];
			}
			return $pref;
		} else {
			$sql = "SELECT Value FROM CALENDAR_USER_PREF WHERE Setting = '$setting' AND UserID = '$userID'";
			$result = $this->returnArray($sql);
			return $result[0]["Value"];
		}
	}
	 
	# Return a list of guests for the provided eventID
	# Optional: Join the list by comma for use in SQL
	function returnGuestList($eventID, $forSql=false) {
		$sql  = "SELECT UserID FROM CALENDAR_EVENT_USER ";
		$sql .= "WHERE EventID='$eventID'";
		$result = $this->returnVector($sql);
		if ($forSql) {
			$result = implode(",", $result);
		}
		return $result;
	}
	
	# Return a list of viewer for the provided calID
	# Optional: Join the list by comma for use in SQL
	function returnCalViewerList($calID, $forSql=false, $group=false) {
		if ($group) {
			$sql  = "SELECT DISTINCT GroupPath, Access FROM CALENDAR_CALENDAR_VIEWER ";
			$sql .= "WHERE CalID='$calID' AND Access != 'P'";
			$sql .= " AND GroupPath IS NOT NULL";
			$result = $this->returnArray($sql);
			
			for ($i=0; $i<sizeof($result); $i++) {
				$viewerID[] = $result[$i]["GroupID"];
			}
		} else {
			$sql  = "SELECT UserID, GroupID, Access, Visible FROM CALENDAR_CALENDAR_VIEWER ";
			$sql .= "WHERE CalID = '$calID' AND Access != 'P'";
			$sql .= " AND GroupPath IS NULL";
			$result = $this->returnArray($sql);
			
			for ($i=0; $i<sizeof($result); $i++) {
				$viewerID[] = $result[$i]["UserID"];
			}
		}
		/*if ($group) {
			$sql  = "SELECT v.UserID, v.GroupID, v.GroupType, v.Access, v.Visible, v.GroupPath FROM CALENDAR_CALENDAR_VIEWER as v ";
			//$sql  .= "inner join INTRANET_USER as u on v.UserID = u.UserID ";
			$sql .= "WHERE v.CalID=$calID AND v.Access != 'P' ";
			//$sql .= "AND u.Status = 'A' ";
			$sql .= "AND v.GroupPath IS NOT NULL ";
			$sql .= "ORDER BY v.GroupPath, v.UserID";
			//$sql .= " AND GroupType IS NOT NULL";
			//$sql .= " AND GroupID IS NOT NULL";
			$result = $this->returnArray($sql);
			
			for ($i=0; $i<sizeof($result); $i++) {
				$viewerID[] = $result[$i]["GroupPath"];
			}
		} else {
			$sql  = "SELECT v.UserID, v.GroupID, v.Access, v.Visible, v.GroupType, v.GroupPath FROM CALENDAR_CALENDAR_VIEWER as v ";
			//$sql  .= "inner join INTRANET_USER as u on v.UserID = u.UserID ";
			$sql .= "WHERE v.CalID=$calID AND v.Access != 'P'";
			//$sql .= "AND u.Status = 'A' ";
			$sql .= " AND v.GroupID IS NULL AND v.GroupType IS NULL AND v.GroupPath IS NULL";
			//$sql .= " AND GroupID IS NULL";
			$result = $this->returnArray($sql);
			
			for ($i=0; $i<sizeof($result); $i++) {
				$viewerID[] = $result[$i]["UserID"];
			}
		}*/
		if ($forSql) {
			$viewerID = implode(",", $viewerID);
			return $viewerID;
		} else {
			return $result;
		}
	}
	
	# See if there is any reminder in the coming N hours
	# Used to determine whether a periodic Ajax call is needed in a page
	function haveFutureReminder($periodHr) {
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$secondToAdd = $periodHr*60*60;

		$sql  = "SELECT ReminderID ";
		$sql .= "FROM CALENDAR_REMINDER ";
		$sql .= "WHERE ReminderType = 'P' AND UserID = '$uid' ";
		//$sql .= "AND ReminderDate > CURRENT_TIMESTAMP AND DATEDIFF(s, '19700101', ReminderDate) < DATEDIFF(s, '19700101', GETDATE())+$secondToAdd ";
		$sql .= "AND ReminderDate > NOW() AND UNIX_TIMESTAMP(ReminderDate) < UNIX_TIMESTAMP()+$secondToAdd ";
		
		$result = $this->returnArray($sql,1);
		
		if (sizeof($result)>0) {
			$ajaxReminder  = "$(window).load(function() {";
			$ajaxReminder .= "setInterval(function() {";
			$ajaxReminder .= "$.ajax({";
			$ajaxReminder .= "type: \"GET\",";
			$ajaxReminder .= "url: \"reminder.php\",";
			$ajaxReminder .= "dataType: \"script\"";
			$ajaxReminder .= "});";
			$ajaxReminder .= "}, 10*1000);";
			$ajaxReminder .= "});";
			return $ajaxReminder;
		} else {
			return "";
		}
	}
	
	function getRepeatPatternDisplay($repeatType, $repeatEndDate, $frequency=0, $detail="", $repeatEndDateTime="") {
		global $iCalendar_NewEvent_Repeats_Every;
		global $iCalendar_NewEvent_Repeats_Daily, $iCalendar_NewEvent_Repeats_DailyMsg1, $iCalendar_NewEvent_Repeats_DailyMsg2, $iCalendar_NewEvent_DurationDays;
		global $iCalendar_NewEvent_Repeats_EveryWeekday2, $iCalendar_NewEvent_Repeats_EveryMonWedFri2, $iCalendar_NewEvent_Repeats_EveryTuesThur2;
		global $iCalendar_NewEvent_Repeats_WeekdayArray, $iCalendar_NewEvent_Repeats_WeekdayArray2, $iCalendar_NewEvent_Repeats_Weekly2, $iCalendar_NewEvent_Repeats_Weekly3;
		global $iCalendar_NewEvent_Repeats_Monthly2, $iCalendar_NewEvent_Repeats_Monthly5, $iCalendar_NewEvent_Repeats_Count;
		global $iCalendar_NewEvent_Repeats_Yearly2, $iCalendar_NewEvent_Repeats_Yearly3;
		global $iCalendar_NewEvent_Repeats_CheckWeekday;
		
		switch($repeatType) {
			case "DAILY":
				if ($frequency==1) {
					$repeatMsg = $iCalendar_NewEvent_Repeats_Daily;
				} else {
					$repeatMsg = "$iCalendar_NewEvent_Repeats_DailyMsg1$frequency $iCalendar_NewEvent_DurationDays";
				} 
				$repeatMsg .= $iCalendar_NewEvent_Repeats_DailyMsg2;
				$repeatMsg .= $repeatEndDate;
				break;
			case "WEEKDAY":
				$repeatMsg = $iCalendar_NewEvent_Repeats_EveryWeekday2;
				$repeatMsg .= $iCalendar_NewEvent_Repeats_DailyMsg2;
				$repeatMsg .= $repeatEndDate;
				break;
			case "MONWEDFRI":
				$repeatMsg = $iCalendar_NewEvent_Repeats_EveryMonWedFri2;
				$repeatMsg .= $iCalendar_NewEvent_Repeats_DailyMsg2;
				$repeatMsg .= $repeatEndDate;
				break;
			case "TUESTHUR":
				$repeatMsg = $iCalendar_NewEvent_Repeats_EveryTuesThur2;
				$repeatMsg .= $iCalendar_NewEvent_Repeats_DailyMsg2;
				$repeatMsg .= $repeatEndDate;
				break;
			case "WEEKLY":
				$weekday_flag = preg_split('#(?<=.)(?=.)#s', $detail);
				for ($i=0; $i<sizeof($iCalendar_NewEvent_Repeats_WeekdayArray); $i++) {
					if ($weekday_flag[$i]=="1")
						$selectedWeekday .= strpos($selectedWeekday,"$iCalendar_NewEvent_Repeats_CheckWeekday")?", $iCalendar_NewEvent_Repeats_WeekdayArray2[$i]":"$iCalendar_NewEvent_Repeats_WeekdayArray2[$i]";
				}
				
				if ($frequency==1) {
					$repeatMsg = $iCalendar_NewEvent_Repeats_Weekly2;
				} else {
					$repeatMsg = "$iCalendar_NewEvent_Repeats_DailyMsg1$frequency $iCalendar_NewEvent_Repeats_Weekly3";
				}
				$repeatMsg .= $selectedWeekday;
				$repeatMsg .= $iCalendar_NewEvent_Repeats_DailyMsg2;
				$repeatMsg .= $repeatEndDate;
				break;
			case "MONTHLY":
				if (preg_match('/^\-.*/i',$detail)){
					if ($frequency==1) 
						$repeatMsg = $iCalendar_NewEvent_Repeats_Monthly2;
					else
						$repeatMsg = "$iCalendar_NewEvent_Repeats_DailyMsg1$iCalendar_NewEvent_Repeats_Monthly5";
					$weekName = Array("Sunday","Monday","Tuesday","Wednesday","Thurday","Friday","Saturday");
					$repeatMsg .= "last ".$weekName[substr($detail,3)].", until $repeatEndDate";
				}
				else{
					$monthlyDetailPiece = explode("-",$detail);
					
					if ($frequency==1) {
						$repeatMsg = $iCalendar_NewEvent_Repeats_Monthly2;
					} else {
						$repeatMsg = "$iCalendar_NewEvent_Repeats_DailyMsg1$frequency $iCalendar_NewEvent_Repeats_Monthly5";
					}
					
					if ($monthlyDetailPiece[0]=="day") {
						$repeatMsg .= $iCalendar_NewEvent_DurationDay." ".ltrim($monthlyDetailPiece[1],"0");
					} else {
						$nth = (int)$monthlyDetailPiece[0];
						$weekday = (int)$monthlyDetailPiece[1];
						$repeatMsg .= $iCalendar_NewEvent_Repeats_Count[$nth-1]." ".$iCalendar_NewEvent_Repeats_WeekdayArray2[$weekday];
					}
				}
				break;
			case "YEARLY":
				if ($frequency==1) {
					$repeatMsg = $iCalendar_NewEvent_Repeats_Yearly2;
				} else {
					$repeatMsg = "$iCalendar_NewEvent_Repeats_DailyMsg1$frequency $iCalendar_NewEvent_Repeats_Yearly3";
				}
				$repeatMsg .= date("F",$this->sqlDatetimeToTimestamp($repeatEndDateTime))." ".date("j",$this->sqlDatetimeToTimestamp($repeatEndDateTime));
				$repeatMsg .= $iCalendar_NewEvent_Repeats_DailyMsg2;
				$repeatMsg .= $repeatEndDate;
				break;
		}
		return $repeatMsg;
	}
	
	function deleteRepeatEventSeries($repeatID, $deleteRepeatPattern=FALSE) {
		$result = array();
		
		$repeatSeries = $this->returnRepeatEventSeries($repeatID); 
		
		// delete Personal Note, all guests's personal note will be deleted also!!
		$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
		$sql = 'Delete From CALENDAR_EVENT_PERSONAL_NOTE where 
						EventID in (
							select EventID from CALENDAR_EVENT_ENTRY where RepeatID = \''.$repeatID.'\')'.$eclassConstraint;
		$result['delete_event_personal_note'] = $this->db_db_query($sql);
		
		$sql = "select * from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID'";
		$relatedInfo = $this->returnArray($sql);
		
		$sql  = "DELETE FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE RepeatID = '$repeatID' OR (RepeatID is null AND CalID = '".$relatedInfo[0]["CalID"]."' AND UID = '".$relatedInfo[0]["UID"]."')";
		$result['delete_event_entry'] = $this->db_db_query($sql);
		
		if ($deleteRepeatPattern) {
			$sql  = "DELETE FROM CALENDAR_EVENT_ENTRY_REPEAT ";
			$sql .= "WHERE RepeatID = '$repeatID'";
			$result['delete_event_repeat_info'] = $this->db_db_query($sql);
		}
		
		for ($i=0; $i<sizeof($repeatSeries); $i++) {
			$sql  = "DELETE FROM CALENDAR_REMINDER ";
			$sql .= "WHERE EventID = '".$repeatSeries[$i]["EventID"]."' ";
			$result["delete_event_reminder_$i"] = $this->db_db_query($sql);
			
			$sql  = "DELETE FROM CALENDAR_EVENT_USER ";
			$sql .= "WHERE EventID = '".$repeatSeries[$i]["EventID"]."' ";
			$result["delete_event_guest_$i"] = $this->db_db_query($sql);
		}
		
		if(!in_array(false, $result)){
			return true;
		} else {
			return false;
		}
	}
	
	function deleteRepeatFollowingEvents($cutOffDate, $repeatID) {
		$result_1 = array();
		
		# retrieve series of events to be deleted
		$sql  = "SELECT * FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE RepeatID = '$repeatID' AND EventDate >= '$cutOffDate'";
		$result = $this->returnArray($sql);		
		
		$delEventID = array();
		for ($i=0; $i<sizeof($result); $i++) {
			$delEventID[$i] = $result[$i]["EventID"];
			$delEventIDSql .= $result[$i]["EventID"].", ";
		}
		$delEventIDSql = rtrim($delEventIDSql, ", ");
		
		# delete Personal Note
		$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
		$sql  = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE ";
		$sql .= "WHERE EventID IN ($delEventIDSql) $eclassConstraint";
		$result_1['delete_event_Personal Note'] = $this->db_db_query($sql);
		/*# delete other school guest Personal note
		$sql = "select * from CALENDAR_EVENT_PERSONAL_NOTE ";
		$sql .= "where EventID IN ($delEventIDSql) And FromSchoolCode is null";
		$Rresult = $this->returnArray($sql);
		$otherSchoolGuest = Array();
		foreach ($Rresult as  $r){
			$otherSchoolGuest[$r["ToSchoolCode"]][] = $r["EventID"];
		}
		foreach ($otherSchoolGuest as $key => $guest){
			$dbName = $this->get_corresponding_dbName(trim($key));
			$sql  = "DELETE FROM ".$dbName.".dbo.CALENDAR_EVENT_PERSONAL_NOTE ";
			$sql .= "WHERE EventID IN (".implode(",",$guest).") And FromSchoolCode ='".$_SESSION["SchoolCode"]."' And CalType = 4";
			$result_1['delete_event_Personal Note_'.$key] = $this->db_db_query($sql);
		}*/
		
		
		# delete event entries
		$sql  = "DELETE FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE EventID IN ($delEventIDSql) OR (RepeatID is null And UID = '".$result[0]["UID"]."' AND CalID = '".$result[0]["CalID"]."' AND EventDate >= '$cutOffDate')";
		
		$result_1['delete_event_entry'] = $this->db_db_query($sql);
		
		# delete reminders
		$sql  = "DELETE FROM CALENDAR_REMINDER ";
		$sql .= "WHERE EventID IN ($delEventIDSql)";
		$result_1['delete_event_reminder'] = $this->db_db_query($sql);
		
		# delete guests
		$sql  = "DELETE FROM CALENDAR_EVENT_USER ";
		$sql .= "WHERE EventID IN ($delEventIDSql) ";
		$result_1['delete_event_guest'] = $this->db_db_query($sql);
		/*# delete other school guest
		$sql = "select * from CALENDAR_EVENT_USER ";
		$sql .= "where EventID IN ($delEventIDSql) And FromSchoolCode is null";
		$result = $this->returnArray($sql);
		$otherSchoolGuest = Array();
		foreach ($result as  $r){
			$otherSchoolGuest[$r["ToSchoolCode"]][] = $r["EventID"];
		}
		foreach ($otherSchoolGuest as $key => $guest){
			$dbName = $this->get_corresponding_dbName(trim($key));
			$sql  = "DELETE FROM ".$dbName.".dbo.CALENDAR_EVENT_USER ";
			$sql .= "WHERE EventID IN (".implode(",",$guest).") And FromSchoolCode ='".$_SESSION["SchoolCode"]."'";
			$result_1['delete_event_guest_'.$key] = $this->db_db_query($sql);
		}*/
		
		# update repeat pattern end date
		//$sql  = "SELECT TOP 1 EventDate FROM CALENDAR_EVENT_ENTRY ";
		$sql = "SELECT EventDate FROM CALENDAR_EVENT_ENTRY ";
		$sql .= "WHERE RepeatID = '$repeatID' ";
		//$sql .= "ORDER BY EventDate DESC";
		$sql .= "ORDER BY EventDate DESC LIMIT 0,1";
		$result = $this->returnArray($sql);
		$newEndDate = $result[0]["EventDate"];
		$sql  = "UPDATE CALENDAR_EVENT_ENTRY_REPEAT SET EndDate = '$newEndDate' ";
		$sql .= "WHERE RepeatID = '$repeatID'";
		$result_1['update_event_entry'] = $this->db_db_query($sql);
		
		if(!in_array(false, $result_1)){
			return true;
		} else {
			return false;
		}
	}
	
	function insertRepeatEventSeries($detailArray, $monthlyRepeatBy="", $isIgnoreModifiedRecord=0, $uid ="", $extraInfo="") {
		$result = array(); 
		
		list($repeatID,$repeatSelect,$repeatEndDate,$frequency,$detail,$userID,$eventDate,$duration,$isImportant,$isAllDay,$access,$title,$description,$location,$url,$calID) = $detailArray;
		
		# local variables
		$repeatEndTime = $this->sqlDatetimeToTimestamp($repeatEndDate);
		$eventTimeStamp = $this->sqlDatetimeToTimestamp($eventDate);
		
		$datePieces = explode(" ", $eventDate);
		$datePieces = explode("-", $datePieces[0]);
		
		# Insert the first event in the series separately for some cases since it may not be same as the pattern
		if ($isIgnoreModifiedRecord == 0 && ($repeatSelect=="DAILY" || $repeatSelect=="WEEKDAY" || $repeatSelect=="MONWEDFRI" || $repeatSelect=="TUESTHUR" || $repeatSelect=="WEEKLY") ){
			/*
			switch($repeatSelect)
			{
				case "WEEKDAY" :
							$temp_TimeStamp = $eventTimeStamp;
							while (date("w", $temp_TimeStamp) == 0 || date("w", $temp_TimeStamp) == 6)
							{
								$temp_TimeStamp = $temp_TimeStamp + SECONDINADAY; 
							}
							$correct_event_date = date("Y-m-d H:i:s", $temp_TimeStamp);
							break;
				case "MONWEDFRI" :
							$temp_TimeStamp = $eventTimeStamp;
							while (date("w", $temp_TimeStamp) != 1 && date("w", $temp_TimeStamp) != 3 && date("w", $temp_TimeStamp) != 5)
							{
								$temp_TimeStamp = $temp_TimeStamp + SECONDINADAY; 
							}
							$correct_event_date = date("Y-m-d H:i:s", $temp_TimeStamp); 
							break;
				case "TUESTHUR" :
							$temp_TimeStamp = $eventTimeStamp;
							while (date("w", $temp_TimeStamp) != 2 && date("w", $temp_TimeStamp) != 4)
							{
								$temp_TimeStamp = $temp_TimeStamp + SECONDINADAY; 
							}
							$correct_event_date = date("Y-m-d H:i:s", $temp_TimeStamp);
							break;
				default :
							$correct_event_date = "";
							break;
			}
			
			$fieldname  = "UserID, RepeatID, EventDate, InputDate, ModifiedDate, Duration, ";
			$fieldname .= "IsImportant, IsAllDay, Access, Title, Description, ";
			$fieldname .= "Location, Url, CalID, UID, ExtraIcalInfo";
			
			if($correct_event_date != ""){
				$fieldvalue  = "$userID, $repeatID, '$correct_event_date', NOW(), NOW(), $duration, ";
				$fieldvalue .= "'$isImportant', '$isAllDay', '$access', '$title', '$description', ";
				$fieldvalue .= "'$location', '$url', '$calID', '$uid', '$extraInfo'";
			}else{
				$fieldvalue  = "$userID, $repeatID, '$eventDate', NOW(), NOW(), $duration, ";
				$fieldvalue .= "'$isImportant', '$isAllDay', '$access', '$title', '$description', ";
				$fieldvalue .= "'$location', '$url', '$calID', '$uid', '$extraInfo'";
			}
			*/
			$fieldname  = "UserID, RepeatID, EventDate, InputDate, ModifiedDate, Duration, ";
			$fieldname .= "IsImportant, IsAllDay, Access, Title, Description, ";
			$fieldname .= "Location, Url, CalID, UID, ExtraIcalInfo";
		
			$fieldvalue  = "'$userID', '$repeatID', '$eventDate', NOW(), NOW(), '$duration', ";
			$fieldvalue .= "'$isImportant', '$isAllDay', '$access', '$title', '$description', ";
			$fieldvalue .= "'$location', '$url', '$calID', '$uid', '$extraInfo'";
		
			$sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
			$result['insert_first_event'] = $this->db_db_query($sql);
		}
		
		# insert the events in the repeating series
		$fieldname  = "EventDate, UserID, RepeatID, InputDate, ModifiedDate, Duration, IsImportant, ";
		$fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID, UID, ExtraIcalInfo "; 
		
		$fieldvalue  = "'$userID', '$repeatID', NOW(), NOW(), '$duration', ";
		$fieldvalue .= "'$isImportant', '$isAllDay', '$access', '$title', '$description', ";
		$fieldvalue .= "'$location', '$url', '$calID', '$uid', '$extraInfo'";
		
		$sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ";
		//$sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) ";
		switch ($repeatSelect) {
			case "DAILY":
				$interval = ((int)$frequency)*SECONDINADAY;
				$cnt = 0;
				#echo $interval;
				//for($j=$eventTimeStamp+$interval; $j<=$repeatEndTime; $j+=$interval) {
				for($j=$eventTimeStamp; $j<=$repeatEndTime; $j+=$interval) {
					$repeatEventDate = $this->TimestampTosqlDatetime($j);
					$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
					$cnt++;
					/*if($cnt == 0){
						$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
						$cnt++;
					} else {
						$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
					}*/
				}
				$sql = rtrim($sql, ", ");
				break;
			case "WEEKDAY":
				$interval = SECONDINADAY;
				$cnt = 0;
				//for($j=$eventTimeStamp+$interval; $j<=$repeatEndTime; $j+=$interval) {
				for($j=$eventTimeStamp; $j<=$repeatEndTime; $j+=$interval) {
					if(date("w", $j) != 0 && date("w", $j) != 6) {
						$repeatEventDate = $this->TimestampTosqlDatetime($j);
						$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
						/*if($cnt == 0){
							$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
							$cnt++;
						} else {
							$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
						}*/$cnt++;
					}
				}
				$sql = rtrim($sql, ", ");
				break;
			case "MONWEDFRI":
				$interval = SECONDINADAY;
				$cnt = 0;
				//for($j=$eventTimeStamp+$interval; $j<=$repeatEndTime; $j+=$interval) {
				for($j=$eventTimeStamp; $j<=$repeatEndTime; $j+=$interval) {
					if(date("w", $j) == 1 || date("w", $j) == 3 || date("w", $j) == 5) {
						$repeatEventDate = $this->TimestampTosqlDatetime($j);
						$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
						/*if($cnt == 0){
							$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
							$cnt++;
						} else {
							$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
						}*/$cnt++;
					}
				}
				$sql = rtrim($sql, ", ");
				break;
			case "TUESTHUR":
				$interval = SECONDINADAY;
				$cnt = 0;
				//for($j=$eventTimeStamp+$interval; $j<=$repeatEndTime; $j+=$interval) {
				for($j=$eventTimeStamp; $j<=$repeatEndTime; $j+=$interval) {
					if(date("w", $j) == 2 || date("w", $j) == 4) {
						$repeatEventDate = $this->TimestampTosqlDatetime($j);
						$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
						/*if($cnt == 0){
							$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
							$cnt++;
						} else {
							$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
						}*/$cnt++;
					}
				}
				$sql = rtrim($sql, ", ");
				break;
			case "WEEKLY":
				$interval = SECONDINADAY;
				$weekday_chosen = array();
				$weekday_flag = preg_split('#(?<=.)(?=.)#s', $detail);
				
				for($i=0; $i<sizeof($weekday_flag); $i++) {
					if($weekday_flag[$i] == "1")
						$weekday_chosen[] = $i;
				}
				
				$count_frequency = $frequency;
				$count_weekday = sizeof($weekday_chosen);

				# Handle the case of StartDate is the first day of selected weekdays
				$isFirstDayStartDate = 0;
				//if(in_array(date("w", $eventTimeStamp), $weekday_chosen) && $count_weekday == 1){
					//$count_frequency++;											# added on 28 Aug
					//$isFirstDayStartDate = 1;
				//}
				
				$seconds_one_week = 7 * SECONDINADAY; // number of seconds in one week
				$accumulated_seconds = 0;
				$cnt = 0;
				//for($j=$eventTimeStamp+$interval; $j<=$repeatEndTime; $j+=$interval) {
				for($j=$eventTimeStamp; $j<=$repeatEndTime; $j+=$interval) {
					$weekday_today = date("w", $j);
					if(in_array($weekday_today, $weekday_chosen)) {
						if (($count_frequency % $frequency) == 0) {
							$repeatEventDate = $this->TimestampTosqlDatetime($j);
							$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
							/*if($cnt == 0){
								$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
							}*/$cnt++;
						}
						/*
						# old approach of counting weekly repeated series
						$count_weekday++;
						if (($count_weekday % sizeof($weekday_chosen)) == 0)
							$count_frequency++;
						*/
					}

					# new approach of counting weekly repeated series	added on 28 Aug
					//if($weekday_today == 6 && $isFirstDayStartDate == 0){  $count_frequency++;  }
					//if($weekday_today == 6 && $isFirstDayStartDate == 1){  $isFirstDayStartDate = 0;  }
					$accumulated_seconds += $interval;
					if( ($accumulated_seconds % $seconds_one_week) == 0) {  $count_frequency++;  } // increase frequency count for one week pass
				}
				$sql = rtrim($sql, ", ");
				// echo $sql;
				// exit;
				break;
			case "MONTHLY":
				$cnt = 0;	// for UNION ALL use for checking SQL
				if ($monthlyRepeatBy == "dayOfMonth") {	// Monthly recurrece pattern - day of the month, e.g. 15th of every 2 months
					if (date("j",$eventTimeStamp) == 31 || date("j",$eventTimeStamp) == 30 || date("j",$eventTimeStamp) == 29){
						$last_month_time = $eventTimeStamp - 3*24*60*60;
					} else{
						$last_month_time = $eventTimeStamp;
					}
					$last_month = date("n", strtotime("+".$frequency." months ago", $last_month_time));
					for ($j=$eventTimeStamp; $j<=$repeatEndTime; $j=$this->returnNextNthDayOfMonth($j)) {
						$cur_month = date("n",$j);
						if ($cur_month < $last_month)
							$cur_change_month = $cur_month+12;
						else
							$cur_change_month = $cur_month;
						if ($cur_change_month-$last_month == (int)$frequency || $cur_change_month-$last_month == 0) {
							$repeatEventDate = $this->TimestampTosqlDatetime($j);
							$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
							/*if($cnt == 0){
								$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
							}*/$cnt++;
						}
						$last_month = $cur_month;
					}
				} else {	// Monthly recurrece pattern - day of the week, e.g. 3rd Tuesday of every 2 months
					$eventHr = date("H", $eventTimeStamp);			// newly added on 26 May by Jason
					$eventMin = date("i", $eventTimeStamp);			// newly added on 26 May by Jason
					$weekday_selected = date("w", $eventTimeStamp);
					$nth = 0;
					
					/*$daysofmonth = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
					$daysofmonthLY = Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
					
					if (date("L",$start)==0)
						$lastDay = $daysofmonth[date("n",$eventTimeStamp))];
					else
						$lastDay = $daysofmonthLY[date("n",$eventTimeStamp))];*/
					//echo $detail; 
					//exit;
					if (preg_match('/^\-.*/i',$detail)){   //insert event for the last weekady
						$weekName = Array("SU","MO","TU","WE","TH","FR","SA");
						$startUTC = $this->toUTC($eventDate);
						$endUTC = $this->toUTC($repeatEndDate);
						$eventRule = Array("freq"=>"MONTHLY", "interval"=> $frequency, "byDay"=>"-1".$weekName[$detail{strlen($detail)-1}],"until"=>$repeatEndDate);
						$allDays = $this->get_all_repeated_day($startUTC,$endUTC,$eventRule);
						
						$timeSuff = explode(" ",$eventDate);
												
						$fieldname  = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
						$fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID, RepeatID, UID, ExtraIcalInfo";
						
						foreach ($allDays as $eventDay){
										
							$sql = "insert into CALENDAR_EVENT_ENTRY($fieldname) values(";
							$sql .= "'$userID', ";
							$sql .= "'".$eventDay." ".$timeSuff[1]."', ";
							$sql .= "NOW(), ";
							$sql .= "NOW(), ";
							//$sql .= "DATEDIFF(s, '19700101', GETDATE()), ";
							//$sql .= "DATEDIFF(s, '19700101', GETDATE()), ";
							$sql .= "'$duration', ";
							$sql .= "'$isImportant', ";
							$sql .= "'".$isAllDay."', ";
							$sql .= "'$access', ";
							$sql .= "'".$this->Get_Safe_Sql_Query($title)."', ";
							$sql .= "'".$this->Get_Safe_Sql_Query($description)."', ";
							$sql .= "'".$this->Get_Safe_Sql_Query($location)."', ";
							$sql .= "'$url', ";
							$sql .= "'".$calID."', ";							
							$sql .= "'$repeatID', '$uid', '$extraInfo')";
							
							$this->db_db_query($sql);
						}
						/*global $schoolNameAbbrev;
						$sql = "select min(EventID) as minEvent from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID'";
						$minEventID = $this->returnArray($sql);
						
						$uid = $_SESSION['SSV_USERID']."-{$calID}-".$minEventID[0]["minEvent"]."@{$schoolNameAbbrev}.tg";
						$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where RepeatID = '$repeatID'";
						$this->db_db_query($sql);*/
						return true;
					}
					
					// find out the event date is which nth weekday of the month
					for($i=1; $i<6; $i++) {
						$nthweekday = $this->NWeekdayOfMonth($i,$weekday_selected,$datePieces[1],$datePieces[0],"d");
						if ((int)$datePieces[2] == (int)$nthweekday) {
							$nth = $i;
							break;
						}
					}
					
					
					if (date("j",$eventTimeStamp) == 31 || date("j",$eventTimeStamp) == 30 || date("j",$eventTimeStamp) == 29)
						$last_month_time = $eventTimeStamp - 3*24*60*60;
					else
						$last_month_time = $eventTimeStamp;
					# $frequency = $repeatMonthlyRange	// newly added on 26 May by Jason
					$last_month = date("n", strtotime(date("Y-m-d",$last_month_time)." + ".$frequency." months ago"))."\n";
					$lower_nth = $this->sqlDatetimeToTimestamp($this->NWeekdayOfMonth($nth, $weekday_selected, date("m",$eventTimeStamp), date("Y",$eventTimeStamp), "Y-m-d H:i:s"));
					$last_j = $lower_nth;
					
					//for ($j=$lower_nth; $j<=$repeatEndTime; $j=$this->sqlDatetimeToTimestamp($this->NWeekdayOfMonth($nth,$weekday_selected,date("m",$j)+$repeatMonthlyRange,date("Y",$j),"Y-m-d H:i:s"))) {
					for ($j=$lower_nth; $j<=$repeatEndTime; $j=$this->sqlDatetimeToTimestamp($this->NWeekdayOfMonth($nth,$weekday_selected,date("m",$j)+$frequency,date("Y",$j),"Y-m-d H:i:s"))) {
						if ($j != 0) {	// The nth weekday is present at this month
							$j = ($eventHr > 0) ? $j + $eventHr*60*60 : $j;				// newly added on 26 May by Jason
							$j = ($eventMin > 0) ? $j + $eventMin*60  : $j;				// newly added on 26 May by Jason
						
							$repeatEventDate = $this->TimestampTosqlDatetime($j);
							$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
							/*if($cnt == 0){
								$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
							}*/$cnt++;
							
							$last_j = $j;
						} else {	// The nth weekday is absent
							$k = 1;
							// loop $j until the next month with the presence of nth weekday is found
							while ($j==0) {
								$j = $last_j;
								$j = $this->sqlDatetimeToTimestamp($this->NWeekdayOfMonth($nth,$weekday_selected,date("m",$j)+($k*$frequency),date("Y",$j),"Y-m-d H:i:s"));
								$k++;
							}
							// Day 29th/30th/31st will cause problem during the calculation of -1 month, e.g. 31/12/2007 -1 month = 1/12/2007, not 31/11/2007
							if (date("j", $j) == 29 || date("j", $j) == 30 || date("j", $j) == 31)
								$j = $this->changeTimestamp($j,"","",28);
							// -1 month so that the the next iteration of for-loop can reach the month with the presence of nth weekday	
							$j = strtotime("-".$frequency.($frequency==1?" month":" months"), $j);
						}
					}
				}
				$sql = rtrim($sql, ", ");
				break;
			case "YEARLY":
				// Handle 29th Feb of leap year
				$cnt = 0;
				if (date("j", $eventTimeStamp)==29 && date("n", $eventTimeStamp)==2) {
					// Only the Least Common Multiple (LCM) of the year range and 4 need to be considered
					for($j=$eventTimeStamp; $j<=$repeatEndTime; $j=strtotime("+".$this->lcm(4,$frequency)." years" ,$j)) {
						if ((date("Y", $j))%400 == 0) {
							
						} else if ((date("Y", $j))%100 == 0) {
							continue;
						} else if ((date("Y", $j))%4 == 0) {
							
						} else {
							continue;
						}
						$repeatEventDate = $this->TimestampTosqlDatetime($j);
						$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
						/*if($cnt == 0){
							$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
							$cnt++;
						} else {
							$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
						}*/$cnt++;
					}
				} else {
					for($j=$eventTimeStamp; $j<=$repeatEndTime; $j=strtotime("+".$frequency." years" ,$j)) {
						$repeatEventDate = $this->TimestampTosqlDatetime($j);
						$sql .= "('$repeatEventDate', ".$fieldvalue."), ";
						/*if($cnt == 0){
							$sql .= "SELECT '$repeatEventDate', ".$fieldvalue."";
							$cnt++;
						} else {
							$sql .= " UNION ALL SELECT '$repeatEventDate', ".$fieldvalue."";
						}*/$cnt++;
					}
				}
				$sql = rtrim($sql, ", ");
				// echo $sql;
				// exit;
				break;
		}
		if($cnt > 0){
			$result['insert_repeat_event_series'] = $this->db_db_query($sql);
		}
		//debug_r($sql);
		if(!in_array(false, $result)){
			return true;
		} else {
			return false;
		}
	}
	
	function generateReminderSelect($reminderType="", $reminderTime=0) {
		global $iCalendar_NewEvent_Reminder_Popup, $iCalendar_NewEvent_Reminder_Email;
		global $iCalendar_NewEvent_DurationMins, $iCalendar_NewEvent_DurationHr, $iCalendar_NewEvent_DurationHrs;
		global $iCalendar_NewEvent_DurationDay, $iCalendar_NewEvent_DurationDays;
		global $iCalendar_NewEvent_DurationWeek, $iCalendar_NewEvent_DurationWeeks;
		global $iCalendar_NewEvent_Reminder_Remove;
		
		## reminder method
		$array_reminder_data = array();
		$array_reminder_data[] = array($iCalendar_NewEvent_Reminder_Popup, "P");
		//$array_reminder_data[] = array($iCalendar_NewEvent_Reminder_Email, "E");
		$select_reminder_type = "<select name='reminderType[]' style='display:none'>";
		for ($i=0; $i<sizeof($array_reminder_data); $i++) {
			$select_reminder_type .= "<option value='".$array_reminder_data[$i][1]."'".(($array_reminder_data[$i][1]==$reminderType)?" selected":"").">".$array_reminder_data[$i][0]."</option>";
		}
		$select_reminder_type .= "</select>";
		
		## reminder time
		$select_reminder_interval = "<select name='reminderTime[]'>";
		#print_r($this->reminderPeriod);
		for ($i=0; $i<sizeof($this->reminderPeriod); $i++) {
			if ($this->reminderPeriod[$i] < 60) {
				$select_reminder_interval .= "<option value='".($this->reminderPeriod[$i])."'".(($reminderTime==($this->reminderPeriod[$i]))?" selected":"").">".$this->reminderPeriod[$i]." $iCalendar_NewEvent_DurationMins</option>";
			} else if ($this->reminderPeriod[$i] < 1440) {
				$select_reminder_interval .= "<option value='".($this->reminderPeriod[$i])."'".(($reminderTime==($this->reminderPeriod[$i]))?" selected":"").">";
				$select_reminder_interval .= ($this->reminderPeriod[$i]/60).((($this->reminderPeriod[$i]/60) == 1)?" $iCalendar_NewEvent_DurationHr":" $iCalendar_NewEvent_DurationHrs")."</option>";
			} else if ($this->reminderPeriod[$i] < 10080) {
				$select_reminder_interval .= "<option value='".($this->reminderPeriod[$i])."'".(($reminderTime==($this->reminderPeriod[$i]))?" selected":"").">";
				$select_reminder_interval .= ($this->reminderPeriod[$i]/1440).((($this->reminderPeriod[$i]/1440) == 1)?" $iCalendar_NewEvent_DurationDay":" $iCalendar_NewEvent_DurationDays")."</option>";
			} else {
				$select_reminder_interval .= "<option value='".($this->reminderPeriod[$i])."'".(($reminderTime==($this->reminderPeriod[$i]))?" selected":"").">";
				$select_reminder_interval .= ($this->reminderPeriod[$i]/10080).((($this->reminderPeriod[$i]/10080) == 1)?" $iCalendar_NewEvent_DurationWeek":" $iCalendar_NewEvent_DurationWeeks")."</option>";
			}
		}
		$select_reminder_interval .= "</select>";
		
		######## disable email reminder type (always popup) ########
		#$select_reminder_type .= "<input type='hidden' name='reminderType[]' value='P' />";
		########################################
		return $select_reminder_type." ".$select_reminder_interval;
	}
	
	function generateViewCalendarControl($keepSyncLink=true) {
		global $iCalendar_Calendar_MyCalendar, $iCalendar_Calendar_OtherCalendar, $iCalendar_Calendar_ManageCalendar;
		global $iCalendar_Calendar_OtherCalendar_School, $iCalendar_Calendar_InvolveEvent, $UserID, $iCalendar_group;
		global $iCalendar_Calendar_OtherCalendar_Foundation, $iCalendar_Calendar_otherInvitation,$iCalendar_course,$plugin;
		
		$uid = $this->getCurrentUserID();
		
		$viewableCalendar 	  = $this->returnViewableCalendar();			# Personal & Shared Calendar
		$ownerDefaultCalID 	  = $this->returnOwnerDefaultCalendar();		
		//$foundationCalendar	  = $this->returnFoundationCalendar();			# Foundation Calendar
		//$otherCalTypeClanedar = $this->returnOtherCalTypeCalendar();		# Other CalType Calendar like cpd, cas
		
		$schoolCalVisible 		= $this->returnUserPref($uid, "schoolCalVisible");
		$involveCalVisible 		= $this->returnUserPref($uid, "involveCalVisible");
		
		//debug_r($viewableCalendar);
		//debug_r($ownerDefaultCalID);
		
		#other school invitaion
		//$otherCalVisible 		= $this->returnUserPref($UserID, "otherInvitaionCalVisible");
		//$foundationCalVisible   = $this->returnUserPref($UserID, "foundationCalVisible");
		
		
		## Main
		$control = "<form id='cal_form' name='cal_form' method='post'>
					<ul>";
		# Owner Calendar
		$controlMy = "<li><a href='javascript:void(0)' onclick='$(this).next().toggle()'><span class='cal_calendar_group'>$iCalendar_Calendar_MyCalendar</span></a><ul>";
		
		# Optional Calendar
		$controlOther = "<li><a href='javascript:void(0)' onclick='$(this).next().toggle()'><span class='cal_calendar_group'>$iCalendar_Calendar_OtherCalendar</span></a><ul>";

		# Group Calendar
		$controlGroup = "<li><a href='javascript:void(0)' onclick='$(this).next().toggle()'><span class='cal_calendar_group'>$iCalendar_group</span></a><ul>";
		
		# CourseCalendar
		$controlCourse = "<li><a href='javascript:void(0)' onclick='$(this).next().toggle()'><span class='cal_calendar_group'>$iCalendar_course</span></a><ul>";
					
		# School Calendar 
		$controlSchool = "<li><a href='javascript:void(0)' onclick='$(this).next().toggle()'><span class='cal_calendar_group'>$iCalendar_Calendar_OtherCalendar_School</span></a><ul>";
		
	
		########### generate five school calendar (eclass only) ##########
		$iCal_api = new icalendar_api($uid);
		#### School Event
		$controlSchool  .= $iCal_api->getSchoolEventControlPanel();
		#### Academic Event
		$controlSchool  .= $iCal_api->getAcademicEventControlPanel();
		#### Group Event
		$controlSchool  .= $iCal_api->getGroupEventControlPanel();
		#### Public hoilday
		$controlSchool  .= $iCal_api->getPublicHolidayControlPanel();
		#### School hoilday
		$controlSchool  .= $iCal_api->getSchoolHolidayControlPanel();
		########### end of generate five school calendar (eclass only) ##########
		
if (($plugin["iCalendarFull"] && $_SESSION['UserType']==USERTYPE_STAFF) || $plugin["iCalendarFullToAllUsers"]){			
		########### generate other external calendar (eclass only) ##########
		
		 $controlOther .= $iCal_api->getEnrollmentControlPanel();
		 $controlOther .= $iCal_api->getDetentionControlPanel();
		 $controlOther .= $iCal_api->getHomeworkControlPanel();
		 
		########### End of generate other external calendar (eclass only) ##########
	
}	

		# Foundation Calendar 
		//$controlFoundation = "<li><span class='cal_calendar_group'>$iCalendar_Calendar_OtherCalendar_Foundation</span></li><ul>";
		
		# Involved Calendar
		$controlInvolved  = "<li><div align='right'><div style='width:93%;text-align:left'><span style='background-color:#".$this->involveCalColor.";color:#".$this->involveCalFontColor.";'>";
		$controlInvolved .= "<input type='checkbox' id='toggleVisible' name='toggleVisible' onClick='toggleInvolveEventVisibility()' ".($involveCalVisible=="1"?"checked='checked'":"")." />";
		$controlInvolved .= "&nbsp;".$iCalendar_Calendar_InvolveEvent;
		$controlInvolved .= "</span>
		<input type='hidden' value='toggleVisible' id='Vinvolve' name='Vinvolve'>
		</div></li>"; 
		
		
		# other invitation Calendar
		//$controlInvolved  .= "<li><div align='right'><div style='width:93%;text-align:left'><span style='background-color:#".$this->otherCalColor.";color:#".$this->otherCalFontColor.";'>";
		//$controlInvolved .= "<input type='checkbox' id='toggleOtherVisible' name='toggleOtherVisible' onClick='toggleOtherInvitaionVisibility()' ".($otherCalVisible=="1"?"checked='checked'":"")." />";
		//$controlInvolved .= "&nbsp;".$iCalendar_Calendar_otherInvitation;
		//$controlInvolved .= "</span></div></li>";
		
		/*$sql = "select * from CALENDAR_CALENDAR_VIEWER as v inner join CALENDAR_CALENDAR as c on
				v.CalID = c.CalID AND c.Owner = v.UserID
				where v.UserID = '".$_SESSION["SSV_USERID"]."' AND c.CalType = 4";
		$resultSet = $this->returnArray($sql);*/
		
		# Other School invitation Calendar
		/*$idName = "toggleCalVisible-OtherCal";
		$controlInvolved  .= "<li><div align='right'><div style='width:93%;text-align:left'><span style='background-color:#".$this->otherCalColor.";color: white;'>";
		$controlInvolved .= "<input type='checkbox' id='$idName' name='$idName' onClick=\"toggleCalVisibility('OtherCal')\" ".($otherCalVisible=="1"?"checked='checked'":"")." />";
		$controlInvolved .= '<input id="CalVisibleID_OTHER" type="hidden" value="OtherCal" name="CalVisibleID[]"/>';
		$controlInvolved .= '<input id="CalVisibility_OtherCal" type="hidden" value="1" name="CalVisibility_OtherCal"/>';
		
		$controlInvolved .= "&nbsp;".$i_Calendar_other_Invite;
		$controlInvolved .= "</span></div></li>";*/
		
		
		# Get Foundation Calendar
	/*	if(count($foundationCalendar) > 0){
			$tmp1 = '';
			for($i=0 ; $i<count($foundationCalendar) ; $i++){
				$syncImage="";
				$syncUrl = trim($viewableCalendar[$i]["SyncURL"]);
				$accessRight = trim($viewableCalendar[$i]["Access"]);
				if (!empty($syncUrl) && ($accessRight=="A"||$accessRight="W"))
					$syncImage = '<a href="javascript:void(0)"><img src="/theme/image_en/calendar/icon_synchronize.gif" style="border:0px" /></a>';
			
				$eleid   = "toggleFoundationCalVisible-".$foundationCalendar[$i]['CalID'];
				$elename = "toggleFoundationCalVisible[]";
				$id = $foundationCalendar[$i]["CalID"];
				$foundationCalVisible = trim($foundationCalendar[$i]['Visible']);
				$checked = ($foundationCalVisible != '') ? (($foundationCalVisible==1) ? 'checked="checked"' : '') : 'checked="checked"';
				$visible = ($foundationCalVisible != '') ? $foundationCalVisible : 1;
				$calType = trim($foundationCalendar[$i]['CalType']);
				$color = trim($foundationCalendar[$i]['Color']);
				$onclick = 'toggleFoundationEventVisibility(this, \''.$id.'\', \''.$calType.'\', \''.$color.'\')';
				
				//$color = $this->getCalDetail($id, true); // true : this calendar belongs Foundation Calendar
				
				$tmp1 .= "<li style='width:140;overflow:hidden'><span style='background-color:".$color.";color:#FFFFFF;'>";
				$tmp1 .= "<input type=\"checkbox\" name=\"$elename\" id=\"$eleid\"onClick=\"$onclick\" $checked /> ";
				$tmp1 .= "<input type=\"hidden\" name=\"CalVisibleID[]\" id=\"CalVisibleID_F".$i."\" value=\"F$id\" />";
				$tmp1 .= "<input type=\"hidden\" name=\"CalVisibility_F$id\" id=\"CalVisibility_F$id\" value=\"".$visible."\" />";
				$tmp1 .= $foundationCalendar[$i]["Name"].$syncImage;
				//$tmp1 .= "";
				$tmp1 .= "</span></li>";
			}
			$controlFoundation .= $tmp1;
		}*/
		
		# Get Personal, School & Shared Calendar  
		if (sizeof($viewableCalendar) > 0) {
			for ($i=0; $i<sizeof($viewableCalendar); $i++) {
				// if ($viewableCalendar[$i]["CalType"] == 4){
					// continue;
				// }
				$syncImage="";
				$syncUrl = trim($viewableCalendar[$i]["SyncURL"]);
				$accessRight = trim($viewableCalendar[$i]["Access"]);
				if (!empty($syncUrl) && ($accessRight=="A"||$accessRight="W") && $plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF)
					$syncImage = ($keepSyncLink?'<a href="javascript:SynchronizeCal(\''.$viewableCalendar[$i]["CalID"].'\')">':'').'
					<img src="/theme/image_en/calendar/icon_synchronize.gif" style="border:0px" />'.($keepSyncLink?'</a>':'');
				
				if($viewableCalendar[$i]["CalID"] == $ownerDefaultCalID){
					$onclick  = "toggleOwnDefaultCalVisibility('".$viewableCalendar[$i]["CalID"]."');";
				} else {
					$onclick  = "toggleCalVisibility('".$viewableCalendar[$i]["CalID"]."');";
				}
				$name = "toggleCalVisible-".$viewableCalendar[$i]["CalID"];
				$id = $name;
				$value = $viewableCalendar[$i]["CalID"];
				$checked = ($viewableCalendar[$i]["Visible"] == "1") ? "checked" : "";
				$calType = trim($viewableCalendar[$i]["CalType"]);
				
				$div = "";
				$div .= "<li style='width:140;overflow:hidden'><span style='background-color:".$viewableCalendar[$i]["Color"].";color:#FFFFFF;'>";
				$div .= "<input type=\"checkbox\" name=\"$name\" id=\"$id\"onClick=\"$onclick\" $checked /> ";
				$div .= "<input type=\"hidden\" name=\"CalVisibleID[]\" id=\"CalVisibleID_".$i."\" value=\"$value\" />";
				$div .= "<input type=\"hidden\" name=\"CalVisibility_$value\" id=\"CalVisibility_$value\" value=\"".$viewableCalendar[$i]["Visible"]."\" />";
				$div .= "<input type=\"hidden\" name=\"Vcal-".$viewableCalendar[$i]["CalID"]."\" id=\"Vcal-".$viewableCalendar[$i]["CalID"]."\" value=\"$id\" />";
				// $div .= ($calType == 2) ? "<input type=\"hidden\" name=\"FoundationCalID[]\" id=\"FoundationCalID_$i\" value=\"".$value."\" />" : "";
				if ($viewableCalendar[$i]["GroupID"] == "" ){
				//&& $viewableCalendar[$i]["CalType"] != 4){
					if($viewableCalendar[$i]["Owner"] != $uid && $calType == 1){
                        $div .= intranet_undo_htmlspecialchars($viewableCalendar[$i]["Name"]).$syncImage;
                    } else {
						$div .= "<a class=\"viewCalControlLink\" href=\"new_calendar.php?calID=".$viewableCalendar[$i]["CalID"]."\">".intranet_undo_htmlspecialchars($viewableCalendar[$i]["Name"])."</a>".$syncImage;	 
					}
				} else {//if ($viewableCalendar[$i]["CalType"] != 4){
					$div .= "<a class=\"viewCalControlLink\" href=\"new_calendar.php?groupID=".$viewableCalendar[$i]["GroupID"]."&groupType=".$viewableCalendar[$i]["GroupType"]."&calID=".$viewableCalendar[$i]["CalID"]."\">".intranet_undo_htmlspecialchars($viewableCalendar[$i]["Name"])."</a>".$syncImage;
				}
				/*else{
					$div .= $viewableCalendar[$i]["Name"].$syncImage;
				}*/
				$div .= "</span></li>";
				
				if ($viewableCalendar[$i]["Owner"] == $uid ){
				//&& $viewableCalendar[$i]["CalType"] != 4){
					
					$controlMy .= $div;
					$controlMy .= ($viewableCalendar[$i]["CalID"] == $ownerDefaultCalID) ? $controlInvolved : "";
				} else {
					if($viewableCalendar[$i]["CalType"] == 1){	# School Calendar List
						$controlSchool .= $div;
					}
					else if ($viewableCalendar[$i]["CalType"] == 2){
						$controlGroup .= $div;  # Group
					}
					else if ($viewableCalendar[$i]["CalType"] == 3){
						$controlCourse .= $div;
					}
					else {									# Optional Calendar List
						$controlOther .= $div;
					}
				}
			}
		} else {
			$controlMy .= "<li><span style='color:#CCCCCC'>$iCalendar_Calendar_NoCalendar</span></li>";
		}
		
		# Get Other CalType Calendar
		/*if(count($otherCalTypeClanedar) > 0){
			$tmp1 = '';
			for($i=0 ; $i<count($otherCalTypeClanedar) ; $i++){		
					
				$RoleRightCondition = trim($otherCalTypeClanedar[$i]['RoleRightCondition']);
				$RoleRightList = $otherCalTypeClanedar[$i]['RoleRightName'];
				$RoleRightArr = explode(",", $RoleRightList);
				
				if(Auth($RoleRightArr, '', $RoleRightCondition)){	# Check CPD User Role Right
					$cal_type_name = trim($otherCalTypeClanedar[$i]['CalTypeName']);
					$manage_type = trim($otherCalTypeClanedar[$i]['ManageType']);
					$cal_type = trim($otherCalTypeClanedar[$i]['CalType']);
					$eleid   = "toggleOtherCalTypeCalVisible-".$otherCalTypeClanedar[$i]['CalID'];
					$elename = "toggleOtherCalTypeCalVisible[]";
					$id = $otherCalTypeClanedar[$i]["CalID"];
					
					$calTypeSetting = $this->Get_Other_CalType_Settings($cal_type);
					$pref_name = $cal_type_name.'CalVisible';
					$otherCalTypeCalVisible = $this->returnUserPref($UserID, $pref_name);
					
					$checked = ($otherCalTypeCalVisible != '') ? (($otherCalTypeCalVisible==1) ? 'checked="checked"' : '') : 'checked="checked"';
					$visible = ($otherCalTypeCalVisible != '') ? $otherCalTypeCalVisible : 1;
					$onclick = 'toggleOtherCalTypeEventVisibility(\''.$cal_type.'\')';
					
					$color = $otherCalTypeClanedar[$i]['Color'];
					
					$tmp1 .= "<li style='width:140;overflow:hidden'><span style='background-color:".$color.";color:#FFFFFF;'>";
					$tmp1 .= "<input type=\"checkbox\" name=\"$elename\" id=\"$eleid\"onClick=\"$onclick\" $checked /> ";
					$tmp1 .= "<input type=\"hidden\" name=\"CalVisibleID[]\" id=\"CalVisibleID_".$cal_type_name.$i."\" value=\"".$cal_type_name.$id."\" />";
					$tmp1 .= "<input type=\"hidden\" name=\"CalVisibility_".$cal_type_name.$id."\" id=\"CalVisibility_".$cal_type_name.$id."\" value=\"".$visible."\" />";
					$tmp1 .= $otherCalTypeClanedar[$i]["Name"];
					$tmp1 .= "</span></li>";
				}
			}
			
			if($manage_type == PERSONAL_CALENDAR){			# Personal Cal List
				$controlMy .= $tmp1;
			} else if($manage_type == SCHOOL_CALENDAR){	# School Cal List
				$controlSchool .= $tmp1;
			} else if($manage_type == FOUNDATION_CALENDAR){	# Foundation Cal List
				$controlFoundation .= $tmp1;
			}
		}*/
 
		# Owner Calendar List
		$control .= $controlMy."</ul></li>";

		///if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
			# School Calendar List 
			$control .= "<p class='dotline_p_narrow'></p>".$controlSchool."</ul></li>";
			
			# School Calendar List 
			//$control .= "<p class='dotline_p_narrow'></p>".$controlFoundation."</ul>";
			# group Calendar List 
			$control .="<p class='dotline_p_narrow'></p>".$controlGroup."</ul></li>";
			# course Calendar List 
			$control .="<p class='dotline_p_narrow'></p>".$controlCourse."</ul></li>";
		//}
		# Optional calendar List
		$control .= "<p class='dotline_p_narrow'></p>".$controlOther."</ul></li>";

		//$control .= $controlMy."</ul><p class='dotline_p_narrow'></p>"; #.$controlOther."</ul><p class='dotline_p_narrow'></p>";
		/*
		# Save As Default
		$control .= '<p class="dotline_p_narrow"></p>';
		$control .= '<div class="sub_layer_button">
				  <a class="sub_layer_link" href="javascript:jSave_Cal_As_Default()">Save as Default</a> | 
				  <a class="sub_layer_link" href="javascript:jReset_Cal_Form();">Restore</a>
				</div>';
		*/
		$control .= '</form>';
		# AJAX - update function
		$control .= '
			<script language="javascript">
			function jReset_Cal_Form(){
				$("#cal_form").resetForm();
			}

			function jSave_Cal_As_Default() {
				$("#cal_form").ajaxSubmit({
					url: "cal_visible_update.php", success: function() {
					}
				});
			}
			
			function SynchronizeCal(calID){
				//if ((location.pathname).match("module/calendar/index\.php$") || (location.pathname).match("module/calendar/$"))
					
					getCalendarDisplay(shownPeriod, calID);
			} 
			</script>
			';
		
				
		return $control;
	}
	
  function generateMonthlyCalendar($year, $month, $print=false, $calViewMode='simple') {
	  global $ImagePathAbs,$iCalendar_new;
      $first_of_month = mktime(0,0,0, $month, 1, $year);
     
      static $day_headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
      $maxdays   = date('t', $first_of_month); # number of days in the month
      $date_info = getdate($first_of_month);   # get info about the first day of the month     
      $month = $date_info['mon'];
      $year = $date_info['year'];

      $calendar = "
	  <input type='hidden' id='startDay' name='startDay' value='1'>
	  <input type='hidden' id='maxDays' name='maxDays' value='$maxdays'>
	  <table cellspacing='0' id='monthly' align='center'>";
      $calendar .= "<colgroup>
      				<col class='Sun' />
					<col class='Mon' />
					<col class='Tue' />
					<col class='Wed' />
					<col class='Thu' />
					<col class='Fri' />
					<col class='Sat' />
					<col />
				</colgroup>";
			$calendar .= "<thead>
					<tr>
					<th class='weekTitle weekTitleText'>Sun</th>
					<th class='weekTitle weekTitleText'>Mon</th>
					<th class='weekTitle weekTitleText'>Tue</th>
					<th class='weekTitle weekTitleText'>Wed</th>
					<th class='weekTitle weekTitleText'>Thu</th>
					<th class='weekTitle weekTitleText'>Fri</th>
					<th class='weekTitle weekTitleText'>Sat</th>
					".((!$print) ? "<td class='cal_view_week' style='width:2%'></td>" : "")."
					</tr>
				</thead>";       
      $calendar .= "<tbody><tr>";
      $class  = "";
      $weekday = $date_info['wday']; #weekday (zero based) of the first day of the month

      $calendar .= "<tr ".(($print) ? "" : "height=\"115\"").">";
      
      # set class of cell: normal or print
      $monthCellClass = ($print) ? "monthCellPrint" : "monthCell";
      $monthTodayCellClass = ($print) ? "monthTodayCellPrint" : "monthTodayCell";
      
      #take care of the first "empty" number of the month
      if($weekday > 0) {
      	for ($i=0; $i<$weekday; $i++) {
        	$calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
      	}
      }
    
    #print the number of the month
    $week = 1;
	
	//*******************
	$conEvent = Array();
	$associateDay = Array();
    for ($day=1; $day<=$maxdays; $day++) {
    	$ts = mktime(0,0,0,$month,$day,$year);
//           debug_pr($ts);
      	if ($year."-".$month."-".$day == date("Y-n-j", time())){
      		$calendar .= "<td style='width:105px;max-width:105px;min-width:103px;overflow-x:hidden' class=\"$monthTodayCellClass\" id=\"$year-$month-$day\"".($print?"":" onmouseover='this.className=\"monthCellHover\"' onmouseout='this.className=\"$monthTodayCellClass\"'").">";
  	 	} else {
      		$calendar .= "<td style='width:105px;max-width:105px;min-width:103px;overflow-x:hidden' class=\"$monthCellClass\" id=\"$year-$month-$day\"".($print?"":" onmouseover='this.className=\"monthCellHover\"' onmouseout='this.className=\"$monthCellClass\"'").">";
  		}
      	$calendar .= "<div class='".$this->displayHoliday($ts, $year, $month)."'><div>";
      	
//       	debug_pr($print);
      	if($print){
	      	$calendar .= "$day<br style=\"clear: both;\"/>";
      	} else {
      		$calendar .= "<span id=\"new_$year-$month-$day\" style=\"display:none\"><a id=\"create_$year-$month-$day\" href=\"new_event.php?calDate=$year-".sprintf("%02d",$month)."-".sprintf("%02d",$day)."&calViewPeriod=monthly\" class=\"day_add_event\"><img src=\"".$ImagePathAbs."icon_add_s.gif\" width=\"9\" height=\"9\" border=\"0\" align=\"absmiddle\"> $iCalendar_new</a></span><a class=\"day_no\" href=\"javascript:document.form1.time.value='".$ts."';changePeriodTab('daily');\">$day</a><br style=\"clear: both;\"/>";
  		}
		
		
      	$calendar .= "</div></div>";
      	#$calendar .= "<div id=\"calBox".date("Y-m-d", $ts)."\" style=\"height:inherit;\">";
      	if ($print){
      		//$calendar .= $this->printMonthlyDate($ts, true);	# old approach
			$calendar .= $this->Print_Monthly_Date($ts, $conEvent, $associateDay, true);	# new approach on 1 Sept
      	} else {
			//$calendar .= $this->printMonthlyDate($ts);		# old approach
			$calendar .= $this->Print_Monthly_Date($ts, $conEvent, $associateDay, false, $calViewMode);		# new approach on 1 Sept			
      	}
		#$calendar .= "</div>";
     	$calendar .= "</td>";
      	$weekday++;
          
      	if($weekday == 7) { #start a new week
      		if(!$print){
	    	  	$calendar .= "<td class=\"cal_view_week\">";
	    	  	$calendar .= "<a href=\"javascript:document.form1.time.value='".mktime(0,0,0,$month,$day-6,$year)."';changePeriodTab('weekly')\"><img src=\"".$ImagePathAbs."icon_view.gif\" border=\"0\" name=\"view_week_$week\" id=\"view_week_$week\" onMouseOver=\"MM_swapImage('view_week_$week','','".$ImagePathAbs."icon_view_over.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a>";
	    	  	$calendar .= "</td>";
    	  	}
          	$calendar .= "</tr>";
          
          	$calendar .= ($day != $maxdays) ? "<tr ".(($print) ? "" : "height=\"115\"").">" : "";
          	//$calendar .= "<tr>";
          	$weekday = ($day != $maxdays) ? 0 : $weekday;
          	$week++;
      	}
    }
    $hiddenDays = '';
    foreach ($associateDay as $key => $days){
		$uniqueDays = array_unique($days);
// 		debug_pr($uniqueDays);
		$daysStr = implode(',',$uniqueDays);
		$hiddenDays .= '<input type="hidden" id="asso'.$key.'" name="asso'.$key.'" value="'.$daysStr.'" >';
	}
	
      if($weekday != 7) {
        for ($i=0; $i<(7-$weekday); $i++) {
	        $calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
        }
        if(!$print){
        	$calendar .= "<td class=\"cal_view_week\"></td>";
    	}
        $calendar .= "</tr>";
      }
      $calendar .= "</tbody></table>";
      $calendar .= $hiddenDays;
      return $calendar;
  }
    
  function generateToolTipDiv($divID, $eventID, $labelArray, $infoArray, $calViewPeriod='monthly') {
	  	global $UserID;
		if (sizeof($labelArray) != sizeof($infoArray))
			return 0;
		
		$uid = $this->getCurrentUserID();	
			
		# Get RepeatID - for delete button use only
		$repeatID = '';
		$eventDate = '';
		$heading = '';
		$sql = "SELECT EventDate, RepeatID, Title FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
		$result = $this->returnArray($sql);
		if(count($result) > 0){
			$heading = trim(str_replace("'", "\'", $result[0]['Title']));
			$repeatID = $result[0]['RepeatID'];
			$tmp = explode(' ', $result[0]['EventDate']);
			$eventDate = $tmp[0];
		}
		# Get User Access Right - for delete button use only
		$userAccess = $this->returnUserEventAccess($uid, $eventID);
		//$sql = "SELECT Access FROM CALENDAR_EVENT_USER WHERE EventID = $eventID AND UserID = $UserID";
		//$userAccess = $this->returnVector($sql);
			
		# ToolTips UI
		$ToolTipDiv  = "<div id='$divID$eventID' style='display:none;'>";
		$ToolTipDiv .= "<table class='eventToolTipTable' border='0' cellpadding='2' cellspacing='0' width='100%'>";
		for($i=0; $i<sizeof($labelArray); $i++) {
			$Title = str_replace(":", "", $labelArray[$i]);
			$content = trim($infoArray[$i]);
			
			if(strcmp($Title, "Link")==0 && $content != ""){
				$link = (strlen($content) > 22) ? substr($content, 0, 22).' ...' : $content;
				//$link = str_replace("/", "/<wbr>", $content);
				$Value = "<a href=\"".$content."\" target=\"_blank\" class=\"sub_layer_link\" style=\"font-size:12px\">".$link."</a>";
			} else if(strcmp($Title, "Description")==0 && $content != ""){
				$Value = (strlen($content) > 22) ? substr($content, 0, 22).' ...' : $content;
			} else {
				if($Title != "Title"){
					$Value = ($content != "") ? nl2br($infoArray[$i]) : '-';
				} else {
					$Value = nl2br($infoArray[$i]);
				}
			}
			$ToolTipDiv .= "<tr><td class='eventToolTipLabel' valign='top'>".$Title.":</td>";
			$ToolTipDiv .= "<td>".$Value."</td></tr>";
		}
		$ToolTipDiv .= "<tr><td colspan='2' align='right'>";
		# Edit Button
		$ToolTipDiv .= "<a class=\"sub_layer_link\" href=\"javascript:window.location='new_event.php?editEventID=event$eventID&calViewPeriod=$calViewPeriod'\">Edit</a>";
		$ToolTipDiv .= "&nbsp;&nbsp;";
		# Delete Button
		if($userAccess == "A" || $userAccess == "W"){	# user with permission "(A): FULL" or "(W): Read & Edit" would allow to remove the event
			$ToolTipDiv .= "<a id=\"deleteEvent\" name=\"deleteEvent\" class=\"sub_layer_link\" href=\"javascript:jDelete_Event($eventID, '$repeatID', '$eventDate', '$heading');\" >Delete</a>";
			$ToolTipDiv .= "&nbsp;&nbsp;";
		}
		# Close Button
		$ToolTipDiv .= "<a class=\"sub_layer_link\" href=\"javascript:jHide_Event_ToolTips()\">Close</a>";
		$ToolTipDiv .= "</td></tr>";
		$ToolTipDiv .= "</table></div>";
		return $ToolTipDiv;
	}
	
	function generateEventGuestDiv($guestUserID, $guestName, $userAccess, $createdBy, $calAccess="") {
		global $iCalendar_NewEvent_Reminder_Remove;
		$div .= "<div class=\"new_event_sub_form_content\">";
		$div .= "<div>".$guestName;
		$div .= "<input name=\"existGuestList[]\" class=\"existGuestList\" id=\"guest-".$guestUserID."\" type=\"hidden\" value=\"".$guestUserID."\" />";
		if ( (($userAccess == "A" || $userAccess == "W") || ($calAccess == 'A' || $calAccess == "W") )/* && $guestUserID != $createdBy */) {
			$div .= "<input name=\"removeGuestList[]\" class=\"removeGuest\" id=\"removeGuest-".$guestUserID."\" type=\"hidden\" value=\"-1\" />";
			$div .= "&nbsp;&nbsp;<span onclick=\"removeGuest(this,'".$guestUserID."')\"><a class=\"sub_layer_link\" href=\"javascript:void(0)\">$iCalendar_NewEvent_Reminder_Remove</a></span>";
		}
		$div .= "</div>";
		$div .= "</div>";
		return $div;
	}
	
	function generateEventGroupGuestList($id, $groupList, $userAccess, $createdBy) {
		global $i_Calendar_RemoveAll, $iCalendar_EditCalendar_Group, $iCalendar_NewEvent_Reminder_Remove;
		global $guestGroupType, $i_eClass_ClassLink, $iCalendar_ChooseViewer_Course, $iCalendar_ChooseViewer_Group;
		
		$classNameBuffer = array();
		
		$ul = "<ul class='accordion' id='$id'>";
		foreach ($groupList as $groupID => $guests) {
			switch ($guestGroupType[$groupID]) {
				case "G":
					$groupPrefix = "[$i_eClass_ClassLink]";
					$groupName = $this->returnClassName(-1,$groupID);
					break;
				case "C":
					$groupPrefix = "[$iCalendar_ChooseViewer_Course]";
					$groupName = $this->returnCourseName($groupID);
					break;
				case "E":
					$groupPrefix = "[$iCalendar_ChooseViewer_Group]";
					$groupName = $this->returnGroupName($groupID);
					break;
				default:
					$groupPrefix = "";
					$groupName = "";
					break;
			}
			$groupName = '';
			$ul .= "<li><a class='tablelink' href='#'>$groupPrefix $groupName</a><ul>";
			
			if ($userAccess == "A" || $userAccess == "W") {
				$ul .= "<li><span class='tablelink' onclick='removeGroup(this)'>$i_Calendar_RemoveAll</li>";
			}
			
			for($m=0; $m<sizeof($guests); $m++) {
				$ul .= "<li>".$guests[$m]["guestName"];
				$ul .= "<input name='existGuestList[]' class='existGuestList' id='guest-".$guests[$m]["guestUserID"]."' type='hidden' value='".$guests[$m]["guestUserID"]."' />";
				if (($userAccess == "A" || $userAccess == "W") /* && $guests[$m]["guestUserID"] != $createdBy */) {
					$ul .= "<input name='removeGuestList[]' class='removeGuest' id='removeGuest-".$guests[$m]["guestUserID"]."' type='hidden' value='-1' />";
					$ul .= "&nbsp;&nbsp;<span onclick=\"removeGuest(this,".$guests[$m]["guestUserID"].")\"><a class=\"sub_layer_link\" href=\"javascript:void(0)\">$iCalendar_NewEvent_Reminder_Remove</a></span>";
				}
				$ul .= "</li>";
			}
			$ul .= "</ul></li>";
		}
		$ul .= "</ul>";
		return $ul;
	}
	
	function formatEventTitle($title, $isImportant, $isPrint, $limit=25) {
		if (strlen($title) != mb_strlen($title,'utf8'))
			$limit--;
		if (!mb_eregi('[\x00 through \x7F]+',$title)){
			$limit -= 2;
			if ($isImportant)
				$limit--;
			if (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))
				$limit--;
		}
		if ($isPrint) {
			if ($isImportant)
				$title = "<strong>".htmlspecialchars($title)."</strong>";
			else
				$title = htmlspecialchars($title);
		} else {
			if ($isImportant)
				$title = "<strong>".htmlspecialchars(mb_substr($title,0,$limit,'utf8')).((mb_strlen($title,'utf8') > $limit) ? "..." : "")."</strong>";
			else
				$title = htmlspecialchars(mb_substr($title,0,$limit,'utf8')).((mb_strlen($title,'utf8') > $limit) ? "...":"");
		}
		return $title;
	}

	
	
	
	
	
	#new approach with multiple day event display 2009-06
	function Display_Cal_Event_New($ts,  &$conEvent, &$associateDay, $print=false, $isLimitEventNum=true, $stringLength=9, $calViewMode='simple'){
		global $events, $ImagePathAbs, $UserID, $lui;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, 
			   $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
		global $num, $numVisible;
		$str = '';
		
		$uid = $this->getCurrentUserID();
		
		$limit = $this->limitEventDisplay;
		
		$current_date = date("Y-m-d", $ts);
// 		debug_pr($conEvent);
// 		debug_pr($events);
		if(sizeof($events[$current_date]) > 0){
			$evt = $events[$current_date];
// 			debug_pr($evt);
			
			#sort by end date in descending order
			#if end date is the same sort by start time
			for ($i = 0; $i<sizeof($evt)-1; $i++){
				for ($j = $i+1; $j <sizeof($evt); $j++){	
					$iduration = intval($evt[$i]["Duration"]);
					if ($evt[$i]["IsAllDay"] == 1)
						$iduration = $iduration - 1440;
						
					$eventiTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
// 					debug_pr($eventiTs);
					$endiDate = strtotime("+".$iduration." minute",$eventiTs);
					$endiDateYmd = date("Y-m-d",$endiDate);
					
// 					if(intval($evt[$i]["Duration"]>1440)){
// 					    debug_pr($evt[$i]["EventDate"]);
// 					    debug_pr($eventiTs);
// 					    debug_pr($endiDateYmd);
// 					    debug_pr($endiDate);					    
// 					    debug_pr($evt[$i]["IsAllDay"]);
// 					    debug_pr($evt[$i]["Duration"]);
// 					    debug_pr($iduration);
// 					}
					
					$jduration = intval($evt[$j]["Duration"]);
					if ($evt[$j]["IsAllDay"] == 1)
						$jduration = $jduration - 1440;
					
					$eventjTs = $this->sqlDatetimeToTimestamp($evt[$j]["EventDate"]);
					$endjDate = strtotime("+".$jduration." minute",$eventjTs);
					$endjDateYmd = date("Y-m-d",$endjDate);
					
					if($eventjTs < $eventiTs) {
						#sort by start time
						$temp = $evt[$i];
						$evt[$i] = $evt[$j];
						$evt[$j] = $temp;
					}else if($eventjTs == $eventiTs && $endiDate > $endjDate)
					{
						# same start date, sort by end date
						$temp = $evt[$i];
						$evt[$i] = $evt[$j];
						$evt[$j] = $temp;
					}
					/* 
					// Commented on 2013-10-23 by Carlos - sort by start time only
					if ($endiDate < $endjDate && $endjDateYmd != $endiDateYmd){
						#sort by end date
						$temp = $evt[$i];
						$evt[$i] = $evt[$j];
						$evt[$j] = $temp;
					}
					else if ($endjDateYmd == $endiDateYmd && $eventjTs < $eventiTs){
						#sort by start time
						$temp = $evt[$i];
						$evt[$i] = $evt[$j];
						$evt[$j] = $temp;
					}
					*/
				}
			}
			//debug_r($evt);
			
			$offset = 0;
			$eventWeek = date("w",strtotime($current_date));
			$countDisplay = 0;
			
			#check for dummy block and fake block
			if ($eventWeek != 0){
				//$temp = 0;
				if (count($conEvent[$current_date])>0){		
					//echo $current_date." ".count($conEvent[$current_date])."<br>";
					
					for($i = 0; $i < count($conEvent[$current_date]); $i++){
						
						$eventProperty = $conEvent[$current_date][$i];
						
						if (strstr($_SERVER["HTTP_USER_AGENT"],"MSIE") && false){
						#for IE only (not use now)
							if (empty($eventProperty["fake"])){
								#if it is a dummy block
								$str .= "<div id=\"".$eventProperty["id"]."\" class=\"".$eventProperty["DivClass"]."\" style=\"display:".$eventProperty["display"].";margin-top: 0px; font-size:13px; background-color: ".$eventProperty["Color"]."; cursor:pointer\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." onclick=\"".$eventProperty["onclick"]."\" rel=\"#".$eventProperty["TipName"]."\">&nbsp;</div>";
								if ($eventProperty["display"] == "block")
									$numVisible++;
							}
							else{				 		
								#if it is a fake block
								if ($eventProperty["display"] == "block"){
									$isBlock = false;
									$prevDateTs = strtotime("-1 day", strtotime($current_date));
									$prevDayBlock = $conEvent[date("Y-m-d",$prevDateTs)][$i];
									if (!empty($prevDayBlock) && preg_match('/^fake.*/', $prevDayBlock["id"]) &&  $prevDayBlock["display"] == "none"){ 
									#if previous day is not display, this day also not display
										$isBlock =  false;
									}
									else{
										for ($j = $i+1; $j < count($conEvent[$current_date]); $j++){
											#check for any dummy in the bottom
											$next = $conEvent[$current_date][$j];	
											if (preg_match('/^dummy.*/', $next["id"]) && $next["display"] == "block"){
												$isBlock = true;
												break;
											}									
										}	
									}
									#create the fake block
									$conEvent[$current_date][$i]["display"] = ($isBlock?"block":"none");
									$str .= "<div id=\"".$eventProperty["id"]."\" class=\"".$eventProperty["DivClass"]."\" style=\"margin-top: 0px; display:".($isBlock?"block":"none").";font-size:14px;\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"").">&nbsp;</div>";
									if ($isBlock)
										$numVisible++;
								}
								else{								
									$str .= "<div id=\"".$eventProperty["id"]."\" class=\"".$eventProperty["DivClass"]."\" style=\"margin-top: 0px; display: none; font-size:14px;\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"").">&nbsp;</div>";
								}
							}
						}
						else{
						$associateDay[$eventProperty["DivClass"]][]=date("j",$ts);
							#for firefox or other browser
							if (empty($eventProperty["fake"])){
							# dummy block
							$isDisplay = $countDisplay>=5?'none':$eventProperty["display"];
							
							$str .= "<div id=\"".$eventProperty["id"]."\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$eventProperty["DivClass"]."\" style=\"display:".$isDisplay.";font-size:14px;\">&nbsp;</div>";
								if ($eventProperty["display"] == "block"){
									$numVisible++;
									$countDisplay++;
								}
							}
							else{
							#fake block
								if ($eventProperty["display"] == "block"){
									$isBlock = false;
									$prevDateTs = strtotime("-1 day", strtotime($current_date));
									$prevDayBlock = $conEvent[date("Y-m-d",$prevDateTs)][$i];
								
									if (!empty($prevDayBlock) && preg_match('/^fake.*/', $prevDayBlock["id"])&&  $prevDayBlock["display"] == "none"){
									#check for the fake block of previous day
											$isBlock = false;
									}
									else{
										for ($j = $i+1; $j < count($conEvent[$current_date]); $j++){
											$next = $conEvent[$current_date][$j];	
											#check for dummy block in the bottom
											if (preg_match('/^dummy.*/', $next["id"]) && $next["display"] == "block"){
												$isBlock = true;
												break;
											}									 
										}		
									}
									$conEvent[$current_date][$i]["display"] = ($isBlock?"block":"none");
									$isDisplay = $countDisplay>=5?false:$isBlock;
									$str .= "<div id=\"".$eventProperty["id"]."\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$eventProperty["DivClass"]."\" style=\"display:".($isDisplay?"block":"none")."; font-size:14px;\">&nbsp;</div>";	
									if ($isBlock){
										$numVisible++;
										$countDisplay++;
									}
								
								}
								else{
									$str .= "<div id=\"".$eventProperty["id"]."\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$eventProperty["DivClass"]."\" style=\"display: none;  font-size:14px;\">&nbsp;</div>";
								}
							}
						}
						
						$offset++;
					}	
											
				}
				
			}
// 			debug_pr($evt);
			for ($i=0; $i<sizeof($evt) ; $i++){		
				$ColorID 	 = ''; 
				$Color 		 = '';
				$isAllDay    = $evt[$i]["IsAllDay"];
				$CalType 	 = trim($evt[$i]['CalType']);

				$eventDateTimePiece = explode(" ", $evt[$i]["EventDate"]);
				$eventDate = $eventDateTimePiece[0];
				$eventTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
				if ($this->systemSettings["TimeFormat"] == 12) {
					$eventTime = date("g:ia", $eventTs);
				} else if ($this->systemSettings["TimeFormat"] == 24) {
					$eventTime = date("G:i", $eventTs);
				}
				
			
				# Check calendar type of the event
				$isInvolve    = (trim($evt[$i]["Permission"]) == "R") ? true : false;
				//$isFoundation = ($CalType == 2) ? true : false;
				//$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
				
			
				
				### Preparation
			/*	if (!empty($evt[$i]["schoolCode"])){ # other school invitation event
					$otherCalVisible = $this->returnUserPref($UserID, "otherInvitaionCalVisible");
					$display     = ($otherCalVisible == "0") ? "none": "block";
					$numVisible += ($otherCalVisible == "0") ? 0 : 1;
					
					# Get Corresponding Color of each Event
					$ColorID = $this->otherCalColorID;
					$Color	  = trim($this->otherCalColor);
					$DivID	  = "event".$evt[$i]["EventID"].'-'.$evt[$i]["schoolCode"];
					$DivClass = "cal-other";
					$TipName  = "eventToopTip";
				
				}
				elseif($isOtherCalType)			# other caltype event : CPD, CAS
				{		
					$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
					$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
					$otherCalTypeCalVisible = $this->returnUserPref($UserID, $cal_type_name."CalVisible");
					$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
					$numVisible += ($otherCalTypeCalVisible == "0") ? 0 : 1;
					
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = $cal_type_name."Event".$evt[$i]["EventID"];
					$DivClass = $cal_type_name;
					$TipName  = "eventToopTip";	
				} 
				else if($isFoundation)			# foundation event
				{
					//$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
					$foundationCalVisible = trim($evt[$i]["Visible"]);
					$display 	 = ($foundationCalVisible == "0") ? "none" : "block";
					$numVisible += ($foundationCalVisible == "0") ? 0 : 1;
					
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = "foundationEvent".$evt[$i]["EventID"];
					$DivClass = "foundation-".$evt[$i]["CalID"];
					$TipName  = "eventToopTip";	
				}
				else */ 
				if (!empty($evt[$i]["CalType"])&&
				($evt[$i]["CalType"] == 1 || $evt[$i]["CalType"]>3)){
				# school calendar
					$display     = ($evt[$i]["Visible"] == "0") ? "none": "block";
					$numVisible += ($evt[$i]["Visible"] == "0") ? 0 : 1;

					# Get Corresponding Color of each Event
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = $evt[$i]["DivPrefix"].$evt[$i]["EventID"];
					$DivClass = $evt[$i]["DivClass"];
					$TipName  = "eventToopTip";
					$CalType = $evt[$i]["CalType"];
				}else if($isInvolve)			# involved event
				{			
					$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
					$display	 = ($involveCalVisible == "0") ? "none" : "block";
					$numVisible += ($involveCalVisible == "0") ? 0 : 1;

					$ColorID  = $this->involveCalColorID;
					$Color    = $this->involveCalColor;
					$DivID    = "involveEvent".$evt[$i]["EventID"];
					$DivClass = "involve";
					$TipName  = "involveEventToopTip";
				} 
				else 						# personal related events
				{				
					$display     = ($evt[$i]["Visible"] == "0") ? "none": "block";
					$numVisible += ($evt[$i]["Visible"] == "0") ? 0 : 1;

					# Get Corresponding Color of each Event
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = "event".$evt[$i]["EventID"];
					$DivClass = "cal-".$evt[$i]["CalID"];
					$TipName  = "eventToopTip";					
				}

				# Get Event Display Title
				//$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= intranet_undo_htmlspecialchars($evt[$i]["Title"]). $this->AppendOnwerName($evt[$i]["UserID"]);
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$evt[$i]["IsImportant"], true, $stringLength+1);
				} else {
					//if ($calViewMode == 'full') {
						$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$evt[$i]["IsImportant"], true, $stringLength+1);
					//} else {
					//	$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$evt[$i]["IsImportant"], false, $stringLength+1);
					//}
				}
				
			/*	if (!empty($evt[$i]["schoolCode"])){
					#for other invitaion only
					$onclick = "jGet_Event_Info('".$evt[$i]["EventID"]."-".$evt[$i]["schoolCode"]."', 'monthly', '4')";
				}
				else if($isOtherCalType){
					# Set Style for other CalType event only
					$CalEventTitle = $this->Set_Style_Other_CalType_Event($CalEventTitle, $evt[$i]);
					
					if($CalType == ENROL_CPD_CALENDAR || $CalType == MY_CPD_CALENDAR){
						$onclick = "jGet_Other_CalType_Event_Info(".$evt[$i]["EventID"].", '".$evt[$i]["ProgrammeID"]."', 'monthly', '".$CalType."')";
					} 
				} else {*/
					$onclick = "jGet_Event_Info(".$evt[$i]["EventID"].", 'monthly', '".$CalType."')";
			//	}
					$associateDay[$DivClass][]=date("j",$ts);		
							 
				$eventWeek = date("w",strtotime($current_date));
				
				
				$duration = intval($evt[$i]["Duration"]);
				if ($evt[$i]["IsAllDay"] == 1)
					$duration = $duration - 1440;
				
				
				#check for continuous event
				$endDate = date("Y-m-d", strtotime("+".$duration." minute",$eventTs));			
				$startDate = date("Y-m-d", $eventTs);
// 				debug_pr($startDate . " || " . $endDate);
				if ($evt[$i]["Duration"]>1440 || ($endDate != $startDate && $isAllDay == 0)){
				
					
					#create event slot					
					$endDateYMD = date("Y-m-d",strtotime("+".$duration." minute",$eventTs));
					$startDateYMD = $current_date;
					
				
					$padding = Array();
					if ($offset != 0){
						$padding = $conEvent[$startDateYMD];
						
					}  
					elseif ($i != 0){
						$nextDateYMD = date("Y-m-d", strtotime("+1 day",strtotime($startDateYMD)));					
						$padding = $conEvent[$nextDateYMD];						
						
					}
					while ($endDateYMD >= $startDateYMD){	
						
						$temp = Array("display"=>$display,
									"DivClass"=>$DivClass,
									"Color"=>$Color,
									"pos" => $i+$offset,
									"onclick" => $onclick,
									"DivID"=>$DivID,
									"TipName"=>$TipName,
									"eventID"=>$evt[$i]["EventID"],
									"isLimitEventNum" =>$isLimitEventNum,
									"id" => "dummy-".$DivID."-".date("j",strtotime($startDateYMD)));						
						
						$tempWeek = date("w",strtotime($startDateYMD));
						if ($startDateYMD != date("Y-m-d",strtotime($current_date)) && $tempWeek == 0){
							$events[$startDateYMD][] = $evt[$i];
							break;
						}
						else{
							//echo $current_date."<br>";
							if (empty($conEvent[$startDateYMD])){
								for ($j = 0; $j < count($padding); $j++){
									$padding[$j]["fake"] = true;
									$padding[$j]["id"] = "fake-".$padding[$j]["DivID"]."-".date("j",strtotime($startDateYMD));
								}
								$conEvent[$startDateYMD] = $padding;
							}
							if ($offset+$i > count($conEvent[$startDateYMD])){
								$j = count($conEvent[$startDateYMD]);
								while($j != $offset+$i){
									$lastDateYMD = date("Y-m-d",strtotime("-1 day",strtotime($startDateYMD)));
									$fake = $conEvent[$lastDateYMD][$j];
									$fake["fake"] = true;
									$fake["id"] = "fake-".$fake["DivID"]."-".date("j",strtotime($startDateYMD));
									$conEvent[$startDateYMD][] = $fake;
									$j++;
								}
							}
							//$conEvent[$startDateYMD][$i+$offset]= $temp;
							$conEvent[$startDateYMD][]= $temp;
						}
						$startDateYMD = date("Y-m-d",strtotime("+1 day",strtotime($startDateYMD)));
					}
					
					$startDateYMD =$current_date;
					$width = $print?115:106;
					$weekOfDay = date("w",strtotime($startDateYMD));					
					if ($weekOfDay < 6){
						$lastDayOfWeek = strtotime("next saturday", strtotime($startDateYMD));
						$endDateTs = strtotime($endDateYMD);						
						$startYM = date("Y-m",strtotime($startDateYMD));
						
						if ($lastDayOfWeek < $endDateTs){
							$endYM = date("Y-m",$lastDayOfWeek);
							if ($startYM != $endYM){
								$lastDayOfMonth = date("t",strtotime($startDateYMD));
								$nowOfDay = date("d",strtotime($startDateYMD));
								$IEoffset = 0;
								if (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))
									$IEoffset = $lastDayOfMonth - $nowOfDay +1;
								$width = $width*($lastDayOfMonth - $nowOfDay +1)+$IEoffset;
							}
							else{
								$lastDayWeekNum = date("w",$lastDayOfWeek);
								$IEoffset = 0;
								if (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))
									$IEoffset = $lastDayWeekNum - $weekOfDay +1;
								$width = $width*($lastDayWeekNum - $weekOfDay +1)+$IEoffset;
							}
							//echo ($lastDayWeekNum - $weekOfDay +1)."<br>";
						}
						else{
							$endDayWeekNum = date("w",$endDateTs);
							$endYM = date("Y-m",$endDateTs);
							
							if ($startYM != $endYM){
								$lastDayOfMonth = date("t",strtotime($startDateYMD));
								$nowOfDay = date("d",strtotime($startDateYMD));
								$IEoffset = 0;
								if (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))
									$offset = $lastDayOfMonth - $nowOfDay +1;
								$width = $width*($lastDayOfMonth - $nowOfDay +1)+$IEoffset;
							}
							else{
								$IEoffset = 0;
								if (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))
									$IEoffset= $endDayWeekNum - $weekOfDay +1;
								$width = $width*($endDayWeekNum - $weekOfDay +1)+$IEoffset;
							}
							//echo ($endDayWeekNum - $weekOfDay +1)."<br>";
						}
					}
					//echo $width; 
					if ($isLimitEventNum) {
						$isDisplay = $countDisplay>=5?'none':$display;
					} else {
						$isDisplay = $display;
					}
					$countDisplay += ($display=='block'?1:0);
					if ($print)
						$isDisplay = $display;
					
					$str .= "<div class=\"cal_month_event_item\" style=\"margin: 0px; padding:0px\">\n";					
					$str .= "<div style=\"display:$isDisplay;font-size:11px;MARGIN: 0px; padding-top:0px;cursor:pointer;position: absolute; overflow: hidden; height: 17px; width: ".$width."px; background-color: $Color;\" ";
					$str .= "onclick=\"".$onclick."\" rel=\"#".$TipName."\" ";
					$str .= ($isLimitEventNum) ? "name=\"date".date("j",$ts)."event[]\" " : "";
					$str .= "id=\"".$DivID."\" class=\"".$DivClass."\">\n";
					//$str .= "<div style=\"width : ".$width."; overflow: hidden\">";
					if(!$isInvolve || ($isInvolve && $this->involveCalValid) ){	
						$str .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\" style=\"color : white\">\n";
						//$str .= "<div style=\"width: ".($width-30)."px; overflow: hidden;>";
						$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" 	src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/>&nbsp;\n";
					} else {
						$str .= "<a class=\"cal_event_\" href=\"javascript:void(0);\" style=\"color : white; float: none; clear: both\">\n"; 
					}
					$str .= $CalEventTitle;//."</div>";
					if (trim($evt[$i]["PersonalNote"]) != ""){
						$str .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					}
					$str .= "</a>\n";
					//$str .= "</div>\n";
					$str .= "</div>\n";
					$str .= "<div id=\"padding-".$evt[$i]["EventID"]."-".date("j",$ts)."\" class=\"".$DivClass."\" style=\"margin:0px; padding-top: 0px; background-color: transparent; font-size:14px;display:$isDisplay;\" >&nbsp;</div>";
					$str .= "</div>\n";
					$num++;
					
					continue;
					
					
				}
				
				if ($isLimitEventNum) {
					$isDisplay = $countDisplay>=5?'none':$display;
				} else {
					$isDisplay = $display;
				}
				
				$countDisplay += ($display=='block'?1:0);
				
				# Content 
				if($print){
					if($display == 'block') // only output visible events html for print view
					{
						$str .= "<div class=\"cal_month_event_item\" style=\"margin: 0px; padding:0px\">\n";
						$str .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;\" ";
						$str .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
						$str .= "id=\"".$DivID."\" class=\"".$DivClass."\">\n";
						if(!$isInvolve || ($isInvolve && $this->involveCalValid) ){
							$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/>&nbsp;\n";
						}
						$str .= "<font color=\"#".$Color."\">".$CalEventTitle;
						if (trim($evt[$i]["PersonalNote"]) != "") 
							$str .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
						$str .= "</font>\n";
						$str .= "</div>\n";
						$str .= "</div>\n";
					}
				} else { 
					$str .= "<div class=\"cal_month_event_item\" style=\"margin: 0px; padding:0px\">\n";
					$str .= "<div style=\"display:$isDisplay;font-size:11px;margin: 0px;padding:0px;cursor:pointer;\" ";
					$str .= "onclick=\"".$onclick."\" rel=\"#".$TipName."\" ";
					$str .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
					$str .= "id=\"".$DivID."\" class=\"".$DivClass."\">\n";
					if(!$isInvolve || ($isInvolve && $this->involveCalValid) ){	
						$str .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\">\n";
						$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" 	src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/>&nbsp;\n";
					} else {
						$str .= "<a class=\"cal_event_\" href=\"javascript:void(0);\">\n";
					}
					$str .= $CalEventTitle;
					if (trim($evt[$i]["PersonalNote"]) != ""){
						$str .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					}
					$str .= "</a>\n";
					$str .= "</div>\n";
					$str .= "</div>\n";
				}
				$num++; 
			} 
			
		}
		#only for IE check for dummy box (not use now)
		elseif (strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")&&false){
			$numOfEvent = sizeof($events[$current_date]);
			//echo $current_date." ".$numOfEvent." ".count($conEvent[$current_date])."<br>";
			if (count($conEvent[$current_date]) > 0){
				$offset = $numOfEvent;
				foreach($conEvent[$current_date] as $dummy){
					//$pos = $dummy["pos"];					
					if (!empty($dummy["fake"])){						
						$str .= "<div id=\"".$eventProperty["id"]."\" ".($dummy["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")."  class=\"".$dummy["DivClass"]."\" style=\"display:".$dummy["display"].";font-size:14px; margin-top: 0px\">&nbsp;</div>";
					}
					else{
						$str .= "<div id=\"".$eventProperty["id"]."\" ".($dummy["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$dummy["DivClass"]."\" style=\"display:".$dummy["display"].";margin-top: 0px;font-size:14px; background-color: ".$dummy["Color"]."; cursor:pointer\" onclick=\"".$dummy["onclick"]."\" rel=\"#".$dummy["TipName"]."\">&nbsp;</div>";
						$offset = $pos+1;
					}
				}
			}
		}
		
		
		return $str;
	}
	
	
	
	
	
	
	# New approach of displaying Calendar Events added on 1 Sept
	# This would handle User owned & shared events And users involved events at the same time
	function Display_Cal_Event($ts, $print=false, $isLimitEventNum=true, $stringLength=9){
		global $events, $ImagePathAbs, $UserID, $lui;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, 
			   $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
		global $num, $numVisible;
		$str = '';

		$uid = $this->getCurrentUserID();

		$limit = $this->limitEventDisplay;
		$current_date = date("Y-m-d", $ts);

		if(sizeof($events[$current_date]) > 0){
			$evt = $events[$current_date];
			for ($i=0; $i<sizeof($evt) ; $i++){
				$ColorID 	 = ''; 
				$Color 		 = '';
				$isAllDay    = $evt[$i]["IsAllDay"];
				$CalType 	 = trim($evt[$i]['CalType']);

				$eventDateTimePiece = explode(" ", $evt[$i]["EventDate"]);
				$eventDate = $eventDateTimePiece[0];
				$eventTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
				if ($this->systemSettings["TimeFormat"] == 12) {
					$eventTime = date("g:ia", $eventTs);
				} else if ($this->systemSettings["TimeFormat"] == 24) {
					$eventTime = date("G:i", $eventTs);
				}
			
				# Check calendar type of the event
				$isInvolve    = (trim($evt[$i]["Permission"]) == "R") ? true : false;
				$isFoundation = ($CalType == 2) ? true : false;
				$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
				
				### Preparation
				if($isOtherCalType)			# other caltype event : CPD, CAS
				{		
					$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
					$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
					$otherCalTypeCalVisible = $this->returnUserPref($uid, $cal_type_name."CalVisible");
					$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
					$numVisible += ($otherCalTypeCalVisible == "0") ? 0 : 1;
					
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = $cal_type_name."Event".$evt[$i]["EventID"];
					$DivClass = $cal_type_name;
					$TipName  = "eventToopTip";	
				} 
				else if($isFoundation)			# foundation event
				{
					//$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
					$foundationCalVisible = trim($evt[$i]["Visible"]);
					$display 	 = ($foundationCalVisible == "0") ? "none" : "block";
					$numVisible += ($foundationCalVisible == "0") ? 0 : 1;
					
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = "foundationEvent".$evt[$i]["EventID"];
					$DivClass = "foundation-".$evt[$i]["CalID"];
					$TipName  = "eventToopTip";	
				}
				else if($isInvolve)			# involved event
				{			
					$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
					$display	 = ($involveCalVisible == "0") ? "none" : "block";
					$numVisible += ($involveCalVisible == "0") ? 0 : 1;

					$ColorID  = $this->involveCalColorID;
					$Color    = $this->involveCalColor;
					$DivID    = "involveEvent".$evt[$i]["EventID"];
					$DivClass = "involve";
					$TipName  = "involveEventToopTip";
				} 
				else 						# personal related events
				{				
					$display     = ($evt[$i]["Visible"] == "0") ? "none": "block";
					$numVisible += ($evt[$i]["Visible"] == "0") ? 0 : 1;

					# Get Corresponding Color of each Event
					$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
					$Color	  = trim($evt[$i]["Color"]);
					$DivID	  = "event".$evt[$i]["EventID"];
					$DivClass = "cal-".$evt[$i]["CalID"];
					$TipName  = "eventToopTip";					
				}

				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= $evt[$i]["Title"]. $this->AppendOnwerName($evt[$i]["UserID"]);
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$evt[$i]["IsImportant"], true, $stringLength);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$evt[$i]["IsImportant"], false, $stringLength);
				}
				
				if($isOtherCalType){
					# Set Style for other CalType event only
					$CalEventTitle = $this->Set_Style_Other_CalType_Event($CalEventTitle, $evt[$i]);
					
					if($CalType == ENROL_CPD_CALENDAR || $CalType == MY_CPD_CALENDAR){
						$onclick = "jGet_Other_CalType_Event_Info(".$evt[$i]["EventID"].", '".$evt[$i]["ProgrammeID"]."', 'monthly', '".$CalType."')";
					} 
				} else {
					$onclick = "jGet_Event_Info(".$evt[$i]["EventID"].", 'monthly', '".$CalType."')";
				}

				# Content 
				if($print){
					$str .= "<div class=\"cal_month_event_item\">\n";
					$str .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;\" ";
					$str .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
					$str .= "id=\"".$DivID."\" class=\"".$DivClass."\">\n";
					if(!$isInvolve || ($isInvolve && $this->involveCalValid) ){
						$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/>&nbsp;\n";
					}
					$str .= "<font color=\"#".$Color."\">".$CalEventTitle;
					if (trim(UTF8_From_DB_Handler($evt[$i]["PersonalNote"])) != "") 
						$str .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					$str .= "</font>\n";
					$str .= "</div>\n";
					$str .= "</div>\n";
				} else { 
					$str .= "<div class=\"cal_month_event_item\">\n";
					$str .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;cursor:pointer;\" ";
					$str .= "onclick=\"".$onclick."\" rel=\"#".$TipName."\" ";
					$str .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
					$str .= "id=\"".$DivID."\" class=\"".$DivClass."\">\n";
					if(!$isInvolve || ($isInvolve && $this->involveCalValid) ){	
						$str .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\">\n";
						$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" 	src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/>&nbsp;\n";
					} else {
						$str .= "<a class=\"cal_event_\" href=\"javascript:void(0);\">\n";
					}
					$str .= $CalEventTitle;
					if (trim(UTF8_From_DB_Handler($evt[$i]["PersonalNote"])) != ""){
						$str .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					}
					$str .= "</a>\n";
					$str .= "</div>\n";
					$str .= "</div>\n";
				}
				$num++;
			}
		}
		return $str;
	}
  
	# Old approach of displaying calendar events 
	# Problems: time consuming 
	function displayCalEvent($viewableCal, $ts, $print=false, $isLimitEventNum=true, $stringLength=13) {
		global $events, $ImagePathAbs;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;;
		global $num, $numVisible;
		$CalEventDiv = "";
		
		$limit = $this->limitEventDisplay;
				
		for ($i=0; $i<sizeof($events); $i++) {
			if($this->resetTimestampDayStart($this->sqlDatetimeToTimestamp($events[$i]["EventDate"]))==$ts && 
			    $events[$i]["CalID"]==$viewableCal["CalID"]) {
				    
			 //if($num < $limit || ($num >= $limit && !$isLimitEventNum) ){
				$isAllDay = $events[$i]["IsAllDay"];
				$viewableCal["Visible"]=="0"?$display="none":$display="block";
				$numVisible += ($viewableCal["Visible"]=="0") ? 0 : 1;
				
				$eventDateTimePiece = explode(" ", $events[$i]["EventDate"]);
		  		$eventDate = $eventDateTimePiece[0];
				$eventTs = $this->sqlDatetimeToTimestamp($events[$i]["EventDate"]);
			  	$endTs = (int)$eventTs + (int)$events[$i]["Duration"]*60;
			  	if ($this->systemSettings["TimeFormat"] == 12) {
			    	$eventTime = date("g:ia", $eventTs);
			    	$endTime = date("g:ia", $endTs);
			    } else if ($this->systemSettings["TimeFormat"] == 24) {
			    	$eventTime = date("G:i", $eventTs);
			    	$endTime = date("G:i", $endTs);
			    }
		    
			    # Get Corresponding Color of each Event
				$ColorID = $this->Get_Calendar_ColorID($viewableCal["Color"]);
		    	
	    		# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= $events[$i]["Title"];
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$events[$i]["IsImportant"], true, $stringLength);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$events[$i]["IsImportant"], false, $stringLength);
				}
				
			 	if ($print) {
					//$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin:5px 1px 2px 1px;padding:2px;border:1px solid #".$viewableCal["Color"].";\" ";
					$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;\" ";
					$CalEventDiv .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
					$CalEventDiv .= "id=\"event".$events[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
					$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$CalEventDiv .= "<font color=\"#".$viewableCal["Color"]."\">".$CalEventTitle."</font>";
					$CalEventDiv .= "</div>";
					$CalEventDiv .= "</div>";
				} else {
					/*
					unset($labelArray); 
					unset($infoArray);
					$labelArray = array($iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate);
					$infoArray[] = htmlspecialchars($events[$i]["Title"]);
					$infoArray[] = $events[$i]["Description"]==""?"-":htmlspecialchars($events[$i]["Description"]);
					$infoArray[] = $eventDate;
					if ($isAllDay == "0") {
						$labelArray[] = $iCalendar_NewEvent_EventTime;
						$infoArray[] = $eventTime.($eventTime==$endTime?"":" - $endTime");
					}
					$labelArray[] = $iCalendar_NewEvent_Link;
					$infoArray[] = ($events[$i]["Url"] == "") ? "" : $events[$i]["Url"];
					$divID = "eventToolTip";
					$ToolTipDiv = $this->generateToolTipDiv($divID, $events[$i]["EventID"], $labelArray, $infoArray);
					*/

					# Content 
					$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;cursor:pointer;\" ";
						//$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin:5px 1px 2px 1px;padding:2px;color:#FFF;background-color:#".$viewableCal["Color"].";cursor:pointer;\" ";
						//$CalEventDiv .= "onclick=\"window.location='new_event.php?editEventID='+this.id+'&calViewPeriod=monthly';\" ";
					# old tooltips look
						//$CalEventDiv .= "rel=\"#eventToolTip".$events[$i]["EventID"]."\" ";
					# new tooltips look
					$CalEventDiv .= "onclick=\"jGet_Event_Info(".$events[$i]["EventID"].", 'monthly')\" ";
					$CalEventDiv .= "rel=\"#eventToolTip\" ";
					
					$CalEventDiv .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
					$CalEventDiv .= "id=\"event".$events[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
					$CalEventDiv .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\">";
					$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$CalEventDiv .= $CalEventTitle;
					$CalEventDiv .= "</a>";
					$CalEventDiv .= "</div>";
					//$CalEventDiv .= $ToolTipDiv;
					$CalEventDiv .= "</div>";
					
				}
		  	  //}
			  $num++;
			}
		}
		
		return $CalEventDiv;
	}
	
	function displayInvolveEvent($ts, $print=false, $isLimitEventNum=true, $stringLength=13) {
		global $ImagePathAbs;
		global $UserID, $events, $ImagePathAbs;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
		global $num, $numVisible;
		
		$uid = $this->getCurrentUserID();
		
		$limit = $this->limitEventDisplay;
		
		$CalEventDiv = "";
		$sql = "SELECT EventID FROM CALENDAR_EVENT_USER WHERE UserID = '$UserID' AND Access IN ('R') AND Status IN ('A', 'R', 'M')";
		$involveEvent = $this->returnArray($sql);
				
		$involveEventSql = array();
		for ($i=0; $i<sizeof($involveEvent); $i++)
			$involveEventSql[$i] = $involveEvent[$i][0];
		
		$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
		$involveCalVisible=="1"?$display="block":$display="none";
		
		for($i=0; $i<sizeof($events); $i++) {
			if($this->resetTimestampDayStart($this->sqlDatetimeToTimestamp($events[$i]["EventDate"]))==$ts && in_array($events[$i]["EventID"], $involveEventSql)) {
				
			  //if($num < $limit || ($num >= $limit && !$isLimitEventNum) ){
				$isAllDay = $events[$i]["IsAllDay"];

				$numVisible += ($involveCalVisible == 0) ? 0 : 1;
				
				$eventDateTimePiece = explode(" ", $events[$i]["EventDate"]);
			    $eventDate = $eventDateTimePiece[0];
				$eventTs = $this->sqlDatetimeToTimestamp($events[$i]["EventDate"]);
			    $endTs = (int)$eventTs + (int)$events[$i]["Duration"]*60;
			    if ($this->systemSettings["TimeFormat"] == 12) {
			    	$eventTime = date("g:ia", $eventTs);
			    	$endTime = date("g:ia", $endTs);
			    } else if ($this->systemSettings["TimeFormat"] == 24) {
			    	$eventTime = date("G:i", $eventTs);
			    	$endTime = date("G:i", $endTs);
			    }
			    	    	
		    	# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= $events[$i]["Title"];
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$events[$i]["IsImportant"], true, $stringLength);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$events[$i]["IsImportant"], false, $stringLength);
				}
				
				# Content	
			    //$eventDivCss = "display:$display;font-size:11px;margin:2px 1px 2px 1px;padding:2px";
			    $eventDivCss = "display:$display;font-size:11px;margin:0px 0px 0px 1px;padding:0px";
			    if ($print) {
				    $CalEventDiv .= "<div class=\"cal_month_event_item\">";
				    $CalEventDiv .= "<div style=\"$eventDivCss\" id=\"involveEvent".$events[$i]["EventID"]."\" class=\"involve\">";
				    if($this->involveCalValid){
						$CalEventDiv .= "<img class=\"cal_event_png_".$this->involveCalColorID."\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_".$this->involveCalColorID.".png\"/> ";
					}
					$CalEventDiv .= "<font color=\"#".$this->involveCalColor."\">".$CalEventTitle."</font>";
					$CalEventDiv .= "</div>";
					$CalEventDiv .= "</div>";
			    } else {
				    unset($labelArray);
					unset($infoArray);
					$labelArray = array($iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate);
					$infoArray[] = htmlspecialchars($events[$i]["Title"]);
					$infoArray[] = $events[$i]["Description"]==""?"-":htmlspecialchars($events[$i]["Description"]);
					$infoArray[] = $eventDate;
					if ($isAllDay == "0") {
						$labelArray[] = $iCalendar_NewEvent_EventTime;
						$infoArray[] = $eventTime.($eventTime==$endTime?"":" - $endTime");
					}
					$labelArray[] = $iCalendar_NewEvent_Link;
					$infoArray[] = ($events[$i]["Url"] == "") ? "" : $events[$i]["Url"];
					$divID = "involveEventToolTip";
					$ToolTipDiv = $this->generateToolTipDiv($divID, $events[$i]["EventID"], $labelArray, $infoArray);
				    
					# Content
					$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;border:0px \" ";
					//$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;border:1px dotted #000000\" ";
					//$CalEventDiv .= "onclick=\"window.location='new_event.php?editEventID=event".$events[$i]["EventID"]."&calViewPeriod=monthly';\" ";
					$CalEventDiv .= "rel=\"#involveEventToolTip".$events[$i]["EventID"]."\" ";
					$CalEventDiv .= "id=\"involveEvent".$events[$i]["EventID"]."\" class=\"involve\">";
					if($this->involveCalValid){
						$CalEventDiv .= "<a class=\"cal_event_".$this->involveCalColorID."\" href=\"javascript:void(0)\">";
						$CalEventDiv .= "<img class=\"cal_event_png_".$this->involveCalColorID."\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_".$this->involveCalColorID.".png\"/> ";
					} else {
						$CalEventDiv .= "<a class=\"cal_event_\" href=\"javascript:void(0)\">";
					}
					$CalEventDiv .= $CalEventTitle;
					$CalEventDiv .= "</a>";
					$CalEventDiv .= "</div>";
					$CalEventDiv .= $ToolTipDiv;
					$CalEventDiv .= "</div>";
				}
		  	  //}
			  $num++;
			}
		}
		return $CalEventDiv;
	}
    
	## Old approach using query_event() function 
  function displayWeeklyCalEvent($viewableCal, $eventList, $allDayOnly=FALSE, $print=false) {
	global $ImagePathAbs, $lui, $UserID;
    global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
    
    $uid = $this->getCurrentUserID();
    
    for ($i=0; $i<sizeof($eventList); $i++) {
	    $eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
	    $eventDate = $eventDateTimePiece[0];
	    $eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
	    //$endTs = (int)$eventTs + (int)$eventList[$i]["Duration"]*60;
	    // 12 OR 24 time format
	    if ($this->systemSettings["TimeFormat"] == 12) {
	    	//$eventTime = date("g:ia", $eventTs);	// g: hour , i:minute , a:am/pm
	    	//$endTime = date("g:ia", $endTs);
	    	$eventTime = date("h:i", $eventTs);
	    	//$endTime = date("h:i", $endTs);
	    } else if ($this->systemSettings["TimeFormat"] == 24) {
	    	$eventTime = date("G:i", $eventTs);
	    	//$endTime = date("G:i", $endTs);
	    }
	    $isAllDay = $eventList[$i]["IsAllDay"];
	    //if ($allDayOnly && $isAllDay=="0")	continue;
	    if (!$allDayOnly && $isAllDay=="1")	continue;
	    	
	   	# Get Corresponding Color of each Event
		$ColorID = $this->Get_Calendar_ColorID($viewableCal["Color"]);
	    
	    if($eventList[$i]["CalID"] == $viewableCal["CalID"]) {		    
		    # Get Event Display Title
			$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
			$CalEventTitle .= $eventList[$i]["Title"]. $this->AppendOnwerName($eventList[$i]["UserID"]);
			if ($print) {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 10);
			} else {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 10);
			}
		    
			# Preparation
		    $viewableCal["Visible"]=="0"?$display="none":$display="block";
		    $eventDivCss = "display:$display;font-size:11px;margin-top:0px;margin-bottom:0px;padding:0px";
		    //$eventDivCss = "display:$display;font-size:11px;margin-top:1px;margin-bottom:5px;padding:2px";
		    $eventLinkCss = ($isAllDay == 0) ? "cal_event_$ColorID" : "cal_calendar_$ColorID";
		    
		    # Content
		    if ($print) {
			    $CalEventDiv .= "<div class=\"cal_month_event_item\">";
			    $CalEventDiv .= ($isAllDay == 0) ? "" : "<ul><li>";
			    $CalEventDiv .= "<div style=\"$eventDivCss\" id=\"event".$eventList[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
			    //$CalEventDiv .= "<div style=\"$eventDivCss;border:1px solid #".$viewableCal["Color"]."\" id=\"event".$eventList[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
					$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$CalEventDiv .= "<font color=\"#".$viewableCal["Color"]."\">".$CalEventTitle;
					if (trim(UTF8_From_DB_Handler($eventList[$i]["PersonalNote"])) != "") 
						$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					$CalEventDiv .= "</font>";
					$CalEventDiv .= "</div>";
					$CalEventDiv .= ($isAllDay == 0) ? "" : "</li></ul>";
					$CalEventDiv .= "</div>";
		    } else {
				/*
			    unset($labelArray);
				unset($infoArray);
				$labelArray = array($iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate);
				$infoArray[] = htmlspecialchars($eventList[$i]["Title"]);
				$infoArray[] = $eventList[$i]["Description"]==""?"-":htmlspecialchars($eventList[$i]["Description"]);
				$infoArray[] = $eventDate;
				if ($isAllDay == "0") {
					$labelArray[] = $iCalendar_NewEvent_EventTime;
					$infoArray[] = $eventTime.($eventTime==$endTime?"":" - $endTime");
				}
				$labelArray[] = $iCalendar_NewEvent_Link;
				$infoArray[] = $eventList[$i]["Url"];
				$divID = "eventToolTip";
				$ToolTipDiv = $this->generateToolTipDiv($divID, $eventList[$i]["EventID"], $labelArray, $infoArray, "weekly");
				*/

				$CalEventDiv .= "<div class=\"cal_month_event_item\">";
				$CalEventDiv .= ($isAllDay == 0) ? "" : "<ul><li>";
				$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;color:#FFF\" ";
					//$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;color:#FFF;background-color:#".$viewableCal["Color"]."\" ";
					//$CalEventDiv .= "onclick=\"window.location='new_event.php?editEventID='+this.id+'&calViewPeriod=weekly';\" ";
				# old tooltip look
					//$CalEventDiv .= "rel=\"#eventToolTip".$eventList[$i]["EventID"]."\" ";
				# new tooltip look
				$CalEventDiv .= "onclick=\"jGet_Event_Info(".$eventList[$i]["EventID"].", 'weekly')\" ";
				$CalEventDiv .= "rel=\"#eventToolTip\"";

				$CalEventDiv .= "id=\"event".$eventList[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
				#if ($eventList[$i]["RepeatID"] != "")
					#$CalEventDiv .= "<sup>Repeat</sup> ";
				$CalEventDiv .= "0000 <a class=\"$eventLinkCss\" href=\"javascript:void(0)\">";
				$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
				$CalEventDiv .= $CalEventTitle;
				if (trim(UTF8_From_DB_Handler($eventList[$i]["PersonalNote"])) != "") 
						$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</a>";
				$CalEventDiv .= "</div>";
				$CalEventDiv .= ($isAllDay == 0) ? "" : "</li></ul>";
				//$CalEventDiv .= $ToolTipDiv;
				$CalEventDiv .= "</div>";	
			}
		}
    }
    return $CalEventDiv;
  }
    
  function displayWeeklyInvolveEvent($eventList, $allDayOnly=false, $print=false) {
	global $ImagePathAbs, $lui;
  	global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
   	global $UserID;
   	
   	$uid = $this->getCurrentUserID();
   	
    $involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
	$involveCalVisible=="1"?$display="block":$display="none";
		
    for ($i=0; $i<sizeof($eventList); $i++) {
	    if ($eventList[$i]["Permission"] == "R" || $eventList[$i]["Permission"] == "W") {
		    $eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
		    $eventDate = $eventDateTimePiece[0];
		    $eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
		    //$endTs = (int)$eventTs + (int)$eventList[$i]["Duration"]*60;
		    if ($this->systemSettings["TimeFormat"] == 12) {
	    		$eventTime = date("g:ia", $eventTs);
		    	//$endTime = date("g:ia", $endTs);
		    } else if ($this->systemSettings["TimeFormat"] == 24) {
		    	$eventTime = date("G:i", $eventTs);
		    	//$endTime = date("G:i", $endTs);
		    }
		    $isAllDay = $eventList[$i]["IsAllDay"];
		    //if ($allDayOnly && $isAllDay=="0")	continue;
		    if (!$allDayOnly && $isAllDay=="1")	continue;
		    			  
		    # Preparation
		    $eventDivCss = "display:$display;font-size:11px;margin-top:0px;margin-bottom:0px;padding:1px";
		    //$eventDivCss = "display:$display;font-size:11px;margin-top:1px;margin-bottom:10px;padding:2px";
		    $eventLinkCss = ($isAllDay == 0) ? "cal_event_".$this->involveCalColorID : "cal_calendar_".$this->involveCalColorID;

		    # Get Event Display Title
			$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
			$CalEventTitle .= $eventList[$i]["Title"]. $this->AppendOnwerName($eventList[$i]["UserID"]);
			if ($print) {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 10);
			} else {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 10);
			}
		    
		    # Content
		    if ($print) {
			    $CalEventDiv .= "<div class=\"cal_month_event_item\">";
			    $CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;\" ";
			    //$CalEventDiv .= "<div style=\"$eventDivCss;border:0px solid #000000\" ";
				$CalEventDiv .= "id=\"event".$eventList[$i]["EventID"]."\" class=\"involve\">";
				if($this->involveCalValid){
					$CalEventDiv .= "<a class=\"cal_event_".$this->involveCalColorID."\" href=\"javascript:void(0)\">";
					$CalEventDiv .= "<img class=\"cal_event_png_".$this->involveCalColorID."\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_".$this->involveCalColorID.".png\"/> ";
				}
				$CalEventDiv .= "<font color=\"#".$this->involveCalColor."\">".$CalEventTitle;
				if (trim($eventList[$i]["PersonalNote"]) != "") 
					$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
						//$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</font>";
				$CalEventDiv .= "</div>";
				$CalEventDiv .= "</div>";
		    } else {
				/*
			    unset($labelArray);
				unset($infoArray);
				$labelArray = array($iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate);
				$infoArray[] = htmlspecialchars($eventList[$i]["Title"]);
				$infoArray[] = $eventList[$i]["Description"]==""?"-":htmlspecialchars($eventList[$i]["Description"]);
				$infoArray[] = $eventDate;
				if ($isAllDay == "0") {
					$labelArray[] = $iCalendar_NewEvent_EventTime;
					$infoArray[] = $eventTime.($eventTime==$endTime?"":" - $endTime");
				}
				$labelArray[] = $iCalendar_NewEvent_Link;
				$infoArray[] = $eventList[$i]["Url"];
				$divID = "involveEventToolTip";
				$ToolTipDiv = $this->generateToolTipDiv($divID, $eventList[$i]["EventID"], $labelArray, $infoArray, "weekly");
				*/

				# Content 
				$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					//$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;border:1px dotted #000000\" ";
					//$CalEventDiv .= "onclick=\"window.location='new_event.php?editEventID='+this.id+'&calViewPeriod=weekly';\" ";
				$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;\" ";
				# old tooltip look
					//$CalEventDiv .= "rel=\"#involveEventToolTip".$eventList[$i]["EventID"]."\" ";
				# new tooltip look
				$CalEventDiv .= "onclick=\"jGet_Event_Info(".$eventList[$i]["EventID"].", 'weekly')\" ";
				$CalEventDiv .= "rel=\"#involveEventToolTip\"";

				$CalEventDiv .= "id=\"involveEvent".$eventList[$i]["EventID"]."\" class=\"involve\">";
				#if ($eventList[$i]["RepeatID"] != "")
					#$CalEventDiv .= "<sup>Repeat</sup> ";
				if($this->involveCalValid){
					$CalEventDiv .= "<a class=\"$eventLinkCss\" href=\"javascript:void(0)\">";
					$CalEventDiv .= "<img class=\"cal_event_png_".$this->involveCalColorID."\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_".$this->involveCalColorID.".png\"/> ";
				} else {
					$CalEventDiv .= "<a class=\"cal_event_\" href=\"javascript:void(0)\">";
				}
				$CalEventDiv .= $CalEventTitle;
				if (trim($eventList[$i]["PersonalNote"]) != "") 
					$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
						//$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</a>";
				$CalEventDiv .= "</div>";
				//$CalEventDiv .= $ToolTipDiv;
				$CalEventDiv .= "</div>";
			}
		} else {
			continue;
		}
	}
	return $CalEventDiv;
  }
    
  function displayDailyCalEvent($viewableCal, $eventList, $allDayOnly=false, $print=false) {
	global $ImagePathAbs,$lui;
    global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
    
    for ($i=0; $i<sizeof($eventList); $i++) {
	    $eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
	    $eventDate = $eventDateTimePiece[0];
	    $eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
	    $endTs = (int)$eventTs + (int)$eventList[$i]["Duration"]*60;
	    if ($this->systemSettings["TimeFormat"] == 12) {
	    	//$eventTime = date("g:ia", $eventTs);
	    	//$endTime = date("g:ia", $endTs);
	    	$eventTime = date("h:i", $eventTs);
	    	$endTime = date("h:i", $endTs);
	    } else if ($this->systemSettings["TimeFormat"] == 24) {
	    	$eventTime = date("G:i", $eventTs);
	    	$endTime = date("G:i", $endTs);
	    }
	    $isAllDay = $eventList[$i]["IsAllDay"];
	    //if ($allDayOnly && $isAllDay=="0")	continue;
	    if (!$allDayOnly && $isAllDay=="1")	continue;
	    
	    # Get Corresponding Color of each Event
		$ColorID = $this->Get_Calendar_ColorID($viewableCal["Color"]);

    	# Content	    	
	    if($eventList[$i]["CalID"]==$viewableCal["CalID"]) {
		    $viewableCal["Visible"]=="0"?$display="none":$display="block";
		    $eventLinkCss = ($isAllDay == 0) ? "cal_event_$ColorID" : "cal_calendar_$ColorID";
		    
		    # Get Event Display Title
			$CalEventTitle  = ($isAllDay == 0) ? $eventTime." - ".$endTime." " : "";
			$CalEventTitle .= $eventList[$i]["Title"]. $this->AppendOnwerName($eventList[$i]["UserID"]);
			if ($print) {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 225);
			} else {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 225);
			}
		    
		    if ($print) {
			    $CalEventDiv .= "<div class=\"cal_month_event_item\">";
			    $CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin-bottom:1px;padding:0px;margin-top:0px\" ";
			    //$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin-bottom:10px;padding:2px;margin-top:1px;border:1px solid #".$viewableCal["Color"]."\" ";
				$CalEventDiv .= "id=\"event".$eventList[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
				$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
				$CalEventDiv .= "<font color=\"".$viewableCal["Color"]."\">".$CalEventTitle;
				if (trim($eventList[$i]["PersonalNote"]) != "") 
					$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					//$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</font>";
				$CalEventDiv .= "</div>";
				$CalEventDiv .= "</div>";
		    } else {
				/*
			    unset($labelArray);
				unset($infoArray);
				$labelArray = array($iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate);
				$infoArray[] = htmlspecialchars($eventList[$i]["Title"]);
				$infoArray[] = $eventList[$i]["Description"]==""?"-":htmlspecialchars($eventList[$i]["Description"]);
				$infoArray[] = $eventDate;
				if ($isAllDay == "0") {
					$labelArray[] = $iCalendar_NewEvent_EventTime;
					$infoArray[] = $eventTime.($eventTime==$endTime?"":" - $endTime");
				}
				$labelArray[] = $iCalendar_NewEvent_Link;
				$infoArray[] = $eventList[$i]["Url"];
				$divID = "eventToolTip";
				$ToolTipDiv = $this->generateToolTipDiv($divID, $eventList[$i]["EventID"], $labelArray, $infoArray, "daily");
			    */

				$CalEventDiv .= "<div class=\"cal_month_event_item\">";
				$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin-bottom:1px;padding:0px;cursor:pointer;margin-top:0px;color:#FFF\" ";
					//$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin-bottom:10px;padding:2px;cursor:pointer;margin-top:1px;color:#FFF; background-color:#".$viewableCal["Color"]."\" ";
					//$CalEventDiv .= "onclick=\"window.location='new_event.php?editEventID='+this.id+'&calViewPeriod=daily';\" ";
				# old tooltip look
					//$CalEventDiv .= "rel=\"#eventToolTip".$eventList[$i]["EventID"]."\" ";
				# new tooltip look
				$CalEventDiv .= "onclick=\"jGet_Event_Info(".$eventList[$i]["EventID"].", 'daily')\" ";
				$CalEventDiv .= "rel=\"#eventToolTip\"";

				$CalEventDiv .= "id=\"event".$eventList[$i]["EventID"]."\" class=\"cal-".$viewableCal["CalID"]."\">";
				$CalEventDiv .= "<a class=\"$eventLinkCss\" href=\"javascript:void(0);\">";
				$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
				$CalEventDiv .= $CalEventTitle;
				if (trim($eventList[$i]["PersonalNote"]) != "") 
					$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					//$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</a>";
				$CalEventDiv .= "</div>";
				//$CalEventDiv .= $ToolTipDiv;
				$CalEventDiv .= "</div>";
			}
		}
    }
    return $CalEventDiv;
  }
    
  function displayDailyInvolveEvent($eventList, $allDayOnly=false, $print=false) {
	global $ImagePathAbs;
    global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
    global $UserID;
    
    $uid = $this->getCurrentUserID();
    
    $involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
	$involveCalVisible=="1"?$display="block":$display="none";
    for ($i=0; $i<sizeof($eventList); $i++) {
	    if ($eventList[$i]["Permission"] == "R" || $eventList[$i]["Permission"] == "W") {
		    $eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
		    $eventDate = $eventDateTimePiece[0];
		    $eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
		    $endTs = (int)$eventTs + (int)$eventList[$i]["Duration"]*60;
		    if ($this->systemSettings["TimeFormat"] == 12) {
		    	$eventTime = date("g:ia", $eventTs);
		    	$endTime = date("g:ia", $endTs);
		    } else if ($this->systemSettings["TimeFormat"] == 24) {
		    	$eventTime = date("G:i", $eventTs);
		    	$endTime = date("G:i", $endTs);
		    }
		    $isAllDay = $eventList[$i]["IsAllDay"];
		    //if ($allDayOnly && $isAllDay=="0")	continue;
		    if (!$allDayOnly && $isAllDay=="1")	continue;
		    
		    # Get Event Display Title
			$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
			$CalEventTitle .= $eventList[$i]["Title"]. $this->AppendOnwerName($eventList[$i]["UserID"]);
			if ($print) {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 225);
			} else {
				$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 225);
			}
		    
			# Preparation
			$eventDivCss = "display:$display;font-size:11px;margin-top:0px;margin-bottom:0px;padding:1px;";
		    //$eventDivCss = "display:$display;font-size:11px;margin-top:1px;margin-bottom:10px;padding:2px;";
		    
		    if ($print) {
			    $CalEventDiv .= "<div class=\"cal_month_event_item\">";
				//$CalEventDiv .= "<div style=\"$eventDivCss;border:1px solid #000000\" ";
				$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;\" ";
				$CalEventDiv .= "id=\"event".$eventList[$i]["EventID"]."\" class=\"involve\">";
				if($this->involveCalValid){
					$CalEventDiv .= "<img class=\"cal_event_png_".$this->involveCalColorID."\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_".$this->involveCalColorID.".png\"/> ";
				}
				$CalEventDiv .= "<font color=\"".$this->involveCalColor."\">".$CalEventTitle;
				if (trim($eventList[$i]["PersonalNote"]) != "") 
					$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					//$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</font>";
				$CalEventDiv .= "</a>";
				$CalEventDiv .= "</div>";
				$CalEventDiv .= "</div>";
		    } else {
				/*
			    unset($labelArray);
				unset($infoArray);
				$labelArray = array($iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate);
				$infoArray[] = htmlspecialchars($eventList[$i]["Title"]);
				$infoArray[] = $eventList[$i]["Description"]==""?"-":htmlspecialchars($eventList[$i]["Description"]);
				$infoArray[] = $eventDate;
				if ($isAllDay == "0") {
					$labelArray[] = $iCalendar_NewEvent_EventTime;
					$infoArray[] = $eventTime.($eventTime==$endTime?"":" - $endTime");
				}
				$labelArray[] = $iCalendar_NewEvent_Link;
				$infoArray[] = $eventList[$i]["Url"];
				$divID = "involveEventToolTip";
				$ToolTipDiv = $this->generateToolTipDiv($divID, $eventList[$i]["EventID"], $labelArray, $infoArray, "daily");
				*/

				$CalEventDiv .= "<div class=\"cal_month_event_item\">";
				$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;border:0px\" ";
					//$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;border:1px dotted #000000\" ";
					//$CalEventDiv .= "onclick=\"window.location='new_event.php?editEventID='+this.id+'&calViewPeriod=daily';\" ";
				# old tooltip look
					//$CalEventDiv .= "rel=\"#involveEventToolTip".$eventList[$i]["EventID"]."\" ";
				# new tooltip look
				$CalEventDiv .= "onclick=\"jGet_Event_Info(".$eventList[$i]["EventID"].", 'daily')\" ";
				$CalEventDiv .= "rel=\"#involveEventToolTip\"";

				$CalEventDiv .= "id=\"involveEvent".$eventList[$i]["EventID"]."\" class=\"involve\">";
				if($this->involveCalValid){
					$CalEventDiv .= "<a class=\"cal_event_".$this->involveCalColorID."\" href=\"javascript:void(0);\">";
					$CalEventDiv .= "<img class=\"cal_event_png_".$this->involveCalColorID."\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_".$this->involveCalColorID.".png\"/> ";
				} else {
					$CalEventDiv .= "<a class=\"cal_event_\" href=\"javascript:void(0);\">";
				}
				$CalEventDiv .= $CalEventTitle;
				if (trim($eventList[$i]["PersonalNote"]) != "") 
					$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					//$CalEventDiv .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
				$CalEventDiv .= "</a>";
				$CalEventDiv .= "</div>";
				//$CalEventDiv .= $ToolTipDiv;
				$CalEventDiv .= "</div>";
			}
		} else {
			continue;
		}
    }
    return $CalEventDiv;
  }
    	 
	## Old apporach with using query_event()
	function printWeeklyEventBlock($eventList, $allDayOnly=false, $print=false) {
		$viewableCal = $this->returnViewableCalendar();
		
	    for ($i=0; $i<sizeof($viewableCal); $i++) {
	    	if ($allDayOnly) {
		    	if ($print)
		    		$calEventDiv = $this->displayWeeklyCalEvent($viewableCal[$i], $eventList, true, true);
		    	else
	    			$calEventDiv = $this->displayWeeklyCalEvent($viewableCal[$i], $eventList, true);
    		} else {
	    		if ($print)
					$calEventDiv = $this->displayWeeklyCalEvent($viewableCal[$i], $eventList, false, true);
				else
					$calEventDiv = $this->displayWeeklyCalEvent($viewableCal[$i], $eventList);
			}
	        if ($calEventDiv != "") {
	        	$date .= $calEventDiv;
			}
		}
		if ($allDayOnly) {
			if ($print)
				$calEventDiv = $this->displayWeeklyInvolveEvent($eventList, true, true);
			else
				$calEventDiv = $this->displayWeeklyInvolveEvent($eventList, true);
		} else {
			if ($print)
				$calEventDiv = $this->displayWeeklyInvolveEvent($eventList, false, true);
			else
				$calEventDiv = $this->displayWeeklyInvolveEvent($eventList);
		}
		if ($calEventDiv != "") {
	        $date .= $calEventDiv;
		}
		return $date."&nbsp";
	}
	
	function printDailyEventBlock($eventList, $allDayOnly=false, $print=false) {
		$viewableCal = $this->returnViewableCalendar();
		
	    for ($i=0; $i<sizeof($viewableCal); $i++) {
	    	if ($allDayOnly) {
		    	if ($print)
		    		$calEventDiv = $this->displayDailyCalEvent($viewableCal[$i], $eventList, true, true);
		    	else
	    			$calEventDiv = $this->displayDailyCalEvent($viewableCal[$i], $eventList, true);
			} else {
				if ($print)
					$calEventDiv = $this->displayDailyCalEvent($viewableCal[$i], $eventList, false, true);
				else
					$calEventDiv = $this->displayDailyCalEvent($viewableCal[$i], $eventList);
			}
	        if ($calEventDiv != "") {
	        	$date .= $calEventDiv;
			}
		}
		if ($allDayOnly) {
			if ($print)
				$calEventDiv = $this->displayDailyInvolveEvent($eventList, true, true);
			else
				$calEventDiv = $this->displayDailyInvolveEvent($eventList, true);
		} else {
			if ($print)
				$calEventDiv = $this->displayDailyInvolveEvent($eventList, false, true);
			else
				$calEventDiv = $this->displayDailyInvolveEvent($eventList);
		}
		if ($calEventDiv != "") {
	        $date .= $calEventDiv;
		}
		return $date."&nbsp";
	}
	
	function generateDailyCalendar($year, $month, $day, $print=false) {
	    global $iCalendar_NewEvent_IsAllDay;
	    global $events;
	    
	    $dayString = "$year-$month-$day";
	    $dayInfo = date("D", mktime(0, 0, 0, $month, $day, $year))." ($day/$month)";
	    for ($i=0; $i<24; $i++) {
		    $start_time = (($i<10)?"0$i":"$i").":00";
	      	$end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	      
	      	if (!isset($eventHappenGrid[$i])) {
	      		$eventHappenGrid[$i] = array();
	    	}
	    	if (!isset($eventHappenDetailGrid[$i])) {
	      		$eventHappenDetailGrid[$i] = array();
	    	}
	      	if (!isset($eventRowSpan[$i])) {
	      		$eventRowSpan[$i] = 0;
	    	}
		    $start_ts = $this->sqlDatetimeToTimestamp($dayString." $start_time");
		    $end_ts = $this->sqlDatetimeToTimestamp($dayString." $end_time");
		    # Handling the timeslot from 23:00 to 00:00(next day) since $this->sqlDatetimeToTimestamp("24:00") return false;
		    if($i+1==24) $end_ts = $start_ts + 60*60;
	      
	      	for ($k=0; $k<sizeof($events); $k++) {
		        $curEventTs = $this->sqlDatetimeToTimestamp($events[$k]["EventDate"]);
		        if ($curEventTs >= $start_ts && $curEventTs < $end_ts) {
			        # Count the time difference if the event does not start at start_Ts time exactly like 12:30 --> not 12:00
			        $extraTs = $curEventTs - $start_ts;
			        $extraMin = ($extraTs <= 60*60) ? $extraTs/60 : 0;
			        $numOfHours = ceil(($events[$k]["Duration"] + $extraMin) / 60 );
			        
			        if ($numOfHours <= 1) {
			        	$eventHappenGrid[$i][] = $events[$k]["EventID"];
			        	$eventHappenDetailGrid[$i][] = $events[$k];
			        	$rowSpan[$i] = ($rowSpan[$i] < $numOfHours) ? 0 : $rowSpan[$i];
			        	if (!isset($eventHappenGrid[$i+1])) {
			        		$eventHappenGrid[$i+1] = array();
			        		$eventHappenDetailGrid[$i+1] = array();
		        		}
		        	} else {
			        	$rowSpan[$i] = ($rowSpan[$i] < $numOfHours) ? $numOfHours : $rowSpan[$i];
				        for ($l=0; $l<$numOfHours; $l++) {
				        	$eventHappenGrid[$i+$l][] = $events[$k]["EventID"];
				        	$eventHappenDetailGrid[$i+$l][] = $events[$k];
			        	}
		        	}
		        } else {
			        if (!isset($eventHappenGrid[$i+1])) {
			        	 $eventHappenGrid[$i+1] = array();
		        	}
		        	if (!isset($eventHappenDetailGrid[$i+1])) {
			        	 $eventHappenDetailGrid[$i+1] = array();
		        	}
		        }
		    }
	    }
    
	    $calendar .= "<table cellspacing='0' cellspacing='0' id='daily'>";
		//$calendar .= "<tbody>";
		$calendar .= "<thead>";
		
		if(!$print){
			$calendar .= "<tr>";
			$calendar .= "<td class=\"cal_none\">&nbsp;</td>";
			$calendar .= "<td class=\"cal_week_week_title\">$dayInfo</td>";
			$calendar .= "</tr>";
		}
		# Top cells for all day event
		$calendar .= "<tr>";
		$calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$iCalendar_NewEvent_IsAllDay</td>";
		//$calendar .= "<td class='timeRow2 timeText' scope='row'>$iCalendar_NewEvent_IsAllDay</td>";
		
		if (date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString){
			$calendar .= "<td class='".($print?"dayFirstCellPrint'":"dayFirstCellToday' onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"dayFirstCellToday\"'").">";
			//$calendar .= "<td class='dayFirstCellPrint' ".($print?"":"onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"dayFirstCellToday\"'").">";
		} else {
			$calendar .= "<td id='$dayString' class='".($print?"dayFirstCellPrint'":"dayFirstCell' onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"dayFirstCell\"'").">";
			//$calendar .= "<td id='$dayString' class='dayFirstCellPrint' ".($print?"":"onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"dayFirstCell\"'").">";
		}
	
		if (sizeof($eventHappenDetailGrid[0]) > 0) {
			if ($print)
				$calendar .= $this->printDailyEventBlock($eventHappenDetailGrid[0], true, true);
			else
				$calendar .= $this->printDailyEventBlock($eventHappenDetailGrid[0], true);
			# remove all day events from the array
			$sizeOfEventHappenDetailGrid = sizeof($eventHappenDetailGrid[0]);
			for($j=0; $j<$sizeOfEventHappenDetailGrid ; $j++) {
				if (trim($eventHappenDetailGrid[0][$j]["IsAllDay"]) == "1"){
					unset($eventHappenDetailGrid[0][$j]);
				}
			}
			sort($eventHappenDetailGrid[0]);
		} else {
			$calendar .= "&nbsp";
		}
		$calendar .= "</td>";
		$calendar .= "</tr>";
			
		$calendar .= "</thead>";
		$calendar .= "<tbody>";
		//$calendar .= "<div style='height:320px;scrolling:yes;overflow:auto'>";
	  	for ($i=$this->workingHourStart; $i<$this->workingHourEnd; $i++) {
	        $calendar .= "<tr>";
	        $start_time = (($i<10)?"0$i":"$i").":00";
	        $end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	        $calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$start_time</td>";
	        //$calendar .= "<td class='timeRow2 timeText' scope='row'>$start_time</td>";
	        
	        if ($i==$this->workingHourStart)
	        	$curLastIntersect = array();
	        else
	        	$curLastIntersect = array_intersect($eventHappenGrid[$i], $eventHappenGrid[$i-1]);
	        
	        if ($i==$this->workingHourEnd)
	        	$curNextIntersect = array();
	        else {
		        if ($eventHappenGrid[$i+1])
	        		$curNextIntersect = array_intersect($eventHappenGrid[$i], $eventHappenGrid[$i+1]);
	      	}

	        # No event in this cell OR the event only need one cell
	        if (empty($curLastIntersect) && empty($curNextIntersect)) {
		        if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString) && $i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCellToday";
		        	//$cellClassName = "dayLastCellToday";
	        	} else if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString)){
		        	$cellClassName = $print?"dayCellPrint":"dayCellToday";
		        	//$cellClassName = "dayCellToday";
	        	} else if ($i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCell";
		        	//$cellClassName = "dayLastCell";
	        	} else {
		        	$cellClassName = $print?"dayCellPrint":"dayCell";
		        	//$cellClassName = "dayCell";
	        	}
		        $calendar .= "<td id='$dayString-$i' class='$cellClassName' ".($print?"":"onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
		        if ($print)
		        	$calendar .= $this->printDailyEventBlock($eventHappenDetailGrid[$i], false, true);
		        else
		        	$calendar .= $this->printDailyEventBlock($eventHappenDetailGrid[$i]);
		        $calendar .= "&nbsp;</td>";
	        }
	        # Row span is needed for event with duration more than 1 hour
	        else if (empty($curLastIntersect) && !empty($curNextIntersect)) {
		        //$rowSpan[$i] = $this->Get_Rowspan_Event_Number($i, $rowSpan[$i]);
		        # Handle the case of intersection among several events shown in UI
		        # Condition: 
		        #	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
		        #	Check The other Events duration beginning within the hour in the above range
		        # 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
		        # 	Because of Event 2 is set within the range of Event 1,
		        # 	the rowspan should be updated to 3 from 2.
		        if($rowSpan[$i] > 1){
		        	$orig_span_range = $i + $rowSpan[$i];
		        	for($m=$i+1 ; $m<$orig_span_range ; $m++){
			        	$other_span_range = $m + $rowSpan[$m];
			        	if($other_span_range > $orig_span_range){
			        		$new_span = $rowSpan[$i] + ($other_span_range - $orig_span_range);
			        		$rowSpan[$i] = $new_span;
		        		}
	        		}
	        	}
		        	
		        if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString) && $i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCellToday";
		        	//$cellClassName = "dayLastCellToday";
	        	} else if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString)){
		        	$cellClassName = $print?"dayCellPrint":"dayCellToday";
		        	//$cellClassName = "dayCellToday";
	         	} else if ($i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCell";
		        	//$cellClassName = "dayLastCell";
	        	} else {
		        	$cellClassName = $print?"dayCellPrint":"dayCell";
		        	//$cellClassName = "dayCell";
	        	}
		        $calendar .= "<td id='$dayString-$i' class='$cellClassName' rowspan='".max(1,$rowSpan[$i])."' ".($print?"":"onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
		        for ($k=1; $k<$rowSpan[$i]; $k++) {
			        $eventHappenGrid[$i] = $this->array_union($eventHappenGrid[$i], $eventHappenGrid[$i+$k]);
			        $eventHappenDetailGrid[$i] = array_merge($eventHappenDetailGrid[$i], $eventHappenDetailGrid[$i+$k]);
		        }
		        $tempArray = array();
		        $exist = array();
		        for ($l=0; $l<sizeof($eventHappenDetailGrid[$i]); $l++) {
			        if (in_array($eventHappenDetailGrid[$i][$l]["EventID"], $eventHappenGrid[$i]) && !in_array($eventHappenDetailGrid[$i][$l]["EventID"], $exist)) {
			        	$tempArray[] = $eventHappenDetailGrid[$i][$l];
			        	$exist[] = $eventHappenDetailGrid[$i][$l]["EventID"];
		        	}
		        }
		        $eventHappenDetailGrid[$i] = $tempArray;
		        if ($print)
		        	$calendar .= $this->printDailyEventBlock($eventHappenDetailGrid[$i], false, true);
		        else
		        	$calendar .= $this->printDailyEventBlock($eventHappenDetailGrid[$i]);
		        $calendar .= "&nbsp;</td>";
		        
	        }
	        $calendar .= "</tr>";
	    }
	    $calendar .= "</tbody>";
	    $calendar .= "</table>";
	    return $calendar;
    }
    
    function generateWeeklyCalendar($year, $month, $day, $print=false) {
	    global $iCalendar_NewEvent_IsAllDay;
	    global $events;
	    
	    $current_week_date = $this->get_week($year, $month, $day, "Y-m-d");
	    for ($i=0; $i<24; $i++) {
		    $start_time = (($i<10)?"0$i":"$i").":00";
        	$end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	        for ($j=0; $j<7; $j++) {
		        if (!isset($eventHappenGrid[$i][$j])) {
		        	$eventHappenGrid[$i][$j] = array();
	        	}
	        	if (!isset($eventHappenDetailGrid[$i][$j])) {
		        	$eventHappenDetailGrid[$i][$j] = array();
	        	}
		        if (!isset($eventRowSpan[$i][$j])) {
		        	$eventRowSpan[$i][$j] = 0;
	        	}
		        $start_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $start_time");
		        $end_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $end_time");
		        # Handling the timeslot from 23:00 to 00:00(next day) since $this->sqlDatetimeToTimestamp("24:00") return false;
	     		if($i+1==24) $end_ts = $start_ts + 60*60;
	     		 
		        for ($k=0; $k<sizeof($events); $k++) {
			        $curEventTs = $this->sqlDatetimeToTimestamp($events[$k]["EventDate"]);
			        if ($curEventTs >= $start_ts && $curEventTs < $end_ts) {
				        $numOfHours = ceil($events[$k]["Duration"]/60);
				        if (date("i",$curEventTs) != "00")
				        	$numOfHours++;
				        if ($numOfHours <= 1) {
				        	$eventHappenGrid[$i][$j][] = $events[$k]["EventID"];
				        	$eventHappenDetailGrid[$i][$j][] = $events[$k];
				        	### Bug Fix on 2008-01-17 ###
				        	# When multiple events start in the same cell, do not reset the
				        	# row span to 0 if there already exist a value
				        	if (!isset($rowSpan[$i][$j]) || empty($rowSpan[$i][$j]))
				        		$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? 0 : $rowSpan[$i][$j];
				        	if (!isset($eventHappenGrid[$i+1][$j])) {
				        		$eventHappenGrid[$i+1][$j] = array();
				        		$eventHappenDetailGrid[$i+1][$j] = array();
			        		}
			        	} else {
				        	$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? $numOfHours : $rowSpan[$i][$j];
					        for ($l=0; $l<$numOfHours; $l++) {
					        	$eventHappenGrid[$i+$l][$j][] = $events[$k]["EventID"];
					        	$eventHappenDetailGrid[$i+$l][$j][] = $events[$k];
				        	}
			        	}
			        } else {
				        if (!isset($eventHappenGrid[$i+1][$j])) {
				       		$eventHappenGrid[$i+1][$j] = array();
			        	}
			        	if (!isset($eventHappenDetailGrid[$i+1][$j])) {
				        	$eventHappenDetailGrid[$i+1][$j] = array();
			        	}
			        }
		        }
	       	}
       	}
	    
      	$current_week = $this->get_week($year, $month, $day, "y/m/d (D)");
    	
    	$calendar .= "<table cellspacing='0' cellspacing='0' id='weekly'>";	    
     	$calendar .= "<colgroup>
	      				<col />
	      				<col class='Sun' />
						<col class='Mon' />
						<col class='Tue' />
						<col class='Wed' />
						<col class='Thu' />
						<col class='Fri' />
						<col class='Sat' />
					  </colgroup>";
		$calendar .= "<thead><tr>";
		$calendar .= "<td class='top_empty'>&nbsp;</td>";
		
		for($i=0; $i<7; $i++) {
			# apply different class for TODAY
			if (date("y/m/d (D)", time()) == $current_week[$i]){
				$calendar .= "<th class='".($print?"weekTitlePrint":"weekTitleToday weekTitleTextToday")."'>$current_week[$i]</th>";
				//$calendar .= "<th class='weekTitleToday weekTitleTextToday'>$current_week[$i]</th>";
		 	} else {
				$calendar .= "<th class='".($print?"weekTitlePrint":"weekTitle weekTitleText")."'>$current_week[$i]</th>";
				//$calendar .= "<th class='weekTitle weekTitleText'>$current_week[$i]</th>";
			}
		}
		
		$calendar .= "</tr></thead>";
		$calendar .= "<tbody>";
		
		# Top cells for all day event
		$calendar .= "<tr>";
		$calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$iCalendar_NewEvent_IsAllDay</td>";
		//$calendar .= "<td class='timeRow2 timeText' scope='row'>$iCalendar_NewEvent_IsAllDay</td>";
		for ($i=0; $i<7; $i++) {
			if (date("y/m/d (D)", time()) == $current_week[$i]){
				$calendar .= "<td id='$current_week_date[$i]' class='".($print?"weekCellPrint":"weekFirstCellToday")."'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"weekFirstCellToday\"'").">";
				//$calendar .= "<td id='$current_week_date[$i]' class='weekFirstCellToday'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"weekFirstCellToday\"'").">";
			} else {
				$calendar .= "<td id='$current_week_date[$i]' class='".($print?"weekCellPrint":"weekFirstCell")."'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"weekFirstCell\"'").">";
				//$calendar .= "<td id='$current_week_date[$i]' class='weekFirstCell'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"weekFirstCell\"'").">";
			}
			
			if (sizeof($eventHappenDetailGrid[0][$i]) > 0) {
				if ($print)
					$calendar .= $this->printWeeklyEventBlock($eventHappenDetailGrid[0][$i], true, true);
				else
					$calendar .= $this->printWeeklyEventBlock($eventHappenDetailGrid[0][$i], true);
				# remove all day events from the array
				$sizeOfEventHappenDetailGrid = sizeof($eventHappenDetailGrid[0][$i]);
				for($j=0; $j<$sizeOfEventHappenDetailGrid ; $j++) {
					if (trim($eventHappenDetailGrid[0][$i][$j]["IsAllDay"]) == "1"){
						unset($eventHappenDetailGrid[0][$i][$j]);
					}
				}
				sort($eventHappenDetailGrid[0][$i]);
			} else {
				$calendar .= "&nbsp";
			}
			$calendar .= "</td>";
		}
		$calendar .= "</tr>";
		
      	for ($i=$this->workingHourStart; $i<$this->workingHourEnd; $i++) {
	        $calendar .= "<tr>";
	        $start_time = sprintf("%02d",$i).":00";
	        $end_time = sprintf("%02d",$i+1).":00";
	        // 12 OR 24 time format
	        if ($this->systemSettings["TimeFormat"] == "12") {
	        	if ($i == 0) {
	        		$display_time = "12:00 am";
	        	} else if ($i == 12) {
	        		$display_time = "12:00 pm";
	        	} else if ($i < 12) {
	        		$display_time = sprintf("%02d",$i).":00 am";
	        	} else {
	        		$display_time = sprintf("%02d",$i-12).":00 pm";
	        	}
	        } else if ($this->systemSettings["TimeFormat"] == "24") {
	        	$display_time = $start_time;
	        }
	        $calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$display_time</td>";
	        //$calendar .= "<td class='timeRow2 timeText' scope='row'>$display_time</td>";
	        for ($j=0; $j<7; $j++) {
		        if ($i==$this->workingHourStart)
		        	$curLastIntersect = array();
		        else
		        	$curLastIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i-1][$j]);
		        
		        if ($i==$this->workingHourEnd)
		        	$curNextIntersect = array();
		        else {
			        if ($eventHappenGrid[$i+1][$j])
		        		$curNextIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i+1][$j]);
	        	}
		        
		        # No event in this cell OR the event only need one cell
		        if (empty($curLastIntersect) && empty($curNextIntersect)) {
			        if (date("y/m/d (D)", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCellToday";
			        	//$cellClassName = "weekLastCellToday";
		        	} else if (date("y/m/d (D)", time()) == $current_week[$j]){
			        	$cellClassName = $print?"weekCellPrint":"weekCellToday";
			        	//$cellClassName = "weekCellToday";
			        } else if ($i == $this->workingHourEnd-1) {
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCell";
			        	//$cellClassName = "weekLastCell";
		        	} else {
			        	$cellClassName = $print?"weekCellPrint":"weekCell";
			        	//$cellClassName = "weekCell";
		        	}
			        $calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
			        if ($print)
			        	$calendar .= $this->printWeeklyEventBlock($eventHappenDetailGrid[$i][$j], false, true);
			        else
			        	$calendar .= $this->printWeeklyEventBlock($eventHappenDetailGrid[$i][$j]);
			        $calendar .= "&nbsp;</td>";
		        }
		        # Row span is needed for event with duration more than 1 hour
		        else if (empty($curLastIntersect) && !empty($curNextIntersect)) {
			        # Handle the case of intersection among several events shown in UI
			        # Condition: 
			        #	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
			        #	Check The other Events duration beginning within the hour in the above range
			        # 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
			        # 	Because of Event 2 is set within the range of Event 1,
			        # 	the rowspan should be updated to 3 from 2.
			        if($rowSpan[$i][$j] > 1){
			        	$orig_span_range = $i + $rowSpan[$i][$j];
			        	for($m=$i+1 ; $m<$orig_span_range ; $m++){
				        	$other_span_range = $m + $rowSpan[$m][$j];
				        	if($other_span_range > $orig_span_range){
				        		$new_span = $rowSpan[$i][$j] + ($other_span_range - $orig_span_range);
				        		$rowSpan[$i][$j] = $new_span;
			        		}
		        		}
		        	}
			        
			        if (date("y/m/d (D)", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCellToday";
			        	//$cellClassName = "weekLastCellToday";
		        	} else if (date("y/m/d (D)", time()) == $current_week[$j]){
			        	$cellClassName = $print?"weekCellPrint":"weekCellToday";
			        	//$cellClassName = "weekCellToday";
		        	} else if ($i == $this->workingHourEnd-1){
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCell";
			        	//$cellClassName = "weekLastCell";
		          	} else {
			        	$cellClassName = $print?"weekCellPrint":"weekCell";
			        	//$cellClassName = "weekCell";
		        	}
			        //echo $i.", ".$j.", ".$rowSpan[$i][$j]."<br />";
			        $calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName' rowspan='".max(1,$rowSpan[$i][$j])."'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
			        for ($k=1; $k<$rowSpan[$i][$j]; $k++) {
				        $eventHappenGrid[$i][$j] = $this->array_union($eventHappenGrid[$i][$j], $eventHappenGrid[$i+$k][$j]);
				        $eventHappenDetailGrid[$i][$j] = array_merge($eventHappenDetailGrid[$i][$j], $eventHappenDetailGrid[$i+$k][$j]);
			        }
			        $tempArray = array();
			        $exist = array();
			        for ($l=0; $l<sizeof($eventHappenDetailGrid[$i][$j]); $l++) {
				        if (in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $eventHappenGrid[$i][$j]) && !in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $exist)) {
				        	$tempArray[] = $eventHappenDetailGrid[$i][$j][$l];
				        	$exist[] = $eventHappenDetailGrid[$i][$j][$l]["EventID"];
			        	}
			        }
			        $eventHappenDetailGrid[$i][$j] = $tempArray;
			        if ($print)
			        	$calendar .= $this->printWeeklyEventBlock($eventHappenDetailGrid[$i][$j], false, true);
			        else
			        	$calendar .= $this->printWeeklyEventBlock($eventHappenDetailGrid[$i][$j]);
			        $calendar .= "&nbsp;</td>";
		        }
	        }
        	$calendar .= "</tr>";
      	}
      	$calendar .= "</tbody></table>";
      	return $calendar;
   	}

	# Testing Only on 16 Sept
	function Print_Agenda($startDate, $endDate, $print=false, $response, $agendaPeriod=1) {
	   	global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_EditEvent_CreatedBy;
	  	global $iCalendar_SchoolEvent_Color, $iCalendar_NewEvent_IsAllDay, $iCalendar_Agenda_NoEvent;
	  	global $iCalendar_NewEvent_Location, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_Personal_Note;
	  	global $i_Calendar_Today, $i_Calendar_NextSevenDays, $i_Calendar_ThisWeek, $i_Calendar_Edit, $iCalendar_Agenda_NoEventsForToday ;
	  	global $events, $schoolEvents, $ImagePathAbs, $UserID, $lui, $iCalendar_NewEvent_Repeats_WeekdayArray2, $i_Calendar_modifiedDate, $iCalendar_NewEvent_Guests, $sys_custom, $Lang;
	  	
		// echo $startDate.':'.$endDate;
		
		$uid = $this->getCurrentUserID();
		
	  	# selection box
	  	$KeyValues = array(
				0 => array($i_Calendar_Today, "0"), 
				1 => array($i_Calendar_NextSevenDays, "1"), 
				2 => array($i_Calendar_ThisWeek, "2"));
		$selectAgendaPeriod  = '';
		if(!$print){
			$selectAgendaPeriod .= '<select name="selectAgendaPeriod" id="selectAgendaPeriod" onchange="changeAgendaPeriod(this)" style="display:none">';
			for($i=0 ; $i<count($KeyValues) ; $i++){
				$selected = ($KeyValues[$i][1] == $agendaPeriod) ? "selected" : ""; 
				$selectAgendaPeriod .= '<option value="'.$KeyValues[$i][1].'" '.$selected.'>'.$KeyValues[$i][0].'</option>';
			}
			$selectAgendaPeriod .= "</select>";
		} 
		
		# UI
		$x  = '';
		$x .= "<div class='cal_agenda_content'>";
		$x .= "<div style='padding:3px;'>$selectAgendaPeriod</div>";
		
		
		
		$viewableCal = $this->returnViewableCalendar();
		for ($i=0; $i<sizeof($viewableCal); $i++) {
			$viewableCalColor[$viewableCal[$i]["CalID"]] = $viewableCal[$i]["Color"];
			$viewableCalVisible[$viewableCal[$i]["CalID"]] = (int)$viewableCal[$i]["Visible"];
		}
		
		$response = array("A","M","W","");
		
		$dayTableExist = array();
		//global $UserID;

		# may need to rebuild events as there can be some events across days
		if(sizeOf($events) > 0)
		{
			$cnt = 0;
			foreach($events as $evt_date => $evt)
			{
				for($i=0 ; $i<count($evt) ; $i++)
				{
					$day_last_for = ($evt[$i]['Duration']/24)/60;
					//debug($evt[$i]['Title'], " for ".$day_last_for. "days");
					
					if (!$event_added_to_date[$evt_date][$evt[$i]['EventID']])
						$event_rebuild[$evt_date][] = $evt[$i];
					
					if (!$is_forcasted[$evt[$i]['EventID']])
					{
						if ($day_last_for>1)
						{
							
							for ($j=2; $j<=$day_last_for; $j++)
							{
								$evt_date_another = date("Y-m-d", strtotime($evt[$i]['EventDate']) + (($j-1) * 60 * 60 * 24));
								if (strtotime($evt_date_another)!=strtotime($evt_date))
								{
									//debug($evt_date_another, $evt[$i]['Title']);
									//$evt[$i]['EventDate'] = date("Y-m-d H:i:s", strtotime($evt[$i]['EventDate']) + (($j-1) * 60 * 60 * 24)); // change cross day event's date to the real date
									$event_rebuild[$evt_date_another][] = $evt[$i];
									$event_added_to_date[$evt_date_another][$evt[$i]['EventID']] = true;
								}
							}
						}
						$is_forcasted[$evt[$i]['EventID']] = true;
					}
				}
			}
		}
		$events = $event_rebuild;
		//debug_pr($events);						
		if(sizeOf($events) > 0) {
			$cnt = 0;
			foreach($events as $evt_date => $evt){
				if(count($evt) > 0){
					usort($evt,array($this,'sortEventCompareMethod')); // need to sort same date events second time due to cross day events are addoc generated
					$startDateTs = strtotime($startDate);
					$endDateTs = strtotime($endDate);
					$eventTs = $this->sqlDatetimeToTimestamp($evt_date);
					$eventEndTs = $eventTs+$evt['Duration']*60;
					 
					if ($startDateTs>$eventEndTs || $endDateTs < $eventTs)
						continue;
					
					$dayTableClass = ($evt_date == date("Y-m-d")) ? "cal_agenda_entry_date_log cal_agenda_content_entry_today" :  "cal_agenda_entry_date_log";

					$x .= '<div id="agenda-" style="display:block">';
					$x .= '<table cellspacing="0" cellpadding="0" border="0" class="'.$dayTableClass.'">';
					$x .= '<tr>';
					$x .= '<td class="cal_agenda_entry_date">';
					$weekday = '<br />'.$iCalendar_NewEvent_Repeats_WeekdayArray2[date("w", $eventTs)];
					if(!$print){
						$x .= '<a class="cal_agenda_entry_event" href="index.php?time='.$eventTs.'&calViewPeriod=daily">'.date("j M", $eventTs).$weekday.'</a>';
					} else {
						//$x .= date("j M", $eventStartTs);
						$x .= date("j M", $eventTs).$weekday;
					}
					$x .= '</td>';
					$x .= '<td>';
					

					# Event Content
					for($i=0 ; $i<count($evt) ; $i++)
					{
						if (in_array($evt["Status"], $response)) {
							### convert MS SQL Datetime format ###
							#$events[$i]["EventDate"] = date("Y-m-d H:i:s", strtotime($events[$i]["EventDate"]));
							
							$eventDateTimePiece = explode(" ", $evt[$i]["EventDate"]);
							$eventDate = $eventDateTimePiece[0];
				
							$border_css = '';
							if($cnt != count($events)-1 && $i == sizeOf($evt)-1 ){
								$border_css = "style=\"border-bottom:0px\"";
							}
							
							$CalType = trim($evt[$i]['CalType']);
							$eventStartTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
							$eventEndTs = $eventStartTs + $evt[$i]["Duration"]*60;
				
							if ($eventStartTs==$eventEndTs) {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs);
								}
							} else {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
								}
							}
							$isAllDay = (int)$evt[$i]["IsAllDay"];
							$createdBy = $this->returnUserFullName($evt[$i]["UserID"]);
																	
							# Check whether it is valid Foundation Event or not
							# added on 23 Feb 2009
						/*	if($CalType == 2 && (
								($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV') || 
								(($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV') && $evt[$i]['Owner'] != $UserID)
								) ){
								$isFoundation = true;
							} else {
								$isFoundation = false;
							}*/

							# Preparation
							$details_display = ($print) ? "block" : "none";
							$isInvolve = (trim($evt[$i]["Permission"]) == "R") ? true : false;
							//$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
				
						/*	if (!empty($evt[$i]["schoolCode"])){ # other school invitation event
								$otherCalVisible = $this->returnUserPref($UserID, "otherInvitaionCalVisible");
								$display     = ($otherCalVisible == "0") ? "none": "block";
								$numVisible += ($otherCalVisible == "0") ? 0 : 1;
								
								# Get Corresponding Color of each Event
								$ColorID = $this->otherCalColorID;
								$Color	  = trim($this->otherCalColor);
								$evt_entry_id	  = "event".$evt[$i]["EventID"].'-'.$evt[$i]["schoolCode"];
								$div_class = "cal-other";
								$TipName  = "eventToopTip";
							
							}
							elseif($isOtherCalType)				# other foundation event : CPD, CAS
							{		
								$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
								$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
								$otherCalTypeCalVisible = $this->returnUserPref($UserID, $cal_type_name."CalVisible");
								$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
								$numVisible += ($otherCalTypeCalVisible == "0") ? 0 : 1;
								
								$ColorID  = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$Color	  = trim($evt[$i]["Color"]);
								$DivID	  = $cal_type_name."Event".$evt[$i]["EventID"];
								$DivClass = $cal_type_name;
								$TipName  = "eventToopTip";	
							}
							else if($isFoundation)				# Foundation Event
							{				
								$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
								$display	 = ($foundationCalVisible == "0") ? "none" : "block";
								$div_class = "foundation";
								$evt_entry_id = "entry_event".$evt[$i]["EventID"];
								$ColorID  = $this->Get_Calendar_ColorID($evt[$i]["Color"]);								
							}
							else*/ 
							if (!empty($evt[$i]["CalType"])&&
							($evt[$i]["CalType"] == 1 ||$evt[$i]["CalType"] > 3 )){
							# external calendar
								$display     = ($evt[$i]["Visible"] == "0") ? "none": "block";
								$numVisible += ($evt[$i]["Visible"] == "0") ? 0 : 1;

								# Get Corresponding Color of each Event
								$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$Color	  = trim($evt[$i]["Color"]);
								$evt_entry_id	  = $evt[$i]["DivPrefix"].$evt[$i]["EventID"];
								$div_class = $evt[$i]["DivClass"];
								$TipName  = "eventToopTip";
								$CalType = $evt[$i]["CalType"]; 
							}elseif($isInvolve)				# Involved Event
							{			
								$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
								$display	 = ($involveCalVisible == "0") ? "none" : "block";
								$div_class = "involve";
								$evt_entry_id = "entry_involveEvent".$evt[$i]["EventID"];

								$ColorID  = $this->involveCalColorID;
								$Color    = $this->involveCalColor;
								$numVisible += ($involveCalVisible == "0") ? 0 : 1;
							} 
							else 							# personal related events
							{				
								$display   = ($viewableCalVisible[$evt[$i]["CalID"]]==1) ? "block" : "none";
								$div_class = "cal-".$evt[$i]["CalID"];
								$evt_entry_id = "entry_event".$evt[$i]["EventID"];
								$ColorID  = $this->Get_Calendar_ColorID($viewableCalColor[$evt[$i]["CalID"]]);
							}
							
							# Do not output html for print view 
							if($print && $display == 'none') continue;
							
							# Content
							$x .= '<div class="'.$div_class.'" style="display:'.$display.'">';
							$x .= '<table cellspacing="0" cellpadding="0" border="0">';
							$x .= '<tr>';
							$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_time' $border_css>";
							$x .= ($isAllDay ? $iCalendar_NewEvent_IsAllDay : $eventTime);
							$x .= "</td>";
							$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_event' $border_css>";
							if($print){
								$x .= "<img class='cal_event_png_$ColorID' width='15' height='15' border='0' align='absmiddle' src='".$ImagePathAbs."calendar/event_$ColorID.png'/>&nbsp;";
								$x .= "<strong>".intranet_undo_htmlspecialchars($evt[$i]["Title"]). $this->AppendOnwerName($evt[$i]["UserID"])."</strong>";
								if (trim($evt[$i]["PersonalNote"]) != "") 
									$x .= '<img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0">';
									//$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
								$x .= "<br />";
							} else {
								$x .= "<a class='cal_agenda_entry_event' href='javascript:jHide_Div(document.getElementById(\"".$evt_entry_id."\"))'>";
								$x .= "<img class='cal_event_png_$ColorID' width='15' height='15' border='0' align='absmiddle' src='".$ImagePathAbs."calendar/event_$ColorID.png'/>&nbsp;";
								$x .= intranet_undo_htmlspecialchars($evt[$i]["Title"]). $this->AppendOnwerName($evt[$i]["UserID"]);
								if (trim($evt[$i]["PersonalNote"]) != "") 
									$x .= '<img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0">';
									//$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
								$x .= "<br />";
								$x .= "</a>";
							}
							$x .= "<div id='".$evt_entry_id."' class='cal_agenda_content_entry_detail' style='display:$details_display'>";
							$x .= $iCalendar_NewEvent_EventDate.": ".substr($evt[$i]["EventDate"],0,10)."<br>";
							$x .= $iCalendar_NewEvent_Location." : ".((trim($evt[$i]["Location"]) == "") ? '-' : trim(intranet_undo_htmlspecialchars($evt[$i]["Location"])))."<br>";
							if($sys_custom['iCalendar']['EventTitleWithParticipantNames'] && !isset($evt[$i]['RecordType'])){
								$participant_names = $this->getEventTitleExtraInfo($evt[$i]['EventID'],1,1);
								if($participant_names != '') $x .= $iCalendar_NewEvent_Guests." : ".$participant_names."<br />";
							}
							$x .= $iCalendar_NewEvent_Description." : ".((trim($evt[$i]["Description"]) == "") ? '-' : trim(intranet_undo_htmlspecialchars($evt[$i]["Description"])))."<br>";
							$x .= $iCalendar_NewEvent_Personal_Note." : ".((trim($evt[$i]["PersonalNote"]) == "") ? '-' : trim(intranet_undo_htmlspecialchars($evt[$i]["PersonalNote"])))."<br>";
							$x .= $iCalendar_EditEvent_CreatedBy." : ".($this->returnUserFullName($evt[$i]["UserID"]) == '' ? $Lang['iCalendar']['null_created_by']:$this->returnUserFullName($evt[$i]["UserID"]))."<br>";
							$x .= $i_Calendar_modifiedDate. " : ".(trim($evt[$i]['DateModified'])==''?'-':$evt[$i]['DateModified']);
							$x .= "</div>";
							$x .= "</td>";
							$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_button' $border_css>";
							//$x .= "<!--<a class='sub_layer_link' href='#'>$i_Calendar_Delete</a> | -->";
							$x .= (!$print) ? '<a class="sub_layer_link" href="new_event.php?editEventID=event'.$evt[$i]["EventID"].'&calViewPeriod=agenda&CalType='.$CalType.'">'.$i_Calendar_Edit.'</a>' : '&nbsp;';
							$x .= "</td>";
							$x .= "</tr>";
							$x .= "</table>";
							$x .= '</div>';
						}
					}
					$x .= '</td>';
					$x .= '<tr>';
					$x .= '</table>';
					$x .= '</div>';
					$cnt++;
				}
			}
			if (!in_array($eventDate, $dayTableExist))
				$dayTableExist[] = $eventDate;
		} else {
			$x .= "<table cellspacing='0' cellpadding='0' border='0' width='95%'>
					<tr>
					  <td class='cal_agenda_entry_date'>$iCalendar_Agenda_NoEventsForToday</td>
					</tr>
				   </table>";

		}
		$x .= "</div>";
		
		return $x;	
   	}
  
   	function printAgenda($startDate, $endDate, $print=false, $response, $agendaPeriod=0) {
	   	global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_EditEvent_CreatedBy;
	  	global $iCalendar_SchoolEvent_Color, $iCalendar_NewEvent_IsAllDay, $iCalendar_Agenda_NoEvent;
	  	global $iCalendar_NewEvent_Location, $iCalendar_NewEvent_Description;
	  	global $i_Calendar_Today, $i_Calendar_NextSevenDays, $i_Calendar_ThisWeek;
	  	global $events, $schoolEvents, $ImagePathAbs;
	  		  	
	  	# selection box
	  	$KeyValues = array(
				0 => array($i_Calendar_Today, "0"), 
				1 => array($i_Calendar_NextSevenDays, "1"), 
				2 => array($i_Calendar_ThisWeek, "2"));
		$selectAgendaPeriod  = '';
		if(!$print){
			$selectAgendaPeriod .= '<select name="selectAgendaPeriod" id="selectAgendaPeriod" onchange="changeAgendaPeriod(this)">';
			for($i=0 ; $i<count($KeyValues) ; $i++){
				$selected = ($KeyValues[$i][1] == $agendaPeriod) ? "selected" : ""; 
				$selectAgendaPeriod .= '<option value="'.$KeyValues[$i][1].'" '.$selected.'>'.$KeyValues[$i][0].'</option>';
			}
			$selectAgendaPeriod .= "</select>";
		} 
		
		# UI
		$x  = '';
		$x .= "<div class='cal_agenda_content'>";
		$x .= "<div style='padding:3px;'>$selectAgendaPeriod</div>";
		
		$viewableCal = $this->returnViewableCalendar();
		for ($i=0; $i<sizeof($viewableCal); $i++) {
			$viewableCalColor[$viewableCal[$i]["CalID"]] = $viewableCal[$i]["Color"];
			$viewableCalVisible[$viewableCal[$i]["CalID"]] = (int)$viewableCal[$i]["Visible"];
		}
		
		$response = array("A","M","W","");
		
		$dayTableExist = array();

		if(sizeOf($events) > 0) {
		  for($i=0; $i<sizeOf($events); $i++) {
			if (in_array($events[$i]["Status"], $response)) {
				### convert MS SQL Datetime format ###
				#$events[$i]["EventDate"] = date("Y-m-d H:i:s", strtotime($events[$i]["EventDate"]));
				
				$eventDateTimePiece = explode(" ", $events[$i]["EventDate"]);
				$eventDate = $eventDateTimePiece[0];
				
				$border_css = '';
				if($i < sizeOf($events)){
					$nextEventDateTimePiece = explode(" ", $events[$i+1]["EventDate"]);
					$nextEventDate = $nextEventDateTimePiece[0];
					if($eventDate != $nextEventDate && $i != sizeOf($events)-1 ){
						$border_css = "style=\"border-bottom:0px\"";
					}
				}
				
				($eventDate == date("Y-m-d")) ? $dayTableClass = "cal_agenda_entry_date_log cal_agenda_content_entry_today" : $dayTableClass = "cal_agenda_entry_date_log";
				$eventStartTs = $this->sqlDatetimeToTimestamp($events[$i]["EventDate"]);
				$eventEndTs = $eventStartTs + $events[$i]["Duration"]*60;
				
				if ($eventStartTs==$eventEndTs) {
					if ($this->systemSettings["TimeFormat"] == "12") {
						$eventTime = date("g:ia",$eventStartTs);
					} if ($this->systemSettings["TimeFormat"] == "24") {
						$eventTime = date("G:i",$eventStartTs);
					}
				} else {
					if ($this->systemSettings["TimeFormat"] == "12") {
						$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
					} if ($this->systemSettings["TimeFormat"] == "24") {
						$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
					}
				}
				$isAllDay = (int)$events[$i]["IsAllDay"];
				$createdBy = $this->returnUserFullName($events[$i]["UserID"]);
				
				# Get Corresponding Color of each Event
				$ColorID  = $this->Get_Calendar_ColorID($viewableCalColor[$events[$i]["CalID"]]);	
				
				# Preparation
				$display = ($viewableCalVisible[$events[$i]["CalID"]]==1) ? "block" : "none";
				
				$details_display = ($print) ? "block" : "none";
				# Content
				if (!in_array($eventDate, $dayTableExist)) {
					if (sizeof($dayTableExist) > 0) $x .= "</table>";
					$x .= "<table cellspacing='0' cellpadding='0' border='0' class='$dayTableClass'>";
					$x .= "<tr>";
					$x .= "<td class='cal_agenda_entry_date'>";
					if(!$print){
						$x .= "<a class='cal_agenda_entry_event' href='index.php?time=$eventStartTs&calViewPeriod=daily'>".date("j M", $eventStartTs)."</a>";
					} else {
						$x .= date("j M", $eventStartTs);
					}
					$x .= "</td>";
				} else {
					$x .= "<tr><td class='cal_agenda_entry_date'></td>";
				}
				$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_time' $border_css>";
				$x .= ($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime);
				$x .= "</td>";
				$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_event' $border_css>";
				if($print){
					$x .= "<img class='cal_event_png_$ColorID' width='15' height='15' border='0' align='absmiddle' src='".$ImagePathAbs."calendar/event_$ColorID.png'/>&nbsp;";
					$x .= "<strong>".$events[$i]["Title"]."</strong><br />";
				} else {
					$x .= "<a class='cal_agenda_entry_event' href='javascript:jHide_Div(document.getElementById(\"entry_event".$events[$i]["EventID"]."\"))'>";
					$x .= "<img class='cal_event_png_$ColorID' width='15' height='15' border='0' align='absmiddle' src='".$ImagePathAbs."calendar/event_$ColorID.png'/>&nbsp;";
					$x .= $events[$i]["Title"]."<br />";
					$x .= "</a>";
				}
				$x .= "<div id='entry_event".$events[$i]["EventID"]."' class='cal_agenda_content_entry_detail' style='display:$details_display'>";
				$x .= $iCalendar_NewEvent_Location." : ".((trim($events[$i]["Location"]) == "") ? '-' : trim($events[$i]["Location"]))."<br>";
				$x .= $iCalendar_NewEvent_Description." : ".((trim($events[$i]["Description"]) == "") ? '-' : trim($events[$i]["Description"]))."<br>";
				$x .= $iCalendar_EditEvent_CreatedBy." : ".($this->returnUserFullName($events[$i]["UserID"]) == '' ? $Lang['iCalendar']['null_created_by']:$this->returnUserFullName($events[$i]["UserID"]));
				$x .= "</div>";
				$x .= "</td>";
				$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_button'>";
				$x .= "<!--<a class='sub_layer_link' href='#'>$i_Calendar_Delete</a> | -->";
				$x .= "<a class='sub_layer_link' href='new_event.php?editEventID=event".$events[$i]["EventID"]."'>$i_Calendar_Edit</a>";
				$x .= "</td>";
				$x .= "</tr>";
			}
			if (!in_array($eventDate, $dayTableExist))
				$dayTableExist[] = $eventDate;
		  }
		} else {
			$x .= "<table cellspacing='0' cellpadding='0' border='0' width='100%'>
					<tr>
					  <td class='cal_agenda_entry_date'>No events for today.</td>
					</tr>";
		}
		$x .= "</table>";
		$x .= "</div>";
		
		return $x;
		
   	}
   	
	function printAgenda_old($startDate, $endDate, $print=false, $response) {
	  global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_EditEvent_CreatedBy;
	  global $iCalendar_SchoolEvent_Color, $iCalendar_NewEvent_IsAllDay, $iCalendar_Agenda_NoEvent;
	  global $events, $schoolEvents;
	  
	  if ($response=="")
	  	$response = array("A","D","M","W","");
	  else
	  	$response = array($response);
	  
	  # Get calendar color
		$viewableCal = $this->returnViewableCalendar();
		for ($i=0; $i<sizeof($viewableCal); $i++) {
			$viewableCalColor[$viewableCal[$i]["CalID"]] = $viewableCal[$i]["Color"];
			$viewableCalVisible[$viewableCal[$i]["CalID"]] = (int)$viewableCal[$i]["Visible"];
		}
		
		$table_agenda = "";
		if ($print) {
			$x = "<table class=\"eSporttableborder\" id=\"agendaTable\" width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$x .= "<tr>";
		} else {
			$x = "<table id=\"agendaTable\" width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$x .= "<tr class=\"tabletop\">";
		}
		$tableHeaderClass = $print?"eSporttdborder eSportprinttabletitle":"tabletoplink";
		$x .= "<td width=\"1\" class=\"$tableHeaderClass\">#</td>";
		$x .= "<td width=\"50%\" class=\"$tableHeaderClass\">$iCalendar_NewEvent_Title</td>";
		$x .= "<td width=\"15%\" class=\"$tableHeaderClass\">$iCalendar_NewEvent_EventDate</td>";
		$x .= "<td width=\"15%\" class=\"$tableHeaderClass\">$iCalendar_NewEvent_EventTime</td>";
		$x .= "<td width=\"20%\" class=\"$tableHeaderClass\">$iCalendar_EditEvent_CreatedBy</td>";
		$x .= "</tr>\n";
		
		$tableHeader = $x;
		
		$cellClass = $print?"eSporttdborder eSportprinttext":"tabletext";
		
		for($j=0; $j<sizeOf($schoolEvents); $j++) {
			$schoolEventDateTimePiece = explode(" ",$schoolEvents[$j]["EventDate"]);
			$schoolEventDate = $schoolEventDateTimePiece[0];
			$schoolEventTs = $this->sqlDatetimeToTimestamp($schoolEvents[$j]["EventDate"]);
			
			if ($print) {
				$x .= "<tr class=\"".$this->setClassSchoolEventType($schoolEvents[$j]["RecordType"])."\">";
				$x .= "<td class=\"$cellClass\" width=\"1\">".($j+1)."</td>";
				$x .= "<td class=\"$cellClass\">".$schoolEvents[$j]["Title"]."</td>";
				$x .= "<td class=\"$cellClass\">$schoolEventDate</td>";
				$x .= "<td class=\"$cellClass\">-</td>";
				$x .= "<td class=\"$cellClass\">";
				if (trim($schoolEvents[$j]["UserID"]) == "") {
					$x .= "-";
				} else {
					$name = $this->returnUserFullName($schoolEvents[$j]["UserID"]);
					if ($name != 0)
						$x .= $name;
				}
				$x .= "</td>";
				$x .= "</tr>";
			} else {
				$x .= "<tr class=\"$css ".$this->setClassSchoolEventType($schoolEvents[$j]["RecordType"])."\">";
				$x .= "<td class=\"$cellClass\" width=\"1\">".($j+1)."</td>";
				$x .= "<td class=\"$cellClass\"><span style=\"font-weight:bold;color:#".$iCalendar_SchoolEvent_Color[$schoolEvents[$j]["RecordType"]]."\">".$schoolEvents[$j]["Title"]."</span></td>";
				$x .= "<td class=\"$cellClass\"><a class=\"tablelink\" href=\"index.php?time=$schoolEventTs&calViewPeriod=daily\">$schoolEventDate</a></td>";
				$x .= "<td class=\"$cellClass\">-</td>";
				$x .= "<td class=\"$cellClass\">";
				if (trim($schoolEvents[$j]["UserID"]) == "") {
					$x .= "-";
				} else {
					$name = $this->returnUserFullName($schoolEvents[$j]["UserID"]);
					if ($name != 0)
						$x .= $name;
				}
				$x .= "</td>";
				$x .= "</tr>";
			}
		}
		
		for($i=0; $i<sizeOf($events); $i++) {
			if (in_array($events[$i]["Status"], $response)) {
				$css = ($css=="tablerow1")?"tablerow2":"tablerow1";
				$eventDateTimePiece = explode(" ", $events[$i]["EventDate"]);
				$eventDate = $eventDateTimePiece[0];
				$eventStartTs = $this->sqlDatetimeToTimestamp($events[$i]["EventDate"]);
				$eventEndTs = $eventStartTs + $events[$i]["Duration"]*60;
				
				if ($eventStartTs==$eventEndTs) {
					if ($this->systemSettings["TimeFormat"] == "12") {
						$eventTime = date("g:ia",$eventStartTs);
					} if ($this->systemSettings["TimeFormat"] == "24") {
						$eventTime = date("G:i",$eventStartTs);
					}
				} else {
					if ($this->systemSettings["TimeFormat"] == "12") {
						$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
					} if ($this->systemSettings["TimeFormat"] == "24") {
						$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
					}
				}
				$isAllDay = (int)$events[$i]["IsAllDay"];
				$createdBy = $this->returnUserFullName($events[$i]["UserID"]);
				
				$display = $viewableCalVisible[$events[$i]["CalID"]]==1?"block":"none";
				
				if ($print) {
					$x .= "<tr class=\"cal-".$events[$i]["CalID"]."\" style=\"display:'$display'\">";
					$x .= "<td class=\"$cellClass\" width=\"1\">".($j+$i+1)."</td>";
					$x .= "<td class=\"$cellClass\">".$events[$i]["Title"]."</td>";
					$x .= "<td class=\"$cellClass\">$eventDate</td>";
					$x .= "<td class=\"$cellClass\">".($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime)."</td>";
					$x .= "<td class=\"$cellClass\">$createdBy</td>";
					$x .= "</tr>";
				} else {
					$x .= "<tr class=\"$css cal-".$events[$i]["CalID"]."\" style=\"display:'$display';background-color:#".$viewableCalColor[$events[$i]["CalID"]].";\">";
					$x .= "<td class=\"$cellClass\" width=\"1\">".($j+$i+1)."</td>";
					$x .= "<td class=\"$cellClass\"><a class=\"agendaEventTitle\" href=\"new_event.php?editEventID=event".$events[$i]["EventID"]."\">".$events[$i]["Title"]."</a></td>";
					$x .= "<td class=\"$cellClass\"><a class=\"tablelink\" href=\"index.php?time=$eventStartTs&calViewPeriod=daily\">$eventDate</a></td>";
					$x .= "<td class=\"$cellClass\">".($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime)."</td>";
					$x .= "<td class=\"$cellClass\">$createdBy</td>";
					$x .= "</tr>";
				}
			}
		}
		
		if ((sizeof($events)==0 && sizeof($schoolEvents)==0) || $x == $tableHeader) {
		    $x .= "<tr class=\"tablerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"9\" align=\"center\">$iCalendar_Agenda_NoEvent</td></tr>\n";
		}
		$x .= "</table>\n";
		$table_agenda = $x;
		return $table_agenda;
	}
	
	function printBriefAgenda($startDate, $endDate, $response) {
		global $SYS_CONFIG, $today_events, $coming_events, $ImagePathAbs, $lui;
		
		$response = ($response=="") ? array("A","D","M","W","") : array($response);
		
		# Get calendar color
		$viewableCal = $this->returnViewableCalendar();
		for ($i=0; $i<sizeof($viewableCal); $i++) {
			$viewableCalColor[$viewableCal[$i]["CalID"]] = $viewableCal[$i]["Color"];
			$viewableCalVisible[$viewableCal[$i]["CalID"]] = (int)$viewableCal[$i]["Visible"];
		}
		
		$cnt = 0;
		$x  = '<table width="95%" border="0" cellspacing="0" cellpadding="0">';
		# Today Event
		$x .= '<tr><td class="cal_agenda_title" colspan="2">Today</td></tr>';
		if(count($today_events) > 0){
			for($i=0; $i<sizeOf($today_events); $i++) {
				if (in_array($today_events[$i]["Status"], $response)) {
					
					$eventDateTimePiece = explode(" ", $today_events[$i]["EventDate"]);
					$eventDate = $eventDateTimePiece[0];
					$eventStartTs = $this->sqlDatetimeToTimestamp($today_events[$i]["EventDate"]);
					$eventEndTs = $eventStartTs + $today_events[$i]["Duration"]*60;
					
					if ($eventStartTs==$eventEndTs) {
						if ($this->systemSettings["TimeFormat"] == "12") {
							$eventTime = date("g:ia",$eventStartTs);
						} if ($this->systemSettings["TimeFormat"] == "24") {
							$eventTime = date("G:i",$eventStartTs);
						}
					} else {
						if ($this->systemSettings["TimeFormat"] == "12") {
							$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
						} if ($this->systemSettings["TimeFormat"] == "24") {
							$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
						}
					}
					$isAllDay = (int)$today_events[$i]["IsAllDay"];
					$createdBy = $this->returnUserFullName($today_events[$i]["UserID"]);
					
					$display = $viewableCalVisible[$today_events[$i]["CalID"]]==1?"block":"none";
					
					# Get Corresponding Color of each Event
					$ColorID = '';
					$ColorID  = $this->Get_Calendar_ColorID($today_events[$i]["CalID"]);
			    	$ColorID = ($ColorID == "") ? $this->involveCalColorID : $ColorID;
					
					$x .= '<tr>';
					$x .= '<td style="width:2%">';
					$x .= '<img class="cal_event_png_'.$ColorID.'" width="15" height="15" border="0" align="absmiddle" src="'.$ImagePathAbs.'calendar/event_'.$ColorID.'.png"/>';
					$x .= '</td>';
					$x .= '<td class="cal_agenda_entry" style="width:98%">';
					$x .= '<a href="src/module/calendar/new_event.php?editEventID=event'.$today_events[$i]["EventID"].'">';
					$x .= $today_events[$i]["Title"];
					if (trim(UTF8_From_DB_Handler($today_events[$i]["PersonalNote"])) != "") 
						$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					$x .= ' </a>';
					$x .= '<span class="day_time">'.($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime).'</span>';
					$x .= '</td>';
					$x .= '</tr>';
					
					$cnt++;
				}
			}
		} else {
			$x .= '<tr><td class="cal_agenda_entry" colspan="2"><span class="day_time">No Records at this moment.</span></td></tr>';
		} 
		
		# Coming Events
		$x .= ' <tr><td class="cal_agenda_title" colspan="2">Coming</td></tr>';
		if(count($coming_events) > 0){
			for($i=0; $i<sizeOf($coming_events); $i++) {
				if (in_array($coming_events[$i]["Status"], $response)) {
					
					$eventDateTimePiece = explode(" ", $coming_events[$i]["EventDate"]);
					$eventDate = $eventDateTimePiece[0];
					$eventStartTs = $this->sqlDatetimeToTimestamp($coming_events[$i]["EventDate"]);
					$eventEndTs = $eventStartTs + $coming_events[$i]["Duration"]*60;
					
					if ($eventStartTs==$eventEndTs) {
						if ($this->systemSettings["TimeFormat"] == "12") {
							$eventTime = date("g:ia",$eventStartTs);
						} if ($this->systemSettings["TimeFormat"] == "24") {
							$eventTime = date("G:i",$eventStartTs);
						}
					} else {
						if ($this->systemSettings["TimeFormat"] == "12") {
							$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
						} if ($this->systemSettings["TimeFormat"] == "24") {
							$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
						}
					}
					$isAllDay = (int)$coming_events[$i]["IsAllDay"];
					$createdBy = $this->returnUserFullName($coming_events[$i]["UserID"]);
					
					$display = $viewableCalVisible[$coming_events[$i]["CalID"]]==1?"block":"none";
					
					# Get Corresponding Color of each Event
					$ColorID = '';
					$ColorID  = $this->Get_Calendar_ColorID($coming_events[$i]["CalID"]);	
			    	$ColorID = ($ColorID == "") ? $this->involveCalColorID : $ColorID;
					
					$x .= '<tr>';
					$x .= '<td style="width:2%">';
					$x .= '<img class="cal_event_png_'.$ColorID.'" width="15" height="15" border="0" align="absmiddle" src="'.$ImagePathAbs.'calendar/event_'.$ColorID.'.png"/>';
					$x .= '</td>';
					$x .= '<td class="cal_agenda_entry" style="width:98%">';
					$x .= '<a href="src/module/calendar/new_event.php?editEventID=event'.$coming_events[$i]["EventID"].'">';
					$x .= $coming_events[$i]["Title"];
					if (trim(UTF8_From_DB_Handler($coming_events[$i]["PersonalNote"])) != "") 
						$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
					$x .= ' </a>';
					$x .= '<span class="day_time">'.($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime).'</span>';
					$x .= '</td>';
					$x .= '</tr>';
					  
					
					$cnt++;
				}
			}
		} else {
			$x .= '<tr><td class="cal_agenda_entry" colspan="2"><span class="day_time">No Records at this moment.</span></td></tr>';
		}
		  
		$x .= '</table>';
		$table_brief_agenda = $x;
		return $table_brief_agenda;
	}
    
  # Get an array containing the formatted weekdays
  function get_week($year, $month, $day, $date_format="r") {
    $today = date("w", mktime(0, 0, 0, $month, $day, $year));
    $first_weekday = mktime(0, 0, 0, $month, $day, $year) - ($today*24*60*60);
    for ($i=0; $i<7; $i++) {
	    $weekday = date($date_format, $first_weekday+($i*24*60*60));
    	$week[] = $weekday;
  	}
    return $week;
  }
  
  function get_navigation($type, $year, $month, $day="", $isWithLink=false) {
    global $iCalendar_NewEvent_Repeats_WeekdayArray2, $ImagePathAbs, $iCalendar_NewEvent_Repeats_MonthArray;
	if (trim($day) == '') {
		$day = date('d');
	}
    $oldTime = mktime(0,0,0,$month,$day,$year);
	$navigation = "";
	if ($type=="monthly") {
	    $time = mktime(0,0,0,$month-1,$day,$year);
		$navigation .= "<form name='form1' style='margin:0px;padding:0px;'>";
		$navigation .= "<a id=\"lastPeriod\" href=\"javascript:document.form1.time.value='".$time."';getCalendarDisplay('monthly');jQuery.datepicker._adjustDate(0, -1, 'M');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_prev_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		$navigation .= "<span id='periodTitle'>";
		$navigation .= ($isWithLink) ? "<a href='".$PathRelative."src/module/calendar/index.php?time=$time&vcalViewPeriod=monthly'>" : "";
		$navigation .= $iCalendar_NewEvent_Repeats_MonthArray[($month-1)]." $year";
		//$navigation .= date('F', mktime(0,0,0,$month))." $year";
		$navigation .= ($isWithLink) ? "</a>" : "";
		$navigation .= "</span>";
		
		$time = mktime(0,0,0,$month+1,$day,$year);
		$navigation .= "<a id=\"nextPeriod\" href=\"javascript:document.form1.time.value='".$time."';getCalendarDisplay('monthly');jQuery.datepicker._adjustDate(0, +1, 'M');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_next_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		
	    $navigation .= "<input type='hidden' name='time' value='$oldTime' />";
	    $navigation .= "<input type='hidden' name='calViewPeriod' value='monthly' />";
	    $navigation .= "</form>";
    } else if ($type=="weekly") {
		$time = mktime(0,0,0,$month,$day,$year);
	    $current_week = $this->get_week($year, $month, $day, "F d, Y");
		$navigation .= "<form name='form1' style='margin:0px;padding:0px;'>";
		$navigation .= "<a id=\"lastPeriod\" href=\"javascript:document.form1.time.value='".($time-(24*60*60*7))."';getCalendarDisplay('weekly');jQuery.datepicker._adjustDate(0,-7,'D');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_prev_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		$navigation .= "<span id='periodTitle'>";
		$navigation .= ($isWithLink) ? "<a href='".$PathRelative."src/module/calendar/index.php?time=$time&vcalViewPeriod=weekly'>" : "";
		$navigation .= $current_week[0]." - ".$current_week[6];
		$navigation .= ($isWithLink) ? "</a>" : "";
		$navigation .= "</span>";
		
		$navigation .= "<a id=\"nextPeriod\" href=\"javascript:document.form1.time.value='".($time+(24*60*60*7))."';getCalendarDisplay('weekly');jQuery.datepicker._adjustDate(0,+7,'D');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_next_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		
	    $navigation .= "<input type='hidden' name='time' value='$oldTime' />";
	    $navigation .= "<input type='hidden' name='calViewPeriod' value='weekly' />";
	    $navigation .= "</form>";
    } else if ($type=="daily") {
		$time = mktime(0,0,0,$month,$day,$year);
		$navigation .= "<form name='form1' style='margin:0px;padding:0px;'>";
		$navigation .= "<a id=\"lastPeriod\" href=\"javascript:document.form1.time.value='".($time-(24*60*60))."';getCalendarDisplay('daily');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_prev_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		$navigation .= "<span id='periodTitle'>".$iCalendar_NewEvent_Repeats_WeekdayArray2[date("w", $time)].", ".date("j-n-Y",$time)."</span>";
		
		$navigation .= "<a id=\"nextPeriod\" href=\"javascript:document.form1.time.value='".($time+(24*60*60))."';getCalendarDisplay('daily');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_next_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		
	    $navigation .= "<input type='hidden' name='time' value='$oldTime' />";
	    $navigation .= "<input type='hidden' name='calViewPeriod' value='daily' />";
	    $navigation .= "</form>";
    } else if ($type=="agenda") {
		$time = mktime(0,0,0,$month,$day,$year);
		$navigation .= "<form name='form1' style='margin:0px;padding:0px;'>";
		
		$navigation .= "<a id=\"lastPeriod\" href=\"javascript:document.form1.time.value='".($time-(24*60*60*7))."';getCalendarDisplay('agenda');jQuery.datepicker._adjustDate(0,-7,'D');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_prev_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		$navigation .= "<span id='periodTitle'>";
			$navigation .= $iCalendar_NewEvent_Repeats_WeekdayArray2[date("w", $time)].", ".date("j-n-Y",$time);
			$navigation .= ' - ';
			$navigation .= $iCalendar_NewEvent_Repeats_WeekdayArray2[date("w", $time+(7*24*60*59))].", ".date("j-n-Y",$time+(7*24*60*59));
		$navigation .= "</span>";
		
		$navigation .= "<a id=\"nextPeriod\" href=\"javascript:document.form1.time.value='".($time+(24*60*60*7))."';getCalendarDisplay('agenda');jQuery.datepicker._adjustDate(0,+7,'D');\">";
		$navigation .= "<img src='".$ImagePathAbs."layout_grey/btn_next_off.gif' width='18' height='18' border='0' align='absmiddle'></a>";
		
		$navigation .= "<input type='hidden' name='time' value='$oldTime' />";
	    $navigation .= "<input type='hidden' name='calViewPeriod' value='agenda' />";
		$navigation .= "</form>";
	}
    return $navigation;
  }
  
	function getCalViewSelector($CalViewSelect='simple', $ShowViewSelector=true) {
		global $iCalendar_viewMode_simple, $iCalendar_viewMode_full, $lui;
		
		if ($ShowViewSelector) {
			$StyleDisplay = '';
		} else {
			$StyleDisplay = 'none';
		}
		
		$CalViewSelector = '';
		$CalViewSelector .= '<form name="formCalViewSelect" style="margin:0px;padding:0px;">';
			$CalViewSelector .= '<select id="calViewSelect" name="calViewSelect" style="display: '.$StyleDisplay.'" onchange="toggleViewMode(\'monthly\');">';
				$CalViewSelector .= '<option value="simple"'.($CalViewSelect == 'simple' ? 'selected' : '').'>'.$iCalendar_viewMode_simple.'</option>';
				$CalViewSelector .= '<option value="full"'.($CalViewSelect == 'full' ? 'selected' : '').'>'.$iCalendar_viewMode_full.'</option>';
			$CalViewSelector .= '</select>';
			// $CalViewSelector .= "<input type='hidden' name='time' value='$oldTime' />";
			// $CalViewSelector .= "<input type='hidden' name='calViewPeriod' value='daily' />";
		$CalViewSelector .= '</form>';
		
		return $CalViewSelector;
	}
	function getTopToolBar() {
	    global $SYS_CONFIG, $Lang, $lui;
		global $time, $iCalendar_ToolLink_QuickAddEvent, $iCalendar_ToolLink_ChooseDate, $iCalendar_ToolLink_Agenda;
	    $toolBar  = "<div style='width:100%;margin:15px 0px 15px 0px;'>";
	    $toolBar .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
	    $toolBar .= "<tbody><tr><td align='left'>";
	    $toolBar .= $this->getToolLink($iCalendar_ToolLink_QuickAddEvent, $PATH_WRT_ROOT."/images/2007a/icon_new.gif", "#", "quickAddEvent_button");
	    $toolBar .= "&nbsp;&nbsp;";
	    $toolBar .= $lui->Get_HyperLink_Open("#").$lui->Get_Image($SYS_CONFIG['Image']['Abs']."icon_print.gif", 18, 18, 'ALIGN="absmiddle" BORDER="0"')." ".$Lang['email']['Print'].$lui->Get_HyperLink_Close();
	    $toolBar .= "</td><td align='right'>";
	    $toolBar .= "<input type='hidden' id='selectDisplayDate' value='' size='' />";
	    $toolBar .= $this->getToolLink($iCalendar_ToolLink_ChooseDate, $PATH_WRT_ROOT."/images/2007a/iCalendar/icon_calendar.gif", "#", "chooseDateLink");
	    $toolBar .= "&nbsp;&nbsp;";
	    $toolBar .= $this->getToolLink($iCalendar_ToolLink_Agenda, $PATH_WRT_ROOT."/images/2007a/iCalendar/icon_upcoming.gif", "agenda.php?time=$time", "upcomingEventLink");
	    $toolBar .= "</td></tr></tbody></table>";
	    $toolBar .= "</div>";
	    return $toolBar;
	}
        
  # Get tool link
  function getToolLink($ParWord, $ParImgPath, $ParHref, $ParID="", $onClick=""){
		$rx  = "<a class=\"contenttool\" href=\"$ParHref\" id=\"$ParID\"".(($onClick=="")?"":" onclick=\"$onclick\"").">";
		$rx .= "<img src=\"$ParImgPath\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
		$rx .= $ParWord;
		$rx .= "</a>";
		$rx .= $extraContent;
		return $rx;
	}
	
	function printLeftMenu() {
		/*global $iCalendar_CalendarList_Title, $iCalendar_ToolLink_Preference;
		
		$x .= "<div id=\"iCalendar_sidemenu\" style=\"position: absolute; z-index: 2; left: 0px; top: -40px; width: 153px\">";
		$x .= "<table width=\"99%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
								<tbody>
									<tr>
										<td><img height=\"70\" src=\"".$PATH_WRT_ROOT."images/2007a/10x10.gif\"/></td>
									</tr>
									<tr>
										<td>
											<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
												<tbody>
													<tr>
														<td width=\"11\" height=\"19\"><img width=\"11\" height=\"19\" src=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_01.gif\"/></td>
														<td height=\"19\" background=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_02.gif\" align=\"left\" class=\"tabletoplink\">$iCalendar_CalendarList_Title</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
												<tbody>
													<tr>
														<td width=\"5\" background=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_03.gif\"><img width=\"5\" height=\"14\" src=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_03.gif\"/></td>
														<td bgcolor=\"#ffffff\">
															<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
																<tbody>
																	<tr>
																		<td align=\"left\">
																			".$this->generateViewCalendarControl()."
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
												<tbody>
													<tr>
														<td width=\"11\" height=\"10\"><img width=\"11\" height=\"10\" src=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_05.gif\"/></td>
														<td height=\"10\" background=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_06.gif\"><img width=\"11\" height=\"10\" src=\"".$PATH_WRT_ROOT."images/2007a/iCalendar/left_board_06.gif\"/></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>";
		$x .= "</div>";
		return $x;*/
		global $iCalendar_CalendarList_Title, $iCalendar_ToolLink_Preference, $PATH_WRT_ROOT;
		
		$x  = "<div id=\"iCalendar_logo\" style=\"position: absolute; z-index: 2; left: 0px; top: -18px;\">";
		$x .= "<table width=\"175\" height=\"110\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
		$x .= "<tbody><tr>";
		$x .= "<td valign=\"bottom\" height=\"110\" background=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/logo_bg_01.gif\" align=\"center\">";
		$x .= "<img width=\"110\" height=\"99\" src=\"".$PATH_WRT_ROOT."images/2009a/leftmenu/icon_icalendar.gif\"/>";
		$x .= "</td>";
		$x .= "<td width=\"33\" valign=\"bottom\">";
		$x .= "<img width=\"33\" height=\"110\" src=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/logo_bg_02.gif\"/>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</tbody></table>";
		$x .= "</div>";
		 
		$x .= "<div id=\"iCalendar_sidemenu\" style=\"position: absolute; z-index: 2; left: 0px;  top: 30px; width: 153px\">";
		$x .= "<table width=\"99%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
				<tbody>
				  <tr>
					<td><img height=\"70\" src=\"".$PATH_WRT_ROOT."images/2009a/10x10.gif\"/></td>
				  </tr>
				  <tr>
					<td>
					  <table width=\"92%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tbody>
						  <tr>
							<td width=\"11\" height=\"19\"><img width=\"11\" height=\"19\" src=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_01.gif\"/></td>
							<td height=\"19\" background=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_02.gif\" align=\"left\" class=\"tabletoplink\">$iCalendar_CalendarList_Title</td>
						  </tr>
						</tbody>
					  </table>
					</td>
				  </tr>
				  <tr>
					<td>
					  <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tbody>
						  <tr>
							<td width=\"5\" background=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_03.gif\"><img width=\"5\" height=\"14\" src=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_03.gif\"/></td>
							<td bgcolor=\"#ffffff\">
							  <table width=\"92%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
								<tbody>
								  <tr>
									<td align=\"left\">";
		//$x .= $this->generateViewCalendarControl();
		$x .= '<div id="cal_left_act_event">';
		$x .= $this->Get_iCalendar_Template_Event_Notice();
		$x .= '<br style="clear:both">';
		// Small Calendar
		$x .= $this->Get_iCalendar_Template_Small_Calendar();
		// Brief Calendar Manager
		$x .= $this->Get_iCalendar_Template_Calendar_Manager();
		$x .= '</div>';
		$x .= "					    </td>
								  </tr>
								</tbody>
							  </table>
							</td>
						  </tr>
						</tbody>
					  </table>
					</td>
				  </tr>
				  <tr>
					<td>
					  <table width=\"92%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
						<tbody>
						  <tr>
							<td width=\"11\" height=\"10\"><img width=\"11\" height=\"10\" src=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_05.gif\"/></td>
							<td height=\"10\" background=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_06.gif\"><img width=\"11\" height=\"10\" src=\"".$PATH_WRT_ROOT."images/2009a/iCalendar/left_board_06.gif\"/></td>
						  </tr>
						</tbody>
					  </table>
					</td>
				  </tr>
				</tbody>
			</table>";
		$x .= "</div>";
		return $x;
	}
  
  ################################################################
  ####################### Helper Functions #######################
  ################################################################
  function createJsArray($arrayName, $Arr) {
  	$x = "var $arrayName = new Array(";
		for($i=0; $i<sizeof($Arr); $i++) {
			$x .= "'".$Arr[$i]."'";
			if ($i<(sizeof($Arr)-1)) $x .= ",";
		}
		$x .= ");";
		return $x;
  }
  
  function removeAnArrayValue($arr, $value) {
  	if (isset($arr) && sizeof($arr) > 0) {
  		$key = array_search($value, $arr);
  		if ($key !== FALSE)
  			unset($arr[$key]);
  		return array_values($arr);
  	} else {
  		return $arr;
  	}
  }
  
  function do_reg($regex, $text) {
		if (preg_match($regex, $text)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	function isValidURL($url) {
		$regex = '|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i';
		$text = trim($url);
		return $this->do_reg($regex, $text);
	}
	
	function isValidDate($date) {
		$regex = '/^(19|20)([0-9]{2}-(([1-9]|0[13-9]|1[0-2])-([1-9]|0[1-9]|[12][0-9]|30)|(0[13578]|1[02])-31|02-(0[1-9]|1[0-9]|2[0-8]))|([2468]0|[02468][48]|[13579][26])-02-29)$/';
		$text = trim($date);
		return $this->do_reg($regex, $text);
	}
	
	function isValidHour($hour) {
		$hour = trim($hour);
		if (is_numeric($hour)) {
			if ($hour >= 0 && $hour <=23)
				return true;
			else
				return false;
		} else {
			return false;
		}
	}
	
	function isValidMin($min) {
		$min = trim($min);
		if (is_numeric($min)) {
			if ($min >= 0 && $min <=59)
				return true;
			else
				return false;
		} else {
			return false;
		}
	}
	
  
  # This function finds the nth weekday of a given month and year
	# weekday values start at 0 = Sunday
	function NWeekdayOfMonth($occurance,$dayOfWeek,$month,$year,$format="Y-m-d") {
		$DOW1day = sprintf("%02d",(($occurance - 1) * 7 + 1));
		$DOW1 = date("w", mktime(0,0,0,$month,$DOW1day,$year));
		$wdate = ($occurance - 1) * 7 + 1 + (7 + $dayOfWeek - $DOW1) % 7;
		if( $wdate > date("t", mktime(0,0,0,$month,1,$year))) {
			return -1;
		} else {
			return date($format, mktime(0,0,0,$month,$wdate,$year));
		}
	}
	
	function gcd($n,$m) {
		$n = abs($n);
		$m = abs($m);
		if ($n==0 and $m==0)
			return 1; //avoid infinite recursion
		if ($n==$m and $n>=1)
			return $n;
		return $m<$n?$this->gcd($n-$m,$n):$this->gcd($n,$m-$n);
	}
	
	function lcm($n,$m) {
		return $m*($n/$this->gcd($n,$m));
	}
	
	######################################################################################################
	#####################################  for iCalendar use in TG #######################################
	function Get_iCalendar_Template_Event_List($type=''){
		$ReturnStr = '';
		switch($type){
			case 0:
				break;
			default:
				$ReturnStr .= '<div id="btn_link">';
				$ReturnStr .= '<a href="icalendar_new_event_advance.htm"><img src="'.$ImagePathAbs.'icon_add.gif" border="0" align="absmiddle">'.$iCalendar_New_Event.'</a> ';
				//$ReturnStr .= '<a href="javascript:openPrintPage()"><img src="'.$ImagePathAbs.'icon_print.gif" border="0" align="absmiddle">Print</a>';
				$ReturnStr .= '</div>';
		}
		return $ReturnStr;
	}
	
	function Get_iCalendar_Template_Event_Notice($type=''){		
		global $ImagePathAbs,$iCalendar_ActiveEvent_Event, $iCalendar_invitation,
$iCalendar_notification,$plugin,$iCalendar_evtRequest;
		if (!$plugin["iCalendarFull"] && !$plugin["iCalendarFullToAllUsers"]){
			return '';
		}
		$InviteEvent = array();
		$NotifyEvent = array();
		$ReturnStr = '';
		
		$ReturnStr .= '
			<div class="cal_act_event_board"> 
				<span class="cal_event_board_01"><span class="cal_event_board_02"></span></span>
				<div class="cal_event_board_03">
				  <div class="cal_event_board_04">
				  	  <span class="portal_cal_active_event_title">
				  	  <img src="'.$ImagePathAbs.'icon_event.gif" width="22" height="22" align="absmiddle">'.$iCalendar_evtRequest.'</span><br>';
		switch($type){
			default:
				$InviteEvent = $this->Get_Calendar_Active_Event("Invitation");
				$NotifyEvent = $this->Get_Calendar_Active_Event("Notification");
				
				$InviteEvent = $this->Return_Distinct_Object_Array($InviteEvent, 'RepeatID', 'EventID');
				$NotifyEvent = $this->Return_Distinct_Object_Array($NotifyEvent, 'RepeatID', 'EventID');
				
				$ReturnStr .= '<span class="cal_act_event_board_content">';
				$ReturnStr .= '<div id="invite_div"><a id="invite_btn" href="javascript:jShow_Active_Event(\'invite_btn\', \'Invitation\', \'\')" class="invite">'.$iCalendar_invitation.'('.count($InviteEvent).')</a></div>';
				$ReturnStr .= '<input type="hidden" name="invite_cnt" id="invite_cnt" value="'.count($InviteEvent).'" />';
				$ReturnStr .= '<div id="notify_div"><a id="notify_btn" href="javascript:jShow_Active_Event(\'notify_btn\', \'Notification\', \'\')" class="notify">'.$iCalendar_notification.'('.count($NotifyEvent).')</a></div>';
				$ReturnStr .= '<input type="hidden" name="notify_cnt" id="notify_cnt" value="'.count($NotifyEvent).'" />';
				$ReturnStr .= '</span>';
		}
		$ReturnStr .= '
				  </div>
				</div>
				<span class="cal_event_board_05"><span class="cal_event_board_06"></span></span>
				</div>
			  </div>';
		return $ReturnStr;
	}
	
	function Get_iCalendar_Template_Small_Calendar($type=''){
		global $ImagePathAbs, $_SESSION;
		$ReturnStr = '';
		
		$ReturnStr .= '<script type="text/javascript" src="js/jquery.js"></script>';
		$ReturnStr .= '<style type="text/css">@import url(css/ui.datepicker.css);</style>';
		$ReturnStr .= '<script type="text/javascript" src="js/ui.datepicker.js"></script>';
		if ($_SESSION["intranet_session_language"] == "b5") {
			$ReturnStr .= '<script type="text/javascript" src="js/ui.datepicker-zh-TW.js"></script>';
		}
		$ReturnStr .= '<script language="javascript">';
		$ReturnStr .= '
			var query = window.location.search.substring(1);
			var vars = query.split("&"); 
			var time;
			var returnDate;
				if (vars.length > 0) {
					for (var i=0; i<vars.length; i++) {
						var pair = vars[i].split("=");
						if (pair[0] == "time") {
							time = pair[1];
							returnDate = new Date(time*1000);
						}
					}
				}
				if (returnDate == null) {
					returnDate = new Date();
				}
		';
		
		
		$ReturnStr .= 'function changeDate(date) {
							datePiece = date.split("-");
							year = datePiece[0];
							month = datePiece[1] - 1;
							day = datePiece[2];
							newDate = new Date(year,month,day);
							newTime = (newDate.getTime())/1000;
							calViewPeriod = $("input[@name=calViewPeriod]").val();
							window.location = "index.php?calViewPeriod="+calViewPeriod+"&time="+newTime;
							$("input[@name=\'time\']").val(newTime);
					}';
		$ReturnStr .= '$(document).ready( function() {
							$("#cal_board_content").datepicker({
								dateFormat: "yy-mm-dd",
								dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
								changeFirstDay: false,
								firstDay: 0,
								defaultDate: new Date (returnDate),
								onSelect: function(date) {
									changeDate(date);
								}
							});
							
						});';
		$ReturnStr .= '</script>';
		
		$ReturnStr .= '
			<div id="small_cal">
				<span class="small_cal_board_01"><span class="small_cal_board_02"></span></span>
				<div style="background-color:#DBEDFF;float:left;width:100%">
					<div id="cal_board_content" class="cal_board_content" style="padding-left:5px">';
		switch($type){
			default:
				$ReturnStr .= '';
		}
		$ReturnStr .= '
					</div>
				</div>
				<span class="small_cal_board_05"><span class="small_cal_board_06"></span></span>
			  </div>';
		return $ReturnStr;
	}
	
	function Get_iCalendar_Template_Calendar_Manager($type='',$keepSyncLink=true){
		global $ImagePathAbs, $iCalendar_Calendar_ManageCalendar,$iCalendar_NewEvent_Calendar,$iCalendar_User_Preference;
		$ReturnStr = '';
		$ReturnStr .= '
			<div id="cal_list">
				<div id="cal_calendar_tab">
				  <ul>
					<li id="current"><a href="#"><span>'.$iCalendar_NewEvent_Calendar.'</span></a></li>
					<!--<li><a href="icalendar_timetable.htm"><span>Timetable</span></a></li>-->
				  </ul>
				</div>
				<span class="cal_list_manage">
					<a class="sub_layer_link" href="manage_calendar.php">'.$iCalendar_Calendar_ManageCalendar.'</a>
				</span>
				<br style="clear: both;"/>
				<span class="cal_list_manage">
					<a class="sub_layer_link" href="user_preference.php">'.$iCalendar_User_Preference.'</a>
				</span>
				<br style="clear: both;"/>
				<div id="cal_calendar_list">';
		switch($type){
			default:
				$ReturnStr .= $this->generateViewCalendarControl($keepSyncLink);
		}
		$ReturnStr .= '
				</div>
			  </div>';
		return $ReturnStr;
	}
	
	function Get_iCalendar_Template_User_Preference() {
		global $ImagePathAbs, $iCalendar_User_Preference;
		$ReturnStr = '';
		$ReturnStr .= '
			<div id="cal_list">
				<div id="cal_calendar_list">
					<ul>
						<p class="dotline_p_narrow"/>
						<li><a href="#">'.$iCalendar_User_Preference.'</a></li>
					</ul>
				</div>
			</div>';
		return $ReturnStr;
	}
	
	function Get_iCalendar_Template($CalLabeling, $CalMain, $EventList='', $EventNotice='', $SmallCalendar='', $Calendar='', $CalManager='', $enablePrint=false,$keepSyncLink=true){
		global $ImagePathAbs,$iCalendar_eventSearch,$iCalendar_Search;
		$ReturnStr = '';
		
		$ReturnStr .= '
			<script language="javascript">
			var isBlocking;			
			function jShow_Active_Event(objName, ActiveEventType, eventID){
				jGet_Active_Event(ActiveEventType, eventID);
				var objDiv = document.getElementById("cal_act_event_div");
				var objLink = document.getElementById(objName);
				objDiv.style.left = getPostion(objLink, "offsetLeft") + 10;
			 	objDiv.style.top = getPostion(objLink, "offsetTop") + 10;
			 	objDiv.style.visibility = "visible";
			}
			function jHide_Active_Event(){
				var objName = "cal_act_event_div";
				var objDiv = document.getElementById(objName);
				objDiv.style.visibility = "hidden";
			}
			function jGet_Active_Event(ActiveEventType, NextEventID) {
				//Reset_Timeout();
				if (!isBlocking) {
					var blockMsg = "";
					if(jQuery_1_3_2){
						(function($){
							$("div#cal_act_event_wrapper").block({message: blockMsg, overlayCSS: { color:"#ccc" ,border:"3px solid #ccc" }});
						})(jQuery_1_3_2);
					}else{
						$("div#cal_act_event_wrapper").block(blockMsg, { color:"#ccc" ,border:"3px solid #ccc" });
					}
				}
				var params = (NextEventID != "") ? "&eventID="+NextEventID : "";
				if(ActiveEventType == "Notification"){
					var serverScript = "get_active_event.php?ActiveEventType="+ActiveEventType+params;
				} else {
					var serverScript = "get_active_event.php?ActiveEventType="+ActiveEventType+params;
				}
				$("#cal_act_event_wrapper").load(
					serverScript, 
				  function(responseText) {
					if (!SessionExpired(responseText)){
						if(jQuery_1_3_2){
							(function($){
								$("div#calendar_wrapper").unblock();
							})(jQuery_1_3_2);
						}else{
							$("div#calendar_wrapper").unblock();
						}
						isBlocking = false;
					}
				  }
				);
			}
			function jUpdate_Active_Event(ActiveEventType, UpdateEventID, NextEventID, Reply){
				//Reset_Timeout();
				if (!isBlocking) {
					var blockMsg = "";
					if(jQuery_1_3_2){
						(function($){
							$("div#cal_act_event_wrapper").block({message: blockMsg, overlayCSS: { color:"#ccc" ,border:"3px solid #ccc" }});
						})(jQuery_1_3_2);
					}else{
						$("div#cal_act_event_wrapper").block(blockMsg, { color:"#ccc" ,border:"3px solid #ccc" });
					}
				}
				var params = (NextEventID != "") ? "&eventID="+NextEventID : "";
				params += (UpdateEventID != "") ? "&updateEventID="+UpdateEventID : "";
				params += "&Reply="+Reply;
				if(ActiveEventType == "Notification"){
					var serverScript = "get_active_event.php?ActiveEventType="+ActiveEventType+params;
				} else {
					var serverScript = "get_active_event.php?ActiveEventType="+ActiveEventType+params;
				}
				$("#cal_act_event_wrapper").load(
					serverScript,  
				  function(responseText) {
					if (!SessionExpired(responseText)){ 
						// Update Number of Invitation OR Notification
						var inviteCntobj = document.getElementById("invite_cnt");
						var notifyCntObj = document.getElementById("notify_cnt");
						var inviteCnt = inviteCntobj.value;
						var notifyCnt = notifyCntObj.value;
						var inviteObj = document.getElementById("invite_div");
						var notifyObj = document.getElementById("notify_div");
						if(ActiveEventType == "Notification"){ 
							notifyCnt = parseInt(notifyCnt) - parseInt(1);
							notifyCntObj.value = notifyCnt;
						}
						if(ActiveEventType == "Invitation"){ 
							inviteCnt = parseInt(inviteCnt) - parseInt(1);
							inviteCntobj.value = inviteCnt;
						}
						// update Event Cnt HTML
						var html = "";
						html += "<a id=\"invite_btn\" href=\"javascript:jShow_Active_Event(\'invite_btn\', \'Invitation\', \'\')\" class=\"invite\">Invitation("+inviteCnt+")</a>";
						inviteObj.innerHTML = html;
						var html = "";
						html += "<a id=\"notify_btn\" href=\"javascript:jShow_Active_Event(\'notify_btn\', \'Notification\', \'\')\" class=\"notify\">Notification("+notifyCnt+")</a>";
						notifyObj.innerHTML = html;
						// Update the Event of the view of Calendar
						document.form1.time.value = '.((isset($_SESSION["iCalDisplayTime"]) && $_SESSION["iCalDisplayTime"] != "") ? $_SESSION["iCalDisplayTime"] : time() ).';
						changePeriodTab(\''.$_SESSION["iCalDisplayPeriod"].'\');
						if(jQuery_1_3_2){
							(function($){
								$("div#calendar_wrapper").unblock();
							})(jQuery_1_3_2);
						}else{
							$("div#calendar_wrapper").unblock();
						}
						isBlocking = false;
					}
				  }
				);
			}
			</script>
		';
		$ReturnStr .= $this->Get_Active_Event_Board_Div();
		global $iCalendar_New_Event,$iCalendar_Print;

		$ReturnStr .= '<div id="ical_left">';
		$ReturnStr .= '<div id="btn_link">';
	  	$ReturnStr .= '<a href="new_event.php"><img src="'.$ImagePathAbs.'icon_add.gif" border="0" align="absmiddle">'.$iCalendar_New_Event.'</a>';
	    $ReturnStr .= ($enablePrint == true) ? '<a href="javascript:openPrintPage()"><img src="'.$ImagePathAbs.'icon_print.gif" border="0" align="absmiddle">'.$iCalendar_Print.'</a>' : '';
		$ReturnStr .= '</div>';
		
		// Event List
		$ReturnStr .= $this->Get_iCalendar_Template_Event_List($EventList);
		$ReturnStr .= '<br style="clear:both">';
		$ReturnStr .= '<div id="cal_left_act_event">';
		//Event Notice
		$ReturnStr .= $this->Get_iCalendar_Template_Event_Notice($EventNotice);
		$ReturnStr .= '<br style="clear:both">';
		//Small Calendar
		$ReturnStr .= $this->Get_iCalendar_Template_Small_Calendar($SmallCalendar);
		//Brief Calendar Manager
		$ReturnStr .= $this->Get_iCalendar_Template_Calendar_Manager($CalManager,$keepSyncLink);
		$ReturnStr .= '<br style="clear:both">';
		//User Preference
		// $ReturnStr .= $this->Get_iCalendar_Template_User_Preference();
		
		$ReturnStr .= '</div>';
		$ReturnStr .= '</div>';
		
		global $iCalendar_seachType_all,$iCalendar_seachType_page,$iCalendar_seach_Keyword;
		$ReturnStr .= '<div id="ical_right">';
		$ReturnStr .= '<br>';
		// Start Calendar Labeling
		$ReturnStr .= $CalLabeling;
		/* 
		// Search Box
		$ReturnStr .= " 
		<div id='search_navigation' name='search_navigation' style='display:none'>&nbsp;</div>
		<!-- <div>&nbsp;</div> /-->
		<div id='searchSystem' style='display: none; width: 300px; float: right;'>
			<form onsubmit='searchEngin.search(document.getElementById(\"searchValue\").value); return false;'>&nbsp;
				<fieldset>
					<legend>$iCalendar_eventSearch</legend> 
					<!-- <div>&nbsp;</div> /-->
					$iCalendar_seach_Keyword:&nbsp;<input id='searchValue' name='searchValue' type='text'>
					<br />
					<div style='display:inline'>
					<input type='radio' name='searchType' id='searchType_0' value='P' checked='true' onclick='searchEngin.setSearchType(\"P\")'>
					$iCalendar_seachType_page
					<input type='radio' name='searchType' value='A' id='searchType_1' onclick='searchEngin.setSearchType(\"A\")'>
					$iCalendar_seachType_all
					</div> 		
					&nbsp;<input type='submit' value='$iCalendar_Search' >	
					<div>&nbsp;</div>		
				</fieldset>
			</form>
			<!-- <p style='line-height:20px'>&nbsp;</p> /-->
		</div>";
		 */
		$ReturnStr .= '<br style="clear:both">';
		// Start Big Calendar
		$ReturnStr .= (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))?'<div style="position:absolute">':'';
		$ReturnStr .= $CalMain;
		
		#Relocated after $CalLabeling
		
		$ReturnStr .= " 
		<div style='float:right;visibility:hidden' id='search_navigation' name='search_navigation' >&nbsp;</div><div>&nbsp;</div>
		<div id='searchSystem' style='display:none'>
		<form onsubmit='searchEngin.search(document.getElementById(\"searchValue\").value); return false;'>&nbsp;
		<fieldset style='height: 55px'>
		<legend>$iCalendar_eventSearch</legend> 
		<div>&nbsp;</div>
		$iCalendar_seach_Keyword:&nbsp;<input id='searchValue' name='searchValue' type='text'>
		<div style='display:inline'>
		<input type='radio' name='searchType' id='searchType_0' value='P' onclick='searchEngin.setSearchType(\"P\")'>
		$iCalendar_seachType_page
		<input type='radio' name='searchType' value='A' id='searchType_1' checked='chceked' onclick='searchEngin.setSearchType(\"A\")'>
		$iCalendar_seachType_all
		</div> 		
		&nbsp;<input type='submit' value='$iCalendar_Search' >	
		<div>&nbsp;</div>		
		</fieldset>
		</form>
		<p style='line-height:20px'>&nbsp;</p>
		</div>";
		
		
		$ReturnStr .= (strstr($_SERVER['HTTP_USER_AGENT'],'MSIE'))?'</div>':'';
		$ReturnStr .= '</div>';
		return $ReturnStr;
	}
	
	
	######################################################################################################
	
	# Get Color Choices for manage Calendar use
	function Get_Color_Selection()
	{
		#Michael Cheung (2009-10-30) - appended color : 000000 to key 21
		$ColorSelectionArr = array(
					"01"=>"b00408", "02"=>"e97e0b", "03"=>"d2a800", "04"=>"467a13", "05"=>"809121", 
				   	"06"=>"4db308", "07"=>"8ad00f", "08"=>"09759d", "09"=>"21a6ae", "10"=>"65979b", 
				   	"11"=>"2f75e9", "12"=>"3ea6ff", "13"=>"1e2cd8", "14"=>"8c66d9", "15"=>"994499", 
				   	"16"=>"5c1fa0", "17"=>"dd4477", "18"=>"d96666", "19"=>"ac5c14", "20"=>"7f4b21",
					"21"=>"000000" );
		return $ColorSelectionArr;
	}
	
	# Get all EventID(s) which belongs "Invitation" or "Notification"
	# ActiveEventType: "Invitation" or "Notification"
	function Get_Calendar_Active_Event($ActiveEventType){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$result = array();
				
		if($ActiveEventType == "Notification"){
			$sub_sql = "a.InviteStatus = 0 AND a.Status = 'W' AND ";
		} else {
			$sub_sql = "a.InviteStatus = 1 AND a.Status = 'W' AND ";
		}
		$sql = "SELECT 
					DISTINCT a.EventID, b.RepeatID, b.EventDate, b.Duration, b.isAllDay
				FROM 
					CALENDAR_EVENT_USER AS a 
				INNER JOIN CALENDAR_EVENT_ENTRY AS b ON
					b.EventID = a.EventID 
				WHERE 
					a.UserID = '$uid' AND 
					$sub_sql 
					a.Access != 'A' 
				ORDER BY 
					b.RepeatID, a.EventID";
		$result = $this->returnArray($sql);
		/*$icalAPI = new icalendar_api();
		$otherSchool = $icalAPI->Get_Invitation_Active_Event($ActiveEventType);
		if (!empty($otherSchool))
			$result = array_merge($result, $otherSchool);*/
		
		return $result;
	}
	
	function Get_Active_Event_Board_Div(){
		global $ImagePathAbs, $iCalendar_ActiveEvent_Event;
		$returnStr  = '';
		$returnStr .= '
			<div id="cal_act_event_div" style="position:absolute; width:200px; z-index:3; visibility: hidden;">
			  <div class="cal_act_event_board_expand">
				<span class="cal_event_board_01"><span class="cal_event_board_02"></span></span>
				<div class="cal_event_board_03">
				  <div class="cal_event_board_04">
				    <span class="portal_cal_active_event_title">
					  <span style="float:left" ><img src="'.$ImagePathAbs.'icon_event.gif" width="22" height="22" align="absmiddle">'.$iCalendar_ActiveEvent_Event.'</span>							
					  <span style="float:right"><span class="sub_layer_close"><a href="javascript:jHide_Active_Event()">X</a></span></span>
				    </span> 
				    <br style="clear:both">
			        <span class="cal_act_event_board_content">
			          <div id="cal_act_event_wrapper">
					    <div id="cal_act_event_content"></div>
					  </div>
				    </span>
				  </div>
				</div>
				<span class="cal_event_board_05"><span class="cal_event_board_06"></span></span>
			  </div>
			</div>';
		return $returnStr;
	}
		
	function Get_Calendar_ColorID($color){
		$color_id = '';
		
		# Get Calendar color Choices
		$ColorSelectionArr = $this->Get_Color_Selection();
		
		# Get Corresponding Color of each Event
		if(count($ColorSelectionArr) > 0){
			foreach($ColorSelectionArr as $key => $ColorCode){
		    	if(strtolower($color) == strtolower($ColorCode)){
			    	$color_id = $key;
			    	continue;
		    	}
	    	}
    	}
		
		return $color_id;
	}
	
	function Get_Public_Calendar_Subscribers($CalID, $getCount=true){
		$result = array();
		
		if($CalID != ""){
			$sql = "SELECT 
						UserID 
					FROM 
						CALENDAR_CALENDAR_VIEWER 
					WHERE 
						CalID = '$CalID' AND 
						Access = 'P'";
			$result = $this->returnArray($sql);
		}
		
		if($getCount){
			return count($result);
		} else {
			return $result;
		}
	}
	
	function Get_Current_Event_Type($CalID, $EventID){
		$result = array();
		
		# Get Current EventType 
		if($CalID != '' && $EventID != ''){
			$sql = "SELECT a.InviteStatus FROM 
						CALENDAR_EVENT_USER AS a 
					INNER JOIN CALENDAR_EVENT_ENTRY AS b ON 
						b.EventID = a.EventID AND 
						b.UserID = a.UserID 
					INNER JOIN CALENDAR_CALENDAR AS c ON 
						c.CalID = b.CalID AND 
						c.Owner = b.UserID 
					WHERE 
						b.CalID = '$CalID' AND 
						a.EventID = '$EventID'";
			$result = $this->returnVector($sql);
		}
		$type = (isset($result[0])) ? $result[0] : 'NULL';
		return $type;
	}
	
	function returnInvolveCalendarSettings(){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$result = array();
		$returnArr = array();
				
		$sql  = "SELECT
					b.Color
				FROM CALENDAR_CALENDAR AS a
					INNER JOIN CALENDAR_CALENDAR_VIEWER AS b ON
					b.UserID = a.Owner AND
					b.CalID = a.CalID
				WHERE
					(b.Access IS NOT NULL AND b.Access != '') AND
					a.Owner = '$uid'
					order by a.CalID ASC limit 1";			
		$result = $this->returnArray($sql,1);
		
		# assign to a set of parameter 
		if(count($result) > 0){
			# Get Corresponding Color of each Event
			$ColorID = $this->Get_Calendar_ColorID($result[0]['Color']);
			
			$returnArr['valid'] = true;
			$returnArr['color'] = $result[0]['Color'];
			$returnArr['colorID'] = $ColorID;
			$returnArr['fontcolor'] = "FFFFFF";
		} else {
			$returnArr['valid'] = false;
			$returnArr['color'] = "FFFFFF";
			$returnArr['colorID'] = '';
			$returnArr['fontcolor'] = "000000";
		}
		return $returnArr;
	}
	
	function displayMoreCalEvent($ts, $print, $evtdate){
		global $events, $num, $ImagePathAbs;
		$CalEventMoreDiv = '';
		$num = 0;
		
		$viewableCal = $this->returnViewableCalendar();
		$limit = $this->limitEventDisplay;
		
		$evtdatapiece = explode('-', $evtdate);
		$date = $evtdatapiece[2];
		
		# Start More Event Div Layer
		$CalEventMoreDiv .= '<div id="class="cal_normal_day" width="180px">';
		$CalEventMoreDiv .= '<div id="date_'.$date.'" style="display:none;position:absolute;width: 180px; background-color:#FFFFFF; border:solid 1px #000000">';
		$CalEventMoreDiv .= '<span>';
		//$CalEventMoreDiv .= '<a href="new_event.php?calDate='.$evtdate.'&calViewPeriod=monthly" class="day_add_event"><img src="'.$ImagePathAbs.'icon_add_s.gif" width="9" height="9" border="0" align="absmiddle"> New</a>';
		$CalEventMoreDiv .= '<a href="javascript:jHide_More_Event_List('.$date.')" class="day_close">X</a><a class="day_st"> | </a><a href="javascript:document.form1.time.value=\''.$ts.'\';changePeriodTab(\'daily\');" class="day_no">'.$date.'</a><br style="clear:both">';
		$CalEventMoreDiv .= '</span>';
		$CalEventMoreDiv .= '<div id="date_detail_'.$date.'" class="cal_month_event_item"><ul>';
		
		# Cal Event
		for ($i=0; $i<sizeof($viewableCal); $i++) {
	        if ($print){
            	$calEventDiv = $this->displayCalEvent($viewableCal[$i], $ts, true, false, 24);
        	} else {
            	$calEventDiv = $this->displayCalEvent($viewableCal[$i], $ts, false, false, 24);
        	}
            if ($calEventDiv != "") {
            	$CalEventMoreDiv .= $calEventDiv;
    		}
    	}
		
		# Involve Event
		if ($print){
    		$involveEventDiv = $this->displayInvolveEvent($ts, true, false, 24);
		} else {
    		$involveEventDiv = $this->displayInvolveEvent($ts, false, false, 24);
		}
    	if ($involveEventDiv != "") {
        	$CalEventMoreDiv .= $involveEventDiv;
		}
		
		# End of More Event Div Layer
		$CalEventMoreDiv .= '</div>';
		$CalEventMoreDiv .= '</div>';
		$CalEventMoreDiv .= '</div>';
		
		return $CalEventMoreDiv;
	}
	
	function generateMonthlySmallCalendar($year, $month, $identity="",$caltype="") {
	  global $image_path,$eclass_db;
		//global $SYS_CONFIG;
		//global $events;

		static $day_headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
		$first_of_month = mktime (0,0,0, $month, 1, $year);
		$maxdays   = date('t', $first_of_month); # number of days in the month
		$date_info = getdate($first_of_month);   # get info about the first day of the month
		$month 	   = $date_info['mon'];
		$year 	   = $date_info['year'];
		$numOfRows = 0;
		if ($caltype == 2){
			$sql = "Select CalID from INTRANET_GROUP where GroupID = $identity";
		}
		else if($caltype == 3){
			$sql = "Select CalID from {$eclass_db}.course where course_id = $identity";
		}
		$lowBound = strtotime("{$year}-{$month}-01");
		$upBound = strtotime("last day",strtotime("+1 month",$lowBound));
		$events = $this->getCalendarRelatedEvent("",$sql,$lowBound,$upBound);
		$total_events= $this->Return_Query_Events_in_Associated_Array($events);
		$eventColor =  $events[0]["Color"];
		
		 
		$month_date = date('F', strtotime("$year-$month-01"));
		$numOfDate = date('t', strtotime("$year-$month-01"));
		$icalendar_path ="newWindow('/home/iCalendar/index.php',31)";
		
		if (isset($caltype))
			$icalendar_path = ($caltype==3)?"newWindow('".$intranet_rel_path."/home/iCalendar/index.php?Caltype=3&CourseID={$identity}&action=CalDisable',32)":"newWindow('/home/iCalendar/index.php?Caltype={$caltype}&GroupID={$identity}&action=CalDisable',31)";
		
		$calendar = '<style type="text/css">
						.small_cal_sunday2 {
							background-color:#FFE6E6;
						}
						.small_cal_blank_sunday2 {
							background-color:#EEE2E2;
						}
						.small_cal_blank2 {
							background-color:#F7F7F7;
						}
						.small_cal_week_sunday2 {
							color:#FF0000;
						}
						</style>
					';
		$calendar .= '
					<div class="select_month">
						<span></span>
						<a href="javascript:'.$icalendar_path .'">
						<span class="select_month_name">
						'.$month_date.' '.$year.'
						</span>
						</a>
						<span></span>
						<br style="clear: both;"/>
					  </div>';
		$calendar .= '<div style="clear: both;">';
		$calendar .= "<table cellpadding='0' cellspacing='1' id='monthly' border='0'>";
		$calendar .= "<colgroup>
						<col class='Sun' />
						<col class='Mon' />
						<col class='Tue' />
						<col class='Wed' />
						<col class='Thu' />
						<col class='Fri' />
						<col class='Sat' />
						<col />
					  </colgroup>";
		$calendar .= "<thead>
						<tr>
						<th class='small_cal_week small_cal_week_sunday2'>S</th> 
						<th class='small_cal_week'>M</th>
						<th class='small_cal_week'>T</th>
						<th class='small_cal_week'>W</th>
						<th class='small_cal_week'>T</th>
						<th class='small_cal_week'>F</th>
						<th class='small_cal_week'>S</th>
						</tr>
					  </thead>";       
		$calendar .= "<tbody><tr>";
		$class  = "";
		$weekday = $date_info['wday']; #weekday (zero based) of the first day of the month

		# set class of cell: normal or print
		$monthTodayCellClass = "small_cal_date small_cal_today";

		#take care of the first "empty" number of the month
		if($weekday > 0) {
			for ($i=($weekday-1); $i>=0; $i--) {
				$previous_month_date_ts = strtotime("$year-$month-1") - (($i+1) * 24 * 60 * 60);
				$previous_month_date = date('d', $previous_month_date_ts);
				$previous_month_month = date('m', $previous_month_date_ts);
				$previous_month_year = date('Y', $previous_month_date_ts);
				$datePresentation = date('w', $previous_month_date_ts);
				$monthCellClass = ($datePresentation == 0) ? "small_cal_blank_sunday2" : "small_cal_blank2";
				$calendar .= "<td class=\"small_cal_date $monthCellClass\">
						<a href=\"javascript:void(0);\">
						  <span>".(int)$previous_month_date."</span>
						</a></td>";
			}
		} 

		#print the number of the month
		$week = 1;
		for ($day=1; $day<=$maxdays; $day++) {
			$result = array();
			$ts = mktime(0,0,0,$month,$day,$year);
			$end_ts = mktime(0,0,0,$month,$day,$year) + 24*60*60;
			 
			$datePresentation = date('w', $ts);
			
			# Get Number of Event in each day
			$current_date = date("Y-m-d", $ts);
			$numOfEvents = count($events[$current_date]);
			
			$monthCellClass = ($datePresentation == 0) ? "small_cal_date small_cal_sunday2" : "small_cal_date";
			
			$textcolor = "";
			if (!empty($total_events[$current_date])){
				$textcolor = 'style="color:#'.$eventColor.';font-weight: bold;"';
			}
			
			# Content
			if ($year."-".$month."-".$day == date("Y-n-j", time())){
				$calendar .= "<td >";
				$calendar .= "<div id=\"$year-$month-$day\" class=\"$monthTodayCellClass small_cal_event\">";
			} else {
				$calendar .= "<td >";
				$calendar .= "<div id=\"$year-$month-$day\" class=\"$monthCellClass\">";
			}
			$calendar .= "<a href=\"javascript:void(0);\">";
			$calendar .= "<span $textcolor>$day</span><br />";
			if($numOfEvents > 0){
				$calendar .= "<div class=\"portal_cal_month_event\" style=\"text-align:left;color:#0066CC\">";
				$calendar .= $numOfEvents;
				$calendar .= "<img src=\"".$image_path."/icon_event_s.gif\" width=\"13\" height=\"14\" class=\"cal_event_blue\" align=\"absbottom\" style=\"border:0\">";
				$calendar .= "</div>";
			}
			$calendar .= "</a>";
			$calendar .= "</div>";
			$calendar .= "</td>";
			$weekday++;
			  
			if($weekday == 7) {
				$calendar .= "</tr>";
				$calendar .= ($day != $maxdays) ? "<tr>" : "";
				$weekday = ($day != $maxdays) ? 0 : $weekday;
				$week++;
				$numOfRows++;
			}
		}

		# Generate Remaining Cell 
		if($weekday != 7) {
			for ($i=0; $i<(7-$weekday); $i++) {
				$next_month_date_ts = strtotime("$year-$month-$maxdays") + (($i+1) * 24 * 60 * 60);
				$next_month_date = date('d', $next_month_date_ts);
				$next_month_month = date('m', $next_month_date_ts);
				$next_month_year = date('Y', $next_month_date_ts);
				$datePresentation = date('w', $next_month_date_ts);
				$monthCellClass = "small_cal_date small_cal_blank2";
				$calendar .= "<td class=\"$monthCellClass\">
						<a href=\"javascript:void(0);\">
						  <span>".(int)$next_month_date."</span>
						</a></td>";
			}
			$calendar .= "</tr>";
			$numOfRows++;
		}
		if($numOfRows < 6){
			$calendar .= "<tr>";
			for ($i=0; $i<7; $i++) {
				$monthCellClass = "small_cal_date small_cal_blank";
				$calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
			}
			$calendar .= "</tr>";
		}
		$calendar .= "</tbody></table>";
		
		$calendar .= '</div>';
		return $calendar;
    }
    
    function generateWeeklySmallCalendar($year, $month, $day) {
	    global $SYS_CONFIG;
	    global $iCalendar_NewEvent_IsAllDay;
	    global $events;
	    
	    $current_week_date = $this->get_week($year, $month, $day, "Y-m-d");
	    for ($i=0; $i<24; $i++) {
		    $start_time = (($i<10)?"0$i":"$i").":00";
        	$end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	        for ($j=0; $j<7; $j++) {
		        if (!isset($eventHappenGrid[$i][$j])) {
		        	$eventHappenGrid[$i][$j] = array();
	        	}
	        	if (!isset($eventHappenDetailGrid[$i][$j])) {
		        	$eventHappenDetailGrid[$i][$j] = array();
	        	}
		        if (!isset($eventRowSpan[$i][$j])) {
		        	$eventRowSpan[$i][$j] = 0;
	        	}
		        $start_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $start_time");
		        $end_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $end_time");
		        # Handling the timeslot from 23:00 to 00:00(next day) since $this->sqlDatetimeToTimestamp("24:00") return false;
	     		if($i+1==24) $end_ts = $start_ts + 60*60;
	     		 
	     		# Count Row Span Algorithm
		        for ($k=0; $k<sizeof($events); $k++) {
			        $curEventTs = $this->sqlDatetimeToTimestamp($events[$k]["EventDate"]);
			        if ($curEventTs >= $start_ts && $curEventTs < $end_ts) {
				        $numOfHours = ceil($events[$k]["Duration"]/60);
				        if (date("i",$curEventTs) != "00")
				        	$numOfHours++;
				        if ($numOfHours <= 1) {
				        	$eventHappenGrid[$i][$j][] = $events[$k]["EventID"];
				        	$eventHappenDetailGrid[$i][$j][] = $events[$k];
				        	### Bug Fix on 2008-01-17 ###
				        	# When multiple events start in the same cell, do not reset the
				        	# row span to 0 if there already exist a value
				        	if (!isset($rowSpan[$i][$j]) || empty($rowSpan[$i][$j]))
				        		$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? 0 : $rowSpan[$i][$j];
				        	if (!isset($eventHappenGrid[$i+1][$j])) {
				        		$eventHappenGrid[$i+1][$j] = array();
				        		$eventHappenDetailGrid[$i+1][$j] = array();
			        		}
			        	} else {
				        	$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? $numOfHours : $rowSpan[$i][$j];
					        for ($l=0; $l<$numOfHours; $l++) {
					        	$eventHappenGrid[$i+$l][$j][] = $events[$k]["EventID"];
					        	$eventHappenDetailGrid[$i+$l][$j][] = $events[$k];
				        	}
			        	}
			        } else {
				        if (!isset($eventHappenGrid[$i+1][$j])) {
				       		$eventHappenGrid[$i+1][$j] = array();
			        	}
			        	if (!isset($eventHappenDetailGrid[$i+1][$j])) {
				        	$eventHappenDetailGrid[$i+1][$j] = array();
			        	}
			        }
		        }
		        if($i == 23) $rowSpan[$i][$j] = 1;
		        if($rowSpan[$i][$j] == NULL || $rowSpan[$i][$j] == "") $rowSpan[$i][$j] = 0;
	       	}
      	}
      	$current_week = $this->get_week($year, $month, $day, "d/m D");
    	
	   	$calendar .= "<table cellspacing='0' cellspacing='0' id='weekly'>";	    
	    $calendar .= "<colgroup>
	      				<col />
	      				<col class='Sun' />
						<col class='Mon' />
						<col class='Tue' />
						<col class='Wed' />
						<col class='Thu' />
						<col class='Fri' />
						<col class='Sat' />
					  </colgroup>";
		$calendar .= "<thead><tr>";
		$calendar .= "<td class='top_empty'>&nbsp;</td>";
		
		for($i=0; $i<7; $i++) {
			# apply different class for TODAY
			$display_date = date("d/m", strtotime($current_week_date[$i]));
			$display_week = date("D", strtotime($current_week_date[$i]));
			if (date("d/m D", time()) == $current_week[$i]){
				$calendar .= "<th class='cal_week_title_today'>".$display_date."<br>".$display_week."</th>";
			} else {
				$calendar .= "<th class='cal_week_title'>".$display_date."<br>".$display_week."</th>";
			}
		}
		$calendar .= "</tr></thead>";
		$calendar .= "<tbody>";
		
		# Top cells for all day event
		$calendar .= "<tr>";
		$calendar .= "<td scope='row'></td>";
		for ($i=0; $i<7; $i++) {
			$numOfAllDayEvent = 0;
			if (sizeof($eventHappenDetailGrid[0][$i]) > 0) {
				for($j=0; $j<sizeof($eventHappenDetailGrid[0][$i]); $j++) {
					if ($eventHappenDetailGrid[0][$i][$j]["IsAllDay"] == "1"){
						$numOfAllDayEvent++;
					}
				}
			}
			if (date("d/m D", time()) == $current_week[$i]){
				$calendar .= "<td id='$current_week_date[$i]' class='portal_cal_week_day_whole_today'>";
			} else {
				$calendar .= "<td id='$current_week_date[$i]' class='portal_cal_week_day_whole'>";
			}
			
			if($numOfAllDayEvent > 0){
				$ts = strtotime($current_week_date[$i]);
				$calendar .= '<a href="'.$PathRelative.'src/module/calendar/index.php?time='.$ts.'&calViewPeriod=daily" class="portal_week_event_item_whole">';
				$calendar .= $numOfAllDayEvent;
				$calendar .= '<img src="'.$SYS_CONFIG['Image']['Abs'].'icon_event_s.gif" width="13" height="14" border="0" align="absmiddle" class="cal_event_blue">';
				$calendar .= '</a>';
			} else {
				$calendar .= "&nbsp;";
			}
			$calendar .= "</td>";
		}
		$calendar .= "</tr>";
		
      for ($i=$this->workingHourStart; $i<$this->workingHourEnd; $i++) {
        $start_time = sprintf("%02d",$i).":00";
        $end_time = sprintf("%02d",$i+1).":00";
        $display_time = sprintf("%02d",$i)."00";
        
        $calendar .= "<tr>";
        $calendar .= "<td class='portal_cal_week_time' scope='row'>$display_time</td>";
        for ($j=0; $j<7; $j++) {
	        if ($i==$this->workingHourStart)
	        	$curLastIntersect = array();
	        else
	        	$curLastIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i-1][$j]);
	        
	        if ($i==$this->workingHourEnd)
	        	$curNextIntersect = array();
	        else {
		        if ($eventHappenGrid[$i+1][$j])
	        		$curNextIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i+1][$j]);
        	}
        	
	        # No event in this cell OR the event only need one cell
	        $numOfEvent = 0;
	        if (empty($curLastIntersect) && empty($curNextIntersect)) {
		        # Count Number of Event 
		        if (count($eventHappenDetailGrid[$i][$j]) > 0){
			        for($k=0 ; $k<count($eventHappenDetailGrid[$i][$j]) ; $k++){
				        if($eventHappenDetailGrid[$i][$j][$k]["IsAllDay"] != 1)
		        			$numOfEvent++;
	        		}
	        	}
		        
	        	# Get style
		        if (date("d/m D", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){ 
		        	$cellClassName = "portal_cal_week_today";
	        	} else if (date("d/m D", time()) == $current_week[$j]) {
		        	$cellClassName = "portal_cal_week_today";
		        } else if ($i == $this->workingHourEnd-1){
		        	$cellClassName = "portal_cal_week_day";
	        	} else {
		        	$cellClassName = "portal_cal_week_day";
	        	}
	        	$ts = strtotime($current_week_date[$j]);
	        	
		        $calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName'>";
		        if($numOfEvent > 0){
					$calendar .= '<a href="'.$PathRelative.'src/module/calendar/index.php?time='.$ts.'&calViewPeriod=daily" class="portal_week_event_item_whole">';
					$calendar .= count($eventHappenDetailGrid[$i][$j]).' ';
					$calendar .= '<img src="'.$SYS_CONFIG['Image']['Abs'].'icon_event_s.gif" width="13" height="14" border="0" align="absmiddle" class="cal_event_blue">';
					$calendar .= '</a>';
				} else {
					$calendar .= "&nbsp;";
				}
		        $calendar .= "</td>";
	        }
	        # Row span is needed for event with duration more than 1 hour
	        else if (empty($curLastIntersect) && !empty($curNextIntersect)) {
		        # Handle the case of intersection among several events shown in UI
		        # Condition: 
		        #	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
		        #	Check The other Events duration beginning within the hour in the above range
		        # 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
		        # 	Because of Event 2 is set within the range of Event 1,
		        # 	the rowspan should be updated to 3 from 2.
		        if($rowSpan[$i][$j] > 1){
		        	$orig_span_range = $i + $rowSpan[$i][$j];
		        	for($m=$i+1 ; $m<=$orig_span_range ; $m++){
			        	$other_span_range = $m + $rowSpan[$m][$j];
			        	if($other_span_range > $orig_span_range){
			        		$new_span = $rowSpan[$i][$j] + ($other_span_range - $orig_span_range);
			        		$rowSpan[$i][$j] = $new_span;
		        		}
	        		}
	        	}
		        
		        # Get Event Number
	        	for ($k=1; $k<$rowSpan[$i][$j]; $k++) {
			        $eventHappenGrid[$i][$j] = $this->array_union($eventHappenGrid[$i][$j], $eventHappenGrid[$i+$k][$j]);
			        $eventHappenDetailGrid[$i][$j] = array_merge($eventHappenDetailGrid[$i][$j], $eventHappenDetailGrid[$i+$k][$j]);
		        }
		        $tempArray = array();
		        $exist = array();
		        for ($l=0; $l<sizeof($eventHappenDetailGrid[$i][$j]); $l++) {
			        if (in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $eventHappenGrid[$i][$j]) && !in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $exist)) {
			        	$tempArray[] = $eventHappenDetailGrid[$i][$j][$l];
			        	$exist[] = $eventHappenDetailGrid[$i][$j][$l]["EventID"];
		        	}
		        }
	        	$numOfEvent = count($tempArray);
	        	
		        # Get Style
		        if (date("d/m D", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){
		        	$cellClassName = "portal_cal_week_today";
	        	} else if (date("d/m D", time()) == $current_week[$j]){
		        	$cellClassName = "portal_cal_week_today";
	        	} else if ($i == $this->workingHourEnd-1){
		        	$cellClassName = "portal_cal_week_day";
	        	} else {
		        	$cellClassName = "portal_cal_week_day";
	        	}
		        $ts = strtotime($current_week_date[$j]);
	        	
		        $calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName' rowspan='".max(1,$rowSpan[$i][$j])."'>";
		        if($numOfEvent > 0){
			        $calendar .= "<div class='portal_week_event_item' style='position:relative'>";
					$calendar .= "<a href='".$PathRelative."src/module/calendar/index.php?time=".$ts."&calViewPeriod=daily'>";
					$calendar .= $numOfEvent.' ';
					$calendar .= "<img src='".$SYS_CONFIG['Image']['Abs']."icon_event_s.gif' width='13' height='14' border='0' align='absmiddle' class='cal_event_blue'>";
					for($m=0 ; $m<$rowSpan[$i][$j] ; $m++){ $calendar .= '<br>';}
					$calendar .= "</a>";
					$calendar .= "</div>";
				} else {
					$calendar .= "&nbsp;";
				}
		        $calendar .= "</td>";
	        }
        }
        $calendar .= "</tr>";
      }
      $calendar .= "</tbody></table>";
      return $calendar;
    }
	
  
    function generateBriefAgenda($startDate, $endDate, $response){
	    global $coming_events, $today_events;
	    $returnStr = '';
	    
	  	# Prepare Today's Agenda
	  	# Set Today Date
		$start = $startDate;
		$end = $endDate;
		
		# Get the today's event array for quicker access
		# TimeStamp of Upper Bound should be minus 1 to exclude the event which is hold on 00:00:00
		$today_lower_bound = $this->sqlDatetimeToTimestamp($start." 00:00:00");
		$today_upper_bound = $this->sqlDatetimeToTimestamp($end." 00:00:00") - 1;
		
		# Differ from a Offset 1970101
		$today_lower_bound += $this->offsetTimeStamp;
		$today_upper_bound += $this->offsetTimeStamp;
		
		$today_events = $this->query_events($today_lower_bound, $today_upper_bound);
		
		# Prepare the next day's Agenda
		# Set Coming Day as the next day
		$start = $end;
		$end = date("Y-m-d", time(date("Y-m-d")." 00:00:00") + (24*60*60*2));
				
		# Get the coming event array for quicker access
		# TimeStamp of Upper Bound should be minus 1 to exclude the event which is hold on 00:00:00
		$coming_lower_bound = $this->sqlDatetimeToTimestamp($start." 00:00:00");
		$coming_upper_bound = $this->sqlDatetimeToTimestamp($end." 00:00:00") - 1;
		
		# Differ from a Offset 1970101
		$coming_lower_bound += $this->offsetTimeStamp;
		$coming_upper_bound += $this->offsetTimeStamp;
		
		$coming_events = $this->query_events($coming_lower_bound, $coming_upper_bound);
	  
		$returnStr = $this->printBriefAgenda($startDate, $endDate, $response);
		
	  	return $returnStr;
    }
	
    function returnOwnerDefaultCalendar(){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		/*$sql  = "SELECT a.CalID, a.SyncURL, b.Access
				 FROM CALENDAR_CALENDAR AS a, CALENDAR_CALENDAR_VIEWER AS b 
				 WHERE
					b.UserID = $UserID AND 
				 	a.CalID = b.CalID AND 
				   (b.Access IS NOT NULL AND b.Access != '') AND 
				 	a.Owner = $UserID AND
					a.CalID in (
				 SELECT 
					MIN(a.CalID) as CalID
				 FROM 
				 	CALENDAR_CALENDAR AS a, CALENDAR_CALENDAR_VIEWER AS b 
				 WHERE 
				 	b.UserID = $UserID AND 
				 	a.CalID = b.CalID AND 
				   (b.Access IS NOT NULL AND b.Access != '') AND 
				 	a.Owner = $UserID 
				 GROUP BY a.Owner)";*/
			$sql  = "SELECT a.CalID, a.SyncURL, b.Access
				 FROM CALENDAR_CALENDAR AS a inner join CALENDAR_CALENDAR_VIEWER AS b
				on a.CalID = b.CalID
				 WHERE
					b.UserID = '$uid' AND 
				 	a.CalID = b.CalID AND 
				   (b.Access IS NOT NULL AND b.Access != '') AND 
				 	a.Owner = '$uid'
					GROUP BY a.Owner
					order by CalID ASC limit 1";
		$result = $this->returnVector($sql);
		
		return $result[0];
	}

	# If Input Array contains some Repeated / Duplicated Data with different ID, 
	# This function could extract all duplicated data & group into a Array with a unqiue ID
	# Input Structure : Array[$cnt_1]['element']
	# Output Structure: Array[$KeyValue][$cnt_2]['Element']
	function Return_Distinct_Object_Array($input, $MappingStr, $KeyValue=''){
		$returnArr = array();
		$KeyValue = ($KeyValue == '') ? $MappingStr : $KeyValue;
		
		if(count($input) > 0){
			for($i=0 ; $i<count($input) ; $i++){
				$start_id = $input[$i][$KeyValue];
				$tmpInfo = array();
		        $tmpInfo[] = $input[$i];
		        for($j=$i+1 ; $j<count($input) ; $j++){
		            if($input[$i][$MappingStr] != "" && $input[$i][$MappingStr] == $input[$j][$MappingStr]){
		            	$tmpInfo[] = $input[$j];
		            }else{
		            	break;
		            }
		        }
		        $i = $j - 1;
		        $returnArr[$start_id] = $tmpInfo;
	        }
        }
        return $returnArr;
	}	
	
	function Turn_To_Associated_Array($InputArr, $ValueName){
		$OutputArr = array();
		if(count($InputArr) > 0){
			for($i=0 ; $i<count($InputArr) ; $i++){
				$OutputArr[] = $InputArr[$i][$ValueName];
			}
		}
		return $OutputArr;
	}
	
	# ViewerPath should be in format: []:=:[]:=:[]:=:[] ...
	# 	Delimiter	:=:  
	# 	E.g. 		A:=:BID
	# 				A - level 1 Type, B - level 2 Type, ID - SubjectName / YearID / SchBasedGroupID
	function Get_Group_Viewer($viewerPath, $returnViewerID=true){
		global $SYS_CONFIG, $Lang;
		$viewerID = array();
		$path = array();
		$name = '';
		$return = ($returnViewID) ? array() : "";
		
		$Lang['email']['Roll_Group_Teacher'] = 'Roll-Group Teacher';
		
		if($viewerPath != ""){
			$path = explode(":=:", $viewerPath);
			
			$result = array();
			$numOfLevel = count($path);
			
			if( $numOfLevel == 1 || ($numOfLevel == 2 && strtoupper($path[1]) == "I") || 
			   ($numOfLevel == 2 && strtoupper($path[1]) == "C") || ($numOfLevel == 2 && strtoupper($path[1]) == "Y") ){
				# Cases - T , N , B , T:=:I , T:=:C , T:=:Y 
				# Selection in Level 1 or Level 2 without Any ID provided behind
				if(strtoupper($path[0]) == "T" || strtoupper($path[0]) == "N"){
					# Teaching Staff or Non-teaching Staff
					if($returnViewerID){
						$Sql = "
							exec Search_StaffEmail 
							@Staff_Type='".strtoupper($path[0]).",', 
							@Roll_Group=',',					
							@School_Year =',',
							@House=',' ,
							@SubjectName=',' , 
							@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'";
					} else {
						if(strtoupper($path[0]) == "T"){
							$name = $Lang['email']['TeachingStaff'];
						} else if(strtoupper($path[0]) == "N"){
							$name = $Lang['email']['NonTeachingStaff'];
						}
					}
				} else if(strtoupper($path[0]) == "B"){
					# School-Based Group
					// handle by Wah
				}
			} else if($numOfLevel == 2){	
				# Cases - T:=:CID , T:=:YID , B:=:BID 
				# Selection in Level 2 with Corresponding ID (SubjectName, YearID)
				$type2_prefix = substr($path[1], 0, 1);
				$type2_suffix = substr($path[1], 1);
				
				if(strtoupper($path[0]) == "T"){
					if(strtoupper($type2_prefix) == "C"){
						# Teacher in Subjcet - Get Users From the SubjectCode
						$SubjectCode = $type2_suffix;
						if($returnViewerID){
							$Sql = " 
								exec Get_Staff_BySubject 
								@SubjectCode='".$SubjectCode."',
								@TTPeriod='".$_SESSION['SSV_SCHOOLYEAR']."'";
						} else {
							$Sql = "
								SELECT Distinct FullName FROM MAZE_SU 
								INNER JOIN MAZE_STMA ON MAZE_SU.SUKey = MAZE_STMA.MKey 
								WHERE MAZE_SU.SUKey = '$SubjectCode'";
							$result = $this->returnVector($Sql);
							$name = $result[0];
						}	
					} else if(strtoupper($type2_prefix) == "Y"){
						# Teachers in School-Year
						$SchoolYear = $type2_suffix;
						if($returnViewerID){
							$Sql = "
								exec Search_StaffEmail 
								@Staff_Type=',', 
								@Roll_Group=',',					
								@School_Year ='".$SchoolYear.",',
								@House=',' ,
								@SubjectName=',' , 
								@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'";
						} else {
							$name = $SchoolYear.' (School Year - '.$Lang['email']['TeachingStaff'].')';
						}
					}
				} else if(strtoupper($path[0]) == "B"){
					if(strtoupper($type2_prefix) == "B"){
						$SchoolGroup = $type2_suffix;
						if($returnViewerID){
							# Teacher in a Group of School-Based Group
							$Sql =  	"
								SELECT
									b.UserName, b.UserEmail, b.MailUserID AS UserID 
								FROM
									MAIL_ADDRESS_MAPPING a,
									MAIL_ADDRESS_USER b
								WHERE
									a.MailUserID = b.MailUserID
									AND a.MailGroupID = '".$SchoolGroup."'	 
							";
						} else {
							$name = ' ('.$Lang['email']['SchoolBasedGroup'].')';
						}
					}
				}
			} else if($numOfLevel == 3){
				# Cases - T:=:YID:=:A , T:=:YID:=:R
				$type2_prefix = substr($path[1], 0, 1);
				$type2_suffix = substr($path[1], 1);
				
				if(strtoupper($path[0]) == "T"){
					# Teachers 
					if(strtoupper($type2_prefix) == "Y"){
						if(strtoupper($path[2]) == "A"){
							# All Teacher in School-Year
							$SchoolYear = $type2_suffix;
							if($returnViewerID){
								$Sql = "
									exec Search_StaffEmail 
									@Staff_Type=',', 
									@Roll_Group=',',					
									@School_Year ='".$SchoolYear.",',
									@House=',' ,
									@SubjectName=',' , 
									@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'";
							} else {
								$name = $SchoolYear.' (School Year - '.$Lang['email']['TeachingStaff'].')';
							}
						} else if(strtoupper($path[2]) == "R"){
							# Roll Group Teacher in School-Year
							$SchoolYear = $type2_suffix;
							if($returnViewerID){
								$Sql = "
									exec Search_StaffEmail 
									@Staff_Type=',', 
									@Roll_Group=',',
									@School_Year =',',
									@House=',' ,
									@SubjectName=',' , 
									@Teach_Roll_Group=',' , 
									@Roll_Group_Year= '".$SchoolYear.",',
									@TTPeriod= '".$_SESSION['SSV_SCHOOLYEAR'].",'";
							} else {
								$name = $SchoolYear." (".$Lang['email']['Roll_Group_Teacher'].")";
							}
						}
					}
				}
			}
			
			if($returnViewerID){
				$result = $this->returnArray($Sql);
				# Prepare ViewID
				if(count($result) > 0){
					for($i=0 ; $i<count($result) ; $i++){
						$viewerID[] = $result[$i]['UserID'];
					}
				}
				$return = $viewerID;
			} else {
				$return = $name;
			}
		}
		return $return;
	}
	
	# Get all DBA users in an array / Check whehter the login user is DBAs or not
	function Retrieve_DBA_User($isCheck=1){
		global $UserID, $SYS_CONFIG;
		
		$uid = $this->getCurrentUserID();
		
		$result = array();
		$sql = "SELECT
					UserID
				FROM
					ROLE_MEMBERS AS a
				INNER JOIN ROLE_LOCAL AS b ON
					b.LocalRoleID = a.RoleID
				WHERE
					b.RoleNameEng = ".$SYS_CONFIG['Role']['SysDBARoleName'];
		$result = $this->returnVector($sql);
		if($isCheck){
			return (in_array($uid, $result)) ? true : false;
		} else {
			return $result;
		}
	}

	# Get all related event entries NOT including Involved Events
	# Input Format of startDateTime & endDateTime : "yyyy-mm-dd hh:mm:ss"
	# Case 1: when own=false & other=false, retrieve own created events & calendar shared events
	# Case 2: when own=true  & other=false, retrieve own created events
	# Case 3: when own=false & other=true , retrieve calendar shared events
	# $associArrOnly is used for adding a new parameter to the result array for later use
	function Get_Viewable_Event_Entry($startDateTime, $endDateTime, $own=false, $other=false, $associArrOnly=false){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$result = array();
		
		if($startDateTime != "" && $endDateTime != ""){
			if($own == true){
				$sub_sql = "c.UserID = b.Owner AND ";
			} else {
				if($other == true){
					$sub_sql = "c.UserID <> b.Owner AND ";
				}
			}

			$sql = "SELECT 
						DISTINCT a.EventID, a.Title, a.EventDate, a.CalID, a.IsImportant, a.IsAllDay, 
						b.Owner, b.CalID, CASE WHEN b.CalType = 2 THEN NULL ELSE b.CalType END AS CalType , 
						c.UserID, c.Color, c.Visible 
					FROM CALENDAR_EVENT_ENTRY AS a 
					INNER JOIN CALENDAR_CALENDAR AS b ON 
						b.CalID = a.CalID 
					INNER JOIN CALENDAR_CALENDAR_VIEWER AS c ON 
						".$sub_sql."
						c.CalID = b.CalID AND 
						(c.Access IS NOT NULL AND c.Access != '')
					WHERE 
						c.UserID = '$uid' AND 
						a.EventDate >= '$startDateTime' AND 
						a.EventDate < '$endDateTime' 
					ORDER BY 
						a.EventDate";
			$result = $this->returnArray($sql);

			if($associArrOnly && count($result) > 0){
				for($i=0 ; $i<count($result) ; $i++){
					$result[$i]['isInvolvedEvt'] = false;
				}
			}
		}
		return $result;
	}

	# Get all user related Involved Events ONLY
	# $associArrOnly is used for adding a new parameter to the result array for later use
	function Get_Viewable_Involved_Event_Entry($startDateTime, $endDateTime, $associArrOnly=false){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$result = array();
		
		if($startDateTime != "" && $endDateTime != ""){
			$sql = "SELECT 
						DISTINCT a.EventID, a.Title, a.EventDate, a.CalID, a.IsImportant, a.IsAllDay, 
						0 AS Owner, a.CalID, '' AS CalType, 
						0 AS UserID, '' AS Color, 1 As Visible 
					FROM CALENDAR_EVENT_ENTRY AS a 
					INNER JOIN CALENDAR_EVENT_USER AS b ON 
						b.EventID = a.EventID AND 
						b.Access = 'R' AND 
						b.Status IN ('A', 'M') 
					WHERE 
						b.UserID = '$uid' AND 
						a.EventDate >= '$startDateTime' AND 
						a.EventDate < '$endDateTime' 
					ORDER BY 
						a.EventDate";
			$result = $this->returnArray($sql);

			if($associArrOnly && count($result) > 0){
				for($i=0 ; $i<count($result) ; $i++){
					$result[$i]['isInvolvedEvt'] = true;
				}
			}
		}
		return $result;
	}
	
	# Get all user related Foundation Events ONLY
	# $associArrOnly is used for adding a new parameter to the result array for later use
	function Get_Viewable_Foundation_Event_Entry($startDateTime, $endDateTime, $associArrOnly=false){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$result = array();
		$isESF = true;
		
		if($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV'){
			$db = new database(false, false, true);
			$isESF = false;
		}
		
		if($startDateTime != "" && $endDateTime != ""){
			$sub_sql = ($_SESSION['SSV_USER_TYPE'] == 'T') ? "AND b.CalSharedType IN ('T', 'A') " : (($_SESSION['SSV_USER_TYPE'] == 'S') ? "AND b.CalSharedType IN ('S', 'A') " : "");
			$sub_sql .= ($isESF == true) ? "AND b.Owner != '$uid' "  : "";
			
			$sql = "SELECT 
						DISTINCT a.EventID, a.Title, a.EventDate, a.CalID, a.IsImportant, a.IsAllDay, 
						b.Owner, b.CalID, b.CalType, 
						c.UserID, c.Color, c.Visible 
					FROM CALENDAR_EVENT_ENTRY AS a 
					INNER JOIN CALENDAR_CALENDAR AS b ON 
						b.CalID = a.CalID AND b.CalType = 2 
						".$sub_sql."
					INNER JOIN CALENDAR_CALENDAR_VIEWER AS c ON 
						c.CalID = b.CalID 
					WHERE 
						a.EventDate >= '$startDateTime' AND 
						a.EventDate < '$endDateTime' 
					ORDER BY 
						a.EventDate";
			$result = ($isESF == true) ? $this->returnArray($sql) : $db->returnArray($sql);

			if($associArrOnly && count($result) > 0){
				for($i=0 ; $i<count($result) ; $i++){
					$result[$i]['isInvolvedEvt'] = false;
				}
			}
		}
		return $result;
	}

	# new approach with ordering by EventDateTime between user related evetns & involved events
	function Display_More_Event_Entry_List_By_EventDateTime($EventListArr, $eventDate){
		global $ImagePathAbs, $lui, $UserID;
		$str = '';

		$uid = $this->getCurrentUserID();

		if(count($EventListArr) > 0){
			foreach($EventListArr as $key => $data){
				if ($key > $eventDate)
					continue;
				
				for($i=0 ; $i<count($data) ; $i++){
					$isAllDay = $data[$i]["IsAllDay"];
					$display  = ($data[$i]["Visible"] == "0") ? "none" : "block";
					$eventTs  = $this->sqlDatetimeToTimestamp($data[$i]["EventDate"]);
					$CalType  = trim($data[$i]['CalType']);

					if ($this->systemSettings["TimeFormat"] == 12) {
						$eventTime = date("g:ia", $eventTs);
					} else if ($this->systemSettings["TimeFormat"] == 24) {
						$eventTime = date("G:i", $eventTs);
					}
					
					# Check the type of the event 
					$isInvolvedEvt = $data[$i]['isInvolvedEvt'];
					$isFoundation = ($CalType == 2) ? true : false;
					$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
					
					# Get Current School DB Name
					$current_dbname = $this->db;
					$central_dbname = $current_dbname; // eclass
					# Check whether this is Foundation Calendar with Non-ESF School Site & Get Centralized Site DB Name
				/*	$isFoundationCalNonESF = ($isFoundation && ($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV')) ? true : false;
					if($isFoundationCalNonESF){
						$db = new database(false, false, true);
						$central_dbname = $db->db;
					} else {
						$central_dbname = $this->db;
					}*/
					
					# Get Personal Note Detail
					$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
					$sql = "SELECT 
								pn.PersonalNote
							FROM 
								CALENDAR_EVENT_ENTRY As e  
							LEFT OUTER JOIN Calendar_Event_Personal_Note As pn ON
								pn.EventID = e.EventID AND 
								pn.UserID = '$uid' 
							WHERE 
								e.EventID = '".$data[$i]["EventID"]."'
								$eclassConstraint";
					$TempResult = $this->returnVector($sql);
					$HasPersonalNote = (trim($TempResult[0]) != "");

					# Preparation
					/*if($isOtherCalType)						# other caltype event : CPD, CAS
					{		
						$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
						$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
						$otherCalTypeCalVisible = $this->returnUserPref($UserID, $cal_type_name."CalVisible");
						$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
						
						$ColorID = $this->Get_Calendar_ColorID($data[$i]["Color"]);
						$isInvolve = 0;
						$div_id	  = $cal_type_name."EventMore".$data[$i]["EventID"];
						$div_class = $cal_type_name;
					} 
					else if($isFoundation)					# foundation event
					{
						$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
						$display = ($foundationCalVisible == "0") ? "none" : "block";
						
						$ColorID = $this->Get_Calendar_ColorID($data[$i]["Color"]);
						$isInvolve = 0;
						$div_id	  = "foundationEventMore".$data[$i]["EventID"];
						$div_class = "foundation";
					}
					else*/ if($isInvolvedEvt == true)			# involved events
					{
						$ColorID = $this->involveCalColorID;
						$isInvolve = 1;
						$div_id	   = "involveEventMore".$data[$i]["EventID"];
						$div_class = "involve";
					} 
					else									# personal events
					{	
						$ColorID = $this->Get_Calendar_ColorID($data[$i]["Color"]);
						$isInvolve = 0;
						$div_id    = "eventMore".$data[$i]["EventID"];
						$div_class = "cal-".$data[$i]["CalID"];
					}

					# Get Event Display Title
					$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
					$CalEventTitle .= $data[$i]["Title"];
					$CalEventTitle  = $this->formatEventTitle($CalEventTitle, (int)$data[$i]["IsImportant"], false, 18);
					if ($_SESSION['UserID'] == $data[$i]["UserID"] && $HasPersonalNote){
						$CalEventTitle .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					}
					
					/*if($isOtherCalType){
						# Set Style for other CalType event only
						$CalEventTitle = $this->Set_Style_Other_CalType_Event($CalEventTitle, $data[$i]);
						
						if($CalType == ENROL_CPD_CALENDAR || $CalType == MY_CPD_CALENDAR){
							$onclick = "jGet_Other_CalType_Event_More_Info(".$data[$i]["EventID"].", '".$data[$i]["ProgrammeID"]."', 'monthly', '".$CalType."')";
						} 
					} else {*/
						$onclick = "jGet_Event_More_Info(".$data[$i]["EventID"].", 'monthly', '".$isInvolve."', '".$CalType."')";
					//}
					if (empty($ColorID)){
						//if (empty($data[$i]["schoolCode"])){
						//debug_r($data[$i]);
							$ColorID = $this->involveCalColorID; 						
						/*}
						else{ 
						//debug_r($data[$i]);
							$div_id    = "eventMore".$data[$i]["EventID"]."-".trim($data[$i]["schoolCode"]); 
							$onclick = "jGet_Event_More_Info('".$data[$i]["EventID"]."-".trim($data[$i]["schoolCode"])."', 'monthly', '".$isInvolve."', '4')";
							$ColorID = $this->otherCalColorID;
						}*/
					}

					# Content 
					$str .= "<div class=\"cal_month_event_item\">";
					$str .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;cursor:pointer;\" ";
					$str .= "onclick=\"".$onclick."\" ";
					$str .= "id=\"".$div_id."\" class=\"".$div_class."\">";
					$str .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\">";
					$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$str .= $CalEventTitle;
					$str .= "</a>";
					$str .= "</div>";
					$str .= "</div>";
				}
			}
		}
		return $str;
	}

	# old approach without ordering by EventDateTime between user related events & involved events
	function Display_More_Event_Entry_List($EventListArr, $isInvolveEvt=false){
		global $ImagePathAbs;
		$CalEventDiv = '';

		if(count($EventListArr) > 0){
			for($i=0 ; $i<count($EventListArr) ; $i++){
				$isAllDay = $EventListArr[$i]["IsAllDay"];
				$display  = ($EventListArr[$i]["Visible"] == "0") ? "none" : "block";
				$eventTs  = $this->sqlDatetimeToTimestamp($EventListArr[$i]["EventDate"]);

				if ($this->systemSettings["TimeFormat"] == 12) {
					$eventTime = date("g:ia", $eventTs);
				} else if ($this->systemSettings["TimeFormat"] == 24) {
					$eventTime = date("G:i", $eventTs);
				}

				# Get Corresponding Color of each Event
				if($isInvolveEvt == true){
					# all user involved events
					$ColorID = $this->involveCalColorID;
				} else {	
					# all user related events excluding involved event
					$ColorID = $this->Get_Calendar_ColorID($EventListArr[$i]["Color"]);
				}

				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= $EventListArr[$i]["Title"];
				$CalEventTitle  = $this->formatEventTitle($CalEventTitle, (int)$EventListArr[$i]["IsImportant"], false, 23);

				# Preparation
				if($isInvolveEvt == true){
					$isInvolve = 1;
					$div_id = "involveEventMore".$EventListArr[$i]["EventID"];
					$div_class = "involve";
				} else {
					$isInvolve = 0;
					$div_id = "eventMore".$EventListArr[$i]["EventID"];
					$div_class = "cal-".$EventListArr[$i]["CalID"];
				}

				# Content 
				$CalEventDiv .= "<div class=\"cal_month_event_item\">";
				$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin:0px 0px 0px 0px;padding:0px;cursor:pointer;\" ";
				$CalEventDiv .= "onclick=\"jGet_Event_More_Info(".$EventListArr[$i]["EventID"].", 'monthly', '".$isInvolve."')\" ";
				$CalEventDiv .= "id=\"".$div_id."\" class=\"".$div_class."\">";
				$CalEventDiv .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\">";
				$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
				$CalEventDiv .= $CalEventTitle;
				$CalEventDiv .= "</a>";
				$CalEventDiv .= "</div>";
				$CalEventDiv .= "</div>";
			}
		}
		return $CalEventDiv;
	}
	
	function Insert_Personal_Note($EventID, $PersonalNote, $CalType='', $SchoolCode='') {
		
		$uid = $this->getCurrentUserID();
		
	$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
		$sql = "SELECT COUNT(1) AS 'Exists' FROM CALENDAR_EVENT_PERSONAL_NOTE 
				WHERE EventID = '".$EventID."' AND UserID = '".$uid."' ".$eclassConstraint;
		//$sql .= ($CalType == 2) ? " AND CalType = 2 " : "";
		
		/*switch ($CalType){
			case 2: $sql .= " AND CalType = 2 "; break;
			case 3: $sql .= " AND CalType = 3 "; break;
			case 4: $sql .= " AND CalType = 4 "; break;
			default: $sql .= " AND CalType <> 3 AND CalType <> 4  "; break;
		};*/
		
		$Exists = $this->returnVector($sql);
		$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
		if ($Exists[0] > 0) {
			$sql = "UPDATE CALENDAR_EVENT_PERSONAL_NOTE SET 
						PersonalNote = '".$this->Get_Safe_Sql_Query($PersonalNote)."', 
						LastUpdateDate = NOW() 
					WHERE 
						EventID = '".$EventID."' AND 
						UserID = '".$uid."'
						$eclassConstraint";
			//$sql .= ($CalType == 2) ? " AND CalType = 2 " : "";
			/*switch ($CalType){
				case 2: $sql .= " AND CalType = 2 "; break;
				case 3: $sql .= " AND CalType = 3 "; break;
				case 4: $sql .= " AND CalType = 4 "; break;
				default: $sql .= " AND CalType <> 3 AND CalType <> 4  "; break;
			};*/
		}
		else {
			$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE 
						(EventID, UserID, PersonalNote, CalType, CreateDate) 
					VALUES (
						'".$EventID."', 
						'".$uid."', 
						'".$this->Get_Safe_Sql_Query($PersonalNote)."', 
						'".$CalType."', 
						NOW()
					)";
		}
		
		return $this->db_db_query($sql);
	}
	
	# Handle the case of intersection among several events shown in UI
    # Condition: 
    #	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
    #	Check The other Events duration beginning within the hour in the above range
    # 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
    # 	Because of Event 2 is set within the range of Event 1,
    # 	the rowspan should be updated to 3 from 2.
    /*
	function Get_Rowspan_Event_Number($cur_hour, $cur_span_num){
		$new_span = $cur_span_num;
        if($cur_span_num > 1){
        	$orig_span_range = $i + $cur_span_num;
        	for($j=$i+1 ; $j<=$orig_span_range ; $j++){
	        	$other_span_range = $j + $cur_span_num;
	        	if($other_span_range > $orig_span_range){
	        		$new_span = $cur_span_num + ($other_span_range - $orig_span_range);
        		} 
    		}
    	} 
		return $new_span;
	}*/
		
	/*  *** Weekly View ***  */
	## New approach using Query_User_Related_Events() & Query_User_Foundation_Events() function
	## added on 23 Feb 2009
	function Generate_Weekly_Calendar($year, $month, $day, $print=false) {
	    global $iCalendar_NewEvent_IsAllDay;
	    global $events;
	    
// 	    debug_pr($events);
	    $allDayConEvt = Array();

	    $current_week_date = $this->get_week($year, $month, $day, "Y-m-d");
// 	    debug_pr($current_week_date);
	    
		# $i -> hours ; $j -> Date Index
	    for ($i=0; $i<24; $i++) {	
		    $start_time = (($i<10)?"0$i":"$i").":00";
        	$end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
        	
	        for ($j=0; $j<7; $j++) {				
		        if (!isset($eventHappenGrid[$i][$j])) {
		        	$eventHappenGrid[$i][$j] = array();
	        	}
	        	if (!isset($eventHappenDetailGrid[$i][$j])) {
		        	$eventHappenDetailGrid[$i][$j] = array();
	        	}
		        if (!isset($eventRowSpan[$i][$j])) {
		        	$eventRowSpan[$i][$j] = 0;
	        	}
		        $start_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $start_time");
		        $end_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $end_time");
// 		        debug_pr($start_ts . " || " . $end_ts);

		        //echo $current_week_date[$j]." ".$start_time." ::: ".$current_week_date[$j]." ".$end_time."<BR>";
		        //echo $start_ts ." ::: ".$end_ts."<BR>";
		        # Handling the timeslot from 23:00 to 00:00(next day) since $this->sqlDatetimeToTimestamp("24:00") return false;
	     		if($i+1==24) $end_ts = $start_ts + 60*60;
	     		 
				//echo $current_week_date[$j].' : '.sizeof($events[$current_week_date[$j]]).' <br>';
// 	     		debug_pr($current_week_date[$j]);
				if(sizeof($events[$current_week_date[$j]]) > 0){
					$evt = $events[$current_week_date[$j]];
// 					debug_pr($evt);
					for ($k=0; $k<sizeof($evt) ; $k++){
					//for ($k=0; $k<sizeof($events); $k++) {
									
						if (date("Y-m-d",strtotime($evt[$k]["EventDate"]))< date("Y-m-d",strtotime($current_week_date[0]))){						    
							$curEventTs = strtotime(date("Y-m-d ",strtotime($current_week_date[0])).date("H:i:s",strtotime($evt[$k]["EventDate"])));						
						}
						else
							$curEventTs = $this->sqlDatetimeToTimestamp($evt[$k]["EventDate"]);
// 							debug_pr($curEventTs);
						$endDay = date("Y-m-d",strtotime("+".$evt[$k]['Duration']." minute",strtotime($evt[$k]["EventDate"])));			
						$startDay = date('Y-m-d',strtotime($evt[$k]["EventDate"]));
						if ($evt[$k]['Duration']>=1440 || $endDay != $startDay){
							if (empty($allDayConEvt[$j]))
								$allDayConEvt[$j][] = $evt[$k];
							else{
								#check duplicate
								$isDuplicate = false;
								foreach($allDayConEvt[$j] as $checkDuplicate){
									if ($evt[$k]["EventID"] == $checkDuplicate["EventID"]){
										$isDuplicate = true;
										break;
									}
								}
								if (!$isDuplicate)
									$allDayConEvt[$j][] = $evt[$k];
							}
							
						}elseif ($curEventTs >= $start_ts && $curEventTs < $end_ts) {
							$numOfHours = ceil($evt[$k]["Duration"]/60);
							
							if (date("i",$curEventTs) != "00")
								$numOfHours++;
							
							if ($numOfHours <= 1) {
								$eventHappenGrid[$i][$j][] = $evt[$k]["EventID"];
								$eventHappenDetailGrid[$i][$j][] = $evt[$k];
								### Bug Fix on 2008-01-17 ###
								# When multiple events start in the same cell, do not reset the
								# row span to 0 if there already exist a value
								if (!isset($rowSpan[$i][$j]) || empty($rowSpan[$i][$j])){								    
									$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? 0 : $rowSpan[$i][$j];
								}
								if (!isset($eventHappenGrid[$i+1][$j])) {
									$eventHappenGrid[$i+1][$j] = array();
									$eventHappenDetailGrid[$i+1][$j] = array();
								}
							} else {
								$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? $numOfHours : $rowSpan[$i][$j];
// 								debug($rowSpan[$i][$j])/;
								for ($l=0; $l<$numOfHours; $l++) {
									$eventHappenGrid[$i+$l][$j][] = $evt[$k]["EventID"];
									$eventHappenDetailGrid[$i+$l][$j][] = $evt[$k];
								}
							}
						} else {
							if (!isset($eventHappenGrid[$i+1][$j])) {
								$eventHappenGrid[$i+1][$j] = array();
							}
							if (!isset($eventHappenDetailGrid[$i+1][$j])) {
								$eventHappenDetailGrid[$i+1][$j] = array();
							}
						}
						
					} # End for
				} # End if
	       	}
       	}
	    
      	$current_week = $this->get_week($year, $month, $day, "y/m/d (D)");
//       	debug_pr($current_week);
    	
    	$calendar .= "
		<input type='hidden' id='startDay' name='startDay' value='".date('j',strtotime($current_week_date[0]))."'>
		<input type='hidden' id='maxDays' name='maxDays' value='".date('j',strtotime($current_week_date[6]))."'>
		<input type='hidden' id='monthEnd' name='monthEnd' value='".date('t',strtotime($current_week_date[0]))."'> 
		<table cellspacing='0' cellspacing='0' id='weekly'>";	    
     	$calendar .= "<colgroup>
	      				<col />
	      				<col class='Sun' />
						<col class='Mon' />
						<col class='Tue' />
						<col class='Wed' />
						<col class='Thu' />
						<col class='Fri' />
						<col class='Sat' />
					  </colgroup>";
		$calendar .= "<thead><tr>";
		$calendar .= "<td class='top_empty'>&nbsp;</td>";
		
		for($i=0; $i<7; $i++) {
			# apply different class for TODAY
			if (date("y/m/d (D)", time()) == $current_week[$i]){
				$calendar .= "<th class='".($print?"weekTitlePrint":"weekTitleToday weekTitleTextToday")."'>$current_week[$i]</th>";
		 	} else {
				$calendar .= "<th class='".($print?"weekTitlePrint":"weekTitle weekTitleText")."'>$current_week[$i]</th>";
			}
		}
		
		$calendar .= "</tr></thead>";
		$calendar .= "<tbody>";
		
		# Top cells for all day event
		$calendar .= "<tr>"; 
		$calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$iCalendar_NewEvent_IsAllDay</td>";
		$conEvent = Array();
		
		
			
		for ($i=0; $i<7; $i++) { 
		 
			if (date("y/m/d (D)", time()) == $current_week[$i]){
				$calendar .= "<td id='$current_week_date[$i]' class='".($print?"weekCellPrint":"weekFirstCellToday")."'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"weekFirstCellToday\"'").">";
			} else {
				$calendar .= "<td id='$current_week_date[$i]' class='".($print?"weekCellPrint":"weekFirstCell")."'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"weekFirstCell\"'").">";
			}
			$calendar .= '<div id="day-'.date('j',strtotime($current_week_date[$i])).'">';
			
			if (sizeof($allDayConEvt) > 0) {
				if ($print){
					//$calendar .= $this->Print_Weekly_Event_Block($eventHappenDetailGrid[0][$i], true);
					$calendar .= $this->Print_Weekly_AllDay_Event(strtotime($current_week_date[$i]), $allDayConEvt[$i],$conEvent,true);
				} else {
					//$calendar .= $this->Print_Weekly_Event_Block($eventHappenDetailGrid[0][$i]);
					$calendar .= $this->Print_Weekly_AllDay_Event(strtotime($current_week_date[$i]), $allDayConEvt[$i],$conEvent);
				}
				# remove all day events from the array
				/*$sizeOfEventHappenDetailGrid = sizeof($eventHappenDetailGrid[0][$i]);
				for($j=0; $j<$sizeOfEventHappenDetailGrid ; $j++) {
					if (trim($eventHappenDetailGrid[0][$i][$j]["IsAllDay"]) == "1"){
						unset($eventHappenDetailGrid[0][$i][$j]);
					}
				}
				sort($eventHappenDetailGrid[0][$i]);*/
			} else {
				$calendar .= "&nbsp";
			}
			$calendar .= "</div>";
			$calendar .= "</td>";
		}
		$calendar .= "</tr>";
		
// 		debug_pr($this->workingHourStart . " || " . $this->workingHourEnd);
		
      	for ($i=$this->workingHourStart; $i<$this->workingHourEnd; $i++) {
	        $calendar .= "<tr>";
	        $start_time = sprintf("%02d",$i).":00";
	        $end_time = sprintf("%02d",$i+1).":00";
	        
// 	        debug_pr($start_time . " || " . $end_time);
	        // 12 OR 24 time format
	        if ($this->systemSettings["TimeFormat"] == "12") {
	        	if ($i == 0) {
	        		$display_time = "12:00 am";
	        	} else if ($i == 12) {
	        		$display_time = "12:00 pm";
	        	} else if ($i < 12) {
	        		$display_time = sprintf("%02d",$i).":00 am";
	        	} else {
	        		$display_time = sprintf("%02d",$i-12).":00 pm";
	        	}
	        } else if ($this->systemSettings["TimeFormat"] == "24") {
	        	$display_time = $start_time;
	        }
	        $calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$display_time</td>";
	        for ($j=0; $j<7; $j++) {
		        if ($i==$this->workingHourStart)
		        	$curLastIntersect = array();
		        else
		        	$curLastIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i-1][$j]);
		        
		        if ($i==$this->workingHourEnd)
		        	$curNextIntersect = array();
		        else {
			        if ($eventHappenGrid[$i+1][$j])
		        		$curNextIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i+1][$j]);
	        	}
		        
				//$rowSpanSlot[$i][$j]
		        # No event in this cell OR the event only need one cell
		        if (empty($curLastIntersect) && empty($curNextIntersect)) {
			        if (date("y/m/d (D)", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCellToday";
		        	} else if (date("y/m/d (D)", time()) == $current_week[$j]){
			        	$cellClassName = $print?"weekCellPrint":"weekCellToday";
			        } else if ($i == $this->workingHourEnd-1) {
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCell";
		        	} else {
			        	$cellClassName = $print?"weekCellPrint" : "weekCell";
		        	}
			        $calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
			        if ($print)
			        	$calendar .= $this->Print_Weekly_Event_Block($eventHappenDetailGrid[$i][$j], true);
			        else
			       		$calendar .= $this->Print_Weekly_Event_Block($eventHappenDetailGrid[$i][$j]);
			        $calendar .= "&nbsp;</td>";
		        }
		        # Row span is needed for event with duration more than 1 hour
		        else if (empty($curLastIntersect) && !empty($curNextIntersect)) {
			        # Handle the case of intersection among several events shown in UI
			        # Condition: 
			        #	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
			        #	Check The other Events duration beginning within the hour in the above range
			        # 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
			        # 	Because of Event 2 is set within the range of Event 1,
			        # 	the rowspan should be updated to 3 from 2.
			        if($rowSpan[$i][$j] > 1){
			        	$orig_span_range = $i + $rowSpan[$i][$j];
			        	/*for($m=$i+1 ; $m<$orig_span_range ; $m++){
				        	$other_span_range = $m + $rowSpan[$m][$j];
				        	if($other_span_range > $orig_span_range){
				        		$new_span = $rowSpan[$i][$j] + ($other_span_range - $orig_span_range);
				        		$rowSpan[$i][$j] = $new_span;
			        		}
		        		}*/
						for($m=$i; $m<$orig_span_range ; $m++){
							$other_span_range = $m + $rowSpan[$m][$j];
				        	if($other_span_range > $orig_span_range){
				        		$new_span = $rowSpan[$i][$j] + ($other_span_range - $orig_span_range);
				        		$rowSpan[$i][$j] = $new_span;
								$orig_span_range = $i+$new_span;
								//$rowSpanSlot[$m][$j] = true;
			        		}
						}
						
		        	} 
			        
			        if (date("y/m/d (D)", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCellToday";
		        	} else if (date("y/m/d (D)", time()) == $current_week[$j]){
			        	$cellClassName = $print?"weekCellPrint":"weekCellToday";
		        	} else if ($i == $this->workingHourEnd-1){
			        	$cellClassName = $print?"weekLastCellPrint":"weekLastCell";
		          	} else {
			        	$cellClassName = $print?"weekCellPrint":"weekCell";
		        	}
			        //echo $i.", ".$j.", ".$rowSpan[$i][$j]."<br />";
			        $calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName' rowspan='".max(1,$rowSpan[$i][$j])."'".($print?"":" onmouseover='this.className=\"weekCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
			        for ($k=1; $k<$rowSpan[$i][$j]; $k++) {
				        $eventHappenGrid[$i][$j] = $this->array_union($eventHappenGrid[$i][$j], $eventHappenGrid[$i+$k][$j]);
				        $eventHappenDetailGrid[$i][$j] = array_merge($eventHappenDetailGrid[$i][$j], $eventHappenDetailGrid[$i+$k][$j]);
			        }
			        $tempArray = array();
			        $exist = array();
			        for ($l=0; $l<sizeof($eventHappenDetailGrid[$i][$j]); $l++) {
				        if (in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $eventHappenGrid[$i][$j]) && !in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $exist)) {
				        	$tempArray[] = $eventHappenDetailGrid[$i][$j][$l];
				        	$exist[] = $eventHappenDetailGrid[$i][$j][$l]["EventID"];
			        	}
			        }
			        $eventHappenDetailGrid[$i][$j] = $tempArray;
// 			        debug_pr( $eventHappenDetailGrid[$i][$j] );
			        if ($print)
			        	$calendar .= $this->Print_Weekly_Event_Block($eventHappenDetailGrid[$i][$j], true);
			       else
			       		$calendar .= $this->Print_Weekly_Event_Block($eventHappenDetailGrid[$i][$j]);
			        $calendar .= "&nbsp;</td>";
		        }
	        }
        	$calendar .= "</tr>";
      	}
      	$calendar .= "</tbody></table>";
      	return $calendar;
   	}
	
	
	
	
	function Print_Weekly_AllDay_Event($ts, $eventList, &$conEvent, $print=false){
		global $ImagePathAbs, $lui, $UserID;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, 
				$iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;

		$uid = $this->getCurrentUserID();

		$current_date = date("Y-m-d", $ts);

		if(sizeof($eventList) > 0){
			#sort by end date in descending order
			#if end date is the same sort by start time
			for ($i = 0; $i<sizeof($eventList)-1; $i++){
				for ($j = $i+1; $j <sizeof($eventList); $j++){	
					$iduration = intval($eventList[$i]["Duration"]);
					if ($eventList[$i]["IsAllDay"] == 1)
						$iduration = $iduration - 1440;
						
					$eventiTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
					$endiDate = strtotime("+".$iduration." minute",$eventiTs);
					$endiDateYmd = date("Y-m-d",$endiDate);
					
					$jduration = intval($eventList[$j]["Duration"]);
					if ($eventList[$j]["IsAllDay"] == 1)
						$jduration = $jduration - 1440;
					
					$eventjTs = $this->sqlDatetimeToTimestamp($eventList[$j]["EventDate"]);
					$endjDate = strtotime("+".$jduration." minute",$eventjTs);
					$endjDateYmd = date("Y-m-d",$endjDate);
					
					if($eventjTs < $eventiTs) {
						#sort by start time
						$temp = $eventList[$i];
						$eventList[$i] = $eventList[$j];
						$eventList[$j] = $temp;
					}else if($eventjTs == $eventiTs && $endiDate > $endjDate)
					{
						# same start date, sort by end date
						$temp = $eventList[$i];
						$eventList[$i] = $eventList[$j];
						$eventList[$j] = $temp;
					}
					/*
					// Commented on 2013-10-23 by Carlos - sort by start time only
					if ($endiDate < $endjDate && $endjDateYmd != $endiDateYmd){
						#sort by end date
						$temp = $eventList[$i];
						$eventList[$i] = $eventList[$j];
						$eventList[$j] = $temp;
					}
					else if ($endjDateYmd == $endiDateYmd && $eventjTs < $eventiTs){
						#sort by start time
						$temp = $eventList[$i];
						$eventList[$i] = $eventList[$j];
						$eventList[$j] = $temp;
					}
					*/
				}
			}
		}
		
		
		$evt = $eventList;
		
		
		$offset = 0;
		$eventWeek = date("w", $ts);
		
		#check for dummy slot
		if ($eventWeek != 0){
			//$temp = 0;
			if (count($conEvent[$eventWeek])>0){		
				//echo $current_date." ".count($conEvent[$current_date])."<br>";
				
				for($i = 0; $i < count($conEvent[$eventWeek]); $i++){
					
					$eventProperty = $conEvent[$eventWeek][$i];
					
					
					
						#for firefox or other browser
					if (empty($eventProperty["fake"])){
						# dummy block
						$CalEventDiv .= "<div id=\"".$eventProperty["id"]."\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$eventProperty["DivClass"]."\" style=\"display:".$eventProperty["display"].";font-size:15px;\">&nbsp;</div>";
						}
						else{
						#fake block
							if ($eventProperty["display"] == "block"){
								$isBlock = false;
								$prevDateTs = strtotime("-1 day", strtotime($current_date));
								$prevDayBlock = $conEvent[date("w",$prevDateTs)][$i];
							
								if (!empty($prevDayBlock) && preg_match('/^fake.*/', $prevDayBlock["id"])&&  $prevDayBlock["display"] == "none"){
								#check for the fake block of previous day
										$isBlock = false;
								}
								else{
									for ($j = $i+1; $j < count($conEvent[$eventWeek]); $j++){
										$next = $conEvent[$eventWeek][$j];	
										#check for dummy block in the bottom
										if (preg_match('/^dummy.*/', $next["id"]) && $next["display"] == "block"){
											$isBlock = true;
											break;
										}									 
									}		
								}
								$conEvent[$eventWeek][$i]["display"] = ($isBlock?"block":"none");
								$CalEventDiv .= "<div id=\"".$eventProperty["id"]."\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$eventProperty["DivClass"]."\" style=\"display:".($isBlock?"block":"none")."; font-size:15px;\">&nbsp;</div>";	
								if ($isBlock)
									$numVisible++;
							
							}
							else{
								$CalEventDiv .= "<div id=\"".$eventProperty["id"]."\" ".($eventProperty["isLimitEventNum"]?"name=\"date".date("j",$ts)."event[]\"":"")." class=\"".$eventProperty["DivClass"]."\" style=\"display: none;  font-size:15px;\">&nbsp;</div>";
							}
						}
				
					
					$offset++;
				}	
										
			}
			
		}
			
		//echo sizeof($eventList);
		
		for ($i = 0; $i<sizeof($eventList) ; $i++) {
			
			$CalType = trim($eventList[$i]['CalType']);
			$eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
			$eventDate = $eventDateTimePiece[0];
			$eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
			// 12 OR 24 time format
			if ($this->systemSettings["TimeFormat"] == 12) {
				$eventTime = date("h:i", $eventTs);
			} else if ($this->systemSettings["TimeFormat"] == 24) {
				$eventTime = date("G:i", $eventTs);
			}
			$isAllDay = $eventList[$i]["IsAllDay"];
			
			# Check the type of the event 
		//	$isFoundation = ($CalType == 2) ? true : false;
			$isInvolve = (trim($eventList[$i]["Permission"]) == "R") ? true : false;
		//	$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
			
			
			
			### Preparation
			/*if (!empty($eventList[$i]["schoolCode"])){ # other school invitation event
				$otherCalVisible = $this->returnUserPref($UserID, "otherInvitaionCalVisible");
				$display     = ($otherCalVisible == "0") ? "none": "block";
				$numVisible += ($otherCalVisible == "0") ? 0 : 1;
				
				# Get Corresponding Color of each Event
				$ColorID = $this->otherCalColorID;
				$Color	  = trim($this->otherCalColor);
				$DivID	  = "event".$eventList["EventID"].'-'.$eventList["schoolCode"];
				$DivClass = "cal-other";
				$TipName  = "eventToopTip";
			
			}
			elseif($isOtherCalType)			# other caltype event : CPD, CAS
			{		
				$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
				$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
				$otherCalTypeCalVisible = $this->returnUserPref($UserID, $cal_type_name."CalVisible");
				$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
				$numVisible += ($otherCalTypeCalVisible == "0") ? 0 : 1;
				
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = trim($eventList[$i]["Color"]);
				$DivID	  = $cal_type_name."Event".$eventList[$i]["EventID"];
				$DivClass = $cal_type_name;
				$TipName  = "eventToopTip";	
			} 
			else if($isFoundation)			# foundation event
			{
				//$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
				$foundationCalVisible = trim($eventList[$i]["Visible"]);
				$display = ($foundationCalVisible == "0") ? "none" : "block";
				
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = trim($eventList[$i]["Color"]);
				$DivID	  = "foundationEvent".$eventList[$i]["EventID"];
				$DivClass = "foundation-".$eventList[$i]["CalID"];
				$TipName  = "eventToopTip";	
			}
			else*/ 
			if (!empty($evt[$i]["CalType"])&&
			($evt[$i]["CalType"] == 1||$evt[$i]["CalType"] >3)){
			# external calendar
				$display     = ($evt[$i]["Visible"] == "0") ? "none": "block";
				$numVisible += ($evt[$i]["Visible"] == "0") ? 0 : 1;

				# Get Corresponding Color of each Event
				$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
				$Color	  = trim($evt[$i]["Color"]);
				$DivID	  = $evt[$i]["DivPrefix"].$evt[$i]["EventID"];
				$DivClass = $evt[$i]["DivClass"];
				$TipName  = "eventToopTip";
				$CalType = $evt[$i]["CalType"] ;
			}
			elseif($isInvolve)			# involved event
			{			
				$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
				$display	 = ($involveCalVisible == "0") ? "none" : "block";

				$ColorID  = $this->involveCalColorID;
				$Color    = $this->involveCalColor;
				$DivID    = "involveEvent".$eventList[$i]["EventID"];
				$DivClass = "involve";
				$TipName  = "involveEventToopTip";
			} 
			else 						# personal related events
			{				
				$display = ($eventList[$i]["Visible"] == "0") ? "none": "block";

				# Get Corresponding Color of each Event
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = $eventList[$i]["Color"];
				$DivID	  = "event".$eventList[$i]["EventID"];
				$DivClass = "cal-".$eventList[$i]["CalID"];
				$TipName  = "eventToopTip";					
			}
			$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
			$CalEventTitle .= intranet_undo_htmlspecialchars($eventList[$i]["Title"]) . $this->AppendOnwerName($eventList[$i]["UserID"]);
			
			//	if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 10);
			//	} else {
			//		$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 10);
			//	}
		/*	if(!$isFoundation || ($isFoundation && (
				($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV') || 
				(($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV') && $eventList[$i]['Owner'] != $UserID)
				) ) ){		    
				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= $eventList[$i]["Title"];
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 10);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 10);
				}
				if (!empty($eventList[$i]["schoolCode"])){
					#for other invitaion only
					$onclick = "jGet_Event_Info('".$eventList[$i]["EventID"]."-".$eventList[$i]["schoolCode"]."', 'monthly', '4')";
				}
				elseif($isOtherCalType){
					# Set Style for other CalType event only
					$CalEventTitle = $this->Set_Style_Other_CalType_Event($CalEventTitle, $eventList[$i]);
					
					if($CalType == ENROL_CPD_CALENDAR || $CalType == MY_CPD_CALENDAR){
						$onclick = "jGet_Other_CalType_Event_Info(".$eventList[$i]["EventID"].", '".$eventList[$i]["ProgrammeID"]."', 'weekly', '".$CalType."')";
					} 
				} else {*/
					$onclick = "jGet_Event_Info(".$eventList[$i]["EventID"].", 'weekly', '".$CalType."')";
				//}
			//}
			
			

			
				# Preparation
				$eventDivCss = "display:$display;font-size:11px;margin-top:0px;margin-bottom:0px;padding:0px";
				$eventLinkCss = ($isAllDay == 0) ? "cal_event_$ColorID" : "cal_calendar_$ColorID";
				
				
				
				$evt = $eventList;
				$eventWeek = date("w",strtotime($current_date));
				
				
				$duration = intval($evt[$i]["Duration"]);
				if ($evt[$i]["IsAllDay"] == 1)
					$duration = $duration - 1440;
				
				$eventTs = strtotime($evt[$i]["EventDate"]);
				
				#check for continuous event
				$endDate = date("Y-m-d", strtotime("+".$duration." minute",$eventTs));
				$startDate = date("Y-m-d", $eventTs);
				if ($evt[$i]["Duration"]>1440 || ($endDate != $startDate && $isAllDay == 0)){
					//echo "con event";
					
					#create event slot					
					$endDateYMD = date("Y-m-d",strtotime("+".$duration." minute",$eventTs));
					$startDateYMD = $current_date;
					
				
					$padding = Array();
					if ($offset != 0){
						$padding = $conEvent[date('w',strtotime($startDateYMD))];
						
					}  
					elseif ($i != 0){
						$nextDateYMD = date("Y-m-d", strtotime("+1 day",strtotime($startDateYMD)));					
						$padding = $conEvent[date('w',strtotime($nextDateYMD))];						
						
					}
					while ($endDateYMD >= $startDateYMD){	
						
						$temp = Array("display"=>$display,
									"DivClass"=>$DivClass,
									"Color"=>$Color,
									"pos" => $i+$offset,
									"onclick" => $onclick,
									"DivID"=>$DivID,
									"TipName"=>$TipName,
									"eventID"=>$evt[$i]["EventID"],
									"isLimitEventNum" =>$isLimitEventNum,
									"id" => "dummy-".$evt[$i]["EventID"]."-".date("j",strtotime($startDateYMD)));						
						
						$tempWeek = date("w",strtotime($startDateYMD));
						if ($startDateYMD != date("Y-m-d",strtotime($current_date)) && $tempWeek == 0){
							$events[date('w',strtotime($startDateYMD))][] = $evt[$i];
							break;
						}
						else{
							//echo $current_date."<br>";
							if (empty($conEvent[date('w',strtotime($startDateYMD))])){
								for ($j = 0; $j < count($padding); $j++){
									$padding[$j]["fake"] = true;
									$padding[$j]["id"] = "fake-".$padding[$j]["eventID"]."-".date("j",strtotime($startDateYMD));
								}
								$conEvent[$startDateYMD] = $padding;
							}
							if ($offset+$i > count($conEvent[date('w',strtotime($startDateYMD))])){
								$j = count($conEvent[date('w',strtotime($startDateYMD))]);
								while($j != $offset+$i){
									$lastDateYMD = date("Y-m-d",strtotime("-1 day",strtotime($startDateYMD)));
									$fake = $conEvent[date('w',strtotime($lastDateYMD))][$j];
									$fake["fake"] = true;
									$fake["id"] = "fake-".$fake["eventID"]."-".date("j",strtotime($startDateYMD));
									$conEvent[date('w',strtotime($startDateYMD))][] = $fake;
									$j++;
								}
							}
							//$conEvent[$startDateYMD][$i+$offset]= $temp;
							$conEvent[date('w',strtotime($startDateYMD))][]= $temp;
						}
						$startDateYMD = date("Y-m-d",strtotime("+1 day",strtotime($startDateYMD)));
					}
					
					$startDateYMD =$current_date;
					$width = 98;
					$weekOfDay = date("w",$ts);					
					if ($weekOfDay < 6){
						$lastDayOfWeek = strtotime("next saturday", $ts);
						$endDateTs = strtotime($endDateYMD);						
						//$startYM = date("Y-m",strtotime($startDateYMD));
						
						if ($lastDayOfWeek < $endDateTs){
							
							$lastDayWeekNum = date("w",$lastDayOfWeek);
							$width = $width*($lastDayWeekNum - $weekOfDay +1)+($lastDayWeekNum - $weekOfDay +1)*2+($lastDayWeekNum - $weekOfDay +1)-1;
							//}
							//echo ($lastDayWeekNum - $weekOfDay +1)."<br>";
						}
						else{
							$endDayWeekNum = date("w",$endDateTs);
							
							$width = $width*($endDayWeekNum - $weekOfDay +1)+($endDayWeekNum - $weekOfDay +1)*2+($endDayWeekNum - $weekOfDay +1)-1;
							//}
							//echo ($endDayWeekNum - $weekOfDay +1)."<br>";
						}
					}
					//echo $width; 
					
					//echo $i.": ".$evt[$i]["EventID"]."<br>";
					if($print && $display == 'none') continue; // do not output html for invisible events in print view
					
					$str = "<div class=\"cal_month_event_item\" style=\"margin: 0px; padding:0px\">\n";					
					$str .= "<div style=\"display:$display;font-size:11px;MARGIN: 0px; padding-top:0px;cursor:pointer;position: absolute; width: ".$width."px; background-color: $Color;\" ";
					$str .= "onclick=\"".$onclick."\" rel=\"#".$TipName."\" ";
					$str .= ($isLimitEventNum) ? "name=\"date".date("j",$eventTs)."event[]\" " : "";
					$str .= "id=\"".$DivID."\" class=\"".$DivClass."\">\n";
					if(!$isInvolve || ($isInvolve && $this->involveCalValid) ){	
						$str .= "<a class=\"cal_event_$ColorID\" href=\"javascript:void(0);\" style=\"color : white\">\n";
						$str .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" 	src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/>&nbsp;\n";
					} else {
						$str .= "<a class=\"cal_event_\" href=\"javascript:void(0);\" style=\"color : white\">\n";
					}
					$str .= $CalEventTitle;
					if (trim($evt[$i]["PersonalNote"]) != ""){
						$str .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					}
					$str .= "</a>\n";
					$str .= "</div>\n";
					$str .= "<div id=\"padding-".$evt[$i]["EventID"]."-".date("j",$ts)."\" class=\"".$DivClass."\" style=\"margin:0px; padding: 0px; font-size:15px;display:$display;\" >&nbsp;</div>";
					$str .= "</div>\n";
					$num++;
					
					$CalEventDiv .= $str;
					continue;
					
				}
				
				
				//echo $i.": ".$evt[$i]["EventID"]."<br>";
				
				
				
				
				# Content
				if ($print) {
					if($display == 'block')
					{
						$CalEventDiv .= "<div class=\"cal_month_event_item\">";
						//$CalEventDiv .= ($isAllDay == 0) ? "" : "<ul><li>";
						$CalEventDiv .= "<div style=\"$eventDivCss\" id=\"".$DivID."\" class=\"".$DivClass."\">";
						$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
						$CalEventDiv .= "<font color=\"#".$Color."\">".$CalEventTitle;
						if (trim($eventList[$i]["PersonalNote"]) != "") 
							$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
						$CalEventDiv .= "</font>";
						$CalEventDiv .= "</div>";
						//$CalEventDiv .= ($isAllDay == 0) ? "" : "</li></ul>";
						$CalEventDiv .= "</div>";
					}
				} else {
					$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					//$CalEventDiv .= ($isAllDay == 0) ? "" : "<ul><li>";
					$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;color:#FFF\" ";
					$CalEventDiv .= "onclick=\"".$onclick."\" id=\"".$DivID."\" class=\"".$DivClass."\" rel=\"#".$TipName."\" >";
					$CalEventDiv .= "<a class=\"$eventLinkCss\" href=\"javascript:void(0)\">";
					$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$CalEventDiv .= $CalEventTitle;
					if (trim($eventList[$i]["PersonalNote"]) != ""){
						$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					}
					$CalEventDiv .= "</a>";
					$CalEventDiv .= "</div>";
					//$CalEventDiv .= ($isAllDay == 0) ? "" : "</li></ul>";
					$CalEventDiv .= "</div>";	
				}
			
		}
		return $CalEventDiv."&nbsp";
		
	
	
	}
	
	
	
	
	
	
	
	## New approach using Query_User_Related_Events() & Query_User_Foundation_Events() function
	## added on 23 Feb 2009
	function Print_Weekly_Event_Block($eventList, $print=false) {
		if ($print){
			$calEventDiv = $this->Display_Weekly_Cal_Event($eventList, true);
		} else {
			$calEventDiv = $this->Display_Weekly_Cal_Event($eventList);
		}
		$date .= $calEventDiv;
		
		return $date."&nbsp";
	}
	
	## New approach using Query_User_Related_Events() & Query_User_Foundation_Events() function
	## added on 23 Feb 2009
	function Display_Weekly_Cal_Event($eventList, $print=false) {
		global $ImagePathAbs, $lui, $UserID;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, 
				$iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
		
		$uid = $this->getCurrentUserID();
		
		//debug_r($eventList);
		for ($i=0 ; $i<sizeof($eventList) ; $i++) {
			$CalType = trim($eventList[$i]['CalType']);
			$eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
			$eventDate = $eventDateTimePiece[0];
			$eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
			// 12 OR 24 time format
			if ($this->systemSettings["TimeFormat"] == 12) {
				$eventTime = date("h:i", $eventTs);
			} else if ($this->systemSettings["TimeFormat"] == 24) {
				$eventTime = date("G:i", $eventTs);
			}
			$isAllDay = $eventList[$i]["IsAllDay"];
			
			# Check the type of the event 
			//$isFoundation = ($CalType == 2) ? true : false;
			$isInvolve = (trim($eventList[$i]["Permission"]) == "R") ? true : false;
			//$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
			
			//echo $eventList[$i]["schoolCode"];
			### Preparation
			/*if (!empty($eventList[$i]["schoolCode"])){ # other school invitation event
				//echo $eventList[$i]["schoolCode"];
				$otherCalVisible = $this->returnUserPref($UserID, "otherInvitaionCalVisible");
				$display     = ($otherCalVisible == "0") ? "none": "block";
				$numVisible += ($otherCalVisible == "0") ? 0 : 1;
				
				# Get Corresponding Color of each Event
				$ColorID = $this->otherCalColorID;
				$Color	  = trim($this->otherCalColor);
				$DivID	  = "event".$eventList[$i]["EventID"].'-'.$eventList[$i]["schoolCode"];
				$DivClass = "cal-other";
				$TipName  = "eventToopTip";
			
			}
			elseif($isOtherCalType)			# other caltype event : CPD, CAS
			{		
				$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
				$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
				$otherCalTypeCalVisible = $this->returnUserPref($UserID, $cal_type_name."CalVisible");
				$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
				$numVisible += ($otherCalTypeCalVisible == "0") ? 0 : 1;
				
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = trim($eventList[$i]["Color"]);
				$DivID	  = $cal_type_name."Event".$eventList[$i]["EventID"];
				$DivClass = $cal_type_name;
				$TipName  = "eventToopTip";	
			} 
			else if($isFoundation)			# foundation event
			{
				//$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
				$foundationCalVisible = trim($eventList[$i]["Visible"]);
				$display = ($foundationCalVisible == "0") ? "none" : "block";
				
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = trim($eventList[$i]["Color"]);
				$DivID	  = "foundationEvent".$eventList[$i]["EventID"];
				$DivClass = "foundation-".$eventList[$i]["CalID"];
				$TipName  = "eventToopTip";	
			}
			else*/ if($isInvolve)			# involved event
			{			
				$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
				$display	 = ($involveCalVisible == "0") ? "none" : "block";

				$ColorID  = $this->involveCalColorID;
				$Color    = $this->involveCalColor;
				$DivID    = "involveEvent".$eventList[$i]["EventID"];
				$DivClass = "involve";
				$TipName  = "involveEventToopTip";
			} 
			else 						# personal related events
			{				
				$display = ($eventList[$i]["Visible"] == "0") ? "none": "block";

				# Get Corresponding Color of each Event
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = $eventList[$i]["Color"];
				$DivID	  = "event".$eventList[$i]["EventID"];
				$DivClass = "cal-".$eventList[$i]["CalID"];
				$TipName  = "eventToopTip";					
			}
			
			/*if(!$isFoundation || ($isFoundation && (
				($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV') || 
				(($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV') && $eventList[$i]['Owner'] != $UserID)
				) ) ){		    
				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= $eventList[$i]["Title"];
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 10);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 10);
				}
				
				
				if (!empty($eventList[$i]["schoolCode"])){
					#for other invitaion only
					$onclick = "jGet_Event_Info('".$eventList[$i]["EventID"]."-".$eventList[$i]["schoolCode"]."', 'monthly', '4')";
				}
				elseif($isOtherCalType){
					# Set Style for other CalType event only
					$CalEventTitle = $this->Set_Style_Other_CalType_Event($CalEventTitle, $eventList[$i]);
					
					if($CalType == ENROL_CPD_CALENDAR || $CalType == MY_CPD_CALENDAR){
						$onclick = "jGet_Other_CalType_Event_Info(".$eventList[$i]["EventID"].", '".$eventList[$i]["ProgrammeID"]."', 'weekly', '".$CalType."')";
					} 
				} else {*/
				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." " : "";
				$CalEventTitle .= intranet_undo_htmlspecialchars($eventList[$i]["Title"]). $this->AppendOnwerName($eventList[$i]["UserID"]);
				//if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 10);
				//} else {
				//	$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 10);
				//}
					$onclick = "jGet_Event_Info(".$eventList[$i]["EventID"].", 'weekly', '".$CalType."')";
				//}
				
				# Preparation
				$eventDivCss = "display:$display;font-size:11px;margin-top:0px;margin-bottom:0px;padding:0px";
				$eventLinkCss = ($isAllDay == 0) ? "cal_event_$ColorID" : "cal_calendar_$ColorID";
				
				# Content
				if ($print) {
					if($display == 'block') // only display visible events html for print view
					{
						$CalEventDiv .= "<div class=\"cal_month_event_item\">";
						$CalEventDiv .= ($isAllDay == 0) ? "" : "<ul><li>";
						$CalEventDiv .= "<div style=\"$eventDivCss\" id=\"".$DivID."\" class=\"".$DivClass."\">";
						$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
						$CalEventDiv .= "<font color=\"#".$Color."\">".$CalEventTitle;
						if (trim($eventList[$i]["PersonalNote"]) != "") 
							$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
						$CalEventDiv .= "</font>";
						$CalEventDiv .= "</div>";
						$CalEventDiv .= ($isAllDay == 0) ? "" : "</li></ul>";
						$CalEventDiv .= "</div>";
					}
				} else {
					$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					$CalEventDiv .= ($isAllDay == 0) ? "" : "<ul><li>";
					$CalEventDiv .= "<div style=\"$eventDivCss;cursor:pointer;color:#FFF\" ";
					$CalEventDiv .= "onclick=\"".$onclick."\" id=\"".$DivID."\" class=\"".$DivClass."\" rel=\"#".$TipName."\" >";
					$CalEventDiv .= "<a class=\"$eventLinkCss\" href=\"javascript:void(0)\">";
					$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$CalEventDiv .= $CalEventTitle;
					if (trim($eventList[$i]["PersonalNote"]) != ""){
						$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					}
					$CalEventDiv .= "</a>";
					$CalEventDiv .= "</div>";
					$CalEventDiv .= ($isAllDay == 0) ? "" : "</li></ul>";
					$CalEventDiv .= "</div>";	
				}
			}
		//}
		return $CalEventDiv;
	}
	/*  *** End of Weekly View ***  */
		
	/*  *** Daily View ***  */
	function Generate_Daily_Calendar($year, $month, $day, $print=false) {
	    global $iCalendar_NewEvent_IsAllDay;
	    global $events;
	     
	    //debug_pr($events);
	    $allDayConEvt = array();
	    $dayString = "$year-$month-$day";
	    $dayInfo = date("D", mktime(0, 0, 0, $month, $day, $year))." ($day/$month)";
	    for ($i=0; $i<24; $i++) {
		    $start_time = (($i<10)?"0$i":"$i").":00";
	      	$end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	      
	      	if (!isset($eventHappenGrid[$i])) {
	      		$eventHappenGrid[$i] = array();
	    	}
	    	if (!isset($eventHappenDetailGrid[$i])) {
	      		$eventHappenDetailGrid[$i] = array();
	    	}
	      	if (!isset($eventRowSpan[$i])) {
	      		$eventRowSpan[$i] = 0;                                                                         
	    	}
		    $start_ts = $this->sqlDatetimeToTimestamp($dayString." $start_time");
		    $end_ts = $this->sqlDatetimeToTimestamp($dayString." $end_time");
		    # Handling the timeslot from 23:00 to 00:00(next day) since $this->sqlDatetimeToTimestamp("24:00") return false;
		    if($i+1==24) $end_ts = $start_ts + 60*60;
	      
		  if(sizeof($events[$dayString]) > 0 ){
			$evt = $events[$dayString];
			for ($k=0; $k<sizeof($evt) ; $k++){
	      	//for ($k=0; $k<sizeof($events); $k++) {
			
				if (date("Y-m-d",strtotime($evt[$k]["EventDate"]))< date("Y-m-d",mktime(0, 0, 0, $month, $day, $year))){
					$curEventTs = strtotime($year.'-'.$month.'-'.$day.' '.date("H:i:s",strtotime($evt[$k]["EventDate"])));
				}
				else
					$curEventTs = $this->sqlDatetimeToTimestamp($evt[$k]["EventDate"]);
				
// 					debug_pr($curEventTs);
			
				
				$endDay = date("Y-m-d",strtotime("+".$evt[$k]['Duration']." minute",strtotime($evt[$k]["EventDate"])));			
				$startDay = date('Y-m-d',strtotime($evt[$k]["EventDate"]));
				
				if ($evt[$k]['Duration']>=1440 || $endDay != $startDay){
					if ($i==0){
						$allDayConEvt[] = $evt[$k];
					}
			
				}
			
			
		       // $curEventTs = $this->sqlDatetimeToTimestamp($evt[$k]["EventDate"]);
		        elseif ($curEventTs >= $start_ts && $curEventTs < $end_ts) {
			        # Count the time difference if the event does not start at start_Ts time exactly like 12:30 --> not 12:00
			        $extraTs = $curEventTs - $start_ts;
			        $extraMin = ($extraTs <= 60*60) ? $extraTs/60 : 0;
			        $numOfHours = ceil(($evt[$k]["Duration"] + $extraMin) / 60 );
			        
			        if ($numOfHours <= 1) {
			        	$eventHappenGrid[$i][] = $evt[$k]["EventID"];
			        	$eventHappenDetailGrid[$i][] = $evt[$k];

			        	$rowSpan[$i] = ($rowSpan[$i] < $numOfHours) ? 0 : $rowSpan[$i];
			        	if (!isset($eventHappenGrid[$i+1])) {
			        		$eventHappenGrid[$i+1] = array();
			        		$eventHappenDetailGrid[$i+1] = array();
		        		}
		        	} else {
			        	$rowSpan[$i] = ($rowSpan[$i] < $numOfHours) ? $numOfHours : $rowSpan[$i];
				        for ($l=0; $l<$numOfHours; $l++) {
				        	$eventHappenGrid[$i+$l][] = $evt[$k]["EventID"];
				        	$eventHappenDetailGrid[$i+$l][] = $evt[$k];
			        	}
		        	}
		        } else {
			        if (!isset($eventHappenGrid[$i+1])) {
			        	 $eventHappenGrid[$i+1] = array();
		        	}
		        	if (!isset($eventHappenDetailGrid[$i+1])) {
			        	 $eventHappenDetailGrid[$i+1] = array();
		        	}
		        }
		    } # End for
		  } # End if 
	    }
    
	    $calendar .= "<table cellspacing='0' cellspacing='0' id='daily'>";
		//$calendar .= "<tbody>";
		$calendar .= "<thead>";
		
		if(!$print){
			$calendar .= "<tr>";
			$calendar .= "<td class=\"cal_none\">&nbsp;</td>";
			$calendar .= "<td class=\"cal_week_week_title\">$dayInfo</td>";
			$calendar .= "</tr>";
		}
		
		
		# Top cells for all day event
		$calendar .= "<tr>";
		$calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$iCalendar_NewEvent_IsAllDay</td>";
		
		if (date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString){
			$calendar .= "<td class='".($print?"dayFirstCellPrint'":"dayFirstCellToday' onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"dayFirstCellToday\"'").">";
		} else {
			$calendar .= "<td id='$dayString' class='".($print?"dayFirstCellPrint'":"dayFirstCell' onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"dayFirstCell\"'").">";
		}
// 	debug_r($allDayConEvt);
		if (sizeof($allDayConEvt) > 0) {
			if ($print){
				$calendar .= $this->Print_Daily_Event_Block($allDayConEvt, true);
			} else {
				$calendar .= $this->Print_Daily_Event_Block($allDayConEvt);
			}
			# remove all day events from the array
			$sizeOfEventHappenDetailGrid = sizeof($eventHappenDetailGrid[0]);
			for($j=0; $j<$sizeOfEventHappenDetailGrid ; $j++) {
				if (trim($eventHappenDetailGrid[0][$j]["IsAllDay"]) == "1"){
					unset($eventHappenDetailGrid[0][$j]);
				}
			}
			sort($eventHappenDetailGrid[0]);
		} else {
			$calendar .= "&nbsp";
		}
		$calendar .= "</td>";
		$calendar .= "</tr>";
			
		$calendar .= "</thead>";
		$calendar .= "<tbody>";
	  	for ($i=$this->workingHourStart; $i<$this->workingHourEnd; $i++) {
	        $calendar .= "<tr>";
	        $start_time = (($i<10)?"0$i":"$i").":00";
	        $end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	        $calendar .= "<td class='".($print?"timeRowPrint":"timeRow2 timeText")."' scope='row'>$start_time</td>";
	        
	        if ($i==$this->workingHourStart)
	        	$curLastIntersect = array();
	        else
	        	$curLastIntersect = array_intersect($eventHappenGrid[$i], $eventHappenGrid[$i-1]);
	        
	        if ($i==$this->workingHourEnd)
	        	$curNextIntersect = array();
	        else {
		        if ($eventHappenGrid[$i+1])
	        		$curNextIntersect = array_intersect($eventHappenGrid[$i], $eventHappenGrid[$i+1]);
	      	}
// 	      	debug_pr($curNextIntersect);

	        # No event in this cell OR the event only need one cell
	        if (empty($curLastIntersect) && empty($curNextIntersect)) {
		        if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString) && $i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCellToday";
	        	} else if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString)){
		        	$cellClassName = $print?"dayCellPrint":"dayCellToday";
	        	} else if ($i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCell";
	        	} else {
		        	$cellClassName = $print?"dayCellPrint":"dayCell";
	        	}
		        $calendar .= "<td id='$dayString-$i' class='$cellClassName' ".($print?"":"onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
		        if ($print){
		        	$calendar .= $this->Print_Daily_Event_Block($eventHappenDetailGrid[$i], true);
				} else {
		        	$calendar .= $this->Print_Daily_Event_Block($eventHappenDetailGrid[$i]);
				}
		        $calendar .= "&nbsp;</td>";
	        }
	        # Row span is needed for event with duration more than 1 hour
	        else if (empty($curLastIntersect) && !empty($curNextIntersect)) {
		        //$rowSpan[$i] = $this->Get_Rowspan_Event_Number($i, $rowSpan[$i]);
		        # Handle the case of intersection among several events shown in UI
		        # Condition: 
		        #	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
		        #	Check The other Events duration beginning within the hour in the above range
		        # 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
		        # 	Because of Event 2 is set within the range of Event 1,
		        # 	the rowspan should be updated to 3 from 2.
		        if($rowSpan[$i] > 1){
		        	$orig_span_range = $i + $rowSpan[$i];
		        	/*for($m=$i+1 ; $m<$orig_span_range ; $m++){
			        	$other_span_range = $m + $rowSpan[$m];
			        	if($other_span_range > $orig_span_range){
			        		$new_span = $rowSpan[$i] + ($other_span_range - $orig_span_range);
			        		$rowSpan[$i] = $new_span;
		        		}
	        		}*/
					for($m=$i; $m<$orig_span_range ; $m++){
						$other_span_range = $m + $rowSpan[$m];
						if($other_span_range > $orig_span_range){
							$new_span = $rowSpan[$i] + ($other_span_range - $orig_span_range);
							$rowSpan[$i] = $new_span;
							$orig_span_range = $i+$new_span;
							//$rowSpanSlot[$m][$j] = true;
						}
					}
	        	}
		        	
		        if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString) && $i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCellToday";
	        	} else if ((date("Y-m-d", time()) == $dayString || date("Y-n-j", time()) == $dayString)){
		        	$cellClassName = $print?"dayCellPrint":"dayCellToday";
	         	} else if ($i == $this->workingHourEnd-1){
		        	$cellClassName = $print?"dayLastCellPrint":"dayLastCell";
	        	} else {
		        	$cellClassName = $print?"dayCellPrint":"dayCell";
	        	}
		        $calendar .= "<td id='$dayString-$i' class='$cellClassName' rowspan='".max(1,$rowSpan[$i])."' ".($print?"":"onmouseover='this.className=\"dayCellHover\"' onmouseout='this.className=\"$cellClassName\"'").">";
		        for ($k=1; $k<$rowSpan[$i]; $k++) {
			        $eventHappenGrid[$i] = $this->array_union($eventHappenGrid[$i], $eventHappenGrid[$i+$k]);
			        $eventHappenDetailGrid[$i] = array_merge($eventHappenDetailGrid[$i], $eventHappenDetailGrid[$i+$k]);
		        }
		        $tempArray = array();
		        $exist = array();
		        
// 		        debug_pr($eventHappenDetailGrid);
		        for ($l=0; $l<sizeof($eventHappenDetailGrid[$i]); $l++) {
			        if (in_array($eventHappenDetailGrid[$i][$l]["EventID"], $eventHappenGrid[$i]) && !in_array($eventHappenDetailGrid[$i][$l]["EventID"], $exist)) {
			        	$tempArray[] = $eventHappenDetailGrid[$i][$l];
			        	$exist[] = $eventHappenDetailGrid[$i][$l]["EventID"];
		        	}
		        }
		        $eventHappenDetailGrid[$i] = $tempArray;
		        if ($print){
		        	$calendar .= $this->Print_Daily_Event_Block($eventHappenDetailGrid[$i], true);
				} else {
		        	$calendar .= $this->Print_Daily_Event_Block($eventHappenDetailGrid[$i]);
				}
		        $calendar .= "&nbsp;</td>";
		        
	        }
	        $calendar .= "</tr>";
	    }
	    $calendar .= "</tbody>";
	    $calendar .= "</table>";
	    return $calendar;
    }
	
	function Print_Daily_Event_Block($eventList, $print=false) {
		if($print){
			$calEventDiv = $this->Display_Daily_Cal_Event($eventList, true);
		} else {
			$calEventDiv = $this->Display_Daily_Cal_Event($eventList);
		}
		$date .= $calEventDiv;
		return $date."&nbsp";
	}
	
	function Display_Daily_Cal_Event($eventList, $print=false) {
		global $ImagePathAbs, $lui, $UserID;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_EventDate, 
				$iCalendar_NewEvent_EventTime, $iCalendar_NewEvent_Link;
		
		$uid = $this->getCurrentUserID();
		
		for ($i=0; $i<sizeof($eventList); $i++) {
			$CalType = trim($eventList[$i]['CalType']);
			$eventDateTimePiece = explode(" ", $eventList[$i]["EventDate"]);
			$eventDate = $eventDateTimePiece[0];
			$eventTs = $this->sqlDatetimeToTimestamp($eventList[$i]["EventDate"]);
			$endTs = (int)$eventTs + (int)$eventList[$i]["Duration"]*60;
			if ($this->systemSettings["TimeFormat"] == 12) {
				$eventTime = date("h:i", $eventTs);
				$endTime = date("h:i", $endTs);
			} else if ($this->systemSettings["TimeFormat"] == 24) {
				$eventTime = date("G:i", $eventTs);
				$endTime = date("G:i", $endTs);
			}
			$isAllDay = $eventList[$i]["IsAllDay"];
			
			# Check the type of the event 
			//$isFoundation = ($CalType == 2) ? true : false;
			$isInvolve = (trim($eventList[$i]["Permission"]) == "R") ? true : false;
			//$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
			
			### Preparation
			/*if (!empty($eventList[$i]["schoolCode"])){ # other school invitation event
				$otherCalVisible = $this->returnUserPref($UserID, "otherInvitaionCalVisible");
				$display     = ($otherCalVisible == "0") ? "none": "block";
				$numVisible += ($otherCalVisible == "0") ? 0 : 1;
				
				# Get Corresponding Color of each Event
				$ColorID = $this->otherCalColorID;
				$Color	  = trim($this->otherCalColor);
				$DivID	  = "event".$eventList[$i]["EventID"].'-'.$eventList[$i]["schoolCode"];
				$DivClass = "cal-other";
				$TipName  = "eventToopTip";
			
			}
			elseif($isOtherCalType)			# other foundation event : CPD, CAS
			{		
				$otheCalTypeSetting = $this->Get_Other_CalType_Settings($CalType);
				$cal_type_name = (count($otheCalTypeSetting) > 0) ? $otheCalTypeSetting[0]['CalTypeName'] : '';
				$otherCalTypeCalVisible = $this->returnUserPref($UserID, $cal_type_name."CalVisible");
				$display 	 = ($otherCalTypeCalVisible == "0") ? "none" : "block";
				$numVisible += ($otherCalTypeCalVisible == "0") ? 0 : 1;
				
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = trim($eventList[$i]["Color"]);
				$DivID	  = $cal_type_name."Event".$eventList[$i]["EventID"];
				$DivClass = $cal_type_name;
				$TipName  = "eventToopTip";	
			} 
			else if($isFoundation)			# foundation event
			{
				//$foundationCalVisible = $this->returnUserPref($UserID, "foundationCalVisible");
				$foundationCalVisible = trim($eventList[$i]["Visible"]);
				$display = ($foundationCalVisible == "0") ? "none" : "block";
				
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = $eventList[$i]["Color"];
				$DivID	  = "foundationEvent".$eventList[$i]["EventID"];
				$DivClass = "foundation-".$eventList[$i]["CalID"];
	 			$TipName  = "eventToopTip";	
			}
			else*/ 
	
			if (!empty($eventList[$i]["CalType"])&&
			($eventList[$i]["CalType"] == 1 || $eventList[$i]["CalType"]>3 )){
			# external calendar
				$display     = ($eventList[$i]["Visible"] == "0") ? "none": "block";
				$numVisible += ($eventList[$i]["Visible"] == "0") ? 0 : 1;

				# Get Corresponding Color of each Event
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = trim($evt[$i]["Color"]);
				$DivID	  = $eventList[$i]["DivPrefix"].$eventList[$i]["EventID"];
				$DivClass = $eventList[$i]["DivClass"];
				$TipName  = "eventToopTip";
				$CalType = $eventList[$i]["CalType"];
			}
			else if($isInvolve)			# involved event
			{			
				$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
				$display	 = ($involveCalVisible == "0") ? "none" : "block";

				$ColorID  = $this->involveCalColorID;
				$Color    = $this->involveCalColor;
				$DivID    = "involveEvent".$eventList[$i]["EventID"];
				$DivClass = "involve";
				$TipName  = "involveEventToopTip";
			} 
			else 						# personal related events
			{				
				$display = ($eventList[$i]["Visible"] == "0") ? "none": "block";

				# Get Corresponding Color of each Event
				$ColorID = $this->Get_Calendar_ColorID($eventList[$i]["Color"]);
				$Color	  = $eventList[$i]["Color"];
				$DivID	  = "event".$eventList[$i]["EventID"];
				$DivClass = "cal-".$eventList[$i]["CalID"];
				$TipName  = "eventToopTip";					
			}
			
			$eventLinkCss = ($isAllDay == 0) ? "cal_event_$ColorID" : "cal_calendar_$ColorID";
			
			# Content	    	
		/*	if(!$isFoundation || ($isFoundation && (
				($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV') || 
				(($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV') && $eventList[$i]['Owner'] != $UserID)
				) ) ){
				
				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." - ".$endTime." " : "";
				$CalEventTitle .= $eventList[$i]["Title"];
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 225);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 225);
				}
				if (!empty($eventList[$i]["schoolCode"])){
					#for other invitaion only
					$onclick = "jGet_Event_Info('".$eventList[$i]["EventID"]."-".$eventList[$i]["schoolCode"]."', 'monthly', '4')";
				}
				elseif($isOtherCalType){
					# Set Style for other CalType event only
					$CalEventTitle = $this->Set_Style_Other_CalType_Event($CalEventTitle, $eventList[$i]);
					
					if($CalType == ENROL_CPD_CALENDAR || $CalType == MY_CPD_CALENDAR){
						$onclick = "jGet_Other_CalType_Event_Info(".$eventList[$i]["EventID"].", '".$eventList[$i]["ProgrammeID"]."', 'daily', '".$CalType."')";
					} 
				} else {*/
				# Get Event Display Title
				$CalEventTitle  = ($isAllDay == 0) ? $eventTime." - ".$endTime." " : "";
				$CalEventTitle .= intranet_undo_htmlspecialchars($eventList[$i]["Title"]). $this->AppendOnwerName($eventList[$i]["UserID"]);
				if ($print) {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], true, 225);
				} else {
					$CalEventTitle = $this->formatEventTitle($CalEventTitle, (int)$eventList[$i]["IsImportant"], false, 225);
				}
					$onclick = "jGet_Event_Info(".$eventList[$i]["EventID"].", 'daily', '".$CalType."')";
				//}
				
				if ($print) {
					if($display == 'block') // only display visible events for print view
					{
						$CalEventDiv .= "<div class=\"cal_month_event_item\">";
						$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin-bottom:1px;padding:0px;margin-top:0px\" ";
						$CalEventDiv .= "id=\"".$DivID."\" class=\"".$DivClass."\">";
						$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
						$CalEventDiv .= "<font color=\"".$Color."\">".$CalEventTitle;
						if (trim($eventList[$i]["PersonalNote"]) != ""){
							$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
						}
						$CalEventDiv .= "</font>";
						$CalEventDiv .= "</div>";
						$CalEventDiv .= "</div>";
					}
				} else {

					$CalEventDiv .= "<div class=\"cal_month_event_item\">";
					$CalEventDiv .= "<div style=\"display:$display;font-size:11px;margin-bottom:1px;padding:0px;cursor:pointer;margin-top:0px;color:#FFF\" ";
					$CalEventDiv .= "onclick=\"".$onclick."\" id=\"".$DivID."\" class=\"".$DivClass."\" rel=\"#".$TipName."\" >";
					$CalEventDiv .= "<a class=\"$eventLinkCss\" href=\"javascript:void(0);\">";
					$CalEventDiv .= "<img class=\"cal_event_png_$ColorID\" width=\"3\" height=\"10\" border=\"0\" align=\"absmiddle\" src=\"".$ImagePathAbs."calendar/event_$ColorID.png\"/> ";
					$CalEventDiv .= $CalEventTitle;
					if (trim($eventList[$i]["PersonalNote"]) != ""){
						$CalEventDiv .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
					}
					$CalEventDiv .= "</a>";
					$CalEventDiv .= "</div>";
					$CalEventDiv .= "</div>";
				}
			}
		//}
		return $CalEventDiv;
	  }
	
	/*  *** End of Daily View ***  */
		
	/*  *** Monthly View of Small Calendar ***  */
	function Generate_Monthly_Small_Calendar($year, $month) {
		global $SYS_CONFIG, $events;

		$first_of_month = mktime (0,0,0, $month, 1, $year);
		static $day_headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
		$maxdays   = date('t', $first_of_month); # number of days in the month
		$date_info = getdate($first_of_month);   # get info about the first day of the month
		$month 	   = $date_info['mon'];
		$year 	   = $date_info['year'];
		$numOfRows = 0;

		$calendar  = "<table cellpadding='0' cellspacing='1' id='monthly' border='0' bgcolor='#CCCCCC'>";
		$calendar .= "<colgroup>
						<col class='Sun' />
						<col class='Mon' />
						<col class='Tue' />
						<col class='Wed' />
						<col class='Thu' />
						<col class='Fri' />
						<col class='Sat' />
						<col />
					  </colgroup>";
		$calendar .= "<thead>
						<tr>
						<th class='cal_week_title'>Sun</th>
						<th class='cal_week_title'>Mon</th>
						<th class='cal_week_title'>Tue</th>
						<th class='cal_week_title'>Wed</th>
						<th class='cal_week_title'>Thu</th>
						<th class='cal_week_title'>Fri</th>
						<th class='cal_week_title'>Sat</th>
						</tr>
					  </thead>";       
		$calendar .= "<tbody><tr>";
		$class  = "";
		$weekday = $date_info['wday']; #weekday (zero based) of the first day of the month

		# set class of cell: normal or print
		$monthCellClass = "portal_cal_normal_day";
		$monthTodayCellClass = "portal_cal_normal_today";

		#take care of the first "empty" number of the month
		if($weekday > 0) {
			for ($i=0; $i<$weekday; $i++) {
				$calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
			}
		} 

		#print the number of the month
		$week = 1;
		for ($day=1; $day<=$maxdays; $day++) {
			$result = array();
			$ts = mktime(0,0,0,$month,$day,$year);
			$end_ts = mktime(0,0,0,$month,$day,$year) + 24*60*60;
			 
			# Get Number of Event in each day
			$current_date = date("Y-m-d", $ts);
			$numOfEvents = count($events[$current_date]);
				
			# Content
			if ($year."-".$month."-".$day == date("Y-n-j", time())){
				$calendar .= "<td>";
				$calendar .= "<div id=\"$year-$month-$day\" class=\"$monthTodayCellClass\">";
			} else {
				$calendar .= "<td>";
				$calendar .= "<div id=\"$year-$month-$day\" class=\"$monthCellClass\">";
			}
			$calendar .= "<a href=\"".$PathRelative."src/module/calendar/index.php?time=$ts&calViewPeriod=daily\">";
			$calendar .= "<span>$day</span><br />";
			if($numOfEvents > 0){
				$calendar .= "<div class=\"portal_cal_month_event\" style=\"text-align:left;color:#0066CC\">";
				$calendar .= $numOfEvents;
				$calendar .= "<img src=\"".$SYS_CONFIG['Image']['Abs']."/icon_event_s.gif\" width=\"13\" height=\"14\" class=\"cal_event_blue\" align=\"absbottom\" style=\"border:0\">";
				$calendar .= "</div>";
			}
			$calendar .= "</a>";
			$calendar .= "</div>";
			$calendar .= "</td>";
			$weekday++;
			  
			if($weekday == 7) {
				$calendar .= "</tr>";
				$calendar .= ($day != $maxdays) ? "<tr>" : "";
				$weekday = ($day != $maxdays) ? 0 : $weekday;
				$week++;
				$numOfRows++;
			}
		}

		# Generate Remaining Cell 
		if($weekday != 7) {
			for ($i=0; $i<(7-$weekday); $i++) {
				$calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
			}
			$calendar .= "</tr>";
			$numOfRows++;
		}
		if($numOfRows < 6){
			$calendar .= "<tr>";
			for ($i=0; $i<7; $i++) {
				$calendar .= "<td class=\"$monthCellClass\">&nbsp;</td>";
			}
			$calendar .= "</tr>";
		}
		$calendar .= "</tbody></table>";

		return $calendar;
    }
	/*  *** End of Monthly View of Small Calendar ***  */
		
	/*  *** Weekly View of Small Calendar ***  */
	function Generate_Weekly_Small_Calendar($year, $month, $day){
	    global $SYS_CONFIG;
	    global $iCalendar_NewEvent_IsAllDay;
	    global $events;
	    
	    $current_week_date = $this->get_week($year, $month, $day, "Y-m-d");
	    for ($i=0; $i<24; $i++) {
		    $start_time = (($i<10)?"0$i":"$i").":00";
        	$end_time = ((($i+1)<10)?"0".($i+1):($i+1)).":00";
	        for ($j=0; $j<7; $j++) {
		        if (!isset($eventHappenGrid[$i][$j])) {
		        	$eventHappenGrid[$i][$j] = array();
	        	}
	        	if (!isset($eventHappenDetailGrid[$i][$j])) {
		        	$eventHappenDetailGrid[$i][$j] = array();
	        	}
		        if (!isset($eventRowSpan[$i][$j])) {
		        	$eventRowSpan[$i][$j] = 0;
	        	}
		        $start_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $start_time");
		        $end_ts = $this->sqlDatetimeToTimestamp($current_week_date[$j]." $end_time");
		        # Handling the timeslot from 23:00 to 00:00(next day) since $this->sqlDatetimeToTimestamp("24:00") return false;
	     		if($i+1==24) $end_ts = $start_ts + 60*60;
	     		 
	     		# Count Row Span Algorithm
				if(sizeof($events[$current_week_date[$j]]) > 0){
					$evt = $events[$current_week_date[$j]];
					for ($k=0; $k<sizeof($evt) ; $k++){
					//for ($k=0; $k<sizeof($events); $k++) {
						$curEventTs = $this->sqlDatetimeToTimestamp($evt[$k]["EventDate"]);
						if ($curEventTs >= $start_ts && $curEventTs < $end_ts) {
							$numOfHours = ceil($evt[$k]["Duration"]/60);
							if (date("i",$curEventTs) != "00")
								$numOfHours++;
							if ($numOfHours <= 1) {
								$eventHappenGrid[$i][$j][] = $evt[$k]["EventID"];
								$eventHappenDetailGrid[$i][$j][] = $evt[$k];
								### Bug Fix on 2008-01-17 ###
								# When multiple events start in the same cell, do not reset the
								# row span to 0 if there already exist a value
								if (!isset($rowSpan[$i][$j]) || empty($rowSpan[$i][$j]))
									$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? 0 : $rowSpan[$i][$j];
								if (!isset($eventHappenGrid[$i+1][$j])) {
									$eventHappenGrid[$i+1][$j] = array();
									$eventHappenDetailGrid[$i+1][$j] = array();
								}
							} else {
								$rowSpan[$i][$j] = ($rowSpan[$i][$j] < $numOfHours) ? $numOfHours : $rowSpan[$i][$j];
								for ($l=0; $l<$numOfHours; $l++) {
									$eventHappenGrid[$i+$l][$j][] = $evt[$k]["EventID"];
									$eventHappenDetailGrid[$i+$l][$j][] = $evt[$k];
								}
							}
						} else {
							if (!isset($eventHappenGrid[$i+1][$j])) {
								$eventHappenGrid[$i+1][$j] = array();
							}
							if (!isset($eventHappenDetailGrid[$i+1][$j])) {
								$eventHappenDetailGrid[$i+1][$j] = array();
							}
						}
					} # End for 
		        } # End if
		        if($i == 23) $rowSpan[$i][$j] = 1;
		        if($rowSpan[$i][$j] == NULL || $rowSpan[$i][$j] == "") $rowSpan[$i][$j] = 0;
	       	}
      	}
      	$current_week = $this->get_week($year, $month, $day, "d/m D");
    	
	   	$calendar .= "<table cellspacing='0' cellspacing='0' id='weekly'>";	    
	    $calendar .= "<colgroup>
	      				<col />
	      				<col class='Sun' />
						<col class='Mon' />
						<col class='Tue' />
						<col class='Wed' />
						<col class='Thu' />
						<col class='Fri' />
						<col class='Sat' />
					  </colgroup>";
		$calendar .= "<thead><tr>";
		$calendar .= "<td class='top_empty'>&nbsp;</td>";
		
		for($i=0; $i<7; $i++) {
			# apply different class for TODAY
			$display_date = date("d/m", strtotime($current_week_date[$i]));
			$display_week = date("D", strtotime($current_week_date[$i]));
			if (date("d/m D", time()) == $current_week[$i]){
				$calendar .= "<th class='cal_week_title_today'>".$display_date."<br>".$display_week."</th>";
			} else {
				$calendar .= "<th class='cal_week_title'>".$display_date."<br>".$display_week."</th>";
			}
		}
		$calendar .= "</tr></thead>";
		$calendar .= "<tbody>";
		
		# Top cells for all day event
		$calendar .= "<tr>";
		$calendar .= "<td scope='row'></td>";
		for ($i=0; $i<7; $i++) {
			$numOfAllDayEvent = 0;
			if (sizeof($eventHappenDetailGrid[0][$i]) > 0) {
				for($j=0; $j<sizeof($eventHappenDetailGrid[0][$i]); $j++) {
					if ($eventHappenDetailGrid[0][$i][$j]["IsAllDay"] == "1"){
						$numOfAllDayEvent++;
					}
				}
			}
			if (date("d/m D", time()) == $current_week[$i]){
				$calendar .= "<td id='$current_week_date[$i]' class='portal_cal_week_day_whole_today'>";
			} else {
				$calendar .= "<td id='$current_week_date[$i]' class='portal_cal_week_day_whole'>";
			}
			
			if($numOfAllDayEvent > 0){
				$ts = strtotime($current_week_date[$i]);
				$calendar .= '<a href="'.$PathRelative.'src/module/calendar/index.php?time='.$ts.'&calViewPeriod=daily" class="portal_week_event_item_whole">';
				$calendar .= $numOfAllDayEvent;
				$calendar .= '<img src="'.$SYS_CONFIG['Image']['Abs'].'icon_event_s.gif" width="13" height="14" border="0" align="absmiddle" class="cal_event_blue">';
				$calendar .= '</a>';
			} else {
				$calendar .= "&nbsp;";
			}
			$calendar .= "</td>";
		}
		$calendar .= "</tr>";
		
		for ($i=$this->workingHourStart; $i<$this->workingHourEnd; $i++) {
			$start_time = sprintf("%02d",$i).":00";
			$end_time = sprintf("%02d",$i+1).":00";
			$display_time = sprintf("%02d",$i)."00";
			
			$calendar .= "<tr>";
			$calendar .= "<td class='portal_cal_week_time' scope='row'>$display_time</td>";
			for ($j=0; $j<7; $j++) {
				if ($i==$this->workingHourStart)
					$curLastIntersect = array();
				else
					$curLastIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i-1][$j]);
				
				if ($i==$this->workingHourEnd)
					$curNextIntersect = array();
				else {
					if ($eventHappenGrid[$i+1][$j])
						$curNextIntersect = array_intersect($eventHappenGrid[$i][$j], $eventHappenGrid[$i+1][$j]);
				}
				
				# No event in this cell OR the event only need one cell
				$numOfEvent = 0;
				if (empty($curLastIntersect) && empty($curNextIntersect)) {
					# Count Number of Event 
					if (count($eventHappenDetailGrid[$i][$j]) > 0){
						for($k=0 ; $k<count($eventHappenDetailGrid[$i][$j]) ; $k++){
							if($eventHappenDetailGrid[$i][$j][$k]["IsAllDay"] != 1)
								$numOfEvent++;
						}
					}
					
					# Get style
					if (date("d/m D", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){ 
						$cellClassName = "portal_cal_week_today";
					} else if (date("d/m D", time()) == $current_week[$j]) {
						$cellClassName = "portal_cal_week_today";
					} else if ($i == $this->workingHourEnd-1){
						$cellClassName = "portal_cal_week_day";
					} else {
						$cellClassName = "portal_cal_week_day";
					}
					$ts = strtotime($current_week_date[$j]);
					
					$calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName'>";
					if($numOfEvent > 0){
						$calendar .= '<a href="'.$PathRelative.'src/module/calendar/index.php?time='.$ts.'&calViewPeriod=daily" class="portal_week_event_item_whole">';
						$calendar .= count($eventHappenDetailGrid[$i][$j]).' ';
						$calendar .= '<img src="'.$SYS_CONFIG['Image']['Abs'].'icon_event_s.gif" width="13" height="14" border="0" align="absmiddle" class="cal_event_blue">';
						$calendar .= '</a>';
					} else {
						$calendar .= "&nbsp;";
					}
					$calendar .= "</td>";
				}
				# Row span is needed for event with duration more than 1 hour
				else if (empty($curLastIntersect) && !empty($curNextIntersect)) {
					# Handle the case of intersection among several events shown in UI
					# Condition: 
					#	E.g. Event 1 - start hour: 1400, end hour: 1530, duration: 90mins, rowspan: 2, span range [14 - 15]
					#	Check The other Events duration beginning within the hour in the above range
					# 	E.g. Event 2 - start hour: 1500, end hour: 1700, duration: 120mins, rowspan: 2, span range [15 - 16]
					# 	Because of Event 2 is set within the range of Event 1,
					# 	the rowspan should be updated to 3 from 2.
					if($rowSpan[$i][$j] > 1){
						$orig_span_range = $i + $rowSpan[$i][$j];
						for($m=$i+1 ; $m<=$orig_span_range ; $m++){
							$other_span_range = $m + $rowSpan[$m][$j];
							if($other_span_range > $orig_span_range){
								$new_span = $rowSpan[$i][$j] + ($other_span_range - $orig_span_range);
								$rowSpan[$i][$j] = $new_span;
							}
						}
					}
					
					# Get Event Number
					for ($k=1; $k<$rowSpan[$i][$j]; $k++) {
						$eventHappenGrid[$i][$j] = $this->array_union($eventHappenGrid[$i][$j], $eventHappenGrid[$i+$k][$j]);
						$eventHappenDetailGrid[$i][$j] = array_merge($eventHappenDetailGrid[$i][$j], $eventHappenDetailGrid[$i+$k][$j]);
					}
					$tempArray = array();
					$exist = array();
					for ($l=0; $l<sizeof($eventHappenDetailGrid[$i][$j]); $l++) {
						if (in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $eventHappenGrid[$i][$j]) && !in_array($eventHappenDetailGrid[$i][$j][$l]["EventID"], $exist)) {
							$tempArray[] = $eventHappenDetailGrid[$i][$j][$l];
							$exist[] = $eventHappenDetailGrid[$i][$j][$l]["EventID"];
						}
					}
					$numOfEvent = count($tempArray);
					
					# Get Style
					if (date("d/m D", time()) == $current_week[$j] && $i == $this->workingHourEnd-1){
						$cellClassName = "portal_cal_week_today";
					} else if (date("d/m D", time()) == $current_week[$j]){
						$cellClassName = "portal_cal_week_today";
					} else if ($i == $this->workingHourEnd-1){
						$cellClassName = "portal_cal_week_day";
					} else {
						$cellClassName = "portal_cal_week_day";
					}
					$ts = strtotime($current_week_date[$j]);
					
					$calendar .= "<td id='$current_week_date[$j]-$i' class='$cellClassName' rowspan='".max(1,$rowSpan[$i][$j])."'>";
					if($numOfEvent > 0){
						$calendar .= "<div class='portal_week_event_item' style='position:relative'>";
						$calendar .= "<a href='".$PathRelative."src/module/calendar/index.php?time=".$ts."&calViewPeriod=daily'>";
						$calendar .= $numOfEvent.' ';
						$calendar .= "<img src='".$SYS_CONFIG['Image']['Abs']."icon_event_s.gif' width='13' height='14' border='0' align='absmiddle' class='cal_event_blue'>";
						for($m=0 ; $m<$rowSpan[$i][$j] ; $m++){ $calendar .= '<br>';}
						$calendar .= "</a>";
						$calendar .= "</div>";
					} else {
						$calendar .= "&nbsp;";
					}
					$calendar .= "</td>";
				}
			}
			$calendar .= "</tr>";
		}
		$calendar .= "</tbody></table>";
		return $calendar;
    }
	/*  *** End of Weekly View of Small Calendar ***  */
		
	/*  *** Agenda View of Index Page ***  */
	function Print_Brief_Agenda($startDate, $endDate, $response){
		global $SYS_CONFIG, $today_events, $coming_events, $ImagePathAbs, $lui, $iCalendar_Agenda_NoEventsForThisMonment;
		
		$response = ($response=="") ? array("A","D","M","W","") : array($response);
		
		# Get calendar color
		$viewableCal = $this->returnViewableCalendar();
		for ($i=0; $i<sizeof($viewableCal); $i++) {
			$viewableCalColor[$viewableCal[$i]["CalID"]] = $viewableCal[$i]["Color"];
			$viewableCalVisible[$viewableCal[$i]["CalID"]] = (int)$viewableCal[$i]["Visible"];
		}
				
		$cnt = 0;
		$x  = '<table width="95%" border="0" cellspacing="0" cellpadding="0">';
		# Today Event
		$x .= '<tr><td class="cal_agenda_title" colspan="2">Today</td></tr>';
		if(sizeOf($today_events) > 0) {
			$cnt = 0;
			foreach($today_events as $evt_date => $evt){
				if(count($evt) > 0){
					for($i=0 ; $i<count($evt) ; $i++){
						if (in_array($evt[$i]["Status"], $response)) {
							$CalType = trim($evt[$i]['CalType']);
							$eventDateTimePiece = explode(" ", $evt[$i]["EventDate"]);
							$eventDate = $eventDateTimePiece[0];
							$eventStartTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
							$eventEndTs = $eventStartTs + $evt[$i]["Duration"]*60;
							
							if ($eventStartTs==$eventEndTs) {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs);
								}
							} else {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
								}
							}
							$isAllDay = (int)$evt[$i]["IsAllDay"];
							$createdBy = $this->returnUserFullName($evt[$i]["UserID"]);
							
							$display = $viewableCalVisible[$evt[$i]["CalID"]]==1?"block":"none";
							
							# Check whether it is valid Foundation Event or not
							# added on 25 Feb 2009
						/*	if($CalType == 2 && ( 
								($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV') || 
								(($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV') && $evt[$i]['Owner'] != $UserID)
								) ){
								$isFoundation = true;
							} else {
								$isFoundation = false;
							}
							$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
							*/
							# Get Corresponding Color of each Event
							$ColorID = '';
							/*if($isOtherCalType){
								$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$params  = "editEventID=event".$evt[$i]["EventID"]."&CalType=$CalType";
								$params .= "&programmeID=".$evt[$i]["ProgrammeID"];
							} else if($isFoundation){
								$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$params  = "editEventID=event".$evt[$i]["EventID"];
							} else {*/
								$ColorID = $this->Get_Calendar_ColorID($viewableCalColor[$evt[$i]["CalID"]]);
								$ColorID = ($ColorID == "") ? $this->involveCalColorID : $ColorID;
								$params  = "editEventID=event".$evt[$i]["EventID"];
						//	}
							
							$x .= '<tr>';
							$x .= '<td style="width:2%">';
							$x .= '<img class="cal_event_png_'.$ColorID.'" width="15" height="15" border="0" align="absmiddle" src="'.$ImagePathAbs.'calendar/event_'.$ColorID.'.png"/>';
							$x .= '</td>';
							$x .= '<td class="cal_agenda_entry" style="width:98%">';
							$x .= '<a href="src/module/calendar/new_event.php?'.$params.'">';
							$x .= $evt[$i]["Title"]. $this->AppendOnwerName($evt[$i]["UserID"]);
							if (trim($evt[$i]["PersonalNote"]) != ""){
								$x .= ' <img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0" >';
							}
							$x .= ' </a>';
							$x .= '<span class="day_time">'.($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime).'</span>';
							$x .= '</td>';
							$x .= '</tr>';
							
							$cnt++;
						}
					} # End for 
				} # End if
			} # End foreach
		} else {
			$x .= '<tr><td class="cal_agenda_entry" colspan="2"><span class="day_time">'.$iCalendar_Agenda_NoEventsForThisMonment.'</span></td></tr>';
		} 
		
		# Coming Events
		$x .= ' <tr><td class="cal_agenda_title" colspan="2">Coming</td></tr>';
		if(count($coming_events) > 0){
			foreach($coming_events as $evt_date => $evt){
				if(count($evt) > 0){
					for($i=0 ; $i<count($evt) ; $i++){
						if (in_array($evt[$i]["Status"], $response)) {
							$CalType = trim($evt[$i]['CalType']);
							$eventDateTimePiece = explode(" ", $evt[$i]["EventDate"]);
							$eventDate = $eventDateTimePiece[0];
							$eventStartTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
							$eventEndTs = $eventStartTs + $evt[$i]["Duration"]*60;
							
							if ($eventStartTs==$eventEndTs) {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs);
								}
							} else {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
								}
							}
							$isAllDay = (int)$evt[$i]["IsAllDay"];
							$createdBy = $this->returnUserFullName($evt[$i]["UserID"]);
							
							$display = $viewableCalVisible[$evt[$i]["CalID"]]==1?"block":"none";
							
							# Check whether it is valid Foundation Event or not
							# added on 25 Feb 2009
							if($CalType == 2 && ( 
								($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV') || 
								(($_SESSION['SchoolCode'] == 'ESFC' || $_SESSION['SchoolCode'] == 'DEV') && $evt[$i]['Owner'] != $UserID)
								) ){
								$isFoundation = true;
							} else {
								$isFoundation = false;
							}
							$isOtherCalType = $this->Is_Other_Calendar_Type($CalType);
							
							# Get Corresponding Color of each Event
							$ColorID = '';
							if($isOtherCalType){
								$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$params  = "editEventID=event".$evt[$i]["EventID"]."&CalType=$CalType";
								$params .= "&programmeID=".$evt[$i]["ProgrammeID"];
							} else if($isFoundation){
								$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$params  = "editEventID=event".$evt[$i]["EventID"];
							} else {
								$ColorID = $this->Get_Calendar_ColorID($viewableCalColor[$evt[$i]["CalID"]]);
								$ColorID = ($ColorID == "") ? $this->involveCalColorID : $ColorID;
								$params  = "editEventID=event".$evt[$i]["EventID"];
							}

							$x .= '<tr>';
							$x .= '<td style="width:2%">';
							$x .= '<img class="cal_event_png_'.$ColorID.'" width="15" height="15" border="0" align="absmiddle" src="'.$ImagePathAbs.'calendar/event_'.$ColorID.'.png"/>';
							$x .= '</td>';
							$x .= '<td class="cal_agenda_entry" style="width:98%">';
							$x .= '<a href="src/module/calendar/new_event.php?'.$params.'">';
							$x .= $evt[$i]["Title"]. $this->AppendOnwerName($evt[$i]["UserID"]);
							if (trim(UTF8_From_DB_Handler($evt[$i]["PersonalNote"])) != ""){
								$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
							}
							$x .= ' </a>';
							$x .= '<span class="day_time">'.($isAllDay?$iCalendar_NewEvent_IsAllDay:$eventTime).'</span>';
							$x .= '</td>';
							$x .= '</tr>';
							  
							
							$cnt++;
						}
					} # End for
				} # End if
			} # End foreach
		} else {
			$x .= '<tr><td class="cal_agenda_entry" colspan="2"><span class="day_time">No Records at this moment.</span></td></tr>';
		}
		  
		$x .= '</table>';
		$table_brief_agenda = $x;
		return $table_brief_agenda;
	}	
	/*  *** End of Agenda View of Index Page ***  */
	
	
	function Get_Other_School_Invitation_Event($lower_bound, $upper_bound){
		$uid = $this->getCurrentUserID();
		
		$iCalApi = new icalendar_api($uid); 
		$startDate = date('Y-m-d',$lower_bound);
		$endDate = date('Y-m-d',$upper_bound);
		return $iCalApi->Get_Data("otherSchool", $startDate, $endDate);
	}
	
	function PreloadOwnerNames($events=""){
		global $events_ownernames, $plugin;
		$events_owners_ids = Array();
		global $UserID;

		foreach ($events as $Key => $Arr)
		{
			$EventDates[] = $Key;
			for ($i=0; $i<sizeof($Arr); $i++)
			{
				if ($Arr[$i]["UserID"]!="" && !in_array($Arr[$i]["UserID"], $events_owners_ids))
				{
					$events_owners_ids[] = $Arr[$i]["UserID"];
				}
			}
		}
		
		if ($plugin["iCalendarShowOwnerInTitle"])
		{
			$events_ownernames = $this->returnUsersFullNames($events_owners_ids);
		}
	}
	
	function sortEventCompareMethod($e1, $e2)
	{
		$e1_date = substr($e1['EventDate'],0,10); // truncate the time component
		$e2_date = substr($e2['EventDate'],0,10);
		$e1_is_all_day = $e1['IsAllDay']==1? 1 : 0; // cater null value and empty value
		$e2_is_all_day = $e2['IsAllDay']==1? 1 : 0;
		if(strcmp($e1_date,$e2_date)==0) // same day
		{
			if($e1_is_all_day == $e2_is_all_day){
				return strcmp($e1['EventDate'],$e2['EventDate']);
			}else if($e1_is_all_day > $e2_is_all_day){
				// IsAllDay is 1 is put at the front, IsAllDay is 0 put at the tail
				return -1;
			}else if($e1_is_all_day < $e2_is_all_day){
				return 1;
			}else{
				return 0;
			}
		}else{
			// sort events by date
			return strcmp($e1['EventDate'],$e2['EventDate']);
		}
	}
	
	function Get_All_Related_Event($lower_bound, $upper_bound, $getMultipleDayEvent=false,$searchValue='',$useTempTable=false){
		global $plugin;
		
		$uid = $this->getCurrentUserID();
		
		//$searchValue=$this->Get_Safe_Sql_Query(urldecode(trim(stripslashes($searchValue))));
		$own_events = $this->Query_User_Related_Events($lower_bound, $upper_bound, false, false, true, false, $getMultipleDayEvent,$searchValue,$useTempTable);		# Personal Event & School Event
		//debug_pr($own_events);
		$involve_events = $this->Query_User_Related_Events($lower_bound, $upper_bound, true, false, true, false,$getMultipleDayEvent,$searchValue,$useTempTable);	# Involved Event 
		//$foundation_events = $this->Query_User_Foundation_Events($lower_bound, $upper_bound, false, true,$getMultipleDayEvent);	# Foundation Event
		//$other_caltype_events = $this->Query_User_Other_CalType_Events($lower_bound, $upper_bound);			# Other CalType Event
		#other school invitation event
		//$other_school_invitation_event = $this->Get_Other_School_Invitation_Event($lower_bound, $upper_bound);
		$externalEvent = array();
		//if ($plugin["iCalendarFull"]&&$_SESSION['UserType']==USERTYPE_STAFF){
		$iCal_api = new icalendar_api($uid);
		$externalEvent = $iCal_api->getAllExternalRelatedEvent($lower_bound, $upper_bound,$searchValue,$useTempTable); 
		//debug_pr($externalEvent);
		if ($useTempTable)
			return;
		//}
		// $schoolEvent = $iCal_api->getAllSchoolEvent($lower_bound, $upper_bound);
		// $HomeworkEvent = $iCal_api->getRelatedHomeworkEvent($lower_bound, $upper_bound);
// debug_r($schoolEvent);		
		$total_events = array_merge($own_events, $involve_events);
		// $total_events = array_merge($total_events, $schoolEvent);
		// debug_r($total_events);
		//$total_events = array_merge($total_events, $foundation_events);
		//$total_events = array_merge($total_events, $other_caltype_events);
		//$total_events = array_merge($total_events, $other_school_invitation_event);
		
		// pls note that $externalEvent uses date as key to group events, so merge it into $total_events first, then sort all day events at top, after that sort events by event date
		if(count($externalEvent)>0){
			foreach($externalEvent as $date_key => $event_ary){
				$total_events = array_merge($total_events,$event_ary);
			}
		}
		//debug_pr($total_events);
		usort($total_events,array($this,'sortEventCompareMethod'));
		
		# Sorting after combining 2 events arrays
		$prevMonthEvent = array();
	//	$counting = 0;
// 		debug_pr($total_events);
		
		if(count($total_events) > 0){
			foreach($total_events as $key => $rows){	
				//if (!isset($searchValue)){
					$lowBoundMonth = date('Y-m-d', $lower_bound);
					$evtMonthTemp = date('Y-m-d', strtotime($rows['EventDate']));
			//	}
				//if ($rows['EventID'] == "7574")
					//$counting++;
					
				$evtEnd = date("Y-m-d",strtotime('+'.$rows['Duration']." minute",strtotime($rows['EventDate'])));
				if ($getMultipleDayEvent && $evtMonthTemp < $lowBoundMonth && $evtEnd > $lowBoundMonth){
					//$prevMonthEvent[]['EventDate'] = $rows['EventDate'];
					$prevMonthEvent[] = $rows;					
				}
				$column[$key] = $rows['EventDate'];
			}
//			array_multisort($column, SORT_ASC, SORT_STRING, $total_events);
			// debug_r($column);
		}
		
		// echo $counting;
		// exit;
		
		$events = $this->Return_Query_Events_in_Associated_Array($total_events);
// 		debug_pr($events);
		
		//if ($searchValue)){
			$lowBoundMonth = date('Y-m-d', $lower_bound);
			if ($getMultipleDayEvent && count($prevMonthEvent)>0){
				foreach($prevMonthEvent as $tempEvt){		
					$events[$lowBoundMonth][] = $tempEvt;
				}
				
			}
		//}
		
		////$events = array_merge_recursive($events, $externalEvent);
		
		//debug_pr($events);
		

		# preload related event owner names
		
		$this->PreloadOwnerNames($events);

		return $events;
		// $EventDate = sort($TmpEventDate,SORT_STRING);
		/*
		foreach($events as $Key => $Arr) {
			$EventDates[] = $Key;
		}
		if (is_array($EventDates))
		{
			natsort($EventDates);
			foreach($EventDates as $Key => $EventDate) {
				$OrderedEvents[$EventDate] = $events[$EventDate];
			}
		}
		
		//return $events;
		return $OrderedEvents;
		*/
	}
	
	# Check whether the provided Calendar Type belongs to Other Calendar Type other than 0, 1, 2
	function Is_Other_Calendar_Type($CalType){
		$result = false;
		if($CalType != ''){
			$settings = $this->Get_Other_CalType_Settings();
			if(count($settings) > 0){
				for($i=0 ; $i<count($settings) ; $i++){
					if($settings[$i]['CalType'] == $CalType)
						$result = true;
				}
			}
		}
		return $result;
	}
	
	# Get Other Type Calendar Info
	function returnOtherCalTypeCalendar($ManageType='', $CalType=''){
		$result = array();
		$sql = "SELECT 
					a.CalID, a.Owner, a.Name, a.DefaultEventAccess, a.SyncURL, a.CalType, 
					a.CalSharedType, b.Color, b.Access, c.CalTypeName, c.ManageType, c.ManageTypeName, 
					c.RoleRightName, c.RoleRightCondition 
				FROM 
					CALENDAR_CALENDAR AS a
				INNER JOIN CALENDAR_CALENDAR_VIEWER AS b ON 
					b.CalID = a.CalID 
				INNER JOIN CALENDAR_CALENDAR_TYPE_SETTINGS AS c ON 
					c.CalType = a.CalType  
				".(($ManageType != '') ? "AND c.ManageType = '$ManageType' " : "")."
				".(($CalType != '') ? "AND c.CalType = '$CalType' " : "")."
				where b.CalType = 3
				ORDER BY a.CalID";
		$result = $this->returnArray($sql);
		return $result; 
	}
	
	# Get Other Calendar Type Settings
	function Get_Other_CalType_Settings($CalType=''){
		$returnArr = array();
		$sql = "SELECT CalType, CalTypeName, ManageType, ManageTypeName, RoleRightName, RoleRightCondition  
				FROM CALENDAR_CALENDAR_TYPE_SETTINGS ";
		$sql .= ($CalType != '') ? "WHERE CalType = '$CalType'" : "";
		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}
	
	# Get All Other Cal Type Events in an array
	function Query_User_Other_CalType_Events($lower_bound, $upper_bound,$getMultipleDayEvent=false){
		$events = array();
		$temp = array();
		
		$uid = $this->getCurrentUserID();
		
		$iCalApi = new icalendar_api($uid);
		
		$start 	= date("Y-m-d", $lower_bound);
		$end 	= date("Y-m-d", $upper_bound);
		$result = $this->Get_Other_CalType_Settings();
		
		if(count($result) > 0){
			for($i=0 ; $i<count($result) ; $i++){
				$temp = $iCalApi->Get_Data($result[$i]['CalTypeName'], $start, $end);
				$events = array_merge($events, $temp);
			}
		}
		return $events;
	}
	
	# Set Style for other CalType event only
	function Set_Style_Other_CalType_Event($title, $evtArr){
		$title = ($evtArr['Bold']) ? "<b>$title</b>" : $title;			# bold
		$title = ($evtArr['Italic']) ? "<i>$title</i>" : $title;		# italic
		$title = ($evtArr['Underline']) ? "<u>$title</u>" : $title;		# underline
		return $title;
	}
	
	/* Testing only */
	function generate_cpd_testing_data($lower_bound, $upper_bound){
		$settings = $this->returnOtherCalTypeCalendar('', ENROL_CPD_CALENDAR);
		for($i=0 ; $i<10 ; $i++){
			$num = $i;
			# Assign Value
			$returnArr[$num]['EventID'] = 'CPD'.rand(10, 90);
			$returnArr[$num]['UserID'] = $settings[0]['UserID'];
			$returnArr[$num]['CalID'] = $settings[0]['CalID'];
			$returnArr[$num]['RepeatID'] = '';
			$returnArr[$num]['EventDate'] = '2009-03-'.rand(10, 30);
			$returnArr[$num]['Title'] = 'CPD Event '.rand(10, 5000);
			$returnArr[$num]['Description'] = '';
			$returnArr[$num]['Duration'] = '';
			$returnArr[$num]['IsImportant'] = '';
			$returnArr[$num]['IsAllDay'] = 1;
			$returnArr[$num]['Access'] = '';
			$returnArr[$num]['Location'] = 'Room '.rand(100, 500);
			$returnArr[$num]['Url'] = '';
			$returnArr[$num]['PersonalNote'] = '';
			$returnArr[$num]['CalType'] = ENROL_CPD_CALENDAR;
			$returnArr[$num]['Owner'] = 0;
			$returnArr[$num]['Color'] = $settings[0]['Color'];

		}
		return $returnArr;
	}
	
	/*
		To extract calendar event from iCalendar statement(RFC 2445)
		param:
			$icalStmt = iCalendar statement
		return value:
			1) false: for statment format error
			2) Array of event: 
				event[]["uid"] = event identifier
				event[]["extra"] = event extra info
				event[]["title"] = title of event
				event[]["description"] = description of event
				event[]["location"] = location of event
				event[]["allDay"] = whether it is all day event
				event[]["inputDay"] = day for the creation of event
				event[]["lastModified"] = day for the latest modification of event
				event[]["start"] = start of event (in db format)
				event[]["end"] = event of event (in db format)
				event[]["rule"] = rule of repeated event <--- reference to get_icalendar_rule($rule)
	*/
	function extract_icalEvent($icalStmt){
		$icalStmt = rtrim($icalStmt,"\n\r \t");
		$icalStmt = ltrim($icalStmt,"\n\r \t");
		
		//echo $icalStmt;
		//exit;
		//if (!ereg("^BEGIN:VCALENDAR.*END:VCALENDAR$",$icalStmt)){
		if (!preg_match('/^BEGIN:VCALENDAR.*END:VCALENDAR$/s',$icalStmt)){
			//echo "error in format<br>";
			//echo "$icalStmt<br>";
			//exit;
			return false;
		}
		$event = Array();
					
		$tok = strtok($icalStmt, "\x0A");
		while($tok !== false){
			$tok = rtrim($tok,"\x0A\x0D");
			$tok = ltrim($tok,"\x20\x09");
			//echo $tok."<br>";
			if ($tok == "BEGIN:VEVENT"){
				$eventprop= $this->get_Event_detail($tok);
				
				if ($eventprop==false){
					return false;
				}
				$event[] = $eventprop;
								
			}
			$tok = strtok("\x0A");
		}
		
		if(count($event)>0){
			usort($event, "sort_event_cmp_func");
		}
		return $event;
	}
	
	/*
		To make iCalendar statement(RFC 2445) from event
		param:
			$events: An array of events
				event[]["uid"] = event identifier
				event[]["extra"] = event extra info
				event[]["title"] = title of event
				event[]["description"] = description of event 
				event[]["location"] = location of event
				event[]["allDay"] = whether it is all day event
				event[]["inputDay"] = day for the creation of event
				event[]["lastModified"] = day for the latest modification of event
				event[]["start"] = start of event (in db format)
				event[]["end"] = event of event (in db format)
				event[]["rule"] = rule of repeated event <--- reference to get_icalendar_rule($rule)
			
		return value:
			1) false if the size of array = 0;
			2) iCalendar statement(RFC 2445)
	*/
	function make_ical($events){
		//if (count($events) == 0)
			//return false;
		$stmt ="BEGIN:VCALENDAR\x0D\x0APRODID:-//BroadLearning Ld//BroadLearning Calendar//EN\x0D\x0AVERSION:2.0\x0D\x0ACALSCALE:GREGORIAN\x0D\x0AMETHOD:PUBLISH\x0D\x0AX-WR-TIMEZONE:Asia/Hong_Kong\x0D\x0ABEGIN:VTIMEZONE\x0D\x0ATZID:Asia/Hong_Kong\x0D\x0AX-LIC-LOCATION:Asia/Hong_Kong\x0D\x0ABEGIN:STANDARD\x0D\x0ATZOFFSETFROM:+0800\x0D\x0ATZOFFSETTO:+0800\x0D\x0ATZNAME:HKT\x0D\x0ADTSTART:19700101T000000\x0D\x0AEND:STANDARD\x0D\x0AEND:VTIMEZONE\x0D\x0A";
		
		//debug_r($events);
		
		for($i = 0; $i < count($events); $i++){
			$evt = $events[$i];
			
			$stmt .= "BEGIN:VEVENT\x0D\x0A";
			if ($evt["allDay"] == "true"){
				$stmt .= ("DTSTART;VALUE=DATE:".$this->toUTC($evt["start"],true)."\x0D\x0A");
				$stmt .= ("DTEND;VALUE=DATE:".$this->toUTC($evt["end"],true)."\x0D\x0A");
			}
			else{
				if (empty($evt["rule"])){
					$stmt .= "DTSTART:".$this->toUTC($evt["start"],false,true)."\x0D\x0A";
					$stmt .= "DTEND:".$this->toUTC($evt["end"],false,true)."\x0D\x0A";
				}
				else{
					$stmt .= ("DTSTART;TZID=Asia/Hong_Kong:".str_replace("Z","",$this->toUTC($evt["start"],false))."\x0D\x0A");
					$stmt .= ("DTEND;TZID=Asia/Hong_Kong:".str_replace("Z","",$this->toUTC($evt["end"],false))."\x0D\x0A");
				}
			}
			
			if (!empty($evt["rule"])){
				$rule = "RRULE:FREQ=".$evt["rule"]["freq"];
				if (!empty($evt["rule"]["interval"]) && $evt["rule"]["interval"] > 1){
					$rule .= (";INTERVAL=".$evt["rule"]["interval"]); 
				}
				if (!empty($evt["rule"]["until"])){
					$rule .= (";UNTIL=".$this->toUTC($evt["rule"]["until"],$evt["allDay"]=="true"?true:false,true)); 
				}
				if (!empty($evt["rule"]["byDay"])){
					$rule .= (";BYDAY=".$evt["rule"]["byDay"]);
				}
				if (!empty($evt["rule"]["byMonthDay"])){
					$rule .= (";BYMONTHDAY=".$evt["rule"]["byMonthDay"]);
				}
				$stmt .= ($rule."\x0D\x0A");				
			}
			
			if (!empty($evt["exDate"])){
				foreach ($evt["exDate"] as $exDate){
					if ($evt["allDay"] == "true"){
						$dateUTC = date("Ymd",($exDate-$this->offsetTimeStamp));
						$stmt .= "EXDATE;VALUE=DATE:".$dateUTC."\x0D\x0A";
					}
					else{
						$dateUTC = date("Ymd\THis",($exDate-$this->offsetTimeStamp));
						$stmt .= "EXDATE;TZID=Asia/Hong_Kong:".$dateUTC."\x0D\x0A";
					}
				}
				
			}
			
			//$class = "PUBLIC";
			//$stmt .= "CLASS:$class\x0D\x0A";
			$stmt .= "CREATED:".$this->toUTC($evt["inputDay"],false,true)."\x0D\x0A";
			$stmt .= "LAST-MODIFIED:".$this->toUTC($evt["lastModified"],false,true)."\x0D\x0A";
			
			
			$stmt .= $evt["extra"];
			$stmt .= $this->line_fold("UID:".$evt["uid"]);
			
			//echo "decription: ".$evt["description"]."<br>";
			$description = $this->format_statement($evt["description"]);
			//addcslashes($evt["title"],"\x2C\x3B\x5C");
			$title = $this->format_statement($evt["title"]);
			//addcslashes($evt["location"],"\x2C\x3B\x5C");
			$location = $this->format_statement($evt["location"]);
			
			//echo "decription: ".$evt["description"]."<br>";
			
			$title = "SUMMARY:".$title;
			$description = "DESCRIPTION:".$description;
			$location = "LOCATION:".$location;
			$stmt .= $this->line_fold($title);
			$stmt .= $this->line_fold($description);
			$stmt .= $this->line_fold($location);
			
			
			
			//echo "decription: ".$this->line_fold($evt["description"])."<br>";
			
			
			//$stmt .= "SEQUENCE:$i\x0D\x0A";
			//$stmt .= "TRANSP:OPAQUE\x0D\x0A";
			//$stmt .= "STATUS:CONFIRMED\x0D\x0A";
			$stmt .= "END:VEVENT\x0D\x0A";
			
			/*echo "############Debug#################";
			
			if ($i <= 5){ 
				echo "<br>".$i."<br>";
				debug_r($evt);
				echo $stmt;
			}
			else{
				echo $stmt;
				exit;
			}
			echo "############Debug#################";*/
			//$i++;			
		}
		
		$stmt .= "END:VCALENDAR\x0D\x0A";
		
		//echo $stmt;
		//exit;
		//exit;
		return $stmt;
	}
	
	/* To format the icalendar statement
		param: 
			$stmt: statement that are going to format
		return:
			formatted statement
	*/
	function format_statement($stmt){
		$stmt = addcslashes($stmt,"\x2C\x3B\x5C");
		$ele = Array("\x0D\x0A","\x0D","\x0A");
		$eleRep = Array("\\n","\\n","\\n");
		return str_replace($ele, $eleRep, $stmt);
	}
	
	
	/*
		To generate line folding when the length of statement > 75
		param:
			$stmt: the line that to be line fold
		return value:
			line folded statement
	*/
	function line_fold($stmt){
		if (strlen($stmt)<=75)
			return $stmt."\x0D\x0A";
		$head = substr($stmt,0,75);
		$tail = substr($stmt,75);
		return $head."\x0D\x0A ".$this->line_fold($tail);
	}
	
	/*
		To extract event detail from event statement in iCalendar statement(RFC 2445)
		$param:
			$stmt: event statement of iCalendar statement(RFC 2445)
		$return value:
			1) false if statement format error
			2) Array of event detail: 
				event["uid"] = event identifier
				event["extra"] = event extra info
				event["title"] = title of event
				event["description"] = description of event
				event["location"] = location of event
				event["allDay"] = whether it is all day event
				event["inputDay"] = day for the creation of event
				event["lastModified"] = day for the latest modification of event
				event["start"] = start of event (in db format)
				event["end"] = event of event (in db format)
				event["rule"] = rule of repeated event <--- reference to get_icalendar_rule($rule)
	*/
	function get_Event_detail($stmt){
		$title="";
		$description="";
		$location="";
		$allDay="";
		$inputDay="";
		$lastModified="";
		$start="";
		$end="";
		$rule = "";
		$current="";
		$recurrent = "";
		$exDate = Array();
		$tok = strtok("\x0A");
		$extra = "";
		while($tok !== false){
			if ($tok{0} == " " || $tok{0} == "\x09"){
				if (empty($current)){
					$tok = strtok("\x0A");
					continue;
				}
				if ($current == "extra"){
					$tok = rtrim($tok,"\x0A\x0D");
					$extra = $extra.$tok."\x0D\x0A"; 
					$tok = strtok("\x0A");
					continue;
				}
				$tok = rtrim($tok,"\x0A\x0D");
				$tok = substr($tok,1);
				$tok = $this->remove_slash($tok);
				$$current .= $tok;
				$tok = strtok("\x0A");
				continue;
			} 
			
			/*if (ereg("^ATTENDEE;",$tok) || ereg("^ORGANIZER;",$tok) ){
				$current = null;
				$tok = strtok("\x0A");
				continue;
			}*/
			
			$tok = rtrim($tok,"\x0A\x0D");
			//$tok = ltrim($tok,"\x20\x09");
			
			
			if (preg_match('/^DTSTART:.*/s',$tok)){
				//echo "start: ".$tok."<br><br>";
				//echo "start initial: ".$this->fromUTC(substr($tok, 8))."<br><br>";
				$start = $this->fromUTC(substr($tok, 8),false,true);
				//echo "start phrased: ".$start."<br><br>";
				$current = "start";
				$allDay="false";
			}
			elseif (preg_match('/^DTEND:.*/s',$tok)){
				$end = $this->fromUTC(substr($tok, 6),false,true);
				$current = "end";
			}
			elseif (preg_match('/^DTSTART;VALUE=DATE:.*/s',$tok)){
				//echo "start: ".$tok."<br><br>";
				//echo "start initial: ".$this->fromUTC(substr($tok, 8))."<br><br>";
				$start = $this->fromUTC(substr($tok, 19),true);
				//echo "start phrased: ".$start."<br><br>";
				$allDay = "true";
				$current = "start";
			}
			elseif (preg_match('/^DTEND;VALUE=DATE:.*/s',$tok)){
				$end = $this->fromUTC(substr($tok, 17),true);
				$current = "end";
			}
			elseif (preg_match('/^DTSTART;TZID=.*/s',$tok)){
				//echo "start: ".$tok."<br><br>";
				//echo "start initial: ".$this->fromUTC(substr($tok, 8))."<br><br>";
				//$start = date("Y-m-d H:i:s.000",strtotime("+8 hour",strtotime(substr($tok,strpos($tok,":")+1))));
				$start = date("Y-m-d H:i:s.000",strtotime(substr($tok,strpos($tok,":")+1)));
				//echo "start phrased: ".$start."<br><br>";
				$current = "start";
				$allDay="false";
			}
			elseif (preg_match('/^DTEND;TZID=.*/s',$tok)){
				//$end = date("Y-m-d H:i:s.000",strtotime("+8 hour",strtotime(substr($tok,strpos($tok,":")+1))));
				$end = date("Y-m-d H:i:s.000",strtotime(substr($tok,strpos($tok,":")+1)));
				$current = "end";
			}
			elseif (preg_match('/^RECURRENCE-ID;VALUE=DATE:.*/s',$tok)){ 
				$recurrent = $this->fromUTC(substr($tok, 25),false,false);
				$extra = $extra.$tok."\x0D\x0A";
			}
			elseif (preg_match('/^RECURRENCE-ID:.*/s',$tok)){
				$recurrent = $this->fromUTC(substr($tok, 14),false,true);
				$extra = $extra.$tok."\x0D\x0A";
			}
			elseif (preg_match('/^RECURRENCE-ID;TZID=.*/s',$tok)){
				$recurrent = date("Y-m-d H:i:s.000",strtotime(substr($tok,strpos($tok,":")+1)));
				$extra = $extra.$tok."\x0D\x0A";
			}
			elseif (preg_match('/^EXDATE:.*/s',$tok)){
				$exDate[] = $this->fromUTC(substr($tok, 7),false,true);
			}
			elseif (preg_match('/^EXDATE;TZID=.*/s',$tok)){
				$exDate[] = date("Y-m-d H:i:s.000",strtotime(substr($tok,strpos($tok,":")+1)));
			}
			elseif (preg_match('/^EXDATE;VALUE=DATE:.*/s',$tok)){
				$exDate[] = date("Y-m-d H:i:s.000",strtotime(substr($tok,strpos($tok,":")+1)));
			}
			elseif (preg_match('/^RRULE:.*/s',$tok)){
				$rule = $this->get_icalendar_rule($tok);
				$current = "rule";
			}
			elseif (preg_match('/^UID:.*/s',$tok)){
				$uid = substr($tok, 4);
				$current = "uid";
			}
			elseif (preg_match('/^CREATED:.*/s',$tok)){
				$inputDay = $this->fromUTC(substr($tok, 8),false,true);
				$current = "inputDay";
			}
			elseif (preg_match('/^LAST-MODIFIED:.*/s',$tok)){
				$lastModified = $this->fromUTC(substr($tok, 14),false,true);
				$current = "lastModified";
			}
			elseif (preg_match('/^LOCATION:.*/s',$tok)){
				$location = substr($tok, 9);
				$location = $this->remove_slash($location);
				$current = "location";
			}
			elseif (preg_match('/^LOCATION(;.*):(.*)/s',$tok,$m)) {
				$location = $m[2];
				$location = $this->remove_slash($location);
				$current = "location";
			}
			elseif (preg_match('/^SUMMARY:.*/s',$tok)){
				//echo $tok."<br><br>";
				$title = substr($tok, 8);
				$title = $this->remove_slash($title);
				$current = "title";
			}
			elseif (preg_match('/^SUMMARY(;.*):(.*)/s',$tok, $m)) {
				$title = $m[2];
				$title = $this->remove_slash($title);
				$current = "title";
			}
			elseif (preg_match('/^DESCRIPTION:.*/s',$tok)){
				if($description == "") {
					$description = substr($tok, 12);
					$description = $this->remove_slash($description);
					$current = "description";
				}
			}
			elseif (preg_match('/^END:VEVENT.*/s',$tok)){
				return Array("extra"=>$extra, "uid"=>$uid, "title"=>$title, "description"=>$description, "location"=>$location, "allDay"=>$allDay, "inputDay"=>$inputDay, "lastModified"=>$lastModified, "start"=>$start, "end"=>$end, "rule"=>$rule, "recurrent"=>$recurrent, "exDate"=>$exDate);
			}
			else{
				$extra = $extra.$tok."\x0D\x0A"; 
				$current = "extra";
			}
			
			$tok = strtok("\x0A");
		}
		return false;
	}
	
	/*
		To get the rule of repeated event
		param:
			$rule = rule in icalendar statement
		return value:
			array of rules
				rule["freq"] = repeating frequency of the event. DAILY, MONTHLY, WEEKLY, YEARLY
				rule["interval"] = the number of interval between each event
				rule["byDay"] = array of event day. SU, MO, TU, WE, TH, FR, SA. 
								1WE = first wednesday of month, -1WE = last wednesday of month
				rule["byMonthDay"] = the date of the month.
				rule["until"] = the end of repeated event
	*/
	function get_icalendar_rule($rule){
		$freq = "";
		$interval = 0;
		$until = "";
		$first = 6;
		$byDay = "";
		$byMonthDay = "";
		while($pos !== false){	
			$pos = strpos($rule, ";");		
			$token = $pos!==false?substr($rule,$first,$pos-$first):substr($rule,$first);
			$rule = substr($rule,$pos+1);
			if (preg_match('/^FREQ.*/s',$token)){
				$freq = substr($token,5);
			}
			elseif (preg_match('/^INTERVAL.*/s',$token)){
				$interval = substr($token,9);
			}
			elseif (preg_match('/^BYDAY.*/s',$token)){
				$byDay = substr($token,6);
			}
			elseif (preg_match('/^UNTIL.*/s',$token)){
				$until = substr($token,6);
				if ($until{strlen($until)-1} == 'Z'){
					$until=$this->fromUTC($until,false,true);
				}
				else{
					$until=$this->fromUTC($until,true);
				}
			}
			elseif (preg_match('/^BYMONTHDAY.*/s',$token)){
				$byMonthDay = substr($token,11);
			}
			elseif (preg_match('/^COUNT.*/s',$token)){
				$count = substr($token,6);
			} 
			$first = 0;
		}
		return Array("freq"=>$freq,"interval"=>$interval,"until"=>$until,"byDay"=>$byDay,"byMonthDay"=>$byMonthDay,"count"=>$count);
	}
	
	/*
		To calculate the time different in minute (db format)
		param:
			$date1: first date 
			$date2: second date
		return value:
			time different in minute
	*/
	function time_diff($date1,$date2){
		$date1 = date("Y-m-d H:i:s",strtotime($date1));
		$date2 = date("Y-m-d H:i:s",strtotime($date2));
		$year = strtok($date1,"- :.");
		$month = strtok("- :.");
		$day = strtok("- :.");
		$hr = strtok("- :.");
		$min = strtok("- :.");
		$second = strtok("- :.");
		$date1 = mktime($hr, $min, $second, $month, $day, $year);
		$year = strtok($date2,"- :.");
		$month = strtok("- :.");
		$day = strtok("- :.");
		$hr = strtok("- :.");
		$min = strtok("- :.");
		$second = strtok("- :.");
		$date2 = mktime($hr, $min, $second, $month, $day, $year);
		$datediff = $date2-$date1;
		return floor($datediff/(60));
	}
	
	/*
		To calculate the time different in minute (UTC format)
		param:
			$date1: first date 
			$date2: second date
		return value:
			time different in minute
	*/
	function time_diff_UTC($date1,$date2){
		$date1 = date("Ymd\THis",strtotime($date1));
		$date2 = date("Ymd\THis",strtotime($date2));
		$year = substr($date1,0,4);
		$month = substr($date1,4,2);
		$day = substr($date1,6,2);
		$hr = substr($date1,9,2);
		$min = substr($date1,11,2);
		$sec = substr($date1,13,2);
		$date1 = mktime($hr, $min, $second, $month, $day, $year);
		$year = substr($date2,0,4);
		$month = substr($date2,4,2);
		$day = substr($date2,6,2);
		$hr = substr($date2,9,2);
		$min = substr($date2,11,2);
		$sec = substr($date2,13,2);
		$date2 = mktime($hr, $min, $second, $month, $day, $year);
		$datediff = $date2-$date1;
		return floor($datediff/(60));
	}
	
	/*
		To convert db time format to UTC time format used in iCalendar statement(RFC 2445)
		param:
			$date: the date in db time format
			$isAllDay: whether it is all day event
		return value:
			date in UTC time format used in iCalendar statement(RFC 2445)
	*/ 
	function toUTC($date,$isAllDay=false,$isTimeShift=false){
		$date = date("Y-m-d H:i:s.000", strtotime($date));
		$year = strtok($date,"- :.");
		$month = strtok("- :.");
		$day = strtok("- :.");
		$hr = strtok("- :.");
		$min = strtok("- :.");
		$second = strtok("- :.");
		$utc = "";
		if ($isAllDay)
			$utc = date("Ymd", mktime(0, 0, 0, $month, $day, $year)); 
		else{
			if ($isTimeShift)
				$utc = date("Ymd\THis\Z", strtotime("-8 hour", mktime($hr, $min, $second, $month, $day, $year)));
			else
				$utc = date("Ymd\THis\Z", mktime($hr, $min, $second, $month, $day, $year));
		}
		return $utc;
	}
	
	/*
		To convert UTC time format used in iCalendar statement(RFC 2445) to db time format 
		param:
			$date: the date in UTC time format used in iCalendar statement(RFC 2445)
			$isAllDay: whether it is all day event
		return value:
			date in db time format
	*/
	function fromUTC($date,$isAllDay=false,$timeShift=false){
		//echo $date."<br><br>";
		$year = substr($date,0,4);
		$month = substr($date,4,2);
		$day = substr($date,6,2);
		$newDate = "";
		if ($isAllDay){
			//$newDate = date("Y-m-d H:i:s.000", mktime(0, 0, 0, $month, $day, $year));
			$newDate = $year."-".$month."-".$day." 00:00:00.000";
		}
		else{
			$hr = substr($date,9,2);
			$min = substr($date,11,2);
			$sec = substr($date,13,2);
			if($hr == '') $hr = '00';
			if($min == '') $min = '00';
			if($sec == '') $sec = '00';
			//$newDate = date("Y-m-d H:i:s.000", mktime($hr, $min, $sec, $month, $day, $year));
			if ($timeShift){
				$newDate = date("Y-m-d H:i:s.000", strtotime("+8 hour", strtotime($year.$month.$day."T".$hr.$min.$sec)));
			}
			else
				$newDate = $year."-".$month."-".$day." ".$hr.":".$min.":".$sec.".000";
			
		}		
		return $newDate;
	}
	
	/*
		To remove slash in excaping character used in iCalendar statement(RFC 2445)
		param:
			$stmt: statement that are going to remove slash
		return value:
			slash removed statement
	*/
	function remove_slash($stmt){
		$ele = array("\\\\", "\\n", "\\");
		$eleToRep  = array("\\", "\x0D\x0A", "");
		return str_replace($ele, $eleToRep, $stmt);
	}
	
	/*
		To store the iCalendar event to database
		param:
			$events: An array of events
				event[]["uid"] = event identifier
				event[]["extra"] = event extra info
				event[]["title"] = title of event
				event[]["description"] = description of event
				event[]["location"] = location of event
				event[]["allDay"] = whether it is all day event
				event[]["inputDay"] = day for the creation of event
				event[]["lastModified"] = day for the latest modification of event
				event[]["start"] = start of event (in db format)
				event[]["end"] = event of event (in db format)
				event[]["rule"] = rule of repeated event <--- reference to get_icalendar_rule($rule)
			$callD: the calendar id			
	*/
	function icalEvent_to_db($events,$calID){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$sql = "SELECT Owner FROM CALENDAR_CALENDAR WHERE CalID='$calID'";
		$record = $this->returnVector($sql);
		$CalOwnerID = $record[0];
		if($CalOwnerID == 0){
			$CalOwnerID = $uid;
		}
		
		$result = array();
		//if (count($event == 0))
			//return true;
		$repeatedEvt = Array();
		$normalEvt = Array();
		foreach($events as $evt){
			if (empty($evt["rule"]))
				$normalEvt[] = $evt;
			else
				$repeatedEvt[] = $evt;
		}
		//$events = array_merge($repeatedEvt,$normalEvt);
		foreach ($events as $key => $event){
			//$duration = 0;
			//debug_pr($event);
			//if (!$event["allDay"]){
			$duration = $this->time_diff($event["start"],$event["end"]);
			//}
			
			$fieldname  = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
			$fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID, UID, ExtraIcalInfo"; 
			
			$isImportant = 0;
			$access = "P";
			$title = $event["title"];
			$description = $event["description"];
			$location = $event["location"];
			$uid = $event["uid"];
			$extra = $event["extra"];
			$url = "";
			$resultSet = Array();
			
			$sql = "select * from CALENDAR_EVENT_ENTRY where UID = '$uid' and CalID = '$calID'";
			$resultSet = $this->returnArray($sql);
			
			
			if (!empty($resultSet)){
				//case for update event
				if (empty($event["rule"])){
					//case for normal event
					$extraConstraint = "";
					if (count($resultSet) > 1){
						if (!empty($event["recurrent"])){ # it is recurrent event
							$sql = "select * from CALENDAR_EVENT_ENTRY where UID = '$uid' and CalID = '$calID' and UNIX_TIMESTAMP(EventDate) = '".(strtotime($event["recurrent"])+$this->offsetTimeStamp)."'";
							$resultSet = $this->returnArray($sql);
							if ($resultSet > 0)
								$extraConstraint = " And EventID = '".$resultSet[0]["EventID"]."'";
							else{
								$sql = "insert into CALENDAR_EVENT_ENTRY($fieldname) values(";
								$sql .= "'$CalOwnerID', ";
								$sql .= "'".$event["start"]."', ";
								$sql .= "'".$event["inputDay"]."', ";
								$sql .= "'".$event["lastModified"]."', ";
								$sql .= "'$duration', "; 
								$sql .= "'$isImportant', ";
								$sql .= "'".($event["allDay"]=="true"?1:0)."', ";
								$sql .= "'$access', ";
								$sql .= "'".$this->Get_Safe_Sql_Query($title)."', ";
								$sql .= "'".$this->Get_Safe_Sql_Query($description)."', ";
								$sql .= "'".$this->Get_Safe_Sql_Query($location)."', ";
								$sql .= "'$url', ";
								$sql .= "'".$calID."', ";
								$sql .= "'$uid', ";
								$sql .= "'$extra')";
								$result["insert_repeated_".$key] = $this->db_db_query($sql);
								continue;
							}
						}
						else{ #cover the existing repeated event
							//remove all existing repeated events
							$sql = "delete from CALENDAR_EVENT_ENTRY where RepeatID = '".$resultSet[0]["RepeatID"]."' OR (RepeatID is null and UID = '".$resultSet[0]["UID"]."') AND CalID = '$calID'";
							$result["update_delete_repeated_".$key] = $this->db_db_query($sql);
							$sql = "UPDATE CALENDAR_EVENT_ENTRY SET ";
							//$sql .= "UserID = '$UserID', ";
							$sql .= "RepeatID = NULL, ";
							$sql .= "EventDate = '".$event["start"]."', ";
							$sql .= "InputDate = '".$event["inputDay"]."', ";
							$sql .= "ModifiedDate = '".$event["lastModified"]."', ";
							$sql .= "Duration = '$duration', ";
							$sql .= "IsImportant = '$isImportant', ";
							$sql .= "IsAllDay = '".($event["allDay"]=="true"?1:0)."', ";
							$sql .= "Access = '$access', ";
							$sql .= "Title = '".$this->Get_Safe_Sql_Query($title)."', ";
							$sql .= "Description = '".$this->Get_Safe_Sql_Query($description)."', ";
							$sql .= "Location = '".$this->Get_Safe_Sql_Query($location)."', ";
							$sql .= "Url = '$url', ";
							$sql .= "CalID = '".$calID."', ";
							$sql .= "ExtraIcalInfo = '$extra' ";
							$sql .= "where UID = '$uid'".$extraConstraint;
							$result["update_single_".$key] = $this->db_db_query($sql);
							continue;
						}
					}
					$sql = "UPDATE CALENDAR_EVENT_ENTRY SET ";
					//$sql .= "UserID = '$UserID', ";
					$sql .= "RepeatID = NULL, ";
					$sql .= "EventDate = '".$event["start"]."', ";
					$sql .= "InputDate = '".$event["inputDay"]."', ";
					$sql .= "ModifiedDate = '".$event["lastModified"]."', ";
					$sql .= "Duration = '$duration', ";
					$sql .= "IsImportant = '$isImportant', ";
					$sql .= "IsAllDay = '".($event["allDay"]=="true"?1:0)."', ";
					$sql .= "Access = '$access', ";
					$sql .= "Title = '".$this->Get_Safe_Sql_Query($title)."', ";
					$sql .= "Description = '".$this->Get_Safe_Sql_Query($description)."', ";
					$sql .= "Location = '".$this->Get_Safe_Sql_Query($location)."', ";
					$sql .= "Url = '$url', ";
					$sql .= "CalID = '".$calID."', ";
					$sql .= "ExtraIcalInfo = '$extra' ";
					$sql .= "where UID = '$uid'".$extraConstraint;
					$result["update_single_".$key] = $this->db_db_query($sql);
				}
				else{
					//case for repeated event
					
					//update repeat event table
					$sql = "update CALENDAR_EVENT_ENTRY_REPEAT set ";
					$sql .= "RepeatType = '".$event["rule"]["freq"]."', ";
					if (empty($event["rule"]["until"])){
						$startUTC = $this->toUTC($event["start"]);
						if(!empty($event["rule"]["count"])){
							$untilDay = $this->getRepeatedEventEndDate($startUTC,$event["rule"]);
							$sql .= "EndDate = '$untilDay', ICalIsInfinite = '0', ";
						}else{
							$untilDay = date("Y-m-d H:i:s.000",strtotime("+1 year",strtotime($startUTC)));
							//$timeSuff = explode(" ",$event["start"]);
							$sql .= "EndDate = '$untilDay', ICalIsInfinite = '1', ";
						}
					}
					else
						$sql .= "EndDate = '".$event["rule"]["until"]."', ICalIsInfinite = '0', ";
					
					//$sql .= "EndDate = '".$event["rule"]["until"]."', ";
					$sql .= "Frequency = '".(empty($event["rule"]["interval"])?1:$event["rule"]["interval"])."'";
					if ($event["rule"]["freq"] != "YEARLY" && $event["rule"]["freq"] != "Daily"){
						$detail = $this->formulate_repeatEvent_detail($event["rule"]);
						$sql .= ", Detail = '".$detail."'";
					}
					$sql .= "where RepeatID = '".$resultSet[0]["RepeatID"]."'";
					$this->db_db_query($sql);
					//echo "update repeat event<br>";debug_pr($event);echo " until ".$untilDay."<br>";
					//remove all existing repeated events
					$sql = "delete from CALENDAR_EVENT_ENTRY where RepeatID = '".$resultSet[0]["RepeatID"]."' OR (RepeatID is null and UID = '".$resultSet[0]["UID"]."') AND CalID = '$calID'";
					$result["update_delete_repeated_".$key] = $this->db_db_query($sql);
					
					//insert repeated event
					$result["update_repeated_".$key] = $this->insert_repeated_event($event, $resultSet[0]["RepeatID"], $calID);
					if (!empty($event["exDate"])){
						for ($i =0; $i < count($event["exDate"]); $i++){
							$event["exDate"][$i] = strtotime($event["exDate"][$i])+$this->offsetTimeStamp;
						}
						$sql = "delete from CALENDAR_EVENT_ENTRY where RepeatID = '".$resultSet[0]["RepeatID"]."' AND UNIX_TIMESTAMP(EventDate) in (".implode(",",$event["exDate"]).")  AND CalID = '$calID'";
						$result["update_redelete_repeated_".$key] = $this->db_db_query($sql);
						
					}
				}
			}
			else{
				//case for inserting new event
				if (empty($event["rule"])){
					//case for normal event
					
					$sql = "insert into CALENDAR_EVENT_ENTRY($fieldname) values(";
					$sql .= "'$CalOwnerID', ";
					$sql .= "'".$event["start"]."', ";
					$sql .= "'".$event["inputDay"]."', ";
					$sql .= "'".$event["lastModified"]."', ";
					$sql .= "'$duration', "; 
					$sql .= "'$isImportant', ";
					$sql .= "'".($event["allDay"]=="true"?1:0)."', ";
					$sql .= "'$access', ";
					$sql .= "'".$this->Get_Safe_Sql_Query($title)."', ";
					$sql .= "'".$this->Get_Safe_Sql_Query($description)."', ";
					$sql .= "'".$this->Get_Safe_Sql_Query($location)."', ";
					$sql .= "'$url', ";
					$sql .= "'".$calID."', ";
					$sql .= "'$uid', ";
					$sql .= "'$extra')";
					$result["insert_repeated_".$key] = $this->db_db_query($sql);
					// echo $sql;debug_r($result["insert_repeated_".$key]);exit;
					
				}
				else{
					//case for repeated event
					
					$repeatTableField = "RepeatType, EndDate, ICalIsInfinite, Frequency";
					if ($event["rule"]["freq"] != "YEARLY" && $event["rule"]["freq"] != "Daily")
						$repeatTableField .= ", Detail";
					$sql = "insert into CALENDAR_EVENT_ENTRY_REPEAT($repeatTableField) values(";
					$sql .= "'".$event["rule"]["freq"]."', ";
					
					if (empty($event["rule"]["until"])){
						$startUTC = $this->toUTC($event["start"]);
						if(!empty($event["rule"]["count"])){
							$untilDay = $this->getRepeatedEventEndDate($startUTC,$event["rule"]);
							$sql .= "'$untilDay', '0', ";
						}else{
							$untilDay = date("Y-m-d H:i:s.000",strtotime("+1 year",strtotime($startUTC)));
							//$timeSuff = explode(" ",$event["start"]);
							$sql .= "'$untilDay', '1', ";
						}
					}
					else
						$sql .= "'".$event["rule"]["until"]."', '0', ";
					
					
					$sql .= "'".(empty($event["rule"]["interval"])?1:$event["rule"]["interval"])."'";
					if ($event["rule"]["freq"] != "YEARLY" && $event["rule"]["freq"] != "Daily"){
						$detail = $this->formulate_repeatEvent_detail($event["rule"]);
						$sql .= ", '$detail')";
					}
					else
						$sql .= ")";
					$result["insert_repeated_repeated_".$key] = $this->db_db_query($sql);
					
					$repeatID = $this->db_insert_id();
					//echo "insert repeat event<br>";debug_pr($event);echo " until ".$untilDay."<br>";
					$result["update_repeated_".$key] = $this->insert_repeated_event($event, $repeatID, $calID);
					
					if (!empty($event["exDate"])){
						for ($i =0; $i < count($event["exDate"]); $i++){
							$event["exDate"][$i] = strtotime($event["exDate"][$i])+$this->offsetTimeStamp;
						}
						$sql = "delete from CALENDAR_EVENT_ENTRY where RepeatID = '".$repeatID."' AND UNIX_TIMESTAMP(EventDate) in (".implode(",",$event["exDate"]).") AND CalID = '$calID'";
						$result["insert_delete_repeated_".$key] =$this->db_db_query($sql);
					}
				}
			}
			
			
		}
		if (in_array(false,$result))
			return false;
	}
	
	/*
		To insert all the repeated event related to the specific repeat id
		param:
			$repeatID = repeat id
			$event = an event array object <-----reference to get_Event_detail();
			$calID = calendar id
		return value:
			false if there is an error
	*/
	function insert_repeated_event($event, $repeatID, $calID){
		global $UserID;
		global $schoolNameAbbrev;
		
		$user_id = $this->getCurrentUserID();
		
		//$duration = 0;			
		//if (!$event["allDay"]){
		$duration = $this->time_diff($event["start"],$event["end"]);
		//}		
		$fieldname  = "UserID, InputDate, ModifiedDate, Duration, IsImportant, ";
		$fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID, UID, ExtraIcalInfo, RepeatID, EventDate"; 		
		$isImportant = 0;
		$access = "P";
		$title = $event["title"];
		$description = $event["description"];
		$location = $event["location"];
		$uid = $event["uid"]; 
		$extra = $event["extra"];
		$url = "";
		
		/*$$detail = $this->formulate_repeatEvent_detail($event["rule"]);
		startUTC = $this->toUTC($event["start"]);		
		$repeatEndDate = empty($event["rule"]["until"])? date("Y-m-d H:i:s.000",strtotime("+1 year",strtotime($startUTC))) : $event["rule"]["until"];
		
		$detailArray = array($repeatID, $event["rule"]["freq"], $repeatEndDate, $event["rule"]["interval"], $detail, $_SESSION['SSV_USERID'],$event["start"], $duration, $isImportant, ($event["allDay"]=="true"?1:0), $access, $this->Get_Safe_Sql_Query(Get_Request($title)), $this->Get_Safe_Sql_Query(Get_Request($description)), $this->Get_Safe_Sql_Query(Get_Request($location)), $url, $calID,"");

					
		$this->insertRepeatEventSeries($detailArray, empty($rule["byDay"])?"weekOfMonth":"dayOfMonth");
		$sql = "select min(EventID) as minEvent where RepeatID = $repeatID";
		$minEventID = $this->returnArray($sql);
		
		$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where RepeatID = '$repeatID'";
		$this->db_db_query($sql);*/
		
		//echo "start initial: ".$event["start"]."<br><br>";
		//echo "end initial: ".$event["rule"]["until"]."<br><br>";
		//echo $event["rule"]["freq"]."<br><br>";
		
		$startUTC = $this->toUTC($event["start"]);		
		$endUTC = empty($event["rule"]["until"])? date("Ymd\THis",strtotime("+1 year",strtotime($startUTC))) : $this->toUTC($event["rule"]["until"]); //repeat last for one year for infinite repeated event
		if(empty($event["rule"]["until"]) && !empty($event["rule"]["count"]))
			$endUTC = $this->toUTC($this->getRepeatedEventEndDate($startUTC,$event["rule"]));
		
		$days = $this->get_all_repeated_day($startUTC,$endUTC, $event["rule"]);
		
		if (!$days)
			return  false;
		
		$timeSuff = explode(" ",$event["start"]);
		// $sql = "insert into CALENDAR_EVENT_ENTRY($fieldname) ";
		 $sql = "insert into CALENDAR_EVENT_ENTRY($fieldname) values ";
		 $sqlSub = "";
		 $cnt = 0;
		foreach ($days as $eventDay){
						
			/*$sql = "insert into CALENDAR_EVENT_ENTRY($fieldname) values(";
			$sql .= "'$UserID', ";
			$sql .= "'".$eventDay." ".$timeSuff[1]."', ";
			$sql .= "'".$event["inputDay"]."', ";
			$sql .= "'".$event["lastModified"]."', ";
			$sql .= "'$duration', ";
			$sql .= "'$isImportant', ";
			$sql .= "'".($event["allDay"]=="true"?1:0)."', ";
			$sql .= "'$access', ";
			$sql .= "'".$this->Get_Safe_Sql_Query($title)."', ";
			$sql .= "'".$this->Get_Safe_Sql_Query($description)."', ";
			$sql .= "'".$this->Get_Safe_Sql_Query($location)."', ";
			$sql .= "'$url', ";
			$sql .= "'".$calID."', ";
			$sql .= "'$uid', ";
			$sql .= "'$extra', ";
			$sql .= "'$repeatID')";*/
			if ($cnt > 0)
				$sqlSub .= ", ";
			#new approach
			$sqlSub .= "('$user_id', '".$event["inputDay"]."', '".$event["lastModified"]."', '$duration', '$isImportant', '".($event["allDay"]=="true"?1:0)."', '$access', '".$this->Get_Safe_Sql_Query($title)."', '".$this->Get_Safe_Sql_Query($description)."', '".$this->Get_Safe_Sql_Query($location)."', '$url', '$calID', '$uid', '$extra', '$repeatID', '".$eventDay." ".$timeSuff[1]."')";
			 
			
			/*if ($cnt == 0){
				$sqlSub = "select '$UserID', '".$event["inputDay"]."', '".$event["lastModified"]."', '$duration', '$isImportant', '".($event["allDay"]=="true"?1:0)."', '$access', '".$this->Get_Safe_Sql_Query($title)."', '".$this->Get_Safe_Sql_Query($description)."', '".$this->Get_Safe_Sql_Query($location)."', '$url', '$calID', '$uid', '$extra', '$repeatID', '".$eventDay." ".$timeSuff[1]."' ";
			}else{
				$sqlSub .= "Union All select '$UserID','".$event["inputDay"]."','".$event["lastModified"]."','$duration','$isImportant','".($event["allDay"]=="true"?1:0)."','$access','".$this->Get_Safe_Sql_Query($title)."','".$this->Get_Safe_Sql_Query($description)."','".$this->Get_Safe_Sql_Query($location)."','$url','$calID','$uid','$extra','$repeatID','".$eventDay." ".$timeSuff[1]."' ";
			}*/
			$cnt++;
		
		}
		
		$sql .= $sqlSub;
		//echo $sql;
		//exit;
		return $this->db_db_query($sql);
		//return true;
	}
	
	/*
		To get all the day that within the scope according a specfic rule
		param:
			$startUTC = a start of date in UTC format
			$endUTC = an end of date in UTC format
			$rule = an event rule array object
		return value:
			1)false is there is an error
			2)an array of days
	*/
	function get_all_repeated_day($startUTC,$endUTC,$rule){
		$days = array();
		$start = strtotime($this->fromUTC($startUTC));
		$end = strtotime($this->fromUTC($endUTC));
		
		//echo date("Y-m-d", $start).' '.date("Y-m-d", $end).'<br>';
		
		
		$interval = empty($rule["interval"])?1:$rule["interval"];
		//$weekName = Array("SU"=>"Sunday","MO"=>"Monday","TU"=>"Tuesday","WE"=>"Wednesday","TH"=>"Thursday","FR"=>"Friday","SA"=>"Saturday");
		switch ($rule["freq"]){
			case "DAILY":
				//echo "start: ".date("Y-m-d H:i:s",$start)." end: ".date("Y-m-d H:i:s",$start);
				while ($start <= $end){
					$days[] = date("Y-m-d",$start);
					$start = strtotime("+".$interval." day",$start);
				}
			break;
			case "WEEKLY":
			/*	$rule["byDay"] = strtoupper(empty($rule["byDay"])?substr(date('D',$start),0,2):$rule["byDay"]);
				$weeks = explode(",",$rule["byDay"]);
				$weeks = empty($week)? $rule["byDay"]:$weeks;
				$start = strtotime("-1 day",$start);
				$continue = true;				
				if (empty ($weeks))
					return false;
				while (true){
					if (count($weeks) > 1){
						foreach($weeks as $week){
							//echo "Start: $startUTC, End: $endUTC, week: $week<br><br>";
							$temp = strtotime("next ".$weekName[$week],$start);
							
							//echo "Byweek: ".$weekName[$week]."<br>";
							//echo "temp: ".date("Y-m-d H:i:s.000",$temp)."<br><br>";
							
							if ($temp <= $end)
								$days[] = date("Y-m-d",$temp);
							else{
								$continue = false;
								break;
							}
						}
					}
					else{
						//echo "Start: $startUTC, End: $endUTC, week: $week<br><br>";
						//echo $rule["byDay"]."<br><br>";
						$temp = strtotime("next ".$weekName[$rule["byDay"]],$start);
						
						//echo "Byday: ".$rule["byDay"]."<br>"; 
						//debug_r($rule);
						//echo "temp: ".date("Y-m-d H:i:s.000",$temp)."<br><br>";
						
						if ($temp <= $end)
							$days[] = date("Y-m-d",$temp);
						else
							$continue = false;
					} 
					
					if (!$continue)
						break;
					$start = strtotime("+".$interval." week",$start);
				} */
				if(!empty($rule['byDay'])){
					$weeks = explode(",",$rule["byDay"]);
					$weeks = empty($weeks)? array($rule["byDay"]):$weeks;
					$temp_weekday = $start;
					$continue = true;
					while (true){
						if($start > $end) break;
						for($i=1;$i<=7;$i++){
							$weekday_prefix = strtoupper(substr(date('D',$temp_weekday),0,2));
							if(in_array($weekday_prefix,$weeks)){
								if ($temp_weekday <= $end)
									$days[] = date("Y-m-d",$temp_weekday);
								else{
									$continue = false;
									break;
								}
							}
							$temp_weekday = strtotime("+1 day",$temp_weekday);
						}
						if (!$continue)
							break;
						$start = strtotime("+".$interval." week",$start);
						$temp_weekday = $start;
					}
				}else{
					while($start <= $end){
						$days[] = date("Y-m-d",$start);
						$start = strtotime("+".$interval." week",$start);
					}
				}
			break;
			case "MONTHLY":				
				if (!empty($rule["byDay"])){
					/*
					 * $rule['byDay'] can be format -1TU (i.e. last Tuesday of a month) or 1TU (i.e. first Tuesday of a month)
					 */ 
					$days = array();
					$weekDays = explode(",",$rule['byDay']);
					$temp = $start;
					$next_start = $start;
					while($next_start <= $end && count($weekDays)>0){
						foreach($weekDays as $weekDay){
							$weekDay = trim($weekDay);
							$iweekday = intval($weekDay);
							if(preg_match("/.*(\w\w).*/",$weekDay,$matches)){
								$weekday = $matches[1];
							}
							$temp = $this->getNthWeekdayOfMonth($iweekday,$weekday,$next_start);
							
							if($temp >= $start && $temp <= $end){
								$days[] = date("Y-m-d", $temp);
							}
						}
						$next_start = strtotime("+".$interval." month",$next_start);
					}
				}
				elseif (!empty($rule["byMonthDay"])){
					//day of week
					while ($start <= $end){
						$days[] = date("Y-m-d",$start);
						$start = strtotime("+".$interval." month",$start);
					}
				}
				
			break;
			case "YEARLY":
				while ($start <= $end){
					$days[] = date("Y-m-d",$start);
					$start = strtotime("+".$interval." year",$start);
				}
			break;
		};
		return $days;
	}
	
	/*
		To formulate the repeated event detail
		param:
			$rule = the array of repeated rule <--- reference to get_icalendar_rule($rule)
		return value:
			formatted repeated event detail
	*/
	function formulate_repeatEvent_detail($rule){
		$week2Num = Array("SU"=>0,"MO"=>1,"TU"=>2,"WE"=>3,"TH"=>4,"FR"=>5,"SA"=>6);
		$detail = "";
		if (!empty($rule["byDay"])){
			if (preg_match('/^[0-9].*/', $rule["byDay"])||preg_match('/^-.*/', $rule["byDay"])){
				if (preg_match('/^-.*/', $rule["byDay"])){
					$detail = "5-".$week2Num[substr($rule["byDay"],2)];
				}
				else{
					$detail = $rule["byDay"]{0}."-".$week2Num[substr($rule["byDay"],1)];
				}
			}
			else{
				$str = "0000000";
				$weeks = explode(",",$rule["byDay"]);
				foreach($weeks as $week){
					$str{$week2Num[$week]} = '1';
				}
				$detail = $str;
			}
		}
		elseif (!empty($rule["byMonthDay"])){
			$detail = "day-".$rule["byMonthDay"];
		}
		return $detail;
	}
	
	/*
		To get the iCalendar statement(RFC 2445) from a given calendar
		param:
			$callD: the calendar id			
		return value:
			iCalendar statement(RFC 2445)
	*/
	function db_to_icalStmt($callD, $calType,$startDate='',$endDate=''){
		global $UserID;		
		global $schoolNameAbbrev;
		
		$uid = $this->getCurrentUserID();
		
		$allEvents = Array();
		$events = Array();
		$iCal_api = new icalendar_api($uid);
		//if ($calType == 0 || $calType == 1){
		if ($calType == 1 || $calType > 3){
			$events = $iCal_api->getFormattedExternalExportEvent($calType,$callD,$startDate,$endDate);
			//$events = $iCal_api->returnFormattedSchoolExportEvent($callD);
		}
		else{
			$sql1 = "select RepeatID from CALENDAR_EVENT_ENTRY where CalID = '$callD' and RepeatID is not NULL group by RepeatID";
			$sql = "select * from CALENDAR_EVENT_ENTRY_REPEAT where RepeatID in(".$sql1.")";

			$repeatEvents = $this->returnArray($sql);
			$allEvents = $this->formulate_repeated_event($repeatEvents, $callD, $calType);
			
			$sql  = "SELECT * FROM CALENDAR_EVENT_ENTRY ";
			$sql .= "WHERE CalID = $callD and RepeatID is NULL";
			$events = $this->returnArray($sql);
		}
	/*	}
		else if ($calType == 2){
			$db = new database(false, false, true);
			$central_dbname = $db->db;
			$sql1 = "select a.RepeatID from ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY as a
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
					a.CalID = b.CalID
					where a.CalID = '$callD' and 
					b.CalID = '2' and a.RepeatID is not NULL group by RepeatID";
			
			$sql = "select * from ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY_REPEAT as a
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
					a.CalID = b.CalID 
					where b.CalID = '2' and a.RepeatID in(".$sql1.")";

			$repeatEvents = $this->returnArray($sql);
			$allEvents = $this->formulate_repeated_event($repeatEvents, $callD, $calType);
			
			$sql  = "SELECT * FROM ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY as a 
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
					a.CalID = b.CalID ";
			$sql .= "WHERE b.CalID = '2' and a.CalID = $callD and RepeatID is NULL";
			$events = $db->returnArray($sql);
		}*/
		
		foreach($events as $event){
			$uid = $event["UID"];
			/*if ($event["UID"]=="" || $event["UID"]=="NULL" || empty($event["UID"])){
				$uid = $event["UserID"]."-".$event["CalID"]."-".$event["EventID"]."@$schoolNameAbbrev.tg";
				$sql = "UPDATE CALENDAR_EVENT_ENTRY SET ";
				$sql = $sql."UID = '".$uid."' ";
				$sql = $sql."where EventID = ".$event["EventID"];
				$this->db_db_query($sql);
			}*/
			
			$timestamp = strtotime($event["EventDate"]);
			
			$endTimestamp = strtotime("+".$event["Duration"]." minute",$timestamp);
			$end = date("Y-m-d H:i:s.000",$endTimestamp);
			
			$allEvents[] = Array("uid"=>$uid, "extra"=>$event["ExtraIcalInfo"], "title"=>$event["Title"], "description"=>$event["Description"], "location"=>trim($event["Location"]), "allDay"=>(empty($event["IsAllDay"])||($event["IsAllDay"]==0)?"false":"true"), "inputDay"=>$event["InputDate"], "lastModified"=>$event["ModifiedDate"], "start"=>$event["EventDate"], "end"=>$end);
		}
		
		return $this->make_ical($allEvents);
	}
	
	/*
		To create the repeated event array object
		param:
			$repeatEvents = The detail of repeated event
		return value:
			repeated event array object
	*/
	function formulate_repeated_event($repeatEvents, $callD, $calType){
		$allEvents = Array();
		foreach ($repeatEvents as $event){
			$repeatID  = $event["RepeatID"];
			$rule = $this->formulate_repeated_rule($event);
			//debug_r($rule); 
			$relatedEvent = Array();
			
			//if ($calType == 0 || $calType == 1){
				$sql = "select * from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID' order by EventDate";
				$repEvent = $this->returnArray($sql);
				
				$exDate = $this->get_exception_date($repEvent, $event, $rule, $callD, $calType); 
				
				
				$StartEvent = $repEvent[0]["EventDate"];
				
				
				#To check the start of event whether it is larger than one of the recurrent event
				$sql = "select * from CALENDAR_EVENT_ENTRY where RepeatID is null And UID = '".$repEvent[0]["UID"]."' AND CalID = '$calID' order by EventDate";
				$relatedEvent = $this->returnArray($sql);
		/*	}
			else if ($calType == 2){
				$db = new database(false, false, true);
				$central_dbname = $db->db;
				$sql = "select * from ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY as a
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
					a.CalID = b.CalID 
					where  b.CalID = '2' and a.RepeatID = '$repeatID' order by EventDate";
				$repEvent = $db->returnArray($sql);
				
				$exDate = $this->get_exception_date($repEvent, $event, $rule, $callD, $calType); 
				
				
				$StartEvent = $repEvent[0]["EventDate"];
				
				
				#To check the start of event whether it is larger than one of the recurrent event
				$sql = "select * from ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY as a
						INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
						a.CalID = b.CalID 
						where b.CalID = '2' and a.RepeatID is null And a.UID = '".$repEvent[0]["UID"]."' AND a.CalID = '$calID' order by EventDate";
				$relatedEvent = $db->returnArray($sql);
			}*/
			
			
			
			if (strstr($relatedEvent[0]["ExtraIcalInfo"],"RECURRENCE-ID")){
				$string = strstr($rt[0]["ExtraIcalInfo"],"RECURRENCE-ID");
				$string = strtok($string,"\x0A");
				$string = trim($string,"\x0A\x0D");
				$recurrent = "";
				if (preg_match('/^RECURRENCE-ID:/s',$string)){
					$recurrent = $this->fromUTC(substr($tok, 14),false,true);
				}else if (preg_match('/^RECURRENCE-ID;TZID=.*/s',$string)){
					$recurrent = date("Y-m-d H:i:s.000",strtotime(substr($tok,strpos($tok,":")+1)));
				}
				if (time_diff($recurrent,$StartEvent)>0){
					$StartEvent = $recurrent;
				}
			}
			
			//$timestamp = strtotime($this->toUTC($repEvent[0]["EventDate"]));
			$timestamp = strtotime($repEvent[0]["EventDate"]);
			//if ($repEvent[0]["IsAllDay"]==1)
				//$end = date("Y-m-d H:m:s.000", strtotime("+1 day",$timestamp));
			//else
			$end = date("Y-m-d H:i:s.000", strtotime("+".$repEvent[0]["Duration"]." minute",$timestamp));
			$allEvents[] = Array("uid"=>$repEvent[0]["UID"], "extra"=>$repEvent[0]["ExtraIcalInfo"], "title"=>$repEvent[0]["Title"], "description"=>$repEvent[0]["Description"], "location"=>$repEvent[0]["Location"], "allDay"=>($repEvent[0]["IsAllDay"]==1?"true":"false"), "inputDay"=>$repEvent[0]["InputDate"], "lastModified"=>$repEvent[0]["ModifiedDate"], "start"=>$StartEvent, "end"=>$end, "rule"=>$rule, "exDate" => $exDate);
		}
		return $allEvents;
	}
	
	#To get the exception date in a repeated series
	function get_exception_date($repEvent,$event,$rule,$calID, $calType){
		$exDate = Array();
		$repEventDate = $repEvent[0]["EventDate"];
		$repeatID  = $event["RepeatID"];
		
		$recurrentEvent = Array();
		
		//if ($calType == 0 || $calType == 1){
			$sql = "select * from CALENDAR_EVENT_ENTRY where RepeatID is null and UID = '".$repEvent[0]["UID"]."' And CalID = '$calID' order by EventDate";
			$recurrentEvent = $this->returnArray($sql);
	/*	}
		else if ($calType == 2){
			$db = new database(false, false, true);
			$central_dbname = $db->db;
			$sql = "select * from ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY as a
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
					a.CalID = b.CalID 
					where b.CalID = '2' and a.RepeatID is null and 
					a.UID = '".$repEvent[0]["UID"]."' And a.CalID = '$calID' order by EventDate";
			$recurrentEvent = $db->returnArray($sql);
		}*/
		
		
		$recurrentDate = Array();
		foreach ($recurrentEvent as $evt){
			if (empty($evt["RepeatID"])){
				$extraInfo = $evt["ExtraIcalInfo"]; 
				//echo $str;
				//exit;
				$tok = strtok($extraInfo,"\x0A");
				while($tok){
					$tok = rtrim($tok,"\x0A\x0D");
					$tok = ltrim($tok,"\x20\x09");
					if (preg_match('/^RECURRENCE-ID:.*/s',$tok)){
						$recurrentDate[] = strtotime($this->fromUTC(substr($tok, 14),false,true))+$this->offsetTimeStamp;
					}
					elseif (preg_match('/^RECURRENCE-ID;TZID=.*/s',$tok)){
						$recurrentDate[] = strtotime(substr($tok,strpos($tok,":")+1))+$this->offsetTimeStamp;
					}
					$tok = strtok("\x0A");
				}
			}
		}
		//echo count($recurrentDate);
		//exit;
		$startUTC = date("Ymd\THis\Z", strtotime($repEventDate));
		$endUTC = date("Ymd\THis\Z", strtotime($event["EndDate"]));
		//echo $startUTC." ".$endUTC;
		//exit;
		$repeatedDay = $this->get_all_repeated_day($startUTC,$endUTC,$rule);
		$timeSuff = explode(" ",$repEventDate);
		$dayStr = "";
		
		for($i = 0; $i < count($repeatedDay); $i ++){
			if ($i == 0)
				$dayStr .= "'".(strtotime($repeatedDay[$i]." ".$timeSuff[1])+$this->offsetTimeStamp)."' ";
			else
				$dayStr .= ", '".(strtotime($repeatedDay[$i]." ".$timeSuff[1])+$this->offsetTimeStamp)."' ";
			
		}
		
		
		//if ($calType == 0 || $calType == 1){
			$subSql = "select UNIX_TIMESTAMP(EventDate) as TimeStamp from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID'";
	/*	}
		else if ($calType == 2){
			$subSql = "select DateDiff(s,'19700101',a.EventDate) as TimeStamp 
					from ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY as a
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR as b on
					a.CalID = b.CalID 
					where b.CalID = '2' and RepeatID = '$repeatID'";
		}*/
		
		
		
		/*$sql = "Declare @TimeSlotTable table (
					[TimeStamp] [int]
				);
		
				insert into @TimeSlotTable ".$dayStr."
				select * from @TimeSlotTable as a ";*/
				
		$sql = "Create Temporary table TimeSlotTable(
					TimeStamp int
				);
				insert into TimeSlotTable ".$dayStr.";
				select * from TimeSlotTable as a
				";
		$sql .= "where a.TimeStamp not in (".$subSql.") ";
		if (!empty($recurrentDate))
		$sql .= "AND a.TimeStamp not in (".implode(",",$recurrentDate).")";
		//echo $sql."<br><br>";
		//echo $sql;
		$rt = Array();
		//if ($calType == 0 || $calType == 1)
			$rt = $this->returnArray($sql); 
		//else
			//$rt = $db->returnArray($sql);
		//echo count($rt);
		//exit;
		$returnResult = Array();
		foreach($rt as $r){
			$returnResult [] = $r["TimeStamp"];
		}
		return $returnResult;
	}

	/*
		To create the repeated event rule array
		param:
			$repeatEvents = The detail of repeated event
		return value:
			repeated event rule array object
	*/
	function formulate_repeated_rule($event){
		$repeatID = $event["RepeatID"];
		$freq = $event["RepeatType"];
		$interval = empty($event["Frequency"])?"":$event["Frequency"];
		$until = ($event["ICalIsInfinite"]=="0")?$event["EndDate"]:"";
		$byDay = ""; 
		$byMonthDay ="";
		switch ($freq){
			case "WEEKLY":
				$byDay = $this->formulate_week($event["Detail"]);					
			break;
			case "MONWEDFRI":
				$freq = "WEEKLY";
				$byDay = "MO,WE,FR";
			break;
			case"TUESTHUR":
				$freq = "WEEKLY";
				$byDay = "TU,TH";
			break;  
			case "WEEKDAY":
				$freq = "WEEKLY";
				$byDay = "MO,TU,WE,TH,FR";
			break;
			case "MONTHLY":
				$detail = $event["Detail"];
				$detail = trim($detail);
				if (preg_match('/^day.*/',$detail)){
					$tok = explode(" ", $detail);
					$byMonthDay = (preg_match('/^0.*/',substr($tok[0],4))?substr($tok[0],5):substr($tok[0],4));
				}
				else{
					$weekName = Array("SU","MO","TU","WE","TH","FR","SA");
					if (preg_match('/^-.*/',$detail)){
						$byDay .= substr($detail,0,2).$weekName[substr($detail,3)];
					}
					else{
						$pieces = explode("-",$detail);
						$byDay = ($pieces[0] > 4 ? "-1":$pieces[0]);
						$byDay .= $weekName[$pieces[1]];
					}
				}
			break;
		};
		return Array("freq"=>$freq,"interval"=>$interval,"until"=>$until,"byDay"=>$byDay,"byMonthDay"=>$byMonthDay);
	}
	
	/*
		To create the repeated event week statement
		param:
			$repeatEvents = The detail of week info in db
		return value:
			repeated event week statement
	*/
	function formulate_week($detail){
		$weekName = Array("SU","MO","TU","WE","TH","FR","SA");
		$result = "";
		if ($detail{0} == "1"){
			$result = $weekName[0];
			$detail{0} = '0';
		}
		$pos = strpos($detail, "1");
		while ($pos){
			$result .= (empty($result)?"":",");
			$result .= $weekName[$pos];
			$detail{$pos} = '0';
			$pos = strpos($detail, "1");
		}
		return $result; 
		
	}
	function Get_Request($FieldValue){
	//main function is to remove addition slashes added by magic quotes setting
		if ($FieldValue)
		{
			//if (get_magic_quotes_gpc())
			if($this->is_magic_quotes_active)
			{
				return trim(stripslashes($FieldValue));
			}
			else
			{
				return trim($FieldValue);
			}
		}
		else
		{
			return "";
		}
	} 
	
	# get events in a Foundation calendar within a period
	function getFoundationCalendarEvent($calID, $lower_bound, $upper_bound, $getMultipleDayEvent = false, $condsEx=''){
		global $UserID;
		
		$uid = $this->getCurrentUserID();
		
		$userEvent = array();
		$isESF = true;
		$result = array();
		
		# Checking for the current school is ESFC or not
		$current_dbname = $this->db;
		if($_SESSION['SchoolCode'] != 'ESFC' && $_SESSION['SchoolCode'] != 'DEV'){
			$db = new database(false, false, true);
			$isESF = false;
			$central_dbname = $db->db;
		}
		
		# Select event within a period
		$conds = "";
		if ($lower_bound != "" && $upper_bound != "" && $getMultipleDayEvent){
			$lowerBoundstr = date("Y-m-d H:i:s.000", $lower_bound);
			$upperBoundstr = date("Y-m-d H:i:s.000", $upper_bound);
			$conds = " AND DATEDIFF(s, e.EventDate, '$lowerBoundstr') >= 0 AND DATEDIFF(s, '$lowerBoundstr', DATEADD(minute, e.Duration, e.EventDate)) > 0 OR DATEDIFF(s, '19700101', e.EventDate) BETWEEN '$lower_bound' AND '$upper_bound' ";
		}
		elseif ($lower_bound != "" && $upper_bound != "") {
			$conds = " AND DATEDIFF(s, '19700101', e.EventDate) BETWEEN $lower_bound AND $upper_bound ";
		}	
		
		if ($idOnly && !$associArrOnly) {
			$fieldname  = "e.EventID";
		} else {
			$fieldname  = "e.EventID, e.UserID, e.CalID, e.RepeatID, CONVERT(VARCHAR(20), e.EventDate, 120) AS EventDate, e.Title, ";
			$fieldname .= "e.Description, e.Duration, e.IsImportant, e.IsAllDay, e.Access, e.Location, e.Url, ";
			$fieldname .= "cast(cast(pn.PersonalNote as varbinary(max)) as image) as PersonalNote, ";
			$fieldname .= "b.CalType, b.Owner ";
			//$fieldname .= "b.Status, b.Access AS Permission ";
			if($associArrOnly){
				$fieldname .= ", CASE WHEN d.Color IS NULL THEN c.Color ELSE d.Color END AS Color, 
								CASE WHEN d.Visible IS NULL THEN c.Visible ELSE d.Visible END AS Visible, 
								c.Access AS CalAccess";
			}
		}

		# Get Details of Foundation EventID
		//if (sizeof($foundationEvent) > 0) {
			$sql = "SELECT $fieldname 
					FROM ".$central_dbname.".dbo.CALENDAR_EVENT_ENTRY AS e 
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR AS b ON 
						b.CalID = e.CalID AND 
						b.CalType = 2 
						".(($_SESSION['SSV_USER_TYPE'] == 'T') ? "AND b.CalSharedType IN ('T', 'A') " : (($_SESSION['SSV_USER_TYPE'] == 'S') ? "AND b.CalSharedType IN ('S', 'A') " : ""))." 
						".(($isESF == true) ? "AND b.Owner != '$uid' "  : "")." 
					INNER JOIN ".$central_dbname.".dbo.CALENDAR_CALENDAR_VIEWER AS c ON 
						c.CalID = e.CalID AND 
						c.UserID = b.Owner 
					LEFT JOIN ".$current_dbname.".dbo.CALENDAR_CALENDAR_VIEWER AS d ON 
						d.CalID = e.CalID AND 
						d.UserID = '".$uid."' AND 
						d.CalType = 2 
					LEFT JOIN ".$current_dbname.".dbo.CALENDAR_EVENT_PERSONAL_NOTE AS pn with (nolock) ON 
						e.EventID = pn.EventID AND 
						pn.UserID = '$uid' AND 
						pn.CalType = 2 
					WHERE 
						b.CalID = '$calID'
						$conds  
					 $condsEx";
			if ($idOnly && !$associArrOnly) {
				$result = ($isESF == true) ? $this->returnVector($sql) : $db->returnVector($sql);
			} else {
				$result = ($isESF == true) ? $this->returnArray($sql) : $db->returnArray($sql);
			}
// echo $sql;
// exit;
			# Remove the duplicated records
			if(count($result) > 0){
				$tmpInfo = array();
				$cnt = 0;
				for($i=0 ; $i<count($result) ; $i++){
					$tmpInfo[$cnt] = $result[$i];
					for($j=$i+1 ; $j<count($result) ; $j++){
						if($result[$i]['EventID'] != $result[$j]['EventID']){ break; }
						if(trim($result[$j]['Permission']) == 'W'){ $tmpInfo[$cnt] = $result[$j]; }
					}
					$cnt++;
					$i = $j - 1;
				}
				$result = $tmpInfo;
			}
		//} 
		 
		return $result;
	}
	function Get_All_Available_Timeslot($userList, $existGuest, $removeGuest,$startDate, $endDate){

		$localSchoolGuest = Array();
		if (empty($userList) && empty($existGuest))
			return Array();
		
		
		if (!empty($userList)){
			foreach($userList as $u){		
			
				$localSchoolGuest[] = $u;
			}
		}
		
		if (!empty($existGuest)){
			if (!empty($removeGuestFilp))
				$removeGuestFilp = array_flip($removeGuest);
			foreach($existGuest as $g){
				if (!empty($removeGuestFilp) && array_key_exists($g,$removeGuestFilp))
					continue;				
				$localSchoolGuest[] = substr($g,1);
			}
		}
		
		$subSql2 ="";
		$startSlot = $startDate;
		$endSlot = date("Y-m-d H:i:s",strtotime("+5 minute",strtotime($startSlot)));
		$subSql2 ="('$startSlot','$endSlot')";
		
		
		$endDate = date("Y-m-d H:i:s",strtotime($endDate));
		
		
		// echo $startSlot."<br>";
		// echo $endSlot."<br>";
		// echo $endDate."<br>";
		// exit;
		while ($endSlot < $endDate){			
			
			$startSlot = date("Y-m-d H:i:s",strtotime("+5 minute",strtotime($startSlot)));
			$endSlot = date("Y-m-d H:i:s",strtotime("+5 minute",strtotime($startSlot)));
			$subSql2 .= ", ('$startSlot','$endSlot')";
					
		}
		// debug_r(implode("','",$localSchoolGuest));
		$subSql = "
		select e.EventDate as StartTime, Date_Format(TIMESTAMPADD(MINUTE,e.duration,e.EventDate),'%Y-%m-%d %T') as EndTime
		from CALENDAR_EVENT_ENTRY e 
		inner join CALENDAR_CALENDAR c on 
			e.CalID = c.CalID 
		where 
			c.CalType = 0 AND
			e.UserID in ('".implode("','",$localSchoolGuest)."') AND			
			(e.EventDate between '$startDate' and '$endDate' OR
			Date_Format(TIMESTAMPADD(MINUTE,e.duration,e.EventDate),'%Y-%m-%d %T') between '$startDate' and '$endDate' OR
			e.EventDate >= '$startDate' and TIMESTAMPADD(MINUTE,e.duration,e.EventDate) >=  '$endDate')";
		
		$sql = "
			create temporary table TimeSlotTable (
				startTime  datetime,
				endTime datetime
			)			
		";
		$result = $this->db_db_query($sql);
		// echo $sql."<br>";
		$sql = "	
				create temporary table TimeSlotTable2 (
					startTime  datetime,
					endTime datetime
				)	";
		$result = $this->db_db_query($sql);
		// echo $sql."<br>";
		$sql = "insert into TimeSlotTable (startTime,endTime) values $subSql2";		
		$result = $this->db_db_query($sql);
		// echo $sql."<br>";
				// exit;
		$sql = "insert into TimeSlotTable2 (startTime,endTime) values $subSql2";
		$result = $this->db_db_query($sql);
		// echo $sql."<br>";
		// debug_r($result);
		
		$sql = "select * From TimeSlotTable2 as fi where fi.startTime not in (
					SELECT Distinct a.startTime
					from TimeSlotTable as a, (".$subSql.") as b
					where 
						(a.startTime between b.startTime and b.endTime)
					AND (a.endTime between b.startTime and b.endTime)		
				)order by fi.startTime";
		$result = $this->returnArray($sql);
		// debug_r($result);
		// echo $sql."<br>";
		// exit;
		return $result;
	}
	
	function Check_User_Availability($userList, $existGuest, $removeGuest,$startDate, $endDate){

		$localSchoolGuest = Array();
		if (empty($userList) && empty($existGuest))
			return Array();
		
		
		if (!empty($userList)){
			foreach($userList as $u){		
			
				$localSchoolGuest[] = $u;
			}
		}
		
		if (!empty($existGuest)){
			if (!empty($removeGuestFilp))
				$removeGuestFilp = array_flip($removeGuest);
			foreach($existGuest as $g){
				if (!empty($removeGuestFilp) && array_key_exists($g,$removeGuestFilp))
					continue;				
				$localSchoolGuest[] = substr($g,1);
			}
		}
		
		$subSql2 ="";
		$startDate = strtotime($startDate)+$iCal->offsetTimeStamp;;
		$endDate = strtotime($endDate)+$iCal->offsetTimeStamp;
		
		$sql = "select Distinct e.UserID 
				from CALENDAR_EVENT_ENTRY as e inner join CALENDAR_CALENDAR as c 
					on c.CalID = e.CalID and
					c.CalType = 0
				where e.UserID in ('".implode("','",$localSchoolGuest)."') and
				(UNIX_TIMESTAMP(EventDate) between $startDate and $endDate OR
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,Duration,EventDate)) between $startDate and $endDate OR
				(UNIX_TIMESTAMP(EventDate) <= '$startDate' AND
				UNIX_TIMESTAMP(TIMESTAMPADD(Minute,Duration,EventDate)) >= $endDate)
				)
				";
		$unAvail = $this->returnVector($sql); //debug_r($unAvail);
		$avail = array_diff($localSchoolGuest,$unAvail);
		//debug_pr($sql);
		$sql = "Select a.UserID, 
				".getNameFieldWithClassNumberByLang('a.')." as UserName, 
				'0' as state
				from INTRANET_USER as a 
				where a.UserID in ('".implode("','",$unAvail)."')
				Union All
				Select b.UserID, 
				".getNameFieldWithClassNumberByLang('b.')." as UserName, 
				'1' as state
				from INTRANET_USER as b 
				where b.UserID in ('".implode("','",$avail)."')
				order by state,UserName,UserID
		"; 
		//$unAvail = $this->returnArray($sql);
		
		/*$sql = "Select a.UserID, 
				".getNameFieldWithClassNumberByLang('a.')." as UserName, 
				'1' as state
				from INTRANET_USER as a 
				where a.UserID in ('".implode("','",$avail)."')
				order by ".getNameFieldWithClassNumberByLang('a.')."
		";
		$avail = $this->returnArray($sql);*/
		
		
		//$result = array_merge_recursive($unAvail,$avail);
		$result = $this->returnArray($sql);
		return $result;
	}
	  
	
	
	
	#caltype: 0->personal, 1->school, 2->group, 3->course
	#return CalID if success
	function createSystemCalendar($calName, $calType, $DefaultEventAccess='P',$description="",$owner=0){
		$calName = $this->Get_Safe_Sql_Query(intranet_htmlspecialchars($calName));
		$description = $this->Get_Safe_Sql_Query(intranet_htmlspecialchars($description));
		$sql ="Insert into CALENDAR_CALENDAR (Name,CalType,DefaultEventAccess,Description,Owner) values
		('$calName','$calType','$DefaultEventAccess','$description','$owner')
		";
		// echo $sql;
		$result = $this->db_db_query($sql);
		if (!$result)
			return false;
		else
			return $this->db_insert_id();
	}
	
	#access: A->all permission, W->read and write, R->read only
	function insertCalendarViewer($calID,$userID=Array(),$access="R",$groupID="",$groupType="",$color="2f75e9",$groupPath=""){
		$sql="insert into CALENDAR_CALENDAR_VIEWER (CalID,UserID,Access,Color,GroupID,GroupType,GroupPath) values ";
		$groupPathValue = $groupPath != ""? "'$groupPath'" : "NULL";
		if (count($userID) > 0){
			if (is_array($userID)){
				foreach($userID as $uid){
					$sql .= "('$calID','$uid','$access','$color','$groupID','$groupType',$groupPathValue), ";
				}
				$sql = rtrim($sql,", ");
			}
			else{
				$sql .= "('$calID','$userID','$access','$color','$groupID','$groupType',$groupPathValue)";
			}
			return $this->db_db_query($sql);
		}
		// return false;
	}
	
	function removeCalendarViewer($calID, $userID=""){
		$allCalID = is_array($calID)?implode("','",$calID):$calID;
		$allUserID = is_array($userID)?implode("','",$userID):$userID;
		$sql ="delete from CALENDAR_CALENDAR_VIEWER where CalID in('$allCalID') and UserID in ('$allUserID')";
		return $this->db_db_query($sql);
	} 
	
	function removeCalendar($identity,$isCalID=true,$calType="",$removeRelated=true){
		if ($isCalID){
			$allCalID = is_array($identity)?implode("','",$identity):$identity;
		}
		else{
			$ownerID = is_array($identity)?implode("','",$identity):$identity;
			if (empty($calType))
				return false;
			$sql = "select CalID from CALENDAR_CALENDAR
					Where Owner in ('$ownerID') and CalType='$calType'";
			$allCalID = $this->returnVector($sql);
			$allCalID = is_array($allCalID)?implode("','",$allCalID):$allCalID;
		}
		$sql ="delete from CALENDAR_CALENDAR where CalID in ('".$allCalID."')";
		$result['calendar'] = $this->db_db_query($sql);
		if ($removeRelated){
			$sql = "delete from CALENDAR_CALENDAR_VIEWER where CalID in ('".$allCalID."')";
			$result['calendar_viewer'] = $this->db_db_query($sql);
			
			$sql = "select EventID from CALENDAR_EVENT_ENTRY where CalID in ('".$allCalID."')";
			$allEvent = $this->returnVector($sql);
			
			if (count($allEvent) > 0){
				if (empty($calType)||$calType=""){
					$extraConstraint = " AND (CalType <> 1 AND CalType <= 3 OR CalType is null)";
				}
				else{
					$extraConstraint = " AND CalType = '$calType'";
				}
				$sql = "delete from CALENDAR_EVENT_PERSONAL_NOTE where EventID in ('".implode("','",$allEvent)."') $extraConstraint";
				$result['calendar_personal_note'] = $this->db_db_query($sql);
				$sql = "delete from CALENDAR_REMINDER where EventID in ('".implode("','",$allEvent)."') $extraConstraint";
				$result['calendar_reminder'] = $this->db_db_query($sql);
			}
			
			$sql = "select RepeatID from CALENDAR_EVENT_ENTRY where CalID in ('".$allCalID."') and RepeatID is not null";
			$sllRepeatEvent = $this->returnVector($sql);
			
			if (count($sllRepeatEvent) > 0){
				$sql = "delete from CALENDAR_EVENT_ENTRY_REPEAT where RepeatID in ('".implode("','",$sllRepeatEvent)."')";
				$result['calendar_repeat_event'] = $this->db_db_query($sql);
			}		
			$sql = "delete from CALENDAR_EVENT_ENTRY where CalID in ('".$allCalID."')";		
			$result['calendar_event'] = $this->db_db_query($sql);
		}
		
		return !in_array(false,$result);
	}
	
	function getCalendarViewer($calID, $accessRight="", $groupType=""){
		if (!empty($accessRight) && $accessRight!="")
			$accessRight = " And Access = '$accessRight' ";
			if (!empty($groupType) && $groupType!="")
			$groupType = " And GroupType = '$groupType' ";
		$sql = "select UserID from CALENDAR_CALENDAR_VIEWER
				where CalID = '$calID' $accessRight $groupType";
		return $this->returnVector($sql);
	}
	
	function getCalendarRelatedEvent($calID="",$calIDSelectsql="",$lowBound="",$upBound=""){
		
		$uid = $this->getCurrentUserID();
		
		if ($lowBound!="" && $upBound != ""){
			$cord = "
			And ((UNIX_TIMESTAMP(EventDate) between $lowBound and $upBound 
				OR UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,Duration,EventDate)) between $lowBound and $upBound)
				OR (UNIX_TIMESTAMP(EventDate) <= $lowBound and 
				UNIX_TIMESTAMP(TIMESTAMPADD(MINUTE,Duration,EventDate)) >= $upBound)
			)";
		}
		if (is_array($calID)){
			$calIDSql = implode("','",$calID);
		}
		else{
			$calIDSql = $calID;
		}
		if (!empty($calIDSelectsql)){
			$subsql = " OR e.CalID in ($calIDSelectsql) ";
		}
		$sql = "Select e.EventID,e.EventDate,e.Duration,v.Color from CALENDAR_EVENT_ENTRY as e
				inner join CALENDAR_CALENDAR_VIEWER as v on 
				e.CalID = v.CalID and
				v.UserID = '".$uid."'
				where 
				(e.CalID in ('$calIDSql') $subsql) $cord
				";
	
		return $this->returnArray($sql);
	}
	
	function Print_Search_result($searchValue,$print=false) {
	   	global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_EventDate, $iCalendar_NewEvent_EventTime, $iCalendar_EditEvent_CreatedBy;
	  	global $iCalendar_SchoolEvent_Color, $iCalendar_NewEvent_IsAllDay, $iCalendar_Agenda_NoEvent;
	  	global $iCalendar_NewEvent_Location, $iCalendar_NewEvent_Description, $iCalendar_NewEvent_Personal_Note;
	  	global  $i_Calendar_Edit, $iCalendar_eventSearch_noRecordFound, $iCalendar_eventSearch_searchingFor;
	  	global $events, $schoolEvents, $ImagePathAbs, $UserID, $lui;
	  	
	  	$uid = $this->getCurrentUserID();
	  		  	
	  	# selection box
	  	/*$KeyValues = array(
				0 => array($i_Calendar_Today, "0"), 
				1 => array($i_Calendar_NextSevenDays, "1"), 
				2 => array($i_Calendar_ThisWeek, "2"));
		$selectAgendaPeriod  = '';
		if(!$print){
			$selectAgendaPeriod .= '<select name="selectAgendaPeriod" id="selectAgendaPeriod" onchange="changeAgendaPeriod(this)">';
			for($i=0 ; $i<count($KeyValues) ; $i++){
				$selected = ($KeyValues[$i][1] == $agendaPeriod) ? "selected" : ""; 
				$selectAgendaPeriod .= '<option value="'.$KeyValues[$i][1].'" '.$selected.'>'.$KeyValues[$i][0].'</option>';
			}
			$selectAgendaPeriod .= "</select>";
		} */
		
		# UI
		if ($print){
			$x .= "<div id='searchContent' class='cal_agenda_content' >";
		}
		else{
			$x .= "<div id='searchContent' class='cal_agenda_content' style='overflow:auto;height: expression( this.scrollHeight > 800 ? \"800px\" : \"auto\" );max-height: 800px; '>";
		}
		$x .= "<div style='padding:3px;'>".str_replace('%%value%%', stripslashes($searchValue),$iCalendar_eventSearch_searchingFor)."</div>";
		 
		
		 
		$viewableCal = $this->returnViewableCalendar();
		for ($i=0; $i<sizeof($viewableCal); $i++) {
			$viewableCalColor[$viewableCal[$i]["CalID"]] = $viewableCal[$i]["Color"];
			$viewableCalVisible[$viewableCal[$i]["CalID"]] = (int)$viewableCal[$i]["Visible"];
		}
		
		$response = array("A","M","W","");
		
		$dayTableExist = array();
		
		$calViewPeriod = isset($_SESSION["iCalDisplayPeriod"])?$_SESSION["iCalDisplayPeriod"] : "monthly";
		
		if(sizeOf($events) > 0) {
			$cnt = 0;
			foreach($events as $evt_date => $evt){
				if(count($evt) > 0){
					$startDateTs = strtotime($startDate);
					$endDateTs = strtotime($endDate);
					$eventTs = $this->sqlDatetimeToTimestamp($evt_date);
					$eventEndTs = $eventTs+$evt['Duration']*60;
					 
					//if ($startDateTs>$eventEndTs || $endDateTs < $eventTs)
						//continue;
					
					$dayTableClass = ($evt_date == date("Y-m-d")) ? "cal_agenda_entry_date_log cal_agenda_content_entry_today" :  "cal_agenda_entry_date_log";

					$x .= '<div id="agenda-" style="display:block">';
					$x .= '<table cellspacing="0" cellpadding="0" border="0" class="'.$dayTableClass.'">';
					$x .= '<tr>';
					$x .= '<td class="cal_agenda_entry_date">';
					if(!$print){
						$x .= '<a class="cal_agenda_entry_event" href="index.php?time='.$eventTs.'&calViewPeriod=daily">'.date("j M, Y", $eventTs).'</a>';
					} else {
						//$x .= date("j M", $eventStartTs);
						$x .= date("j M, Y", $eventTs);
					}
					$x .= '</td>';
					$x .= '<td>';

					# Event Content
					for($i=0 ; $i<count($evt) ; $i++){

						if (in_array($evt["Status"], $response)) {
							### convert MS SQL Datetime format ###
							#$events[$i]["EventDate"] = date("Y-m-d H:i:s", strtotime($events[$i]["EventDate"]));
							
							$eventDateTimePiece = explode(" ", $evt[$i]["EventDate"]);
							$eventDate = $eventDateTimePiece[0];
				
							$border_css = '';
							if($cnt != count($events)-1 && $i == sizeOf($evt)-1 ){
								$border_css = "style=\"border-bottom:0px\"";
							}
							
							$CalType = trim($evt[$i]['CalType']);
							$eventStartTs = $this->sqlDatetimeToTimestamp($evt[$i]["EventDate"]);
							$eventEndTs = $eventStartTs + $evt[$i]["Duration"]*60;
				
							if ($eventStartTs==$eventEndTs) {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs);
								}
							} else {
								if ($this->systemSettings["TimeFormat"] == "12") {
									$eventTime = date("g:ia",$eventStartTs)."-".date("g:ia",$eventEndTs);
								} if ($this->systemSettings["TimeFormat"] == "24") {
									$eventTime = date("G:i",$eventStartTs)."-".date("G:i",$eventEndTs);
								}
							}
							$isAllDay = (int)$evt[$i]["IsAllDay"];

							$createdBy = $this->returnUserFullName($evt[$i]["UserID"]);
						

							# Preparation
							$details_display = ($print) ? "block" : "none";
							$isInvolve = (trim($evt[$i]["Permission"]) == "R") ? true : false;
							
							if (!empty($evt[$i]["CalType"])&&
							($evt[$i]["CalType"] == 1 ||$evt[$i]["CalType"] > 3 )){
							# external calendar
								//$display     = ($evt[$i]["Visible"] == "0") ? "none": "block";
								$display = 'block';
								$numVisible += ($evt[$i]["Visible"] == "0") ? 0 : 1;

								# Get Corresponding Color of each Event
								$ColorID = $this->Get_Calendar_ColorID($evt[$i]["Color"]);
								$Color	  = trim($evt[$i]["Color"]);
								$evt_entry_id	  = $evt[$i]["DivPrefix"].$evt[$i]["EventID"];
								//$div_class = $evt[$i]["DivClass"];
								$TipName  = "eventToopTip";
								$CalType = $evt[$i]["CalType"]; 
							}elseif($isInvolve)				# Involved Event
							{			
								$involveCalVisible = $this->returnUserPref($uid, "involveCalVisible");
								//$display	 = ($involveCalVisible == "0") ? "none" : "block";
								$display = 'block';
								//$div_class = "involve";
								$evt_entry_id = "entry_involveEvent".$evt[$i]["EventID"];

								$ColorID  = $this->involveCalColorID;
								$Color    = $this->involveCalColor;
								$numVisible += ($involveCalVisible == "0") ? 0 : 1;
							} 
							else 							# personal related events
							{				
								//$display   = ($viewableCalVisible[$evt[$i]["CalID"]]==1) ? "block" : "none";
								$display = 'block';
								//$div_class = "cal-".$evt[$i]["CalID"];
								$evt_entry_id = "entry_event".$evt[$i]["EventID"];
								$ColorID  = $this->Get_Calendar_ColorID($viewableCalColor[$evt[$i]["CalID"]]);
							}

							# Content
							$x .= '<div class="'.$div_class.'" style="display:'.$display.'">';
							$x .= '<table cellspacing="0" cellpadding="0" border="0">';
							$x .= '<tr>';
							$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_time' $border_css>";
							$x .= ($isAllDay ? $iCalendar_NewEvent_IsAllDay : $eventTime);
							$x .= "</td>";
							$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_event' $border_css>";
							if($print){
								$x .= "<img class='cal_event_png_$ColorID' width='15' height='15' border='0' align='absmiddle' src='".$ImagePathAbs."calendar/event_$ColorID.png'/>&nbsp;";
								$x .= "<strong>".intranet_undo_htmlspecialchars($evt[$i]["Title"]. $this->AppendOnwerName($evt[$i]["UserID"]))."</strong>";
								if (trim($evt[$i]["PersonalNote"]) != "") 
									$x .= '<img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0">';
									//$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
								$x .= "<br />";
							} else {
								$x .= "<a class='cal_agenda_entry_event' href='javascript:jHide_Div(document.getElementById(\"".$evt_entry_id."\"))'>";
								$x .= "<img class='cal_event_png_$ColorID' width='15' height='15' border='0' align='absmiddle' src='".$ImagePathAbs."calendar/event_$ColorID.png'/>&nbsp;";
								$x .= intranet_undo_htmlspecialchars($evt[$i]["Title"]).  $this->AppendOnwerName($evt[$i]["UserID"]);
								if (trim($evt[$i]["PersonalNote"]) != "") 
									$x .= '<img src="'.$ImagePathAbs.'calendar/icon_personalnote.gif" width="10" height="10" border="0">';
									//$x .= " ".$lui->Get_Image($ImagePathAbs.'calendar/icon_personalnote.gif', 10, 10, 'border=0');
								$x .= "<br />";
								$x .= "</a>";
							}
							$x .= "<div id='".$evt_entry_id."' class='cal_agenda_content_entry_detail' style='display:$details_display'>";
							$x .= $iCalendar_NewEvent_Location." : ".((trim($evt[$i]["Location"]) == "") ? '-' : trim(intranet_undo_htmlspecialchars($evt[$i]["Location"])))."<br>";
							$x .= $iCalendar_NewEvent_Description." : ".((trim($evt[$i]["Description"]) == "") ? '-' : trim(intranet_undo_htmlspecialchars($evt[$i]["Description"])))."<br>";
							$x .= $iCalendar_NewEvent_Personal_Note." : ".((trim($evt[$i]["PersonalNote"]) == "") ? '-' : trim(intranet_undo_htmlspecialchars($evt[$i]["PersonalNote"])))."<br>";
							$x .= $iCalendar_EditEvent_CreatedBy." : ".$this->returnUserFullName($evt[$i]["UserID"]);
							$x .= "</div>";
							$x .= "</td>";
							$x .= "<td class='cal_agenda_content_entry cal_agenda_content_entry_button' $border_css>";
							//$x .= "<!--<a class='sub_layer_link' href='#'>$i_Calendar_Delete</a> | -->";
							$x .= (!$print) ? '<a class="sub_layer_link" href="new_event.php?editEventID=event'.$evt[$i]["EventID"].'&calViewPeriod='.$calViewPeriod.'&CalType='.$CalType.'">'.$i_Calendar_Edit.'</a>' : '&nbsp;';
							$x .= "</td>";
							$x .= "</tr>";
							$x .= "</table>";
							$x .= '</div>';
						}
					}
					$x .= '</td>';
					$x .= '<tr>';
					$x .= '</table>';
					$x .= '</div>';
					$cnt++;
				}
			}
			if (!in_array($eventDate, $dayTableExist))
				$dayTableExist[] = $eventDate;
		} else {
			$x .= "<table cellspacing='0' cellpadding='0' border='0' width='95%'>
					<tr>
					  <td class='cal_agenda_entry_date'>$iCalendar_eventSearch_noRecordFound </td>
					</tr>
				   </table>";

		}
		$x .= "</div>";
		
		return $x;	
   	}
	
	function Update_User_Preference($PostedValue) {
		global $Lang;
		
		$uid = $this->getCurrentUserID();
		
		$CalViewMode = ($PostedValue['CalViewMode'] == 'full') ? 1 : 0;
		
		$checkExistsSql1 = "SELECT CASE WHEN Count(*) > 0 THEN 1 else 0 END returnCount FROM CALENDAR_USER_PREF WHERE UserID = '".$uid."' AND Setting = 'CalViewMode';";
		$checkExistsSql2 = "SELECT CASE WHEN Count(*) > 0 THEN 1 else 0 END returnCount FROM CALENDAR_USER_PREF WHERE UserID = '".$uid."' AND Setting = 'DefaultCalendar';";
		$result1 = $this->returnArray($checkExistsSql1);
		$result2 = $this->returnArray($checkExistsSql2);
		// echo $checkExistsSql1.'<br>';
		// echo $checkExistsSql2.'<br>';
		if ($result1[0]['returnCount'] ==1) {
			$UpdateSql1 = "UPDATE CALENDAR_USER_PREF SET Value = '".$CalViewMode."' WHERE UserID = ".$uid." AND Setting = 'CalViewMode';";
		} else {
			$UpdateSql1 = "INSERT CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('".$uid."','CalViewMode','".$CalViewMode."');";
		}
		if ($result2[0]['returnCount'] == 1) {
			$UpdateSql2 = "UPDATE CALENDAR_USER_PREF SET Value = '".$PostedValue['CalID']."' WHERE UserID = '".$uid."' AND Setting = 'DefaultCalendar';";
		} else {
			$UpdateSql2 = "INSERT CALENDAR_USER_PREF (UserID, Setting, Value) VALUES ('".$uid."','DefaultCalendar','".$PostedValue['CalID']."');";
		}
		
		// echo $UpdateSql1.'<br>';
		// echo $UpdateSql2.'<br>';
		
		if ($this->db_db_query($UpdateSql1) && $this->db_db_query($UpdateSql2)) {
			return $Lang['ReturnMsg']['UserPreferenceUpdateSuccess'];
		} else {
			return $Lang['ReturnMsg']['UserPreferenceUpdateUnSuccess'];
		}
	}	
	
	//*****************  eclass  *******************************//
	
	function generateCalendarEventList($event){
		$html = "";
		$i = 0;
		foreach($event as $evt){
		$html .='
			<li id="'.($i==0?'current':'').'">
				<a href="javascript:void(0);">'.$evt["Title"].'</a>
			</li>	
			';
			$i++;
		}
		return Array("SelectedEvent"=>$event[0]['EventID'], "html"=>$html);
	}
	
	function getRepeatedEventEndDate($startUTC,$rule)
	{
		$start = strtotime($this->fromUTC($startUTC));
		//$freq = $rule['freq'];
		$interval = empty($rule['interval'])?1:$rule['interval'];
		//$until = $rule['until'];
		//$byDay = $rule['byDay'];
		//$byMonthDay = $rule['byMonthDay'];
		//$count = $rule['count'];
		$end = strtotime($rule['until']);
		
		// if no until date specified and repeat count specified, + 1 year as default for repeat indefinitely
		if(empty($rule['until']) && empty($rule['count']))
			return date("Y-m-d H:i:s.000",strtotime("+1 year",$start));
		// if until date is specified, format it and return directly
		if(!empty($rule['until']))
			return date("Y-m-d H:i:s.000",$end);
		
		//$weekName = array("SU"=>"Sunday","MO"=>"Monday","TU"=>"Tuesday","WE"=>"Wednesday","TH"=>"Thursday","FR"=>"Friday","SA"=>"Saturday");
		//$countName = array(1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' , 6 => 'sixth', 7 =>'seventh' , 8 => 'eighth', 9 => 'ninth', 10 => 'tenth', 11 => 'eleventh', 12 => 'twelfth');
		// otherwise cater repeat rule
		switch($rule['freq'])
		{
			case "DAILY":
				$n = 1;
				while($n < $rule['count']){
					$start = strtotime("+".$interval." day",$start);
					$n+=1;
				}
				$end = $start;
			break;
			case "WEEKLY":
				if(!empty($rule['byDay'])){
					$weeks = explode(",",$rule["byDay"]);
					$weeks = empty($weeks)? array($rule["byDay"]):$weeks;
					$n = 1;
					$temp_end = $start;
					while ($n <= $rule['count'] && count($weeks)>0){
						$temp_weekday = $start;
						for($i=1;$i<=7;$i++){
							if($n > $rule['count']) break;
							$weekday_prefix = strtoupper(substr(date('D',$temp_weekday),0,2));
							if(in_array($weekday_prefix,$weeks)){
								$temp_end = $temp_weekday;
								$n+=1;
							}
							$temp_weekday = strtotime("+1 day",$temp_weekday);
						}
						$start = strtotime("+".$interval." week",$start);
					}
					$end = $temp_end;
				}else{
					$n = 1;
					while($n < $rule['count']){
						$start = strtotime("+".$interval." week",$start);
						$n+=1;
					}
					$end = $start;
				}
			break;
			case "MONTHLY":
				if(!empty($rule['byDay'])){
					$weekDays = explode(",",$rule['byDay']);
					$n = 1;
					$temp_end = $start;
					$next_start = $start;
					while($n < $rule['count'] && count($weekDays)>0){
						foreach($weekDays as $weekDay){
							if($n >= $rule['count']){continue;}
							$weekDay = trim($weekDay);
							$iweekday = intval($weekDay);
							if(preg_match("/.*(\w\w).*/",$weekDay,$matches)){
								$weekday = $matches[1];
							}
							$temp = $this->getNthWeekdayOfMonth($iweekday,$weekday,$next_start);
							if($temp > $start && $temp > $temp_end){
								$temp_end = $temp;
								$n++;
							}
						}
						$next_start = strtotime("+".$interval." month",$next_start);
					}
					$end = $temp_end;
				}
				else{
					$n = 1;
					while($n < $rule['count']){
						$start = strtotime("+".$interval." month",$start);
						$n+=1;
					}
					$end = $start;
				}
			break;
			case "YEARLY":
				$n = 1;
				while($n < $rule['count']){
					$start = strtotime("+".$interval." year",$start);
					$n+=1;
				}
				$end = $start;
			break;
		}
		
		return date("Y-m-d H:i:s.000",$end);
	}
	
	/*
	 * $nth - if positive int: count in ascending order, e.g. 3 means third $weekday of a month
 	 * 		- if negative int: count in reverse order, e.g. -2 means last second $weekday of a month
	 * $weekday - weekday code, SU=Sunday,MO=Monday,TU=Tuesday,WE=Wednesday,TH=Thursday,FR=Friday,SA=Saturday
	 * $ts - timestamp of given event datetime
	 */
	function getNthWeekdayOfMonth($nth,$weekday,$ts)
	{
		$month_ts = array();
		$month_weekday = array();
		$numOfDaysAMonth = date("t",$ts);// total number of days in current month
		$startDayOfAMonth = date("j",$ts); // day number of given timestamp
		$firstDayOfAMonth = 1;
		$cur_ts = $ts - ($startDayOfAMonth-1)*86400;
		for($d=$firstDayOfAMonth;$d<=$numOfDaysAMonth;$d++){
			$month_ts[] = $cur_ts;
			$month_weekday[] = strtoupper(substr(date("D",$cur_ts),0,2));
			$cur_ts += 86400; // +1 day
		}
		
		$weekday = strtoupper($weekday);
		$cnt = 0;
		if($nth < 0){ // find reverse nth weekday
			for($i=count($month_weekday)-1;$i>0;$i--){
				if($month_weekday[$i] == $weekday) $cnt--;
				if($cnt == $nth) return $month_ts[$i];
			}
		}else{
			for($i=0;$i<count($month_weekday);$i++){
				if($month_weekday[$i] == $weekday) $cnt++;
				if($cnt == $nth) return $month_ts[$i];
			}
		}
		return 0;
	}
	
	function addCalendarViewerToGroup($ParGroupID,$ParUserID){
		$userIDArr = (array)$ParUserID;
		$userIDStr = implode(",",$userIDArr);
		$groupPath = "O".$ParGroupID;
		# find  all calendars that are shared to this group 
		$sql = "SELECT DISTINCT v.CalID FROM CALENDAR_CALENDAR_VIEWER as v INNER JOIN CALENDAR_CALENDAR as c ON c.CalID = v.CalID WHERE v.GroupPath = '$groupPath' ";
		$calIDArr = $this->returnVector($sql);
		$calIDStr = implode(",",$calIDArr);
		# find existing group users and viewer attributes
		$sql = "SELECT CalID,GROUP_CONCAT(UserID) as UserIDArray,GroupType,Access,Color  
				FROM CALENDAR_CALENDAR_VIEWER 
				WHERE CalID IN ($calIDStr) AND GroupPath = '$groupPath' 
				GROUP BY CalID,GroupPath ";
		$rs = $this->returnArray($sql);	
		
		$result = array();
		for($i=0;$i<count($rs);$i++){
			list($calID, $userIDArray,$groupType,$access,$color) = $rs[$i];
			$existUserIDArray = explode(",",$userIDArray);
			if(trim($access)=='') $access = "R";
			if(trim($color)=='') $color = "2f75e9";
			# insert users that are new group member and are not viewer yet 
			$insertUserIDArr = array_values(array_unique(array_diff($userIDArr,$existUserIDArray)));
			/*
			if(count($insertUserIDArr)>0){
				$sql = "SELECT UserID,RecordType,REVERSE(CONV(AdminAccessRight,10,2)) as rightbits FROM INTRANET_USERGROUP WHERE GroupID='$ParGroupID' AND UserID IN (".implode(",",$insertUserIDArr).")";
				$usergroups = $this->returnResultSet($sql);
				$usergroups_count = count($usergroups);
				for($j=0;$j<$usergroups_count;$j++){
					// if is group admin or has internal event right 4 (reversed bit string [XXXX1XXXX]), add calendar admin right
					$access = $usergroups[$j]['RecordType'] == 'A' || ($usergroups[$j]['rightbits']!='' && substr($usergroups[$j]['rightbits'],4,1) == '1') ? "W" : "R";
					$result['insertCalendarViewer'.$calID] = $this->insertCalendarViewer($calID,array($usergroups[$j]['UserID']),$access,"",$groupType="",$color,$groupPath);
				}
			}
			*/
			$result['insertCalendarViewer'.$calID] = $this->insertCalendarViewer($calID,$insertUserIDArr,$access,"",$groupType="",$color,$groupPath);
		}
		
		return !in_array(false,$result);
	}
	
	function removeCalendarViewerFromGroup($ParGroupID,$ParUserID){
		$groupPath = "O".$ParGroupID;
		# find  all calendars that are shared to this group 
		$sql = "SELECT DISTINCT v.CalID FROM CALENDAR_CALENDAR_VIEWER as v INNER JOIN CALENDAR_CALENDAR as c ON c.CalID = v.CalID WHERE v.GroupPath = '$groupPath' ";
		$calIDArr = $this->returnVector($sql);
		
		$result = array();
		for($i=0;$i<count($calIDArr);$i++){
			$result['removeCalendarViewer'.$calIDArr[$i]] = $this->removeCalendarViewer($calIDArr[$i], $ParUserID);
		}
		return !in_array(false,$result);
	}
	
	// get dates to skip array
	function getSkipDates($StartDate,$EndDate)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libcycleperiods.php");
		
		$libcycleperiods = new libcycleperiods();
		
		$weekdayDateAry = array();
		$start_date_ts = strtotime($StartDate);
		$end_date_ts = strtotime($EndDate);
		// cycle periods
		$periodAry = $libcycleperiods->returnPeriods_NEW($PeriodID='', $PublishedOnly=1, $AcademicYearID='', $StartDate, $EndDate);
		for($i=0;$i<count($periodAry);$i++) {
			$period_start = $periodAry[$i]['PeriodStart'];
			$period_end = $periodAry[$i]['PeriodEnd'];
			$saturday_counted = $periodAry[$i]['SaturdayCounted'] == '1';
			
			$period_start_ts = strtotime($period_start);
			$period_end_ts = strtotime($period_end);
			$start_ts = $period_start_ts > $start_date_ts ? $period_start_ts : $start_date_ts;
			$end_ts = $period_end_ts < $end_date_ts ? $period_end_ts : $end_date_ts;
			// find non-school day Sunday and Saturday
			for($ts=$start_ts;$ts<=$end_ts;$ts+=86400) {
				$weekday = date("w",$ts);
				if($weekday == 0 || ($weekday==6 && $saturday_counted)){
					$weekdayDateAry[] = date("Y-m-d",$ts);
				}
			}
		}
		// school holidays and public holidays
		$sql = "SELECT DISTINCT DATE_FORMAT(EventDate,'%Y-%m-%d') as EventDate FROM INTRANET_EVENT WHERE (EventDate BETWEEN '$StartDate 00:00:00' AND '$EndDate 23:59:59') AND RecordType IN (3,4)";
		$holidayDateAry = $this->returnVector($sql);
		
		$skipDateAry = array_unique( array_merge($holidayDateAry,$weekdayDateAry) );
		
		return $skipDateAry;
	}
	
	function getOwnCalendarSelection($tags, $selectedCalID="")
	{
		global $intranet_root, $intranet_session_language;
		include_once($intranet_root."/lang/ical_lang.$intranet_session_language.php");
		
		$uid = $this->getCurrentUserID();
		
		$ownCalendar = $this->returnViewableCalendar('', '', true);
		for ($i=0; $i<sizeof($ownCalendar); $i++) {
			if ($ownCalendar[$i]["CalType"] == 4)
				continue;
			$calName = trim(htmlspecialchars(intranet_undo_htmlspecialchars($ownCalendar[$i]["Name"])));
			$calName =  $ownCalendar[$i]["CalType"] == 3? intranet_undo_htmlspecialchars($calName):$calName;
			//$calName = strlen($calName)>30?substr($calName,0,30)." ...":$calName;
			$calName = chopString($calName,30);
			
			$calendarTypeArr[] = array($calName, $ownCalendar[$i]["CalID"]);
			
			if($ownCalendar[$i]["CalType"] == 0){
				if($ownCalendar[$i]["Owner"] != $uid){
					$arrOptionalCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
				}else{
					$arrPersonalCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
				}
			}else if($ownCalendar[$i]["CalType"] == 1){
				$arrSchoolCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
			}else if($ownCalendar[$i]["CalType"] == 2){
				$arrGroupCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
			}else if($ownCalendar[$i]["CalType"] == 3){
				$arrCourseCalendar[] = array($ownCalendar[$i]["CalID"], $calName);
			}
		}
		
		$select_calendar_type .= '<select '.$tags.'>';
		if(sizeof($arrPersonalCalendar)>0){
			foreach($arrPersonalCalendar as $key=>$arrPersonal){
				if($key == 0){
					$select_calendar_type .= '<optgroup label="'.$iCalendar_Calendar_MyCalendar.'">';
				}
				$personal_cal_id = $arrPersonal[0];
				$personal_cal_name = $arrPersonal[1];
				$selected = ($personal_cal_id == $selectedCalID) ? " SELECTED " : "";
				$select_calendar_type .= '<option id="'.$personal_cal_id.'" class="calPersonal" value="'.$personal_cal_id.'" '.$selected.'>'.$personal_cal_name.'</option>';
			}
		}
		if(sizeof($arrSchoolCalendar)>0){
			foreach($arrSchoolCalendar as $key=>$arrSchool){
				if($key == 0){
					$select_calendar_type .= '<optgroup label="'.$iCalendar_Calendar_OtherCalendar_School.'">';
				}
				$school_cal_id = $arrSchool[0];
				$school_cal_name = $arrSchool[1];
				$selected = ($school_cal_id == $selectedCalID) ? " SELECTED " : "";
				$select_calendar_type .= '<option id="'.$school_cal_id.'" class="calSchool" value="'.$school_cal_id.'" '.$selected.'>'.$school_cal_name.'</option>';
			}
		}
		if(sizeof($arrGroupCalendar)>0){
			foreach($arrGroupCalendar as $key=>$arrGroup){
				if($key == 0){
					$select_calendar_type .= '<optgroup label="'.$iCalendar_group.'">';
				}
				$group_cal_id = $arrGroup[0];
				$group_cal_name = $arrGroup[1];
				$selected = ($group_cal_id == $selectedCalID) ? " SELECTED " : "";
				/*
				if(($isEditEvent) && ($group_cal_id == $selectedCalID)){
					$display_checkbox = "";
					
					$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = ".$eventID;
					$arrSchoolCalEventID = $iCal->returnVector($sql);
					if(sizeof($arrSchoolCalEventID) > 0){
						$setCheckBoxChecked = " checked ";
					} 
				}else{
					$display_checkbox = "style='display: none;'";
				}
				*/
				$select_calendar_type .= '<option id="'.$group_cal_id.'" class="calGroup" value="'.$group_cal_id.'" '.$selected.'>'.$group_cal_name.'</option>';
			}
		}
		if(sizeof($arrCourseCalendar)>0){
			foreach($arrCourseCalendar as $key=>$arrCourse){
				if($key == 0){
					$select_calendar_type .= '<optgroup label="'.$iCalendar_course.'">';
				}
				$course_cal_id = $arrCourse[0];
				$course_cal_name = $arrCourse[1];
				$selected = ($course_cal_id == $selectedCalID) ? " SELECTED " : "";
				$select_calendar_type .= '<option id="'.$course_cal_id.'" class="calCourse" value="'.$course_cal_id.'" '.$selected.'>'.($course_cal_name).'</option>';
			}
		}
		if(sizeof($arrOptionalCalendar)>0){
			foreach($arrOptionalCalendar as $key=>$arrOptional){
				if($key == 0){
					$select_calendar_type .= '<optgroup label="'.$iCalendar_Calendar_OtherCalendar.'">';
				}
				$optional_cal_id = $arrOptional[0];
				$optional_cal_name = $arrOptional[1];
				$selected = ($optional_cal_id == $selectedCalID) ? " SELECTED " : "";
				$select_calendar_type .= '<option id="'.$optional_cal_id.'" class="calOptional" value="'.$optional_cal_id.'" '.$selected.'>'.$optional_cal_name.'</option>';
			}
		}
		$select_calendar_type .= '</select>';
		
		return $select_calendar_type;
	}
	
	function getEventTitleExtraInfo($eventId, $forceGenerateNew=0, $returnFullNameList=0)
	{
		global $Lang, $sys_custom;
		
		if(!$sys_custom['iCalendar']['EventTitleWithParticipantNames']) return '';
		
		if(!$forceGenerateNew && !$returnFullNameList){
			$sql = "SELECT TitleExtraInfo FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventId'";
			$records = $this->returnVector($sql);
			$title = $records[0];
		}
		if($title == '' || $forceGenerateNew || $returnFullNameList)
		{
			$name_field = getNameFieldByLang2("u.");
			$sql = "SELECT 
						u.EnglishName as UserName 
					FROM CALENDAR_EVENT_ENTRY as e
					INNER JOIN CALENDAR_EVENT_USER as eu ON eu.EventID=e.EventID 
					INNER JOIN INTRANET_USER as u ON u.UserID=eu.UserID 
					WHERE e.EventID='$eventId' ORDER BY UserName";
			$users = $this->returnVector($sql);
			$users = array_values(array_unique($users));
			$count = count($users);
			if($returnFullNameList){
				$title = $count > 0 ? implode(", ",$users) : '';
			}else if($count>10){
				$title = str_replace('<!--N-->',$count,$Lang['iCalendar']['NParticipantsInvolved']);
			}else{
				$title = $count == 0? '' : ' ('.implode(", ",$users).')';
			}
		}
		return $title;
	}
	
	function updateEventTitleWithExtraInfo($eventId, $title, $extra_info)
	{
		global $Lang, $sys_custom;
		
		if(!$sys_custom['iCalendar']['EventTitleWithParticipantNames']) return false;
		
		$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Title='".$this->Get_Safe_Sql_Query($title.$extra_info)."',TitleExtraInfo='".$this->Get_Safe_Sql_Query($extra_info)."' WHERE EventID ".(is_array($eventId)?" IN (".implode(",",$eventId).")":"='$eventId'");
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function sendPushMessageNotifyParticipants($eventId)
	{
		global $intranet_root, $PATH_WRT_ROOT, $Lang, $eclassAppConfig;
		
		//include_once($intranet_root."/includes/libuser.php");
		include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
		
		//$libuser = new libuser($_SESSION['UserID']);
		$libeClassApp = new libeClassApp();
		
		//$sender_name = $libuser->DisplayName;
		//$sender_name = Get_Lang_Selection($_SESSION['ChineseName'],$_SESSION['EnglishName']);
		$sender_name = $_SESSION['EnglishName'];
		$name_field = getNameFieldByLang2("u.");
		$sql = "SELECT 
					e.EventID,
					e.EventDate,
					e.IsAllDay,
					e.Title,
					e.Location,
					eu.InviteStatus,
					u.UserID,
					$name_field as UserName 
				FROM CALENDAR_EVENT_ENTRY as e
				INNER JOIN CALENDAR_EVENT_USER as eu ON eu.EventID=e.EventID 
				INNER JOIN INTRANET_USER as u ON u.UserID=eu.UserID 
				WHERE e.EventID='$eventId' 
				ORDER BY UserName";
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		
		if($record_size == 0) return false;
		
		$today = date("Y-m-d");
		$ts = strtotime($records[0]['EventDate']);
		$date = date("Y-m-d",$ts);
		$time = date("H:i",$ts);
		$is_all_day = $records[0]['IsAllDay'] == 1;
		$title = $records[0]['Title'];
		$title_extra_info = $this->getEventTitleExtraInfo($eventId);
		$title = str_replace($title_extra_info, '', $title);
		$venue = trim($records[0]['Location']);
		$is_invite = $records[0]['InviteStatus']==1;
		
		$messageAry = array();
		$messageKey = $is_invite? 'Invitation': 'Compulsory';
		$messageTitle = str_replace('<!--NAME-->', $sender_name, str_replace('<!--DATE-->',$date, $Lang['iCalendar'][$messageKey.'EventPushNotificationTitle']));
		$time_statement = $is_all_day?'':str_replace('<!--TIME-->',$time,$Lang['iCalendar'][$messageKey.'EventPushNotificationContentTimeStatement']);
		$venue_statement = $venue == ''?'' : str_replace('<!--VENUE-->',$venue,$Lang['iCalendar'][$messageKey.'EventPushNotificationContentVenueStatement']);
		$messageContent = str_replace('<!--VENUE_STATEMENT-->',$venue_statement, str_replace('<!--TIME_STATEMENT-->',$time_statement, str_replace('<!--DATE-->',$date, str_replace('<!--TITLE-->',$title,$Lang['iCalendar'][$messageKey.'EventPushNotificationContent']))));
		for($i=0;$i<$record_size;$i++)
		{
			$idAssocAry = array();
			$idAssocAry[$records[$i]['UserID']] = array($records[$i]['UserID']);
			$messageAry[] = array('relatedUserIdAssoAry'=>$idAssocAry,'messageTitle'=>$messageTitle,'messageContent'=>$messageContent);
		}
		
		$result = $libeClassApp->sendPushMessage($messageAry, $messageTitle, $messageContent='MULTIPLE MESSAGES', $isPublic=false, $recordStatus=1, $appType=$eclassAppConfig['appType']['Teacher'], $sendTimeMode='now');
		return $result;
	}
	
	function sendPushMessageReminder($user_id='')
	{
		global $intranet_root, $PATH_WRT_ROOT, $Lang, $eclassAppConfig;
		
		include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
		$libeClassApp = new libeClassApp();
		
		$name_field = getNameFieldByLang2("u.");
		$sql = "SELECT 
					r.ReminderID,
					r.UserID,
					e.EventID,
					e.EventDate,
					e.IsAllDay,
					e.Title,
					e.Location,
					eu.InviteStatus,  
					$name_field as SenderName 
				FROM CALENDAR_REMINDER as r 
				INNER JOIN CALENDAR_EVENT_ENTRY as e ON e.EventID=r.EventID 
				INNER JOIN CALENDAR_EVENT_USER as eu ON eu.EventID=e.EventID AND r.UserID=eu.UserID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=e.UserID 
				WHERE ".($user_id != '' || is_array($user_id)? " r.UserID IN (".implode(",",(array)$user_id).") AND ":"")." UNIX_TIMESTAMP() > UNIX_TIMESTAMP(r.ReminderDate) AND r.LastSent IS NULL 
				ORDER BY r.ReminderDate";
		
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		
		if($record_size == 0) return 0;
		
		$today = date("Y-m-d");
		$reminderIdAry = array();
		$eventIdToAry = array();
		$result = 0;
		// group same event users into a message ary
		for($i=0;$i<$record_size;$i++)
		{
			$reminderIdAry[] = $records[$i]['ReminderID'];
			
			$ts = strtotime($records[$i]['EventDate']);
			$date = date("Y-m-d",$ts);
			$time = date("H:i",$ts);
			$is_all_day = $records[$i]['IsAllDay'] == 1;
			$title = $records[$i]['Title'];
			$title_extra_info = $this->getEventTitleExtraInfo($records[$i]['EventID']);
			$title = str_replace($title_extra_info, '', $title);
			$venue = trim($records[$i]['Location']);
			$is_invite = $records[$i]['InviteStatus']==1;
			$sender_name = $records[$i]['SenderName'];
			
			$messageKey = $is_invite? 'Invitation': 'Compulsory';
			$messageTitle = str_replace('<!--NAME-->', $sender_name, str_replace('<!--DATE-->',$date, $Lang['iCalendar'][$messageKey.'EventPushNotificationTitle']));
			$time_statement = $is_all_day?'':str_replace('<!--TIME-->',$time,$Lang['iCalendar'][$messageKey.'EventPushNotificationContentTimeStatement']);
			$venue_statement = $venue == ''?'' : str_replace('<!--VENUE-->',$venue,$Lang['iCalendar'][$messageKey.'EventPushNotificationContentVenueStatement']);
			$messageContent = str_replace('<!--VENUE_STATEMENT-->',$venue_statement, str_replace('<!--TIME_STATEMENT-->',$time_statement, str_replace('<!--DATE-->',$date, str_replace('<!--TITLE-->',$title,$Lang['iCalendar'][$messageKey.'EventPushNotificationContent']))));
			
			if(!isset($eventIdToAry[$records[$i]['EventID']])){
				$idAssocAry = array();
				$idAssocAry[$records[$i]['UserID']] = array($records[$i]['UserID']);
				$eventIdToAry[$records[$i]['EventID']] = array('relatedUserIdAssoAry'=>$idAssocAry,'messageTitle'=>$messageTitle,'messageContent'=>$messageContent);
			}else{
				$eventIdToAry[$records[$i]['EventID']]['relatedUserIdAssoAry'][$records[$i]['UserID']] = array($records[$i]['UserID']);
			}
		}
		
		$messageAry = array();
		foreach($eventIdToAry as $event_id => $ary)
		{
			$messageAry[] = $ary;
		}
		
		$result = $libeClassApp->sendPushMessage($messageAry, '', $messageContent='MULTIPLE MESSAGES', $isPublic=false, $recordStatus=1, $appType=$eclassAppConfig['appType']['Teacher'], $sendTimeMode='now');
		
		$sql = "UPDATE CALENDAR_REMINDER SET LastSent = '".date("Y-m-d H:i:s")."' WHERE ReminderID IN (".implode(",",$reminderIdAry).") ";
		$this->db_db_query($sql);
		
		return $result;
	}
	
	function getEventDetail($CurrentUserID,$EventID,$CalType)
	{
		$detail = array();
		if($CalType == 1 || $CalType > 3){
			$iCal_api = new icalendar_api($CurrentUserID);
			$detail = $iCal_api->getExternalEventDetail($CalType,$EventID);
		}else{
			$sql = "SELECT 
						a.EventID, a.UserID, a.CalID, a.RepeatID, Date_Format(a.EventDate, '%Y-%m-%d %H:%i:%s') AS EventDate,
						a.Title, a.Description, a.Duration, a.IsImportant, a.IsAllDay, a.Access, a.Location, a.Url, pn.PersonalNote
					FROM CALENDAR_EVENT_ENTRY AS a  
					LEFT JOIN CALENDAR_EVENT_PERSONAL_NOTE AS pn ON a.EventID = pn.EventID and pn.UserID = '".$this->Get_Safe_Sql_Query($CurrentUserID)."' 
					WHERE a.EventID='".$this->Get_Safe_Sql_Query($EventID)."' ";
			$detail = $this->returnArray($sql);
		}
		
		if(count($detail)==0){
			return array();
		}
		$is_repeat_event = $detail[0]['RepeatID'] != '' && $detail[0]['RepeatID'] > 0;
		if($is_repeat_event){
			
			$sql = "SELECT 
						RepeatID, RepeatType, Date_Format(EndDate,'%Y-%m-%d %T') AS EndDate, Frequency, Detail 
					FROM CALENDAR_EVENT_ENTRY_REPEAT 
					WHERE RepeatID='".$detail[0]['RepeatID']."' 
					ORDER BY EndDate";
			$repeat_entries = $this->returnArray($sql);
			$detail[0]['RepeatEntries'] = $repeat_entries;
		}else{
			$detail[0]['RepeatEntries'] = array();
		}
		
		$sql = "SELECT a.*, b.EnglishName, b.ChineseName 
				FROM CALENDAR_EVENT_USER AS a 
				INNER JOIN INTRANET_USER AS b ON b.UserID=a.UserID 
				WHERE a.EventID='".$this->Get_Safe_Sql_Query($EventID)."' 
				GROUP BY a.UserID ";
		$detail[0]['EventUsers'] = $this->returnArray($sql);
		
		return $detail[0];
	}
	/*
	 * e.Status: [A] Accepted, [W] Waiting for accept, [R] Rejected, [M] Maybe join
	 * e.Access: [A] All permission, [R] Read only
	 * e.InviteStatus: [0]/[''] Compulsory, [1] Invitation for participation 
	 */
	function getEventParticipants($EventID,$returnEventIdToRecords=false,$returnUserIdOnly=true,$JoinStatus=array('','A'))
	{
		$EventID = IntegerSafe($EventID);
		//$name_field = getNameFieldByLang2("u.");
		$sql = "SELECT 					
					u.UserID,
					u.UserLogin,
					u.EnglishName,
					u.ChineseName,
					u.ClassName,
					u.ClassNumber,
					u.RecordStatus as UserStatus,
					u.RecordType as UserType,
					e.EventID,
					e.CalID,
					eu.Status,
					eu.Access,
					eu.InviteStatus 
				FROM CALENDAR_EVENT_ENTRY as e
				INNER JOIN CALENDAR_EVENT_USER as eu ON eu.EventID=e.EventID 
				INNER JOIN INTRANET_USER as u ON u.UserID=eu.UserID 
				WHERE e.EventID IN (".(implode(",",(array)$EventID)).") AND eu.Status IN ('".implode("','",(array)$JoinStatus)."')
				ORDER BY e.EventID,u.EnglishName";
		$records = $this->returnResultSet($sql);
		//debug_pr($sql);
		if($returnEventIdToRecords){
			$record_size = count($records);
			$eventIdToRecords = array();
			for($i=0;$i<$record_size;$i++){
				if(!isset($eventIdToRecords[$records[$i]['EventID']])){
					$eventIdToRecords[$records[$i]['EventID']] = array();
				}
				$eventIdToRecords[$records[$i]['EventID']][] = $returnUserIdOnly? $records[$i]['UserID'] : $records[$i];
			}
			return $eventIdToRecords;
		}
		return $records;
	}
	
	// calculate end datetime by start datetime and duration
	function convertToEventEndDatetime($startDatetime,$duration) // $duration is minute
	{
		$start_ts = $this->sqlDatetimeToTimestamp($startDatetime);
		$end_ts = $start_ts + $duration * 60;
		
		$endDatetime = $this->TimestampTosqlDatetime($end_ts);
		return $endDatetime;
	}
	
	function newEventUpdate($currentUserId,$params,$handleBookingRecords=false)
	{
		global $intranet_root, $PATH_WRT_ROOT, $sys_custom, $plugin, $Lang, $intranet_session_language;
		include_once($intranet_root."/lang/lang.$intranet_session_language.php");
		include_once($intranet_root."/lang/ical_lang.$intranet_session_language.php");
		include_once($intranet_root."/home/iCalendar/choose/new/User_Group_Transformer.php");
				
		extract($params);
		
		$transformer = new User_Group_Transformer();
		
		$db = $this;
		$iCal = $this;
		
		$result = array();

		## Get Data
		
		$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($eventDate);
		
		if($ShowInSchoolCalendar){
			$targetStartDate = $eventDate;
			$targetEndDate = $eventEndDate;
		}
		
		$datePieces = explode("-", $eventDate);
		
		$datePieces2 = explode("-", $eventEndDate);
		
		$m = strlen($datePieces[1])==1? "0".$datePieces[1]:$datePieces[1];
		$d = strlen($datePieces[2])==1? "0".$datePieces[2]:$datePieces[2];
		$h = "00";
		if (isset($eventHr))
			$h = strlen($eventHr)==1? "0".$eventHr:$eventHr;
		$i = "00";
		if (isset($eventMin))
			$i = strlen($eventMin)==1? "0".$eventMin:$eventMin;
		$startEvent = $datePieces[0].$m.$d."T".$h.$i."00";
		$eventTimeStamp=mktime($h,$i,0,$m,$d,$datePieces[0]);//modified on 16 July 2009
		$m = strlen($datePieces2[1])==1? "0".$datePieces2[1]:$datePieces2[1];
		$d = strlen($datePieces2[2])==1? "0".$datePieces2[2]:$datePieces2[2];
		$h = "00";
		if (isset($eventEndHr))
			$h = strlen($eventEndHr)==1? "0".$eventEndHr:$eventEndHr;
		$i = "00";
		if (isset($eventEndMin))
			$i = strlen($eventEndMin)==1? "0".$eventEndMin:$eventEndMin;
		$endEvent = $datePieces2[0].$m.$d."T".$h.$i."00";
		
		$duration = $iCal->time_diff_UTC($startEvent,$endEvent);
		if ($duration < 0){
			$Msg = $Lang['ReturnMsg']['EventCreateUnSuccess'];
			//header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&Msg=$Msg");
			//exit;
			return false;
		}
		
		if (!isset($isAllDay) || $isAllDay != 1 || $isAllDay != "1") {
			$isAllDay = 0;
			$eventDate = "$eventDate $eventHr:$eventMin:00";
		} else {
			$eventDate = "$eventDate 00:00:00";
			$duration = $duration + 1440; 
		}
		
		$access == ""?$access="D":$access=$access;
		
		if (!$iCal->isValidURL($url))
			$url=NULL;
			
		
		$eBookingRecordRemarksSql = '';
		$eBookingRecordRemarksSql .= $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title));
		if ($description) {
			$eBookingRecordRemarksSql .= "\n\n".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description));
		}
		
		$allEventIdAry = array();
		
		if ($repeatSelect == "NOT" || !isset($repeatSelect)) {
			// Single Event
			# Insert main detail
			$fieldname  = "UserID, EventDate, InputDate, ModifiedDate, Duration, IsImportant, ";
			$fieldname .= "IsAllDay, Access, Title, Description, Location, Url, CalID"; 
			
			//*********temporary approach**************
			//if($isAllDay=='1'&& $duration == 0)
			//	$duration = 1440;
			//************************************** 
			
			$fieldvalue  = $currentUserId.", '$eventDate', NOW(), NOW(), '$duration', ";
			$fieldvalue .= "'$isImportant', '$isAllDay', '$access', '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
			$fieldvalue .= "'".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', '$url', '$calID'";
			
			$sql = "INSERT INTO CALENDAR_EVENT_ENTRY ($fieldname) VALUES ($fieldvalue)";
			
			$result['insert_event_entry'] = $iCal->db_db_query($sql);
			$eventID = $iCal->db_insert_id();
			
			//global $schoolNameAbbrev;
			$uid = $currentUserId."-{$calID}-{$eventID}@".$_SERVER['SERVER_NAME'];
			$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where EventID = '$eventID'";
			$iCal->db_db_query($sql);
			
			$sql = "select * from  CALENDAR_CALENDAR where CalID = '".trim($calID)."' ";
			$resultSet = $iCal->returnArray($sql);
			
			// insert Personal Note
			if(!in_array(false, $result)){
				$pNote =$iCal->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote));
				$pNote = empty($pNote)?"NULL":"'".$pNote."'";
				$sql = 'insert into CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, PersonalNote, CreateDate, CalType) 
								values 
								(\''.$eventID.'\',\''.$currentUserId.'\','.$pNote.',NOW(),\''.$resultSet[0]["CalType"].'\')';
				$result['insert_personal_note'] = $iCal->db_db_query($sql);
			}
			
			## Show in school calendar (Group Event Only)##
			if($ShowInSchoolCalendar)
			{
				$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
				$arrTargetGroupID = $db->returnVector($sql);
				$targetGroupID = $arrTargetGroupID[0]; 
				
				if($targetGroupID != "")
				{
					$NumOfDay = round( abs(strtotime($targetEndDate)-strtotime($targetStartDate)) / 86400, 0 );
					
					for($i=0; $i<=$NumOfDay; $i++) 
					{
						## Calculate the Event Target Date
						$ts_EventStartDate = strtotime($targetStartDate);
						$ts_TargetEventDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
						$TargetEventDate = date("Y-m-d",$ts_TargetEventDate);
						//$title = trim(stripslashes(urldecode($title)));
						//$description = trim(stripslashes(urldecode($description)));
						$sql = "INSERT INTO 
									INTRANET_EVENT 
									(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
								VALUES
									('$TargetEventDate',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
						$db->db_db_query($sql);
					
						//if($i==0) {
							$ParentSchoolCalEventID = $db->db_insert_id();
							$CurrentSchoolCalEventID = $db->db_insert_id();
						//} else {
						//	$CurrentSchoolCalEventID = $db->db_insert_id();
						//}
										
						$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
						$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
						$db->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EVENT 
								SET 
									RelatedTo = '$ParentSchoolCalEventID',
									DateModified = NOW(),
									ModifyBy = '$currentUserId'
								WHERE 
									EventID = '$CurrentSchoolCalEventID'";
						$db->db_db_query($sql);
						
						$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$eventID','$CurrentSchoolCalEventID')";
						$db->db_db_query($sql);
					}
				}
				## update school calendar (calendar view) 
				include_once($intranet_root."/includes/libcycleperiods.php");
				$lcycleperiods = new libcycleperiods();
				$lcycleperiods->generatePreview();
				$lcycleperiods->generateProduction();
			}
			
			##### EBOOKING #####
			if($plugin['eBooking'] && $handleBookingRecords)
			{
				if($hiddenBookingIDs != "")
				{
					include_once($intranet_root."/includes/libebooking.php");
					$lebooking = new libebooking();
					$hiddenBookingIDs = IntegerSafe($hiddenBookingIDs);
					$hiddenBookingIdAry = explode(',', $hiddenBookingIDs);
								
					$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
					$arrRoomID = $iCal->returnVector($sql);
					$roomId = $arrRoomID[0];
					
					$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($hiddenBookingIdAry);
					$bookingDate = $bookingInfoAry[0]['Date'];
					$bookingStartTime = $bookingInfoAry[0]['StartTime'];
					$bookingEndTime = $bookingInfoAry[0]['EndTime'];
					
					$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
					$IsRoomAvailable = false;
					if ($RoomNeedApproval[$hiddenBookingIDs]) {
						$RoomBookingStatus = 0;
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($hiddenBookingIDs)";
						$iCal->db_db_query($sql);
					
		//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($hiddenBookingIDs)";
		//				$iCal->db_db_query($sql);
					} else {
						$RoomBookingStatus = 1;
						
						$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $bookingDate, $bookingStartTime, $bookingEndTime);
						if ($IsRoomAvailable) {
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
							$iCal->db_db_query($sql);
						}
					
		//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
		//				$iCal->db_db_query($sql);
					}
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET Remark = '".$eBookingRecordRemarksSql."' Where BookingID IN ($hiddenBookingIDs)";
					$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$eventID' WHERE BookingID IN ($hiddenBookingIDs)";
					$iCal->db_db_query($sql);
					
					include_once($PATH_WRT_ROOT."includes/libinventory.php");
					$linventory = new libinventory();
					$sql = "SELECT 
								CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
							FROM
								INVENTORY_LOCATION_BUILDING AS building 
								INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
								INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
								INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = 1)
							WHERE
								BookingID IN ($hiddenBookingIDs)";
								
					$arrLocation = $linventory->returnVector($sql);
		
					$location = $arrLocation[0];
					
					$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$eventID' ";
					$iCal->db_db_query($sql);
					
					if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable) {
						$lebooking->Email_Booking_Result($hiddenBookingIDs);
					}
				}
			}
			
		} else {
			# convert repeat end date to datetime format		//REPEATED EVENT 
			
			$repeatEndDatePieces = explode("-", $repeatEnd);
			
			if ($isAllDay != 1) {
				$repeatEndTime = mktime($eventHr,$eventMin,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
			} else {
				$repeatEndTime = mktime(0,0,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
			}
			$repeatEndDate = date("Y-m-d H:i:s", $repeatEndTime);
			
			// save the repeat pattern for future reference
			$fieldname  = "RepeatType, EndDate, Frequency, Detail";
			$detail = "";
			$frequency = 0;
			switch ($repeatSelect) {
				case "DAILY":
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatDailyDay, NULL";
					$frequency = $repeatDailyDay;
					break;
				case "WEEKDAY":
				case "MONWEDFRI":
				case "TUESTHUR":
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
					break;
				case "WEEKLY":
					$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
					$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatWeeklyRange, '$detail'";
					$frequency = $repeatWeeklyRange;
					break;
				case "MONTHLY":
					if ($monthlyRepeatBy == "dayOfMonth")
						$detail = "day-".$datePieces[2];
					else {
						$eventDateWeekday = date("w", $eventTimeStamp);
						$nth = 0;
						for($i=1; $i<6; $i++) {
							$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
							if ((int)$datePieces[2] == (int)$nthweekday) {
								$nth = $i;
								break;
							} 
						}
						
						$formatDate = $datePieces[0].$datePieces[1].$datePieces[2];
						if (date("m",strtotime("+7 day",$eventTimeStamp))!=$datePieces[1])
							$nth = -1;
						
						$detail = "$nth-$eventDateWeekday";
					}
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
					$frequency = $repeatMonthlyRange;
					break;
				case "YEARLY":
					$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
					$frequency = $repeatYearlyRange;
					break;
			}
			
			$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
			$result['insert_event_repeat'] = $iCal->db_db_query($sql);
			$repeatID = $iCal->db_insert_id();
			
			# Insert the recurrence event series
			$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $currentUserId,
							$eventDate, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
							$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID,$iCal->Get_Request($PersonalNote));
				
			if (!isset($monthlyRepeatBy))
				$monthlyRepeatBy = "";
			
			# Main function for inserting recurrence events
			$result['insert_repeat_event_entry'] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, $isIgnoreModifiedRecord = 1);
			
			$sql = "select min(EventID) as minEvent from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID'";
			$minEventID = $iCal->returnArray($sql);
			
			$uid = $currentUserId."-{$calID}-".$minEventID[0]["minEvent"]."@".$_SERVER['SERVER_NAME'];
			$sql = "update CALENDAR_EVENT_ENTRY set UID='$uid' where RepeatID = '$repeatID'";
			$iCal->db_db_query($sql);
			
			$sql = "select * from  CALENDAR_CALENDAR where CalID = '".trim($calID)."'";
			$resultSet = $iCal->returnArray($sql);
			
			if(!in_array(false, $result)){
			$pNote = $iCal->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote));
				$pNote = empty($pNote)?"NULL":"'".$pNote."'";
				
				$sql = "insert into CALENDAR_EVENT_PERSONAL_NOTE (EventID,UserID,PersonalNote,CreateDate,CalType) 
							select EventID, '".$currentUserId."', ".$pNote.", NOW(), '".$resultSet[0]["CalType"]."'
							from CALENDAR_EVENT_ENTRY where RepeatID = '".$repeatID."'";
				$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
			}
			
			## Show in school calendar (Group Event Only)##
			if($ShowInSchoolCalendar)
			{
				$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
				$arrTargetGroupID = $db->returnVector($sql);
				$targetGroupID = $arrTargetGroupID[0]; 
				
				if($targetGroupID != "")
				{
					$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
					$arrCalendarEvent = $iCal->returnArray($sql,2);
					
					if(sizeof($arrCalendarEvent)>0)
					{
						for($i=0; $i<sizeof($arrCalendarEvent); $i++)
						{
							list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
							
							//$title = trim(stripslashes(urldecode($title)));
							//$description = trim(stripslashes(urldecode($description)));
							
							$sql = "INSERT INTO 
											INTRANET_EVENT 
											(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
									VALUES
											('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
							$iCal->db_db_query($sql);
							
							//if($i==0) {
								$ParentSchoolCalEventID = $db->db_insert_id();
								$CurrentSchoolCalEventID = $db->db_insert_id();
							//} else {
							//	$CurrentSchoolCalEventID = $db->db_insert_id();
							//}
										
							$value = "($targetGroupID,$CurrentSchoolCalEventID,NOW(),NOW())";
							$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
							$db->db_db_query($sql);
						
							$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
						
							$sql = "UPDATE INTRANET_EVENT 
									SET 
										RelatedTo = '$ParentSchoolCalEventID',
										DateModified = NOW(),
										ModifyBy = '$currentUserId'
									WHERE 
										EventID = '$CurrentSchoolCalEventID'";
							$db->db_db_query($sql);
							
							$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
							$db->db_db_query($sql);
						}
					}
				}
				## update school calendar (calendar view) 
				include_once($intranet_root."/includes/libcycleperiods.php");
				$lcycleperiods = new libcycleperiods();
				$lcycleperiods->generatePreview();
				$lcycleperiods->generateProduction();
			}
			
			##### EBOOKING ######
			if($plugin['eBooking'] && $handleBookingRecords)
			{
				if($hiddenBookingIDs != "")
				{
					include_once($intranet_root."/includes/libebooking.php");
					$lebooking = new libebooking();
					$hiddenBookingIDs = IntegerSafe($hiddenBookingIDs);
					$hiddenBookingIdAry = explode(',', $hiddenBookingIDs);
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET Remark = '".$eBookingRecordRemarksSql."' Where BookingID IN ($hiddenBookingIDs)";
					$iCal->db_db_query($sql);
					
					$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
					$arrRoomID = $iCal->returnVector($sql);
					$roomId = $arrRoomID[0];
					
					$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($hiddenBookingIdAry);
					$bookingDate = $bookingInfoAry[0]['Date'];
					$bookingStartTime = $bookingInfoAry[0]['StartTime'];
					$bookingEndTime = $bookingInfoAry[0]['EndTime'];
					
					
					$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
					$IsRoomAvailable = false;
					if ($RoomNeedApproval[$hiddenBookingIDs]) {
						$RoomBookingStatus = 0;
						
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($hiddenBookingIDs)";
						$iCal->db_db_query($sql);
						
		//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($hiddenBookingIDs)";
		//				$iCal->db_db_query($sql);
					} else {
						$RoomBookingStatus = 1;
						
						$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $bookingDate, $bookingStartTime, $bookingEndTime);
						if ($IsRoomAvailable) {
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
							$iCal->db_db_query($sql);
						}
						
		//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($hiddenBookingIDs)";
		//				$iCal->db_db_query($sql);
					}
						
					$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
					$arrEventInfo = $iCal->returnArray($sql,2);
					
					$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs)";
					$arrBookingInfo = $iCal->returnArray($sql,2);
					
					if(sizeof($arrEventInfo)>0)
					{
						for($i=0; $i<sizeof($arrEventInfo); $i++)
						{
							list($event_id, $event_date) = $arrEventInfo[$i];
							$arrEventDate[] = $event_date;
							$arrMergeInfo[$event_date]['EventID'] = $event_id;
						}
					}
					
					if(sizeof($arrBookingInfo)>0)
					{
						for($i=0; $i<sizeof($arrBookingInfo); $i++)
						{
							list($booking_id, $booking_date) = $arrBookingInfo[$i];
							$arrEventDate[] = $booking_date;
							$arrMergeInfo[$booking_date]['BookingID'] = $booking_id;
						}
					}
					$arrEventDate = array_unique($arrEventDate);
					
					foreach($arrEventDate as $key=>$date)
					{
						$targetEventID = $arrMergeInfo[$date]['EventID'];
						$targetBookingID = $arrMergeInfo[$date]['BookingID'];
						
						if(empty($targetBookingID))
							continue;
						
						$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetBookingID'";
						$iCal->db_db_query($sql);
						
						include_once($PATH_WRT_ROOT."includes/libinventory.php");
						$linventory = new libinventory();
						$sql = "SELECT 
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
								FROM
									INVENTORY_LOCATION_BUILDING AS building 
									INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
									INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
									INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
								WHERE
									BookingID IN ($targetBookingID)";
						
						$arrLocation = $linventory->returnVector($sql);
						$location = $arrLocation[0];
						
						$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
						$iCal->db_db_query($sql);
						
						if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable) {
							$lebooking->Email_Booking_Result($targetBookingID);
						}
					}
				}
			}
			
		}
		
		if (!isset($viewer)) {
			$viewer = array();
		}
		$viewer = array_values(array_unique($viewer));
		################################################################################################
		# When create NEW event
		# Case 1: if no any guest is invited , EventType is regarded ar NOT Defined
		# Case 2: if at least one guest is invited, EventType is set to be Compulsory / Optional
		#
		# if number of guest > 0, inviteStatus is valid
		# if number of guest = 0, inviteStatus is not valid and set to NULL
		$InviteStatus = (count($viewer) > 0) ? $InviteStatus : "NULL";
		$InviteEventUserStatus = $InviteStatus == "1" ?  "W" : "A";
		if($InviteStatus == '0') $InviteStatus="NULL"; // if it is compulsory, force to join the event. 
		################################################################################################
		
		# New guest list
		# to be done
		# Original Approach
		$viewerList = array();
		$otherSchoolViewer = array();
		
		for ($i=0; $i<sizeof($viewer); $i++) {
			$viewerPrefix = substr($viewer[$i], 0, 1);
			$viewerID = substr($viewer[$i], 1);
			if ($viewerPrefix == 'U')
				$viewerList[] = $viewerID ;
			else{
				$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
				$viewerList = array_merge($viewerList,$tempGuest);
			}
		}
		################
		
		# remove owner
		//$viewerList = $iCal->removeAnArrayValue($viewerList, $_SESSION['UserID']);
		//$viewerClassList = $iCal->removeAnArrayValue($viewerClassList, $_SESSION['UserID']);
		//$viewerCourseList= $iCal->removeAnArrayValue($viewerCourseList, $_SESSION['UserID']);
		//$viewerGroupList = $iCal->removeAnArrayValue($viewerGroupList, $_SESSION['UserID']);
		
		$viewerList = array_values(array_unique($viewerList));
		
		# remove duplicate reminder
		if(count($reminderTime) > 0){
			for($i=0; $i<sizeof($reminderTime) ; $i++) {
				$reminders[] = $reminderType[$i].$reminderTime[$i];
			}
			array_unique($reminders);
		}
		
		# insert reminder
		//if (isset($reminderType) && isset($reminderTime)) {
		if (isset($reminders) && sizeof($reminders) > 0) {
			// if($CalType == 2 || $CalType == 3)
			// {
				// $fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore, CalType";
			// }else
			// {
				$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
			// }
			if ($repeatSelect == "NOT") {
				# Insert reminder for creator
				//$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
				$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) values ";
				$cnt = 0;
				//for ($i=0; $i<sizeof($reminderType); $i++) {
				for ($i=0; $i<sizeof($reminders); $i++) {
					$rType = substr($reminders[$i], 0 ,1);
					$rTime = substr($reminders[$i], 1);
					# convert duration to actual datetime of sending the reminder
					$reminderDate[$i] = $eventTimeStamp - ((int)$rTime)*60; //modified on 16 July 2009
				//	$reminderDate[$i] = $eventTimeStamp;//modified on 16 July 2009
					$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
					//$sql .= "($eventID, ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$i]."', ".$rTime."), ";
					// if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
					// {
						// if($cnt == 0){
							// $sql .= "SELECT $eventID, ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$i]."', ".$rTime.", ".$CalType;
							// $cnt++;
						// } else {
							// $sql .= " UNION ALL SELECT $eventID, ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$i]."', ".$rTime.", ".$CalType;
						// }
					// }else
					// {
						if($cnt == 0){
							$sql .= "('$eventID', '".$currentUserId."', '".$rType."', '".$reminderDate[$i]."', '".$rTime."')";
							$cnt++;
						} else {
							$sql .= ", ('$eventID', '".$currentUserId."', '".$rType."', '".$reminderDate[$i]."', '".$rTime."')";
						}
					//}
				}
				
				####### Insert a default reminders for guests #######
				$guestReminderDate = $eventTimeStamp - 10*60;	# 10 minutes before the event
				$guestReminderDate = date("Y-m-d H:i:s", $guestReminderDate);
				
				if (isset($viewerList) && sizeof($viewerList) != 0) {
					for ($i=0; $i<sizeof($viewerList); $i++) {
						//$sql .= "($eventID, ".$viewerList[$i].", 'P', '$guestReminderDate', 10), ";
						/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
						{
							if($cnt == 0){
								$sql .= "SELECT $eventID, ".$viewerList[$i].", 'P', '$guestReminderDate', 10, $CalType";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT $eventID, ".$viewerList[$i].", 'P', '$guestReminderDate', 10, $CalType";
							}
						}else
						{*/
							if($cnt == 0){
								$sql .= " ('$eventID', '".$viewerList[$i]."', 'P', '$guestReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('$eventID', '".$viewerList[$i]."', 'P', '$guestReminderDate', 10)";
							}
						//}
					}
				}
				
				if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
					for ($i=0; $i<sizeof($viewerClassList); $i++) {
						//$sql .= "($eventID, ".$viewerClassList[$i].", 'P', '$guestReminderDate', 10), ";
						/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
						{
							if($cnt == 0){
								$sql .= "SELECT $eventID, ".$viewerClassList[$i].", 'P', '$guestReminderDate', 10, $CalType";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT $eventID, ".$viewerClassList[$i].", 'P', '$guestReminderDate', 10, $CalType";
							}
						}else
						{*/
							if($cnt == 0){
								$sql .= " ('$eventID', '".$viewerClassList[$i]."', 'P', '$guestReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('$eventID', '".$viewerClassList[$i]."', 'P', '$guestReminderDate', 10)";
							}
						//}
					}
				}
				
				if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
					for ($i=0; $i<sizeof($viewerCourseList); $i++) {
						//$sql .= "($eventID, ".$viewerCourseList[$i].", 'P', '$guestReminderDate', 10), ";
						/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
						{
							if($cnt == 0){
								$sql .= "SELECT $eventID, ".$viewerCourseList[$i].", 'P', '$guestReminderDate', 10, $CalType";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT $eventID, ".$viewerCourseList[$i].", 'P', '$guestReminderDate', 10, $CalType";
							}
						}else
						{*/
							if($cnt == 0){
								$sql .= "('$eventID', '".$viewerCourseList[$i]."', 'P', '$guestReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('$eventID', '".$viewerCourseList[$i]."', 'P', '$guestReminderDate', 10)";
							}
						//}
					}
				}
				
				if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
					for ($i=0; $i<sizeof($viewerGroupList); $i++) {
						//$sql .= "($eventID, ".$viewerGroupList[$i].", 'P', '$guestReminderDate', 10), ";
						/*if($CalType == 2 || $CalType ==3)//modified on 20 July 2009
						{
							if($cnt == 0){
								$sql .= "SELECT $eventID, ".$viewerGroupList[$i].", 'P', '$guestReminderDate', 10, $CalType";
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT $eventID, ".$viewerGroupList[$i].", 'P', '$guestReminderDate', 10, $CalType";
							}
						}else
						{*/
							if($cnt == 0){
								$sql .= " ('$eventID', '".$viewerGroupList[$i]."', 'P', '$guestReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('$eventID', '".$viewerGroupList[$i]."', 'P', '$guestReminderDate', 10)";
							}
						//}
					}
				}
				
				$sql = rtrim($sql, ", ");
				$result['insert_event_reminder'] = $iCal->db_db_query($sql);
			} else {
				$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
				//$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) ";
				$cnt = 0;
				$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
				for ($i=0; $i<sizeof($repeatSeries) ; $i++) {
				//	$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 16 July 2009
					$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 17 July 2009
					# Insert reminder for creator
					for ($j=0; $j<sizeof($reminders); $j++) {
						$rType = substr($reminders[$j], 0 ,1);		// modified on 14 May 08	$i to $j
						$rTime = substr($reminders[$j], 1);			// modified on 14 May 08
					//	$reminderDate[$j] = $eventTimeStamp - ((int)$rTime)*60;//modified on 17 July 2009
						$reminderDate[$j] = $repeatEventTimeStamp - ((int)$rTime)*60;//modified on 17 July 2009
						$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
						//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$j]."', ".$rTime."), ";
						/*if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
						{
							if($cnt == 0){
								$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$j]."', ".$rTime.", ".$CalType;
								$cnt++;
							} else {
								$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", '".$rType."', '".$reminderDate[$j]."', ".$rTime.", ".$CalType;
							}
						}else
						{*/
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$rType."', '".$reminderDate[$j]."', '".$rTime."')";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$rType."', '".$reminderDate[$j]."', '".$rTime."')";
							}
						//}
					}
					
					$guestReminderDate = $eventTimeStamp - 10*60;	# 10 minutes before the event
					$guestReminderDate = date("Y-m-d H:i:s", $guestReminderDate);
					
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						# Insert reminder for guests with default values
						for ($k=0; $k<sizeof($viewerList); $k++) {
							//$sql .= "(".$repeatSeries[$i]["EventID"].", ".$viewerList[$k].", 'P', '$guestReminderDate', 10), ";
							/*if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
							{
								if($cnt == 0){
									$sql .= "SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerList[$k].", 'P', '$guestReminderDate', 10, $CalType";
									$cnt++;
								} else {
									$sql .= " UNION ALL SELECT ".$repeatSeries[$i]["EventID"].", ".$viewerList[$k].", 'P', '$guestReminderDate', 10, $CalType";
								}
							}else
							{*/
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$guestReminderDate', 10)";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$guestReminderDate', 10)";
								}
							//}
						}
					}
				}
				$sql = rtrim($sql, ", ");
				$result['insert_event_reminder'] = $iCal->db_db_query($sql);	
			}
		}
		
		
		
		# Sharing
		if ($repeatSelect == "NOT" || !isset($repeatSelect)) {
			$allEventIdAry[] = $eventID;
			// insert the owner of the event with "A"(Full) access right
		//	$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
		//	$fieldvalue = $eventID.", ".$_SESSION['UserID'].", 'A', 'A', ".$InviteStatus."";
		//	$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ($fieldvalue)";
			
		//	$result['insert_event_onwer'] = $iCal->db_db_query($sql);
			
			if (isset($viewerList) && sizeof($viewerList) != 0) {
				$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
				$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
				//$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) ";
				$cnt = 0;
				for ($i=0; $i<sizeof($viewerList); $i++) {
					//$sql .= "(".$eventID.", '".$viewerList[$i]."', 'W'), ";
					if($cnt == 0){
						$sql .= "('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$currentUserId?'A':'R')."','".($InviteStatus)."')";
						$cnt++;
					} else {
						$sql .= ", ('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$currentUserId?'A':'R')."','".($InviteStatus)."')";
					}
				}
				$sql = rtrim($sql, ", ");
				$result['insert_event_guest'] = $iCal->db_db_query($sql);
				
				if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
					$title_extra_info = $iCal->getEventTitleExtraInfo($eventID);
					$iCal->updateEventTitleWithExtraInfo($eventID, $iCal->Get_Request($title), $title_extra_info);
				}
			}

		} else {
			$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
			$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
			// $sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) ";
			$cnt1 = 0;
			$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
			for ($i=0; $i<sizeof($repeatSeries); $i++) {
				$allEventIdAry[] = $repeatSeries[$i]["EventID"];
				// insert the owner of the event with "A"(Full) access right
				//$sql1 .= "(".$repeatSeries[$i]["EventID"].", ".$_SESSION['UserID'].", 'A', 'A'), ";
				if($cnt1 == 0){
					$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', 'A', 'A', '".$InviteStatus."')";
					$cnt1++;
				} else {
					$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', 'A', 'A', '".$InviteStatus."')";
				}		
				
				if (isset($viewerList) && sizeof($viewerList) != 0) {
					$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
					$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
					// $sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) ";
					$cnt2 = 0;
					for ($j=0; $j<sizeof($viewerList); $j++) {
						//$sql2 .= "(".$repeatSeries[$i]["EventID"].", '".$viewerList[$j]."', 'W'), ";
						if($cnt2 == 0){
							$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."','".($InviteStatus)."')";
							$cnt2++;
						} else {
							$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."','".($InviteStatus)."')";
						}
					}
					$sql2 = rtrim($sql2, ", ");
					$result['insert_event_guest'] = $iCal->db_db_query($sql2);
				}
				
				
		
		
			}
			$sql1 = rtrim($sql1, ", ");
		//	$result['insert_event_owner'] = $iCal->db_db_query($sql1);
			
			if($sys_custom['iCalendar']['EventTitleWithParticipantNames'])
			{
				for ($i=0; $i<sizeof($repeatSeries); $i++) {
					$title_extra_info = $iCal->getEventTitleExtraInfo($repeatSeries[$i]["EventID"]);
					$iCal->updateEventTitleWithExtraInfo($repeatSeries[$i]["EventID"], $iCal->Get_Request($title), $title_extra_info);
				}
			}
		}
		
		if(!in_array(false, $result)){
			$Msg = $Lang['ReturnMsg']['EventCreateSuccess'];
		} else {
			$Msg = $Lang['ReturnMsg']['EventCreateUnSuccess'];
		}
		
		if($plugin['eClassTeacherApp'] && $submitMode == 'submitAndSendPushMsg' && count($allEventIdAry)>0){
			$iCal->sendPushMessageNotifyParticipants($allEventIdAry[0]);
		}
		
		return !in_array(false, $result);
	}
	
	function editEventUpdate($currentUserId,$params,$handleBookingRecords=false)
	{
		global $intranet_root, $sys_custom, $plugin, $PATH_WRT_ROOT, $Lang, $intranet_session_language;
		include_once($intranet_root."/lang/lang.$intranet_session_language.php");
		include_once($intranet_root."/lang/ical_lang.$intranet_session_language.php");
		include_once($intranet_root."/home/iCalendar/choose/new/User_Group_Transformer.php");
		
		extract($params);
		
		$iCal = $this;
		$db = $this;
		$transformer = new User_Group_Transformer();
			
		## Get Post Data
		$CalType 	 = (isset($CalType) && $CalType != '') ? $CalType : '';
		$ProgrammeID = (isset($ProgrammeID) && $ProgrammeID != '') ? $ProgrammeID : '';		# CPD use only 
		
		## Initialization
		$result = array();
		$viewerList 	  = array();
		$viewerClassList  = array();
		$viewerCourseList = array();
		$viewerGroupList  = array();
		$otherSchoolViewer = array();
		
		if(!isset($viewer)){
			$viewer = array();
		}
		$viewer = array_values(array_unique($viewer));
		$allEventIdAry = array();
		
		## Preparation
		# Check whether it is other Calendar Type Event
		// $isOtherCalType = $iCal->Is_Other_Calendar_Type($CalType);
		
		# User access right for this event
		$userEventAccess = $iCal->returnUserEventAccess($currentUserId,$eventID);
		if (!$userEventAccess&&$CalType != 1){
			//header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]);
			return false;
		}
		
		
		## Handle form parameters
		
		
		$eventTimeStamp = $iCal->sqlDatetimeToTimestamp($eventDateTime);
		
		# this variable won't be change in the code
		$eventTs 	  = $eventTimeStamp;
		$datePieces   = explode("-", $eventDate);
		$duration 	  = (int)$durationHr * 60 + (int)$durationMin;
		
		$datePieces2 = explode("-", $eventEndDate);
		
		$m = strlen($datePieces[1])==1? "0".$datePieces[1]:$datePieces[1];
		$d = strlen($datePieces[2])==1? "0".$datePieces[2]:$datePieces[2];
		$h = "00";
		if (isset($eventHr))
			$h = strlen($eventHr)==1? "0".$eventHr:$eventHr;
		$i = "00";
		if (isset($eventMin))
			$i = strlen($eventMin)==1? "0".$eventMin:$eventMin;
		$startEvent = $datePieces[0].$m.$d."T".$h.$i."00";
		if (empty($eventDate)){
			$eventTimeStamp = strtotime($eventDateTime);
		}
		else
			$eventTimeStamp=mktime($h,$i,0,$m,$d,$datePieces[0]);//modified on 16 July 2009
		$eventTs=$eventTimeStamp;//modified on 16 July 2009
		
		$ebookingEventDate = $eventDate;
		$ebookingStartTime = $h.':'.$i.':00';
		
		
		$m = strlen($datePieces2[1])==1? "0".$datePieces2[1]:$datePieces2[1];
		$d = strlen($datePieces2[2])==1? "0".$datePieces2[2]:$datePieces2[2];
		$h = "00";
		if (isset($eventEndHr))
			$h = strlen($eventEndHr)==1? "0".$eventEndHr:$eventEndHr;
		$i = "00";
		if (isset($eventEndMin))
			$i = strlen($eventEndMin)==1? "0".$eventEndMin:$eventEndMin;
		$endEvent = $datePieces2[0].$m.$d."T".$h.$i."00";
		$duration = $iCal->time_diff_UTC($startEvent,$endEvent);
		
		$ebookingEndTime = $h.':'.$i.':00';
		
		if ($duration < 0){
			$Msg = $Lang['ReturnMsg']['EventCreateUnSuccess'];
			//header("Location: index.php?time=".$_SESSION["iCalDisplayTime"]."&calViewPeriod=".$_SESSION["iCalDisplayPeriod"]."&Msg=$Msg");
			//exit;
			return false;
		}
		
		if (!($CalType == 1 || $CalType > 3 || ($plugin["iCalendarEditByOwner"] && $createdBy!=$currentUserId))){
			if ($userEventAccess=="A" || $userEventAccess=="W") {
				if ($isAllDay != 1) {// updated on 13 May 08
					$eventDateTime = "$eventDate $eventHr:$eventMin:00";
				} else {
					$eventDateTime = "$eventDate 00:00:00";
					$duration = $duration + 1440; 
				}
			}
		}
		
		if($CalType == 2){		// used for group event 
			$OldRepeatID = $repeatID;
		}
		
		if ($isAllDay != 1) {
			$isAllDay = 0;
		}
		
		$insertUserID = (isset($createdBy)) ? $createdBy : $currentUserId;
		$response 	  = ($response == "") ? "W" : $response;
		$url 		  = (!$iCal->isValidURL($url)) ? NULL : $url;
		
		if (isset($calAccess) && !isset($existGuestList)) {
			$existGuestList = $iCal->returnGuestList($eventID);
			//unset($existGuestList[array_search($insertUserID, $existGuestList)]);
		}
		
		$eBookingRecordRemarksSql = '';
		$eBookingRecordRemarksSql .= $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title));
		if ($description) {
			$eBookingRecordRemarksSql .= "\n\n".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description));
		}
		
		################################################################################################
		# Get & Check for Current EventType 
		if ($CalType == 1 || $CalType > 3 || ($plugin["iCalendarEditByOwner"] && $createdBy!=$currentUserId)){ # eclass calendar
			$sql = "select PersonalNoteID from CALENDAR_EVENT_PERSONAL_NOTE where
					UserID = '".$currentUserId."' And EventID = '$eventID'
					and CalType = '$CalType' ";
			$personalNote = $db->returnVector($sql);	
			if (!empty($personalNote)){
				$sql = "Update CALENDAR_EVENT_PERSONAL_NOTE set
						PersonalNote = '".$db->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote))."'
						where PersonalNoteID = '".$personalNote[0]."'
						";
				$result["Update_personal_note_caltype1"] = $db->db_db_query($sql);
			}
			else{
				$sql = "Insert into CALENDAR_EVENT_PERSONAL_NOTE 
				(EventID,UserID,PersonalNote,CreateDate,LastUpdateDate,CalType)
				values
				('$eventID','".$currentUserId."','".$db->Get_Safe_Sql_Query($iCal->Get_Request($PersonalNote))."',now(),now(),'$CalType')";
				$result["insert_personal_note_caltype1"] = $db->db_db_query($sql);
			} 
			
			####  reminder 
			$reminderIDSql = isset($reminderID)?implode("','",$reminderID):'';
			$sql = "delete from CALENDAR_REMINDER where ReminderID not in 
					('$reminderIDSql') 
					and UserID = '".$currentUserId."' 
					and EventID = '$eventID'";
			$result["delete_removeed_reminder"] = $db->db_db_query($sql); 
			$cnt = 0;
			if (count($reminderID)>0){
				foreach ($reminderID as $rid){
					$sql = "update CALENDAR_REMINDER set 
					ReminderDate = '$eventDateTime', 
					ReminderBefore = '".$reminderTime[$cnt]."'
					where ReminderID = '$rid'
					";
					$result["update_reminder_".$cnt] = $db->db_db_query($sql);
					$cnt++;
				}
			}
		
			if (count($reminderTime)>count($reminderID)){
				$sql= "insert into CALENDAR_REMINDER 
					(EventID,UserID,ReminderType,ReminderDate,ReminderBefore,CalType) 
					values			
				";
				for ($i=$cnt; $i<count($reminderTime);$i++){
					$sql .= " ('$eventID','".$currentUserId."','P','$eventDateTime','".$reminderTime[$i]."','$CalType'), ";
				}
				$sql = rtrim($sql,', ');
				$result["insert_reminder"] = $db->db_db_query($sql);
			}
			#######################
			
			if(!in_array(false, $result)){
				$Msg = $Lang['ReturnMsg']['EventUpdateSuccess'];
			} else {
				$Msg = $Lang['ReturnMsg']['EventUpdateUnSuccess'];
			}
			//intranet_closedb();
			//header("Location: index.php?Msg=$Msg");
			//exit;
			return !in_array(false,$result);
		}
		
		
		$EventType = $iCal->Get_Current_Event_Type($calID, $eventID);
		if($EventType != "0" && $EventType != "1"){
			# When create NEW event
			# Case 1: if no any guest is invited , EventType is regarded ar NOT Defined
			# Case 2: if at least one guest is invited, EventType is set to be Compulsory / Optional
			#
			# if number of guest > 0, inviteStatus is valid
			# if number of guest = 0, inviteStatus is not valid and set to NULL
			$InviteStatus = (count($viewer) > 0) ? $InviteStatus : "NULL";
		} else {
			$InviteStatus = ($EventType != $InviteStatus) ? $EventType : $InviteStatus;
		}
		$InviteEventUserStatus = $InviteStatus == "1" ?  "W" : "A";
		################################################################################################
		
		
		# Guests to be removed
		if (isset($removeGuestList)) {
			$temp = array();
			for ($j=0; $j<sizeof($removeGuestList); $j++) {
				if ($removeGuestList[$j] != "-1")
					$temp[] = $removeGuestList[$j];
			}
			$removeGuestList = $temp;
		} 
		
		# New guests
		for ($i=0; $i<sizeof($viewer); $i++) {
			$viewerPrefix = substr($viewer[$i], 0, 1);
			$viewerID = substr($viewer[$i], 1);
			if ($viewerPrefix == 'U')
			$viewerList[] = $viewerID ;
			else{
				$tempGuest = $transformer->get_userID_from_group($viewerPrefix,$viewerID);
				$viewerList = array_merge($viewerList,$tempGuest);
			}
		}
		if(is_array($existGuestList) && count($existGuestList)>0)
		{
			$viewerList = array_values(array_diff($viewerList, $existGuestList));
		}
		// remove owner
		//$viewerList 	  = $iCal->removeAnArrayValue($viewerList, $UserID);
		//$viewerClassList  = $iCal->removeAnArrayValue($viewerClassList, $UserID);
		//$viewerCourseList = $iCal->removeAnArrayValue($viewerCourseList, $UserID);
		//$viewerGroupList  = $iCal->removeAnArrayValue($viewerGroupList, $UserID);
		
		$viewerList = array_values(array_unique($viewerList));
		
		## Main
		if (isset($isRepeatEvent) && $saveRepeatEvent!="OTI")
		{
			#echo "I'm a repeated event but not save OTI (only this instance)"."\n";
			if ($userEventAccess=="A" || $userEventAccess=="W") 
			{
				$repeatEndDatePieces = explode("-", $repeatEnd);
				if ($isAllDay != 1) {	// updated on 13 May 08
					$repeatEndTime = mktime($eventHr,$eventMin,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
				} else {
					$repeatEndTime = mktime(0,0,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
				}
				$repeatEndDate = date("Y-m-d H:i:s", $repeatEndTime);
				
				# Construct the repeat pattern
				switch ($repeatSelect) {
					case "DAILY":
						$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatDailyDay."#";
						break;
					case "WEEKDAY":
					case "MONWEDFRI":
					case "TUESTHUR":
						$repeatPattern = $repeatSelect."#".$repeatEndDate."##";
						break;
					case "WEEKLY":
						$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
						$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
						$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatWeeklyRange."#".$detail;
						break;
					case "MONTHLY":
						if ($monthlyRepeatBy == "dayOfMonth")
							$detail = "day-".$datePieces[2];
						else {
							$eventDateWeekday = date("w", $eventTimeStamp);
							$nth = 0;
							for($i=1; $i<6; $i++) {
								$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
								if ((int)$datePieces[2] == $nthweekday) {
									$nth = $i;
									break;
								}
							}
							$detail = "$nth-$eventDateWeekday";
						}
						$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatMonthlyRange."#".$detail;
						break;
					case "YEARLY":
						$repeatPattern = $repeatSelect."#".$repeatEndDate."#".$repeatYearlyRange."#";
						break;
				}
			}
			
			if ($saveRepeatEvent=="ALL") 
			{
				#echo "Save ALL events instance"."\n";
				$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);	
				
				# Event date and repeat pattern are UNCHANGED
				if ($userEventAccess=="R" || ($oldEventDate==$eventDate && $oldRepeatPattern==$repeatPattern)) {
					for ($i=0; $i<sizeof($repeatSeries); $i++) {
						$allEventIdAry[] = $repeatSeries[$i]["EventID"];
						
						$curRepeatDateTime = $repeatSeries[$i]["EventDate"];
						$curRepeatTimeStamp = $iCal->sqlDatetimeToTimestamp($curRepeatDateTime);
						
						# Update only if user have All/Write permission
						if ($userEventAccess=="A" || $userEventAccess=="W") 
						{
							# Update main detail
							$sql  = "UPDATE CALENDAR_EVENT_ENTRY SET ";
							if (!$isAllDay && $oldEventTime != "$eventHr:$eventMin:00") {
								$existDateTimePiece = explode(" ", $curRepeatDateTime);
								$sql .= "EventDate = '".$existDateTimePiece[0]." $eventHr:$eventMin:00', ";
							}
							#$sql .= "EventDate = '$eventDateTime', ";
							$sql .= "ModifiedDate = NOW(), ";
							$sql .= "Duration = '$duration', ";
							$sql .= "IsImportant = '$isImportant', ";
							$sql .= "IsAllDay = '$isAllDay', ";
							$sql .= "Access = '$access', ";
							$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
							$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
							$sql .= "Location = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
							$sql .= "Url = '$url', ";
							$sql .= "CalID = '$calID'";
							//$sql .= "UID = '".$repeatSeries[0]["UID"]."' ";
							$sql .= "WHERE EventID = ".$repeatSeries[$i]["EventID"];
							
							$result["update_event_entry_ALL_".$i] = $iCal->db_db_query($sql);
							// update personal note
							if ($result["update_event_entry_ALL_".$i]) {
								$result["update_event_entry_personal_note_ALL_".$i] = $iCal->Insert_Personal_Note($repeatSeries[$i]["EventID"], $iCal->Get_Request($PersonalNote), $CalType, '');
							}
						}
						else 
						{
							// update personal note
							$result["update_event_entry_personal_note_ALL_".$i] = $iCal->Insert_Personal_Note($repeatSeries[$i]["EventID"], $iCal->Get_Request($PersonalNote), $CalType, '');
						}
						
						# Reminder
						$reminderID = $iCal->returnReminderID($currentUserId,$repeatSeries[$i]["EventID"]);
						if (sizeof($reminderID)>0 || isset($reminderType)) {
							# $reminderType > $reminderID means new reminder is added
							if (sizeof($reminderType) >= sizeof($reminderID)) {
								for ($j=0; $j<sizeof($reminderType); $j++) {
									$reminderDate[$j] = $curRepeatTimeStamp - ((int)$reminderTime[$j])*60;
									$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
									if ($j < sizeof($reminderID)) {
										# Update existing reminder
										$sql  = "UPDATE CALENDAR_REMINDER SET ";
										$sql .= "ReminderType = '".$reminderType[$j]."', ";
										$sql .= "ReminderDate = '".$reminderDate[$j]."', ";
										$sql .= "LastSent = NULL, ";
										$sql .= "ReminderBefore = '".$reminderTime[$j]."' ";
										$sql .= "WHERE ReminderID = '".$reminderID[$j]."' ";
										$result["update_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
									} else {
										# insert new reminder
										$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
										$fieldvalue = "'".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."'";
										$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ($fieldvalue)";
										$result["insert_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);	
									}
								}
							} else {	# $reminderType < $reminderID means some reminder have to be updated and some have to be removed
								for ($j=0; $j<sizeof($reminderID); $j++) {
									if ($j < sizeof($reminderType)) {
										$reminderDate[$j] = $curRepeatTimeStamp - ((int)$reminderTime[$j])*60;
										$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
										$sql  = "UPDATE CALENDAR_REMINDER SET ";
										$sql .= "ReminderType = '".$reminderType[$j]."', ";
										$sql .= "ReminderDate = '".$reminderDate[$j]."', ";
										$sql .= "LastSent = NULL, ";
										$sql .= "ReminderBefore = '".$reminderTime[$j]."' ";
										$sql .= "WHERE ReminderID = '".$reminderID[$j]."' ";
										$result["update_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
									} else {
										$sql = "DELETE FROM CALENDAR_REMINDER WHERE ReminderID = '".$reminderID[$j]."' ";
										$result["delete_event_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
									}
								}
							}
						}
						
						# Update only if user have All/Write permission
						if (($userEventAccess=="A" || $userEventAccess=="W") ) {								################################# Break Point
							# Remove guests in CALENDAR_EVENT_USER & CALENDAR_REMINDER
							if (isset($removeGuestList) && sizeof($removeGuestList)>0) {
								for ($j=0; $j<sizeof($removeGuestList); $j++) {
									//$pieces = explode($removeGuestList[$j]);
		
									$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$removeGuestList[$j]."' ";//." AND FromSchoolCode is null AND ToSchoolCode is null";
									$result["delete_event_guest_ALL_$i_$j"] = $iCal->db_db_query($sql);
									$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$removeGuestList[$j]."' ";
									$result["delete_event_guest_reminder_ALL_$i_$j"] = $iCal->db_db_query($sql);
									
									// remove personal note of that guests
									$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
									$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$removeGuestList[$j]."' ".$eclassConstraint;//." AND CalType <> 3 AND CalType <> 4";
									$result["delete_event_guest_Personal_Note_ALL_$i_$j"] = $iCal->db_db_query($sql);
								}
							}
							$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
							$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
							$cnt = 0;
							
							$curReminderDate = $curRepeatTimeStamp - 10*60;
							$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
							
							# Add reminders
							if (isset($viewerList) && sizeof($viewerList) != 0) {
								for ($j=0; $j<sizeof($viewerList); $j++) {
									if($cnt == 0){
										$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', 'P', '$curReminderDate', 10)";
										$cnt++;
									} else {
										$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', 'P', '$curReminderDate', 10)";
									}
								}
							}
							
							$sql = rtrim($sql, ", ");
							if($cnt > 0){
								$result["InsertReminder_RepeatAll_$i"] = $iCal->db_db_query($sql);
							}
							
							# Add new guests
							$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
							$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
							$cnt = 0;
							
							if (isset($viewerList) && sizeof($viewerList) != 0) {
								for ($j=0; $j<sizeof($viewerList); $j++) {
									if($cnt == 0){
										$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."','".$InviteStatus."')";
										$cnt++;
									} else {
										$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."','".$InviteStatus."')";
									}
								}
								$sql = rtrim($sql, ", ");
								if($cnt > 0){
									$result["add_event_new_guest_ALL_$i"] = $iCal->db_db_query($sql);
								}
							}
												
							# Update Existing Guest
							# Individual
							if (sizeof($existGuestList) != 0) {
								for ($k=0; $k<sizeof($existGuestList); $k++) {
									if (!in_array($existGuestList[$k], $removeGuestList) && $existGuestList[$k] != $currentUserId) {
										$pieces = explode("-",$existGuestList[$k]);
										$sql3  = "UPDATE CALENDAR_EVENT_USER SET InviteStatus = '".$InviteStatus."' ";
										$sql3 .= "WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$existGuestList[$k]."' ";//." AND FromSchoolCode is null AND ToSchoolCode is null";
										$result["update_event_exist_guest_ALL_$i_$k"] = $iCal->db_db_query($sql3);
									}
								}
							}
						}
						
						# Update user own status
						$sql  = "UPDATE CALENDAR_EVENT_USER SET ";
						if($userEventAccess == 'A'){	// User with All Access right
							$sql .= "InviteStatus = '$InviteStatus' ";
							$sql .= ($EventType != "1") ? "" : ", Status = '$response' ";
						} else {						// User with only read right
							$sql .= "Status = '$response' ";	
						}
						$sql .= "WHERE EventID = '".$repeatSeries[$i]["EventID"]."' AND UserID = '".$currentUserId."' ";
						$result["update_event_guest_status_ALL_$i"] = $iCal->db_db_query($sql);
						
						##### EBOOKING ##### (ALL)
						if($plugin['eBooking'] && $handleBookingRecords)
						{					
							$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$repeatSeries[$i]["EventID"]."' ";
							$arrEventInfo = $iCal->returnArray($sql,2);
							
							if(sizeof($arrEventInfo)>0)
							{
								for($k=0; $k<sizeof($arrEventInfo); $k++)
								{
									list($event_id, $event_datetime) = $arrEventInfo[$k];
									
									$event_date = substr($event_datetime,0,10);
									$event_time = substr($event_datetime,11,strlen($event_datetime));
									
									$arrEventDate[] = $event_datetime;
									$arrMergeInfo[$event_datetime]['EventID'] = $event_id;
									$arrMergeInfo[$event_datetime]['EventDate'] = $event_date;
									
									### Get New BookingID
									$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
									$arrNewBooking = $iCal->returnArray($sql,2);
									if(sizeof($arrNewBooking)>0) {
										list($new_booking_id, $new_booking_date) = $arrNewBooking[0];
										$arrMergeInfo[$event_datetime]['NewBookingID'] = $new_booking_id;
									} else {
										$arrMergeInfo[$event_datetime]['NewBookingID'] = "";
									}
									
									### Get Old BookingID
									$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '".$repeatSeries[$i]["EventID"]."' ";
									$arrOldBooking = $iCal->returnArray($sql,2);
									if(sizeof($arrOldBooking)>0) {
										list($old_booking_id, $old_booking_date) = $arrOldBooking[0];
										$arrMergeInfo[$event_datetime]['OldBookingID'] = $old_booking_id;
									} else {
										$arrMergeInfo[$event_datetime]['OldBookingID'] = "";
									}
								}
							}
							$arrEventDate = array_unique($arrEventDate);
						}
						
						## (CONT.) Event date and repeat pattern are UNCHANGED ##
						## Show in school calendar (Group Event Only) (Click : Save ALL) ##
						if($ShowInSchoolCalendar)
						{
							$iCalendarEventID = $repeatSeries[$i]["EventID"];
							
							$event_date = date("Y-m-d",$iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]));
							
							### GET EventID In School Calendar
							$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID' ";
							$arr_SchoolCalEventID = $iCal->returnVector($sql);
							
							if(sizeof($arr_SchoolCalEventID) > 0)
							{
								$SchoolCalEventID = $arr_SchoolCalEventID[0];
								$arrUpdateCalenderEventID[] = $iCalendarEventID;
							
								### GET GroupID 
								$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
								$arrGroupID = $iCal->returnVector($sql);
								$targetGroupID = $arrGroupID[0];
								
								$sql = "UPDATE INTRANET_EVENT SET ";
								$sql .= "EventDate = '".$event_date."', ";
								$sql .= "RecordType = '2', ";
								$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
								$sql .= "EventVenue = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
								$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
								$sql .= "DateModified = NOW(), ";
								$sql .= "ModifyBy = '".$currentUserId."' ";
								$sql .= "WHERE EventID = '".$SchoolCalEventID."' ";
								$iCal->db_db_query($sql);
															
								$sql = "UPDATE INTRANET_GROUPEVENT SET GroupID = '".$targetGroupID."', DateModified = NOW() WHERE EventID = '".$SchoolCalEventID."' ";
								$db->db_db_query($sql);
							}
							else
							{
								$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
								$arrTargetGroupID = $db->returnVector($sql);
								$targetGroupID = $arrTargetGroupID[0]; 
								
								if($targetGroupID != "")
								{
									$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
									$arrCalendarEvent = $iCal->returnArray($sql,2);
									
									if(sizeof($arrCalendarEvent)>0)
									{
										for($i=0; $i<sizeof($arrCalendarEvent); $i++)
										{
											list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
											
											//$title = trim(stripslashes(urldecode($title)));
											//$description = trim(stripslashes(urldecode($description)));
											
											$sql = "INSERT INTO 
															INTRANET_EVENT 
															(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
													VALUES
															('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
											$iCal->db_db_query($sql);
											
											$ParentSchoolCalEventID = $db->db_insert_id();
											$CurrentSchoolCalEventID = $db->db_insert_id();
														
											$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
											$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
											$db->db_db_query($sql);
										
											$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
										
											$sql = "UPDATE INTRANET_EVENT 
													SET 
														RelatedTo = '$ParentSchoolCalEventID',
														DateModified = NOW(),
														ModifyBy = '$currentUserId'
													WHERE 
														EventID = '$CurrentSchoolCalEventID'";
											$db->db_db_query($sql);
											
											$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
											$db->db_db_query($sql);
										}
									}
								}
							}
						}
						else
						{
							$iCalendarEventID = $repeatSeries[$i]["EventID"];
							$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
							$arr_SchoolCalEventID = $iCal->returnVector($sql);
							if(sizeof($arr_SchoolCalEventID) > 0)
							{
								$SchoolCalEventID = $arr_SchoolCalEventID[0];
								$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."'";
								$iCal->db_db_query($sql);
								
								$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."'";
								$iCal->db_db_query($sql);
								
								$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
								$iCal->db_db_query($sql);
							}
						}
					}
					// [2014-11-06 Carlos]: Comment the following block of code. Why delete INTRANET_EVENT while the above for loop has update/delete INTRAENT_EVENT ?
					/*
					if(sizeof($arrUpdateCalenderEventID) > 0){
						$targetUpdateCalenderEventID = implode(",",$arrUpdateCalenderEventID);
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID NOT IN ($targetUpdateCalenderEventID)";
						$arrNotUpdatedSchoolEventID = $iCal->returnVector($sql);
						if(sizeof($arrNotUpdatedSchoolEventID) > 0)
						{
							$targetNotUpdatedSchoolEventID = implode(",",$arrNotUpdatedSchoolEventID);
							$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetNotUpdatedSchoolEventID)";
							$iCal->db_db_query($sql);
						}
					}
					*/
					## update school calendar (calendar view)
					if($ShowInSchoolCalendar)
					{ 
						include_once($intranet_root."/includes/libcycleperiods.php");
						$lcycleperiods = new libcycleperiods();
						$lcycleperiods->generatePreview();
						$lcycleperiods->generateProduction();
					}
					
					##### EBOOKING ##### (ALL)
					if($plugin['eBooking'] && $handleBookingRecords)
					{
						include_once($intranet_root."/includes/libebooking.php");
						$lebooking = new libebooking();
						
						foreach($arrEventDate as $key=>$date)
						{
							$targetEventID = $arrMergeInfo[$date]['EventID'];
							$targetEventDate = $arrMergeInfo[$date]['EventDate'];
							$targetNewBookingID = $arrMergeInfo[$date]['NewBookingID'];
							$targetOldBookingID = $arrMergeInfo[$date]['OldBookingID'];
							
							if($targetNewBookingID != "") {
								$targetNewBookingIDAry = explode(',', $targetNewBookingID);
								
								$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNewBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
								$arrRoomID = $iCal->returnVector($sql);
								$roomId = $arrRoomID[0];
								
		//						$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($targetNewBookingIDAry);
		//						$bookingDate = $bookingInfoAry[0]['Date'];
		//						$bookingStartTime = $bookingInfoAry[0]['StartTime'];
		//						$bookingEndTime = $bookingInfoAry[0]['EndTime'];
								
								$RoomNeedApproval[$targetNewBookingID] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
								$IsRoomAvailable = false;
								
								if ($RoomNeedApproval[$targetNewBookingID]) {
									$RoomBookingStatus = 0;
									
									$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($targetNewBookingID)";
									$iCal->db_db_query($sql);
									
		//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($targetNewBookingID)";
		//							$iCal->db_db_query($sql);
								} else {
									$RoomBookingStatus = 1;
									
									$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $targetEventDate, $ebookingStartTime, $ebookingEndTime);
									if ($IsRoomAvailable) {
										$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
										$iCal->db_db_query($sql);
									}
									
		//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
		//							$iCal->db_db_query($sql);
								}
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD Set StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '$targetNewBookingID'";
								$iCal->db_db_query($sql);
								
								
								$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetNewBookingID'";
								$iCal->db_db_query($sql);
								
								
								
								include_once($intranet_root."/includes/libinventory.php");
								$linventory = new libinventory();
								$sql = "SELECT 
											CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
										FROM
											INVENTORY_LOCATION_BUILDING AS building 
											INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
											INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
											INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
										WHERE
											BookingID IN ($targetNewBookingID)";
								$arrLocation = $linventory->returnVector($sql);
								$location = $arrLocation[0];
								
								$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
								$iCal->db_db_query($sql);
							}
							
							if($targetOldBookingID != "" && $targetOldBookingID != $targetNewBookingID) {
								$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$targetOldBookingID'";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
		//						$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
							}
							
							if ($RoomNeedApproval[$targetNewBookingID] == false && $IsRoomAvailable) {
								$lebooking->Email_Booking_Result($targetNewBookingID);
							}
						}
					}
				}
				# Event date is changed or repeat pattern is changed
				else if ($userEventAccess!="R" && ($oldEventDate!=$eventDate || $oldRepeatPattern!=$repeatPattern)) 
				{
					#echo "Event Date or Repeat Pattern have been changed!!!\n";
					
					if($CalType == 2){					### delete existing target Group Events in School Calendar 
						### delete old data [start]
						$sql = "SELECT relation.SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation INNER JOIN CALENDAR_EVENT_ENTRY as entry on (relation.CalendarEventID = entry.EventID) WHERE entry.RepeatID = '$OldRepeatID'";
						$arr_SchoolCalEventID = $iCal->returnVector($sql);
						if(sizeof($arr_SchoolCalEventID) > 0)
						{
							//$SchoolCalEventID = $arr_SchoolCalEventID[0];
							$SchoolCalEventID = implode(",",$arr_SchoolCalEventID);
							$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$SchoolCalEventID.")";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$SchoolCalEventID.")";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN (".$SchoolCalEventID.")";
							$iCal->db_db_query($sql);
						}
						### delete old data [end]
					}
					
					$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
					$result['delete_repeat_event_series_ALL'] = $iCal->deleteRepeatEventSeries($repeatID, (($oldRepeatPattern!=$repeatPattern)?TRUE:FALSE));
					
					if ($oldRepeatPattern != $repeatPattern) {
						#echo "Repeat Pattern have been changed!!!\n";
						// save the repeat pattern for future reference
						$fieldname  = "RepeatType, EndDate, Frequency, Detail";
						$detail = "";
						$frequency = 0;
						switch ($repeatSelect) {
							case "DAILY":
								$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatDailyDay, NULL";
								$frequency = $repeatDailyDay;
								break;
							case "WEEKDAY":
							case "MONWEDFRI":
							case "TUESTHUR":
								$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
								break;
							case "WEEKLY":
								$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
								$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
								$fieldvalue = "'$repeatSelect', '$repeatEndDate', $repeatWeeklyRange, '$detail'";
								$frequency = $repeatWeeklyRange;
								break;
							case "MONTHLY":
								if ($monthlyRepeatBy == "dayOfMonth")
									$detail = "day-".$datePieces[2];
								else {
									$eventDateWeekday = date("w", $eventTimeStamp);
									$nth = 0;
									for($i=1; $i<6; $i++) {
										$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
										if ((int)$datePieces[2] == $nthweekday) {
											$nth = $i;
											break;
										}
									}
									$detail = "$nth-$eventDateWeekday";
								}
								$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
								$frequency = $repeatMonthlyRange;
								break;
							case "YEARLY":
								$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
								$frequency = $repeatYearlyRange;
								break;
						}
						$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
						$result['insert_event_repeat_info_ALL'] = $iCal->db_db_query($sql);
						$repeatID = $iCal->db_insert_id();
					}
					
					
					# Insert the recurrence event series
					$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $insertUserID,
									$eventDateTime, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
									$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID);
					
					if (!isset($monthlyRepeatBy))
						$monthlyRepeatBy = "";
					
					# Main function for inserting recurrence events
					$result['insert_repeat_event_series_ALL'] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, 1, $repeatSeries[0]["UID"], $repeatSeries[0]["ExtraIcalInfo"]);
					
					# insert current personal note into to new event series
					if(!in_array(false, $result)){
						$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, PersonalNote, CalType, CreateDate) 
								SELECT EventID, '".$currentUserId."', '".$iCal->Get_Request($PersonalNote)."', '$CalType', NOW()
								FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '".$repeatID."'";
						$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
					}
					
					# Insert reminders
					
					$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
					$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
					$cnt = 0;
					
					$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
					for ($i=0; $i<sizeof($repeatSeries); $i++) {
						$allEventIdAry[] = $repeatSeries[$i]["EventID"];
						
						$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 16 July 2009
						# reminders for event creator
						for ($j=0; $j<sizeof($reminderType); $j++) {
							$reminderDate[$j] = $repeatEventTimeStamp - ((int)$reminderTime[$j])*60;//modified on 17 July 2009
							$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
							}
						}
						
						# reminder for newly add guests
						$curReminderDate = $repeatEventTimeStamp - 10*60;//modified on 17 July 2009
						$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
						
						if (isset($viewerList) && sizeof($viewerList) != 0) {
							for ($k=0; $k<sizeof($viewerList); $k++) {
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
								}
							}
						}
						
						# reminder for existing guests
						if (sizeof($existGuestList) != 0) {
							for ($l=0; $l<sizeof($existGuestList); $l++) {
								if (!in_array($existGuestList[$l], $removeGuestList) && $existGuestList[$l] != $currentUserId) {
									if($cnt == 0){
										$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
										$cnt++;
									} else {
										$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
									}
								}
							}
						}
						
						##### EBOOKING ##### (ALL)
						if($plugin['eBooking'] && $handleBookingRecords)
						{					
							$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
							$arrEventInfo = $iCal->returnArray($sql,2);
							
							if(sizeof($arrEventInfo)>0)
							{
								for($k=0; $k<sizeof($arrEventInfo); $k++)
								{
									list($event_id, $event_datetime) = $arrEventInfo[$k];
									
									$event_date = substr($event_datetime,0,10);
									$event_time = substr($event_datetime,11,strlen($event_datetime));
									
									$arrEventDate[] = $event_datetime;
									$arrMergeInfo[$event_datetime]['EventID'] = $event_id;
									$arrMergeInfo[$event_datetime]['EventDate'] = $event_date;
									
									### Get New BookingID
									$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
									$arrNewBooking = $iCal->returnArray($sql,2);
									if(sizeof($arrNewBooking)>0) {
										list($new_booking_id, $new_booking_date) = $arrNewBooking[0];
										$arrMergeInfo[$event_datetime]['NewBookingID'] = $new_booking_id;
									} else {
										$arrMergeInfo[$event_datetime]['NewBookingID'] = "";
									}
									
									### Get Old BookingID
									$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
									$arrOldBooking = $iCal->returnArray($sql,2);
									if(sizeof($arrOldBooking)>0) {
										list($old_booking_id, $old_booking_date) = $arrOldBooking[0];
										$arrMergeInfo[$event_datetime]['OldBookingID'] = $old_booking_id;
									} else {
										$arrMergeInfo[$event_datetime]['OldBookingID'] = "";
									}
								}
							}
							$arrEventDate = array_unique($arrEventDate);
						}
						
						## (CONT.) # Event date is changed or repeat pattern is changed ##
						## Show in school calendar (Group Event Only) (Click : Save ALL) ##
						if($ShowInSchoolCalendar)
						{
							$iCalendarEventID = $repeatSeries[$i]["EventID"];
							$event_date = date("Y-m-d",$iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]));
							
							### GET EventID In School Calendar
							$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
							$arr_SchoolCalEventID = $iCal->returnVector($sql);
							
							if(sizeof($arr_SchoolCalEventID) > 0)
							{
								//debug("Update");
								$SchoolCalEventID = $arr_SchoolCalEventID[0];
								
								$arrUpdateCalenderEventID[] = $iCalendarEventID;
							
								### GET GroupID 
								$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
								$arrGroupID = $iCal->returnVector($sql);
								$targetGroupID = $arrGroupID[0];
								
								$sql = "UPDATE INTRANET_EVENT SET ";
								$sql .= "EventDate = '".$event_date."', ";
								$sql .= "RecordType = '2', ";
								$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
								$sql .= "EventVenue = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
								$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
								$sql .= "DateModified = NOW(), ";
								$sql .= "ModifyBy = '".$currentUserId."' ";
								$sql .= "WHERE EventID = '".$SchoolCalEventID."' ";
								$iCal->db_db_query($sql);
															
								$sql = "UPDATE INTRANET_GROUPEVENT SET GroupID = '".$targetGroupID."', DateModified = NOW() WHERE EventID = '".$SchoolCalEventID."'";
								$db->db_db_query($sql);
							}
							else
							{
								//debug("Insert");
								$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\'';
								$arrTargetGroupID = $db->returnVector($sql);
								$targetGroupID = $arrTargetGroupID[0]; 
								
								if($targetGroupID != "")
								{
									$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
									$arrCalendarEvent = $iCal->returnArray($sql,2);
									
									if(sizeof($arrCalendarEvent)>0)
									{
										for($i=0; $i<sizeof($arrCalendarEvent); $i++)
										{
											list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
											
											//$title = trim(stripslashes(urldecode($title)));
											//$description = trim(stripslashes(urldecode($description)));
											
											$sql = "INSERT INTO 
															INTRANET_EVENT 
															(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
													VALUES
															('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
											$iCal->db_db_query($sql);
											
											$ParentSchoolCalEventID = $db->db_insert_id();
											$CurrentSchoolCalEventID = $db->db_insert_id();
														
											$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
											$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
											$db->db_db_query($sql);
											
											$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
										
											$sql = "UPDATE INTRANET_EVENT 
													SET 
														RelatedTo = '$ParentSchoolCalEventID',
														DateModified = NOW(),
														ModifyBy = '$currentUserId'
													WHERE 
														EventID = '$CurrentSchoolCalEventID'";
											$db->db_db_query($sql);
											
											$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
											$db->db_db_query($sql);
										}
									}
								}
							}
						}
						else
						{
							//debug("Delete");
							$iCalendarEventID = $repeatSeries[$i]["EventID"];
							$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
							$arr_SchoolCalEventID = $iCal->returnVector($sql);
							if(sizeof($arr_SchoolCalEventID) > 0)
							{
								$SchoolCalEventID = $arr_SchoolCalEventID[0];
								$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."' ";
								$iCal->db_db_query($sql);
								
								$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."' ";
								$iCal->db_db_query($sql);
								
								$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
								$iCal->db_db_query($sql);
							}
						}
					}
					// [2014-11-06 Carlos]: Comment the following block of code. Why delete INTRANET_EVENT while the above for loop has update/delete INTRAENT_EVENT ?
					/*
					if(sizeof($arrUpdateCalenderEventID) > 0){
						$targetUpdateCalenderEventID = implode(",",$arrUpdateCalenderEventID);
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID NOT IN ($targetUpdateCalenderEventID)";
						$arrNotUpdatedSchoolEventID = $iCal->returnVector($sql);
						if(sizeof($arrNotUpdatedSchoolEventID) > 0)
						{
							$targetNotUpdatedSchoolEventID = implode(",",$arrNotUpdatedSchoolEventID);
							$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetNotUpdatedSchoolEventID)";
							$iCal->db_db_query($sql);
						}
					}
					*/
					## update school calendar (calendar view)
					if($ShowInSchoolCalendar)
					{ 
						include_once($intranet_root."/includes/libcycleperiods.php");
						$lcycleperiods = new libcycleperiods();
						$lcycleperiods->generatePreview();
						$lcycleperiods->generateProduction();
					}
					
					if($plugin['eBooking'] && $handleBookingRecords)
					{
						include_once($intranet_root."/includes/libebooking.php");
						$lebooking = new libebooking();
						foreach($arrEventDate as $key=>$date)
						{
							$targetEventID = $arrMergeInfo[$date]['EventID'];
							$targetEventDate = $arrMergeInfo[$date]['EventDate'];
							$targetNewBookingID = $arrMergeInfo[$date]['NewBookingID'];
							$targetOldBookingID = $arrMergeInfo[$date]['OldBookingID'];
							
							
							if($targetNewBookingID != "") {
								$targetNewBookingIDAry = explode(',', $targetNewBookingID);
								
								$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNewBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
								$arrRoomID = $iCal->returnVector($sql);
								$roomId = $arrRoomID[0];
								
		//						$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($targetNewBookingIDAry);
		//						$bookingDate = $bookingInfoAry[0]['Date'];
		//						$bookingStartTime = $bookingInfoAry[0]['StartTime'];
		//						$bookingEndTime = $bookingInfoAry[0]['EndTime'];
								
								$RoomNeedApproval[$targetNewBookingID] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
								$IsRoomAvailable = false;
								
								if ($RoomNeedApproval[$targetNewBookingID]) {
									$RoomBookingStatus = 0;
									
									$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($targetNewBookingID)";
									$iCal->db_db_query($sql);
									
		//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($targetNewBookingID)";
		//							$iCal->db_db_query($sql);
								} else {
									$RoomBookingStatus = 1;
									
									$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $targetEventDate, $ebookingStartTime, $ebookingEndTime);
									if ($IsRoomAvailable) {
										$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
										$iCal->db_db_query($sql);
									}
									
		//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
		//							$iCal->db_db_query($sql);
								}
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD Set StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '$targetNewBookingID'";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetNewBookingID'";
								$iCal->db_db_query($sql);
								
								include_once($intranet_root."/includes/libinventory.php");
								$linventory = new libinventory();
								$sql = "SELECT 
											CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
										FROM
											INVENTORY_LOCATION_BUILDING AS building 
											INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
											INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
											INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
										WHERE
											BookingID IN ($targetNewBookingID)";
								$arrLocation = $linventory->returnVector($sql);
								$location = $arrLocation[0];
								
								$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
								$iCal->db_db_query($sql);
							}
							
							if($targetOldBookingID != "" && $targetOldBookingID != $targetNewBookingID) {
								$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$targetOldBookingID'";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
		//						$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
							}
							
							if ($RoomNeedApproval[$targetNewBookingID] == false && $IsRoomAvailable) {
								$lebooking->Email_Booking_Result($targetNewBookingID);
							}
						}
					}
					
					$sql = rtrim($sql, ", ");
					if($cnt > 0){
						$result['insert_event_reminder_ALL'] = $iCal->db_db_query($sql);
					}
					
					# Insert guests
					$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
					$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
					$cnt1 = 0;
					
					for ($i=0; $i<sizeof($repeatSeries); $i++) {
						if($cnt1 == 0){
							$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', 'A', 'A', '".$InviteStatus."')";
							$cnt1++;
						} else {
							$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', 'A', 'A', '".$InviteStatus."')";
						}
						
						if (isset($viewerList) && sizeof($viewerList) != 0) {
							$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
							$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
							$cnt2 = 0;
							for ($j=0; $j<sizeof($viewerList); $j++) {
								if($cnt2 == 0){
									$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
									$cnt2++;
								} else {
									$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
								}
							}
							$sql2 = rtrim($sql2, ", ");
							if($cnt2 > 0){
								$result["insert_event_guest_ALL_$i"] = $iCal->db_db_query($sql2);
							}
						}
						
						#other school guest
						
						if (sizeof($existGuestList) != 0) {
							$fieldname3  = "EventID, UserID, Status, Access, InviteStatus";
							$sql3 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) VALUES ";
							$cnt3 = 0;
							$islocal = false;
							for ($k=0; $k<sizeof($existGuestList); $k++) {
								if (!in_array($existGuestList[$k], $removeGuestList) && $existGuestList[$k] != $currentUserId) {
									$pieces = explode("-",$existGuestList[$k]);
									$islocal  = true;
									if($cnt3 == 0){
										$sql3 .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$currentUserId?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
										$cnt3++;
									} else {
										$sql3 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$currentUserId?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
									}
								}
							}
							if ($islocal){
								$sql3 = rtrim($sql3, ", ");
								if ($sql3 != "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) VALUES ") {
									$result["insert_exist_event_guest_ALL_$i"] = $iCal->db_db_query($sql3);
								}
							}
						}
					}
					$sql1 = rtrim($sql1, ", ");
		//			$result["insert_event_owner_ALL"] = $iCal->db_db_query($sql1);
				}
			} 
			else if ($userEventAccess!="R" &&  $saveRepeatEvent=="FOL")
			{
				#echo "Save ALL FOLLOWING events"."\n";
				
				# delete this and following events from the original recurrence series
				$cutOffDate = $oldEventDate." 00:00:00";
				
				if($plugin['eBooking']) {
					$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID' AND EventDate >= '$cutOffDate'";
					$arrOldEventID = $iCal->returnVector($sql);
				}
				
				if($CalType == 2){					### delete existing target Group Events in School Calendar 
					### delete old data [start]
					$sql = "SELECT relation.SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation INNER JOIN CALENDAR_EVENT_ENTRY as entry on (relation.CalendarEventID = entry.EventID) WHERE entry.RepeatID = '$OldRepeatID' AND entry.EventDate >= '$cutOffDate'";
					$arr_SchoolCalEventID = $iCal->returnVector($sql);
					if(sizeof($arr_SchoolCalEventID) > 0)
					{
						$SchoolCalEventID = implode(",",$arr_SchoolCalEventID);
						$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$SchoolCalEventID.")";
						$iCal->db_db_query($sql);
										
						$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$SchoolCalEventID.")";
						$iCal->db_db_query($sql);
										
						$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN (".$SchoolCalEventID.")";
						$iCal->db_db_query($sql);
					}
					### delete old data [end]
				}
				
				$result["delete_repeat_fol_events"] = $iCal->deleteRepeatFollowingEvents($cutOffDate, $repeatID);
				
				# save the new repeat pattern
				$fieldname  = "RepeatType, EndDate, Frequency, Detail";
				$detail = "";
				$frequency = 0;
				switch ($repeatSelect) {
					case "DAILY":
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatDailyDay', NULL";
						$frequency = $repeatDailyDay;
						break;
					case "WEEKDAY":
					case "MONWEDFRI":
					case "TUESTHUR":
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
						break;
					case "WEEKLY":
						$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
						$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatWeeklyRange', '$detail'";
						$frequency = $repeatWeeklyRange;
						break;
					case "MONTHLY":
						if ($monthlyRepeatBy == "dayOfMonth")
							$detail = "day-".$datePieces[2];
						else {
							$eventDateWeekday = date("w", $eventTimeStamp);
							$nth = 0;
							for($i=1; $i<6; $i++) {
								$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
								if ((int)$datePieces[2] == $nthweekday) {
									$nth = $i;
									break;
								}
							}
							$detail = "$nth-$eventDateWeekday";
						}
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
						$frequency = $repeatMonthlyRange;
						break;
					case "YEARLY":
						$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
						$frequency = $repeatYearlyRange;
						break;
				}
				$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
				$result["insert_event_repeat_info_FOL"] = $iCal->db_db_query($sql);
				$repeatID = $iCal->db_insert_id();
				
				# Insert the new recurrence event series
				$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $insertUserID,
								$eventDateTime, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
								$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID);
				
				if (!isset($monthlyRepeatBy))
					$monthlyRepeatBy = "";
				
				# Main function for inserting recurrence events
				$result["insert_repeat_event_series_FOL"] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, 1);
				
				$sqlR = "select * from CALENDAR_EVENT_ENTRY where RepeatID = '$repeatID' Order by EventID";
				$resultSet = $iCal->returnArray($sqlR);
				$newUID = $currentUserId."-".$resultSet[0]['CalID'].'-'.$resultSet[0]['EventID']."@".$_SESSION["SchoolCode"].".tg";
				$sqlR = "update CALENDAR_EVENT_ENTRY set UID = '$newUID' where RepeatID = '$repeatID'";
				$result["insert_repeat_event_series_FOL_insert_UID"] = $iCal->db_db_query($sqlR);
				
				# insert current personal note into to new event series
				if(!in_array(false, $result)){
					$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, CalType, PersonalNote, CreateDate) 
							SELECT EventID, '".$currentUserId."', '$CalType', '".($iCal->Get_Request($PersonalNote))."', NOW()
							FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = ".$repeatID;
					$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
				}
				# get the EventIDs of the recurrence series 
				$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
				
				# Insert reminder
				$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
				$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
				$cnt = 0;
				for ($i=0; $i<sizeof($repeatSeries); $i++) {
					$allEventIdAry[] = $repeatSeries[$i]["EventID"];
					
					$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 17 July 2009
					for ($j=0; $j<sizeof($reminderType); $j++) {
						$reminderDate[$j] = $repeatEventTimeStamp - ((int)$reminderTime[$j])*60;//modified on 17 July 2009
						$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
						if($cnt == 0){
							$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
							$cnt++;
						} else {
							$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderTime[$j]."')";
						}
					}
					
					$curReminderDate = $repeatEventTimeStamp - 10*60;//modified on 17 July 2009
					$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
					
					# reminder for newly add guests
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						for ($k=0; $k<sizeof($viewerList); $k++) {
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', ".$viewerList[$k].", 'P', '$curReminderDate', 10)";
							}
						}
					}
		
					# reminder for existing guests
					if (sizeof($existGuestList) != 0) {
						for ($l=0; $l<sizeof($existGuestList); $l++) {
							if (!in_array($existGuestList[$l], $removeGuestList) && $existGuestList[$l] != $currentUserId) {
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
								}
							}
						}
					}
				}
				$sql = rtrim($sql, ", ");
				if($cnt > 0){
					$result['insert_event_reminder_FOL'] = $iCal->db_db_query($sql);
				}
				
				# Insert guests
				$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
				$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
				$cnt1 = 0;
				
				for ($i=0; $i<sizeof($repeatSeries); $i++) {
					if($cnt1 == 0){
						$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', 'A', 'A', '".$InviteStatus."')";
						$cnt1++;
					} else {
						$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', 'A', 'A', '".$InviteStatus."')";
					}
					
					# Add new guests
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
						$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
						$cnt2 = 0;
						for ($j=0; $j<sizeof($viewerList); $j++) {
							if($cnt2 == 0){
								$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
								$cnt2++;
							} else {
								$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
							}
						}
						$sql2 = rtrim($sql2, ", ");
						if($cnt2 > 0){
							$result["insert_event_new_guest_FOL_$i"] = $iCal->db_db_query($sql2);
						}
					}
					
					# Add existing guests
					if (sizeof($existGuestList) != 0) {
						$fieldname3  = "EventID, UserID, Status, Access, InviteStatus";
						$sql3 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) VALUES ";
						$cnt3 = 0;
						$isLocal = false; 
						for ($k=0; $k<sizeof($existGuestList); $k++) {
							if (!in_array($existGuestList[$k], $removeGuestList) && $existGuestList[$k] != $currentUserId) {
								$pieces = explode("-",$existGuestList[$k]);
								$isLocal = true;
								if($cnt3 == 0){
									$sql3 .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$currentUserId?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
									$cnt3++;
								} else {
									$sql3 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$k]."', '".($existGuestList[$k]==$currentUserId?'A':$InviteEventUserStatus)."', '".($existGuestList[$k]==$currentUserId?'A':'R')."', '".$InviteStatus."')";
								}
							}
						}
						if ($isLocal){
							$sql3 = rtrim($sql3, ", ");
							if ($sql3 != "INSERT INTO CALENDAR_EVENT_USER ($fieldname3) ") {
								$result["insert_event_user_status_FOL"] = $iCal->db_db_query($sql3);
							}
						}
					}
					
					##### EBOOKING ##### (FOL)
					if($plugin['eBooking'] && $handleBookingRecords)
					{
						if($hiddenBookingIDs != "")
						{
							$targetOldEventID = implode(",",$arrOldEventID);
							
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($targetOldEventID)";
							$arrOldBookingID = $iCal->returnVector($sql);
							$strOldBookingID = implode(",",$arrOldBookingID);
							$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID IN ($strOldBookingID)";
							$iCal->db_db_query($sql); 
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 999 WHERE BookingID IN ($strOldBookingID)";
							$iCal->db_db_query($sql); 
		//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 999 WHERE BookingID IN ($strOldBookingID)";
		//					$iCal->db_db_query($sql); 
							
							$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
							$arrEventInfo = $iCal->returnArray($sql,2);
							
							if(sizeof($arrEventInfo)>0)
							{
								for($k=0; $k<sizeof($arrEventInfo); $k++)
								{
									list($event_id, $event_datetime) = $arrEventInfo[$k];
									$event_date = substr($event_datetime,0,10);
									$event_time = substr($event_datetime,11,strlen($event_datetime));
									
									$arrEventDate[] = $event_datetime;
									$arrMergeInfo[$event_datetime]['EventID'] = $event_id;
									$arrMergeInfo[$event_datetime]['EventDate'] = $event_date;
		
									### Get New BookingID
									$sql = "SELECT BookingID, CONCAT(Date,' ',StartTime) AS EventDate FROM INTRANET_EBOOKING_RECORD WHERE BookingID in ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
									$arrNewBooking = $iCal->returnArray($sql,2);
									if(sizeof($arrNewBooking)>0) {
										list($new_booking_id, $new_booking_date) = $arrNewBooking[0];
										$arrMergeInfo[$event_datetime]['NewBookingID'] = $new_booking_id;
										$arrAssignedBookingID[] = $new_booking_id;
									} else {
										$arrMergeInfo[$event_datetime]['NewBookingID'] = "";
									}
									
									### Get Old BookingID
									$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '".$repeatSeries[$i]["EventID"]."'";
									$arrOldBooking = $iCal->returnArray($sql,2);
									if(sizeof($arrOldBooking)>0) {
										list($old_booking_id, $old_booking_date) = $arrOldBooking[0];
										$arrMergeInfo[$event_datetime]['OldBookingID'] = $old_booking_id;
									} else {
										$arrMergeInfo[$event_datetime]['OldBookingID'] = "";
									}
								}
							}
							$arrEventDate = array_unique($arrEventDate);
						}
					}
					
					## (CONT.)Save ALL FOLLOWING events ##
					## Show in school calendar (Group Event Only) ##
					if($ShowInSchoolCalendar)
					{
						$iCalendarEventID = $repeatSeries[$i]["EventID"];
						$event_date = date("Y-m-d",$iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]));
						
						### GET EventID In School Calendar
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
						$arr_SchoolCalEventID = $iCal->returnVector($sql);
						
						if(sizeof($arr_SchoolCalEventID) > 0)
						{
							//debug("Update");
							$SchoolCalEventID = $arr_SchoolCalEventID[0];
							
							$arrUpdateCalenderEventID[] = $iCalendarEventID;
						
							### GET GroupID 
							$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\' ';
							$arrGroupID = $iCal->returnVector($sql);
							$targetGroupID = $arrGroupID[0];
							
							$sql = "UPDATE INTRANET_EVENT SET ";
							$sql .= "EventDate = '".$event_date."', ";
							$sql .= "RecordType = '2', ";
							$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
							$sql .= "EventVenue = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
							$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
							$sql .= "DateModified = NOW(), ";
							$sql .= "ModifyBy = '".$currentUserId."' ";
							$sql .= "WHERE EventID = '".$SchoolCalEventID."' ";
							$iCal->db_db_query($sql);
														
							$sql = "UPDATE INTRANET_GROUPEVENT SET GroupID = '".$targetGroupID."', DateModified = NOW() WHERE EventID = '".$SchoolCalEventID."' ";
							$db->db_db_query($sql);
						}
						else
						{
							//debug("Insert");
							$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\' ';
							$arrTargetGroupID = $db->returnVector($sql);
							$targetGroupID = $arrTargetGroupID[0]; 
							
							if($targetGroupID != "")
							{
								$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
								$arrCalendarEvent = $iCal->returnArray($sql,2);
								
								if(sizeof($arrCalendarEvent)>0)
								{
									for($i=0; $i<sizeof($arrCalendarEvent); $i++)
									{
										list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
										
										//$title = trim(stripslashes(urldecode($title)));
										//$description = trim(stripslashes(urldecode($description)));
										
										$sql = "INSERT INTO 
														INTRANET_EVENT 
														(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
												VALUES
														('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
										$iCal->db_db_query($sql);
										
										$ParentSchoolCalEventID = $db->db_insert_id();
										$CurrentSchoolCalEventID = $db->db_insert_id();
													
										$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
										$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
										$db->db_db_query($sql);
										
										$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
									
										$sql = "UPDATE INTRANET_EVENT 
												SET 
													RelatedTo = '$ParentSchoolCalEventID',
													DateModified = NOW(),
													ModifyBy = '$currentUserId'
												WHERE 
													EventID = '$CurrentSchoolCalEventID'";
										$db->db_db_query($sql);
										
										$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
										$db->db_db_query($sql);
									}
								}
							}
						}
					}
					else
					{
						//debug("Delete");
						$iCalendarEventID = $repeatSeries[$i]["EventID"];
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
						$arr_SchoolCalEventID = $iCal->returnVector($sql);
						if(sizeof($arr_SchoolCalEventID) > 0)
						{
							$SchoolCalEventID = $arr_SchoolCalEventID[0];
							$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."' ";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."' ";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
							$iCal->db_db_query($sql);
						}
					}
				}
				// [2014-11-06 Carlos]: Comment the following block of code. Why delete INTRANET_EVENT while the above for loop has update/delete INTRAENT_EVENT ?
				/*
				if(sizeof($arrUpdateCalenderEventID) > 0){
					$targetUpdateCalenderEventID = implode(",",$arrUpdateCalenderEventID);
					$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID NOT IN ($targetUpdateCalenderEventID)";
					$arrNotUpdatedSchoolEventID = $iCal->returnVector($sql);
					if(sizeof($arrNotUpdatedSchoolEventID) > 0)
					{
						$targetNotUpdatedSchoolEventID = implode(",",$arrNotUpdatedSchoolEventID);
						$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetNotUpdatedSchoolEventID)";
						$iCal->db_db_query($sql);
					}
				}
				*/
				## update school calendar (calendar view)
				if($ShowInSchoolCalendar)
				{ 
					include_once($intranet_root."/includes/libcycleperiods.php");
					$lcycleperiods = new libcycleperiods();
					$lcycleperiods->generatePreview();
					$lcycleperiods->generateProduction();
				}
				
				$sql1 = rtrim($sql1, ", ");
		//		$result["insert_event_owner_FOL"] = $iCal->db_db_query($sql1);
				
				##### EBOOOKING #####
				if($plugin['eBooking'] && $handleBookingRecords)
				{
					if(sizeof($arrEventDate)>0)
					{
						foreach($arrEventDate as $key=>$date)
						{
							$targetEventID = $arrMergeInfo[$date]['EventID'];
							$targetEventDate = $arrMergeInfo[$date]['EventDate'];
							$targetNewBookingID = $arrMergeInfo[$date]['NewBookingID'];
							$targetOldBookingID = $arrMergeInfo[$date]['OldBookingID'];
							
							if($targetNewBookingID != "") {
								$targetNewBookingIDAry = explode(',', $targetNewBookingID);
								
								include_once($PATH_WRT_ROOT."includes/libebooking.php");
								$lebooking = new libebooking();
								$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNewBookingID) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
								$arrRoomID = $iCal->returnVector($sql);
								$roomId = $arrRoomID[0];
								
		//						$bookingInfoAry = $lebooking->Get_Booking_Record_By_BookingID($targetNewBookingIDAry);
		//						$bookingDate = $bookingInfoAry[0]['Date'];
		//						$bookingStartTime = $bookingInfoAry[0]['StartTime'];
		//						$bookingEndTime = $bookingInfoAry[0]['EndTime'];
								
								$RoomNeedApproval[$targetNewBookingID] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
								$IsRoomAvailable = false;
								if ($RoomNeedApproval[$targetNewBookingID]) {
									$RoomBookingStatus = 0;
									
									$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID IN ($targetNewBookingID)";
									$iCal->db_db_query($sql);
									
		//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID IN ($targetNewBookingID)";
		//							$iCal->db_db_query($sql);
								} else {
									$RoomBookingStatus = 1;
									
									$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $targetEventDate, $ebookingStartTime, $ebookingEndTime);
									if ($IsRoomAvailable) {
										$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
										$iCal->db_db_query($sql);
									}
									
		//							$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID IN ($targetNewBookingID)";
		//							$iCal->db_db_query($sql);
								}
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD Set StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '$targetNewBookingID'";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$targetEventID' WHERE BookingID = '$targetNewBookingID'";
								$iCal->db_db_query($sql);
								
								include_once($intranet_root."/includes/libinventory.php");
								$linventory = new libinventory();
								$sql = "SELECT 
											CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
										FROM
											INVENTORY_LOCATION_BUILDING AS building 
											INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
											INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
											INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
										WHERE
											BookingID IN ($targetNewBookingID)";
								$arrLocation = $linventory->returnVector($sql);
								$location = $arrLocation[0];
								
								$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$targetEventID'";
								$iCal->db_db_query($sql);
							}
							
							if($targetOldBookingID != "" && $targetOldBookingID != $targetNewBookingID) {
								$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$targetOldBookingID'";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
		//						$iCal->db_db_query($sql);
								
								$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetOldBookingID)";
								$iCal->db_db_query($sql);
							}
							$arrAvailableBookingID = explode(",",$hiddenBookingIDs);
							$arrNotAssignedBookingID = array_diff($arrAvailableBookingID, $arrAssignedBookingID);
							
		//					if(sizeof($arrNotAssignedBookingID) > 0){
		//						$targetNotAssignedBookingID = implode(",",$arrNotAssignedBookingID);
		//						$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($targetNotAssignedBookingID)";
		//						$iCal->db_db_query($sql);
		//						
		//						$sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($targetNotAssignedBookingID)";
		//						$iCal->db_db_query($sql);
		//						
		////						$sql = "DELETE FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID IN ($targetNotAssignedBookingID)";
		////						$iCal->db_db_query($sql);
		//						
		//						$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID IN ($targetNotAssignedBookingID)";
		//						$iCal->db_db_query($sql);
		//					}
							
							if ($RoomNeedApproval[$targetNewBookingID] == false && $IsRoomAvailable) {
								$lebooking->Email_Booking_Result($targetNewBookingID);
							}
						}
					}
				}
			}
			
			if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
				for($i=0;$i<count($allEventIdAry);$i++)
				{
					$title_extra_info = $iCal->getEventTitleExtraInfo($allEventIdAry[$i],1);
					$iCal->updateEventTitleWithExtraInfo($allEventIdAry[$i], $iCal->Get_Request($title), $title_extra_info);
				}
			}
		} 
		else 	# non-repeat event OR repeat event but save only this instance
		{ 
			#echo "I'm a normal event or save as OTI"."\n";
			if ($userEventAccess=="A" || $userEventAccess=="W")
			{
				if($CalType == 2){					### delete existing target Group Events in School Calendar 
					### delete old data [start]
					$sql = "SELECT relation.SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation INNER JOIN CALENDAR_EVENT_ENTRY as entry on (relation.CalendarEventID = entry.EventID) WHERE entry.EventID = '$eventID'";
					$arr_SchoolCalEventID = $iCal->returnVector($sql);
					if(sizeof($arr_SchoolCalEventID) > 0)
					{
						$SchoolCalEventID = implode(",",$arr_SchoolCalEventID);
						$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$SchoolCalEventID.")";
						$iCal->db_db_query($sql);
										
						$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$SchoolCalEventID.")";
						$iCal->db_db_query($sql);
										
						$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN (".$SchoolCalEventID.")";
						$iCal->db_db_query($sql);
					}
					### delete old data [end]
				}
				
				# Update main detail
				$sql  = "UPDATE CALENDAR_EVENT_ENTRY SET ";
				
				# Edit recurrence event - only this instance - change RepeatID to NULL
				if (isset($isRepeatEvent) && $saveRepeatEvent=="OTI") {
					#echo "Save as OTI"."\n";
					$sql2 = "select * from CALENDAR_EVENT_ENTRY where EventId = '$eventID'";
					$rt = $iCal->returnArray($sql2);
					$rString = "RECURRENCE-ID;TZID=Asia/Hong_Kong:".date("Yms\THis",strtotime($rt[0]["EventDate"]));
					if (!strstr($rt[0]["ExtraIcalInfo"],"RECURRENCE-ID")){
						$original = $rt[0]["ExtraIcalInfo"];
						$rString = str_replace($string,$rString,$original);
						$sql .= "ExtraIcalInfo = '$rString', ";
					}
					
					$sql .= "RepeatID = NULL, ";
				}
		
				$sql .= "EventDate = '$eventDateTime', ";
				$sql .= "ModifiedDate = NOW(), ";
				$sql .= "Duration = '$duration', ";
				$sql .= "IsImportant = '$isImportant', ";
				$sql .= "IsAllDay = '$isAllDay', ";
				$sql .= "Access = '$access', ";
				$sql .= "Title = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($title))."', ";
				$sql .= "Description = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description))."', ";
				$sql .= "Location = '".$iCal->Get_Safe_Sql_Query($iCal->Get_Request($location))."', ";
				$sql .= "Url = '$url', ";
				$sql .= "CalID = '$calID' ";
				$sql .= "WHERE EventID = '$eventID'";
				$result['update_event_entry'] = $iCal->db_db_query($sql);
				
				// update personal note for this event and user
				$result['update_event_personal_note'] = $iCal->Insert_Personal_Note($eventID, $iCal->Get_Request($PersonalNote), $CalType, '');
				
				# Normal event -> recurrence event
				if (isset($repeatSelect) && $repeatSelect != "NOT" && !isset($isRepeatEvent)) 
				{
					#echo "Normal event -> recurrence event"."\n";
					# convert repeat end date to datetime format
					$repeatEndDatePieces = explode("-", $repeatEnd);
					$repeatEndTime = mktime(0,0,0,$repeatEndDatePieces[1],$repeatEndDatePieces[2],$repeatEndDatePieces[0]);
					$repeatEndDate = date("Y-m-d H:i:s", $repeatEndTime);
					
					# save the repeat pattern for future reference
					$fieldname  = "RepeatType, EndDate, Frequency, Detail";
					$detail = "";
					$frequency = 0;
					switch ($repeatSelect) {
						case "DAILY":
							$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatDailyDay', NULL";
							$frequency = $repeatDailyDay;
							break;
						case "WEEKDAY":
						case "MONWEDFRI":
						case "TUESTHUR":
							$fieldvalue = "'$repeatSelect', '$repeatEndDate', NULL, NULL";
							break;
						case "WEEKLY":
							$detail = (isset($Sun)?"1":"0").(isset($Mon)?"1":"0").(isset($Tue)?"1":"0").(isset($Wed)?"1":"0");
							$detail .= (isset($Thu)?"1":"0").(isset($Fri)?"1":"0").(isset($Sat)?"1":"0");
							$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatWeeklyRange', '$detail'";
							$frequency = $repeatWeeklyRange;
							break;
						case "MONTHLY":
							if ($monthlyRepeatBy == "dayOfMonth")
								$detail = "day-".$datePieces[2];
							else {
								$eventDateWeekday = date("w", $eventTimeStamp);
								$nth = 0;
								for($i=1; $i<6; $i++) {
									$nthweekday = $iCal->NWeekdayOfMonth($i,$eventDateWeekday,$datePieces[1],$datePieces[0],"d");
									if ((int)$datePieces[2] == $nthweekday) {
										$nth = $i;
										break;
									}
								}
								$detail = "$nth-$eventDateWeekday";
							}
							$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatMonthlyRange', '$detail'";
							$frequency = $repeatMonthlyRange;
							break;
						case "YEARLY":
							$fieldvalue = "'$repeatSelect', '$repeatEndDate', '$repeatYearlyRange', NULL";
							$frequency = $repeatYearlyRange;
							break;
					}
					$sql = "INSERT INTO CALENDAR_EVENT_ENTRY_REPEAT ($fieldname) VALUES ($fieldvalue)";
					$result['update_event_repeat_info'] = $iCal->db_db_query($sql);
					$repeatID = $iCal->db_insert_id();
					
					#Get the uid of the event to be removed
					$sql = "select UID from CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
					$oldUID = $iCal->returnArray($sql);
					
					# Remove the existing single event
					$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
					$iCal->db_db_query($sql);
					
					# Remove the existing single event personal Note
					$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
					$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '$eventID' "."$eclassConstraint";
					$iCal->db_db_query($sql);
								
					# Insert the recurrence event series
					$detailArray = array($repeatID, $repeatSelect, $repeatEndDate, $frequency, $detail, $currentUserId,
									$eventDateTime, $duration, $isImportant, $isAllDay, $access, $iCal->Get_Safe_Sql_Query($iCal->Get_Request($title)), 
									$iCal->Get_Safe_Sql_Query($iCal->Get_Request($description)), $iCal->Get_Safe_Sql_Query($iCal->Get_Request($location)), $url, $calID);
					
					if (!isset($monthlyRepeatBy))
						$monthlyRepeatBy = "";
							
						
					# Main function for inserting recurrence events
					//$isIgnoreModifiedRecord = 1;		// newly added on 27 May 08 to control inserting a series of new record with ignoring the original modified record
					$result['insert_repeat_event_entry'] = $iCal->insertRepeatEventSeries($detailArray, $monthlyRepeatBy, 1, $oldUID[0]["UID"]);
					
					# insert current personal note into to new event series
					if(!in_array(false, $result)){
						$sql = "INSERT INTO CALENDAR_EVENT_PERSONAL_NOTE (EventID, UserID, PersonalNote, CalType, CreateDate) 
								SELECT EventID, '".$currentUserId."', '".($iCal->Get_Request($PersonalNote))."', '$CalType', NOW()
								FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '".$repeatID."' ";
						$result['insert_repeat_event_personal_note'] = $iCal->db_db_query($sql);
					}
				}
				
				##### EBOOKING ##### (OTI)
				if($plugin['eBooking'] && $handleBookingRecords)
				{
					if($hiddenBookingIDs != "")
					{
						if (isset($isRepeatEvent))
						{
							## Repeat Event
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '$eventID'";
							$arrOldBookingID = $iCal->returnVector($sql);
							$strOldBookingID = $arrOldBookingID[0];
							
							$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0, StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."' WHERE BookingID IN ($strOldBookingID)";
							$iCal->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = '$strOldBookingID'";
							$iCal->db_db_query($sql);
							
		//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = $strOldBookingID";
		//					$iCal->db_db_query($sql);
							
							//2012-1129-1604-13066
		//					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($strOldBookingID)";
		//					$iCal->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$strOldBookingID'";
							$iCal->db_db_query($sql);
							
							
							$sql = "SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
							$arrEventDate = $iCal->returnVector($sql);
							$event_date = substr($arrEventDate[0],0,10);
							$event_time = substr($arrEventDate[0],11,strlen($arrEventDate[0]));
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($hiddenBookingIDs) AND Date = '$event_date' AND StartTime = '$event_time'";
							$arrNewBookingID = $iCal->returnVector($sql);
							
							include_once($intranet_root."/includes/libebooking.php");
							$lebooking = new libebooking();
							$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
							$arrRoomID = $iCal->returnVector($sql);
							$roomId = $arrRoomID[0];
							
							$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
							$IsRoomAvailable = false;
							if ($RoomNeedApproval[$hiddenBookingIDs]) {
								$RoomBookingStatus = 0;
								
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID = '".$arrNewBookingID[0]."' ";
								$iCal->db_db_query($sql);
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID = ".$arrNewBookingID[0];
		//						$iCal->db_db_query($sql);
							} else {
								$RoomBookingStatus = 1;
								
								$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $ebookingEventDate, $ebookingStartTime, $ebookingEndTime);
								if ($IsRoomAvailable) {
									$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID = '".$arrNewBookingID[0]."'";
									$iCal->db_db_query($sql);
								}
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID = ".$arrNewBookingID[0];
		//						$iCal->db_db_query($sql);
							}
							
							$sql = "UPDATE INTRANET_EBOOKING_RECORD SET Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID = '".$arrNewBookingID[0]."'";
							$iCal->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$eventID' WHERE BookingID = '".$arrNewBookingID[0]."'";
							$iCal->db_db_query($sql);
							
							include_once($intranet_root."/includes/libinventory.php");
							$linventory = new libinventory();
							$sql = "SELECT 
										CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
									FROM
										INVENTORY_LOCATION_BUILDING AS building 
										INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
										INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
										INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
									WHERE
										BookingID IN (".$arrNewBookingID[0].")";
							$arrLocation = $linventory->returnVector($sql);
							$location = $arrLocation[0];
							
							$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$eventID'";
							$iCal->db_db_query($sql);
							
							## Remove Temp Record
							$arrAssignedBookingID[] = $arrNewBookingID[0];
							$arrHiddenBookingIDs = explode(",",$hiddenBookingIDs);
							$arrNotAssignedBookingID = array_diff($arrHiddenBookingIDs, $arrAssignedBookingID);
							
		//					if(sizeof($arrNotAssignedBookingID) > 0)
		//					{
		//						$strNotAssignedBookingID = implode(",",$arrNotAssignedBookingID);
		//						$sql = "DELETE FROM INTRANET_EBOOKING_RECORD WHERE BookingID IN ($strNotAssignedBookingID)";
		//						$iCal->db_db_query($sql);
		//						
		//						$sql = "DELETE FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($strNotAssignedBookingID)";
		//						$iCal->db_db_query($sql);
		//						
		////						$sql = "DELETE FROM INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS WHERE BookingID IN ($strNotAssignedBookingID)";
		////						$iCal->db_db_query($sql);
		//						
		//						$sql = "DELETE FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE BookingID IN ($strNotAssignedBookingID)";
		//						$iCal->db_db_query($sql);
		//					}
							
							if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable)
							{
								$lebooking->Email_Booking_Result($arrNewBookingID[0]);
							}
						}
						else
						{
							## Single Event					
							$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID = '$eventID'";
							$arrOldBookingID = $iCal->returnVector($sql);
							$strOldBookingID = $arrOldBookingID[0];
							
							
							$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0, StartTime = '".$ebookingStartTime."', EndTime = '".$ebookingEndTime."', Remark = '".$eBookingRecordRemarksSql."' WHERE BookingID IN ($strOldBookingID)";
							$iCal->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = '$strOldBookingID'";
							$iCal->db_db_query($sql);
							
		//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID = $strOldBookingID";
		//					$iCal->db_db_query($sql);
							
							//2012-1129-1604-13066
		//					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($strOldBookingID)";
		//					$iCal->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = NULL WHERE BookingID = '$strOldBookingID'";
							$iCal->db_db_query($sql);
							
							include_once($intranet_root."/includes/libebooking.php");
							$lebooking = new libebooking();
							$sql = "SELECT FacilityID FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID IN ($hiddenBookingIDs) AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM;
							$arrRoomID = $iCal->returnVector($sql);
							$roomId = $arrRoomID[0];
							
							$RoomNeedApproval[$hiddenBookingIDs] = $lebooking->Check_If_Booking_Need_Approval($currentUserId, '', $roomId);
							$IsRoomAvailable = false;
							if ($RoomNeedApproval[$hiddenBookingIDs]) {
								$RoomBookingStatus = 0;
								
								$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus' WHERE BookingID = '$hiddenBookingIDs'";
								$iCal->db_db_query($sql);
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus WHERE BookingID = $hiddenBookingIDs";
		//						$iCal->db_db_query($sql);
							} else {
								$RoomBookingStatus = 1;
								
								$IsRoomAvailable = $lebooking->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM, $roomId, $ebookingEventDate, $ebookingStartTime, $ebookingEndTime);
								if ($IsRoomAvailable) {
									$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = '$RoomBookingStatus', ProcessDate = NOW() WHERE BookingID = '$hiddenBookingIDs'";
									$iCal->db_db_query($sql);
								}
								
		//						$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = $RoomBookingStatus, ProcessDate = NOW() WHERE BookingID = $hiddenBookingIDs";
		//						$iCal->db_db_query($sql);
							}
							
							$sql = "UPDATE INTRANET_EBOOKING_CALENDAR_EVENT_RELATION SET EventID = '$eventID' WHERE BookingID = '$hiddenBookingIDs'";
							$iCal->db_db_query($sql);
							
							include_once($intranet_root."/includes/libinventory.php");
							$linventory = new libinventory();
							$sql = "SELECT 
										CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
									FROM
										INVENTORY_LOCATION_BUILDING AS building 
										INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
										INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
										INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS AS room_booking ON (room.LocationID = room_booking.FacilityID AND room_booking.FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM.")
									WHERE
										BookingID IN ($hiddenBookingIDs)";
							$arrLocation = $linventory->returnVector($sql);
							$location = $arrLocation[0];
							
							$sql = "UPDATE CALENDAR_EVENT_ENTRY SET Location = '$location' WHERE EventID = '$eventID'";
							$iCal->db_db_query($sql);
							
							if ($RoomNeedApproval[$hiddenBookingIDs] == false && $IsRoomAvailable)
							{
								$lebooking->Email_Booking_Result($$hiddenBookingIDs);
							}
						}
					}
				}
				
				## (CONT.)Save ALL FOLLOWING events ##
				## Show in school calendar (Group Event Only) ##
				if($ShowInSchoolCalendar)
				{
					$sql = 'SELECT GroupID FROM INTRANET_GROUP WHERE CalID = \''.$calID.'\' ';
					$arrTargetGroupID = $db->returnVector($sql);
					$targetGroupID = $arrTargetGroupID[0];
					
					if (isset($isRepeatEvent)) // Original : Repeat event, but save only this event 
					{
						$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
						$arrCalendarEvent = $iCal->returnArray($sql,2);
						
						if(sizeof($arrCalendarEvent)>0)
						{
							for($i=0; $i<sizeof($arrCalendarEvent); $i++)
							{
								list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
								
								//$title = trim(stripslashes(urldecode($title)));
								//$description = trim(stripslashes(urldecode($description)));
								
								$sql = "INSERT INTO 
												INTRANET_EVENT 
												(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
										VALUES
												('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
								$iCal->db_db_query($sql);
								
								$ParentSchoolCalEventID = $db->db_insert_id();
								$CurrentSchoolCalEventID = $db->db_insert_id();
											
								$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
								$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
								$db->db_db_query($sql);
								
								$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
							
								$sql = "UPDATE INTRANET_EVENT 
										SET 
											RelatedTo = '$ParentSchoolCalEventID',
											DateModified = NOW(),
											ModifyBy = '$currentUserId'
										WHERE 
											EventID = '$CurrentSchoolCalEventID'";
								$db->db_db_query($sql);
								
								$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
								$db->db_db_query($sql);
							}
						}
					}
					else
					{
						if($targetGroupID != "")
						{
							if($repeatID != "")		// Single to repeat
							{
								### SINGLE Event (Have repeat selection)
								$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = '$repeatID'";
								$arrCalendarEvent = $iCal->returnArray($sql);
							}	
							else					// Single to single
							{
								### SINGLE Event (No repeat selection)
								$sql = "SELECT EventID, EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID = '$eventID'";
								$arrCalendarEvent = $iCal->returnArray($sql,2);
							}
									
							if(sizeof($arrCalendarEvent)>0)
							{
								for($i=0; $i<sizeof($arrCalendarEvent); $i++)
								{
									list($calendar_event_id, $calendar_event_date) = $arrCalendarEvent[$i];
									
									//$title = trim(stripslashes(urldecode($title)));
									//$description = trim(stripslashes(urldecode($description)));
									
									$sql = "INSERT INTO 
													INTRANET_EVENT 
													(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy)
											VALUES
													('$calendar_event_date',2,'".$db->Get_Safe_Sql_Query($iCal->Get_Request($title))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($location))."','','".$db->Get_Safe_Sql_Query($iCal->Get_Request($description))."',0,1,NOW(),'$currentUserId',NOW(),'$currentUserId')";
									$iCal->db_db_query($sql);
									
									$ParentSchoolCalEventID = $db->db_insert_id();
									$CurrentSchoolCalEventID = $db->db_insert_id();
												
									$value = "('$targetGroupID','$CurrentSchoolCalEventID',NOW(),NOW())";
									$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
									$db->db_db_query($sql);
									
									$arrSchoolCalendarInfo[$TargetEventDate][] = $CurrentSchoolCalEventID; 
								
									$sql = "UPDATE INTRANET_EVENT 
											SET 
												RelatedTo = '$ParentSchoolCalEventID',
												DateModified = NOW(),
												ModifyBy = '$currentUserId'
											WHERE 
												EventID = '$CurrentSchoolCalEventID'";
									$db->db_db_query($sql);
									
									$sql = "INSERT INTO CALENDAR_EVENT_SCHOOL_EVENT_RELATION (CalendarEventID, SchoolEventID) VALUES ('$calendar_event_id','$CurrentSchoolCalEventID')";
									$db->db_db_query($sql);
								}
							}
						}
					}
				}
				else
				{
					//debug("Delete");
					if (isset($isRepeatEvent))
					{
						$iCalendarEventID = $eventID;
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
						$arr_SchoolCalEventID = $iCal->returnVector($sql);
						if(sizeof($arr_SchoolCalEventID) > 0)
						{
							$SchoolCalEventID = $arr_SchoolCalEventID[0];
							$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."'";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."'";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
							$iCal->db_db_query($sql);
						}	
					}
					else
					{
						$iCalendarEventID = $eventID;
						$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
						$arr_SchoolCalEventID = $iCal->returnVector($sql);
						if(sizeof($arr_SchoolCalEventID) > 0)
						{
							$SchoolCalEventID = $arr_SchoolCalEventID[0];
							$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = '".$SchoolCalEventID."'";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = '".$SchoolCalEventID."'";
							$iCal->db_db_query($sql);
							
							$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID = '$iCalendarEventID'";
							$iCal->db_db_query($sql);
						}
					}
					
				}
				## update school calendar (calendar view)
				if($ShowInSchoolCalendar)
				{ 
					include_once($intranet_root."/includes/libcycleperiods.php");
					$lcycleperiods = new libcycleperiods();
					$lcycleperiods->generatePreview();
					$lcycleperiods->generateProduction();
				}
			}
			else 
			{
				$result['update_event_personal_note'] = $iCal->Insert_Personal_Note($eventID, $iCal->Get_Request($PersonalNote), $CalType, $otherSchoolCode);
			}
			
			#echo "About to insert reminders.....<br />";
			# Reminder
			if (isset($reminderID) || isset($reminderType)) 
			{
				# Normal event -> recurrence event
				if (isset($repeatSelect) && $userEventAccess!="R" && $repeatSelect != "NOT" && !isset($isRepeatEvent))
				{
					# Remove the old reminders
					$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = '$eventID'";
					$iCal->db_db_query($sql);
					
					# Get all the events in the recurrence series
					$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
					if($CalType == 2 || $CalType == 3)//modified on 20 July 2009
					{
						$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore, CalType";
					}else
					{
						$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
					}
					$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ";
					$cnt = 0;
					for ($i=0; $i<sizeof($repeatSeries); $i++) {
						$repeatEventTimeStamp = $iCal->sqlDatetimeToTimestamp($repeatSeries[$i]["EventDate"]);//modified on 17 July 2009
						for ($j=0; $j<sizeof($reminderType); $j++) {
							$reminderDate[$j] = $repeatEventTimeStamp - ((int)$reminderTime[$j])*60;//modified on 17 July 2009
							$reminderDate[$j] = date("Y-m-d H:i:s", $reminderDate[$j]);
							if($cnt == 0){
								$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderDate[$j]."')";
								$cnt++;
							} else {
								$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$currentUserId."', '".$reminderType[$j]."', '".$reminderDate[$j]."', '".$reminderDate[$j]."')";
							}
						}
						
						
						# reminder for newly add guests
						$curReminderDate = $repeatEventTimeStamp - 10*60;//modified on 17 July 2009
						$curReminderDate = date("Y-m-d H:i:s", $curReminderDate);
						
						if (isset($viewerList) && sizeof($viewerList) != 0) {
							for ($k=0; $k<sizeof($viewerList); $k++) {
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$k]."', 'P', '$curReminderDate', 10)";
								}
							}
						}
						
						if (isset($viewerClassList) && sizeof($viewerClassList) != 0) {
							for ($k=0; $k<sizeof($viewerClassList); $k++) {
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerClassList[$k]."', 'P', '$curReminderDate', 10)";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerClassList[$k]."', 'P', '$curReminderDate', 10)";
								}
							}
						}
						
						if (isset($viewerCourseList) && sizeof($viewerCourseList) != 0) {
							for ($k=0; $k<sizeof($viewerCourseList); $k++) {
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerCourseList[$k]."', 'P', '$curReminderDate', 10)";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerCourseList[$k]."', 'P', '$curReminderDate', 10)";
								}
							}
						}
						
						if (isset($viewerGroupList) && sizeof($viewerGroupList) != 0) {
							for ($k=0; $k<sizeof($viewerGroupList); $k++) {
								if($cnt == 0){
									$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerGroupList[$k]."', 'P', '$curReminderDate', 10)";
									$cnt++;
								} else {
									$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerGroupList[$k]."', 'P', '$curReminderDate', 10)";
								}
							}
						}
						
						# reminder for existing guests
						if (sizeof($existGuestList) != 0) {
							for ($l=0; $l<sizeof($existGuestList); $l++) {
								if (!in_array($existGuestList[$l], $removeGuestList) && $existGuestList[$l] != $currentUserId) {
									if($cnt == 0){
										$sql .= " ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
										$cnt++;
									} else {
										$sql .= ", ('".$repeatSeries[$i]["EventID"]."', '".$existGuestList[$l]."', 'P', '$curReminderDate', 10)";
									}
								}
							}
						}
					}
					$sql = rtrim($sql, ", ");
					$result['insert_event_reminder'] = $iCal->db_db_query($sql);	
				} 
				else 		# $reminderType > $reminderID means new reminder is added
				{
					if (sizeof($reminderType) >= sizeof($reminderID)) {
						for ($i=0; $i<sizeof($reminderType); $i++) {
								if(trim($eventTimeStamp) == "")
								{
									$EventDateResult=$iCal->returnArray("SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventID'");
									$reminderDate[$i] = strtotime($EventDateResult[0]['EventDate']) - ((int)$reminderTime[$i])*60;
									$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
								}else
								{
									$reminderDate[$i] = $eventTimeStamp - ((int)$reminderTime[$i])*60;//modified on 16 July 2009
									$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
								}
							if ($i < sizeof($reminderID)) {
								# Update existing reminder
								$sql  = "UPDATE CALENDAR_REMINDER SET ";
								$sql .= "ReminderType = '".$reminderType[$i]."', ";
								$sql .= "ReminderDate = '".$reminderDate[$i]."', ";
								$sql .= "LastSent = NULL, ";
								$sql .= "ReminderBefore = '".$reminderTime[$i]."' ";
								$sql .= "WHERE ReminderID = '".$reminderID[$i]."' ";
								$result['update_event_reminder_'.$i] = $iCal->db_db_query($sql);
							} else {
								# insert new reminder
								$fieldname  = "EventID, UserID, ReminderType, ReminderDate, ReminderBefore";
								$fieldvalue = "'$eventID', '".$currentUserId."', '".$reminderType[$i]."', '".$reminderDate[$i]."', '".$reminderTime[$i]."' ";
								$sql = "INSERT INTO CALENDAR_REMINDER ($fieldname) VALUES ($fieldvalue)";
								$result['update_event_reminder_'.$i] = $iCal->db_db_query($sql);
							}
						}
					} else {	# $reminderType < $reminderID means some reminder have to be updated and some have to be removed
						for ($i=0; $i<sizeof($reminderID); $i++) {
							if ($i < sizeof($reminderType)) {
								if(trim($eventTimeStamp) == "")
								{
									$EventDateResult=$iCal->returnArray("SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventID'");
									$reminderDate[$i] = strtotime($EventDateResult[0]['EventDate']) - ((int)$reminderTime[$i])*60;
									$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
								}else
								{
									$reminderDate[$i] = $eventTimeStamp - ((int)$reminderTime[$i])*60;//modified on 16 July 2009
									$reminderDate[$i] = date("Y-m-d H:i:s", $reminderDate[$i]);
								}
								$sql  = "UPDATE CALENDAR_REMINDER SET ";
								$sql .= "ReminderType = '".$reminderType[$i]."', ";
								$sql .= "ReminderDate = '".$reminderDate[$i]."', ";
								$sql .= "LastSent = NULL, ";
								$sql .= "ReminderBefore = '".$reminderTime[$i]."' ";
								$sql .= "WHERE ReminderID = '".$reminderID[$i]."' ";
								$result['update_event_reminder_'.$i] = $iCal->db_db_query($sql);
							} else {
								$result['delete_event_reminder_'.$i] = $sql = "DELETE FROM CALENDAR_REMINDER WHERE ReminderID = '".$reminderID[$i]."' ";
								$iCal->db_db_query($sql);
							}
						}
					}
				}
			}
			
			# Update reminders of other users
			$existOtherReminder = $iCal->returnReminderDetail($eventID, true);
			if ($userEventAccess!="R" && $repeatSelect != "NOT" && !isset($isRepeatEvent)) {
				
			} else {
				for($i=0; $i<sizeof($existOtherReminder); $i++) {
					if(trim($eventTimeStamp) == "")
					{
						$EventDateResult=$iCal->returnArray("SELECT EventDate FROM CALENDAR_EVENT_ENTRY WHERE EventID='$eventID'");
						$newReminderTs = strtotime($EventDateResult[0]['EventDate']) - $existOtherReminder[$i]["ReminderBefore"]*60;
						$newReminderDateTime = date("Y-m-d H:i:s", $newReminderTs);
					}else
					{
						$newReminderTs = $eventTimeStamp - $existOtherReminder[$i]["ReminderBefore"]*60;//modified on 16 July 2009
						$newReminderDateTime = date("Y-m-d H:i:s", $newReminderTs);
					}
					$sql  = "UPDATE CALENDAR_REMINDER SET ";
					$sql .= "LastSent = NULL, ";
					$sql .= "ReminderDate = '$newReminderDateTime' ";
					$sql .= "WHERE ReminderID = '".$existOtherReminder[$i]["ReminderID"]."' ";
					$result['update_event_guest_reminder_'.$i] = $iCal->db_db_query($sql);
				}
			}
			
			if ($userEventAccess=="A" || $userEventAccess=="W") {
				# Remove guests
				$temp = array();
				for ($i=0; $i<sizeof($removeGuestList); $i++) {
					if ($removeGuestList[$i] != -1)
						$temp[] = $removeGuestList[$i];
				}
				$removeGuestList = $temp;
				
				if (sizeof($removeGuestList)>0) {
					for ($i=0; $i<sizeof($removeGuestList); $i++) {
						$pieces = explode("-",$removeGuestList[$i]);
						$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = '$eventID' AND UserID = '".$removeGuestList[$i]."' ";//." AND FromSchoolCode is null AND ToSchoolCode is null";
						$result['remove_event_guest_'.$i] = $iCal->db_db_query($sql);
						$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = '$eventID' AND UserID = '".$removeGuestList[$i]."' ";
						$result['remove_event_guest_reminder_'.$i] = $iCal->db_db_query($sql);
						$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
						$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '$eventID' AND UserID = '".$removeGuestList[$i]."' ".$eclassConstraint;//." AND CalType <> 3 AND CalType <> 4";
						$result['remove_event_guest_personal_note_'.$i] = $iCal->db_db_query($sql);
					}
				}
			}
			
			# Normal event -> recurrence event
			if (isset($repeatSelect) && $userEventAccess!="R" && $repeatSelect != "NOT" && !isset($isRepeatEvent))
			{
				$existGuestList = $iCal->returnViewerID($eventID);
				for ($i=0; $i<sizeof($existGuestList); $i++) {
					if($existGuestList[$i]["UserID"] != $insertUserID){
							$viewerList[] = $existGuestList[$i]["UserID"];
					}
				}
				
				$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
				$fieldname1  = "EventID, UserID, Status, Access, InviteStatus";
				$sql1 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname1) VALUES ";
				$cnt1 = 0;
				for ($i=0; $i<sizeof($repeatSeries); $i++) {
					$allEventIdAry[] = $repeatSeries[$i]["EventID"];
					// insert the owner of the event with "A"(Full) access right
					if($cnt1 == 0){
						$sql1 .= " ('".$repeatSeries[$i]["EventID"]."', '".$insertUserID."', 'A', 'A', '$InviteStatus')";
						$cnt1++;
					} else {
						$sql1 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$insertUserID."', 'A', 'A', '$InviteStatus')";
					}
					
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						$fieldname2  = "EventID, UserID, Status, Access, InviteStatus";
						$sql2 = "INSERT INTO CALENDAR_EVENT_USER ($fieldname2) VALUES ";
						$cnt2 = 0;
						for ($j=0; $j<sizeof($viewerList); $j++) {
							if($cnt2 == 0){
								$sql2 .= " ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."', '$InviteStatus')";
								$cnt2++;
							} else {
								$sql2 .= ", ('".$repeatSeries[$i]["EventID"]."', '".$viewerList[$j]."', '".($viewerList[$j]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$j]==$currentUserId?'A':'R')."', '$InviteStatus')";
							}
						}
						$result['insert_event_guest_'.$i] = $iCal->db_db_query($sql2);
					}
				}
		//		$result['insert_event_owner'] = $iCal->db_db_query($sql1);
				
				# added on 4 June 08
				# remove existing personal note for thos guests
				$eclassConstraint =" AND (CalType <> 1 AND CalType <= 3 OR CalType is null) ";
				$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = '$eventID' AND UserID = '".$currentUserId."' ".$eclassConstraint;//." And FromSchoolCode is null And CalType <> 3 And CalType <> 4";
				$iCal->db_db_query($sql);
				# Remove the existing guest in the old single event(normal event)
				$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = '$eventID' ";//." AND FromSchoolCode is null";
				$iCal->db_db_query($sql);
				
				if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
					for($i=0;$i<count($allEventIdAry);$i++)
					{
						$title_extra_info = $iCal->getEventTitleExtraInfo($allEventIdAry[$i],1);
						$iCal->updateEventTitleWithExtraInfo($allEventIdAry[$i], $iCal->Get_Request($title), $title_extra_info);
					}
				}
			} 
			else 
			{
				$allEventIdAry[] = $eventID;
				if ($userEventAccess!="R") {
					# Add new guests
					if (isset($viewerList) && sizeof($viewerList) != 0) {
						$fieldname  = "EventID, UserID, Status, Access, InviteStatus";
						$sql = "INSERT INTO CALENDAR_EVENT_USER ($fieldname) VALUES ";
						$cnt = 0;
						for ($i=0; $i<sizeof($viewerList); $i++) {
							if($cnt == 0){
								$sql .= " ('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$currentUserId?'A':'R')."','".$InviteStatus."')";
								$cnt++;
							} else {
								$sql .= ", ('".$eventID."', '".$viewerList[$i]."', '".($viewerList[$i]==$currentUserId?'A':$InviteEventUserStatus)."', '".($viewerList[$i]==$currentUserId?'A':'R')."','".$InviteStatus."')";
							}
						}
						$result['insert_new_event_guest'] = $iCal->db_db_query($sql);
					}
				}
				
				# Update user own status
				$sql  = "UPDATE CALENDAR_EVENT_USER SET ";
				if($userEventAccess == 'A'){	// User with All Access right
					$sql .= "InviteStatus = '$InviteStatus' ";
					$sql .= ($EventType != "1") ? "" : ", Status = '$response' ";
				} else {						// User with only read right
					$sql .= "Status = '$response' ";	
				}
				$sql .= "WHERE EventID = '$eventID' AND UserID = '".$currentUserId."' ";//." And FromSchoolCode is null AND ToSchoolCode is null";
				$result['update_event_user_status'] = $iCal->db_db_query($sql);
			}
			
			if($sys_custom['iCalendar']['EventTitleWithParticipantNames']){
				$title_extra_info = $iCal->getEventTitleExtraInfo($eventID,1);
				$iCal->updateEventTitleWithExtraInfo($eventID, $iCal->Get_Request($title), $title_extra_info);
			}
		}
		
		if(!in_array(false, $result)){
			$Msg = $Lang['ReturnMsg']['EventUpdateSuccess'];
		} else {
			$Msg = $Lang['ReturnMsg']['EventUpdateUnSuccess'];
		}
		
		if($plugin['eClassTeacherApp'] && $submitMode == 'submitAndSendPushMsg' && count($allEventIdAry)>0){
			$iCal->sendPushMessageNotifyParticipants($allEventIdAry[0]);
		}
		
		return !in_array(false, $result);	
	}
	
	function removeEventUpdate($currentUserId,$params,$handleBookingRecords=false)
	{
		global $intranet_root, $PATH_WRT_ROOT, $plugin, $sys_custom, $Lang, $intranet_session_language;
		include_once($intranet_root."/lang/lang.$intranet_session_language.php");
		include_once($intranet_root."/lang/ical_lang.$intranet_session_language.php");
		
		extract($params);
		
		$iCal = $this;
		
		$result = array();

		# User access right for this event
		$userEventAccess = $iCal->returnUserEventAccess($currentUserId,$eventID);
		
		if ($userEventAccess != "A") {
			header("Location: index.php");
		}
		
		if (isset($isRepeatEvent) && $deleteRepeatEvent!="OTI") {
			$repeatID = $iCal->returnRepeatID($eventID);
			# get the whole repeat series
			$repeatSeries = $iCal->returnRepeatEventSeries($repeatID);
			for ($i=0; $i<sizeof($repeatSeries); $i++) {
				$repeatSeriesSql[$i] = $repeatSeries[$i]["EventID"];
			}
			$repeatSeriesSql = implode(",", $repeatSeriesSql);
			
			if ($deleteRepeatEvent=="ALL") {		### Delete All events
				# Check any record in School Calendar
				$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN ($repeatSeriesSql)";
				$arrSchoolEventID = $iCal->returnVector($sql);
				# delete related event record in school calendar
				if(sizeof($arrSchoolEventID) >0){
					$targetSchoolEventID = implode(",",$arrSchoolEventID);
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$targetSchoolEventID.")";
					$iCal->db_db_query($sql);
					$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$targetSchoolEventID.")";
					$iCal->db_db_query($sql);
					$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN (".$targetSchoolEventID.")";
					$iCal->db_db_query($sql);
				}
			
				# delete main detail
				$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = $repeatID";
				$result['delete_event_entry'] = $iCal->db_db_query($sql);
				
				# delete repeat pattern
				$sql = "DELETE FROM CALENDAR_EVENT_ENTRY_REPEAT WHERE RepeatID = $repeatID";
				$result['delete_repeat_event'] = $iCal->db_db_query($sql);
				
				# delete reminders
				$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID IN ($repeatSeriesSql)";
				$result['delete_event_reminder'] = $iCal->db_db_query($sql);
				
				# delete guests
				$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID IN ($repeatSeriesSql)";
				$result['delete_event_guest'] = $iCal->db_db_query($sql);
				
				# delete guests
				$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID IN ($repeatSeriesSql)";
				$result['delete_event_Personal_NOTE'] = $iCal->db_db_query($sql);
				
				##### EBOOKING #####
				if($plugin['eBooking'] && $handleBookingRecords)
				{
					$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($repeatSeriesSql)";
					$arrBookingID = $iCal->returnVector($sql);
					
					if(sizeof($arrBookingID) > 0)
					{
						$targetBookingID = implode(",",$arrBookingID);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
						$iCal->db_db_query($sql);
							
						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
						$iCal->db_db_query($sql);
						
		//				$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
		//				$iCal->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
						$iCal->db_db_query($sql);
					}
				}
			} else if ($deleteRepeatEvent=="FOL") { 	### Delete All of the following events
				### Get the cut off date
				$cutOffDate = $oldEventDate." 00:00:00";
				
				##### EBOOKING ######
				if($plugin['eBooking'] && $handleBookingRecords)
				{
					$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE RepeatID = $repeatID AND EventDate >= '$cutOffDate'";
					$arrTargetEventID = $iCal->returnVector($sql);
					
					if(sizeof($arrTargetEventID) > 0)
					{
						$strTargetEventID = implode(",",$arrTargetEventID);
						
						$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($strTargetEventID)";
						$arrBookingID = $iCal->returnVector($sql);
						
						if(sizeof($arrBookingID) > 0)
						{
							$targetBookingID = implode(",",$arrBookingID);
							
							$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
							$iCal->db_db_query($sql);
								
							$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
							$iCal->db_db_query($sql);
							
		//					$sql = "UPDATE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
		//					$iCal->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
							$iCal->db_db_query($sql);
						}
					}
				}
				
				# Check any record in School Calendar
				$sql = "SELECT
							relation.SchoolEventID 
						FROM 
							CALENDAR_EVENT_SCHOOL_EVENT_RELATION as relation 
						WHERE 
							relation.CalendarEventID IN (
															SELECT 
																cal.EventID 
															FROM 
																CALENDAR_EVENT_ENTRY as cal 
															WHERE cal.RepeatID = $repeatID AND cal.EventDate >= '$cutOffDate')";
				$arrSchoolEventID = $iCal->returnVector($sql);
				
				# delete related event record in school calendar
				if(sizeof($arrSchoolEventID) >0){
					$targetSchoolEventID = implode(",",$arrSchoolEventID);
					$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$targetSchoolEventID.")";
					$iCal->db_db_query($sql);
					$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$targetSchoolEventID.")";
					$iCal->db_db_query($sql);
					$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN (".$targetSchoolEventID.")";
					$iCal->db_db_query($sql);
				}
				
				# delete this and following events from the original recurrence series
				$result['delete_repeat_events_fol'] = $iCal->deleteRepeatFollowingEvents($cutOffDate, $repeatID);
			}
		} else { # non-repeat event OR repeat event but delete only this instance
			# Check any record in School Calendar
			$sql = "SELECT SchoolEventID FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN ($eventID)";
			$arrSchoolEventID = $iCal->returnVector($sql);
			# delete related event record in school calendar
			if(sizeof($arrSchoolEventID) >0){
				$targetSchoolEventID = implode(",",$arrSchoolEventID);
				$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN (".$targetSchoolEventID.")";
				$iCal->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN (".$targetSchoolEventID.")";
				$iCal->db_db_query($sql);
				$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE CalendarEventID IN (".$targetSchoolEventID.")";
				$iCal->db_db_query($sql);
			}
		
			# delete main detail
			$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID = $eventID";
			$result['delete_event_entry'] = $iCal->db_db_query($sql);
			
			# delete Personal Note
			$sql = "DELETE FROM CALENDAR_EVENT_PERSONAL_NOTE WHERE EventID = $eventID";
			$result['delete_event_personal_note'] = $iCal->db_db_query($sql);
			
			# delete reminders
			$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID = $eventID";
			$result['delete_event_reminder'] = $iCal->db_db_query($sql);
			
			# delete guests
			$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID = $eventID";
			$result['delete_event_guest'] = $iCal->db_db_query($sql);
			
			##### EBOOKING #####
			if($plugin['eBooking'] && $handleBookingRecords)
			{
				$sql = "SELECT BookingID FROM INTRANET_EBOOKING_CALENDAR_EVENT_RELATION WHERE EventID IN ($eventID)";
				$arrBookingID = $iCal->returnVector($sql);
				
				if(sizeof($arrBookingID) > 0)
				{
					$targetBookingID = implode(",",$arrBookingID);
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
					$iCal->db_db_query($sql);
						
					$sql = "UPDATE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
					$iCal->db_db_query($sql);
					
		//			$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET BookingStatus = 0 WHERE BookingID IN ($targetBookingID)";
		//			$iCal->db_db_query($sql);
					
					$sql = "UPDATE INTRANET_EBOOKING_RECORD SET RecordStatus = 0 WHERE BookingID IN ($targetBookingID)";
					$iCal->db_db_query($sql);
				}
			}
		}
		
		if(!in_array(false, $result)){
			$Msg = $Lang['ReturnMsg']['EventDeleteSuccess'];	
		} else {
			$Msg = $Lang['ReturnMsg']['EventDeleteUnSuccess'];
		}
		
		return !in_array(false,$result);	
	}
	
	// choose users to a calendar or choose participants to an event
	function chooseUsers($CurrentUserID, $OptValue='', $ChooseGroupID=array(), $ChooseUserID=array())
	{
		global $intranet_root, $sys_custom, $PATH_WRT_ROOT, $plugin, $special_feature, $Lang, $intranet_session_language;
		include_once($intranet_root."/includes/libuser.php");
		include_once($intranet_root."/includes/role_manage.php");
		include_once($intranet_root."/includes/form_class_manage.php");
		include_once($intranet_root."/includes/libgrouping.php");
		include_once($intranet_root."/lang/lang.$intranet_session_language.php");
		include_once($intranet_root."/lang/ical_lang.$intranet_session_language.php");
		include_once($intranet_root."/includes/libwebmail.php");
		include_once($intranet_root."/includes/user_right_target.php");
		
		$li = new libuser($CurrentUserID);
		$lrole = new role_manage();
		$fcm = new form_class_manage();
		$lgrouping = new libgrouping();
		$lwebmail = new libwebmail();
		
		if(!isset($UserID)){
			global $UserID;
			$UserID = $CurrentUserID;
		}
		
		if(!isset($_SESSION['SSV_USER_TARGET'])){
			$UserRightTarget = new user_right_target();
			$_SESSION['SSV_USER_TARGET'] = $UserRightTarget->Load_User_Target($CurrentUserID);
		}
		
		$name_field = getNameFieldWithClassNumberByLang("a.");
		$identity = $lrole->Get_Identity_Type($CurrentUserID);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		
		$result_to_options = array();
		$result_to_options['ToTeachingStaffOption'] = 0;
		$result_to_options['ToNonTeachingStaffOption'] = 0;
		$result_to_options['ToStudentOption'] = 0;
		$result_to_options['ToParentOption'] = 0;
		$result_to_options['ToAlumniOption'] = 0;
		
		$result_to_options['ToTeacherAndStaff'] = 0;
		if($sys_custom['iMail_RecipientCategory_StaffAndTeacher'])
		{
			if(($identity == "Teaching") || ($identity == "NonTeaching"))
			{
				$result_to_options['ToTeacherAndStaff'] = 1;
			}
		}
		
		if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
		{
			if($sys_custom['iMail_RemoveTeacherCat']) {
				$result_to_options['ToTeachingStaffOption'] = 0;
			} else {
				$result_to_options['ToTeachingStaffOption'] = 1;
			}
		}
		if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
		{
			if($sys_custom['iMail_RemoveNonTeachingCat']) {
				$result_to_options['ToNonTeachingStaffOption'] = 0;
			} else {
				$result_to_options['ToNonTeachingStaffOption'] = 1;
			}
		}
		if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
		{
			$result_to_options['ToStudentOption'] = 1;
		}
		if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
		{
			$result_to_options['ToParentOption'] = 1;
		}
		if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['All-Yes'] || $_SESSION['SSV_USER_TARGET']['Alumni-All']) )
		{
			$result_to_options['ToAlumniOption'] = 1;
		}
		if( ($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
			($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
			($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
			($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
			($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || 
			($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) ||
			($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) ||
			($_SESSION['SSV_USER_TARGET']['Student-All']) ||
			($_SESSION['SSV_USER_TARGET']['Parent-All']) || 
			($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || 
			$_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']))
			)
		{
			$result_to_options['ToGroupOption'] = 1;
			
			$result_to_group_options['ToTeacher'] = 0;
			$result_to_group_options['ToStaff'] = 0;
			$result_to_group_options['ToStudent'] = 0;
			$result_to_group_options['ToParent'] = 0;
			$result_to_group_options['ToAlumni'] = 0;
			
			if($_SESSION['SSV_USER_TARGET']['All-Yes']){
				$result_to_group_options['ToTeacher'] = 1;
				$result_to_group_options['ToStaff'] = 1;
				$result_to_group_options['ToStudent'] = 1;
				$result_to_group_options['ToParent'] = 1;
				if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
			}else{
				if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
					$result_to_group_options['ToTeacher'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
					$result_to_group_options['ToStaff'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
					$result_to_group_options['ToStudent'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
					$result_to_group_options['ToParent'] = 1;
				}
				if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']) ){
					$result_to_group_options['ToAlumni'] = 1;
				}
			}
		}
		
		$AlumniNoTargetCond = true;
		if($special_feature['alumni']) $AlumniNoTargetCond = $_SESSION['SSV_USER_TARGET']['Alumni-All'] == '' && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] == '';
		### If no mail targeting is set in the front-end, than will assign some default targeting to user ###
		if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') && 
		($_SESSION['SSV_USER_TARGET']['All-No'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') && 
		($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Student-All'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
		($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') && 
		($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == '') && 
		$AlumniNoTargetCond 
		)
		{
			if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0 && $result_to_options['ToAlumniOption']==0)
			{
				if(($identity == "Teaching") || ($identity == "NonTeaching"))
				{
					### If user is Teacher, then will have the follow targeting :
					###  - to All Teaching Staff
					###  - to All NonTeaching Staff
					###  - to All Student 
					###  - to All Parent 
					$result_to_options['ToTeachingStaffOption'] = 1;
					$result_to_options['ToNonTeachingStaffOption'] = 1;
					$result_to_options['ToStudentOption'] = 1;
					$result_to_options['ToParentOption'] = 1;
					if($special_feature['alumni']) $result_to_options['ToAlumniOption'] = 1;
					$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
				}
				if($identity == "Student")
				{
					### If user is Student, then will have the follow targeting :
					###  - to Student Own Class Student
					###  - to Student Own Subject Group Student
					$result_to_options['ToStudentOption'] = 1;
					$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;	
					$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
				}
				if($identity == "Parent")
				{
					### If user is Parent, then will have the follow targeting :
					###  - to Their Child's Own Class Teacher
					###  - to Their Child's Own Subject Group Teacher
					$result_to_options['ToTeachingStaffOption'] = 1;
					$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;	
					$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
				}
			}
			$result_to_options['ToGroupOption'] = 1;
			
			$result_to_group_options['ToTeacher'] = 0;
			$result_to_group_options['ToStaff'] = 0;
			$result_to_group_options['ToStudent'] = 0;
			$result_to_group_options['ToParent'] = 0;
			$result_to_group_options['ToAlumni'] = 0;
			
			if($_SESSION['SSV_USER_TARGET']['All-Yes']){
				$result_to_group_options['ToTeacher'] = 1;
				$result_to_group_options['ToStaff'] = 1;
				$result_to_group_options['ToStudent'] = 1;
				$result_to_group_options['ToParent'] = 1;
				if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
			}else{
				if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
					$result_to_group_options['ToTeacher'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
					$result_to_group_options['ToStaff'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
					$result_to_group_options['ToStudent'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
					$result_to_group_options['ToParent'] = 1;
				}
				if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
					$result_to_group_options['ToAlumni'] = 1;
				}
			}
		}
		
		if($identity != "Student")
		{
			$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE ParentID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."'";
			$result = $li->returnVector($sql);
			if($result[0]>0)
				$result_to_options['ToMyChildrenOption'] = 1;
		}
		if($identity == "Student")
		{
			$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE StudentID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."'";
			$result = $li->returnVector($sql);
			if($result[0]>0)
				$result_to_options['ToMyParentOption'] = 1;
		}
		
		$return_ary = array();
		$level1_ary = array('name'=>'OptValue','optgroup'=>array());
		$level2_ary = array('name'=>'ChooseGroupID[]','option'=>array());
		$level3_ary = array('name'=>'ChooseUserID[]','option'=>array());
		
		## New 1st level selection box (By Identity / By Group)
		$x1  = ($OptValue!="" && $OptValue > 0) ? "<select name='OptValue' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()' >\n" : "<select name='OptValue' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]); this.form.submit()' >\n";
		$x1 .= "<option value='0' >--{$button_select}--</option>\n";
		$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByIdentity'],ENT_QUOTES)."'>";
		
		$optgroup_identity = array('label'=>$Lang['iMail']['FieldTitle']['ByIdentity'],'option'=>array());
		
		# Create Cat list - IP25 Only #
		## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
		## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
		if ($result_to_options['ToTeacherAndStaff'])
		{
			$x1 .= "<option value=-3 ".(($OptValue==-3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['TeacherAndStaff']."</option>\n";
			$optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['TeacherAndStaff'],'name'=>'OptValue','value'=>-3,'selected'=>$OptValue==-3);
		}
		if ($result_to_options['ToTeachingStaffOption'])
		{
		    $x1 .= "<option value=-1 ".(($OptValue==-1)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Teacher']."</option>\n";
		    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Teacher'],'name'=>'OptValue','value'=>-1,'selected'=>$OptValue==-1);
		}
		if ($result_to_options['ToNonTeachingStaffOption'])
		{
		    $x1 .= "<option value=-2 ".(($OptValue==-2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['NonTeachingStaff']."</option>\n";
		    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['NonTeachingStaff'],'name'=>'OptValue','value'=>-2,'selected'=>$OptValue==-2);
		}
		if ($result_to_options['ToStudentOption'])
		{
		    $x1 .= "<option value=2 ".(($OptValue==2)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Student']."</option>\n";
		    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Student'],'name'=>'OptValue','value'=>2,'selected'=>$OptValue==2);
		}
		if ($result_to_options['ToParentOption'])
		{
		    $x1 .= "<option value=3 ".(($OptValue==3)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Parent']."</option>\n";
		    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Parent'],'name'=>'OptValue','value'=>3,'selected'=>$OptValue==3);
		}
		if ($special_feature['alumni'] && $result_to_options['ToAlumniOption'])
		{
		    $x1 .= "<option value=-4 ".(($OptValue==-4)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Alumni']."</option>\n";
		    $optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['Alumni'],'name'=>'OptValue','value'=>-4,'selected'=>$OptValue==-4);
		}
		if ($result_to_options['ToMyChildrenOption'])
		{
			$x1 .= "<option value=5 ".(($OptValue==5)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyChildren']."</option>\n";
			$optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['MyChildren'],'name'=>'OptValue','value'=>5,'selected'=>$OptValue==5);
		}
		if ($result_to_options['ToMyParentOption'])
		{
			$x1 .= "<option value=6 ".(($OptValue==6)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['MyParent']."</option>\n";
			$optgroup_identity['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['MyParent'],'name'=>'OptValue','value'=>6,'selected'=>$OptValue==6);
		}
		$x1 .= "</optgroup>";
		
		$optgroup_group = array('label'=>$Lang['iMail']['FieldTitle']['ByGroup'],'option'=>array());
		
		//$lclubsenrol = new libclubsenrol();
		$arrExcludeGroupCatID[] = 0;
		//if($lclubsenrol->isUsingYearTermBased == 1){
		//	$arrExcludeGroupCatID[] = 5;
		//}
		$targetGroupCatIDs = implode(",",IntegerSafe($arrExcludeGroupCatID));
		
		if($_SESSION['SSV_USER_TARGET']['All-Yes'])
		{
			$sql = "SELECT DISTINCT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY CategoryName ASC";
		}
		else
		{
			if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
			{
				$sql = "SELECT DISTINCT a.GroupCategoryID, a.CategoryName FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE c.UserID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."' AND b.RecordType != 0 AND a.GroupCategoryID NOT IN ($targetGroupCatIDs) ORDER BY a.CategoryName ASC";
			}
		}
		$result = $li->returnArray($sql);
		if(sizeof($result) > 0){
			for($i=0; $i<sizeof($result); $i++)
			{
				if($i == 0){
					$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByGroup'],ENT_QUOTES)."'>";
				}
				list($GroupCatID, $GroupCatName) = $result[$i];
				$GroupCatID = "GROUP_".$GroupCatID;
				
				$x1 .= "<option value='$GroupCatID' ".(($GroupCatID == $OptValue)?"SELECTED":"").">".$GroupCatName."</option>";
		
				if($i== sizeof($result)-1) {
					$x1 .= "</optgroup>";
				}
				
				$optgroup_group['option'][] = array('display'=>$GroupCatName,'name'=>'OptValue','value'=>$GroupCatID,'selected'=>$OptValue==$GroupCatID);
			}
		}
		$x1 .= "</select>";
		
		if($OptValue != "")
		{
			if(strpos($OptValue,"GROUP_") !== false)
			{
				## GROUP 
				$GroupOpt = 2;
				$CatID = 4;
				$ChooseGroupCatID = substr($OptValue,6,strlen($OptValue));
				$x1 .= "<input type=\"hidden\" name=\"CatID\" value=\"".escape_double_quotes($CatID)."\">";
			}
			else
			{	
				## Identity
				$GroupOpt = 1;
				$CatID = $OptValue;
				$x1 .= "<input type=\"hidden\" name=\"CatID\" value=\"".escape_double_quotes($CatID)."\">";
			}
		}
		else
		{
			$GroupOpt = "";
			$CatID = "";
			$ChooseGroupCatID = "";
			$x1 .= "<input type='hidden' name='CatID' value=''>";
		}
		
		$level1_ary['optgroup'][] = $optgroup_identity;
		$level1_ary['optgroup'][] = $optgroup_group;
		$return_ary[] = $level1_ary;
		
		if(!isset($ChooseGroupID)){
			$ChooseGroupID = array();
		}else if(!is_array($ChooseGroupID)){
			$ChooseGroupID = (array)$ChooseGroupID;
		}
		
		# 2nd Level Cat List - IP25 only #
		if(($CatID != "") || ($CatID != 0) || ($CatID != 4)){
			if($CatID == -1){
				//$x2 = "<select name='Cat2ID' id='Cat2ID' multiple size='10'>";
				$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
				## All Teaching Staff ##
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])){
					
					$num_of_all_teacher = $lwebmail->returnNumOfAllTeachingStaff($identity);
					if($num_of_all_teacher > 0){
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
						else
							$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'1','selected'=>in_array(1,$ChooseGroupID));
					}
				}
				## Form Teacher ##
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyForm'])){
					
					$num_of_form_teacher = $lwebmail->returnNumOfFormTeacher($identity);
					if($num_of_form_teacher > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
						else
							$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormTeachingStaff']."</option>";
							
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToFormTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'2','selected'=>in_array(2,$ChooseGroupID));
					}
				}
				## Class Teacher ##
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyClass'])){
					
					$num_of_class_teacher = $lwebmail->returnNumOfClassTeacher($identity);
					if($num_of_class_teacher > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
						else
							$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassTeachingStaff']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToClassTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'3','selected'=>in_array(3,$ChooseGroupID));
					}
				}
				## Subject Teacher ##
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubject'])){
					
					$num_of_subject_teacher = $lwebmail->returnNumOfSubjectTeacher($identity);
					if($num_of_subject_teacher > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
						else
							$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff']."</option>";
							
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'4','selected'=>in_array(4,$ChooseGroupID));
					}
				}
				## Subject Group Teacher ##
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])){
					
					$num_of_subject_group_teacher = $lwebmail->returnNumOfSubjectGroupTeacher($identity);
					if($num_of_subject_group_teacher > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
						else
							$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'],'name'=>'ChooseGroupID[]','value'=>'5','selected'=>in_array(5,$ChooseGroupID));
					}
				}
				$x2 .= "</select>";
			}
			if($CatID == -2){
				## Non-teaching Staff ##
				$result = $fcm->Get_Non_Teaching_Staff_List();
				$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
				if(sizeof($result)>0){
					for($i=0; $i<sizeof($result); $i++){
						list($u_id, $u_name) = $result[$i];
						$x3 .= "<option value='$u_id'>".$u_name."</option>";
						
						$level3_ary['option'][] = array('display'=>$u_name,'name'=>'ChooseUserID[]','value'=>$u_id,'selected'=>in_array($u_id,$ChooseUserID));
					}
				}else{
					$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
				}
				$x3 .= "</select>";
			}
			## Newly added by Ronald (20091118) - for �^�خѰ| [CRM Ref No.: 2009-1116-0915]
			## Control by flag - $sys_custom['iMail_RecipientCategory_StaffAndTeacher']
			if($CatID == -3){
				## Teachers / Staff ##
				$sql = "Select UserID, ".getNameFieldByLang()." as Name From INTRANET_USER WHERE RecordStatus = '1' AND RecordType = '1' ORDER BY EnglishName";
				$result = $fcm->returnArray($sql,2);
				
				$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
				if(sizeof($result)>0){
					for($i=0; $i<sizeof($result); $i++){
						list($u_id, $u_name) = $result[$i];
						$x3 .= "<option value='$u_id'>".$u_name."</option>";
						
						$level3_ary['option'][] = array('display'=>$u_name,'name'=>'ChooseUserID[]','value'=>$u_id,'selected'=>in_array($u_id,$ChooseUserID));
					}
				}else{
					$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
				}
				$x3 .= "</select>";
			}
			if($CatID == 2){
				$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
				## All Student
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])){
					
					$num_of_all_student = $lwebmail->returnNumOfAllStudent($identity);
					if($num_of_all_student > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
						else
							$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsStudent']."</option>";
							
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToIndividualsStudent'],'name'=>'ChooseGroupID[]','value'=>'1','selected'=>in_array(1,$ChooseGroupID));
					}
				}
				## Form Student
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyForm'])){
					
					$num_of_form_subject = $lwebmail->returnNumOfFormStudent($identity);
					if($num_of_form_subject > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
						else
							$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormStudent']."</option>";
							
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToFormStudent'],'name'=>'ChooseGroupID[]','value'=>'2','selected'=>in_array(2,$ChooseGroupID));
					}
				}
				## Class Student
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
					
					$num_of_class_student = $lwebmail->returnNumOfClassStudent($identity);
					if($num_of_class_student > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
						else
							$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassStudent']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToClassStudent'],'name'=>'ChooseGroupID[]','value'=>'3','selected'=>in_array(3,$ChooseGroupID));
					}
				}
				## Subject Student
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubject'])){
					
					$num_of_subject_student = $lwebmail->returnNumOfSubjectStudent($identity);
					if($num_of_subject_student > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
						else
							$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectStudent']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectStudent'],'name'=>'ChooseGroupID[]','value'=>'4','selected'=>in_array(4,$ChooseGroupID));
					}
				}
				## Subject Group Student
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
					
					$num_of_subject_group_student = $lwebmail->returnNumOfSubjectGroupStudent($identity);
					if($num_of_subject_group_student > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
						else
							$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'],'name'=>'ChooseGroupID[]','value'=>'5','selected'=>in_array(5,$ChooseGroupID));
					}
				}
				$x2 .= "</select>";
			}
			if($CatID == 3){
				$x2 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
				## All Parent
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])){
					
					$num_of_all_parent = $lwebmail->returnNumOfAllParent($identity);
					if($num_of_all_parent > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='1' ".((in_array(1,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
						else
							$x2 .= "<option value='1' >".$Lang['iMail']['FieldTitle']['ToIndividualsParents']."</option>";
							
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToIndividualsParents'],'name'=>'ChooseGroupID[]','value'=>'1','selected'=>in_array(1,$ChooseGroupID));
					}
				}
				## Form Parent
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyForm'])){
					
					$num_of_form_parent = $lwebmail->returnNumOfFormParent($identity);
					if($num_of_form_parent > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='2' ".((in_array(2,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
						else
							$x2 .= "<option value='2' >".$Lang['iMail']['FieldTitle']['ToFormParents']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToFormParents'],'name'=>'ChooseGroupID[]','value'=>'2','selected'=>in_array(2,$ChooseGroupID));
					}
				}
				## Class Parent
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
					
					$num_of_class_parent = $lwebmail->returnNumOfClassParent($identity);
					if($num_of_class_parent > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='3' ".((in_array(3,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
						else
							$x2 .= "<option value='3' >".$Lang['iMail']['FieldTitle']['ToClassParents']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToClassParents'],'name'=>'ChooseGroupID[]','value'=>'3','selected'=>in_array(3,$ChooseGroupID));
					}
				}
				## Subject Parent
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubject'])){
					
					$num_of_subject_parent = $lwebmail->returnNumOfSubjectParent($identity);
					if($num_of_subject_parent > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='4' ".((in_array(4,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
						else
							$x2 .= "<option value='4' >".$Lang['iMail']['FieldTitle']['ToSubjectParents']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectParents'],'name'=>'ChooseGroupID[]','value'=>'4','selected'=>in_array(4,$ChooseGroupID));
					}
				}
				## Subject Group Parent
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
					
					$num_of_subject_group_parent = $lwebmail->returnNumOfSubjectGroupParent($identity);
					if($num_of_subject_group_parent > 0)
					{
						if(sizeof($ChooseGroupID)>0)
							$x2 .= "<option value='5' ".((in_array(5,$ChooseGroupID))?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
						else
							$x2 .= "<option value='5' >".$Lang['iMail']['FieldTitle']['ToSubjectGroupParents']."</option>";
						
						$level2_ary['option'][] = array('display'=>$Lang['iMail']['FieldTitle']['ToSubjectGroupParents'],'name'=>'ChooseGroupID[]','value'=>'5','selected'=>in_array(5,$ChooseGroupID));
					}
				}
				$x2 .= "</select>";
			}
			if($special_feature['alumni'] && $CatID == -4){
				## All Alumni ##
				$NameField = getNameFieldByLang();
				$sql = "Select UserID, ".$NameField." as Name From INTRANET_USER where RecordStatus = 1 and RecordType = 4 ORDER BY EnglishName";
				$result = $li->returnArray($sql);
				$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
				if(sizeof($result)>0){
					for($i=0; $i<sizeof($result); $i++){
						list($u_id, $u_name) = $result[$i];
						$x3 .= "<option value='$u_id'>".$u_name."</option>";
						
						$level3_ary['option'][] = array('display'=>$u_name,'name'=>'ChooseUserID[]','value'=>$u_id,'selected'=>in_array($u_id,$ChooseUserID));
					}
				}else{
					$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
				}
				$x3 .= "</select>";
			}
			
			if($CatID == 5){
				$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.")." FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ParentID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."'";
				$result = $li->returnArray($sql,2);
				
				$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
				if(sizeof($result)>0){
					for($i=0; $i<sizeof($result); $i++){
						list($u_id, $u_name) = $result[$i];
						$x3 .= "<option value='$u_id'>".$u_name."</option>";
						
						$level3_ary['option'][] = array('display'=>$u_name,'name'=>'ChooseUserID[]','value'=>$u_id,'selected'=>in_array($u_id,$ChooseUserID));
					}
				}else{
					$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
				}
				$x3 .= "</select>";
			}
			if($CatID == 6){
				$sql = "SELECT b.UserID, ".getNameFieldByLang2("b.")." FROM INTRANET_PARENTRELATION AS a INNER JOIN INTRANET_USER AS b ON (a.ParentID = b.UserID) WHERE a.StudentID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."'";
				$result = $li->returnArray($sql,2);
				
				$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
				if(sizeof($result)>0){
					for($i=0; $i<sizeof($result); $i++){
						list($u_id, $u_name) = $result[$i];
						$x3 .= "<option value='$u_id'>".$u_name."</option>";
						
						$level3_ary['option'][] = array('display'=>$IMap->$u_name,'name'=>'ChooseUserID[]','value'=>$u_id,'selected'=>in_array($u_id,$ChooseUserID));
					}
				}else{
					$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
				}
				$x3 .= "</select>";
			}
		}
		
		if($GroupOpt == 2 && $CatID == 4 && $ChooseGroupCatID!="")
		{
			if($_SESSION['SSV_USER_TARGET']['All-Yes'])
			{
				if($ChooseGroupCatID != 5)
				{
					$sql = "SELECT GroupID, Title FROM INTRANET_GROUP WHERE RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' AND RecordType != 0 AND AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' ORDER BY Title";
				}
				else
				{
					$CurrentSemesterID = getCurrentSemesterID();
					$sql = "SELECT 
								a.GroupID, a.Title 
							FROM 
								INTRANET_GROUP AS a
								INNER JOIN INTRANET_ENROL_GROUPINFO AS b ON (a.GroupID = b.GroupID)
							WHERE 
								a.RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' 
								AND a.RecordType != 0 
								AND (
									(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.Semester = '') 
									OR
									(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.Semester IS NULL) 
									OR
									(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.Semester = '".$li->Get_Safe_Sql_Query($CurrentSemesterID)."') 
									)
							ORDER BY a.Title";
				}
			}else{
				if($result_to_group_options['ToTeacher'] || $result_to_group_options['ToStaff'] || $result_to_group_options['ToStudent'] || $result_to_group_options['ToParent'] || ($special_feature['alumni'] && $result_to_group_options['ToAlumni']))
				{
					if($ChooseGroupCatID != 5)
					{
						$sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) WHERE a.RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' AND a.RecordType != 0 AND a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND b.UserID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."' ORDER BY Title";
					}
					else
					{
						$CurrentSemesterID = getCurrentSemesterID();
						$sql = "SELECT 
									a.GroupID, a.Title 
								FROM 
									INTRANET_GROUP AS a 
									INNER JOIN INTRANET_ENROL_GROUPINFO AS enrol_group ON (a.GroupID = enrol_group.GroupID)
									INNER JOIN INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID AND enrol_group.EnrolGroupID = b.EnrolGroupID) 
								WHERE 
									a.RecordType = '".$li->Get_Safe_Sql_Query($ChooseGroupCatID)."' 
									AND a.RecordType != 0 
									AND (
										(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND enrol_group.Semester = '') 
										OR
										(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND enrol_group.Semester IS NULL) 
										OR
										(a.AcademicYearID = '".$li->Get_Safe_Sql_Query($CurrentAcademicYearID)."' AND enrol_group.Semester = '".$li->Get_Safe_Sql_Query($CurrentSemesterID)."') 
										)
									AND b.UserID = '".$li->Get_Safe_Sql_Query($CurrentUserID)."' 
								ORDER BY a.Title";
					}
				}
			}
			$GroupArray = $li->returnArray($sql,2);
			$x2_5 = "<select name='ChooseGroupID[]' id='ChooseGroupID[]' multiple size='10'>";
			if(sizeof($GroupArray) > 0){
				for($i=0; $i<sizeof($GroupArray); $i++)
				{
					list($GroupID, $GroupName) = $GroupArray[$i];
					if(sizeof($ChooseGroupID)>0)
						$x2_5 .= "<option value='$GroupID' ".((in_array($GroupID,$ChooseGroupID))?"SELECTED":"").">".$GroupName."</option>";
					else
						$x2_5 .= "<option value='$GroupID' >".$GroupName."</option>";
						
					$level2_ary['option'][] = array('display'=>$GroupName,'name'=>'ChooseGroupID[]','value'=>$GroupID,'selected'=>in_array($GroupID,$ChooseGroupID));
				}
			}else{
				$x2_5 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			}
			$x2_5 .= "</select>";
			$ShowSubGroupSelection = true;
		}
		else
		{
			$ShowSubGroupSelection = false;
		}
		
		if($CatID!='' && $CatID != 0 && $CatID!=-2 && $CatID!=5 && $CatID!=6 && $CatID!=-3 && $CatID!=-4)
		{
			if(($GroupOpt == 1 && $CatID != 4) || $ShowSubGroupSelection || $CatID == "INTERNAL_GROUP") {
				$return_ary[] = $level2_ary;
			}
		}
				
		if($CatID != "" && sizeof($ChooseGroupID)>0)
		{
			$sql = "";
			if($CatID == -1) // teaching staff
			{
				for($i=0; $i<sizeof($ChooseGroupID); $i++)
				{			
					if($ChooseGroupID[$i] == 1)
					{
						## All Teaching Staff ##
						if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent") || ($special_feature['alumni'] && $identity == "Alumni"))
						{
							//$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.")." FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 AND all_user.UserID != $UserID)";
							$all_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.")." FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
							$sql = $all_sql;
						}
					}
					if($ChooseGroupID[$i] == 2)
					{
						## Form Teacher ##
						if($identity == "Teaching")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to form teacher
						}
						if($identity == "Student")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "Parent")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
					}
					if($ChooseGroupID[$i] == 3)
					{		
						## Class Teacher ##
						if($identity == "Teaching")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to class teacher
						}
						if($identity == "Student")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "Parent")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
					}
					if($ChooseGroupID[$i] == 4)
					{
						## Subject Teacher ##
						if($identity == "Teaching")
						{					
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to subject teacher
						}
						if($identity == "Student")
						{					
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "Parent")
						{					
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
					}
					if($ChooseGroupID[$i] == 5)
					{
						## Subject Group Teacher ##
						if($identity == "Teaching")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to subject group teacher
						}
						if($identity == "Student")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "Parent")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID') ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
					}
				}
				$result = $li->returnArray($sql);
			}
			if($CatID == 2) // student
			{
				for($i=0; $i<sizeof($ChooseGroupID); $i++)
				{
					if($ChooseGroupID[$i] == 1)
					{
						## All Student ##
						if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
						{
							//$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND UserID != $UserID AND (ClassName != '' OR ClassName != NULL) AND (ClassNumber != '' OR ClassNumber != NULL) ORDER BY ClassName, ClassNumber, EnglishName)";
							$all_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
							$sql = $all_sql;
						}
					}
					if($ChooseGroupID[$i] == 2)
					{
						## My Form Student ##
						if($identity == "Teaching")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to form student
						}
						if($identity == "Student")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "Parent")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = $UserID AND b.AcademicYearID = $CurrentAcademicYearID AND c.AcademicYearID = $CurrentAcademicYearID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$CurrentUserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
					}
					if($ChooseGroupID[$i] == 3)
					{
						## My Class Student ##
						if($identity == "Teaching")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to class student
						}
						if($identity == "Student")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "Parent")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.")." FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
					}
					if($ChooseGroupID[$i] == 4)
					{
						## My Subject Student ##
						if($identity == "Teaching")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to Subject student
						}
						if($identity == "Student")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "Parent")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
					}
					if($ChooseGroupID[$i] == 5)
					{		
						## My Subject Group Student ##		
						if($identity == "Teaching")
						{	
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to subject group student
						}
						if($identity == "Student")
						{	
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "Parent")
						{	
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID) AND b.UserID != $UserID AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.")." FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
					}
				}
				$result = $li->returnArray($sql);
			}
			if($CatID == 3) // parent
			{
				for($i=0; $i<sizeof($ChooseGroupID); $i++)
				{
					if($ChooseGroupID[$i] == 1)
					{
						## All Parents ##
						if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
						{					
							//$name_field = getParentNameWithStudentInfo("a.","c.");
							//$all_sql = "(SELECT b.ParentID, CONCAT('(',a.ClassName,'-',a.ClassNumber,') ',a.EnglishName,'\'s Parent',' (',c.EnglishName,')') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT DISTINCT StudentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							//$all_sql = "(SELECT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT DISTINCT StudentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1 AND b.ParentID != $UserID) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							
							$all_sql = "(SELECT DISTINCT a.UserID, IF(c.EnglishName != '' OR c.EnglishName IS NOT NULL, CONCAT(IF(c.ClassName IS NOT NULL AND c.ClassNumber IS NOT NULL, CONCAT('(',c.ClassName,'-',c.ClassNumber,') '),''), ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".getNameFieldByLang2('a.').",')'), ".getNameFieldByLang2('a.').") FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
							$sql = $all_sql;
						}
					}
					if($ChooseGroupID[$i] == 2)
					{
						## My Form Parents ##
						if($identity == "Teaching")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$form_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE a.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to form parent
						}
						if($identity == "Student")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
						if($identity == "Parent")
						{
							//$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID) AND a.AcademicYearID = $CurrentAcademicYearID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$form_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$form_sql;
						}
					}
					if($ChooseGroupID[$i] == 3)
					{
						## My Class Parents ##
						if($identity == "Teaching")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$class_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to class parent
						}
						if($identity == "Student")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
						if($identity == "Parent")
						{
							//$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = $UserID AND a.AcademicYearID = $CurrentAcademicYearID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$class_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$CurrentUserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$class_sql;
						}
					}
					if($ChooseGroupID[$i] == 4)
					{
						## My Subject Parents ##
						if($identity == "Teaching")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$subject_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to subject parent
						}
						if($identity == "Student")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
						if($identity == "Parent")
						{
							//$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID AND c.YearTermID = $CurrentTermID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_sql;
						}
					}
					if($ChooseGroupID[$i] == 5)
					{
						## My Subject Group Parents ##
						if($identity == "Teaching")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to subject group parent
						}
						if($identity == "Student")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
						if($identity == "Parent")
						{
							//$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $UserID AND b.YearTermID = $CurrentTermID)) AND a.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$name_field = getParentNameWithStudentInfo("a.","c.");
							//$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = $UserID AND b.YearTermID = $CurrentTermID)) AND c.UserID != $UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
							$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$CurrentUserID' AND b.YearTermID = '$CurrentTermID')) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
							if($sql != "")
								$delimiter = " UNION ";
							
							$sql .= $delimiter.$subject_group_sql;
						}
					}			
				}
				$result = $li->returnArray($sql);
			}
			if($CatID == 4) // Group
			{
				if(sizeof($ChooseGroupID)>0)
				{			
					$TargetGroupID = implode(",",IntegerSafe($ChooseGroupID));
					
					$cond = "";
					if($result_to_group_options['ToTeacher'] == 1){
						if($cond != "")
							$cond .= " OR ";
						$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
					}
					if($result_to_group_options['ToStaff'] == 1){
						if($cond != "")
							$cond .= " OR ";
						$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
					}
					if($result_to_group_options['ToStudent'] == 1){
						if($cond != "")
							$cond .= " OR ";
						$cond .= " (a.RecordType = 2) ";
					}
					if($result_to_group_options['ToParent'] == 1){
						if($cond != "")
							$cond .= " OR ";
						$cond .= " (a.RecordType = 3) ";
					}
					if($special_feature['alumni'] && $result_to_group_options['ToAlumni'] == 1){
						if($cond != "")
							$cond .= " OR ";
						$cond .= " (a.RecordType = 4) ";
					}
					
					if($cond != "")
						$final_cond = " AND ( $cond ) ";
					
					//$sql = "SELECT a.UserID, ".getNameFieldByLang2("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE b.GroupID IN ($TargetGroupID) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
					$sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID) $final_cond ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
					$result = $li->returnArray($sql,2);
				}
			}
			$x3 = "<select id='ChooseUserID[]' name='ChooseUserID[]' multiple size=10>";
			if(sizeof($result)>0){
				for($i=0; $i<sizeof($result); $i++){
					list($u_id, $u_name) = $result[$i];
					$x3 .= "<option value='$u_id'>$u_name</option>";
					
					$level3_ary['option'][] = array('display'=>$u_name,'name'=>'ChooseUserID[]','value'=>$u_id,'selected'=>in_array($u_id,$ChooseUserID));
				}
			}else{
				$x3 .= "<option value=''>".$Lang['General']['NoRecordAtThisMoment']."</option>";
			}
			$x3 .= "</select>";
		
		}
		
		$direct_to_step3 = false;
		if($CatID != "")
		{
			if(($GroupOpt == 1) && (($CatID == -3)||($CatID == -2)||($CatID == 5)||($CatID == 6) || ($CatID == -4))){
				$direct_to_step3 = true;
			}else if(($GroupOpt == 2) && ($CatID == 4) && (sizeof($ChooseGroupID)>0) && (sizeof($ChooseGroupCatID)>0)) {
				$direct_to_step3 = true;
			} else {
				if(($GroupOpt == 1) && ($CatID != 4) && (sizeof($ChooseGroupID)>0)){
					$direct_to_step3 = true;
				}
			}
		}
		
		if($direct_to_step3){
			$return_ary[] = $level3_ary;
		}
		
		return $return_ary;
	}

	function Get_IMap_Calendar_Content($events)
	{
		if(!is_array($events)) {
			return '';
		}
		global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		global $iCalendar_NewEvent_Title, $iCalendar_NewEvent_IsAllDay, $iCalendar_NewEvent_DateTime, $iCalendar_NewEvent_Location, $iCalendar_NewEvent_Description;
		include_once($intranet_root."/lang/ical_lang.$intranet_session_language.php");

		$x = '';
		foreach($events as $event) {
			$x .= '<table width="100%" cellspacing="0" cellpadding="5" border="1">
					<tbody>';

			$x .= '<tr>
						<td width="120" align="left">
							<span>'.$iCalendar_NewEvent_Title.' :</span>
						</td>
						<td>'.$event['title'].'</td>
					</tr>';

			if($event['allDay'] == 'true') {
				$event_time = substr($event['start'], 0, strpos($event['start'],'.000')).' '.$iCalendar_NewEvent_IsAllDay;
			} else {
				$event_time = substr($event['start'], 0, strpos($event['start'],'.000')). ' - '.substr($event['end'], 0, strpos($event['end'],'.000'));
			}
			
			$x .= '<tr>
						<td width="120" align="left">
							<span>'.$iCalendar_NewEvent_DateTime.' :</span>
						</td>
						<td>'.$event_time.'</td>
					</tr>';

			$x .= '<tr>
						<td width="120" align="left">
							<span>'.$iCalendar_NewEvent_Location.' :</span>
						</td>
						<td>'.(($event['location'] == '') ? '-' : $event['location']).'</td>
					</tr>';

			$x .= '<tr>
						<td width="120" align="left">
							<span>'.$iCalendar_NewEvent_Description.' :</span>
						</td>
						<td>'.nl2br($event['description']).'</td>
					</tr>';

			$x .= '</tbody>
					</table><br>';
		}

		return $x;
	}
	//*****************  end of class  ***************************//	
}
// } // End of directive
?>