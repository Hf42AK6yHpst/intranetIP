<?
// using by 
include_once("libdb.php");
include_once("libaccessright.php");

class staff_access_right extends libaccessright
{
	var $Module;
	
	function staff_access_right()
	{
		parent::libdb();
		$this->Module = 'StaffAttendance3';
	}
	
	function Get_Group_List($Keyword)
	{
		if (trim($Keyword) != "")
		{
			$sql = 'select distinct 
								g.GroupID 
							From 
								ACCESS_RIGHT_GROUP as g  
							where 
								Module = \''.$this->Module.'\'
							and
								(
								g.GroupTitle like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								g.GroupDescription like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								g.GroupType like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								)';
			//debug_r($sql);
			$GroupIDList = '-1,'.implode(',',$this->returnVector($sql));
		}
		
		$sql = 'SELECT UserID FROM INTRANET_USER WHERE RecordType=\'1\' AND RecordStatus=\'1\' ';
		$StaffIDList = '-1,'.implode(',',$this->returnVector($sql));
		
		$sql = 'SELECT
				  g.GroupID,
				  g.GroupTitle,
				  IF(g.GroupDescription IS NULL OR TRIM(g.GroupDescription) = \'\',\'--\',g.GroupDescription) as GroupDescription,
				  g.GroupType,
				  CASE 
				  	WHEN count(1) = 1 and ug.GroupID IS NULL THEN 0 
				  	ELSE count(1) 
				  END as NumberOfMember
				FROM 
					ACCESS_RIGHT_GROUP as g
				LEFT JOIN ACCESS_RIGHT_GROUP_MEMBER as ug
					ON g.GroupID = ug.GroupID 
					AND ug.UserID IN ('.$StaffIDList.') 
				WHERE 
					g.Module = \''.$this->Module.'\' ';
		
		if (trim($Keyword)!="") 
			$sql .= 'AND g.GroupID in ('.$GroupIDList.') ';
			
		
			$sql .= 'GROUP BY
						  g.GroupID 
					ORDER BY
						  g.GroupTitle';
		
		return $this->returnArray($sql);
	}
	
	function Get_Group_Info($GroupID)
	{
		$sql = "SELECT
					GroupID,
					GroupTitle,
					GroupType,
					GroupDescription,
					DateModified
				FROM 
					ACCESS_RIGHT_GROUP 
				WHERE 
					GroupID = '$GroupID' 
					AND Module = '".$this->Module."' ";
					
		return $this->returnArray($sql);
	}
	
	function Insert_Group_Access_Right($GroupID, $GroupTitle, $GroupDescription,$Right)
	{
		$Results = array();
		if ($GroupID == "") {
			$GroupID = $this->GET_ACCESS_RIGHT_GROUP_ID('A',$this->Get_Safe_Sql_Query($GroupTitle));
			
			if($GroupID == "")
			{
				$GroupID = $this->NEW_ACCESS_RIGHT_GROUP('A', $this->Get_Safe_Sql_Query($GroupTitle),$this->Get_Safe_Sql_Query($GroupDescription));
				$Result['CreateGroup'] = $GroupID;
			}
			else 
				$Result['UpdateGroup'] = $this->UPDATE_ACCESS_RIGHT_GROUP($GroupID, $this->Get_Safe_Sql_Query($GroupTitle), $this->Get_Safe_Sql_Query($GroupDescription));
		}
		$Result['ClearOldRight'] = $this->DELETE_ACCESS_RIGHT($GroupID,'T'); 
		
		for ($i=0; $i< sizeof($Right); $i++) {
			$RightDetail = explode('_',$Right[$i]);
			$sql = "INSERT INTO ACCESS_RIGHT_GROUP_SETTING 
					(GroupID, Module, Section, Function, Action, DateInput, DateModified)
					 VALUES('$GroupID', '".$this->Module."', '".$RightDetail[0]."', '".$RightDetail[1]."', '".$RightDetail[2]."', NOW(), NOW())";
			$Result['InsertNewRight:'.$Right[$i]]=$this->db_db_query($sql);
		}
		
		if (in_array(false, $Result)) 
			return false;
		else 
			return $GroupID;
	}
	
	function Delete_Group($GroupID)
	{
		$Results=array();
		
		$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID = '$GroupID' ";
		$Results['Delete_Members'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID = '$GroupID' AND Module = '".$this->Module."' ";
		$Results['Delete_Settings'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM ACCESS_RIGHT_GROUP WHERE GroupID = '$GroupID' AND Module='".$this->Module."' ";
		$Results['Delete_Group'] = $this->db_db_query($sql);
		
		return (!in_array(false, $Results));
 	}
 	
 	function Rename_Group($GroupTitle,$GroupID)
 	{
		$sql = 'UPDATE ACCESS_RIGHT_GROUP SET
							GroupTitle = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\', 
							DateModified = NOW()
				WHERE
					GroupID = \''.$GroupID.'\' AND Module = \''.$this->Module.'\' ';
		
		return $this->db_db_query($sql);
	}
	
	function Check_Group_Title($GroupTitle, $GroupID)
	{
		$sql = "SELECT
					GroupID 
				FROM 
					ACCESS_RIGHT_GROUP
				WHERE
					GroupID != '$GroupID' 
					AND GroupTitle = '".$this->Get_Safe_Sql_Query($GroupTitle)."' 
					AND Module = '".$this->Module."' ";
					
		return (sizeof($this->returnVector($sql))>0 ?false:true);
	}
	
	function Set_Group_Description($GroupDescription, $GroupID)
	{
		$sql = "UPDATE 
					ACCESS_RIGHT_GROUP 
				SET 
					GroupDescription = '".$this->Get_Safe_Sql_Query($GroupDescription)."',
					DateModified = NOW() 
				WHERE GroupID = '$GroupID' AND Module = '".$this->Module."' ";
				
		return $this->db_db_query($sql);
	}
	
	function Get_Group_Access_Right_Users($GroupID, $Keyword="")
	{
		$NameField = getNameFieldByLang("u.");
		
		$sql = "SELECT 
					u.UserID,
					".$NameField." as UserName,
					u.Teaching 
				FROM 
					INTRANET_USER as u 
					LEFT JOIN 
					ACCESS_RIGHT_GROUP_MEMBER as m 
					ON u.UserID = m.UserID 
					LEFT JOIN 
					ACCESS_RIGHT_GROUP as g
					ON m.GroupID = g.GroupID 
				WHERE 
					u.RecordType = '1' 
					AND 
					u.RecordStatus = '1' 
					AND 
					m.GroupID = '$GroupID'
					AND 
					(
					u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
					OR 
					u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
					or 
					u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					or 
					u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
					)";
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Group_Access_Right_Aval_User_List()
	{
		$NameField = getNameFieldByLang("u.");
		/*
		$sql = "SELECT 
					u.UserID,
					".$NameField." AS StaffName
				FROM 
					INTRANET_USER u 
					LEFT JOIN 
					ACCESS_RIGHT_GROUP_MEMBER m 
					ON u.UserID = m.UserID 
				WHERE 
					u.RecordType = '1' 
					AND 
					u.RecordStatus = '1' 
					AND 
					m.GroupID IS NULL 
				ORDER BY 
					u.EnglishName";
		*/
		$sql = "SELECT 
					DISTINCT m.UserID 
				FROM 
					ACCESS_RIGHT_GROUP as g
					INNER JOIN ACCESS_RIGHT_GROUP_MEMBER as m ON m.GroupID = g.GroupID
				WHERE
					g.Module = '".$this->Module."' ";
		$StaffList=$this->returnVector($sql);
		$sql = "SELECT 
					u.UserID,
					".$NameField." AS StaffName
				FROM 
					INTRANET_USER u  
				WHERE 
					u.RecordType = '1' 
					AND 
					u.RecordStatus = '1' 
					AND u.UserID NOT IN (".(sizeof($StaffList)>0?implode(",",$StaffList):"-1").") 
				ORDER BY 
					u.EnglishName ";
		return $this->returnArray($sql);
	}
	
	function Add_Member($GroupID,$AddUserID)
	{
		if (sizeof($AddUserID) > 0) 
		{
			$StaffList = implode(',',$AddUserID);
		}
		
		for ($i=0; $i< sizeof($AddUserID); $i++) {
			$sql = 'INSERT INTO ACCESS_RIGHT_GROUP_MEMBER 
								(GroupID,UserID, DateInput) 
								VALUES
								(\''.$GroupID.'\',\''.$AddUserID[$i].'\',\'NOW()\')';
			$Result['Add-'.$AddUserID[$i]] = $this->db_db_query($sql);
		}
		
		return (!in_array(false,$Result));
	}
	
	function Remove_Member($GroupID,$StaffID)
	{
		$StaffList = implode(',',$StaffID);
		
		$sql = "DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE UserID IN (".$StaffList.") AND GroupID = '$GroupID' ";
		$Result['delete-ACCESS_RIGHT_GROUP_MEMBER'] = $this->db_db_query($sql);
		
		return (!in_array(false,$Result));
	}
}
?>