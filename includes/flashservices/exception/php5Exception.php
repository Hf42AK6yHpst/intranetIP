<?php
function amfErrorHandler($level, $string, $file, $line, $context)
{
	//forget about errors not defined at reported
	if( defined('AMFPHP_ERROR_LEVEL') && ((AMFPHP_ERROR_LEVEL >> log($level,2)) % 2 == 1))
	{
    	throw new Exception($string);
    }
}
set_error_handler("amfErrorHandler");
?>