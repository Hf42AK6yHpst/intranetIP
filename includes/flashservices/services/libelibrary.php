<?php
// ---------------------------
// class name must be the same
// as the php file name
// ---------------------------

// using by Paul

#################################################################################################
## Modification Log
## 2019-05-02 Pun
## - Prevent Command-injection
## 2016-09-28: Paul
## - replace preg_split by explode as error found

## 2009-03-02: Adam Hui
## - add function to save/get Book TLF xml string
## - modify function to get Review

## 2009-03-29: Adam Hui
## - Modify saveProgress function to cater finish time

#################################################################################################

class libelibrary
{
	var $block_save = false;
	var $view_right = true;
	var $errMsg = array(
				"No error",
				"Permission denied!",
				"Failed to save due to unkown reason, please try again.",
				"No Record"
				);
	var $encrypt_key = "YuEn_eClass";
	var $FILE_FOLDER;
	var $ECLASS_DB;
	var $ECLASS_DB_LOGIN;
	var $ECLASS_DB_PASSWD;
	var $ECLASS_HTTPPATH;
	var $ECLASS_FILEPATH;
	var $SERVER_IP;
	var $INTRANET_HTTPPATH;
	var $MESSAGECHARSET;
	var $INTRANET_ROOT;
	var $sql;
	var $db;
	var $rs;

	// constructor function
	function libelibrary()
	{
		// -----------------------------------------
		// the method table describes all the
		// available methods for this class to flash
		// and define the roles of these methods
		// -----------------------------------------
		$this->methodTable = array(
			// name of the function

			"getLoad" => array(
				"description" => "return saved data",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"sendSave" => array(
				"description" => "submit data to save in server",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"getBookmark" => array(
				"description" => "return saved bookmark page",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"addBookmark" => array(
				"description" => "add a new bookmark of a user",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"deleteBookmark" => array(
				"description" => "delete a bookmark",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"saveFormat" => array(
				"description" => "saving text format",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string", "integer")
			),

			"removeFormat" => array(
				"description" => "removing text format",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string", "integer")
			),

			"getFormat" => array(
				"description" => "return saved text format",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"saveNotes" => array(
				"description" => "saving notes",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"removeNotes" => array(
				"description" => "removing notes",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"getNotes" => array(
				"description" => "getting notes",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array(
							"boolean", "string", "string",
							array("string", "string", "string", "string", "string")
							)
			),

			"saveReview" => array(
				"description" => "saving review",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"updateReview" => array(
				"description" => "update review",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"getReview" => array(
				"description" => "getting review",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array(
							"boolean", "string", "string",
							array("string", "string", "string", "string", "string"),
							array("string", "string", "string")
							)
			),

			"getSearch" => array(
				"description" => "getting search",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getDescription" => array(
				"description" => "getting description",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getCopyright" => array(
				"description" => "getting copyright",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getTotalPageNum" => array(
				"description" => "getting total page num",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "string")
			),

			"getBookContentTLF" => array(
				"description" => "getting whole book content in TLF format",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")
			),

			"saveBookContentTLF" => array(
				"description" => "saving whole book content in TLF format",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"getPageContent" => array(
				"description" => "return saved text format",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array", "integer")	// describe the argument to be passed
			),

			"getPageArray" => array(
				"description" => "return saved text format",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array", "array")	// describe the argument to be passed
			),

			"sendMail" => array(
				"description" => "send mail",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"getBookCover" => array(
				"description" => "return saved bookcover page",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getAudio" => array(
				"description" => "getting audio",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getLogo" => array(
				"description" => "getting logo",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"testData" => array(
				"description" => "do a testing",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => "boolean"
			),

			"getBookSource" => array(
				"description" => "get book source",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getProgress" => array(
				"description" => "return user's reading progress",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"saveProgress" => array(
				"description" => "save user's reading progress",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			)
		);


		## 2015-09-11

		global $intranet_session_language;
		if ($intranet_session_language=="")
		{
			$intranet_session_language = "en";
		}

		include_once("../../global.php");
		//include_once("../../libdb.php");

		/*
		if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set))
		{
			if (in_array("b5",$intranet_default_lang_set))
			{
				$MessageCharset = "big5";
			}
			else
			{
				$MessageCharset = "GB2312";
			}
		}
		else
		{
			$MessageCharset = "big5";
		}
		*/

		//if(!defined("MESSAGECHARSET")) define("MESSAGECHARSET", $MessageCharset);
		//$this->MESSAGECHARSET = constant("MESSAGECHARSET");

		if(!isset($_SESSION['flash_book']['MESSAGECHARSET']))
			$_SESSION['flash_book']['MESSAGECHARSET'] = $MessageCharset;
		$this->MESSAGECHARSET = $_SESSION['flash_book']['MESSAGECHARSET'];

		//if(!defined("ECLASS_HTTPPATH")) define("ECLASS_HTTPPATH", $eclass_httppath);
		//$this->ECLASS_HTTPPATH = constant("ECLASS_HTTPPATH");

		if(!isset($_SESSION['flash_book']['ECLASS_HTTPPATH']))
			$_SESSION['flash_book']['ECLASS_HTTPPATH'] = $eclass_httppath;
		$this->ECLASS_HTTPPATH = $_SESSION['flash_book']['ECLASS_HTTPPATH'];

		//if(!defined("ECLASS_FILEPATH")) define("ECLASS_FILEPATH", $eclass_filepath);
		//$this->ECLASS_FILEPATH = constant("ECLASS_FILEPATH");

		if(!isset($_SESSION['flash_book']['ECLASS_FILEPATH']))
			$_SESSION['flash_book']['ECLASS_FILEPATH'] = $eclass_filepath;
		$this->ECLASS_FILEPATH = $_SESSION['flash_book']['ECLASS_FILEPATH'];

		//if(!defined("SERVER_IP")) define("SERVER_IP", $server_ip);
		//$this->SERVER_IP = constant("SERVER_IP");

		if(!isset($_SESSION['flash_book']['SERVER_IP']))
			$_SESSION['flash_book']['SERVER_IP'] = $server_ip;
		$this->SERVER_IP = $_SESSION['flash_book']['SERVER_IP'];

		//if(!defined("INTRANET_HTTPPATH")) define("INTRANET_HTTPPATH", $intranet_httppath);
		//$this->INTRANET_HTTPPATH = constant("INTRANET_HTTPPATH");

		if(!isset($_SESSION['flash_book']['INTRANET_HTTPPATH']))
			$_SESSION['flash_book']['INTRANET_HTTPPATH'] = $intranet_httppath;
		$this->INTRANET_HTTPPATH = $_SESSION['flash_book']['INTRANET_HTTPPATH'];

		//if(!defined("INTRANET_ROOT")) define("INTRANET_ROOT", $intranet_root);
		//$this->INTRANET_ROOT = constant("INTRANET_ROOT");

		if(!isset($_SESSION['flash_book']['INTRANET_ROOT']))
			$_SESSION['flash_book']['INTRANET_ROOT'] = $intranet_root;
		$this->INTRANET_ROOT = $_SESSION['flash_book']['INTRANET_ROOT'];

		//if(!defined("FILE_FOLDER")) define("FILE_FOLDER", "$intranet_root/files");
		//$this->FILE_FOLDER = constant("FILE_FOLDER");

		if(!isset($_SESSION['flash_book']['FILE_FOLDER']))
			$_SESSION['flash_book']['FILE_FOLDER'] = "$intranet_root/files";
		$this->FILE_FOLDER = $_SESSION['flash_book']['FILE_FOLDER'];

		//if(!defined("ECLASS_DB")) define("ECLASS_DB", $intranet_db);
		//$this->ECLASS_DB = constant("ECLASS_DB");
		//$this->db = constant("ECLASS_DB");

		if(!isset($_SESSION['flash_book']['ECLASS_DB']))
			$_SESSION['flash_book']['ECLASS_DB'] = $intranet_db;
		$this->ECLASS_DB = $_SESSION['flash_book']['ECLASS_DB'];
		$this->db = $_SESSION['flash_book']['ECLASS_DB'];

		//if(!defined("ECLASS_DB_LOGIN")) define("ECLASS_DB_LOGIN", $intranet_db_user);
		//$this->ECLASS_DB_LOGIN = constant("ECLASS_DB_LOGIN");

		if(!isset($_SESSION['flash_book']['ECLASS_DB_LOGIN']))
			$_SESSION['flash_book']['ECLASS_DB_LOGIN'] = $intranet_db_user;
		$this->ECLASS_DB_LOGIN = $_SESSION['flash_book']['ECLASS_DB_LOGIN'];

		//if(!defined("ECLASS_DB_PASSWD")) define("ECLASS_DB_PASSWD", $intranet_db_pass);
		//$this->ECLASS_DB_PASSWD = constant("ECLASS_DB_PASSWD");

		if(!isset($_SESSION['flash_book']['ECLASS_DB_PASSWD']))
			$_SESSION['flash_book']['ECLASS_DB_PASSWD'] = $intranet_db_pass;
		$this->ECLASS_DB_PASSWD = $_SESSION['flash_book']['ECLASS_DB_PASSWD'];

		//$new_sys_custom = $sys_custom['MySQL_Server_Host'];

		if(!isset($_SESSION['flash_book']['MySQL_Server_Host']))
			$_SESSION['flash_book']['MySQL_Server_Host'] = $sys_custom['MySQL_Server_Host'];
		$new_sys_custom = $_SESSION['flash_book']['MySQL_Server_Host'];

		$this->conn = mysql_pconnect($new_sys_custom ? $new_sys_custom : "localhost", $this->ECLASS_DB_LOGIN, $this->ECLASS_DB_PASSWD);
		mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");


		### 2015-09-11 END~~




		//$this->doConnectDB();

		return;
	}



	##########################* MAIN FUNCTIONS *#########################
    // function get total page num
    function getTotalPageNum($itemID){
	    $errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$data = "";

		$sql = "SELECT count(*)
				FROM
				INTRANET_ELIB_BOOK_PAGE
				WHERE
				BookID=	'".$itemData["book_id"]."'
				";

		$row = $this->getSqlResult($sql, 1);

		if (count($row)>0)
		{
			$data = $row[0][0];

			$success = true;
			$errCode = 0;				// no error
		}
		else
		{
			$success = false;
			$errCode = 1;				// Permission denied!
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "data"=>$data);
		return $r_arr;
	} // end function get total page number

	// function send mail
	function sendMail($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$Recipient = $data[0];
		$RecipientIDArr = $data[0];
		//$Subject = $data[1];
		//$PreMessage = $data[2];
		$Message = $data[1];
		$checkMyself = $data[2];

		$RecipientStr = $Recipient[0];
		for($i = 1; $i < count($Recipient); $i++)
		{
			$RecipientStr .= ",".$Recipient[$i];
		}

		if($checkMyself == true)
		{
			$userID = "U".$itemData["user_id"];
			$Recipient[count($Recipient)] = $userID;
		}

		if(count($Recipient) > 0)
		$ToMailList = $this->getUserEmail($Recipient);
		else
		$ToMailList = "";

		$PATH_WRT_ROOT = "../../../";
		include_once($PATH_WRT_ROOT."home/eLearning/elibrary/elib_mail.php");

		$eMail = new elib_mail();

		$Message = strip_tags($Message, '<p>');
		$Message = str_replace(' ALIGN="LEFT"', "", $Message);
		//$Message = str_replace('<P>', "<p>", $Message);
		//$Message = str_replace('</P>', "</p>", $Message);

		$lang = $_SESSION["intranet_session_language"];
		if($lang == "en"){
			$Subject = "Sharing eBook";
			$PreMessage = "Please click the following link to read the shared eBook.";
		}else{
			$Subject = "分享電子圖書";
			$PreMessage = "請點擊下列連結以閱讀所分享的電子圖書。";
			$PreMessage = addslashes($PreMessage); //handle strange big5 characters (i.e.?\) with monster codes
		}

		//$Subject = $eMail->intranet_htmlspecialchars(trim($Subject));
		//$PreMessage = $eMail->intranet_htmlspecialchars(trim($PreMessage));
		//$Message = $eMail->intranet_htmlspecialchars(trim($Message));

		/*
		if($this->MESSAGECHARSET == "GB2312")
		{
			//$Subject = iconv("UTF-8", "GB2312", $Subject);
			//$PreMessage = iconv("UTF-8", "GB2312", $PreMessage);
			$Message = iconv("UTF-8", "GB2312", $Message);
		}
		else
		{
			//$Subject = iconv("UTF-8", "big5", $Subject);
			//$PreMessage = iconv("UTF-8", "big5", $PreMessage);
			$Message = iconv("UTF-8", "big5", $Message);
		}
		*/
		$Message = addslashes($Message); //handle strange big5 characters with monster codes

		$UserInfo = $this->getUserInfo($itemData["user_id"]);
		$myMail = $UserInfo[0][1];
		$UserName = $UserInfo[0][2];

		$to = $ToMailList;
		$subject = $Subject;

		//prefer use $intranet_httppath rather than SERVER_IP
		if($this->INTRANET_HTTPPATH != ""){
		//if($intranet_httppath != ""){
			$LinkPath = "http://".$this->INTRANET_HTTPPATH."/home/elibrary/book_detail.php?BookID=".$itemData["book_id"];
		}else{
			if($this->SERVER_IP == "192.168.0.146"){ //port 52001 is required in the development site
				$LinkPath = "http://".$this->SERVER_IP.":31002"."/home/eLearning/elibrary/book_detail.php?BookID=".$itemData["book_id"];
			}else{
				$LinkPath = "http://".$this->SERVER_IP."/home/eLearning/elibrary/book_detail.php?BookID=".$itemData["book_id"];
			}
		}

		$body .= "<html><head>";
		$body .= "</head>";
		$body .= "<body>";
		$body .= "<p>$PreMessage</p>";
		$body .= "<p><a href=\"$LinkPath\">";
		$body .= $LinkPath;
		$body .= "</a></p>";
		$body .= $Message;
		$body .= "</body></html>";

		// To send HTML mail, the Content-type header must be set
		$headers  .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset='.$this->MESSAGECHARSET." \r\n";
		// Additional headers
		$headers .= 'To: '.$ToMailList."\r\n";
		$headers .= 'From: '.$UserName.' <'.$myMail.'>'."\r\n";

		// send mail to external email
		//$errMsg = $eMail->SendMail($to, $subject, $body, $headers);

		$ParArr = array();
		$ParArr["UserID"] = $itemData["user_id"];

		$ParArr["RecipientID"] = $RecipientStr;
		$ParArr["RecipientIDArr"] = $RecipientIDArr;

		if($checkMyself == true)
		$ParArr["InternalCCID"] = "U".$itemData["user_id"];

		$ParArr["Subject"] = $subject;
		$ParArr["Message"] = $body;
		$ParArr["t_encoding"] = $this->MESSAGECHARSET;

		$success = $this->SendMail_Intranet($ParArr);

		//$errMsg = $to."\r\n".$headers."\r\n".$subject."\r\n".$body;
		$errMsg = $ParArr;

		if (!$success)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$errMsg);
		return $r_arr;
	} // end function send mail

	function SendMail_Intranet($ParArr="")
	{
		$errCode = 0;
		$theUserID = $ParArr["UserID"];
		$RecipientID = $ParArr["RecipientID"];
		$InternalCCID = $ParArr["InternalCCID"];
		$InternalBCCID = $ParArr["InternalBCCID"];
		$to_address_list = $ParArr["to_address_list"];
		$cc_address_list = $ParArr["cc_address_list"];
		$bcc_address_list = $ParArr["bcc_address_list"];
		$Subject = $ParArr["Subject"];
		$Message = $ParArr["Message"];
		$t_encoding = $ParArr["t_encoding"];
		$Attachment = $ParArr["Attachment"];
		$IsAttachment = $ParArr["IsAttachment"];
		$IsImportant = $ParArr["IsImportant"];
		$IsNotification = $ParArr["IsNotification"];
		$RecordType = $ParArr["RecordType"];
		$size = $ParArr["size"];
		$isHTML = $ParArr["isHTML"];

		$sql = "
		INSERT INTO INTRANET_CAMPUSMAIL (
		UserID, SenderID, RecipientID,InternalCC,InternalBCC,
		ExternalTo,ExternalCC,ExternalBCC,
		Subject, Message,".($t_encoding!=""?"MessageEncoding,":"")."Attachment,
		IsAttachment, IsImportant, IsNotification,
		MailType,UserFolderID,
		RecordType, DateInput, DateModified, AttachmentSize,isHTML
		)
		VALUES (
		$theUserID, $theUserID, '$RecipientID','$InternalCCID','$InternalBCCID',
		'$to_address_list','$cc_address_list','$bcc_address_list',
		'$Subject', '$Message',".($t_encoding!=""?"'$t_encoding',":"")."'$Attachment',
		'$IsAttachment', '$IsImportant', '$IsNotification',
		1,'$RecordType',
		'$RecordType', now(), now(), '$size','$isHTML'
		)";

		$success = $this->doQuery($sql);
		$CampusMailFromID = $this->getSqlInsertID();
		//////////////////////////////////////////////////////////////////////////////
		/////////// for other receipient /////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		$actual_receivers_list = implode(",", $ParArr["RecipientIDArr"]);
		$actual_recipient_array = $this->returnRecipientUserIDArrayWithQuota($actual_receivers_list);

		//return $actual_receivers_list;
		//return $actual_recipient_array;
		//$actual_recipient_array = $ParArr["RecipientIDArr"];

		$campusmail_sql_header = "
		INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, SenderID,
		RecipientID,InternalCC, Subject, Message, MessageEncoding,
		RecordType, MailType, UserFolderID,
		DateInput, DateModified) VALUES ";

		$sql = $campusmail_sql_header;
		$row = $actual_recipient_array;
		$replies_values = "";
		$delimiter = "";
		$users_no_space = array();
		$users_count = 0;

		/*
		//get all user's quota
		$ReceiversList = 0;
		for($i=0; $i<sizeof($row); $i++)
		{
			$ReceiversList .= ",".$row[$i][0];
		}


		$sql_quota = "SELECT UserID, SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID IN ($ReceiversList) GROUP BY UserID";
		$row_quota = $li->returnArray($sql_quota, 2);
		for ($i=0; $i<sizeof($row_quota); $i++)
		{
			$user_quota_used[$row_quota[$i][0]] = (int)$row_quota[$i][1];
		}
		*/

		for($i=0; $i<sizeof($row); $i++)
		{
			$ReceiverID = $row[$i][0];
			$ReceiverName = $row[$i][1];
			$ReceiverQuota = $row[$i][2];
		/*
		$left = (is_integer($user_quota_used[$ReceiverID])) ? $ReceiverQuota*1024 - $user_quota_used[$ReceiverID] : $ReceiverQuota*1024;

		if ($size > $left)
		{
			$users_no_space[$users_count] = $ReceiverID;
			$users_count++;
			continue;
		}
		*/

		# CampusMailFromID, UserID, RecordType only
		$sql .= "$delimiter($CampusMailFromID, $ReceiverID, $theUserID, '$RecipientID', '$InternalCCID', '$Subject', '$Message', '$t_encoding', '2' , '1', '2', now(), now())";
		$delimiter = ",";
		} // end for loop $receipient ID arr
		$success = $this->doQuery($sql);

		if($InternalCCID != "")
		{
			$TmpInternalCCID = str_replace("U", "", $InternalCCID);
			$sql = "
			INSERT INTO INTRANET_CAMPUSMAIL (CampusMailFromID, UserID, SenderID,
			RecipientID,InternalCC, Subject, Message, MessageEncoding,
			RecordType, MailType, UserFolderID,
			DateInput, DateModified) VALUES ";

			$sql .= "($CampusMailFromID, $TmpInternalCCID, $theUserID, '$RecipientID', '$InternalCCID', '$Subject', '$Message', '$t_encoding', '2', '1', '2', now(), now())";
			$success = $this->doQuery($sql);
		}

		return $success;
		//return $sql;
	} // end function IntranetSendMail


	function returnAccessList()
	{
		$intranet_root = $this->INTRANET_ROOT;
		$file_content = $this->get_file_content ("$intranet_root/file/campusmail_set.txt");
		if ($file_content == "") return array(1,2,3);
		$content = explode("\n",$file_content);
		# Get row 1,2,4 only. Compatible with previous version
		if ($content[0][0]==1) $permitted[] = 1;
		if ($content[1][0]==1) $permitted[] = 2;
		if ($content[3][0]==1) $permitted[] = 3;
		return $permitted;
	} // end function returnAccessList

	function get_file_content($file)
	{
         clearstatcache();
        if(file_exists($file) && is_file($file) && filesize($file)!=0){
                $x =  ($fd = fopen($file, "r")) ? fread($fd,filesize($file)) : "";
                if ($fd)
                    fclose ($fd);
        }
        return $x;
	}

	function getNameFieldWithClassNumberEng ($prefix="")
	{
		$username_field = $this->getNameFieldEng($prefix);
		$field = "CONCAT($username_field,IF($prefix"."ClassNumber IS NULL OR $prefix"."ClassNumber = '','',CONCAT(' (',$prefix"."ClassName,'-',$prefix"."ClassNumber,')')))";
		return $field;
	}

	function getNameFieldEng($prefix="")
	{
         $name_field = $this->getNameFieldEng2($prefix);
         $title_field = "CASE $prefix"."Title
                         WHEN 0 THEN ' Mr. '
                         WHEN 1 THEN ' Miss '
                         WHEN 2 THEN ' Mrs. '
                         WHEN 3 THEN ' Ms. '
                         WHEN 4 THEN ' Dr. '
                         WHEN 5 THEN ' Prof. '
                         ELSE '' END
                         ";
         $field = "TRIM(CONCAT(IF($prefix"."RecordType=1 OR $prefix"."RecordType=3,$title_field,''),$name_field))";
         return $field;
	}

	function getNameFieldEng2 ($prefix ="")
	{
		$username_field = "TRIM($prefix"."EnglishName)";
		return $username_field;
	}

	function array_union($a1,$a2)
	{
         $result = array_merge($a1,$a2);
         $result = array_unique($result);
         $result = array_values($result);
         return $result;
	}

	function returnVector($sql)
	{
		$i = 0;
		$x = array();
		$this->rs = $this->db_db_query($sql);

		if($this->rs && $this->db_num_rows()!=0)
		{
			while($row = $this->db_fetch_array())
			{
				$x[$i] = $row[0];
				$i++;
			}
		}

		if ($this->rs)
		$this->db_free_result();

		return $x;
	}

	function db_free_result()
	{
		return mysql_free_result($this->rs);
	}

     function db_db_query($query)
     {
                mysql_select_db($this->db) or exit(mysql_error());
                return mysql_query($query);

//                return mysql_db_query($this->db, $query);
      }

      function db_num_rows()
      {
		return mysql_num_rows($this->rs);
      }

      function db_fetch_array()
      {
		return mysql_fetch_array($this->rs);
      }

	function returnRecipientUserIDArrayWithQuota($Recipient)
	{

		$row = $this->returnRecipientIDArray($Recipient);

		$accessList = implode(",", $this->returnAccessList());

		$GroupIDList = $row[0];
		$UserIDsList = $row[1];
		$ParentGroupIDList = $row[2];
		$ParentStudentIDList = $row[3];

		# Group
		$username_field = $this->getNameFieldWithClassNumberEng("a.");

          if ($GroupIDList!="")
          {
              $groupList = $GroupIDList;
              $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                      FROM INTRANET_USERGROUP as b
                           LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.UserID
                           LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                      WHERE a.RecordType IN ($accessList)
                      AND b.GroupID IN ($groupList)";
			$groupNames = $this->returnVector($sql);
          }
          else
          {
              $groupNames = array();
          }

          if ($UserIDsList!="")
          {
              $userList = $UserIDsList;

              $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                      FROM INTRANET_USER as a
                           LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                      WHERE a.RecordType IN ($accessList)
                      AND a.UserID IN ($userList)";
              $userNames = $this->returnVector($sql);
          }
          else
          {
              $userNames = array();
          }
          $pos = 1;


          if ($ParentGroupIDList!="")
          {
              # Get Student List from Group first
              $sql = "SELECT DISTINCT a.UserID FROM INTRANET_USERGROUP as b
                             LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.UserID
                             WHERE b.GroupID IN ($ParentGroupIDList)
                                   AND a.RecordType = 2";
              $temp = $this->returnVector($sql);

              # Get Parent List from Student List
              if (sizeof($temp)!=0)
              {
                  $targetStudentList = implode(",",$temp);
                  $sql = "SELECT DISTINCT a.UserID FROM INTRANET_PARENTRELATION as b
                                 LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.ParentID
                                 WHERE b.StudentID IN ($targetStudentList)
                                       AND a.RecordType = 3";
                  $temp = $this->returnVector($sql);
                  if (sizeof($temp)!=0)
                  {
                      # Get Name and Quota for the list
                      $list = implode(",",$temp);
                      $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                                     FROM INTRANET_USER as a
                                     LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                                     WHERE a.RecordType IN ($accessList)
                                           AND a.UserID IN ($list)";
                      $parentGroupNames = $this->returnVector($sql);
                  }
                  else
                  {
                      $parentGroupNames = array();
                  }
              }
              else
              {
                  $parentGroupNames = array();
              }
          }
          else
          {
              $parentGroupNames = array();
          }
          if ($ParentStudentIDList!="")
          {
                  $sql = "SELECT DISTINCT a.UserID FROM INTRANET_PARENTRELATION as b
                                 LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.ParentID
                                 WHERE b.StudentID IN ($ParentStudentIDList)
                                       AND a.RecordType = 3";
                  $temp = $this->returnVector($sql);
                  if (sizeof($temp)!=0)
                  {
                      # Get Name and Quota for the list
                      $list = implode(",",$temp);
                      $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                                     FROM INTRANET_USER as a
                                     LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                                     WHERE a.RecordType IN ($accessList)
                                           AND a.UserID IN ($list)";
                      $parentStudentNames = $this->returnVector($sql);
                  }
                  else
                  {
                      $parentStudentNames = array();
                  }

          }
          else
          {
              $parentStudentNames = array();
          }

          if (sizeof($userNames)==0 && sizeof($groupNames)==0 && sizeof($parentGroupNames)==0 && sizeof($parentStudentNames)==0) return array();
          #if ($UserIDsList=="" && $GroupIDList=="") return array();

          # Take Union of arrays
          $overall = $this->array_union($userNames, $groupNames);
          $overall = $this->array_union($overall, $parentGroupNames);
          $overall = $this->array_union($overall, $parentStudentNames);

          # Separate UserIDs and Names
          while (list ($key, $val) = each ($overall))
          {
                 $row = explode(":::",$val);
                 $result[] = $row;
          }
          return $result;
     } // end function returnRecipientUserIDArrayWithQuota

	function returnRecipientIDArray($Recipient)
	{
          $GroupIDList = "";
          $UserIDsList = "";
          $ParentStudentIDsList = "";
          $ParentGroupIDsList = "";
          $group_delimiter = "";
          $user_delimiter = "";
          $parent_student_delim = "";
          $parent_group_delim = "";
          $row = explode(",",$Recipient);
          for($i=0; $i<sizeof($row); $i++){
               $RecipientType = substr($row[$i],0,1);
               $RecipientID = substr($row[$i],1);
               if($RecipientType=="G")    # Group
               {
                  $GroupIDList .= $group_delimiter.$RecipientID;
                  $group_delimiter = ",";
               }
               else if ($RecipientType=="Q")       # Parents of the students in group
               {
                    $ParentGroupIDsList .= $parent_group_delim.$RecipientID;
                    $parent_group_delim = ",";
               }
               else if ($RecipientType=="P")    # Parents of the students
               {
                    $ParentStudentIDsList .= $parent_student_delim.$RecipientID;
                    $parent_student_delim = ",";
               }
               else # Target User
               {
                   $UserIDsList .= $user_delimiter.$RecipientID;
                   $user_delimiter = ",";
               }
          }
          $x[0] = ($GroupIDList == ""? 0:$GroupIDList);
          $x[1] = ($UserIDsList == ""? 0:$UserIDsList);
          $x[2] = ($ParentGroupIDsList == ""? 0:$ParentGroupIDsList);
          $x[3] = ($ParentStudentIDsList == ""? 0:$ParentStudentIDsList);
          return $x;
	} // end function returnRecipientID Array


	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////			End Get iMail		///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	function getUserEmail($RecipientArr="")
	{
		for($i = 0; $i < count($RecipientArr); $i++)
		{
			$RecipientArr[$i] = str_replace("U", "", $RecipientArr[$i]);
		}

		$con = "UserID = '".$RecipientArr[0]."'";
		for($i = 1; $i < count($RecipientArr); $i++)
		{
			$con .= " OR UserID = '".$RecipientArr[$i]."'";
		}

		$sql = "
		SELECT
		UserID, UserEmail, EnglishName
		FROM
		INTRANET_USER
		WHERE
		$con
		";

		$ReturnArr = $this->getSqlResult($sql, 3);

		$ToMailList = "";

		$ToMailList =  $ReturnArr[0][2]." <".$ReturnArr[0][1].">";

		for($i = 1; $i < count($ReturnArr); $i++)
		{
			$ToMailList .=  " , ".$ReturnArr[$i][2]." <".$ReturnArr[$i][1].">";
		}

		return $ToMailList;
	} // end function get user email

	function getUserInfo($theUserID="")
	{
		$con = "UserID = '".$theUserID."'";

		$sql = "
		SELECT
		UserID, UserEmail, EnglishName
		FROM
		INTRANET_USER
		WHERE
		$con
		";

		$ReturnArr = $this->getSqlResult($sql, 3);
		return $ReturnArr;
	} // end function get user info


	function getLoad($itemID){

		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$data = "";

		if ($this->view_right)
		{
			if ($itemData["powerconcept_id"])
			{
				$sql = "SELECT DataContent
						FROM powerconcept
						WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
				$row = $this->getSqlResult($sql, 1);
				$data = unserialize($row[0][0]);
			} elseif ($itemData["file_path"]!="")
			{
				if (is_file($itemData["file_path"]))
				{
					clearstatcache();
					if ($fd = fopen($itemData["file_path"], "r"))
					{
						$data = $this->dataDecrypt(fread($fd, filesize($itemData["file_path"])),$this->encrypt_key);
						$data = unserialize($data);
					}
					fclose ($fd);
				}
			}
			$success = true;
			$errCode = 0;				// no error
		} else
		{
			$success = false;
			$errCode = 1;				// Permission denied!
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "data"=>$data);
		return $r_arr;
	}


	function sendSave($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$errCode = 0;					// no error
		if (!$_SESSION["pc_block_save"])
		{
			$success = false;
			$errCode = 0;
			$data = serialize($data);

			if ($itemData["powerconcept_id"])
			{
				# load the writing
				$sql = "UPDATE powerconcept
						SET DataContent='".addslashes($data)."', ModifiedDate=now()
						WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
				$success = $this->doQuery($sql);
				if (!$success)
				{
					$errCode = 2;		// Failed to save due to unkown reason, please try again.
				}
			} elseif ($itemData["file_id"] || $itemData["is_file_new"])
			{
				# save to temporary file named "tmp/1/powerconcept.epc"
				# when the file is saved, copy the temporart file to be the real file
				clearstatcache();
				$data = $this->dataEncrypt($data,$this->encrypt_key);
				$success = (($fd = fopen($itemData["file_path"], "w")) && (fputs($fd, $data))) ? true : false;
				fclose ($fd);
				if (!$success)
				{
					$errCode = 2;		// Failed to save due to unkown reason, please try again.
				} elseif ($itemData["file_id"]>0)
				{
					# update file size in DB
					$FileSize = (file_exists($itemData["file_path"])) ? ceil((filesize($itemData["file_path"]))/1024) : "1";
					if ($FileSize<1)
					{
						$FileSize = 1;
					}
					$sql = "UPDATE eclass_file SET size='$FileSize' WHERE fileID='".$itemData["file_id"]."' ";
					$this->doQuery($sql);
				}
			} else
			{
				$errCode = 1;			// Permission denied!
			}
		} else
		{
			$errCode = 1;				// Permission denied!
			$success = false;
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]);
		return $r_arr;
	}

	function getBookmark($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						PageID
					FROM
						INTRANET_ELIB_USER_BOOKMARK
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
					ORDER BY
						PageID ASC
				";

		$row = $this->getSqlResult($sql, 1);


		if (count($row)>0)
		{
			$data = $row;

			$success = true;
			$errCode = 0;				// no error
		}
		else
		{
			$success = false;
			$errCode = 1;				// Permission denied!
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$data);
		return $r_arr;
	}

	function addBookmark($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		/*
		$sql = "
					SELECT
						BookmarkID
					FROM
						INTRANET_ELIB_USER_BOOKMARK
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
				";

		$row = $this->getSqlResult($sql, 1);

		if (count($row)>0)
		{
			# load the writing
			$sql = "
						UPDATE
							INTRANET_ELIB_USER_BOOKMARK
						SET
							PageID='".addslashes($data)."', DateModified=now()
						WHERE
							BookID=	'".$itemData["book_id"]."'
							AND UserID='".$itemData["user_id"]."'
					 ";
			$success = $this->doQuery($sql);
			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.
			}
		}
		else
		{
		*/

			# load the writing
			$sql = "
						INSERT INTO
						INTRANET_ELIB_USER_BOOKMARK
						(BookID,UserID,PageID,DateModified)
						VALUES
							('".$itemData["book_id"]."','".$itemData["user_id"]."','".addslashes($data)."',now())
					 ";
			$success = $this->doQuery($sql);
			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.

			}
		/*}*/

		//$success = true;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql);

		return $r_arr;
	}

	function deleteBookmark($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					DELETE FROM
					INTRANET_ELIB_USER_BOOKMARK
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
						AND PageID='".addslashes($data)."'
				";
		$success = $this->doQuery($sql);
		if (!$success)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.

		}

		//$success = true;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql);

		return $r_arr;
	}

	function getFormat($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						PageID,StartIndex,EndIndex,FormatType,FormatValue
					FROM
						INTRANET_ELIB_USER_FORMAT
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
						AND PageID='".addslashes($data[0])."'
					ORDER BY
						DateModified
				";

		$row = $this->getSqlResult($sql, 5);

		if (count($row)>0)
		{
			$dataOutput[0] = $row;
			$dataOutput[1] = $data[1];
			$dataOutput[2] = $data[0];

			$success = true;
			$errCode = 0;				// no error
		}
		else
		{
			$dataOutput[0] = "null";
			$dataOutput[1] = $data[1];
			$dataOutput[2] = $data[0];

			$success = false;
			$errCode = 1;				// Permission denied!
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$dataOutput);
		return $r_arr;
	}


	//Data array = (Page, Start Index, End Index, Format Type, Format Value)

	function saveFormat($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);


		# load the writing
		$sql = "
						INSERT INTO
						INTRANET_ELIB_USER_FORMAT
						(BookID,UserID,PageID,StartIndex,EndIndex,FormatType,FormatValue,DateModified)
					VALUES
						('".$itemData["book_id"]."','".$itemData["user_id"]."','".addslashes($data[0])."','".addslashes($data[1])."'
						,'".addslashes($data[2])."','".addslashes($data[3])."','".addslashes($data[4])."',now())
				 ";
		$success = $this->doQuery($sql);
		if (!$success)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.
		}

		//$success = true;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "pageID"=>$data[0]);

		return $r_arr;
	}

	function removeFormat($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);


		# load the writing
		$sql = "
					DELETE FROM
						INTRANET_ELIB_USER_FORMAT
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
						AND PageID='".$data[0]."'
						AND StartIndex='".$data[1]."'
						AND EndIndex='".$data[2]."'
						AND FormatType='".$data[3]."'
						AND FormatValue='".$data[4]."'
				";
		$success = $this->doQuery($sql);
		if (!$success)
		{
			$errCode = 1;
		}

		//$success = true;
		$r_arr = array("success"=>$success, "errCode"=>$errCode." ".$sql, "errMsg"=>$this->errMsg[$errCode], "pageID"=>$data[0]);

		return $r_arr;
	}

	function saveNotes($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		if (($data[0] != "") && ($data[0] != -1))
		{
			# load the writing
			$sql = "
						UPDATE
							INTRANET_ELIB_USER_MYNOTES
						SET
							NoteType='".addslashes($data[1])."',
							PageID='".addslashes($data[2])."',
							Content='".addslashes($data[3])."',
							Category='".addslashes($data[4])."',
							DateModified=now()
						WHERE
							BookID=	'".$itemData["book_id"]."'
							AND UserID='".$itemData["user_id"]."'
							AND NoteID='".$data[0]."'
					 ";
			$success = $this->doQuery($sql);

			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.
			}
		}
		else
		{
			# load the writing
			$sql = "
							INSERT INTO
							INTRANET_ELIB_USER_MYNOTES
							(BookID,UserID,NoteType,PageID,Content,Category,DateModified)
						VALUES
							('".$itemData["book_id"]."','".$itemData["user_id"]."','".addslashes($data[1])."'
							,'".addslashes($data[2])."','".addslashes($data[3])."','".addslashes($data[4])."',now())
					 ";
			$success = $this->doQuery($sql);
			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.
			}
		}

		//$success = true;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]);

		return $r_arr;
	} // end function save notes

	// removing notes
	function  removeNotes($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		if (($data[0] != "") && ($data[0] >= 0))
		{
			$sql = "
						DELETE FROM
							INTRANET_ELIB_USER_MYNOTES
						WHERE
							BookID=	'".$itemData["book_id"]."'
							AND UserID='".$itemData["user_id"]."'
							AND NoteID='".$data[0]."'
					 ";

			$success = $this->doQuery($sql);

			if (!$success)
			{
				$errCode = 1;
			}
		}
		else
		{
			$success = false;
			$errCode = 1;
		}


		$r_arr = array("success"=>$success, "errCode"=>$errCode." ".$sql, "errMsg"=>$this->errMsg[$errCode]);
		return $r_arr;
	} // end function remove notes

	// getting notes
	function getNotes($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						NoteID, NoteType, PageID, Content, Category, DateModified
					FROM
						INTRANET_ELIB_USER_MYNOTES
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
					ORDER BY
						NoteID DESC

					 ";

		$success = $this->getSqlResult($sql, 6);

		if (!$success)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.
		}
		else
		{
			$data = $success;
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$data);
		return $r_arr;
	} // end function get notes

	// saving review
	function saveReview($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

			$sql = "
						INSERT INTO
							INTRANET_ELIB_BOOK_REVIEW
							(BookID,UserID,Rating,Content,DateModified)
						VALUES
							('".$itemData["book_id"]."','".$itemData["user_id"]."','".addslashes($data[1])."'
							,'".addslashes($data[2])."', now())
					 ";
			$success = $this->doQuery($sql);

			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.
			}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]);
		return $r_arr;
	} // end function save review

	// update review
	function updateReview($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);
		$success = false;

		if (($data[0] != "") && ($data[0] >= 0))
		{
			$sql = "
						INSERT INTO
							INTRANET_ELIB_BOOK_REVIEW_HELPFUL
							(ReviewID,BookID,UserID,choose,DateModified)
						VALUES
							('".addslashes($data[0])."','".$itemData["book_id"]."','".$itemData["user_id"]."'
							,'".addslashes($data[1])."', now())
					 ";
			$success = $this->doQuery($sql);
		}

		if (!$success)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]);
		return $r_arr;
	} // end function update review


	// getting review
	function getReview($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						a.ReviewID, a.Rating, a.Content, a.DateModified,
						b.ChineseName, b.FirstName, b.LastName, a.UserID
					FROM
						INTRANET_ELIB_BOOK_REVIEW a
					LEFT JOIN
					INTRANET_USER b
					ON
					b.UserID = a.UserID
					WHERE
						a.BookID='".$itemData["book_id"]."'
					 ";

		$row = $this->getSqlResult($sql, 8);


		if (count($row)<=0)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.
			$success = false;
		}
		else
		{
			$data = $row;
			$success = true;

			// encoding the chinese name
			/*
			for($i = 0; $i < count($data); $i++)
			{
				//$data[$i][2] = iconv("BIG-5", "UTF-8", $data[$i][2]);
				$data[$i][4] = iconv("BIG-5", "UTF-8", $data[$i][4]);
			}
			*/
		}

		// select helpful
		$sql2 = "
					SELECT
						ReviewID,UserID,Choose
					FROM
						INTRANET_ELIB_BOOK_REVIEW_HELPFUL
					WHERE
						BookID='".$itemData["book_id"]."'
					 ";

		$row2 = $this->getSqlResult($sql2, 3);

		if (count($row2)<=0)
		{
			$errCode = 2;		// Failed to save due to unkown reason, please try again.
		}
		else
		{
			$data2 = $row2;
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$data, "data2"=>$data2);
		return $r_arr;
	} // end function get review

	// getting search
	function getSearch($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		// $data[0], keyword str
		// $data[1], check content
		// $data[2], check notes

		$search_str = addslashes($data[0]);
		$IsCheckContent = $data[1];
		$IsCheckNotes = $data[2];

		$ReturnArr2 = array();
		if ($IsCheckNotes)
		{
			// search the notes which contain keywords
			$sql1 = "
						SELECT
							NoteType,
							PageID,
							Content,
							NoteID
						FROM
							INTRANET_ELIB_USER_MYNOTES
						WHERE
							BookID='".$itemData["book_id"]."'
							AND UserID='".$itemData["user_id"]."'
							AND Content LIKE '%".$search_str."%'
						ORDER BY
							PageID
						 ";

			$ReturnArr1 = $this->getSqlResult($sql1, 4);

			for ($i=0;$i<count($ReturnArr1);$i++)
			{
				$NoteType = $ReturnArr1[$i][0];
				$PageID = $ReturnArr1[$i][1];
				$Content = $ReturnArr1[$i][2];
				$NoteID = $ReturnArr1[$i][3];

				if (substr($ReturnArr1[$i][0],0,1) == "n")
				{
					//$Tmp1 = $NoteType.$PageID;
					$Tmp1 = $NoteType;
					$Tmp2 = "";
					//$Tmp3 = $Content;
					$Tmp3 = "...".$this->GetStringPart($Content, $search_str)."...";
					$Tmp4 = $NoteID;

					$ReturnArr2[] = array($Tmp1,$Tmp3,$Tmp4);
				}
				else if ($ReturnArr1[$i][0] == "na")
				{
					$Tmp1 = $NoteType;
					$Tmp2 = "";
					//$Tmp3 = $Content;
					$Tmp3 = "...".$this->GetStringPart($Content, $search_str)."...";
					$Tmp4 = $NoteID;

					$ReturnArr2[] = array($Tmp1,$Tmp3,$Tmp4);
				}
			}
		}


		if ($IsCheckContent)
		{
			// search the page which contain keywords
			$sql = "
						SELECT
							PageType,
							PageID,
							Content,
							BookPageID
						FROM
							INTRANET_ELIB_BOOK_PAGE
						WHERE
							BookID='".$itemData["book_id"]."'
							AND Content LIKE '%".$search_str."%'
						ORDER BY
							PageID
						 ";

			$ReturnArr1 = $this->getSqlResult($sql, 4);

			for ($i=0;$i<count($ReturnArr1);$i++)
			{
				$PageType = $ReturnArr1[$i][0];
				$PageID = $ReturnArr1[$i][1];
				$Content = str_replace("\n","",$ReturnArr1[$i][2]);
				$BookPageID = $ReturnArr1[$i][3];

				$Tmp1 = "c".$PageID;
				$Tmp2 = "";
				//$Tmp3 = $Content;
				$Tmp3 =  "...".$this->GetStringPart($Content, $search_str)."...";
				$Tmp4 = $BookPageID;

				$ReturnArr2[] = array($Tmp1,$Tmp3,$Tmp4);
			}
		}

		$success = true;
		$errCode = 0;				// no error

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql1, "data"=>$ReturnArr2);

		return $r_arr;

	} // end get search

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ADDED by Adam at 2010 Jan 20th /////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get and Save whole book content in Text Layout Framework xml format ////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// getting book contentTLF
	function getBookContentTLF($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						ContentTLF
					FROM
						INTRANET_ELIB_BOOK
					WHERE
						BookID=	'".$itemData["book_id"]."'
			   ";

		$row = $this->getSqlResult($sql, 1);

		if (count($row)>0)
		{
			$data = $row;

			$success = true;
			$errCode = 0;				// no error
		}
		else
		{
			$success = false;
			$errCode = 1;				// Permission denied!
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$data);
		return $r_arr;
	} // end function get book content TLF

	// saving book content TLF
	function saveBookContentTLF($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

			$sql = "
						UPDATE
							INTRANET_ELIB_BOOK
						SET
							ContentTLF='".addslashes($data[0])."',
							DateModified=now()
						WHERE
							BookID=	'".$itemData["book_id"]."'
					 ";

			$success = $this->doQuery($sql);

			if (!$success) {
				$errCode = 2;		// Failed to save due to unkown reason, please try again.
			}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." / sql: ".$sql);
		return $r_arr;
	} // end function save book content TLF
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	function getPageContent($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						ChapterID, IsChapterStart, PageType, OrigPageID, PdfID, Content, ContentXML
					FROM
						INTRANET_ELIB_BOOK_PAGE
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND PageID='".addslashes($data)."'
					ORDER BY
						DateModified
				";

		$row = $this->getSqlResult($sql, 7);

		if (count($row)>0)
		{
			$TmpChapter = $row[0][0];

			$lineArr1 = array();
			//$lineArr1 = explode("\n",$row[0][5]);
			$TmpArr1 = array();
			$TmpLine = "";
			//Adam: as the flash file don't use $TmpLineHTML at this moment, thus don't send back it now
			//$TmpLineHTML = "";

			$pdfid = $row[0][4];
			if($pdfid != -1 && $pdfid != NULL){
				$TmpLine = $itemData["folder_path"]."/file/elibrary/content/".$itemData["book_id"]."/pdf/".$pdfid.".swf";
				$TmpArr1[] = array($TmpLine,"pdf");
				$TmpLine = "";
			}else{
				$tmpXml = $row[0][6];
				//$tmpXml = html_entity_decode($row[0][6]);
				$xmlContent = str_replace("</p>", "</p>\n", $tmpXml);
				$lineArr1 = explode("\n",$xmlContent);

				for ($i=0;$i<count($lineArr1);$i++)
				{
					if (substr(trim($lineArr1[$i]),18,strlen("<img")) == "<img")
					{
						if ($TmpLine != "")
						{
							//$TmpArr1[] = array($TmpLine,"text",$TmpLineHTML);
							$TmpArr1[] = array($TmpLine,"text");
						}

						$pos2 = strpos(trim($lineArr1[$i]),"\"",strlen("<img src=\"")+1+20);
						$TmpLine = substr(trim($lineArr1[$i]),strlen("<img src=\"")+18,$pos2-strlen("<img src=\"")-18);
						$TmpLine = $itemData["folder_path"]."/file/elibrary/content/".$itemData["book_id"]."/".$TmpLine;
						//$TmpLineHTML = trim($lineArr1[$i]);

						//$TmpArr1[] = array($TmpLine,"image",$TmpLineHTML);
						$TmpArr1[] = array($TmpLine,"image");

						$TmpLine = "";
						//$TmpLineHTML = "";
					}
					else
					{
						$TmpLine .= $lineArr1[$i]."\n";
						//$TmpLineHTML .= $lineArr1[$i]."<br />";
					}
				}
				//$TmpArr1[] = array($TmpLine,"text",$TmpLineHTML);
				$TmpArr1[] = array($TmpLine,"text");
			}

			$success = true;
			$errCode = 0;				// no error
		}
		else
		{
			$success = false;
			$errCode = 3;				// Permission denied!
		}

		//$ReturnArray = array($row[0][5], $row[0][6]);
		//Adam: as the flash file don't use $lineArr1 at this moment, thus don't send back it now
		//$ReturnArray = array($TmpArr1,$lineArr1);
		$ReturnArray = array($TmpArr1,"disable lineArr1");
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$ReturnArray, "PageID"=>$data);
		return $r_arr;
	} // end function get page content

	function getPageArray($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		// get page arr
		$sql = "
					SELECT
						BookPageID, PageID, PageType, OrigPageID, ChapterID, SoundStartCue, SoundEndCue
					FROM
						INTRANET_ELIB_BOOK_PAGE
					WHERE
						BookID=	'".$itemData["book_id"]."'
					ORDER BY
						PageID
				";

		$row = $this->getSqlResult($sql, 7);

		// get table of content index
		$sql2 = "
					SELECT
						BookChapterID, ChapterID, SubChapterID, Title, OrigPageID, IsChapter
					FROM
						INTRANET_ELIB_BOOK_CHAPTER
					WHERE
						BookID=	'".$itemData["book_id"]."'
					ORDER BY
						OrigPageID ASC, SubChapterID ASC, IsChapter DESC
				";

		$row2 = $this->getSqlResult($sql2, 6);


		$ReturnArr = $row;
		$ReturnArr2 = $row2;

		$errCode = 0;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$ReturnArr, "data2"=>$ReturnArr2);
		return $r_arr;
	} // end function get page array

	function getDescription($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						b.Title, a.Author, b.Category, b.Publisher, b.DateModified, b.Preface, b.Source, b.Level, b.SeriesEditor, b.SubCategory
					FROM
						INTRANET_ELIB_BOOK_AUTHOR a, INTRANET_ELIB_BOOK b
					WHERE
						b.BookID= '".$itemData["book_id"]."'
						AND a.AuthorID = b.AuthorID
				";

		$row = $this->getSqlResult($sql, 10);

		$ReturnArr = $row;

		if($row != null)
		$success = true;
		else
		$success = false;

		$errCode = 0;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$ReturnArr);
		return $r_arr;
	} // end function get description

	function getCopyright($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						a.Copyright
					FROM
						INTRANET_ELIB_BOOK a
					WHERE
						a.BookID= '".$itemData["book_id"]."'
				";

		$row = $this->getSqlResult($sql, 1);

		$ReturnArr = $row;

		if($row != null)
		{
			$success = true;
			// replace the number code
			$TmpCopyright = $ReturnArr[0];
			$TmpCopyright = str_replace("<b>[JS1]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[JS2]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[JS3]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[JS4]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[JS5]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[/JS1]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[/JS2]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[/JS3]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[/JS4]</b>",  "", $TmpCopyright);
			$TmpCopyright = str_replace("<b>[/JS5]</b>",  "", $TmpCopyright);
			$ReturnArr[0] = $TmpCopyright;
		}
		else
		$success = false;

		$errCode = 0;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$ReturnArr);
		return $r_arr;
	} // end function get copyright

	function getBookSource($itemID)
	{
		$errCode = 0;
        $itemData = $this->returnItemData($itemID);

        $sql = "
					SELECT
						Source
					FROM
						INTRANET_ELIB_BOOK
					WHERE
						BookID= '".$itemData["book_id"]."'
				";

		$row = $this->getSqlResult($sql, 1);

        if($row != null)
		$success = true;
		else
		$success = false;

		$errCode = 0;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$row);
		return $r_arr;
    } // end function get book source

	function getAudio($itemID, $data)
	{
		$errCode = 0;
		//$pageID = $data[0];
		$chapterID = $data[0];
		$objName = $data[1];
		$itemData = $this->returnItemData($itemID);
		$path = $itemData["folder_path"];
		$book_id = $itemData["book_id"];

		//$sql = "
		//			SELECT
		//				ChapterID
		//			FROM
		//				INTRANET_ELIB_BOOK_PAGE
		//			WHERE
		//				BookID= '".$itemData["book_id"]."'
		//				AND PageID='".$pageID."'
		//		";

		//$row = $this->getSqlResult($sql, 1);

		$ReturnArr = array();

		//$path = "Audio/Bad Love/";
		//$path = $path."file/elibrary/Audio/".$book_id."/";


		//if($pageID == "1" || $pageID == "7")
		//$row = $path."1.mp3";
		//else if ($pageID == "2" || $pageID == "8")
		//$row = $path."2.mp3";
		//else if ($pageID == "3" || $pageID == "9")
		//$row = $path."3.mp3";
		//else
		//$row = $path."4.mp3";

		//$ReturnArr[0] = $row;

		//$fullPath = $path."file/elibrary/Audio/".$book_id."/".$row[0][0].".mp3";
		$fullPath = $path."file/elibrary/Audio/".$book_id."/".$chapterID.".mp3";

		$ReturnArr[0] = $fullPath;
		$ReturnArr[1] = $objName;
		$ReturnArr[2] = $chapterID;

		// for intranet
		$checkPath1 = "../../../file/elibrary/Audio/".$book_id."/".$chapterID.".mp3";
		// for emeeting site
		//$checkPath2 = "../../../../../../intranetIP/file/elibrary/Audio/".$book_id."/".$row[0][0].".mp3";

		//if($row != null && (is_file($checkPath1)))
		if(is_file($checkPath1))
		$success = true;
		else
		$success = false;

		//$ReturnArr[2] = $chapterID;

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$ReturnArr);
		return $r_arr;
	} // end function get Audio

	function getBookCover($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);
		$path = $itemData["folder_path"];
		$book_id = $itemData["book_id"];

		$ReturnArr = array();

		$row = $path."/file/elibrary/content/".$book_id."/image/cover.jpg";
		//$row = "content/cover_".$book_id.".jpg";
		$ReturnArr[0] = $row;

		if($row != null)
		$success = true;
		else
		$success = false;

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "data"=>$ReturnArr);
		return $r_arr;
	} // end function get book cover

	function getLogo($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);
		$path = $itemData["folder_path"];

		$sql = "
					SELECT
						Source, Level
					FROM
						INTRANET_ELIB_BOOK
					WHERE
						BookID= '".$itemData["book_id"]."'
				";

		$row = $this->getSqlResult($sql, 2);

		$ReturnArr = array();

		$imagePath = "";
		if($data == "logo")
		{
			if($row[0][0] == "green")
			$imagePath = $path."file/elibrary/logo/logo_green.png";
			else if($row[0][0] == "cup")
			$imagePath = $path."file/elibrary/logo/logo_cup.png";
		}
		else if($data == "level")
		{
			if($row[0][0] == "cup" && $row[0][1] != NULL)
			{
				$imagePath = $path."file/elibrary/logo/level".$row[0][1].".png";
			}
		}

		$ReturnArr[0] = $imagePath;
		$ReturnArr[1] = $data;
		$ReturnArr[2] = $row[0][1];

		if($imagePath != "")
		$success = true;
		else
		$success = false;

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$ReturnArr);
		return $r_arr;
	} // end function get book logo

	function getProgress($itemID)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);

		$sql = "
					SELECT
						PageID, Percentage, FinishTime
					FROM
						INTRANET_ELIB_USER_PROGRESS
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
				";

		$row = $this->getSqlResult($sql, 3);


		if (count($row)>0)
		{
			$data = $row[0];

			$success = true;
			$errCode = 0;				// no error
		}
		else
		{
			$success = false;
			$errCode = 1;				// Permission denied!
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql, "data"=>$data);
		return $r_arr;
	}//end function getProgress

	function saveProgress($itemID, $data)
	{
		$errCode = 0;
		$itemData = $this->returnItemData($itemID);
		$ReadPageID = $data[0];
		$ReadPercent = $data[1];

		//determine whether the student has finish this book
		if($ReadPercent == 100){
			$finishTime = 1;
		}else{
			$finishTime = 0;
		}

		$sql = "
					SELECT
						ProcessID
					FROM
						INTRANET_ELIB_USER_PROGRESS
					WHERE
						BookID=	'".$itemData["book_id"]."'
						AND UserID='".$itemData["user_id"]."'
				";

		$row = $this->getSqlResult($sql, 1);

		if (count($row)>0)
		{
			//modify the resding progress
			$sql = "
						UPDATE
							INTRANET_ELIB_USER_PROGRESS
						SET
							PageID='".addslashes($ReadPageID)."',
							Percentage='".addslashes($ReadPercent)."',
							FinishTime='".addslashes($finishTime)."',
							DateModified=now()
						WHERE
							BookID=	'".$itemData["book_id"]."'
							AND UserID='".$itemData["user_id"]."'
					 ";
			$success = $this->doQuery($sql);
			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.
			}
		}
		else
		{
			//add reading progress
			$sql = "
						INSERT INTO
						INTRANET_ELIB_USER_PROGRESS
						(BookID,UserID,PageID,Percentage,FinishTime,DateModified)
						VALUES
							('".$itemData["book_id"]."','".$itemData["user_id"]."'
							,'".addslashes($ReadPageID)."','".addslashes($ReadPercent)."','".addslashes($finishTime)."',now())
					 ";
			$success = $this->doQuery($sql);
			if (!$success)
			{
				$errCode = 2;		// Failed to save due to unkown reason, please try again.

			}
		}

		//$success = true;
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]." ".$sql);

		return $r_arr;
	}//end function saveProgress

	function returnItemData($itemID)
	{
		$ra = array();
		list($db, $ra["user_id"], $ra["book_id"], $ra["folder_path"]) = explode("SeP", $itemID);

		if ($db!="")
		{
			$_SESSION["elib_course_db"] = $db;
		}

		return $ra;
	}

	function GetStringPart($WholeStr, $SearchStr)
	{
		//header('Content-Type: text/html; charset=utf-8');
		$PrevChar = 12;
		$AfterChar = 12;

		$tmpWholeStr = strtoupper($WholeStr);
		$tmpSearchStr = strtoupper($SearchStr);

		mb_internal_encoding("UTF-8");

		//$Pos1 = strpos($WholeStr,$SearchStr);
		$Pos1 = mb_strpos($tmpWholeStr,stripslashes($tmpSearchStr));

		if ($Pos1 === false)
		{
			return "";
		}
		else
		{
			if (($Pos1-$PrevChar)<0)
			{
				$StartPos = 0;
			}
			else
			{
				$StartPos = $Pos1-$PrevChar;
			}

			//if (($Pos1+5)<strlen($WholeStr))
			if (($Pos1+$AfterChar)<mb_strlen($WholeStr))
			{
				$EndPos = $Pos1+$AfterChar+mb_strlen($SearchStr);
			}
			else
			{
				//$EndPos = strlen($WholeStr);
				$EndPos = mb_strlen($WholeStr);
			}

			//$EndPos = mb_strlen($WholeStr);
		}
		//$ReturnStr = substr($WholeStr,$StartPos,$EndPos);
		$ReturnStr = mb_substr($WholeStr,$StartPos,$EndPos-$StartPos);


		return $ReturnStr;
	}

	###########################* ADDITIONAL FUNCTIONS *##########################


	function dataEncrypt($string, $key)
	{
		$errCode = 0;
		$result = '';
		for($i=1; $i<=strlen($string); $i++)
		{
			$char = substr($string, $i-1, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return $result;
	}


	function dataDecrypt($string, $key)
	{
		$errCode = 0;
		$result = '';
		for($i=1; $i<=strlen($string); $i++)
		{
			$char = substr($string, $i-1, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}


	function getCategoryRoot($folderID, $coursefolder){
		switch ($folderID){
			case 0: $root = $this->FILE_FOLDER."/$coursefolder/notes"; break;
			case 1: $root = $this->FILE_FOLDER."/$coursefolder/reference"; break;
			case 2: $root = $this->FILE_FOLDER."/$coursefolder/glossary"; break;
			case 3: $root = $this->FILE_FOLDER."/$coursefolder/assignment"; break;
			case 4: $root = $this->FILE_FOLDER."/$coursefolder/handin"; break;
			case 5: $root = $this->FILE_FOLDER."/$coursefolder/question"; break;
			case 6: $root = $this->FILE_FOLDER."/$coursefolder/public"; break;
			case 7: $root = $this->FILE_FOLDER."/$coursefolder/group"; break;
			case 8: $root = $this->FILE_FOLDER."/$coursefolder/personal"; break;
			case 9: $root = $this->FILE_FOLDER."/public"; break;
			case 10: $root = $this->FILE_FOLDER."/album"; break;
			default: $root = $this->FILE_FOLDER."/$coursefolder/notes"; break;
		}
		return $root;
	}







	###########################* COMMON FUNCTIONS *##########################


	// This function will pretend as authentication but used to initialize
	function _authenticate($db, $userKey)
	{
		return "valid_user";

		if ($db!="" && $userKey!="")
		{
			$_SESSION["elib_course_db"] = $db;

			# KEY: login_session_id + user_id + timestamp
			list($session_id, $user_id, $ts, $powerconcept_id, $pc_sort, $file_id, $is_file_new, $fileName, $categoryID) = explode("SeP", $userKey);

			# normal access
			if ($db!=$this->ECLASS_DB)
			{
				$sql = "SELECT login_session_id FROM login_session
						WHERE login_session_id='$session_id' AND user_id='$user_id'
							AND UNIX_TIMESTAMP(inputdate)='$ts' ";
				$row = $this->getSqlResult($sql, 1);
			}

			if (sizeof($row)>0 || ($ts=="" && sizeof($row)<=0) || $categoryID!="")
			{
				# check if graded, if yes, block save
				if ($pc_sort=="A")
				{
					$sql = "SELECT h.grade FROM powerconcept AS p, handin AS h
							WHERE p.PowerConceptID=$powerconcept_id AND p.assignment_id=h.assignment_id
								AND (p.user_id=h.user_id OR p.group_id=h.group_id)
								AND (h.status='L' OR h.status='LR') ";
					$row_h = $this->getSqlResult($sql, 1);
					$grade = trim($row_h[0][0]);
					$_SESSION["pc_block_save"] = ($grade!="");
				} else
				{
					$_SESSION["pc_block_save"] = (($file_id==0 || $file_id=="") && !$is_file_new);
					if (!$_SESSION["pc_block_save"] && $user_id!="")
					{
						$pc_file_path_key = "pc_file_path_u".$user_id."_f".$file_id;
						# determinate the file path
						if ($is_file_new)
						{
							$_SESSION[$pc_file_path_key] = $this->FILE_FOLDER."/".$db."/tmp/$user_id";

							if (!file_exists($_SESSION[$pc_file_path_key]))
							{
								$folderpath = $this->FILE_FOLDER."/".$db."/tmp";
								if (!file_exists($folderpath))
								{
								    $folderpath = OsCommandSafe($folderpath);
									exec("mkdir \"".$folderpath."\"");
								}
								$folderpath .= "/$user_id";
								if (!file_exists($folderpath))
								{
								    $folderpath = OsCommandSafe($folderpath);
									exec("mkdir \"".$folderpath."\"");
								}
							}
							$file_remove = $_SESSION[$pc_file_path_key]."/*.epc";
							$_SESSION[$pc_file_path_key] .= "/".$fileName;
							if (file_exists($_SESSION[$pc_file_path_key]))
							{
								rename($_SESSION[$pc_file_path_key], $_SESSION[$pc_file_path_key]."_tmp");
							}
							$file_remove = OsCommandSafe($file_remove);
							exec("rm $file_remove");
							if (file_exists($_SESSION[$pc_file_path_key]."_tmp"))
							{
								rename($_SESSION[$pc_file_path_key]."_tmp", $_SESSION[$pc_file_path_key]);
							}
						} elseif ($file_id>0)
						{
							if ($categoryID>=9)
							{
								$_SESSION["elib_course_db"] = $this->ECLASS_DB;
							}

							# YuEn: select from eclass_file
							$sql = "SELECT Location, Title, Category FROM eclass_file WHERE fileID='$file_id' ";
							$row_file = $this->getSqlResult($sql, 3);
							list($Location, $Title, $Category) = $row_file[0];
							$_SESSION[$pc_file_path_key] = $this->getCategoryRoot($Category, $_SESSION["elib_course_db"])."/".$Location."/".$Title;
						}
					}
				}

				return "valid_user";
			}
		}

		return false;
	}


	function getParseArray(){
		$rArr = array();
		for ($i=0; $i<func_num_args(); $i+=2)
		{
			$value = is_array(func_get_arg($i+1)) ? func_get_arg($i+1) : trim(func_get_arg($i+1));
			$arrB = array(func_get_arg($i)=>$value);
			$rArr = array_merge($rArr, $arrB);
		}

		return $rArr;
	}


	function getSqlResult($sql, $field_no){
		$i = 0;
		$x = null;
		$result = $this->doQuery($sql);
		if ($result && mysql_num_rows($result)!=0)
		{
			while ($row = mysql_fetch_array($result))
			{
				for ($k=0; $k<$field_no; $k++)
				{
					$x[$i][$k] = $row[$k];
				}
				$i++;
			}
		}
		mysql_free_result($result);

		return $x;
	}


	function doConnectDB(){
		# load eClass config and connect to MySql

		//after moving to sites 146/149, this variable, $BLOCK_LIB_LOADING, has to to be commented out, if not, database cannot be connected
		//$BLOCK_LIB_LOADING = true;

		include_once("../../global.php");
		//include_once("../../libdb.php");

		/*
		if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set))
		{
			if (in_array("b5",$intranet_default_lang_set))
			{
				$MessageCharset = "big5";
			}
			else
			{
				$MessageCharset = "GB2312";
			}
		}
		else
		{
			$MessageCharset = "big5";
		}
		*/

		//if(!defined("MESSAGECHARSET")) define("MESSAGECHARSET", $MessageCharset);
		//$this->MESSAGECHARSET = constant("MESSAGECHARSET");

		if(!isset($_SESSION['flash_book']['MESSAGECHARSET']))
			$_SESSION['flash_book']['MESSAGECHARSET'] = $MessageCharset;
		$this->MESSAGECHARSET = $_SESSION['flash_book']['MESSAGECHARSET'];

		//if(!defined("ECLASS_HTTPPATH")) define("ECLASS_HTTPPATH", $eclass_httppath);
		//$this->ECLASS_HTTPPATH = constant("ECLASS_HTTPPATH");

		if(!isset($_SESSION['flash_book']['ECLASS_HTTPPATH']))
			$_SESSION['flash_book']['ECLASS_HTTPPATH'] = $eclass_httppath;
		$this->ECLASS_HTTPPATH = $_SESSION['flash_book']['ECLASS_HTTPPATH'];

		//if(!defined("ECLASS_FILEPATH")) define("ECLASS_FILEPATH", $eclass_filepath);
		//$this->ECLASS_FILEPATH = constant("ECLASS_FILEPATH");

		if(!isset($_SESSION['flash_book']['ECLASS_FILEPATH']))
			$_SESSION['flash_book']['ECLASS_FILEPATH'] = $eclass_filepath;
		$this->ECLASS_FILEPATH = $_SESSION['flash_book']['ECLASS_FILEPATH'];

		//if(!defined("SERVER_IP")) define("SERVER_IP", $server_ip);
		//$this->SERVER_IP = constant("SERVER_IP");

		if(!isset($_SESSION['flash_book']['SERVER_IP']))
			$_SESSION['flash_book']['SERVER_IP'] = $server_ip;
		$this->SERVER_IP = $_SESSION['flash_book']['SERVER_IP'];

		//if(!defined("INTRANET_HTTPPATH")) define("INTRANET_HTTPPATH", $intranet_httppath);
		//$this->INTRANET_HTTPPATH = constant("INTRANET_HTTPPATH");

		if(!isset($_SESSION['flash_book']['INTRANET_HTTPPATH']))
			$_SESSION['flash_book']['INTRANET_HTTPPATH'] = $intranet_httppath;
		$this->INTRANET_HTTPPATH = $_SESSION['flash_book']['INTRANET_HTTPPATH'];

		//if(!defined("INTRANET_ROOT")) define("INTRANET_ROOT", $intranet_root);
		//$this->INTRANET_ROOT = constant("INTRANET_ROOT");

		if(!isset($_SESSION['flash_book']['INTRANET_ROOT']))
			$_SESSION['flash_book']['INTRANET_ROOT'] = $intranet_root;
		$this->INTRANET_ROOT = $_SESSION['flash_book']['INTRANET_ROOT'];

		//if(!defined("FILE_FOLDER")) define("FILE_FOLDER", "$intranet_root/files");
		//$this->FILE_FOLDER = constant("FILE_FOLDER");

		if(!isset($_SESSION['flash_book']['FILE_FOLDER']))
			$_SESSION['flash_book']['FILE_FOLDER'] = "$intranet_root/files";
		$this->FILE_FOLDER = $_SESSION['flash_book']['FILE_FOLDER'];

		//if(!defined("ECLASS_DB")) define("ECLASS_DB", $intranet_db);
		//$this->ECLASS_DB = constant("ECLASS_DB");
		//$this->db = constant("ECLASS_DB");

		if(!isset($_SESSION['flash_book']['ECLASS_DB']))
			$_SESSION['flash_book']['ECLASS_DB'] = $intranet_db;
		$this->ECLASS_DB = $_SESSION['flash_book']['ECLASS_DB'];
		$this->db = $_SESSION['flash_book']['ECLASS_DB'];

		//if(!defined("ECLASS_DB_LOGIN")) define("ECLASS_DB_LOGIN", $intranet_db_user);
		//$this->ECLASS_DB_LOGIN = constant("ECLASS_DB_LOGIN");

		if(!isset($_SESSION['flash_book']['ECLASS_DB_LOGIN']))
			$_SESSION['flash_book']['ECLASS_DB_LOGIN'] = $intranet_db_user;
		$this->ECLASS_DB_LOGIN = $_SESSION['flash_book']['ECLASS_DB_LOGIN'];

		//if(!defined("ECLASS_DB_PASSWD")) define("ECLASS_DB_PASSWD", $intranet_db_pass);
		//$this->ECLASS_DB_PASSWD = constant("ECLASS_DB_PASSWD");

		if(!isset($_SESSION['flash_book']['ECLASS_DB_PASSWD']))
			$_SESSION['flash_book']['ECLASS_DB_PASSWD'] = $intranet_db_pass;
		$this->ECLASS_DB_PASSWD = $_SESSION['flash_book']['ECLASS_DB_PASSWD'];

		//$new_sys_custom = $sys_custom['MySQL_Server_Host'];

		if(!isset($_SESSION['flash_book']['MySQL_Server_Host']))
			$_SESSION['flash_book']['MySQL_Server_Host'] = $sys_custom['MySQL_Server_Host'];
		$new_sys_custom = $_SESSION['flash_book']['MySQL_Server_Host'];

		$this->conn = mysql_pconnect($new_sys_custom ? $new_sys_custom : "localhost", $this->ECLASS_DB_LOGIN, $this->ECLASS_DB_PASSWD);
		mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");
		return;
	}


	function doQuery($query){
		mysql_select_db($_SESSION["elib_course_db"]) or exit(mysql_error());
		return mysql_query($query);
	}


	function getSqlInsertID(){
		return mysql_insert_id();
	}


	function testData($answers){
		$sqlstr = (is_array($answers)) ? implode(", ", $answers) : $answers;
		$sql = "INSERT INTO ec3dev_eclass.test (sqlstr, inputtime) VALUES ('$sqlstr', now()) ";

		return $this->doQuery($sql);
	}
}
?>