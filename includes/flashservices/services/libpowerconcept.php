<?php
// ---------------------------
// class name must be the same
// as the php file name
// ---------------------------
/*
 * Editing by Siuwan
 * 
 * Modification Log:
 * 
 * 2015-10-20 (Siuwan) [ip.2.5.6.12.1]
 * 		- set default lang if $intranet_session_language is empty
 * 2011-12-08 (Jason)
 * 		- support remote eclass db host
 * 2011-06-24 (Jason)
 * 		- modify returnItemData() to add new index as 'is_marking'
 * 		- modify sendSave() to update teacher marking into a new field
 * 		- modify getLoad() to load teacher marked data or student original submitted data
 */

class libpowerconcept
{
	var $block_save = false;
	var $view_right = true;
	var $errMsg = array(
				"No error",
				"Permission denied!",
				"Failed to save due to unkown reason, please try again."
				);
	var $encrypt_key = "YuEn_eClass";
	var $FILE_FOLDER;
	var $ECLASS_DB;
	var $ECLASS_DB_LOGIN;
	var $ECLASS_DB_PASSWD;
	var $ECLASS_DB_HOST;

	// constructor function
	function libpowerconcept()
	{
		global $intranet_session_language;
		// -----------------------------------------
		// the method table describes all the
		// available methods for this class to flash
		// and define the roles of these methods
		// -----------------------------------------
		$this->methodTable = array(
			// name of the function

			"getLoad" => array(
				"description" => "return saved data",
				"access" => "remote",	// available values are private, public, remote
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"sendSave" => array(
				"description" => "submit data to save in server",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string"),
							array("name" => "array", "required" => true, "type" => "array")
							),
				"returns" => array("boolean", "string", "string")
			),

			"sendReset" => array(
				"description" => "clear all version drawing",
				"access" => "remote",
				"roles" => "valid_user",
				"arguments" => array (
							array("name" => "string", "required" => true, "type" => "string")
							),
				"returns" => "boolean"
			)
		);
		if ($intranet_session_language=="")
		{
			$intranet_session_language = "en";
		}
		//$BLOCK_LIB_LOADING = true;
		include_once("../../global.php");
		define("FILE_FOLDER", "$eclass_root/files");
		$this->FILE_FOLDER = constant("FILE_FOLDER");
		define("ECLASS_DB_HOST", "");
		$this->ECLASS_DB_HOST = constant("ECLASS_DB_HOST");
		define("ECLASS_DB", $intranet_db);
		$this->ECLASS_DB = constant("ECLASS_DB");
		define("ECLASS_DB_LOGIN", $intranet_db_user);
		$this->ECLASS_DB_LOGIN = constant("ECLASS_DB_LOGIN");
		define("ECLASS_DB_PASSWD", $intranet_db_pass);
		$this->ECLASS_DB_PASSWD = constant("ECLASS_DB_PASSWD");



		$this->debug = array($this->FILE_FOLDER, $this->ECLASS_DB, $this->ECLASS_DB_LOGIN, $this->ECLASS_DB_PASSWD);

		//$this->doConnectDB();
		$this->conn = mysql_pconnect($this->ECLASS_DB_HOST, $this->ECLASS_DB_LOGIN, $this->ECLASS_DB_PASSWD);
		
		// Copied from lib.php to support UTF8 Chinese filename.
		mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");
		
		return;
	}



	##########################* MAIN FUNCTIONS *#########################


	function getLoad($itemID){
		$itemData = $this->returnItemData($itemID);
		//global $log;
		//$log->addLog($itemData);

		$data = "";

		if ($this->view_right)
		{
			if ($itemData["powerconcept_id"])
			{
				$sql = "SELECT DataContent
							FROM TOOLS_POWERCONCEPT
							WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
				$row = $this->getSqlResult($sql, 2);
				$data_content = (count($row) > 0) ? trim($row[0][0]) : '';
				$marked_content = (count($row) > 0) ? trim($row[0][1]) : '';
				
				if($itemData['is_marking'] == 1 && $marked_content != ''){
					# load teacher marked data content if it is in marking view
					$data = unserialize($marked_content);
				} else {
					$data = unserialize($data_content);
				}
				/*
				$sql = "SELECT DataContent
						FROM powerconcept
						WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
				$row = $this->getSqlResult($sql, 1);
				$data = unserialize($row[0][0]);
				*/
			} elseif ($itemData["file_path"]!="")
			{
				if (is_file($itemData["file_path"]))
				{
					if ($fd = fopen($itemData["file_path"], "r"))
					{
						$data = $this->dataDecrypt(fread($fd, filesize($itemData["file_path"])),$this->encrypt_key);
						$data = unserialize($data);
					}
					fclose ($fd);
				}
			}
			$success = true;
			$errCode = 0;				// no error
		} else
		{
			$success = false;
			$errCode = 1;				// Permission denied!
		}
//$log->addLog('success? '.$success);
		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "data"=>$data);
		return $r_arr;
	}


	function sendSave($itemID, $data){
	//function sendSave($data){
		$itemData = $this->returnItemData($itemID);

		$errCode = 0;					// no error
		if (!$_SESSION["pc_block_save"])
		{
			$success = false;
			$errCode = 0;
			$data = serialize($data);

			if ($itemData["powerconcept_id"])
			{
				if($itemData["is_marking"] == 1){
					# Update Teacher Marked Data Content if it is marking now
					/*
					$sql = "UPDATE IES_POWERCONCEPT_TEMP
							SET MarkedDataContent='".addslashes($data)."', MarkedDate=now()
							WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
					*/
				} else {
					# load the writing
					$sql = "UPDATE TOOLS_POWERCONCEPT
							SET DataContent='".addslashes($data)."', ModifiedDate=now()
							WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
					$success = $this->doQuery($sql);		
					
				}
				if (!$success)
				{
					$errCode = 2;		// Failed to save due to unkown reason, please try again.
				}
			} elseif ($itemData["file_id"] || $itemData["is_file_new"])
			{
				# save to temporary file named "tmp/1/powerconcept.epc"
				# when the file is saved, copy the temporart file to be the real file
				$data = $this->dataEncrypt($data,$this->encrypt_key);
				$success = (($fd = fopen($itemData["file_path"], "w")) && (fputs($fd, $data))) ? true : false;
				fclose ($fd);
				if (!$success)
				{
					$errCode = 2;		// Failed to save due to unkown reason, please try again.
				} elseif ($itemData["file_id"]>0)
				{
					# update file size in DB
					$FileSize = (file_exists($itemData["file_path"])) ? ceil((filesize($itemData["file_path"]))/1024) : "1";
					if ($FileSize<1)
					{
						$FileSize = 1;
					}
					$sql = "UPDATE eclass_file SET size='$FileSize' WHERE fileID='".$itemData["file_id"]."' ";
					$this->doQuery($sql);
				}
			} else
			{
				$errCode = 1;			// Permission denied!
			}
		} else
		{
			$errCode = 1;				// Permission denied!
			$success = false;
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode]);
		return $r_arr;
	}


	function returnItemData($itemID){
		$ra = array();
/*
		$ra["user_id"] = $_SESSION["pc_user_id"];
		$ra["powerconcept_id"] = $_SESSION["pc_powerconcept_id"];
		$ra["file_id"] = $_SESSION["pc_file_id"];
		$ra["is_file_new"] = $_SESSION["pc_is_file_new"];
		$ra["file_path"] = $_SESSION["pc_file_path"];
*/
		list($db, $ra["user_id"], $ra["powerconcept_id"], $ra["file_id"], $ra["is_file_new"], $ra["r_comeFrom"]) = explode("SeP", $itemID);
		$pc_file_path_key = "pc_file_path_u".$ra["user_id"]."_f".$ra["file_id"];
		$ra["file_path"] = $_SESSION[$pc_file_path_key];

		if ($db!="")
		{
			$_SESSION["pc_course_db"] = $db;
		}

		return $ra;
	}


/*
	function sendReset(){
		$itemData["powerconcept_id"] = $_SESSION["pc_powerconcept_id"];

		$sql = "UPDATE powerconcept
				SET DataContent=NULL, ModifiedDate=now()
				WHERE PowerConceptID='".$itemData["powerconcept_id"]."' ";
		$success = $this->doQuery($sql);

		return $success;
	}
*/



	###########################* ADDITIONAL FUNCTIONS *##########################


	function dataEncrypt($string, $key)
	{
		$result = '';
		for($i=1; $i<=strlen($string); $i++)
		{
			$char = substr($string, $i-1, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return $result;
	}


	function dataDecrypt($string, $key)
	{
		$result = '';
		for($i=1; $i<=strlen($string); $i++)
		{
			$char = substr($string, $i-1, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}


	function getCategoryRoot($folderID, $coursefolder){
		switch ($folderID){
			case 0: $root = $this->FILE_FOLDER."/$coursefolder/notes"; break;
			case 1: $root = $this->FILE_FOLDER."/$coursefolder/reference"; break;
			case 2: $root = $this->FILE_FOLDER."/$coursefolder/glossary"; break;
			case 3: $root = $this->FILE_FOLDER."/$coursefolder/assignment"; break;
			case 4: $root = $this->FILE_FOLDER."/$coursefolder/handin"; break;
			case 5: $root = $this->FILE_FOLDER."/$coursefolder/question"; break;
			case 6: $root = $this->FILE_FOLDER."/$coursefolder/public"; break;
			case 7: $root = $this->FILE_FOLDER."/$coursefolder/group"; break;
			case 8: $root = $this->FILE_FOLDER."/$coursefolder/personal"; break;
			case 9: $root = $this->FILE_FOLDER."/public"; break;
			case 10: $root = $this->FILE_FOLDER."/album"; break;
			default: $root = $this->FILE_FOLDER."/$coursefolder/notes"; break;
		}
		return $root;
	}







	###########################* COMMON FUNCTIONS *##########################


	// This function will pretend as authentication but used to initialize
	function _authenticate($db, $userKey){
		$_SESSION["pc_course_db"] = $db;
		$_SESSION['pc_block_save'] = false;
		/*
		if ($db!="" && $userKey!="")
		{
			$_SESSION["pc_course_db"] = $db;

			# KEY: login_session_id + user_id + timestamp
			list($session_id, $user_id, $ts, $powerconcept_id, $pc_sort, $file_id, $is_file_new, $fileName, $categoryID) = explode("SeP", $userKey);
			
			//$_SESSION["pc_user_id"] = $user_id;
			//$_SESSION["pc_powerconcept_id"] = $powerconcept_id;
			//$_SESSION["pc_file_id"] = $file_id;
			//$_SESSION["pc_is_file_new"] = $is_file_new;

			# normal access
			if ($db!=$this->ECLASS_DB)
			{
				$sql = "SELECT login_session_id FROM login_session
						WHERE login_session_id='$session_id' AND user_id='$user_id'
							AND UNIX_TIMESTAMP(inputdate)='$ts' ";
				$row = $this->getSqlResult($sql, 1);
			}

			if (sizeof($row)>0 || ($ts=="" && sizeof($row)<=0) || $categoryID!="")
			{
				# check if graded, if yes, block save
				if ($pc_sort=="A")
				{
					$sql = "SELECT h.grade FROM powerconcept AS p, handin AS h
							WHERE p.PowerConceptID=$powerconcept_id AND p.assignment_id=h.assignment_id
								AND (p.user_id=h.user_id OR p.group_id=h.group_id)
								AND (h.status='L' OR h.status='LR') ";
					$row_h = $this->getSqlResult($sql, 1);
					$grade = trim($row_h[0][0]);
					$_SESSION["pc_block_save"] = ($grade!="");
				} else
				{
					$_SESSION["pc_block_save"] = (($file_id==0 || $file_id=="") && !$is_file_new);
					if (!$_SESSION["pc_block_save"] && $user_id!="")
					{
						$pc_file_path_key = "pc_file_path_u".$user_id."_f".$file_id;
						# determinate the file path
						if ($is_file_new)
						{
							$_SESSION[$pc_file_path_key] = $this->FILE_FOLDER."/".$db."/tmp/$user_id";

							if (!file_exists($_SESSION[$pc_file_path_key]))
							{
								$folderpath = $this->FILE_FOLDER."/".$db."/tmp";
								if (!file_exists($folderpath))
								{
									exec("mkdir \"".$folderpath."\"");
								}
								$folderpath .= "/$user_id";
								if (!file_exists($folderpath))
								{
									exec("mkdir \"".$folderpath."\"");
								}
							}
							$file_remove = $_SESSION[$pc_file_path_key]."/*.epc";
							$_SESSION[$pc_file_path_key] .= "/".$fileName;
							if (file_exists($_SESSION[$pc_file_path_key]))
							{
								rename($_SESSION[$pc_file_path_key], $_SESSION[$pc_file_path_key]."_tmp");
							}
							exec("rm $file_remove");
							if (file_exists($_SESSION[$pc_file_path_key]."_tmp"))
							{
								rename($_SESSION[$pc_file_path_key]."_tmp", $_SESSION[$pc_file_path_key]);
							}
						} elseif ($file_id>0)
						{
							if ($categoryID>=9)
							{
								$_SESSION["pc_course_db"] = $this->ECLASS_DB;
							}

							# YuEn: select from eclass_file
							$sql = "SELECT Location, Title, Category FROM eclass_file WHERE fileID='$file_id' ";
							$row_file = $this->getSqlResult($sql, 3);
							list($Location, $Title, $Category) = $row_file[0];
							$_SESSION[$pc_file_path_key] = $this->getCategoryRoot($Category, $_SESSION["pc_course_db"])."/".$Location."/".$Title;
						}
					}
				}

				return "valid_user";
			}
		}

		return false;
		*/
		return "valid_user";
	}


	function getParseArray(){
		$rArr = array();
		for ($i=0; $i<func_num_args(); $i+=2)
		{
			$value = is_array(func_get_arg($i+1)) ? func_get_arg($i+1) : trim(func_get_arg($i+1));
			$arrB = array(func_get_arg($i)=>$value);
			$rArr = array_merge($rArr, $arrB);
		}

		return $rArr;
	}


	function getSqlResult($sql, $field_no){
		$i = 0;
		$result = $this->doQuery($sql);
		if ($result && mysql_num_rows($result)!=0)
		{
			while ($row = mysql_fetch_array($result))
			{
				for ($k=0; $k<$field_no; $k++)
				{
					$x[$i][$k] = $row[$k];
				}
				$i++;
			}
		}
		mysql_free_result($result);

		return $x;
	}


	function doConnectDB(){
		# load eClass config and connect to MySql
		$BLOCK_LIB_LOADING = true;
		include_once("../../../../system/settings/global.php");
		define("FILE_FOLDER", "$eclass_root/files");
		$this->FILE_FOLDER = constant("FILE_FOLDER");
		define("ECLASS_DB", $eclass_db);
		$this->ECLASS_DB = constant("ECLASS_DB");
		define("ECLASS_DB_LOGIN", $eclass_db_login);
		$this->ECLASS_DB_LOGIN = constant("ECLASS_DB_LOGIN");
		define("ECLASS_DB_PASSWD", $eclass_db_passwd);
		$this->ECLASS_DB_PASSWD = constant("ECLASS_DB_PASSWD");

		return;
	}


	function doQuery($query){
		mysql_select_db($_SESSION["pc_course_db"]) or exit(mysql_error());
		return mysql_query($query);
	}


	function getSqlInsertID(){
		return mysql_insert_id();
	}


	function testData($answers){
		$sqlstr = (is_array($answers)) ? implode(", ", $answers) : $answers;
		$sql = "INSERT INTO ec3dev_eclass.test (sqlstr, inputtime) VALUES ('$sqlstr', now()) ";

		return $this->doQuery($sql);
	}
}
?>