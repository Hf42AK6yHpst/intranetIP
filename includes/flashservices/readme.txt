For installation instructions, see amfphp.org/wiki

CHANGELOG FOR MS2

 - Added new methodTable option (per method) "fastArray" => true|false for fast array serializing on return (will only make a difference for large multidimensional nested voodoo arrays)
 - Added new method Headers::getHeader($key) available from all services, also HeadersFilter.php added
 - Added FrontBase support
 - Added Pear::db support
 - Added CSV-based recordsets support (to use, set returns => "csv recordset" in methodTable and return an associative array containing keys "cols" => array("colname1", "colname2") and "filename" => "filename.csv")
 - Renamed sql folder to adapters to fit with the CSV recordsets
 - Various bugfixes for PHP4 MethodTable class
 - Major overhaul of service browser, should work much better now
 - New actionscript template system for service browser, see browser/templates/ for examples
 - Added new return type binary that will write the value as a string but without charsetHandling
 - Added new return type raw that will write to the output stream directly (careful)
 - SSL with ie hopefully works now
 - Bugfixes

MS2 07/07/2005

 - SSL with IE issue confirmed working
 - Removed NetDebug::trace calls in debugging fastArray

MS2 07/21/2005

 - Completely reimplemented util/MethodTable using PHP tokenizer
 - Added PDO adapter
 - Added automatic PEAR::DB and PDO recognition
 - Fixed trigger_error in PHP4

MS2 07/22/2005

 - Tweaks to generated code
 - Further improvements in MethodTable.php