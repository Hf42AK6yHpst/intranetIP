<?php
# using:  
/*
 * Modification Log:
 * 
 * 	20151210 (Cameron)
 * 	- add function displayCustAnnouncement()
 * 
 * 	20151015 (Ivan) [ip.2.5.6.12.1]
 * 	- modified function display() to sync parameter with libannonuce.php to solve the PHP5.4 "Strict Standards" error
 *
 */

class libannounce_ip30 extends libannounce
{
	function libannounce_ip30($AnnouncementID="")
	{
		$this->libannounce($AnnouncementID);
	}
	public function getIP30Announcement($UserID, $Groups, $ajax_target='all', $from_more=false)
	{
		global $PATH_WRT_ROOT, $plugin, $special_feature;

		if (is_array($Groups) && count($Groups)>0)
		{
			$groupList = implode(",",IntegerSafe($Groups));
		}
		else
		{
			$groupList = '';
		}

		if ($plugin['power_voice'])
		{
			$ExtraField = ", a.VoiceFile";
			$ArrayNum = 8;
		}
		else
		{
			$ExtraField = "";
			$ArrayNum = 7;
		}

		$orderBy = " a.onTop DESC, a.AnnouncementDate DESC, a.AnnouncementID DESC, a.Title ASC";
// 	        $orderBy = "a.AnnouncementDate DESC, a.AnnouncementID ASC ";

		$name_field = getNameFieldWithClassNumberByLang("b.");
		$sql = "SELECT DISTINCT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT";
		$AnnouncementIDs = $this->db_sub_select($sql);

		if($from_more){
            if(!$special_feature['sch_announcement_display_future'])
            {
                $date_cond = "CURDATE()>=a.AnnouncementDate";
            }
            else {
                $date_cond = '1';
            }
        }
		else{
		    if(!$special_feature['sch_announcement_display_future']) {
                $date_cond = ' (CURDATE()>=a.AnnouncementDate AND CURDATE() <= a.EndDate)';
            }
            else {
                $date_cond = ' CURDATE() <= a.EndDate';
            }
        }
        // [2016-0513-1440-15206] add checking for Setting - Allow users to view past news
        include_once($PATH_WRT_ROOT.'includes/libschoolnews.php');
        $lschoolnews = new libschoolnews();
        $date_cond .= " AND ".($lschoolnews->allowUserToViewPastNews? " 1 " : " a.EndDate >= CURDATE() ");

		$conds = "AND a.RecordStatus = '1' AND (a.EndDate is null OR $date_cond )";

		switch ($ajax_target) {
			case 'all':
				$target_conds = '	(
				( ga.GroupID IN ('.$groupList.') AND ga.AnnouncementID = a.AnnouncementID ) 
				OR a.AnnouncementID NOT IN ('.$AnnouncementIDs.')
				)';
				break;
			case 'public':
				$target_conds = 'a.AnnouncementID NOT IN ('.$AnnouncementIDs.')';
				break;
			default :
				$target_conds = '( ga.GroupID IN ('.$groupList.') AND ga.AnnouncementID = a.AnnouncementID )';
				break;
		}

		$sql2 = "	SELECT 
							if(a.AnnouncementID IN ($AnnouncementIDs), 'group', 'public')as `Type`,
            				a.AnnouncementID, 
            				DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d') as `AnnouncementDate`, 
            				a.Title as `AnnouncementTitle`, 
            				IF((LOCATE(';$UserID;',a.ReadFlag)=0 || a.ReadFlag Is Null), 1, 0) as `Read`, 
            				a.Attachment, $name_field  as `ip_30_name_field`, c.Title,
            				DATE_FORMAT(a.EndDate, '%Y-%m-%d') as `EndDate` {$ExtraField}
                    	FROM 
                    		INTRANET_ANNOUNCEMENT as a
						LEFT OUTER JOIN 
							INTRANET_USER as b 
								ON a.UserID = b.UserID
						LEFT OUTER JOIN 
							INTRANET_GROUP as c 
								ON a.OwnerGroupID = c.GroupID
						LEFT OUTER JOIN 
							INTRANET_GROUPANNOUNCEMENT as ga 
								ON a.AnnouncementID = ga.AnnouncementID
						WHERE 
							$target_conds
							$conds 
						ORDER BY 
							$orderBy
						";

		return $this->returnArray($sql2,$ArrayNum, 1);
	}
	
	public function announcementListHtml($UserID, $Groups, $ajax_target, $from_more=false)
	{
		global $i_AnnouncementOwner, $i_no_record_exists_msg, $i_general_more, $ip20_public;
	    $rows = $this->getIP30Announcement($UserID, $Groups, $ajax_target, $from_more);

		$x = '<ul class="indexlist-container">';
	    if (count($rows)) {
			foreach($rows as $announcement) {
				$_class = ($announcement['Type'] == 'public') ? 'news-public' : 'news-groups';
				$x .= '<li class="indexnewslist-item '. $_class. '">';
				$x .= 	'<a href="javascript:void(0)" onclick="javascript:fe_view_announcement('. $announcement['AnnouncementID'] .', 0)">';
				$x .= 		'<div class="indexnewslist">';
				$x .= 		  	'<span class="indexnewslist-title">'. $announcement['AnnouncementTitle']. '</span>';
								if($announcement['Read']) {
				$x .= 		  	'<span class="new-alert"></span>';
								}
								if($announcement['Attachment']){
				$x .= 			'<span class="attachment-icon"></span>';
								}
				$x .= 		'</div>';
				if ($from_more) {
					$x .= 	'<div class="indexnewsdesc">('.$announcement['AnnouncementDate'].' - '.$announcement['EndDate'] .') '. $i_AnnouncementOwner.' '.$announcement['ip_30_name_field']. '</div>';
				} else {
					$x .= 	'<div class="indexnewsdesc">('.$announcement['AnnouncementDate'].') '. $i_AnnouncementOwner.' '. $announcement['ip_30_name_field'].'</div>';
				}
				$x .= 	'</a>';
				$x .=   '</li>';
			}
		} else {
	    	// no records
			$x .= '<li class="indexnewslist-item">'. $i_no_record_exists_msg. '</li>';
		}
		$x .= '</ul>';
	    if(! $from_more) {
			if ($ajax_target == 'group') {
				$group_str = is_array($Groups) ? implode(",", $Groups) : implode(",", array($Groups));
				$x .= '<div class="indexlist-more">
					<a href="#" onclick="javascript:moreGroupNews(1, \'' . $group_str . '\')">' . $i_general_more . '...</a>
				</div>';
			} elseif ($ajax_target == 'public') {
				$x .= '<div class="indexlist-more">
					<a href="#" onclick="javascript:moreNews(0)">' . $i_general_more . '...</a>
				</div>';
			} elseif ($ajax_target == 'all') {
				$group_str = is_array($Groups) ? implode(",", $Groups) : implode(",", array($Groups));
				$x .= '<div class="indexlist-more">
					<a href="#" onclick="javascript:moreGroupNews(2, \'' . $group_str . '\')">' . $i_general_more . '...</a>
				</div>';
			}
		}

	    return $x;
	}
}

?>
