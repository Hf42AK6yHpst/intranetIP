<?php
// Editing by 
### http://www.sitepoint.com/forums/showthread.php?263852-Encrypt-passed-URL-parameters
/*
 * 2019-06-06 Carlos: Before/after base64 encode/decode, handle dirty words that the url maybe blocked by school firewall.
 */
if (!defined("LIBURLPARAHANDLER_DEFINED")) {
	define("LIBURLPARAHANDLER_DEFINED", true);
	
	class liburlparahandler {
		var $encryptKey;
		var $paraEncrypted;
		var $paraDecrypted;
		var $paraAry;
		var $encryptBit;
		var $encodeMode;
		
		const defaultKey = 'defaultKey_asvmp04^VW$C';
		const actionEncrypt = 'e';
		const actionDecrypt = 'd';
		const safe_b64 = 'safe_b64';
		const b64 = 'b64';
//		var $defaultKey = 'defaultKey_asvmp04^VW$C';
//		var $actionEncrypt = 'e';
//		var $actionDecrypt = 'd';
		
		var $dirty_words = array('sex','fuck','prick','bastard','ass','arse','cunt','balls','shit');
		
		function __construct($action='', $paraText='', $key='', $encryptBit='', $encodeMode='') {
		//function liburlparahandler($action='', $paraText='', $key='') {
			$key = ($key=='')? self::defaultKey : $key;
			//$key = ($key=='')? $this->defaultKey : $key;
			$encryptBit = ($encryptBit=='')? 256 : $encryptBit;
			$encodeMode = ($encodeMode=='')? self::safe_b64 : $encodeMode;
			
			
			$this->setEncryptKey($key);
			$this->setEncryptBit($encryptBit);
			$this->setEncodeMode($encodeMode);
			
			if ($action == self::actionEncrypt) {
			//if ($action == $this->actionEncrypt) {
				$this->setParaDecrypted($paraText);
			}
			else if ($action == self::actionDecrypt) {
			//else if ($action == $this->actionDecrypt) {
				$this->setParaEncrypted($paraText);
			}
			
			$this->performAction($action);
		}
		
		private function setEncryptKey($val) {
		//function setEncryptKey($val) {
			$this->encryptKey = $val;
		}
		private function getEncryptKey() {
		//function getEncryptKey() {
			return $this->encryptKey;
		}
		
		public function setParaEncrypted($val) {
		//function setParaEncrypted($val) {
			$this->paraEncrypted = $val;
		}
		public function getParaEncrypted() {
		//function getParaEncrypted() {
			return $this->paraEncrypted;
		}
		
		public function setParaDecrypted($val) {
		//function setParaDecrypted($val) {
			$this->paraDecrypted = $val;
		}
		public function getParaDecrypted() {
		//function getParaDecrypted() {
			return $this->paraDecrypted;
		}
		
		private function setParaAry($val) {
		//function setParaAry($val) {
			$this->paraAry = $val;
		}
		public function getParaAry() {
		//function getParaAry() {
			return $this->paraAry;
		}
		
		private function setEncryptBit($val) {
			$this->encryptBit = $val;
		}
		public function getEncryptBit() {
			return $this->encryptBit;
		}
		
		private function setEncodeMode($val) {
			$this->encodeMode = $val;
		}
		public function getEncodeMode() {
			return $this->encodeMode;
		}
		
		
		
		public function performAction($action) {
		//function performAction($action) {
			if ($action == self::actionEncrypt) {
			//if ($action == $this->actionEncrypt) {
				$this->doEncrypt();
			}
			else if ($action == self::actionDecrypt) {
			//else if ($action == $this->actionDecrypt) {
				$this->doDecrypt();
			}
		}
		
		public function doEncrypt() {
		//function doEncrypt() {  
			$encryptBit = $this->getEncryptBit();
			$encodeMode = $this->getEncodeMode();
			
			if(function_exists('mcrypt_create_iv') && function_exists('mcrypt_get_iv_size') && function_exists('mcrypt_encrypt')) {
				$encryptBitConst = ($encryptBit==256)? MCRYPT_RIJNDAEL_256 : MCRYPT_RIJNDAEL_128;
			
				$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptBitConst, MCRYPT_MODE_ECB), MCRYPT_RAND);
				$passcrypt = mcrypt_encrypt($encryptBitConst, $this->getEncryptKey(), $this->getParaDecrypted(), MCRYPT_MODE_ECB, $iv);
			}
			else {
				$passcrypt = $this->getParaDecrypted();
			}
			
			if ($encodeMode == 'safe_b64') {
				$this->setParaEncrypted($this->safe_b64encode($passcrypt));
			} else if ($encodeMode == 'b64') {
				$this->setParaEncrypted(base64_encode($passcrypt));
			}
		}
		
		public function doDecrypt() {
		//function doDecrypt() {
			$encodeMode = $this->getEncodeMode();
			
			if ($encodeMode == 'safe_b64') {
				$decoded = $this->safe_b64decode($this->getParaEncrypted());
			} else if ($encodeMode == 'b64') {
				$decoded = base64_decode($this->getParaEncrypted());
			}
			
			if(function_exists('mcrypt_create_iv') && function_exists('mcrypt_get_iv_size') && function_exists('mcrypt_encrypt')) {
			    $encryptBit = $this->getEncryptBit();
				$encryptBitConst = ($encryptBit==256)? MCRYPT_RIJNDAEL_256 : MCRYPT_RIJNDAEL_128;
				
				$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptBitConst, MCRYPT_MODE_ECB), MCRYPT_RAND);  
			    $decrypted = mcrypt_decrypt($encryptBitConst, $this->getEncryptKey(), $decoded, MCRYPT_MODE_ECB, $iv);
			    $this->setParaDecrypted(str_replace("\0", '', $decrypted));
			}
			else {
				$this->setParaDecrypted($decoded);
			}
			
			$this->setParaAry($this->returnParaDecryptedAry());
		}
		
		private function returnParaDecryptedAry() {
		//function returnParaDecryptedAry() {
			$returnAry = array();
			parse_str($this->getParaDecrypted(), $returnAry);
			return $this->parseParaDecrypted($returnAry);
		}
		
		private function parseParaDecrypted($arr) {
		//function parseParaDecrypted($arr) {
			$narr = array();
		    while(list($key, $val) = each($arr)){
		        if (is_array($val)){
		            $val = $this->parseParaDecrypted($val);
		            if (count($val) > 0){
		                $narr[stripslashes(intranet_undo_htmlspecialchars($key))] = $val;
		            }
		        }
		        else {
					$narr[stripslashes(intranet_undo_htmlspecialchars($key))] = trim(stripslashes($val));
		        }
		    }
		    unset($arr);
		    return $narr;
		}
		
		private function safe_b64encode($string) {
		//function safe_b64encode($string) {
	        $data = base64_encode($string);
	        $data = str_replace(array('+','/','='),array('-','_',''),$data);
	        $data = $this->replace_dirty_words($data);
	        return $data;
	    }
		
		private function safe_b64decode($string) {
	    //function safe_b64decode($string) {
	    	$string = $this->reverse_replace_dirty_words($string);
	        $data = str_replace(array('-','_'),array('+','/'),$string);
	        $mod4 = strlen($data) % 4;
	        if ($mod4) {
	            $data .= substr('====', $mod4);
	        }
	        return base64_decode($data);
	    }
	    
	    public function returnParaValue($paraKey) {
	    //function returnParaValue($paraKey) {
	    	$paraAry = $this->getParaAry();
	    	return $paraAry[$paraKey];
	    }
	    
	   	// do case insensitive search and replace dirty words
	    function replace_dirty_words($word)
		{
			$dirty_words = $this->dirty_words;
			
			foreach($dirty_words as $dirty_word){
				$matches = array();
				if(preg_match_all('/('.$dirty_word.')/i',$word,$matches)){
					if(count($matches)>1){
						foreach($matches[1] as $match){
							$word = str_replace($match,'lllll'.strrev($match).'lllll',$word);
						}
					}
				}
			}
			return $word;
		}
		
		// do case insensitive search and restore replaced dirty words
		function reverse_replace_dirty_words($word)
		{
			$dirty_words = $this->dirty_words;
			
			foreach($dirty_words as $dirty_word){
				$matches = array();
				if(preg_match_all('/lllll('.strrev($dirty_word).')lllll/i',$word,$matches)){
					if(count($matches)>1){
						foreach($matches[1] as $match){
							$word = str_replace('lllll'.$match.'lllll',strrev($match),$word);
						}
					}
				}
			}
			return $word;
		}
	}
}
?>