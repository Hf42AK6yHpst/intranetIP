<?
class importDataManager{
	private $importDataObj = null;

	public function importDataManager($importDataObj){
		$this->importDataObj= $importDataObj;
	}

	public function setImportData($importDataObj){
		$this->importDataObj = $importDataObj;
	}

	public function getDisplayGuide(){
		global $Lang;
		$importField = $this->importDataObj->getDataDefinition();

		$h_guide = '<table border="0">';

		$_columnNo = 0;
		$h_guide .='<tr>';
		$h_guide .= '<td>'.$Lang['General']['ImportArr']['Column'].'</td>';
		$h_guide .= '<td>Title</td>';
		$h_guide .= '<td>Description</td>';
		$h_guide .= '</tr>';

		//define in $cfgFieldTypeValue , /home/web/eclass40/intranetIP25/includes/import/importDataDefinition.php
		$is_mandatory = 1;
		$is_reference = 2;
		$is_optional  = 3;

		foreach ($importField as $aFieldName => $aFieldDetails){
			$_columnNo = $_columnNo  + 1;
			$_description = $aFieldDetails['description'];
			$_data_type_description = $aFieldDetails['data_type_description'];
			$_data_mandatory = $aFieldDetails['data_mandatory'];
			$_remark = $aFieldDetails['remark'];

			switch($_data_mandatory){
				case $is_mandatory:  //define in $cfgFieldTypeValue , /home/web/eclass40/intranetIP25/includes/import/importDataDefinition.php
					$_thisTitle = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Required'].'</span>'.$aFieldName;
					break;
				case $is_reference:
					$_thisTitle = '<span class="tabletextrequire">'.$Lang['General']['ImportArr']['ImportSymbolArr']['Reference'].'</span>'.$aFieldName;			
					break;
				case $is_optional:
					$_thisTitle = $aFieldName;

					break;
			}
	
			if($_remark != ''){
				$_remark = '<font size = "1"><i>('.$_remark.')</i></font>';
			}

			$h_guide .='<tr>';
			$h_guide .= '<td>'.$_columnNo.'</td>';
			$h_guide .= '<td>'.$_thisTitle.'</td>';
			$h_guide .= '<td>'.$_description.' <br/>'.$_remark.'</td>';
			
			$h_guide .= '</tr>';
		}

		$h_guide .= '</table>';

		return $h_guide;
	}
	public function getDisplayErrorRecord(){



			if($this->getNoOfInvalidRecord() > 0){
				$checkResult = $this->getImportError();
				$dataArray  = $this->importDataObj->getImportDataSource();

				$tmpTitle = $this->importDataObj->getDataDefaultCsvHeaderArr();
				$engTitle = array();
				for($i = 0,$i_max = count($tmpTitle);$i <$i_max; $i++){
					$engTitle[$tmpTitle[$i]] = $tmpTitle[$i];
				}

					$h_error .= '<table class="common_table_list_v30 view_table_list_v30">';
					$h_error .= '<thead>';
							$h_error .= '<tr>';
								$h_error .= '<th width="10">'.$Lang['General']['ImportArr']['Row'].'</th>';
								//for ($j=0,$j_max=count($engTitle); $j<$j_max; $j++)
								foreach ($engTitle as $_name => $_value)
								{
									$thisDisplay = $_name;					
									$h_error .= '<th>'.$thisDisplay.'</th>';
								}
								$h_error .= '<th>'.$Lang['General']['Remark'].'</th>';
							$h_error .= '</tr>';
					$h_error .= '</thead>';

					foreach ($checkResult as $_lineString => $_errorDetails){
						$_error = $_errorDetails['ERROR'];
						list($dummyStr , $_lineInt) = explode('_',$_lineString);

						if(count($_error) > 0){
							$h_error .='<tr style="vertical-align:top">';
								$h_error .='<td>'.($_lineInt+3).'</td>';

								foreach ($dataArray[$_lineInt] as $_fieldName => $_csvValue){
									$h_error .='<td>'.$_csvValue.'&nbsp;</td>';
								}

							$h_error .= '<td><font color="red">';				
							foreach ($_error as $_errorCode => $_errorString){
								$h_error .= ' - &nbsp;'.$_errorString.'<br/>';
							}
							$h_error .= '</font></td>';

							$h_error .='</tr>';

						}
					}

					$h_error .= '</table>';
			}
			return $h_error;
	}
	public function getDataDefaultCsvHeaderArr(){
		return $this->importDataObj->getDataDefaultCsvHeaderArr();
	}
	public function getDataColumnPropertyArr(){
		return $this->importDataObj->getDataColumnPropertyArr();
	}
	public function setImportDataSource($dataArray){
		$this->importDataObj->setImportDataSource($dataArray);
	}
	public function checkImportData(){
		$returnResultAry = $this->importDataObj->checkImportData();
		return $returnResultAry;
	}
	public function processImportData(){
		$this->importDataObj->processImportData();
	}
	public function getImportError(){
		return $this->importDataObj->getImportError();
	}
	public function getNoOfValidRecord(){
		return $this->importDataObj->getNoOfValidRecord();
	}
	public function getNoOfInvalidRecord(){
		return $this->importDataObj->getNoOfInvalidRecord();
	}
}
?>