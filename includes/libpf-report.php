<?php
/** [Modification Log] Being modified by: 
 * *******************************************
 * 2020-03-31 Philips [2020-0330-1038-43073]
 * - Modified GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF(), avoid missing <td> element cause TCPDF error
 * 2019-05-27 Anna
 * - Modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID(), added $sys_custom['iPf']['StewardsPooiKeiCollege']['Report']['StudentLearningProfile'] harcode subject id 
 * 
 * 2019-01-22 Anna
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID(), added ignore assessment for get assessment grade [W155316]
 * 
 * 2019-01-07 Anna
 * - modified GEN_SELF_ACCOUNT_TABLE_SLP_PDF() - fixed cannot show self-account when have 全型space
 * 
 * 2018-07-12 Anna
 * - modified customizeHeader_layout_1() - added table td width
 * 
 * 2018-03-29 Anna [S137527]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID(), added TermAssessment is null for $StudentAcademicResultInfoArr
 * 
 * 2018-02-28 Ivan [F136181] [ip.2.5.9.3.1]
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID() and GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID() to support $sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbol']
 * 
 * 2018-01-17 Omas
 * -modified added $sys_custom['iPf']['SLP']['GroupSubjectByID']  for slp grouping up the subject - #P134253
 * 
 * 2017-10-31 Isaac
 * -modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP() -  added class="acadamic_t_boarder" inn <td> to add border to the table
 * 
 * 2017-03-16 Omas
 * -modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP() -  added  2 flag -  #S114597
 * - $sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectEmptySymbol'] , $sys_custom['iPf']['Report']['AcademicResult']['cmpSubjectStyle']
 * 
 * 2017-02-27 Omas
 * -modified GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF_SAGC() - force student photo width to 100px - #V113720 
 * 
 * 2017-02-22 Omas
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID(), GET_ACADEMIC_PERFORMANCE_SLP_IN_ID() - $sys_custom['iPf']['Report']['SkipRepeatAssessmentResult'], $sys_custom['iPf']['Report']['SkipSubjectIdArr'] - #V113395 
 * 
 * 2016-12-08 Omas
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID(), GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID() - not show fullmark if that subject don't have score #W109947 
 * 
 * 2016-11-28 Omas
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID(), GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID() - as fullmark changed to decimal. if fullmark 100.0 will show as 100 
 * 
 * 2016-05-20 Omas
 * - modified GET_STUDENT_PARTICULARS_SLP(), retrievePortfolioTranscriptConfig() - replace split() by explode() php5.4
 *  
 * 2015-04-09 Bill [2015-0401-1610-41164]
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID(), add $sys_custom['iPf']['DynamicReport']['AcademicResultSectionDisplay10ptFontSize'] to set font size of Academic Performance table in dynamic report to 10pt
 * 
 * 2014-02-21 Ivan [2014-0221-1003-21184]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID() to check component subject by component id instead of component code
 * 
 * 2013-05-31 Yuen
 * - improve SLP performance table and award for better page break
 * 
 * 2013-03-26 Ivan [2013-0326-1024-09132]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID() to record lastest term of each years of each students
 * 	
 * 2013-01-18 Ivan [2013-0117-1206-59073]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID(), not include subject_class_mapping.php to reduce memory usage
 * 
 * 2012-03-06 Ivan [2012-0223-0947-30071]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID, GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID, GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID
 * - added setPrintRequsetFrom, getPrintRequsetFrom
 * - Fixed problem: when printing report for the whole class, if there are any repeaters in the class, the class information of other students will have extra years information.
 * 
 * 2012-03-05 Ivan [2012-0224-1652-13073]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID, GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID, GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID to show zero mark
 * 
 * 2012-02-22 Ivan
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID(), GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID() to add show empty symbol logic
 * 
 * 2012-02-17 Ivan
 * - modified GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID(), GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID() to add show / hide full mark display logic
 * 
 * 2011-07-05 Ivan [CRM:2011-0705-1622-04071]
 * - modified GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF() to solve the student info left align problem
 * 
 * 2011-06-16 Ivan [CRM:2011-0526-0927-30071]
 * - modified GEN_SELF_ACCOUNT_TABLE_SLP_PDF() to solve the Self Account tab data problem
 * 
 * 2011-06-01 Ivan
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID, added flag $sys_custom['iPf']['Report']['SLP']['Use_ID_For_Class_Mapping'] to control the retrieve of Class Info by ID or Name
 * 
 * 2011-05-06 Ivan
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID, improved the function to get the latest term info first. Otherwise, subject academic result will retrieve from different terms
 * 
 * 2011-04-26 Ivan [CRM:2011-0421-1441-26073]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP_IN_ID, get the overall result at last in order to display 2nd term result if there are no overall result
 * 
 * 2011-03-16 Ivan [CRM:2011-0317-1615-36073]
 * - modified GET_ACADEMIC_PERFORMANCE_SLP, subject ordering follows the School Settings now
 * 
 * 2010-09-06 Max (201009060900)
 * - Modified GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF to show custom student info
 * 
 * 2010-07-20 Max (201007201019)
 * - Modified GEN_STUDENT_PARTICULARS_TABLE_SLP to show HKID according to $special_feature['ava_hkid'] in settings.php
 * 
 * 2010-06-04 Max (201006041431)
 * - GEN_REPORT_HEADER_SLP added parameter to set page break after table
 * *******************************************
 */
// The following setting should be put into settings.php for getCustomOLEYearToSystemYearMapping() in libpf-sem-map.php.
//define('CFG_SCHOOL_CODE', 'UCCKE');

// Get the function convertListOfOLEYearToSystemYear() for libpf_report::getYearSemesterForReportPrinting().
include_once('libpf-sem-map.php');
include_once('libfilesystem.php');


/**
 * Date			: 20151012
 * Description	: modified GEN_SELF_ACCOUNT_TABLE_SLP_PDF() - replacing full width () with half width ()
 * By 			: Omas
 * 
 * Type			: Bug Fixing, Enhancement
 * Date 		: 200911121040
 * Description	: 1) Fix the Report Type section's displacement in Firefox
 * C=CurrentIssue 2) Add radio buttons (Chinese), (English) and (Bilingual) below "Date of Issue"
 * 				  3C) After the Print is pressed, the print page can support different langauges based on the selection of radio buttons added above
 * By			: Max Wong
 * Case Number	: 200911121040MaxWong
 */
class libpf_report extends libportfolio2007
{
	//RECORD the report is request print from which module , [SLP report , dynamic report..]
	private $printRequsetFrom;
	// record the studentId of the current printing report (for academic result)
	private $reportStudentId;
	
	//PDF, WORD or HTML
	private $printFormat;
	
	function setPrintRequsetFrom($requestFrom){
		global $ipf_cfg;

		$this->printRequsetFrom = '';
		if(array_key_exists($requestFrom ,$ipf_cfg['slpAcademicResultPrintRequestFrom'])) {
			$this->printRequsetFrom = $requestFrom;
		}else{
		}
	}

	function getPrintRequsetFrom(){
		return $this->printRequsetFrom;
	}
	
	function setReportStudentId($int) {
		$this->reportStudentId = $int;
	}
	function getReportStudentId() {
		return $this->reportStudentId;
	}
	
	function setPrintFormat($ParType) {
		$this->printFormat = $ParType;
	}
	function getPrintFormat() {
		return $this->printFormat;
	}
	
	# get Transcript Config
	function retrievePortfolioTranscriptConfig(){

		global $eclass_root, $file_path_name, $intranet_root;


		//$portfolio_setting_file = "$admin_root_path/$file_path_name/portfolio_transcript.txt";
		$portfolio_setting_file = "$eclass_root/files/portfolio_transcript.txt";
		$filecontent = trim(get_file_content($portfolio_setting_file));
		$settings = unserialize($filecontent);

		if(file_exists("$intranet_root/file/school_data.txt"))
		{
			$admin_console_content = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
			$settings['school_phone'] = $admin_console_content[2];

			# unset school address loaded from portfolio_transcript
			# and load the address from admin setting
			if(count($admin_console_content) > 3 && $admin_console_content[3] != "")
			{
				$settings['school_address'] = "";
				for($i=3; $i<count($admin_console_content); $i++)
					$settings['school_address'] .= $admin_console_content[$i]."\n";
			}
		}

		return $settings;
	}

	#Get Class List For Report
	function getReportClassListData()
	{
			$class_arr = $this->returnClassListData();
			$class_activated_no = $this->returnClassStudentActivated();

			for ($i=0; $i<=sizeof($class_arr); $i++)
			{
					$class_obj = $class_arr[$i];

					if ($i<sizeof($class_arr))
					{
						if ($class_activated_no[$class_obj["ClassName"]]>0)
						{
								$ClassArray[] = $class_obj["ClassName"];
						}
					}

			}
			return $ClassArray;
	}

	function returnDistinctYear($TableName, $ParOrder=0)
	{
		global $eclass_db;

		$Ordering = ($ParOrder==1) ? "DESC" : "ASC";
		$sql = "SELECT DISTINCT Year FROM {$eclass_db}.{$TableName} ORDER BY Year {$Ordering}";
		$ReturnArray = $this->returnVector($sql);

		return $ReturnArray;
	}
  
  function getYearSemesterForReportPrinting()
	{
		global $eclass_db, $ck_user_rights_ext, $intranet_root;

		$ActivityYearArray = $this->returnDistinctYear("ACTIVITY_STUDENT");
		$AttendanceYearArray = $this->returnDistinctYear("ATTENDANCE_STUDENT");
		$AwardYearArray = $this->returnDistinctYear("AWARD_STUDENT");
		$ConductYearArray = $this->returnDistinctYear("CONDUCT_STUDENT");
		$MeritYearArray = $this->returnDistinctYear("MERIT_STUDENT");
		$ServiceYearArray = $this->returnDistinctYear("SERVICE_STUDENT");

    # Eric Yip (20091112): Also get OLE year
    include_once($intranet_root."/includes/libpf-slp.php");
		$OLEYearArray = libpf_slp::GET_OLE_YEAR_LIST();
		$OLEYearArray = convertListOfOLEYearToSystemYear($OLEYearArray);

		$TempYearArray = array_merge($ActivityYearArray, $AttendanceYearArray, $AwardYearArray, $ConductYearArray, $MeritYearArray, $ServiceYearArray, $OLEYearArray);
		if(is_array($TempYearArray))
		{
			$TempYearArray = array_unique($TempYearArray);
			sort($TempYearArray);
			for($i=0; $i<sizeof($TempYearArray); $i++)
			{
				$year = $TempYearArray[$i];
				$SplitArr = explode("-", $year);
				if(sizeof($SplitArr)==1)
				{
					$new_year = $year."-".($year+1);
					if(!in_array($new_year, $TempYearArray))
						$TargetYearArr[] = $new_year;
				}
				else
				{
					$TargetYearArr[] = $year;
				}
			}
			if(is_array($TargetYearArr))
				sort($TargetYearArr);
		}

		return $TargetYearArr;
	}

#####################################################
#################### Full Report ####################
#####################################################

	function getReportYearCondition($Year, $prefix="")
	{

		if($Year!="")
		{
			$SplitArray = explode("-", $Year);
			if(sizeof($SplitArray)>1)
			{
				$Year2 = trim($SplitArray[0]);
				$conds .= " AND (".$prefix."Year = '$Year' OR ".$prefix."Year = '$Year2')";
			}
			else
				$conds .= " AND ".$prefix."Year = '$Year'";
		}

		return $conds;
	}

	function returnFilteredYearClass($StudentID, $RecordType=0, $Year="")
	{
		global $eclass_db, $intranet_db;

		$conds = ($Year!="") ? $this->getReportYearCondition($Year) : "";
		$sql = "SELECT DISTINCT
					Year,
					ClassName
				FROM
					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
				WHERE
					UserID = '{$StudentID}'
					$conds
				ORDER BY
					Year
				";
		$AssessmentResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($AssessmentResultArr); $i++)
		{
			list($t_year, $t_classname) = $AssessmentResultArr[$i];
			$ReturnArray[trim($t_year)] = trim($t_classname);
		}

		$conds = ($Year!="") ? $this->getReportYearCondition($Year, "Academic") : "";
		$sql = "SELECT DISTINCT
					AcademicYear,
					ClassName
				FROM
					{$intranet_db}.PROFILE_CLASS_HISTORY
				WHERE
					UserID = '{$StudentID}'
					$conds
				ORDER BY
					AcademicYear
				";
		$AcademicResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($AcademicResultArr); $i++)
		{
			list($t_year, $t_classname) = $AcademicResultArr[$i];
			if($ReturnArray[trim($t_year)]=="")
				$ReturnArray[trim($t_year)] = trim($t_classname);
		}

		$TableName = ($RecordType==1) ? "ATTENDANCE_STUDENT" : "MERIT_STUDENT";
		$conds = ($Year!="") ? $this->getReportYearCondition($Year) : "";
		$sql = "SELECT DISTINCT
					Year,
					ClassName
				FROM
					{$eclass_db}.{$TableName}
				WHERE
					UserID = '{$StudentID}'
					$conds
				ORDER BY
					Year
				";
		$RecordResultArr = $this->returnArray($sql);
		for($i=0; $i<sizeof($RecordResultArr); $i++)
		{
			list($t_year, $t_classname) = $RecordResultArr[$i];
			if($ReturnArray[trim($t_year)]=="")
				$ReturnArray[trim($t_year)] = trim($t_classname);
		}
		if(is_array($ReturnArray))
		{
		  # Eric Yip : Order year by class variable
		  if($this->YearDescending)
        krsort($ReturnArray);
      else
        ksort($ReturnArray);
		}

		return $ReturnArray;
	}

	# Get ClassNumber from Intranet According to UserID
	function getStudentClassInfo($StudentID){
		global $intranet_db;

		$ClassNumberField = getClassNumberField("iu.");
		$sql = "	SELECT
						iu.ClassName,
						$ClassNumberField as ClassNumber
					FROM
						{$intranet_db}.INTRANET_USER AS iu
					WHERE
						iu.UserID = '$StudentID'
					LIMIT 1
				";
		$row = $this->returnArray($sql, 2);

		return $row[0];
	}
	
	# get Student Information and Return in Object
	function getStudentObject_print($my_user_id){
		global $intranet_rel_path, $intranet_root, $ec_iPortfolio;

		//$studentInCurrentYear  = false , since support with Alumni , student no need in current $studentInCurrentYear = false
		$student_info_arr = $this->GET_STUDENT_DATA($my_user_id,$studentInCurrentYear = false);
		$student_house = $this->GET_STUDENT_HOUSE($my_user_id);

		for ($i=0; $i<sizeof($student_info_arr); $i++)
		{
			// non-official photo
			$filepath = $intranet_root.$student_info_arr[$i]["PhotoLink"];
			$file_url = $intranet_rel_path.$student_info_arr[$i]["PhotoLink"];

			// official photo
			$official_photo_arr = $this->GET_OFFICIAL_PHOTO($student_info_arr[$i]["WebSAMSRegNo"]);
			list($photo_filepath, $photo_url) = $official_photo_arr;

			if (is_file($photo_filepath))
			{
				$student_info_arr[$i]["PhotoLink"] = "<img src=".$photo_url." border='1' width='100' height='130'>";
			}
			else if(is_file($filepath))
			{
				$student_info_arr[$i]["PhotoLink"] = "<img src=".$file_url." border='1' width='100' height='130'>";
			}
			else
			{
				$student_info_arr[$i]["PhotoLink"] = "<i>".$ec_iPortfolio['student_photo_no']."</i>";
			}

			$student_info_arr[$i]["WebSAMSRegNo"] = str_replace("#","",$student_info_arr[$i]["WebSAMSRegNo"]);

			#Trim the Date
			$tempDOB = explode(" ", $student_info_arr[$i]["DateOfBirth"]);
			$tempLM = explode(" ", $student_info_arr[$i]["DateModified"]);

			$student_info_arr[$i]["DateOfBirth"] = $tempDOB[0];
			$student_info_arr[$i]["DateModified"] = $tempLM[0];

			$student_info_arr[$i]["House"] = $student_house[$student_info_arr[$i]["UserID"]];
		}

		return $student_info_arr;
	}
	
	function GET_OLE_TABLE_STYLE()
	{
		$x = "";
		$x .= "<STYLE TYPE='text/css'>\n";
		$x .= ".ole_head_sub{font-family: Arial, Helvetica, Mingliu, Sans-Serif;font-size: 11px;font-weight: bold;}\n";
		$x .= ".ole_content{font-family: Arial, Helvetica, Mingliu, Sans-Serif;font-size: 11px;}\n";
		$x .= "</STYLE>\n";
		
		echo $x;
	}

	# Generate Student Full Report for Printing
	function generateFullReportPrint($StudentID, $Year="", $Semester="")
	{
		global $ec_iPortfolio, $eclass_filepath, $SchoolName;
		global $lpf_sturec; 

		// Page Break Style
		$page_break_style = "<STYLE TYPE='text/css'>\n.breakhere {page-break-after: always}\n</STYLE>\n";
		$page_break_here = "<br class='breakhere' />\n";

		////////////////////////////////////
		// load report settings
		$portfolio_report_config_file = $eclass_filepath."/files/portfolio_report_config.txt";
		$filecontent = trim(get_file_content($portfolio_report_config_file));
		$config_array = unserialize($filecontent);
		$NoPhotoConfig = $config_array[13];
		$NoStudentDetailsConfig = $config_array[14];
		$NoSchoolNameConfig = $config_array[15];

		// Get Report Settings
		$fieldArr[1] = "parent_record";
		$fieldArr[2] = "merit_record";
		$fieldArr[3] = "transcript_score_record";
		$fieldArr[4] = "transcript_rank_record";
		$fieldArr[5] = "transcript_grade_record";
		$fieldArr[6] = "transcript_stand_score_record";
		$fieldArr[7] = "activity_record";
		$fieldArr[8] = "award_record";
		$fieldArr[9] = "comment_record";
		$fieldArr[10] = "attendance_record";
		$fieldArr[11] = "service_record";
		$fieldArr[12] = "ole_record";

		for($i=1; $i<=12; $i++)
		{
			$temp = $config_array[$i];

			if($temp[1]==1)
			{
				${"show_".$fieldArr[$i]} = 1;

				switch($i)
				{
					case 3:
						$score_cmp = $temp[2];
						break;
					case 4:
						$rank_cmp = $temp[2];
						break;
					case 5:
						$grade_cmp = $temp[2];
						break;
					case 6:
						$stand_score = $temp[2];
						break;
					case 12:
						$show_remark = $temp[2];
						break;
				}

				$report_array[$temp[0]] = $fieldArr[$i];
			}
		}
		if(is_array($report_array))
			ksort($report_array);

		////////////////////////////////////////

		$StudentObjArr = $this->getStudentObject_print($StudentID);

		$rx = $page_break_style;
		for($sx=0; $sx<sizeof($StudentObjArr); $sx++)
		{
			$student_obj = $StudentObjArr[$sx];
			$student_id = $student_obj["UserID"];
			if($NoPhotoConfig==1)
			{
				$student_info_display = "&nbsp;";
			}
			else
			{
				# generate student photo table
				$photolink = $student_obj["PhotoLink"];
				$datemodified = $student_obj["DateModified"];
				$student_photo_display = $this->generateStudentPhotoTable($photolink, $datemodified);
			}
			# generate student info table
			$student_info_display = $this->generateStudentInfoTable_print($student_id, $student_obj, $NoStudentDetailsConfig);

			# retrieve class history
			$class_history_display = $this->generateClassHistoryTable_print($student_id, $Year);

			// student info part
			if(!$NoSchoolNameConfig)
			{
				$rx .= "<table cellSpacing=0 cellPadding=0 width='90%' border=1 bordercolor='black' align='center'>
					   <tr>
						  <td valign='middle' align='center' height='50'>
						   <FONT face=simhei size=6>".$SchoolName."</FONT>
						   </td>
						</tr>
						</table><br/>";
			}

			$personal_info = "<table cellSpacing=0 cellPadding=5 width='80%' align='center' border=0>
								<tr>";
			if(!$NoPhotoConfig)
			{
				$personal_info .= "<td valign='bottom'>
									".$student_photo_display."
								  </td>";
			}
			$personal_info .= "<td valign='top'>
							  ".$student_info_display."
							  </td>
							  <td align='center' valign='top'>
							 ".$class_history_display."
								</td>
							</tr>
							</table><br/><br/>";

			if($show_parent_record==1)
			{
				# generate student parent info table
				$parent_display = $this->generateParentInfoTable_print($student_id);

				// parent info part
				$parent_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['guardian_info']."</td>
								</tr>
								</table>
								<table width='100%' border='1' cellspacing='0' cellpadding='0' bgcolor='#000000' class='table_print'>
								<tr><td>".$parent_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_merit_record==1)
			{

				# generate merit summary table
				$merit_display = $this->displayStudentMeritSummary_print($student_id, $Year, $Semester);

				// student merit summary part
				$merit_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['merit']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000' align='center'>
								<tr><td>".$merit_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			$Printable = true; // set print grid on
			if($show_transcript_score_record==1)
			{
				# transcript by score
				$transcrip_score_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "Score", "Print", $score_cmp, $Year, $Semester, $Printable);

				// transcript part (by score)
				$transcript_score_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr><td height='30'>(".$ec_iPortfolio['display_by_score'].")</td></tr>
								<tr><td>".$transcrip_score_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_transcript_rank_record==1)
			{
				# transcript by rank
				$transcrip_rank_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "Rank", "Print", $rank_cmp, $Year, $Semester, $Printable);

				// transcript part (by rank)
				$transcript_rank_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr><td height='30'>(".$ec_iPortfolio['display_by_rank'].")</td></tr>
								<tr><td>".$transcrip_rank_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_transcript_grade_record==1)
			{
				# transcript by grade
				$transcrip_grade_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "Grade", "Print", $grade_cmp, $Year, $Semester, $Printable);

				// transcript part (by grade)
				$transcript_grade_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr><td height='30'>(".$ec_iPortfolio['display_by_grade'].")</td></tr>
								<tr><td>".$transcrip_grade_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_transcript_stand_score_record==1)
			{
				# transcript by grade
				$transcrip_stand_score_display = $lpf_sturec->generateAssessmentReport3($student_id, "", "StandardScore", "Print", $grade_cmp, $Year, $Semester, $Printable);

				// transcript part (by standard score)
				$transcript_stand_score_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['assessment_report']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr><td height='30'>(".$ec_iPortfolio['display_by_stand_score'].")</td></tr>
								<tr><td>".$transcrip_stand_score_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_activity_record==1)
			{
				# generate activity table
				$activity_display = $this->generateActivityTable_print($student_id, $Year, $Semester);

				// activity info part
				$activity_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['activity']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
								<tr><td>".$activity_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_award_record==1)
			{
				# generate award table

				$award_display = $this->generateAwardTable_print($student_id, $Year, $Semester);

				// award info part
				$award_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['award']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
								<tr><td>".$award_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_comment_record==1)
			{
				# generate teacher comment table
				$comment_display = $this->generateCommentTable_print($student_id, $Year, $Semester);

				// teacher comment info part
				$comment_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['teacher_comment']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
								<tr><td>".$comment_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_attendance_record==1)
			{
				# generate attendance table
				$attendance_display = $this->displayStudentAttendanceSummary_print($student_id, $Year, $Semester);

				// attendance info part
				$attendance_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['attendance']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
								<tr><td>".$attendance_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_service_record==1)
			{
				# generate attendance table
				$service_display = $this->generateServiceTable_print($student_id, $Year, $Semester);

				// attendance info part
				$service_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['service']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
								<tr><td>".$service_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			if($show_ole_record==1)
			{

				# generate attendance table
				# call convertSystemYearToOLEYear to convert system year to OLE year
				$ole_display = $this->generateOLETable_print($student_id, convertSystemYearToOLEYear($Year), $IsShowApprovedBy, $show_remark);

				// attendance info part
				$ole_record = "<table cellSpacing=0 cellPadding=5 width='80%' border=0 align='center'>
								<tr><td>
								<table border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='30' class='head_sub_4'>".$ec_iPortfolio['ole']."</td>
								</tr>
								</table>
								<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#000000'>
								<tr><td>".$ole_display."</td></tr>
								</table>
								</td>
								</tr>
								</table><br/><br/>";
			}

			$rx .= $personal_info;
			if(sizeof($report_array)!=0)
			{
				foreach ($report_array as $order => $values)
				{
					$rx .= ${$values};
				}
			}

			if ($sx<sizeof($StudentObjArr)-1)
			{
				$rx .= $page_break_here;
			}
		}
		return $rx;
	}
	
	function generateStudentPhotoTable($photo, $date)
	{
		global $ec_iPortfolio;

		$x = " <table width='100%' border='0' cellspacing='0' cellpadding='0'>
				  <tr>
					<td class='style16'><div align='left'>".$photo."</div></td>
				  </tr>
				  <tr>
					<td height='35' valign='bottom' class='style17' nowrap><div align='center'>
						<p align='left' class='style11'>
						  <span class='chi_content_12'>".$ec_iPortfolio['student_photo']."<br>
						  </span><span class='chi_content_10'>".$ec_iPortfolio['last_update'].":".$date."</span></p>
					  </div></td>
				  </tr>
				</table>";

		return $x;
	}
	
	# Student Info Table generated for report printing
	function generateStudentInfoTable_print($StudentID, $student_obj, $NoStudentDetailsConfig)
	{
		global $ec_iPortfolio, $ec_student_word, $profile_dob, $profile_gender;
		global $sys_custom;
		
		if($sys_custom['student_info_table_no_house'])
		$houseStyle = "style='display:none'";
		else
		$houseStyle = "";
		
		if($student_obj['DateOfBirth'] == "0000-00-00")
		$stdDOB = "--";
		else
		$stdDOB = $student_obj['DateOfBirth'];

		# get student admission date
		$admission_date = $this->GET_STUDENT_ADMISSION_DATE($StudentID);
		$admission_date = ($admission_date=="" || $admission_date=="0000-00-00") ? '--' : $admission_date;

		//$x .= "<table width='80%' border='0' cellpadding='5' cellspacing='1' bgcolor='#000000'>
		$x .= "<table width='80%' cellpadding='5' cellspacing='0' bgcolor='#000000' border=1 class='table_print'>
				<tr>
					<td height='30' class='head_sub_2' bgcolor='#FFFFFF' nowrap='nowrap'>".$ec_student_word['name_english']."</td>
					<td bgcolor='#FFFFFF' nowrap>".$student_obj["EnglishName"]."</td>
				</tr>
				<tr>
					<td bgcolor='#FFFFFF' height='30' class='head_sub_2' nowrap='nowrap'>".$ec_student_word['name_chinese']."</td>
					<td bgcolor='#FFFFFF' nowrap>".$student_obj['ChineseName']."</td>
				</tr>
				<tr>
					<td bgcolor='#FFFFFF' height='30' class='head_sub_2' nowrap='nowrap'>".$ec_student_word['registration_no']."</td>
					<td bgcolor='#FFFFFF' nowrap>".$student_obj['WebSAMSRegNo']."</td>
				</tr>";
		if(!$NoStudentDetailsConfig)
		{
			$x .= "<tr>
						<td bgcolor='#FFFFFF' height='30' class='head_sub_2' nowrap='nowrap'>".$profile_dob."</td>
						<td bgcolor='#FFFFFF' nowrap>".$stdDOB."</td>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF' height='30' class='head_sub_2' nowrap='nowrap'>".$profile_gender."</td>
						<td bgcolor='#FFFFFF' nowrap>".$student_obj['Gender']."</td>
					</tr>
					<tr ".$houseStyle.">
						<td bgcolor='#FFFFFF' height='30' class='head_sub_2' nowrap='nowrap'>".$ec_iPortfolio['house']."</td>
						<td bgcolor='#FFFFFF' nowrap>".($student_obj['House']==''?'--':$student_obj['House'])."</td>
					</tr>
					<tr>
						<td bgcolor='#FFFFFF' height='30' class='head_sub_2' nowrap='nowrap'>".$ec_iPortfolio['admission_date']."</td>
						<td bgcolor='#FFFFFF' nowrap><font face='Arial, Helvetica, sans-serif'>".$admission_date."</font></td>
					</tr>";
		}
		$x .= "</table>";

		return $x;
	}

	// Class History Table generated for report printing
	function generateClassHistoryTable_print($StudentID, $Year = "")
	{
		global $no_record_msg, $ec_iPortfolio, $intranet_session_language,$sys_custom;

		$class_history = $this->GET_STUDENT_CLASS_HISTORY($StudentID, $Year, $ParIncludeAcademicResult=false);
		
		# Eric Yip : If no year is specified, display current class also
		$curAYearID = Get_Current_Academic_Year_ID();
		$curAYear = new academic_year($curAYearID);
		if($Year == "" || $Year == $curAYear->YearNameEN || $Year == $curAYear->YearNameB5)
		  $current_class = $this->getStudentClassInfo($StudentID);

		$CurrentYearExist = false;
		for ($i=0;$i<count($class_history);$i++)
		{
			if ($class_history[$i]["Year"] == $curAYear->YearNameEN || $class_history[$i]["Year"] == $curAYear->YearNameB5)
			{
				$CurrentYearExist = true;
				break;
			}
		}

		//$rx = "<table width='100%' width='200' height='100%' border='0' bgcolor='black' cellpadding='5' cellspacing='1'>
		$rx = "<table width='100%' width='200' height='100%' border='1' bgcolor='black' cellpadding='5' cellspacing='0' class='table_print'>
				<tr>
				  <td height='20' bgcolor='#FFFFFF'>
					<div align='center' class='head_sub_2'>".$ec_iPortfolio['year']."</div></td>
				  <td height='20' bgcolor='#FFFFFF'>
					<div align='center' class='head_sub_2'>".$ec_iPortfolio['class']."</div></td>
				  <td height='20' bgcolor='#FFFFFF'>
					<div align='center' class='head_sub_2'>".$ec_iPortfolio['number']."</div></td>
				</tr>";

		//Modified by Wah, 2008-07-09 
		// If all year is chosen, then display all class history+current year class info
		// If previous year is chosen, display that year class history	
		// If current year is chosen, display current year class info
		
		if($Year == "")
		{
		//All Year	
			if(sizeof($class_history)!=0 || $Year == $curAYear->YearNameEN || $Year == $curAYear->YearNameB5 || count($current_class)>0)
			{
				//Previous class
				for($i=0; $i<sizeof($class_history); $i++)
				{

					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					//very special customzation for "2012-1108-0917-55071 - �{�K�R§�H���� - How to burn CD for last year F6 students"  , do it one time only , can be removed later//
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					if($sys_custom['iPf']['case']['2012-1108-0917-55071']){
						$_pattern = "/^s/i"; 
						if( (trim($class_history[$i]['Year']) == '2012-2013')  && 
							 preg_match($_pattern, $class_history[$i]['ClassName']) // class name start with 's'
						) 
						{
							continue; 
						}
					}
					//////////////////////////////
					// End of the customization //
					//////////////////////////////

					$rx .=	"
										<tr>
											<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$class_history[$i]['Year']."</div></td>
											<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$class_history[$i]['ClassName']."</div></td>
											<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$class_history[$i]['ClassNumber']."</div></td>
										</tr>
									";
				}
				
				//Current class
				if (!$CurrentYearExist)
				{
					if (count($current_class)>0)
					{
						$rx .=	"
											<tr>
												<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$curAYear->{'YearName'.strtoupper($intranet_session_language)}."</div></td>
												<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$current_class['ClassName']."</div></td>
												<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$current_class['ClassNumber']."</div></td>
											</tr>
										";								
					}
				}
			}
			else
			{
				$rx .= "<tr height=60><td colspan=3 align=center bgcolor='#FFFFFF'>&nbsp;$no_record_msg</td></td>";
			}
		}
		else
		{
			//specific year = current year
			if ($Year == $this->school_academic_year)
			{
				if (count($current_class)>0)
				{
					$rx .=	"
										<tr>
											<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$this->school_academic_year."</div></td>
											<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$current_class['ClassName']."</div></td>
											<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$current_class['ClassNumber']."</div></td>
										</tr>
									";								
				}				
				else
				{
					$rx .= "<tr height=60><td colspan=3 align=center bgcolor='#FFFFFF'>&nbsp;$no_record_msg</td></td>";
				}			
			}
			else
			{
			//specific year = previous year	
				if(sizeof($class_history) > 0)	
				{
					for($i=0; $i<sizeof($class_history); $i++)
					{
						$rx .=	"
											<tr>
												<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$class_history[$i]['Year']."</div></td>
												<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$class_history[$i]['ClassName']."</div></td>
												<td height='20' bgcolor='#FFFFFF'> <div align='center'>".$class_history[$i]['ClassNumber']."</div></td>
											</tr>
										";
					}					
				}
				else
				{
					$rx .= "<tr height=60><td colspan=3 align=center bgcolor='#FFFFFF'>&nbsp;$no_record_msg</td></td>";
				}			
			}
		}		  
		
		$rx .= "</table>";

		return $rx;
	}
	
	// Parent Info generated for printing
	function generateParentInfoTable_print($StudentID)
	{
		global $ec_student_word, $ec_iPortfolio, $no_record_msg;

		#retrieve student parent info
		$parent_obj = $this->GET_PARENT_OBJECT($StudentID);

		$parent_display = "";

		for($i=0; $i<sizeof($parent_obj); $i++)
		{
			$p_obj = $parent_obj[$i];

			//$parent_display .= "<table width='100%' border='0' cellspacing='1' cellpadding='5'>";
			$parent_display .= "<table width='100%' border='1' cellspacing='0' cellpadding='5' class='table_print'>";
			$parent_display .= ($p_obj["IsMain"]==1) ? "<tr>
								<td height='30' colspan='2' class='head_sub_2' bgcolor='#FFFFFF'>(".$ec_iPortfolio['main_guardian'].")</td>
								</tr>" : "<tr><td height='30' colspan='2' bgcolor='#FFFFFF'>&nbsp;</td></tr>";

			$parent_display .= "<tr>
								<td width='100' height='20' class='head_sub_2' bgcolor='#FFFFFF'>".$ec_student_word['name_english']."</td>
								<td height='20' bgcolor='#FFFFFF'>".$p_obj["EnName"]."</td>
							  </tr>
							  <tr>
								<td width='100' height='20' class='head_sub_2' bgcolor='#FFFFFF'>".$ec_student_word['name_chinese']."
								</td>
								<td height='20' bgcolor='#FFFFFF'>".$p_obj["ChName"]."</td>
							  </tr>
							  <tr>
								<td width='100' height='20' class='head_sub_2' bgcolor='#FFFFFF'>".$ec_iPortfolio['relation']."</td>
								<td height='20' bgcolor='#FFFFFF'>".$p_obj["Relation"]."</td>
							  </tr>
							  <tr>
								<td width='100' height='20' class='head_sub_2' bgcolor='#FFFFFF'>".$ec_iPortfolio['phone']."</td>
								<td height='20' bgcolor='#FFFFFF'>".($p_obj["Phone"]==""?"--":$p_obj["Phone"])."</td>
							  </tr>
							  <tr>
								<td width='100' height='20' class='head_sub_2' bgcolor='#FFFFFF'>".$ec_iPortfolio['em_phone']."</td>
								<td height='20' bgcolor='#FFFFFF'>".($p_obj["EmPhone"]==""?"--":$p_obj["EmPhone"])."</td>
							  </tr>
							</table>";
		}

		if(sizeof($parent_obj)==0)
		{
			$parent_display .= "<table width='100%' border='0' cellspacing='1' cellpadding='5'>
							 <tr>
								<td height='100' colspan='2' align='center' bgcolor='#FFFFFF'>".$no_record_msg."</td>
							  </tr>
							</table>";
		}

		return $parent_display;
	}
	
	# Student Merit Summary Table generated for printing
	function displayStudentMeritSummary_print($StudentID, $Year, $Semester)
	{
		global $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $i_Merit_TypeArray, $no_record_msg;
		global $merit_col_not_display;
		global $lpf_sturec, $lpf_slp, $intranet_db,$sys_custom;
		
		$result = $lpf_sturec->returnStudentMeritSummary($StudentID, $Year, $Semester);

		$title_array = $lpf_sturec->returnMeritReportTitleArray();
		$field_disabled_array = $lpf_sturec->returnMeritFieldAbility();
		$mtypeArr = $lpf_sturec->returnMeritTypeAbility();

		# get Years according to ASSESSMENT_STUDENT_SUBJECT_RECORD	
		$YearClassArr = $lpf_sturec->returnFilteredYearClass($StudentID, 0, $Year);		

		if($sys_custom['iPf']['case']['2012-1108-0917-55071']){
			$objDB = new libdb();
			$sql  = 'select userid as \'userid\' from '.$intranet_db.'.INTRANET_USER where classname like \'s%\'';

			$rsTmp = $objDB->returnResultSet($sql);

			$SUserIDAry = array();
			if(count($rsTmp) > 0){
				for($i = 0,$i_max = count($rsTmp); $i < $i_max; $i++){
					$SUserIDAry[] = $rsTmp[$i]['userid'];
				}
				if(count($SUserIDAry) > 0){
					if(in_array($StudentID,$SUserIDAry)){
						unset($YearClassArr['2012-2013']);
					}
				}
			}

			/*
			$SUserIDAry = array('15494','15502','15599','15646','15652','15998','16024','16027','16029','16030','16033','16034','16035','16037','16038','16039','16042','16044','16045','16046','16047','16052','16053','16054','16055','16057','16061','16062','16064','16065','16066','16069','16071','16072','16073','16074','16075','16076','16077','16078','16079','16080','16083','16085','16088','16089','16090','16091','16095','16096','16098','16100','16101','16104','16105','16109','16110','16113','16114','16117','16119','16120','16122','16123','16124','16125','16127','16128','16129','16134','16138','16140','16142','16143','16145','16146','16147','16148','16149','16150','16152','16153','16154','16155','16156','16158','16159','16164','16167','16169','16171','16174','16175','16176','16177','16178','16180','16182','16183','16184','16185','16186','16187','16188','16189','16193','16195','16200','16201','16202','16204','16205','16206','16207','16208','16209','16210','16211','16212','16214','16215','16216','16217','16218','16221','16222','16224','16225','16226','17016','17235','17237','17694','18132','18133','18135');
			*/
		}


/*
    # Eric Yip (20100211): Get semester array in year loop below
		if($Semester=="")
		{
			# retrieve school semesters
			list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		}
		else
			$SemesterArr[] = $Semester;
    if(is_array($SemesterArr) && $this->SemesterAscending)
      sort($SemesterArr);   # Eric Yip : sort semester

		$SemesterArr[] = $ec_iPortfolio['whole_year'];
*/
		
		$TypeCount = 0;
        //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
    {
			if ($merit_col_not_display[$i]==1) continue;
			if (!$this->ClassDisplay && $i==1) continue;    # Eric Yip : hide class
			if (!$field_disabled_array[$i])
			{
				$td_style = ($i<=2) ? " width='150' align='left'" : " width='100' align='center'";
				$TypeCount++;
				 $x .= "<td class='head_sub_2' bgcolor='#FFFFFF' $td_style><b>".$title_array[$i]."</b></td>";
			}
    }
		$x .= "</tr>\n";

		$num = 0;
		if(!empty($YearClassArr))
		{
			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
				
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				$SemesterArr = array_values(getSemesters($ay_id));
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				
				for($j=0; $j<sizeof($SemesterArr); $j++)
				{
					$sem = trim($SemesterArr[$j]);
					if (($Semester != "") && ($sem==$ec_iPortfolio['whole_year']))
					{
						continue;
					}

					if($year!=$CurrYear)
					{
						$x .= "<tr valign='middle' height=25>\n";
						$x .= "<td bgcolor='#FFFFFF'>$year</td>\n";
						$x .= $this->ClassDisplay?"<td bgcolor='#FFFFFF'>$ClassName</td>\n":"";   # Eric Yip : hide class
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr valign='middle' height=25>\n";
						$x .="<td bgcolor='#FFFFFF'>&nbsp;</td>\n";
						$x .= $this->ClassDisplay?"<td bgcolor='#FFFFFF'>&nbsp;</td>\n":"";   # Eric Yip : hide class
					}
					
					# Eric Yip : Emphasis for whole year
					$cell_colour = ($sem==$ec_iPortfolio['whole_year'])?"#CCCCCC":"#FFFFFF";
					$x .= "<td $style bgcolor='$cell_colour'>$sem</td>\n";
					foreach($mtypeArr as $type => $valid)
					{
						if($valid==1)
						{
							$value = ($result[$year][$sem][$type]!='') ? $result[$year][$sem][$type] : 0;
							$x .= "<td bgcolor='$cell_colour' align='center'>$value</td>\n";

							# get overall total
							if($sem==$ec_iPortfolio['whole_year'])
								$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;
						}
					}
					$x .= "</tr>\n";
				}
			}
			if ($Semester == "")
			{
				# Eric Yip : Normalize total merits/demerits
				$result[$ec_iPortfolio['total']] = $lpf_slp->returnNormalizedMerit($result[$ec_iPortfolio['total']]);
			
				$x .= "<tr valign='middle' class='chi_content_12' height=25>\n";
				$x .= "<td bgcolor='#FFFFFF'>&nbsp;</td>\n";
				$x .= $this->ClassDisplay?"<td bgcolor='#FFFFFF'>&nbsp;</td>\n":"";         # Eric Yip : hide class
				$x .= "<td bgcolor='#FFFFFF'>[".$ec_iPortfolio['total']."]</td>\n";
				foreach($mtypeArr as $type => $valid)
				{
					if($valid==1)
					{
						$x .= "<td bgcolor='#FFFFFF' align='center'>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</td>\n";
					}
				}
			}
			$x .= "</tr>\n";
		}
		else
		{
			$x .= "<tr valign='middle'><td height='100' colspan='".($this->ClassDisplay?$TypeCount+3:$TypeCount+2)."' align='center' bgcolor='#FFFFFF'>".$no_record_msg."</td></tr>";          # Eric Yip : hide class
		}
    $x .= "</table>\n";

    return $x;
  }
  
	# generate activiey table for report printing
	function generateActivityTable_print($StudentID, $Year, $Semester)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $lpf_sturec;
		
		$result = $lpf_sturec->returnActivityRecord($StudentID, $Year, $Semester);

		$title_array = array($ec_iPortfolio['year'], $ec_iPortfolio['semester'], $ec_iPortfolio['activity_name'], $ec_iPortfolio['role'], $ec_iPortfolio['performance']);

        //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
        {
           $x .= "<td class='head_sub_2' bgcolor='#FFFFFF' nowrap><b>".$title_array[$i]."</b></td>";
        }
		$x .= "</tr>\n";

        if (sizeof($result)==0)
        {
           return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF' nowrap>".$no_record_msg."</td></tr></table>\n";
        }

		$num = 0;
		$current_year = "";
		$current_sem = "";
        for($i=0; $i<sizeof($result); $i++)
        {
			list($year, $sem, $name, $role, $performance, $modified_date) = $result[$i];

            $x .= "<tr valign='middle' height=25>\n";

			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each year once only
			if($this->YearDescending)
			{
				if($year != $current_year)
				{
					$x .= $year;
					$current_year = $year;
					$current_sem = "";
				}
				else
					$x .= "&nbsp;";
			}
			else
				$x .= $year;
			$x .= "</td>\n";
			
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each semester once only
			if($this->SemesterAscending)
			{
				if($sem != $current_sem)
				{
					//$x .= $sem;
					$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
					$current_sem = $sem;
				}
				else
					$x .= "&nbsp;";
			}
			else
			{
				//$x .= $sem;
				$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
			}
			$x .= "</td>\n";

			$x .= "<td bgcolor='#FFFFFF'>$name</td>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>$role</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$performance</td>\n";
			$x .= "</tr>\n";
		}
    $x .= "</table>\n";

    return $x;
	}
	
	# generate award table for report printing
	function generateAwardTable_print($StudentID, $Year, $Semester)
	{
		global $ec_iPortfolio, $no_record_msg, $sys_custom;
		global  $lpf_sturec;
		
		$result = $lpf_sturec->returnAwardRecord($StudentID, $Year, $Semester);
		$title_array = array($ec_iPortfolio['year'], $ec_iPortfolio['semester'], $ec_iPortfolio['date'], $ec_iPortfolio['award_name']);
		if($sys_custom['iPortfolioHideRemark']==false)
			$title_array[] = $ec_iPortfolio['remark'];

        //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
        {
           $x .= "<td class='head_sub_2' bgcolor='#FFFFFF' nowrap><b>".$title_array[$i]."</b></td>";
        }
		$x .= "</tr>\n";

        if (sizeof($result)==0)
        {
           return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF' nowrap>".$no_record_msg."</td></tr></table>\n";
        }

		$num = 0;
		$current_year = "";
		$current_sem = "";
        for($i=0; $i<sizeof($result); $i++)
        {
			list($year, $sem, $date, $name, $remark, $last_modified) = $result[$i];

            $x .= "<tr valign='middle' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each year once only
			if($this->YearDescending)
			{
				if($year != $current_year)
				{
					$x .= $year;
					$current_year = $year;
					$current_sem = "";
				}
				else
					$x .= "&nbsp;";
			}
			else
				$x .= $year;
			$x .= "</td>\n";
			
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each semester once only
			if($this->SemesterAscending)
			{
				if($sem != $current_sem)
				{
					//$x .= $sem;
					$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
					$current_sem = $sem;
				}
				else
					$x .= "&nbsp;";
			}
			else
			{
				//$x .= $sem;
				$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
			}
			
			# 2013-02-01 added for catering blank value
			if($date==''){
				$date = "&nbsp;";
			}
			if($name==''){
				$name = "&nbsp;";
			}
			if($remark==''){
				$remark = "&nbsp;";
			}
			
			$x .= "</td>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>$date</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$name</td>\n";
			$x .= ($sys_custom['iPortfolioHideRemark']==false) ? "<td bgcolor='#FFFFFF'>$remark</td>\n" : "";
			$x .= "</tr>\n";
		}
    $x .= "</table>\n";

    return $x;
	}
	
	# generate teacher comment table for report printing
	function generateCommentTable_print($StudentID, $Year, $Semester)
	{
		global $ec_iPortfolio, $no_record_msg;
		global $lpf_sturec;
		
		$result = $lpf_sturec->returnCommentRecord($StudentID, $Year, $Semester);

		$title_array = array($ec_iPortfolio['year'], $ec_iPortfolio['semester'], $ec_iPortfolio['class_and_number'], $ec_iPortfolio['conduct_grade'], $ec_iPortfolio['comment_chi'], $ec_iPortfolio['comment_eng']);

        //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
        {
			$x .= "<td class='head_sub_2' bgcolor='#FFFFFF' nowrap><b>".$title_array[$i]."</b></td>";
        }
		$x .= "</tr>\n";

        if (sizeof($result)==0)
        {
           return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF' nowrap>".$no_record_msg."</td></tr></table>\n";
        }

		$num = 0;
		$current_year = "";
		$current_sem = "";
        for($i=0; $i<sizeof($result); $i++)
        {
			list($year, $sem, $class_number, $grade, $comment_chi, $comment_eng, $last_updated) = $result[$i];

			$role = ($role=="") ? "--" : $role;
			$performance = ($performance=="") ? "--" : $performance;

            $x .= "<tr valign='middle' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each year once only
			if($this->YearDescending)
			{
				if($year != $current_year)
				{
					$x .= $year;
					$current_year = $year;
					$current_sem = "";
				}
				else
					$x .= "&nbsp;";
			}
			else
				$x .= $year;
			$x .= "</td>\n";
			
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each semester once only
			if($this->SemesterAscending)
			{
				if($sem != $current_sem)
				{
					//$x .= $sem;
					$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
					$current_sem = $sem;
				}
				else
					$x .= "&nbsp;";
			}
			else
			{
				//$x .= $sem;
				$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
			}
			
			# 2013-02-01 added for catering blank value
			if($class_number==''){
				$class_number = "&nbsp;";
			}
			if($grade==''){
				$grade = "&nbsp;";
			}
			if($comment_chi==''){
				$comment_chi = "&nbsp;";
			}
			if($comment_eng==''){
				$comment_eng = "&nbsp;";
			}
			
			$x .= "</td>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>$class_number</td>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>$grade</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$comment_chi</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$comment_eng</td>\n";
			$x .= "</tr>\n";
		}
    $x .= "</table>\n";

    return $x;
	}
	
	# Display Student Attendance Summary
	function displayStudentAttendanceSummary_print($StudentID, $Year, $Semester)
	{
		global $ec_iPortfolio, $i_Profile_Absent, $i_Profile_Late, $i_Profile_EarlyLeave, $no_record_msg,$sys_custom;
    global $lpf_sturec, $intranet_db;

		# get the attendance summary
		$result = $lpf_sturec->returnStudentAttendanceSummary($StudentID, $Year, $Semester);

		# get Years and ClassName according to ASSESSMENT_STUDENT_SUBJECT_RECORD
	  # Eric Yip : Last parameter : Descending order		
		$YearClassArr = $this->returnFilteredYearClass($StudentID, 1, $Year);

		if($sys_custom['iPf']['case']['2012-1108-0917-55071']){
			$objDB = new libdb();
			$sql  = 'select userid as \'userid\' from '.$intranet_db.'.INTRANET_USER where classname like \'s%\'';

			$rsTmp = $objDB->returnResultSet($sql);

			$SUserIDAry = array();
			if(count($rsTmp) > 0){
				for($i = 0,$i_max = count($rsTmp); $i < $i_max; $i++){
					$SUserIDAry[] = $rsTmp[$i]['userid'];
				}
				if(count($SUserIDAry) > 0){
					if(in_array($StudentID,$SUserIDAry)){
						unset($YearClassArr['2012-2013']);
					}
				}
			}

			/*
			//UserID of S6A to S6E:
			$SUserIDAry = array('15494','15502','15599','15646','15652','15998','16024','16027','16029','16030','16033','16034','16035','16037','16038','16039','16042','16044','16045','16046','16047','16052','16053','16054','16055','16057','16061','16062','16064','16065','16066','16069','16071','16072','16073','16074','16075','16076','16077','16078','16079','16080','16083','16085','16088','16089','16090','16091','16095','16096','16098','16100','16101','16104','16105','16109','16110','16113','16114','16117','16119','16120','16122','16123','16124','16125','16127','16128','16129','16134','16138','16140','16142','16143','16145','16146','16147','16148','16149','16150','16152','16153','16154','16155','16156','16158','16159','16164','16167','16169','16171','16174','16175','16176','16177','16178','16180','16182','16183','16184','16185','16186','16187','16188','16189','16193','16195','16200','16201','16202','16204','16205','16206','16207','16208','16209','16210','16211','16212','16214','16215','16216','16217','16218','16221','16222','16224','16225','16226','17016','17235','17237','17694','18132','18133','18135');
			if(in_array($StudentID,$SUserIDAry)){
				unset($YearClassArr['2012-2013']);
			}
			*/
		}
/*
		if($Semester=="")
		{
			# retrieve school semesters
			list($SemesterArr, $ShortSemesterArr) = $this->getSemesterNameArrFromIP();
		}
		else
			$SemesterArr[] = $Semester;
    if(is_array($SemesterArr) && $this->SemesterAscending)
      sort($SemesterArr);   # Eric Yip : sort semester

		$SemesterArr[] = $ec_iPortfolio['whole_year'];
*/
        //$x = "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x = "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
        $x .= "<tr height=25>\n";
		$x .= "<td nowrap class='head_sub_2' width='20%' bgcolor='#FFFFFF'><b>".$ec_iPortfolio['year']."</b></td>";
		$x .= $this->ClassDisplay?"<td nowrap class='head_sub_2' width='20%' bgcolor='#FFFFFF'><b>".$ec_iPortfolio['class']."</b></td>":"";   # Eric Yip : hide class
		$x .= "<td nowrap class='head_sub_2' width='15%' bgcolor='#FFFFFF'><b>".$ec_iPortfolio['semester']."</b></td>";
		$x .= "<td nowrap class='head_sub_2' width='15%' bgcolor='#FFFFFF' align='center'><b>".$i_Profile_Absent."</b></td>";
		$x .= "<td nowrap class='head_sub_2' width='15%' bgcolor='#FFFFFF' align='center'><b>".$i_Profile_Late."</b></td>";
		$x .= "<td nowrap class='head_sub_2' width='15%' bgcolor='#FFFFFF' align='center'><b>".$i_Profile_EarlyLeave."</b></td>";
		$x .= "</tr>\n";

		if(!empty($YearClassArr))
		{
			$num = 0;
			foreach($YearClassArr as $year => $ClassName)
			{
				$CurrYear = "";
				
				# Eric Yip (20100211): Get semester array according to year name
				# (Not work for years with same name)
				unset($SemesterArr);
				$sql = "SELECT AcademicYearID FROM {$intranet_db}.ACADEMIC_YEAR WHERE YearNameEN = '".$year."'";
				$ay_id = current($this->returnVector($sql));
				$SemesterArr = array_values(getSemesters($ay_id));
				$SemesterArr[] = $ec_iPortfolio['whole_year'];
				
				unset($SemesterEnArr);
				$SemesterEnArr = array_values(getSemesters($ay_id, $ReturnAsso=1, $ParLang='en'));
				$SemesterEnArr[] = $ec_iPortfolio['whole_year'];
				
				for($j=0; $j<sizeof($SemesterArr); $j++)
				{					
					$sem = trim($SemesterArr[$j]);
					$semEn = trim($SemesterEnArr[$j]);
					
					if($Semester!="")
					{	
						if($sem==$ec_iPortfolio['whole_year'])		
							continue;
					}					
					if($year!=$CurrYear)
					{
						$x .= "<tr valign='middle' height=25>\n";
						$x .= "<td bgcolor='#FFFFFF'>$year</td>\n";
						$x .= $this->ClassDisplay?"<td bgcolor='#FFFFFF'>$ClassName</td>\n":""; 
						$num++;
						$CurrYear = $year;
					}
					else
					{
						$x .= "<tr valign='middle' height=25>\n";
						$x .= "<td bgcolor='#FFFFFF'>&nbsp;</td>\n";
						$x .= $this->ClassDisplay?"<td bgcolor='#FFFFFF'>&nbsp;</td>\n":"";   # Eric Yip : hide class
					}
					# Eric Yip : Emphasis for whole year
					$cell_colour = ($sem==$ec_iPortfolio['whole_year'])?"#CCCCCC":"#FFFFFF";
					$x .= "<td bgcolor='$cell_colour'>$sem</td>\n";
					for($type=1; $type<=3; $type++)
					{
						$value = ($result[$year][$semEn][$type]!='') ? $result[$year][$semEn][$type] : 0;
						$x .= "<td bgcolor='$cell_colour' align='center'>$value</td>\n";

						if($sem==$ec_iPortfolio['whole_year'])
							$result[$ec_iPortfolio['total']][$type] = $result[$ec_iPortfolio['total']][$type] + $value;

					}
					$x .= "</tr>\n";
				}
			}
			
			if($Semester=="")
			{			
				$x .= "<tr valign='middle' class='chi_content_12' height=25>\n";
				$x .= "<td bgcolor='#FFFFFF'>&nbsp;</td>\n";
				$x .= $this->ClassDisplay?"<td bgcolor='#FFFFFF'>&nbsp;</td>\n":"";   # Eric Yip : hide class
				$x .= "<td bgcolor='#FFFFFF'>[".$ec_iPortfolio['total']."]</td>\n";
				for($type=1; $type<=3; $type++)
				{
					$x .= "<td bgcolor='#FFFFFF' align='center'>".($result[$ec_iPortfolio['total']][$type]!='' ? $result[$ec_iPortfolio['total']][$type] : 0)."</td>\n";
				}
				$x .= "</tr>\n";
			}
		}
		else
		{
			$x .= "<tr valign='middle'><td height='100' colspan='7' align='center' bgcolor='#FFFFFF'>".$no_record_msg."</td></tr>";
		}
		$x .= "</table>\n";

		return $x;
	}
	
	# generate service table for report printing
	function generateServiceTable_print($StudentID, $Year, $Semester)
	{
		global $ec_iPortfolio, $no_record_msg;
    global $lpf_sturec;
    
		$result = $lpf_sturec->returnServiceRecord($StudentID, $Year, $Semester);

		$title_array = array($ec_iPortfolio['year'], $ec_iPortfolio['semester'], $ec_iPortfolio['date'], $ec_iPortfolio['service_name'], $ec_iPortfolio['role'], $ec_iPortfolio['performance']);

        //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
        {
           $x .= "<td class='head_sub_2' bgcolor='#FFFFFF' nowrap><b>".$title_array[$i]."</b></td>";
        }
		$x .= "</tr>\n";

        if (sizeof($result)==0)
        {
           return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF' nowrap>".$no_record_msg."</td></tr></table>\n";
        }

		$num = 0;
		$current_year = "";
		$current_sem = "";
        for($i=0; $i<sizeof($result); $i++)
        {
			list($year, $sem, $date, $name, $role, $performance, $modified_date) = $result[$i];

            $x .= "<tr valign='middle' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each year once only
			if($this->YearDescending)
			{
				if($year != $current_year)
				{
					$x .= $year;
					$current_year = $year;
					$current_sem = "";
				}
				else
					$x .= "&nbsp;";
			}
			else
				$x .= $year;
			$x .= "</td>\n";
			
			$x .= "<td bgcolor='#FFFFFF' nowrap>";
			# Eric Yip : Show each semester once only
			if($this->SemesterAscending)
			{
				if($sem != $current_sem)
				{
					//$x .= $sem;
					$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
					$current_sem = $sem;
				}
				else
					$x .= "&nbsp;";
			}
			else
			{
				//$x .= $sem;
				$x .= Get_Lang_Selection($this->semEN2B5map_arr[$sem], $sem);
			}
			$x .= "</td>\n";
			$x .= "<td bgcolor='#FFFFFF' nowrap>$date</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$name</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$role</td>\n";
			$x .= "<td bgcolor='#FFFFFF'>$performance</td>\n";
			$x .= "</tr>\n";
		}
        $x .= "</table>\n";

        return $x;
	}

	# generate OLE table for report printing
	function generateOLETable_print($StudentID, $Year, $IsShowApprovedBy=false, $ShowFields="")
	{
		global $ec_iPortfolio, $no_record_msg;
		global $lpf_sturec, $lpf_slp;

		# Add 3rd parameter to show all category
		$result = $lpf_sturec->returnOLERecord($StudentID, $Year, true);

		$title_array = array($ec_iPortfolio['date']."/".$ec_iPortfolio['period'], $ec_iPortfolio['title']);
		
		$OLE_fields =	array	(
													$ec_iPortfolio['category'],
													$ec_iPortfolio['ele'],
													$ec_iPortfolio['ole_role'],
													$ec_iPortfolio['hours'],
													$ec_iPortfolio['achievement'],
													$ec_iPortfolio['details'],
													$ec_iPortfolio['approved_by'],
													$ec_iPortfolio['school_remark']
												);
												
		$t_component = explode(",", $ShowFields);

		for($k=0; $k<count($OLE_fields); $k++)
		{
			if($OLE_fields[$k] == $ec_iPortfolio['approved_by'])
			{
				if($IsShowApprovedBy)
					$title_array[] = $OLE_fields[$k];
			}
			else if($OLE_fields[$k] == $ec_iPortfolio['school_remark'])
			{
				 if(in_array($k-1, $t_component))
					$title_array[] = $OLE_fields[$k];
			}
			else if(in_array($k, $t_component))
				$title_array[] = $OLE_fields[$k];
		}

//		if ($IsShowApprovedBy)
//			$title_array[] = $ec_iPortfolio['approved_by'];
			
//		if($IsShowRemarks)
//			$title_array[] = $ec_iPortfolio['school_remark'];

        //$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='1' align='center'>\n";
        $x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
        {
           $x .= "<td class='head_sub_2' bgcolor='#FFFFFF'><b>".$title_array[$i]."&nbsp;</b></td>";
        }
		$x .= "</tr>\n";

        if (sizeof($result)==0)
        {
           return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF'>".$no_record_msg."&nbsp;</td></tr></table>\n";
        }

		# Add parameter to show all ELE
		$DefaultELEArray = $lpf_slp->GET_ELE(true);
		$num = 0;
        for($i=0; $i<sizeof($result); $i++)
        {
			list($period, $title, $category, $role, $hours, $achievement, $details, $approved_by, $process_date, $remark, $modified, $statdate, $ele) = $result[$i];

			$ELEIDArray = explode(",", $ele);
			unset($ELEArray);
			for($m=0; $m<sizeof($ELEIDArray); $m++)
			{
				$tmp = trim($ELEIDArray[$m]);
				$ELEArray[] = $DefaultELEArray[$tmp];
			}
			$DisplayELE = (!empty($ELEArray)) ? implode("<br/>", $ELEArray) : "";

            $x .= "<tr valign='top' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF'>$period</td>\n";
			$x .= "<td bgcolor='#FFFFFF'><div style='width:120px'>$title</div></td>\n";
			
			if(in_array(0, $t_component))
				$x .= "<td bgcolor='#FFFFFF'>{$category}&nbsp;</td>\n";
			if(in_array(1, $t_component))
				$x .= "<td bgcolor='#FFFFFF'>{$DisplayELE}&nbsp;</td>\n";
			if(in_array(2, $t_component))
				$x .= "<td bgcolor='#FFFFFF'>{$role}&nbsp;</td>\n";
			if(in_array(3, $t_component))
				$x .= "<td bgcolor='#FFFFFF'>{$hours}&nbsp;</td>\n";
			if(in_array(4, $t_component))
				$x .= "<td bgcolor='#FFFFFF'>{$achievement}&nbsp;</td>\n";
			if(in_array(5, $t_component))
			$x .= "<td bgcolor='#FFFFFF'>{$details}&nbsp;</td>\n";
			if ($IsShowApprovedBy)
			{
				$x .= "<td bgcolor='#FFFFFF'>{$approved_by}&nbsp;</td>\n";
			}
			if(in_array(6, $t_component))
			{
				$x .= "<td bgcolor='#FFFFFF'>{$remark}&nbsp;</td>\n";
			}
			$x .= "</tr>\n";
		}
        $x .= "</table>\n";

        return $x;
	}
	
###############################################################################
#	For Transcript Printing
###############################################################################
	
	function getTemplateConfig(){
		$template_obj['template'] = 0;
		$template_obj['report_style'] = "A";

		$template_obj["config"] = $this->retrievePortfolioTranscriptConfig();

		return $template_obj;
	}
	
	# read CSV data from file
	function loadCSVData($my_template_folder){
		$csv_file = $my_template_folder . "/csv/data.csv";
		$columns = array();
		$records = array();

		if (file_exists($csv_file))
		{

			//$lo = new phpduoFileSystem();
			$lo = new libfilesystem();
			$data = $lo->file_read_csv($csv_file);
			$header_row = array_shift($data);

			// get column name
			for ($i=1; $i<sizeof($header_row); $i++)
			{
				$columns[] = $header_row[$i];
			}

			// get data
			for ($i=0; $i<sizeof($data); $i++)
			{
				$RegNo = str_replace("#", "", $data[$i][0]);
				$records[$RegNo] = array();
				for ($j=0; $j<sizeof($columns); $j++)
				{
					$value = $data[$i][$j+1];
					$records[$RegNo][] = ($value=="") ? "&nbsp;" : $value;
				}
			}
		}

		return array($columns, $records);
	}
	
	# Patterns to be Replaced: Template Code And Some Paths Used in HTML
	function loadReplacementPattern($report_type, $CSV_columns){
		# iPortfolio supports
		$patterns = array (
						"/\"images\//",
						"/'images\//",
						"/\"css\//",
						"/'css\//",
						"/<=IP_STUDENT_NAME_ENG=>/",
						"/<=IP_DATE_OF_BIRTH=>/",
						"/<=IP_PLACE_OF_BIRTH=>/",

						"/<=IP_NATIONALITY=>/",
						"/<=IP_ADDRESS=>/",
						"/<=IP_ADMISSION_DATE=>/",
						"/<=IP_GRADUATE_DATE=>/",
						"/<=IP_STUDENT_PHOTO=>/",

						"/<=IP_REPORT_SHEET_".$report_type."=>/",
						"/<=IP_SCHOOL_NAME=>/",
						"/<=IP_SCHOOL_ADDRESS=>/",
						"/<=IP_TRANSCRIPT_DESCRIPTION=>/",
						"/<=CSV_DATE_OF_ISSUED=>/",
						"/<=CSV_REFERENCE_NO=>/"
					);

		for ($i=0; $i<sizeof($CSV_columns); $i++)
		{
			$patterns[] = "/<=".$CSV_columns[$i]."=>/";
		}

		return $patterns;
	}
	
	# load template used
	function loadTranscriptTemplate($my_template_folder){
		$template_file = $my_template_folder . "/index.html";
		$template_file_cust = $my_template_folder . "/index_cust.html";
		
//		if (!file_exists($template_file)) {
//			$template_file = $my_template_folder . "/index.htm";
//		}
		if (file_exists($template_file_cust)) {
			$template_file = $template_file_cust;
		}
		else {
			if (!file_exists($template_file)) {
				$template_file = $my_template_folder . "/index.htm";
			}
		}
		

		$template_html = get_file_content($template_file);

		$template_html_lower = strtolower($template_html);
		$body_start_pos = strpos($template_html_lower, "<body");
		$body_end_pos = strpos($template_html_lower, "</body");
		$template_html_header = substr($template_html, 0, $body_start_pos);
		$template_html_body = substr($template_html, $body_start_pos, $body_end_pos-$body_start_pos);
		$template_html_footer = substr($template_html, $body_end_pos);

		return array("header"=>$template_html_header, "body"=>$template_html_body, "footer"=>$template_html_footer);
	}
	
	# Get Student's Data
	function getStudentRecords($my_students){

		global $ec_iPortfolio;

		if ($my_students==0)
		{
			$students[] = array(
									"UserID" => "&nbsp;",
									"RegNo" => "&nbsp;",
									"EnglishName" => "&nbsp;",
									"DOB" => "&nbsp;",
									"POB" => "&nbsp;",
									"Nationality" => "&nbsp;",
									"Address" => "&nbsp;",
									"AdmissionDate" => "&nbsp;",
									//"GraduateDate" => "&nbsp;",
									"PhotoImg" => "&nbsp;"
								);
		} else
		{
			# retrieve students' data of given students
			$student_obj = $this->getStudentObject_print($my_students);

			# parse the records into 2D array
			for ($i=0; $i<sizeof($student_obj); $i++)
			{
				$student = $student_obj[$i];
				$students[] = array(
									"UserID" => (trim($student['UserID']!="")) ? $student['UserID'] : "&nbsp;",
									"RegNo" => $student['WebSAMSRegNo'],
									"EnglishName" => (trim($student['EnglishName']!="")) ? $student['EnglishName'] : "&nbsp;",
									"DOB" => (trim($student['DateOfBirth']!="")) ? $student['DateOfBirth'] : "&nbsp;",
									"POB" => (trim($student['PlaceOfBirth']!="")) ? $student['PlaceOfBirth'] : "&nbsp;",
									"Nationality" => (trim($student['Nationality']!="")) ? $student['Nationality'] : "&nbsp;",
									"Address" => (trim($student['Address']!="")) ? $student['Address'] : "&nbsp;",
									"AdmissionDate" => (trim($student['AdmissionDate']!="")) ? $student['AdmissionDate'] : "&nbsp;",
									//"GraduateDate" => "&nbsp;",
									"PhotoImg" => str_replace($ec_iPortfolio["student_photo_no"], "&nbsp;", $student[PhotoLink])
								);
			}
		}

		return $students;
	}

	# get Student's Assessment Report
	function getAssessmentReport($StudentID, $report_style, $Year="", $Semester=""){
		// YuEn: will extend to provid more choices
		global $lpf_sturec;
		
		$Printable = true;
		switch ($report_style)
		{
			case "A":
			default:
				$rx = $lpf_sturec->generateAssessmentReport3($StudentID, "", "", "Print", 1, $Year, $Semester, $Printable);
				break;
		}

		return $rx;
	}
	
	# Generate Student Transcript for Printing
	# input student_list or $my_students==0 for template preview
	function generateTranscriptPrint($my_students, $Year="", $Semester="", $IsShowApprovedBy = false)
	{
		global $admin_root_path, $admin_url_path, $file_path_name, $li_npf, $image_path, $eclass_httppath;

		############################ Initialization ############################

		// get Report Sheet Style
		$templat_config = $this->getTemplateConfig();
		$ip_school_address = ($templat_config['config']['school_address']) ?  nl2br($templat_config['config']['school_address']) : "&nbsp;";
		$ip_transcript_description = ($templat_config['config']['school_description']) ?  $templat_config['config']['school_description'] : "&nbsp;";
		$PhotoConfig = $templat_config['config']['PhotoConfig'];

		// get Template Path
		$template_path = $file_path_name."/portfolio/transcript/template_".$templat_config["template"];
		$template_path_root = $admin_root_path."/".$template_path;
		$template_path_url = "http://".$eclass_httppath.$admin_url_path."/".$template_path;

		
//		debug_r($template_path);
//				debug_r($template_path_root);
//		debug_r($template_path_url);

		// get CSV Columns
		list($CSV_columns, $CSV_records) = $this->loadCSVData($template_path_root);

		# get school name
		$ip_school_name = $li_npf->returnSchoolName();

		// get Pattern
		$patterns = $this->loadReplacementPattern($templat_config["report_style"], $CSV_columns);

		// Page Break Style
		$page_break_style = "<STYLE TYPE='text/css'>\nP.breakhere {page-break-before: always}\n</STYLE>\n";
		$page_break_here = "<p class='breakhere'></p>\n";

		// Transcript Template
		$template_obj = $this->loadTranscriptTemplate($template_path_root);
		

		$Student_Records = $this->getStudentRecords($my_students);

		############################ Each Student's Report ############################

		for ($i=0; $i<sizeof($Student_Records); $i++)
		{
			$student_obj = $Student_Records[$i];
			// retrieve student's data
			$image_new_path1 = "\"".$template_path_url."/images/";
			$image_new_path2 = "'".$template_path_url."/images/";
			$css_new_path1 = "\"".$template_path_url."/css/";
			$css_new_path2 = "'".$template_path_url."/css/";
			$ip_student_name_eng = $student_obj["EnglishName"];
			$ip_date_of_birth = $student_obj["DOB"];
			if ($ip_date_of_birth == "0000-00-00")
			{
				$ip_date_of_birth = "--";
			}
			$ip_place_of_birth = $student_obj["POB"];
			$ip_nationality = $student_obj["Nationality"];
			$ip_address = $student_obj["Address"];
			$ip_admission_date = $student_obj["AdmissionDate"];
			$ip_graduate_date = $student_obj["GraduateDate"];
			$photo = ($my_students==0) ? "<img src=\"{$image_path}/photo_m.gif\" width=100 height=130 border=1>" : (strpos($student_obj["PhotoImg"], "&nbsp;")?"<div style=\"width:100px;height:130px;border-style:solid;border-width:1px\"><i>Photo Here</i></div>":$student_obj["PhotoImg"]);
			$ip_student_photo = ($PhotoConfig) ? "&nbsp;" : $photo;

			// generate the report sheet used
			$ip_report_sheet = $this->getAssessmentReport($student_obj["UserID"], $templat_config['report_style'], $Year, $Semester);

			# Contents of Replacement (ATTENTION: SAME Order as Patterns to be Replaced)

			// CSV
			$replacements_csv = array();
			$CSV_data = $CSV_records[$student_obj["RegNo"]];
			for ($j=0; $j<sizeof($CSV_data); $j++)
			{
				$replacements_csv[] = $CSV_data[$j];
				${strtolower($CSV_columns[$j])} = $CSV_data[$j];
			}
			// iPortfolio
			$replacements = array (
							$image_new_path1,
							$image_new_path2,
							$css_new_path1,
							$css_new_path2,
							$ip_student_name_eng,
							$ip_date_of_birth,
							$ip_place_of_birth,

							$ip_nationality,
							$ip_address,
							$ip_admission_date,
							$ip_graduate_date,
							$ip_student_photo,

							$ip_report_sheet,
							$ip_school_name,
							$ip_school_address,
							$ip_transcript_description
							);

			$replacements = array_merge($replacements, $replacements_csv);

			$rx .= preg_replace($patterns, $replacements, $template_obj["body"]);

			if ($i<sizeof($Student_Records)-1)
			{
				$rx .= $page_break_here;
			}
		}

		############################### All Reports ###############################

		# update Template Header for CSS and Image Paths
		$template_obj["header"] = preg_replace($patterns, $replacements, $template_obj["header"]) . $page_break_style;

		# finalize With Header and Footer Merged
		$rx = $template_obj["header"] . $rx . $template_obj["footer"];

		return $rx;
	}
	
###############################################################################
#	For Student Learning Profile Report Printing
###############################################################################

	# Get Student data for printing
	function GET_STUDENT_PARTICULARS_SLP($ParStudentIDArr)
	{
		global $intranet_db, $eclass_db, $eclass_filepath, $intranet_root;

		$SchoolName = GET_SCHOOL_NAME();

		# School address
		$SchoolAddress = get_file_content($eclass_filepath."/files/portfolio_transcript.txt");
		$SchoolAddress = unserialize($SchoolAddress);
		$SchoolAddress = $SchoolAddress['school_address'];

		if(file_exists($intranet_root."/file/school_data.txt"))
		{
			$admin_console_content = explode("\n",get_file_content($intranet_root."/file/school_data.txt"));
			$SchoolPhone = $admin_console_content[2];

			# unset school address loaded from portfolio_transcript
			# and load the address from admin setting
			if(count($admin_console_content) > 3 && $admin_console_content[3] != "")
			{
				$SchoolAddress = "";
				for($i=3; $i<count($admin_console_content); $i++)
					$SchoolAddress .= $admin_console_content[$i]."\n";
			}
		}

		# Get WebSAMS school code
		# (Copy from libwebsamsattendancecode.php)
		$sql =	"
							SELECT
								SchoolID
							FROM
								INTRANET_WEBSAMS_ATTENDANCE_BASIC
							WHERE
								RecordID = 0
						";
		$temp = $this->returnVector($sql);
		$SchoolID = $temp[0];

		# Get student info
		$sql =	"
							SELECT
								iu.UserID,
								iu.EnglishName,
								iu.ChineseName,
								IF(iu.DateOfBirth LIKE '%0000-00-00%', '', iu.DateOfBirth) AS DateOfBirth,
								/* ps.AdmissionDate, */
								IF (iups.AdmissionDate is null, ps.AdmissionDate, iups.AdmissionDate) as AdmissionDate,
								iu.HKID,
								iu.Gender,
								iu.ClassName,
								iu.ClassNumber,
								iu.WebSAMSRegNo,
								iu.UserLogin
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
								INNER JOIN {$intranet_db}.INTRANET_USER as iu ON iu.UserID = ps.UserID
								Left Outer Join {$intranet_db}.INTRANET_USER_PERSONAL_SETTINGS as iups On (iu.UserID = iups.UserID)
							WHERE
								iu.UserID IN ('".implode("','", (array)$ParStudentIDArr)."')
						";

		$SPArr = $this->returnArray($sql);

		# Reorganize data into associative array
		$ReturnArr = array();
		for($i=0; $i<count($SPArr); $i++)
		{
			$ReturnArr[$SPArr[$i]['UserID']]['EnglishName'] = $SPArr[$i]['EnglishName'];
			$ReturnArr[$SPArr[$i]['UserID']]['ChineseName'] = $SPArr[$i]['ChineseName'];
			$ReturnArr[$SPArr[$i]['UserID']]['DateOfBirth'] = substr($SPArr[$i]['DateOfBirth'], 0, 10);
			$ReturnArr[$SPArr[$i]['UserID']]['SchoolName'] = $SchoolName;
			$ReturnArr[$SPArr[$i]['UserID']]['AdmissionDate'] = $SPArr[$i]['AdmissionDate'];
			$ReturnArr[$SPArr[$i]['UserID']]['SchoolAddress'] = $SchoolAddress;
			$ReturnArr[$SPArr[$i]['UserID']]['SchoolPhone'] = $SchoolPhone;
			$ReturnArr[$SPArr[$i]['UserID']]['HKID'] = $SPArr[$i]['HKID'];
			$ReturnArr[$SPArr[$i]['UserID']]['Gender'] = $SPArr[$i]['Gender'];
			$ReturnArr[$SPArr[$i]['UserID']]['SchoolID'] = $SchoolID;
			$ReturnArr[$SPArr[$i]['UserID']]['UserLogin'] = $SPArr[$i]['UserLogin'];
			
			# For LaSalle Customarization
			$ReturnArr[$SPArr[$i]['UserID']]['WebSAMSRegNo'] = $SPArr[$i]['WebSAMSRegNo'];
			$ReturnArr[$SPArr[$i]['UserID']]['ClassName'] = $SPArr[$i]['ClassName'];
			$ReturnArr[$SPArr[$i]['UserID']]['ClassNumber'] = $SPArr[$i]['ClassNumber'];
			$ReturnArr[$SPArr[$i]['UserID']]['OfficialPhoto'] = $this->GET_OFFICIAL_PHOTO($SPArr[$i]['WebSAMSRegNo']);
		}

		return $ReturnArr;
	}
	
	# Get academic performance
	function GET_ACADEMIC_PERFORMANCE_SLP($ParStudentIDArr)
	{
		global $intranet_db, $eclass_db, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		$libscm = new subject_class_mapping();
		$SubjectInfoOrderedArr = $libscm->Get_Subject_List();

		# Get subject mapping
		# (subject code => array(EnglishName, ChineseName))
		$sql =	"
							SELECT
								subj.EN_SNAME,
								subj.EN_DES,
								subj.CH_DES
							FROM
								{$intranet_db}.ASSESSMENT_SUBJECT as subj
								Left Outer Join
								{$intranet_db}.LEARNING_CATEGORY as lc On (subj.LearningCategoryID = lc.LearningCategoryID)
							Where
								subj.RecordStatus = 1
							Order By
								lc.DisplayOrder, subj.DisplayOrder
						";
		$temp = $this->returnArray($sql);

		for($i=0; $i<count($temp); $i++)
		{
			$SubjArr[$temp[$i]['EN_SNAME']] = array($temp[$i]['EN_DES'], $temp[$i]['CH_DES']);
		}

		# Get assessment records
		$sql =	"
							SELECT
								assr.UserID,
								assr.Year,
								y.WEBSAMSCode,
								assr.SubjectCode,
								assr.Score,
								assr.Grade
							FROM
								{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr
							LEFT JOIN
								{$intranet_db}.ASSESSMENT_SUBJECT as subj
							ON
								assr.SubjectCode = subj.EN_SNAME
							Left Outer Join
							{$intranet_db}.LEARNING_CATEGORY as lc 
							On 
								(subj.LearningCategoryID = lc.LearningCategoryID)
							LEFT JOIN
								{$intranet_db}.YEAR_CLASS as yc
							ON
								assr.ClassName = yc.ClassTitleEN
							LEFT JOIN
								{$intranet_db}.YEAR as y
							ON
								yc.YearID = y.YearID
							WHERE
								assr.UserID IN (".implode(',', $ParStudentIDArr).") AND
								(assr.SubjectComponentCode IS NULL Or assr.SubjectComponentCode = '') AND
								y.WEBSAMSCode IN ('S4', 'S5', 'S6', 'F4', 'F5', 'F6') AND
								(assr.Score > 0 OR (assr.Score <= 0 AND assr.Grade != '' AND assr.Grade IS NOT NULL && assr.OrderMeritForm > 0)) And
								subj.RecordStatus = 1
							ORDER BY
								assr.Year DESC, lc.DisplayOrder, subj.DisplayOrder
						";
		$APArr = $this->returnArray($sql);

		$ReturnArr = array();
		$YearArr = array();
		for($i=0; $i<count($APArr); $i++)
		{
			$ReturnArr[$APArr[$i]['UserID']][$APArr[$i]['SubjectCode']][$APArr[$i]['Year']]['Score'] = $APArr[$i]['Score'];
			$ReturnArr[$APArr[$i]['UserID']][$APArr[$i]['SubjectCode']][$APArr[$i]['Year']]['Grade'] = $APArr[$i]['Grade'];

			$YearArr[$APArr[$i]['Year']] = $APArr[$i]['WEBSAMSCode'];
		}

		return array($SubjArr, $YearArr, $ReturnArr, $SubjectInfoOrderedArr);
	}
	
	# Get academic performance
	function GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($ParStudentIDArr, $includeComponentSubject=false, $ParWebSAMSCodeAry='',$ignoreAssessment = true)
	{
		global $intranet_db, $eclass_db, $PATH_WRT_ROOT, $sys_custom;
		
		//2013-0117-1206-59073 - do not load subject_class_mapping.php for performance tuning
		//include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		//$libscm = new subject_class_mapping();
		//$SubjectInfoOrderedArr = $libscm->Get_Subject_List();
		
		if ($ParWebSAMSCodeAry === '') {
			$conds_webSAMSCode = " And y.WEBSAMSCode IN ('S4', 'S5', 'S6', 'S7', 'F4', 'F5', 'F6', 'F7') ";
		}
		else {
			$conds_webSAMSCode = " And y.WEBSAMSCode IN ('".implode("','", (array)$ParWebSAMSCodeAry)."') ";
		}
		
		$sql = 'select
						  cmpSubj.DisplayOrder as Sequence,
						  cmpSubj.RecordID as SubjectID,
						  cmpSubj.CodeID as WEBSAMSCode,
						  cmpSubj.EN_DES as SubjectDescEN,
						  cmpSubj.CH_DES as SubjectDescB5,
						  cmpSubj.EN_SNAME as SubjectShortNameEN,
						  cmpSubj.CH_SNAME as SubjectShortNameB5,
						  If (cmpSubj.CMP_CODEID IS NULL Or cmpSubj.CMP_CODEID=\'\', 1, 2) as ParentCmpDisplayOrder,
						  If (cmpSubj.CMP_CODEID IS NULL Or cmpSubj.CMP_CODEID=\'\', 0, 1) as IsComponent
						from
						  ASSESSMENT_SUBJECT as mainSubj
						  Inner Join ASSESSMENT_SUBJECT AS cmpSubj ON (mainSubj.CODEID = cmpSubj.CODEID)
						  left outer join LEARNING_CATEGORY as lc On (mainSubj.LearningCategoryID = lc.LearningCategoryID)
						where 
							mainSubj.RecordStatus = \'1\' 
							AND cmpSubj.RecordStatus = \'1\'
							and (mainSubj.CMP_CODEID is null or mainSubj.CMP_CODEID = \'\')
						order by 
							lc.DisplayOrder, mainSubj.DisplayOrder, ParentCmpDisplayOrder, cmpSubj.DisplayOrder
						';
		$SubjectInfoOrderedArr = $this->returnArray($sql);
		

		# Get subject mapping
		# (SubjectID => array(EnglishName, ChineseName))
		$sql =	"
							SELECT
								subj.RecordID,
								subj.EN_DES,
								subj.CH_DES
							FROM
								{$intranet_db}.ASSESSMENT_SUBJECT as subj
								Left Outer Join
								{$intranet_db}.LEARNING_CATEGORY as lc On (subj.LearningCategoryID = lc.LearningCategoryID)
							Where
								subj.RecordStatus = 1
							Order By
								lc.DisplayOrder, subj.DisplayOrder
						";
		//$SubjectInfoArr = $this->returnArray($sql);
		$SubjectInfoArr = $this->returnResultSet($sql);
		$numOfSubject = count($SubjectInfoArr);

		$SubjectIDNameAssoArr = array();
		for($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $SubjectInfoArr[$i]['RecordID'];
			$thisSubjectNameEn = $SubjectInfoArr[$i]['EN_DES'];
			$thisSubjectNameCh = $SubjectInfoArr[$i]['CH_DES'];
			
			$SubjectIDNameAssoArr[$thisSubjectID] = array($thisSubjectNameEn, $thisSubjectNameCh);
		}
		
		
		# Get the latest Term
		if ($sys_custom['iPf']['Report']['SLP']['Use_ID_For_Class_Mapping']) {
			$JoinCondition_Class = 'ycu.YearClassID = yc.YearClassID';
		}
		else {
			$JoinCondition_Class = 'assr.ClassName = yc.ClassTitleEN';
		}
		
		# Show the grade without form ranking or not
		$FormRanking_conds = " And assr.OrderMeritForm > 0 ";
		if ($sys_custom['iPf']['Report']['SLP']['AcademicResultShowGradeWithoutFormRanking']) {
			$FormRanking_conds = '';
		}
		
		if ($sys_custom['iPf']['Report']['AcademicResult']['ShowAllTermResult']) {
			$LatestTermCondition = " And assr.YearTermID != '' And assr.YearTermID Is NOT Null ";
		}
		else {
			// 2013-0326-1024-09132 - added UserID to record lastest term of each years of each students
			
			if ($includeComponentSubject) {
				$conds_componentSubject = "";
			}
			else {
				$conds_componentSubject = " And (assr.SubjectComponentID IS NULL Or assr.SubjectComponentID = '') "; 
			}
			
			$sql =	"	SELECT
							assr.AcademicYearID,
							If (ayt.TermStart is Null, 1, 0) as YearTermDisplayOrder, /* Term Result First, Overall Result at last. */
							ayt.YearTermID,
							assr.UserID
						FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr
							Inner Join
							{$intranet_db}.ASSESSMENT_SUBJECT as subj ON (assr.SubjectID = subj.RecordID)
							Left Outer Join
							{$intranet_db}.LEARNING_CATEGORY as lc On (subj.LearningCategoryID = lc.LearningCategoryID)
							Left Outer Join
							{$intranet_db}.YEAR_CLASS_USER as ycu On (assr.UserID = ycu.UserID)
							Inner Join
							{$intranet_db}.YEAR_CLASS as yc ON (".$JoinCondition_Class." And yc.AcademicYearID = assr.AcademicYearID)
							Inner Join
							{$intranet_db}.YEAR as y ON (yc.YearID = y.YearID)
							Inner Join
							{$intranet_db}.ACADEMIC_YEAR as ay ON (assr.AcademicYearID = ay.AcademicYearID)
							Left Outer Join
							{$intranet_db}.ACADEMIC_YEAR_TERM as ayt ON (assr.YearTermID = ayt.YearTermID)
						WHERE
							assr.UserID IN (".implode(',', (array)$ParStudentIDArr).")
							$conds_webSAMSCode
							And (assr.Score > 0 OR (assr.Score=0 And assr.OrderMeritForm > 0) OR ((assr.Score <= 0 Or assr.Score is null) AND assr.Grade != '' AND assr.Grade IS NOT NULL $FormRanking_conds))
							And subj.RecordStatus = 1
							$conds_componentSubject
						ORDER BY
							assr.Year DESC, YearTermDisplayOrder, ayt.TermStart
					";
		
			//$LatestTermInfoArr = $this->returnArray($sql);
			$LatestTermInfoArr = $this->returnResultSet($sql);
			$numOfRecord = count($LatestTermInfoArr);
			$LatestTermInfoAssoArr = array();	// $LatestTermInfoAssoArr[$AcademicYearID] = Latest YearTermID ('' for Overall)
			// Loop through each records to get the latest term result of each academic year
			for ($i=0; $i<$numOfRecord; $i++) {
				$thisAcademicYearID = $LatestTermInfoArr[$i]['AcademicYearID'];
				$thisYearTermID = $LatestTermInfoArr[$i]['YearTermID'];
				$thisUserID = $LatestTermInfoArr[$i]['UserID'];
				
				$LatestTermInfoAssoArr[$thisUserID][$thisAcademicYearID] = $thisYearTermID;
			}
			// build condition
			$LatestTermConditionArr = array();
			$LatestTermCondition = '';
			foreach((array)$LatestTermInfoAssoArr as $thisUserID => $thisInfoAry) {
				$thisStudentConditionAry = array();
				
				foreach((array)$thisInfoAry as $thisAcademicYearID => $thisYearTermID) {
					$thisTermCondition = ($thisYearTermID=='')? "(assr.YearTermID = '' Or assr.YearTermID Is Null)" : "assr.YearTermID = '$thisYearTermID'";
					$thisStudentConditionAry[] = " (assr.AcademicYearID = '$thisAcademicYearID' And $thisTermCondition) ";
				}
				
				$LatestTermConditionArr[] = " ( assr.UserID = '".$thisUserID."' And ( ".implode(' Or ', (array)$thisStudentConditionAry)." ) ) ";
			}
			if (count((array)$LatestTermConditionArr) > 0) {
				$LatestTermCondition = " And ( ".implode(' Or ', (array)$LatestTermConditionArr)." ) ";
			}
		}
		
		
		# Get assessment records
		$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');
		$YearTermNameField = Get_LAng_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');
		
		if ($includeComponentSubject) {
			$conds_componentSubject = "";
			$conds_subjectIdField = "subj.RecordID";
			$conds_subjectTableJoin = "IF (assr.SubjectComponentID is not null AND assr.SubjectComponentID > 0, assr.SubjectComponentID = subj.RecordID, assr.SubjectID = subj.RecordID)";
		}
		else {
			$conds_componentSubject = " And (assr.SubjectComponentID IS NULL Or assr.SubjectComponentID = '') ";
			$conds_subjectIdField = "assr.SubjectID";
			$conds_subjectTableJoin = "assr.SubjectID = subj.RecordID"; 
		}
		
		if($ignoreAssessment){
		    $conds_assessment = " AND assr.TermAssessment IS NULL";
		}
		$sql =	"
							SELECT
								assr.UserID,
								assr.AcademicYearID,
								y.WEBSAMSCode,
								$conds_subjectIdField as SubjectID,
								assr.Score,
								assr.Grade,
								y.YearID,
								$AcademicYearNameField as AcademicYearName,
								If (ayt.TermStart is Null, 1, 0) as YearTermDisplayOrder /* Term Result First, Overall Result at last. */,
								assr.YearTermID,
								ayt.YearTermNameB5 as YearTermNameCh,
								ayt.YearTermNameEN as YearTermNameEn
							FROM
								{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD as assr
								Inner Join {$intranet_db}.ASSESSMENT_SUBJECT as subj ON ($conds_subjectTableJoin)
								Left Outer Join {$intranet_db}.LEARNING_CATEGORY as lc On (subj.LearningCategoryID = lc.LearningCategoryID)
								Left Outer Join {$intranet_db}.YEAR_CLASS_USER as ycu On (assr.UserID = ycu.UserID)
								Inner Join {$intranet_db}.YEAR_CLASS as yc ON (".$JoinCondition_Class." And yc.AcademicYearID = assr.AcademicYearID)
								Inner Join {$intranet_db}.YEAR as y ON (yc.YearID = y.YearID)
								Inner Join {$intranet_db}.ACADEMIC_YEAR as ay ON (assr.AcademicYearID = ay.AcademicYearID)
								Left Outer Join {$intranet_db}.ACADEMIC_YEAR_TERM as ayt ON (assr.YearTermID = ayt.YearTermID)
							WHERE
								assr.UserID IN (".implode(',', (array)$ParStudentIDArr).")
								$conds_webSAMSCode
								And (assr.Score > 0 OR (assr.Score=0 And assr.OrderMeritForm > 0) OR ((assr.Score <= 0 Or assr.Score is null) AND assr.Grade != '' AND assr.Grade IS NOT NULL $FormRanking_conds))
								And subj.RecordStatus = 1 $conds_assessment
								/* And assr.IsAnnual = 1 */
								$LatestTermCondition
								$conds_componentSubject
							ORDER BY
								assr.Year DESC, YearTermDisplayOrder, ayt.TermStart, lc.DisplayOrder, subj.DisplayOrder
						";
		//$APArr = $this->returnArray($sql);
		$APArr = $this->returnResultSet($sql);
		
		//debug_pr($APArr);
		//debug_pr($sql);
		
		# Get student class info
		$studentIdAry = array_values(array_unique(Get_Array_By_Key($APArr, 'UserID')));
		$academicYearIdAry = array_values(array_unique(Get_Array_By_Key($APArr, 'AcademicYearID')));
		$sql = "Select
						ycu.UserID, yc.AcademicYearID, yc.YearID
				From
						YEAR_CLASS as yc
						Inner Join YEAR_CLASS_USER as ycu ON (yc.YearClassID = ycu.YearClassID)
				Where
						ycu.UserID IN ('".implode("','", (array)$studentIdAry)."')
						And yc.AcademicYearID IN ('".implode("','", (array)$academicYearIdAry)."')
				";
		$studentClassInfoAssoAry = BuildMultiKeyAssoc($this->returnResultSet, array('AcademicYearID', 'UserID'));
		
		if(isset($sys_custom['iPf']['SLP']['GroupSubjectByID'])){
			$groupSubjectArr = array_keys($sys_custom['iPf']['SLP']['GroupSubjectByID']);
		}
		
		$StudentAcademicResultInfoArr = array();
		$StudentFormInfoArr = array();
		for($i=0; $i<count($APArr); $i++)
		{
			$thisStudentID = $APArr[$i]['UserID'];
			$thisSubjectID = $APArr[$i]['SubjectID'];
			$thisAcademicYearID = $APArr[$i]['AcademicYearID'];
			$thisAcademicYearName = $APArr[$i]['AcademicYearName'];
			$thisScore = $APArr[$i]['Score'];
			$thisGrade = $APArr[$i]['Grade'];
			$thisYearID = $APArr[$i]['YearID'];
			$thisFormWebSAMSCode = $APArr[$i]['WEBSAMSCode'];
			$thisYearTermID = ($APArr[$i]['YearTermID']=='')? 0 : $APArr[$i]['YearTermID'];
			$thisYearTermNameEn = $APArr[$i]['YearTermNameEn'];
			$thisYearTermNameCh = $APArr[$i]['YearTermNameCh'];
			
			if($sys_custom['iPf']['StewardsPooiKeiCollege']['Report']['StudentLearningProfile']){
			    $thisSubjectID = $thisSubjectID=='658' ||$thisSubjectID=='657'? '639':$thisSubjectID;
			}
			
			
			if ($sys_custom['iPf']['Report']['AcademicResult']['ShowAllTermResult']) {
				$StudentAcademicResultInfoArr[$thisStudentID][$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['Score'] = $thisScore;
				$StudentAcademicResultInfoArr[$thisStudentID][$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['Grade'] = $thisGrade;
				$StudentAcademicResultInfoArr[$thisStudentID][$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['YearID'] = $thisYearID;
				
				//$StudentFormInfoArr[$thisAcademicYearID]['AcademicYearName'] = $thisAcademicYearName;
				//$StudentFormInfoArr[$thisAcademicYearID]['FormWebSAMSCode'] = $thisFormWebSAMSCode;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['AcademicYearName'] = $thisAcademicYearName;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['YearTermAry'][$thisYearTermID]['YearTermNameEn'] = $thisYearTermNameEn;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['YearTermAry'][$thisYearTermID]['YearTermNameCh'] = $thisYearTermNameCh;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['FormWebSAMSCode'] = $thisFormWebSAMSCode;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['YearID'] = $thisYearID;
			}
			else {
				# P134253 
				if(isset($sys_custom['iPf']['SLP']['GroupSubjectByID']) && in_array($thisSubjectID ,(array)$groupSubjectArr)){
					$thisSubjectID = $sys_custom['iPf']['SLP']['GroupSubjectByID'][$thisSubjectID];
				}
				
				$StudentAcademicResultInfoArr[$thisStudentID][$thisSubjectID][$thisAcademicYearID]['Score'] = $thisScore;
				$StudentAcademicResultInfoArr[$thisStudentID][$thisSubjectID][$thisAcademicYearID]['Grade'] = $thisGrade;
				$StudentAcademicResultInfoArr[$thisStudentID][$thisSubjectID][$thisAcademicYearID]['YearID'] = $thisYearID;
	
				//$StudentFormInfoArr[$thisAcademicYearID]['AcademicYearName'] = $thisAcademicYearName;
				//$StudentFormInfoArr[$thisAcademicYearID]['FormWebSAMSCode'] = $thisFormWebSAMSCode;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['AcademicYearName'] = $thisAcademicYearName;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['YearTermAry'][0]['YearTermNameEn'] = '';
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['YearTermAry'][0]['YearTermNameCh'] = '';
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['FormWebSAMSCode'] = $thisFormWebSAMSCode;
				$StudentFormInfoArr[$thisStudentID][$thisAcademicYearID]['YearID'] = $thisYearID;
			}
		}
		
		// V113395 
		if($sys_custom['iPf']['Report']['SkipRepeatAssessmentResult']){
			foreach((array)$StudentFormInfoArr as $_studentid => $_sInfoArr){
				$_formArr = array();
				$_academicYearIdArr = array();
				foreach((array)$_sInfoArr as $__academicYearId => $__yearInfoArr){
					$__formWebSams = $__yearInfoArr['FormWebSAMSCode'];
					if(!in_array($__formWebSams,$_formArr)){
						$_academicYearIdArr[] = $__academicYearId;
						$_formArr[] = $__formWebSams;
					}else{
						// skip repeated form -  e.g. 2014-15, 2015-16 are both S5. unset 2014-15 here   
						unset($StudentFormInfoArr[$_studentid][$__academicYearId]);
					}
				}
			}
		}
		return array($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr);
	}
	
	
	# Get awards and major achievement
	function GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP($ParStudentIDArr, $ParYear="")
	{
		global $intranet_db, $eclass_db;

		$sql =	"
							SELECT DISTINCT
								UserID,
								Year,
								AwardName,
								Remark,
								Organization
							FROM
								{$eclass_db}.AWARD_STUDENT
							WHERE
								UserID IN (".implode(',', $ParStudentIDArr).") AND
								RecordType = '1'
						";
		if($ParYear != "")
		{
			$year_arr = explode("-", $ParYear);
			$sql .=  " AND (Year = '".$ParYear."' OR Year = '".$year_arr[0]."')";
		}
		$sql .=	"
							ORDER BY
								Year DESC
						";
						
		$AwardArr = $this->returnArray($sql);

		$ReturnArr = array();
		for($i=0; $i<count($AwardArr); $i++)
		{
			$ReturnArr[$AwardArr[$i]['UserID']][] = array	(
																											'Year' => $AwardArr[$i]['Year'],
																											'AwardName' => $AwardArr[$i]['AwardName'],
																											'Remark' => $AwardArr[$i]['Remark'],
																											'Organization' => $AwardArr[$i]['Organization']
																										);
		}

		return $ReturnArr;
	}
	
	# Get extra-curricular activity
	function GET_ACTIVITY_SLP($ParStudentIDArr, $ParYear="")
	{
		global $eclass_db;
	
		$sql =	"
							SELECT
								UserID,
								Year,
								ActivityName,
								Role
							FROM
								{$eclass_db}.ACTIVITY_STUDENT
							WHERE
								UserID IN (".implode(',', $ParStudentIDArr).")
						";
		if($ParYear != "")
		{
			$year_arr = explode("-", $ParYear);
			$sql .=  " AND (Year = '".$ParYear."' OR Year = '".$year_arr[0]."')";
		}
		$sql .=	"
							ORDER BY
								Year, ActivityName
						";
		$ActivityArr = $this->returnArray($sql);

		$ReturnArr = array();
		for($i=0; $i<count($ActivityArr); $i++)
		{
			$ReturnArr[$ActivityArr[$i]['UserID']][] = array	(
																													'Year' => $ActivityArr[$i]['Year'],
																													'ActivityName' => $ActivityArr[$i]['ActivityName'],
																													'Role' => $ActivityArr[$i]['Role']
																												);
		}
	
		return $ReturnArr;
	}
	
	# Get OLE
	function GET_OLE_SLP($ParStudentIDArr)
	{
		global $eclass_db;
	
		$sql =	"
							SELECT
								UserID,
								IF(StartDate <> '0000-00-00', IF(MONTH(StartDate) >= 9, YEAR(StartDate), YEAR(StartDate)-1), '') AS StartYear,
								IF(EndDate <> '0000-00-00', IF(MONTH(EndDate) >= 9, YEAR(EndDate)+1, YEAR(EndDate)), '') AS EndYear,
								Title,
								Hours,
								ELE,
								IntExt
							FROM
								{$eclass_db}.OLE_STUDENT
							WHERE
								UserID IN (".implode(',', $ParStudentIDArr).") AND
								RecordStatus IN (2,4)
							ORDER BY
								StartDate
						";
		$OLEArr = $this->returnArray($sql);

		for($i=0; $i<count($OLEArr); $i++)
		{
			$ELEArr = explode(",", $OLEArr[$i]['ELE']);

			for($j=0; $j<count($ELEArr); $j++)
				$ReturnArr[$OLEArr[$i]['UserID']][trim($ELEArr[$j])][$OLEArr[$i]['IntExt']][] = array	(
																																													"Year" => $OLEArr[$i]['StartYear'].(($OLEArr[$i]['StartYear'] != "" && $OLEArr[$i]['EndYear'] != "") ? "-" : "").$OLEArr[$i]['EndYear'],
																																													"Title" => $OLEArr[$i]['Title'],
																																													"Hours" => $OLEArr[$i]['Hours']
																																												);
		}

		return $ReturnArr;
	}
	
	# Get self account
	function GET_SELF_ACCOUNT_SLP($ParStudentIDArr)
	{
		global $intranet_db, $eclass_db;

		$sql =	"
							SELECT
								UserID,
								Title,
								Details
							FROM
								{$eclass_db}.SELF_ACCOUNT_STUDENT
							WHERE
								UserID IN ('".implode("','", (array)$ParStudentIDArr)."') AND
								INSTR(DefaultSA, 'SLP') > 0
							ORDER BY
								UserID, ModifiedDate DESC
						";

		$SAArr = $this->returnArray($sql);

		$ReturnArr = array();
		$CurrentUserID = "";
		for($i=0; $i<count($SAArr); $i++)
		{
			if($CurrentUserID != $SAArr[$i]['UserID'])
			{
				$ReturnArr[$SAArr[$i]['UserID']]['Title'] = $SAArr[$i]['Title'];
				$ReturnArr[$SAArr[$i]['UserID']]['Details'] = $SAArr[$i]['Details'];

				$CurrentUserID = $SAArr[$i]['UserID'];
			}
		}

		return $ReturnArr;
	}

	# Generate Student Learning Profile for Printing
	function GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT($ParStudentID, $ParIssueDate="", $ParYear="",$ParPrintIssueDate = "")
	{
		global $ec_iPortfolio, $eclass_filepath, $SchoolName, $eclass_root;
		global $lpf_ui, $lpf_slp;

		// Page Break Style
//		$page_break_style = "<STYLE TYPE='text/css'>\nP.breakhere {page-break-before: always}\n</STYLE>\n";
//		$page_break_here = "<p class='breakhere'></p>\n";
		# Table style
		$SLP_Style['report_header'] = array("padding:3px", "font-size:13pt", "font-weight:bold", "font-family:Arial");
		$SLP_Style['module_title'] = array("color:#FFFFFF", "padding:3px", "font-size:12pt", "font-weight:bold", "font-family:Arial");
		$SLP_Style['padding_all'] = array("padding:3px");
		
		# Add style for customarized report
		switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
		{
			case "LaSalle":
				$SLP_Style['report_header'] = array("padding:3px", "font-size:20pt", "font-family:Arial");
				$SLP_Style['report_subheader'] = array("padding:3px", "font-size:14pt", "font-family:Arial");
				$SLP_Style['border_table'] = array("border-style:solid", "border-color:#000000", "border-width:thin");
				$SLP_Style['cell_font_arial'] = array("font-size:10pt", "font-family:arial");
				$SLP_Style['cell_font_times'] = array("font-size:11pt", "font-family:times");
				$SLP_Style['bottom_border'] = array("border-bottom-color:#000000", "border-bottom-width:thin");
				break;
		}
		
		$SLP_style_to_class = "<STYLE TYPE='text/css'>\n";
		foreach($SLP_Style as $class_name => $class_property)
		{
			$SLP_style_to_class .= ".".$class_name." {\n".implode(";\n",$class_property)."\n}\n\n";
		}
		$SLP_style_to_class .= "</STYLE>";
		
		# Get student particulars
		$SPArr = $this->GET_STUDENT_PARTICULARS_SLP($ParStudentID);
		
		# Get academic performance
		list($SubjArr, $YearArr, $APArr, $SubjectInfoOrderedArr) = $this->GET_ACADEMIC_PERFORMANCE_SLP($ParStudentID);
		
		# Get award and major achievements in school
//fai(show be open)		$AwardArr = $this->GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP($ParStudentID, $ParYear);
		
		# Get extra-curricular activity
		$ActivityArr = $this->GET_ACTIVITY_SLP($ParStudentID, $ParYear);
		
		# OLE
		$OLEArr = $this->GET_OLE_SLP($ParStudentID);		

		# Get self account
		$SelfAccountArr = $this->GET_SELF_ACCOUNT_SLP($ParStudentID);

		# Display Setting
		$ModuleTitle[] = "student_particular";
		$ModuleTitle[] = "academic_performance";
		$ModuleTitle[] = "ole";
		$ModuleTitle[] = "activity";
		$ModuleTitle[] = "award_in_school";
		$ModuleTitle[] = "award_outside_school";
		$ModuleTitle[] = "self_account";
		
		# Display order in terms of index of $ModuleTitle
		# Number of entries must be consistent with $ModuleTitle		
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
			if(file_exists($portfolio_report_config_file))
			{
				list($filecontent, $OLESettings) = explode("\n", trim(get_file_content($portfolio_report_config_file)));
				$config_array_temp = unserialize($filecontent);
	
				# Get order only
				for($i=1; $i<count($config_array_temp); $i++)
				{
					# skip activity
					if($i>2)
						$config_array[$i+1] = $config_array_temp[$i][0];
					else
						$config_array[$i] = $config_array_temp[$i][0];
				}
				
				$controlOrder = array_flip($config_array);
				# Default:
				# student_particular : first
				# activity : last
				$controlOrder[0] = 0;
				$controlOrder[6] = 3;
			}
			else
				$controlOrder = array(0,1,2,3,4,5,6);
		}
		else
		{
			switch($this->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					$controlOrder = array(0,1,4,3,2,5,6);
					break;
			}
		}

		# Display control in terms of index of $ModuleTitle
		# Number of entries must be consistent with $ModuleTitle
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			$portfolio_report_config_file = "$eclass_root/files/portfolio_slp_config.txt";
			if(file_exists($portfolio_report_config_file))
			{
				$filecontent = trim(get_file_content($portfolio_report_config_file));
				$config_array = unserialize($filecontent);
	
				# Get display option only
				for($i=1; $i<count($config_array); $i++)
				{
					# skip activity
					if($i>2)
						$controlShow[$i+1] = $config_array[$i][1];
					else
						$controlShow[$i] = $config_array[$i][1];
				}
				$controlShow[0] = !$config_array[count($config_array)];
				
				# not show activity
				$controlShow[3] = false;
			}
			else
				$controlShow = array(true,true,true,false,true,true,true);
		}
		else
		{
			switch($this->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					$controlShow = array(true,true,true,true,true,false,true);
					break;
			}
		}
		
		$ReturnStr = $SLP_style_to_class;


		/*generate PDF*/
		//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

		//set some language-dependent strings
		$pdf->setLanguageArray($l); 

			// ---------------------------------------------------------

		// set font
		//$pdf->SetFont('helvetica', '', 8);
		//$pdf->SetFont('arialunicid0', 'U', 20);
		$pdf->SetFont('arialunicid0-chi', '', 8);

		# Generate Main Content
		for($i=0; $i<count($ParStudentID); $i++)
		{
//			$pdf->numpages=10;
			$pdf->AddPage();

			$pdfFormat = "";  //empty the content first

			$StudentID = $ParStudentID[$i];
		
			//$ReturnStr .=	$this->GEN_REPORT_HEADER_SLP($ParIssueDate, $SPArr[$StudentID]['SchoolAddress'], $ParYear,$ParPrintIssueDate);

			$a = true;
			$ReturnStr .=	$this->GEN_REPORT_HEADER_SLP($ParIssueDate, $SPArr[$StudentID]['SchoolAddress'], $ParYear,$a,$ParPrintIssueDate);
			$pdfFormat .= $this->GEN_REPORT_HEADER_SLP_PDF($ParIssueDate, $SPArr[$StudentID]['SchoolAddress'], $ParYear,$ParPrintIssueDate);
			for($j=0; $j<count($controlOrder); $j++)
			{	
				$module_name = $ModuleTitle[$controlOrder[$j]];
				$show = $controlShow[$controlOrder[$j]];

				$ReturnStr .=	"<tr><td valign='top'>";

				switch($module_name)
				{
					# Student Particulars
					case "student_particular":
						if($show)
						{
							$ReturnStr .= $this->GEN_STUDENT_PARTICULARS_TABLE_SLP($SPArr[$StudentID]);
							$ReturnStr .= "<br/><br/>\n";
							$pdfFormat .= $this->GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF($SPArr[$StudentID]);
						}
						break;

					# Academic Performance
					case "academic_performance":
						if($show)
						{
							$ReturnStr .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP($SubjArr, $YearArr, $APArr[$StudentID], $SubjectInfoOrderedArr);
							$ReturnStr .= "<br/><br/>\n";
							$pdfFormat .= $this->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF($SubjArr, $YearArr, $APArr[$StudentID], $SubjectInfoOrderedArr, $SubjectInfoOrderedArr);

						}
						break;
					
					# OLE
					case "ole":
						if($show)
						{
							$ReturnStr .= $this->GEN_OLE_TABLE_SLP($OLEArr[$StudentID], $StudentID, convertSystemYearToOLEYear($ParYear));
							$pdfFormat .= $this->GEN_OLE_TABLE_SLP_PDF($OLEArr[$StudentID], $StudentID, convertSystemYearToOLEYear($ParYear));
						}
						break;
						
					# Extra-curricular activity
					case "activity":
						if($show)
						{
							$ReturnStr .= $this->GEN_ACTIVITY_TABLE_SLP($ActivityArr[$StudentID]);
							$pdfFormat .= $this->GEN_ACTIVITY_TABLE_SLP_PDF($ActivityArr[$StudentID]);
						}
						break;
					
					# Awards and Achievements in school
					case "award_in_school":
						if($show)
						{
							$ReturnStr .= $this->GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP($AwardArr[$StudentID]);
							$ReturnStr .= "<br/><br/>\n";
							$pdfFormat .= $this->GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP_PDF($AwardArr[$StudentID]);
						}
						break;
						
					# Awards and Achievements outside school
					case "award_outside_school":
						if($show)
						{
							$ReturnStr .= $this->GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL($StudentID, convertSystemYearToOLEYear($ParYear));
							$pdfFormat .= $this->GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL_PDF($StudentID, convertSystemYearToOLEYear($ParYear));
						}
						break;
											
					# Self Account
					case "self_account":
						if($show)
						{
							$ReturnStr .= $this->GEN_SELF_ACCOUNT_TABLE_SLP($SelfAccountArr[$StudentID]);
							$ReturnStr .= "<br/><br/>\n";
							$pdfFormat .= $this->GEN_SELF_ACCOUNT_TABLE_SLP_PDF($SelfAccountArr[$StudentID]);
						}
						break;
				}
				$pdfFormat .= "<br/>";			
				$ReturnStr .=	"</td></tr>";
			}			

//			debug($pdfFormat);

			if($this->broken_tags($pdfFormat))
			{
				echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
				echo '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">';
				echo "<head><title>Testing</title></head>\n";
				echo "<body>\n";
				echo "<p style=\"color:red\">data is broken</p>\n";
				echo $pdfFormat;
				echo "</body>\n";
				echo "</html>\n";
//				debug($pdfFormat);
			}
			$pdf->writeHTML($pdfFormat, true, false, false, false, '');	
			$ReturnStr .=	$this->GEN_REPORT_FOOTER_SLP();
			
		}//END OF EACH STDUENT
		
		/*
		test for pdf
		*/
		// add a page
//		echo $pdfFormat;exit();
		$pdf->Output('testing1.pdf', 'I');
		exit();  // hard code to exist first
		return $ReturnStr;
	}
	
	# General function to generate Student Learning Profile module
	function GEN_SLP_MODULE_TABLE($ParTitle="", $ParDescription="", $ParContent="")
	{
		$ReturnStr = str_replace("<!--Heading-->", (trim($ParTitle) == "" ? "" : "<tr style='background:#808080;'><td class='module_title'>".$ParTitle."</td></tr>"), $this->SLP_module_frame);
		$ReturnStr = str_replace("<!--Description-->", (trim($ParDescription) == "" ? "" : "<tr style='background:#DFDFDF;'><td style='padding:3px;'>".$ParDescription."</td></tr>"), $ReturnStr);
		$ReturnStr = str_replace("<!--Content-->", (trim($ParContent) == "" ? "" : "<tr><td>".$ParContent."</td></tr>"), $ReturnStr);
		
		return $ReturnStr;
	}
	
	# General function to generate Student Learning Profile module
	function GEN_SLP_MODULE_TABLE_PDF($ParTitle="", $ParDescription="", $ParContent="")
	{
		global $ipf_cfg,$sys_custom;

		$fontColor='white';
		//$backgroundColor1 = 'gray';  // old value, change it to HTML color code which is same as gray
		$backgroundColor1 = '#808080';
		$fontSize = '';
		$tableCellPadding = '';
		//handle style for request from specific report [e.g dynamic report]
		if(strtolower($this->getPrintRequsetFrom()) == strtolower($ipf_cfg['slpAcademicResultPrintRequestFrom']['DynamicReport'])){
			if($sys_custom['iPf']['DynamicReport']['AcademicReportSectionDisplayStyleSameAsDataSection']){
				$fontColor ='black';
				$backgroundColor1 = '#BBBBBB';

				$fontSize = 'font-size:12pt';
				$tableCellPadding = 'cellpadding="2"';
			}

			if(is_int($sys_custom['iPf']['DynamicReport']['AcademicReportSectionDisplayFontSize'])){
				$fontSize = ' font-size:'.$sys_custom['iPf']['DynamicReport']['AcademicReportSectionDisplayFontSize'].'pt ';
			}
		}		

		$ParTitle = (trim($ParTitle) == "") ? "" : "<table width=\"100%\" ".$tableCellPadding."><tr><td style=\"background-color:".$backgroundColor1.";color:".$fontColor.";".$fontSize." \">  ".$ParTitle."</td></tr></table>";

		$ParDescription = (trim($ParDescription) == "")?"":"<table width =\"100%\"><tr><td style=\"background-color:#CCCCCC;color:#000000\">".$ParDescription."</td></tr></table>";


		if (empty($ParDescription)) {
			$ReturnStr .= "<table width=\"100%\" border=\"1\"><tr><td>".$ParTitle."</td></tr></table>";
		} else {
			$ReturnStr .= "<table width=\"100%\" border=\"1\"><tr><td>".$ParTitle."</td></tr><tr><td>".$ParDescription."</td></tr></table>";
		}


		$pattern1   = array("\r\n", "\n", "\r");
		$replacement = '';
		$ParContent = str_replace($pattern1, $replacement, $ParContent);

		$parttern = '/<xml>.*?<\/xml>/';
		$replacement = '';
		$ParContent = preg_replace($parttern,$replacement,$ParContent);

		$parttern = '/<style>.*?<\/style>/';
		$replacement = '';
		$ParContent = preg_replace($parttern,$replacement,$ParContent);

		$ReturnStr .= $ParContent;

		return $ReturnStr;
	}

	# Generate report header
	function GEN_REPORT_HEADER_SLP($ParIssueDate="", $ParSchoolAddress="", $ParYear="", $IsPageBreakAfter=true,$ParPrintIssueDate, $IsPageBreakBefore=false)
	{
		global $ec_iPortfolio, $eclass_httppath, $sys_custom;
		global $lpf_slp,$Lang;
		if ($IsPageBreakAfter) {
			$pageBreakAfterString = "page-break-after:always;";
		}
		if ($IsPageBreakBefore) {
			$pageBreakAfterString = "page-break-before:always;";
		}
		$t_year = (strpos($ParYear, "0000") !== false) ? "" : $ParYear;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
		  # Eric Yip (20091202): Handle dummy date
		//  $t_year = (strpos($ParYear, "0000") !== false) ? "" : $ParYear; //move upper the if , wait for delete
		
			$ReturnStr =	"
											<table width='100%' cellpadding='5' cellspacing='0' border='0' align='center' style='$pageBreakAfterString;'>
												<tr>
													<td align='center' class='report_header'>".($sys_custom['iPortfolio_SLP_Report_minor']['chc'] ? "<p class='report_header'>".GET_SCHOOL_NAME()."</p>" : "").$this->BiLangOut('["student_learning_profile"]',empty($t_year)?"":"$t_year ")."</td>
												</tr>";
			if($ParPrintIssueDate){
				$ReturnStr .= "<tr><td align='right'>".$this->BiLangOut('["date_issue"]',"","","H").": ".$ParIssueDate."</td></tr></table>";
			}
		}


//		if($sys_custom['ipf']['slp_report']['ucc']){
		if($sys_custom['ipf']['slp_report']['customizeHeader']){

			$ReturnStr = ''; //for safe , reset the $ReturnStr
			$schoolCustomizeCode = $sys_custom['ipf']['slp_report']['customizeHeaderSchoolCode'];
			$schoolCustomizeHeaderLayout = ($sys_custom['ipf']['slp_report']['customizeHeaderStyle'] == '')?'layout_1' : $schoolCustomizeHeaderStyle;
	
			//this dynamice function support with different school with different header layout in the future, depend on $sys_custom['ipf']['slp_report']['customizeHeaderStyle'] , default is layout_1  --> f: customizeHeader_layout_1

			$customzieHearFunctionName = 'customizeHeader_'.$schoolCustomizeHeaderLayout;

			$ReturnStr = $this->$customzieHearFunctionName($schoolCustomizeCode,$pageBreakAfterString,$ParIssueDate,$ParPrintIssueDate,$ParYear,'html');

		}
		return $ReturnStr;
	}
	
	function GEN_REPORT_HEADER_SLP_IMAGE($ParIssueDate,$ParPrintIssueDate)
	{
		global $ec_iPortfolio, $eclass_httppath, $sys_custom;
		global $lpf_slp,$Lang,$PATH_WRT_ROOT,$ipf_cfg,$HTTP_SERVER_VARS,$intranet_root;
		
		$schoolHeaderImage = '';
		$SchoolImgDir = $PATH_WRT_ROOT.$ipf_cfg['SLPArr']['SLPFolderPath'];

		if(is_dir($SchoolImgDir)) {
			$dh = opendir($SchoolImgDir);
			 
			$LogofileName = $ipf_cfg['SLPArr']['SLPSchoolHeaderImageName'];
			$ExistFileName = '';
			 
			while (($file = readdir($dh)) !== false)
			{		
				$pos = strpos($file,$LogofileName);
				
				if($pos === false) {
					 // string needle NOT found in $file
				}
				else {
				 	$ExistFileName = $file;
				}
			}
			closedir($dh);
			
			$h_space = '&nbsp;';
			if(strtolower($this->getPrintFormat())=='pdf')
			{
				$h_space = '';
			}
			
			$SchoolImgFile = $SchoolImgDir.$ExistFileName;
			if(file_exists($SchoolImgFile))
			{
				if ($SchoolImgFile != '') 
				{
					$pageURL = curPageURL(0,0);
					
					//for default case, getPrintFormat() = HTML
					$SchoolImageFile_HTTP = $pageURL.$ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName.'?rand='.time();  //1235px					
					$h_style = '';
					$h_class = ' class="report_header" ';
					if(strtolower($this->getPrintFormat())=='pdf')
					{
						$SchoolImageFile_HTTP = $intranet_root.$ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName;  //1235px					
						$h_style = 'font-size:14;';
						$h_class = '';
					}				
					$schoolHeaderImage = '<table width="100%" StyleLater  >';


					$h_heightAttr  = $this->getHTMLVersionHeaderImgDisplayHeight(); 
					$h_widthAttr   = $this->getHTMLVersionHeaderImgDisplayWidth(); 

					$h_heightAttr = ($h_heightAttr == '')?'' :' height="'.$h_heightAttr .'" ';
					$h_widthAttr = ($h_widthAttr == '')?'' :' width="'.$h_widthAttr.'" ';

					$schoolHeaderImage .= '<tr><td align="center"><img '.$h_widthAttr.' '.$h_heightAttr.' src="'.$SchoolImageFile_HTTP.'"/></td></tr>';

					$schoolHeaderImage .= '<tr height="20px"><td>'.$h_space.'</td></tr>';					
					$schoolHeaderImage .= '<tr><td align="center" style="'.$h_style.'" '.$h_class.'>'.$this->BiLangOut('["student_learning_profile"]',"","","").'</td></tr>';
					if($ParPrintIssueDate)
					{
						$schoolHeaderImage .= "<tr><td align=\"right\">".$this->BiLangOut('["date_issue"]',"","","H").": ".$ParIssueDate."<br/></td></tr>";
					}					
					$schoolHeaderImage .= '</table>';
				}
				else
				{
					$schoolHeaderImage = $h_space;
				}
			}
		}
				
		return $schoolHeaderImage;
	}
	
	# Generate report header
	function GEN_REPORT_HEADER_SLP_PDF($ParIssueDate="", $ParSchoolAddress="", $ParYear="",$ParPrintIssueDate)
	{
		global $ec_iPortfolio, $eclass_httppath, $sys_custom;
		global $lpf_slp,$Lang;
	
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			# Eric Yip (20091202): Handle dummy date
			$t_year = (strpos($ParYear, "0000") !== false) ? "" : $ParYear;
		
			$ReturnStr = "<table width=\"100%\" cellspacing=\"0\" border=\"0\" align=\"center\">
							<tr>
								<td align=\"center\" style=\"font-size:14;\">".($sys_custom['iPortfolio_SLP_Report_minor']['chc'] ? "<p >".GET_SCHOOL_NAME()."</p>" : "").$this->BiLangOut('["student_learning_profile"]',empty($t_year)?"":"$t_year ")."</td>
							</tr>";
			if($ParPrintIssueDate)
			{
				$ReturnStr .= "<tr><td align=\"right\">".$this->BiLangOut('["date_issue"]',"","","H").": ".$ParIssueDate."<br/></td></tr>";
			}
			$ReturnStr .= "</table>";
    }			
		if($sys_custom['ipf']['slp_report']['customizeHeader']){

			$ReturnStr = ''; //for safe , reset the $ReturnStr
			$schoolCustomizeCode = $sys_custom['ipf']['slp_report']['customizeHeaderSchoolCode'];

			$schoolCustomizeHeaderLayout = ($sys_custom['ipf']['slp_report']['customizeHeaderStyle'] == '')?'layout_1' : $schoolCustomizeHeaderStyle;
	
			//this dynamice function support with different school with different header layout in the future, depend on $sys_custom['ipf']['slp_report']['customizeHeaderStyle'] , default is layout_1  --> f: customizeHeader_layout_1

			$customzieHearFunctionName = 'customizeHeader_'.$schoolCustomizeHeaderLayout;

			$ReturnStr = $this->$customzieHearFunctionName($schoolCustomizeCode,$pageBreakAfterString,$ParIssueDate,$ParPrintIssueDate,$ParYear,'pdf');

		}
	/*
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{

			}
		}
*/
		return $ReturnStr;
	}

	# Generate student particular table
	function GEN_STUDENT_PARTICULARS_TABLE_SLP($ParSPArr, $ParDetailSettings)
	{
		global $ec_iPortfolio, $ec_words, $ec_student_word, $iPort;
		global $i_UserStudentName, $i_UserDateOfBirth, $i_HKID, $i_UserGender, $i_no_record_exists_msg;
		global $intranet_session_language;
		global $lpf_slp;
		global $special_feature,$sys_custom;


		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParSPArr) && !empty($ParSPArr) && array_sum($ParDetailSettings))
			{	

				$studentName = $this->getStudentName($ParSPArr);

				$studentGender = ($ParSPArr['Gender'] == "F") ? $this->BiLangOut("['FEMALE']") : $this->BiLangOut("['MALE']");

				$temp_ContentStr = "";
				$MAX_NUM_OF_ROWS = 5;
				if (is_array($ParDetailSettings)) {
					$currentRow = 1;
					
					foreach($ParDetailSettings as $key => $value) {
						if ($value) {
							if ($currentRow%$MAX_NUM_OF_ROWS == 1) {
								$temp_ContentStr .= "<td valign='top'width='50%'><table border='0' width='100%' cellpadding='3' cellspacing='0'>";
							}
							switch(strtoupper($key)) {
								case "STUDENTNAME":
										$temp_ContentStr .= "
																	<tr>
																		<td width='30%' valign='top' nowrap>".$this->BiLangOut("['StudentName']").":</td>
																		<td width='60%'>" . $studentName . "</td>
																	</tr>
																	";
									break;
								case "DATEOFBIRTH":
										$temp_ContentStr .= "
																	<tr>
																		<td width='30%' valign='top' nowrap>".$this->BiLangOut("['DateOfBirth']").":</td>
																		<td width='60%'>".$ParSPArr['DateOfBirth']."</td>
																	</tr>
																	";
									break;
								case "EDUCATIONINSTITUTION":
										$temp_ContentStr .= "
																	<tr>
																		<td width='30%' valign='top' nowrap>".$this->BiLangOut("['NameOfEducationInstituation']").":</td>
																		<td width='60%'>".$ParSPArr['SchoolName']."</td>
																	</tr>
																	";
									break;
								case "ADMISSIONDATE":
										$temp_ContentStr .= "
																	<tr>
																		<td width='30%' valign='top' nowrap>".$this->BiLangOut("['AdmissionDate']").":</td>
																		<td width='60%'>".$ParSPArr['AdmissionDate']."</td>
																	</tr>
																	";
									break;
								case "SCHOOLADDRESS":
										$temp_ContentStr .= "
																	<tr>
																		<td width='30%' valign='top' nowrap>".$this->BiLangOut("['SchoolAddress']").":</td>
																		<td width='60%'>".$ParSPArr['SchoolAddress']."</td>
																	</tr>
																	";
									break;
								case "SCHOOLPHONE":
										$temp_ContentStr .= "
																	<tr>
																		<td width='30%' valign='top' nowrap>".$this->BiLangOut("['SchoolPhone']").":</td>
																		<td width='60%'>".$ParSPArr['SchoolPhone']."</td>
																	</tr>
																";
									break;
								case "HKID":
									if ($special_feature['ava_hkid']) {
										$temp_ContentStr .= "
																	<tr>
																		<td width='40%' valign='top' nowrap>".$this->BiLangOut("['HKID']").":</td>
																		<td width='60%'>".$ParSPArr['HKID']."</td>
																	</tr>
																	";
									}
									break;
								case "GENDER":
										$temp_ContentStr .= "
																	<tr>
																		<td width='40%' valign='top' nowrap>".$this->BiLangOut("['Gender']").":</td>
																		<td width='60%'>".$studentGender."</td>
																	</tr>
																	";
									break;
								case "SCHOOLCODE":
										$temp_ContentStr .= "
																	<tr>
																		<td width='40%' valign='top' nowrap>".$this->BiLangOut("['SchoolCode']").":</td>
																		<td width='60%'>".$ParSPArr['SchoolID']."</td>
																	</tr>
																	";
									break;
								default:
									break;							
							}
							if ($currentRow%$MAX_NUM_OF_ROWS == 0) {
								$temp_ContentStr .= "</table></td>";
							}
							$currentRow++;
						}
					}
					if ($currentRow%$MAX_NUM_OF_ROWS != 1) {
						$temp_ContentStr .= "</table></td>";
					}
				} else {
					// do nothing
				}
//				debug_r(htmlspecialchars($temp_ContentStr));
				$ContentStr =	"
												<table width='100%' border='0' cellpadding='0' cellspacing='0' class='padding_all'>
													<tr>
														$temp_ContentStr
													</tr>
												</table>
											";

				if($sys_custom['ipf']['slp_report']['ucc']){ // cust report 
					$ContentStr = '';
					$ContentStr = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="padding_all">
													<tr>
														<td width="15%">'.$this->BiLangOut("['StudentName']").':</td><td width="35%">'.$studentName.'</td><td width="15%">'.$this->BiLangOut("['ClassName_ClassNo']").'</td><td width="35%">'.$ParSPArr['ClassName']." (".$ParSPArr['ClassNumber'].')</td>
													</tr>
													<tr>
														<td width="15%">'.$this->BiLangOut("['StudentID']").':</td><td width="35%">'.$ParSPArr['UserLogin'].'</td><td width="15%">&nbsp;</td><td width="35%">&nbsp;</td>
													</tr>
												</table>';
				}

			}
			else
	//			$ContentStr = $i_no_record_exists_msg;
				$ContentStr = "
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td height='100' valign='middle' align='center'>--</td>
													</tr>
												</table>
											";
			
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['StudentParticulars']"), "", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(is_array($ParSPArr) && !empty($ParSPArr))
					{
						$ReturnStr =	"
														<table width='100%' border='0' cellpadding='0' cellspacing='0' class='padding_all'>
															<tr>
																<td valign='top' width='60%'>
																	<table width='85%' border='0' cellpadding='3' cellspacing='0' class='padding_all'>
																		<tr>
																			<td width='30%' valign='top' class='cell_font_times' nowrap>".$ec_student_word['name_english'].":</td>
																			<td width='55%' class='bottom_border cell_font_times' style='border-bottom-style:dotted'>".$ParSPArr['EnglishName']."</td>
																		</tr>
																		<tr>
																			<td width='30%' valign='top' class='cell_font_times' nowrap>".$ec_student_word['name_chinese'].":</td>
																			<td width='55%' class='bottom_border cell_font_times' style='border-bottom-style:dotted'>".$ParSPArr['ChineseName']."</td>
																		</tr>
																		<tr>
																			<td width='30%' valign='top' class='cell_font_times' nowrap>".$ec_iPortfolio['WebSAMSRegNo'].":</td>
																			<td width='55%' class='bottom_border cell_font_times' style='border-bottom-style:dotted'>".$ParSPArr['WebSAMSRegNo']."</td>
																		</tr>
																		<tr>
																			<td width='30%' valign='top' class='cell_font_times' nowrap>".$ec_iPortfolio['admission_date'].":</td>
																			<td width='55%' class='bottom_border cell_font_times' style='border-bottom-style:dotted'>".$ParSPArr['AdmissionDate']."</td>
																		</tr>
																		<tr>
																			<td width='30%' valign='top' class='cell_font_times' nowrap>".$iPort['class'].":</td>
																			<td width='55%' class='bottom_border cell_font_times' style='border-bottom-style:dotted'>".$ParSPArr['ClassName']."</td>
																		</tr>
																		<tr>
																			<td width='30%' valign='top' class='cell_font_times' nowrap>".$iPort['class_no'].":</td>
																			<td width='55%' class='bottom_border cell_font_times' style='border-bottom-style:dotted'>".$ParSPArr['ClassNumber']."</td>
																		</tr>
																	</table>
																</td>
																<td valign='top' width='40%' align='right'>".(file_exists($ParSPArr['OfficialPhoto'][0]) ? "<img src=\"".$ParSPArr['OfficialPhoto'][1]."\" width=\"100\" height=\"130\" />" : "")."</td>
															</tr>
														</table>
											";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}
	
	# St.Antonious Girls College Customization
	function GEN_STUDENT_PARTICULARS_TABLE_SLP_SAGC($ParSPArr, $ParDetailSettings)
	{
		global $lpf_slp;
		if(is_array($ParSPArr) && !empty($ParSPArr) && array_sum($ParDetailSettings))
		{	
			$studentName = $this->getStudentName($ParSPArr);

			$studentGender = ($ParSPArr['Gender'] == "F") ? $this->BiLangOut("['FEMALE']") : $this->BiLangOut("['MALE']");
			
			$photolink = $ParSPArr['OfficialPhoto'][1];
			
			$ContentStr =	"
							<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
							<tr>
							<td width=\"50%\" valign=\"top\">
								<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" class=\"padding_all\">
									<tr>
										<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['StudentName']").":</td>
										<td width=\"70%\">".$studentName."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['DateOfBirth']").":</td>
										<td>".$ParSPArr['DateOfBirth']."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['NameOfEducationInstituation']").":</td>
										<td>".$lpf_slp->stringLanguageSplitByWords($ParSPArr['SchoolName'],'br','b5')."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['AdmissionDate']").":</td>
										<td>".$ParSPArr['AdmissionDate']."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['SchoolAddress']").":</td>
										<td>".$lpf_slp->stringLanguageSplitByWords($ParSPArr['SchoolAddress'],'br','b5')."</td>
									</tr>
								</table>
							</td>
							<td width=\"50%\" valign=\"top\" style=\"border-left:2px solid #000\">
								<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"padding_all\">
									<tr>
										<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['SchoolPhone']").":</td>
										<td width=\"30%\">".$ParSPArr['SchoolPhone']."</td>
										<td width=\"40%\" rowspan=\"5\" align=\"right\" valign=\"top\"><img src=\"$photolink\" /></td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['Gender']").":</td>
										<td>".$studentGender."</td>
										<td></td>
									</tr>
									<tr><td valign=\"top\" nowrap>".$this->BiLangOut("['SchoolCode']").":</td>
										<td>".$ParSPArr['SchoolID']."</td>
										<td></td>
									</tr>
									<tr><td valign=\"top\" nowrap></td>
										<td></td>
									</tr>
									<tr><td valign=\"top\" nowrap></td>
										<td></td>
									</tr>
								</table>
							</td>
							</tr>
							</table>
						";
		}else{
			$ContentStr = "
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td height='100' valign='middle' align='center'>--</td>
								</tr>
							</table>
						";		
		}
		$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['StudentParticulars']"), "", $ContentStr);
		
		return $ReturnStr;
	}
		
	# St.Antonious Girls College Customization
	function GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF_SAGC($ParSPArr,$ParDetailSettings)
	{
		global $lpf_slp;
		
		if(is_array($ParSPArr) && !empty($ParSPArr))
		{
			$studentName = $this->getStudentName($ParSPArr);

			$studentGender = ($ParSPArr['Gender'] == "F") ? $this->BiLangOut("['FEMALE']") : $this->BiLangOut("['MALE']");
			
			$photolink = $ParSPArr['OfficialPhoto'][1];
			
			$ContentStr =	"
							<table border=\"1\" width=\"100%\">
							<tr>
							<td width=\"50%\" valign=\"top\">
								<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"padding_all\">
									<tr>
										<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['StudentName']").":</td>
										<td width=\"70%\" style=\"border-right:solid 2px #000\">".$studentName."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['DateOfBirth']").":</td>
										<td style=\"border-right:solid 2px #000\">".$ParSPArr['DateOfBirth']."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['NameOfEducationInstituation']").":</td>
										<td style=\"border-right:solid 2px #000\">".$lpf_slp->stringLanguageSplitByWords($ParSPArr['SchoolName'],'br','b5')."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['AdmissionDate']").":</td>
										<td style=\"border-right:solid 2px #000\">".$ParSPArr['AdmissionDate']."</td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['SchoolAddress']").":</td>
										<td style=\"border-right:solid 2px #000\">".$lpf_slp->stringLanguageSplitByWords($ParSPArr['SchoolAddress'],'br','b5')."</td>
									</tr>
								</table>
							</td>
							<td width=\"50%\" valign=\"top\">
								<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"padding_all\">
									<tr>
										<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['SchoolPhone']").":</td>
										<td width=\"30%\">".$ParSPArr['SchoolPhone']."</td>
										<td width=\"40%\" rowspan=\"5\" align=\"right\" valign=\"top\"><img width=\"100px\" src=\"$photolink\" /></td>
									</tr>
									<tr>
										<td valign=\"top\" nowrap>".$this->BiLangOut("['Gender']").":</td>
										<td>".$studentGender."</td>
										<td></td>
									</tr>
									<tr><td valign=\"top\" nowrap>".$this->BiLangOut("['SchoolCode']").":</td>
										<td>".$ParSPArr['SchoolID']."</td>
										<td></td>
									</tr>
									<tr><td valign=\"top\" nowrap></td>
										<td></td>
									</tr>
									<tr><td valign=\"top\" nowrap></td>
										<td></td>
									</tr>
								</table>
							</td>
							</tr>
							</table>
						";
		}else{
			$ContentStr = "
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td height='100' valign='middle' align='center'>--</td>
								</tr>
							</table>
						";		
		}

		$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($this->BiLangOut("['StudentParticulars']"), "", $ContentStr);
		$ReturnStr .= "<br/>";
		
		//$ReturnStr = str_replace('<--studentImage-->',"<img src='$photolink'>",$ReturnStr);
		return $ReturnStr;
	}
	
	
	

   /**
	* DISPLAY SUDENT NAME IN THE SLP REPORT , IT CAN PRINT THE STUDENT NAME DEPEND ON THE USER SELECTED LANG AND SUPPORT WITH BILINGO
	* @owner : Fai (20110307)
	* @param : Array  $ParSPArr STUDENT INFORMATION
	* @return : String STUDENT NAME
	* 
	*/
	function getStudentName($ParSPArr){
		global $intranet_session_language;

		$studentName = "";
		$printLang = $this->getReportLang();

		switch(strtoupper($printLang)){
			case "BI":
				if(trim($ParSPArr['ChineseName']) == ""){
					//in case no chinese name
					$studentName = $ParSPArr['EnglishName'];
				}else{
					$studentName = $ParSPArr['ChineseName']."<br/>".$ParSPArr['EnglishName'];
				}
				break;
			case "EN":
				$studentName = $ParSPArr['EnglishName'];
				break;
			case "B5":
				$studentName = $ParSPArr['ChineseName'];
				break;
			default:
				$studentName = ($intranet_session_language == 'en') ? $ParSPArr['EnglishName'] : $ParSPArr['ChineseName'];
			break;
		}
		return $studentName;
	}
				


	# Generate student particular table
	function GEN_STUDENT_PARTICULARS_TABLE_SLP_PDF($ParSPArr,$ParDetailSettings)
	{
		global $ec_iPortfolio, $ec_words, $ec_student_word, $iPort;
		global $i_UserStudentName, $i_UserDateOfBirth, $i_HKID, $i_UserGender, $i_no_record_exists_msg;
		global $intranet_session_language;
		global $lpf_slp;
		global $special_feature,$sys_custom;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParSPArr) && !empty($ParSPArr))
			{
				$studentName = $this->getStudentName($ParSPArr);
				$studentGender = ($ParSPArr['Gender'] == "F") ? $this->BiLangOut("['FEMALE']") : $this->BiLangOut("['MALE']");

				$temp_ContentStr = "";
				$MAX_NUM_OF_ROWS = 5;
				
				if (is_array($ParDetailSettings)) {
					$currentRow = 1;
					foreach($ParDetailSettings as $key => $value) {
						if ($value) {
							if ($currentRow%$MAX_NUM_OF_ROWS == 1) {
								$temp_ContentStr .= "<td valign=\"top\"><table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
							}
							switch(strtoupper($key)) {
								case "STUDENTNAME":
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['StudentName']")."</td>
																	<td width=\"60%\">" . $studentName . "</td>
																</tr>
																	";
									break;
								case "DATEOFBIRTH":
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['DateOfBirth']").":</td>
																	<td width=\"60%\">".$ParSPArr['DateOfBirth']."</td>
																</tr>
																	";
									break;
								case "EDUCATIONINSTITUTION":
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['NameOfEducationInstituation']").":</td>
																	<td width=\"60%\">".$ParSPArr['SchoolName']."</td>
																</tr>
																	";
									break;
								case "ADMISSIONDATE":
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['AdmissionDate']").":</td>
																	<td width=\"60%\">".$ParSPArr['AdmissionDate']."</td>
																</tr>
																	";
									break;
								case "SCHOOLADDRESS":
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['SchoolAddress']").":</td>
																	<td width=\"60%\">".$ParSPArr['SchoolAddress']."</td>
																</tr>
																	";
									break;
								case "SCHOOLPHONE":
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['SchoolPhone']").":</td>
																	<td width=\"60%\">".$ParSPArr['SchoolPhone']."</td>
																</tr>
																";
									break;
								case "HKID":
									if ($special_feature['ava_hkid']) {
										$temp_ContentStr .= "
																<tr>
																	<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['HKID']").":</td>
																	<td width=\"60%\">".$ParSPArr['HKID']."</td>
																</tr>
																	";
									}
									break;
								case "GENDER":
										$temp_ContentStr .= "
																	<tr>
																		<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['Gender']").":</td>
																		<td width=\"60%\">".$studentGender."</td>
																	</tr>
																	";
									break;
								case "SCHOOLCODE":
										$temp_ContentStr .= "
																	<tr>
																		<td width=\"30%\" valign=\"top\" nowrap>".$this->BiLangOut("['SchoolCode']").":</td>
																		<td width=\"60%\">".$ParSPArr['SchoolID']."</td>
																	</tr>
																	";
									break;
								default:
									break;							
							}
							if ($currentRow%$MAX_NUM_OF_ROWS == 0) {
								$temp_ContentStr .= "</table></td>";
							}
							$currentRow++;
						}
					}
					if ($currentRow%$MAX_NUM_OF_ROWS != 1) {
						$temp_ContentStr .= "</table></td>";
					}
				} else {
					// do nothing
				}
				// 2020-03-31 (Philips) [2020-0330-1038-43073] - avoid missing <td> element cause TCPDF error
				if($temp_ContentStr=='') $temp_ContentStr = '<td> -- </td>';
				$ContentStr =	"
											<table width=\"100%\" border=\"1\">
												<tr>
													$temp_ContentStr
												</tr>
											</table>
											";
				if($sys_custom['ipf']['slp_report']['ucc']){ // cust report 
					$ParSPArr['UserLogin'] = str_replace('ucc', '', $ParSPArr['UserLogin']);
					
					$ContentStr = '';
					$ContentStr = '<table width="100%" border="1">
													<tr>
														<td valign="top">
															<table border="0" cellpadding="3" cellspacing="0">
																<tr>
																	<td>'.$this->BiLangOut("['StudentName']").':</td>
																	<td>'.$studentName.'</td>
																</tr>
															</table>
														</td>
														<td>
															<table border="0" cellpadding="3" cellspacing="0">
																<tr>
																	<td>'.$this->BiLangOut("['ClassName_ClassNo']").'</td>
																	<td>'.$ParSPArr['ClassName']." (".$ParSPArr['ClassNumber'].')</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td valign="top">
															<table border="0" cellpadding="3" cellspacing="0">
																<tr>
																	<td>'.$this->BiLangOut("['StudentID']").':</td>
																	<td>'.$ParSPArr['UserLogin'].'</td>
																</tr>
															</table>
														</td>
														<td>
															<table border="0" cellpadding="3" cellspacing="0">
																<tr>
																	<td></td>
																	<td></td>
																</tr>
															</table>
														</td>
													</tr>
									</table>';

				}

			}
			else
			{
				$ContentStr = "<table width=\"100%\" border=\"1\"><tr><td height=\"100\" valign=\"middle\" align=\"center\">--</td></tr></table>";
			}

			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($this->BiLangOut("['StudentParticulars']"), "", $ContentStr);
			$ReturnStr .= "<br/>";
//			debug_r(htmlspecialchars($ReturnStr));die;
		}
		else
		{
			/*
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
			}
			*/
		}
		
		return $ReturnStr;
	}

	# Generate accademic performance table
	function GEN_ACADEMIC_PERFORMANCE_TABLE_SLP($ParSubjArr, $ParYearArr, $ParAPArr, $ParSubjectInfoOrderedArr)
	{
		global $ec_iPortfolio, $ec_iPortfolio_Report, $i_no_record_exists_msg;
		global $intranet_session_language;
		global $lpf_slp;
	
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(
					(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
					(is_array($ParYearArr) && !empty($ParYearArr)) &&
					(is_array($ParAPArr) && !empty($ParAPArr))
				)
			{
				$ContentStr =	"<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
				
				# Header row
				$ContentStr .=	"
													<tr style='background:#D9D9D9'>
														<td>&nbsp;</td>
														<!--<td style='font-size=8pt;display:none'>&nbsp;</td>-->
												";				
				foreach($ParYearArr as $Year => $WebSAMSLevel)
					$ContentStr .=	"					
														<td align='center' style='font-size=8pt'>
															<table>
																<tr><td align='center'><strong>".$Year."</strong></td></tr>
																<tr><td align='center'><strong>".$WebSAMSLevel."</strong></td></tr>
															</table>
														</td>
													";
				$ContentStr .=	"
														<!--<td>&nbsp;</td>-->
													</tr>
												";
				# Table header row
				$ContentStr .=	"
													<tr>
														<td style='font-size=8pt'><strong>".$this->BiLangOut("['Subject']")."</strong></td>
														<!--<td align='center' style='font-size=8pt;display:none'><strong>".$this->BiLangOut("['FullMark']")."</strong></td>-->
											";
				for($i=0; $i<count($ParYearArr); $i++)
					$ContentStr .=	"
														<td align='center' style='font-size=8pt'><strong>".$this->BiLangOut("['MarkPerformance']")."</strong></td>
													";
				$ContentStr .=	"
														<!--<td align='center' style='font-size=8pt;display:none'><strong>".$this->BiLangOut("['NameOfPro/ExtAct/Prog']")."</strong></td>-->
													</tr>
												";
												
				# Main content
				// Changed the Subject looping logic to follow the School Settings now
//				foreach($ParAPArr as $subjectcode => $v)
//				{
//					# Display subject name in one language only
//					# (may display bilingual)
//					$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
//					$ContentStr .=	"
//														<tr>
//															<td style='font-size=8pt'>".$ParSubjArr[$subjectcode][$subj_lang]."</td>
//															<!--<td style='display:none'>&nbsp;</td>-->
//													";
//					foreach($ParYearArr as $Year => $WebSAMSLevel)
//					{
//						$ContentStr .=	"
//															<td align='center' style='font-size=8pt'>".($ParAPArr[$subjectcode][$Year]['Score'] <= 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
//														";
//					}
//					$ContentStr .=	"
//															<!--<td style='display:none'>&nbsp;</td>-->
//														</tr>
//													";
//				}

				$numOfSubject = count($ParSubjectInfoOrderedArr);
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$subjectcode =  $ParSubjectInfoOrderedArr[$i]['SubjectShortNameEN'];
					
					if (isset($ParAPArr[$subjectcode]))
					{
						$v = $ParAPArr[$subjectcode];
						
						# Display subject name in one language only
						# (may display bilingual)
						$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
						$ContentStr .=	"
															<tr>
																<td style='font-size=8pt'>".$ParSubjArr[$subjectcode][$subj_lang]."</td>
																<!--<td style='display:none'>&nbsp;</td>-->
														";
						foreach($ParYearArr as $Year => $WebSAMSLevel)
						{
							$ContentStr .=	"
																<td align='center' style='font-size=8pt'>".($ParAPArr[$subjectcode][$Year]['Score'] <= 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
															";
						}
						$ContentStr .=	"
																<!--<td style='display:none'>&nbsp;</td>-->
															</tr>
														";
					}
				}
		
				$ContentStr .=	"
													</table>
												";
			}
			else
	//			$ContentStr = $i_no_record_exists_msg;
				$ContentStr = "
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td height='100' valign='middle' align='center'>--</td>
													</tr>
												</table>
											";
				
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['AcademicPerformanceInSchool']"), "", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(
							(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
							(is_array($ParYearArr) && !empty($ParYearArr) && (in_array('S6',$ParYearArr) || in_array('F6',$ParYearArr))) &&
							(is_array($ParAPArr) && !empty($ParAPArr))
						)
					{
						$ReturnStr =	"
														<table width='100%' border='0' cellpadding='3' cellspacing='0' class='border_table' style='page-break-after:always'>
													";
						
						# Header row
						$ReturnStr .=	"
															<tr>
																<td class='bottom_border' style='border-bottom-style:solid;'>&nbsp;</td>
																<td class='cell_font_arial bottom_border' style='display:none;border-bottom-style:solid;'>&nbsp;</td>
													";
						foreach($ParYearArr as $Year => $WebSAMSLevel)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial bottom_border' style='border-bottom-style:solid;'><strong>
																".$Year."<br/>
																".$WebSAMSLevel."
																</strong></td>
															";
						$ReturnStr .=	"
															</tr>
													";
						# Table header row
						$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'><strong>".$ec_iPortfolio_Report['subject']."</strong></td>
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['full_mark']."</strong></td>
													";
						for($i=0; $i<count($ParYearArr); $i++)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial'><strong>".$ec_iPortfolio['mark_performance']."</strong></td>
														";
						$ReturnStr .=	"
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['proj_ext_act_prog']."</strong></td>
															</tr>
													";
														
						# Main content
						foreach($ParAPArr as $subjectcode => $v)
						{
							# Display subject name in one language only
							# (may display bilingual)
							$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
							$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'>".$ParSubjArr[$subjectcode][$subj_lang]."</td>
																<td style='display:none'>&nbsp;</td>
														";
							foreach($ParYearArr as $Year => $WebSAMSLevel)
							{
								$ReturnStr .=	"
																<td align='center' class='cell_font_arial'>".($ParAPArr[$subjectcode][$Year]['Score'] <= 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
															";
							}
							$ReturnStr .=	"
																<td style='display:none'>&nbsp;</td>
															</tr>
														";
						}
				
						$ReturnStr .=	"
														</table>
													";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}
	
	# Generate accademic performance table
	function GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID($ParSubjArr, $ParYearArr, $ParAPArr, $ParSubjectInfoOrderedArr, $ParSubjectFullMarkArr, $ParPrintMode='', $ParDisplayFullMark=true, $ParEmptySymbol=null, $ParDisplayComponentSubject=false)
	{
		global $ec_iPortfolio, $ec_iPortfolio_Report, $i_no_record_exists_msg;
		global $intranet_session_language;
		global $lpf_slp, $ipf_cfg, $sys_custom;
		
		$ParPrintMode = ($ParPrintMode=='')? $ipf_cfg['slpAcademicResultPrintMode_default'] : $ParPrintMode;
		
		if ($ParEmptySymbol===null) {
			//$ParEmptySymbol = ($sys_custom['iPf']['Report']['SLP']['AcademicResultShowEmptySymbol'])? $ipf_cfg['slpAcademicResultEmptySymbol_default'] : '';
			$ParEmptySymbol = $ipf_cfg['slpAcademicResultEmptySymbol_default'];
		}
	
		$reportStudentId = $this->getReportStudentId();
		$ReportYearArr = $ParYearArr[$reportStudentId];
		
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(
					(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
					(is_array($ReportYearArr) && !empty($ReportYearArr)) &&
					(is_array($ParAPArr) && !empty($ParAPArr))
				)
			{
				
				if (!$sys_custom['iPf']['Report']['AcademicResult']['ShowAllTermResult']) {
					$tempAPArr = array();
					foreach((array)$ParAPArr as $_subjectId => $_subjectInfoAry) {
						foreach((array)$_subjectInfoAry as $__academicYearId => $__subjectAcademicInfoAry) {
							$tempAPArr[$_subjectId][$__academicYearId][0] = $__subjectAcademicInfoAry;
						}
					}
					$ParAPArr = $tempAPArr;
					unset($tempAPArr);
				}
				
				
				$ContentStr =	"<table width='100%' border='0' cellpadding='3' cellspacing='0' class='table_pb table_print'>";
				
				# Header row
				$ContentStr .=	"
													<thead><tr style='background:#D9D9D9'>
														<td class=\"acadamic_t_boarder\">&nbsp;</td>
												";
				
				$displayedFullMark = false;
				foreach($ReportYearArr as $thisAcademicYearID => $thisAcademicYearInfoArr)
				{
					// calculate the number of colspan for each year
					$thisNumOfTerm = count((array)$thisAcademicYearInfoArr['YearTermAry']);
					if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
						$thisNumOfColSpan = $thisNumOfTerm;
					}
					else {
						$thisNumOfColSpan = 1;	// show one result column in general case
					}
					
					// get the student form and academic year name info
					$thisAcademicYearName = $thisAcademicYearInfoArr['AcademicYearName'];
					$thisFormWebSAMSCode = $thisAcademicYearInfoArr['FormWebSAMSCode'];
					
					// check if need to display full mark or not
					$_displayFullMarkCol = false;
					if ($ParDisplayFullMark) {
						if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
							if (!$displayedFullMark) {
								$_displayFullMarkCol = true;
							}
						}
						else {
							$_displayFullMarkCol = true;
						}
					}
					
					// full mark display
					if ($_displayFullMarkCol) {
						$ContentStr .=	"
													<td class=\"acadamic_t_boarder\"style='font-size=8pt;'>&nbsp;</td>
													";
						$displayedFullMark = true;
					}
					
					// academic year name and form websams code display
					$ContentStr .=	"
														<td class=\"acadamic_t_boarder\" align='center' style='font-size=8pt' colspan='$thisNumOfColSpan'>
															<table>
																<tr><td align='center'><strong>".$thisAcademicYearName."</strong></td></tr>
																<tr><td align='center'><strong>".$thisFormWebSAMSCode."</strong></td></tr>
															</table>
														</td>
													";
				}
				
				
				
				$ContentStr .=	"
														<!--<td>&nbsp;</td>-->
													</tr>
												";
				# Table header row
				$ContentStr .=	"
													<tr>
														<td class=\"acadamic_t_boarder\" style='font-size=8pt'><strong>".$this->BiLangOut("['Subject']")."</strong></td>
											";
				
				$numOfAcademicYear = count($ReportYearArr);
				$displayedFullMark = false;
				//for($i=0; $i<$numOfAcademicYear; $i++)
				foreach ((array)$ReportYearArr as $_academicYearId => $_academicYearInfoAry)
				{
					$_displayFullMarkCol = false;
					if ($ParDisplayFullMark) {
						if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
							if (!$displayedFullMark) {
								$_displayFullMarkCol = true;
							}
						}
						else {
							$_displayFullMarkCol = true;
						}
					}
					
					if ($_displayFullMarkCol) {
						$ContentStr .=	"
														<td class=\"acadamic_t_boarder\" align='center' style='font-size=8pt;'><strong>".$this->BiLangOut("['FullMark']")."</strong></td>
													";
						$displayedFullMark = true;
					}
					
					if ($sys_custom['iPf']['Report']['AcademicResult']['ShowAllTermResult']) {
						foreach ((array)$_academicYearInfoAry['YearTermAry'] as $__yearTermId => $__academicYearTermInfoAry) {
							$__yearTermNameEn = $__academicYearTermInfoAry['YearTermNameEn'];
							$__yearTermNameCh = $__academicYearTermInfoAry['YearTermNameCh'];
							
							$ContentStr .=	"
														<td class=\"acadamic_t_boarder\" align='center' style='font-size=8pt'><strong>".$__yearTermNameCh.'<br>'.$__yearTermNameEn."</strong></td>
													";
						}
					}
					else {
						$ContentStr .=	"
														<td class=\"acadamic_t_boarder\" align='center' style='font-size=8pt'><strong>".$this->BiLangOut("['MarkPerformance']")."</strong></td>
													";
					}
				}
				
				$ContentStr .=	"
														<!--<td align='center' style='font-size=8pt;display:none'><strong>".$this->BiLangOut("['NameOfPro/ExtAct/Prog']")."</strong></td>-->
													</tr></thead><tbody>
												";
			
				$numOfSubject = count($ParSubjectInfoOrderedArr);
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$thisSubjectID =  $ParSubjectInfoOrderedArr[$i]['SubjectID'];
					$thisWEBSAMSCode =  $ParSubjectInfoOrderedArr[$i]['WEBSAMSCode'];
					$thisIsComponent =  $ParSubjectInfoOrderedArr[$i]['IsComponent'];
					$thisDisplayedFullMark = false;
					
					if (!$ParDisplayComponentSubject && $thisIsComponent) {
						continue;
					}
					
					if (in_array($thisWEBSAMSCode, (array)$sys_custom['iPf']['Report']['AcademicResult']['ComponentSubjectAsParentSubjectCodeAry'])) {
						if ($thisIsComponent == 0) {
							// skip parent subject
							continue;
						}
						else {
							// display component as parent subject
							$thisIsComponent = 0;
						}
					}
					
					//$thisFontStyleAttr = '';
					$thisFontSizeAttr = 'font-size:10pt;';
					if ($thisIsComponent) {
						// disable because PDF not support
						//$thisFontStyleAttr = 'font-style:italic;';
						$thisFontSizeAttr = 'font-size:8pt;';
					}
					
					if (isset($ParAPArr[$thisSubjectID]))
					{
						//$v = $ParAPArr[$thisSubjectID];
						$thisSubjectTr = '';
						$thisSubjectAllScoreEmpty = true;
						
						# Display subject name in one language only
						# (may display bilingual)
						$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
						if($thisIsComponent && $sys_custom['iPf']['Report']['AcademicResult']['cmpSubjectStyle']){
							switch($sys_custom['iPf']['Report']['AcademicResult']['cmpSubjectStyle']){
								//S114597 
								case 1:
									$thisSubjectTr .=	"
															<tr>
																<td class=\"acadamic_t_boarder\" style='".$thisFontSizeAttr."'>&nbsp;&nbsp;".$ParSubjArr[$thisSubjectID][$subj_lang]."</td>
														";
									break;
							}
						}else{
							$thisSubjectTr .=	"
															<tr>
																<td class=\"acadamic_t_boarder\" style='".$thisFontSizeAttr."'>".$ParSubjArr[$thisSubjectID][$subj_lang]."</td>
														";
						}							
														
						// Loop each Academic Year
						foreach($ReportYearArr as $thisAcademicYearID => $thisAcademicYearInfoArr)
						{
							$thisYearID = $thisAcademicYearInfoArr['YearID'];
														
							$thisFullMarkInt = $ParSubjectFullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID]['FullMarkInt'];
							$thisFullMarkGrade = $ParSubjectFullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID]['FullMarkGrade'];
							$thisFullMark = ($thisFullMarkGrade == '')? $thisFullMarkInt : $thisFullMarkGrade;
							$thisPassMark = $ParSubjectFullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID]['PassMarkInt'];
							
							### Full Mark Display
							$thisFullMark = '';
							switch ($ParPrintMode) {
								case $ipf_cfg['slpAcademicResultPrintMode']['mark']:
									$thisFullMark = $thisFullMarkInt;
									break;
								case $ipf_cfg['slpAcademicResultPrintMode']['grade']:
									$thisFullMark = $thisFullMarkGrade;
									break;
								case $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']:
									$thisFullMark = ($thisFullMarkInt == '')? $thisFullMarkGrade : $thisFullMarkInt;
									break;
								case $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']:
									$thisFullMark = ($thisFullMarkGrade == '')? $thisFullMarkInt : $thisFullMarkGrade;
									break;
							}

							foreach((array)$thisAcademicYearInfoArr['YearTermAry'] as $thisYearTermID => $thisAcademicYearTermInfoArr) {
								### Score Display
								$thisScore = trim($ParAPArr[$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['Score']);
								$thisGrade = trim($ParAPArr[$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['Grade']);
								
								$thisScoreDisplayPrefix = '';
								$thisScoreDisplaySuffix = '';
								if ($sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbol']) {
								    if (is_numeric($thisScore) && $thisPassMark > 0) {
								        if ($thisScore < $thisPassMark) {
								            $thisScoreDisplayPrefix = $sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbolPrefix'];
								            $thisScoreDisplaySuffix = $sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbolSuffix'];
								        }
								    }
								}
								
								$thisScoreDisplay = '';
								switch ($ParPrintMode) {
									case $ipf_cfg['slpAcademicResultPrintMode']['mark']:
										$thisScoreDisplay = ($thisScore < 0) ? '' : $thisScore;
										break;
									case $ipf_cfg['slpAcademicResultPrintMode']['grade']:
										$thisScoreDisplay = $thisGrade;
										break;
									case $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']:
										$thisScoreDisplay = ($thisScore < 0 || $thisScore == '') ? $thisGrade : $thisScore;
										break;
									case $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']:
										$thisScoreDisplay = ($thisGrade=='') ? $thisScore : $thisGrade;
										break;
								}
								
								if ($thisScoreDisplay != '') {
									$thisSubjectAllScoreEmpty = false;
								}
								
								
								$_displayFullMarkCol = false;
								if ($ParDisplayFullMark) {
									if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
										if (!$thisDisplayedFullMark) {
											$_displayFullMarkCol = true;
										}
									}
									else {
										$_displayFullMarkCol = true;
									}
								}
								
								if ($_displayFullMarkCol) {
									//2014-0310-1425-12073
									//$thisFullMark = ($thisFullMark=='')? $ParEmptySymbol : $thisFullMark;
									if (!$thisIsComponent && in_array($thisWEBSAMSCode, (array)$sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectCodeAry'])) {
										//S114597
										if($sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectEmptySymbol']){
											$thisFullMark = '';
										}else{
											$thisFullMark = $ParEmptySymbol;
										}
									}
									else {
										$thisFullMark = ($thisFullMark=='')? $ParEmptySymbol : $thisFullMark;	
									}
									
									// fullmark changed to decimal
									$fullmarkArr = explode('.',$thisFullMark);
									if($fullmarkArr[1] === '0'){
										// if demical is 0 ignore it.
										$thisFullMark = (int)$thisFullMark;
									}
									
									// #W109947 
									if($sys_custom['iPf']['Report']['AcademicResult']['HideFullmarkIfDroped'] && $thisScore == '' && $thisGrade == ''){
										$thisFullMark = $ParEmptySymbol;
									}
									
									$thisSubjectTr .=	"
																	<td class=\"acadamic_t_boarder\" align='center' style='".$thisFontSizeAttr."'>".$thisFullMark."</td>
																";
																
									$thisDisplayedFullMark = true;
								}
								
								//2014-0310-1425-12073
								//$thisScoreDisplay = ($thisScoreDisplay=='')? $ParEmptySymbol : $thisScoreDisplay;
								if (!$thisIsComponent && in_array($thisWEBSAMSCode, (array)$sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectCodeAry'])) {
									if($sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectEmptySymbol']){
										//S114597
										$thisScoreDisplay = '';
									}else{
										$thisScoreDisplay = $ParEmptySymbol;
									}
								}
								else {
								    $thisScoreDisplay = ($thisScoreDisplay=='')? $ParEmptySymbol : $thisScoreDisplayPrefix.$thisScoreDisplay.$thisScoreDisplaySuffix;
								}
								
								$thisSubjectTr .=	"
																	<td class=\"acadamic_t_boarder\" align='center' style='".$thisFontSizeAttr."'>".$thisScoreDisplay."</td>
																";
							}
						}
						$thisSubjectTr .=	"
																<!--<td style='display:none'>&nbsp;</td>-->
															</tr>
														";
						
						if (!$thisSubjectAllScoreEmpty) {
							$ContentStr .= $thisSubjectTr;
						}
					}
				}
		
				$ContentStr .=	"
													</tbody></table>
												";
			}
			else
	//			$ContentStr = $i_no_record_exists_msg;
				$ContentStr = "
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td height='100' valign='middle' align='center'>--</td>
													</tr>
												</table>
											";
				
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['AcademicPerformanceInSchool']"), "", $ContentStr);
			if ($sys_custom['ipf']['slp_report']['customizeHeaderSchoolCode'] == 'ucc') {
				$ReturnStr .= "EX - Excellent, GD - Good, AV - Average, S - Satisfactory, U - Unsatisfactory";
			}
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(
							(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
							(is_array($ParYearArr) && !empty($ParYearArr) && (in_array('S6',$ParYearArr) || in_array('F6',$ParYearArr))) &&
							(is_array($ParAPArr) && !empty($ParAPArr))
						)
					{
						$ReturnStr =	"
														<table width='100%' border='0' cellpadding='3' cellspacing='0' class='border_table' style='page-break-after:always'>
													";
						
						# Header row
						$ReturnStr .=	"
															<tr>
																<td class='bottom_border' style='border-bottom-style:solid;'>&nbsp;</td>
																<td class='cell_font_arial bottom_border' style='display:none;border-bottom-style:solid;'>&nbsp;</td>
													";
						foreach($ParYearArr as $Year => $WebSAMSLevel)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial bottom_border' style='border-bottom-style:solid;'><strong>
																".$Year."<br/>
																".$WebSAMSLevel."
																</strong></td>
															";
						$ReturnStr .=	"
															</tr>
													";
						# Table header row
						$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'><strong>".$ec_iPortfolio_Report['subject']."</strong></td>
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['full_mark']."</strong></td>
													";
						for($i=0; $i<count($ParYearArr); $i++)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial'><strong>".$ec_iPortfolio['mark_performance']."</strong></td>
														";
						$ReturnStr .=	"
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['proj_ext_act_prog']."</strong></td>
															</tr>
													";
														
						# Main content
						foreach($ParAPArr as $subjectcode => $v)
						{
							# Display subject name in one language only
							# (may display bilingual)
							$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
							$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'>".$ParSubjArr[$subjectcode][$subj_lang]."</td>
																<td style='display:none'>&nbsp;</td>
														";
							foreach($ParYearArr as $Year => $WebSAMSLevel)
							{
								$ReturnStr .=	"
																<td align='center' class='cell_font_arial'>".($ParAPArr[$subjectcode][$Year]['Score'] <= 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
															";
							}
							$ReturnStr .=	"
																<td style='display:none'>&nbsp;</td>
															</tr>
														";
						}
				
						$ReturnStr .=	"
														</table>
													";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}
	
		# Generate accademic performance table
	function GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF($ParSubjArr, $ParYearArr, $ParAPArr, $ParSubjectInfoOrderedArr)
	{
		global $ec_iPortfolio, $ec_iPortfolio_Report, $i_no_record_exists_msg;
		global $intranet_session_language;
		global $lpf_slp;
	
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(
					(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
					(is_array($ParYearArr) && !empty($ParYearArr)) &&
					(is_array($ParAPArr) && !empty($ParAPArr))
				)
			{
				$ContentStr =	"<table width=\"100%\" border=\"1\" cellspacing=\"0\">";
				
				# Header row
				$ContentStr .=	"
													<tr style=\"background:#D9D9D9;\">
														<td>&nbsp;</td>
														<!--<td style=\"font-size=8pt;display:none\">&nbsp;</td>-->
												";
				foreach($ParYearArr as $Year => $WebSAMSLevel)
					$ContentStr .=	"
														<td align=\"center\" style=\"font-size=8pt;\"><strong>".$Year."<br/>".$WebSAMSLevel."</strong></td>
													";
				$ContentStr .=	"
														<!--<td>&nbsp;</td>-->
													</tr>
												";
				# Table header row
				$ContentStr .=	"
													<tr>
														<td style=\"font-size=8pt\"><strong>".$this->BiLangOut("['Subject']")."</strong></td>
														<!--<td align=\"center\" style=\"font-size=8pt;display:none\"><strong>".$this->BiLangOut("['FullMark']")."</strong></td>-->
											";
				for($i=0; $i<count($ParYearArr); $i++)
					$ContentStr .=	"
														<td align=\"center\" style=\"font-size=8pt\"><strong>".$this->BiLangOut("['MarkPerformance']")."</strong></td>
													";
				$ContentStr .=	"
														<!--<td align=\"center\" style=\"font-size=8pt;display:none\"><strong>".$this->BiLangOut("['NameOfPro/ExtAct/Prog']")."</strong></td>-->
													</tr>
												";
												
				# Main content
//				foreach($ParAPArr as $subjectcode => $v)
//				{
//					# Display subject name in one language only
//					# (may display bilingual)
//					$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
//					$ContentStr .=	"
//														<tr>
//															<td style=\"font-size=8pt\">".$ParSubjArr[$subjectcode][$subj_lang]."</td>
//															<!--<td style=\"display:none\">&nbsp;</td>-->
//													";
//					foreach($ParYearArr as $Year => $WebSAMSLevel)
//					{
//						$ContentStr .=	"
//															<td align=\"center\" style=\"font-size=8pt\">".($ParAPArr[$subjectcode][$Year]['Score'] == 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
//														";
//					}
//					$ContentStr .=	"
//															<!--<td style=\"display:none\">&nbsp;</td>-->
//														</tr>
//													";
//				}

				$numOfSubject = count($ParSubjectInfoOrderedArr);
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$subjectcode =  $ParSubjectInfoOrderedArr[$i]['SubjectShortNameEN'];
					
					if (isset($ParAPArr[$subjectcode]))
					{
						$v = $ParAPArr[$subjectcode];
						
						# Display subject name in one language only
						# (may display bilingual)
						$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
						$ContentStr .=	"
															<tr>
																<td style=\"font-size=8pt\">".$ParSubjArr[$subjectcode][$subj_lang]."</td>
																<!--<td style=\"display:none\">&nbsp;</td>-->
														";
						foreach($ParYearArr as $Year => $WebSAMSLevel)
						{
							$ContentStr .=	"
																<td align=\"center\" style=\"font-size=8pt\">".($ParAPArr[$subjectcode][$Year]['Score'] == 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
															";
						}
						$ContentStr .=	"
																<!--<td style=\"display:none\">&nbsp;</td>-->
															</tr>
														";
					}
				}
				
		
				$ContentStr .=	"
													</table>
												";
			}
			else
			{
				$ContentStr = "<table width=\"100%\" border=\"1\" cellspacing=\"0\">
									<tr>
										<td height=\"100\" valign=\"middle\" align=\"center\">--</td>
									</tr>
								</table>
							  ";
			}			
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($this->BiLangOut("['AcademicPerformanceInSchool']"), "", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(
							(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
							(is_array($ParYearArr) && !empty($ParYearArr) && (in_array('S6',$ParYearArr) || in_array('F6',$ParYearArr))) &&
							(is_array($ParAPArr) && !empty($ParAPArr))
						)
					{
						$ReturnStr =	"
														<table width='100%' border='0' cellpadding='3' cellspacing='0' class='border_table' style='page-break-after:always'>
													";
						
						# Header row
						$ReturnStr .=	"
															<tr>
																<td class='bottom_border' style='border-bottom-style:solid;'>&nbsp;</td>
																<td class='cell_font_arial bottom_border' style='display:none;border-bottom-style:solid;'>&nbsp;</td>
													";
						foreach($ParYearArr as $Year => $WebSAMSLevel)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial bottom_border' style='border-bottom-style:solid;'><strong>".$Year."<br/>".$WebSAMSLevel."</strong></td>
															";
						$ReturnStr .=	"
															</tr>
													";
						# Table header row
						$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'><strong>".$ec_iPortfolio_Report['subject']."</strong></td>
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['full_mark']."</strong></td>
													";
						for($i=0; $i<count($ParYearArr); $i++)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial'><strong>".$ec_iPortfolio['mark_performance']."</strong></td>
														";
						$ReturnStr .=	"
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['proj_ext_act_prog']."</strong></td>
															</tr>
													";
														
						# Main content
						foreach($ParAPArr as $subjectcode => $v)
						{
							# Display subject name in one language only
							# (may display bilingual)
							$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
							$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'>".$ParSubjArr[$subjectcode][$subj_lang]."</td>
																<td style='display:none'>&nbsp;</td>
														";
							foreach($ParYearArr as $Year => $WebSAMSLevel)
							{
								$ReturnStr .=	"
																<td align='center' class='cell_font_arial'>".($ParAPArr[$subjectcode][$Year]['Score'] == 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
															";
							}
							$ReturnStr .=	"
																<td style='display:none'>&nbsp;</td>
															</tr>
														";
						}
				
						$ReturnStr .=	"
														</table>
													";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}
	
	function GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($ParSubjArr, $ParYearArr, $ParAPArr, $ParSubjectInfoOrderedArr, $ParSubjectFullMarkArr, $ParPrintMode='', $ParDisplayFullMark=true, $ParEmptySymbol=null, $ParDisplayComponentSubject=false, $parLowerForm=false)
	{

		global $ec_iPortfolio, $ec_iPortfolio_Report, $i_no_record_exists_msg;
		global $intranet_session_language;
		global $lpf_slp, $ipf_cfg, $sys_custom;
		
		//$fontSize = 8;
		$fontSize = 8;
		$HeaderFontSize = 8;
		$boldStyleStart = '<strong>';
		$boldStyleEnd = '</strong>';
		$headerBackGroudStyle= '';	
		$headerTextColor = '';
		$tableCellPadding = '';
		//handle font size
		if(strtolower($this->getPrintRequsetFrom()) == strtolower($ipf_cfg['slpAcademicResultPrintRequestFrom']['DynamicReport'])){			
			if($sys_custom['iPf']['DynamicReport']['AcademicReportSectionDisplayStyleSameAsDataSection']){
				$boldStyleStart = '';
				$boldStyleEnd = '';
				$headerBackGroudStyle= 'background-color:#888888;';	
				$headerTextColor = 'color:#FFFFFF;';
				$tableCellPadding = 'cellpadding="2"';
				$HeaderFontSize = 10;
			}
			// [2015-0401-1610-41164] Set font size of Academic Performance table in dynamic report to 10pt
			if($sys_custom['iPf']['DynamicReport']['AcademicResultSectionDisplay10ptFontSize']){
				$HeaderFontSize = 10;
				$fontSize = 10;
			}
		}		

		$ParPrintMode = ($ParPrintMode=='')? $ipf_cfg['slpAcademicResultPrintMode_default'] : $ParPrintMode;
		
		if ($ParEmptySymbol===null) {
			//$ParEmptySymbol = ($sys_custom['iPf']['Report']['SLP']['AcademicResultShowEmptySymbol'])? $ipf_cfg['slpAcademicResultEmptySymbol_default'] : '';
			$ParEmptySymbol = $ipf_cfg['slpAcademicResultEmptySymbol_default'];
		}
		
		$reportStudentId = $this->getReportStudentId();
		$ReportYearArr = $ParYearArr[$reportStudentId];

		$subjectNameTdWidth = '';  // seem not work , leave it empty first
		$fullMarkTdWidth = '';     // seem not work , leave it empty first

		$subjectNameTdColSpan = '';
		if($sys_custom['iPf']['Report']['academicResult']['SubjectNoWrap']){
			//with plugin fist 
			$subjectNameTdColSpan = ' colspan="2" ';
		}

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{

			if(
					(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
					(is_array($ReportYearArr) && !empty($ReportYearArr)) &&
					(is_array($ParAPArr) && !empty($ParAPArr))
				)
			{
				
				if (!$sys_custom['iPf']['Report']['AcademicResult']['ShowAllTermResult']) {
					$tempAPArr = array();
					foreach((array)$ParAPArr as $_subjectId => $_subjectInfoAry) {
						foreach((array)$_subjectInfoAry as $__academicYearId => $__subjectAcademicInfoAry) {
							$tempAPArr[$_subjectId][$__academicYearId][0] = $__subjectAcademicInfoAry;
						}
					}
					$ParAPArr = $tempAPArr;
					unset($tempAPArr);
				}
				
				$ContentStr =	"<table width=\"100%\" border=\"1\" cellspacing=\"0\" ".$tableCellPadding.">";
//				$ContentStr =	"<table border=\"1\" cellspacing=\"0\" ".$tableCellPadding.">";
				
				# Header row
				//2012-0313-1101-36071
//				$ContentStr .=	"
//													<tr style=\"background:#D9D9D9;\">
//														<td style=\"font-size:".$fontSize."pt;".$headerBackGroudStyle."\">&nbsp;</td>
//												";
				// This is subject name empty column (empty header)
				$ContentStr .=	"
													<tr style=\"background:#D9D9D9;\">
														<td {$subjectNameTdColSpan} {$subjectNameTdWidth} style=\"font-size:".$fontSize."pt;".$headerBackGroudStyle."\"></td>
												";
												
				//Handle DISPLAY ACADEMIC YEAR COLUMN
				$displayedFullMark = false;
				foreach($ReportYearArr as $thisAcademicYearID => $thisAcademicYearInfoArr)
				{
					// calculate the number of colspan for each year
					$thisNumOfTerm = count((array)$thisAcademicYearInfoArr['YearTermAry']);
					if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
						$thisNumOfColSpan = $thisNumOfTerm;
					}
					else {
						$thisNumOfColSpan = 1;	// show one result column in general case
					}
					
					$thisAcademicYearName = $thisAcademicYearInfoArr['AcademicYearName'];
					$thisFormWebSAMSCode = $thisAcademicYearInfoArr['FormWebSAMSCode'];
					
					// check if need to display full mark or not
					$_displayFullMarkCol = false;
					if ($ParDisplayFullMark) {
						if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
							if (!$displayedFullMark) {
								$_displayFullMarkCol = true;
							}
						}
						else {
							$_displayFullMarkCol = true;
						}
					}
					
					// full mark display
					if ($_displayFullMarkCol) {
						//2012-0313-1101-36071
//						$ContentStr .=	"
//														<td style=\"font-size:".$fontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\">&nbsp;</td>
//													";
						//full mark empty header
						$ContentStr .=	"
														<td {$fullMarkTdWidth} style=\"font-size:".$fontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\"></td>
													";
						$displayedFullMark = true;
					}
					
					//This is ACADEMIC YEAR column
					$ContentStr .=	"
														<td align=\"center\" colspan=\"".$thisNumOfColSpan."\" style=\"font-size:".$HeaderFontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\">".$boldStyleStart.$thisAcademicYearName.$boldStyleEnd."<br/>".$boldStyleStart.$thisFormWebSAMSCode.$boldStyleEnd."</td>
													";
				}
				
				$ContentStr .=	"
													</tr>
												";
				# Table header row
				$ContentStr .=	"
													<tr>
														<td {$subjectNameTdColSpan} {$subjectNameTdWidth} style=\"font-size:".$HeaderFontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\">".$boldStyleStart.$this->BiLangOut("['Subject']").$boldStyleEnd."</td>
											";

				$numOfAcademicYear = count($ReportYearArr);
				$displayedFullMark = false;
				//for($i=0; $i<$numOfAcademicYear; $i++)
				foreach ((array)$ReportYearArr as $_academicYearId => $_academicYearInfoAry)
				{
					$_displayFullMarkCol = false;
					if ($ParDisplayFullMark) {
						if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
							if (!$displayedFullMark) {
								$_displayFullMarkCol = true;
							}
						}
						else {
							$_displayFullMarkCol = true;
						}
					}
					
					if ($_displayFullMarkCol) {
						$ContentStr .=	"
			<td {$fullMarkTdWidth} align=\"center\" style=\"font-size:".$HeaderFontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\">".$boldStyleStart.$this->BiLangOut("['FullMark']").$boldStyleEnd."</td>
													";
						$displayedFullMark = true;
					}
					
					
					if ($sys_custom['iPf']['Report']['AcademicResult']['ShowAllTermResult']) {
						foreach ((array)$_academicYearInfoAry['YearTermAry'] as $__yearTermId => $__academicYearTermInfoAry) {
							$__yearTermNameEn = $__academicYearTermInfoAry['YearTermNameEn'];
							$__yearTermNameCh = $__academicYearTermInfoAry['YearTermNameCh'];
							
							$ContentStr .=	"<td align=\"center\" style=\"font-size:".$HeaderFontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\">".$boldStyleStart.$__yearTermNameCh.'<br/>'.$__yearTermNameEn.$boldStyleEnd."</td>";
						}
					}
					else {
						$markPerformanceLang = 'MarkPerformance';
						if($sys_custom['iPf']['Report']['academicResult']['SubjectNoWrap']){
							$markPerformanceLang = 'MarkPerformanceWithBR';
						}
						$ContentStr .=	"<td align=\"center\"				style=\"font-size:".$HeaderFontSize."pt;".$headerBackGroudStyle."".$headerTextColor."\">".$boldStyleStart.$this->BiLangOut("['".$markPerformanceLang."']").$boldStyleEnd."</td>";
					}
				}				
				$ContentStr .=	"</tr>";
								
				$numOfSubject = count($ParSubjectInfoOrderedArr);
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$thisSubjectID =  $ParSubjectInfoOrderedArr[$i]['SubjectID'];
					$thisWEBSAMSCode =  $ParSubjectInfoOrderedArr[$i]['WEBSAMSCode'];
					$thisIsComponent =  $ParSubjectInfoOrderedArr[$i]['IsComponent'];
					$thisDisplayedFullMark = false;
					
					if (!$ParDisplayComponentSubject && $thisIsComponent) {
						continue;
					}
 					
					// V113395 
					if(in_array($thisSubjectID, (array)$sys_custom['iPf']['Report']['SkipSubjectIdArr'])){
						continue;
					}
					
					if (in_array($thisWEBSAMSCode, (array)$sys_custom['iPf']['Report']['AcademicResult']['ComponentSubjectAsParentSubjectCodeAry'])) {
						if ($thisIsComponent == 0) {
							// skip parent subject
							continue;
						}
						else {
							// display component as parent subject
							$thisIsComponent = 0;
						}
					}
					
					$thisFontSizeAttr = 'font-size:'.$fontSize.'pt;';
					if ($thisIsComponent) {
						$thisFontSizeAttr = 'font-size:6pt;';
					}
					
					if (isset($ParAPArr[$thisSubjectID]))
					{
						$v = $ParAPArr[$thisSubjectID];
						$thisSubjectTr = '';
						$thisSubjectAllScoreEmpty = true;
						
						# Display subject name in one language only
						# (may display bilingual)
						$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
						$thisSubjectTr .=	"
															<tr>
																<td {$subjectNameTdColSpan} {$subjectNameTdWidth} style=\"".$thisFontSizeAttr."\">".$ParSubjArr[$thisSubjectID][$subj_lang]." </td>
														";

						
						// Loop each Academic Year
						foreach($ReportYearArr as $thisAcademicYearID => $thisAcademicYearInfoArr)
						{
							### Full Mark Display
							//$thisYearID = $ParAPArr[$thisSubjectID][$thisAcademicYearID]['YearID'];
							$thisYearID = $thisAcademicYearInfoArr['YearID'];
							
							$thisFullMarkInt = $ParSubjectFullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID]['FullMarkInt'];
							$thisFullMarkGrade = $ParSubjectFullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID]['FullMarkGrade'];
							$thisFullMark = ($thisFullMarkGrade == '')? $thisFullMarkInt : $thisFullMarkGrade;
							$thisPassMark = $ParSubjectFullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID]['PassMarkInt'];
							
							$thisFullMark = '';
							switch ($ParPrintMode) {
								case $ipf_cfg['slpAcademicResultPrintMode']['mark']:
									$thisFullMark = $thisFullMarkInt;
									break;
								case $ipf_cfg['slpAcademicResultPrintMode']['grade']:
									$thisFullMark = $thisFullMarkGrade;
									break;
								case $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']:
									$thisFullMark = ($thisFullMarkInt == '')? $thisFullMarkGrade : $thisFullMarkInt;
									break;
								case $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']:
									$thisFullMark = ($thisFullMarkGrade == '')? $thisFullMarkInt : $thisFullMarkGrade;
									break;
							}
							
							foreach((array)$thisAcademicYearInfoArr['YearTermAry'] as $thisYearTermID => $thisAcademicYearTermInfoArr) {
								### Score Display
								$thisScore = trim($ParAPArr[$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['Score']);
								$thisGrade = trim($ParAPArr[$thisSubjectID][$thisAcademicYearID][$thisYearTermID]['Grade']);
								
								$thisScoreDisplayPrefix = '';
								$thisScoreDisplaySuffix = '';
								if ($sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbol']) {
								    if (is_numeric($thisScore) && $thisPassMark > 0) {
								        if ($thisScore < $thisPassMark) {
								            $thisScoreDisplayPrefix = $sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbolPrefix'];
								            $thisScoreDisplaySuffix = $sys_custom['iPf']['DynamicReport']['DisplayFailedSubjectSymbolSuffix'];
								        }
								    }
								}
								
								$thisScoreDisplay = '';
								switch ($ParPrintMode) {
									case $ipf_cfg['slpAcademicResultPrintMode']['mark']:
										$thisScoreDisplay = ($thisScore < 0) ? '' : $thisScore;
										break;
									case $ipf_cfg['slpAcademicResultPrintMode']['grade']:
										$thisScoreDisplay = $thisGrade;
										break;
									case $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade']:
										$thisScoreDisplay = ($thisScore < 0 || $thisScore == '') ? $thisGrade : $thisScore;
										break;
									case $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark']:
										$thisScoreDisplay = ($thisGrade=='') ? $thisScore : $thisGrade;
										break;
								}
								
								if ($thisScoreDisplay != '') {
									$thisSubjectAllScoreEmpty = false;
								}
								
								
								$_displayFullMarkCol = false;
								if ($ParDisplayFullMark) {
									if ($sys_custom['iPf']['Report']['AcademicResult']['ShowOneFullMarkColumnOnly']) {
										if (!$thisDisplayedFullMark) {
											$_displayFullMarkCol = true;
										}
									}
									else {
										$_displayFullMarkCol = true;
									}
								}
								
								if ($_displayFullMarkCol) {
									//2014-0310-1425-12073
									//$thisFullMark = ($thisFullMark=='')? $ParEmptySymbol : $thisFullMark;
									if (!$thisIsComponent && in_array($thisWEBSAMSCode, (array)$sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectCodeAry'])) {
										$thisFullMark = $ParEmptySymbol;
									}
									else {
										$thisFullMark = ($thisFullMark=='')? $ParEmptySymbol : $thisFullMark;	
									}
									
									// fullmark changed to decimal
									$fullmarkArr = explode('.',$thisFullMark);
									if($fullmarkArr[1] === '0'){
										// if demical is 0 ignore it.
										$thisFullMark = (int)$thisFullMark;
									}
									
									// #W109947
									if($sys_custom['iPf']['Report']['AcademicResult']['HideFullmarkIfDroped'] && $thisScore == '' && $thisGrade == ''){
										$thisFullMark = $ParEmptySymbol;
									}
									
									$thisSubjectTr .=	"
																	<td {$fullMarkTdWidth} align=\"center\" style=\"".$thisFontSizeAttr."\">".$thisFullMark."</td>
																";
									$thisDisplayedFullMark = true;
								}
								
								//2014-0310-1425-12073
								//$thisScoreDisplay = ($thisScoreDisplay=='')? $ParEmptySymbol : $thisScoreDisplay;
								if (!$thisIsComponent && in_array($thisWEBSAMSCode, (array)$sys_custom['iPf']['Report']['AcademicResult']['HideSubjectMarkSubjectCodeAry'])) {
									$thisScoreDisplay = $ParEmptySymbol;
								}
								else {
								    $thisScoreDisplay = ($thisScoreDisplay=='')? $ParEmptySymbol : $thisScoreDisplayPrefix.$thisScoreDisplay.$thisScoreDisplaySuffix;
								}
								
								$thisSubjectTr .=	"
																	<td align=\"center\" style=\"".$thisFontSizeAttr."\">".$thisScoreDisplay."</td>
																";
							}
						}
						
						$thisSubjectTr .=	"
															</tr>
														";
						
						if (!$thisSubjectAllScoreEmpty) {
							$ContentStr .= $thisSubjectTr;
						}
					}
				}
		
				$ContentStr .=	"
													</table>
												";
			}
			else
			{
				$ContentStr = "<table width=\"100%\" border=\"1\" cellspacing=\"0\">
									<tr>
										<td height=\"100\" valign=\"middle\" align=\"center\">--</td>
									</tr>
								</table>
							  ";
			}
			
			if ($parLowerForm) {
				$tableTitle = $this->BiLangOut("['AcademicPerformanceInSchool_LowerForm']");
			}
			else {
				$tableTitle = $this->BiLangOut("['AcademicPerformanceInSchool']");
			}
			
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($tableTitle, "", $ContentStr);
			if ($sys_custom['ipf']['slp_report']['customizeHeaderSchoolCode'] == 'ucc') {
				$ReturnStr .= "EX - Excellent, GD - Good, AV - Average, S - Satisfactory, U - Unsatisfactory";
				$ReturnStr .= "<br/>";
			}
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(
							(is_array($ParSubjArr) && !empty($ParSubjArr)) &&
							(is_array($ParYearArr) && !empty($ParYearArr) && (in_array('S6',$ParYearArr) || in_array('F6',$ParYearArr))) &&
							(is_array($ParAPArr) && !empty($ParAPArr))
						)
					{
						$ReturnStr =	"
														<table width='100%' border='0' cellpadding='3' cellspacing='0' class='border_table' style='page-break-after:always'>
													";
						
						# Header row
						$ReturnStr .=	"
															<tr>
																<td class='bottom_border' style='border-bottom-style:solid;'>&nbsp;</td>
																<td class='cell_font_arial bottom_border' style='display:none;border-bottom-style:solid;'>&nbsp;</td>
													";
						foreach($ParYearArr as $Year => $WebSAMSLevel)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial bottom_border' style='border-bottom-style:solid;'><strong>".$Year."<br/>".$WebSAMSLevel."</strong></td>
															";
						$ReturnStr .=	"
															</tr>
													";
						# Table header row
						$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'><strong>".$ec_iPortfolio_Report['subject']."</strong></td>
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['full_mark']."</strong></td>
													";
						for($i=0; $i<count($ParYearArr); $i++)
							$ReturnStr .=	"
																<td align='center' class='cell_font_arial'><strong>".$ec_iPortfolio['mark_performance']."</strong></td>
														";
						$ReturnStr .=	"
																<td align='center' class='cell_font_arial' style='display:none'><strong>".$ec_iPortfolio['proj_ext_act_prog']."</strong></td>
															</tr>
													";
														
						# Main content
						foreach($ParAPArr as $subjectcode => $v)
						{
							# Display subject name in one language only
							# (may display bilingual)
							$subj_lang = $intranet_session_language == 'en' ? 0 : 1; 
							$ReturnStr .=	"
															<tr>
																<td class='cell_font_arial'>".$ParSubjArr[$subjectcode][$subj_lang]."</td>
																<td style='display:none'>&nbsp;</td>
														";
							foreach($ParYearArr as $Year => $WebSAMSLevel)
							{
								$ReturnStr .=	"
																<td align='center' class='cell_font_arial'>".($ParAPArr[$subjectcode][$Year]['Score'] == 0 ? $ParAPArr[$subjectcode][$Year]['Grade'] : $ParAPArr[$subjectcode][$Year]['Score'])."</td>
															";
							}
							$ReturnStr .=	"
																<td style='display:none'>&nbsp;</td>
															</tr>
														";
						}
				
						$ReturnStr .=	"
														</table>
													";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}

	# Get award table
	function GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP($ParAwardArr)
	{
		global $ec_iPortfolio, $i_no_record_exists_msg, $iPort;
		global $lpf_slp;
		
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParAwardArr) && !empty($ParAwardArr))
			{
				$ContentStr .=	"
													<table width='100%' border='0' cellpadding='3' cellspacing='0' class='table_pb table_print'>
														<thead><tr>
															<td style='font-size=8pt' class='award_t_boarder'><strong>".$this->BiLangOut("['SchoolYear']")."</strong></td>
															<td style='font-size=8pt'class='award_t_boarder'><strong>".$this->BiLangOut("['AwardsAndAchievements']")."</strong></td>
															<td style='font-size=8pt' class='award_t_boarder'><strong>".$this->BiLangOut("['Remarks']")."</strong></td>
														</tr></thead><tbody>
												";
	
				for($i=0; $i<count($ParAwardArr); $i++)
				{
					$ContentStr .=	"
														<tr style='page-break-inside:avoid; page-break-after:auto;'>
															<td style='font-size=8pt' valign='middle' class='ole_content'>".$ParAwardArr[$i]['Year']."</td>
															<td style='font-size=8pt' valign='middle' class='ole_content'>".$ParAwardArr[$i]['AwardName']."</td>
															<td style='font-size=8pt' valign='middle' class='ole_content'>".$ParAwardArr[$i]['Remark']."</td>
														</tr>
													";
				}			
				$ContentStr .=	"</tbody></table>";
			}
			else
	//			$ContentStr = $i_no_record_exists_msg;
				$ContentStr = "
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td height='100' valign='middle' align='center'>--</td>
													</tr>
												</table>
											";
			
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($this->BiLangOut("['ListOfAwardsAndMABySchool']"), "", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(is_array($ParAwardArr) && !empty($ParAwardArr))
					{
						$ReturnStr .=	"
															<p style='font-size:150%;font-weight:bold'>".$ec_iPortfolio['list_award']."</p>
															<table width='100%' border='0' cellpadding='3' cellspacing='0' class='padding_all border_table' style='page-break-after:always'>
																<tr>
																	<td class='cell_font_times'><strong>".$ec_iPortfolio['school_year']."</strong></td>
																	<td class='cell_font_times'><strong>".$ec_iPortfolio['award_achievement']."</strong></td>
																	<td class='cell_font_times'><strong>".$iPort["external_ole_report"]["organization"]."</strong></td>
																</tr>
														";
			
						for($i=0; $i<count($ParAwardArr); $i++)
						{
							$ReturnStr .=	"
																<tr>
																	<td class='cell_font_times'>".$ParAwardArr[$i]['Year']."</td>
																	<td class='cell_font_times'>".$ParAwardArr[$i]['AwardName']."</td>
																	<td class='cell_font_times'>".$ParAwardArr[$i]['Organization']."</td>
																</tr>
															";
						}			
						$ReturnStr .=	"</table>";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}
	

	# Get award table
	function GEN_AWARDS_AND_MAJ_ACHIEVEMENT_TABLE_SLP_PDF($ParAwardArr)
	{
		global $ec_iPortfolio, $i_no_record_exists_msg, $iPort;
		global $lpf_slp;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParAwardArr) && !empty($ParAwardArr))
			{
				$ContentStr .=	"
													<table width=\"100%\" border=\"1\" cellspacing=\"0\" >
														<tr>
															<td style=\"font-size=8pt\"><strong>".$this->BiLangOut("['SchoolYear']")."</strong></td>
															<td style=\"font-size=8pt\"><strong>".$this->BiLangOut("['AwardsAndAchievements']")."</strong></td>
															<td style=\"font-size=8pt\"><strong>".$this->BiLangOut("['Remarks']")."</strong></td>
														</tr>
												";
	
				for($i=0; $i<count($ParAwardArr); $i++)
				{
					$ContentStr .=	"
														<tr>
															<td style=\"font-size=8pt\">".$ParAwardArr[$i]['Year']."</td>
															<td style=\"font-size=8pt\">".$ParAwardArr[$i]['AwardName']."</td>
															<td style=\"font-size=8pt\">".$ParAwardArr[$i]['Remark']."</td>
														</tr>
													";
				}			
				$ContentStr .=	"</table>";
			}
			else
			{
				$ContentStr =	"<table width=\"100%\" border=\"1\" cellspacing=\"0\">
									<tr>
										<td height=\"100\" valign=\"middle\" align=\"center\">--</td>
									</tr>
									</table>
								";
			}
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($this->BiLangOut("['ListOfAwardsAndMABySchool']"), "", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
			}
		}
		
		return $ReturnStr;
	}

	# Generate activity table
	# Customarization function, but keep the format similar to other functions	
	function GEN_ACTIVITY_TABLE_SLP($ParActivityArr)
	{
		global $ec_iPortfolio;

		if(!$this->IS_CUSTOMARIZE_SLP_REPORT())
		{
		}
		else
		{
			switch($this->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(is_array($ParActivityArr) && !empty($ParActivityArr))
					{
						$ReturnStr .=	"
															<p style='font-size:150%;font-weight:bold'>".$ec_iPortfolio['eca']."</p>
															<table width='100%' border='0' cellpadding='3' cellspacing='0' class='padding_all border_table' style='page-break-after:always'>
																<tr>
																	<td class='cell_font_times'><strong>".$ec_iPortfolio['school_year']."</strong></td>
																	<td class='cell_font_times'><strong>".$ec_iPortfolio['activity_name']."</strong></td>
																	<td class='cell_font_times'><strong>".$ec_iPortfolio['role']."</strong></td>
																</tr>
														";

						$CurrentYear = "";
						for($i=0; $i<count($ParActivityArr); $i++)
						{						
							$ReturnStr .=	"
																<tr>
																	<td class='cell_font_times'>".($CurrentYear!=$ParActivityArr[$i]['Year'] ? $ParActivityArr[$i]['Year'] : "&nbsp;")."</td>
																	<td class='cell_font_times'>".$ParActivityArr[$i]['ActivityName']."</td>
																	<td class='cell_font_times'>".$ParActivityArr[$i]['Role']."</td>
																</tr>
															";
															
							if($CurrentYear != $ParActivityArr[$i]['Year'])
								$CurrentYear = $ParActivityArr[$i]['Year'];
						}			
						$ReturnStr .=	"
														</table>
														<br/><br/>
													";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}

	# Generate activity table
	# Customarization function, but keep the format similar to other functions	
	function GEN_ACTIVITY_TABLE_SLP_PDF($ParActivityArr)
	{
		global $ec_iPortfolio;
	
		if(!$this->IS_CUSTOMARIZE_SLP_REPORT())
		{
		}
		else
		{
			switch($this->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
			}
		}
		
		return $ReturnStr;
	}
	
	# Generate OLE table
	function GEN_OLE_TABLE_SLP($ParOLEArr="", $ParStudentID="", $ParYear="")
	{
		global $ec_iPortfolio, $iPort;
		global $lpf_slp;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
			$ReturnStr = $this->GET_STUDENT_LEARNING_PROFILE_OLE($ParStudentID, $ParYear);
		else
		{
			switch($this->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(is_array($ParOLEArr) && !empty($ParOLEArr))
					{
						$ELENameArr = $this->GET_ELE();

						# Title for name of activity in different OLE module
						# Not set => default is $ec_iPortfolio['activity_name']
						$TitleArr = array	(
																"[CS]" => $ec_iPortfolio['service_name']
															);
															
						$ELEIDMapArr = $this->GET_ELE_LIST();

						foreach($ParOLEArr as $ELE => $OLEArr)
						{
							$ReturnStr .=	"
															<p style='font-size:150%;font-weight:bold'>".$ec_iPortfolio['ole']."</p>
															<table width='100%' border='0' cellpadding='3' cellspacing='0' class='padding_all border_table' style='page-break-after:always'>
																<tr>
																	<td width='15%' class='cell_font_times'>".$ec_iPortfolio['school_year']."</td>
																	<td width='75%' class='cell_font_times'>".($TitleArr[$ELE]==""?$ec_iPortfolio['activity_name']:$TitleArr[$ELE])."</td>
																	<td width='10%' class='cell_font_times'>".$ec_iPortfolio['hours']."</td>
																</tr>
																<tr>
																	<td colspan='3' class='cell_font_times'><strong>".$ELENameArr[$ELE]."</strong></td>
																</tr>
														";
							foreach($OLEArr as $IntExt => $DetailArr)
							{
									$ReturnStr .=	"
																	<tr>
																		<td colspan='3' class='cell_font_times'>".($IntExt=="INT"?$iPort["internal_record"]:$iPort["external_record"])."</td>
																	</tr>
																";
								for($i=0; $i<count($DetailArr); $i++)
								{
									$ReturnStr .=	"
																	<tr>
																		<td class='cell_font_times'>".$DetailArr[$i]['Year']."</td>
																		<td class='cell_font_times'>".$DetailArr[$i]['Title']."</td>
																		<td class='cell_font_times'>".$DetailArr[$i]['Hours']."</td>
																	</tr>
																";
								}
							}
							
							$OLEComment = $this->GET_OLE_COMMENT($ParStudentID, $ELE);
							$OLECommentBy = $OLEComment[2];
							$OLEComment = $OLEComment[1];
							
							if($OLEComment != "")
							{
									$ReturnStr .=	"
																	<tr>
																		<td colspan='3' align='center' style='padding-top=50px;'>
																			<table width='90%' border='0' cellpadding='3' cellspacing='0' class='padding_all border_table'>
																				<tr>
																					<td class='cell_font_times'>".$ec_iPortfolio['ole_comment'].":</td>
																				</tr>
																				<tr>
																					<td height='100' valign='top' class='cell_font_times'>".nl2br($OLEComment)."</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td colspan='3' align='center'  style='padding-bottom=50px;'>
																			<table width='90%' border='0' cellpadding='3' cellspacing='0' class='padding_all'>
																				<tr>
																					<td width='170' nowrap class='cell_font_times'>".$ec_iPortfolio['ole_comment_teacher']."</td>
																					<td width='200' class='bottom_border cell_font_times' style='border-bottom-style:solid' nowrap>".$OLECommentBy."</td>
																					<td width='100%'>&nbsp;</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																";
							}
														
							$ReturnStr .=	"</table>\n";
						}
					}
					break;
			}
		}
		
		return $ReturnStr;
	}
		# Generate OLE table
	function GEN_OLE_TABLE_SLP_PDF($ParOLEArr="", $ParStudentID="", $ParYear="")
	{
		global $ec_iPortfolio, $iPort;
		global $lpf_slp;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			$ReturnStr = $this->GET_STUDENT_LEARNING_PROFILE_OLE_PDF($ParStudentID, $ParYear);			
		}
		else
		{

		}
		
		return $ReturnStr;
	}
	
	function GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL($StudentID="", $Year="")
	{
		global $iPort, $ec_iPortfolio;
//		global $li_pf_ey;
		
		// table heading
//		$ole_heading = 	$li_pf_ey->GEN_SLP_MODULE_TABLE($iPort["external_ole_report"]["performance"], $iPort["external_ole_report"]["table_description"]);
		$ole_heading = 	$this->GEN_SLP_MODULE_TABLE($iPort["external_ole_report"]["performance"], $iPort["external_ole_report"]["table_description"]);
		
		$ole_remark = $this->GET_PRINT_TABLE_REMARK($iPort["ole_report"]["required_remark"]);
			
		$ole_display = $this->GET_OLE_EXTERNAL_TABLE_PRINT($StudentID, $Year);

		$ole_record = $this->GET_PRINT_TABLE_RECORD($ole_heading, $ole_display, $ole_remark);
		
		return $ole_record;
	}

	function GET_STUDENT_LEARNING_PROFILE_OLE_EXTERNAL_PDF($StudentID="", $Year="")
	{
		global $iPort, $ec_iPortfolio;
//		global $li_pf_ey;
		
		// table heading
//		$ole_heading = 	$li_pf_ey->GEN_SLP_MODULE_TABLE($iPort["external_ole_report"]["performance"], $iPort["external_ole_report"]["table_description"]);
		$ole_heading = 	$this->GEN_SLP_MODULE_TABLE_PDF($iPort["external_ole_report"]["performance"], $iPort["external_ole_report"]["table_description"]);
		
		$ole_remark = $this->GET_PRINT_TABLE_REMARK_PDF($iPort["ole_report"]["required_remark"]);
			
		$ole_display = $this->GET_OLE_EXTERNAL_TABLE_PRINT_PDF($StudentID, $Year);

		$ole_record = $this->GET_PRINT_TABLE_RECORD_PDF($ole_heading, $ole_display, $ole_remark);
		
		return $ole_record;
	}

	
	# generate OLE external table for report printing
	function GET_OLE_EXTERNAL_TABLE_PRINT($StudentID="", $Year="")
	{
		global $ec_iPortfolio, $no_record_msg, $iPort, $lpf_slp;
		
		$result = $lpf_slp->GET_OLE_RECORD($StudentID, $Year, "Ext");
		
		$title_array = array();
		$title_array[] = $iPort["ole_report"]["programmes_with_description"];
		$title_array[] = $iPort["ole_report"]["school_year"];
		$title_array[] = $iPort["ole_report"]["role_of_participation"];
		$title_array[] = $iPort["external_ole_report"]["organization"];
		$title_array[] = $iPort["ole_report"]["achievements_if_any"];
		
		$x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			$x .= "<td class='ole_head_sub' bgcolor='#FFFFFF'><b>".$title_array[$i]."</b></td>";
		}
		$x .= "</tr>\n";

		if (sizeof($result)==0)
		{
			//return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF'>".$no_record_msg."</td></tr></table>\n";
			return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF'>--</td></tr></table>\n";
		}

		$DefaultELEArray = $lpf_slp->GET_ELE();
		
		$num = 0;
		for($i=0; $i<sizeof($result); $i++)
		{
			list($period, $title, $category, $role, $hours, $achievement, $details, $approved_by, $process_date, $remark, $modified, $statdate, $ele, $organization) = $result[$i];

			$ELEIDArray = explode(",", $ele);
			unset($ELEArray);
			for($m=0; $m<sizeof($ELEIDArray); $m++)
			{
				$tmp = trim($ELEIDArray[$m]);
				$ELEArray[] = $DefaultELEArray[$tmp];
			}
			$DisplayELE = (!empty($ELEArray)) ? implode("<br/>", $ELEArray) : "";
			
			$year_period = "--";
			if($period != "")
			{
				$TmpDate = explode("|", $period);
				
				if(count($TmpDate) == 1)
				$year_period = substr($TmpDate[0], 0, 4);
				else if(count($TmpDate) == 2)
				{
					$start_year = substr($TmpDate[0], 0, 4);
					$end_year = substr($TmpDate[1], 0, 4);
					
					if($start_year != $end_year)
					$year_period = $start_year."-".$end_year;
					else
					$year_period = $start_year;
				}
			}
			
			$StyleClass = "align='center' valign='middle' class='ole_content'";
			
			$x .= "<tr valign='top' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' class='ole_content'>$title</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$year_period</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$role</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$organization</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$achievement</td>\n";
			$x .= "</tr>\n";
			$x .= "<tr valign='top' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' class='ole_content'>$details</td>\n";
			$x .= "</tr>\n";
			
		}
		$x .= "</table>";

		return $x;
	} // end function get ole external table print
	
	# generate OLE external table for report printing
	function GET_OLE_EXTERNAL_TABLE_PRINT_PDF($StudentID="", $Year="")
	{
		global $ec_iPortfolio, $no_record_msg, $iPort, $lpf_slp;
		
		$result = $lpf_slp->GET_OLE_RECORD($StudentID, $Year, "Ext");
		
		$title_array = array();
		$title_array[] = $iPort["ole_report"]["programmes_with_description"];
		$title_array[] = $iPort["ole_report"]["school_year"];
		$title_array[] = $iPort["ole_report"]["role_of_participation"];
		$title_array[] = $iPort["external_ole_report"]["organization"];
		$title_array[] = $iPort["ole_report"]["achievements_if_any"];
		
		$x .= "<table width=\"100%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" >\n";
		$x .= "<thead>";
		$x .= "<tr height=\"35\">\n";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			$x .= "<td><b>".$title_array[$i]."</b></td>";
		}
		$x .= "</tr>\n";
		$x .= "</thead>";
		if (sizeof($result)==0)
		{
			return "$x<tr nobr=\"true\" height=\"25\"><td align=\"center\" height=\"100\" colspan=\"".(sizeof($title_array))."\" bgcolor=\"#FFFFFF\">--</td></tr></table>\n";
		}

		$DefaultELEArray = $lpf_slp->GET_ELE();
		
		$num = 0;
		for($i=0; $i<sizeof($result); $i++)
		{
			list($period, $title, $category, $role, $hours, $achievement, $details, $approved_by, $process_date, $remark, $modified, $statdate, $ele, $organization) = $result[$i];

			$ELEIDArray = explode(",", $ele);
			unset($ELEArray);
			for($m=0; $m<sizeof($ELEIDArray); $m++)
			{
				$tmp = trim($ELEIDArray[$m]);
				$ELEArray[] = $DefaultELEArray[$tmp];
			}
			$DisplayELE = (!empty($ELEArray)) ? implode("<br/>", $ELEArray) : "";
			
			$year_period = "--";
			if($period != "")
			{
				$TmpDate = explode("|", $period);
				
				if(count($TmpDate) == 1)
				$year_period = substr($TmpDate[0], 0, 4);
				else if(count($TmpDate) == 2)
				{
					$start_year = substr($TmpDate[0], 0, 4);
					$end_year = substr($TmpDate[1], 0, 4);
					
					if($start_year != $end_year)
					$year_period = $start_year."-".$end_year;
					else
					$year_period = $start_year;
				}
			}
			
			$x .= "<tr valign=\"top\" height=\"25\">\n";
			$x .= "<td >$title</td>\n";
			$x .= "<td rowspan=\"2\" >$year_period</td>\n";
			$x .= "<td rowspan=\"2\" >$role</td>\n";
			$x .= "<td rowspan=\"2\" >$organization</td>\n";
			$x .= "<td rowspan=\"2\" >$achievement</td>\n";
			$x .= "</tr>\n";
			$x .= "<tr valign=\"top\" height=25>\n";
			$x .= "<td>$details</td>\n";
			$x .= "</tr>\n";
			
		}
		$x .= "</table>";

		return $x;
	} // end function get ole external table print
	
	function GET_STUDENT_LEARNING_PROFILE_OLE($StudentID="", $Year="")
	{
		global $iPort, $ec_iPortfolio;
//		global $li_pf_ey;
		
			// table heading
			//$ole_heading = $this->GET_PRINT_TABLE_HEADING($iPort["ole_report"]["other_learning_experiences"], $iPort["ole_report"]["table_description"]);
//			$ole_heading = 	$li_pf_ey->GEN_SLP_MODULE_TABLE($iPort["ole_report"]["other_learning_experiences"], $iPort["ole_report"]["table_description"]);
			$ole_heading = 	$this->GEN_SLP_MODULE_TABLE($iPort["ole_report"]["other_learning_experiences"], $iPort["ole_report"]["table_description"]);
			
			$ole_remark = $this->GET_PRINT_TABLE_REMARK($iPort["ole_report"]["required_remark"]);
			
			$ole_display = $this->GET_OLE_TABLE_PRINT($StudentID, $Year);

			$ole_record = $this->GET_PRINT_TABLE_RECORD($ole_heading, $ole_display, $ole_remark);
		
		return $ole_record;
	} // end function GET_STUDENT_LEARNING_PROFILE_OLE

	function GET_STUDENT_LEARNING_PROFILE_OLE_PDF($StudentID="", $Year="")
	{
		global $iPort, $ec_iPortfolio;
//		global $li_pf_ey;
		
			// table heading
			//$ole_heading = $this->GET_PRINT_TABLE_HEADING($iPort["ole_report"]["other_learning_experiences"], $iPort["ole_report"]["table_description"]);
//			$ole_heading = 	$li_pf_ey->GEN_SLP_MODULE_TABLE($iPort["ole_report"]["other_learning_experiences"], $iPort["ole_report"]["table_description"]);
			$ole_heading = 	$this->GEN_SLP_MODULE_TABLE_PDF($iPort["ole_report"]["other_learning_experiences"], $iPort["ole_report"]["table_description"]);
			
			$ole_remark = $this->GET_PRINT_TABLE_REMARK_PDF($iPort["ole_report"]["required_remark"]);
			
			
			$ole_display = $this->GET_OLE_TABLE_PRINT_PDF($StudentID, $Year);

			$ole_record = $this->GET_PRINT_TABLE_RECORD_PDF($ole_heading, $ole_display, $ole_remark);
		
		return $ole_record;
	} // end function GET_STUDENT_LEARNING_PROFILE_OLE

	# generate OLE table for report printing
	function GET_OLE_TABLE_PRINT($StudentID="", $Year="")
	{
		global $ec_iPortfolio, $no_record_msg, $iPort, $lpf_slp;
		
		$result = $lpf_slp->GET_OLE_RECORD($StudentID, $Year, "Int");
		
		#####################################################################
		# Eric Yip (20090205): Check the number of records can be print
		#####################################################################
		$StudentObj = $this->GET_STUDENT_DATA($StudentID);
		$StudentLevel = $this->getClassLevel($StudentObj[0]['ClassName']);
		$OLEAssignedArr = $lpf_slp->GET_OLE_RECORD_ASSIGNED($StudentID);
		$OLESettings = $lpf_slp->GET_OLE_SETTINGS_SLP();
		if(is_array($OLESettings))
		{
			$IsPrintAll = false;
			
			if(!empty($OLEAssignedArr))
			{
				for($i=0; $i<count($result); $i++)
				{
					if(in_array($result[$i]['RecordID'], $OLEAssignedArr))
					{
						$temp_result[$result[$i]['SLPOrder']-1] = $result[$i];
					}
				}
				ksort($temp_result);
				$result = array_values($temp_result);
				if($OLESettings[$StudentLevel[1]][0] != "")
					$NumRecordAllowed = MIN(count($result), $OLESettings[$StudentLevel[1]][0]);
				else
					$NumRecordAllowed = count($result);
			}
			else if($OLESettings[$StudentLevel[1]][0] != "")
				$NumRecordAllowed = $OLESettings[$StudentLevel[1]][0];
			else
				$IsPrintAll = true;
		}
		else
			$IsPrintAll = true;
		#####################################################################
		
		//$title_array = array($ec_iPortfolio['date']."/".$ec_iPortfolio['period'], $ec_iPortfolio['title'], $ec_iPortfolio['category'], $ec_iPortfolio['ele'], $ec_iPortfolio['ole_role'], $ec_iPortfolio['hours'], $ec_iPortfolio['achievement'], $ec_iPortfolio['details'], $ec_iPortfolio['school_remark']);
		$title_array = array();
		$title_array[] = $iPort["ole_report"]["programmes_with_description"];
		$title_array[] = $iPort["ole_report"]["school_year"];
		$title_array[] = $iPort["ole_report"]["role_of_participation"];
		$title_array[] = $iPort["ole_report"]["partner_organizations_if_any"];
		$title_array[] = $iPort["ole_report"]["essential_learning_experiences"];
		$title_array[] = $iPort["ole_report"]["achievements_if_any"];
		
		$x .= "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print'>\n";
		$x .= "<tr height='35'>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			$x .= "<td class='ole_head_sub' bgcolor='#FFFFFF'><b>".$title_array[$i]."</b></td>";
		}
		$x .= "</tr>\n";

		if (sizeof($result)==0)
		{
			//return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF'>".$no_record_msg."</td></tr></table>\n";
			return "$x<tr height=25><td align=center height='100' colspan='".(sizeof($title_array))."' bgcolor='#FFFFFF'>--</td></tr></table>\n";
		}

		$DefaultELEArray = $lpf_slp->GET_ELE(true);
		
		$num = 0;
		for($i=0; $i<sizeof($result); $i++)
		{
			if(!$IsPrintAll && $i+1 > $NumRecordAllowed) break;
		

			/* 200911121040MaxWong support */
			list($period, $title, $category, $role, $hours, $achievement, $details, $approved_by, $process_date, $remark, $modified, $statdate, $ele, $organization, $SLPOrder) = $result[$i];
//			list($period, $title, $titleChi, $category, $role, $hours, $achievement, $details, $detailsChi, $approved_by, $process_date, $remark, $modified, $statdate, $ele, $organization, $SLPOrder) = $result[$i];  //FOR BILINGUAL , COMMENT OUT FIRST , FAI 20091130 

			/* 200911121040MaxWong */
			
			$ELEIDArray = explode(",", $ele);
			unset($ELEArray);
			for($m=0; $m<sizeof($ELEIDArray); $m++)
			{
				$tmp = trim($ELEIDArray[$m]);
				$ELEArray[] = $DefaultELEArray[$tmp];
			}
			$DisplayELE = (!empty($ELEArray)) ? implode("<br/>", $ELEArray) : "";
			
			$year_period = "--";
			if($period != "")
			{
				$TmpDate = explode("|", $period);
				
				if(count($TmpDate) == 1)
				$year_period = substr($TmpDate[0], 0, 4);
				else if(count($TmpDate) == 2)
				{
					$start_year = substr($TmpDate[0], 0, 4);
					$end_year = substr($TmpDate[1], 0, 4);
					
					if($start_year != $end_year)
					$year_period = $start_year."-".$end_year;
					else
					$year_period = $start_year;
				}
			}
			
			$StyleClass = "align='center' valign='middle' class='ole_content'";

			/* 200911121040MaxWong */
			/*
			//for bilingual , comment out first , fai 20091130
			global $langOrder, $printingLanguage;
			if (isset($printingLanguage) && $printingLanguage == "3") {
				$this->formatCaption($title, $this->orderStr($titleChi,$title,$langOrder), 0);
				$this->formatCaption($details, $this->orderStr($detailsChi,$details,$langOrder), 0);
			}
			*/
			/* 200911121040MaxWong */
			$x .= "<tr valign='top' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' class='ole_content'>$title</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$year_period</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$role</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$organization</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$DisplayELE</td>\n";
			$x .= "<td bgcolor='#FFFFFF' rowspan='2' $StyleClass>$achievement</td>\n";
			$x .= "</tr>\n";
			$x .= "<tr valign='top' height=25>\n";
			$x .= "<td bgcolor='#FFFFFF' class='ole_content'>$details</td>\n";
			$x .= "</tr>\n";
		}
		$x .= "</table>";

		return $x;
	} // end function get ole table print
	
		# generate OLE table for report printing
	function GET_OLE_TABLE_PRINT_PDF($StudentID="", $Year="")
	{
		global $ec_iPortfolio, $no_record_msg, $iPort, $lpf_slp;
		
		$result = $lpf_slp->GET_OLE_RECORD($StudentID, $Year, "Int");
		
		#####################################################################
		# Eric Yip (20090205): Check the number of records can be print
		#####################################################################
		$StudentObj = $this->GET_STUDENT_DATA($StudentID);
		$StudentLevel = $this->getClassLevel($StudentObj[0]['ClassName']);
		$OLEAssignedArr = $lpf_slp->GET_OLE_RECORD_ASSIGNED($StudentID);
		$OLESettings = $lpf_slp->GET_OLE_SETTINGS_SLP();
		if(is_array($OLESettings))
		{
			$IsPrintAll = false;
			
			if(!empty($OLEAssignedArr))
			{
				for($i=0; $i<count($result); $i++)
				{
					if(in_array($result[$i]['RecordID'], $OLEAssignedArr))
					{
						$temp_result[$result[$i]['SLPOrder']-1] = $result[$i];
					}
				}
				ksort($temp_result);
				$result = array_values($temp_result);
				if($OLESettings[$StudentLevel[1]][0] != "")
					$NumRecordAllowed = MIN(count($result), $OLESettings[$StudentLevel[1]][0]);
				else
					$NumRecordAllowed = count($result);
			}
			else if($OLESettings[$StudentLevel[1]][0] != "")
				$NumRecordAllowed = $OLESettings[$StudentLevel[1]][0];
			else
				$IsPrintAll = true;
		}
		else
			$IsPrintAll = true;
		#####################################################################
		
		//$title_array = array($ec_iPortfolio['date']."/".$ec_iPortfolio['period'], $ec_iPortfolio['title'], $ec_iPortfolio['category'], $ec_iPortfolio['ele'], $ec_iPortfolio['ole_role'], $ec_iPortfolio['hours'], $ec_iPortfolio['achievement'], $ec_iPortfolio['details'], $ec_iPortfolio['school_remark']);
		$title_array = array();
		$title_array[] = $iPort["ole_report"]["programmes_with_description"];
		$title_array[] = $iPort["ole_report"]["school_year"];
		$title_array[] = $iPort["ole_report"]["role_of_participation"];
		$title_array[] = $iPort["ole_report"]["partner_organizations_if_any"];
		$title_array[] = $iPort["ole_report"]["essential_learning_experiences"];
		$title_array[] = $iPort["ole_report"]["achievements_if_any"];
		
		$x .= "<table width=\"100%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$x .= "<thead nobr=\"true\" ><tr nobr=\"true\">";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			$x .= "<td><b>1100 ".$title_array[$i]."</b></td>";
		}
		$x .= "</tr></thead>";

		if (sizeof($result)==0)
		{
			return "$x<tr nobr=\"true\" height=\"25\"><td align=\"center\" height=\"100\" colspan=\"".(sizeof($title_array))."\">--</td></tr></table>\n";
		}

		$DefaultELEArray = $lpf_slp->GET_ELE(true);
		
		$num = 0;
		for($i=0; $i<sizeof($result); $i++)
		{
			if(!$IsPrintAll && $i+1 > $NumRecordAllowed) break;
		

			/* 200911121040MaxWong support */
			list($period, $title, $category, $role, $hours, $achievement, $details, $approved_by, $process_date, $remark, $modified, $statdate, $ele, $organization, $SLPOrder) = $result[$i];
//			list($period, $title, $titleChi, $category, $role, $hours, $achievement, $details, $detailsChi, $approved_by, $process_date, $remark, $modified, $statdate, $ele, $organization, $SLPOrder) = $result[$i];  //FOR BILINGUAL , COMMENT OUT FIRST , FAI 20091130 

			/* 200911121040MaxWong */
			
			$ELEIDArray = explode(",", $ele);
			unset($ELEArray);
			for($m=0; $m<sizeof($ELEIDArray); $m++)
			{
				$tmp = trim($ELEIDArray[$m]);
				$ELEArray[] = $DefaultELEArray[$tmp];
			}
			$DisplayELE = (!empty($ELEArray)) ? implode("<br/>", $ELEArray) : "";
			
			$year_period = "--";
			if($period != "")
			{
				$TmpDate = explode("|", $period);
				
				if(count($TmpDate) == 1)
				$year_period = substr($TmpDate[0], 0, 4);
				else if(count($TmpDate) == 2)
				{
					$start_year = substr($TmpDate[0], 0, 4);
					$end_year = substr($TmpDate[1], 0, 4);
					
					if($start_year != $end_year)
					$year_period = $start_year."-".$end_year;
					else
					$year_period = $start_year;
				}
			}
			
			$StyleClass = "align='center' valign='middle' ";

			/* 200911121040MaxWong */
			/*
			//for bilingual , comment out first , fai 20091130
			global $langOrder, $printingLanguage;
			if (isset($printingLanguage) && $printingLanguage == "3") {
				$this->formatCaption($title, $this->orderStr($titleChi,$title,$langOrder), 0);
				$this->formatCaption($details, $this->orderStr($detailsChi,$details,$langOrder), 0);
			}
			*/
			/* 200911121040MaxWong */
			$x .= "<tr valign=\"top\" height=\"25\">\n";
			$x .= "<td  >$title</td>\n";
			$x .= "<td rowspan=\"2\">$year_period</td>\n";
			$x .= "<td rowspan=\"2\">$role</td>\n";
			$x .= "<td rowspan=\"2\">$organization</td>\n";
			$x .= "<td rowspan=\"2\">$DisplayELE</td>\n";
			$x .= "<td rowspan=\"2\">$achievement</td>\n";
			$x .= "</tr>\n";
			$x .= "<tr valign=\"top\" height=\"25\">\n";
			$x .= "<td >$details</td>\n";
			$x .= "</tr>\n";
		}
		$x .= "</table>";

		return $x;
	} // end function get ole table print

	/* 200911121040MaxWong */
	function isOdd($num) {
		return fmod($num, 2);
	}
	/**
	 * @para &$Caption The caption to be formated
	 * @para $CaptionArr The caption elements
	 * @para $type 0=str1<br/>str2...1=str1:<br/>str2:..., 2=str1 str2..., 3=str1(str2)...
	 * return A formatted caption
	 */
	function formatCaption(&$Caption, $CaptionArr, $type=0) {
		$tempOuputStr = "";
		if (is_array($CaptionArr)) {
			foreach($CaptionArr as $key => $value) {
				switch($type)
				{
					case 0:
						if ($this->isOdd($key)) {
							$tempOuputStr .= $value;
						} else {
							$tempOuputStr .= $value."<br/>";
						}
						break;
					case 1:
						if ($this->isOdd($key)) {
							$tempOuputStr .= $value.":";
						} else {
							$tempOuputStr .= $value.":<br/>";
						}
						break;
					case 2:
						if ($this->isOdd($key)) {
							$tempOuputStr .= $value;
						} else {
							$tempOuputStr .= $value."&nbsp;";
						}
						break;
					case 3:
						if ($this->isOdd($key)) {
							$tempOuputStr .= "(".$value.")";
						} else {
							$tempOuputStr .= $value;
						}
						break;
					default:
						$tempOuputStr .= $value;
						break;
				}
			}
		}
		$Caption = $tempOuputStr;
		return $Caption;
	}
	/**
	 * Order the strings
	 * @para $str1 The 1st string to ordered
	 * @para $str2 The 2nd string to be ordered
	 * @para $order 1=[str1][str2], 2=[str2][str1]
	 * return Array The ordered strings
	 */
	function orderStr($str1 = "", $str2 = "", $order = 1) {
		$outputOrder = array();
		switch($order)
		{
			case 1:
				$outputOrder[0] = $str1;
				$outputOrder[1] = $str2;
				break;
			case 2:
				$outputOrder[0] = $str2;
				$outputOrder[1] = $str1;		
				break;
			default:
				$outputOrder[0] = $str1;
				$outputOrder[1] = $str2;
				break;
		}
		return $outputOrder;
	}
	/* 200911121040MaxWong */
	
	
	# Generate self account table
	function GEN_SELF_ACCOUNT_TABLE_SLP($ParSAArr)
	{
		global $ec_iPortfolio, $i_no_record_exists_msg;
		global $lpf_slp;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParSAArr) && !empty($ParSAArr) && trim($ParSAArr['Details']) != "")
			{
				$lpfObj = new libportfolio();
				$ParSAArr['Details'] = $lpfObj->displaySelfAccountForHTML($ParSAArr['Details']);
				
				$ContentStr .=	"
													<table width='100%' border='0' cellpadding='3' cellspacing='0' class='padding_all'>
														<tr>
															<td>".$ParSAArr['Details']."</td>
														</tr>
													";	
				$ContentStr .=	"</table>";
			}
			else
	//			$ContentStr = $i_no_record_exists_msg;
				$ContentStr = "
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td height='100' valign='middle' align='center'>--</td>
													</tr>
												</table>
											";
			
			$desc = $this->BiLangOut("['StudentSelfAccount_Optional']");
	//		$desc .= "<br/><br/><span style='color:#FFFFFF;padding:3px;font-size:10pt;font-weight:bold;font-family:Arial'>".str_replace(array("<!--EngLimit-->", "<!--ChiLimit-->"), array(1000, 1600), $ec_iPortfolio['within_word_limit'])."</span>";
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE($desc, "<span style='font-size:8pt;'>".$this->BiLangOut("['SelfAccount_TableDescription']")."</span>", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					if(is_array($ParSAArr) && !empty($ParSAArr) && trim($ParSAArr['Details']) != "")
					{
						$ReturnStr .=	"
														<p style='font-size:150%;font-weight:bold'>".$ec_iPortfolio['student_self_account']."</p>
														<table width='100%' border='0' cellpadding='3' cellspacing='0' class='padding_all border_table'>
															<tr>
																<td class='cell_font_times'>".$ParSAArr['Details']."</td>
															</tr>
														</table>
													";
					}
					else
					{
						$ReturnStr .=	"
														<p style='font-size:150%;font-weight:bold'>".$ec_iPortfolio['student_self_account']."</p>
														<table width='100%' border='0' cellpadding='3' cellspacing='0' class='padding_all border_table'>
															<tr>
																<td height='100'>&nbsp;</td>
															</tr>
														</table>
													";
					}
					break;
			}
		}
		
		return $ReturnStr;
	}

	# Generate self account table
	function GEN_SELF_ACCOUNT_TABLE_SLP_PDF($ParSAArr)
	{
		global $ec_iPortfolio, $i_no_record_exists_msg;
		global $lpf_slp;
		
		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if(is_array($ParSAArr) && !empty($ParSAArr) && trim($ParSAArr['Details']) != "")
			{
				$ParSAArr['Details'] = strip_tags($ParSAArr['Details'], "<p><u><br><span>");
				$ParSAArr['Details'] = preg_replace('/<p.*?>/', '<p>', $ParSAArr['Details']);
				$ParSAArr['Details'] = str_replace('&nbsp;', "<span style=\"font-size:100%\">&nbsp;</span>", $ParSAArr['Details']);
				$ParSAArr['Details'] = str_replace(array('（','）'),array('(',')'),$ParSAArr['Details']);
				$ParSAArr['Details'] = str_replace('　',' ',$ParSAArr['Details']);
				$ParSAArr['Details'] = str_replace('    ','  ',$ParSAArr['Details']); 
				
				$lpfObj = new libportfolio();
				$ParSAArr['Details'] = $lpfObj->spaceIndentationForPDF($ParSAArr['Details']);
				
				$ContentStr .=	"<table width=\"100%\" border=\"1\" cellspacing=\"0\" >
														<tr>
															<td>".$ParSAArr['Details']."</td>
														</tr>
													";
				$ContentStr .=	"</table>";
			}
			else
			{
				$ContentStr = "<table width=\"100%\" border=\"1\" cellspacing=\"0\">
									<tr>
										<td height=\"100\" valign=\"middle\" align=\"center\">--</td>
									</tr>
								</table>
								";
			}
			$desc = $this->BiLangOut("['StudentSelfAccount_Optional']");
			$ReturnStr = $this->GEN_SLP_MODULE_TABLE_PDF($desc, "<span style=\"font-size:8pt;\">".$this->BiLangOut("['SelfAccount_TableDescription']")."</span>", $ContentStr);
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
	
			}
		}
		
		return $ReturnStr;
	}
	
	function GET_PRINT_TABLE_REMARK($Remark="")
	{
		$x = "";
		$x .= "<p class='ole_content'>*".$Remark."</p>";
		
		return $x;
	}
	
	function GET_PRINT_TABLE_REMARK_PDF($Remark="")
	{
		$x = "";
		$x .= "<p>*".$Remark."</p>";
		
		return $x;
	}

	function GET_PRINT_TABLE_RECORD($Heading="", $Record="", $Remark="")
	{
		$x = "<table cellSpacing=0 cellPadding=0 width='100%' border=0 align='center'>
		<tr><td>
		<table width='100%' border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
		<tr>
		<td height='30'>".$Heading."</td>
		</tr>
		<tr height='10'>
		<td></td>
		</tr>
		</table>
		<table width='100%' border='1' bordercolor='black' cellspacing='0' cellpadding='0' bgcolor='#000000'>
		<tr><td>".$Record."</td></tr>
		</table>
		<table border='0' bordercolor='black' cellspacing='0' cellpadding='1'>
		<tr><td>".$Remark."</td></tr>
		</table>
		</td>
		</tr>
		</table><br/><br/>";
		
		return $x;
	}

	function GET_PRINT_TABLE_RECORD_PDF($Heading="", $Record="", $Remark="") {
		$x = $Heading.$Record.$Remark;
		return $x;
	}


	
	# Generate report footer
	function GEN_REPORT_FOOTER_SLP_PDF()
	{
		global $ec_iPortfolio, $sys_custom,$iPort,$Lang;
		global $lpf_slp;
		if($sys_custom['ipf']['slp_report']['ucc']){
			$returnStr = '<br/><br/><br/>
												<table width="95%" border="0" bordercolor="black" cellspacing="0" cellpadding="0">
                    								<tr>
                    								  <td width="45%" height="30" align="right" nowrap="nowrap">'.$iPort['usertype_s'].' '.$Lang['iPortfolio']['Cust_ucc']['SLP_Report_Signature'].'</td>
                    									<td width="20%" height="30" align="right" >&nbsp;________________________</td>
                    									<td width="5%">&nbsp;</td>
                    									<td width="10%" height="30" align="right" nowrap="nowrap">'.$ec_iPortfolio['date'].':</td>
                    									<td width="20%" height="30" align="right" >&nbsp;________________________</td>
                    								</tr>
                    							</table>
						';
		}else{
			$returnStr = '<br/><br/><table><tr><td align="center"><strong>'.$this->BiLangOut("['End']").'</strong></td></tr></table>';
		}
		return $returnStr;
	}
	function GEN_REPORT_FOOTER_SLP()
	{
		global $ec_iPortfolio, $sys_custom,$iPort,$Lang;
		global $lpf_slp;

		if(!$lpf_slp->IS_CUSTOMARIZE_SLP_REPORT())
		{
			if($sys_custom['iPortfolio_SLP_Report_minor']['chc']){
				$ReturnStr =	"
													<tr>
														<td align='right'>
															<table width='200'>
																<tr><td height='100'></td></tr>
																<tr><td><hr /></td></tr>
																<tr><td align='center'>".$ec_iPortfolio['school_chop']."</td></tr>
															</table>
														</td>
													</tr>
												</table>
											";
			}elseif($sys_custom['ipf']['slp_report']['ucc']){
				$ReturnStr =	"<tr>	
									<td align='right'>
										<table cellSpacing=0 cellPadding=5 width='60%' border=0 align='right'>
                							<tr><td>
                  								<table width='100%' border='0' bordercolor='black' cellspacing='0' cellpadding='0'>
                    								<tr>
                    								  <td width='25%' height='30' class='head_sub_2' align='left' nowrap='nowrap'>{$iPort['usertype_s']} {$Lang['iPortfolio']['Cust_ucc']['SLP_Report_Signature']}:</td>
                    									<td width='35%' height='30' class='head_sub_2' align='left' style='border-bottom: 1px solid black'>&nbsp;</td>
                    									<td width='5%'>&nbsp;</td>
                    									<td width='10%' height='30' class='head_sub_2' align='left' nowrap='nowrap'>{$ec_iPortfolio['date']}:</td>
                    									<td width='25%' height='30' class='head_sub_2' align='left' style='border-bottom: 1px solid black'>&nbsp;</td>
                    								</tr>
                    							</table>
                  							</td></tr>
                  						</table>
									</td>
								 </tr>
							 </table>
							";
			}
			else
			{
	  			$ReturnStr =	"<tr><td align='center'><strong>".$this->BiLangOut("['End']")."</strong></td></tr>
									</table>";
			}
		}
		else
		{
			switch($lpf_slp->GET_CUSTOMARIZE_SCHOOL_SLP())
			{
				case "LaSalle":
					$ReturnStr =	"
														<tr>
															<td align='right' height='150'>
																<table width='300' height='100%' border='0' cellpadding='5' cellspacing='0'>
																	<tr>
																		<td height='100%' valign='absbottom' class='bottom_border' style='border-bottom-style:solid;' nowrap>&nbsp;</td>
																	</tr>
																	<tr>
																		<td class='cell_font_times' style='padding-left:30px'>".ucfirst(strtolower($ec_iPortfolio['title_principal_eng']))."</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												";
					break;
			}
		}
									
		return $ReturnStr;
	}

function broken_tags($str)
{
//List of self closing tags
$exclude_close = array("<br/>","</br>", "</img>", "</hr>" , "</area>" , "</base>", "</basefont>", "</input>", "</link>", "</meta>");

preg_match_all("/(<\w+)(?:.){0,}?>/", $str, $v1);
preg_match_all("/<\/\w+>/", $str, $v2);


$open = array_map('strtolower', $v1[1]);
$closed = array_map('strtolower', $v2[0]);

/* debug_r($open);
debug_r($closed);*/

## Check Opening Tag has Corresponding Closing Tag
foreach ($open as $tag)
{
$end_tag = preg_replace("/<(.*)/", "</$1>", $tag);

// echo "[". $end_tag."] \n";
if (!in_array($end_tag, $closed) && !in_array($end_tag, $exclude_close) )
return true;

if(!in_array($end_tag, $exclude_close))
unset($closed[array_search($end_tag, $closed)]);
}


## Broken closing tags remain
if(is_array($closed) && $closed != array()){
return true;
}

return false;
}

	function Get_Student_Year_Class_Mapping($AcademicYearArr='', $StudentIDArr='')
	{
		if($AcademicYearArr!='')
			$cond_AcademicYearID = " AND yc.AcademicYearID IN (".implode(",",(array)$AcademicYearArr).") ";
		if($StudentIDArr!='')
			$cond_StudentIDArr = " AND ycu.UserID IN (".implode(",",(array)$StudentIDArr).") ";
		
		$sql = "
			SELECT 
				* 
			FROM 
				YEAR_CLASS yc
				INNER JOIN YEAR_CLASS_USER ycu ON yc.YearClassID = ycu.YearClassID
			WHERE
				1
				$cond_AcademicYearID
			$cond_StudentIDArr
		";
		
		$result = $this->returnArray($sql);
		return $result;
	}

	function customizeHeader_layout_1($schoolCustomizeCode,$pageBreakAfterString,$ParIssueDate,$ParPrintIssueDate,$ParYear,$printFormat){
		global $Lang;
		$schoolLogo = $schoolCustomizeCode.'_logo.jpg';
		$schoolName = $Lang['iPortfolio']['Cust_'.$schoolCustomizeCode]['SLP_Report_SchoolName'];
		$reportName = $Lang['iPortfolio']['Cust_'.$schoolCustomizeCode]['SLP_Report_ReportName'];



		$ReturnStr = '<table width="100%" cellpadding="5" cellspacing="0" border="0" align="center" style="'.$pageBreakAfterString.'"><tr><td>
								<table width="100%" cellpadding="1" cellspacing="1" border="0" align="center">
									<tr>
										<td rowspan="2" align="right" width="300" width="25%"><img src="/file/portfolio/'.$schoolLogo.'" height="100" width="121"/></td>
										<td align = "center" class="report_header" width="50%">'.$schoolName.'<br/><br/></td>
										<td rowspan="2" align="right" width="300" width="25%">&nbsp;</td>
									</tr>
									<tr>
										<td align = "center" class="report_header">'.$ParYear.'&nbsp;&nbsp;'.$reportName.'</td>
									</tr>										
								 </table>
							 </td></tr>';								 
		if($ParPrintIssueDate){
			$ReturnStr .= "<tr><td align='right'>".$this->BiLangOut('["date_issue"]',"","","H").": ".$ParIssueDate."</td></tr>";
		}
		$ReturnStr .= "</table>";
		
		if(strtolower($printFormat) == 'pdf'){
			$ReturnStr = '';
			//2012-0313-1101-36071
//			$ReturnStr = '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
//							<tr><td>
//								<table width="100%" cellpadding="1" cellspacing="1" border="0" >
//									<tr>
//									<td>
//										<table width = "200" cellpadding="0" cellspacing="0" border="0" >
//										<tr><td align ="right"><img src="/file/portfolio/'.$schoolLogo.'" height="100" />&nbsp;</td></tr></table>
//									</td>
//									<td>
//										<table width="100%" cellpadding="0" cellspacing="0" border="0" ><tr><td><b>
//										'.$schoolName.'<br/><br/>
//										'.$ParYear.'&nbsp;&nbsp;'.$reportName.'</b>
//										</td></tr></table>
//									</td>
//									<td>
//										<table width = "200" cellpadding="0" cellspacing="0" border="0" >
//										<tr><td>&nbsp;</td></tr></table>
//									</td>
//									</tr>
//								</table>
//							</td></tr>';
			  $ReturnStr = '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
							<tr><td>
								<table width="100%" cellpadding="1" cellspacing="1" border="0" >
									<tr>
									<td>
										<table width = "200" cellpadding="0" cellspacing="0" border="0" >
										<tr><td align ="right"><img src="/file/portfolio/'.$schoolLogo.'" height="100" /></td></tr></table>
									</td>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" ><tr><td><b>
										'.$schoolName.'<br/><br/>
										'.$ParYear.' '.$reportName.'</b>
										</td></tr></table>
									</td>
									<td>
										<table width = "200" cellpadding="0" cellspacing="0" border="0" >
										<tr><td></td></tr></table>
									</td>
									</tr>
								</table>
							</td></tr>';						 
			if($ParPrintIssueDate){
				$ReturnStr .= "<tr><td align=\"right\">".$this->BiLangOut('["date_issue"]',"","","H").": ".$ParIssueDate."</td></tr>";
			}
			$ReturnStr .= "</table>";
		}
		return $ReturnStr;
	}

	function getHTMLVersionHeaderImgDisplayHeight(){
		global $sys_custom;
		return $sys_custom['iPf']['Report']['SLP']['imgHTMLHeaderHeight'];
	}
	function getHTMLVersionHeaderImgDisplayWidth(){
		global $sys_custom;
		return $sys_custom['iPf']['Report']['SLP']['imgHTMLHeaderWidth'];		
	}


}

?>