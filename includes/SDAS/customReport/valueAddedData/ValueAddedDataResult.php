<?php
/**
 * Change Log:
 * 2017-08-25 Pun
 *  - File Create
 */

if(!class_exists('ValueAddedDataResult')){
class ValueAddedDataResult {
    
    public $resultId;
    public $className;
    public $subject;
    public $subjectGroups;
    
    public $studentIds;
    public $teacherIds;
    public $result;
    
	public function __construct(){
	    $this->resultId = uniqid('');
	    $this->subjectGroups = array();
	    $this->studentIds = array();
	    $this->teacherIds = array();
	}
    
    

	public function addSubjectGroups($subjectGroup){
	    if(is_array($subjectGroup)){
	        foreach($subjectGroup as $group){
	            $this->addSubjectGroups($group);
	        }
	        return;
	    }
	    $this->subjectGroups[$subjectGroup->subjectGroupId] = $subjectGroup;
	    $this->initStudentIds();
	    $this->initTeacherIds();
	}
    
    
    private function initStudentIds(){
        foreach($this->subjectGroups as $subjectGroup){
            $this->studentIds = array_merge($this->studentIds, $subjectGroup->studentIds);
        }
    }
    
    private function initTeacherIds(){
        foreach($this->subjectGroups as $subjectGroup){
            $this->teacherIds = array_merge($this->teacherIds, $subjectGroup->teacherIds);
        }
    }
    
    
    

    public function __toString(){
        $className = $this->className;
        $subjectId = $this->subject->subjectId;
        $subjectName = Get_Lang_Selection($this->subject->subjectTitleB5,$this->subject->subjectTitleEN);
        
        $subjectGroupIds = array();
        foreach($this->subjectGroups as $subjectGroup){
            $subjectGroupIds[] = $subjectGroup->subjectGroupId;
        }
        $subjectGroupIdStr = implode(',', $subjectGroupIds);
        
        return "{$className}: [{$subjectId}]{$subjectName} ({$subjectGroupIdStr})";
    }
    
} // End Class
}
