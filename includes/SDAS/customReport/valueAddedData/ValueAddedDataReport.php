<?php 
/**
 * Change Log:
 * 2018-01-23 Pun
 *  - Modified calculateCorrelation(), fixed cannot use in PHP5.4
 * 2017-08-25 Pun
 *  - File Create
 */
include_once(dirname(__FILE__).'/ValueAddedDataObjectPool.php');
include_once(dirname(__FILE__).'/ValueAddedDataSubject.php');
include_once(dirname(__FILE__).'/ValueAddedDataSubjectGroup.php');
include_once(dirname(__FILE__).'/ValueAddedDataClass.php');
include_once(dirname(__FILE__).'/ValueAddedDataUser.php');
include_once(dirname(__FILE__).'/ValueAddedDataResult.php');
class ValueAddedDataReport{
	
    const VALUE_ADDED_TYPE_SUBJECT_GROUP = '0';
    const VALUE_ADDED_TYPE_TEACHER = '1';
    
	private $db;
	private $objectPool;
	
	private $academicYearId;
	private $yearTermId;
	private $vsYearTermId;
	private $termAssessment;
	private $vsTermAssessment;
	
	public function __construct($db){
		$this->db = $db;
		$this->objectPool = new ValueAddedDataObjectPool();
	}
	
	private function getPortfolioDatabase(){
	    global $eclass_db;
	    return $eclass_db;
	}
	
	
	public function getReportData($reportType, $academicYearId, $yearTerm, $vsYearTerm, $filterArr){
	    #### Init object data START ####
	    $this->academicYearId = $academicYearId;
	    
	    $this->yearTermId = $this->getYearTermId($yearTerm);
	    $this->vsYearTermId = $this->getYearTermId($vsYearTerm);
	    
	    $this->termAssessment = $this->getTermAssessment($yearTerm);
	    $this->vsTermAssessment = $this->getTermAssessment($vsYearTerm);
	    
	    $this->initSubjectGroup();
	    $this->initSubjectGroupTeacher();
	    $this->initSubjectGroupStudent();
	    $this->initClass();
	    #### Init object data END ####
	    
	    #### Get score data START ####
	    $rs = $this->getScoreData($filterArr);
	    $data = BuildMultiKeyAssoc($rs, array('YearTermID', 'TermAssessment', 'SubjectID', 'SubjectComponentID', 'UserID'));
	    #### Get score data END ####
	    
	    #### Calculate data START ####
	    $_data = $this->calculateCorrelation($data);
	    #### Calculate data END ####
	    
	    #### Pack data START ####
	    $data = array();
	    if($reportType == self::VALUE_ADDED_TYPE_SUBJECT_GROUP){
	        foreach($_data as $result){
	            $className = $result->className;

	            foreach($result->subjectGroups as $subjectGroup){
	                if($subjectGroup->classNameType == ValueAddedDataSubjectGroup::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS){
    	                foreach($subjectGroup->years as $yearName){
        	                $data[$yearName][$className][] = $result;
    	                }
	                }else{
    	                foreach($subjectGroup->years as $yearName){
        	                $data[$yearName]["({$yearName}) {$className}"][] = $result;
    	                }
	                }
	            }
	        }
	    }else if($reportType == self::VALUE_ADDED_TYPE_TEACHER){
	        foreach($_data as $result){
	            $className = $result->className;
	            $teachers = $this->getTeacherByIds($result->teacherIds);
	            
	            if(count($teachers)){
    	            foreach($teachers as $teacher){
    	                $teacherName = $teacher->__toString();
    	                $data[$teacherName][$className] = $result;
    	            }
	            }else{
	                $teacherName = '';
	                $data[$teacherName][$className] = $result;
	            }
	        }
	        foreach($data as $teacherName => $d){
	            ksort($data[$teacherName]);
	        }
	    }
	    
        ksort($data);
	    #### Pack data END ####
	    
	    return $data;
	}
	
	private function getYearTermId($yearTerm){
	    $TermID = explode('_',$yearTerm);
	    if(empty($TermID[0])){
	        return 0;
	    }else{
	        $sql = "SELECT
	            YearTermID
            FROM
	            ACADEMIC_YEAR_TERM
            WHERE
                AcademicYearID = '{$this->academicYearId}'
            AND
            (
	            YearTermNameEN = '{$TermID[0]}'
	        OR
	            YearTermNameB5 = '{$TermID[0]}'
	        )
	        ";
	        $rs = $this->db->returnVector($sql);
	        
	        if(count($rs == 1)){
	            return $rs[0];
	        }else{
	            throw new Exception("Cannot find ACADEMIC_YEAR_TERM by {$yearTerm}");
	        }
	    }
	}
	
	private function getTermAssessment($yearTerm){
	    $TermID = explode('_',$yearTerm);

	    if(empty($TermID[1])){
	        return '';
	    }else{
	        return $TermID[1];
	    }
	}
	
	private function initSubjectGroup(){

	    #### Get SubjectGroup-Year relation START #### 
	    $sql = "SELECT
	        STCYR.SubjectGroupID,
	        Y.YearID,
	        Y.YearName
        FROM
	        SUBJECT_TERM_CLASS_YEAR_RELATION STCYR
        INNER JOIN
	        YEAR Y
        ON
	        STCYR.YearID = Y.YearID
        ORDER BY
	        Y.Sequence
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    $subjectGroupYearMapping = array();
	    foreach($rs as $r){
	         $subjectGroupYearMapping[ $r['SubjectGroupID'] ][ $r['YearID'] ] = $r['YearName'];
	    }
	    #### Get SubjectGroup-Year relation END ####
	    
	    #### Get SubjectGroup info START ####
	    $sql = "SELECT 
	        STC.SubjectGroupID,
	        STC.ClassTitleEN,
	        STC.ClassTitleB5,
	        ST.SubjectID,
	        ASS.CH_DES,
	        ASS.EN_DES,
	        ST.YearTermID,
	        AYT.AcademicYearID
        FROM
	        SUBJECT_TERM_CLASS STC
        INNER JOIN
	        SUBJECT_TERM ST
        ON
	        STC.SubjectGroupID = ST.SubjectGroupID
        INNER JOIN 
            ASSESSMENT_SUBJECT ASS
        ON
            ST.SubjectID = ASS.RecordID
        INNER JOIN
	        ACADEMIC_YEAR_TERM AYT
        ON
	        ST.YearTermID = AYT.YearTermID
        WHERE
	        AYT.AcademicYearID = '{$this->academicYearId}'
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $r){
	        $subject = &$this->objectPool->getSubjectById($r['SubjectID']);
	        if(empty($subject)){
	            $subject = new ValueAddedDataSubject(
	                $r['SubjectID'],
	                $r['EN_DES'],
	                $r['CH_DES'],
	                $r['YearTermID'],
	                $r['AcademicYearID']
                );
	            
	            $this->objectPool->addSubject($subject);
	        }
	        
	        
	        $subjectGroup = &$this->objectPool->getSubjectGroupById($r['SubjectID']);
	        if(empty($subjectGroup)){
	            $subjectGroup = new ValueAddedDataSubjectGroup(
    	            $r['SubjectGroupID'],
    	            $r['ClassTitleEN'],
    	            $r['ClassTitleB5'],
	                $r['YearTermID']
                );
	            $subjectGroup->setSubject($subject);
	            if($subjectGroupYearMapping[$r['SubjectGroupID']]){
	                $subjectGroup->addYears($subjectGroupYearMapping[$r['SubjectGroupID']]);
	            }
	            
	            $this->objectPool->addSubjectGroup($subjectGroup);
	        }
	    }
	    #### Get SubjectGroup info END ####
	}
	
	private function initSubjectGroupTeacher(){
	    $subjectGroupArr = $this->objectPool->subjectGroupPool;
	    $subjectGroupIds = array_keys($subjectGroupArr);
	    $subjectGroupIdSql = implode("','", $subjectGroupIds);
	    $sql = "SELECT
	        STCT.UserID,
	        STCT.SubjectGroupID
        FROM
            SUBJECT_TERM_CLASS_TEACHER STCT
        INNER JOIN
	        SUBJECT_TERM_CLASS STC
        ON
	        STCT.SubjectGroupID = STC.SubjectGroupID
        WHERE
	        STCT.SubjectGroupID IN ('{$subjectGroupIdSql}')
        ";
	    $rs = $this->db->returnResultSet($sql);

	    foreach($rs as $r){
	        $subjectGroup = &$this->objectPool->getSubjectGroupById($r['SubjectGroupID']);
            $subjectGroup->addTeacherId($r['UserID']);
	    }
	}
	
	private function initSubjectGroupStudent(){
	    $subjectGroupArr = $this->objectPool->subjectGroupPool;
	    $subjectGroupIds = array_keys($subjectGroupArr);
	    $subjectGroupIdSql = implode("','", $subjectGroupIds);
	    $sql = "SELECT
	        STCU.UserID,
	        STCU.SubjectGroupID
        FROM
            SUBJECT_TERM_CLASS_USER STCU
        INNER JOIN
	        SUBJECT_TERM_CLASS STC
        ON
	        STCU.SubjectGroupID = STC.SubjectGroupID
        WHERE
	        STCU.SubjectGroupID IN ('{$subjectGroupIdSql}')
        ";
	    $rs = $this->db->returnResultSet($sql);

	    foreach($rs as $r){
	        $subjectGroup = &$this->objectPool->getSubjectGroupById($r['SubjectGroupID']);
            $subjectGroup->addStudentId($r['UserID']);
	    }
	}
	
	private function initClass(){
	    $sql = "SELECT
	        YC.YearClassID,
	        YC.AcademicYearID,
	        YC.YearID,
	        YC.ClassTitleEN,
	        YC.ClassTitleB5,
	        Y.YearName
        FROM
	        YEAR_CLASS YC
        INNER JOIN
            YEAR Y
        ON
            YC.YearID = Y.YearID
        WHERE
	        AcademicYearID = '{$this->academicYearId}'
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    $yearNameArr = array();
	    $classNameArr = array();
	    foreach($rs as $r){
	        $class = &$this->objectPool->getClassById($r['YearClassID']);
	        if(empty($class)){
	            $class = new ValueAddedDataClass(
	                $r['YearClassID'],
	                $r['AcademicYearID'],
	                $r['YearID'],
	                $r['ClassTitleEN'],
	                $r['ClassTitleB5'],
	                $r['YearName']
	            );
	            
	            $classNameArr[] = strtoupper($r['ClassTitleEN']);
	            $classNameArr[] = strtoupper($r['ClassTitleB5']);
	            $yearNameArr[] = strtoupper($r['YearName']);
	        }
	    }
	    
	    ## Update SubjectGroup object's class name START ##
	    $subjectGroupArr = $this->objectPool->subjectGroupPool;
	    foreach($subjectGroupArr as &$subjectGroup){
	        $subjectGroupTitle = Get_Lang_Selection($subjectGroup->subjectGroupTitleB5, $subjectGroup->subjectGroupTitleEN);
	        $subjectGroupName = end(explode(' ', $subjectGroupTitle));
            $subjectGroup->className = $subjectGroupName;
            $subjectGroup->classNameType = ValueAddedDataSubjectGroup::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_YEAR;
	        
	        foreach($classNameArr as $className){
	            if( $subjectGroupName == $className ){
	                $subjectGroup->classNameType = ValueAddedDataSubjectGroup::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS;
	                break;
	            }
	        }
	    }
	    ## Update SubjectGroup object's class name END ##
	}
	
	
	private function getScoreData($filterArr) {
	    $eclass_db = $this->getPortfolioDatabase();
	    
	    if($this->yearTermId){
	        $filterYearTermSql = " ASSR.YearTermID = '{$this->yearTermId}'";
	    }else{
	        $filterYearTermSql = " (ASSR.YearTermID IS NULL OR ASSR.YearTermID = '0')";
	    }
	    if($this->termAssessment){
	        $filterTermAssessmentSql = " ASSR.TermAssessment = '{$this->termAssessment}'";
	    }else{
	        $filterTermAssessmentSql = " (ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')";
	    }
	    
	    if($this->vsYearTermId){
	        $filterVsYearTermSql = " ASSR.YearTermID = '{$this->vsYearTermId}'";
	    }else{
	        $filterVsYearTermSql = " (ASSR.YearTermID IS NULL OR ASSR.YearTermID = '0')";
	    }
	    if($this->vsTermAssessment){
	        $filterVsTermAssessmentSql = " ASSR.TermAssessment = '{$this->vsTermAssessment}'";
	    }else{
	        $filterVsTermAssessmentSql = " (ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')";
	    }
	    
	    $filterSql = '';
	    if($filterArr['SubjectId']){
	        $filterSql = " AND SubjectID IN ('{$filterArr['SubjectId']}')";
	    }
	    
	    $sql = "SELECT
	        ASSR.RecordID,
	        ASSR.UserID,
	        ASSR.SubjectID,
	        ASSR.SubjectComponentID,
	        ASSR.Score,
	        ASSR.AcademicYearID,
	        ASSR.YearTermID,
	        ASSR.TermAssessment
        FROM
	        {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
        WHERE
	        ASSR.AcademicYearID = '{$this->academicYearId}'
        AND
        (
            (
                {$filterYearTermSql}
            AND
                {$filterTermAssessmentSql}
            ) OR (
                {$filterVsYearTermSql}
            AND
                {$filterVsTermAssessmentSql}
            )
        )
            {$filterSql}
        ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	
	
	private function calculateCorrelation($data){
	    //$data = BuildMultiKeyAssoc($rs, array('YearTermID', 'TermAssessment', 'SubjectID', 'SubjectComponentID', 'UserID'));
	    
	    
	    #### Get all Subject Group list START ####
	    $subjectGroupArr = $this->objectPool->subjectGroupPool;
	    $classStudentMap = array();
	    $yearStudentMap = array();
	    foreach($subjectGroupArr as &$subjectGroup){
	        $className = $subjectGroup->className;
	        $subjectId = $subjectGroup->subjectId;
	        $subjectGroupId = $subjectGroup->subjectGroupId;
	        
	        if($subjectGroup->classNameType == ValueAddedDataSubjectGroup::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS){
                if($classStudentMap[$className][$subjectId] == null){
                    $classStudentMap[$className][$subjectId] = array();
                }
                $classStudentMap[$className][$subjectId] = $subjectGroup;
	        }else if($subjectGroup->classNameType == ValueAddedDataSubjectGroup::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_YEAR){
                if($yearStudentMap[$className][$subjectId] == null){
                    $yearStudentMap[$className][$subjectId] = array();
                }
                $yearStudentMap[$className][$subjectId] = $subjectGroup;
	        }
	    }
	    #### Get all Subject Group list END ####
	    
	    #### Get result START ####
	    $resultArr = array();
	    $studentMap = array_merge($classStudentMap, $yearStudentMap);
	    foreach($studentMap as $className => $d1){
	        foreach($d1 as $subjectId => &$subjectGroupArr){
                $result = new ValueAddedDataResult();
                $result->className = $className;
                $result->subject = &$this->objectPool->getSubjectById($subjectId);
                $result->addSubjectGroups($subjectGroupArr);
                
                $resultArr[] = $result;
	        }
	    }
        
        $subjectComponentID = null;
        foreach($resultArr as &$result){
            $subjectId = $result->subject->subjectId;
            $yearTermId = ($this->yearTermId)?$this->yearTermId:null;
            $termAssessment = ($this->termAssessment)?$this->termAssessment:null;
            $vsYearTermId = ($this->vsYearTermId)?$this->vsYearTermId:null;
            $vsTermAssessment = ($this->vsTermAssessment)?$this->vsTermAssessment:null;
            
            $scoreArr = $data[$yearTermId][$termAssessment][$subjectId][$subjectComponentID];
	        $vsScoreArr = $data[$vsYearTermId][$vsTermAssessment][$subjectId][$subjectComponentID];

	        $scoreTuple = array();
	        foreach($result->studentIds as $studentId){
	            if(
	                $scoreArr[$studentId] &&
	                $vsScoreArr[$studentId] &&
	                $scoreArr[$studentId]['Score'] != -1 &&
	                $vsScoreArr[$studentId]['Score'] != -1 &&
	                $scoreArr[$studentId]['Score'] !== null &&
	                $vsScoreArr[$studentId]['Score'] !== null
                ){
	                $scoreTuple['Y'][] = $scoreArr[$studentId]['Score'];
	                $scoreTuple['X'][] = $vsScoreArr[$studentId]['Score'];
	            }
	        }
            if(count($scoreTuple)){
                $result->result = $this->_calculateCorrelation($scoreTuple);
                $this->objectPool->addResult($result);
            }
        }
	    #### Get result END ####
	    
        return $this->objectPool->resultPool;
	}
	
	private function _calculateCorrelation($scoreTuple){
	    $libSDAS = new libSDAS();
	    
	    $count = count($scoreTuple['Y']);
	    $meanY = array_sum($scoreTuple['Y']) / $count;
	    $meanX = array_sum($scoreTuple['X']) / $count;
	    
	    $sdY = $libSDAS->sd($scoreTuple['Y']);
	    $sdX = $libSDAS->sd($scoreTuple['X']);
	    
	    if($sdY && $sdX){
    	    $productArr = array();
    	    for($i=0;$i<$count;$i++){
    	        $scoreY = $scoreTuple['Y'][$i];
    	        $ySubtractBarY = $scoreY - $meanY;
    	        $ySubtractBarYOverSdY = $ySubtractBarY / $sdY;
    	        
    	        $scoreX = $scoreTuple['X'][$i];
    	        $xSubtractBarX = $scoreX - $meanX;
    	        $xSubtractBarXOverSdX = $xSubtractBarX / $sdX;
    	        
    	        $productArr[] = $ySubtractBarYOverSdY * $xSubtractBarXOverSdX;
    	    }
    	    
    	    $correlation = array_sum($productArr) / $count;
	    }else{
	        $correlation = null;
	    }
	    
	    return $correlation;
	}

	
	public function getTeacherByIds($teacherIds){
	    $teacherArr = array();
	    
	    #### Find teacher in Object Pool START ####
	    $teacherIdToAdd = array();
	    foreach($teacherIds as $teacherId){
            $teacher = &$this->objectPool->getUserById($teacherId);
            if(empty($teacher)){
                $teacherIdToAdd[] = $teacherId;
            }else{
                $teacherArr[] = $teacher;
            }
	    }
	    #### Find teacher in Object Pool END ####
	    

	    #### Find teacher in Database START ####
	    $teacherIdToAddSql = implode("','", $teacherIdToAdd);
	    $sql = "SELECT
	        UserID,
	        ChineseName,
	        EnglishName
        FROM
	        INTRANET_USER
        WHERE
	        UserID IN ('{$teacherIdToAddSql}')
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $r){
	        $user = new ValueAddedDataUser($r['UserID'], $r['ChineseName'], $r['EnglishName']);
	        
	        $teacherArr[] = $user;
	        $this->objectPool->addUser($user);
	    }
	    #### Find teacher in Database END ####
	    
	    usort($teacherArr, array('ValueAddedDataUser', 'compareName'));
	    
	    return $teacherArr;
	}
	
	
	
	
	
	
	
	
	
} // End Class