<?php
/**
 * Change Log:
 * 2017-08-25 Pun
 *  - File Create
 */

if(!class_exists('ValueAddedDataClass')){
class ValueAddedDataClass {
    
    public $yearClassID;
    public $academicYearID;
    public $yearID;
    public $classTitleEN;
    public $classTitleB5;
    public $yearName;
    
	public function __construct($yearClassID, $academicYearID, $yearID, $classTitleEN, $classTitleB5, $yearName){
        $this->yearClassID = $yearClassID;
        $this->academicYearID = $academicYearID;
        $this->yearID = $yearID;
        $this->classTitleEN = $classTitleEN;
        $this->classTitleB5 = $classTitleB5;
        $this->yearName = $yearName;
	}
    
    
    
    
    
    
    
    
} // End Class
}
