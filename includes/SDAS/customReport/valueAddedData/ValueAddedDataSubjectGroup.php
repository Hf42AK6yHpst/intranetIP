<?php
/**
 * Change Log:
 * 2017-08-25 Pun
 *  - File Create
 */

if(!class_exists('ValueAddedDataSubjectGroup')){
class ValueAddedDataSubjectGroup {

    const VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS = 'CLASS';
    const VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_YEAR = 'YEAR';
    
    public $subjectGroupId;
    
    public $subjectGroupTitleEN;
    public $subjectGroupTitleB5;
    public $subjectId;
    public $yearTermId;
    
    private $subject;
    public $years;
    
    public $className;
    public $classNameType;
    
    public $teacherIds;
    public $studentIds;
	
	public function __construct($subjectGroupId, $classTitleEN, $classTitleB5, $yearTermId){
	    $this->subjectGroupId = $subjectGroupId;
	    $this->subjectGroupTitleEN = $classTitleEN;
	    $this->subjectGroupTitleB5 = $classTitleB5;
	    $this->yearTermId = $yearTermId;
	    $this->years = array();
	    $this->teacherIds = array();
	    $this->studentIds = array();
	}
    
	public function setSubject($subject){
	    $this->subject = $subject;
	    $this->subjectId = $subject->subjectId;
        $subject->addSubjectGroups($this);
	}
    
	public function &getSubject(){
	    return $this->subject;
	}
	
	public function addYears($years){
	    $this->years = array_merge($this->years, $years);
	}
	
	public function addTeacherId($userId){
	    $this->teacherIds[] = $userId;
	}
	
	public function addStudentId($userId){
	    $this->studentIds[] = $userId;
	}
	




	public function __toString(){
	    $groupId = $this->subjectGroupId;
	    $subjectName = Get_Lang_Selection($this->subject->subjectTitleB5,$this->subject->subjectTitleEN);
	    $name = Get_Lang_Selection($this->subjectGroupTitleB5,$this->subjectGroupTitleEN);
	    
	    return "{$groupId}_{$this->yearTermId}: {$name} ({$subjectName})";
	}
    
} // End Class
}

?>