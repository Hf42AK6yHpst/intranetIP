<?php

class promotionAssessment
{

    private $config;

    private $mode;

    private $andOr;
    // ########################################### Obj Basics Start ############################################
    public function __construct($config, $mode, $andOr)
    {
        $this->config = $config;
        $this->mode = $mode;
        
        $andOrSymbol = array('and'=>' && ', 'or'=>' || ');
        foreach($andOr as $_type => $_andor){
            $this->andOr[$_type] = $andOrSymbol[$_andor];
        }
    }

    public function checkRequirement($assessmentResultArr, $type)
    {
        if ($this->mode == 'junior') {
            $result = $this->checkJuniorRequirement($assessmentResultArr, $type);
        } elseif ($this->mode == 'senior') {
            $result = $this->checkSeniorRequirement($assessmentResultArr, $type);
        } else {
            $result = false;
        }
        return $result;
    }
    // ########################################### Obj Basics End ############################################
    private function checkJuniorRequirement($assessmentResultArr, $type)
    {
        $thisRequirements = $this->config[$type];
        
        $failArr = array();
        foreach ((array) $thisRequirements as $_reqInfo) {
            $_subjectId = $_reqInfo['Subject'];
            $_thisSubjectReq =  $_reqInfo['ScoreRequire'];
            $_thisScore = (float) $assessmentResultArr[$_subjectId];
            $failArr[] = ($_thisScore < $_thisSubjectReq) ? 'true' : 'false';
        }
            
        $conditionString = implode($this->andOr[$type], $failArr);
        
        $result = (eval("return \$result = $conditionString ;")) ? true : false;
        
        return $result;
    }

    private function checkSeniorRequirement($assessmentResultArr, $type)
    {
        $cemlWeighting = $this->config['CEMLWeighting'];
        $thisRequirement = $this->config[$type];
        $cemlRequirement = $thisRequirement['CEMLScore'];
        $electiveRequirement = $thisRequirement['electives'];
        
        $meetCeml = $this->checkMeetCEMLRequirement($assessmentResultArr, $cemlRequirement, $cemlWeighting);
        $meetElectives = $this->checkMeetElectiveRequirement($assessmentResultArr, $electiveRequirement);
        $result = $this->andOr[$type] == 'or' ? ($meetCeml || $meetElectives) : ($meetCeml && $meetElectives);
       
        return $result;
    }

    public function calculateCEMLScore($assessmentResultArr, $cemlWeighting)
    {
        $thisCemlScore = 0;
        foreach ($cemlWeighting as $_subjectId => $_weighting) {
            $_thisSubjectScore = (float) $assessmentResultArr[$_subjectId];
            $thisCemlScore += $_thisSubjectScore * $_weighting;
        }
        
        $totalWeighting = array_sum(array_values($cemlWeighting));
        $thisCemlScore = $thisCemlScore / $totalWeighting;
        
        return $thisCemlScore;
    }

    private function checkMeetCEMLRequirement($assessmentResultArr, $requirement, $cemlWeighting)
    {
        $thisCemlScore = $this->calculateCEMLScore($assessmentResultArr, $cemlWeighting);
        
        return $thisCemlScore < $requirement;
    }

    private function checkMeetElectiveRequirement($assessmentResultArr, $electiveRequirement)
    {
        $coreSubjectArr = $this->config['coreSubject'];
        
        $requireNo = $electiveRequirement['numOfElectives'];
        $scoreRequirement = $electiveRequirement['scoreRequirement'];
        
        $failedSubjects = array();
        foreach ($assessmentResultArr as $_subjectId => $_subjectScore) {
            if (! in_array($_subjectId, $coreSubjectArr) && $_subjectScore < $scoreRequirement) {
                $failedSubjects[] = $_subjectId;
            }
        }
        
        return count($failedSubjects) >= $requireNo;
    }
    // ########################################### Simple End ############################################
    private function simple($scoreArr)
    {
        // get config
        $andOrConds = $this->getObjVariable('andOrConds');
        $config = $this->getObjVariable('currentConfig');
        
        $requirementArr = $config['rules'];
        $passArr = array();
        foreach ((array) $requirementArr as $_subjectId => $_thisSubjectReq) {
            $_thisScore = (int) $scoreArr[$_subjectId];
            $passArr[] = ($_thisScore >= $_thisSubjectReq) ? 'true' : 'false';
        }
        $passConditionString = implode($andOrConds, $passArr);
        $result = (eval("return $passConditionString;")) ? true : false;
        return $result;
    }
    // ########################################### Simple End ############################################
}

?>
