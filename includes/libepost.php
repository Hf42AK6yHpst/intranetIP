<?php
# using : thomas

/* Modification Log:
 * 
 */

if (!defined("LIBEPOST_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOST_DEFINED",true);
	include_once($intranet_root."/lang/lang.$intranet_session_language.php");
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	//include_once($intranet_root."/includes/ePost/tmpLang_$intranet_session_language.php");
	
	class libepost extends libdb
	{
		var $user_obj;
		var $is_editor;
		var $is_publish_manager;
		var $post_ids;
		
		function libepost(){
			global $intranet_root,$UserID;
			
			include($intranet_root."/includes/libuser.php");
			
			$this->libdb();
			$this->user_obj = new libuser($UserID);
			$this->post_ids = array();
	
			# Check the current user is editor or not
			$this->check_is_editor();
			
			# Check the current user is publish manager or not
			$this->check_is_publish_manager();
		}
		
		function check_is_expired(){
			global $plugin;
			
			$result = true;
			
			if($plugin['ePost']===true){
				$result = false;
			}
			else if(is_string($plugin['ePost'])){
				$expiry_date_ary = explode('-', $plugin['ePost']);
				
				if(count($expiry_date_ary)==3){
					$Year  = (int)$expiry_date_ary[0];
					$Month = (int)$expiry_date_ary[1];
					$Day   = (int)$expiry_date_ary[2];
					
					if($Year>=2012 && $Month>=1 && $Month<=12 && $Day>=1 && $Day<=31){
					   	$result = time() >= strtotime($plugin['ePost']);
					}
				}
			}
			
			return $result;
		}
		
		function auth(){
			global $intranet_root;
			
			if($this->check_is_expired()){
				header('Location:/home/ePost/expired.php');
				die();
			}
		}
		
		function portal_auth(){
			global $intranet_root;
			$this->auth();
			
			if(!$this->is_editor){
				header('Location:/home/ePost/index.php');
				die();
			}
		}
		
		function check_is_editor(){
			global $cfg_ePost;
			
			if($this->user_obj->isTeacherStaff()){
				$this->is_editor = true;
			}
			else if($this->user_obj->isStudent()){
				# Check is student editor
				$sql = "SELECT
							COUNT(*)
						FROM
							EPOST_STUDENT_EDITOR
						WHERE
							UserID = ".$this->user_obj->UserID." AND
							Status = ".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist'];
				$this->is_editor = current($this->returnVector($sql)) > 0;
			}
		}
		
		function check_is_publish_manager(){
			global $cfg_ePost;
			
			if($this->user_obj->isTeacherStaff()){
				$sql = "SELECT
							COUNT(*)
						FROM
							EPOST_PUBLISH_MANAGER
						WHERE
							UserID = ".$this->user_obj->UserID." AND
							Status = ".$cfg_ePost['BigNewspaper']['PublushManager_status']['exist'];
				$this->is_publish_manager = current($this->returnVector($sql)) > 0;
			}
			else if($this->user_obj->isStudent()){
				$this->is_publish_manager = false;
			}
		}
	}
	
	class libepost_ui extends libepost
	{
		function libepost_ui(){
			# Initialize ePost Object
			$this->libepost();
		}
		
		function gen_portal_page_tab(){
			global $HTTP_SERVER_VARS, $intranet_httppath, $Lang;
			
			$is_current_newspaper 	    = strpos($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/newspaper/")!==false;
			$is_current_request_list    = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/request_list/")!==false;
			$is_current_student_editor  = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/student_editor/")!==false;
			$is_current_article_shelf   = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/article_shelf/")!==false;
			$is_current_publish_manager = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/publish_manager/")!==false;
			
			if($is_current_newspaper || $is_current_request_list || $is_current_student_editor || $is_current_article_shelf || $is_current_publish_manager){
				$html  = "<div class='page_tab'>";
				$html .= "	<ul>";
				$html .= "		<li ".($is_current_newspaper || $is_current_article_shelf? "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/newspaper/index.php'><span>ePost</span></a></li>";
				$html .= "		<li ".($is_current_request_list? "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/request_list/index.php'><span>".$Lang['ePost']['RequestList']."</span></a></li>";
				
				if($this->user_obj->isTeacherStaff() && $this->is_editor){
					$html .= "	<li ".($is_current_student_editor?  "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/student_editor/index.php'><span>".$Lang['ePost']['StudentEditor']."</span></a></li>";
					$html .= "	<li ".($is_current_publish_manager? "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/publish_manager/index.php'><span>".$Lang['ePost']['PublishingManager']."</span></a></li>";
				}
				
				$html .= "	</ul>";
				$html .= "	<p class='spacer'>&nbsp;</p>";
				$html .= "</div>";
			}
			
			return $html;
		}
		
		function gen_portal_page_nav($nav_ary=array()){
			$html = "<div class=\"natvigation\">";
	    	
	    	for($i=0;$i<count($nav_ary);$i++){
	    		$title = $nav_ary[$i]["title"];
	    		$link  = $nav_ary[$i]["link"];
	    		
	    		$html .= trim($link)!=""? "<a href=\"$link\">$title</a>" : "<span>$title</span>";
	    	}
	    	
	    	$html .= "</div>";
	    	
	    	return $html;
		}
		function gen_admin_publish_manager_page_nav($selected_item){
			global $Lang;
			
	    	$html .= "<div class=\"shadetabs\">";
			$html .= "		<ul>";
			$html .= "			<li".($selected_item==0? " class=\"selected\"":"")."><a href=\"/home/ePost/publish_manager/index.php\">".$Lang['ePost']['TeacherEditors']."</a></li>";
			$html .= "			<li".($selected_item==1? " class=\"selected\"":"")."><a href=\"/home/ePost/student_editor/index.php\">".$Lang['ePost']['StudentEditors']."</a></li>";
			$html .= "		</ul>";
			$html .= "</div>";
			
			return $html;
	    }		
		function gen_admin_newspaper_page_nav($selected_item){
			global $Lang;
			
	    	$html .= "<div class=\"shadetabs\">";
			$html .= "		<ul>";
			$html .= "			<li".($selected_item==0? " class=\"selected\"":"")."><a href=\"/home/ePost/newspaper/index.php\">".$Lang['ePost']['Issues']."</a></li>";
			$html .= "			<li".($selected_item==1? " class=\"selected\"":"")."><a href=\"/home/ePost/article_shelf/index.php\">".$Lang['ePost']['ArticleShelf']."</a></li>";
			$html .= "		</ul>";
			$html .= "</div>";
			
			return $html;
	    }
		
		function gen_courseware_drop_down_list($id='', $name='', $default_choice=1, $other_attr=''){
	    	global $cfg_ePost, $Lang;
	    	
	    	$courseware_ary 	= $cfg_ePost['BigNewspaper']['ArticleType']['courseware'];
	    	$non_courseware_ary = $cfg_ePost['BigNewspaper']['ArticleType']['non_courseware'];
	    	
	    	$html .= "<select id=\"$id\" name=\"$name\" $other_attr>";
			$html .= "	<option value=\"0\">".$Lang['ePost']['Filter']['All']."</option>";
			$html .= "	<option value=\"".$courseware_ary['id']."\" ".($default_choice==$courseware_ary['id']? "selected":"")." disabled>".$courseware_ary['name']."</option>";
			$html .= "	<option value=\"".$non_courseware_ary['id']."\" ".($default_choice==$non_courseware_ary['id']? "selected":"").">".$non_courseware_ary['name']."</option>";
			$html .= "</select>";
			
			return $html;
	    }
		
		function gen_article_type_drop_down_list($courseware, $id='', $name='', $default_choice='', $restriction_ary=array(), $other_attr='', $allow_select_all=true){
	    	global $cfg_ePost, $Lang;
	    	
	    	if($courseware>0){
	    		$courseware_ary = $courseware==1? $cfg_ePost['BigNewspaper']['ArticleType']['courseware'] : $cfg_ePost['BigNewspaper']['ArticleType']['non_courseware'];
	    		
		    	$html .= "<select id=\"$id\" name=\"$name\" $other_attr>";
		    	$html .= $allow_select_all? "<option value=\"\" ".(!$default_choice? 'selected':'').">".$Lang['ePost']['Filter']['All']."</option>":"";
		    	
				foreach($courseware_ary['types'] as $type=>$type_ary){
			    	if(!in_array($type, $restriction_ary))
			    		$html .= "<option value=\"$type\" ".($type==$default_choice? "selected":"").">".$type_ary['name']."</option>";
			    }
		    	
		    	$html .= "</select>";
	    	}
	    	
	    	return $html;
	    }
	    
	    # Remark : function gen_article_request_drop_down_list() is for non-courseware use only
	    function gen_article_request_drop_down_list($courseware, $id='', $name='', $default_choice='', $other_attr='', $allow_select_all=true){
	    	global $cfg_ePost, $intranet_root, $Lang;
	    	
	    	if($courseware==2){
		    	include_once('libepost_request.php');
		    	$ePostRequests = new libepost_requests();
		    	
		    	$html .= "<select id=\"$id\" name=\"$name\" $other_attr>";
			    $html .= $allow_select_all? "<option value=\"\" ".(!$default_choice? 'selected':'').">".$Lang['ePost']['Filter']['All']."</option>":"";
			    
			    if(is_array($ePostRequests->Requests) && count($ePostRequests->Requests)){
			    	foreach($ePostRequests->Requests as $RequestObj){
			    		$html .= "<option value=\"".$RequestObj->RequestID."\" ".($RequestObj->RequestID==$default_choice? "selected":"").">".$RequestObj->Topic."</option>";
			    	}
			    }
				
			    $html .= "</select>";
	    	}
	    	
	    	return $html;
	    }
	    
	    function gen_article_sub_type_drop_down_list($courseware, $article_type, $id='', $name='', $default_choice='', $other_attr=''){
	    	global $cfg_ePost, $Lang;
	    	
	    	if($courseware>0){
		    	$sub_type_ary = $courseware==1? $cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types'][$article_type]['sub_types'] : $cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types'][$article_type]['sub_types'];
		    	
		    	if(is_array($sub_type_ary) && count($sub_type_ary)){
		    		$html .= "<select id=\"$id\" name=\"$name\" $other_attr>";
		    		$html .= "	  <option value=\"\" ".(!$default_choice? 'selected':'').">".$Lang['ePost']['Filter']['All']."</option>";
		    		
		    		foreach($sub_type_ary as $type=>$type_ary){
		    			$html .= "<option value=\"$type\" ".($type==$default_choice? "selected":"").">".$type_ary['name']."</option>";
		    		}
		    		
		    		$html .= "</select>";
	    		}
	    	}
	    	return $html;
	    }
	    
	    function gen_article_theme_type_filter($id='', $name='', $default_choice='', $other_attr=''){
	    	global $cfg_ePost;
	    	
	    	if(is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'])){
		    	$html  = "<select id='$id' name='$name' $other_attr>";
		    	
		    	foreach($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'] as $Type=>$Type_ary){
		    		$html .= "<option value='$Type' ".($Type==$default_choice? "selected":"").">".$Type_ary['name']."</option>";
		    	}
		    	
		    	$html .= "</select>";
	    	}
	    	
	    	return $html;
	    }
	    
	    function gen_article_theme_theme_filter($Type, $id='', $name='', $default_choice='', $other_attr=''){
	    	global $cfg_ePost;
	    	
	    	if(is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'])){
		    	$html  = "<select id='$id' name='$name' $other_attr>";
		    	
		    	foreach($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'] as $Theme=>$Theme_ary){
		    		$html .= "<option value='$Theme' ".($Theme==$default_choice? "selected":"").">".$Theme_ary['name']."</option>";
		    	}
		    	
		    	$html .= "</select>";
	    	}
	    	
	    	return $html;
	    }
	    
	    function gen_article_theme_color_filter($Type, $Theme, $id='', $name='', $default_choice='', $other_attr=''){
	    	global $cfg_ePost;
	    	
	    	if(is_array($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'])){
		    	$html  = "<select id='$id' name='$name' $other_attr>";
		    	
		    	foreach($cfg_ePost['BigNewspaper']['ArticleTheme']['Types'][$Type]['Themes'][$Theme]['Color'] as $Color=>$Color_ary){
		    		$html .= "<option value='$Color' ".($Color==$default_choice? "selected":"").">".$Color_ary['name']."</option>";
		    	}
		    	
		    	$html .= "</select>";
	    	}
	    	
	    	return $html;
	    }
	    
	    function gen_view_newspaper_page_drop_down_list($NewspaperObj, $IsZoomOut=false, $id='', $name='', $default_page_order='', $other_attr=''){
			$html .= "<select id='$id' name='$name' $other_attr>";
			
			if(is_array($NewspaperObj->Newspaper_PageIDs) && count($NewspaperObj->Newspaper_PageIDs)){
				$PageNoCounter = 1;
				foreach($NewspaperObj->Newspaper_PageIDs as $PageOrder=>$PageObj){
					if($IsZoomOut){
						if($PageNoCounter==1 || $PageNoCounter%2==0){
							$PageNoDosplay = $PageNoCounter==1 || $PageNoCounter==count($this->Newspaper_PageIDs)? $PageNoCounter : $PageNoCounter."-".($PageNoCounter+1);
							$html .= "<option value='$PageOrder' ".($PageOrder==$default_page_order? "selected":"").">$PageNoDosplay</option>";
						}
					}
					else{
						$html .= "<option value='$PageOrder' ".($PageOrder==$default_page_order? "selected":"").">$PageNoCounter</option>";
					}
					$PageNoCounter++;
				}
			}
			
			$html .= "</select>";
			
			return $html;
		}
	    
	    function gen_writer_flv($flv_path, $IsSelectArticlePage=false, $cust_player_id='', $width=320, $height=255){
	    	$cust_player_id = $cust_player_id? $cust_player_id : '1'; 
	    	
	    	$width  = $width?  $width  : 320;
	    	$height = $height? $height : 255;
	    	
			$fixsize = 1;
			$playerID = 'flvplayer_'.$cust_player_id;
			$paddingID = '_'.$cust_player_id;
			
			$video  = "<div id=\"$playerID\" name=\"$playerID\" style=\"width:".$width.";height:".$height."\">\n";
			$video .= "<p><a href=\"http://www.adobe.com/go/getflashplayer\"><img src=\"http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif\" alt=\"Get Adobe Flash player\" /></a></p>\n";
			$video .= "</div>\n";
			
			$video .= '<script language="javascript">
							function generateSWF'.$paddingID.'() {
								var backColor = "#000000";
								var swfWidth = '.$width.';
								var swfHeight = '.$height.';
								var fileName = "/home/ePost/flash/flvplayer.swf";
								var divID = "'.$playerID.'";
								var flashvars = {
									path: "'.$flv_path.'",
									autoStart: "0",
									playMaxTime: "999",
									fixsize: "'.$fixsize.'",
									stageW: "'.$width.'",
									stageH: "'.$height.'"
								}	
								//alert(flashvars.FlashVars);
					
								var params = {
									play: "true",
									loop: "true",
									menu: "true",
									quality: "high",
									scale: "showall",
									wmode: "window", 
									bgcolor: backColor,
									devicefont: "false",
									allowscriptaccess: "sameDomain",
									allowfullscreen: "true"
								};
								var attributes = {
									id: "flashContent'.$paddingID.'",
									name: "flashContent'.$paddingID.'",
									align: "middle"
								};
						
								$(document).ready(function(){swfobject.embedSWF(fileName, divID, swfWidth, swfHeight, "9.0.0", "expressInstall.swf", flashvars, params, attributes);});
							}						
					   </script>';
	
			# New player
	
			$player .= $IsSelectArticlePage? "<div style=\"text-align:center;\"><br>\n" : "<div>\n";
			$player .= $video;
			$player .= "<script language='javascript'>generateSWF{$paddingID}()</script>";
			$player .= "</div>\n";
			
			return $player;
	    }
	    
	    /* Function gen_article_display()
	     * Structure of $para_ary
	     * $para_ary['Title'] 								- Type: String 	Usage: Article Title
	     * $para_ary['Content'] 							- Type: String 	Usage: Article Content
	     * $para_ary['Attachment'] 							- Type: Array 	Usage: Attachment Parameter
	     * $para_ary['Attachment']['Original']				- Type: String  Usage: Original Value of Attachment
	     * $para_ary['Attachment']['FilePath']   			- Type: String  Usage: Attachment File Path
	     * $para_ary['Attachment']['HTTPPath']   			- Type: String  Usage: Attachment HTTP Path
	     * $para_ary['Attachment']['SkipResize']  			- Type: Bool    Usage: Pass the attachment for resize or not
		 * $para_ary['Attachment']['img_width']   			- Type: Int	    Usage: Set width of image
	     * $para_ary['Attachment']['img_height']  			- Type: Int	    Usage: Set height of image
	     * $para_ary['Attachment']['flvplayerID'] 			- Type: String  Usage: Set the ID of the FLV player
	     * $para_ary['Attachment']['flv_width']   			- Type: Int	    Usage: Set width of FLV player
	     * $para_ary['Attachment']['flv_height']  			- Type: Int	    Usage: Set height of FLV player
	     * $para_ary['Attachment']['IsSelectArticlePage'] 	- Type: Bool	Usage: Set the Page is select article page or not
	     * $para_ary['Attachment']['Caption']				- Type: String	Usage: Set the Caption shown under the image
	     * $para_ary['Attachment']['simple_layout']		    - Type: Bool	Usage: Set the Image is shown with just a <img> tag or not
	     * */
	    function gen_article_display($para_ary=array()){
	    	global $cfg_ePost;
	    	
	    	# Initialize Variable
	    	$Title		= $para_ary['Title']? $para_ary['Title'] : '';
	    	$Content	= $para_ary['Content']? $para_ary['Content'] : '';
	    	$Attachment = $para_ary['Attachment']? $para_ary['Attachment'] : array();
	    	
	    	$title_html 	 = $Title;
	    	$content_html 	 = $Content;
	    	$attachment_html = '';
	    	$suggest_height  = '';
	    	
	    	# Process Attachment
			$ext = strtoupper(substr($Attachment['Original'], '-4'));
			
			if(in_array($ext, array('.JPG','.GIF','.JPE','.PNG','.BMP')) && file_exists($Attachment['FilePath'])){
				if(!$Attachment['SkipResize']){
					if($Attachment['img_width'] && $Attachment['img_height'])
						$size_ary = array('max_width'=>$Attachment['img_width'], 'max_height'=>$Attachment['img_height']);
					else
						$size_ary = $cfg_ePost['writer_photo'];
						
					$sizeInfo = $this->resize_content_image($Attachment['FilePath'], $size_ary);
					$final_width  = $sizeInfo[0];
					$final_height = $sizeInfo[1];
					$attr  = ($final_height != '')? 'height="'.$final_height.'px" ' : '';
					$attr .= ($final_width != '') ? 'width="'.$final_width.'px"' : '';
				}
					
				if($para_ary['Attachment']['simple_layout']){
					$attachment_html = "<center><img src=\"".$Attachment['HTTPPath']."\" ".$attr."/></center>";
				}
				else{
					$attachment_html = "<table align=\"center\" class=\"log_photo\">
											<tr>
												<td class=\"log_photo_file\"><img src=\"".$Attachment['HTTPPath']."\"  ".$attr."/></td>
											</tr>";
					if($para_ary['Attachment']['Caption']){
						$attachment_html .= "<tr>
												<td class=\"log_photo_desc\">".$Attachment['Caption']."</td>
											 </tr>";
					}
					$attachment_html .= "</table>";
				}
			}
			else if($ext=='.FLV'){
				$final_width  = $Attachment['flv_width'];
				$final_height = $Attachment['flv_height'];
				$IsSelectArticlePage = $Attachment['IsSelectArticlePage'];
				$attachment_html = $this->gen_writer_flv($para_ary['Attachment']['HTTPPath'], $IsSelectArticlePage, $Attachment['flvplayerID'], $final_width, $final_height);
				
				if(!$para_ary['Attachment']['simple_layout']){
					$attachment_html = "<table align=\"center\" class=\"log_photo\">
											<tr>
												<td class=\"log_photo_file\">$attachment_html</td>
											</tr>";
					if($para_ary['Attachment']['Caption']){
						$attachment_html .= "<tr>
												<td class=\"log_photo_desc\">".$Attachment['Caption']."</td>
											 </tr>";
					}
					$attachment_html .= "</table>";
				}
			}
			else{
				$attachment_html = "";
			}
			
			# Process the return array
			$return_ary = array('title'=>$title_html,
								'content'=>$content_html,
								'attachment'=>$attachment_html,
								'attachment_w'=>$final_width,
								'attachment_h'=>$final_height,
								'suggest_height'=>$suggest_height);
			
			return $return_ary;
	    }
	    
	    function resize_content_image($attach_file, $cfgWriterPhoto=''){
	    	global $cfg_ePost;
	    	
	    	if($cfgWriterPhoto == ''){
	    		$cfgWriterPhoto = $cfg_ePost['writer_photo'];
	    	}
	    	
	    	if(file_exists($attach_file)){
	    		list($width, $height, $type, $attr) = getimagesize($attach_file);
				if($width > $height)
				{
					$final_width  = $width > $cfgWriterPhoto['max_width'] ? $cfgWriterPhoto['max_width'] : $width;
					$final_height = $width > $cfgWriterPhoto['max_width'] ? floor(($height / $width) * $cfgWriterPhoto['max_width']) : $height;
					if($final_height > $cfgWriterPhoto['max_height'])
					{
						$final_width  = floor(($final_width / $final_height) * $cfgWriterPhoto['max_height']);
						$final_height = $cfgWriterPhoto['max_height'];
					}
				}
				else if($width < $height)
				{
					$final_width  = $height > $cfgWriterPhoto['max_height'] ? floor(($width / $height) * $cfgWriterPhoto['max_height']) : $width;
					$final_height = $height > $cfgWriterPhoto['max_height'] ? $cfgWriterPhoto['max_height'] : $height;
					if($final_width > $cfgWriterPhoto['max_width'])
					{
						$final_width  =  $cfgWriterPhoto['max_width'];
						$final_height = floor(($final_height / $final_width) * $cfgWriterPhoto['max_width']);
					}
				}
				else
				{
					$final_width  = $width > $cfgWriterPhoto['max_width'] ? $cfgWriterPhoto['max_width'] : $width;
					$final_height = $height > $cfgWriterPhoto['max_height'] ? $cfgWriterPhoto['max_height'] : $height;
			    }
			}
			$final_width = ($final_width == '') ? $cfgWriterPhoto['max_width'] : $final_width;
			return array($final_width, $final_height);
	    }
	    
	    function gen_big_page_layout($PageObj, $IsPortal=true, $IsEditMode=false){
	    	global $cfg_ePost;
			
			# Get Page Format Class
			$page_type = $PageObj->get_page_format();
			
			switch($page_type){
				case $cfg_ePost['BigNewspaper']['PageType']['Cover']:
					$format_class = "page_big_cover".$PageObj->get_page_big_class_suffix();
					$newspaper_body_class = $PageObj->Newspaper_CoverBanner? "customized":"";
				break;
				case $cfg_ePost['BigNewspaper']['PageType']['Left']:
					$format_class = "page_big_left".$PageObj->get_page_big_class_suffix();
					$newspaper_body_class = $PageObj->Newspaper_InsideBanner? "customized":"";
				break;
				case $cfg_ePost['BigNewspaper']['PageType']['Right']:
					$format_class = "page_big_right".$PageObj->get_page_big_class_suffix();
					$newspaper_body_class = $PageObj->Newspaper_InsideBanner? "customized":"";
				break;
			}
			
			$html .= $IsPortal? "<div class=\"".($IsEditMode? "print_alert_line ":"")."newspaper_body_bg\">\n" : "";
			$html .= "	<div class=\"newspaper_body $newspaper_body_class\">\n";
			$html .= "		<div class=\"newspaper_page_big $format_class\">\n";
			$html .= "			<div class=\"page_top\">\n";
			$html .= "				<div class=\"page_top_right\">\n";
			$html .= "					<div class=\"page_top_bg\">\n";
			$html .= "						<h1 ".($PageObj->Newspaper_SchoolLogo? "class=\"with_logo\"":"").">\n";
			
			if($page_type!=$cfg_ePost['BigNewspaper']['PageType']['Cover']){
				$html .= "						<div ".($PageObj->Newspaper_InsideBanner? "style=\"background-image:url('".$PageObj->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner'])."')\"":"")." class=\"custom_banner\"></div>";
				$html .= $PageObj->Newspaper_InsideBanner && $PageObj->Newspaper_InsideBannerHideTitle? "":"<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>\n";
			}
			else{
				$html .= "						<div ".($PageObj->Newspaper_CoverBanner? "style=\"background-image:url('".$PageObj->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner'])."')\"":"")." class=\"custom_banner\"></div>";
				$html .= $PageObj->Newspaper_CoverBanner && $PageObj->Newspaper_CoverBannerHideTitle? "":"<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>\n";
			}
			
			$html .= "							<span class=\"school_logo\">";
			$html .= "								<img src=\"".($PageObj->Newspaper_SchoolLogo? $PageObj->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']):"")."\">";
			$html .= "							</span>";
			
			if($page_type!=$cfg_ePost['BigNewspaper']['PageType']['Cover']){
				$html .= $PageObj->Newspaper_InsideBanner && $PageObj->Newspaper_InsideBannerHideName? "":"<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>\n";
				$html .= $PageObj->Newspaper_InsideBanner && $PageObj->Newspaper_InsideBannerHidePageCat? "":"<span id=\"page_cat\" class=\"".$cfg_ePost['BigNewspaper']['Theme'][$PageObj->Page_Theme]['class']."\">".$PageObj->Page_Name."</span>\n";
			}
			else{
				$html .= $PageObj->Newspaper_CoverBanner && $PageObj->Newspaper_CoverBannerHideName? "":"<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>\n";
			}
			
			$html .= "						</h1>\n";
			$html .= "					</div>\n";
			$html .= "				</div>\n";
			$html .= "			</div>\n";
			$html .= "			<p class=\"spacer\"></p>\n";
			$html .= "			<div class=\"page_body\">\n";
			$html .= "				<div class=\"page_body_right\">\n";
			$html .= "					<div class=\"page_body_bg\">\n";
			$html .= "						<div class=\"page_body_content\">\n";
			$html .= "							<p class=\"spacer\"></p>\n";
			
			$article_ary	 = $PageObj->get_articles();
			$article_cfg_ary = $PageObj->get_page_TypeLayout_articles_ary();
			for($i=1;$i<=count($article_cfg_ary);$i++){
				$para_ary	 = array();
				$para_ary['PageID'] 			= $PageObj->PageID;
				$para_ary['Position'] 			= $i;
				$para_ary['ArticleObj']			= $article_ary[$i];
				$para_ary['ArticleCfg'] 		= $article_cfg_ary[$i];
				$para_ary['IsEditMode']		 	= $IsEditMode;
				$para_ary['ShowMoreBtn'] 		= false;
	     		$para_ary['RestrictionNameAry'] = $PageObj->get_article_restriction_name_ary($i);
				
				$html .= $this->gen_page_article_display($para_ary);
				$html .= $article_cfg_ary[$i]['needspacer']? "<p class=\"spacer\"></p>":"";
			}
			
			$html .= "							<p class=\"spacer\"></p>\n";
			$html .= "						</div>\n";
			$html .= "					</div>\n";
			$html .= "				</div>\n";
			$html .= "			</div>\n";
			$html .= "			<div class=\"page_bottom\">\n";
			$html .= "				<div class=\"page_bottom_right\">\n";
			$html .= "					<div class=\"page_bottom_bg\">&nbsp;</div>\n";
			$html .= "				</div>\n";
			$html .= "			</div>\n";
			$html .= "		</div>\n";
			$html .= "	</div>\n";
			$html .= $IsPortal? "</div>\n":"";
			$html .= "<p class=\"spacer\"></p>\n";
			
			return $html;
	    }
		function gen_single_big_page_layout($PageObj, $IsPortal=true, $IsEditMode=false){
	    	global $cfg_ePost,$Lang;
			
			# Get Page Format Class
			$page_type = $PageObj->get_page_format();
			
			switch($page_type){
				case $cfg_ePost['BigNewspaper']['PageType']['Cover']:
					$format_class = "page_big_cover".$PageObj->get_page_big_class_suffix();
					$newspaper_body_class = $PageObj->Newspaper_CoverBanner? "customized":"";
				break;
				case $cfg_ePost['BigNewspaper']['PageType']['Left']:
					$format_class = "page_big_left".$PageObj->get_page_big_class_suffix();
					$newspaper_body_class = $PageObj->Newspaper_InsideBanner? "customized":"";
				break;
				case $cfg_ePost['BigNewspaper']['PageType']['Right']:
					$format_class = "page_big_right".$PageObj->get_page_big_class_suffix();
					$newspaper_body_class = $PageObj->Newspaper_InsideBanner? "customized":"";
				break;
			}
			$html .= $IsPortal? "<div class=\"".($IsEditMode? "print_alert_line ":"")."newspaper_body_bg\">\n" : "";
			if($IsEditMode){
			
				$html .= "
					<div class=\"page_edit_tool\"><div class=\"Content_tool\"><a href=\"javascript:add_new_page(".$PageObj->PageID.")\"  class=\"edit_text\">".$Lang['ePost']['EditPageInformationAndLayout']."</a> </div></div>";
				if($PageObj->Page_ModifiedByUser!=''){
					$html .= "<span class=\"m_date\">".$Lang['General']['LastModified']." : ".substr($PageObj->Page_ModifiedDate,0,10)." ".$Lang['ePost']['LastModifiedBy'][0]." ".$PageObj->Page_ModifiedByUser." ".$Lang['ePost']['LastModifiedBy'][1]."</span>";
				}
			}
			$html .= "	<div class=\"newspaper_body $newspaper_body_class\">\n";
			$html .= "		<div class=\"newspaper_page_big $format_class\">\n";
			$html .= "			<div class=\"page_top\">\n";
			$html .= "				<div class=\"page_top_right\">\n";
			$html .= "					<div class=\"page_top_bg\">\n";
			$html .= "						<h1 ".($PageObj->Newspaper_SchoolLogo? "class=\"with_logo\"":"").">\n";
			
			if($page_type!=$cfg_ePost['BigNewspaper']['PageType']['Cover']){
				$html .= "						<div ".($PageObj->Newspaper_InsideBanner? "style=\"background-image:url('".$PageObj->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner'])."')\"":"")." class=\"custom_banner\"></div>";
				$html .= $PageObj->Newspaper_InsideBanner && $PageObj->Newspaper_InsideBannerHideTitle? "":"<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>\n";
			}
			else{
				$html .= "						<div ".($PageObj->Newspaper_CoverBanner? "style=\"background-image:url('".$PageObj->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner'])."')\"":"")." class=\"custom_banner\"></div>";
				$html .= $PageObj->Newspaper_CoverBanner && $PageObj->Newspaper_CoverBannerHideTitle? "":"<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>\n";
			}
			
			$html .= "							<span class=\"school_logo\">";
			$html .= "								<img src=\"".($PageObj->Newspaper_SchoolLogo? $PageObj->return_newspaper_attachment_http_path($cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']):"")."\">";
			$html .= "							</span>";
			
			if($page_type!=$cfg_ePost['BigNewspaper']['PageType']['Cover']){
				$html .= $PageObj->Newspaper_InsideBanner && $PageObj->Newspaper_InsideBannerHideName? "":"<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>\n";
				$html .= $PageObj->Newspaper_InsideBanner && $PageObj->Newspaper_InsideBannerHidePageCat? "":"<span id=\"page_cat\" class=\"".$cfg_ePost['BigNewspaper']['Theme'][$PageObj->Page_Theme]['class']."\">".$PageObj->Page_Name."</span>\n";
			}
			else{
				$html .= $PageObj->Newspaper_CoverBanner && $PageObj->Newspaper_CoverBannerHideName? "":"<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>\n";
			}
			
			$html .= "						</h1>\n";
			$html .= "					</div>\n";
			$html .= "				</div>\n";
			$html .= "			</div>\n";
			$html .= "			<p class=\"spacer\"></p>\n";
			$html .= "			<div class=\"page_body\">\n";
			$html .= "				<div class=\"page_body_right\">\n";
			$html .= "					<div class=\"page_body_bg\">\n";
			$html .= "						<div class=\"page_body_content\">\n";
			$html .= "							<p class=\"spacer\"></p>\n";
			
			$article_ary	 = $PageObj->get_articles();
			$article_cfg_ary = $PageObj->get_page_TypeLayout_articles_ary();
			for($i=1;$i<=count($article_cfg_ary);$i++){
				$para_ary	 = array();
				$para_ary['PageID'] 			= $PageObj->PageID;
				$para_ary['Position'] 			= $i;
				$para_ary['ArticleObj']			= $article_ary[$i];
				$para_ary['ArticleCfg'] 		= $article_cfg_ary[$i];
				$para_ary['IsEditMode']		 	= $IsEditMode;
				$para_ary['ShowMoreBtn'] 		= false;
	     		$para_ary['RestrictionNameAry'] = $PageObj->get_article_restriction_name_ary($i);
				
				$html .= $this->gen_page_article_display($para_ary);
				$html .= $article_cfg_ary[$i]['needspacer']? "<p class=\"spacer\"></p>":"";
			}
			
			$html .= "							<p class=\"spacer\"></p>\n";
			$html .= "						</div>\n";
			$html .= "					</div>\n";
			$html .= "				</div>\n";
			$html .= "			</div>\n";
			$html .= "			<div class=\"page_bottom\">\n";
			$html .= "				<div class=\"page_bottom_right\">\n";
			$html .= "					<div class=\"page_bottom_bg\">&nbsp;</div>\n";
			$html .= "				</div>\n";
			$html .= "			</div>\n";
			$html .= "		</div>\n";
			$html .= "	</div>\n";
			$html .= $IsPortal? "</div>\n":"";
			$html .= "<p class=\"spacer\"></p>\n";
			
			return $html;
	    }	    
	    /* Function gen_page_article_display()
	     * Structure of $para_ary
	     * $para_ary['ArticleObj']			- Type: libepost_article		Usage: Article Object
	     * $para_ary['ArticleCfg'] 			- Type: Array					Usage: 
	     * $para_ary['IsEditMode']			- Type: Bool					Usage: Set the article is in edit mode or not
	     * $para_ary['ShowMoreBtn']			- Type: Bool					Usage: Set the article is shown with more button or not
	     * $para_ary['ThickboxHeight']		- Type: Integer					Usage: Thickbox height
	     * $para_ary['ThickboxWidth']		- Type: Integer					Usage: Thickbox width
	     */
	    function gen_big_page_article_layout($para_ary=array()){
	    	global $cfg_ePost, $Lang;
			
			$ArticleObj		 = $para_ary['ArticleObj'];
			$ArticleShelfObj = new libepost_articleshelf($ArticleObj->ArticleShelfID);
			$WriterObj 		 = new libepost_writer($ArticleShelfObj->WritingID);
			$ArticleCfg	 	 = $para_ary['ArticleCfg'];
			$IsEditMode		 = $para_ary['IsEditMode'];
			$ShowMoreBtn	 = $para_ary['ShowMoreBtn'];
			$ThickboxHeight  = $para_ary['ThickboxHeight'];
			$ThickboxWidth   = $para_ary['ThickboxWidth'];
			
			$level_code  = $WriterObj->WritingID? $WriterObj->LevelCode  : '';
			$module_code = $WriterObj->WritingID? $WriterObj->ModuleCode : '';
			$theme_code  = $WriterObj->WritingID? $WriterObj->ThemeCode  : '';
			$topic_code  = $WriterObj->WritingID? $WriterObj->TopicCode  : '';
			
			# Define $para_ary for function gen_article_display()
			$article_display_par['Title'] 						   	   = $ArticleShelfObj->Title;
			$article_display_par['Content'] 						   = nl2br($ArticleShelfObj->Content);
			$article_display_par['Attachment'] 					   	   = array();
			$article_display_par['Attachment']['Original']			   = $ArticleShelfObj->Attachment;
			$article_display_par['Attachment']['FilePath'] 		   	   = $ArticleShelfObj->format_attachment_path(false);
			$article_display_par['Attachment']['HTTPPath'] 		   	   = $ArticleShelfObj->format_attachment_path(true);
			$article_display_par['Attachment']['SkipResize'] 		   = false;
		 	$article_display_par['Attachment']['img_width'] 		   = $ArticleCfg['image_size'][$ArticleObj->Article_Size]['W'];
			$article_display_par['Attachment']['img_height'] 		   = $ArticleCfg['image_size'][$ArticleObj->Article_Size]['H'];
			$article_display_par['Attachment']['flvplayerID'] 		   = $ArticleShelfObj->WritingID."_".$ArticleObj->Article_Position;
			$article_display_par['Attachment']['flv_width'] 		   = $ArticleCfg['video_size'][$ArticleObj->Article_Size]['W'];
			$article_display_par['Attachment']['flv_height'] 		   = $ArticleCfg['video_size'][$ArticleObj->Article_Size]['H'];
			$article_display_par['Attachment']['IsSelectArticlePage']  = false;
			$article_display_par['Attachment']['Caption']			   = $ArticleShelfObj->Caption;
			$article_display_par['Attachment']['simple_layout']	   	   = true;
			
			$article_display_ary = $this->gen_article_display($article_display_par);
			$title		  = $article_display_ary['title'];
			$content	  = $article_display_ary['content'];
			$attachment   = $article_display_ary['attachment'];
			$attachment_w = $article_display_ary['attachment_w'];
			$attachment_h = $article_display_ary['attachment_h'];
			
			$ArticleTheme  = "log_header".($ArticleObj->Article_Type? '_'.$ArticleObj->Article_Type:'').($ArticleObj->Article_Theme? '_'.$ArticleObj->Article_Theme:'').($ArticleObj->Article_Color? '_'.$ArticleObj->Article_Color:'')." ";
			$ArticleTheme .= "log_header_".$ArticleObj->Article_Type." ";
			$ArticleTheme .= "log_header_".$ArticleObj->Article_Type."_left";
		
			$html .= "<!-- News start-->";
			$html .= "<div class=\"".$ArticleCfg['class']." $ArticleTheme ".($IsEditMode? "edit_mode ":"")."\">";
				
			if($IsEditMode){
				$html .= "<div class=\"edit_mode_tool\">";
				if($this->user_obj->isTeacherStaff() && $this->is_editor){
					$html .= "	  <a title=\"".$Lang['ePost']['Delete']."\" class=\"delete_log\" href=\"javascript:go_delete_article(".$ArticleObj->ArticleID.")\">".$Lang['ePost']['Delete']."</a>";
				}
				if($ArticleShelfObj->IsAppendix==$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['Yes']){
					$html .= "<a title=\"".$Lang['ePost']['Edit']."\" class=\"thickbox edit_text\" href=\"javascript:go_add_appendix(".$ArticleObj->PageID.",".$ArticleObj->Article_Position.")\">".$Lang['ePost']['Edit']."</a>";
				}
				else{
					$html .= "<a title=\"".$Lang['ePost']['Edit']."\" class=\"thickbox edit_text\" href=\"javascript:go_select_article(".$ArticleObj->PageID.",".$ArticleObj->Article_Position.")\">".$Lang['ePost']['Edit']."</a>";
				}
				
				$html .= "</div>";
				$html .= "<p class=\"spacer\"></p>";
			}
			
			$html .= "	<div class=\"news_log\">";
			$html .= "		<div class=\"log_header\">";
			$html .= "			<div class=\"log_header_top\">";
			$html .= "				<div class=\"log_header_top_right\">";
			$html .= "					<div class=\"log_header_top_bg\"></div>";
			$html .= "				</div>";
			$html .= "			</div>";
			$html .= "			<div class=\"log_header_body\">";
			$html .= "				<div class=\"log_header_body_right\">";
			$html .= "					<div class=\"log_header_body_bg\">";
			$html .= "						<h1 id=\"Article_title_".$ArticleObj->ArticleID."\">$title</h1>";
			$html .= "					</div>";
			$html .= "				</div>";
			$html .= "			</div>";
			$html .= "			<div class=\"log_header_bottom\">";
			$html .= "				<div class=\"log_header_bottom_right\">";
			$html .= "					<div class=\"log_header_bottom_bg\"></div>";
			$html .= "				</div>";
			$html .= "			</div>";
			$html .= "		</div>";
			$html .= "		<p class=\"spacer\"></p>";
			$html .= "		<div class=\"log_content_body\">";
			$html .= "			<div class=\"log_content\">";
			$html .= "				<p class=\"spacer\"></p>";
			
			if($ArticleObj->Article_Alignment!=$cfg_ePost['BigNewspaper']['Alignment']['Hide']){
				$html .= "			<table class=\"log_photo\" style=\"float:".array_search($ArticleObj->Article_Alignment, $cfg_ePost['BigNewspaper']['Alignment']).";width:".($attachment_w + 8)."px\">";
				$html .= "				<tr>";
				$html .= "					<td class=\"log_photo_file\">";
				$html .= "						$attachment";
				$html .= "					</td>";
				$html .= "				</tr>";
				$html .= $ArticleShelfObj->Caption? "<tr><td class=\"log_photo_desc\">".$ArticleShelfObj->Caption."</td></tr>":"";
				$html .= "			</table>";
				
				if(strtoupper(substr($ArticleShelfObj->Attachment, '-4'))=='.FLV' && $ArticleCfg[$ArticleObj->Article_Position]['flv_needspacer']){
					$html .= "		<p class=\"spacer\"></p>";
				}
			}
			
			$html .= "				<span class=\"log_content_text\">".$content."</span>";
			$html .= "			</div>";
			$html .= "			<p class=\"spacer\"></p>";
			
			# Define $para_ary for function gen_article_info_bar()
			$info_bar_par['ArticleObj'] 	 = $ArticleObj;
			$info_bar_par['ArticleShelfObj'] = $ArticleShelfObj;
			$info_bar_par['WriterObj'] 		 = $WriterObj;
			$info_bar_par['ShowMoreBtn'] 	 = $ShowMoreBtn;
			
			if($ArticleShelfObj->IsAppendix==$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['No']){
				$html .= $this->gen_article_info_bar($info_bar_par);
			}
			
			$html .= "			<p class=\"spacer\"></p>";
			$html .= "		</div>";
			$html .= "		<p class=\"spacer\"></p>";
			
			$html .= "	</div>";
			$html .= "</div>";
			$html .= "<!-- News end-->";
			
			return $html;
	    }
	    
	    function gen_small_cover_back_page_layout($PageObj, $IsCover=false){
			global $cfg_ePost;
			
			$html .= "<div class=\"newspaper_body\">";
			$html .= "	<div class=\"newspaper_page page_".($IsCover? "cover":"back").$PageObj->get_page_class_suffix()."\">";
			$html .= "		<div class=\"page_top\">";
			$html .= "			<div class=\"page_top_right\">";
			$html .= "				<div class=\"page_top_bg\">";
			$html .= "					<h1>";
			
			if($IsCover){
				$html .= "					<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>";
				$html .= "					<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>";
			}
			else{
				$html .= "					<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>";
				$html .= "					<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>";
				$html .= "					<span id=\"page_cat\" class=\"".$cfg_ePost['BigNewspaper']['Theme'][$PageObj->Page_Theme]['class']."\">".$PageObj->Page_Name."</span>";
			}
			
			$html .= "					</h1>";
			$html .= "				</div>";
			$html .= "			</div>";
			$html .= "		</div>";
			$html .= "		<p class=\"spacer\"></p>";
			$html .= "		<div class=\"page_body\">";
			$html .= "			<div class=\"page_body_right\">";
			$html .= "				<div class=\"page_body_bg\">";
			$html .= "					<div class=\"page_body_content\">";
			$html .= "						<p class=\"spacer\"></p>";
			
			$article_ary	 = $PageObj->get_articles();
			$article_cfg_ary = $PageObj->get_page_TypeLayout_articles_ary();
			for($i=1;$i<=count($article_cfg_ary);$i++){
				$para_ary	 					= array();
				$para_ary['PageID'] 			= $PageObj->PageID;
				$para_ary['Position'] 			= $i;
				$para_ary['ArticleObj']			= $article_ary[$i];
				$para_ary['ArticleCfg'] 		= $article_cfg_ary[$i];
				$para_ary['IsEditMode']		 	= false;
				$para_ary['ShowMoreBtn'] 		= true;
	     		$para_ary['RestrictionNameAry'] = $PageObj->get_article_restriction_name_ary($i);
				
				$html .= $this->gen_page_article_display($para_ary);
				$html .= $article_cfg_ary[$i]['needspacer']? "<p class=\"spacer\"></p>":"";
			}
			
			$html .= "						<p class=\"spacer\"></p>";
			$html .= "					</div>";
			$html .= "				</div>";
			$html .= "			</div>";
			$html .= "		</div>";
			$html .= "		<div class=\"page_bottom\">";
			$html .= "			<div class=\"page_bottom_right\">";
			$html .= "				<div class=\"page_bottom_bg\">&nbsp;</div>";
			$html .= "			</div>";
			$html .= "		</div>";
			$html .= "	</div>";
			$html .= "</div>";
			
			return $html;
		}
	    
	    function gen_small_inside_page_layout($PageObj, $IsLeft=true){
			global $cfg_ePost;
			
			if($IsLeft){
				$html .= "<div class=\"newspaper_body\">";
				$html .= "	<div class=\"newspaper_page page_inside\">";
				$html .= "		<div class=\"page_inside_top\">";
				$html .= "			<div class=\"page_inside_top_right\">";
				$html .= "				<div class=\"page_inside_top_bg\"></div>";
				$html .= "			</div>";
				$html .= "		</div>";
				$html .= "		<p class=\"spacer\"></p>";
				$html .= "		<div class=\"page_inside_body\">";
				$html .= "			<div class=\"page_inside_body_right\">";
				$html .= "				<div class=\"page_inside_body_bg\">";
				$html .= "					<div class=\"page_inside_body_content\">";
			}
			
			$html .= "							<div class=\"newspaper_page page_".($IsLeft? "left":"right").$PageObj->get_page_class_suffix()."\">";
			$html .= "								<div class=\"page_top\">";
			$html .= "									<div class=\"page_top_right\">";
			$html .= "										<div class=\"page_top_bg\">";
			$html .= "											<h1>";
			$html .= "												<span class=\"issue_title\">".$PageObj->Newspaper_Name."</span>";
			$html .= "												<span class=\"newspaper_title\">".$PageObj->Newspaper_Title."</span>";
			$html .= "												<span id=\"page_cat\" class=\"".$cfg_ePost['BigNewspaper']['Theme'][$PageObj->Page_Theme]['class']."\">".$PageObj->Page_Name."</span>";
			$html .= "											</h1>";
			$html .= "										</div>";
			$html .= "									</div>";
			$html .= "								</div>";
			$html .= "								<p class=\"spacer\"></p>";
			$html .= "								<div class=\"page_body\">";
			$html .= "									<div class=\"page_body_right\">";
			$html .= "										<div class=\"page_body_bg\">";
			$html .= "											<div class=\"page_body_content\">";
			$html .= "												<p class=\"spacer\"></p>";
			
			$article_ary	 = $PageObj->get_articles();
			$article_cfg_ary = $PageObj->get_page_TypeLayout_articles_ary();
			for($i=1;$i<=count($article_cfg_ary);$i++){
				$para_ary 						= array();
				$para_ary['PageID'] 			= $PageObj->PageID;
				$para_ary['Position'] 			= $i;
				$para_ary['ArticleObj']			= $article_ary[$i];
				$para_ary['ArticleCfg'] 		= $article_cfg_ary[$i];
				$para_ary['IsEditMode']		 	= false;
				$para_ary['ShowMoreBtn'] 		= true;
	     		$para_ary['RestrictionNameAry'] = $PageObj->get_article_restriction_name_ary($i);
				
				$html .= $this->gen_page_article_display($para_ary);
				$html .= $article_cfg_ary[$i]['needspacer']? "<p class=\"spacer\"></p>":"";
			}
			
			
			$html .= "												<p class=\"spacer\"></p>";
			$html .= "											</div>";
			$html .= "										</div>";
			$html .= "									</div>";
			$html .= "								</div>";
			$html .= "								<div class=\"page_bottom\">";
			$html .= "									<div class=\"page_bottom_right\">";
			$html .= "										<div class=\"page_bottom_bg\">&nbsp;</div>";
			$html .= "									</div>";
			$html .= "								</div>";
			$html .= "							</div>";
			
			if(!$IsLeft){
				$html .= "						<p class=\"spacer\"></p>";
				$html .= "					</div>";
				$html .= "				</div>";
				$html .= "			</div>";
				$html .= "		</div>";
				$html .= "		<div class=\"page_inside_bottom\">";
				$html .= "			<div class=\"page_inside_bottom_right\">";
				$html .= "				<div class=\"page_inside_bottom_bg\">&nbsp;</div>";
				$html .= "			</div>";
				$html .= "		</div>";
				$html .= "	</div>";
				$html .= "</div>";
			}
			
			return $html;
		}
	    
	    /* Function gen_page_article_display()
	     * Structure of $para_ary
	     * $para_ary['ArticleObj']			- Type: libepost_article		Usage: Article Object
	     * $para_ary['ArticleShelfObj']		- Type: libepost_articleshelf	Usage: Article Shelf Object
	     * $para_ary['WriterObj']			- Type: libepost_writer			Usage: Writer Object
	     * $para_ary['ShowMoreBtn']			- Type: Bool					Usage: Set the article is shown with more button or not
	     */
	    function gen_article_info_bar($para_ary=array()){
	    	global $cfg_ePost, $Lang;
	    	
	    	$ArticleObj 	 = $para_ary['ArticleObj'];
	    	$ArticleShelfObj = $para_ary['ArticleShelfObj'];
	    	$WriterObj  	 = $para_ary['WriterObj'];
	    	$ShowMoreBtn     = $para_ary['ShowMoreBtn'];
	    	
			$no_of_ppl_enjoy = count($WriterObj->ReadFlag); 
			$TeacherComment  = $WriterObj->TeacherComment;
			$UserName 		 = $WriterObj->UserName;
			$EnglishName	 = $WriterObj->EnglishName;
			$ClassName		 = $WriterObj->ClassName;
			$ClassNumber	 = $WriterObj->ClassNumber;
			/*
			$html .= "<style>";
			$html .= "	a.enjoy_btn, a.teacher_comment_btn{color: #0044FF}";
			$html .= "</style>";
			*/
			$html .= "<div class=\"log_info\">";
			
			if($ShowMoreBtn){
				$thickbox_h = 500;
				$thickbox_w = 850;
				
				$html .= "<p class=\"spacer\"></p>";
				$html .= "<a class=\"thickbox log_content_more\" title=\"View Article\" href=\"view_newspaper_article.php?ArticleID=".$ArticleObj->ArticleID."&TB_iframe=true&width=$thickbox_w&height=$thickbox_h\">(more..)</a>";
				$html .= "<p class=\"spacer\"></p>";
			}
			
			if($ArticleObj->Article_DisplayAuthor==$cfg_ePost['BigNewspaper']['Article_DisplayAuthor']['Yes']){
				$html .= "	<div class=\"log_author\">".($EnglishName? ($Lang['ePost']['By']." $UserName".($ClassName? " ($ClassName".($ClassNumber? " - $ClassNumber":"").")":"")."") : "")."</div>";
			}
			
			$html .= "	<div class=\"log_enjoy_comment_tool print_hide\">";
			$html .= "		<a class=\"enjoy_btn\" title=\"".$Lang['ePost']['ClickToEnjoy']."\" href=\"javascript:enjoy_article(".$WriterObj->RecordID.")\">".$Lang['ePost']['Enjoy']."</a>";
			$html .= "		<a name=\"enjoy_ppl_link_".$WriterObj->RecordID."\" onclick=\"show_enjoy_list(".$ArticleObj->PageID.", ".$ArticleObj->Article_Position.", ".$WriterObj->RecordID.", 'show')\" title=\"".($no_of_ppl_enjoy? "$no_of_ppl_enjoy ".($no_of_ppl_enjoy>1? $Lang['ePost']['PeopleEnjoy']:$Lang['ePost']['PersonEnjoy']) : $Lang['ePost']['NoOneEnjoyYet'])."\" href=\"javascript:void(0)\">($no_of_ppl_enjoy)</a>";
			
			if($TeacherComment){
				$html .= "	<span> | </span>";
				$html .= "	<a class=\"teacher_comment_btn\" onclick=\"show_teacher_comment(".$ArticleObj->PageID.", ".$ArticleObj->Article_Position.", 'show')\" href=\"javascript:void(0)\">".$Lang['ePost']['Comment']."</a>";
				$html .= "	<p class=\"spacer\"></p>";
				$html .= "	<div id=\"comment_".$ArticleObj->PageID."_".$ArticleObj->Article_Position."\" class=\"enjoy_ppl_board\" style=\"padding-left:75px\">";
				$html .= "		<div class=\"enjoy_ppl_board_top_left\">";
				$html .= "			<div class=\"enjoy_ppl_board_top_right\">";
				$html .= "				<div class=\"enjoy_ppl_board_top_bg\"></div>";
				$html .= "			</div>";
				$html .= "		</div>";
				$html .= "		<div class=\"enjoy_ppl_board_left\">";
				$html .= "			<div class=\"enjoy_ppl_board_right\">";
				$html .= "				<div class=\"enjoy_ppl_board_bg\">";
				$html .= "					<a onclick=\"show_teacher_comment(".$ArticleObj->PageID.", ".$ArticleObj->Article_Position.", 'hide')\" class=\"btn_close\" href=\"javascript:void(0)\">".$Lang['ePost']['Close']."</a><br>";
				$html .= "					<div class=\"enjoy_ppl_list\">$TeacherComment</div>";
				$html .= "				</div>";
				$html .= "			</div>";
				$html .= "		</div>";
				$html .= "		<div class=\"enjoy_ppl_board_bottom_left\">";
				$html .= "			<div class=\"enjoy_ppl_board_bottom_right\">";
				$html .= "				<div class=\"enjoy_ppl_board_bottom_bg\"></div>";
				$html .= "			</div>";
				$html .= "		</div>";
				$html .= "	</div>";
			}
			$html .= "		<p class=\"spacer\"></p>";
			$html .= "		<div id=\"enjoy_ppl_".$ArticleObj->PageID."_".$ArticleObj->Article_Position."\" name=\"enjoy_ppl_board_".$WriterObj->RecordID."\" class=\"enjoy_ppl_board\"></div>";
			$html .= "	</div>";
			$html .= "</div>";
			
			return $html;
		}
	    
	    /* Function gen_page_article_display()
	     * Structure of $para_ary
	     * $para_ary['PageID'] 				- Type: Integer				Usage: PageID of libepost_page object
	     * $para_ary['Position'] 			- Type: String 				Usage: Article Title
	     * $para_ary['ArticleObj']			- Type: libepost_article 	Usage: Article Object
	     * $para_ary['ArticleCfg'] 			- Type: Array				Usage: 
	     * $para_ary['IsEditMode']			- Type: Bool				Usage: Set the article is in edit mode or not
	     * $para_ary['ShowMoreBtn']			- Type: Bool				Usage: Set the article is shown with more button or not
	     * $para_ary['RestrictionNameAry'] 	- Type: Array				Usage: Restriction Name Array of Position of Page Object
	     * */
		function gen_page_article_display($para_ary=array()){
			global $Lang;
			
			$thickbox_h = 550;
			$thickbox_w = 850;
			
			if(!is_null($para_ary['ArticleObj']) && is_object($para_ary['ArticleObj'])){
				$article_layout_par['ArticleObj']	   = $para_ary['ArticleObj'];
				$article_layout_par['ArticleCfg']	   = $para_ary['ArticleCfg']; 
				$article_layout_par['IsEditMode']	   = $para_ary['IsEditMode'];
				$article_layout_par['ShowMoreBtn']	   = $para_ary['ShowMoreBtn'];
				$article_layout_par['ThickboxHeight']  = $thickbox_h;
				$article_layout_par['ThickboxWidth']   = $thickbox_w;
				
				$html .= $this->gen_big_page_article_layout($article_layout_par);
			}
			else{
				$html .= "<!-- News start-->";
				$html .= "<div class=\"".$para_ary['ArticleCfg']['class']." ".($para_ary['IsEditMode']? "edit_mode ":"")."\" ".($para_ary['IsEditMode']? "":"style=\"visibility:hidden\"").">";
				
				if($para_ary['IsEditMode']){
					$html .= "<div class=\"edit_mode_tool\">";
					if($this->user_obj->isTeacherStaff() && $this->is_editor){
						$html .= "	<a title=\"".$Lang['ePost']['Create']."\" class=\"thickbox\" href=\"javascript:go_add_appendix(".$para_ary['PageID'].",".$para_ary['Position'].")\">".$Lang['ePost']['Create']."</a>";
						$html .= "	<a title=\"".$Lang['ePost']['Select']."\" class=\"thickbox\" href=\"javascript:go_select_article(".$para_ary['PageID'].",".$para_ary['Position'].")\">".$Lang['ePost']['Select']."</a>";
					}
					$html .= "</div>";
					$html .= "<p class=\"spacer\"></p>";
				}
				
				$html .= "	<div class=\"news_log\">";
				$html .= "		<span class=\"log_no_content\">";
				$html .= "			".$Lang['ePost']['NoArticleSelectedYet']."<br>";
				$html .= "			<br>";
				
				/* Temporary hide by Thomas on 2012-09-28
				$html .= "			Support :<br>";
				$html .= "			<strong>".implode("</strong>, <strong>", $para_ary['RestrictionNameAry'])."</strong><br/>";
				$html .= "			<br>";
				*/
				
				$html .= "		</span>";
				$html .= "		<p class=\"spacer\"></p>";
				$html .= "	</div>";
				$html .= "</div>";
				$html .= "<!-- News end-->";
			}
			
			return $html;
		}
		
		/* Function gen_image_video_size_option()
	     * Structure of $para_ary
		 * $para_ary['Type']
		 * $para_ary['ArticleCfg']
		 * $para_ary['Name']
		 * $para_ary['DefaultSize']
		 * $para_ary['DefaultWidth']
		 * $para_ary['DefaultHeight']
		 * */
		function gen_image_video_size_option($para_ary){
			global $Lang;
			
			switch($para_ary['Type']){
				case 'IMG': $size_ary = $para_ary['ArticleCfg']['image_size']; break;
				case 'FLV': $size_ary = $para_ary['ArticleCfg']['video_size']; break;
			}
			
			if(is_array($size_ary) && count($size_ary)){
				foreach($size_ary as $size=>$size_detail_ary){
					$html .= "<input type='radio' id='".$para_ary['Name']."_$size' name='".$para_ary['Name']."' value='$size' ".(($size==$para_ary['DefaultSize'] || ($para_ary['DefaultWidth']==$size_detail_ary['W'] && $para_ary['DefaultHeight']==$size_detail_ary['H']))? 'checked':'')."/>\n";
					$html .= "<label for='".$para_ary['Name']."_$size'>".$Lang['ePost'][$size]." (".$size_detail_ary['W']." x ".$size_detail_ary['H'].")</label>\n";
				}
			}
			
			return $html;
		}
		
		function gen_folder_issue_list($FolderObjs){
			global $cfg_ePost, $Lang;
			
			if(count($FolderObjs->Folders)){
				$html  = "<ul>";
				for($i=0;$i<count($FolderObjs->Folders);$i++){
					$FolderObj = $FolderObjs->Folders[$i];
					$html .= "<li id='Folder_".$FolderObj->FolderID."'>";
					$html .= "<a href='javascript:openFolder(".$FolderObj->FolderID.")'>".($FolderObj->Folder_Title==$cfg_ePost['BigNewspaper']['DefaultFolderTitle']? $Lang['ePost']['PublicFolder']:$FolderObj->Folder_Title)."</a>";
					if(count($FolderObj->Folder_NewspaperIDs)>0){
						$Newspaper_ary = $FolderObj->get_newspapers();
						$html .= "<ul style='display:none'>";
						for($j=0;$j<count($Newspaper_ary);$j++){
							$NewspaperObj = $Newspaper_ary[$j];
							
							if(count($NewspaperObj->Newspaper_PageIDs)>0)
								$html .= "<li><a href='javascript:openNewspaper(".$NewspaperObj->NewspaperID.", 0)'>".$NewspaperObj->Newspaper_Title." - ".$NewspaperObj->Newspaper_Name."</a></li>";
						}
						$html .= "</ul>";
					}
					$html .= "</li>";
				}
				$html .= "</ul>";
			}
			
			return $html? $html : "<span class='intro'>".$Lang['ePost']['NoIssuesFound']."</span>";
		}
		
		function gen_issue_list($FolderObjs, $IsPublicList=false){
			global $Lang;
			
			if(count($FolderObjs->Folders)){
				for($i=0;$i<count($FolderObjs->Folders);$i++){
					$FolderObj = $FolderObjs->Folders[$i];
					if(count($FolderObj->Folder_NewspaperIDs)>0){
						$Newspaper_ary = $FolderObj->get_newspapers();
						for($j=0;$j<count($Newspaper_ary);$j++){
							$NewspaperObj = $Newspaper_ary[$j];
							if((!$IsPublicList && $NewspaperObj->is_published()) || ($IsPublicList && $NewspaperObj->is_open_to_public())){
								$encoded_qstr = base64_encode("NewspaperID=".$NewspaperObj->NewspaperID);
								$html .= "<li><a href='javascript:openNewspaper(\"$encoded_qstr\")'>".$NewspaperObj->Newspaper_Title." - ".$NewspaperObj->Newspaper_Name."</a></li>";
							}
						}
						$html = $html? "<ul class='issue_only'>".$html."</ul>" : "";
					}
				}
			}
			
			return $html? $html : "<span class='intro'>".$Lang['ePost']['NoIssuesFound']."</span>";
		}
		
		function get_page_display($NewspaperObj, $current_page_order){
			$Newspaper_Pages_keys  = array_keys($NewspaperObj->Newspaper_PageIDs);
			$Current_Pages_Display = array_search($current_page_order, $Newspaper_Pages_keys) + 1; // array count from 0, but display count from 1
		
			return $Current_Pages_Display;
		}
		
		/*
		 * $RefPageOrder : Define the Page Order of the Referencing Page
		 * $Displacement : Define the next / previous number of Page that you want the Page Order
		 * 				   e.g. $Displacement = 0 means the current page
		 * 				  		$Displacement > 0 means the the next page
		 * 						$Displacement < 0 means the the previous page
		 * */
		function get_prev_next_page_order($NewspaperObj, $RefPageOrder, $Displacement=0){
			$Newspaper_Pages_keys = array_keys($NewspaperObj->Newspaper_PageIDs);
			$RefPages_keys_ptr 	  = array_search($RefPageOrder, $Newspaper_Pages_keys);
			
			if(($RefPages_keys_ptr + $Displacement) >= 0 && ($RefPages_keys_ptr + $Displacement) < count($Newspaper_Pages_keys)){
				$result_PageOrder = $Newspaper_Pages_keys[$RefPages_keys_ptr + $Displacement];
			}
			else{
				$result_PageOrder = -1;
			}
			
			return $result_PageOrder;
		}
		
		function gen_view_newspaper_bottom_bar($NewspaperObj, $current_page, $IsZoomOut=false){
			global $Lang;
			 
			$prev_PageOrder = $this->get_prev_next_page_order($NewspaperObj, $current_page, ($IsZoomOut && $this->get_page_display($NewspaperObj, $current_page)!=2? -2:-1));
			$next_PageOrder = $this->get_prev_next_page_order($NewspaperObj, $current_page, ($IsZoomOut && $this->get_page_display($NewspaperObj, $current_page)!=1?  2: 1));
			
			if($prev_PageOrder!=-1){
				$prev_btn = "<a class=\"page_prev\" href=\"javascript:go_page($prev_PageOrder)\">".$Lang['ePost']['Previous']."</a>";
			}
			
			if($next_PageOrder!=-1){
				$next_btn = "<a class=\"page_next\" href=\"javascript:go_page($next_PageOrder)\">".$Lang['ePost']['Next']."</a>";
			}
			
			$html .= "<script>";
			$html .= "	function newspaper_zoom(PageOrder, IsZoomOut){";
			$html .= "		var obj = document.form1;";
			$html .= "		obj.PageOrder.value = PageOrder;";
			$html .= "		obj.IsZoomOut.value = IsZoomOut;";
			$html .= "		obj.action = 'view_newspaper.php';";
			$html .= "		obj.submit();";
			$html .= "	}";
			
			$html .= "	function go_page(PageOrder){";
			$html .= "		var obj = document.form1;";
			$html .= "		obj.PageOrder.value = PageOrder;";
			$html .= "		obj.action = 'view_newspaper.php';";
			$html .= "		obj.submit();";
			$html .= "	}";
			$html .= "</script>";
			
			$html .= "<div class=\"page_control_panel print_hide\" style=\"margin-top:45px;\">";
			$html .= "	<div class=\"page_control_panel_right\">";
			$html .= "		<div class=\"page_control_panel_bg\">";
			$html .= "			<div class=\"page_num\" style=\"width:225px\">"; // Temporary add the width by Thomas 2012-09-28
			$html .= "				<span>".$Lang['ePost']['Page'][0]." </span>";
			$html .= "				<span class=\"page_select\">".$this->gen_view_newspaper_page_drop_down_list($NewspaperObj, $IsZoomOut, 'page_select', 'page_select', $current_page, 'onchange="go_page(this.value)" style="width:65px"')."</span>";
			$html .= "				<span>/ ".count($NewspaperObj->Newspaper_PageIDs)." ".$Lang['ePost']['Page'][1]."</span>";
			
			/* Temporary hide by Thomas on 2012-09-28
			$html .= "				<em> | </em>";
			$html .= "				<a class=\"".($IsZoomOut? "zoom_in":"zoom_out")."\" href=\"javascript:newspaper_zoom($current_page, ".($IsZoomOut? 0:1).")\"> ".($IsZoomOut? "Zoom in":"Zoom out")."</a>";
			*/
			
			$html .= "				<em> | </em>";
			$html .= "				<a class=\"printer\" href=\"javascript:void(0)\" onclick=\"window.print()\">".$Lang['ePost']['Print']."</a>";
			$html .= "			</div>";
			$html .= "			<p class=\"spacer\"></p>";
			$html .= $prev_btn;
			$html .= $next_btn;
			$html .= "		</div>";
			$html .= "	</div>";
			$html .= "</div>";
			
			return $html;
		}
		
		function gen_student_editor_selection($StudentEditorsObj, $StudentEditorSelected_ary=array(), $IsReadOnly=false){
			global $Lang;
			
			$StudentEditorSelected_UserID_ary = array();
			
			if(!$IsReadOnly){
				$html .= "<script>";
				$html .= "	$(document).ready(function(){\n";
				$html .= "		$('#AddAll').click(function(){\n";
				$html .= "			$('#StudentEditor option').each(function(){\n";
				$html .= "				$(this).attr('selected','selected');\n";
				$html .= "			});\n";
				$html .= "			MoveSelectedOptions('StudentEditor', 'StudentEditorSelected');\n";
				$html .= "		});\n";
				$html .= "		$('#Add').click(function(){\n";
				$html .= "			MoveSelectedOptions('StudentEditor', 'StudentEditorSelected');\n";
				$html .= "		});\n";
				$html .= "		$('#Remove').click(function(){\n";
				$html .= "			MoveSelectedOptions('StudentEditorSelected', 'StudentEditor');\n";
				$html .= "		});\n";
				$html .= "		$('#RemoveAll').click(function(){\n";
				$html .= "			$('#StudentEditorSelected option').each(function(){\n";
				$html .= "				$(this).attr('selected','selected');\n";
				$html .= "			});\n";
				$html .= "			MoveSelectedOptions('StudentEditorSelected', 'StudentEditor');\n";
				$html .= "		});\n";
				$html .= "	});\n";
				$html .= "	function MoveSelectedOptions(from_select_id, to_select_id){\n";
				$html .= "		$('#'+from_select_id+' option:selected').each(function(i){\n";
				$html .= "			var student_id   = $(this).val();\n";
				$html .= "			var student_name = $(this).text();\n";
				$html .= "			$('#'+to_select_id).append($('<option></option>').attr('value',student_id).text(student_name));\n";
				$html .= "			$(this).remove();\n";
				$html .= "		});";
				$html .= "		Reorder_Selection_List(from_select_id);\n";
				$html .= "		Reorder_Selection_List(to_select_id);\n";
				$html .= "	}\n";
				$html .= "	function Reorder_Selection_List(selectId) {\n";
				$html .= "		var selectList = document.getElementById(selectId);\n";
				$html .= "		for (var i = 0; i < selectList.length; i++) {\n";
				$html .= "			for (var j=i; j < selectList.length; j++) {\n";
				$html .= "				if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()){\n";
				$html .= "					var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);\n";
				$html .= "					var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);\n";
				$html .= "					selectList.options[i] = tempOption2;\n";
				$html .= "					selectList.options[j] = tempOption1;\n";
				$html .= "				}\n";
				$html .= "			}\n";
				$html .= "		}\n";
				$html .= "	}\n";
				$html .= "</script>";
			}
			
			$html .= "<table width=\"90%\" cellspacing=\"0\" cellpadding=\"5\" border=\"0\">";
			$html .= "	<tr>";
			$html .= "		<td width=\"50%\" bgcolor=\"#EFFEE2\" class=\"steptitletext\">".$Lang['ePost']['StudentSelected']."</td>";
			$html .= "		<td width=\"40\">&nbsp;</td>";
			$html .= "		<td width=\"50%\" ".(!$IsReadOnly? "bgcolor=\"#EEEEEE\"":"")."></td>";
			$html .= "	</tr>";
			$html .= "	<tr>";
			$html .= "		<td bgcolor=\"#EFFEE2\">";
			$html .= "			<select multiple=\"true\" size=\"10\" id=\"StudentEditorSelected\" name=\"StudentEditorSelected[]\" style=\"width: 100%;\">";
			
			for($i=0;$i<count($StudentEditorSelected_ary);$i++){
				$StudentEditorSelected_UserID_ary[] = $StudentEditorSelected_ary[$i]['UserID'];
				
				$EditorUserID 	   = $StudentEditorSelected_ary[$i]['UserID'];
				$EditorUserName    = $StudentEditorSelected_ary[$i]['UserName'];
				$EditorEnglishName = $StudentEditorSelected_ary[$i]['EnglishName'];
				$EditorClassName   = $StudentEditorSelected_ary[$i]['ClassName'];
				$EditorClassNumber = $StudentEditorSelected_ary[$i]['ClassNumber'];
				
				$html .= "<option value=\"$EditorUserID\">".($EditorClassName? $EditorClassName.($EditorClassNumber? "-".$EditorClassNumber:""):"").$EditorUserName."</option>";
			}
			
			$html .= "			</select>";
			$html .= " 		</td>";
			$html .= "		<td>";
			
			if(!$IsReadOnly){
				$html .= "		<input type=\"button\" title=\"".$Lang['ePost']['AddAll']."\" style=\"width:40px;\" value=\"&lt;&lt;\"  class=\"formsubbutton\" id=\"AddAll\"><br>";
				$html .= "		<input type=\"button\" title=\"".$Lang['ePost']['AddSelected']."\" style=\"width:40px;\" value=\"&lt;\"  class=\"formsubbutton\" id=\"Add\"><br><br>";
				$html .= "		<input type=\"button\" title=\"".$Lang['ePost']['RemoveSelected']."\" style=\"width:40px;\" value=\"&gt;\"  class=\"formsubbutton\" id=\"Remove\"><br>";
				$html .= "		<input type=\"button\" title=\"".$Lang['ePost']['RemoveAll']."\" style=\"width:40px;\" value=\"&gt;&gt;\"  class=\"formsubbutton\" id=\"RemoveAll\">";
			}
			else{
				$html .= "		&nbsp;";
			}
			
			$html .= "		</td>";
			$html .= "		<td ".(!$IsReadOnly? "bgcolor=\"#EEEEEE\"":"")." align=\"center\">";
			
			if(!$IsReadOnly){
				$html .= "		<div id=\"StudentEditorList\">";
				$html .= "			<select multiple=\"true\" size=\"10\" id=\"StudentEditor\" name=\"StudentEditor\" style=\"width: 100%;\">";
			
				for($i=0;$i<count($StudentEditorsObj->StudentEditors);$i++){
					if(!in_array($StudentEditorsObj->StudentEditors[$i]->UserID, $StudentEditorSelected_UserID_ary)){
						$EditorUserID 	   = $StudentEditorsObj->StudentEditors[$i]->UserID;
						$EditorUserName    = $StudentEditorsObj->StudentEditors[$i]->UserName;
						$EditorEnglishName = $StudentEditorsObj->StudentEditors[$i]->EnglishName;
						$EditorClassName   = $StudentEditorsObj->StudentEditors[$i]->ClassName;
						$EditorClassNumber = $StudentEditorsObj->StudentEditors[$i]->ClassNumber;
						
						$html .= "		<option value=\"$EditorUserID\">".($EditorClassName? $EditorClassName.($EditorClassNumber? "-".$EditorClassNumber:""):"").$EditorUserName."</option>";
					}
				}
			
				$html .= "			</select>";
				$html .= "		</div>";
			}
			else{
				$html .= "		&nbsp;";
			}
			
			$html .= "		</td>";
			$html .= "	</tr>";
			$html .= "</table>";
			
			return $html;
		}
		
		function gen_publish_status_drop_down_list($id='', $name='', $default_choice=0, $other_attr='', $EnableDefaultOnly=false){
			global $cfg_ePost, $Lang;
			
			$html  = "<select id=\"$id\" name=\"$name\" $other_attr>";
			
			$IsSelected = $default_choice==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft'];
			$IsDisabled = $EnableDefaultOnly && !$IsSelected;
			$html .= "	  <option value=\"".$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Draft']."\" ".($IsSelected? "selected":"")." ".($IsDisabled?"disabled":"").">".$Lang['ePost']['Filter']['Draft']."</option>";
			
			$IsSelected = $default_choice==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['WaitApproval'];
			$IsDisabled = $EnableDefaultOnly && !$IsSelected;
			$html .= "	  <option value=\"".$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['WaitApproval']."\" ".($IsSelected? "selected":"")." ".($IsDisabled?"disabled":"").">".$Lang['ePost']['Filter']['WaitingApproval']."</option>";
			
			if($this->is_publish_manager || $default_choice==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']){
				$IsSelected = $default_choice==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published'];
				$IsDisabled = $EnableDefaultOnly && !$IsSelected;
				$html .= "<option value=\"".$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']."\" ".($IsSelected? "selected":"")." ".($IsDisabled?"disabled":"").">".$Lang['ePost']['Filter']['Published']."</option>";
			}
			
			$html .= "</select>";
			
			return $html;
		}
		function get_post_ids($FolderObjs,$IsPublicList=false){
			$ids = array();
			if(count($FolderObjs->Folders)){
				for($i=0;$i<count($FolderObjs->Folders);$i++){
					$FolderObj = $FolderObjs->Folders[$i];
					if(count($FolderObj->Folder_NewspaperIDs)>0){
						$Newspaper_ary = $FolderObj->get_newspapers();
						for($j=0;$j<count($Newspaper_ary);$j++){
							$NewspaperObj = $Newspaper_ary[$j];
							if((!$IsPublicList && $NewspaperObj->is_published()) || ($IsPublicList && $NewspaperObj->is_open_to_public())){
								array_push($ids,$NewspaperObj->NewspaperID);
							}
						}
					}
				}
			}
			return $ids;
		}
		function gen_post_list($FolderObjs,$SearchArr=array(),$IsPublicList=false){
			$NewsArr = array();
			$n = 0;
			if(count($FolderObjs->Folders)){
				$this->post_ids = $this->get_post_ids($FolderObjs,$IsPublicList);
				
				list($result_total,$Newspaper_ary) = $this->get_newspapers_ary($SearchArr);
				
				
				for($j=0;$j<count($Newspaper_ary);$j++){
					$NewspaperObj = new libepost_newspaper(0, $Newspaper_ary[$j]);
					if($this->is_editor ||(!$IsPublicList && $NewspaperObj->is_published()) || ($IsPublicList && $NewspaperObj->is_open_to_public())){
						if($this->is_editor){ 
							$NewsArr[$n]['link'] = 'javascript:openNewspaper('.$NewspaperObj->NewspaperID.',0)';
						}else{
							$NewsArr[$n]['link'] = 'javascript:openNewspaper(\''.base64_encode("NewspaperID=".$NewspaperObj->NewspaperID).'\')';;
						}
						$NewsArr[$n]['title'] = $NewspaperObj->Newspaper_Title;
						$NewsArr[$n]['name'] = $NewspaperObj->Newspaper_Name;
						$NewsArr[$n]['date'] = substr($NewspaperObj->Newspaper_IssueDate,0,10);
						$n++;
					}
				}
					
				
			}
			
			return array($result_total,$NewsArr);
		}
				
		function get_newspapers_ary($SearchArr=array()){
		
			global $cfg_ePost;
			$result_ary = array();
			if(count($this->post_ids) > 0){
				$PageIDSeparator = '|=|';
				switch($SearchArr['sortby']){
					case 'title':$cond=' ORDER BY Newspaper_Title';break;
					case 'name':$cond=' ORDER BY Newspaper_Name';break;
					case 'date':$cond=' ORDER BY Newspaper_IssueDate';break;
					default:$cond='';
				}
				$cond .= ($SearchArr['order']=='asc')?'':' DESC';
				if(!empty($SearchArr['keyword']) && $SearchArr['keyword']!=''){
					$keyword_search = "
						AND(
								en.Title LIKE '%".$SearchArr['keyword']."%'
							OR en.Name LIKE '%".$SearchArr['keyword']."%'
							OR en.IssueDate LIKE '%".$SearchArr['keyword']."%'
						)
					";
				}
				$limit = $SearchArr['offset']*$SearchArr['amount'].','.$SearchArr['amount'];
				$sql = "SELECT
							en.NewspaperID,
							en.FolderID,
							en.Title AS Newspaper_Title,
							en.Name AS Newspaper_Name,
							en.IssueDate AS Newspaper_IssueDate,
							en.OpenToPublic AS Newspaper_OpenToPublic,
							en.Skin AS Newspaper_Skin,
							en.SchoolLogo AS Newspaper_SchoolLogo,
							en.CoverBanner AS Newspaper_CoverBanner,
							en.CoverBannerHideTitle AS Newspaper_CoverBannerHideTitle,
							en.CoverBannerHideName AS Newspaper_CoverBannerHideName,
							en.InsideBanner AS Newspaper_InsideBanner,
							en.InsideBannerHideTitle AS Newspaper_InsideBannerHideTitle,
							en.InsideBannerHideName AS Newspaper_InsideBannerHideName,
							en.InsideBannerHidePageCat AS Newspaper_InsideBannerHidePageCat,
							en.NoOfView AS Newspaper_NoOfView,
							en.PublishStatus AS Newspaper_PublishStatus,
							en.Status AS Newspaper_Status,
							en.InputDate AS Newspaper_InputDate,
							en.ModifiedBy AS Newspaper_ModifiedBy,
							en.ModifiedDate AS Newspaper_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Newspaper_ModifiedByUser,
							iu.EnglishName AS Newspaper_EnglishName,
							iu.ClassName AS Newspaper_ClassName,
							iu.ClassNumber AS Newspaper_ClassNumber,
							GROUP_CONCAT(DISTINCT ep.PageID ORDER BY ep.PageOrder SEPARATOR '$PageIDSeparator') AS Newspaper_PageIDs
						FROM
							EPOST_NEWSPAPER AS en INNER JOIN
							INTRANET_USER AS iu ON en.ModifiedBy = iu.UserID LEFT JOIN
							EPOST_NEWSPAPER_PAGE AS ep ON en.NewspaperID = ep.NewspaperID AND ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
						WHERE
							en.NewspaperID IN (".implode(',', $this->post_ids).") AND
							en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."
							".$keyword_search."
						GROUP BY
							en.NewspaperID
							".$cond."
						 LIMIT 
							".$limit."
						";
				$result_ary = $this->returnArray($sql); 
				$sql = "SELECT
							en.NewspaperID,
							en.FolderID,
							en.Title AS Newspaper_Title,
							en.Name AS Newspaper_Name,
							en.IssueDate AS Newspaper_IssueDate,
							en.OpenToPublic AS Newspaper_OpenToPublic,
							en.Skin AS Newspaper_Skin,
							en.SchoolLogo AS Newspaper_SchoolLogo,
							en.CoverBanner AS Newspaper_CoverBanner,
							en.CoverBannerHideTitle AS Newspaper_CoverBannerHideTitle,
							en.CoverBannerHideName AS Newspaper_CoverBannerHideName,
							en.InsideBanner AS Newspaper_InsideBanner,
							en.InsideBannerHideTitle AS Newspaper_InsideBannerHideTitle,
							en.InsideBannerHideName AS Newspaper_InsideBannerHideName,
							en.InsideBannerHidePageCat AS Newspaper_InsideBannerHidePageCat,
							en.NoOfView AS Newspaper_NoOfView,
							en.PublishStatus AS Newspaper_PublishStatus,
							en.Status AS Newspaper_Status,
							en.InputDate AS Newspaper_InputDate,
							en.ModifiedBy AS Newspaper_ModifiedBy,
							en.ModifiedDate AS Newspaper_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Newspaper_ModifiedByUser,
							iu.EnglishName AS Newspaper_EnglishName,
							iu.ClassName AS Newspaper_ClassName,
							iu.ClassNumber AS Newspaper_ClassNumber,
							GROUP_CONCAT(DISTINCT ep.PageID ORDER BY ep.PageOrder SEPARATOR '$PageIDSeparator') AS Newspaper_PageIDs
						FROM
							EPOST_NEWSPAPER AS en INNER JOIN
							INTRANET_USER AS iu ON en.ModifiedBy = iu.UserID LEFT JOIN
							EPOST_NEWSPAPER_PAGE AS ep ON en.NewspaperID = ep.NewspaperID AND ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
						WHERE
							en.NewspaperID IN (".implode(',', $this->post_ids).") AND
							en.Status = ".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."
							".$keyword_search."
						GROUP BY
							en.NewspaperID
							".$cond."
						";
				$count_ary = $this->returnArray($sql); 
				
				# Get Newspaper Student Editor
				$sql = "SELECT
							esem.ManageItemID,
							iu.UserID,
							iu.EnglishName,
							iu.ClassName,
							iu.ClassNumber
						FROM
							EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
							EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
							INTRANET_USER AS iu ON ese.UserID = iu.UserID
						WHERE
							ese.Status = ".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']." AND
							esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
							esem.Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."
						ORDER BY
							iu.ClassName, iu.ClassNumber, iu.EnglishName";
				$StudentEditor_ary = $this->returnArray($sql);
				
				for($i=0;$i<count($result_ary);$i++){
					$result_ary[$i]['Newspaper_PageIDs'] 	= $result_ary[$i]['Newspaper_PageIDs']? explode($PageIDSeparator, $result_ary[$i]['Newspaper_PageIDs']) : array();
					$result_ary[$i]['Newspaper_StudentEditors'] 		= array();
					$result_ary[$i]['Newspaper_StudentEditorUserIDs'] = array();
					
					for($j=0;$j<count($StudentEditor_ary);$j++){
						if($result_ary[$i]['NewspaperID']==$StudentEditor_ary[$j]['ManageItemID']){
							$result_ary[$i]['Newspaper_StudentEditorUserIDs'][] = $StudentEditor_ary[$j]['UserID'];
							$result_ary[$i]['Newspaper_StudentEditors'][]  = array(
								"UserID" => $StudentEditor_ary[$j]['UserID'],
								"EnglishName" => $StudentEditor_ary[$j]['EnglishName'],
								"ClassName"	  => $StudentEditor_ary[$j]['ClassName'],
								"ClassNumber" => $StudentEditor_ary[$j]['ClassNumber']
							);
						}
					}
				}
			}
			return array(count($count_ary),$result_ary);
		}
		function gen_editor_page_tab(){
			global $HTTP_SERVER_VARS, $intranet_httppath, $Lang;
			
			$is_current_newspaper 	    = strpos($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/newspaper/")!==false;
			$is_current_request_list    = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/request_list/")!==false;
			$is_current_student_editor  = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/student_editor/")!==false;
			$is_current_article_shelf   = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/article_shelf/")!==false;
			$is_current_publish_manager = strstr($HTTP_SERVER_VARS["SCRIPT_NAME"], "home/ePost/publish_manager/")!==false;
			
			if($is_current_newspaper || $is_current_request_list || $is_current_student_editor || $is_current_article_shelf || $is_current_publish_manager){
				$html  = "<div class='board_tab'>";
				$html .= "	<ul>";

				$html .= "<li ".($is_current_request_list? "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/request_list/index.php'><span>".$Lang['ePost']['RequestList']."</span></a></li>";
				$html .= "	<li ".($is_current_article_shelf?  "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/article_shelf/index.php'><span>".$Lang['ePost']['ArticleShelf']."</span></a></li>";
				
				$html .= "		<li ".($is_current_newspaper? "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/newspaper/index.php'><span>".$Lang['ePost']['Issues']."</span></a></li>";
				if($this->user_obj->isTeacherStaff() && $this->is_editor){
					$html .= "	<li ".($is_current_publish_manager||$is_current_student_editor? "class='current_tab'":"")."><a href='$intranet_httppath/home/ePost/publish_manager/index.php'><span>".$Lang['ePost']['AssigningEditors']."</span></a></li>";
				}
				$html .= "	</ul>";
				$html .= "</div>";
			}
			
			return $html;
		}
		
		function generateXML($Data) {
			$xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
			for ($i=0;$i<count($Data);$i++) {
				list($ParTag, $ParValue, $ParCData, $ParSpcChar) = $Data[$i];
				$x .= $this->str_to_xml($ParTag, $ParValue, $ParCData, $ParSpcChar);
			}
			$xml .= $this->str_to_xml("elements", $x);
			return $xml;
		}		
		function str_to_xml($ParTag, $ParValue, $ParCData=false, $ParSpcChar=false) {
			return "<".$ParTag.">".($ParCData?"<![CDATA[":"").($ParSpcChar?intranet_htmlspecialchars($ParValue):$ParValue).($ParCData?"]]>":"")."</".$ParTag.">";
		}		
	}
}
?>