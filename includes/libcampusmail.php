<?php
## Using By : 
##########################################################

## Modification Log
## 2020-01-17: Sam
## - modified displayiMailAttachment(), url encode the attachment file name in url. [2020-0116-1533-20235]
##
## 2019-10-25: Carlos
## - modified displayiMailAttachment(), base64 encode the attachment file name in url. Deploy with /home/imail/downloadattachment.php
##
## 2019-04-12: Carlos
## - modified displayiMailAttachment() to sort attachment files by name with usort() on file list.
##
## 2017-03-06: Carlos
## - added GetCampusMailNewMailCount() and UpdateCampusMailNewMailCount() for storing number of new/unread emails in each folder.
## - modified returnNumNewMessage_iMail() to use GetCampusMailNewMailCount().
##
## 2014-04-16: Carlos
## - added returnAllAvailableRecipientUserIDArray() for getting all UserIDs that current user can send mail to according to mail target settings
##
## 2013-03-13: Carlos
## - modified smartstripslashes(), remove <base> tags from message content
## 2011-11-04: Carlos
## - modified constructor, do convertBadWords() of Subject and Message in constructor
##
## 2011-03-24: Carlos
## - modified libcampusmail() fix getting setting campusmail_policy
##
## 2011-03-22: Yuen
## - modified function returnNextID() and returnPrevID() to limit 1 result
##
## 2010-12-14: Carlos
## - modified function displayiMailAttachment(), add checking on AttachmentPath
##
## 2010-12-13: Ronald
## - create function smartstripslashes(), used to remove backslash in subject and mail content 
##
## 2010-12-06: Ronald
## - modified function displayiMailAttachment(), now directly access into user attachmen folder to get all the related attachments 
##
## 2010-11-15: Ronald
## - create new function directoryList(), to show all the attachments in the related email attachment folder.  
## - modified function displayiMailAttachment(), now will use read attachment folder to show all the attachments if the filename stored in DB is empty.
##
## 2010-09-17: Ronald
## - modified view_image_embed_message(), add $CampusMailID to the link of showing the img.
## - modified convert_image_embeded_message(), now will use strtoupper to cater the <img> in both UPPER case and LOWER Case  
##
## 2010-09-17: Ronald
## - modified returnRecipientUserIDArrayWithQuota(), add one more condition "AND a.RecordStatus IN (1)", so now can only send internal mail to the active user.
##
## 2010-09-06: Marcus
## - Add param is_array to externalRecipientFormatting, to pass arrat instead of str.
##
## 2010-03-16: Ronald
## - modify function getRecipientNames(), add sorting order to the related query.
##
## 2009-08-04: Ronald
## - modify function returnRecipientUserIDArrayWithQuota(), add more type which is used in IP25 (T,S,P,O)
##
## 2009-08-04: Ronald
## - modify function returnRecipientIDArray(), add more type which is used in IP25 (T,S,P,O)
##
## 2009-03-18: Ronald
## - modify getRecipientNames(), now can handle the student's parent
##
## 2005-07-18: Kenneth Wong
## - Convert embed image from Outlook (cid) and store to DB
## - Convert back to iMail attachment type when viewing
##
$embed_attach_key1 = "<!-- eClass iMail 1 -->[image attached ";
$embed_attach_key2 = "]<!-- eClass iMail 2 -->";

class libcampusmail extends libdb{

     var $CampusMail;
     var $CampusMailID;
     var $CampusMailFromID;
     var $UserID;
     var $UserFolderID;
     var $SenderID;
     var $RecipientID;
     var $Subject;
     var $Message;
	 var $raw_message;			# Raw Message get from INTRANET_RAWCAMPUSMAIL
     var $Attachment;
     var $IsAttachment;
     var $IsImportant;
     var $IsNotification;
     var $RecordType;
     var $RecordStatus;
     var $DateInput;
     var $DateModified;
     var $IsRead;
     var $MessageReply;
     var $policy;

     # iMail Ext
     var $isDeleted;
     var $mailType;
     var $SenderEmail;
     var $InternalCC;
     var $InternalBCC;
     var $ExternalTo;
     var $ExternalCC;
     var $ExternalBCC;
     var $isHTML;
     var $MessageEncoding;

     var $usage_internal_disabled;
     var $usage_external_disabled;
	 var $has_raw_message;		# Load RawMessage from INTRANET_RAWCAMPUSMAIL if exists
	 var $messageCharset;		# Raw Message Charset
	 var $headerPath;			# Header Path for original Mail;
	 var $messagePath;			# Message Path for original Mail;

	 var $NewMailCountCacheAry; // $NewMailCountCacheAry[UserID][FolderID] = number of new mail count
     # ------------------------------------------------------------------------------------

     function libcampusmail($CampusMailID=""){
          $this->libdb();
          $this->NewMailCountCacheAry = array();
          if($CampusMailID<>""){
               $this->CampusMail = $this->returnCampusMail($CampusMailID);
               $this->CampusMailID = $this->CampusMail[0][0];
               $this->CampusMailFromID = $this->CampusMail[0][1];
               $this->UserID = $this->CampusMail[0][2];
               $this->UserFolderID = $this->CampusMail[0][3];
               $this->SenderID = $this->CampusMail[0][4];
               $this->RecipientID = $this->CampusMail[0][5];
               $this->Subject = $this->convertBadWords($this->CampusMail[0][6]);
               $this->Message = $this->convertBadWords($this->CampusMail[0][7]);
               $this->Attachment = $this->CampusMail[0][8];
               $this->IsAttachment = $this->CampusMail[0][9];
               $this->IsImportant = $this->CampusMail[0][10];
               $this->IsNotification = $this->CampusMail[0][11];
               $this->RecordType = $this->CampusMail[0][12];
               $this->RecordStatus = $this->CampusMail[0][13];
               $this->DateInput = $this->CampusMail[0][14];
               $this->DateModified = $this->CampusMail[0][15];
			   $this->has_raw_message = $this->HasRawMessage($CampusMailID);
			   $this->raw_message = $this->GrabRawMessage($CampusMailID);
			   $this->messageCharset = $this->GrabMessageCharset($CampusMailID);
			   $this->headerPath = $this->GrabHeaderPath($CampusMailID);
			   $this->messagePath = $this->GrabMessagePath($CampusMailID);

               global $special_feature;
               if ($special_feature['imail'])
               {
                   $array = $this->returnCampusMail_iMailExt($CampusMailID);
                   list($this->isDeleted, $this->mailType,$this->SenderEmail
                        ,$this->InternalCC,$this->InternalBCC, $this->ExternalTo
                        ,$this->ExternalCC,$this->ExternalBCC,
                        $this->isHTML, $this->MessageEncoding) = $array;
               }
          }
          global $intranet_root, $_SESSION;
          if (!isset($_SESSION['campusmail_policy'])/* && $_SESSION['campusmail_policy']=="" && $_SESSION['campusmail_policy']<>0*/)
          {
	          $this->policy = get_file_content ("$intranet_root/file/campusmail_policy.txt");
	          if ($this->policy!=1) $this->policy=0;
	          $_SESSION['campusmail_policy'] = $this->policy;
          } else
          {
          	$this->policy = $_SESSION['campusmail_policy'];
          }

		  if (!isset($_SESSION['usage_internal_disabled'])/* && $_SESSION['usage_internal_disabled']=="" && $_SESSION['usage_internal_disabled']<>0*/)
          {   
	          $file_content = get_file_content($intranet_root."/file/campusmail_set_usage.txt");
	          $set_data = explode("\n",$file_content);
	          $this->usage_internal_disabled = (trim($set_data[0])=="1");
	          $this->usage_external_disabled = (trim($set_data[1])=="1");
	          $_SESSION['usage_internal_disabled'] = $this->usage_internal_disabled;
	          $_SESSION['usage_external_disabled'] = $this->usage_external_disabled;
          } else
          {
	          $this->usage_internal_disabled = $_SESSION['usage_internal_disabled'];
	          $this->usage_external_disabled = $_SESSION['usage_external_disabled'];
          }
     }
     
     function directoryList($start,$win32=false)
	 {
		if($win32){
			$slash="\\";
		}else{
	 		$slash="/";
	    }
	   
	   	$tmp_basename = pathinfo($start);
	   	$basename = $tmp_basename['basename'];
		$ls=array();
		$dir = dir($start);
		while($item = $dir->read())
		{
	   		if(is_dir($start.$slash.$item)&& $item!="." && $item!="..")
	   		{
				if(strpos($start,'origin')===false)
				{
	       			$tmp=$this->directoryList($start.$slash.$item,$win32);
					$ls = array_merge($ls,$tmp);
				}
	   		}
	   		else
	   		{
	       		if($item!="."&&$item!="..")
	       		{
	           		$ls[$start][]= array(
						'file'=>$item,
						'size'=>round(filesize($start.$slash.$item)/1024), 
						'modified'=>filemtime($start.$slash.$item)
					);
	       		}
	   		}
		}
		return $ls;
	 }

     function isAutoReply ()
     {
              return ($this->policy == 1);
     }

     function returnCampusMail($CampusMailID){
          global $UserID;
          $sql = "SELECT
                    CampusMailID,
                    CampusMailFromID,
                    UserID,
                    UserFolderID,
                    SenderID,
                    RecipientID,
                    Subject,
                    Message,
                    Attachment,
                    IsAttachment,
                    IsImportant,
                    IsNotification,
                    RecordType,
                    RecordStatus,
                    DateInput,
                    DATE_FORMAT(DateModified, '%Y-%m-%d %H:%i')
                    FROM INTRANET_CAMPUSMAIL
                    WHERE CampusMailID = '".$this->Get_Safe_Sql_Query($CampusMailID)."' AND UserID = '".$this->Get_Safe_Sql_Query($UserID)."'
          ";
          return $this->returnArray($sql,16);
     }
     function returnCampusMail_iMailExt($CampusMailID){
          global $UserID;

          $sql = "SELECT
                    Deleted,MailType,SenderEmail,InternalCC,InternalBCC,ExternalTo,ExternalCC,ExternalBCC,
                    isHTML, MessageEncoding
                    FROM INTRANET_CAMPUSMAIL
                    WHERE CampusMailID = '".$this->Get_Safe_Sql_Query($CampusMailID)."' AND UserID = '".$this->Get_Safe_Sql_Query($UserID)."'
          ";
          $temp = $this->returnArray($sql,10);
          return $temp[0];
     }

     # ------------------------------------------------------------------------------------

     function returnRecipientIDArray($Recipient){

          $GroupIDList = "";
          $UserIDsList = "";
          $ParentStudentIDsList = "";
          $ParentGroupIDsList = "";
          $group_delimiter = "";
          $user_delimiter = "";
          $parent_student_delim = "";
          $parent_group_delim = "";
          $TeacherGroupList = "";
          $TeacherGroupDelim = "";
          $StudentGroupList = "";
          $StudentGroupDelim = "";
          $ParentGroupList = "";
          $ParentGroupDelim = "";
          $row = explode(",",$Recipient);

          for($i=0; $i<sizeof($row); $i++){
               $RecipientType = substr($row[$i],0,1);
               $RecipientID = substr($row[$i],1);
               if($RecipientType=="G")    # Group
               {
                  $GroupIDList .= $group_delimiter.$RecipientID;
                  $group_delimiter = ",";
               }
               else if ($RecipientType=="Q")       # Parents of the students in group
               {
                    $ParentGroupIDsList .= $parent_group_delim.$RecipientID;
                    $parent_group_delim = ",";
               }
               else if ($RecipientType=="P")    # Parents of the students
               {
                    $ParentStudentIDsList .= $parent_student_delim.$RecipientID;
                    $parent_student_delim = ",";
               }
               else if ($RecipientType == "T")	## Teachers By Group (Newly for IP25)
               {
               		$TeacherGroupList .= $TeacherGroupDelim.$RecipientID;
               		$TeacherGroupDelim = ",";
               }
               else if ($RecipientType == "S")	## Students By Group (Newly for IP25)
               {
               		$StudentGroupList .= $StudentGroupDelim.$RecipientID;
               		$StudentGroupDelim = ",";
               }
               else if ($RecipientType == "R")	## Parent By Group (Newly for IP25)
               {
               		$ParentGroupList .= $ParentGroupDelim.$RecipientID;
               		$ParentGroupDelim = ",";
               }
               else if ($RecipientType == "O")	## Parent By Group (Newly for IP25)
               {
               		$GroupIDList .= $group_delimiter.$RecipientID;
                  	$group_delimiter = ",";
               }
               else # Target User
               {
                   $UserIDsList .= $user_delimiter.$RecipientID;
                   $user_delimiter = ",";
               }
          }
          $x[0] = ($GroupIDList == ""? 0:$GroupIDList);
          $x[1] = ($UserIDsList == ""? 0:$UserIDsList);
          $x[2] = ($ParentGroupIDsList == ""? 0:$ParentGroupIDsList);
          $x[3] = ($ParentStudentIDsList == ""? 0:$ParentStudentIDsList);
          $x[4] = ($TeacherGroupList == ""? 0:$TeacherGroupList);
          $x[5] = ($StudentGroupList == ""? 0:$StudentGroupList);
          $x[6] = ($ParentGroupList == ""? 0:$ParentGroupList);
          return $x;
     }

     function returnCampusMailReply(){
     /*
              global $intranet_session_language;
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldWithClassNumberByLang("a.");
              $row = $this->returnRecipientUserIDArray($this->RecipientID);
              $UserIDsList = 0;
              for($i=0; $i<sizeof($row); $i++){
                  $UserIDsList .= ",".$row[$i][0];
              }
              $order_str = "a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
//              $sql  = "SELECT a.UserID, CONCAT(a.$username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ), b.CampusMailReplyID, b.CampusMailID, b.IsRead, b.Message FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_CAMPUSMAIL_REPLY AS b ON a.UserID = b.UserID AND b.CampusMailID = ".$this->CampusMailID." WHERE a.UserID IN ($UserIDsList)";
              $sql  = "SELECT a.UserID, IF(a.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$username_field), b.CampusMailReplyID, b.CampusMailID, b.IsRead, b.Message FROM INTRANET_CAMPUSMAIL_REPLY AS b LEFT OUTER JOIN INTRANET_USER AS a ON a.UserID = b.UserID WHERE b.CampusMailID = '".$this->CampusMailID."' AND a.UserID IN ($UserIDsList) ORDER BY $order_str";
              return $this->returnArray($sql, 6);
     }
     # /home/imail/outbox_view.php      eClass IP - iMail
     # change sorting order by reply date (DateModified)
     function returnCampusiMailReply()
     {
              $username_field = getNameFieldWithClassNumberByLang("a.");
              //$order_str = "a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
              $order_str = "b.DateModified DESC, a.ClassName, a.ClassNumber, a.EnglishName, b.UserName";
              $sql = "SELECT a.UserID, IF(a.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$username_field),
                             b.CampusMailReplyID, b.CampusMailID, b.IsRead, b.Message, a.RecordType, b.DateModified
                       FROM INTRANET_CAMPUSMAIL_REPLY AS b LEFT OUTER JOIN INTRANET_USER AS a ON a.UserID = b.UserID WHERE b.CampusMailID = '".$this->CampusMailID."' ORDER BY $order_str";
              return $this->returnArray($sql, 8);
     }
     function returnAccessList()
     {
              global $intranet_root, $_SESSION;
              if ($_SESSION['campusmail_set_content']=="")
              {
              	$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
              	$_SESSION['campusmail_set_content'] = (trim($file_content)=="")?"NULL_CONTENT":$file_content;
              } else
              {
              	$file_content = ($_SESSION['campusmail_set_content']=="NULL_CONTENT") ? "":$_SESSION['campusmail_set_content'];
              }
              if ($file_content == "") return array(1,2,3);
              $content = explode("\n",$file_content);
              # Get row 1,2,4 only. Compatible with previous version
              if ($content[0][0]==1) $permitted[] = 1;
              if ($content[1][0]==1) $permitted[] = 2;
              if ($content[3][0]==1) $permitted[] = 3;
              return $permitted;
     }

     # Not used after 2.0 (used in 2.0) when RecordType storing single user type
     function returnAccessUserList ()
     {
              global $intranet_root, $_SESSION;
              if ($_SESSION['campusmail_set_content']=="")
              {
              	$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
              	$_SESSION['campusmail_set_content'] = (trim($file_content)=="")?"NULL_CONTENT":$file_content;
              } else
              {
              	$file_content = ($_SESSION['campusmail_set_content']=="NULL_CONTENT") ? "":$_SESSION['campusmail_set_content'];
              }
              $content = explode("\n",$file_content);
              $count = 0;
              $permitted = array();
              for ($i=0; $i<sizeof ($content); $i++)
              {
                   $settings = explode(":",$content[$i]);
                   $access_right[$i] = $settings[0];
                   if ($settings[0]==1)
                   {
                       $permitted[$count] = $i+1;
                       $count++;
                   }
              }
              if ($file_content != "" && sizeof($permitted)!= 0)
              {
                  $list = implode(",",$permitted);
                  $sql = "SELECT DISTINCT a.UserID FROM INTRANET_USER as a, INTRANET_USERGROUP as b WHERE a.UserID = b.UserID AND a.RecordStatus = 1 AND b.GroupID IN ($list)";
                  $users_array = $this->returnVector($sql);
                  return $users_array;
              }
              else
              {
                  return ($file_content == "");            // if true, all permitted, else all not-permitted (should be invalid case)
              }
     }

     function returnRecipientUserIDArray($Recipient){

          $row = $this->returnRecipientIDArray($Recipient);
          if($this->returnAccessList())
          	$accessList = implode(",",$this->returnAccessList());
          $GroupIDList = $row[0];
          $UserIDsList = $row[1];
          # Method in 2.x
          # Select UserIDs separately from group and user basis
          # then merge the 2 arrays

          # Group
          $username_field = getNameFieldWithClassNumberEng("a.");
          if ($GroupIDList!="")
          {
              $groupList = $GroupIDList;
              $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',a.RecordType)
                      FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                      WHERE a.UserID = b.UserID
                      AND a.RecordType IN ($accessList)
                      AND b.GroupID IN ($groupList)";
              $groupNames = $this->returnVector($sql);
          }
          else
          {
              $groupNames = array();
          }

          if ($UserIDsList!="")
          {
              $userList = $UserIDsList;
              $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',a.RecordType)
                      FROM INTRANET_USER as a
                      WHERE a.RecordType IN ($accessList)
                      AND a.UserID IN ($userList)";
              $userNames = $this->returnVector($sql);
          }
          else
          {
              $userNames = array();
          }

          if ($UserIDsList=="" && $GroupIDList=="") return array();

          # Take Union of arrays
          $overall = array_union($userNames,$groupNames);

          # Separate UserIDs and Names
          while (list ($key, $val) = each ($overall))
          {
                 $row = explode(":::",$val);
                 $result[] = $row;
          }
          return $result;


/*
          # Method in 2.0 or before
          $user_array = $this->returnAccessUserList();
          if (!is_array($user_array))
          {
               if (!$user_array)
                    return array();
               else
                   $conds = "";
          }
          else
          {
              $can_list = implode (",",$user_array);
              $conds = "AND UserID IN ($can_list)";
          }

          $sql  = "SELECT DISTINCT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ($GroupIDList) $conds";
          $tmpGroupUserList = $this->db_sub_select($sql);
          if ($tmpGroupUserList == 0)
          {
              $tmpGroupUserList = "";
          }

          if ($UserIDsList != "" && $tmpGroupUserList != "")
              $UserIDsList .= ",".$tmpGroupUserList;
          else
              $UserIDsList .= $tmpGroupUserList;
          $username_field = getNameFieldByLang();
          $sql  = "SELECT UserID, CONCAT($username_field, IF (ClassNumber IS NULL OR ClassNumber = '', '', CONCAT(' (',ClassName,'-',ClassNumber,')') ) ) FROM INTRANET_USER WHERE UserID IN ($UserIDsList) $conds";
          return $this->returnArray($sql, 2);
          */
     }

     # Use in iMail
     function returnRecipientUserIDArrayWithQuota($Recipient){

          $row = $this->returnRecipientIDArray($Recipient);

          $accessList = implode(",",$this->returnAccessList());
          $GroupIDList = $row[0];
          $UserIDsList = $row[1];
          $ParentGroupIDList = $row[2];
          $ParentStudentIDList = $row[3];
          $TeacherGroupList = $row[4];
          $StudentGroupList = $row[5];
          $ParentGroupList = $row[6];

          # Group
          $username_field = getNameFieldWithClassNumberEng("a.");
          if ($GroupIDList!="")
          {
          		if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
				{
					$result_to_options['ToTeachingStaffOption'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
				{
					$result_to_options['ToNonTeachingStaffOption'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
				{
					$result_to_options['ToStudentOption'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
				{
					$result_to_options['ToParentOption'] = 1;
				}
				if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']))
				{
					$result_to_options['ToGroupOption'] = 1;

					$result_to_group_options['ToTeacher'] = 0;
					$result_to_group_options['ToStaff'] = 0;
					$result_to_group_options['ToStudent'] = 0;
					$result_to_group_options['ToParent'] = 0;

					if($_SESSION['SSV_USER_TARGET']['All-Yes']){
						$result_to_group_options['ToTeacher'] = 1;
						$result_to_group_options['ToStaff'] = 1;
						$result_to_group_options['ToStudent'] = 1;
						$result_to_group_options['ToParent'] = 1;
					}else{
						if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
							$result_to_group_options['ToTeacher'] = 1;
						}
						if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
							$result_to_group_options['ToStaff'] = 1;
						}
						if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
							$result_to_group_options['ToStudent'] = 1;
						}
						if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
							$result_to_group_options['ToParent'] = 1;
						}
					}
				}
				if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') &&
					($_SESSION['SSV_USER_TARGET']['All-No'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') &&
					($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Student-All'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') &&
					($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == ''))
					{
						if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0)
						{
							if(($identity == "Teaching") || ($identity == "NonTeaching"))
							{
								### If user is Teacher, then will have the follow targeting :
								###  - to All Teaching Staff
								###  - to All NonTeaching Staff
								###  - to All Student
								###  - to All Parent
								$result_to_options['ToTeachingStaffOption'] = 1;
								$result_to_options['ToNonTeachingStaffOption'] = 1;
								$result_to_options['ToStudentOption'] = 1;
								$result_to_options['ToParentOption'] = 1;
								$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
							}
							if($identity == "Student")
							{
								### If user is Student, then will have the follow targeting :
								###  - to Student Own Class Student
								###  - to Student Own Subject Group Student
								$result_to_options['ToStudentOption'] = 1;
								$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;
								$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
							}
							if($identity == "Parent")
							{
								### If user is Parent, then will have the follow targeting :
								###  - to Their Child's Own Class Teacher
								###  - to Their Child's Own Subject Group Teacher
								$result_to_options['ToTeachingStaffOption'] = 1;
								$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;
								$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
							}
						}
						$result_to_options['ToGroupOption'] = 1;

						$result_to_group_options['ToTeacher'] = 0;
						$result_to_group_options['ToStaff'] = 0;
						$result_to_group_options['ToStudent'] = 0;
						$result_to_group_options['ToParent'] = 0;

						if($_SESSION['SSV_USER_TARGET']['All-Yes']){
							$result_to_group_options['ToTeacher'] = 1;
							$result_to_group_options['ToStaff'] = 1;
							$result_to_group_options['ToStudent'] = 1;
							$result_to_group_options['ToParent'] = 1;
						}else{
							if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
								$result_to_group_options['ToTeacher'] = 1;
							}
							if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
								$result_to_group_options['ToStaff'] = 1;
							}
							if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
								$result_to_group_options['ToStudent'] = 1;
							}
							if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
								$result_to_group_options['ToParent'] = 1;
							}
						}
					}

				$cond = "";
				if($result_to_group_options['ToTeacher'] == 1){
					if($cond != "")
						$cond .= " OR ";
					$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
				}
				if($result_to_group_options['ToStaff'] == 1){
					if($cond != "")
						$cond .= " OR ";
					$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
				}
				if($result_to_group_options['ToStudent'] == 1){
					if($cond != "")
						$cond .= " OR ";
					$cond .= " (a.RecordType = 2) ";
				}
				if($result_to_group_options['ToParent'] == 1){
					if($cond != "")
						$cond .= " OR ";
					$cond .= " (a.RecordType = 3) ";
				}

				if($cond != "")
					$final_cond = " AND ( $cond ) ";

				$groupList = $GroupIDList;
              $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                      FROM INTRANET_USERGROUP as b
                           LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.UserID
                           LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                      WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
                      AND b.GroupID IN ($groupList) $final_cond";
              $groupNames = $this->returnVector($sql);
          }
          else
          {
              $groupNames = array();
          }

          if ($UserIDsList!="")
          {
              $userList = $UserIDsList;

              $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                      FROM INTRANET_USER as a
                           LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                      WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
                      AND a.UserID IN ($userList)";
              $userNames = $this->returnVector($sql);
          }
          else
          {
              $userNames = array();
          }
          $pos = 1;


          if ($ParentGroupIDList!="")
          {
              # Get Student List from Group first
              $sql = "SELECT DISTINCT a.UserID FROM INTRANET_USERGROUP as b
                             LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.UserID
                             WHERE b.GroupID IN ($ParentGroupIDList)
                                   AND a.RecordType = 2 AND a.RecordStatus IN (1)";
              $temp = $this->returnVector($sql);

              # Get Parent List from Student List
              if (sizeof($temp)!=0)
              {
                  $targetStudentList = implode(",",$temp);
                  $sql = "SELECT DISTINCT a.UserID FROM INTRANET_PARENTRELATION as b
                                 LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.ParentID
                                 WHERE b.StudentID IN ($targetStudentList)
                                       AND a.RecordType = 3 AND a.RecordStatus IN (1)";
                  $temp = $this->returnVector($sql);
                  if (sizeof($temp)!=0)
                  {
                      # Get Name and Quota for the list
                      $list = implode(",",$temp);
                      $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                                     FROM INTRANET_USER as a
                                     LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                                     WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
                                           AND a.UserID IN ($list)";
                      $parentGroupNames = $this->returnVector($sql);
                  }
                  else
                  {
                      $parentGroupNames = array();
                  }
              }
              else
              {
                  $parentGroupNames = array();
              }
          }
          else
          {
              $parentGroupNames = array();
          }
          if ($ParentStudentIDList!="")
          {
                  $sql = "SELECT DISTINCT a.UserID FROM INTRANET_PARENTRELATION as b
                                 LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.ParentID
                                 WHERE b.StudentID IN ($ParentStudentIDList)
                                       AND a.RecordType = 3 AND a.RecordStatus IN (1)";
                  $temp = $this->returnVector($sql);
                  if (sizeof($temp)!=0)
                  {
                      # Get Name and Quota for the list
                      $list = implode(",",$temp);
                      $sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
                                     FROM INTRANET_USER as a
                                     LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
                                     WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
                                           AND a.UserID IN ($list)";
                      $parentStudentNames = $this->returnVector($sql);
                  }
                  else
                  {
                      $parentStudentNames = array();
                  }

          }
          else
          {
              $parentStudentNames = array();
          }
          if($TeacherGroupList != "")
          {
          		$temp = $this->getRelatedGroupTeacherList($TeacherGroupList);
          		if(sizeof($temp)>0){
					# Get Name and Quota for the list
					$list = implode(",",$temp);
					$sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
									FROM INTRANET_USER as a
									LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
									WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
										AND a.UserID IN ($list)";
					$TeacherGroupName = $this->returnVector($sql);
                }else{
                	$TeacherGroupName = array();
                }
          }
          else
          {
				$TeacherGroupName = array();
          }
          if($StudentGroupList != "")
		  {
		  		$temp = $this->getRelatedGroupStudentList($StudentGroupList);

		  		if(sizeof($temp)>0){
					# Get Name and Quota for the list
					$list = implode(",",$temp);
					$sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
									FROM INTRANET_USER as a
									LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
									WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
										AND a.UserID IN ($list)";
					$StudentGroupName = $this->returnVector($sql);
                }else{
                	$StudentGroupName = array();
                }
		  }
		  else
		  {
				$StudentGroupName = array();
		  }
		  if($ParentGroupList != "")
		  {
		  		$temp = $this->getRelatedGroupParentList($ParentGroupList);

		  		if(sizeof($temp)>0){
					# Get Name and Quota for the list
					$list = implode(",",$temp);
					$sql = "SELECT CONCAT(a.UserID,':::',$username_field,':::',IF(c.Quota IS NULL,0,c.Quota))
									FROM INTRANET_USER as a
									LEFT OUTER JOIN INTRANET_CAMPUSMAIL_USERQUOTA as c ON a.UserID = c.UserID
									WHERE a.RecordType IN ($accessList) AND a.RecordStatus IN (1)
										AND a.UserID IN ($list)";
					$ParentGroupName = $this->returnVector($sql);
                }else{
                	$ParentGroupName = array();
                }
		  }
		  else
		  {
		  		$ParentGroupName = array();
		  }

          if (sizeof($userNames)==0 && sizeof($groupNames)==0 && sizeof($parentGroupNames)==0 && sizeof($parentStudentNames)==0 && sizeof($TeacherGroupName)==0 && sizeof($StudentGroupName)==0 && sizeof($ParentGroupName)==0) return array();
          #if ($UserIDsList=="" && $GroupIDList=="") return array();

          # Take Union of arrays
          $overall = array_union($userNames,$groupNames);
          $overall = array_union($overall, $parentGroupNames);
          $overall = array_union($overall, $parentStudentNames);
          $overall = array_union($overall, $TeacherGroupName);
          $overall = array_union($overall, $StudentGroupName);
          $overall = array_union($overall, $ParentGroupName);

          # Separate UserIDs and Names
          while (list ($key, $val) = each ($overall))
          {
                 $row = explode(":::",$val);
                 $result[] = $row;
          }
          return $result;
     }
     function returnRecipientOption($Recipient){
              //global $intranet_session_language;
              global $i_general_targetParent;

              $GroupArray = array();
              $UserArray = array();
              $ParentGroupArray = array();
              $ParentUserArray = array();
              $groupTeacher = array();
              $groupStudent = array();
              $groupParent = array();

              if($this->UserID<>""){
              /*
                 $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
                 $username_field = ($chi? "ChineseName": "EnglishName");
                 */
                 global $Lang;
                 $username_field = getNameFieldWithClassNumberByLang();
                 $row = $this->returnRecipientIDArray($Recipient);
                 $GroupIDList = $row[0];
                 $UserIDsList = $row[1];
                 $ParentGroupIDsList = $row[2];
                 $ParentStudentIDsList = $row[3];
                 $TeacherGroupList = $row[4];
                 $StudentGroupList = $row[5];
                 $ParentGroupList = $row[6];
                 $text_parent = intranet_htmlspecialchars($i_general_targetParent);

                 if ($GroupIDList!="")
                 {
                     $sql  = "SELECT CONCAT('G', GroupID), Title FROM INTRANET_GROUP WHERE GroupID IN ($GroupIDList)";
                     $GroupArray = $this->returnArray($sql, 2);
                 }
                 if ($UserIDsList!="")
                 {
                     $sql  = "SELECT CONCAT('U', UserID), $username_field FROM INTRANET_USER WHERE UserID IN ($UserIDsList)";
                     $UserArray = $this->returnArray($sql, 2);
                 }
                 if ($ParentGroupIDsList!="")
                 {
                     $sql  = "SELECT CONCAT('Q', GroupID), CONCAT(Title,'$text_parent') FROM INTRANET_GROUP WHERE GroupID IN ($ParentGroupIDsList)";
                     $ParentGroupArray = $this->returnArray($sql, 2);
                 }
                 if ($ParentStudentIDsList!="")
                 {
                     $sql  = "SELECT CONCAT('P', UserID), CONCAT($username_field,'$text_parent') FROM INTRANET_USER WHERE UserID IN ($ParentStudentIDsList)";
                     $ParentUserArray = $this->returnArray($sql, 2);
                 }
                 if($TeacherGroupList!=""){
	              		$tmp_arr = explode(",",$TeacherGroupList);
	              		for($i=0; $i<sizeof($TeacherGroupList); $i++)
	              		{
	              			$groupTeacher[$i][] = "T".$TeacherGroupList[$i];
	              			switch($TeacherGroupList[$i]){
	              				case 1:
	              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'];
	              					break;
	              				case 2:
	              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToFormTeachingStaff'];
	              					break;
	              				case 3:
	              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToClassTeachingStaff'];
	              					break;
	              				case 4:
	              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'];
	              					break;
	              				case 5:
	              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'];
	              					break;
	              			}
	              		}
	              }
	              if($StudentGroupList!=""){
	              		$tmp_arr = explode(",",$StudentGroupList);
	              		for($i=0; $i<sizeof($StudentGroupList); $i++)
	              		{
	              			$groupStudent[$i][] = "S".$StudentGroupList[$i];

	              			switch($StudentGroupList[$i]){
	              				case 1:
	              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToIndividualsStudent'];
	              					break;
	              				case 2:
	              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToFormStudent'];
	              					break;
	              				case 3:
	              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToClassStudent'];
	              					break;
	              				case 4:
	              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectStudent'];
	              					break;
	              				case 5:
	              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'];
	              					break;
	              			}
	              		}
	              }
	              if($ParentGroupList!=""){
	              		$tmp_arr = explode(",",$ParentGroupList);
	              		for($i=0; $i<sizeof($ParentGroupList); $i++)
	              		{
	              			$groupParent[$i][] = "S".$ParentGroupList[$i];
	              			switch($ParentGroupList[$i]){
	              				case 1:
	              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToIndividualsParents'];
	              					break;
	              				case 2:
	              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToFormParents'];
	              					break;
	              				case 3:
	              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToClassParents'];
	              					break;
	              				case 4:
	              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectParents'];
	              					break;
	              				case 5:
	              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectGroupParents'];
	              					break;
	              			}
	              		}
	              }
                 return array_merge($GroupArray, $UserArray, $ParentGroupArray, $ParentUserArray, $groupTeacher, $groupStudent, $groupParent);
              }
     }

     function returnSender(){
     /*
              global $intranet_session_language;
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              $username_field = getNameFieldByLang();
              global $i_general_sysadmin;

          $sql = "SELECT CONCAT($username_field, IF (ClassNumber IS NULL OR ClassNumber = '', '', CONCAT(' (',ClassName,'-',ClassNumber,')') ) ) FROM INTRANET_USER WHERE UserID = '".$this->SenderID."' ";
          $row = $this->returnArray($sql,1);
          $x = $row[0][0];
          return ($x == "") ? "$i_general_sysadmin" : $x ;
     }
     function isEmail()
     {
              if ($this->mailType == 2) return true;
              else return false;
     }
     function returniMailSender(){
     /*
              global $intranet_session_language;
              $chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
              $username_field = ($chi? "ChineseName": "EnglishName");
              */
              if ($this->isEmail())
              {
                  return $this->convert2mailto($this->parseEmailAddress($this->SenderEmail));
              }
              else
              {
                  $username_field = getNameFieldWithClassNumberByLang();
                  global $i_general_sysadmin;
                  if ($this->SenderID == 0) return $i_general_sysadmin;
                  $sql = "SELECT $username_field FROM INTRANET_USER WHERE UserID = '".$this->SenderID."' ";
                  $row = $this->returnArray($sql,1);
                  $x = $row[0][0];
                  return ($x == "") ? "$i_general_sysadmin" : $x ;
              }
     }
     function returnRecipient(){
          $x = "";
          $row = $this->returnRecipientOption($this->RecipientID);
          for($i=0; $i<sizeof($row); $i++){
               $x .= ($i!=0 && $i%5==0) ? "<br>" : "";
               $x .= ($i==0 || $i%5==0) ? "" : ", ";
               $x .= $row[$i][1]."\n";
          }
          return $x;
     }
     function parseInternalRecipients_Short($recipientString)
     {
          $x = "";
          $row = $this->returnRecipientOption($recipientString);
          //for($i=0; $i<sizeof($row); $i++){ //}

          for($i=0; $i<10; $i++){
               $x .= ($i!=0 && $i%5==0) ? "<br>" : "";
               $x .= ($i==0 || $i%5==0) ? "" : ", ";
               $x .= $row[$i][1]."\n";
          }
          $x .= ", ...";
          return $x;
     }
     function parseInternalRecipients($recipientString)
     {
          $x = "";
          $row = $this->returnRecipientOption($recipientString);
          for($i=0; $i<sizeof($row); $i++){
               $x .= ($i!=0 && $i%5==0) ? "<br>" : "";
               $x .= ($i==0 || $i%5==0) ? "" : ", ";
               $x .= $row[$i][1]."\n";
          }
          return $x;
     }

     # Return array in (email_address , name)
     function parseEmailAddress($email_string)
     {
              $email_string = trim($email_string);
              $pos = strpos($email_string,"<");
              if ($pos===false)
              {
                  return array($email_string,"");
              }
              else
              {
                  $endpos = strpos($email_string,">");
                  $name_part = trim(substr($email_string,0,$pos));
                  $email_part = trim(substr($email_string,$pos+1,$endpos-$pos-1 ));
                  return array($email_part,$name_part);
              }
     }
     function convert2mailto($email_parts)
     {
              list($email_part,$name_part) = $email_parts;
                 if ($name_part != "")
	                  $x = "<a href=\"compose.php?targetname=".urlencode($name_part)."&targetemail=".urlencode($email_part)."\" title=\"".urlencode($email_part)."\">".intranet_htmlspecialchars($name_part)."</a>";
	              else
	                  $x = "<a href=\"compose.php?targetemail=".urlencode($email_part)."\" title=\"".urlencode($email_part)."\">".intranet_htmlspecialchars($email_part)."</a>";
              return $x;
     }
     function parseExternalRecipients($recipientString)
     {
              $recipientString = trim($recipientString);
              if ($recipientString == "") return "";

              $insideQuote = false;
              $array = array();
              $target_string = "";
              for ($i=0; $i<strlen($recipientString); $i++)
              {
                   $target_char = $recipientString[$i];
                   if ($target_char == ",")
                   {
                       if ($insideQuote)
                       {
                           $target_string .= $target_char;
                       }
                       else
                       {
                           $array[] = $target_string;
                           $target_string = "";
                       }
                   }
                   elseif ($target_char == "\"")
                   {
                       if ($insideQuote)
                       {
                           $insideQuote = false;
                       }
                       else $insideQuote = true;
                   }
                   else
                   {
                       $target_string .= $target_char;
                   }
              }
              $array[] = $target_string;
              #$array = explode(",",$recipientString);

              for ($i=0; $i<sizeof($array); $i++)
              {
                   $email_string = $array[$i];
                   if($email_string == "")
                   		break;
                   ### Check if recipient name is empty or not ###
                   if(!strpos(trim($email_string),"<") === false)
                   {
                   		### if recipient name is empty, than show full email address
                   		//$recipient_name = substr(trim($email_string),1,strlen(trim($email_string)));
                   		//$recipient_name = substr(trim($email_string),strpos(trim($email_string),"<")+1,strpos(trim($email_string),">")-1);
                   		//$email_string = trim($recipient_name.$email_string);
                   }
                   $parsed = $this->parseEmailAddress($email_string);
                   $converted = $this->convert2mailto($parsed);
                   
                   $x .= ($i!=0 && $i%5==0) ? "<br>" : "";
                   $x .= ($i==0 || $i%5==0) ? "" : ", ";
                   $x .= "$converted";
              }
             
              return $x;
     }
     function parseExternalRecipients_Short($recipientString)
     {
              $recipientString = trim($recipientString);
              if ($recipientString == "") return "";

              $insideQuote = false;
              $array = array();
              $target_string = "";
              for ($i=0; $i<strlen($recipientString); $i++)
              {
                   $target_char = $recipientString[$i];
                   if ($target_char == ",")
                   {
                       if ($insideQuote)
                       {
                           $target_string .= $target_char;
                       }
                       else
                       {
                           $array[] = $target_string;
                           $target_string = "";
                       }
                   }
                   elseif ($target_char == "\"")
                   {
                       if ($insideQuote)
                       {
                           $insideQuote = false;
                       }
                       else $insideQuote = true;
                   }
                   else
                   {
                       $target_string .= $target_char;
                   }
              }
              $array[] = $target_string;
              #$array = explode(",",$recipientString);

              for ($i=0; $i<10; $i++)
              {
                   $email_string = $array[$i];
                   if($email_string == "")
                   		break;
                   ### Check if recipient name is empty or not ###
                   if(!strpos(trim($email_string),"<") === false)
                   {
                   		### if recipient name is empty, than show full email address
                   		//$recipient_name = substr(trim($email_string),1,strlen(trim($email_string)));
                   		//$recipient_name = substr(trim($email_string),strpos(trim($email_string),"<")+1,strpos(trim($email_string),">")-1);
                   		//$email_string = trim($recipient_name.$email_string);
                   }
                   $parsed = $this->parseEmailAddress($email_string);
                   $converted = $this->convert2mailto($parsed);
                   $x .= ($i!=0 && $i%5==0) ? "<br>" : "";
                   $x .= ($i==0 || $i%5==0) ? "" : ", ";
                   $x .= "$converted";
              }
              $x .= ", ...";
              return $x;
     }
     function returnIntToRecipients()
     {
              return $this->parseInternalRecipients($this->RecipientID);
     }
     function returnIntToRecipients_Short()
     {
              return $this->parseInternalRecipients_Short($this->RecipientID);
     }
     function returnIntCCRecipients()
     {
              return $this->parseInternalRecipients($this->InternalCC);
     }
     function returnIntCCRecipients_Short()
     {
              return $this->parseInternalRecipients_Short($this->InternalCC);
     }
     function returnIntBCCRecipients()
     {
              return $this->parseInternalRecipients($this->InternalBCC);
     }
     function returnIntBCCRecipients_Short()
     {
              return $this->parseInternalRecipients_Short($this->InternalBCC);
     }
     function returnExtToRecipients()
     {
              return $this->parseExternalRecipients($this->ExternalTo);
     }
     function returnExtToRecipients_Short()
     {
              return $this->parseExternalRecipients_Short($this->ExternalTo);
     }
     function returnExtCCRecipients()
     {
              return $this->parseExternalRecipients($this->ExternalCC);
     }
     function returnExtCCRecipients_Short()
     {
              return $this->parseExternalRecipients_Short($this->ExternalCC);
     }
     function returnExtBCCRecipients()
     {
              return $this->parseExternalRecipients($this->ExternalBCC);
     }
     function returnExtBCCRecipients_Short()
     {
              return $this->parseExternalRecipients_Short($this->ExternalBCC);
     }


     function returnUserName(){
              $namefield = getNameFieldWithClassNumberByLang();
          $sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID = '".$this->Get_Safe_Sql_Query($this->UserID)."'";
          $row = $this->returnArray($sql,1);
          return addslashes($row[0][0]);
     }

     # ------------------------------------------------------------------------------------

     function displayAttachment(){
          global $file_path, $image_path, $intranet_httppath;
          $path = "$file_path/file/mail/".$this->Attachment;

          $a = new libfiletable("", $path, 0, 0, "");
          $files = $a->files;
          while (list($key, $value) = each($files)) {
                 $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                 $url = intranet_handle_url($url);

               $x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
               //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
               $x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
               $x .= " (".ceil($files[$key][1]/1000)."Kb)";
               $x .= "<br>\n";
          }
          return $x;
     }

     function displayCampusMail(){
          global $image_path, $i_frontpage_campusmail_icon_important, $i_frontpage_campusmail_date, $i_frontpage_campusmail_subject, $i_frontpage_campusmail_sender, $i_frontpage_campusmail_message, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_recipients;
          $x = "";
          if($this->UserID<>""){
               $x .= "<table border=0 cellpadding=2 cellspacing=0>\n";
               $x .= "<tr>
                         <td class=campusmail_edge1><img src=$image_path/space.gif width=8 height=5 border=0></td>
                         <td class=campusmail_edge2 width=100><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge3><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge4><img src=$image_path/space.gif width=13 height=5 border=0></td>
                         </tr>\n";
               $x .= "<tr>
                         <td class=campusmail_edge5><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge2 align=right>".(($this->IsImportant) ? $i_frontpage_campusmail_icon_important : "")." $i_frontpage_campusmail_date:</td>
                         <td class=campusmail_edge3>".$this->DateModified."</td>
                         <td class=campusmail_edge6><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         </tr>\n";
               if($this->CampusMailFromID<>""){
               $x .= "<tr>
                         <td class=campusmail_edge5><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge2 align=right>$i_frontpage_campusmail_sender:</td>
                         <td class=campusmail_edge3>".$this->returnSender()."</td>
                         <td class=campusmail_edge6><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         </tr>\n";
               }else{
               $x .= "<tr>
                         <td class=campusmail_edge5><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge2 align=right>$i_frontpage_campusmail_recipients:</td>
                         <td class=campusmail_edge3>".$this->returnRecipient()."</td>
                         <td class=campusmail_edge6><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         </tr>\n";
               }
               $x .= "<tr>
                         <td class=campusmail_edge5><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge2 align=right>$i_frontpage_campusmail_subject:</td>
                         <td class=campusmail_edge3>".$this->Subject."</td>
                         <td class=campusmail_edge6><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         </tr>\n";
               $x .= "<tr>
                         <td class=campusmail_edge7><img src=$image_path/space.gif width=8 height=18 border=0></td>
                         <td class=campusmail_edge8><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge9><img src=$image_path/space.gif width=1 height=1 border=0></td>
                         <td class=campusmail_edge10><img src=$image_path/space.gif width=13 height=18 border=0></td>
                         </tr>\n";
               $x .= "</table>\n";
               $x .= "<br>\n";

               # Convert Message
               if ($this->isHTMLMessage($this->Message))
               {
                   $display_message = $this->Message;
               }
               else
               {
                   $display_message = $this->convertAllLinks2(nl2br(intranet_htmlspecialchars($this->Message)));
               }

               $x .= "<table width=100% border=0 cellpadding=1 cellspacing=0><tr><td bgcolor=#F5D20A>\n";
               $x .= "<table width=100% border=0 cellpadding=5 cellspacing=0 bgcolor=#FFFFFF>\n";
               $x .= "<tr><td width=20% align=right>$i_frontpage_campusmail_message:</td><td width=80%>".$display_message."</td></tr>\n";
               $x .= ($this->IsAttachment) ? "<tr><td align=right>$i_frontpage_campusmail_attachment:</td><td>".$this->displayiMailAttachment()."</td></tr>\n" : "";
               $x .= "</table>\n";
               $x .= "</td></tr></table>\n";
          }
          return $x;
     }

     # /home/campusmail/inbox_view.php      intranet 1.2 design
     function displayCampusMail12(){
          global $image_path, $i_frontpage_campusmail_icon_important, $i_frontpage_campusmail_date, $i_frontpage_campusmail_subject, $i_frontpage_campusmail_sender, $i_frontpage_campusmail_message, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_recipients;
          $x = "";
          if($this->UserID<>""){
               $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>\n";
               $x .= "<tr><td><img src=/images/campusmail/papertop2.gif></td></tr>\n";
               $x .= "</table>\n";
               $x .= "<table width=685 border=0 cellpadding=3 cellspacing=0 background=/images/campusmail/paperbg3.gif class=body>
                <tr>
                  <td width=120 align=right valign=top><strong>".(($this->IsImportant) ? $i_frontpage_campusmail_icon_important : "")."
                    &nbsp;$i_frontpage_campusmail_date : </strong></td>
                  <td width=565 align=left valign=top>".$this->DateModified."</td>
                </tr>\n";
               if($this->CampusMailFromID<>"")
               {
                  $x .= "<tr><td width=120 align=right valign=top><strong>$i_frontpage_campusmail_sender : </strong></td>
                  <td width=565 align=left valign=top>".$this->returnSender()."</td></tr>";
               }
               else
               {
                   $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_recipients : </strong></td>
                  <td width=565 align=left valign=top>".$this->returnRecipient()."</td>
                </tr>";
               }
               $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_subject :</strong></td>
                  <td width=565 align=left valign=top>".intranet_wordwrap($this->Subject,50,"\n",1)."</td>
                </tr>
              </table>";
              $x .= "<table height=150 width=685 border=0 cellpadding=0 cellspacing=0 background=/images/campusmail/paperbg4.gif class=body>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_message :</strong></td>
                  <td width=535>".$this->convertAllLinks2(nl2br($this->Message),50)."</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>";
                if ($this->IsAttachment)
                {
                $x .= "
                <tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_attachment : </strong></td>
                  <td width=535>".$this->displayAttachment()."</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>";
                }
                $x .= "
              </table>";

          }
          return $x;
     }

     function displayCampusMailNotification(){
          global $button_send, $i_frontpage_campusmail_read, $i_frontpage_campusmail_reply;
          $x = "";
          if($this->UserID<>"" && $this->IsNotification){
               $sql = "SELECT IsRead, Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$this->Get_Safe_Sql_Query($this->CampusMailFromID)."' AND UserID = '".$this->Get_Safe_Sql_Query($this->UserID)."'";
               $row = $this->returnArray($sql, 2);
               $this->IsRead = $row[0][0];
               $this->MessageReply = $row[0][1];
               $x .= "<form name=form1 action=inbox_update.php method=post>\n";
               $x .= "<table border=0 cellpadding=2 cellspacing=0>\n";
               $x .= "<tr><td><br></td><td><input type=checkbox name=IsRead value=1 ".(($this->IsRead)?"CHECKED":"")."> $i_frontpage_campusmail_read</td></tr>\n";
               $x .= "<tr><td width=100 align=right>$i_frontpage_campusmail_reply:</td><td><textarea name=Message cols=30 rows=5>".cleanHtmlJavascript($this->MessageReply)."</textarea></td></tr>\n";
               $x .= "<tr><td><br></td><td><input class=submit type=submit value=\"$button_send\"></td></tr>\n";
               $x .= "</table>\n";
               $x .= "<input type=hidden name=CampusMailID value=".escape_double_quotes($this->CampusMailID).">\n";
               $x .= "</form>\n";
          } else {
               $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '1' WHERE CampusMailID = '".$this->Get_Safe_Sql_Query($this->CampusMailID)."' AND UserID = '".$this->Get_Safe_Sql_Query($this->UserID)."'";
               $this->db_db_query($sql);
          }
          return $x;
     }

     function displayCampusMailNotification12(){
          global $button_send, $i_frontpage_campusmail_read, $i_frontpage_campusmail_reply,$i_frontpage_campusmail_read;
          global $i_frontpage_campusmail_notification_instruction,$i_frontpage_campusmail_notification_instruction_message_only,$i_frontpage_campusmail_alert_not_reply;
          global $image_send;
          $x = "";
          if($this->UserID<>"" && $this->IsNotification){
               $sql = "SELECT IsRead, Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$this->Get_Safe_Sql_Query($this->CampusMailFromID)."' AND UserID = '".$this->Get_Safe_Sql_Query($this->UserID)."'";
               $row = $this->returnArray($sql, 2);
               $this->IsRead = $row[0][0];
               $this->MessageReply = $row[0][1];
               $instruction = "";
               if (!$this->IsRead && !$this->isAutoReply())
               {
                    $checkBox = "<input type=checkbox name=IsRead value=1>$i_frontpage_campusmail_read";
                    $textScript = "onFocus=\"this.form.IsRead.checked=true\"";
                    $instruction = $i_frontpage_campusmail_notification_instruction;
                    $submitScript = "onClick=\"if (!this.form.IsRead.checked) { var send=confirm('$i_frontpage_campusmail_alert_not_reply'); if (send) {history.back(); return false;} else return false; }\"";
//
               }
               else
               {
                    $checkBox = "<input type=hidden name=IsRead value=1>";
                    $textScript = "";
                    $submitScript = "";
               }
               if ($this->MessageReply == "")
               {
                   if ($instruction == "") $instruction = $i_frontpage_campusmail_notification_instruction_message_only;
                   $replyBox = "<textarea name=Message cols=50 rows=10 $textScript></textarea>";
                   $submitButton = "<input type=image src=$image_send alt='$button_send' $submitScript>
                          <input type=hidden name=CampusMailID value=".$this->CampusMailID.">";
               }
               else
               {
                   $replyBox = "<table border=0 width=100%><tr><td height=30>".intranet_wordwrap($this->MessageReply,40,"\n",1)."</td></tr></table>\n";
                   $submitButton = "";
               }
               $instruction_row = ($instruction ==""?"":"<tr><td align=center><table width=400 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#FBFCF0 bordercolordark=#F7CF73 class=body><tr><td>$instruction</td></tr></table></td></tr>\n");
               $x .= "<table width=685 border=0 cellpadding=10 cellspacing=0 background=/images/campusmail/paperbg4.gif>\n";
               $x .= $instruction_row;
               $x .= "<tr>
                  <td align=center><table width=400 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#FBFCF0 bordercolordark=#F7CF73 class=body>
                      <tr>
                        <td bgcolor=#FFE1A0><strong>$i_frontpage_campusmail_reply :</strong></td>
                      </tr>
                      <tr>
                        <td>
                        <form name=form1 action=inbox_update.php method=post>$checkBox<br>
                          $replyBox
                          <br>
                          $submitButton
                          </form>
                        </td>
                      </tr>
                    </table></td>
                </tr>
              </table>\n";
              if ($this->isAutoReply())
              {
                  $sql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET IsRead = '1' WHERE CampusMailID = '".$this->Get_Safe_Sql_Query($this->CampusMailFromID)."' AND UserID = '".$this->Get_Safe_Sql_Query($this->UserID)."'";
                  $this->db_db_query($sql);
              }
          }
          $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '1' WHERE (RecordStatus IS NULL OR RecordStatus NOT IN (1,2)) AND CampusMailID = '".$this->Get_Safe_Sql_Query($this->CampusMailID)."' AND UserID = '".$this->Get_Safe_Sql_Query($this->UserID)."'";
          $this->db_db_query($sql);
          return $x;
     }

     function viewPaperBottom()
     {
              $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>
                <tr>
                  <td><img src=/images/campusmail/paperbottom2.gif></td>
                </tr>
              </table>\n";
              return $x;
     }

     function displayCampusMailReply(){
          global $image_path, $i_frontpage_campusmail_read, $i_frontpage_campusmail_unread, $i_frontpage_campusmail_status, $i_frontpage_campusmail_icon_read, $i_frontpage_campusmail_icon_unread;
          $x = "";
          if($this->UserID<>"" && $this->IsNotification){
               $row = $this->returnCampusMailReply();
               $TotalReadFlag = sizeof($row);
               $ReadFlag = 0;
               for($i=0; $i<$TotalReadFlag; $i++){
                    $UserName = $row[$i][1];
                    $IsRead = $row[$i][4];
                    $Message = $row[$i][5];
                    if($IsRead == 1) $ReadFlag++;
               }
               $x .= "<table border=0 cellpadding=0 cellspacing=0>\n";
               $x .= "<tr>\n";
               $x .= "<td><img src=$image_path/frontpage/campusmail/side1.gif border=0></td>\n";
               $x .= "<td class=campusmail_darkgreen>$i_frontpage_campusmail_status: ".$ReadFlag." $i_frontpage_campusmail_icon_read $i_frontpage_campusmail_read <img src=$image_path/space.gif width=10 height=10 border=0> ".($TotalReadFlag - $ReadFlag)." $i_frontpage_campusmail_icon_unread $i_frontpage_campusmail_unread</td>\n";
               $x .= "<td><img src=$image_path/frontpage/campusmail/side2.gif border=0></td>\n";
               $x .= "</tr>\n";
               $x .= "</table>\n";
               $x .= "<table width=400 border=0 cellpadding=2 cellspacing=0>\n";
               for($i=0; $i<$TotalReadFlag; $i++){
                    $UserName = $row[$i][1];
                    $IsRead = $row[$i][4];
                    $Message = $row[$i][5];
                    $x .= "<tr ".(($IsRead) ? "class=campusmail_green" : "class=campusmail_lightgreen").">\n";
                    $x .= "<td width=30%>$UserName<br></td>\n";
                    $x .= "<td width=70%>$Message<br></td>\n";
                    $x .= "<td>".(($IsRead) ? $i_frontpage_campusmail_icon_read : $i_frontpage_campusmail_icon_unread)."</td>\n";
                    $x .= "</tr>\n";
               }
               $x .= "</table>\n";
          }
          return $x;
     }

     # /home/campusmail/outbox_view.php       intranet12
     function displayCampusMailReply12(){
          global $image_path, $i_frontpage_campusmail_read, $i_frontpage_campusmail_unread, $i_frontpage_campusmail_status, $i_frontpage_campusmail_icon_read, $i_frontpage_campusmail_icon_unread;
          $x = "";
          if($this->UserID<>"" && $this->IsNotification){
               $row = $this->returnCampusMailReply();
               $TotalReadFlag = sizeof($row);
               $ReadFlag = 0;
               for($i=0; $i<$TotalReadFlag; $i++){
                    $UserName = $row[$i][1];
                    $IsRead = $row[$i][4];
                    $Message = intranet_wordwrap($row[$i][5],20,"\n",1);
                    if($IsRead == 1) $ReadFlag++;
               }
               $x .= "<table width=490 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#BAF9FC bordercolordark=#555555 class=h1>\n";
               $x .= "<tr>\n";
               $x .= "<td style=\"background-image: url(/images/campusmail/paperbg5.gif)\">$i_frontpage_campusmail_status : $i_frontpage_campusmail_icon_read
                    <font color=#3E78B4>$ReadFlag</font> $i_frontpage_campusmail_read $i_frontpage_campusmail_icon_unread
                    <font color=#3E78B4>".($TotalReadFlag - $ReadFlag)."</font> $i_frontpage_campusmail_unread</td>";
               $x .= "</tr>\n";
               $x .= "<td bgcolor=#BAF9FC><table width=470 border=0 cellpadding=3 cellspacing=0 class=body>";
               for($i=0; $i<$TotalReadFlag; $i++){
                    $UserName = $row[$i][1];
                    $IsRead = $row[$i][4];
                    $Message = $row[$i][5];
                    $x .= "<tr>\n";
                    $x .= "<td width=240>$UserName<br></td>\n";
                    $x .= "<td width=200 >$Message<br></td>\n";
                    $x .= "<td width=30 align=right>".(($IsRead) ? $i_frontpage_campusmail_icon_read : $i_frontpage_campusmail_icon_unread)."</td>\n";
                    $x .= "</tr>\n";
               }
               $x .= "</table>\n";
               $x .= "</td></tr></table>\n";
          }
          return $x;
     }
     # /home/imail/outbox_view.php       eClass IP - iMail
     function displayCampusMailReplyiMail(){
          global $image_path, $i_frontpage_campusmail_read, $i_frontpage_campusmail_unread, $i_frontpage_campusmail_status, $i_frontpage_campusmail_icon_read, $i_frontpage_campusmail_icon_unread;
          $x = "";
          if($this->UserID<>"" && $this->IsNotification){
               $row = $this->returnCampusiMailReply();
               $TotalReadFlag = sizeof($row);
               $ReadFlag = 0;
               for($i=0; $i<$TotalReadFlag; $i++){
                   $IsRead = $row[$i][4];
                    if($IsRead == 1) $ReadFlag++;
               }
               $x .= "<table width=490 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#BAF9FC bordercolordark=#555555 class=h1>\n";
               $x .= "<tr>\n";
               $x .= "<td style=\"background-image: url(/images/campusmail/paperbg5.gif)\">$i_frontpage_campusmail_status : $i_frontpage_campusmail_icon_read
                    <font color=#3E78B4>$ReadFlag</font> $i_frontpage_campusmail_read $i_frontpage_campusmail_icon_unread
                    <font color=#3E78B4>".($TotalReadFlag - $ReadFlag)."</font> $i_frontpage_campusmail_unread</td>";
               $x .= "</tr>\n";
               $x .= "<td bgcolor=#BAF9FC><table width=470 border=0 cellpadding=3 cellspacing=0 class=body>";
               global $intranet_httppath,$i_general_show_child;
               $right2Send = auth_sendmail();
               for($i=0; $i<$TotalReadFlag; $i++){
                    list ($uid, $UserName, $ReplyID, $MailID, $IsRead, $Message, $id_type) = $row[$i];
                    $Message = intranet_wordwrap($Message,20,"\n",1);

                    $Message = intranet_undo_htmlspecialchars($Message);
                    if ($right2Send)
                    {
                        $UserName = "<a class=functionlink_new href=compose.php?CampusMailReplyID=$ReplyID>$UserName</a>";
                    }
                    if ($id_type==3)
                    {
                        $UserName .= " <a title='$i_general_show_child' onMouseOver=\"window.status='$i_general_show_child';return true;\" onMouseOut=\"window.status='';return true;\" href=javascript:newWindow('$intranet_httppath/home/view_child.php?uid=$uid',1)><img src=\"$image_path/icon_parentlink.gif\" border=0></a>";
                    }
#                    $UserName = $row[$i][1];
#                    $IsRead = $row[$i][4];
#                    $Message = $row[$i][5];
                    $x .= "<tr>\n";
                    $x .= "<td width=240>$UserName<br></td>\n";
                    $x .= "<td width=200 >$Message<br></td>\n";
                    $x .= "<td width=30 align=right>".(($IsRead) ? $i_frontpage_campusmail_icon_read : $i_frontpage_campusmail_icon_unread)."</td>\n";
                    $x .= "</tr>\n";
               }
               $x .= "</table>\n";
               $x .= "</td></tr></table>\n";
          }
          return $x;
     }
     // Check new campus mail
     function returnNumNewMessage($__UserID)
     {
           $sql = "SELECT COUNT(CampusMailID) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$this->Get_Safe_Sql_Query($__UserID)."' AND RecordType = 2 AND RecordStatus IS NULL AND CampusMailFromID IS NOT NULL AND Subject IS NOT NULL";
           $result = $this->returnVector($sql,1);
           return $result[0];
     }
     # iMail version
     function returnNumNewMessage_iMail($__UserID)
     {
              //$sql = "SELECT COUNT(CampusMailID) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$__UserID' AND UserFolderID = 2 AND RecordStatus IS NULL AND Subject IS NOT NULL AND Deleted != 1";
              //$result = $this->returnVector($sql,1);
              //return $result[0];
              return $this->GetCampusMailNewMailCount($__UserID, 2);
     }
     # ------------------------------------------------------------------------------------

     function sendMail($recipients, $subject, $message, $senderID="")
     {
              $delimiter = "";
              $list = "";
              $values = "";
              for ($i=0; $i<sizeof($recipients); $i++)
              {
                   $uid = $recipients[$i];
                   $list .= "$delimiter"."U$uid";
                   $delimiter = ",";
              }
              $sql = "LOCK TABLES INTRANET_CAMPUSMAIL WRITE";
              $this->db_db_query($sql);

              # Make a copy to sender outbox if sender is not ADMIN
              if ($senderID != "")
              {
                  $sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL
                          (UserID,SenderID,RecipientID,Subject,Message,IsAttachment,AttachmentSize
                          ,IsImportant,IsNotification,RecordType,UserFolderID,DateInput,DateModified)
                          VALUES
                          ('".$this->Get_Safe_Sql_Query($senderID)."','".$this->Get_Safe_Sql_Query($senderID)."','".$this->Get_Safe_Sql_Query($list)."','".$this->Get_Safe_Sql_Query($subject)."','".$this->Get_Safe_Sql_Query($message)."','0',0
                          ,'0','','0',0,now(),now())";

                  $this->db_db_query($sql);
                  $MailFromID = $this->db_insert_id();
                  $imail_conds = " OR CampusMailID = '".$this->Get_Safe_Sql_Query($MailFromID)."'";
              }
              else
              {
                  $MailFromID = -1;
                  $update_from_field = ",CampusMailFromID = NULL";
              }
              if ($MailFromID == 0)
              {
                  $sql = "UNLOCK TABLES";
                  $this->db_db_query($sql);
                  return false;
              }
              $delimiter = "";
              for ($i=0; $i<sizeof($recipients); $i++)
              {
                   $uid = $recipients[$i];
                   $values .= "$delimiter ('$MailFromID','$uid')";
                   $delimiter = ",";
              }

              $sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL
                      (CampusMailFromID,UserID)
                      VALUES $values";
              $this->db_db_query($sql);

              global $special_feature;
              if ($special_feature['imail'])
              {
                  $sql = "UPDATE INTRANET_CAMPUSMAIL SET MailType = 1
                                 WHERE CampusMailFromID = '".$this->Get_Safe_Sql_Query($MailFromID)."' $imail_conds";
                  $this->db_db_query($sql);
              }
              $sql = "UPDATE INTRANET_CAMPUSMAIL SET SenderID = '".$this->Get_Safe_Sql_Query($senderID)."',
                                 RecipientID = '".$this->Get_Safe_Sql_Query($list)."', Subject = '".$this->Get_Safe_Sql_Query($subject)."', Message = '".$this->Get_Safe_Sql_Query($message)."',
                                 IsAttachment = '0', AttachmentSize = '', IsImportant = '0',
                                 IsNotification = '', RecordType = '2', UserFolderID = '2',
                                 DateInput=now(), DateModified=now()
                                 $update_from_field
                             WHERE CampusMailFromID = '".$this->Get_Safe_Sql_Query($MailFromID)."'";
              $this->db_db_query($sql);


              $sql = "UNLOCK TABLES";
              $this->db_db_query($sql);
              return true;
     }

     # Kenneth: Remember to edit the code in eClass too
     function sendMailByEmail($recipientEmails, $subject, $message, $senderEmail="")
     {
              $email_list = "'".implode("','",$this->Get_Safe_Sql_Query($recipientEmails))."'";
              $sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail IN ($email_list)";
              $recipients = $this->returnVector($sql);

              if ($senderEmail != "")
              {
                  $sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail = '".$this->Get_Safe_Sql_Query($senderEmail)."'";
                  $temp = $this->returnVector($sql);
                  $senderID = $temp[0];
              }
              else
              {
                  $senderID = "";
              }

              $delimiter = "";
              $list = "";
              $values = "";
              for ($i=0; $i<sizeof($recipients); $i++)
              {
                   $uid = $recipients[$i];
                   $list .= "$delimiter"."U$uid";
                   $delimiter = ",";
              }
              $sql = "LOCK TABLES INTRANET_CAMPUSMAIL WRITE";
              $this->db_db_query($sql);

              # Make a copy to sender outbox if sender is not ADMIN
              if ($senderID != "")
              {
                  $sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL
                          (UserID,SenderID,RecipientID,Subject,Message,IsAttachment,AttachmentSize
                          ,IsImportant,IsNotification,RecordType,UserFolderID,DateInput,DateModified)
                          VALUES
                          ('".$this->Get_Safe_Sql_Query($senderID)."','".$this->Get_Safe_Sql_Query($senderID)."','".$this->Get_Safe_Sql_Query($list)."','".$this->Get_Safe_Sql_Query($subject)."','".$this->Get_Safe_Sql_Query($message)."','0',0
                          ,'0','','0',0,now(),now())";
                  $this->db_db_query($sql);
                  $MailFromID = $this->db_insert_id();
                  $imail_conds = " OR CampusMailID = '".$this->Get_Safe_Sql_Query($MailFromID)."'";
              }
              else
              {
                  $MailFromID = -1;
                  $update_from_field = ",CampusMailFromID = NULL";
              }
              if ($MailFromID == 0)
              {
                  $sql = "UNLOCK TABLES";
                  $this->db_db_query($sql);
                  return false;
              }
              $delimiter = "";
              for ($i=0; $i<sizeof($recipients); $i++)
              {
                   $uid = $recipients[$i];
                   $values .= "$delimiter ('".$this->Get_Safe_Sql_Query($MailFromID)."','".$this->Get_Safe_Sql_Query($uid)."')";
                   $delimiter = ",";
              }

              $sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL
                      (CampusMailFromID,UserID)
                      VALUES $values";
              $this->db_db_query($sql);

              global $special_feature;
              if ($special_feature['imail'])
              {
                  $sql = "UPDATE INTRANET_CAMPUSMAIL SET MailType = 1
                                 WHERE CampusMailFromID = '".$this->Get_Safe_Sql_Query($MailFromID)."' $imail_conds";
                  $this->db_db_query($sql);
              }
              $sql = "UPDATE INTRANET_CAMPUSMAIL SET SenderID = '".$this->Get_Safe_Sql_Query($senderID)."',
                                 RecipientID = '".$this->Get_Safe_Sql_Query($list)."', Subject = '".$this->Get_Safe_Sql_Query($subject)."', Message = '".$this->Get_Safe_Sql_Query($message)."',
                                 IsAttachment = '0', AttachmentSize = '', IsImportant = '0',
                                 IsNotification = '', RecordType = '2', UserFolderID = '2',
                                 DateInput=now(), DateModified=now()
                                 $update_from_field
                             WHERE CampusMailFromID = '".$this->Get_Safe_Sql_Query($MailFromID)."'";
              $this->db_db_query($sql);

              $sql = "UNLOCK TABLES";
              $this->db_db_query($sql);
              return true;
     }

     function returnNextID($folder=2)
     {
              global $UserID;
              $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$this->Get_Safe_Sql_Query($UserID)."' AND RecordType = '".$this->Get_Safe_Sql_Query($folder)."'
                             AND unix_timestamp(DateInput) > unix_timestamp('".$this->DateInput . "') ORDER BY DateInput ASC LIMIT 1";
              $result = $this->returnVector($sql);
              return $result[0];
     }
     function returnPrevID($folder=2)
     {
              global $UserID;
              $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$this->Get_Safe_Sql_Query($UserID)."' AND RecordType = '".$this->Get_Safe_Sql_Query($folder)."'
                             AND unix_timestamp(DateInput) < unix_timestamp('".$this->DateInput . "') ORDER BY DateInput DESC LIMIT 1";
              $result = $this->returnVector($sql);
              return $result[0];
     }

     # iMail functions
     # /home/campusmail/viewmail.php      iMail
     function displayiMail(){
          global $image_path, $i_frontpage_campusmail_icon_important, $i_frontpage_campusmail_date, $i_frontpage_campusmail_subject, $i_frontpage_campusmail_sender, $i_frontpage_campusmail_message, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_recipients;
          global $i_CampusMail_New_InternalRecipients,$i_CampusMail_New_ExternalRecipients
                 ,$i_CampusMail_New_To,$i_CampusMail_New_CC,$i_CampusMail_New_BCC
                 ,$i_CampusMail_New_Reply,$i_CampusMail_New_ReplyAll,$i_CampusMail_New_Forward;
          global $view_remove_html,$i_CampusMail_New_ViewFormat_PlainText,$i_CampusMail_New_ViewFormat_HTML,$button_remove;
          global $i_CampusMail_New_ViewFormat_MessageSource;

          global $nextID,$prevID;

          $x = "";
          if($this->UserID<>""){
               $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>\n";
               $x .= "<tr><td><img src=/images/campusmail/papertop2.gif></td></tr>\n";
               $x .= "</table>\n";
               $x .= "<table width=685 border=0 cellpadding=3 cellspacing=0 background=/images/campusmail/paperbg3.gif class=body>
                <tr>
                  <td colspan=2>
                    <table width=95% border=0 cellpadding=2 cellspacing=2 align=center>
                      <tr>
                        <td width=70%>
                          <a href=compose.php?CampusMailID=".$this->CampusMailID."&action=R><img src=\"$image_path/frontpage/imail/icon_reply.gif\" border=0 alt='$i_CampusMail_New_Reply'>$i_CampusMail_New_Reply</a>
                          &nbsp;|&nbsp;<a href=compose.php?CampusMailID=".$this->CampusMailID."&action=RA><img src=\"$image_path/frontpage/imail/icon_replyall.gif\" border=0 alt='$i_CampusMail_New_ReplyAll'>$i_CampusMail_New_ReplyAll</a>
                          &nbsp;|&nbsp;<a href=compose.php?CampusMailID=".$this->CampusMailID."&action=F><img src=\"$image_path/frontpage/imail/icon_forward.gif\" border=0 alt='$i_CampusMail_New_Forward'>$i_CampusMail_New_Forward</a>
                          &nbsp;|&nbsp;<a href=javascript:removeMail()><img src=\"$image_path/icon_clearrejectrecord.gif\" border=0 alt='$button_remove'>$button_remove</a>
                        </td>
                        <td width=30% align=right>";

                        /*
              if (false && $this->isHTMLMessage($this->Message))
              {
                $x .= "<a href=viewmail.php?CampusMailID=".$this->CampusMailID."&view_remove_html=".($view_remove_html==1? 0:1).">".($view_remove_html==1? "$i_CampusMail_New_ViewFormat_HTML":"$i_CampusMail_New_ViewFormat_PlainText")."</a> | \n";
              }
              */

              $view_msg_source_link = "<a href=javascript:viewMessageSource()> $i_CampusMail_New_ViewFormat_MessageSource</a>";
              $x .= $view_msg_source_link;
               $x .="
                       </td>
                      </tr>
                    </table></td>
                  </tr>
                <tr><td colspan=2>&nbsp;</td></tr>\n";
                $x .= "<tr>
                  <td width=120 align=right valign=top><strong>".(($this->IsImportant) ? $i_frontpage_campusmail_icon_important : "")."
                    &nbsp;$i_frontpage_campusmail_date : </strong></td>
                  <td width=565 align=left valign=top>".$this->DateModified."</td>
                </tr>\n";
                  $x .= "<tr><td width=120 align=right valign=top><strong>$i_frontpage_campusmail_sender : </strong></td>
                  <td width=565 align=left valign=top>".$this->returniMailSender()."</td></tr>";

                  # Recipients
                  /*
                   $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_recipients : </strong></td>
                  <td width=565 align=left valign=top>".$this->returnRecipient()."</td>
                </tr>";
*/
                ## Get recipients
                $int_to = $this->returnIntToRecipients();
                $int_cc = $this->returnIntCCRecipients();
                $int_bcc = $this->returnIntBCCRecipients();
                $ext_to = $this->returnExtToRecipients();
                $ext_cc = $this->returnExtCCRecipients();
                $ext_bcc = $this->returnExtBCCRecipients();

                ## Internal (has internal recipients)
                if ($int_to!="" || $int_cc!="" || $int_bcc!="")
                {
                    $recipient_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>";
                    if ($int_to != "")
                    {
                        $recipient_table .= "<tr><td width=30><I>$i_CampusMail_New_To:</I></td><td>$int_to</td></tr>\n";
                    }
                    if ($int_cc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I>$i_CampusMail_New_CC:</I></td><td>$int_cc</td></tr>\n";
                    }
                    if ($int_bcc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I>$i_CampusMail_New_BCC:</I></td><td>$int_bcc</td></tr>\n";
                    }
                    $recipient_table .= "</table>";
                   $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_CampusMail_New_InternalRecipients : </strong></td>
                  <td width=565 align=left valign=top>$recipient_table</td>
                </tr>";
                }

                ## External (has external recipients)
                if ($ext_to!=""||$ext_cc!=""||$ext_bcc!="")
                {
                    $recipient_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>";
                    if ($ext_to != "")
                    {
                        $recipient_table .= "<tr><td width=30><I>$i_CampusMail_New_To:</I></td><td>$ext_to</td></tr>\n";
                    }
                    if ($ext_cc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I>$i_CampusMail_New_CC:</I></td><td>$ext_cc</td></tr>\n";
                    }
                    if ($ext_bcc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I>$i_CampusMail_New_BCC:</I></td><td>$ext_bcc</td></tr>\n";
                    }
                    $recipient_table .= "</table>";
                   $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_CampusMail_New_ExternalRecipients : </strong></td>
                  <td width=565 align=left valign=top>$recipient_table</td>
                </tr>";
                }

               $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_subject :</strong></td>
                  <td width=565 align=left valign=top>".intranet_wordwrap($this->Subject,50,"\n",1)."</td>
                </tr>
              </table>";
              $this->Message = $this->view_image_embed_message($this->Message);
              /*
              $temp_pos = strpos($this->Message,"</body></html>");
              if ($temp_pos !== false)
              {
                  $this->Message = substr($this->Message, 0, $temp_pos+14);
              }
              */

              # Convert Message
              /*
              if ($this->isHTML == 1)
              {
                  $html_flag = true;
              }
              else if ($this->isHTML == -1)
              {
                   $html_flag = false;
              }
              else
              {
                  $html_flag = $this->isHTMLMessage($this->Message);
              }
              */
              $html_flag = $this->isHTMLMessage($this->Message);
              if ($html_flag)
              {
                  $display_message = $this->Message;
                  if ($view_remove_html == 1)
                  {
                      $display_message = "<pre>".($this->removeHTMLtags($display_message))."</pre>";
                  }
                  else
                  {
                      #$display_message = $this->convertCodedString($display_message);
                  }
              }
              else
              {
                  $text = $this->removeHTMLtags($this->Message);
                  $text = nl2br($text); #nl2br(intranet_htmlspecialchars($text));
                  $text = $this->convertURLS2($text);
                  $text = $this->convertEmailToiMailCompose($text);
                  #$text = $this->convertMail2($text);
                  $display_message = $text;
              }

              $x .= "<table height=150 width=685 border=0 cellpadding=0 cellspacing=0 background=/images/campusmail/paperbg4.gif class=body>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_message :</strong></td>
                  <td width=535>".$display_message."</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>";
                if ($this->IsAttachment)
                {
                $x .= "
                <tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_attachment : </strong></td>
                  <td width=535>".$this->displayiMailAttachment()."</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>";
                }
                $x .= "
              </table>";

          }
          return $x;
     }


     # iMail functions
     # used in /home/imail/viewmail.php (viewmail_content.php)     iMail
     function displayiMail2(){
          global $image_path, $i_frontpage_campusmail_icon_important, $i_frontpage_campusmail_date, $i_frontpage_campusmail_subject, $i_frontpage_campusmail_sender, $i_frontpage_campusmail_message, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_recipients;
          global $i_CampusMail_New_InternalRecipients,$i_CampusMail_New_ExternalRecipients
                 ,$i_CampusMail_New_To,$i_CampusMail_New_CC,$i_CampusMail_New_BCC
                 ,$i_CampusMail_New_Reply,$i_CampusMail_New_ReplyAll,$i_CampusMail_New_Forward;
          global $view_remove_html,$i_CampusMail_New_ViewFormat_PlainText,$i_CampusMail_New_ViewFormat_HTML,$button_remove;
          global $i_CampusMail_New_ViewFormat_MessageSource;
		  global $intranet_session_language;
          global $nextID,$prevID;

        # warning message
        $forward_link = "";
        if($intranet_session_language=="b5")
        	$sys_enc = "big5";
        else if($intranet_session_language=="gb")
        	$sys_enc = "gb2312";
        else if($intranet_session_language=="en"){
        	if(is_array($intranet_default_lang_set) && in_array("b5",$intranet_default_lang_set))
        		$sys_enc = "big5";
        	else if(is_array($intranet_default_lang_set) && in_array("gb",$intranet_default_lang_set))
        		$sys_enc = "gb2312";
        	else $sys_enc="big5";
		}

        $mail_enc =$this->MessageEncoding==""?"iso-8859-1":strtolower($this->MessageEncoding);
        if($mail_enc!="iso-8859-1" && $mail_enc!=$sys_enc && $this->Attachment!="" ){
                //$forward_link="href='#' onClick=\"javascript:if(parent.warning_msg()) location.href='compose.php?CampusMailID=".$this->CampusMailID."&action=F';\"";
                $forward_link="href=\"javascript:if(parent.warning_msg()) location.href='compose.php?CampusMailID=".$this->CampusMailID."&action=F';\"";
        }else{
                $forward_link = "href='compose.php?CampusMailID=".$this->CampusMailID."&action=F'";
        }
        # end warning message
          $x = "";
          if($this->UserID<>""){
               $x .= "<table width=685 border=0 cellspacing=0 cellpadding=0>\n";
               $x .= "<tr><td><img src=/images/campusmail/papertop2.gif></td></tr>\n";
               $x .= "</table>\n";
               $x .= "<table width=685 border=0 cellpadding=3 cellspacing=0 background=/images/campusmail/paperbg3.gif class=body>
                <tr>
                  <td colspan=2>
                    <table width=95% border=0 cellpadding=2 cellspacing=2 align=center>
                      <tr>
                        <td width=70%>
                          <a href=compose.php?CampusMailID=".$this->CampusMailID."&action=R><img src=\"$image_path/frontpage/imail/icon_reply.gif\" border=0 alt='' name='reply_alt'><span id='reply'></span></a>
                          &nbsp;|&nbsp;<a href=compose.php?CampusMailID=".$this->CampusMailID."&action=RA><img src=\"$image_path/frontpage/imail/icon_replyall.gif\" border=0 alt='' name='replyall_alt'><span id='replyall'></span></a>
                          &nbsp;|&nbsp;<a ".$forward_link."><img src=\"$image_path/frontpage/imail/icon_forward.gif\" border=0 alt='' name='forward_alt'><span id='forward'></span></a>
                          &nbsp;|&nbsp;<a href=javascript:removeMail()><img src=\"$image_path/icon_clearrejectrecord.gif\" border=0 alt='' name='delete_alt'><span id='delete'></span></a>
                        </td>
                        <td width=30% align=right>";

                        /*
              if (false && $this->isHTMLMessage($this->Message))
              {
                $x .= "<a href=viewmail.php?CampusMailID=".$this->CampusMailID."&view_remove_html=".($view_remove_html==1? 0:1).">".($view_remove_html==1? "$i_CampusMail_New_ViewFormat_HTML":"$i_CampusMail_New_ViewFormat_PlainText")."</a> | \n";
              }
              */

              $view_msg_source_link = "<a href=javascript:viewMessageSource()><span id='view_source'></span></a>";
              $x .= $view_msg_source_link;
               $x .="
                       </td>
                      </tr>
                    </table></td>
                  </tr>
                <tr><td colspan=2>&nbsp;</td></tr>\n";
                $x .= "<tr>
                  <td width=120 align=right valign=top><strong>".(($this->IsImportant) ? $i_frontpage_campusmail_icon_important : "")."
                    &nbsp;<span id='date'></span> : </strong></td>
                  <td width=565 align=left valign=top>".$this->DateModified."</td>
                </tr>\n";
                  $x .= "<tr><td width=120 align=right valign=top><strong><span id='sender'></span> : </strong></td>
                  <td width=565 align=left valign=top>".$this->returniMailSender()."</td></tr>";

                # Recipients
                  /*
                   $x .= "<tr>
                  <td width=120 align=right valign=top><strong>$i_frontpage_campusmail_recipients : </strong></td>
                  <td width=565 align=left valign=top>".$this->returnRecipient()."</td>
                </tr>";
*/
                ## Get recipients
                $int_to = $this->returnIntToRecipients();
                $int_cc = $this->returnIntCCRecipients();
                $int_bcc = $this->returnIntBCCRecipients();
                $ext_to = $this->returnExtToRecipients();
                $ext_cc = $this->returnExtCCRecipients();
                $ext_bcc = $this->returnExtBCCRecipients();

                ## Internal (has internal recipients)
                if ($int_to!="" || $int_cc!="" || $int_bcc!="")
                {
                    $recipient_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>";
                    if ($int_to != "")
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='int_new_to'></span>:</I></td><td>$int_to</td></tr>\n";
                    }
                    if ($int_cc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='int_new_cc'></span>:</I></td><td>$int_cc</td></tr>\n";
                    }
                    if ($int_bcc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='int_new_bcc'></span>:</I></td><td>$int_bcc</td></tr>\n";
                    }
                    $recipient_table .= "</table>";
                   $x .= "<tr>
                  <td width=120 align='right' valign='top'><strong><span id='int_recipient'></span> : </strong></td>
                  <td width=565 align='left' valign='top'>$recipient_table</td>
                </tr>";
                }

                ## External (has external recipients)
                if ($ext_to!=""||$ext_cc!=""||$ext_bcc!="")
                {
                    $recipient_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>";
                    if ($ext_to != "")
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='ext_new_to'></span>:</I></td><td>$ext_to</td></tr>\n";
                    }
                    if ($ext_cc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='ext_new_cc'></span>:</I></td><td>$ext_cc</td></tr>\n";
                    }
                    if ($ext_bcc != "")
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='ext_new_bcc'></span>:</I></td><td>$ext_bcc</td></tr>\n";
                    }
                    $recipient_table .= "</table>";
                   $x .= "<tr>
                  <td width=120 align=right valign=top><strong><span id='ext_recipient'></span> : </strong></td>
                  <td width=565 align=left valign=top>$recipient_table</td>
                </tr>";
                }

               $x .= "<tr>
                  <td width=120 align=right valign=top><strong><span id='subject'></span> :</strong></td>
                  <td width=565 align=left valign=top>".intranet_wordwrap($this->Subject,50,"\n",1)."</td>
                </tr>";
                if ($this->IsAttachment)
                {
                $x .= "
                <tr>
                  <td width=120 align=right valign=top><strong><span id='attachment'></span> : </strong></td>
                  <td width=535>".$this->displayiMailAttachment()."</td>
                </tr>";
                }
              $x.="</table>";
              $this->Message = $this->view_image_embed_message($this->Message);
              /*
              $temp_pos = strpos($this->Message,"</body></html>");
              if ($temp_pos !== false)
              {
                  $this->Message = substr($this->Message, 0, $temp_pos+14);
              }
              */

              # Convert Message
              /*
              if ($this->isHTML == 1)
              {
                  $html_flag = true;
              }
              else if ($this->isHTML == -1)
              {
                   $html_flag = false;
              }
              else
              {
                  $html_flag = $this->isHTMLMessage($this->Message);
              }
              */
              $html_flag = $this->isHTMLMessage($this->Message);

              if ($html_flag)
              {
                  //$display_message = nl2br($this->Message);
                  $display_message = $this->Message;
                  if ($view_remove_html == 1)
                  {
                      $display_message = "<pre>".($this->removeHTMLtags($display_message))."</pre>";
                  }
                  else
                  {
                      #$display_message = $this->convertCodedString($display_message);
                  }
              }
              else
              {
                  $text = $this->removeHTMLtags($this->Message);
                  $text = nl2br($text); #nl2br(intranet_htmlspecialchars($text));
                  $text = $this->convertURLS2($text);
                  $text = $this->convertEmailToiMailCompose($text);
                  #$text = $this->convertMail2($text);
                  $display_message = $text;
              }

              $x .= "<table height=150 width=685 border=0 cellpadding=0 cellspacing=0 background=/images/campusmail/paperbg4.gif class=body>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top><strong><span id='message'></span> :</strong></td>
                  <td width=535>".$display_message."</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>";
                /*
                if ($this->IsAttachment)
                {
                $x .= "
                <tr>
                  <td width=120 align=right valign=top><strong><span id='attachment'></span> : </strong></td>
                  <td width=535>".$this->displayiMailAttachment()."</td>
                  <td width=30>&nbsp;</td>
                </tr>
                <tr>
                  <td width=120 align=right valign=top>&nbsp;</td>
                  <td width=535>&nbsp;</td>
                  <td width=30>&nbsp;</td>
                </tr>";
                }
                */
                $x .= "
              </table>";

          }
          return $x;
     }
     function isHTMLMessage($message)
     {
              $removed_tag_message = removeHTMLtags($message);
              if ($removed_tag_message == $message)
              {
                  return false;
              }
              else
              {
                  #return true;
                  $message = strtolower($message);
                  if (strpos($message, '<xbody')===0)
                  {
                      return true;
                  }
                  else if (strpos($message, '<xhtml')===0)
                  {
                       return true;
                  }
                  else if (strpos($message, '</div>') !== false)
                  {
                       return true;
                  }
                  else if (strpos($message, '<br') !== false)
                  {
                       return true;
                  }
                  else if (strpos($message, '<p') !== false)
                  {
                       return true;
                  }
                  else if(strpos($message,'<table')!==false){
                           return true;
                      }
                  else return false;
              }
              /*
              */
     }
     /*
     function convertCodedString($string)
     {
              $coded = str_replace("=\r\n","",$string);
              if ($coded == $string)
              {
                  return $string;
              }
              else
              {
                  return quoted_printable_decode($coded);
              }
     }
     */
     function removeHTMLtags($message)
     {
              $message = str_replace("</div>","</div>\n",$message);
              $message = str_replace("</DIV>","</DIV>\n",$message);
              //$message = preg_replace("/<base[^>]+>/i","",$message); // remove <base href="" target=""> tags
              
              return strip_tags ($message);
     /*
              $pos_start = strpos($message,"<");
              $pos_end = strpos($message,">");
              echo "<!-- S: $pos_start E: $pos_end -->\n";
              if ($pos_start === false || $pos_end === false || $pos_end < $pos_start)  # Base case
              {
                  return $message;
              }
              else
              {
                  $substring1 = substr($message, 0, $pos_start);
                  $substring2 = substr($message, $pos_end);
                  $message = $substring1.$substring2;
                  return $this->removeHTMLtags($message);
              }
              */

     }

     # used in /home/imail/viewmail.php (viewmail_content.php) iMail
     function displayiMailNotification2()
     {
          global $button_send, $i_frontpage_campusmail_read, $i_frontpage_campusmail_reply,$i_frontpage_campusmail_read;
          global $i_frontpage_campusmail_notification_instruction,$i_frontpage_campusmail_notification_instruction_message_only,$i_frontpage_campusmail_alert_not_reply;
          global $image_send;
          $x = "";

          if($this->UserID<>"" && $this->IsNotification && !$this->isEmail()){
             # Check whether Sender's copy still exists
             $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE CampusMailID = '".$this->CampusMailFromID."' ";
             $temp = $this->returnVector($sql);
             if ($temp[0]!="")           # Still exists
             {
                 $sql = "SELECT IsRead, Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$this->CampusMailFromID."' AND UserID = '".$this->UserID."' ";
                 $row = $this->returnArray($sql, 2);
                 $this->IsRead = $row[0][0];
                 # $this->MessageReply = $row[0][1];
                 $this->MessageReply = intranet_undo_htmlspecialchars($row[0][1]);
                 $instruction = "";
                 if (!$this->IsRead && !$this->isAutoReply())
                 {
                      $checkBox = "<input type=checkbox name=IsRead value=1><span id='campusmail_read'>$i_frontpage_campusmail_read</span>";
                      $textScript = "onFocus=\"this.form.IsRead.checked=true\"";
                      $instruction = $i_frontpage_campusmail_notification_instruction;
                      $instruction_id=1;
                      $submitScript = "onClick=\"if (!this.form.IsRead.checked) { var send=confirmNotification(); if (send) {history.back(); return false;} else return false; }\"";

                 }
                 else
                 {
                     $checkBox = "<input type=hidden name=IsRead value=1>";
                     $textScript = "";
                     $submitScript = "";
                 }
                 if ($this->MessageReply == "")
                 {
                     if ($instruction == ""){
                             $instruction = $i_frontpage_campusmail_notification_instruction_message_only;
                             $instruction_id=2;
                         }
                     $replyBox = "<textarea name=Message cols=80 rows=5 $textScript></textarea>";
                     $submitButton = "<input type=image src=$image_send alt='$button_send' $submitScript>
                          <input type=hidden name=CampusMailID value=".$this->CampusMailID.">";
                     global $viewtype;
                     if ($viewtype != "")
                     {
                         $submitButton .= "\n<input type=hidden name=viewtype value=\"$viewtype\">";
                     }
                 }
                 else
                 {
                     $replyBox = "<table border=0 width=100%><tr><td height=30>".intranet_wordwrap($this->MessageReply,40,"\n",1)."</td></tr></table>\n";
                     $submitButton = "";
                 }
                 $instruction_row = ($instruction ==""?"":"<tr><td width='728' align='left' valign='top'><table align=left width=685 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#FBFCF0 bordercolordark=#F7CF73 class=body><tr><td><span id='instruction$instruction_id'>$instruction</span></td></tr></table></td></tr>\n");

                  //$x .= "<table width=685 border=0 cellpadding=10 cellspacing=0 background=/images/campusmail/paperbg4.gif>\n";
                 $x .= "<table width=728 border=0 cellpadding=2 cellspacing=0>\n";

                 $x .= $instruction_row;
                 $x .= "<tr>
                        <td align=left><table width=685 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#FBFCF0 bordercolordark=#F7CF73 class=body>
                        <tr>
                          <td bgcolor=#FFE1A0><strong><span id='campusmail_reply'>$i_frontpage_campusmail_reply</span> :</strong></td>
                        </tr>
                        <tr>
                          <td>
                          <form name=form1 action=inbox_update.php method=post>$checkBox<br>
                            $replyBox
                            <br>
                            $submitButton
                            </form>
                          </td>
                        </tr>
                      </table></td>
                  </tr>
                </table>\n";
                 if ($this->isAutoReply())
                 {
                     $sql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET IsRead = '1' WHERE CampusMailID = '".$this->CampusMailFromID."' AND UserID = '".$this->UserID."' ";
                     $this->db_db_query($sql);
                 }
             }
             else
             {
                 $sql = "UPDATE INTRANET_CAMPUSMAIL SET IsNotification = '0' WHERE CampusMailID = '".$this->CampusMailID."' ";
                 $this->db_db_query($sql);
             }
          }
          $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '1' WHERE (RecordStatus IS NULL OR RecordStatus NOT IN (1,2)) AND CampusMailID = '".$this->CampusMailID."' AND UserID = '".$this->UserID."' ";
          $this->db_db_query($sql);
          return $x;
     }

       function displayiMailNotification(){
          global $button_send, $i_frontpage_campusmail_read, $i_frontpage_campusmail_reply,$i_frontpage_campusmail_read;
          global $i_frontpage_campusmail_notification_instruction,$i_frontpage_campusmail_notification_instruction_message_only,$i_frontpage_campusmail_alert_not_reply;
          global $image_send;
          $x = "";

          if($this->UserID<>"" && $this->IsNotification && !$this->isEmail()){
             # Check whether Sender's copy still exists
             $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE CampusMailID = '".$this->CampusMailFromID."' ";
             $temp = $this->returnVector($sql);
             if ($temp[0]!="")           # Still exists
             {
                 $sql = "SELECT IsRead, Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$this->CampusMailFromID."' AND UserID = '".$this->UserID."'";
                 $row = $this->returnArray($sql, 2);
                 $this->IsRead = $row[0][0];
                 $this->MessageReply = $row[0][1];
                 $instruction = "";
                 if (!$this->IsRead && !$this->isAutoReply())
                 {
                      $checkBox = "<input type=checkbox name=IsRead value=1>$i_frontpage_campusmail_read";
                      $textScript = "onFocus=\"this.form.IsRead.checked=true\"";
                      $instruction = $i_frontpage_campusmail_notification_instruction;
                      $submitScript = "onClick=\"if (!this.form.IsRead.checked) { var send=confirm('$i_frontpage_campusmail_alert_not_reply'); if (send) {history.back(); return false;} else return false; }\"";

                 }
                 else
                 {
                     $checkBox = "<input type=hidden name=IsRead value=1>";
                     $textScript = "";
                     $submitScript = "";
                 }
                 if ($this->MessageReply == "")
                 {
                     if ($instruction == "")
                             $instruction = $i_frontpage_campusmail_notification_instruction_message_only;
                     $replyBox = "<textarea name=Message cols=50 rows=10 $textScript></textarea>";
                     $submitButton = "<input type=image src=$image_send alt='$button_send' $submitScript>
                          <input type=hidden name=CampusMailID value=".$this->CampusMailID.">";
                     global $viewtype;
                     if ($viewtype != "")
                     {
                         $submitButton .= "\n<input type=hidden name=viewtype value=\"$viewtype\">";
                     }
                 }
                 else
                 {
                     $replyBox = "<table border=0 width=100%><tr><td height=30>".intranet_wordwrap($this->MessageReply,40,"\n",1)."</td></tr></table>\n";
                     $submitButton = "";
                 }
                 $instruction_row = ($instruction ==""?"":"<tr><td align=center><table width=400 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#FBFCF0 bordercolordark=#F7CF73 class=body><tr><td>$instruction</td></tr></table></td></tr>\n");
                 $x .= "<table width=685 border=0 cellpadding=10 cellspacing=0 background=/images/campusmail/paperbg4.gif>\n";
                 $x .= $instruction_row;
                 $x .= "<tr>
                        <td align=center><table width=400 border=1 cellpadding=10 cellspacing=0 bordercolorlight=#FBFCF0 bordercolordark=#F7CF73 class=body>
                        <tr>
                          <td bgcolor=#FFE1A0><strong>$i_frontpage_campusmail_reply :</strong></td>
                        </tr>
                        <tr>
                          <td>
                          <form name=form1 action=inbox_update.php method=post>$checkBox<br>
                            $replyBox
                            <br>
                            $submitButton
                            </form>
                          </td>
                        </tr>
                      </table></td>
                  </tr>
                </table>\n";
                 if ($this->isAutoReply())
                 {
                     $sql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET IsRead = '1' WHERE CampusMailID = '".$this->CampusMailFromID."' AND UserID = '".$this->UserID."'";
                     $this->db_db_query($sql);
                 }
             }
             else
             {
                 $sql = "UPDATE INTRANET_CAMPUSMAIL SET IsNotification = '0' WHERE CampusMailID = '".$this->CampusMailID."'";
                 $this->db_db_query($sql);
             }
          }
          $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '1' WHERE (RecordStatus IS NULL OR RecordStatus NOT IN (1,2)) AND CampusMailID = '".$this->CampusMailID."' AND UserID = '".$this->UserID."' ";
          $this->db_db_query($sql);
          return $x;
     }
     function returniMailPrevID ($folder=2)
     {
			global $UserID;
            if($folder != -1){
            	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$folder' AND Deleted != 1
            				AND DateInput > '".$this->DateInput . "' ORDER BY DateInput ASC";
        	}else{
	        	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$folder' AND Deleted = 1
            				AND DateInput > '".$this->DateInput . "' ORDER BY DateInput ASC";
        	}
			$result = $this->returnVector($sql);
			return $result[0];
     }
     function returniMailNextID($folder=2)
     {
            global $UserID;
            if($folder != -1){
            	$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$folder' AND Deleted != 1
            	    		AND DateInput < '".$this->DateInput . "' ORDER BY DateInput DESC";
			}else{
				$sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '$folder' AND Deleted = 1
                			AND DateInput < '".$this->DateInput . "' ORDER BY DateInput DESC";
            }
            $result = $this->returnVector($sql);
            return $result[0];
     }

     # Retrieve the ID and names of Recipients storing in INTRANET_CAMPUSMAIL.Recipient
     # Param: Recipient - String
     # Return: Array of ( (G/U)ID, Name, )
     function getRecipientNames($string)
     {
     		  global $Lang;
              $array = $this->returnRecipientIDArray($string);
              $grouplist = $array[0];
              $userlist = $array[1];
              $parentlist = $array[3];
              $TeacherGroupList = $array[4];
              $StudentGroupList = $array[5];
              $ParentGroupList = $array[6];
              $groupTeacher = array();
              $groupStudent = array();
              $groupParent = array();

              $sql = "SELECT CONCAT('G',GroupID), Title FROM INTRANET_GROUP WHERE GroupID IN ($grouplist) ORDER BY Title";
              $groups = $this->returnArray($sql,2);
              $namefield = getNameFieldWithClassNumberByLang();
              $sql = "SELECT CONCAT('U',UserID), $namefield FROM INTRANET_USER WHERE UserID IN ($userlist) ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,'0'), EnglishName";
              $users = $this->returnArray($sql,2);
              $sql = "SELECT CONCAT('P',UserID), CONCAT($namefield,'\'s Parent') FROM INTRANET_USER WHERE UserID IN ($parentlist) ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,'0'), EnglishName";
              $parents = $this->returnArray($sql,2);

              if($TeacherGroupList!=""){
              		$tmp_arr = explode(",",$TeacherGroupList);
              		for($i=0; $i<sizeof($TeacherGroupList); $i++)
              		{
              			$groupTeacher[$i][] = "T".$TeacherGroupList[$i];
              			switch($TeacherGroupList[$i]){
              				case 1:
              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'];
              					break;
              				case 2:
              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToFormTeachingStaff'];
              					break;
              				case 3:
              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToClassTeachingStaff'];
              					break;
              				case 4:
              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'];
              					break;
              				case 5:
              					$groupTeacher[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'];
              					break;
              			}
              		}
              }
              if($StudentGroupList!=""){
              		$tmp_arr = explode(",",$StudentGroupList);
              		for($i=0; $i<sizeof($StudentGroupList); $i++)
              		{
              			$groupStudent[$i][] = "S".$StudentGroupList[$i];

              			switch($StudentGroupList[$i]){
              				case 1:
              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToIndividualsStudent'];
              					break;
              				case 2:
              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToFormStudent'];
              					break;
              				case 3:
              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToClassStudent'];
              					break;
              				case 4:
              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectStudent'];
              					break;
              				case 5:
              					$groupStudent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'];
              					break;
              			}
              		}
              }
              if($ParentGroupList!=""){
              		$tmp_arr = explode(",",$ParentGroupList);
              		for($i=0; $i<sizeof($ParentGroupList); $i++)
              		{
              			$groupParent[$i][] = "S".$ParentGroupList[$i];
              			switch($ParentGroupList[$i]){
              				case 1:
              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToIndividualsParents'];
              					break;
              				case 2:
              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToFormParents'];
              					break;
              				case 3:
              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToClassParents'];
              					break;
              				case 4:
              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectParents'];
              					break;
              				case 5:
              					$groupParent[$i][] = $Lang['iMail']['FieldTitle']['ToSubjectGroupParents'];
              					break;
              			}
              		}
              }
              return array_merge($groups,$users,$parents,$groupTeacher,$groupStudent,$groupParent);
     }
     function getiMailAttachments()
     {
              if ($this->CampusMailFromID != "" && $this->CampusMailFromID > 0)
              {
                  $conds = "CampusMailID = '".$this->CampusMailFromID."'";
              }
              else
              {
                  $conds = "CampusMailID = '".$this->CampusMailID."'";
              }
              #$sql = "SELECT PartID, AttachmentPath, FileName, FileSize FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE CampusMailID = '".$this->CampusMailID."' OR CampusMailID = '".$this->CampusMailFromID."' ORDER BY FileName";
              $sql = "SELECT PartID, AttachmentPath, FileName, FileSize FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE $conds ORDER BY FileName";
              //debug_r($sql);
              return $this->returnArray($sql,4);
     }
/*
    function displayiMailAttachment(){
          global $file_path, $image_path, $intranet_httppath;

          # Try to browse DB
          $parts = $this->getiMailAttachments();
          if (sizeof($parts)!=0)
          {
                  $x="<script language='javascript'>
                                  function download_file(id){
                                          obj = document.getElementById('f'+id);
                                          text=obj.innerHTML;
                                          window.open('downloadattachment.php?PartID='+id+'&fname='+text);
                            }
                         </script>";
              # Not Direct link
              for ($i=0; $i<sizeof($parts); $i++)
              {
                   list($partID, $attachment_path, $filename, $filesize) = $parts[$i];

                     #$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
                     $x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                     //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                     $x .= "<a onClick='download_file($partID)' onMouseOver=\"window.status='".addslashes($filename)."';return true;\" onMouseOut=\"window.status='';return true;\">".$filename."</a>";
                     $x.="<span id='f$partID' style='visibility:hidden'>$filename</span>";
                     $x .= " (".$filesize."Kb)";
                     $x .= "<br>\n";
              }
          }
          else
          {
              $path = "$file_path/file/mail/".$this->Attachment;
              $a = new libfiletable("", $path, 0, 0, "");
              $files = $a->files;
              while (list($key, $value) = each($files)) {
                   $url = str_replace($file_path, "", $path)."/".$files[$key][0];
                   $url = intranet_handle_url($url);

                     $x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
                     //$x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                     $x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
                     $x .= " (".ceil($files[$key][1]/1000)."Kb)";
                     $x .= "<br>\n";
              }
          }
          return $x;
     }

*/
	 function sortAttachmentFilesByName($a, $b)
	 {
	 	return strcasecmp($a['file'],$b['file']);
	 }
	 
     function displayiMailAttachment(){
          global $file_path, $image_path, $intranet_httppath;
          global $i_CampusMail_ClickHereToDownloadAttachment,$special_feature;	### added by Ronald on 11 Feb 2009

          # Try to browse DB
          if ($this->CampusMailFromID != "" && $this->CampusMailFromID > 0)
          {
              $conds = "CampusMailID = '".$this->CampusMailFromID."'";
          }
          else
          {
              $conds = "CampusMailID = '".$this->CampusMailID."'";
          }
          /*  Old method - orignal will check DB table to see if the filename is empty, then change to view attachment by using file list method
          $sql = "SELECT COUNT(*) FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE $conds ";
          $arrTotalNumOfAttacment = $this->returnVector($sql);
          
          $sql = "SELECT COUNT(FileName) FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE $conds AND (FileName != '' AND FileName IS NOT NULL)";
          $arrNumOfAttacmentWithFileName = $this->returnVector($sql);
          if($arrNumOfAttacmentWithFileName[0] == $arrTotalNumOfAttacment[0])
          {
          		$parts = $this->getiMailAttachments();
	          	if (sizeof($parts)!=0)
	          	{
					# Not Direct link
					for ($i=0; $i<sizeof($parts); $i++)
					{
						list($partID, $attachment_path, $filename, $filesize) = $parts[$i];
						
						$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
						//$x .= "<a target=_blank href=\"downloadattachment.php?PartID=$partID&CampusMailID=".$this->CampusMailID."\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".intranet_undo_htmlspecialchars($filename)."</a>";
						$x .= "<a target=_blank href=\"downloadattachment.php?PartID=$partID&CampusMailID=".$this->CampusMailID."\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".htmlspecialchars_decode(stripslashes($filename))."</a>";
						
						$x .= " (".$filesize."Kb) ";
						
						if($special_feature['imail_alt_downloadt']){
							$x .= "(<a target=_blank href=\"downloadattachment.php?PartID=$partID&CampusMailID=".$this->CampusMailID."&method=2\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".$i_CampusMail_ClickHereToDownloadAttachment."</a>)";
						}
						$x .= "<br>\n";
					}
	          	}
	          	else
	          	{
	              $path = "$file_path/file/mail/".$this->Attachment;
	              $a = new libfiletable("", $path, 0, 0, "");
	              $files = $a->files;
	              while (list($key, $value) = each($files)) {
	
						$url = str_replace($file_path, "", $path)."/".$files[$key][0];
						$url = rawurlencode($file_path.$url);
	
						$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
						$x .= "<a target=_blank href=\"/home/download_attachment.php?target=".$url."\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
	
						$x .= " (".ceil($files[$key][1]/1000)."Kb)";
						$x .= "<br>\n";
	
	              }
	          	}
          }
          else
          {
          		global $intranet_root;
            	include_once("libosaccount.php");
            	include_once("libftp.php");
            	$lftp = new libftp();
            	
            	if ($this->CampusMailFromID != "" && $this->CampusMailFromID > 0)
            	{
            		$conds = "CampusMailID = '".$this->CampusMailFromID."'";
            	}
            	else
            	{
            		$conds = "CampusMailID = '".$this->CampusMailID."'";
            	}
            	
            	$sql = "SELECT DISTINCT AttachmentPath FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE $conds";
            	$arrAttachmentPath = $this->returnVector($sql);
            	$AttachmentPath = $arrAttachmentPath[0];
            	$AttachmentPath = $intranet_root."/file/mail/".$AttachmentPath;
            	
            	$arrFileList = $this->directoryList($AttachmentPath);
            	
            	foreach($arrFileList as $keypath => $fileinfo)
            	{
            		for($i=0; $i<count($fileinfo);$i++)
            		{
            			$filename = $fileinfo[$i]['file'];
            			$filesize = $fileinfo[$i]['size'];
						$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
						$x .= "<a target=_blank href=\"downloadattachment.php?CampusMailID=".$this->CampusMailID."&filename=".rawurlencode($filename)."\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".$filename."</a>";
						$x .= " (".$filesize."Kb)";
						$x .= "<br>\n";						
					} # End of Looping each Files in each Folder
				} # End of Looping each Folder
          }
          */
            global $intranet_root;
	    	include_once("libosaccount.php");
	    	include_once("libftp.php");
	    	$lftp = new libftp();
	    	
	    	if ($this->CampusMailFromID != "" && $this->CampusMailFromID > 0)
	    	{
	    		$conds = "CampusMailID = '".$this->CampusMailFromID."'";
	    	}
	    	else
	    	{
	    		$conds = "CampusMailID = '".$this->CampusMailID."'";
	    	}
	    	
	    	$sql = "SELECT DISTINCT AttachmentPath FROM INTRANET_IMAIL_ATTACHMENT_PART WHERE $conds";
	    	$arrAttachmentPath = $this->returnVector($sql);
	    	if(sizeof($arrAttachmentPath)>0)// Check if there is a valid attachment path
	    	{
		    	$AttachmentPath = $arrAttachmentPath[0];
		    	$AttachmentPath = $intranet_root."/file/mail/".$AttachmentPath;
		    	if(file_exists($AttachmentPath)){
			    	$arrFileList = $this->directoryList($AttachmentPath);
			    	
			    	foreach($arrFileList as $keypath => $fileinfo)
			    	{
			    		usort($fileinfo, array($this,'sortAttachmentFilesByName'));
			    		for($i=0; $i<count($fileinfo);$i++)
			    		{
			    			$filename = $fileinfo[$i]['file'];
			    			$filesize = $fileinfo[$i]['size'];
							$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
							//$x .= "<a target=_blank href=\"downloadattachment.php?CampusMailID=".$this->CampusMailID."&filename=".rawurlencode($filename)."\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".$filename."</a>";
							$x .= "<a target=_blank href=\"downloadattachment.php?CampusMailID=".$this->CampusMailID."&b_filename=".rawurlencode(base64_encode($filename))."\" onMouseOver=\"window.status=format_filename(this.innerHTML);return true;\" onMouseOut=\"window.status='';return true;\">".intranet_htmlspecialchars($filename)."</a>";
							$x .= " (".$filesize."Kb)";
							$x .= "<br>\n";						
						} # End of Looping each Files in each Folder
					} # End of Looping each Folder
		    	}
	    	}
          return $x;
     }

     function convertBadWords($msg)
     {
              global $imail_feature_allowed;
              if (!$imail_feature_allowed['bad_words_filter'])
              {
                   return $msg;
              }
              else
              {
                  global $intranet_root,$imail_bad_words_replacement;
                  if ($imail_bad_words_replacement=="")
                  {
                      $imail_bad_words_replacement = "***";
                  }
                  $base_dir = "$intranet_root/file/templates/";
                  $target_file = "$base_dir"."mail_badwords.txt";
                  $data = trim(get_file_content($target_file));
                  if ($data=="") return $msg;

                  $bad_words = explode("\n",$data);
                  for ($i=0; $i<sizeof($bad_words); $i++)
                  {
                       #$bad_words_noslash[] = trim($bad_words[$i]);
                       #$bad_words_decode[] = trim(html_entity_decode($bad_words[$i]));
                       #$bad_words[$i] = addslashes(trim($bad_words[$i]));
                       $itr_bad_word = trim($bad_words[$i]);
                       if ($itr_bad_word != "")
                       {
                           #$msg = str_replace($itr_bad_word,$imail_bad_words_replacement,$msg);
                           $msg = stri_replace($itr_bad_word,$msg,$imail_bad_words_replacement);

                           $itr_bad_word_undo = intranet_undo_htmlspecialchars($itr_bad_word);
                           if ($itr_bad_word_undo != "")
                           {
#                               $msg = str_replace($itr_bad_word_undo, $imail_bad_words_replacement,$msg);
                           }

                           $itr_bad_word_slashes = addslashes($itr_bad_word);
                           if ($itr_bad_word_slashes != "")
                           {
                               $msg = str_replace($itr_bad_word_slashes, $imail_bad_words_replacement,$msg);
                           }
                           $itr_bad_word_unicode = Big5ToUnicode($itr_bad_word);
                           if ($itr_bad_word_unicode != "")
                           {
                               $msg = str_replace($itr_bad_word_unicode, $imail_bad_words_replacement,$msg);
                           }
                           $itr_bad_word_decode = utf8Encode($itr_bad_word_unicode);
                           if ($itr_bad_word_decode != "")
                           {
                               $msg = str_replace($itr_bad_word_decode, $imail_bad_words_replacement,$msg);
                           }
                       }
                  }

                  return $msg;
              }
     }

     function sendWelcomeMail($new_user_ids)
     {
              $subject = "Welcome to eClass";
              $message = "Welcome to eClass";
              $this->sendMail($new_user_ids, $subject, $message);
     }

     # Convert back embedded message to view
     function view_image_embed_message($message)
     {
              global $embed_attach_key1,$embed_attach_key2;

              # Find pos of attach
              $target_pos = strpos($message,$embed_attach_key1);
              if ($target_pos != 0)
              {
                  # Find pos of end attach
                  $pos_attach_end = strpos($message,$embed_attach_key2,$target_pos);
                  # Get filename inside
                  $start = $target_pos + strlen($embed_attach_key1);
                  $temp_filename = substr($message,$start, $pos_attach_end-$start);

                  # Get 2 sub-string
                  $temp_str1 = substr($message, 0, $target_pos);
                  $temp_str2 = substr($message, $pos_attach_end+strlen($embed_attach_key2));
                  # Get Attachment path
                  if ($this->CampusMailFromID != "")
                  {
                      $id_value = $this->CampusMailFromID;
/*
                      global $UserID;
                      $sql = "SELECT a.PartID FROM INTRANET_IMAIL_ATTACHMENT_PART as a
                                     LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON  (a.CampusMailID = b.CampusMailID OR a.CampusMailID = b.CampusMailFromID) AND b.UserID = $UserID
                                     AND b.UserID IS NOT NULL AND a.FileName = '$temp_filename'
                                     AND a.CampusMailID = '".$this->CampusMailFromID."'
                                     ";
                                     */
                  }
                  else
                  {
                      $id_value = $this->CampusMailID;
                  /*
                      $conds = "CampusMailID = '".$this->CampusMailID."'";
                      $sql = "SELECT PartID FROM INTRANET_IMAIL_ATTACHMENT_PART
                                 WHERE $conds AND FileName = '$temp_filename'";
                                 */
                  }
                  $sql = "SELECT PartID FROM INTRANET_IMAIL_ATTACHMENT_PART
                                 WHERE CampusMailID = '$id_value' AND FileName = '$temp_filename'";
                   #              echo $sql;
                  $temp = $this->returnVector($sql);
                  $t_partID = $temp[0];
                  if ($t_partID != "")
                  {
                  	  ### OLD
                  	  //$inside_text = "<img src=\"downloadattachment.php?PartID=569&CampusMailID=36163\">";
                      $inside_text = "<img src=\"downloadattachment.php?PartID=".$t_partID."&CampusMailID=".$this->CampusMailID."\">";
                  }
                  else
                  {
                      $inside_text = "[image attached $temp_filename]";
                  }
                  $msg = $temp_str1.$inside_text.$temp_str2;
                  return $this->view_image_embed_message($msg);
              }
              else
              {
                  return $message;
              }
     }

     # Convert embedded image from Outlook
     function convert_image_embeded_message($message, $embed_ids)
     {
              global $embed_attach_key1,$embed_attach_key2;
			  
              for ($i=0; $i<sizeof($embed_ids); $i++)
              {
                   list ($e_content_id, $e_filename) = $embed_ids[$i];

                   # Find pos
                   $target_string = "cid:".$e_content_id;
                   $pos_cid = strpos($message, $target_string);
                   
                   if ($pos_cid != 0)
                   {
                       # Find img tag
                       $temp_substr = substr($message, 0, $pos_cid);
                       //$pos_img_start = strrpos($temp_substr, "<IMG ");		## OLD
                       $pos_img_start = strrpos(strtoupper($temp_substr), "<IMG ");
                       if ($pos_img_start != 0)
                       {
                           # Find the end of this tag
                           $pos_img_end = strpos($message, ">", $pos_cid);
                           
                           if ($pos_img_end != 0)
                           {
                               # replace the img tag
                               $str_replacement = "$embed_attach_key1$e_filename$embed_attach_key2";
                               $str_1 = substr($message, 0, $pos_img_start);
                               $str_3 = substr($message, $pos_img_end+1);
                               $message = $str_1 . $str_replacement.$str_3;
                           }
                       }
                   }
              }
			  // exit;
              return $message;
     }

        function convertEmailToiMailCompose($text) {
                $text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a href='compose.php?targetemail=\\0'>\\0</a>", $text);
                return $text;
        }

        function convertEmailAddressComma ($recipientString)
        {
              $insideQuote = false;
              $array = array();
              $target_string = "";
              for ($i=0; $i<strlen($recipientString); $i++)
              {
                   $target_char = $recipientString[$i];
                   if ($target_char == ",")
                   {
                       if ($insideQuote)
                       {
                           $target_string .= $target_char;
                       }
                       else
                       {
                           $target_string .= ";";
                       }
                   }
                   elseif ($target_char == "\"")
                   {
                       if ($insideQuote)
                       {
                           $insideQuote = false;
                       }
                       else $insideQuote = true;
                       $target_string .= $target_char;
                   }
                   else
                   {
                       $target_string .= $target_char;
                   }
              }
              return $target_string;
        }
## Adding double quotes (" ") to the external recipient user name
## used in /home/imail/compose_update.php
function externalRecipientFormatting($recipients,$is_array=1){
        $address = array();
        $address_list = array();

        if((is_string($recipients) && trim($recipients)=="") || (is_array($recipients) && count($recipients)==0))
                return array();
                
        if($is_array)
        	$temp = $recipients;
        else
        	$temp = explode(";",$recipients);

        for($i=0;$i<sizeof($temp);$i++){

                $address[$i] = trim($temp[$i]);
                $address_list[$i]=trim($temp[$i]);

                $addr = trim($temp[$i]);
                $last_pos = strrpos($addr,"<");
                if($last_pos!==false){
                        $temp_name = trim(substr($addr,0,$last_pos));
                        $temp_addr = trim(substr($addr,$last_pos));
                        if($temp_name!=""){
                                $temp_name = stripslashes($temp_name);

                                // if the username has been quoted by " " .
                                if(substr($temp_name,0,1)=="\"" && substr($temp_name,strlen($temp_name)-1)=="\""){
                                        $temp_name = substr($temp_name,1,strlen($temp_name)-2);
                                }
;
                                $address[$i] = "\""."=?UTF-8?B?".base64_encode(addslashes($temp_name))."?= "."\""." ".$temp_addr;	// this one is used for PHP mail(), so need to encode by base64
                                //$address_list[$i] = "\""."=?UTF-8?B?".base64_encode(intranet_htmlspecialchars(addslashes($temp_name)))."?= "."\""." ".$temp_addr;
                                $address_list[$i] = "\"".intranet_htmlspecialchars(addslashes($temp_name))."\""." ".$temp_addr;		// this one will store in DB, so no need to encode by base64

                                /*
                                $b = explode('?', $address[$i]);
								header("Content-Type:text/html; charset=".$b[1].";");
								var_dump($b);
								var_dump(base64_decode($b[3]));
								*/
                        }
                }
        }

        $address_list = implode(",",$address_list);
        return array($address,$address_list);
}

## 1. Convert the semi-colon (;) in external recipient user name to comma (,)
## 2. Convert the delimiter comma (,) to semi-colon (;)
## used in :/home/imail/compose.php
##                         /home/imail/compose_content2.php
      function convertExternalEmailAddressComma($recipientString)
        {
              $insideQuote = false;
              $array = array();
              $target_string = "";
              $name_string="";
              for ($i=0; $i<strlen($recipientString); $i++)
              {
                   $target_char = $recipientString[$i];

                   if($target_char=="\""){
                           if(!$insideQuote){ ## Start of Recipient Name
                                        $insideQuote = true;
                               }else{ ## End of Recipient Name
                                               $insideQuote = false;
                                               ## Convert
                                               if($name_string!=""){
                                                       $name_string = intranet_undo_htmlspecialchars($name_string);
                                                       $name_string = str_replace(";",",",$name_string);
                                                       $name_string = intranet_htmlspecialchars($name_string);
                                                       $name_string = "\"".$name_string."\"";
                                                       $target_string.=$name_string;
                                                       $name_string = "";
                                               }
                                   }
                       }
                   else if($target_char==","){
                           if(!$insideQuote){
                                   $target_string.=";";
                               }else{
                                       $name_string .=$target_char;
                                   }
                       }else if(!$insideQuote){
                               $target_string.=$target_char;
                           }else{
                                   $name_string.=$target_char;
                               }

              }
              return $target_string;
        }

        function getRelatedGroupTeacherList($TeacherGroupList)
		{
			include_once("role_manage.php");
			include_once("form_class_manage.php");
			include_once("libgrouping.php");

			global $UserID;
			$lrole = new role_manage();
			$fcm = new form_class_manage();
			$lgrouping = new libgrouping();

			$identity = $lrole->Get_Identity_Type($UserID);
			$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
			$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
			$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		    $GroupTeacherArray = explode(",",$TeacherGroupList);

		    for($i=0; $i<sizeof($GroupTeacherArray); $i++)
			{
				if($GroupTeacherArray[$i] == 1)
				{
					## All Teaching Staff ##
					if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
					{
						$all_sql = "(SELECT all_user.UserID FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1)";
						$sql = $all_sql;
					}
				}
				if($GroupTeacherArray[$i] == 2)
				{
					## Form Teacher ##
					if($identity == "Teaching")
					{
						$form_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
					if($identity == "NonTeaching")
					{
						## Non teaching suppose cannnot send to form teacher
					}
					if($identity == "Student")
					{
						$form_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";

						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
					if($identity == "Parent")
					{
						$form_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";

						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
				}
				if($GroupTeacherArray[$i] == 3)
				{
					## Class Teacher ##
					if($identity == "Teaching")
					{
						$class_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
					if($identity == "NonTeaching")
					{
						## Non teaching suppose cannnot send to class teacher
					}
					if($identity == "Student")
					{
						$class_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
					if($identity == "Parent")
					{
						$class_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.YearClassID IN (SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
				}
				if($GroupTeacherArray[$i] == 4)
				{
					## Subject Teacher ##
					if($identity == "Teaching")
					{
						$subject_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
					if($identity == "NonTeaching")
					{
						## Non teaching suppose cannnot send to subject teacher
					}
					if($identity == "Student")
					{
						$subject_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
					if($identity == "Parent")
					{
						$subject_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
				}
				if($GroupTeacherArray[$i] == 5)
				{
					## Subject Group Teacher ##
					if($identity == "Teaching")
					{
						$subject_group_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
					if($identity == "NonTeaching")
					{
						## Non teaching suppose cannnot send to subject group teacher
					}
					if($identity == "Student")
					{
						$subject_group_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
					if($identity == "Parent")
					{
						$subject_group_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE b.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
				}
			}
			$result = $this->returnVector($sql);
			return $result;
		}

		function getRelatedGroupStudentList($StudentGroupList)
		{
			include_once("role_manage.php");
			include_once("form_class_manage.php");
			include_once("libgrouping.php");

			global $UserID;
			$lrole = new role_manage();
			$fcm = new form_class_manage();
			$lgrouping = new libgrouping();

			$identity = $lrole->Get_Identity_Type($UserID);
			$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
			$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
			$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		    $GroupStudentArray = explode(",",$StudentGroupList);

			for($i=0; $i<sizeof($GroupStudentArray); $i++)
			{
				if($GroupStudentArray[$i] == 1)
				{
					## All Student ##
					if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
					{
						//$all_sql = "(SELECT DISTINCT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND (ClassName != '' OR ClassName != NULL) AND (ClassNumber != '' OR ClassNumber != NULL) ORDER BY ClassName, ClassNumber, EnglishName)";
						$all_sql = "(SELECT DISTINCT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
						$sql = $all_sql;
					}
				}
				if($GroupStudentArray[$i] == 2)
				{
					## My Form Student ##
					if($identity == "Teaching")
					{
						$form_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
					if($identity == "NonTeaching")
					{
						## support staff suppose cannot send to form student
					}
					if($identity == "Student")
					{
						$form_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
					if($identity == "Parent")
					{
						$form_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
				}
				if($GroupStudentArray[$i] == 3)
				{
					## My Class Student ##
					if($identity == "Teaching")
					{
						$class_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
					if($identity == "NonTeaching")
					{
						## support staff suppose cannot send to class student
					}
					if($identity == "Student")
					{
						$class_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
					if($identity == "Parent")
					{
						$class_sql = "(SELECT DISTINCT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
				}
				if($GroupStudentArray[$i] == 4)
				{
					## My Subject Student ##
					if($identity == "Teaching")
					{
						$subject_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
					if($identity == "NonTeaching")
					{
						## support staff suppose cannot send to Subject student
					}
					if($identity == "Student")
					{
						$subject_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
					if($identity == "Parent")
					{
						$subject_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
				}
				if($GroupStudentArray[$i] == 5)
				{
					## My Subject Group Student ##
					if($identity == "Teaching")
					{
						$subject_group_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
					if($identity == "NonTeaching")
					{
						## support staff suppose cannot send to subject group student
					}
					if($identity == "Student")
					{
						$subject_group_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
					if($identity == "Parent")
					{
						$subject_group_sql = "(SELECT DISTINCT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID') AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY b.ClassName, b.ClassNumber, b.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
				}
			}
			$result = $this->returnVector($sql);
			return $result;
		}
		function getRelatedGroupParentList($ParentGroupList)
		{
			include_once("role_manage.php");
			include_once("form_class_manage.php");
			include_once("libgrouping.php");

			global $UserID;
			$lrole = new role_manage();
			$fcm = new form_class_manage();
			$lgrouping = new libgrouping();

			$identity = $lrole->Get_Identity_Type($UserID);
			$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
			$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
			$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		    $GroupParentArray = explode(",",$ParentGroupList);

		    for($i=0; $i<sizeof($GroupParentArray); $i++)
			{
				if($GroupParentArray[$i] == 1)
				{
					## All Parents ##
					if(($identity == "Teaching") || ($identity == "NonTeaching") || ($identity == "Student") || ($identity == "Parent"))
					{
						//$all_sql = "(SELECT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT DISTINCT StudentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						$all_sql = "(SELECT DISTINCT a.UserID FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
						$sql = $all_sql;
					}
				}
				if($GroupParentArray[$i] == 2)
				{
					## My Form Parents ##
					if($identity == "Teaching")
					{
						$form_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
					if($identity == "NonTeaching")
					{
						## Support Staff suppose cannot send to form parent
					}
					if($identity == "Student")
					{
						$form_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
					if($identity == "Parent")
					{
						$form_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN (SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID') AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$form_sql;
					}
				}
				if($GroupParentArray[$i] == 3)
				{
					## My Class Parents ##
					if($identity == "Teaching")
					{
						$class_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
					if($identity == "NonTeaching")
					{
						## Support Staff suppose cannot send to class parent
					}
					if($identity == "Student")
					{
						$class_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
					if($identity == "Parent")
					{
						$class_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN (SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$class_sql;
					}
				}
				if($GroupParentArray[$i] == 4)
				{
					## My Subject Parents ##
					if($identity == "Teaching")
					{
						$subject_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
					if($identity == "NonTeaching")
					{
						## Support Staff suppose cannot send to subject parent
					}
					if($identity == "Student")
					{
						$subject_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
					if($identity == "Parent")
					{
						$subject_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_sql;
					}
				}
				if($GroupParentArray[$i] == 5)
				{
					## My Subject Group Parents ##
					if($identity == "Teaching")
					{
						$subject_group_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
					if($identity == "NonTeaching")
					{
						## Support Staff suppose cannot send to subject group parent
					}
					if($identity == "Student")
					{
						$subject_group_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
					if($identity == "Parent")
					{
						$subject_group_sql = "(SELECT DISTINCT b.ParentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE b.StudentID IN (SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN (SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID')) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName)";
						if($sql != "")
							$delimiter = " UNION ";

						$sql .= $delimiter.$subject_group_sql;
					}
				}
			}
			$result = $this->returnVector($sql);
			return $result;
		}

		function HasRawMessage($CampusMailID='') {
			if ($CampusMailID > 0) {
				$sql = "SELECT COUNT(*) FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID = '$CampusMailID'";
				$result = $this->returnVector($sql);
			}
			if ($result[0] > 0) {
				return true;
			} else {
				return false;
			}
		}

		function GrabRawMessage($CampusMailID='') {
			if ($CampusMailID > 0) {
				$sql = "SELECT Message FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID = '$CampusMailID'";
				$result = $this->returnArray($sql);
			}
			if ($result[0][0] != '') {
				return $result[0][0];
			} else {
				return null;
			}
		}

		function GrabMessageCharset($CampusMailID='') {
			if ($CampusMailID > 0) {

				$sql = "SELECT MessageEncoding FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID = '$CampusMailID'";
				$result = $this->returnArray($sql);
			}

			if ($result[0][0] != '') {
				return $result[0][0];
			} else {
				return null;
			}

		}

		function GrabHeaderPath($CampusMailID='') {
			if ($CampusMailID > 0) {
				$sql = "SELECT HeaderFilePath FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID = '$CampusMailID'";
				$result = $this->returnArray($sql);
			}

			if ($result[0][0] != '') {
				return $result[0][0];
			} else {
				return null;
			}
		}

		function GrabMessagePath($CampusMailID='') {
			if ($CampusMailID > 0) {
				$sql = "SELECT MessageFilePath FROM INTRANET_RAWCAMPUSMAIL WHERE CampusMailID = '$CampusMailID'";
				$result = $this->returnArray($sql);
			}

			if ($result[0][0] != '') {
				return $result[0][0];
			} else {
				return null;
			}
		}
		
		function smartstripslashes($str) {
			$str = preg_replace("/<base[^>]+>/i","",$str); // remove <base href="" target=""> tags
		  $cd1 = substr_count($str, "\"");
		  $cd2 = substr_count($str, "\\\"");
		  $cs1 = substr_count($str, "'");
		  $cs2 = substr_count($str, "\\'");
		  $tmp = strtr($str, array("\\\"" => "", "\\'" => ""));
		  $cb1 = substr_count($tmp, "\\");
		  $cb2 = substr_count($tmp, "\\\\");
		  if ($cd1 == $cd2 && $cs1 == $cs2 && $cb1 == 2 * $cb2) {
		    return strtr($str, array("\\\"" => "\"", "\\'" => "'", "\\\\" => "\\"));
		  }
		  return $str;
		}
		
		function returnAllAvailableRecipientUserIDArray($UserIdPrefix="")
		{
			global $intranet_root, $sys_custom, $special_feature, $webmail_identity_allowed, $UserID;
			include_once($intranet_root."/includes/libuser.php");
			include_once($intranet_root."/includes/role_manage.php");
			include_once($intranet_root."/includes/form_class_manage.php");
			include_once($intranet_root."/includes/libgrouping.php");
			include_once($intranet_root."/includes/libclass.php");
			
			//$user_id = $_SESSION['UserID'];
        	//$user_type =  $_SESSION['UserType'];
			
			$li = new libuser($UserID);
			$lrole = new role_manage();
			$fcm = new form_class_manage();
			$lgrouping = new libgrouping();
			
			$identity = $lrole->Get_Identity_Type($UserID);
			$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
			$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
			$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
			
			$result_to_options['ToTeachingStaffOption'] = 0;
			$result_to_options['ToNonTeachingStaffOption'] = 0;
			$result_to_options['ToStudentOption'] = 0;
			$result_to_options['ToParentOption'] = 0;
			$result_to_options['ToAlumniOption'] = 0;
			
			$result_to_options['ToTeacherAndStaff'] = 0;
			if($sys_custom['iMail_RecipientCategory_StaffAndTeacher'])
			{
				if(($identity == "Teaching") || ($identity == "NonTeaching"))
				{
					$result_to_options['ToTeacherAndStaff'] = 1;
				}
			}
			
			if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup']))
			{
				if($sys_custom['iMail_RemoveTeacherCat']) {
					$result_to_options['ToTeachingStaffOption'] = 0;
				} else {
					$result_to_options['ToTeachingStaffOption'] = 1;
				}
			}
			if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']))
			{
				if($sys_custom['iMail_RemoveNonTeachingCat']) {
					$result_to_options['ToNonTeachingStaffOption'] = 0;
				} else {
					$result_to_options['ToNonTeachingStaffOption'] = 1;
				}
			}
			if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Student-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup']))
			{
				$result_to_options['ToStudentOption'] = 1;
			}
			if(($_SESSION['SSV_USER_TARGET']['All-Yes']) || ($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyForm']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyClass']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubject']) || ($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup']))
			{
				$result_to_options['ToParentOption'] = 1;
			}
			if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['All-Yes'] || $_SESSION['SSV_USER_TARGET']['Alumni-All']) && is_array($webmail_identity_allowed) && in_array(USERTYPE_ALUMNI, $webmail_identity_allowed))
			{
				$result_to_options['ToAlumniOption'] = 1;
			}
			
			if(	($_SESSION['SSV_USER_TARGET']['All-Yes']) || 
				($_SESSION['SSV_USER_TARGET']['Staff-MyGroup']) || 
				($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup']) || 
				($_SESSION['SSV_USER_TARGET']['Student-MyGroup']) || 
				($_SESSION['SSV_USER_TARGET']['Parent-MyGroup']) || 
				($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup']) || 
				($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || 
				($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || 
				($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Student-All']) || 
				($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Parent-All'])
			)
			{
				$result_to_options['ToGroupOption'] = 1;
				
				$result_to_group_options['ToTeacher'] = 0;
				$result_to_group_options['ToStaff'] = 0;
				$result_to_group_options['ToStudent'] = 0;
				$result_to_group_options['ToParent'] = 0;
				$result_to_group_options['ToAlumni'] = 0;
				
				if($_SESSION['SSV_USER_TARGET']['All-Yes']){
					$result_to_group_options['ToTeacher'] = 1;
					$result_to_group_options['ToStaff'] = 1;
					$result_to_group_options['ToStudent'] = 1;
					$result_to_group_options['ToParent'] = 1;
					if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
				}else{
					if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
						$result_to_group_options['ToTeacher'] = 1;
					}
					if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
						$result_to_group_options['ToStaff'] = 1;
					}
					if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
						$result_to_group_options['ToStudent'] = 1;
					}
					if( ($sys_custom['iMailShowAllGroupsWithIdentityAll'] && $_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
						$result_to_group_options['ToParent'] = 1;
					}
					if($special_feature['alumni'] && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] ){
						$result_to_group_options['ToAlumni'] = 1;
					}
				}
			}
			
			$AlumniNoTargetCond = true;
			if($special_feature['alumni'])
				$AlumniNoTargetCond = $_SESSION['SSV_USER_TARGET']['Alumni-All'] == '' && $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'] == '';
			### If no mail targeting is set in the front-end, than will assign some default targeting to user ###
			if( ($_SESSION['SSV_USER_TARGET']['All-Yes'] == '') && 
			($_SESSION['SSV_USER_TARGET']['All-No'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-MyForm'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-MyClass'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-MySubject'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'] == '') && 
			($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Student-All'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Student-MyForm'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Student-MyClass'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Student-MySubject'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Student-MyGroup'] == '') &&
			($_SESSION['SSV_USER_TARGET']['Parent-All'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Parent-MyForm'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Parent-MyClass'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Parent-MySubject'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'] == '') && 
			($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'] == '') && 
			$AlumniNoTargetCond 
			)
			{
				if($result_to_options['ToTeachingStaffOption'] == 0 && $result_to_options['ToNonTeachingStaffOption'] == 0 && $result_to_options['ToStudentOption'] == 0 && $result_to_options['ToParentOption'] == 0 && $result_to_options['ToAlumniOption'] == 0)
				{
					if(($identity == "Teaching") || ($identity == "NonTeaching"))
					{
						### If user is Teacher, then will have the follow targeting :
						###  - to All Teaching Staff
						###  - to All NonTeaching Staff
						###  - to All Student 
						###  - to All Parent 
						$result_to_options['ToTeachingStaffOption'] = 1;
						$result_to_options['ToNonTeachingStaffOption'] = 1;
						$result_to_options['ToStudentOption'] = 1;
						$result_to_options['ToParentOption'] = 1;
						if($special_feature['alumni']) $result_to_options['ToAlumniOption'] = 1;
						$_SESSION['SSV_USER_TARGET']['All-Yes'] = true;
					}
					if($identity == "Student")
					{
						### If user is Student, then will have the follow targeting :
						###  - to Student Own Class Student
						###  - to Student Own Subject Group Student
						$result_to_options['ToStudentOption'] = 1;
						$_SESSION['SSV_USER_TARGET']['Student-MyClass'] = true;	
						$_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'] = true;
					}
					if($identity == "Parent")
					{
						### If user is Parent, then will have the follow targeting :
						###  - to Their Child's Own Class Teacher
						###  - to Their Child's Own Subject Group Teacher
						$result_to_options['ToTeachingStaffOption'] = 1;
						$_SESSION['SSV_USER_TARGET']['Staff-MyClass'] = true;	
						$_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'] = true;
					}
				}
				$result_to_options['ToGroupOption'] = 1;
				
				$result_to_group_options['ToTeacher'] = 0;
				$result_to_group_options['ToStaff'] = 0;
				$result_to_group_options['ToStudent'] = 0;
				$result_to_group_options['ToParent'] = 0;
				$result_to_group_options['ToAlumni'] = 0;
				
				if($_SESSION['SSV_USER_TARGET']['All-Yes']){
					$result_to_group_options['ToTeacher'] = 1;
					$result_to_group_options['ToStaff'] = 1;
					$result_to_group_options['ToStudent'] = 1;
					$result_to_group_options['ToParent'] = 1;
					if($special_feature['alumni']) $result_to_group_options['ToAlumni'] = 1;
				}else{
					if(($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching']) || ($_SESSION['SSV_USER_TARGET']['Staff-MyGroup'])){
						$result_to_group_options['ToTeacher'] = 1;
					}
					if(($_SESSION['SSV_USER_TARGET']['Staff-AllNonTeaching']) || ($_SESSION['SSV_USER_TARGET']['NonTeaching-MyGroup'])){
						$result_to_group_options['ToStaff'] = 1;
					}
					if(($_SESSION['SSV_USER_TARGET']['Student-All']) || ($_SESSION['SSV_USER_TARGET']['Student-MyGroup'])){
						$result_to_group_options['ToStudent'] = 1;
					}
					if(($_SESSION['SSV_USER_TARGET']['Parent-All']) || ($_SESSION['SSV_USER_TARGET']['Parent-MyGroup'])){
						$result_to_group_options['ToParent'] = 1;
					}
					if($special_feature['alumni'] && ($_SESSION['SSV_USER_TARGET']['Alumni-All'] || $_SESSION['SSV_USER_TARGET']['Alumni-MyGroup'])){
						$result_to_group_options['ToAlumni'] = 1;
					}
				}
			}
			
			if($identity != "Student")
			{
				$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
				$result = $li->returnVector($sql);
				if($result[0]>0)
					$result_to_options['ToMyChildrenOption'] = 1;
			}
			if($identity == "Student")
			{
				$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION WHERE StudentID = '$UserID'";
				$result = $li->returnVector($sql);
				if($result[0]>0)
					$result_to_options['ToMyParentOption'] = 1;
			}
			
			$returnUserIDArray = array();
			
			if($_SESSION['SSV_USER_TARGET']['All-Yes']){
				$sql = "SELECT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',UserID)":"UserID")."  FROM INTRANET_USER WHERE RecordStatus=1";
		    	$returnUserIDArray = $li->returnVector($sql);
				return $returnUserIDArray;
			}
			
			if ($result_to_options['ToTeacherAndStaff'] || $result_to_options['ToTeachingStaffOption'])
			{
				## All Teaching Staff ##
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])){
					$sql = "SELECT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',UserID)":"UserID")."  FROM INTRANET_USER WHERE RecordStatus=1 and RecordType=1 AND Teaching='1'";
			    	$UserIdAry = $li->returnVector($sql);
					$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
				}else{
					## Form Teacher ##
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyForm'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to form teacher
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					
					## Class Teacher ##
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MyClass'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to class teacher
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					
					## Subject Teacher ##
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubject'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to subject teacher
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					
					## Subject Group Teacher ##
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Staff-AllTeaching'])||($_SESSION['SSV_USER_TARGET']['Staff-MySubjectGroup'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Non teaching suppose cannnot send to subject group teacher
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
				}
			}
			
			if ($result_to_options['ToNonTeachingStaffOption'])
			{
			    $sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',UserID)":"UserID")."  FROM INTRANET_USER WHERE RecordStatus=1 AND RecordType=1 AND (Teaching=0 OR Teaching IS NULL)";
			    $UserIdAry = $li->returnVector($sql);
				$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
			}
			if ($result_to_options['ToStudentOption'])
			{
			    ## All Student
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])){
					$sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',UserID)":"UserID")."  FROM INTRANET_USER WHERE RecordStatus=1 AND RecordType=2";
			    	$UserIdAry = $li->returnVector($sql);
					$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
				}else{
					## Form Student
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyForm'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)"; 
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to form student
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					
					## Class Student
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MyClass'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to class student
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					## Subject Student
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubject'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to Subject student
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					## Subject Group Student
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Student-All'])||($_SESSION['SSV_USER_TARGET']['Student-MySubjectGroup'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## support staff suppose cannot send to subject group student
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID); 
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")."  FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
				}
			}
			if ($result_to_options['ToParentOption'])
			{
			    ## All Parent
				if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])){
					$sql = "SELECT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',UserID)":"UserID")."  FROM INTRANET_USER where RecordStatus = 1 and RecordType = 3";
			   	 	$UserIdAry = $li->returnVector($sql);
					$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
				}else{
					## Form Parent
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyForm'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearID = $li->returnVector($sql);
							$targetYearID = implode(",",$arrTargetYearID);
							
							$sql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")."  FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE a.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to form parent
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearID = $li->returnVector($sql);
							$targetYearID = implode(",",$arrTargetYearID);
							
							$sql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							 
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")."  FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearID = $li->returnVector($sql);
							$targetYearID = implode(",",$arrTargetYearID);
							
							$sql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$form_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")."  FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($form_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					## Class Parent
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MyClass'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")."  FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to class parent
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")."  FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							$arrTargetYearClassID = $li->returnVector($sql);
							$targetYearClassID = implode(",",$arrTargetYearClassID);
							
							$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$class_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($class_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					## Subject Parent
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubject'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to subject parent
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$subject_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($subject_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
					## Subject Group Parent
					if(($_SESSION['SSV_USER_TARGET']['All-Yes'])||($_SESSION['SSV_USER_TARGET']['Parent-All'])||($_SESSION['SSV_USER_TARGET']['Parent-MySubjectGroup'])){
						if($identity == "Teaching")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "NonTeaching")
						{
							## Support Staff suppose cannot send to subject group parent
						}
						if($identity == "Student")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
						if($identity == "Parent")
						{
							$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
							$arrTargetSubjectGroupID = $li->returnVector($sql);
							$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
							
							$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
							$arrTargetUserID = $li->returnVector($sql);
							$targetUserID = implode(",",$arrTargetUserID);
							
							$subject_group_sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',b.ParentID)":"b.ParentID")." FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID)";
							$UserIdAry = $li->returnVector($subject_group_sql);
							$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
						}
					}
				}
			}
			if ($special_feature['alumni'] && $result_to_options['ToAlumniOption'])
			{
			    $sql = "SELECT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',UserID)":"UserID")."  FROM INTRANET_USER where RecordStatus = 1 and RecordType = 4";
			    $UserIdAry = $li->returnVector($sql);
				$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
			}
			
			if ($result_to_options['ToMyChildrenOption'])
			{
				$sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',u.UserID)":"u.UserID")."  FROM INTRANET_USER as u INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=u.UserID WHERE r.ParentID='$UserID'";
				$UserIdAry = $li->returnVector($sql);
				$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
			}
			if ($result_to_options['ToMyParentOption'])
			{
				$sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',u.UserID)":"u.UserID")."  FROM INTRANET_USER as u INNER JOIN INTRANET_PARENTRELATION as r ON r.ParentID=u.UserID WHERE r.StudentID='$UserID'";
				$UserIdAry = $li->returnVector($sql);
				$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
			}
			
			if($result_to_options['ToGroupOption'])
			{
				$sql = "SELECT DISTINCT ".($UserIdPrefix!=""?"CONCAT('$UserIdPrefix',a.UserID)":"a.UserID")." 
						FROM INTRANET_USER AS a 
						INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) 
						WHERE a.RecordStatus = 1 ";
				$condAry = array();
				if($result_to_group_options['ToTeacher']){
					$condAry[] = " (a.RecordType=1 AND a.Teaching=1) ";
				}
				if($result_to_group_options['ToStaff']){
					$condAry[] = " (a.RecordType=1 AND (a.Teaching=0 OR a.Teaching IS NULL)) ";
				}
				if($result_to_group_options['ToStudent']){
					$condAry[] = " (a.RecordType=2) ";
				}
				if($result_to_group_options['ToParent']){
					$condAry[] = " (a.RecordType=3) ";
				}
				if($result_to_group_options['ToAlumni']){
					$condAry[] = " (a.RecordType=4) ";
				}
				if(count($condAry)>0){
					$sql .= " AND (".implode(" OR ",$condAry).")";
				}
				
				$UserIdAry = $li->returnVector($sql);
				$returnUserIDArray = array_merge($returnUserIDArray,$UserIdAry);
			}
        	
			$returnUserIDArray = array_unique($returnUserIDArray);
			
			return $returnUserIDArray;
		}
		
		function GetCampusMailNewMailCount($uid, $folderId)
		{
			if(!isset($this->NewMailCountCacheAry[$uid])){
				$this->NewMailCountCacheAry[$uid] = array();
			}
			
			if(isset($this->NewMailCountCacheAry[$uid][$folderId]))
			{
				return $this->NewMailCountCacheAry[$uid][$folderId];
			}
			
			$sql = "SELECT * FROM INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT WHERE UserID='$uid' AND FolderID='$folderId'";
			$record = $this->returnResultSet($sql);
			
			if(count($record)>0){
				$this->NewMailCountCacheAry[$uid][$folderId] = $record[0]['NewMail'];
				return $record[0]['NewMail'];
			}
			
			// count number of new/unread emails
			$sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID='$uid' ";
			if($folderId != -1) // trash folder is virtual, use Deleted to classify deleted emails
			{
				$sql.= " AND UserFolderID='$folderId' ";
			}
			$sql.= " AND (RecordStatus IS NULL OR RecordStatus='') ";
			if($folderId != -1){
				$sql.= " AND Deleted<>1 ";
			}else{
				$sql.= " AND Deleted=1 ";
			}
			
			$count_record = $this->returnVector($sql);
			$new_mail_count = $count_record[0];
			$this->NewMailCountCacheAry[$uid][$folderId] = $new_mail_count;
			
			$sql = "INSERT INTO INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT (UserID,FolderID,NewMail) VALUES ('$uid','$folderId','$new_mail_count')";
			$insert_success = $this->db_db_query($sql);
			
			return $new_mail_count;
		}
		
		function UpdateCampusMailNewMailCount($uid, $folderId, $count, $relative=1)
		{
			$sql = "UPDATE INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT SET ";
			if($relative){ // increment/decrement relatively
				$sql .= " NewMail=GREATEST(NewMail+($count),0) ";
			}else{ // update absolutely
				$sql .= " NewMail=GREATEST($count,0) ";
			}
			$sql.= " WHERE UserID='$uid' AND FolderID='$folderId'";
			
			$update_success = $this->db_db_query($sql);
			$affected_rows = $this->db_affected_rows();
			if($affected_rows <= 0){
				$this->GetCampusMailNewMailCount($uid, $folderId);
				
				$sql = "INSERT INTO INTRANET_CAMPUSMAIL_NEW_MAIL_COUNT (UserID,FolderID,NewMail) VALUES ('$uid','$folderId',GREATEST(0,$count)) ON DUPLICATE KEY UPDATE NewMail=GREATEST(NewMail+($count),0)";
				$insert_success = $this->db_db_query($sql);
				return $insert_success;
			}else{
				return $update_success;
			}
		}
		
## End ##		
}

function stri_replace($search, $subject, $replace=false){
    $textn=$subject;
    $texti=strtolower($subject);
    $wordi=strtolower($search);
    $recd=array();
    $len=strlen($wordi);
    while(is_integer($pos=strpos($texti,$wordi))){
        $rec["str"]=substr($textn,$pos,$len);
        $rec["pre"]=substr($textn,0,$pos);
        $recd[]=$rec;
        $cut=$pos+$len;
        $texti=substr($texti,$cut);
        $textn=substr($textn,$cut);
    }

    for($i=0;$i<count($recd);$i++){
        if(!$replace) $replace="<b>".$recd[$i]["str"]."</b>";
        $ntext=$ntext.$recd[$i]["pre"].$replace;
    }
    $ntext.=$textn;
    return $ntext;
}
?>