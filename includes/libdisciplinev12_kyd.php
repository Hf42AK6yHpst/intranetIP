<?php
// Modifying by : 
 


if (!defined("LIBDISCIPLINEv12_kyd_DEFINED"))         // Preprocessor directives
{
	class libdisciplinev12_kyd extends libdisciplinev12
	{
		function libdisciplinev12_kyd()
		{
			$this->libdb();
		}
	
		/*
		function countLateInPeriod($studentID,$SFrom,$STo)
		{
			  $sql = "SELECT COUNT(*) 
            				FROM PROFILE_STUDENT_ATTENDANCE 
            				WHERE UserID=$studentID
					AND RecordType=2  
            				AND AttendanceDate>='$SFrom' 
		        		AND AttendanceDate<='$STo'";
		  	return $this->returnVector($sql);			
		}
		*/
		
		function display_monthly_report_form_ui($targetClass='', $startdate='', $enddate='')
		{
			global $lclass, $i_Discipline_Class, $i_To, $i_Discipline_System_Reports_Report_Period, $linterface, $button_view, $i_Discipline_System_Award_Punishment_All_Classes;
			global $i_alert_pleaseselect, $Lang;
			
			$startdate = $startdate ? $startdate : date("Y-m-01");
			$enddate = $enddate ? $enddate : date("Y-m-t");
			
			$select_class = $lclass->getSelectClassID("name=\"targetClass\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, false);

			$x = "
				<script language=\"javascript\">
	
				function reset_innerHtml()
				{
				 	document.getElementById('div_select_class_warning').innerHTML = \"\";
				 	document.getElementById('div_DateEnd_err_msg').innerHTML = \"\";
				}
				
				function goCheck(obj) 
				{
					//// Reset div innerHtml
					reset_innerHtml();
					
					var error_no=0;
					var focus_field='';
					
					if(obj.targetClass.value==\"\") 
					{
						document.getElementById('div_select_class_warning').innerHTML = '<font color=\"red\">". $i_alert_pleaseselect . $i_Discipline_Class ."</font>';
						error_no++;
						focus_field = \"targetClass\";
					}
					
					if (compareDate(obj.Section2Fr.value, obj.Section2To.value) > 0) 
					{
						document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color=\"red\">". $Lang['SysMgr']['SchoolCalendar']['JSWarning']['EndDateLaterThanStartDate'] ."</font>';
						error_no++;
						if(focus_field==\"\")	focus_field = \"Section2Fr\";
					}
					
					if(error_no>0)
					{
						eval(\"obj.\" + focus_field +\".focus();\");
						return false;
					}
					else
					{
						return true;
					}
				}
				</script>
			
				<form name=\"form1\" method=\"post\" action=\"monthly_report_view.php\" onSubmit=\"return goCheck(this)\">
				<table class=\"form_table_v30\">
				<tr>
					<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Discipline_Class ."</td>
					<td>". $select_class ."<br><span id=\"div_select_class_warning\"></span></td>
				</tr>
				<tr>
					<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Discipline_System_Reports_Report_Period ."</td>
					<td>". $linterface->GET_DATE_PICKER("Section2Fr", $startdate) . $i_To . " " . $linterface->GET_DATE_PICKER("Section2To", $enddate) ."
					<br><span id=\"div_DateEnd_err_msg\"></span>
					</td>
				</tr>
				</table>
				
				". $linterface->MandatoryField() ."
				<div class=\"edit_bottom_v30\">
					". $linterface->GET_ACTION_BTN($button_view, "submit") ."
				</div>	
				</form>
			";
			return $x;
		}		
	}
} // End of directives

		
?>