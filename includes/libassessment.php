<?php
# using: yat
if (!defined("LIBASSESSMENT_DEFINED"))         // Preprocessor directives
{
 define("LIBASSESSMENT_DEFINED",true);

 class libassessment extends libclass{
       var $AssessmentID;
       var $UserID;
       var $year;
       var $semester;
       var $assessmentDate;
       var $assessByUserID;
       var $assessByName;
       var $formID;
       var $ansStr;
       var $RecordType;
       var $RecordStatus;

       function libassessment($AssessmentID="")
       {
                $this->libclass();
                if ($AssessmentID != "")
                {
                    $this->AssessmentID = $AssessmentID;
                    $this->retrieveRecord($this->AssessmentID);
                }
       }
       function retrieveRecord($id)
       {
                $fields = "UserID, Year, Semester, AssessmentDate, RecordType, RecordStatus,
                           AssessBy, AssessByName, FormID, AnsString";
                $conds = "AssessmentID = '$id'";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_ASSESSMENT WHERE $conds";
                $result = $this->returnArray($sql,10);
                list(
                $this->UserID,$this->year,$this->semester, $this->AssessmentDate,$this->RecordType,
                $this->RecordStatus, $this->assessByUserID,$this->assessByName, $this->formID,$this->ansStr) = $result[0];
                return $result;
       }
       function returnYears($studentid)
       {
           $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_ASSESSMENT WHERE UserID = '".IntegerSafe($studentid)."' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }
       function returnArchiveYears($studentid)
       {
           $sql = "SELECT DISTINCT Year FROM PROFILE_ARCHIVE_ASSESSMENT WHERE UserID = '".IntegerSafe($studentid)."' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }       
       function getCountByClassName ($class)
       {
                $students = $this->getClassStudentList($class);
                if (sizeof($students)!=0)
                {
                    $student_list = implode(",",$students);
                    $sql = "SELECT COUNT(AssessmentID) FROM PROFILE_STUDENT_ASSESSMENT
                            WHERE UserID IN ($student_list) AND RecordStatus = 1";
                    $entry = $this->returnVector($sql);
                    return $entry[0]+0;
                }
                else
                {
                    return 0;
                }
       }

       function getClassAssessmentList ()
       {
                $classList = $this->getClassList();
                if (sizeof($classList)==0) return array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($id, $name,$lvlID) = $classList[$i];
                     $count = $this->getCountByClassName($name);
                     $result[] = array($id,$name,$count,$lvlID);
                }
                return $result;
       }
       function returnFormConds($FormID)
       {
                if ($FormID == "")
                {
                    return "";
                }
                else
                {
                    return " AND FormID = $FormID";
                }
       }
       function getClassAssessmentListByForm($FormID="")
       {
                $formConds = $this->returnFormConds($FormID);
                $sql = "SELECT c.YearClassID, COUNT(a.AssessmentID)
                        FROM PROFILE_STUDENT_ASSESSMENT as a 
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN
                        WHERE 1 = 1 $formConds
                        GROUP BY c.YearClassID";
                $result = $this->returnArray($sql,2);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($ClassID,$formCount) = $result[$i];
                     $counts[$ClassID] = $formCount;
                }

                $classList = $this->getClassList();
                $returnResult = array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $formCount = $counts[$ClassID]+0;
                     $returnResult[] = array($ClassID, $ClassName, $formCount, $LvlID);
                }
                return $returnResult;

       }

       function getCountByStudent($studentid)
       {
                $sql = "SELECT COUNT(AssessmentID) FROM PROFILE_STUDENT_ASSESSMENT
                        WHERE UserID=$studentid AND RecordStatus = 1";
                $count = $this->returnVector($sql);
                return $count[0]+0;
       }
       function getAssessmentListByClass($classid)
       {
                $studentList = $this->getClassStudentNameList($classid);
                if (sizeof($studentList)==0) return array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($id, $name, $classnumber) = $studentList[$i];
                     $count = $this->getCountByStudent($id);
                     $result[] = array($id,$name,$classnumber,$count);
                }
                return $result;
       }
       function getAssessmentListByClassForm($classid,$FormID)
       {
                $formConds = $this->returnFormConds($FormID);
                $sql = "SELECT b.UserID, COUNT(a.AssessmentID)
                        FROM PROFILE_STUDENT_ASSESSMENT as a 
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN YEAR_CLASS as c ON c.ClassTitleEN = b.ClassName
                        WHERE c.YearClassID = $classid $formConds
                        GROUP BY b.UserID";
                $result = $this->returnArray($sql,2);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$count) = $result[$i];
                     $counts[$StudentID] = $count;
                }

                $studentList = $this->getClassStudentNameList($classid);
                $returnResult = array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $ClassNumber) = $studentList[$i];
                     $count = $counts[$StudentID]+0;
                     $returnResult[] = array($StudentID, $StudentName,$ClassNumber, $count);
                }
                return $returnResult;

       }
       function getAssessmentByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND a.Year = '$year'":"");
                $conds .= ($sem != ""? " AND a.Semester = '$sem'":"");
                $name_field = getNameFieldWithLoginByLang("c.");
                $sql = "SELECT a.AssessmentID, a.Year, a.Semester, IF (a.AssessmentDate IS NULL,'-',a.AssessmentDate),b.FormName,
                               IF(c.UserID IS NULL,a.AssessByName,$name_field)
                               FROM PROFILE_STUDENT_ASSESSMENT as a
                               LEFT OUTER JOIN INTRANET_FORM as b ON a.FormID = b.FormID
                               LEFT OUTER JOIN INTRANET_USER as c ON a.AssessBy = c.UserID
                               WHERE a.UserID = $studentid $conds
                               ORDER BY a.Year DESC, a.Semester, b.FormName";
                return $this->returnArray($sql,6);
       }
       function displayAssessmentAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_Profile_Year,$i_Profile_Semester,$i_Assessment_Date,$i_Assessment_By,$i_Form_Name;
                global $i_no_record_exists_msg,$i_general_sysadmin;
                $result = $this->getAssessmentByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_Profile_Year</td>
                      <td width=110 align=center class=tableTitle_new>$i_Profile_Semester</td>
                      <td width=90 align=center class=tableTitle_new>$i_Assessment_Date</td>
                      <td width=170 align=center class=tableTitle_new>$i_Form_Name</td>
                      <td width=120 align=center class=tableTitle_new>$i_Assessment_By</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($assessmentID, $year,$sem,$aDate,$form,$assessor) = $result[$i];
                     $form = "<a href=javascript:viewFormAns($assessmentID)>$form</a>";
                     $assessor = ($assessor==""?"$i_general_sysadmin":$assessor);
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$aDate</td>
                              <td align=center>$form</td>
                              <td align=center>$assessor</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       
        // ++++++ for archive student ++++++ \\            
       
       function displayArchiveAssessmentAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_Profile_Year,$i_Profile_Semester,$i_Assessment_Date,$i_Assessment_By,$i_Form_Name;
                global $i_no_record_exists_msg,$i_general_sysadmin;
                $result = $this->getArchiveAssessmentByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_Profile_Year</td>
                      <td width=110 align=center class=tableTitle_new>$i_Profile_Semester</td>
                      <td width=90 align=center class=tableTitle_new>$i_Assessment_Date</td>
                      <td width=170 align=center class=tableTitle_new>$i_Form_Name</td>
                      <td width=120 align=center class=tableTitle_new>$i_Assessment_By</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($assessmentID, $year,$sem,$aDate,$form,$assessor) = $result[$i];
                     $form = "<a href=javascript:viewFormAns($assessmentID)>$form</a>";
                     $assessor = ($assessor==""?"$i_general_sysadmin":$assessor);
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$aDate</td>
                              <td align=center>$form</td>
                              <td align=center>$assessor</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }       
       
       function getArchiveAssessmentByStudent($studentid, $year,$sem="")
       {                
                $conds = ($year != ""? " AND Year = '$year'":"");
                $conds .= ($sem != ""? " AND Semester = '$sem'":"");
                $name_field = getNameFieldWithLoginByLang("c.");
                $sql = "SELECT RecordID, Year, Semester, IF (RecordDate IS NULL,'-',RecordDate),FormName,AssessorName 
                               FROM PROFILE_ARCHIVE_ASSESSMENT 
                               WHERE UserID = $studentid $conds
                               ORDER BY Year DESC, Semester, FormName";                               
                return $this->returnArray($sql,6);                       
       }
       
       function setArchiveFileID($RecordID){
	            $this->libclass();
	            
                if ($RecordID != "")
                {
                    $this->FileID = $RecordID;
                    $this->retrieveArchiveRecord($this->FileID);
                }
              
       }
       
       function retrieveArchiveRecord($id)
       {
                $fields = "UserID, Year, Semester, RecordDate, AssessorName, RecordID, FormContent";
                $conds = "RecordID = $id";
                $sql = "SELECT $fields FROM PROFILE_ARCHIVE_ASSESSMENT WHERE $conds";
                
                $result = $this->returnArray($sql,7);
                list(
                $this->UserID,$this->year,$this->semester, $this->AssessmentDate,$this->assessByName, $this->formID,$this->ansStr) = $result[0];
                return $result;
       }
       
       // +++++ end of archive student +++++ \\          
       
 }


} // End of directives
?>