<?php
#  Editing by
/*
 *  Date : 2019-07-29 (Bill)
 *          modified Get_Student_ECA_Import_Export_Column_Title_Arr()
 *  Date : 2019-03-21 (Bill)    [2018-1002-1146-08277]
 *          modified Delete_Report_Class_ECA_Record()
 *          added Get_Student_ECA_Import_Export_Column_Title_Arr(), Delete_Import_Temp_Data()
 */

class libreportcardrubrics_eca extends libreportcardrubrics {
	
	/**
	 * Constructor
	 */
	function libreportcardrubrics_eca() {
		parent:: libreportcardrubrics();
	}
	
	function Get_ECA_Category_Info($EcaCategoryID='')
	{
		if(trim($EcaCategoryID)!='')
			$cond_CategoryID = " AND rec.EcaCategoryID = '$EcaCategoryID' ";
		
		$RC_ECA_CATEGORY = $this->Get_Table_Name('RC_ECA_CATEGORY');
		$RC_ECA_SUBCATEGORY = $this->Get_Table_Name('RC_ECA_SUBCATEGORY');
		
		$sql = "
			SELECT 
				rec.*,
				COUNT(res.EcaSubCategoryID) AS NumOfSubCategory
			FROM
				$RC_ECA_CATEGORY rec
				LEFT JOIN $RC_ECA_SUBCATEGORY res ON rec.EcaCategoryID = res.EcaCategoryID AND res.RecordStatus = 1
			WHERE
				rec.RecordStatus = 1
				$cond_CategoryID
			GROUP BY
				rec.EcaCategoryID
			ORDER BY
				rec.DisplayOrder
		";
		
		$result = $this->returnArray($sql);
		
		return $result;
	}

	function Get_ECA_SubCategory_Info($EcaCategoryID='',$EcaSubCategoryID='')
	{
		if(trim($EcaSubCategoryID)!='')
			$cond_EcaSubCategoryID = " AND res.EcaSubCategoryID = '$EcaSubCategoryID' ";
		else if(trim($EcaCategoryID)!='')
			$cond_CategoryID = " AND res.EcaCategoryID = '$EcaCategoryID' ";
		
		$RC_ECA_ITEM = $this->Get_Table_Name('RC_ECA_ITEM');
		$RC_ECA_SUBCATEGORY = $this->Get_Table_Name('RC_ECA_SUBCATEGORY');
		
		$sql = "
			SELECT 
				res.*,
				COUNT(rei.EcaItemID) AS NumOfItem
			FROM
				$RC_ECA_SUBCATEGORY res 
				LEFT JOIN $RC_ECA_ITEM rei ON res.EcaSubCategoryID = rei.EcaSubCategoryID AND rei.RecordStatus = 1
			WHERE
				res.RecordStatus = 1
				$cond_CategoryID
				$cond_EcaSubCategoryID
			GROUP BY
				res.EcaSubCategoryID
			ORDER BY
				res.DisplayOrder
		";
		
		$result = $this->returnArray($sql);

		return $result;
	}
	
	function Get_ECA_Category_Item_Info($EcaCategoryID='',$EcaSubCategoryID='',$EcaItemID='', $ShowItemExistOnly=0)
	{
		if(trim($EcaItemID)!='')
			$cond_EcaItemID = " AND rei.EcaItemID = '$EcaItemID' ";		
		else if(trim($EcaSubCategoryID)!='')
			$cond_EcaSubCategoryID = " AND res.EcaSubCategoryID = '$EcaSubCategoryID' ";
		else if(trim($EcaCategoryID)!='')
			$cond_CategoryID = " AND rec.EcaCategoryID = '$EcaCategoryID' ";

		if($ShowItemExistOnly!=1)
			$JOIN = "LEFT JOIN";
		else
			$JOIN = "INNER JOIN";
		

		$RC_ECA_ITEM = $this->Get_Table_Name('RC_ECA_ITEM');
		$RC_ECA_SUBCATEGORY = $this->Get_Table_Name('RC_ECA_SUBCATEGORY');
		$RC_ECA_CATEGORY = $this->Get_Table_Name('RC_ECA_CATEGORY');
		
		$sql = "
			SELECT
				rec.EcaCategoryID, 
				res.EcaSubCategoryID,
				rei.EcaItemID,
				rei.EcaItemID,
				rei.EcaItemCode,
				rei.EcaItemNameEn,
				rei.EcaItemNameCh,
				rei.DisplayOrder
			FROM
				$RC_ECA_CATEGORY rec
				$JOIN $RC_ECA_SUBCATEGORY res ON rec.EcaCategoryID = res.EcaCategoryID AND res.RecordStatus = 1
				$JOIN $RC_ECA_ITEM rei ON res.EcaSubCategoryID = rei.EcaSubCategoryID AND rei.RecordStatus = 1
			WHERE
				rec.RecordStatus = 1
				$cond_EcaItemID
				$cond_CategoryID
				$cond_EcaSubCategoryID
			ORDER BY
				rec.DisplayOrder, res.DisplayOrder, rei.DisplayOrder
		";
		

		$result = $this->returnArray($sql);
		return $result;
	}
		
	function Is_ECA_Category_Code_Valid($EcaCategoryCode, $EcaCategoryID='')
	{
		if($EcaCategoryID!='')
			$cond_EcaCategoryID = " AND EcaCategoryID <> '$EcaCategoryID' ";
		
		$RC_ECA_CATEGORY = $this->Get_Table_Name('RC_ECA_CATEGORY');
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				$RC_ECA_CATEGORY
			WHERE
				RecordStatus = 1
				AND EcaCategoryCode = '$EcaCategoryCode'
				$cond_EcaCategoryID
		";
		
		$result = $this->returnVector($sql);
		
		return $result[0]>0?false:true;
	}
	
	function InsertCategory($EcaCategoryCode,$EcaCategoryNameEn,$EcaCategoryNameCh,$DisplayOrder='')
	{
		$RC_ECA_CATEGORY = $this->Get_Table_Name('RC_ECA_CATEGORY');
		$sql = "
			INSERT INTO
				$RC_ECA_CATEGORY
				(
					EcaCategoryCode,
					EcaCategoryNameEn,
					EcaCategoryNameCh,
					DisplayOrder,
					DateModified,
					LastModifiedBy,
					DateInput,
					InputBy
				)
			VALUES
				(
					'".$this->Get_Safe_Sql_Query($EcaCategoryCode)."',
					'".$this->Get_Safe_Sql_Query($EcaCategoryNameEn)."',
					'".$this->Get_Safe_Sql_Query($EcaCategoryNameCh)."',
					'$DisplayOrder',
					NOW(),
					'".$_SESSION["UserID"]."',
					NOW(),
					'".$_SESSION["UserID"]."'
				)
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
	}
	
	function UpdateCategory($EcaCategoryID,$EcaCategoryInfoArr)
	{
		foreach((array)$EcaCategoryInfoArr as $Field => $Value)
		{
			$FieldSql .= "$Field = '".$this->Get_Safe_Sql_Query($Value)."',";
		}
		
		$RC_ECA_CATEGORY = $this->Get_Table_Name('RC_ECA_CATEGORY');
		$sql = "
			UPDATE
				$RC_ECA_CATEGORY
			SET
				$FieldSql
				DateModified = NOW(),
				LastModifiedBy = '".$_SESSION["UserID"]."'
			WHERE
				EcaCategoryID = '$EcaCategoryID'

		";
		
		$result = $this->db_db_query($sql);

		return $result?true:false;		
	}
	
	function Is_ECA_SubCategory_Code_Valid($EcaSubCategoryCode, $EcaSubCategoryID='')
	{
		if($EcaSubCategoryID!='')
			$cond_EcaSubCategoryID = " AND EcaSubCategoryID <> '$EcaSubCategoryID' ";
		
		$RC_ECA_SUBCATEGORY = $this->Get_Table_Name('RC_ECA_SUBCATEGORY');
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				$RC_ECA_SUBCATEGORY
			WHERE
				RecordStatus = 1
				AND EcaSubCategoryCode = '$EcaSubCategoryCode'
				$cond_EcaSubCategoryID
		";
		
		$result = $this->returnVector($sql);
		
		return $result[0]>0?false:true;
	}
	
	function InsertSubCategory($EcaCategoryID, $EcaSubCategoryCode,$EcaSubCategoryNameEn,$EcaSubCategoryNameCh,$DisplayOrder='')
	{
		$RC_ECA_SUBCATEGORY = $this->Get_Table_Name('RC_ECA_SUBCATEGORY');
		$sql = "
			INSERT INTO
				$RC_ECA_SUBCATEGORY
				(
					EcaCategoryID,
					EcaSubCategoryCode,
					EcaSubCategoryNameEn,
					EcaSubCategoryNameCh,
					DisplayOrder,
					DateModified,
					LastModifiedBy,
					DateInput,
					InputBy
				)
			VALUES
				(
					'".$this->Get_Safe_Sql_Query($EcaCategoryID)."',
					'".$this->Get_Safe_Sql_Query($EcaSubCategoryCode)."',
					'".$this->Get_Safe_Sql_Query($EcaSubCategoryNameEn)."',
					'".$this->Get_Safe_Sql_Query($EcaSubCategoryNameCh)."',
					'$DisplayOrder',
					NOW(),
					'".$_SESSION["UserID"]."',
					NOW(),
					'".$_SESSION["UserID"]."'
				)
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
	}
	
	function UpdateSubCategory($ID,$EcaSubCategoryInfoArr, $ByCateogryID=0)
	{
		foreach((array)$EcaSubCategoryInfoArr as $Field => $Value)
		{
			$FieldSql .= "$Field = '".$this->Get_Safe_Sql_Query($Value)."',";
		}
		
		$IDField = $ByCateogryID==1?'EcaCategoryID':'EcaSubCategoryID';
		
		if(is_array($ID))
		{
			$ID = implode(',',$ID);
		}
		
		$RC_ECA_SUBCATEGORY = $this->Get_Table_Name('RC_ECA_SUBCATEGORY');
		$sql = "
			UPDATE
				$RC_ECA_SUBCATEGORY
			SET
				$FieldSql
				DateModified = NOW(),
				LastModifiedBy = '".$_SESSION["UserID"]."'
			WHERE
				$IDField IN ($ID)

		";
		
		
		$result = $this->db_db_query($sql);

		return $result?true:false;		
	}
	
	function Is_ECA_Item_Code_Valid($EcaItemCode, $EcaItemID='')
	{
		if($EcaItemID!='')
			$cond_EcaItemID = " AND EcaItemID <> '$EcaItemID' ";
		
		$RC_ECA_ITEM = $this->Get_Table_Name('RC_ECA_ITEM');
		$sql = "
			SELECT 
				COUNT(*)
			FROM
				$RC_ECA_ITEM
			WHERE
				RecordStatus = 1
				AND EcaItemCode = '$EcaItemCode'
				$cond_EcaItemID
		";
		
		$result = $this->returnVector($sql);
		
		return $result[0]>0?false:true;
	}
	
	function InsertItem($EcaSubCategoryID, $EcaItemCode,$EcaItemNameEn,$EcaItemNameCh,$DisplayOrder='')
	{
		$RC_ECA_ITEM = $this->Get_Table_Name('RC_ECA_ITEM');
		$sql = "
			INSERT INTO
				$RC_ECA_ITEM
				(
					EcaSubCategoryID,
					EcaItemCode,
					EcaItemNameEn,
					EcaItemNameCh,
					DisplayOrder,
					DateModified,
					LastModifiedBy,
					DateInput,
					InputBy
				)
			VALUES
				(
					'".$this->Get_Safe_Sql_Query($EcaSubCategoryID)."',
					'".$this->Get_Safe_Sql_Query($EcaItemCode)."',
					'".$this->Get_Safe_Sql_Query($EcaItemNameEn)."',
					'".$this->Get_Safe_Sql_Query($EcaItemNameCh)."',
					'$DisplayOrder',
					NOW(),
					'".$_SESSION["UserID"]."',
					NOW(),
					'".$_SESSION["UserID"]."'
				)
		";
		
		$result = $this->db_db_query($sql);
		
		return $result?true:false;
	}
	
	function UpdateItem($ID,$EcaItemInfoArr,$BySubCateogryID=0)
	{
		foreach((array)$EcaItemInfoArr as $Field => $Value)
		{
			$FieldSql .= "$Field = '".$this->Get_Safe_Sql_Query($Value)."',";
		}
		
		$IDField = $BySubCateogryID==1?'EcaSubCategoryID':'EcaItemID';
	
		if(is_array($ID))
		{
			$ID = implode(',',$ID);
		}
			
		$RC_ECA_ITEM = $this->Get_Table_Name('RC_ECA_ITEM');
		$sql = "
			UPDATE
				$RC_ECA_ITEM
			SET
				$FieldSql
				DateModified = NOW(),
				LastModifiedBy = '".$_SESSION["UserID"]."'
			WHERE
				$IDField IN ($ID)

		";

		$result = $this->db_db_query($sql);

		return $result?true:false;		
	}
	
	function Get_ECA_Latest_Modification($ReportID, $YearClassID='')
	{
		if(trim($YearClassID) != '')
			$cond_YearClassID= " AND ycu.YearClassID = '$YearClassID' ";
		
		$NameField = getNameFieldByLang("iu.");
		
		$RC_STUDENT_ECA = $this->Get_Table_Name("RC_STUDENT_ECA");
		
		$sql = "
			SELECT
				$NameField AS LastModifiedBy,
				rse.DateModified
			FROM 
				$RC_STUDENT_ECA rse
				INNER JOIN YEAR_CLASS_USER ycu ON rse.StudentID = ycu.UserID
				INNER JOIN INTRANET_USER iu ON iu.UserID = rse.LastModifiedBy
			WHERE
				rse.ReportID = '$ReportID'
				$cond_YearClassID
			ORDER BY 
				rse.DateModified DESC
			LIMIT 1
		";
		
		
		$result = $this->returnArray($sql);
		return $result[0];
	}
	
	function Delete_Report_Class_ECA_Record($ReportID, $ClassID, $StudentIDArr='', $ItemIDArr='')
	{
	    if(is_array($StudentIDArr) || (!is_array($StudentIDArr) && $StudentIDArr != '')) {
	        // do nothing
	    }
	    else {
    		$StudentArr = $this->GET_STUDENT_BY_CLASS($ClassID);
    		$StudentIDArr = Get_Array_By_Key($StudentArr, "UserID");
	    }
		if(empty($StudentIDArr)) {
			return false;
		}
		$StudentIDSql = implode("','", (array)$StudentIDArr);
		
		if(is_array($ItemIDArr) || (!is_array($ItemIDArr) && $ItemIDArr != '')) {
		    $ItemIDSql = " AND EcaItemID IN ('".implode("','", (array)$ItemIDArr)."')";
		}
		
		$RC_STUDENT_ECA = $this->Get_Table_Name("RC_STUDENT_ECA");
		$sql = "
			DELETE FROM
				$RC_STUDENT_ECA
			WHERE
				ReportID = '$ReportID' AND 
                StudentID IN ('$StudentIDSql')
                $ItemIDSql ";
		$result = $this->db_db_query($sql);
		
		return $result;
	}
	
	function Insert_Student_ECA_Record($ReportID,$StudentItemArr)
	{
		foreach((array)$StudentItemArr as $StudentID => $ItemArr)
		{
			foreach((array)$ItemArr as $ItemID)
			{
				$ValueSqlArr[] = "('$StudentID','$ReportID','$ItemID',NOW(),'".$_SESSION['UserID']."')\n";  
			}
		}
		$ValueSql = implode(",",(array)$ValueSqlArr);
		
		$RC_STUDENT_ECA = $this->Get_Table_Name("RC_STUDENT_ECA");
		$sql = "
			INSERT INTO
				$RC_STUDENT_ECA
			(StudentID, ReportID, EcaItemID, DateModified, LastModifiedBy)
			VALUES
			$ValueSql
		";
		
		$result = $this->db_db_query($sql);
		
		return $result;
	}
	
	function Get_Report_Student_ECA_Record($ReportID, $ClassID='', $StudentIDArr='')
	{
		if(trim($ReportID) != '')
			$cond_ReportID= " AND rse.ReportID = '$ReportID' ";

		if(trim($ClassID) != '')
		{
			$StudentArr = $this->GET_STUDENT_BY_CLASS($ClassID);
			$StudentIDArr = Get_Array_By_Key($StudentArr,"UserID");
			
			if(!empty($StudentIDArr))
			{
				$StudentIDSql = implode(",",(array)$StudentIDArr);
				$cond_ClassID = " AND rse.StudentID IN ($StudentIDSql) ";
			}
		}
		
		if ($StudentIDArr != '')
		{
			if (!is_array($StudentIDArr))
				$StudentIDArr = array($StudentIDArr);
			$cond_StudentID = " And rse.StudentID In (".implode(',', $StudentIDArr).") ";
		}
		
		$RC_STUDENT_ECA = $this->Get_Table_Name("RC_STUDENT_ECA");
		
		$sql = "
			SELECT
				rse.ReportID, 
				rse.StudentID, 
				rse.EcaItemID
			FROM
				$RC_STUDENT_ECA rse
			WHERE
				1
				$cond_ReportID
				$cond_ClassID
				$cond_StudentID
		";
		
		$result = $this->returnArray($sql);

		return $result;
	}
	
	function Delete_Import_Temp_Data()
	{
	    $TEMP_IMPORT_TABLE = $this->Get_Table_Name('TEMP_IMPORT_STUDENT_ECA');
	    
	    $sql = "DELETE FROM $TEMP_IMPORT_TABLE WHERE ImportUserID = '".$_SESSION["UserID"]."'";
	    $Success = $this->db_db_query($sql);
	    
	    return $Success;
	}
	
	function Get_Student_ECA_Import_Export_Column_Title_Arr()
	{
	    global $Lang;
	    
	    $titleArr = array();
	    
	    // row 1
	    $titleRowArr = array();
        foreach($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderEN'] as $thisHeaderName) {
            $titleRowArr[] = $thisHeaderName;
        }
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatCode'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatNameEn'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['SubCatCode'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['SubCatNameEn'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatItemCode'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['CatItemNameEn'];
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['Class'];
	    $titleArr[] = $titleRowArr;
	    
	    // row 2
	    $titleRowArr = array();
        foreach($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeaderB5'] as $thisHeaderName) {
            $titleRowArr[] = $thisHeaderName;
        }
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['StudentNameEn'];
	    $titleArr[] = $titleRowArr;
	    
	    // row 3
	    //$titleRowArr = array();
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = '';
	    //$titleRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ImportHeader']['StudentUserLogin'];
	    //$titleArr[] = $titleRowArr;
	    
	    return $titleArr;
	}
}

?>