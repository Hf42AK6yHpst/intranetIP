<?php
include_once($intranet_root."/includes/libfilesystem.php");
if (!class_exists('libuser')){
	include_once($intranet_root."/includes/libuser.php");
}
class ls extends libdb
{		
	function ls()
	{
		$this->libdb();
	}
	
	function getAdminUser()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/ls/admin_user.txt"));

		return $AdminUser;
	}
	
	function getTotalUsedLicense()
	{
		$sql = "select count(*) from LS_USER";
		$return_obj = $this->returnVector($sql);
		$used_license = $return_obj[0];	
		
		return $used_license;
	}
	
	function hasFullLicense()
	{
			return true;
	}
	
	function hasUsedUpAllLicense()
	{
		return false;
	}
	
	function getTotalLicenseLeft()
	{
		global $lslp_license;
		
		if($lslp_license==9999)
		{
			return 9999;	
		}
		else
		{
			$used = $this->getTotalUsedLicense();
			return $lslp_license - $used;
		}
	}
	
	
	function hasLSAccess()
	{
		global $UserID,$intranet_root,$intranet_db, $lu;
		
		if (isset($lu) && $lu->UserID==$UserID)
		{
			$lib_user = $lu;
		} else
		{
			include_once($intranet_root."/includes/libuser.php");
			$lib_user = new libuser($UserID);
			$lib_user->db = $intranet_db;
			$lib_user->UserID = $UserID;
		}
		
		if($lib_user->isTeacherStaff())
		{
			return true;
		}
		else
		{
			if(!$this->hasFullLicense())
			{
				global $UserID;
				
				$sql = " select count(*) from LS_USER where UserID = '".$UserID."'";
				$access_obj = $this->returnVector($sql);
				
				return $access_obj[0];
			}
			else
			return true;
		}
	}
	function isInLicencePeriod()
	{
		return true;
	}
	
	function isAdminUser()
	{
		global $UserID;
		$admin_user = $this->getAdminUser();
		$admin_user_arr = explode(",",$admin_user);
		
		if(in_array($UserID,$admin_user_arr))
			return true;
	}
	
	function getLicenseType()
	{
		global $lslp_license,$i_LSLP;
		return $i_LSLP['school_license'];
	}
	function getLicensePeriod()
	{

	}
}

?>