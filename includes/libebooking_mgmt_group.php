<?php
// Using : 

class eBooking_MgmtGroup extends libebooking {
	var $TableName;
	var $ID_FieldName;
	var $ObjectID;
	
	var $GroupID;
	var $GroupName;
	var $Description;
	var $InputBy;
	var $ModifiedBy;
	var $RecordType;
	var $RecordStatus;
	var $DateInput;
	var $DateModified;
	
	function eBooking_MgmtGroup($ObjectID=''){
		parent::libebooking();
		
		$this->TableName = 'INTRANET_EBOOKING_MANAGEMENT_GROUP';
		$this->ID_FieldName = 'GroupID';
		
		$this->MaxLength['GroupName'] = 255;
		
		if ($ObjectID != '')
		{
			$this->ObjectID = $ObjectID;
			$this->Get_Self_Info();
		}
	}
	
	function Get_Self_Info()
	{
		$sql = "";
		$sql .= " SELECT * FROM ".$this->TableName;
		$sql .= " WHERE ".$this->ID_FieldName." = '".$this->ObjectID."'";
		
		$resultSet = $this->returnArray($sql);
		$infoArr = $resultSet[0];
		
		foreach ($infoArr as $key => $value)
			$this->{$key} = $value;
	}
	
	function Insert_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = $_SESSION['UserID'];
		# DateModify
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# ModifyBy
		$fieldArr[] = 'ModifiedBy';
		$valueArr[] = $_SESSION['UserID'];
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$sql = '';
		$sql .= ' INSERT INTO '.$this->TableName;
		$sql .= ' ( '.$fieldText.' ) ';
		$sql .= ' VALUES ';
		$sql .= ' ( '.$valueText.' ) ';
		$success = $this->db_db_query($sql);
		
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			$this->Commit_Trans();
			
			return $insertedID;
		}
	}
	
	function Update_Object($DataArr=array())
	{
		if (count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldText = '';
		foreach ($DataArr as $field => $value)
		{
			$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
		}
		$valueFieldText .= ' DateModified = now(), ModifiedBy = \''.$_SESSION['UserID'].'\' ';
		
		$sql = '';
		$sql .= ' UPDATE '.$this->TableName;
		$sql .= ' SET '.$valueFieldText;
		$sql .= ' WHERE '.$this->ID_FieldName.' = \''.$this->ObjectID.'\' ';
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Delete_Object($ObjectIDArr='')
	{
		$SuccessArr = array();
		
		if ($ObjectIDArr=='')
			$ObjectIDArr = array($this->ObjectID);
		$ObjectIDList = implode(',', $ObjectIDArr);
		
		$this->Start_Trans();
		
		# Delete Group
		$sql = "Delete From ".$this->TableName." Where ".$this->ID_FieldName." In ($ObjectIDList) ";
		$SuccessArr['DeleteGroup'] = $this->db_db_query($sql);
		
		# Delete Group Member
		$sql = "Delete From INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER Where ".$this->ID_FieldName." In ($ObjectIDList) ";
		$SuccessArr['DeleteGroupMember'] = $this->db_db_query($sql);
		
		if (in_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	function Is_Name_Valid($Name, $ExcludeObjectID='')
	{
		$cond_excludeObjectID = '';
		if ($ExcludeObjectID != '')
			$cond_excludeObjectID = " And ".$this->ID_FieldName." != '".$ExcludeObjectID."' ";
			
		$sql = "Select
						".$this->ID_FieldName."
				From
						".$this->TableName."
				Where
						GroupName = '".$this->Get_Safe_Sql_Query($Name)."'
						$cond_excludeObjectID
				";
		$resultSet = $this->returnVector($sql);
		
		if (count($resultSet) == 0)
			return 1;
		else
			return 0;
	}
	
	function Get_Member_List()
	{
		$namefield = getNameFieldWithClassNumberByLang('iu.');
		$sql = "Select
						iu.UserID,
						$namefield
				From
						INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER as mgm
						Inner Join
						INTRANET_USER as iu
						On (mgm.UserID = iu.UserID)
				Where
						mgm.GroupID = '".$this->ObjectID."'
				";
		$returnArr = $this->returnArray($sql);
		
		return $returnArr;
	}
	
	function Add_Member($TargetMemberArr)
	{
		$TargetMemberArr = array_remove_empty($TargetMemberArr);
		$UserIDArr = $this->Get_User_Array_From_Selection($TargetMemberArr);
		$numOfUser = count($UserIDArr);
		
		if ($numOfUser > 0)
		{
			$InsertValueArr = array();
			for ($i=0; $i<$numOfUser; $i++)
			{
				$thisUserID = $UserIDArr[$i];
				
				$InsertValueArr[] = "('".$thisUserID."', '".$this->ObjectID."', NULL, NULL, '".$_SESSION['UserID']."', '".$_SESSION['UserID']."', now(), now())";
			}
			
			$insertValueList = implode(', ', $InsertValueArr);
			
			$sql = "Insert Into INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER
						(UserID, GroupID, RecordType, RecordStatus, InputBy, ModifiedBy, DateInput, DateModified)
					Values
						$insertValueList
					";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	function Delete_Member($UserIDArr=array())
	{
		if (count($UserIDArr) == 0)
			return false;
			
		$UserIDList = implode(', ', $UserIDArr);
		
		$sql = "Delete From 
					INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER
				Where
					GroupID = '".$this->ObjectID."'
					And
					UserID In ($UserIDList)
				";
		$success = $this->db_db_query($sql);
		
		return $success;
	}
	
	function Get_Available_User($SearchUserLogin='', $ExcludeUserIDArr='')
	{
		if (!is_array($ExcludeUserIDArr))
			$ExcludeUserIDArr = array();
			
		$ExcludeUserIDArr = array_remove_empty($ExcludeUserIDArr);
		if (is_array($ExcludeUserIDArr) && count($ExcludeUserIDArr) > 0)
		{
			$excludeUserList = implode(',', $ExcludeUserIDArr);
			$conds_excludeUserID = " And UserID Not In ($excludeUserList) ";
		}
		
		$MemberInfoArr = $this->Get_Member_List();
		$MemberUserIDArr = Get_Array_By_Key($MemberInfoArr, 'UserID');
		if (count($MemberUserIDArr) > 0)
		{
			$MemberList = implode(',', $MemberUserIDArr);
			$conds_excludeMember = " And UserID Not In ($MemberList) ";
		}
		
		if ($SearchUserLogin != '')
			$conds_userlogin = " And UserLogin Like '%$SearchUserLogin%' ";
		
		$name_field = getNameFieldByLang();
		$sql = "Select
						UserID,
						$name_field as UserName,
						UserLogin
				From
						INTRANET_USER
				Where
						RecordStatus = 1
						And
						RecordType = 1
						$conds_excludeUserID
						$conds_excludeMember
						$conds_userlogin
				Order By
						EnglishName
				";
		$returnArr = $this->returnArray($sql);
		return $returnArr;
	}
	
	function Get_Invalidate_Member_Selection($SelectionList)
	{
		$InvalidSelectionArr = array();
		
		$SelectionArr = explode(',', $SelectionList);
		$SelectionArr = array_remove_empty($SelectionArr);
		$numOfSelection = count($SelectionArr);
		if ($numOfSelection > 0)
		{
			$GroupMemberInfoArr = $this->Get_Member_List();
			$GroupMemberUserIDArr = Get_Array_By_Key($GroupMemberInfoArr, 'UserID');
			
			for ($i=0; $i<$numOfSelection; $i++)
			{
				$thisSelection = $SelectionArr[$i];
				$thisUserIDArr = $this->Get_User_Array_From_Selection(array($thisSelection));
				
				# $thisIsMemberUserIDArr contains the UserID which is member and is in the user selection => which is an invalid selection
				$thisIsMemberUserIDArr = array_intersect($GroupMemberUserIDArr, $thisUserIDArr);
				
				if (count($thisIsMemberUserIDArr))
					$InvalidSelectionArr[] = $thisSelection;
			}
		}
		
		$InvalidSelectionList = implode(',', $InvalidSelectionArr);
		return $InvalidSelectionList;
	}
	
	function Get_All_Management_Group($returnAsso=0)
	{
		$sql = "Select
						GroupID,
						GroupName,
						Description
				From
						INTRANET_EBOOKING_MANAGEMENT_GROUP
				Order By
						GroupName
				";
		$resultArr = $this->returnArray($sql);
		
		if ($returnAsso == 0)
			$returnArr = $resultArr;
		else
		{
			foreach((array)$resultArr as $key => $ValueArr)
			{
				$thisGroupID = $ValueArr['GroupID'];
				$returnArr[$thisGroupID] = $ValueArr;
			}
		}
		
		return $returnArr;
	}
}

?>