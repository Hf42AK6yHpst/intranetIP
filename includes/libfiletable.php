<?php
class libfiletable {

     var $root;
     var $path;
     var $order;
     var $field;
     var $keyword;
     var $folders = array();
     var $files = array();
     var $file_name;
     var $file_size;
     var $file_time;
     var $file_path;
     var $image_path;
     var $folder_img;
     var $file_img;
     var $upfolder_img;
     var $upfolder_msg;
     var $sortby= " Sort By";
     var $no_col = 4;
     var $no_msg;
     var $checkField = 0;
     var $checkField2 = 0;
     var $radioField = 0;
     var $attachment = "";
     var $script_name;

     function libfiletable($root, $path, $order, $field, $keyword){
          global $HTTP_SERVER_VARS, $file_path, $image_path, $i_no_record_exists_msg, $i_no_search_result_msg, $i_FileSystemUp, $i_FileSystemName, $i_FileSystemSize, $i_FileSystemModified, $list_sortby;
          $this->root = $root;
          $this->path = rawurldecode($path);
          $this->order = $order;
          $this->field = $field;
          $this->keyword = $keyword;
          $this->image_path = $image_path;
          $this->no_msg = ($keyword=="") ? $i_no_record_exists_msg : $i_no_search_result_msg;
          $this->file_path = $file_path;
          $this->entry();
          $this->file_name = $i_FileSystemName;
          $this->file_size = $i_FileSystemSize;
          $this->file_time = $i_FileSystemModified;
          $this->folder_img = "/images/folder.gif";
          $this->file_img = "/images/file.gif";
          $this->upfolder_img = "/images/upfolder.gif";
          $this->upfolder_msg = $i_FileSystemUp;
          $this->sortby = $list_sortby;
          $this->script_name = $HTTP_SERVER_VARS["SCRIPT_NAME"];
     }

     function entry(){
          $i = 0; $j = 0;
          $path = $this->root."/".$this->path;
          if (!is_dir($path)) return;

          $d = dir($this->root."/".$this->path);
          while($entry = $d->read()) {
               $filepath = $this->root."/".$this->path."/".$entry;
               if($this->keyword == ""){
                    if (is_dir($filepath) && $entry<>"." && $entry<>"..") $this->folders[$i++] = array($entry, filesize($filepath), filemtime($filepath));
                    if (is_file($filepath)) $this->files[$j++] = array($entry, filesize($filepath), filemtime($filepath));
               }
               else if(strstr($entry,$this->keyword)){
                    if (is_dir($filepath) && $entry<>"." && $entry<>"..") $this->folders[$i++] = array($entry, filesize($filepath), filemtime($filepath));
                    if (is_file($filepath)) $this->files[$j++] = array($entry, filesize($filepath), filemtime($filepath));
               }
          }
          $d->close();
          
          # added by Ivan on 18 May 2008 - sort the file list by the file names
          # 高主教書院小學部 [CRM Ref No.: 2009-0506-0923]
          asort($this->files);
     }

     function column($field_index, $field_name){
          $x = "";
          if($this->field==$field_index){
               $x .= ($this->order==1) ? "<a class=tableTitle href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">" : "<a class=tableTitle href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
          }else{
               $x .= "<a class=tableTitle href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
          }
          $x .= $field_name;
          if($this->field==$field_index){
               $x .= ($this->order==1) ? "<img src='".$this->image_path."/desc.gif' hspace=2 border=0>" : "<img src='".$this->image_path."/asc.gif' hspace=2  border=0>";
          }
          $x .= "</a>";
          return $x;
     }

     function show_column(){
          $x  = (trim($this->path)<>"") ? "<tr><td class=tableTitle2 colspan=".($this->no_col + $this->checkField + $this->radioField).">".$this->path."</td></tr>\n" : "";
          $x .= (trim($this->path)<>"") ? "<tr><td class=tableTitle2 colspan=".($this->no_col + $this->checkField + $this->radioField)."><img src=".$this->upfolder_img." border=0> <a class=iconLink href=\"javascript:fs_openfolder('..',document.form1,'".$this->script_name."')\" onMouseOver=\"window.status='';return true;\" onMouseOut=\"window.status='';return true;\">".$this->upfolder_msg."</a></td></tr>\n" : "";
          $x .= "<tr>\n";
          $x .= "<td class=tableTitle width=1><br></td>\n";
          $x .= "<td class=tableTitle width=50%>".$this->column(0, $this->file_name)."</td>\n";
          $x .= "<td class=tableTitle width=10%>".$this->column(1, $this->file_size)."</td>\n";
          $x .= "<td class=tableTitle width=40%>".$this->column(2, $this->file_time)."</td>\n";
          $x .= ($this->checkField) ? "<td class=tableTitle align=right width=1>".$this->check("filename[]")."</td>\n" : "";
          $x .= ($this->checkField2) ? "<td class=tableTitle align=right width=1>".$this->check("filename[]")."</td>\n" : "";
          $x .= ($this->radioField) ? "<td class=tableTitle align=right width=1><br></td>\n" : "";
          $x .= "</tr>\n";
          return $x;
     }

     function check($id){
          $x = "<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";
          return $x;
     }

     function functionbar($n, $m, $color, $a){
          $x = "";
          if(strstr($m , $n)){
               $x .= "<table width=95% border=0 cellpadding=1 cellspacing=0>\n";
               $x .= "<tr><td class=tableTitle align=right>$a</td></tr>\n";
               $x .= "</table>\n";
          }
          return $x;
     }

     function displayFunctionbar($a, $b){
          return "<table width=100% border=0 cellpadding=1 cellspacing=0><tr><td>$a</td><td align=right>$b</td></tr></table>\n";
     }

     function display(){
          global $intranet_httppath;
          $i = 0;
          $folders = $this->folders;
          $files = $this->files;
          function docmp($a,$b) {
               global $field, $order;
               if ($a[$field] > $b[$field]){
                    if($order == 0) return 1;
                    if($order == 1) return -1;
               }
               if ($a[$field] < $b[$field]){
                    if($order == 0) return -1;
                    if($order == 1) return 1;
               }
               return 0;
          }
          uasort($folders, docmp);
          uasort($files, docmp);

          $x  = "<table width=560 border=0 cellpadding=2 cellspacing=0 align='center' style='border-right: solid #DBD6C4;  border-left: solid #DBD6C4'>\n";
          $x .= $this->show_column();
          while (list($key, $value) = each($folders)) {
               $url = str_replace(" ","%20",$folders[$key][0]);
			   $css = ($i%2) ? "2" : "";
               $x .= "<tr>\n";
               $x .= "<td class=tableContent$css><img src=".$this->folder_img." border=0></td>\n";
               $x .= "<td class=tableContent$css><a class=tableContentLink href=javascript:fs_openfolder(\"".$url."\",document.form1,'".$this->script_name."') onMouseOver=\"window.status='".$folders[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$folders[$key][0]."</a></td>\n";
               $x .= "<td class=tableContent$css>".ceil($folders[$key][1]/1000)."Kb</td>\n";
               $x .= "<td class=tableContent$css>".date("Y-m-d h:i:A",$folders[$key][2])."</td>\n";
               $x .= ($this->checkField) ? "<td class=tableContent$css align=right><input type=checkbox name=filename[] value=\"".$folders[$key][0]."\"></td>\n" : "";
               $x .= ($this->checkField2) ? "<td class=tableContent$css align=right><br></td>\n" : "";
               $x .= ($this->radioField) ? "<td class=tableContent$css align=right><br></td>\n" : "";
               $x .= "</tr>\n";
               $i++;
          }
          while (list($key, $value) = each($files)) {
               $checked = (strstr($this->attachment,$files[$key][0])) ? "CHECKED" : "";
               $url = str_replace(" ", "%20", str_replace($this->file_path, "", $this->root)."/".$this->path."/".$files[$key][0]);
			   $css = ($i%2) ? "2" : "";
               $x .= "<tr>\n";
               $x .= "<td class=tableContent$css><img src=".$this->file_img." border=0></td>\n";
//               $x .= "<td class=tableContent><a class=tableContent href=javascript:fs_view(\"".$url."\") onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a></td>\n";
               $x .= "<td class=tableContent$css><a class=tableContentLink target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a></td>\n";
               $x .= "<td class=tableContent$css>".ceil($files[$key][1]/1000)."Kb</td>\n";
               $x .= "<td class=tableContent$css>".date("Y-m-d h:i:A",$files[$key][2])."</td>\n";
               $x .= ($this->checkField) ? "<td class=tableContent$css align=right><input type=checkbox name=filename[] value=\"".$files[$key][0]."\" $checked></td>\n" : "";
               $x .= ($this->checkField2) ? "<td class=tableContent$css align=right><input type=checkbox name=filename[] value=\"".$files[$key][0]."\" $checked></td>\n" : "";
               $x .= ($this->radioField) ? "<td class=tableContent$css align=right><input type=radio name=filename[] value=\"".$files[$key][0]."\" $checked></td>\n" : "";
               $x .= "</tr>\n";
               $i++;
          }
          $x .= ($i==0) ? "<tr><td class=tableContent align=center colspan=5><br>".$this->no_msg."<br><br></td></tr>\n" : "";
          $x .= "</table>\n";
          return $x;
     }

}
?>