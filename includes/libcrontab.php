<?php
// Editing by 
/*
 * Created by Carlos on 2014-04-11
 * @Description : Simple library for setting cron job
 */

class libcrontab
{
	function libcrontab()
	{
		
	}
	
	/*
	 * $minute (0-59)(use wildcard * for any value)
	 * $hour (0-23)(use wildcard * for any value)
	 * $dayOfMonth (1-31)(use wildcard * for any value)
	 * $month (1-12)(use wildcard * for any value)
	 * $dayOfWeek (0-6) (Sunday is 0)(use wildcard * for any value)
	 * 
	 * $script - http full path to be run, e.g. http://192.168.0.146:31002/schedule_task/job.php
	 */
	function setJob($minute, $hour, $dayOfMonth, $month, $dayOfWeek, $script)
	{
		if($minute[0] != '*' && ($minute == '' || $minute < 0 || $minute > 59))
			return false;
		if($hour[0] != '*' && ($hour == '' || $hour < 0 || $hour > 23))
			return false;
		if($dayOfMonth[0] != '*' && ($hour == '' || $dayOfMonth < 1 || $dayOfMonth > 31))
			return false;
		if($month[0] != '*' && ($month == '' || $month < 0 || $month > 12))
			return false;
		if($dayOfWeek[0] != '*' && ($dayOfWeek == '' || $dayOfWeek < 0 || $dayOfWeek > 6))
			return false;
		if(trim($script) == '')
			return false;
		
		if($minute[0] != '*')
			$minute = sprintf("%d",$minute);
		if($hour[0] != '*')
			$hour = sprintf("%d",$hour);
		if($dayOfMonth[0] != '*')
			$dayOfMonth = sprintf("%d",$dayOfMonth);
		if($month[0] != '*')
			$month = sprintf("%d",$month);
		if($dayOfWeek[0] != '*')
			$dayOfWeek = sprintf("%d",$dayOfWeek);
		
		$crontab_content = shell_exec("crontab -l");
		$jobs = explode("\n",$crontab_content);
		$job_cmd = "$minute $hour $dayOfMonth $month $dayOfWeek /usr/bin/wget -q '$script'";
		
		$crontab_content = "";
		for($i=0;$i<sizeof($jobs);$i++){
			$job = trim($jobs[$i]);
			if($job != ""){
				if(strstr($job,$script) === FALSE){
					$crontab_content .= $job."\n";
				}
			}
		}
		
		$crontab_content .= $job_cmd."\n";
		
		$tmp_file = "/tmp/crontab.txt";
		$fh = fopen($tmp_file,"w");
		if($fh){
			fputs($fh, $crontab_content);
			fclose($fh);
		}
		
		if(file_exists($tmp_file)){
			$output = shell_exec("crontab $tmp_file");
			unlink($tmp_file);
			return $output == ""? true : false;
		}else{
			return false;
		}
	}
	
	/*
	 * $script - http full path to be run, e.g. http://192.168.0.146:31002/schedule_task/job.php
	 */
	function removeJob($script)
	{
		$crontab_content = shell_exec("crontab -l");
		$jobs = explode("\n",$crontab_content);
		
		$crontab_content = "";
		for($i=0;$i<sizeof($jobs);$i++){
			$job = trim($jobs[$i]);
			if($job != ""){
				if(strstr($job,$script) === FALSE){
					$crontab_content .= $job."\n";
				}
			}
		}
		
		$tmp_file = "/tmp/crontab.txt";
		$fh = fopen($tmp_file,"w");
		if($fh){
			fputs($fh, $crontab_content);
			fclose($fh);
		}
		
		if(file_exists($tmp_file)){
			$output = shell_exec("crontab $tmp_file");
			unlink($tmp_file);
			return $output==""? true: false;
		}else{
			return false;
		}
	}
	
	/*
	 * Viewing all existing jobs
	 */
	function getAllJobs()
	{
		$crontab_content = shell_exec("crontab -l");
		$jobs = explode("\n",$crontab_content);
		
		$jobAry = array();
		for($i=0;$i<sizeof($jobs);$i++){
			$job = trim($jobs[$i]);
			if($job != ""){
				$jobAry[] = $job;
			}
		}
		return $jobAry;
	}
	
	/*
	 * $script - http full path to be run, e.g. http://192.168.0.146:31002/schedule_task/job.php
	 */
	function getJobTimes($script)
	{
		$crontab_content = shell_exec("crontab -l");
		$jobs = explode("\n",$crontab_content);
		
		$target_job = "";
		for($i=0;$i<sizeof($jobs);$i++){
			$job = trim($jobs[$i]);
			if($job != ""){
				if(strstr($job,$script) !== FALSE){
					$target_job = $job;
					break;
				}
			}
		}
		
		if($target_job == "")
			return false;
			
		$parts = explode(" ",$target_job);
		
		return array('minute'=>$parts[0],'hour'=>$parts[1],'dayOfMonth'=>$parts[2],'month'=>$parts[3],'dayOfWeek'=>$parts[4]);
	}
	
}

?>