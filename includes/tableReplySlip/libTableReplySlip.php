<?php
// editing by 
/*
 * 2013-09-17 (Carlos): added updateShowUserInfoInResult()
 */

if (!defined("LIBTABLEREPLYSLIP_DEFINED")) {
	define("LIBTABLEREPLYSLIP_DEFINED", true);
	
	class libTableReplySlip extends libdbobject {
		private $replySlipId;
		private $linkToModule;
		private $linkToType;
		private $linkToId;
		private $title;
		private $description;
		private $ansAllQuestion;
		private $showUserInfoInResult;
		private $recordType;
		private $recordStatus;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			parent::__construct('INTRANET_TABLE_REPLY_SLIP', 'ReplySlipID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setLinkToModule($val) {
			$this->linkToModule = $val;
		}
		public function getLinkToModule() {
			return $this->linkToModule;
		}
		
		public function setLinkToType($val) {
			$this->linkToType = $val;
		}
		public function getLinkToType() {
			return $this->linkToType;
		}
		
		public function setLinkToId($val) {
			$this->linkToId = $val;
		}
		public function getLinkToId() {
			return $this->linkToId;
		}
		
		public function setTitle($val) {
			$this->title = $val;
		}
		public function getTitle() {
			return $this->title;
		}
		
		public function setDescription($val) {
			$this->description = $val;
		}
		public function getDescription() {
			return $this->description;
		}
		
		public function setAnsAllQuestion($val) {
			$this->ansAllQuestion = $val;
		}
		public function getAnsAllQuestion() {
			return $this->ansAllQuestion;
		}
		
		public function setShowUserInfoInResult($val) {
			$this->showUserInfoInResult = $val;
		}
		public function getShowUserInfoInResult() {
			return $this->showUserInfoInResult;
		}
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			return $this->recordType;
		}
		
		public function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		public function getRecordStatus() {
			return $this->recordStatus;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('ReplySlipID', 'int', 'setReplySlipId', 'getReplySlipId');
			$fieldMappingAry[] = array('LinkToModule', 'str', 'setLinkToModule', 'getLinkToModule');
			$fieldMappingAry[] = array('LinkToType', 'str', 'setLinkToType', 'getLinkToType');
			$fieldMappingAry[] = array('LinkToID', 'int', 'setLinkToId', 'getLinkToId');
			$fieldMappingAry[] = array('Title', 'str', 'setTitle', 'getTitle');
			$fieldMappingAry[] = array('Description', 'str', 'setDescription', 'getDescription');
			$fieldMappingAry[] = array('AnsAllQuestion', 'int', 'setAnsAllQuestion', 'getAnsAllQuestion');
			$fieldMappingAry[] = array('ShowUserInfoInResult', 'int', 'setShowUserInfoInResult', 'getShowUserInfoInResult');
			$fieldMappingAry[] = array('RecordType', 'int', 'setRecordType', 'getRecordType');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($this->getInputBy());
			$this->setModifiedDate('now()');
			$this->setModifiedBy($this->getModifiedBy());
			
			return true;
		}

		public function deleteRecord() {
			global $tableReplySlipConfig;
			
			$this->setRecordStatus($tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['deleted']);
			$objectId = $this->save();
			
			return ($objectId > 0)? true : false; 
		}
		
		public function updateShowUserInfoInResult($val)
		{
			$this->setShowUserInfoInResult($val);
			$objectId = $this->save();
			
			return ($objectId > 0)? true : false;
		}
		
		public function returnItemData($RowColumnAssoc=false) {
			$replySlipId = $this->getReplySlipId();
			$sql = "Select ItemID,RecordType,Content,RowOrder,ColumnOrder From INTRANET_TABLE_REPLY_SLIP_ITEM Where ReplySlipID = '".$replySlipId."' ORDER BY RowOrder,ColumnOrder";
			$record = $this->objDb->returnResultSet($sql);
			if($RowColumnAssoc) {
				$assocRecord = array();
				$numOfRecord = count($record);
				for($i=0;$i<$numOfRecord;$i++) {
					$__row = $record[$i]['RowOrder'];
					$__column = $record[$i]['ColumnOrder'];
					if(!isset($assocRecord[$__row])) {
						$assocRecord[$__row] = array();
					}
					$assocRecord[$__row][$__column] = $record[$i];
				}
				return $assocRecord;
			}
			
			return $record;
		}
		
		public function returnOptionData($TypeUniqueIdAssoc=false) {
			$replySlipId = $this->getReplySlipId();
			
			$sql = "SELECT OptionID,UniqueID,RecordType,Content,DisplayOrder FROM INTRANET_TABLE_REPLY_SLIP_OPTION WHERE ReplySlipID='".$replySlipId."' ORDER BY UniqueID,DisplayOrder";
			$record = $this->objDb->returnResultSet($sql);
			if($TypeUniqueIdAssoc) {
				$assocRecord = array();
				$numOfRecord = count($record);
				for($i=0;$i<$numOfRecord;$i++) {
					$__recordType = $record[$i]['RecordType'];
					$__uniqueID = $record[$i]['UniqueID'];
					if(!isset($assocRecord[$__recordType])) {
						$assocRecord[$__recordType] = array();
					}
					if(!isset($assocRecord[$__recordType][$__uniqueID])) {
						$assocRecord[$__recordType][$__uniqueID] = array();
					}
					$assocRecord[$__recordType][$__uniqueID][] = $record[$i];
				}
				return $assocRecord;
			}
			
			return $record;
		}
	}
}
?>