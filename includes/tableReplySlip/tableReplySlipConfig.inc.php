<?php
$tableReplySlipConfig = array();

$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['yes'] = 1;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['no'] = 2;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordType']['realForm'] = 1;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordType']['template'] = 2;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['active'] = 1;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['draft'] = 2;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['deleted'] = 3;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['importTemp'] = 4;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['show'] = 1;
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['hide'] = 2;

$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText'] = 'HT';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText'] = 'DT';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['shortText'] = 'ST';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['longText'] = 'LT';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelection'] = 'SS';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelection'] = 'MS';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckbox'] = 'SC';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckbox'] = 'MC';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelectionOption'] = 'SSO';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelectionOption'] = 'MSO';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckboxOption'] = 'SCO';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckboxOption'] = 'MCO';

$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleSelectionOption'] = 'SSO';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleSelectionOption'] = 'MSO';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleCheckboxOption'] = 'SCO';
$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleCheckboxOption'] = 'MCO';

$tableReplySlipConfig['ajaxDataSeparator'] = '||';
$tableReplySlipConfig['importDataLineBreakReplacement'] = '<!--lineBreakHere-->';
$tableReplySlipConfig['itemIdHiddenFieldSeparator'] = ',';

$tableReplySlipConfig['exportTypeAry']['layoutView'] = 'layoutView';
$tableReplySlipConfig['exportTypeAry']['rawData'] = 'rawData';
$tableReplySlipConfig['exportTypeAry']['freqStat'] = 'freqStat';
?>