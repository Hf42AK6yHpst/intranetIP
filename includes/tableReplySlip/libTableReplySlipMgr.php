<?php
// editing by 
/*
 * 2014-12-01 (Carlos): modified returnReplySlipHtml(), added parameter ShowSelfAnswerOnly
 * 2013-09-18 (Carlos): added updateShowUserInfoInResult()
 */
if (!defined("LIBTABLEREPLYSLIPMGR_DEFINED")) {
	define("LIBTABLEREPLYSLIPMGR_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	include_once($PATH_WRT_ROOT.'includes/libinterface.php');
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	include_once($PATH_WRT_ROOT.'includes/tableReplySlip/tableReplySlipConfig.inc.php');
	include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlip.php');
	include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlipItem.php');
	include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlipOption.php');
	include_once($PATH_WRT_ROOT."includes/table/table_commonTable.php");
	include_once($PATH_WRT_ROOT."lang/table_reply_slip_lang.".(!isset($intranet_session_language)? (isset($intranet_default_lang)? $intranet_default_lang : 'en') : $intranet_session_language).".php");
	
	class libTableReplySlipMgr {
		
		private $libdb;
		private $linterface;
		private $limporttext;
		private $csvFilePath;
		private $csvDataAry;
		private $csvErrorAry;
		private $curUserId;
		private $replySlipId;
		private $linkToModule;
		private $linkToType;
		private $linkToId;
		private $ansAllQuestion;
		private $showUserInfoInResult;
		private $recordType;
		private $recordStatus;
		
		public function __construct() {
			$this->libdb = new libdb();
			$this->linterface = new interface_html();
			$this->csvDataAry = array();
			$this->resetCsvErrorAry();
		}
	
		private function getInstanceLibImportText() {
			if ($this->limporttext === null) {
				$this->limporttext = new libimporttext();
			}
			
			return $this->limporttext;
		}
		
		public function setCsvFilePath($val) {
			$this->csvFilePath = $val;
			$this->loadCsvFileData();
		}
		public function getCsvFilePath() {
			return $this->csvFilePath;
		}
		
		public function setCsvDataAry($val) {
			$this->csvDataAry = $val;
		}
		public function getCsvDataAry() {
			return $this->csvDataAry;
		}
		
		public function setCurUserId($val) {
			$this->curUserId = $val;
		}
		public function getCurUserId() {
			return $this->curUserId;
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setLinkToModule($val) {
			$this->linkToModule = $val;
		}
		public function getLinkToModule() {
			return $this->linkToModule;
		}
		
		public function setLinkToType($val) {
			$this->linkToType = $val;
		}
		public function getLinkToType() {
			return $this->linkToType;
		}
		
		public function setLinkToId($val) {
			$this->linkToId = $val;
		}
		public function getLinkToId() {
			return $this->linkToId;
		}
		
		public function setAnsAllQuestion($val) {
			$this->ansAllQuestion = $val;
		}
		public function getAnsAllQuestion() {
			global $tableReplySlipConfig;
			
			if ($this->ansAllQuestion == '') {
				return $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['no'];
			}
			else {
				return $this->ansAllQuestion;
			}
		}
		
		public function setShowUserInfoInResult($val) {
			$this->showUserInfoInResult = $val;
		}
		public function getShowUserInfoInResult() {
			global $tableReplySlipConfig;
			
			if ($this->showUserInfoInResult == '') {
				return $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['hide'];
			}
			else {
				return $this->showUserInfoInResult;
			}
		}
		
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			global $tableReplySlipConfig;
			
			if ($this->recordType == '') {
				return $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordType']['realForm'];
			}
			else {
				return $this->recordType;
			}
		}
		
		public function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		public function getRecordStatus() {
			global $tableReplySlipConfig;
			
			if ($this->recordStatus == '') {
				return $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['active'];
			}
			else {
				return $this->recordStatus;
			}
		}
		
		private function resetCsvErrorAry() {
			$this->csvErrorAry = array();
		}
		private function addCsvError($lineNumber, $columnNumber, $errorCode) {
			$this->csvErrorAry[$lineNumber][] = array($columnNumber, $errorCode);
		}
		public function getCsvErrorAry() {
			return $this->csvErrorAry;
		}
		
		public function returnReplySlipHtmlByCsvData() {
			$successAry = array();
			
			if ($this->isCsvFileValid()) {
				$replySlipHtml = $this->returnReplySlipHtml($forCsvPreview=true);
				return $replySlipHtml;
			}
			else {
				return $this->returnCsvWarningTable();
			}
		}
		
		private function loadCsvFileData() {
			global $tableReplySlipConfig;
			
			$libimport = $this->getInstanceLibImportText();
			$filePath = $this->getCsvFilePath();
			
			$this->setCsvDataAry($libimport->GET_IMPORT_TXT($filePath, $incluedEmptyRow=0, $tableReplySlipConfig['importDataLineBreakReplacement']));
		}
		
		public function isCsvFileValid() {
			global $tableReplySlipConfig;
			
			$libimport = $this->getInstanceLibImportText();
			$isValid = true;
			
			$headerAry = $this->getImportHeaderAry();
			$coreHeaderAry = $headerAry['En'];
			$libimport->SET_CORE_HEADER($coreHeaderAry);
			
			$csvDataAry = $this->getCsvDataAry();
			$numOfMaxColumn = $this->getMaxNumOfColumnInCsvData(true); // count max non-empty table columns
			//debug_r($csvDataAry);
			//debug_r($numOfMaxColumn);
			### Check header
			$isHeaderValid = $libimport->VALIDATE_HEADER($csvDataAry, $ParExactlyCore=false);
			if (!$isHeaderValid) {
				$isValid = false;
				$this->addCsvError(0, 1, 'invalidHeader');
			}
			
			### Check data
			array_shift($csvDataAry);
			array_shift($csvDataAry);
			$numOfData = count($csvDataAry);
			$optionIdAry = array(); // for checking duplicated option ID definition
			for ($i=0; $i<$numOfData; $i++) {
				$_lineNumber = $i + 2;
				$numOfColumn = count($csvDataAry[$i]);
				$numOfNonEmptyColumn = $this->countNonEmptyColumn($csvDataAry[$i]);
				
				$firstColumnType = strtoupper(trim($csvDataAry[$i][0]));
				$firstColumnContent = trim($csvDataAry[$i][1]);
				
				// check missing table columns
				if(in_array($firstColumnType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType'])
					&& !in_array($firstColumnType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']) 
					&& ($numOfNonEmptyColumn < $numOfMaxColumn) ) {
					$isValid = false;
					$this->addCsvError($_lineNumber, $numOfColumn, 'missingColumns');
				}
				
				// check option row - all type should be in same option type
				if(in_array($firstColumnType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])) {
					$invalidOptionColumns = $this->getOptionRowInvalidColumns($csvDataAry[$i]);
					$numOfInvalidOptionColumn = count($invalidOptionColumns);
					if($numOfInvalidOptionColumn > 0) {
						for($j=0;$j<$numOfInvalidOptionColumn;$j++) {
							$isValid = false;
							$this->addCsvError($_lineNumber, $invalidOptionColumns[$j],'invalidOptions');
						}
					}
					if(in_array($firstColumnContent,$optionIdAry)) {
						$isValid = false;
						$this->addCsvError($_lineNumber, 2,'duplicatedOptionID');
					}
					$optionIdAry[] = $firstColumnContent; // Option ID
				}
				
				for($j=0;$j<$numOfColumn;$j+=2) {
					
					$itemType = strtoupper(trim($csvDataAry[$i][$j]));
					$itemContent = trim($csvDataAry[$i][$j+1]);
					
					// check if valid item type
					if ($itemType == '') {
						if($j != 0 && in_array($firstColumnType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']))
						{
							
						} else if (!in_array($itemType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])
								&& $j < $numOfMaxColumn){
							$isValid = false;
							$this->addCsvError($_lineNumber, $j+1,'missingItemType');
						}
					}
					else {
						
						if (!in_array($itemType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType'])) {
							$isValid = false;
							$this->addCsvError($_lineNumber, $j+1, 'invalidItemType');
						}
					}
					
					if($j==0 && in_array($itemType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']) && $itemContent=='') {
						$isValid = false;
						$this->addCsvError($_lineNumber, $j+2, 'missingOptionID');
					}
					
					// get options
					if(in_array($itemType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])) {
						$_optionAry = $this->getImportOptionAryFromRowData($csvDataAry[$i]);
						$_numOfOption = count($_optionAry);
						if ($_numOfOption == 0) {
							$isValid = false;
							$this->addCsvError($_lineNumber, $j+1, 'missingOptions');
						}
					}
				}
			}
			
			return $isValid;
		}
		
		public function returnCsvWarningTable() {
			global $Lang;
			
			$errorAry = $this->getCsvErrorAry();
			$csvDataAry = $this->getCsvDataAry();
			
			$importHeaderAry = $this->getImportHeaderAry();
			$importHeaderAry = Get_Lang_Selection($importHeaderAry['Ch'], $importHeaderAry['En']);
			$numOfHeader = count($importHeaderAry);
			
			$numOfMaxColumn = $this->getMaxNumOfColumnInCsvData(false);
			//$numOfEmptyRow = $numOfMaxColumn - 1;
			
			
			### Show error result
			$x = '';
			if (isset($errorAry[0])) {
				// Have general error => show error message instead of error in each row
				$numOfError = count($errorAry[0]);
				$errorDisplayAry = array();
				for ($i=0; $i<$numOfError; $i++) {
					$errorDisplayAry[] = $this->returnImportErrorMsgByErrorCode($errorAry[0][$i][1]);
				}
				$x .= $this->linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['General']['Error'].'</span>', $errorDisplayAry);
			}
			else {
				// Create table
				$tableObj = new table_commonTable();
				$tableObj->setTableType('view');
				$tableObj->setApplyConvertSpecialChars(false);
				
				// table header
				$tableObj->addHeaderTr(new tableTr());
				$tableObj->addHeaderCell(new tableTh($Lang['General']['ImportArr']['Row']));
				for ($i=0; $i<$numOfMaxColumn; $i++) {
					$_headerTitle = $importHeaderAry[$i];
					$tableObj->addHeaderCell(new tableTh($_headerTitle));
				}
				//for ($i=0; $i<$numOfEmptyRow; $i++) {
				//	$tableObj->addHeaderCell(new tableTh('&nbsp;'));
				//}
				$tableObj->addHeaderCell(new tableTh($Lang['General']['Error']));
				
				// table body
				foreach ((array)$errorAry as $_rowNum => $_errorCodeAry) {
					$_csvDataAry = $csvDataAry[$_rowNum];
					$_numOfCol = count((array)$_csvDataAry);
					
					$errorColumnNumAry = array();
					$_numOfError = count($_errorCodeAry);
					$_errorDisplayAry = array();
					for ($i=0; $i<$_numOfError; $i++) {
						$errorColumnNumAry[] = $_errorCodeAry[$i][0];
						$_errorDisplayAry[] = $Lang['TableReplySlip']['Column'].' '.$_errorCodeAry[$i][0].' - '.$this->returnImportErrorMsgByErrorCode($_errorCodeAry[$i][1]);
					}
					$_errorDisplay = implode('<br />', $_errorDisplayAry);
					
					$tableObj->addBodyTr(new tableTr());
					$tableObj->addBodyCell(new tableTd($_rowNum+1));
					for ($i=0; $i<$_numOfCol; $i++) {
						if(in_array($i+1,$errorColumnNumAry)) {
							$__displayContent = '<span class="tabletextrequire">'.($_csvDataAry[$i]==''?'###':$_csvDataAry[$i]).'</span>';
						}else {
							$__displayContent = $_csvDataAry[$i];
						}
						$tableObj->addBodyCell(new tableTd($__displayContent));
					}
					$tableObj->addBodyCell(new tableTd($_errorDisplay));
				}
				
				$x = $tableObj->returnHtml();
			}
			
			return $x;
		}
		
		private function returnImportErrorMsgByErrorCode($code) {
			global $Lang;
			return $Lang['TableReplySlip']['WarningArr'][$code];
		}
		
		public function saveCsvDataToDb() {
			global $tableReplySlipConfig;
			
			$successAry = array();
			
			$replySlipId = $this->getReplySlipId();
			
			$csvDataAry = $this->getCsvDataAry();
			array_shift($csvDataAry);
			array_shift($csvDataAry);
			
			### Create Reply Slip
			$replySlipObj = new libTableReplySlip($replySlipId);
			$replySlipObj->setLinkToModule($this->getLinkToModule());
			$replySlipObj->setLinkToType($this->getLinkToType());
			$replySlipObj->setLinkToId($this->getLinkToId());
			$replySlipObj->setAnsAllQuestion($this->getAnsAllQuestion());
			$replySlipObj->setShowUserInfoInResult($this->getShowUserInfoInResult());
			$replySlipObj->setRecordType($this->getRecordType());
			$replySlipObj->setRecordStatus($this->getRecordStatus());
			$replySlipObj->setInputBy($this->getCurUserId());
			$replySlipObj->setModifiedBy($this->getCurUserId());
			$replySlipId = $replySlipObj->save();
			$successAry['insertReportSlip'] = ($replySlipId > 0)? true : false;
			
			
			### Create Questions & Options
			$numOfRow = count($csvDataAry);
			$rowNumber = -1; // counting of question table row
			for ($i=0; $i<$numOfRow; $i++) {
				$numOfColumn = count($csvDataAry[$i]);
				
				$firstColumnItemType = $csvDataAry[$i][0];
				$firstColumnItemContent = $csvDataAry[$i][1];
				
				if(in_array($firstColumnItemType, $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])) {
					// item for options
					$__uniqueId = $firstColumnItemContent;
					
					$_numOfOptions = count($csvDataAry[$i]);
					for ($j=2; $j<$_numOfOptions; $j+=2) {
						$__optionType = $csvDataAry[$i][$j];
						$__optionContent = $csvDataAry[$i][$j+1];
						$__optionContent = str_replace($tableReplySlipConfig['importDataLineBreakReplacement'], "\n", $__optionContent);
						
						if($__optionType != "") {
							$__optionObj = new libTableReplySlipOption();
							$__optionObj->setReplySlipId($replySlipId);
							$__optionObj->setUniqueId($__uniqueId);
							$__optionObj->setContent($__optionContent);
							$__optionObj->setRecordType($__optionType);
							$__optionObj->setDisplayOrder(intval($j / 2));
							$__optionObj->setInputBy($this->getCurUserId());
							$__optionObj->setModifiedBy($this->getCurUserId());
							$__optionId = $__optionObj->save();
							
							$successAry[$replySlipId][$__optionId]['insertOption'][] = ($__optionId > 0)? true : false;
						}
					}
					
				} else if(in_array($firstColumnItemType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType'])){
					// item for table questions
					$rowNumber++;
					for($j=0;$j<$numOfColumn;$j+=2) {
					
						$_itemType = $csvDataAry[$i][$j];
						$_itemContent = $csvDataAry[$i][$j+1];
						$_itemContent = str_replace($tableReplySlipConfig['importDataLineBreakReplacement'], "\n", $_itemContent);
						
						if($_itemType != "") {
							// Create Question
							$_itemObj = new libTableReplySlipItem();
							$_itemObj->setReplySlipId($replySlipId);
							$_itemObj->setRecordType($_itemType);
							$_itemObj->setContent($_itemContent);
							$_itemObj->setRowOrder($rowNumber);
							$_itemObj->setColumnOrder($j / 2);
							$_itemObj->setInputBy($this->getCurUserId());
							$_itemObj->setModifiedBy($this->getCurUserId());
							$_itemId = $_itemObj->save();
							$successAry[$replySlipId]['insertItem'][] = ($_itemId > 0)? true : false;
						}
					}
					
				}
			}
			
			if (in_multi_array(false, $successAry)) {
				return false;
			}
			else {
				$this->setReplySlipId($replySlipId);
				return true;
			}
		}
		
		public function deleteReplySlipData() {
			$replySlipId = $this->getReplySlipId();
			if ($replySlipId == '') {
				return false;
			}
			
			### Get reply slip info
			$replySlipObj = new libTableReplySlip($replySlipId);
			return $replySlipObj->deleteRecord();
		}
		
		private function getDataArrayForSelection($dataAry,$keyFieldName,$valueFieldName)
		{
			$returnAry = array();
			$numOfRecord = count($dataAry);
			
			for($i=0;$i<$numOfRecord;$i++) {
				$value = intranet_htmlspecialchars($dataAry[$i][$keyFieldName]);
				$display_text = intranet_htmlspecialchars($dataAry[$i][$valueFieldName]);
				$returnAry[] = array($value,$display_text);
			}
			return $returnAry;
		}
		
		public function returnReplySlipHtml($forCsvPreview=false, $showUserAnswer=false, $disabledAnswer=false, $showSelfAnswerOnly=false) {
			global $tableReplySlipConfig, $Lang;
			
			### Get reply slip info
			$questionAry = array();
			$optionAssoAry = array();	
			if ($forCsvPreview) {
				$replySlipObj = new libTableReplySlip();
				$replySlipObj->setAnsAllQuestion($this->getAnsAllQuestion());
				$replySlipObj->setRecordStatus($tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['importTemp']);
				
				### Create Questions & Options
				$csvDataAry = $this->getCsvDataAry();
				//debug_r($csvDataAry);
				array_shift($csvDataAry);
				array_shift($csvDataAry);
				//debug_r($csvDataAry);
				### Create Questions & Options
				$numOfRow = count($csvDataAry);
				$rowNumber = -1; // counting of question table row
				for ($i=0; $i<$numOfRow; $i++) {
					$numOfColumn = count($csvDataAry[$i]);
					
					$firstColumnItemType = $csvDataAry[$i][0];
					$firstColumnItemContent = $csvDataAry[$i][1];
					
					if(in_array($firstColumnItemType, $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])) {
						// item for options
						$__uniqueId = $firstColumnItemContent;
						
						$_numOfOptions = count($csvDataAry[$i]);
						for ($j=2; $j<$_numOfOptions; $j+=2) {
							$__optionType = $csvDataAry[$i][$j];
							$__optionContent = $csvDataAry[$i][$j+1];
							$__optionContent = str_replace($tableReplySlipConfig['importDataLineBreakReplacement'], "\n", $__optionContent);
							
							if(!isset($optionAssoAry[$__optionType])) {
								$optionAssoAry[$__optionType] = array();
							}
							if(!isset($optionAssoAry[$__optionType][$__uniqueId])) {
								$optionAssoAry[$__optionType][$__uniqueId] = array();
							}
							$optionAssoAry[$__optionType][$__uniqueId][] = array('Content'=> $__optionContent);
							
						}
						
					} else if(in_array($firstColumnItemType,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType'])){
						$rowNumber++;
						// item for table questions
						if(!isset($questionAry[$rowNumber])) {
							$questionAry[$rowNumber] = array();
						}
						
						for($j=0;$j<$numOfColumn;$j+=2) {
							$__col = $j / 2;
							
							$_itemType = $csvDataAry[$i][$j];
							$_itemContent = $csvDataAry[$i][$j+1];
							$_itemContent = str_replace($tableReplySlipConfig['importDataLineBreakReplacement'], "\n", $_itemContent);
							
							if($_itemType != "") {
								// item for table questions
								$questionAry[$rowNumber][$__col] = array('ItemID'=> $rowNumber.'_'.$__col,'RecordType'=>$_itemType,'Content'=>$_itemContent);
							}
						}
						
					}
				}
			}
			else {
				$replySlipId = $this->getReplySlipId();
				if ($replySlipId == '') {
					return false;
				}
				
				### Get Reply Slip object
				$replySlipObj = new libTableReplySlip($replySlipId);
				
				### Get reply slip question
				$questionAry = $replySlipObj->returnItemData(true);
				
				### Get options of each questions
				$optionAssoAry = $replySlipObj->returnOptionData(true);
			}
			//debug_r($questionAry);
			//debug_r($optionAssoAry);
			$recordStatus = $replySlipObj->getRecordStatus();
			$numOfRow = count($questionAry);
			$numOfColumn = count($questionAry[0]);
			
			if ($disabledAnswer) {
				$disableOption = true;
			}
			else {
				$disableOption = ($recordStatus == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['importTemp'])? true : false;
			}
			$withWarningDiv = false;
			if(!$forCsvPreview && !$disableOption) {
				$withWarningDiv = true;
			}
			//$disableOption = false;
			$curUserId = $this->getCurUserId();
			if ($showUserAnswer && $curUserId != '') {
				$answerAssoAry = $this->returnUserAnswer($replySlipId,array($curUserId),true);
			}
			
			
			### Create table
			$tableObj = new table_commonTable();
			$tableObj->setTableType('view');
			$tableObj->setTableExtraClass('superTable');
			$tableObj->setTableId('replySlipTbl_'.$this->getReplySlipId());
			$tableObj->setApplyConvertSpecialChars(false);
			
			$questionHtml = '';
			$numOfHeaderRow = $this->countNumOfHeaderRow($questionAry);
			$numOfFixedCol = $this->countNumOfFixedColumn($questionAry);
			$itemIdAry = array();
			for ($i=0; $i<$numOfRow; $i++) {
				if ($questionAry[$i][0]['RecordType'] == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText']) {
					$isHeaderTr = true;
				}
				else {
					$isHeaderTr = false;
				}
				
				if ($isHeaderTr) {
					$tableObj->addHeaderTr(new tableTr());
				}
				else {
					$tableObj->addBodyTr(new tableTr());
				}
				
				for($j=0; $j < $numOfColumn; $j++) {
				
					$_itemId = $questionAry[$i][$j]['ItemID'];
					$_recordType = $questionAry[$i][$j]['RecordType'];
					
					$_content = $questionAry[$i][$j]['Content'];
					$__elementId = 'tableReplySlipItem_'.$replySlipId.'_'.$_itemId;
					
					if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText'])
					{
						$_content = nl2br(strip_tags($questionAry[$i][$j]['Content'], '<br>'));
						$cellObj = new tableTh($_content);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText'])
					{
						$_content = nl2br(strip_tags($questionAry[$i][$j]['Content'], '<br>'));
						$cellObj = new tableTd($_content);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['shortText'])					
					{
						$itemIdAry[] = $_itemId;
						$__paramAry = array();
						if ($disableOption) {
							$__paramAry['disabled'] = 'disabled';
						}
						//$__paramAry['size'] = '100';
						$userAnswer = '';
						if(isset($answerAssoAry[$curUserId]) && isset($answerAssoAry[$curUserId][$_itemId])) {
							$userAnswer = $answerAssoAry[$curUserId][$_itemId][0]['Content'];
						}
						if($showSelfAnswerOnly){
							$_optionHtml = $userAnswer;
						}else{
							$_optionHtml = $this->linterface->GET_TEXTBOX($__elementId, $__elementId, $userAnswer, $OtherClass='replySlipAnswer_'.$replySlipId.'_'.$_itemId, $__paramAry);
						}
						if($withWarningDiv) {
							$_optionHtml .= $this->linterface->Get_Thickbox_Warning_Msg_Div('tableReplySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_itemId, $Lang['TableReplySlip']['WarningArr']['mustAnswerQuestion'], 'tableReplySlipWarningDiv_'.$replySlipId);
						}
						$cellObj = new tableTd($_optionHtml);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['longText']) {
						$itemIdAry[] = $_itemId;
						$userAnswer = '';
						if(isset($answerAssoAry[$curUserId]) && isset($answerAssoAry[$curUserId][$_itemId])) {
							$userAnswer = $answerAssoAry[$curUserId][$_itemId][0]['Content'];
						}
						if($showSelfAnswerOnly){
							$_optionHtml = $userAnswer;
						}else{
							$_optionHtml = $this->linterface->GET_TEXTAREA($__elementId, $userAnswer, $taCols=70, $taRows=5, $OnFocus="", ($disableOption?1:0), $other='', $class='replySlipAnswer_'.$replySlipId.'_'.$_itemId, $__elementId, $CommentMaxLength='');
						}
						if($withWarningDiv) {
							$_optionHtml .= $this->linterface->Get_Thickbox_Warning_Msg_Div('tableReplySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_itemId, $Lang['TableReplySlip']['WarningArr']['mustAnswerQuestion'], 'tableReplySlipWarningDiv_'.$replySlipId);
						}
						$cellObj = new tableTd($_optionHtml);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelection'])
					{
						
						$itemIdAry[] = $_itemId;
						$_tag = 'id="'.$__elementId.'" name="'.$__elementId.'"';
						if ($disableOption) {
							$_tag .= ' disabled="disabled"';
						}
						$_optionDataAry = $this->getDataArrayForSelection($optionAssoAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleSelectionOption']][$_content],'Content','Content');
						$userAnswer = '';
						if(isset($answerAssoAry[$curUserId]) && isset($answerAssoAry[$curUserId][$_itemId])) {
							$userAnswer = intranet_htmlspecialchars($answerAssoAry[$curUserId][$_itemId][0]['Content']);
						}
						if($showSelfAnswerOnly){
							$_optionHtml = $userAnswer;
						}else{
							$_optionHtml = $this->linterface->GET_SELECTION_BOX($_optionDataAry, $_tag, '', $userAnswer);
						}
						if($withWarningDiv) {
							$_optionHtml .= $this->linterface->Get_Thickbox_Warning_Msg_Div('tableReplySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_itemId, $Lang['TableReplySlip']['WarningArr']['mustAnswerQuestion'], 'tableReplySlipWarningDiv_'.$replySlipId);
						}
						$cellObj = new tableTd($_optionHtml);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelection'])
					{
						$itemIdAry[] = $_itemId;
						$_tag = 'id="'.$__elementId.'" name="'.$__elementId.'[]" multiple="multiple" ';
						if ($disableOption) {
							$_tag .= ' disabled="disabled"';
						}
						$_optionDataAry = $this->getDataArrayForSelection($optionAssoAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleSelectionOption']][$_content],'Content','Content');
						$userAnswerArr = array();
						if(isset($answerAssoAry[$curUserId]) && isset($answerAssoAry[$curUserId][$_itemId])) {
							$userAnswerArr = Get_Array_By_Key($answerAssoAry[$curUserId][$_itemId],'Content');
							for($n=0;$n<count($userAnswerArr);$n++) {
								$userAnswerArr[$n] = intranet_htmlspecialchars($userAnswerArr[$n]);
							}
						}
						if(count($_optionDataAry) >= 10){
							$_tag .= ' size="10"';
						}
						if($showSelfAnswerOnly){
							$_optionHtml = implode('<br />',$userAnswerArr);
						}else{
							$_optionHtml = $this->linterface->GET_SELECTION_BOX($_optionDataAry, $_tag, '', $userAnswerArr);
						}
						if($withWarningDiv) {
							$_optionHtml .= $this->linterface->Get_Thickbox_Warning_Msg_Div('tableReplySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_itemId, $Lang['TableReplySlip']['WarningArr']['mustAnswerQuestion'], 'tableReplySlipWarningDiv_'.$replySlipId);
						}
						$cellObj = new tableTd($_optionHtml);
					}else if ($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckbox']) {
						$itemIdAry[] = $_itemId;
						$_radioAry = $optionAssoAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleCheckboxOption']][$_content];
						$_numOfRadio = count($_radioAry);
						$_optionHtml = '';
						$userAnswer = '';
						if(isset($answerAssoAry[$curUserId]) && isset($answerAssoAry[$curUserId][$_itemId])) {
							$userAnswer = intranet_htmlspecialchars($answerAssoAry[$curUserId][$_itemId][0]['Content']);
						}
						
						for($k=0;$k<$_numOfRadio;$k++) {
							$_val = intranet_htmlspecialchars($_radioAry[$k]['Content']);
							if($showSelfAnswerOnly){
								if($_val == $userAnswer){
									$_optionHtml .= $_val.'<br />';
								}
							}else{
								$_optionHtml .= $this->linterface->Get_Radio_Button($__elementId."_".$k, $__elementId, $_val, ($_val == $userAnswer), $class='replySlipAnswer_'.$replySlipId.'_'.$_itemId, '', $Onclick="", $disableOption).'</em>';
								$_optionHtml .= '<span><label for="'.$__elementId."_".$k.'">'.$_val.'</label></span>';
								$_optionHtml .= '<br />';
							}
						}
						if($withWarningDiv) {
							$_optionHtml .= $this->linterface->Get_Thickbox_Warning_Msg_Div('tableReplySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_itemId, $Lang['TableReplySlip']['WarningArr']['mustAnswerQuestion'], 'tableReplySlipWarningDiv_'.$replySlipId);
						}
						$cellObj = new tableTd($_optionHtml);
					}else if ($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckbox']) {
						$itemIdAry[] = $_itemId;
						$_checkboxAry = $optionAssoAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleCheckboxOption']][$_content];
						$_numOfCheckbox = count($_checkboxAry);
						$_optionHtml = '';
						
						$userAnswerArr = array();
						if(isset($answerAssoAry[$curUserId]) && isset($answerAssoAry[$curUserId][$_itemId])) {
							$userAnswerArr = Get_Array_By_Key($answerAssoAry[$curUserId][$_itemId],'Content');
							for($n=0;$n<count($userAnswerArr);$n++) {
								$userAnswerArr[$n] = intranet_htmlspecialchars($userAnswerArr[$n]);
							}
						}
						
						for($k=0;$k<$_numOfCheckbox;$k++) {
							$_val = intranet_htmlspecialchars($_checkboxAry[$k]['Content']);
							if($showSelfAnswerOnly){
								if(in_array($_val,$userAnswerArr)){
									$_optionHtml .= $_val.'<br />';
								}
							}else{
								$_optionHtml .= $this->linterface->Get_Checkbox($__elementId."_".$k, $__elementId.'[]', $_val, in_array($_val,$userAnswerArr), $class='replySlipAnswer_'.$replySlipId.'_'.$_itemId, '', $Onclick='', $disableOption);
								$_optionHtml .= '<span><label for="'.$__elementId."_".$k.'">'.$_val.'</label></span>';
								$_optionHtml .= '<br />';
							}
						}
						if($withWarningDiv) {
							$_optionHtml .= $this->linterface->Get_Thickbox_Warning_Msg_Div('tableReplySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_itemId, $Lang['TableReplySlip']['WarningArr']['mustAnswerQuestion'], 'tableReplySlipWarningDiv_'.$replySlipId);
						}
						$cellObj = new tableTd($_optionHtml);
					}
					
					
					### Add tr and td to the table
					if ($isHeaderTr) {
						$tableObj->addHeaderCell($cellObj);
					}
					else {
						$tableObj->addBodyCell($cellObj);
					}
				}
			}
			
			
			$x = '';
			$_optionHtml = $tableObj->returnHtml();
			$questionHtml .= '<div class="reply_slip_content" style="text-align:left;">'."\r\n";
				$questionHtml .= $_optionHtml."\r\n";
			$questionHtml .= '</div>'."\r\n";
			
			$x .= '<div>';
				$x .= $questionHtml;
				$x .= $this->linterface->GET_HIDDEN_INPUT('tableReplySlipItemIdList_'.$replySlipId, 'tableReplySlipItemIdList_'.$replySlipId, implode($tableReplySlipConfig['itemIdHiddenFieldSeparator'], $itemIdAry));
				$x .= $this->linterface->GET_HIDDEN_INPUT('tableReplySlipUserId_'.$replySlipId, 'tableReplySlipUserId_'.$replySlipId, $this->getCurUserId());
				$x .= $this->linterface->GET_HIDDEN_INPUT('tableReplySlipNumOfHeaderRow_'.$replySlipId, 'tableReplySlipNumOfHeaderRow_'.$replySlipId, $numOfHeaderRow);
				$x .= $this->linterface->GET_HIDDEN_INPUT('tableReplySlipNumOfFixedCol_'.$replySlipId, 'tableReplySlipNumOfFixedCol_'.$replySlipId, $numOfFixedCol);
				
			$x .= '</div>';
			
			return $x;
		}
		
		private function getImportHeaderAry() {
			global $Lang;
			
			$headerAry = array();
			$headerAry['En'][] = $Lang['TableReplySlip']['ItemType']['En'];
			$headerAry['En'][] = $Lang['TableReplySlip']['ItemContent']['En'];
			$headerAry['Ch'][] = $Lang['TableReplySlip']['ItemType']['Ch'];
			$headerAry['Ch'][] = $Lang['TableReplySlip']['ItemContent']['Ch'];
			
			return $headerAry;
		}
		
		private function getImportOptionAryFromRowData($rowDataAry) {
			$_optionAry = array();
			$_optionCount = 0;
			$_numOfOption = count($rowDataAry) - 2;
			for ($j=2; $j<($_numOfOption+2); $j+=2) {
				$__optionType = trim($rowDataAry[$j]);
				$__optionContent = trim($rowDataAry[$j+1]);
				
				if ($__optionContent != '') {
					$_optionAry[] = $__optionContent;
					$_optionCount++;
				}
			}
			
			return $_optionAry;
		}
		
		// @param bool checkTableOnly : only count max number of columns for table content, skip count answer options
		private function getMaxNumOfColumnInCsvData($checkTableOnly=true) {
			global $tableReplySlipConfig;
			
			$csvDataAry = $this->getCsvDataAry();
			array_shift($csvDataAry);
			array_shift($csvDataAry);
			
			$numOfData = count($csvDataAry);
			if ($numOfData == 0) {
				return 0;
			}
			else {
				$maxValue = 0;
				for ($i=0; $i<$numOfData; $i++) {
					$__type = strtoupper($csvDataAry[$i][0]);
					if($checkTableOnly && in_array($__type,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])
						&& !in_array($__type,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType'])) {
						continue;
					}
					
					if($checkTableOnly) {
						if(!in_array($__type,$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType'])) {
							$__numOfCol = $this->countNonEmptyColumn($csvDataAry[$i]);
							//debug_r($__type);
							//debug_r($__numOfCol);
						}
					}else {
						$__numOfCol = count($csvDataAry[$i]);
					}
					if($__numOfCol > $maxValue) {
						$maxValue = $__numOfCol;
					}
				}
				return $maxValue;
			}
		}
		
		private function countNonEmptyColumn($rowDataAry) {
			$numOfColumn = count($rowDataAry);
			$numOfNonEmptyColumn = 0;
			for($i=0;$i<$numOfColumn;$i+=2) {
				if(trim($rowDataAry[$i]) != '' /* && trim($rowDataAry[$i+1]) != '' */) { 
					$numOfNonEmptyColumn += 2;
				}
			}
			return $numOfNonEmptyColumn;
		}
		
		private function getOptionRowInvalidColumns($rowDataAry) {
			global $tableReplySlipConfig;
			$numOfColumn = count($rowDataAry);
			
			$firstType = strtoupper($rowDataAry[0]);
			$firstContent = $rowDataAry[1];
			$invalidColumns = array();
			for($i=0;$i<$numOfColumn;$i+=2) {
				if($rowDataAry[$i]!='' && $rowDataAry[$i] != $firstType) {
					$invalidColumns[] = $i+1;
				}
			}
			
			return $invalidColumns;
		}
		
		public function saveUserAnswer($replySlipId, $replySlipUserId, $itemIdAry, $answerAry) {
			global $tableReplySlipConfig;
			
			$successAry = array();
			$this->libdb->Start_Trans();
			
			if ($replySlipUserId == '') {
				$replySlipUserId = $_SESSION['UserID'];
			}
			
			$numOfItem = count($itemIdAry);
			if($numOfItem > 0) {
				$sql = "DELETE FROM INTRANET_TABLE_REPLY_SLIP_USER_ANSWER WHERE UserID='$replySlipUserId' AND ReplySlipID='$replySlipId' AND ItemID IN (".implode(",",$itemIdAry).")";
				$successAry['DeleteOldAnswers'] = $this->libdb->db_db_query($sql);
			}
			
			$values = array();
			
			for($i=0;$i<$numOfItem;$i++) {
				$var_key = 'tableReplySlipItem_'.$replySlipId.'_'.$itemIdAry[$i];
				if(isset($answerAry[$var_key])) {
					if(is_array($answerAry[$var_key]) && count($answerAry[$var_key])>0) {
						// multiple selection or multiple checkboxes
						$answer_ary = $answerAry[$var_key];
						$num_of_answer = count($answer_ary);
						for($j=0;$j<$num_of_answer;$j++) {
							$__ans = $this->libdb->Get_Safe_Sql_Query($answer_ary[$j]);
							$values[] = "('$replySlipUserId','$replySlipId','".$itemIdAry[$i]."','".$__ans."',NOW(),'$replySlipUserId',NOW(),'$replySlipUserId')";
						}
					}else {
						// single value answer - text | textarea | single selection | radio checkbox
						$__ans = $this->libdb->Get_Safe_Sql_Query($answerAry[$var_key]);
						$values[] = "('$replySlipUserId','$replySlipId','".$itemIdAry[$i]."','".$__ans."',NOW(),'$replySlipUserId',NOW(),'$replySlipUserId')";
					}
				}
			}
			
			if(count($values) > 0){
				$value_chunks = array_chunk($values,1000);
				$numOfChunk = count($value_chunks);
				for($i=0;$i<$numOfChunk;$i++) {
					$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP_USER_ANSWER 
							(UserID,ReplySlipID,ItemID,Content,InputDate,InputBy,ModifiedDate,ModifiedBy) 
							VALUES ".implode(",",$value_chunks[$i]);
					$successAry['InsertAnswerChunk'.$i] = $this->libdb->db_db_query($sql);
				}
			}
			
			if (in_array(false, $successAry)) {
				$this->libdb->RollBack_Trans();
				return false;
			}
			else {
				$this->libdb->Commit_Trans();
				return true;
			}
		}
		
		public function returnUserAnswer($replySlipId, $replySlipUserIdAry=array(),$userIdItemIdAssoc=false) {
			global $tableReplySlipConfig;
			
			if(is_array($replySlipUserIdAry) && count($replySlipUserIdAry)>0) {
				$replySlipUserIdCond = " AND a.UserID IN (".implode(",",$replySlipUserIdAry).")";
			}
			$userNameField = getNameFieldByLang('u.');
			
			$sql = "SELECT 
						a.UserAnswerID,a.UserID,a.ReplySlipID,a.ItemID,a.Content,$userNameField as answerUserName 
					FROM 
						INTRANET_TABLE_REPLY_SLIP_USER_ANSWER as a 
						INNER JOIN INTRANET_USER as u ON a.UserID=u.UserID
					WHERE a.ReplySlipID='$replySlipId' $replySlipUserIdCond
					ORDER BY answerUserName";
			$records = $this->libdb->returnResultSet($sql);
			
			if($userIdItemIdAssoc) {
				$assocRecords = array();
				$numOfRecord = count($records);
				for($i=0;$i<$numOfRecord;$i++) {
					$user_id = $records[$i]['UserID'];
					$item_id = $records[$i]['ItemID'];
					if(!isset($assocRecords[$user_id])){
						$assocRecords[$user_id] = array();
					}
					if(!isset($assocRecords[$user_id][$item_id])) {
						$assocRecords[$user_id][$item_id] = array();
					}
					$assocRecords[$user_id][$item_id][] = $records[$i];
				}
				return $assocRecords;
			}
			return $records;
		}
		
		public function returnReplySlipStatisticsHtml($showDiv=false) {
			global $tableReplySlipConfig, $Lang;
			
			$replySlipId = $this->getReplySlipId();
			
			// get reply slip data
			$replySlipObj = new libTableReplySlip($replySlipId);
			$showUserInfoInResult = ($replySlipObj->getShowUserInfoInResult()==$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['show'])? true : false;
			
			$itemAry = $replySlipObj->returnItemData(true);
			$optionAry = $replySlipObj->returnOptionData(true);
			$answerAry = $this->returnUserAnswer($replySlipId);
			
			// count answers stat
			$numOfAnswer = count($answerAry);
			$itemIdToAnswer = array();
			
			for($i=0;$i<$numOfAnswer;$i++) {
				$item_id = $answerAry[$i]['ItemID'];
				$answer_content = intranet_htmlspecialchars($answerAry[$i]['Content']);
				$user_name = $answerAry[$i]['answerUserName'];
				if(!isset($itemIdToAnswer[$item_id])) {
					$itemIdToAnswer[$item_id] = array();
				}
				if($answer_content == '') {
					continue;
				}
				if(!isset($itemIdToAnswer[$item_id][$answer_content])) {
					$itemIdToAnswer[$item_id][$answer_content] = array();
				}
				$itemIdToAnswer[$item_id][$answer_content][] = $user_name;
			}
			
			$numOfRow = count($itemAry);
			$numOfColumn = count($itemAry[0]);
			
			if ($showDiv) {
				$divStyle = 'style="display: block;"';
			}
			$userNameStyle = ' style="background-color:#F9F8E7;"';
			
			### Create table
			$tableObj = new table_commonTable();
			$tableObj->setTableType('view');
			$tableObj->setTableExtraClass('superTable');
			$tableObj->setTableId('replySlipStatTbl_'.$this->getReplySlipId());
			$tableObj->setApplyConvertSpecialChars(false);
			
			$numOfHeaderRow = $this->countNumOfHeaderRow($itemAry);
			$numOfFixedCol = $this->countNumOfFixedColumn($itemAry);
			$questionHtml = '';
			for ($i=0; $i<$numOfRow; $i++) {
				if ($itemAry[$i][0]['RecordType'] == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText']) {
					$isHeaderTr = true;
				}
				else {
					$isHeaderTr = false;
				}
				
				if ($isHeaderTr) {
					$tableObj->addHeaderTr(new tableTr());
				}
				else {
					$tableObj->addBodyTr(new tableTr());
				}

				
				for($j=0; $j < $numOfColumn; $j++) {
				
					$_itemId = $itemAry[$i][$j]['ItemID'];
					$_recordType = $itemAry[$i][$j]['RecordType'];
					
					$_content = $itemAry[$i][$j]['Content'];
					
					if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText'])
					{
						$_content = nl2br(strip_tags($itemAry[$i][$j]['Content'], '<br>'));
						$cellObj = new tableTh($_content);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText'])
					{
						$_content = nl2br(strip_tags($itemAry[$i][$j]['Content'], '<br>'));
						$cellObj = new tableTd($_content);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['shortText'])					
					{
						$_optionHtml = '<ul>'."\n";
						if(isset($itemIdToAnswer[$_itemId]) && count($itemIdToAnswer[$_itemId]) > 0) {
							foreach($itemIdToAnswer[$_itemId] as $answer_text => $user_ary) {
								$num_of_user = count($user_ary);
								$_optionHtml .= '<li>'."\r\n";
								$_optionHtml .= '<em class="reply_stat_result">'.$num_of_user.'</em>';
								$_optionHtml .= '<span> '.$Lang['DocRouting']['Selected'].' '.nl2br($answer_text).'</span>';
								if($showUserInfoInResult && $num_of_user>0) {
									$_optionHtml .= '<span'.$userNameStyle.'>'.implode(", ",$user_ary).'</span>';
								}
								$_optionHtml .= '</li>'."\r\n";
							}
						}
						$_optionHtml .= '</ul>'."\n";
						
						$cellObj = new tableTd($_optionHtml);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['longText']) {
						$_optionHtml = '<ul>'."\n";
						if(isset($itemIdToAnswer[$_itemId]) && count($itemIdToAnswer[$_itemId]) > 0) {
							foreach($itemIdToAnswer[$_itemId] as $answer_text => $user_ary) {
								$num_of_user = count($user_ary);
								$_optionHtml .= '<li>'."\r\n";
								$_optionHtml .= '<em class="reply_stat_result">'.$num_of_user.'</em>';
								$_optionHtml .= '<span> '.$Lang['DocRouting']['Selected'].' '.nl2br($answer_text).'</span>';
								if($showUserInfoInResult && $num_of_user>0) {
									$_optionHtml .= '<span'.$userNameStyle.'>'.implode(", ",$user_ary).'</span>';
								}
								$_optionHtml .= '</li>'."\r\n";
							}
						}
						$_optionHtml .= '</ul>'."\n";
						
						$cellObj = new tableTd($_optionHtml);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelection'])
					{
						$_optionDataAry = $this->getDataArrayForSelection($optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleSelectionOption']][$_content],'Content','Content');
						$num_of_option = count($_optionDataAry);
						$_optionHtml = '<ul>'."\n";
						if($num_of_option > 0) {
							for($k=0;$k<$num_of_option;$k++) {
								$num_of_answer = 0;
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]]);
								}
								$_optionHtml .= '<li>'."\r\n";
								$_optionHtml .= '<em class="reply_stat_result">'.$num_of_answer.'</em>';
								$_optionHtml .= '<span> '.$Lang['DocRouting']['Selected'].' '.$_optionDataAry[$k][1].'</span>';
								if($showUserInfoInResult && $num_of_answer > 0) {
									$_optionHtml .= '<span'.$userNameStyle.'>'.implode(", ",$itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]]).'</span>';
								}
								$_optionHtml .= '</li>'."\r\n";
							}
						}
						$_optionHtml .= '</ul>'."\n";
						
						$cellObj = new tableTd($_optionHtml);
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelection'])
					{
						$_optionDataAry = $this->getDataArrayForSelection($optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleSelectionOption']][$_content],'Content','Content');
						$num_of_option = count($_optionDataAry);
						$_optionHtml = '<ul>'."\n";
						if($num_of_option > 0) {
							for($k=0;$k<$num_of_option;$k++) {
								$num_of_answer = 0;
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]]);
								}
								$_optionHtml .= '<li>'."\r\n";
								$_optionHtml .= '<em class="reply_stat_result">'.$num_of_answer.'</em>';
								$_optionHtml .= '<span> '.$Lang['DocRouting']['Selected'].' '.$_optionDataAry[$k][1].'</span>';
								if($showUserInfoInResult && $num_of_answer > 0) {
									$_optionHtml .= '<span'.$userNameStyle.'>'.implode(", ",$itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]]).'</span>';
								}
								$_optionHtml .= '</li>'."\r\n";
							}
						}
						$_optionHtml .= '</ul>'."\n";
						
						$cellObj = new tableTd($_optionHtml);
					}else if ($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckbox']) {
						$_radioAry = $optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleCheckboxOption']][$_content];
						$_numOfRadio = count($_radioAry);
						$_optionHtml = '<ul>'."\n";
						if($_numOfRadio > 0) {
							for($k=0;$k<$_numOfRadio;$k++) {
								$num_of_answer = 0;
								$radio_text = intranet_htmlspecialchars($_radioAry[$k]['Content']);
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$radio_text])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$radio_text]);
								}
								$_optionHtml .= '<li>'."\r\n";
								$_optionHtml .= '<em class="reply_stat_result">'.$num_of_answer.'</em>';
								$_optionHtml .= '<span> '.$Lang['DocRouting']['Selected'].' '.$radio_text.'</span>';
								if($showUserInfoInResult && $num_of_answer > 0) {
									$_optionHtml .= '<span'.$userNameStyle.'>'.implode(", ",$itemIdToAnswer[$_itemId][$radio_text]).'</span>';
								}
								$_optionHtml .= '</li>'."\r\n";
							}
						}
						$_optionHtml .= '</ul>'."\n";
						
						$cellObj = new tableTd($_optionHtml);
					}else if ($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckbox']) {
						$_checkboxAry = $optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleCheckboxOption']][$_content];
						$_numOfCheckbox = count($_checkboxAry);
						$_optionHtml = '<ul>'."\n";
						if($_numOfCheckbox > 0) {
							for($k=0;$k<$_numOfCheckbox;$k++) {
								$num_of_answer = 0;
								$checkbox_text = intranet_htmlspecialchars($_checkboxAry[$k]['Content']);
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$checkbox_text])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$checkbox_text]);
								}
								$_optionHtml .= '<li>'."\r\n";
								$_optionHtml .= '<em class="reply_stat_result">'.$num_of_answer.'</em>';
								$_optionHtml .= '<span> '.$Lang['DocRouting']['Selected'].' '.$checkbox_text.'</span>';
								if($showUserInfoInResult && $num_of_answer > 0) {
									$_optionHtml .= '<span'.$userNameStyle.'>'.implode(", ",$itemIdToAnswer[$_itemId][$checkbox_text]).'</span>';
								}
								$_optionHtml .= '</li>'."\r\n";
							}
						}
						$_optionHtml .= '</ul>'."\n";
						
						$cellObj = new tableTd($_optionHtml);
					}
					
					### Add tr and td to the table
					if ($isHeaderTr) {
						$tableObj->addHeaderCell($cellObj);
					}
					else {
						$tableObj->addBodyCell($cellObj);
					}
				}
			}
			
			$tableHtml = $tableObj->returnHtml();
			$questionHtml .= $this->returnGetExportCsvButton($replySlipId).'<br />';
			$questionHtml .= '<div class="reply_slip_stat triangle-border top" '.$divStyle.'>'."\r\n";
				$questionHtml .= $tableHtml."\r\n";
				$questionHtml .= '<br />'."\r\n";
			$questionHtml .= '</div>'."\r\n";
			$questionHtml .= $this->linterface->GET_HIDDEN_INPUT('tableStatReplySlipNumOfHeaderRow_'.$replySlipId, 'tableStatReplySlipNumOfHeaderRow_'.$replySlipId, $numOfHeaderRow);
			$questionHtml .= $this->linterface->GET_HIDDEN_INPUT('tableStatReplySlipNumOfFixedCol_'.$replySlipId, 'tableStatReplySlipNumOfFixedCol_'.$replySlipId, $numOfFixedCol);
			
			return $questionHtml;
		}
		
		public function returnGetSampleCsvLink() {
			global $intranet_root, $PATH_WRT_ROOT, $image_path, $Lang, $LAYOUT_SKIN;
			
			$encryptedPath = getEncryptedText($intranet_root.'/home/common_table_reply_slip/table_reply_slip_sample.csv');
			$attachmentLink = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$encryptedPath;
			
			$link = '<a class="tablelink" href="'.$attachmentLink.'\" target="_blank" title="'.$Lang['General']['ClickHereToDownloadSample'].'"><img src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/icon_attachment.gif" border="0" align="absmiddle" valign="2">['.$Lang['General']['ClickHereToDownloadSample'].']</a>';
			$link .= '&nbsp;<a class="tablelink" href="javascript:newWindow(\''.$PATH_WRT_ROOT.'home/common_table_reply_slip/help.php\',32);">['.$Lang['General']['Help'].']</a>';
			
			return $link;
		}
		
		public function exportReplySlipStatisticsCSV($title, $exportType) {
			global $tableReplySlipConfig, $Lang, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$libexport = new libexporttext();
			$replySlipId = $this->getReplySlipId();
			
			$exportHeaderAry = array();
			$exportContentAry = array();
			
			// get reply slip data
			$replySlipObj = new libTableReplySlip($replySlipId);
			$itemAry = $replySlipObj->returnItemData(true);
			$optionAry = $replySlipObj->returnOptionData(true);
			$answerAry = $this->returnUserAnswer($replySlipId);
			
			// count answers stat
			$numOfAnswer = count($answerAry);
			$itemIdToAnswer = array();
			
			for($i=0;$i<$numOfAnswer;$i++) {
				$item_id = $answerAry[$i]['ItemID'];
				$answer_content = intranet_htmlspecialchars($answerAry[$i]['Content']);
				$user_name = $answerAry[$i]['answerUserName'];
				if(!isset($itemIdToAnswer[$item_id])) {
					$itemIdToAnswer[$item_id] = array();
				}
				if($answer_content == '') {
					continue;
				}
				if(!isset($itemIdToAnswer[$item_id][$answer_content])) {
					$itemIdToAnswer[$item_id][$answer_content] = array();
				}
				$itemIdToAnswer[$item_id][$answer_content][] = $user_name;
			}
			
			if ($exportType == $tableReplySlipConfig['exportTypeAry']['layoutView']) {
				$exportContentAry = $this->returnExportReplySlipLayoutViewData($itemAry, $optionAry, $itemIdToAnswer);
			}
			else if ($exportType == $tableReplySlipConfig['exportTypeAry']['rawData']) {
				$exportContentAry = $this->returnExportReplySlipRawData($itemAry, $answerAry);
			}
			else if ($exportType == $tableReplySlipConfig['exportTypeAry']['freqStat']) {
				$exportContentAry = $this->returnExportReplySlipFrequencyStatisticsData($itemAry, $optionAry, $itemIdToAnswer);
			}
			
			$filename = "reply_slip.csv";
			if($title != ""){
				if(strtolower(substr($title,strrpos($title,"."))) != ".csv") {
					$filename = $title.".csv";
				}else {
					$filename = $title;
				}
			}
			
			$export_content = $libexport->GET_EXPORT_TXT($exportContentAry, $exportHeaderAry,"","\r\n","",0,"11",1);
			$libexport->EXPORT_FILE($filename, $export_content);	
		}
		
		private function returnExportReplySlipLayoutViewData($itemAry, $optionAry, $itemIdToAnswer) {
			global $tableReplySlipConfig;
			
			$rows = array();
			$replySlipId = $this->getReplySlipId();
			$replySlipObj = new libTableReplySlip($replySlipId);
			
			$showUserInfoInResult = ($replySlipObj->getShowUserInfoInResult()==$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['show'])? true : false;
			$numOfRow = count($itemAry);
			$numOfColumn = count($itemAry[0]);
			
			for ($i=0; $i<$numOfRow; $i++) {
				$row = array();
				
				for($j=0; $j < $numOfColumn; $j++) {
				
					$_itemId = $itemAry[$i][$j]['ItemID'];
					$_recordType = $itemAry[$i][$j]['RecordType'];
					
					$_content = $itemAry[$i][$j]['Content'];
					
					if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText'])
					{
						$_content = strip_tags($itemAry[$i][$j]['Content'], '<br>');
						$row[] = $_content;
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText'])
					{
						$_content = strip_tags($itemAry[$i][$j]['Content'], '<br>');
						$row[] = $_content;
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['shortText'])					
					{
						$col = "";
						if(isset($itemIdToAnswer[$_itemId]) && count($itemIdToAnswer[$_itemId]) > 0) {
							$br = "";
							foreach($itemIdToAnswer[$_itemId] as $answer_text => $user_ary) {
								$num_of_user = count($user_ary);
								$col .= $br. "[".$num_of_user."] ";
								$col .= $answer_text." ";
								if($showUserInfoInResult && $num_of_user>0) {
									$col .= "[".implode(", ",$user_ary)."]";
								}
								$br = "\n";
							}
						}
						$row[] = $col;
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['longText']) {
						$col = "";
						if(isset($itemIdToAnswer[$_itemId]) && count($itemIdToAnswer[$_itemId]) > 0) {
							$br = "";
							foreach($itemIdToAnswer[$_itemId] as $answer_text => $user_ary) {
								$num_of_user = count($user_ary);
								$col .= $br. "[".$num_of_user."] ";
								$col .= $answer_text." ";
								if($showUserInfoInResult && $num_of_user>0) {
									$col .= "[".implode(", ",$user_ary)."]";
								}
								$br = "\n";
							}
						}
						$row[] = $col;
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelection'])
					{
						$_optionDataAry = $this->getDataArrayForSelection($optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleSelectionOption']][$_content],'Content','Content');
						$num_of_option = count($_optionDataAry);
						$col = "";
						if($num_of_option > 0) {
							$br = "";
							for($k=0;$k<$num_of_option;$k++) {
								$num_of_answer = 0;
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]]);
								}
								$col .= $br . "[".$num_of_answer."] ";
								$col .= $_optionDataAry[$k][1]." ";
								if($showUserInfoInResult && $num_of_answer > 0) {
									$col .= "[".implode(", ",$itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]])."]";
								}
								$br = "\n";
							}
						}
						$row[] = $col;
					}else if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelection'])
					{
						$_optionDataAry = $this->getDataArrayForSelection($optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleSelectionOption']][$_content],'Content','Content');
						$num_of_option = count($_optionDataAry);
						$col = "";
						if($num_of_option > 0) {
							$br = "";
							for($k=0;$k<$num_of_option;$k++) {
								$num_of_answer = 0;
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]]);
								}
								$col .= $br . "[".$num_of_answer."] ";
								$col .= $_optionDataAry[$k][1]." ";
								if($showUserInfoInResult && $num_of_answer > 0) {
									$col .= "[".implode(", ",$itemIdToAnswer[$_itemId][$_optionDataAry[$k][1]])."]";
								}
								$br = "\n";
							}
						}
						$row[] = $col;
					}else if ($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckbox']) {
						$_radioAry = $optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['singleCheckboxOption']][$_content];
						$_numOfRadio = count($_radioAry);
						$col = "";
						if($_numOfRadio > 0) {
							$br = "";
							for($k=0;$k<$_numOfRadio;$k++) {
								$num_of_answer = 0;
								$radio_text = intranet_htmlspecialchars($_radioAry[$k]['Content']);
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$radio_text])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$radio_text]);
								}
								$col .= $br . "[".$num_of_answer."] ";
								$col .= $radio_text." ";
								if($showUserInfoInResult && $num_of_answer > 0) {
									$col .= "[".implode(", ",$itemIdToAnswer[$_itemId][$radio_text])."]";
								}
								$br = "\n";
							}
						}
						$row[] = $col;
					}else if ($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckbox']) {
						$_checkboxAry = $optionAry[$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_OPTION']['RecordType']['multipleCheckboxOption']][$_content];
						$_numOfCheckbox = count($_checkboxAry);
						$col = "";
						if($_numOfCheckbox > 0) {
							$br = "";
							for($k=0;$k<$_numOfCheckbox;$k++) {
								$num_of_answer = 0;
								$checkbox_text = intranet_htmlspecialchars($_checkboxAry[$k]['Content']);
								if(isset($itemIdToAnswer[$_itemId]) && isset($itemIdToAnswer[$_itemId][$checkbox_text])){
									$num_of_answer = count($itemIdToAnswer[$_itemId][$checkbox_text]);
								}
								$col .= $br . "[".$num_of_answer."] ";
								$col .= $checkbox_text." ";
								if($showUserInfoInResult && $num_of_answer > 0) {
									$col .= "[".implode(", ",$itemIdToAnswer[$_itemId][$checkbox_text])."]";
								}
								$br = "\n";
							}
						}
						$row[] = $col;
					}
				}
				$rows[] = $row;
			}
			
			return $rows;
		}
		
		private function returnExportReplySlipRawData($itemAry, $answerAry) {
			global $tableReplySlipConfig, $Lang;
			
			$rows = array();
			$replySlipId = $this->getReplySlipId();
			$replySlipObj = new libTableReplySlip($replySlipId);
			
			$answerAssoAry = BuildMultiKeyAssoc($answerAry, 'ItemID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$numOfHeaderRow = $this->countNumOfHeaderRow($itemAry);
			$numOfFixedCol = $this->countNumOfFixedColumn($itemAry);
			
			$numOfRow = count($itemAry);
			$numOfColumn = count($itemAry[0]);
			
			
			// construct the export csv
			$exportAry = array();
			$iCounter = 0;
			for ($i=0; $i<$numOfRow; $i++) {
				if ($i < $numOfHeaderRow) {
					// construct header rows
					for ($j=0; $j<$numOfFixedCol; $j++) {
						$__recordType = $itemAry[$i][$j]['RecordType'];
						$__content = $itemAry[$i][$j]['Content'];
						
						if ($__recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText']) {
							$exportAry[$iCounter][] = $__content;
						}
						else if ($__recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText']) {
							$exportAry[$iCounter][] = $__content;
						}
					}
					$exportAry[$iCounter][] = $Lang['TableReplySlip']['Item'];
					$exportAry[$iCounter][] = $Lang['TableReplySlip']['User'];
					$exportAry[$iCounter][] = $Lang['TableReplySlip']['Answer'];
					$iCounter++;
				}
				else {
					// construct content rows
					for($j=0; $j<$numOfColumn; $j++) {
						$__itemId = $itemAry[$i][$j]['ItemID'];
						$__recordType = $itemAry[$i][$j]['RecordType'];
						
						// get header field of the content (e.g. Industriousness, Maturity, Analytical power, etc...)
						$__headerField = $itemAry[0][$j]['Content'];
						
						// loop each answer of each item
						$__itemAnswerAry = $answerAssoAry[$__itemId];
						$__numOfItemAnswer = count($__itemAnswerAry);
						for ($k=0; $k<$__numOfItemAnswer; $k++) {
							$___answer = $__itemAnswerAry[$k]['Content'];
							$___answerUserName = $__itemAnswerAry[$k]['answerUserName'];
							
							// get the fixed column info (e.g. Class, Class No., Student Name, etc...)
							for ($l=0; $l<$numOfFixedCol; $l++) {
								$____content = $itemAry[$i][$l]['Content'];
								$exportAry[$iCounter][] = $____content; 
							}
							
							// export header field (e.g. Industriousness, Maturity, Analytical power, etc...)
							$exportAry[$iCounter][] = $__headerField;
							
							// export user names
							$exportAry[$iCounter][] = $___answerUserName;
							
							// export user input / selected option field (e.g. Excellent, Good, Average, etc...)
							$exportAry[$iCounter][] = $___answer;
							
							$iCounter++;
						}
					}
				}
			}
			
			return $exportAry;
		} 
		
		private function returnExportReplySlipFrequencyStatisticsData($itemAry, $optionAry, $itemIdToAnswer) {
			global $tableReplySlipConfig, $Lang;
			
			$rows = array();
			$replySlipId = $this->getReplySlipId();
			$replySlipObj = new libTableReplySlip($replySlipId);
			$showUserInfoInResult = ($replySlipObj->getShowUserInfoInResult()==$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['show'])? true : false;
			
			$numOfHeaderRow = $this->countNumOfHeaderRow($itemAry);
			$numOfFixedCol = $this->countNumOfFixedColumn($itemAry);
			
			$numOfRow = count($itemAry);
			$numOfColumn = count($itemAry[0]);
			
			// construct the export csv
			$exportAry = array();
			$iCounter = 0;
			for ($i=0; $i<$numOfRow; $i++) {
				if ($i < $numOfHeaderRow) {
					// construct header rows
					for ($j=0; $j<$numOfFixedCol; $j++) {
						$__recordType = $itemAry[$i][$j]['RecordType'];
						$__content = $itemAry[$i][$j]['Content'];
						
						if ($__recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText']) {
							$exportAry[$iCounter][] = $__content;
						}
						else if ($__recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText']) {
							$exportAry[$iCounter][] = $__content;
						}
					}
					$exportAry[$iCounter][] = $Lang['TableReplySlip']['Item'];
					$exportAry[$iCounter][] = $Lang['TableReplySlip']['Answer'];
					$exportAry[$iCounter][] = $Lang['TableReplySlip']['Frequency'];
					$iCounter++;
				}
				else {
					// construct content rows
					for($j=0; $j<$numOfColumn; $j++) {
						$__itemId = $itemAry[$i][$j]['ItemID'];
						$__recordType = $itemAry[$i][$j]['RecordType'];
						$__content = $itemAry[$i][$j]['Content'];
						
						// get header field of the content (e.g. Industriousness, Maturity, Analytical power, etc...)
						$__headerField = $itemAry[0][$j]['Content'];
						
						switch ($__recordType) {
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText']:
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText']:
								// no need to add new row for display text and header text item 
								continue;
							break;
							
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['shortText']:
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['longText']:
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelection']:
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelection']:
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckbox']:
							case $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckbox']:
								if ($__recordType==$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['shortText'] || $__recordType==$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['longText']) {
									// get all input answers
									$__optionAry = array_keys((array)$itemIdToAnswer[$__itemId]);
								}
								else {
									// get all options 
									$__optionCode = $__recordType.'O';
									$__optionAry = Get_Array_By_Key($optionAry[$__optionCode][$__content], 'Content');
								}
								
								// new row for each items
								$__numOfOption = count($__optionAry);
								for ($k=0; $k<$__numOfOption; $k++) {
									$___optionContent = $__optionAry[$k];
									$___optionAnsweredUserAry = $itemIdToAnswer[$__itemId][$___optionContent];
									$___numOfOptionAnsweredUser = count((array)$___optionAnsweredUserAry);
									
									// get the fixed column info (e.g. Class, Class No., Student Name, etc...)
									for ($l=0; $l<$numOfFixedCol; $l++) {
										$____content = $itemAry[$i][$l]['Content'];
										$exportAry[$iCounter][] = $____content; 
									}
									
									// export header field (e.g. Industriousness, Maturity, Analytical power, etc...)
									$exportAry[$iCounter][] = $__headerField;
									
									// export option field (e.g. Excellent, Good, Average, etc...)
									$exportAry[$iCounter][] = $___optionContent;
									
									// export number of users input or choose this option
									$exportAry[$iCounter][] = $___numOfOptionAnsweredUser;
									
									// export user names who input or selected this option
									if ($showUserInfoInResult) {
										for ($l=0; $l<$___numOfOptionAnsweredUser; $l++) {
											$____userName = $___optionAnsweredUserAry[$l];
											$exportAry[$iCounter][] = $____userName;
										}
									}
									
									$iCounter++;
								}
							break;
						}
					}
				}
			}
			
			return $exportAry;
		}
		
		public function returnGetExportCsvButton($replySlipId, $title="") {
			global $intranet_root, $PATH_WRT_ROOT, $image_path, $Lang, $LAYOUT_SKIN, $tableReplySlipConfig;
			
//			$link = '/home/common_table_reply_slip/export_reply_slip_csv.php?ReplySlipID='.$replySlipId.'&Title='.rawurlencode($title);
//			return $this->linterface->GET_LNK_EXPORT_IP25($link);
			
			$replySlipObj = new libTableReplySlip($replySlipId);
			$showUserInfoInResult = ($replySlipObj->getShowUserInfoInResult()==$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['ShowUserInfoInResult']['show'])? true : false;
			
			$link = '/home/common_table_reply_slip/export_reply_slip_csv.php?ReplySlipID='.$replySlipId.'&Title='.rawurlencode($title);
			
			
			// Export
			$ExportOptionArr = array();
			$ExportOptionArr[] = array($link.'&exportType='.$tableReplySlipConfig['exportTypeAry']['layoutView'], $Lang['TableReplySlip']['ExportTypeArr']['LayoutView']);
			if ($showUserInfoInResult) {
				$ExportOptionArr[] = array($link.'&exportType='.$tableReplySlipConfig['exportTypeAry']['rawData'], $Lang['TableReplySlip']['ExportTypeArr']['RawData']);
			}
			$ExportOptionArr[] = array($link.'&exportType='.$tableReplySlipConfig['exportTypeAry']['freqStat'], $Lang['TableReplySlip']['ExportTypeArr']['FrequencyStatistics']);
			
			$x = '';
			$x .= '<div class="Conntent_tool">'."\r\n";
				$x .= $this->linterface->Get_Content_Tool_v30('export', $href="javascript:void(0);", $text="", $ExportOptionArr, $other="", $divID='ExportDiv'); 
			$x .= '</div>'."\r\n";
			
			return $x;
		}
		
		function getHelpPage() {
			global $intranet_root, $PATH_WRT_ROOT, $image_path, $Lang, $LAYOUT_SKIN;
			
			$x = '';
			$x.= '<div style="text-align:center;font-weight:bold;"><h3>'.$Lang['TableReplySlip']['TableReplySlipUsageGuide'].'</h3></div>';
			$x.= '<div style="text-align:left"><h4>'.$Lang['TableReplySlip']['DataTypes'].':</h4></div>';
			$x.= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
			$x.= '<tbody>';
			// table header
				$x.= '<tr>';
					$x .= '<th style="width:5%">'.$Lang['TableReplySlip']['Abbreviation'].'</th>';
					$x .= '<th style="width:10%">'.$Lang['TableReplySlip']['Meaning'].'</th>';
					$x .= '<th style="width:30%">'.$Lang['TableReplySlip']['Example'].'</th>';
					$x .= '<th style="width:25%">'.$Lang['TableReplySlip']['Preview'].'</th>';
					$x .= '<th style="width:30%">'.$Lang['TableReplySlip']['Remark'].'</th>';
				$x.= '</tr>'."\n";
				
				$dataAry = array();
				for($i=1;$i<=5;$i++) {
					$dataAry[] = array('Option '.$i,'Option '.$i);
				}
				
				$_optionHtmlSC = '';
				for($k=1;$k<=3;$k++) {
					$_optionHtmlSC .= $this->linterface->Get_Radio_Button("SC_".$k, "SC", '', $k==1, '', '', $Onclick="", '').'</em>';
					$_optionHtmlSC .= '<span><label for="'."SC_".$k.'">'.'Checkbox '.$k.'</label></span>';
					$_optionHtmlSC .= '<br />';
				}
				
				$_optionHtmlMC = '';
				for($k=1;$k<=3;$k++) {
					$_optionHtmlMC .= $this->linterface->Get_Checkbox("MC_".$k, 'MC[]', '', $k==1, '', '', $Onclick='', '');
					$_optionHtmlMC .= '<span><label for="'."MC_".$k.'">'.'Checkbox '.$k.'</label></span>';
					$_optionHtmlMC .= '<br />';
				}
				
				// SSO - Single Selection Options
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">SSO</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_SSO</td>';
						for($k=1;$k<=5;$k++) {
							$ex .= '<td style="border:solid 1px grey;">SSO</td>';
							$ex .= '<td style="border:solid 1px grey;">Option '.$k.'</td>';
						}
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">SSO</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['SingleSelectionOption'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$this->linterface->GET_SELECTION_BOX($dataAry, '', '', '').'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][1].'</td>';
				$x .= '</tr>'."\n";
				
				// MSO - Multiple Selection Options
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">MSO</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_MSO</td>';
						for($k=1;$k<=5;$k++) {
							$ex .= '<td style="border:solid 1px grey;">MSO</td>';
							$ex .= '<td style="border:solid 1px grey;">Option '.$k.'</td>';
						}
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">MSO</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['MultipleSelectionOption'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$this->linterface->GET_SELECTION_BOX($dataAry, 'multiple="multiple"', '', '').'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][1].'</td>';
				$x .= '</tr>'."\n";
				
				// SCO - Single Checkbox Options
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">SCO</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_SCO</td>';
						for($k=1;$k<=3;$k++) {
							$ex .= '<td style="border:solid 1px grey;">SCO</td>';
							$ex .= '<td style="border:solid 1px grey;">Checkbox '.$k.'</td>';
						}
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">SCO</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['SingleCheckboxOption'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$_optionHtmlSC.'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][1].'</td>';
				$x .= '</tr>'."\n";
				
				// MCO - Multiple Checkbox Options
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">MCO</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_MCO</td>';
						for($k=1;$k<=3;$k++) {
							$ex .= '<td style="border:solid 1px grey;">MCO</td>';
							$ex .= '<td style="border:solid 1px grey;">Checkbox '.$k.'</td>';
						}
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">MCO</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['MultipleCheckboxOption'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$_optionHtmlMC.'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][1].'</td>';
				$x .= '</tr>'."\n";
				
				// HT - Header Text
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">HT</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['HeaderText'].'</td>';
					$ex = '<table>';
						$ex .= '<tr>';
							$ex .= '<td style="border:solid 1px grey;">HT</td>';
							$ex .= '<td style="border:solid 1px grey;">Header Text</td>';
						$ex .= '</tr>';
					$ex.= '</table>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>';
						$x .= '<table class="common_table_list_v30 view_table_list_v30">';
							$x .= '<tbody>';
								$x .= '<tr><th>Header Text</th></tr>';
							$x .= '</tbody>';
						$x .= '</table>';
					$x .= '</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][2].'</td>';
				$x .= '</tr>'."\n";
				
				
				// DT - Display Text
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">DT</td>';
						$ex .= '<td style="border:solid 1px grey;">Simple Text</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">DT</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['DisplayText'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>Simple Text</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][3].'.</td>';
				$x .= '</tr>'."\n";
				
				// ST - Short Text
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">ST</td>';
						$ex .= '<td style="border:solid 1px grey;">&nbsp;&nbsp;</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">ST</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['ShortText'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$this->linterface->GET_TEXTBOX('', '', '', '', array()).'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][4].'</td>';
				$x .= '</tr>'."\n";
				
				// LT - Long Text
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">LT</td>';
						$ex .= '<td style="border:solid 1px grey;">&nbsp;&nbsp;</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">LT</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['LongText'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$this->linterface->GET_TEXTAREA('', '', $taCols=70, $taRows=5, $OnFocus="", 0, $other='', '', $CommentMaxLength='').'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][4].'</td>';
				$x .= '</tr>'."\n";
				
				// SS - Single Selection 
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">SS</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_SSO</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">SS</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['SingleSelection'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$this->linterface->GET_SELECTION_BOX($dataAry, '', '', '').'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][5].'</td>';
				$x .= '</tr>'."\n";
				
				// MS - Multiple Selection
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">MS</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_MSO</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">MS</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['MultipleSelection'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$this->linterface->GET_SELECTION_BOX($dataAry, 'multiple="multiple"', '', '').'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][5].'</td>';
				$x .= '</tr>'."\n";
				
				// SC - Single Checkboxes
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">SC</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_SCO</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">SC</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['SingleCheckbox'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$_optionHtmlSC.'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][5].'</td>';
				$x .= '</tr>'."\n";
				
				// MC - Multiple Checkboxes
				$ex = '<table>';
					$ex .= '<tr>';
						$ex .= '<td style="border:solid 1px grey;">MC</td>';
						$ex .= '<td style="border:solid 1px grey;">ID_MCO</td>';
					$ex .= '</tr>';
				$ex.= '</table>';
				$x .= '<tr>';
					$x .= '<td style="font-weight:bold">MC</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['MultipleCheckbox'].'</td>';
					$x .= '<td>'.$ex.'</td>';
					$x .= '<td>'.$_optionHtmlMC.'</td>';
					$x .= '<td>'.$Lang['TableReplySlip']['RemarkArr'][0].$Lang['TableReplySlip']['RemarkArr'][5].'</td>';
				$x .= '</tr>'."\n";
				
			$x.= '</tbody>';
			$x.= '</table>'."\n";
			
			$x .= '<br />'."\n";
			$x .= '<div style="text-align:left">'."\n";
			$x .= '<h4>'.$Lang['TableReplySlip']['UsageExamples'].':</h4>'."\n";
			//$x .= '<br />'."\n";
			
			$sample1_encryptedPath = getEncryptedText($intranet_root.'/home/common_table_reply_slip/conduct_mark.csv');
			$sample1_attachmentLink = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$sample1_encryptedPath;
			$sample1_link = '<a class="tablelink" href="'.$sample1_attachmentLink.'\" target="_blank" title="'.$Lang['General']['ClickHereToDownloadSample'].'"><img src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/icon_attachment.gif" border="0" align="absmiddle" valign="2">['.'Conduct Mark Sample'.']</a>';
			$sample1_image = '<img src="'.$PATH_WRT_ROOT.'home/common_table_reply_slip/conduct_mark.png" />';
			
			$sample2_encryptedPath = getEncryptedText($intranet_root.'/home/common_table_reply_slip/reference_report.csv');
			$sample2_attachmentLink = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$sample2_encryptedPath;
			$sample2_link = '<a class="tablelink" href="'.$sample2_attachmentLink.'\" target="_blank" title="'.$Lang['General']['ClickHereToDownloadSample'].'"><img src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/icon_attachment.gif" border="0" align="absmiddle" valign="2">['.'JUPAS Application Reference Report Sample'.']</a>';
			$sample2_image = '<img src="'.$PATH_WRT_ROOT.'home/common_table_reply_slip/reference_report.png" />';
			
			$x .= $sample1_link;
			$x .= '<br />'."\n";
			$x .= $sample1_image;
			$x .= '<br />'."\n";
			$x .= '<br />'."\n";
			
			$x .= $sample2_link;
			$x .= '<br />'."\n";
			$x .= $sample2_image;
			$x .= '<br />'."\n";
			
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		function updateShowUserInfoInResult($val)
		{
			$replySlipId = $this->getReplySlipId();
			$replySlipObj = new libTableReplySlip($replySlipId);
			
			return $replySlipObj->updateShowUserInfoInResult($val);
		}
		
		private function countNumOfHeaderRow($itemAry) {
			global $tableReplySlipConfig;
			
			$numOfHeaderRow = 0;
			$numOfRow = count($itemAry);
			for ($i=0; $i<$numOfRow; $i++) {
				if ($itemAry[$i][0]['RecordType'] == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['headerText']) {
					$numOfHeaderRow++;
				}
			}
			
			return $numOfHeaderRow;
		}
		
		private function countNumOfFixedColumn($itemAry) {
			global $tableReplySlipConfig;
			
			$numOfFixedCol = 0;
			$lastRowAry = $itemAry[count((array)$itemAry) - 1];
			$numOfColumn = count($lastRowAry);
			for ($i=0; $i<$numOfColumn; $i++) {
				$_recordType = $lastRowAry[$i]['RecordType'];
					
				if($_recordType == $tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['displayText']) {
					$numOfFixedCol++;
				}
			}
			
			return $numOfFixedCol;
		}
	}
}

?>