<?php
## Using By : Bill

###############################################
#  請用utf-8 save file,  function retrieveConductPotentialsReport() >>> 標楷體
###############################################

###########################################
#
#	Date:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 			modified generateStudentPunishmentNotice(), to delay notice start date & end date   ($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#   Date:   2019-03-12 (Bill)   [2018-0710-1351-39240]
#           added getGeneratedNoticeLog(), insertGeneratedNoticeLog(), removeGeneratedNoticeLog()
#           added generateStudentPunishmentNotice(), getGeneratedNoticeContent(), getDemeritLinkedNoticeContent()
#
#	Date:	2017-02-16	Bill	[2016-0823-1255-04240]
#			modified getStudentConductPunishmentSummaryData(), to return correct upgraded demerit count
#
#	Date:	2017-01-16	Bill	[2016-0825-1456-31225]
#			added getStudentUpgradedLateDemeritRecord()
#
#	Date:	2017-01-11	Bill	[2016-0808-1743-20240]
#			added getStudentAllClassTeacherName(), getStudentDetentionByDate(), getStudentConductPunishmentSummaryData()
#
#	Date:	2016-07-08	Bill
#			modified get_SKSS_AssessmentItemTable(), for PHP 5.4
#			- change split() to explode()
#			- replace $FormLevel[0]['YearID'] by $FormLevel[0] as getClassLevelIDByUserID() using returnVector() to get data (Illegal string offset Warning)
#
#	Date:	2015-11-11	Bill	[2015-0303-1550-45164]
#			added returnMeritStatInMonthlySummary(), to support Monthly Punishment Summary
#
#	Date:	2015-02-26	Bill [2015-0128-1121-06170]
#			modified getDetentionPunishmentStudents() - allow return records of not take attendance
#
#	Date:	2014-12-10	Bill
#			modified getMisconductAndPunishmentNotificationRecords() - ensure return unsend records when $send_status != '1'
#
#	Date:	2014-11-12	Bill
#			$sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord'] - added updateStudentReportTargetLang(), removeStudentReportTargetLang()
#
#	Date:	2014-06-11	Carlos
#			$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added getMisconductAndPunishmentNotificationRecords(), updateEmailNotificationRecord()
#
#	Date:	2014-01-10	YatWoon
#		update returnPunishmentDataByNoticeID()
#
#	Date:	2013-07-19	YatWoon
#		add returnPunishmentDataByNoticeID() for MST customization
#
#	Date:	2013-06-03	Carlos
#			TWGHs Chen Zao Men College 
#			Added getDetentionNoticeStudents() and getDetentionPunishmentStudents()
#
#	Date:	2013-05-15	YatWoon
#			HONG KONG TAOIST ASSOCIATION THE YUEN YUEN INSTITUTE NO.3 SECONDARY SCHOOL (PJ-2013-0205)
#
###########################################

if (!defined("LIBDISCIPLINEv12_CUST_DEFINED"))                     
{

	define("LIBDISCIPLINEv12_CUST_DEFINED",true);
	
	class libdisciplinev12_cust extends libdisciplinev12 
	{
		function libdiscipline_cust ()
		{
			$this->libdb();
		}
		
		####################################################################################### 
		# [START] HONG KONG TAOIST ASSOCIATION THE YUEN YUEN INSTITUTE NO.3 SECONDARY SCHOOL (PJ-2013-0205)
		#	flag related:
		#	- $sys_custom['eDiscipline']['yy3']
		#	requirment information:
		#	- Also count with "non-released" records
		#######################################################################################
		function retrieveStudentConductMarkSummary($StudentID, $StartDate, $EndDate)
		{
			# retrieve all approved AP records
			$sql = "select 
						a.RecordID,
						a.ConductScoreChange,
						b.TagID
					from
						DISCIPLINE_MERIT_RECORD as a
						left join DISCIPLINE_AP_ITEM_TAG as b on b.APItemID=a.ItemID
					where
						a.StudentID = $StudentID
						and a.RecordDate >= '$StartDate' and a.RecordDate <= '$EndDate'
						and a.RecordStatus = ". DISCIPLINE_STATUS_APPROVED ."
					";
			$result = $this->returnArray($sql);
			
			$data_ary = array();
			if(!empty($result))
			{
				$record_ary = array();
				
				# build result array with RecordID as index (maybe 1 AP item related to severl tags)
				foreach($result as $k=>$d)
				{
					$record_ary[$d['RecordID']]['conduct'] = $d['ConductScoreChange'];
					if($d['TagID'])
						$record_ary[$d['RecordID']]['tag'][] = $d['TagID'];
				}

				# build result array
				foreach($record_ary as $k=>$d)
				{
					if($d['conduct'] > 0)	# total +
						$data_ary['A'] += $d['conduct'];
					else					# total -
						$data_ary['S'] += ($d['conduct']);
					
					# Tag related
					for($t=0;$t<sizeof($d['tag']);$t++)
						$data_ary['Tag'][$d['tag'][$t]] += ($d['conduct'] > 0 ? $d['conduct'] : 0);
				}
				
			}
			return $data_ary;
		}
		
		
		####################################################################################### 
		# [END] HONG KONG TAOIST ASSOCIATION THE YUEN YUEN INSTITUTE NO.3 SECONDARY SCHOOL (PJ-2013-0205)
		#######################################################################################
		
		################################################################################################
		# [Start] [TWGHs Chen Zao Men College]
		################################################################################################
		function getDetentionNoticeStudents($DetentionDate, $YearClassIdAry)
		{
			$ClassTitleField = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
			$NameField = getNameFieldByLang("u.");
			$academicYearID = Get_Current_Academic_Year_ID();
			
			$cond = " AND yc.YearClassID IN (".implode(",",(array)$YearClassIdAry).") ";
			
			$sql = "SELECT 
						yc.YearClassID,
						$ClassTitleField as ClassTitle,
						ycu.ClassNumber, 
						s.StudentID,
						$NameField AS StudentName, 
						s.Reason, 
						s.Remark,
						s.RecordID 
					FROM DISCIPLINE_DETENTION_SESSION as d 
					INNER JOIN DISCIPLINE_DETENTION_STUDENT_SESSION as s ON s.DetentionID=d.DetentionID 
					INNER JOIN INTRANET_USER as u ON (s.StudentID = u.UserID)
					INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=u.UserID)
					INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
					WHERE d.DetentionDate='$DetentionDate' AND s.RecordStatus=1 AND yc.AcademicYearID='$academicYearID' $cond
					ORDER BY yc.Sequence,ycu.ClassNumber,u.UserID";
			$records = $this->returnArray($sql);
			//debug_r($sql);
			
			/*
			// test data
			for($i=0;$i<10;$i++) {
				$records[] = array('YearClassID'=>366,'ClassTitle'=>'F1A','ClassNumber'=>'0','StudentName'=>'Test','Reason'=>'Test Reason');
				$records[] = array('YearClassID'=>367,'ClassTitle'=>'6S','ClassNumber'=>'0','StudentName'=>'Test','Reason'=>'Test Reason');
				$records[] = array('YearClassID'=>368,'ClassTitle'=>'K1A','ClassNumber'=>'0','StudentName'=>'Test','Reason'=>'Test Reason');
			}
			*/
			return $records;
		}
		
		// @param TargetType : 'form' | 'class' | 'student'
		function getDetentionPunishmentStudents($StartDate, $EndDate, $TargetType, $TargetIdAry, $AttendanceStatusAry, $NumOfTimes)
		{
			$ClassTitleField = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
			$NameField = getNameFieldByLang("u.");
			$academicYearID = Get_Current_Academic_Year_ID();
			
			$cond = "";
			
			if($TargetType == 'form') {
				$cond .= " AND y.YearID IN (".implode(",",$TargetIdAry).") ";
			}else if($TargetType == 'class'){
				$cond .= " AND yc.YearClassID IN (".implode(",",$TargetIdAry).")";
			}else if($TargetType == 'student') {
				$cond .= " AND ycu.UserID IN (".implode(",",$TargetIdAry).") "; 
			}else{
				return array();
			}
			
			$cond .= " AND (d.DetentionDate BETWEEN '$StartDate' AND '$EndDate') ";
			
			// Display Not Take Attandance checking [2015-0128-1121-06170]
			if(in_array('NTA', $AttendanceStatusAry)){
				$NTA_cond = " OR s.AttendanceStatus IS NULL";
			}
			// Add $NTA_cond [2015-0128-1121-06170]
//			$cond .= " AND s.AttendanceStatus IN ('".implode("','",$AttendanceStatusAry)."') ";
			$cond .= " AND (s.AttendanceStatus IN ('".implode("','",$AttendanceStatusAry)."') $NTA_cond)";
			
			$sql = "SELECT 
						yc.YearClassID,
						$ClassTitleField as ClassTitle,
						ycu.ClassNumber, 
						s.StudentID,
						$NameField AS StudentName, 
						d.DetentionDate,
						s.Reason, 
						s.Remark,
						s.RecordID 
					FROM DISCIPLINE_DETENTION_SESSION as d 
					INNER JOIN DISCIPLINE_DETENTION_STUDENT_SESSION as s ON s.DetentionID=d.DetentionID 
					INNER JOIN INTRANET_USER as u ON (s.StudentID = u.UserID)
					INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=u.UserID)
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$academicYearID'
					INNER JOIN YEAR as y ON y.YearID=yc.YearID 
					WHERE s.RecordStatus=1 AND yc.AcademicYearID='$academicYearID' $cond 
					ORDER BY yc.Sequence,ClassTitle,ycu.ClassNumber,u.UserID,d.DetentionDate";
			//debug_pr($sql);		
			$records = $this->returnResultSet($sql);
			$record_count = count($records);
			$studentIdToRecords = array();
			for($i=0;$i<$record_count;$i++) {
				$student_id = $records[$i]['StudentID'];
				if(!isset($studentIdToRecords)) {
					$studentIdToRecords[$student_id] = array();
				}
				$studentIdToRecords[$student_id][] = $records[$i];
				// start test data
				//for($j=0;$j<50;$j++){
				//	$studentIdToRecords[$student_id][] = $records[$i];
				//}
				// end test data
			}
			
			// Filter number of times
			$finalStudentIdToRecords = array();
			if($NumOfTimes == 0) {
				$finalStudentIdToRecords = $studentIdToRecords;
			}else{
				$student_count = count($studentIdToRecords);
				if($student_count > 0){
					foreach($studentIdToRecords as $student_id => $records){
						if(count($records)>=$NumOfTimes) {
							$finalStudentIdToRecords[$student_id] = $records;
						}
					}
				}
			}
			
			return $finalStudentIdToRecords;
		}
		################################################################################################
		# [End] [TWGHs Chen Zao Men College]
		################################################################################################
		
		
		
		function retrieveConductPotentialsReport($classID, $issueDate="", $option=array()) {

			global $linterface;
			global $Lang, $button_export, $button_cancel, $button_print;
			global $UserID;
			
			$academicYearID = Get_Current_Academic_Year_ID();
			
			$startDate = getStartDateOfAcademicYear($academicYearID);
			$endDate = getEndDateOfAcademicYear($academicYearID);				
			$dateRange = " AND RecordDate BETWEEN '$startDate' AND '$endDate'";
			
			$yearInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
			$EngAcademicYear = str_replace("-", " / ",$yearInfo[1]);
			
			$heading .= "<span class='SchoolNameCh'>".$Lang['eDiscipline']['SKSS']['SchoolName']['CH']."</span><br>";
			$heading .= "<span class='SchoolNameEn'>".$Lang['eDiscipline']['SKSS']['SchoolName']['EN']."</span><br>";
			$heading .= "<span class='report_title'><b>".$Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Heading']."<br>".$EngAcademicYear ."</b></span>";
					
			$tableContent .= "<style type=\"text/css\">
						.SchoolNameCh {font-size: 24px; font-family: \"標楷體\"}
						.SchoolNameEn {font-size: 21px; font-family: \"Times New Roman\"}
						.report_title {font-size: 14px; font-family: \"Times New Roman\", \"標楷體\"}
						.td_bottom {border-bottom: 1px solid #000000;}
						td { font-size: 15px; font-family: \"Times New Roman\", \"標楷體\";}
						html, body{ margin: 0px; padding: 0px; }
				</style>
				";
			
			$studentID = array();
			$classID = "::".$classID;
			$studentID = $this->storeStudent(0, $classID, $academicYearID);

			$assessmentItem = array();
			$assessmentItem = $this->get_SKSS_AssessmentItemTable($studentID);
				
			$meritDemeritContent = $this->getMeritDemeritTable_SKSS($studentID, $startDate, $endDate);
			
			$i = 0;
			foreach($studentID as $sid) {
				$stdInfo = $this->retrieveStudentInfoByID2($sid, $academicYearID);
				list($engname, $chiname, $clsName, $clsNo) = $stdInfo[0];
				$sql = "SELECT IFNULL(WebSAMSRegNo,IF(WebSAMSRegNo !='', WebSAMSRegNo ,'&nbsp;')) as regno FROM INTRANET_USER WHERE UserID=$sid";
				$temp = $this->returnVector($sql);
				$sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE AcademicYearID=".$academicYearID." AND (ClassTitleB5='$clsName' OR ClassTitleEN='$clsName')";
				$temp2 = $this->returnVector($sql);
				$clsName = $temp2[0];
				
				
				$regno = substr($temp[0],0,1)=="#" ? substr($temp[0],1) : $temp[0];
				if($regno=="") $regno = "&nbsp;";
			
				$topHeight = 920;
				$bottomHeight = 70;
				$tableContent .= "<br><br>";
				$tableContent .= "<table width='90%' border='0' align='center'><tr><td valign='top' height='$topHeight'>";
				$tableContent .= "<table width='100%' border='0' $view_css>";
				$tableContent .= "<tr><td align='center' height='60' valign='top'>$heading</td></tr>";
				$tableContent .= "<tr><td><br>";
				$tableContent .= "<table width='100%' border='0'><tr>";
				$tableContent .= "
					<td width='11%' nowrap>".$Lang['eDiscipline']['SKSS']['Name']."</td>
					<td class='td_bottom' width='18%' nowrap align='center'>$engname</td>
					<td width='12%' nowrap>（". $chiname."）</td>
					<td width='12%' align='right' nowrap>".$Lang['eDiscipline']['SKSS']['Class']."</td>
					<td class='td_bottom' align='center' width='10%' nowrap align='center'>$clsName ($clsNo)</td>
					<td align='center' width='22%' align='center' nowrap>".$Lang['eDiscipline']['SKSS']['RegNo']."</td>
					<td class='td_bottom' width='15%' align='center' nowrap align='center'>$regno</td>";
				$tableContent .= "</tr></table><br>";
				$tableContent .= "</td></tr>";
				$tableContent .= "<tr><td>";
				$tableContent .= $assessmentItem[$sid];
				$tableContent .= "</td></tr>";
				$tableContent .= "</table>";
				$tableContent .= "<br>";
				
				//$tableContent .= "<table width='98%' border='0' $view_css><tr><td>";
				$tableContent .= "<table width='100%' align='center' $view_css border='0'>";
				$tableContent .= "<tr><td colspan='4'><b><u>".$Lang['eDiscipline']['SKSS']['MeritsDemerits']."</u></b></td><td><b><u>".$Lang['eDiscipline']['SKSS']['Particulars']."</u></b></td></tr>";
				
				
				$tableContent .= "<tr>";
				$tableContent .= "<td valign='top' width='5%' height='30'>1.</td>";
				$tableContent .= "<td valign='top' width='23%' >".$Lang['eDiscipline']['SKSS']['NoOfMerits'][0]."</td>";
				$tableContent .= "<td valign='top' width='15%' >".$Lang['eDiscipline']['SKSS']['NoOfMerits'][1]."</td>";
				$tableContent .= "<td valign='top' width='10%' >".((!isset($meritDemeritContent[$sid][1]['Sum']) || $meritDemeritContent[$sid][1]['Sum']==0) ? "---" : $meritDemeritContent[$sid][1]['Sum'])."</td>";
				//$tableContent .= "<td valign='top' width='60%'>".$meritDemeritContent[$sid][1]['Display']."</td>";
				$tableContent .= "<td valign='top'>".$meritDemeritContent[$sid][1]['Display']."</td>";
				$tableContent .= "</tr>";
				
				$tableContent .= "<tr>";
				$tableContent .= "<td valign='top' width='1' height='30'>2.</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMinorHonour'][0]."</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMinorHonour'][1]."</td>";
				$tableContent .= "<td valign='top'>".((!isset($meritDemeritContent[$sid][2]['Sum']) || $meritDemeritContent[$sid][2]['Sum']==0) ? "---" : $meritDemeritContent[$sid][2]['Sum'])."</td>";
				//$tableContent .= "<td valign='top'>".(((!isset($meritDemeritContent[$sid][2]['Remark']) || $meritDemeritContent[$sid][2]['Remark']=="") && (!isset($meritDemeritContent[$sid][2]['ItemText']) || $meritDemeritContent[$sid][2]['ItemText']=="")) ? "<br><br>" : (($meritDemeritContent[$sid][2]['Remark']=="") ? $meritDemeritContent[$sid][2]['ItemText'] : $meritDemeritContent[$sid][2]['Remark']))."</td>";
				$tableContent .= "<td valign='top'>".$meritDemeritContent[$sid][2]['Display']."</td>";
				$tableContent .= "</tr>";
				
				$tableContent .= "<tr>";
				$tableContent .= "<td valign='top' width='1' height='30'>3.</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMajorHonour'][0]."</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMajorHonour'][1]."</td>";
				$tableContent .= "<td valign='top'>".((!isset($meritDemeritContent[$sid][3]['Sum']) || $meritDemeritContent[$sid][3]['Sum']==0) ? "---" : $meritDemeritContent[$sid][3]['Sum'])."</td>";
				//$tableContent .= "<td valign='top'>".(((!isset($meritDemeritContent[$sid][3]['Remark']) || $meritDemeritContent[$sid][3]['Remark']=="") && (!isset($meritDemeritContent[$sid][3]['ItemText']) || $meritDemeritContent[$sid][3]['ItemText']=="")) ? "<br><br>" : (($meritDemeritContent[$sid][3]['Remark']=="") ? $meritDemeritContent[$sid][3]['ItemText'] : $meritDemeritContent[$sid][3]['Remark']))."</td>";
				$tableContent .= "<td valign='top'>".$meritDemeritContent[$sid][3]['Display']."</td>";
				$tableContent .= "</tr>";
				
				$tableContent .= "<tr><td colspan='5'>&nbsp;</td></tr>";
				
				$tableContent .= "<tr>";
				$tableContent .= "<td valign='top' width='1' height='30'>4.</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfDemerits'][0]."</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfDemerits'][1]."</td>";
				$tableContent .= "<td valign='top'>".((!isset($meritDemeritContent[$sid][-1]['Sum']) || $meritDemeritContent[$sid][-1]['Sum']==0) ? "---" : $meritDemeritContent[$sid][-1]['Sum'])."</td>";
				//$tableContent .= "<td valign='top'>".(((!isset($meritDemeritContent[$sid][-1]['Remark']) || $meritDemeritContent[$sid][-1]['Remark']=="") && (!isset($meritDemeritContent[$sid][-1]['ItemText']) || $meritDemeritContent[$sid][-1]['ItemText']=="")) ? "<br><br>" : (($meritDemeritContent[$sid][-1]['Remark']=="") ? $meritDemeritContent[$sid][-1]['ItemText'] : $meritDemeritContent[$sid][-1]['Remark']))."</td>";
				$tableContent .= "<td valign='top'>".$meritDemeritContent[$sid][-1]['Display']."</td>";
				$tableContent .= "</tr>";
				
				$tableContent .= "<tr>";
				$tableContent .= "<td valign='top' width='1' height='30'>5.</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMinorOffence'][0]."</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMinorOffence'][1]."</td>";
				$tableContent .= "<td valign='top'>".((!isset($meritDemeritContent[$sid][-2]['Sum']) || $meritDemeritContent[$sid][-2]['Sum']==0) ? "---" : $meritDemeritContent[$sid][-2]['Sum'])."</td>";
				//$tableContent .= "<td valign='top'>".(((!isset($meritDemeritContent[$sid][-2]['Remark']) || $meritDemeritContent[$sid][-2]['Remark']=="") && (!isset($meritDemeritContent[$sid][-2]['ItemText']) || $meritDemeritContent[$sid][-2]['ItemText']=="")) ? "<br><br>" : (($meritDemeritContent[$sid][-2]['Remark']=="") ? $meritDemeritContent[$sid][-2]['ItemText'] : $meritDemeritContent[$sid][-2]['Remark']))."</td>";
				$tableContent .= "<td valign='top'>".$meritDemeritContent[$sid][-2]['Display']."</td>";
				$tableContent .= "</tr>";
				
				$tableContent .= "<tr>";
				$tableContent .= "<td valign='top' width='1' height='30'>6.</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMajorOffence'][0]."</td>";
				$tableContent .= "<td valign='top'>".$Lang['eDiscipline']['SKSS']['NoOfMajorOffence'][1]."</td>";
				$tableContent .= "<td valign='top'>".((!isset($meritDemeritContent[$sid][-3]['Sum']) || $meritDemeritContent[$sid][-3]['Sum']==0) ? "---" : $meritDemeritContent[$sid][-3]['Sum'])."</td>";
				//$tableContent .= "<td valign='top'>".(((!isset($meritDemeritContent[$sid][-3]['Remark']) || $meritDemeritContent[$sid][-3]['Remark']=="") && (!isset($meritDemeritContent[$sid][-3]['ItemText']) || $meritDemeritContent[$sid][-3]['ItemText']=="")) ? "<br><br>" : (($meritDemeritContent[$sid][-3]['Remark']=="") ? $meritDemeritContent[$sid][-3]['ItemText'] : $meritDemeritContent[$sid][-3]['Remark']))."</td>";
				$tableContent .= "<td valign='top'>".$meritDemeritContent[$sid][-3]['Display']."</td>";
				$tableContent .= "</tr>";
				$tableContent .= "</table>";
				
				
				$tableContent .= "</td></tr>";
				$tableContent .= "</table>";
				
				//$tableContent .= "<td></tr><tr><td valign='bottom' align='center' height='$bottomHeight'>";
				$tableContent .= $this->return_SKSS_Signature($option, $issueDate, $this->getClassIDByClassName($clsName, Get_Current_Academic_Year_ID()));
		
				//$tableContent .= "</td></tr></table>";
					
				if($i!=(sizeof($studentID)-1))
					$tableContent .= "<div style='page-break-after:always'>&nbsp;</div>";
			
			$i++;
			}
			return $tableContent;			
		}
		
		function get_SKSS_AssessmentItemTable($studentIDAry=array())
		{
			global $Lang;
			
			if(sizeof($studentIDAry)>0) {
				$sql = "SELECT StudentID, GradeString FROM DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT WHERE StudentID IN (".implode(',',$studentIDAry).")";
				$result = $this->returnArray($sql,2);
				for($i=0; $i<sizeof($result); $i++) {
					list($sid, $gradeString) = $result[$i];
					$temp = explode(";", $gradeString);
					for($j=0; $j<sizeof($temp); $j++) {
						$g = explode(":",$temp[$j]);
						$data[$sid][$g[0]] = $g[1];
					}					
				}
				
				$FormLevel = $this->getClassLevelIDByUserID($studentIDAry[0]);
				//$FormLevelID = $FormLevel[0]['YearID'];
				$FormLevelID = $FormLevel[0];
				$juniorForm = array(1,2,3);
				
				if(in_array($FormLevelID, $juniorForm)) {
					$ItemNameAry = $Lang['eDiscipline']['SKSS']['Items_Junior'];
				} else {
					$ItemNameAry = $Lang['eDiscipline']['SKSS']['Items'];
				}
				
				if(sizeof($data)>0) {
					foreach($data as $sid=>$dataResult) {
							# table content
							$returnAry[$sid] .= "<table width='70%' border='0' cellpadding='0' cellspacing='1' >";
							$returnAry[$sid] .= "<tr class='tabletext'><td colspan='4'><b><u>".$Lang['eDiscipline']['SKSS']['AssessmentItems']."</u></b></td></tr>";
							$returnAry[$sid] .= "<tr class='tabletext'><td colspan='4'>".$Lang['eDiscipline']['SKSS']['GradeDescription']."</td></tr>";
							for($k=1; $k<sizeof($ItemNameAry); $k++) {
								$returnAry[$sid] .= "<tr>";
								$returnAry[$sid] .= "<td width='7%' >$k.</td>";
								$returnAry[$sid] .= "<td width='48%' >".$ItemNameAry[$k][0]."</td>";
								$returnAry[$sid] .= "<td width='30%' >".$ItemNameAry[$k][1]."</td>";
								$returnAry[$sid] .= "<td >".$dataResult[$k]."</td>";
								$returnAry[$sid] .= "</tr>";
							}
							$returnAry[$sid] .= "<tr><td colspan='4'>&nbsp;</td></tr>";
							$returnAry[$sid] .= "</table>";
					}
				}
				return $returnAry;
			}
		}
		
		function return_SKSS_Signature($option=array(), $issueDate="", $YearClassID="")
		{
			global $Lang;
			if(sizeof($option)>0) {
				if($issueDate!="")
	        			$issueDate = date("j/n/Y", mktime(0,0,0,substr($issueDate,5,2),substr($issueDate,8,2),substr($issueDate,0,4)));
	        			
				$w = 185;
		$tablewidth = $w * sizeof($option);
		
		$width = 100/(sizeof($option));

		$bottomHeight = 70;
 		
 				//$table = "<table width=$tablewidth border='01' width='95%' align='center' height='$bottomHeight';><tr>";
 				$table = "<table border='0' width='90%' align='center' height='$bottomHeight';><tr>";
				foreach($option as $opt) {
					$signAry = $Lang['eDiscipline']['SKSS']['ConductPotentialSignature'][$opt];
					$teacherText = "";
					$displayText = "&nbsp;";
					if($opt==0)
						$teacherText = "<br>".$Lang['eDiscipline']['SKSS']['Principal'];
					else if($opt==1 && $YearClassID!="") {
						$teacherText = "";
						$name_field = getNameFieldByLang("USR.", "b5");
						$sql = "SELECT $name_field as name FROM YEAR_CLASS_TEACHER yct LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=yct.UserID) WHERE yct.YearClassID='$YearClassID'";
						$teacherAry = $this->returnVector($sql);
						
						if(sizeof($teacherAry)>0) {
							$teacherText .= "<br>".implode('/<br>',$teacherAry);
						}

					}
					$displayText = ($opt==3 && $issueDate!="") ? $issueDate : "&nbsp;";

	
					$table .= "<td ailgn='center' width='".$width."%' valign='top'>";
					$table .= "	<table width='95%' border='0' align='center'>";
					$table .= "		<tr><td class='td_bottom' align='center' >$displayText</td></tr>";
					$table .= "		<tr><td align='center' >".$signAry[0]."<br>".$signAry[1]."$teacherText</td></tr>";
					$table .= "	</table>";
					$table .= "</td>";
				}
				$table .= "</tr></table>";
				return $table;
			}
		}
		
		##############################################
		## MST [start]
		##############################################
		function returnPunishmentDataByNoticeID($NoticeID)
		{
			global $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit, $i_Merit_Warning, $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit;
			$sql = "select Remark, ItemText, 
						CASE ProfileMeritType 
						WHEN 1 THEN \"".addslashes($i_Merit_Merit)."\"
						WHEN 2 THEN \"".addslashes($i_Merit_MinorCredit)."\"
						WHEN 3 THEN \"".addslashes($i_Merit_MajorCredit)."\"
						WHEN 4 THEN \"".addslashes($i_Merit_SuperCredit)."\"
						WHEN 5 THEN \"".addslashes($i_Merit_UltraCredit)."\"
						WHEN 0 THEN \"".addslashes($i_Merit_Warning)."\"
						WHEN -1 THEN \"".addslashes($i_Merit_BlackMark)."\"
						WHEN -2 THEN \"".addslashes($i_Merit_MinorDemerit)."\"
						WHEN -3 THEN \"".addslashes($i_Merit_MajorDemerit)."\"
						WHEN -4 THEN \"".addslashes($i_Merit_SuperDemerit)."\"
						WHEN -5 THEN \"".addslashes($i_Merit_UltraDemerit)."\"
						END AS ProfileMeritType, 
						ProfileMeritCount,
						RecordNumber,
						RecordDate,
						AcademicYearID,
						Remark
					from DISCIPLINE_MERIT_RECORD where NoticeID=$NoticeID and MeritType=-1";
			$result = $this->returnArray($sql);
			return $result[0];
		}
		
		function returnClassNameEn($sid, $aid)
		{
			include_once("form_class_manage.php");
			$lfc = new form_class_manage();
			
			$class_id = $lfc->Get_Student_Studying_Class($sid, $aid);
			$lyc = new year_class($class_id);
			return $lyc->ClassTitleEN;
		}
		##############################################
		## MST [end]
		##############################################
		
		function getMisconductAndPunishmentNotificationRecords($options)
		{
			global $Lang, $PATH_WRT_ROOT;
			
			/*
			 * TemplateType
			 * 1: 已達5次遲到記錄 (accumulate 5 lates)
			 * 2: 遲到6次轉記缺點 (accumulate 6 lates and to demerit)
			 * 3: 違規記錄已扣5分 (misconduct accumulate 5 scores)
			 * 4: 扣分達6分或以上轉記缺點 (misconduct >= 6 scores)
			 * 5: 違反校規被處分 (punishment)
			 */
			$template_type = $options['TemplateType'];
			$send_status = $options['SendStatus'];
			$rankTarget = $options['rankTarget'];
			$selectYear = Get_Current_Academic_Year_ID();
			if(isset($options['RecordID']) && count($options['RecordID'])){
				$recordIdCond = " AND a.RecordID IN ('".implode("','",$options['RecordID'])."') ";
			}else{
				if($rankTarget == 'form'){
					$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				}else if($rankTarget == 'class'){
					$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				}else if($rankTarget == 'student'){
					$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
					$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
				}
			}
			
			$EmptySymbol = $Lang['General']['EmptySymbol'];
			$StylePrefix = '<span class="tabletextrequire">';
			$StyleSuffix = '</span>';
			$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
			$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
			$StudentNameField = getNameFieldByLang("iu.");
			$senderNameField = getNameFieldByLang("iu2.");
			$clsName = $YearClassNameField;
			
			if(in_array($template_type,array(1,2))){
				$categoryCond = " AND a.CategoryID='".PRESET_CATEGORY_LATE."' ";
			}else{
				//$categoryCond = " AND a.CategoryID<>'".PRESET_CATEGORY_LATE."' ";
			}
			if(in_array($template_type,array(2,4))){
				$groupCond = " AND (a.UpgradedRecordID IS NOT NULL AND a.UpgradedRecordID>0) ";
			}
			if(in_array($template_type,array(1,3))){
				$groupCond = " AND (a.UpgradedRecordID=0 OR a.UpgradedRecordID IS NULL) ";
			}
			
			if($send_status == '1'){
				$sendstatusCond = " AND (e.SendStatus IS NOT NULL AND e.SendStatus='1') ";
			}else{
				$sendstatusCond = " AND (e.SendStatus IS NULL OR e.SendStatus='0') ";
			}
			
			$list = array();
			
			if($send_status == '1'){ // find sent records
				$sql = "SELECT 
							iu.UserID,
							yc.YearClassID,
							$YearClassNameField as ClassName,
							ycu.ClassNumber,
							CONCAT(
								IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
								$StudentNameField
							) as StudentName,
							a.RecordID,
							a.TemplateType,
							a.RelatedRecordID,
							a.RecordDetail,
							a.RecordAry,
							a.DateModified,
							$senderNameField as SenderName 
						FROM INTRANET_USER as iu 
						INNER JOIN DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD as a ON a.StudentID=iu.UserID 
						LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
						LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
						LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
						LEFT JOIN INTRANET_USER as iu2 ON iu2.UserID=a.ModifiedBy 
						WHERE iu.RecordType='".USERTYPE_STUDENT."' AND a.AcademicYearID='".$selectYear."' AND a.TemplateType='$template_type'
							 AND yc.AcademicYearID = '".$selectYear."' $recordIdCond 
							 $yearIdCond $classIdCond $studentIdCond 
						GROUP BY a.RecordID 
						ORDER BY iu.ClassName,iu.ClassNumber+0,a.DateModified";
				$records = $this->returnResultSet($sql);
				$record_count = count($records);
//				debug_r($sql);
				for($i=0;$i<$record_count;$i++){
					$record_detail = $records[$i]['RecordDetail'];
					$record_ary_ary = explode("<br>",$records[$i]['RecordAry']);
					
					$record = array();
					$record['UserID'] = $records[$i]['UserID'];
			 		$record['ClassName'] = $records[$i]['ClassName'];
			 		$record['ClassNumber'] = $records[$i]['ClassNumber'];
			 		$record['StudentName'] = $records[$i]['StudentName'];
			 		$record['SendStatus'] = '1';
			 		$record['DateModified'] = $records[$i]['DateModified'];
			 		$record['SenderName'] = $records[$i]['SenderName'];
			 		$record['RecordID'] = $records[$i]['RecordID'];
			 		$record['TemplateType'] = $records[$i]['TemplateType'];
			 		$record['RelatedRecordID'] = $records[$i]['RelatedRecordID']; // GM or AP RecordID separated with comma
			 		$record['RecordDetail'] = $record_detail;
			 		$record['RecordAry'] = array();
			 		
			 		for($j=0;$j<count($record_ary_ary);$j+=2){
			 			$record['RecordAry'][] = array($record_ary_ary[$j],$record_ary_ary[$j+1]);
			 		}
			 		
			 		$list[] = $record;
				}
				
			}else{ // find unsend records
				if(in_array($template_type,array(1,2,3,4)))
				{
					$sql = "SELECT 
								iu.UserID,
								yc.YearClassID,
								$YearClassNameField as ClassName,
								ycu.ClassNumber,
								CONCAT(
									IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
									$StudentNameField
								) as StudentName,
								a.RecordID,
								a.CategoryID,
								DATE_FORMAT(a.RecordDate,'%d/%m/%Y') as RecordDate,
								a.AcademicYearID,
								a.UpgradedRecordID,
								a.ScoreChange,
								c.Name,
								c.ItemCode,
								e.RecordID as EmailRecordID,
								IF(e.SendStatus IS NULL,'0',e.SendStatus) as SendStatus,
								e.DateModified,
								$senderNameField as SenderName 
							FROM INTRANET_USER as iu 
							INNER JOIN DISCIPLINE_ACCU_RECORD as a ON a.StudentID=iu.UserID 
							LEFT JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as c ON c.ItemID=a.ItemID 
							LEFT JOIN DISCIPLINE_EMAIL_NOTIFICATION_RECORD as e ON e.RecordType='1' AND e.RelatedRecordID=a.RecordID AND e.StudentID=a.StudentID 
							LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
							LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
							LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
							LEFT JOIN INTRANET_USER as iu2 ON iu2.UserID=e.ModifiedBy 
							WHERE iu.RecordType='".USERTYPE_STUDENT."' AND a.RecordType=-1 AND a.RecordStatus='". DISCIPLINE_STATUS_APPROVED ."' 
								AND a.AcademicYearID='".$selectYear."' AND yc.AcademicYearID = '".$selectYear."' $recordIdCond 
								$categoryCond $groupCond $yearIdCond $classIdCond $studentIdCond 
							GROUP BY a.RecordID 
							ORDER BY iu.ClassName,iu.ClassNumber+0,a.RecordDate";
					 $records = $this->returnResultSet($sql);
					 $record_count = count($records);
					 //debug_r($sql);
					 //debug_r($records);
					 
					 $late5_ary = array();
					 $late5_group_ary = array();
					 $late5_date_ary = array();
					 $late6_ary = array();
					 $late6_date_ary = array();
					 $gm5_ary = array();
					 $gm5_group_ary = array();
					 $gm6_ary = array();
					 $gm5_score = array();
					 
					 for($i=0;$i<$record_count;$i++){
					 	$user_id = $records[$i]['UserID'];
					 	$cat_id = $records[$i]['CategoryID'];
					 	$upgraded_record_id = $records[$i]['UpgradedRecordID'];
					 	$score_change = $records[$i]['ScoreChange'];
					 	$email_record_id = $records[$i]['EmailRecordID'];
					 	
					 	if($score_change == 0 || (in_array($template_type,array(3,4)) && $cat_id==PRESET_CATEGORY_LATE)) continue;
					 	
					 	if($template_type == '1'){
					 		$late5_group_ary[$user_id][] = $records[$i];
					 		if(!isset($late5_date_ary[$user_id][$records[$i]['RecordDate']])){
					 			$late5_date_ary[$user_id][$records[$i]['RecordDate']] = 0;
					 		}
					 		$late5_date_ary[$user_id][$records[$i]['RecordDate']] += 1;
					 		if(count($late5_group_ary[$user_id])>=5){
					 			$late5_ary[$user_id][] = $late5_group_ary[$user_id];
					 			$tmp = $late5_group_ary[$user_id][0];
					 			$tmp_ids = Get_Array_By_Key($late5_group_ary[$user_id],'RecordID');
					 			sort($tmp_ids,SORT_NUMERIC);
					 			$tmp['RecordID'] = implode(',',$tmp_ids);
					 			$tmp['RecordDetail'] = '';
					 			$tmp['RecordAry'] = array();
					 			$tmp['RecordStatusAry'] = array(); // SendStatus array
					 			$tmp_count = count($late5_group_ary[$user_id]);
					 			$delim = '';
					 			for($j=0;$j<$tmp_count;$j++)
					 			{
					 				$tmp['RecordStatusAry'][] = $late5_group_ary[$user_id][$j]['SendStatus'];
					 				$tmp['RecordAry'][] = array($late5_group_ary[$user_id][$j]['RecordDate'],$late5_date_ary[$user_id][$late5_group_ary[$user_id][$j]['RecordDate']]);
					 				$tmp['RecordDetail'] .= $delim.$late5_group_ary[$user_id][$j]['RecordDate'].'('.$late5_date_ary[$user_id][$late5_group_ary[$user_id][$j]['RecordDate']].')';
					 				$delim = ', ';
					 			}
					 			
					 			# Get send email records
					 			$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$tmp['RecordID']."'";
					 			$rs = $this->returnVector($sql);
					 			
					 			$uniq_ary = array_unique($tmp['RecordStatusAry']);
					 			if(count($uniq_ary) == 1 && $uniq_ary[0]=='1'){ // if all SendStatus are same
					 				// match SendStatus filter condition
					 				//if(($send_status == '1' && $uniq_ary[0] == '1') || ($send_status == '0' && $uniq_ary[0] != '1')){
					 				//	$list[] = $tmp;
					 				//}
									//$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$tmp['RecordID']."'";
									//$rs = $this->returnVector($sql);
					 				if(count($rs)>0 && $rs[0]>0){ // if this group if ids find in send history
					 					$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$tmp['RecordID'].") AND RecordType='1'";
					 					$this->db_db_query($sql);
					 				}else{
					 					$tmp['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $tmp;
					 					}
					 				}
					 			}else{
					 				// SendStatus are not the same, remove sent records, treat the records as unsent
					 				$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$tmp['RecordID'].") AND RecordType='1'";
					 				$this->db_db_query($sql);
					 				
					 				//if($send_status != '1'){
					 					$tmp['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $tmp;
					 					}
					 				//}
					 			}
					 			$late5_group_ary[$user_id] = array();
					 			$late5_date_ary[$user_id] = array();
					 		}
					 	}else if($template_type == '2'){
					 		$late6_ary[$upgraded_record_id][] = $records[$i];
					 		if(!isset($late6_date_ary[$upgraded_record_id][$records[$i]['RecordDate']])){
					 			$late6_date_ary[$upgraded_record_id][$records[$i]['RecordDate']] = 0;
					 		}
					 		$late6_date_ary[$upgraded_record_id][$records[$i]['RecordDate']] += 1;
					 	}else if($template_type == '3'){
					 		$gm5_group_ary[$user_id][] = $records[$i];
					 		if(!isset($gm5_score[$user_id])){
					 			$gm5_score[$user_id] = 0;
					 		}
					 		$gm5_score[$user_id] += $score_change;
					 		if($gm5_score[$user_id] >= 5){
					 			//debug_r($gm5_group_ary[$user_id]);
					 			$gm5_ary[$user_id][] = $gm5_group_ary[$user_id];
					 			$tmp = $gm5_group_ary[$user_id][0];
					 			$tmp_ids = Get_Array_By_Key($gm5_group_ary[$user_id],'RecordID');
					 			sort($tmp_ids,SORT_NUMERIC);
					 			$tmp['RecordID'] = implode(',',$tmp_ids);
					 			$tmp['RecordDetail'] = '';
					 			$tmp['RecordAry'] = array();
					 			$tmp['RecordStatusAry'] = array(); // SendStatus array
					 			$tmp_count = count($gm5_group_ary[$user_id]);
					 			$delim = '';
					 			for($j=0;$j<$tmp_count;$j++)
					 			{
					 				$tmp['RecordStatusAry'][] = $gm5_group_ary[$user_id][$j]['SendStatus'];
					 				$tmp['RecordAry'][] = array($gm5_group_ary[$user_id][$j]['Name'].' ('.$gm5_group_ary[$user_id][$j]['ItemCode'].') ['.$gm5_group_ary[$user_id][$j]['RecordDate'].']',$gm5_group_ary[$user_id][$j]['ScoreChange']);
					 				$tmp['RecordDetail'] .= $delim.$gm5_group_ary[$user_id][$j]['RecordDate'].' '.$gm5_group_ary[$user_id][$j]['Name'].'(-'.$gm5_group_ary[$user_id][$j]['ScoreChange'].')';
					 				$delim = ', ';
					 			}
					 			
					 			# Get send email records
					 			$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$tmp['RecordID']."'";
					 			$rs = $this->returnVector($sql);
					 			
					 			$uniq_ary = array_unique($tmp['RecordStatusAry']);
					 			//debug_r($tmp['RecordStatusAry']);
					 			if(count($uniq_ary) == 1 && $uniq_ary[0]=='1'){ // if all SendStatus are same
					 				// match SendStatus filter condition
					 				//if(($send_status == '1' && $uniq_ary[0] == '1') || ($send_status == '0' && $uniq_ary[0] != '1')){
					 				//	$list[] = $tmp;
					 				//}
					 				//$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$tmp['RecordID']."'";
					 				//$rs = $this->returnVector($sql);
					 				if(count($rs)>0 && $rs[0]>0){ // if this group if ids find in send history
					 					$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$tmp['RecordID'].") AND RecordType='1'";
					 					$this->db_db_query($sql);
					 				}else{
					 					$tmp['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $tmp;
					 					}
					 				}
					 			}else{
					 				// SendStatus are not the same, remove sent records, treat the records as unsent
					 				$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$tmp['RecordID'].") AND RecordType='1'";
					 				$this->db_db_query($sql);
					 				
					 				//if($send_status != '1'){
					 					$tmp['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $tmp;
					 					}
					 				//}
					 			}
					 			//$list[] = $tmp;
					 			$gm5_group_ary[$user_id] = array();
					 			$gm5_score[$user_id] = 0;
					 			//debug_r($gm5_ary[$user_id]);
					 		}
					 	}else if($template_type == '4'){
					 		$gm6_ary[$upgraded_record_id][] = $records[$i];
					 	}
					 	
					 }
					 
					 if($template_type == '2'){
					 	if(count($late6_ary)>0){
				 			foreach($late6_ary as $ugid => $ary)
				 			{
				 				$record = $ary[0];
				 				$tmp_ids = Get_Array_By_Key($ary,'RecordID');
				 				sort($tmp_ids,SORT_NUMERIC);
					 			$record['RecordID'] = implode(',',$tmp_ids);
					 			$record['RecordDetail'] = '';
					 			$record['RecordAry'] = array();
					 			$record['RecordStatusAry'] = array(); // SendStatus array
					 			$tmp_count = count($ary);
					 			$delim = '';
					 			for($j=0;$j<$tmp_count;$j++)
					 			{
					 				$record['RecordStatusAry'][] = $ary[$j]['SendStatus'];
					 				$record['RecordAry'][] = array($ary[$j]['RecordDate'],$late6_date_ary[$ugid][$ary[$j]['RecordDate']]);
					 				$record['RecordDetail'] .= $delim.$ary[$j]['RecordDate'].'('.$late6_date_ary[$ugid][$ary[$j]['RecordDate']].')';
					 				$delim = ', ';
					 			}
					 			$uniq_ary = array_unique($record['RecordStatusAry']);
					 			
					 			# Get send email records
					 			$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$record['RecordID']."'";
					 			$rs = $this->returnVector($sql);
					 			
					 			if(count($uniq_ary) == 1 && $uniq_ary[0]=='1'){ // if all SendStatus are same
					 				// match SendStatus filter condition
					 				//if(($send_status == '1' && $uniq_ary[0] == '1') || ($send_status == '0' && $uniq_ary[0] != '1')){
					 				//	$list[] = $record;
					 				//}
									//$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$record['RecordID']."'";
									//$rs = $this->returnVector($sql);
					 				if(count($rs)>0 && $rs[0]>0){ // if this group if ids find in send history
					 					$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$record['RecordID'].") AND RecordType='1'";
					 					$this->db_db_query($sql);
					 				}else{
					 					$tmp['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $tmp;
					 					}
					 				}
					 			}else{
					 				// SendStatus are not the same, remove sent records, treat the records as unsent
					 				$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$record['RecordID'].") AND RecordType='1'";
					 				$this->db_db_query($sql);
					 				
					 				//if($send_status != '1'){
					 					$record['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $record;
					 					}
					 				//}
					 			}
					 			//$list[] = $record;
				 			}
					 	}
					 }
					 if($template_type == '4'){
					 	if(count($gm6_ary)>0){
					 		foreach($gm6_ary as $ugid => $ary){
					 			$record = $ary[0];
					 			$tmp_ids = Get_Array_By_Key($ary,'RecordID');
					 			sort($tmp_ids,SORT_NUMERIC);
					 			$record['RecordID'] = implode(',',$tmp_ids);
					 			$record['RecordDetail'] = '';
					 			$record['RecordAry'] = array();
					 			$record['RecordStatusAry'] = array(); // SendStatus array
					 			$tmp_count = count($ary);
					 			$delim = '';
					 			for($j=0;$j<$tmp_count;$j++)
					 			{
					 				$record['RecordStatusAry'][] = $ary[$j]['SendStatus'];
					 				$record['RecordAry'][] = array($ary[$j]['Name'].' ('.$ary[$j]['ItemCode'].') ['.$ary[$j]['RecordDate'].']', $ary[$j]['ScoreChange'] );
					 				$record['RecordDetail'] .= $delim.$ary[$j]['RecordDate'].' '.$ary[$j]['Name'].'(-'.$ary[$j]['ScoreChange'].')';
					 				$delim = ', ';
					 			}
					 			$uniq_ary = array_unique($record['RecordStatusAry']);
					 			
					 			# Get send email records
					 			$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$record['RecordID']."'";
					 			$rs = $this->returnVector($sql);
					 			
					 			if(count($uniq_ary) == 1 && $uniq_ary[0]=='1'){ // if all SendStatus are same
					 				// match SendStatus filter condition
					 				//if(($send_status == '1' && $uniq_ary[0] == '1') || ($send_status == '0' && $uniq_ary[0] != '1')){
					 				//	$list[] = $record;
					 				//}
					 				//$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE RelatedRecordID='".$record['RecordID']."'";
					 				//$rs = $this->returnVector($sql);
					 				if(count($rs)>0 && $rs[0]>0){ // if this group if ids find in send history
					 					$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$record['RecordID'].") AND RecordType='1'";
					 					$this->db_db_query($sql);
					 				}else{
					 					$tmp['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $tmp;
					 					}
					 				}
					 			}else{
					 				// SendStatus are not the same, remove sent records, treat the records as unsent
					 				$sql = "DELETE FROM DISCIPLINE_EMAIL_NOTIFICATION_RECORD WHERE RelatedRecordID IN (".$record['RecordID'].") AND RecordType='1'";
					 				$this->db_db_query($sql);
					 				
					 				//if($send_status != '1'){
					 					$record['SendStatus'] = '0';
					 					if($rs[0]==''){
					 						$list[] = $record;
					 					}
					 				//}
					 			}
					 			//$list[] = $record;
					 		}
					 	}
					 }
				}else{
					$sql = "SELECT 
								iu.UserID,
								yc.YearClassID,
								$YearClassNameField as ClassName,
								ycu.ClassNumber,
								CONCAT(
									IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
									$StudentNameField
								) as StudentName,
								a.RecordID,
								a.ItemID,
								IF(c.ItemID IS NOT NULL,c.ItemName,a.ItemText) as ItemName,
								c.ItemCode,
								d.CategoryName, 
								DATE_FORMAT(a.RecordDate,'%d/%m/%Y') as RecordDate,
								a.AcademicYearID,
								a.ProfileMeritType,
								a.ProfileMeritCount,
								e.RecordID as EmailRecordID,
								e.SendStatus,
								e.DateModified,
								$senderNameField as SenderName 
							FROM INTRANET_USER as iu 
							INNER JOIN DISCIPLINE_MERIT_RECORD as a ON a.StudentID=iu.UserID 
							LEFT JOIN DISCIPLINE_MERIT_ITEM as c ON c.ItemID=a.ItemID 
							LEFT JOIN DISCIPLINE_MERIT_ITEM_CATEGORY as d ON d.CatID=c.CatID 
							LEFT JOIN DISCIPLINE_EMAIL_NOTIFICATION_RECORD as e ON e.RecordType='2' AND e.RelatedRecordID=a.RecordID AND e.StudentID=a.StudentID 
							LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
							LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
							LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
							LEFT JOIN INTRANET_USER as iu2 ON iu2.UserID=e.ModifiedBy 
							WHERE iu.RecordType='".USERTYPE_STUDENT."' AND (a.fromConductRecords <> '1' OR a.fromConductRecords IS NULL) AND a.MeritType=-1 AND a.ProfileMeritType!=-999 AND a.RecordStatus='". DISCIPLINE_STATUS_APPROVED ."' 
								AND a.AcademicYearID='".$selectYear."' AND yc.AcademicYearID = '".$selectYear."' $recordIdCond 
								$categoryCond $groupCond $yearIdCond $classIdCond $studentIdCond $sendstatusCond
							GROUP BY a.RecordID 
							ORDER BY iu.ClassName,iu.ClassNumber+0,a.RecordDate";
					 $records = $this->returnResultSet($sql);
					 $record_count = count($records);
					 //debug_r($sql);
					 //debug_r($records);
					 //$cur_uid = -1;
					 $record = array();
					 for($i=0;$i<$record_count;$i++){
					 	
					 	if($records[$i]['UserID'] != $records[$i-1]['UserID']){
					 		$record = array();
					 		$record['UserID'] = $records[$i]['UserID'];
					 		$record['ClassName'] = $records[$i]['ClassName'];
					 		$record['ClassNumber'] = $records[$i]['ClassNumber'];
					 		$record['StudentName'] = $records[$i]['StudentName'];
					 		$record['SendStatus'] = $records[$i]['SendStatus'];
					 		$record['DateModified'] = $records[$i]['DateModified'];
					 		$record['SenderName'] = $records[$i]['SenderName'];
					 		$record['AcademicYearID'] = $records[$i]['AcademicYearID'];
					 		$record['RecordID'] = '';
					 		$record['RecordDetail'] = '';
					 		$record['RecordAry'] = array();
					 		$delim = '';
					 		$delim_nospace = '';
					 		//$cur_uid = $records[$i]['UserID'];
					 	}
					 	
					 	$ap_punishment = '';
					 	switch($records[$i]['ProfileMeritType'])
						{
							//case '-999':
							//	$i_Merit_NoAwardPunishment;
							//break;
							case '0':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['Warning'];
							break;
							case '-1':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['BlackMark'];
		
							break;
							case '-2':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MinorDemerit'];
							break;
							case '-3':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['MajorDemerit'];
							break;
							case '-4':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['SuperDemerit'];
							break;
							case '-5':
								$ap_punishment = $Lang['eDiscipline']['AttendanceAndAwardPunishmentReport']['UltraDemerit'];
							break;
						}
						$ap_punishment = $records[$i]['ProfileMeritCount'].' '.$ap_punishment;
					 	
					 	$record['RecordID'] .= $delim_nospace.$records[$i]['RecordID'];
					 	$record['RecordDetail'] .= $delim.$records[$i]['RecordDate'].' '.$records[$i]['ItemName'].' ('.$ap_punishment.')';
					 	$record['RecordAry'][] = array($records[$i]['ItemName'].' ['.$records[$i]['RecordDate'].'] ('.$records[$i]['CategoryName'].') ('.$records[$i]['ItemCode'].') ', $ap_punishment);
					 	$delim = ', ';
					 	$delim_nospace = ',';
					 	
					 	if($records[$i]['UserID'] != $records[$i+1]['UserID'] && count($record)>0){
					 		$tmp_ids = explode(',',$record['RecordID']);
					 		sort($tmp_ids,SORT_NUMERIC);
					 		$record['RecordID'] = implode(',',$tmp_ids);
					 		$list[] = $record;
					 	}
					 }
				}
			}
			
			return $list;
		}
		
		function updateEmailNotificationRecord($data)
		{
			if(count($data) == 0){
				return false;
			}
			
			foreach($data as $key => $value){
				${$key} = $value;
			}
			
			$sql = "UPDATE DISCIPLINE_EMAIL_NOTIFICATION_RECORD SET SendStatus='$SendStatus', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."' 
					WHERE StudentID='$StudentID' AND RelatedRecordID='$RelatedRecordID' AND RecordType='$RecordType'";
			$success = $this->db_db_query($sql);
			$updated_row = $this->db_affected_rows(); 
			
			if($updated_row == 0){
				$sql = "INSERT INTO DISCIPLINE_EMAIL_NOTIFICATION_RECORD (StudentID,RelatedRecordID,RecordType,SendStatus,DateInput,DateModified,InputBy,ModifiedBy)
						 VALUES ('$StudentID','$RelatedRecordID','$RecordType','$SendStatus',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		## Buddhist Fat Ho Memorial College (Individual Discipline Record) [2014-0930-1221-53164]
		function updateStudentReportTargetLang($options){
			global $intranet_root, $PATH_WRT_ROOT, $intranet_session_language, $Lang, $ldiscipline;
		
			# POST
			$rankTarget = $options['rankTarget'];
			$selectYear = $options['selectYear']; 
			$targetLang = $options['targetLang']; 
			
			$insertAry = array();
			$successAry = array();
			
			if($rankTarget == 'student'){
				$studentIDAry = $options['studentID'];
			} 
			# Get Student ID
			else {
				if($rankTarget == 'form'){
					$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				}else if($rankTarget == 'class'){
					$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				}
				
				# Target Students
				$sql = "SELECT iu.UserID FROM INTRANET_USER as iu 
						LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
						LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
						LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
						WHERE yc.AcademicYearID = '".$selectYear."' 
						$yearIdCond $classIdCond 
						GROUP BY iu.UserID 
						ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";	
				$studentIDAry = $ldiscipline->returnVector($sql);
			}

			$numOfStudent = count($studentIDAry);
			for ($i=0; $i<$numOfStudent; $i++) {
				$studentID = $studentIDAry[$i];
				
				$sql = "Select RecordID From DISCIPLINE_STUDENT_REPORT_LANG Where AcademicYearID = '".$selectYear."' And StudentID = '".$studentID."'";
				$resultAry = $this->returnResultSet($sql);
				
				if (count($resultAry) > 0) {
					$recordID = $resultAry[0]['RecordID'];
					# Update existing records
					$sql = "Update DISCIPLINE_STUDENT_REPORT_LANG set DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' where RecordID = '".$recordID."'";
					$successAry['update'][$recordID] = $this->db_db_query($sql);
				}
				else 
				{
					$insertAry[] = " ('".$studentID."', '".$selectYear."', '".$targetLang."',  now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
				}
			}
			
			# Insert new records
			if (count($insertAry) > 0) {
				$sql = "Insert Into DISCIPLINE_STUDENT_REPORT_LANG (StudentID, AcademicYearID, RecordLang, DateInput, InputBy, DateModified, ModifiedBy) Values ".implode(', ', (array)$insertAry);
				$successAry['insert'] = $this->db_db_query($sql);
			}
			return !in_array(false, $successAry);	
		}
		
		function removeStudentReportTargetLang($recordIdAry) {
			$sql = "Delete From DISCIPLINE_STUDENT_REPORT_LANG Where RecordID IN ('".implode("','", (array)$recordIdAry)."') ";
			return $this->db_db_query($sql);
		}
		
		# [Jockey Club Government Secondary School] Start 
		function returnMeritStatInMonthlySummary($statAry){
			$count = 0;
			foreach((array)$statAry as $statContent){
				$count += intval($statContent["ProfileMeritCount"]);
			}
			return $count;
		}
		# [Jockey Club Government Secondary School] End 
		
		# [Tung Wah Group Of Hospitals Wong Fut Nam College] Start 
		function getStudentDetentionByDate($dataAry)
		{
			# POST data
			$studentType = $dataAry["targetType"];
			$classAry = $dataAry["rankTargetDetail"];
			$targetDate = $dataAry["textDate"];
			$academicYearID = $dataAry["selectYear"];
			
			# Cond
			$cond = "";
			$yearClassField = "1";
			if($studentType == "class"){
				$yearClassField = "yc.YearClassID";
				$cond .= " AND yc.YearClassID IN (".implode(",", (array)$classAry).")";
			}
			$cond .= " AND d.DetentionDate = '$targetDate' ";
			
			# Detention Students
			$sql = "SELECT 
						$yearClassField as YearClassID,
						yc.ClassTitleB5 as ClassTitle,
						ycu.ClassNumber, 
						s.StudentID,
						u.ChineseName AS StudentName,
						d.Location
					FROM DISCIPLINE_DETENTION_SESSION as d 
						INNER JOIN DISCIPLINE_DETENTION_STUDENT_SESSION as s ON (s.DetentionID = d.DetentionID) 
						INNER JOIN INTRANET_USER as u ON (s.StudentID = u.UserID)
						INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = u.UserID)
						INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$academicYearID')
						INNER JOIN YEAR as y ON (y.YearID = yc.YearID) 
					WHERE
						s.RecordStatus = 1 AND yc.AcademicYearID = '$academicYearID' $cond 
					ORDER BY
						yc.Sequence, ClassTitle, ycu.ClassNumber, u.UserID, d.DetentionDate";
			$records = $this->returnResultSet($sql);
			$record_count = count($records);
			if($record_count > 0)
				$records = BuildMultiKeyAssoc($records, array("YearClassID", "Location", "StudentID"));
			
			return $records;
		}
		
		function getStudentConductPunishmentSummaryData($dataAry)
		{
			global $ldiscipline, $sys_custom;
			
			# POST data
			//$format = $dataAry['format'];
			$selectYear = $dataAry['selectYear'];
			$selectSemester = $dataAry['selectSemester'];
			$textFromDate = $dataAry['textFromDate'];
			$textToDate = $dataAry['textToDate'];
			
			# Target Students
			$clsName = "yc.ClassTitleEN";	
			$sql = "SELECT 
						iu.UserID,
						yc.YearClassID,
						yc.ClassTitleB5 as ClassName,
						ycu.ClassNumber,
						CONCAT(
							IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
							iu.ChineseName
						) as StudentName
					FROM INTRANET_USER as iu 
						LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
						LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
						LEFT JOIN YEAR as y ON y.YearID = yc.YearID
					WHERE
						yc.AcademicYearID = '".$selectYear."' AND yc.YearClassID IN ('".implode("','", (array)$_POST['rankTargetDetail'])."')  
					GROUP BY
						iu.UserID 
					ORDER BY
						y.Sequence, yc.Sequence, ycu.ClassNumber";
			$students = $ldiscipline->returnResultSet($sql);
			$student_ids = Get_Array_By_Key($students, "UserID");
			$student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID", "UserID"));
			
			# Misconduct - Approved
			$sql = "SELECT
						r.RecordID,
						r.StudentID,
						cat.CategoryID,
						cat.Name,
						r.RecordType,
						DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
						r.YearTermID,
						SUM(1) AS GMRecordCount,
						r.UpgradedRecordID,
						IF(mr.RecordID IS NOT NULL, mr.ProfileMeritCount, '0') as UpgradeRecordCount
					FROM 
						DISCIPLINE_ACCU_RECORD as r
						INNER JOIN 
							DISCIPLINE_ACCU_CATEGORY as cat ON (cat.CategoryID = r.CategoryID)
						LEFT JOIN 
							DISCIPLINE_MERIT_RECORD as mr ON (r.UpgradedRecordID IS NOT NULL AND r.UpgradedRecordID = mr.RecordID AND 
																mr.MeritType = '".MISCONDUCT."' AND mr.ProfileMeritType = '-1' AND 
																mr.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND mr.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."')
					WHERE 
						r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.AcademicYearID = '$selectYear' AND 
			 			r.RecordDate >= '$textFromDate 00:00:00' AND r.RecordDate <= '$textToDate 23:59:59' AND 
						r.RecordType = '".MISCONDUCT."' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'
					GROUP BY 
						r.StudentID, r.RecordType, r.CategoryID, r.UpgradedRecordID";
			$gm_records = $ldiscipline->returnResultSet($sql);
			$gm_records = BuildMultiKeyAssoc((array)$gm_records, array("StudentID", "CategoryID", "UpgradedRecordID"));
			
			# Punishment - Approved and Released
			$sql = "SELECT 
						DISTINCT r.RecordID,
						r.StudentID,
						m.ItemCode,
						m.ItemName,
						r.ProfileMeritType,
						r.RecordDate,
						r.YearTermID,
						ROUND(r.ProfileMeritCount) AS MeritRecordCount,
						ar.CategoryID as UpgradedGMCat
					FROM 
						DISCIPLINE_MERIT_RECORD as r
						INNER JOIN DISCIPLINE_MERIT_ITEM as m ON (m.ItemID = r.ItemID)
						LEFT JOIN DISCIPLINE_ACCU_RECORD as ar ON (r.RecordID = ar.UpgradedRecordID)
					WHERE 
						r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.AcademicYearID = '$selectYear' AND 
						r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate' AND 
						r.MeritType = '".MISCONDUCT."' AND r.ProfileMeritType IN ('0', '-1', '-2', '-3') AND (
							ar.CategoryID IS NULL OR
							( ar.CategoryID IS NOT NULL AND ar.CategoryID != '".PRESET_CATEGORY_LATE."' AND ar.CategoryID != '".$sys_custom['eDiscipline']['BWWTC_WithoutSubmCatID']."' )
						) AND 
						r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
					ORDER BY
						RecordID";
			$ap_records = $ldiscipline->returnResultSet($sql);
			$ap_record_type = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "ProfileMeritType", "RecordID"));
			$ap_record_individual = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "RecordID"));
			
			# Build array
			$resultAry = array();
			$resultAry["StudentInfo"] = $student_ary;
			$resultAry["ConductRecord"] = $gm_records;
			$resultAry["PunishmentRecord"] = $ap_record_individual;
			$resultAry["PunishmentRecordType"] = $ap_record_type;
			return $resultAry;
		}
		
		function getStudentAllClassTeacherName($studentid="", $yearID="", $yearclassID="")
		{
			# Fields
			$academicYearID = ($yearID=="") ? Get_Current_Academic_Year_ID() : $yearID;
			$name_field = "USR.ChineseName";
			//$name_field = getNameFieldByLang('USR.');
						
			// Get Class Teacher by YearClassID
			if($yearclassID > 0)
			{
				$sql = "SELECT 
							DISTINCT $name_field
						FROM
							YEAR_CLASS yc
							LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID)
						 	LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID = yct.UserID)
						WHERE
							yc.YearClassID = '$yearclassID' AND yc.AcademicYearID = '$academicYearID' AND yct.UserID IS NOT NULL
						ORDER BY
							$name_field";
			}
			// Get Class Teacher by StudentID
			else
			{
				$sql = "SELECT 
							DISTINCT $name_field
						FROM 
							YEAR_CLASS_USER ycu
							LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$academicYearID')
							LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID)
							LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID = yct.UserID)
						WHERE 
							ycu.UserID = '$studentid' AND yct.UserID IS NOT NULL
						ORDER BY
							$name_field";
			}
			return $this->returnVector($sql);
		}
		# [Tung Wah Group Of Hospitals Wong Fut Nam College] End 
		
		// [HKSYC & IA Chan Nam Chong Memorial College] Start
		function getStudentUpgradedLateDemeritRecord($dataAry)
		{
			global $ldiscipline;
			
			# POST data
			//$format = $dataAry['format'];
			$selectClassList = $dataAry['rankTargetDetail'];
			$selectYear = $dataAry['selectYear'];
			$textFromDate = $dataAry['textFromDate'];
			$textToDate = $dataAry['textToDate'];
			
			# Target Students
			$StylePrefix = '<span class="tabletextrequire">';
			$StyleSuffix = '</span>';
			$sql = "SELECT 
						iu.UserID,
						yc.YearClassID,
						yc.ClassTitleB5 as ClassName,
						ycu.ClassNumber,
						CONCAT(
							IF(yc.ClassTitleB5 IS NULL OR yc.ClassTitleB5='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
							iu.ChineseName
						) as StudentName
					FROM INTRANET_USER as iu 
						LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = iu.UserID)
						LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$selectYear."')
						LEFT JOIN YEAR as y ON (y.YearID = yc.YearID)
					WHERE
						yc.AcademicYearID = '".$selectYear."' AND yc.YearClassID IN ('".implode("','", (array)$selectClassList)."')  
					GROUP BY
						iu.UserID 
					ORDER BY
						y.Sequence, yc.Sequence, ycu.ClassNumber";
			$student_ary = $ldiscipline->returnResultSet($sql);
			$student_ids = Get_Array_By_Key($student_ary, "UserID");
			$student_ary = BuildMultiKeyAssoc((array)$student_ary, array("YearClassID", "UserID"));
			
			# Punishment - Approved and Released
			$sql = "SELECT 
						mr.RecordID,
						mr.StudentID,
						DATE_FORMAT(mar.RecordDate,'%Y-%m-%d') as RecordDate,
						mar.RecordID as GMRecordID,
						mar.Remark,
						mar.CountNo
					FROM 
						DISCIPLINE_MERIT_RECORD as mr 
						INNER JOIN DISCIPLINE_ACCU_RECORD as mar ON (mr.RecordID = mar.UpgradedRecordID)
					WHERE 
						mr.StudentID IN ('".implode("','", (array)$student_ids)."') AND mr.AcademicYearID = '".$selectYear."' AND 
						mr.RecordDate >= '".$textFromDate."' AND mr.RecordDate <= '".$textToDate."' AND 
						mr.MeritType = '".MISCONDUCT."' AND mar.CategoryID = '".PRESET_CATEGORY_LATE."' AND 
						mr.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'
					ORDER BY
						mr.RecordDate, mar.RecordDate desc";
			$ap_records = $ldiscipline->returnResultSet($sql);
			$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "RecordID", "GMRecordID"));
			
			# Build array
			$resultAry = array();
			$resultAry["StudentInfo"] = $student_ary;
			$resultAry["PunishmentRecord"] = $ap_records;
			return $resultAry;
		}
		// [HKSYC & IA Chan Nam Chong Memorial College] End
		
		// [NT Heung Yee Kuk Tai Po District Secondary School] Start
		function getGeneratedNoticeLog($AcademicYearID, $SemesterID='', $StartDate='', $EndDate='', $LogID='', $forSelection=false, $checkOverlap=false, $lastRecords='')
		{
		    // Condition
		    $cond = "";
		    if($LogID != '') {
		        $cond .= " GenerateLogID = '$LogID' ";
		    }
		    else {
		        $cond .= " AcademicYearID = '$AcademicYearID' ";
		        if($SemesterID != '') {
		            $cond .= " AND SemesterID = '$SemesterID' ";
		        }
		        if($StartDate != '' && $EndDate != '')
		        {
    		        if($checkOverlap) {
    		            $cond .= " AND StartDate = '$StartDate' AND EndDate = '$EndDate' ";
    		        } else {
        		        $cond .= " AND StartDate >= '$StartDate' AND EndDate <= '$EndDate' ";
    		        }
    		    }
		    }
		    $cond .= " AND isDeleted = '0' ";
		    
		    // Fields & Ordering
		    $fields = $forSelection? " GenerateLogID, CONCAT(StartDate, '-', EndDate, ' (', InputDate, ')') as GenerateDetails" : " * ";
		    $order_by = $forSelection? ' StartDate, EndDate, InputDate ' : ' StartDate DESC, EndDate DESC, InputDate DESC ';
		    if($lastRecords != '' && $lastRecords > 0) {
		        $order_by = ' InputDate DESC ';
		        $limited = ' LIMIT '.$lastRecords;
		    }
		    
		    $sql = "SELECT $fields FROM DISCIPLINE_HYK_NOTICE_GENERATE_LOG
					    WHERE $cond
					ORDER BY $order_by
		            $limited ";
		    return $this->returnArray($sql);
		}
		
		function insertGeneratedNoticeLog($dataAry)
		{
		    $yearID = $dataAry["YearID"];
		    $termID = $dataAry["selectSemester"];
		    $startDate = $dataAry["StartDate"];
		    $endDate = $dataAry["EndDate"];
		    $noticeIDArr = $dataAry["NoticeID"];
// 		    $class = $dataAry["rankTargetDetail"];
		    
		    # INSERT INTO [DISCIPLINE_HYK_NOTICE_GENERATE_LOG]
		    $sql = "INSERT INTO DISCIPLINE_HYK_NOTICE_GENERATE_LOG 
                        (TargetType, TargetID, AcademicYearID, SemesterID, StartDate, EndDate, InputBy, InputDate)
					VALUES
					    ('class', '', '$yearID', '$termID', '$startDate', '$endDate', '".$_SESSION['UserID']."', NOW())";
		    $result = $this->db_db_query($sql);
		    $GenerateLogID = $this->db_insert_id();
		    
		    # INSERT INTO [DISCIPLINE_HYK_NOTICE]
		    $result2 = true;
		    $valueArr = array();
		    foreach((array)$noticeIDArr as $notice_id) {
		        $valueArr[] = " ('$GenerateLogID', '$notice_id', '".$_SESSION['UserID']."', NOW()) ";
		    }
		    if(!empty($valueArr))
		    {
    		    $sql = "INSERT INTO DISCIPLINE_HYK_NOTICE
                            (GenerateLogID, NoticeID, InputBy, InputDate)
    					VALUES 
                            ".implode(",", (array)$valueArr);
    		    $result2 = $this->db_db_query($sql);
		    }
		    
		    return ($result && $result2);
		}
		
		function removeGeneratedNoticeLog($dataAry)
		{
		    global $intranet_root;
		    
		    $yearID = $dataAry["YearID"];
		    $termID = $dataAry["selectSemester"];
		    $startDate = $dataAry["StartDate"];
		    $endDate = $dataAry["EndDate"];
// 		    $class = $dataAry["rankTargetDetail"];
// 		    $notice = $dataAry["NoticeID"];
		    
		    // Get overlapped Generation Log
		    $overlappedLogs = $this->getGeneratedNoticeLog($yearID, $termID, $startDate, $endDate, '', $forSelection=false, $checkOverlap=true);
		    if(!empty($overlappedLogs))
		    {
		        $LogIDArr = Get_Array_By_Key((array)$overlappedLogs, 'GenerateLogID');
		        
		        # Delete Generated Notice
		        $relatedNoticeIDArr = array();
		        foreach((array)$LogIDArr as $thisLogID) {
		            $thisLogNotice = $this->getGeneratedNoticeContent($thisLogID);
		            $thisLogNoticeIDArr = Get_Array_By_Key((array)$thisLogNotice, 'NoticeID');
		            $relatedNoticeIDArr = array_merge($relatedNoticeIDArr, $thisLogNoticeIDArr);
		        }
		        $relatedNoticeIDArr = array_values(array_unique(array_filter($relatedNoticeIDArr)));
		        if(!empty($relatedNoticeIDArr))
		        {
		            foreach((array)$relatedNoticeIDArr as $thisNoticeID) {
    		            include_once($intranet_root."/includes/libnotice.php");
    		            $lnotice = new libnotice($thisNoticeID);
    		            $lnotice->deleteNotice($thisNoticeID);
		            }
		        }
		        
		        # UPDATE [DISCIPLINE_HYK_NOTICE_GENERATE_LOG] - soft delete
		        $sql = "UPDATE DISCIPLINE_HYK_NOTICE_GENERATE_LOG
                            SET isDeleted = 1
    					WHERE
                            GenerateLogID IN ('".implode("', '", (array)$LogIDArr)."')";
                $this->db_db_query($sql);
		    }
		}
		
		function generateStudentPunishmentNotice($dataAry)
		{
		    global $intranet_root, $PATH_WRT_ROOT, $Lang, $sys_custom;
		    
		    include_once($intranet_root."/includes/libnotice.php");
		    include_once($intranet_root."/includes/libucc.php");
		    
		    $lnotice = new libnotice();
		    if ($lnotice->disabled) {
		        return false;
		    }
		    
		    # POST data
		    $yearID = $dataAry["YearID"];
		    $termID = $dataAry["selectSemester"];
		    $startDate = $dataAry["StartDate"];
		    $endDate = $dataAry["EndDate"];
		    $class = $dataAry["rankTargetDetail"];
		    $issueDate = $dataAry["IssueDate"];
		    if($issueDate != '' && !intranet_validateDate($issueDate)) {
		        $issueDate = '';
		    }
		    
		    # Special Handling for Issue Date display
		    $issueDateDisplay = '二零一_____年<u>&nbsp; &nbsp;&nbsp;</u>月<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>日';
		    if($issueDate != '')
		    {
		        list($year, $month, $day) = explode('-', $issueDate);
		        $chiYear = $this->GET_CHINESE_NUMBER_DATE($year, true);
		        $chiMonth = $Lang['eDiscipline']['Month2'][(int)$month];
		        $chiDay = $this->GET_CHINESE_NUMBER_DATE($day, true, true);
		        $issueDateDisplay = $chiYear.$Lang['eDiscipline']['IssueDateChi']['Year'].$chiMonth.$chiDay.$Lang['eDiscipline']['IssueDateChi']['Day'];
		    }
		    
		    # Special Handling for Whole Year
		    $isRangeCrossTerm = false;
		    $isWholeYear = $termID == "0";
		    if($isWholeYear)
		    {
		        $startTermInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
		        $endTermInfo = getAcademicYearInfoAndTermInfoByDate($endDate);
		        $startTermID = $startTermInfo[2];
		        $endTermID = $endTermInfo[2];
		        
		        $isRangeCrossTerm = $startTermID != $endTermID;
		        $termID = $isRangeCrossTerm ? "" : $endTermID;
		    }
		    
		    # Year - Date Range
		    $SQL_startdate = getStartDateOfAcademicYear($yearID, $termID);
		    $SQL_enddate = getEndDateOfAcademicYear($yearID, $termID);
		    $yearFromDate = date("Y-m-d", strtotime($SQL_startdate));
		    $yearToDate = date("Y-m-d", strtotime($SQL_enddate));
		    $targetTermStartDate = $yearFromDate;
		    if($isRangeCrossTerm) {
		        $targetTermStartDate = getStartDateOfAcademicYear($yearID, $endTermID);
		        $targetTermStartDate = date("Y-m-d", strtotime($targetTermStartDate));
		    }
		    
		    # Target End Date
		    $endDateField = ($yearToDate > $endDate)? $endDate : $yearToDate;
		    
		    # Target Class
		    $class = $this->getRankClassList("", $yearID);
		    $class = Get_Array_By_Key($class, 'YearClassID');
		    $classIdCond = " yc.YearClassID IN ('".implode("','",(array)$class)."') ";
		    
		    # Target Term
		    $termCond = $termID? " AND r.YearTermID = '".$termID."' " : "";
		    
		    # Target Student
		    $sql = "SELECT
    					iu.UserID,
    					yc.YearClassID,
    					yc.ClassTitleEN as ClassName,
    					ycu.ClassNumber,
    					CONCAT(
    						IF(yc.ClassTitleEN IS NULL OR yc.ClassTitleEN='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".'<span class="tabletextrequire">'."^</span>', ''),
    						iu.ChineseName
						) as StudentName
					FROM INTRANET_USER as iu
						LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
						LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$yearID')
						LEFT JOIN YEAR as y ON y.YearID = yc.YearID
					WHERE
						yc.AcademicYearID = '".$yearID."' AND $classIdCond
					GROUP BY
						iu.UserID
					ORDER BY
						y.Sequence, yc.Sequence, ycu.ClassNumber";
		    $students = $this->returnResultSet($sql);
		    $student_ids = Get_Array_By_Key($students, "UserID");
		    $student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID", "UserID"));
		    $student_count = count($students);
		    
		    # Misconduct - Approved
		    $sql = "SELECT
            		    r.RecordID,
            		    r.StudentID,
            		    DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
            		    r.YearTermID,
            		    r.RecordType,
            		    cat.CategoryID,
            		    cat.Name,
            		    IF(r.GMConductScoreChange IS NULL, 0, r.GMConductScoreChange) as GMConductScoreChange,
            		    IF(r.RecordDate >= '$startDate 00:00:00' AND r.RecordDate <= '$endDate 23:59:59', 1, 0) as InDateRange,
            		    IF(r.RecordDate >= '$targetTermStartDate 00:00:00' AND r.RecordDate <= '$endDate 23:59:59', 1, 0) as TermDateRange
        		    FROM
            		    DISCIPLINE_ACCU_RECORD as r
            		    INNER JOIN DISCIPLINE_ACCU_CATEGORY as cat ON cat.CategoryID = r.CategoryID
        		    WHERE
            		    r.AcademicYearID = '$yearID' ".$termCond." AND
            		    r.RecordDate >= '$yearFromDate 00:00:00' AND r.RecordDate <= '$endDateField 23:59:59' AND
            		    r.StudentID IN ('".implode("','", (array)$student_ids)."') AND
    					r.RecordType = '".MISCONDUCT."' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'";
		    $gm_records = $this->returnResultSet($sql);
		    $gm_records = BuildMultiKeyAssoc((array)$gm_records, array("StudentID", "RecordID"));
		    
		    # Awards / Punishment - Approved and Released
		    // TEMP - not consider "ProfileMeritType = 0 > Conduct Deduct"
		    $sql = "SELECT
            		    r.RecordID,
            		    r.StudentID,
            		    DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate,
            		    r.YearTermID,
            		    r.ProfileMeritType,
            		    IF(r.ProfileMeritType < 0 AND r.ProfileMeritType > -999, 1, 0) as ConductRelated,
            		    r.ProfileMeritCount,
            		    m.ItemCode,
            		    m.ItemName,
            		    IF(r.ConductScoreChange IS NULL, 0, r.ConductScoreChange) as ConductScoreChange,
            		    IF(r.RecordDate >= '$startDate' AND r.RecordDate <= '$endDate', 1, 0) as InDateRange,
            		    IF(r.RecordDate >= '$targetTermStartDate' AND r.RecordDate <= '$endDate', 1, 0) as TermDateRange
        		    FROM
        		        DISCIPLINE_MERIT_RECORD as r
        		        INNER JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
        		    WHERE
            		    r.AcademicYearID = '$yearID' ".$termCond." AND
            		    r.RecordDate >= '$yearFromDate' AND r.RecordDate <= '$endDateField' AND
            		    r.StudentID IN ('".implode("','", (array)$student_ids)."') AND
    					r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'";
		    $ap_records = $this->returnResultSet($sql);
		    $ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "ConductRelated", "RecordID"));
		    
		    # Notice Template
		    $template_info = $this->RETRIEVE_NOTICE_TEMPLATE_INFO($sys_custom['eDiscipline']['MonthlyNoticeTemplateID']);
		    
		    // loop class
		    $this_count = 0;
		    $resultArr = array();
		    $noticeIDArr = array();
		    foreach((array)$class as $current_classid)
		    {
		        // loop student
		        $student_list = $student_ary[$current_classid];
		        if(count($student_list) > 0)
		        {
		            foreach((array)$student_list as $currentStudentID => $currentStudent)
		            {
		                $ap_record_id = '';
		                $gm_record_id = '';
		                
		                $totalDeduct = 0;
		                $thisRangeDeduct = 0;
		                $thisTargetTermDeduct = 0;
		                
		                # AP records related to Conduct Mark
		                $this_ap_conduct_score = $ap_records[$currentStudentID][1];
		                foreach((array)$this_ap_conduct_score as $record_id => $this_ap)
		                {
		                    if($this_ap["ConductScoreChange"] < 0)
		                    {
		                        $totalDeduct += abs($this_ap["ConductScoreChange"]);
		                        if($this_ap["InDateRange"] == 1)
		                        {
		                            $thisRangeDeduct += abs($this_ap["ConductScoreChange"]);
		                        }
		                        if($isRangeCrossTerm && $this_ap["TermDateRange"] == 1)
		                        {
		                            $thisTargetTermDeduct += abs($this_ap["ConductScoreChange"]);
		                        }
		                        
		                        if($this_ap["InDateRange"] == 1 || ($isRangeCrossTerm && $this_ap["TermDateRange"] == 1)) {
		                            $ap_record_id = $record_id; 
		                        }
		                    }
		                }
		                
		                # GM records related to Conduct Mark
		                $this_gm_conduct_score = $gm_records[$currentStudentID];
		                foreach((array)$this_gm_conduct_score as $record_id => $this_gm)
		                {
		                    if($this_gm["GMConductScoreChange"] < 0)
		                    {
		                        $totalDeduct += abs($this_gm["GMConductScoreChange"]);
		                        if($this_gm["InDateRange"] == 1)
		                        {
		                            $thisRangeDeduct += abs($this_gm["GMConductScoreChange"]);
		                        }
		                        if($isRangeCrossTerm && $this_gm["TermDateRange"] == 1)
		                        {
		                            $thisTargetTermDeduct += abs($this_gm["GMConductScoreChange"]);
		                        }
		                        
		                        if($this_gm["InDateRange"] == 1 || ($isRangeCrossTerm && $this_gm["TermDateRange"] == 1)) {
		                            $gm_record_id = $record_id;
		                        }
		                    }
		                }
		                
		                # Calculate Demerit Number using Conduct Mark
		                $conductMarkData = array();
		                $totalConductMarkData = array();
		                if($totalDeduct > 0)
		                {
		                    // Special Case for Whole Year  ( ( 1st Total - Previous ) + ( 2nd Total ) )
		                    if($isRangeCrossTerm)
		                    {
		                        $beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
		                        $firstTermTotalDeduct = $totalDeduct - $thisTargetTermDeduct;
		                        $firstTermRangeDeduct = (floor($firstTermTotalDeduct / 8) - floor($beforeRangeDeduct / 8)) * 8;
		                        $thisCrossRangeDeduct = $firstTermRangeDeduct + $thisTargetTermDeduct;
		                        
		                        $conductMarkData["ConductMark"] = $thisTargetTermDeduct > 0 ? ($thisTargetTermDeduct % 8) : 0;
		                        $conductMarkData[-1] = floor($thisCrossRangeDeduct / 8) % 3;
		                        $conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0;
		                        $conductMarkData[-2] = floor($thisCrossRangeDeduct / 24) % 3;
		                        $conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
		                        $conductMarkData[-3] = floor($thisCrossRangeDeduct / 72) % 3;
		                        $conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
		                        
		                        $totalDeduct = $thisTargetTermDeduct;
		                    }
		                    // Normal Case ( Total - Previous )
		                    else
		                    {
		                        $beforeRangeDeduct = $totalDeduct - $thisRangeDeduct;
		                        $conductMarkData["ConductMark"] = $thisRangeDeduct > 0? ($totalDeduct % 8) : 0;
		                        $conductMarkData[-1] = (floor($totalDeduct / 8) % 3) - (floor($beforeRangeDeduct / 8) % 3);
		                        $conductMarkData[-1] = $conductMarkData[-1] >= 0 ? $conductMarkData[-1] : 0;
		                        $conductMarkData[-2] = (floor($totalDeduct / 24) % 3) - (floor($beforeRangeDeduct / 24) % 3);
		                        $conductMarkData[-2] = $conductMarkData[-2] >= 0 ? $conductMarkData[-2] : 0;
		                        $conductMarkData[-3] = floor($totalDeduct / 72) - floor($beforeRangeDeduct / 72);
		                        $conductMarkData[-3] = $conductMarkData[-3] >= 0 ? $conductMarkData[-3] : 0;
		                        
		                        $totalConductMarkData[-1] = (floor($totalDeduct / 8) % 3);
		                        $totalConductMarkData[-2] = (floor($totalDeduct / 24) % 3);
		                        $totalConductMarkData[-3] = (floor($totalDeduct / 72));
		                    }
		                }
		                
		                # Have Record
		                $withDemeritRecord = $conductMarkData[-1] || $conductMarkData[-2] || $conductMarkData[-3];
		                if(!$withDemeritRecord) {
		                    continue;
		                }
		                
		                # Notice Data
		                $SpecialData = array();
		                if($ap_record_id > 0) {
		                    $SpecialData['MeritRecordID'] = $ap_record_id;
		                } else if ($gm_record_id > 0) {
		                    $SpecialData['ConductRecordID'] = $gm_record_id;
		                }
		                $template_data = $this->TEMPLATE_VARIABLE_CONVERSION($template_info[0]['CategoryID'], $currentStudentID, $SpecialData, $TextAdditionalInfo='', $includeReferenceNo=1);
		                $template_data['BlackMarkNum'] = $totalConductMarkData[-1]? $totalConductMarkData[-1] : "---";
		                $template_data['MinorDemeritNum'] = $totalConductMarkData[-2]? $totalConductMarkData[-2] : "---";
		                $template_data['MajorDemeritNum'] = $totalConductMarkData[-3]? $totalConductMarkData[-3] : "---";
		                $template_data['IssueDateChi'] = $issueDateDisplay;
		                $UserName = $this->getUserNameByID($_SESSION['UserID']);
		                
		                # Notice Settings
		                $Module = $this->Module;
		                $NoticeNumber = time().'_'.$this_count;
		                $TemplateID = $sys_custom['eDiscipline']['MonthlyNoticeTemplateID'];
		                $Variable = $template_data;
		                $IssueUserID = $_SESSION['UserID'];
		                $IssueUserName = $UserName[0];
		                $TargetRecipientType = "U";
		                $RecipientID = array($currentStudentID);
		                $RecordType = NOTICE_TO_SOME_STUDENTS;
		                
		                # Send Notice
		                $lc = new libucc();
                        $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                        // [2020-1009-1753-46096] delay notice start date & end date
                        if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                        {
                            $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                            $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                            $lc->setDateStart($DateStart);

                            if ($lc->defaultDisNumDays > 0) {
                                $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                                $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                                $lc->setDateEnd($DateEnd);
                            }
                        }

                        $NoticeID = $lc->sendNotice($emailNotify=0);
                        
                        # Suspend Notice
                        if($sys_custom['eDiscipline']['NotReleaseNotice'] && $NoticeID > 0)
                        {
                            include_once($PATH_WRT_ROOT."includes/libnotice.php");
                            $lnotice = new libnotice();
                            $lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
                        }
                        
                        $resultArr[] = $NoticeID? true : false;
                        if($NoticeID) {
                            $noticeIDArr[] = $NoticeID;
                        }
		                $this_count++;
	                }
		        }
		    }
		    
		    # Insert Generate Log
		    if(!in_array(false, $resultArr))
		    {
		        $this->removeGeneratedNoticeLog($dataAry);
		        
		        $dataAry['NoticeID'] = $noticeIDArr;
		        $this->insertGeneratedNoticeLog($dataAry);
		    }
		}
		
		function getGeneratedNoticeContent($log_id, $user_ids='')
		{
		    $cond = '';
		    if(!empty($user_ids))
		    {
    		    foreach((array)$user_ids as $this_index => $this_user_id) {
    		        $user_ids[$this_index] = 'U'.$this_user_id;
    		    }
    		    $cond .= " AND nt.RecipientID IN ('".implode("', '", (array)$user_ids)."')";
		    }
		    
		    $sql = "SELECT
                        nt.NoticeID, SUBSTRING(nt.RecipientID, 2) as RecipientID, nt.Description
                    FROM 
                        DISCIPLINE_HYK_NOTICE hykn
                        INNER JOIN INTRANET_NOTICE nt ON (hykn.NoticeID = nt.NoticeID)
                    WHERE
                        hykn.GenerateLogID = '$log_id' AND 
                        nt.Module = '".$this->Module."' AND nt.isModule = 1 
                        $cond ";
            return $this->returnArray($sql);
		}
		
		function getDemeritLinkedNoticeContent($notice_ids)
		{
		    $sql = "SELECT
                        NoticeID, SUBSTRING(RecipientID, 2) as RecipientID, DateStart, DateEnd, Description
                    FROM
                        INTRANET_NOTICE
                    WHERE
                        NoticeID IN ('".implode("', '", (array)$notice_ids)."') AND 
                        Module = '".$this->Module."' AND isModule = 1 
                    ORDER BY
                        DateStart, NoticeID";
		    return $this->returnArray($sql);
		}
		// [NT Heung Yee Kuk Tai Po District Secondary School] End
	}
}
?>