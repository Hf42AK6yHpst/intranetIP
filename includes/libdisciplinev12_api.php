<?php
// Modifying by : 
/*
 * 2015-11-13 (Carlos): Modified getStudentInDetentionSessionByID(), only get active detention student session records. 
 */

include_once("libaccessright.php");

define("LIBDISCIPLINEv12_api_DEFINED",true);

class libdisciplinev12_api extends libaccessright
{
	var $Module;
	
	function libstaffattend3_api() {
		parent::libaccessright();
		$this->Module = "DISCIPLINE";	
	}
	
	function getDetentionDetailByDetentionDate($date="")
	{
		$conds = ($date=="") ? " WHERE DetentionDate=CURDATE()" : " WHERE DetentionDate='$date'";
		
		$sql = "SELECT DetentionID, DetentionDate, StartTime, EndTime, Location FROM DISCIPLINE_DETENTION_SESSION $conds ";
		return $this->returnArray($sql, 5);
	} 
	
	function getStudentInDetentionSessionByID($detentionID="")
	{
		if($detentionID!="") {
			$sql = "SELECT SES.StudentID, SES.AttendanceStatus, USR.CardID, USR.EnglishName, USR.ChineseName, USR.ClassName, USR.ClassNumber FROM DISCIPLINE_DETENTION_STUDENT_SESSION SES LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=SES.StudentID) WHERE SES.DetentionID='$detentionID' AND SES.RecordStatus='1'";
			return $this->returnArray($sql,7);
		}
	}		
	
	function updateDetentionByRecordID($ParDetentionArr=array(), $ParRecordID)
	{
		global $UserID;
		
		$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET ";
		
		foreach($ParDetentionArr as $field=>$value)
			$sql .= $field . "=". $value.", ";
		
		$sql .= "DateModified=NOW() ";
		$sql .= "WHERE RecordID=$ParRecordID";
		return $this->db_db_query($sql);
	}
	
	function updateDetentionBySessionIDAndStudentID($ParDetentionArr=array(), $detentionID="", $studentID="")
	{
		global $UserID;
		
		$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET ";
		
		foreach($ParDetentionArr as $field=>$value)
			$sql .= $field . "=". $value.", ";
		
		$sql .= "DateModified=NOW(), ModifiedBy='$UserID' ";
		$sql .= "WHERE DetentionID='$detentionID' AND StudentID='$studentID'";
		return $this->db_db_query($sql);
	}
	
	
	function getDetentionSessionPIC($detentionID="")
	{
		if($detentionID!="") {
			$sql = "SELECT ChineseName, EnglishName FROM DISCIPLINE_DETENTION_SESSION_PIC pic LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=pic.UserID) WHERE pic.DetentionID=$detentionID";	
			$result = $this->returnArray($sql,2);
			return $result;
		}
			
	}
	
	
}

?>