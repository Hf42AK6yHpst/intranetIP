<?php
# using : 

/********************** Change Log ***********************
#
#   Date    :   2020-10-19 (Cameron)
#               show HandinRequired homework by adding filter condition HandinRequired=1 to exportDailyHomeworkList()
#
#   Date    :   2020-10-14 (Cameron)
#               by applying getNameFieldByLang2 to show EnglishName if there's no ChineseName in exportDailyHomeworkList(), getCustomizedStatusReport(), getNotSubmitList(),
#               getNotSubmitListPrint(), getNotSubmitListPrint_Format2(), getSupplementaryList(), getSupplementaryListPrint()
#
#   Date    :   2020-10-09 (Cameron)
#               add function exportDailyHomeworkList(), getCustomizedStatusReport()
#
#   Date    :   2020-10-08 (Cameron)
#               modify getNotSubmitList(), add case targetID=4 (form) [case #U176404]
#
#   Date    :   2020-09-28 (Bill)   [2020-0923-1234-59235]
#               modified getAllClassesInvolvedByTeacherID(), getClassesInvolvedBySubjectGroupTeacherID(), to return class & class level sequence
#
#   Date    :   2020-09-22 (Cameron)
#               - add function getReportHomeworkListTabs() [case #U176404]
#               - move sql part to getHomeworkListSql() in weeklyHomeworkList()
#
#	Date	:	2019-12-12 (Philips)
#				modified exportHomeworkListRecord(), added showHID parameter
#
#   Date    :   2019-07-15 (Bill)   [2019-0715-0921-29066]
#               modified GET_MODULE_OBJ_ARR(), to fix php error message display
#
#   Date    :   2019-05-16 (Bill)
#               modified getStudyingSubjectList(), to prevent SQL Injection
#
#   Date    :   2019-05-14 (Bill)
#               modified insertHomeworkListRecord(), displayIndex(), to prevent SQL Injection
#
#   Date    :   2018-03-08 (Bill)
#               modified displayAttachment_IMG(), to support token parm in attachment url
#
#   Date    :   2018-05-18 (Frankie)
#               Modified for #Buddhist Wong Wan Tin College - Case F134948
#               getHomeworkListTabs()
#               getStudentsByParam()
#               getLatestConfirmationDate()
#               checkTransferOrNot()
#               checkUpdateIsMoreThanOneDay()
#               AddCancelMisconductLog()
#               cancelMisconductFromDiscipline()
# 
#   Date    :   2018-05-14 (Anna)
#               modify handinList() - change $name_field to add * before leave student, added StudentStatus, LeaveStudent #138644
#
#   Date    :   2018-02-05 (Frankie)
#               modify handinList() for display sub handin status
#
#	Date	:	2017-11-22 (Frankie)
#				modified getHomeListByArgs(), getStudentsByParam() for retrieve Homework with All Terms of Current Year
#
#	Date	:	2017-10-12 (Bill)	[2017-1010-1136-20235]
#				modified getTeachingSubjectList(), weeklyHomeworkList(), monthlyHomeworkList(), to fix display not related subjects / homeworks
#
#	Date	:	2017-10-12 (Bill)	[2017-0928-1709-10235]
#				modified handinList(), retrieveStudentListByYearClassID()
#				added IsTeacherTeachingTargetSubjectGroup(), to ensure Class Teacher can view own class student only
#
#	Date	:	2017-06-30 (Carlos) $sys_custom['LivingHomeopathy'] Added getHomeworkHandinListRecords($map)
#
#	Date	:	2017-05-12 (Frankie) [C116951]
#				modify printHomeworkStatistics() -  Add YearTerm to Stat by Subject
#
#	Date	:	2017-03-17 (Frankie)	[C100917]
#				added cancelMisconductFromDiscipline to update Hand-in cancel status from Discipline
#				added AddCancelMisconductLog to store the log record for Cancel miscount
#
#	Date	:	2017-01-11 (Biil)	[DM#3131]
#				modified getNotSubmitListPrint_Format2(), to remove php error message
#
#	Date	:	2016-12-22 (Frankie)
#				Add getAllHandInStatus() and setAllHandInStatus(), getStudentsByParam() for managing Hand-in Status
#				Modified GET_MODULE_OBJ_ARR(), viewSupplementaryDetail(), printSupplementaryDetail(), getSupplementaryList() and Get_Handin_Status() for managing Hand-in Status
#
#	Date	:	2016-08-09 (Bill)	[2016-0519-1005-58207]
#				modified handinList(), to fix Non-teaching Staff and Viewer Group Member cannot view Hand-in List status
#
#	Date	:	2016-06-07 (Ivan) ip.2.5.7.7.1
#				updated GET_MODULE_OBJ_ARR(), show weekly diary and newspaper cutting only if student app enabled
#
#	Date	:	2016-05-03 (Tiffany)
#				add getClassName()
#
#	Date	:	2016-04-18 (Tiffany)
#				add Weekly Diary and Newspaper Cutting to the management
#
#	Date	:	2016-03-09 (Bill)	[2016-0309-1012-23066]
#				modified insertHomeworkListRecord(), prevent assign incorrect subjectid when insert homework
#
#	Date	:	2016-01-11 (Bill)	[DM#2934]
#				modified weeklyHomeworkList(), exportWeeklyHomeworkList(), parent (not admin) cannot view future homework
#
#	Date	:	2015-12-31 (Cameron)
#				cast $csv_user_id as array in checkSubjectLeaderImportError() to avoid error when import subject leader
#
#	Date	:	2015-08-03 (Bill) [2015-0730-1036-32207]
#				modified getNotSubmitList(), remove due dete condition for subject group, allow searching homework before due date
#
#	Date	:	2015-03-11 (Bill)
#				modified printHomeworkReport(), to modify column title
#
#	Date	:	2015-02-12 (Bill) [2015-0203-1618-41066]
#				modified printHomeworkReport(), exportHomeworkReport(), to add column teacher
#
#   Date    :   2014-09-17 (Tiffany)
#               add displayAttachment_IMG() to show img
#
#	Date	:	2014-07-29 (YatWoon)
#				modified handinList(), add checking for subject leader [Case#T64460]
#				deloy: ip.2.5.5.8.1
#
#	Date	:	2014-06-20 (YatWoon)
#				modified Get_Handin_Status(), add flag checking for status "Supplementary" [Case#C62721]
#
#	Date	:	2014-01-22 (Yuen)
#				implemented notice for no submission

#	Date	:	2013-12-06 (YatWoon)
#				modified getNotSubmitList(), viewNotSubmitDetail(), getNotSubmitListPrint(), getNotSubmitListPrint_Format2(), remove "include due date"
#
#	Date	:	2013-10-18 (Ivan) [2013-1016-1517-25156]
#				modified getTeachingSubjectGroupList() to break the long sql statement to separate small sql statement
#
#	Date	:	2013-09-03 (Ivan) [2013-0903-0923-30066]
#				modified insertSubjectLeaderRecord() to get student from INTRANET_USER only. Otherwise, alumni user with the same class name and class nubmer will be searched
#
#	Date	: 	2013-03-19 (Ivan)
#				modified getAllClassesInvolvedByTeacherID(), performance tunning of SQL
#
#	Date	:	2012-12-05 (YatWoon)
#				modified getTeachingSubjectList(), add $own condition checking
#
#	Date	:	2012-10-18 (YatWoon)
#				modified getTeachingSubjectGroupList(), add $classID condintion checking [Case#2012-1009-1756-25132]
#
#	Date	:	2012-07-18 (Henry Chow)
#				modified displayAttachment(), encrypt the url of attachment
#
#	Date	:	2012-03-27 (Henry Chow)
#				modified getNotSubmitList(), getNotSubmitListPrint(), getNotSubmitListPrint_Format2(), only return homework list (total amount) which status of student is approved
#
#	Date	:	2012-03-26 (Henry Chow)
#				modified viewNotSubmitDetail(), only return student list which status of student is approved
#
#	Date	:	2012-02-01 (Henry Chow)
#				modified weeklyHomeworkList(), monthlyHomeworkList(), exportWeeklyHomeworkList(), exportMonthlyHomeworkList()
#				revise the SQL performance of teaching staff (use INNER JOIN instead of LEFT OUTER JOIN)
#
#	Date	:	2011-12-05 (Henry Chow)
#				added getSupplementaryList(), getNotSubmitList(), getSupplementaryListPrint(), viewSupplementaryDetail(), printSupplementaryDetail()
#				for "Supplementary" status [CRM Ref. No. : 2011-0914-1653-05073]
#
#	Date	:	2011-11-10 (YatWoon)
#				update displayAttachment(), changed to use download_attachment.php to download the attachment.
#
#	Date	:	2011-10-11 (Henry Chow)
#				modified getNotSubmitList(), getNotSubmitListPrint(), getNotSubmitListPrint_Format2(), printNotSubmitDetail(), viewNotSubmitDetail()
#				display Today's homework in "Reports > Not Submit List"
#
#	Date	:	2011-10-04 (Henry Chow)
#				modified handinList(), return "DueDate" field
#
#	Date	:	2011-10-04 (Henry Chow)
#				modified printHomeworkStatistics() & exportHomeworkStatistics(), order "Subject Group" as 2nd order field
#
#	Date	:	2011-09-22 (Henry Chow)
#				modified isViewOnly(), revise the checking on class teacher (cater the option $this->OnlyCanEditDeleteOwn)
#
#	Date	:	2011-09-15 (Henry Chow)
#				modified checkImportError(), revise the validation of mandatory field
#
#	Date	:	2011-09-15 (Henry Chow)
#				modified displayIndex(), returnSmallGroup & added viewerView(),
#				display all homework for viewer group member
#
#	Date	:	20110901 (Henry Chow)
#				modified monthlyHomeworkList(), parent / student fail to see homework in monthly list
#
#	Date	:	20110805 (Henry Chow)
#				modified getTeachingSubjectList(), cater the parameter "$this->ClassTeacherCanViewHomeworkOnly" while returning subject list
#
#	Date	:	20110805 (Henry Chow)
#				added getClassesInvolvedBySubjectGroupTeacherID(), return Class Array by teacher ID
#
#	Date	:	20110802 (Henry Chow)
#				modified getTeachingSubjectGroupList(), Class teacher can only view the subject groups of his/her own class
#
#	Date	:	20110621 (Marcus)
#				modified checkImportError, insertHomeworkListRecord, RetrieveSubjectGroupTeacherLogin to cater same subject group in different term
#				added Get_Subject_Group_Corresponding_Classes_Class_Teacher to sync 2 sql in  checkImportError for better maintainance
#
#	Date	:	2011-06-07 (Henry Chow)
#				new isViewOnly() & $ViewOnly, check whether current user only has "View" right
#
#	Date	:	2011-06-02 (Henry Chow)
#				apply isViewerGroupMember() to left menu
#
#	Date	:	2011-05-04 (Henry Chow)
#				added isViewerGroupMember(), check whether user is in "Viewer Group" or not
#
#	Date	:	2011-03-31 (Henry Chow)
#				added removeViewerGroupMember(), removeViewerGroupMember(), removeViewerGroupMember()
#				management of "Viewer Group"
#
#
#	Date	:	2011-03-31 (Henry Chow)
#				modified getTeachingSubjectGroupList(), display the subject group according to the linkage between SUBJECT_TERM & SUBJECT_TERM_CLASS (stc1)
#
#	Date	:	2011-03-31 (Henry Chow)
#				added returnSmallGroup(), special handling on "Admin" account on display homework in portal
#
#	Date	:	2011-03-30 (Henry Chow)
#				added getStudyingSubjectGroupListByLeader(), display homework for subjet leader in index.php
#
#	Date	:	2011-03-23 (Henry Chow)
#				modified getTeachingSubjectGroupList(), performance tuning
#
#	Date	:	2011-03-15 (Henry Chow)
#				modified getNotSubmitListPrint_Format2(), getNotSubmitListPrint()
#				retrieve homework in current academic year if $YearTermID is null
#
#	Date	:	2011-02-15 (Henry Chow)
#				modified checkImportError(),
#				1) allow to import homework which start date already passed (homework maybe in previous semester)
#				2) validate date format of start date & due date
#
#	Date	:	2011-01-21 (Henry Chow)
#				add getHomeworkSubmitStatusByHomeworkID(), getStudentListBySubjectGroupID(), return "Submit homework" status of HomeworkID
#
#	Date	:	2011-01-20 (Henry Chow)
#				add exportHomeworkCollectionList(), export "Collection List"
#
#	Date	:	2011-01-17 (Henry Chow)
#				modified getStudyingSubjectGroupList(), displayIndex(), teacherview(), studentview(), parentview()
#				allow to display the homework of previous semester which due date not yet passed
#
#	Date	:	2011-01-05 (Henry Chow)
#				add retrieveStudentListByYearClassID(), retrieveSubjectGroupByStudentID(), getGroupMemberByGroupIDAndClassID(), getSubjectLeaderInfoByGroupID(), getSubjectLeaderByGroupID()
#				for "Subject Leader > View Subject Leader by Class"
#
#	Date	:	2010-12-21 (Henry Chow)
#				modified checkImportError(), Class Teacher / Subject Leader also can import homework
#
#	Date	:	2010-12-21 (Henry Chow)
#				modified insertHomeworkListRecord(), retrieve "SubjectID" base on the SubjectGroupID
#
#	Date	:	2010-12-14 (Henry Chow)
#				modified checkImportError(), add IS_ADMIN()
#				allow ADMIN to import homework
#
#	Date	:	2010-12-01 (Henry Chow)
#				modified checkImportError(), insertHomeworkListRecord()
#				remove the column of "Subject Code" in Import file
#
#	Date	:	2010-12-01 (Henry Chow)
#				modified exportHomeworkListRecord(), retrieve 2 more columns for homework export
#
#	Date	:	2010-11-18 (YatWoon)
#				update printHomeworkStatistics(), exportHomeworkStatistics(), add "Total Workload" data
#
#	Date	:	2010-11-01 (Henry Chow)
#				update checkImportError() and added getAllSubjectCodeOnly(), return checking on "Subject Code" when importing homework
#
#	Date	:	2010-10-28 (YatWoon)
#				update insertHomeworkListRecord(), add CreatedBy field
#
#	Date	:	2010-10-26 (Henry Chow)
#	Detail	:	update checkImportError(), revise the checking on Subject & Subject Code
#
#	Date	:	2010-10-22 (Henry Chow)
#	Detail	:	update checkImportError(), checking the control on $this->useHomeworkCollect
#
#	Date	:	2010-10-06 (Henry Chow)
#	Detail	:	update checkSubjectLeaderImportError(), compare imported "SubjectGroupCode" without changing to HTML code ()
#
#	Date	:	2010-09-29 (Henry Chow)
#	Detail	:	update printHomeworkStatistics() & exportHomeworkStatistics(), revise the checking of $classID (display homework "statistics" of particular class)
#
#	Date	:	2010-09-28 (Henry Chow)
#	Detail	:	update getNotSubmitListPrint_Format2(), export "Homework not submitted" report in "Format 2"
#
#	Date	:	2010-09-13 (Henry Chow)
#	Detail	:	update exportHomeworkReport(), printHomeworkReport(), display column "Teacher/Subject Leader" in "eHomework > Reports > Homework Report"
#
#	Date	:	2010-09-13 (Henry Chow)
#	Detail	:	update getTeachingSubjectGroupList() & getTeachingSubjectList, cannot add homework to subject group without student
#
#	Date	:	2010-09-09 (Henry Chow)
#	Detail	:	update insertHomeworkListRecord(), add single quotation mark in SQL
#
#	Date	:	2010-09-09 (Henry Chow)
#	Detail	:	update monthlyHomeworkList() & exportMonthlyHomeworkList(), "eService > Homework List > Monthly Homework List"
#
#	Date	:	2010-09-08 (Henry Chow)
#	Detail	:	update getAllClassesInvolvedByTeacherID() & getTeachingSubjectList(), display "Class" list for non-admin teaching staff even he/she is not the class teacher
#
#	Date	:	2010-09-06 (Henry Chow)
#	Detail	:	update getTeachingSubjectGroupList(), cater "$clsConds" if null
#
#	Date	:	2010-09-01 (Henry Chow)
#	Detail	:	update getTeachingSubjectList(), SQL tuning on non-admin Staff
#
#	Date	:	2010-08-09 (Henry Chow)
#	Detail	:	update getNotSubmitList(), getNotSubmitListPrint(), viewNotSubmitDetail(), add criteria of "order by" & "group by"
#
#	Date	:	2010-08-04 (Henry Chow)
#	Detail	:	update getNotSubmitList(), getNotSubmitListPrint(), add criteria of "Times" (not submit count)
#
#	Date	:	2010-05-17 (Henry Chow)
#	Detail	:	update getNotSubmitList(), viewNotSubmitDetail() & getNotSubmitListPrint(), count "yearTermID" as optional criteria
#
#	Date	:	2010-05-14 (Henry Chow)
#	Detail	:	update exportHomeworkListRecord(), add 1 column "PIC" in export file
#
#	Date	:	2010-05-10 (YatWoon)
#	Detail	:	update GET_MODULE_OBJ_ARR(), update the logo link
#
#	Date	:	2010-05-07 (Henry)
#	Detail	:	modified getTeachingSubjectList(), getTeachingSubjectGroupList(), display "Subject" & "Subject Group" selection menus base on "Class" selection of admin user
#
#	Date	:	2010-05-07 (Henry)
#	Detail	:	modified getTeachingSubjectGroupList(), display homework for class teacher (view his/her own class)
#
#	Date	:	2010-05-03 (Henry)
#	Detail	:	modified getAllClassesInvolvedByTeacherID(), getTeachingSubjectGroupList()
#				display class name if $UserID is class teacher (use "OR" to select yc.AcademicYearID & st.YearTermID)
#
#	Date	:	2010-04-30 (Henry)
#	Detail	:	modified GET_MODULE_OBJ_ARR(), display "Homework Not Submitted Report" base on eHomework-Admin checking, rather than $plugin['eHomework']
#
#	Date	:	2010-03-24 (Henry)
#	Detail	:	modified exportWeeklyHomeworkList(), can export weekly homework list base on the input of start & end dates
#
#	Date	:	2010-03-23 (Henry)
#	Detail	:	modified weeklyHomeworkList(), exportWeeklyHomeworkList(), should no display future homework in student view ONLY
#
#	Date	:	2010-03-18 (Henry)
#	Detail	:	modified weeklyHomeworkList(), exportWeeklyHomeworkList(), should no display future homework in student view
#
#	Date	:	2010-03-18 (Henry)
#	Detail	:	modified checkImportError(), insertHomeworkListRecord(), if "teacher field is empty", one of the subject group teachers will be assign to teacher field
#
#	Date	:	2010-03-17 (Henry)
#	Detail	:	modified checkImportError(), check whether the teacher who upload homework is subject teacher or not (if "teacher" field in CSV is empty)
#
#	Date	:	2010-03-10 (Henry)
#	Detail	:	modified returnSmallGroup(), will not display future homework
#
#	Date	:	2010-03-10 (Henry)
#	Detail	:	modified insertHomeworkListRecord(), if no teacher name is entered, use $PosterID as the teacher
#
#	Date	:	2010-03-05 (Henry)
#	Detail	:	modified handinList(), class teacher can view homework list of his/her class even he/she not the subject teacher
#
#	Date	:	2010-03-02 (Henry)
#	Detail	:	modified weeklyHomeworkList(), exportWeeklyHomeworkList(), monthlyHomeworkList(), exportMonthlyHomeworkList(), getTeachingSubjectList(), getTeachingSubjectGroupList()
#				- add checking on class teacher (class teacher can view the subjects, subjet groups and homeworks of his/her own class)
#
#	Date	:	2010-02-23 (Henry)
#	Detail	:	modified displayWeeklyHomeworklist(), printWeeklyHomeworklist(), displayMonthlyHomeworklist(), printMonthlyHomeworklist()
#				- display Monthly & Weekly Homework List with IP25 standard
#
#	Date	:	2010-02-10 (Henry)
#	Detail	:	modified displayMonthlyHomeworklist(), printMonthlyHomeworklist(), displayDayBlockForMonthlyHomeworkList(), exportMonthlyHomeworkList(), exportHomeworkListRecord(), weeklyHomeworkList(), exportWeeklyHomeworkList(), monthlyHomeworkList(), getNotSubmitList(), viewNotSubmitDetail(), getNotSubmitListPrint(), printHomeworkReport(), exportHomeworkReport()
#				- display Homework Type base on $lhomework->useHomeworkType
#
#	Date	:	2010-02-10 (Henry)
#	Detail	:	modified returnSmallGroup(), displaySmallTable(), change IP25 Homepage
#				- class teacher can view his/her own class homework, and subject group teacher can view his/her subject group homework even they are not the poster of Homework
#
#	Date	:	2010-02-08 (Henry)
#	Detail	:	add getHomeworkType(), retrieve Homework Type data
#
#	Date	:	2010-02-08 (Henry)
#	Detail	:	modifed GET_MODULE_OBJ_ARR(), add option in "Settings"
#
#	Date	:	2010-01-30 [Henry]
#				add getAllClassInfo(), get all class info (classID & class name)
#
#	Date	:	2010-01-30 [Henry]
#				update getTeachingSubjectList() & getTeachingSubjectGroupList(), display the subject info & subject group info base on the classID (if any)
#
#	Date	:	2010-01-11 [YatWoon]
#				update getNotSubmitList(), If user select date range, no need to check the Term in the mysql query
#
#	Date	:	2010-01-11 [YatWoon]
#				update getTeachingSubjectList(), check the user is Admin or not in the function.  If admin, all subjects should be displayed
#				update getTeachingSubjectGroupList(), check subjectID in the function.  subjectID=-1 means display all subjects related
#
#	Date	:	2009-12-18 (Henry)
#	Detail	:	modified getAcademicYearNameAndYearTermName(), YearTermID is not a must.
#
#	Date	:	2009-12-16 (Henry)
#	Detail	:	added monthlyHomeworkList(), displayMonthlyHomeworklist(), printMonthlyHomeworklist(), exportMonthlyHomeworkList(), display monthly homework list.
#
#	Date	:	2009-12-16 (Henry)
#	Detail	:	added displayDayBlockForMonthlyHomeworkList(), base on the flag of $this->useStartDateToGenerateList to display homework list by startdate or duedate
#
#	Date	:	2009-12-15 (Henry)
#	Detail	:	modifed GET_MODULE_OBJ_ARR(), change the UI wording of menu
#
#	Date	:	2009-12-15 (Henry)
#	Detail	:	modifed printWeeklyHomeworklist() & displayWeeklyHomeworklist, in order to display Weekly Homework list vertically, generate homework list by start date / due date
#
******************* End Of Change Log *******************/
class libhomework2007 extends libhomework
{
	var $ViewOnly;

	function libhomework2007(){
		$this->libhomework();
		$this->ViewOnly = $this->isViewOnly();
	}


	###########################################################
	# Left menu                                               #
	###########################################################
	function GET_MODULE_OBJ_ARR(){
		global $top_menu_mode,$PATH_WRT_ROOT, $LAYOUT_SKIN, $eHomework, $CurrentPage, $ip20TopMenu, $CurrentPageArr, $UserID, $sys_custom;
		global $Lang, $plugin, $intranet_root;
			
		### Used for check access permission
		//include_once($PATH_WRT_ROOT."includes/libuser.php");
			
		//$lu = new libuser($UserID);

		# Current Page Information init
		$PageManagement	= 0;
		$PageManagement_HomeworkList = 0;
		$PageManagement_SubjectLeader = 0;
		$PageManagement_WeeklyDiary = 0;
		$PageManagement_NewspaperCutting = 0;
		$PageManagement_HomeworkImage = 0;
		$PageReports = 0;
		$PageReports_WeekHomeworkList = 0;
		$PageReports_NotSubmitList = 0;
		$PageReports_HomeworkReport = 0;
		$PageReports_ParentNotice = 0;
		$PageStatistics = 0;
		$PageSettings = 0;
		$PageSettings_BasicSettings = 0;
		$PageSettings_HandInStatusSettings = 0;

		# Current Page Information init
		switch ($CurrentPage) {
			#-------------- Management
			case "Management_HomeworkList":
				$PageManagement = 1;
				$PageManagement_HomeworkList = 1;
				break;
			case "Management_SubjectLeader":
				$PageManagement = 1;
				$PageManagement_SubjectLeader = 1;
				break;
			case "Management_WeeklyDiary":
				$PageManagement = 1;
				$PageManagement_WeeklyDiary = 1;
				break;
			case "Management_NewspaperCutting":
				$PageManagement = 1;
				$PageManagement_NewspaperCutting = 1;
				break;
			case "Management_HomeworkImage":
				$PageManagement = 1;
				$PageManagement_HomeworkImage = 1;
				break;

				#-------------- Reports
			case "Reports_WeekHomeworkList":
				$PageReports = 1;
				$PageReports_WeekHomeworkList = 1;
				break;
			case "Reports_NotSubmitList":
				$PageReports = 1;
				$PageReports_NotSubmitList = 1;
				break;
			case "Reports_SupplementaryList":
				$PageReports = 1;
				$PageReports_SupplementaryList = 1;
				break;
					
			case "Reports_HomeworkReport":
				$PageReports = 1;
				$PageReports_HomeworkReport = 1;
				break;
					
					
			case "Reports_ParentNotice":
				$PageReports = 1;
				$PageReports_ParentNotice = 1;
				break;
				 
			case "Reports_NotSubmitRecord":
				$PageReports = 1;
				$PageReports_NotSubmitRecord = 1;
				break;
				 
			case "Reports_ViolationRecord":
				$PageReports = 1;
				$PageReports_ViolationRecord = 1;
				break;
				 
			case "Reports_CancelViolationRecord":
				$PageReports = 1;
				$PageReports_CancelViolationRecord = 1;
				break;

				#-------------- Statistics
			case "Statistics_HomeworkStatistics":
				$PageStatistics = 1;
				$PageStatistics_HomeworkStatistics = 1;
				break;
					
			case "Statistics_StatsByMonth":
				$PageStatistics = 1;
				$PageStatistics_StatsByMonth = 1;
				break;
					
			case "Statistics_StatsByClass":
				$PageStatistics = 1;
				$PageStatistics_StatsByClass = 1;
				break;
				 
				#-------------- Settings
			case "Settings_BasicSettings":
				$PageSettings = 1;
				$PageSettings_BasicSettings = 1;
				break;
			case "Settings_HandInStatusSettings":
				$PageSettings = 1;
				$PageSettings_HandInStatusSettings = 1;
				break;
			case "Settings_HomeworkType":
				$PageSettings = 1;
				$PageSettings_HomeworkType = 1;
				break;
			case "Settings_ViewerGroup":
				$PageSettings = 1;
				$PageSettings_ViewerGroup = 1;
		}

		# Menu information
		if($CurrentPageArr['eAdminHomework'])
		{
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/index.php";
				
			if(!$this->disabled && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $this->isViewerGroupMember($UserID)))
			{
				if($_SESSION['isTeaching'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $this->isViewerGroupMember($UserID)){

					if($_SESSION['isTeaching'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]) {
						# Management
						$MenuArr["Management"] = array($Lang['SysMgr']['Homework']['Management'], "#", $PageManagement);
						$MenuArr["Management"]["Child"]["Homework_List"] = array($Lang['SysMgr']['Homework']['HomeworkList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/management/homeworklist/index.php", $PageManagement_HomeworkList);
							
						global $eclassAppConfig;
						include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
						$leClassApp = new libeClassApp();
						if ($leClassApp->isEnabledStudentApp()) {
							$showWeeklyDiary = false;
							## Get form-based app access right settings
						    $accessRightAssoAry = $leClassApp->getAccessRightInfo($eclassAppConfig['appType']['Student']);
                            if(!empty($accessRightAssoAry)) {
                                $accessRightAssoAry = (array)$accessRightAssoAry;
                                foreach ($accessRightAssoAry as &$value) {
                                    if($value['weeklyDiary']['RecordStatus']==1){
                                        $showWeeklyDiary = true;
                                    }
                                }
                            }
			                if($showWeeklyDiary){			              			                	               							
							$MenuArr["Management"]["Child"]["Weekly_Diary"] = array($Lang['SysMgr']['Homework']['WeeklyDiary'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/management/weeklydiary/index.php", $PageManagement_WeeklyDiary);
							$MenuArr["Management"]["Child"]["Newspaper Cutting"] = array($Lang['SysMgr']['Homework']['NewspaperCutting'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/management/newspapercutting/index.php", $PageManagement_NewspaperCutting);
			                }
						}
							
						if($_SESSION['isTeaching'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $this->isViewerGroupMember($UserID))
							$MenuArr["Management"]["Child"]["Subject_Leader"] = array($Lang['SysMgr']['Homework']['SubjectLeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/management/subjectleader/index.php", $PageManagement_SubjectLeader);

                        if($sys_custom['eHomework']['HomeworkImage']){
                        	$MenuArr["Management"]["Child"]["Homework_Image"] = array($Lang['SysMgr']['Homework']['HomeworkImage'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/management/homeworkImage/index.php", $PageManagement_HomeworkImage);                        	
                        }
					}
					# Reports
					$MenuArr["Reports"] = array($Lang['SysMgr']['Homework']['Reports'], "#", $PageReports);

					$MenuArr["Reports"]["Child"]["HomeworkList"] = array($Lang['SysMgr']['Homework']['HomeworkList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/weeklyhomeworklist/index.php", $PageReports_WeekHomeworkList);
					//if($plugin['eHomework']){
					//}
					if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || $this->isViewerGroupMember($UserID)) {
						//$MenuArr["Reports"]["Child"]["NotSubmitList"] = array($Lang['SysMgr']['Homework']['NotSubmitList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/notsubmitlist/index.php", $PageReports_NotSubmitList);
						$MenuArr["Reports"]["Child"]["NotSubmitList"] = array($Lang['eHomework']['HandinStatusReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/notsubmitlist/index.php", $PageReports_NotSubmitList);
						if ($sys_custom['eHomework_Status_Supplementary']) {
							$MenuArr["Reports"]["Child"]["SupplementaryList"] = array($Lang['SysMgr']['Homework']['SupplementaryList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/supplementarylist/index.php", $PageReports_SupplementaryList);
						}
						$MenuArr["Reports"]["Child"]["HomeworkReport"] = array($Lang['SysMgr']['Homework']['HomeworkReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/homeworkreport/index.php", $PageReports_HomeworkReport);

						if ($sys_custom['eHomework_parent_notice']) {
							$MenuArr["Reports"]["Child"]["ParentNotice"] = array($Lang['SysMgr']['Homework']['ParentNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/notsubmitlist/parent_notice.php", $PageReports_ParentNotice);
						}
							
						if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
							// $MenuArr["Reports"]["Child"]["NotSubmitRecord"] = array($Lang['SysMgr']['Homework']['NotSubmitRecord'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/notsubmitlist/notsubmit.php", $PageReports_NotSubmitRecord);
							$MenuArr["Reports"]["Child"]["ViolationRecord"] = array($Lang['SysMgr']['Homework']['ViolationRecord'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/notsubmitlist/hw_violation.php", $PageReports_ViolationRecord);
							$MenuArr["Reports"]["Child"]["CancelViolationRecord"] = array($Lang['SysMgr']['Homework']['CancelViolationRecord'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/notsubmitlist/cancel_hwviolation.php", $PageReports_CancelViolationRecord);
						}

						# Statistics
						$MenuArr["Statistics"] = array($Lang['SysMgr']['Homework']['Statistics'], "#", $PageStatistics);
							
						if ($sys_custom['eHomework_stats_class']) {
								
							$MenuArr["Statistics"]["Child"]["StatsByMonth"] = array($Lang['SysMgr']['Homework']['StatsByMonth'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/statistics/homeworkstatistics/month_index.php", $PageStatistics_StatsByMonth);
						}
							
						$MenuArr["Statistics"]["Child"]["HomeworkStatistics"] = array($Lang['SysMgr']['Homework']['HomeworkStatistics'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/statistics/homeworkstatistics/index.php", $PageStatistics_HomeworkStatistics);
							
							
						if ($sys_custom['eHomework_stats_class']) {
								
							$MenuArr["Statistics"]["Child"]["StatsByClass"] = array($Lang['SysMgr']['Homework']['StatsByClass'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/statistics/homeworkstatistics/class_index.php", $PageStatistics_StatsByClass);
						}
					}
				}
				else{
					# Management
					$MenuArr["Management"] = array($Lang['SysMgr']['Homework']['Management'], "#", $PageManagement);
					$MenuArr["Management"]["Child"]["Homework_List"] = array($Lang['SysMgr']['Homework']['HomeworkList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/management/homeworklist/index.php", $PageManagement_HomeworkList);
					# Reports
					$MenuArr["Reports"] = array($Lang['SysMgr']['Homework']['Reports'], "#", $PageReports);
					$MenuArr["Reports"]["Child"]["WeekHomeworkList"] = array($Lang['SysMgr']['Homework']['WeeklyHomeworkList'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/reports/weeklyhomeworklist/index.php", $PageReports_WeekHomeworkList);
				}
			}

			# Settings
			if($_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework']){
				$MenuArr["Settings"] = array($Lang['SysMgr']['Homework']['Settings'], "#", $PageSettings);
				$MenuArr["Settings"]["Child"]["BasicSettings"] = array($Lang['SysMgr']['Homework']['BasicSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/settings/index.php", $PageSettings_BasicSettings);
				if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) {
					$MenuArr["Settings"]["Child"]["HandInStatusSettings"] = array($Lang['SysMgr']['Homework']['HandInStatusSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/settings/handinstatus_settings.php", $PageSettings_HandInStatusSettings);
				}
				if($this->useHomeworkType)
					$MenuArr["Settings"]["Child"]["HomeworkType"] = array($Lang['SysMgr']['Homework']['HomeworkType'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/settings/homeworkType.php", $PageSettings_HomeworkType);
					$MenuArr["Settings"]["Child"]["ViewerGroup"] = array($Lang['eHomework']['ViewerGroup'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/homework/settings/viewergroup.php", $PageSettings_ViewerGroup);
			}
		}
		else {
			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/homework/index.php";

			# Management
			$MenuArr["Management"] = array($Lang['SysMgr']['Homework']['Management'], "#", $PageManagement);
			$MenuArr["Management"]["Child"]["Homework_List"] = array($Lang['SysMgr']['Homework']['HomeworkList'], $PATH_WRT_ROOT."home/eService/homework/management/homeworklist/index.php", $PageManagement_HomeworkList);


			# Reports
			$MenuArr["Reports"] = array($Lang['SysMgr']['Homework']['Reports'], "#", $PageReports);
			$MenuArr["Reports"]["Child"]["HomeworkList"] = array($Lang['SysMgr']['Homework']['HomeworkList'], $PATH_WRT_ROOT."home/eService/homework/reports/weeklyhomeworklist/index.php", $PageReports_WeekHomeworkList);
		}
			
        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eHomework";'."\n";
        $js.= '</script>'."\n";

		# module information
		$MODULE_OBJ['title'] = $ip20TopMenu['eHomework'].$js;
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_ediscipline.gif";
		$MODULE_OBJ['menu'] = $MenuArr;
		$MODULE_OBJ['title_css'] = "menu_opened";
		return $MODULE_OBJ;
	}

	function displayIndex($classID, $UserID)
	{
		global $UserID;
		
		$today = date('Y-m-d');
		# Current Year
		$yearID = Get_Current_Academic_Year_ID();
		# Current Year Term
		$currentYearTerm = getAcademicYearAndYearTermByDate($today);
		$yearTermID = $currentYearTerm[0];
		
		$UserID = IntegerSafe($UserID);
		$sql = "SELECT RecordType, Teaching FROM INTRANET_USER WHERE UserID = '$UserID'";
		$temp = $this->returnArray($sql,2);
		list($type,$teaching) = $temp[0];
			
		if($type==1 && $this->isViewOnly($UserID) && $this->isViewerGroupMember($UserID))
		{
			# staff in Viewer Group (View only, not class teacher nor subject teacher)
			$view = 5;
		}
		else if ($type==1 && $teaching==1)
		{
			# teaching staff
			//$subjectGroupList = $this->getTeachingSubjectGroupList($UserID, "", $yearID, $yearTermID);
			$subjectGroupList = $this->getTeachingSubjectGroupList($UserID, "", $yearID, "");

			if (sizeof($subjectGroupList)==0)                       // No Homework posted, show all class view
			{
				$view = 2;
			}
			else
			{
				$view = 2;
			}
		}
		elseif ($type==1)
		{	# non-teaching staff
			if ($this->nonteachingAllowed)
				$view = 1;
				else
					$view = 0;
		}
		elseif ($type==2)
		{	# student
			//$subjectGroupList = $this->getStudyingSubjectGroupList($UserID, "", $yearID, $yearTermID);
			$subjectGroupList = $this->getStudyingSubjectGroupList($UserID, "", $yearID, "");
			$view = 3;

		}
		elseif ($type==3)
		{	# parent
			$view = 4;
		}
			
		switch ($view)
		{
			case 1: return $this->allClass();
			case 2: return $this->teacherView($UserID, $subjectGroupList, $type);
			case 3: return $this->studentView($UserID, $subjectGroupList, $type);
			case 4: return $this->parentView($UserID);
			case 5: return $this->viewerView($UserID);
			case 0:
			default: return "";
		}
	}
	 
	function teacherView($UserID, $sgList, $type){
		global $i_Homework_Import,$image_path,$intranet_session_language,$i_Homework_New,$i_Homework_SearchSubject,$i_Homework_subject,$i_Homework_myrecords;
		 
		$hwImagePath = "$image_path/homework/$intranet_session_language/hw";

		$hwBlock = "";
		$hwBlock .= "<FORM name='form1' method='get' action=\"#homework\">\n";
		 
		$HomeworkArr = $this->returnSmallGroup($sgList, $UserID, $type);

		$subjectNameArr = array();
		foreach($HomeworkArr as $SubjectID => $SubjectArr){
			foreach($SubjectArr as $SubjectGroupID => $SubjectGroupArr){
				foreach($SubjectGroupArr as $HomeworkRecordID => $HomeworkRecordArr){
					$SubjectName = $HomeworkRecordArr[2];
					if(!in_array($SubjectName, $subjectNameArr))
						array_push($subjectNameArr, $SubjectName);
						break;
				}
			}

		}
			
		$subjectGroupArr = array();
		for($i=0;$i<count($sgList);$i++)
		{
			$subjectGroupArr[$sgList[$i][0]] = $sgList[$i][1];
		}
		 
		$hwBlock .= $this->displaySmallTable($subjectNameArr, $subjectGroupArr, $HomeworkArr, $type);
		 
		$hwBlock .= "</form>\n";
		 
		return $hwBlock;
	}

	function studentView($UserID, $sgList, $type, $studentName=""){

		global $image_path,$intranet_session_language;
		 
		$hwImagePath = "$image_path/homework/$intranet_session_language/hw";

		 
		$hwBlock = "";
		$hwBlock .= "<FORM name='form1' method='get' action=\"#homework\">\n";

		$HomeworkArr = $this->returnSmallGroup($sgList, $UserID, $type);

		$subjectNameArr = array();
		foreach($HomeworkArr as $SubjectID => $SubjectArr){
			foreach($SubjectArr as $SubjectGroupID => $SubjectGroupArr){
				foreach($SubjectGroupArr as $HomeworkRecordID => $HomeworkRecordArr){
					$SubjectName = $HomeworkRecordArr[2];
					if(!in_array($SubjectName, $subjectNameArr))
						array_push($subjectNameArr, $SubjectName);
						break;
				}
			}
		}
			
		$subjectGroupArr = array();
		for($i=0;$i<count($sgList);$i++)
		{
			$subjectGroupArr[$sgList[$i][0]] = $sgList[$i][1];
		}
		 
		$hwBlock .= $this->displaySmallTable($subjectNameArr, $subjectGroupArr, $HomeworkArr, $type, $studentName);

		$hwBlock .= "</form>\n";
		 
		return $hwBlock;
	}

	function parentView($UserID){
		global $image_path,$intranet_session_language,$i_Homework_ChildrenList, $i_no_record_exists_msg;
			
		$today = date('Y-m-d');
		# Current Year
		$yearID = Get_Current_Academic_Year_ID();
			
		# Current Year Term
		/*
			$currentYearTerm = getAcademicYearAndYearTermByDate($today);
			$yearTermID = $currentYearTerm[0];
			*/
		$allSemesters = getSemesters($yearID);
		if(sizeof($allSemesters)>0) {
			$terms = array_keys($allSemesters);
		}
			
		$hwImagePath = "$image_path/homework/$intranet_session_language/hw";
		$hwBlock = "";
		$hwBlock .= "<FORM name='form1' method='get' action=\"#homework\">\n";
			
		# Get children List
		$parentID = $UserID;
		$namefield = getNameFieldWithClassNumberByLang("b.");
		$sql = "SELECT a.StudentID, $namefield, b.RecordType
		FROM INTRANET_PARENTRELATION as a, INTRANET_USER as b
		WHERE a.StudentID = b.UserID AND a.ParentID = $parentID";
			
		$children = $this->returnArray($sql,2);
			
		if (sizeof($children)==0)
		{
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr>
			<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
			<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
			<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
			</tr>";
			$x .= "</table>";

			return $x;
		}
			
		else{
			for ($i=0; $i<sizeof($children); $i++){
				list($id, $name, $type) = $children[$i];
					
				//$sgList = $this->getStudyingSubjectGroupList($id, "", $yearID, $yearTermID);
				$sgList = $this->getStudyingSubjectGroupList($id, "", $yearID, $terms);
					
				$HomeworkArr = $this->returnSmallGroup($sgList, $id, $type);

				$subjectNameArr = array();
				foreach($HomeworkArr as $SubjectID => $SubjectArr){
					foreach($SubjectArr as $SubjectGroupID => $SubjectGroupArr){
						foreach($SubjectGroupArr as $HomeworkRecordID => $HomeworkRecordArr){
							$SubjectName = $HomeworkRecordArr[2];
							if(!in_array($SubjectName, $subjectNameArr))
								array_push($subjectNameArr, $SubjectName);
								break;
						}
					}
				}
					
				$subjectGroupArr = array();
				for($j=0;$j<count($sgList);$j++)
				{
					$subjectGroupArr[$sgList[$j][0]] = $sgList[$j][1];
				}
					
				if(sizeof($subjectNameArr)!=0 && sizeof($subjectGroupArr)!=0){
					$hwBlock .= $this->displaySmallTable($subjectNameArr, $subjectGroupArr, $HomeworkArr, $type, $name);
					$hwBlock .= "<br />";
				}
			}
		}
		$hwBlock .= "</form>\n";
		return $hwBlock;
	}

	function viewerView($userid)
	{
		# viewer group homework list in portal
		global $i_no_record_exists_msg, $intranet_session_language;
		$HomeworkArr = $this->returnSmallGroup($subjectGroup=array(), $userid, $recordType=1, $viewOnly=1);
			
		$hwBlock = "";
		$hwBlock .= "<FORM name='form1' method='get' action=\"#homework\">\n";
			
		$subjectNameArr = array();
		foreach($HomeworkArr as $SubjectID => $SubjectArr){
			foreach($SubjectArr as $SubjectGroupID => $SubjectGroupArr){
				foreach($SubjectGroupArr as $HomeworkRecordID => $HomeworkRecordArr){
					$SubjectName = $HomeworkRecordArr[2];
					if(!in_array($SubjectName, $subjectNameArr))
						array_push($subjectNameArr, $SubjectName);
						break;
				}
			}
		}
			
		include_once("subject_class_mapping.php");
		$scm = new subject_class_mapping();
		$subjectGroups = $scm->Get_Subject_Group_List(getCurrentSemesterID());
			
		if(count($subjectGroups)>0) {
			foreach($subjectGroups['SubjectGroupList'] as $sgid=>$sgInfo) {
				$sgList[] = array(
						$sgInfo['SubjectGroupID'],
						Get_Lang_Selection($sgInfo['ClassTitleB5'], $sgInfo['ClassTitleEN'])
				);
			}
		}
		//debug_pr($sgList);
			
		$subjectGroupArr = array();
		for($i=0;$i<count($sgList);$i++)
		{
			$subjectGroupArr[$sgList[$i][0]] = $sgList[$i][1];
		}
		 
		$hwBlock .= $this->displaySmallTable($subjectNameArr, $subjectGroupArr, $HomeworkArr, $type);

		$hwBlock .= "</form>\n";
		 
		return $hwBlock;
	}

	function returnSmallGroup($cid, $uid, $type, $viewOnly=0) {
		global $i_Homework_today, $intranet_session_language, $UserID;
			
		$fields = "	CONCAT(a.Title, if (a.StartDate=CURDATE(),' ','')),
		DATE_FORMAT(a.DueDate,'%Y-%m-%d'), IF('$intranet_session_language'='en', b.EN_DES, b.CH_DES)As SubjectName, a.Loading, a.HomeworkID, a.PosterUserID,
		a.Description, a.SubjectID, a.AttachmentPath, a.HandinRequired, a.ClassGroupID, a.TypeID";
		$dbtables = "INTRANET_HOMEWORK as a
                         LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID";
			
			
		if (is_array($cid) && count($cid) >0){
			$ClassImplode = "";
			for ($i=0;$i<count($cid);$i++)
			{
				if ($i==0)
					$ClassImplode .= $cid[$i][0];
					else
						$ClassImplode .= ",".$cid[$i][0];
			}
			$ClassRestrict = " AND ClassGroupID IN ($ClassImplode) ";
		}
		else if($viewOnly!=1) {
			$ClassRestrict = " AND ClassGroupID = '$cid' ";
		}
			
		if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $viewOnly==1) {
			# DO NOTHING
			$extraConds = " AND a.SubjectID!=0";
		}
		else if($type==1) {
			# class teacher can view own class all subjects/subject groups homework
			$clsSQL = "SELECT yct.YearClassID FROM YEAR_CLASS_TEACHER yct LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID) WHERE yct.UserID=$uid AND yc.AcademicYearID=".Get_Current_Academic_Year_ID();
			$result = $this->returnVector($clsSQL);

			if(sizeof($result)==0) {		# not class teacher, so subject teacher can only view the homework of his/her own subject groups
				$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID)";
				$extraConds = " AND (stct.UserID=$uid OR a.PosterUserID = $uid)";
				//$poster = "AND a.PosterUserID = $uid";
			} else {						# class teacher can view own class homework
				//$poster = "AND a.PosterUserID = $uid";
				$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=a.ClassGroupID)
									LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
									LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID)
									";
				$extraConds = " AND (ycu.YearClassID IN (".implode(',', $result).") OR a.PosterUserID = $uid OR stct.UserID=$uid)";
			}
		} else
			$poster ="";


			//$conds = "	{$ClassRestrict} $poster AND a.StartDate<=NOW() AND a.DueDate >= CURDATE() $extraConds";
			$conds = " a.StartDate<=NOW() AND a.DueDate >= CURDATE() $extraConds $ClassRestrict";
				
			$sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.HomeworkID ORDER BY SubjectName, a.SubjectID";
				
			$returnArr1 = $this->returnArray($sql,12);
			$returnArr2 = array();

			for ($i=0;$i<count($returnArr1);$i++)
			{
				$SubjectID = $returnArr1[$i][7];
				$SubjectGroupID = $returnArr1[$i][10];

				$returnArr2[$SubjectID][$SubjectGroupID][$i] = $returnArr1[$i];
			}
			 
			return $returnArr2;
	}

	function displaySmallTable($subjectNameArr="", $subjectGroupArr="", $HomeworkArr="", $type, $studentName=""){
		global $image_path, $LAYOUT_SKIN, $i_no_record_exists_msg;
		 
		$x = "";
		 
		if (is_array($HomeworkArr) && count($HomeworkArr)>0)
		{
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";

			if ($studentName != "")
			{
				$x .= "
							<tr>
								<td><span class=\"indextabclasslist\"><b><i><u>".$studentName."<u></i></b></span></td>
							</tr>
							";
			}

			for($i=0; $i<sizeof($subjectNameArr); $i++){
				$x .= "<tr>
								<td><span class=\"indextabclasslist\"><b>".$subjectNameArr[$i]."</b></span></td>
							</tr>
							";
					
				foreach($HomeworkArr as $RecordID => $RecordArr){
					$RowCnt = 0;
					foreach($RecordArr as $SubjectGroupID => $ClassArr){
						$x .= "<tr>";
							
						if (is_array($ClassArr) && count($ClassArr)>0){
							foreach($ClassArr as $SubjectID => $SubjectArr){
								$SubjectName = $SubjectArr[2];
								break;
							}
							if($SubjectName ==$subjectNameArr[$i]){
									
								if (($RowCnt%2)!=0)
								{
									$x .= "<td class=\"indextabwhiterow\">";
								} else {
									$x .= "<td class=\"indextabgreenrow\">";
								}
									
								$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
								<tr>
								<td width=\"19\" valign=\"top\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_homework.gif\" /></td>
								<td><span class=\"indextabclasslist\">".$subjectGroupArr[$SubjectGroupID]."&nbsp;&nbsp;</span></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>
												<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
											";
								foreach($ClassArr as $SubjectID => $SubjectArr)
								{
									$typeName = "";
									if($this->useHomeworkType) {
										$homeworkTypeInfo = $this->getHomeworkType($SubjectArr['TypeID']);
										if($homeworkTypeInfo[0]['TypeName']) $typeName = "[".$homeworkTypeInfo[0]['TypeName']."] ";
									}

									$x .= "<tr>
													<td width=\"5\" valign=\"top\" ><a class=\"indexhomelist\">-</a></td>
													<td><a href=\"javascript:fe_view_homework_detail(".$type.", ".$SubjectArr[4].")\" class=\"indexhomelist\"> ".$typeName.$SubjectArr[0]." </a></td>
											   </tr>";

								}
								$x .= "</table>
											 </td>
											 </tr>
										   </table>";
							}
							//$RowCnt++;
						}

						$x .= "</td>";
						$x .= "</tr>";
						$RowCnt++;
					}
				}
			}
			$x .= "</table>";
		}
		else
		{
			$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr>
			<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
			<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
			<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
			</tr>
			";
			$x .= "</table>";
		}
		 
		return $x;
	}
		

	# used by import_result.php in homework list
	function getAllSubjectGroupCode($yearID, $yearTermID){
		global $intranet_session_language;
		$sql = "SELECT a.ClassCode,
		IF('$intranet_session_language'='en', a.ClassTitleEN, a.ClassTitleB5)
		FROM SUBJECT_TERM_CLASS AS a
		INNER JOIN SUBJECT_TERM AS b ON b.SubjectGroupID = a.SubjectGroupID
		INNER JOIN ACADEMIC_YEAR_TERM AS c ON c.YearTermID = b.YearTermID
		INNER JOIN ACADEMIC_YEAR AS d ON d.AcademicYearID = c.AcademicYearID
		WHERE d.AcademicYearID = $yearID AND c.YearTermID = $yearTermID
		ORDER BY a.ClassCode";
			
		$temp = $this->returnArray($sql, 2);
		return $temp;
	}

	function getAllSubjectCode($yearID, $yearTermID){
		global $intranet_session_language;
		$sql = "SELECT b.CODEID,
		IF('$intranet_session_language'='en', b.EN_DES, b.CH_DES)
		FROM SUBJECT_TERM AS a
		INNER JOIN ASSESSMENT_SUBJECT AS b ON b.RecordID = a.SubjectID
		INNER JOIN ACADEMIC_YEAR_TERM AS c ON c.YearTermID = a.YearTermID
		INNER JOIN ACADEMIC_YEAR AS d ON d.AcademicYearID = c.AcademicYearID
		WHERE d.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND b.RecordStatus = 1 AND (CMP_CODEID is NULL OR CMP_CODEID='')
		GROUP BY b.CODEID ORDER BY b.CODEID";
			
		$temp = $this->returnArray($sql, 2);
		return $temp;
	}

	function getAllSubjectCodeOnly($year, $reviseToHTMLCode=0)
	{
		$sql = "SELECT CODEID FROM ASSESSMENT_SUBJECT WHERE RecordStatus=1 AND (CMP_CODEID is NULL OR CMP_CODEID='')";
		$result = $this->returnVector($sql);
			
		if($reviseToHTMLCode==1) {
			for($i=0; $i<sizeof($result); $i++) {
				$tempAry[] = intranet_htmlspecialchars($result[$i]);
			}
			$result = $tempAry;
		}
		return $result;
	}

	function getSubjectTerm($yearID, $yearTermID){
		$sql = "SELECT c.CODEID, a.ClassCode
		FROM SUBJECT_TERM_CLASS AS a
		INNER JOIN SUBJECT_TERM AS b ON b.SubjectGroupID = a.SubjectGroupID
		INNER JOIN ASSESSMENT_SUBJECT AS c  ON c.RecordID = b.SubjectID
		INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = b.YearTermID
		INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
		WHERE e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID";
			
		$temp = $this->returnArray($sql, 2);
		return $temp;
	}

	/*function getTeachingGroupSubject($userID){

	$sql = "SELECT a.ClassCode, b.CODEID FROM SUBJECT_TERM_CLASS As a,
	ASSESSMENT_SUBJECT As b, SUBJECT_TERM AS c, SUBJECT_TERM_CLASS_TEACHER AS d
	WHERE a.SubjectGroupID = d.SubjectGroupID AND b.RecordID = c.SubjectID AND c.SubjectGroupID = d.SubjectGroupID AND d.UserID = $userID";
		
	//echo $sql;
	$temp = $this->returnArray($sql, 2);
	return $temp;
	}*/

	function getTeachingSubject($userID, $yearID, $yearTermID){

		$sql = "SELECT a.CODEID
		FROM ASSESSMENT_SUBJECT As a
		INNER JOIN SUBJECT_TERM AS b ON b.SubjectID=a.RecordID
		INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS c ON c.SubjectGroupID = b.SubjectGroupID
		INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = b.YearTermID
		INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
		WHERE c.UserID = $userID AND e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID GROUP BY a.CODEID";
			
		$temp = $this->returnArray($sql,1);
		return $temp;
	}

	function getTeachingSubjectGroup($userID='', $yearID, $yearTermID){

		if($userID)
		{
			$sql = "SELECT a.ClassCode
			FROM SUBJECT_TERM_CLASS As a
			INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN SUBJECT_TERM AS c ON c.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = c.YearTermID
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			WHERE b.UserID = $userID and e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID GROUP BY a.ClassCode";
		}
		else	# is eHomework admin
		{
			$sql = "SELECT
			a.ClassCode
			FROM
			SUBJECT_TERM_CLASS As a
			INNER JOIN SUBJECT_TERM AS c ON c.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = c.YearTermID
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			WHERE e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID GROUP BY a.ClassCode";
		}
			
		$temp = $this->returnArray($sql, 1);
		return $temp;
	}

	# used by import_result in homework list
	function checkImportError($data, $yearID, $yearTermID){
		global $UserID, $Lang, $UserLogin, $i_Homework_Collect_Required, $i_general_no, $i_general_yes, $i_Homework_HomeworkType;
			
		$allSubjectCode = array();
		$allSubjectName = array();
		$allSubjectGroupCode = array();
		$allSubjectGroupName = array();
		$allSubjectTerm = array();
			
		$teacherID = array();
		$teacherName = array();
		$nonTeacherName = array();
		$invalidPicName = array();
			
		$teacherSubjectList = array();
		$teacherSubjectGroupList = array();
			
		$sql ="SELECT UserLogin FROM INTRANET_USER WHERE UserID=$UserID";
		$temp = $this->returnVector($sql, 1);
		$userName = $temp[0];
			
		$subjectCodeName = $this->getAllSubjectCode($yearID, $yearTermID);
		$subjectGroupCodeName = $this->getAllSubjectGroupCode($yearID, $yearTermID);
		$subjectTerm = $this->getSubjectTerm($yearID, $yearTermID);
			
			
		# Put All Subject Code and Name into Array
		for($i=0; $i<sizeof($subjectCodeName);$i++){
			list($code, $name)= $subjectCodeName[$i];
			array_push($allSubjectCode, "\"".$code."\"");
			array_push($allSubjectName, $name);
		}
			
		$sbjCodeArray = $this->getAllSubjectCodeOnly($yearID, $reviseToHTMLCode=1);

		# Put All Subject Group Code and Name into Array
		for($i=0; $i<sizeof($subjectGroupCodeName);$i++){
			list($code, $name)= $subjectGroupCodeName[$i];
			array_push($allSubjectGroupCode, intranet_htmlspecialchars($code));
			array_push($allSubjectGroupName, $name);
		}
			
		# Put All Subject Group Code and Name into Array
		for($i=0; $i<sizeof($subjectTerm);$i++){
			list($scode, $sgcode)= $subjectTerm[$i];
			$temp[0] = intranet_htmlspecialchars($scode);
			$temp[1] = intranet_htmlspecialchars($sgcode);

			array_push($allSubjectTerm, $temp);
		}
			
		$teachingSubjectCode = $this->getTeachingSubject($UserID, $yearID, $yearTermID);
		$teachingSubjectGroupCode = $this->getTeachingSubjectGroup($UserID, $yearID, $yearTermID);
			
		# Put User's ID and Subject Code into Array
		for($i=0; $i<sizeof($teachingSubjectCode); $i++){
			$temp[0] = $UserID;
			$temp[1] = $teachingSubjectCode[$i]['CODEID'];

			array_push($teacherSubjectList, $temp);
		}
			
		# Put User's ID and Subject Group Code into Array
		for($x=0; $x<sizeof($teachingSubjectGroupCode); $x++){
			$temp[0] = $UserID;
			$temp[1] = $teachingSubjectGroupCode[$x]['ClassCode'];

			array_push($teacherSubjectGroupList, $temp);
		}
			
		### List out the import result
		$result = "<table width=\"95%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
		$result .= "<tr>";
		$result .= "<td width='4%' class='tablebluetop tabletopnolink'>#</td>";
		/*
			$result .= "<td width='8%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
			*/
		$result .= "<td width='12%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
		$result .= "<td width='18%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
		$result .= "<td width='10%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Teacher']."</td>";
		$result .= "<td width='12%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
		$result .= "<td width='12%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
		$result .= "<td width='8%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['HandinRequired']."</td>";
		if($this->useHomeworkCollect)
			$result .= "<td width='8%' class='tablebluetop tabletopnolink'>".$i_Homework_Collect_Required."</td>";
			if($this->useHomeworkType)
				$result .= "<td width='8%' class='tablebluetop tabletopnolink'>".$i_Homework_HomeworkType."</td>";
					
				$result .= "<td width='24%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Error']."</td>";
				$result .= "</tr>";

				$this->importSuccessCount = 0;
				$this->importErrorCount = 0;
					
				# Loop each Record
				for($i=0;$i<sizeof($data);$i++){
					if(!is_array($data[$i]) || trim(implode("",$data[$i]))=="")
						continue;

						if($this->useHomeworkCollect)
							//list($subjectCode, $subjectGroupCode, $title, $workload, $pic, $start, $due, $description, $handin,$collect, $typeID) = $data[$i];
							list($subjectGroupCode, $title, $workload, $pic, $start, $due, $description, $handin,$collect, $typeID) = $data[$i];
							else
								//list($subjectCode, $subjectGroupCode, $title, $workload, $pic, $start, $due, $description, $handin, $typeID) = $data[$i];
								list($subjectGroupCode, $title, $workload, $pic, $start, $due, $description, $handin, $typeID) = $data[$i];
									
								# 20091118 yat
								# if teacher login field is empty, then system retrieve related teacher
								if($pic=="")
								{
									$SubjectGroupTeachers = $this->RetrieveSubjectGroupTeacherLogin($subjectGroupCode, $yearTermID);
									if(!empty($SubjectGroupTeachers))
									{
										$pic = $SubjectGroupTeachers[0][0];
									}
								}
								/*
								 # if teacher login field is empty, use the upload staff as PIC, added by Henry on 20100317
								 if($pic=="") {
								 $sql ="SELECT UserLogin FROM INTRANET_USER WHERE UserID='$UserID'";
								 $temp = $this->returnVector($sql);
								 $pic = $temp[0];
								 }
								 */
								$error_css = "class=\"red\"";
								$err = "";

								# initialize css
								$dataRowError = 0;
								$css_subjectCode = "";
								$css_subjectGroupCode = "";
								$css_title = "";
								$css_pic = "";
								$css_start = "";
								$css_due = "";
								$css_handin = "";

								$workload = ($workload=="")? 0:$workload;
								$description = ($description=="")? "--":$description;
								$handin = ($handin=="")? "No": strtoupper($handin);
								$collect = ($collect=="")? "No": strtoupper($collect);

								$requiredArray = array('YES','NO');

								$delim = "";
								//$allowed_empty_fields = array(3,4,7,8);


								//$allowed_empty_fields[] = 8;
								//$allowed_empty_fields = array(0);
								$allowed_empty_fields = array(2,3,6,7);
								$pos = 8;

								if($this->useHomeworkCollect) {
									$allowed_empty_fields[] = $pos;
									$pos++;
								}
								if($this->useHomeworkType) {
									$allowed_empty_fields[] = $pos;
									$pos++;
								}

								# Check Empty Field
								for ($j=0; $j < sizeof ($data[$i]); $j++){
									if ($data[$i][$j] =="" && !in_array($j, $allowed_empty_fields))
									{
										$dataRowError++;
										$err .= "<li>".$j.$Lang['SysMgr']['Homework']['Remark']['EmptyFields']."</li>";

										//$css_subjectCode = ($subjectCode==""? $error_css:"");
										$css_subjectGroupCode = ($subjectGroupCode==""? $error_css:"");
										$css_title = ($title==""? $error_css:"");
										$css_pic = ($pic==""? $error_css:"");
										$css_start = ($start==""?$error_css:"");
										$css_due = ($due==""?$error_css:"");
									}
								}

								//$subjectCode = intranet_htmlspecialchars($subjectCode);
								$subjectGroupCode = intranet_htmlspecialchars($subjectGroupCode);
								$title = intranet_htmlspecialchars($title);
								$pic = intranet_htmlspecialchars($pic);
								$description = intranet_htmlspecialchars($description);
								$handin_required = intranet_htmlspecialchars($handin);
								$collect = intranet_htmlspecialchars($collect);

								# get Subject Name if exist
								/*
								 $searchValue = array_search($subjectCode, $allSubjectCode);
								 if($searchValue!==false)
								 	$subjectName = $allSubjectName[$searchValue];
								 	else
								 		$subjectName = $subjectCode;
								 		*/
								 	# get Subject Group Name if exist
								 	$searchValue = array_search($subjectGroupCode, $allSubjectGroupCode);
								 	if($searchValue!==false)
								 		$subjectGroupName = $allSubjectGroupName[$searchValue];
								 		else
								 			$subjectGroupName = $subjectGroupCode;
								 			/*
								 			 # Check Subject Code exist
								 			 //if (!in_array("\"".$subjectCode."\"",$allSubjectCode)){
								 			 if (!in_array($subjectCode,$sbjCodeArray)){
								 			 //echo "No such subject<br>";
								 			 $dataRowError++;
								 			 $err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['NoSubject']."</li>";
								 			 $css_subjectCode = $error_css;
								 			 }
								 			 */
								 			# Check Subject Group Code exist
								 			if (!in_array($subjectGroupCode,$allSubjectGroupCode)){
								 				//echo "No such group<br>";
								 				$dataRowError++;
								 				$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['NoSubjectGroup']."</li>";
								 				$css_subjectGroupCode = $error_css;
								 			}
								 			/*
								 			 # Check Subject Group belong to the Subject
								 			 //else if (!in_array(array($subjectCode,$subjectGroupCode),$allSubjectTerm)){
								 			 else if (!in_array(array($subjectCode,$subjectGroupCode),$allSubjectTerm)){
								 			 //echo "No such group<br>";
								 			 $dataRowError++;
								 			 $err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['SubjectGroupNotMatch']."</li>";
								 			 $css_subjectGroupCode = $error_css;
								 			 }
								 			 */
								 			# Check Teacher with Subject and Subject Group
								 			else if(!$this->IS_ADMIN($pic) && $pic!=""){
								 					
								 				# If pic exist in the teacher list
								 				if(in_array($pic, $teacherName)){

								 					//echo "Here 1<br>";
								 					//echo "Record $i <br><br>";
								 					$picID = $teacherID[array_search($pic, $teacherName)];

								 					/*if(!in_array(array($picID, $subjectCode), $teacherSubjectList)){
								 					 $dataRowError++;
								 					 $err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['SubjectTeacher']."</li>";
								 					 $css_subjectCode = $error_css;
								 					 $css_pic = $error_css;
								 					 }

								 					 else */
								 					if(!in_array(array($picID, $subjectGroupCode), $teacherSubjectGroupList)){
								 						$dataRowError++;
								 						$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['GroupTeacher']."</li>";
								 						$css_subjectGroupCode = $error_css;
								 						$css_pic = $error_css;
								 					}

								 				}
								 					
								 				# If pic exist in the non teaching staff list
								 				else if(in_array($pic, $nonTeacherName)){
								 					//echo "Here 2<br>";
								 					//echo "Record $i <br><br>";

								 					$dataRowError++;
								 					$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['NonTeachingStaff']."</li>";
								 					$css_pic = $error_css;
								 				}
								 					
								 				# If pic exist in the invalid user list
								 				else if(in_array($pic, $invalidPicName)){
								 					//echo "Here 3<br>";
								 					//echo "Record $i <br><br>";

								 					$dataRowError++;
								 					$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['InvalidUser']."</li>";
								 					$css_pic = $error_css;
								 				}
								 					
								 				# If pic is not exist in the list
								 				else{

								 					$sql ="SELECT UserID, UserLogin FROM INTRANET_USER WHERE UserLogin='$pic'";
								 					$temp = $this->returnArray($sql, 2);
								 					$picID = $temp[0]['UserID'];

								 					# If pic is a valid user
								 					if($picID!=""){
								 						$picName = $temp[0]['UserLogin'];
								 						$lu = new libuser($picID);
								 							
								 						# If pic is a teacher
								 						if($lu->teaching==1){
								 							array_push($teacherID, $picID);
								 							array_push($teacherName, $picName);

								 							# If pic is the user
								 							if($pic == $userName){
								 								//echo "Here 4<br>";
								 								//echo "Record $i <br><br>";
								 								# check whether pic is the class teacher
								 								//										$sql = "SELECT distinct yct.UserID FROM SUBJECT_TERM_CLASS stc INNER JOIN
								 								//												SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=stc.SubjectGroupID) INNER JOIN
								 								//												YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN
								 								//												YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID) INNER JOIN
								 								//												YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID)
								 								//												WHERE
								 								//													stc.ClassCode='$subjectGroupCode' AND yc.AcademicYearID='".Get_Current_Academic_YEar_ID()."'";
								 								//										$classTeacherAry = $this->returnVector($sql);
								 								$classTeacherAry = $this->Get_Subject_Group_Corresponding_Classes_Class_Teacher($subjectGroupCode,$yearTermID);
								 									
								 								/*
								 								 if(!in_array(array($picID, $subjectCode), $teacherSubjectList)){
								 								 $dataRowError++;
								 								 $err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['SubjectTeacher']."</li>";
								 								 $css_subjectCode = $error_css;
								 								 $css_pic = $error_css;
								 								 }
								 								 	
								 								 else */
								 								if(sizeof($classTeacherAry)==0 || !in_array($picID, $classTeacherAry)) {
								 									if(!in_array(array($picID, intranet_undo_htmlspecialchars($subjectGroupCode)), $teacherSubjectGroupList)){
								 										$dataRowError++;
								 										$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['GroupTeacher']."</li>";
								 										$css_subjectGroupCode = $error_css;
								 										$css_pic = $error_css;
								 									}
								 								}
								 							}

								 							# If pic is other users
								 							else{
								 								//echo "Here 5<br>";
								 								//echo "Record $i <br><br>";
								 									
								 								$tempTeachingSubjectCode = $this->getTeachingSubject($picID, $yearID, $yearTermID);
								 									
								 								for($x=0; $x<sizeof($tempTeachingSubjectCode); $x++){
								 									$temp[0] = $picID;
								 									$temp[1] = $tempTeachingSubjectCode[$x]['CODEID'];

								 									array_push($teacherSubjectList, $temp);
								 								}
								 									
								 								$tempTeachingSubjectGroupCode = $this->getTeachingSubjectGroup($picID, $yearID, $yearTermID);
								 								for($x=0; $x<sizeof($tempTeachingSubjectGroupCode); $x++){
								 									$temp[0] = $picID;
								 									$temp[1] = intranet_htmlspecialchars($tempTeachingSubjectGroupCode[$x]['ClassCode']);

								 									array_push($teacherSubjectGroupList, $temp);
								 								}
								 									
								 								/*if(!in_array(array($picID, $subjectCode), $teacherSubjectList)){
								 								 $dataRowError++;
								 								 $err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['SubjectTeacher']."</li>";
								 								 $css_subjectCode = $error_css;
								 								 $css_pic = $error_css;
								 								 }
								 								 	
								 								 else */
								 								if(!in_array(array($picID, $subjectGroupCode), $teacherSubjectGroupList)){

								 									# check whether pic is the class teacher
								 									//										$sql = "SELECT distinct yct.UserID FROM SUBJECT_TERM_CLASS stc INNER JOIN
								 									//												SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=stc.SubjectGroupID) INNER JOIN
								 									//												YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN
								 									//												YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID) INNER JOIN
								 									//												YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID)
								 									//												WHERE
								 									//													stc.ClassCode='".intranet_undo_htmlspecialchars($subjectGroupCode)."' AND yc.AcademicYearID='".Get_Current_Academic_YEar_ID()."'";
								 									//										$classTeacherAry = $this->returnVector($sql);
								 									$classTeacherAry = $this->Get_Subject_Group_Corresponding_Classes_Class_Teacher($subjectGroupCode, $yearTermID);

								 									if(sizeof($classTeacherAry)==0 || !in_array($picID, $classTeacherAry)) {
								 										$dataRowError++;
								 										$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['GroupTeacher']."</li>";
								 										$css_subjectGroupCode = $error_css;
								 										$css_pic = $error_css;
								 									}
								 								}
								 							}
								 						}
								 							
								 						# If pic is not a teacher
								 						else{
								 							# check whether pic is the subject
								 							$sql = "SELECT COUNT(*) FROM INTRANET_SUBJECT_LEADER sl INNER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=sl.ClassID) WHERE stc.ClassCode='".intranet_undo_htmlspecialchars($subjectGroupCode)."'";
								 							$count = $this->returnVector($sql);
								 							//echo $sql;
								 							if($count[0]==0) {		# non subject leader
								 								//echo $sql.'<br>';
								 								//echo "Here 6<br>";
								 								//echo "Record $i <br><br>";
								 								array_push($nonTeacherName, $picName);

								 								$dataRowError++;
								 								$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['NonTeachingStaff']."</li>";
								 								$css_pic = $error_css;
								 							}
								 						}
								 					}

								 					# IF pic is not a valid user
								 					else{
								 						//echo "Here 7<br>";
								 						//echo "Record $i <br><br>";
								 						array_push($invalidPicName, $pic);
								 							
								 						$dataRowError++;
								 						$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['InvalidUser']."</li>";
								 						$css_pic = $error_css;
								 					}
								 				}
								 			}

								 			# Check Start Data
								 			$start_stamp = strtotime($start);
								 			$due_stamp = strtotime($due);
								 			$today = time();

								 			//echo checkDateIsValid($start).'/'.checkDateIsValid($due);
								 			//if($start_stamp=="" || $due_stamp=="") {
								 			if(!checkDateIsValidFor2Format($start) || !checkDateIsValidFor2Format($due)) {
								 				$dataRowError++;
								 				$err .= "<li>".$Lang['General']['InvalidDateFormat']."</li>";
								 					
								 				if(!checkDateIsValidFor2Format($start))
								 					$css_start = $error_css;
								 					if(!checkDateIsValidFor2Format($due))
								 						$css_due = $error_css;

								 			}

								 			/*
								 			 if (!$this->pastInputAllowed && compareDate($start_stamp,$today) < 0){  # start date eariler than today
								 			 $dataRowError++;
								 			 $err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['StartDateError']."</li>";
								 			 $css_start = $error_css;
								 			 }
								 			 */
								 			else if (compareDate($due_stamp,$start_stamp)<0){ # End Date eariler than start date
								 				$dataRowError++;
								 				$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['DueDateError']."</li>";
								 				$css_start = $error_css;
								 				$css_due = $error_css;
								 					
								 			}

								 			# Check Handin Required
								 			if (trim($handin_required)!=""){
								 				if(!in_array(strtoupper($handin_required),$requiredArray))
								 				{
								 					$dataRowError++;
								 					$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['Handin']."</li>";
								 					$css_handin = $error_css;
								 				}
								 			}

								 			# Check Collect Required
								 			if($this->useHomeworkCollect) {
								 				if (trim($collect)!=""){
								 					if(!in_array(strtoupper($collect),$requiredArray))
								 					{
								 						$dataRowError++;
								 						$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['Collect']."</li>";
								 						$css_collect = $error_css;
								 					}
								 				}
								 			}

								 			if($dataRowError>0){
								 				$this->importErrorCount++;
								 				$result .= "<tr valign=\"top\"><td>".($i+1)."</td>";
								 				//$result .= "<td $css_subjectCode>$subjectName</td>";
								 				$result .= "<td $css_subjectGroupCode>$subjectGroupName</td>";
								 				$result .= "<td $css_title>$title</td>";
								 				$result .= "<td $css_pic>$pic</td>";
								 				$result .= "<td $css_start>$start</td>";
								 				$result .= "<td $css_due>$due</td>";
								 				$handinStr = ${"i_general_".strtolower($handin)};
								 				$result .= "<td $css_handin>$handinStr</td>";
								 				if($this->useHomeworkCollect) {
								 					$collectStr = ${"i_general_".strtolower($collect)};
								 					$result .= "<td $css_collect>$collectStr</td>";
								 				}
								 				if($this->useHomeworkType) {
								 					$homeworkTypeInfo = $this->getHomeworkType($typeID);
								 					$typeName = ($homeworkTypeInfo[0]['TypeName']) ? $homeworkTypeInfo[0]['TypeName'] : "---";
								 					$result .= "<td $css_collect>$typeName</td>";
								 				}
								 				$result .= "<td $error_css>$err</td></tr>";
								 			}

								 			else
								 				$this->importSuccessCount++;

				}
				$result .= "</table>";
				return $result;
	}

	function Get_Subject_Group_Corresponding_Classes_Class_Teacher($subjectGroupCode, $YearTermID)
	{
		$sql = "
				SELECT
					distinct yct.UserID
				FROM
					SUBJECT_TERM st
					INNER JOIN SUBJECT_TERM_CLASS stc ON st.SubjectGroupID = stc.SubjectGroupID
					INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=stc.SubjectGroupID)
					INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
					INNER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID)
					INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID)
				WHERE
					st.YearTermID='".$YearTermID."'
					AND stc.ClassCode='".intranet_undo_htmlspecialchars($subjectGroupCode)."'
					AND yc.AcademicYearID='".Get_Current_Academic_YEar_ID()."'";
			
		$classTeacherAry = $this->returnVector($sql);
		return $classTeacherAry;
	}

	# used by import_update.php in homework list
	function insertHomeworkListRecord($tempData){
		global $UserID;
		$recordCount = 0;
			
		# Insert records to Live table
		$fields = "ClassGroupID, SubjectID, PosterUserID, PosterName, StartDate, DueDate, Loading, Title, Description, RecordType, LastModified, TypeID, HandinRequired, CollectRequired, AcademicYearID, YearTermID, CreatedBy";
			
		for ($i=0; $i<sizeof($tempData); $i++){
			/*
				$sql = "SELECT RecordID FROM ASSESSMENT_SUBJECT WHERE CODEID = '".$tempData[$i][0]."'";
				$temp = $this->returnVector($sql);
				$SubjectID = $temp[0];
				*/

			// [2016-0309-1012-23066] only get subjectid from current term
			//$sql = "SELECT st.SubjectID FROM SUBJECT_TERM st LEFT OUTER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=st.SubjectGroupID) WHERE stc.ClassCode = '".intranet_undo_htmlspecialchars($tempData[$i][0])."'";
			$sql = "SELECT
							st.SubjectID
						FROM
							SUBJECT_TERM st
							LEFT OUTER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=st.SubjectGroupID)
						WHERE
							stc.ClassCode = '".intranet_undo_htmlspecialchars($tempData[$i][0])."' AND st.YearTermID = '".$tempData[$i][9]."'";
			$temp = $this->returnVector($sql);
			$SubjectID = $temp[0];

			//				$sql = "SELECT SubjectGroupID FROM SUBJECT_TERM_CLASS WHERE ClassCode = '".intranet_undo_htmlspecialchars($tempData[$i][0])."'";
			$sql = "SELECT
							stc.SubjectGroupID
						FROM
							SUBJECT_TERM_CLASS stc
							INNER JOIN SUBJECT_TERM st ON st.SubjectGroupID = stc.SubjectGroupID AND st.YearTermID = '".$tempData[$i][9]."'
						WHERE
							stc.ClassCode = '".intranet_undo_htmlspecialchars($tempData[$i][0])."'
				";

			$temp = $this->returnVector($sql);

			$ClassGroupID = $temp[0];

			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '".$tempData[$i][4]."'";
			$temp = $this->returnVector($sql);
			$PicID = $temp[0];

			if($tempData[$i][4] == NULL || TRIM($tempData[$i][4])=="") {
					
				//$PosterUserID = $UserID;
				$SubjectGroupTeachers = $this->RetrieveSubjectGroupTeacherLogin($tempData[$i][0], $tempData[$i][9]);
				if(!empty($SubjectGroupTeachers))
				{
					$PosterUserID = $SubjectGroupTeachers[0][1];
				}
					
			} else if($PicID!="")
				$PosterUserID = $PicID;
				else
					$PosterUserID = $UserID;

					$nameField = getNameFieldByLang("");
					$sql = "SELECT $nameField FROM INTRANET_USER WHERE UserID = '$PosterUserID'";
					$temp = $this->returnVector($sql);
					$PosterName = $temp[0];

					$StartDate = $tempData[$i][1];
					$DueDate = $tempData[$i][2];
					$Loading = $tempData[$i][3]/0.5;
					$Title = $tempData[$i][5];
					$Description = $tempData[$i][6];
					$HandinRequired = $tempData[$i][7];
					$AcademicYearID = $tempData[$i][8];
					$YearTermID = $tempData[$i][9];
					$CollectRequired = $tempData[$i][10];
					$RecordType = 1;
					$TypeID = $tempData[$i][11];

					//$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
					//$today_str = date("Y-m-d",$today);

					$values = "'$ClassGroupID', '$SubjectID', '$PosterUserID', '$PosterName', '$StartDate', '$DueDate', '$Loading', '$Title', '$Description', '$RecordType', now(), '$TypeID', '$HandinRequired', '$CollectRequired', '$AcademicYearID', '$YearTermID', '$UserID'";
					$sql = "INSERT INTO INTRANET_HOMEWORK ($fields) VALUES ($values)";

					$result = $this->db_db_query($sql);

					if($result)
						$recordCount++;
		}
			
		return $recordCount;
	}

	# used by export.php in homework list
	function exportHomeworkListRecord($conds, $classID="", $showHID = false){
		global $intranet_session_language;
			
		//$name_field = getNameFieldByLang("d.");
		$name_field = "d.UserLogin";

		# Select records from INTRANET_HOMEWORK table
		$fields = "IF('$intranet_session_language'='en', c.EN_DES, c.CH_DES) as Subject,
		IF('$intranet_session_language'='en', b.ClassTitleEN, b.ClassTitleB5),
		a.Title, a.Loading/2, a.StartDate, a.DueDate, a.Description, a.HandinRequired, a.CollectRequired, $name_field as Name, a.TypeID, c.CODEID, b.ClassCode";
		if($showHID){
			$fields .= ",a.HomeworkID AS HomeworkID ";
		}
		$dbtables = "INTRANET_HOMEWORK AS a
						LEFT OUTER JOIN SUBJECT_TERM_CLASS AS b ON b.SubjectGroupID = a.ClassGroupID
						 LEFT OUTER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = a.SubjectID
						 LEFT OUTER JOIN INTRANET_USER as d ON (d.UserID=a.PosterUserID)";
			
		if($classID!="") {
			$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=b.SubjectGroupID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";
			$conds .= " AND ycu.YearClassID='$classID'";
		}

		$sql = ($conds=='')? "SELECT $fields FROM $dbtables" : "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.HomeworkID";
			
		$sql .= " order by Subject";
		//echo $sql;
		return $this->returnArray($sql, 9);
	}

	# used by Handin_list.php in homework list
	function handinList($hid, $uid=''){
		global $UserID;
		
// 		$name_field = getNameFieldByLang("A.");
		$order_str="ASC";
		$order_field=1;
			
		if ($uid == '') {
			$targetUserId = $UserID;
			$targetUserType = $_SESSION['UserType'];
			$targetIsTeaching = $_SESSION['isTeaching'];
		}
		else {
			$targetUserId = $uid;
			$userObj = new libuser($uid);
			$targetUserType = $userObj->RecordType;
			$targetIsTeaching = $userObj->teaching;
		}
		
		// [2017-0928-1709-10235] Check Class Teacher & Subject Group Teacher
		$IsHomeworkSubjectGroupTeacher = false;
		if($targetUserType==USERTYPE_STAFF && $targetIsTeaching && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
		{
			// Get Homework Info & Check Subject Group Teacher
			$thisHomeworkInfo = $this->returnRecord($hid);
			$thisSubjectGroup = $thisHomeworkInfo["ClassGroupID"];
			$IsHomeworkSubjectGroupTeacher = $this->IsTeacherTeachingTargetSubjectGroup($targetUserId, $thisSubjectGroup);
			
			// Get Class Info & Check Class Teacher
			$thisTeachingClassList = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $targetUserId);
			$isClassTeacher = !empty($thisTeachingClassList);
			
			// Subject Group Teacher
			if($IsHomeworkSubjectGroupTeacher) {
				// do nothing
			}
			// Class Teacher > Get Class Student
			else if($isClassTeacher) {
				$thisTeachingClassList = Get_Array_By_Key((array)$thisTeachingClassList, "YearClassID");
				$thisTeacherClassStudentList = $this->retrieveStudentListByYearClassID($thisTeachingClassList, 2);
				$thisTeacherClassStudentList = Get_Array_By_Key((array)$thisTeacherClassStudentList, "UserID");
				$conds2 = (sizeof($thisTeacherClassStudentList) > 0) ? " OR A.UserID IN (".implode(",", (array)$thisTeacherClassStudentList).")" : "";
			}
			// Original Logic
			else {
				$sql = "SELECT
							ycu.UserID
						FROM
							YEAR_CLASS_USER ycu LEFT OUTER JOIN
							YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
							YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID) LEFT OUTER JOIN
							SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=ycu.UserID) LEFT OUTER JOIN
							SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=stcu.SubjectGroupID)
						WHERE
							(yct.UserID=$targetUserId OR stct.UserID=$targetUserId) AND
							yc.AcademicYearID=".Get_Current_Academic_Year_ID();
				$temp = $this->returnVector($sql);
				$conds = (sizeof($temp)>0) ? " AND A.UserID IN (".implode(',',$temp).")" : "";
				$conds2 = (sizeof($temp)>0) ? " OR A.UserID IN (".implode(',',$temp).")" : "";
			}
		}
		
		/*
			$sql = "SELECT DISTINCT A.UserID, $name_field AS StudentName, A.ClassName, A.ClassNumber,
			D.RecordStatus, D.RecordID, D.DateModified, C.HandinConfirmUserID, C.HandinConfirmTime, C.Title
			FROM INTRANET_USER AS A LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS B ON A.UserID = B.UserID
			LEFT OUTER JOIN INTRANET_HOMEWORK AS C ON B.SubjectGroupID = C.ClassGroupID
			LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS D ON (D.HomeworkID = $hid AND A.UserID = D.StudentID)
			WHERE C.HomeworkID = $hid $conds";
		*/
		
		// [2016-0519-1005-58207] fixed: Non-teaching Staff and Viewer Group Member cannot view Hand-in List if not subject teacher
		// if(!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $this->isSubjectLeader($targetUserId)))
		if(!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) ||
				$this->isSubjectLeader($targetUserId) || $this->isViewerGroupMember($UserID)))
			$additionCond = " AND (stct.UserID = '$targetUserId' $conds2)";
		
			

		$NameField = getNameFieldByLang("A.");
		
		$name_field = "CASE
						WHEN A.RecordStatus = 3 THEN CONCAT(\"<font style=\'color:red;\'>*</font>\",".$NameField.")
						ELSE ".$NameField."
					END";
	
			
		$sql = "SELECT 
		DISTINCT A.UserID, $name_field AS StudentName, A.ClassName, A.ClassNumber,
					D.RecordStatus, D.RecordID, D.DateModified, 
					C.HandinConfirmUserID, C.HandinConfirmTime, C.Title, C.DueDate,
                    D.SuppRecordStatus, A.RecordStatus AS StudentStatus, IF (A.RecordStatus = 3, D.StudentID, '-1') as LeaveStudent
				FROM INTRANET_USER AS A 
					LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS B ON (A.UserID = B.UserID)
					LEFT OUTER JOIN INTRANET_HOMEWORK AS C ON (B.SubjectGroupID = C.ClassGroupID)
					LEFT JOIN INTRANET_HOMEWORK_HANDIN_LIST AS D ON (D.HomeworkID = $hid AND A.UserID = D.StudentID)
					LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID = C.ClassGroupID)
				WHERE C.HomeworkID = $hid $additionCond ";
	
		switch($order_field){
			case 1 : $sort_str = " ORDER BY A.ClassName,A.ClassNumber ";break;
			case 2 : $sort_str = " ORDER BY StudentName "; break;
			case 3 : $sort_str = " ORDER BY E.RecordStatus "; break;
			default: $sort_str = " ORDER BY A.ClassName , A.ClassNumber ";
		}
		
		$sort_str .= $order_str;
		$sql .= $sort_str;
		$result = $this->returnArray($sql,10);
		
		return $result;
	}
  	
	function IsTeacherTeachingTargetSubjectGroup($UserID, $SubjectGroupID) {
		$sql = "SELECT UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE SubjectGroupID = '$SubjectGroupID'";
		$teacher_list = $this->returnVector($sql);
		return in_array($UserID, (array)$teacher_list);
	}
	
	# Used - index.php in Subject Leader, index.php, history.php, add.php in Homework List, Weekly Homework List
	function getTeachingSubjectList($userID="", $yearID="", $yearTermID="", $classID="", $own=0) {
		global $intranet_session_language;
		
		$termConds = ($yearTermID=="")? "" : " AND B.YearTermID = $yearTermID";
		
		# Class filtering
		// Selected Class
		if($classID != "") {
			$tableJoin =
			"
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = F.UserID AND ycu.YearClassID = '$classID')
			";
			$additionConds = " AND ycu.YearClassID = '$classID'";
			$additionConds2 = " OR ycu.YearClassID = '$classID'";
			$clsConds = " AND ycu.YearClassID = '$classID'";
		}
		// All Classes
		else {
			$clsInfo = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $userID);		# Get Class - Current Year Class Teacher
			for($i=0; $i<sizeof($clsInfo); $i++) {
				$clsIDAry[] = $clsInfo[$i][0];
			}
			$additionConds2 = (sizeof((array)$clsIDAry)>0) ? " OR yct.YearClassID IN (".implode(',',$clsIDAry).")" : "";
			$additionConds2 = " OR A.UserID = $userID";
		}
		
		# Subject Group filtering
		if($this->ClassTeacherCanViewHomeworkOnly) {
			$sgAry = $this->getSubjectGroupIDByTeacherID($userID, $yearID, $yearTermID);
			for($a=0; $a<count($sgAry); $a++) {
				$sgConds .= $delim.$sgAry[$a][0];
				$delim = ", ";
			}
			if($sgConds != "") $sgConds = " AND B.SubjectGroupID IN ($sgConds)";
		}
		
		//if($classID && ($userID=="" || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $this->isViewerGroupMember($userID)))
		if(!$own && ($userID=="" || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $this->isViewerGroupMember($userID)))
		{
			# All Subjects
			$sql = "SELECT C.RecordID, IF('$intranet_session_language'='en', C.EN_DES, C.CH_DES) AS Subject
					FROM SUBJECT_TERM_CLASS AS A
						INNER JOIN SUBJECT_TERM AS B ON  B.SubjectGroupID = A.SubjectGroupID
						INNER JOIN ASSESSMENT_SUBJECT AS C ON C.RecordID = B.SubjectID
						INNER JOIN ACADEMIC_YEAR_TERM AS D ON D.YearTermID = B.YearTermID
						INNER JOIN ACADEMIC_YEAR AS E ON E.AcademicYearID = D.AcademicYearID
						INNER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID = B.SubjectGroupID)
						$tableJoin
					WHERE E.AcademicYearID = $yearID $termConds $additionConds
					GROUP BY C.RecordID 
					ORDER BY C.DisplayOrder";
		}
		else
		{
			# Class filtering - Current Year Class Teacher
			$clsInfo2 = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $userID);
			for($i=0; $i<sizeof($clsInfo2); $i++) {
				$clsIDAry2[] = $clsInfo2[$i][0];
			}
			$additionConds2 = (sizeof((array)$clsIDAry2)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry2).")" : " AND yct.YearClassID IN ('')";
			
			# Own class and Own group Subjects ($_SESSION['isTeaching']==true)
			if($this->ClassTeacherCanViewHomeworkOnly) {
				$sql = "SELECT
							C.RecordID,
							IF('$intranet_session_language'='en', C.EN_DES, C.CH_DES) AS Subject
						FROM
							ASSESSMENT_SUBJECT as C
							INNER JOIN SUBJECT_TERM as B ON (B.SubjectID = C.RecordID)
							INNER JOIN ACADEMIC_YEAR_TERM AS D ON (D.YearTermID = B.YearTermID)
							LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID = B.SubjectGroupID)
							LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = F.UserID)
							LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID = ycu.YearClassID $additionConds2)
							LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as A ON (B.SubjectGroupID = A.SubjectGroupID AND F.SubjectGroupID = A.SubjectGroupID)
							INNER JOIN SUBJECT_TERM_CLASS_USER G ON (G.SubjectGroupID = B.SubjectGroupID)
						WHERE
							(yct.UserID = '$userID' OR A.UserID = $userID) AND
							D.AcademicYearID = '$yearID'
							$clsConds $termConds $sgConds
						GROUP BY C.RecordID 
						ORDER BY C.DisplayOrder";
			}
			else {
				$sql = "SELECT
							C.RecordID,
							IF('$intranet_session_language'='en', C.EN_DES, C.CH_DES) AS Subject
						FROM
							ASSESSMENT_SUBJECT as C
							INNER JOIN SUBJECT_TERM as B ON (B.SubjectID = C.RecordID)
							INNER JOIN ACADEMIC_YEAR_TERM AS D ON (D.YearTermID = B.YearTermID)
							LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID = B.SubjectGroupID)
							LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = F.UserID)
							LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID = ycu.YearClassID $additionConds2)
							LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as A ON (B.SubjectGroupID = A.SubjectGroupID AND F.SubjectGroupID = A.SubjectGroupID)
							INNER JOIN SUBJECT_TERM_CLASS_USER G ON (G.SubjectGroupID = B.SubjectGroupID)
						WHERE
							(yct.UserID = '$userID' OR A.UserID = $userID) AND
							D.AcademicYearID = '$yearID'
							$clsConds $termConds
						GROUP BY C.RecordID 
						ORDER BY C.DisplayOrder";
			}
		}
		
		$temp = $this->returnArray($sql);
		return $temp;
	}

	function getCurrentTeachingSubjects($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true,$firstValueValue=""){
		$x = "<SELECT $attrib>\n";
		$empty_selected = ($selected == '')? "SELECTED":"";
		if($displayAllClassValue)
		{
			$x .= "<OPTION value='". $firstValueValue ."' $empty_selected>$firstValue</OPTION>\n";
		}

		for ($i=0; $i < sizeof($result); $i++)
		{
			list($id, $name) = $result[$i];
			$selected_str = ($id==$selected? "SELECTED":"");
			$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
		}
		$x .= "</SELECT>\n";

		return $x;
	}

	function getTeachingSubjectGroupList($userID="", $subjectID="", $yearID="", $yearTermID="", $classID="", $own=0){
		global $intranet_session_language;
			
		$subjectID = $subjectID!="-1" ? $subjectID : "";
			
		if($classID!="") {
			$tableJoin = "

						LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID)
					";
			$additionConds = " AND ycu.YearClassID='$classID'";
		}
			
		$semAry = getSemesters($yearID);
		$termIDAry = array_keys($semAry);
			
		//$classID &&
		if(!$own && ($userID=="" || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $this->isViewerGroupMember($userID)))
		{
			# admin / non-teaching staff can view all homework, therefore can select all subject groups
			$conds = ($yearTermID=="")? "AND c.YearTermID IN (".implode(',',$termIDAry).")" : "AND c.YearTermID = '$yearTermID'";

			if($subjectID!=""){
				$sql = "SELECT a.SubjectGroupID,
				IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup
				From SUBJECT_TERM_CLASS As a
				INNER JOIN SUBJECT_TERM As b ON b.SubjectGroupID = a.SubjectGroupID
				INNER JOIN ACADEMIC_YEAR_TERM As c ON c.YearTermID = b.YearTermID
				INNER JOIN ACADEMIC_YEAR As d ON d.AcademicYearID = c.AcademicYearID
				INNER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=b.SubjectGroupID)
				$tableJoin
				WHERE b.SubjectID = '$subjectID' AND d.AcademicYearID = '$yearID' $conds $additionConds
				GROUP BY b.SubjectGroupID
				ORDER BY SubjectGroup";
					
			}
			else
			{
					
				$sql = "SELECT a.SubjectGroupID,
				IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup
				From SUBJECT_TERM_CLASS As a
				INNER JOIN SUBJECT_TERM As b ON b.SubjectGroupID = a.SubjectGroupID
				INNER JOIN ACADEMIC_YEAR_TERM As c ON c.YearTermID = b.YearTermID
				INNER JOIN ACADEMIC_YEAR As d ON d.AcademicYearID = c.AcademicYearID
				INNER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=b.SubjectGroupID)
				$tableJoin
				WHERE d.AcademicYearID = '$yearID' $conds $additionConds
				GROUP BY b.SubjectGroupID
				ORDER BY SubjectGroup";
				//echo $sql;
			}

		}
		else		# teaching staff
		{
			$conds = ($yearTermID=="")? " AND ayt.YearTermID IN (".implode(',',$termIDAry).")" : " AND ayt.YearTermID = $yearTermID";
			$SGconds = ($yearTermID=="")? " AND ayt.YearTermID IN (".implode(',',$termIDAry).")" : " AND ayt.YearTermID = $yearTermID";
			$SubjectIDCond = ($subjectID!="")? " and st.SubjectID = '".$subjectID."'":"";

			// 				if($this->ClassTeacherCanViewHomeworkOnly) {
			// 					//$sgAry = $this->getTeachingSubjectGroupList($userID, $subjectID, $yearID, $yearTermID, $classID);
			// 					//debug($sgAry);
			// 				}
			/*
				$AcademicYearID = ($yearID == "")? Get_Current_Academic_Year_ID():$yearID;
				$clsInfo = $this->getClassInfoByTeacherID($AcademicYearID, $userID);	# get classID which is user is the class teacher
				for($i=0; $i<sizeof($clsInfo); $i++)
					$clsIDAry[] = $clsInfo[$i][0];
					$clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : "";
					$newConds .= sizeof($clsIDAry)==0 ? "" : " AND ycu.YearClassID IN (".implode(',',$clsIDAry).")";
					*/
				$yearTermConds = ($yearTermID=="") ? "st.YearTermID IN (".implode(',',$termIDAry).")" : "st.YearTermID='$yearTermID'";

				/* #ORIGINAL#
				 $sql = "
				 SELECT
				 IF(stc.SubjectGroupID IS NOT NULL,stc.SubjectGroupID,stc1.SubjectGroupID) as SubjectGroupID,
				 IF (stc.SubjectGroupID IS NOT NULL,
				 IF('".$intranet_session_language."' = 'en', stc.ClassTitleEN, stc.ClassTitleB5),
				 IF('".$intranet_session_language."' = 'en', stc1.ClassTitleEN, stc1.ClassTitleB5)
				 )
				 AS SubjectGroup
				 From
				 ACADEMIC_YEAR as y
				 INNER JOIN
				 ACADEMIC_YEAR_TERM as ayt
				 on (y.AcademicYearID = ayt.AcademicYearID
				 ".$conds.")
				 LEFT JOIN
				 SUBJECT_TERM as st
				 on (ayt.YearTermID = st.YearTermID
				 ".$SubjectIDCond.")
				 LEFT JOIN
				 SUBJECT_TERM_CLASS_TEACHER as stct
				 on (st.SubjectGroupID = stct.SubjectGroupID
				 and stct.UserID = '".$userID."')
				 LEFT JOIN
				 SUBJECT_TERM_CLASS as stc
				 on
				 (stct.SubjectGroupID = stc.SubjectGroupID)
				 LEFT JOIN
				 YEAR_CLASS as yc
				 on
				 (ayt.AcademicYearID = yc.AcademicYearID)
				 LEFT JOIN
				 YEAR_CLASS_TEACHER as yct
				 on
				 (yc.YearClassID = yct.YearClassID
				 and
				 yct.UserID = '".$userID."')
				 LEFT JOIN
				 YEAR_CLASS_USER as ycu
				 on (yct.YearClassID = ycu.YearClassID)
				 LEFT JOIN
				 SUBJECT_TERM_CLASS_USER stcu
				 on
				 (ycu.UserID = stcu.UserID)
				 LEFT JOIN
				 SUBJECT_TERM_CLASS stc1
				 on
				 (st.SubjectGroupID = stc1.SubjectGroupID)
				 WHERE
				 (stc.SubjectGroupID IS NOT NULL OR stc1.SubjectGroupID IS NOT NULL) AND
				 (yct.UserID='$userID' OR stct.UserID='$userID')
				 	
				 GROUP BY
				 SubjectGroupID
				 ORDER BY
				 SubjectGroup
				 ";*/
					
				// 2013-1016-1517-25156
				//							$sql = "
				//								SELECT
				//									IF(stc.SubjectGroupID IS NOT NULL,stc.SubjectGroupID,stc1.SubjectGroupID) as SubjectGroupID,
				//									IF (stc.SubjectGroupID IS NOT NULL,
				//										IF('".$intranet_session_language."' = 'en', stc.ClassTitleEN, stc.ClassTitleB5),
				//										IF('".$intranet_session_language."' = 'en', stc1.ClassTitleEN, stc1.ClassTitleB5)
				//										)
				//									AS SubjectGroup
				//								From
				//									ACADEMIC_YEAR as y
				//									INNER JOIN ACADEMIC_YEAR_TERM as ayt
				//										on (y.AcademicYearID = ayt.AcademicYearID $conds)
				//									INNER JOIN SUBJECT_TERM as st
				//										on (ayt.YearTermID = st.YearTermID $SubjectIDCond)
				//									INNER JOIN SUBJECT_TERM_CLASS as stc
				//										on (st.SubjectGroupID = stc.SubjectGroupID)
				//									LEFT JOIN SUBJECT_TERM_CLASS_TEACHER as stct
				//										on (st.SubjectGroupID = stct.SubjectGroupID and stct.UserID = '$userID')
				//									LEFT JOIN SUBJECT_TERM_CLASS_USER stcu
				//										on (stcu.SubjectGroupID=st.SubjectGroupID)
				//									LEFT JOIN YEAR_CLASS_USER ycu
				//										ON (ycu.UserID=stcu.UserID AND st.SubjectGroupID=stcu.SubjectGroupID)
				//									LEFT JOIN YEAR_CLASS as yc
				//										on (ycu.YearClassID= yc.YearClassID AND yc.AcademicYearID='$yearID')
				//									LEFT JOIN YEAR_CLASS_TEACHER as yct
				//										on (yc.YearClassID = yct.YearClassID and yct.UserID = '$userID')
				//									LEFT JOIN SUBJECT_TERM_CLASS stc1
				//										on (st.SubjectGroupID = stc1.SubjectGroupID)
				//								WHERE
				//									(stc.SubjectGroupID IS NOT NULL OR stc1.SubjectGroupID IS NOT NULL) AND
				//									(yct.UserID='$userID' OR stct.UserID='$userID')
				//									$additionConds
				//								GROUP BY
				//									SubjectGroupID
				//								ORDER BY
				//									SubjectGroup
				//							 ";

				// make sure the term is existing now
				$sql = "Select YearTermID From ACADEMIC_YEAR_TERM as ayt Where 1 $conds";
				$yearTermIdAry = $this->returnVector($sql);
				$yearTermIdConds = " AND st.YearTermID IN ('".implode("','", (array)$yearTermIdAry)."') ";

				// class filtering
				$classStudentIdConds = '';
				if($classID!="") {
					$sql = "Select UserID From YEAR_CLASS_USER as ycu Where ycu.YearClassID='$classID'";
					$classStudentIdAry = $this->returnVector($sql);

					$classStudentIdConds_class = " And ycu.UserID IN ('".implode("','", (array)$classStudentIdAry)."') ";
					$classStudentIdConds_subjectGroup = " And stcu.UserID IN ('".implode("','", (array)$classStudentIdAry)."') ";
				}

				// get teacher teaching class
				$sql = "	Select
				yct.YearClassID
				From
				YEAR_CLASS as yc
				Inner Join YEAR_CLASS_TEACHER as yct On (yc.YearClassID = yct.YearClassID)
				Where
				yct.UserID = '$userID'
				AND yc.AcademicYearID = '$yearID'";
				$yearClassIdAry = $this->returnVector($sql);
				$numOfYearClass = count($yearClassIdAry);
				$yearClassIdConds = " AND ycu.YearClassID IN ('".implode("','", (array)$yearClassIdAry)."') ";

				// get class student's subject group
				$sql = "	Select
				st.SubjectGroupID
				From
				YEAR_CLASS_USER as ycu
				Inner Join SUBJECT_TERM_CLASS_USER as stcu ON (ycu.UserID = stcu.UserID)
				Inner Join SUBJECT_TERM as st ON (stcu.SubjectGroupID = st.SubjectGroupID)
				Where
				1
				$yearClassIdConds
				$yearTermIdConds
				$SubjectIDCond
				$classStudentIdConds_class
				Group By
				st.SubjectGroupID
				";
				$classTeacherSubjectGroupIdAry = $this->returnVector($sql);

				// get teaching subject group
				$sql = "	Select
												st.SubjectGroupID
										From
												SUBJECT_TERM_CLASS_TEACHER as stct
												Inner Join SUBJECT_TERM as st ON (stct.SubjectGroupID = st.SubjectGroupID)
												Left Outer Join SUBJECT_TERM_CLASS_USER as stcu ON (stct.SubjectGroupID = stcu.SubjectGroupID)
										Where
												stct.UserID = '".$userID."'
												$yearTermIdConds
												$SubjectIDCond
												$classStudentIdConds_subjectGroup
												Group By
												st.SubjectGroupID
												";
												$subjectTeacherSubjectGroupIdAry = $this->returnVector($sql);
													
												// get subject group info
												$sql = "Select
											stc.SubjectGroupID,
											IF('".$intranet_session_language."' = 'en', stc.ClassTitleEN, stc.ClassTitleB5) as SubjectGroup
									From
											SUBJECT_TERM_CLASS as stc
									Where
											stc.SubjectGroupID In ('".implode("','", (array)$classTeacherSubjectGroupIdAry)."')
											Or stc.SubjectGroupID In ('".implode("','", (array)$subjectTeacherSubjectGroupIdAry)."')
									GROUP BY
											stc.SubjectGroupID
									ORDER BY
											SubjectGroup
									";
												//echo $sql.'<br>';
												/*
												 if($subjectID!=""){
												 $sql = "SELECT
												 a.SubjectGroupID,
												 IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup
												 From
												 SUBJECT_TERM_CLASS As a
												 LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER As b ON (b.SubjectGroupID = a.SubjectGroupID)
												 LEFT OUTER JOIN SUBJECT_TERM As c ON (c.SubjectGroupID = a.SubjectGroupID)
												 LEFT OUTER JOIN ACADEMIC_YEAR_TERM As d ON (d.YearTermID = c.YearTermID)
												 LEFT OUTER JOIN ACADEMIC_YEAR As e ON (e.AcademicYearID = d.AcademicYearID)
												 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=b.SubjectGroupID)
												 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=c.SubjectGroupID)
												 LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
												 LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID)

												 WHERE
												 c.SubjectID = '$subjectID' AND
												 (b.UserID='$userID'  OR (yct.UserID='$userID' $clsConds)) AND
												 e.AcademicYearID = '$yearID'
												 $additionConds
												 $conds
												 GROUP BY
												 b.SubjectGroupID
												 ORDER BY
												 SubjectGroup";
												 //echo $sql;
												 }
												 else{
												 	
												 $yearTermConds = ($yearTermID=="") ? "st.YearTermID IN (".implode(',',$termIDAry).")" : "st.YearTermID='$yearTermID'";
												 	
												 $sql = "SELECT
												 stc.SubjectGroupID,
												 IF('$intranet_session_language' = 'en', stc.ClassTitleEN, stc.ClassTitleB5) AS SubjectGroup
												 FROM
												 SUBJECT_TERM st LEFT OUTER JOIN
												 SUBJECT_TERM_CLASS stc ON (st.SubjectGroupID=stc.SubjectGroupID) LEFT OUTER JOIN
												 SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=st.SubjectGroupID) INNER JOIN
												 SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) LEFT OUTER JOIN
												 YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) LEFT OUTER JOIN
												 YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
												 YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)

												 WHERE
												 	
												 ((yct.UserID='$userID' $newConds) OR ($yearTermConds AND stct.UserID='$userID'))
												 $additionConds
												 GROUP BY
												 st.SubjectGroupID";

												 //echo $sql."<br>";
												 }
												 */
		}
			
		return $this->returnArray($sql,2);
	}

	function getCurrentTeachingGroups($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
		$x = "<SELECT $attrib>\n";
		$empty_selected = ($selected == '')? "SELECTED":"";
		if($displayAllClassValue)
		{
			$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
		}

		for ($i=0; $i < sizeof($result); $i++)
		{
			list($id, $name) = $result[$i];
			$selected_str = ($id==$selected? "SELECTED":"");
			$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
		}
		$x .= "</SELECT>\n";

		return $x;
	}
    
	# used by index.php, history.php add.php in homeworklist, weekly homework list
	function getStudyingSubjectList($userID, $leader="", $yearID="", $yearTermID="")
	{
		global $intranet_session_language;
		
		$userID = IntegerSafe($userID);
		$yearID = IntegerSafe($yearID);
		$yearTermID = IntegerSafe($yearTermID);
		$conds = ($yearTermID=="")? "" : "AND d.YearTermID = '$yearTermID'";
		
		if($leader != "")
		{
			$sql = "SELECT
                        a.RecordID,
            			IF('$intranet_session_language'='en', a.EN_DES, a.CH_DES) AS Subject
        			FROM ASSESSMENT_SUBJECT AS a
            			INNER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID
            			INNER JOIN INTRANET_SUBJECT_LEADER AS c ON  b.SubjectGroupID = c.ClassID
            			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = b.YearTermID
            			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
        			WHERE
                        c.UserID IN ($userID) AND e.AcademicYearID = '$yearID' $conds AND  
                        c.RecordStatus = 1 
                    GROUP BY a.RecordID 
                    ORDER BY Subject";
		}
		else
		{
			$sql = "SELECT
                        c.RecordID,
            			IF('$intranet_session_language'='en', c.EN_DES, c.CH_DES) AS Subject
        			FROM SUBJECT_TERM_CLASS_USER AS a
            			INNER JOIN SUBJECT_TERM AS b ON  b.SubjectGroupID = a.SubjectGroupID
            			INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = b.SubjectID
            			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = b.YearTermID
            			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
        			WHERE
                        a.UserID IN ($userID) AND e.AcademicYearID = '$yearID'
            			$conds
        			GROUP BY c.RecordID 
                    ORDER BY Subject";
		}
		
		$temp = $this->returnArray($sql);
		return $temp;
	}

	function getCurrentStudyingSubjects($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
		$x = "<SELECT $attrib>\n";
		$empty_selected = ($selected == '')? "SELECTED":"";
		if($displayAllClassValue)
		{
			$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
		}

		for ($i=0; $i < sizeof($result); $i++)
		{
			list($id, $name) = $result[$i];
			$selected_str = ($id==$selected? "SELECTED":"");
			$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
		}
		$x .= "</SELECT>\n";

		return $x;
	}

	# used by index.php, history.php add.php in homeworklist with subject leader allowed
	function getLeaderingSubjectGroupList($userID, $subjectID, $yearID="", $yearTermID=""){
		global $intranet_session_language;
		$conds = ($yearTermID=="")? "" : "AND d.YearTermID = $yearTermID";
			
		$sql = "SELECT a.SubjectGroupID,
		IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup
		From SUBJECT_TERM_CLASS As a
		INNER JOIN INTRANET_SUBJECT_LEADER As b ON b.ClassID = a.SubjectGroupID
		INNER JOIN SUBJECT_TERM As c ON c.SubjectGroupID = a.SubjectGroupID
		INNER JOIN ACADEMIC_YEAR_TERM As d ON d.YearTermID = c.YearTermID
		INNER JOIN ACADEMIC_YEAR As e ON e.AcademicYearID = d.AcademicYearID
		WHERE b.UserID=$userID AND b.RecordStatus = '1' AND b.SubjectID = $subjectID
		AND e.AcademicYearID = $yearID $conds
		ORDER BY SubjectGroup";
			
			
		return $this->returnArray($sql,2);
	}

	# used by index.php, history.php in homeworklist, weekly homework list
	function getStudyingSubjectGroupList($userID, $subjectID="", $AcademicYearID="", $YearTermID="")
	{
		global $intranet_session_language;
			
		if ($subjectID != "") {
			$conds = "and st.SubjectID = $subjectID ";
		}
			
		if(is_array($YearTermID)) {
			$conds .= " AND st.YearTermID IN (".implode(',', $YearTermID).")";
		}
		else if($YearTermID!="") {
			$conds .= " AND st.YearTermID='$YearTermID'";
		} else if($AcademicYearID!="") {
			$allSemesters = getSemesters($AcademicYearID);
			if(sizeof($allSemesters)>0) {
				$delim = "";
				$x = "";
				foreach($allSemesters as $termId=>$termName) {
					$x .= $delim.$termId;
					$delim = ", ";
				}
				$conds .= " AND st.YearTermID IN ($x)";
			}
		}
		$YearTermInfo = getCurrentAcademicYearAndYearTerm();
		$SubjectGroupTitleField = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
		$sql = "SELECT
							stc.SubjectGroupID,
							".$SubjectGroupTitleField." as SubjectGroup
						From
							SUBJECT_TERM_CLASS_USER As stcu
							INNER JOIN
							SUBJECT_TERM_CLASS As stc
							ON
								stcu.UserID='".$userID."'
								and
								stcu.SubjectGroupID = stc.SubjectGroupID
							INNER JOIN
							SUBJECT_TERM As st
							ON
								stc.SubjectGroupID = st.SubjectGroupID
					
								".$conds."
							INNER JOIN
							ASSESSMENT_SUBJECT as s
							on st.SubjectID = s.RecordID
						ORDER BY
							s.DisplayOrder ASC, stc.ClassTitleEN";
		//debug_r($sql);
		return $this->returnArray($sql,2);
	}

	function getStudyingSubjectGroupListByLeader($userID, $yearID="", $yearTermID="")
	{
			
		global $intranet_session_language;
			
		if($yearID!="" && $yearTermID=="") {
			$yearTermInfo = getSemesters($yearID);
			$_key = array_keys($yearTermInfo);
			for($i=0, $i_max=sizeof($yearTermInfo); $i<$i_max; $i++) {
				$yearTermID[] = $_key[$i];
			}
			$yearTermConds = " AND st.YearTermID IN ('".implode('\',\'', $yearTermID)."')";
		} else {
			$yearTermConds = " AND st.YearTermID = '$yearTermID'";
		}
			
		$conds = ($yearTermID=="")? "" : "AND d.YearTermID = $yearTermID";

		$sql = "SELECT stc.SubjectGroupID,
		IF('$intranet_session_language'='en', stc.ClassTitleB5, stc.ClassTitleEN) AS SubjectGroup
		FROM
		INTRANET_SUBJECT_LEADER AS sl
		INNER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=sl.ClassID)
		INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stc.SubjectGroupID $yearTermConds)
		WHERE
		sl.UserID='$userID' AND sl.RecordStatus=1
		GROUP BY
		stc.SubjectGroupID";
			
		$temp = $this->returnArray($sql);
		return $temp;
	}


	# used by index.php, history.php add.php in homeworklist, weekly homework list
	function getCurrentStudyingGroups($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
		$x = "<SELECT $attrib>\n";
		$empty_selected = ($selected == '')? "SELECTED":"";
		if($displayAllClassValue)
		{
			$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
		}

		for ($i=0; $i < sizeof($result); $i++)
		{
			list($id, $name) = $result[$i];
			$selected_str = ($id==$selected? "SELECTED":"");
			$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
		}
		$x .= "</SELECT>\n";

		return $x;
	}


	# used by index.php, history.php in homeworklist
	function getChildrenList($userID){
		global $intranet_session_language;
			
		$name = getNameFieldByLang("b.");
		//IF('$intranet_session_language'='en', Concat(b.EnglishName,' (',b.ClassName,'-',b.ClassNumber,')'), b.EnglishName)
		$sql = "SELECT
		a.StudentID,
		$name
		From
		INTRANET_PARENTRELATION AS a
		LEFT OUTER JOIN INTRANET_USER AS b ON b.UserID = a.StudentID
		WHERE
		a.ParentID = $userID
		and b.ClassName is not NULL
		and (b.ClassNumber is not NULL and  b.ClassNumber > 0)
		";
		return $this->returnArray($sql,2);
	}

	# used by index.php, history.php in homeworklist
	function getCurrentChildren($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
		$x = "<SELECT $attrib>\n";
		$empty_selected = ($selected == '')? "SELECTED":"";
		if($displayAllClassValue)
		{
			$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
		}

		for ($i=0; $i < sizeof($result); $i++)
		{
			list($id, $name) = $result[$i];
			$selected_str = ($id==$selected? "SELECTED":"");
			$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
		}
		$x .= "</SELECT>\n";

		return $x;
	}

	function getClassName($classID){
		global $intranet_session_language;
			
		$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) AS Class From YEAR_CLASS WHERE YearClassID = $classID";
		$result = $this->returnArray($sql,1);
			
		return $result[0]['Class'];
	}

	function getSubjectName($subjectID){
		global $intranet_session_language;
			
		$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) AS Subject From ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
		$result = $this->returnArray($sql,1);
			
		return $result[0]['Subject'];
	}

	function getSubjectGroupName($subjectGroupID){
		global $intranet_session_language;
			
		$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) AS SubjectGroup From SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
		$result = $this->returnArray($sql,1);
			
		return $result[0]['SubjectGroup'];
	}


	/*# used by add.php in subject leader, and weekly homework list
		function getTeachingSubjectGroups($tags, $selected, $noFirst=0){
		global $UserID, $intranet_session_language;
			
		$sql = "SELECT a.SubjectGroupID, IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) From SUBJECT_TERM_CLASS As a, SUBJECT_TERM_CLASS_TEACHER As b
		WHERE a.SubjectGroupID = b.SubjectGroupID AND b.UserID=$UserID ORDER BY a.SubjectGroupID";
			
		$result = $this->returnArray($sql,2);
		return getSelectByArray($result,$tags,$selected,0,$noFirst);
		}

		# used by add.php in subject leader, and weekly homework list
		function getStudyingSubjectGroups($tags, $selected, $studentID="", $noFirst=0){
		global $UserID, $intranet_session_language;
			
		if($studentID!="")
			$uid=$studentID;
			else
				$uid=$UserID;

				$sql = "SELECT a.SubjectGroupID, IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) From SUBJECT_TERM_CLASS As a, SUBJECT_TERM_CLASS_USER As b
				WHERE a.SubjectGroupID = b.SubjectGroupID AND b.UserID=$uid ORDER BY a.SubjectGroupID";
					
				$result = $this->returnArray($sql,2);
				return getSelectByArray($result,$tags,$selected,0,$noFirst);
				}

				# used by weekly homework list,
				function getSubjectGroups($tags, $selected, $leader="", $noFirst=0){
				global $UserID, $intranet_session_language;
					
				if($leader!="")
					$result = $this->getLeaderingSubjectGroupList($UserID);

					else {
					$sql = "SELECT a.SubjectGroupID, IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup From SUBJECT_TERM_CLASS As a ORDER BY a.SubjectGroupID";
					$result = $this->returnArray($sql,2);
					}
					return getSelectByArray($result,$tags,$selected,0,$noFirst);
					}

					# used by add.php in subject leader
					function getChildren($tags, $selected, $noFirst=0){
					global $UserID, $intranet_session_language;
						
					$sql = "SELECT a.StudentID, IF('$intranet_session_language'='en', Concat(b.EnglishName,' (',b.ClassName,'-',b.ClassNumber,')'), Concat(b.ChineseName,' (',b.ClassName,'-',b.ClassNumber,')'))
					From INTRANET_PARENTRELATION AS a LEFT OUTER JOIN INTRANET_USER AS b ON b.UserID = a.StudentID WHERE a.ParentID = $UserID";
						
					$result = $this->returnArray($sql,2);
					return getSelectByArray($result,$tags,$selected,0,$noFirst);
					}
					*/

		# Used - weeklyhomeworklist.php
		function weeklyHomeworkList($ts, $studentID="", $condition="", $leader="", $print="", $classID="")
		{
            $sql = $this->getHomeworkListSql($ts, $studentID, $condition, $leader, $print, $classID, $period="weekly");
			
			if($print!="") {
				return $this->printWeeklyHomeworklist($sql,$ts);
			}
			else {
				return $this->displayWeeklyHomeworklist($sql,$ts);
			}
		}

		# Used - monthlyList.php
		function monthlyHomeworkList($ts, $studentID="", $condition="", $leader="", $print="", $classID="")
		{
			global $UserID, $intranet_session_language;
			
			$month = date('m',$ts);
			$year = date('Y',$ts);
			$next = mktime(0,0,0,$month+1,1,$year);
			$start = $ts;
			$end = $next;
			//$end = $ts+604800;
			
			# Fields
			if($this->useStartDateToGenerateList) {
				$date_conds = "UNIX_TIMESTAMP(a.StartDate) >= $start AND UNIX_TIMESTAMP(a.StartDate)<$end";
			}
			else {
				$date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";
			}
			
			$username_field = getNameFieldWithClassNumberByLang("c.");
			
			$fields = "a.HomeworkID, IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES),
						IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5),a.StartDate, a.DueDate, a.Loading,
						a.Title, a.Description, a.ClassGroupID, a.SubjectID, a.AttachmentPath, IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5), a.TypeID";
			
			# Homework Admin / Non-Teacher Staff
			if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
			{
				$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
			 					LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
				$conds = "$condition AND $date_conds";
			}
			# Teacher Staff
			else if($_SESSION['UserType']==USERTYPE_STAFF && ($_SESSION['isTeaching'] || !$this->isViewerGroupMember($UserID)))
			{
				# Class filtering - Current Year Class Teacher
				$clsInfo = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $UserID);
				for($i=0; $i<sizeof($clsInfo); $i++){
					$clsIDAry[] = $clsInfo[$i][0];
				}
				//$clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : "";
				$clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : " AND yct.YearClassID IN ('')";
				
				$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
								LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as c ON c.SubjectGroupID = a.ClassGroupID
								LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
								INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
								INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
								LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID $clsConds)";
				
				if(!$this->isViewerGroupMember($UserID)) {
					$conds = "$condition AND (c.UserID = $UserID OR yct.UserID=$UserID) AND
					$date_conds";
				}
				else {
					$conds = $condition;
				}
			}
			# Subject Leader
			else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && $leader==1){
				$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
								 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
								 LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
				$conds = "$condition AND c.UserID = $UserID AND $date_conds";
			}
			# Others
			else {
				$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
							  LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
							  LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
				
				if ($studentID!=""){
					$conds = "$condition AND c.UserID = $studentID AND $date_conds";
				}
				# Staff
				else if($_SESSION['UserType']==1) {
					$conds = "$condition AND c.UserID = $UserID AND $date_conds";
				}
				else {
					$conds = "$condition AND $date_conds";
				}
			}
			
			if($classID!="") {
				$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=d.SubjectGroupID)
								LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=F.UserID AND ycu2.YearClassID='$classID')";
				$conds .= " AND ycu2.YearClassID='$classID'";
			}
			
			if($conds != "") $conds = " WHERE $conds";
			
			$sql = "SELECT $fields FROM $dbtables $conds GROUP BY HomeworkID ORDER BY a.DueDate";
			
			if($print!="") {
				return $this->printMonthlyHomeworklist($sql,$ts, $month, $year);
			}
			else {
				return $this->displayMonthlyHomeworklist($sql,$ts, $month, $year);
			}
		}
		
		function displayWeeklyHomeworklist($sql,$ts){
			global $image_path,$LAYOUT_SKIN,$intranet_session_language,$UserID,$i_frontpage_day;
			global $i_Homework_minimum,$i_Homework_morethan;
			global $i_AnnouncementAttachment,$Lang, $i_Homework_HomeworkType;

			$hwImagePath = "$image_path/homework/$intranet_session_language/hw";

			# Get Homework data
			$data = $this->returnArray($sql,15);

			# Set $ts to sunday of the week
			$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
			$today = date('Y-m-d');


			# Get Cycle days
			$b = new libcycleperiods();
			$cStart = date('Y-m-d',$ts);
			$cEnd = date('Y-m-d',$ts+6*86400);
			$cycle_days = $b->getCycleInfoByDateRange($cStart,$cEnd);


			for ($i=0; $i<7; $i++)
			{
		  $week_ts[] = $ts+$i*86400;

			}
			/*
			 $x .= "<tr>";
			 	
			 for ($i=0; $i<7; $i++)
			 {
			 $current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
			 $day = date('l',$week_ts[$i]);

			 $x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['WeekDay'][$i]."<br>$current</td>";
			 }
			 $x .="</tr>";
			 */
			for ($i=0; $i<7; $i++)
			{
				$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
				$day = date('l',$week_ts[$i]);

				$dateAry[] .= "<td align='left'>".$Lang['SysMgr']['Homework']['WeekDay'][$i]."<br>$current</td>";
			}
				

			if(sizeof($data)!=0){
				for ($i=0; $i<7; $i++)
				{
					$x .="<tr bgcolor='#CCCCCC'>";
					$x .= $dateAry[$i];
					$x .="</tr>";
					$x .="<tr>";
					$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
						
					if ($today == $current)
					{
						//$bgcolor = "bgcolor=#cccccc";
					}
					else
					{
						$bgcolor = "";
					}

					$x .="<td valign=\"top\" $bgcolor><table>";
					$count = 0;
					$cell = "";
						
					for($j=0;$j<sizeof($data);$j++){

						list($hid,$subject,$subjectgroup,$startdate,$duedate,$loading,$title,$description,$subjectGroupID,$subjectID,$AttachmentPath, $TypeID) = $data[$j];

						$displayDate = ($this->useStartDateToGenerateList) ? $startdate : $duedate;		// modified by Henry on 20091215
						//echo $current.'/'.$displayDate.'<br>';
						if ($current == $displayDate){
							if ($loading==0)
							{
								$tload = 0;
							}
							else if ($loading <= 16)
							{
								$tload = $loading/2;
							}
							else
							{
								$tload = "$i_Homework_morethan 8";
							}
								
							$useHwType = "";
							if($this->useHomeworkType) {
								$homeworkTypeInfo = $this->getHomeworkType($TypeID);
								$typeName = ($homeworkTypeInfo[0]['TypeName']) ? $homeworkTypeInfo[0]['TypeName'] : "---";
								$useHwType = "<em>".$i_Homework_HomeworkType.": </em>$typeName<br>";
							}

							$text = "<a class=tablelink href=javascript:viewHmeworkDetail($hid)>$title</a><span><em>".
									$Lang['SysMgr']['Homework']['Subject'].": </em>$subject<br><em>".
									$Lang['SysMgr']['Homework']['SubjectGroup'].": </em>$subjectgroup<br>".
									$useHwType.
									"<em>".$Lang['SysMgr']['Homework']['Workload'].": </em>$tload ".$Lang['SysMgr']['Homework']['Hours']."<br>";
									$text .= ($this->useStartDateToGenerateList) ? "<em>".$Lang['SysMgr']['Homework']['DueDate'].": </em>$duedate<br>" : "<em>".$Lang['SysMgr']['Homework']['StartDate'].": </em>$startdate<br>";

									/*if ($description != "")
									 $text .= " <img src=\"$hwImagePath/icon_alt.gif\" title=\"$description\"> ";
									 */
									if ($AttachmentPath!=NULL)
										$text .=" <img src=\"$hwImagePath/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\"> ";

										$text .="</span><br><br>";
											
											

										$cell .="<tr class='homework_day'>
										<td valign=\"top\"><div class='homework_list'>$text</div></td>
										</tr>";
										$count++;
						}
					}
					if ($count==0)
					{
						$cell = "&nbsp;";
					}
					$x .= "$cell</table></td>";
				}
				$x .="</tr>";
			}
			else{
				$x .="<tr>";
				$x .="<td align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>";
				$x .="</tr>";
			}
			return $x;
		}

		function printWeeklyHomeworklist($sql,$ts){
			global $image_path,$LAYOUT_SKIN,$intranet_session_language,$UserID,$i_frontpage_day;
			global $i_Homework_minimum,$i_Homework_morethan;
			global $i_AnnouncementAttachment,$Lang, $i_Homework_HomeworkType;

			$hwImagePath = "$image_path/homework/$intranet_session_language/hw";

			# Get Homework data
			$data = $this->returnArray($sql,15);


			# Set $ts to sunday of the week
			$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
			$today = date('Y-m-d');


			# Get Cycle days
			$b = new libcycleperiods();
			$cStart = date('Y-m-d',$ts);
			$cEnd = date('Y-m-d',$ts+6*86400);
			$cycle_days = $b->getCycleInfoByDateRange($cStart,$cEnd);


			for ($i=0; $i<7; $i++)
			{
		  $week_ts[] = $ts+$i*86400;

			}
			/*
			 $x .= "<tr>";
			 	
			 for ($i=0; $i<7; $i++)
			 {
			 $current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
			 $day = date('l',$week_ts[$i]);

			 $x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle' width='10%'>".$Lang['SysMgr']['Homework']['WeekDay'][$i]."<br>$current</td>";
			 }
			 $x .="</tr>";
			 */
			for ($i=0; $i<7; $i++)
			{
				$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
				$day = date('l',$week_ts[$i]);

				$dateAry[] .= "<td>".$Lang['SysMgr']['Homework']['WeekDay'][$i]."<br>$current</td>";
			}
				
			//$x .="<tr>";
				
			if(sizeof($data)!=0){
				for ($i=0; $i<7; $i++)
				{
					$x .="<tr bgcolor='#CCCCCC'>";
					$x .= $dateAry[$i];
					$x .="</tr>";
					$x .="<tr>";
					$current = date('Y-m-d',$week_ts[$i]);  #date('Y-m-d',$ts+$i*86400);
						
					if ($today == $current)
					{
						$bgcolor = "bgcolor=#cccccc";
					}
					else
					{
						$bgcolor = "";
					}

					$x .= "<td valign=\"top\" class='eHomeworktdborder eHomeworkprinttext'><table>";
					$count = 0;
					$cell = "";
						
					for($j=0;$j<sizeof($data);$j++){

						list($hid,$subject,$subjectgroup,$startdate,$duedate,$loading,$title,$description,$subjectGroupID,$subjectID,$AttachmentPath, $TypeID) = $data[$j];

						$displayDate = ($this->useStartDateToGenerateList) ? $startdate : $duedate;		// modified by Henry on 20091215

						if ($current == $displayDate){
							if ($loading==0)
							{
								$tload = 0;
							}
							else if ($loading <= 16)
							{
								$tload = $loading/2;
							}
							else
							{
								$tload = "$i_Homework_morethan 8";
							}
							$useHwType = "";
							if($this->useHomeworkType) {
								$homeworkTypeInfo = $this->getHomeworkType($TypeID);
								$typeName = ($homeworkTypeInfo[0]['TypeName']) ? $homeworkTypeInfo[0]['TypeName'] : "---";
								$useHwType = "<em>".$i_Homework_HomeworkType.": </em>$typeName<br>";
							}

							$text = $title.
							"<span><em>".$Lang['SysMgr']['Homework']['Subject'].": </em>$subject<br><em>".
							$Lang['SysMgr']['Homework']['SubjectGroup'].": </em>$subjectgroup<br>".
							$useHwType.
							"<em>".$Lang['SysMgr']['Homework']['Workload'].": </em>$tload ".$Lang['SysMgr']['Homework']['Hours']."<br>";
							$text .= ($this->useStartDateToGenerateList) ? "<em>".$Lang['SysMgr']['Homework']['DueDate'].": </em>$duedate<br>" : "<em>".$Lang['SysMgr']['Homework']['StartDate'].": </em>$startdate<br>";


							/*if ($description != "")
							 $text .= " <img src=\"$hwImagePath/icon_alt.gif\" title=\"$description\"> ";
							 if ($AttachmentPath!=NULL)
							 	$text .=" <img src=\"$hwImagePath/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\"> ";
							 	*/
							 $text .="</span><br><br>";
							 	
							 	

							 $cell .="<tr>
								<td valign=\"top\"><div class='homework_list'>$text</div></td>
								</tr>";
							 $count++;
						}
					}
					if ($count==0)
					{
						$cell = "&nbsp;";
					}
					$x .= "$cell</table></td>";
					$x .= "</tr>";
				}
			}
				
			else{
				$x .= "<tr>";
				$x .="<td align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>";
				$x .= "</tr>";
			}
			//$x .="</tr>";
			return $x;
		}

		# used by weeklyhomeworklist.php
		function exportWeeklyHomeworkList($ts, $studentID="", $condition="", $leader="", $classID="", $fromTs="", $toTs=""){
				
			global $UserID, $intranet_session_language;
				
			if($fromTs!="" && $toTs!="") {
				$start = $fromTs;
				$end = $toTs + 86400;	# original datetime is 00:00:00, therefore should count the 1 day after in SQL statement
			} else {
				$start = $ts;
				$end = $ts+604800;
			}
				
				
			if($this->useStartDateToGenerateList)
				$date_conds = "UNIX_TIMESTAMP(a.StartDate) >= $start AND UNIX_TIMESTAMP(a.StartDate)<$end";
				else
					$date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";
						
					// [DM#2934] Parent - Cannot export future homework
					if($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
						$date_conds .= " AND a.StartDate<=CURDATE()";

						$username_field = getNameFieldWithClassNumberByLang("c.");
							
						$fields = "a.HomeworkID, IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES),
			   IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5),a.StartDate, a.DueDate, a.Loading,
			   a.Title, a.Description, a.ClassGroupID, a.SubjectID, a.AttachmentPath, a.TypeID";

						if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
							$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";

							$conds = "$condition AND $date_conds";

						}
							
						else if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $this->isViewerGroupMember($UserID)){

							$clsInfo = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $UserID);	# get classID which is user is the class teacher
							for($i=0; $i<sizeof($clsInfo); $i++)
								$clsIDAry[] = $clsInfo[$i][0];
								$clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : "";
									
								$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
								LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as c ON c.SubjectGroupID = a.ClassGroupID
								LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
								INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
								INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
								LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID $clsConds)
								";
								if(!$this->isViewerGroupMember($UserID))
									$conds = "$condition AND (c.UserID = $UserID OR yct.UserID=$UserID) AND $date_conds";


						}
							
						else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && $leader==1){
								
							$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";

							$conds = "$condition AND c.UserID = $UserID AND $date_conds";
						}
							
						else {
								
							$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
				  LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
				  LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
								
							if ($studentID!=""){
								$conds = "$condition AND c.UserID = $studentID AND $date_conds";
							}

							else
								$conds = "$condition AND c.UserID = $UserID AND $date_conds";
						}
							
						if($classID!="") {
							$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=d.SubjectGroupID)
							LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=F.UserID AND ycu2.YearClassID='$classID')";
							$conds .= " AND ycu2.YearClassID='$classID'";
						}
							
						if($conds !="")
							$conds = " WHERE $conds";
								
							$sql = "SELECT $fields FROM $dbtables $conds GROUP BY HomeworkID ORDER BY a.DueDate";
							$data = $this->returnArray($sql,15);
							//echo $sql;
							return $data;
		}

		function displayMonthlyHomeworklist($sql, $ts, $month, $year){
			global $image_path,$LAYOUT_SKIN,$intranet_session_language,$UserID,$i_frontpage_day;
			global $i_Homework_minimum,$i_Homework_morethan;
			global $i_AnnouncementAttachment,$Lang;
			//echo $sql;
			$hwImagePath = "$image_path/homework/$intranet_session_language/hw";

			$start = mktime(0,0,0,$month,1,$year);
			$end = mktime(0,0,0,$month+1,1,$year);
				
			$lcal = new libcalevent();
			$row = $lcal->returnCalendarArray($month,$year);
			$lcycle = new libcycleperiods();
			$cycle_days = $lcycle->getCycleInfoByDateRange(date('Y-m-d',$start),date('Y-m-d',$end));
				
			# Get Homework data
			$data = $this->returnArray($sql,15);
				
			$this->calData = $data;
				
			//$x .= "<table width=90% border=1 cellpadding=1 cellspacing=0 bordercolordark=#CCCCCC class=body bodercolorlight=#CCCCCC>\n";
			$x .= "<table width=90% class='calendar_board'>\n";
			for($i=0; $i<sizeof($row); $i++)
			{
				if($i==0) {
					$x .= "
					<tr valign=top class='week_title'>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][0]}</td>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][1]}</td>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][2]}</td>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][3]}</td>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][4]}</td>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][5]}</td>
					<td>{$Lang['SysMgr']['Homework']['WeekDay'][6]}</td>
					</tr>";
				}
				$x .= ($i%7==0) ? "<tr class='homework_day'>" : "";
				if($row[$i]!='') {
					$pos = $i%7;
					$x .= ($pos==0) ? "</tr>\n<tr height=50 class='homework_day'>\n" : "";
					$lang = $intranet_session_language=="b5" ? 1 : 0;
					$recordDate = date('Y-m-d',$row[$i]);
					$cycle = $cycle_days[$recordDate][$lang];
						
					if($cycle===-1 || $cycle==="") $display="";
					else $display=$cycle;
						
					$display = ($display=="" ? "" : "(".$display.")");
						
					$odd = $pos%2;
					$x .= $this->displayDayBlockForMonthlyHomeworkList($row[$i],$display,$odd,0);
				} else {
					$x .= "<td class='day_sunday day_dummy'><br style='clear:both' /></td>";
				}
			}
			$x .= "</tr></table>\n";
				
			return $x;
		}

		function printMonthlyHomeworklist($sql, $ts, $month, $year){
			global $image_path,$LAYOUT_SKIN,$intranet_session_language,$UserID,$i_frontpage_day;
			global $i_Homework_minimum,$i_Homework_morethan;
			global $i_AnnouncementAttachment,$Lang, $i_Homework_Homewrok_Type;
			//echo $sql;
			$hwImagePath = "$image_path/homework/$intranet_session_language/hw";

			$start = mktime(0,0,0,$month,1,$year);
			$end = mktime(0,0,0,$month+1,1,$year);
			$lcal = new libcalevent();
			$row = $lcal->returnCalendarArray($month,$year);
			$lcycle = new libcycleperiods();
			$cycle_days = $lcycle->getCycleInfoByDateRange(date('Y-m-d',$start),date('Y-m-d',$end));
				
			# Get Homework data
			$data = $this->returnArray($sql,15);
			$this->calData = $data;
				
			$x .= "<table width=90% class='calendar_board'>\n";
			for($i=0; $i<sizeof($row); $i++)
			{
				if($i==0) {
					$x .= "
					<tr valign=top class='week_title'>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][0]}</td>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][1]}</td>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][2]}</td>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][3]}</td>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][4]}</td>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][5]}</td>
					<td width=14% align=left>{$Lang['SysMgr']['Homework']['WeekDay'][6]}</td>
					</tr>";
				}
				if($row[$i]!='') {
					$pos = $i%7;
					$x .= ($pos==0) ? "</tr>\n<tr height=50>\n" : "";
					$lang = $intranet_session_language=="b5" ? 1 : 0;
					$recordDate = date('Y-m-d',$row[$i]);
					$cycle = $cycle_days[$recordDate][$lang];
						
					if($cycle===-1 || $cycle==="") $display="";
					else $display=$cycle;
						
					$display = ($display=="" ? "" : "(".$display.")");
						
					$odd = $pos%2;
					$x .= $this->displayDayBlockForMonthlyHomeworkList($row[$i],$display,$odd,1);
				} else {
					$x .= "<td class='day_sunday day_dummy'>&nbsp;</td>";
				}
			}
			$x .= "</tr></table>\n";
				
			return $x;
		}

		function exportMonthlyHomeworkList($ts, $studentID="", $condition="", $leader="", $classID=""){
				
			global $UserID, $intranet_session_language;
				
			$month = date('m',$ts);
			$year = date('Y',$ts);
			$next = mktime(0,0,0,$month+1,1,$year);


			$start = $ts;
			//$end = $ts+604800;
			$end = $next;

				
			if($this->useStartDateToGenerateList)
				$date_conds = "UNIX_TIMESTAMP(a.StartDate) >= $start AND UNIX_TIMESTAMP(a.StartDate)<$end";
				else
					$date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";

					$username_field = getNameFieldWithClassNumberByLang("c.");
						
					$fields = "a.HomeworkID, IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES),
					IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5),a.StartDate, a.DueDate, a.Loading,
					a.Title, a.Description, a.ClassGroupID, a.SubjectID, a.AttachmentPath, a.TypeID";

					if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
						$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";

						$conds = "$condition AND $date_conds";
					}
						
					else if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $this->isViewerGroupMember($UserID)){
						$clsInfo = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $UserID);	# get classID which is user is the class teacher
						for($i=0; $i<sizeof($clsInfo); $i++)
							$clsIDAry[] = $clsInfo[$i][0];
							$clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : "";
								
							$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
							LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as c ON c.SubjectGroupID = a.ClassGroupID
							LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
							INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
							INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
							LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID $clsConds)
							";

							if(!$this->isViewerGroupMember($UserID))
								$conds = "$condition AND (c.UserID = $UserID OR yct.UserID=$UserID) AND $date_conds";
								else
									$conds = $condition;
					}
						
					else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && $leader==1){
						$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";

						$conds = "$condition AND c.UserID = $UserID AND $date_conds";
					}
						
					else {
						$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
				  LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
				  LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
							
						if ($studentID!=""){
							$conds = "$condition AND c.UserID = $studentID AND $date_conds";
						}

						else if($_SESSION['UserType']==1) # staff
							$conds = "$condition AND c.UserID = $UserID AND $date_conds";
							else
								$conds = "$condition AND $date_conds";
					}
						
					if($classID!="") {
						$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=d.SubjectGroupID)
						LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=F.UserID AND ycu2.YearClassID='$classID')";
						$conds .= " AND ycu2.YearClassID='$classID'";
					}
						
						
					if($conds != "") $conds = " WHERE $conds";
					$sql = "SELECT $fields FROM $dbtables $conds GROUP BY a.HomeworkID ORDER BY a.DueDate";
					//echo $sql;
					$data = $this->returnArray($sql,15);
						
					return $data;
		}

		function displayDayBlockForMonthlyHomeworkList($ts, $cycle,$odd,$print=0)
		{
			global $i_Homework_minimum,$i_Homework_morethan,$i_Homework_hours,$image_path,$intranet_session_language, $Lang;
			global $i_Homework_loading,$i_Homework_subject, $i_Homework_HomeworkType;
			global $UserID;
			global $i_AnnouncementAttachment;
			 
			$currentID = $UserID;
			$hwImagePath = "$image_path/homework/$intranet_session_language/hw";
			//$cell_bg = ($odd==1 ? "class=homework_cal_month_cell_bg1" : "class=homework_cal_month_cell_bg0");
			$today = mktime(0,0,0,date('m'),date('d'),date('Y'));
			/*
			 if ($today == $ts)
			 {
			 $cell_bg = "bgcolor='#CCCCCC'";
			 $cell_bgcolor = "#EBCF87";
			 $dayfontcolor = "#0000FF";
			 }
			 else
			 {
			 $cell_bgcolor = "#87EBE0";
			 $dayfontcolor = "black";
			 }
			 */
			if($ts=="")
			{
				$x = "<td width=99 $cell_bg align=center><br></td>";
			}
			else
			{
				if ($cycle=="") $cycle = "&nbsp;";
				$i = 0;
				$text = "";
				 
				$cell = "";
				$blockDate = date('Y-m-d',$ts);
				$day = date('j',$ts);

				for($k=0; $k<sizeof($this->calData); $k++) {
					list($hid,$subject,$classTitle,$startdate,$duedate,$loading,$title,$description,$recordClassID,$recordSubjectID,$AttachmentPath, $subjectGroup, $TypeID) = $this->calData[$k];


					$displayDate = ($this->useStartDateToGenerateList) ? $startdate : $duedate;
					//echo $blockDate.'/'.$displayDate.'<br>';

					//while ($blockDate == $startdate)
					if($blockDate == $displayDate)
					{
						if ($loading==0)
						{
							$tload = $i_Homework_minimum;
						}
						else if ($loading <= 16)
						{
							$tload = $loading/2;
						}
						else
						{
							$tload = "$i_Homework_morethan 8";
						}
						$HomeworkType = "";
						if($this->useHomeworkType) {
							$homeworkTypeInfo = $this->getHomeworkType($TypeID)   ;
							$HomeworkType = $homeworkTypeInfo[0]['TypeName'];
						}
						//$title = ($this->useHomeworkType && $HomeworkType != "") ? "[$HomeworkType] ".$title : $title;
						$typeName = ($this->useHomeworkType && $HomeworkType != "") ? "<em>".$i_Homework_HomeworkType.":</em> ".$HomeworkType."<br>" : "";
						 
						 
						if($print)
							$text = "$title
							<span><em>$i_Homework_subject</em>: $subject<br>
							<em>{$Lang['SysMgr']['Homework']['SubjectGroup']}:</em> $subjectGroup<br>
							$typeName
							<em>$i_Homework_loading:</em>
							$tload $i_Homework_hours<br>";
							else
								$text = "<a href='javascript:viewHmeworkDetail($hid)'>$title</a>
								<span><em>$i_Homework_subject:</em> $subject<br>
								<em>{$Lang['SysMgr']['Homework']['SubjectGroup']}:</em> $subjectGroup<br>
								$typeName
								<em>$i_Homework_loading:</em>
								$tload $i_Homework_hours<br>";

								/*
								 if ($description != "")
								 	$text .= " <img src=\"$hwImagePath/icon_alt.gif\" alt=\"$description\"> ";
								 	if ($AttachmentPath !="")
								 		$text .="<img src=\"$image_path/icon_attachment.gif\" alt=\"$i_AnnouncementAttachment\"> ";
								 		*/
								 	if ($this->useHomeworkHandin && $handinRequired && $this->hasAccessHandinListRight($UserID,$hid)){
								 		$text .= $this->returnHandinListLink($hid);
								 	}
								 	$editAllowed = false;
								 	if ($courseID != "" && $courseAssignmentID!="")
								 	{
								 	}
								 	else if ($currentID == $posterID)
								 	{
								 		$editAllowed = true;
								 	}
								 	else if ($posterTeaching != 1 && $this->hasTeachingSubject($recordClassID,$recordSubjectID))
								 	{
								 		$editAllowed = true;
								 	}

								 	if ($editAllowed)
								 	{
								 		$text .= $this->returnEditDeleteLink($hid);
								 	}
								 	if ($courseID != "" && $courseAssignmentID !="")
								 	{
								 		$text .= $this->returneClassIcon();
								 	}
								 	$text .= $this->useStartDateToGenerateList ? "<em>".$Lang['SysMgr']['Homework']['DueDate'].":</em> $duedate<br>" : "<em>".$Lang['SysMgr']['Homework']['StartDate'].":</em> $startdate<br>";
								 	$text .= "</span>";
								 	//$text .= "<br>";
								 	/*
								 	 $cell .= "<table width=100% border=0 cellpadding=0 cellspacing=0 class=body>\n
								 	 <tr>
								 	 <td width=10 align=left valign=top><img src=\"$image_path/homework/month_bullet.gif\" width=8 height=12></td>
								 	 <td align=left valign=top>$text</td>
								 	 </tr>
								 	 <tr>
								 	 <td>&nbsp;</td>
								 	 <td>&nbsp;</td>
								 	 </tr>
								 	 </table>\n";
								 	 */
								 	$cell .= "<div class='homework_list'>$text</div>\n";
								 	 
								 	 
					}
				}
				$x  = "<td valign=top>";
				//$x .= "<table width=100% border=0 cellpadding=0 cellspacing=0 bgcolor=$cell_bgcolor class='tablebluetop tabletopnolink'>
				$x .= " <span class='day_no'>$day</span><span class='cycle_day_no'>$cycle</span><p class='spacer'></p>
				$cell
				";
				$x .= "</td>\n";
			}
			return $x;
		}

		# used by notsubmitlist.php
		function notSubmitList($subjectID, $subjectGroupID, $yearID, $yearTermID){
			global $Lang, $intranet_session_language;
				
			$fields = "b.ClassName, b.ClassNumber,
			IF('$intranet_session_language'='en', e.ClassTitleEN , e.ClassTitleB5) AS SubjectGroup,
			IF('$intranet_session_language'='en', b.EnglishName, b.ChineseName) AS StudentName,
			CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',a.SubjectGroupID,',',a.UserID,',$yearID,$yearTermID)>',count(d.RecordStatus),'</a>')";
				
			$dbtables = "SUBJECT_TERM_CLASS_USER AS a LEFT OUTER JOIN INTRANET_USER AS b ON b.UserID = a.UserID
				 LEFT OUTER JOIN INTRANET_HOMEWORK AS c ON c.ClassGroupID = a.SubjectGroupID
				 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS d ON (d.StudentID = b.UserID && d.HomeworkID = c.HomeworkID)
				 LEFT OUTER JOIN SUBJECT_TERM_CLASS AS e ON e.SubjectGroupID = a.SubjectGroupID";
				
			$date_cond = "AND c.DueDate < CURDATE()";
				
			if($subjectGroupID!=""){
				$conds = "a.SubjectGroupID = $subjectGroupID AND c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND d.RecordStatus = '-1' $date_cond Group by a.UserID Order by b.ClassName ASC ";
			}
				
			else{
				$conds = "c.SubjectID = $subjectID AND c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND d.RecordStatus = '-1' $date_cond Group by a.UserID Order by b.ClassName ASC";
			}

			$sql = "SELECT $fields from $dbtables WHERE $conds";
				
			//echo $sql;
			$data = $this->returnArray($sql,5);
				
			$x .= "<tr>";
			$x .= "<td class='tablebluetop tabletopnolink' width='5%'>#</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='8%'>".$Lang['SysMgr']['Homework']['Class']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='12%'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='25%'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='20%'>".$Lang['SysMgr']['Homework']['StudentName']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='15%'>".$Lang['SysMgr']['Homework']['NotSubmitCount']."</td>";
			$x .= "</tr>";
			if(sizeof($data)!=0){
				for($i=0; $i<sizeof($data); $i++){
					list($className, $calssNumber, $subjectGroup, $studentName, $recordCount) = $data[$i];
					$x .= "<tr>";
					$x .= "<td>".($i+1)."</td>";
					$x .= "<td>$className</td>";
					$x .= "<td>$calssNumber</td>";
					$x .= "<td>$subjectGroup</td>";
					$x .= "<td>$studentName</td>";
					$x .= "<td>$recordCount</td>";
					$x .= "</tr>";
				}
			}
				
			else{
				$x .="<tr><td colspan=\"7\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
			}
			return $x;
		}

		# used by notsubmitlist.php
		function printNotSubmitList($subjectID, $subjectGroupID, $yearID, $yearTermID){
			global $Lang, $intranet_session_language;

			$fields = "b.ClassName, b.ClassNumber,
			IF('$intranet_session_language'='en', e.ClassTitleEN , e.ClassTitleB5) AS SubjectGroup,
			IF('$intranet_session_language'='en', b.EnglishName, b.ChineseName) AS StudentName,
			count(d.RecordStatus)";
				
			$dbtables = "SUBJECT_TERM_CLASS_USER AS a LEFT OUTER JOIN INTRANET_USER AS b ON b.UserID = a.UserID
				 LEFT OUTER JOIN INTRANET_HOMEWORK AS c ON c.ClassGroupID = a.SubjectGroupID
				 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS d ON (d.StudentID = b.UserID && d.HomeworkID = c.HomeworkID)
				 LEFT OUTER JOIN SUBJECT_TERM_CLASS AS e ON e.SubjectGroupID = a.SubjectGroupID";
				
			$date_cond = "AND c.DueDate < CURDATE()";
				
			if($subjectGroupID!=""){
				$conds = "a.SubjectGroupID = $subjectGroupID AND c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND d.RecordStatus = '-1' $date_cond Group by a.UserID Order by b.ClassName ASC ";
			}
				
			else{
				$conds = "c.SubjectID = $subjectID AND c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND d.RecordStatus = '-1' $date_cond Group by a.UserID Order by b.ClassName ASC";
			}

			$sql = "SELECT $fields from $dbtables WHERE $conds";
				
			//echo $sql;
			$data = $this->returnArray($sql,5);
				
			$x .= "<tr>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Class']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['StudentName']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['NotSubmitCount']."</td>";
			$x .= "</tr>";
			if(sizeof($data)!=0){
				for($i=0; $i<sizeof($data); $i++){
					list($className, $calssNumber, $subjectGroup, $studentName, $recordCount) = $data[$i];
					$x .= "<tr>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$className</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$calssNumber</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$subjectGroup</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$studentName</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$recordCount</td>";
					$x .= "</tr>";
				}
			}
				
			else{
				$x .="<tr><td colspan=\"7\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
			}
			return $x;
		}

		# used by export.php in homework report
		function exportNotSubmitList($subjectID, $subjectGroupID, $yearID, $yearTermID){
			global $intranet_session_language;
				
			$fields = "b.ClassName, b.ClassNumber,
			IF('$intranet_session_language'='en', e.ClassTitleEN , e.ClassTitleB5) AS SubjectGroup,
			IF('$intranet_session_language'='en', b.EnglishName, b.ChineseName) AS StudentName,
			count(d.RecordStatus)";
				
			$dbtables = "SUBJECT_TERM_CLASS_USER AS a LEFT OUTER JOIN INTRANET_USER AS b ON b.UserID = a.UserID
				 LEFT OUTER JOIN INTRANET_HOMEWORK AS c ON c.ClassGroupID = a.SubjectGroupID
				 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS d ON (d.StudentID = b.UserID && d.HomeworkID = c.HomeworkID)
				 LEFT OUTER JOIN SUBJECT_TERM_CLASS AS e ON e.SubjectGroupID = a.SubjectGroupID";
				
			$date_cond = "AND c.DueDate < CURDATE()";
				
			if($subjectGroupID!=""){
				$conds = "a.SubjectGroupID = $subjectGroupID AND c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND d.RecordStatus = '-1' $date_cond Group by a.UserID Order by b.ClassName ASC ";
			}
				
			else{
				$conds = "c.SubjectID = $subjectID AND c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND d.RecordStatus = '-1' $date_cond Group by a.UserID Order by b.ClassName ASC";
			}

			$sql = "SELECT $fields from $dbtables WHERE $conds";
			return $this->returnArray($sql,5);
		}

		function viewNotSubmitDetail($subjectGroupID, $studentID, $yearID, $yearTermID="", $dateChoice="", $startDate="", $endDate="", $group="", $yearClassID="", $DueDateEqualToToday=0, $status="-1"){
			global $Lang;
				
			$fields = "a.HomeworkID, a.Title, a.Description, a.StartDate, a.DueDate, a.TypeID";
			$dbtables = "INTRANET_HOMEWORK As a LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS b ON b.HomeworkID = a.HomeworkID INNER JOIN INTRANET_USER u ON (u.UserID=b.StudentID AND u.RecordStatus=1)";
				
			//$date_cond = ($DueDateEqualToToday) ? " AND a.DueDate <= CURDATE()" : " AND a.DueDate < CURDATE()";
				
			if($dateChoice=="DATE") {
				$dateConds = " AND (a.DueDate BETWEEN '$startDate' AND '$endDate')";
			} else {
				$dateConds = " AND a.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND a.YearTermID = $yearTermID";
			}
				
			$conds = "a.ClassGroupID = $subjectGroupID $dateConds and b.RecordStatus = '$status' $date_cond";
			if($group==2) {
				include_once("libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
				$tempAry = $ldiscipline->storeStudent(0, "::".$yearClassID);
				$conds .= " AND b.StudentID IN (".implode(',', $tempAry).")";
			} else {
				$conds .= " AND b.StudentID = $studentID ";
			}
			$sql = "SELECT $fields from $dbtables WHERE $conds";
				
			//echo $sql;
			$data = $this->returnArray($sql,6);

			$x .= "<tr>";
			$x .= "<td class='tablebluetop tabletopnolink' width='2%'>#</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['Description']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
			$x .= "</tr>";
				
			if(sizeof($data)!=0){
				for($i=0; $i<sizeof($data); $i++){
					list($homeworkID, $title, $description, $startDate, $dueDate, $TypeID) = $data[$i];
					$description = ($description=="")? "--" : $description;
					$typeName = "";
					if($this->useHomeworkType) {
						$homeworkTypeInfo = $this->getHomeworkType($TypeID);
						if($homeworkTypeInfo[0]['TypeName'])
							$typeName = "[".$homeworkTypeInfo[0]['TypeName']."] ";
					}

					$x .= "<tr>";
					$x .= "<td class='attendancepresent'>".($i+1)."</td>";
					$x .= "<td class='attendancepresent'>$typeName$title</td>";
					$x .= "<td class='attendancepresent'>$description</td>";
					$x .= "<td class='attendancepresent'>$startDate</td>";
					$x .= "<td class='attendancepresent'>$dueDate</td>";
					$x .= "</tr>";
				}

			}
			return $x;
		}

		function viewSupplementaryDetail($subjectGroupID, $studentID, $yearID, $yearTermID="", $dateChoice="", $startDate="", $endDate="", $group="", $yearClassID="", $DueDateEqualToToday=0){
			global $Lang;
				
			$fields = "a.HomeworkID, a.Title, a.Description, a.StartDate, a.DueDate, a.TypeID";
			$dbtables = "INTRANET_HOMEWORK As a LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS b ON b.HomeworkID = a.HomeworkID";
				
			$date_cond = ($DueDateEqualToToday) ? " AND a.DueDate <= CURDATE()" : " AND a.DueDate < CURDATE()";
				
			if($dateChoice=="DATE") {
				$dateConds = " AND (a.DueDate BETWEEN '$startDate' AND '$endDate')";
			} else {
				$dateConds = " AND a.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND a.YearTermID = $yearTermID";
			}
				
			$conds = "a.ClassGroupID = $subjectGroupID $dateConds and b.RecordStatus in ('6','7','-11') $date_cond";
			if($group==2) {
				include_once("libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
				$tempAry = $ldiscipline->storeStudent(0, "::".$yearClassID);
				$conds .= " AND b.StudentID IN (".implode(',', $tempAry).")";
			} else {
				$conds .= " AND b.StudentID = $studentID ";
			}
			$sql = "SELECT $fields from $dbtables WHERE $conds";
				
			//echo $sql;
			$data = $this->returnArray($sql,6);
				
			$x .= "<tr>";
			$x .= "<td class='tablebluetop tabletopnolink' width='2%'>#</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['Description']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
			$x .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
			$x .= "</tr>";
				
			if(sizeof($data)!=0){
				for($i=0; $i<sizeof($data); $i++){
					list($homeworkID, $title, $description, $startDate, $dueDate, $TypeID) = $data[$i];
					$description = ($description=="")? "--" : $description;
					$typeName = "";
					if($this->useHomeworkType) {
						$homeworkTypeInfo = $this->getHomeworkType($TypeID);
						if($homeworkTypeInfo[0]['TypeName'])
							$typeName = "[".$homeworkTypeInfo[0]['TypeName']."] ";
					}

					$x .= "<tr>";
					$x .= "<td class='attendancepresent'>".($i+1)."</td>";
					$x .= "<td class='attendancepresent'>$typeName$title</td>";
					$x .= "<td class='attendancepresent'>$description</td>";
					$x .= "<td class='attendancepresent'>$startDate</td>";
					$x .= "<td class='attendancepresent'>$dueDate</td>";
					$x .= "</tr>";
				}

			}
			return $x;
		}

		# used by view.php in notsubmitlist
		function printNotSubmitDetail($subjectGroupID, $studentID, $yearID, $yearTermID, $group="", $yearClassID="", $DueDateEqualToToday=0){
			global $Lang;
				
			$fields = "a.HomeworkID, a.Title, a.Description, a.StartDate, a.DueDate, a.TypeID";
			$dbtables = "INTRANET_HOMEWORK As a LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS b ON b.HomeworkID = a.HomeworkID";
				
			$date_cond = ($DueDateEqualToToday) ? " AND a.DueDate <= CURDATE()" : " AND a.DueDate < CURDATE()";
			$conds = "a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID and b.RecordStatus = '-1' $date_cond";
				
			if($yearTermID!="") $conds .= " AND a.YearTermID = $yearTermID";
				
			if($group==2) {
				include_once("libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
				$tempAry = $ldiscipline->storeStudent(0, "::".$yearClassID);
				$conds .= " AND b.StudentID IN (".implode(',', $tempAry).")";
			} else {
				$conds .= " AND b.StudentID = $studentID ";
			}
				
			$sql = "SELECT $fields from $dbtables WHERE $conds";
				
			//echo $sql;
			$data = $this->returnArray($sql,6);
				
			if (sizeof($data)!=0){
				$x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eHomeworktableborder'>";
				$x .= "<tr>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Description']."</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
				$x .= "</tr>";

				for ($i=0; $i<sizeof($data); $i++)
				{
					list($homeworkID, $title, $description, $startDate, $dueDate, $TypeID) = $data[$i];
					$description = ($description=="")? "--" : $description;
					$typeName = "";
					if($this->useHomeworkType) {
						$homeworkTypeInfo = $this->getHomeworkType($TypeID);
						if($homeworkTypeInfo[0]['TypeName'])
							$typeName = "[".$homeworkTypeInfo[0]['TypeName']."] ";
					}
						
					$x .= "<tr class='$css'>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$typeName$title</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$description</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$startDate</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$dueDate</td>";
					$x .= "</tr>\n";
				}

				$x .= "</table>\n";
			}
			return $x;
		}

		function printSupplementaryDetail($subjectGroupID, $studentID, $yearID, $yearTermID, $group="", $yearClassID="", $DueDateEqualToToday=0){
			global $Lang;
				
			$fields = "a.HomeworkID, a.Title, a.Description, a.StartDate, a.DueDate, a.TypeID";
			$dbtables = "INTRANET_HOMEWORK As a LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS b ON b.HomeworkID = a.HomeworkID";
				
			$date_cond = ($DueDateEqualToToday) ? " AND a.DueDate <= CURDATE()" : " AND a.DueDate < CURDATE()";
			$conds = "a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID and b.RecordStatus in ('6', '7', '-11') $date_cond";
				
			if($yearTermID!="") $conds .= " AND a.YearTermID = $yearTermID";
				
			if($group==2) {
				include_once("libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
				$tempAry = $ldiscipline->storeStudent(0, "::".$yearClassID);
				$conds .= " AND b.StudentID IN (".implode(',', $tempAry).")";
			} else {
				$conds .= " AND b.StudentID = $studentID ";
			}
				
			$sql = "SELECT $fields from $dbtables WHERE $conds";
				
			//echo $sql;
			$data = $this->returnArray($sql,6);
				
			if (sizeof($data)!=0){
				$x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eHomeworktableborder'>";
				$x .= "<tr>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Description']."</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
				$x .= "</tr>";

				for ($i=0; $i<sizeof($data); $i++)
				{
					list($homeworkID, $title, $description, $startDate, $dueDate, $TypeID) = $data[$i];
					$description = ($description=="")? "--" : $description;
					$typeName = "";
					if($this->useHomeworkType) {
						$homeworkTypeInfo = $this->getHomeworkType($TypeID);
						if($homeworkTypeInfo[0]['TypeName'])
							$typeName = "[".$homeworkTypeInfo[0]['TypeName']."] ";
					}
						
					$x .= "<tr class='$css'>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$typeName$title</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$description</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$startDate</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$dueDate</td>";
					$x .= "</tr>\n";
				}

				$x .= "</table>\n";
			}
			return $x;
		}

		# used by index.php in statistics
		function getForms($yearID){
			$sql = "SELECT a.YearID, a.YearName
			From YEAR AS a
			INNER JOIN YEAR_CLASS AS b ON b.YearID = a.YearID
			WHERE b.AcademicYearID = $yearID GROUP BY b.YearID";
			return $this->returnArray($sql,2);
		}

		# used by subjectleader.php, teacherview.php, teachhistory.php
		function getCurrentForms($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($formID, $formName) = $result[$i];
				$selected_str = ($formID==$selected? "SELECTED":"");
				$x .= "<OPTION value='$formID' $selected_str>$formName</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}

		function getFormSubjectList($formID="", $yearID="", $yearTermID="", $classID=""){
			global $intranet_session_language;
				
			$conds = ($yearTermID=="")? "" : "AND e.YearTermID = $yearTermID";
			/*$sql = "SELECT a.RecordID, IF('$intranet_session_language' = 'en', a.EN_DES, a.CH_DES) AS Subject
			 FROM ASSESSMENT_SUBJECT AS a
			 LEFT OUTER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID
			 LEFT OUTER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION AS c ON c.SubjectGroupID = b.SubjectGroupID";
			 */
			$sql = "SELECT c.RecordID,
			IF('$intranet_session_language'='en', c.EN_DES, c.CH_DES) AS Subject
			FROM SUBJECT_TERM_CLASS AS a
			INNER JOIN SUBJECT_TERM AS b ON  b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = b.SubjectID
			INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION AS d ON d.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS e ON e.YearTermID = b.YearTermID
			INNER JOIN ACADEMIC_YEAR AS f ON f.AcademicYearID = e.AcademicYearID";

			if($classID!="") {
				$sql .= " INNER JOIN SUBJECT_TERM_CLASS_USER g ON (g.SubjectGroupID=b.SubjectGroupID)
				INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=g.UserID AND ycu.YearClassID=$classID)";
			}
				
			if($formID!="")
				$conds = " WHERE d.YearID = $formID AND f.AcademicYearID = $yearID $conds
				GROUP BY c.RecordID ORDER BY Subject";
				else
					$conds = " WHERE f.AcademicYearID = $yearID $conds
					GROUP BY c.RecordID ORDER BY Subject";
						
					$sql .= $conds;
						
					return $this->returnArray($sql, 2);

		}

		# used by export.php in statistics
		function exportHomeworkStatistics($date_conds,$conds,$classID=""){
			global $intranet_session_language;
				
			# set conditions
			$fields = "IF('$intranet_session_language' = 'en', g.YearTermNameEN, g.YearTermNameB5) AS YearTermName,
			IF('$intranet_session_language' = 'en', f.EN_DES, f.CH_DES) AS Subject,
			IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup, count(DISTINCT d.HomeworkID),
			(sum(d.Loading)/(count(if(d.Loading=2,1,0))/count(DISTINCT d.HomeworkID))*0.5) as TotalLoading";
			$dbtables = "SUBJECT_TERM_CLASS AS a
			INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN YEAR AS c ON b.YearID = c.YearID
			LEFT OUTER JOIN INTRANET_HOMEWORK AS d ON (d.ClassGroupID = a.SubjectGroupID $date_conds)
			LEFT OUTER JOIN SUBJECT_TERM AS e ON e.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS f ON f.RecordID = e.SubjectID
			INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = e.YearTermID
			INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
				
			if($classID!="") {
				$dbtables .= " INNER JOIN SUBJECT_TERM_CLASS_USER k ON (k.SubjectGroupID=e.SubjectGroupID)
						INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=k.UserID)";
				$clsConds .= " AND ycu.YearClassID=$classID";
			}
				
			$conds = ($conds=="")? "c.YearID IS NOT NULL" : $conds." AND c.YearID IS NOT NULL";
			$conds .= $clsConds;
			$sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.SubjectGroupID ORDER BY Subject ASC,".Get_Lang_Selection("a.ClassTitleB5", "a.ClassTitleEN");
				
			return $this->returnArray($sql);
		}

		# used by print_preview.php in statistics
		function printHomeworkStatistics($date_conds,$conds,$classID=""){
			global $intranet_session_language, $Lang;
				
			# set conditions
			$fields = "IF('$intranet_session_language' = 'en', g.YearTermNameEN, g.YearTermNameB5) AS YearTermName,
			IF('$intranet_session_language' = 'en', f.EN_DES, f.CH_DES) AS Subject,
			IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup, count(DISTINCT d.HomeworkID),
			(sum(d.Loading)/(count(if(d.Loading=2,1,0))/count(DISTINCT d.HomeworkID))*0.5) as TotalLoading";
			$dbtables = "SUBJECT_TERM_CLASS AS a
			INNER JOIN SUBJECT_TERM_CLASS_YEAR_RELATION AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN YEAR AS c ON b.YearID = c.YearID
			LEFT OUTER JOIN INTRANET_HOMEWORK AS d ON (d.ClassGroupID = a.SubjectGroupID $date_conds)
			LEFT OUTER JOIN SUBJECT_TERM AS e ON e.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS f ON f.RecordID = e.SubjectID
			INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = e.YearTermID
			INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
				
				
			if($classID!="") {
				$dbtables .= " INNER JOIN SUBJECT_TERM_CLASS_USER k ON (k.SubjectGroupID=e.SubjectGroupID)
					INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=k.UserID)";
				$conds .= " AND ycu.YearClassID=$classID";
			}
			$conds = ($conds=="")? "GROUP BY a.SubjectGroupID ORDER BY Subject" : $conds." GROUP BY a.SubjectGroupID ORDER BY YearTermNameEN, Subject";
			$sql = "SELECT $fields FROM $dbtables WHERE $conds ASC, ".Get_Lang_Selection("a.ClassTitleB5", "a.ClassTitleEN");
			//echo $sql;
			$data = $this->returnArray($sql);

			$x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eHomeworktableborder'>";
			$x .= "<tr>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['YearTerm']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['HomeworkCount']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['TotalWorkload']."</td>";
			$x .= "</tr>";
			if (sizeof($data)!=0){
				for ($i=0; $i<sizeof($data); $i++)
				{
					list($YearTermName, $subject, $subjectGroup, $homeworkCount, $totalLoad) = $data[$i];
					$subject = ($subject=="")? "--" : $subject;
					$subjectGroup = ($subjectGroup=="")? "--" : $subjectGroup;
						
					$x .= "<tr class='$css'>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$YearTermName</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$subject</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$subjectGroup</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$homeworkCount</td>";
					$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$totalLoad &nbsp;</td>";
					$x .= "</tr>\n";
				}
			}
			else{
				$x .="<tr><td colspan=\"4\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
			}

			$x .= "</table>\n";
			return $x;
		}

		# used by homeworkreport.php
		function getTeachers($yearID, $yearTermID=""){
			global $intranet_session_language;
			$conds = ($yearTermID=="")? "" : "AND d.YearTermID = $yearTermID";
				
			$sql = "SELECT a.UserID,
			IF('$intranet_session_language'='en', b.EnglishName, b.ChineseName)
			FROM SUBJECT_TERM_CLASS_TEACHER AS a
			INNER JOIN INTRANET_USER AS b ON b.UserID=a.UserID
			INNER JOIN SUBJECT_TERM AS c ON c.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = c.YearTermID
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			WHERE e.AcademicYearID = $yearID $conds GROUP BY a.UserID";
				
			return $this->returnArray($sql,2);
		}

		# 20091130 yat homeworkreport/index.php
		function getSubjectLeaders($yearID, $yearTermID=""){
			global $intranet_session_language;
			$conds = ($yearTermID=="")? "" : "AND a.YearTermID = $yearTermID";
			$name = getNameFieldWithClassNumberByLang("c.");
				
			$sql = "select
			distinct(a.PosterUserID),
			$name
			from
			INTRANET_HOMEWORK as a
			inner join INTRANET_SUBJECT_LEADER as b on (b.UserID = a.PosterUserID)
			inner join INTRANET_USER AS c ON (c.UserID=a.PosterUserID)
			where
			a.AcademicYearID = $yearID
			$conds
			order by
			c.EnglishName
			";
			return $this->returnArray($sql,2);
		}

		function getCurrentTeachers($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($teacherID, $teacherName) = $result[$i];
				$selected_str = ($teacherID==$selected? "SELECTED":"");
				$x .= "<OPTION value='$teacherID' $selected_str>$teacherName</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}

		# used by export.php in homework report
		function exportHomeworkReport($date_conds,$conds,$classID,$having=""){
			global $intranet_session_language;
				
			$name_field = getNameFieldByLang("USR.");
			$teacher_name_field = getNameFieldByLang("TUSR.");
				
			// Update sql to query Teacher of Subject Group [2015-0203-1618-41066]
			# set conditions
			$fields = " IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
			IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
			a.Title as title, a.Description, a.StartDate, a.DueDate, a.Loading/2, IF(e.TypeName!='', e.TypeName, '---'),
			$name_field as name,
			IFNULL(GROUP_CONCAT(DISTINCT $teacher_name_field SEPARATOR ', '), '--') as teacher_name
			";
				
			$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
				 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
				 LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE e ON (e.TypeID=a.TypeID)
				 INNER JOIN INTRANET_USER USR ON (USR.UserID=a.PosterUserID)
			 	 LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as SCT ON (SCT.SubjectGroupID = a.ClassGroupID)
				 LEFT OUTER JOIN INTRANET_USER TUSR ON (SCT.UserID = TUSR.UserID)
				";
				
			$conds = ($conds=="")? "WHERE $date_conds" : "WHERE $conds $date_conds";
				
			if($classID!="") {
				$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";
				$conds .= " AND ycu.YearClassID='$classID'";
			}
				
			// added $having to sql [2015-0203-1618-41066]
			$sql = "SELECT $fields FROM $dbtables $conds GROUP BY a.HomeworkID $having ORDER BY Subject ";
				
			//echo $sql;
			return $data = $this->returnArray($sql,7);
		}

		# used by print_preview.php in homework report
		function printHomeworkReport($date_conds, $conds, $classID="", $having=""){
			global $intranet_session_language, $Lang, $i_Homework_HomeworkType;
				
			$name_field = getNameFieldByLang("d.");
			$teacher_name_field = getNameFieldByLang("u.");
				
			// Update sql to query Teacher of Subject Group [2015-0203-1618-41066]
			# set conditions
			$fields = " IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
			IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
			a.Title as title, a.Description, a.StartDate, a.DueDate, a.Loading/2, IF(e.TypeName!='', e.TypeName, '---'),
			$name_field as name,
			IFNULL(GROUP_CONCAT(DISTINCT $teacher_name_field SEPARATOR ',<br>'), '--') as teacher_name
			";
				
			$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
				 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
				 INNER JOIN INTRANET_USER as d on (d.UserID=a.PosterUserID)
				 LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE e ON (e.TypeID=a.TypeID)
				 LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as sct ON (sct.SubjectGroupID = a.ClassGroupID)
				 LEFT OUTER JOIN INTRANET_USER u ON (sct.UserID = u.UserID)";

				
			$conds = ($conds=="")? "WHERE $date_conds" : "WHERE $conds $date_conds";
				
			if($classID!="") {
				$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";
				$conds .= " AND ycu.YearClassID='$classID'";
			}

			// added $having to sql [2015-0203-1618-41066]
			$sql = "SELECT $fields FROM $dbtables $conds GROUP BY a.HomeworkID $having ORDER BY Subject";
				
				
			$data = $this->returnArray($sql,7);

			$x .= "<table width='90%' border='0' align='center' cellpadding='4' cellspacing='0' class='eHomeworktableborder'>";
			$x .= "<tr>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
			if($this->useHomeworkType) {
				$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>$i_Homework_HomeworkType</td>";
			}
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Description']."</td>";
			// Change Print Preview Column Title
			//			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['TeacherSubjectLeader']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['General']['LastModifiedBy']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['SubjectGroupTeacher']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
			$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")</td>";
			$x .= "</tr>";
				
			if (sizeof($data)!=0){
				for ($i=0; $i<sizeof($data); $i++)
				{
					list($subject, $subjectGroup, $topic, $description, $startDate, $dueDate, $loading, $typeName, $posterName, $subjectTeacherName) = $data[$i];
					$description = nl2br($description);
					if($description=="")
						$description = "--";
						$x .= "<tr class='$css'>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$subject</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$subjectGroup</td>";
						if($this->useHomeworkType) {
							$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$typeName</td>";
						}
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$topic</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$description</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$posterName</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$subjectTeacherName</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$startDate</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$dueDate</td>";
						$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>$loading</td>";
						$x .= "</tr>\n";
				}
			}
			else{
				$x .="<tr><td colspan=\"8\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
			}
			$x .= "</table>\n";
			return $x;
		}

		# used by view.php
		function displayAttachment($homeworkID){
			global $file_path, $image_path;
			$sql =  "SELECT AttachmentPath FROM INTRANET_HOMEWORK WHERE HomeworkID='$homeworkID'";
			$tmp = $this->returnVector($sql,1);
			$folder = $tmp[0];
			$x ="";
			if($folder=="") return x;
			$path = "$file_path/file/homework/".$folder.$homeworkID;
			include_once("libfiletable.php");
			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			$x = "<table border=0 cellpadding=0 cellspacing=0>";
			while (list($key, $value) = each($files)) {
				// 					$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				// 					$url = intranet_handle_url($url);
				$x .= "<tr>";
				$x .="<td id='a_".$key."'><img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
				// 					$x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\" >".$files[$key][0]."</a>";

				$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				$url_download = rawurlencode($file_path.$url);
				//$x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target=".$url_download."\" >".$files[$key][0]."</a>";
				$encryptedPath = getEncryptedText($file_path.$url);
				$x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".$encryptedPath."\" >".$files[$key][0]."</a>";
				$x .= " (".ceil($files[$key][1]/1000)."Kb)</td>";
				$x .= "</tr>\n";
			}
			$x .="</table>";
			return $x;
		}

		function displayAttachment_IMG($homeworkID, $token=''){
			global $file_path, $image_path;
			
			$sql =  "SELECT AttachmentPath FROM INTRANET_HOMEWORK WHERE HomeworkID='$homeworkID'";
			$tmp = $this->returnVector($sql,1);
			$folder = $tmp[0];
			
			$x = "";
			if($folder=="") return x;
			
			$token_parms = '';
			if($token != '') {
			    $token_parms = "&token=".$token;
			}
			
			$path = "$file_path/file/homework/".$folder.$homeworkID;
			
			include_once("libfiletable.php");
			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			
			$x = "<table border=0 cellpadding=0 cellspacing=0 style='width: 100%'>";
			while (list($key, $value) = each($files)) {
				// 					$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				// 					$url = intranet_handle_url($url);
				$x .= "<tr>";
				$x .="<td id='a_".$key."'><img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
				// 					$x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\" >".$files[$key][0]."</a>";
                
				$path_size = $path."/".$files[$key][0];
				$size = GetImageSize($path_size);
				list($width, $height, $type, $attr) = $size;
                
				$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				$url_download = rawurlencode($file_path.$url);
				//$x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target=".$url_download."\" >".$files[$key][0]."</a>";
				$encryptedPath = getEncryptedText($file_path.$url);
				
				$x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".$encryptedPath.$token_parms."\" >".$files[$key][0]."</a>";
				$x .= " (".ceil($files[$key][1]/1000)."Kb)</td>";
				$x .= "</tr>\n";
				
				$target_filepath = "$path/".$files[$key][0];
				if(isImage($target_filepath)){
					if($width > 300)
						$image_html_tag = "style='width: 100%'";
						$x .= "<tr><td>";
						$x .= "<img border=0 src=\"$url\" $image_html_tag>";
						$x .= "</td></tr>\n";
				}
			}
			$x .="</table>";
			return $x;
		}
        
		function displayAttachment_app($homeworkID){
			global $file_path, $image_path;
			$sql =  "SELECT AttachmentPath FROM INTRANET_HOMEWORK WHERE HomeworkID='$homeworkID'";
			$tmp = $this->returnVector($sql,1);
			$folder = $tmp[0];
			$x ="";
			if($folder=="") return x;
			$path = "$file_path/file/homework/".$folder.$homeworkID;
			include_once("libfiletable.php");
			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			$x = "<table border=0 cellpadding=0 cellspacing=0>";
			while (list($key, $value) = each($files)) {
				// 					$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				// 					$url = intranet_handle_url($url);
				$x .= "<tr>";
				$x .="<td id='a_".$key."'><img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
				// 					$x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\" >".$files[$key][0]."</a>";

				$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				$url_download = rawurlencode($file_path.$url);
				//$x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target=".$url_download."\" >".$files[$key][0]."</a>";
				$encryptedPath = getEncryptedText($file_path.$url);
				$x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".$encryptedPath."\" >".$files[$key][0]."</a>";
				$x .= " (".ceil($files[$key][1]/1000)."Kb)</td>";
				$x .= "</tr>\n";
			}
			$x .="</table>";
			return $x;
		}

		# used by print_preview.php
		function displayAttachment2($homeworkID){
			global $file_path, $image_path;
			$sql =  "SELECT AttachmentPath FROM INTRANET_HOMEWORK WHERE HomeworkID='$homeworkID'";
			$tmp = $this->returnVector($sql,1);
			$folder = $tmp[0];
			$x ="";
			if($folder=="") return x;
			$path = "$file_path/file/homework/".$folder.$homeworkID;
			include_once("libfiletable.php");
			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			$x = "<table border=0 cellpadding=0 cellspacing=0>";
			while (list($key, $value) = each($files)) {
				$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				$url = intranet_handle_url($url);
				$x .= "<tr>";
				$x .="<td id='a_".$key."'><img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
				$x .= $files[$key][0];
				$x .= " (".ceil($files[$key][1]/1000)."Kb)</td>";
				$x .= "</tr>\n";
			}
			$x .="</table>";
			return $x;
		}

		function GetAllAcademicYear(){
			global $intranet_session_language;
				
			$sql = "SELECT AcademicYearID,
			IF('$intranet_session_language'= 'en', YearNameEN, YearNameB5) As YearName
			FROM ACADEMIC_YEAR ORDER BY Sequence";
			$result = $this->returnArray($sql,2);
				
			return $result;
		}

		function getCurrentYear($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($id, $name) = $result[$i];
				$selected_str = ($id==$selected? "SELECTED":"");
				$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}

		function getAllAcademicYearTerm($yearID){
			global $intranet_session_language;
				
			$sql = "SELECT YearTermID,
			IF('$intranet_session_language'= 'en', YearTermNameEN, YearTermNameB5) As YearTermName
			FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = $yearID ORDER BY YearTermID";
			$result = $this->returnArray($sql,2);
				
			return $result;


		}

		function getCurrentYearTerm($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($id, $name) = $result[$i];
				$selected_str = ($id==$selected? "SELECTED":"");
				$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}

		# Subject Leader Import
		# Get All Class Names
		function getAllStudentInfo($yearID, $yearTermID){
			$sql = "SELECT a.YearClassID, d.EnglishName, d.ClassName, d.ClassNumber
			FROM YEAR_CLASS_USER AS a
			INNER JOIN YEAR_CLASS AS b ON b.YearClassID = a.YearClassID
			INNER JOIN ACADEMIC_YEAR_TERM AS c ON c.AcademicYearID = b.AcademicYearID
			INNER JOIN INTRANET_USER AS d ON a.UserID = d.UserID
			WHERE c.AcademicYearID = $yearID AND c.YearTermID = $yearTermID";
				
			$temp = $this->returnArray($sql, 3);
			return $temp;
		}

		# Get All Subject Group Students
		function getAllSubjectGroupStudent($userID, $yearID, $yearTermID){
			$sql = "SELECT a.SubjectGroupID, a.ClassCode, f.UserID, g.EnglishName, g.ClassName, g.ClassNumber
			FROM SUBJECT_TERM_CLASS As a
			INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN SUBJECT_TERM AS c ON c.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = c.YearTermID
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			INNER JOIN SUBJECT_TERM_CLASS_USER AS f ON f.SubjectGroupID = a.SubjectGroupID
			INNER JOIN INTRANET_USER AS g ON g.UserID = f.UserID
			WHERE b.UserID = $userID AND e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID
			GROUP BY a.SubjectGroupID";
				
			$temp = $this->returnArray($sql, 5);
			return $temp;
		}

		# Check Import Subject Leader
		function checkSubjectLeaderImportError($data, $yearID, $yearTermID){
			global $UserID, $Lang, $UserLogin;
				
			$allSubjectGroupCode = array();
			$allSubjectGroupName = array();
			$teacherSubjectGroupList = array();
			$allStudentInfo = array();
			$allSubjectGroupStudent = array();
				
			$subjectGroupCodeName = $this->getAllSubjectGroupCode($yearID, $yearTermID);
				
			# if user is eHomework admin, then he can assign all the subject/subject group's leadr
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
			$teachingSubjectGroupCode = $this->getTeachingSubjectGroup($UserIDTmp, $yearID, $yearTermID);
			//$studentInfo = $this->getAllStudentInfo($yearID, $yearTermID);
			//$subjectGroupStudent = $this->getAllSubjectGroupStudent($UserID, $yearID, $yearTermID);
			### retrieve student's subject group
			# retrieve csv student's USerID
			$studentInfo = array();
			include_once("libuser.php");
			$lu = new libuser();
			foreach($data as $k=>$d)
			{
				list($tmp_SubjectGorupID, $tmp_Class, $tmp_ClassNo) = $d;
				$tmp = $lu->GET_USER_BY_CLASS_CLASSNO($tmp_Class, $tmp_ClassNo);
				if($tmp)
				{
					$studentInfo[] = array($tmp[0]['ClassTitleEN'], $tmp[0]['ClassNumber']);
					$csv_user_id[] = $tmp[0]['UserID'];
				}
			}
				
			$csv_user_id_list = implode(",",(array)$csv_user_id);
			$sql = "
			SELECT
			a.ClassCode,
			h.ClassTitleEN,
			g.ClassNumber
			FROM
			SUBJECT_TERM_CLASS As a
			INNER JOIN SUBJECT_TERM_CLASS_USER AS f ON (f.SubjectGroupID = a.SubjectGroupID)
			INNER JOIN SUBJECT_TERM AS c ON c.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = c.YearTermID
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			INNER JOIN YEAR_CLASS_USER as g on (g.UserID = f.UserID)
			INNER join YEAR_CLASS as h on (h.YearClassID=g.YearClassID)
			WHERE
			e.AcademicYearID = $yearID AND
			d.YearTermID = $yearTermID and
			f.UserID in ($csv_user_id_list)
			";
			$tmpresult = $this->returnArray($sql,3);
			$subjectGroupStudent = array();
			foreach($tmpresult as $k=>$d)
			{
				$subjectGroupStudent[] = array($d['ClassCode'],$d['ClassTitleEN'],$d['ClassNumber']) ;
			}
				
			# Put All Subject Group Code and Name into Array
			for($x=0; $x<sizeof($subjectGroupCodeName);$x++){
				list($code, $name)= $subjectGroupCodeName[$x];
				array_push($allSubjectGroupCode, $code);
				array_push($allSubjectGroupName, $name);
			}
				
			# Put User's ID and Subject Group Code into Array
			for($x=0; $x<sizeof($teachingSubjectGroupCode); $x++){
				$temp[0] = $UserID;
				$temp[1] = $teachingSubjectGroupCode[$x]['ClassCode'];

				array_push($teacherSubjectGroupList, $temp);
			}
				
			/*
			 # Put Student info into Array
			 for($x=0; $x<sizeof($studentInfo); $x++){
			 $temp[0] = $studentInfo[$x]['ClassName'];
			 $temp[1] = $studentInfo[$x]['ClassNumber'];
			 array_push($allStudentInfo, $temp);
			 }
			 */
				
			/*
			 # Put Student info and Subject Group Code into Array
			 for($x=0; $x<sizeof($subjectGroupStudent); $x++){
			 $temp[0] = $subjectGroupStudent[$x]['ClassCode'];
			 $temp[1] = $subjectGroupStudent[$x]['ClassName'];
			 $temp[2] = $subjectGroupStudent[$x]['ClassNumber'];

			 array_push($allSubjectGroupStudent, $temp);
			 }
			 */

			### List out the import result
			$result = "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
			$result .= "<tr>";
			$result .= "<td width='4%' class='tablebluetop tabletopnolink'>#</td>";
			$result .= "<td width='20%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
			$result .= "<td width='20%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Class']."</td>";
			$result .= "<td width='15%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
			$result .= "<td width='40%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Error']."</td>";
			$result .= "</tr>";
				
				
			$this->successCount = 0;
			$this->errorCount = 0;
				
			# Loop each Record
			for($i=0;$i<sizeof($data);$i++){
				if(!is_array($data[$i]) || trim(implode("",$data[$i]))=="")
					continue;

					list($subjectGroupCode, $className, $classNumber) = $data[$i];

					$error_css = "class=\"red\"";
					$err = "";

					# initialize css
					$DataRowError = 0;
					$css_subjectGroupCode = "";
					$css_className = "";
					$css_classNumber = "";

					# Check Empty Field
					for ($j=0; $j < sizeof ($data[$i]); $j++){
						if ($data[$i][$j] =="" && !in_array($j, $allowed_empty_fields))
						{
							$DataRowError++;
							$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['EmptyFields']."</li>";

							$css_subjectGroupCode = ($subjectGroupCode==""? $error_css:"");
							$css_class = ($title==""? $error_css:"");
							$css_classNumber = ($pic==""? $error_css:"");
						}
					}

					# commented by Henry Chow n 20101006 (fail to compare special characters, e.g. "&")
					//$subjectGroupCode = intranet_htmlspecialchars($subjectGroupCode);
					$className = intranet_htmlspecialchars($className);
					$classNumber = intranet_htmlspecialchars($classNumber);

					# Check Subject Group
					# get Subject Group Name if exist
					$searchValue = array_search($subjectGroupCode, $allSubjectGroupCode);
					if($searchValue!==false)
						$subjectGroupName = $allSubjectGroupName[$searchValue];
						else
							$subjectGroupName = $subjectGroupCode;
								
							# Check Subject Group Code exist
							if (!in_array($subjectGroupCode,$allSubjectGroupCode)){
								//echo "No such group<br>";
								$DataRowError++;
								$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['NoSubjectGroup']."</li>";
								$css_subjectGroupCode = $error_css;
							}

							# Check teacher is teaching those subject group
							else if(!in_array(array($UserID, $subjectGroupCode), $teacherSubjectGroupList)){
								$DataRowError++;
								$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['GroupTeacher']."</li>";
								$css_subjectGroupCode = $error_css;
							}

							# Check Student exist
							// 				else if(!in_array(array($className, $classNumber),$allStudentInfo))
							else if(!in_array(array($className, $classNumber),$studentInfo))
							{
								$DataRowError++;
								$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['InvalidUser']."</li>";
								$css_className = $error_css;
								$css_classNumber = $error_css;
							}

							# Check Student exist
							// 				else if(!in_array(array($subjectGroupCode, $className, $classNumber),$allSubjectGroupStudent))
							else if(!in_array(array($subjectGroupCode, $className, $classNumber),$subjectGroupStudent))
							{
								$DataRowError++;
								$err .= "<li>".$Lang['SysMgr']['Homework']['Remark']['GroupStudent']."</li>";
								$css_className = $error_css;
								$css_classNumber = $error_css;
							}

							if($DataRowError>0){
								$this->errorCount++;
								$result .= "<tr valign=\"top\">";
								$result .= "<td>".($i+1)."</td>";
								$result .= "<td $css_subjectGroupCode>$subjectGroupName</td>";
								$result .= "<td $css_className>$className</td>";
								$result .= "<td $css_classNumber>$classNumber</td>";
								$result .= "<td $error_css>$err</td></tr>";
							}

							else
								$this->successCount++;

			}
			$result .= "</table>";
			return $result;
		}

		# Import Subject Leader
		function insertSubjectLeaderRecord($tempData, $YearTermID=""){
			global $UserID;
			$recordCount = 0;
				
			if($YearTermID=="") $YearTermID = getCurrentSemesterID();
				
			# Insert records to Live table
			for ($i=0; $i<sizeof($tempData); $i++){
				$sql = "SELECT a.SubjectID, a.SubjectGroupID
				FROM SUBJECT_TERM AS a
				INNER JOIN SUBJECT_TERM_CLASS AS b ON (b.SubjectGroupID = a.SubjectGroupID AND a.YearTermID='$YearTermID')
				WHERE b.ClassCode = '".$tempData[$i][0]."'";
					
				$result = $this->returnArray($sql, 2);
				$subjectID = $result[0]['SubjectID'];
				$subjectGroupID = $result[0]['SubjectGroupID'];

				$sql = "SELECT UserID
				FROM INTRANET_USER
				WHERE ClassName = '".$tempData[$i][1]."' AND ClassNumber = ".$tempData[$i][2].' AND RecordType = 2';

				$result = $this->returnArray($sql, 1);
				$studentID = $result[0]['UserID'];

				$sql = "SELECT LeaderID, UserID, RecordStatus
				FROM INTRANET_SUBJECT_LEADER
				WHERE ClassID = ".$subjectGroupID." AND UserID = ".$studentID;

				$result = $this->returnArray($sql);

				if(sizeof($result)!= 0){
					if($result[0]['RecordStatus']=="0"){
						$sql = "UPDATE INTRANET_SUBJECT_LEADER
						SET RecordStatus = '1', DateModified = now()
						WHERE LeaderID = ".$result[0]['LeaderID'];
						$result = $this->db_db_query($sql);
					}
				}
					
				else{
					$sql = "INSERT INTO INTRANET_SUBJECT_LEADER
					(UserID, ClassID, SubjectID, RecordStatus, DateInput)
					VALUES
					($studentID, $subjectGroupID, $subjectID, '1', now())";
					$result = $this->db_db_query($sql);
				}

				if($result)
					$recordCount++;
			}
				
			return $recordCount;
		}

		############################################################################
		function getCurrentAcademicYearAndYearTerm(){
			global $intranet_session_language;
			/*
			 $currentYearTermInfo = getAcademicYearInfoAndTermInfoByDate();
			 debug_pr($currentYearTermInfo);
			 */
			$sql = "SELECT a.AcademicYearID,
			IF('$intranet_session_language'= 'en', a.YearNameEN, a.YearNameB5),
			a.Sequence, b.YearTermID,
			IF('$intranet_session_language'= 'en', b.YearTermNameEN, b.YearTermNameB5),
			b.TermStart,
			b.TermEnd
			FROM ACADEMIC_YEAR as a
			INNER JOIN ACADEMIC_YEAR_TERM as b ON a.AcademicYearID = b.AcademicYearID
			WHERE NOW() between b.TermStart and b.TermEnd";
				
			//echo $sql;
			$Result = $this->returnArray($sql);
				
			return $Result;
		}

		function getSlectedList($attrib, $result, $selected="", $firstValue="", $displayAllClassValue=true){
			$x = "<SELECT $attrib>\n";
			$empty_selected = ($selected == '')? "SELECTED":"";
			if($displayAllClassValue)
			{
				$x .= "<OPTION value='' $empty_selected>$firstValue</OPTION>\n";
			}

			for ($i=0; $i < sizeof($result); $i++)
			{
				list($id, $name) = $result[$i];
				$selected_str = ($id==$selected? "SELECTED":"");
				$x .= "<OPTION value='$id' $selected_str>$name</OPTION>\n";
			}
			$x .= "</SELECT>\n";

			return $x;
		}

		function getAcademicYearNameAndYearTermName($yearID, $yearTermID=""){
			global $intranet_session_language;
				
			$conds = ($yearTermID!="") ? " AND b.YearTermID=$yearTermID" : "";

			$sql = "SELECT IF('$intranet_session_language'= 'en', a.YearNameEN, a.YearNameB5),
			IF('$intranet_session_language'= 'en', b.YearTermNameEN, b.YearTermNameB5)
			FROM ACADEMIC_YEAR AS a
			INNER JOIN ACADEMIC_YEAR_TERM AS b ON a.AcademicYearID = b.AcademicYearID
			WHERE b.AcademicYearID = $yearID $conds LIMIT 1";
				
			//echo $sql;
			$Result = $this->returnArray($sql, 2);
				
			return $Result;
		}

		function getNotSubmitList($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate="", $endDate="", $dateChoice="", $times="", $range="", $group="", $order="", $DueDateEqualToToday=0, $status="-1"){
			global $intranet_session_language;
				
			if($subjectID==""){
				$subjects = $this->getTeachingSubjectList("", $yearID, $yearTermID);
				if(sizeof($subjects)!=0){
					$allSubjects = " AND d.SubjectID IN (";
					for ($i=0; $i < sizeof($subjects); $i++)
					{
						list($sid)=$subjects[$i];
						$allSubjects .= $sid.",";
					}
					$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				} else{
					$allSubjects ="";
				}
			} else {
				$allSubjects = " AND d.SubjectID IN ($subjectID)";
			}

			if($dateChoice=='DATE') {
				$dateConds = " (d.DueDate BETWEEN '$startDate' AND '$endDate')";
			} else {
				$dateConds = "i.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND i.YearTermID = $yearTermID";
			}
				
			if($yearTermID!="") $termConds = " AND d.YearTermID = $yearTermID";
				
			if($times!="" && $range!="")
				$having = ($range==1) ? " HAVING total>=$times" : " HAVING total<=$times";
					
				$groupby = ($group==1) ? " a.UserID " : " h.YearClassID ";
				if($targetID==1){       // class
					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";
                    $studentName = getNameFieldByLang2("c.");

					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,',$status)>',count(DISTINCT e.RecordID),'</a>'),
					d.TypeID,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID AND c.RecordStatus=1
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID ";
					if($dateChoice != 'DATE')
					{
						$sql .= " INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID";
					}
					//$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";

					$sql .= " WHERE $dateConds
					$allSubjects
					AND ( e.RecordStatus = '$status' or e.SuppRecordStatus='$status' )
					$dueDateConds
					$yearClassID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total DESC, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";
					$data = $this->returnArray($sql);
				}
				//debug($sql);
				if($targetID==2){       // student
					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";

					$studentID = " AND e.StudentID IN (";
					for($i=0; $i<sizeof($studentListResult); $i++) {
						$studentID .= $studentListResult[$i].",";
					}
					$studentID = substr($studentID,0,strlen($studentID)-1).")";
						
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					IF('$intranet_session_language'='en', c.EnglishName, c.ChineseName) AS StudentName,
					CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,',$status)>',count(DISTINCT e.RecordID),'</a>'),
					d.TypeID,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					AND e.RecordStatus = '$status'";
					//$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
					$sql .= "
					$dueDateConds
					$yearClassID
					$studentID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
					
				if($targetID==3){       // subject group
					$subjectGroupID = " AND d.ClassGroupID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$subjectGroupID .= $targetListResult[$i].",";
					}
					$subjectGroupID = substr($subjectGroupID,0,strlen($subjectGroupID)-1).")";

					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					IF('$intranet_session_language'='en', c.EnglishName, c.ChineseName) AS StudentName,
					CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,',$status)>',count(DISTINCT e.RecordID),'</a>'),
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$subjectGroupID
					AND e.RecordStatus = '$status'";
					// [2015-0730-1036-32207] remove due dete condition, allow searching homework before due date
					//$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
					$sql .= "
					$dueDateConds
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
				//echo $sql;

                if($targetID==4){       // form (class level)
                    $formID = " AND h.YearID IN (";
                    for($i=0; $i<sizeof($targetListResult); $i++) {
                        $formID .= $targetListResult[$i].",";
                    }
                    $formID = substr($formID,0,strlen($formID)-1).")";


                    $sql = "SELECT c.ClassName, c.ClassNumber,
                        IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
                        IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
                        IF('$intranet_session_language'='en', c.EnglishName, c.ChineseName) AS StudentName,
                        CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,',$status)>',count(DISTINCT e.RecordID),'</a>'),
                        d.TypeID,
                        count(DISTINCT e.RecordID) as total
                        FROM YEAR_CLASS_USER AS a
                        INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
                        INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID AND c.RecordStatus=1
                        INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
                        INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
                        INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
                        INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
                        INNER JOIN YEAR_CLASS AS h ON (h.YearClassID = a.YearClassID AND h.AcademicYearID='{$yearID}') ";
                    if($dateChoice != 'DATE')
                    {
                        $sql .= " INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID";
                    }
                    //$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";

                    $sql .= " WHERE $dateConds
                        $allSubjects
                        AND ( e.RecordStatus = '$status' or e.SuppRecordStatus='$status' )
                        $dueDateConds
                        $formID
                        Group by f.SubjectGroupID, $groupby
                        $having";
                    $sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total DESC, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";
                    $data = $this->returnArray($sql);
                }

				return $data;
		}

		function getSupplementaryList($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate="", $endDate="", $dateChoice="", $times="", $range="", $group="", $order="", $DueDateEqualToToday=0){
			global $intranet_session_language;
				
			if($subjectID==""){
				$subjects = $this->getTeachingSubjectList("", $yearID, $yearTermID);
				if(sizeof($subjects)!=0){
					$allSubjects = " AND d.SubjectID IN (";
					for ($i=0; $i < sizeof($subjects); $i++)
					{
						list($sid)=$subjects[$i];
						$allSubjects .= $sid.",";
					}
					$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
				else{
					$allSubjects ="";
				}
			}

			else{
				$allSubjects = " AND d.SubjectID IN ($subjectID)";
			}

			if($dateChoice=='DATE') {
				$dateConds = " (d.DueDate BETWEEN '$startDate' AND '$endDate')";
			} else {
				$dateConds = "i.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND i.YearTermID = $yearTermID";
			}
				
			if($yearTermID!="") $termConds = " AND d.YearTermID = $yearTermID";
				
			if($times!="" && $range!="")
				$having = ($range==1) ? " HAVING total>=$times" : " HAVING total<=$times";
					
				$groupby = ($group==1) ? " a.UserID " : " h.YearClassID ";
					
                $studentName = getNameFieldByLang2("c.");

				if($targetID==1){
					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,')>',count(DISTINCT e.RecordID),'</a>'),
					d.TypeID,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID ";
					if($dateChoice != 'DATE')
					{
						$sql .= " INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID";
					}
					$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";

					$sql .= " WHERE $dateConds
					$allSubjects
					AND ( e.RecordStatus in ('6') OR e.SuppRecordStatus in ('7', '-11') )
					$dueDateConds
					$yearClassID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total DESC, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;
					//echo $sql;
					$data = $this->returnArray($sql);
				}
				//echo $sql;
				if($targetID==2){
					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";

					$studentID = " AND e.StudentID IN (";
					for($i=0; $i<sizeof($studentListResult); $i++) {
						$studentID .= $studentListResult[$i].",";
					}
					$studentID = substr($studentID,0,strlen($studentID)-1).")";
						
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,')>',count(DISTINCT e.RecordID),'</a>'),
					d.TypeID,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					AND ( e.RecordStatus in ('6') OR e.SuppRecordStatus in ('7', '-11') )";
					$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
					$sql .= "
					$yearClassID
					$studentID
					$dueDateConds
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
					
				if($targetID==3){
					$subjectGroupID = " AND d.ClassGroupID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$subjectGroupID .= $targetListResult[$i].",";
					}
					$subjectGroupID = substr($subjectGroupID,0,strlen($subjectGroupID)-1).")";

					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					CONCAT('<a class=tablelink href=javascript:viewNotHandinDetail(',b.SubjectGroupID,',',b.UserID,',\"$yearID\",\"$yearTermID\",',h.YearClassID,')>',count(DISTINCT e.RecordID),'</a>'),
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$subjectGroupID
					AND ( e.RecordStatus in ('6') OR e.SuppRecordStatus in ('7', '-11') )";
					$dueDateConds.= ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
					$sql .= "
					$dueDateConds
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
				//echo $sql;
				return $data;
		}

		function getNotSubmitListPrint($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate="", $endDate="", $dateChoice="", $times="", $range="", $order="", $group="", $DueDateEqualToToday=0, $status="-1"){
			global $intranet_session_language;
			//debug_pr($targetListResult);
				
			if($dateChoice=='DATE') {
				$dateConds = " d.DueDate BETWEEN '$startDate' AND '$endDate'";
				$yearInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
				$yearID = $yearInfo[0];
				$yearTermID = $yearInfo[2];
				# if YearTermID of start date & end date are different, then homework should be included both semesters
				$currentYearTerm2 = getAcademicYearInfoAndTermInfoByDate($endDate);
				$endDate_yearTermID = $currentYearTerm2[2];
				if($endDate_yearTermID!=$yearTermID)
					$yearTermID = "";

			} else {
				$dateConds = "i.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND i.YearTermID = $yearTermID";
			}
				
			if($subjectID==""){
				$subjects = $this->getTeachingSubjectList("", $yearID, $yearTermID);
				if(sizeof($subjects)!=0){
					$allSubjects = " AND d.SubjectID IN (";
					for ($i=0; $i < sizeof($subjects); $i++)
					{
						list($sid)=$subjects[$i];
						$allSubjects .= $sid.",";
					}
					$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
				else{
					$allSubjects ="";
				}
			}

			else{
				$allSubjects = " AND d.SubjectID IN ($subjectID)";
			}

			if($yearTermID!="") $termConds = " AND d.YearTermID = $yearTermID";
				
			if($times!="" && $range!="")
				$having = ($range==1) ? " HAVING total>=$times" : " HAVING total<=$times";
					
				$groupby = ($group==1) ? " a.UserID " : " h.YearClassID ";
					
				//$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
					
                $studentName = getNameFieldByLang2("c.");

				if($targetID==1){
					$yearClassID = " AND a.YearClassID IN (".implode(',',$targetListResult).")";
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID and c.RecordStatus=1
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					AND e.RecordStatus = '$status'
					$dueDateConds
					$yearClassID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total DESC, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					$data = $this->returnArray($sql, 6);
				}
					
				if($targetID==2){

					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";

					$studentID = " AND e.StudentID IN (";
					for($i=0; $i<sizeof($studentListResult); $i++) {
						$studentID .= $studentListResult[$i].",";
					}
					$studentID = substr($studentID,0,strlen($studentID)-1).")";
						
					//AND d.DueDate < CURDATE()
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID and c.RecordStatus=1
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					AND e.RecordStatus = '$status'

					$yearClassID
					$studentID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
					
				if($targetID==3){

					$subjectGroupID = " AND d.ClassGroupID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$subjectGroupID .= $targetListResult[$i].",";
					}
					$subjectGroupID = substr($subjectGroupID,0,strlen($subjectGroupID)-1).")";

					//AND d.DueDate < CURDATE()
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID and c.RecordStatus=1
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$subjectGroupID
					AND e.RecordStatus = '$status'
						
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? "	Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : "	Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
				//echo $sql;
				return $data;
		}

		function getSupplementaryListPrint($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate="", $endDate="", $dateChoice="", $times="", $range="", $order="", $group="", $DueDateEqualToToday=0){
			global $intranet_session_language;
			//debug_pr($targetListResult);
				
			if($dateChoice=='DATE') {
				$dateConds = " d.DueDate BETWEEN '$startDate' AND '$endDate'";
				$yearInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
				$yearID = $yearInfo[0];
				$yearTermID = $yearInfo[2];
				# if YearTermID of start date & end date are different, then homework should be included both semesters
				$currentYearTerm2 = getAcademicYearInfoAndTermInfoByDate($endDate);
				$endDate_yearTermID = $currentYearTerm2[2];
				if($endDate_yearTermID!=$yearTermID)
					$yearTermID = "";

			} else {
				$dateConds = "i.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND i.YearTermID = $yearTermID";
			}
				
			if($subjectID==""){
				$subjects = $this->getTeachingSubjectList("", $yearID, $yearTermID);
				if(sizeof($subjects)!=0){
					$allSubjects = " AND d.SubjectID IN (";
					for ($i=0; $i < sizeof($subjects); $i++)
					{
						list($sid)=$subjects[$i];
						$allSubjects .= $sid.",";
					}
					$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
				else{
					$allSubjects ="";
				}
			}

			else{
				$allSubjects = " AND d.SubjectID IN ($subjectID)";
			}

			if($yearTermID!="") $termConds = " AND d.YearTermID = $yearTermID";
				
			if($times!="" && $range!="")
				$having = ($range==1) ? " HAVING total>=$times" : " HAVING total<=$times";
					
				$groupby = ($group==1) ? " a.UserID " : " h.YearClassID ";
					
				$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
					
                $studentName = getNameFieldByLang2("c.");

				if($targetID==1){
					$yearClassID = " AND a.YearClassID IN (".implode(',',$targetListResult).")";
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					AND e.RecordStatus = '6'
					$dueDateConds
					$yearClassID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total DESC, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					$data = $this->returnArray($sql, 6);
				}
					
				if($targetID==2){

					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";

					$studentID = " AND e.StudentID IN (";
					for($i=0; $i<sizeof($studentListResult); $i++) {
						$studentID .= $studentListResult[$i].",";
					}
					$studentID = substr($studentID,0,strlen($studentID)-1).")";
						
					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					AND e.RecordStatus = '6'
					$dueDateConds
					$yearClassID
					$studentID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
					
				if($targetID==3){

					$subjectGroupID = " AND d.ClassGroupID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$subjectGroupID .= $targetListResult[$i].",";
					}
					$subjectGroupID = substr($subjectGroupID,0,strlen($subjectGroupID)-1).")";

					$sql = "SELECT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					count(DISTINCT e.RecordID) as total
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$subjectGroupID
					AND e.RecordStatus = '6'
					$dueDateConds
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? "	Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : "	Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql, 6);
				}
				//echo $sql;
				return $data;
		}

		function RetrieveSubjectGroupTeacherLogin($SubjectGroupCode='', $yearTermID='')
		{
			if($SubjectGroupCode && $yearTermID)
			{
				$sql = "select
				c.UserLogin, c.UserID
				from
				SUBJECT_TERM_CLASS_TEACHER as a
				INNER join SUBJECT_TERM_CLASS as b on (b.SubjectGroupID = a.SubjectGroupID)
				INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID = b.SubjectGroupID AND YearTermID = '$yearTermID'
				left join INTRANET_USER as c on (c.UserID = a.UserID)
				where
				b.ClassCode='$SubjectGroupCode'
				";

				$result = $this->returnArray($sql);

				return $result;
			}
		}

		function RetrieveSubjectbySubjectGroupID($SubjectGroupID)
		{
			global $intranet_session_language;
				
			$sql = "select
			a.SubjectID,
			IF('$intranet_session_language'='en', b.EN_DES, b.CH_DES) as SubjectName
			from
			SUBJECT_TERM as a
			left join ASSESSMENT_SUBJECT as b on (b.RecordID = a.SubjectID)
			where
			SubjectGroupID = $SubjectGroupID
			";
			$result = $this->returnArray($sql);
				
			return $result;
		}

		function getAllClassInfo($yearID="", $form="")
		{
			if($yearID=="") $yearID = Get_Current_Academic_Year_ID();
			if($form!="") $conds = " AND yc.YearID=$form";
			$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." FROM YEAR_CLASS yc LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE yc.AcademicYearID=$yearID $conds ORDER BY y.Sequence, yc.Sequence";
			return $this->returnArray($sql, 2);
		}

		function getHomeworkType($typeID)
		{
			$sql = "SELECT * FROM INTRANET_HOMEWORK_TYPE WHERE TypeID=$typeID";
			return $this->returnArray($sql);
		}

		function getHomeworkTypeMaxOrder()
		{
			$sql = "SELECT MAX(DisplayOrder) FROM INTRANET_HOMEWORK_TYPE WHERE RecordStatus!=-1";
			$result = $this->returnVector($sql);
			return $result[0];
		}

		function updateHomeworkTypeOrder($old, $new)
		{
			$sql = "SELECT TypeID, DisplayOrder FROM INTRANET_HOMEWORK_TYPE WHERE DisplayOrder=$old";
			$result = $this->returnArray($sql, 2);
			$typeID = $result[0][0];
			$order = $result[0][1];
				
			$maxOrder = $this->getHomeworkTypeMaxOrder();
			//$sql = "UPDATE INTRANET_HOMEWORK_TYPE SET DisplayOrder=$new WHERE DisplayOrder=$old";
			//$this->db_db_query($sql);
				
			if($new==1) {
				$sql = "UPDATE INTRANET_HOMEWORK_TYPE SET DisplayOrder=DisplayOrder+1 WHERE DisplayOrder<$old";
			} else if ($new==$maxOrder) {
				$sql = "UPDATE INTRANET_HOMEWORK_TYPE SET DisplayOrder=DisplayOrder-1 WHERE DisplayOrder>$old";
			} else {
				$sql = "UPDATE INTRANET_HOMEWORK_TYPE SET DisplayOrder=$old WHERE DisplayOrder=$new";
			}
			$this->db_db_query($sql);
				
			$sql = "UPDATE INTRANET_HOMEWORK_TYPE SET DisplayOrder=$new WHERE TypeID=$typeID";
			$this->db_db_query($sql);
		}

		function isClassTeacher($YearClassID="", $teacherID)
		{
			$conds = " UserID=$teacherID";
			$conds .= ($YearClassID!="") ? " AND YearClassID=$YearClassID" : "";
			$sql = "SELECT COUNT(*) FROM YEAR_CLASS_TEACHER WHERE $conds";
			$result = $this->returnVector($sql);
			$returnValue = ($result[0]==0) ? 0 : 1;
				
			return $returnValue;
				
		}

		function getClassInfoByTeacherID($AcademicYearID, $teacherID)
		{
			$sql = "SELECT
				yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")."
				FROM
				YEAR_CLASS_TEACHER yct LEFT OUTER JOIN
				YEAR_CLASS yc ON (yct.YearClassID=yc.YearClassID)
				WHERE
				yc.AcademicYearID=$AcademicYearID AND yct.UserID=$teacherID
				";
			return $this->returnArray($sql, 2);
		}
		
		//function getAllClassesInvolvedByTeacherID($AcademicYearID="", $YearTermID="", $teacherID)
		function getAllClassesInvolvedByTeacherID($AcademicYearID="", $YearTermID="", $teacherID, $returnClassSeq=false)
		{
		    // [2020-0923-1234-59235]
		    $extra_fields = $returnClassSeq ? " , y.Sequence as ySeq, yc.Sequence as ycSeq " : "";
		    
			$sql = "SELECT
        				yc.YearClassID,
        				".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")."
                        $extra_fields
				    FROM
        				YEAR_CLASS yc 
        				INNER JOIN YEAR y ON (y.YearID = yc.YearID) 
        				INNER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID = yc.YearClassID) 
        				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID = yc.YearClassID) 
        				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID = ycu.UserID) 
        				INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID = stcu.SubjectGroupID) 
        				INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID = stct.SubjectGroupID)
				    WHERE
        				yc.AcademicYearID = '$AcademicYearID' AND 
        				((yct.UserID = '$teacherID') OR (st.YearTermID = '$YearTermID' AND stct.UserID = '$teacherID'))
    				GROUP BY yc.YearClassID
    				ORDER BY y.Sequence, yc.Sequence
				";
				//debug_pr($sql);
			/*
			 $sql = "SELECT
				yc.YearClassID,
				".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")."
				FROM
				YEAR_CLASS yc LEFT OUTER JOIN
				YEAR_CLASS_TEACHER yct ON (yct.YearClassID=yc.YearClassID) LEFT OUTER JOIN

				";
				*/
				
			return $this->returnArray($sql, 2);
		}

		//unction getClassesInvolvedBySubjectGroupTeacherID($AcademicYearID="", $YearTermID="", $teacherID)
		function getClassesInvolvedBySubjectGroupTeacherID($AcademicYearID="", $YearTermID="", $teacherID, $returnClassSeq=false)
		{
		    // [2020-0923-1234-59235]
		    $extra_fields = $returnClassSeq ? " , y.Sequence as ySeq, yc.Sequence as ycSeq " : "";
		    $extra_joins = $returnClassSeq ? " INNER JOIN YEAR y ON (yc.YearID = y.YearID) " : "";
		    
			$sql = "SELECT
        				yc.YearClassID,
        				".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")."
                        $extra_fields
    				FROM
        				YEAR_CLASS yc 
                        INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcademicYearID') 
                        INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID = ycu.UserID) 
                        INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID = stcu.SubjectGroupID AND stct.UserID = '$teacherID') 
                        INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID = stct.SubjectGroupID AND st.YearTermID = '$YearTermID')
                        $extra_joins
    				GROUP BY yc.YearClassID
    				ORDER BY yc.Sequence
				";
			return $this->returnArray($sql, 2);
		}

		function getSubjectGroupIDByTeacherID($teacherID, $AcademicYearID="", $YearTermID="")
		{
			$AcademicYearID = ($AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
			$YearTermID = ($YearTermID=="") ? getCurrentSemesterID() : $YearTermID;
				
			$sql = "SELECT
				STC.SubjectGroupID, ".Get_Lang_Selection("STC.ClassTitleB5","STC.ClassTitleEN")."
				FROM
				SUBJECT_TERM_CLASS STC LEFT OUTER JOIN
				SUBJECT_TERM ST ON (ST.SubjectGroupID=STC.SubjectGroupID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER STCT ON (STCT.SubjectGroupID=STC.SubjectGroupID)
				WHERE
				STCT.UserID=$teacherID AND ST.YearTermID=$YearTermID
				$conds
				";
				return $this->returnArray($sql, 2);
		}

		function getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate="", $endDate="", $dateChoice="", $times="", $range="", $order="", $group="", $DueDateEqualToToday=0,$status="-1"){
			global $intranet_session_language;
			//debug_pr($targetListResult);
				
			if($dateChoice=='DATE') {
				$dateConds = " d.DueDate BETWEEN '$startDate' AND '$endDate'";
				$yearInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
				$yearID = $yearInfo[0];
				$yearTermID = $yearInfo[2];
				# if YearTermID of start date & end date are different, then homework should be included both semesters
				$currentYearTerm2 = getAcademicYearInfoAndTermInfoByDate($endDate);
				$endDate_yearTermID = $currentYearTerm2[2];
				if($endDate_yearTermID!=$yearTermID)
					$yearTermID = "";

			} else {
				$dateConds = "i.AcademicYearID = $yearID";
				if($yearTermID!="") $dateConds .= " AND i.YearTermID = $yearTermID";
			}
				
			if($subjectID==""){
				$subjects = $this->getTeachingSubjectList("", $yearID, $yearTermID);
				if(sizeof($subjects)!=0){
					$allSubjects = " AND d.SubjectID IN (";
					for ($i=0; $i < sizeof($subjects); $i++)
					{
						list($sid)=$subjects[$i];
						$allSubjects .= $sid.",";
					}
					$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
				}
				else{
					$allSubjects ="";
				}
			}

			else{
				$allSubjects = " AND d.SubjectID IN ($subjectID)";
			}

			if($yearTermID!="") $termConds = " AND d.YearTermID = $yearTermID";
				
			if($times!="" && $range!="")
				$having = ($range==1) ? " HAVING total>=$times" : " HAVING total<=$times";
					
				$groupby = ($group==1) ? " a.UserID " : " h.YearClassID ";
				$groupby .= ", d.HomeworkID, b.UserID ";
					
				//$dueDateConds = ($DueDateEqualToToday) ? " AND d.DueDate <= CURDATE()" : " AND d.DueDate < CURDATE()";
				if ($status==-3)
				{
					$sqlhandinStatus = " AND e.RecordStatus in (-1, 2) ";
				} else
				{
					$sqlhandinStatus = " AND e.RecordStatus = '$status' ";
				}
					
                $studentName = getNameFieldByLang2("c.");

				if($targetID==1){
					$yearClassID = " AND a.YearClassID IN (".implode(',', (array)$targetListResult).")";
					$sql = "SELECT DISTINCT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					d.Title,
					d.Description,
					d.StartDate,
					d.DueDate,
					count(DISTINCT e.RecordID) as total,
					c.UserID,
					e.RecordStatus
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID and c.RecordStatus=1
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					$sqlhandinStatus
					$dueDateConds
					$yearClassID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total DESC, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;
					//echo $sql;
					$data = $this->returnArray($sql);
				}
					
				if($targetID==2){

					$yearClassID = " AND a.YearClassID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$yearClassID .= $targetListResult[$i].",";
					}
					$yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";

					$studentID = " AND e.StudentID IN (";
					for($i=0; $i<sizeof($studentListResult); $i++) {
						$studentID .= $studentListResult[$i].",";
					}
					$studentID = substr($studentID,0,strlen($studentID)-1).")";
						
					//AND d.DueDate < CURDATE()
					$sql = "SELECT DISTINCT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					d.Title,
					d.Description,
					d.StartDate,
					d.DueDate,
					count(DISTINCT e.RecordID) as total,
					c.UserID,
					e.RecordStatus
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$allSubjects
					$sqlhandinStatus

					$yearClassID
					$studentID
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? " Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : " Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql);
				}
					
				if($targetID==3){

					$subjectGroupID = " AND d.ClassGroupID IN (";
					for($i=0; $i<sizeof($targetListResult); $i++) {
						$subjectGroupID .= $targetListResult[$i].",";
					}
					$subjectGroupID = substr($subjectGroupID,0,strlen($subjectGroupID)-1).")";

					//AND d.DueDate < CURDATE()
					$sql = "SELECT DISTINCT c.ClassName, c.ClassNumber,
					IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
					IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
					{$studentName} AS StudentName,
					d.Title,
					d.Description,
					d.StartDate,
					d.DueDate,
					count(DISTINCT e.RecordID) as total,
					c.UserID,
					e.RecordStatus
					FROM YEAR_CLASS_USER AS a
					INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
					INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID
					INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
					INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
					INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
					INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
					INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID
					INNER JOIN ACADEMIC_YEAR_TERM AS i ON i.AcademicYearID = h.AcademicYearID
					WHERE $dateConds
					$subjectGroupID
					$sqlhandinStatus
						
					Group by f.SubjectGroupID, $groupby
					$having";
					$sql .= ($order==1) ? "	Order by c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC" : "	Order by total desc, c.ClassName ASC, c.ClassNumber ASC, Subject ASC, SubjectGroup ASC";;

					//echo $sql;
					$data = $this->returnArray($sql);
				}

				return $data;
		}

		function IS_ADMIN($user_id)
		{
			$sql = "SELECT rr.* FROM ROLE_RIGHT rr INNER JOIN ROLE_MEMBER rm ON (rr.RoleID=rm.RoleID) INNER JOIN INTRANET_USER USR ON (USR.UserID=rm.UserID) WHERE rr.FunctionName='eAdmin-eHomework' AND USR.UserLogin='$user_id' GROUP BY rr.RoleID";
			$count = $this->returnVector($sql);
				
			$returnFlag = ($count[0]==0) ? 0 : 1;
			return $returnFlag;
		}

		function retrieveStudentListByYearClassID($YearClassID="", $displayFormat="1")
		{
			if(!empty($YearClassID))
			{
				$name_field = getNameFieldByLang("USR.");
				if($displayFormat == 1){
					$displayField = " $name_field ";
				}
				else if($displayFormat == 2){
					$displayField = " CONCAT($name_field , ' ( ', ycu.ClassNumber , ' ) ') ";
				}
				
				$YearClassCond = "ycu.YearClassID = '$YearClassID'";
				if(is_array($YearClassID)) {
					$YearClassCond = "ycu.YearClassID IN ('".implode("', '", (array)$YearClassID)."')";
				}
				
				$sql = "SELECT USR.UserID, $displayField as name FROM YEAR_CLASS_USER ycu INNER JOIN INTRANET_USER USR ON (USR.UserID = ycu.UserID) WHERE $YearClassCond ORDER BY ycu.ClassNumber";
				return $this->returnArray($sql,2);
			}
		}

		function retrieveSubjectGroupByStudentID($YearTermID="", $studentID="")
		{
			if($YearTermID=="") $YearTermID = getCurrentSemesterID();
			if($studentID!="") {
				$sql = "SELECT stc.SubjectGroupID, CONCAT(".Get_Lang_Selection("SUB.CH_DES","SUB.EN_DES").",' - ',".Get_Lang_Selection("stc.ClassTitleB5", "stc.ClassTitleEN").") as name FROM SUBJECT_TERM_CLASS_USER stcu INNER JOIN SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=stcu.SubjectGroupID) INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stc.SubjectGroupID) INNER JOIN ASSESSMENT_SUBJECT SUB ON (SUB.RecordID=st.SubjectID) WHERE st.YearTermID='$YearTermID' AND stcu.UserID='$studentID'";
				return $this->returnArray($sql,2);
			}
		}

		function getGroupMemberByGroupIDAndClassID($groupID="", $YearClassID="")
		{
			$name_field = getNameFieldByLang("USR.");
			$sql = "SELECT stcu.UserID, CONCAT('(',".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', ycu.ClassNumber, ') ', $name_field) as name FROM SUBJECT_TERM_CLASS_USER stcu INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) INNER JOIN INTRANET_USER USR ON (USR.UserID=ycu.UserID) WHERE yc.YearClassID='$YearClassID' AND stcu.SubjectGroupID='$groupID' ORDER BY yc.Sequence, ycu.ClassNumber";
			$data = $this->returnArray($sql,2);
			return $data;
		}

		function getSubjectLeaderInfoByGroupID($SubjectGroupID="", $YearClassID="") {
			$name_field = getNameFieldByLang("USR.");
			$sql = "SELECT
			sl.UserID,
			CONCAT('(',".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", ' - ', ycu.ClassNumber, ') ', $name_field) as name
			FROM
			SUBJECT_TERM_CLASS_USER stcu INNER JOIN
			INTRANET_SUBJECT_LEADER sl ON (stcu.SubjectGroupID=sl.ClassID) INNER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=sl.UserID) INNER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) INNER JOIN
			INTRANET_USER USR ON (USR.UserID=ycu.UserID)
			WHERE
			sl.ClassID='$SubjectGroupID' AND
			sl.RecordStatus=1 AND
			yc.YearClassID='$YearClassID'
			GROUP BY USR.UserID
			ORDER BY yc.Sequence, ycu.ClassNumber";
			//echo $sql.'<br>';
			return $this->returnArray($sql,2);

		}

		function getSubjectLeaderByGroupID($SubjectGroupID="", $YearClassID="") {
			if($SubjectGroupID!="") {
				if($YearClassID!="") {
					$memberAry = $this->getGroupMemberByGroupIDAndClassID($SubjectGroupID, $YearClassID);
					$memberIDs = array();
					for($j=0; $j<sizeof($memberAry); $j++)
						$memberIDs[] = $memberAry[$j][0];
						if(sizeof($memberIDs)>0)
							$conds = " AND sl.UserID IN (".implode(',',$memberIDs).")";
				}

				$sql = "SELECT sl.UserID FROM INTRANET_SUBJECT_LEADER sl WHERE sl.ClassID='$SubjectGroupID' AND sl.RecordStatus=1 $conds";
				return $this->returnVector($sql);
			}
		}

		function getSubjectGroupByClassID($YearClassID)
		{
			$sql = "SELECT stcu.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER stcu INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID) WHERE st.YearTermID='".getCurrentSemesterID()."' AND ycu.YearClassID='$YearClassID' GROUP BY stcu.SubjectGroupID";
			return $this->returnVector($sql);
		}

		# used by export_collection_list.php in homework list
		function exportHomeworkCollectionList($conds, $classID=""){
			global $intranet_session_language, $i_general_yes, $Lang;
				
			$name_field = getNameFieldByLang("USR.");

			# Select records from INTRANET_HOMEWORK table
			$fields = "
		h.HomeworkID,
		CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").",' - ',ycu.ClassNumber) as classNameNum,
		$name_field as name,
		".Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN")." as subjectGroupName,
		h.Title,
		IF(h.HandinRequired=1, '".$Lang['SysMgr']['Homework']['NotSubmitted']."', '---') as handin,
		USR.UserID,
		h.ClassGroupID
				";

		$dbtables = "
		INTRANET_HOMEWORK h INNER JOIN
		SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=h.ClassGroupID) INNER JOIN
		ASSESSMENT_SUBJECT SUB ON (h.SubjectID=SUB.RecordID) INNER JOIN
		SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=stc.SubjectGroupID) INNER JOIN
		YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN
		YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') INNER JOIN
		YEAR y ON (y.YearID=yc.YearID) INNER JOIN
		INTRANET_USER USR ON (USR.UserID=ycu.UserID)

	";
			
		if($classID!="") {
			$conds .= " AND ycu.YearClassID='$classID'";
		}
			
		$conds .= " AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'";

		//$sql = ($conds=='')? "SELECT $fields FROM $dbtables" : "SELECT $fields FROM $dbtables WHERE $conds GROUP BY stcu.UserID, stc.SubjectGroupID, h.HomeworkID";
			
		if($conds !="") $conds = " WHERE $conds";
			
		$sql = "SELECT $fields FROM $dbtables $conds";
			
		$sql .= " order by y.Sequence, yc.Sequence, ycu.ClassNumber";
		//echo $sql; exit;
		return $this->returnArray($sql, 9);
		}

		function getHomeworkSubmitStatusByHomeworkID($HomeworkIDAry=array())
		{
			$ReturnAry = array();
				
			if(sizeof($HomeworkIDAry)>0) {
				$sql = "SELECT l.HomeworkID, stcu.UserID, RecordStatus FROM SUBJECT_TERM_CLASS_USER stcu INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST l ON (stcu.UserID=l.StudentID) INNER JOIN INTRANET_HOMEWORK h ON (h.HomeworkID=l.HomeworkID AND stcu.SubjectGroupID=h.ClassGroupID)  WHERE l.HomeworkID IN (".implode(',', $HomeworkIDAry).") GROUP BY l.HomeworkID, l.StudentID";

				$result = $this->returnArray($sql,3);

				for($i=0; $i<sizeof($result); $i++) {
					list($homeworkid, $studentid, $status) = $result[$i];
					$ReturnAry[$homeworkid][$studentid] = $status;
				}
			}
			return $ReturnAry;
		}

		function getStudentListBySubjectGroupID($SubjectGroupID)
		{
			$sql = "SELECT UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID='$SubjectGroupID'";
			return $this->returnVector($sql);
		}

		function memberInViewerGroup()
		{
			$sql = "SELECT UserID FROM INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER";
			return $this->returnVector($sql);
		}

		function addViewerGroupMember($userAry=array())
		{
			if(sizeof($userAry)>0) {
				for($i=0, $i_max=sizeof($userAry); $i<$i_max; $i++) {
					$sql = "SELECT COUNT(*) FROM INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER WHERE UserID='".$userAry[$i]."'";
					$count = $this->returnVector($sql);
					if($count[0]==0) {
						$sql = "INSERT INTO INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER SET UserID='".$userAry[$i]."', DateInput=NOW()";
						$result[] = $this->db_db_query($sql);
					}
				}
				return $result;
			}
		}

		function removeViewerGroupMember($userAry=array())
		{
			if(sizeof($userAry)>0) {
				$sql = "DELETE FROM INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER WHERE UserID IN (".implode(',', $userAry).")";
				$result[] = $this->db_db_query($sql);
				return $result;
			}
		}

		function isViewerGroupMember($userid="")
		{
			$sql = "SELECT COUNT(*) FROM INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER WHERE UserID='$userid'";
			$count = $this->returnVector($sql);
			return $count[0]>0 ? 1 : 0;
		}

		function isViewOnly($userid="")
		{
			global $UserID;
				
			# Module Admin
			if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) return 0;
				
			if($userid=="") $userid = $UserID;
				
			$classTeacher = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $userid);
			/*
			 if(sizeof($classTeacher)==0) {	# not class teacher, then check whether user is subject teacher
			 $yearTermID = getCurrentSemesterID();
			 $sql = "SELECT COUNT(*) FROM SUBJECT_TERM_CLASS_TEACHER stct INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stct.SubjectGroupID) WHERE stct.UserID='$userid' AND st.YearTermID='$yearTermID'";
			 $result = $this->returnVector($sql);	# check is subject teacher?
			 return ($result[0]>0) ? 0 : 1;
			 } else {	# class teacher
			 //return ($this->OnlyCanEditDeleteOwn) ? 1 : 0;
			 return 0;

			 }
			 */
			$yearTermID = getCurrentSemesterID();
			$sql = "SELECT COUNT(*) FROM SUBJECT_TERM_CLASS_TEACHER stct INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stct.SubjectGroupID) WHERE stct.UserID='$userid' AND st.YearTermID='$yearTermID'";
			$result = $this->returnVector($sql);
				
			if($result[0]>0) {
				# is subject group teacher
				//echo 0;
				return 0;
			} else {	# is class teacher
				if(count($classTeacher)>0 && !$this->ClassTeacherCanViewHomeworkOnly) {
					//echo 1;
					return 0;
				} else {
					//echo 2;
					return 1;
				}
			}
		}

		function Get_Handin_Status($returnAssociateArray=0)
		{
			global $Lang, $sys_custom;
			/*
			 $returnAry[] = array("1",$Lang['SysMgr']['Homework']['Submitted']);
			 $returnAry[] = array("-1",$Lang['SysMgr']['Homework']['NotSubmitted']);
			 $returnAry[] = array("2",$Lang['SysMgr']['Homework']['LateSubmitted']);
			 $returnAry[] = array("4",$Lang['SysMgr']['Homework']['NoNeedSubmit']);
			 $returnAry[] = array("5",$Lang['SysMgr']['Homework']['UnderProcessing']);
			 if($sys_custom['eHomework_Status_Supplementary'])
			 {
			 $returnAry[] = array("6",$Lang['SysMgr']['Homework']['Supplementary']);
			 }
			 return ($returnAssociateArray) ? build_assoc_array($returnAry) : $returnAry;
			 */
			$handInArr = $this->getAllHandInStatus(TRUE);
			if (count($handInArr) > 0) {
				foreach ($handInArr as $kk => $vv) {
					if ($returnAssociateArray) {
						$returnAry[$vv["HandInStatusValue"]] = Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"]);
					} else {
						$returnAry[] = array(
								$vv["HandInStatusValue"],
								Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"])
						);
					}
				}
			}
			return $returnAry;
		}


		function GenerateMonthStats($academicYearID, $formID, $datefrom, $dateto,  $IsExport=false)
		{
			global $intranet_session_language, $Lang;
				
			# get the subjectgroupIDs involved by the select form

			# get homework and subjectgroupID in the period
				
			# consolide the data into
			#  $HWDataConsolidated[$SubjectID][$ClassID][$DayNumber]
			#  $HWDataConsolidated[$SubjectID][$ClassID]["MonthTotal"]
			#  $ClassSummary[$ClassID][$DayNumber]
			#  $DaySummary[$DayNumber]
				
			$HWDataConsolidated = array();
			$ClassSummary = array();
			$DaySummary = array();
				
			$Data2Export = array();
				
			$MonthNow = (int) date("m", strtotime($datefrom));
			$TotalDays = date("t", strtotime($datefrom));
				
			for ($k=1; $k<=$TotalDays; $k++)
			{
				$DaySummary[$k] = 0;
			}

				
			# limit to the year term(s)
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='{$academicYearID}' ";
			$rows = $this->returnVector($sql);
			$YearTermIDS = implode(", ", $rows);
				
			$sql_cond .= ($formID!="" && $formID>0) ? " AND yc.YearID='{$formID}' " : "";
				
			# SQL1: get subject_group_id, subject_id by year_class (joining by studentIDs)
			$sql_new = "SELECT DISTINCT yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, st.SubjectGroupID, st.SubjectID " .
					" FROM " .
					"	YEAR_CLASS AS yc, YEAR_CLASS_USER AS ycu, SUBJECT_TERM_CLASS_USER AS stcu , SUBJECT_TERM AS st " .
					" WHERE" .
					"	   yc.AcademicYearID={$academicYearID}  " .
					" 	and ycu.YearClassID = yc.YearClassID AND stcu.UserID=ycu.UserID and st.SubjectGroupID=stcu.SubjectGroupID " .
					" {$sql_cond} AND st.YearTermID IN ({$YearTermIDS}) " .
					" ORDER BY yc.Sequence, yc.ClassTitleEN  ";
			$ClassSubjectGroupRaw = $this->returnResultSet($sql_new);
			//debug_r($ClassSubjectGroupRaw);
				
			$SubjectInvolved = array();
			$SubjectGroupInvolved = array();
				
			# consolidate the data
			$ClassSubjectGroup = array();
			$SubjectGroupInClasses = array();
			for ($i=0; $i<sizeof($ClassSubjectGroupRaw); $i++)
			{
				$ObjRow = $ClassSubjectGroupRaw[$i];

				$ClassSubjectGroup[$ObjRow["YearClassID"]]["Title"] = ($intranet_session_language=="en") ? $ObjRow["ClassTitleEN"] : $ObjRow["ClassTitleB5"];
				$ClassSubjectGroup[$ObjRow["YearClassID"]]["SubjectGroups"][] = $ObjRow["SubjectGroupID"];
				$ClassSubjectGroup[$ObjRow["YearClassID"]]["SubjectIDs"][] = $ObjRow["SubjectID"];

				if (!in_array($ObjRow["SubjectID"], $SubjectInvolved))
				{
					$SubjectInvolved[] = $ObjRow["SubjectID"];
				}

				if (!in_array($ObjRow["SubjectGroupID"], $SubjectGroupInvolved))
				{
					$SubjectGroupInvolved[] = $ObjRow["SubjectGroupID"];
				}

				if (!in_array($ObjRow["YearClassID"], $SubjectGroupInClasses))
				{
					$SubjectGroupInClasses[$ObjRow["SubjectGroupID"]][] = $ObjRow["YearClassID"];
				}
			}
				
			if (is_array($SubjectGroupInvolved) && sizeof($SubjectGroupInvolved)>0)
			{
				$subject_groups = implode(", ", $SubjectGroupInvolved);
			}


			//debug_r($SubjectInvolved);
			//debug_r($ClassSubjectGroup);
				
			if (is_array($SubjectInvolved) && sizeof($SubjectInvolved)>0)
			{
				$TitleInLanguage = ($intranet_session_language=="en") ? "EN_DES" : "CH_DES";
				# get subjects
				$sql = "SELECT RecordID AS SubjectID, {$TitleInLanguage} AS SubjectTitle FROM ASSESSMENT_SUBJECT where RecordID IN (".implode(",", $SubjectInvolved).") AND RecordStatus='1' ORDER BY DisplayOrder";
				$SubjectArr = $this->returnResultSet($sql);
				//debug_r($SubjectArr);
			}
				

			#$datefrom, $dateto;
			$sql_class = "SELECT DISTINCT ih.HomeworkID, ih.Loading, ih.Title, ih.HandinRequired, ih.SubjectID, ih.ClassGroupID As SubjectGroupID, ih.DueDate  " .
					" FROM " .
					"   INTRANET_HOMEWORK As ih " .
					" WHERE" .
					" 	unix_timestamp(ih.DueDate) >= unix_timestamp('$datefrom') AND unix_timestamp(ih.DueDate) <= unix_timestamp('$dateto')" .
					"   AND ih.ClassGroupID IN ({$subject_groups}) ";
			$hw_raws = $this->returnResultSet($sql_class);
			//debug($sql_class);
				
			for ($i=0; $i<sizeof($hw_raws); $i++)
			{
				$ThisSubjectID = $hw_raws[$i]["SubjectID"];
				$ThisSubjectGroupID = $hw_raws[$i]["SubjectGroupID"];
				$ThisDay = date("j", strtotime($hw_raws[$i]["DueDate"]));
				//debug($ThisSubjectID, $ThisSubjectGroupID, $ThisDay, $hw_raws[$i]["DueDate"]);
				#  $HWDataConsolidated[$SubjectID][$ClassID][$DayNumber]
				for ($j=0; $j<sizeof($SubjectGroupInClasses[$ThisSubjectGroupID]); $j++)
				{
					$ThisClassID = $SubjectGroupInClasses[$ThisSubjectGroupID][$j];
					$HWDataConsolidated[$ThisSubjectID][$ThisClassID][$ThisDay] ++;
					$ClassSummary[$ThisClassID][$ThisDay] ++;
					$DaySummary[$ThisDay] ++;
				}
			}
				
			/*debug_r($HWDataConsolidated);
			 debug_r($ClassSummary);
			 debug_r($DaySummary);*/
				
			$BorderStyle = "1px solid #888888";
				
			$TableRX = "<table width='100%' align='center' border='0' cellpadding='5' cellspacing='0'>\n";
				
			$TableRX .= "<thead>";
			$TableRX .= "<tr class='tabletop'><th rowspan='2' align='center' style='border:{$BorderStyle};'>".$Lang['SysMgr']['Homework']['Subject']."</th><th rowspan='2' align='center' style='border-top:{$BorderStyle};border-bottom:{$BorderStyle};border-right:{$BorderStyle};'>".$Lang['SysMgr']['Homework']['Class']."</th>";
				
			$TableRX .= "<th colspan='{$TotalDays}' style='border-top:{$BorderStyle};border-right:{$BorderStyle};' align='center'>".$Lang['StaffAttendance']['Months'][$MonthNow]."</th>";
			$TableRX .= "<th rowspan='2' align='center' style='border:{$BorderStyle};'>".$Lang['SysMgr']['Homework']['Total']."</th></tr>\n";
			$TotalDays = date("t", strtotime($datefrom));
			for ($i=1; $i<=$TotalDays; $i++)
			{
				if ($i<10)
				{
					$i_display = "&nbsp;". $i ."&nbsp;";
				} else
				{
					$i_display = $i;
				}
				$TableRX .= "<th class='tabletop' align='center' style='border-top:{$BorderStyle}; border-bottom:{$BorderStyle}; border-right:{$BorderStyle};'>".$i_display."</th>";
			}
			$TableRX .= "</thead>\n";
			$TableRX .= "<tbody>";
				
			$RowCounter = 0;
				
			for ($i=0; $i<sizeof($SubjectArr); $i++)
			{
				$RowSubjectID = $SubjectArr[$i]["SubjectID"];
				$ClassTotal = sizeof($ClassSubjectGroup);
				$TableRX .= "<tr><td align='center' rowspan='{$ClassTotal}' style='border:{$BorderStyle};'><b>".$SubjectArr[$i]["SubjectTitle"]."</b></td>\n";

				$Data2Export[$RowCounter][] = $SubjectArr[$i]["SubjectTitle"];

				# classes
				if ($ClassTotal>0)
				{
					$IsJustStart = true;
					foreach ($ClassSubjectGroup AS $YearClassID => $YearClassObj)
					{
						if (!$IsJustStart)
						{
							$Data2Export[$RowCounter][] = "";
						}
						$RowClass = ($RowCounter%2) ? "class='tablerow2'" : "";
						$TopBorder = ($IsJustStart) ? "border-top:{$BorderStyle}" : "";
						$TableRX .= "<td {$RowClass} align='center' style='border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$YearClassObj["Title"]."</td>\n";
						$Data2Export[$RowCounter][] = $YearClassObj["Title"];
						$RowSubjectClassTotal = 0;
						for ($k=1; $k<=$TotalDays; $k++)
						{
							$CellValue = $HWDataConsolidated[$RowSubjectID][$YearClassID][$k];
							if ($CellValue=="")
							{
								$CellValue = "&nbsp;";
							}
							$TableRX .= "<td {$RowClass} align='center' style='border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$CellValue."</td>";
							$RowSubjectClassTotal += $HWDataConsolidated[$RowSubjectID][$YearClassID][$k];
							$Data2Export[$RowCounter][] = $HWDataConsolidated[$RowSubjectID][$YearClassID][$k];
						}
						$TableRX .= "<td {$RowClass} align='center' style='border-left:{$BorderStyle}; border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$RowSubjectClassTotal."</td>";
						$Data2Export[$RowCounter][] = $RowSubjectClassTotal;
						$TableRX .= "</tr>\n";

						$IsJustStart = false;
						$RowCounter ++;
					}
				}

				//$TableRX .= "</tr>\n";
			}
				
			if ($RowCounter==0)
			{
				$TableRX .= "<tr><td colspan='".($TotalDays+3)."'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>\n";
			}
				
			$TableRX .= "<tr><td colspan='".($TotalDays+3)."'>&nbsp;</td></tr>\n";
			if ($IsExport)
			{
				$Data2Export[$RowCounter] = array("", "");
				$RowCounter ++;
			}
				
			# class summary
			if ($RowCounter>0)
			{
				$TableRX .= "<tr><td align='center' rowspan='{$ClassTotal}' style='border:{$BorderStyle};'>".$Lang['SysMgr']['Homework']['ClassTotal'] ."</td>\n";
				$Data2Export[$RowCounter][] = $Lang['SysMgr']['Homework']['ClassTotal'];
				if ($ClassTotal>0)
				{
					$IsJustStart = true;
					foreach ($ClassSubjectGroup AS $YearClassID => $YearClassObj)
					{
						if (!$IsJustStart)
						{
							$Data2Export[$RowCounter][] = "";
						}
						$RowClass = ($RowCounter%2) ? "class='tablerow2'" : "";

						$TopBorder = ($IsJustStart) ? "border-top:{$BorderStyle}" : "";
						$TableRX .= "<td align='center' {$RowClass} style='border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$YearClassObj["Title"]."</td>\n";
						$Data2Export[$RowCounter][] = $YearClassObj["Title"];
						$RowClassTotal = 0;
						for ($k=1; $k<=$TotalDays; $k++)
						{
							$CellValue = $ClassSummary[$YearClassID][$k];
							if ($CellValue=="")
							{
								$CellValue = "&nbsp;";
							}
							$TableRX .= "<td align='center' {$RowClass} style='border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$CellValue."</td>";
							$RowClassTotal += $ClassSummary[$YearClassID][$k];
							$Data2Export[$RowCounter][] = $ClassSummary[$YearClassID][$k];
						}
						$TableRX .= "<td align='center' {$RowClass} style='border-left:{$BorderStyle}; border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$RowClassTotal."</td>";
						$Data2Export[$RowCounter][] = $RowClassTotal;
						$TableRX .= "</tr>\n";

						$RowCounter ++;

						$IsJustStart = false;
					}
				}

				# daily summary
				$TableRX .= "<tr class='tablebottom'><td align='center' colspan='2' style='border:{$BorderStyle};'>".$Lang['SysMgr']['Homework']['DayTotal'] ."</td>\n";
				$Data2Export[$RowCounter][] = $Lang['SysMgr']['Homework']['DayTotal'];
				$Data2Export[$RowCounter][] = "";
				$RowMonthTotal = 0;
				$TopBorder = "border-top:{$BorderStyle}";
				for ($k=1; $k<=$TotalDays; $k++)
				{
					$TableRX .= "<td align='center' style='border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$DaySummary[$k]."</td>";
					$RowMonthTotal += $DaySummary[$k];
					$Data2Export[$RowCounter][] = $DaySummary[$k];
				}
				$TableRX .= "<td align='center' style='border-left:{$BorderStyle}; border-right:{$BorderStyle}; border-bottom:{$BorderStyle}; {$TopBorder}'>".$RowMonthTotal."</td>";
				$Data2Export[$RowCounter][] = $RowMonthTotal;
				$TableRX .= "</tr>\n";
			}
				
			$TableRX .= "</tbody>";
				
				
			$TableRX .= "</table>";
				
			if ($IsExport)
			{
				return $Data2Export;
			} else
			{
				return $TableRX;
			}
		}


		function GenerateClassStats($academicYearID, $yearTermID, $formID, $classID, $subjectID, $datefrom, $dateto, $academicYearTerm)
		{
			global $intranet_session_language;
				
			# handle given year term
			$sql_yearTermID = "";
			if ($yearTermID!="" && $yearTermID>0)
			{
				$sql_yearTermID = $yearTermID;
			} else
			{
				for ($i=0; $i<sizeof($academicYearTerm); $i++)
				{
					$sql_yearTermID .= (($sql_yearTermID!="") ? ", " : "" ) . $academicYearTerm[$i]["YearTermID"];
				}
				if ($sql_yearTermID=="")
				{
					$sql_yearTermID = 0;
				}
			}
				
			$sql_cond = "";
			$sql_cond .= ($formID!="" && $formID>0) ? " AND yc.YearID='{$formID}' " : "";
			$sql_cond .= ($classID!="" && $classID>0) ? " AND yc.YearClassID='{$classID}' " : "";
				
			# SQL1: get subject_group_id, subject_id by year_class (joining by studentIDs)
			$sql_new = "SELECT DISTINCT yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, st.SubjectGroupID, st.SubjectID " .
					" FROM " .
					"	YEAR_CLASS AS yc, YEAR_CLASS_USER AS ycu, SUBJECT_TERM_CLASS_USER AS stcu , SUBJECT_TERM AS st " .
					" WHERE" .
					"	   yc.AcademicYearID='{$academicYearID}' and st.YearTermID in ({$sql_yearTermID}) " .
					" 	and ycu.YearClassID = yc.YearClassID AND stcu.UserID=ycu.UserID and st.SubjectGroupID=stcu.SubjectGroupID " .
					" {$sql_cond} " .
					" ORDER BY yc.Sequence, yc.ClassTitleEN  ";
			//debug($sql_new);
			$ClassSubjectGroupRaw = $this->returnResultSet($sql_new);
			//debug_r($ClassSubjectGroupRaw);
				
			# consolidate the data
			$ClassSubjectGroup = array();
			for ($i=0; $i<sizeof($ClassSubjectGroupRaw); $i++)
			{
				$ObjRow = $ClassSubjectGroupRaw[$i];

				$ClassSubjectGroup[$ObjRow["YearClassID"]]["Title"] = ($intranet_session_language=="en") ? $ObjRow["ClassTitleEN"] : $ObjRow["ClassTitleB5"];
				if ($subjectID!="" && $subjectID>0 && $subjectID!=$ObjRow["SubjectID"])
				{
					//debug($subjectID, $ObjRow["SubjectID"]);
					continue;
				} else
				{
					$ClassSubjectGroup[$ObjRow["YearClassID"]]["SubjectGroups"][] = $ObjRow["SubjectGroupID"];
					$ClassSubjectGroup[$ObjRow["YearClassID"]]["SubjectIDs"][] = $ObjRow["SubjectID"];
				}
			}
				
			//debug_r($ClassSubjectGroup);
				
			# SQL2: get studentID (SID) by year_class (joining by studentIDs)
			$sql_new = "SELECT DISTINCT yc.YearClassID, yc.ClassTitleEN, ycu.UserID " .
					" FROM " .
					"	YEAR_CLASS AS yc, YEAR_CLASS_USER AS ycu " .
					" WHERE" .
					"	   yc.AcademicYearID='{$academicYearID}' AND ycu.YearClassID=yc.YearClassID " .
					" ORDER BY yc.Sequence, yc.ClassTitleEN  ";
			//debug($sql_new);
			$ClassStudentRaw = $this->returnResultSet($sql_new);
			//debug_r($ClassStudentRaw);
				
			# consolidate the data
			$ClassStudent = array();
			for ($i=0; $i<sizeof($ClassStudentRaw); $i++)
			{
				$ObjRow = $ClassStudentRaw[$i];
				$ClassStudent[$ObjRow["YearClassID"]][] = $ObjRow["UserID"];
			}
				
			//debug_r($ClassStudent);
				
				
			# create temp table
			$sql_temp = "CREATE TEMPORARY TABLE TEMP_HW_CLASS_STATS (
				ClassName VARCHAR(64),
				TotalAmount int(4),
				TotalSubject int(4),
	    		TotalLoading float,
	    		TotalLate int(4),
	    		TotalNoSubmission int(4) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
			$this->db_db_query($sql_temp);
				
				
			# looping: count number of hw, workload, handin (check SID), late, no submission
			if (sizeof($ClassSubjectGroup)>0)
			{

				$HandinStatus[-1] = "SubmissionNO";
				$HandinStatus[2] = "SubmissionLATE";
				$HandinStatus[1] = "SubmissionYES";

				foreach ($ClassSubjectGroup AS $YearClassID => $RowObj)
				{
					$subject_groups = 0;
					if (sizeof($RowObj["SubjectGroups"])>0 && is_array($RowObj["SubjectGroups"]))
					{
						$subject_groups = implode(", ", $RowObj["SubjectGroups"]);
					}
					//debug($subject_groups);
					$sql_class = "SELECT DISTINCT ih.HomeworkID, ih.Loading, ih.Title, ih.HandinRequired, ih.SubjectID  " .
							" FROM " .
							"   INTRANET_HOMEWORK As ih " .
							" WHERE" .
							" 	unix_timestamp(ih.DueDate) >= unix_timestamp('$datefrom') AND unix_timestamp(ih.DueDate) <= unix_timestamp('$dateto')" .
							"   AND ih.AcademicYearID = {$academicYearID} AND ih.YearTermID IN ({$sql_yearTermID}) " .
							"   AND ih.ClassGroupID IN ({$subject_groups}) ";
					$hw_raws = $this->returnResultSet($sql_class);
					//debug_r($hw_raws);
						
					$HW = array();
					$HW["ClassName"] = $RowObj["Title"];
					$HW["Total"] = sizeof($hw_raws);
					$HW["Loading"] = 0;
						
					# Count records - 1
					$HW_IDs = array();
					for ($j=0; $j<sizeof($hw_raws); $j++)
					{
						$HW["Loading"] += $hw_raws[$j]["Loading"];
						if ($hw_raws[$j]["HandinRequired"]==1)
						{
							$HW["HomeworkIDs"][] = $hw_raws[$j]["HomeworkID"];
						}
						$HW["Subjects"][$hw_raws[$j]["SubjectID"]] = 1;
					}
					//debug_r($HW);
					if (sizeof($HW["HomeworkIDs"])>0)
					{
						$class_students = implode(", ", $ClassStudent[$YearClassID]);
						if ($class_students=="")
						{
							$class_students = 0;
						}

						$class_homeworks = implode(", ", $HW["HomeworkIDs"]);
						if ($class_homeworks=="")
						{
							$class_homeworks = 0;
						}

						$sql_class = "SELECT ihh.RecordStatus, Count(ihh.RecordID) AS RecordTotal  " .
								" FROM " .
								"   INTRANET_HOMEWORK_HANDIN_LIST As ihh " .
								" WHERE" .
								"   ihh.HomeworkID in ({$class_homeworks}) AND ihh.StudentID IN ({$class_students}) " .
								" GROUP BY" .
								"	ihh.RecordStatus ";
						$handin_raws = $this->returnResultSet($sql_class);
						//debug($sql_class);
						//debug_r($handin_raws);
						for ($j=0; $j<sizeof($handin_raws); $j++)
						{
							$HW[$HandinStatus[$handin_raws[$j]["RecordStatus"]]] = $handin_raws[$j]["RecordTotal"];
						}

						# -1 for no; 2 for late
					}
						
					# insert to temp table
					$sql_data[] = "('".addslashes($HW["ClassName"])."', '".$HW["Total"]."', '".sizeof($HW["Subjects"])."', '".$HW["Loading"]."', '".$HW["SubmissionLATE"]."', '".$HW["SubmissionNO"]."') ";
					//debug_r($HW);
				}
			}
				
			if (sizeof($sql_data)>0)
			{
				$sql= "INSERT INTO TEMP_HW_CLASS_STATS (ClassName, TotalAmount, TotalSubject, TotalLoading, TotalLate, TotalNoSubmission) VALUES ". implode(",", $sql_data);
				$this->db_db_query($sql);
			}
		}

		function getHomeworkListTabs($self, $query_str = "") {
			global $Lang;
			global $sys_custom;
				
			$TAGS_OBJ = array();
			$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?" . $query_str, ($self == "index.php" || $self == "add.php") ? 1 : 0);
			$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?" . $query_str, ($self == "history.php" || $self == "edit.php") ? 1 : 0);
				
			if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && $_SESSION['UserType'] == USERTYPE_STAFF) {
				$allowSupplyButton = false;
				if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
					if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
						$allowSupplyButton = true;
					}
				} else {
					// if(!$this->ViewOnly && $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] && !$_SESSION['isTeaching']) {
					if(!$this->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
						$allowSupplyButton = true;
					}
				}
				if ($allowSupplyButton) {
/********************************************************************/
					if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
						$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SubmittedInput'], "hw_submitted_input.php?" . $query_str, ($self == "hw_submitted_input.php") ? 1 : 0);
					}
					$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['NotSubmittedInput'], "notsubmitted.php?" . $query_str, ($self == "notsubmitted.php") ? 1 : 0);
					$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SupplementaryRecord'], "sup_records.php?" . $query_str, ($self == "sup_records.php") ? 1 : 0);
					if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] || $sys_custom['eHomework_manual_transfer_to_eDis']) {
						$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['DataTransferringForDis'], "hw_confirm.php?" . $query_str, ($self == "hw_confirm.php") ? 1 : 0);
					}
/********************************************************************/
				}
			}
			if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$sys_custom['eHomework']['HideClearHomeworkRecords'] ) {
				$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", ($self == "clear_homework.php") ? 1 : 0);
			}
			return $TAGS_OBJ;
		}

		function getAllHandInStatus($enableOnly = false, $init = true) {
			global $Lang;
			global $sys_custom;
				
			$statusArr = array();
			$sql = "SELECT HandInStatusID, HandInStatusTitleEN, HandInStatusTitleB5, HandInStatusValue, statusType, HandInStatusLabel, isDisabled FROM INTRANET_HOMEWORK_HANDIN_STATUS";
			$where_sql = "";
			if ($enableOnly) {
				$where_sql .= " isDisabled != '1'";
			}
				
			$deftotal = 12;
			if (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) {
				if (!empty($where_sql)) {
					$where_sql .= " AND ";
				}
				$where_sql .= " HandInStatusValue IN (-1,1,2,4,5,6) ";
				$deftotal = 6;
			} else if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
				if (!empty($where_sql)) {
					$where_sql .= " AND ";
				}
				$where_sql .= " HandInStatusValue NOT IN (-11, -12, -13) ";
			} else {
				if (!empty($where_sql)) {
					$where_sql .= " AND ";
				}
				$where_sql .= " HandInStatusValue NOT IN (-30) ";
			}
			$AllResult = $this->returnResultSet($sql);
			if (!empty($where_sql)) {
				$sql .= " WHERE " . $where_sql;
			}
			$sql .= " ORDER BY pripority ASC";
			$result = $this->returnResultSet($sql);
				
			if (count($result) > 0 && count($AllResult) ==  12) {
				foreach ($result as $kk => $vv) {
					$statusArr[$vv["HandInStatusValue"]] = $vv;
				}
			} else {
				$this->tableDataInit();
				if ($init) {
					$statusArr = $this->getAllHandInStatus($enableOnly, false);
				}
			}
			return $statusArr;
		}

		function tableDataInit() {
			global $sys_custom;
			$sqls = array();
			$i = 0;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'Submitted', "HandInStatusTitleEN" => 'Submitted', "HandInStatusTitleB5" => '已交',
					"HandInStatusValue" => "1", "statusType" => 'main', "pripority" => ($i+1),
					"isDisabled" => "0"
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'notSubmitStr', "HandInStatusTitleEN" => 'Without submission after due date', "HandInStatusTitleB5" => '逾期未交',
					"HandInStatusValue" => "-1", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => "0"
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'LateSubmitted', "HandInStatusTitleEN" => 'Late Submitted', "HandInStatusTitleB5" => '遲交',
					"HandInStatusValue" => "2", "statusType" => 'main', "pripority" => ($i+1),
					"isDisabled" => "0"
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'NoNeedSubmit', "HandInStatusTitleEN" => 'No Need Submit', "HandInStatusTitleB5" => '不需繳交',
					"HandInStatusValue" => "4", "statusType" => 'main', "pripority" => ($i+1),
					"isDisabled" => "0"
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'UnderProcessing', "HandInStatusTitleEN" => 'Under Processing', "HandInStatusTitleB5" => '未處理',
					"HandInStatusValue" => "5", "statusType" => 'display', "pripority" => ($i+1),
					"isDisabled" => "0"
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'Supplementary', "HandInStatusTitleEN" => 'Supplementary', "HandInStatusTitleB5" => '補交',
					"HandInStatusValue" => "6", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => "0"
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'SupplementarySubmitted', "HandInStatusTitleEN" => 'Supplementary with Submission', "HandInStatusTitleB5" => '已補交',
					"HandInStatusValue" => "7", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) ? 1 : 0
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'SuppNotSubmitted', "HandInStatusTitleEN" => 'Supplementary without Submission', "HandInStatusTitleB5" => '欠補交',
					"HandInStatusValue" => "-11", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] || $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) ? 1 : 0
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'DueDateAbsence', "HandInStatusTitleEN" => 'Absent in due date', "HandInStatusTitleB5" => '當天缺席',
					"HandInStatusValue" => "-12", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) ? 1 : 0
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'ABSHomeworkReturn', "HandInStatusTitleEN" => 'Absent in issue homework date', "HandInStatusTitleB5" => '派功課日缺席',
					"HandInStatusValue" => "-13", "statusType" => 'main', "pripority" => ($i+1),
					"isDisabled" => (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) ? 1 : 0
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'NotSubmittedRecord', "HandInStatusTitleEN" => 'Mark as not submitted', "HandInStatusTitleB5" => '記欠交',
					"HandInStatusValue" => "-20", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) ? 1 : 0
			);
			$i++;
			$param[$i] = array(
					"HandInStatusID" => ($i+1), "HandInStatusLabel" => 'NoSubmissionLabel', "HandInStatusTitleEN" => 'No submission', "HandInStatusTitleB5" => '沒有補交',
					"HandInStatusValue" => "-30", "statusType" => 'supplementary', "pripority" => ($i+1),
					"isDisabled" => (!$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']) ? 1 : 0
			);
			$i++;
				
			$sprint_str = "INSERT INTO INTRANET_HOMEWORK_HANDIN_STATUS (HandInStatusID, HandInStatusLabel, HandInStatusTitleEN, HandInStatusTitleB5, HandInStatusValue, statusType, pripority, isDisabled, DateInput, InputBy, LastModified, ModifiedBy) VALUES (";
			$sprint_str .= "'%d', '%s', '%s', '%s', '%s', '%s', %d, %d, NOW(), " . $_SESSION["UserID"] . ", NOW(), " . $_SESSION["UserID"] . ");";
			if (count($param) > 0) {

				$sql = "TRUNCATE TABLE INTRANET_HOMEWORK_HANDIN_STATUS";
				$this->db_db_query($sql);

				foreach ($param as $kk => $vv) {
					$sql = vsprintf($sprint_str, $vv);
					$this->db_db_query($sql);
				}
			}
			unset($param);
		}

		function setAllHandInStatus($updateArr) {
			if (count($updateArr) > 0) {
				foreach ($updateArr as $kk => $vv) {
					$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_STATUS SET isDisabled='" . $vv . "', LastModified=NOW(), ModifiedBy='" . $_SESSION["UserID"] . "' WHERE HandInStatusID='" . $kk . "'";
					$this->db_db_query($sql);
				}
				unset($updateArr);
			}
		}

		/**
		 * Modified Information
		 * 	-	2017-11-22 Frankie : modified getHomeListByArgs(), getStudentsByParam() for retrieve Homework with All Terms of Current Year
		 * @param array $param
		 * @return array
		 */
		function getStudentsByParam($param) {
			global $sys_custom;
			/*******************/
			/* init result type
			/*******************/
			$sqlType = "";
			
			if (!isset($param["sqlType"]) || empty($param["sqlType"]))
			{
				switch (strtolower($param["callBy"]))
				{
					case "notsubmitted.php":
						$sqlType = "forNotSubmitInput";
						break;
					case "sup_records.php":
						$sqlType = "forSupplementaryInput";
						break;
					case "hw_confirm.php":
						$sqlType = "forDataTransfer";
						break;
					case "hw_violation.php":
					case "cancel_hwviolation.php":
					case "export_hwsp.php":
					case "print_hwsp_preview.php":
						$sqlType = "forReport";
						break;
				}
			}
			else
			{
				$sqlType = $param["sqlType"];
			}
			/*******************/
			if (isset($param["selectedDate"]))
			{
				$selected_date = $param["selectedDate"];
			}
			else
			{
				$selected_date = date("Y-m-d");
				/* for Dev
				 * $selected_date = date("Y-m-d", strtotime(date("Y-m-d"). " +1 days"));
				 */
			}
			
			include_once("libclass.php");
			
			/* If allow to get Year by Date, some page can not coordinate with Year ID, so please don't turn on it */
			$allowGetYearByDate = false;
			if ($allowGetYearByDate)
			{
				$reqGetAcademicYear = false;
				if (isset($param["startDate"]) && isset($param["endDate"]))
				{
					$AcademicYearAndYearTerm = getAcademicYearAndYearTermByDate($param["startDate"]);
					$searchYearTermID = $AcademicYearAndYearTerm["YearTermID"];
					$reqGetAcademicYear = true;
				}
				else
				{
					if (isset($param["isReport"]) && $param["isReport"])
					{
						$AcademicYearAndYearTerm = getAcademicYearAndYearTermByDate($selected_date);
						$searchYearTermID = $AcademicYearAndYearTerm["YearTermID"];
						$reqGetAcademicYear = true;
					}
					else
					{
						$TermInfoArr = getCurrentAcademicYearAndYearTerm();
						$searchYearTermID = $TermInfoArr['YearTermID'];
					}
				}
				
				$objTerm = new academic_year_term($searchYearTermID, false);
				$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				// $termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] &&
				    !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
				{
					# for Buddhist Wong Wan Tin College
					$termStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($objTerm->AcademicYearID)));
				}
				else
				{
					$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				}
				if ($reqGetAcademicYear)
				{
					$param["yearID"] = $objTerm->AcademicYearID;
					$param["yearTermID"] = $objTerm->YearTermID;
				}
				
			}
			else
			{
				/*
		 		$TermInfoArr = getCurrentAcademicYearAndYearTerm();
				$searchYearTermID = $TermInfoArr['YearTermID'];
				$objTerm = new academic_year_term($searchYearTermID, false);

				// $termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
				{
					# for Buddhist Wong Wan Tin College
					$termStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($objTerm->AcademicYearID)));
				}
				else 
				{
					$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				}
				*/
				if (isset($param["startDate"]) && isset($param["endDate"]) &&
				    $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] &&
				    $sqlType == "forReport")
				{
					$AcademicYearAndYearTerm = getAcademicYearAndYearTermByDate($param["startDate"]);
					$searchYearTermID = $AcademicYearAndYearTerm["YearTermID"];
					$reqGetAcademicYear = true;
					$termStart = $param["startDate"];
					$termEnd = $param["termEnd"];
				}
				else
				{
					$TermInfoArr = getCurrentAcademicYearAndYearTerm();
					$searchYearTermID = $TermInfoArr['YearTermID'];
					$objTerm = new academic_year_term($searchYearTermID, false);
					// $termStart = date("Y-m-d", strtotime($objTerm->TermStart));
					if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] &&
					    !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
					{
						# for Buddhist Wong Wan Tin College
						$termStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($objTerm->AcademicYearID)));
					}
					else 
					{
						$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
					}
				}
			}
			/***************************************************************/
			$sql_param = "SELECT IU.UserID, UserLogin, " . Get_Lang_Selection('ChineseName', 'EnglishName') . " AS StudentName, IU.RecordStatus as StudentStatus, EnglishName as enStudentName, ycu.ClassNumber ";
			$sql_param .= ", ihhl.StudentID, y.WEBSAMSCode, ih.HomeworkID, ih.Title, ih.Description, ihhl.RecordStatus, ih.StartDate, ih.DueDate";
			$sql_param .= ", yc.YearClassID, " . Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN') . " AS ClassTitle, yc.ClassTitleEN as enClassTitle, " . Get_Lang_Selection('CH_DES', 'EN_DES') . " AS Subject, EN_DES as enSubject, HandinRequired, ihhl.RecordStatus, ih.ClassGroupID, ih.SubjectID, ihhl.RecordID, ihhl.DateModified, ihhl.DateInput ";
			$sql = " FROM INTRANET_HOMEWORK AS ih ";
			if ($sqlType != "forNotSubmitInput")
			{
				$sql .= " JOIN INTRANET_HOMEWORK_HANDIN_LIST AS ihhl ON (ih.HomeworkID=ihhl.HomeworkID AND ihhl.RecordStatus NOT IN (1) )";
			}
			else
			{
				$sql .= " JOIN INTRANET_HOMEWORK_HANDIN_LIST AS ihhl ON (ih.HomeworkID=ihhl.HomeworkID)";
			}
			// $sql .= " JOIN INTRANET_HOMEWORK_HANDIN_LIST AS ihhl ON (ih.HomeworkID=ihhl.HomeworkID)";
			$sql .= " JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID=ihhl.StudentID)";
			if (isset($param["classID"]) && !empty($param["classID"]))
			{
				$sql .= " JOIN YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=" . $param["yearID"] . " AND yc.YearClassID='" . $param["classID"] . "')";
			}
			else
			{
				$sql .= " JOIN YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=" . $param["yearID"] . ")";
			}
			$sql .= " JOIN YEAR AS y ON (y.YearID=yc.YearID) ";
			$sql .= " JOIN INTRANET_USER AS IU ON (IU.UserID=ihhl.StudentID)";
			if (isset($param["subjectID"]) && !empty($param["subjectID"]) && $param["subjectID"] != "-1")
			{
				$sql .= " JOIN ASSESSMENT_SUBJECT AS b ON (b.RecordID = ih.SubjectID AND b.RecordID='" . $param["subjectID"]. "')";
			}
			else
			{
				$sql .= " JOIN ASSESSMENT_SUBJECT AS b ON (b.RecordID = ih.SubjectID)";
			}
			/***************************************************************/
			$sql_date = "";
			if (!empty($param["startDate"]) && !empty($param["endDate"]))
			{
				$sql_date = "ih.DueDate BETWEEN '" . $param["startDate"] . "' AND '" . $param["endDate"] . "'";
			}
			$result = array();
			switch ($sqlType)
			{
				case "forNotSubmitInput":
					$sql_param .= ", ihhl.SuppRecordStatus, ihhl.SuppRecordDate, ihhl.HWViolationID, ihhl.HWViolationDate, ihhl.CancelViolationStatus, ihhl.CancelViolationDate";
					$sql .= " WHERE ih.HandinRequired='1'";
					if (!empty($sql_date))
					{
						$sql .= " AND " . $sql_date;
					}
					$sql .= " AND ih.HomeworkID='" . $param["homeworkID"] . "' AND ih.AcademicYearID='" . $param["yearID"] . "'";
					break;
				case "forSupplementaryInput" :
				case "forDataTransfer":
				case "forDataTransferBatchOldDataUpdate":
					if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
					{
						$defaultSupplement = array(-1, 2, 6, -11, -20, -9999);
					}
					else
					{
						$defaultSupplement = array(-1, 2, 6, -11, -12, -13, -20, -9999);
					}
					$sql_param .= ", ihhl.SuppRecordStatus, ihhls.SuppRecordStatus as DateSuppRecordStatus";
					$sql_param .= ", ihhl.SuppRecordDate, ihhls.RecordDate, ihhl.HWViolationID, ihhl.HWViolationDate, ihhl.CancelViolationStatus, ihhl.CancelViolationDate";
					/*****************************************/
					$sql .= " LEFT JOIN INTRANET_HOMEWORK_HANDIN_LIST_SUPP AS ihhls ON ( ";
					$sql .= " ihhl.RecordID=ihhls.HW_RecordID";
					$sql .= " AND ihhls.RecordDate LIKE '" . $selected_date. "%' )";
					/*****************************************/
					if ($sqlType == "forSupplementaryInput" && !empty($param["searchTxt"]) )
					{
						$sql .= " LEFT JOIN DISCIPLINE_ACCU_HW_GROUPED_MAPPING AS dajwgm ON (ihhl.RecordID=dajwgm.HomeworkID)";
					}
					$sql .= " WHERE ih.HandinRequired='1'";
					if ($sqlType == "forSupplementaryInput" && !empty($param["searchTxt"]) )
					{
						$sql .= " AND (";
						$sql .= " ChineseName like '%" . $param["searchTxt"] . "%'";
						$sql .= " OR EnglishName like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR CONCAT(ClassTitleB5, '(',  ycu.ClassNumber, ')') like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR CONCAT(ClassTitleEN, '(',  ycu.ClassNumber, ')') like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR ih.DueDate like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR ihhl.CancelViolationDate like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR ih.Title like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR dajwgm.DateInput like '%" . $param["searchTxt"] . "%' ";
						$sql .= " OR ih.Description like '%" . $param["searchTxt"] . "%' ";
						$sql .= " ) ";
					}
					$sql .= " AND ihhl.RecordStatus NOT IN (1, 4, 5) ";
					if (isset($param["HandInStatusID"]) && !empty($param["HandInStatusID"]))
					{
						$sql .= " AND ( ihhl.RecordStatus IN (" . $param["HandInStatusID"] . ") OR ihhl.SuppRecordStatus IN (" . $param["HandInStatusID"] . ") )";
					}
					if ($sqlType != "forDataTransferBatchOldDataUpdate")
					{
						$sql .= " AND (";
						/*** Get Data when SuppRecordStatus is not passing to Discipline ***/ 
						$sql .= " ( ( ihhl.SuppRecordStatus IN (" . implode(", ", $defaultSupplement) . ") OR ihhl.SuppRecordStatus IS NULL OR ihhl.SuppRecordStatus='0' ) ";
						$sql .= " AND ( CancelViolationDate IS NULL OR CancelViolationDate LIKE '0000-00-00%' ) ";
						$sql .= " AND ";
						$sql .= " ( ";
						$sql .= " ihhl.SuppRecordDate IS NULL OR ihhl.SuppRecordDate LIKE '0000-00-00%' OR ihhl.SuppRecordDate < '" . date("Y-m-d", strtotime($selected_date. " +1 days")) . "'";
						$sql .= " ) ) ";
						$sql .= " OR ";
						/*** Get Data when SuppRecordStatus is passing to Discipline (Today) includes 3 Status ***/
						$sql .= " ( ";
						$sql .= " ihhl.SuppRecordStatus IN (7, -20, -30) AND ihhl.SuppRecordDate LIKE '" . $selected_date . "%'";
						$sql .= " ) ";
						$sql .= " )";
					}
					$sql .= " AND ih.AcademicYearID='" . $param["yearID"] . "'";
					if (!empty($sql_date))
					{
						$sql .= " AND " . $sql_date;
					}
					if ($sqlType == "forDataTransfer")
					{
						$sql .= " AND ( ";
						if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
						{
							$sql .= "ihhl.HWViolationID IS NULL AND ihhl.SuppRecordStatus IN (-20) AND ( ihhl.DateModified > ihhl.HWViolationDate OR ihhl.HWViolationDate IS NULL OR ihhl.HWViolationDate LIKE '0000-00-00%' ) ";
							$sql .= " OR ";
							$sql .= "ihhl.SuppRecordStatus IN (7) AND ( ihhl.DateModified > ihhl.HWViolationDate OR ihhl.HWViolationDate IS NULL OR ihhl.HWViolationDate LIKE '0000-00-00%' ) ";
							$sql .= " ) ";
						}
						else
						{
							/*********************************************/
							/* Empty Supplementary status to detention
							 /*********************************************/
							$sql .= " ihhl.SuppRecordDate IS NULL";
							$sql .= " OR ihhl.SuppRecordDate LIKE '0000-00-00%'";
							$sql .= " OR (ihhl.SuppRecordStatus NOT IN (7, -9999))";
							/*********************************************/
							/* Must "Supplementary without Submission" to detention
							 /* $sql .= " ihhl.SuppRecordStatus NOT IN (7, -9999) AND ihhl.SuppRecordStatus IS NOT NULL";
							 /*********************************************/
							$sql .= " ) AND ( ";
							$sql .= " ihhl.HWViolationID IS NULL OR ihhl.HWViolationID = '0' OR ihhl.HWViolationDate < ihhl.SuppRecordDate";
							$sql .= " ) AND ihhl.SuppRecordStatus IS NOT NULL";
							$sql .= " AND ihhl.SuppRecordStatus != '0' ";
						}
						/*************************************************************************************************/
						if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
						{
							$sql .= " AND ihhl.SuppRecordStatus NOT IN (-11)";
						}
						/*************************************************************************************************/
					}
					else if ($sqlType == "forDataTransferBatchOldDataUpdate")
					{
						/**** Get all Status (-1) ***********/
						$sql .= " AND ( ";
						if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
						{
							$sql .= "ihhl.HWViolationID IS NULL AND ihhl.SuppRecordStatus IN (-20) AND ( ihhl.DateModified > ihhl.HWViolationDate OR ihhl.HWViolationDate IS NULL OR ihhl.HWViolationDate LIKE '0000-00-00%' ) ";
							$sql .= " OR ";
							$sql .= "ihhl.SuppRecordStatus IN (7) AND ( ihhl.DateModified > ihhl.HWViolationDate OR ihhl.HWViolationDate IS NULL OR ihhl.HWViolationDate LIKE '0000-00-00%' ) ";
							$sql .= " ) ";
						}
						else
						{
							// $sql .= " ihhl.RecordStatus = '-1' AND ih.DueDate >= '" . $param["batchDueDate"] . "'";
							// Batch 2
							$sql .= " ihhl.RecordStatus = '-1' AND ih.DueDate >= '" . $param["batchDueDate"] . "'";
							$sql .= " ) ";
						}
						/*************************************************************************************************/
					}
					break;
				case "forReport":
					if (!$param["cancelOnly"])
					{
						$sql .= " JOIN DISCIPLINE_ACCU_RECORD AS dae ON (dae.StudentHomeworkID = ih.HomeworkID AND dae.AcademicYearID=ih.AcademicYearID AND dae.StudentID=ihhl.StudentID) ";
					}
					if (!$param["cancelOnly"] && $param["dataopt"] == "confirm_date")
					{
						/***************************************************************************/
						/* find Homework's Misconduct Record by Date (exclude Misconduct */
						/***************************************************************************/
						$HWVRecordID = array();
						$sub_strSQL = " SELECT HWVRecordID, StudentID, RecordID, HomeworkID, SuppRecordStatus 
									FROM INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST
									WHERE SuppRecordStatus IN (-1, 7, -20) AND SuppRecordDate LIKE '" . $param["selectedDate"] . "%' AND RecordType LIKE 'VIOLATION'";
						$sub_result = $this->returnResultSet($sub_strSQL);
						if (count($sub_result) > 0)
						{
							$HWVRecordID = BuildMultiKeyAssoc($sub_result, "HWVRecordID");
							foreach($HWVRecordID as $kk => $vv)
							{
								if ($vv["SuppRecordStatus"] != "-20")
								{
									$sub_strSQL = " SELECT HWVRecordID, StudentID, RecordID, HomeworkID
										FROM INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST
										WHERE
											StudentID='" . $vv["StudentID"] . "' AND RecordID='" . $vv["RecordID"] . "' AND HomeworkID like '" . $vv["HomeworkID"] . "' AND
											SuppRecordStatus IN (-20) AND SuppRecordDate NOT LIKE '" . $param["selectedDate"] . "%' AND RecordType LIKE 'VIOLATION'";
									
									$sub_result = $this->returnResultSet($sub_strSQL);
									if (count($sub_result) > 0)
									{
										unset($HWVRecordID[$kk]);
									}
								}
							}
						}
						/***************************************************************************/
						$sql_param .= ", ihhl.SuppRecordStatus, ihhvh.SuppRecordDate";
						$sql .= " JOIN INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST AS ihhvh ON (";
						$sql .= " ihhvh.RecordID = ihhl.RecordID AND ihhvh.SuppRecordStatus IN (-1, 7, -20) AND ihhvh.RecordType LIKE 'VIOLATION'";
						$sql .= " AND ihhvh.SuppRecordDate LIKE '" . $param["selectedDate"] . "%'";
						$sql .= " AND DATE(ihhvh.SuppRecordDate) = DATE(dae.RecordDate)";
						/***************************************************************************/
						if (count($HWVRecordID) > 0)
						{
							$sql .= " AND ihhvh.HWVRecordID IN ('" . implode("','", array_keys($HWVRecordID)) . "')";
						}
						/***************************************************************************/
						$sql .= ")";
					}
					else
					{
						$sql_param .= ", ihhl.SuppRecordStatus, ihhl.SuppRecordDate";
					}
					$sql .= " WHERE ih.HandinRequired='1'";
					$sql .= " AND ihhl.RecordStatus IN (-1, 7, -20) ";
					if (isset($param["HandInStatusID"]) && !empty($param["HandInStatusID"]))
					{
						// $sql .= " AND ( ihhl.RecordStatus IN (" . $param["HandInStatusID"] . ") OR ihhl.SuppRecordStatus IN (" . $param["HandInStatusID"] . ") )";
					}
					if ($param["cancelOnly"])
					{
						$sql_param .= ", ihhl.SuppRecordStatus, ihhl.SuppRecordDate, ihhl.CancelViolationDate ";
						$sql .= " AND (ihhl.CancelViolationDate IS NOT NULL AND ihhl.CancelViolationDate NOT like '0000-00-00%' OR ihhl.CancelViolationDate > ihhl.SuppRecordDate)";
					}
					else
					{
						$sql .= " AND ( ihhl.CancelViolationDate IS NULL OR ihhl.CancelViolationDate like '0000-00-00%' OR ihhl.CancelViolationDate < ihhl.SuppRecordDate )";
					}
					if ($param["dataopt"] == "confirm_date")
					{
						if ($param["cancelOnly"])
						{
							$sql .= " AND ihhl.CancelViolationDate like '" . $param["selectedDate"] . "%'";
						}
						else
						{
							$sql .= " AND ihhvh.SuppRecordDate like '" . $param["selectedDate"] . "%'";
						}
					}
					else
					{
						/*
						if ($param["cancelOnly"]) {
							// $sql .= " AND ihhl.CancelViolationDate Between '" . $param["startDate"] . "' AND '" . date("Y-m-d", strtotime($param["endDate"] . " +1 days")) . "'";
							$sql .= " AND ihhl.CancelViolationDate Between '" . $param["startDate"] . "' AND '" . $param["endDate"] . "'";
						} else {
							// $sql .= " AND ih.DueDate Between '" . $param["startDate"] . "' AND '" . date("Y-m-d", strtotime($param["endDate"] . " +1 days")) . "'";
							$sql .= " AND ih.DueDate Between '" . $param["startDate"] . "' AND '" . $param["endDate"] . "'";
						}*/
						$sql .= " AND ih.DueDate Between '" . $param["startDate"] . "' AND '" . $param["endDate"] . "'";
					}
					break;
			}
			/***************************************************************/
			if ($sqlType == "forReport")
			{
				if (!$param["cancelOnly"] && $param["dataopt"] != "confirm_date")
				{
					// $sql .= " AND ih.DueDate >= '" . $termStart . "' AND ih.DueDate <= '" . date("Y-m-d") . "'";
				}
			}
			else
			{
				if (!$param["cancelOnly"])
				{
					$sql .= " AND ih.DueDate >= '" . $termStart . "' AND ih.DueDate <= '" . date("Y-m-d") . "'";
				}
			}
			/***************************************************************/
			
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $_SESSION["UserID"];
			$subject = $this->getTeachingSubjectList($UserIDTmp, $param["yearID"], "", $param["classID"]);
			$sbjGps = $this->getTeachingSubjectGroupList($UserIDTmp,"", $param["yearID"], "", $param["classID"]);
			if(sizeof($sbjGps)>0)
			{
				for($i=0; $i<sizeof($sbjGps); $i++)
				{
					$temp[$i] = $sbjGps[$i][0];
				}
				$sql .= " AND ih.ClassGroupID IN (".implode(',',$temp).") ";
			}
			if ($sqlType == "forReport" && !$param["cancelOnly"])
			{
				$sql .= " GROUP BY ihhl.StudentID, ih.HomeworkID, yc.YearClassID, ih.ClassGroupID, ih.SubjectID, ihhl.RecordID ";
			}
			switch ($param["custOrderBy"])
			{
				case "studentID":
					$sql .= " ORDER BY ClassTitle ASC, ycu.ClassNumber ASC, StudentName ASC, ih.DueDate ASC, ih.StartDate ASC, ih.HomeworkID ASC ";
					break;
				default:
					if ($sqlType == "forSupplementaryInput" && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
					{
						$sql .= " ORDER BY ClassTitleEN ASC, ycu.ClassNumber ASC, ih.DueDate ASC, ih.StartDate ASC, ih.HomeworkID ASC";
					}
					else
					{
						$sql .= " ORDER BY ih.DueDate ASC, ih.StartDate ASC, ih.HomeworkID ASC, ClassTitleEN ASC, ycu.ClassNumber ASC ";
					}
					break;
			}
			$sql = $sql_param . $sql;
			/***************************************************************/
			if (in_array(getRemoteIpAddress(), array('10.0.3.133'))) {
				/* DEV ONLY for retrieve SQL into Browser * /
				$reqDebug = array( " FROM " => "<br>FROM ", " WHERE " => "<br><br>WHERE<br><br>", " JOIN " => "<br>JOIN<br>&nbsp;&nbsp;&nbsp;", " ORDER " => "<br><br>ORDER ", " AND " => "<br>&nbsp;&nbsp;&nbsp;AND " );
				$debug_SQL = str_replace(array_keys($reqDebug), array_values($reqDebug), $sql);
				echo $sqlType . "<hr>";
				echo $debug_SQL;
				echo "<hr>";
				if ($sqlType == "forDataTransfer") {
                    exit;
				}
				/***************************************************************/
			}
			/***************************************************************/
			$result = $this->returnResultSet($sql);
			return $result;
		}

		function getHandleInStatus($args = array()) {
			/*
			 $args = array(
			 "attr" => "name=\"HandInStatusID\" onChange=\"reloadForm()\"",
			 "dataArr" => $handInStatusArr,
			 "dataType" => "Supplementary",
			 "selectedID" => $HandInStatusID,
			 "firstLabel" => $Lang['SysMgr']['Homework']['PleaseSelect'],
			 "firstVal" => ""
			 );
			 */
			$selectHTML = "";
			$allowStatus = array();
			if (isset($args["dataArr"]) && count($args["dataArr"]) > 0) {
				switch ($args["dataType"]) {
					case "Supplementary":
						$allowStatus = array(7, -1, -11, -12, -13, -20);
						break;
				}
				foreach ($args["dataArr"] as $kk => $vv) {
					if ($vv["isDisabled"] != 1) {
						if (count($allowStatus) > 0) {
							if (in_array($kk, $allowStatus)) {
								$handInStatus[$kk] = $vv;
							}
						} else {
							$handInStatus[$kk] = $vv;
						}
					}
				}

				$selectHTML = "<SELECT " . $args["attr"] . ">\n";
				$empty_selected = (!isset($args["selectedID"]) || $args["selectedID"] == '')? "SELECTED":"";
				if (isset($args["firstLabel"]) && !empty($args["firstLabel"])) {
					$selectHTML .= "<OPTION value='". ((isset($args["firstVal"]))? $args["firstVal"] : -1) ."' " . $empty_selected . ">" . $args["firstLabel"] . "</OPTION>\n";
				}
				foreach ($handInStatus as $kk => $vv)
				{
					$id = $vv["HandInStatusValue"];
					$name = Get_Lang_Selection($vv["HandInStatusTitleB5"], $vv["HandInStatusTitleEN"]);
					$selected_str = ($id==$args["selectedID"]? "SELECTED":"");
					$selectHTML .= "<OPTION value='" . $id . "' " . $selected_str . ">" . $name . "</OPTION>\n";
				}
				$selectHTML .= "</SELECT>\n";
			}
			return $selectHTML;
		}

		
		/**
		 * Modified Information
		 * 	-	2017-11-22 Frankie : modified getHomeListByArgs(), getStudentsByParam() for retrieve Homework with All Terms of Current Year
		 * @param array $param
		 * @return array
		 */
		function getHomeListByArgs($args) {
			global $sys_custom;
			/*
			 $args = array(
			 "attr" => "name=\"homeworkID\" onChange=\"reloadForm()\"",
			 "param" => $handin_param,
			 "firstLabel" => $Lang['SysMgr']['Homework']['PleaseSelect'],
			 "firstVal" => ""
			 );
			 */
			$selectHTML = "";
			$allowStatus = array();
			
			/* If allow to get Year by Date, some page can not coordinate with Year ID, so please don't turn on it */ 
			$allowGetYearByDate = false;
			if ($allowGetYearByDate) {
				if (isset($args["param"]["startDate"]) && isset($args["param"]["endDate"])) {
					$AcademicYearAndYearTerm = getAcademicYearAndYearTermByDate($args["param"]["startDate"]);
					$searchYearTermID = $AcademicYearAndYearTerm["YearTermID"];
					$reqGetAcademicYear = true;
				} else {
					$TermInfoArr = getCurrentAcademicYearAndYearTerm();
					$searchYearTermID = $TermInfoArr['YearTermID'];
				}
				$objTerm = new academic_year_term($searchYearTermID, false);
				$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				// $termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
				{
					# for Buddhist Wong Wan Tin College
					$termStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($objTerm->AcademicYearID)));
				}
				else
				{
					$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				}
				if ($reqGetAcademicYear) {
					$args["param"]["yearID"] = $objTerm->AcademicYearID;
					$args["param"]["yearTermID"] = $objTerm->YearTermID;
				}
			} else {
				$TermInfoArr = getCurrentAcademicYearAndYearTerm();
				$searchYearTermID = $TermInfoArr['YearTermID'];
				$objTerm = new academic_year_term($searchYearTermID, false);
				// $termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
				{
					# for Buddhist Wong Wan Tin College
					$termStart = date("Y-m-d", strtotime(getStartDateOfAcademicYear($objTerm->AcademicYearID)));
				}
				else
				{
					$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
				}
				$args["param"]["yearID"] = $objTerm->AcademicYearID;
				$args["param"]["yearTermID"] = $objTerm->YearTermID;
			}
			
			$sql = "SELECT ih.HomeworkID, ih.Title, ih.Description, ih.DueDate, ih.ClassGroupID, yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5, ih.SubjectID, EN_DES, CH_DES FROM INTRANET_HOMEWORK AS ih ";
			//$sql .= " LEFT JOIN INTRANET_HOMEWORK_HANDIN_LIST AS ihhl ON (ih.HomeworkID=ihhl.HomeworkID)";
			//$sql .= " JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID=ihhl.StudentID AND ycu.YearClassID='" . $args["param"]["classID"] . "')";
			$sql .= " JOIN SUBJECT_TERM_CLASS AS stc ON stc.SubjectGroupID = ih.ClassGroupID";
			$sql .= " JOIN ASSESSMENT_SUBJECT AS hwas ON (ih.SubjectID=hwas.RecordID) ";
			$sql .= " JOIN SUBJECT_TERM_CLASS_USER AS stcu ON stcu.SubjectGroupID = stc.SubjectGroupID";
			$sql .= " JOIN YEAR_CLASS_USER AS ycu ON ycu.UserID = stcu.UserID";
			if (isset($args["param"]["classID"]) && !empty($args["param"]["classID"])) {
				$sql .= " JOIN YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=" . $args["param"]["yearID"] . " AND yc.YearClassID='" . $args["param"]["classID"] . "')";
			} else {
				$sql .= " JOIN YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=" . $args["param"]["yearID"] . ")";
			}
			$sql .= " WHERE ih.AcademicYearID='" . $args["param"]["yearID"] . "'";
			if($args["param"]["subjectID"] != "-1" && !empty($args["param"]["subjectID"])){
				$sql .= " AND ih.SubjectID='" . $args["param"]["subjectID"] . "'";
			}
			$sql .= " AND ( DueDate BETWEEN '" . $args["param"]["startDate"] . "'";
			$sql .= " AND '" . $args["param"]["endDate"] . "')";
			
			if (isset($args["param"]["classID"]) && !empty($args["param"]["classID"])) {
				$sql .= " AND yc.YearClassID='" . $args["param"]["classID"] . "'";
			}
			$sql .= " AND DueDate >= '" . $termStart . "'";

			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $_SESSION["UserID"];
			$subject = $this->getTeachingSubjectList($UserIDTmp, $args["param"]["yearID"], "", $args["param"]["classID"]);
			if($args["param"]["subjectID"] != "-1" && !empty($args["param"]["subjectID"])){
				$sbjGps = $this->getTeachingSubjectGroupList($UserIDTmp,$args["param"]["subjectID"], $args["param"]["yearID"], "", $args["param"]["classID"]);
			} else {
				$sbjGps = $this->getTeachingSubjectGroupList($UserIDTmp,"", $args["param"]["yearID"], "", $args["param"]["classID"]);
			}
			if(sizeof($sbjGps)>0) {
				for($i=0; $i<sizeof($sbjGps); $i++) {
					$temp[$i] = $sbjGps[$i][0];
				}
				$sql .= "AND ih.ClassGroupID IN (".implode(',',$temp).") ";
			}
			$sql .= " GROUP BY ih.HomeworkID ORDER BY Title desc";

			$result = $this->returnResultSet($sql);
			return $result; 
		}

		/* Get Select Element with Handin Param */
		function getSelectOptHomeworkList($args = array()) {
			/*
			$args = array(
					"attr" => "name=\"homeworkID\" onChange=\"reloadForm()\"",
					"param" => $handin_param,
					"firstLabel" => $Lang['SysMgr']['Homework']['PleaseSelectHomework'],
					"firstVal" => ""
			);
			*/
			$result = $this->getHomeListByArgs($args);
			$selectHTML = "<SELECT " . $args["attr"] . ">\n";
			$empty_selected = (!isset($args["param"]["homeworkID"]) || $args["param"]["homeworkID"] == '')? "SELECTED":"";
			if (isset($args["firstLabel"]) && !empty($args["firstLabel"])) {
				$selectHTML .= "<OPTION value='". ((isset($args["firstVal"]))? $args["firstVal"] : -1) ."' $empty_selected>" . $args["firstLabel"] . "</OPTION>\n";
			}
			if (isset($result) && count($result) > 0) {
				foreach ($result as $kk => $vv)
				{
					$id = $vv["HomeworkID"];
					$name =$vv["Title"];
					$selected_str = ($id==$args["param"]["homeworkID"]? "SELECTED":"");
					$selectHTML .= "<OPTION value='" . $id . "' $selected_str>$name</OPTION>\n";
				}
			}
			$selectHTML .= "</SELECT>\n";
			return $selectHTML;
		}

		function cust_replaceHTML($orgArr) {
			$htmlRepl = array( "\n" => " ", "\r" => " ", "'" => "&#39;", '"' => "&#34;" );
			if (count($orgArr) > 0) {
				foreach ($orgArr as $kk => $vv) {
					$orgArr[$kk] = str_replace(array_keys($htmlRepl), array_values($htmlRepl), $vv);
				}
			}
			return $orgArr;
		}

		/* Get Latest Misconduct check and sync to Discipline */
		function getLatestGenerateTime() {
			$strSQL = "SELECT DateInput FROM INTRANET_HOMEWORK_HANDIN_VIOLATION ORDER BY DateInput DESC LIMIT 1";
			$result = $this->returnResultSet($strSQL);
			return $result[0]["DateInput"];
		}

		/* This function is using to convert and retrieve for Discipline */   
		function getStudentsByParamForDiscipline($param) {
			/*
			 * Data retrieved by Handin Param
			 */
			global $sys_custom;
				
			$handin_studentsArr = $this->getStudentsByParam($param);
					

			$_violation = array(); /* Storing Data for Misconduct record */
			$handleArr = array(); /* Storing Data for Checking / Logging */
			$discipData = array(); /* Storing Data for Discipline */
			$dataArr = array(); /* Temp for store Orginial Data */
			$DD_dataArr = array(); /* Temp for store Discipline Data */
			if (count($handin_studentsArr) > 0) {
				foreach ($handin_studentsArr as $kk => $vv) {
					if ($vv["HandinRequired"] === "1") {
						if (!isset($dataArr[$vv["StudentID"]])) {
							$dataArr[$vv["StudentID"]] = array();
							$dataArr[$vv["StudentID"]]["info"]["UserID"] = $vv["UserID"];
							$dataArr[$vv["StudentID"]]["info"]["UserLogin"] = $vv["UserLogin"];
							$dataArr[$vv["StudentID"]]["info"]["StudentName"] = $vv["StudentName"];
							$dataArr[$vv["StudentID"]]["info"]["ClassNumber"] = $vv["ClassNumber"];
							$dataArr[$vv["StudentID"]]["info"]["StudentID"] = $vv["StudentID"];
							$dataArr[$vv["StudentID"]]["info"]["WEBSAMSCode"] = $vv["WEBSAMSCode"];
							$dataArr[$vv["StudentID"]]["info"]["ClassTitle"] = $vv["ClassTitle"];
							$dataArr[$vv["StudentID"]]["info"]["NoOfViolation"] = 1;
							$dataArr[$vv["StudentID"]]["info"] = $this->cust_replaceHTML($dataArr[$vv["StudentID"]]["info"]);
								
							$DD_dataArr[$vv["StudentID"]] = array();
							$DD_dataArr[$vv["StudentID"]] = $dataArr[$vv["StudentID"]];
						}
						$homeworkArr = array(
								"HomeworkID" => $vv["HomeworkID"],
								"Title" => $vv["Title"],
								"Description" => $vv["Description"],
								"RecordStatus" => $vv["RecordStatus"],
								"StartDate" => $vv["StartDate"],
								"DueDate" => $vv["DueDate"],
								"YearClassID" => $vv["YearClassID"],
								"ClassGroupID" => $vv["ClassGroupID"],
								"SubjectID" => $vv["SubjectID"],
								"Subject" => $vv["Subject"],
								"RecordID" => $vv["RecordID"],
								"RecordDate" => $vv["RecordDate"],
								"HandinRequired" => $vv["HandinRequired"],
								"SuppRecordStatus" => $vv["SuppRecordStatus"],
								"SuppRecordDate" => $vv["SuppRecordDate"],
								"HWViolationID" => $vv["HWViolationID"],
								"HWViolationDate" => $vv["HWViolationDate"]
						);
							
						$DD_homeworkArr = array(
								"HomeworkID" => $vv["HomeworkID"],
								"Title" => $vv["Title"],
								"YearClassID" => $vv["YearClassID"],
								"ClassGroupID" => $vv["ClassGroupID"],
								"SubjectID" => $vv["SubjectID"],
								"Subject" => $vv["Subject"],
								"RecordID" => $vv["RecordID"]
						);
						$homeworkArr = $this->cust_replaceHTML($homeworkArr);
						$DD_homeworkArr = $this->cust_replaceHTML($DD_homeworkArr);
							
						if ($param["sqlType"] == "forDataTransferBatchOldDataUpdate") {

							$strSQL = "SELECT HWVRecordID FROM INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST WHERE StudentID='" . $vv["StudentID"] . "' AND RecordID='" . $vv["RecordID"] . "' AND RecordType='VIOLATION_BATCH';";
							$result = $this->returnResultSet($strSQL);
							if (!(count($result) > 0)) {
								$dataArr[$vv["StudentID"]]["_violation"][$vv["RecordID"]] = $homeworkArr;
								$DD_dataArr[$vv["StudentID"]]["_violation"][$vv["RecordID"]] = $DD_homeworkArr;
								$_violation[] = $vv["StudentID"];
								$HWViolationDate = $vv["DueDate"];
								
								switch ($vv["WEBSAMSCode"]) {
									case "S1":
									case "S2":
										$handleArr["S1_S2"][$vv["StudentID"]] = $dataArr[$vv["StudentID"]];
										break;
									default:
										$handleArr["S3_S7"][$vv["StudentID"]] = $dataArr[$vv["StudentID"]];
										break;
								}
								$discipData[$HWViolationDate][$vv["StudentID"]] = $DD_dataArr[$vv["StudentID"]];
							}

						} else {
							switch ($vv["SuppRecordStatus"]) {
								/* "Mark as NOT SUBMIT" status */
								case "-20":
									$dataArr[$vv["StudentID"]]["_violation"][$vv["RecordID"]] = $homeworkArr;
									$DD_dataArr[$vv["StudentID"]]["_violation"][$vv["RecordID"]] = $DD_homeworkArr;
									$_violation[] = $vv["StudentID"];
									/*************************************************************************************************/
									/** IF "Mark as NOT SUBMIT", no need to strip Detention **/
									// $DD_dataArr[$vv["StudentID"]]["_detention"][$vv["RecordID"]] = $DD_homeworkArr;
									if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
										$dataArr[$vv["StudentID"]]["_detention"][$vv["RecordID"]] = $homeworkArr;
									}
									/*************************************************************************************************/
									break;
								case "7":
									if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
										/** Supplement Data is using for Remove Detention record from Discipline after Add Misconduct and Detention Record **/  
										/* This array is using for "Remove" Detention record */
										$dataArr[$vv["StudentID"]]["_supplement"][$vv["RecordID"]] = $homeworkArr;
										/* This array is using for "Add" Detention record */
										$dataArr[$vv["StudentID"]]["_detention"][$vv["RecordID"]] = $homeworkArr;
										/* This array is using for "Add" Misconduct record */
										$dataArr[$vv["StudentID"]]["_violation"][$vv["RecordID"]] = $homeworkArr;
										
										$DD_dataArr[$vv["StudentID"]]["_violation"][$vv["RecordID"]] = $DD_homeworkArr;
										$_violation[] = $vv["StudentID"];
										// 	$DD_dataArr[$vv["StudentID"]]["_supplement"][$vv["RecordID"]] = $DD_homeworkArr;
									}
									break;
								default:
									/* "ABS" status */
									if (in_array($vv["RecordStatus"], array(-12, -13))) {
										/* SuppRecordStatus is Empty */
										if ($vv["SuppRecordStatus"] == "-9999") {
											$dataArr[$vv["StudentID"]]["_ABS"][$vv["RecordID"]] = $homeworkArr;
										}
									}
									/* for Buddhist Wong Wan Tin College */
									if ( ! $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] ) {
										$dataArr[$vv["StudentID"]]["_detention"][$vv["RecordID"]] = $homeworkArr;
										$DD_dataArr[$vv["StudentID"]]["_detention"][$vv["RecordID"]] = $DD_homeworkArr;
									}
									break;
							}
							switch ($vv["WEBSAMSCode"]) {
								case "S1":
								case "S2":
									$handleArr["S1_S2"][$vv["StudentID"]] = $dataArr[$vv["StudentID"]];
									break;
								default:
									$handleArr["S3_S7"][$vv["StudentID"]] = $dataArr[$vv["StudentID"]];
									break;
							}
							$discipData[$vv["StudentID"]] = $DD_dataArr[$vv["StudentID"]];
						}
					}
				}
			}
			$currDate = date("Y-m-d");

			if (count($discipData) > 0 && $param["sqlType"] != "forDataTransferBatchOldDataUpdate") {
				foreach ($discipData as $studentID => $vv) {
					// $strSQL = "SELECT HWViolationID FROM INTRANET_HOMEWORK_HANDIN_VIOLATION WHERE StudentID='" . $studentID . "' AND HWViolationDate like '" . $currDate . "%'";
					$strSQL = "SELECT RecordID FROM INTRANET_HOMEWORK_HANDIN_VIOLATION AS ihv";
					$strSQL .= " JOIN INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST AS ihvl ON (ihv.HWViolationID=ihvl.HWViolationID)";
					$strSQL .= " WHERE ihv.StudentID='" . $studentID . "' AND HWViolationDate='" . $currDate . "%'";

					if (count($vv["_violation"]) > 0) {
						$sql = $strSQL . " AND RecordID IN (" . implode(", ", array_keys($vv["_violation"])) . ") GROUP BY RecordID";
						$result = $this->returnResultSet($sql);
						if (count($result) > 0) {
							foreach ($result as $okk => $ovv) {
								// unset($discipData[$studentID]["_violation"][$ovv["RecordID"]]);
							}
						}
					}
					if (isset($discipData[$studentID]["_violation"]) && count($discipData[$studentID]["_violation"]) == 0) {
						unset($discipData[$studentID]["_violation"]);
					}
					if (count($vv["_detention"]) > 0) {
						$sql = $strSQL . " AND RecordID IN (" . implode(", ", array_keys($vv["_detention"])) . ") GROUP BY RecordID";
						$result = $this->returnResultSet($sql);
						if (count($result) > 0) {
							foreach ($result as $okk => $ovv) {
								unset($discipData[$studentID]["_detention"][$ovv["RecordID"]]);
							}
						}
					}
					
					if (isset($discipData[$studentID]["_detention"]) && count($discipData[$studentID]["_detention"]) == 0) {
						unset($discipData[$studentID]["_detention"]);
					}
					if (!isset($discipData[$studentID]["_violation"]) && !isset($discipData[$studentID]["_detention"])) {
						unset($discipData[$studentID]);
					}
				}
			}
			unset($dataArr);
			unset($DD_dataArr);

			return array(
					$_violation,
					$handleArr,
					$discipData
			);
		}

		function getTotalViolationByUserIDs($ids, $handin_param = array())
		{
			global $sys_custom;
			if (isset($handin_param["startDate"]) && $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
			{
				$AcademicYearAndYearTerm = getAcademicYearAndYearTermByDate($handin_param["startDate"]);
				$AcademicYearID = $AcademicYearAndYearTerm["AcademicYearID"];
				$searchYearTermID = $AcademicYearAndYearTerm["YearTermID"];
			}
			else
			{
				$TermInfoArr = getCurrentAcademicYearAndYearTerm();
				$AcademicYearID = $TermInfoArr["AcademicYearID"];
				$searchYearTermID = $TermInfoArr['YearTermID'];
			}

			$objTerm = new academic_year_term($searchYearTermID, false);
			$termStart = $objTerm->TermStart;
			$termEnd = $objTerm->TermEnd;

			# Added AcademicYearID to handle Total record
			$output = array();
			$strSQL = "SELECT ihhl.StudentID, Count(ihhl.StudentID) AS Totalrec";
			$strSQL .= " FROM INTRANET_HOMEWORK AS ih";
			$strSQL .= " JOIN INTRANET_HOMEWORK_HANDIN_LIST AS ihhl ON (ih.HomeworkID=ihhl.HomeworkID)";
			$strSQL .= " JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID=ihhl.StudentID)";
			$strSQL .= " JOIN YEAR_CLASS AS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "') ";
			$strSQL .= " JOIN DISCIPLINE_ACCU_RECORD AS dae ON (dae.StudentHomeworkID = ih.HomeworkID AND dae.AcademicYearID=ih.AcademicYearID AND dae.StudentID=ihhl.StudentID) ";
			$strSQL .= " WHERE ihhl.StudentID in (" . implode(", ", $ids) . ") AND ih.HandinRequired='1' ";
			// $strSQL .= " AND SuppRecordStatus='-20'";
			$strSQL .= " AND ihhl.RecordStatus in ('-1','7','-20')";
			$strSQL .= " AND ih.AcademicYearID='" . $AcademicYearID . "'";
			$strSQL .= " AND ( CancelViolationDate IS NULL OR CancelViolationDate like '0000-00-00%' OR ihhl.CancelViolationDate < ihhl.SuppRecordDate)";
			// $strSQL .= " AND ih.DueDate >= '" . $termStart . "'";
			if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
			{
				$strSQL .= " AND ih.DueDate >= '" . $termStart . "' and ih.DueDate <= '" . $termEnd . "'";
			}
			$strSQL .= " GROUP BY ihhl.StudentID";
			$result = $this->returnResultSet($strSQL);

			if (count($result) > 0) {
				foreach ($result as $kk => $vv) {
					$output[$vv["StudentID"]] = $vv["Totalrec"];
				}
			}
			return $output;
		}

		function getLatestConfirmationDate() {
		    
		    
		    $strSQL = "SELECT DateInput FROM INTRANET_HOMEWORK_HANDIN_VIOLATION ORDER BY DateInput DESC LIMIT 1";
		    $result = $this->returnResultSet($strSQL);
		    $output = "";
		    if (count($result) > 0) {
		        $output = $result[0]["DateInput"];
		    }
		    return $output;
		}
		
		function checkTransferOrNot()
		{
		    $latest_trasnfer = $this->getLatestConfirmationDate();
			$strSQL = "SELECT DateModified FROM INTRANET_HOMEWORK_HANDIN_LIST ORDER BY DateModified DESC LIMIT 1";
			$result = $this->returnResultSet($strSQL);
			$latest_modified_date = "";
			if (count($result) > 0) {
			    $latest_modified_date = $result[0]["DateModified"];
			}
			
			if (!empty($latest_trasnfer) && !empty($latest_modified_date) )
			{
			    if (strtotime($latest_trasnfer) < strtotime($latest_modified_date))
			    {
			        return $latest_modified_date;
			    }
			} else if (empty($latest_trasnfer) && !empty($latest_modified_date) ){
			    
			    return $latest_modified_date;
			    
			} else if (!empty($latest_trasnfer) && empty($latest_modified_date) ){
			    
			    return false;
			    
			}
			return false;
		}
		
		function checkUpdateIsMoreThanOneDay($latest_modified_date)
		{
		    if ($latest_modified_date === false) {
		        return false;
		    } else {
		        $today = date("Y-m-d");
		        if (strtotime($latest_modified_date) <= strtotime($today)) {
		            return true;
		        } else {
		            return false;
		        }
		    }
		}
		
		function getHandInNumOfRecords($hwIDs = array())
		{
			$handInRec = array();
			if (count($hwIDs) > 0) {
				$strSQL = "SELECT HomeworkID, COUNT(RecordID) as TOTAL, MAX(MainStatusRecordDate) as LatestDate FROM INTRANET_HOMEWORK_HANDIN_LIST";
				$strSQL .= " WHERE HomeworkID in (" . implode(", ", $hwIDs) . ")";
				$strSQL .= " GROUP BY HomeworkID";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					foreach ($result as $kk => $vv) {
						$handInRec[$vv["HomeworkID"]] = $vv;
					}
				}
			}
			return $handInRec;
		}
		
		function checkDisciplineRec($handinArr)
		{
			$eDisArr = array();
			if (count($handinArr) > 0) {
				foreach ($handinArr as $kk => $handinRec) {
					$studentID = $handinRec["StudentID"];
					$recordID = $handinRec["RecordID"];
					if ($studentID > 0 && $recordID > 0) {
						$strSQL = "SELECT GroupingID, DateInput FROM DISCIPLINE_ACCU_HW_GROUPED_MAPPING WHERE StudentID='" . $studentID . "' AND HomeworkID='" . $recordID . "' ORDER BY DateInput DESC LIMIT 1";
						$result = $this->returnResultSet($strSQL);
						if (count($result) > 0) {
							$eDisArr[$studentID][$recordID] = $result[0];
						}
					}
				}
			}
			return $eDisArr;
		}
		
		function AddCancelMisconductLog($cancelInfo = array())
		{
			global $sys_custom;
			if (isset($cancelInfo["RecordID"]) && $cancelInfo["RecordID"] > 0
					&& isset($cancelInfo["HomeworkID"]) && $cancelInfo["HomeworkID"] > 0
					&& isset($cancelInfo["StudentID"]) && isset($cancelInfo["StudentID"]) > 0
			        && ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] || $sys_custom['eHomework_allow_cancel_conduct_button']))
			{
				$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST_CANCEL (" . implode(", ", array_keys($cancelInfo)) . ", InputBy, DateInput) VALUES ";
				$strSQL .= " ('" . implode("', '", array_values($cancelInfo)) . "', '" . $_SESSION["UserID"] . "', NOW());";
				$this->db_db_query($strSQL);
				// echo $strSQL . '<br>';
			}
		}
		
		function cancelMisconductFromDiscipline($cancelInfo = array())
		{
			global $sys_custom;
			if (isset($cancelInfo["HomeworkID"]) && $cancelInfo["HomeworkID"] > 0
					&& isset($cancelInfo["StudentID"]) && isset($cancelInfo["StudentID"]) > 0
			        && ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] || $sys_custom['eHomework_allow_cancel_conduct_button']))
			{
				$strSQL = "SELECT RecordID FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE HomeworkID='" . $cancelInfo["HomeworkID"] . "' AND StudentID='" . $cancelInfo["StudentID"] . "'";
				// echo $strSQL . '<br>';
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$cancelInfo["RecordID"] = $result[0]["RecordID"];
					$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET CancelViolationStatus='1', CancelViolationDate=NOW() WHERE ";
					$strSQL .= " RecordID='" . $cancelInfo["RecordID"] . "'";
					$strSQL .= " AND StudentID='" . $cancelInfo["StudentID"] . "'";
					$strSQL .= " AND HomeworkID='" . $cancelInfo["HomeworkID"] . "'";
					// $strSQL .= " AND CancelViolationDate IS NOT NULL AND CancelViolationDate NOT LIKE '0000-00-00%'";
					$this->db_db_query($strSQL);
					// echo $strSQL . '<br>';
					$cancelInfo["Remark"] = "Cancel from Discipline";
					$this->AddCancelMisconductLog($cancelInfo);
				}
				
			}
		}
		
		function getHomeworkHandinListRecords($map)
		{
			$cond = "";
			if(isset($map['RecordID'])){
				$record_id = IntegerSafe($map['RecordID']);
				if(is_array($map['RecordID'])){
					$cond .= " AND h.RecordID IN (".implode(",",$record_id).") ";
				}else{
					$cond .= " AND h.RecordID='".$record_id."' ";
				}
			}
			if(isset($map['HomeworkID'])){
				$homework_id = IntegerSafe($map['HomeworkID']);
				if(is_array($map['HomeworkID'])){
					$cond .= " AND h.HomeworkID IN (".implode(",",$homework_id).") ";
				}else{
					$cond .= " AND h.HomeworkID='".$homework_id."' ";
				}
			}
			if(isset($map['StudentID'])){
				$student_id = IntegerSafe($map['StudentID']);
				if(is_array($map['StudentID'])){
					$cond .= " AND h.StudentID IN (".implode(",",$student_id).") ";
				}else{
					$cond .= " AND h.StudentID='".$student_id."' ";
				}
			}
			if(isset($map['GetTeacherName']) && $map['GetTeacherName']){
				$teacher_document_name = getNameFieldByLang2("u1.");
				$teacher_score_name = getNameFieldByLang2("u2.");
				$more_fields = ",$teacher_document_name as TeacherDocumentUploaderName,$teacher_score_name as TextScoreUpdaterName ";
				$join_tables = " LEFT JOIN INTRANET_USER as u1 ON u1.RecordType=1 AND u1.UserID=h.TeacherDocumentUploadedBy 
								LEFT JOIN INTRANET_USER as u2 ON u2.RecordType=1 AND u2.UserID=h.TextScoreUpdatedBy ";
			}
			$sql = "SELECT h.* $more_fields FROM INTRANET_HOMEWORK_HANDIN_LIST as h $join_tables WHERE 1 $cond";
			$records = $this->returnResultSet($sql);
			
			if(isset($map['StudentIDToRecord'])){
				$ary = array();
				$record_size = count($records);
				for($i=0;$i<$record_size;$i++){
					$ary[$records[$i]['StudentID']] = $records[$i];
				}
				return $ary;
			}
			
			return $records;
		}
		
        function getReportHomeworkListTabs($CurrentTag)
        {
            global $Lang, $sys_custom;
            $TAGS_OBJ = array();
            $TagNow[$CurrentTag] = true;

            $TAGS_OBJ[] = array(
                $Lang['SysMgr']['Homework']['WeeklyHomeworkList'],
                "index.php",
                $TagNow["ByWeek"]
            );
            $TAGS_OBJ[] = array(
                $Lang['SysMgr']['Homework']['MonthlyHomeworkList'],
                "monthlyList.php",
                $TagNow["ByMonth"]
            );
            if ($sys_custom['eHomework']['DailyHomeworkList']) {
                $TAGS_OBJ[] = array(
                    $Lang['SysMgr']['Homework']['DailyHomeworkList'],
                    "dailyList.php",
                    $TagNow["ByDate"]
                );
            }
            return $TAGS_OBJ;
        }


        function getHomeworkListSql($ts, $studentID="", $condition="", $leader="", $print="", $classID="", $period="weekly")
        {
            global $UserID, $intranet_session_language;

            $start = $ts;
            if ($period == 'daily') {
                $end = $ts+86400;
            }
            else {      // weekly
                $end = $ts+604800;
            }

            # Fields
            if($this->useStartDateToGenerateList) {
                $date_conds = "UNIX_TIMESTAMP(a.StartDate) >= $start AND UNIX_TIMESTAMP(a.StartDate)<$end";
            }
            else {
                $date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";
            }

            // [DM#2934] Parent - Cannot view future homework in reports
            if($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
                $date_conds .= " AND a.StartDate<=CURDATE()";
            }

            $username_field = getNameFieldWithClassNumberByLang("c.");

            $fields = "a.HomeworkID, IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES),
					   IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5),a.StartDate, a.DueDate, a.Loading,
					   a.Title, a.Description, a.ClassGroupID, a.SubjectID, a.AttachmentPath, a.TypeID";

            # Homework Admin / Viewer Group Member / Non-Teacher Staff
            if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $this->isViewerGroupMember($UserID))
            {
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
		 						LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
                $conds = "$condition AND $date_conds";
            }
            # Teacher Staff
            else if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'])
            {
                # Class filtering - Current Year Class Teacher
                $clsInfo = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $UserID);
                for($i=0; $i<sizeof($clsInfo); $i++) {
                    $clsIDAry[] = $clsInfo[$i][0];
                }
                //$clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : "";
                $clsConds = (sizeof($clsIDAry)>0) ? " AND yct.YearClassID IN (".implode(',',$clsIDAry).")" : " AND yct.YearClassID IN ('')";

                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
								LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as c ON c.SubjectGroupID = a.ClassGroupID
								LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
								INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
								INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
								LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID $clsConds) ";
                $conds = "$condition AND (c.UserID = $UserID OR yct.UserID=$UserID) AND $date_conds";
            }
            # Subject Leader
            else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && $leader==1)
            {
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
								 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
								 LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";
                $conds = "$condition AND c.UserID = $UserID AND $date_conds";
            }
            # Others
            else {
                $dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
							  LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER as c ON c.SubjectGroupID = a.ClassGroupID
							  LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID";

                if ($studentID!=""){
                    $conds = "$condition AND c.UserID = $studentID AND $date_conds";
                }
                else {
                    $conds = "$condition AND c.UserID = $UserID AND $date_conds";
                }
            }

            if($classID!="") {
                $dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=d.SubjectGroupID)
								LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=F.UserID AND ycu2.YearClassID='$classID')";
                $conds .= " AND ycu2.YearClassID='$classID'";
            }

            $sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY HomeworkID ORDER BY a.DueDate";
            return $sql;
        }

    function exportDailyHomeworkList($date, $classID, $subjectID, $subjectGroupID, $yearID, $yearTermID)
    {
        $condition = "a.AcademicYearID = '$yearID' AND a.YearTermID = '$yearTermID' AND a.HandinRequired=1";
        if ($subjectID != '') {
            $condition .= " AND a.SubjectID = '$subjectID'";
        }
        if ($subjectGroupID != '') {
            $condition .= " AND a.ClassGroupID = '$subjectGroupID'";
        }
        if ($classID != "") {
            $condition .= " AND ycu.YearClassID='$classID'";
        }

        $ts = mktime(0,0,0,substr($date,5,2), substr($date,8,2), substr($date,0,4));
        $start = $ts;
        $end = $ts + 86400;

        $subjectName = Get_Lang_Selection('b.CH_DES', 'b.EN_DES');
        $subjectGroupName = Get_Lang_Selection('d.ClassTitleB5', 'd.ClassTitleEN');
        $className = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
        $studentName = getNameFieldByLang2("u.");

        # Fields
        if ($this->useStartDateToGenerateList) {
            $date_conds = "UNIX_TIMESTAMP(a.StartDate) >= $start AND UNIX_TIMESTAMP(a.StartDate)<$end";
        } else {
            $date_conds = "UNIX_TIMESTAMP(a.DueDate) >= $start AND UNIX_TIMESTAMP(a.DueDate)<$end";
        }

        $fields = "yc.YearClassID,
                    ycu.UserID,
                    a.ClassGroupID as SubjectGroupID,
                    a.HomeworkID, 
                    {$subjectName} as SubjectName,
					{$subjectGroupName} as SubjectGroupName,
					{$studentName} as StudentName,
					a.Title, 
					a.HandinRequired,
					{$className} as ClassName,
					ycu.ClassNumber";

        # Homework Admin / Viewer Group Member / Non-Teacher Staff
        if (($_SESSION['UserType'] == USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $this->isViewerGroupMember($_SESSION['UserID'])) {
            $dbtables = "INTRANET_HOMEWORK as a 
                            LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
                            LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
                            INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
                            INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
                            INNER JOIN INTRANET_USER u ON (u.UserID=ycu.UserID)
                            INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$yearID')
                            INNER JOIN YEAR y ON y.YearID=yc.YearID";

            $conds = "$condition AND $date_conds";
        } elseif ($_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION['isTeaching']) {     # Teacher Staff
            # Class filtering - Current Year Class Teacher
            $clsInfo = $this->getClassInfoByTeacherID(Get_Current_Academic_Year_ID(), $_SESSION['UserID']);
            for ($i = 0; $i < sizeof($clsInfo); $i++) {
                $clsIDAry[] = $clsInfo[$i][0];
            }

            $clsConds = (sizeof($clsIDAry) > 0) ? " AND yct.YearClassID IN (" . implode(',', $clsIDAry) . ")" : " AND yct.YearClassID IN ('')";

            $dbtables = "INTRANET_HOMEWORK as a 
                            LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
                            LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER as c ON c.SubjectGroupID = a.ClassGroupID
                            LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
                            INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
                            INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
                            INNER JOIN INTRANET_USER u ON (u.UserID=ycu.UserID)
                            INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$yearID')
                            INNER JOIN YEAR y ON y.YearID=yc.YearID
                            LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID $clsConds) ";
            $conds = "$condition AND (c.UserID = '" . $_SESSION['UserID'] . "' OR yct.UserID='" . $_SESSION['UserID'] . "') AND $date_conds";
        } elseif ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]) {
            $dbtables = "INTRANET_HOMEWORK as a 
                            LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
                            LEFT OUTER JOIN SUBJECT_TERM_CLASS as d ON d.SubjectGroupID = a.ClassGroupID
                            INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=d.subjectGroupID)
                            INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID)
                            INNER JOIN INTRANET_USER u ON (u.UserID=ycu.UserID)
                            INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$yearID')
                            INNER JOIN YEAR y ON y.YearID=yc.YearID";

            if ($subjectID == '') {
                $subject = $this->getStudyingSubjectList($_SESSION['UserID'], 1, $yearID, $yearTermID);
                $nrSubject = count($subject);
                if($nrSubject > 0){
                    $allSubjects = " a.SubjectID IN (";
                    for ($i=0; $i < $nrSubject; $i++){
                        list($ID)=$subject[$i];
                        $allSubjects .= $ID.",";
                    }
                    $allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
                    $condition .= " AND " . $allSubjects;
                }
            }
            $conds = "$condition AND $date_conds";

        }else {  # Others
            return false;
        }


        $sql = "SELECT $fields FROM $dbtables WHERE $conds ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber, a.DueDate, a.HomeworkID";

        $rsAry = $this->returnResultSet($sql);
        $studentHomework = BuildMultiKeyAssoc($rsAry, array('YearClassID', 'ClassNumber', 'HomeworkID'));
        $classHomework = BuildMultiKeyAssoc($rsAry, array('YearClassID', 'HomeworkID'));
        $classList = BuildMultiKeyAssoc($rsAry, array('YearClassID'), array('ClassName'),1);
        $ret = array('StudentHomework'=>$studentHomework, 'ClassHomework'=>$classHomework, 'ClassList'=>$classList);
        return $ret;
    }

    // group by user, base: 1 - form, 2 - class
    function getCustomizedStatusReport($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate="", $endDate="", $times="", $range="", $status="-1", $base="2"){
        global $intranet_session_language;

        if($subjectID==""){
            $subjects = $this->getTeachingSubjectList("", $yearID, $yearTermID);
            $nrSubject = count($subjects);
            if($nrSubject!=0){
                $allSubjects = " AND d.SubjectID IN (";
                for ($i=0; $i < $nrSubject; $i++){
                    list($sid)=$subjects[$i];
                    $allSubjects .= $sid.",";
                }
                $allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
            } else{
                $allSubjects ="";
            }
        } else {
            $allSubjects = " AND d.SubjectID IN ($subjectID)";
        }

        $dateConds = " (d.DueDate BETWEEN '$startDate' AND '$endDate')";

        if($yearTermID!="") {
            $termConds = " AND d.YearTermID = $yearTermID";
        }

        if($times!="" && $range!="") {
            $having = ($range==1) ? " HAVING total>=$times" : " HAVING total<=$times";
        }

        switch ($targetID){
            case 1:     // class
                $targetConds = " AND a.YearClassID IN (";
                for($i=0, $iMax=count($targetList); $i<$iMax; $i++) {
                    $targetConds .= $targetList[$i].",";
                }
                $targetConds = substr($targetConds,0,strlen($targetConds)-1).")";

                $statusConds = " AND ( e.RecordStatus = '$status' or e.SuppRecordStatus='$status' )";
                break;

            case 2:     // student
                $yearClassID = " AND a.YearClassID IN (";
                for($i=0, $iMax=count($targetList); $i<$iMax; $i++) {
                    $yearClassID .= $targetList[$i].",";
                }
                $yearClassID = substr($yearClassID,0,strlen($yearClassID)-1).")";

                $studentID = " AND e.StudentID IN (";
                for($i=0, $iMax=count($studentList); $i<$iMax; $i++) {
                    $studentID .= $studentList[$i].",";
                }
                $studentID = substr($studentID,0,strlen($studentID)-1).")";

                $targetConds = $yearClassID . $studentID;

                $statusConds = " AND e.RecordStatus = '$status'";
                break;

            case 3:     // subject group
                $subjectGroupID = " AND d.ClassGroupID IN (";
                for($i=0, $iMax=count($targetList); $i<$iMax; $i++) {
                    $subjectGroupID .= $targetList[$i].",";
                }
                $targetConds = substr($subjectGroupID,0,strlen($subjectGroupID)-1).")";

                $statusConds = " AND e.RecordStatus = '$status'";
                break;

            case 4:     // form
                $formID = " AND h.YearID IN (";
                for($i=0; $i<sizeof($targetList); $i++) {
                    $formID .= $targetList[$i].",";
                }
                $targetConds = substr($formID,0,strlen($formID)-1).")";

                $statusConds = " AND e.RecordStatus = '$status'";
                break;
        }

        $currentAcademicYearID = Get_Current_Academic_Year_ID();

        $sql = "SELECT
                     b.SubjectClassUserID, count(DISTINCT e.RecordID) as total
                FROM 
                    YEAR_CLASS_USER AS a
                    INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
                    INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
                    INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
                    INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
                    INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
                    INNER JOIN YEAR_CLASS AS h ON h.YearClassID = a.YearClassID 
                WHERE 
                    $dateConds
                    $allSubjects
                    $statusConds
                    $dueDateConds
                    $targetConds
                    Group by f.SubjectGroupID, b.UserID
                    $having
                    ORDER BY f.SubjectGroupID, b.UserID";

        $subjectGroupUserIDAry = $this->returnVector($sql);
        $subjectGroupUserCond = count($subjectGroupUserIDAry) ? " AND b.SubjectClassUserID IN (".implode(",",$subjectGroupUserIDAry).")" : " AND 1=0";

        ## YearName, ClassName and ClassNumber show those of current academic year
        $sql = "SELECT 
                    y.YearID,
                    y.YearName,
                    a.YearClassID,
                    e.RecordID,
                    IF('$intranet_session_language'='en', h.ClassTitleEN , h.ClassTitleB5) AS ClassName,
                    a.ClassNumber,
                    ".getNameFieldByLang2("c.")." AS StudentName,
                    IF('$intranet_session_language'='en', g.EN_DES , g.CH_DES) AS Subject,
                    IF('$intranet_session_language'='en', f.ClassTitleEN , f.ClassTitleB5) AS SubjectGroup,
                    d.Title,
                    d.Description,
                    d.DueDate                        
                FROM 
                    YEAR_CLASS_USER AS a
                    INNER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.UserID = a.UserID
                    INNER JOIN INTRANET_USER AS c ON c.UserID = b.UserID AND c.RecordStatus=1
                    INNER JOIN INTRANET_HOMEWORK AS d ON d.ClassGroupID = b.SubjectGroupID
                    INNER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.StudentID = a.UserID AND e.HomeworkID = d.HomeworkID AND d.AcademicYearID = $yearID $termConds)
                    INNER JOIN SUBJECT_TERM_CLASS AS f ON f.SubjectGroupID = b.SubjectGroupID
                    INNER JOIN ASSESSMENT_SUBJECT AS g ON g.RecordID = d.SubjectID
                    INNER JOIN YEAR_CLASS AS h ON (h.YearClassID = a.YearClassID AND h.AcademicYearID='$currentAcademicYearID')
                    INNER JOIN YEAR y ON y.YearID=h.YearID
                WHERE 
                    $dateConds
                    $allSubjects
                    $statusConds
                    $dueDateConds
                    $targetConds
                    $subjectGroupUserCond
                    Order by y.Sequence, h.Sequence ASC, a.ClassNumber ASC, Subject ASC, SubjectGroup ASC";
        $data = $this->returnResultSet($sql);

        $ret = array();
        if ($base == '2') {
            $ret['data'] = BuildMultiKeyAssoc($data,array('YearClassID','RecordID'));
            $ret['class'] = BuildMultiKeyAssoc($data,array('YearClassID'), array('ClassName'), 1);
        }
        else {
            $ret['data'] = BuildMultiKeyAssoc($data,array('YearID','YearClassID','RecordID'));
            $ret['form'] = BuildMultiKeyAssoc($data,array('YearID'), array('YearName'), 1);
            $ret['class'] = BuildMultiKeyAssoc($data,array('YearClassID'), array('ClassName'), 1);
        }

        return $ret;
    }

}
?>