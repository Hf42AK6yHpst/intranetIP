<?
// using by 
/*
 * 	Log
 * 	2016-10-28 [Cameron]
 * 		- create this file
 */
include_once("libdb.php");

class staff_monitoring_right extends libdb
{
	var $Module;
	
	function staff_monitoring_right()
	{
		parent::libdb();
		$this->Module = 'StaffAttendance3';
	}
	
	function Get_Group_List($Keyword)
	{
		if (trim($Keyword) != "")
		{
			$sql = 'select distinct 
								g.GroupID 
							From 
								MONITORING_RIGHT_GROUP as g  
							where 
								Module = \''.$this->Module.'\'
							and
								(
								g.GroupTitle like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								g.GroupDescription like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\'
								)';
			$GroupIDList = '-1,'.implode(',',$this->returnVector($sql));
		}
		
		$sql = 'SELECT UserID FROM INTRANET_USER WHERE RecordType=\'1\' AND RecordStatus=\'1\' ';	// staff
		$StaffIDList = '-1,'.implode(',',$this->returnVector($sql));
		
		$sql = 'SELECT
				  g.GroupID,
				  g.GroupTitle,
				  IF(g.GroupDescription IS NULL OR TRIM(g.GroupDescription) = \'\',\'--\',g.GroupDescription) as GroupDescription,
				  CASE 
				  	WHEN count(1) = 1 and ug.GroupID IS NULL THEN 0 
				  	ELSE count(1) 
				  END as NumberOfMember
				FROM 
					MONITORING_RIGHT_GROUP as g
				LEFT JOIN MONITORING_RIGHT_GROUP_MEMBER as ug
					ON g.GroupID = ug.GroupID 
					AND ug.UserID IN ('.$StaffIDList.') 
				WHERE 
					g.Module = \''.$this->Module.'\' ';
		
		if (trim($Keyword)!="") 
			$sql .= 'AND g.GroupID in ('.$GroupIDList.') ';
			
		
			$sql .= 'GROUP BY
						  g.GroupID 
					ORDER BY
						  g.GroupTitle';
		
		return $this->returnResultSet($sql);
	}
	
	function Get_Group_Info($GroupID)
	{
		$sql = "SELECT
					GroupID,
					GroupTitle,
					GroupDescription,
					DateModified
				FROM 
					MONITORING_RIGHT_GROUP 
				WHERE 
					GroupID = '$GroupID' 
					AND Module = '".$this->Module."' ";
					
		return $this->returnResultSet($sql);
	}
	
	function GET_MONITORING_RIGHT_GROUP_ID($GroupTitle)
	{
		$sql = "SELECT GroupID FROM MONITORING_RIGHT_GROUP WHERE Module='". $this->Module."'";
		if($GroupTitle=='') {
			$sql .= " AND (GroupTitle is NULL or GroupTitle ='') ";
		} else {
			$sql .= " AND GroupTitle='$GroupTitle'";
		}
		$GroupID = $this->returnVector($sql);
		return $GroupID[0];
	}
	
	function NEW_MONITORING_RIGHT_GROUP($GroupTitle, $GroupDescription)
	{
		$sql = "INSERT INTO MONITORING_RIGHT_GROUP SET GroupTitle='$GroupTitle', GroupDescription='$GroupDescription', DateInput=NOW(), DateModified=NOW(), Module='". $this->Module."', CreatedBy='".$_SESSION['UserID']."', ModifiedBy='".$_SESSION['UserID']."'";
		$Result = $this->db_db_query($sql);
		
		if ($Result) 
			return $this->db_insert_id();
		else 
			return false;
	}

	function UPDATE_MONITORING_RIGHT_GROUP($GroupID, $GroupTitle, $GroupDescription)
	{
		$sql = "UPDATE MONITORING_RIGHT_GROUP SET GroupTitle='$GroupTitle', GroupDescription='$GroupDescription', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE GroupID=$GroupID";
		return $this->db_db_query($sql);
	}
	
	function DELETE_MONITORING_RIGHT($GroupID)
	{
		$Result = array();
		for($i=0;$i<sizeof($GroupID);$i++) {
			$sql = "DELETE FROM MONITORING_RIGHT_GROUP WHERE GroupID=$GroupID[$i]";
			$Result[] = $this->db_db_query($sql);
			$sql = "DELETE FROM MONITORING_RIGHT_GROUP_MEMBER WHERE GroupID=$GroupID[$i]";
			$Result[] = $this->db_db_query($sql);
		}
		return !in_array(false,$Result);
	}
	
	function Insert_Group_Monitoring_Right($GroupID, $GroupTitle, $GroupDescription)
	{
		$Results = array();
		if ($GroupID == "") {
			$GroupID = $this->GET_MONITORING_RIGHT_GROUP_ID($this->Get_Safe_Sql_Query($GroupTitle));
			
			if($GroupID == "")
			{
				$GroupID = $this->NEW_MONITORING_RIGHT_GROUP($this->Get_Safe_Sql_Query($GroupTitle),$this->Get_Safe_Sql_Query($GroupDescription));
				$Result['CreateGroup'] = $GroupID;
			}
			else 
				$Result['UpdateGroup'] = $this->UPDATE_MONITORING_RIGHT_GROUP($GroupID, $this->Get_Safe_Sql_Query($GroupTitle), $this->Get_Safe_Sql_Query($GroupDescription));
		}
		if (in_array(false, $Result)) 
			return false;
		else 
			return $GroupID;
	}
	
	function Delete_Group($GroupID)
	{
		$Results=array();
		
		$sql = "DELETE FROM MONITORING_RIGHT_GROUP_MEMBER WHERE GroupID = '$GroupID' ";
		$Results['Delete_Members'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM MONITORING_RIGHT_GROUP WHERE GroupID = '$GroupID' AND Module='".$this->Module."' ";
		$Results['Delete_Group'] = $this->db_db_query($sql);
		
		return (!in_array(false, $Results));
 	}
 	
 	function Rename_Group($GroupTitle,$GroupID)
 	{
		$sql = 'UPDATE MONITORING_RIGHT_GROUP SET
							GroupTitle = \''.$this->Get_Safe_Sql_Query($GroupTitle).'\', 
							DateModified = NOW(), ModifiedBy=\''.$_SESSION['UserID'].'\'
				WHERE
					GroupID = \''.$GroupID.'\' AND Module = \''.$this->Module.'\' ';
		
		return $this->db_db_query($sql);
	}
	
	function Check_Group_Title($GroupTitle, $GroupID)
	{
		$sql = "SELECT
					GroupID 
				FROM 
					MONITORING_RIGHT_GROUP
				WHERE
					GroupID != '$GroupID' 
					AND GroupTitle = '".$this->Get_Safe_Sql_Query($GroupTitle)."' 
					AND Module = '".$this->Module."' ";
					
		return (sizeof($this->returnVector($sql))>0 ?false:true);
	}
	
	function Set_Group_Description($GroupDescription, $GroupID)
	{
		$sql = "UPDATE 
					MONITORING_RIGHT_GROUP 
				SET 
					GroupDescription = '".$this->Get_Safe_Sql_Query($GroupDescription)."',
					DateModified = NOW() 
				WHERE GroupID = '$GroupID' AND Module = '".$this->Module."' ";
				
		return $this->db_db_query($sql);
	}
	
	function Get_Group_Monitoring_Right_Users($GroupID, $Keyword="")
	{
		$NameField = getNameFieldByLang("u.");
		
		$sql = "SELECT 
					u.UserID,
					".$NameField." as UserName,
					u.Teaching,
					m.UserRole 
				FROM 
					INTRANET_USER as u 
					LEFT JOIN 
					MONITORING_RIGHT_GROUP_MEMBER as m 
					ON u.UserID = m.UserID 
					LEFT JOIN 
					MONITORING_RIGHT_GROUP as g
					ON m.GroupID = g.GroupID 
				WHERE 
					u.RecordType = '1' 
					AND 
					u.RecordStatus = '1' 
					AND 
					m.GroupID = '$GroupID'
					AND 
					(
					u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
					OR 
					u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
					or 
					u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					or 
					u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
					)";
		//debug_r($sql);
		return $this->returnResultSet($sql);
	}
	
	function Get_Group_Monitoring_Right_Aval_User_List($GroupID)
	{
		$NameField = getNameFieldByLang("u.");
		$sql = "SELECT 
					DISTINCT m.UserID 
				FROM 
					MONITORING_RIGHT_GROUP as g
					INNER JOIN MONITORING_RIGHT_GROUP_MEMBER as m ON m.GroupID = g.GroupID
				WHERE
					g.Module = '".$this->Module."'";
		if ($GroupID) { 
			$sql .= " AND g.GroupID = '".$GroupID."'";
		}
		$StaffList=$this->returnVector($sql);
		$sql = "SELECT 
					u.UserID,
					".$NameField." AS StaffName
				FROM 
					INTRANET_USER u  
				WHERE 
					u.RecordType = '1' 
					AND 
					u.RecordStatus = '1' 
					AND u.UserID NOT IN (".(sizeof($StaffList)>0?implode(",",$StaffList):"-1").") 
				ORDER BY 
					u.EnglishName ";
		return $this->returnResultSet($sql);
	}
	
	function Add_Member($GroupID,$AddUserID)
	{
		if (sizeof($AddUserID) > 0) 
		{
			$StaffList = implode(',',$AddUserID);
		}
		
		for ($i=0; $i< sizeof($AddUserID); $i++) {
			$sql = 'INSERT INTO MONITORING_RIGHT_GROUP_MEMBER 
								(GroupID,UserID, DateInput, CreatedBy, ModifiedBy) 
								VALUES
								(\''.$GroupID.'\',\''.$AddUserID[$i].'\',\'NOW()\',\''.$_SESSION['UserID'].'\', \''.$_SESSION['UserID'].'\')';
			$Result['Add-'.$AddUserID[$i]] = $this->db_db_query($sql);
		}
		
		return (!in_array(false,$Result));
	}
	
	function Remove_Member($GroupID,$StaffID)
	{
		$StaffList = implode(',',$StaffID);
		
		$sql = "DELETE FROM MONITORING_RIGHT_GROUP_MEMBER WHERE UserID IN (".$StaffList.") AND GroupID = '$GroupID' ";
		$Result['delete-MONITORING_RIGHT_GROUP_MEMBER'] = $this->db_db_query($sql);
		
		return (!in_array(false,$Result));
	}
	
	function Set_UserRole($GroupID,$StaffID,$UserRole) {
		if ($GroupID && (count($StaffID) > 0)) {
			$StaffList = implode(',',$StaffID);
		
			$sql = "UPDATE MONITORING_RIGHT_GROUP_MEMBER 
				SET 
					UserRole = '".($UserRole=='Leader'?1:0)."',
					DateModified = NOW(),
					ModifiedBy='".$_SESSION['UserID']."'
				WHERE GroupID = '$GroupID' AND UserID IN (".$StaffList.")";
				
			return $this->db_db_query($sql);
		}
		else {
			return false;
		}
	}
}
?>