<?php
// Editing by 
/********
 * 2020-07-28 Ray
 *          - modified getPushMessageReferenceInfo, added ReturnMessage
 * 
 * 2018-12-27 Anna
 *          - modified checkNotifyMessageAccessRight() , added pic access right 
 * 
 * 2018-12-12 Isaac
 *          - added getPushMessageUserSendSuccessStatus()
 *           
 * 2017-12-01 Ivan [K132111] [ip.2.5.9.1.1]
 *			- modified getPushMessageTargetInfo() to retrieve archive user also
 * 2017-04-24 Carlos
 * 			- $sys_custom['DHL'] Modified GET_MODULE_OBJ_ARR(), only open Push Notification to Staff.
 * 2017-03-07 Carlos [ip.2.5.8.4.1] [require update together with libcampusmail.php]
 * 			- modified send_campusmail(), update campus mail new mail counter. 
 * 
 * 2016-05-18 Ivan [L95978] [ip.2.5.7.7.1]
 * 			- modified function getPushMessageStatusDisplay() and getPushMessageStatusSelection() to add "not registered device" status
 * 2016-03-11 Ivan [ip.2.5.7.4.1]
 * 			- modified function GET_MODULE_OBJ_ARR(), checkNotifyMessageAccessRight() to add student app logic 
 * 
 * 2016-02-03 Kenneth
 * 			- modifiy sendMassMail() - add param $RowNumber
 * 			- added getLastMailRowNumber()
 * 2015-07-27 Omas [ip2.5.6.7.1]
 * 			- add push msg template to menuArr, merge push msg in to a new tab -> push notification service
 * 2014-10-24 Ivan [ip.2.5.5.10.1]
 * 			- modified checkNotifyMessageAccessRight() to check license also
 * 2014-10-23 Roy [ip.2.5.5.10.1]
 * 			- modified GET_MODULE_OBJ_ARR(), show eClass App module if $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"] = true
 * 			- modified checkNotifyMessageAccessRight(), for club pic and activity pic
 * 2014-07-23 Ivan [ip.2.5.5.8.1]
 * 			- added getPushMessageStatusSelection()
 * 
 * 2014-07-08 Ivan [ip.2.5.5.8.1]
 * 			- modified GET_MODULE_OBJ_ARR() to add teacher app notification menu
 * 			- added checkNotifyMessageAccessRight() for single source code access right checking
 * 
 * 2014-03-13 Carlos
 * 			- modified getSenderEmail(), if use iMail plus but no mail account, construct with userlogin@domain  
 * 				If finally fail to get iMail plus and webmail email, try use INTRANET_USER.UserEmail
 * 
 * 2012-11-07 Rita
 * 			-add clearcoo for SMS btn
 * 
 * 
 */

if (!defined("LIBMESSAGECENTER_DEFINED"))         // Preprocessor directives
{

 define("LIBMESSAGECENTER_DEFINED",true);

 class libmessagecenter extends libdb{
 	
 	var $AttachmentTotal;
 	var $UseCampusMail;
    
    var $libcampusmail; // reuse the libcampusmail class instance
       
	function libmessagecenter()
	{
		global $eclass_httppath_posfix, $eclass_filepath;
		$this->libdb();
		$this->Module = "MessageCenter";
		$this->UseCampusMail = false; // set true for testing using Campusmail
		if ((strstr($eclass_httppath_posfix, "junior") && trim($eclass_httppath_posfix)!="") || (strstr($eclass_filepath, "junior") && trim($eclass_filepath)!=""))
		{
			$this->text_encoding = "big5";
		} else
		{
			$this->text_encoding = "utf-8";
		}
	}
	
	
	function CaterBigForAJAX()
	{
		if ($this->text_encoding=="big5")
		{
			header("Content-Type:text/html;charset=BIG5");
		}
	}
       

    function GET_MODULE_OBJ_ARR()
    {
            global $UserID, $plugin, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root, $top_menu_mode, $special_feature, $sys_custom;
            
            ### wordings 
            global  $Lang, $_SESSION;
            
            $isKIS = $_SESSION["platform"]=="KIS";
            
            # Current Page Information init
            //$PageSurveyList = 0;
            
            
			switch ($CurrentPage) 
            {
            	case "PageMassMailing":
        				$PageMailMerge = true;
                	break;
                case "PageSMS":
                	$PageSMS = true;
                	$PageSMSMessage = true;
                	break;
                case "SMSUsage":
                	$PageSMS = true;
                	$PageSMSUsageReport = true;
                	break;
                case "SMSTemplate":
                	$PageSMS = true;
                	$PageSMSTemplates = true;
                	break;
                case "PageParentNotification":
                	$PagePushNotification = true;
                	$PageNotification = true;
                	break;
                case "PageTeacherNotification":
                	$PagePushNotification = true;
                	$PageTeacherNotification = true;
                	break;
                case "PageStudentNotification":
                	$PagePushNotification = true;
                	$PageStudentNotification = true;
                	break;
                case "PagePushNotificationTemplate":
                	$PagePushNotification = true;
                	$PagePushNotificationTemplate = true;
                	break;
        	}

//			$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/";
   
			// KIS has individual coding for mail merge
			if (!$isKIS && $_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] && !$sys_custom['DHL']) {	  
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/";
				$MenuArr["MassMailing"] = array($Lang['Header']['Menu']['MassMailing'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/", $PageMailMerge);
			}
			
			// KIS has individual coding for SMS
			if (!$isKIS && $_SESSION["SSV_USER_ACCESS"]['eAdmin-SMS'] && !$sys_custom['DHL']) {
				if (!isset($MODULE_OBJ['root_path'])) {
					$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/SMS/index.php?clearCoo=1";
				}
				
				//$MenuArr["MsgSMS"] = array($Lang['Header']['Menu']['SMS'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/sms/", $PageSMS);
				$MenuArr["MsgSMS"] = array($Lang['Header']['Menu']['SMS'], "", $PageSMS);
                        $MenuArr["MsgSMS"]["Child"]["Arrangement_EnrolmentUpdate"] = array($Lang['SMS']['MessageMgmt'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/SMS/index.php?clearCoo=1", $PageSMSMessage);
                        $MenuArr["MsgSMS"]["Child"]["Arrangement_Schedule"] = array($Lang['SMS']['UsageReport'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/SMS/usage.php", $PageSMSUsageReport);
                        $MenuArr["MsgSMS"]["Child"]["Report_ExportRecord"] = array($Lang['SMS']['MessageTemplates'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/SMS/template.php", $PageSMSTemplates);
				
			}
			
			if( $_SESSION["SSV_USER_ACCESS"]['eAdmin-ParentAppNotify'] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"] || $_SESSION["SSV_USER_ACCESS"]['eAdmin-TeacherAppNotify'] || $_SESSION["SSV_USER_ACCESS"]['eAdmin-StudentAppNotify']){
				$MenuArr["PushNotification"] = array($Lang['Header']['Menu']['PushNotification'], "", $PagePushNotification);
				if (($_SESSION["SSV_USER_ACCESS"]['eAdmin-ParentAppNotify'] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"]) && !$sys_custom['DHL']) {
					if (!isset($MODULE_OBJ['root_path'])) {
						$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/?appType=P";
					}
					$MenuArr["PushNotification"]["Child"]["ParentAppNotify"] = array($Lang['MessageCenter']['ToParent'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/?appType=P", $PageNotification);
				}
				if ($_SESSION["SSV_USER_ACCESS"]['eAdmin-TeacherAppNotify']) {
					if (!isset($MODULE_OBJ['root_path'])) {
						$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/TeacherNotification/";
					}
					$MenuArr["PushNotification"]["Child"]["TeacherAppNotify"] = array($Lang['MessageCenter']['ToTeacher'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/TeacherNotification/", $PageTeacherNotification);
				}
				if ($_SESSION["SSV_USER_ACCESS"]['eAdmin-StudentAppNotify'] && !$sys_custom['DHL']) {
					if (!isset($MODULE_OBJ['root_path'])) {
						$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/StudentNotification/";
					}
					$MenuArr["PushNotification"]["Child"]["StudentAppNotify"] = array($Lang['MessageCenter']['ToStudent'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/StudentNotification/", $PageStudentNotification);
				}
				
				if (!isset($MODULE_OBJ['root_path'])) {
					$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."/home/eClassApp/common/pushMessageTemplate/template_view.php?Module=MessageCenter";
				}
				$MenuArr["PushNotification"]["Child"]["PushNotificationTemplate"] = array($Lang['SMS']['MessageTemplates'], $PATH_WRT_ROOT."/home/eClassApp/common/pushMessageTemplate/template_view.php?Module=MessageCenter", $PagePushNotificationTemplate);
			}
//			if ($_SESSION["SSV_USER_ACCESS"]['eAdmin-ParentAppNotify'] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"]) {
//				$MenuArr["ParentAppNotify"] = array($Lang['Header']['Menu']['ParentAppsNotify'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/", $PageNotification);
//			}
//			if ($_SESSION["SSV_USER_ACCESS"]['eAdmin-TeacherAppNotify']) {
//				$MenuArr["TeacherAppNotify"] = array($Lang['Header']['Menu']['TeacherAppsNotify'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/MessageCenter/TeacherNotification/", $PageTeacherNotification);
//			}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass Message Center";'."\n";
        $js.= '</script>'."\n";
            
            ### module information
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['MessageCenter'].$js;
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_messagecenter.png";
            
            $MODULE_OBJ['menu'] = $MenuArr;
			
            return $MODULE_OBJ;
    }
    
    
    function GetCSVFile($MassMailingID, $CSVFile="")
    {
    	global $intranet_root;
    	
    	$Filepathmark = "LOADFILE:::";
    	
    	$CSVFileArr = array();
    	if ($CSVFile=="")
    	{
	    	$sql = "SELECT CSVFile FROM INTRANET_MASS_MAILING WHERE  MassMailingID='{$MassMailingID}'";
	    	$rows = $this->returnVector($sql);
	    	$CSVFile = $rows[0];
    	}
    	if (strstr($CSVFile, "LOADFILE:::"))
    	{
    		# physical file, e.g. file/mailmergecsv/{$MassMailingID}m97348732984734.dat
    		$CSVFileArr["filepath"] = str_replace("$Filepathmark", "", $CSVFile);
    		$CSVFileArr["data"] = unserialize(get_file_content($intranet_root."/".$CSVFileArr["filepath"]));
    	} else 
    	{
    		$CSVFileArr["data"] = unserialize($CSVFile);
    	}
    	
    	return $CSVFileArr;
    }
    
    
    function MergeContent($HTML_template, $HeaderArr, $CSVDataArr, $ForPreview=false)
    {
    	$merged_content = $HTML_template;
    	for ($i=0; $i<sizeof($HeaderArr); $i++)
    	{
    		if ($CSVDataArr[$HeaderArr[$i]]=="" || $CSVDataArr[$HeaderArr[$i]]=="---")
    		{
    			$CSVDataArr[$HeaderArr[$i]] = "--";
    		}
    		$MergedTo = ($ForPreview) ? "<span style='background:#88FF88;'>".$CSVDataArr[$HeaderArr[$i]]."</span>" : $CSVDataArr[$HeaderArr[$i]];
    		$merged_content = str_ireplace("[=".$HeaderArr[$i]."=]", $MergedTo, $merged_content);
    	}
    	
    	if ($ForPreview)
    	{
    		$ContentArr = explode("[=", $merged_content);
    		$unMerged = "";
    		if (sizeof($ContentArr)>1)
    		{
	    		for ($i=0; $i<sizeof($ContentArr); $i++)
	    		{
	    			$TagCloseArr = explode("=]", $ContentArr[$i]);
	    			if (sizeof($TagCloseArr)>1)
	    			{
	    				for ($k=0; $k<sizeof($TagCloseArr); $k++)
	    				{
	    					if ($k==0)
	    					{
	    						$unMerged .= "<span style='background:#FF6666;'>[=".$TagCloseArr[$k]."=]</span>";
	    					} else
	    					{
	    						$unMerged .= $TagCloseArr[$k];
	    					}
	    				}
	    			} else
	    			{
	    				$unMerged .= $ContentArr[$i];
	    			}
	    		}
	    		$merged_content = $unMerged;
    		} else
    		{
    			# no tag left!
    		}
    	}
    	
    	return $merged_content;
    }
    
    function ParseTemplateTags($HTML_template, $HeaderArr)
    {
    	$rx_template = $HTML_template;
    	for ($i=0; $i<sizeof($HeaderArr); $i++)
    	{
    		if (trim($HeaderArr[$i])!="")
    		{
	    		$regex = "/=<.*[^>][^\n]*?>".$HeaderArr[$i]."<.*?[^>][^\n]*?>=/i";
	    		$replaceby = "=".$HeaderArr[$i]."=";
				$rx_template = @preg_replace($regex, $replaceby, $rx_template);
    		}
    	}
    	return $rx_template;
    }


    function ShowError($MyTitle, $ErrArr)
    {
    	$rx = "<div class='table_board' width='300'> <table border='0' class='common_table_list view_table_list' >";
    	
    	$rx .= "<thead><tr><th colspan='2'><font color='red'>{$MyTitle}</font></th></tr></thead>";
    	
    	for ($i=0; $i<sizeof($ErrArr); $i++)
    	{
    		$rx .= "<tr><td>&nbsp;</td><td width='90%'>".$ErrArr[$i]."</td></tr>";
    	}
    	$rx .= "</table></div>\n";
    	
    	return $rx;
    }
    
    
    function getSenderEmail($SendAsAdmin, $user_id)
    {
    	global $plugin, $PATH_WRT_ROOT, $intranet_root, $sys_custom, $SYS_CONFIG;
    	
    	if ($SendAsAdmin)
		{
			$EmailSender = get_webmaster();
		}else
		{
			include_once($PATH_WRT_ROOT."includes/libwebmail.php");
			$liwm = new libwebmail();
			if ($plugin['imail_gamma'])
			{
				$sql = "SELECT ImapUserEmail, UserLogin FROM INTRANET_USER where UserID='$user_id'";
				$rows = $this->returnArray($sql);
				$EmailSender = $rows[0]['ImapUserEmail']; 
				if(trim($EmailSender) == ''){
					$EmailSender = $rows[0]['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix']; 
				}
			} elseif ($liwm->isExternalMailAvaliable())
			{
			    $sql = "SELECT a.UserLogin, b.ACL FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE a.UserID='".$this->Get_Safe_Sql_Query($user_id)."'";
				$rows = $this->returnArray($sql);
				if ($rows[0]["ACL"]==1 || $rows[0]["ACL"]==3)
				{
					$EmailSender = $rows[0]["UserLogin"]."@".$liwm->mailaddr_domain;
				}
			}
			if(trim($EmailSender) == ''){
				$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID='$user_id'";
				$row = $this->returnVector($sql);
				if(trim($row[0]) != ''){
					$EmailSender = $row[0];
				}
			}
		}

		return $EmailSender;
    }
    
    
    function CheckEmailAttachment($MassMailingID, $Attachment)
    {
    	global $intranet_root, $Lang, $lfs, $PATH_WRT_ROOT;
    	
    	if ($Attachment!="")
		{
			if (file_exists($Attachment))
			{
				$attachment_file = $Attachment;
			} else
			{
				$attachment_file = $intranet_root."/file/mailmerge_attachment/att".$MassMailingID."/".$Attachment;
			}
			if (file_exists($attachment_file))
			{
				if (!isset($lfs))
		    	{
					include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		    		$lfs = new libfilesystem();
		    	}
		    	$FileName = $lfs->get_file_basename($attachment_file);
				return "<p>".$Lang['MassMailing']['Attachments'].": <a href=\"/home/download_attachment.php?target_e=".getEncryptedText($attachment_file)."\" ><img border='0' align='absmiddle' src='/images/2009a/iMail/icon_attachment.gif'>".$FileName."</a></p>";
			}
			$message = str_replace("<--FileName-->", $Attachment, $Lang['MassMailing']['Attachments_NotFound']);
			return "<p><font color='red'>{$message}</font></p>";
		}
    }
    
    
    function GetAttachmentSummary($MassMailingID, $Link=false, $NullDisplay="--")
    {
    	global $intranet_root, $Lang, $lfs, $PATH_WRT_ROOT;
    	
    	if (!isset($lfs))
    	{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
    		$lfs = new libfilesystem();
    	}
    	$attachment_folder = $intranet_root."/file/mailmerge_attachment/att".$MassMailingID;
    	
    	
		if (file_exists($attachment_folder))
		{
    		$fileArr = $lfs->return_files($attachment_folder);
    		if (sizeof($fileArr)>0) 
    		{
    			$NoOfFiles = ($Link) ? "<a href=\"ajax_list_attachment.php?MassMailingID={$MassMailingID}&TB_iframe=true\" class=\"thickbox\"><img border='0' align='absmiddle' src='/images/2009a/iMail/icon_attachment.gif'><span id='AttachmentSizeInfo'>".sizeof($fileArr). "</span> ". $Lang['MassMailing']['AttachmentFiles'] . "</a>" : sizeof($fileArr). " " . $Lang['MassMailing']['AttachmentFiles'];
    			
    		} else
    		{
    			$NoOfFiles = "--";
    		}
    		
		} else
		{
			$NoOfFiles = "--";
		}
		
		return $NoOfFiles;
    	
    }
    
    
    function removeEmailAttachment($MassMailingID, $FileArr)
    {
    	global $intranet_root, $lfs, $PATH_WRT_ROOT;
    	
    	if (!isset($lfs))
    	{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
    		$lfs = new libfilesystem();
    	}
    	
    	for ($i=0; $i<sizeof($FileArr); $i++)
    	{
    		$Attachment = $FileArr[$i];
    		$attachment_file = $intranet_root."/file/mailmerge_attachment/att".$MassMailingID."/".$Attachment;
    		//debug($attachment_file);
    		$lfs->file_remove($attachment_file);
    	}
    }
    
    
    function ListAttachmentEdit($MassMailingID)
    {
    	global $intranet_root, $Lang, $lfs, $PATH_WRT_ROOT;
    	
    	if (!isset($lfs))
    	{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
    		$lfs = new libfilesystem();
    	}
    	$attachment_folder = $intranet_root."/file/mailmerge_attachment/att".$MassMailingID;
    	
    	
		if (file_exists($attachment_folder))
		{
    		$fileArr = $lfs->return_files($attachment_folder);
    		if (is_array($fileArr) && sizeof($fileArr)>0)
    		{ 
				natcasesort ($fileArr);
				$FileTotal = 0;
    			 for ($i=0; $i<sizeof($fileArr); $i++)
    			 {
    			 	$FilePath = $fileArr[$i];
    			 	if (file_exists($FilePath))
    			 	{
    			 		$FileName = $lfs->get_file_basename($FilePath);
    			 		$rxHtml .= "<tr><td ><a href=\"/home/download_attachment.php?target_e=".getEncryptedText($FilePath)."\" ><img border='0' align='absmiddle' src='/images/2009a/iMail/icon_attachment.gif'>".$FileName."</a></td><td>".number_format(filesize($FilePath)/1024)." KB</td><td>".date ("Y-m-d H:i:s", filemtime($FilePath))."</td><td><input type='checkbox' name='RemoveFiles[]' id='RemoveFiles[]' value=\"".$FileName."\" /></td></tr>";
    			 		$FileTotal ++;
    			 	}
    			 }
    			 $this->AttachmentTotal = $FileTotal;		 
    		} else
    		{
    			$rxHtml = "";
    		}
    		
		} else
		{
			$rxHtml = "";
		}
		
		return $rxHtml;
    }
    
    
    function ChangeEmailMethod($UseCampusMail)
    {
    	if ($UseCampusMail)
    	{
    		$this->UseCampusMail = true;
    	}	
    }
    
    
    function send_campusmail($RecipientID, $Subject, $Message, $EmailSender, $Attachment)
    {
    	global $UserID, $intranet_root, $lfs, $PATH_WRT_ROOT;
    	
    	if(!is_object($this->libcampusmail)){
			include_once($intranet_root."/includes/libcampusmail.php");
			$this->libcampusmail = new libcampusmail();
		}
    	
    	if ($Attachment!="" && file_exists($Attachment))
    	{
    		if (!isset($lfs))
	    	{
				include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	    		$lfs = new libfilesystem();
	    	}
		    	
    		$composeFolder = session_id().".".time();
			$composePath = $intranet_root."/file/mail";
			if(!is_dir($composePath)){
				$lfs->folder_new($composePath);
			}
			$composePath = $composePath."/u".$RecipientID;
			if(!is_dir($composePath)){
				$lfs->folder_new($composePath);
			}
			$composePath = $composePath."/".$composeFolder;
			$composeFolder = "u$RecipientID/".$composeFolder;
			if(!is_dir($composePath)){
				$lfs->folder_new($composePath);
			}
			
			if(is_dir($composePath)){
				$folder_copy_success = $lfs->file_copy($Attachment,$composePath."/");
			}
			if($folder_copy_success)
			{
				$AttachmentSize = round(filesize($Attachment)/1024);
				$AttachmentFolder = $composeFolder;
				$IsAttachment = "1";
			}
    	} else
    	{
    		$IsAttachment = "0";
    	}
    	$t_encoding = $this->text_encoding;
    	$IsImportant = 0;
    	$IsNotification = 0;
    	$RecordType = "2";
    	$isHTML = "1";
    	
	    $sql = "
	     INSERT INTO INTRANET_CAMPUSMAIL (
	          UserID, SenderID, RecipientID,InternalCC,InternalBCC,
	          ExternalTo,ExternalCC,ExternalBCC,
	          Subject, Message,".($t_encoding!=""?"MessageEncoding,":"")."Attachment,
	          IsAttachment, IsImportant, IsNotification,
	          MailType,UserFolderID,
	          RecordType, DateInput, DateModified, DateInFolder, AttachmentSize,isHTML
	     )
	     VALUES (
	          $RecipientID, $UserID, 'U{$RecipientID}','','',
	          '','','',
	          '$Subject', '".addslashes($Message)."',".($t_encoding!=""?"'$t_encoding',":"")."'$AttachmentFolder',
	          '$IsAttachment', '$IsImportant', '$IsNotification',
	          1,'$RecordType',
	          '$RecordType', now(), now(), now(), '$AttachmentSize','$isHTML'
	     )";

		//write_file_content($sql, $intranet_root."/file/mailmerge_attachment/sendlog_cm.txt");
		$result = $this->db_db_query($sql);
		$CampusMailID = $this->db_insert_id();
		
		if ($IsAttachment=="1")
		{
				$sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID, AttachmentPath, FileName, FileSize)
                        VALUES ('$CampusMailID', '$AttachmentFolder', '".$lfs->get_file_basename($Attachment)."', '$AttachmentSize') ";
		         $this->db_db_query($sql);
		         
		         $sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed + $AttachmentSize WHERE UserID = '$RecipientID'";
		         $this->db_db_query($sql);
		}
		
		if(isset($this->libcampusmail))
		{
			// Increment inbox new mail counter by 1
			$this->libcampusmail->UpdateCampusMailNewMailCount($RecipientID, 2, 1);
		}
		
		return $result;
    }
    
    
    function sendMassMail($MassMailingID, $EmailTitle, $HTML_template, $HeaderArr, $CSVDataRow, $EmailSender, $ShowContent, $ToEmail, $to_user_id, $RecipientName, $StdClass, $StdClassNumber, $IsParent="0", $Attachment="", $MailHeader="", $SenderName="", $RowNumber=0)
    {
    	global $libemail, $UserID, $intranet_root;
    	
    	# to campusmail ... TBD ... YUEN
    	
    	$result = false;
    	$MergeHTML = $this->MergeContent($HTML_template, $HeaderArr, $CSVDataRow);
		if ($this->UseCampusMail || (intranet_validateEmail($ToEmail) && intranet_validateEmail($EmailSender)))
		{
			
			if ($ShowContent==1)
			{
				$EmailBody = $MergeHTML;
			}

 			//write_file_content($Attachment, $intranet_root."/file/mailmerge_attachment/sendloglib2.txt");
 			if ($this->UseCampusMail)
 			{
 				$result = $this->send_campusmail($to_user_id, $EmailTitle, $MergeHTML, $EmailSender, $Attachment);
 			} else
 			{
				$result = $libemail->send_email($ToEmail, $EmailTitle, $MergeHTML, $EmailSender, "", "", $Attachment, $MailHeader,$SenderName);
 			}
			if ($result)
			{
				# record in database
				$sql = "INSERT INTO INTRANET_MASS_MAILING_LOG" .
						"(MassMailingID, RecipientEmail, RecipientName, IsParent, Class, ClassNumber, SenderEmail, EmailBody, Attachment, RowNumber, InputBy, InputDate)" .
						"values" .
						"({$MassMailingID}, '".addslashes($ToEmail)."', '".addslashes($RecipientName)."', '$IsParent', '".addslashes($StdClass)."', '{$StdClassNumber}', '".addslashes($EmailSender)."'," .
						"'".addslashes($EmailBody)."', '".addslashes($Attachment)."',".$RowNumber.", '$UserID', now() )";
				//debug_pr($sql);
				$this->db_db_query($sql);				
			}
		}
    	
    	return $result;
    }
    
    
    function updateMassMailStatus($MassMailingID, $Status)
    {
    	if ($Status=="COMPLETE")
    	{
    		$RecordStatus = "2"; 
    	} elseif ($Status=="IN_PROGRESS")
    	{
    		$RecordStatus = "1"; 
    	} else
    	{
    		$RecordStatus = "0";
    	}
    	$sql = "UPDATE INTRANET_MASS_MAILING SET RecordStatus='$RecordStatus' WHERE MassMailingID='{$MassMailingID}' AND RecordStatus<>'2' ";

		return $this->db_db_query($sql);	
    }
    
    function deleteRecord($MassMailingID)
    {
    	# TBD - permission?
    	
    	$sql = "DELETE FROM INTRANET_MASS_MAILING WHERE MassMailingID='{$MassMailingID}' ";
    	if ($this->db_db_query($sql))
    	{
    		$sql = "DELETE FROM INTRANET_MASS_MAILING_LOG WHERE MassMailingID='{$MassMailingID}' ";
    	}
    	
    	return $this->db_db_query($sql);
    }
    
    function getPushMessageTargetInfo($messageIdAry, $orderByName=false, $orderByClassNameClassNumber=false) {
    	if ($orderByName) {
    		//$orderBy = 'Order By iu.EnglishName';
    		$orderBy = 'Order By EnglishNameForSorting';
    	}
    	else if ($orderByClassNameClassNumber) {
    		//$orderBy = 'Order By iu.ClassName, iu.ClassNumber';
    		$orderBy = 'Order By ClassNameForSorting, ClassNumberForSorting';
    	}
    	
    	// 			$sql = "Select
    	// 						t.NotifyMessageID, t.NotifyMessageTargetID, t.TargetType, t.TargetID, t.MessageTitle, t.MessageContent
    	// 				From
    	// 						INTRANET_APP_NOTIFY_MESSAGE_TARGET as t
    	// 						INNER JOIN INTRANET_USER as iu ON (t.TargetID = iu.UserID)
    	// 				Where
    	// 						t.NotifyMessageID In ('".implode("','", (array)$messageIdAry)."')
    	// 						$orderBy
    	// 						";
    	$sql = "Select
						t.NotifyMessageID, t.NotifyMessageTargetID, t.TargetType, t.TargetID, t.MessageTitle, t.MessageContent,
						IF (iu.UserID is not null, iu.EnglishName, iau.EnglishName) as EnglishNameForSorting,
						IF (iu.UserID is not null, iu.ClassName, iau.ClassName) as ClassNameForSorting,
						IF (iu.UserID is not null, iu.ClassNumber, iau.ClassNumber) as ClassNumberForSorting
				From
						INTRANET_APP_NOTIFY_MESSAGE_TARGET as t
						LEFT JOIN INTRANET_USER as iu ON (t.TargetID = iu.UserID)
						LEFT JOIN INTRANET_ARCHIVE_USER as iau ON (t.TargetID = iau.UserID)
				Where
						t.NotifyMessageID In ('".implode("','", (array)$messageIdAry)."')
						$orderBy
						";
						return $this->returnResultSet($sql);
    }
    
    function getPushMessageRelatedToInfo($messageIdAry) {
    	$sql = "Select 
						mt.NotifyMessageID, mt.NotifyMessageTargetID, mtrt.UserID
				From
						INTRANET_APP_NOTIFY_MESSAGE_TARGET as mt
						Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO as mtrt ON (mt.NotifyMessageTargetID = mtrt.NotifyMessageTargetID) 
				Where
						mt.NotifyMessageID In ('".implode("','", (array)$messageIdAry)."')
				";
		return $this->returnResultSet($sql);
    }
    
    function getPushMessageReferenceInfo($messageIdAry) {
    	$sql = "Select
						NotifyMessageID, NotifyMessageTargetID, MessageStatus, ErrorCode, ReturnMessage
				From
						INTRANET_APP_NOTIFY_MESSAGE_REFERENCE
				Where
						NotifyMessageID In ('".implode("','", (array)$messageIdAry)."')
				";
		return $this->returnResultSet($sql);
    }
    
    function getPushMessageStatusDisplay($status) {
    	global $eclassAppConfig, $Lang;
    	
    	$statusLang = '';
    	if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']) {
    		$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'];
    	}
    	else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']) {
    		$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendSuccess'];
    	}
    	else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']) {
    		$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['userHasRead'];
    	}
    	else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']) {
    		$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['waitingToSend'];
    	}
    	else if ((string)$status == (string)$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']) {
    		$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['noRegisteredDevice'];
    	}
    	else if ((string)$status == (string)$eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'] || (string)$status == (string)$eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer']) {
    		$statusLang = $Lang['MessageCentre']['pushMessageAry']['statusAry']['sendFailed'].' ('.$Lang['MessageCentre']['pushMessageAry']['errorStatusAry']['cannotConnectToServiceProvider'].')';
    	}
    	
    	return $statusLang;
    }
    
    function getPushMessageStatusSelection($parId, $parName, $selectedStatus, $parOnchange='', $parNotifyMessageId='') {
    	global $Lang, $eclassAppConfig, $PATH_WRT_ROOT;
    	
    	if ($parNotifyMessageId > 0) {
    		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
    		$leClassApp = new libeClassApp();
    		$statisticsAry = $leClassApp->getPushMessageStatistics($parNotifyMessageId, $excludeHasReadFromSuccess=true);
    		
    		$numOfRecepient = ' ('.$statisticsAry[$parNotifyMessageId]['numOfRecepient'].')';
    		$numOfSuccess = ' ('.$statisticsAry[$parNotifyMessageId]['numOfSendSuccess'].')';
			$numOfFailed = ' ('.$statisticsAry[$parNotifyMessageId]['numOfSendFailed'].')';
			$numOfHasRead = ' ('.$statisticsAry[$parNotifyMessageId]['numOfHasRead'].')';
			$numOfWaitingToSend = ' ('.$statisticsAry[$parNotifyMessageId]['numOfWaitingToSend'].')';
			$numOfNoRegisteredDevice = ' ('.$statisticsAry[$parNotifyMessageId]['numOfNoRegisteredDevice'].')';
    	}
    	
    	$optionAry = array();
    	$optionAry[-1] = Get_Selection_First_Title($Lang['MessageCentre']['pushMessageAry']['AllStatus'].$numOfRecepient);
    	$optionAry[$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']] = $this->getPushMessageStatusDisplay($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']).$numOfWaitingToSend;
    	$optionAry[$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']] = $this->getPushMessageStatusDisplay($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']).$numOfSuccess;
    	$optionAry[$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']] = $this->getPushMessageStatusDisplay($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']).$numOfFailed;
    	$optionAry[$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']] = $this->getPushMessageStatusDisplay($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']).$numOfHasRead;
    	$optionAry[$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']] = $this->getPushMessageStatusDisplay($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']).$numOfNoRegisteredDevice;

		$onchangeAttr = '';
		if ($parOnchange != "") {
			$onchangeAttr = 'onchange="'.$parOnchange.'"';
		}
		
		$selectionAttr = ' id="'.$parId.'" name="'.$parName.'" '.$onchangeAttr;
		
		return getSelectByAssoArray($optionAry, $selectionAttr, $selectedStatus, $all=0, $noFirst=1);
    }
    
    function getPushMessageFromImportTempTable() {
    	$sql = "SELECT * FROM TEMPSTORE_PUSHMESSAGE_FILE_USER where UserID = '".$_SESSION['UserID']."'";
		return $this->returnResultSet($sql);
    }
    
    function checkNotifyMessageAccessRight($appType) {
    	global $PATH_WRT_ROOT, $intranet_root, $eclassAppConfig, $sys_custom;
    	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
    	
    	if ($appType == '') {
    		$appType = $eclassAppConfig['appType']['Parent'];
    	}
    	
    	if($sys_custom['DHL']){
    	    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    	    $libdhl = new libdhl();
    	    $isPIC = $libdhl->isPIC();
    	}
    	
    	$canAccess = false;
    	$leClassApp = new libeClassApp();
		if ($leClassApp->isSchoolInLicense($appType)) {
			if ($appType==$eclassAppConfig['appType']['Parent'] && (
	    		$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"]
	    		)) {
	    		$canAccess = true;
	    	}
	    	else if ($appType==$eclassAppConfig['appType']['Teacher'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"] || ($isPIC && $sys_custom['DHL']))) {
	    		$canAccess = true;
	    	}
	    	else if ($appType==$eclassAppConfig['appType']['Student'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"]) {
	    		$canAccess = true;
	    	}			
		}
    	
    	if (!$canAccess) {
    		No_Access_Right_Pop_Up();
    	}
    	
    	return true;
    }
	function getLastMailRowNumber($MassMailingID){
		$sql = "Select MassMailingLogID, RowNumber
					From INTRANET_MASS_MAILING_LOG 
					WHERE MassMailingID = '".$MassMailingID."'
					Order By RowNumber Desc Limit 1";
		$returnArr = $this->returnArray($sql);
		$RowNumber = $returnArr[0]['RowNumber'];
		return $RowNumber;
	}
 
	function getPushMessageUserSendSuccessStatus($notifyMessageIdAry) {#return all users with send sucess status(true/false)
	    global $eclassAppConfig;
	    $field = Get_Lang_Selection("if(iu.ChineseName != '', iu.ChineseName, iu.EnglishName) as name" ,"iu.EnglishName as name");
	    $sql = "Select
	    $field, mt.TargetID as RecepientUserID, mr.MessageStatus
	    From
	    INTRANET_APP_NOTIFY_MESSAGE_TARGET as mt
	    Inner Join INTRANET_APP_NOTIFY_MESSAGE_REFERENCE as mr ON (mt.NotifyMessageTargetID = mr.NotifyMessageTargetID)
	    Inner Join INTRANET_USER as iu ON (mt.TargetID = iu.UserID)
    					Where
    							mt.NotifyMessageID IN ('".implode("','", (array)$notifyMessageIdAry)."')
    							    
    					";
	    $messageAry = $this->returnResultSet($sql);
	    
	    $resultAry =array();
	    foreach($messageAry as $userMessageStatusAry){
	        if ($userMessageStatusAry['MessageStatus'] == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']) {
	            $__sendSuccess = true;
	        }
	        else if ($userMessageStatusAry['MessageStatus'] == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']) {
	            $__sendSuccess = true;
	        } else{
	            $__sendSuccess = false;
	        }
	        $RecepientUserID = $userMessageStatusAry['RecepientUserID'];
	        if(empty($resultAry[$RecepientUserID]['name'])){
	            $resultAry[$RecepientUserID]['name'] = $userMessageStatusAry['name'];
	        }
	        if(!$resultAry[$RecepientUserID]['MessageStatus']){//only update success status when status is false / if status is true do nth
	            $resultAry[$RecepientUserID]['MessageStatus'] = $__sendSuccess;
	        }
	    }
	    return $resultAry;
	}
 }
} // End of directives
?>