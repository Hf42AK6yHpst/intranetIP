<?php
if (!defined("LIBCLICKATELLSMS_DEFINED"))                     // Preprocessor directive
{
define("LIBCLICKATELLSMS_DEFINED", true);

include_once("$intranet_root/includes/libhttpclient.php");
include_once("$intranet_root/includes/libwebmail.php");
include_once("$intranet_root/lang/email.php");
class libclickatellsms {

      var $service_host;
      var $service_port;
      var $service_action;
      var $data_action;
      var $usage_action;
      var $status_action;
      var $login;
      var $password;
      var $school_code;
      var $area_code;
      var $simulate_only;
      var $cancel_previous_action;
      var $start_transaction;
      var $end_transaction;
      var $quicksend;

        function libclickatellsms ()
        {
                 global $sms_clickatell_school_code, $sms_clickatell_host, $sms_clickatell_port, $sms_clickatell_login, $sms_clickatell_passwd,
                        $sms_clickatell_api_send_request, $sms_clickatell_api_get_reply, $sms_clickatell_api_get_usage, $sms_clickatell_api_get_status,
                        $sms_clickatell_simulate_only, $sms_clickatell_api_cancel_previous, $sms_clickatell_api_start_transaction, $sms_clickatell_api_end_transaction, $sms_clickatell_area_code, $sms_clickatell_api_quicksend;
                 $this->service_host = $sms_clickatell_host;
                 $this->service_port = $sms_clickatell_port;
                 $this->service_action = $sms_clickatell_api_send_request;
                 $this->data_action = $sms_clickatell_api_get_reply;
                 $this->usage_action = $sms_clickatell_api_get_usage;
                 $this->status_action = $sms_clickatell_api_get_status;
                 $this->login = $sms_clickatell_login;
                 $this->password = $sms_clickatell_passwd;
                 $this->school_code = $sms_clickatell_school_code;
		 $this->area_code = $sms_clickatell_area_code;
                 $this->simulate_only = $sms_clickatell_simulate_only;
                 
                 $this->quicksend = $sms_clickatell_api_quicksend;
                 $this->cancel_previous_action = $sms_clickatell_api_cancel_previous;
                 $this->start_transaction_action = $sms_clickatell_api_start_transaction;
                 $this->end_transaction_action = $sms_clickatell_api_end_transaction;

                 if ($this->school_code == "")
                 {
                     die("Bad Configuration. Please contact eClass support. <support@broadlearning.com>");
                 }
        }

	/*
	Description: Use to send individual SMS
	*/

        function sendSMS($recipient, $message, $message_code="", $delivery_time="", $isReplyMessage)
        {
		global $sms_clickatell_debug_email, $sms_clickatell_debug_mobile, $webmaster, $checkMultiRecipient;

              	// add debug mobile
	        if($sms_clickatell_debug_mobile!=""){
			$recipient.=",".$sms_clickatell_debug_mobile;
		        $message_code.=",0";
		}

                $checkMultiRecipient = strpos($recipient,",");
		if ($checkMultiRecipient===false)
                {
			//Create the URL
        	         $message = stripslashes($message);
                	 $post_data = "user=".$this->login . "&password=".$this->password."&to=".$this->area_code.$recipient;
	                 $post_data .= "&unicode=1&api_id=".$this->school_code;
//	                 $post_data .= "&api_id=".$this->school_code;
        	         $post_data .= "&text=".urlencode($message);
                }
		else {
			$splitReceipient = explode(",", $recipient);

			for($i = 0; $i < count($splitReceipient); $i++){
				//echo "Receipient $i = $splitReceipient[$i] <br />";
				$finalReceipient .= $this->area_code.$splitReceipient[$i];
				if ($i < (count($splitReceipient) - 1))
					$finalReceipient .= ",";
			}
        	        $message = stripslashes($message);
                	$post_data = "user=".$this->login . "&password=".$this->password."&to=".$finalReceipient;
//                        $post_data .= "&unicode=1&api_id=".$this->school_code;
                        $post_data .= "&api_id=".$this->school_code;
			//print ">>> Final receipient: $finalReceipient <BR>";
		}
		

		 if ( $delivery_time != "")
                 	$post_data .= "&deliv_time=".$delivery_time;


                 if ($this->simulate_only)
                 {
                     global $intranet_root;
                     $logpath = "$intranet_root/file/clickatell.send";
                     $content = get_file_content($logpath);
                     write_file_content ($content."\n".$post_data,$logpath);
                     return "SIMULATE";
                 }
                 else                 
                 {
	                 // send mail
		         if($sms_clickatell_debug_email!=""){
	        	 	$lwebmail = new libwebmail();
	        	 	$subject ="Send SMS Notification";
	        	 	$mail_message = "Message : $message\n<BR>";
	        	 	$mail_message.= "Recipient: $recipient\n<BR>";
	        	 	$mail_message.= "School Code : ".$this->school_code."\n<BR>";
	        	 	if($delivery_time!=""){
		        	 	$mail_message.= "Delivery Time : ".$delivery_time."\n<BR>";
		        	}
//		        	if($lwebmail->has_webmail){
//		         		$lwebmail->sendMail($subject,$mail_message,$webmaster,array($sms_clickatell_debug_email),array(),array(),"","",$webmaster,$webmaster);
//		         	}
		         }
	              
                     # Try to connect to service host
                     $lhttp = new libhttpclient($this->service_host,$this->service_port);
                     if (!$lhttp->connect()){    # Exit if failed to connect
   
                          return false;
                     }

		// Not work (20090219)
                if($isReplyMessage){      

			//cancel the previous sms
			$target_url = "http://".$this->service_host.$this->cancel_previous_action."?user=".$this->login."&password=".$this->password."&api_id=".$this->school_code;
                 	$fp = fopen($target_url,"r");
			fclose($fp);
						
			//start transaction
			$target_url = "http://".$this->service_host.$this->start_transaction_action."?user=".$this->login."&password=".$this->password."&api_id=".$this->school_code;
			$fp = fopen($target_url,"r");
			fclose($fp);
		}


                if ($checkMultiRecipient===false){
			//send sms
                    	$lhttp->set_path($this->service_action);
                    	$lhttp->post_data = $post_data;
	                $response = $lhttp->send_request();
               	}else{
                        //send sms - 20090224
			// Start Batch mode and get session ID
			$startbatch_url = "http://".$this->service_host.$this->start_transaction_action."?user=".$this->login."&password=".$this->password."&unicode=1&api_id=".$this->school_code."&template=".urlencode($message);

			// Connect to clickaTell - StartBatch
			$lhttp->set_path($startbatch_url);
                        $returnSessionID = $lhttp->send_request();

		$findReturnString = strrchr($returnSessionID,"ID:");
		if ($findReturnString == ""){
                        return false;
		}else{

                        $returnSessionIDArray = explode(":",$findReturnString);
			
			// Create SMS string 
                        $post_data .= "&batch_id=". trim($returnSessionIDArray[1]);

			// Send SMS 20090225
                        $lhttp->set_path($this->quicksend);
                        $lhttp->post_data = $post_data;
                        $response = $lhttp->send_request();

			$endbatch_url = "http://".$this->service_host.$this->end_transaction_action."?user=".$this->login."&password=".$this->password."&api_id=".$this->school_code."&batch_id=".trim($returnSessionIDArray[1]);
		}
	}

                # Put in log for debug
		global $intranet_root;
                write_file_content ($post_data,"$intranet_root/file/clickatell.send");
                write_file_content ($response,"$intranet_root/file/clickatell.log");

                # Filter the HTTP protocol message and extract the XML document only
		
		// Not work (20090219)
                $pos = strpos($response,"<?xml ");
                if ($pos===false)
                {
                	$pos = strpos($response,"<?XML ");
                }
                $response = substr($response,$pos);
                $records = XML2Obj($response);
                     
                if($isReplyMessage){
	        	//close transaction
	                $target_url = "http://".$this->service_host.$this->end_transaction_action."?user=".$this->login."&password=".$this->password."&api_id=".$this->school_code;
	                $fp = fopen($target_url,"r");
	                fclose($fp);
                 }
                     return $records;

            }

        }


# phase 2        
        # recipeintData : list of array(phone,message,message code,delivery time)
        function sendBulkSMS($recipientData,$isReplyMessage)
        {
	        	 global $sms_clickatell_debug_email, $sms_clickatell_debug_mobile, $webmaster;


			/* Skip this.. $recipientData checking always fail */
			// if($recipientData=="" || sizeof($recipientData)<=0){
			//        return false;
			// }


	             // add debug mobile
	        	 if($sms_clickatell_debug_mobile!=""){
		        	 //$recipient.=",".$sms_clickatell_debug_mobile;
		        	 //$recipientData[] = ($sms_clickatell_debug_mobile, $message, $message_code,$delivery_time) 
		         }
		         			    			    
			    if (!$this->simulate_only){
    
			             # Try to connect to service host
			             $lhttp = new libhttpclient($this->service_host,$this->service_port);
			             if (!$lhttp->connect()){    # Exit if failed to connect
			                  return false;
			             }
			        	if($isReplyMessage){
						//cancel the previous sms
						$target_url = "http://".$this->service_host.$this->cancel_previous_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
				             	$fp = fopen($target_url,"r");
							fclose($fp);
							
							//start transaction
							$target_url = "http://".$this->service_host.$this->start_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
							$fp = fopen($target_url,"r");
							fclose($fp);
						}
				}
								
				$records = array();
				
				for($i=0;$i<sizeof($recipientData);$i++){
					list($recipient, $message, $message_code,$delivery_time) = $recipientData[$i];
                 	$message = stripslashes($message);
	                $post_data = "user=".$this->login . "&password=".$this->password."&to=".$this->area_code.$recipient;
                 	$post_data .= "&unicode=1&api_id=".$this->school_code;
//                 	$post_data .= "&api_id=".$this->school_code;
                 	$post_data .= "&text=".urlencode($message);

	                 if ($this->simulate_only)
	                 {
	                     global $intranet_root;
	                     $logpath = "$intranet_root/file/clickatell.send";
	                     $content = get_file_content($logpath);
	                     write_file_content ($content."\n".$post_data,$logpath);
	
	                     // return "SIMULATE";
	                     $records[] = "SIMULATE";
	                 }
	                 else                 
	 	         {
                          
	   		     //send sms
	                     $lhttp->set_path($this->service_action);
	                     $lhttp->post_data = $post_data;
	                     $response = $lhttp->send_request();

                     
	                     # Put in log for debug
	                     global $intranet_root;
	                     write_file_content ($post_data,"$intranet_root/file/clickatell.send");
	                     write_file_content ($response,"$intranet_root/file/clickatell.log");

	                     # Filter the HTTP protocol message and extract the XML document only
	                     $pos = strpos($response,"<?xml ");
	                     if ($pos===false)
	                     {
	                                $pos = strpos($response,"<?XML ");
	                     }
	                     $response = substr($response,$pos);
	                     $records[] = XML2Obj($response);
	                     

                 	 }
                 	 
 
             	}
             	if(!$this->simulate_only){
	             		if($isReplyMessage){
	       	         	     //close transaction
			                 $target_url = "http://".$this->service_host.$this->end_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
			             	 $fp = fopen($target_url,"r");
			                 fclose($fp);
						}
	            }
 	
                return $records;


        }
        function retrieveReply($start, $end)
        {

                 $target_url = "http://".$this->service_host.$this->data_action."?user=".$this->login."&password=".$this->password
                                ."&start_date=".urlencode($start)."&end_date=".urlencode($end);
                 $fp = fopen($target_url,"r");

                 if (!$fp)
                 {
                      # Server Down
                      return array();
                 }
                 $data = array();
                 $row = 0;
                 while (($temp = fgetcsv($fp, 1000, ",")) !== FALSE) {
                         $num = count($temp);
                         for ($c=0; $c < $num; $c++) {
                              $data[$row][$c] = iconv( "UTF-8", "Big5", $temp[$c]);
                         }
                         $row++;
                 }
                 return $data;


                 /*
                 $content = "";
                 while (!feof($fp))
                 {
                         $content .= fgets($fp, 4096);
                 }
                 fclose($fp);



                 # Convert to Big5
                 $content = iconv( "UTF-8", "Big5", $content);
                 $lines = explode("\n",$content);
                 $data = array();
                 for ($i=0; $i<sizeof($lines); $i++)
                 {
                      $raw = explode(",",$lines[$i]);
                      $data[$i] = $raw;
                 }
                 return $data;
                 */
        }
        
        function retrieveMessageCount($start, $end)
		{
			$target_url = "http://".$this->service_host.$this->usage_action
				."?login_id=".$this->login
				."&password=".$this->password
				."&school_code=".$this->school_code
				."&start_date=".urlencode($start)
				."&end_date=".urlencode($end);
				
                 $fp = fopen($target_url,"r");

                 if (!$fp)
                 {
                      # Server Down
                      return 0;
                 }
                 
                 $data = array();
				while (!feof($fp)) 
				{
					$data[] = fgets($fp, 4096);
				}
				fclose($fp);
				
				list($scode, $c) = split(",",$data[1]);

				return trim($c);
		}


}


function XML2Obj ($xDoc)
{
         $data = $xDoc;
         $parser = xml_parser_create();
         xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
         xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
         xml_parse_into_struct($parser,$data,$values,$tags);
         xml_parser_free($parser);

         // loop through the structures
         foreach ($tags as $key=>$val)
         {
                  if ($key == "item")   # Specified in clickatell Document, each record is inside tag <msg>
                  {
                      $recordranges = $val;
                      // each contiguous pair of array entries are the
                      // lower and upper range for each record definition
                      for ($i=0; $i < count($recordranges); $i+=2)
                      {
                           $offset = $recordranges[$i] + 1;
                           $len = $recordranges[$i + 1] - $offset;
                           $tdb[] = parseRecord(array_slice($values, $offset, $len));
                      }
                  }
                  else
                  {
                      continue;
                  }
         }
         return $tdb;

}

# Parse the clickatellSMSRecord 
function parseRecord($mvalues) {
    for ($i=0; $i < count($mvalues); $i++)
        $mol[$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
    return new clickatellSMSRecord($mol);
}

# Data Structure only
class clickatellSMSRecord
{
      var $recipient;
      var $reference_id;
      var $message_code;

      function clickatellSMSRecord($aa)
      {
               foreach ($aa as $k=>$v)
                        $this->$k = $aa[$k];
      }
}

}        // End of directive
?>
