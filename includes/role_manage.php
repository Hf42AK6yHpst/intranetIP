<?
// using : 

/*
 * 2016-01-26 Kenneth
 * - modified Search_User(), sql improvement (remove dulipcates)
 * 2014-10-06 Bill
 *  - modified role(), to sort member list
 * 2011-09-08 Carlos
 *  - modified Create_Role(), pre-insert all target [All-Yes] for new role (requested in 2011-0831-0951-35073)
 * 2011-03-22 Kenneth Chung
 * 	- edit contructor of role_mange, edit the sql for performance turning
 * 2010-04-20 Ivan
 * 	- Get_Role_List(), only get approved user account now. (i.e. RecordStatus = 1 in INTRANET_USER)
 */

include_once('libdb.php');
include_once('libuser.php');
include_once('lib.php');

class role extends libdb {
	var	$RoleID;
	var	$RoleName;
	var	$RoleType;
	var	$DateInput;
	var	$InputBy;
	var	$DateModified;
	var	$ModifyBy;
	var	$RightList;
	var	$TargetList;
	var	$MemberList;

	function role($RoleID="",$LoadRight=false,$LoadTarget=false,$LoadMember=false) {
		global $Lang;
		parent::libdb();
		if ($RoleID != "") {
			$InputByNameField = getNameFieldByLang('u.');
			$ModifiedByNamefield = getNameFieldByLang('u1.');

			$sql = 'select
								r.RoleID,
								r.RoleName,
								r.DateInput,
								r.InputBy,
								r.DateModified,
								r.ModifyBy,
								'.$InputByNameField.' as InputByName,
								'.$ModifiedByNamefield.' as ModifiedByName
							From
								ROLE r
								LEFT JOIN
								INTRANET_USER u
								on r.ModifyBy = u.UserID
								LEFT JOIN
								INTRANET_USER u1
								on r.InputBy = u1.UserID
							Where
								r.RoleID = \''.$RoleID.'\'';
			$Result = $this->returnArray($sql);

			$this->RoleID = $Result[0]['RoleID'];
			$this->RoleName = $Result[0]['RoleName'];
			$this->DateInput = $Result[0]['DateInput'];
			$this->InputBy = $Result[0]['InputBy'];
			$this->DateModified = $Result[0]['DateModified'];
			$this->ModifyBy = $Result[0]['ModifyBy'];
			$this->InputByName = $Result[0]['InputByName'];
			$this->ModifiedByName = $Result[0]['ModifiedByName'];

			if ($LoadRight) {
				$sql = 'Select
		  						FunctionName,
		  						RightFlag
		  					From
		  						ROLE_RIGHT
		  					WHERE
		  						RoleID = \''.$this->RoleID.'\' 
		  					order by 
		  						FunctionName 
		  				 ';
		  	$this->RightList = $this->returnArray($sql,2);
			}

			if ($LoadTarget) {
				$sql = 'Select
		  						TargetName,
		  						RightFlag
		  					From
		  						ROLE_TARGET
		  					WHERE
		  						RoleID = \''.$this->RoleID.'\' 
		  					order by 
		  						TargetName 
		  				 ';
		  	$Result = $this->returnArray($sql,3);
		  	for ($i=0; $i< sizeof($Result); $i++) {
		  		//$this->TargetList[$Result[$i]['Identity']][$Result[$i]['TargetName']] = $Result[$i];
		  		$this->TargetList[$Result[$i]['TargetName']] = $Result[$i];
		  	}
			}

			if ($LoadMember) {
				$NameField = getNameFieldByLang('u.');
				$ParentNameField = getParentNameWithStudentInfo("r.","u.","",1);
				$StudentNameField = getNameFieldWithClassNumberByLang("u.");
				
//				if(is_array($SortingArray) && count($SortingArray) > 0){
//					$sortingType = $SortingArray[1]? '' : 'DESC';
//					$columnArray = array(1=>'u.ClassName '.$sortingType.', u.ClassNumber, u.RecordType', 2=>'u.ClassNumber '.$sortingType.', u.RecordType, u.ClassName', 3=>'u.RecordType '.$sortingType.', u.ClassName, u.ClassNumber');
//					$orderSql = $columnArray[$SortingArray[0]];
//					$orderSql .= ', u.EnglishName, rm.UserID'; $this->sql=$orderSql;
//				} else {
//					$orderSql = 'u.RecordType, u.ClassName, u.ClassNumber, u.EnglishName, rm.UserID';
//				}

				$sql = 'Select
									rm.UserID,
									'.$NameField.' as MemberName,
									rm.IdentityType as UserType,
									u.RecordType, 
									'.$ParentNameField.' as ParentMemberName, 
									'.$StudentNameField.' as StudentMemberName 
								From
									ROLE_MEMBER as rm
									inner join
									INTRANET_USER as u
									on rm.UserID = u.UserID and rm.RoleID = \''.$this->RoleID.'\' and u.RecordStatus = \'1\' 
									LEFT JOIN 
									INTRANET_PARENTRELATION as pr 
									on u.UserID = pr.ParentID 
									LEFT JOIN 
									INTRANET_USER as r 
									on pr.StudentID = r.UserID 
								Order by
									u.RecordType, u.ClassName, u.ClassNumber, u.EnglishName, rm.UserID
							';
				//debug_r($sql);
				$TempMemberList = $this->returnArray($sql);

				for ($i=0; $i< sizeof($TempMemberList); $i++) {
					if ($TempMemberList[$i]['UserID'] != $TempMemberList[$i-1]['UserID'])
						$this->MemberList[] = $TempMemberList[$i];
					else {
						$this->MemberList[(sizeof($this->MemberList)-1)]['ClassTitleB5'] .= ', '.$TempMemberList[$i]['ClassTitleB5'];
						$this->MemberList[(sizeof($this->MemberList)-1)]['ClassTitleEN'] .= ', '.$TempMemberList[$i]['ClassTitleEN'];
					}
				}
			}
		}
	}
}

class role_manage extends libdb {
	function role_manage() {
		parent::libdb();
		global $HasCheckedUpdatedUserRoleInAdminConsole;
		
		if ($HasCheckedUpdatedUserRoleInAdminConsole == false)
		{
			### IP25 DM #1333
			### 20/4 Elies: Change staff to non-teaching staff, identity cannot update to non-teaching too
			// check any updated UserType when loading the page
			/*$sql = 'Select 
							rm.UserID,
							rm.IdentityType,
							TmpTypeTable.CurIdentityType
					From
						 	ROLE_MEMBER as rm
							Inner Join
							(
								Select
										u.UserID,
										CASE
											WHEN u.RecordType = \'2\' then \'Student\'
											WHEN u.RecordType = \'3\' then \'Parent\'
											WHEN u.RecordType = \'1\' and u.Teaching = \'1\' then \'Teaching\'
											WHEN u.RecordType = \'1\' and (u.Teaching <> \'1\' or u.Teaching IS NULL) then \'NonTeaching\'
											ELSE \'Alumni\'
										END as CurIdentityType
								From
										INTRANET_USER as u
								Where
										u.RecordStatus = 1
							) as TmpTypeTable
							On
							rm.UserID = TmpTypeTable.UserID
					Where
							rm.IdentityType != TmpTypeTable.CurIdentityType
					Group By
							rm.UserID
					';
			*/
			$sql = 'Select 
							rm.UserID,
							rm.IdentityType,
							CASE
                WHEN u.RecordType = \'2\' then \'Student\'
                WHEN u.RecordType = \'3\' then \'Parent\'
                WHEN u.RecordType = \'1\' and u.Teaching = \'1\' then \'Teaching\'
                WHEN u.RecordType = \'1\' and (u.Teaching <> \'1\' or u.Teaching IS NULL) then \'NonTeaching\'
                ELSE \'Alumni\'
            END as CurIdentityType
					From
						 	ROLE_MEMBER as rm
							Inner Join
            INTRANET_USER as u 
							On
							rm.UserID = u.UserID 
              and 
              u.RecordStatus = 1
              Group By
							rm.UserID
					having
							rm.IdentityType != CurIdentityType';
			$UpdateUserArr = $this->returnArray($sql);
			$numOfUser = count($UpdateUserArr);
			$ResultArr = array();
			for ($i=0; $i<$numOfUser; $i++)
			{
				$thisUserID = $UpdateUserArr[$i]['UserID'];
				$thisCurUserType = $UpdateUserArr[$i]['CurIdentityType'];
				
				$sql = "Update ROLE_MEMBER Set IdentityType = '$thisCurUserType' Where UserID = '$thisUserID'";
				$ResultArr[$thisUserID] = $this->db_db_query($sql);
			}
		}		
		$HasCheckedUpdatedUserRoleInAdminConsole = true;
	}

	function Get_Avaliable_User_List($AddUserID,$IdentityType,$YearClassID,$ParentStudentID,$RoleID) {
		$RoleMemberIDs = $this->Get_Role_Member_List($RoleID);
		if (sizeof($RoleMemberIDs) > 0) {
			$RoleMemberIDs = implode(',',$RoleMemberIDs);
			$RoleCondition = '-1,'.$RoleMemberIDs;
		}
		else
			$RoleCondition = '-1';

		switch($IdentityType) {
			case "Teaching":
				$NameField = getNameFieldByLang('u.');
				$Condition = ' u.RecordStatus=\'1\' and u.RecordType = \'1\' and u.Teaching = \'1\' ';
				break;
			case "NonTeaching":
				$NameField = getNameFieldByLang('u.');
				$Condition = ' u.RecordStatus=\'1\' and u.RecordType = \'1\' and (u.Teaching <> \'1\' or u.Teaching IS NULL) ';
				break;
			case "Student":
				$NameField = getNameFieldWithClassNumberByLang('u.');
				$Condition = ' u.RecordStatus=\'1\' and u.RecordType = \'2\' ';
				break;
			case "Parent":
				$NameField = getNameFieldByLang('u.');
				$NameField1 = getNameFieldByLang('u1.');
				$Condition = ' u.RecordStatus=\'1\' and u.RecordType = \'3\' ';
				break;
			default:
				break;
		}

		if (sizeof($AddUserID) > 0) {
			//var_dump($AddUserID); die;
			$UserQuery = 'and u.UserID not in ('.implode(',',$AddUserID).') ';
		}

		// student
		if ($IdentityType == "Student") {
			$sql = 'select
							  u.UserID,
							  '.$NameField.' as Name
							From
								YEAR as y
								inner join
							  YEAR_CLASS as yc
							  on y.YearId = yc.YearID and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\'
							  inner join
							  YEAR_CLASS_USER as ycu
							  on yc.YearClassID = ycu.YearClassID ';
			if ($YearClassID != "") {
				$sql .= '	 and yc.YearClassID = \''.$YearClassID.'\'';
			}
			$sql .= ' inner join
							  INTRANET_USER as u
							  on ycu.UserID = u.UserID '.$UserQuery.'
							where
								u.UserID not in ('.$RoleCondition.')
							order by
								y.sequence asc, yc.sequence asc, ycu.ClassNumber asc
						';
			//echo $sql;
		}
		// Parent
		else if ($IdentityType == "Parent") {
			// Parent final selected
			if (trim($ParentStudentID) != "") {
				if ($ParentStudentID == "all") {
					$sql = 'select
									  u.UserID,
									  concat('.$NameField.',\'(\','.$NameField1.',\')\') as Name
									From
									  YEAR_CLASS as yc
									  inner join
									  YEAR_CLASS_USER as ycu
									  on yc.YearClassID = ycu.YearClassID and yc.YearClassID = \''.$YearClassID.'\'
									  inner join
									  INTRANET_USER as u1
									  on ycu.UserID = u1.UserID
									  inner join
									  INTRANET_PARENTRELATION as pr
									  on u1.UserID = pr.StudentID
									  inner join
									  INTRANET_USER as u
									  on pr.ParentID = u.UserID
									where
										u.RecordStatus = \'1\'
										'.$UserQuery.'
										and u.UserID not in ('.$RoleCondition.')
									';
				}
				else {
					$sql = 'select
									  u.UserID,
									  concat('.$NameField.',\'(\','.$NameField1.',\')\') as Name
									From
									  YEAR_CLASS as yc
									  inner join
									  YEAR_CLASS_USER as ycu
									  on yc.YearClassID = ycu.YearClassID and yc.YearClassID = \''.$YearClassID.'\' and ycu.UserID = \''.$ParentStudentID.'\'
									  inner join
									  INTRANET_USER as u1
									  on ycu.UserID = u1.UserID
									  inner join
									  INTRANET_PARENTRELATION as pr
									  on u1.UserID = pr.StudentID
									  inner join
									  INTRANET_USER as u
									  on pr.ParentID = u.UserID
									where
										u.RecordStatus = \'1\'
										'.$UserQuery.'
										and u.UserID not in ('.$RoleCondition.')
									';
				}
			}
			// get student list for parent select
			else if ($YearClassID != ""){
				$sql = 'select
								  u.UserID,
								  '.$NameField.' as Name
								From
								  YEAR_CLASS as yc
								  inner join
								  YEAR_CLASS_USER as ycu
								  on yc.YearClassID = ycu.YearClassID and yc.YearClassID = \''.$YearClassID.'\'
								  inner join
								  INTRANET_USER as u
								  on ycu.UserID = u.UserID
								where
									u.UserID not in ('.$RoleCondition.')
								order by
									ycu.ClassNumber
							';
				//echo $sql; die;
			}
			// get whole parent record set from db
			else {
				$sql = 'select
									u.UserID,
									concat('.$NameField.',\'(\','.$NameField1.',\')\') as Name
								From
									YEAR as y
									inner join
								  YEAR_CLASS as yc
								  on y.YearID = yc.YearID and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\'
								  inner join
								  YEAR_CLASS_USER as ycu
								  on yc.YearClassID = ycu.YearClassID
								  inner join
								  INTRANET_USER as u1
								  on ycu.UserID = u1.UserID
								  inner join
								  INTRANET_PARENTRELATION as pr
								  on u1.UserID = pr.StudentID
								  inner join
								  INTRANET_USER as u
								  on pr.ParentID = u.UserID
								where
									u.RecordStatus = \'1\'
									'.$UserQuery.'
									and u.UserID not in ('.$RoleCondition.')
								order by
									y.sequence, yc.sequence, ycu.ClassNumber asc
								';
			}
		}
		// teacher/ staff
		else {
			$sql = 'select
								u.UserID,
								'.$NameField.' as Name
							From
								INTRANET_USER as u
							where
								'.$Condition.'
								'.$UserQuery.'
								and u.UserID not in ('.$RoleCondition.')
							Order By
								u.EnglishName
						';
		}

		//echo $sql; die;
		return $this->returnArray($sql);
	}

	function Get_Role_List($GetMemberDistribution=true) {
		$sql = 'Select
						  r.RoleID,
						  r.RoleName';
		if ($GetMemberDistribution) {
			$sql .= '
						  ,
						  CASE
						    WHEN TeachingMemberCount IS NULL THEN \'0\'
						    ELSE TeachingMemberCount
						  END as TeachingMemberCount,
						  CASE
						    WHEN NonTeachingMemberCount IS NULL THEN \'0\'
						    ELSE NonTeachingMemberCount
						  END as NonTeachingMemberCount,
						  CASE
						    WHEN StudentMemberCount IS NULL THEN \'0\'
						    ELSE StudentMemberCount
						  END as StudentMemberCount,
						  CASE
						    WHEN ParentMemberCount IS NULL THEN \'0\'
						    ELSE ParentMemberCount
						  END as ParentMemberCount 
						  ';
		}
		$sql .= '
						From
						  ROLE as r ';
		if ($GetMemberDistribution) {
			$sql .= '
						  left join (
						  select
						    ROLE.RoleID,
						    count(1) as TeachingMemberCount
						  from
						    ROLE
						    inner join
						    ROLE_MEMBER
						    on ROLE.RoleID = ROLE_MEMBER.RoleID and ROLE_MEMBER.IdentityType = \'Teaching\'
						    inner join
						    INTRANET_USER
						    on ROLE_MEMBER.UserID = INTRANET_USER.UserID
						  where
							INTRANET_USER.RecordStatus = 1
						  group by
						    ROLE.RoleID
						  ) as TeachingStat
						  on r.RoleID = TeachingStat.RoleID
						  left join (
						  select
						    ROLE.RoleID,
						    count(1) as NonTeachingMemberCount
						  from
						    ROLE
						    inner join
						    ROLE_MEMBER on
						    ROLE.RoleID = ROLE_MEMBER.RoleID and ROLE_MEMBER.IdentityType = \'NonTeaching\'
						    inner join
						    INTRANET_USER
						    on ROLE_MEMBER.UserID = INTRANET_USER.UserID
						  where
							INTRANET_USER.RecordStatus = 1
						  group by
						    ROLE.RoleID
						  ) as NonTeachingStat
						  on r.RoleID = NonTeachingStat.RoleID
						  left join (
						  select
						    ROLE.RoleID,
						    count(1) as StudentMemberCount
						  from
						    ROLE
						    inner join
						    ROLE_MEMBER on
						    ROLE.RoleID = ROLE_MEMBER.RoleID and ROLE_MEMBER.IdentityType = \'Student\'
						    inner join
						    INTRANET_USER
						    on ROLE_MEMBER.UserID = INTRANET_USER.UserID
						  where
							INTRANET_USER.RecordStatus = 1
						  group by
						    ROLE.RoleID
						  ) as StudentStat
						  on r.RoleID = StudentStat.RoleID
						  left join (
						  select
						    ROLE.RoleID,
						    count(1) as ParentMemberCount
						  from
						    ROLE
						    inner join
						    ROLE_MEMBER on
						    ROLE.RoleID = ROLE_MEMBER.RoleID and ROLE_MEMBER.IdentityType = \'Parent\'
						    inner join
						    INTRANET_USER
						    on ROLE_MEMBER.UserID = INTRANET_USER.UserID
						  where
							INTRANET_USER.RecordStatus = 1
						  group by
						    ROLE.RoleID
						  ) as ParentStat
						  on r.RoleID = ParentStat.RoleID ';
		}
		$sql .= '
						Order by
						  r.RoleName';
		//echo '<pre>'.$sql.'</pre>'; die;
		return $this->returnArray($sql);
	}

	function Check_Role_Name($RoleName,$RoleID="") {
		$sql = 'select
							count(1)
						from
							ROLE
						where
							RoleName = \''.$this->Get_Safe_Sql_Query($RoleName).'\'
							and
							RoleID <> \''.$RoleID.'\'';
		$Result = $this->returnVector($sql);
		//echo $sql; die;
		return ($Result[0] == 0);
	}

	function Create_Role($RoleName) {
		$sql = 'insert into ROLE (
							RoleName,
							DateInput,
							InputBy
						)
						value (
							\''.$this->Get_Safe_Sql_Query($RoleName).'\',
							NOW(),
							\''.$_SESSION['UserID'].'\'
						)';
		$Result = $this->db_db_query($sql);

		if ($Result){
			$RoleID = $this->db_insert_id();
			# Default pre-insert all role target [All-Yes] for new role
			$this->Save_Role_Right_Target($RoleID,"",array("All-Yes"),"Target");
		}else
			$RoleID = false;

		return $RoleID;
	}

	function Rename_Role($RoleID,$RoleName) {
		$sql = 'update ROLE set
							RoleName = \''.$this->Get_Safe_Sql_Query($RoleName).'\',
							ModifyBy = \''.$_SESSION['UserID'].'\'
						where
							RoleID = \''.$RoleID.'\'';
		//echo $sql; die;
		return $this->db_db_query($sql);
	}

	function Add_Role_Member($RoleID,$TargetUserID) {
		/*$sql = 'delete from ROLE_MEMBER where RoleID = \''.$RoleID.'\'';
		$Result['ClearOldMembers'] = $this->db_db_query($sql);*/
		if (sizeof($TargetUserID) > 0) {
			$TargetUserID = array_values(array_unique($TargetUserID));
			for ($i=0; $i< sizeof($TargetUserID); $i++) {
				if ($TargetUserID != "") {
					$sql = 'insert into ROLE_MEMBER (
										RoleID,
										UserID,
										IdentityType,
										DateInput,
										InputBy
									)
									value (
										\''.$RoleID.'\',
										\''.$TargetUserID[$i].'\',
										\''.$this->Get_Identity_Type($TargetUserID[$i]).'\',
										Now(),
										\''.$_SESSION['UserID'].'\'
									)';
					//debug_r($sql);
					$Result['InsertRoleMember:'.$TargetUserID[$i]] = $this->db_db_query($sql);
				}
			}

			//debug_r($Result);
			return (!in_array(false,$Result));
		}
		else {
			return false;
		}
	}

	function Remove_Role($RoleID) {
		$sql = 'delete from ROLE_RIGHT where RoleID = \''.$RoleID.'\'';
		$Result['DeleteRight'] = $this->db_db_query($sql);

		$sql = 'delete from ROLE_TARGET where RoleID = \''.$RoleID.'\'';
		$Result['DeleteTarget'] = $this->db_db_query($sql);

		$sql = 'delete from ROLE_MEMBER where RoleID = \''.$RoleID.'\'';
		$Result['DeleteMember'] = $this->db_db_query($sql);

		$sql = 'delete from ROLE where RoleID = \''.$RoleID.'\'';
		$Result['DeleteROLE'] = $this->db_db_query($sql);

		return (!in_array(false,$Result));
	}

	function Remove_Role_Member($RoleID,$TargetUserID) {
		if (sizeof($TargetUserID) > 0) {
			$UserQuery = implode(',',$TargetUserID);

			$sql = 'delete from ROLE_MEMBER
							where
								RoleID = \''.$RoleID.'\'
								and
								UserID in ('.$UserQuery.')';

			return $this->db_db_query($sql);
		}
		else
			return false;
	}

	function Save_Role_Right_Target($RoleID,$RoleRight,$RoleTarget,$RightOrTarget) {


		if ($RightOrTarget == "Right") {
			// delete old role right
			$sql = 'delete from ROLE_RIGHT where RoleID = \''.$RoleID.'\'';
			$Result['DeleteRoleRight'] = $this->db_db_query($sql);

			// insert new role right selection
			for ($i=0; $i< sizeof($RoleRight); $i++) {
				$sql = 'insert into ROLE_RIGHT (RoleID,FunctionName,RightFlag,DateInput,InputBy)
								values (\''.$RoleID.'\',\''.$RoleRight[$i].'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$Result['InsertRoleRight:'.$RoleRight[$i]] = $this->db_db_query($sql);
			}
		}
		else {
			// delete old role target
			$sql = 'delete from ROLE_TARGET where RoleID = \''.$RoleID.'\'';
			$Result['DeleteRoleTarget'] = $this->db_db_query($sql);

			/*foreach ($RoleTarget as $Identity => $TargetList) {
				// insert new role target selection
				for ($i=0; $i< sizeof($TargetList); $i++) {
					$sql = 'insert into ROLE_TARGET (RoleID,TargetName,RightFlag,DateInput,InputBy,Identity)
									values (\''.$RoleID.'\',\''.$TargetList[$i].'\',1,NOW(),\''.$_SESSION['UserID'].'\',\''.$Identity.'\')';
					$Result['InsertRoleTarget:'.$Identity.'-'.$TargetList[$i]] = $this->db_db_query($sql);
				}
			}*/

			for ($i=0; $i< sizeof($RoleTarget); $i++) {
				$sql = 'insert into ROLE_TARGET (RoleID,TargetName,RightFlag,DateInput,InputBy)
								values (\''.$RoleID.'\',\''.$RoleTarget[$i].'\',1,NOW(),\''.$_SESSION['UserID'].'\')';
				$Result['InsertRoleTarget:'.$TargetList[$i]] = $this->db_db_query($sql);
			}
		}

		$sql = 'update ROLE set
							ModifyBy = \''.$_SESSION['UserID'].'\',
							DateModified = NOW()
						where
							RoleID = \''.$RoleID.'\'';
		$Result['UpdateLastUpdateInfo'] = $this->db_db_query($sql);

		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>';
		die;*/
		return (!in_array(false,$Result));
	}

	function Search_User($SearchValue,$AddUserID) {
		$CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
		$CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
		$NameField = getNameFieldByLang('u.');
		$UserCondition = (trim($AddUserID) == "")? "": ' and u.UserID not in ('.$AddUserID.') ';
		$SearchValue = $this->Get_Safe_Sql_Query($SearchValue);

		$this->With_Nolock_Trans();
		$sql = 'select 
              u.UserID,
              '.$NameField.' as Name,
						  CASE
								WHEN yc.ClassTitleEN IS NULL THEN "NA"
								ELSE yc.ClassTitleEN
							END as ClassTitleEN,
							CASE
								WHEN yc.ClassTitleB5 IS NULL THEN "NA"
								ELSE yc.ClassTitleB5
							END as ClassTitleB5,
							CASE
								WHEN ycu.ClassNumber IS NULL THEN "NA"
								ELSE ycu.ClassNumber
							END as ClassNumber,
							CASE
								WHEN u.RecordType = \'2\' then \'Student\'
								WHEN u.RecordType = \'3\' then \'Parent\'
								WHEN u.RecordType = \'1\' and u.Teaching = \'1\' then \'Teaching\'
								WHEN u.RecordType = \'1\' and (u.Teaching <> \'1\' or u.Teaching IS NULL)then \'Non Teaching\'
								ELSE \'Alumni\'
							END as IdentityType
					  from
							INTRANET_USER as u
							left join YEAR_CLASS_TEACHER as yct on u.UserID = yct.UserID
							left join YEAR_CLASS_USER as ycu on u.UserID = ycu.UserID
							left join YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID or yct.YearClassID = yc.YearClassID
							left join ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID and ay.AcademicYearID = \''.$CurrentAcademicYearID.'\'
					  where
					  	u.RecordStatus = \'1\'
              '.$UserCondition.'
					  	and (
					  		u.UserLogin like \'%'.$SearchValue.'%\'
					  		OR
					  		u.EnglishName like \'%'.$SearchValue.'%\'
					  		OR
					  		u.UserEmail like \'%'.$SearchValue.'%\'
					  		OR
					  		u.ChineseName like \'%'.$SearchValue.'%\'
					  	)
					  group by u.userid			
					  	';
		//debug_pr($sql);
		//echo $sql; die;
		return $this->returnArray($sql);
	}

	function Get_Identity_Type($TargetUserID) {
		$sql = 'select
							CASE
								WHEN u.RecordType = \'2\' then \'Student\'
								WHEN u.RecordType = \'3\' then \'Parent\'
								WHEN u.RecordType = \'1\' and u.Teaching = \'1\' then \'Teaching\'
								WHEN u.RecordType = \'1\' and (u.Teaching <> \'1\' or u.Teaching IS NULL) then \'NonTeaching\'
								ELSE \'Alumni\'
							END as IdentityType
						From
							INTRANET_USER as u
						Where
							UserID = \''.$TargetUserID.'\'';
		$Result = $this->returnVector($sql);
		return $Result[0];
	}

	function Get_Role_Member_List($RoleID) {
		$sql = 'select
							UserID
						from
							ROLE_MEMBER
						where
							RoleID = \''.$RoleID.'\'';
		return $this->returnVector($sql);
	}

	function Get_Member_Class_Info($MemberID,$UserType) {
		if ($UserType == 2) // student
			$TableName = ' YEAR_CLASS_USER ';
		else
			$TableName = ' YEAR_CLASS_TEACHER ';

		$sql = 'select
							ClassTitleEN,
							ClassTitleB5
					 ';
		if ($UserType == 2)
			$sql .= ', ClassNumber ';
		else
			$sql .= ', \'NA\' as ClassNumber ';
		$sql .= 'from
							'.$TableName.' as a
							inner join
							YEAR_CLASS as yc
							on a.UserID = \''.$MemberID.'\' and yc.AcademicYearID = \''.GET_CURRENT_ACADEMIC_YEAR_ID().'\' and a.YearClassID = yc.YearClassID
							inner join
							YEAR as y
							on yc.YearID = y.YearID
						order by
							y.sequence, yc.sequence
					 ';
		$ClassInfo = $this->returnArray($sql);

		if (sizeof($ClassInfo) == 0) {
			return array("ClassTitleEN" => "NA", "ClassTitleB5" => "NA", "ClassNumber" => "NA");
		}
		else {
			for ($i=0; $i< sizeof($ClassInfo); $i++) {
				if ($i==0) {
					$Return["ClassTitleEN"] = $ClassInfo[$i]["ClassTitleEN"];
					$Return["ClassTitleB5"] = $ClassInfo[$i]["ClassTitleB5"];
					$Return["ClassNumber"] = $ClassInfo[$i]["ClassNumber"];
				}
				else {
					$Return["ClassTitleEN"] .= ", ".$ClassInfo[$i]["ClassTitleEN"];
					$Return["ClassTitleB5"] .= ", ".$ClassInfo[$i]["ClassTitleB5"];
				}
			}

			return $Return;
		}
	}
}
?>