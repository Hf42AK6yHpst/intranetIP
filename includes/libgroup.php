<?php
## Using By : 

/********************** Change Log ***********************/
#	Date:	2016-03-02 (Kenneth)
#			modified returnAllGroupIDAndName(), add params: $RecordType and $AcademicYearID
#			deploy: ip2.5.7.3.1
#
#	Date:	2015-04-28 (Omas)
#			modified isAccessTool() avoid substr when it is array preventing error
#
#	Date:	2014-11-04 (Omas)
#			new function returnGroupName(), return GroupName Array
#
#	Date:	2014-06-10 (Bill)
#			modified returnGroupUser() and displayGroupUsers_ip20(), return result of admin status filtering 
#
#	Date:	2014-03-17 (YatWoon)
#			modified returnGroupUsedStorageQuota(), sum() with NULL result will return null and failed to display the total [Case#L60038]
#			
#	Date:	2013-03-28 (YatWoon)
#			modified displayGroupSelection()
#
#	Date:	2012-01-13 (Henry Chow)
#			modified returnStaffListByGroup(), add group by in order to distinct user
#
#	Date:	2011-10-31 YatWoon
#			Add Group Code for edit with flag $sys_custom['eEnrolment']['TempClubCode'] (until the group code as general deployment) # for client Heung To Middle School at the moment
#
#	Date:	2011-09-12	[Marcus]
#			modified Copy_Group_And_Member(), copy all settings of the group 
#
#	Date:	2011-06-21	[Henry Chow]
#			add returnStaffListByGroup(), return teacher list of group (used in "Digital Archive")
#
#	Date:	2011-05-17	[YatWoon]
#			update returnGroupsByCategory(), display title name with lang
#
#	Date:	2010-12-21 [YatWoon]
#			update returnGroup(), add AllowDeleteOthersAnnouncement option (for eComm announcement settings)
#
#	Date	: 2010-12-13 [YatWoon]
#				update LoadGroupData(), returnGroup(), returnAllGroupIDAndName(), Copy_Group_And_Member(), getSelectGroups(), getSelectGroupsByAdminRight(), getSelectGroupsByTool(), getSelectGroupsByAdminRightsArray(), add "TitleChinese"
#				add var $this->TitleDisplay, for display title name according to the UI lang
#
#	Date	: 2010-11-24 [Marcus]
#				update returnAvailableGroupAdminFunction, default hide photoAlbum
#
#	Date	: 2010-11-22 [YatWoon]
#				update returnGroupUser4Display(), add user type checking
#
#	Date	: 2010-10-27 [Marcus]
#				fixed LoadGroupData(), several variables has to be reset for each time to reinit libgroup.
#
#	Date	: 2010-10-12 [Yuen]
#				modified libgroup() to preload multiple group data using one query
#				added AssignToBatchGroups() to set user data to $this->BatchGroups in associative array using group_id
#				added LoadGroupData() to load group data to current object according to group_id
#
#	Date:		2010-10-27 Ivan
#				modified returnAvailableGroupAdminFunction to change wordings and add remarks for editing Member List admin right
#
#   Date:		2010-09-24 Marcus
#				modified returnGroupUser , move eEnrolment special handling inside the function.
#
#   Date:		2010-09-06 Marcus
#				modified returnAvailableGroupAdminFunction, display Forum, Photo, File even if the functions are disabled.
#
#   Date:		2010-09-06 Marcus
#				modified isAccessLinks, isAccessFiles, isAccessPhotoAlbum
#
#   Date:		2010-09-01 Marcus
#				modified Get_Group_Mgmt_Copy_UI, allow user to copy class groups which is not linked to class
#
#   Date:		2010-08-30 Marcus
#				modified functions, if FunctionAccess = 0 , also can access all functions
#
#   Date:		2010-08-11 Marcus
#				modified returnGroupUser , check YearClassID, ClassNumber of YEAR_CLASS_USER instead of INTRANET_USER
#
#   Date:		2010-07-14 Marcus
#				Added functions Group_Adding_Additional_Task, Copy_Group_And_Member, Get_Group_Mgmt_Copy_UI For Group Copying
#
#   Date:		2010-06-08 Max
#				Modified UPDATE_GROUP_TO_DB to fix the bug when return the function access from some value to ALL will set the Function Access to zero
#
#
#   Date:		2010-04-16 Marcus
#				Add param $cond to displayGroupUsersEmailOption
#
#	Date:		2010-04-09	[Max]
#				Added object variable to store original function access value
#				Modified setFunctionAccess() to set object's FunctionAccess in binary value
#				Modified UPDATE_GROUP_TO_DB() to convert object's FunctionAccess to base10 before update DB
#
#	Date:		2010-04-09	[YatWoon]
#				update returnRoleType(), add "order by" to the query
#
#   Date:		20100114 Marcus
#				add param foreComm to getSelectOtherGroups, getSelectGroups for hiding groups in ecomm
#
#	Date	:	2010-01-06 Max (200912311437)
#				add setPublicStatus(), setFunctionAccess(), UPDATE_GROUP_TO_DB()
#
#	Date	:	2010-01-06 [YatWoon]
#				add returnGroupFunctionAccessValue()
#				Change the Function access value from binary 
#
#	Date	:	2010-01-04 : Max (200912311437)
#	Details	:	in function [returnAvailableGroupAdminFunction()] comment $rights[] = ""; for error fix
#				in function [hasAdminBulletin()] and [hasAdminFiles()], depress the checking of group access rights that use $this->isAccessBulletin() and $this->isAccessFiles() corresspondantly
#				Modifiy constructor and function[returnGroup()] to support a new field [AllowedWebsite] in DB
#	
# 	Date	:	2009-12-31 [YatWoon]
#	Details	:	modified getSelectOtherGroups(), getSelectGroups()
#				Add academic year checking
#
# 	Date	:	2009-12-17 [Yuen]
#	Details	:	modified displayStorage() to round off the storage to 2 digits after integer
#
#
/******************* End Of Change Log *******************/

class libgroup extends libdb {

        var $Group;
        var $GroupID;
        var $Title;
        var $TitleChinese;		# 2010-12-13 YatWoon
        var $TitleDisplay;		# 2010-12-13 YatWoon
        var $Description;
        var $RecordType;
        var $RecordStatus;
        var $DateInput;
        var $DateModified;
        var $DateModifiedTimeStamp;
        var $URL;
        var $StorageQuota;
        var $AnnounceAllowed;
        var $no_of_rows;
        var $page_size;
        var $total;
        var $pagename;
        var $pagePrev;
        var $pageNext;
        var $pageNo;
        var $new_navigation;
        var $order;
        var $field;
        var $field_array;
        var $keyword;
        var $questionBankExist;
        var $FunctionAccess;
        var $isAdmin;
        var $AdminAccessRight;
        var $CurrentUserID;
        var $form_name;
		var $PublicStatus;
		var $GroupLogoLink;
		var $IndexAnnounceNo;
		var $SharingLatestNo;
		var $needApproval;
		var $DefaultViewMode;
		var $AllowedImageTypes;
		var $AllowedFileTypes;
		var $AllowedVideoTypes;
		var $CalID;
		var $CalDisplayIndex;
		var $CalPublicStatus;
		var $AllowedWebsite;
		var $DisplayInCommunity;
		var $FunctionAccessOriginal;
		var $BatchGroups;
		var $AdminRightArray;
		var $AllowDeleteOthersAnnouncement;
		var $GroupCode;
		
		# DEFINE BIT POSITION
		# * please do not use negative
		const FA_TIMETABLE_BIT_POS		= 0;
		const FA_CHAT_BIT_POS			= 1;
		const FA_BULLETIN_BIT_POS		= 2;
		const FA_SHARED_LINKS_BIT_POS	= 3;
		const FA_SHARED_FILES_BIT_POS	= 4;
		const FA_QUESTION_BANK_BIT_POS	= 5;
		const FA_PHOTO_ALBUM_BIT_POS	= 6;
		const FA_SURVEY_BIT_POS			= 7;
		
		# DEFINE ENABLE AND DISABLE VALUE
		const ENABLE				= true;
		const DISABLE				= false;

        function libgroup($GroupID="", $GroupsIDs="", $PreloadAllGroupOfUser=""){
                global $page_size, $list_total, $list_page, $list_prev, $list_next, $pageNo,$list_sortby;
                $this->page_size = $page_size; #5*$page_size;
                $this->pageNo = (trim($pageNo) == "") ? 1 : $pageNo;
                $this->total = (trim($list_total) == "") ? "Total" : $list_total;
                $this->pagename = (trim($list_page) == "") ? "Page" : $list_page;
                $this->pagePrev = (trim($list_prev) == "") ? "Prev" : $list_prev;
                $this->pageNext = (trim($list_next) == "") ? "Next" : $list_next;
                $this->new_navigation = false;
                $this->sortby = (trim($list_sortby) == "") ? "Sort By" : $list_sortby;
                $this->isAdmin = false;
                $this->AdminAccessRight = "NULL";
                $this->CurrentUserID = "";
                $this->form_name = "form1";
                $this->libdb();
                
                if($PreloadAllGroupOfUser==1)
                	$GroupsIDs = implode(",",$this->getGroupsID(1));
                	
                $GroupResult = $this->returnGroup($GroupID, $GroupsIDs);
                $this->AssignToBatchGroups($GroupResult);
                
    			if($GroupID!='')
					$this->LoadGroupData($GroupID);
				else
                	$this->LoadGroupData($GroupResult[0][0]);
                /*
                if($GroupID<>""){
                        $this->Group = $this->returnGroup($GroupID);
                        $this->GroupID = $this->Group[0][0];
                        $this->Title = $this->Group[0][1];
                        $this->Description = $this->Group[0][2];
                        $this->RecordType = $this->Group[0][3];
                        $this->RecordStatus = $this->Group[0][4];
                        $this->DateInput = $this->Group[0][5];
                        $this->DateModified = $this->Group[0][6];
                        $this->DateModifiedTimeStamp = $this->Group[0][7];
                        $this->URL = $this->Group[0][8];
                        $this->StorageQuota = $this->Group[0][9];
                        if ($this->StorageQuota == "") $this->StorageQuota = 5;
                        $this->AnnounceAllowed = $this->Group[0][10];
                        $this->FunctionAccess = $this->Group[0][11];
                        $this->PublicStatus = $this->Group[0][12];
                        $this->GroupLogoLink = $this->Group[0][13];
                        $this->IndexAnnounceNo = $this->Group[0][14];
                        $this->SharingLatestNo = $this->Group[0][15];
                        $this->needApproval = $this->Group[0][16];
                        $this->DefaultViewMode = $this->Group[0][17];
                        $this->AllowedImageTypes = $this->Group[0][18];
                        $this->AllowedFileTypes = $this->Group[0][19];
                        $this->AllowedVideoTypes = $this->Group[0][20];
                        $this->CalID = $this->Group[0][21];
                        $this->CalDisplayIndex = $this->Group[0][22];
                        $this->CalPublicStatus = $this->Group[0][23];
                        $this->AllowedWebsite = $this->Group[0][24];
                        $this->DisplayInCommunity = $this->Group[0][25];
                        $this->FunctionAccessOriginal = $this->Group[0]["FunctionAccessOriginal"];
                        $this->AcademicYearID = $this->Group[0]["AcademicYearID"];
                        
						$AlbumAccess = $this->AllowedImageTypes!="DISABLED" || $this->AllowedVideoTypes!="DISABLED";
						$FileAccess = $this->AllowedFileTypes!="DISABLED";
						$WebsiteAccess = $this->AllowedWebsite==1;
						
						$this->setFunctionAccess($AlbumAccess,6);
						$this->setFunctionAccess($FileAccess,4);
						$this->setFunctionAccess($WebsiteAccess,3);
                }
                */
                
        }
        
        # added on 2010-10-12
        function LoadGroupData($gid) {
        	
        	global $UserID, $intranet_session_language;
        	
        	if (is_array($this->BatchGroups[$gid]) && sizeof($this->BatchGroups[$gid])>0)
        	{
        		$groupobj = $this->BatchGroups[$gid];        	
        	} else
        	{
        		$groupArr = $this->returnGroup($gid);
        		$groupobj = $groupArr[0];
        	}
        	
            $this->new_navigation = false;
            $this->isAdmin = false;
            $this->AdminAccessRight = "NULL";
            $this->CurrentUserID = "";
        	
            $this->GroupID = $groupobj[0];
            $this->Title = $groupobj[1];
            $this->Description = $groupobj[2];
            $this->RecordType = $groupobj[3];
            $this->RecordStatus = $groupobj[4];
            $this->DateInput = $groupobj[5];
            $this->DateModified = $groupobj[6];
            $this->DateModifiedTimeStamp = $groupobj[7];
            $this->URL = $groupobj[8];
            $this->StorageQuota = $groupobj[9];
            if ($this->StorageQuota == "") 
            	$this->StorageQuota = 5;
            $this->AnnounceAllowed = $groupobj[10];
            $this->FunctionAccess = $groupobj[11];
            $this->PublicStatus = $groupobj[12];
            $this->GroupLogoLink = $groupobj[13];
            $this->IndexAnnounceNo = $groupobj[14];
            $this->SharingLatestNo = $groupobj[15];
            $this->needApproval = $groupobj[16];
            $this->DefaultViewMode = $groupobj[17];
            $this->AllowedImageTypes = $groupobj[18];
            $this->AllowedFileTypes = $groupobj[19];
            $this->AllowedVideoTypes = $groupobj[20];
            $this->CalID = $groupobj[21];
            $this->CalDisplayIndex = $groupobj[22];
            $this->CalPublicStatus = $groupobj[23];
            $this->AllowedWebsite = $groupobj[24];
            $this->DisplayInCommunity = $groupobj[25];
            $this->FunctionAccessOriginal = $groupobj["FunctionAccessOriginal"];
            $this->AcademicYearID = $groupobj["AcademicYearID"];
            $this->TitleChinese = $groupobj["TitleChinese"];
            $this->TitleDisplay = $intranet_session_language=="en" ? $this->Title : $this->TitleChinese;
			$this->GroupCode = $groupobj['GroupCode'];
                        
            $this->AllowDeleteOthersAnnouncement = $groupobj["AllowDeleteOthersAnnouncement"];
            
            
			$AlbumAccess = $this->AllowedImageTypes!="DISABLED" || $this->AllowedVideoTypes!="DISABLED";
			$FileAccess = $this->AllowedFileTypes!="DISABLED";
			$WebsiteAccess = $this->AllowedWebsite==1;
			
			$this->setFunctionAccess($AlbumAccess,6);
			$this->setFunctionAccess($FileAccess,4);
			$this->setFunctionAccess($WebsiteAccess,3);
			
			$this->retrieveAdminRights($UserID);
        }
        
        function AssignToBatchGroups($BatchGroups)
        {
        	for ($i=0; $i<sizeof($BatchGroups); $i++)
        	{
        		# store using UserID
        		$this->BatchGroups[$BatchGroups[$i][0]] = $BatchGroups[$i];
        	}
        }

        function returnGroup($GroupID="", $GroupsIDs=""){

			if ($GroupID=="" && $GroupsIDs=="")
			{
				return false;
			}

			$sql = "SELECT GroupID, Title, Description, RecordType, RecordStatus, DateInput, DateModified, UNIX_TIMESTAMP(DateModified), URL, StorageQuota, AnnounceAllowed, IF(FunctionAccess IS NULL OR FunctionAccess = 0,'ALL',REVERSE(BIN(FunctionAccess))), PublicStatus, GroupLogoLink, IndexAnnounceNo, SharingLatestNo, needApproval, DefaultViewMode, AllowedImageTypes, AllowedFileTypes, AllowedVideoTypes, CalID, CalDisplayIndex, CalPublicStatus, AllowedWebsite, DisplayInCommunity, FunctionAccess AS FunctionAccessOriginal, AcademicYearID, TitleChinese, AllowDeleteOthersAnnouncement, GroupCode FROM INTRANET_GROUP WHERE ";
        	
        	$sql.= ($GroupsIDs!="") ? "GroupID in ($GroupsIDs)" : "GroupID ='$GroupID' ";
        	
			return $this->returnArray($sql);
        }
		
		function returnGroupName($AcademicYearID){
			$sql = "
					SELECT
						Title,
						TitleChinese
					FROM
						INTRANET_GROUP
					WHERE
						AcademicYearID = '".$AcademicYearID."' OR AcademicYearID IS NULL
						
					";
			return $this->returnResultSet($sql);
		}
        # ------------------------------------------------------------------------------------

        function returnCategoryName($gid="")
        {
                 if ($gid=="") $gid = $this->GroupID;
                 if ($gid=="") return "";
                 $sql = "SELECT CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID = '".$this->RecordType."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnAllCategoryName()
        {
                 $sql = "SELECT CategoryName FROM INTRANET_GROUP_CATEGORY";
                 $result = $this->returnVector($sql);
                 return $result;
        }
        function returnAllCategory($Other = '')
        {
      		 
                 $sql = "SELECT GroupCategoryID, CategoryName FROM INTRANET_GROUP_CATEGORY $Other";
                 $result = $this->returnArray($sql);
                 $returnAry=Array();
                 foreach((array)$result as $data)
                 {
                 	$returnAry[$data["GroupCategoryID"]] = 	$data["CategoryName"];
                 }

                 return $returnAry;
        }
        function returnMemberInfo ($mem_id)
        {
                 $sql = "SELECT b.Title, a.Performance
                         FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_ROLE as b
                         ON a.RoleID = b.RoleID WHERE UserID = '$mem_id' AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnArray($sql,2);
                 return $result[0];
        }

        // edited by Ivan (22 July 2008) added $conds to add condition to the query
       /* function returnGroupUser($special_order="", $conds="", $UserType=''){
                global $i_status_approved, $i_status_suspended, $i_status_pendinguser;
                
                $field  = " a.UserID, a.UserLogin, a.UserEmail, a.FirstName, a.LastName, ";
                $field .= "IF(a.RecordStatus='1','$i_status_approved',IF(a.RecordStatus='2','$i_status_pendinguser','$i_status_suspended')), ";
                $field .= "IFNULL(c.Title, '-'), a.UserPassword, a.URL, b.RecordType, a.ClassName, a.ClassNumber, a.EnglishName, a.ChineseName, b.Performance";
                if ($this->RecordType == 3)
                    $field .= ", IF(d.LeaderID IS NULL,0,1) ";
                $field .= ", b.CommentStudent";		# 20081016 move the "CommentStudent" to the end, otherwise, "isSubjectLeader" is count as wrong index
                $field .= ", IF((a.RecordType = 2 AND (yc.YearClassID IS NOT NULL AND ycu.ClassNumber IS NOT NULL AND TRIM(yc.YearClassID) <> '' AND TRIM(ycu.ClassNumber) <> '' )),'<span class=\"red\">^</span>','') AS Remarks";	
                $sql  = "SELECT $field FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID AND b.GroupID = '".$this->GroupID."' ";
                $sql .= "LEFT OUTER JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID ";
                if ($this->RecordType == 3)
                    $sql .= "LEFT OUTER JOIN INTRANET_SUBJECT_LEADER AS d ON a.UserID = d.UserID AND d.ClassID = ".$this->GroupID." ";
                $sql .= "LEFT JOIN YEAR_CLASS_USER AS ycu ON a.UserID = ycu.UserID ";
                $sql .= "LEFT JOIN YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."'";
                #$sql .= "WHERE b.GroupID = ".$this->GroupID;
                $sql .= "WHERE b.GroupID IS NOT NULL AND b.GroupID = ".$this->GroupID;
                $keyword = $this->keyword;
                $sql .= " AND  a.RecordStatus=1 ";
                $sql .= " AND (";

                // edited by Ivan (22 July 2008) modifying the SQL to add condition
                if (trim($conds)!=""){
	                $sql .= "(".$conds.") AND (";
                }

                $sql .= " (a.UserLogin LIKE '%$keyword%') OR
                         (a.UserEmail LIKE '%$keyword%') OR
                         (a.EnglishName LIKE '%$keyword%') OR
                         (a.ChineseName LIKE '%$keyword%') OR
                         (a.ClassName LIKE '%$keyword%') OR
                         (a.ClassNumber LIKE '%$keyword%') OR
                         (b.Performance LIKE '%$keyword%') )
                ";

                if (trim($conds)!=""){
	                $sql .= ")";
                }
				$sql .= " GROUP BY UserID ";
                $sql .= $special_order;

                $cols = ($this->RecordType==3? 16: 15);

                return $this->returnArray($sql,$cols);
        }*/

 		function returnGroupUser($special_order="", $conds="", $UserType='',$AcademicYearID='',$adminfilter=1){
                global $i_status_approved, $i_status_suspended, $i_status_pendinguser,$plugin,$PATH_WRT_ROOT;
                
//                $field  = "DISTINCT a.UserID, a.UserLogin, a.UserEmail, a.FirstName, a.LastName, ";
//                $field .= "IF(a.RecordStatus='1','$i_status_approved',IF(a.RecordStatus='2','$i_status_pendinguser','$i_status_suspended')), ";
//                $field .= "IFNULL(c.Title, '-'), a.UserPassword, a.URL, b.RecordType, a.ClassName, a.ClassNumber, a.EnglishName, a.ChineseName, b.Performance"; //$field .= "IFNULL(c.Title, '-'), a.UserPassword, a.URL, b.RecordType, a.ClassName, a.ClassNumber, a.EnglishName, a.ChineseName, b.Performance";
//                if ($this->RecordType == 3)
//                    $field .= ", IF(d.LeaderID IS NULL,0,1) ";
//                $field .= ", b.CommentStudent";		# 20081016 move the "CommentStudent" to the end, otherwise, "isSubjectLeader" is count as wrong index
//                $sql  = "SELECT $field FROM INTRANET_USER AS a LEFT JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID AND b.GroupID = '".$this->GroupID."' ";
//                $sql .= "LEFT JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID ";
//                if ($this->RecordType == 3)
//                    $sql .= "LEFT JOIN INTRANET_SUBJECT_LEADER AS d ON a.UserID = d.UserID AND d.ClassID = ".$this->GroupID." ";
////                $sql .= "LEFT JOIN YEAR_CLASS_USER AS ycu ON a.UserID = ycu.UserID ";
////                $sql .= "LEFT JOIN YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->AcademicYearID."'";
//                #$sql .= "WHERE b.GroupID = ".$this->GroupID;
//                $sql .= "WHERE b.GroupID IS NOT NULL AND b.GroupID = ".$this->GroupID;

				$AcademicYearID = $this->AcademicYearID?$this->AcademicYearID:$AcademicYearID;

				# special handling for enrolment group 
				if($this->RecordType==5 && $plugin['eEnrollment'])
				{
					include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
					
					$libenroll = new libclubsenrol();
					
					$GroupInfoArr = $libenroll->Get_Group_Info_By_GroupID($this->GroupID);
					
					if(!empty($GroupInfoArr))
					{
						$GroupClubType = $GroupInfoArr['ClubType'];
						if($GroupClubType=='S')
						{
							$TermInfoArr = getCurrentAcademicYearAndYearTerm();
							$curYearTermID = $TermInfoArr['YearTermID'];
							
							$InvolvedSem = $libenroll->Get_Group_Involoved_Semester($this->GroupID);
							if(empty($Semester)) 
								$Semester = $InvolvedSem[0];
							$termfilterbar .= $libenroll->Get_Term_Selection("Semester", '', $Semester, $OnChange='this.form.pageNo.value=1; this.form.submit()',1 ,0 ,0);
							$termfilterbar .= "<input type='hidden' name='filter' value='$filter'>";
							
							$EnrolGroupID = $libenroll->Get_Club_Of_Semester($this->GroupID, $Semester);
						}
						else
						{
							$EnrolGroupID = $libenroll->Get_Club_Of_Semester($this->GroupID, '');
						}
						
						$GroupUsersList = $libenroll->Get_Member_List($EnrolGroupID,"club",0);
						$GroupUsers = count($GroupUsersList);
						
						$EnrolIDCond = " AND EnrolGroupID = '$EnrolGroupID' "; 
					} 
				//	$EnrolIDForAdd = "&EnrolGroupID=".$EnrolGroupID; 
				}

				$sql .= "SELECT ";
						$sql .= "a.UserID, \n"; 
						$sql .= "a.UserLogin, \n"; 
						$sql .= "a.UserEmail, \n"; 
						$sql .= "a.FirstName, \n"; 
						$sql .= "a.LastName, \n"; 
						$sql .= "IF(a.RecordStatus='1','Approved',IF(a.RecordStatus='2','Pending','Suspended')), \n"; 
						$sql .= "IFNULL(c.Title, '-'), \n"; 
						$sql .= "a.UserPassword, \n"; 
						$sql .= "a.URL, \n"; 
						$sql .= "b.RecordType, \n"; 
						$sql .= "yc.ClassTitleEn, \n"; 
						$sql .= "ycu.ClassNumber, \n"; 
						$sql .= "a.EnglishName, \n"; 
						$sql .= "a.ChineseName, \n"; 
						$sql .= "b.Performance, \n";
						if ($this->RecordType == 3) 
							$sql .= "IF(d.LeaderID IS NULL,0,1) , \n"; 
						$sql .= "b.CommentStudent \n"; 
					$sql .= "FROM \n";
						$sql .= "YEAR_CLASS_USER ycu \n";
						$sql .= "INNER JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$AcademicYearID'\n";
						$sql .= "RIGHT JOIN INTRANET_USERGROUP b ON b.UserID = ycu.UserID \n";
						$sql .= "INNER JOIN INTRANET_USER a ON a.UserID = b.UserID \n";
						$sql .= "LEFT JOIN INTRANET_ROLE c ON c.RoleID = b.RoleID \n";
						if ($this->RecordType == 3) 
							$sql .= "LEFT JOIN INTRANET_SUBJECT_LEADER AS d ON a.UserID = d.UserID AND d.ClassID = '".$this->GroupID."' \n"; 
					$sql .= "WHERE \n";
						$sql .= "b.GroupID = ".$this->GroupID."\n";

                $keyword = $this->keyword;
                $sql .= " AND  a.RecordStatus IN(1,0)";
				if ($adminfilter == 3){$sql .= " AND b.RecordType = 'A'";}
				elseif ($adminfilter == 2){$sql .= " AND b.RecordType IS NULL";}
//                $sql .= " AND (a.RecordType <> 2 OR (yc.YearClassID IS NOT NULL AND ycu.ClassNumber IS NOT NULL AND TRIM(yc.YearClassID) <> '' AND TRIM(ycu.ClassNumber) <> '' )) ";
                $sql .= " AND (
						 (a.UserLogin LIKE '%$keyword%') OR
                         (a.UserEmail LIKE '%$keyword%') OR
                         (a.EnglishName LIKE '%$keyword%') OR
                         (a.ChineseName LIKE '%$keyword%') OR
                         (a.ClassName LIKE '%$keyword%') OR
                         (a.ClassNumber LIKE '%$keyword%') OR
                         (b.Performance LIKE '%$keyword%') 
						)
                ";
                
                $sql .= $EnrolIDCond;
                 if (trim($conds)!=""){
                 	$sql .= " AND $conds ";
                 }
//                // edited by Ivan (22 July 2008) modifying the SQL to add condition
//                if (trim($conds)!=""){
//	                $sql .= "(".$conds.") AND (";
//                }


//                // edited by Ivan (22 July 2008) modifying the SQL to add condition
//                if (trim($conds)!=""){
//	                $sql .= "(".$conds.") AND (";
//                }
//
//                if (trim($conds)!=""){
//	                $sql .= ")";
//                }

                $sql .= $special_order;
//debug_pr($sql);
//debug_pr(mysql_error());
                $cols = ($this->RecordType==3? 16: 15);
//hdebug_pr($sql);
                return $this->returnArray($sql,$cols);
        }
        function returnGroupUser4Display($order="", $user_type=''){
                global $i_status_approved, $i_status_suspended, $i_status_pendinguser;
                $username_field = getNameFieldWithClassNumberByLang("a.");

                $user_type_con = $user_type ? " a.RecordType in ($user_type) and " : "";
                $field  = "a.UserID, a.UserLogin, a.UserEmail, $username_field, ";
                $field .= "IF(a.RecordStatus='1','$i_status_approved',IF(a.RecordStatus='2','$i_status_pendinguser','$i_status_suspended')), ";
                $field .= "IFNULL(c.Title, '-'), a.UserPassword, a.URL, b.RecordType, a.ClassName, a.ClassNumber, a.EnglishName, a.ChineseName ";
                
                $sql  = "SELECT $field FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b ";
                $sql .= "LEFT OUTER JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID ";
                $sql .= "WHERE $user_type_con a.UserID = b.UserID AND a.RecordStatus IN (0,1,2) AND b.GroupID = ".$this->GroupID;
                $sql .= $order;
                return $this->returnArray($sql,13);
        }

        function returnGroupAnnouncement(){
                $sql  = "SELECT a.AnnouncementID, a.Title FROM INTRANET_ANNOUNCEMENT AS a, INTRANET_GROUPANNOUNCEMENT AS b WHERE a.RecordStatus = '1' AND a.AnnouncementID = b.AnnouncementID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,2);
        }

        function returnGroupEvent(){
                $sql  = "SELECT a.EventID, a.EventDate, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b WHERE a.RecordStatus = '1' AND a.EventID = b.EventID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,3);
        }

        function returnGroupPolling(){
                $sql  = "SELECT a.PollingID, a.Question FROM INTRANET_POLLING AS a, INTRANET_GROUPPOLLING AS b WHERE a.PollingID = b.PollingID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,2);
        }

        function returnGroupTimetable(){
                $sql  = "SELECT a.TimetableID, a.Title, a.Description, a.URL FROM INTRANET_TIMETABLE AS a, INTRANET_GROUPTIMETABLE AS b WHERE a.RecordStatus = '1' AND a.TimetableID = b.TimetableID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,4);
        }

        function returnGroupResource(){
                $sql  = "SELECT a.ResourceID, a.ResourceCode, a.Title FROM INTRANET_RESOURCE AS a, INTRANET_GROUPRESOURCE AS b WHERE a.RecordStatus = '1' AND a.ResourceID = b.ResourceID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,3);
        }

        function returnGroupUsedStorageQuota()
        {
                 //$sql1 = "SELECT SUM(Size) FROM INTRANET_FILE WHERE GroupID=".$this->GroupID;
                 $sql1 = "SELECT if(count(*)>0, SUM(Size), 0) FROM INTRANET_FILE WHERE GroupID='".$this->GroupID."'";
//                 $result = $this->returnArray($sql1,1);
//                 $v1 = $result[0][0];

                 ## forum attachment
                 //$sql2 = "SELECT SUM(AttSize) FROM INTRANET_BULLETIN WHERE GroupID=".$this->GroupID;
                 $sql2 = "SELECT if(count(*)>0, SUM(AttSize), 0) FROM INTRANET_BULLETIN WHERE GroupID='".$this->GroupID."'";
//                 $result = $this->returnArray($sql2,1);
//                 $v2 = $result[0][0];

				$sql = "SELECT ($sql1) + ($sql2)";
				$result = $this->returnArray($sql,1);				

                 return $result[0][0];
        }

        function returnGroupsByCategory($category="", $AcademicYearID="")
        {
	        global $intranet_session_language;
	        
			$title_field = $intranet_session_language =="en" ? "Title":"TitleChinese";       
			 
                 $conds = ($category==""? "" :" WHERE RecordType = $category");
                 if($AcademicYearID)
                 {
									$conds .= ($conds==""? " where" :" and ");
									$conds .= "AcademicYearID = $AcademicYearID";
								}

                 $sql = "SELECT GroupID, Title as TitleEn, ". $title_field ." FROM INTRANET_GROUP $conds ORDER BY Title";
                 return $this->returnArray($sql,2);
        }

		function returnIdentityGroupsID()
		{
			$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordStatus = 1";
			$result = $this->returnVector($sql);

			return $result;
		}
		function returnAllGroupIDAndName($Cond= '', $OrderBy='',$RecordType='',$AcademicYearID='')
		{
			if(trim($OrderBy)!='')
				$OrderBy = " Order By $OrderBy ";
			
			if($RecordType!=''){
				$Cond.= " AND RecordType=".$RecordType;
			}
			if($AcademicYearID!=''){
				$Cond .= " AND AcademicYearID =".$AcademicYearID;
			}
			
			$sql = "SELECT GroupID,Title,AcademicYearID, RecordType, TitleChinese FROM INTRANET_GROUP WHERE 1 $Cond $OrderBy";
			$result = $this->returnArray($sql);

			return $result;
		}
		
		function returnAllSiteInfo($Cond= '', $OrderBy='',$RecordType='',$AcademicYearID='')
		{
		    if(trim($OrderBy)!='')
		        $OrderBy = " Order By $OrderBy ";
		        
		        if($RecordType!=''){
		            $Cond.= " AND RecordType=".$RecordType;
		        }
		        if($AcademicYearID!=''){
		            $Cond .= " AND AcademicYearID =".$AcademicYearID;
		        }
		        
		        $sql = "SELECT GroupID,Title,AcademicYearID, RecordType, TitleChinese FROM INTRANET_GROUP WHERE 1 $Cond $OrderBy";
		        $result = $this->returnArray($sql);
		        
		        return $result;
		}

		function returnGroupDefaultRole($ParGroupID='')
		{
			if(trim($ParGroupID)=='')
				$ParGroupID = $this->GroupID;

			$sql = "
				SELECT
					b.RoleID
				FROM
					INTRANET_GROUP_CATEGORY a
					INNER JOIN INTRANET_ROLE b ON a.GroupCategoryID = b.RecordType
					INNER JOIN INTRANET_GROUP c ON a.GroupCategoryID = c.RecordType
				WHERE
					b.RecordStatus = 1
					AND GroupID = '$ParGroupID'
			";
			$tmp = $this->returnVector($sql);

			return $tmp[0];

		}

		function isGroupMember($ParUserID, $ParGroupID='')
		{
			if(trim($ParGroupID)=='') $ParGroupID = $this->GroupID;
			$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE UserID = '$ParUserID' AND GroupID = '$ParGroupID'	";
			$tmp = $this->returnVector($sql);
			return $tmp[0];

		}
        # ------------------------------------------------------------------------------------

        //edited by Ivan (22 July 2008) add the parameter $conds
        function getNumberGroupUsers($conds="",$AcademicYearID=''){
                return sizeof($this->returnGroupUser("", $conds,'',$AcademicYearID));
        }

        function getNumberGroupAnnouncement(){
                return sizeof($this->returnGroupAnnouncement());
        }

        function getNumberGroupEvent(){
                return sizeof($this->returnGroupEvent());
        }

        function getNumberGroupPolling(){
                return sizeof($this->returnGroupPolling());
        }

        function getNumberGroupTimetable(){
                return sizeof($this->returnGroupTimetable());
        }

        function getNumberGroupResource(){
                return sizeof($this->returnGroupResource());
        }

        # ------------------------------------------------------------------------------------

        function check($id){
                $x = "<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";
                return $x;
        }

        function displayFunctionbar($a="", $b=""){
                return "<table width=100% border=0 cellpadding=1 cellspacing=0><tr><td>$a</td><td align=right>$b</td></tr></table>\n";
        }

        function column($field_index, $field_name){
                global $image_path;
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
                        if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
                }
                $x .= "</a>";
                return $x;
        }

        function column_ip20($field_index, $field_name){
                global $image_path, $LAYOUT_SKIN;
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a class=\"tabletoplink\" href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a class=\"tabletoplink\" href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a class=\"tabletoplink\" href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index){
					if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
					if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
                }

                $x .= "</a>";
                return $x;
        }

        function navigation(){
                global $image_path;
                $x .= $this->record_range();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_prev();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->nav_next();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_page();
                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }
                return $x;
        }
        function navigation_ip20(){
                global $image_path;

                $x .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
                $x .= "<tr><td class='tabletext' align='left'>";

                $x .= "<table cellspacing='0' cellpadding='0' border='0'><tr><td class='tabletext'>";
                $x .= $this->record_range();
                $x .= "</td></tr></table>";

                $x .= "</td><td align='right'>";

                $x .= "<table cellspacing='0' cellpadding='0' border='0'><tr><td>";
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= "</td><td class='tabletext'>".$this->nav_prev_ip20()."</td>";
                $x .= "<td class='tabletext'>".$this->nav_page()."</td>";
                $x .= "<td class='tabletext'>".$this->nav_next_ip20()."</td>";

                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<td><img src=$image_path/space.gif width=10 height=10 border=0></td>\n";
                    $x .= "<td class='tabletext'>".$this->page_size_input()."</td>";
                }
                $x .= "</tr></table>";

                $x .= "</td>";
                $x .= "</tr></table>";

                return $x;
        }

		
        /*
        function navigation2(){
                global $image_path;
                $x .= $this->record_range();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_prev();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->nav_next();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_page();
                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }
                return $x;
        }
        */
        # For Alumni
        function prev_n2(){
                global $image_path,$image_pre;
                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $previous_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_l.gif' width='13' height='13' align='absmiddle'>";
                else
                    $previous_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_l.gif' width='13' height='13' align='absmiddle'>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? "<span >".$previous_icon.$this->pagePrev."</span>\n" : "<a  href=javascript:"."gopage(".($n_page-1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }
        function next_n2(){
                global $image_path,$image_next;
                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $next_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_r.gif' width='13' height='13' align='absmiddle'>";
                else
                    $next_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_r.gif' width='13' height='13' align='absmiddle'>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? "<span >".$this->pageNext.$next_icon."</span>\n" : "<a href=javascript:"."gopage(".($n_page+1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }
        function navigation2(){
                global $image_path;

                                $x = "<table width='90%' border='0' cellspacing='0' cellpadding='0'>";
                                $x .= "<tr>";
                $x .= "<td width='10'><img src='$image_path/page_l.gif' width='10' height='34'></td>";
                $x .= "<td align='center' class='alumni_next_page_cellbg' valign='middle'>";
                $x .= $this->record_range();
                $x .= $this->prev_n2();
                $x .= $this->nav_page();
                $x .= $this->next_n2();

                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }

                                $x .= "</td>";
                $x .= "<td width='10'><img src='$image_path/page_r.gif' width='10' height='34'></td></tr>";
                                $x .= "</table>";

                return $x;
        }
        function page_size_input ()
        {
                 global $i_general_EachDisplay ,$i_general_PerPage;

                 $x = "$i_general_EachDisplay <select name='num_per_page' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                 $x .= "<option value=10 ".($this->page_size==10? "SELECTED":"").">10</option>\n";
                 $x .= "<option value=20 ".($this->page_size==20? "SELECTED":"").">20</option>\n";
                 $x .= "<option value=30 ".($this->page_size==30? "SELECTED":"").">30</option>\n";
                 $x .= "<option value=40 ".($this->page_size==40? "SELECTED":"").">40</option>\n";
                 $x .= "<option value=50 ".($this->page_size==50? "SELECTED":"").">50</option>\n";
                 $x .= "<option value=60 ".($this->page_size==60? "SELECTED":"").">60</option>\n";
                 $x .= "<option value=70 ".($this->page_size==70? "SELECTED":"").">70</option>\n";
                 $x .= "<option value=80 ".($this->page_size==80? "SELECTED":"").">80</option>\n";
                 $x .= "<option value=90 ".($this->page_size==90? "SELECTED":"").">90</option>\n";
                 $x .= "<option value=100 ".($this->page_size==100? "SELECTED":"").">100</option>\n";
                 $x .= "</select>$i_general_PerPage\n";
                 return $x;
        }

        function record_range(){
				global $Lang;

	        	$n_title = ($this->title == '')? $Lang['General']['Record'] : $this->title;
                //$n_title = $this->title;
                $n_start = ($this->pageNo-1)*$this->page_size+1;
                $n_end = min($this->no_of_rows, ($this->pageNo*$this->page_size));
                $n_total = $this->no_of_rows;
                $x = "$n_title $n_start - $n_end, ".$this->total." $n_total\n";
                return $x;
        }

        function nav_prev(){
                global $image_path,$image_pre;
                if ($this->new_navigation)
                    $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
                else
                    $previous_icon = "<img src=$image_path/previous_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? $previous_icon.$this->pagePrev."\n" : "<a href=javascript:gopage(".($n_page-1).",document.form1) onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }

        function nav_prev_ip20(){
                global $image_path,$image_pre, $LAYOUT_SKIN;
                if ($this->new_navigation)
                    $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
                else
                    $previous_icon = "<img src=$image_path/$LAYOUT_SKIN/icon_prev_off.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? $previous_icon."\n" : "<a href=javascript:gopage(".($n_page-1).",document.form1) onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon."</a>\n";
                return $x;
        }

        function nav_next(){
                global $image_path,$image_next;
                if ($this->new_navigation)
                    $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
                else
                    $next_icon = "<img src=$image_path/next_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? $this->pageNext.$next_icon."\n" : "<a href=javascript:gopage(".($n_page+1).",document.form1) onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }

        function nav_next_ip20(){
                global $image_path,$image_next, $LAYOUT_SKIN;
                if ($this->new_navigation)
                    $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
                else
                    $next_icon = "<img src=$image_path/$LAYOUT_SKIN/icon_next_off.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? $next_icon."\n" : "<a href=javascript:gopage(".($n_page+1).",document.form1) onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$next_icon."</a>\n";
                return $x;
        }

        function nav_page(){
                $x  = $this->pagename;
                $x .= " <select name=pageNoSelect onChange='this.form.pageNo.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                for($i=1; $i<=ceil($this->no_of_rows/$this->page_size); $i++)
                $x .= "<option value=$i ".(($this->pageNo==$i)?"SELECTED":"").">$i</option>\n";
                $x .= "</select>\n";
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function displayGroupUsers(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                global $i_UserEnglishName, $i_UserChineseName,$i_ClassNameNumber;
                global $i_ActivityPerformance;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","b.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "15%";
                    $width_login = "15%";
                    $width_eng = "30%";
                    $width_chi = "15%";
                    $width_class = "10%";
                    $width_perform = "15%";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "15%";
                    $width_login = "20%";
                    $width_eng = "30%";
                    $width_chi = "20%";
                    $width_class = "15%";
                    $total_col = 7;
                }
                $x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= "<tr>";
                $x .= "<td class=tableTitle width=1>#</td>\n";
                $x .= "<td class=tableTitle width=$width_role>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td class=tableTitle width=$width_login>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td class=tableTitle width=$width_eng>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td class=tableTitle width=$width_chi>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td class=tableTitle width=$width_class>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                if ($this->RecordType == 5)
                {
                    $x .= "<td class=tableTitle width=$width_perform>".$this->column(5,$i_ActivityPerformance)."</td>\n";
                }
                $x .= "<td class=tableTitle width=1>".$this->check("UserID[]")."</td>\n";
                $x .= "</tr>\n";
                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=$total_col class=tableContent><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $tUserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $isSubjectLeader = ($row[$i][15]==1?"<font color=green>#</font>":"");
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $EnglishName = ($EnglishName=="") ? "&nbsp;" : $EnglishName;
                                                $ChineseName = ($ChineseName=="") ? "&nbsp;" : $ChineseName;
                                                $x .= "<tr>";
                        $x .= "<td class=tableContent>".($i+1)."</td>\n";
                        $x .= "<td class=tableContent>$RoleTitle $isAdmin $isSubjectLeader</td>\n";
                        $x .= "<td class=tableContent>$UserLogin</td>\n";
                        $x .= "<td class=tableContent>$EnglishName</td>\n";
                        $x .= "<td class=tableContent>$ChineseName</td>\n";
                        $x .= "<td class=tableContent>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class=tableContent>$Performance</td>\n";
                        }
                        $x .= "<td class=tableContent><input type=checkbox name=UserID[] value=$tUserID></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= (sizeof($row)==0) ? "" : "<tr><td style='vertical-align:middle' align=right colspan=$total_col>".$this->navigation()."</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=pageNo value=\"$this->pageNo\">";
                return $x;
        }

        /* edited by Ivan(22 Jul 2008) to display information excluding LoginID
		*
		*		$from: the page calling this function
		*		$withoutLoginID: boolean to determine if loginID will be displayed
		*		$conds: condition of sql
		*/
		/* edited by Ivan(1 Augl 2008) to check the selected checkbox after viewing of another page
		*
		*		$uid_checked_list: UID of the checkbox which should be checked
		*/
        function displayGroupUsers_ip20($from="", $withoutLoginID="", $conds="", $uid_checked_list="", $recordType="", $AcademicYearID='', $roleType=1){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                global $i_UserEnglishName, $i_UserChineseName,$i_ClassNameNumber;
                global $i_ActivityPerformance, $Lang, $PATH_WRT_ROOT;

                if ($withoutLoginID){
	                $this->field_array = array("c.Title","a.ClassNumber,a.ClassName","a.EnglishName","a.ChineseName","b.Performance");
                }
                else{
	                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","b.Performance");
                }

                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassNumber, a.ClassName, a.EnglishName";
                }
                else
                {
	            	if (strcmp($from,"member_index.php")==0 && $this->field == 1){ //class number field (cannot handle students who do not have a class number if this line is deleted)
			        	$order_str = "a.ClassNumber, a.ClassName, a.EnglishName";
	                }

	                else{
		                $order_str = $this->field_array[$this->field];
	                }
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                //$row = $this->returnGroupUser(" ORDER BY $order_str", $conds,"",$AcademicYearID);
   				$row = $this->returnGroupUser(" ORDER BY $order_str", $conds,"",$AcademicYearID,$roleType);
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);

                if ($this->RecordType == 5)
                {
	                if ($withoutLoginID){
		                $width_role = "15%";
	                    $width_class = "15%";
	                    $width_eng = "25%";
	                    $width_chi = "25%";
	                    $width_perform = "20%";
	                    $total_col = 8;
	                }
	                else{
		                $width_role = "15%";
	                    $width_login = "15%";
	                    $width_eng = "30%";
	                    $width_chi = "15%";
	                    $width_class = "10%";
	                    $width_perform = "15%";
	                    $total_col = 8;
	                }
                }
                else
                {
	                if ($withoutLoginID){
	                	$width_role = "20%";
	                    $width_class = "20%";
	                    $width_eng = "30%";
	                    $width_chi = "30%";
	                    $total_col = 7;
		            }
	                else{
		                $width_role = "15%";
	                    $width_login = "20%";
	                    $width_eng = "30%";
	                    $width_chi = "20%";
	                    $width_class = "15%";
	                    $total_col = 7;
	                }

                }
                $x .= "<table width='96%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
                $x .= "<tr class='tabletop'>";
                $x .= "<td class='tabletext' width='1'><span class='tabletoplink'>#</span></td>\n";
                $x .= "<td class='tabletext' width='$width_role'>".$this->column_ip20(0,$Lang['Group']['RoleSetting'])."</td>\n";

                if($withoutLoginID){
	                $x .= "<td class='tabletext' width='$width_class'>".$this->column_ip20(1,$i_ClassNameNumber)."</td>\n";
	                $x .= "<td class='tabletext' width='$width_eng'>".$this->column_ip20(2,$i_UserEnglishName)."</td>\n";
	                $x .= "<td class='tabletext' width='$width_chi'>".$this->column_ip20(3,$i_UserChineseName)."</td>\n";
	            }
                else{
	                $x .= "<td class='tabletext' width='$width_login'>".$this->column_ip20(1,$i_UserLogin)."</td>\n";
	                $x .= "<td class='tabletext' width='$width_eng'>".$this->column_ip20(2,$i_UserEnglishName)."</td>\n";
	                $x .= "<td class='tabletext' width='$width_chi'>".$this->column_ip20(3,$i_UserChineseName)."</td>\n";
	                $x .= "<td class='tabletext' width='$width_class'>".$this->column_ip20(4,$i_ClassNameNumber)."</td>\n";
                }

                if ($this->RecordType == 5)
                {
	                if($withoutLoginID){
		                $x .= "<td class='tableTitle' width='$width_perform'>".$this->column_ip20(4,$i_ActivityPerformance)."</td>\n";
	                }
	                else{
		                $x .= "<td class='tableTitle' width='$width_perform'>".$this->column_ip20(5,$i_ActivityPerformance)."</td>\n";
	                }
                }
                $x .= "<td class='tableTitle' width='1'>".$this->check("UID[]")."</td>\n";
                $x .= "</tr>\n";
                $x .= (sizeof($row)==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                
                include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
                $AcademicYearID = $this->AcademicYearID?$this->AcademicYearID:$AcademicYearID;
                $ay = new academic_year($AcademicYearID);
                for($i=$start; $i<$end; $i++){
                		 $UID = $row[$i][0];
		                $UserLogin = $row[$i][1];
	                    $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        if ($recordType == 1 || $recordType == 3)
                        {
	                        //get role from INTRANET_ENROL_GROUPSTAFF for non-student
	                        $sql = "SELECT EnrolGroupID FROM INTRANET_ENROL_GROUPINFO WHERE GroupID = '".$this->GroupID."'";
	                        $result = $this->returnArray($sql,1);
	                        $enrolGroupID = $result[0][0];
	                     	$sql = "SELECT StaffType FROM INTRANET_ENROL_GROUPSTAFF WHERE UserID = '$UID' AND EnrolGroupID = '$enrolGroupID'";
	                     	$result = $this->returnArray($sql,1);
	                     	$RoleTitle = $result[0][0];

                        }
                        else
                        {
	                        //student => use the default role
	                        $RoleTitle = $row[$i][6];
                        }

                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        $Remarks = $ay->Check_User_Existence_In_Academic_Year($UID)?"":"<font color=red>^</font>";
                        
						if ($Performance == "") $Performance = "-";
                        $isSubjectLeader = ($row[$i][15]==1?"<font color=green>#</font>":"");
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $EnglishName = ($EnglishName=="") ? "&nbsp;" : $EnglishName;
                                                $ChineseName = ($ChineseName=="") ? "&nbsp;" : $ChineseName;
                                                $x .= "<tr class='tablerow".(($i % 2) + 1)."'>";
                        $x .= "<td class='tabletext'>".($i+1)."</td>\n";
                        $x .= "<td class='tabletext'>$RoleTitle $isAdmin $isSubjectLeader</td>\n";

                        if($withoutLoginID){
	                        $x .= "<td class='tabletext'>$class_display</td>\n";
                        }
                        else{
	                        $x .= "<td class='tabletext'>".$Remarks.$UserLogin."</td>\n";
                        }
                        $x .= "<td class='tabletext'>$EnglishName</td>\n";
                        $x .= "<td class='tabletext'>$ChineseName</td>\n";

                        if(!$withoutLoginID){
	                        $x .= "<td class='tabletext'>$class_display</td>\n";
                        }

                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class='tabletext'>$Performance</td>\n";
                        }


                        $isInList = false;
                        if ($uid_checked_list!=NULL){
	                        for($j=0; $j<sizeof($uid_checked_list); $j++){
	                     		if ($uid_checked_list[$j]==$UID){
		                     		$isInList = true;
		                     		break;
	                     		}
		                    }
                        }
                        if ($isInList){
	                        $x .= "<td class='tabletext'><input type='checkbox' name='UID[]' value='$UID' checked></td>\n";
                        }
                        else{
	                        $x .= "<td class='tabletext'><input type='checkbox' name='UID[]' value='$UID'></td>\n";
                        }

                        //$x .= "<td class='tabletext'><input type='checkbox' name='UID[]' value='$UID'></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= (sizeof($row)==0) ? "" : "<tr><td style='vertical-align:middle' align='right' colspan='$total_col' class='tablebottom'>".$this->navigation_ip20()."</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type='hidden' name='pageNo' value=\"$this->pageNo\">";
                return $x;
        }

        function displayGroupUsersForAdmin(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus,$i_ClassNameNumber;
                global $i_UserEnglishName, $i_UserChineseName,$i_ActivityPerformance;
                global $image_path;
                $this->new_navigation = true;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","a.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "100";
                    $width_login = "90";
                    $width_eng = "140";
                    $width_chi = "110";
                    $width_class = "90";
                    $width_perform = "150";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "100";
                    $width_login = "100";
                    $width_eng = "205";
                    $width_chi = "185";
                    $width_class = "90";
                    $total_col = 7;
                }
                /*$x .= "<table width=100% border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0\">";
                $x .= "<tr align=left >";
                $x .= "<td bgcolor=#FCD5AE width=20 class=title_head align=center width=1>#</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_role class=title_head>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_login class=title_head>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_eng class=title_head>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_chi class=title_head>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_class class=title_head>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                */

                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
                $x .= "<tr class=forumtablerow align=left >";
                $x .= "<td width=20 class=forumtabletop forumtabletoptext align=center width=1>#</td>\n";
                $x .= "<td width=$width_role class=forumtabletop forumtabletoptext>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td width=$width_login class=forumtabletop forumtabletoptext>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td width=$width_eng class=forumtabletop forumtabletoptext>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td width=$width_chi class=forumtabletop forumtabletoptext>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td width=$width_class class=forumtabletop forumtabletoptext>".$this->column(4,$i_ClassNameNumber)."</td>\n";

                if ($this->RecordType == 5)
                {
                    $x .= "<td width=$width_perform class=forumtabletop forumtabletoptext>".$this->column(5,$i_ActivityPerformance)."</td>\n";
                }

                $x .= "<td width=20 class=forumtabletop forumtabletoptext>".$this->check("targetUserID[]")."</td>\n";
                $x .= "</tr>\n";

                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=$total_col class=body><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $tUserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $x .= "<tr align=left valign=top>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>".($i+1)."</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$RoleTitle $isAdmin</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$UserLogin</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$EnglishName &nbsp;</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$ChineseName &nbsp;</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class='albumtablerow tabletext' align='center'>$Performance</td>\n";
                        }
 //                        $x .= "<td width=150 class=body>$UserEmail</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'><input type=checkbox name=targetUserID[] value=$tUserID></td>\n";
#                        $x .= "<td class=h1 >&nbsp;</td>\n";
                        $x .= "</tr>\n";
                }

                $x .= (sizeof($row)==0) ? "" : "<tr><td colspan=3>&nbsp;</td><td bgcolor=#FFE6BC style='vertical-align:middle' align=center colspan=".($total_col-4).">".$this->navigation()."</td><td>&nbsp;</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=pageNo value=\"$this->pageNo\">";
                return $x;
        }
        function displayGroupUsersForAdmin1(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus,$i_ClassNameNumber;
                global $i_UserEnglishName, $i_UserChineseName,$i_ActivityPerformance;
                global $image_path;
                $this->new_navigation = true;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","a.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "100";
                    $width_login = "90";
                    $width_eng = "140";
                    $width_chi = "110";
                    $width_class = "90";
                    $width_perform = "150";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "100";
                    $width_login = "100";
                    $width_eng = "205";
                    $width_chi = "185";
                    $width_class = "90";
                    $total_col = 7;
                }
                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#5DA5C9 bordercolordark=#FFFFFF bgcolor=#FFFFFF class=13-black>";
                $x .= "<tr bgcolor=#E3DB9C>";

                $x .= "<table width=100% border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0\">";
                $x .= "<tr align=left >";
                $x .= "<td bgcolor=\"#E3DB9C\" width=20 class=13-black-bold align=center width=1>#</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_role class=13-black-bold>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_login class=13-black-bold>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_eng class=13-black-bold>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_chi class=13-black-bold>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_class class=13-black-bold>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                if ($this->RecordType == 5)
                {
                    $x .= "<td bgcolor=\"#E3DB9C\" width=$width_perform class=13-black-bold>".$this->column(5,$i_ActivityPerformance)."</td>\n";
                }

                $x .= "<td bgcolor=\"#E3DB9C\" width=20 class=13-black-bold>".$this->check("targetUserID[]")."</td>\n";
                $x .= "</tr>\n";

                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=$total_col class=body><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $tUserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $css = ($i%2?"":"2");
                        $x .= "<tr class=tableContent$css align=left valign=top>\n";
                        $x .= "<td class=body align=center>".($i+1)."</td>\n";
                        $x .= "<td class=body>$RoleTitle $isAdmin</td>\n";
                        $x .= "<td class=body>$UserLogin</td>\n";
                        $x .= "<td class=body>$EnglishName &nbsp;</td>\n";
                        $x .= "<td class=body>$ChineseName &nbsp;</td>\n";
                        $x .= "<td class=body>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class=body>$Performance</td>\n";
                        }
 //                        $x .= "<td width=150 class=body>$UserEmail</td>\n";
                        $x .= "<td ><input type=checkbox name=targetUserID[] value=$tUserID></td>\n";
#                        $x .= "<td class=h1 >&nbsp;</td>\n";
                        $x .= "</tr>\n";
                }

                #$x .= (sizeof($row)==0) ? "" : "<tr><td colspan=3>&nbsp;</td><td bgcolor=#FFE6BC style='vertical-align:middle' align=center colspan=".($total_col-4).">".$this->navigation2()."</td><td>&nbsp;</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=pageNo value=\"$this->pageNo\">";
                return $x;
        }
        # ------------------------------------------------------------------------------------

        function displayGroupAnnouncement(){
                return $this->displayRow($this->returnGroupAnnouncement());
        }

        function displayGroupEvent(){
                return $this->displayRow($this->returnGroupEvent());
        }

        function displayGroupPolling(){
                return $this->displayRow($this->returnGroupPolling());
        }

        function displayGroupTimetable(){
                return $this->displayRow($this->returnGroupTimetable());
        }

        function displayGroupResource(){
                return $this->displayRow($this->returnGroupResource());
        }

        # ------------------------------------------------------------------------------------

        function displayRow($row){
                for($i=0; $i<sizeof($row); $i++){
                        $x .= "".implode(":", $row[$i])."<br>\n";
                }
                return $x;
        }

        function displayGroupUsersEmailOption($cond=''){
                $row = $this->returnGroupUser("$cond ORDER BY a.UserEmail ASC");
                for($i=0; $i<sizeof($row); $i++){
                        $UserEmail = $row[$i][2];
                        $x .= "<option value=$UserEmail SELECTED>$UserEmail</option>\n";
                }
                return $x;
        }

        function displayGroupUsersPassword(){
                $row = $this->returnGroupUser(" ORDER BY a.UserLogin ASC");
                $x = "UserLogin ; EnglishName ; ChineseName ; ClassName ; ClassNumber ; UserEmail\n";
                for($i=0; $i<sizeof($row); $i++){
                        $UserLogin = $row[$i][1];
                        $UserEmail = $row[$i][2];
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $UserPassword = $row[$i][7];
                        $ClassName = $row[$i][10];
                        $ClassNumber = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $Chinesename = $row[$i][13];
                        $x .= "$UserLogin ; $EnglishName ; $ChineseName ; $ClassName ; $ClassNumber ; $UserEmail\n";
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------
/*
        function displayGroupImport(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                $row = $this->returnGroupUser();
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                $x .= "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr>";
                $x .= "<td class=tableTitle width=1>#</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_admintitle_role</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserLogin</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserFirstName</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserLastName</td>\n";
                $x .= "<td class=tableTitle width=25%>$i_UserEmail</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserRecordStatus</td>\n";
                $x .= "<td class=tableTitle width=1>".$this->check("UserID[]")."</td>\n";
                $x .= "</tr>\n";
                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=8 class=tableContent><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $tUserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RecordStatus = $row[$i][5];
                        $RoleTitle = $row[$i][6];
                        $x .= "<tr>";
                        $x .= "<td class=tableContent>".($i+1)."</td>\n";
                        $x .= "<td class=tableContent>$RoleTitle</td>\n";
                        $x .= "<td class=tableContent>$UserLogin</td>\n";
                        $x .= "<td class=tableContent>$FirstName</td>\n";
                        $x .= "<td class=tableContent>$LastName</td>\n";
                        $x .= "<td class=tableContent>$UserEmail</td>\n";
                        $x .= "<td class=tableContent><select name=memberType_$tUserID><option value=S>S</option><option value=T>T</option><option value=A>A</option></select></td>\n";
                        $x .= "<td class=tableContent><input type=checkbox name=UserID[] value=$tUserID CHECKED></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= (sizeof($row)==0) ? "" : "<tr><td style='vertical-align:middle' align=right colspan=8>".$this->navigation()."</td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }
*/
        # ------------------------------------------------------------------------------------

        function returnRoleType($InsertIfEmpty=""){
                $sql = "SELECT RoleID, Title, RecordStatus FROM INTRANET_ROLE WHERE RecordType = ".$this->RecordType ." order by Title";
                $tmp = $this->returnArray($sql,3);
                if(empty($tmp) && $InsertIfEmpty)
                {
	                $sql = "INSERT INTO
								INTRANET_ROLE
								(
									Title,
									RecordStatus,
									RecordType,
									DateInput,
									DateModified
								)
								VALUES
								(
									'member',
									1,
									'".$this->RecordType."',
									NOW(),
									NOW()
								)
							";
                	$this->db_db_query($sql);
                	$RoleID = mysql_insert_id();
                	$tmp = array(
                		0 => array(
	                		0 => $RoleID, 1 => "member", 2 => 1,
							"RoleID" => $RoleID, "Title"=> "member", "RecordStatus" => 1
                		)
                	);
                }
                return $tmp;
        }

        function returnGroupRoleType(){
                $sql = "SELECT RoleID, Title, RecordStatus FROM INTRANET_ROLE WHERE RecordType = 5";
                return $this->returnArray($sql,3);
        }

        # ------------------------------------------------------------------------------------

        function displayDirectoryUsers(){
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                global $hideEmail;                       # Display Option
                global $i_status_approved;
                $row = $this->returnGroupUser4Display(" ORDER BY c.title, a.ClassName, a.ClassNumber, a.EnglishName");
                $cols = 2;
                $width = 100/$cols;
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= $i_no_record_exists_msg."\n";
                }else{
                        $x .= "<table width=90% border=1 cellspacing=0 cellpadding=5 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5>\n";
                        $x .= "<tr>\n";
                        $row_count = 0;
                        for($i=0; $i<sizeof($row); $i++){
                                $tUserID = $row[$i][0];
                                $UserLogin = $row[$i][1];
                                $UserEmail = $row[$i][2];
                                $Username = $row[$i][3];
                                $RecordStatus = $row[$i][4];
                                $RoleTitle = $row[$i][5];
                                $URL = $row[$i][7];
                                $convertedURL = $this->returnImageIconForURL($URL);

                                if ($RecordStatus != $i_status_approved)
                                {
                                    $Username = "$Username <i>($RecordStatus)</i>";
                                }

                                if ($RoleTitle!=$row[$i-1][5])
                                {
                                    if ($row_count % $cols)
                                    {
                                        $x .= "<td width=$width%>&nbsp;</td>";
                                    }
                                    $x .= "</tr>\n<tr>\n<td colspan=$cols><b>$RoleTitle</b></td>\n</tr>\n<tr>\n";
                                    $row_count = 0;
                                }
                                if ($i!=0 && $row_count%$cols==0)
                                {
                                    $x .= "</tr><tr>";
                                    $row_count = 0;
                                }
                                $row_count++;
                                if ($hideEmail)
                                {
                                    $x .= "<td width=$width%>$Username  $convertedURL </td>\n";
                                }
                                else
                                {
                                    $x .= "<td width=$width%><a href=mailto:$UserEmail>$Username </a> $convertedURL </td>\n";
                                }
                        }
                        if ($row_count % $cols)
                        {
                            $x .= "<td width=$width%>&nbsp;</td>\n";
                        }
                        $x .= "</tr>\n";
                        $x .= "</table>\n";
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function showGroupTimetable(){
                $row = $this->returnGroupTimetable();
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= $i_no_record_exists_msg."\n";
                }else{
                        $x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $TimetableID = $row[$i][0];
                                $Title = $row[$i][1];
                                $Description = nl2br($this->convertAllLinks2($row[$i][2]));
                                $Url = str_replace(" ","%20",$row[$i][3]);
                                if($this->validateURL($Url)){
                                        $Url = "<a href=".$Url.">$Title</a>\n";
                                } else if ($Url<>"") {
                                        $Url = "<a href=/file/timetable".$Url.">$Title</a>\n";
                                } else {
                                        $Url = $Title;
                                }
                                $x .= "<tr>";
                                $x .= "<td>$Url</td>\n";
                                $x .= "<td>$Description</td>\n";
                                $x .= "</tr>\n";
                        }
                        $x .= "</table>\n";
                }
                return $x;
        }

        function displayGroupIcon(){
                global $file_path, $image_path;
                $limit = 50;
                $icon = "/file/group/g".$this->GroupID."_".$this->DateModifiedTimeStamp.".gif";
                $default = "/images/admin/icons/default.gif";
                $x = file_exists($file_path.$icon) ? $icon : $default;
                $image_info = file_exists($file_path.$icon) ? getimagesize($file_path.$icon) : getimagesize($file_path.$default);
                $width = $image_info[0];
                $height = $image_info[1];
                $width = ($width <= $limit) ? $width: $limit;
                $height = ($height <= $limit) ? $height: $limit;
                $x = "<img src=$x width=$width height=$height border=0 hspace=5 vspace=5 align=absmiddle>";
                return $x;
        }

        function returnImageIconForURL ($target)
        {
                 global $i_image_home;
                 if ($target != "")
                     $x = "<A HREF=\"$target\" target=_blank> $i_image_home </a>";
                 else $x ="";
                 return $x;
        }

        function displayStorage ()
        {
                 global $i_Campusquota_max, $i_Campusquota_used,$i_Campusquota_left,$i_Campusquota_using1,$i_Campusquota_using2;
                 $used = $this->returnGroupUsedStorageQuota();
                 $total = $this->StorageQuota * 1000;
                 if ($used == "") $used = 0;
                 if ($total == 0)
                 {
                     $pused = 100;
                     $left = 0;
                 }
                 else
                 {
                     $pused = 100*($used/$total);
                     $left = $total - $used;
                 }

                 $title = "$i_Campusquota_max: $total Kb(s)\n$i_Campusquota_used: $used Kb(s)\n$i_Campusquota_left: $left Kb(s)";

               $total .= " Kb(s) ";
               $left .= " Kb(s) ";
               $used .= " Kb(s), $pused% ";
               $storage = "<TABLE width=100% align=left><tr><td style=\"font-size:10px;\">0%</td>";
               $storage .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td  title='$title' width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
               $storage .= "<td style=\"font-size:10px;\">100%</td></tr>";
               $storage .= "</table>";
               $x .= "<table width=80% border=1 cellspacing=0 cellpadding=5 bordercolor=#555555 bgcolor=ACEFD5 class=decription>\n";
               $x .= "<tr><td><table width=100% border=0 cellspacing=1 cellpadding=0>";
               $x .= "<tr><td class=td_description align=right width=15%>$i_Campusquota_max </td><td class=td_description>:</td><td width=85% class=td_description> $total</td></tr>
                      <tr><td class=td_description align=right>$i_Campusquota_left</td><td class=td_description>:</td><td class=td_description> $left</td></tr>
                      <tr><td colspan=3 class=td_description>$storage </td></tr>
                      <tr><td class=td_description align=right>$i_Campusquota_used</td><td class=td_description>:</td><td class=td_description> $used</td></tr>
                      <tr><td colspan=3 class=td_description>$i_Campusquota_using1 $pused% $i_Campusquota_using2 </td></tr>
                         ";
               $x .= "</table></td></tr></table>\n";
               return $x;
        }
        function displayStorage2 ()
        {
                 global $i_Campusquota_max, $i_Campusquota_used,$i_Campusquota_left,$i_Campusquota_using1,$i_Campusquota_using2;
                 $used = $this->returnGroupUsedStorageQuota();
                 $total = $this->StorageQuota * 1000;
                 if ($used == "") $used = 0;
                 if ($total == 0)
                 {
                     $pused = 100;
                     $left = 0;
                 }
                 else
                 {
                     $pused = 100*($used/$total);
                     $left = $total - $used;
                 }

                 $title = "$i_Campusquota_max: $total Kb(s)\n$i_Campusquota_used: $used Kb(s)\n$i_Campusquota_left: $left Kb(s)";

               $total .= " Kb(s) ";
               $left .= " Kb(s) ";
               $used .= " Kb(s), $pused% ";

               $x .= "<table width=80% border=0 align=right cellpadding=2 cellspacing=0 bgcolor=#ACE5EF class=\"11-black\" bordercolorlight=\"#555555\" bordercolordark=\"#ACE5EF\">";
               $x .= "<tr class=11-black>
                <td class=11-black>$i_Campusquota_max : <span class=\"11-grey\">$total</span><br>
                  $i_Campusquota_left : <span class=\"11-grey\">$left</span></td>
                <td class=td_center_middle>
                <table width=100% cellpadding=0 cellspacing=0 border=0 align=center>
                <tr>
                <td class=11-black>0%</td><td width=130><table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td  title='$title' width=100%  bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table></td><td class=11-black> 100%</td>
                </tr>
                </table>
                </td>
                <td align=right class=11-black>$i_Campusquota_used : <span class=\"11-grey\">$used</span><br>
                  $i_Campusquota_using1 $pused% $i_Campusquota_using2</td>
              </tr>
            </table>";

               return $x;
        }

        function displayStorage3()
        {
			global $image_path, $LAYOUT_SKIN, $i_Campusquota_used, $i_Campusquota_used, $eComm;

	        $used = $this->returnGroupUsedStorageQuota();
 			$total = $this->StorageQuota * 1000;
 			if ($used == "") $used = 0;
             if ($total == 0)
             {
                 $pused = 100;
                 $left = 0;
             }
             else
             {
             	 # [2009-12-17]
                 $pused = round(100*($used/$total), 2);
                 $left = $total - $used;
             }

			$x = "
				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>


					<td valign=\"bottom\">
						<div id=\"show_storage\" style=\"position:absolute; width:190px; height:50px; z-index:1; visibility: hidden; text-align:center;\" class=\"storage_border\">
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
						<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
						<td align=\"left\" class=\"tabletext\">". $i_Campusquota_used ." : ". $used ." / ". $total ." Kb(s)&nbsp;</td>
						</tr>
						<tr>
						<td align=\"left\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"imailusage\">
						<tr>
						<td valign=\"middle\"><div id=\"Layer1\" style=\"position:absolute;width:150px;height:14px;z-index:2;\">
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
						<td align=\"center\" class=\"imailusagetext\">". $pused ."% </td>
						</tr>
						</table>
						</div>
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/usage_bar.gif\" width=\"". ($pused*150/100) ."\" height=\"14\"></td>
						</tr>
						</table></td>
						</tr>
						</table></td>
						</tr>
						</table>
						</div> </td>
						<td nowrap><span style='cursor:pointer;' class='tabletool' onMouseOver=\"MM_showHideLayers('show_storage','','show')\" onMouseOut=\"MM_showHideLayers('show_storage','','hidden')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_storage.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> ". $eComm['Storage'] ." : ". $pused ."% ". $i_Campusquota_used ." </span>&nbsp;</td>

					</tr>
					</table>
	        ";

	        return $x;
        }


        # /home/school/index
        # Shown besides icons
        function returnNumberOfNewBulletin()
        {
                 global $UserID;
                 $sql = "SELECT COUNT(BulletinID) FROM INTRANET_BULLETIN WHERE locate(';$UserID;',ReadFlag)=0 AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnNumberOfNewLinks()
        {
                 global $UserID;
                 $sql = "SELECT COUNT(LinkID) FROM INTRANET_LINK WHERE locate(';$UserID;',ReadFlag)=0 AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnNumberOfNewFiles()
        {
                 global $UserID;
                 $sql = "SELECT COUNT(FileID) FROM INTRANET_FILE WHERE locate(';$UserID;',ReadFlag)=0 AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        # ------------------------------------------------------------------------------------

        # ----------------------------------------------------------------------------------
        # Admin/user/new.php
        function getSelectClass($js_event="")
        {
                 global $button_select;
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE RecordType = 3 AND RIGHT(Title,1) <> '*' AND LOCATE('-',Title) = 0 ORDER BY Title";
                 $result = $this->returnVector($sql);
                 $x = "<SELECT $js_event>\n";
                 $x .= "<OPTION> -- $button_select -- </OPTION>\n";
                 for ($i=0; $i < sizeof($result); $i++)
                 {
                      $name = $result[$i];
                      $x .= "<OPTION value='$name'>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;

        }

        function getSelectLevel($js_event="")
        {
                 global $button_select;
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE RecordType = 3 AND RIGHT(Title,1) = '*' ORDER BY Title";
                 $result = $this->returnVector($sql);

                 $x = "<SELECT $js_event>\n";
                 $x .= "<OPTION> -- $button_select -- </OPTION>\n";
                 for ($i=0; $i < sizeof($result); $i++)
                 {
                      $name = substr($result[$i],0,-1);
                      $x .= "<OPTION value='$name'>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;

        }

        # ----------------------------------------------------------------
        # Group tools settings
        # Field : FunctionAccess
        #         - Bit-1 (LSB) : timetable
        #         - Bit-2       : chat
        #         - Bit-3       : bulletin
        #         - Bit-4       : shared links
        #         - Bit-5       : shared files
        #         - Bit-6       : question bank
        #         - Bit-7       : Photo Album
        #         - Bit-8 (MSB) : Survey
        # For adding new tool, add to the array result of returnGroupAvailableFunctions() in lib.php
        # Add corresponding isAccess function
        # ------------------------------------------------------------------


        function getSelectAvailableFunctions ()
        {
	        global $i_frontpage_schoolinfo_groupinfo_group_timetable, $i_GroupSettingsSurvey;

                 $functions = returnGroupAvailableFunctions();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 for ($i=0; $i<sizeof($functions); $i++)
                 {
//                      if ($functions[$i]!="")		# Disabled Timetable in Group/eCommunity
						if ($functions[$i]!="" && !($functions[$i]==$i_frontpage_schoolinfo_groupinfo_group_timetable || $functions[$i]==$i_GroupSettingsSurvey) )
						{
                          $x .= "
                          	<tr class=\"textboxtext\">
                          		<td><input type=checkbox name=grouptools[] value=$i CHECKED DISABLED id=\"check{$i}\"></td>
                          		<td><label for=\"check{$i}\">".$functions[$i]."</label></td>
                          	</tr>\n";
                      	}
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function isAccessAllTools()
        {
                 if ($this->GroupID != "" && $this->FunctionAccess == "ALL")
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
        }

        function isAccessTool($type)
        {
                 $available = returnGroupAvailableFunctions();
                 if ($this->isAccessAllTools()) return ($available[$type]!="");
                 
//                 $s = substr($this->FunctionAccess,$type,1);
//                 return ($s == "1");
                 
                 if (is_array($this->FunctionAccess)) {
                 	$result = false;
                 }
                 else {
                 	$s = substr($this->FunctionAccess,$type,1);
                 	$result = ($s == "1")? true : false;
                 }
                 
                 return $result;
                 
        }

        function isAccessTimeTable()
        {
                 return $this->isAccessTool(0);
        }
        function isAccessChatroom()
        {
                 return $this->isAccessTool(1);
        }
        function isAccessBulletin()
        {
                 return $this->isAccessTool(2);
        }
        function isAccessLinks()
        {
                 return $this->isAccessTool(3);
//                 return $this->AllowedWebsite!="DISABLED";
        }
        function isAccessFiles()
        {
        	return $this->isAccessTool(4);
//            return $this->AllowedFileTypes!="DISABLED";
        }
        function isAccessQB()
        {
                 if ($this->RecordType == 2)
                 {
                     return $this->isAccessTool(5);
                 }
                 return false;
        }
        function isAccessPhotoAlbum()
        {
//        	return $this->AllowedImageTypes!="DISABLED" ||$this->AllowedVideoTypes!="DISABLED"; 
                 return $this->isAccessTool(6);
        }
        function isAccessSurvey()
        {
                 return $this->isAccessTool(7);
        }
        function isAccessPhoto()
        {
                 return $this->AllowedImageTypes!="DISABLED";
        }
        function isAccessVideo()
        {
                 return $this->AllowedVideoTypes!="DISABLED";
        }
        function getSelectCurrentAvailableFunctions ()
        {
	        global $i_frontpage_schoolinfo_groupinfo_group_timetable, $i_GroupSettingsSurvey;

                 $option_status = ($this->isAccessAllTools()? "DISABLED": "");
                 $functions = returnGroupAvailableFunctions();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 for ($i=0; $i<sizeof($functions); $i++)
                 {
                      //if ($functions[$i]!="")
                      //if ($functions[$i]!="" && $functions[$i]!=$i_frontpage_schoolinfo_groupinfo_group_timetable)
                      if ($functions[$i]!="" && !($functions[$i]==$i_frontpage_schoolinfo_groupinfo_group_timetable || $functions[$i]==$i_GroupSettingsSurvey) )
                      {
                          $checked = ($this->isAccessTool($i)? "CHECKED":"");
                          $x .= "<tr class=\"tabletext\"><td><input type=checkbox name=grouptools[] value=$i $checked $option_status id=\"check{$i}\"></td><td><label for=\"check{$i}\">".$functions[$i]."</label></td></tr>\n";
                      }
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        # ----------------------------------------------------------------------------------

        # --------------------------------------------------------------------------------------
        # Group Admin access right settings
        #
        function returnAvailableGroupAdminFunction()
        {
                 if ($this->GroupID == "") return array();
                 global $i_GroupSettingsBasicInfo, $i_GroupSettingsMemberList,
                        $i_GroupAdminRightInternalAnnouncement, $i_GroupAdminRightAllAnnouncement,
                        $i_GroupAdminRightInternalEvent, $i_GroupAdminRightAllEvent,
                        $i_GroupAdminRightTimetable, $eComm,
                        $i_GroupAdminRightLinks, $i_GroupAdminRightFiles,
                        $i_GroupAdminRightQB, $i_GroupAdminRightInternalSurvey,
                        $i_GroupAdminRightAllSurvey, $i_frontpage_schoolinfo_groupinfo_group_photoalbum;
                 global $Lang, $plugin;

                 # Basic Information, Member List
                 //$rights = array ($i_GroupSettingsBasicInfo,$i_GroupSettingsMemberList);
                 $rights = array();
                 
                 # Basic Information
                 $rights[] = $i_GroupSettingsBasicInfo;
                 
                 # Member List
                 $MemberListDisplay = $Lang['Group']['AdminRightArr']['MemberManagement'];
                 if ($plugin['eEnrollment'] && $this->RecordType == 5)
                 	$MemberListDisplay .= '<span class="tabletextremark"> ('.$Lang['Group']['Note'].': '.$Lang['Group']['AdminRightArr']['MemberManagement_eEnrolRemarks'].')</span>';
                 $rights[] = $MemberListDisplay;

                 # Internal Announcements
                 $rights[] = $i_GroupAdminRightInternalAnnouncement;

                 if ($this->AnnounceAllowed == 1)
                 {
                     $rights[] = $i_GroupAdminRightAllAnnouncement;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 // $rights[] = ""; commented for error fix

                 $rights[] = $i_GroupAdminRightInternalEvent;

                 /*	# no in IP25
                 if ($this->AnnounceAllowed == 1)
                 {
                     $rights[] = $i_GroupAdminRightAllEvent;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 */
                 $rights[] = "";

                 /*	# no in IP25
                 if ($this->isAccessTimetable())
                 {
                     $rights[] = $i_GroupAdminRightTimetable;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 */
                 $rights[] = "";

//                 if ($this->isAccessBulletin())
//                 {
                     $rights[] = $eComm['Forum'];
//                 }
//                 else
//                 {
//                     $rights[] = "";
//                 }

                 /*	# no in IP25
                 if ($this->isAccessLinks())
                 {
                     $rights[] = $i_GroupAdminRightLinks;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 */
                 $rights[] = "";

//                 if ($this->isAccessFiles())
//                 {
                     $rights[] = $i_GroupAdminRightFiles;
//                 }
//                 else
//                 {
//                     $rights[] = "";
//                 }

                 if ($this->isAccessQB())
                 {
                     $rights[] = $i_GroupAdminRightQB;
                 }
                 else
                 {
                     $rights[] = "";
                 }

                 /*	# no in IP25
                 if ($this->isAccessSurvey())
                 {
                     $rights[] = $i_GroupAdminRightInternalSurvey;
                     if ($this->AnnounceAllowed == 1)
                     {
                         $rights[] = $i_GroupAdminRightAllSurvey;
                     }
                     else
                     {
                         $rights[] = "";
                     }
                 }
                 else
                 {
                     $rights[] = "";
                     $rights[] = "";
                 }
                 */
                 $rights[] = "";
                     $rights[] = "";


//                 if ($this->isAccessPhotoAlbum())
//                 {
//                     $rights[] = $i_frontpage_schoolinfo_groupinfo_group_photoalbum;
//                 }
//                 else
//                 {
                     $rights[] = "";
//                 }
                 return $rights;
        }

        function getSelectAvailableAdminRights ()
        {
                 $rights = $this->returnAvailableGroupAdminFunction();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 for ($i=0; $i<sizeof($rights); $i++)
                 {
                      $name = $rights[$i];
                      if (trim($name)!= "")
                          $x .= "<tr><td><input type=checkbox name=grouptools[] value=$i CHECKED DISABLED></td><td>$name</td></tr>\n";
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function retrieveAdminRights($UserID)
        {
        	if ( $this->AdminRightArray[$this->GroupID][$UserID] && sizeof(( $this->AdminRightArray[$this->GroupID][$UserID]))>0)
        	{
        		 list ($admin, $rights) = $this->AdminRightArray[$this->GroupID][$UserID];
        	} else
        	{
                 $sql = "SELECT RecordType, IF(AdminAccessRight IS NULL OR AdminAccessRight = 0,'ALL',REVERSE(BIN(AdminAccessRight))),
						GroupID, UserID
                         FROM INTRANET_USERGROUP WHERE UserID = '$UserID' ";

                 $result = $this->returnArray($sql,2);

				 foreach ($result as $rec)
				 { 
					$this->AdminRightArray[$rec['GroupID']][$rec['UserID']] =$rec;
				 }
				list ($admin, $rights) = $this->AdminRightArray[$this->GroupID][$UserID];
//				debug_pr($keyedAssoc[1]);
//				debug_pr("keyedAssoc[$this->GroupID][$UserID] = $admin, $rights");
//				$sql = "SELECT RecordType, IF(AdminAccessRight IS NULL OR AdminAccessRight = 0,'ALL',REVERSE(BIN(AdminAccessRight))),
//						UserID
//                         FROM INTRANET_USERGROUP WHERE GroupID = ".$this->GroupID." AND UserID ='$UserID'";
//
//				$result = $this->returnArray($sql,2);
//				
//                 $this->AdminRightArray[$this->GroupID][$UserID] = $result[0];
//                 list ($admin, $rights) = $result[0];

        	}
                 $this->CurrentUserID = $UserID;

                 if ($admin == "A")
                 {
                     $this->isAdmin = true;
                     if ($rights == 0)
                     {
                         $this->AdminAccessRight = "ALL";
                     }
                     else
                     {
                         $this->AdminAccessRight = $rights;
                     }

                 }
                 else
                 {
                     $this->isAdmin = false;
                     $this->AdminAccessRight = "NULL";
                 }
        }

        function isGroupAdmin($UserID)
        {
                 if ($this->GroupID == "") return false;
                 
                 if ($this->CurrentUserID != $UserID)
                 {
                     $this->retrieveAdminRights($UserID);
                 }
                 return $this->isAdmin;
        }
        function hasAllAdminRights($UserID)
        {
                 if ($this->GroupID == "") return false;
                 if ($this->CurrentUserID != $UserID)
                 {
                     $this->retrieveAdminRights($UserID);
                 }
                 if ($this->AdminAccessRight == "ALL")
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
        }
        function hasAdminRight($UserID, $type)
        {
                 if (!$this->isGroupAdmin($UserID)) return false;
                 if ($this->hasAllAdminRights($UserID)) return true;
                 $s = substr($this->AdminAccessRight,$type,1);
                 return ($s == "1");
        }
        function hasAdminBasicInfo ($UserID)
        {
                 return $this->hasAdminRight($UserID, 0);
        }
        function hasAdminMemberList ($UserID)
        {
                 return $this->hasAdminRight($UserID, 1);
        }
        function hasAdminInternalAnnounce ($UserID)
        {
                 return $this->hasAdminRight($UserID, 2);
        }
        function hasAdminAllAnnounce ($UserID)
        {
                 return ($this->AnnounceAllowed==1 && $this->hasAdminRight($UserID, 3) );
        }
        function hasAdminInternalEvent ($UserID)
        {
                 return $this->hasAdminRight($UserID, 4);
        }
        function hasAdminAllEvent ($UserID)
        {
                 return ($this->AnnounceAllowed && $this->hasAdminRight($UserID, 5));
        }
        function hasAdminTimetable ($UserID)
        {
                 return $this->isAccessTimeTable() && $this->hasAdminRight($UserID, 6);
        }
        function hasAdminBulletin ($UserID)
        {
                 //return $this->isAccessBulletin() && $this->hasAdminRight($UserID, 7);
                 return $this->hasAdminRight($UserID, 7);
        }
        function hasAdminLinks ($UserID)
        {
                 return $this->isAccessLinks() && $this->hasAdminRight($UserID, 8);
        }
        function hasAdminFiles ($UserID)
        {
                 //return $this->isAccessFiles() && $this->hasAdminRight($UserID, 9);
                 return $this->hasAdminRight($UserID, 9);
        }
        function hasAdminQB ($UserID)
        {
                 return $this->isAccessQB() && $this->hasAdminRight($UserID, 10);
        }
        function hasAdminInternalSurvey ($UserID)
        {
                 return $this->isAccessSurvey() && $this->hasAdminRight($UserID, 11);
        }
        function hasAdminAllSurvey ($UserID)
        {
                 return ($this->AnnounceAllowed && $this->isAccessSurvey() && $this->hasAdminRight($UserID, 12));
        }
        function hasAdminPhotoAlbum ($UserID)
        {
                 return $this->isAccessPhotoAlbum() && $this->hasAdminRight($UserID, 13);
        }

        function getSelectCurrentAdminRights ($uid)
        {
                 $rights = $this->returnAvailableGroupAdminFunction();
                 
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 if ($this->hasAllAdminRights($uid))
                 {
                     $ch_str = "CHECKED";
                     $dis_str = "DISABLED";
                 }
                 else
                 {
                     $dis_str = "";
                 }
                 
                 for ($i=0; $i<sizeof($rights); $i++)
                 {
                      $name = $rights[$i];
                      if (trim($name)!= "")
                      {
                          if ($this->hasAdminRight($uid,$i))
                          {
                              $ch_str = "CHECKED";
                          }
                          else
                          {
                              $ch_str = "";
                          }
                          $x .= "<tr><td><input type=checkbox name=grouprights[] value=$i $ch_str $dis_str></td><td>$name</td></tr>\n";
                      }
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function getSelectCurrentAdminRights_ip20 ($uid)
        {
                 $rights = $this->returnAvailableGroupAdminFunction();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 if ($this->hasAllAdminRights($uid))
                 {
                     $ch_str = "CHECKED";
                     $dis_str = "DISABLED";
                 }
                 else
                 {
                     $dis_str = "";
                 }
                 for ($i=0; $i<sizeof($rights); $i++)
                 {
                      $name = $rights[$i];
                      if (trim($name)!= "")
                      {
                          if ($this->hasAdminRight($uid,$i))
                          {
                              $ch_str = "CHECKED";
                          }
                          else
                          {
                              $ch_str = "";
                          }
                          $x .= "<tr class='tabletext'><td><input type=checkbox name=grouprights[] value=$i $ch_str $dis_str id=\"check{$i}\"></td><td><label for=\"check{$i}\">$name</label></td></tr>\n";
                      }
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function arrayToHTMLSelect($array,$js_event,$selected)
        {
                 $x = "<SELECT $js_event>\n";
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      list ($id, $name) = $array[$i];
                      $op_sel = ($selected==$id? "SELECTED":"");
                      $x .= "<OPTION value=$id $op_sel>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        # Get drop-down menu according to tools
        function getSelectGroups($js_event,$selected, $AcademicYearID='',$foreComm='')
        {
                 global $UserID, $intranet_session_language;
                 
                 $AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
                 $title_field = $intranet_session_language =="en" ? "a.Title":"a.TitleChinese";
                 if($foreComm) $cond = " AND a.DisplayInCommunity = 1 ";
                 $sql = "SELECT a.GroupID, ". $title_field ." FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.UserID = '$UserID' 
                         and (a.AcademicYearID = '$AcademicYearID' or a.GroupID<=4) $cond
                         ORDER BY a.RecordType+0, ".$title_field;
                         
                 $result = $this->returnArray($sql,2);
                 
                 return getSelectByArray($result,$js_event,$selected,0,1);
        }
        function getFirstSelectGroupsID()
        {
                 global $UserID;
                 $sql = "SELECT a.GroupID FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.UserID = '$UserID'
                         ORDER BY a.RecordType+0, a.Title";
                 $result = $this->returnArray($sql,1);

                 return $result[0][0];
        }

        function getSelectOtherGroups($js_event,$selected, $AcademicYearID='', $foreComm='')
        {
	        global $UserID, $PATH_WRT_ROOT;

	        include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");

			//$lu = new libuser($UserID);
			$lu2007 	= new libuser2007($UserID);

	        $mygroup_str = $lu2007->OtherGroupStr();

	        $AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
	        
	        if($foreComm) $cond = " AND a.DisplayInCommunity = 1 ";
             $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a
                     WHERE
                     '$mygroup_str' not like concat('%:',a.GroupID,':%')
                     and (AcademicYearID = '$AcademicYearID' or GroupID<=4) $cond
                     ORDER BY a.RecordType+0, a.Title";
             $result = $this->returnArray($sql,2);
             return getSelectByArray($result,$js_event,$selected,0,1);

        }


        function getSelectGroupsByTool ($tool, $js_event,$selected)
        {
                 global $UserID, $intranet_session_language;
                 
                 $title_field = $intranet_session_language =="en" ? "a.Title":"a.TitleChinese";
                 $AcademicYearID = Get_Current_Academic_Year_ID();
                 $sql = "
					SELECT 
						a.GroupID, 
						". $title_field ." 
					FROM 
						INTRANET_GROUP as a   
						INNER JOIN INTRANET_USERGROUP as b ON a.GroupID = b.GroupID  AND (a.AcademicYearID = '$AcademicYearID' OR a.GroupID<=4 )
					WHERE 
						b.UserID = '$UserID' 
						AND (a.FunctionAccess IS NULL OR FunctionAccess = 0 OR SUBSTRING(REVERSE(BIN(a.FunctionAccess)),$tool,1)=1)
				";
				
                 $groups = $this->returnArray($sql,2);
                 return $this->arrayToHTMLSelect($groups,$js_event,$selected);
        }

        function getSelectTimetableGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(1,$js_event,$selected);
        }
        function getSelectChatGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(2,$js_event,$selected);
        }
        function getSelectBulletinGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(3,$js_event,$selected);
        }
        function getSelectLinksGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(4,$js_event,$selected);
        }
        function getSelectFilesGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(5,$js_event,$selected);
        }
        function getSelectOthersGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(5,$js_event,$selected);
        }
        function getSelectQBGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(6,$js_event,$selected);
        }
        function getSelectPhotoGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(7,$js_event,$selected);
        }

        function returnAdminToolMapping()            # Map admin right to corresponding tool bit position
        {
                 $adminToolMapping = array();
                 /*
                 $adminToolMapping[6] = 1;
                 $adminToolMapping[7] = 3;
                 $adminToolMapping[8] = 4;
                 $adminToolMapping[9] = 5;
                 $adminToolMapping[10] = 6;
                 $adminToolMapping[12] = 8;
                 $adminToolMapping[13] = 8;
                 */
                 $adminToolMapping[7] = 1;
                 $adminToolMapping[8] = 3;
                 $adminToolMapping[9] = 4;
                 $adminToolMapping[10] = 5;
                 $adminToolMapping[11] = 6;
                 $adminToolMapping[12] = 8;
                 $adminToolMapping[13] = 8;
                 $adminToolMapping[14] = 7;
                 return $adminToolMapping;

        }
        function getSelectGroupsByAdminRight($right,$js_event,$selected)
        {
                 global $UserID, $intranet_session_language;
                 $mapping = $this->returnAdminToolMapping();
                 if ($mapping[$right]!="")
                 {
                     $conds = " AND (a.FunctionAccess IS NULL OR FunctionAccess = 0 OR SUBSTRING(REVERSE(BIN(a.FunctionAccess)),".$mapping[$right].",1) = 1)";
                 }
                 $AcademicYearID = Get_Current_Academic_Year_ID();
                 
                 $title_field = $intranet_session_language=="en" ? "a.Title":"a.TitleChinese";
                 $sql = "SELECT a.GroupID, ". $title_field ."
                         FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND (a.AcademicYearID = '$AcademicYearID' OR a.GroupID<=4 ) AND b.RecordType='A' AND
                         b.UserID = '$UserID' AND
                         (b.AdminAccessRight IS NULL OR b.AdminAccessRight=0 OR SUBSTRING(REVERSE(BIN(b.AdminAccessRight)),$right,1) )
                         $conds";
                 $groups = $this->returnArray($sql,2);
                 return $this->arrayToHTMLSelect($groups,$js_event,$selected);
        }
        function getSelectGroupsByAdminRightsArray($right_array,$js_event,$selected)
        {
                 global $UserID, $intranet_session_language;
                 $conds = "";
                 $aconds = "";
                 $mapping = $this->returnAdminToolMapping();
                 for ($i=0; $i<sizeof($right_array); $i++)
                 {
                      $conds .= " OR SUBSTRING(REVERSE(BIN(b.AdminAccessRight)),".$right_array[$i].",1)=1";
                      $mapTool = $mapping[$right_array[$i]];
                      if ($mapTool != "")
                          $aconds = " OR SUBSTRING(REVERSE(BIN(a.FunctionAccess)),$mapTool,1) = 1";

                 }
                 if ($aconds != "")
                 {
                     $aconds = " AND (a.FunctionAccess IS NULL OR FunctionAccess = 0 $aconds )";
                 }
                 
                 $AcademicYearID = Get_Current_Academic_Year_ID();
                 $title_field = $intranet_session_language=="en" ? "a.Title":"a.TitleChinese";
                 
                 $sql = "SELECT a.GroupID, ". $title_field ."
                         FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND (a.AcademicYearID = '$AcademicYearID' OR a.GroupID<=4 ) 
						AND b.RecordType='A' 
						AND b.UserID = '$UserID' 
						AND (b.AdminAccessRight IS NULL OR b.AdminAccessRight=0 $conds ) $aconds";
                 $groups = $this->returnArray($sql,2);
                 return $this->arrayToHTMLSelect($groups,$js_event,$selected);
        }
        function getSelectAdminBasicInfo($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRight(1,$js_event,$selected);
        }
        function getSelectAdminMember($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRight(2,$js_event,$selected);
        }
        function getSelectAdminQB($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRight(11,$js_event,$selected);
        }
        function getSelectAdminAnnounce($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(3,4),$js_event,$selected);
        }
        function getSelectAdminEvent($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(5,6),$js_event,$selected);
        }
        function getSelectAdminBulletin($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(8),$js_event,$selected);
        }
        function getSelectAdminShared($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(9,10),$js_event,$selected);
        }
        function getSelectAdminSurvey($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(12,13),$js_event,$selected);
        }
        function getSelectAdminPhotoAlbum($js_event, $selected="")
        {
                 return $this->getSelectGroupsByAdminRight(14,$js_event,$selected);
        }
        function returnGroupEvents($filter)
        {
                 switch ($filter)
                 {
                         case 1: $conds = ""; break;
                         case 2: $conds = " AND b.EventDate >= CURDATE()"; break;
                         case 3: $conds = " AND b.EventDate < CURDATE()"; break;
                         default: $conds = " AND b.EventDate >= CURDATE()"; break;
                 }
                 $namefield = getNameFieldWithClassNumberByLang("d.");
                 $sql = "SELECT DISTINCT b.EventID, b.Title,DATE_FORMAT(b.EventDate, '%Y-%m-%d'), c.Title, $namefield,IF((LOCATE(';$UserID;',b.ReadFlag)=0 || b.ReadFlag Is Null), 1, 0)
                         FROM INTRANET_GROUPEVENT as a LEFT OUTER JOIN INTRANET_EVENT as b ON a.EventID = b.EventID
                              LEFT OUTER JOIN INTRANET_GROUP as c ON c.GroupID = b.OwnerGroupID
                              LEFT OUTER JOIN INTRANET_USER as d ON b.UserID = d.UserID
                         WHERE a.GroupID = ".$this->GroupID." $conds ORDER BY b.EventDate, b.Title";
                 return $this->returnArray($sql,6);
        }
        function displayGroupEventInGroup($filter)
        {
                 global $i_general_sysadmin,$i_EventTitle,$i_EventPoster,$i_EventDate;
                 $events = $this->returnGroupEvents($filter);
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#FCEA88 bordercolordark=#FFFFFF bgcolor=#FCEA88>
                    <tr>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_EventDate</strong></td>
                      <td align=left bgcolor=#FFC800 class=body><strong>$i_EventTitle</strong></td>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_EventPoster</strong></td>
                    </tr>";
                if(sizeof($events)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($events); $i++)
                        {
                            list ($EventID,$EventTitle,$EventDate,$ownerGroup,$poster,$IsNew) = $events[$i];
                            if ($ownerGroup == "")
                            {
                                $displayName = "$i_general_sysadmin";
                            }
                            else
                            {
                                $displayName = "$poster <br> $ownerGroup";
                            }
                            $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                            $x .= "<tr>\n";
                            $x .= "<td valign=top class=body>
                                    <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                      <tr>
                                        <td width=30>$newFlag</td>
                                        <td>$EventDate</td>
                                      </tr>
                                    </table>
                                   </td>
                                   <td align=left valign=top class=bodylink><a class=bodylink href=javascript:fe_view_event($EventID)>$EventTitle</a></td>
                                   <td valign=top class=bodylink>
                                   <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                     <tr>
                                       <td>$displayName</td>
                                     </tr>
                                   </table>
                                  </td>\n";
                            $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }

        # INTRANET_COURSE
        function getCourseGroupSubjectID($CourseID)
        {
                 $sql = "SELECT ClassGroupID,SubjectID FROM INTRANET_COURSE WHERE CourseID = '$CourseID'";
                 $result = $this->returnArray($sql,2);
                 $record = $result[0];
                 return (sizeof($record)==0? false: $record);
        }

        function getSubjectLeaderSelection ($name1, $name2, $targetUserID)
        {
                 $ClassID = $this->GroupID;
                 # Check it is normal class or special class
                 $sql = "SELECT ClassID FROM YEAR_CLASS WHERE GroupID = '$ClassID'";
                 $result = $this->returnVector($sql);
                 if ($result[0]!="")           # Class
                 {
                     $ClassTableID = $result[0];


                     $sql = "SELECT DISTINCT a.SubjectID, a.SubjectName, IF(b.LeaderID IS NULL,'0','1')
                             FROM INTRANET_SUBJECT_TEACHER as c
                                  LEFT OUTER JOIN INTRANET_SUBJECT as a ON c.SubjectID = a.SubjectID AND a.RecordStatus = 1
                                  LEFT OUTER JOIN INTRANET_SUBJECT_LEADER as b ON a.SubjectID = b.SubjectID AND b.ClassID = '$ClassID' AND b.UserID = '$targetUserID'
                             WHERE c.ClassID = '$ClassTableID'
                             ORDER BY a.SubjectName";


                 }
                 else                          # Special
                 {
                     $sql = "SELECT a.SubjectID, a.SubjectName, IF(b.LeaderID IS NULL,'0','1')
                             FROM INTRANET_SUBJECT as a
                                  LEFT OUTER JOIN INTRANET_SUBJECT_LEADER as b ON a.SubjectID = b.SubjectID AND b.ClassID ='$ClassID' AND b.UserID = '$targetUserID'
                             WHERE a.RecordStatus = 1
                             ORDER BY a.SubjectName";
                 }
                 #echo "1. $sql\n";
                 $result = $this->returnArray($sql,3);
                 $x = "<table width=100% border=0 cellpadding=5 cellspacing=0>\n";
                 $x .= "<tr><td class=tableContent width=50%>\n";
                 $x .= "<select name=$name1 size=10 multiple>\n";
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list($subjectID, $name, $leader) = $result[$i];
                      if ($leader == 1)
                      {
                          $x .= "<option value=$subjectID>$name</OPTION>\n";
                      }
                 }
                 $x .= "<option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>\n";
                 $x .= "</SELECT>\n";
                 $x .= "</td>\n";
                 $x .= "<td class=tableContent align=center>
<p><input type='image' src='/images/admin/button/s_btn_addto_b5.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"$name2\"],this.form.elements[\"$name1\"]);return false;'></p>
<p><input type='image' src='/images/admin/button/s_btn_deleteto_b5.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"$name1\"],this.form.elements[\"$name2\"]);return false;'></p>
                 </td>
<td class=tableContent width=50%>";
                 $x .= "<select name=$name2 size=10 multiple>\n";
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list($subjectID, $name, $leader) = $result[$i];
                      if ($leader != 1)
                      {
                          $x .= "<option value=$subjectID>$name</OPTION>\n";
                      }
                 }
                 $x .= "<option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>\n";
                 $x .= "</select>\n";
                 $x .= "</td></tr></table>\n";
                 return $x;
        }

        function getGroupsID($returnVector=0)
        {
			global $UserID;
//			$sql = "SELECT 
//						a.GroupID 
//					FROM 
//						INTRANET_GROUP as a 
//						INNER JOIN INTRANET_USERGROUP as b ON  a.GroupID = b.GroupID
//					WHERE 
//						b.UserID = '$UserID'";
//			return $this->returnArray($sql,1);
			return $this->getTargetGroupsID($UserID, $returnVector);

        }

        function getTargetGroupsID($TarUserID, $returnVector=0)
        {
			global $UserID;
			$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
			$sql = "SELECT 
						a.GroupID 
					FROM 
						INTRANET_GROUP as a 
						INNER JOIN INTRANET_USERGROUP as b ON  a.GroupID = b.GroupID
					WHERE 
						b.UserID = '$TarUserID'
						AND 
						(
							a.AcademicYearID = '$CurrentAcademicYearID'
							OR a.GroupID<=4
						)
			";
//			$sql = "SELECT a.GroupID FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
//				WHERE a.GroupID = b.GroupID AND b.UserID = $TarUserID";

			return $returnVector==1?$this->returnVector($sql,1):$this->returnArray($sql,1);


        }

        //added on 4 Aug 08
       function getStudentSelectByGroup($group,$tags,$selected="", $all=0, $type="", $typeID=-1)
       {
	        global $button_select, $button_select_all;
	        global $i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved;

	        $list = array();

	        // edited by Ivan (24 July 08) adding $GroupID to hide existing member in the list
	        if ($type==""){	//no specific type => list all students in the group
	        	$list = $this->returnStudentListByGroup($group);
	        }
	        else{
	            if ($typeID==-1){	//no ID number => show all students in the group
		            $list = $this->returnStudentListByGroup($group);
	            }
	            else if ($type==0){	//filter group member
	        		$EnrolGroupID = $typeID;

	        		$name_field = getNameFieldWithClassNumberByLang("b.");
	            	//get all UserID of members
					$sql = "SELECT b.UserID, $name_field
							FROM
									INTRANET_USERGROUP as a
								LEFT JOIN
									INTRANET_ENROL_GROUPSTUDENT as c ON a.UserID = c.StudentID
								INNER JOIN
									INTRANET_USER as b ON a.UserID = b.UserID
							WHERE (a.EnrolGroupID = '$EnrolGroupID' AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2) AND c.RecordStatus IN (0,2))
							";
					$memberIDArr = $this->returnArray($sql,1);

					if (sizeof($memberIDArr)==0){	//no one is member => show all
						$list = $this->returnStudentListByGroup($group);
					}
					else{	//hide the member in the list

						//contruct a string of NOT IN list for sql
						$filterList = "";
						for ($i=0; $i<sizeof($memberIDArr); $i++){
							$filterList .= $memberIDArr[$i][0];
							if ($i != sizeof($memberIDArr)-1){
								$filterList .= ", ";
							}
						}

						$filterList = "AND a.UserID NOT IN (".$filterList.")";
						$list = $this->returnStudentListByGroup($group, $filterList);
					}
				}
				else if ($type==1 || $type==3){	//filter activity joiner
			        $EnrolEventID = $typeID;

			        $name_field = getNameFieldWithClassNumberByLang("b.");
	            	//get all UserID of members
					$sql = "SELECT b.UserID, $name_field
							FROM INTRANET_ENROL_EVENTSTUDENT as a INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
							WHERE (a.EnrolEventID = '$EnrolEventID' AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2) AND a.RecordStatus IN (0,2))
							";
					$memberIDArr = $this->returnArray($sql,1);

					if (sizeof($memberIDArr)==0){	//no one joined yet => show all
						$list = $this->returnStudentListByGroup($group);
					}
					else{	//hide the member in the list

						//contruct a string of NOT IN list for sql
						$filterList = "";
						for ($i=0; $i<sizeof($memberIDArr); $i++){
							$filterList .= $memberIDArr[$i][0];
							if ($i != sizeof($memberIDArr)-1){
								$filterList .= ", ";
							}
						}
						$filterList = "AND a.UserID NOT IN (".$filterList.")";
						$list = $this->returnStudentListByGroup($group, $filterList);
					}
		        }
		        else{	//unknown type => show all students in the class
		            $list = $this->returnStudentListByGroup($group);
	            }
	        }



	        $x = "<SELECT $tags>\n";
	        $empty_selected = ($selected == '')? "SELECTED":"";
	        //$x .= "<OPTION value='' $empty_selected>{$button_select}</OPTION>\n";

	        if($all)
	        $x .= "<OPTION value='-1'>{$button_select_all}</OPTION>\n";

	        for ($i=0; $i<sizeof($list); $i++)
	        {
	             list($id,$name) = $list[$i];
	             $status = "";

	             # show student record status
                 if ($typeID==-1)
                 {
                  	$status = "";
                 }
                 else if ($type==0 || $type==2)
                 {
                     $sql = "SELECT
                     				CASE RecordStatus
						                   WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
						                   WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
						                   WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
						            END
						     FROM INTRANET_ENROL_GROUPSTUDENT
						     WHERE EnrolGroupID = '$typeID' AND StudentID = '$id'
						     ";
					 $result = $this->returnVector($sql);
					 if ($result[0]!="")
					 {
					 	$status = "[".$result[0]."]";
				 	 }
                 }
                 else if ($type==1 || $type==3)
                 {
                     $sql = "SELECT
                     				CASE RecordStatus
						                   WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
						                   WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
						                   WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
						            END
						     FROM INTRANET_ENROL_EVENTSTUDENT
						     WHERE EnrolEventID = '$typeID' AND StudentID = '$id'
						     ";
					 $result = $this->returnVector($sql);
					 if ($result[0]!="")
					 {
					 	$status = "[".$result[0]."]";
				 	 }
                 }


	             $sel_str = ($selected == $id? "SELECTED":"");
	             $x .= "<OPTION value=$id $sel_str>$name $status</OPTION>\n";
	        }
	        $x .= "</SELECT>\n";
	        return $x;
       }

        //added on 4 Aug 08
       function returnStudentListByGroup($group_id, $filter="")
       {
                $name_field = getNameFieldWithClassNumberByLang("a.");
                $sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a INNER JOIN INTRANET_USERGROUP as b on a.UserID = b.UserID
                        WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND b.GroupID = '$group_id' $filter ORDER BY a.ClassNumber";
                //$sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a
                //        WHERE a.RecordType = 2 AND a.RecordStatus != 3 AND a.ClassName = '$class_name' ORDER BY a.ClassNumber";

                return $this->returnArray($sql,2);
       }
       
       # added by Henry Chow on 20110621 (used in Digital Archive)
       function returnStaffListByGroup($group_id, $filter="")
       {
                $name_field = getNameFieldWithClassNumberByLang("a.");
                $sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a INNER JOIN INTRANET_USERGROUP as b on a.UserID = b.UserID WHERE a.RecordType = 1 AND a.RecordStatus IN (0,1,2) AND b.GroupID = '$group_id' $filter GROUP BY a.UserID ORDER BY a.ClassNumber";
                

                return $this->returnArray($sql,2);
       }
       
		function returnGroupFunctionAccessValue($BinValue)
		{
			$BinValue = $BinValue =="ALL" ? "11111111" : $BinValue;
			$x = 0;
			for($i=0;$i<strlen($BinValue);$i++)
			{
				if($BinValue[$i])
				{
					$x += pow(2,$i);
				}
			}
			return $x;
		}
		
		function setPublicStatus($ParPublicStatus=0) {
			$this->PublicStatus = $ParPublicStatus;
		}		
		
		function setFunctionAccess($ParEnable=true, $ParTargetFuncAccess=-1) {
			if ($ParTargetFuncAccess>-1) {
				//$functionAccess = pow(2, $ParTargetFuncAccess);
				//$fa = $this->returnGroupFunctionAccessValue($this->FunctionAccess);
				$fa = $this->FunctionAccess;
				if ($ParEnable) {
					if (!$this->isAccessTool($ParTargetFuncAccess)) {
						$fa[$ParTargetFuncAccess] = 1;
						$fa =  $fa == "11111111" ? "NULL" : $fa; // set NULL when full rights
//						$fa += $functionAccess;
//						$fa = $fa == 255 ? "NULL" : $fa; // set NULL when full rights
					}
				} else {
					if ($this->isAccessTool($ParTargetFuncAccess)) {
						$fa[$ParTargetFuncAccess] = 0;
//						$fa -= $functionAccess;
					}
				}
//				if($fa != "NULL") {
//					$fa = base_convert($fa, 10, 2);
//				}
				$this->FunctionAccess = $fa;
			}	
		}
		
		function UPDATE_GROUP_TO_DB() {
			$AlbumAccess = $this->AllowedImageTypes!="DISABLED" || $this->AllowedVideoTypes!="DISABLED";
			$FileAccess = $this->AllowedFileTypes!="DISABLED";
			$WebsiteAccess = $this->AllowedWebsite==1;
			
			$this->setFunctionAccess($AlbumAccess,6);
			$this->setFunctionAccess($FileAccess,4);
			$this->setFunctionAccess($WebsiteAccess,3);
			
			if ($this->FunctionAccess != "NULL" && $this->FunctionAccess != "ALL") {
				$functionAccessToChange = base_convert($this->reverse($this->FunctionAccess), 2, 10);
			} else {
				$functionAccessToChange = "ALL";
			}
			$sql = "
				UPDATE INTRANET_GROUP SET
					TITLE = '" . $this->Title . "',
					DESCRIPTION = '" . $this->Description . "',
					RECORDTYPE = '" . $this->RecordType . "',
					RECORDSTATUS = '" . $this->RecordStatus . "',
					DATEINPUT = '" . $this->DateInput . "',
					DATEMODIFIED = '" . $this->DateModified . "',
					URL = '" . $this->URL . "',
					STORAGEQUOTA = '" . $this->StorageQuota . "',
					ANNOUNCEALLOWED = '" . $this->AnnounceAllowed . "',
					FUNCTIONACCESS = " . ($functionAccessToChange == "ALL"?"NULL":"'".$functionAccessToChange."'") . ",
					PUBLICSTATUS = '" . $this->PublicStatus . "',
					GROUPLOGOLINK = '" . $this->GroupLogoLink . "',
					INDEXANNOUNCENO = '" . $this->IndexAnnounceNo . "',
					SHARINGLATESTNO = '" . $this->SharingLatestNo . "',
					NEEDAPPROVAL = '" . $this->needApproval . "',
					DEFAULTVIEWMODE = '" . $this->DefaultViewMode . "',
					ALLOWEDIMAGETYPES = '" . $this->AllowedImageTypes . "',
					ALLOWEDFILETYPES = '" . $this->AllowedFileTypes . "',
					ALLOWEDVIDEOTYPES = '" . $this->AllowedVideoTypes . "',
					CALID = '" . $this->CalID . "',
					CALDISPLAYINDEX = '" . $this->CalDisplayIndex . "',
					CALPUBLICSTATUS = '" . $this->CalPublicStatus . "',
					ALLOWEDWEBSITE = '" . $this->AllowedWebsite . "',
					AllowDeleteOthersAnnouncement = '" . $this->AllowDeleteOthersAnnouncement . "'
				WHERE
					GROUPID = '" . $this->GroupID . "'
				";
			$this->db_db_query($sql) or die(mysql_error());
			return mysql_affected_rows();
		}
		
		function Get_Group_User($UserType='')
		{
			$conds_usertype = '';
			if ($UserType != '')
				$conds_usertype = " And iu.RecordType In ($UserType) ";
				
			$name_field = getNameFieldByLang('iu.');
			$sql = "Select
							iug.UserID,
							$name_field
					From
							INTRANET_USERGROUP as iug
							Inner Join
							INTRANET_USER as iu
							On (iug.UserID = iu.UserID)
					Where
							iug.GroupID = '".$this->GroupID."'
							And
							iu.RecordStatus = 1
							$conds_usertype
					Group By
							iu.UserID
					Order By
							iu.EnglishName
					";
			$returnArr = $this->returnArray($sql);
			return $returnArr;
		}
		
		function Get_Group_Mgmt_Copy_UI($CopyFrom, $CopyTo, $GroupArr='All', $MemberArr='All')
		{
			global $PATH_WRT_ROOT, $plugin, $linterface, $Lang, $intranet_session_language;
			
			if($CopyFrom == $CopyTo)
			{
				$tbody .= '<tr>';
					$tbody .= '<td class="tabletext" align="center" colspan=4 style="padding:30px">'.$Lang['Group']['warnSelectDifferentYear'].'</td>';
				$tbody .= '</tr>';
			}
			else
			{
				# exclude class group (commented, do not exclude class group not linked to class)
				//$cond .= " AND RecordType <> 3 ";
				
				# exclude class group
				if($plugin['eEnrollment'])
				{ 
					$cond .= " AND RecordType <> 5 ";
				}
				
				# select groups of current year 
				$curr_year = " AND AcademicYearID = '$CopyFrom' ";
				$cond .= " AND RecordType <> 0 "; // dont copy identity (by group cat)
				$cond .= " AND GroupID > 4 "; // dont copy identity (by GroupID)
				$GroupList = $this->returnAllGroupIDAndName($curr_year.$cond," RecordType ");
				
				# select groups of target year
				$target_year = " AND AcademicYearID = '$CopyTo' ";
				$targetYearGroupList = $this->returnAllGroupIDAndName($target_year.$cond," RecordType ");
				$targetYearGroupNameList = Get_Array_By_Key($targetYearGroupList,"Title");

				# excluded campus tv and alumni group
				global $tv_bulletin_groupid, $alumni_GroupID;
				@include_once($PATH_WRT_ROOT."plugins/alumni_conf.php");
				@include_once($PATH_WRT_ROOT."plugins/tv_conf.php");
	
				if(count($GroupList)>0)
				{
					$CategoryList = $this->returnAllCategory();
									
					$CopyGroupList = array();
					foreach((array)$GroupList as $thisGroup)
					{
						list($GroupID,$Title,$AcademicYear,$CategoryID, $TitleChinese) = $thisGroup; 

						if(in_array($GroupID,array($tv_bulletin_groupid, $alumni_GroupID)))
							continue;

						$TitleDisplay = $intranet_session_language == "en" ? $Title : $TitleChinese;
						
						if($CategoryID == 3 && $this->returnLinkedClass($GroupID)) // skip class group which linked to class
							continue;
					
						$CategoryName = $CategoryList[$CategoryID];
						$tbody .= '<tr>';
							$tbody .= '<td class="tabletext">'.$CategoryName.'</td>';
							$tbody .= '<td class="tabletext">'.$TitleDisplay.'</td>';

							if(in_array($Title,$targetYearGroupNameList))
							{
								$tbody .= '<td class="tabletext" align="center" colspan="2"><span class="tabletextremark">'.$Lang['Group']['GroupExistInTargetYear'].'</span></td>';
							}
							else
							{
								$isGroupChecked = $GroupArr=='All'||in_array($GroupID,(array)$GroupArr)?'checked':'';
								$GroupCheckBox = $linterface->Get_Checkbox($ID = 'Group'.$GroupID, $Name = 'CopyGroup[]', $Value=$GroupID, $isGroupChecked, $Class='Group', $Display='', $Onclick='UpdateMemberCheckBox('.$GroupID.')', $Disabled='');
								$isMemberChecked = $MemberArr=='All'||in_array($GroupID,(array)$MemberArr)?'checked':'';
								$MemberCheckBox = $linterface->Get_Checkbox($ID = 'Member'.$GroupID, $Name = 'CopyMember[]', $Value=$GroupID, $isMemberChecked, $Class='Member', $Display='', $Onclick='', $Disabled='');
							
								$tbody .= '<td class="tabletext" align="center">'.$GroupCheckBox.'</td>';
								$tbody .= '<td class="tabletext" align="center">'.$MemberCheckBox.'</td>';
								$ctr++;
							}
						$tbody .= '</tr>';
						
						$HiddenValue .=  '<input name="GroupIDList[]" type="hidden" value="'.$GroupID.'">' ;
						$HiddenValue .=  '<input name="GroupCatList[]" type="hidden" value="'.$CategoryName.'">' ;
						$HiddenValue .=  '<input name="GroupNameList[]" type="hidden" value="'.$Title.'">' ;
						$HiddenValue .=  '<input name="GroupNameChineseList[]" type="hidden" value="'.$TitleChinese.'">' ;
							
					}
				}
				
				if(empty($tbody))
				{
					$tbody .= '<tr>';
						$tbody .= '<td class="tabletext" align="center" colspan=4 style="padding:30px">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
					$tbody .= '</tr>';
				}
			}
			
			### Get Check Box
			$isAllGroupChecked = $GroupArr=='All'||$ctr == count($GroupArr)?'checked':'';
			$CheckAllGroup = $linterface->Get_Checkbox($ID = 'CheckAllGroup', $Name = '', $Value, $isAllGroupChecked, $Class='', $Display='', $Onclick='CheckAll(\'Group\'); UpdateMemberCheckBox();', $Disabled='');
			$isAllMemberChecked = $MemberArr=='All'||$ctr == count($MemberArr)?'checked':''; 
			$CheckAllMember = $linterface->Get_Checkbox($ID = 'CheckAllMember', $Name = '', $Value, $isAllMemberChecked, $Class='Member', $Display='', $Onclick='CheckAll(\'Member\'); ', $Disabled='');
			
			$x .= '<table border="0" cellpadding="0" cellspacing="0" class="common_table_list">';
				$x .= '<thead>';
					$x .= '<tr>';
						$x .= '<th width="35%">'.$Lang['Group']['Category'].'</td>';
						$x .= '<th width="35%">'.$Lang['Group']['Group'].'</td>';
						$x .= '<th width="15%" style="text-align:center">'.$Lang['Group']['Copy'].'<br>'.$CheckAllGroup.'</td>';
						$x .= '<th width="15%" style="text-align:center">'.$Lang['Group']['CopyMember'].'<br>'.$CheckAllMember.'</td>';
					$x .= '</tr>';
				$x .= '</thead>';
				$x .= '<tbody>';
					$x .= $tbody;
				$x .= '</tbody>';
			$x .= '</table>';
			$x .= $HiddenValue;		
			
			return $x;	
		}
		
		function Copy_Group_And_Member($FromYear, $ToYear, $GroupArr, $MemberArr)
		{
		    global $PATH_WRT_ROOT;
		    global $ssoservice;
		    include_once($PATH_WRT_ROOT."includes/settings.php");
		    if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
		        include_once ($PATH_WRT_ROOT . "includes/google_api/libgoogleapi.php");
		        include_once ($PATH_WRT_ROOT . "includes/sso/libSSO_db.php");
		        include_once ($PATH_WRT_ROOT . "includes/sso/libGoogleSSO.php");
		        $SSOEnabled = true;
		        $libGoogleSSO = new libGoogleSSO();
		    }
			
			$GroupStr = implode(",",$GroupArr);
		
			$fieldnameArr = array(
				"Title", 
				"TitleChinese", 
				"Description", 
				"RecordType", 
				"StorageQuota", 
				"FunctionAccess", 
				"DisplayInCommunity", 
				"AnnounceAllowed",
				"URL",
				"PublicStatus",
				"GroupLogoLink",
				"IndexAnnounceNo",
				"ForumAttStatus",
				"SharingLatestNo",
				"needApproval",
				"DefaultViewMode",
				"AllowedImageTypes",
				"AllowedFileTypes",
				"AllowedVideoTypes",
				"CalDisplayIndex",
				"CalPublicStatus",
				"AllowedWebsite",
				"AllowDeleteOthersAnnouncement"
			);
			$fieldname = implode(",",$fieldnameArr);
			
			$sql = "
				SELECT 
					$fieldname ,GroupID
				FROM
					INTRANET_GROUP
				WHERE
					AcademicYearID = '$FromYear'
					AND	GroupID IN ($GroupStr)			
			";

			$result = $this->returnArray($sql);
			
			// loop copying each group 
			foreach ($result as $groupinfo)
			{
				$OldGroupID = $groupinfo["GroupID"];
				$thisValueArr = array();
				foreach($fieldnameArr as $thisfield)
				{
					$thisValueArr[$thisfield] = (trim($groupinfo[$thisfield])==''?'NULL':('"'.$groupinfo[$thisfield]).'"');
				}
				
				$fieldvalue = implode(",",(array)$thisValueArr);
					
				$sql = "INSERT INTO INTRANET_GROUP ($fieldname, AcademicYearID , CopyFromGroupID, DateInput,DateModified) VALUES ($fieldvalue, '$ToYear', $OldGroupID, NOW(),NOW())";
				$this->db_db_query($sql);

				$GroupID = $this->db_insert_id();

				if($GroupID)
				{
					$SuccessArr["CopyGroup"][$OldGroupID] = true;
					$AdditionalTaskSuccessArr = $this->Group_Adding_Additional_Task($GroupID,$groupinfo["Title"],$groupinfo["Description"],$groupinfo["RecordType"]);
					
					if ($SSOEnabled) {
					    $GroupIDArr = array($GroupID);
					    $results = $libGoogleSSO->syncGroupFromEClassToGoogle($this, $GroupIDArr,$ToYear);
					}
					
					# copy group member
					if(in_array($OldGroupID,(array)$MemberArr))
					{
						if(!empty($AdditionalTaskSuccessArr["EnrolGroupID"]))
						{
							$EnrolGroupField = ",EnrolGroupID ";
							$EnrolGroupValue = ",".$AdditionalTaskSuccessArr["EnrolGroupID"];
						}
						
						if(!empty($AdditionalTaskSuccessArr["ICalID"]))
						{
							$ICalID = $AdditionalTaskSuccessArr["ICalID"];
						}
						
						if(!empty($AdditionalTaskSuccessArr["EnrolGroupID"]))
						{
							$EnrolGroupField = ",EnrolGroupID ";
							$EnrolGroupValue = ",".$AdditionalTaskSuccessArr["EnrolGroupID"];
						}
						
						$sql = "
							SELECT 
								DISTINCT a.UserID, a.RoleID, a.RecordType, a.AdminAccessRight 
							FROM 
								INTRANET_USERGROUP a 
								INNER JOIN INTRANET_USER b ON a.UserID = b.UserID 
								LEFT JOIN YEAR_CLASS_USER ycu ON ycu.UserID = b.UserID
								LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID
							WHERE 
								a.GroupID = '$OldGroupID' 
								AND b.RecordStatus = 1
						";

						$CopyUserList = $this->returnArray($sql);
						
						if(count($CopyUserList)>0)
						{
							$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, RecordType, AdminAccessRight $EnrolGroupField, DateInput, DateModified) VALUES ";
							$cal_sql = "insert into CALENDAR_CALENDAR_VIEWER (CalID, UserID, GroupID, GroupType, Access, Color, Visible) values "; 
							
							$delimiter = '';
							foreach((array)$CopyUserList as $UserInfo)
							{
								foreach($UserInfo as $key=>$value)
									$UserInfo[$key] = $value?"'".$value."'":"NULL";
								$sql .= $delimiter."('$GroupID',".$UserInfo["UserID"].",".$UserInfo["RoleID"].",".$UserInfo["RecordType"].",".$UserInfo["AdminAccessRight"]." ".$EnrolGroupValue.",now(),now())";
								$cal_sql .= $delimiter."($ICalID, ".$UserInfo["UserID"].", $GroupID, 'E', 'R', '2f75e9', '1')";
								
								$delimiter = ",";
							}
						
							$SuccessArr["CopyMember"][$OldGroupID] = $this->db_db_query($sql);
							$SuccessArr["CalViewer"][$OldGroupID] = $this->db_db_query($cal_sql);
							
							if ($SSOEnabled) {
							    $userIDArr = array();
							    foreach ($CopyUserList as $value) {
							        $userIDArr[] = $value['UserID'];
							    }
							    $userIDArr = array_unique($userIDArr);
							    $user_groups = $libGoogleSSO->enabledUserForGoogle($this, $userIDArr);
							    if (count($user_groups['activeUsers'])>0) {
							        for ($i = 0; $i < count($user_groups['activeUsers']); $i++) {
							            $sql = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $user_groups['activeUsers'][$i] . '\';';
							            $userlogin = current($this->returnVector($sql));
							            $libGoogleSSO->syncGroupMemberFromEClassToGoogle($userlogin, $GroupID);
							        }
							    }
							}
						}
					}
				}
				else
					$SuccessArr["CopyGroup"][$OldGroupID] = false;
			}

			return $SuccessArr;
			
		}
		
		# 20100715 Marcus
		# This function was called in following file
		# 1. group/import.php
		# 2. group/new_updated.php
		# 3. includes/libgroup.php -> function Copy_Group_And_Member (Called in group/copy_result.php)
		# Please also modified the above file if need.
		function Group_Adding_Additional_Task($GroupID,$Title,$Description,$RecordType)
		{
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/icalendar.php");
			$iCal = new icalendar();
			
			$calID = $iCal->createSystemCalendar($Title, 2,'P',$Description);
			if (!empty($calID)){
				// echo $calID;
				// exit;
				$sql = "Update INTRANET_GROUP set CalID = '$calID' where GroupID='$GroupID'";
				$this->db_db_query($sql);
				$SuccessArr['Update_ICal'] = mysql_affected_rows()>0;
				$SuccessArr['ICalID'] = $calID;
			}
			
			# Match with INTRANET_CLASS
//			if ($RecordType == 3)
//			{
//				$sql = "UPDATE INTRANET_CLASS SET GroupID = $GroupID WHERE ClassName = '$Title'";
//				$this->db_db_query($sql);
//				$SuccessArr['Match_INTRANET_CLASS'] = mysql_affected_rows()>0;
//			}
		
			// Old coding, commented by Marcus 20100715 
//			# INTRANET_ORPAGE_GROUP
//			if ($hide==1)
//			{
//				include_once($PATH_WRT_ROOT."includes/liborganization.php");
//				$lorg = new liborganization();
//				$SuccessArr['setGroupHidden'] = $lorg->setGroupHidden($GroupID);
//			}
		
			# INTRANET_ENROL_GROUPINFO - set default status to 0
			if ($RecordType == 5)
			{
				include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
				$libenroll = new libclubsenrol();
				
				$DataArr = array();
				$DataArr['GroupID'] = $GroupID;
				$DataArr['ClubType'] = 'Y';
				$DataArr['ApplyOnceOnly'] = 0;
				$DataArr['FirstSemEnrolOnly'] = 0;
				$SuccessArr['Insert_Group_Main_Info'] = $libenroll->Insert_Group_Main_Info($DataArr);
						
				### Create Enrolment Club Record
				$SuccessArr['Insert_Year_Based_Club'] = $libenroll->Insert_Year_Based_Club($GroupID);
				$SuccessArr['EnrolGroupID'] = mysql_insert_id();
				
				//$sql = "INSERT INTO INTRANET_ENROL_GROUPINFO (GroupID, Quota, RecordStatus) VALUES ($GroupID, 0, 0)";
				//$li->db_db_query($sql);
			}
			
			return $SuccessArr;
		}
		
		function returnLinkedClass($ParGroupID='')
		{
			if(trim($ParGroupID)== '')
				$ParGroupID = $this->GroupID;
			
			if(trim($ParGroupID)== '')
				return false;
				
			$sql = " SELECT ClassID, ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE GroupID = '$ParGroupID'";
			$result = $this->returnArray($sql);
			
			return $result;
			
		}
		function reverse($fa)
		{
			for($i=strlen($fa)-1;$i>=0;$i-- )
				$reversed .= $fa[$i];
				
			return $reversed;
		}
		
		function getTopTabInfoAry($curTab) {
			global $Lang;
			
			switch ($curTab) {
				case 'group':
					$groupPage = 'index.php?clearCoo=1';
					$groupCategoryPage = 'groupcategory/';
					$groupRolePage = 'role/';
					break;
				case 'groupMember':
					$groupPage = '../?clearCoo=1';
					$groupCategoryPage = '../groupcategory/';
					$groupRolePage = '../role/';
					break;
				case 'groupCategory':
					$groupPage = '../index.php?clearCoo=1';
					$groupCategoryPage = 'index.php';
					$groupRolePage = '../role/';
					break;
				case 'groupRole':
					$groupPage = '../index.php?clearCoo=1';
					$groupCategoryPage = '../groupcategory/';
					$groupRolePage = 'index.php';
					break;
			}
			
			$TAGS_OBJ = array();
			$TAGS_OBJ[] = array($Lang['Group']['GroupMgmtCtr'], $groupPage, ($curTab=='group'||$curTab=='groupMember'));
			$TAGS_OBJ[] = array($Lang['Group']['GroupCatSetting'], $groupCategoryPage, ($curTab=='groupCategory'));
			$TAGS_OBJ[] = array($Lang['Group']['RoleSetting'], $groupRolePage, ($curTab=='groupRole'));
			
			return $TAGS_OBJ;
		}
		
		function displayGroupSelection($tag="", $defaultValue="")
		{
			global $intranet_session_language;
			$title_field = $intranet_session_language =="en" ? "Title":"TitleChinese";      
			$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
			
			include_once("libgroupcategory.php");
			$lgc = new libgroupcategory();
			$group_category_ary = $lgc->returnCats();
			$group_ary = array();
			foreach($group_category_ary as $k=>$d)
			{
				$pass_CurrentAcademicYearID = $d['GroupCategoryID']==0 ? "" : $CurrentAcademicYearID;
				$group_data_ary = $this->returnGroupsByCategory($d['GroupCategoryID'], $pass_CurrentAcademicYearID);
				foreach($group_data_ary as $k2 => $d2)
				{
					$group_ary[$d['CategoryName']][$d2['GroupID']] = $d2[$title_field];
				}
			}
			
			return getSelectByAssoArray($group_ary, $tag, $defaultValue);
			
		}
}
?>