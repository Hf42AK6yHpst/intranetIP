<?php
// using : 
/******************************************************************
 * Remarks:
 * 		If you need to generate csv with line break value,
 * 		please pass $includeLineBreak=1 to GET_EXPORT_TXT 
 * ****************************************************************/
 
###******************* Change Log *******************###
###		Date: 20141223 (Henry)
###				- modified EXPORT_FILE(), support GBK for $BaseCode parameter
###
###		Date: 20130607 (YatWoon)
###				- modified GET_EXPORT_TXT(), replace tab data in the header (option may include tab and will cause column shift)
###
###		Date: 20121113 (YatWoon)
###				- modified EXPORT_FILE(), add $BaseCode parameter
###
###		Date: 20110414 (Marcus)
###				- modified GET_EXPORT_TXT, cater new standard of export (2 rows of column title), behave like GET_EXPORT_TXT_WITH_REFERENCE 
###				- modified GET_EXPORT_TXT_WITH_REFERENCE, direct call GET_EXPORT_TXT in GET_EXPORT_TXT_WITH_REFERENCE, for better maintainance
###
###		Date: 20110106 (Marcus)
###				- modified GET_EXPORT_TXT, add param $includeLineBreak=0 to handle line break value in csv
###
###		Date: 20101130 (Kenneth Chung)
###				- add "while (@ob_end_clean());" before export file in EXPORT_FILE
###
###		Date: 20100813 (Ivan)
###				- added GET_EXPORT_HEADER_COLUMN() to generate the header column array
###
### 	Date: 20100708 (Henry Chow)
###				- added GET_EXPORT_TXT_WITH_REFERENCE(), apply new standard of export 
###		
### 	Date: 20100531 (Henry Yuen)
###				- function EXPORT_FILE add paramter ignoreLength,	
###						if this is set to true, then content-length is not set in header
###		
### 	Date: 20100331 (Henry Yuen)
###				- function EXPORT_FILE supports .XLS 
###		
###		Date: 20091215 (Kenneth Chung)
###				- add checking client browser and urlencode the file name on EXPORT_FILE 
###					function for utf8 chinese file name issue.
### 
###		Date: 20090408 (Ronald)
###				- disable the str_replace(array("\n", "\r", "\t"), array(" ", " ", "")) when generating the csv body
###					(if this affect other modules, please let me know, thanks.)
###
###**************************************************###


if (!defined("LIBEXPORTTEXT_DEFINED"))                     // Preprocessor directive
{
  define("LIBEXPORTTEXT_DEFINED", true);

  //global $import_coding, $g_encoding_unicode;
  //global $g_encoding_unicode;
  
  class libexporttext extends libdb
  {
		/*
		* Initialize
		*/
		function libexporttext()
		{
			global $intranet_root;
			$this->libdb();
		}
		
		/*
		* Prepare Content for Export
		* 	$data : 2D array of data to export
		* 	$ColumnDef: header, i.e. the first row
		*		$Delimiter: delimiter, e.g. comma, tab
		*		$LineBreak: linebreak, can be vary due to different system
		*		$ColumnDefDelimiter: set the delimiter of header different than the default
		*		$DataSize: need to specify if number of column header is different from the data OR no column header
		*		$Quoted: double-quoted the column and/or data item, "00"=both not quoted, "11"=both quoted
		*/
		function GET_EXPORT_TXT($data, $ColumnDef, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=0)
		{
			$Delimiter = ($Delimiter=="")?"\t":"$Delimiter";
			$ColumnDefDelimiter = ($ColumnDefDelimiter=="")?"\t":"$ColumnDefDelimiter";
			
//			for ($i=0; $i<sizeof($ColumnDef); $i++) {
//				if ($Quoted == "11" || $Quoted == "10")
//					$clabel[] = "\"".$ColumnDef[$i]."\"";
//				else
//					$clabel[] = $ColumnDef[$i];
//			}
			if(array_depth($ColumnDef)==1) $ColumnDef = array($ColumnDef);
			
			for ($i=0; $i<sizeof($ColumnDef); $i++) {
				if ($Quoted == "11" || $Quoted == "10") {
					for($j=0; $j<sizeof($ColumnDef[$i]); $j++)
						$clabel[$i][] = "\"". str_replace("\t"," ",$ColumnDef[$i][$j])."\"";
				} else {
					for($j=0; $j<sizeof($ColumnDef[$i]); $j++)
						$clabel[$i][] = str_replace("\t"," ",$ColumnDef[$i][$j]);
				}
			}
			
			// Header
			// $ColumnDef can be empty, i.e. no column header
			if ($ColumnDef != "")
			{
				for($i=0; $i<sizeof($clabel); $i++) {
					$x .= implode($ColumnDefDelimiter, $clabel[$i]).$LineBreak;
				}
			}
			// Provide $DataSize (number of column of data retrieved form DB) if
			// 1) number of column header is different from the data
			// 2) no column header
			if ($DataSize == 0)
				$DataSize = sizeof($clabel);
				
			// Body
			for ($i=0; $i<sizeof($data); $i++)
			{
//				debug_pr($data[$i]);
				unset($row);
				for ($j=0; $j<sizeof($data[$i]); $j++) {
					if($includeLineBreak==1){ // disable str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), quoted both column and/or data item
					//if($includeLineBreak==1 && (strpos($data[$i][$j], "\n")!==false || strpos($data[$i][$j], "\r")!==false)){ // disable str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), quoted both column and/or data item
						$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES))."\"";
					}
					else if ($Quoted == "11" || $Quoted == "10"){
						$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), ENT_QUOTES))."\"";
						//$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES))."\"";
					}
					else{
						$row[] = str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), ENT_QUOTES));
						//$row[] = str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES));
					}
				}
				// debug_r($row);
				if (count($row) > 0) {
					$x .= implode($Delimiter, $row);
				}
//				//debug_r($x);

				if($i!=sizeof($data)-1)
					$x .= $LineBreak;
			}
			return $x;
		}
		
		function GET_EXPORT_TXT2($data, $ColumnDef, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00")
		{
			$Delimiter = ($Delimiter=="")?"\t":"$Delimiter";
			$ColumnDefDelimiter = ($ColumnDefDelimiter=="")?"\t":"$ColumnDefDelimiter";
			
			for ($i=0; $i<sizeof($ColumnDef); $i++) {
				if ($Quoted == "11" || $Quoted == "10")
					$clabel[] = "\"".$ColumnDef[$i]."\"";
				else
					$clabel[] = $ColumnDef[$i];
			}
			
			// Header
			// $ColumnDef can be empty, i.e. no column header
			if ($ColumnDef != "")
				$x = implode($ColumnDefDelimiter, $clabel).$LineBreak;
			
			// Provide $DataSize (number of column of data retrieved form DB) if
			// 1) number of column header is different from the data
			// 2) no column header
			if ($DataSize == 0)
				$DataSize = sizeof($clabel);
	
			// Body
			for ($i=0; $i<sizeof($data); $i++)
			{
				unset($row);
				for ($j=0; $j<$DataSize; $j++) {
					if ($Quoted == "11" || $Quoted == "10"){
						//$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), ENT_QUOTES))."\"";
						$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES))."\"";
					}
					else{
						//$row[] = str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), ENT_QUOTES));
						$row[] = str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES));
					}
					
				}

				$x .= implode($Delimiter, $row).$LineBreak;
			}
			
			return $x;
		}
		
		function GET_EXPORT_TXT_WITH_REFERENCE($data, $ColumnDef, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=0)
		{
			return $this->GET_EXPORT_TXT($data, $ColumnDef, $Delimiter, $LineBreak, $ColumnDefDelimiter, $DataSize, $Quoted, $includeLineBreak);
			
			/*
			$Delimiter = ($Delimiter=="")?"\t":"$Delimiter";
			$ColumnDefDelimiter = ($ColumnDefDelimiter=="")?"\t":"$ColumnDefDelimiter";
			
			for ($i=0; $i<sizeof($ColumnDef); $i++) {
				if ($Quoted == "11" || $Quoted == "10") {
					for($j=0; $j<sizeof($ColumnDef[$i]); $j++)
						$clabel[$i][] = "\"".$ColumnDef[$i][$j]."\"";
				} else {
					for($j=0; $j<sizeof($ColumnDef[$i]); $j++)
						$clabel[$i][] = $ColumnDef[$i][$j];
				}
			}
			//debug_pr($clabel);
			// Header
			// $ColumnDef can be empty, i.e. no column header
			if ($ColumnDef != "") {
				for($i=0; $i<sizeof($clabel); $i++) {
					$x .= implode($ColumnDefDelimiter, $clabel[$i]).$LineBreak;
				}
			}
			
			// Provide $DataSize (number of column of data retrieved form DB) if
			// 1) number of column header is different from the data
			// 2) no column header
			if ($DataSize == 0)
				$DataSize = sizeof($clabel);

			// Body
			for ($i=0; $i<sizeof($data); $i++)
			{
				unset($row);
				for ($j=0; $j<sizeof($data[$i]); $j++) {
					if ($Quoted == "11" || $Quoted == "10"){
						$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), ENT_QUOTES))."\"";
					}
					else{
						$row[] = str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), $data[$i][$j]), ENT_QUOTES));
					}
				}

				$x .= implode($Delimiter, $row);
				
				if($i!=sizeof($data)-1)
					$x .= $LineBreak;

			}
			return $x;*/
		}
		
		
		
		/*
		* Export File

		*/
		# Henry Yuen (2010-05-31): add parameter ignoreLength 
		function EXPORT_FILE($filename, $data, $isXLS = false, $ignoreLength = false, $BaseCode="utf")
		{
			if($BaseCode=="GBK")
			{
// 				$data = mb_convert_encoding($data,'UTF-16LE','UTF-8'); 
				$data = mb_convert_encoding($data, "GBK", "UTF-8");
// 				$data = "\xFF\xFE".$data;
			}
			else if($BaseCode=="Big5")
			{
// 				$data = mb_convert_encoding($data,'UTF-16LE','UTF-8'); 
				$data = mb_convert_encoding($data, "Big5", "UTF-8");
// 				$data = "\xFF\xFE".$data;
			}
			else 
			{
				//debug_r(mb_list_encodings());
				$data = mb_convert_encoding($data,'UTF-16LE','UTF-8');
				// Add BOM (Byte Order Mark) to identify the file is in UTF-16LE encoding
				$data = "\xFF\xFE".$data;
			}	
			
			// detect client's browser, if browser is IE, urlencode the file name to utf8 file name show
			if (stristr($_SERVER['HTTP_USER_AGENT'],'MSIE') !== FALSE || stristr($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0') !== FALSE) 
				$filename = urlencode($filename);
			
			// flush all output buffer before export
			while (@ob_end_clean());
			
			// BOM header for UTF-8
			//$data = "\xEF\xBB\xBF".$data;	
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			
			if($isXLS){								
				header("Content-type: application/vnd.ms-excel");			
			}
			else{
				header('Content-type: application/octet-stream');
			}
			
			# Henry Yuen (2010-05-31): do not set content-length if ignroeLength is set to true
			if(!$ignoreLength){ 
				header('Content-Length: '.strlen($data));
			}
			
			header('Content-Disposition: attachment; filename="'.$filename.'";');
			print $data;
		}
		
		/*
		 * $ColumnTitleArr['En'] 	Array of English Title
		 * $ColumnTitleArr['Ch'] 	Array of Chinese Title
		 * $ColumnPropertyArr 		Stores the property of each title of the export column
		 * 							1: Required, 2: Reference, 3: Optional
		 */
		function GET_EXPORT_HEADER_COLUMN($ColumnTitleArr, $ColumnPropertyArr, $AddBracket=1)
		{
			global $Lang;
			
			$ColumnArr = array();
			$RowCounter = 0;
			foreach ($ColumnTitleArr as $thisLang => $thisLangTitleArr)
			{
				$numOfTitle = count($thisLangTitleArr);
				for ($i=0; $i<$numOfTitle; $i++)
				{
					$thisTitle = $thisLangTitleArr[$i];
					
					if ($thisLang == 'Ch' && $AddBracket==1)
						$thisTitle = '('.$thisTitle.')';
					
					if ($ColumnPropertyArr[$i] == 2)
						$thisTitle .= ' '.$Lang['General']['ImportArr']['Reference'][$thisLang];
					else if ($ColumnPropertyArr[$i] == 3)
						$thisTitle .= ' '.$Lang['General']['ImportArr']['Optional'][$thisLang];
						
					$ColumnArr[$RowCounter][$i] = $thisTitle;
				}
				
				$RowCounter++;
			}
			
			return $ColumnArr;
		}
	}
}        // End of directive
?>