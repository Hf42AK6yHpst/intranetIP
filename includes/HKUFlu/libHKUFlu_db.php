<?php

/*******************************************
 * 2016-08-09 Henry HM
 * - Create File
 * 
 *******************************************/

include_once $intranet_root . '/includes/HKUFlu/HKUFlu.php';

class libHKUFlu_db extends libdb{

	private $hkuFlu;

	public function __construct(){
		parent::__construct();
		$this->hkuFlu = new HKUFlu();
	}
	
	//-----------------------------------------------------------------------
	
	/* Shortcut Functions */
	
	// Get_Safe_Sql_Query()
	public function s($value){
		return $this->Get_Safe_Sql_Query($value);
	}
	
	/* END - Shortcut Functions */
	
	//-----------------------------------------------------------------------
	
	/* Workflow Functions */
	
	public function signAgreement($parent_id, $student_id, $is_agreed, $parent_name, $sign_date){
		$rows_agreement = $this->getAgreementByStudentID($student_id);
		foreach((array)$rows_agreement as $row_agreement){
			$this->releaseDisagreedAgreement($row_agreement['AgreementID']);
		}
		$this->agreement_insert($parent_id, $student_id, $is_agreed, $parent_name, $sign_date);
	}
	
	public function getAgreementByStudentID($student_id){
		return $rows_agreement = $this->agreement_getByStudentID($student_id);
	}
	
	public function addSickLeave($eClass_leave_id, $eClass_preset_leave_id, $parent_id, $student_id, $sick_date, $leave_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $from = 'A'){
		$sick_leave_id = $this->sickLeave_insert($eClass_leave_id, $parent_id, $student_id, $sick_date, $leave_date, $is_sick_leave = true, $from);
		$this->sickLeaveSymptom_insert($sick_leave_id, $array_symptom_code, $other_symptom);
		$this->sickLeaveSick_insert($sick_leave_id, $array_sick_code, $other_sick);
		$this->sickLeavePresetLeave_insert($sick_leave_id, $eClass_preset_leave_id);
		
		return $sick_leave_id;
	}
	
	public function addNonSickLeave($eClass_leave_id, $eClass_preset_leave_id, $parent_id, $student_id, $leave_date, $from = 'A'){
		$sick_leave_id = $this->sickLeave_insert($eClass_leave_id, $parent_id, $student_id, $sick_date = '', $leave_date, $is_sick_leave = false, $from);
		$this->sickLeavePresetLeave_insert($sick_leave_id, $eClass_preset_leave_id);
		
		return $sick_leave_id;
	}
	
	public function addSickLeaveByPresetLeave($eClass_preset_leave_id, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave = true, $form = 'T'){
		$rows_preset_leave = $this->presetLeave_getByID($eClass_preset_leave_id);
		if(count($rows_preset_leave) > 0){
			$row_preset_leave = $rows_preset_leave[0];
			$student_id = $row_preset_leave['StudentID'];
			$leave_date = $row_preset_leave['RecordDate'];
			
			$sick_leave_id = $this->addSickLeave('', $eClass_preset_leave_id, '', $student_id, $sick_date, $leave_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $form);
		}
		
		return $sick_leave_id;
	}
	
	public function editSickLeaveByPresetLeave($eClass_preset_leave_id, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave = true, $from = 'A'){
		$rows_sick_leave = $this->sickLeave_getByPresetLeaveID($eClass_preset_leave_id, $include_not_sick_leave = true);
		foreach($rows_sick_leave as $row_sick_leave){
			$sick_leave_id = $row_sick_leave['SickLeaveID'];
			$this->editSickLeaveBySickLeaveID($sick_leave_id, $eClass_leave_id=null, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave, $from);
		}
		
		return $sick_leave_id;
	}
	
	public function editSickLeaveBySickLeaveID($sick_leave_id, $eClass_leave_id, $sick_date, $array_symptom_code, $other_symptom, $array_sick_code, $other_sick, $is_sick_leave = true, $from = 'A'){
		$rows_sick_leave = $this->sickLeave_getByID($sick_leave_id, $include_not_sick_leave = true);
		$row_sick_leave = $rows_sick_leave[0];
		if($row_sick_leave['SubmittedFrom'] == 'A' && $from == 'T'){
			$from = 'C';
		}else{
			$from = null;
		}
		$this->sickLeave_updateByID($sick_leave_id, $eClass_leave_id, $parent_id = null, $student_id = null, $is_sick_leave, $sick_date, $leave_date = null, $from);
		$this->sickLeaveSymptom_deleteBySickLeaveID($sick_leave_id);
		$this->sickLeaveSick_deleteBySickLeaveID($sick_leave_id);
		$this->sickLeaveSymptom_insert($sick_leave_id, $array_symptom_code, $other_symptom);
		$this->sickLeaveSick_insert($sick_leave_id, $array_sick_code, $other_sick);
		
		return $sick_leave_id;
	}
	
	public function releaseDisagreedAgreement($agreement_id){
		$this->agreement_updateStatus($agreement_id, 'x');
	}
	
	public function getFullSickLeaveBySickLeave($rows_sick_leave){
		$array_return = array();
		
		foreach((array)$rows_sick_leave as $row_sick_leave){
			$sick_leave_id = $row_sick_leave['SickLeaveID'];
			$array_return[$sick_leave_id]['sick_leave'] = $row_sick_leave;
			$array_return[$sick_leave_id]['sick_leave_symptom'] = $this->sickLeaveSymptom_getBySickLeaveID($sick_leave_id);
			$array_return[$sick_leave_id]['sick_leave_sick'] = $this->sickLeaveSick_getBySickLeaveID($sick_leave_id);
			$array_return[$sick_leave_id]['sick_leave_preset_leave'] = $this->sickLeavePresetLeave_getBySickLeaveID($sick_leave_id);
		}
		
		return $array_return;
	}
	
	public function getFullSickLeaveBySickLeaveID($sick_leave_id){
		$rows_sick_leave = $this->sickLeave_getByID($sick_leave_id);
		return $this->getFullSickLeaveBySickLeave($rows_sick_leave);
	}
	
	public function getSickLeaveByStudentID($student_id){
		$rows_sick_leave = $this->sickLeave_getByStudentID($student_id);
		$array_return = $this->getFullSickLeaveBySickLeave($rows_sick_leave);
		
		return $array_return;
	}
	
	public function getSickLeaveByStudentIDAfterSignDate($student_id){
		$array_return = array();
		$sign_date = '';
		
		$rows_agreement = $this->getAgreementByStudentID($student_id);
		foreach((array)$rows_agreement as $row_agreement){
			$sign_date = $row_agreement['SignDate'];
		}
		
		if($sign_date!=''){
			$rows_sick_leave = $this->sickLeave_getByStudentIDAfterDate($student_id, $sign_date);
			$array_return = $this->getFullSickLeaveBySickLeave($rows_sick_leave);
		}
		
		return $array_return;
	}
	
	public function getSickLeaveByPresentLeaveID($eClass_preset_leave_id, $include_not_sick_leave = false){
		$rows_sick_leave = $this->sickLeave_getByPresetLeaveID($eClass_preset_leave_id, $include_not_sick_leave);
		$array_return = $this->getFullSickLeaveBySickLeave($rows_sick_leave);
		
		return $array_return;
	}
	
	public function updatePresetLeaveIDOfSickLeaveByLeaveID($eClass_leave_id, $eClass_preset_leave_id){
		$rows_sick_leave = $this->sickLeave_getByLeaveID($eClass_leave_id, true);
		foreach((array)$rows_sick_leave as $row_sick_leave){
			$sick_leave_id = $row_sick_leave['SickLeaveID'];
			$this->sickLeavePresetLeave_insert($sick_leave_id, $eClass_preset_leave_id);
		}
	}
	
	public function getPresetLeaveByStudentDatePeriod($student_id,$record_date,$day_period){
		return $this->presetLeave_getByStudentDatePeriod($student_id,$record_date,$day_period);
	}
	
	public function getSickLeaveReportByTypeID($type, $array_type_id, $start_date, $end_date){
		$rows_type = $this->studentID_getByTypes($type, $array_type_id);
		$array_student_id = $rows_type['array_student_id'];
		return $this->sickLeave_getByStudentIDAndDateRange($array_student_id, $start_date, $end_date);
	}
	
	public function getStandAloneSickLeaveByStudentIDAndDate($array_student_id, $date){
		$rows_sick_leave = $this->sickLeave_getByStudentIDAndDateRange($array_student_id, $date, $date, false, $include_not_sick_leave = true);
		return $this->getFullSickLeaveBySickLeave($rows_sick_leave);
	}
	
	public function getSickLeaveByStudentIDAndDate($array_student_id, $date){
		$rows_sick_leave = $this->sickLeave_getByStudentIDAndDateRange($array_student_id, $date, $date);
		return $this->getFullSickLeaveBySickLeave($rows_sick_leave);
	}
	
	public function getAgreedStudentIDAfterSignDate($now = ''){
		if($now == ''){
			$now = date('Y-m-d');
		}
		$now = substr($now, 0, 10) . ' 23:59:59';
		
		$rows_agreement = $this->agreement_getByIsAgreedAfterSignDate(true, $now);
		
		$array_return = array();
		foreach((array)$rows_agreement as $row_agreement){
			array_push($array_return, $row_agreement['StudentID']);
		}
		
		return $array_return;
	}

	/* END - Workflow Functions */
	
	//-----------------------------------------------------------------------
	
	/* DB Functions */
	
	private function agreement_insert($parent_id, $student_id, $is_agreed, $parent_name, $sign_date){
		$sql = 'INSERT INTO `HKUSPH_AGREEMENT`(`StudentID`,`ParentID`,`IsAgreed`,`ParentName`,`SignDate`,`Status`)' .
				' VALUES(\''.$this->s($student_id).'\',\''.$this->s($parent_id).'\',\''.$this->s($is_agreed).'\',\''.$this->s($parent_name).'\',\''.$this->s($sign_date).'\',\''.$this->s('a').'\');';
		$this->db_db_query($sql);
		return $this->db_insert_id();
	}
	
	private function agreement_updateStatus($agreement_id, $status){
		$sql = 'UPDATE `HKUSPH_AGREEMENT` SET `Status` = \''.$this->s($status).'\' WHERE `AgreementID` = \''.$this->s($agreement_id).'\';';
		$this->db_db_query($sql);
	}
	
	private function agreement_getByID($agreement_id){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_AGREEMENT').' FROM `HKUSPH_AGREEMENT` WHERE `AgreementID` = \''.$this->s($agreement_id).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function agreement_getByStudentID($student_id){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_AGREEMENT').' FROM `HKUSPH_AGREEMENT` WHERE `StudentID` = \''.$this->s($student_id).'\' AND `Status` = \'a\' ORDER BY SignDate DESC LIMIT 1;';
		return $this->returnResultSet($sql);
	}
	
	private function agreement_getByIsAgreedAfterSignDate($is_agreed = true, $now = ''){
		if($now == ''){
			$now = date('Y-m-d') . ' 00:00:00';
		}
		
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_AGREEMENT').' FROM `HKUSPH_AGREEMENT` WHERE `IsAgreed` = \''.$this->s($is_agreed?'1':'0').'\' AND `SignDate` <= \''.$this->s($now).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeave_insert($eClass_leave_id, $parent_id, $student_id, $sick_date, $leave_date, $is_sick_leave = true, $submitted_from = 'A'){
		$userID = $_SESSION['UserID'];
		$sql = 'INSERT INTO `HKUSPH_SICK_LEAVE`(`eClassLeaveID`,`StudentID`,`ParentID`,`IsSickLeave`,`SickDate`,`LeaveDate`,`SubmittedFrom`,`CreatedBy`,`CraetedDate`)' .
				' VALUES(\''.$this->s($eClass_leave_id==''?'-1':$eClass_leave_id).'\',\''.$this->s($student_id).'\',\''.$this->s($parent_id==''?'-1':$parent_id).'\',\''.$this->s($is_sick_leave?'1':'0').'\',\''.$this->s($sick_date).'\',\''.$this->s($leave_date).'\',\''.$this->s($submitted_from).'\',\''.$this->s($submitted_from == 'A'?$parent_id:$userID).'\',now());';
		$this->db_db_query($sql);
		
		return $this->db_insert_id();
	}
	
	private function sickLeave_updateByID($sick_leave_id, $eClass_leave_id = null, $parent_id = null, $student_id = null, $is_sick_leave = null, $sick_date = true, $leave_date = null, $submitted_from = 'A'){
		$userID = $_SESSION['UserID'];
		$sql = 'UPDATE `HKUSPH_SICK_LEAVE` SET ';
		$sql_update = '';
		$sql_update .= $this->getUpdateField($sql_update,'eClassLeaveID',$eClass_leave_id);
		$sql_update .= $this->getUpdateField($sql_update,'ParentID',$parent_id);
		$sql_update .= $this->getUpdateField($sql_update,'StudentID',$student_id);
		$sql_update .= $this->getUpdateField($sql_update,'IsSickLeave',$is_sick_leave?'1':'0');
		$sql_update .= $this->getUpdateField($sql_update,'SickDate',$sick_date);
		$sql_update .= $this->getUpdateField($sql_update,'LeaveDate',$leave_date);
		$sql_update .= $this->getUpdateField($sql_update,'SubmittedFrom',$submitted_from);
		$sql_update .= $this->getUpdateField($sql_update,'ModifiedBy',$userID);
		$sql_update .= $this->getUpdateField($sql_update,'ModifiedDate','now()',$sql_function=true);
		$sql .= $sql_update;
		$sql .= ' WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';

		$this->db_db_query($sql);
	}
	
	private function sickLeave_getByID($sick_leave_id, $include_not_sick_leave = false){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE').' FROM `HKUSPH_SICK_LEAVE` WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\''.($include_not_sick_leave?'':' AND `IsSickLeave` = \'1\'').';';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeave_getByStudentID($student_id, $include_not_sick_leave = false){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE').' FROM `HKUSPH_SICK_LEAVE` WHERE `StudentID` = \''.$this->s($student_id).'\' '.($include_not_sick_leave?'':' AND `IsSickLeave` = \'1\'').' ORDER BY `LeaveDate` DESC, `SickDate` DESC, `SickLeaveID` ASC;';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeave_getByStudentIDAfterDate($student_id, $date, $include_not_sick_leave = false){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE').' FROM `HKUSPH_SICK_LEAVE` WHERE `CraetedDate`>=\''.$this->s($date).'\' AND `StudentID` = \''.$this->s($student_id).'\' '.($include_not_sick_leave?'':' AND `IsSickLeave` = \'1\'').' ORDER BY `LeaveDate` DESC, `SickDate` DESC, `SickLeaveID` ASC;';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeave_getByPresetLeaveID($eClass_preset_leave_id, $include_not_sick_leave = false){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE').' FROM `HKUSPH_SICK_LEAVE` WHERE `SickLeaveID` IN (SELECT `SickLeaveID` FROM `HKUSPH_SICK_LEAVE_PRESET_LEAVE` WHERE `eClassPresetLeaveID` = \''.$this->s($eClass_preset_leave_id).'\')'.($include_not_sick_leave?'':' AND `IsSickLeave` = \'1\'').';';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeave_getByLeaveID($eClass_leave_id, $include_not_sick_leave = false){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE').' FROM `HKUSPH_SICK_LEAVE` WHERE `eClassLeaveID` = \''.$this->s($eClass_leave_id).'\''.($include_not_sick_leave?'':' AND `IsSickLeave` = \'1\'').';';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeave_getByStudentIDAndDateRange($array_student_id, $start_date, $end_date, $record_alone_only = false, $include_not_sick_leave = false){
		$sql_student_ids = implode('\',\'', $array_student_id);
		
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE').',`iu`.`ClassName`,`iu`.`ClassNumber`,`iu`.`EnglishName`,`iu`.`ChineseName` FROM `HKUSPH_SICK_LEAVE` AS sl INNER JOIN `INTRANET_USER` AS `iu` ON `iu`.`UserID`=`sl`.`StudentID` ' .
				' WHERE `StudentID` IN (\''.$sql_student_ids.'\') AND `LeaveDate` BETWEEN \''.$this->s($start_date).' 00:00:00\' AND \''.$this->s($end_date).' 23:59:59\'' .
						' AND (`eClassLeaveID` <= 0'.($record_alone_only?'':' OR (`eClassLeaveID` > 0 AND EXISTS(SELECT * FROM `HKUSPH_SICK_LEAVE_PRESET_LEAVE` `slpl` WHERE `slpl`.`SickLeaveID` = `sl`.`SickLeaveID`))').')' .
						($include_not_sick_leave?'':' AND `sl`.`IsSickLeave` = \'1\'') .
						' ORDER BY `LeaveDate` DESC;';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeaveSymptom_insert($sick_leave_id, $array_symptom_code, $other){
		foreach((array)$array_symptom_code as $symptom_code){
			$value_other = '';
			if($symptom_code == $this->hkuFlu->getSymptomCode(HKUFlu::SYMPTOM_OTHER)){
				$value_other = $other;
			}
			
			$sql = 'INSERT INTO `HKUSPH_SICK_LEAVE_SYMPTOM`(`SickLeaveID`,`SymptomCode`,`Other`)' .
					' VALUES(\''.$this->s($sick_leave_id).'\',\''.$this->s($symptom_code).'\',\''.$this->s($value_other).'\');';
			$this->db_db_query($sql);
		}
	}
	
	private function sickLeaveSymptom_deleteBySickLeaveID($sick_leave_id){
		$sql = 'DELETE FROM `HKUSPH_SICK_LEAVE_SYMPTOM` WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';
		$this->db_db_query($sql);
	}
	
	private function sickLeaveSymptom_getBySickLeaveID($sick_leave_id){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE_SYMPTOM').' FROM `HKUSPH_SICK_LEAVE_SYMPTOM` WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeaveSick_insert($sick_leave_id, $array_sick_code, $other){
		foreach((array)$array_sick_code as $sick_code){
			$value_other = '';
			if($sick_code == $this->hkuFlu->getSickCode(HKUFlu::SICK_OTHER)){
				$value_other = $other;
			}
			
			$sql = 'INSERT INTO `HKUSPH_SICK_LEAVE_SICK`(`SickLeaveID`,`SickCode`,`Other`)' .
					' VALUES(\''.$this->s($sick_leave_id).'\',\''.$this->s($sick_code).'\',\''.$this->s($value_other).'\');';
			$this->db_db_query($sql);
		}
	}
	
	private function sickLeaveSick_deleteBySickLeaveID($sick_leave_id){
		$sql = 'DELETE FROM `HKUSPH_SICK_LEAVE_SICK` WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';
		$this->db_db_query($sql);
	}
	
	private function sickLeaveSick_getBySickLeaveID($sick_leave_id){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE_SICK').' FROM `HKUSPH_SICK_LEAVE_SICK` WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function sickLeavePresetLeave_insert($sick_leave_id, $eClass_preset_leave_id){
		if($eClass_preset_leave_id!=''){
			$sql = 'INSERT INTO `HKUSPH_SICK_LEAVE_PRESET_LEAVE`(' . $this->getSelectField('HKUSPH_SICK_LEAVE_PRESET_LEAVE') . ')' .
					' VALUES(\''.$this->s($sick_leave_id).'\',\''.$this->s($eClass_preset_leave_id).'\');';
			$this->db_db_query($sql);
		}
	}
	
	private function sickLeavePresetLeave_updateBySickLeaveID($sick_leave_id, $eClass_preset_leave_id = null){
		if($eClass_preset_leave_id != null){
			$userID = $_SESSION['UserID'];
			$sql = 'UPDATE `HKUSPH_SICK_LEAVE_PRESET_LEAVE` SET ';
			$sql_update = '';
			$sql_update .= $this->getUpdateField($sql_update,'eClassPresetLeaveID',$eClass_preset_leave_id);
			$sql .= $sql_update;
			$sql .= 'WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';
		
			$this->db_db_query($sql);
		}
	}
	
	private function sickLeavePresetLeave_getBySickLeaveID($sick_leave_id){
		$sql = 'SELECT '.$this->getSelectField('HKUSPH_SICK_LEAVE_PRESET_LEAVE').' FROM `HKUSPH_SICK_LEAVE_PRESET_LEAVE` WHERE `SickLeaveID` = \''.$this->s($sick_leave_id).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function presetLeave_getByID($eClass_preset_leave_id){
		$sql = 'SELECT '.$this->getSelectField('CARD_STUDENT_PRESET_LEAVE').' FROM `CARD_STUDENT_PRESET_LEAVE` WHERE `RecordID` = \''.$this->s($eClass_preset_leave_id).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function presetLeave_getByStudentDatePeriod($student_id,$record_date,$day_period){
		$sql = 'SELECT '.$this->getSelectField('CARD_STUDENT_PRESET_LEAVE').' FROM `CARD_STUDENT_PRESET_LEAVE`' .
				' WHERE `StudentID` = \''.$this->s($student_id).'\'' .
						' AND `RecordDate` = \''.$this->s($record_date).'\'' .
								' AND `DayPeriod` = \''.$this->s($day_period).'\';';
		return $this->returnResultSet($sql);
	}
	
	private function studentID_getByTypes($TargetType, $TargetID){
		global $intranet_root, $PATH_WRT_ROOT;
		
		include_once($intranet_root."/includes/libclass.php");
		
		$lclass = new libclass();
		
		$StudentIdAry = array();
		if($TargetType=='student'){
			$StudentIdAry = $TargetID;
			$ClassIdAry = array();
			$FormIdAry = array();
		}else if($TargetType == 'class'){
			$FormIdAry = array();
			$ClassIdAry = $TargetID;
			$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
			$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
		}else if($TargetType == 'form'){
			$FormIdAry = $TargetID;
			$ClassIdAry = array();
			
			for($i=0;$i<sizeof($FormIdAry);$i++)
			{
				$ClassList = $lclass->returnClassListByLevel($FormIdAry[$i]);
				for($j=0;$j<sizeof($ClassList);$j++) {
					$ClassIdAry[] = $ClassList[$j][0];
				}
			}
			$StudentAry = $lclass->getStudentByClassId($ClassIdAry);
			$StudentIdAry = Get_Array_By_Key($StudentAry,'UserID');
		}
		
		return array(
			'array_student_id' => $StudentIdAry,
			'array_class_id' => $ClassIdAry,
			'array_form_id' => $FormIdAry
		);
}
	
	/* END - DB Functions */
	
	//-----------------------------------------------------------------------
	
	/* SQL Functions */
	
	private function getSelectField($table){
		switch($table){
			case 'HKUSPH_AGREEMENT':
				return '`AgreementID`,`StudentID`,`ParentID`,`IsAgreed`,`ParentName`,`SignDate`,`Status`';
				break;
				
			case 'HKUSPH_SICK_LEAVE':
				return '`SickLeaveID`,`eClassLeaveID`,`StudentID`,`ParentID`,`IsSickLeave`,`SickDate`,`LeaveDate`,`SubmittedFrom`,`CreatedBy`,`CraetedDate`,`ModifiedBy`,`ModifiedDate`';
				break;
				
			case 'HKUSPH_SICK_LEAVE_SYMPTOM':
				return '`SickLeaveID`,`SymptomCode`,`Other`';
				break;
				
			case 'HKUSPH_SICK_LEAVE_SICK':
				return '`SickLeaveID`,`SickCode`,`Other`';
				break;
				
			case 'HKUSPH_SICK_LEAVE_PRESET_LEAVE':
				return '`SickLeaveID`,`eClassPresetLeaveID`';
				break;
				
			case 'CARD_STUDENT_PRESET_LEAVE':
				return '`RecordID`,`StudentID`,`RecordDate`,`DayPeriod`,`TargetStatus`,`Reason`,`Waive`,`DocumentStatus`,`Remark`,`RecordType`,`RecordStatus`,`DateInput`,`DateModified`,`RecordType`';
				break;
		}
	} 
	
	private function getUpdateField($sql_update, $name, $value = null, $sql_function = false){
		if($value !== null){
			if($sql_function){
				return ($sql_update==''?' ':',').'`'.$name.'`='.$this->s($value).'';
			}else{
				return ($sql_update==''?' ':',').'`'.$name.'`=\''.$this->s($value).'\'';
			}
		}else{
			return '';
		}
	}
	
	/* END - SQL Functions */
}

?>