<?php

class HKUFlu{
	const SICK_URTI = 0;
	const SICK_LRTI = 1;
	const SICK_GASTROENTERITIS = 2;
	const SICK_FOOT_AND_MOUTH_DISEASE = 3;
	const SICK_CHICKEN_POX = 4;
	const SICK_ACUTE_CONJUNCTIVITIS = 5;
	const SICK_SCARLET_FEVER = 6;
	const SICK_MUMPS = 7;
	const SICK_MEASLES = 8;
	const SICK_RUBELLA = 9;
	const SICK_NO_CONSULTATION = 10;
	const SICK_OTHER = 11;
	
	const SYMPTOM_FEVER = 0;
	const SYMPTOM_TIRED = 1;
	const SYMPTOM_HEADACHE = 2;
	const SYMPTOM_EYEDISCHARGE = 3;
	const SYMPTOM_RUNNYNOSE = 4;
	const SYMPTOM_MOUTHBLISTERS = 5;
	const SYMPTOM_COUGH = 6;
	const SYMPTOM_VOMITING = 7;
	const SYMPTOM_COLD = 8;
	const SYMPTOM_SORETHROAT = 9;
	const SYMPTOM_RASH = 10;
	const SYMPTOM_DIARRHEA = 11;
	const SYMPTOM_LIMBSBLISTERS = 12;
	const SYMPTOM_OTHER = 13;
	
	function getSickCode($enum = null){
		$sick_code = array();
		$sick_code[HKUFlu::SICK_URTI] = 'urti';
		$sick_code[HKUFlu::SICK_LRTI] = 'lrti';
		$sick_code[HKUFlu::SICK_GASTROENTERITIS] = 'gastroenteritis';
		$sick_code[HKUFlu::SICK_FOOT_AND_MOUTH_DISEASE] = 'foot and mouth disease';
		$sick_code[HKUFlu::SICK_CHICKEN_POX] = 'chicken pox';
		$sick_code[HKUFlu::SICK_ACUTE_CONJUNCTIVITIS] = 'acute conjunctivitis';
		$sick_code[HKUFlu::SICK_SCARLET_FEVER] = 'scarlet fever';
		$sick_code[HKUFlu::SICK_MUMPS] = 'mumps';
		$sick_code[HKUFlu::SICK_MEASLES] = 'measles';
		$sick_code[HKUFlu::SICK_RUBELLA] = 'rubella';
		$sick_code[HKUFlu::SICK_NO_CONSULTATION] = 'no consultation';
		$sick_code[HKUFlu::SICK_OTHER] = 'other';

		if($enum === null){
			return $sick_code;
		}else{
			return $sick_code[$enum];
		}
	}
	
	function getSymptomCode($enum = null){
		$symptom_code = array();
		$symptom_code[HKUFlu::SYMPTOM_FEVER] = 'fever';
		$symptom_code[HKUFlu::SYMPTOM_TIRED] = 'tired';
		$symptom_code[HKUFlu::SYMPTOM_HEADACHE] = 'headache';
		$symptom_code[HKUFlu::SYMPTOM_EYEDISCHARGE] = 'eyedischarge';
		$symptom_code[HKUFlu::SYMPTOM_RUNNYNOSE] = 'runnynose';
		$symptom_code[HKUFlu::SYMPTOM_MOUTHBLISTERS] = 'mouthblisters';
		$symptom_code[HKUFlu::SYMPTOM_COUGH] = 'cough';
		$symptom_code[HKUFlu::SYMPTOM_VOMITING] = 'vomiting';
		$symptom_code[HKUFlu::SYMPTOM_COLD] = 'cold';
		$symptom_code[HKUFlu::SYMPTOM_SORETHROAT] = 'sorethroat';
		$symptom_code[HKUFlu::SYMPTOM_RASH] = 'rash';
		$symptom_code[HKUFlu::SYMPTOM_DIARRHEA] = 'diarrhea';
		$symptom_code[HKUFlu::SYMPTOM_LIMBSBLISTERS] = 'limbsblisters';
		$symptom_code[HKUFlu::SYMPTOM_OTHER] = 'other';
		
		if($enum === null){
			return $symptom_code;
		}else{
			return $symptom_code[$enum];
		}
	}
	
	function getIndexFromSickCode($code){
		$codes = $this->getSickCode();
		foreach((array)$codes as $index => $sick_code){
			if($sick_code == $code){
				return $index;
			}
		}
		return -1;
	}
	
	function getIndexFromSymptomCode($code){
		$codes = $this->getSymptomCode();
		foreach((array)$codes as $index => $symptom_code){
			if($symptom_code == $code){
				return $index;
			}
		}
		return -1;
	}
}

?>