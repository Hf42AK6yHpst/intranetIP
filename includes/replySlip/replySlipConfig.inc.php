<?php
$replySlipConfig = array();

$replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['show'] = 1;
$replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['hide'] = 2;
$replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['yes'] = 1;
$replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['no'] = 2;
$replySlipConfig['INTRANET_REPLY_SLIP']['RecordType']['realForm'] = 1;
$replySlipConfig['INTRANET_REPLY_SLIP']['RecordType']['template'] = 2;
$replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['active'] = 1;
$replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['draft'] = 2;
$replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['deleted'] = 3;
$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'] = 1;
$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide'] = 2;

$replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['singleMC'] = 1;
$replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['multipleMC'] = 2;
$replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['shortQuestion'] = 3;
$replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['longQuestion'] = 4;
$replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na'] = 5;

$replySlipConfig['importReplySlip']['questionTypeCode']['MC'] = 'singleMC';
$replySlipConfig['importReplySlip']['questionTypeCode']['MMC'] = 'multipleMC';
$replySlipConfig['importReplySlip']['questionTypeCode']['SQ'] = 'shortQuestion';
$replySlipConfig['importReplySlip']['questionTypeCode']['LQ'] = 'longQuestion';
$replySlipConfig['importReplySlip']['questionTypeCode']['NA'] = 'na';

$replySlipConfig['ajaxDataSeparator'] = '||';
$replySlipConfig['importDataLineBreakReplacement'] = '<!--lineBreakHere-->';
$replySlipConfig['questionIdHiddenFieldSeparator'] = ',';
?>