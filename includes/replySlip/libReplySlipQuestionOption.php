<?php
// editing by ivan

if (!defined("LIBREPLYSLIPQUESTIONOPTION_DEFINED")) {
	define("LIBREPLYSLIPQUESTIONOPTION_DEFINED", true);
	
	class libReplySlipQuestionOption extends libdbobject {
		private $optionId;
		private $questionId;
		private $content;
		private $displayOrder;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			parent::__construct('INTRANET_REPLY_SLIP_QUESTION_OPTION', 'OptionID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setOptionId($val) {
			$this->optionId = $val;
		}
		public function getOptionId() {
			return $this->optionId;
		}
		
		public function setQuestionId($val) {
			$this->questionId = $val;
		}
		public function getQuestionId() {
			return $this->questionId;
		}
		
		public function setContent($val) {
			$this->content = $val;
		}
		public function getContent() {
			return $this->content;
		}
		
		public function setDisplayOrder($val) {
			$this->displayOrder = $val;
		}
		public function getDisplayOrder() {
			return $this->displayOrder;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('OptionID', 'int', 'setOptionId', 'getOptionId');
			$fieldMappingAry[] = array('QuestionID', 'int', 'setQuestionId', 'getQuestionId');
			$fieldMappingAry[] = array('Content', 'str', 'setContent', 'getContent');
			$fieldMappingAry[] = array('DisplayOrder', 'int', 'setDisplayOrder', 'getDisplayOrder');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($this->getInputBy());
			$this->setModifiedDate('now()');
			$this->setModifiedBy($this->getModifiedBy());
			
			return true;
		}	
	}
}
?>