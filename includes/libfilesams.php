<?php
class libfilesams extends libfilesystem {

	var $len;
	var $lang;
	var $domain_name;

	function libfilesams(){
		global $intranet_session_language, $HTTP_SERVER_VARS;
		$this->lang = $intranet_session_language;
		$this->domain_name = $HTTP_SERVER_VARS["SERVER_NAME"];
		$this->len = array(1,1,9,2,4,4,2,9,35,12,15,16,9,1,12,8,3,1,3,16,2,12,1,1,1,1,1,1,1,2,1,8,10,12,1,1,1,12,5,3,5,10,6,10,40,20,40,20,40,40,30,20,1,1,1,8);
	}

	# ------------------------------------------------------------------------------------

	function returnSplitSamsRecord($string){
		$len = $this->len;
		$i = 0;
		while (strlen($string) > 0) {
			$l = ($i == sizeof($len)) ? strlen($string) : $len[$i];
			$field[$i] = trim(substr($string, 0, $l));
			$string = substr($string, $l); 
			$i++;
		}
		return $field;
	}

	function returnSamsRecord($contents){
		$len = $this->len;
		$l = 0;
		for($i=0; $i<sizeof($len); $i++){
			$l += $len[$i];
		}
		$l++;
		$i = 0;
		while (strlen($contents) > 0) {
			if(strlen($contents) > (4*$l))
			$x[$i++] = $this->returnSplitSamsRecord(substr($contents, strlen($contents)-$l));
			$contents = substr($contents, 0, strlen($contents)-$l); 
		}
		return $x;
	}

	# ------------------------------------------------------------------------------------

	function file_read_sams($data){
		$records = $this->returnSamsRecord($data);
		for($i=0; $i<sizeof($records); $i++){
			$field = $records[$i];
			$UserLogin = $field[2];
			$UserEmail = $field[2]."@".$this->domain_name;
			$FirstName = ($this->lang == "en") ? substr($field[8], strpos($field[8]," ")) : substr($field[9], 2);
			$LastName = ($this->lang == "en") ? substr($field[8], 0, strpos($field[8]," ")) : substr($field[9], 0, 2);
			$ChineseName = ($this->lang == "en") ? $field[8] : $field[9];
			$Title = ($field[13]=="M") ? 0: 1;
			$Gender = ($field[13]=="M") ? "M" : "F" ;
			$DOB = substr($field[15], 0, 4)."-".substr($field[15], 4, 2)."-".substr($field[15], 6, 2);
			$Home = $field[37];
			$Office = "";
			$Mobile = "";
			$Fax = "";
			$ICQ = "";
			$Address = ($this->lang == "en") ? $field[38]." ".$field[39]." ".$field[40].", ".$field[44].", ".$field[46].", ".$field[48].", ".$field[50] : $field[41]." ".$field[42]." ".$field[43].", ".$field[45].", ".$field[47].", ".$field[49].", ".$field[51];
			$Country = $field[16];
			$ClassNumber = $field[6];
			$ClassName = $field[5];
			$ClassLevel = $field[3];
			$x[$i] = array($UserLogin, $UserEmail, $FirstName, $LastName, $ChineseName, $Title, $Gender, $DOB, $Home, $Office, $Mobile, $Fax, $ICQ, $Address, $Country, $ClassNumber, $ClassName, $ClassLevel);
		}
		return $x;
	}

	# ------------------------------------------------------------------------------------

	function get_file_array($format="", $filepath="", $filename=""){
		$ext = strtoupper($this->file_ext($filename));
		$x = 0;
		if($format == 0 && $ext == ".DBF") {
			$x = $this->file_read_sams($this->file_read($filepath));
		}
		if($format == 1 && $ext == ".CSV") {
			$x = $this->file_read_csv($filepath);
			array_shift($x);
		}
		return $x;
	}

	# ------------------------------------------------------------------------------------

}
?>