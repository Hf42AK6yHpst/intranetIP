<?php
if (!defined("LIBEHOMEWORK_DIARY_DEFINED")) {
	define("LIBEHOMEWORK_DIARY_DEFINED", true);
	
	class libhomework_diary extends libdb {
		var $ModuleTitle;
		var $SettingsObj;
		var $Type_WeeklyDiary = 1;
		var $Type_NewsPaperCutting = 2;
		
		
		function libhomework_diary() {
			$this->libdb();
			$this->ModuleTitle = 'eHomework_diray';
		}
		
		function getDiaryData($parHomeworkId) {
			$sql = "Select 
							HomeworkID, ClassGroupID, SubjectID, StartDate, DueDate, Title, Description, Type, ClassID 
					From
							INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING
					Where 
							HomeworkID = '".$parHomeworkId."'
					";
			return $this->returnResultSet($sql);
		}
		
		function getDiaryHandinData($parHomeworkId, $parArticleIdAry='') {
			
			if ($parArticleIdAry !== '') {
				$condsArticleId = " AND ArticleID IN ('".implode("','", (array)$parArticleIdAry)."') ";
			}
			
			$sql = "Select 
							ArticleID, UserID, HomeworkID, Title, Content, WordCount, HasAttachment, TeacherHasRead, TeacherComment, ExcellentArticle, SubmitTime, TeacherScore
					From
							INTRANET_APP_WEEKLYDIARY_ARTICLE
					Where 
							HomeworkID = '".$parHomeworkId."'
							$condsArticleId
					";
			return $this->returnResultSet($sql);
		}
		
		function getDiaryHandinAttachment($parArticleIdAry) {
			$sql = "Select 
							AttachmentID, ArticleID, Type, Url, Title, Description, Image, ShowInList
					From
							INTRANET_APP_WEEKLYDIARY_ARTICLE_ATTACHMENT
					Where 
							ArticleID = '".implode("','", (array)$parArticleIdAry)."'
					Order By
							ShowInList desc
					";
			return $this->returnResultSet($sql);
		}
		
		function saveTeacherFeedback($ArticleID, $teacherScore, $teacherComment) {
			$sql = "Update 
							INTRANET_APP_WEEKLYDIARY_ARTICLE
					Set
							TeacherHasRead = 1,
							TeacherScore = '".$this->Get_Safe_Sql_Query($teacherScore)."',
							TeacherComment = '".$this->Get_Safe_Sql_Query($teacherComment)."',
							TeacherDateModified = now(),
							TeacherLastModifiedBy = '".$_SESSION['UserID']."'
					Where
							ArticleID = '".$ArticleID."'
					";
			return $this->db_db_query($sql);
		}
		
		function getSubmissionStatusSelection($parId, $parName, $parValue, $parOnChange) {
			global $Lang;
			
			$selectAry = array();
			$selectAry[''] = $Lang['SysMgr']['Homework']['AllSubmissionStatus'];
			$selectAry[1] = $Lang['SysMgr']['Homework']['Submitted'];
			$selectAry[2] = $Lang['SysMgr']['Homework']['NotSubmitted'];
			
			$onchange = '';
			if ($parOnChange != "")
				$onchange = 'onchange="'.$parOnChange.'"';
				
			$selectionTags = ' id="'.$parId.'" name="'.$parName.'" '.$onchange;
			return getSelectByAssoArray($selectAry, $selectionTags, $parValue, $all=0, $noFirst=1);
		}
		
		function getGradingStatusSelection($parId, $parName, $parValue, $parOnChange) {
			global $Lang;
			
			$selectAry = array();
			$selectAry[''] = $Lang['SysMgr']['Homework']['AllGradingStatus'];
			$selectAry[1] = $Lang['SysMgr']['Homework']['HasGraded'];
			$selectAry[2] = $Lang['SysMgr']['Homework']['NotYetGraded'];
			
			$onchange = '';
			if ($parOnChange != "")
				$onchange = 'onchange="'.$parOnChange.'"';
				
			$selectionTags = ' id="'.$parId.'" name="'.$parName.'" '.$onchange;
			return getSelectByAssoArray($selectAry, $selectionTags, $parValue, $all=0, $noFirst=1);
		}
		
		function getDiaryAttachmentDisplay($parAttachmentAry) {
			global $intranet_root,$image_path,$LAYOUT_SKIN;
			
			$x = '';
			$attachmentId = $parAttachmentAry['AttachmentID'];
			$attachmentType = $parAttachmentAry['Type'];
			
			if ($attachmentType == 1) { 
				// image
				include_once($intranet_root.'/includes/libfilesystem.php');
				$lfs = new libfilesystem();
				
				$imagePath = $intranet_root.'/'.$parAttachmentAry['Image'];
				$x .= '<img alt="Embedded Image" style="max-width:400px; max-height:400px;" src="data:image/png;base64,'.base64_encode($lfs->file_read($imagePath)).'" />';
			}
			else if ($attachmentType == 2) {
				// url
				$url = $parAttachmentAry['Url'];
				$title = $parAttachmentAry['Title'];
				$description = $parAttachmentAry['Description'];
				$image = $parAttachmentAry['Image'];
				if($image=="")$image = $image_path."/".$LAYOUT_SKIN."/ic_insert_link.png";
				$x .= '<a id="attachmentLink_'.$attachmentId.'" class="attachmentLink" href="'.$url.'" target="_blank" class="tablelink">'.chopString($url, 50).'</a>'."\r\n";
				$x .= '<br>'."\r\n";
				$x .= '<table width="100%">'."\r\n";
				$x .= '<tr><td rowspan="2" width="70px" style="border:0;vertical-align: middle;"> <img alt="Embedded Image" style="width:70px; height:70px;" src="'.$image.'" /></td>'."\r\n";
				$x .= '<td height="40px" style="font-size:large;border:0;" nowrap="nowrap" >'.$title.'</td></tr>'."\r\n";					
				$x .= '<tr height="30px"><td nowrap="nowrap" style="border:0;">'.$description.'</td></tr>'."\r\n";	
				$x .= '</table>'."\r\n";
			}
			
			return $x;
		}
	}
}
?>