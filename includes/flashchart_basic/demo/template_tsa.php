<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
$title = new title( "能力/範疇 統計" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( '能力 / 範疇' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('數範疇','度量範疇','圖形與空間範疇','數據處理範疇') );

$y_legend = new y_legend( 'Mark' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 10, 1 );
$y->set_offset(true);

$bar0 = new bar_stack_group();
$bar0->set_values( array(2,0,0,0) );
$bar0->set_colour( '#72a9db' );
$bar0->set_tooltip( '五位數:#val#' );
$bar0->set_key( '五位數', '13' );
$bar0->set_id( 0 );
$bar0->set_visible( true );

$bar1 = new bar_stack_group();
$bar1->set_values( array(6,0,0,0) );
$bar1->set_colour( '#eb593b' );
$bar1->set_tooltip( '四則計算:#val#' );
$bar1->set_key( '四則計算', '13' );
$bar1->set_id( 1 );
$bar1->set_visible( true );

$bar2 = new bar_stack_group();
$bar2->set_values( array(1,0,0,0) );
$bar2->set_colour( '#aaaaaa' );
$bar2->set_tooltip( '分數:#val#' );
$bar2->set_key( '分數', '13' );
$bar2->set_id( 2 );
$bar2->set_visible( true );

$bar3 = new bar_stack_group();
$bar3->set_values( array(0,1,0,0) );
$bar3->set_colour( '#92d24f' );
$bar3->set_tooltip( '香港通用的貨幣:#val#' );
$bar3->set_key( '香港通用的貨幣', '13' );
$bar3->set_id( 3 );
$bar3->set_visible( true );

$bar4 = new bar_stack_group();
$bar4->set_values( array(0,2,0,0) );
$bar4->set_colour( '#eaa325' );
$bar4->set_tooltip( '長度和距離:#val#' );
$bar4->set_key( '長度和距離', '13' );
$bar4->set_id( 4 );
$bar4->set_visible( true );

$bar5 = new bar_stack_group();
$bar5->set_values( array(0,1,0,0) );
$bar5->set_colour( '#f0e414' );
$bar5->set_tooltip( '時間:#val#' );
$bar5->set_key( '時間', '13' );
$bar5->set_id( 5 );
$bar5->set_visible( true );

$bar6 = new bar_stack_group();
$bar6->set_values( array(0,1,0,0) );
$bar6->set_colour( '#6198ca' );
$bar6->set_tooltip( '重量:#val#' );
$bar6->set_key( '重量:', '13' );
$bar6->set_id( 6 );
$bar6->set_visible( true );

$bar7 = new bar_stack_group();
$bar7->set_values( array(0,0,1,0) );
$bar7->set_colour( '#ca482a' );
$bar7->set_tooltip( '立體圖形:#val#' );
$bar7->set_key( '立體圖形', '13' );
$bar7->set_id( 7 );
$bar7->set_visible( true );

$bar8 = new bar_stack_group();
$bar8->set_values( array(0,0,2,0) );
$bar8->set_colour( '#989898' );
$bar8->set_tooltip( '平面圖形:#val#' );
$bar8->set_key( '平面圖形', '13' );
$bar8->set_id( 8 );
$bar8->set_visible( true );

$bar9 = new bar_stack_group();
$bar9->set_values( array(0,0,1,0) );
$bar9->set_colour( '#81c13e' );
$bar9->set_tooltip( '角:#val#' );
$bar9->set_key( '角', '13' );
$bar9->set_id( 9 );
$bar9->set_visible( true );

$bar10 = new bar_stack_group();
$bar10->set_values( array(0,0,1,0) );
$bar10->set_colour( '#d99214' );
$bar10->set_tooltip( '四個主要方向:#val#' );
$bar10->set_key( '四個主要方向', '13' );
$bar10->set_id( 10 );
$bar10->set_visible( true );

$bar11 = new bar_stack_group();
$bar11->set_values( array(0,0,0,2) );
$bar11->set_colour( '#efd303' );
$bar11->set_tooltip( '象形圖:#val#' );
$bar11->set_key( '象形圖', '13' );
$bar11->set_id( 11 );
$bar11->set_visible( true );

$tooltip = new tooltip();
$tooltip->set_hover();
//$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#000000" );
//$tooltip->set_background_colour( "#ffffff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $bar0 );
$chart->add_element( $bar1 );
$chart->add_element( $bar2 );
$chart->add_element( $bar3 );
$chart->add_element( $bar4 );
$chart->add_element( $bar5 );
$chart->add_element( $bar6 );
$chart->add_element( $bar7 );
$chart->add_element( $bar8 );
$chart->add_element( $bar9 );
$chart->add_element( $bar10 );
$chart->add_element( $bar11 );
$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			content.appendChild(c);
		}
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart-develop.swf", "my_chart", "650", "350", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span id="testHead"></span> : <span id="testContent"></span>
		<td>
	</tr>
</table>
</body>
</html>

