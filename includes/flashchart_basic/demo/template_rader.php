<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
$title = new title( "學生學習經歷種類" );
$title->set_style( "{font-size: 12px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$r = new radar_axis( 30 );
$r->set_colour( '#C1CBFF' );
$r->set_grid_colour( '#C1CBFF' );
//$labels = new radar_axis_labels( array('Zero','','','Middle','','High') );
//$labels->set_colour( '#9F819F' );
//$r->set_labels( $labels );
$spoke_labels = new radar_spoke_labels('Zero','First','Second','Middle','Quite High','High');        
$spoke_labels->set_colour( '#9F819F' );
$r->set_spoke_labels( $spoke_labels );	

$tooltip = new tooltip();
$tooltip->set_proximity();

$line = new line_dot();
$line->set_values(array(7,12,8,5,18,22));
//$line->set_default_dot_style( new s_hollow_dot('#FBB829', 4) );
$line->set_width( 2 );
$line->set_colour( '#00FF21' );
//$line->set_tooltip( "#val#" );
$line->set_key( "Test", 10 );
$line->loop();
$line->set_id( 'component' );
//$line->set_expandable( false ); //can click to call sub-level chart
//$line->set_visible( true ); //visible? in the beginning	

$chart = new open_flash_chart();
$chart->set_bg_colour( '#ffffff' );
$chart->set_title( $title );
$chart->set_radar_axis( $r );
$chart->add_element( $line );
$chart->set_tooltip( $tooltip );
?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			content.appendChild(c);
		}
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "850", "350", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span id="testHead"></span> : <span id="testContent"></span>
		<td>
	</tr>
</table>
</body>
</html>

