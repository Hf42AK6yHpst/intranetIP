<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

############################################################################
# UPLOAD
############################################################################
/*
if (!is_null($_FILES['Filedata'])){

	$imgdir = $PATH_WRT_ROOT."home/admin/disciplinev12/flashdemo/upload/";

	echo $imgdir;

	if (!is_dir($imgdir)){ 
		mkdir($imgdir, 0777);
	}

	move_uploaded_file($_FILES['Filedata']['tmp_name'], $imgdir.$_FILES['Filedata']['name']);  
	chmod($imgdir.$_FILES['Filedata']['name'], 0777); 

	if($imgdir.$_FILES['Filedata']['name']){
		echo "success"; //let actionscript knows
	}

}else{
	//error
	echo "error";
}
*/

// default path for the image to be stored //
//$default_path = $PATH_WRT_ROOT."home/web/eclass40/intranetIP25/includes/flashchart_basic/demo/upload/";
$default_path = $intranet_root."/file/temp/";

if (!file_exists($default_path)) mkdir($default_path, 0777, true);

// full path to the saved image including filename //
$destination = $default_path . basename( $_FILES[ 'Filedata' ][ 'name' ] ); 

// move the image into the specified directory //
if (move_uploaded_file($_FILES[ 'Filedata' ][ 'tmp_name' ], $destination)) {
    echo "The file " . basename( $_FILES[ 'Filedata' ][ 'name' ] ) . " has been uploaded;";
} else {
    echo "FILE UPLOAD FAILED";
}

?>