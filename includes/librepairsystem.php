<?php
# using :

####### Change log [Start] #######

#
#   Date    :   2020-10-27 Cameron
#               - add function insertFollowupPersonID(), deleteFollowUpPersonID() [case X190081]
#               - change sendNotifytoReporter to sendNotifyToRelatedPerson(), add parameter $InformReporter, $InformFollowupPerson [case X190077]
#
#   Date    :   2020-10-23 Cameron
#               - add function getFollowupPersonSelection(), checkRecordAccessRight(), getFollowupPersonID(), getGroupMembers()
#
#   Date    :   2020-10-22 Cameron
#               - add function isUsePlupload(), updateTempPluploadInfo(), getFollowupPhotos(), getPhotoListUI() [case #X190085]
#               - add parameter $status to getPhotosByRepairID()
#
#   Date    :   2020-10-21 Cameron
#               - add function getManagementGroupSelection() [case #X190081]
#
#   Date    :   2020-09-29 Cameron
#               - add define LOCATION_SHOW
#               - add filter IsShow=1 to getInventoryBuildingArray() and getInventoryLevelArray() [case #X195586]
#
#   Date    :   2020-08-25 Cameron
#               - add fitler IsShow=1 to getInventoryLocationArray() [case #X190073]
#
#   Date    :   2020-08-21 Cameron
#               - add LocationProperties to GET_MODULE_OBJ_ARR()
#               - copy Get_Mode_Toolbar(), initJavaScript(), Get_All_Building_Array(), Get_All_Floor_Array(), Get_All_Room_Array(),
#                 getRoomPropertyEditTable(), getRoomPropertyViewTable() from ebooking and modify
#
#   Date    :   2020-07-03 Cameron
#               return sql executed result in following functions: changeRequestStatus(), changeFollowUpPersonID(), resetFollowUpPerson(), changeFollowUpPerson(), resetFollowUpPersonID(),
#               changeInventoryItemID(), resetInventoryItemID(), updateRepairRecord(), addRecordRemark(), sendNotifyToReporter()
#
#   Date    :   2019-05-13 Cameron
#               fix potential sql injection problem by enclosing var with apostrophe
#
#	Date	:	2017-06-05  (Cameron)
#				Fix bug to support special characters like '"<>&\/ in changeFollowUpPerson()
#
#	Date	:	2017-05-25 	(Cameron)
#				add parameter $run_sql to insertMgmtGroup and set return value 
#
#	Date	:	2017-05-24  (Cameron)
#				- retrieve RecordStatus in getRepairSystemEmailContent()
#				- add function sendNotifyToReporter()
#
#	Date	:	2017-05-23	(Cameron)
#				- add function getRoomSelectionUI(), copy getInventoryNameByLang() from inventory
#				- modify returnRequestStatisticsForm_ui() to add location filter
#
#	Date	:	2017-05-10 	(Cameron)
#				add function insertRepairPhoto(), getPhotosByRepairID(), getPhotosByPhotoID(), getPhotoCSS(), getRepairReqPhotoUI()
#
#	Date	:	2016-06-30	(Cara)
#				Added changeInventoryItemID(), resetInventoryItemID() 
#
#	Date	:	2016-06-30	(Cara)
#				Added getGroupMemberByGroupId(), changeFollowUpPersonID(), resetFollowUpPersonID(), changeFollowUpPerson(), resetFollowUpPerson()
#
#	Date	:	2016-05-11  (Cameron)
#				replace split with explode in checkDateFormat() to support php 5.4
#
#   Date    :   2016-04-27  (Omas)
#               Fix getRequestSummarySelection() - exclude deleted category's RequestSummary
#
#	Date	:	2015-08-31	(Omas)
#				Fix getInventoryLocationArray() - exclude deleted building
#
#	Date	:	2014-11-05	(Omas)
#				New function retrieveNewCaseNumber() generate NewCaseNumber, record status - 4 = awaiting to process
#				Add New Status in the form returnRequestStatisticsForm_ui()
#
#	Date	:	2011-09-15 (Henry Chow)
#				add getRepairSystemEmailContent(), 
#
#	Date	:	2011-06-13 (Henry Chow)
#				modified returnRequestStatisticsForm_ui(), display the option of "Display Archived Record" in terms of checkbox 
#
#	Date	:	2011-06-09 (Henry Chow)
#				modified removeRepairRecord(), allow to remove "Archived" record
#
#	Date	:	2011-06-08 (Henry Chow)
#				added archiveRepairRecord(), archive "Completed" / "Rejected" record
#
#	Date	:	2011-03-28 (Henry Chow)
#				1) modified GET_MODULE_OBJ_ARR(), hide "Settings > Location" from menu
#				2) add parameter $DateOfApplyCampus
#
#	Date:	2011-01-03	YatWoon
#			update returnRequestStatisticsForm_ui(), add "rejected" status
#
#	Date:	2010-12-23	YatWoon
#			update returnRequestStatisticsForm_ui(), add status selection
#
#	Date:	2010-12-20	YatWoon
#			update updateGroupMember(), trim char "U" of userid "Uxxx"
# 
#	Date:	2010-12-15	YatWoon
#			update getCategorySelection(), getRequestSummarySelection(), add parameter to check need to cater with mgmt group or not
#
#	Date:	2010-11-26 [YatWoon]
#			add countRequest(), returnFirstCatID(),displayRequestSummary_ui()
#
#	Date:	2010-11-25 [YatWoon]
#			add insertRequestSummary(), returnRequestSummaryInfo(),updateRequestSummary(), getRequestSummaryTitle(), returnRequestStatisticsForm_ui()
#
#	Date:	2010-11-24 [YatWoon]
#			update GET_MODULE_OBJ_ARR(), add menu Settings > Request Summary
#
#	Date:	2010-11-23 [YatWoon]
#			update getCategorySelection(), remove tag setting in the function (js occurs if the flag off)
#
#	Date:	2010-10-27	[YatWoon]
#			update getCategorySelection(), set default of "Request Details" (flag: $sys_custom['RepairSystem_DefaultDetailsContent'])
#
####### Change log [End] #######


if (!defined("LIBREPAIRSYSTEM_DEFINED"))                     // Preprocessor directive
{
  define("LIBREPAIRSYSTEM_DEFINED", true);
  
	#### Record Status in REPAIR_SYSTEM_RECORD ####
	define("PENDING", 			"0");
	define("REPAIR_COMPLETED", 	"1");
	
	#### Record Status in REPAIR_SYSTEM_LOCATION / REPAIR_SYSTEM_CATEGORY ####
	define("REPAIR_SYSTEM_STATUS_APPROVED", 			"1");
	define("REPAIR_SYSTEM_STATUS_DELETED", 				"-1");

	#### Record Status in REPAIR_SYSTEM_RECORD ####
	define("REPAIR_RECORD_STATUS_CANCELLED", 			"0");
	define("REPAIR_RECORD_STATUS_PROCESS", 				"1");
	define("REPAIR_RECORD_STATUS_COMPLETED", 			"2");
	define("REPAIR_RECORD_STATUS_REJECTED", 			"3");
	define("REPAIR_RECORD_STATUS_PENDING", 				"4");
	define("REPAIR_RECORD_STATUS_DELETED", 				"-1");
	define("REPAIR_RECORD_STATUS_ARCHIVED", 			"-2");

    define("LOCATION_SHOW", 	"1");
	
	  
	class librepairsystem extends libdb
	{
        /*
        # Record
        var $NoticeID;
        var $NoticeNumber;
        var $Title;
        var $Module;  #added by Kelvin Ho 2008-11-12 for ucc
        var $DateStart;
        var $DateEnd;
        var $Description;
        var $IssueUserID;
        var $RecipientID;
        var $Question;
        var $ReplySlipContent;	# 20090611 added for reply slip content
        var $Attachment;
        var $RecordType;
        var $RecordStatus;
        var $DebitMethod;		# added by Henry 20100125
        var $IssueUserName;
        var $AllFieldsReq;
        */
        var $DateOfApplyCampus;

        function librepairsystem($ID="")
        {
	        
            //global $intranet_root,$intranet_version,$_SESSION;
            
            $this->libdb();
            $this->DateOfApplyCampus = "2011-03-27 00:00:00";
			/*
            if (!isset($_SESSION["SSV_PRIVILEGE"]["notice"]))
         	{
				include_once("libgeneralsettings.php");
				$lgeneralsettings = new libgeneralsettings();
				
				$settings_ary = $lgeneralsettings->Get_General_Setting($this->ModuleName);
				if(!empty($settings_ary))
				{
					foreach($settings_ary as $key=>$data)
					{
						$_SESSION["SSV_PRIVILEGE"]["notice"][$key] = $data;
						$this->$key = $data; 
					}
				}
				else
				{
					$this->disabled = false;
                 	$this->fullAccessGroupID = "";
                 	$this->normalAccessGroupID = "";
                 	$this->isClassTeacherEditDisabled = false;
                 	$this->isAllAllowed = false;
                 	$this->defaultNumDays = 4;
                 	$this->showAllEnabled = false;
                 	$this->DisciplineGroupID = "";
                 	$this->enablePaymentNotice = false;
				}
			}
			else
			{
				$this->disabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"];
				$this->fullAccessGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["fullAccessGroupID"];
				$this->normalAccessGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["normalAccessGroupID"];			
				$this->isClassTeacherEditDisabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["isClassTeacherEditDisabled"];
				$this->isAllAllowed = $_SESSION["SSV_PRIVILEGE"]["notice"]["isAllAllowed"];
				$this->defaultNumDays = $_SESSION["SSV_PRIVILEGE"]["notice"]["defaultNumDays"];
				$this->showAllEnabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["showAllEnabled"];			            
				$this->DisciplineGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["DisciplineGroupID"];		
				$this->enablePaymentNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["enablePaymentNotice"];		
             }
             
             if ($ID!="")
             {
                 $this->returnRecord($ID);
             }
             */
        }

        function GET_MODULE_OBJ_ARR()
        {
                global $UserID, $PATH_WRT_ROOT, $plugin, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root, $special_feature, $_SESSION;
                
                //include_once("libgeneralsettings.php");
				//$lgeneralsettings = new libgeneralsettings();
				//$moduleName = 'RepairSystem';
				//$settingName = array("'PaymentNoticeEnable'");
				//$enablePaymentNotice = $lgeneralsettings->Get_General_Setting($moduleName, $settingName);
				
                ### wordings 
                global  $Lang;
                
                # Current Page Information init
                $PageManagement 			= 0;
                $PageList					= 0;
                
				$PageSettings 				= 0;
                $PageCategorySettings 		= 0;
                $PageRequestSummary			= 0;
                $PageMgmtGroupSettings 		= 0;
                $PageRequestStatistics		= 0;
                
                switch ($CurrentPage) {
                    case "PageList":
                    	$PageManagement = 1;
                    	$PageList = 1;
                    	break;
                    case "PageCategorySettings":
                    	$PageSettings = 1;
                    	$PageCategorySettings = 1;
                    	break;
                     case "PageRequestSummary":
                    	$PageSettings = 1;
                    	$PageRequestSummarySettings = 1;
                    	break;	
                    case "PageLocationSettings":
                    	$PageSettings = 1;
                    	$PageLocationSettings = 1;
                    	break;
                    case "PageLocationProperties":
                        $PageSettings = 1;
                        $PageLocationProperties = 1;
                        break;
                    case "PageMgmtGroupSettings":
                    	$PageSettings = 1;
                    	$PageMgmtGroupSettings = 1;
                    	break;
                    case "PageRequestStatistics":
                    	$PageStatistics = 1;
                    	$PageRequestStatistics = 1;
                    	break;	
                    
                    	
                }
                
                if($CurrentPageArr['eAdminRepairSystem'])
                {
	                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/";
	                
					$MenuArr["Management"]	= array($Lang['Menu']['RepairSystem']['Management'], "#", $PageManagement);
					$MenuArr["Management"]["Child"]["List"] 	= array($Lang['RepairSystem']['AllRequests'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/?clearCoo=1", $PageList);
//					$MenuArr["Management"]["Child"]["List"] 	= array($Lang['RepairSystem']['AllRequests'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/?clearCoo=1", $PageList);
						
					
					if($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"]) {
						$MenuArr["Statistics"]								= array($Lang['RepairSystem']['Statistics'], "#", $PageStatistics);
						$MenuArr["Statistics"]["Child"]["RequestStatistics"] = array($Lang['RepairSystem']['RequestStatistics'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/statistics/request_statistics", $PageRequestStatistics);
						
						$MenuArr["Settings"]								= array($Lang['Menu']['RepairSystem']['Settings'], "#", $PageSettings);
						$MenuArr["Settings"]["Child"]["Category"] 			= array($Lang['RepairSystem']['Category'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/settings/category", $PageCategorySettings);
						$MenuArr["Settings"]["Child"]["RequestSummary"] 	= array($Lang['RepairSystem']['RequestSummary'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/settings/RequestSummary/?clearCoo=1", $PageRequestSummarySettings);
						//$MenuArr["Settings"]["Child"]["Location"] 			= array($Lang['RepairSystem']['Location'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/settings/location", $PageLocationSettings);
                        $MenuArr["Settings"]["Child"]["LocationProperty"] = array($Lang['RepairSystem']['LocationProperties'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/settings/location_property", $PageLocationProperties);
						$MenuArr["Settings"]["Child"]["ManagementGroup"] 	= array($Lang['RepairSystem']['MgmtGroup'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/RepairSystem/settings/mgmt_group", $PageMgmtGroupSettings);
						
						
					}
				}
				else if($CurrentPageArr['eServiceRepairSystem'])
				{
					$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/RepairSystem/";
					
					$MenuArr["Management"]	= array($Lang['RepairSystem']['AllRequests'], $PATH_WRT_ROOT."home/eService/RepairSystem/?clearCoo=1", $PageManagement);
		//			$MenuArr["Management"]	= array($Lang['RepairSystem']['AllRequests'], $PATH_WRT_ROOT."home/eService/RepairSystem/?clearCoo=1", $PageManagement);
									
				}

                # change page web title
                $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
                $js.= 'document.title="eClass Repair System";'."\n";
                $js.= '</script>'."\n";

                ### module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['RepairSystem'].$js;
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_repairsystem.gif";
                
                $MODULE_OBJ['menu'] = $MenuArr;
				
                return $MODULE_OBJ;
        }
        
		function IS_ADMIN_USER($ParUserID='')
		{
			return $_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"];
		}
		        
        function getMgmtGpInfo($GroupID)
        {
			if($GroupID!="") {
			 	$sql = "SELECT * FROM REPAIR_SYSTEM_GROUP WHERE GroupID='$GroupID'";   
			 	$result = $this->returnArray($sql);
			 	return $result;
			}
        }
        
        function updateMgmtGroup($GroupID, $dataAry=array())
        {
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "UPDATE REPAIR_SYSTEM_GROUP SET ";
			foreach($dataAry as $field=>$value)
				$sql .= $field . "=\"". $value."\", ";
			$sql .= "DateModified=now()";
			$sql .= "WHERE GroupID='$GroupID'";
			
			$this->db_db_query($sql);
        }
        
        function insertMgmtGroup($dataAry=array(),$run_sql=true)
        {
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "INSERT INTO REPAIR_SYSTEM_GROUP (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "RecordStatus, DateInput, DateModified) values (";
			foreach($values as $value)	$sql .= "'". $value ."', ";
			$sql .= REPAIR_SYSTEM_STATUS_APPROVED.", now(), now())";
			if ($run_sql) {
				$result = $this->db_db_query($sql);
				return $result ? $this->db_insert_id() : false;	
			}
			else {
				return $sql;
			}
        }
        
        function removeMgmtGroup($GroupIDAry=array())
        {
	        global $UserID;
	        
	        if(sizeof($GroupIDAry)>0) {
		        $gpList = implode("','",$GroupIDAry);
		        
				$sql = "UPDATE REPAIR_SYSTEM_GROUP SET RecordStatus=".REPAIR_SYSTEM_STATUS_DELETED.", DateModified=NOW(), LastModifiedBy='$UserID' WHERE GroupID IN ('$gpList')";
				$this->db_db_query($sql);
				/*
				$sql = "DELETE FROM REPAIR_SYSTEM_GROUP_MEMBER WHERE GroupID IN ($gpList)";
				$this->db_db_query($sql);
				*/
			}
        }
        
        function updateGroupMember($GroupID, $StudentIDAry)
        {
			for($i=0; $i<sizeof($StudentIDAry); $i++) 
			{
				$this_UserID = substr($StudentIDAry[$i], 0,1)=="U" ? substr($StudentIDAry[$i],1,strlen($StudentIDAry[$i])-1): $StudentIDAry[$i];
				$sql = "SELECT COUNT(UserID) FROM REPAIR_SYSTEM_GROUP_MEMBER WHERE GroupID='$GroupID' AND UserID='".$this_UserID."'";
				$result = $this->returnVector($sql);
				
				if($result[0]==0) {
					$sql = "INSERT INTO REPAIR_SYSTEM_GROUP_MEMBER SET GroupID='$GroupID', UserID='".$this_UserID."', DateInput=NOW()";	
					$this->db_db_query($sql);
				}
			}
        }
        
        function removeGroupMember($GroupID, $UserIDAry)
        {
	        if(sizeof($UserIDAry)>0) {
		        $sql = "DELETE FROM REPAIR_SYSTEM_GROUP_MEMBER WHERE GroupID='$GroupID' AND UserID IN ('".implode("','",$UserIDAry)."')";
		        $this->db_db_query($sql);
	        }
        }

		function removeLocation($LocIDAry=array())
		{
			global $UserID;
			
			if(sizeof($LocIDAry)>0) {
				$locList = implode("','",$LocIDAry);
				
				$sql = "UPDATE REPAIR_SYSTEM_LOCATION SET RecordStatus=".REPAIR_SYSTEM_STATUS_DELETED.", DateModified=now(), LastModifiedBy='$UserID' WHERE LocationID IN ('$locList')";
				$this->db_db_query($sql);
			}
		}
		
		function insertLocation($dataAry=array())
		{
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "INSERT INTO REPAIR_SYSTEM_LOCATION (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "RecordStatus, DateInput, DateModified, LastModifiedBy) values (";
			foreach($values as $value)	$sql .= "'". $value ."', ";
			$sql .= REPAIR_SYSTEM_STATUS_APPROVED.", now(), now(), '$UserID')";
			$this->db_db_query($sql);
		}		
		
		function getLocationInfo($LocID)
		{
			$sql = "SELECT * FROM REPAIR_SYSTEM_LOCATION WHERE LocationID='$LocID'";	
			return $this->returnArray($sql);
		}
		
		function updateLocation($LocID, $dataAry=array())
		{
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "UPDATE REPAIR_SYSTEM_LOCATION SET ";
			foreach($dataAry as $field=>$value)
			$sql .= $field . "=\"". $value."\", ";
			$sql .= "DateModified=now(), LastModifiedBy='$UserID'";
			$sql .= " WHERE LocationID='$LocID'";
			$this->db_db_query($sql);
		}	
		
		function getGroupSelection($GroupID="", $tag="")
		{
			$sql = "SELECT GroupID, GroupTitle FROM REPAIR_SYSTEM_GROUP WHERE RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." ORDER BY GroupTitle";	
			$result = $this->returnArray($sql,2);
			
			return getSelectByArray($result, "name='GroupID' id='GroupID' $tag", $GroupID);
		}
		
		function insertCategory($dataAry=array())
		{
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "INSERT INTO REPAIR_SYSTEM_CATEGORY (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "RecordStatus, DateInput, DateModified, LastModifiedBy) values (";
			foreach($values as $value)	$sql .= "'". $value ."', ";
			$sql .= REPAIR_SYSTEM_STATUS_APPROVED.", now(), now(), '$UserID')";
			$this->db_db_query($sql);
		}		

		function getCategoryInfo($CatID)
		{
			$sql = "SELECT * FROM REPAIR_SYSTEM_CATEGORY WHERE CategoryID='$CatID'";	
			return $this->returnArray($sql);
		}
		
		function updateCategory($CatID, $dataAry=array())
		{
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "UPDATE REPAIR_SYSTEM_CATEGORY SET ";
			foreach($dataAry as $field=>$value)
			$sql .= $field . "=\"". $value."\", ";
			$sql .= "DateModified=now(), LastModifiedBy='$UserID'";
			$sql .= " WHERE CategoryID='$CatID'";
			$this->db_db_query($sql);
		}	
		
		function removeCategory($CatIDAry=array())
		{
			global $UserID;
			
			if(sizeof($CatIDAry)>0) {
				$CatList = implode("','",$CatIDAry);
				
				$sql = "UPDATE REPAIR_SYSTEM_CATEGORY SET RecordStatus=".REPAIR_SYSTEM_STATUS_DELETED.", DateModified=now(), LastModifiedBy='$UserID' WHERE CategoryID IN ('$CatList')";
				$this->db_db_query($sql);
			}
		}		
			
		function getCategorySelection($CatID="", $tag="", $showAll=1, $firstOption="", $noFirst=0, $select_name="CatID", $NeedCheckMgmtGroup=0)
		{
			$select_name=$select_name ? $select_name : "CatID";
			if($NeedCheckMgmtGroup && !$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'])
			{
				global $UserID;
				$sql = "
					SELECT 
						a.CategoryID, a.Name 
					FROM 
						REPAIR_SYSTEM_CATEGORY as a
						inner join REPAIR_SYSTEM_GROUP_MEMBER as b on (b.GroupID=a.GroupID and b.UserID='$UserID')
					WHERE 
						a.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." ORDER BY a.Name";	
			}
			else
			{
				$sql = "SELECT CategoryID, Name FROM REPAIR_SYSTEM_CATEGORY WHERE RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." ORDER BY Name";	
			}
			$result = $this->returnArray($sql,2);
			
			return getSelectByArray($result, "name='". $select_name ."' id='CatID' $tag", $CatID, $showAll, $noFirst, $firstOption);
		}
		
		function retrieveRecordYears()
		{
			$sql = "select DISTINCT(DATE_FORMAT(DateInput,'%Y')) from REPAIR_SYSTEM_RECORDS ORDER BY DateInput";
			$result = $this->returnVector($sql);
			return $result;
		}
		
		function retrieveNewCaseNumber($CatID)
		{
			$sql = 'SELECT Max(RecordID) FROM REPAIR_SYSTEM_RECORDS';
			$LastID = $this->returnVector($sql);
			
			$CatInfo = $this->getCategoryInfo($CatID);
			$CatPrefix = $CatInfo[0]['CaseNumberPrefix'];
			
			$code = str_pad(($LastID[0]+1), 6, '0', STR_PAD_LEFT);
			$result = $CatPrefix.$code;
			return $result;
		}		
		
		function getLocationSelection($LocID="", $tag="", $showAll=0, $firstOption="")
		{
			$sql = "SELECT LocationID, LocationName FROM REPAIR_SYSTEM_LOCATION WHERE RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." ORDER BY LocationName";	
			$result = $this->returnArray($sql,2);
			
			return getSelectByArray($result, "name='locID' id='locID' $tag", $LocID, $showAll, 0, $firstOption);
		}		
		
		function insertRepairRecord($dataAry=array())
		{			
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "INSERT INTO REPAIR_SYSTEM_RECORDS (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "RecordStatus, DateInput, DateModified, LastModifiedBy) values (";
			foreach($values as $value)	$sql .= "'". $value ."', ";
			$sql .= REPAIR_RECORD_STATUS_PENDING.", now(), now(), '$UserID')";
			//echo $sql.'<br>';
			$this->db_db_query($sql);
			
			return $this->db_insert_id();
		}		

		function updateRepairRecord($id, $dataAry=array())
		{			
			global $UserID;
			
			//foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET ";
			foreach($dataAry as $field=>$value)
				$sql .= $field . "='". $value."', ";	
			$sql .= "DateModified=NOW(), LastModifiedBy='$UserID' WHERE RecordID='$id'";
			//echo $sql.'<p>';
			return $this->db_db_query($sql);
		}		
		
		function changeRequestStatus($RecordID, $status) 
		{
			global $UserID;
			
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET RecordStatus='$status', DateModified=NOW(), LastModifiedBy='$UserID' WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}
		
		function changeFollowUpPersonID($RecordID, $FollowUpPersonID)
		{
			global $UserID;
				
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET FollowUpPersonID='$FollowUpPersonID' WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}		
		
		function resetFollowUpPersonID($RecordID)
		{
			
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET FollowUpPersonID=NULL WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}	
		
		function changeFollowUpPerson($RecordID, $FollowUpPerson)
		{
			global $UserID;
		
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET FollowUpPerson='".$this->Get_Safe_Sql_Query($FollowUpPerson)."' WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}
		
		function resetFollowUpPerson($RecordID)
		{
				
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET FollowUpPerson=NULL WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}		
		
		function changeInventoryItemID($RecordID, $inventoryItemID)
		{
				
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET InventoryItemID='$inventoryItemID' WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}
		
		function resetInventoryItemID($RecordID)
		{
		
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET InventoryItemID=NULL WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}
				
		function addRecordRemark($RecordID, $remark, $userName="")
		{
			global $UserID, $Lang;
			
			$sql = "SELECT Remark FROM REPAIR_SYSTEM_RECORDS WHERE RecordID='$RecordID'";
			$result = $this->returnVector($sql);
			
			if($userName=="") {
				$stdInfo = $this->getStudentNameByID($UserID);
				$userName = $stdInfo[0][1];
			}			
				
			$remark = "\n[$userName ".$Lang['RepairSystem']['on']." ".date("Y-m-d H:i:s")."]  \n".$remark;
			
			if($result[0]!="") {
				$newRemark = $result[0].intranet_htmlspecialchars($remark);	
			} else {
				$newRemark = intranet_htmlspecialchars($remark);
			}
			
			$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET Remark=\"$newRemark\", DateModified=NOW(), LastModifiedBy='$UserID' WHERE RecordID='$RecordID'";
			return $this->db_db_query($sql);
		}
		
		function getStudentNameByID($ID)
		{
			$name_field = getNameFieldByLang('USR.');
			
			$sql = "SELECT USR.UserID, $name_field FROM INTRANET_USER USR WHERE USR.RecordType = 1 AND USR.RecordStatus IN (0,1,2) AND USR.UserID = '$ID'";
			$temp = $this->returnArray($sql,2);

			return $temp;
		}	
		
		function removeRepairRecord($RecordIDAry=array(), $FromArchivePage="")
		{
			global $UserID;
			
			if(sizeof($RecordIDAry)>0) {
				$RecordIDList = implode("','",$RecordIDAry);
				
				if($FromArchivePage) $conds = " OR ArchivedBy IS NOT NULL";
				
				$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET RecordStatus=".REPAIR_SYSTEM_STATUS_DELETED.", DateModified=now(), LastModifiedBy='$UserID' WHERE RecordID IN ('$RecordIDList') AND (RecordStatus='".REPAIR_RECORD_STATUS_CANCELLED."' $conds)";
				$this->db_db_query($sql);
//				debug_pr($sql);
//				die();
				return $this->db_affected_rows();
			}
		}				
		
		function userInMgmtGroup($parUserID="")
		{
			if($parUserID!="") {
				$sql = "SELECT COUNT(*) FROM REPAIR_SYSTEM_GROUP_MEMBER WHERE UserID='$parUserID'";	
				$result = $this->returnVector($sql);
				
				return $result[0];
			}
		}
		
		function memberInMgmtGroup($GroupID) 
		{
			$sql = "SELECT UserID FROM REPAIR_SYSTEM_GROUP_MEMBER WHERE GroupID='$GroupID'";	
			return $this->returnVector($sql);
		}
		
		function checkDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($year, $month, $day) = explode('-',$RecordDate);
			else 
				list($year, $month, $day) = explode('/',$RecordDate);
								
			if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
			{
				if(checkdate($month,$day,$year))
				{
					return true;
				}
			}
			return false;
		}		
		
		function returnRequestSummaryList($CatID='')
		{
			if($CatID)
			{
				
			}
			else
				return array();
		}
		
		function insertRequestSummary($dataAry=array())
		{
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "INSERT INTO REPAIR_SYSTEM_REQUEST_SUMMARY (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "DateInput, InputBy, DateModified, ModifyBy) values (";
			foreach($values as $value)	$sql .= "'". $value ."', ";
			$sql .= "now(), '$UserID', now(), '$UserID')";
 			$this->db_db_query($sql);
		}		
		
		function returnRequestSummaryInfo($RecordID='')
		{
			if($RecordID)
			{
				$sql = "select CategoryID, Title, RecordStatus from REPAIR_SYSTEM_REQUEST_SUMMARY where RecordID='$RecordID'";
				return $this->returnArray($sql);
			}
			else
			{
				return array();	
			}
			
		}
		
		function updateRequestSummary($id='', $dataAry=array())
		{
			global $UserID;
			
			if($id!='' && isset($dataAry) && is_array($dataAry))
			{
				$sql = "UPDATE REPAIR_SYSTEM_REQUEST_SUMMARY SET ";
				foreach($dataAry as $field=>$value)
					$sql .= $field . "='". $value."', ";	
				$sql .= "DateModified=NOW(), ModifyBy='$UserID' WHERE RecordID='$id'";
				$this->db_db_query($sql);
			}
		}
		
		function getRequestSummarySelection($CatID="", $tag="", $showAll=1, $firstOption="", $RecordStatus=1, $RecordID='',$NeedCheckMgmtGroup=0)
		{
			if($NeedCheckMgmtGroup && !$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'])
			{
				global $UserID;
				$sql="
					select a.RecordID, a.Title
					from 
					REPAIR_SYSTEM_REQUEST_SUMMARY as a
					inner join REPAIR_SYSTEM_CATEGORY as b on (b.CategoryID=a.CategoryID and b.RecordStatus=1)
					inner join REPAIR_SYSTEM_GROUP_MEMBER as c on (c.GroupID=b.GroupID and c.UserID='$UserID')
					";
			}
			else
			{
				$conds = $CatID ? " and a.CategoryID = '$CatID' " : "";
// #L95425 				
// 				$sql = "SELECT RecordID, Title FROM REPAIR_SYSTEM_REQUEST_SUMMARY WHERE RecordStatus=$RecordStatus $conds ORDER BY Sequence";
				$sql = "SELECT a.RecordID, a.Title FROM REPAIR_SYSTEM_REQUEST_SUMMARY as a inner join REPAIR_SYSTEM_CATEGORY as b on (b.CategoryID=a.CategoryID and b.RecordStatus = 1) WHERE a.RecordStatus = '$RecordStatus' $conds ORDER BY a.Sequence";
			}
			$result = $this->returnArray($sql);
			return getSelectByArray($result, "name='RequestID' id='RequestID' $tag", $RecordID, $showAll, 0, $firstOption);
		}
		
		function getRequestSummaryTitle($RecordID='')
		{
			$x = "";
			
			if($RecordID)
			{
				$sql = "select Title from REPAIR_SYSTEM_REQUEST_SUMMARY where RecordID='$RecordID'";
				$result = $this->returnVector($sql);
				$x = $result[0];
			}
			
			return $x;
		}
		
		function returnRequestStatisticsForm_ui($startdate='', $enddate='', $hiddenValue=0, $orderby='CategoryName', $Status='', $withArchive='', $TargetFloor='',$TargetRoom='')
		{
			global $Lang, $linterface, $category_selection, $select_all_btn, $i_Discipline_Generate_Update_Statistic, $i_alert_pleaseselect, $i_general_status;
			global $eDiscipline, $i_status_cancel, $i_To, $i_status_rejected;
			global $llocation_ui, $button_select_all;
			
			$StatusSelect = "<SELECT name='Status' id='Status'>\n";
			$StatusSelect.= "<OPTION value='' ".(($Status=='') ? "selected" : "").">".$eDiscipline['Setting_Status_All']."</OPTION>\n";
			$StatusSelect.= "<OPTION value='4' ".(($Status=='4') ? "selected" : "").">".$Lang['RepairSystem']['Pending']."</OPTION>\n";
			$StatusSelect.= "<OPTION value='1' ".(($Status=='1') ? "selected" : "").">".$Lang['RepairSystem']['Processing']."</OPTION>\n";
			$StatusSelect.= "<OPTION value='2' ".(($Status=='2') ? "selected" : "").">".$Lang['General']['Completed']."</OPTION>\n";
			$StatusSelect.= "<OPTION value='0' ".(($Status=='0') ? "selected" : "").">".$i_status_cancel."</OPTION>\n";
			$StatusSelect.= "<OPTION value='3' ".(($Status=='3') ? "selected" : "").">".$i_status_rejected."</OPTION>\n";
			//$StatusSelect.= "<OPTION value='-2' ".(($Status=='-2') ? "selected" : "").">".$Lang['eDiscipline']['Archived']."</OPTION>\n";
			$StatusSelect.= "</SELECT>&nbsp;\n";

			$building_selection = $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', '', 0, 0, '');
			$room_selection = $TargetRoom ? $this->getRoomSelectionUI($TargetFloor, $TargetRoom) : '';
			
			$x = "
				<script language=\"javascript\">
				<!--
				function SelectAll()
				{

					var obj = document.getElementById('CatID');
					var i;
					
					for (i=0; i<obj.length; i++)
					{
						obj.options[i].selected = true;
					}
				}
				
				function checkForm()
				{
					//// Reset div innerHtml
					reset_innerHtml();
					
					var obj = document.form1;
					var error_no=0;
					var focus_field='';
									
					if (compareDate(obj.startdate.value, obj.enddate.value) > 0) 
					{
						document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color=\"red\">". $Lang['SysMgr']['SchoolCalendar']['JSWarning']['EndDateLaterThanStartDate'] ."</font>';
						error_no++;
						if(focus_field==\"\")	focus_field = \"startdate\";
					}
					
					if (countOption(document.getElementById('CatID')) == 0) 
					{
						document.getElementById('div_Category_err_msg').innerHTML = '<font color=\"red\">". $i_alert_pleaseselect . $Lang['RepairSystem']['Category']  ."</font>';
						error_no++;
						if(focus_field==\"\")	focus_field = \"CatID\";
					}
					
					if(error_no>0)
					{
						eval(\"obj.\" + focus_field +\".focus();\");
						return false;
					}
					else
					{
						return true;
					}
				}
				
				function reset_innerHtml()
				{
				 	document.getElementById('div_DateEnd_err_msg').innerHTML = \"\";
				 	document.getElementById('div_Category_err_msg').innerHTML = \"\";
				}
				
				$(document).ready(function(){
					$('#TargetFloor').change(function(){
						if ($(this).val() != '') {
							$.ajax({
								dataType: \"json\",
								type: \"POST\",
								url: 'ajax_stat.php',
								data : {
									'action': 'getSubLocation',
									'LocationLevelID': $(this).val()
									
								},		  
								success: update_sublocation,
								error: show_ajax_error
							});
						}
						else {
							$('#SubLocationSelection').html('');
							$('#SublocationRow').css(\"display\",\"none\");
						}		
					});
					
					$('#selectAllBtnSubLoc').click(function(){
						$('#TargetRoom option').attr('selected', true);  		
					});	
				});
				
				
				function update_sublocation(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
						$('#SubLocationSelection').html(ajaxReturn.html);
						$('#SublocationRow').css(\"display\",\"\");
					}
				}	
				
				function show_ajax_error() {
					alert('".$Lang['General']['Error']."');
				}

				//-->
				</script>
				
				<form name=\"form1\" action=\"result.php\" onSubmit=\"return checkForm();\">
					<table class=\"form_table_v30\">
					
					<tr>
						<td class=\"field_title\"><span class=\"tabletextrequire\">*</span>". $Lang['RepairSystem']['RequestPeriod'] ."</td>
						<td>
							". $linterface->GET_DATE_PICKER("startdate", $startdate) . $i_To . ' ' . $linterface->GET_DATE_PICKER("enddate", $enddate) ."<br><span id=\"div_DateEnd_err_msg\"></span></td>
					</td>		
					
					<tr>
						<td class=\"field_title\"><span class=\"tabletextrequire\">*</span>". $Lang['RepairSystem']['Category'] ."</td>
						<td>". $category_selection ." ". $select_all_btn ."<br><span id=\"div_Category_err_msg\"></span></td>
					</tr>
					
					<tr>
						<td class=\"field_title\">". $i_general_status ."</td>
						<td>". $StatusSelect ."<input type='checkbox' name='withArchive' id='withArchive' value='1'".($withArchive?" checked":"")."><label for='withArchive'>".$Lang['RepairSystem']['IncludeArchivedRecord']."</label></td>
					</tr>

					<tr>
						<td class=\"field_title\">". $Lang['RepairSystem']['Location'] ."</td>
						<td>". $building_selection ."</td>
					</tr>

					<tr id=\"SublocationRow\" style=\"display:".($TargetRoom ? '': 'none')."\">
						<td class=\"field_title\">".$Lang['SysMgr']['Location']['Room']."</td>
						<td><span id=\"SubLocationSelection\">".$room_selection."</span>
						".$linterface->GET_BTN($button_select_all, "button", "", "selectAllBtnSubLoc")."
						<span class=\"indextvlink\">".$Lang['RepairSystem']['PressCtrlKey']."</span></td>
					</tr>
					
					<tr>
						<td class=\"field_title\"><span class=\"tabletextrequire\">*</span>". $Lang['StaffAttendance']['SortBy'] ."</td>
						<td>
							<input type='radio' name='orderby' value='CategoryName' id='orderby0' ". ($orderby=="CategoryName" ? "checked":"") ."> <label for='orderby0'>". $Lang['RepairSystem']['Category'] ."</label>
							<input type='radio' name='orderby' value='Title' id='orderby1' ". ($orderby=="Title" ? "checked":"") .">  <label for='orderby1'>". $Lang['RepairSystem']['RequestSummary'] ."</label>
							<input type='radio' name='orderby' value='c' id='orderby2' ". ($orderby=="c" ? "checked":"") .">  <label for='orderby2'>". $Lang['RepairSystem']['NoOfRecords'] ."</label>
						</td>
					</tr>
					
					</table>
					
					". $linterface->MandatoryField() ."
					
					<div class=\"edit_bottom_v30\">
					<p class=\"spacer\"></p>
						".  $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit", "") ."
					<p class=\"spacer\"></p>
					</div>
				";
				
			if($hiddenValue==1)
			{
				$x .="
					<input type=\"hidden\" name=\"pageNo\" value=\"". $li->pageNo ."\" />
					<input type=\"hidden\" name=\"order\" value=\"". $li->order ."\" />
					<input type=\"hidden\" name=\"field\" value=\"". $li->field ."\" />
					<input type=\"hidden\" name=\"page_size_change\" value=\"\" />
					<input type=\"hidden\" name=\"numPerPage\" value=\"". $li->page_size ."\" />
					";
			}
			
			$x .="</form>";
			
			return $x;
			
		}
		
		function countRequest($RecordID)
		{
			# retrieve original category
			$sql = "select CategoryID from REPAIR_SYSTEM_REQUEST_SUMMARY where RecordID='$RecordID'";
			$result = $this->returnVector($sql);
			$CatID = $result[0];
			
			$sql = "select 
						count(a.Title) 
					from 
						REPAIR_SYSTEM_RECORDS as a
						left join REPAIR_SYSTEM_REQUEST_SUMMARY as b on (b.Title=a.Title)
					where
						b.RecordID='$RecordID'
						and a.CategoryID='$CatID'
					";
			$result = $this->returnVector($sql);
			return $result[0];
		}
		
		function returnFirstCatID()
		{
			$sql = "SELECT CategoryID FROM REPAIR_SYSTEM_CATEGORY WHERE RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." ORDER BY Name limit 1";
			$result = $this->returnVector($sql);
			return $result[0];	
		}
		
		function displayRequestSummary_ui($CatID='', $in_use='-1', $keyword='')
		{
			global $Lang, $image_path, $LAYOUT_SKIN, $i_no_record_exists_msg;
			
			$CatID = $CatID=="" ? $this->returnFirstCatID() : $CatID;
			$conds .= ($in_use != "-1" && isset($in_use)) ? " AND a.RecordStatus='$in_use'" : "";
//			$keyword = convertKeyword($keyword);
//			$conds .= $keyword != "" ? " AND a.Title like '%$keyword%'" : "";
			if ($keyword) {
				$ukw = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));		// A&<>'"\B ==> A&<>\'\"\\\\B
				$ckw = intranet_htmlspecialchars(str_replace("\\","\\\\",$keyword));	// A&<>'"\B ==> A&amp;&lt;&gt;\&#039;&quot;\\\\B
				$conds .= " AND (a.Title like '%$ukw%' OR a.Title like '%$ckw%') ";	
			}	
			
			$sql = "select
						a.RecordID,
						a.Title,
						if(a.RecordStatus=1, '". $Lang['SysMgr']['FormClassMapping']['InUse'] ."','". $Lang['SysMgr']['FormClassMapping']['NotInUse'] ."') as InUseStatus,
						a.Sequence,
						CONCAT('<input type=checkbox name=RecordID[] id=RecordID[] value=', a.RecordID ,'>') as checkbox
					from
						REPAIR_SYSTEM_REQUEST_SUMMARY as a
						left join REPAIR_SYSTEM_CATEGORY as b on (b.CategoryID=a.CategoryID)
					where
						a.CategoryID='$CatID'
						$conds
					order by
						a.Sequence
					";
			$result = $this->returnArray($sql);
			
			$display = "";
			if(!empty($result))
			{
				for($i=0;$i<sizeof($result);$i++)
				{
					list($this_RecordID, $this_Title, $this_InUseStatus, $this_Sequence, $this_checkbox) = $result[$i];
					$display.="<tr>";
					$display.="<td>". ($i+1) ."</td>";
					$display.="<td>". $this_Title ."</td>";
					$display.="<td>". $this_InUseStatus ."</td>";
					
					# display order image
					if(sizeof($result)>1) 
					{
						if($i==0)	# first
						{
							$ordering_img = "
								<img src={$image_path}/{$LAYOUT_SKIN}/10x10.gif width=30 height=2>
								<a href=\"javascript:change_order('down',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Down'  title='$i_Discipline_System_Award_Punishment_Move_Down'></a>
								<a href=\"javascript:change_order('bottom',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Bottom'  title='$i_Discipline_System_Award_Punishment_Move_Bottom'></a>
								";
						}
						else if($i==sizeof($result)-1)	# last
						{
							$ordering_img = "
								<a href=\"javascript:change_order('top',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Top'  title='$i_Discipline_System_Award_Punishment_Move_Top'></a>
								<a href=\"javascript:change_order('up',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Up'  title='$i_Discipline_System_Award_Punishment_Move_Up'></a>
								";			
						}
						else 
						{
							$ordering_img = "
								<a href=\"javascript:change_order('top',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Top'  title='$i_Discipline_System_Award_Punishment_Move_Top'></a>
								<a href=\"javascript:change_order('up',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Up'  title='$i_Discipline_System_Award_Punishment_Move_Up'></a>
								<a href=\"javascript:change_order('down',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Down'  title='$i_Discipline_System_Award_Punishment_Move_Down'></a>
								<a href=\"javascript:change_order('bottom',$this_RecordID)\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif\" border='0' alt='$i_Discipline_System_Award_Punishment_Move_Bottom'  title='$i_Discipline_System_Award_Punishment_Move_Bottom'></a>
								";
						}
					}
					else
					{
						$ordering_img = "---";
					}
					$display.="<td>". $ordering_img."</td>";
					$display.="<td>". $this_checkbox."</td>";
					$display.="</tr>";
				}	
			}
			else
			{
				$display.="<tr>";
				$display.="<td colspan='5' align='center'><br>". $i_no_record_exists_msg."<br><br></td>";
				$display.="</tr>";
			}
			
			$x = "
				<table class=\"common_table_list\">
				<thead>
					<tr>
						<th class=\"num_check\">#</th>
						<th>". $Lang['RepairSystem']['RequestSummary'] ."</th>
						<th>". $Lang['General']['Status'] ."</th>
						<th>". $Lang['RepairSystem']['ReOrder'] ."</th>
						<th class=\"num_check\"><input type=\"checkbox\" onClick=(this.checked)?setChecked(1,this.form,'RecordID[]'):setChecked(0,this.form,'RecordID[]')></th>
					</tr>
				</thead>
				
				<tbody>
				". $display ."
				</tbody>
				</table>
			";
			
			return $x;
		}
		
		function getInventoryBuildingArray()
		{
			$sql = "SELECT 
			            b.BuildingID, ".
                        Get_Lang_Selection("b.NameChi", "b.NameEng")." as BuildingName 
                    FROM 
                        INVENTORY_LOCATION_BUILDING b
                        INNER JOIN INVENTORY_LOCATION_LEVEL lvl ON (lvl.BuildingID=b.BuildingID)
                        INNER JOIN INVENTORY_LOCATION loc ON loc.LocationLevelID=lvl.LocationLevelID
                    WHERE 
                        b.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED."
                        AND lvl.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED."
                        AND loc.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED."
                        AND loc.IsShow=".LOCATION_SHOW."
                        GROUP BY b.BuildingID 
                        ORDER BY b.DisplayOrder";
			return $this->returnArray($sql);	
		}
		
		function getInventoryLevelArray($BuildingID="") 
		{
			if($BuildingID!="") {
				$buildingConds = " lvl.BuildingID='$BuildingID' AND ";	
			}
			$sql = "SELECT 
			            lvl.BuildingID, 
			            lvl.LocationLevelID, ".Get_Lang_Selection("lvl.NameChi", "lvl.NameEng")." as LevelName 
			        FROM 
			            INVENTORY_LOCATION_LEVEL lvl 
			            INNER JOIN INVENTORY_LOCATION_BUILDING b ON (lvl.BuildingID=b.BuildingID AND lvl.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED.") 
			            INNER JOIN INVENTORY_LOCATION loc ON loc.LocationLevelID=lvl.LocationLevelID
			        WHERE 
			            $buildingConds
			            b.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." 
			            AND lvl.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." 
			            AND loc.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED."
			            AND loc.IsShow=".LOCATION_SHOW."
			            GROUP BY lvl.LocationLevelID
			            ORDER BY b.DisplayOrder, lvl.DisplayOrder";
			return $this->returnArray($sql);
		}
		
		function getInventoryLocationArray($LocationLevelID="")
		{
			if($LocationLevelID!="") {
				$locationConds = " loc.LocationLevelID='$LocationLevelID' AND ";	
			}
			$sql = "SELECT 
			            loc.LocationLevelID, 
			            loc.LocationID, ".
                        Get_Lang_Selection("loc.NameChi", "loc.NameEng")." as LocationName 
                    FROM 
                        INVENTORY_LOCATION loc 
                        INNER JOIN INVENTORY_LOCATION_LEVEL lvl ON (lvl.LocationLevelID=loc.LocationLevelID) 
                        INNER JOIN INVENTORY_LOCATION_BUILDING b ON (b.BuildingID=lvl.BuildingID) 
                    WHERE 
                        $locationConds 
                        loc.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." 
                        AND lvl.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." 
                        AND b.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." 
                        AND loc.IsShow=".LOCATION_SHOW."
                        ORDER BY b.DisplayOrder, lvl.DisplayOrder, loc.DisplayOrder";
			//echo $sql;
			return $this->returnArray($sql);
		}
		
		function getBuildingNameByID($buildingID="")
		{
			$sql = "SELECT BuildingID, ".Get_Lang_Selection("NameChi", "NameEng")." as BuildingName FROM INVENTORY_LOCATION_BUILDING WHERE BuildingID='$buildingID' AND RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED;
			$temp = $this->returnArray($sql,2);
			return $temp[0];
		}

		function getLocationLevelNameByID($LocationLevelID="")
		{
			$sql = "SELECT LocationLevelID, ".Get_Lang_Selection("NameChi", "NameEng")." as LocationLevelName FROM INVENTORY_LOCATION_LEVEL WHERE LocationLevelID='$LocationLevelID' AND RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED;
			$temp = $this->returnArray($sql,2);
			return $temp[0];
		}
	
		function getLocationNameByID($LocationID="")
		{
			$sql = "SELECT LocationID, ".Get_Lang_Selection("NameChi", "NameEng")." as LocationName FROM INVENTORY_LOCATION WHERE LocationID='$LocationID' AND RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED;
			$temp = $this->returnArray($sql,2);
			return $temp[0];
		}
		
		function archiveRepairRecord($RecordIDAry=array())
		{
			global $UserID;
			
			if(sizeof($RecordIDAry)>0) {
				$RecordIDList = implode(',',$RecordIDAry);
				
				$sql = "UPDATE REPAIR_SYSTEM_RECORDS SET ArchivedBy='$UserID', DateArchived=NOW(), DateModified=now(), LastModifiedBy='$UserID' WHERE RecordID IN ($RecordIDList) AND RecordStatus IN (".REPAIR_RECORD_STATUS_COMPLETED.",".REPAIR_RECORD_STATUS_REJECTED.")";
				
				$this->db_db_query($sql);
				
				return $this->db_affected_rows();
			}
		}
		
		function getRepairSystemEmailContent($RecordID)
		{
			global $Lang;
//			$buildingName = Get_Lang_Selection("bu.NameChi", "bu.NameEng");
//			$levelName = Get_Lang_Selection("lv.NameChi", "lv.NameEng");
//			$LocationName = Get_Lang_Selection("l.NameChi", "l.NameEng");
//			IF((r.LocationBuildingID!=0),	
//				CONCAT(IF($buildingName IS NOT NULL, $buildingName, '".$Lang['RepairSystem']['MainBuilding']."'), IF($levelName!='', CONCAT(' &gt; ', $levelName), ''), IF($LocationName IS NOT NULL, CONCAT(' &gt; ', $LocationName), ''), IF(r.DetailsLocation!='', CONCAT(' &gt; ', r.DetailsLocation), '')), 
//				CONCAT(lo.LocationName, IF((r.DetailsLocation IS NOT NULL AND r.DetailsLocation!=''), CONCAT(' &gt; ', r.DetailsLocation), ''))) 
//			as LocationName,
			
			$sql = "SELECT 
						r.UserID,
						r.Title, 
						c.Name as CatName, 
						r.Content, 
						r.Remark, 
						r.DateInput,
						IF((r.LocationBuildingID!=0),	
							CONCAT(IF(bu.NameChi IS NOT NULL, bu.NameChi, '".$Lang['RepairSystem']['B5']['MainBuilding']."'), IF(lv.NameChi!='', CONCAT(' &gt; ', lv.NameChi), ''), IF(l.NameChi IS NOT NULL, CONCAT(' &gt; ', l.NameChi), ''), IF(r.DetailsLocation!='', CONCAT(' &gt; ', r.DetailsLocation), '')), 
							CONCAT(lo.LocationName, IF((r.DetailsLocation IS NOT NULL AND r.DetailsLocation!=''), CONCAT(' &gt; ', r.DetailsLocation), ''))) 
						as LocationNameB5,
						IF((r.LocationBuildingID!=0),	
							CONCAT(IF(bu.NameChi IS NOT NULL, bu.NameEng, '".$Lang['RepairSystem']['EN']['MainBuilding']."'), IF(lv.NameEng!='', CONCAT(' &gt; ', lv.NameEng), ''), IF(l.NameEng IS NOT NULL, CONCAT(' &gt; ', l.NameEng), ''), IF(r.DetailsLocation!='', CONCAT(' &gt; ', r.DetailsLocation), '')), 
							CONCAT(lo.LocationName, IF((r.DetailsLocation IS NOT NULL AND r.DetailsLocation!=''), CONCAT(' &gt; ', r.DetailsLocation), ''))) 
						as LocationNameEN,
						g.GroupTitle,
						r.RecordStatus,
						r.CaseNumber
					FROM 
						REPAIR_SYSTEM_RECORDS r INNER JOIN 
						REPAIR_SYSTEM_CATEGORY c ON (c.CategoryID=r.CategoryID) 
						LEFT JOIN INVENTORY_LOCATION l ON (l.LocationID=r.LocationID)
						LEFT JOIN INVENTORY_LOCATION_LEVEL lv ON (lv.LocationLevelID=r.LocationLevelID)
						LEFT JOIN INVENTORY_LOCATION_BUILDING as bu on (bu.BuildingID=r.LocationBuildingID)
						LEFT JOIN REPAIR_SYSTEM_LOCATION as lo on (lo.LocationID=r.LocationID)
						LEFT JOIN REPAIR_SYSTEM_GROUP g ON (g.GroupID=c.GroupID)
					WHERE 
						r.RecordID='$RecordID'";
						
			$result = $this->returnArray($sql);
			return $result[0];	
		}				
		
		function getGroupMemberByGroupId($groupId){
			$sql = "SELECT
							MEM.UserID,
							u.EnglishName,
							u.ChineseName
					FROM
							REPAIR_SYSTEM_GROUP_MEMBER AS MEM
							INNER JOIN REPAIR_SYSTEM_GROUP AS GP ON (GP.GroupID=MEM.GroupID)
							INNER JOIN INTRANET_USER AS u ON(u.UserID = MEM.UserID) 
					WHERE
							GP.GroupID='$groupId'";
			
			$result = $this->returnArray($sql);
			return $result;
			
		}
		
		function insertRepairPhoto($dataAry=array())
		{			
			global $UserID;
			
			foreach($dataAry as $fields[]=>$values[]);
			
			$sql = "INSERT INTO REPAIR_SYSTEM_PHOTOS (";
			foreach($fields as $field)	$sql .= $field .", ";
			$sql .= "InputDate, LastModifiedBy) values (";
			foreach($values as $value)	$sql .= "'". $value ."', ";
			$sql .= "now(), '$UserID')";

			$this->db_db_query($sql);
			
			return $this->db_insert_id();
		}		


		function getPhotosByPhotoID($photoID) {
			$sql = "SELECT
							PhotoID,
							FileName,
							FileHashName,
							SizeInBytes
					FROM
							REPAIR_SYSTEM_PHOTOS
					WHERE
							PhotoID='$photoID'";
			
			$result = $this->returnResultSet($sql);
			return $result;
		}


		## RecordStatus default is 4 - pending
		function getPhotosByRepairID($repairID, $status='4') {
            $condition = $status ? " AND RecordStatus='".$status."' " : "";
			$sql = "SELECT
							PhotoID,
							FileName,
							FileHashName,
							SizeInBytes
					FROM
							REPAIR_SYSTEM_PHOTOS
					WHERE
							RecordID='$repairID'
							{$condition}
					ORDER BY PhotoID";

			$result = $this->returnResultSet($sql);
			return $result;
		}
		
		function getPhotoCSS() {
			$x = '<style>
					.repair_photo_table tr td {border-bottom: 0px solid #FFFFFF;}
					.repair_photo {width:200px; height:120px; position:relative; display:inline-block; }
					.repair_photo img {width:auto; max-width:100%; height:auto; max-height:85%; border:0;}
					.repair_photo .name {text-align:left;}
				</style>';
			return $x;
		}
		
		function getRepairReqPhotoUI($recordID) {
            return $this->getPhotoListUI($recordID);
		}
		

		// copy from libinventory.php
        function getInventoryNameByLang($prefix ="")
        {
			global $intranet_session_language;
			$chi = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
			if ($chi)
			{
				 $firstChoice = "NameChi";
				 $altChoice = "NameEng";
			}
			else
			{
				 $firstChoice = "NameEng";
				 $altChoice = "NameChi";
			}
	        $inventory_item_name_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
			
			return $inventory_item_name_field;
		}

		function getRoomSelectionUI($TargetFloor, $TargetRoom='') {
			global $Lang;
			
			if ($TargetFloor) {
				$sql = "SELECT LocationID, ".$this->getInventoryNameByLang()." AS LocationName FROM INVENTORY_LOCATION WHERE LocationLevelID ='".$TargetFloor."' AND RecordStatus = 1 order by DisplayOrder";
				$result = $this->returnResultSet($sql);
			}
			$x = '';
			ob_start();			
		?>
			<select id="TargetRoom" name="TargetRoom[]" multiple size="10">
				<option value="" selected><?=$Lang['RepairSystem']['SelectSublocation']?></option>
			<? foreach((array)$result as $rs):?>
				<?	if ($TargetRoom && (in_array($rs['LocationID'],(array)$TargetRoom))) {
						$selected = ' selected';
				  	}
				  	else {
				  		$selected = '';
				  	}
				?>
				<option value="<?=$rs['LocationID']?>"<?=$selected?>><?=$rs['LocationName']?></option>
			<? endforeach;?>
			</select>
		<?			
	 		$x = ob_get_contents();
			ob_end_clean();
			return $x;
		}
		
		function sendNotifyToRelatedPerson($RecordID, $InformReporter=0, $InformFollowupPerson=0, $messageWay=1){
			global $intranet_root, $plugin, $Lang, $eclassAppConfig;
			include_once($intranet_root."/includes/libuser.php");
			include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
			
			$recordData = $this->getRepairSystemEmailContent($RecordID);
			switch ($recordData['RecordStatus']) {
				case REPAIR_RECORD_STATUS_PROCESS:
						$subject = str_replace("__CASENUMBER__",$recordData['CaseNumber'],$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Process']);
						$statusB5 = $Lang['RepairSystem']['B5']['StatusProcess'];				
						$statusEN = $Lang['RepairSystem']['EN']['StatusProcess'];
					break;
				case REPAIR_RECORD_STATUS_COMPLETED:
						$subject = str_replace("__CASENUMBER__",$recordData['CaseNumber'],$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Completed']);
						$statusB5 = $Lang['RepairSystem']['B5']['StatusCompleted'];				
						$statusEN = $Lang['RepairSystem']['EN']['StatusCompleted'];
					break;
					
				case REPAIR_RECORD_STATUS_REJECTED:
						$subject = str_replace("__CASENUMBER__",$recordData['CaseNumber'],$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Rejected']);
						$statusB5 = $Lang['RepairSystem']['B5']['StatusRejected'];				
						$statusEN = $Lang['RepairSystem']['EN']['StatusRejected'];
					break;
					
				case REPAIR_RECORD_STATUS_PENDING:
						$subject = str_replace("__CASENUMBER__",$recordData['CaseNumber'],$Lang['AppNotifyMessage']['RepairSystem']['Subject']['Pending']);
						$status = $Lang['RepairSystem']['Pending'];
						$statusB5 = $Lang['RepairSystem']['B5']['StatusPending'];
						$statusEN = $Lang['RepairSystem']['EN']['StatusPending'];
					break;   	
			} 
			
			$content = str_replace("__DATEINPUT__", substr($recordData['DateInput'],0,10), $Lang['AppNotifyMessage']['RepairSystem']['Content']);
			$content = str_replace("__CATEGORY__", $recordData['CatName'], $content);
			$content = str_replace("__LOCATION_B5__", $recordData['LocationNameB5'], $content);
			$content = str_replace("__LOCATION_EN__", $recordData['LocationNameEN'], $content);
			$content = str_replace("__SUMMARY__", $recordData['Title'], $content);
			$content = str_replace("__DETAILS__", $recordData['Content'], $content);
			$content = str_replace("__MGMTGROUP__", $recordData['GroupTitle'], $content);
			$content = str_replace("__REMARK__", ($recordData['Remark']=="" ? $Lang['General']['EmptySymbol'] : $recordData['Remark']), $content);
			$content = str_replace("__STATUS_B5__", $statusB5, $content);
			$content = str_replace("__STATUS_EN__", $statusEN, $content);

            $leClassApp = new libeClassApp();
            $method = 'ByEmail';    // default notify method, ByApp has higher prority
            $individualMessageInfoAry = array();

			if ($InformReporter == 1) {
                $reporterID = $recordData['UserID'];
                $luser = new libuser($reporterID);
                $userType = $luser->RecordType;

                switch ($userType) {
                    case USERTYPE_STAFF:
                        if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T')) {
//                            $method = 'ByApp';
                            $appType = 'T';
                            $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$reporterID][] = $reporterID;
                        }
                        break;

                    case USERTYPE_STUDENT:
                        if ($plugin['eClassStudentApp'] && $leClassApp->isEnabledStudentApp() && $leClassApp->isSchoolInLicense('P')) {        // student use parent's license
//                            $method = 'ByApp';
                            $appType = 'S';
                            $_targetStudentId = $leClassApp->getDemoSiteUserId($reporterID);
                            $StudentAssoAry[$reporterID] = array($_targetStudentId);
                            $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
                        }
                        break;

                    case USERTYPE_PARENT:
                        if ($plugin['eClassApp'] && $leClassApp->isSchoolInLicense('P')) {
//                            $method = 'ByApp';
                            $appType = 'P';

                            if (!empty($reporterID)) {
                                $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo('', array($reporterID)), 'ParentID', $IncludedDBField = array('StudentID'), $SingleValue = 1, $BuildNumericArray = 1);
                            } else {
                                $parentStudentAssoAry = array();
                            }
                            $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
                        }
                        break;
                }
            }

			if ($InformFollowupPerson == 1) {
                $followupPersonInfo = $this->getFollowupPersonID($RecordID);
                $followupPersonIDAry = Get_Array_By_Key($followupPersonInfo, 'UserID');
                if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T')) {
//                    $method = 'ByApp';
                    $appType = 'T';
                    foreach((array)$followupPersonIDAry as $followupPersonID) {
                        $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$followupPersonID][] = $followupPersonID;
                    }
                }
            }

			if ($messageWay) {	// higher priority than email
				$content = intranet_undo_htmlspecialchars(strip_tags(str_replace("<br>","\n",$content)));
				$notifyMessageId = $leClassApp->sendPushMessage($individualMessageInfoAry, $subject, $content, $isPublic='N', $recordStatus=1, $appType, $sendTimeMode='', $sendTimeString='', '', '', 'RepairSystem', $RecordID);
				return $notifyMessageId;
			}
			else {		// ByEmail
				include_once($intranet_root."/includes/libwebmail.php");
				include_once($intranet_root."/lang/email.php");
				$lwebmail = new libwebmail();
                $followupPersonIDAry[] = $reporterID;
                $emailPerson = $followupPersonIDAry;
				$exmail_success = $lwebmail->sendModuleMail((array)$emailPerson, $subject, nl2br($content), 1);
				return $exmail_success;
			}
		}
		
		function getInventoryGroupMember($groupID) {
			$namefield = getNameFieldWithClassNumberByLang("u.");
			$sql = "SELECT 
							u.UserID, 
							$namefield as UserName
					FROM 
							INVENTORY_ADMIN_GROUP_MEMBER m
					LEFT OUTER JOIN
							INTRANET_USER u ON u.UserID = m.UserID
					WHERE 
							m.AdminGroupID = '$groupID' 
						AND	m.RecordType IN (1,2,3) 
						AND	m.RecordStatus IN (0,1,2)
					ORDER BY
							u.ClassName, u.ClassNumber, u.EnglishName";
			
			$result = $this->returnResultSet($sql);
			
			return $result;
		}

		function getInventoryGroupName($groupID) {
			
			$sql = "SELECT ".
							$this->getInventoryNameByLang()." AS GroupName 
					FROM 
							INVENTORY_ADMIN_GROUP
					WHERE 
							AdminGroupID = '$groupID'";
			
			$result = $this->returnResultSet($sql);
			return count($result) ? $result[0]['GroupName'] : '';
		}
	
		function getInventoryPhoto($itemID) {
			if ($itemID) {
				$sql = "SELECT
								PhotoPath,
								PhotoName
						FROM
								INVENTORY_PHOTO_PART
						WHERE
								ItemID='$itemID'
						ORDER BY PartID";
				
				$result = $this->returnResultSet($sql);
			}
			else {
				$result = "";
			}
			return $result;
		}

        function Get_Mode_Toolbar($ModeArr)
        {
            global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

            $numOfMode = count($ModeArr);
            $Toolbar = '';

            $Toolbar .= '<span class="thumb_list">' . "\n";
            for ($i = 0; $i < $numOfMode; $i ++) {
                list ($page_name, $icon_file, $page_link, $is_current_mode) = $ModeArr[$i];

                if ($i > 0)
                    $Toolbar .= " | ";

                $Toolbar .= "<img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/" . $icon_file . "' align='absmiddle'>";

                if ($is_current_mode)
                    $Toolbar .= "<span>" . $page_name . "</span>";
                else
                    $Toolbar .= "<a href='$page_link'>" . $page_name . "</a>";
            }
            $Toolbar .= '</span>' . "\n";

            return $Toolbar;
        }

        function getRoomPropertyViewTable($targetBuilding, $targetFloor, $targetRoom, $pageNo = '', $numPerPage = '', $order = '', $SortField = '')
        {
            global $Lang, $image_path, $LAYOUT_SKIN, $intranet_root, $PATH_WRT_ROOT;
            include_once ("libdbtable.php");
            include_once ("libdbtable2007a.php");

            $li = new libdbtable2007($SortField, $Order, $pageNo);
            $li->page_size = $numPerPage;
            $li->total_row = sizeof($arrResult);
            $li->form_name = "form1";
            $li->numPerPage_name = 'numPerPage_RepairSystem_Settings_Room_View';
            $li->pageNo_name = 'pageNo_RepairSystem_Settings_Room_View';

            if ($targetBuilding == "undefined")
                $targetBuilding = "";
            if ($targetFloor == "undefined")
                $targetFloor = "";

            $ModeArr = '';
            $ModeArr[] = array(
                $Lang['Btn']['View'],
                "icon_view.gif",
                "",
                1
            );
            if (($targetBuilding != "") && ($targetFloor != ""))
                $ModeArr[] = array(
                    $Lang['Btn']['Edit'],
                    "icon_edit_b.gif",
                    "javascript:Go_Edit_Mode();",
                    0
                );
            $mode_toolbar = $this->Get_Mode_Toolbar($ModeArr);

            $table_content .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
            $table_content .= "<tr>";
            $table_content .= "<td align='right'>" . $mode_toolbar . "</td>";
            $table_content .= "</tr>";
            $table_content .= "</table>";

            $table_content .= "<table border='0' cellpadding='4' cellspacing='0' width='100%'>";
            $table_content .= "<tbody>";
            if (($targetBuilding != "") && ($targetFloor != "")) {
                if ($targetBuilding != "")
                    $cond .= " AND building.BuildingID = '$targetBuilding' ";
                if ($targetFloor != "")
                    $cond .= " AND floor.LocationLevelID = '$targetFloor' ";
                if ($targetRoom != "")
                    $cond .= " AND room.LocationID = '$targetRoom' ";

                $sql = "SELECT 
						floor.LocationLevelID,
						room.LocationID,
						" . $this->getInventoryNameByLang("floor.") . ",
						" . $this->getInventoryNameByLang("room.") . "
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
					WHERE
						building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
						$cond
					ORDER BY building.DisplayOrder, floor.DisplayOrder, room.DisplayOrder";
                $arrResult = $this->returnArray($sql);

                if ($pageNo == 1) {
                    $start = $pageNo;
                    $end = $numPerPage;
                    $li->n_start = $start - 1;
                    $li->n_end = min(sizeof($arrResult), ($li->pageNo * $li->page_size));
                } else {
                    $start = ($pageNo * $numPerPage) - $numPerPage + 1;
                    $end = ($pageNo * $numPerPage);
                    $li->n_start = $start - 1;
                    $li->n_end = min(sizeof($arrResult), ($li->pageNo * $li->page_size));
                }

                if (sizeof($arrResult) > 0) {
                    $arrLocationID = array();
                    $arrLocationName = array();
                    for ($i = 0; $i < sizeof($arrResult); $i ++) {
                        list ($location_id, $sub_location_id, $location_name, $sub_location_name) = $arrResult[$i];
                        if (! in_array($location_id, $arrLocationID)) {
                            $arrLocationID[] = $location_id;
                            $arrLocationName[$location_id] = $location_name;
                        }
                        $arrSubLocationID[$location_id][] = $sub_location_id;
                        $arrSubLocationName[$location_id][$sub_location_id] = $sub_location_name;
                    }
                }

                $table_content .= "<tr class='tabletop'>";
                $table_content .= "<td class='tablebluetop' width='10%'>" . $Lang['RepairSystem']['Settings']['Location'] . "</td>";
                $table_content .= "<td width='11%'>" . $Lang['RepairSystem']['Settings']['Room'] . "</td>";
                $table_content .= "<td width='1%'>&nbsp;</td>";
                $table_content .= "</tr>";

                if (sizeof($arrLocationID) > 0) {
                    $table_content .= "<tr class='row_approved record_Waived'>";

                    for ($i = 0; $i < sizeof($arrLocationID); $i ++) {
                        $location_id = $arrLocationID[$i];
                        $location_name = $arrLocationName[$location_id];
                        $table_content .= "<td valign='top' class='tablebluerow2'>$location_name</td>";
                        $table_content .= "<td colspan='2'>";
                        $table_content .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";

                        for ($j = $start - 1; $j < $end; $j ++) {
                            $sub_location_id = $arrSubLocationID[$location_id][$j];
                            if ($sub_location_id != "") {
                                // # get location's Desc and IsShow setting - show/hide in repair system when creating request
                                $sql = "SELECT Description, IsShow FROM INVENTORY_LOCATION WHERE LocationID = '$sub_location_id'";
                                $arrStatus = $this->returnArray($sql);

                                if (sizeof($arrStatus) > 0) {
                                    list ($description, $isShow) = $arrStatus[0];

                                    if ($isShow == 1) {
                                        $iconIsShow = "<img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_tick_green.gif'>";
                                        $row_css = "row_approved";
                                    } else {
                                        $iconIsShow = "<img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_norecord_bw.gif'>";
                                        $row_css = "row_approved record_Waived";
                                    }
                                }

                                $table_content .= "<tr>";
                                $table_content .= "<td width='10%' class='$row_css'>
												" . $arrSubLocationName[$location_id][$sub_location_id] . "&nbsp;";
                                if ($description != "") {
                                    $desc_link_div_id = "ShowDescription_" . $sub_location_id;
                                    $action = "ShowLocationDescription";
                                    $table_content .= "<a id='$desc_link_div_id' href='javascript: js_Show_Detail_Layer(\"$action\",\"$sub_location_id\",\"$desc_link_div_id\")'><img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_remark.gif' border='0' align='absmiddle' valign='2'></a>";
                                }

                                $table_content .= $iconIsShow;
                                $table_content .= " </td>";
                                $table_content .= "</tr>";
                            }
                        }
                        $table_content .= "</table>";
                        $table_content .= "</td>";
                    }
                    $table_content .= "</tr>";
                } else {
                    $table_content .= "<tr><td align='center' colspan='2'>" . $Lang['General']['NoRecordAtThisMoment'] . "</td></tr>";
                }
                // ## Table Navigation Bar ###
                $li->page_size = $numPerPage;
                $li->total_row = sizeof($arrResult);
                $li->form_name = "form1";

                $table_content .= "<tr class='tablebottom' height='20px'><td colspan='3'>";
                if (sizeof($arrResult) > 0) {
                    $table_content .= $li->navigation_ebooking();   // copied form ebooking, the name doesn't matter
                }
                $table_content .= "</td></tr>";
                $table_content .= "</tbody>";
                $table_content .= "</table>";
            } else {
                $table_content .= "<tr><td align='center' colspan='2'>";
                $table_content .= "<fieldset class='instruction_box'>";
                $table_content .= "<legend class='instruction_title'>" . $Lang['RepairSystem']['Settings']['Instruction'] . "</legend>";
                $table_content .= "<span>" . $Lang['RepairSystem']['Settings']['PleaseSelectLocation'] . "</span>";
                $table_content .= "</fieldset>";
                $table_content .= "</td></tr>";
                $table_content .= "</tbody>";
                $table_content .= "</table>";
            }

            $table_content .= "<table border='0'>";
            $table_content .= "<tr>";
            $table_content .= "<input type='hidden' id='pageNo_RepairSystem_Settings_Room_View' name='pageNo_RepairSystem_Settings_Room_View' value='" . $li->pageNo . "'>";
            $table_content .= "<input type='hidden' id='numPerPage' name='numPerPage' value='" . $li->page_size . "'>";
            $table_content .= "</table>";

            return $table_content;
        }

        function getRoomPropertyEditTable($targetBuilding, $targetFloor, $targetRoom, $pageNo = '', $numPerPage = '', $order = '', $SortField = '')
        {
            global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
            include_once ("libinterface.php");
            include_once ("libdbtable.php");
            include_once ("libdbtable2007a.php");

            if ($pageNo == '') {
                $pageNo = $_COOKIE['pageNo_RepairSystem_Settings_Room_View'];
            }
            if ($numPerPage == '') {
                $numPerPage = $_COOKIE['numPerPage_RepairSystem_Settings_Room_View'];
            }

            $linterface = new interface_html();
            $li = new libdbtable2007($SortField, $order, $pageNo);
            $displayedSubLocationIdAry = array();

            // Default Table Settings
            $pageNo = ($pageNo == '') ? $li->pageNo = 1 : $li->pageNo = $pageNo;
            $numPerPage = ($numPerPage == '') ? $li->page_size = 20 : $li->page_size = $numPerPage;
            $order = ($order == '') ? 1 : $order;
            $SortField = ($SortField == '') ? 0 : $SortField;

            $ModeArr = '';
            $ModeArr[] = array(
                $Lang['Btn']['View'],
                "icon_view.gif",
                "javascript:Go_View_Mode();",
                0
            );
            $ModeArr[] = array(
                $Lang['Btn']['Edit'],
                "icon_edit_b.gif",
                "",
                1
            );
            $mode_toolbar = $this->Get_Mode_Toolbar($ModeArr);

            $table_content .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
            $table_content .= "<tr>";
            $table_content .= "<td align='right'>" . $mode_toolbar . "</td>";
            $table_content .= "</tr>";
            $table_content .= "</table>";

            if (($targetBuilding != "") && ($targetFloor != "")) {
                if ($targetBuilding != "")
                    $cond .= " AND building.BuildingID = '$targetBuilding' ";
                if ($targetFloor != "")
                    $cond .= " AND floor.LocationLevelID = '$targetFloor' ";
                if ($targetRoom != "")
                    $cond .= " AND room.LocationID = '$targetRoom' ";

                $sql = "SELECT 
						floor.LocationLevelID,
						room.LocationID,
						" . $this->getInventoryNameByLang("floor.") . ",
						" . $this->getInventoryNameByLang("room.") . "
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
					WHERE
						building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
						$cond
					ORDER BY building.DisplayOrder, floor.DisplayOrder, room.DisplayOrder";
                $arrResult = $this->returnArray($sql);

                if ($pageNo == 1) {
                    $start = $pageNo;
                    $end = $numPerPage;
                    $li->n_start = $start - 1;
                    $li->n_end = min(sizeof($arrResult), ($li->pageNo * $li->page_size));
                } else {
                    $start = ($pageNo * $numPerPage) - $numPerPage + 1;
                    $end = ($pageNo * $numPerPage);
                    $li->n_start = $start - 1;
                    $li->n_end = min(sizeof($arrResult), ($li->pageNo * $li->page_size));
                }

                if (sizeof($arrResult) > 0) {
                    $arrLocationID = array();
                    $arrLocationName = array();
                    for ($i = 0; $i < sizeof($arrResult); $i ++) {
                        list ($location_id, $sub_location_id, $location_name, $sub_location_name) = $arrResult[$i];
                        if (! in_array($location_id, $arrLocationID)) {
                            $arrLocationID[] = $location_id;
                            $arrLocationName[$location_id] = $location_name;
                        }
                        $arrSubLocationID[$location_id][] = $sub_location_id;
                        $arrSubLocationName[$location_id][$sub_location_id] = $sub_location_name;
                    }
                }

                $table_content .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                $table_content .= "<tr>";
                $table_content .= "<td class='tabletop tablebluetop' width='10%'>" . $Lang['RepairSystem']['Settings']['Location'] . "</td>";
                $table_content .= "<td>";
                $table_content .= "<table border='0' cellpadding='3' cellspacing='0' width='100%'>";
                $table_content .= "<tr class='tabletop'>";
                $table_content .= "<td colspan='4'>" . $Lang['RepairSystem']['Settings']['Room'] . "</td>";
                $table_content .= "</tr>";
                $table_content .= "<tr class='tablebottom'>";
                $table_content .= "<td width='20%' class='tablebottom'>&nbsp;</td>";

                // # batch update - IsShowRoom for repair system
                $thisAllIsShowRoom = "AllIsShowRoom";
                $table_content .= "<td width='7%'><div id='" . $thisAllIsShowRoom . "' class='selectbox_group selectbox_group_filter' style='float:right;'><a href='javascript: js_Show_Detail_Layer(\"ShowAllIsShowRoom\",0,\"$thisAllIsShowRoom\")'><img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_tick_green.gif' border='0'></a></div></td>";
                $table_content .= "<td width='60%'>&nbsp;</td>";

                // # check all
                $table_content .= "<td width='3%'><input type='checkbox' id='checkAll' onclick='Check_All_Options_By_Class(\"locationChk\", this.checked);'></td>";

                $table_content .= "</tr>";
                $table_content .= "</table>";
                $table_content .= "</td>";
                $table_content .= "</tr>";

                if (sizeof($arrLocationID) > 0) {
                    $table_content .= "<tr class='row_approved record_Waived'>";
                    for ($i = 0; $i < sizeof($arrLocationID); $i ++) {
                        $location_id = $arrLocationID[$i];
                        $location_name = $arrLocationName[$location_id];

                        $table_content .= "<td valign='top' class='tablebluerow2'>$location_name</td>";
                        $table_content .= "<td colspan='4'>";
                        $table_content .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";

                        for ($j = $start - 1; $j < $end; $j ++) {
                            $sub_location_id = $arrSubLocationID[$location_id][$j];

                            if ($sub_location_id != "") {
                                $displayedSubLocationIdAry[] = $sub_location_id;

                                // # get location's Desc and IsShow setting - show/hide in repair system when creating request
                                $sql = "SELECT Description, IsShow FROM INVENTORY_LOCATION WHERE LocationID = '$sub_location_id'";
                                $arrStatus = $this->returnArray($sql);
                                if (sizeof($arrStatus) > 0) {
                                    list ($description, $isShow) = $arrStatus[0];
                                    if ($isShow == 1) {
                                        $iconIsShow = "<img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_tick_green.gif' border='0'>";
                                        $row_css = "row_approved";
                                    } else {
                                        $iconIsShow = "<img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_norecord_bw.gif' border='0'>";
                                        $row_css = "row_approved record_Waived";
                                    }
                                }

                                $table_content .= "<tr>";
                                $table_content .= "<td width='20%' class='$row_css'>" . $arrSubLocationName[$location_id][$sub_location_id];
                                if ($description != "") {
                                    $desc_link_div_id = "ShowDescription_" . $sub_location_id;
                                    $action = "ShowLocationDescription";
                                    $table_content .= "<a id='$desc_link_div_id' href='javascript: js_Show_Detail_Layer(\"$action\",\"$sub_location_id\",\"$desc_link_div_id\")'><img src='" . $PATH_WRT_ROOT . $image_path . "/" . $LAYOUT_SKIN . "/icon_remark.gif' border='0' align='absmiddle' valign='2'></a>";
                                }

                                $table_content .= "</td>";


                                // # Div - Sub Location's IsShow for repair system
                                $thisIsShowLayerID = "IsShow_" . $sub_location_id;
                                $table_content .= "<td width='10%' class='$row_css'><div id='" . $thisIsShowLayerID . "' class='selectbox_group selectbox_group_filter' style='float:right'><a href='javascript: js_Show_Detail_Layer(\"ShowIsShowRoom\",$sub_location_id,\"$thisIsShowLayerID\")'>" . $iconIsShow . "</a></div></td>";
                                $table_content .= "<td width='68%' class='$row_css'></td>";

                                // # checkbox
                                $table_content .= "<td width='2%' class='$row_css'>". $linterface->Get_Checkbox('locationChk_'.$sub_location_id, 'locationChk_'.$sub_location_id, $Value=1, $isChecked=0, $Class="locationChk", $Display="", $Onclick="Uncheck_SelectAll('checkAll', this.checked);") . "</td>";

                                $table_content .= "</tr>";
                            }
                        }
                        $table_content .= "</table>";
                        $table_content .= "</td>";
                    }
                    $table_content .= "</tr>";
                } else {
                    $table_content .= "<tr><td align='center' colspan='4'>" . $Lang['General']['NoRecordAtThisMoment'] . "</td></tr>";
                }
                $table_content .= "<tr class='tablebottom'><td colspan='4'>";
                // ## Table Navigation Bar ###
                $li->page_size = $numPerPage;
                $li->total_row = sizeof($arrResult);
                $li->form_name = "form1";
                $li->numPerPage_name = 'numPerPage_RepairSystem_Settings_Room_View';
                $li->pageNo_name = 'pageNo_RepairSystem_Settings_Room_View';

                $table_content .= "<tr class='tablebottom' height='20px'><td colspan='3'>";
                if (sizeof($arrResult) > 0)
                    $table_content .= $li->navigation_ebooking();
                $table_content .= "</td></tr>";
                $table_content .= "</table>";
            }
            $table_content .= "<table border='0'>";
            $table_content .= "<tr>";
            $table_content .= "<input type='hidden' id='pageNo_RepairSystem_Settings_Room_View' name='pageNo_RepairSystem_Settings_Room_View' value='" . $li->pageNo . "'>";
            $table_content .= "<input type='hidden' id='numPerPage' name='numPerPage' value='" . $li->page_size . "'>";
            $table_content .= "<input type='hidden' id='displayedSubLocationIdList' name='displayedSubLocationIdList' value='" . implode(',', (array) $displayedSubLocationIdAry) . "'>";
            $table_content .= "</tr>";
            $table_content .= "</table>";

            return $table_content;
        }

        function Get_All_Building_Array($IsShow = "")
        {
            if ($IsShow != "") {
                $Cond_IsShow = " AND room.IsShow = '$IsShow' ";
            }

            $sql = "
					SELECT DISTINCT 
						building.BuildingID, 
						" . $this->getInventoryNameByLang("building.") . " 
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID) 
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID) 
					WHERE 
						building.RecordStatus = 1 
						AND floor.RecordStatus = 1 
						AND room.RecordStatus = 1 
						$Cond_IsShow
					ORDER BY
						building.DisplayOrder ASC, floor.DisplayOrder ASC, room.DisplayOrder ASC
				";
            $arrBuilding = $this->returnArray($sql);

            return $arrBuilding;
        }

        function Get_All_Floor_Array($targetBuilding, $IsShow = "")
        {
            if ($IsShow != "") {
                $Cond_IsShow = " AND room.IsShow = '$IsShow' ";
            }

            $sql = "
					SELECT DISTINCT 
						floor.LocationLevelID, " . $this->getInventoryNameByLang("floor.") . " 
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID) 
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID) 
					WHERE 
						building.RecordStatus = 1 
						AND floor.RecordStatus = 1 
						AND room.RecordStatus = 1  
						and building.BuildingID = '$targetBuilding' 
						$Cond_IsShow
					ORDER BY
						building.DisplayOrder ASC, floor.DisplayOrder ASC, room.DisplayOrder ASC
				";
            $arrFloor = $this->returnArray($sql);

            return $arrFloor;
        }

        function Get_All_Room_Array($targetBuilding, $targetFloor, $IsShow = "")
        {
            if ($IsShow != "") {
                $Cond_IsShow = " AND room.IsShow = '$IsShow' ";
            }

            $sql = "
					SELECT DISTINCT 
						room.LocationID, 
						" . $this->getInventoryNameByLang("room.") . " 
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID) 
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID) 
					WHERE 
						building.RecordStatus = 1 
						AND floor.RecordStatus = 1 
						AND room.RecordStatus = 1 
						AND building.BuildingID = '$targetBuilding' 
						AND floor.LocationLevelID = '$targetFloor' 
						$Cond_IsShow 
					ORDER BY
						building.DisplayOrder ASC, floor.DisplayOrder ASC, room.DisplayOrder ASC
				";
            $arrRoom = $this->returnArray($sql);

            return $arrRoom;
        }

        function initJavaScript()
        {
            global $PATH_WRT_ROOT, $LAYOUT_SKIN;

            $x = '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/' . $LAYOUT_SKIN . '/js/script.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.colorPicker.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.tablednd_0_5.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.scrollTo-min.js"></script>
				<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.cookies.2.2.0.js"></script>
				<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.colorPicker.css" type="text/css" />
				<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.css" type="text/css" />
				<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />
				<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.css" type="text/css" />';

            return $x;
        }

        function getManagementGroupSelection($GroupID="", $tag="", $showAll=0, $firstOption="", $noFirst=0, $select_name="GroupID", $NeedCheckMgmtGroup=0)
        {
            $select_name=$select_name ? $select_name : "GroupID";
            if($NeedCheckMgmtGroup && !$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'])
            {
                $sql = "
					SELECT 
						g.GroupID, g.GroupTitle 
					FROM 
						REPAIR_SYSTEM_GROUP as g
						INNER JOIN REPAIR_SYSTEM_GROUP_MEMBER as m ON m.GroupID=g.GroupID
					WHERE 
						g.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." 
						AND m.UserID='".$_SESSION['UserID'] . " ORDER BY g.GroupTitle";
            }
            else
            {
                $sql = "SELECT GroupID, GroupTitle FROM REPAIR_SYSTEM_GROUP WHERE RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED." ORDER BY GroupTitle";
            }
            $result = $this->returnArray($sql);

            return getSelectByArray($result, "name='". $select_name ."' id='GroupID' $tag", $GroupID, $showAll, $noFirst, $firstOption);
        }

        function isUsePlupload()
        {
            global $userBrowser;
            $is_below_IE8 = ($userBrowser->browsertype == "MSIE" && intval($userBrowser->version) < 8);
            $is_older_firefox = ($userBrowser->browsertype == "Firefox" && intval($userBrowser->version) < 4);
            $is_mobile_tablet_platform = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod");
            $use_plupload = !$is_mobile_tablet_platform && !$is_below_IE8 && !$is_older_firefox;
            return $use_plupload;
        }

        function updateTempPluploadInfo()
        {
            global $intranet_root;
            global $pluploadButtonId, $pluploadDropTargetId, $pluploadFileListDivId, $pluploadContainerId, $tempFolderPath;
            include_once($intranet_root."/includes/libfilesystem.php");

            $lfs = new libfilesystem();

            // set for output
            $pluploadButtonId = 'UploadButton';
            $pluploadDropTargetId = 'DropFileArea';
            $pluploadFileListDivId = 'FileListDiv';
            $pluploadContainerId = 'pluploadDiv';
            $tempFolderPath = '/u'.$_SESSION['UserID'].'_'.time();

            // Clean old temp folders
            $sql = "SELECT * FROM REPAIR_SYSTEM_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND TIMESTAMPDIFF(SECOND,InputDate,NOW()) > 86400";
            $tempFolderAry = $this->returnResultSet($sql);
            $folderCount = count($tempFolderAry);

            $folderIdToRemove = array();
            for($i=0;$i<$folderCount;$i++) {
                $tempFolder = $tempFolderAry[$i]['Folder'];

                $tempDeleteFolderPath = str_replace('//', '/', $PATH_WRT_ROOT."file/repair/temp/".$tempFolder);
                if(file_exists($tempDeleteFolderPath)) {
                    $lfs->deleteDirectory($tempDeleteFolderPath);
                }
                $folderIdToRemove[] = $tempFolderAry[$i]['FolderID'];
            }
            if(count($folderIdToRemove) > 0){
                $sql = "DELETE FROM REPAIR_SYSTEM_TEMP_UPLOAD_FOLDER WHERE UserID='".$_SESSION['UserID']."' AND FolderID IN (".implode(",",$folderIdToRemove).")";
                $this->db_db_query($sql);
            }

            // Store the new temp folder
            $sql = "INSERT INTO REPAIR_SYSTEM_TEMP_UPLOAD_FOLDER (UserID,Folder,InputDate) VALUES ('".$_SESSION['UserID']."','".$tempFolderPath."',NOW())";
            $this->db_db_query($sql);

        }

        function getFollowupPhotos($repairID)
        {
            $sql = "SELECT
							PhotoID,
							FileName,
							FileHashName,
							SizeInBytes
					FROM
							REPAIR_SYSTEM_PHOTOS
					WHERE
							RecordID='$repairID'
							AND RecordStatus<>4
					ORDER BY PhotoID";

            $result = $this->returnResultSet($sql);
            return $result;
        }

        // $type: 1 - request, 2 - follow-up
        function getPhotoListUI($recordID, $type=1)
        {
            if ($type == 1) {
                $photos = $this->getPhotosByRepairID($recordID);
            }
            else {
                $photos = $this->getFollowupPhotos($recordID);
            }
            $nrPhotos = count($photos);
            $col = 4;	// number of columns to show
            $nrPhotoRows = ceil($nrPhotos / $col);
            $remainPhotos = $nrPhotos % $col;

            $x = '';
            ob_start();
            if ($nrPhotos):
                ?>
                <tr>
                    <td colspan="2">
                        <table class="repair_photo_table" border="0">
                            <? for($i=0;$i<$nrPhotoRows;$i++):?>
                                <tr>
                                    <? $nrPhotoCols = ($i == $nrPhotoRows-1) ? ($remainPhotos ? $remainPhotos : $col) : $col;?>
                                    <? for ($j=0;$j<$nrPhotoCols;$j++):?>
                                        <td class="repair_photo">
                                            <a href="#TB_inline?height=500&width=800&inlineId=FakeLayer" onclick="view_photo('<?=$photos[$i*$col+$j]['PhotoID']?>'); return false;" title="<?=$photos[$i*$col+$j]['FileName']?>">
                                                <img src="/file/repair/final/<?=$photos[$i*$col+$j]['FileHashName']?>"><br><div class="name"><?=$photos[$i*$col+$j]['FileName']?></div>
                                            </a>
                                        </td>
                                    <? endfor;?>
                                    <? if (($nrPhotoCols) < $col):?>
                                        <? for ($j=0;$j<$col-$nrPhotoCols;$j++):?>
                                            <td class="repair_photo">&nbsp;</td>
                                        <? endfor;?>
                                    <? endif;?>
                                </tr>
                            <? endfor;?>
                        </table>
                    </td>
                </tr>
            <?
            endif;

            $x = ob_get_contents();

            ob_end_clean();
            return $x;
        }

        // three columns: follow-up person to be selected, buttons, selected follow-up person
        function getFollowupPersonSelection($groupMember=array(), $selectedPerson=array())
        {
            global $Lang, $linterface;

            if (count($selectedPerson) == 0 ) {
                $disable = 'disabled="disabled"';
//                $isDisabled = 1;
                $isDisabled = 0;
            }
            else {
                $disable = '';
                $isDisabled = 0;
            }

            $x = '
				<table class="no_bottom_border">
					<tr>';

                $x .= '<td>';
                    $x .= '<span id="PersonSelectionSpan"><select name=AvailableSysPersonID[] id=AvailableSysPersonID '.$disable.' style="min-width:200px; height:156px;" multiple>';

                    for ($i=0, $iMax=count($groupMember); $i<$iMax; $i++) {
                        $userID = $groupMember[$i]['UserID'];
                        $userName = $groupMember[$i]['Name'];
                        $x .= '<option value="' . $userID . '">' . $userName . '</option>';
                    }

                    $x .= '</select></span>';
                $x .= '</td>';

                $x .= '<td align=center>';
                    $x .= $linterface->GET_BTN(">> " . $Lang['Btn']['Add'], "button", "checkOptionTransferUnique(this.form.elements['AvailableSysPersonID[]'],this.form.elements['SysPersonID[]']);return false;", "btnSelect", "", $isDisabled) . "<br /><br />";
                    $x .= $linterface->GET_BTN($Lang['Btn']['Delete'] . " <<", "button", "checkOptionTransferUnique(this.form.elements['SysPersonID[]'],this.form.elements['AvailableSysPersonID[]']);return false;", "btnUnSelect", "", $isDisabled);
                $x .= '</td>';

                $x .= '<td>
					    <select name=SysPersonID[] id=SysPersonID '.$disable.' style="min-width:200px; height:156px;" multiple>';
                for ($i=0, $iMax=count($selectedPerson); $i<$iMax; $i++) {
                    $personID = $selectedPerson[$i]['UserID'];
                    $personName = $selectedPerson[$i]['Name'];
                    $x .= '<option value="' . $personID . '">' . $personName . '</option>';
                }
                    $x .= '</select>';
                $x .= '</td>';

            $x .= '	</tr>										
				</table>';
            return $x;
        }

        // return true if user is in one of following roles:
        // RepairSystem eAdmin, category access group member, follow-up group member, follow-up person
        function checkRecordAccessRight($recordID)
        {
            if ($_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem']) {
                return true;
            }

            // check if user belongs to category access group member
            $sql = "SELECT 
                        r.RecordID 
                    FROM 
                        REPAIR_SYSTEM_RECORDS r 
                        INNER JOIN REPAIR_SYSTEM_CATEGORY c ON c.CategoryID=r.CategoryID 
                        INNER JOIN REPAIR_SYSTEM_GROUP_MEMBER m ON m.GroupID=c.GroupID 
                    WHERE 
                        m.UserID='".$_SESSION['UserID']."' 
                        AND r.RecordID='".$recordID."'";
            $result = $this->returnResultSet($sql);
            if (count($result)) {
                return true;
            }

            // check if user belongs to last follow-up group member
            $sql = "SELECT 
                        r.RecordID 
                    FROM 
                        REPAIR_SYSTEM_RECORDS r 
                        INNER JOIN REPAIR_SYSTEM_GROUP_MEMBER m ON m.GroupID=r.LastFollowupGroupID 
                    WHERE 
                        m.UserID='".$_SESSION['UserID']."' 
                        AND r.RecordID='".$recordID."'";
            $result = $this->returnResultSet($sql);
            if (count($result)) {
                return true;
            }

            // check if user is in follow-up person list
            $sql = "SELECT PersonID FROM REPAIR_SYSTEM_FOLLOWUP_PERSON WHERE RecordID='".$recordID."' AND PersonID='".$_SESSION['UserID']."'";
            $result = $this->returnResultSet($sql);
            if (count($result)) {
                return true;
            }

            return false;
        }

        function getFollowupPersonID($recordID, $personID='')
        {
            $name = getNameFieldByLang2('u.');
            $sql = "SELECT 
                        u.UserID,
                        {$name} as Name 
                    FROM 
                        REPAIR_SYSTEM_FOLLOWUP_PERSON p
                        INNER JOIN INTRANET_USER u ON u.UserID=p.PersonID 
                    WHERE 
                        p.RecordID='".$recordID."'";
            if ($personID != '') {
                $sql .= " AND p.PersonID='".$personID."'";
            }
            $result = $this->returnResultSet($sql);

            return $result;
        }

        function getGroupMembers($groupID, $recordID=0)
        {
            if ($recordID) {
                $personAry = $this->getFollowupPersonID($recordID);
                $personIDAry = Get_Array_By_Key($personAry, 'UserID');
                $condition = "AND u.UserID NOT IN ('".implode("','", $personIDAry)."')";
            }
            else {
                $condition = "";
            }

            $name = getNameFieldByLang2('u.');
            $sql = "SELECT 
                        u.UserID,
                        {$name} as Name 
                    FROM 
                        REPAIR_SYSTEM_GROUP g
                        INNER JOIN REPAIR_SYSTEM_GROUP_MEMBER m ON m.GroupID=g.GroupID
                        INNER JOIN INTRANET_USER u ON u.UserID=m.UserID 
                    WHERE 
                        g.GroupID='".$groupID."' {$condition}
                        ORDER BY Name";

            $result = $this->returnResultSet($sql);

            return $result;
        }

        function insertFollowupPersonID($recordID, $personID)
        {
            $sql = "INSERT INTO REPAIR_SYSTEM_FOLLOWUP_PERSON (RecordID, PersonID) VALUES ('$recordID', '$personID')";
            return $this->db_db_query($sql);
        }

        function deleteFollowUpPersonID($recordID)
        {
            $sql = "DELETE FROM REPAIR_SYSTEM_FOLLOWUP_PERSON WHERE RecordID='$recordID'";
            return $this->db_db_query($sql);
        }

        function getMyFollowupRecord($myID)
        {
            $sql = "SELECT DISTINCT RecordID FROM REPAIR_SYSTEM_FOLLOWUP_PERSON WHERE PersonID='".$myID."' ORDER BY RecordID";
            $result = $this->returnResultSet($sql);
            return Get_Array_By_Key($result, 'RecordID');
        }
    }
}        // End of directive
?>