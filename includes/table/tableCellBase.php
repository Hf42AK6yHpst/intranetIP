<?php
if (!defined("TABLECELLBASE_DEFINED")) {

	class tableCellBase {
		var $tagName;
		var $content;
		var $width;
		var $colspan;
		var $rowspan;
		var $applyConvertSpecialChars;
		
		function tableCellBase() {
			$this->setApplyConvertSpecialChars(true);
		}
		
		function setTagName($val) {
			$this->tagName = $val;
		}
		function getTagName() {
			return $this->tagName;
		}
		
		function setContent($val) {
			$this->content = $val;
		}
		function getContent() {
			return $this->content;
		}
		
		function setWidth($val) {
			$this->width = $val;
		}
		function getWidth() {
			return $this->width;
		}
		
		function setColspan($val) {
			$this->colspan = $val;
		}
		function getColspan() {
			return $this->colspan;
		}
		
		function setRowspan($val) {
			$this->rowspan = $val;
		}
		function getRowspan() {
			return $this->rowspan;
		}
		
		function setApplyConvertSpecialChars($val) {
			$this->applyConvertSpecialChars = $val;
		}
		function getApplyConvertSpecialChars() {
			return $this->applyConvertSpecialChars;
		}
		
		function returnTagAttribute() {
			$attrAry = array();
			$attrAry['width'] = $this->getWidth();
			$attrAry['colspan'] = $this->getColspan();
			$attrAry['rowspan'] = $this->getRowspan();
			
			$attrTextAry = array();
			foreach ($attrAry as $_attrKey => $_value) {
				if ($_value == '') {
					continue;
				}
				
				$attrTextAry[] = $_attrKey.'="'.$_value.'"';
			}
			
			return implode(' ', $attrTextAry);
		}
		
		function returnHtml() {
			global $Lang;
			
			$tagName = $this->getTagName();
			$content = $this->getContent();
			$content = ($content=='')? $Lang['General']['EmptySymbol'] : $content;
			$content = ($this->getApplyConvertSpecialChars())? intranet_htmlspecialchars($content) : $content;
			
			$attribute = $this->returnTagAttribute();
			
			$x = '';
			$x .= '<'.$tagName.' '.$attribute.'>'."\n";
				$x .= $content."\n";
			$x .= '</'.$tagName.'>'."\n";
			
			return $x;
		}
	}
}
?>