<?php
# modifying : ivan
if (!defined("LIBTABLE_LAYOUT"))         // Preprocessor directives
{
	define("LIBTABLE_LAYOUT", true);

	class libtable_layout {
		private $resultSet;
		private $columnInfoAry;
		private $showResultNumber;
		private $resultNumberColumnExtraAttr;
		private $headerTrClassAry;
		private $dataTrClassAry;
		
		private $curPage;
		private $numPerPage;
		private $sortColumnIndex;
		private $sortOrder;
		
		private $submitMode;
		private $submitFormID;
		private $submitHref;
		private $submitFunctionName;
		private $submitExtraPara;
		
		public function __construct() {
			$this->resetColumnInfoAry();
    	}
    	
    	
    	#########################################################
    	############## [Start] Get / Set Functions ##############
    	#########################################################
    	public function setResultSet($ary) {
    		$this->resultSet = $ary;
    	}
    	private function getResultSet() {
    		return $this->resultSet;
    	}
    	
    	public function setSortColumnIndex($int) {
    		$this->sortColumnIndex = $int;
    	}
    	private function getSortColumnIndex() {
    		return $this->sortColumnIndex;
    	}
    	
    	public function setSortOrder($int) {
    		$this->sortOrder = $int;
    	}
    	private function getSortOrder() {
    		return $this->sortOrder;
    	}
    	
    	public function setShowResultNumber($bool) {
    		$this->showResultNumber = $bool;
    	}
    	private function getShowResultNumber() {
    		return $this->showResultNumber;
    	}
    	
    	public function setResultNumberColumnExtraAttr($str) {
    		$this->resultNumberColumnExtraAttr = $str;
    	}
    	private function getResultNumberColumnExtraAttr() {
    		return $this->resultNumberColumnExtraAttr;
    	}
    	
    	public function setColumnInfoAry($ary) {
    		$this->columnInfoAry = $ary;
    	}
    	private function getColumnInfoAry() {
    		return $this->columnInfoAry;
    	}
    	
    	public function setHeaderTrClassAry($ary) {
    		$this->headerTrClassAry = $ary;
    	}
    	private function getHeaderTrClassAry() {
    		return $this->headerTrClassAry;
    	}
    	
    	public function setDataTrClassAry($ary) {
    		$this->dataTrClassAry = $ary;
    	}
    	private function getDataTrClassAry() {
    		return $this->dataTrClassAry;
    	}
    	
    	public function setCurPage($int) {
    		$this->curPage = $int;
    	}
    	private function getCurPage() {
    		return $this->curPage;
    	}
    	
    	public function setNumPerPage($int) {
    		$this->numPerPage = $int;
    	}
    	private function getNumPerPage() {
    		return $this->numPerPage;
    	}
    	
    	public function setSubmitMode($str) {
    		$this->submitMode = $str;
    	}
    	private function getSubmitMode() {
    		return $this->submitMode;
    	}
    	
    	public function setSubmitFormID($str) {
    		$this->submitFormID = $str;
    	}
    	private function getSubmitFormID() {
    		return $this->submitFormID;
    	}
    	
    	public function setSubmitHref($str) {
    		$this->submitHref = $str;
    	}
    	private function getSubmitHref() {
    		return $this->submitHref;
    	}
    	
    	public function setSubmitFunctionName($str) {
    		$this->submitFunctionName = $str;
    	}
    	private function getSubmitFunctionName() {
    		return $this->submitFunctionName;
    	}
    	
    	public function setSubmitExtraPara($str) {
    		$this->submitExtraPara = $str;
    	}
    	private function getSubmitExtraPara() {
    		return $this->submitExtraPara;
    	}
    	#########################################################
    	############### [End] Get / Set Functions ###############
    	#########################################################
    	
    	private function resetColumnInfoAry() {
    		$this->columnInfoAry = array();
    	}
    	
    	/**
		* Generate the table HTML to show the result set
		*
		* @return	String HTML of the table to display the result set
		*/  	
    	public function display() {
    		$showResultNumber = $this->getShowResultNumber();
    		
    		$html = '';
    		$html .= '<table class="common_table_list">'."\r\n";
				if ($showResultNumber) {
					$html .= '<colgroup>'."\r\n";
						$html .= '<col nowrap="nowrap">'."\r\n";
					$html .= '</colgroup>'."\r\n";
				}
	    		
	    		$html .= $this->generateTableHeaderHtml();
	    		$html .= $this->generateTableBodyHtml();
	    		
    		$html .= '</table>'."\r\n";
    		
    		return $html;
    	}
    	
    	/**
		* Generate the table header HTML
		*
		* @return	String HTML of the table header
		*/  
    	private function generateTableHeaderHtml() {
    		$sortColumnIndex = $this->getSortColumnIndex();
    		$sortOrder = $this->getSortOrder();
    		
    		$showResultNumber = $this->getShowResultNumber();
    		$headerTrClassAry = $this->getHeaderTrClassAry();
    		$columnInfoAry = $this->getColumnInfoAry();
    		$numOfColumn = count($columnInfoAry);
    		$curRowLevel = 0;
    		
    		$html = '';
    		$html .= '<thead>'."\r\n";
    			for ($i=0; $i<$numOfColumn; $i++) {
    				$_uiTitle = $columnInfoAry[$i]['uiTitle'];
    				$_isSortable = $columnInfoAry[$i]['isSortable'];
    				$_width = $columnInfoAry[$i]['width'];
    				$_headerClass = $columnInfoAry[$i]['headerClass'];
    				$_rowspan = $columnInfoAry[$i]['rowspan'];
    				$_colspan = $columnInfoAry[$i]['colspan'];
    				$_rowLevel = $columnInfoAry[$i]['rowLevel'];
    				$_extraAttribute = $columnInfoAry[$i]['extraAttribute'];
    				
    				$_uiTitle = ($_uiTitle=='')? '&nbsp;' : $_uiTitle;
    				$_cellTag = ($_rowLevel==1)? 'th' : 'td';
    				
    				if ($_isSortable) {
    					$_isCurSortField = false;
    					$_sortClass = '';
    					if ($i == $sortColumnIndex) {
    						// current sorting column => show the sorting icon
    						$_isCurSortField = true;
	    					$_sortClass = ($sortOrder)? 'sort_dec' : 'sort_asc';
	    				}
	    				
	    				$_sortByTitleLang = intranet_htmlspecialchars($this->generateSortColumnLang($_uiTitle));
	    				$_href = $this->generateColumnHref($i, $_isCurSortField);
	    				
	    				$_uiTitle = '<a class="'.$_sortClass.'" title="'.$_sortByTitleLang.'" href="'.$_href.'" >'.$_uiTitle.'</a>';
    				}
    				
    				### Check Start new row
    				if ($_rowLevel > $curRowLevel) {
    					$_trCss = $headerTrClassAry[$_rowLevel-1];
    					$html .= '<tr class="'.$_trCss.'">'."\r\n";
    						
						### Check show record number
	    				if ($showResultNumber) {
	    					$_display = ($_rowLevel==1)? '#' : '&nbsp;';
	    					$_extraAttr = $this->getResultNumberColumnExtraAttr();
	    					
	    					$html .= '<'.$_cellTag.' class="num_check" '.$_extraAttr.'>'.$_display.'</'.$_cellTag.'>'."\r\n";
	    				}
    				}
    				
    				### Build Current Title th
    				$_classAttr = ($_headerClass=='')? '' : ' class="'.$_headerClass.'" ';
    				$_widthAttr = ($_width=='')? '' : ' width="'.$_width.'" ';
    				$_rowspanAttr = ($_rowspan=='')? '' : ' rowspan="'.$_rowspan.'" ';
    				$_colspanAttr = ($_colspan=='')? '' : ' colspan="'.$_colspan.'" ';
    				
    				$html .= '<'.$_cellTag.$_classAttr.$_widthAttr.$_rowspanAttr.$_colspanAttr.$_extraAttribute.'>'.$_uiTitle.'</'.$_cellTag.'>'."\r\n";
    				
    				### Check End current row
    				$_nextColumnRowLevel = $columnInfoAry[$i+1]['rowLevel'];
    				if ($i == $numOfColumn || $_rowLevel < $_nextColumnRowLevel) {
    					$html .= '</tr>'."\r\n";
    				}
    				
    				$curRowLevel = $_rowLevel;
    			}
    		$html .= '</thead>'."\r\n";
    		
    		return $html;
    	}
    	
    	/**
		* Generate the table body to show the result set
		*
		* @return	String HTML of the table body to display the result set
		*/  
    	private function generateTableBodyHtml() {
    		global $Lang;
    		
    		//$displayColumnInfoAry = $this->getColumnInfoAry();
    		$displayColumnInfoAry = $this->generateColumnDisplayWithOrder();
    		$numOfDisplayColumn = count($displayColumnInfoAry);
    		
    		$showResultNumber = $this->getShowResultNumber();
    		$resultSet = $this->getResultSet();
    		$numOfResult = count($resultSet);
    		
    		$html .= '<tbody>'."\r\n";
    		if ($numOfResult == 0) {
    			$html .= '<tr><td colspan="100%" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\r\n";
    		}
    		else {
    			$curPage = $this->getCurPage();
    			$numPerPage = $this->getNumPerPage();
    			$curRowIndex = (($curPage - 1) * $numPerPage) + 1;
    			
    			for ($i=0; $i<$numOfResult; $i++) {
    				$_trCustClass = $resultSet[$i]['trCustClass'];
    				if ($_trCustClass == '') {
    					$_trClass = $this->generateDataRowClass($curRowIndex);
    				}
    				else {
    					$_trClass = $_trCustClass;
    				}
    				
    				$html .= '<tr class="'.$_trClass.'">'."\r\n";
    					if ($showResultNumber) {
    						$html .= '<td>'.($curRowIndex++).'</td>'."\r\n";
    					}
    					
	    				for ($j=0; $j<$numOfDisplayColumn; $j++) {
	    					$_dbField = $displayColumnInfoAry[$j]['dbField'];
	    					$_dataClass = $displayColumnInfoAry[$j]['dataClass'];
	    					$_extraAttribute = $displayColumnInfoAry[$j]['extraAttribute'];
	    					
	    					if ($_dbField == '') {
	    						// colspan column without data
	    						continue;
	    					}
	    					
	    					$html .= '<td class="'.$_dataClass.'" '.$_extraAttribute.'>'.$resultSet[$i][$_dbField].'</td>'."\r\n";
	    				}
    				$html .= '</tr>'."\r\n";
    			}
    		}
    		$html .= '</tbody>'."\r\n";
    		
    		return $html;
    	}
    	
    	/**
		* Generate the table row class depends on the settings and row index
		*
		* @param	String $rowIndex, the "#" value of the table row
		* @return	String class of the row
		*/  
    	private function generateDataRowClass($rowIndex) {
    		$trClassAry = $this->getDataTrClassAry();
    		$numOfTrClass = count((array)$trClassAry);
    		
    		$classIndex = ($rowIndex - 1) % $numOfTrClass;
    		$class = $trClassAry[$classIndex];
    		
    		return $class;
    	}
    	
    	/**
		* Generate the Column Info Array which includes the display column only and is ordered according to the data display.
		* The colspan-ed columns without dbField will be ignored.
		*
		* @return	Array Column Info of the ordered display only column
		*/  
    	private function generateColumnDisplayWithOrder() {
    		$columnInfoAry = $this->getColumnInfoAry();
    		$numOfColumn = count($columnInfoAry);
    		
    		### Get the max row level
    		$maxRowLevel = 0;
    		for ($i=0; $i<$numOfColumn; $i++) {
    			$_rowLevel = $columnInfoAry[$i]['rowLevel'];
    			
    			if ($_rowLevel > $maxRowLevel) {
    				$maxRowLevel = $_rowLevel;
    			}
    		}
    		
    		### Get the Column array of the max level
    		$maxRowLevelColumnInfoAry = array();
    		for ($i=0; $i<$numOfColumn; $i++) {
    			$_rowLevel = $columnInfoAry[$i]['rowLevel'];
    			
    			if ($_rowLevel == $maxRowLevel) {
    				$maxRowLevelColumnInfoAry[] = $columnInfoAry[$i];
    			}
    		}
    		
    		    		
    		
    		### Build the Column Info Array with Order
    		$columnInfoWithOrderAry = array();
    		$maxRowColumnIndex = 0;
    		for ($i=0; $i<$numOfColumn; $i++) {
    			$_dbField = $columnInfoAry[$i]['dbField'];
    			$_rowLevel = $columnInfoAry[$i]['rowLevel'];
    			$_colspan = $columnInfoAry[$i]['colspan'];
    			
    			if ($_rowLevel > 1) {
    				// only loop topest level
    				continue;
    			}
    			
    			if ($_dbField == '') {
    				for ($j=0; $j<$_colspan; $j++) {
    					$columnInfoWithOrderAry[] = $maxRowLevelColumnInfoAry[$maxRowColumnIndex++];
    				}
    			}
    			else {
    				$columnInfoWithOrderAry[] = $columnInfoAry[$i];
    			}
    		}
    		
    		return $columnInfoWithOrderAry;
    	}
    	
    	private function generateSortColumnLang($columnTitle) {
    		global $Lang;
    		
    		return str_replace('<!--FieldName-->', $columnTitle, $Lang['General']['SortByField']);
    	}
    	
    	private function generateColumnHref($columnIndex, $isCurSortField) {
    		$curPage = $this->getCurPage();
    		$numPerPage = $this->getNumPerPage();
    		//$sortColumnIndex = $this->getSortColumnIndex();
    		$sortOrder = $this->getSortOrder();
    		$submitMode = $this->getSubmitMode();
    		$submitFormID = $this->getSubmitFormID();
    		$submitHref = $this->getSubmitHref();
    		$submitFunctionName = $this->getSubmitFunctionName();
    		$submitExtraPara = $this->getSubmitExtraPara();
    		
    		if ($isCurSortField) {
    			$targetSortOrder = ($sortOrder==1)? 0 : 1;
    		}
    		else {
    			$targetSortOrder = $sortOrder;
    		}
    		
    		$href = "javascript:reloadResultTable('$curPage', '$numPerPage', '$columnIndex', '$targetSortOrder', '$submitMode', '$submitFormID', '$submitHref', '$submitFunctionName', '$submitExtraPara');";	
    		return $href;
    	}
	}
}
?>