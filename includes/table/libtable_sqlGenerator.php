<?php
# modifying : ivan
if (!defined("LIBTABLE_SQL_GENERATOR"))         // Preprocessor directives
{
	define("LIBTABLE_SQL_GENERATOR", true);

	class libtable_sqlGenerator{
		private $objDb;
		private $sql;
		
		private $curPage;
		private $numPerPagePage;
		private $columnInfoAry;
		private $sortColumnIndex;
		private $sortOrder;
		private $extraSortInfoAry;
		private $applyLimit;
		
		public function __construct() {
			$this->objDb = new libdb();
			$this->resetColumnInfoAry();
    		$this->resetExtraSortInfoAry();
    	}
    	
    	#########################################################
    	############## [Start] Get / Set Functions ##############
    	#########################################################
    	public function setSql($str) {
    		$this->sql = $str;
    	}
    	private function getSql() {
    		return $this->sql;
    	}
    	
    	public function setCurPage($int) {
    		$this->curPage = $int;
    	}
    	private function getCurPage() {
    		return $this->curPage;
    	}
    	
    	public function setNumPerPage($int) {
    		$this->numPerPage = $int;
    	}
    	private function getNumPerPage() {
    		return $this->numPerPage;
    	}
    	
    	public function setSortColumnIndex($int) {
    		$this->sortColumnIndex = $int;
    	}
    	private function getSortColumnIndex() {
    		return $this->sortColumnIndex;
    	}
    	
    	public function setSortOrder($int) {
    		$this->sortOrder = $int;
    	}
    	private function getSortOrder() {
    		return $this->sortOrder;
    	}
    	
    	public function setApplyLimit($bool) {
    		$this->applyLimit = $bool;
    	}
    	private function getApplyLimit() {
    		return $this->applyLimit;
    	}
    	
    	public function setColumnInfoAry($ary) {
    		$this->columnInfoAry = $ary;
    	}
    	private function getColumnInfoAry() {
    		return $this->columnInfoAry;
    	}
    	
    	public function setExtraSortInfoAry($ary) {
    		$this->extraSortInfoAry = $ary;
    	}
    	private function getExtraSortInfoAry() {
    		return $this->extraSortInfoAry;
    	}
    	#########################################################
    	############### [End] Get / Set Functions ###############
    	#########################################################
    	
    	private function resetColumnInfoAry() {
    		$this->columnInfoAry = array();
    	}    	
    	private function resetExtraSortInfoAry() {
    		$this->extraSortInfoAry = array();
    	}
    	
    	public function generateSql() {
    		$sql = '';
    		$sql .= $this->sql;
    		
    		$sql .= $this->generateOrderBySql();
    		
    		
    		if ($this->getApplyLimit()) {
	    		$curPage = $this->getCurPage();
	    		$numPerPage = $this->getNumPerPage();
	    		$startIndex = (($curPage - 1) * $numPerPage);
    		
    			$sql .= " LIMIT $startIndex, $numPerPage ";
    		}
    		
    		return $sql;
    	}
    	
    	/**
		* Generate the Sorting Criteria of SQL
		*
		* @return	String of the Order By part of the SQL
		*/
    	private function generateOrderBySql() {
    		$columnInfoAry = $this->getColumnInfoAry();
    		$sortColumnIndex = $this->getSortColumnIndex();
    		$sortOrder = $this->getSortOrder();
    		$extraSortInfoAry = $this->getExtraSortInfoAry();
    		$numOfExtraSortInfo = count($extraSortInfoAry);
    		
    		$dbField = $columnInfoAry[$sortColumnIndex]['dbField'];
    		$sortDbField = $columnInfoAry[$sortColumnIndex]['sortDbField'];
    		$targetSortDbField = ($sortDbField=='')? $dbField : $sortDbField;

    		$sortOrderStr = ($sortOrder)? 'DESC' : 'ASC';
    		
    		$x = "";
    		$x .= " ORDER BY $targetSortDbField $sortOrderStr ";
    		
    		for ($i=0; $i<$numOfExtraSortInfo; $i++) {
    			$_sortDbField = $extraSortInfoAry[$i]['dbField'];
    			$_sortOrder = $extraSortInfoAry[$i]['sortOrder'];
    			$_sortOrderStr = ($sortOrder)? 'DESC' : 'ASC';
    			
    			$x .= ", $_sortDbField $_sortOrderStr ";
    		}
    		
    		return $x;
    	}
	}
}
?>