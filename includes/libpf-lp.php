<?php
/**
 * Change Log:
 * 2019-07-17 Anna [164961] 
 * - modified COPY_PORTFOLIO_FILES(), find School site no need move admin root path, remove first 
 * 
 * 2019-07-08 ANNA [#163902]
 * - modified COPY_PORTFOLIO_FILES(), remove admin root path before student photo path 
 * 2019-05-02 Pun
 *  - Prevent Command-injection
 *
 * 2018-01-23 Pun [134389] [ip.2.5.9.3.1]
 * - Modified GET_STUDENT_PORTFOLIO_PATH(), fix cannot load content if site is https
 *
 * 2017-03-28 Siuwan [ip.2.5.8.4.1][Case#X115139]
 * - replaced spilt by explode for PHP5.4
 *
 * 2014-03-03 Siuwan
 * - Modified function GEN_LEARNING_PORTFOLIO_LIST() URL parameter "ck_user_id" change to "user_id"
 *
 * 2012-03-27 Henry Chow [CRM : 2012-0302-1442-18071]
 * - Modified handleDir(), fixed the problem of broken link in HTML of offline SP (replace the space in folder name)
 *
 * 2012-03-09 Henry Chow [CRM : 2012-0302-1442-18071]
 * - Modified handleDir(), fixed the problem of broken link in HTML of offline SP (Burn CD)
 *
 * 2012-01-05 Ivan [CRM:2012-0104-1135-57066]
 * - Modified generateReportPrint() to fix SLP report cannot view academic result problem
 *
 * 2011-04-26 Ivan [CRM:2011-0413-1544-51067]
 * - Modified GET_LEARNING_PORTFOLIO_LIST() to get all the LP related to the students (as the edit period checking will be done in the pop-up windows)
 *
 * 2011-01-27 Ivan
 * - Modified GET_PEER_LEARNING_PORTFOLIO_LIST() to prevent duplicated student and portfolio shown if the student is in both Friend List and Review Type
 *
 * 2010-11-01 Max (201011010947)
 * - Modified getLPLinkHTML,getSchoolRecordLinkHTML,getSBSLinkHTML,getfrpLinkHTML,Get_Index_Html_ForBurningCD to pop up hyperlink in index page
 *
 * 2010-08-26 FAI (20100826)
 * - Modified GEN_LEARNING_PORTFOLIO_LIST() to display edit button for the specific class teacher only (ParUserClassID)
 *
 * 2010-05-07 Max (201005091353)
 * - Modified GEN_LP_COMMENT_LIST() to handle new line comment
 * **************************************************************
 */
include_once 'libportfolio.php';
include_once 'libportfolio2007a.php';

class libpf_lp extends libportfolio2007
{

	# Generate learning portfolio template list for display
	function GEN_LP_TEMPLATE_LIST($ParPageInfoArr, $ParField="", $ParOrder=""){
		global $image_path, $LAYOUT_SKIN, $no_record_msg, $i_general_status, $ec_iPortfolio, $i_general_title, $i_status_activated, $i_status_suspended, $i_status_usable;

		$lp_template_list = $this->GET_TEMPLATE_LIST($ParField, $ParOrder);
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$ReturnStr =	"
										<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">\n
											<tr class=\"tabletop\">
												<td width=\"15\" class=\"tabletoplink\">#</td>
												<td><a href=\"javascript:jSORT_TABLE('title', '".($ParField=="title"?($ParOrder=="ASC"?"DESC":"ASC"):"ASC")."')\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort0','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$i_general_title." ".($ParField=="title"?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort0\" id=\"sort0\" />":"")."</a></td>
												<td><a href=\"javascript:jSORT_TABLE('used_num', '".($ParField=="used_num"?($ParOrder=="ASC"?"DESC":"ASC"):"ASC")."')\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort1','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['number_using_template']." ".($ParField=="used_num"?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" />":"")."</a></td>
												<td><a href=\"javascript:jSORT_TABLE('IsSuspend', '".($ParField=="IsSuspend"?($ParOrder=="ASC"?"DESC":"ASC"):"ASC")."')\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort2','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$i_general_status." ".($ParField=="IsSuspend"?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" />":"")."</a></td>
												<td><a href=\"javascript:jSORT_TABLE('DateModified', '".($ParField=="DateModified"?($ParOrder=="ASC"?"DESC":"ASC"):"ASC")."')\" class=\"tabletoplink\" onMouseOver=\"MM_swapImage('sort3','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\">".$ec_iPortfolio['last_update_time']." ".($ParField=="DateModified"?"<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".($ParOrder=="ASC"?"a":"d")."_off.gif\" border=\"0\" width=\"13\" height=\"13\" name=\"sort1\" id=\"sort1\" />":"")."</a></td>
												<td width=\"25\"><input type=\"checkbox\" name=\"checkmaster\" value=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'file_ids[]'):setChecked(0,this.form,'file_ids[]')\"></td>
											</tr>\n
									";

		for($i=0; $i<count($lp_template_list); $i++)
		{
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$RowClass = ($i%2==0)?"tablerow1":"tablerow2";

				$ReturnStr .=	"
												<tr class=\"".$RowClass."\">
													<td valign=\"top\" ><span class=\"tabletext\">".($i+1)."</span></td>
													<td ><a class=\"tablelink\" href=\"javascript:editTemplate('".addslashes($lp_template_list[$i][0])."')\">".get_file_basename($lp_template_list[$i][0])."</a></td>
													<td class=\"tabletext\">".$lp_template_list[$i][1]."</td>
													<td class=\"tabletext\">".($lp_template_list[$i][2]==0?($lp_template_list[$i][1]>0?$i_status_activated:$i_status_usable):$i_status_suspended)."</td>
													<td class=\"tabletext\">".$lp_template_list[$i][3]."</td>
													<td class=\"tabletext\">".$lp_template_list[$i][4]."</td>
												</tr>
											";
			}
		}

		if(count($lp_template_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\" colspan=\"6\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}

		$ReturnStr .=	"</table>\n";

		return array($ReturnStr, $lp_template_list);
	}

	# Get template list
	function GET_TEMPLATE_LIST($ParField="", $ParOrder=""){
		global $intranet_db;
		global $ck_course_id;
		global $eclass_db,$sys_custom;
		global $ck_user_email;
	$_thisUserID = $_SESSION['UserID'];
$extraSQL = '';
if($sys_custom['iPf']['LP']['editLPForCreatedTeacherOnly']){
	if($this->IS_IPF_SUPERADMIN()) {

	}else{
//		$objEclass40 = new libeclass();  <-- tryed with this but include "libeclass40.php" with redeclare error
//		$result = $objEclass40->returnSpecialRooms($ck_user_email,$ipfRoomType = 4);
		$sql = 'select user_id from '.$eclass_db.'.user_course where user_email = \''.$ck_user_email.'\' and course_id ='.$ck_course_id.' ';
		$result = $this->returnResultSet($sql);

		if(count($result) == 1){
			$result = current($result);
			$this_eclass_use_id = $result['user_id'];
			$extraSQL = ' and f.User_id ='.$this_eclass_use_id.' ';
		}

	}
}



		if($ParField != "" && $ParOrder != "")
			$f_order =	"
										ORDER BY
											".$ParField." ".$ParOrder."
									";

		$sql =	"
							SELECT
								CONCAT(IFNULL(f.VirPath, '/'), f.Title) as Title,
								COUNT(n.notes_id) as used_num,
								f.IsSuspend,
								f.DateModified,
								CONCAT('<input onClick=\"this.form.checkmaster.checked=false\" type=checkbox name=\'file_ids[]\' value=\"', f.FileID,'\" flink=\"', CONCAT(IFNULL(f.VirPath, '/'), f.Title), '\" />', if(COUNT(n.notes_id)=0, '', '*'))
							FROM
								".$this->course_db.".eclass_file as f
							LEFT JOIN
								".$this->course_db.".notes as n
							ON
								n.url = CONCAT(IFNULL(f.VirPath, '/'), f.Title)
							WHERE
								f.VirPath='/TEMPLATES/' AND
								f.Category='0' AND
								f.IsDir<>1
								{$extraSQL}
							GROUP BY
								f.FileID
							{$f_order}
						";

		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}

	# Suspend / Activate template
	function SET_TEMPLATE_ACTIVATE($ParActivateStatus, $ParFileIDs)
	{
		$FileIDs = (is_array($ParFileIDs)) ? implode(",", $ParFileIDs) : $ParFileIDs;

		$sql =	"
							UPDATE
								".$this->course_db.".eclass_file
							SET
								IsSuspend = '".$ParActivateStatus."'
							WHERE
								FileID IN (".$FileIDs.")
						";

		return $this->db_db_query($sql);
	}

	# Copy template file with a new title
	function COPY_TEMPLATE($ParFileID, $ParNewTitle)
	{
		# Get details of original template
		$sql =	"
							SELECT
								Category,
								ParentDirID
							FROM
								".$this->course_db.".eclass_file
							WHERE
								FileID = '".$ParFileID."'";
		$row = $this->returnArray($sql);
		list($categoryID, $folderID) = $row[0];

		# Create new file name for copying
		$search = array(".html", ".HTML", ".htm", ".HTM");
		$newTitle = str_replace($search, "", $ParNewTitle);
		$newTitle = $newTitle.".html";

		# check whether the name already exist
		$repeated = false;
		$sql =	"
							SELECT
								Title
							FROM
								".$this->course_db.".eclass_file
							WHERE
								ParentDirID = '".$folderID."' AND
								category = '".$categoryID."'";
		$row = $this->returnVector($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
			if(trim($newTitle)==trim($row[$i]))
			{
				$repeated = true;
				break;
			}
		}

		if($repeated)
			$msg = "copy_failed_dup_name";
		else
		{
			# Get more template files detail
			$sql =	"
								SELECT
									FileID,
									Location,
									User_id,
									UserName,
									UserEmail,
									memberType,
									Description,
									Size,
									ParentDirID,
									IsDir,
									LastModifiedBy,
									Permission,
									DateInput,
									DateModified,
									VirPath,
									Title
								FROM
									".$this->course_db.".eclass_file
								WHERE
									FileID = '".$ParFileID."'";
			$row = $this->returnArray($sql);

			if (sizeof($row)>0)
			{
				$FileID = $row[0][0];
				$Location = $row[0][1];
				$User_id = $row[0][2];
				$UserName = addslashes($row[0][3]);
				$UserEmail = addslashes($row[0][4]);
				$memberType = $row[0][5];
				$Description = addslashes($row[0][6]);
				$Size = $row[0][7];
				$ParentDirID = $row[0][8];
				$IsDir = $row[0][9];
				$LastModifiedBy = $row[0][10];
				$Permission = $row[0][11];
				$DateInput = $row[0][12];
				$DateModified = $row[0][13];
				$VirPath = $row[0][14];
				$Title = $row[0][15];

				# Prepare file path for copying
				$myPath = $this->GET_CATEGORY_ROOT($categoryID)."/".$Location;
				$filepath_new = str_replace("\'", "'", ($myPath."/".$newTitle));
				$filepath_old = $myPath."/".$Title;

				if (file_exists($filepath_old))
				{
					$success = copy($filepath_old, $filepath_new);

					if($success==1)
					{
						# Insert new record for copied template
						$sql =	"
											INSERT INTO
												".$this->course_db.".eclass_file
													(Title, Location, VirPath, User_id, UserName, UserEmail, memberType, Description, Size, Category, ParentDirID, IsDir, LastModifiedBy, Permission, DateInput, DateModified)
											VALUES
												('".$newTitle."', '".addslashes($Location)."', '".addslashes($VirPath)."', '$User_id', '$UserName', '$UserEmail', '$memberType', '$Description', '$Size', '".$categoryID."', '$ParentDirID', '$IsDir', '$LastModifiedBy', '$Permission', now(), now())
										";
						$this->db_db_query($sql);

						$this->UPDATE_FOLDER_SIZE($folderID);
					}
				}
			}
			$msg = ($success==1) ? "copy" : "copy_failed";
		}

		return $msg;
	}

	# Get category root
	function GET_CATEGORY_ROOT($ParCategoryID){
		$coursefolder = $this->course_db;

		switch ($ParCategoryID){
			case 0: $root = $this->file_path."/".$coursefolder."/notes"; break;
			case 1: $root = $this->file_path."/".$coursefolder."/reference"; break;
			case 2: $root = $this->file_path."/".$coursefolder."/glossary"; break;
			case 3: $root = $this->file_path."/".$coursefolder."/assignment"; break;
			case 4: $root = $this->file_path."/".$coursefolder."/handin"; break;
			case 5: $root = $this->file_path."/".$coursefolder."/question"; break;
			case 6: $root = $this->file_path."/".$coursefolder."/public"; break;
			case 7: $root = $this->file_path."/".$coursefolder."/group"; break;
			case 8: $root = $this->file_path."/".$coursefolder."/personal"; break;
			case 9: $root = $this->file_path."/public"; break;
			case 10: $root = $this->file_path."/album"; break;
			default: $root = $this->file_path."/".$coursefolder."/notes"; break;
		}
		return $root;
	}

	# Update folder size after modifying files
	function UPDATE_FOLDER_SIZE($ParFileID) {
		# recursively outward
		if ($ParFileID=="")
			return;
		$sql =	"
							SELECT
								SUM(Size)
							FROM
								".$this->course_db.".eclass_file
							WHERE
								ParentDirID = '".$ParFileID."'
						";
		$row = $this->returnArray($sql);
		if (sizeof($row)>0) {
			$size = $row[0][0];
			$sql =	"
								UPDATE
									".$this->course_db.".eclass_file
								SET
									Size = '".$size."'
								WHERE
									FileID = '".$ParFileID."'
							";
			$this->db_db_query($sql);

			$sql =	"
								SELECT
									ParentDirID
								FROM
									".$this->course_db.".eclass_file
								WHERE
									FileID = '".$ParFileID."'
							";
			$result = $this->returnArray($sql);
			if (sizeof($result)>0) {
				# do recursively
				$this->UPDATE_FOLDER_SIZE($result[0][0]);
			}
		}
	}

	# Delete template files
	function DELETE_TEMPLATE($ParFileIDs)
	{
		for($i=0; $i<sizeof($ParFileIDs); $i++)
		{
			$fid = $ParFileIDs[$i];

			# check whether the template is being used, if yes, do not allow to delete
			$sql =	"
								SELECT
									COUNT(n.notes_id),
									f.ParentDirID
								FROM
									".$this->course_db.".eclass_file as f
								LEFT JOIN
									".$this->course_db.".notes as n
								ON
									n.url = CONCAT(IFNULL(f.VirPath, '/'), f.Title)
								WHERE
									f.FileID = '".$fid."'
								GROUP BY
									f.FileID
							";
			$check = $this->returnArray($sql);
			list($exist_count, $folderID) = $check[0];

			if($exist_count==0)
			{
				# Get template files detail
				$sql =	"
									SELECT
										FileID,
										Location,
										User_id,
										IsDir,
										Permission,
										Title,
										ifnull(ParentDirID, 'rt')
									FROM
										".$this->course_db.".eclass_file
									WHERE
										FileID = '".$fid."'
								";
				$row = $this->returnArray($sql);

				if (sizeof($row)>0)
				{
					$FileID = $row[0][0];
					$Location = $row[0][1];
					$User_id = $row[0][2];
					$IsDir = $row[0][3];
					$Permission = $row[0][4];
					$Title = $row[0][5];
					$isRoot = ($row[0][6]=='rt');
					$filepath = $this->GET_CATEGORY_ROOT(0)."/".$Location;

					# remove if access right granted
					# delete physical file
					if (file_exists($filepath))
					{
						$filename = $filepath."/".$Title;
						# delete file
						if(file_exists($filename))
							unlink($filename);

						# delete folder if no file remained
						$fileExist = false;
						$dh  = opendir($filepath);
						while (false !== ($file_n = readdir($dh))) {
							if($file_n != "." && $file_n != "..")
							{
								$fileExist = true;
								break;
							}
						}
						closedir($dh);

						if(!$fileExist)
							rmdir($filepath);
					}

					# delete DB row
					$sql =	"
										DELETE FROM
											".$this->course_db.".grouping_function
										WHERE
											function_id = '".$FileID."' AND
											function_type='FM'
									";
					$this->db_db_query($sql);

					$sql =	"
										DELETE FROM
											".$this->course_db.".eclass_file
										WHERE
											FileID = '".$FileID."'
									";
					$this->db_db_query($sql);
				}
				$this->UPDATE_FOLDER_SIZE($folderID);
			}
			else
				$ReturnArr[] = $fid;
		}
		return $ReturnArr;
	}

/*
	# Get class list which has published learning portfolios
	function GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST()
	{
		global $eclass_db, $intranet_db;

		$sql =	"
							SELECT DISTINCT
								iu.ClassName
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as ucf
							ON
								wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							ON
								ucf.user_id = ps.CourseUserID
							INNER JOIN
								".$intranet_db.".INTRANET_USER as iu
							ON
								iu.UserID = ps.UserID
							WHERE
								ucf.notes_published IS NOT NULL AND
								wp.status = '1' AND
								(iu.ClassName IS NOT NULL AND iu.ClassName != '')
							ORDER BY
								iu.ClassName
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}
*/
	# Get class list which has published learning portfolios
	function GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST()
	{
		global $eclass_db, $intranet_db;

		$sql =	"
							SELECT DISTINCT
                y.YearName,
                yc.YearClassID,
								yc.ClassTitleEN
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN ".$this->course_db.".user_config as ucf
                ON wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN ".$eclass_db.".PORTFOLIO_STUDENT as ps
                ON ucf.user_id = ps.CourseUserID
              INNER JOIN ".$intranet_db.".INTRANET_USER as iu
                ON iu.UserID = ps.UserID
              INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu
                ON ycu.UserID = iu.UserID
              INNER JOIN ".$intranet_db.".YEAR_CLASS as yc
                ON yc.YearClassID = ycu.YearClassID
              INNER JOIN ".$intranet_db.".YEAR as y
                ON y.YearID = yc.YearID
							WHERE
								ucf.notes_published IS NOT NULL AND
								wp.status = '1' AND yc.AcademicYearID = ".Get_Current_Academic_Year_ID()."
							ORDER BY
								y.Sequence, yc.Sequence
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}

	function GET_ALL_LEARNING_PORTFOLIO_CLASS_LIST()
	{
		global $eclass_db, $intranet_db;

		$sql =	"
				SELECT DISTINCT
               				y.YearName,
               				yc.YearClassID,
							yc.ClassTitleEN
							FROM
  								{$intranet_db}.INTRANET_USER AS iu
  							INNER JOIN ".$intranet_db.".YEAR_CLASS_USER as ycu
			                ON ycu.UserID = iu.UserID
			              INNER JOIN ".$intranet_db.".YEAR_CLASS as yc
			                ON yc.YearClassID = ycu.YearClassID
			              INNER JOIN ".$intranet_db.".YEAR as y
			                ON y.YearID = yc.YearID
  						 LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                  				ON ps.UserID = iu.UserID
  							WHERE
  								iu.RecordType = '2' AND
  								iu.RecordStatus = '1' AND
  								ps.WebSAMSRegNo IS NOT NULL AND
  								ps.UserKey IS NOT NULL AND
  								ps.IsSuspend = 0 AND
  								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  							GROUP BY
  								iu.ClassName
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}

	# Generate learning portfolio table for display
	function GEN_LEARNING_PORTFOLIO_LIST($ParPageInfoArr, $ParUserID,$ParUserClassID = 0)
	{
		global $image_path, $LAYOUT_SKIN, $i_LastModified, $ec_iPortfolio, $button_publish_iPortfolio, $i_status_published, $i_status_shared, $no_record_msg;
		global $PATH_WRT_ROOT, $ck_is_alumni, $eclass_httppath, $Lang,$eclass40_httppath;

		//this function share with teacher login and student login
		//for student login , it will not pass variable $ParUserClassID to this function
		//it is safe even if $ParUserClassID = 0 , it checks for teacher login only (for teacher view)
		$this->setStudentClassID($ParUserClassID);

		$lp_list = $this->GET_LEARNING_PORTFOLIO_LIST($ParUserID);

		$cu_id = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID,$ck_is_alumni);

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n";

		for($i=0; $i<count($lp_list); $i++)
		{
      # Eric Yip (20090916): Crypt parameters
      $key = $this->GET_STUDENT_CRYPTED_STRING($lp_list[$i]['web_portfolio_id'], $ParUserID);

			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$lp = $this->GET_LEARNING_PORTFOLIO($lp_list[$i]['web_portfolio_id'], $ParUserID);

				if($lp['UserID'] != "" && $lp['allow_review'] == "1")
					$status_arr[] = "<span class=\"LP_shared\">".$i_status_shared."</span>";
				if($lp['UserID'] != "" && $lp['notes_published'] != "")
				{

					//$status_arr[] = "<span class=\"LP_publish\">".$i_status_published."<a href=\"javascript:newWindow('/home/portfolio/learning_portfolio/browse/student_view.php?key=".$key."&eclasskey=".$session_key."', 95)\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></span>";
					//$status_arr[] = "<a href=\"javascript:newWindow('http://{$eclass_httppath}/src/ip2portfolio/contents_student/management/notes_print_update.php?web_portfolio_id=".$lp_list[$i]['web_portfolio_id']."', 33)\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/file/doc.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
					//$status_arr[] = "<a href=\"http://{$eclass_httppath}/src/ip2portfolio/contents_student/management/notes_print_update.php?web_portfolio_id=".$lp_list[$i]['web_portfolio_id']."\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/file/doc.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
					if(empty($lp['version'])){ //Siuwan 20140305 Hide Word Icon for new LP version
						$status_arr[] = "<span class=\"LP_publish\">".$i_status_published."<a href=\"javascript:openEclassWindow('student_view','".$key."','".$ParUserID."',95)\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></span>";
						$worddoc_path = $this->file_path."/".$this->course_db."/portfolio/student_u{$cu_id}_wp{$lp_list[$i]['web_portfolio_id']}/".parseFileName(undo_htmlspecialchars($lp["title"])).".doc";
						$worddoc_action = (file_exists($worddoc_path)) ? "http://{$eclass_httppath}/src/ip2portfolio/contents_student/management/worddoc_download.php?web_portfolio_id={$lp_list[$i]['web_portfolio_id']}&user_id={$cu_id}" : "javascript:alert('{$Lang['iPortfolio']['LPnoWordDoc']}')";
	          			$status_arr[] = "<a href=\"{$worddoc_action}\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/file/doc.gif\" width=\"16\" height=\"16\" border=\"0\"></a>";
					}else{
						$status_arr[] = "<span class=\"LP_publish\">".$i_status_published."<a href=\"http://".$eclass40_httppath."src/iportfolio/?portfolio=".$key."\" target=\"_blank\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></span>";
					}
				}
				else
					$status_arr[] = "<span class=\"LP_draft\">".$ec_iPortfolio['draft']."</span>";

				$status_str = implode(" | ", $status_arr);
				unset($status_arr);

				if($lp['comment_count'] > 0)
//					$comment_str = "<span class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"></span> <span class=\"LP_comment\">".$lp['comment_count'].$ec_iPortfolio['new_comments']."</span>";
					$comment_str = "<span class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"></span> <span class=\"tablelink\">".$lp['comment_count']." ".$ec_iPortfolio['comments']."</span>";
				else
					$comment_str = "";
//					$comment_str = "<a href=\"http://".$lp_list[$i]['web_portfolio_id']."\" class=\"LP_comment\">".$lp['comment_count'].$ec_iPortfolio['new_comments']."</a>";
//				else
//					$comment_str = "<a href=\"http://".$lp_list[$i]['web_portfolio_id']."\" class=\"tablelink\">".$ec_iPortfolio['comments']."</a>";

				if($i % 2 == 0)
					$ReturnStr .=	"<tr>\n";

				if($lp['UserID'] != "" && $lp['notes_published'] != "")
				{
					$ReturnStr .=	"
													<td align=\"center\" valign=\"top\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_01.gif\" width=\"5\" height=\"5\"></td>
																<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\" width=\"5\" height=\"5\"></td>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_03.gif\" width=\"5\" height=\"5\"></td>
															</tr>
															<tr>
																<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\" width=\"5\" height=\"5\"></td>
																<td>
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".($lp['template_selected'] == ""?"01":$lp['template_selected']).".jpg\" width=\"240\" height=\"172\"></td>
																		</tr>
																		<tr>
																			<td align=\"right\" bgcolor=\"#89bf6c\"><a href=\"javascript:;\"></a>
																				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																					<tr>
												";
					if(!$ck_is_alumni)
					{
						if($_SESSION['UserType'] == 1 && $this->HAS_RIGHT("manage_student_learning_sharing"))
						{

							$ReturnStr .=	"
																							<td height=\"30\" class=\"tabletext\">
																								<a href=\"javascript:openEclassWindow('student','".$key."','".$ParUserID."',95) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"javascript:jTO_LP_SETTING('".$lp_list[$i]['web_portfolio_id']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a>
																							</td>
														";
						}
						else if($_SESSION['UserType'] == 2)
						{

							if($lp['deadline'] == "" || $lp['deadline'] > time())
								$ReturnStr .=	"
																								<td height=\"30\" class=\"tabletext\">
																									<a href=\"javascript:openEclassWindow('student','".$key."','".$ParUserID."',95) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a>
                                                  <a href=\"/home/portfolio/learning_portfolio/contents_student/management/notes_setting.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a>
																								</td>
																								<td align=\"right\" class=\"tabletext\">
																									<input type=\"button\" class=\"formbutton\" onClick=\"self.location='./contents_student/management/notes_publish.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."'\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"".$button_publish_iPortfolio."\">
																								</td>
															";
							else
								$ReturnStr .=	"
																								<td height=\"30\" class=\"tabletext\" colspan=\"2\">&nbsp;</td>
															";
						}
					}
					$ReturnStr .=	"
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\" width=\"5\" height=\"5\"></td>
															</tr>
															<tr>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_07.gif\" width=\"5\" height=\"5\"></td>
																<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\" width=\"5\" height=\"5\"></td>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_09.gif\" width=\"5\" height=\"5\"></td>
															</tr>
														</table>
														<table width=\"250\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td>
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td valign=\"top\" class=\"LP_on_title\">".$lp['title']."</td>
																			<td align=\"right\" valign=\"top\" nowrap class=\"tabletext\">
																				".$status_str."
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">
																	".$i_LastModified."	: ".$lp['modified']."<br>
																	".$ec_iPortfolio['publish_date']."	: ".$lp['notes_published']."
																</td>
															</tr>
															<tr>
																<td>
																	".$comment_str."
																</td>
															</tr>
														</table>
														<br>
													</td>
												";
				}
				else
				{
					$ReturnStr .=	"
													<td align=\"center\" valign=\"top\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: solid 1px #cccccc; margin:5px;\">
															<tr>
																<td>
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".($lp['template_selected'] == ""?"01":$lp['template_selected']).".jpg\" width=\"240\" height=\"172\"></td>
																		</tr>
																		<tr>
																			<td align=\"right\" bgcolor=\"#cccccc\"><a href=\"javascript:;\"></a>
																				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																					<tr>
												";
					if($_SESSION['UserType'] == 1 && $this->HAS_RIGHT("manage_student_learning_sharing"))
					{

						$ReturnStr .=	"
																						<td height=\"30\" class=\"tabletext\"><a href=\"javascript:openEclassWindow('student','".$key."','".$ParUserID."',93) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"javascript:jTO_LP_SETTING('".$lp_list[$i]['web_portfolio_id']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a></td>
													";
					}
					else if($_SESSION['UserType'] == 2)
					{

						if($lp['deadline'] == "" || $lp['deadline'] > time())
							$ReturnStr .=	"
																							<td height=\"30\" class=\"tabletext\"><a href=\"javascript:openEclassWindow('student','".$key."','".$ParUserID."',93) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"/home/portfolio/learning_portfolio/contents_student/management/notes_setting.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a></td>
																							<td align=\"right\" class=\"tabletext\">
																								<input type=\"button\" class=\"formbutton\" onClick=\"self.location='./contents_student/management/notes_publish.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."'\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"".$button_publish_iPortfolio."\">
																							</td>
														";
						else
							$ReturnStr .=	"
																							<td height=\"30\" class=\"tabletext\" colspan=\"2\">&nbsp;</td>
														";
					}
					$ReturnStr .=	"
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<table width=\"250\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td>
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td valign=\"top\" class=\"LP_on_title\">".$lp['title']."</td>
																			<td align=\"right\" valign=\"top\" nowrap class=\"tabletext\">
																				".$status_str."
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">
																	".$i_LastModified."	: ".($lp['UserID'] != ""?$lp['modified']:"--")."<br>
																	".$ec_iPortfolio['publish_date']."	: ".($lp['UserID'] != ""?$lp['notes_published']:"--")."
																</td>
															</tr>
															<tr>
																<td>
																	".$comment_str."
																</td>
															</tr>
														</table>
														<br>
													</td>
												";
				}

				if($i % 2 == 1)
					$ReturnStr .=	"</tr>\n";
				else if($i+1 == count($lp_list))
					$ReturnStr .=	"
														<td>&nbsp;</td>\n
													</tr>\n
												";
			}
		}

		if(count($lp_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}

		$ReturnStr .=	"</table>\n";

		return array($ReturnStr, $lp_list);
	}

	//Get Peer Porfolio List
	function GEN_PEER_LEARNING_PORTFOLIO($ParPageInfoArr, $ParUserID)
	{
		global $image_path, $LAYOUT_SKIN, $i_LastModified, $ec_iPortfolio, $button_publish_iPortfolio, $i_status_published, $i_status_shared, $no_record_msg;
		global $PATH_WRT_ROOT, $ck_is_alumni;

		$lp_list = $this->GET_LEARNING_PORTFOLIO_LIST($ParUserID);

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n";

		for($i=0; $i<count($lp_list); $i++)
		{
      # Eric Yip (20090916): Crypt parameters
      $key = $this->GET_STUDENT_CRYPTED_STRING($lp_list[$i]['web_portfolio_id'], $ParUserID);

			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$lp = $this->GET_LEARNING_PORTFOLIO($lp_list[$i]['web_portfolio_id'], $ParUserID);

				if($lp['UserID'] != "" && $lp['notes_published'] != "")
					$status_arr = "<span class=\"LP_publish\">".$i_status_published."<a href=\"javascript:openEclassWindow('student_view','".$key."','".$ParUserID."',93)\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></span>";


				$status_str = $status_arr;
				unset($status_arr);

				if($lp['comment_count'] > 0)
					$comment_str = "<span class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"></span> <span class=\"tablelink\">".$lp['comment_count']." ".$ec_iPortfolio['comments']."</span>";
				else
					$comment_str = "";

				if($i % 2 == 0)
					$ReturnStr .=	"<tr>\n";

				if($lp['UserID'] != "" && $lp['allow_review'] == "1" && $lp['notes_published'] != "")
				{
					$ReturnStr .=	"
													<td align=\"center\" valign=\"top\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_01.gif\" width=\"5\" height=\"5\"></td>
																<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\" width=\"5\" height=\"5\"></td>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_03.gif\" width=\"5\" height=\"5\"></td>
															</tr>
															<tr>
																<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\" width=\"5\" height=\"5\"></td>
																<td>
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".($lp['template_selected'] == ""?"01":$lp['template_selected']).".jpg\" width=\"240\" height=\"172\"></td>
																		</tr>
												";

					$ReturnStr .=	"
																	</table>
																</td>
																<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\" width=\"5\" height=\"5\"></td>
															</tr>
															<tr>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_07.gif\" width=\"5\" height=\"5\"></td>
																<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\" width=\"5\" height=\"5\"></td>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_09.gif\" width=\"5\" height=\"5\"></td>
															</tr>
														</table>
														<table width=\"250\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td>
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td valign=\"top\" class=\"LP_on_title\">".$lp['title']."</td>
																			<td align=\"right\" valign=\"top\" nowrap class=\"tabletext\">
																				".$status_str."
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">
																	<!--".$i_LastModified."	: ".$lp['modified']."<br>-->
																	".$ec_iPortfolio['publish_date']."	: ".$lp['notes_published']."
																</td>
															</tr>

														</table>
														<br>
													</td>
												";
				}

				if($i % 2 == 1)
					$ReturnStr .=	"</tr>\n";
				else if($i+1 == count($lp_list))
					$ReturnStr .=	"
														<td>&nbsp;</td>\n
													</tr>\n
												";
			}
		}

		if(count($lp_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}

		$ReturnStr .=	"</table>\n";

		return array($ReturnStr, $lp_list);
	}

	# Get Learning Portfolio list of a student
	function GET_LEARNING_PORTFOLIO_LIST($ParUserID)
	{
		global $eclass_db, $intranet_db, $ck_is_alumni;

		$lo = new libgroups($this->course_db);
		$exceptionList = $lo->returnFunctionIDs("PORTw",$this->IP_USER_ID_TO_EC_USER_ID($ParUserID));

		if($ck_is_alumni)
		{
			# Get learning portfolio which student has been edited (can be out of editable period)
			$sql =	"
								SELECT DISTINCT
									wp.web_portfolio_id
								FROM
									".$this->course_db.".web_portfolio as wp
								LEFT JOIN
									".$this->course_db.".user_config as ucf
								ON
									wp.web_portfolio_id = ucf.web_portfolio_id
								INNER JOIN
									".$eclass_db.".PORTFOLIO_STUDENT as ps
								ON
									ucf.user_id = ps.CourseUserID
								WHERE
									ps.UserID = '".$ParUserID."' AND
									ucf.notes_published IS NOT NULL AND
									wp.status = 1
								ORDER BY
									ucf.notes_published DESC
							";
			$ReturnArr = $this->returnArray($sql);
		}
		else
		{
			# Get learning portfolio which can be edited (in editable period)
			$sql =	"
								SELECT DISTINCT
									wp.web_portfolio_id
								FROM
									".$this->course_db.".web_portfolio AS wp
								LEFT JOIN
									".$this->course_db.".notes_student as ns
								ON
									(wp.web_portfolio_id = ns.web_portfolio_id AND ns.user_id = '".$this->IP_USER_ID_TO_EC_USER_ID($ParUserID)."')
								WHERE
									wp.web_portfolio_id NOT IN ($exceptionList) AND wp.status='1'
									/* AND (((wp.starttime < now()) AND (wp.deadline > now())) OR (wp.starttime IS NULL AND wp.deadline IS NULL) OR (wp.starttime IS NULL AND (wp.deadline > now())) OR ((wp.starttime < now()) AND wp.deadline IS NULL)) */
								GROUP BY
									wp.web_portfolio_id
							";
			$Set1 = $this->returnArray($sql);

			### [2011-0413-1544-51067] Ivan - Get all the LP related to the students (as the edit period checking will be done in the pop-up windows)
			# Get learning portfolio which student has been edited (can be out of editable period)
			$sql =	"
								SELECT DISTINCT
									wp.web_portfolio_id
								FROM
									".$this->course_db.".web_portfolio as wp
								LEFT JOIN
									".$this->course_db.".user_config as ucf
								ON
									wp.web_portfolio_id = ucf.web_portfolio_id
								INNER JOIN
									".$eclass_db.".PORTFOLIO_STUDENT as ps
								ON
									ucf.user_id = ps.CourseUserID
								WHERE
									ps.UserID = '".$ParUserID."' AND
									/* ucf.notes_published IS NOT NULL AND */
									wp.status = 1
								ORDER BY
									ucf.notes_published DESC
							";
			$Set2 = $this->returnArray($sql);


			# Merge the 2 sets into 1
			foreach($Set2 as $wp_id)
			{
				$wp_id_arr[] = $wp_id[0];
			}
			foreach($Set1 as $wp_id)
			{
				if(!is_array($wp_id_arr) || !in_array($wp_id[0], $wp_id_arr))
					$wp_id_arr[] = $wp_id[0];
			}

			# Rearrange data into proper format
			for($i=0; $i<count($wp_id_arr); $i++)
			{
				$ReturnArr[] = array(
															0 => $wp_id_arr[$i],
															"web_portfolio_id" => $wp_id_arr[$i]
														);
			}
		}

		return $ReturnArr;
	}

	function GET_LEARNING_PORTFOLIO_TITLE($ParWebPortfolioID)
	{
		# Get web portfolio title independently
		$sql =	"
							SELECT DISTINCT
								title
							FROM
								".$this->course_db.".web_portfolio
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."'
						";
		$WP_Title = current($this->returnVector($sql));

		return $WP_Title;
	}

	# Get Learning Portfolio
	function GET_LEARNING_PORTFOLIO($ParWebPortfolioID, $ParUserID)
	{
		global $eclass_db;

		# Get web portfolio title independently
		$WP_Title = $this->GET_LEARNING_PORTFOLIO_TITLE($ParWebPortfolioID);

		# Get web portfolio title independently
		$sql =	"
							SELECT DISTINCT
								UNIX_TIMESTAMP(deadline)
							FROM
								".$this->course_db.".web_portfolio
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."'
						";
		$WP_Deadline = $this->returnVector($sql);

		# Eric Yip (20100715): Get last modified time
		$sql =  "
              SELECT
                date_format(ns.modified,'%Y/%m/%d') as modified
              FROM
                ".$this->course_db.".notes_student as ns
              INNER JOIN ".$eclass_db.".PORTFOLIO_STUDENT as ps
                ON ns.user_id = ps.CourseUserID
              WHERE
								ns.web_portfolio_id = '".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
							ORDER BY
                ns.modified DESC
            ";
    $WP_LastModified = $this->returnVector($sql);

		# Get web portfolio details according to user id
		$sql =	"
							SELECT DISTINCT
								date_format(ucf.notes_published,'%Y/%m/%d') as notes_published,
								ucf.allow_review,
								IF(ucf.template_selected IS NULL, '01', ucf.template_selected) as template_selected,
								ps.UserID,
								wp.version
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as ucf
							ON
								wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							ON
								ucf.user_id = ps.CourseUserID
							WHERE
								wp.web_portfolio_id = '".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
							ORDER BY
								notes_published DESC
						";
		$ReturnArr = $this->returnArray($sql);

		# Add comment count, title and deadline to return array
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['comment_count'] = count($this->GET_LP_COMMENT_LIST($ParWebPortfolioID, $ParUserID));
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['title'] = $WP_Title;
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['deadline'] = $WP_Deadline[0];
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['modified'] = $WP_LastModified[0];

		return $ReturnArr[0];
	}

	# Get comment list in a learning portfolio
	function GET_LP_COMMENT_LIST($ParWebPortfolioID, $ParUserID){

		global $eclass_db;

		$sql =	"
							SELECT
								rc.comment,
								rc.author_user_id,
								date_format(rc.inputdate,'%Y/%m/%d') as inputdate,
								rc.review_comment_id
							FROM
								".$this->course_db.".review_comment as rc
							INNER JOIN
								{$eclass_db}.PORTFOLIO_STUDENT as ps
							ON
								rc.iPortfolio_user_id = ps.CourseUserID
							WHERE
								rc.web_portfolio_id='".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
							ORDER BY
								rc.inputdate DESC
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}

	# Generate peer learning portfolio table for display
	function GEN_PEER_LEARNING_PORTFOLIO_LIST($ParPageInfoArr, $ParUserID)
	{
		global $image_path, $LAYOUT_SKIN, $i_LastModified, $no_record_msg, $ec_iPortfolio;

		$plp_list = $this->GET_PEER_LEARNING_PORTFOLIO_LIST($ParUserID);
		# sort result set in descending order of time
		if(is_array($plp_list) && count($plp_list) > 0)
		{
			foreach($plp_list as $key => $plp)
				$plp_time[$key] = $plp['notes_published'];

			array_multisort($plp_time, SORT_DESC, $plp_list);
		}

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";

		for($i=0; $i<count($plp_list); $i++)
		{
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$plp = $this->GET_LEARNING_PORTFOLIO($plp_list[$i]['web_portfolio_id'], $plp_list[$i]['UserID']);
				$lu = new libuser($plp_list[$i]['UserID']);

				# Eric Yip (20090916): Crypt parameters
        $key = $this->GET_STUDENT_CRYPTED_STRING($plp_list[$i]['web_portfolio_id'], $plp_list[$i]['UserID']);

				$ReturnStr .=	"
												<tr>\n
													<td align=\"center\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: solid 1px #cccccc\">
															<tr>
																<td>
																	<a href=\"javascript:openEclassWindow('student_view','".$key."','".$ParUserID."',93)\" ><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".$plp['template_selected'].".jpg\" width=\"160\" height=\"113\" border=\"0\"></a>
																</td>
															</tr>
														</table>
														<table width=\"180\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
															<tr>
																<td>
																	<a href=\"javascript:openEclassWindow('student_view','".$key."','".$ParUserID."',93)\" class=\"tablelink\" >".$plp['title']."<br>
																	".$lu->getNameForRecord()."</a>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">".$ec_iPortfolio['publish_date']."	: ".$plp['notes_published']."<br></td>
															</tr>
														</table>
														<br>
													</td>
												</tr>\n
											";
			}
		}

		if(count($plp_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}

		$ReturnStr .=	"</table>\n";

		return array($ReturnStr, $plp_list);
	}

	# Get the peer learning portfolio list of a student
	function GET_PEER_LEARNING_PORTFOLIO_LIST($ParUserID){

		global $eclass_db, $intranet_db, $eclass_filepath;

		$learning_portfolio_config_file = "$eclass_filepath/files/learning_portfolio_config.txt";

		$filecontent = trim(get_file_content($learning_portfolio_config_file));
		list($status, $review_type, $friend_review) = unserialize($filecontent);

		$set1 = array();
		$set2 = array();
		if($status)
		{
			if($friend_review)
			{
				# Get learning portfolio from friends
				$sql  = "
									SELECT DISTINCT
										ps.UserID,
										wp.web_portfolio_id,
										uc.notes_published
									FROM
										{$intranet_db}.INTRANET_USER AS iu
									LEFT JOIN
										{$eclass_db}.PORTFOLIO_STUDENT AS ps
									ON
										iu.UserID = ps.UserID
									LEFT JOIN
										".$this->course_db.".user_friend as uf
									ON
										ps.UserID = uf.UserID
									LEFT JOIN
										".$this->course_db.".user_config as uc
									ON
										(ps.CourseUserID = uc.user_id AND uc.allow_review = 1)
									LEFT JOIN
										".$this->course_db.".web_portfolio as wp
									ON
										uc.web_portfolio_id = wp.web_portfolio_id
									LEFT JOIN
										".$this->course_db.".grouping_function as gf
									ON
										gf.function_id = wp.web_portfolio_id
									LEFT JOIN
										".$this->course_db.".user_group as ug
									ON
										ug.group_id = gf.group_id
									WHERE
										uf.FriendID = '".$ParUserID."' AND
										iu.UserID != '".$ParUserID."' AND
										iu.RecordType = '2' AND
										iu.RecordStatus = '1' AND
										ps.UserKey IS NOT NULL AND
										ps.IsSuspend = 0 AND
										wp.web_portfolio_id IS NOT NULL
								";

				$set1 = $this->returnArray($sql);

			}

			switch($review_type)
			{
				case "SCHOOL":
					break;
				case "LEVEL":
					$stu_obj = $this->GET_STUDENT_DATA($ParUserID);
					$conds = " And yc.YearID = '".$stu_obj[0]['YearID']."' ";
					break;
				case "CLASS":
					$stu_obj = $this->GET_STUDENT_DATA($ParUserID);
					$conds =	"
											AND iu.ClassName = '".$stu_obj[0]['ClassName']."'
										";
					break;
			}


			### Added on 2011-01-27 by Ivan: The below condition is added to prevent duplicated student and portfolio shown if the student is in both Friend List and Review Type
			$conds_NotInFriendStudentID = '';
			if (is_array($set1) && count($set1) > 0)
			{
				$FriendStudentIDArr = Get_Array_By_Key($set1, 'UserID');
				$conds_NotInFriendStudentID = " And ps.UserID Not In (".implode(',', (array)$FriendStudentIDArr).") ";
			}
			$sql =	"
								SELECT
									DISTINCT ps.UserID,
									wp.web_portfolio_id,
									uc.notes_published
								FROM
									{$intranet_db}.INTRANET_USER AS iu
									LEFT JOIN
									{$eclass_db}.PORTFOLIO_STUDENT AS ps ON	(iu.UserID = ps.UserID)
									INNER Join
									{$intranet_db}.YEAR_CLASS_USER as ycu On (iu.UserID = ycu.UserID)
									Inner Join
									{$intranet_db}.YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
									LEFT JOIN
									".$this->course_db.".user_config as uc ON (ps.CourseUserID = uc.user_id AND uc.allow_review = 1)
									LEFT JOIN
									".$this->course_db.".web_portfolio as wp ON	(uc.web_portfolio_id = wp.web_portfolio_id)
									LEFT JOIN
									".$this->course_db.".grouping_function as gf ON (gf.function_id = wp.web_portfolio_id)
									LEFT JOIN
									".$this->course_db.".user_group as ug ON (ug.group_id = gf.group_id)
								WHERE
									iu.UserID != '".$ParUserID."' AND
									iu.RecordType = '2' AND
									iu.RecordStatus = '1' AND
									ps.UserKey IS NOT NULL AND
									ps.IsSuspend = 0 AND
									wp.web_portfolio_id IS NOT NULL And
									yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
									$conds
									$conds_NotInFriendStudentID
							";

			$set2 = $this->returnArray($sql);

		}

		return array_merge($set1, $set2);
	}

	# Generate crypted string (web portfolio id + user id) for a Student
	function GET_STUDENT_CRYPTED_STRING($ParWebPortfolioID, $ParUserID){
		return base64_encode($ParWebPortfolioID . "join" . $ParUserID);
	}

	# Decrypt crypted string to (web portfolio id + user id)
	function GET_STUDENT_DECRYPTED_STRING($ParCryptedStr){
		return explode("join", base64_decode($ParCryptedStr));
	}

	# Get user_course_id of user in iPortfolio
	function GET_USER_COURSE_ID($ParUserID)
	{
		global $eclass_db;

		$sql =	"
							SELECT
								user_course_id
							FROM
								{$eclass_db}.user_course
							WHERE
								user_id = '".$this->IP_USER_ID_TO_EC_USER_ID($ParUserID)."' AND
								course_id = '".$this->course_id."'
						";
		$ReturnVal = $this->returnVector($sql);

		return $ReturnVal[0];
	}

	# Generate learning portfolio comment list for display
	function GEN_LP_COMMENT_LIST($ParPageInfoArr, $ParWebPortfolioID, $ParUserID)
	{
		global $image_path, $intranet_session_language, $LAYOUT_SKIN, $no_record_msg;

		$lp_comment_list = $this->GET_LP_COMMENT_LIST($ParWebPortfolioID, $ParUserID);
		$lu = new libuser();

		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}

		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";

		for($i=0; $i<count($lp_comment_list); $i++)
		{
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$user_id = $this->EC_USER_ID_TO_IP_USER_ID($lp_comment_list[$i]['author_user_id']);

				$user_obj = $lu->returnUser($user_id);
				switch($user_obj[0]['RecordType'])
				{
					# Teacher
					case 1:
						$photo_link = $image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg";
						$name_str = ($intranet_session_language=="b5"?$user_obj[0]['ChineseName']:$user_obj[0]['EnglishName']);
						break;
					# Student
					case 2:
						list($photo_filepath, $photo_link) = $this->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
						if(!file_exists($photo_filepath))
							$photo_link = $image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg";
						$name_str = ($intranet_session_language=="b5"?$user_obj[0]['ChineseName']:$user_obj[0]['EnglishName'])." (".$user_obj[0]['ClassName']."-".$user_obj[0]['ClassNumber'].")";
						break;
					default:
						$name_str = "&nbsp;";
						$photo_link = $image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg";
						break;
				}

				# Remove Comment
				//$RemoveCommentStr = ($user_id == $ParUserID || $this->IS_IPF_ADMIN() || $user_obj[0]['RecordType'] == 1) ? "<a href='javascript:checkRemove(".$lp_comment_list[$i]['review_comment_id'].")'><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" border=\"0\" width=\"12\" height=\"12\"></a>" : "&nbsp;";
				$cur_user_obj = $lu->returnUser($_SESSION['ck_intranet_user_id']);
				$RemoveCommentStr = ($ParUserID == $_SESSION['ck_intranet_user_id'] || $this->IS_IPF_ADMIN() || $cur_user_obj[0]['RecordType'] == 1) ? "<a href='javascript:checkRemove(".$lp_comment_list[$i]['review_comment_id'].")'><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" border=\"0\" width=\"12\" height=\"12\"></a>" : "&nbsp;";

				// Eric Yip (20100716): No htmlspecialchars since it has been done during saving
				//$html_comment = nl2br(htmlspecialchars($lp_comment_list[$i]['comment']));
				$html_comment = nl2br($lp_comment_list[$i]['comment']);

				$ReturnStr .=	"
												<tr>
													<td>
														<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td width=\"100\">
																	<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
																		<tr>
																			<td width=\"120\" align=\"center\">
																				<table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																					<tr>
																						<td><div id=\"div\"><span><img src=\"".$photo_link."\" width=\"45\" height=\"60\" border=\"0\"></span> </div></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width=\"120\" align=\"center\" class=\"tabletext\">".$name_str."</td>
																		</tr>
																	</table>
																</td>
																<td align=\"left\" valign=\"top\">
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td width=\"25\" height=\"7\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>
																			<td height=\"7\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>
																			<td width=\"9\" height=\"7\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>
																		</tr>
																		<tr>
																			<td width=\"25\" valign=\"top\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_04.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_04b.gif\" width=\"25\" height=\"40\"></td>
																			<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\">
																				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																					<tr>
																						<td align=\"right\" class=\"tabletext forumtablerow\">
																							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																								<tr>
																									<td align=\"left\">".$RemoveCommentStr."</td>
																									<td align=\"right\" class=\"tabletext\">".$lp_comment_list[$i]['inputdate']."</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr>
																						<td class=\"tabletext\">".$html_comment."</td>
																					</tr>
																					<tr>
																						<td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"10\"></td>
																					</tr>
																				</table>
																			</td>
																			<td width=\"9\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_06.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_06.gif\" width=\"9\" height=\"80\"></td>
																		</tr>
																		<tr>
																			<td width=\"25\" height=\"10\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>
																			<td height=\"10\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>
																			<td width=\"9\" height=\"10\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											";
			}
		}

		if(count($lp_comment_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}

		$ReturnStr .=	"</table>\n";

		return array($ReturnStr, $lp_comment_list);

	}

	# Generate template selection table
	function GEN_TEMPLATE_SELECTION_TABLE($ParTemplateSelected)
	{
		global $image_path, $LAYOUT_SKIN, $button_preview, $i_status_activated, $eclass_httppath, $button_apply;

		$TemplateArr = $this->GET_LEARNING_PORTFOLIO_TEMPLATE_LIST();

		$ReturnStr =	"<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n";

		for($i=0; $i<count($TemplateArr); $i++)
		{
			if($i%3 == 0)
				$ReturnStr .= "<tr>\n";

			if(substr($TemplateArr[$i], -2) == $ParTemplateSelected)
				$ReturnStr .=	"
												<td align=\"center\">
													<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
														<tr>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_01.gif\" width=\"5\" height=\"5\"></td>
															<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\" width=\"5\" height=\"5\"></td>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_03.gif\" width=\"5\" height=\"5\"></td>
														</tr>
														<tr>
															<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\" width=\"5\" height=\"5\"></td>
															<td>
																<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																	<tr>
																		<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".substr($TemplateArr[$i], -2).".jpg\" width=\"210\" height=\"150\" name=\"template[]\" id=\"".$TemplateArr[$i]."\" style=\"\" /></td>
																	</tr>
																	<tr>
																		<td align=\"right\" bgcolor=\"#89bf6c\">
																			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																				<tr>
																					<td height=\"30\" class=\"tabletext\"><strong>#".substr($TemplateArr[$i], -2)." <span class=\"tabletopnolink\">(".$i_status_activated.")</span></strong></td>
																					<td align=\"right\" class=\"tabletext\">&nbsp;</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\" width=\"5\" height=\"5\"></td>
														</tr>
														<tr>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_07.gif\" width=\"5\" height=\"5\"></td>
															<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\" width=\"5\" height=\"5\"></td>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_09.gif\" width=\"5\" height=\"5\"></td>
														</tr>
													</table>
												</td>
											";
			else
				$ReturnStr .=	"
												<td align=\"center\">
													<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: solid 1px #cccccc; margin:5px;\">
														<tr>
															<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".substr($TemplateArr[$i], -2).".jpg\" width=\"210\" height=\"150\" name=\"template[]\" id=\"".$TemplateArr[$i]."\" style=\"opacity:0.4;filter:alpha(opacity=40)\" /></td>
														</tr>
														<tr>
															<td align=\"right\" bgcolor=\"#cccccc\">
																<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																	<tr>
																		<td height=\"30\" class=\"tabletext\">#".substr($TemplateArr[$i], -2)."</td>
																		<td align=\"right\" class=\"tabletext\">
																			<input type=\"button\" class=\"formsubbutton\" value=\"".$button_preview."\"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onClick=\"newWindow('http://".$eclass_httppath."system/portfolio/site_templates/template_".substr($TemplateArr[$i], -2)."/index_main.html', 93)\">
																			<input type=\"button\" class=\"formsubbutton\" value=\"".$button_apply."\"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onClick=\"jSELECT_TEMPLATE('".substr($TemplateArr[$i], -2)."')\">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											";

			if($i%3 == 2 || $i+1 == count($TemplateArr))
				$ReturnStr .= "</tr>\n";
		}

		$ReturnStr .= "</table>\n";

		return $ReturnStr;
	}

	# Get learning portfolio template list
	function GET_LEARNING_PORTFOLIO_TEMPLATE_LIST()
	{
		global $eclass_filepath;

		# Open the folder containing portfolio template files
		if ($handle = opendir($eclass_filepath.'/system/portfolio/site_templates'))
		{
	    while(false !== ($template = readdir($handle)))
			{
				if ($template != "." && $template != "..")
	        $ReturnArr[] = $template;
	    }
	    closedir($handle);
		}

		return $ReturnArr;
	}

	# Generate friend multiple selection list
	function GEN_FRIEND_SELECTION_STUDENT($ParUserID)
	{
		# Get friends
		$FriendArr = $this->GET_STUDENT_FRIEND_LIST($ParUserID);

		# Reorganize array for generating selection list
		for($i=0; $i<count($FriendArrTemp); $i++)
		{
			$FriendArr[$i] = array($FriendArr[$i]['FriendID'], $FriendArr[$i]['DisplayName']);
		}

		$ReturnStr = getSelectByArray($FriendArr, "size='15' multiple='multiple' name='StudentID[]' id='StudentID[]' class='formtextbox'", "", 0, 1);

		return $ReturnStr;
	}

	# Get student friend list
	function GET_STUDENT_FRIEND_LIST($ParUserID)
	{
		global $intranet_db;

		$namefield = getNameFieldByLang2("iu.");
		//$ClassNumberField = getClassNumberField("iu.");

		$sql =	"
							SELECT
								uf.FriendID,
								CONCAT('(', iu.ClassName, ' - ', iu.ClassNumber, ') ', {$namefield}) as DisplayName
							FROM
								".$this->course_db.".user_friend AS uf
							INNER JOIN
								{$intranet_db}.INTRANET_USER as iu
							ON
								uf.FriendID = iu.UserID
							WHERE
								uf.UserID = '".$ParUserID."'
						";
		$ReturnArr = $this->returnArray($sql);

		return $ReturnArr;
	}

	# Update learning portfolio chosen
	function UPDATE_LP_TEMPLATE_SELECT($ParWebPortfolioID, $ParUserID, $ParTemplate)
	{
		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);

		# Check if the learning portfolio is initialized by student
		$sql =	"
							SELECT
								count(*)
							FROM
								".$this->course_db.".user_config
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."' AND
								user_id = '".$CourseUserID."'
						";
		$RowCount = $this->returnVector($sql);

		# Determine whether add or update record
		if($RowCount[0] > 0)
			$sql =	"
								UPDATE
									".$this->course_db.".user_config
								SET
									template_selected = '".$ParTemplate."',
									modified = NOW()
								WHERE
									web_portfolio_id = '".$ParWebPortfolioID."' AND
									user_id = '".$CourseUserID."'
							";
		else
			$sql =	"
								INSERT INTO
									".$this->course_db.".user_config
										(user_id, template_selected, inputdate, modified, web_portfolio_id, allow_review)
								VALUES
									('".$CourseUserID."', '".$ParTemplate."', NOW(), NOW(), '".$ParWebPortfolioID."', '0')
							";

		return $this->db_db_query($sql);
	}

	# Add student friends
	function ADD_STUDENT_FRIEND($ParUserID, $ParFriendIDs)
	{
		$FriendIDArr = is_array($ParFriendIDs) ? $ParFriendIDs : array($ParFriendIDs);

		for($i=0; $i<count($FriendIDArr); $i++)
		{
			if($FriendIDArr[$i] != "")
			{
				$sql =	"
									INSERT INTO
										".$this->course_db.".user_friend
											(UserID, FriendID, TimeInput, TimeModified)
									VALUES
										('".$ParUserID."', '".$FriendIDArr[$i]."', NOW(), NOW())
								";

				$this->db_db_query($sql);
			}
		}
	}

	# Delete student friends
	function DELETE_STUDENT_FRIEND($ParUserID)
	{
		$sql =	"
							DELETE FROM
								".$this->course_db.".user_friend
							WHERE
								UserID = '".$ParUserID."'
						";

		$this->db_db_query($sql);
	}

	# Set sharing status of learning portfolio
	function SET_ALLOW_REVIEW($ParWebPortfolioID, $ParUserID, $ParAllowReview)
	{
		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);

		# Check if the learning portfolio is initialized by student
		$sql =	"
							SELECT
								count(*)
							FROM
								".$this->course_db.".user_config
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."' AND
								user_id = '".$CourseUserID."'
						";
		$RowCount = $this->returnVector($sql);

		# Determine whether add or update record
		if($RowCount[0] > 0)
			$sql =	"
								UPDATE
									".$this->course_db.".user_config
								SET
									allow_review = '".$ParAllowReview."',
									modified = NOW()
								WHERE
									web_portfolio_id = '".$ParWebPortfolioID."' AND
									user_id = '".$CourseUserID."'
							";
		else
			$sql =	"
								INSERT INTO
									".$this->course_db.".user_config
										(user_id, template_selected, inputdate, modified, web_portfolio_id, allow_review)
								VALUES
									('".$CourseUserID."', '01', NOW(), NOW(), '".$ParWebPortfolioID."', '".$ParAllowReview."')
							";

		return $this->db_db_query($sql);
	}

	# Add new comment to learning portfolio
	function ADD_LEARNING_PORTFOLIO_COMMENT($ParLPCommentArr){

		$sql =	"
							INSERT INTO
								".$this->course_db.".review_comment
									(iPortfolio_user_id, comment, author, author_user_id, author_host, web_portfolio_id, inputdate, modified)
							VALUES
								('".$ParLPCommentArr['iPortfolio_user_id']."', '".$ParLPCommentArr['comment']."', '".$ParLPCommentArr['author']."', '".$ParLPCommentArr['author_user_id']."', '".$ParLPCommentArr['author_host']."', '".$ParLPCommentArr['web_portfolio_id']."', NOW(), NOW())
						";

		return $this->db_db_query($sql);
	}

	# Translate User ID in eclass to User ID in IP
	function EC_USER_ID_TO_IP_USER_ID($ParCourseUserID){
		global $eclass_db, $intranet_db;

		# Select from user course
		$sql =	"
							SELECT
								iu.UserID
							FROM
								{$eclass_db}.user_course as uc
							LEFT JOIN
								{$intranet_db}.INTRANET_USER as iu
							ON
								iu.UserEmail = uc.user_email
							WHERE
								uc.user_id = '".$ParCourseUserID."' AND
								uc.course_id = '".$this->course_id."'
						";
		$ReturnVal = $this->returnVector($sql);

		return $ReturnVal[0];
	}

	# Generate publish notes selection table
	function GEN_NOTES_FOR_PUBLISH($ParWebPortfolioID, $ParUserID){

		global $iPort;

		list($NotesArrL1, $NotesArrL2, $NotesArrL3) = $this->GET_NOTES_FOR_PUBLISH($ParWebPortfolioID, $ParUserID);

		$ReturnStr =	"
										<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
											<tr class=\"tabletop\">
												<td width=\"25\" class=\"tabletopnolink\">&nbsp;</td>
												<td class=\"tabletopnolink\">".$iPort["report_section"]." / ".$iPort["component"]."</td>
											</tr>
									";

		for($i=0; $i<count($NotesArrL1); $i++)
		{
			$a_no = $NotesArrL1[$i][1];
			$RowStyleL1 = $NotesArrL1[$i][9] == "1" ? "tablerow1":"tablerow2";
			$CellStyleL1 = $NotesArrL1[$i][9] == "1" ? "row_on":"row_off";

			$ReturnStr .= "
											<tr class=\"".$RowStyleL1."\">
												<td align=\"center\" valign=\"top\" class=\"".$CellStyleL1."\"><input name=\"notes_student_id[]\" level=\"1\" a_no=\"".$a_no."\" type=\"checkbox\" value=\"".$NotesArrL1[$i][0]."\" ".($NotesArrL1[$i][9]=="1"?"checked":"")." onClick=\"jTRIGGER_CHILDREN(this, 2, ".$a_no.")\" /></td>
										";
			if(count($NotesArrL2[$a_no]) > 0)
			{
				$ReturnStr .=	"
												<td class=\"".$CellStyleL1." tabletext\">
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
														<tr>
															<td align=\"left\">".$NotesArrL1[$i][6]."</td>
														</tr>
													</table>
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-top: solid 1px #CCCCCC\">
											";

				for($j=0; $j<count($NotesArrL2[$a_no]); $j++)
				{
					$b_no = $NotesArrL2[$a_no][$j][2];
					$RowStyleL2 = $NotesArrL2[$a_no][$j][9] == "1" ? "tablerow1":"tablerow2";
					$CellStyleL2 = $NotesArrL2[$a_no][$j][9] == "1" ? "row_on":"row_off";

					$ReturnStr .=	"
													<tr class=\"".$RowStyleL2."\">
														<td width=\"25\" align=\"center\" valign=\"top\" class=\"".$CellStyleL2."\"><input name=\"notes_student_id[]\" level=\"2\" a_no=\"".$a_no."\" b_no=\"".$b_no."\" type=\"checkbox\" value=\"".$NotesArrL2[$a_no][$j][0]."\" ".($NotesArrL2[$a_no][$j][9]=="1"?"checked":"")." onClick=\"jTRIGGER_CHILDREN(this, 3, ".$a_no.", ".$b_no."); triggerParents(this.form, '|".$NotesArrL1[$i][0]."|', this.checked)\" /></td>
												";

					if(count($NotesArrL3[$a_no][$b_no]) > 0)
					{
						$ReturnStr .=	"
														<td class=\"".$CellStyleL2." tabletext\">
															<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
																<tr>
																	<td align=\"left\">".$NotesArrL2[$a_no][$j][6]."</td>
																</tr>
															</table>
															<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-top: solid 1px #CCCCCC\">
													";

						for($k=0; $k<count($NotesArrL3[$a_no][$b_no]); $k++)
						{
							$RowStyleL3 = $NotesArrL3[$a_no][$b_no][$k][9] == "1" ? "tablerow1":"tablerow2";
							$CellStyleL3 = $NotesArrL3[$a_no][$b_no][$k][9] == "1" ? "row_on":"row_off";

							$ReturnStr .=	"
															<tr class=\"".$RowStyleL3."\">
																<td width=\"25\" align=\"center\" class=\"".$CellStyleL3."\"><input name=\"notes_student_id[]\" a_no=\"".$a_no."\" b_no=\"".$b_no."\" level=\"3\" type=\"checkbox\" value=\"".$NotesArrL3[$a_no][$b_no][$k][0]."\" ".($NotesArrL3[$a_no][$b_no][$k][9]=="1"?"checked":"")." onClick=\"triggerParents(this.form, '|".$NotesArrL1[$i][0]."||".$NotesArrL2[$a_no][$j][0]."|', this.checked)\" /></td>
																<td class=\"".$CellStyleL3." tabletext\">".$NotesArrL3[$a_no][$b_no][$k][6]."</td>
															</tr>
														";
						}

						$ReturnStr .=	"
																</table>
															</td>
														</tr>
													";
					}
					else
						$ReturnStr .=	"
															<td class=\"".$CellStyleL2." tabletext\">".$NotesArrL2[$a_no][$j][6]."</td>
														</tr>
													";
				}
				$ReturnStr .=	"
														</table>
													</td>
												</tr>
											";
			}
			else
				$ReturnStr .=	"
													<td class=\"".$CellStyleL1." tabletext\">".$NotesArrL1[$i][6]."</td>
												</tr>
											";
		}

		$ReturnStr .=	"</table>";

		return $ReturnStr;
	}

	# Get notes of learning portfolio
	function GET_NOTES_FOR_PUBLISH($ParWebPortfolioID, $ParUserID){

		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);
		# get notes
//		$fieldname = ($ck_memberType=="T") ? "if(status<>1, CONCAT('<font color=".$this->private_color.">',title, '</font>'), title)" : "title";
		$sql =	"
							SELECT
								notes_student_id,
								a_no,
								b_no,
								c_no,
								d_no,
								e_no,
								title,
								url,
								readflag,
								status,
								modified,
								description,
								if(starttime='0000-00-00 00:00:00', '', starttime) as starttime,
								if(endtime='0000-00-00 00:00:00', '', endtime) as endtime,
								RecordType
							FROM
								".$this->course_db.".notes_student
							WHERE
								user_id = '".$CourseUserID."' AND
								web_portfolio_id = '".$ParWebPortfolioID."'
							ORDER BY
								a_no, b_no, c_no, d_no, e_no
						";
		$NotesArr = $this->returnArray($sql);

		# Organize data into an associative array
		for($i=0; $i<count($NotesArr); $i++)
		{
			$note_level = $this->GET_NOTES_LEVEL($NotesArr[$i][1], $NotesArr[$i][2], $NotesArr[$i][3], $NotesArr[$i][4], $NotesArr[$i][5]);

			if($note_level == 1)
				$ReturnArrL1[] = $NotesArr[$i];
			else if($note_level == 2)
				$ReturnArrL2[$NotesArr[$i][1]][] = $NotesArr[$i];
			else if($note_level == 3)
				$ReturnArrL3[$NotesArr[$i][1]][$NotesArr[$i][2]][] = $NotesArr[$i];
		}

		return array($ReturnArrL1, $ReturnArrL2, $ReturnArrL3);
	}

	# Get level of notes
	function GET_NOTES_LEVEL($ParA, $ParB, $ParC, $ParD, $ParE){

		# Level 1
		if ($ParA != 0 && $ParB == 0 && $ParC == 0 && $ParD == 0 && $ParE == 0)
			return 1;
		# Level 2
		else if($ParA != 0 && $ParB != 0 && $ParC == 0 && $ParD == 0 && $ParE == 0)
			return 2;
		# Level 3
		else if($ParA != 0 && $ParB != 0 && $ParC != 0 && $ParD == 0 && $ParE == 0)
			return 3;
		# Level 4
		else if($ParA != 0 && $ParB != 0 && $ParC != 0 && $ParD != 0 && $ParE == 0)
			return 4;
		# Level 5
		else if($ParA != 0 && $ParB != 0 && $ParC != 0 && $ParD != 0 && $ParE != 0)
			return 5;
		else
			return 0; // undefined
	}

/*
	function GEN_PUBLISH_LP_CLASS_SELECTION()
	{
		global $i_alert_pleaseselect, $i_general_WholeSchool;

		$PublishedLPClass = $this->GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST();

		# Reorganize data
		for($i=0; $i<count($PublishedLPClass); $i++)
		{
			$PublishedLPClass[$i] = array($PublishedLPClass[$i][0], $PublishedLPClass[$i][0]);
		}

		$ReturnStr = getSelectByArray($PublishedLPClass, "name='ClassName' id='ClassName' class='formtextbox'", "", 0, 1);

		return $ReturnStr;
	}

	# Copy portfolio files to a folder for burning
	function COPY_PORTFOLIO_FILES($ParClassName, $ParComment)
	{
		global $eclass_filepath, $intranet_session_language;

		$fs = new libfilesystem();

		# To check whether the /eclass/files/portfolio_burnging folder exist and create it if not exist
		$folder_tmp = $this->file_path."/portfolio_2_burn";
		if(!file_exists($folder_tmp))
		{
			$fs->folder_new($folder_tmp);
		}

		# To check whether the /eclass/files/portfolio_burning/$ParClassName folder exist. If exists, delete if and created again.
		$folder_tmp .= "/".trim($ParClassName);
		$return_folder = $folder_tmp;
		if(file_exists($folder_tmp))
		{
			$fs->folder_remove_recursive($folder_tmp);
		}
		$fs->folder_new($folder_tmp);

		$Students = $this->GET_STUDENTS_FOR_COPY_PORTFOLIO($ParClassName);

		for($i=0; $i<sizeof($Students); $i++)
		{
			list($user_id, $WebSAMSRegNo) = $Students[$i];
			$WebSAMSRegNo = str_replace("#", "", $WebSAMSRegNo);
			$folder_target = $folder_tmp."/".$WebSAMSRegNo;

			if(file_exists($folder_target))
			{
				$fs->folder_remove_recursive($folder_target);
			}
			$fs->folder_new($folder_target);

			$wp_id_arr = $this->GET_STUDENT_ALL_PUBLISHED_PORTFOLIO($user_id);

			# Copy published web portfolio files to the destination
			if(sizeof($wp_id_arr)!=0)
			{
				# prepare web portfolio selection
				$web_portfolio_select = "<SELECT onChange='load_page(this.value,".$user_id.");' name='selected_wid'>";

				for($j=0; $j<sizeof($wp_id_arr); $j++)
				{
					list($wp_id, $wp_title) = $wp_id_arr[$j];
					$web_portfolio_select .= "<OPTION value='$wp_id'>".$wp_title."</OPTION>";

					$student_folder = "student_u".$user_id."_wp".$wp_id;
					$folder_source = $this->file_path."/".$this->course_db."/portfolio/".$student_folder;

					exec("cp -r $folder_source $folder_target");

					//$this->replaceImagePath($student_folder, $folder_target."/".$student_folder."/index_main.html");

					# if comment required
					if($ParComment==1)
					{
						# create student_comment.html
						$comment_list = $this->GET_COMMENT_LIST_FOR_CD_BURNING($wp_id, $user_id);
						$comment_content = str_replace("STUDENT_COMMENT_LIST", $comment_list, $this->comment_template);
						$fs->file_write($comment_content, $folder_target."/".$student_folder."/student_comment.html");
					}
				}
				$web_portfolio_select .= "</SELECT>";

				# create the index.php for the student folder
				$first_student_folder = "student_u".$user_id."_wp".$wp_id_arr[0][0];
				$student_url = $first_student_folder."/index_main.html";

				# if comment required
				if($ParComment==1)
				{
					$comment_url = $first_student_folder."/student_comment.html";
					$index_content = str_replace("STUDENT_URL", $student_url, $this->index_template_full);
					$index_content = str_replace("STUDENT_COMMENT", $comment_url, $index_content);
				}
				else
				{
					$index_content = str_replace("STUDENT_URL", $student_url, $this->index_template);
				}
				$fs->file_write($index_content, $folder_target."/index.html");

				# prepare the web_portfolio selection and create the student_comment_menu.html for the student folder
				$menu_content = ($ParComment==1) ? str_replace("WEB_PORTFOLIO_SELECT", $web_portfolio_select, $this->menu_template_full) : str_replace("WEB_PORTFOLIO_SELECT", $web_portfolio_select, $this->menu_template);
				$fs->file_write($menu_content, $folder_target."/student_comment_menu.html");

				############## prepare auto run part ########################
				# prepare the autorun.inf for the student folder
				$autofun_template = "[AutoRun]\n";
				$autofun_template .= "open=autohtml.exe /s\n";
				$autofun_template .= "icon=iportfolio.ico";
				$fs->file_write($autofun_template, $folder_target."/autorun.inf");

				# prepare the showhtml.ini for the student folder
				$showhtml_template = "[Display]\n";
				$showhtml_template .= "htmlfile=\index.html";
				$fs->file_write($showhtml_template, $folder_target."/showhtml.ini");

				# copy the autohtml exe file and the icon
				$fs->file_copy($eclass_filepath."/system/portfolio/cd_burning/autohtml.exe", $folder_target."/autohtml.exe");
				$fs->file_copy($eclass_filepath."/system/portfolio/cd_burning/iportfolio.ico", $folder_target."/iportfolio.ico");
				##############################################################

				$lang = ($intranet_session_language=="en")?"eng":"chib5";
				# copy images files to the student folder
				$source_image_path = $eclass_filepath."/images/portfolio_".$lang."/Icon";
				$source_bg = $source_image_path."/SliverBG.gif";
				$source_comment_icon = $source_image_path."/icon_WriteComment.gif";
				$target_bg_image_path = $folder_target."/images";
				if(!file_exists($target_bg_image_path))
				{
					$fs->folder_new($target_bg_image_path);
				}
				$fs->file_copy($source_bg, $target_bg_image_path);
				$fs->file_copy($source_comment_icon, $target_bg_image_path);
			}
		}

		return $return_folder;
	}

	# Get student list of a class which has published learning portfolio
	function GET_STUDENTS_FOR_COPY_PORTFOLIO($ParClassName){
		global $eclass_db, $intranet_db;

		//$ClassNumberField = getClassNumberField("iu.");
		$sql =	"
							SELECT DISTINCT
								ps.CourseUserID,
								ps.WebSAMSRegNo
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								".$this->course_db.".user_config as uc
							ON
								ps.CourseUserID = uc.user_id
							WHERE
								iu.ClassName = '".$ParClassName."' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								iu.RecordType = '2' AND
								iu.RecordStatus = '1' AND
								ps.IsSuspend = 0 AND
								uc.notes_published IS NOT NULL
							ORDER BY
								iu.ClassNumber,
								iu.EnglishName
						";

		return $this->returnArray($sql);
	}
*/
	# Copy portfolio files to a folder for burning
	function COPY_PORTFOLIO_FILES($ParYearClassID, $ParComment, $Parslp, $Parsbs, $input_student_array='', $Parfreport='', $with_attachment='')
	{
		global $eclass_filepath, $intranet_session_language, $PATH_WRT_ROOT, $ck_course_id, $image_path, $_SESSION, $eclass_httppath;
		$admin_root_path = $PATH_WRT_ROOT;
		$input_array_bool = false;
		if(!empty($input_student_array)){
			$input_array_bool = true;
		}
		$fs = new libfilesystem();

		# To check whether the /eclass/files/portfolio_burnging folder exist and create it if not exist
		##teacher
		if($_SESSION['UserType'] == 1){
			$folder_tmp = $this->file_path."/portfolio_2_burn";
		}
		## Student
		else if($_SESSION['UserType'] == 2){
			$folder_tmp = $this->file_path."/portfolio_2_burn/student";
		}
		## parent
		else if($_SESSION['UserType'] == 3){
			$folder_tmp = $this->file_path."/portfolio_2_burn/parent";
		}
		else{
			exit();
		}

		if(!file_exists($folder_tmp))
		{
			$fs->folder_new($folder_tmp);
		}

		# To check whether the /eclass/files/portfolio_burning/$ParClassName folder exist. If exists, delete if and created again.
		$folder_tmp .= "/".trim($ParYearClassID);
		$return_folder = $folder_tmp;
		if(file_exists($folder_tmp))
		{
			$fs->folder_remove_recursive($folder_tmp);
		}
		$fs->folder_new($folder_tmp);

		$Students = $this->GET_STUDENTS_FOR_COPY_PORTFOLIO($ParYearClassID);

		if($with_attachment){
			$_parStudentIDs = array_keys($input_student_array);
			$student_attachments = $this->GET_STUDENT_OLE_ATTACHMENT($_parStudentIDs);
		}

		for($i=0; $i<sizeof($Students); $i++)
		{

			list($user_id, $WebSAMSRegNo,$DisplayName,$StudentID, $EnglishName) = $Students[$i];
			if($input_student_array[$user_id] || !$input_student_array || $input_student_array[$StudentID]){
				$WebSAMSRegNo = str_replace("#", "", $WebSAMSRegNo);
				$folder_target = $folder_tmp."/".$WebSAMSRegNo."_".str_replace(" ", "_", $this->filename_safe($EnglishName));
				##change the $return_folder to parent and student But not to teacher
				if($_SESSION['UserType'] == 2){
					$return_folder = $folder_target;
				}
				## parent
				else if($_SESSION['UserType'] == 3){
					$return_folder = $folder_target;
				}

				if(file_exists($folder_target))
				{
					$fs->folder_remove_recursive($folder_target);
				}
				$fs->folder_new($folder_target);

				$wp_id_arr = $this->GET_STUDENT_ALL_PUBLISHED_PORTFOLIO($user_id);

				// school record++++++
					if($Parslp==1)
					{
						# create school_record.html

						$school_record_list = $this->generateReportPrint($StudentID, $ParYearClassID);
						$school_record_content = "<html><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><body>".$school_record_list."</body></html>";
						$fs->file_write($school_record_content, $folder_target."/school_record.html");

						// write index.html to group school record & learning portfolio & SchoolBasedScheme
						$index_content = $this->Get_Index_Html_ForBurningCD(sizeof($wp_id_arr), $ParComment, $Parslp, $Parsbs, $folder_target, $admin_root_path, $Parfreport);
						$fs->file_write($index_content, $folder_target."/index.html");
					}
					// School Based Scheme
					if($Parsbs==1)
					{
						# create school_record.html

						$school_record_list = $this->generateSBSPrint($StudentID, $DisplayName, $user_id);
						$school_record_content = "<head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><body><STYLE TYPE='text/css'></head>";
						$school_record_content .= $school_record_list."</html>";
						$fs->file_write($school_record_content, $folder_target."/SBS_index.html");
						$SBS_Path = $folder_target."/SBS";
						if(!file_exists($SBS_Path))
						{
							$fs->folder_new($SBS_Path);
						}
						$include_Path = $folder_target."/SBS/include";
						if(!file_exists($include_Path))
						{
							$fs->folder_new($include_Path);
						}
						$fs->file_copy($admin_root_path."/templates/layer.js", $folder_target."/SBS/include/layer.js");
						$fs->file_copy($admin_root_path."/templates/online_form_edit.js", $folder_target."/SBS/include/online_form_edit.js");
						$fs->file_copy($admin_root_path."/templates/online_form_view.js", $folder_target."/SBS/include/online_form_view.js");
//						$fs->copyFile($admin_root_path."/src/includes/css/style_portfolio_content.css", $folder_target."/SBS/include/style_portfolio_content.css");
//						$fs->copyFile($admin_root_path."/src/includes/css/style_portfolio_test.css", $folder_target."/SBS/include/style_portfolio_test.css");
//						$fs->copyFile($admin_root_path."/src/includes/css/style_portfolio_body.css", $folder_target."/SBS/include/style_portfolio_body.css");
//
//
//
						$sb_icon_path = $folder_target."/SBS/include/icon/";
						if(!file_exists($sb_icon_path))
						{
							$fs->folder_new($sb_icon_path);
						}
						$fs->file_copy($admin_root_path."/images/icon/circle.jpg", $sb_icon_path."circle.jpg");
						$fs->file_copy($admin_root_path."/images/icon/checkbox.jpg", $sb_icon_path."checkbox.jpg");
						$fs->file_copy($admin_root_path."/images/icon/circle_tick.jpg", $sb_icon_path."circle_tick.jpg");
						$fs->file_copy($admin_root_path."/images/icon/checkbox_tick.jpg", $sb_icon_path."checkbox_tick.jpg");
//						$fs->copyFile($admin_root_path."/images/portfolio_chib5/gimg/content_bg.gif", $sb_icon_path."content_bg.gif");
//						$fs->copyFile($admin_root_path."/images/portfolio_chib5/gimg/3d_board_top_bg.gif", $sb_icon_path."3d_board_top_bg.gif");
//
//
						$this->generateSBSfiles($StudentID, $SBS_Path, $user_id);

						// write index.html to group school record & learning portfolio & SchoolBasedScheme if
						// $SchoolRecord is not write it
						if($Parslp != 1){
							$index_content = $this->Get_Index_Html_ForBurningCD(sizeof($wp_id_arr), $ParComment, $Parslp, $Parsbs, $folder_target, $admin_root_path, $Parfreport);
							$fs->file_write($index_content, $folder_target."/index.html");
						}
					}
					if($Parfreport==1)
					{
						# create school_record.html
						$report_list = $this->generatefReportPrint($StudentID);
						$report_content = "<html><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><body>".$report_list."</body></html>";
						preg_match_all("/<img src=([^ >]*)/", $report_content, $arr2);
						if(is_array($arr2[1])){
							foreach($arr2[1] as $img){
								$report_content = str_replace($img, basename($img), $report_content);
//								echo "<img src=".$admin_root_path.$img.">";
// 								debug_pr($img);
// 								debug_pr($folder_target);
// 								debug_pr(basename($img));
								$fs->file_copy($img, $folder_target."/".basename($img));
								$fs->file_copy($admin_root_path.$img, $folder_target."/".basename($img));
							}
						}

						$fs->file_write($report_content, $folder_target."/frp_index.html");



						// write index.html to group school record & learning portfolio & SchoolBasedScheme if
						// $SchoolRecord is not write it
						if($Parslp != 1 && $Parsbs != 1){
							$index_content = $this->Get_Index_Html_ForBurningCD(sizeof($wp_id_arr), $ParComment, $Parslp, $Parsbs, $folder_target, $admin_root_path, $Parfreport);
							$fs->file_write($index_content, $folder_target."/index.html");
						}
					}
					//end of ++++++




				# Copy published web portfolio files to the destination
				if(sizeof($wp_id_arr)!=0)
				{
					# prepare web portfolio selection
					$web_portfolio_select = "<SELECT onChange='load_page(this.value,".$user_id.");' name='selected_wid'>";

					for($j=0; $j<sizeof($wp_id_arr); $j++)
					{
						list($wp_id, $wp_title, $wp_version) = $wp_id_arr[$j];

						$web_portfolio_select .= "<OPTION value='$wp_id'>".$wp_title."</OPTION>";

						$student_folder = "student_u".$user_id."_wp".$wp_id;
						$folder_source = $this->file_path."/".$this->course_db."/portfolio/".$student_folder;
						$fs->folder_new($folder_source);
//					$cmd = "cp -r $folder_source $folder_target";
//					error_log("cmd [".$cmd."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");

						$_folder_source = OsCommandSafe($folder_source);
						$_folder_target = OsCommandSafe($folder_target);
						exec("cp -r $_folder_source $_folder_target");


						//$this->replaceImagePath($student_folder, $folder_target."/".$student_folder."/index_main.html");

						# if comment required
						if($ParComment==1)
						{
							# create student_comment.html
							$comment_list = $this->GET_COMMENT_LIST_FOR_CD_BURNING($wp_id, $user_id);
							$comment_content = str_replace("STUDENT_COMMENT_LIST", $comment_list, $this->comment_template);
							$fs->file_write($comment_content, $folder_target."/".$student_folder."/student_comment.html");
						}
					}


					$web_portfolio_select .= "</SELECT>";

					# create the index.php for the student folder
					$first_student_folder = "student_u".$user_id."_wp".$wp_id_arr[0][0];
					$student_url = $first_student_folder."/index_main.html";

					# if comment required
					if($ParComment==1)
					{
						$comment_url = $first_student_folder."/student_comment.html";
						$index_content = str_replace("STUDENT_URL", $student_url, $this->index_template_full);
						$index_content = str_replace("STUDENT_COMMENT", $comment_url, $index_content);
					}
					else
					{
						$index_content = str_replace("STUDENT_URL", $student_url, $this->index_template);
					}
					//++++++++here
					if($Parslp==1)
					{

						if(sizeof($wp_id_arr) != 0){
							$fs->file_write($index_content, $folder_target."/learning_portfolio.html");
						}

					}
					else if($Parsbs==1)
					{
						if(sizeof($wp_id_arr) != 0){
							$fs->file_write($index_content, $folder_target."/learning_portfolio.html");
						}
					}
					else if($Parfreport==1)
					{
						if(sizeof($wp_id_arr) != 0){
							$fs->file_write($index_content, $folder_target."/learning_portfolio.html");
						}
					}
					else{

						$fs->file_write($index_content, $folder_target."/index.html");
					}

					//end+++++++

					# prepare the web_portfolio selection and create the student_comment_menu.html for the student folder
					$menu_content = ($ParComment==1) ? str_replace("WEB_PORTFOLIO_SELECT", $web_portfolio_select, $this->menu_template_full) : str_replace("WEB_PORTFOLIO_SELECT", $web_portfolio_select, $this->menu_template);
					$fs->file_write($menu_content, $folder_target."/student_comment_menu.html");



					############## prepare auto run part ########################
					# prepare the autorun.inf for the student folder
					$autofun_template = "[AutoRun]\n";
					$autofun_template .= "open=autohtml.exe /s\n";
					$autofun_template .= "icon=iportfolio.ico";
					$fs->file_write($autofun_template, $folder_target."/autorun.inf");

					# prepare the showhtml.ini for the student folder
					$showhtml_template = "[Display]\n";
					$showhtml_template .= "htmlfile=\index.html";
					$fs->file_write($showhtml_template, $folder_target."/showhtml.ini");

					# copy the autohtml exe file and the icon
					$fs->file_copy($eclass_filepath."/system/portfolio/cd_burning/autohtml.exe", $folder_target."/autohtml.exe");
					$fs->file_copy($eclass_filepath."/system/portfolio/cd_burning/iportfolio.ico", $folder_target."/iportfolio.ico");
					##############################################################

					$lang = ($intranet_session_language=="en")?"eng":"chib5";
					# copy images files to the student folder
					$source_image_path = $eclass_filepath."/images/portfolio_".$lang."/Icon";
					$source_bg = $source_image_path."/SliverBG.gif";
					$source_comment_icon = $source_image_path."/icon_WriteComment.gif";
					$target_bg_image_path = $folder_target."/images";
					if(!file_exists($target_bg_image_path))
					{
						$fs->folder_new($target_bg_image_path);
					}
//					error_log("source_bg [".$source_bg."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
//					error_log("target_bg_image_path [".$target_bg_image_path."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");

					$fs->file_copy($source_bg, $target_bg_image_path);
//					error_log("source_comment_icon[".$source_comment_icon."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
//					error_log("target_bg_image_path [".$target_bg_image_path."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
					$fs->file_copy($source_comment_icon, $target_bg_image_path);
				}

				//OLE Attachmenet - Start
				if($with_attachment&&$student_attachments[$user_id]){
					$attachments = $student_attachments[$user_id];
					if(!file_exists($folder_target."/attachments")){
						$fs->folder_new($folder_target."/attachments");
					}
					for($k=0, $k_max=count($attachments); $k<$k_max; $k++){
						$_attachments = $attachments[$k][Attachment];
						$folder_url = $eclass_filepath."/files/portfolio/ole/r".$attachments[$k][RecordID]."/";
						//$_title =  $this->filename_safe($attachments[$i][Title]);
						$_title =  'attachment - '.($k+1);
						for($j=0, $j_max=count($_attachments); $j<$j_max; $j++){
							$_fileName = $_attachments[$j];
							$_fileNameEle = explode('.', $_fileName);

							$new_fileName = $_title." - ".($j+1).".".$_fileNameEle[count($_fileNameEle)-1];
							$fs->file_copy($folder_url.$_fileName, $folder_target."/attachments/".$new_fileName);
						}
					}
				}
				//OLE Attachment - End

				//do the rename file
				$this->prepareDir($folder_target);
			}
		}
		return $return_folder;
	}

	function GET_STUDENT_OLE_ATTACHMENT($parStudentID){
		global $eclass_db;

		$sql = "SELECT
					ps.CourseUserID,
					os.UserID,
					os.RecordID,
					os.Title,
					os.Attachment from {$eclass_db}.OLE_STUDENT os
				LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT ps on os.UserID = ps.UserID
				WHERE
					ps.CourseUserID in (".implode(',',(array)$parStudentID).")
					AND NULLIF(Attachment,'') is not null
				";
		$resultAry = $this->returnResultSet($sql);

		$returnAry = array();
		foreach($resultAry as $_data){
			$_attachment = explode(':', $_data[Attachment]);
			$_data[Attachment] = $_attachment;
			$returnAry[$_data[CourseUserID]][] = $_data;
		}
		return $returnAry;
	}

	# Get student list of a class which has published learning portfolio
	function GET_STUDENTS_FOR_COPY_PORTFOLIO($ParYearClassID){
		global $eclass_db, $intranet_db;

		//$ClassNumberField = getClassNumberField("iu.");
		$NameField = getNameFieldByLang2("iu.");
		$sql =	"
							SELECT DISTINCT
								ps.CourseUserID,
								ps.WebSAMSRegNo,$NameField AS StudentName,ps.UserID, iu.EnglishName AS EnglishName
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							INNER JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                ON  ps.UserID = iu.UserID AND
                    ps.WebSAMSRegNo IS NOT NULL AND
                    ps.UserKey IS NOT NULL
              INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
                ON  iu.UserID = ycu.UserID
							LEFT JOIN ".$this->course_db.".user_config as uc
                ON ps.CourseUserID = uc.user_id
							WHERE
								ycu.YearClassID = '".$ParYearClassID."' AND
								iu.RecordType = '2' AND
								iu.RecordStatus = '1' AND
                ps.IsSuspend = 0
							ORDER BY
								ycu.ClassNumber,
								iu.EnglishName
						";

		return $this->returnArray($sql);
	}

	# Get ID and title of publised Portfolios
	function GET_STUDENT_ALL_PUBLISHED_PORTFOLIO($ParCourseUserID){

		$fieldName = array();
		$sql = "Desc ".$this->course_db.".web_portfolio";
		$fieldAry = $this->returnArray($sql);
		foreach($fieldAry as $_field){
			$fieldName[] = $_field[Field];
		}

		$field = in_array('version', $fieldName) ? "wp.version" : "1 as version" ;

		$sql =	"
							SELECT DISTINCT
								wp.web_portfolio_id,
								wp.title,
								$field
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as uc
							ON
								wp.web_portfolio_id = uc.web_portfolio_id
							WHERE
								uc.user_id = '".$ParCourseUserID."' AND
								uc.notes_published IS NOT NULL
							ORDER BY
								uc.notes_published DESC
				";
		$row = $this->returnArray($sql);

		return $row;
	}

	# iPortfolio Comment for CD burning
	function GET_COMMENT_LIST_FOR_CD_BURNING($ParWebPortfolioID, $ParCourseUserID){
		global $ec_iPortfolio, $image_path;

		$sql =	"
							SELECT
								comment,
								author,
								modified
							FROM
								".$this->course_db.".review_comment
							WHERE
								iPortfolio_user_id = '".$ParCourseUserID."' AND
								web_portfolio_id = '".$ParWebPortfolioID."'
							ORDER BY
								inputdate DESC
						";
		$row = $this->returnArray($sql);

		if(sizeof($row)!=0)
		{
			for ($i=0; $i<sizeof($row); $i++)
			{
				list($comment, $author, $modified) = $row[$i];
				// No htmlspecialchars since it has been done during saving
				$comment = nl2br($comment);

				$rx .= "<TR align=middle><TD>\n";
				$rx .= "<TABLE cellSpacing=0 cellPadding=1 width='100%' bgColor='#FFD5C9' border=0>\n";
				$rx .= "<TR>\n";
				$rx .= "<TD><TABLE cellSpacing=0 cellPadding=3 width=100% border=0>\n";
				$rx .= "<TR>\n";
				$rx .= "<TD height=70 bgcolor='#FFFFCC'><TABLE cellSpacing=0 cellPadding=5 width=100% height='100%' border=0><tr><td style=\"filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#FFFFF4', endColorStr='#FFFFCC', gradientType='1')\"><span style='line-height:145%'>$comment </span></td></tr></table></TD>\n";
				$rx .= "</TR>\n";
				$rx .= "<TR>\n";
				$rx .= "<TD width='100%' height=20 style=\"filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#FFEFDF', endColorStr='#FFCCCC', gradientType='1')\" ><span style='FONT-SIZE:11px; FONT-FAMILY:Arial; color:#BB4455;'>".$ec_iPortfolio['review_author']."{$author}, $modified</span></TD>\n";
				$rx .= "</TR>\n";
				$rx .= "</TABLE></TD>\n";
				$rx .= "</TR>\n";
				$rx .= "</TABLE>\n";
				$rx .= "</TD></TR>\n";
				$rx .= "<TR align=middle><TD>&nbsp;</TD></TR>\n";
			}
		}
		else
		{

		}

		return $rx;
	}

/*
	# Delete portfolio files prepared
	function REMOVE_PORTFOLIO_FILES($ParClassName)
	{
		$folder_tmp = $this->file_path."/portfolio_2_burn/".$ParClassName;

		if(file_exists($folder_tmp))
		{
			exec("rm -r \"$folder_tmp\"");
		}

		$x = (!file_exists($folder_tmp)) ? 1 : 0;

		return $x;
	}
*/
	# Delete portfolio files prepared
	function REMOVE_PORTFOLIO_FILES($ParYearClassID)
	{
		$folder_tmp = $this->file_path."/portfolio_2_burn/".$ParYearClassID;

		if(file_exists($folder_tmp))
		{
		    $_folder_tmp = OsCommandSafe($folder_tmp);
			exec("rm -r \"$_folder_tmp\"");
		}

		$x = (!file_exists($folder_tmp)) ? 1 : 0;

		return $x;
	}

	# Generate selection list for classes, which have activated students
	function GEN_CLASS_SELECTION_STUDENT($ParClassName="")
	{
		global $i_alert_pleaseselect, $i_general_WholeSchool;

		# Get classes with activated students
		$ClassActivatedNo = $this->GET_CLASS_STUDENT_ACTIVATED();

		if(is_array($ClassActivatedNo))
		{
			# Classes are stored in keys
			$ClassActivated = array_keys($ClassActivatedNo);

			$ClassArr = $this->GET_CLASS_LIST_DATA();

			if(is_array($ClassArr))
			{
				for($i=0; $i<count($ClassArr); $i++)
				{
					# If class includes classes with activated students, add to array
					if(
							in_array($ClassArr[$i]['ClassName'], $ClassActivated) &&
							($ParClassLevelID == "" || $ParClassLevelID == $ClassArr[$i]['ClassLevelID']) &&
							(!isset($ClassNameActivated) || !in_array($ClassArr[$i]['ClassName'], $ClassNameActivated))
						)
					{
						$ClassNameActivated[] = $ClassArr[$i]['ClassName'];
					}
				}
			}

			# Reorganize array for generating selection list
			for($i=0; $i<count($ClassNameActivated); $i++)
			{
				$ClassNameActivated[$i] = array($ClassNameActivated[$i], $ClassNameActivated[$i]);
			}
		}

		$ReturnStr = getSelectByArrayTitle($ClassNameActivated, "name='ClassName' id='ClassName' class='formtextbox' onChange='jCHANGE_FIELD(\"classname\")'", $i_alert_pleaseselect, $ParClassName);

		return $ReturnStr;
	}

	# Generate selection list for students, which have activated students
	function GEN_STUDENT_SELECTION_STUDENT($ParClassName)
	{
		global $ck_intranet_user_id;

		# Get classes with activated students
		$StudentArrTemp = $this->GET_STUDENT_LIST_DATA($ParClassName);
		if(is_array($StudentArrTemp))
		{
			# Reorganize array for generating selection list
			for($i=0; $i<count($StudentArrTemp); $i++)
			{
				if($StudentArrTemp[$i]['UserID'] != $ck_intranet_user_id)
					$StudentArr[] = array($StudentArrTemp[$i]['UserID'], $StudentArrTemp[$i]['DisplayName']);
			}

			$ReturnStr = getSelectByArray($StudentArr, "size='21' multiple='multiple' ame='StudentID[]' id='StudentID[]' class='formtextbox'", "", 0, 1);
		}

		return $ReturnStr;
	}

	# Remove learning portfolio comments
	function DELETE_LP_COMMENT($ParCommentID)
	{
		$sql =	"
					DELETE FROM
						".$this->course_db.".review_comment
					WHERE
						review_comment_id='".$ParCommentID."'
				";
		$this->db_db_query($sql);
	}

	# Get student portfolio file path
	function GET_STUDENT_PORTFOLIO_PATH($ParWebPortfolioID, $ParUserID){
		global $eclass_filepath, $eclass_httppath;

		$user_data = $this->GET_USER_FOLDER_ID($ParWebPortfolioID, $ParUserID);
		$file_main = "/files/".$this->course_db."/portfolio/".$user_data["folder_name"]."/index_main.html";
//error_log("file_main [".$file_main."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		if (file_exists($eclass_filepath."/".$file_main)){
		    $portcol = (checkHttpsWebProtocol())? 'https' : 'http';
			$file_main = "{$portcol}://{$eclass_httppath}{$file_main}";
		}else
			$file_main = null;
//error_log("return  [".$file_main."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		return $file_main;
	}

	# Get user folder id and name
	function GET_USER_FOLDER_ID($ParWebPortfolioID, $ParUserID){
		global $eclass_db;

		$sql =	"
							SELECT
								ps.CourseUserID
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as ucf
							ON
								wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							ON
								ucf.user_id = ps.CourseUserID
							WHERE
								wp.web_portfolio_id = '".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
						";

		$CourseUserID = $this->returnVector($sql);
		$CourseUserID = $CourseUserID[0];

		$folderName = "student_u{$CourseUserID}_wp".$ParWebPortfolioID;

		$sql =	"
							SELECT
								FileID
							FROM
								{$eclass_db}.eclass_file
							WHERE
								Title='$folderName' AND
								Category=7 AND
								IsDir=1 AND
								VirPath IS NULL
						";
		$row = $this->returnVector($sql);
/*
		if (($folderID=$row[0])=="")
		{
			include_once("$eclass_filepath/src/includes/php/lib-user.php");
			include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

			# create folder if necessary

			$lu = new libuser($user_id, $this->course_db);
			$fm = new fileManager($this->course_id, 7, "");
			$fm->user_name = addslashes($lu->user_name());
			$fm->memberType = "S";
			$fm->permission = "";
			$fm->maxSize = $this->returnPersonalFolderSize($user_id);
			$scheme_obj = $this->getSchemeInfo($ParWebPortfolioID);
			$folder_description = addslashes($scheme_obj[0]["title"]." (User Folder)");
			$folderID = $fm->createFolderDB($folderName, $folder_description, "", 7);
		}
*/
		return Array("folder_id"=>$folderID, "folder_name"=>$folderName);
	}


	/**
	* UPDATE TEACHER VIEW STUDENT LP TIME
	* THIS FUNCTION WILL CALL updateTeacherTracking() , but it help to convert ID IN INTRANET_USER to EClass USER ID
	* @param : INT $StudentID IN INTRANET_USER
	* @param : INT $WebPortfolioID
	* @param : INT $UserID IN INTRANET_USER
	* @return : NULL
	*
	*/
	function updateTeacherViewTime($StudentID,$WebPortfolioID,$UserID)
	{

		$isTeacherLogin = ($StudentID != $UserID) ? true:false;
		if($isTeacherLogin)
		{

			$StudentIDInIportfolio = $this->IP_USER_ID_TO_EC_USER_ID($StudentID);
			$teacherIDInIportfolio = $this->IP_USER_ID_TO_EC_USER_ID($UserID);

			$this->updateTeacherTracking($StudentIDInIportfolio, $WebPortfolioID, $teacherIDInIportfolio);
		}
		//else { do nothing}
	}

	# update for student's new update when teacher view the LP
	# //THIS FUNCTION COPY FROM /home/web/eclass40/eclass30/src/includes/php/lib-notes_portfolio.php
	function updateTeacherTracking($my_student_id, $my_web_portfolio_id, $my_user_id){

		# get the record
		$sql = "SELECT
					portfolio_tracking_id
				FROM
					".$this->course_db.".portfolio_tracking_last
				WHERE
					student_id={$my_student_id}
					AND teacher_id={$my_user_id}
					AND web_portfolio_id={$my_web_portfolio_id}
				";
		$row = $this->returnVector($sql);

		# if record not found, add a new one
		if (sizeof($row)>0)
		{
			$portfolio_tracking_id = $row[0];
			# if found, update the teacher's view time
			$sql = "UPDATE
						".$this->course_db.".portfolio_tracking_last
					SET
						teacher_view_time=now(),
						modified=now()
					WHERE
						portfolio_tracking_id={$portfolio_tracking_id}
					";
			$this->db_db_query($sql);

		}

	}

	function Get_Index_Html_ForBurningCD($wp_id_num, $comment, $SchoolRecord, $SchoolBasedScheme='', $folder_target = '', $admin_root_path ='', $fullreport='')
	{
		global $sys_custom;

		if($sys_custom['iPortfolio_BurnCD']['HinHua']){
			$index_tmpl = file_get_contents("{$admin_root_path}/home/portfolio/learning_portfolio/contents_admin/tmpl/HinHua/burnCD_HinHua.html.tmpl", true);
			$libfs = new libfilesystem();
			$hinhua_image_path = $folder_target."/images";
			if(!file_exists($hinhua_image_path))
			{
				$libfs->folder_new($hinhua_image_path);
			}
			$libfs->file_copy($admin_root_path."/home/portfolio/learning_portfolio/contents_admin/tmpl/HinHua/images/intranet4bg.gif", $hinhua_image_path."/intranet4bg.gif");
			$libfs->file_copy($admin_root_path."/home/portfolio/learning_portfolio/contents_admin/tmpl/HinHua/images/sch.gif", $hinhua_image_path."/sch.gif");

		}
		else{
			$index_tmpl = "
			<style type='text/css'>
	a {
	text-decoration: none;
		font-weight:bold;
		font-size:16;
		color: #FFFFFF;
		display:block;
		line-height:30px;
		background-color:#6699cc;
		border-bottom: solid 1px #666666;
		border-top: solid 1px #FFFFFF;
		border-left: solid 1px #FFFFFF;
		border-right: solid 1px #666666;
	}

	a:hover {
		text-decoration: none;
		color: yellow;
		font-weight:bold;
		font-size:16;
		background-color:#94be6e;
		border-bottom: solid 1px #666666;
		border-top: solid 1px #FFFFFF;
		border-left: solid 1px #FFFFFF;
		border-right: solid 1px #666666;
		}

	body {
		background-color: #d8e9fe;
		font-family:Verdana, Arial, Helvetica, sans-serif;
	}
	</style>
	";

			$index_tmpl .= "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>";
			$index_tmpl .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' height='70%'>";
			$index_tmpl .= "<tr><td><br />";
			$index_tmpl .= "<!--Link_LP-->";
			$index_tmpl .= "<!--Link_SchoolRecord-->";
			$index_tmpl .= "<!--Link_SB--><!--fullreport-->";
			$index_tmpl .= "</td></tr>";
			$index_tmpl .= "</table>";
			$index_tmpl .= "</body></html>";

		}
		// Get and insert three link into the index_tmpl
		$x_lp_cmd = "";
		$x_sr_cmd = "";
		$x_sbs_cmd = "";

		$IS_POPUP = true;
		if($wp_id_num != 0)
		{

			$x_lp_cmd .= $this->getLPLinkHTML($IS_POPUP);
		}

		if($SchoolRecord == 1)
		{
			$x_sr_cmd .= $this->getSchoolRecordLinkHTML($IS_POPUP);
		}
		if($SchoolBasedScheme == 1)
		{
			$x_sbs_cmd .= $this->getSBSLinkHTML($IS_POPUP);
		}
		if($fullreport == 1)
		{
			$x_frp_cmd .= $this->getfrpLinkHTML($IS_POPUP);
		}
		$order = array("<!--Link_LP-->", "<!--Link_SchoolRecord-->", "<!--Link_SB-->", "<!--fullreport-->");
		$replace = array($x_lp_cmd, $x_sr_cmd, $x_sbs_cmd, $x_frp_cmd);
		$index_tmpl_fn = str_replace($order, $replace, $index_tmpl);

		return $index_tmpl_fn;
	} // end function get index html for burningCD

	function getLPLinkHTML($ParIsPopup = false){
		global $iPort;
		if ($ParIsPopup) {
			$about_blank_pop = " target='_blank' ";
		}
		$x_lp_cmd .= "<table width='300' border='0' cellspacing='10' cellpadding='0' align='center'>";
		$x_lp_cmd .= "<tr><td align='center' valign='middle' bgcolor='#6699cc'>";
		$x_lp_cmd .= "<a href='learning_portfolio.html' $about_blank_pop>".$iPort['menu']['learning_portfolio']."</a>";
		$x_lp_cmd .= "</td></tr>";
		$x_lp_cmd .= "</table>";
		return $x_lp_cmd;
	}
	function getSchoolRecordLinkHTML($ParIsPopup = false){
		global $ec_iPortfolio;
		if ($ParIsPopup) {
			$about_blank_pop = " target='_blank' ";
		}
		$x_sr_cmd .= "<table width='300' border='0' cellspacing='10' cellpadding='0' align='center'>";
		$x_sr_cmd .= "<tr><td align='center' valign='middle' bgcolor='#6699cc'>";
		$x_sr_cmd .= "<a href='school_record.html' $about_blank_pop>".$ec_iPortfolio['student_learning_profile']."</a>";
		$x_sr_cmd .= "</td></tr>";
		$x_sr_cmd .= "</table>";
		return $x_sr_cmd;
	}
	function getSBSLinkHTML($ParIsPopup = false){
		global $iPort;
		if ($ParIsPopup) {
			$about_blank_pop = " target='_blank' ";
		}
		$x_sbs_cmd .= "<table width='300' border='0' cellspacing='10' cellpadding='0' align='center'>";
		$x_sbs_cmd .= "<tr><td align='center' valign='middle' bgcolor='#6699cc'>";
		$x_sbs_cmd .= "<a href='SBS_index.html' $about_blank_pop>".$iPort['menu']['school_based_scheme']."</a>";
		$x_sbs_cmd .= "</td></tr>";
		$x_sbs_cmd .= "</table>";

		return $x_sbs_cmd;
	}
	function getfrpLinkHTML($ParIsPopup = false){
		global $ec_iPortfolio;
		if ($ParIsPopup) {
			$about_blank_pop = " target='_blank' ";
		}
		$x_sbs_cmd .= "<table width='300' border='0' cellspacing='10' cellpadding='0' align='center'>";
		$x_sbs_cmd .= "<tr><td align='center' valign='middle' bgcolor='#6699cc'>";
		$x_sbs_cmd .= "<a href='frp_index.html' $about_blank_pop>".$ec_iPortfolio['full_report']."</a>";
		$x_sbs_cmd .= "</td></tr>";
		$x_sbs_cmd .= "</table>";

		return $x_sbs_cmd;
	}

	# Generate Student Full Report for Printing
	function generateReportPrint($StudentID, $ParYearClassID)
	{
		global $intranet_root, $lpf_slp;
		include_once("libpf-slp.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/slp_report.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/slp_report_data.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/common.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/full_report.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/transcript.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/slp_report_html.php");

		if($StudentID!="")
		{
			$StudentArr = array($StudentID);
		}
		$lpf_slp = new libpf_slp();
		# Academic Year
	    $lib_ay = new academic_year();
	    $Year = $lib_ay->YearNameEN;

	    $lpf_slp_data = new slp_report_data();
	    $lpf_slp_data->SET_DATA_ARRAY($StudentArr, "");

	    $lib_slp_report = new slp_report_html();

	    # Set data to report printing object
	    // if modified below coding, please modify "/home/portfolio/profile/report/report_print.php" also
	    $lib_slp_report->SET_SPArr($lpf_slp_data->GET_SPArr());
	    $lib_slp_report->SET_SubjArr($lpf_slp_data->GET_SubjArr());
	    $lib_slp_report->SET_YearArr($lpf_slp_data->GET_YearArr());
	    $lib_slp_report->SET_APArr($lpf_slp_data->GET_APArr());
	    $lib_slp_report->SET_AwardArr($lpf_slp_data->GET_AwardArr());
	    $lib_slp_report->SET_ActivityArr($lpf_slp_data->GET_ActivityArr());
	    $lib_slp_report->SET_OLEArr($lpf_slp_data->GET_OLEArr());
	    $lib_slp_report->SET_ExtPerformArr($lpf_slp_data->GET_ExtPerformArr());
	    $lib_slp_report->SET_SelfAccountArr($lpf_slp_data->GET_SelfAccountArr());
	    $lib_slp_report->SET_SubjectInfoOrderedArr($lpf_slp_data->GET_SubjectInfoOrderedArr());
    	$lib_slp_report->SET_SubjectFullMarkArr($lpf_slp_data->GET_SubjectFullMarkArr());

		# Report printing
		$report_html = $lib_slp_report->GEN_STUDENT_LEARNING_PROFILE_REPORT_PRINT($StudentArr, $issuedate, $Year);
		return $report_html;
	}

	# Generate Student Full Report for Printing
	function generatefReportPrint($StudentID)
	{
		global $intranet_root, $lpf_slp, $lpf_report, $lpf_sturec;
		include_once("libpf-slp.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/slp_report.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/slp_report_data.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/common.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/full_report.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/transcript.php");
		include_once("$intranet_root/home/portfolio/profile/report/report_lib/slp_report_html.php");
		include_once("$intranet_root/includes/libpf-sturec.php");


		if($StudentID!="")
		{
			$StudentArr = array($StudentID);
		}

		$lpf_sturec = new libpf_sturec();
    	$lpf_report = new libpf_report();
    	$lpf_slp = new libpf_slp();

		$report_html = generateFullReportPrint($StudentArr);
		return $report_html;
	}

	function generateSBSPrint($StudentID, $DisplayName, $user_id){
	global $ck_course_id, $ck_default_lang, $project_phase_name, $assignments_name, $ec_iPortfolio, $no_record_msg,$iPort;
	include_once('libportfolio_group.php');



	$db_name = $this->course_db;

	if ($user_id!="")
	{
		$lo = new libportfolio_group($db_name);
		$workList = $lo->returnFunctionIDs("A",$user_id);
	}




	$sql  = "SELECT a.title as title, a.assignment_id as assignment_id, a.modified as modified FROM $db_name.assignment AS a ";
	$sql .= "LEFT JOIN $db_name.assignment AS b ON a.assignment_id = b.parent_id ";
	$sql .= "LEFT JOIN $db_name.handin AS c ON (b.assignment_id = c.assignment_id AND c.user_id = '$user_id') ";
	$sql .= "WHERE a.worktype=6 AND a.assignment_id NOT IN ($workList) AND a.status='1' ";
	$sql .= "GROUP BY a.assignment_id ";

	$rs = $this->ReturnArray($sql);

	$style= <<<HTML

.div_top{
	width:85%;
	background-image:url("./SBS/include/icon/3d_board_top_bg.gif");
	heigth:45px;
	text-align:center;
	color:white;
	font-weight: bold;
}
.div_content{
	background-color:#FFFFFF;
	padding-top: 5px;
	width:85%;
	text-align:center;
}
.div_bottom{

}
table th{
	font-size:13px;
}
.EE{
	background-color:#EEEEEE;
	font-family: Verdana, Arial, Helvetica, Mingliu, sans-serif;
	font-size: 13px;


}
.FF{
	background-color:#FFFFFF;
	font-family: Verdana, Arial, Helvetica, Mingliu, sans-serif;
	font-size: 13px;

}
table{
	border-collapse:collapse;
	width:85%;
	vertical-align:top;
	border-color:#C2E0FE;
	border:1px dashed;

}
table td{
	border-width:0;
}
HTML;

//	$return = "<link href=\"./SBS/include/style_portfolio_body.css\" rel=\"stylesheet\" type=\"text/css\">" .
//			"<link href=\"./SBS/include/style_portfolio_test.css\" rel=\"stylesheet\" type=\"text/css\">";
//	$return .= "<link href=\"./SBS/include/style_portfolio_content.css\" rel=\"stylesheet\" type=\"text/css\">";
//	$return = "<body background=\"./SBS/include/icon/content_bg.gif\" style=\"text-align:center\">";

	$return .= "<style>$style</style>";

	$return .= "<div class=\"div_top\">".$iPort['menu']['school_based_scheme']."</div>";
	$return .= "<div class=\"div_content\">";
	$return .= "<p><b>".$DisplayName."</b></p>";

	//table
	$return .= "<table>";
	$return .= "<tr>";
	$return .= "<th height='35' width='5%' align='center' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>#</th>";
	$return .= "<th width='45%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$assignments_name."</th>";
	$return .= "<th width='35%' bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$project_phase_name."</th>";
	$return .= "<th bgcolor='#D6EDFA' class='link_chi' style='border-bottom:2px solid #7BC4EA;'>".$ec_iPortfolio['last_modified']."</th>";
	$return .= "</tr>";
	$row_num = 1;

	$t_color = "class=\"EE\"";
	if(sizeof($rs) > 0){
		foreach($rs as $value){
			if($row_num % 2){
				$t_color = "class=\"FF\"";
			}
			else
			{
				$t_color = "class=\"EE\"";
			}
			$return .= "<tr><td $t_color align='center'>$row_num&nbsp;</td>";
			$return .= "<td $t_color style=\"vertical-align: center;\">";
			$return .= $value["title"]."</td><td $t_color>";
			$return .= $this->getSchemePhases_table($value["assignment_id"], $user_id, $value["title"]);
			$return .= "<td $t_color style=\"vertical-align: top;\">".$value["modified"]."</td>";

			$return .= "</td></tr>";
			$row_num ++;
			//echo $row_num;

		}
	}
	else{
		$return .= "<tr><td style=\"text-align:center\" colspan=\"4\">$no_record_msg</td></tr>";
	}
	$return .= "</table><br \><br \><br \></div></body>";
	return $return;
	}

	// get the table for SBS_index.html
	function getSchemePhases_table($my_assignment_id, $user_id="",$sbs_title){
		global $ck_course_id, $ck_default_lang;
		$db_name = $this->course_db;
				$sql  = "
					SELECT
							a.assignment_id as phases_id, a.title as title
					FROM
							$db_name.assignment AS a
							LEFT JOIN $db_name.handin AS h ON (h.assignment_id=a.assignment_id AND h.user_id='{$user_id}')
					WHERE
							a.parent_id='$my_assignment_id'
					GROUP BY
							a.assignment_id
					ORDER BY
							a.starttime, a.deadline
				";
				$rs = $this->ReturnArray($sql);
				$return ='';
				foreach($rs as $v){
					$phase_title = $v["title"];
					$phase_id = $v["phases_id"];
					$return .="<a href=\"./SBS/{$my_assignment_id}/{$phase_id}.html\">".$phase_title. "</a><br \>";

				}
				unset($v);
				return $return;
	}

	function generateSBSfiles($StudentID, $path, $user_id){
		global $ck_course_id, $ck_user_id, $ck_memberType, $ck_user_course_id;
		global $admin_url_path, $ck_ole_teacher_right, $ck_login_session_id;
		global $ck_helper_job, $ck_function_rights, $ck_room_type, $ck_user_role, $ck_roleType;
		global $ck_default_lang, $intranet_root, $eclass_root, $image_path, $eclass_url_path ;
		global $_SESSION;

		$fs = new libfilesystem();

//		$pf = new portfolio();
//		$pf->accessControl("growth_scheme");
//		$user_id = $pf->getCourseUserID($StudentID);
		$sql = "SELECT course_id FROM jr20Sbj.course WHERE RoomType = 4;";
		$rs = $this->ReturnArray($sql);
		$db_name = $this->course_db;
		$NoPhoto = 1;
		$assignment_id = '';

		if ($user_id!="")
		{
			$lo = new libportfolio_group($db_name);
			$workList = $lo->returnFunctionIDs("A",$user_id);
		}
		// GET all assignment
		$sql  = "SELECT a.assignment_id as assignment_id FROM $db_name.assignment AS a ";
		$sql .= "LEFT JOIN $db_name.assignment AS b ON a.assignment_id = b.parent_id ";
		$sql .= "LEFT JOIN $db_name.handin AS c ON (b.assignment_id = c.assignment_id AND c.user_id = '$user_id') ";
		$sql .= "WHERE a.worktype=6 AND a.assignment_id NOT IN ($workList) AND a.status='1' ";
		$sql .= "GROUP BY a.assignment_id ";

		$rs = $this->ReturnArray($sql);

		//get all phase of each assigment
		foreach($rs as $value){
			$assignment_id = $value["assignment_id"];
			$this_path = $path."/".$value["assignment_id"];
			if(!file_exists($this_path))
			{
					$fs->folder_new($this_path);
			}
			$phase_id_array = $this->getSchemePhases_id($assignment_id, $user_id);

			//$url = $eclass_url_path ."/home/portfolio/student/sbs/print_phase.php";
			$url = "/home/portfolio/student/sbs/print_phase.php";


			foreach($phase_id_array as $va){
				$phase_id = $va["phases_id"];

				$formdata = array (
							"burn" => 1,
							"user_id" => $user_id,
							"phase_id" => $phase_id,
							"assignment_id" => $assignment_id,
							"NoPhoto" => 1,
//							"ck_course_id" => $ck_course_id,
//							"ck_user_id" => $ck_user_id,
//							"ck_memberType" => $ck_memberType,
//							"admin_url_path" => $admin_url_path,
//							"ck_helper_job" => $ck_helper_job,
//							"ck_user_course_id" => $ck_user_course_id,
//							"ck_function_rights" => $ck_function_rights,
//							"ck_room_type" => $ck_room_type,
//							"ck_roleType" => $ck_room_type,
//							"ck_user_role" => $ck_user_role,
//							"ck_ole_teacher_right" =>$ck_ole_teacher_right,
//							"ck_login_session_id" =>$ck_login_session_id
				);
				foreach($_SESSION as $key=>$value){
					if(!is_array($value)){
						$formdata[$key] = $value;
					}
				}
				unset($key);
				unset($value);


				$content  = $this ->Posthttp($url ,$formdata);
				$search  = array("../../../../templates/", $image_path, $eclass_url_path ."/src/includes/css/");
				$replace = array("../include/", "../include", "../include/");

				$content = str_replace($search, $replace, $content);
				$content = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">".$content;
				$fs->file_write($content, $path."/".$assignment_id."/".$phase_id.".html");



			}
		}
		unset($value);

	}
	function getSchemePhases_id($my_assignment_id, $user_id=""){
		global $ck_course_id, $ck_default_lang;
		$db_name = $this->course_db;
		$sql  = "
			SELECT
					a.assignment_id as phases_id
			FROM
					$db_name.assignment AS a
					LEFT JOIN $db_name.handin AS h ON (h.assignment_id=a.assignment_id AND h.user_id='{$user_id}')
			WHERE
					a.parent_id='$my_assignment_id'
			GROUP BY
					a.assignment_id
			ORDER BY
					a.starttime, a.deadline
		";
		return $this->ReturnArray($sql);


	}

	function Posthttp($url, $formdata){

		global $ck_course_id, $ck_user_id, $ck_memberType, $ck_user_course_id;
		global $admin_url_path, $ck_ole_teacher_right, $ck_login_session_id;
		global $ck_helper_job, $ck_function_rights, $ck_room_type, $ck_user_role, $ck_roleType;

		$server_ip = $_SERVER["SERVER_NAME"];
		$server_port = $_SERVER["SERVER_PORT"];
		//or .dll, etc. for authnet, etc.
		$x = "";


		//build the post string
		  foreach($formdata AS $key => $val){
		    $poststring .= urlencode($key) . "=" . urlencode($val) . "&";
		  }
		// strip off trailing ampersand
		$poststring = substr($poststring, 0, -1);

		$fp = fsockopen($server_ip, $server_port, $errno, $errstr, 10);

		if(!$fp){
		 //error tell us
		 echo "$errstr ($errno)\n";

		}else{

		  //send the server request
		  fputs($fp, "POST $url HTTP/1.0\r\n");
		  fputs($fp, "Host: $server_ip\r\n");
		  //fputs($fp, "User-Agent: Mozilla 4.0\r\n");
		  fputs($fp, "Content-length: ".strlen($poststring)."\r\n");
		  fputs($fp, "Content-type:application/x-www-form-urlencoded\r\n");
		  fputs($fp, "Connection: Close\r\n\r\n");
		  fputs($fp, $poststring."\r\n\r\n");


		$raw_file_content = "";
    	while (!feof($fp)) {
    		$raw_file_content .= fgets($fp, 128);
    	}
    	fclose($fp);
    	$raw_file_content_array = explode("\r\n\r\n", $raw_file_content);
    	$raw_file_content = explode("\r\n\r\n", $raw_file_content);
    	$file_content = $raw_file_content_array[1];
    	return $file_content;
	   }

	}
	function filename_safe($name) {
	    $except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|','(',')');
	    return str_replace($except, '', $name);
	}


	function analysisFile($fileList,$fileLocation){

	$returnList = array();
	$exceptionList = array();

	//please don't update this exceptionList unless you confirm the change

	$exceptionList[] = '^index';		//iPf LP system file
	$exceptionList[] = '^mypage';		//iPf LP system file
	$exceptionList[] = '^student_u';	//pattern with 'student_u2_wp35'
	$exceptionList[] = 'css$';			 // css file
	$exceptionList[] = 'iportfolio.ico'; // system file
	$exceptionList[] = 'frp_index.html'; // full report file
	$exceptionList[] = 'showhtml.ini';   // auto run file
	$exceptionList[] = 'autorun.inf';    // auto run file
	$exceptionList[] = 'autohtml.exe';   // auto run file
	$exceptionList[] = 'student_comment_menu.html';	//comment file
	$exceptionList[] = '^SBS';						//SBS relate file
	$exceptionList[] = 'school_record.html';		//Student Learning Profile Report
	$exceptionList[] = 'learning_portfolio.html';		//iPf LP system file


	for($f = 0,$f_max = sizeof($fileList);$f < $f_max;$f++) {

		$keep = false;

		$_file = $fileList[$f];

		for($i = 0,$i_max = sizeof($exceptionList);$i < $i_max;$i++){
			$_pattern = '/'.$exceptionList[$i].'/i';
			if(preg_match($_pattern, $_file)){
				$keep = true;
				break;
			}
		}

		if(preg_match('/student_u\d+_wp\d+/',$fileLocation)){
			//do nothing
		}else{
			//if the $fileLocation not include pattern "student_u\d+_wp\d+", that mean it is at the most upper level, don't rename the file
			$keep = true;
		}

		$_renameStatus = ($keep) ? false : true;
		$_urlEncoding = urlencode($_file);

		//replace and "+" in the file to "%20", after first round urlencode, suppose there is no "+" in the $_urlEncoding,
		//if it still can '+' , the '+' come from urlencode() and that mean the file name with space
		//some time urlencode() encode 'space' => '+' , so replace '+' =>'%20' , such that it can replace the file in the HTML content (with '%20' if the image or doc with space
		$_urlEncoding = str_replace("+", "%20", $_urlEncoding);
		// some files fail to change "(", ")" to HTML code
		//$_urlEncoding = str_replace("(", "%28", $_urlEncoding);
		//$_urlEncoding = str_replace(")", "%29", $_urlEncoding);

		$_fullPath = $fileLocation."/".$_file;
		$_isDirectory = (is_dir($_fullPath))? true: false;


		$returnList[$_file] = array('renameStatus'	=> $_renameStatus,
									'name'			=> $_file,
									'urlEncoding'	=> $_urlEncoding,
									'isDirectory'	=> $_isDirectory,
									'location'=>$fileLocation
								);
	}
	return $returnList;
	}
	function makeCopyDirectory($srcDir,$copyDir){

	    if(file_exists($srcDir) && is_dir($srcDir)){
	        $srcDir = OsCommandSafe($srcDir);
	        $copyDir = OsCommandSafe($copyDir);
			$cmd = 'cp -r '.$srcDir.' '.$copyDir;
			exec($cmd,$fileListArr);

			if(file_exists($copyDir)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function clearTmpDirectory($srcDir){

	    if(file_exists($srcDir) && trim($srcDir) != ""){
	        $srcDir = OsCommandSafe($srcDir);
    		$cmd = 'rm -rf '.$srcDir;
    		exec($cmd);
    	}
	}

	function replaceContent($file,$patternList,$replacementList){


		$patternlist_tmp = array();
		$replacementList_tmp = array();

		for($i = 0,$i_max = sizeof($patternList); $i< $i_max; $i++){

			$patternlist_tmp[] = '/'.$patternList[$i].'/';
		}
		$patternList = $patternlist_tmp;
	if(file_exists($file)){
		$_line		=  file_get_contents($file);
		if(trim($_line) != '' ){
			$_newLine	=  preg_replace($patternList,$replacementList,$_line);
//echo 'done after file '.$file.'<br/><br/><br/>';
			file_put_contents($file, $_newLine);
		}
	}
}
function escape_string_for_regex($str)
{
        // \ ^ . $ | ( ) [ ]
        // * + ? { } ,

       $patterns = array('/\//', '/\^/', '/\./', '/\$/', '/\|/', '/\(/', '/\)/', '/\[/', '/\]/', '/\*/', '/\+/','/\?/', '/\{/', '/\}/', '/\,/');
       $replace = array('\/', '\^', '\.', '\$', '\|', '\(', '\)','\[', '\]', '\*', '\+', '\?', '\{', '\}', '\,');


//       $patterns = array('/\//', '/\+/');
//       $replace = array('\/', '\+');



        return preg_replace($patterns,$replace, $str);
}

//	function handleDir($directory,$orgDirectory,&$fileStack,&$renameIdCounter,&$patternList,&$replacementList){  <-- can be delete
	function handleDir($directory,$orgDirectory,&$fileStack,&$renameIdCounter){

	$result = $this->getFileList($directory);

	$resultAnalysis = $this->analysisFile($result,$directory);
	$reNameExceptionList[] = 'student_comment.html';

	foreach($resultAnalysis as $fileName =>$details){

		$_renameRequire = $details['renameStatus'];
		$_isDir = $details['isDirectory'];
		$_urlEncodingName = $details['urlEncoding'];
		$_location = $details['location'];
		$_fullPath = $directory.'/'.$fileName;
		$_orgFullPath = $orgDirectory.'/'.$fileName;

//		echo $fileName.' url encode is -->'.$details['urlEncoding'].'<--<br/>';
		if($_renameRequire){

			$renameIdCounter = $renameIdCounter + 1;

			//this unique Part to prevent the rename file may be original exist in client file structure.
			//ie. car.jpg --> 2.jpg (but 2.jpg may be already exist in client site
			$uniquePart = '_eciPf';

			//get the new file name
			$ext = $this->getFileExtension($fileName);  //get the original file ext
			$_newFileName = ($ext == "")? $renameIdCounter.$uniquePart : $renameIdCounter.$uniquePart.'.'.$ext; //append the ext to the file if needed

			//importance!! override the $_fullPath since file / directory is renamed, prepare for the next level f:handleDir
			$_fullPath = $directory.'/'.$_newFileName;

			//record the new file name back to the fileName $resultAnalysis[$fileName]
			$resultAnalysis[$fileName]['newName'] = $_newFileName;


			if(in_array($fileName,$reNameExceptionList)){
				//file in the exceptionList do nothing

			}else{
				rename($directory.'/'.$fileName , $directory.'/'.$_newFileName);
			}



		}else{
			//since this file no need to rename, "newName" = $fileName (original file name)
			$resultAnalysis[$fileName]['newName'] = $fileName;
		}

		$fileStack[$orgDirectory][] = array($fileName,$resultAnalysis[$fileName]);

		if($_isDir){
			//It is a directory  , go into it and redo the rename over again
			$exceptionList = array();

			//please don't update this exceptionList unless you confirm the change
			$exceptionList[] = 'SBS';
			if(in_array($fileName,$exceptionList)){
				//file in the exceptionList do nothing
			}else{
				$this->handleDir($_fullPath,$_orgFullPath,$fileStack,$renameIdCounter);
			}
		}
	}
	//since the $patternList will be update if a same file name exist in other directory, it will cause replace error.
	//A same file replaced with a other file id in other directory.
	//So, Only do the replacment while the working directory is extractly in pattern "student_u2_wp48" only, after than empty the $patternList and $replacementList
	if(preg_match('/student_u\d+_wp\d+$/',$directory)){

	$patternList2 = array();
	$replacementList2 = array();
	foreach($fileStack as $directory=>$subDirAry){



	$pattern = '/^.*\/student_u\d+_wp\d+\/?(.*)$/';
	$replacement = '${1}';
	$directory2 = preg_replace($pattern,$replacement,$directory);

	$directorPart = explode('/',$directory2);


	$directory3 = "";

//	$patternDirectoryDelimiter = '\/';
	$patternDirectoryDelimiter = '/';
	$replacementDirectoryDelimiter = '/';

	if(trim($directory2) == ''){
		$patternDirectoryDelimiter = '';
		$replacementDirectoryDelimiter = '';
	}
	for($z = 0,$z_max = sizeof($directorPart);$z <$z_max;$z++){
		//$directory3 .= urlencode($directorPart[$z]).'\/';
		$directory3 .= urlencode($directorPart[$z]).$patternDirectoryDelimiter;
	}

//	echo 'dir1 -->'.$directory.'<-- dir2 -->'.$directory2.'<-- url dir3 -->'.$directory3.'<--<br/>';


	for($z = 0,$z_max = sizeof($subDirAry);$z < $z_max;$z++){
		$_element = $subDirAry[$z];

		$_file = $_element[0];
		$_fileDetails = $_element[1];
		$_isDirectory = $_fileDetails["isDirectory"];
		$_urlEncoding = $_fileDetails["urlEncoding"];
		$_location = $_fileDetails["location"];
		$_locationReplace = preg_replace($pattern,$replacement,$_location);
		$_newName = $_fileDetails["newName"];
		if($_isDirectory){
			//do nothing
		}else{


			//$patternList2[] = '([\'"])'.$directory3.$_urlEncoding;
//			$patternList2[] = '([\'"])'.$this->escape_string_for_regex($directory3.$_urlEncoding);


			// replace the space with HTML code (i.e. change "+" to "%20") before global replace
			$directory3 = str_replace("+", "%20", $directory3);

			//&quot; generate by type = weblog

			$patternList2[] = '([\'|"|&quot;])'.$this->escape_string_for_regex($directory3.$_urlEncoding);
			$replacementList2[] = '${1}'.$_locationReplace.$replacementDirectoryDelimiter.$_newName;

			// some clients fail to change special characters in file name to html code (e.g. %20, %28...), therefore rename the html content with original file name (for Burn CD) [CRM : 2012-0302-1442-18071]

			$patternList2[] = '([\'|"|&quot;])'.$this->escape_string_for_regex($directory3.$_file);
			$replacementList2[] = '${1}'.$_locationReplace.$replacementDirectoryDelimiter.$_newName;

		}
	}
	/*
	debug_pr($patternList2);
	debug_pr($replacementList2);
	exit;
	*/


}
		//start the replace content
		foreach($resultAnalysis as $fileName =>$details){
			$ext = strtolower(trim($this->getFileExtension($fileName)));

			$_renameRequire = $details['renameStatus'];
			$_newName = $details['newName'];
			$_location = $details['location'];

			if($_renameRequire){
				$_fName = $_newName;
			}else{
				$_fName = $fileName;
			}
			$_fullPath = $_location.'/'.$_fName;

			if($ext == 'html' || $ext == 'htm'){

				$this->replaceContent($_fullPath,$patternList2,$replacementList2);
			}
		}
		$fileStack = array();
	}


}

function prepareDir($srcDir){

	$copyDir = $srcDir.'_ipf_burncd_tmp';
	$this->clearTmpDirectory($copyDir);

	$copyOK = $this->makeCopyDirectory($srcDir,$copyDir);
	if($copyOK){
		$renameIdCounter = 0;

		//$fileStack store pattern and replacement then use to replace the filename in HTML file
		$fileStack = array();

		$orgDir = $copyDir;
		$this->handleDir($copyDir,$orgDir,$fileStack,$renameIdCounter);

		if(file_exists($copyDir) && is_dir($copyDir)){
		    $srcDir = OsCommandSafe($srcDir);
		    $copyDir = OsCommandSafe($copyDir);
			$cmd = 'rm -rf '.$srcDir.'; mv '.$copyDir.' '.$srcDir;
			exec($cmd);
		}

	}else{
//		echo "COPY ERROR !!!<br/>";
	}

}
function getFileList($location){
    $fileListArr = array();
    $location = OsCommandSafe($location);
	$cmd = 'ls '.$location;
	exec($cmd,$fileListArr);
	return $fileListArr;
}

function getFileExtension($filename){

	if(trim($filename) == ""){
		return "";
	}

	$ext = end(explode('.', $filename));

	if($ext == $filename){
		return "";
	}else{
		return $ext;
	}
}

}
?>