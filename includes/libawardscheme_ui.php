<?php
if (! defined("LIBAWARDSCHEME_UI_DEFINED")) // Preprocessor directives
{
    
    define("LIBAWARDSCHEME_UI_DEFINED", true);

    class libawardscheme_ui extends libawardscheme
    {

        function libawardscheme_ui()
        {
            $this->libawardscheme();
        }

        function GEN_CATEGORY_SELECTION($ParCategory, $ParExtraAttribute = "", $ParAllowAll = false)
        {
            global $award_scheme;
            
            $CategoryArr = $this->GET_CATEGORY_LIST();
            
            $ReturnStr = getSelectByArrayTitle($CategoryArr, "name='Category' id='Category' " . $ParExtraAttribute, ($ParAllowAll ? $award_scheme['all'] : $award_scheme["please_select"]), $ParCategory, false, 2);
            
            return $ReturnStr;
        }

        function GEN_PHASE_SELECTION($ParPhase, $ParExtraAttribute = "", $ParAllowAll = false)
        {
            global $award_scheme;
            
            foreach ($award_scheme["phase_short"] as $PhaseID => $PhaseName)
                $PhaseArr[] = array(
                    $PhaseID,
                    $PhaseName
                );
            
            $ReturnStr = getSelectByArrayTitle($PhaseArr, "name='Phase' id='Phase' " . $ParExtraAttribute, ($ParAllowAll ? $award_scheme['all'] : $award_scheme["please_select"]), $ParPhase, false, 2);
            
            return $ReturnStr;
        }

        function GEN_ITEM_SELECTION($ParItemCode, $ParLevel = "", $ParCategory = "", $ParExtraAttribute = "", $ParAllowAll = false)
        {
            global $award_scheme;
            
            $t_ItemArr = $this->GET_ITEMCODE_LIST($ParLevel, $ParCategory);
            
            for ($i = 0; $i < count($t_ItemArr); $i ++) {
                $ItemArr[] = array(
                    $t_ItemArr[$i]['ItemCode'],
                    $t_ItemArr[$i]['ItemCode']
                );
            }
            
            $ReturnStr = getSelectByArrayTitle($ItemArr, "name='ItemCode' id='ItemCode' " . $ParExtraAttribute, ($ParAllowAll ? $award_scheme['all'] : $award_scheme["please_select"]), $ParItemCode, false, 2);
            
            return $ReturnStr;
        }

        function GEN_TEACHER_SELECTION($ParTeacherID = "", $ParExtraAttribute = "", $ParAllowAll = false)
        {
            global $award_scheme;
            
            $TeacherArr = $this->GET_TEACHER_LIST();
            
            $ReturnStr = getSelectByArrayTitle($TeacherArr, "name='TeacherID' id='TeacherID' " . $ParExtraAttribute, ($ParAllowAll ? $award_scheme['all'] : $award_scheme["please_select"]), $ParTeacherID, false, 2);
            
            return $ReturnStr;
        }

        function GEN_CLASS_SELECTION($ParClass = "", $ParExtraAttribute = "", $ParAllowAll = false)
        {
            global $award_scheme;
            
            $ClassArr = $this->GET_CLASS_LIST();
            
            $ReturnStr = getSelectByArrayTitle($ClassArr, "name='Class' id='Class' " . $ParExtraAttribute, ($ParAllowAll ? $award_scheme['all'] : $award_scheme["please_select"]), $ParClass, false, 2);
            
            return $ReturnStr;
        }

        function GEN_STU_LEVEL_YEAR_SELECTION($ParLevelYear = "", $ParExtraAttribute = "", $ParAllowAll = false)
        {
            global $award_scheme;
            
            $t_LevelYearsArr = $this->GET_STU_LEVEL_YEAR_LIST();
            
            for ($i = 0; $i < count($t_LevelYearsArr); $i ++) {
                $LevelYearsArr[] = array(
                    $t_LevelYearsArr[$i],
                    $t_LevelYearsArr[$i] . "-" . ($t_LevelYearsArr[$i] + 1)
                );
            }
            
            $ReturnStr = getSelectByArrayTitle($LevelYearsArr, "name='LevelYear' id='LevelYear' " . $ParExtraAttribute, ($ParAllowAll ? $award_scheme['all'] : $award_scheme["please_select"]), $ParLevelYear, false, 2);
            
            return $ReturnStr;
        }

        function GEN_ADD_STU_AWARD_CONFIRMATION_TABLE($ParStudentIDs, $ParItemCode)
        {
            global $award_scheme;
            
            $StudentArr = $this->GET_STUDENT_LIST($ParStudentIDs);
            
            $FinalStuItemArr = $this->GET_STUDENT_ITEM_DOWNGRADE($ParStudentIDs, $ParItemCode);
            
            if (is_array($FinalStuItemArr)) {
                foreach ($FinalStuItemArr as $StudentID => $FinalItem) {
                    if ($ParItemCode == $FinalItem[0]) {
                        $FinalStuItemArr[$StudentID] = $this->GET_STUDENT_ITEM_UPGRADE($StudentID, $ParItemCode);
                    }
                }
            }
            
            for ($i = 0; $i < count($StudentArr); $i ++) {
                $ReturnStr .= "
												<tr>
													<td class=\"tabletext\">" . $StudentArr[$i]['ClassNumber'] . "<input type='hidden' name='StudentID[]' value='" . $StudentArr[$i]['UserID'] . "' /></td>
													<td class=\"tabletext\">" . $StudentArr[$i]['StudentName'] . "</td>
													<td class=\"tabletext\">" . $ParItemCode . "<input type='hidden' name='ItemCodeInput[]' value='" . $ParItemCode . "' /></td>
													<td class=\"tabletext\">" . $FinalStuItemArr[$StudentArr[$i]['UserID']][0] . "<input type='hidden' name='ActualItemCode[]' value='" . $FinalStuItemArr[$StudentArr[$i]['UserID']][0] . "' /></td>
													<td class=\"tabletext\">" . ($FinalStuItemArr[$StudentArr[$i]['UserID']][1] != "--" ? $award_scheme["logic"] . " " . substr($FinalStuItemArr[$StudentArr[$i]['UserID']][1], 1, 1) : $FinalStuItemArr[$StudentArr[$i]['UserID']][1]) . "</td>
												</tr>
											";
            }
            
            return $ReturnStr;
        }

        function GEN_AWARD_OVERVIEW_TABLE($ParStudentID = "")
        {
            global $award_scheme, $UserID;
            global $i_general_name, $i_UserClassName, $i_UserClassNumber;
            
            if ($ParStudentID == "")
                $ParStudentID = $UserID;
            
            $level_list = $this->GET_LEVEL_LIST();
            $category_list_temp = $this->GET_CATEGORY_LIST();
            for ($i = 0; $i < count($category_list_temp); $i ++) {
                $category_list[$category_list_temp[$i][0]] = $category_list_temp[$i][1];
            }
            
            $award_list = $this->GET_ITEMCODE_LIST();
            $CurrentCategory = "";
            for ($i = 0; $i < count($award_list); $i ++) {
                if ($award_list[$i]['Category'] != $CurrentCategory) {
                    $CurrentCategory = $award_list[$i]['Category'];
                    $CurrentSubCategory = "";
                    $CurrentItemNumber = "";
                }
                if ($award_list[$i]['SubCategory'] != $CurrentSubCategory) {
                    $RowCount[$award_list[$i]['Category']] ++;
                    $CurrentSubCategory = $award_list[$i]['SubCategory'];
                }
                if ($award_list[$i]['ItemNumber'] != $CurrentItemNumber) {
                    $RowCount[$award_list[$i]['Category']] ++;
                    $CurrentItemNumber = $award_list[$i]['ItemNumber'];
                }
            }
            
            $studentDetail = $this->GET_STUDENT_DETAIL($ParStudentID);
            $ReturnStr = "
											<table border='0' cellspacing='0' cellpadding='3' width='250'>
												<tr>
													<td class=\"tabletext\">" . $i_general_name . ": " . $studentDetail["StudentName"] . "</td>
													<td class=\"tabletext\">" . $i_UserClassName . ": " . $studentDetail["ClassName"] . "</td>
												</tr>
												<tr>
												  <td class=\"tabletext\">" . $award_scheme["strn"] . ": " . $studentDetail["WebSAMSRegNo"] . "</td>
													<td class=\"tabletext\">" . $i_UserClassNumber . ": " . $studentDetail["ClassNumber"] . "</td>
												</tr>
												<tr>
												  <td class=\"tabletext\">" . $award_scheme["current_phase"] . ": " . $studentDetail["CurrentLevel"] . "</td>
													<td class=\"tabletext\">&nbsp;</td>
												</tr>
												<tr>
												  <td class=\"tabletext\" colspan=\"2\">" . $award_scheme["view_date"] . ": " . $studentDetail["ViewTime"] . "</td>
												</tr>
											</table>
										";
            
            // Begin table and header
            $ReturnStr .= "
											<table border='0' cellspacing='1' cellpadding='3' bgcolor='#00000' width='100%'>
												<tr>
													<td bgcolor=\"#EEEEEE\" class=\"tbheading\">" . $award_scheme["category"] . "</td>
										";
            for ($i = 0; $i < count($level_list); $i ++) {
                if ($this->IS_STU_COMPLETE_LEVEL($ParStudentID, $level_list[$i][0]))
                    $ReturnStr .= "<td bgcolor=\"#99FF99\" class=\"tbheading\"><strong>" . str_replace("<!--NoItem-->", $this->UpgradeCriteria[$level_list[$i][0]]["overall"], $award_scheme["phase"][$level_list[$i][0]]) . "</strong></td>";
                else
                    $ReturnStr .= "<td bgcolor=\"#EEEEEE\" class=\"tbheading\">" . str_replace("<!--NoItem-->", $this->UpgradeCriteria[$level_list[$i][0]]["overall"], $award_scheme["phase"][$level_list[$i][0]]) . "</td>";
            }
            $ReturnStr .= "</tr>";
            
            // Main content
            $CurrentCategory = "";
            for ($i = 0; $i < count($award_list); $i ++) {
                if ($award_list[$i]['Category'] != $CurrentCategory) {
                    $CurrentCategory = $award_list[$i]['Category'];
                    $CurrentSubCategory = $award_list[$i]['SubCategory'];
                    $CurrentItemNumber = "";
                    
                    $ReturnStr .= "
													<tr>
														<td bgcolor=\"#FFFFFF\" class=\"tabletext\" rowspan=\"" . $RowCount[$CurrentCategory] . "\">" . $category_list[$CurrentCategory] . "</td>
												";
                    for ($j = 0; $j < count($level_list); $j ++) {
                        if ($this->IS_STU_COMPLETE_SUBCATEGORY($ParStudentID, $level_list[$j][0], $CurrentCategory, $CurrentSubCategory))
                            $ReturnStr .= "<td bgcolor=\"#99FF99\" class=\"tabletext\"><strong>" . str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]) . "</strong></td>";
                        else
                            $ReturnStr .= "<td bgcolor=\"#FFFFFF\" class=\"tabletext\">" . str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]) . "</td>";
                    }
                    $ReturnStr .= "</tr>";
                } else if ($award_list[$i]['SubCategory'] != $CurrentSubCategory) {
                    $CurrentSubCategory = $award_list[$i]['SubCategory'];
                    
                    $ReturnStr .= "<tr>";
                    for ($j = 0; $j < count($level_list); $j ++) {
                        if ($this->IS_STU_COMPLETE_SUBCATEGORY($ParStudentID, $level_list[$j][0], $CurrentCategory, $CurrentSubCategory))
                            $ReturnStr .= "<td bgcolor=\"#99FF99\" class=\"tabletext\"><strong>" . str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]) . "</strong></td>";
                        else
                            $ReturnStr .= "<td bgcolor=\"#FFFFFF\" class=\"tabletext\">" . str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]) . "</td>";
                    }
                    $ReturnStr .= "</tr>";
                }
                
                if ($award_list[$i]['ItemNumber'] != $CurrentItemNumber) {
                    $CurrentItemNumber = $award_list[$i]['ItemNumber'];
                    
                    $ReturnStr .= "<tr>";
                    
                    for ($j = 0; $j < count($level_list); $j ++)
                        $ReturnStr .= "<td bgcolor=\"<!--ItemColour" . $level_list[$j][0] . $CurrentCategory . $CurrentSubCategory . $CurrentItemNumber . "-->\" class=\"tabletext\"><span <!--JSAction" . $level_list[$j][0] . $CurrentCategory . $CurrentSubCategory . $CurrentItemNumber . "-->><!--" . $level_list[$j][0] . $CurrentCategory . $CurrentSubCategory . $CurrentItemNumber . "--></span></td>";
                    
                    $ReturnStr .= "</tr>";
                }
                
                $ReturnStr = str_replace("<!--" . $award_list[$i]['ItemCode'] . "-->", $award_list[$i]['ItemCode'], $ReturnStr);
                $ReturnStr = str_replace("<!--JSAction" . $award_list[$i]['ItemCode'] . "-->", "onMouseOver=\"jUPDATE_DESCRIPTION('" . $award_list[$i]['ItemCode'] . "')\" onMouseOut=\"jHIDE_TOOLTIP()\" style=\"cursor:hand\"", $ReturnStr);
                
                if ($this->IS_STU_COMPLETE_ITEM($ParStudentID, $award_list[$i]['ItemCode']))
                    $ReturnStr = str_replace("<!--ItemColour" . $award_list[$i]['ItemCode'] . "-->", "#99FF99", $ReturnStr);
                else
                    $ReturnStr = str_replace("<!--ItemColour" . $award_list[$i]['ItemCode'] . "-->", "#FFFFFF", $ReturnStr);
            }
            $ReturnStr .= "</table>";
            
            // Legend
            $ReturnStr .= "<br />";
            $ReturnStr .= "
											<table border='0' cellspacing='10' cellpadding='0'>
											  <tr>
													<td width=\"50\" align=\"center\"><span class='tabletextrequire'>*</span></td>
													<td>" . $award_scheme["logic_up"] . "</td>
												</tr>
												<tr>
													<td bgcolor=\"#99FF99\" width=\"50\">&nbsp;</td>
													<td>" . $award_scheme["completed"] . "</td>
												</tr>
											</table>
										";
            
            // End table
            
            // Handle cell with null content
            $ReturnStr = preg_replace("/<!--[a-zA-Z0-9]{4}-->/", "&nbsp;", $ReturnStr);
            $ReturnStr = preg_replace("/<!--ItemColour[a-zA-Z0-9]{4}-->/", "#FFFFFF", $ReturnStr);
            $ReturnStr = preg_replace("/<!--JSAction[a-zA-Z0-9]{4}-->/", "", $ReturnStr);
            
            return $ReturnStr;
        }

        function GEN_CLASS_REPORT($ParClass)
        {
            global $i_UserClassNumber, $i_STRN, $i_UserName;
            global $award_scheme;
            
            $CategoryArr = $this->GET_CATEGORY_LIST();
            $DataArr = $this->GET_DATA_FOR_CLASS_REPORT_PRINT($ParClass);
            $LevelArr = $this->GET_LEVEL_LIST();
            
            $ReturnStr = "<table border='0' cellspacing='0' cellpadding='5'>";
            
            // Header Row
            $ReturnStr .= "
											<tr>
												<td align='center' nowrap>" . $i_UserClassNumber . "</td>
												<td align='center' nowrap>" . $award_scheme["strn"] . "</td>
												<td align='center' nowrap>" . $i_UserName . "</td>
										";
            for ($i = 1; $i <= count($LevelArr); $i ++)
                $ReturnStr .= "<td align='center' nowrap>" . $award_scheme["phase_short"][$i] . "</td>";
            foreach ($CategoryArr as $Category)
                $ReturnStr .= "<td align='center' nowrap>" . $Category[1] . "</td>";
            $ReturnStr .= "</tr>";
            
            for ($i = 0; $i < count($DataArr); $i ++) {
                $CompletedItemArr = $this->GET_STUDENT_COMPLETED_ITEM($DataArr[$i]['UserID']);
                
                for ($j = 0; $j < count($CompletedItemArr); $j ++) {
                    $CompletedItemCountArr[$CompletedItemArr[$j]['Category']] ++;
                }
                
                $ReturnStr .= "<tr>";
                $ReturnStr .= "<td align='center' nowrap>" . $DataArr[$i]['ClassNumber'] . "</td>";
                $ReturnStr .= "<td align='center' nowrap>" . $DataArr[$i]['STRN'] . "</td>";
                $ReturnStr .= "<td align='center' nowrap>" . $DataArr[$i]['StudentName'] . "</td>";
                for ($j = 1; $j <= count($LevelArr); $j ++) {
                    if ($j <= $DataArr[$i]['StudentLevel'])
                        $ReturnStr .= "<td align='center' nowrap>" . $award_scheme["complete"] . "</td>";
                    else
                        $ReturnStr .= "<td align='center' nowrap>&nbsp;</td>";
                }
                
                foreach ($CategoryArr as $Category)
                    $ReturnStr .= "<td align='center' nowrap>" . ($CompletedItemCountArr[$Category[0]] == "" ? 0 : $CompletedItemCountArr[$Category[0]]) . "</td>";
                
                $ReturnStr .= "</tr>";
                
                unset($CompletedItemCountArr);
            }
            
            $ReturnStr .= "</table>";
            
            return $ReturnStr;
        }

        function GEN_CATEGORY_REPORT($ParLevel, $ParCategory)
        {
            global $i_UserClassNumber, $i_STRN, $i_UserName;
            global $award_scheme;
            
            $StudentCount = $this->GET_ALL_STUDENT_COUNT();
            $ItemCodeArr = $this->GET_ITEMCODE_LIST($ParLevel, $ParCategory);
            
            $ReturnStr = "<table border='0' cellspacing='0' cellpadding='5'>";
            
            // Header Row
            $ReturnStr .= "
											<tr>
												<td align='center' nowrap>" . $award_scheme["item_code"] . "</td>
												<td nowrap>" . $award_scheme["item"] . "</td>
												<td nowrap>" . $award_scheme["complete_student"] . "</td>
												<td nowrap>" . $award_scheme["complete_percentage"] . "</td>
											</tr>
										";
            
            for ($i = 0; $i < count($ItemCodeArr); $i ++) {
                $CompletedStudentArr = $this->GET_ITEM_COMPLETED_SUDENT($ItemCodeArr[$i]['ItemCode']);
                
                $ReturnStr .= "<tr>";
                $ReturnStr .= "<td align='center' nowrap>" . $ItemCodeArr[$i]['ItemCode'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $ItemCodeArr[$i]['Description'] . "</td>";
                $ReturnStr .= "<td nowrap>" . count($CompletedStudentArr) . "</td>";
                $ReturnStr .= "<td nowrap>" . sprintf("%.2f", (count($CompletedStudentArr) / $StudentCount * 100)) . " %</td>";
                $ReturnStr .= "</tr>";
            }
            
            $ReturnStr .= "</table>";
            
            return $ReturnStr;
        }

        function GEN_LEVEL_REPORT($ParLevel)
        {
            global $i_UserClassName, $i_UserClassNumber, $i_UserName;
            global $award_scheme;
            
            $StudentArr = $this->GET_DATA_FOR_LEVEL_REPORT_PRINT($ParLevel);
            
            $ReturnStr = "<table border='0' cellspacing='0' cellpadding='5'>";
            
            // Header Row
            $ReturnStr .= "
											<tr>
												<td nowrap>" . $i_UserClassName . "</td>
												<td nowrap>" . $i_UserClassNumber . "</td>
												<td nowrap>" . $award_scheme["strn"] . "</td>
												<td nowrap>" . $i_UserName . "</td>
											</tr>
										";
            
            for ($i = 0; $i < count($StudentArr); $i ++) {
                $ReturnStr .= "<tr>";
                $ReturnStr .= "<td nowrap>" . $StudentArr[$i]['ClassName'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $StudentArr[$i]['ClassNumber'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $StudentArr[$i]['STRN'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $StudentArr[$i]['StudentName'] . "</td>";
                $ReturnStr .= "</tr>";
            }
            
            $ReturnStr .= "</table>";
            
            return $ReturnStr;
        }

        function GEN_BEST_ITEM_REPORT($ParLevel)
        {
            global $award_scheme;
            
            $StudentCount = $this->GET_ALL_STUDENT_COUNT();
            $ItemArr = $this->GET_DATA_FOR_BEST_ITEM_REPORT_PRINT($ParLevel);
            
            $ReturnStr = "<table border='0' cellspacing='0' cellpadding='5'>";
            
            // Header Row
            $ReturnStr .= "
											<tr>
												<td nowrap>" . $award_scheme["ranking"] . "</td>
												<td nowrap>" . $award_scheme["category"] . "</td>
												<td nowrap>" . $award_scheme["item_code"] . "</td>
												<td nowrap>" . $award_scheme["item"] . "</td>
												<td nowrap>" . $award_scheme["complete_student"] . "</td>
												<td nowrap>" . $award_scheme["complete_percentage"] . "</td>
											</tr>
										";
            
            for ($i = 0; $i < count($ItemArr); $i ++) {
                $ReturnStr .= "<tr>";
                $ReturnStr .= "<td nowrap>" . $award_scheme["rank"][$i + 1] . "</td>";
                $ReturnStr .= "<td nowrap>" . $ItemArr[$i]['CategoryName'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $ItemArr[$i]['ActualItemCode'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $ItemArr[$i]['Description'] . "</td>";
                $ReturnStr .= "<td nowrap>" . $ItemArr[$i]['RecordCount'] . "</td>";
                $ReturnStr .= "<td nowrap>" . sprintf("%.2f", ($ItemArr[$i]['RecordCount'] / $StudentCount * 100)) . " %</td>";
                $ReturnStr .= "</tr>";
            }
            
            $ReturnStr .= "</table>";
            
            return $ReturnStr;
        }

        function GEN_IMPORT_STU_AWARD_ERROR_TABLE($ParErrorRows)
        {
            global $award_scheme, $i_UserClassName, $i_UserClassNumber, $i_UserRemark;
            
            $ReturnStr = "
								<table align=\"center\" width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">
									<tr>
										<td width=\"50\" class=\"tabletext\" align=\"center\">" . $award_scheme["line_number"] . "</td>
										<td width=\"15%\" class=\"tabletext\" align=\"center\">" . $i_UserClassName . "</td>
										<td width=\"15%\" class=\"tabletext\" align=\"center\">" . $i_UserClassNumber . "</td>
										<td width=\"15%\" class=\"tabletext\" align=\"center\">" . $award_scheme["item_code"] . "</td>
										<td class=\"tabletext\">" . $i_UserRemark . "</td>
									</tr>
							";
            
            for ($i = 0; $i < count($ParErrorRows); $i ++) {
                // Error message
                switch ($ParErrorRows[$i][4]) {
                    case "itemcode":
                        $warning_msg = $award_scheme["message_itemcode_not_exist"];
                        break;
                    case "studentID":
                        $warning_msg = $award_scheme["message_studentid_not_exist"];
                        break;
                }
                
                $ReturnStr .= "
										<tr>
											<td class=\"tabletext\" align=\"center\">" . $ParErrorRows[$i][0] . "</td>
											<td class=\"tabletext\" align=\"center\">" . $ParErrorRows[$i][1] . "</td>
											<td class=\"tabletext\" align=\"center\">" . $ParErrorRows[$i][2] . "</td>
											<td class=\"tabletext\" align=\"center\">" . $ParErrorRows[$i][3] . "</td>
											<td class=\"tabletext\">" . $warning_msg . "</td>
										</tr>
								";
            }
            
            $ReturnStr .= "</table>";
            
            return $ReturnStr;
        }

        function GEN_IMPORT_STU_AWARD_REMAIN_TABLE($ParRemainRows)
        {
            global $award_scheme, $i_UserClassName, $i_UserClassNumber, $i_UserRemark;
            
            if (count($ParRemainRows) > 0) {
                $ReturnStr = "<table width=\"45%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
                $ReturnStr .= "
									<tr>
										<td width=\"15%\" class=\"tabletext\">" . $i_UserClassName . "</td>
										<td width=\"15%\" class=\"tabletext\">" . $i_UserClassNumber . "</td>
										<td width=\"15%\" class=\"tabletext\">" . $award_scheme["item_code"] . "</td>
									</tr>
								";
                
                for ($i = 0; $i < count($ParRemainRows); $i ++) {
                    $ReturnStr .= "
											<tr>
												<td class=\"tabletext\">" . $ParRemainRows[$i][1] . "</td>
												<td class=\"tabletext\">" . $ParRemainRows[$i][2] . "</td>
												<td class=\"tabletext\">" . $ParRemainRows[$i][0] . "</td>
											</tr>
									";
                }
                
                $ReturnStr .= "</table>";
            } else {
                $ReturnStr = "--";
            }
            
            return $ReturnStr;
        }

        /**
         * *****************************************************
         */
        /**
         * ****************** For PDF Export *******************
         */
        /**
         * *****************************************************
         */
        function GEN_AWARD_OVERVIEW_PDF_EXPORT($ParStudentID = "")
        {
            global $award_scheme, $UserID;
            global $i_general_name, $i_UserClassName, $i_UserClassNumber;
            
            global $pdf;
            
            if ($ParStudentID == "")
                $ParStudentID = $UserID;
            
            $level_list = $this->GET_LEVEL_LIST();
            $category_list_temp = $this->GET_CATEGORY_LIST();
            for ($i = 0; $i < count($category_list_temp); $i ++) {
                $category_list[$category_list_temp[$i][0]] = $category_list_temp[$i][1];
            }
            
            $award_list = $this->GET_ITEMCODE_LIST();
            $CurrentCategory = "";
            for ($i = 0; $i < count($award_list); $i ++) {
                if ($award_list[$i]['Category'] != $CurrentCategory) {
                    $CurrentCategory = $award_list[$i]['Category'];
                    $CurrentSubCategory = "";
                    $CurrentItemNumber = "";
                }
                if ($award_list[$i]['SubCategory'] != $CurrentSubCategory) {
                    $RowCount[$award_list[$i]['Category']] ++;
                    $CurrentSubCategory = $award_list[$i]['SubCategory'];
                }
                if ($award_list[$i]['ItemNumber'] != $CurrentItemNumber) {
                    $RowCount[$award_list[$i]['Category']] ++;
                    $CurrentItemNumber = $award_list[$i]['ItemNumber'];
                }
            }
            
            $studentDetail = $this->GET_STUDENT_DETAIL($ParStudentID);
            
            $pdf->AddUniGBhwFont('uGB', 'AdobeSongStd-Light-Acro');
            // $pdf->AddUniCNShwFont('uni');
            
            $pdf->AddPage();
            $pdf->SetFont('uGB', 'b', 11);
            
            $pdf->Cell(90, 8, $i_general_name . ": " . $studentDetail["StudentName"]);
            $pdf->Cell(90, 8, $i_UserClassName . ": " . $studentDetail["ClassName"], 0, 1);
            $pdf->Cell(90, 8, $award_scheme["strn"] . ": " . $studentDetail["WebSAMSRegNo"]);
            $pdf->Cell(90, 8, $i_UserClassNumber . ": " . $studentDetail["ClassNumber"], 0, 1);
            $pdf->Cell(90, 8, $award_scheme["current_phase"] . ": " . $studentDetail["CurrentLevel"], 0, 1);
            
            $pdf->SetFillColor(238, 238, 238);
            $pdf->Cell(25, 8, $award_scheme["category"], 1, 0, "L", true);
            for ($i = 0; $i < count($level_list); $i ++) {
                if ($this->IS_STU_COMPLETE_LEVEL($ParStudentID, $level_list[$i][0])) {
                    $pdf->SetFillColor(153, 255, 153);
                    $pdf->Cell(55, 8, $award_scheme["phase_short"][$level_list[$i][0]], 1, 0, "L", true);
                } else {
                    $pdf->SetFillColor(238, 238, 238);
                    $pdf->Cell(55, 8, $award_scheme["phase_short"][$level_list[$i][0]], 1, 0, "L", true);
                }
            }
            $pdf->Ln();
            
            // Main content
            $CurrentCategory = "";
            for ($i = 0; $i < count($award_list); $i ++) {
                if ($award_list[$i]['Category'] != $CurrentCategory) {
                    $CurrentCategory = $award_list[$i]['Category'];
                    $CurrentSubCategory = $award_list[$i]['SubCategory'];
                    $CurrentItemNumber = "";
                    
                    $pdf->Cell(25, 8, $category_list[$CurrentCategory], "LTR");
                    for ($j = 0; $j < count($level_list); $j ++) {
                        if ($this->IS_STU_COMPLETE_SUBCATEGORY($ParStudentID, $level_list[$j][0], $CurrentCategory, $CurrentSubCategory)) {
                            $pdf->SetFillColor(153, 255, 153);
                            $pdf->Cell(55, 8, str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]), 1, 0, "L", true);
                        } else {
                            $pdf->SetFillColor(238, 238, 238);
                            $pdf->Cell(55, 8, str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]), 1, 0, "L", true);
                        }
                    }
                    $pdf->Ln();
                } else if ($award_list[$i]['SubCategory'] != $CurrentSubCategory) {
                    $CurrentSubCategory = $award_list[$i]['SubCategory'];
                    
                    $pdf->Cell(25, 8, "", "LR");
                    for ($j = 0; $j < count($level_list); $j ++) {
                        if ($this->IS_STU_COMPLETE_SUBCATEGORY($ParStudentID, $level_list[$j][0], $CurrentCategory, $CurrentSubCategory)) {
                            $pdf->SetFillColor(153, 255, 153);
                            $pdf->Cell(55, 8, str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]), 1, 0, "L", true);
                        } else {
                            $pdf->SetFillColor(238, 238, 238);
                            $pdf->Cell(55, 8, str_replace(array(
                                "<!--SubCategory-->",
                                "<!--ItemNo-->"
                            ), array(
                                $CurrentSubCategory,
                                $this->UpgradeCriteria[$level_list[$j][0]][$CurrentCategory][$CurrentSubCategory]
                            ), $award_scheme["item_complete"]), 1, 0, "L", true);
                        }
                    }
                    $pdf->Ln();
                }
                
                if ($award_list[$i]['ItemNumber'] != $CurrentItemNumber) {
                    $CurrentItemNumber = $award_list[$i]['ItemNumber'];
                    
                    $pdf->Cell(25, 8, "", "LR");
                    
                    for ($j = 0; $j < count($level_list); $j ++) {
                        $CurItemCode = $level_list[$j][0] . $CurrentCategory . $CurrentSubCategory . $CurrentItemNumber;
                        
                        if ($this->IS_ITEM_EXISTS($CurItemCode)) {
                            if ($this->IS_STU_COMPLETE_ITEM($ParStudentID, $CurItemCode)) {
                                $pdf->SetFillColor(153, 255, 153);
                                $pdf->Cell(55, 8, $CurItemCode, 1, 0, "L", true);
                            } else {
                                $pdf->Cell(55, 8, $CurItemCode, 1);
                            }
                        } else {
                            $pdf->Cell(55, 8, "", 1);
                        }
                    }
                    
                    $pdf->Ln();
                }
            }
            
            // Legend
            $pdf->Cell(25, 8, "", "T"); // Bottom border of first column
            $pdf->Ln();
            
            $pdf->SetFillColor(153, 255, 153);
            $pdf->Cell(20, 8, "", 0, 0, "L", true);
            $pdf->Cell(20, 8, $award_scheme["completed"]);
            // End table
            
            /*
             * # Handle cell with null content
             * $ReturnStr = preg_replace("/<!--[a-zA-Z0-9]{4}-->/", "&nbsp;", $ReturnStr);
             * $ReturnStr = preg_replace("/<!--ItemColour[a-zA-Z0-9]{4}-->/", "#FFFFFF", $ReturnStr);
             * $ReturnStr = preg_replace("/<!--JSAction[a-zA-Z0-9]{4}-->/", "", $ReturnStr);
             *
             * return $ReturnStr;
             */
        }
    }
} // End of directives
?>