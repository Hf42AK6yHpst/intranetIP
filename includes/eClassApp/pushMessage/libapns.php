<?php
// editing by 
 
/********************
* change log:
* 2018-04-17 Ronald: modified buildMessagePayloadJsonString(), add apn title and enable mutable-content
* 2015-08-10 Roy: set fromModule = '' and moduleRecordID = '' as default value in buildMessagePayloadJsonString()
* 2015-07-03 Roy: [ip.2.5.6.7.1.0]: add parameter fromModule and moduleRecordID for link up push message to other module
 ********************/

include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
if (!defined("LIBAPNS_DEFINED")) {
	define("LIBAPNS_DEFINED", true);
	
	class libapns extends libdb {
		function libapns() {
			$this->libdb();
		}
		
		function getMaxMessagePayloadByte() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxMessagePayloadByte']['apns'];
		}
		
		function buildMessageJson($notifyMessageId, $messageAry) {
			global $intranet_root, $eclassAppConfig;
			
			//$numOfMessage = count($messageAry);
			
			### consolidate message info
			$consolidatedMessageAry = array();
			foreach ((array)$messageAry as $_apnsType => $_messageAry) {
				foreach ((array)$_messageAry as $__appType => $__appMessageAry) {
					$__messageCount = 0;
					$__numOfMessage = count($__appMessageAry);
					for ($i=0; $i<$__numOfMessage; $i++) {
						$___messageTitle = $__appMessageAry[$i]['messageTitle'];
						$___messageContent = $__appMessageAry[$i]['messageContent'];
						$___userAssoAry = $__appMessageAry[$i]['userAssoAry'];
						$___fromModule = $__appMessageAry[$i]['fromModule'];
						$___moduleRecordID = $__appMessageAry[$i]['moduleRecordID'];
                        $___attachmentPath = $__appMessageAry[$i]['attachmentPath'];

						foreach ((array)$___userAssoAry as $____recipientUserId => $____userInfoAry) {
							$____relatedUserIdAry = $____userInfoAry['relatedUserIdAry'];
							$____deviceIdAry = $____userInfoAry['deviceIdAry'];
							$____numOfDeviceId = count((array)$____deviceIdAry);
							
							for ($j=0; $j<$____numOfDeviceId; $j++) {
								$_____deviceId = $____deviceIdAry[$j];
								
								$consolidatedMessageAry[$_apnsType][$__appType][$__messageCount]['token'] = $_____deviceId;
								$consolidatedMessageAry[$_apnsType][$__appType][$__messageCount]['payload'] = $this->buildMessagePayloadJsonString($notifyMessageId, $___messageTitle, $___messageContent, $____relatedUserIdAry, $___fromModule, $___moduleRecordID, $___attachmentPath);
								
								$__messageCount++;
							}
						}
					}
				}
			}
			
			
			return $consolidatedMessageAry;
		}
		
		function buildMessagePayloadJsonString($notifyMessageId, $messageTitle, $messageContent, $userMappingAry, $fromModule='', $moduleRecordID='', $attachmentUrl='') {
			global $config_school_code, $intranet_root, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();
			
			$maxMessagePayloadByte = $this->getMaxMessagePayloadByte();
			
			$userLoginAry = array_keys((array)$userMappingAry);
			$userLogin = $userLoginAry[0];
			
			$relatedUserIdAry = $userMappingAry[$userLogin]; 
			$messageBody = $messageTitle.": ".$messageContent;
			//$messageBody = $messageTitle;
            $titles = preg_split("/(\r\n|\n|\r)/",$messageTitle);

			$payloadAry = array();
            $payloadAry['aps']['alert']['title'] = $titles[0];
            if(count($titles) > 1){
                $payloadAry['aps']['alert']['subtitle'] = $titles[1];
            }
			$payloadAry['aps']['alert']['body'] = $messageContent;
			$payloadAry['aps']['badge'] = 0;
			$payloadAry['aps']['sound'] = 'default';
			$payloadAry['aps']['mutable-content'] = 1;
            $payloadAry['extras']['sc'] = $config_school_code;
			$payloadAry['extras']['apiV'] = $eclassAppConfig['pushMessage']['apiVersion']['apns'];
			$payloadAry['extras']['rid'] = $notifyMessageId;
			$payloadAry['extras']['ul'] = $userLogin;
			$payloadAry['extras']['rUids'] = $relatedUserIdAry;
			$payloadAry['extras']['fm'] = $fromModule;
			$payloadAry['extras']['mId'] = $moduleRecordID;
			if($attachmentUrl != ''){
                $payloadAry['extras']['attachment-url'] = $attachmentUrl;
                $payloadAry['aps']['content-available'] = 1;
            }

			$payloadJsonString = $jsonObj->encode($payloadAry);
			$payloadJsonStringLength = strlen($payloadJsonString);
			
			if ($payloadJsonStringLength > $maxMessagePayloadByte) {
				//2014-1105-1524-44206
				//$nMaxTextLen = $nTextLen = strlen($messageBody) - ($payloadJsonStringLength - $maxMessagePayloadByte);
				$nMaxTextLen = $nTextLen = strlen($jsonObj->encode($messageBody)) - ($payloadJsonStringLength - $maxMessagePayloadByte);
				if ($nMaxTextLen > 0) {
	            	// auto resize body
	            	// https://code.google.com/p/apns-php/source/browse/trunk/ApnsPHP/Message.php
	            	$nTextLen += 3; 	// for 3 dots
	            	//$messageBodySubStr = mb_substr($messageBody, 0, --$nTextLen, 'UTF-8');
	            	$messageBodySubStr = mb_strcut ( $messageBody , 0, --$nTextLen, 'UTF-8' );
	            	if ($messageBodySubStr != $messageBody) {
	            		$messageBodySubStr .= '...';
	            	}
	               	
	                $payloadAry['aps']['alert']['body'] = $messageBodySubStr;
	                
	                $payloadJsonString = $jsonObj->encode($payloadAry);
	            } else {
	            	// cannot send message due to too large body size
					$payloadJsonString = false;
	            }
			}
			
			return $payloadJsonString;
		}
		
		function saveMessageResult($notifyMessageId, $resultAry, $parNotifyMessageTargetIdAry) {
			global $eclassAppConfig, $intranet_root;
			
			include_once($intranet_root.'/includes/eClassApp/libeClassApp.php');
			$leClassApp = new libeClassApp();
			
			if ($parNotifyMessageTargetIdAry != '') {
				$conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
			}
			
			$successAry = array();
			foreach ((array)$resultAry['result'] as $_apnsType => $_resultAry) {
				foreach ((array)$_resultAry as $__appType => $__appMessageResultAry) {
					if ($__appMessageResultAry['ErrorCode']) {
						$sql = "Update 
										INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
								Set 
										ErrorCode = '".$this->Get_Safe_Sql_Query($__appMessageResultAry['ErrorCode'])."', 
										MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']."',
										DateModified = now()
								Where
										NotifyMessageID = '".$notifyMessageId."'
										And ServiceProvider = 'apns'
										And APNSType = '".$_apnsType."'
										$conds_notifyMessageTargetId
								";
						$successAry[$_apnsType][$__appType]['updateErrorCode'] = $this->db_db_query($sql);
					}
					else {
						$__messageResultAry = $__appMessageResultAry['results'];
						$__numOfMessage = count($__messageResultAry);
						
						// order by recordid for safety
						$sql = "Select RecordID From INTRANET_APP_NOTIFY_MESSAGE_REFERENCE Where NotifyMessageID = '".$notifyMessageId."' And ServiceProvider = 'apns' And APNSType = '".$_apnsType."' $conds_notifyMessageTargetId Order By RecordID";
						$__referenceDataAry = $this->returnResultSet($sql);
						
						
						### consolidate message send status
						$__messageCount = 0;
						$__dbDataAssoAry = array();
						for ($i=0; $i<$__numOfMessage; $i++) {
							$___identifier = $__messageResultAry[$i]['identifier'];
							$___status = $__messageResultAry[$i]['status'];
							$___errorCode = $__messageResultAry[$i]['errorCode'];
							
							$___referenceId = $__referenceDataAry[$__messageCount]['RecordID'];
							if ($___referenceId == '') {
								// for safety only, prevent broken SQL
								continue;
							}
							
							$__messageCount++;
								
							$___messageStatus = '';
							if ($___errorCode != '' || $___status == 0) {
								$___messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
							}
							else {
//								if ($___status == 3) {
//									$___messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'];
//								}
//								else {
									$___messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
//								}
							}
							
							$___tmpAry = array();
							$___tmpAry['MessageID'] = $___identifier;
							$___tmpAry['ErrorCode'] = $___errorCode;
							$___tmpAry['MessageStatus'] = $___messageStatus;
							$__dbDataAssoAry[$___referenceId] = $___tmpAry;
						}
						
						
						### update DB in batch
						$fieldAry = array('MessageID', 'ErrorCode', 'MessageStatus');
						$numOfField = count($fieldAry);
						
						$__dbDataChunkAry = array_chunk($__dbDataAssoAry, 500, true);
						$__numOfChunk = count((array)$__dbDataChunkAry);
						for ($i=0; $i<$__numOfChunk; $i++) {
							$___dbDataAssoAry = $__dbDataChunkAry[$i];
							$___recordIdAry = array_keys($___dbDataAssoAry);
							
							$___setFieldSqlAry = array();
							for ($j=0; $j<$numOfField; $j++) {
								$____dbField = $fieldAry[$j];
								
								$____dbFieldWhenAry = array();
								foreach ((array)$___dbDataAssoAry as $_____recordId => $_____recordDataAry) {
									$_____fieldData = $_____recordDataAry[$____dbField];
									$____dbFieldWhenAry[] = " WHEN $_____recordId THEN '".$this->Get_Safe_Sql_Query($_____fieldData)."' ";
								}
								$___setFieldSqlAry[] = " $____dbField = (CASE RecordID ".implode("\n", (array)$____dbFieldWhenAry)." END) ";
							}
							
							$_sql = "Update INTRANET_APP_NOTIFY_MESSAGE_REFERENCE SET ".implode(', ', (array)$___setFieldSqlAry).", DateModified = now() Where RecordID IN ('".implode("','", (array)$___recordIdAry)."') And MessageStatus != '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']."'";
							$successAry[$_apnsType][$__appType]['updateMessageStatus'][] = $this->db_db_query($_sql);
						}
						
						
						### unregister device if returned by apple
						$__unregDeviceAry = $__appMessageResultAry['feedback']['UnregDeviceToken'];
						$__numOfUnregDevice = count($__unregDeviceAry);
						if ($__numOfUnregDevice > 0) {
							include_once($intranet_root.'/includes/json.php');
						
							for ($i=0; $i<$__numOfUnregDevice; $i++) {
								$___deviceToken = $__unregDeviceAry[$i];
								$successAry[$_apnsType][$__appType]['unregDevice'][$___deviceToken] = $leClassApp->inactivateUserDevice($___deviceToken, '', $eclassAppConfig['deviceOS']['iOS']);
							}
						}
					}
				}
			}
			
			return !in_array(false, $successAry);
		}
	}
}
?>