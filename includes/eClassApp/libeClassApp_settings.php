<?php
// editing by 
 
/********************
 * 2014-11-06 Ivan [ip.2.5.5.12.1]: modified setDefaultSettings() to add default value for eAttendance tap card settings
 ********************/
if (!defined("LIBECLASSAPP_SETTINGS_DEFINED")) {
	define("LIBECLASSAPP_SETTINGS_DEFINED", true);
	
	class libeClassApp_settings extends libdb {
		var $settingsAssoAry;
		
		function libeClassApp_settings() {
			$this->libdb();
		}
		
		function saveSettings($parSettingsAssoAry)
		{
			if (count($parSettingsAssoAry)==0 || !is_array($parSettingsAssoAry)) return false;
			
			$this->Start_Trans();
			$successAry = array();
			
			$sql = "Select SettingsID, SettingName, SettingValue From INTRANET_APP_SETTINGS";
			$curSettingsAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'SettingName');
			
			$insertAry = array();
			foreach ((array)$parSettingsAssoAry as $_settingsName => $_settingsValue) {
				if (isset($curSettingsAssoAry[$_settingsName])) {
					// update
					$sql = "Update INTRANET_APP_SETTINGS 
							Set SettingValue = '".$this->Get_Safe_Sql_Query($_settingsValue)."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' 
							Where SettingsID = '".$curSettingsAssoAry[$_settingsName]['SettingsID']."'";
					$successAry[$_settingsName] = $this->db_db_query($sql);
				}
				else {
					// insert
					$insertAry[] = " ('".$this->Get_Safe_Sql_Query($_settingsName)."', '".$this->Get_Safe_Sql_Query($_settingsValue)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
				}
			}
			
			if (count($insertAry) > 0) {
				$sql = "INSERT INTO INTRANET_APP_SETTINGS (SettingName, SettingValue, DateInput, InputBy, DateModified, ModifiedBy)
						Values ".implode(', ', (array)$insertAry);
				$successAry['insert'] = $this->db_db_query($sql);
			}
			
			if(in_array(false, $successAry)) {
				$this->RollBack_Trans();
				return false;
			}
			else {
				$this->Commit_Trans();
				return true;
			}
		}
	 
		function loadSettings($forceReload=false)
		{
			if ($forceReload || $this->settingsAssoAry == null) {
				$sql = "SELECT SettingName, SettingValue, DateModified FROM INTRANET_APP_SETTINGS";
				$settingsAry = $this->returnResultSet($sql);
				$this->settingsAssoAry = BuildMultiKeyAssoc($settingsAry, "SettingName");
				$this->setDefaultSettings();
			}
		}
		
		function setDefaultSettings(){
			global $intranet_root, $eclassAppConfig;
			
			// default enable eAttendance tap card push message
//			include_once($intranet_root.'/includes/libclass.php');
//			$lclass = new libclass();
//			$formAry = $lclass->getLevelArray();
			$sql = 'Select YearID as ClassLevelID, YearName as LevelName From YEAR Order By Sequence ';
			$formAry = $this->returnResultSet($sql);
			$numOfForm = count($formAry);
			for ($i=0; $i<$numOfForm; $i++) {
				$_classLevelId = $formAry[$i]['ClassLevelID'];
				
				$_settingKey = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_'.$_classLevelId;
				if (!isset($this->settingsAssoAry[$_settingKey])) {
					$this->settingsAssoAry[$_settingKey]['SettingValue'] = 1;
				}
			}
			//initilize student contacts can be shown to class teacher
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact']]['SettingValue'] = 1;
			}
			//initilize student attendance status can be shown in homepage
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']]['SettingValue'] = 1;
			}
			if (!isset($this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']])) {
				$this->settingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']]['SettingValue'] = 1;
			}
		}
		
		function getSettingsValue($settingsName) {
			$this->loadSettings();
			return $this->settingsAssoAry[$settingsName]['SettingValue'];
		}
		
		function getSettingsLastModifiedDate($settingsName) {
			$this->loadSettings();
			return $this->settingsAssoAry[$settingsName]['DateModified'];
		}
	}
}
?>