<?php
// Editing by 

/*************
 * 2020-09-29 Ray: modify getEmailContentHeaderHtml, add $_bgColor
 * 2020-01-23 add getContentCKEditor for displaying HTML email content
 * 2014-07-23 add method getImailImportantAndNotificationHtml,extractEmailAddressFromAngleBrackets,isEmailAddressValide and modify method getImailExternalTrans. modify getEmailListHtml
 * 2014-12-08 add getImailFolderHtml for imail_main list
 * created by qiao
 * 2014-11-13
 * ***************/

include_once($PATH_WRT_ROOT."includes/libdb.php");	
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");
	class emailClassApp{
		var $linterface;
		var $Lang;
		function emailClassApp() {
			$this->linterface  = new interface_html();
		}
		
		function getEmailComposePageHeader($emil_reply_forward_operation_form_action){
		 global $Lang;
		 $refreshIconUrl = $this->linterface->Get_Ajax_Loading_Image($noLang=true);
         $loadingX = $this->getLoadingHtml();  
		 $emailComposePageHeader = '<header data-role ="header" data-position="fixed" style ="background-color:#f6f6f6;border-bottom-width: 0px; z-index:999;">
								  '.$loadingX.'
								  <a  href="#" data-rel="back" data-icon="myapp-delete" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"></a>
								  <p  style="text-align:left; margin-left:30px;" >'."&nbsp&nbsp  ".$Lang['Gamma']['App']['ComposeMail'].'</p>
								  <div class="ui-btn-right" data-role="controlgroup" data-type="horizontal">
								  <a href="#"    id="saveEmailToTrash"  name="Save"  data-icon="myapp-saveAsDraft"     data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" ></a>
								  <a href="#"    id="sendBtn"  name="Send"  data-icon="myapp-send"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;"  ></a>
								  </div>
								  </header>
								  <!-- /header -->            
								  <!-- Home -->
								  <div role="main" class="ui-content" sytle = " padding-left: 0px; padding-right: 0px;" style=" padding-left: 0px; padding-right: 0px; padding-top: 0px;" >
								  <form class="ui-filterable"  id = "email_compose_process" name = "email_compose_process" style = "background-color:#FFFFFF"  method="post" action="'.$emil_reply_forward_operation_form_action.'" enctype="multipart/form-data">';
		return $emailComposePageHeader;			
		}
		
		function getEmailCollipseNTagAddressHtml($fieldsetID,$addressFilterTitle,$tagListUlID,$originalAddressAry){
		$emailCollipseNTagAddressHtml = '<fieldset id = "'.$fieldsetID.'" data-role="collapsible" data-inset="false" data-iconpos="right" style = "background-color:#FFFFFF;width:100%;" >
									     <legend><table style ="width:100%;background-color:#f6f6f6"><tr><td style ="width:10%;background-color:#f6f6f6; vertical-align: top;padding-top: 10px;">'.$addressFilterTitle.'</td>
									     <td style ="width:80%;background-color:#f6f6f6">
									     <ul id = "'.$tagListUlID.'" class = "tagit ui-widget ui-widget-content ui-corner-all" style ="margin-top: 0px;margin-bottom: 0px;">';
									     ##ADD RECEIVER TAG
										 if(count($originalAddressAry)>0){
										   for($i =0;$i<count($originalAddressAry);$i++){
										   	$emailCollipseNTagAddressHtml .='<li id ="'.$originalAddressAry[$i]['UID'].'">'.$originalAddressAry[$i]['Receiver'].'</li>';	
										   }
										 }
        $emailCollipseNTagAddressHtml .='</ul></td></tr></table></legend>'; 
		return $emailCollipseNTagAddressHtml;
		}
		
        function getEmailchosenAddressInitListHtml($ListDivID,$listUlID,$DataInputID,$isAccessInternetMail,$isAccessExternetMail){
        global $Lang;
        
        $firstAddressLayerList = $this->firstAddressLayerListGenerator($isAccessInternetMail,$isAccessExternetMail);
        $emailChosenAddressInitList = '<div id="'.$ListDivID.'">
									<ul id="'.$listUlID.'" data-role="listview" data-filter="true" data-input="'.$DataInputID.'"  data-inset="false">
									'.$firstAddressLayerList.'
									</ul></div>';
		return $emailChosenAddressInitList;
        }
        
        function firstAddressLayerListGenerator($isAccessInternetMail,$isAccessExternetMail){
        	global $Lang;
        	$firstAddressLayerList = '';
        	$ExternetMailChoosenList = '';
            $InternetMailChoosenList = '';
		    if($isAccessExternetMail){
		    $ExternetMailChoosenList = '<li data-name="0"><a href="#" >'.$Lang['Gamma']['SelectFromExternalRecipientGroup'].'</a></li><li data-name="1"><a href="#" >'.$Lang['Gamma']['SelectFromExternalRecipient'].'</a></li>';
		    }
		    if($isAccessInternetMail){
		    $InternetMailChoosenList ='<li data-name="2"><a href="#" >'.$Lang['Gamma']['SelectFromInternalRecipientGroup'].'</a></li><li data-name="3"><a href="#" >'.$Lang['Gamma']['SelectFromInternalRecipient'].'</a></li>';	
		    }
        	$firstAddressLayerList = $ExternetMailChoosenList.$InternetMailChoosenList;
        	return $firstAddressLayerList;
        }
        
        function isShowFoldCcNBccHtml($foldStatus){
        $folderCcAndBcc = '<div class = '.$foldStatus.' id="foldCc">
						   <table style="width: 100%;padding-left: 15px;background-color:#f6f6f6;">
						   <tr>
						   <td style ="width:50px;">CC/BCC:</td>
						   <td><input type="email" name="" id="CcNBccTextClick" value=""></td>
						   <td ></td>
						   </tr></table></div>
						    <!--/Fold CC N BCC -->';
        return $folderCcAndBcc;
        }
        
        function getSubjectHtml($subject){
         global $Lang;
         $subjectHtml = '<table style="width: 100%;padding-left: 15px; background-color:#f6f6f6;">
						   <tr>
						   <td style ="width:50px;">'.$Lang['Gamma']['Subject'].':</td>
						   <td><input type="text" name="Subject" id="Subject" value="'.$subject.'" ></td>
						   <td style ="width:30px;">
						   <label for="attachmentFile" id="upload-file-container" style =" width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;"></label>   
						   <input type="file"  id="attachmentFile" name="attachmentFile" multiple="" style="display: inline; position: fixed; top: 0px; right: 0px; z-index:-1;">
						   </tr></table>';
		return $subjectHtml;			   
        }
        
        function getAttachmentHtml($currentAttachmentAry,$isInitialAttach){
        if($isInitialAttach){
        if(count($currentAttachmentAry)>0){
   	     for($__CACount =0;$__CACount<count($currentAttachmentAry);$__CACount++){
   	   	  $__currentMailAttachment .= '<img src="/images/2009a/iMail/app_view/icon_attachment.png">'.$currentAttachmentAry[$__CACount]['FileName'].'<a href="#"  id="deleteChosenFile"  name="deleteChosenFile"   style = "width: 20px; margin-right: 10px;" ><img src="/images/2009a/iMail/app_view/icon_close.png" id="deleteChosenFile_'.$currentAttachmentAry[$__CACount]['FileName'].'" onClick="delete_attachment_click(this.id)"></a><input type="hidden" id ="'.$currentAttachmentAry[$__CACount]['FileName'].'" name="chosenFiles[]"  value="'.$currentAttachmentAry[$__CACount]['FileName'].'"><br clear="left"/>';   		
   	      }
        }	
        }else{
         $__currentMailAttachment = '';	
        }
        $attathmentHtml = '<div id="selectedFiles"  style = "margin-top:10px;">'.$__currentMailAttachment.'</div>';
		return $attathmentHtml;			   
        }
        
        function getContentHtml($currentMailShowedContent){
        $contentHtml = '<textarea id="emailMainBody" name = "emailMainBody">'.
                        strip_tags($currentMailShowedContent['ContentTitleMessage'])."\n".
						strip_tags($currentMailShowedContent['ContentSenderMessage'])."\n".
						strip_tags($currentMailShowedContent['ContentDateMessage'])."\n".
						strip_tags($currentMailShowedContent['Content'])."\n".
                        '</textarea>';	
		return $contentHtml;			   
        }
		
        function getContentCKEditor($currentMailShowedContent){
			$msg = $currentMailShowedContent['ContentTitleMessage']."<br/>".
			       $currentMailShowedContent['ContentSenderMessage']."<br/>".
				   $currentMailShowedContent['ContentDateMessage']."<br/>".
				   $currentMailShowedContent['Content']."<br/>";
			$contentHtml = getCkEditor('emailMainBody', $msg, 'imail_app_webview', '400px');
			return $contentHtml;
        }
        
        function getHiddeenInputHtml($ExternalCcAddresses,$InternalCCIDs){
        $hiddenInputHtml = '<input type="hidden" id ="myReceiverLatestInputAddress" name="myReceiverLatestInputAddress">
						  <input type="hidden" id ="myCcLatestInputAddress" name="myCcLatestInputAddress" >
						  <input type="hidden" id ="myBccLatestInputAddress" name="myBccLatestInputAddress" >    
						      
						  <input type="hidden" id ="chosenRecipientIDs" name="chosenRecipientIDs" >
						  <input type="hidden" id ="chosenInternalCCIDs" name="chosenInternalCCIDs" value="'.$InternalCCIDs.'">
						  <input type="hidden" id ="chosenInternalBCCIDs" name="chosenInternalBCCIDs" >     
						  <input type="hidden" id ="chosenExternalRecipientAddresses" name="chosenExternalRecipientAddresses" >
						  <input type="hidden" id ="chosenExternalCcAddresses" name="chosenExternalCcAddresses" value="'.$ExternalCcAddresses.'">
						  <input type="hidden" id ="chosenExternalBCCAddresses" name="chosenExternalBCCAddresses" >
						  
						  <input type="hidden" id ="chosenFromEmailAddressListGenerator" name="chosenFromEmailAddressListGenerator" >
						  <input type="hidden" id ="chosenCCFromEmailAddressListGenerator" name="chosenCCFromEmailAddressListGenerator" >
						  <input type="hidden" id ="chosenBCCFromEmailAddressListGenerator" name="chosenBCCFromEmailAddressListGenerator" >
						  
						  <input type="hidden" id ="chosenUploadedAttachmentLink" name="chosenUploadedAttachmentLink">';
		return	$hiddenInputHtml;	 		  
        }
        
        function getLoadingHtml(){
        $loadingHtml = '<div id="loadingmsg" style="display: none;"><br><br></div>
						<div id="loadingover" style="display: none;"></div>';
        return $loadingHtml;
        }
        
        function getEmailListHtml($_unSeen,$currentUID,$_hasAttachment,$subjectPrint,$mailbox,$prettydate,$myAttachmentIconUrl,$isImportant = false,$importantFlagUrl ='',$IsNotification = false,$notificationFlagUrl=''){
        if($_unSeen){
        $__currentBGClass = "unSeen";
   		$__fontweight ="bold"; 	
        }else{
        $__currentBGClass = "seen";
 	   	$__fontweight ="normal";	
        }
        
        $emailListHtml .= "<li data-icon='false' class='".$__currentBGClass."' value='".$__currentBGClass."' style='width:100% !important;'>";
        $emailListHtml .= "<div class = 'ui-checkbox' style = 'margin-top: 0px;margin-bottom: 0px;'><label style='padding: 20px 10px 20px 10px !important;margin: 0px 0px 0px 0px !important;border-width: 0px 1px 0px 0px !important;height:30px;float:left;width:30px;' data-corners='false'>
                           <fieldset data-role='controlgroup' style='height:0px !important'><input type='checkbox' id='checkbox_".$currentUID."' data-enhanced='true' name='emailListChoseEmailBox[]'  style='margin-top: 30px;' value = '".$currentUID."' onClick='' onchange = 'AutoCalculateMandateOnChange()'/></fieldset>	
					       </label></div>";
        $emailListHtml .= "<a href='#' id = '".$currentUID."'  onclick='getEmailDetails(this.id);' style='padding-left: 0px;background: transparent !important;font-weight: $__fontweight!important;'><div class='my_email_title'>";
		if($isImportant){
	    	$emailListHtml .= "<input type='image' id ='importantEmail'  src=$importantFlagUrl >";
	    }
	    if($IsNotification){
	    	$emailListHtml .= "<input type='image' id ='notifiedEmail'  src=$notificationFlagUrl >";
	    }
        if($_hasAttachment){
	        $emailListHtml .= "<input type='image' id ='markEmailAttachment'  src=$myAttachmentIconUrl >";
	    }
	    $emailListHtml .= "$mailbox</div><p class = 'ui-li-text-my-h5' style = 'margin-bottom:0px'>".$subjectPrint."</p><p class='ui-li-aside ui-li-text-my-h5' style='right:1em;'>"
	    .$prettydate."</p></a></li>"; 
	    
	    return $emailListHtml;
        }
        
        function getMoveToHtml($moveToFolderAry,$originalFolderTag){
        global $Lang;
             $moveToHtml = "<select name='select-moveto-folder' id='select-moveto-folder' onchange='changeEmailFolder(this);'>";
			 $moveToHtml .= "<option>".$Lang['Gamma']['App']['MoveTo']."</option>";
			 for($i=0;$i<count($moveToFolderAry);$i++){
             $__currentTargetFolder = $moveToFolderAry[$i]['TargetFolderName'];
        	 $__currentTargetDisplayFolderName = $moveToFolderAry[$i]['TargetDisplayFolderName'];
        	 if(($__currentTargetFolder!= $originalFolderTag)&&($__currentTargetDisplayFolderName!='')){
        	  $moveToHtml .="<option value='".$__currentTargetFolder."'>".$__currentTargetDisplayFolderName."</option>";
        	 }
        }
             $moveToHtml .= "</select>";	
        return 	$moveToHtml;
        }
        function getEmailContentHeaderHtml($_displayName,$moveToHtml, $_bgColor = ''){
        	global $sys_custom;
        	
        $loadingX = $this->getLoadingHtml();
        $deleteComfirmPopupHtml = $this->getDeleteComfirmPopupHtml();
        if($_bgColor == '') {
        $bgColor = ($sys_custom['eClassTeacherApp']['iMailToolbarColorCode'])? $sys_custom['eClassTeacherApp']['iMailToolbarColorCode'] : '#429DEA';
		} else {
			$bgColor = $_bgColor;
		}
        $emailContentHeaderHtml = '<header data-role ="header"  data-position="fixed"  style ="background-color:'.$bgColor.';">
                                  '.$loadingX.'
								 <a  href="#" data-icon="myapp-myOutbox" data-role="button" class = "ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon ui-btn-left" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 10px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" onclick ="getEmailListPage();"></a>
								 <p  style="text-align:left; margin-left:30px;margin-top: 18px;" class = "ui-li-text-my-title-font">'."&nbsp&nbsp".$_displayName.'</p>
								 <fieldset  class="ui-btn-right" data-role="controlgroup" data-type="horizontal" style="right: 0px;!important" >
								 <div class="ui-controlgroup-controls" style="font-size:0;!important">
								 '.$moveToHtml.'
								 <a href="#" data-icon="myapp-reply"    id = "Reply" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;left:0px" onclick = "ComposeEmail(this.id);"></a>
								 <a href="#" data-icon="myapp-replyAll" id = "ReplyAll" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 2px;left:2px"  onclick = "ComposeEmail(this.id);"></a>
								 <a href="#" data-icon="myapp-forward"  id = "Forward" data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 0px;" onclick = "ComposeEmail(this.id);"></a>
								 <a href="#popupDeleteComfirm" data-icon="myapp-trash"    data-rel ="popup"  data-position-to="window" data-role="button"  class =" ui-corner-all ui-shadow ui-btn-inline  ui-btn-a ui-nodisc-icon" style="width:36px!important;height:36px!important;background-size: 36px 36px!important;top: 5px;padding-bottom: 0px;padding-top: 0px;padding-left: 0px;padding-right: 1px;"  data-transition="pop"></a>
								 '.$deleteComfirmPopupHtml.'
                                 </div>
								 </fieldset>   
								 </header>';
        return $emailContentHeaderHtml;
        }
        
        function getDeleteComfirmPopupHtml(){
        global $Lang;
        $deleteComfirmPopupHtml =  '<div data-role="popup" id="popupDeleteComfirm" data-theme="b" data-dismissible="false" style="max-width:400px;">
									<div data-role="header" data-theme="a">
									<h1>'.$Lang['Gamma']['App']['DeleteEmailHeader'].'</h1>
									</div>
									<div role="main" class="ui-content">
								    <h3 class="ui-title">'.$Lang['Gamma']['App']['DeleteEmailTitle'].'</h3>
									<div align="center">								   
									<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back">'.$Lang['Gamma']['App']['DeleteEmailCancel'].'</a>
								    <a href="#" id="comfirmDeleteEmails"  class="ui-btn  ui-corner-all ui-shadow ui-btn-inline ui-btn-b" >'.$Lang['Gamma']['App']['DeleteEmailComfirm'].'</a>
								    </div></div></div>';

        return $deleteComfirmPopupHtml;
        }        
        function getNameLanSql($parLang){
        if($parLang=='en'){
        	$nameLan = "EnglishName";
    	}else{
    		$nameLan = "ChineseName";
    	}
    	return $nameLan;
        }
        
        function multiexplode ($delimiters,$string) {
        return explode($delimiters[0],str_replace($delimiters,$delimiters[0],$string)); 
        }
        
        function getImailExternalTrans($externalAryString,$ReceiverTag,$UIDTag){
        $resultAry = array();
        $externalAry = array();
        $externalAry = $this->multiexplode(array(",",";"),$externalAryString);
        $externalAry = array_filter($externalAry);
        $tempAry = array();
        for($i = 0;$i<count($externalAry);$i++){
        	$tempMail = $this->extractEmailAddressFromAngleBrackets($externalAry[$i]) == ""?$externalAry[$i]:$this->extractEmailAddressFromAngleBrackets($externalAry[$i]);
        	$isEmailAddressValide = $this->isEmailAddressValide($tempMail);
        	if($isEmailAddressValide){
	        	$tempAry[$ReceiverTag] = $tempMail;
	        	$tempAry[$UIDTag] = -1;
	        	$resultAry[] = $tempAry;
        	}
        }     
        return $resultAry;
        }
        
        function extractEmailAddressFromAngleBrackets($str) 
		{ 
		$result = array(); 
		preg_match_all("/(?:<)(.*)(?:>)/i",$str, $result); 
		return $result[1][0]; 
		}
		
		function isEmailAddressValide($emailaddress){
			$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
			
			if (preg_match($pattern, $emailaddress) === 1) {
			 return true; 
			}
			return false;
		}

        function getImailInternalTrans($internalAryString,$ReceiverTag,$UIDTag,$parLang,$lc){
        $resultAry = array();
        $nameLan = $this->getNameLanSql($parLang);
        $internalAryString =  str_replace("U","",$internalAryString);
        $internalIDAry = explode(",", $internalAryString);
        $internalIDAry = array_filter($internalIDAry);
        $tempInternalAry = array();
        $tempInternalMailAry = array();
        for($j= 0;$j<count($internalIDAry);$j++){
        $sql  = "select ".$nameLan.",UserEmail from INTRANET_USER where UserID ='$internalIDAry[$j]'";	
        $tempInternalMailAry = $lc->returnArray($sql);
        if($tempInternalMailAry[0][$nameLan]!=null){
        $tempInternal = $tempInternalMailAry[0][$nameLan]."<".$tempInternalMailAry[0]['UserEmail'].">";	
        }else{
        $tempInternal = $tempInternalMailAry[0]['UserEmail'];
        }
        $tempInternalAry[$ReceiverTag] = $tempInternal;
    	$tempInternalAry[$UIDTag] = $internalIDAry[$j];
    	$resultAry[] = $tempInternalAry;
        }
        return $resultAry;
        }  
        
        function getImailFolderHtml($folderID,$folderImagePath,$folderDisplayName){
        	$imailFolderHtml = "<li data-icon='false' style ='background-color: white !important;'><a href='#'  id = '".$folderID."' style ='background-color: transparent !important;padding-left:60px;padding-top:0px;height: 60px;padding-bottom: 0px;' onclick = 'getFolderEmailList(this.id);'>
			<img src=".$folderImagePath." style = 'height: 50px;width:50px!important;background-size: 50px 50px!important;top:5px;'>
			<h1 style='margin-top:20px'>".$folderDisplayName."</h1></a>
			</li>"."\r\n";
			return $imailFolderHtml;
        }
        
      function getImailImportantAndNotificationHtml($initialImportant,$importantFlagUrl,$isNotificationUrl){
      	global $Lang;
      	 $imailImportantAndNotificationHtml .= '<table style="width:100%"><tr><td>'.
	        '<input type="checkbox" name="ImportantFlag" id="CheckImportant" value ="1" '.$initialImportant.'> <label for="CheckImportant" style="background-color: white;border-top-width: 0px;border-right-width: 0px;border-left-width: 0px;border-bottom-width: 0px;"><img src='.$importantFlagUrl.' align="absmiddle" border="0" alt="'.$Lang['Gamma']['App']['MarkMailImportant'].'">'.$Lang['Gamma']['App']['MarkMailImportant'].'</label>'.
	        '<input type="checkbox" name="IsNotification" id="CheckNotification" value ="1" checked> <label for="CheckNotification" style="background-color: white;border-top-width: 0px;border-right-width: 0px;border-left-width: 0px;border-bottom-width: 0px;"><img src='.$isNotificationUrl.' align="absmiddle" border="0" alt="'.$Lang['Gamma']['App']['NotifyReceiver'].'">'.$Lang['Gamma']['App']['NotifyReceiver'].'<span style="color:gray;font-weight: normal;"> '.$Lang['Gamma']['App']['NotificationOnlyForInternal'].'</span></label>'.
	        '</td></tr></table>';
        
        return $imailImportantAndNotificationHtml;
      }
}

?>
