<?php
// editing by
if (!defined("LIBECLASSAPP_REPRINTCARD_DEFINED")) {
    define("LIBECLASSAPP_REPRINTCARD_DEFINED", true);

    class libeClassApp_reprintCard extends libdb {
        function libeClassApp_reprintCard() {
            $this->libdb();
            $this->ModuleTitle = 'eClassApp_reprintCard';
        }

        function ws_addReprintCardRecord($ApplicantUserID, $StudentUserID, $Reason, $ImageData, $DeliveryMethod, $DeliveryAddress, $DeliveryDistrict, $DeliveryName, $DeliveryTelNum)
        {
            global $PATH_WRT_ROOT,$intranet_root,$intranet_rel_path;

            // retrieve UserID (parent + active)
            $sql = "select UserID, UserLogin from INTRANET_USER where UserID='" . $ApplicantUserID . "' and RecordType=3";
            $rs = $this->returnResultSet($sql);
            $ParentUserID = $rs [0] ['UserID'];

            // retrieve UserID (student + active)
            $sql = "select UserID, UserLogin, STRN, Barcode from INTRANET_USER where UserID='" . $StudentUserID . "' and RecordType=2";
            $rs = $this->returnResultSet($sql);
            $StudentUserID = $rs [0] ['UserID'];
            $StudentSTRN = $rs [0] ['STRN'];
            $StudentBarcode = $rs [0] ['Barcode'];

            // check user exists
            if (!$ParentUserID || !$StudentUserID) {
                return 104;
            }

            //double check if cardstatus is 0,1,2,3 will not new a record.

            $sql = "Select
						r.RecordID
				From
						REPRINT_CARD_RECORD as r
				Where
						r.StudentID = '" . $StudentUserID . "'
						And r.CardStatus IN (0,1,2,3)     
				";

            $result = $this->returnArray ( $sql );
            $numOfRecord = count ( $result );

            if($numOfRecord!=0){
                return "exist record";
            }

            $successAry = array();
            $recordId = '';
            $filePathBase = $intranet_root;

            //get payment fee from central server
            $paymentFee = $this->getPaymentFee($DeliveryDistrict);
            $paymentItemFee = $paymentFee['ReprintCardPrize'];
            $paymentDeliverFee = $paymentFee['MailPrice'];
            $paymentAmountFee = $paymentItemFee + $paymentDeliverFee;

            //get PPS
            $sql = "select PPSAccountNo from PAYMENT_ACCOUNT where StudentID='" . $StudentUserID . "'";
            $rs = $this->returnResultSet($sql);
            $StudentPPS = $rs [0] ['PPSAccountNo'];

            $sql = "Insert Into REPRINT_CARD_RECORD 
							(StudentID, StudentSTRN, StudentBarcode, StudentPPSAccountNo,ReprintCardReason, DeliveryMethod, DeliveryName, DeliveryTelNum, DeliveryAddress, DeliveryDistrict, PaymentItemFee, PaymentDeliverFee, PaymentAmountFee, PaymentItem, CardStatus, ApplyDate, InputDate, InputBy, ModifiedDate, ModifiedBy)
						Values
							('" . $StudentUserID . "', '" . $StudentSTRN . "','" . $StudentBarcode . "','" . $StudentPPS . "','" . $this->Get_Safe_Sql_Query($Reason)  . "', '" . $DeliveryMethod . "', '" .  $this->Get_Safe_Sql_Query($DeliveryName) . "', '" .  $this->Get_Safe_Sql_Query($DeliveryTelNum) . "', '" .  $this->Get_Safe_Sql_Query($DeliveryAddress) . "', '" .  $this->Get_Safe_Sql_Query($DeliveryDistrict)  . "',  '" . $paymentItemFee . "',  '" . $paymentDeliverFee . "',  '" . $paymentAmountFee . "', 'REPRINTCARD', 0 , now(), now(), '" . $ParentUserID . "', now(), '" . $ParentUserID . "')
						";

            $successAry ['insertReprintCardRecord'] = $this->db_db_query($sql);


            if ($successAry ['insertReprintCardRecord']) {

                $recordId = $this->db_insert_id();

                /*
                // send push message handle teacher

                $sql = "Select
										yct.UserID
								From
										YEAR_CLASS_USER as ycu
										Inner Join YEAR_CLASS_TEACHER as yct ON (ycu.YearClassID = yct.YearClassID)
										Inner Join YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID)
										Inner Join INTRANET_USER as teacher ON (yct.UserID = teacher.UserID)
								Where
										ycu.UserID = '" . $StudentUserID . "'
										And teacher.RecordStatus = 1
										And yc.AcademicYearID = '" . Get_Current_Academic_Year_ID () . "'
								Group By
										yct.UserID
								";
                $teacherIdAry = $libeClassApp->returnVector ( $sql );
                $numOfTeacher = count ( $teacherIdAry );

                $teacherIndividualMessageInfoAry = array ();
                $teacherAssoAry = array ();
                for($i = 0; $i < $numOfTeacher; $i ++) {
                    $_teacherId = $teacherIdAry [$i];
                    $_targetTeacherId = $libeClassApp->getDemoSiteUserId ( $_teacherId );

                    // link the message to be related to oneself
                    $teacherAssoAry [$_teacherId] = array (
                        $_targetTeacherId
                    );
                }

                $messageTitle = $Lang ['StudentAttendance'] ['ApplyLeaveAry'] ['NotificationAry'] ['StudentLeaveApplicationTitle'];
                $messageContent = $Lang ['StudentAttendance'] ['ApplyLeaveAry'] ['NotificationAry'] ['StudentLeaveApplicationContent'];
                $messageContent = str_replace ( '<!--studentNameEnWithClass-->', $objUser->EnglishName . ' (' . $objUser->ClassName . '-' . $objUser->ClassNumber . ')', $messageContent );
                $messageContent = str_replace ( '<!--studentNameChWithClass-->', $objUser->ChineseName . ' (' . $objUser->ClassName . '-' . $objUser->ClassNumber . ')', $messageContent );
                $messageContent = str_replace ( '<!--startDateTime-->', date ( 'Y-m-d', strtotime ( $StartDate ) ) . ' (' . $StartDateType . ')', $messageContent );
                $messageContent = str_replace ( '<!--endDateTime-->', date ( 'Y-m-d', strtotime ( $EndDate ) ) . ' (' . $EndDateType . ')', $messageContent );
                $messageContent = str_replace ( '<!--reason-->', $Reason, $messageContent );
                $messageContent = str_replace ( '<!--numOfDays-->', $Duration, $messageContent );

                $teacherIndividualMessageInfoAry [0] ['relatedUserIdAssoAry'] = $teacherAssoAry;
                $successAry ['sendPushMessageClassTeacher'] = $libeClassApp->sendPushMessage ( $teacherIndividualMessageInfoAry, $messageTitle, $messageContent, 'N', $recordStatus = 0, $eclassAppConfig ['appType'] ['Teacher'] );
                */
            }

            if ($recordId > 0 ){
                if($ImageData){

                    include_once ($PATH_WRT_ROOT . 'includes/libfilesystem.php');
                    $lfs = new libfilesystem ();
                    $_folderDivision = $this->returnFolderDivisionNum ( $recordId );

                    $_filePath = $filePathBase . '/file/eClassApp/ReprintCard/' . $_folderDivision . '/' . $recordId . '_' . date ( 'Ymd_His' ). '.jpg';

                    $_folderPath = $filePathBase . '/file/eClassApp/ReprintCard/' . $_folderDivision . '/';
                    if (! file_exists ( $_folderPath )) {
                        $successAry ['createRootFolder'] = $lfs->folder_new ( $_folderPath );
                    }

                    $_imageBinary = base64_decode ( $ImageData );
                    $successAry ['uploadFile'] = $lfs->file_write ( $_imageBinary, $_filePath );
                    if ($successAry ['uploadFile']) {
                        $_dbFilePath = str_replace ( $filePathBase, '', $_filePath );
                        $sql = "Update 
										REPRINT_CARD_RECORD 
								Set 
										PhotoPath = '" . $this->Get_Safe_Sql_Query ( $_dbFilePath ) . "',
										ModifiedDate = now(),
										ModifiedBy = '" . $ParentUserID . "'
								Where
										RecordID = '" . $recordId . "'
								";
                        $successAry ['updateAttachmentRecord'] = $this->db_db_query ( $sql );
                    }

                }else{
                    include_once($intranet_root.'/includes/libuser.php');
                    $lu = new libuser();
                    $photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($StudentUserID);
                    $photoPath = $photo[1];
                    $photoPath = str_replace ( $intranet_rel_path, '', $photoPath );
                    if(strpos($photoPath, 'samplephoto_official')==false){
                        $sql = "Update 
										REPRINT_CARD_RECORD 
								Set 
										PhotoPath = '" . $this->Get_Safe_Sql_Query ( $photoPath ) . "',
										ModifiedDate = now(),
										ModifiedBy = '" . $ParentUserID . "'
								Where
										RecordID = '" . $recordId . "'
								";
                        $successAry ['updateAttachmentRecord'] = $this->db_db_query ( $sql );
                    }
                }
            }


            $returnAry = array();
            $returnAry ['RecordID'] = $recordId;
            return $returnAry;
        }

        function ws_updateReprintCardRecordDelivery($RecordID, $DeliveryMethod, $DeliveryAddress, $DeliveryDistrict, $DeliveryName, $DeliveryTelNum)
        {
            //get payment fee from central server
            $paymentFee = $this->getPaymentFee($DeliveryDistrict);
            $paymentItemFee = $paymentFee['ReprintCardPrize'];
            $paymentDeliverFee = $paymentFee['MailPrice'];
            $paymentAmountFee = $paymentItemFee + $paymentDeliverFee;


            $sql = "Update 
							REPRINT_CARD_RECORD
					Set
							DeliveryMethod = '" . $DeliveryMethod . "',
							DeliveryName = '" . $this->Get_Safe_Sql_Query ( $DeliveryName ) . "',
							DeliveryTelNum = '" . $this->Get_Safe_Sql_Query ( $DeliveryTelNum ) . "',
							DeliveryAddress = '" . $this->Get_Safe_Sql_Query ( $DeliveryAddress ) . "',
							DeliveryDistrict = '" . $this->Get_Safe_Sql_Query ( $DeliveryDistrict ) . "',
							PaymentItemFee = '" . $paymentItemFee . "',
							PaymentDeliverFee = '" . $paymentDeliverFee . "',
							PaymentAmountFee = '" . $paymentAmountFee . "',
							ModifiedDate = now(),
							ModifiedBy = '" . $_SESSION['UserID'] . "'
					Where
							RecordID = '" . $RecordID . "'
					";
            $result = $this->db_db_query ( $sql );

            $returnAry = array();
            $returnAry ['UpdateSuccess'] = $result;
            return $returnAry;
        }

        function ws_cancelReprintCardRecord($RecordID)
        {
            $sql = "Update 
							REPRINT_CARD_RECORD
					Set
							CardStatus = '6',
							CancelDate = now(),
							ModifiedDate = now(),
							ModifiedBy = '" . $_SESSION['UserID'] . "'
					Where
							RecordID = '" . $RecordID . "'
					";
            $result = $this->db_db_query ( $sql );

            $returnAry = array();
            $returnAry ['CancelSuccess'] = $result;
            return $returnAry;
        }

        function ws_getCurrentReprintCardRecord($StudentUserID){

            $sql = "Select
						r.RecordID, r.StudentID, r.PhotoPath, r.RejectReason, r.ReprintCardReason, r.DeliveryMethod, r.DeliveryName, r.DeliveryTelNum, r.DeliveryAddress, r.DeliveryDistrict, r.PaymentItemFee, r.PaymentDeliverFee,r.PaymentAmountFee, r.PaymentMethod, r.PaymentDate, r.ReferenceNumber, r.CardStatus, r.ApplyDate
				From
						REPRINT_CARD_RECORD as r
				Where
						r.StudentID = '" . $StudentUserID . "'
						And r.CardStatus IN (0,1,2,3)
				Order By r.ApplyDate desc
				limit 1        
				";

            $result = $this->returnArray ( $sql );

            $numOfRecord = count ( $result );

            $ReturnAry ['CurrentReprintCardRecord'] = array ();
            for($i = 0; $i < $numOfRecord; $i ++) {
                $ReturnAry ['CurrentReprintCardRecord'] [] = array (
                    'RecordID' => $result [$i] ['RecordID'],
                    'StudentID' => $result [$i] ['StudentID'],
                    'PhotoPath' => is_null($result [$i] ['PhotoPath'])? '' : $result [$i] ['PhotoPath'],
                    'RejectReason' => is_null($result [$i] ['RejectReason'])? '' : $result [$i] ['RejectReason'],
                    'ReprintCardReason' => $result [$i] ['ReprintCardReason'],
                    'DeliveryMethod' => $result [$i] ['DeliveryMethod'],
                    'DeliveryName' => $result [$i] ['DeliveryName'],
                    'DeliveryTelNum' => $result [$i] ['DeliveryTelNum'],
                    'DeliveryAddress' => $result [$i] ['DeliveryAddress'],
                    'DeliveryDistrict' => $result [$i] ['DeliveryDistrict'],
                    'PaymentItemFee' => $result [$i] ['PaymentItemFee'],
                    'PaymentDeliverFee' => $result [$i] ['PaymentDeliverFee'],
                    'PaymentAmountFee' => $result [$i] ['PaymentAmountFee'],
                    'PaymentMethod' => $result [$i] ['PaymentMethod'],
                    'PaymentDate' => is_null($result [$i] ['PaymentDate'])? '' : $result [$i] ['PaymentDate'],
                    'ReferenceNumber' => is_null($result [$i] ['ReferenceNumber'])? '' : $result [$i] ['ReferenceNumber'],
                    'CardStatus' => $result [$i] ['CardStatus'],
                    'ApplyDate' => $result [$i] ['ApplyDate']
                );
            }

            return $ReturnAry;
        }

        function ws_getReprintCardRecordHistory($StudentUserID){

            $sql = "Select
						r.RecordID, r.StudentID, r.PhotoPath, r.RejectReason, r.ReprintCardReason, r.DeliveryMethod, r.DeliveryName, r.DeliveryTelNum, r.DeliveryAddress, r.DeliveryDistrict, r.PaymentItemFee, r.PaymentDeliverFee,r.PaymentAmountFee, r.PaymentMethod, r.PaymentDate, r.ReferenceNumber, r.CardStatus, r.ApplyDate, r.CancelDate
				From
						REPRINT_CARD_RECORD as r
				Where
						r.StudentID = '" . $StudentUserID . "'
						And r.CardStatus IN (4,5,6)
				Order By r.ApplyDate desc 
				";

            $result = $this->returnArray ( $sql );

            $numOfRecord = count ( $result );

            $ReturnAry ['ReprintCardRecordHistory'] = array ();

            for($i = 0; $i < $numOfRecord; $i ++) {
                $ReturnAry ['ReprintCardRecordHistory'] [] = array (
                    'RecordID' => $result [$i] ['RecordID'],
                    'StudentID' => $result [$i] ['StudentID'],
                    'PhotoPath' => $result [$i] ['PhotoPath'],
                    'RejectReason' => $result [$i] ['RejectReason'],
                    'ReprintCardReason' => $result [$i] ['ReprintCardReason'],
                    'DeliveryMethod' => $result [$i] ['DeliveryMethod'],
                    'DeliveryName' => $result [$i] ['DeliveryName'],
                    'DeliveryTelNum' => $result [$i] ['DeliveryTelNum'],
                    'DeliveryAddress' => $result [$i] ['DeliveryAddress'],
                    'DeliveryDistrict' => $result [$i] ['DeliveryDistrict'],
                    'PaymentItemFee' => $result [$i] ['PaymentItemFee'],
                    'PaymentDeliverFee' => $result [$i] ['PaymentDeliverFee'],
                    'PaymentAmountFee' => $result [$i] ['PaymentAmountFee'],
                    'PaymentMethod' => $result [$i] ['PaymentMethod'],
                    'PaymentDate' => $result [$i] ['PaymentDate'],
                    'ReferenceNumber' => $result [$i] ['ReferenceNumber'],
                    'CardStatus' => $result [$i] ['CardStatus'],
                    'ApplyDate' => $result [$i] ['ApplyDate'],
                    'CancelDate' => $result [$i] ['CancelDate']
                );
            }

            return $ReturnAry;
        }

        function ws_updateReprintCardRecordPaymentStatus($RecordID,$PaymentStatus,$ReferenceNum,$PaymentMethod,$PaymentDate){
            $returnAry = array();
            if($PaymentStatus==1){
                $sql = "Update
							REPRINT_CARD_RECORD
					Set
							ReferenceNumber = '" . $ReferenceNum . "',
							PaymentMethod = '" . $PaymentMethod . "',
							PaymentDate = '" . $PaymentDate . "',
							CardStatus = '2'
					Where
							RecordID = '" . $RecordID . "'
					";
                $result = $this->db_db_query ( $sql );


                $returnAry ['UpdateSuccess'] = $result;

                //Handle MIS part API

                $this->sendToMIS($RecordID);

            }
            return $returnAry;
        }

        function ws_updateReprintCardOrderStatus($RecordID,$CardStatus){
            $sql = "Update
							REPRINT_CARD_RECORD
					Set
							CardStatus = '" . $CardStatus . "'
					Where
							RecordID = '" . $RecordID . "'
					";
            $result = $this->db_db_query ( $sql );

            $returnAry = array();
            $returnAry ['UpdateSuccess'] = $result;
            return $returnAry;
        }

        function returnFolderDivisionNum($fileId) {
            return ceil($fileId / 1500);
        }

        function getPaymentFee($DeliveryDistrict)
        {
            global $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;

            include_once($PATH_WRT_ROOT.'/includes/global.php');
            include_once($PATH_WRT_ROOT.'/includes/libdb.php');
            include_once($PATH_WRT_ROOT.'/includes/libeclassapiauth.php');
            include_once($PATH_WRT_ROOT.'/includes/eClassApp/eClassAppConfig.inc.php');
            include_once($PATH_WRT_ROOT."/includes/eClassApp/libAES.php");
            include_once($PATH_WRT_ROOT."/includes/json.php");

            $lapiAuth = new libeclassapiauth();
            $libaes = new libAES($eclassAppConfig['aesKey']);
            $jsonObj = new JSON_obj();

            $platformApiKey = $lapiAuth->GetAPIKeyByProject('IP/EJ platform');
            $apiPath = $eclassAppConfig['paymentGateway']['apiPath'];

            $requestAry = array();
            $requestAry['RequestMethod'] = 'returnReprintCardPriceInfo';
            $requestAry['APIKey'] = $platformApiKey;
            $requestAry['Request']['SchoolCode'] = $config_school_code;
            $requestAry['Request']['MailLocation'] = $DeliveryDistrict;
            $requestJsonString = $jsonObj->encode($requestAry);


            $jsonAry = array();
            $jsonAry['eClassRequestEncrypted'] = $libaes->encrypt($requestJsonString);
            $postJsonString = $jsonObj->encode($jsonAry);


            session_write_close();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_URL, $apiPath);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJsonString);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            $responseJson = curl_exec($ch);
            curl_close($ch);

            $json_ary = $jsonObj->decode($responseJson);
            return $json_ary['MethodResult'];
        }

        function sendToMIS($RecordID)
        {
            global $PATH_WRT_ROOT, $eclassAppConfig,$intranet_root;

            include_once($PATH_WRT_ROOT."includes/global.php");
            include_once($PATH_WRT_ROOT."includes/libdb.php");
            include_once($PATH_WRT_ROOT."includes/json.php");
            include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
            include_once($PATH_WRT_ROOT."includes/eClassApp/eClassAppConfig.inc.php");
            include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
            include_once($PATH_WRT_ROOT."includes/eClassApp/libAES.php");
            include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
            include_once($PATH_WRT_ROOT."includes/libexporttext.php");

            intranet_opendb();

            $ldb = new libdb();
            $lauth = new libeclassapiauth();
            $laes = new libAES($eclassAppConfig['aesKey_reprintCard']);
            $jsonObj = new JSON_obj();
            $lfs = new libfilesystem();
            $lexport = new libexporttext();
            $filePathBase = $intranet_root;

            // get the access key of eClass Store
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module = 'eClassStore' AND SettingName = 'AccessKey'";
            $settingAry = $ldb->returnResultSet($sql);
            $accessKey = $settingAry[0]['SettingValue'];

            //get related data from DB

            $sql = "Select
                        iu.ChineseName as StudentNameChi,
                        iu.EnglishName as StudentNameEng,
                        yc.ClassTitleEN as ClassName, 
                        ycu.ClassNumber, 
                        r.StudentSTRN,
                        r.StudentBarcode,
                        r.StudentSociety,
                        r.StudentPPSAccountNo,
                        r.ApplyDate,
                        r.IssueDate,
                        r.ExpiryDate,
                        r.YearName1,
                        r.YearName2,
                        r.YearName3,
                        r.YearName4,
                        r.YearName5,
                        r.YearName6,
                        r.YearName7,
                        r.YearName8,
                        r.YearName9,
                        r.YearName10,
                        r.YearName11,
                        r.YearName12,
                        r.ClassName1,
                        r.ClassName2,
                        r.ClassName3,
                        r.ClassName4,
                        r.ClassName5,
                        r.ClassName6,
                        r.ClassName7,
                        r.ClassName8,
                        r.ClassName9,
                        r.ClassName10,
                        r.ClassName11,
                        r.ClassName12,
                        SUBSTRING_INDEX(r.PhotoPath, '/', -1) as PhotoName
                From 
                        REPRINT_CARD_RECORD as r
                        Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
                        Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
                        Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
                Where
                        r.RecordID = '" . $RecordID . "'
                        And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
                ";

            $result = $this->returnArray ( $sql );

            $sql1 = "Select                     
                        r.PhotoPath,
                        r.DeliveryMethod,
                        r.DeliveryName,
                        r.DeliveryTelNum,
                        r.DeliveryAddress,
                        r.PaymentItemFee,
                        r.PaymentDeliverFee,
                        r.PaymentMethod,
                        r.TeacherRemark,
                        r.InputBy
                From 
                        REPRINT_CARD_RECORD as r                       
                Where
                        r.RecordID = '" . $RecordID . "'
                ";
            $resultInfo = $this->returnArray ( $sql1 );

            $successAry = array();
            //get parent info


            $sql = "select UserID, UserLogin,ChineseName,UserEmail,MobileTelNo from INTRANET_USER where UserID='" . $resultInfo[0]['InputBy'] . "' and RecordType=3";
            $parentInfo = $this->returnResultSet($sql);

            //handle csv file


            $exportColumn = array("StudentNameChi", "StudentNameEng", "ClassName", "ClassNumber", "StudentSTRN", "Barcode", "Society", "PPS", "ApplyDate", "IssueDate", "ExpiryDate",
                                  "YearName1","YearName2","YearName3","YearName4","YearName5","YearName6","YearName7","YearName8","YearName9","YearName10","YearName11","YearName12",
                                  "ClassName1","ClassName2","ClassName3","ClassName4","ClassName5","ClassName6","ClassName7","ClassName8","ClassName9","ClassName10","ClassName11","ClassName12", "PhotoName");

            $export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);


            $_filePath = $filePathBase . '/file/eClassApp/ReprintCard/RecordToMIS/' . $RecordID . '/ReprintCardRecord_'.$RecordID.'.txt';

            $_folderPath = $filePathBase . '/file/eClassApp/ReprintCard/RecordToMIS/' . $RecordID . '/';
            if (! file_exists ( $_folderPath )) {
                $successAry ['createRootFolder'] = $lfs->folder_new ( $_folderPath );
            }

            $successAry ['fileWrite']= $lfs->file_write($export_content,$_filePath);


            $_photoSourcePath = $filePathBase . $resultInfo[0]['PhotoPath'];
            $_photoMISPath = $filePathBase . '/file/eClassApp/ReprintCard/RecordToMIS/' . $RecordID . '/'.$result[0]['PhotoName'];

            $successAry ['photoCopy']  = $lfs->file_copy($_photoSourcePath,$_photoMISPath);

            ### Download path
            $fileName = $RecordID ;

            $downloadZipFile = $fileName. '.zip';

            $_ZIPFilePath =  $filePathBase . '/file/eClassApp/ReprintCard/RecordToMIS/';

            $_ZIPFile =  $filePathBase . '/file/eClassApp/ReprintCard/RecordToMIS/'. $RecordID . '.zip';

            ### Generated zip file
            # 1st param: download file name
            # 2nd param: download zip file path
            # 3rd param: download folder location
            $successAry ['ZIPFolder'] = $lfs->file_zip($fileName,$downloadZipFile,$_ZIPFilePath);

            // get zip file and then base64
            $zipFileDataBase64 = base64_encode($lfs->file_read($_ZIPFile));

            // get IP/EJ platform API key
            $apiKey = $lauth->GetAPIKeyByProject('IP/EJ platform');

            // prepare API data
            $orderRecordId = $RecordID;
            $parentName = $parentInfo [0] ['ChineseName'];
            $parentEmail = $parentInfo [0] ['UserEmail'];
            $parentPhone = $parentInfo [0] ['MobileTelNo'];
            $parentUserLogin = $parentInfo [0] ['UserLogin'];
            $orderRemarks = $resultInfo[0]['TeacherRemark'];
            if($resultInfo[0]['DeliveryMethod']==1){
                $deliveryMethod = 0;    // 1:post or 0:self-take
            }else if($resultInfo[0]['DeliveryMethod']==2){
                $deliveryMethod = 1;    // 1:post or 0:self-take
            }
            $deliveryName= $resultInfo[0]['DeliveryName'];
            $deliveryTelNum = $resultInfo[0]['DeliveryTelNum'];
            $deliveryAddress = $resultInfo[0]['DeliveryAddress'];
            $cardPrice = $resultInfo[0]['PaymentItemFee'];
            $deliveryFee = $resultInfo[0]['PaymentDeliverFee'];
            $payGateway = $resultInfo[0]['PaymentMethod'];
            if ($payGateway == 'tng') {
                $payStatus = 8;
            }
            else if ($payGateway == 'alipay') {
                $payStatus = 7;
            }

            $dataAry['eClassRequest']['RequestMethod'] = 'AddReprintCardRecord';
            $dataAry['eClassRequest']['APIKey'] = $apiKey;
            $dataAry['eClassRequest']['Request']['AccessKey'] = $accessKey;
            $dataAry['eClassRequest']['Request']['IntranetOrderRecordID'] = $orderRecordId;
            $dataAry['eClassRequest']['Request']['ParentName'] = $parentName;
            $dataAry['eClassRequest']['Request']['ParentUserLogin'] = $parentUserLogin;
            $dataAry['eClassRequest']['Request']['ParentEmail'] = $parentEmail;
            $dataAry['eClassRequest']['Request']['ParentPhone'] = $parentPhone;
            $dataAry['eClassRequest']['Request']['FileData'] = $zipFileDataBase64;
            $dataAry['eClassRequest']['Request']['Remarks'] = $orderRemarks;
            $dataAry['eClassRequest']['Request']['DeliveryMethod'] = $deliveryMethod;
            $dataAry['eClassRequest']['Request']['DeliveryName'] = $deliveryName;
            $dataAry['eClassRequest']['Request']['DeliveryTelNum'] = $deliveryTelNum;
            $dataAry['eClassRequest']['Request']['DeliveryAddress'] = $deliveryAddress;
            $dataAry['eClassRequest']['Request']['CardPrice'] = $cardPrice;
            $dataAry['eClassRequest']['Request']['DeliveryFee'] = $deliveryFee;
            $dataAry['eClassRequest']['Request']['PayStatus'] = $payStatus;

            $jsonPlainText = $jsonObj->encode($dataAry);
            $jsonPostAry['eClassRequestEncrypted'] = $laes->encrypt($jsonPlainText);
            $jsonPostText = $jsonObj->encode($jsonPostAry);


            $headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
            $centralServerUrl = $eclassAppConfig['reprintCard']['apiPath'];

            session_write_close();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonPostText);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            $responseJson = curl_exec($ch);
            curl_close($ch);

            $responseJson = standardizeFormPostValue($responseJson);
            $responseJsonAry = $jsonObj->decode($responseJson);

            $reponseJsonEncryptedText = $responseJsonAry['eClassResponseEncrypted'];
            $responseJsonDecryptedText = $laes->decrypt($reponseJsonEncryptedText);
            if ($responseJsonDecryptedText != '') {
                $responseJsonDecryptedAry = $jsonObj->decode($responseJsonDecryptedText);
            }

            $returnAry ['MISresponseArray'] = $responseJsonDecryptedAry;
        }

    }
}
?>