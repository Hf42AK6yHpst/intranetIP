<?php
// editing by 
 
/********************
 * 	20151023 (Ivan) [K87821]
 * 	- modified function WS_saveDeviceAuthCode() to cater empty AuthCode case
 ********************/
if (!defined("LIBECLASSAPP_AUTHCODE_DEFINED")) {
	define("LIBECLASSAPP_AUTHCODE_DEFINED", true);
	
	class libeClassApp_authCode extends libdb {
		function libeClassApp_authCode() {
			$this->libdb();
			$this->ModuleTitle = 'eClassApp_authCode';
		}
		
		function WS_saveDeviceAuthCode($parUserId, $parAuthCode, $parDeviceId) {
			global $PATH_WRT_ROOT, $eclassAppConfig;
			
			$parAuthCode = trim($parAuthCode);
			$applyStatus = '';
			$errorCode = '';
			
			// no code input
			if ($parAuthCode == '') {
				$applyStatus = 'N';
				$errorCode = 'InvalidCode';
			}
			else {
				// not a valid code
				$notUsedCodeAry = $this->getAuthCodeData($parUserId, $useStatus=2, $parAuthCode, '', $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']);
				$recordId = $notUsedCodeAry[0]['RecordID'];
				if ($recordId == '') {
					$applyStatus = 'N';
					$errorCode = 'InvalidCode';
				}
				
				// code in use already
				$inUsedCodeAry = $this->getAuthCodeData($parUserId, $useStatus=1, $parAuthCode, '', $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']);
				if (count($inUsedCodeAry) > 0) {
					$applyStatus = 'N';
					$errorCode = 'CodeInUseAlready';
				}
				
				if ($recordId > 0 && $applyStatus == '') {
					$sql = "Update 
									APP_AUTH_CODE 
							Set 
									DeviceID = '".$this->Get_Safe_Sql_Query($parDeviceId)."',
									RegistrationDate = now()
							Where
									RecordID = '".$recordId."'
							";
					$success = $this->db_db_query($sql);
					
					if ($success) {
						$applyStatus = 'Y';
					}
					else {
						$applyStatus = 'N';
						$errorCode = 'RegistrationFailed';
					}
				}
			}
			
			$returnAry = array();
			$returnAry['ApplySuccess'] = $applyStatus;
			$returnAry['ErrorCode'] = $errorCode;
			
			return $returnAry;
		}
		
		function WS_getDeviceAuthCode($parUserId, $parDeviceId) {
			global $eclassAppConfig;
			$authCode = '';
			
			// commented this because did not remove the space when saving the device id
			//$parDeviceId = str_replace(' ', '', $parDeviceId);	// remove space in device id
			
			$authCodeAry = $this->getAuthCodeData($parUserId, $useStatus='', $authCode='', $parDeviceId, $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']);
			$authCode = $authCodeAry[0]['AuthCode'];
			$authCode = ($authCode==null)? '' : $authCode;
			
			$returnAry = array();
			$returnAry['AuthCode'] = $authCode;
			
			return $returnAry;
		}
		
		function addAuthCodeForUser($parUserIdAry, $parNumOfCode) {
			global $eclassAppConfig;
			
			$successAry = array();
			
			$sql = "Select UserID, CodeNum, AuthCode From APP_AUTH_CODE Where UserID IN ('".implode("','", (array)$parUserIdAry)."')";
			$userCodeAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'UserID', $IncludedDBField=array('CodeNum', 'AuthCode'), $SingleValue=0, $BuildNumericArray=1);
			
			$numOfUser = count($parUserIdAry);
			$insertAry = array();
			for ($i=0; $i<$numOfUser; $i++) {
				$_userId = $parUserIdAry[$i];
				
				$_firstCodeNum = 1;
				if (isset($userCodeAssoAry[$_userId])) {
					$_firstCodeNum = max(Get_Array_By_Key($userCodeAssoAry[$_userId], 'CodeNum')) + 1;
				}
				
				$_existingAuthCodeAry = Get_Array_By_Key($userCodeAssoAry[$_userId], 'AuthCode');
				
				for ($j=0; $j<$parNumOfCode; $j++) {
					$__targetCodeNum = $_firstCodeNum + $j;
					
					$__count = 0;
					do {
						$__targetCodeNum += $__count;
						$__targetAuthCode = $this->generateAuthCode($_userId, $__targetCodeNum);
						$__count++;
					} while (in_array($__targetAuthCode, (array)$_existingAuthCodeAry));
					
					$insertAry[] = " ('".$_userId."', '".$this->Get_Safe_Sql_Query($__targetAuthCode)."', '".$__targetCodeNum."', '".$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
				}
			}
			
			$insertChunkAry = array_chunk($insertAry, 1000);
			$numOfChunk = count($insertChunkAry);
			for ($i=0; $i<$numOfChunk; $i++) {
				$_insertAry = $insertChunkAry[$i];
				
				$sql = "Insert Into APP_AUTH_CODE
								(UserID, AuthCode, CodeNum, RecordStatus, InputDate, InputBy, ModifiedDate, ModifiedBy)
						Values
								".implode(', ', (array)$_insertAry)."
						";
				$successAry[] = $this->db_db_query($sql);
			}
			
			return !in_array(false, $successAry);
		}
		
		function generateAuthCode($parUserId, $parCodeNum) {
			global $eclassAppConfig;
			
			return strtoupper(substr(md5($parUserId.$eclassAppConfig['urlSalt'].$parCodeNum), 0, 6));
		}
		
		function getAuthCodeData($userIdAry='', $useStatus='', $authCode='', $deviceId='', $recordStatusAry='') {
			global $eclassAppConfig;
			
			if ($userIdAry !== '') {
				$conds_userId = " AND aac.UserID in ('".implode("','", (array)$userIdAry)."') ";
			}
			
			if ($useStatus === '') {
				// no filtering
			}
			else if ($useStatus == 1) {
				// used code
				$conds_useStatus = " AND aac.RegistrationDate != '' AND aac.RegistrationDate is not null ";
			}
			else if ($useStatus == 2) {
				// not used code
				$conds_useStatus = " AND (aac.RegistrationDate = '' OR aac.RegistrationDate is null) ";
			}
			
			if ($authCode != '') {
				$conds_authCode = " AND BINARY AuthCode = '".$this->Get_Safe_Sql_Query($authCode)."' ";
			}
			
			if ($deviceId != '') {
				$conds_deviceId = " AND DeviceID = '".$this->Get_Safe_Sql_Query($deviceId)."' ";
			}
			
			if ($recordStatusAry != '') {
				$conds_recordStatus = " AND RecordStatus IN ('".implode("','", (array)$recordStatusAry)."') ";
			}
			
			$sql = "Select 
							aac.RecordID, aac.UserID, aac.DeviceID, aac.AuthCode, aac.RegistrationDate, aac.RecordStatus
					From
							APP_AUTH_CODE as aac
					Where
							1
							$conds_userId
							$conds_useStatus
							$conds_authCode
							$conds_$deviceId
							$conds_recordStatus
					Order By
							aac.UserID, aac.CodeNum
					";
			return $this->returnResultSet($sql);
		}
		
		function getStudentInfo($parYearClassIdAry='') {
			if ($parYearClassIdAry !== '') {
				$conds_yearClassId = " AND yc.YearClassID IN ('".implode("','", (array)$parYearClassIdAry)."') ";
			}
			
			$sql = "Select
							ycu.UserID
					From
							INTRANET_USER as iu
							Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
							Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
							Inner Join YEAR as y ON (yc.YearID = y.YearID)
					Where
							yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							AND iu.RecordType = 2
							$conds_yearClassId
					Group By
							y.Sequence, yc.Sequence, ycu.ClassNumber
					";
			return $this->returnResultSet($sql);
		}
		
		function getAuthCodeStatusSelection($parId, $parName, $parValue, $parOnChange) {
			global $Lang;
			
			$selectAry = array();
			$selectAry[''] = $Lang['eClassApp']['AllRegistrationStatus'];
			$selectAry[1] = $Lang['eClassApp']['Registered'];
			$selectAry[2] = $Lang['eClassApp']['NotYetRegistered'];
					
			$onchange = '';
			if ($parOnChange != "")
				$onchange = 'onchange="'.$parOnChange.'"';
				
			$selectionTags = ' id="'.$parId.'" name="'.$parName.'" '.$onchange;
			return getSelectByAssoArray($selectAry, $selectionTags, $parValue, $all=0, $noFirst=1);
		}
		
		function getAuthCodeParentStatusSelection($parId, $parName, $parValue, $parOnChange) {
			global $Lang;
			
			$selectAry = array();
			$selectAry[''] = $Lang['eClassApp']['AllParent'];
			$selectAry[1] = $Lang['eClassApp']['Registered'];
			$selectAry[2] = $Lang['eClassApp']['NotYetRegistered'];
					
			$onchange = '';
			if ($parOnChange != "")
				$onchange = 'onchange="'.$parOnChange.'"';
				
			$selectionTags = ' id="'.$parId.'" name="'.$parName.'" '.$onchange;
			return getSelectByAssoArray($selectAry, $selectionTags, $parValue, $all=0, $noFirst=1);
		}
		
		function getAuthCodeRecordStatusSelection($parId, $parName, $parValue, $parOnChange) {
			global $Lang, $eclassAppConfig;
			
			$selectAry = array();
			$selectAry[''] = $Lang['eClassApp']['AllEnableStatus'];
			$selectAry[$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']] = $Lang['General']['Enabled'];
			$selectAry[$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['inactive']] = $Lang['General']['Disabled'];
					
			$onchange = '';
			if ($parOnChange != "")
				$onchange = 'onchange="'.$parOnChange.'"';
				
			$selectionTags = ' id="'.$parId.'" name="'.$parName.'" '.$onchange;
			return getSelectByAssoArray($selectAry, $selectionTags, $parValue, $all=0, $noFirst=1);
		}
		
		function updateAuthCodeStatus($parRecordIdAry, $parRecordStatus) {
			$sql = "Update APP_AUTH_CODE Set RecordStatus = '".$parRecordStatus."' Where RecordID In ('".implode("','", (array)$parRecordIdAry)."')";
			return $this->db_db_query($sql);
		}
	}
}
?>