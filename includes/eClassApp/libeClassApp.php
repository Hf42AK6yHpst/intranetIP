<?php
// editing by  

/********************
 * change log:
 * 2020-11-06 Ray: Modified deleteAppAccountServerFollowUp add SessionID
 * 2020-09-22 Ray: saveUserDevice check empty deviceid
 * 2020-09-03 Ray: modified getUserInfo add $withTitle
 * 2020-08-12 Ray: modified getSchoolExtraFlag add reason flag
 * 2020-08-03 Bill: modified standardizePushMessageText(), remove '&#160;' in push msg  [2020-0728-1019-47066]	[removed]
 * 2020-07-20 Ray: modify sendPushMessage add huawei
 * 2020-07-14 Ray: modify getSchoolExtraFlag add flag ePayment_TopUpWithEWallet_VisaMaster
 * 2020-07-13 Ray: modify getUserModuleAccessRight() add access right "ePaymentMultiple" fps payment
 * 2020-07-08 Ray: add $sys_custom['ParentAppBodyTemperature']
 * 2020-07-08 Ray: modified getSchoolExtraFlag add ePayment_FPSUseQRCode
 * 2020-07-06 Ray: add getAppApplicableModule powerclacc parent app imail checking
 * 2020-06-26 Cameron: modified getUserExtraFlag() and getAppApplicableModule(), add eInventoryStocktake for teacher app
 * 2020-06-16 Ray: modified getSchoolExtraFlag to add ePayment_AlipayPaymentTimeout
 * 2020-06-10 Tiffany: hide eLearning Timetable for PowerClass and KIS
 * 2020-05-29 Cameron: use $intranet_root instead of $PATH_WRT_ROOT in deleteScheduledPushMessageInServer() to avoid error
 * 2020-05-07 Ivan [ip.2.5.11.5.1]: modified getSchoolExtraFlag() to add $sys_custom['eClassApp']['hideHomepageStudentPhotoIfNotUploaded'] handling
 * 2020-03-23 Bill [ip.2.5.11.3.1]: added getUserPassInfo(), modified getUserInfoForSSO(), to handle LDAP webview / sso access problem  [2020-0221-1443-40207]
 * 2020-03-19 Tiffany : add eLearningTimetable SSO setting
 * 2019-03-18 Tiffany [ip.2.5.11.5.1]: modified getSchoolExtraFlag() to add ePayment_TopUpWithEWallet_WithOneThousandDollarOption
 * 2020-02-12 Cameron [ip.2.5.11.5.1]: modified getUserExtraFlag(), getAppApplicableModule() and getModuleName(), add eInventory for teacher app
 * 2020-02-11 Bill [ip.2.5.11.3.1]: modified getSchoolExtraFlag(), to add flag for top-up wallet - ePayment_TopUpWithEWallet_Fps
 * 2020-01-20 Tommy [ip.2.5.11.1.1]: modified checkKisEnrolment(), for disable KIS eEnrolment if $sys_custom['KIS_eEnrolment']['DisableApp'] is true
 * 2020-01-16 Tommy [ip.2.5.11.1.1]: modified getAppApplicableModule(), hide eEnrolment for KIS client #A173960
 * 2020-01-10 Bill [ip.2.5.11.1.1]:	modified sendPushMessageByBatch(), no view details button for edis push msg [DM#3736]
 * 2019-11-25 Philips [ip.2.5.10.12.1]: modified getParentAppLoginUserSql(), added hideLeftStudent to hide student who left school already [2019-1108-1515-14235]
 * 2019-11-19 Bill [ip.2.5.10.12.1]: modified sendPushMessageByBatch(), fix no view details button if use payload to get data	[2019-1118-1635-25235]
 * 									 modified logRequest(), to add app request log	($bug_tracing['app_request_log'])
 * 2019-11-14 Tiffany [ip.2.5.10.12.1]: modified sendPushMessage(), set $forceUseClass to true to force using default json_encode / decode (in json.class module)[Z174638]
 * 2019-10-17 Philips: modify getModuleObjArr(), checkAccessRight(), getParentAppLoginUserSql() to allow Class Teacher access parent app login status
 * 2019-10-17 Tiffany: modify iMail to use sso in app
 * 2019-10-16 Bill [ip.2.5.10.10.1]: modify getUserRelatedPushMessage(), allow parent to get push msg of suspended children	[DM#3668]
 * 2019-10-08 Ronald: modify getUserModuleAccessRight() add access right "ePaymentFps" fps payment
 * 2019-10-08 Ronald: modify getUserModuleAccessRight() add access right "ePaymentTapAndGo" tap and go payment
 * 2019-10-02 Ronald: modify getSchoolExtraFlag() add flag "ePayment_TopUpWithEWallet_TapAndGo" for tap and go top up payment
 * 2019-09-11 Tiffany [ip.2.5.10.10.1]: modified getSchoolExtraFlag() flag "KIS_DisableRightClick", that add Digital Channel hide download button by settings
 * 2019-09-05 Ivan [ip.2.5.10.10.1]: modified getSchoolExtraFlag() to add checking for ePayment enable top-up settings
 * 2019-09-05 Tiffany [ip.2.5.10.10.1]: hide status performance if $special_feature['lite_for_eAdmin'] = true
 * 2019-08-28 Ronald [ip.2.5.10.6.1]: modify function savePushMessageAttachment, update file name
 * 2019-08-01 Tiffany [ip.2.5.10.10.1]: modify ePos
 * 2019-08-01 Cameron [ip.2.5.10.10.1]: add ePos to getUserExtraFlag() - [2019-08-01 disable first]
 * 2019-07-30 Bill [2019-0723-1334-33206]: modified getSchoolExtraFlag(), to add flag for editing Official Photo & Personal Photo
 * 2019-07-16 Tiffany - Add eEnrolmentStudentViewSsoUrl for  eEnrolment student app
 * 2019-07-16 Henry [ip.2.5.10.10.1]- Add eCircular to teacher app for KIS client
 * 2019-07-10 Tiffany - Add eEnrolment to student app
 * 2019-06-28 Tiffany - Add iMail to student app with flag $sys_custom['eClassStudentApp']['enableiMail']
 * 2019-06-14 Anna [ip.2.5.10.6.1]- Modified getUserModuleAccessRight, check if not iPo admin, hide iPo button
 * 2019-06-11 Anna [ip.2.5.10.6.1]: modified getSSOParamAry(), added $ignoreTimeChecking = true at getDecryptedText(), to avoid timestamp checking
 * 2019-05-29 Ronald [ip.2.5.10.6.1]: modified sendPushMessageByBatch(), add attachment
 *                                    modified sendPushMessage(), handle resend get attachment path, only store attachment when create message
 * 2019-04-18 Tiffany [ip.2.5.10.4.1]: add iPortfolio to parent and teacher app
 * 2019-04-01 Cameron [ip.2.5.10.4.1]: apply AppUserCanUploadPhotos for digital channels to getUserExtraFlag()
 * 2019-01-29 Ivan [ip.2.5.10.1.1]: modified getSchoolExtraFlag() to add ePayment_TopUpWithEWallet_WithOneDollarOption
 * 2019-01-04 Anna: modified checkAccessRight,getModuleObjArr - added DHL PIC access right
 * 2018-12-05 Ivan [ip.2.5.10.1.1]: modified getSchoolExtraFlag() to add flag "ePayment_TopUpWithEWallet_Alipay" and "ePayment_TopUpWithEWallet_TNG" to app
 * 2018-12-04 Cameron [ip.2.5.10.1.1] add control $plugin['medical'] to getAppApplicableModule()
 * 2018-10-19 Ivan [ip.2.5.9.11.1]: added ws_saveUserPhoto(), ws_useOfficialPhotoAsPersonalPhoto(), ws_cancelOfficialPhotoAsPersonalPhoto() for student app upload photo function
 * 2018-10-04 Ronald [ip.2.5.9.11.1]: modified sendPushMessage(), add attachment Url path for supporting attachment in push message
 *                                    add function savePushMessageAttachment for storing attachment
 * 2018-08-31 Ronald [ip.2.5.9.10.1]: modified getUserModuleAccessRight(), ensure having ePayment module before checking TNG / Alipay access right
 * 2018-08-27 Ivan [X145107] [ip.2.5.9.10.1] : modified getSchoolExtraFlag() to fix showing wrong warning message when changing password if the school applied strong pwd policy
 * 2018-07-19 Ronald [ip.2.5.9.10.1]: modified getUserModuleAccessRight(), add alipay module
 * 2018-05-09 Cameron [ip.2.5.9.5.1]: add medicalCaringSleepSSOParam and medicalCaringBowelSSOParam_v2 to getUserExtraFlag()
 * 2018-04-25 Ivan [ip.2.5.9.5.1]: modified returnSchoolBanner() to disable default banner logic for DHL and SFOC project
 * 2018-04-06 Ivan [ip.2.5.9.5.1]: modified getAppApplicableModule() to hide classroom and iPf function in Student App for KIS and PowerClass
 * 2018-03-12 Ronald [ip.2.5.9.3.1]: add saveSchoolDefaultImage(), save image if selecting default image
 *                                   modified saveSchoolImage(), save as using default image if not uploading files
 *                                   modified getSchoolImageUrlPath(), get default image path if using default image
 *                                   modified getSchoolImage(), getting RecordStatus to distinguish using upload or default image
 *                                   modified returnSchoolBanner(), return background image url by calling function
 *
 * 2018-03-12 Ronald [ip.2.5.9.3.1]: modified saveSchoolImage(), add image type accountPageBanner switching
 *                                   modified returnSchoolBanner(), return account page banner for parent app
 * 2017-10-27 Anna [ip.2.5.9.1.1] :modified  getSelectionBox, change button to lang words
 * 2017-10-16 Bill [ip.2.5.8.10.1]: modified getAccessRightInfo(), not allow access to eDiscipline in Teacher App if enabled some flags
 * 2017-09-20 Ivan [ip.2.5.8.10.1]: modified getUserExtraFlag() to add $sys_custom['eClassApp']['iMail']['AstriUI']
 * 2017-09-20 Ivan [ip.2.5.8.10.1]: added clearLicenseCache()
 * 2017-09-14 Bill [ip.2.5.8.10.1]: modified getAppApplicableModule(), to support eHomework for some KIS clients ($sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] & $sys_custom['KIS_eHomework']['EnableModule'])
 * 2017-08-25 Ivan [ip.2.5.8.7.1 CD3]: modified getUserExtraFlag() to change teahcer app edis path
 * 2017-06-22 HenryHM : added flag for Classroom in getUserExtraFlag()
 * 2017-05-29 HenryHM : added excludeSuspendedUser() in order to suspend sending push notification to the suspended users
 * 2017-04-25 Carlos : modified getTeacherAppLoginUserSql(), added Department field for DHL.
 * 2017-04-21 Ivan [ip.2.5.8.4.1]: modified getAppApplicableModule() to hide some modules for PowerClass platform
 * 2017-03-21 Tiffany [ip.2.5.8.4.1][#K114594]: modified getUserModuleAccessRight() to support setting DisallowNonTeachingStaffTakeAttendance in take attendance.
 * 2017-02-27 Ivan [ip.2.5.8.4.1] ***NEED TO DEPLOY WITH ADDON SCHEMA***: modified logRequest() to add timer and memory usage for the log
 * 2017-01-20 Tiffany: modified getUserModuleAccessRight(), to support epayment by tng
 * 2016-12-05 Paul [ip.2.5.8.1.1]: modified updatePushMessageReadStatus(), add new db update mechanism to get the time a push message is read in classroom notify_usage table.
 * 2016-11-10 Ivan [ip.2.5.8.1.1]: modified savePushMessageResponse(), not update status to "pending to send" if the status is "not registered device"
 * 2016-08-23 Ivan [ip.2.5.7.10.1]: modified getModuleObjArr() to hide meun if the app flag is turned on
 * 2016-07-06 Ivan [ip.2.5.7.7.1]: modified getAppApplicableModule() to add School Info for student app
 * 2016-06-07 Ivan [ip.2.5.7.7.1]: adde function isEnabledStudentApp()
 * 2016-05-18 Ivan [ip.2.5.7.7.1][L95978]: modified sendPushMessage() and getPushMessageStatistics() to cater users that have no push message registered
 * 2016-05-03 Ivan [ip.2.5.7.7.1.0] [A95524]: modified getAppApplicableModule() to hide Student Performance for KIS
 * 2016-04-05 Roy [ip.2.5.7.4.1.0]: modified getUserExtraFlag(), add $eAttendanceHideLateStatus and $eAttendanceHideEarlyLeaveStatus
 * 2016-03-18 Ivan [ip.2.5.7.4.1.0]: added function getUserLoginAppInfo()
 * 2016-03-17 Ivan [ip.2.5.7.4.1.0]: modified function sendPushMessageByBatch(), resendPushMessage() added param $fromModule and $moduleRecordID to fix failed to display "view enotice" button in android app push message page
 * 2016-03-11 Ivan [ip.2.5.7.4.1.0]: added function getStudentAppLoginUserSql() for student app login status report
 * 2016-03-10 Ivan [ip.2.5.7.4.1.0]: added function getUserObj(), modified function getSchoolExtraFlag() and getUserModuleAccessRight() to improve login perforamce
 * 2016-03-10 Ivan [ip.2.5.7.4.1.0]: modified getSSOParam(), getUserInfoForSSO() to improve performance by caching data
 * 2016-03-10 Ivan [ip.2.5.7.4.1.0]: modified getUserModuleAccessRight() to add student app logic
 * 2016-03-09 Ivan [ip.2.5.7.4.1.0]: modified getModuleObjArr(), isSchoolInLicense(), checkAccessRight() to add student app menu
 * 2016-03-07 Kenneth [ip.2.5.7.4.1.0]: modified sendPushMessage(), global $forceUseClass to force using default json_encode / decode (in overrided json.class module, e.g. eLibPlus)
 * 2016-03-03 Kenneth [ip.2.5.7.4.1.0]: modified getUserRelatedPushMessage(), remove fromModule before sending to push noti, as add fromModule = SchoolNews (search 20160303 )
 * 									   modified overrideExistingScheduledPushMessageFromModule(), add parAppType
 * 2016-02-19 Tiffany [ip.2.5.7.4.1.0]: modified getUserExtraFlag(), add ePayment KIS_OnlyShowPaymentRecords setting
 * 2016-02-15 Kenneth [ip.2.5.7.4.1.0]: modified saveSchoolImage(), fixed IE cannot upload problem
 * 2016-02-15 Ivan [ip.2.5.7.4.1.0]: modified getAppApplicableModule() to add flag for medical caring system
 * 2016-01-25 Ivan [ip.2.5.7.4.1.0] : modified getUserDevice(), sendPushMessage() to filter monster code device id
 * 2015-12-11 Ivan [ip.2.5.7.1.1.0] : added getSSOParam(), getUserExtraFlag(), modified getSSOParamAry() to support SSO from app
 * 2015-11-24 Tiffany [ip.2.5.7.1.1.0] : modified function getSelectionHtml(), delete data-native-menu="false" in select
 * 2015-11-13 Ivan [ip.2.5.7.1.1.0] [B88937]: modified function sendPushMessage(), added logic $sys_custom['eClassApp']['hideAppPushMessageDirectLink'] to hide the direct link in app push message
 * 2015-11-03 Ivan [ip.2.5.7.1.1.0]: added function isEnabledSendBulkPushMessageInBatches(), getSendPushMessageBatchFrequency(), getSendPushMessageBatchRecipient(), sendPushMessageByBatch()
 * 									modified sendPushMessage()
 * 									to add the send push message by batch logic
 * 2015-09-17 Ivan [ip.2.5.6.10.1.0]: modified getPushMessageStatistics() count the message priority as follows now: 1) has read, 2) sent success, 3) waiting to send, 4) send failed
 * 2015-08-27 Ivan [ip.2.5.6.7.1.0 CD#2]: modified saveUserDeviceInfoToCloud() to send targetUserIdAry to central server. Otherwise, if all devices of the users are logged out, central server failed to update records due to no UserID as associate key
 * 2015-08-26 Roy [ip.2.5.6.10.1.0]: modified getUserExtraFlag(), add flag 'groupMessageApiPath' & 'groupMessageWsPath'
 * 2015-08-05 Roy [ip.2.5.6.7.1.0]: modified getUserExtraFlag(), add flag 'homeAttendanceViewStyle'
 * 2015-08-04 Roy [ip.2.5.6.7.1.0]: modified inactivateUserDevice(), set default Login Status to "del"
 * 2015-08-04 Ivan [ip.2.5.6.7.1.0]: modified saveUserDevice() to count logged out devices and change LoginStatus to "del" for "DeviceTooMuch" case
 * 2015-08-03 Roy [ip.2.5.6.7.1.0]: modified saveUserDevice(), inactivateUserDevice() for saving login / logout status
 * 2015-07-31 Roy [ip.2.5.6.7.1.0]: modified getUserDevice(), get correct device by SQL statement for teacher app logout
 * 2015-07-29 Evan [ip.2.5.6.7.1.0]: modified saveSchoolImage(), add parameter $type and picture resize function
 * 2015-07-27 Roy [ip.2.5.6.7.1.0]: modified getUserDevice(), use $loggedOutTeacherCanReceivePushMessage to sort logged out device
 * 2015-07-24 Ivan [ip.2.5.6.7.1.0]: modified getAppApplicableModule() to public release Group Message, School Info, and Student Performance. (But disable Group Message for China client)
 * 2015-07-23 Roy [ip.2.5.6.7.1.0]:	modified getTeacherAppAdvancedSettingsTagObj() & getModuleObjArr(), add push message right tab for teacher app advanced settings
 * 2015-07-17 Roy [ip.2.5.6.7.1.0]:	modified getUserExtraFlag(), add "showAttendanceDetails" flag
 * 2015-07-17 Roy [ip.2.5.6.7.1.0]:	modified getUserModuleAccessRight(), disable eAttendance module access right checking
 * 2015-07-15 Ivan:[ip.2.5.6.7.1.0]: modified getAppApplicableModule() to update the module display sequence
 * 2015-07-15 Ivan [ip.2.5.6.7.1.0]: modified updateScheduledPushMessageStatus() to add log for the response json of scheduled push message
 * 2015-07-13 Roy	modified getUserModuleAccessRight(), add eAttendance module access right checking
 * 2015-06-25 Roy	modified sendPushMessage(), send $fromModule and $moduleRecordID with push message
 * 2015-06-19 Roy	modified overrideExistingScheduledPushMessageFromModule(), add quote to SQL statement
 * 2015-06-18 Qiao [ip.2.5.6.7.1.0]: modified getSchoolExtraFlag add key AllowToChangePassword RequireComplexPassword For App change password
 * 2015-05-27 Qiao [ip.2.5.6.7.1.0]: modified getUserExtraFlag() add addSchoolNewsUrl; add function isSchoolNewsAdmin()
 * 2015-05-26 Ivan [ip.2.5.6.7.1.0]: modified sendPushMessageToCentralServer(), savePushMessageResponse() to add performance log logic
 * 2015-05-13 Ivan [ip.2.5.6.5.1.0]: modified getParentAppLoginUserSql() to add studentIdList in the SQL for export use
 * 2015-05-04 Qiao:change function getAppApplicableModule, add $eclassAppConfig['moduleCode']['digitalChannels'] to $moduleAry;
 * 2015-04-21 Qiao:[ip.2.5.6.5.1.0]change function getUserExtraFlag() add Digital Channels Info URL.
 * 2015-04-14 Roy: [ip.2.5.6.5.1.0]: modified inactivateUserDevice(), if $parDeviceId == '' && $parRecordId == '', return true
 * 2015-04-08 Ivan: [ip.2.5.6.5.1.0]: modified getModuleName(), getAppApplicableModule() to add digital channels access right
 * 2015-04-01 Roy: [ip.2.5.6.5.1.0]: modified sendPushMessage(), deleteAppAccountServerFollowUp() to add GeTui service
 * 2015-03-16 Ivan [ip.2.5.6.5.1.0]: modified getUserExtraFlag() to retrieve flipped channels central server path
 * 2015-03-11 Ivan [ip.2.5.6.3.1.0]: added sendPushMessageFromApi() for sending push message from video server API
 * 2015-03-05 Ivan [ip.2.5.6.3.1.0]: modified getUserModuleAccessRight() to hide School Info if there are no public pages in first layer
 * 2015-02-26 Ivan [ip.2.5.6.3.1.0]: modified getAppApplicableModule() to hide eCircular for KIS client
 * 2015-02-24 Pun : Added push notification for Windows Phone
 * 2015-02-03 Ivan [ip.2.5.6.3.1.0]: added function doHouseKeeping() and standardizePushMessageText()
 * 2015-01-29 Qiao :change getSelectionHtml(),add getIntranetAppTeacherListEnableGroup();getAllIntranetAppTeacherListGroup();getSelectionBox();addIntranetAppTeacherStatusGroups();deleteIntranetAppTeacherStatusRemovedGroups()
 * 2015-01-13 Qiao :change method getUserModuleAccessRight add extra $returnAry[$eclassAppConfig['moduleCode']['studentStatus']]['RecordStatus'] checking
 * 2015-01-12 Qiao :add function getTeacherAppAdvancedSettingsTagObj() to generate advanced settings tag by function
 * 2014-12-16 Tiffany: add functions saveSchInfoCustomIcon(), deleteSchInfoCustomIcon(), updateSchoolInfoFlashUploadPath(),
 *                     deleteSchoolInfo(), deleteSchInfoItem(), deleteSchInfoMenu()
 * 2014-12-11 Qiao: add studentPerformanceUrl in getUserExtraFlag
 * 2014-12-11 Ivan [ip.2.5.6.1.1.0]: modified checkAccessRight() and getModuleObjArr() to add message group admin access right
 * 2014-12-09 Ivan [ip.2.5.6.1.1.0]: modified getUserModuleAccessRight() to add class teacher checking for Teacher App Student List function
 * 2014-12-08 Ivan [ip.2.5.6.1.1.0]: modified getUserModuleAccessRight() to add group message module access right
 * 2014-12-04 Qiao :add function getCenterHeader()
 * 2014-12-03 Qiao :add function getSelectionHtml()
 * 2014-11-12 Ivan [ip.2.5.5.12.1]: modified checkAccessRight() to check common function access right
 * 2014-11-12 Ivan [ip.2.5.5.12.1]: modified getModuleObjArr() to add "Common Function" section and "Group Message" menu
 * 2014-11-11 Qiao : modified getModuleName(),getAppApplicableModule() add module for student status
 * 2014-11-11 Qiao : modified getUserExtraFlag(), add studentStatusUrl to userCustFlagAry
 * 2014-11-06 Ivan [ip.2.5.5.12.1]: added getParentAppAdvancedSettingsTagObj() to generate advanced settings tag by function
 * 2014-11-06 Qiao : modified getUserExtraFlag(), add teacherStatusUrl to userCustFlagAry
 * 2014-11-05 Ivan [ip.2.5.5.12.1]: modified getAppLoggedInUser(), getParentAppLoginUserSql() and getTeacherAppLoginUserSql() to add filtering of accept push message status
 * 2014-11-05 Qiao : modified getModuleName(),getAppApplicableModule(),getUserModuleAccessRight()  add module for teacher status
 * 2014-11-05 Qiao : modified getModuleName(),getAccessRightInfo() add advanced setting
 * 2014-11-05 Qiao : add getAllIntranetAppTeacherStatusUser(),addIntranetAppTeacherStatusUsers(),deleteIntranetAppTeacherStatusRemovedUsers(),getAllIntranetAppTeacherStatusUserName()
 * * 2014-11-04 Ivan [ip.2.5.5.12.1]: modified getParentAppLoginUserSql() and getTeacherAppLoginUserSql() to search keyword with UserLogin also
 * 2014-10-24 Ivan [ip.2.5.5.10.1]: modified checkAccessRight() to check license also
 * 2014-10-22 Roy: modified getModuleObjArr(), add advanced setting
 ********************/
include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');

if (!defined("LIBECLASSAPP_DEFINED")) {
	define("LIBECLASSAPP_DEFINED", true);

	class libeClassApp extends libdb {
		var $ModuleTitle;
		var $SettingsObj;

		function libeClassApp() {
			$this->libdb();
			$this->ModuleTitle = 'eClassApp';
			$this->SettingsObj = null;
		}

		function getUserObj($parUserId, $parUserLogin='') {
			$funcParamAry = get_defined_vars();
			$cacheResultAry = $this->getCacheResult('getUserObj', $funcParamAry);
	    	if ($cacheResultAry !== null) {
	    		return $cacheResultAry;
	    	}

			global $intranet_root;

			include_once($intranet_root.'/includes/libuser.php');
			$userObj = new libuser($parUserId, $parUserLogin);

			$this->setCacheResult('getUserObj', $funcParamAry, $userObj);
        	return $userObj;
		}

		function checkAccessRight($appType) {
		    global $plugin, $eclassAppConfig,$sys_custom;
			$canAccess = false;
			if ($appType == $eclassAppConfig['appType']['Common'] ) {
				$canAccessParentApp = false;
// 				if ($plugin['eClassApp'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-eClassApp']) && $this->isSchoolInLicense($eclassAppConfig['appType']['Parent'])) {
				if ($plugin['eClassApp'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-eClassApp'] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher']) && $this->isSchoolInLicense($eclassAppConfig['appType']['Parent'])) {
					$canAccessParentApp = true;
				}

				$canAccessTeacherApp = false;
				if ($plugin['eClassTeacherApp'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-eClassTeacherApp'] && $this->isSchoolInLicense($eclassAppConfig['appType']['Teacher'])) {
					$canAccessTeacherApp = true;
				}

				$canAccessStudentApp = false;
				// student app license is depending on
				if ($plugin['eClassStudentApp'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-eClassStudentApp'] && $this->isSchoolInLicense($eclassAppConfig['appType']['Parent'])) {
					$canAccessStudentApp = true;
				}
				if($sys_custom['DHL']){
				    global $PATH_WRT_ROOT;
				    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
				    $libdhl = new libdhl();
				    $isPIC = $libdhl->isPIC();
				}

				if (	(
							($plugin['eClassApp'] && $this->isSchoolInLicense($eclassAppConfig['appType']['Parent']))
							|| ($plugin['eClassTeacherApp'] && $this->isSchoolInLicense($eclassAppConfig['appType']['Teacher']))
							|| ($plugin['eClassStudentApp'] && $this->isSchoolInLicense($eclassAppConfig['appType']['Parent']))
						)
				    && ($_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"] || ($isPIC && $sys_custom['DHL'])) ) {
					$canAccessCommonFunction = true;
				}
				
				if ($canAccessParentApp || $canAccessTeacherApp || $canAccessStudentApp || $canAccessCommonFunction) {
					$canAccess = true;
				}
			}
			else {
				if ($this->isSchoolInLicense($appType)) {
					if ($appType == $eclassAppConfig['appType']['Parent']) {
						//if ($plugin['eClassApp'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-eClassApp']) {
						if ($plugin['eClassApp'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-eClassApp'] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher'])) {
							$canAccess = true;
						}
					}
					else if ($appType == $eclassAppConfig['appType']['Teacher']) {
						if ($plugin['eClassTeacherApp'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-eClassTeacherApp']) {
							$canAccess = true;
						}
					}
					else if ($appType == $eclassAppConfig['appType']['Student']) {
						if ($plugin['eClassStudentApp'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-eClassStudentApp']) {
							$canAccess = true;
						}
					}
				}
			}

			if (!$canAccess) {
				No_Access_Right_Pop_Up();
			}
		}

		function isAdminUser($targetUserId) {
			global $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT.'includes/user_right_target.php');
			$liburt = new user_right_target();

			$accessRightAry = $liburt->Load_User_Right($targetUserId);
			return $accessRightAry['eAdmin-eClassApp']? true : false;
		}

		function logRequest($requestJsonEncryptedString, $requestJsonDecryptedString, $responseJsonEncryptedString, $responseJsonDecryptedString, $startApiTime, $endApiTime, $runTime) {
			global $bug_tracing;

			$memoryUsed = memory_get_usage();
			$memoryUsedText = convert_size(memory_get_usage());

            if($bug_tracing['app_request_log'])
            {
            	global $file_path;
                $temp_log_filepath = "$file_path/file/log_app_request.txt";

                $temp_time = date("Y-m-d H:i:s");
                $temp_request_decrypt = $requestJsonDecryptedString;
                $temp_response_decrypt = $responseJsonDecryptedString;
                $temp_logentry = "\"$temp_time\"\n\n,\"$temp_request_decrypt\"\n\n,\"$temp_response_decrypt\"\n\n";

                $temp_content = get_file_content($temp_log_filepath);
                $temp_content .= $temp_logentry;
                write_file_content($temp_content, $temp_log_filepath);
            }

			$sql = "Insert Into APP_REQUEST_LOG 
						(RequestTextEncrypted, RequestText, ResponseTextEncrypted, ResponseText, DateInput, StartTime, EndTime, ProcessSecond, MemoryUsage, MemoryUsageText) 
					Values 
						('".$this->Get_Safe_Sql_Query($requestJsonEncryptedString)."', '".$this->Get_Safe_Sql_Query($requestJsonDecryptedString)."', '".$this->Get_Safe_Sql_Query($responseJsonEncryptedString)."', '".$this->Get_Safe_Sql_Query($responseJsonDecryptedString)."', now(), '".$startApiTime."', '".$endApiTime."', '".$runTime."', '".$memoryUsed."', '".$memoryUsedText."') ";
			return $this->db_db_query($sql);
		}

		function getSchoolExtraFlag($parUserLogin, $parUserId='') {
			global $sys_custom, $special_feature, $eclassAppConfig,$PATH_WRT_ROOT;

			$flagAry = array();
			$flagCount = 0;

			### get smartcard balance display or not
			$showSmartcardBalance = '1';
			if (isKIS($parUserLogin) && $sys_custom['ePayment']['KIS_NoBalance']) {
				// KIS => not show balance
				$showSmartcardBalance = '0';
			}
			$flagAry[$flagCount]['key'] = 'ShowSmartcardBalance';
			$flagAry[$flagCount]['value'] = $showSmartcardBalance;
			$flagCount++;

			//For App change password
			include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
//			include_once($PATH_WRT_ROOT."includes/libuser.php");
//			$lu = new libuser('', $parUserLogin);
			$lgeneralsettings = new libgeneralsettings();
            $data = $lgeneralsettings->Get_General_Setting("UserInfoSettings");

            if ($parUserId) {
            	// only pass userid to function getUserObj() to triger the cache mechanism to improve performance
            	$lu = $this->getUserObj($parUserId);
            }
            else {
            	$lu = $this->getUserObj('', $parUserLogin);
            }
            $userType = $lu->RecordType;

			if($data['CanUpdatePassword_'.$userType] == 1){
				$flagAry[$flagCount]['key'] = 'AllowToChangePassword';
				$flagAry[$flagCount]['value'] = 1;
				$flagCount++;

				$flagAry[$flagCount]['key'] = 'RequireComplexPassword';
				$flagAry[$flagCount]['value'] = $data['EnablePasswordPolicy_'.$userType]? $data['EnablePasswordPolicy_'.$userType] : -1;
				if ($sys_custom['UseStrongPassword'] && $flagAry[$flagCount]['value']==-1) {
				    $flagAry[$flagCount]['value'] = 6;
				}
				$flagCount++;
			}

			## use auth code mechanism or not
			$flagAry[$flagCount]['key'] = 'ApplyAuthCode';
			$flagAry[$flagCount]['value'] = ($sys_custom['eClassApp']['authCode'])? '1' : '0';
			$flagCount++;

			## payment gateway path
			$flagAry[$flagCount]['key'] = 'PaymentGatewayPath';
			$flagAry[$flagCount]['value'] = ($eclassAppConfig['paymentGateway']['apiPath'])? $eclassAppConfig['paymentGateway']['apiPath'] : '';
			$flagCount++;

			#GroupMessage showOneToOne setting
			$groupMessageShowOneToOne = "0";
			if($sys_custom['eClassTeacherApp']['GroupMessage']['showOneToOne']){
				$groupMessageShowOneToOne = "1";
			}


			$flagAry[$flagCount]['key'] = 'GroupMessage_ShowOneToOne';
			$flagAry[$flagCount]['value'] = $groupMessageShowOneToOne;
			$flagCount++;

			include_once($PATH_WRT_ROOT.'includes/libpayment.php');
			$lpayment = new libpayment();

			$flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_Alipay';
			$flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['Alipay'])? '1' : '0';
			$flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_TNG';
            $flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['TNG'])? '1' : '0';
            $flagCount++;

            $flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_TapAndGo';
            $flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['TAPANDGO'])? '1' : '0';
            $flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_Fps';
			$flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['FPS'])? '1' : '0';
			$flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_VisaMaster';
			$flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['VisaMaster'])? '1' : '0';
			$flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_WeChat';
			$flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['WeChat'])? '1' : '0';
			$flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_AlipayCN';
			$flagAry[$flagCount]['value'] = ($lpayment->Settings['EnableTopUpInApp'] && $sys_custom['ePayment']['TopUpEWallet']['AlipayCN'])? '1' : '0';
			$flagCount++;

            $flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_WithOneDollarOption';
            $flagAry[$flagCount]['value'] = ($sys_custom['ePayment']['TopUpEWalletWithOneDollarOption'])? '1' : '0';
            $flagCount++;

			// [2019-0723-1334-33206]
            $flagAry[$flagCount]['key'] = 'AccountMgmt_AllowEditOfficialPhoto';
            $flagAry[$flagCount]['value'] = ($userType == 2 && $data['CanUpdatePhoto_'.$userType] == 1)? '1' : '0';
            $flagCount++;

            // [2019-0723-1334-33206]
            $flagAry[$flagCount]['key'] = 'AccountMgmt_AllowEditPersonalPhoto';
            $flagAry[$flagCount]['value'] = ($data['CanUpdatePersonalPhoto_'.$userType] == 1)? '1' : '0';
            $flagCount++;

            $flagAry[$flagCount]['key'] = 'ePayment_TopUpWithEWallet_WithOneThousandDollarOption';
            $flagAry[$flagCount]['value'] = ($sys_custom['ePayment']['TopUpEWalletWithOneThousandDollarOption'])? '1' : '0';
            $flagCount++;
            
            $flagAry[$flagCount]['key'] = 'HideHomepageStudentPhotoIfNotUploaded';
            $flagAry[$flagCount]['value'] = ($sys_custom['eClassApp']['hideHomepageStudentPhotoIfNotUploaded'])? '1' : '0';
            $flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_AlipayPaymentTimeout';
			$flagAry[$flagCount]['value'] = ($sys_custom['ePayment']['AlipayPaymentTimeout'])? $sys_custom['ePayment']['AlipayPaymentTimeout'] : '5m';
			$flagCount++;

			$flagAry[$flagCount]['key'] = 'ePayment_FPSUseQRCode';
			$flagAry[$flagCount]['value'] = ($sys_custom['ePayment']['FPSUseQRCode'])? '1' : '0';
			$flagCount++;

			include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
			$lapplyleave = new libapplyleave();
			$settingAssoAry = $lapplyleave->getSettingsAry();

			$flagAry[$flagCount]['key'] = 'applyLeave_ReasonRequired';
			$flagAry[$flagCount]['value'] = ($settingAssoAry['reasonMustInput'] != '0')? '1' : '0';
			$flagCount++;

			return $flagAry;
		}

		function getUserExtraFlag($parUserLogin, $parUserId) {
			global $sys_custom, $special_feature, $eclassAppConfig, $plugin, $config_school_code, $intranet_root, $FlippedChannelsConfig;

//			$targetUserLogin = $this->getDemoSiteUserLogin($parUserLogin);
//			if ($parUserLogin != $targetUserLogin) {
//				$sql = "SELECT UserID FROM INTRANET_USER where UserLogin='".$targetUserLogin."'";
//				$resultAry = $this->returnResultSet($sql);
//				$targetUserId = $resultAry[0]['UserID'];
//
//				if ($targetUserId > 0) {
//					$parUserLogin = $targetUserLogin;
//					$parUserId = $targetUserId;
//				}
//			}


			$flagAry = array();
			$flagCount = 0;

			### Define token for web-view verification
			$delimiter = "###";
			$schoolDomain = curPageURL($withQueryString=false, $withPageSuffix=false);
			if(substr($schoolDomain,-1)=="/") {
				$schoolDomain = substr($schoolDomain,0,-1);
			}
			$token = $this->getUrlToken($parUserId, $parUserLogin);


			### iMail URL



			if ($sys_custom['eClassApp']['iMail']['AstriUI']) {
				$iMailSsoParam = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['iMail']);

			    include_once($intranet_root.'/includes/eClassApp/libeClassApp_api.php');
			    $leClassAppApi = new libeClassAppApi();
			    $apiToken = $leClassAppApi->getApiToken($parUserId, $parUserLogin, $eclassAppConfig['moduleCode']['iMail']);

			    $iMailUrl = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$iMailSsoParam.'&at='.urlencode($apiToken);
			}
			else {

                $iMailUrl = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['iMail']);
                //$iMailUrl = $schoolDomain.'/home/imail_gamma/app_view/email_list.php?TargetFolderName=INBOX&token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			}
			$flagAry[$flagCount]['key'] = 'iMailUrl';
			$flagAry[$flagCount]['value'] = $iMailUrl;
			$flagCount++;

			### Take attendance URL
// 			if ($sys_custom['eClassApp']['env'] == 'dev' && $parUserLogin == 'isaaccheng') {
// 				$takeAttUrl = $schoolDomain.'/test/isaac/take_attendance/attendance_list.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
// 			}
// 			else {
				$takeAttUrl = $schoolDomain.'/home/smartcard/attendance/app_view/attendance_list.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
// 			}
			$flagAry[$flagCount]['key'] = 'takeAttendanceUrl';
			$flagAry[$flagCount]['value'] = $takeAttUrl;
			$flagCount++;

			### Cross Boundary School Coaches webview link
			$flagAry[$flagCount]['key'] = 'crossBoundarySchoolCoachesUrlPrefix';
			$flagAry[$flagCount]['value'] = $eclassAppConfig['schoolBus']['webViewUrlPrefix'];
			$flagCount++;

			### Teacher Status URL
			$teacherStatusUrl = $schoolDomain.'/home/eClassApp/teacherApp/teacherStatus/teacher_status.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'teacherStatusUrl';
			$flagAry[$flagCount]['value'] = $teacherStatusUrl;
			$flagCount++;

			### eEnrolment URL
			$eEnrolmentUrl = $schoolDomain.'/home/eAdmin/StudentMgmt/enrollment/app_view/attendance_list.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'eEnrolmentUrl';
			$flagAry[$flagCount]['value'] = $eEnrolmentUrl;
			$flagCount++;

			###Student Status URL
			$studentStatusUrl = $schoolDomain.'/home/eClassApp/teacherApp/studentStatus/student_status_classlist.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'studentStatusUrl';
			$flagAry[$flagCount]['value'] = $studentStatusUrl;
			$flagCount++;

			### Teacher Performance URL
			$studentPerformanceUrl = $schoolDomain.'/home/eClassApp/teacherApp/studentPerformance/teacher_term_groupList.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'studentPerformanceUrl';
			$flagAry[$flagCount]['value'] = $studentPerformanceUrl;
			$flagCount++;

			### School Info URL
			$schoolInfoUrl = $schoolDomain.'/home/eClassApp/common/school_info/menu.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'schoolInfoUrl';
			$flagAry[$flagCount]['value'] = $schoolInfoUrl;
			$flagCount++;

			### Digital Channels Info URL
			$photoCommentUrl = $schoolDomain.'/home/eClassApp/eclassApp/digitalChannel/photo_comment.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'photoCommentUrl';
			$flagAry[$flagCount]['value'] = $photoCommentUrl;
			$flagCount++;
		    $photoInforUrl = $schoolDomain.'/home/eClassApp/eclassApp/digitalChannel/photo_information.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'photoInforUrl';
			$flagAry[$flagCount]['value'] = $photoInforUrl;
			$flagCount++;


			### Flipped Channels central server url
			if ($plugin['FlippedChannels']) {
				include_once($intranet_root.'/includes/FlippedChannels/libFlippedChannels.php');
				$lfc = new libFlippedChannels();
				$centralServerPath = $lfc->getCurCentralServerUrl();
				$centralServerPath = ($centralServerPath==null)? '' : $centralServerPath;
				$flagAry[$flagCount]['key'] = 'flippedChannelsCentralServerUrl';
				$flagAry[$flagCount]['value'] = $centralServerPath;
				$flagCount++;
			}

			### eHomework URL
			$eHomeworkListUrl = $schoolDomain.'/home/eAdmin/StudentMgmt/homework/management/homeworklist/app_view/ehomework_list.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'eHomeworkListUrl';
			$flagAry[$flagCount]['value'] = $eHomeworkListUrl;
			$flagCount++;
			$eHomeworkManagementUrl = $schoolDomain.'/home/eAdmin/StudentMgmt/homework/management/homeworklist/app_view/all_ehomework_list.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'eHomeworkManagementUrl';
			$flagAry[$flagCount]['value'] = $eHomeworkManagementUrl;
			$flagCount++;
			$eHomeworkNewUrl = $schoolDomain.'/home/eAdmin/StudentMgmt/homework/management/homeworklist/app_view/new_homework.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'eHomeworkNewUrl';
			$flagAry[$flagCount]['value'] = $eHomeworkNewUrl;
			$flagCount++;

			##Add School News URL
			if($this->isSchoolNewsAdmin($parUserId)){
			$addSchoolNewsUrl = $schoolDomain.'/home/eClassApp/teacherApp/schoolNews/schoolNews_new.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'addSchoolNewsUrl';
			$flagAry[$flagCount]['value'] = $addSchoolNewsUrl;
			$flagCount++;
			}

			## show attendance details
			$eClassAppSettingsObj = $this->getAppSettingsObj();
			$eAttendanceEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']);
			if($eAttendanceEnable == null){
				$eAttendanceEnable = '1';
			}
			$flagAry[$flagCount]['key'] = 'showAttendanceDetails';
			$flagAry[$flagCount]['value'] = ($eAttendanceEnable)? '1' : '0';
			$flagCount++;

			## hide app late status
			$eClassAppSettingsObj = $this->getAppSettingsObj();
			$eAttendanceHideLateStatus  = $sys_custom['eClassApp']['eAttendanceHideLateStatus'];
			if($eAttendanceHideLateStatus == null){
				$eAttendanceHideLateStatus = '0';
			}
			$flagAry[$flagCount]['key'] = 'eAttendanceHideLateStatus';
			$flagAry[$flagCount]['value'] = ($eAttendanceHideLateStatus)? '1' : '0';
			$flagCount++;

			## hide app early leave status
			$eClassAppSettingsObj = $this->getAppSettingsObj();
			$eAttendanceHideEarlyLeaveStatus  = $sys_custom['eClassApp']['eAttendanceHideEarlyLeaveStatus'];
			if($eAttendanceHideEarlyLeaveStatus == null){
				$eAttendanceHideEarlyLeaveStatus = '0';
			}
			$flagAry[$flagCount]['key'] = 'eAttendanceHideEarlyLeaveStatus';
			$flagAry[$flagCount]['value'] = ($eAttendanceHideEarlyLeaveStatus)? '1' : '0';
			$flagCount++;

            ### attendance status statistics in eAttendance module
			$eClassAppSettingsObj = $this->getAppSettingsObj();
            $eAttendanceStatusStatisticsEnable = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']);
			if($eAttendanceStatusStatisticsEnable == null){
				$eAttendanceStatusStatisticsEnable = 1;
			}
			$flagAry[$flagCount]['key'] = 'eAttendanceStatusStatisticsEnable';
			$flagAry[$flagCount]['value'] = ($eAttendanceStatusStatisticsEnable)? '1' : '0';
			$flagCount++;

			## home page attendance view status
			$eClassAppSettingsObj = $this->getAppSettingsObj();
			$attendanceStatusEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']);
			if($attendanceStatusEnable == null){
				$attendanceStatusEnable = 1;
			}
			$attendanceTimeEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']);
			if($attendanceTimeEnable == null){
				$attendanceTimeEnable = 1;
			}
			$attendanceLeaveSectionEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']);
			if($attendanceLeaveSectionEnable == null){
				$attendanceLeaveSectionEnable = 1;
			}

			if (!$attendanceStatusEnable && !$attendanceTimeEnable)  {
				$flagValue = $eclassAppConfig['eClassApp']['home']['attendanceView']['displayStyle']['hide'];
			} else if (!$attendanceLeaveSectionEnable) {
				$flagValue = $eclassAppConfig['eClassApp']['home']['attendanceView']['displayStyle']['amOnly'];
			} else {
				$flagValue = $eclassAppConfig['eClassApp']['home']['attendanceView']['displayStyle']['full'];
			}
			$flagAry[$flagCount]['key'] = 'homeAttendanceViewStyle';
			$flagAry[$flagCount]['value'] = $flagValue;
			$flagCount++;

			## group message api path
			$flagAry[$flagCount]['key'] = 'groupMessageApiPath';
			$flagAry[$flagCount]['value'] = $eclassAppConfig['groupMessage']['apiPath'];
			$flagCount++;

			## group message web socket path
			$flagAry[$flagCount]['key'] = 'groupMessageWsPath';
			$flagAry[$flagCount]['value'] = $eclassAppConfig['groupMessage']['wsPath'];
			$flagCount++;

			if (!$plugin['DisableiCalendar']) {
				$flagAry[$flagCount]['key'] = 'iCalendarSSOParam';
				$flagAry[$flagCount]['value'] = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['iCalendar']);
				$flagCount++;
			}

			if ($plugin['medical']) {
			    #New Medical Caring Bowel Movement in Teacher App
			    $flagAry[$flagCount]['key'] = 'medicalCaringBowelSSOParam';
			    $flagAry[$flagCount]['value'] = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['medicalCaringBowel']);
			    $flagCount++;
			    #New Medical Caring Bowel Movement in Teacher App (Webview)
			    $flagAry[$flagCount]['key'] = 'medicalCaringBowelSSOParam_v2';
			    $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['medicalCaringBowel_v2']);
			    $flagCount++;

			    #New Medical Caring Student Log in Teacher App
			    $flagAry[$flagCount]['key'] = 'medicalCaringLogSSOParam';
			    $flagAry[$flagCount]['value'] = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['medicalCaringLog']);
			    $flagCount++;
			    #New Medical Caring Student Log in Teacher App
			    $flagAry[$flagCount]['key'] = 'medicalCaringLogSSOParam_v2';
			    $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['medicalCaringLog_v2']);
			    $flagCount++;

			    #New Medical Caring Sleep Record in Teacher App
			    $flagAry[$flagCount]['key'] = 'medicalCaringSleepSSOParam';
			    $flagAry[$flagCount]['value'] = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['medicalCaringSleep']);
			    $flagCount++;
			    #New Medical Caring Sleep Record in Teacher App (Webview)
			    $flagAry[$flagCount]['key'] = 'medicalCaringSleepSSOParam_v2';
			    $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['medicalCaringSleep_v2']);
			    $flagCount++;
			}

			#ePayment KIS_OnlyShowPaymentRecords setting
			$ePayment_KIS_OnlyShowPaymentRecords = "0";
			if($sys_custom['ePayment']['KIS_OnlyShowPaymentRecords']){
				$ePayment_KIS_OnlyShowPaymentRecords = "1";
			}
			$flagAry[$flagCount]['key'] = 'ePayment_KIS_OnlyShowPaymentRecords';
			$flagAry[$flagCount]['value'] = $ePayment_KIS_OnlyShowPaymentRecords;
			$flagCount++;

			## timetable url
			$timeTableUrl = $schoolDomain.'/home/eClassApp/studentApp/timeTable/showTimeTable.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'timetableUrl';
			$flagAry[$flagCount]['value'] = $timeTableUrl;
			$flagCount++;

			## Classroom setting
            $flagAry[$flagCount]['key'] = 'ClassroomSSOParam';
            $flagAry[$flagCount]['value'] = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['classroom']);
            $flagCount++;

			## eBooking url
            if ($plugin['eBooking']) {
                $eBookingUrl = $schoolDomain.'/home/eClassApp/common/web_module/?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin.'#ebooking/management/list';
                $flagAry[$flagCount]['key'] = 'eBookingUrl';
                $flagAry[$flagCount]['value'] = $eBookingUrl;
                $flagCount++;
            }

			## eDiscipline url
			if ($plugin['Disciplinev12']) {
			    //$eDisciplineUrl = $schoolDomain.'/home/eClassApp/common/web_module/?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin.'#ediscipline/goodconduct_misconduct/management/mylist';
			    $eDisciplineSsoParam = $this->getSSOParam($parUserId, $eclassAppConfig['moduleCode']['eDiscipline']);
			    $eDisciplineUrl = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$eDisciplineSsoParam.'#ediscipline/goodconduct_misconduct/management/mylist';	//keep the para after "#" for app hardcoded to detect "#"
			    $flagAry[$flagCount]['key'] = 'eDisciplineUrl';
			    $flagAry[$flagCount]['value'] = $eDisciplineUrl;
			    $flagCount++;
			}


			### Hostel take attendance
			$hostelTakeAttendanceUrl = $schoolDomain.'/home/eAdmin/StudentMgmt/attendance/dailyoperation/hostel/app_view/attendance_list.php?token='.$token.'&uid='.$parUserId.'&ul='.$parUserLogin;
			$flagAry[$flagCount]['key'] = 'hostelTakeAttendanceUrl';
			$flagAry[$flagCount]['value'] = $hostelTakeAttendanceUrl;
			$flagCount++;

			### Student App iPortfolio
			$flagAry[$flagCount]['key'] = 'iPortfolioUrl';
			$flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'iPortfolio');
			$flagCount++;

		    ### DisableRightClick setting
            //if using Photo Album, check the flag to control the download button. If using Digital Channles, check the settings to control the download button.

            include_once ($intranet_root.'/includes/libgeneralsettings.php');
            $LibGeneralSettings = new libgeneralsettings();
            $LibGeneralSettings_DigitalChannels = $LibGeneralSettings->Get_General_Setting("DigitalChannels",array("'AllowDownloadOriginalPhoto'"));

            $KIS_Album_Hide =  $sys_custom['KIS_Album']['DisableRightClick'];
            $Digital_Channel_Allow = $LibGeneralSettings_DigitalChannels['AllowDownloadOriginalPhoto'];

            if($plugin['DigitalChannels'] && $Digital_Channel_Allow != '1'){
                $KIS_DisableRightClick = '1';
            }else if(isKIS() && !$plugin['DigitalChannels'] && $KIS_Album_Hide == '1'){
                $KIS_DisableRightClick = '1';
            }else{
                $KIS_DisableRightClick = '0';
            }

			$flagAry[$flagCount]['key'] = 'KIS_DisableRightClick';
			$flagAry[$flagCount]['value'] = $KIS_DisableRightClick;
			$flagCount++;

			if ($plugin['eSchoolBus']) {
			    $flagAry[$flagCount]['key'] = 'eSchoolBusSsoUrl';
			    $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eSchoolBus');
			    $flagCount++;

                $flagAry[$flagCount]['key'] = 'eSchoolBusTeacherSsoUrl';
                $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eSchoolBusTeacher');
                $flagCount++;
			}

			if ($plugin['eEnrollment']) {
			    $flagAry[$flagCount]['key'] = 'eEnrolmentUserViewSsoUrl';
			    $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eEnrolmentUserView');
			    $flagCount++;
			}

            if ($plugin['eEnrollment']) {
                $flagAry[$flagCount]['key'] = 'eEnrolmentStudentViewSsoUrl';
                $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eEnrolmentStudentView');
                $flagCount++;
            }

			if ($plugin['DigitalChannels']) {
			    $flagAry[$flagCount]['key'] = 'DigitalChannelsEnable';
			    $flagAry[$flagCount]['value'] = '1';
			    $flagCount++;

			    ## TODO: check user can add album / upload photos
			    include_once($intranet_root.'/includes/DigitalChannels/libdigitalchannels.php');
			    $libdigitalchannels = new libdigitalchannels ();
			    $userCanUploadPhoto = $libdigitalchannels->AppUserCanUploadPhotos($parUserId);

			    $flagAry[$flagCount]['key'] = 'DigitalChannelsUserCanUploadPhotos';
			    $flagAry[$flagCount]['value'] = $userCanUploadPhoto;
			    $flagCount++;
			}

           if ($plugin['ePOS']) {
                $flagAry[$flagCount]['key'] = 'ePOSSsoUrl';
                $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'ePOS');
                $flagCount++;

               /*
                $flagAry[$flagCount]['key'] = 'ePosTeacherSsoUrl';
                $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'ePosStudent');
                $flagCount++; */
           }
           
           if ($plugin['Inventory']) {
               $flagAry[$flagCount]['key'] = 'eInventorySsoUrl';
               $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eInventory');
               $flagCount++;
               
               $flagAry[$flagCount]['key'] = 'eInventoryStocktakeSsoUrl';
               $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eInventoryStocktake');
               $flagCount++;
           }
           
		   $flagAry[$flagCount]['key'] = 'eLearningTimetableUrl';
		   $flagAry[$flagCount]['value'] = $schoolDomain.'/api/eClassApp/sso_intranet.php?'.$this->getSSOParam($parUserId, 'eLearningTimetable');
		   $flagCount++;

			return $flagAry;
		}

		function getUserModuleAccessRight($parUserId, $parAppType='') {
		    global $PATH_WRT_ROOT, $eclassAppConfig, $sys_custom, $plugin, $eclass_db;

//			include_once($PATH_WRT_ROOT.'includes/libuser.php');
//			$luser = new libuser($parUserId);
			$luser = $this->getUserObj($parUserId);
			$recordType = $luser->RecordType;
			$isTeaching = $luser->teaching;

			if ($recordType==USERTYPE_STAFF) {
				$appType = $eclassAppConfig['appType']['Teacher'];
				$formId = 0;

				$groupMessageTargetUserIdAry = array($parUserId);
			}
			else if ($recordType==USERTYPE_STUDENT) {
				$appType = ($parAppType=='')? $eclassAppConfig['appType']['Parent'] : $parAppType;
				$sql = "Select
								yc.YearID
						From
								YEAR_CLASS as yc
								Inner Join YEAR_CLASS_USER as ycu On (yc.YearClassID = ycu.YearClassID)
						Where
								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								And ycu.UserID = '".$parUserId."'
						";
				$formInfoAry = $this->returnResultSet($sql);
				$formId = $formInfoAry[0]['YearID'];

				$sql = "Select ParentID From INTRANET_PARENTRELATION Where StudentID = '".$parUserId."'";
				$groupMessageTargetUserIdAry = Get_Array_By_Key($this->returnResultSet($sql), 'ParentID');
			}

			## Get form-based app access right settings
			$accessRightAssoAry = $this->getAccessRightInfo($appType);

			$returnAry = array();
			if (isset($accessRightAssoAry[$formId])) {
				$returnAry = $accessRightAssoAry[$formId];

				// do not show "Group Message" if the user has no related groups
				if ($sys_custom['eClassApp']['groupMessage']) {
					include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
					$lgroupMessage = new libeClassApp_groupMessage();
					$groupAry = $lgroupMessage->getGroupMemberList($groupIdAry='', $groupMessageTargetUserIdAry);
//					if (count($groupAry) > 0) {
//						$returnAry[$eclassAppConfig['moduleCode']['groupMessage']]['RecordStatus'] = '1';
//					}
//					else {
//						$returnAry[$eclassAppConfig['moduleCode']['groupMessage']]['RecordStatus'] = '0';
//					}
					if (count($groupAry) == 0) {
						$returnAry[$eclassAppConfig['moduleCode']['groupMessage']]['RecordStatus'] = '0';
					}
					if ($recordType==USERTYPE_STAFF && $sys_custom['eClassTeacherApp']['GroupMessage']['showOneToOne'] && ($_SESSION["USER_BASIC_INFO"]["is_class_teacher"] || $_SESSION["USER_BASIC_INFO"]["is_subject_teacher"])) {
						// need to enable module for teacher to add 1-to-1 message even they have no group
						$returnAry[$eclassAppConfig['moduleCode']['groupMessage']]['RecordStatus'] = '1';
					}
				}

				// do not show "School Info" if there are no public page in first layer
				if ($sys_custom['eClassApp']['schoolInfo']) {
					include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_schoolInfo.php');
					$lschoolInfo = new libeClassApp_schoolInfo();
					$firstLayerMeunAry = $lschoolInfo->getItemDetail($ParentMenuID=0);

					if (count($firstLayerMeunAry) == 0) {
						$returnAry[$eclassAppConfig['moduleCode']['schoolInfo']]['RecordStatus'] = '0';
					}
				}

				if($returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'] == '1'){
					if($sys_custom['ePayment']['MultiPaymentGatewayTopUp']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentMultipleTopUp']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					} else if($sys_custom['ePayment']['MultiPaymentGateway']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentMultiple']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					} else if ($sys_custom['ePayment']['TNG']) {
                        $returnAry[$eclassAppConfig['moduleCode']['ePaymentTng']]['RecordStatus']= $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
                        $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
                    }else if($sys_custom['ePayment']['Alipay']) {
                        $returnAry[$eclassAppConfig['moduleCode']['ePaymentAlipay']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
                        $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
                    }else if($sys_custom['ePayment']['TAPANDGO']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentTapAndGo']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					}else if($sys_custom['ePayment']['FPS']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentFps']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					}else if($sys_custom['ePayment']['VisaMaster']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentVisaMaster']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					}else if($sys_custom['ePayment']['WeChat']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentWeChat']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					}else if($sys_custom['ePayment']['AlipayCN']) {
						$returnAry[$eclassAppConfig['moduleCode']['ePaymentAlipayCN']]['RecordStatus'] = $returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus'];
						$returnAry[$eclassAppConfig['moduleCode']['ePayment']]['RecordStatus']='0';
					}
                }

//				// 20150713 Roy: eAttendance access right checking
//				$eClassAppSettingsObj = $this->getAppSettingsObj();
//				$eAttendanceEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']);
//				if($eAttendanceEnable == null){
//					$eAttendanceEnable = 1;
//				}
//				if (!$eAttendanceEnable) {
//					$returnAry[$eclassAppConfig['moduleCode']['eAttendance']]['RecordStatus'] = '0';
//				}
			}

			if ($recordType==USERTYPE_STAFF) {
				// teacher status access right checking
				$sql = "SELECT SettingValue FROM INTRANET_APP_SETTINGS WHERE SettingName = '".$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherStatus']."'";
				$teacherStatusSettingAry = $this->returnResultSet($sql);
				$teacherStatusSetting = $teacherStatusSettingAry[0]['SettingValue'];
				if($teacherStatusSetting == 0){
					$sql = "SELECT * FROM INTRANET_APP_TEACHER_STATUS_USER WHERE UserID = '".$parUserId."'";
					$teacherStatusValid = $this->returnResultSet($sql);
					if(count($teacherStatusValid)<=0){
						$returnAry[$eclassAppConfig['moduleCode']['teacherStatus']]['RecordStatus'] = '0';
					}
				}

				// student status access right checking
				$sql = "Select
								Distinct(yct.YearClassID)
						From
								YEAR_CLASS_TEACHER as yct
								Inner Join YEAR_CLASS as yc On (yct.YearClassID = yc.YearClassID)
						Where
								yct.UserID = '".$parUserId."'
								AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				";
				$classAry = $this->returnResultSet($sql);
				if (count($classAry) == 0) {
					$eClassAppSettingsObj = $this->getAppSettingsObj();
					$nonClassTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewAllStudentList']);
					if($nonClassTeacherClassStudentListRight != 1){
					$returnAry[$eclassAppConfig['moduleCode']['studentStatus']]['RecordStatus'] = '0';
					}
				}


				// staff attendance
				if (!$plugin['attendancestaff']) {
					$returnAry[$eclassAppConfig['moduleCode']['staffAttendance']]['RecordStatus'] = '0';
				}

				// override take attendance setting[#K114594]
				include_once($PATH_WRT_ROOT.'includes/libgeneralsettings.php');
				$GeneralSetting = new libgeneralsettings();
                if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
                	$SettingList[] = "'DisallowNonTeachingStaffTakeAttendance'";
	                $Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
					$disallowNonTeachingStaffTakeAttendance = $Settings['DisallowNonTeachingStaffTakeAttendance'];
					if ($disallowNonTeachingStaffTakeAttendance==1&&$isTeaching==0) {
						$returnAry[$eclassAppConfig['moduleCode']['takeAttendance']]['RecordStatus'] = '0';
					}
                }

				if($sys_custom['StudentAttendance']['HostelAttendance'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $recordType==USERTYPE_STAFF){
					include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
					$lcardstudentattend2 = new libcardstudentattend2();
					$hostel_attendance_admin_users = $lcardstudentattend2->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
					$hostel_attendance_groups = $lcardstudentattend2->getHostelAttendanceGroupsConfirmedRecords(array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>date("Y-m-d"),'CategoryID'=>$lcardstudentattend2->HostelAttendanceGroupCategory,'IsGroupStaff'=>true,'GroupUserID'=>$parUserId));

					if(count($hostel_attendance_groups) == 0 && in_array($parUserId,$hostel_attendance_admin_users) == false) {
						if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
							$returnAry[$eclassAppConfig['moduleCode']['hostelTakeAttendance']]['RecordStatus'] = '0';
						}
					}
				}

                // check if not iPo admin, hide iPo button (schhol setting> role
                $sql = "Select rr.* from ROLE_MEMBER   as rm 
                        INNER JOIN ROLE_RIGHT  as rr ON rm.RoleID = rr.RoleID
                        where rm.UserID= '".$parUserId."' and FunctionName = 'other-iPortfolio'";
                $RoleAry = $this->returnResultSet($sql);
                if (count($RoleAry) == 0) { // not iPortfolio admin , hide button
                    $returnAry[$eclassAppConfig['moduleCode']['iPortfolio']]['RecordStatus'] = '0';
                }

			}

			if ($recordType==USERTYPE_STUDENT) {
			    // hide iPortfolio if the student has not activated iPf license
			    $sql = "SELECT RecordID FROM $eclass_db.PORTFOLIO_STUDENT WHERE UserID = '".$parUserId."' AND IsSuspend = 0";
			    $iPfLicenseAry = $this->returnResultSet($sql);
			    if ($iPfLicenseAry[0]['RecordID'] > 0) {
			        // has iPf license => do nth
			    }
			    else {
			        // hide iPf in student app
			        $returnAry[$eclassAppConfig['moduleCode']['iPortfolio']]['RecordStatus'] = '0';
			    }
			}
			return $returnAry;
		}

		function isTokenValid($parToken, $parUserId, $parUserLogin) {
			$token = $this->getUrlToken($parUserId, $parUserLogin);

			$valid = false;
			if ($parToken == $token) {
				$valid = true;
			}

			return $valid;
		}

		function getUrlToken($parUserId, $parUserLogin) {
			global $PATH_WRT_ROOT, $special_feature, $eclassAppConfig;
			include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

			$delimiter = '###';
			$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$parUserId.$delimiter.$parUserLogin.$eclassAppConfig['urlSalt'];
			$token = md5($keyStr);

			return $token;
		}

		function handleInvalidToken() {
			global $i_general_no_access_right;
			echo $i_general_no_access_right	;
			exit;
		}

		function getUserInfoByUserLogin($parUserLogin) {
			// get UserID from UserLogin
			$sql = "Select UserID, RecordType From INTRANET_USER Where UserLogin = '".$this->Get_Safe_Sql_Query($parUserLogin)."'";
			$userAry = $this->returnResultSet($sql);

			return $userAry;
		}

		function getArchivedUserInfoByUserLogin($parUserLogin) {
			// get UserID from UserLogin
			$sql = "Select UserID, RecordType From INTRANET_ARCHIVE_USER Where UserLogin = '".$this->Get_Safe_Sql_Query($parUserLogin)."'";
			$userAry = $this->returnResultSet($sql);

			return $userAry;
		}

		function getUserPassInfo($userId, $userLogin) {
		    global $PATH_WRT_ROOT, $intranet_password_salt;
            include_once($PATH_WRT_ROOT."includes/libauth.php");

            $li = new libauth();
            return md5(strtolower($userLogin).$li->GetUserDecryptedPassword($userId).$intranet_password_salt);
		}

		function saveUserDevice($parUserLogin, $parDeviceId, $parDeviceOS, $parDeviceModel='', $parApnsType='', $parWpnsUri='', $parUserId='') {
			global $eclassAppConfig, $sys_custom;
			$successAry = array();

			$needSyncCloud = false;
			if ($parUserLogin == '' && $parUserId == '') {
				return '104';
			}
			else if ($parUserLogin != '' && $parUserId == '') {
				$parUserLogin = $this->getDemoSiteUserLogin($parUserLogin);
				$userAry = $this->getUserInfoByUserLogin($parUserLogin);
				$targetUserId = $userAry[0]['UserID'];
				if ($targetUserId == '') {
					return '104';
				}
			}
			else {
				$targetUserId = $parUserId;
			}


			$parDeviceId = str_replace(' ', '', $parDeviceId);	// remove space in device id
			if(strlen($parDeviceId) == 0) {
				return '';
			}

			$parApnsType = strtolower($parApnsType);
			$maxDevicePerUser = $this->getMaxDevicePerUser();

			if ($parDeviceOS == '') {
				$parDeviceOS = $eclassAppConfig['deviceOS']['Android'];
			}
			else if (strtolower($parDeviceOS) == 'iphone os') {
				$parDeviceOS = $eclassAppConfig['deviceOS']['iOS'];
			}

			// insert device record for user if not appeared yet
			// find existing device record (logged in / logged out) in DB
			$sql = "Select
						 RecordID, DeviceModel, DeviceOS, APNSType 
					From 
						APP_USER_DEVICE 
					Where 
						UserID = '".$targetUserId."' 
					And DeviceID = '".$this->Get_Safe_Sql_Query($parDeviceId)."'
					And DeviceOS = '".$parDeviceOS."' 
					And (	
								RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."'
							Or (	
										RecordStatus= '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive']."' 
									And LoginStatus='".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedOut']."'
								)
						)";
			$deviceInfoAry = $this->returnResultSet($sql);
			$recordId = $deviceInfoAry[0]['RecordID'];
			if ($recordId > 0) {
				// 20150803 Roy: update login status and record status
				$sql = "Update APP_USER_DEVICE Set RecordStatus= '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."', LoginStatus= '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedIn']."', DisableReason = '', DateModified = now() Where RecordID = '".$recordId."' ";
				$successAry['updateDeviceModel'] = $this->db_db_query($sql);
				$needSyncCloud = true;

				// update device model if it is not recorded in the database yet
				$deviceModel = $deviceInfoAry[0]['DeviceModel'];
				if ($deviceModel == '' && $parDeviceModel != '') {
					$sql = "Update APP_USER_DEVICE Set DeviceModel = '".$this->Get_Safe_Sql_Query($parDeviceModel)."', DateModified = now() Where RecordID = '".$recordId."' ";
					$successAry['updateDeviceModel'] = $this->db_db_query($sql);
//					$needSyncCloud = true;
				}

				// update device APNS type
				$deviceOS = $deviceInfoAry[0]['DeviceOS'];
				$apnsType = $deviceInfoAry[0]['APNSType'];
				if ($deviceOS == $eclassAppConfig['deviceOS']['iOS'] && $parApnsType != '' && ($apnsType == '' || $apnsType != $parApnsType)) {
					$sql = "Update APP_USER_DEVICE Set APNSType = '".$this->Get_Safe_Sql_Query($parApnsType)."', DateModified = now() Where RecordID = '".$recordId."' ";
					$successAry['updateAPNSType'] = $this->db_db_query($sql);
//					$needSyncCloud = true;
				}

				// update device WPNSUri
				$deviceOS = $deviceInfoAry[0]['DeviceOS'];
				$wpnsUri = $deviceInfoAry[0]['WPNSUri'];
				if ($deviceOS == $eclassAppConfig['deviceOS']['Win'] && $parWpnsUri != '' && ($wpnsUri == '' || $wpnsUri != $parWpnsUri)) {
					$sql = "Update APP_USER_DEVICE Set WPNSUri = '".$this->Get_Safe_Sql_Query($parWpnsUri)."', DateModified = now() Where RecordID = '".$recordId."' ";
					$successAry['updateWPNSUri'] = $this->db_db_query($sql);
//					$needSyncCloud = true;
				}

			}
			else {
				// insert new device id
				$sql = "insert Into APP_USER_DEVICE 
							(UserID, DeviceID, DeviceOS, DeviceModel, APNSType, WPNSUri, RecordStatus, LoginStatus, DateInput, DateModified) 
						Values 
							('".$targetUserId."', '".$this->Get_Safe_Sql_Query($parDeviceId)."', '".$this->Get_Safe_Sql_Query($parDeviceOS)."', '".$this->Get_Safe_Sql_Query($parDeviceModel)."', '".$this->Get_Safe_Sql_Query($parApnsType)."', '".$this->Get_Safe_Sql_Query($parWpnsUri)."', '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."', '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedIn']."', now(), now())";
				$successAry['insertDevice'] = $this->db_db_query($sql);
				$needSyncCloud = true;
			}

			// inactivate device if exceeded the limitation
			$sql = "Select 
							RecordID 
					From 
							APP_USER_DEVICE 
					Where 
							UserID = '".$targetUserId."' 
							And (
									RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."'
									OR (
											RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive']."' 
											And LoginStatus = '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedOut']."'
										)
								) 
					Order By 
							RecordID";
			$userDeviceAry = $this->returnResultSet($sql);
			$numOfActiveDevice = count($userDeviceAry);
			if ($numOfActiveDevice >= $maxDevicePerUser) {
				// inactive the old device records if exceeded maximum number of device per user
				//$numOfInactiveDevice = $numOfActiveDevice - $eclassAppConfig['maxDevicePerUser'] + 1;
				$numOfInactiveDevice = $numOfActiveDevice - $maxDevicePerUser;
				$inactiveRecordIdAry = array();
				for ($i=0; $i<$numOfInactiveDevice; $i++) {
					$inactiveRecordIdAry[] = $userDeviceAry[$i]['RecordID'];
				}

				$sql = "Update APP_USER_DEVICE set RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive']."', DisableReason = '".$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['tooMuchDevice']."', LoginStatus = '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['deleted']."', DateModified = now() Where RecordID In ('".implode("','", (array)$inactiveRecordIdAry)."') ";
				$successAry['setInactiveDevice'] = $this->db_db_query($sql);
				$needSyncCloud = true;
			}

			$returnAry = array();
			$returnAry['Status'] = (!in_array(false, (array)$successAry))? 'ACK' : '';

			if ($needSyncCloud) {
				$success = $this->saveUserDeviceInfoToCloud($targetUserId);
			}

			return $returnAry;
		}

		function saveUserDeviceInfoToCloud($targetUserIdAry) {
			global $intranet_root, $config_school_code;

			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$userDeviceAry = $this->getUserDevice($targetUserIdAry);
			$userDeviceJsonString = $jsonObj->encode($userDeviceAry);

			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'saveUserDeviceInfo';
			$postParamAry['SchoolCode'] = $config_school_code;
			$postParamAry['TargetUserIdAry'] = $targetUserIdAry;
			$postParamAry['UserDeviceJsonString'] = $userDeviceJsonString;
			$postJsonString = $jsonObj->encode($postParamAry);

			//debug_pr($postParamAry);

			$headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
			$centralServerUrl = $this->getCurCentralServerUrl('groupMessage');

			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			$responseJson = curl_exec($ch);
			curl_close($ch);

			//debug_pr($responseJson);

			$responseJson = standardizeFormPostValue($responseJson);
			$responseJsonAry = $jsonObj->decode($responseJson);
			$saveSuccess = ($responseJsonAry['MethodResult']=='1')? true : false;

			return $saveSuccess;
		}

		function saveUserInfoToCloud($parUserIdAry) {
		    // get related students or parents
		    $sql = "SELECT ParentID, StudentID FROM INTRANET_PARENTRELATION Where ParentID IN ('".implode("','", (array)$parUserIdAry)."') OR StudentID IN ('".implode("','", (array)$parUserIdAry)."')";
		    $parentStudentUserAry = $this->returnResultSet($sql);
		    $parentUserIdAry = Get_Array_By_Key($parentStudentUserAry, 'ParentID');
		    $studentUserIdAry = Get_Array_By_Key($parentStudentUserAry, 'StudentID');

		    // merge all related students, parents, and teachers into a single array
		    $targetUserIdAry = array_values(array_unique(array_merge((array)$parUserIdAry, (array)$parentUserIdAry, (array)$studentUserIdAry)));

		    // get user basic info
		    $userInfoAry = $this->getUserInfo($targetUserIdAry);
		    $numOfUser = count($userInfoAry);

		    // get user device info
		    $userDeviceAssoAry = BuildMultiKeyAssoc($this->getUserDevice($targetUserIdAry, $recordIdAry='', $checkDeviceIdValid=true), 'UserID', array('DeviceID', 'DeviceOS', 'DeviceModel', 'APNSType'), $SingleValue=0, $BuildNumericArray=1);

		    // merge user device info into user basic info array
		    for ($i=0; $i<$numOfUser; $i++) {
		        $_userId = $userInfoAry[$i]['UserID'];

		        $userInfoAry[$i]['userDeviceAry'] = array();
		        if (is_array($userDeviceAssoAry[$_userId])) {
		            $userInfoAry[$i]['userDeviceAry'] = $userDeviceAssoAry[$_userId];
		        }
		    }

		    // send data to central server
		    $resultAry['pushMessageServer'] = $this->sendRequestToCentralServer('pushMessage', 'saveUserInfo', array('userInfoAry' => $userInfoAry, 'parentStudentRelationAry' => $parentStudentUserAry));
		    $resultAry['groupMessageServer'] = $this->sendRequestToCentralServer('groupMessage', 'saveUserInfo', array('userInfoAry' => $userInfoAry, 'parentStudentRelationAry' => $parentStudentUserAry));
		    $saveSuccess = ($resultAry['pushMessageServer']['MethodResult']['success']=='1' || $resultAry['groupMessageServer']['MethodResult']['success']=='1')? true : false;

		    return $saveSuccess;
		}

		function sendRequestToCentralServer($parServerType, $parRequestMethod, $parDataAry) {
		    global $intranet_root, $config_school_code;

		    include_once($intranet_root.'/includes/json.php');
		    $jsonObj = new JSON_obj();

		    $postParamAry = array();
		    $postParamAry['RequestMethod'] = $parRequestMethod;
		    $postParamAry['SchoolCode'] = $config_school_code;
		    foreach ((array)$parDataAry as $_key => $_value) {
		        $postParamAry[$_key] = $_value;
		    }
		    $postJsonString = $jsonObj->encode($postParamAry);

		    $headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
		    $centralServerUrl = $this->getCurCentralServerUrl($parServerType);

		    session_write_close();
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
		    curl_setopt($ch, CURLOPT_HEADER, false);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
		    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		    $responseJson = curl_exec($ch);
		    curl_close($ch);

		    $responseJson = standardizeFormPostValue($responseJson);
		    $responseJsonAry = $jsonObj->decode($responseJson);

		    return $responseJsonAry;
		}

		function getUserDevice($userIdAry, $recordIdAry='', $checkDeviceIdValid=false, $parExcludeDeviceOs='', $showStatus = false) {
			global $eclassAppConfig;
			$eClassAppSettingsObj = $this->getAppSettingsObj();
			$loggedOutTeacherCanReceivePushMessage= $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);


			if ($userIdAry != '') {
				$conds_userId = " And aud.UserID IN ('".implode("','", (array)$userIdAry)."') ";
			}
			if ($recordIdAry != '') {
				$conds_recordId = " And aud.RecordID IN ('".implode("','", (array)$recordIdAry)."') ";
			}

			$leftJoin_intranetUser = "Inner Join INTRANET_USER As iu On (aud.UserID = iu.UserID)";
			if ($loggedOutTeacherCanReceivePushMessage == 1) {
				$conds_teacherLoggedOut = " And ((
												iu.recordType = '1' AND 
														(aud.RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."' OR
														aud.LoginStatus = '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedOut']."')) 
												OR (iu.recordType != '1' AND
														aud.RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."'))";
			} else {
				$conds_teacherLoggedOut = " And aud.RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."'";
			}

			if ($parExcludeDeviceOs != '') {
				$conds_excludeDeviceOs = " And aud.DeviceOS NOT IN ('".implode("','", (array)$parExcludeDeviceOs)."') ";
			}
			
			if ($showStatus){
				$extraField = ", aud.RecordStatus, aud.LoginStatus ";
			}


			$sql = "Select 
						aud.RecordID, aud.UserID, aud.DeviceID, aud.DeviceOS, aud.DeviceModel, aud.APNSType, aud.WPNSUri $extraField
 					From 
						APP_USER_DEVICE As aud 
						$leftJoin_intranetUser
					Where 1
						$conds_userId 
						$conds_recordId
						$conds_teacherLoggedOut 
						$conds_recordStatus
						$conds_excludeDeviceOs
					Group By aud.UserID, aud.DeviceID";
			$deviceIdAry = $this->returnResultSet($sql);

 			if ($checkDeviceIdValid) {
 				$numOfDevice = count($deviceIdAry);
 				for ($i=0; $i<$numOfDevice; $i++) {
 					$_deviceId = $deviceIdAry[$i]['DeviceID'];

 					preg_match('/[0-9a-zA-Z\-\_\:]*/', $_deviceId, $_matches);
					if ($_deviceId == $_matches[0]) {
						// valid
					}
					else {
						// invalid
						unset($deviceIdAry[$i]);
					}
 				}

 				$deviceIdAry = array_values($deviceIdAry);
 			}

 			return $deviceIdAry;
		}

		function getMaxDevicePerUser() {
			global $eclassAppConfig, $sys_custom;

			$maxNum = $eclassAppConfig['maxDevicePerUser'];
			if (isset($sys_custom['eClassApp']['maxDevicePerUser']) && $sys_custom['eClassApp']['maxDevicePerUser'] > 0) {
				$maxNum = $sys_custom['eClassApp']['maxDevicePerUser'];
			}

			return $maxNum;
		}

		function isEnabledSendBulkPushMessageInBatches() {
			global $sys_custom;

			$enable = true;		// default enable
			if (isset($sys_custom['eClassApp']['sendBulkPushMessageInBatches'])) {
				$enable = $sys_custom['eClassApp']['sendBulkPushMessageInBatches'];		// disable on request
			}

			return $enable;
		}
		function getSendPushMessageBatchRecipient() {
			global $sys_custom, $eclassAppConfig;

			$numOfRecipient = $eclassAppConfig['pushMessage']['sendBulkPushMessageInBatchesRecipientPerBatch'];
			if (isset($sys_custom['eClassApp']['sendBulkPushMessageInBatches_recipientPerBatch'])) {
				$numOfRecipient = $sys_custom['eClassApp']['sendBulkPushMessageInBatches_recipientPerBatch'];
			}
			return $numOfRecipient;
		}
		function getSendPushMessageBatchFrequency() {
			global $sys_custom, $eclassAppConfig;

			$frequency = $eclassAppConfig['pushMessage']['sendBulkPushMessageInBatchesFrequency'];
			if (isset($sys_custom['eClassApp']['sendBulkPushMessageInBatches_frequency'])) {
				$frequency = $sys_custom['eClassApp']['sendBulkPushMessageInBatches_frequency'];
			}
			return $frequency;
		}

		function sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='', $moduleRecordID='', $messageAttachment=null) {
			global $eclassAppConfig;

			if ($appType == '') {
				$appType = $eclassAppConfig['appType']['Parent'];
			}
			if ($sendTimeMode == '') {
				$sendTimeMode = $eclassAppConfig['pushMessageSendMode']['now'];
			}
			$notifyMessageId = $this->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus, $appType, $sendTimeMode, $sendTimeString, $parNotifyMessageId, $parNotifyMessageTargetIdAry, $fromModule, $moduleRecordID, $createRecordOnly=true, $messageAttachment);

			// prepare the send by batch variables
			$targetSendTimeMode = $eclassAppConfig['pushMessageSendMode']['scheduled'];
			if ($sendTimeMode == $eclassAppConfig['pushMessageSendMode']['now']) {
				$targetSendTimeTs = strtotime(date('Y-m-d H:i:s'));
			}
			else if ($sendTimeMode == $eclassAppConfig['pushMessageSendMode']['scheduled']) {
				$targetSendTimeTs = strtotime($sendTimeString);
			}

			$numOfRecipientPerBatch = $this->getSendPushMessageBatchRecipient();
			$batchFrequency = $this->getSendPushMessageBatchFrequency();

			// break the push message in batches and send as scheduled push message
			$numOfMessage = count($individualMessageInfoAry);
			$counter = 0;
			for ($i=0; $i<$numOfMessage; $i++) {
				$_recipientUserIdAry = array_keys((array)$individualMessageInfoAry[$i]['relatedUserIdAssoAry']);

				$sql = "Select NotifyMessageTargetID, TargetID From INTRANET_APP_NOTIFY_MESSAGE_TARGET Where NotifyMessageID = '".$notifyMessageId."' AND TargetID IN ('".implode("','", (array)$_recipientUserIdAry)."')";
				$_messageTargetAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('TargetID'), array('NotifyMessageTargetID'), $SingleValue=1, $BuildNumericArray=1);

				$_recipientUserIdAryChunk = array_chunk($_recipientUserIdAry, $numOfRecipientPerBatch);
				$_numOfChunk = count($_recipientUserIdAryChunk);
				for ($j=0; $j<$_numOfChunk; $j++) {
					$__recipientUserIdAry = $_recipientUserIdAryChunk[$j];

					$__notifyMessageTargetIdAry = array();
					$__numOfRecipient = count($__recipientUserIdAry);
					for ($k=0; $k<$__numOfRecipient; $k++) {
						$___recipientUserId = $__recipientUserIdAry[$k];
						$__notifyMessageTargetIdAry = array_merge($__notifyMessageTargetIdAry, (array)$_messageTargetAssoAry[$___recipientUserId]);
					}

					$__targetSendTimeTs = $targetSendTimeTs + ($counter * $batchFrequency);
					$__targetSendTimeString = date('Y-m-d H:i:s', $__targetSendTimeTs);

					if (count($__notifyMessageTargetIdAry) > 0)
					{
						// insert message batch records
						$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_BATCH (NotifyMessageID, SendTime, NotifyMessageTargetIdList, DateInput, InputBy) Values ('".$notifyMessageId."', '".$__targetSendTimeString."', '".implode(',', (array)$__notifyMessageTargetIdAry)."', now(), '".$_SESSION['UserID']."')";
						$successAry[$counter]['INTRANET_APP_NOTIFY_MESSAGE_BATCH'] = $this->db_db_query($sql);

                        // [2019-1118-1635-25235] commented - fixed no view details button
						// send batch message
						//if ($fromModule != 'eNotice') {
						//	$fromModule = '';
						//	$moduleRecordID = '';
						//}

                        // [DM#3736] no view details button for edis push msg
                        if ($fromModule == 'DisciplineDemerit' || $fromModule == 'eDiscipline_Merit' || $fromModule == 'eNoticeS') {
                            $fromModule = '';
                            $moduleRecordID = '';
                        }

						$successAry[$counter]['sendPushMessage'] = $this->resendPushMessage($notifyMessageId, $__notifyMessageTargetIdAry, $targetSendTimeMode, $__targetSendTimeString, $fromModule, $moduleRecordID);
						$counter++;
					}
				}
			}

			return $notifyMessageId;
		}

		/*
		 * $individualMessageInfoAry[0, 1, 2, 3, ...]['relatedUserIdAssoAry'][$recipientUserId] = array(1001, 1002);
		 * $individualMessageInfoAry[0, 1, 2, 3, ...]['messageTitle'] = 'xxx';				<-- for send by csv only
		 * $individualMessageInfoAry[0, 1, 2, 3, ...]['messageContent'] = 'yyyyyyyyyyyyy';	<-- for send by csv only
		 */
//		function sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic='', $recordStatus=1) {
//			global $intranet_root, $config_school_code, $sys_custom, $eclassAppConfig;
//
//			if ($config_school_code == '') {
//				return false;
//			}
//
//			$isPublic = (strtoupper($isPublic)=='Y')? 'Y' : 'N';
//
//			$numOfMessage = count($individualMessageInfoAry);
//			$allRecipientUserIdAry = array();
//			for ($i=0; $i<$numOfMessage; $i++) {
//				$allRecipientUserIdAry = array_merge($allRecipientUserIdAry, array_keys((array)$individualMessageInfoAry[$i]['relatedUserIdAssoAry']));
//			}
//			$allRecipientUserIdAry = array_values(array_unique($allRecipientUserIdAry));
//
//
//			include_once($intranet_root.'/includes/json.php');
//			include_once($intranet_root.'/includes/libuser.php');
//			$jsonObj = new JSON_obj();
//			$userObj = new libuser('', '', $allRecipientUserIdAry);
//			$successAry = array();
//
//
//			### get the device id of the users
//			$userDeviceAssoAry = BuildMultiKeyAssoc($this->getUserDevice($allRecipientUserIdAry), 'UserID', $IncludedDBField=array('RecordID', 'DeviceID'), $SingleValue=0, $BuildNumericArray=1);
//
//
//			### add push message record in school database first
//			$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE
//						(NotifyDateTime, MessageTitle, MessageContent, NotifyFor, IsPublic, NotifyUser, NotifyUserID, RecordStatus, DateInput, DateModified, ServiceProvider)
//					Values
//						(now(), '".$this->Get_Safe_Sql_Query($messageTitle)."', '".$this->Get_Safe_Sql_Query($messageContent)."', '', '".$isPublic."', '', '".$_SESSION['UserID']."', '".$recordStatus."', now(), now(), '".$eclassAppConfig['pushMessage']['serviceProvider']."')
//					";
//			$successAry['insertPushMessageRecord'] = $this->db_db_query($sql);
//			$notifyMessageId = $this->db_insert_id();
//
//			if (!$successAry['insertPushMessageRecord'] || $notifyMessageId <= 0) {
//				return false;
//			}
//
//
//			### consolidate each push message item
//			$pushMsgInfoAry = array();
//			$pushMessagePrivateInfoAry = array();
//			$relatedToInsertSqlAry = array();
//			$messageCount = -1;
//			$messageExtraJsonAry = array();
//			for ($i=0; $i<$numOfMessage; $i++) {
//				$_relatedUserIdAssoAry = $individualMessageInfoAry[$i]['relatedUserIdAssoAry'];
//				$_messageTitle = (isset($individualMessageInfoAry[$i]['messageTitle']))? $individualMessageInfoAry[$i]['messageTitle'] : $messageTitle;
//				$_messageContent = (isset($individualMessageInfoAry[$i]['messageContent']))? $individualMessageInfoAry[$i]['messageContent'] : $messageContent;
//
//				$_deviceCount = 0;
//				$_firstMessageUser = true;
//				$_extraJsonAry = array();
//				$_extraJsonAry['IntranetReferenceID'] = $notifyMessageId;
//				$_extraJsonAry['SchoolCode'] = $config_school_code;
//				foreach ((array)$_relatedUserIdAssoAry as $__recipientUserId => $__relatedToUserIdAry) {
//					$userObj->LoadUserData($__recipientUserId);
//					$__userLogin = $userObj->UserLogin;
//
//					$__deviceInfoAry = $userDeviceAssoAry[$__recipientUserId];
//					$__numOfDevice = count((array)$__deviceInfoAry);
//					$__numOfRelatedToUser = count((array)$__relatedToUserIdAry);
//
//					if ($__numOfDevice == 0 || $__numOfRelatedToUser == 0) {
//						continue;
//					}
//
//					if ($_firstMessageUser || ($_deviceCount + $__numOfDevice) >= $eclassAppConfig['maxDevicePerMessage']) {
//						// consolidate last message extra json string
//						if (!$_firstMessageUser) {
//							$_extraJsonString = $jsonObj->encode($_extraJsonAry);
//							$pushMsgInfoAry[$messageCount]['ExtraJsonString'] = $_extraJsonString;
//						}
//
//						// open a new message
//						// 1) for the first user
//						// 2) if exceeded the limit per push message
//						$messageCount++;
//						$_deviceCount = 0;
//
//						$_extraJsonAry = array();
//						$_extraJsonAry['IntranetReferenceID'] = $notifyMessageId;
//						$_extraJsonAry['SchoolCode'] = $config_school_code;
//
//						$pushMsgInfoAry[$messageCount]['MessageTitle'] = $_messageTitle;
//						$pushMsgInfoAry[$messageCount]['MessageContent'] = $_messageContent;
//
//						$_firstMessageUser = false;
//					}
//					$pushMsgInfoAry[$messageCount]['DeviceIdAry'] = array_values(array_merge((array)$pushMsgInfoAry[$messageCount]['DeviceIdAry'], Get_Array_By_Key($__deviceInfoAry, 'DeviceID')));
//					$_extraJsonAry['UserMapping'][$__userLogin] = $__relatedToUserIdAry;
//
//
//
//					### insert message item to database
//					$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_TARGET
//								(NotifyMessageID, TargetType, TargetID, DateInput, MessageTitle, MessageContent, DateModified)
//							Values
//								('".$notifyMessageId."', 'U', '".$__recipientUserId."', now(), '".$this->Get_Safe_Sql_Query($_messageTitle)."', '".$this->Get_Safe_Sql_Query($_messageContent)."', now())
//							";
//					$successAry['insertPushMessageTarget'][$__recipientUserId] = $this->db_db_query($sql);
//
//					$__notifyMessageTargetId = '';
//					if ($successAry['insertPushMessageTarget'][$__recipientUserId]) {
//						$__notifyMessageTargetId = $this->db_insert_id();
//
//						// prepare INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO insert statement
//						for ($j=0; $j<$__numOfRelatedToUser; $j++) {
//							$___relatedUserId = $__relatedToUserIdAry[$j];
//							$relatedToInsertSqlAry[] = " ('".$__notifyMessageTargetId."', '".$___relatedUserId."', now()) ";
//						}
//
//						// one device one push message
//						for ($j=0; $j<$__numOfDevice; $j++) {
//							$___deviceRecordId = $__deviceInfoAry[$j]['RecordID'];
//							$___deviceId = $__deviceInfoAry[$j]['DeviceID'];
//
//							$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_REFERENCE
//										(NotifyMessageID, NotifyMessageTargetID, DeviceRecordID, DateInput, DateModified)
//									Values
//										('".$notifyMessageId."', '".$__notifyMessageTargetId."', '".$___deviceRecordId."', now(), now())
//									";
//							$successAry['insertPushMessageReference'][$__recipientUserId][$___deviceRecordId] = $this->db_db_query($sql);
//
//							$___notifyMessageReferenceId = '';
//							if ($successAry['insertPushMessageReference'][$__recipientUserId][$___deviceRecordId]) {
//								$___notifyMessageReferenceId = $this->db_insert_id();
//								$pushMessagePrivateInfoAry[$messageCount]['ReferenceIdAry'][] = $___notifyMessageReferenceId;
//							}
//						}
//					}
//				}
//
//				$_extraJsonString = $jsonObj->encode($_extraJsonAry);
//				$pushMsgInfoAry[$messageCount]['ExtraJsonString'] = $_extraJsonString;
//			}
//
//			$relatedToInsertSqlChunkAry = array_chunk($relatedToInsertSqlAry, 1000);
//			$numOfChunk = count((array)$relatedToInsertSqlChunkAry);
//			for ($i=0; $i<$numOfChunk; $i++) {
//				$_chunkAry = $relatedToInsertSqlChunkAry[$i];
//
//				$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO (NotifyMessageTargetID, UserID, DateInput) Values ".implode(', ', (array)$_chunkAry);
//				$successAry['insertPushMessageTargetRelatedTo'][] = $this->db_db_query($sql);
//			}
//
//
//			### send push message by specific service provider
//			include_once($intranet_root.'/includes/eClassApp/pushMessage/lib'.$eclassAppConfig['pushMessage']['serviceProvider'].'.php');
//			$libpushMessage = null;
//			eval ('$libpushMessage = new lib'.$eclassAppConfig['pushMessage']['serviceProvider'].'();');
//			$responseAry = $libpushMessage->sendRequestToCentralServer($config_school_code, $pushMsgInfoAry);
//
//			$numOfMessage = count($pushMsgInfoAry);
//			$resultAry = $responseAry['MethodResult'];
//			$errorAry = $responseAry['ErrorCode'];
//
//			$errorCodeGeneral = implode(', ', (array)$errorAry);
//			$insertReferenceAry = array();
//			for ($i=0; $i<$numOfMessage; $i++) {
//				$_referenceIdAry = $pushMessagePrivateInfoAry[$i]['ReferenceIdAry'];
//				$_numOfReferenceId = count($_referenceIdAry);
//
//				$_multicastId = $resultAry[$i]['multicast_id'];
//				$_successNum = $resultAry[$i]['success'];
//				$_failureNum = $resultAry[$i]['failure'];
//				$_canonicalIdNum = $resultAry[$i]['canonical_ids'];
//
//				for ($j=0; $j<$_numOfReferenceId; $j++) {
//					$__referenceId = $_referenceIdAry[$j];
//
//					$__messageId = $resultAry[$i]['results'][$j]['message_id'];
//					$__errorCode = $resultAry[$i]['results'][$j]['error'];
//
//					$__messageStatus = '';
//					if ($errorCodeGeneral != '') {
//						$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
//						$__errorCode = $errorCodeGeneral;
//					}
//					else if (isset($__errorCode) || $__messageId == '') {
//						$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
//					}
//					else {
//						$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
//					}
//
//					$sql = "Update INTRANET_APP_NOTIFY_MESSAGE_REFERENCE set MulticastID = '".$this->Get_Safe_Sql_Query($_multicastId)."', MessageID = '".$this->Get_Safe_Sql_Query($__messageId)."', ErrorCode = '".$this->Get_Safe_Sql_Query($__errorCode)."', MessageStatus = '".$__messageStatus."' Where RecordID = '".$__referenceId."'";
//					$successAry['updateReferenceData'][$__referenceId] = $this->db_db_query($sql);
//				}
//			}
//
//			return (!in_array(false, (array)$successAry))? $notifyMessageId : false;
//		}

        function excludeSuspendedUser($array_user_id){
            global $intranet_root;
            include_once($intranet_root.'/includes/libuser.php');

            $array_user_id_without_suspended = array();

            foreach((array)$array_user_id as $user_id){
                $userObj = new libuser($user_id);
                $userObj->LoadUserData($user_id);
                if($userObj->RecordStatus != '0'){
                    array_push($array_user_id_without_suspended, $user_id);
                }
            }
            return $array_user_id_without_suspended;
        }

        function sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='', $moduleRecordID='', $createRecordOnly=false, $messageAttachment=null) {
			//debug_pr($individualMessageInfoAry);
			//debug_pr($messageTitle);
			//debug_pr($messageContent);
			//debug_pr($isPublic);
			//debug_pr($recordStatus);
			//debug_pr($appType);
			//debug_pr($sendTimeMode);
			//debug_pr($sendTimeString);
			//debug_pr($parNotifyMessageId);
			//debug_pr($parNotifyMessageTargetIdAry);
			//debug_pr('$fromModule='.$fromModule);
			//debug_pr('$moduleRecordID='.$moduleRecordID);
			//debug_pr($createRecordOnly);

			global $intranet_root, $config_school_code, $sys_custom, $eclassAppConfig,$forceUseClass;
            $forceUseClass = true;

			if (!$sys_custom['eClassApp']['AndroidPushServiceProvider'] || $sys_custom['eClassApp']['AndroidPushServiceProvider'] == '') {
				$androidPushServiceProvider = "gcm";
			} else {
				$androidPushServiceProvider = $sys_custom['eClassApp']['AndroidPushServiceProvider'];
			}

			if ($sys_custom['eClassApp']['hideAppPushMessageDirectLink']) {
				$fromModule = '';
				$moduleRecordID = '';
			}

			if ($config_school_code == '') {
				return false;
			}

			// M74821
			$messageTitle = $this->standardizePushMessageText($messageTitle);
			$messageContent = $this->standardizePushMessageText($messageContent);

			if ($appType == '') {
				$appType = $eclassAppConfig['appType']['Parent'];
			}

			if ($sendTimeMode == '') {
				$sendTimeMode = $eclassAppConfig['pushMessageSendMode']['now'];
			}
			if ($sendTimeMode==$eclassAppConfig['pushMessageSendMode']['scheduled'] && $sendTimeString=='') {
				return false;
			}

			if ($parNotifyMessageId == '') {
				$fromResend = false;
			}
			else {
				$fromResend = true;
			}

			$isPublic = (strtoupper($isPublic)=='Y')? 'Y' : 'N';

			### get all recipient user id to retrieve user data later
			$numOfMessage = count($individualMessageInfoAry);
			$allRecipientUserIdAry = array();
			for ($i=0; $i<$numOfMessage; $i++) {
				$allRecipientUserIdAry = array_merge($allRecipientUserIdAry, array_keys((array)$individualMessageInfoAry[$i]['relatedUserIdAssoAry']));
			}
			$allRecipientUserIdAry = array_values(array_unique($allRecipientUserIdAry));
			$allRecipientUserIdAry = $this->excludeSuspendedUser($allRecipientUserIdAry);

			include_once($intranet_root.'/includes/json.php');
			include_once($intranet_root.'/includes/libuser.php');
			$jsonObj = new JSON_obj();
			$userObj = new libuser('', '', $allRecipientUserIdAry);

			$successAry = array();

			### get the device id of the users
			if ($sys_custom['eClassApp']['AndroidPushServiceProvider'] == "getui") {
				if ($sys_custom['eClassApp']['AndroidPushAutoChooseServiceProvider']) {
					$excludeDeviceOs = '';
				}
				else {
					$excludeDeviceOs = $eclassAppConfig['deviceOS']['Android'];
				}
 			}
 			else {
 				$excludeDeviceOs = $eclassAppConfig['deviceOS']['Android_CN'];
 			}
			$userDeviceAssoAry = BuildMultiKeyAssoc($this->getUserDevice($allRecipientUserIdAry, '', $checkDeviceIdValid=true, $excludeDeviceOs, $showStatus = true), 'UserID', $IncludedDBField=array('RecordID', 'DeviceID', 'DeviceOS', 'APNSType', 'WPNSUri', 'DeviceModel'), $SingleValue=0, $BuildNumericArray=1);

            $attachmentPath = '';

			### add push message record in school database first
			if ($fromResend) {
				$notifyMessageId = $parNotifyMessageId;

				// Get attachment path when resend push message
                $sql = "SELECT AttachmentPath FROM INTRANET_APP_NOTIFY_MESSAGE_ATTACHMENT WHERE NotifyMessageID = '$notifyMessageId'";
                $attachmentResult = $this->returnResultSet($sql);

                if (sizeof($attachmentResult) > 0) {
                    $attachmentPath = $eclassAppConfig['urlFilePath'].$attachmentResult[0]["AttachmentPath"];
                    $attachmentPath = str_replace($intranet_root.'/', '', $attachmentPath);
                    $attachmentPath = str_replace(curPageURL($withQueryString=false, $withPageSuffix=false).'/', '', $attachmentPath);

                    $attachmentPath = curPageURL($withQueryString=false, $withPageSuffix=false).'/'.$attachmentPath;
                }
			}
			else {
				if ($sendTimeMode==$eclassAppConfig['pushMessageSendMode']['now']) {
					$notifyDateTime = 'now()';
				}
				else if ($sendTimeMode==$eclassAppConfig['pushMessageSendMode']['scheduled']) {
					$notifyDateTime = "'".$sendTimeString."'";
				}

				$centralServerUrl = $this->getCurCentralServerUrl();
				$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE 
							(NotifyDateTime, MessageTitle, MessageContent, NotifyFor, IsPublic, NotifyUser, NotifyUserID, RecordStatus, ShowInApp, AppType, ToServer, SendTimeMode, DateInput, DateModified)
						Values
							($notifyDateTime, '".$this->Get_Safe_Sql_Query($messageTitle)."', '".$this->Get_Safe_Sql_Query($messageContent)."', '', '".$isPublic."', '', '".$_SESSION['UserID']."', '".$recordStatus."', 1, '".$appType."', '".$this->Get_Safe_Sql_Query($centralServerUrl)."', '".$sendTimeMode."', now(), now())
						";
				$successAry['insertPushMessageRecord'] = $this->db_db_query($sql);
				$notifyMessageId = $this->db_insert_id();

//				if (!$successAry['insertPushMessageRecord']) {
//					return false;
//				}

				// 2015-6-3 Roy
				if ($successAry['insertPushMessageRecord'] ) {
					$successAry['overrideScheduledPushMessage'] = $this->overrideExistingScheduledPushMessageFromModule($fromModule, $moduleRecordID, $notifyMessageId, $appType);

				} else {
					return false;
				}

				//store attachment if attached
                if($messageAttachment){
                    $attachmentStorageResult = $this->savePushMessageAttachment($notifyMessageId, $messageAttachment);
                    if($attachmentStorageResult['uploadSuccess']){
                        $attachmentPath = $attachmentStorageResult['targetFilePath'];
                    }
                }
			}

//			if ($fromModule != 'eNotice') {
//				$fromModule = '';
//				$moduleRecordID = '';
//			}

			if ($notifyMessageId <= 0) {
				return false;
			}

			if($fromModule == 'eNoticeS') {
				$fromModule = '';
				$moduleRecordID = '';
			}

			### insert other message related data
			### consolidate each push message item of each service provider into $individualMessageInfoAry[$i]['deviceIdAssoAry'][$serviceProvider] = array of deviceId
			$relatedToInsertSqlAry = array();
			$referenceInsertSqlAry = array();
			for ($i=0; $i<$numOfMessage; $i++) {
				$_relatedUserIdAssoAry = $individualMessageInfoAry[$i]['relatedUserIdAssoAry'];
				$_messageTitle = (isset($individualMessageInfoAry[$i]['messageTitle']))? $individualMessageInfoAry[$i]['messageTitle'] : $messageTitle;
				$_messageContent = (isset($individualMessageInfoAry[$i]['messageContent']))? $individualMessageInfoAry[$i]['messageContent'] : $messageContent;

				// M74821
				$_messageTitle = $this->standardizePushMessageText($_messageTitle);
				$_messageContent = $this->standardizePushMessageText($_messageContent);

				foreach ((array)$_relatedUserIdAssoAry as $__recipientUserId => $__relatedToUserIdAry) {
					$userObj->LoadUserData($__recipientUserId);
					$__userLogin = $userObj->UserLogin;

					$__deviceInfoAry = $userDeviceAssoAry[$__recipientUserId];
					$__numOfDevice = count((array)$__deviceInfoAry);
					$__numOfRelatedToUser = count((array)$__relatedToUserIdAry);

					//if ($__numOfDevice == 0 || $__numOfRelatedToUser == 0) {
					if ($__numOfRelatedToUser == 0) {
						continue;
					}

					$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['relatedUserIdAry'][$__userLogin] = $__relatedToUserIdAry;
					if ($eclassAppConfig['pushMessage']['apiVersion']['gcm'] == 2) {
						// change to use UserID as key instead of UserLogin in this API version
						// temp include both userlogin and userid for backward compatibility
						$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['relatedUserIdAry'][$__recipientUserId] = $__relatedToUserIdAry;
					}

					$__userDeviceModelAssoAry = BuildMultiKeyAssoc($__deviceInfoAry, array('DeviceModel', 'DeviceOS'));

					//$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['userDeviceAry'] = BuildMultiKeyAssoc($__deviceInfoAry, 'DeviceOS', array('DeviceID'), $SingleValue=1, $BuildNumericArray=1);
					for ($j=0; $j<$__numOfDevice; $j++) {
						$___deviceOs = $__deviceInfoAry[$j]['DeviceOS'];
						$___deviceId = $__deviceInfoAry[$j]['DeviceID'];
						$___apnsType = $__deviceInfoAry[$j]['APNSType'];
						$___wpnsUri = $__deviceInfoAry[$j]['WPNSUri'];
						$___deviceModel = $__deviceInfoAry[$j]['DeviceModel'];
// 						$___deviceRS = $__deviceInfoAry[$j]['RecordStatus'];
// 						$___deviceLS = $__deviceInfoAry[$j]['LoginStatus'];
// 						if($___deviceRS!= $eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']){
// 							continue;
// 						}
// 						if($___deviceLS!= $eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedOut']){
// 							continue;
// 						}

						if ($___deviceOs == $eclassAppConfig['deviceOS']['Android']) {
							$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['userDeviceAry'][$___deviceOs][] = $___deviceId;
						}
						else if ($___deviceOs == $eclassAppConfig['deviceOS']['iOS']) {
							if ($___apnsType == '') {
								$___apnsType = $eclassAppConfig['apnsType']['sandbox'];
							}
							$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['userDeviceAry'][$___deviceOs][$___apnsType][] = $___deviceId;
						}
						else if ($___deviceOs == $eclassAppConfig['deviceOS']['Win']) {
							$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['userDeviceAry'][$___deviceOs][] = $___wpnsUri;
						}
						else if ($___deviceOs == $eclassAppConfig['deviceOS']['Android_CN']) {
							// only send to GeTui if the device has no GCM registration
							$___includeDevice = true;
							if (!$sys_custom['eClassApp']['AndroidPushSendToAllServiceProviders'] && isset($__userDeviceModelAssoAry[$___deviceModel][$eclassAppConfig['deviceOS']['Android']])) {
								$___includeDevice = false;
							}
							if ($___includeDevice) {
								$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['userDeviceAry'][$___deviceOs][] = $___deviceId;
							}
						} else if ($___deviceOs == $eclassAppConfig['deviceOS']['Huawei']) {
							$individualMessageInfoAry[$i]['userAssoAry'][$__recipientUserId]['userDeviceAry'][$___deviceOs][] = $___deviceId;
						}
					}


					### insert message item to database
					$__notifyMessageTargetId = '';
					if ($fromResend) {
						// get the target id by NotifyMessageID, recipient's UserID, message title and content
						$sql = "Select 
										NotifyMessageTargetID 
								From 
										INTRANET_APP_NOTIFY_MESSAGE_TARGET
								Where
										NotifyMessageID = '".$notifyMessageId."'
										And TargetID = '".$__recipientUserId."'
										And MessageTitle = '".$this->Get_Safe_Sql_Query($_messageTitle)."'
										And MessageContent = '".$this->Get_Safe_Sql_Query($_messageContent)."'
								";
						$__targetInfoAry = $this->returnResultSet($sql);
						$__notifyMessageTargetId = $__targetInfoAry[0]['NotifyMessageTargetID'];

						// delete old related to and reference records
						$sql = "Delete From INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO Where NotifyMessageTargetID = '".$__notifyMessageTargetId."'";
						$successAry['deleteOldMessageTargetRelatedTo']['notifyMessageTargetId='.$__notifyMessageTargetId] = $this->db_db_query($sql);

						$sql = "Delete From INTRANET_APP_NOTIFY_MESSAGE_REFERENCE Where NotifyMessageID = '".$notifyMessageId."' and NotifyMessageTargetID = '".$__notifyMessageTargetId."'";
						$successAry['deleteOldMessageReference']['notifyMessageTargetId='.$__notifyMessageTargetId] = $this->db_db_query($sql);
					}
					else {
						$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_TARGET
									(NotifyMessageID, TargetType, TargetID, DateInput, MessageTitle, MessageContent, DateModified)
								Values
									('".$notifyMessageId."', 'U', '".$__recipientUserId."', now(), '".$this->Get_Safe_Sql_Query($_messageTitle)."', '".$this->Get_Safe_Sql_Query($_messageContent)."', now())
								";
						$successAry['insertPushMessageTarget'][$__recipientUserId] = $this->db_db_query($sql);

						if ($successAry['insertPushMessageTarget'][$__recipientUserId]) {
							$__notifyMessageTargetId = $this->db_insert_id();
						}
					}


					if ($__notifyMessageTargetId > 0) {
						// prepare INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO insert statement
						for ($j=0; $j<$__numOfRelatedToUser; $j++) {
							$___relatedUserId = $__relatedToUserIdAry[$j];
							$relatedToInsertSqlAry[] = " ('".$__notifyMessageTargetId."', '".$___relatedUserId."', now()) ";
						}

						if ($__numOfDevice == 0) {
							$referenceInsertSqlAry[] = "('".$notifyMessageId."', '".$__notifyMessageTargetId."', 0, '', '', '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'] ."', now(), now())";
						}
						else {
							for ($j=0; $j<$__numOfDevice; $j++) {
								$___deviceRecordId = $__deviceInfoAry[$j]['RecordID'];
								$___deviceId = $__deviceInfoAry[$j]['DeviceID'];
								$___deviceOs = $__deviceInfoAry[$j]['DeviceOS'];
								$___apnsType = $__deviceInfoAry[$j]['APNSType'];
								$___pushMessageServiceProvider = $eclassAppConfig['pushMessage']['serviceProvider'][$___deviceOs];

								if ($___deviceOs == $eclassAppConfig['deviceOS']['iOS']) {
									if ($___apnsType == '') {
										$___apnsType = $eclassAppConfig['apnsType']['sandbox'];
									}
								}

								$referenceInsertSqlAry[] = "('".$notifyMessageId."', '".$__notifyMessageTargetId."', '".$___deviceRecordId."', '".$this->Get_Safe_Sql_Query($___pushMessageServiceProvider)."', '".$___apnsType."', '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'] ."', now(), now())";
							}
						}
					}
				}
			//exit();
			}

			$relatedToInsertSqlChunkAry = array_chunk($relatedToInsertSqlAry, 1000);
			$numOfChunk = count((array)$relatedToInsertSqlChunkAry);
			for ($i=0; $i<$numOfChunk; $i++) {
				$_chunkAry = $relatedToInsertSqlChunkAry[$i];

				$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO (NotifyMessageTargetID, UserID, DateInput) Values ".implode(', ', (array)$_chunkAry);
				$successAry['insertPushMessageTargetRelatedTo'][] = $this->db_db_query($sql);
			}

			$referenceInsertSqlChunkAry = array_chunk($referenceInsertSqlAry, 1000);
			$numOfChunk = count((array)$referenceInsertSqlChunkAry);
			for ($i=0; $i<$numOfChunk; $i++) {
				$_chunkAry = $referenceInsertSqlChunkAry[$i];

				$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_REFERENCE (NotifyMessageID, NotifyMessageTargetID, DeviceRecordID, ServiceProvider, APNSType, MessageStatus, DateInput, DateModified) Values ".implode(', ', (array)$_chunkAry);
				$successAry['insertPushMessageReference'][] = $this->db_db_query($sql);
			}

			/**
			 * Due to adding fromModule = 'SchoolNews', Android app
			 */
			### separate the message info for each service provide
			$messageCountAry = array();
			foreach ((array)$eclassAppConfig['pushMessage']['serviceProvider'] as $_deviceOs => $_serviceProvider) {
				$messageCountAry[$_deviceOs] = 0;
			}
			$pushMessageAssoAry = array();
			foreach ((array)$eclassAppConfig['pushMessage']['serviceProvider'] as $_deviceOs => $_serviceProvider) {
				//$_messageCount = 0;
				for ($i=0; $i<$numOfMessage; $i++) {
					$__messageTitle = (isset($individualMessageInfoAry[$i]['messageTitle']))? $individualMessageInfoAry[$i]['messageTitle'] : $messageTitle;
					$__messageContent = (isset($individualMessageInfoAry[$i]['messageContent']))? $individualMessageInfoAry[$i]['messageContent'] : $messageContent;

					$__userAssoAry = $individualMessageInfoAry[$i]['userAssoAry'];
					$__tmpUserAssoAry = array();

					//if ($_deviceOs == $eclassAppConfig['deviceOS']['Android'] && strcmp($eclassAppConfig['pushMessage']['serviceProvider']['Android'], $androidPushServiceProvider)==0) {
					if ($_deviceOs == $eclassAppConfig['deviceOS']['Android']) {
						foreach ((array)$__userAssoAry as $___recipientUserId => $___userInfoAry) {
							if (count((array)$___userInfoAry['userDeviceAry'][$_deviceOs]) > 0) {
								$__tmpUserAssoAry[$___recipientUserId]['relatedUserIdAry'] = $___userInfoAry['relatedUserIdAry'];
								$__tmpUserAssoAry[$___recipientUserId]['deviceIdAry'] = $___userInfoAry['userDeviceAry'][$_deviceOs];
							}
						}

						if ($__tmpUserAssoAry) {
							$__msgCounter = $messageCountAry[$eclassAppConfig['deviceOS']['Android']];

							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['messageTitle'] = $__messageTitle;
							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['messageContent'] = $__messageContent;
							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['userAssoAry'] = $__tmpUserAssoAry;
							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['fromModule'] = $fromModule;
							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['moduleRecordID'] = $moduleRecordID;
                            $pushMessageAssoAry[$_serviceProvider][$__msgCounter]['attachmentPath'] = $attachmentPath;
							$messageCountAry[$eclassAppConfig['deviceOS']['Android']]++;
						}
					}
					else if ($_deviceOs == $eclassAppConfig['deviceOS']['iOS']) {
						$__isAllContinued = true;
						foreach((array)$eclassAppConfig['apnsType'] as $__apnsTypeCode => $__apnsTypeValue) {
							$__tmpUserAssoAry = array();
							foreach ((array)$__userAssoAry as $___recipientUserId => $___userInfoAry) {

								if ($___userInfoAry['userDeviceAry'][$_deviceOs][$__apnsTypeCode] == '') {
									continue;
								}

								$__tmpUserAssoAry[$___recipientUserId]['relatedUserIdAry'] = $___userInfoAry['relatedUserIdAry'];
								$__tmpUserAssoAry[$___recipientUserId]['deviceIdAry'] = $___userInfoAry['userDeviceAry'][$_deviceOs][$__apnsTypeCode];


								$__msgCounter = $messageCountAry[$eclassAppConfig['deviceOS']['iOS']];
								$pushMessageAssoAry[$_serviceProvider][$__apnsTypeCode][$appType][$__msgCounter]['messageTitle'] = $__messageTitle;
								$pushMessageAssoAry[$_serviceProvider][$__apnsTypeCode][$appType][$__msgCounter]['messageContent'] = $__messageContent;
								$pushMessageAssoAry[$_serviceProvider][$__apnsTypeCode][$appType][$__msgCounter]['userAssoAry'] = $__tmpUserAssoAry;
								$pushMessageAssoAry[$_serviceProvider][$__apnsTypeCode][$appType][$__msgCounter]['fromModule'] = $fromModule;
								$pushMessageAssoAry[$_serviceProvider][$__apnsTypeCode][$appType][$__msgCounter]['moduleRecordID'] = $moduleRecordID;
                                $pushMessageAssoAry[$_serviceProvider][$__apnsTypeCode][$appType][$__msgCounter]['attachmentPath'] = $attachmentPath;

								$__isAllContinued = false;
							}
						}

						if (!$__isAllContinued) {
							$messageCountAry[$eclassAppConfig['deviceOS']['iOS']]++;
						}
					}else if ($_deviceOs == $eclassAppConfig['deviceOS']['Win']) {
						foreach ((array)$__userAssoAry as $___recipientUserId => $___userInfoAry) {
							if (count((array)$___userInfoAry['userDeviceAry'][$_deviceOs]) > 0) {
								$__tmpUserAssoAry[$___recipientUserId]['relatedUserIdAry'] = $___userInfoAry['relatedUserIdAry'];
								$__tmpUserAssoAry[$___recipientUserId]['deviceIdAry'] = $___userInfoAry['userDeviceAry'][$_deviceOs];
							}
						}

						if ($__tmpUserAssoAry) {
							$__msgCounter = $messageCountAry[$eclassAppConfig['deviceOS']['Win']];

							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['messageTitle'] = $__messageTitle;
							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['messageContent'] = $__messageContent;
							$pushMessageAssoAry[$_serviceProvider][$__msgCounter]['userAssoAry'] = $__tmpUserAssoAry;

							$messageCountAry[$eclassAppConfig['deviceOS']['Win']]++;
						}
					}
					//else if ($_deviceOs == $eclassAppConfig['deviceOS']['Android_CN'] && strcmp($eclassAppConfig['pushMessage']['serviceProvider']['Android_CN'], $androidPushServiceProvider)==0) {
					else if ($_deviceOs == $eclassAppConfig['deviceOS']['Android_CN']) {
						foreach ((array)$__userAssoAry as $___recipientUserId => $___userInfoAry) {
							if (count((array)$___userInfoAry['userDeviceAry'][$_deviceOs]) > 0) {
								$__tmpUserAssoAry[$___recipientUserId]['relatedUserIdAry'] = $___userInfoAry['relatedUserIdAry'];
								$__tmpUserAssoAry[$___recipientUserId]['deviceIdAry'] = $___userInfoAry['userDeviceAry'][$_deviceOs];
							}
						}

						if ($__tmpUserAssoAry) {
							$__msgCounter = $messageCountAry[$eclassAppConfig['deviceOS']['Android_CN']];

							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['messageTitle'] = $__messageTitle;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['messageContent'] = $__messageContent;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['userAssoAry'] = $__tmpUserAssoAry;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['fromModule'] = $fromModule;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['moduleRecordID'] = $moduleRecordID;

							$messageCountAry[$eclassAppConfig['deviceOS']['Android_CN']]++;
						}
					} else if ($_deviceOs == $eclassAppConfig['deviceOS']['Huawei']) {
						foreach ((array)$__userAssoAry as $___recipientUserId => $___userInfoAry) {
							if (count((array)$___userInfoAry['userDeviceAry'][$_deviceOs]) > 0) {
								$__tmpUserAssoAry[$___recipientUserId]['relatedUserIdAry'] = $___userInfoAry['relatedUserIdAry'];
								$__tmpUserAssoAry[$___recipientUserId]['deviceIdAry'] = $___userInfoAry['userDeviceAry'][$_deviceOs];
							}
						}

						if ($__tmpUserAssoAry) {
							$__msgCounter = $messageCountAry[$eclassAppConfig['deviceOS']['Huawei']];

							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['messageTitle'] = $__messageTitle;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['messageContent'] = $__messageContent;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['userAssoAry'] = $__tmpUserAssoAry;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['fromModule'] = $fromModule;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['moduleRecordID'] = $moduleRecordID;
							$pushMessageAssoAry[$_serviceProvider][$appType][$__msgCounter]['attachmentPath'] = $attachmentPath;
							$messageCountAry[$eclassAppConfig['deviceOS']['Huawei']]++;
						}
					}
					//$_messageCount++;
				}
			}

			### build the json format of each service provider
			$consolidatedMessageAssoAry = array();
			foreach ((array)$pushMessageAssoAry as $_serviceProvider => $_messageAry) {
				if (is_array($_messageAry) && file_exists($intranet_root.'/includes/eClassApp/pushMessage/lib'.$_serviceProvider.'.php')) {
					include_once($intranet_root.'/includes/eClassApp/pushMessage/lib'.$_serviceProvider.'.php');
					$libpushMessage[$_serviceProvider] = null;
					eval ('$libpushMessage['.$_serviceProvider.'] = new lib'.$_serviceProvider.'();');
					$consolidatedMessageAssoAry[$_serviceProvider] = $libpushMessage[$_serviceProvider]->buildMessageJson($notifyMessageId, $_messageAry);
				}
			}

			//debug_pr($consolidatedMessageAssoAry);die();
			unset($pushMessageAssoAry);
//			die();
//debug_pr('point 1 = '.convert_size(memory_get_usage(true)));


			### send request to central server
			if ($createRecordOnly) {
				// do nth
			}
			else {
				$responseAry = $this->sendPushMessageToCentralServer($notifyMessageId, $consolidatedMessageAssoAry, $sendTimeMode, $sendTimeString, $messageTitle);
			}
			//$responseAry = false;
//debug_pr('point 2 = '.convert_size(memory_get_usage(true)));

			if ($createRecordOnly) {
				$successAry['saveResponse'] = $this->savePushMessageResponse($notifyMessageId, array(), $eclassAppConfig['pushMessageSendMode']['scheduled'], $parNotifyMessageTargetIdAry);
			}
			else {
				$successAry['saveResponse'] = $this->savePushMessageResponse($notifyMessageId, $responseAry, $sendTimeMode, $parNotifyMessageTargetIdAry, $consolidatedMessageAssoAry);
			}
//debug_pr('point 3 = '.convert_size(memory_get_usage(true)));

            $forceUseClass = false;
			return (!in_array(false, (array)$successAry))? $notifyMessageId : false;
		}

		function sendPushMessageToCentralServer($notifyMessageId, $messageAssoAry, $sendTimeMode, $sendTimeString, $messageTitle) {
			global $intranet_root, $eclassAppConfig, $config_school_code, $sys_custom;

			if (!is_array($messageAssoAry) || count($messageAssoAry)==0) {
				return false;
			}

			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'sendPushMessage';
			$postParamAry['SchoolCode'] = $config_school_code;
			$postParamAry['APIVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['overall'];
			$postParamAry['NotifyMessageID'] = $notifyMessageId;
			$postParamAry['Title'] = $messageTitle;
			$postParamAry['SendTimeMode'] = $sendTimeMode;
			$postParamAry['SendTime'] = $sendTimeString;
			$postParamAry['MessageAssoAry'] = $messageAssoAry;
			$postJsonString = $jsonObj->encode($postParamAry);
			//$headers = array('Content-Type: application/json');
			$headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
			$centralServerUrl = $this->getCurCentralServerUrl();

			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; Start sending request to central server at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}

			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
			$responseJson = curl_exec($ch);
			
			//$responseJson = '{"CentralMsgID":102645,"ResultAry":{"gcm":{"result":[{"multicast_id":7080157433076066222,"success":0,"failure":1,"canonical_ids":0,"results":[{"error":"NotRegistered"}]}]}}}';
			curl_close($ch);

			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; End sending request to central server at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}

			$responseJson = standardizeFormPostValue($responseJson);
			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; Start adding message log to db at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}
			$this->addPushMessageLog($notifyMessageId, $postJsonString, $responseJson);
			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; End adding message log to db at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}

			include_once($intranet_root.'/includes/services_json.php');
			$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);

			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; Start json decode at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}
			$decodedJsonString = $json->decode($responseJson);
			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; End json decode at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}

			return $decodedJsonString;
		}

		function addPushMessageLog($parNotifyMessageId, $parMessageJsonString, $parResponseJsonString) {
			$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_LOG 
						(NotifyMessageID, MessageJson, ResponseJson, DateInput)
					Values
						($parNotifyMessageId, '".$this->Get_Safe_Sql_Query($parMessageJsonString)."', '".$this->Get_Safe_Sql_Query($parResponseJsonString)."', now())
					";
			return $this->db_db_query($sql);
		}

		function savePushMessageResponse($notifyMessageId, $responseAry, $sendTimeMode, $parNotifyMessageTargetIdAry='', $parSourceMessageAry='') {
			global $eclassAppConfig, $intranet_root, $sys_custom;

			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; Start saving response to db at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}

			$successAry = array();
			$serviceProviderResponseAry = array();
			if (isset($responseAry['MethodResult']['CentralMsgID'])) {
				// API with central message id
				$sql = "Update INTRANET_APP_NOTIFY_MESSAGE set CentralMsgID = '".$responseAry['MethodResult']['CentralMsgID']."' where NotifyMessageID = '".$notifyMessageId."'";
				$successAry['updateCentralMsgID'] = $this->db_db_query($sql);

				$serviceProviderResponseAry = $responseAry['MethodResult']['ResultAry'];
			}
			else {
				// old API
				$serviceProviderResponseAry = $responseAry['MethodResult'];
			}

			### save result in DB
			if (!is_array($responseAry) || count((array)$responseAry['ErrorCode']) > 0) {
				if (is_array($parSourceMessageAry) && count($parSourceMessageAry) > 0) {
					if (!is_array($responseAry)) {
						$errorCode = $eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer'];
					}
					else if (count((array)$responseAry['ErrorCode']) > 0){
						$errorCode = implode(',', $responseAry['ErrorCode']);
					}

					if ($parNotifyMessageTargetIdAry != '') {
						$conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
					}
					$sql = "Update 
									INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
							Set 
									ErrorCode = '".$this->Get_Safe_Sql_Query($errorCode)."', 
									MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']."'
							Where
									NotifyMessageID = '".$notifyMessageId."'
									$conds_notifyMessageTargetId
							";
					$successAry['updateErrorCode'] = $this->db_db_query($sql);
				}
				else {
					// just send push message to parents without registered push message devices => do nth
				}
			}
			else {
				if ($sendTimeMode == $eclassAppConfig['pushMessageSendMode']['now']) {
					foreach ((array)$serviceProviderResponseAry as $_serviceProvider => $_resultAry) {
						include_once($intranet_root.'/includes/eClassApp/pushMessage/lib'.$_serviceProvider.'.php');
						$libpushMessage[$_serviceProvider] = null;
						eval ('$libpushMessage['.$_serviceProvider.'] = new lib'.$_serviceProvider.'();');

						if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
							error_log('$notifyMessageId = '.$notifyMessageId."; Start saving response to db for $_serviceProvider at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
						}

						$successAry['saveMessageResult'][$_serviceProvider] = $libpushMessage[$_serviceProvider]->saveMessageResult($notifyMessageId, $_resultAry, $parNotifyMessageTargetIdAry);

						if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
							error_log('$notifyMessageId = '.$notifyMessageId."; End saving response to db for $_serviceProvider at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
						}
					}
				}
				else if ($sendTimeMode == $eclassAppConfig['pushMessageSendMode']['scheduled']) {
				    if ($parNotifyMessageTargetIdAry != '') {
				        $conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
				    }
					$sql = "Update INTRANET_APP_NOTIFY_MESSAGE_REFERENCE Set MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']."' Where NotifyMessageID = '".$notifyMessageId."' And MessageStatus != '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']."' $conds_notifyMessageTargetId";
					$successAry['updateMessageStatus'] = $this->db_db_query($sql);
				}
			}

			if ($sys_custom['eClassApp']['enableSendPushMessagePerformanceLog']) {
				error_log('$notifyMessageId = '.$notifyMessageId."; End saving response to db at ".date('Ymd_His')."\n", 3, "/tmp/pm_".date('Ymd').".log");
			}

			return !in_array(false, (array)$successAry);
		}

		function resendPushMessage($notifyMessageId, $notifyMessageTargetIdAry, $sendTimeMode='', $sendTimeString='', $fromModule='', $moduleRecordID='') {
			global $eclassAppConfig, $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();

			if ($sendTimeMode=='') {
				$sendTimeMode = $eclassAppConfig['pushMessageSendMode']['now'];
			}

			$successAry = array();


			### old related-to record (for log)
			$sql = "Select * From INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO Where NotifyMessageTargetID In ('".implode("','", (array)$notifyMessageTargetIdAry)."')";
			$messageRelatedToAry = $this->returnResultSet($sql);
			$numOfRelatedTo = count($messageRelatedToAry);
			$logMessageRelatedToAry = array();
			for ($i=0; $i<$numOfRelatedTo; $i++) {
				$_logTextAry = array();
				foreach ((array)$messageRelatedToAry[$i] as $__key => $__val) {
					$_logTextAry[] = $__key.' = '.$__val;
				}

				$logMessageRelatedToAry[] = implode('||keySeparator||', (array)$_logTextAry);
			}

			### old reference record (for log)
			$sql = "Select * From INTRANET_APP_NOTIFY_MESSAGE_REFERENCE Where NotifyMessageID = '".$notifyMessageId."' And NotifyMessageTargetID In ('".implode("','", (array)$notifyMessageTargetIdAry)."')";
			$messageReferenceAry = $this->returnResultSet($sql);
			$numOfReference = count($messageReferenceAry);
			$logMessageReferenceAry = array();
			for ($i=0; $i<$numOfReference; $i++) {
				$_logTextAry = array();
				foreach ((array)$messageReferenceAry[$i] as $__key => $__val) {
					$_logTextAry[] = $__key.' = '.$__val;
				}

				$logMessageReferenceAry[] = implode('||keySeparator||', (array)$_logTextAry);
			}

			### get message related user info
			$sql = "Select 
							mt.TargetID, rt.UserID, mt.NotifyMessageTargetID, mt.MessageTitle, mt.MessageContent
					From
							INTRANET_APP_NOTIFY_MESSAGE_TARGET as mt
							Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO as rt ON (mt.NotifyMessageTargetID = rt.NotifyMessageTargetID)
					Where
							mt.NotifyMessageID = '".$notifyMessageId."'
							And mt.NotifyMessageTargetID IN ('".implode("','", (array)$notifyMessageTargetIdAry)."')
					";
			$messageRelatedUserAry = $this->returnResultSet($sql);


			### send message
			$individualMessageInfoAry = array();
			$sql = "Select MessageTitle, MessageContent, IsPublic, AppType From INTRANET_APP_NOTIFY_MESSAGE Where NotifyMessageID = '".$notifyMessageId."'";
			$messageInfoAry = $this->returnResultSet($sql);
			$messageTitle = $messageInfoAry[0]['MessageTitle'];
			$messageContent = $messageInfoAry[0]['MessageContent'];
			$isPublic = $messageInfoAry[0]['IsPublic'];
			$sppType = $messageInfoAry[0]['AppType'];
			if ($messageContent == 'MULTIPLE MESSAGES') {
				// from import
				$messageTargetInfoAssoAry = BuildMultiKeyAssoc($messageRelatedUserAry, 'NotifyMessageTargetID', $IncludedDBField=array('MessageTitle', 'MessageContent'), $SingleValue=0, $BuildNumericArray=0);
				$messageRelatedToAssoAry = BuildMultiKeyAssoc($messageRelatedUserAry, array('NotifyMessageTargetID', 'TargetID'), $IncludedDBField=array('UserID'), $SingleValue=1, $BuildNumericArray=1);

				foreach ((array)$messageTargetInfoAssoAry as $_notifyMessageTargetId => $_messageInfoAry) {
					$_messageTitle = $_messageInfoAry['MessageTitle'];
					$_messageContent = $_messageInfoAry['MessageContent'];

					$_individualMessageInfoAry = array();
					$_individualMessageInfoAry['relatedUserIdAssoAry'] = $messageRelatedToAssoAry[$_notifyMessageTargetId];
					$_individualMessageInfoAry['messageTitle'] = $_messageTitle;
					$_individualMessageInfoAry['messageContent'] = $_messageContent;

					$individualMessageInfoAry[] = $_individualMessageInfoAry;
				}
			}
			else {
				// from send individual message
				$userAssoAry = BuildMultiKeyAssoc($messageRelatedUserAry, 'TargetID', $IncludedDBField=array('UserID'), $SingleValue=1, $BuildNumericArray=1);
				$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $userAssoAry;
			}
			$notifyMessageId = $this->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $sppType, $sendTimeMode, $sendTimeString, $notifyMessageId, $notifyMessageTargetIdAry, $fromModule, $moduleRecordID);


			### add log
			$tmpAry = array();
		    $tmpAry['NotifyMessageID'] = $notifyMessageId;
		    $tmpAry['NotifyMessageTargetID'] = implode(',', $notifyMessageTargetIdAry);
		    $tmpAry['MessageRelatedTo'] = implode('||recordSeparator||', $logMessageRelatedToAry);
		    $tmpAry['MessageReference'] = implode('||recordSeparator||', $logMessageReferenceAry);
		    $successAry['Log_Delete'] = $liblog->INSERT_LOG($this->ModuleTitle, 'resendPushMessage', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_APP_NOTIFY_MESSAGE_REFERENCE', $notifyMessageId);

			return !in_array(false, $successAry);
		}

		function updateScheduledPushMessageStatus($NotifyMessageID, $ResponseJson, $SendTime) {
			global $intranet_root, $eclassAppConfig;

			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$ResponseJson = standardizeFormPostValue($ResponseJson);

			// check if the push message was sent in batch or not
			$sql = "Select NotifyMessageTargetIdList From INTRANET_APP_NOTIFY_MESSAGE_BATCH Where NotifyMessageID = '".$NotifyMessageID."' And SendTime = '".$SendTime."'";
			$resultAry = $this->returnResultSet($sql);
			if (count($resultAry) == 0) {
				// not send in batches => update all message status
				$notifyMessageTargetIdAry = '';
			}
			else {
				// send in batches => update specific target message status
				$notifyMessageTargetIdAry = explode(',', $resultAry[0]['NotifyMessageTargetIdList']);
			}

			$successAry['addLog'] = $this->addPushMessageLog($NotifyMessageID, '', $ResponseJson);
			$successAry['saveResponse'] = $this->savePushMessageResponse($NotifyMessageID, $jsonObj->decode($ResponseJson), $eclassAppConfig['pushMessageSendMode']['now'], $notifyMessageTargetIdAry);

			$returnAry = array();
			$returnAry['canSaveResponse'] = (!in_array(false, $successAry))? '1' : '0';
			return $returnAry;
		}

		function updatePushMessageReadStatus($parUserLogin, $parMessageReferenceIdList) {
			global $eclassAppConfig, $sys_custom, $eclass_db, $intranet_root;

			$parUserLogin = $this->getDemoSiteUserLogin($parUserLogin);
			$userAry = $this->getUserInfoByUserLogin($parUserLogin);
			$targetUserId = $userAry[0]['UserID'];
			if ($targetUserId == '') {
				return '104';
			}

			if (is_array($parMessageReferenceIdList)) {
				$messageReferenceIdAry = $parMessageReferenceIdList;
			}
			else {
				$messageReferenceIdAry = explode(',', $parMessageReferenceIdList);
			}
			$sql = "Update 
							INTRANET_APP_NOTIFY_MESSAGE_REFERENCE as ianmr Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt ON (ianmr.NotifyMessageTargetID = ianmt.NotifyMessageTargetID)  
					Set 
							ianmr.MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']."', 
							ianmr.DateModified = now() 
					Where 
							ianmt.NotifyMessageID IN ('".implode("','", (array)$messageReferenceIdAry)."') 
							AND ianmt.TargetID = '".$targetUserId."' 
							/* And ianmr.MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']."' */
							AND ianmr.MessageStatus != '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']."'
					";
			$success = $this->db_db_query($sql);
			if($sys_custom['project']['centennialcollege']){
				include_once($intranet_root.'/cc_eap/settings.php');
				if($sys_custom['message_with_feedback']){
					if($sys_custom['centennialcollege']['dev']['on']){
						$cond = " a.course_code LIKE '%EAP_%' ";
					}else{
						$cond = " a.RoomType='0' ";
					}

					$fieldname  = "a.course_id";
					$sql = "SELECT $fieldname FROM {$eclass_db}.course AS a WHERE $cond ORDER BY a.course_id";

					$course = $this->returnVector($sql);
					foreach($course as $c){
						$courseDB = classNamingDB($c);
						$sql = "SELECT notify_usage_id FROM $courseDB.notify_usage WHERE NotifyMessageID IN ('".implode("','", (array)$messageReferenceIdAry)."');";
						$RecToBeUpdate = $this->returnVector($sql);
						if(!empty($RecToBeUpdate)){
							$sql = "UPDATE $courseDB.notify_usage SET response=NOW(), verify_code='' WHERE notify_usage_id IN ('".implode("','", $RecToBeUpdate)."')";
							$this->db_db_query($sql);
						}
					}
				}
			}
			$returnAry = array();
			$returnAry['Status'] = ($success)? 'ACK' : '';
			return $returnAry;
		}

		function getPushMessageStatistics($messageIdAry, $excludeHasReadFromSuccess=false) {
			global $eclassAppConfig;

			$sql = "Select
							mt.NotifyMessageID, mt.NotifyMessageTargetID, mt.TargetID as RecepientUserID, mr.MessageStatus
					From
							INTRANET_APP_NOTIFY_MESSAGE_TARGET as mt
							Inner Join INTRANET_APP_NOTIFY_MESSAGE_REFERENCE as mr ON (mt.NotifyMessageTargetID = mr.NotifyMessageTargetID)
					Where
							mt.NotifyMessageID IN ('".implode("','", (array)$messageIdAry)."')
					";
			$messageAry = $this->returnResultSet($sql);
			$messageAssoAry = BuildMultiKeyAssoc($messageAry, array('NotifyMessageID', 'NotifyMessageTargetID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);


			$messageTargetIdAry = array_values(array_unique(Get_Array_By_Key($messageAry, 'NotifyMessageTargetID')));
			$sql = "Select NotifyMessageTargetID, UserID From INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO Where NotifyMessageTargetID IN ('".implode("','", (array)$messageTargetIdAry)."')";
			$messageRelatedToAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('NotifyMessageTargetID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);


			$statisticsAssoAry = array();
			foreach ((array)$messageAssoAry as $_messageId => $_messageInfoAry) {
				$_numOfRecepient = count((array)$_messageInfoAry);
				$statisticsAssoAry[$_messageId]['numOfRecepient'] = $_numOfRecepient;
				$statisticsAssoAry[$_messageId]['numOfSendSuccess'] = 0;
				$statisticsAssoAry[$_messageId]['numOfSendFailed'] = 0;
				$statisticsAssoAry[$_messageId]['numOfHasRead'] = 0;
				$statisticsAssoAry[$_messageId]['numOfWaitingToSend'] = 0;
				$statisticsAssoAry[$_messageId]['numOfNoRegisteredDevice'] = 0;
				$statisticsAssoAry[$_messageId]['sendFailAssoAry'] = array();

				foreach ((array)$_messageInfoAry as $__notifyMessageTargetId => $__recepientMessageInfoAry) {
					$__recepientUserId = $__recepientMessageInfoAry[0]['RecepientUserID'];
					$__numOfRecepientMessage = count((array)$__recepientMessageInfoAry);

					$__sendSuccess = false;
					$__hasRead = false;
					$__waitingToSend = false;
					$__notRegisteredDevice = false;
					for ($i=0; $i<$__numOfRecepientMessage; $i++) {
						$___messageStatus = $__recepientMessageInfoAry[$i]['MessageStatus'];

						if ($___messageStatus == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']) {
							$__sendSuccess = true;
							$__hasRead = true;
							$__waitingToSend = false;
							$__notRegisteredDevice = false;
							break;	// read by any one of devices => show read
						}
						else if ($___messageStatus == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']) {
							$__sendSuccess = true;
							$__hasRead = false;
							$__waitingToSend = false;
							$__notRegisteredDevice = false;
						}
						else if ($___messageStatus == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend']) {
							if ($__sendSuccess || $__hasRead) {
								// if other device has read or send success => ignore this pending record (esp. for getui case)
							}
							else {
								$__sendSuccess = false;
								$__hasRead = false;
								$__waitingToSend = true;
								$__notRegisteredDevice = false;
							}
						}
						else if ($___messageStatus == $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice']) {
							if ($__sendSuccess || $__hasRead || $__waitingToSend) {
								// if other device has read or send success or waiting to send => ignore this not registered device record (esp. for getui case)
							}
							else {
								$__sendSuccess = false;
								$__hasRead = false;
								$__waitingToSend = false;
								$__notRegisteredDevice = true;
							}
						}
					}

					if ($__waitingToSend) {
						$statisticsAssoAry[$_messageId]['numOfWaitingToSend']++;
					}
					else if ($__sendSuccess) {
						if ($excludeHasReadFromSuccess && $__hasRead) {
							// do not count as success for has read message => for drop down list filtering display
						}
						else {
							$statisticsAssoAry[$_messageId]['numOfSendSuccess']++;
						}
					}
					else if ($__notRegisteredDevice) {
						$statisticsAssoAry[$_messageId]['numOfNoRegisteredDevice']++;
					}
					else {
						$statisticsAssoAry[$_messageId]['numOfSendFailed']++;

						$__relatedToUserIdAry = Get_Array_By_Key((array)$messageRelatedToAssoAry[$__notifyMessageTargetId], 'UserID');
						$statisticsAssoAry[$_messageId]['sendFailAssoAry'][$__recepientUserId] = $__relatedToUserIdAry;
					}

					if ($__hasRead) {
						$statisticsAssoAry[$_messageId]['numOfHasRead']++;
					}
				}
			}

			return $statisticsAssoAry;
		}

		function getUserRelatedPushMessage($parUserLogin, $referenceIdList, $dotContent=false) {
            global $intranet_root, $eclassAppConfig;

			$userAry = $this->getUserInfoByUserLogin($parUserLogin);
			$targetUserId = $userAry[0]['UserID'];
			$recordType = $userAry[0]['RecordType'];
			if ($targetUserId == '') {
				return '104';
			}

			$targetUserIdAry = array();
			if ($recordType == USERTYPE_PARENT) {
				// return chlidern's info for parent account
				$sql = "Select 
								ipr.StudentID 
						From 
								INTRANET_PARENTRELATION as ipr
								Inner Join INTRANET_USER as iu ON (ipr.StudentID = iu.UserID)
						Where
								ipr.ParentID = '".$targetUserId."'
								AND (iu.RecordStatus = 1 OR iu.RecordStatus = 0)
						";
				$targetUserIdAry = Get_Array_By_Key($this->returnResultSet($sql), 'StudentID');
			}
			else {
				$targetUserIdAry[] = $targetUserId;
			}

			$referenceIdAry = explode(',', IntegerSafe($referenceIdList));

			// get the push message title and content
			$sql = "Select ianm.NotifyMessageID, ianm.MessageTitle, ianm.MessageContent, ianm.NotifyDateTime, ianmmr.ModuleName As FromModule, ianmmr.ModuleRecordID, ianma.AttachmentPath
					From 
						INTRANET_APP_NOTIFY_MESSAGE as ianm
						Left Outer Join INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION As ianmmr On ianm.NotifyMessageID = ianmmr.NotifyMessageID
						Left Outer Join INTRANET_APP_NOTIFY_MESSAGE_ATTACHMENT As ianma On ianm.NotifyMessageID = ianma.NotifyMessageID
					Where 
						ianm.NotifyMessageID IN ('".implode("','", (array)$referenceIdAry)."') 
					Order By ianm.NotifyDateTime";
			$messageAry = $this->returnResultSet($sql);
			$numOfMessage = count($messageAry);

			$multipleMessageIdAry = array();
			for ($i=0; $i<$numOfMessage; $i++) {
				$_notifyMessageId = $messageAry[$i]['NotifyMessageID'];
				$_content = $messageAry[$i]['MessageContent'];

				if ($_content == 'MULTIPLE MESSAGES') {
					$multipleMessageIdAry[] = $_notifyMessageId;
				}
			}
			$numOfMultipleMessage = count($multipleMessageIdAry);

			if ($numOfMultipleMessage > 0) {
				// get multiple message info array
				$sql = "Select 
								ianmt.NotifyMessageID, ianmt.NotifyMessageTargetID, ianmt.MessageTitle, ianmt.MessageContent
						From 
								INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt
						Where
								ianmt.NotifyMessageID IN ('".implode("','", (array)$multipleMessageIdAry)."')
								And ianmt.TargetID = '".$targetUserId."'
						";
				$multipleMessageAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('NotifyMessageID', 'NotifyMessageTargetID'));
			}

			// get related to info
			$sql = "Select 
							ianmt.NotifyMessageID, ianmt.NotifyMessageTargetID, ianmtrt.UserID
					From 
							INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt
							Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO as ianmtrt ON (ianmt.NotifyMessageTargetID = ianmtrt.NotifyMessageTargetID)
					Where
							ianmt.NotifyMessageID IN ('".implode("','", (array)$referenceIdAry)."')
							And ianmtrt.UserID In ('".implode("','", (array)$targetUserIdAry)."')
							And ianmt.TargetID = '".$targetUserId."'
					";
			$messageTargetAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'NotifyMessageID', array(), $SingleValue=0, $BuildNumericArray=1);

			$returnAry = array();
			$returnAry['pushNotificationAry'] = array();
			for ($i=0; $i<$numOfMessage; $i++) {
				$_notifyMessageId = $messageAry[$i]['NotifyMessageID'];
				$_title = $messageAry[$i]['MessageTitle'];
				$_content = $messageAry[$i]['MessageContent'];
				$_notifyDateTime = $messageAry[$i]['NotifyDateTime'];
				$_fromModule = $messageAry[$i]['FromModule'];
				$_moduleRecordID = $messageAry[$i]['ModuleRecordID'];
                $_attachmentUrl = '';
                if($messageAry[$i]['AttachmentPath']){
                    $_attachmentUrl = $eclassAppConfig['urlFilePath'].$messageAry[$i]['AttachmentPath'];
                    $_attachmentUrl = str_replace($intranet_root.'/', '', $_attachmentUrl);
                    $_attachmentUrl = str_replace(curPageURL($withQueryString=false, $withPageSuffix=false).'/', '', $_attachmentUrl);
                    $_attachmentUrl = curPageURL($withQueryString=false, $withPageSuffix=false).'/'.$_attachmentUrl;
                }

				//20160303 - added school news scheduled push message but app not support direct link at this moment
				// so do not return fromModule to app now
				if (!in_array($_fromModule, array('eNotice','hkuFlu','SchoolNews','eCircular','hkuFluMain','ePayment'))) {
					$_fromModule = '';
					$_moduleRecordID = '';
				}

				if ($_content == 'MULTIPLE MESSAGES') {
					// multiple message => each target one array
					$_meesageTargetAry = $messageTargetAssoAry[$_notifyMessageId];
					$_numOfTarget = count((array)$_meesageTargetAry);
					for ($j=0; $j<$_numOfTarget; $j++) {
						$__notifyMessageTargetId = $_meesageTargetAry[$j]['NotifyMessageTargetID'];
						$__relatedToUserId = $_meesageTargetAry[$j]['UserID'];

						$__messageTitle = $multipleMessageAssoAry[$_notifyMessageId][$__notifyMessageTargetId]['MessageTitle'];
						$__messageContent = $multipleMessageAssoAry[$_notifyMessageId][$__notifyMessageTargetId]['MessageContent'];

						$__tmpAry = array();
						$__tmpAry['referenceId'] = $_notifyMessageId;
						$__tmpAry['relatedStudentIdAry'][] = $__relatedToUserId;
						$__tmpAry['title'] = $this->standardizePushMessageText($this->convertToUtf8($__messageTitle));
						$__tmpAry['content'] = ($dotContent)? '...' : $this->standardizePushMessageText($this->convertToUtf8($__messageContent));
						$__tmpAry['notifyDateTime'] = $this->convertToUtf8($_notifyDateTime);
						$__tmpAry['fromModule'] = $_fromModule;
						$__tmpAry['moduleRecordID'] = $_moduleRecordID;
                        $__tmpAry['attachmentUrl'] = $_attachmentUrl;

						$returnAry['pushNotificationAry'][] = $__tmpAry;
					}
				}
				else {
					// general message => one array for all messages
					$_tmpAry = array();
					$_tmpAry['referenceId'] = $_notifyMessageId;
					$_tmpAry['relatedStudentIdAry'] = Get_Array_By_Key($messageTargetAssoAry[$_notifyMessageId], 'UserID');
					$_tmpAry['title'] = $this->standardizePushMessageText($this->convertToUtf8($_title));
					$_tmpAry['content'] = ($dotContent)? '...' : $this->standardizePushMessageText($this->convertToUtf8($_content));
					$_tmpAry['notifyDateTime'] = $this->convertToUtf8($_notifyDateTime);
					$_tmpAry['fromModule'] = $_fromModule;
					$_tmpAry['moduleRecordID'] = $_moduleRecordID;
                    $_tmpAry['attachmentUrl'] = $_attachmentUrl;

					$returnAry['pushNotificationAry'][] = $_tmpAry;
				}
			}

			return $returnAry;
		}

		function getUserRelatedLatestPushMessage($parUserLogin, $parReferenceId) {
			$userAry = $this->getUserInfoByUserLogin($parUserLogin);
			$targetUserId = $userAry[0]['UserID'];
			$recordType = $userAry[0]['RecordType'];
			if ($targetUserId == '') {
				return '104';
			}

			$sql = "Select 
							ianm.NotifyMessageID 
					From 
							INTRANET_APP_NOTIFY_MESSAGE as ianm
							Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt ON (ianm.NotifyMessageID = ianmt.NotifyMessageID)
					Where 
							ianmt.TargetID = '".$targetUserId."' 
							And ianmt.NotifyMessageID > '".$parReferenceId."' 
							And (ianm.RecordStatus = 1 Or ianm.ShowInApp = 1)
					";
			$targetNotifyMessageAry = $this->returnResultSet($sql);
			$targetNotifyMessageIdAry = array_values(array_unique(Get_Array_By_Key((array)$targetNotifyMessageAry, 'NotifyMessageID')));
			$targetNotifyMessageIdList = implode(',', (array)$targetNotifyMessageIdAry);

			//return $this->getUserRelatedPushMessage($parUserLogin, $targetNotifyMessageIdList, $dotContent=true);
			return $this->getUserRelatedPushMessage($parUserLogin, $targetNotifyMessageIdList);
		}

		function getUserRelatedLatestPushMessageId($parUserLogin) {
			$userAry = $this->getUserInfoByUserLogin($parUserLogin);
			$targetUserId = $userAry[0]['UserID'];
			$recordType = $userAry[0]['RecordType'];
			if ($targetUserId == '') {
				return '104';
			}

			$sql = "Select 
							max(ianm.NotifyMessageID) as MaxNotifyMessageID
					From 
							INTRANET_APP_NOTIFY_MESSAGE as ianm
							Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt ON (ianm.NotifyMessageID = ianmt.NotifyMessageID)
					Where 
							ianmt.TargetID = '".$targetUserId."' 
							And (ianm.RecordStatus = 1 Or ianm.ShowInApp = 1)
					";
			$latestNotifyMessageAry = $this->returnResultSet($sql);
			$latestNotifyMessageId = $latestNotifyMessageAry[0]['MaxNotifyMessageID'];

			$returnAry = array();
			$returnAry['LastPushNotificationId'] = ($latestNotifyMessageId > 0)? $latestNotifyMessageId : 0;

			return $returnAry;
		}

		function getUserRelatedLatestPushMessageByTs($parUserLogin, $parLastRetrieveTime, $parIncludeLastRetrieveTimeMessage='') {

			$parUserLogin = $this->getDemoSiteUserLogin($parUserLogin);
			$userAry = $this->getUserInfoByUserLogin($parUserLogin);
			$targetUserId = $userAry[0]['UserID'];
			$recordType = $userAry[0]['RecordType'];
			if ($targetUserId == '') {
				return '104';
			}

			### returns last 100 days message only (for initialization)
			$dateLimit = date('Y-m-d', strtotime('-100 days'));
			$datetimeLimit = $dateLimit.' 00:00:00';

			$compareSymbol = ">";
			if ($parIncludeLastRetrieveTimeMessage == 1) {
				$compareSymbol = ">=";
			}

			if ($parLastRetrieveTime == '0000-00-00 00:00:00') {
				$parLastRetrieveTime = '1971-01-01 00:00:00';
			}

			$sql = "Select 
							ianm.NotifyMessageID 
					From 
							INTRANET_APP_NOTIFY_MESSAGE as ianm
							Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt ON (ianm.NotifyMessageID = ianmt.NotifyMessageID)
					Where 
							ianmt.TargetID = '".$targetUserId."' 
							And UNIX_TIMESTAMP(ianm.NotifyDateTime) $compareSymbol UNIX_TIMESTAMP('".$parLastRetrieveTime."') AND UNIX_TIMESTAMP(ianm.NotifyDateTime) <= UNIX_TIMESTAMP(now())
							And ianm.NotifyDateTime >= '".$datetimeLimit."'
							And (ianm.RecordStatus = 1 Or ianm.ShowInApp = 1)
					";
			$targetNotifyMessageAry = $this->returnResultSet($sql);
			$targetNotifyMessageIdAry = array_values(array_unique(Get_Array_By_Key((array)$targetNotifyMessageAry, 'NotifyMessageID')));
			$targetNotifyMessageIdList = implode(',', (array)$targetNotifyMessageIdAry);

			//return $this->getUserRelatedPushMessage($parUserLogin, $targetNotifyMessageIdList, $dotContent=true);
			return $this->getUserRelatedPushMessage($parUserLogin, $targetNotifyMessageIdList);
		}

		function returnSchoolBanner($parAppType='') {
		    global $intranet_root, $eclassAppConfig, $sys_custom;

			if ($parAppType == '') {
				$parAppType = $eclassAppConfig['appType']['Parent'];
			}

			$sql = "Select RecordStatus, ImageType, ImagePath From APP_SCHOOL_IMAGE Where AppType = '".$parAppType."' And RecordStatus = 1";
			$imageAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'ImageType');

            $returnBackgroundFilePath = $this->getSchoolImageUrlPath($parAppType, $eclassAppConfig['imageType']['backgroundImage']);

            if($returnBackgroundFilePath != ""){
                $bannerExist = 1;
            }else{
                $bannerExist = 0;
            }

            $returnBackgroundFilePath = str_replace($intranet_root.'/', '', $returnBackgroundFilePath);

            $returnBackgroundFilePath = str_replace(curPageURL($withQueryString=false, $withPageSuffix=false).'/', '', $returnBackgroundFilePath);
			//### School Banner Background Image
			//$imageType = $eclassAppConfig['imageType']['backgroundImage'];
			//$dbImagePath = $imageAssoAry[$imageType]['ImagePath'];
			//$recordStatus = $imageAssoAry[$imageType]['RecordStatus'];
            //
	 		//if ($dbImagePath == '') {
	 		//	// get image from default banner path
	 		//	$backgroundPath = $intranet_root.'/'.$eclassAppConfig['bannerPath'];
	 		//}
	 		//else {
	 		//	// get image from app settings
	 		//	$backgroundPath = $eclassAppConfig['urlFilePath'].$dbImagePath;
	 		//}
			//if (file_exists($backgroundPath)) {
	 		//	$bannerExist = 1;
            //
	 		//	// from "/home2/web/eclass40/intranetdata/file/app/schoolImage/P_badge.jpg" to "file/app/schoolImage/P_badge.jpg"
	 		//	$returnBackgroundFilePath = str_replace($intranet_root.'/', '', $backgroundPath);
	 		//}
	 		//else {
	 		//	$bannerExist = 0;
	 		//	$returnBackgroundFilePath = "";
	 		//}

	 		### School Badge
	 		$imageType = $eclassAppConfig['imageType']['schoolBadge'];
	 		$dbImagePath = $imageAssoAry[$imageType]['ImagePath'];
	 		if ($dbImagePath == '') {
	 			// get image from admin console settings
	 			$badgeFilePath = $intranet_root.'/file/'.get_file_content($intranet_root."/file/schoolbadge.txt");
	 		}
	 		else {
	 			// get image from app settings
	 			$badgeFilePath = $eclassAppConfig['urlFilePath'].$dbImagePath;
	 		}

	 		if (file_exists($badgeFilePath)) {
	 			// from "/home2/web/eclass40/intranetdata/file/app/schoolImage/P_badge.jpg" to "file/app/schoolImage/P_badge.jpg"
	 			$returnBadgeFilePath = str_replace($intranet_root.'/', '', $badgeFilePath);
	 		}
	 		else {
	 			$returnBadgeFilePath = "";
	 		}

            ### School Website URL
            $eClassAppSettingsObj = $this->getAppSettingsObj();
            $schoolWebsiteURL = $eClassAppSettingsObj->getSettingsValue('parentAppSchoolWebsiteURL');
            $returnSchoolWebsiteURL = "";
            if($schoolWebsiteURL != null){
                $returnSchoolWebsiteURL = $schoolWebsiteURL;
            }

	 		### Account Page Banner (For Parent App only)
	 		if($parAppType == $eclassAppConfig['appType']['Parent']) {

	 		    //$imageType = $eclassAppConfig['imageType']['accountPageBanner'];
	 		    //$dbImagePath = $imageAssoAry[$imageType]['ImagePath'];
	 		    //
	 		    //$returnAccountPageBannerFilePath = '';
	 		    //
	 		    //if ($dbImagePath != '') {
	 		    //    // get image from app settings
	 		    //    $accountPageBannerPath = $eclassAppConfig['urlFilePath'].$dbImagePath;
	 		    //
	 		    //    if (file_exists($accountPageBannerPath)) {
	 		    //
	 		    //        $returnAccountPageBannerFilePath = str_replace($intranet_root.'/', '', $accountPageBannerPath);
	 		    //    }
	 		    //}

                $returnAccountPageBannerFilePath = $this->getSchoolImageUrlPath($parAppType, $eclassAppConfig['imageType']['accountPageBanner']);

                $returnAccountPageBannerFilePath = str_replace($intranet_root.'/', '', $returnAccountPageBannerFilePath);

                $returnAccountPageBannerFilePath = str_replace(curPageURL($withQueryString=false, $withPageSuffix=false).'/', '', $returnAccountPageBannerFilePath);

                return array('BannerExist' => $bannerExist, 'BannerPath' => intranet_handle_url($this->convertToUtf8($returnBackgroundFilePath)), 'BadgePath' => intranet_handle_url($this->convertToUtf8($returnBadgeFilePath)), 'AccountBannerPath' => intranet_handle_url($this->convertToUtf8($returnAccountPageBannerFilePath)), 'WebsiteURL' => $returnSchoolWebsiteURL);
	 		}

	 		if ($sys_custom['DHL'] || $sys_custom['eClassApp']['SFOC']) {
	 		    $bannerExist = 0;
	 		    $returnBackgroundFilePath = "";
	 		}
            return array('BannerExist' => $bannerExist, 'BannerPath' => intranet_handle_url($returnBackgroundFilePath), 'BadgePath' => intranet_handle_url($returnBadgeFilePath), 'WebsiteURL' => $returnSchoolWebsiteURL);
        }

		function inactivateUserDevice($parDeviceId, $parUserId='', $parDeviceOS='', $parReason='', $parRecordId='', $parLoginStatus='') {
			global $eclassAppConfig;

			if ($parDeviceId == '' && $parRecordId == '') {
				// must have either one
//				return false;

				// if user do not have google play service, both will be empty; therefore skip it
				return true;
			}

			if ($parUserId !== '') {
				$conds_userId = " And UserID = '".$parUserId."' ";
			}
			if ($parDeviceOS !== '') {
				$conds_deviceOS = " And DeviceOS = '".$parDeviceOS."' ";
			}
			if ($parDeviceId !== '') {
				$conds_deviceId = " And DeviceID = '".$this->Get_Safe_Sql_Query($parDeviceId)."' ";
			}
			if ($parRecordId !== '') {
				$conds_recordId = " And RecordID = '".$parRecordId."' ";
			}

			if ($parReason !== '') {
				$set_disableReason = " , DisableReason = '".$this->Get_Safe_Sql_Query($parReason)."' ";
			}

//			if ($parLoginStatus != '') {
//				$set_loginStatus = " , LoginStatus = '".$this->Get_Safe_Sql_Query($parLoginStatus)."' ";
//			}
			if ($parLoginStatus == '') {
				$parLoginStatus = $eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['deleted'];
			}
			$set_loginStatus = " , LoginStatus = '".$this->Get_Safe_Sql_Query($parLoginStatus)."' ";


			$sql = "Update APP_USER_DEVICE set RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive']."' $set_disableReason $set_loginStatus , DateModified = now() Where (LoginStatus != 'del' || ((LoginStatus is null) && RecordStatus = '1'))  $conds_deviceId $conds_recordId $conds_userId $conds_deviceOS";
			return $this->db_db_query($sql);
		}

		function deleteAppAccountServerFollowUp($parUserLogin, $parDeviceId='', $geTuiClientId='', $loginStatus='', $SessionID='') {
			global $eclassAppConfig;

			$SessionID = trim($SessionID);
			if(!empty($SessionID)) {
				$sql = "UPDATE APP_LOGIN_SESSION SET RecordStatus=0, DateModified=now() WHERE SessionID='$SessionID'";
				$this->db_db_query($sql);
			}

			$userAry = $this->getUserInfoByUserLogin($parUserLogin);
			$targetUserId = $userAry[0]['UserID'];
			if ($targetUserId == '') {
				$archiveUserAry = $this->getArchivedUserInfoByUserLogin($parUserLogin);
				$targetUserId = $archiveUserAry[0]['UserID'];
			}
			if ($targetUserId == '') {
				return '104';
			}
			if ($loginStatus == '') {
				$loginStatus = $eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['deleted'];
			}

			if ($loginStatus == $eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedOut']) {
				$disableReason = $eclassAppConfig['APP_USER_DEVICE']['DisableReason']['logoutAccount'];
			} else if ($loginStatus == $eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['deleted']) {
				$disableReason = $eclassAppConfig['APP_USER_DEVICE']['DisableReason']['deleteAccount'];
			}


			//$sql = "Update APP_USER_DEVICE set RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive']."', DateModified = now() Where UserID = '".$targetUserId."' And DeviceID = '".$this->Get_Safe_Sql_Query($parDeviceId)."'";
			//$success = $this->db_db_query($sql);
			$successAry = array();

			// For GCM:
			$parDeviceId = str_replace(' ', '', $parDeviceId);
			$success = $this->inactivateUserDevice($parDeviceId, $targetUserId, $parDeviceOS='', $disableReason, '', $loginStatus);
			$successAry[] = ($success)? "1" : "0";

			// For GeTui:
			$geTuiClientId = str_replace(' ', '', $geTuiClientId);
			$success = $this->inactivateUserDevice($geTuiClientId, $targetUserId, $parDeviceOS='', $disableReason, '', $loginStatus);
			$successAry[] = ($success)? "1" : "0";
			$returnValue =  !in_array("0", (array)$successAry) ? "1" : "0";

			$returnAry = array();
			$returnAry['DeleteSuccess'] = $returnValue;

			$successSync = $this->saveUserDeviceInfoToCloud($targetUserId);

			return $returnAry;
		}

		function isSchoolInLicense($appType) {
			$funcParamAry = get_defined_vars();
			$cacheResultAry = $this->getCacheResult('isSchoolInLicense', $funcParamAry);
	    	if ($cacheResultAry !== null) {
	    		return $cacheResultAry;
	    	}

			global $intranet_root, $config_school_code, $eclassAppConfig;

			if ($appType == $eclassAppConfig['appType']['Student']) {
				// student license is depending on the parent app
				$appType = $eclassAppConfig['appType']['Parent'];
			}

			$settingsName = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['appInLicense_'.$appType];
			$settingsObj = $this->getAppSettingsObj();
			$licenseLastModified = $settingsObj->getSettingsLastModifiedDate($settingsName);

			$nowTs = time();
			$licenseTs = strtotime($licenseLastModified);
			$timeDiff = $nowTs - $licenseTs;
			if ($licenseLastModified=='' || $timeDiff > $eclassAppConfig['refreshLicenseInterval']) {
				include_once($intranet_root."/includes/json.php");
				$jsonObj = new JSON_obj();

				$postParamAry = array();
				$postParamAry['RequestMethod'] = 'isSchoolInLicense';
				$postParamAry['SchoolCode'] = $config_school_code;
				$postParamAry['ModuleName'] = $eclassAppConfig['appLicenseModuleName'][$appType];
				$postJsonString = $jsonObj->encode($postParamAry);

				$headers = array('Content-Type: application/json');
				$centralServerUrl = $this->getCurCentralServerUrl();

				session_write_close();
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				$responseJson = curl_exec($ch);
				curl_close($ch);

				$responseJson = standardizeFormPostValue($responseJson);
				$responseJsonAry = $jsonObj->decode($responseJson);
				$isSchoolInAppLicense = $responseJsonAry['MethodResult']['InLicense'];

				$updateDb = false;
				$dbValue = '';
				if ($isSchoolInAppLicense == 'Y') {
					$updateDb = true;
					$dbValue = 1;
				}
				else if ($isSchoolInAppLicense == 'N') {
					$updateDb = true;
					$dbValue = 0;
				}

				if ($updateDb) {
					$tmpSettingsAssoAry = array();
					$tmpSettingsAssoAry[$settingsName] = $dbValue;
					$settingsObj->saveSettings($tmpSettingsAssoAry);
					$settingsObj->loadSettings($forceReload=true);
				}
			}

			$forceUseClass = true;
			//return ($isSchoolInAppLicense == 'Y')? true : false;
			//return ($settingsObj->getSettingsValue($settingsName) == 1)? true : false;
			$result = ($settingsObj->getSettingsValue($settingsName) == 1)? true : false;
			$this->setCacheResult('isSchoolInLicense', $funcParamAry, $result);
			return $result;
		}

		function convertToUtf8($str) {
			// no need convert for IP25
			//return iconv('BIG5-HKSCS', "UTF-8//IGNORE", $str);
			return $str;
		}

		function getModuleObjArr() {
			global $PATH_WRT_ROOT, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage;
			global $plugin, $special_feature, $eclassAppConfig, $indexVar;
			global $intranet_session_language, $Lang, $sys_custom;

	        # Current Page Information init
//	        switch ($CurrentPage) {
//				case "ParentApp_AccessRight":
//		        case "ParentApp_SchoolImage":
//		        case "ParentApp_LoginStatus":
//		        case "ParentApp_AuthCode":
//		        case "ParentApp_Blacklist":
//		        case "ParentApp_AdvancedSetting":
//		        	$PageParentApp = 1;
//		        	break;
//		        case "StudentApp_AccessRight":
//		        case "StudentApp_SchoolImage":
//		        	$PageStudentApp = 1;
//		        	break;
//		        case "TeacherApp_AccessRight":
//		        case "TeacherApp_SchoolImage":
//		        case "TeacherApp_LoginStatus":
//		        case "TeacherApp_AdvancedSetting":
//					$PageTeacherApp = 1;
//		        	break;
//		        case "CommonFunction_GroupMessage":
//		        	$PageCommonFunction = 1;
//		        	break;
//		    }
		    if (strtolower(substr($CurrentPage, 0, 6)) == 'parent') {
		    	$PageParentApp = 1;
		    }
		    else if (strtolower(substr($CurrentPage, 0, 7)) == 'student') {
		    	$PageStudentApp = 1;
		    }
		    else if (strtolower(substr($CurrentPage, 0, 7)) == 'teacher') {
		    	$PageTeacherApp = 1;
		    }
		    else if (strtolower(substr($CurrentPage, 0, 6)) == 'common') {
		    	$PageCommonFunction = 1;
		    }

		    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$eclassAppConfig['eAdminPath'];

		    $isBroadlearningAccount = false;
		    if ($sys_custom['eClassApp']['SFOC'] || $sys_custom['DHL']) {
				include_once($intranet_root.'/includes/libuser.php');
				$userObj = new libuser($_SESSION['UserID']);

				if ($userObj->UserLogin == 'broadlearning') {
					$isBroadlearningAccount = true;
				}
			}
			if($sys_custom['DHL']){
			    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
			    $libdhl = new libdhl();
			    $isPIC = $libdhl->isPIC();
			}

		    ### Parent App
		    if ($plugin['eClassApp'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"]) {
		    	$appType = $eclassAppConfig['appType']['Parent'];

		    	$MenuArr["ParentApp"] = array($Lang['eClassApp']['ParentApp'], "#", $PageParentApp);
				// Access Right
				$MenuArr["ParentApp"]["Child"]["AccessRight"] = array($Lang['eClassApp']['FunctionAccessRight'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/access_right/list', ($CurrentPage=='ParentApp_AccessRight'));
				// Appearance
				$MenuArr["ParentApp"]["Child"]["Appearance"] = array($Lang['eClassApp']['SchoolImage'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/school_image/list', ($CurrentPage=='ParentApp_SchoolImage'));
				// Login Status
				$MenuArr["ParentApp"]["Child"]["LoginStatus"] = array($Lang['eClassApp']['LoginStatus'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/login_status/list', ($CurrentPage=='ParentApp_LoginStatus'));

				if ($sys_custom['eClassApp']['authCode']) {
					// Auth Code
					$MenuArr["ParentApp"]["Child"]["AuthCode"] = array($Lang['eClassApp']['AuthorizationCode'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/auth_code/list', ($CurrentPage=='ParentApp_AuthCode'));
				}

				// Blacklist
				$MenuArr["ParentApp"]["Child"]["Blacklist"] = array($Lang['eClassApp']['Blacklist'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/blacklist/list', ($CurrentPage=='ParentApp_Blacklist'));

				// Advanced Setting
				$MenuArr["ParentApp"]["Child"]["AdvancedSetting"] = array($Lang['eClassApp']['AdvancedSetting'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/advanced_setting/push_message_right/list', ($CurrentPage=='ParentApp_AdvancedSetting'));

		    }

			### Student App
			$showStudentAppMenu = true;
			if ($sys_custom['eClassApp']['SFOC'] && !$isBroadlearningAccount) {
				$showStudentAppMenu = false;
			}
			if ($showStudentAppMenu && $plugin['eClassStudentApp'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"]) {
				$appType = $eclassAppConfig['appType']['Student'];

		    	$MenuArr["StudentApp"] = array($Lang['eClassApp']['StudentApp'], "#", $PageStudentApp);
				// Access Right
				$MenuArr["StudentApp"]["Child"]["AccessRight"] = array($Lang['eClassApp']['FunctionAccessRight'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=studentApp/access_right/list', ($CurrentPage=='StudentApp_AccessRight'));
				// Appearance
				$MenuArr["StudentApp"]["Child"]["Appearance"] = array($Lang['eClassApp']['SchoolImage'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=studentApp/school_image/list', ($CurrentPage=='StudentApp_SchoolImage'));
				// Login Status
				$MenuArr["StudentApp"]["Child"]["LoginStatus"] = array($Lang['eClassApp']['LoginStatus'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=studentApp/login_status/list', ($CurrentPage=='StudentApp_LoginStatus'));
				// Blacklist
//				$MenuArr["StudentApp"]["Child"]["Blacklist"] = array($Lang['eClassApp']['Blacklist'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=studentApp/blacklist/list', ($CurrentPage=='StudentApp_Blacklist'));
			}


			### Teacher App
			if ($plugin['eClassTeacherApp'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] || ($sys_custom['DHL'] && $isPIC))) {
				$appType = $eclassAppConfig['appType']['Teacher'];

				if ($sys_custom['DHL']) {
					$PageTeacherApp = true;
				}

		    	$MenuArr["TeacherApp"] = array($Lang['eClassApp']['TeacherApp'], "#", $PageTeacherApp);
				// Access Right
				$showAccessRightMenu = true;
				if ($sys_custom['DHL'] && !$isBroadlearningAccount) {
					$showAccessRightMenu = false;
				}

				if ($showAccessRightMenu) {
					$MenuArr["TeacherApp"]["Child"]["AccessRight"] = array($Lang['eClassApp']['FunctionAccessRight'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=teacherApp/access_right/list', ($CurrentPage=='TeacherApp_AccessRight'));
				}

				// Appearance
				$showSchoolImageMenu = true;
				if ($sys_custom['DHL'] && !$isBroadlearningAccount) {
					$showSchoolImageMenu = false;
				}
				if ($showSchoolImageMenu) {
					$MenuArr["TeacherApp"]["Child"]["Appearance"] = array($Lang['eClassApp']['SchoolImage'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=teacherApp/school_image/list', ($CurrentPage=='TeacherApp_SchoolImage'));
				}
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]){
				    // Login Status
				    $MenuArr["TeacherApp"]["Child"]["LoginStatus"] = array($Lang['eClassApp']['LoginStatus'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=teacherApp/login_status/list', ($CurrentPage=='TeacherApp_LoginStatus'));

				}
				  //  Advanced Setting
		       if ($sys_custom['DHL']) {
			       	// hide advanced settings for DHL project
		       }
		       else {
		       		$MenuArr["TeacherApp"]["Child"]["AdvancedSetting"] = array($Lang['eClassApp']['AdvancedSetting'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=teacherApp/advanced_setting/push_message_right/list', ($CurrentPage=='TeacherApp_AdvancedSetting'));
		       }
		    }

			### Common Function
		    if (($sys_custom['eClassApp']['groupMessage']||$sys_custom['eClassApp']['schoolInfo']) && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"] || ($sys_custom['DHL'] && $isPIC))) {
				$appType = $eclassAppConfig['appType']['Common'];

				if ($sys_custom['DHL']) {
					// show Group Message and School News in Teacher App section for DHL project
					$menuKey = 'TeacherApp';
				}
				else {
					$menuKey = 'CommonFunction';
					$MenuArr["CommonFunction"] = array($Lang['eClassApp']['CommonFunction'], "#", $PageCommonFunction);
				}

				// Access Right
				if ($sys_custom['eClassApp']['groupMessage'] && get_client_region() != 'zh_CN' && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"])) {
					$MenuArr[$menuKey]["Child"]["GroupMessage"] = array($Lang['eClassApp']['ModuleNameAry']['GroupMessage'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=commonFunction/group_message/index', ($CurrentPage=='CommonFunction_GroupMessage'));
				}

				if ($sys_custom['eClassApp']['schoolInfo'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] || ($isPIC && $sys_custom['DHL']))) {
					$MenuArr[$menuKey]["Child"]["SchoolInfo"] = array($Lang['eClassApp']['ModuleNameAry']['SchoolInfo'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=commonFunction/school_info/index', ($CurrentPage=='CommonFunction_SchoolInfo'));
				}
			}
			
			
			### Class Teacher Only
			if(empty($MenuArr) && $_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher']){
				$appType = $eclassAppConfig['appType']['Parent'];
				
				$MenuArr["ParentApp"] = array($Lang['eClassApp']['ParentApp'], "#", $PageParentApp);
				// Login Status
				$MenuArr["ParentApp"]["Child"]["LoginStatus"] = array($Lang['eClassApp']['LoginStatus'], $MODULE_OBJ['root_path'].'?appType='.$appType.'&task=parentApp/login_status/list', ($CurrentPage=='ParentApp_LoginStatus'));
			}

			if ($sys_custom['DHL']) {
				$logoName = 'icon_DHLApp';
			}
			else {
				$logoName = 'icon_eClassApp';
			}

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass eClassApp";'."\n";
            $js.= '</script>'."\n";

		    ### module information
	        $MODULE_OBJ['title'] = $Lang['Header']['Menu']['eClassApp'].$js;
	        $MODULE_OBJ['title_css'] = "menu_opened";
	        $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/".$logoName.".png";
	        $MODULE_OBJ['menu'] = $MenuArr;

	        return $MODULE_OBJ;
		}

		function getAccessRightInfo($parAppType='') {
			global $eclassAppConfig, $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

			### add default value if not set
			if ($parAppType == '') {
				$parAppType = $eclassAppConfig['appType']['Parent'];
			}
			$moduleAry = $this->getAppApplicableModule($parAppType);
			$numOfModule = count($moduleAry);


			### get access right settings for the enabled modules
			if ($parAppType != '') {
				$conds_appType = " And AppType = '".$parAppType."' ";
			}
			$sql = "Select AppType, YearID, Module, RecordStatus From APP_ACCESS_RIGHT Where Module In ('".implode("','", (array)$moduleAry)."') $conds_appType";
			$accessRightAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('AppType', 'YearID', 'Module'), array('RecordStatus'));

			### get all forms
			$formAry = array();
			if ($parAppType == $eclassAppConfig['appType']['Parent'] || $parAppType == $eclassAppConfig['appType']['Student']) {
				$libYear = new Year();
				$formAry = $libYear->Get_All_Year_List();
			}
			else if ($parAppType == $eclassAppConfig['appType']['Teacher']) {
				$formAry[0]['YearID'] = 0;
			}
			$numOfForm = count($formAry);

			for ($i=0; $i<$numOfForm; $i++) {
				$_yearId = $formAry[$i]['YearID'];

				for ($j=0; $j<$numOfModule; $j++) {
					$__moduleCode = $moduleAry[$j];

					if (!isset($accessRightAssoAry[$parAppType][$_yearId][$__moduleCode])) {
// 					    $accessRightAssoAry[$parAppType][$_yearId][$__moduleCode]['RecordStatus'] = 1;
                        // Student App eEnrol - default turn off
					    if ($parAppType==$eclassAppConfig['appType']['Student'] && ($__moduleCode=='eEnrolmentStudentView' || $__moduleCode=='ePOS')) {
					        $accessRightAssoAry[$parAppType][$_yearId][$__moduleCode]['RecordStatus'] = 0;
					    }
					    else if ($parAppType==$eclassAppConfig['appType']['Parent'] && $__moduleCode=='ePOS') {
					        $accessRightAssoAry[$parAppType][$_yearId][$__moduleCode]['RecordStatus'] = 0;
					    } else if ($parAppType==$eclassAppConfig['appType']['Parent'] && $__moduleCode=='bodyTemperature') {
							$accessRightAssoAry[$parAppType][$_yearId][$__moduleCode]['RecordStatus'] = 0;
					    }
					    else {
					        $accessRightAssoAry[$parAppType][$_yearId][$__moduleCode]['RecordStatus'] = 1;
					    }
					}
				}
			}

			$returnAry = array();
			if ($parAppType != '') {
				$returnAry = $accessRightAssoAry[$parAppType];
			}
			else {
				$returnAry = $accessRightAssoAry;
			}

			return $returnAry;
		}

		function getAppApplicableModule($appType='') {
			global $plugin, $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root,$sys_custom,$special_feature;

			if ($appType == '') {
				$appType = $eclassAppConfig['appType']['Parent'];
			}

			$moduleAry = array();

			if ($appType == $eclassAppConfig['appType']['Parent']) {
				// apply leave & eAttendance
				if ($plugin['attendancestudent']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['applyLeave'];
					$moduleAry[] = $eclassAppConfig['moduleCode']['eAttendance'];
				}

				// Cross Boundary School Coaches
				if ($plugin['CrossBoundarySchoolCoaches']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['crossBoundarySchoolCoaches'];
				}

				// Digital Channels
				if (isKIS()) {
				    // Photo Album is a general feature in KIS
				    //$moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				    if ($plugin['DigitalChannels']) {
				        $moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				    }
				    else {
				        if ($sys_custom['KIS_HidePhotoAlbum']) {
				            // hide photo album
				        }
				        else {
				            $moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				        }
				    }
				}
				else {
					if ($plugin['DigitalChannels']) {
						$moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
					}
				}

				// eEnrolment
				if ($plugin['eEnrollment']) {
				    if ($_SESSION['platform'] == "KIS" && $sys_custom['KIS_eEnrolment']['DisableApp']){
				    }else{
				        $moduleAry[] = $eclassAppConfig['moduleCode']['eEnrolmentUserView'];
				    }
				}

				// eHomework
				if (isKIS() && !($sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] && $sys_custom['KIS_eHomework']['EnableModule'])) {
					// KIS does not have eHomework
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['eHomework'];
				}

				// eNotice
				include_once($PATH_WRT_ROOT.'includes/libnotice.php');
				$lnotice = new libnotice();
				if (!$lnotice->disabled) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['eNotice'];
				}

				// ePayment
				if ($plugin['payment']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['ePayment'];
				}

				if ($plugin['eSchoolBus']) {
				    $moduleAry[] = $eclassAppConfig['moduleCode']['eSchoolBus'];
				}

				// Group Message
				//if($sys_custom['eClassApp']['groupMessage']){
				if (get_client_region() == 'zh_CN'){
					// disable Group Message for China client
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['groupMessage'];
				}

				// School Calendar
				$moduleAry[] = $eclassAppConfig['moduleCode']['schoolCalendar'];

				//School Info
				//if($sys_custom['eClassApp']['schoolInfo']){
					$moduleAry[] = $eclassAppConfig['moduleCode']['schoolInfo'];
				//}

				// School News
				//if (isKIS()) {
					// KIS does not have school news
				//}
				//else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['schoolNews'];
				//}

				//Medical Caring
					if (($plugin['medical_module']['bowel'] && !$plugin['medical_module']['blockParent']['bowel']) || ($plugin['medical_module']['studentLog'] && !$plugin['medical_module']['blockParent']['studentLog']) || ($plugin['medical_module']['sleep'] && !$plugin['medical_module']['blockParent']['sleep'])) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['medicalCaring'];
				}

				// HKU Flu
				if ($sys_custom['eClassApp']['HKUSPH']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['hkuFlu'];
				}

				// Smart Card Reprint
                if ($sys_custom['eClassApp']['enableReprintCard']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['reprintCard'];
                }

                if ($plugin['iPortfolio']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['iPortfolio'];
                }

                // ePOS
                if ($plugin['ePOS']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['ePOS'];
                }

                //iMail
				if ($sys_custom['PowerClass']) {

				} else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['iMail'];
				}

                if ($sys_custom['PowerClass']||isKIS()) {
                    // hide eLearningTimetable for PowerClass and kis
                }
                else {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['eLearningTimetable'];
                }

                //if($sys_custom['ParentAppBodyTemperature']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['bodyTemperature'];
				//}
            }
			else if ($appType == $eclassAppConfig['appType']['Teacher']) {
				include_once($PATH_WRT_ROOT.'includes/libaccess.php');
				$laccess = new libaccess();

				// Digital Channels
				if (isKIS()) {
				    // Photo Album is a general feature in KIS
				    //$moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				    if ($plugin['DigitalChannels']) {
				        $moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				    }
				    else {
				        if ($sys_custom['KIS_HidePhotoAlbum']) {
				            // hide photo album
				        }
				        else {
				            $moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				        }
				    }
				}
				else {
					if ($plugin['DigitalChannels']) {
						$moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
					}
				}

				// Take Attendance
				if ($plugin['attendancestudent']||$plugin['attendancelesson']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['takeAttendance'];
				}

				// eCircular
				//if (isKIS()) {
					// KIS does not have eCircular
				//}
				//else {
					include_once($PATH_WRT_ROOT.'includes/libcircular.php');
					$lcircular = new libcircular();
					if (!$lcircular->disabled) {
						$moduleAry[] = $eclassAppConfig['moduleCode']['eCircular'];
					}
				//}

				//eEnrolment
				if($plugin['eEnrollment']){
					$moduleAry[] = $eclassAppConfig['moduleCode']['eEnrolment'];
				}

				// eHomework
				if (isKIS() && !($sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] && $sys_custom['KIS_eHomework']['EnableModule'])) {
					// KIS does not have eHomework
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['eHomework'];
				}

				//ENoticeS
				$moduleAry[] = $eclassAppConfig['moduleCode']['eNoticeS'];

				// Flipped Channels
				if ($plugin['FlippedChannels']) {
// 					if ($sys_custom['PowerClass']) {
// 						// hide FC for powerclass
// 					}
// 					else {
						$moduleAry[] = $eclassAppConfig['moduleCode']['flippedchannel'];
// 					}
				}


				// Group Message
				//if($sys_custom['eClassApp']['groupMessage']){
				if (get_client_region() == 'zh_CN'){
					// disable Group Message for China client
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['groupMessage'];
				}

				// iMail
				if ($sys_custom['PowerClass']) {
					// hide iMail for PowerClass
				}
				else {
					if ($laccess->retrieveAccessCampusmailForType(1)) {		// 1 means teacher
						$moduleAry[] = $eclassAppConfig['moduleCode']['iMail'];
					}
				}


				// School Calendar
				$moduleAry[] = $eclassAppConfig['moduleCode']['schoolCalendar'];

				//School Info
				//if($sys_custom['eClassApp']['schoolInfo']){
			     	$moduleAry[] = $eclassAppConfig['moduleCode']['schoolInfo'];
			  	//}

				// School News
				//if (isKIS()) {
					// KIS does not have school news
				//}
				//else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['schoolNews'];
				//}

				// Staff Attendance
				if ($plugin['attendancestaff']) {
					if ($sys_custom['PowerClass']) {
						// hide Staff Attendance for PowerClass
					}
					else {
						$moduleAry[] = $eclassAppConfig['moduleCode']['staffAttendance'];
					}
				}


				//Teacher Status
				$moduleAry[] = $eclassAppConfig['moduleCode']['teacherStatus'];

				//Student Status
				$moduleAry[] = $eclassAppConfig['moduleCode']['studentStatus'];

				// Student Performance
//				if($sys_custom['eClassApp']['studentPerformance']){
//					$moduleAry[] = $eclassAppConfig['moduleCode']['studentPerformance'];
//				}
				if (isKIS() || $sys_custom['PowerClass'] || $special_feature['lite_for_eAdmin']) {
					// hide student perfomance for KIS and PowerClass
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['studentPerformance'];
				}

				//Medical Caring
				if ($plugin['medical_module']['bowel'] || $plugin['medical_module']['studentLog'] || $plugin['medical_module']['sleep'] ) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['medicalCaring'];
				}

				//eBooking
				if($plugin['eBooking']){
					$moduleAry[] = $eclassAppConfig['moduleCode']['eBooking'];
				}

				//eDiscipline (excluded for 10 enabled flags)
				if($plugin['Disciplinev12'] &&
					!(
						$sys_custom['eDiscipline']['PooiToMiddleSchool'] ||
						$sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] ||
						$sys_custom['eDiscipline']['yy3'] ||
						$sys_custom['eDiscipline']['CSCProbation'] ||
						$sys_custom['eDiscipline']['HeungToHideWarningwhenAddAP'] ||
						$sys_custom['eDiscipline']['add_AP_records_hide_meritnconduct'] ||
						$sys_custom['eDiscipline']['add_AP_records_send_mail_to_classteacher'] ||
						$sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] ||
						$sys_custom['Discipline_AP_Item_Tag_Seperate_Function'] ||
						$sys_custom['Discipline_AP_Item_Tag_ControlConductType']
					)
				)
				{
					$moduleAry[] = $eclassAppConfig['moduleCode']['eDiscipline'];
				}

				if ($plugin['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) {
				    $moduleAry[] = $eclassAppConfig['moduleCode']['hostelTakeAttendance'];
				}

                if ($plugin['eSchoolBus']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['eSchoolBusTeacher'];
                }

                if ($plugin['iPortfolio']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['iPortfolio'];
                }
                
                if ($plugin['Inventory']) {
                     $moduleAry[] = $eclassAppConfig['moduleCode']['eInventory'];
                     $moduleAry[] = $eclassAppConfig['moduleCode']['eInventoryStocktake'];
                }
                
			}
			else if ($appType == $eclassAppConfig['appType']['Student']) {
				// eHomework
				if (isKIS() && !($sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] && $sys_custom['KIS_eHomework']['EnableModule'])) {
					// KIS does not have eHomework
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['eHomework'];
				}

				// eNotice
				include_once($PATH_WRT_ROOT.'includes/libnotice.php');
				$lnotice = new libnotice();
				if (!$lnotice->disabled) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['eNotice'];
				}

				// School Calendar
				//$moduleAry[] = $eclassAppConfig['moduleCode']['schoolCalendar'];

				// School Info
				$moduleAry[] = $eclassAppConfig['moduleCode']['schoolInfo'];

				// School News
				$moduleAry[] = $eclassAppConfig['moduleCode']['schoolNews'];

				if ($sys_custom['PowerClass']) {
					// hide weekly diary and newscut for PowerClass
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['weeklyDiary'];
				}


				if ($plugin['library_management_system']) {
					$moduleAry[] = $eclassAppConfig['moduleCode']['eLibPlus'];
				}

				if ($sys_custom['PowerClass']) {
					// hide timetable for PowerClass
				}
				else {
					$moduleAry[] = $eclassAppConfig['moduleCode']['timetable'];
				}

				// School Calendar
				$moduleAry[] = $eclassAppConfig['moduleCode']['schoolCalendar'];

                // Classroom
				if (isKIS() || $sys_custom['PowerClass']) {
				    // hide classroom for KIS and PowerClass
				}
				else {
				    $moduleAry[] = $eclassAppConfig['moduleCode']['classroom'];
				}

                // iPortfolio
				if (isKIS() || $sys_custom['PowerClass']) {
				    // hide classroom for KIS and PowerClass
				}
				else {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['iPortfolio'];
				}

				// Digital Channels
				if (isKIS()) {
				    // Photo Album is a general feature in KIS
				    //$moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				    if ($plugin['DigitalChannels']) {
				        $moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				    }
				    else {
				        if ($sys_custom['KIS_HidePhotoAlbum']) {
				            // hide photo album
				        }
				        else {
				            $moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
				        }
				    }
				}
				else {
					if ($plugin['DigitalChannels']) {
						$moduleAry[] = $eclassAppConfig['moduleCode']['digitalChannels'];
					}
				}

                // Smart Card Reprint
//                 if ($sys_custom['eClassApp']['enableReprintCard']) {
//                     $moduleAry[] = $eclassAppConfig['moduleCode']['reprintCard'];
//                 }

                // iMail

                if ($sys_custom['eClassStudentApp']['enableiMail']) {		// 1 means teacher
                    $moduleAry[] = $eclassAppConfig['moduleCode']['iMail'];
                }

                // eEnrolment
                if ($plugin['eEnrollment']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['eEnrolmentStudentView'];
                }

                // ePOS
                if ($plugin['ePOS']) {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['ePOS'];
                }


                if ($sys_custom['PowerClass']||isKIS()) {
                    // hide eLearningTimetable for PowerClass and kis
                }
                else {
                    $moduleAry[] = $eclassAppConfig['moduleCode']['eLearningTimetable'];
                }
            }

			return $moduleAry;
		}

		function getModuleName($moduleCode) {
		    global $Lang, $eclassAppConfig, $plugin;

			switch ($moduleCode) {
				case $eclassAppConfig['moduleCode']['applyLeave']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['ApplyLeave'];
					break;
				case $eclassAppConfig['moduleCode']['eAttendance']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eAttendance'];
					break;
				case $eclassAppConfig['moduleCode']['takeAttendance']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['takeAttendance'];
					break;
				case $eclassAppConfig['moduleCode']['eHomework']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eHomework'];
					break;
				case $eclassAppConfig['moduleCode']['eNotice']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eNotice'];
					break;
				case $eclassAppConfig['moduleCode']['ePayment']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['ePayment'];
					break;
				case $eclassAppConfig['moduleCode']['schoolCalendar']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['SchoolCalendar'];
					break;
				case $eclassAppConfig['moduleCode']['schoolNews']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['SchoolNews'];
					break;
				case $eclassAppConfig['moduleCode']['iMail']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['iMail'];
					break;
				case $eclassAppConfig['moduleCode']['eCircular']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eCircular'];
					break;
				case $eclassAppConfig['moduleCode']['eNoticeS']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eNotice'];
					break;
				case $eclassAppConfig['moduleCode']['crossBoundarySchoolCoaches']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['CrossBoundarySchoolCoaches'];
					break;
				case $eclassAppConfig['moduleCode']['teacherStatus']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['TeacherStatus'];
					break;
				case $eclassAppConfig['moduleCode']['eEnrolment']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eEnrolment'];
					break;
				case $eclassAppConfig['moduleCode']['studentStatus']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['StudentStatus'];
					break;
				case $eclassAppConfig['moduleCode']['staffAttendance']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['StaffAttendance'];
					break;
				case $eclassAppConfig['moduleCode']['studentPerformance']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['StudentPerformance'];
					break;
				case $eclassAppConfig['moduleCode']['schoolInfo']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['SchoolInfo'];
					break;
				case $eclassAppConfig['moduleCode']['groupMessage']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['GroupMessage'];
					break;
				case $eclassAppConfig['moduleCode']['flippedchannel']:
				    $moduleName = $Lang['eClassApp']['ModuleNameAry']['flippedchannel'];
				    break;
				case $eclassAppConfig['moduleCode']['digitalChannels']:
				    $moduleName = (isKIS() && !$plugin['DigitalChannels'])? $Lang['eClassApp']['ModuleNameAry']['photoAlbum'] : $Lang['eClassApp']['ModuleNameAry']['digitalChannels'];
				    break;
				case $eclassAppConfig['moduleCode']['medicalCaring']:
			    	$moduleName = $Lang['eClassApp']['ModuleNameAry']['medicalCaring'];
			    	break;
			    case $eclassAppConfig['moduleCode']['weeklyDiary']:
			    	$moduleName = $Lang['eClassApp']['ModuleNameAry']['weeklyDiary'];
			    	break;
			    case $eclassAppConfig['moduleCode']['eLibPlus']:
			    	$moduleName = $Lang['eClassApp']['ModuleNameAry']['eLibPlus'];
			    	break;
			    case $eclassAppConfig['moduleCode']['timetable']:
			    	$moduleName = $Lang['eClassApp']['ModuleNameAry']['timetable'];
			    	break;
                case $eclassAppConfig['moduleCode']['hkuFlu']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['hkuFlu'];
                    break;
                case $eclassAppConfig['moduleCode']['classroom']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['classroom'];
                    break;
                case $eclassAppConfig['moduleCode']['eBooking']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eBooking'];
					break;
				case $eclassAppConfig['moduleCode']['eDiscipline']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['eDiscipline'];
					break;
				case $eclassAppConfig['moduleCode']['hostelTakeAttendance']:
				    $moduleName = $Lang['eClassApp']['ModuleNameAry']['hostelTakeAttendance'];
				    break;
				case $eclassAppConfig['moduleCode']['iPortfolio']:
				    $moduleName = $Lang['eClassApp']['ModuleNameAry']['iPortfolio'];
				    break;
				case $eclassAppConfig['moduleCode']['eSchoolBus']:
				    $moduleName = $Lang['eClassApp']['ModuleNameAry']['eSchoolBus'];
				    break;
				case $eclassAppConfig['moduleCode']['eEnrolmentUserView']:
				    $moduleName = $Lang['eClassApp']['ModuleNameAry']['eEnrolmentUserView'];
				    break;
                case $eclassAppConfig['moduleCode']['reprintCard']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['reprintCard'];
                    break;
                case $eclassAppConfig['moduleCode']['eSchoolBusTeacher']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['eSchoolBus'];
                    break;
                case $eclassAppConfig['moduleCode']['eEnrolmentStudentView']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['eEnrolmentUserView'];
                    break;
                case $eclassAppConfig['moduleCode']['ePOS']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['ePOS'];
                    break;
                case $eclassAppConfig['moduleCode']['eInventory']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['eInventory'];
                    break;
                case $eclassAppConfig['moduleCode']['eLearningTimetable']:
                    $moduleName = $Lang['eClassApp']['ModuleNameAry']['eLearningTimetable'];
                    break;
				case $eclassAppConfig['moduleCode']['bodyTemperature']:
					$moduleName = $Lang['eClassApp']['ModuleNameAry']['bodyTemperature'];
					break;
				default:
					$moduleName = '';
			}
			return $moduleName;
		}

		function saveModuleAccessRightSettings($appType, $settingAssoAry) {
			$sql = "Select RecordID, Module, YearID From APP_ACCESS_RIGHT Where AppType = '".$appType."'";
			$curSettingsAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('Module', 'YearID'), array('RecordID'));

			$successAry = array();
			foreach ((array)$settingAssoAry as $_moduleCode => $_moduleYearAry) {
				foreach ((array)$_moduleYearAry as $__yearId => $__isEnable) {
					$__recordStatus = ($__isEnable)? 1 : 0;

					if (isset($curSettingsAssoAry[$_moduleCode][$__yearId])) {
						// edit
						$__recordId = $curSettingsAssoAry[$_moduleCode][$__yearId]['RecordID'];
						$sql = "Update
										APP_ACCESS_RIGHT 
								Set 
										RecordStatus = '".$__recordStatus."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."'
								Where
										RecordID = '".$__recordId."'
								";
						$successAry['update'][$_moduleCode][$__yearId] = $this->db_db_query($sql);
					}
					else {
						// insert
						$sql = "Insert Into APP_ACCESS_RIGHT 
									(AppType, Module, YearID, RecordStatus, InputDate, InputBy, ModifiedDate, ModifiedBy)
								Values
									('".$this->Get_Safe_Sql_Query($appType)."', '".$this->Get_Safe_Sql_Query($_moduleCode)."', '".$__yearId."', '".$__recordStatus."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
								";
						$successAry['insert'][$_moduleCode][$__yearId] = $this->db_db_query($sql);
					}
				}
			}

			return !in_multi_array(false, (array)$successAry);
		}

		function getSchoolImage($appType, $imageType='') {
			if ($imageType != '') {
				$conds_imageType = " And ImageType = '".$imageType."' ";
			}
			$sql = "Select RecordID, ImageType, RecordStatus, ImagePath from APP_SCHOOL_IMAGE Where AppType = '".$appType."' And RecordStatus > 0 $conds_imageType";
			return $this->returnResultSet($sql);
		}

		function getSchoolImageUrlPath($appType, $imageType) {
			global $eclassAppConfig, $image_path, $LAYOUT_SKIN;

			$imageInfoAry = $this->getSchoolImage($appType, $imageType);
			$imagePath = $imageInfoAry[0]['ImagePath'];
            $status = $imageInfoAry[0]['RecordStatus'];

            if($status == 1){
                $urlPath = curPageURL($withQueryString=false, $withPageSuffix=false).'/'.$eclassAppConfig['urlBrowsePath'].$imagePath;
            }else{
                if(($imageType != 'badge') && ($appType != $eclassAppConfig['appType']['Student'])){
                    if ($appType == $eclassAppConfig['appType']['Parent'])
                    {
                        $defaultImageFolder = 'parentApp';
                    }
                    if ($appType == $eclassAppConfig['appType']['Teacher'])
                    {
                        $defaultImageFolder = 'teacherApp';
                    }
                    if ($appType == $eclassAppConfig['appType']['Student'])
                    {
                        $defaultImageFolder = 'studentApp';
                    }

                    if ($imagePath == "")
                    {
                        $imagePath = 2;
                    }

                    $urlPath = $image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/'.$imageType.'/default'.$imagePath.'.jpg';
                }else{
                    $urlPath = "";
                }
            }


			return $urlPath;
		}

		function saveSchoolDefaultImage($appType, $imageType, $defaultImageNo = 0) {
            global $eclassAppConfig, $PATH_WRT_ROOT;
            include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
            $lfs = new libfilesystem();

            if(!$defaultImageNo || $defaultImageNo == "" || $defaultImageNo < 1){
                $defaultImageNo = $eclassAppConfig['appIamge']['defaultImage']['defaultSelection'];
            }

            // backup old file first
            $sql = "Select RecordID, RecordStatus, ImagePath From APP_SCHOOL_IMAGE Where AppType = '".$appType."' And ImageType = '".$imageType."'";
            $dbImageInfoAry = $this->returnResultSet($sql);
            $recordId = $dbImageInfoAry[0]['RecordID'];
            $recordStatus = $dbImageInfoAry[0]['RecordStatus'];
            $oldImagePath = $dbImageInfoAry[0]['ImagePath'];
            $oldFilePath = $eclassAppConfig['urlFilePath'].$oldImagePath;
            if ($recordId > 0 && file_exists($oldFilePath) && $recordStatus == $eclassAppConfig['appImage']['status']['upload']) {
                $successAry['backupOldFile'] = $lfs->file_rename($oldFilePath, $oldFilePath.'.'.date('Ymd_His'));
            }
            if ($recordId > 0) {
                // update
                $sql = "Update APP_SCHOOL_IMAGE Set ImagePath = $defaultImageNo, RecordStatus = '".$eclassAppConfig['appImage']['status']['defaultImage']."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RecordID = '".$recordId."'";
                $successAry['updateDbRecord'] = $this->db_db_query($sql);
            }
            else {
                // insert
                $sql = "Insert Into APP_SCHOOL_IMAGE 
									(AppType, ImageType, ImagePath, RecordStatus, InputDate, InputBy, ModifiedDate, ModifiedBy)
								Values
									('".$appType."', '".$imageType."', $defaultImageNo, '".$eclassAppConfig['appImage']['status']['defaultImage']."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
								";
                $successAry['insertDbRecord'] = $this->db_db_query($sql);
            }

            return !in_array(false, (array)$successAry);
        }

		function saveSchoolImage($appType, $imageType, $parImageInfoAry) {
			global $eclassAppConfig, $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$lfs = new libfilesystem();

			$successAry = array();
			if ($parImageInfoAry['name'] != ''  && $parImageInfoAry['error'] > 0) {
				$successAry['imageDataValid'] = false;
			}
			else if ($parImageInfoAry['name'] == '') {
			    $successAry['emptyImage'] = true;
			}
			else {
				// create root folder
				$tmpFilePath = $eclassAppConfig['urlFilePath'].'schoolImage/';
				if (!file_exists($tmpFilePath)) {
					$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
				}

				// backup old file first
				$sql = "Select RecordID, ImagePath From APP_SCHOOL_IMAGE Where AppType = '".$appType."' And ImageType = '".$imageType."'";
				$dbImageInfoAry = $this->returnResultSet($sql);
				$recordId = $dbImageInfoAry[0]['RecordID'];
				$oldImagePath = $dbImageInfoAry[0]['ImagePath'];
				$oldFilePath = $eclassAppConfig['urlFilePath'].$oldImagePath;
				if ($recordId > 0 && file_exists($oldFilePath)) {
					$successAry['backupOldFile'] = $lfs->file_rename($oldFilePath, $oldFilePath.'.'.date('Ymd_His'));
				}

				// upload new files
				$orgFileName = $parImageInfoAry['name'];
				$tmpName = $parImageInfoAry['tmp_name'];
				$fileExt = get_file_ext($orgFileName);
				$targetFileName = $appType.'_'.$imageType.$fileExt;
				$targetFilePath = $tmpFilePath.$targetFileName;

				//Resize before upload
				// Create image from file
				$info = getimagesize($parImageInfoAry['tmp_name']);
				$imageMineType = $info['mime'];
				switch(strtolower($imageMineType))
				{
				     case 'image/jpeg':
				     case 'image/pjpeg':   //For IE only
				         $image = imagecreatefromjpeg($parImageInfoAry['tmp_name']);
				         break;
				     case 'image/png':
				     case 'image/x-png':	   //For IE only
				         $image = imagecreatefrompng($parImageInfoAry['tmp_name']);
				         break;
				     case 'image/gif':
				         $image = imagecreatefromgif($parImageInfoAry['tmp_name']);
				         break;
				}

				// Target dimensions
				switch($imageType) {
				    case $eclassAppConfig['imageType']['accountPageBanner']:
				        $max_width = $eclassAppConfig['suggestedResolution']['accountPageBanner']['width'];
				        $max_height = $eclassAppConfig['suggestedResolution']['accountPageBanner']['height'];
				        break;
			        case $eclassAppConfig['imageType']['backgroundImage']:
			            $max_width = $eclassAppConfig['suggestedResolution']['backgroundImage']['width'];
			            $max_height = $eclassAppConfig['suggestedResolution']['backgroundImage']['height'];
			            break;
			        default:
			            $max_width = $eclassAppConfig['suggestedResolution']['schoolBadge']['width'];
			            $max_height = $eclassAppConfig['suggestedResolution']['schoolBadge']['height'];
		                break;
				}

// 				if ($imageType == $eclassAppConfig['imageType']['backgroundImage']){
// 					$max_width = 1080;
// 					$max_height = 246;
// 				}else{
// 					$max_width = 300;
// 					$max_height = 300;
// 				}

				// Get current dimensions
				$old_width  = imagesx($image);
				$old_height = imagesy($image);

				// Calculate the scaling we need to do to fit the image inside our frame
				$scale      = min($max_width/$old_width, $max_height/$old_height);

				if ($scale < 1){//if resize is needed
					// Get the new dimensions
					$new_width  = ceil($scale*$old_width);
					$new_height = ceil($scale*$old_height);

					// Create new empty image
					$new = imagecreatetruecolor($new_width, $new_height);

					// Keep the transparent background for png
					imagealphablending($new, false);
				    imagesavealpha($new, true);
				    $transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
				    imagefilledrectangle($new, 0, 0, $old_width, $old_height, $transparent);

					// Resize old image into new
					imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

					// Catch the imagedata
					$successAry['uploadFile'] = imagepng($new, $targetFilePath);

					// Destroy resources
					imagedestroy($image);
					imagedestroy($new);
				}else{ //if no resize process is needed
					$successAry['uploadFile'] = $lfs->file_copy($tmpName, $targetFilePath);
				}

				if ($successAry['uploadFile']) {
					$dbFilePath = str_replace($eclassAppConfig['urlFilePath'], '', $targetFilePath);
					if ($recordId > 0) {
						// update
						$sql = "Update APP_SCHOOL_IMAGE Set ImagePath = '".$this->Get_Safe_Sql_Query($dbFilePath)."', RecordStatus = '".$eclassAppConfig['appImage']['status']['upload']."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RecordID = '".$recordId."'";
						$successAry['updateDbRecord'] = $this->db_db_query($sql);
					}
					else {
						// insert
						$sql = "Insert Into APP_SCHOOL_IMAGE 
									(AppType, ImageType, ImagePath, RecordStatus, InputDate, InputBy, ModifiedDate, ModifiedBy)
								Values
									('".$appType."', '".$imageType."', '".$dbFilePath."', '".$eclassAppConfig['appImage']['status']['upload']."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
								";
						$successAry['insertDbRecord'] = $this->db_db_query($sql);
					}
				}
			}

			return !in_array(false, (array)$successAry);
		}
//		function saveSchoolImage($appType, $imageType, $parImageInfoAry) {
//			global $eclassAppConfig, $PATH_WRT_ROOT;
//
//			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
//			$lfs = new libfilesystem();
//
//			$successAry = array();
//			if ($parImageInfoAry['name'] != ''  && $parImageInfoAry['error'] > 0) {
//				$successAry['imageDataValid'] = false;
//			}
//			else if ($parImageInfoAry['name'] == '') {
//				$successAry['emptyImage'] = true;
//			}
//			else {
//				// create root folder
//				$tmpFilePath = $eclassAppConfig['urlFilePath'].'schoolImage/';
//				if (!file_exists($tmpFilePath)) {
//					$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
//				}
//
//				// backup old file first
//				$sql = "Select RecordID, ImagePath From APP_SCHOOL_IMAGE Where AppType = '".$appType."' And ImageType = '".$imageType."'";
//				$dbImageInfoAry = $this->returnResultSet($sql);
//				$recordId = $dbImageInfoAry[0]['RecordID'];
//				$oldImagePath = $dbImageInfoAry[0]['ImagePath'];
//				$oldFilePath = $eclassAppConfig['urlFilePath'].$oldImagePath;
//				if ($recordId > 0 && file_exists($oldFilePath)) {
//					$successAry['backupOldFile'] = $lfs->file_rename($oldFilePath, $oldFilePath.'.'.date('Ymd_His'));
//				}
//
//				// upload new files
//				$orgFileName = $parImageInfoAry['name'];
//				$tmpName = $parImageInfoAry['tmp_name'];
//				$fileExt = get_file_ext($orgFileName);
//				$targetFileName = $appType.'_'.$imageType.$fileExt;
//				$targetFilePath = $tmpFilePath.$targetFileName;
//				$successAry['uploadFile'] = $lfs->file_copy($tmpName, $targetFilePath);
//				if ($successAry['uploadFile']) {
//					$dbFilePath = str_replace($eclassAppConfig['urlFilePath'], '', $targetFilePath);
//					if ($recordId > 0) {
//						// update
//						$sql = "Update APP_SCHOOL_IMAGE Set ImagePath = '".$this->Get_Safe_Sql_Query($dbFilePath)."', RecordStatus = 1, ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RecordID = '".$recordId."'";
//						$successAry['updateDbRecord'] = $this->db_db_query($sql);
//					}
//					else {
//						// insert
//						$sql = "Insert Into APP_SCHOOL_IMAGE
//									(AppType, ImageType, ImagePath, RecordStatus, InputDate, InputBy, ModifiedDate, ModifiedBy)
//								Values
//									('".$appType."', '".$imageType."', '".$dbFilePath."', 1, now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
//								";
//						$successAry['insertDbRecord'] = $this->db_db_query($sql);
//					}
//				}
//			}
//
//			return !in_array(false, (array)$successAry);
//		}

// 		function saveSchInfoCustomIcon($id,$parImageInfoAry) {
// 			global $eclassAppConfig, $PATH_WRT_ROOT;

// 			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
// 			$lfs = new libfilesystem();

// 			$successAry = array();
// 			if ($parImageInfoAry['name'] != ''  && $parImageInfoAry['error'] > 0) {
// 				$successAry['imageDataValid'] = false;
// 			}
// 			else if ($parImageInfoAry['name'] == '') {
// 				$successAry['emptyImage'] = true;
// 			}
// 			else {
// 				// create root folder
// 				$subfolder = ceil($id/30000);//1-30000 insert into 1,then 30001-60000 into 2...
// 				$tmpFilePath = $eclassAppConfig['urlFilePath'].'schInfoCustomIcon/'.$subfolder.'/';
// 				if (!file_exists($tmpFilePath)) {
// 					$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
// 					chmod($tmpFilePath, 0777);
// 				}
// 				// upload new files
// 				$orgFileName = $parImageInfoAry['name'];
// 				$tmpName = $parImageInfoAry['tmp_name'];
// 				$fileExt = get_file_ext($orgFileName);
// 				$targetFileName = $id.$fileExt;
// 				$targetFilePath = $tmpFilePath.$targetFileName;
// 				$successAry['uploadFile'] = $lfs->file_copy($tmpName, $targetFilePath);
// 				if ($successAry['uploadFile']) {
// 					$dbFilePath = str_replace($eclassAppConfig['urlFilePath'], '', $targetFilePath);

// 						// update
// 						$sql = "Update SCHOOL_INFO Set Icon = '".$this->Get_Safe_Sql_Query($dbFilePath)."', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' Where MenuID = '".$id."'";
// 						$successAry['updateDbRecord'] = $this->db_db_query($sql);

// 				}
// 			}

// 			return !in_array(false, (array)$successAry);
// 		}

		function deleteSchoolImage($appType, $imageType) {
			global $eclassAppConfig, $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$lfs = new libfilesystem();
			$liblog = new liblog();

			$successAry = array();

			### get image data
			$imageInfoAry = $this->getSchoolImage($appType, $imageType);
			$recordId = $imageInfoAry[0]['RecordID'];
			$imagePath = $eclassAppConfig['urlFilePath'].$imageInfoAry[0]['ImagePath'];

			### backup file
			$targetImagePath = $imagePath.'.'.date('Ymd_His');
			$successAry['backupOldFile'] = $lfs->file_rename($imagePath, $targetImagePath);

			### update db record
			$sql = "Update APP_SCHOOL_IMAGE Set RecordStatus = '".$eclassAppConfig['appImage']['status']['deleted']."', ModifiedDate = now(), ModifiedBy = '".$_SESSION['UserID']."' Where AppType = '".$appType."' And ImageType = '".$imageType."'";
			$successAry['changeRecordStatus'] = $this->db_db_query($sql);

			### add delete log
			$tmpAry = array();
		    $tmpAry['appType'] = $appType;
		    $tmpAry['imageType'] = $imageType;
		    $successAry['Log_Delete'] = $liblog->INSERT_LOG($this->ModuleTitle, 'deleteImage', $liblog->BUILD_DETAIL($tmpAry), 'APP_SCHOOL_IMAGE', $recordId);

			return $successAry;
		}

// 		function deleteSchInfoCustomIcon($id) {
// 			global $eclassAppConfig, $PATH_WRT_ROOT;

// 			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
// 			$lfs = new libfilesystem();

// 			$successAry = array();

// 			### get image data
// 			$sql = "Select Icon from SCHOOL_INFO Where MenuID = '".$id."'";
// 			$result = $this->returnVector($sql);

// 			$imagePath = $eclassAppConfig['urlFilePath'].$result[0];

// 			### delete image
// 			unlink($imagePath);

// 			### update db record
// 			$sql = "Update SCHOOL_INFO Set Icon = '', IconType = '2', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' Where MenuID = '".$id."'";
// 			$successAry['changeRecordStatus'] = $this->db_db_query($sql);

// 			return $successAry;
// 		}

		function getParentAppLoginUserSql($targetStatus, $filterFormClassId, $keyword, $outputMode='', $pushMessageStatus='', $isTeachingClassOnly = 0, $hideLeftStudent = false) {
			$loggedInUserIdAry = Get_Array_By_Key($this->getAppLoggedInUser($excludeDeviceChecking=true), 'UserID');

			// apply login status filtering
			if ($targetStatus == 'loggedIn') {
				$conds_parentUserId = " And parent.UserID IN ('".implode("','", (array)$loggedInUserIdAry)."') ";

				$lastLoginTimeField = " , al.LastLoginTime as LastLoginTime ";
				$joinLoginLogTbl = " Inner Join APP_LOGIN_LOG as al ON (parent.UserID = al.UserID) ";
			}
			else if ($targetStatus == 'notLoggedIn') {
				$conds_parentUserId = " And parent.UserID NOT IN ('".implode("','", (array)$loggedInUserIdAry)."') ";
			}

			// apply class filtering
			if ($filterFormClassId != '' && $filterFormClassId != "0") {
				$targetYearClassIdAry = array();
			    if (is_numeric($filterFormClassId)) {
			    	// selected whole Form => get all Classes of the Form
			    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID = '$filterFormClassId' And AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
			    	if($isTeachingClassOnly){
			    		$sql .= " AND YearClassID IN ('" . implode("','", $_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher']) . "') ";
			    	}
			    	$targetYearClassIdAry = $this->returnVector($sql);
				} else if($isTeachingClassOnly && is_array($filterFormClassId)) {
					$targetYearClassIdAry = $filterFormClassId;
				}
			    else {
			    	// selected one class only
			    	$targetYearClassIdAry[] = substr($filterFormClassId, 2);
			    }

			    // get the parent who has children studying in selected class
			    $sql = "Select
								ip.ParentID
						From
								INTRANET_PARENTRELATION as ip
								Inner Join YEAR_CLASS_USER as ycu ON (ip.StudentID = ycu.UserID)
						Where
								ycu.YearClassID IN ('".implode("','", (array)$targetYearClassIdAry)."')
						";
				$conds_parentUserId2 = " AND parent.UserID IN ('".implode("','", $this->returnVector($sql))."') ";
			}

			// apply push message status filtering
			if ($pushMessageStatus != '') {
				$acceptPushMessageUserIdAry = Get_Array_By_Key($this->getAppLoggedInUser($excludeDeviceChecking=false), 'UserID');

				if ($pushMessageStatus == 1) {
					// show accept push message users only
					$pushMessageStatusUserIdAry = $acceptPushMessageUserIdAry;
				}
				else if ($pushMessageStatus == 2) {
					// show not accept push message users only
					$pushMessageStatusUserIdAry = array_values(array_diff($loggedInUserIdAry, $acceptPushMessageUserIdAry));
				}

				$conds_pushMessageStatusUserId = "  And parent.UserID IN ('".implode("','", (array)$pushMessageStatusUserIdAry)."') ";
			}

			$parentNameField = getNameFieldByLang('parent.');
			$studentNameField = getNameFieldWithClassNumberByLang('student.');

			if ($keyword !== '' && $keyword !== null) {
				$conds_keyword = " And (
										$parentNameField Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or $studentNameField Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or student.ClassName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or parent.UserLogin Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									  ) 
								";
			}

			if ($outputMode == 'csv') {
				$studentSeparator = ', ';
			}
			else {
				$studentSeparator = ',<br>';
			}
			
			if($hideLeftStudent){
				$cond_studentType = " AND student.RecordStatus <> '3' ";
			}

			$sql = "Select
							parent.UserLogin as UserLogin,
							$parentNameField as ParentName,
							GROUP_CONCAT(DISTINCT $studentNameField ORDER BY student.ClassName, student.ClassNumber SEPARATOR '".$studentSeparator."') as StudentName
							$lastLoginTimeField,
							GROUP_CONCAT(DISTINCT student.UserID ORDER BY student.ClassName, student.ClassNumber SEPARATOR '".$studentSeparator."') as StudentUserIdList
					From
							INTRANET_USER as parent
							Inner Join INTRANET_PARENTRELATION as ipr ON (parent.UserID = ipr.ParentID)
							Inner Join INTRANET_USER as student ON (ipr.StudentID = student.UserID)
							$joinLoginLogTbl
					Where
							parent.RecordType = '".USERTYPE_PARENT."'
							And parent.RecordStatus = 1
							$conds_parentUserId
							$conds_parentUserId2
							$conds_keyword
							$cond_studentType
							$conds_pushMessageStatusUserId
					Group By
							parent.UserID
					";
			return $sql;
		}

		function getTeacherAppLoginUserSql($targetStatus, $keyword, $outputMode='', $pushMessageStatus='') {
			global $intranet_root, $sys_custom, $Lang;

			if($sys_custom['DHL']){
				include_once($intranet_root."/includes/DHL/libdhl.php");
				$libdhl = new libdhl();
				//$organization_field = ",".$libdhl->formatDBOriganizationDisplayName("c.", "v.", "d.",$_REQUEST['outputMode']=="csv")." as Department ";
				$organization_field = ",".$libdhl->formatDBCompanyDisplayName("c.",$_REQUEST['outputMode']=="csv")." as Company ";
				$organization_field.= ",".$libdhl->formatDBDivisionDisplayName("v.",$_REQUEST['outputMode']=="csv")." as Division ";
				$organization_field.= ",".$libdhl->formatDBDepartmentDisplayName("d.",$_REQUEST['outputMode']=="csv")." as Department ";
				$left_join_dhl_tables = "LEFT JOIN DHL_DEPARTMENT_USER as du ON du.UserID=iu.UserID 
										LEFT JOIN DHL_DEPARTMENT as d ON d.DepartmentID=du.DepartmentID 
										LEFT JOIN DHL_DIVISION as v ON v.DivisionID=d.DivisionID 
										LEFT JOIN DHL_COMPANY_DIVISION as cd ON cd.DivisionID=v.DivisionID 
										LEFT JOIN DHL_COMPANY as c ON c.CompanyID=cd.CompanyID ";
			}

			$loggedInUserIdAry = Get_Array_By_Key($this->getAppLoggedInUser($excludeDeviceChecking=true), 'UserID');

			// apply login status filtering
			if ($targetStatus == 'loggedIn') {
				$conds_teacherUserId = " And iu.UserID IN ('".implode("','", (array)$loggedInUserIdAry)."') ";

				$lastLoginTimeField = " , al.LastLoginTime as LastLoginTime ";
				$joinLoginLogTbl = " Inner Join APP_LOGIN_LOG as al ON (iu.UserID = al.UserID) ";
			}
			else if ($targetStatus == 'notLoggedIn') {
				$conds_teacherUserId = " And iu.UserID NOT IN ('".implode("','", (array)$loggedInUserIdAry)."') ";
			}

			// apply push message status filtering
			if ($pushMessageStatus != '') {
				$acceptPushMessageUserIdAry = Get_Array_By_Key($this->getAppLoggedInUser($excludeDeviceChecking=false), 'UserID');

				if ($pushMessageStatus == 1) {
					// show accept push message users only
					$pushMessageStatusUserIdAry = $acceptPushMessageUserIdAry;
				}
				else if ($pushMessageStatus == 2) {
					// show not accept push message users only
					$pushMessageStatusUserIdAry = array_values(array_diff($loggedInUserIdAry, $acceptPushMessageUserIdAry));
				}

				$conds_pushMessageStatusUserId = "  And iu.UserID IN ('".implode("','", (array)$pushMessageStatusUserIdAry)."') ";
			}

			$teacherNameField = getNameFieldByLang('iu.');

			if ($keyword !== '' && $keyword !== null) {
				$conds_keyword = " And (
										$teacherNameField Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or iu.UserLogin Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									  ) 
								";
			}

			$sql = "Select
							iu.UserLogin as UserLogin,
							$teacherNameField as TeacherName 
							$organization_field 
							$lastLoginTimeField
					From
							INTRANET_USER as iu
							$joinLoginLogTbl 
							$left_join_dhl_tables 
					Where
							iu.RecordType = '".USERTYPE_STAFF."'
							And iu.RecordStatus = 1
							$conds_teacherUserId
							$conds_pushMessageStatusUserId
							$conds_keyword
					Group By
							iu.UserID
					";
			return $sql;
		}

		function getStudentAppLoginUserSql($targetStatus, $filterFormClassId, $keyword, $outputMode='', $pushMessageStatus='') {
			global $Lang;

			$loggedInUserIdAry = Get_Array_By_Key($this->getAppLoggedInUser($excludeDeviceChecking=true), 'UserID');

			// apply login status filtering
			if ($targetStatus == 'loggedIn') {
				$conds_studentUserId = " And iu.UserID IN ('".implode("','", (array)$loggedInUserIdAry)."') ";

				$lastLoginTimeField = " , al.LastLoginTime as LastLoginTime ";
				$joinLoginLogTbl = " Inner Join APP_LOGIN_LOG as al ON (iu.UserID = al.UserID) ";
			}
			else if ($targetStatus == 'notLoggedIn') {
				$conds_studentUserId = " And iu.UserID NOT IN ('".implode("','", (array)$loggedInUserIdAry)."') ";
			}

			// apply class filtering
			if ($filterFormClassId != '' && $filterFormClassId != "0") {
				$targetYearClassIdAry = array();
			    if (is_numeric($filterFormClassId)) {
			    	// selected whole Form => get all Classes of the Form
			    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID = '$filterFormClassId' And AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
			    	$targetYearClassIdAry = $this->returnVector($sql);
				}
			    else {
			    	// selected one class only
			    	$targetYearClassIdAry[] = substr($filterFormClassId, 2);
			    }

			    // get the students studying in selected class
			    $sql = "Select
								ycu.UserID
						From
								YEAR_CLASS_USER as ycu
						Where
								ycu.YearClassID IN ('".implode("','", (array)$targetYearClassIdAry)."')
						";
				$conds_studentUserId2 = " AND iu.UserID IN ('".implode("','", $this->returnVector($sql))."') ";
			}

			// apply push message status filtering
			if ($pushMessageStatus != '') {
				$acceptPushMessageUserIdAry = Get_Array_By_Key($this->getAppLoggedInUser($excludeDeviceChecking=false), 'UserID');

				if ($pushMessageStatus == 1) {
					// show accept push message users only
					$pushMessageStatusUserIdAry = $acceptPushMessageUserIdAry;
				}
				else if ($pushMessageStatus == 2) {
					// show not accept push message users only
					$pushMessageStatusUserIdAry = array_values(array_diff($loggedInUserIdAry, $acceptPushMessageUserIdAry));
				}

				$conds_pushMessageStatusUserId = "  And iu.UserID IN ('".implode("','", (array)$pushMessageStatusUserIdAry)."') ";
			}

			$studentNameField = getNameFieldByLang('iu.');

			if ($keyword !== '' && $keyword !== null) {
				$conds_keyword = " And (
										$studentNameField Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or iu.ClassName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or iu.ClassNumber Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									  ) 
								";
			}

			$sql = "Select
							If (iu.ClassName is null Or iu.ClassName = '', '".$Lang['General']['EmptySymbol']."', iu.ClassName) as ClassName,
							If (iu.ClassNumber is null Or iu.ClassNumber = '', '".$Lang['General']['EmptySymbol']."', iu.ClassNumber) as ClassNumber,
							$studentNameField as StudentName
							$lastLoginTimeField
					From
							INTRANET_USER as iu
							Inner Join YEAR_CLASS_USER as ycu on (iu.UserID = ycu.UserID)
							Inner Join YEAR_CLASS as yc on (ycu.YearClassID = yc.YearClassID)
							Inner Join YEAR as y ON (yc.YearID = y.YearID)
							$joinLoginLogTbl
					Where
							iu.RecordType = '".USERTYPE_STUDENT."'
							And iu.RecordStatus = 1
							And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							$conds_studentUserId
							$conds_studentUserId2
							$conds_pushMessageStatusUserId
							$conds_keyword
					Group By
							iu.UserID
					";
			return $sql;
		}

		function getAppLoggedInUser($excludeDeviceChecking=false) {
			global $sys_custom, $eclassAppConfig;

			if ($excludeDeviceChecking) {

			}
			else {
				$innerJoin_APP_USER_DEVICE = " Inner Join APP_USER_DEVICE as d ON (l.UserID = d.UserID) ";
				$conds_deviceRecordStatus = " And d.RecordStatus = 1 ";

				if ($sys_custom['eClassApp']['AndroidPushServiceProvider'] == "getui") {
					$excludeDeviceOs = $eclassAppConfig['deviceOS']['Android'];
				}
				else {
					$excludeDeviceOs = $eclassAppConfig['deviceOS']['Android_CN'];
				}
				$conds_excludeDeviceOs = " And d.DeviceOS NOT IN ('".implode("','", (array)$excludeDeviceOs)."') ";
			}
			$sql = "Select 
							l.UserID, l.LastLoginTime 
					From 
							APP_LOGIN_LOG as l
							$innerJoin_APP_USER_DEVICE
					Where
							1
							$conds_deviceRecordStatus
							$conds_excludeDeviceOs
					";
			return $this->returnResultSet($sql);
		}

		function getDemoSiteUserLogin($parUserLogin) {
			global $sys_custom;

			$targetUserLogin = $parUserLogin;
			if ($sys_custom['demo_site_account_mapping'] && $parUserLogin != '')
		    {
		    	$Map2DemoAccount = false;
		    	if (strlen($parUserLogin)==strpos($parUserLogin, "_t")+2) {
		    		$Map2DemoAccount = true;
		    		$targetUserLogin = "t";
		    	} elseif (strlen($parUserLogin)==strpos($parUserLogin, "_s")+2) {
		    		$Map2DemoAccount = true;
		    		$targetUserLogin = "s";
		    	} elseif (strlen($parUserLogin)==strpos($parUserLogin, "_p")+2) {
		    		$Map2DemoAccount = true;
		    		$targetUserLogin = "p";
		    	}
		    }

		    return $targetUserLogin;
		}

		function getDemoSiteUserId($parUserId) {
			global $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			$userObj = new libuser($parUserId);
			$userLogin = $userObj->UserLogin;

			$targetUserLogin = $this->getDemoSiteUserLogin($userLogin);
			$targetUserId = $parUserId;
			if ($userLogin != $targetUserLogin) {
				$targetUserObj = new libuser("", $targetUserLogin);
				if ($targetUserObj->UserID > 0) {
					$targetUserId = $targetUserObj->UserID;
				}
			}

			return $targetUserId;
		}

		function getStaffAttendanceTodayRecord($parUserId) {
			global $PATH_WRT_ROOT, $Lang;

			include_once($PATH_WRT_ROOT.'includes/libstaffattend3.php');
			$lstaffattend3 = new libstaffattend3();

			$logAry = $lstaffattend3->Get_Daily_Log_Entry_Info(date('Y'), date('m'), $parUserId, $StartDate=date('Y-m-d'), $EndDate=date('Y-m-d'));
			$numOfLog = count($logAry);

			$targetInTime = '';
			$targetOutTime = '';
			for ($i=0; $i<$numOfLog; $i++) {
				$_inTime = $logAry[$i]['InTime'];
				$_outTime = $logAry[$i]['OutTime'];

				if ($targetInTime=='' || ($_inTime != '' && $_inTime < $targetInTime)) {
					$targetInTime = $_inTime;
				}
				if ($targetOutTime=='' || ($_outTime != '' && $_outTime > $targetOutTime)) {
					$targetOutTime = $_outTime;
				}
			}
			$targetInTime = ($targetInTime=='')? $Lang['General']['EmptySymbol'] : substr($targetInTime, 0, 5);
			$targetOutTime = ($targetOutTime=='')? $Lang['General']['EmptySymbol'] : substr($targetOutTime, 0, 5);

			$returnAry = array();
			$returnAry['InTime'] = $targetInTime;
			$returnAry['OutTime'] = $targetOutTime;

			return $returnAry;
		}

		function getCurCentralServerUrl($parFunction='') {
			global $eclassAppConfig;

			if ($parFunction == '') {
				$parFunction = 'pushMessage';
			}

			$sql = "Select 
							ServerUrl 
					From 
							INTRANET_APP_CENTRAL_SERVER_SETTINGS
					Where
							EffectiveDateTime <= now()
							AND Functionality = '".$this->Get_Safe_Sql_Query($parFunction)."'
					Order By
							EffectiveDateTime desc
					Limit 1
					";
			$curServerInfoAry = $this->returnResultSet($sql);
			$dbCurCentralServerUrl = $curServerInfoAry[0]['ServerUrl'];

			$curCentralServerUrl = '';
			if ($dbCurCentralServerUrl == '') {
				// no value in DB => use config
				$curCentralServerUrl = $eclassAppConfig[$parFunction]['apiPath'];
			}
			else {
				// have value in DB => use DB value
				$curCentralServerUrl = $dbCurCentralServerUrl;
			}

			return $curCentralServerUrl;
		}

		function addCentralServerSettings($serverUrl, $effectiveDateTime, $fromServer, $functionality='') {
			if ($functionality == '') {
				$functionality = 'pushMessage';
			}

			$sql = "INSERT INTO INTRANET_APP_CENTRAL_SERVER_SETTINGS
						(ServerUrl, EffectiveDateTime, FromServer, DateInput, Functionality)
					VALUES
						('".$this->Get_Safe_Sql_Query($serverUrl)."', '".$effectiveDateTime."', '".$this->Get_Safe_Sql_Query($fromServer)."', now(), '".$functionality."')
					";
			return $this->db_db_query($sql);
		}

		function getAppSettingsObj() {
			global $PATH_WRT_ROOT;

			include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp_settings.php');
			if ($this->SettingsObj == null) {
				$this->SettingsObj = new libeClassApp_settings();
				$this->SettingsObj->loadSettings();
			}

			return $this->SettingsObj;
		}

		function getAppWebPageInitStart(){
			global $PATH_WRT_ROOT;

			$appWebPageInitStart ="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>" .
					                "<script src=\"".$PATH_WRT_ROOT."templates/jquery/jquery-1.11.1.min.js\"></script>" .
					                "<script src=\"".$PATH_WRT_ROOT."templates/jquery.mobile/jquery.mobile.min.js\"></script>" .
					                "<link rel=\"stylesheet\" href=\"".$PATH_WRT_ROOT."templates/jquery.mobile/jquery.mobile.min.css\">" .
					                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" .
					                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />";
			return $appWebPageInitStart;
		}

		function getAppWebPageInitEnd(){
			$appWebPageInitEnd ="</div></div></body></html>";
			return $appWebPageInitEnd;
		}
		function getAllIntranetAppTeacherStatusUser(){
			$sql = "SELECT UserID FROM INTRANET_APP_TEACHER_STATUS_USER";
			return $this->returnVector($sql);
		}

		function addIntranetAppTeacherStatusUsers($addedUerID) {
		 	$sql = "INSERT INTO INTRANET_APP_TEACHER_STATUS_USER
				(UserID, DateInput, InputBy, DateModified,ModifiedBy)
			VALUES
				('".$addedUerID."',now(),'".$_SESSION['UserID']."',now(),'".$_SESSION['UserID']."')";
			return $this->db_db_query($sql);
		}

		function deleteIntranetAppTeacherStatusRemovedUsers($removedUsers) {
		 	$sql = "Delete From INTRANET_APP_TEACHER_STATUS_USER Where UserID in (".$removedUsers.")";
			return $this->db_db_query($sql);
		}

		function getAllIntranetAppTeacherStatusUserName($uidAryString,$_displayNameLang){
			$sql = "SELECT
			IFNULL(IF(".$_displayNameLang."='', '---', ".$_displayNameLang."),'---') as ".$_displayNameLang.",UserID
		FROM 
		    INTRANET_USER
		WHERE
			RecordType = 1 AND RecordStatus=1 AND UserID in (".$uidAryString.") order by $_displayNameLang";
			return $this->returnResultSet($sql);
		}

		function getPushMessageAcceptStatusSelection($parId, $parName, $parValue, $parOnChange) {
			global $Lang;

			$selectAry = array();
			$selectAry[''] = $Lang['eClassApp']['AllStatus'];
			$selectAry[1] = $Lang['eClassApp']['AcceptPushMessage'];
			$selectAry[2] = $Lang['eClassApp']['NotAcceptPushMessage'];

			$onchange = '';
			if ($parOnChange != "")
				$onchange = 'onchange="'.$parOnChange.'"';

			$selectionTags = ' id="'.$parId.'" name="'.$parName.'" '.$onchange;
			return getSelectByAssoArray($selectAry, $selectionTags, $parValue, $all=0, $noFirst=1);
		}

		function getParentAppAdvancedSettingsTagObj($curPage) {
			global $Lang, $eclassAppConfig;

			$appType = $eclassAppConfig['appType']['Parent'];
			$moduleAry = $this->getAppApplicableModule($appType);

			$tagAry = array();
			$tagAry[] = array($Lang['eClassApp']['AdvancedSettingList']['PushMessageRight'], '?task=parentApp/advanced_setting/push_message_right/list', ($curPage==$eclassAppConfig['moduleCode']['pushMessage']));

			if (in_array($eclassAppConfig['moduleCode']['eAttendance'], $moduleAry)) {
				$tagAry[] = array($Lang['eClassApp']['ModuleNameAry']['eAttendance'], '?task=parentApp/advanced_setting/attendance/list', ($curPage==$eclassAppConfig['moduleCode']['eAttendance']));
			}

			return $tagAry;
		}

        function getUserInfo($userIdAry, $withTitle = false) {
            global $intranet_session_language;
            if($withTitle) {
                $old_intranet_session_language = $intranet_session_language;
                $intranet_session_language = 'en';
                $en_name_field = getNameFieldByLang('a.');
                $intranet_session_language = 'b5';
                $chi_name_field = getNameFieldByLang('a.');
                $intranet_session_language = $old_intranet_session_language;
            } else {
                $en_name_field = 'EnglishName';
                $chi_name_field = 'ChineseName';
            }

            $sql = "Select UserID, $en_name_field as NameEn, $chi_name_field as NameCh, RecordType, RecordStatus, IF (Teaching is null OR Teaching = 0, 0, Teaching) as isTeaching From INTRANET_USER a Where UserID IN ('".implode("','", (array)$userIdAry)."')";
			$userAry = $this->returnResultSet($sql);
			$numOfUser = count($userAry);

			for ($i=0; $i<$numOfUser; $i++) {
				$_recordType = $userAry[$i]['RecordType'];
				$_isTeaching = $userAry[$i]['isTeaching'];

				$_userType = '';
				switch ($_recordType) {
					case USERTYPE_STAFF:
						$_userType = ($_isTeaching==1)? 'T' : 'NT';
						break;
					case USERTYPE_STUDENT:
						$_userType = 'S';
						break;
					case USERTYPE_PARENT:
						$_userType = 'P';
						break;
					case USERTYPE_ALUMNI:
						$_userType = 'A';
						break;
					default:
						$_userType = '';
						break;
				}
				$userAry[$i]['RecordType'] = $_userType;
			}

			return $userAry;
		}

	   function getSelectionHtml($selectAry,$selectionLabel,$selectedID,$selectionLabelStyle,$onChangeHandler=''){
	   	    $onChangeHandler = $onChangeHandler == ''?'updateTerms(this);':$onChangeHandler;

           	$terms_selection_body = '<div class="ui-field-contain">';
           	 if($selectionLabel != ''){
           	 $terms_selection_body .= '<label for="select-items" style="margin-right: '.$selectionLabelStyle.' margin-left: 45px;">'.$selectionLabel.'</label>';
           	 }

             $terms_selection_body .= '<select name="select-items" id="select-items" onchange="'.$onChangeHandler.'">';
			 foreach ($selectAry as $key => $value){
								$isSelected = '';
								if($key==$selectedID){
								$isSelected = 'selected="selected"';
								}
								$terms_selection_body .= '<option value="'.$key.'" '.$isSelected.'>'.$value.'</option>';
							}
		    $terms_selection_body .= '</select></div>';
        return 	$terms_selection_body;
        }

//         function updateSchoolInfoFlashUploadPath($MenuID,$Content,$action='new'){
//         	global $cfg, $PATH_WRT_ROOT;

//         	include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
//         	$lfs = new libfilesystem();

//         	$Content = stripslashes(htmlspecialchars_decode($Content));
//         	if($action=='copy'){
//         		$Content = $lfs->copy_fck_flash_image_upload_to_new_id_loc($MenuID, $MenuID, $Content, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolInfo']);
//         	}else{
//         		$Content = ($lfs->copy_fck_flash_image_upload($MenuID, $Content, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolInfo']));
//         	}
//         	$Content = trim(intranet_htmlspecialchars($Content));
//         	$sql = "UPDATE SCHOOL_INFO SET Description = '".$this->Get_Safe_Sql_Query($Content)."' WHERE MenuID = '$MenuID'";
//         	$this->db_db_query($sql);
//         }

//         function deleteSchoolInfo($MenuIDAry=array())
//         {


//         	for($i=0, $i_max=sizeof($MenuIDAry); $i<$i_max; $i++) {
//         		$menuID = $MenuIDAry[$i];

//         		if($menuID=="") continue;

//         		$sql = "SELECT IsItem FROM SCHOOL_INFO WHERE MenuID='".$menuID."'";
//         		$result = $this->returnVector($sql);

//         		if(sizeof($result)>0) {
//         			if($result[0]!=0) {
//         				$result[] = $this->deleteSchInfoItem($menuID);

//         			} else {
//         				$result[] = $this->deleteSchInfoMenu($menuID);
//         			}
//         		}
//         	}
//         	return $result;
//         }

//         function deleteSchInfoItem($menuID)
//         {
//         	global $UserID;

//             $sql = "UPDATE SCHOOL_INFO SET RecordStatus=0, DateModified=NOW(), ModifyBy='".$UserID."' WHERE MenuID='".$menuID."'";

//         	$this->db_db_query($sql);

//         	return $result;
//         }

//         function deleteSchInfoMenu($menuID)
//         {
//         	global $UserID;

//         	$sql = "SELECT MenuID FROM SCHOOL_INFO WHERE ParentMenuID='$menuID'";
//         	$childMenu = $this->returnVector($sql);

//         	if (sizeof($childMenu)>0)
//         	{
//         		$this->deleteSchoolInfo($childMenu);
//         	}

//         	$sql = "UPDATE SCHOOL_INFO SET RecordStatus = 0, DateModified=NOW(), ModifyBy='".$UserID."' WHERE MenuID='".$menuID."'";
//         	$result = $this->db_db_query($sql);

//         	return $result;
//         }

        function getCenterHeader($title){
        $header = "<div data-role='header'  data-position='fixed' style ='background-color:#429DEA;'>
          <h1 style ='text-align: center;width=100%;font-size:20px;color:white;text-shadow: none!important;margin-left:0px;margin-right:0px'>".$title
          ."</h1></div>";
        	return $header;
        }
        function getTeacherAppAdvancedSettingsTagObj($curPage) {
			global $Lang, $eclassAppConfig;

			$appType = $eclassAppConfig['appType']['Teacher'];
			$moduleAry = $this->getAppApplicableModule($appType);

			$tagAry = array();

			$tagAry[] = array($Lang['eClassApp']['AdvancedSettingList']['PushMessageRight'], '?task=teacherApp/advanced_setting/push_message_right/list&appType='.$appType, ($curPage==$eclassAppConfig['moduleCode']['pushMessage']));

			if (in_array($eclassAppConfig['moduleCode']['teacherStatus'], $moduleAry)) {
			    $tagAry[] = array($Lang['eClassApp']['ModuleNameAry']['TeacherStatus'], '?task=teacherApp/advanced_setting/teacher_status_right/list&appType='.$appType, ($curPage==$eclassAppConfig['moduleCode']['teacherStatus']));
			}
			if (in_array($eclassAppConfig['moduleCode']['studentStatus'], $moduleAry)) {
				$tagAry[] = array($Lang['eClassApp']['ModuleNameAry']['StudentStatus'], '?task=teacherApp/advanced_setting/student_list_right/list&appType='.$appType, ($curPage==$eclassAppConfig['moduleCode']['studentStatus']));
			}

			return $tagAry;
		}

        function getBlacklistUser($parRecordStatus='', $parUserIdAry='', $parUserTypeAry='') {
        	global $eclassAppConfig;

        	if ($parRecordStatus !== '') {
        		$conds_recordStatus = " AND bu.RecordStatus = '".$parRecordStatus."' ";
        	}

        	if ($parUserIdAry !== '') {
        		$conds_userId = " AND bu.UserID IN ('".implode("','", (array)$parUserIdAry)."') ";
        	}

        	if ($parUserTypeAry !== '') {
        		$conds_userType = " AND iu.RecordType IN ('".implode("','", (array)$parUserTypeAry)."') ";
        		$innerJoin_INTRANET_USER = ' INNER JOIN INTRANET_USER as iu ON (bu.UserID = iu.UserID) ';
        	}

        	$sql = "SELECT 
							bu.RecordID, bu.UserID, bu.RecordStatus 
					FROM 
							INTRANET_APP_BLACKLIST_USER as bu
							$innerJoin_INTRANET_USER
					WHERE 
							1 
							$conds_recordStatus 
							$conds_userId
							$conds_userType
					";
        	return $this->returnResultSet($sql);
        }

        function isBlacklistedUser($parUserLogin) {
        	global $eclassAppConfig;

        	$sql = "SELECT
							blu.UserID
					FROM
							INTRANET_APP_BLACKLIST_USER as blu 
							INNER JOIN INTRANET_USER as iu ON (iu.UserID = blu.UserID)
					WHERE
							iu.UserLogin = '".$this->Get_Safe_Sql_Query($parUserLogin)."'
							AND blu.RecordStatus = '".$eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['active']."'
					";
			return (count($this->returnResultSet($sql)) > 0)? true : false;
        }

        function addBlacklistUser($parUserIdAry) {
        	global $eclassAppConfig;

        	$blacklistUserIdAry = BuildMultiKeyAssoc($this->getBlacklistUser(), 'UserID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);

        	$successAry = array();
        	$insertAry = array();
        	$numOfUser = count((array)$parUserIdAry);
        	for ($i=0; $i<$numOfUser; $i++) {
        		$_userId = $parUserIdAry[$i];

        		if (!isset($blacklistUserIdAry[$_userId])) {
        			$insertAry[] = " ('".$_userId."', '".$eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['active']."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
        		}
        		else {
        			$_recordId = $blacklistUserIdAry[$_userId]['RecordID'];
        			$_recordStatus = $blacklistUserIdAry[$_userId]['RecordStatus'];
        			if ($_recordStatus == $eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['deleted']) {
        				$sql = "UPDATE INTRANET_APP_BLACKLIST_USER SET RecordStatus = '".$eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['active']."', ModifiedDate = now() WHERE RecordID = '".$_recordId."'";
        				$successAry['updateRecordStatus'][$_recordId] = $this->db_db_query($sql);
        			}
        		}
        	}

        	if (count($insertAry) > 0) {
        		$sql = "INSERT INTO INTRANET_APP_BLACKLIST_USER (UserID, RecordStatus, InputDate, InputBy, ModifiedDate, ModifyBy) VALUES ".implode(',', (array)$insertAry);
        		$successAry['insertRecord'] = $this->db_db_query($sql);
        	}

        	return !in_array(false, (array)$successAry);
        }

        function deleteBlacklistUser($parUserIdAry) {
        	global $eclassAppConfig;

        	$sql = "UPDATE INTRANET_APP_BLACKLIST_USER SET RecordStatus = '".$eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['deleted']."', ModifiedDate = now() WHERE UserID IN ('".implode("','", (array)$parUserIdAry)."')";
        	return $this->db_db_query($sql);
        }

       function getIntranetAppTeacherListEnableGroup($returnAryType =''){
        	if($returnAryType != ''){
        		$sql = "select ".$returnAryType." from INTRANET_APP_TEACHER_LIST_ENABLE_GROUP GROUP BY ".$returnAryType." order by ".$returnAryType;
        		return $this->returnVector($sql);
        	}else {
        	    $sql = "select g.GroupID as ID,g.Title,g.TitleChinese from INTRANET_GROUP as g INNER JOIN INTRANET_APP_TEACHER_LIST_ENABLE_GROUP as eg ON g.GroupID=eg.GroupID ORDER BY g.Title";
                return $this->returnResultSet($sql);
        	}
        }

        function getAllIntranetAppTeacherListGroup(){
        $TermInfoArr = getCurrentAcademicYearAndYearTerm();
        $curYear = $TermInfoArr['AcademicYearID'];
	    $sql = "SELECT DISTINCT u.UserID 
						FROM INTRANET_USER as u 
						WHERE u.RecordType = ".USERTYPE_STAFF." AND u.RecordStatus = 1 ";
		$returnUserIDArray =  $this->returnVector($sql);
		$available_users_id_str = sizeof($returnUserIDArray)>0 ? implode(",",$returnUserIDArray):"";
	    $sql = "select g.Title,g.TitleChinese, g.GroupID as ID from INTRANET_GROUP as g INNER JOIN INTRANET_USERGROUP as ug ON ug.GroupID=g.GroupID WHERE ug.UserID IN ($available_users_id_str) AND g.AcademicYearID=".$curYear." GROUP BY g.GroupID ORDER BY g.Title";
        return $this->returnResultSet($sql);
        }

       function getSelectionBox($row,$totalSelectionAry,$nameLang,$nameBackupLang,$idTag){
       	global $PATH_WRT_ROOT,$Lang;
        $chosenIDAry = array();
          $x = '';
          $x .= '<table width=100% border=0 cellpadding=5 cellspacing=0 class="inside_form_table">';
          $x .= '<tr>';
          $x .= '<td class=tableContent width=50%>';
          $x .= '<select name=GroupID[] id=GroupID size=10 multiple>';

          for($i = 0;$i<count($row);$i++){
           $chosenIDAry[] = $row[$i][$idTag];
           $tempName = $row[$i][$nameLang] !=null?$row[$i][$nameLang]:$row[$i][$nameBackupLang];
    	   $x .= '<option value="'.$row[$i][$idTag].'">'.$tempName.'</option>';
          }
          $x .= "<option>";
          for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
            $x .= "</option>";
            $x .= "</select>";
            $x .= "</td>";

            $x .= "<td class=tableContent align=center>";
		       if($PATH_WRT_ROOT=="")	$PATH_WRT_ROOT = "../../";
		        include_once($PATH_WRT_ROOT."includes/libinterface.php");
		        $linterface 	= new interface_html();
		        $x .= $linterface->GET_BTN($Lang['Btn']['AddAllWithArrow'], "submit", "allSelectionGroupOperation('ADDALL');return false;", "addAll") . "<br /><br />";
		        $x .= $linterface->GET_BTN($Lang['Btn']['AddWithArrow'], "submit", "checkOptionTransfer(this.form.elements['AvailableGroupID[]'],this.form.elements['GroupID[]']);return false;", "add") . "<br /><br />";
		        $x .= $linterface->GET_BTN($Lang['Btn']['DeleteAllWithArrow'], "submit", "allSelectionGroupOperation('REMOVEALL');return false;", "deleteAll"). "<br /><br />";
		        $x .= $linterface->GET_BTN($Lang['Btn']['DeleteWithArrow'], "submit", "checkOptionTransfer(this.form.elements['GroupID[]'],this.form.elements['AvailableGroupID[]']);return false;", "delete");
            $x .= "</td>";

	        $x .= "<td class=tableContent width=50%>";
	        $x .= "<select id=AvailableGroupID name=AvailableGroupID[] size=10 multiple>";
            for($i = 0;$i<count($totalSelectionAry);$i++){
            	if(!in_array($totalSelectionAry[$i][$idTag], $chosenIDAry)){
            		$tempName = $totalSelectionAry[$i][$nameLang] !=null?$totalSelectionAry[$i][$nameLang]:$row[$i][$nameBackupLang];
            		$x .= '<option value="'.$totalSelectionAry[$i][$idTag].'">'.$tempName.'</option>';
            	}
            }
            $x .= "<option>";
           for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
             $x .= "</option>";
             $x .= "</select>";
             $x .= "</td>";
             $x .= "</tr>";
             $x .= "</table>";
          return $x;
        }

       function addIntranetAppTeacherStatusGroups($groupID) {
		 	$sql = "INSERT INTO INTRANET_APP_TEACHER_LIST_ENABLE_GROUP
				(GroupID, DateInput, InputBy, DateModified,ModifiedBy)
			VALUES
				('".$groupID."',now(),'".$_SESSION['UserID']."',now(),'".$_SESSION['UserID']."')";
			return $this->db_db_query($sql);
		}

		function deleteIntranetAppTeacherStatusRemovedGroups($groupID) {
		 	$sql = "Delete From INTRANET_APP_TEACHER_LIST_ENABLE_GROUP Where GroupID in (".$groupID.")";
			return $this->db_db_query($sql);
		}

        function doHouseKeeping() {
        	global $eclassAppConfig;
        	$successAry = array();

        	$sql = "Delete From APP_REQUEST_LOG Where DateInput < DATE_SUB(NOW(), INTERVAL 14 day)";
        	$successAry['APP_REQUEST_LOG'] = $this->db_db_query($sql);

        	$sql = "Delete From INTRANET_APP_NOTIFY_MESSAGE_LOG Where DateInput < DATE_SUB(NOW(), INTERVAL 30 day)";
        	$successAry['INTRANET_APP_NOTIFY_MESSAGE_LOG'] = $this->db_db_query($sql);

        	$sql = "Select UserID From APP_LOGIN_LOG Where LastLoginTime < DATE_SUB(NOW(), INTERVAL 180 day)";
        	$inactiveUserIdAry = Get_Array_By_Key($this->returnResultSet($sql), 'UserID');
        	$sql = "Update APP_USER_DEVICE Set RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive']."', LoginStatus = '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['deleted']."', DisableReason = '".$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['didNotLoginForLong']."_".date('Ymd_His')."' Where UserID in ('".implode("', '", (array)$inactiveUserIdAry)."') And RecordStatus = '".$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active']."' And LoginStatus = '".$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedIn']."'";
        	$successAry['APP_USER_DEVICE'] = $this->db_db_query($sql);

        	return !in_array(false, $successAry);
        }

        function standardizePushMessageText($text) {
            // [2020-0728-1019-47066]	[removed]
            //$text = str_replace('&#160;', '', $text);

            // M74821
			// http://stackoverflow.com/questions/1497885/remove-control-characters-from-php-string/1497911#1497911
        	return preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $text);
        }

        function isSchoolNewsAdmin($parUserID){
        	global $PATH_WRT_ROOT;
        	include_once ($PATH_WRT_ROOT.'includes/libgeneralsettings.php');
			$LibGeneralSettings = new libgeneralsettings();
			$SchoolSettingsAdmin = $LibGeneralSettings->Get_General_Setting("SchoolSettings",array("'admin_user'"));
			$SchoolSettingsAdminAry = explode(",", $SchoolSettingsAdmin[admin_user]);
			$isAdmin = in_array($parUserID, $SchoolSettingsAdminAry);
			if($isAdmin) return true;

			include_once($PATH_WRT_ROOT.'includes/user_right_target.php');
			$liburt = new user_right_target();
			$accessRightAry = $liburt->Load_User_Right($parUserID);
			return $accessRightAry['other-schoolNews']? true : false;
        }


        function sendPushMessageFromApi($parUserIdList, $parTitle, $parContent) {
        	global $plugin, $eclassAppConfig;

        	$httpReferrer = $_SERVER['HTTP_REFERER'];

        	$trustedSiteAry = array();
        	$trustedSiteAry[] = '192.168.0.146';
        	$trustedSiteAry[] = '192.168.0.149';
        	$trustedSiteAry[] = 'videolib-dev.eclass.com.hk';
        	$trustedSiteAry[] = 'videolib.eclass.com.hk';
        	$trustedSiteAry[] = 'store.eclass.com.hk';
        	$numOfTrusted = count($trustedSiteAry);
        	$isFromTrustedSite = false;
        	for ($i=0; $i<$numOfTrusted; $i++) {
        		$_siteUrl = $trustedSiteAry[$i];

        		if (stripos($httpReferrer, $_siteUrl) !== false) {
        			$isFromTrustedSite = true;
        			break;
        		}
        	}

        	if ($isFromTrustedSite) {
        		$userIdAry = explode(',', $parUserIdList);
	        	$sql = "Select UserID, RecordType From INTRANET_USER Where UserID IN ('".implode("','", (array)$userIdAry)."')";
	        	$userInfoAry = $this->returnResultSet($sql);
	        	$numOfUser = count($userInfoAry);

	        	$userTypeAssoAry = array();
	        	for ($i=0; $i<$numOfUser; $i++) {
	        		$_userId = $userInfoAry[$i]['UserID'];
	        		$_recordType = $userInfoAry[$i]['RecordType'];

	        		$userTypeAssoAry[$_recordType][] = $_userId;
	        	}

	        	$returnAry = array();
	        	if ($plugin['eClassTeacherApp']) {
	        		$targetUserIdAry = $userTypeAssoAry[USERTYPE_STAFF];

	        		if (is_array($targetUserIdAry) && count($targetUserIdAry) > 0) {

	        			$numOfTargetUser = count($targetUserIdAry);
	        			$individualMessageInfoAry = array();

	        			for ($i=0; $i<$numOfTargetUser; $i++) {
	        				$_userId = $targetUserIdAry[$i];

	        				$individualMessageInfoAry[0]['relatedUserIdAssoAry'][$_userId] = array($_userId);
	        			}

	        			$notifyMessageId = $this->sendPushMessage($individualMessageInfoAry, $parTitle, $parContent, $isPublic=0, $recordStatus=0, $eclassAppConfig['appType']['Teacher']);
	        			$returnAry['eClassTeacherApp_notifyMessageId'] = $notifyMessageId;
	        		}
	        	}

	        	return $returnAry;
        	}
        	else {
        		return '502';
        	}
        }

        function overrideExistingScheduledPushMessageFromModule($fromModule='', $moduleRecordID='', $newNotifyMessageId='', $parAppType='') {
        	if ($parAppType != '') {
        		$cond_appType = " And ianm.AppType = '".$parAppType."' ";
        	}
        	// delete not yet sent existing scheduled message
			$sql = "Select ianm.NotifyMessageID 
					From INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION as ianmmr 
					inner join INTRANET_APP_NOTIFY_MESSAGE as ianm on ianmmr.NotifyMessageID = ianm.NotifyMessageID 
					Where ianmmr.ModuleName = '$fromModule' 
					And ianmmr.ModuleRecordID = '$moduleRecordID'
					And ianm.NotifyDateTime > now()
					$cond_appType
					";
			$oldScheduledMessageIDAry = $this->returnVector($sql);

			if (count($oldScheduledMessageIDAry) > 0) {
				$successAry['deleteOldScheduledMessage'] = $this->deleteScheduledPushMessageInServer($oldScheduledMessageIDAry, $parAppType);
			}

			if ($fromModule != '' && $moduleRecordID != '' && $newNotifyMessageId != '') {
				$sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION
						(NotifyMessageID, ModuleName, ModuleRecordID, DateInput)
					Values
						('$newNotifyMessageId', '$fromModule', '$moduleRecordID', now())
					";
				$successAry['insertPushMessageModuleRelation'] = $this->db_db_query($sql);

			} else {

			}

			return (!in_array(false, (array)$successAry))? true : false;
        }

        function deleteScheduledPushMessageInServer($notifyMessageIDAry, $parAppType='') {

        	global $PATH_WRT_ROOT, $intranet_root,  $eclassAppConfig, $config_school_code;
        	include_once($intranet_root."/includes/libmessagecenter.php");
			include_once($intranet_root.'/includes/json.php');
			include_once($intranet_root.'/includes/liblog.php');

        	$li = new libdb();
			$jsonObj = new JSON_obj();
			$list =implode(",", $notifyMessageIDAry);

			// delete schedule push message in central server
			$list =implode(",", $notifyMessageIDAry);

			if ($parAppType != '') {
        		$cond_appType = " And AppType = '".$parAppType."' ";
        	}

			$sql = "Select NotifyMessageID, ToServer From INTRANET_APP_NOTIFY_MESSAGE WHERE NotifyMessageID IN ($list) And SendTimeMode = '".$eclassAppConfig['pushMessageSendMode']['scheduled']."' ".$cond_appType;
			$scheduledMessageAssoAry = BuildMultiKeyAssoc($li->returnResultSet($sql), 'ToServer', array('NotifyMessageID'), $SingleValue=1, $BuildNumericArray=1);


			$canDelete = true;
			foreach ((array)$scheduledMessageAssoAry as $_toServer => $_notifyMessageIdAry) {
				$postParamAry = array();
				$postParamAry['RequestMethod'] = 'cancelScheduledPushMessage';
				$postParamAry['SchoolCode'] = $config_school_code;
				$postParamAry['NotifyMessageIDList'] = implode(',', $_notifyMessageIdAry);
				$postJsonString = $jsonObj->encode($postParamAry);


				$headers = array('Content-Type: application/json');

				session_write_close();
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_URL, $_toServer);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
				$responseJson = curl_exec($ch);
				curl_close($ch);

				$responseJson = standardizeFormPostValue($responseJson);
				$responseAry = $jsonObj->decode($responseJson);

				$SuccessArr['DeleteFromCentralServer'] = ($responseAry['MethodResult']['response']=='ACK')? true : false;
				$canDelete = $SuccessArr['DeleteFromCentralServer'];
			}


			if ($canDelete) {
				//$sql = "DELETE FROM INTRANET_APP_NOTIFY_MESSAGE WHERE NotifyMessageID IN ($list)";
				$sql = "UPDATE INTRANET_APP_NOTIFY_MESSAGE SET RecordStatus = 0, ShowInApp = 0 WHERE NotifyMessageID IN ($list) ".$cond_appType;
				$success = $li->db_db_query($sql);

				# insert delete log
				$liblog = new liblog();
				$tmpAry = array();
				$tmpAry['NotifyMessageID'] = $list;
				$SuccessArr['Log_Delete'] = $liblog->INSERT_LOG('MessageCenter', 'Delete_Parent_Push_Notification', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_APP_NOTIFY_MESSAGE');
			}

			return $success;
        }

        function getSSOParam($parUserId, $parModule) {
        	global $PATH_WRT_ROOT, $eclassAppConfig, $config_school_code;

        	include_once($PATH_WRT_ROOT.'includes/libeclassapiauth.php');
        	$lapiAuth = new libeclassapiauth();

//        	$sql = "Select UserLogin, HashedPass, RecordType From INTRANET_USER Where UserID = '".$parUserId."'";
//        	$userAry = $this->returnResultSet($sql);
			$userAry = $this->getUserInfoForSSO($parUserId);
        	$userLogin = $userAry[0]['UserLogin'];
        	$hashedPass = $userAry[0]['HashedPass'];
			$recordType = $userAry[0]['RecordType'];

			switch ($recordType) {
                case USERTYPE_STAFF:
                    $apiKey = $lapiAuth->GetAPIKeyByProject('eClass App (Teacher)');
                    break;
                case USERTYPE_STUDENT:
                    $apiKey = $lapiAuth->GetAPIKeyByProject('eClass App (Student)');
                    break;
               case USERTYPE_PARENT:
                    $apiKey = $lapiAuth->GetAPIKeyByProject('eClass App');
                    break;
			}

			$paramAry = array();
			$paramAry[] = 'ul'.$eclassAppConfig['separator'].$userLogin;
			$paramAry[] = 'hp'.$eclassAppConfig['separator'].$hashedPass;
			$paramAry[] = 'ak'.$eclassAppConfig['separator'].$apiKey;
			$paramAry[] = 'm'.$eclassAppConfig['separator'].$eclassAppConfig['moduleCode'][$parModule];
			$paramAry[] = 'sc'.$eclassAppConfig['separator'].$config_school_code;
			$param = implode($eclassAppConfig['delimiter'], $paramAry);

			return 'p='.getEncryptedText($param, $eclassAppConfig['ssoSalt']);
		}

		function getUserInfoForSSO($parUserId) {
			$funcParamAry = get_defined_vars();
			$cacheResultAry = $this->getCacheResult('getUserInfoForSSO', $funcParamAry);
	    	if ($cacheResultAry !== null) {
	    		return $cacheResultAry;
	    	}

			$sql = "Select UserLogin, HashedPass, RecordType From INTRANET_USER Where UserID = '".$parUserId."'";
        	$userAry = $this->returnResultSet($sql);

        	// [2020-0221-1443-40207] Get required str if empty
        	foreach((array)$userAry as $index => $curUserAry) {
        		if($curUserAry['HashedPass'] == '') {
                    $userAry[$index]['HashedPass'] = $this->getUserPassInfo($parUserId, $curUserAry['UserLogin']);
				}
			}

        	$this->setCacheResult('getUserInfoForSSO', $funcParamAry, $userAry);
        	return $userAry;
		}

		function getSSOParamAry($parStr) {
			global $eclassAppConfig, $config_school_code;

			$param = getDecryptedText($parStr, $eclassAppConfig['ssoSalt'],10,$ignoreTimeChecking=true);
			$param = str_replace($eclassAppConfig['delimiter'], '&', $param);
			$param = str_replace($eclassAppConfig['separator'], '=', $param);

			$paramAry = array();
			parse_str($param, $paramAry);

			return $paramAry;
		}

		function getUserLoginAppInfo($parUserIdAry) {
			$sql = "Select UserID, LastLoginTime From APP_LOGIN_LOG Where UserID In ('".implode("','", (array)$parUserIdAry)."')";
			return $this->returnResultSet($sql);
		}

		function isEnabledStudentApp() {
			global $plugin, $eclassAppConfig;

			$eClassAppSettingsObj = $this->getAppSettingsObj();
			$enableStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']);

			return ($plugin['eClassStudentApp'] && $enableStudentApp)? true : false;
		}

		function clearLicenseCache() {
			$sql = "DELETE FROM INTRANET_APP_SETTINGS WHERE SettingName like '%appInLicense_%'";
			$returnAry['success'] = ($this->db_db_query($sql))? 1 : 0;
			return $returnAry;
		}

        function savePushMessageAttachment($messageID, $attachmentInfoAry) {
            global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;

            include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
            $lfs = new libfilesystem();

            $successAry = array();
            if ($attachmentInfoAry['name'] != ''  && $attachmentInfoAry['error'] > 0) {
                $successAry['dataValid'] = false;
            }
            else if ($attachmentInfoAry['name'] == '') {
                $successAry['emptyFile'] = false;
            }
            else {
                // create root folder
                $tmpFilePath = $eclassAppConfig['urlFilePath'].'pushMessage/';

                $messageID = intval($messageID);
                $controlConstant = 10000;

                $quotient = (int)($messageID / $controlConstant);

                $messageIDReminder = $messageID % $controlConstant;

                $tmpFilePath = $tmpFilePath.$quotient.'/'.$messageIDReminder.'/';

                if (!file_exists($tmpFilePath)) {
                    $successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
                }

                // upload new files
                $orgFileName = $attachmentInfoAry['name'];
                $tmpName = $attachmentInfoAry['tmp_name'];
                $fileExt = get_file_ext($orgFileName);
                $targetFileName = $messageID.'_attachment'.$fileExt;
                $targetFilePath = $tmpFilePath.$targetFileName;


                //Resize before upload
                // Create image from file
                $info = getimagesize($attachmentInfoAry['tmp_name']);
                $imageMineType = $info['mime'];
                switch(strtolower($imageMineType))
                {
                    case 'image/jpeg':
                    case 'image/pjpeg':   //For IE only
                        $image = imagecreatefromjpeg($attachmentInfoAry['tmp_name']);
                        break;
                    case 'image/png':
                    case 'image/x-png':	   //For IE only
                        $image = imagecreatefrompng($attachmentInfoAry['tmp_name']);
                        break;
                    case 'image/gif':
                        $image = imagecreatefromgif($attachmentInfoAry['tmp_name']);
                        break;
                }

                $max_width = 1024;
                $max_height = 1024;

                // Get current dimensions
                $old_width  = imagesx($image);
                $old_height = imagesy($image);

                // Calculate the scaling we need to do to fit the image inside our frame
                $scale      = min($max_width/$old_width, $max_height/$old_height);

                if ($scale < 1){//if resize is needed
                    // Get the new dimensions
                    $new_width  = ceil($scale*$old_width);
                    $new_height = ceil($scale*$old_height);

                    // Create new empty image
                    $new = imagecreatetruecolor($new_width, $new_height);

                    // Keep the transparent background for png
                    imagealphablending($new, false);
                    imagesavealpha($new, true);
                    $transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
                    imagefilledrectangle($new, 0, 0, $old_width, $old_height, $transparent);

                    // Resize old image into new
                    imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

                    // Catch the imagedata
                    $successAry['uploadFile'] = imagepng($new, $targetFilePath);

                    // Destroy resources
                    imagedestroy($image);
                    imagedestroy($new);
                }else{ //if no resize process is needed
                    $successAry['uploadFile'] = $lfs->file_copy($tmpName, $targetFilePath);
                }

                if ($successAry['uploadFile']) {
                    $dbFilePath = str_replace($eclassAppConfig['urlFilePath'], '', $targetFilePath);

                    // insert
                    $sql = "Insert Into INTRANET_APP_NOTIFY_MESSAGE_ATTACHMENT 
                                (NotifyMessageID, FileName, AttachmentPath)
                            Values
                                (".$messageID.", '".$targetFileName."', '".$dbFilePath."')
                            ";
                    $successAry['insertDbRecord'] = $this->db_db_query($sql);
                }
            }

            if(!in_array(false, (array)$successAry)){
                $returnAry['uploadSuccess'] = true;

                $returnFilePath = str_replace($intranet_root.'/', '', $targetFilePath);
                $returnFilePath = str_replace(curPageURL($withQueryString=false, $withPageSuffix=false).'/', '', $returnFilePath);

                $returnAry['targetFilePath'] = curPageURL($withQueryString=false, $withPageSuffix=false).'/'.$returnFilePath;
            }else{
                $returnAry['uploadSuccess'] = false;
            }

            return $returnAry;
        }
        
        function checkKisEnrolment(){
            $sql = "SELECT * FROM APP_ACCESS_RIGHT WHERE Module = 'eEnrolmentUserView' AND RecordStatus = 1";
            $result = $this->returnArray($sql);
            
            if(sizeof($result) > 0){
                $sql = "UPDATE APP_ACCESS_RIGHT SET RecordStatus = 0 WHERE Module = 'eEnrolmentUserView'";
                $success = $this->db_db_query($sql);
            }
        }

        function ws_saveUserPhoto($parUserId, $parPhotoType, $parPhotoData) {
            global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;

            include_once($PATH_WRT_ROOT.'includes/libuserphoto.php');
            include_once($PATH_WRT_ROOT.'includes/libeclass_ws.php');
            $lphoto= new libuserphoto();
            $leclass_ws = new libeclass_ws();

            $returnAry = array();
            $successAry = array();

            if ($parPhotoData == '') {
                $successAry['ErrorBecausePhotoIsEmpty'] = false;
            }
            else {
                $photoBinary = base64_decode($parPhotoData);

                // Personal Photo
                if (strtoupper($parPhotoType) == 'P') {
                    $successAry['savePersonalPhoto'] = $lphoto->savePersonalPhoto($parUserId, $photoBinary);
                }
                else if (strtoupper($parPhotoType) == 'O') {
                    $successAry['saveOfficialPhoto'] = $lphoto->saveOfficialPhoto($parUserId, $photoBinary);
                }
            }

            $returnAry['UpdateSuccess'] = in_array(false, (array)$successAry)? '0' : '1';

            $officialPhotoAry = $leclass_ws->WS_GetUserOfficialPhotoPath($parUserId);
            $returnAry['OfficialPhotoPath'] = $officialPhotoAry['photoPath'];
            $personalPhotoAry = $leclass_ws->WS_GetUserPersonalPhotoPath($parUserId);
            $returnAry['PersonalPhotoPath'] = $personalPhotoAry['photoPath'];

            return $returnAry;
        }

        function ws_useOfficialPhotoAsPersonalPhoto($parUserId) {
            global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;

            include_once($PATH_WRT_ROOT.'includes/libuserphoto.php');
            $lphoto = new libuserphoto();

            $successAry = array();
            $successAry['useOfficialAsPersonalPhoto'] = $lphoto->useOfficialPhotoAsPersonalPhoto($parUserId);

            $returnAry['UpdateSuccess'] = in_array(false, (array)$successAry)? '0' : '1';
            return $returnAry;
        }

        function ws_cancelOfficialPhotoAsPersonalPhoto($parUserId) {
            global $eclassAppConfig, $PATH_WRT_ROOT, $intranet_root;

            include_once($PATH_WRT_ROOT.'includes/libuserphoto.php');
            include_once($PATH_WRT_ROOT.'includes/libeclass_ws.php');
            $lphoto = new libuserphoto();
            $leclass_ws = new libeclass_ws();

            $successAry = array();
            $successAry['cancelOfficialAsPersonalPhoto'] = $lphoto->cancelOfficialPhotoAsPersonalPhoto($parUserId);

            $returnAry['UpdateSuccess'] = in_array(false, (array)$successAry)? '0' : '1';

            $personalPhotoAry = $leclass_ws->WS_GetUserPersonalPhotoPath($parUserId);
            $returnAry['PersonalPhotoPath'] = $personalPhotoAry['photoPath'];

            return $returnAry;
        }
	}
}
?>
