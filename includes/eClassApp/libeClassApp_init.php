<?php
// editing by 
 
/********************

 ********************/
if (!defined("LIBECLASSAPP_INIT_DEFINED")) {
	define("LIBECLASSAPP_INIT_DEFINED", true);
	
	class libeClassApp_init {
		
		function libeClassApp_init() {
			
		}
		
		function getPageLang($parLang) {
			global $PATH_WRT_ROOT;
			
			//F112163 php5.4 cannot get $parLang directly, need to check $_GET / $_POST
			//$pageLang = 'en';
			if ($parLang=='') {
				if (isset($_GET['parLang'])) {
					$parLang = $_GET['parLang'];
				}
				else if (isset($_POST['parLang'])) {
					$parLang = $_POST['parLang'];
				}
				else {
					$pageLang = 'en';
				}
			}
			
			if (file_exists($PATH_WRT_ROOT."file/eclass_app.php")) {
				include_once($PATH_WRT_ROOT."file/eclass_app.php");
				$pageLang = $eclass_app_language;
			} 
			else {
				if ($parLang != '') {
					switch (strtolower($parLang)) {
						case 'en':
							$pageLang = 'en';
							break;
						default:
							$pageLang = 'b5';
					}
				}
			}
			
			return $pageLang;
		}
	}
}
?>