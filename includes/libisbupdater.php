<?php
/*
 * Using: 
 * 
 * Description: updater class for handling ischoolbag auto-update
 * Terminology: 	
 * 		Version: iSchoolBag version, unique within the same module
 * 		Package: individual file, each belongs to a specific Version. Packages, altogether build up a Version
 * 		Patch  : a downloadable patch built from Package, which is then used by ischoolbag client to upgrade Version
 * Note: iSchoolBag contains not a single Version, but each Module does
 * Current Modules include: Main, Updater
 * Patch is built by combining all the version packages within client current version and ischoolbag latest version
 * Created Date: 2010-06-22
 * 
 * Change log
 * ==============
 * 
 * 20100818 : move version storage under "webserviceapi"
 * 
*/

class libisbupdater extends libdb{
	# package type
	var $PACKAGE_UPDATER = 1;
	var $PACKAGE_CONFIG = 2;
	var $PACKAGE_FILES= 3;
	var $PACKAGE_SCHEMA = 4;
	
	var $PACKAGE_NAMING = array('', 'Updater', 'Config', 'Files', 'Schema');
	
	# allowed Module
	var $ALLOWED_MODULES = array('Main', 'Updater');
	
	# static variable
	var $FOLDER_ROOT = 'webserviceapi/updates/ischoolbag/versions';  // under intranet root
	var $FOLDER_UPDATER = 'updater';
	var $FOLDER_CONFIG = 'config';
	var $FOLDER_FILES = 'files';
	var $FOLDER_SCHEMA = 'schema';
	
	var $PATCH_ROOT = 'webserviceapi/updates/ischoolbag/patches'; // under intranet root
	var $EXTENSION_ZIP = '.zip';
	var $EXTENSION_TXT = '.txt';
	
	# error code
	var $ERR_UNHANDLE = 1;
	var $ERR_INVALIDMODULE = 2;
	var $ERR_VERSIONEXISTED = 3;
	var $ERR_INVALIDVERSION = 4;
	var $ERR_INVALIDPACKAGE = 5;
	var $ERR_INVALIDPATCH = 6;
	
	function libisbupdater(){
         $this->libdb();
    }
	
	function testFunc() {
		global $intranet_root;
		
	}
	
	## This function return all the cached patches
	function getAllPatches(){
		$sql = 'SELECT PatchID, FromVersionID, ToVersionID, PackageType, PhysicalPath, CreatedDate FROM ISCHOOLBAG_PATCH ORDER BY ToVersionID, FromVersionID';
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	
	## This function return all the cached patches given the ToVersionID
	function getAllPatches_WithToVersionID($ToVersionID){
		$sql = 'SELECT P.PatchID, V.Version, V.Module, P.FromVersionID, P.ToVersionID, P.PackageType, P.PhysicalPath, P.CreatedDate';
		$sql .= ' FROM ISCHOOLBAG_PATCH P INNER JOIN ISCHOOLBAG_VERSION V ON P.FromVersionID = V.VersionID';
		$sql .= ' WHERE P.ToVersionID = '. $ToVersionID;
		$sql .= ' ORDER BY V.Module, V.Version';		
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	
	## This function return all the available versions
	function getAllVersions(){
		$sql = 'SELECT VersionID, Module, Version, ForceUpdate, CreatedDate, Released FROM ISCHOOLBAG_VERSION ORDER BY Module, Version';
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	
	## This function return the version details given VersionID
	function getVersionDetails($VersionID){
		$sql = 'SELECT VersionID, Module, Version, ForceUpdate, CreatedDate, Released FROM ISCHOOLBAG_VERSION Where VersionID = '. $VersionID;
		$returnArray = $this->returnArray($sql);
		return $returnArray[0];
	}
	
	## This function return all packages giving a VersionID
	function getAllPackages($VersionID){
		$sql = 'SELECT PackageID, VersionID, PackageType, PhysicalPath, Repository, CreatedDate FROM ISCHOOLBAG_VERSION_PACKAGES WHERE VersionID = '. $VersionID. ' ORDER BY PackageType, Repository, PhysicalPath';
		$returnArray = $this->returnArray($sql);
		return $returnArray;
	}
	
	## This function return VersionID giving Version and Module
	function getVersionID($Module, $Version, $checkReleased = false){
		$sql = 'SELECT VersionID FROM ISCHOOLBAG_VERSION WHERE Version = \''. $Version. '\' AND Module = \''. $Module . '\'';		
		if($checkReleased){
			$sql .= ' AND Released = 1';
		}
		
		$returnVector = $this->returnVector($sql);
		$VersionID = $returnVector[0];
		return $VersionID;
	}
	
	## This function return the latest update version given the current version	
	function getUpdateVersion($Module, $CurrentVersion){
		$sql = 'SELECT VersionID, Module, Version, ForceUpdate, CreatedDate, Released FROM ISCHOOLBAG_VERSION';
		$sql .= ' WHERE Released = 1 AND Module = \''. $Module. '\' AND Version >= \''. $CurrentVersion. '\'';
		$sql .= ' ORDER BY Version DESC';
	
		$versionArray = $this->returnArray($sql);
		
		$updateVersion = $versionArray[0];
		
		## if latest version is not a force-update one, then loop back to find a force-update one		
		$countVersion = count($versionArray);
		for($i = 0; $i < $countVersion - 1; $i++){
			if($versionArray[$i]['ForceUpdate'] == 1){
				$updateVersion['ForceUpdate'] = 1;					
			}
		}		
		return $updateVersion;
	}

	## This function return all the packages necessary for updating from CurrentVersion to UpdateVersion
	function getUpdatePatch($Module, $CurrentVersion, $UpdateVersion){
		global $ischoolbag_website, $intranet_root;
		
		# Get Latest version		
		$CurrentVersionID = $this->getVersionID($Module, $CurrentVersion);
		$UpdateVersionID = $this->getVersionID($Module, $UpdateVersion);				
		
		//if(!($CurrentVersionID and $UpdateVersionID)){
		//	return false;
		//}
		
		# select relevant packages
		# NOTE that ordering by Version is important for building the patch
		$sql = 'SELECT V.Version, P.PackageType, P.PhysicalPath, P.Repository, V.VersionID ';
		$sql .= ' FROM ISCHOOLBAG_VERSION_PACKAGES P RIGHT JOIN ISCHOOLBAG_VERSION V ON P.VersionID = V.VersionID';
		$sql .= ' WHERE V.Module = \''. $Module. '\' AND V.Released = 1 AND V.Version > \''. $CurrentVersion. '\' AND V.Version <= \''. $UpdateVersion. '\'';
		$sql .= ' ORDER BY V.Version';
						
		$returnArray = $this->returnArray($sql);
		# build up the update patch
		$patch = array();
		foreach($returnArray as $package){
			if($package['PackageType'] != ''){ // handle the situation when a Version got no package at all
				# 2010-10-18
				$package['PhysicalPath'] = $intranet_root. '/'. $package['PhysicalPath'];
				$patch[$package['PackageType']][] = $package;								
			}
		}
		
		# return list of patch location for downloads
		$returnPatch = array();
		foreach($patch as $subPatchType => $subPatch){
			$returnPatch[$subPatchType] = $ischoolbag_website. '/'. $this->buildPatch($subPatchType, $subPatch, $CurrentVersionID, $UpdateVersionID);
		}
		return $returnPatch;						
	}
	
	## This function build a subPatch using the relevant packages
	#	- The building logic is dependent on the Package_Type	
	function buildPatch($PackageType, $Packages, $FromVersionID, $ToVersionID){
		global $intranet_root;
		include_once('libfilesystem.php');		
		$libfilesystem = new libfilesystem();				
		
		## if the patch is already built, just return the previous built 
		$patchLocation = $this->getPatch($PackageType, $FromVersionID, $ToVersionID);		
		if($patchLocation){
			return $patchLocation;
		}		
		
		## generate a folder for storing the patch
		$folderName = MD5($PackageType. $FromVersionID. $ToVersionID. time());
		$patchRoot = $intranet_root. '/'. $this->PATCH_ROOT;
		$patchLocation = $patchRoot. '/'. $folderName;
		
		## build the patch
		switch($PackageType){
		case $this->PACKAGE_UPDATER:
			## for updater, return the latest updater
			$sourcePath = $Packages[count($Packages) - 1]['PhysicalPath'];
			$destinationPath = $patchLocation. $libfilesystem->file_ext($sourcePath);
			$libfilesystem->createFolder($patchLocation);
			$libfilesystem->file_copy($sourcePath, $destinationPath);			
			
			$patchLocation_underRoot = $this->PATCH_ROOT. '/'. $folderName. $libfilesystem->file_ext($sourcePath);
		break;
		case $this->PACKAGE_CONFIG:			
			## for config, combine all the config files into one
			# 	config in XML format, e.g. <config><update></update><update></update></config>
			$libfilesystem->createFolder($patchRoot);
			$patchLocation .= $this->EXTENSION_TXT;
			$fh = fopen($patchLocation, 'w');
			fwrite($fh, '<?xml version="1.0" encoding="utf-8"?>');
			fwrite($fh, '<config>');
			foreach($Packages as $package){
				$tmp_file = $package['PhysicalPath'];
				if($libfilesystem->file_ext($tmp_file) == $this->EXTENSION_TXT){
					$fh2 = fopen($tmp_file, 'r');
					$sqlContent = fread($fh2, filesize($tmp_file));					
					fclose($fh2);
					fwrite($fh, $sqlContent);
				}								
			}
			fwrite($fh, '</config>');
			fclose($fh);
			
			$patchLocation_underRoot = $this->PATCH_ROOT. '/'. $folderName. $this->EXTENSION_TXT;			
		break;
		case $this->PACKAGE_FILES:
			## for files, zip all the files
			# NOTE that the packages now are ordering by Version, which is important for building the patch
			# 	if same file exists under the same repository, the one from later version will 
			foreach($Packages as $package){
				$sourcePath = $package['PhysicalPath'];
				$repository = $package['Repository'] == ''? '': $package['Repository']. '/';
				$destinationPath = $patchLocation. '/'. $repository;
				//echo 'copy file from '. $sourcePath. ' to '. $destinationPath. '<br><br>';
				$libfilesystem->createFolder($destinationPath);
				$libfilesystem->file_copy($sourcePath, $destinationPath);
			}			
			chdir("$patchLocation");
			$Str = exec("zip -r ../$folderName *");
			$libfilesystem->folder_remove_recursive($patchLocation);
				
			$patchLocation_underRoot = $this->PATCH_ROOT. '/'. $folderName. $this->EXTENSION_ZIP;
		break;
		case $this->PACKAGE_SCHEMA:
			## for schema, combine all the schema files into one
			# 	schema in XML format, e.g. <schema><sql></sql><sql></sql></schema>
			$libfilesystem->createFolder($patchRoot);
			$patchLocation .= $this->EXTENSION_TXT;
			$fh = fopen($patchLocation, 'w');
			fwrite($fh, '<?xml version="1.0" encoding="utf-8"?>');
			fwrite($fh, '<schema>');
			foreach($Packages as $package){
				$tmp_file = $package['PhysicalPath'];
				if($libfilesystem->file_ext($tmp_file) == $this->EXTENSION_TXT){
					$fh2 = fopen($tmp_file, 'r');
					$sqlContent = fread($fh2, filesize($tmp_file));					
					fclose($fh2);
					fwrite($fh, $sqlContent);
				}								
			}
			fwrite($fh, '</schema>');
			fclose($fh);
			
			$patchLocation_underRoot = $this->PATCH_ROOT. '/'. $folderName. $this->EXTENSION_TXT;			
		break;
		}
		
		## insert db record
		$sql = 'INSERT INTO ISCHOOLBAG_PATCH (FromVersionID, ToVersionID, PackageType, PhysicalPath, CreatedDate)';
		$sql .= ' VALUES ('. $FromVersionID. ', '. $ToVersionID. ', '. $PackageType. ', \''. $patchLocation_underRoot. '\', now())';
		//echo $sql;
		$result = $this->db_db_query($sql);
		
		## return patch location
		if($result){
			return $patchLocation_underRoot;	
		}
		else{
			return false;
		}
	}
	
	## This function return the physical path for a patch (if exists), false if not exists
	function getPatch($PackageType, $FromVersionID, $ToVersionID){
		$sql = 'SELECT PhysicalPath FROM ISCHOOLBAG_PATCH P';
		$sql .= ' WHERE P.PackageType = '. $PackageType. ' AND P.FromVersionID = '. $FromVersionID . ' AND P.ToVersionID = '. $ToVersionID;			
		$returnVector = $this->returnVector($sql);
		if($returnVector[0] != ''){		
			$PhysicalPath = $returnVector[0];
			return $PhysicalPath;		
		}
		else{
			return false;
		}				
	}
	
	
	## This function return a specific package giving a PackageID
	#	return false if not existed
	function getPackage($PackageID){
		$sql = 'select PackageID, VersionID, PackageType, PhysicalPath, Repository, CreatedDate FROM ISCHOOLBAG_VERSION_PACKAGES WHERE PackageID = '. $PackageID;
		$returnArray = $this->returnArray($sql);
		if($returnArray[0]['PackageID'] == ''){
			return false;
		}
		else{
			return $returnArray[0];
		}
	}

	## This function return a specific package giving a PackageID
	#	return false if not existed
	function getPatch_withPatchID($PatchID){
		$sql = 'SELECT PatchID, FromVersionID, ToVersionID, PackageType, PhysicalPath, CreatedDate FROM ISCHOOLBAG_PATCH WHERE PatchID = '. $PatchID;		
		$returnArray = $this->returnArray($sql);
		if($returnArray[0]['PatchID'] == ''){
			return false;
		}
		else{
			return $returnArray[0];
		}
	}
	

	## This function update Version status (released | closed)
	function updateVersionStatus($VersionID, $Released){
		## check whether this version is created
		if(!$this->isVersionExist_WithVersionID($VersionID)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_VERSIONEXISTED;
			return $returnContent;
		}
		
		$sql = 'UPDATE ISCHOOLBAG_VERSION SET Released = '. $Released. ' WHERE VersionID = '. $VersionID;
		$result = $this->db_db_query($sql);
		
		if($result){
			$returnContent['isSuccess'] = true;
		}
		else{
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;			
		}				
		return $returnContent;
	}
	
	## This function update Version status (released | closed)
	function updateVersionForceUpdate($VersionID, $ForceUpdate){
		## check whether this version is created
		if(!$this->isVersionExist_WithVersionID($VersionID)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_VERSIONEXISTED;
			return $returnContent;
		}
		
		$sql = 'UPDATE ISCHOOLBAG_VERSION SET ForceUpdate = '. $ForceUpdate. ' WHERE VersionID = '. $VersionID;
		$result = $this->db_db_query($sql);
		
		if($result){
			$returnContent['isSuccess'] = true;
		}
		else{
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;			
		}				
		return $returnContent;
	}
	
	## This function upload a update package
	function uploadPackage($VersionID, $PackageType, $Repository, $Filename, $FileTempPath){							
		## genereate the package directory
		$physicalFolderPath = $this->getPackagePath($VersionID, $PackageType, $Repository);
		$physicalFilePath = $physicalFolderPath. '/'. $Filename;
		
		## check whether the input file exists
		if(!is_file($FileTempPath)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;
			return $returnContent;
		}
		
		## check whether Version ID exists
		if(!$this->isVersionExist_WithVersionID($VersionID)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_INVALIDVERSION;
			return $returnContent;			
		} 
		
		## create directory and copy tmp file to the package directory
		if(!is_dir($physicalFolderPath)){
			include_once('libfilesystem.php');
			$libfilesystem = new libfilesystem();
			
			if(!$libfilesystem->createFolder($physicalFolderPath)){
				$returnContent['isSuccess'] = false;
				$returnContent['errorCode'] = $this->ERR_UNHANDLE;
				return $returnContent;
			}
		}		
		if (!copy($FileTempPath, $physicalFilePath)) {
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;
			return $returnContent;
		}
		
		## create a package record in DB if not existsed		
		# 2010-11-03: only part under root
		$physicalFolderPath_underRoot = $this->getPackagePath_underRoot($VersionID, $PackageType, $Repository);
		$physicalFilePath_underRoot = $physicalFolderPath_underRoot. '/'. $Filename;
		
		if(!$this->isPackageExist_WithVersionID($VersionID, $physicalFilePath_underRoot)){
			$sql = 'INSERT INTO ISCHOOLBAG_VERSION_PACKAGES (VersionID, PackageType, PhysicalPath, Repository, CreatedDate)';		
			$sql .= ' VALUES ('. $VersionID. ', '. $PackageType. ', \''. $physicalFilePath_underRoot. '\', \''. $Repository. '\', now())';		
			$result = $this->db_db_query($sql);			
		}
		else{
			$result = true;
		}
		
		if($result){
			$returnContent['isSuccess'] = true;
		}
		else{
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;			
		}				
		return $returnContent;
	}
	
	function deletePackage($PackageID){
		include_once('libfilesystem.php');
		$libfilesystem = new libfilesystem();
		
		## check whether Package exists		
		$package = $this->getPackage($PackageID);
		if($package == false){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_INVALIDPACKAGE;
			return $returnContent;			
		}	
		
		## remove db record
		$sql = 'DELETE FROM ISCHOOLBAG_VERSION_PACKAGES WHERE PackageID = '. $PackageID;
		$result1 = $this->db_db_query($sql);
		
		## delete physical file
		$libfilesystem->item_remove($package['PhysicalPath']);
		
		$returnContent['isSuccess'] = true;
		return $returnContent;
	}
		
	function deletePatch($PatchID){
		global $intranet_root;
		include_once('libfilesystem.php');
		$libfilesystem = new libfilesystem();
		
		## check whether Package exists		
		$patch = $this->getPatch_withPatchID($PatchID);
		if($patch == false){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_INVALIDPATCH;
			return $returnContent;			
		}	
		
		## remove db record
		$sql = 'DELETE FROM ISCHOOLBAG_PATCH WHERE PatchID = '. $PatchID;		
		$result1 = $this->db_db_query($sql);
		
		## delete physical file		
		//echo $intranet_root. '/'. $patch['PhysicalPath'];
		$libfilesystem->item_remove($intranet_root. '/'. $patch['PhysicalPath']);
		
		$returnContent['isSuccess'] = true;
		return $returnContent;
	}	
		
	## This function delete a update version
	function deleteVersion($VersionID){
		include_once('libfilesystem.php');
		$libfilesystem = new libfilesystem();
		
		## check whether this version is created
		if(!$this->isVersionExist_WithVersionID($VersionID)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_VERSIONEXISTED;
			return $returnContent;
		}
		
		## remove cached patches
		$sql = 'SELECT PatchID from ISCHOOLBAG_PATCH where FromVersionID = '. $VersionID. ' OR ToVersionID = '. $VersionID;
		$returnPatchID = $this->returnArray($sql);
		$count = count($returnPatchID);
		for($i = 0; $i < $count; $i++){
			$this->deletePatch($returnPatchID[$i]['PatchID']);
		}
		
		## remove db records
		$sql = 'DELETE FROM ISCHOOLBAG_VERSION_PACKAGES WHERE VersionID = '. $VersionID;
		$result1 = $this->db_db_query($sql);	
		
		$sql = 'DELETE FROM ISCHOOLBAG_VERSION WHERE VersionID = '. $VersionID;
		$result2 = $this->db_db_query($sql);
		
		## remove packages file	
		$folderPath = $this->getVersionPath($VersionID);
		$libfilesystem->folder_remove_recursive($folderPath);
		
		if($result1 and $result2){
			$returnContent['isSuccess'] = true;
		}
		else{
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;
		}
		return $returnContent;
	}	
		
	## This function create a new ischoolbag version
	# paramters:
	# 	module: e.g. main | updater
	# 	version: version number e.g. 1.0.0.1 
	# 	forceUpdate: indicates whether this version is a force-update version e.g. 1 | 0
	# return:
	#	isSuccess 1 - success, 2 fail with error code
	function createNewVersion($Module, $Version, $ForceUpdate){
		## check whether Module is valid
		if(!$this->isModuleValid($Module)){				
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_INVALIDMODULE;
			return $returnContent;
		}
		
		## check whether version is valid input
		if(!$this->isVersionValid($Version)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_INVALIDVERSION;
			return $returnContent;
		}
		
		## check whether this version is created
		if($this->isVersionExist_WithModuleVersion($Module, $Version)){
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_VERSIONEXISTED;
			return $returnContent;
		}		
		
		## insert new version
		$sql = 'INSERT INTO ISCHOOLBAG_VERSION (Module, Version, ForceUpdate, CreatedDate, Released)';		
		$sql .= 'VALUES (\''. $Module. '\', \''. $Version. '\', '. $ForceUpdate. ', now(), 0)';		
		$result = $this->db_db_query($sql);
		
		if($result){
			$returnContent['isSuccess'] = true;
		}
		else{
			$returnContent['isSuccess'] = false;
			$returnContent['errorCode'] = $this->ERR_UNHANDLE;			
		}				
		return $returnContent;
	}		
	
	## This function return the physical path storing the version packages
	function getVersionPath($VersionID){
		global $intranet_root;		
		$folderPath = $intranet_root. '/'. $this->FOLDER_ROOT. '/'. $VersionID;
		return $folderPath;
	}
	
	# 2010-11-03
	function getPackagePath_underRoot($VersionID, $PackageType, $Repository){
		switch($PackageType){
		case $this->PACKAGE_UPDATER:
			$folder = $this->FOLDER_UPDATER;
		break;		
		case $this->PACKAGE_CONFIG:
			$folder = $this->FOLDER_CONFIG;
		break;
		case $this->PACKAGE_FILES:
			$folder = $this->FOLDER_FILES;
		break;
		case $this->PACKAGE_SCHEMA:
			$folder = $this->FOLDER_SCHEMA;
		break;
		}
		
		$folderPath = $this->FOLDER_ROOT. '/'. $VersionID. '/'. $folder;
		if($Repository != ''){
			$folderPath .= '/'. $Repository;
		}
		return $folderPath;
	}
	
	## This function return the physical package path giving the VersionID, packageType and repository
	function getPackagePath($VersionID, $PackageType, $Repository){
		switch($PackageType){
		case $this->PACKAGE_UPDATER:
			$folder = $this->FOLDER_UPDATER;
		break;		
		case $this->PACKAGE_CONFIG:
			$folder = $this->FOLDER_CONFIG;
		break;
		case $this->PACKAGE_FILES:
			$folder = $this->FOLDER_FILES;
		break;
		case $this->PACKAGE_SCHEMA:
			$folder = $this->FOLDER_SCHEMA;
		break;
		}
		
		$folderPath = $this->getVersionPath($VersionID). '/'. $folder;
		if($Repository != ''){
			$folderPath .= '/'. $Repository;
		}
		return $folderPath;
	}
	
	## This function check whether a versionID exists using VersionID
	function isVersionExist_WithVersionID($VersionID){
		$sql = 'SELECT VersionID FROM ISCHOOLBAG_VERSION WHERE VersionID = '. $VersionID;
		$returnVector = $this->returnVector($sql);
		return $returnVector[0] != '';		
	}
	
	## This function check whether a versionID exists using Module and Version
	function isVersionExist_WithModuleVersion($Module, $Version){
		$sql = 'SELECT VersionID FROM ISCHOOLBAG_VERSION WHERE Module = \''. $Module. '\' AND Version = \''. $Version. '\'';
		$returnVector = $this->returnVector($sql);
		return $returnVector[0] != '';		
	}
	
	## This function check whether a package exists using VersionID and physicalPath
	function isPackageExist_WithVersionID($VersionID, $PhysicalPath){
		$sql = 'SELECT VersionID FROM ISCHOOLBAG_VERSION_PACKAGES WHERE VersionID = '. $VersionID. ' AND PhysicalPath = \''. $PhysicalPath. '\'';
		$returnVector = $this->returnVector($sql);
		return $returnVector[0] != '';
	}
	
	## This function check whether the module is valid
	function isModuleValid($Module){
		return in_array($Module, $this->ALLOWED_MODULES);				
	}
	
	## This function check whether the input version is valid
	function isVersionValid($Version){
		return $Version != '';				
	}
}
?>