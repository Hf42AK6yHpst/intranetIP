<?php
// USING :
##########################################################
## Modification Log
## 2020-07-30 Henry Modified displayiMail2(), bug fix for hyper link crashed [Case#X191364]
## 2019-09-05 Ray Add smartstripslashes in DisplayMail2 for subject
## 2018-08-08 Carlos [ip.2.5.9.10.1] Modified displayiMail2(), apply cleanHtmlJavascript($html) to remove all script tags and onXXX event handlers.
## 2015-04-13 Carlos [ip2.5.6.5.1] Modified displayiMail2(), allow [Reply] and [Reply All] for iMail Archive.
## 2014-10-15 Carlos [ip2.5.5.10.1]
## - modified displayiMail2() added [Print] email button. added displayMailPrintVersion() for print version of email
## 2014-08-25 Carlos
## - modified displayiMail2(), if forward iMail Archive emails, would close the Archive window
## 2013-09-26 Carlos
## - modified displayiMail2(), add ";" to allowed character in url regular expression pattern
## 2012-02-27 Carlos
## - modified convertEmailToiMailCompose() regular expression, to prevent misreplacement of pattern in hyperlink
## 2011-12-13 Carlos
##  - modified displayiMail2(), added hypertext link replacement function
## 2011-07-22 Carlos
##  - modified displayiMail2(), fix fail to determine html content problem
## 2011-05-19 Carlos
##  - modified displayiMail2(), javascript redirect using location.href, Google Chrome will load the new page in iframe, not parent window, fix by changing to parent.window.location.href
##
## 2010-12-13 Ronald
##	- modified displayiMail2(), use a custom made function smartstripslashes() (in libcampusmail.php) to remove backslash
##		in the Subject & Message
##
## 2010-10-07 Ronald
##	- modified displayiMail2(), use intranet_undo_htmlspecialchars() & stripslashes() to remove special character before showing the Subject & Message in HTML format
##
## 2010-07-12 Marcus
##	- modified displayiMail2, for gamma mail, hide reply,forward if not auth_sendmail()
##
## 2010-06-22 Marcus
##	- modified convert2mailto, if $SYS_CONFIG['Mail']['hide_imail'] true, modified the link to send mail in gamma
##
## 2005-07-18: Kenneth Wong
## - Convert embed image from Outlook (cid) and store to DB
## - Convert back to iMail attachment type when viewing
## 2010-01-27: Michael Cheung
## returniMailSender()
## - if the user has no English / Chinese Name but still exists, it will display the Login ID

$embed_attach_key1 = "<!-- eClass iMail 1 -->[image attached ";
$embed_attach_key2 = "]<!-- eClass iMail 2 -->";

class libcampusmail2007 extends libcampusmail
{

    var $CampusMail;
    var $CampusMailID;
    var $CampusMailFromID;
    var $UserID;
    var $UserFolderID;
    var $SenderID;
    var $RecipientID;
    var $Subject;
    var $Message;
    var $Attachment;
    var $IsAttachment;
    var $IsImportant;
    var $IsNotification;
    var $RecordType;
    var $RecordStatus;
    var $DateInput;
    var $DateModified;
    var $IsRead;
    var $MessageReply;
    var $policy;

    # iMail Ext
    var $isDeleted;
    var $mailType;
    var $SenderEmail;
    var $InternalCC;
    var $InternalBCC;
    var $ExternalTo;
    var $ExternalCC;
    var $ExternalBCC;
    var $isHTML;
    var $MessageEncoding;



    # ------------------------------------------------------------------------------------
    function  libcampusmail2007($CampusMailID="")
    {
        $this->libcampusmail($CampusMailID);
    }

    # iMail functions
    # used in /home/imail/viewmail.php (viewmail_content.php)     iMail
    # For IP 2.0
    # To find the day passed
    function GET_DATE_FIELD()
    {
        return parseDateTime($this->DateModified);
    }

    # iMail functions
    # used in /home/imail/viewmail.php (viewmail_content.php)     iMail
    # For IP 2.0
    function displayiMail2()
    {
        global $image_path, $i_frontpage_campusmail_icon_important, $i_frontpage_campusmail_date, $i_frontpage_campusmail_subject, $i_frontpage_campusmail_sender, $i_frontpage_campusmail_message, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_recipients;
        global $i_CampusMail_New_InternalRecipients,$i_CampusMail_New_ExternalRecipients
               ,$i_CampusMail_New_To,$i_CampusMail_New_CC,$i_CampusMail_New_BCC
               ,$i_CampusMail_New_Reply,$i_CampusMail_New_ReplyAll,$i_CampusMail_New_Forward;
        global $view_remove_html,$i_CampusMail_New_ViewFormat_PlainText,$i_CampusMail_New_ViewFormat_HTML,$button_remove;
        global $i_CampusMail_New_ViewFormat_MessageSource,$i_CampusMail_New_No_Sendmail, $i_CampusMail_New_Show_All_Recipient;
        global $intranet_session_language;
        global $nextID,$prevID, $LAYOUT_SKIN;
        global $special_feature, $Lang, $PATH_WRT_ROOT;	### added by Ronald on 11 Feb 2009

        # warning message
        $forward_link ="";
        /*
        if($intranet_session_language=="b5")
            $sys_enc = "big5";
        else if ($intranet_session_language=="gb")
            $sys_enc = "gb2312";
        else
        {
            if(is_array($intranet_default_lang_set) && in_array("b5",$intranet_default_lang_set))
                $sys_enc = "big5";
            else if(is_array($intranet_default_lang_set) && in_array("gb",$intranet_default_lang_set))
                $sys_enc = "gb2312";
            else $sys_enc="big5";
        }
        */
        $sys_enc="utf-8";
        $mail_enc =$this->MessageEncoding==""?"iso-8859-1":strtolower($this->MessageEncoding);

        if($mail_enc!="iso-8859-1" && $mail_enc!=$sys_enc && $this->Attachment!="")
        {
            $forward_link="href=\"javascript:if(parent.warning_msg()) parent.window.location.href='compose.php?CampusMailID=".$this->CampusMailID."&action=F';\"";
        }
        else
        {
            $forward_link = "href='compose.php?CampusMailID=".$this->CampusMailID."&action=F'";
        }
        # end warning message
        $x = "";

        if($this->UserID<>"")
        {
            $x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' > \n
            		<tr> \n
              			<td colspan='2' bgcolor='#EEEEEE' > \n
                		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' > \n
                  		<tr> \n
                    		<td > \n
					";

            global $SYS_CONFIG;
            if($SYS_CONFIG['Mail']['hide_imail']===true)
            {
                if(auth_sendmail())
                {
                    //if($this->mailType==2)
                    //{
                    $ReplyBtn = "
									<td> \n
		                        	<a href='javascript:void(0);' onclick='parent.opener.window.location=\"/home/imail_gamma/compose_email.php?CampusMailID=".$this->CampusMailID."&MailAction=Reply\"; parent.opener.window.focus();top.window.close();' class='contenttool'> \n
		                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_reply.gif' width='20' height='20' border='0' align='absmiddle' /> \n
		                        	<span id='reply' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span> \n
		                        	</a> \n
		                        	</td> \n
		                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
		                        	<td> \n
		                        	<a href='javascript:void(0);' onclick='parent.opener.window.location=\"/home/imail_gamma/compose_email.php?CampusMailID=".$this->CampusMailID."&MailAction=ReplyAll\"; parent.opener.window.focus();top.window.close();' class='contenttool'> \n
		                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_reply_all.gif' width='20' height='20' border='0' align='absmiddle'> \n
		                        	<span id='replyall' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span> \n
		                        	</a> \n
		                        	</td> \n
		                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
						";
                    //}


                    $ForwardBtn = "
	                        	<td> \n
	                        	<p> \n
	                        	<a href=\"javascript:void(0);\" onclick=\"parent.opener.window.location='/home/imail_gamma/compose_email.php?CampusMailID=".$this->CampusMailID."&MailAction=Forward';parent.opener.window.focus();top.window.close();\" class=\"contenttool\"> \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_forward.gif' width='20' height='20' border='0' align='absmiddle' /> \n
	                        	<span id='forward' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span>  \n
	                        	</a> \n
	                        	</p> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
					";
                }

                $x .= "
                		<table border='0' cellspacing='0' cellpadding='2' > \n
                      	<tr> \n
                        	$ReplyBtn
							$ForwardBtn
                        	<td> \n
                        	<p> \n
                        	<a href='javascript:removeMail()' class='contenttool' > \n
                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif' width='20' height='20' border='0' align='absmiddle' />  \n
                        	<span id='delete' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\" ></span> \n
                        	</a> \n
                      	</tr> \n
                  		</table> \n
                  		";

            }
            else if (!auth_sendmail())
            {
                $forward_link = "href='javascript:alert(\"{$i_CampusMail_New_No_Sendmail}\")'";

                $x .= "
	                		<table border='0' cellspacing='0' cellpadding='2' > \n
	                      	<tr> \n
	                        	<td> \n
	                        	<a href='javascript:alert(\"{$i_CampusMail_New_No_Sendmail}\")' class='contenttool'> \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_reply.gif' width='20' height='20' border='0' align='absmiddle' /> \n
	                        	<span id='reply' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span> \n
	                        	</a> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
	                        	<td> \n
	                        	<a href='javascript:alert(\"{$i_CampusMail_New_No_Sendmail}\")' class='contenttool' > \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_reply_all.gif' width='20' height='20' border='0' align='absmiddle'> \n
	                        	<span id='replyall' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span> \n
	                        	</a> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
	                        	<td> \n
	                        	<p> \n
	                        	<a ".$forward_link." class='contenttool'> \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_forward.gif' width='20' height='20' border='0' align='absmiddle' /> \n
	                        	<span id='forward' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span>  \n
	                        	</a> \n
	                        	</p> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
	                        	<td> \n
	                        	<p> \n
	                        	<a href='javascript:removeMail()' class='contenttool' > \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif' width='20' height='20' border='0' align='absmiddle' />  \n
	                        	<span id='delete' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\" ></span> \n
	                        	</a> \n
	                      	</tr> \n
	                  		</table> \n
	                  		";
            }
            else
            {
                if($this->UserFolderID != -1){
                    $x .= "
	                		<table border='0' cellspacing='0' cellpadding='2' > \n
	                      	<tr> \n
	                        	<td> \n
	                        	<a href='compose.php?CampusMailID=".$this->CampusMailID."&action=R' class='contenttool'> \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_reply.gif' width='20' height='20' border='0' align='absmiddle' /> \n
	                        	<span id='reply' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span> \n
	                        	</a> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
	                        	<td> \n
	                        	<a href='compose.php?CampusMailID=".$this->CampusMailID."&action=RA' class='contenttool' > \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_reply_all.gif' width='20' height='20' border='0' align='absmiddle'> \n
	                        	<span id='replyall' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span> \n
	                        	</a> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
	                        	<td> \n
	                        	<p> \n
	                        	<a ".$forward_link." class='contenttool'> \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_forward.gif' width='20' height='20' border='0' align='absmiddle' /> \n
	                        	<span id='forward' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\"></span>  \n
	                        	</a> \n
	                        	</p> \n
	                        	</td> \n
	                        	<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
	                        	<td> \n
	                        	<p> \n
	                        	<a href='javascript:removeMail()' class='contenttool' > \n
	                        	<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif' width='20' height='20' border='0' align='absmiddle' />  \n
	                        	<span id='delete' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\" ></span> \n
	                        	</a> \n
	                      	</tr> \n
	                  		</table> \n
	                  		";
                }
            }
            $x .= "
				    		</td> \n
				    		<td align='right' > \n
							<table border='0' cellspacing='0' cellpadding='2'> \n
							<tr> \n
								<td><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' /></td> \n
								<td> \n
								<p><a href='javascript:viewMessageSource()' class='contenttool'> \n
								<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_view_source.gif' width='20' height='20' border='0' align='absmiddle'> \n
								<span id='view_source' class='contenttool' onMouseOver=\"this.className='contenttool_over'\" onMouseOut=\"this.className='contenttool'\" ></span> \n
								</a> \n
								</p> \n
								</td> \n
								<td><div class=\"Conntent_tool\"><a href=\"javascript:newWindow('".$PATH_WRT_ROOT."home/imail/print.php?CampusMailID=".$this->CampusMailID."',35);\" class=\"print\">".$Lang['Btn']['Print']."</a></div></td>
							</tr> \n
							</table> \n
				   			</td> \n
				  		</tr> \n
						</table> \n
						</td> \n
					</tr> \n
					<tr> \n
						<td colspan='2' >&nbsp;</td> \n
					</tr> \n
					<tr> \n
						<td colspan='2' > \n
						<table width='100%' border='0' cellspacing='0' cellpadding='5' > \n
						<tr> \n
				            <td width='120' align='left' class='formfieldtitle' >".(($this->IsImportant) ? $i_frontpage_campusmail_icon_important_2007 : "")."<span id='date' class='iMailComposefieldtitle' ></span></td> \n
				            <td width='4' align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle'> :</span></td> \n
				            <td class='tabletext' >".$this->DateModified." (".parseDateTime($this->DateModified,'',false).")</td> \n
						</tr> \n
				      	<tr>\n
				        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='sender' ></span></td>\n
				        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
				        	<td class='formfieldtitle' ><span class='tabletext'>".$this->returniMailSender()."</span></td>\n
				      	</tr>\n
				    ";

            ## Get recipients
            $int_to = $this->returnIntToRecipients();
            $int_to_short = $this->returnIntToRecipients_Short();
            $int_cc = $this->returnIntCCRecipients();
            $int_cc_short = $this->returnIntCCRecipients_Short();
            $int_bcc = $this->returnIntBCCRecipients();
            $int_bcc_short = $this->returnIntBCCRecipients_Short();
            $ext_to = $this->returnExtToRecipients();
            $ext_to_short = $this->returnExtToRecipients_short();
            $ext_cc = $this->returnExtCCRecipients();
            $ext_cc_short = $this->returnExtCCRecipients_short();
            $ext_bcc = $this->returnExtBCCRecipients();
            $ext_bcc_short = $this->returnExtBCCRecipients_short();

            $int_to_num = count($this->returnRecipientOption($this->RecipientID));
            $int_cc_num = count($this->returnRecipientOption($this->InternalCC));
            $int_bcc_num = count($this->returnRecipientOption($this->InternalBCC));

            $ex_to_num = $this->countExternalRecipients($this->ExternalTo);
            $ex_cc_num = $this->countExternalRecipients($this->ExternalCC);
            $ex_bcc_num = $this->countExternalRecipients($this->ExternalBCC);

            ## Internal (has internal recipients)
            if ($int_to!="" || $int_cc!="" || $int_bcc!="")
            {
                $recipient_table = "<table width='100%' border='0' cellspacing='1' cellpadding='1' class='tabletext' >";
                if ($int_to != "")
                {
                    if ($int_to_num > 10)
                    {
                        $int_to2 = str_replace("<br>",",",$int_to);
                        $int_to2 = str_replace(array("<br>","\n"),"",$int_to2);
                        $recipient_table .= "<tr ><td width='30' ><i><span id='int_new_to'></span>:</i></td><td width='90%'>";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"InterToShortDiv\" >";
                        //$recipient_table .= "<tr><td>".$int_to2."</td>";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$int_to_short."</td>";
                        $recipient_table .= "<td ><a href=\"javascript:parent.window.imail_frame.showInternalRep('to');\"><span id='RecipientToDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";

                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"InterToLongDiv\" style=\"display:none\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$int_to."</td>\n";
                        $recipient_table .= "<td ><a href=\"javascript:parent.window.imail_frame.hideInternalRep('to');\"><span id='HideRecipientToDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "</td></tr>\n";
                    }
                    else
                    {
                        $recipient_table .= "<tr><td width='30' ><i><span id='int_new_to'></span>:</i></td><td>$int_to</td></tr>\n";
                    }
                }
                if ($int_cc != "")
                {
                    if ($int_cc_num >10)
                    {
                        $int_cc2 = str_replace(array("<br>","\n"),"",$int_cc);
                        $recipient_table .= "<tr><td width='30' ><i><span id='int_new_cc'></span>:</i></td><td width='90%'>";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"InterCcShortDiv\" >";
                        //$recipient_table .= "<tr><td>".$int_cc2."</td>\n";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$int_cc_short."</td>\n";
                        $recipient_table .= "<td ><a href=\"javascript:parent.window.imail_frame.showInternalRep('cc');\"><span id='RecipientCcDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";

                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"InterCcLongDiv\" style=\"display:none\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$int_cc."</td>\n";
                        $recipient_table .= "<td ><a href=\"javascript:parent.window.imail_frame.hideInternalRep('cc');\"><span id='HideRecipientCcDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "</td></tr>\n";
                    }
                    else
                    {
                        $recipient_table .= "<tr><td width='30' ><i><span id='int_new_cc'></span>:</i></td><td>$int_cc</td></tr>\n";
                    }
                }
                if ($int_bcc != "")
                {
                    if ($int_bcc_num >10)
                    {
                        $int_bcc2 = str_replace(array("<br>","\n"),"",$int_bcc);

                        $recipient_table .= "<tr><td width=30><i><span id='int_new_bcc'></span>:</i></td><td width='90%'>";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"InterBccShortDiv\" >";
                        //$recipient_table .= "<tr><td>".$int_bcc2."</td>\n";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$int_bcc_short."</td>\n";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.showInternalRep('bcc');\"><span id='RecipientBccDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"InterBccLongDiv\" style=\"display:none\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$int_bcc."</td>\n";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.hideInternalRep('bcc');\"><span id='HideRecipientBccDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "</td></tr>\n";
                    }
                    else
                    {
                        $recipient_table .= "<tr><td width=30><i><span id='int_new_bcc'></span>:</i></td><td>$int_bcc</td></tr>\n";
                    }
                }
                $recipient_table .= "</table>";

                $x .= "
                      	<tr>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='int_recipient' ></span></td>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
                        	<td class='formfieldtitle' ><span class='tabletext'>".$recipient_table."</span></td>\n
                      	</tr>\n
            			";
            }

            ## External (has external recipients)
            if ($ext_to!=""||$ext_cc!=""||$ext_bcc!="")
            {
                $recipient_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>";
                if ($ext_to != "")
                {
                    if ($ex_to_num >10)
                    {

                        $ext_to2 = str_replace(array("<br>","\n"),"",$ext_to);
                        $recipient_table .= "<tr ><td width='30' ><i><span id='ext_new_to'></span>:</i></td><td width='90%'>";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"ExterToShortDiv\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        //$recipient_table .= "<tr><td>".$ext_to2."</td>";
                        $recipient_table .= "<tr><td>".$ext_to_short."</td>";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.showExternalRep('to');\"><span id='RecipientExToDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";

                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"ExterToLongDiv\" style=\"display:none\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$ext_to."</td>\n";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.hideExternalRep('to');\"><span id='HideRecipientExToDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "</td></tr>\n";

                    }
                    else
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='ext_new_to'></span>:</I></td><td>$ext_to</td></tr>\n";
                    }
                }
                if ($ext_cc != "")
                {
                    if ($ex_cc_num >10)
                    {

                        $ext_cc2 = str_replace(array("<br>","\n"),"",$ext_cc);
                        $recipient_table .= "<tr ><td width='30' ><i><span id='ext_new_cc'></span>:</i></td><td width='90%'>";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"ExterCcShortDiv\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        //$recipient_table .= "<tr><td>".$ext_cc2."</td>";
                        $recipient_table .= "<tr><td>".$ext_cc_short."</td>";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.showExternalRep('cc');\"><span id='RecipientExCcDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";

                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"ExterCcLongDiv\" style=\"display:none\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$ext_cc."</td>\n";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.hideExternalRep('cc');\"><span id='HideRecipientExCcDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "</td></tr>\n";
                    }
                    else
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='ext_new_cc'></span>:</I></td><td>$ext_cc</td></tr>\n";
                    }
                }
                if ($ext_bcc != "")
                {
                    if ($ex_bcc_num >10)
                    {

                        $ext_bcc2 = str_replace(array("<br>","\n"),"",$ext_bcc);
                        $recipient_table .= "<tr ><td width='30' ><i><span id='ext_new_bcc'></span>:</i></td><td width='90%'>";
                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"ExterBccShortDiv\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        //$recipient_table .= "<tr><td>".$ext_bcc2."</td>";
                        $recipient_table .= "<tr><td>".$ext_bcc_short."</td>";
                        $recipient_table .= "<td width=\"80\"><a href=\"javascript:parent.window.imail_frame.showExternalRep('bcc');\"><span id='RecipientExBccDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";

                        $recipient_table .= "<table width=\"100%\" class=\"tbclip\" id=\"ExterBccLongDiv\" style=\"display:none\" >";
                        $recipient_table .= "<col width='90%'><col width='10%'>";
                        $recipient_table .= "<tr><td>".$ext_bcc."</td>\n";
                        $recipient_table .= "<td><a href=\"javascript:parent.window.imail_frame.hideExternalRep('bcc');\"><span id='HideRecipientExBccDiv'></span></a></td></tr>\n";
                        $recipient_table .= "</table>\n";
                        $recipient_table .= "</td></tr>\n";
                    }
                    else
                    {
                        $recipient_table .= "<tr><td width=30><I><span id='ext_new_bcc'></span>:</I></td><td>$ext_bcc</td></tr>\n";
                    }
                }
                $recipient_table .= "</table>";
                $x .= "
                      	<tr>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='ext_recipient' ></span></td>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
                        	<td class='formfieldtitle' ><span class='tabletext'>".$recipient_table."</span></td>\n
                      	</tr>\n
            			";
            }
            $display_subject = $this->smartstripslashes($this->Subject);
            $x .= "
                      	<tr>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='subject' ></span></td>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
                        	<td class='formfieldtitle' ><span class='tabletext'>".intranet_wordwrap(str_replace(array('<','>'),array('&lt;','&gt;'),$display_subject),50,"\n",1)."</span></td>\n
                      	</tr>\n
					";
            if ($this->IsAttachment)
            {
                $x .= "
	                      	<tr>\n
	                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='attachment' ></span></td>\n
	                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
	                        	<td class='formfieldtitle' ><span class='tabletext'>".$this->displayiMailAttachment()."</span></td>\n
	                      	</tr>\n
	            		";
            }
            $x .= "
                        </table> \n
                        </td> \n
                      </tr> \n
					";

            $this->Message = $this->view_image_embed_message($this->Message);
            $this->Message = cleanHtmlJavascript($this->Message);

            # Convert Message
            /*
            if ($this->isHTML == 1)
            {
                $html_flag = true;
            }
            else if ($this->isHTML == -1)
            {
                 $html_flag = false;
            }
            else
            {
                $html_flag = $this->isHTMLMessage($this->Message);
            }
            */

            $html_flag = ($this->isHTMLMessage($this->Message) || $this->isHTML == 1);

            # Hypertext link replacement function - copy from imap_gamma.php, just create an anonymous function since I do not want to mess up other modules in case of function conflict/override
            $LinkExp='(.{0,1})?((https?://|www\.)+([-\w\.]+)+(:\d+)?([:/](([\w~#%&;\'\+/_\-\.]*)(\?[^ <>]+)?)?)*)(.{0,1})?';
            $Replace_Link = create_function('$matches',
                'if(trim($matches[1])!="")
				{
					if($matches[1]==\'=\' || $matches[1]==\'"\' || $matches[1]=="\'" || $matches[count($matches)-1]=="\'" || $matches[count($matches)-1]==\'"\')
					{
						return $matches[0];
					}else
					{
						if(stristr($matches[2], \'https://\') || stristr($matches[2], \'http://\'))
						{
							return $matches[1].\'<a href="\'.$matches[2].\'" target="_blank">\'.$matches[2].\'</a>\'.$matches[count($matches)-1];
						}else
						{
							return $matches[1].\'<a href="http://\'.$matches[2].\'" target="_blank">\'.$matches[2].\'</a>\'.$matches[count($matches)-1];
						}
					}
				}else
				{
					if(stristr($matches[2], \'http://\') || stristr($matches[2], \'https://\'))
					{
						return $matches[1].\'<a href="\'.$matches[2].\'" target="_blank">\'.$matches[2].\'</a>\'.$matches[count($matches)-1];
					}else
					{
						return $matches[1].\'<a href="http://\'.$matches[2].\'" target="_blank">\'.$matches[2].\'</a>\'.$matches[count($matches)-1];
					}
				}'
            );

            if ($html_flag)
            {
                $display_message = $this->Message;
                $display_message = $this->smartstripslashes($display_message);
                //$display_message = intranet_undo_htmlspecialchars(stripslashes($display_message));
                if ($view_remove_html == 1)
                {
                    $display_message = "<pre>".($this->removeHTMLtags($display_message))."</pre>";
                }
                else
                {
                    #$display_message = $this->convertCodedString($display_message);
                    $display_message = preg_replace_callback('@'.$LinkExp.'@i',$Replace_Link,$display_message);
                    $display_message = $this->convertEmailToiMailCompose($display_message);
                }
            }
            else
            {
                $text = $this->removeHTMLtags($this->Message);
                $text = nl2br($text); #nl2br(intranet_htmlspecialchars($text));
                $text = $this->convertURLS2($text);
                $text = $this->convertEmailToiMailCompose($text);
                #$text = $this->convertMail2($text);
                $display_message = $text;
                $display_message = $this->smartstripslashes($display_message);
                //$display_message = intranet_undo_htmlspecialchars(stripslashes($display_message));
            }

            $x .= "
					<tr>
						<td colspan='2' >
						<table width='100%' border='0' cellspacing='0' cellpadding='10' >
						<tr>
							<td>
							<p class='style1'>{$display_message}</td>
						</tr>
						</table>
						</td>
					</tr>
					";
            $x .= "</table>";

        }
        return $x;
    }


    # /home/imail/outbox_view.php       eClass IP - iMail
    # For IP 2.0
    # show reply date
    function displayCampusMailReplyiMail()
    {
        global $image_path, $i_frontpage_campusmail_read, $i_frontpage_campusmail_unread, $i_frontpage_campusmail_status;
        global $i_frontpage_campusmail_icon_read, $i_frontpage_campusmail_icon_unread;
        global $intranet_httppath,$i_general_show_child;
        global $LAYOUT_SKIN, $i_CampusMail_New_Recipient_Status, $i_CampusMail_New_Icon_NewMail_2007, $i_CampusMail_New_Icon_ReadMail_2007;


        $x = "";
        if($this->UserID<>"" && $this->IsNotification)
        {
            $row = $this->returnCampusiMailReply();
            $TotalReadFlag = sizeof($row);
            $ReadFlag = 0;
            for($i=0; $i<$TotalReadFlag; $i++)
            {
                $IsRead = $row[$i][4];
                if($IsRead == 1) $ReadFlag++;
            }

            $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"8\" >\n";
            $x .= "<tr>";
            $x .= "<td height=\"1\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\"></td>";
            $x .= "</tr>";
            $x .= "<tr>";
            $x .= "<td align=\"center\">";

            $x .= "<table width='90%' border='0' cellspacing='0' cellpadding='3'>";
            $x .= "<tr class='tablegreenbottom'>";
            $x .= "<td width='25%'><span class='tabletext'>{$i_CampusMail_New_Recipient_Status}</span></td>";
            $x .= "<td width='50%'>&nbsp;</td>";
            $x .= "<td width='10%' align='center' class='tabletext'>{$i_frontpage_campusmail_unread}<br />(".($TotalReadFlag - $ReadFlag).")</td>";
            $x .= "<td width='15%' align='center' class='tabletext'><center>{$i_frontpage_campusmail_read}<br />({$ReadFlag})</center></td>";
            $x .= "</tr>";

            $right2Send = auth_sendmail();
            for($i=0; $i<$TotalReadFlag; $i++)
            {
                list ($uid, $UserName, $ReplyID, $MailID, $IsRead, $Message, $id_type, $DateModified) = $row[$i];
                $Message = intranet_wordwrap($Message,20,"\n",1);

                $Message = intranet_undo_htmlspecialchars($Message);
                if ($right2Send)
                {
                    $UserName = "<a class='tablegreenlink' href='compose.php?CampusMailReplyID=$ReplyID' >$UserName</a>";
                }
                if ($id_type==3)
                {
                    $UserName .= " <a class='tablegreenlink' title='$i_general_show_child' onMouseOver=\"window.status='$i_general_show_child';return true;\" onMouseOut=\"window.status='';return true;\" href=javascript:newWindow('$intranet_httppath/home/view_child.php?uid=$uid',1)><img src=\"$image_path/icon_parentlink.gif\" border=0></a>";
                }
#                    $UserName = $row[$i][1];
#                    $IsRead = $row[$i][4];
#                    $Message = $row[$i][5];

                if ($i%2==0)
                {
                    $x .= "<tr class='tabletext' >\n";
                } else {
                    $x .= "<tr class='tablegreenrow2 tabletext' >\n";
                }
                $x .= "<td nowrap='nowrap'>$UserName<br /></td> \n";
                $x .= "<td>$Message<br></td>\n";
                if ($IsRead)
                {
                    $x .= "<td align='center'>&nbsp;</td>";
                    $x .= "<td align='center'>".str_replace(" ", "<br />", $DateModified)."{$i_CampusMail_New_Icon_ReadMail_2007}</td>";
                } else {
                    $x .= "<td align='center'>{$i_CampusMail_New_Icon_NewMail_2007}</td>";
                    $x .= "<td align='center'>&nbsp;</td>";
                }
                $x .= "</tr >\n";

            }
            $x .= "</table>\n";

            $x .= "</td></tr></table>\n";
        }
        return $x;
    }

    # used in /home/imail/viewmail.php (viewmail_content.php) iMail
    function displayiMailNotification2()
    {
        global $button_send, $i_frontpage_campusmail_read, $i_frontpage_campusmail_reply,$i_frontpage_campusmail_read;
        global $i_frontpage_campusmail_notification_instruction,$i_frontpage_campusmail_notification_instruction_message_only,$i_frontpage_campusmail_alert_not_reply;
        global $image_send, $LAYOUT_SKIN, $button_submit, $image_path, $button_send, $linterface, $i_frontpage_campusmail_icon_notification2_2007;

        $x = "";

        if($this->UserID<>"" && $this->IsNotification && !$this->isEmail())
        {
            # Check whether Sender's copy still exists
            $sql = "SELECT CampusMailID FROM INTRANET_CAMPUSMAIL WHERE CampusMailID = '".$this->CampusMailFromID."'";
            $temp = $this->returnVector($sql);
            if ($temp[0]!="")           # Still exists
            {
                $sql = "SELECT IsRead, Message FROM INTRANET_CAMPUSMAIL_REPLY WHERE CampusMailID = '".$this->CampusMailFromID."' AND UserID = '".$this->UserID."'";
                $row = $this->returnArray($sql, 2);
                $this->IsRead = $row[0][0];
                # $this->MessageReply = $row[0][1];
                $this->MessageReply = intranet_undo_htmlspecialchars($row[0][1]);
                $instruction = "";

                if (!$this->IsRead && !$this->isAutoReply())
                {
                    $checkBox = "<input type='checkbox' name='IsRead' id='IsRead' value='1' /><label for='IsRead' ><span id='campusmail_read' class='tabletext'>$i_frontpage_campusmail_read</span></label><br />";
                    $textScript = "onFocus=\"this.form.IsRead.checked=true;\" onClick=\"this.rows=5;\"";
                    $instruction = $i_frontpage_campusmail_notification_instruction;
                    $instruction_id=1;
                    //$submitScript = "onClick=\"if (!this.form.IsRead.checked) { var send=parent.confirmNotification(); if (send) {history.back(); return false;} else return false; }\"";
                    //$submitScript = "if (!this.form.IsRead.checked) { var send=parent.confirmNotification(); if (send) {history.back(); return false;} else return false; }";
                    $submitScript = "onClick=\"sendReply(this.form)\"";
                }
                else
                {
                    $checkBox = "<input type='hidden' name='IsRead' value='1' >";
                    $textScript = "";
                    $submitScript = "";
                }
                if ($this->MessageReply == "")
                {
                    if ($instruction == "")
                    {
                        $instruction = $i_frontpage_campusmail_notification_instruction_message_only;
                        $instruction_id=2;
                    }

                    //$replyBox = "<textarea name='Message' cols='50' $textScript  rows='5' wrap='virtual' class='textboxtext' ></textarea>";
                    //$replyBox = $linterface->GET_TEXTAREA('Message', '', 50, 5);
                    //$replyBox = "<textarea name='Message' cols='50' $textScript  rows='2' class='textboxtext' ></textarea>";
                    $replyBox = "<textarea name='Message' id='Message' cols='60' $textScript wrap='virtual' rows='3' class='textboxtext' ></textarea>";


                    $replyBox .= "<script language=\"javascript\" >";
                    $replyBox .= "  function sendReply(formObj)
										{
											if(!(formObj.IsRead.checked || formObj.IsRead.value=='1'))
											{ var send=confirmNotification(); if (send) {history.back(); return false;} else return false; }
											formObj.submit();
										}
									  ";

                    $replyBox .= "</script>";

                    /*
                    $submitButton = "<input name='submit2' type='submit' class='formbutton' value='{$button_send}'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" {$submitScript} />
                                            <input type='hidden' name='CampusMailID' value=".$this->CampusMailID.">";
                    */

                    //$submitButton = "<input name='submit2' type='submit' class='formbutton' value='{$button_send}' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" {$submitScript} />";
                    $submitButton = "<input name='submit2' type='submit' class='formbutton' value='{$button_send}' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" {$submitScript} />
					<input type='hidden' name='CampusMailID' value=".$this->CampusMailID.">";

                    global $viewtype;
                    if ($viewtype != "")
                    {
                        $submitButton .= "\n<input type='hidden' name='viewtype' value=\"$viewtype\">";
                    }
                    $noticeImage = $i_frontpage_campusmail_icon_notification2_2007;
                }
                else
                {
                    //$replyBox = "<table border='0' width='100%'><tr><td height='30' >".intranet_wordwrap($this->MessageReply,40,"\n",1)."</td></tr></table>\n";
                    $replyBox = $linterface->GET_TEXTAREA('Message', intranet_wordwrap($this->MessageReply,40,"\n",1), 60, 5, "", 1);
                    $submitButton = "";
                    $noticeImage = "";
                }

                global $PATH_WRT_ROOT;
                $replyBox .= "\n<script type=\"text/javascript\" src=\"{$PATH_WRT_ROOT}templates/jquery/jquery.autogrowtextarea.js\"></script>\n";
                $replyBox .= "<script language=\"javascript\" >\n";
                $replyBox .= "$(document).ready(function (){ $('#Message').autoGrow(); });\n";
                $replyBox .= "</script>\n";

                $instruction_row = ($instruction ==""?"":"<tr><td align=center><table width='400' border='1' cellpadding='10' cellspacing='0' bordercolorlight='#FBFCF0' bordercolordark='#F7CF73' class='body' ><tr><td><span id='instruction$instruction_id'>$instruction</span></td></tr></table></td></tr>\n");

                $x .= " 	<form name='form1a' action='inbox_update.php' method='post' >
							<table width='100%' border='0' cellspacing='0' cellpadding='8' >

						  	<tr>
								<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
                          	</tr>
                          	<tr>
                            	<td align='center'>
                            	<table width='80%' border='0' cellpadding='2' cellspacing='0' >
                              	<tr>
                                	<td valign='top'>{$noticeImage}</td>
                                	<td class='iMailReadnotification'><span id='instruction$instruction_id'>$instruction</span></td>
                                	<td>&nbsp;</td>
                              	</tr>
                              	<tr>
                              		<td colspan='2' >
                              		<table width='100%' border='0' cellpadding='0' cellspacing='0' >
                              		<tr>
	                              		<td width='15%' valign='top' class='tabletext'><strong><span id='campusmail_reply'>$i_frontpage_campusmail_reply</span> :</strong></td>
	                                	<td align='left' >
	                                	{$checkBox}
	                                	{$replyBox} <br />
	                                	{$submitButton}
	                                	</td>
                              		</tr>
                            		</table>
                              		</td>
                              	</tr>
                            	</table>
                              	</td>
							</tr>
                        	</table>
							</form>
               			";

                if ($this->isAutoReply())
                {
                    $sql = "UPDATE INTRANET_CAMPUSMAIL_REPLY SET IsRead = '1' WHERE CampusMailID = '".$this->CampusMailFromID."' AND UserID = '".$this->UserID."'";
                    $this->db_db_query($sql);
                }
            }
            else
            {
                $sql = "UPDATE INTRANET_CAMPUSMAIL SET IsNotification = '0' WHERE CampusMailID = '".$this->CampusMailID."'";
                $this->db_db_query($sql);
            }
        }
        $sql = "UPDATE INTRANET_CAMPUSMAIL SET RecordStatus = '1' WHERE (RecordStatus IS NULL OR RecordStatus NOT IN (1,2)) AND CampusMailID = '".$this->CampusMailID."' AND UserID = '".$this->UserID."'";
        $this->db_db_query($sql);
        return $x;
    }


    function returniMailSender()
    {
        if ($this->isEmail())
        {
            return $this->convert2mailto($this->parseEmailAddress($this->SenderEmail));
        }
        else
        {
            $username_field = getNameFieldWithClassNumberByLang();
            global $i_general_sysadmin, $Lang, $i_UserLogin;
            if ($this->SenderID == 0) return $i_general_sysadmin;
            $sql = "SELECT $username_field, UserLogin FROM INTRANET_USER WHERE UserID = '".$this->SenderID."'";
            $row = $this->returnArray($sql,1);

            $x = $row[0][0];
            $returnUserLogin = $row[0]['UserLogin'];
            //return ($x == "") ? "$i_general_sysadmin" : $x ;
            // return ($x == "") ? "<b><i>".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."</i></b>" : $x ;
            return (count($row) == 0) ? "<b><i>".$Lang['iMail']['FieldTitle']['PersonLeaveSchool']."</i></b>" : (trim($x) == '' ? $i_UserLogin.' : '.$returnUserLogin : $x);
        }
    }

    function convert2mailto($email_parts)
    {
        global $SYS_CONFIG,$PATH_WRT_ROOT;

        list($email_part,$name_part) = $email_parts;

        if($SYS_CONFIG['Mail']['hide_imail']===true)
        {
            if ($name_part != "")
                $x = "<a href='javascript:void(0)' onclick='parent.opener.window.location=\"".$PATH_WRT_ROOT."home/imail_gamma/compose_email.php?person=".urlencode($name_part)."&to=".urlencode($email_part)."\"; parent.opener.window.focus();' title=$email_part>$name_part &lt;{$email_part}&gt;</a>";
            else
                $x = "<a href='javascript:void(0)' onclick='parent.opener.window.location=\"".$PATH_WRT_ROOT."home/imail_gamma/compose_email.php?to=$email_part\"; parent.opener.window.focus();' title=$email_part>$email_part</a>";
        }
        else
        {
            if ($name_part != "")
                $x = "<a href=compose.php?targetname=".urlencode($name_part)."&targetemail=".urlencode($email_part)." title=$email_part>$name_part &lt;{$email_part}&gt;</a>";
            else
                $x = "<a href=compose.php?targetemail=$email_part title=$email_part>$email_part</a>";
        }
        return $x;
    }

    function convertEmailToiMailCompose($text)
    {
        //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a href='compose.php?targetemail=\\0'>\\0</a>", $text);
        // replaced above regex with the below one on 2012-02-27
        // only replace email addresses that after space or after end tag, and prevent replace this pattern e.g. reply.php?to=bl@abc.com
        $text = preg_replace("/([\s|>|;])([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))/","\\1<a href='compose.php?targetemail=\\2'>\\2</a>",$text);
        return $text;
    }


    function countExternalRecipients($recipientString)
    {
        $recipientString = trim($recipientString);
        if ($recipientString == "") return "";

        $insideQuote = false;
        $array = array();
        $target_string = "";
        for ($i=0; $i<strlen($recipientString); $i++)
        {
            $target_char = $recipientString[$i];
            if ($target_char == ",")
            {
                if ($insideQuote)
                {
                    $target_string .= $target_char;
                }
                else
                {
                    $array[] = $target_string;
                    $target_string = "";
                }
            }
            elseif ($target_char == "\"")
            {
                if ($insideQuote)
                {
                    $insideQuote = false;
                }
                else $insideQuote = true;
            }
            else
            {
                $target_string .= $target_char;
            }
        }
        $array[] = $target_string;

        return count($array);
    }

    function displayMailPrintVersion()
    {
        global $image_path, $i_frontpage_campusmail_icon_important, $i_frontpage_campusmail_date, $i_frontpage_campusmail_subject, $i_frontpage_campusmail_sender, $i_frontpage_campusmail_message, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_recipients;
        global $i_CampusMail_New_InternalRecipients,$i_CampusMail_New_ExternalRecipients
               ,$i_CampusMail_New_To,$i_CampusMail_New_CC,$i_CampusMail_New_BCC
               ,$i_CampusMail_New_Reply,$i_CampusMail_New_ReplyAll,$i_CampusMail_New_Forward;
        global $view_remove_html,$i_CampusMail_New_ViewFormat_PlainText,$i_CampusMail_New_ViewFormat_HTML,$button_remove;
        global $i_CampusMail_New_ViewFormat_MessageSource,$i_CampusMail_New_No_Sendmail, $i_CampusMail_New_Show_All_Recipient;
        global $intranet_session_language;
        global $nextID,$prevID, $LAYOUT_SKIN;
        global $special_feature, $Lang, $PATH_WRT_ROOT, $linterface;

        $x = "";

        if($this->UserID<>"")
        {
            $x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","print_button").'</td>
						</tr>
					</table>';
            $x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' > \n
            		<tr> \n
              			<td colspan='2' > \n
                		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' > \n
					";

            $x .= "<tr> \n
						<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle'>$i_frontpage_campusmail_date</span></td>\n
			            <td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle'> :</span></td> \n
			            <td class='tabletext' >".$this->DateModified."</td> \n
					</tr> \n
			      	<tr>\n
			        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='sender' >$i_frontpage_campusmail_sender</span></td>\n
			        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
			        	<td class='formfieldtitle' ><span class='tabletext'>".$this->returniMailSender()."</span></td>\n
			      	</tr>\n";

            ## Get recipients
            $int_to = $this->returnIntToRecipients();
            $int_to_short = $this->returnIntToRecipients_Short();
            $int_cc = $this->returnIntCCRecipients();
            $int_cc_short = $this->returnIntCCRecipients_Short();
            $int_bcc = $this->returnIntBCCRecipients();
            $int_bcc_short = $this->returnIntBCCRecipients_Short();
            $ext_to = $this->returnExtToRecipients();
            $ext_to_short = $this->returnExtToRecipients_short();
            $ext_cc = $this->returnExtCCRecipients();
            $ext_cc_short = $this->returnExtCCRecipients_short();
            $ext_bcc = $this->returnExtBCCRecipients();
            $ext_bcc_short = $this->returnExtBCCRecipients_short();

            $int_to_num = count($this->returnRecipientOption($this->RecipientID));
            $int_cc_num = count($this->returnRecipientOption($this->InternalCC));
            $int_bcc_num = count($this->returnRecipientOption($this->InternalBCC));

            $ex_to_num = $this->countExternalRecipients($this->ExternalTo);
            $ex_cc_num = $this->countExternalRecipients($this->ExternalCC);
            $ex_bcc_num = $this->countExternalRecipients($this->ExternalBCC);

            ## Internal (has internal recipients)
            if ($int_to!="" || $int_cc!="" || $int_bcc!="")
            {
                $recipient_table = "<table width='100%' border='0' cellspacing='1' cellpadding='1' class='tabletext' >";
                if ($int_to != "")
                {
                    $recipient_table .= "<tr><td width='30' ><i><span id='int_new_to'>$i_CampusMail_New_To</span>:</i></td><td>$int_to</td></tr>\n";
                }
                if ($int_cc != "")
                {
                    $recipient_table .= "<tr><td width='30' ><i><span id='int_new_cc'>$i_CampusMail_New_CC</span>:</i></td><td>$int_cc</td></tr>\n";
                }
                if ($int_bcc != "")
                {
                    $recipient_table .= "<tr><td width=30><i><span id='int_new_bcc'>$i_CampusMail_New_BCC</span>:</i></td><td>$int_bcc</td></tr>\n";
                }
                $recipient_table .= "</table>";

                $x .= "
                      	<tr>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='int_recipient' >$i_CampusMail_New_InternalRecipients</span></td>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
                        	<td class='formfieldtitle' ><span class='tabletext'>".$recipient_table."</span></td>\n
                      	</tr>\n
            			";
            }

            ## External (has external recipients)
            if ($ext_to!=""||$ext_cc!=""||$ext_bcc!="")
            {
                $recipient_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>";
                if ($ext_to != "")
                {
                    $recipient_table .= "<tr><td width=30><I><span id='ext_new_to'>$i_CampusMail_New_To</span>:</I></td><td>$ext_to</td></tr>\n";
                }
                if ($ext_cc != "")
                {
                    $recipient_table .= "<tr><td width=30><I><span id='ext_new_cc'>$i_CampusMail_New_CC</span>:</I></td><td>$ext_cc</td></tr>\n";
                }
                if ($ext_bcc != "")
                {
                    $recipient_table .= "<tr><td width=30><I><span id='ext_new_bcc'>$i_CampusMail_New_BCC</span>:</I></td><td>$ext_bcc</td></tr>\n";
                }
                $recipient_table .= "</table>";
                $x .= "
                      	<tr>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='ext_recipient' >$i_CampusMail_New_ExternalRecipients</span></td>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
                        	<td class='formfieldtitle' ><span class='tabletext'>".$recipient_table."</span></td>\n
                      	</tr>\n
            			";
            }

            $x .= "
                      	<tr>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='subject' >$i_frontpage_campusmail_subject</span></td>\n
                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
                        	<td class='formfieldtitle' ><span class='tabletext'>".intranet_wordwrap(str_replace(array('<','>'),array('&lt;','&gt;'),$this->Subject),50,"\n",1)."</span></td>\n
                      	</tr>\n
					";
            if ($this->IsAttachment)
            {
                $x .= "
	                      	<tr>\n
	                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' id='attachment' >$i_frontpage_campusmail_attachment</span></td>\n
	                        	<td align='left' class='formfieldtitle' ><span class='iMailComposefieldtitle' >:</span></td>\n
	                        	<td class='formfieldtitle' ><span class='tabletext'>".$this->displayiMailAttachment()."</span></td>\n
	                      	</tr>\n
	            		";
            }
            $x .= "
                        </table> \n
                        </td> \n
                      </tr> \n
					";

            $this->Message = $this->view_image_embed_message($this->Message);



            $html_flag = ($this->isHTMLMessage($this->Message) || $this->isHTML == 1);

            if ($html_flag)
            {
                $display_message = $this->Message;
                $display_message = $this->smartstripslashes($display_message);
                //$display_message = intranet_undo_htmlspecialchars(stripslashes($display_message));
                if ($view_remove_html == 1)
                {
                    $display_message = "<pre>".($this->removeHTMLtags($display_message))."</pre>";
                }
                else
                {
                    #$display_message = $this->convertCodedString($display_message);
                    //$display_message = preg_replace_callback('@'.$LinkExp.'@i',$Replace_Link,$display_message);
                    $display_message = $this->convertEmailToiMailCompose($display_message);
                }
            }
            else
            {
                $text = $this->removeHTMLtags($this->Message);
                $text = nl2br($text); #nl2br(intranet_htmlspecialchars($text));
                $text = $this->convertURLS2($text);
                $text = $this->convertEmailToiMailCompose($text);
                #$text = $this->convertMail2($text);
                $display_message = $text;
                $display_message = $this->smartstripslashes($display_message);
                //$display_message = intranet_undo_htmlspecialchars(stripslashes($display_message));
            }

            $x .= "
					<tr>
						<td colspan='2' >
						<table width='100%' border='0' cellspacing='0' cellpadding='10' >
						<tr>
							<td>
							<p class='style1'>{$display_message}</td>
						</tr>
						</table>
						</td>
					</tr>
					";
            $x .= "</table>";

        }
        return $x;
    }
}

?>