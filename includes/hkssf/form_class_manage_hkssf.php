<?php

class form_class_manage_hkssf extends libdb {
	function form_class_manage_hkssf() {
		$this->libdb();
	}
	
	function Get_Student_By_Class($YearClassIDArr) {
	
		if ($YearClassIDArr != '')
		{
			if (!is_array($YearClassIDArr))
				$YearClassIDArr = array($YearClassIDArr);
				$cond_YearClassID = " And yc.YearClassID In (".implode(',', $YearClassIDArr).") ";
		}
	
		$NameField = getNameFieldByLang2('u.');
	
		$sql = "SELECT
                    ycu.UserID,
                    ycu.ClassNumber,
                    $NameField as StudentName
                FROM YEAR_CLASS_USER as ycu
                INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
                INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
                LEFT JOIN INTRANET_USER as u ON (ycu.UserID = u.UserID)
                LEFT JOIN INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID)
                WHERE
                    au.UserID IS NULL AND
                    u.RecordStatus <> 3
                    $cond_YearClassID
                ORDER BY
                  y.Sequence, yc.Sequence, ycu.ClassNumber";
		return $this->returnArray($sql);
	}
	
	/**
 	* en_name, zh_name, gender, date_of_birth, level_id, identity
 	* 
 	* @param $uids
 	* @return $array
 	* 
 	* **/
	function HKSSF_fetchStudentsInfo($uids)
	{
        $uids = (is_array($uids)) ? $uids : array($uids);
        $cond = 'WHERE u.RecordStatus <> 3 AND u.UserID IN ( ';
        foreach($uids as $uid)
        {
            $cond .= $this->pack_value($uid,'int').', ';
        }
        $cond = rtrim($cond,', ');
        $cond .= ')';

        $sql = "SELECT
                    u.UserID as eclass_id,
                    EnglishName as en_name,
                    ChineseName as zh_name,
                    Gender as gender,
                    IF(DATE_FORMAT(DateOfBirth,'%Y') = 0, '', DATE_FORMAT(DateOfBirth,'%Y-%m-%d')) as date_of_birth,
                    CASE 
                        WHEN y.WEBSAMSCode IS NULL THEN y.YearName
                        WHEN y.WEBSAMSCode = 'NA' THEN y.YearName
                        WHEN y.WEBSAMSCode = '' THEN y.YearName
                        ELSE y.WEBSAMSCode 
                    END as level_id,
			        HKID as identity
			    FROM INTRANET_USER as u
                INNER JOIN YEAR_CLASS_USER as ycu on ycu.UserID = u.UserID
                INNER JOIN YEAR_CLASS as yc On ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = ".Get_Current_Academic_Year_ID()."
                INNER JOIN YEAR as y On yc.YearID = y.YearID
			    $cond ";
		return $this->returnArray($sql,null,1);
	}

    /**
     * @param $uids
     * @return array
     */
    function HKSSF_fetchStudentsPhoto($uids)
    {
        global $intranet_root, $PATH_WRT_ROOT;

        include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
        include_once($PATH_WRT_ROOT."includes/SimpleImage.php");

        $lf = new libfilesystem();
        $simple_image = new SimpleImage();

        $best_width = 412; // px
        $best_height = 459; // px

        // create folder for resize photo
        $tmp_dir = $intranet_root."/file/tmp_resize_user_photo";
        if (!file_exists($tmp_dir)) {
            $lf->folder_new($tmp_dir);
        }

        $uids = (is_array($uids)) ? $uids : array($uids);
        $cond = 'WHERE u.RecordStatus <> 3 AND u.UserID IN ( ';
        foreach($uids as $uid)
        {
            $cond .= $this->pack_value($uid,'int').', ';
        }
        $cond = rtrim($cond,', ');
        $cond .= ')';

        $sql = "SELECT 
                    u.UserID as eclass_id,
			        u.UserLogin
			    FROM
			        INTRANET_USER as u
                $cond ";
        $students = $this->returnArray($sql,null,1);

        // loop students
        foreach((array)$students as $key => $student)
        {
            $stu_user_login = $student['UserLogin'];
            unset($students[$key]['UserLogin']);

            # Official Photo
            $im_data = '';
            $user_photo_path = $intranet_root."/file/user_photo/".$stu_user_login.".jpg";
            if(file_exists($user_photo_path))
            {
                $simple_image->load($user_photo_path);
                $image_width = $simple_image->getWidth();
                $image_height = $simple_image->getHeight();
                $width_ratio = $image_width / (float)$best_width;
                $height_ratio = $image_height / (float)$best_height;

                // resize photo if too large
                $need_resize = $image_width > 0 && $image_height > 0 && $width_ratio > 1.0 && $height_ratio > 1.0;
                if($need_resize)
                {
                    $target_ratio = 1.0 / max($width_ratio, $height_ratio);
                    $target_width = (int)($target_ratio * $image_width);
                    $target_height = (int)($target_ratio * $image_height);
                    $simple_image->resize($target_width, $target_height);

                    $user_photo_path = $intranet_root."/file/tmp_resize_user_photo/".$stu_user_login.".jpg";
                    $simple_image->save($user_photo_path);
                }

                // encode photo content for json return
                $im = file_get_contents($user_photo_path);
                $im_data = base64_encode($im);

                // remove resize photo
                if($need_resize && file_exists($user_photo_path)) {
                    $lf->file_remove($user_photo_path);
                }
            }
            $students[$key]['im_data'] = $im_data;
        }

        return $students;
    }
}
?>