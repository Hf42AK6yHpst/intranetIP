<?php
# modifying By Paul

/********************** Change Log ***********************/
#	Date 	:	2017-02-15 [Paul]
#	Details	:	Allow teacher with no eClass Admin right to view teacher sharing files
#
#	Date 	:	2016-01-11 [Paul]
#	Details	:	Removed the strange line that reassigning the language file
#
#	Date 	:	2015-12-18 [Pun] [84012]
#	Details	:	Modified displayEClassDirectory12(), displayEClassDirectorySubject() added Subject to eClass classroom for directory
#
#	Date 	:	2015-10-19 [Paul]
#	Details	:	Modified GET_MODULE_OBJ_ARR(), Add new menu item "LfAssessmentReport" for LangFlow customization
#
#	Date	: 	2012-07-14 [Jason]
#	Details	: 	modified constructor(), GET_MODULE_OBJ_ARR()
#				added getModuleTitle(), seteClassRootPath(), seteClassMenuSetting(), checkeSpecialRoomtAccessPermission()
#				added eclass_folder, roomType, myeClass, eClassDirectory, eClassManagement, eClassSettings
#				eClassStatistics, teacherSharingArea, specialClassroom
#
#	Date 	:	2011-11-09 [Thomas]
#	Details	:	Modified displayEClassDirectorySubject(), Get Number of Distinct course_id instead of Number of Subject Group for display  

class libeclass2007 extends libeclass
{
	var $eclass_folder;
	var $roomType;
	var $myeClass;
	var $myChildreneClass;
	var $eClassDirectory;
	var $eClassManagement;
	var $eClassSettings;
	var $eClassStatistics;
	var $eClassAssessmentReport;
	var $eClassLfAssessmentReport;
	var $teacherSharingArea;
	var $specialClassroom;
	
	function libeclass2007($course_id="", $roomType=0, $specialClassroom=false)
	{
		$this->libeclass($course_id);
		
		$this->specialClassroom = $specialClassroom;
		$this->roomType = (strlen(trim($roomType)) > 0) ? (int)$roomType : '';
		$this->seteClassRootPath();
		$this->seteClassMenuSetting();
	}
	
	function seteClassRootPath(){
		if($this->specialClassroom == true){
			$this->eclass_folder = '/eclass2';
		} else {
			$this->eclass_folder = '/eclass';
		}
	}
	
	function seteClassMenuSetting(){
		if($this->specialClassroom == true){
			switch((string)$this->roomType){
				case '13': 
					# for Wong Hua San eLearning Project only
					$this->myeClass = 1;
					$this->myChildreneClass = 1;
					$this->eClassDirectory = 0;
					$this->eClassManagement = 1;
					$this->eClassSettings = 0;
					$this->eClassStatistics = 0;
					$this->eClassAssessmentReport = 0;
					$this->teacherSharingArea = 0;
					$this->eClassLfAssessmentReport = 0;
					break;
				default:
					$this->myeClass = 1;
					$this->myChildreneClass = 1;
					$this->eClassDirectory = 0;
					$this->eClassManagement = 0;
					$this->eClassSettings = 0;
					$this->eClassStatistics = 0;
					$this->eClassAssessmentReport = 0;
					$this->teacherSharingArea = 0;
					$this->eClassLfAssessmentReport = 1;
					break;
			}
		} else {
			# for normal classroom
			$this->myeClass = 1;
			$this->myChildreneClass = 1;
			$this->eClassDirectory = 1;
			$this->eClassManagement = 1;
			$this->eClassSettings = 1;
			$this->eClassStatistics = 1;
			$this->eClassAssessmentReport = 1;
			$this->teacherSharingArea = 1;
			$this->eClassLfAssessmentReport = 1;
		}
	}
     
     function displayUserEClass12($UserEmail,$main="")
     {
		global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
		global $i_status_suspended, $image_path, $LAYOUT_SKIN;
		
		$row = $this->returnEClassUserIDCourseIDStandard($UserEmail);

		if(sizeof($row)==0)
		{
			$x .= "
					<tr>
						<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
						<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
						<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
					</tr>
					";			
		}
		else
		{
			for($i=0; $i<sizeof($row); $i++)
			{
				$course_id = $row[$i][0];
				$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
				$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
				$user_id = $row[$i][3];
				$memberType = $row[$i][4];
				$lastused = $row[$i][5];
				$user_course_id = $row[$i][6];
				$status = $row[$i][7];
				
				$whatsnew = $this->displayEClassWhatIsNew($course_id, $user_id, $memberType);
				$whatsnew2 = $this->checkEClassWhatIsNew($course_id, $user_id, $memberType);
				
								
		        if ($whatsnew2)
		        {
		            $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class_new.gif\" >";
		        }
		        else
		        {			        
		            $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
		        }
		        if ($i%2==0)
		        {
			        $RowClass = " class=\"indextabwhiterow\" ";
		        } else {
			        $RowClass = " class=\"indextabbluerow\" ";
		        }

		        $x .= "<tr><td $RowClass >";
		        $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		        
		        if ($status == "suspended")
		        {
			        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
			        
			        $x .= "<tr>";
			        $x .= "<td width=\"19\">{$class_icon}</td>";
			        			        
					$x .= "<td wrap=true>$course_code - $course_name ($memberType - <i>$i_status_suspended</i>) </td>";																
					$x .= "</tr>";										        
		        } else {
			        $x .= "<tr>";
			        $x .= "<td width=\"19\">{$class_icon}</td>";
					$x .= "<td wrap=true><a href=\"javascript:fe_eclass('{$user_course_id}')\" class=\"indextabclasslist\">$course_code - $course_name ($memberType) </a></td>";																
					$x .= "</tr>";										        			        
		        }
		        $x .= "<tr>";
		        $x .= "<td >&nbsp;</td>";
				$x .= "<td >{$whatsnew}</td>";																
				$x .= "</tr>";										        
				
		        $x .= "</table> \n";
		        $x .= "</td></tr> \n";
				
			}
		}
		return $x;
     }
     
	
	function checkEClassWhatIsNew($course_id, $user_id, $memberType)
	{
		$count_notes = $this->returnNotesCount($course_id, $user_id,$memberType);
		$count_announcement = $this->returnAnnouncementCount($course_id, $user_id);
		$count_links = $this->returnLinksCount($course_id, $user_id);
		$count_bulletin = $this->returnBulletinCount($course_id, $user_id, $memberType);
		
		if (($count_notes == 0) && ($count_announcement==0) && ($count_links==0) && ($count_bulletin==0))
		{
			return false;
		} else {
			return true;		
		}
	}
     
	function displayEClassWhatIsNew($course_id, $user_id, $memberType, $DisplayType=0)
	{
		if($DisplayType)
                	$x .= "<table border='0' cellspacing='0' cellpadding='6'>";
		else
			$x .= "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" >";
		$x .= "<tr>";
		$x .= "<td align=\"center\" >".$this->showNotes($course_id, $user_id,$memberType)."</td>";
		$x .= "<td align=\"center\" >".$this->showAssessment($course_id, $user_id, $memberType)."</td>";
		$x .= "<td align=\"center\" >".$this->showBulletin($course_id, $user_id, $memberType)."</td>";
		$x .= "<td align=\"center\" >".$this->showAnnouncement($course_id, $user_id)."</td>";
		//$x .= "<td align=\"center\" >".$this->showLinks($course_id, $user_id)."</td>";		
		$x .= "</tr>";
		$x .= "</table>";
																
		return $x;
	}
	
	
	function showNotes($course_id, $user_id, $memberType)
	{
		global $i_frontpage_notes, $eclass_filepath, $image_path, $LAYOUT_SKIN;
		
		$f = new libgroups($this->db_prefix."c$course_id");
		if ($memberType=="G" || $memberType=="S")
		{
			$notes_id_blocked = $f->returnFunctionIDs("NOTE",$user_id);
		
			include_once("$eclass_filepath/src/includes/php/lib-notes.php");
			
			$ln = new notes($course_id);
			$ln->memberType = $memberType;
			$ln->notes2($notes_id_blocked);
			$count = 0;
			for ($i=0; $i<sizeof($ln->documents); $i++)
			{
				$readflag = $ln->documents[$i][8];
				if (!strstr($readflag, ";".$user_id.";"))
				{
					$count ++;
				}
			}
		} 
		else
		{
			$sql = "SELECT COUNT(*) FROM ".$this->db_prefix."c".$course_id.".notes WHERE status = '1' AND (readflag is null OR readflag not like '%;".$user_id.";%') ";		
			$row = $this->returnVector($sql);
			$count = (int) $row[0];
		}
		if ($count != 0)
		{		 	
		 	$x  = "<span class=\"indextabclassicon\" >\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_econtent.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_notes'>{$count} \n";
			$x .= "</span>\n";
		} else {
		 	$x  = "<span class=\"indextabclassiconoff\" >\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_econtent_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_notes'>{$count} \n";
			$x .= "</span>\n";
		}
		
		return $x;
     }
	   
	   # new function - eclass40, added by kit
     function showAssessment($course_id, $user_id, $memberType)
     {
          global $i_frontpage_assessment, $eclass40_filepath, $image_path, $LAYOUT_SKIN;
          
          $count = 0;
          /*
          if ($memberType=="G" || $memberType=="S")
          {            
            $cfg_eclass_lib_path = $eclass40_filepath."/src/includes/php";
            include_once("$eclass40_filepath/src/includes/php/lib-assessment.php");
            $lass = new assessment($course_id);
            $AssCount =  $lass->Get_Assessment_Status_Count($user_id);
            $count = $AssCount[$user_id]['NotCompletedNotSubmitted'];
          }
          */
          if ($count != 0)
          {              
    		 		$x  = "<span class=\"indextabclassicon\" >\n";
    				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_assessment.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_assessment'>{$count} \n";
    				$x .= "</span>\n";              
          } else {
    		 		$x  = "<span class=\"indextabclassiconoff\" >\n";
    				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_assessment_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_assessment'>{$count} \n";
    				$x .= "</span>\n";              	          
          }
          
          return $x;
     }
     	
     function showAnnouncement($course_id, $user_id)
     {
          global $i_frontpage_announcement, $image_path, $LAYOUT_SKIN;
          
          $sql = "SELECT count(announcement_id) FROM ".$this->db_prefix."c".$course_id.".announcement WHERE (readflag is null OR readflag not like '%;$user_id;%') AND ((now() BETWEEN date_start AND date_end) OR  (date_start IS NULL AND date_end IS NULL))";
          $counta = $this->returnArray($sql,1);
          $count = $counta[0][0];
          if ($count != 0)
          {              
		 		$x  = "<span class=\"indextabclassicon\" >\n";
				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_annou.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_announcement'>{$count} \n";
				$x .= "</span>\n";              
          } else {
		 		$x  = "<span class=\"indextabclassiconoff\" >\n";
				$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_annou_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_announcement'>{$count} \n";
				$x .= "</span>\n";              	          
          }
          
          return $x;
     }
 
     function showLinks($course_id, $user_id)
     {
		global $i_frontpage_links, $image_path, $LAYOUT_SKIN;
		
		$sql = "SELECT count(weblink_id) FROM ".$this->db_prefix."c".$course_id.".weblink WHERE (readflag is null OR readflag not like '%;$user_id;%')";
		$counta = $this->returnArray($sql,1);
		$count = $counta[0][0];
		if ($count != 0)
		{              
			$x  = "<span class=\"indextabclassicon\" >\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_links'>{$count} \n";
			$x .= "</span>\n";		
		} else {
			$x  = "<span class=\"indextabclassiconoff\" >\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_link_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_links'>{$count} \n";
			$x .= "</span>\n";		
		}
          
		return $x;
     }
   
     function showBulletin($course_id, $user_id, $memberType)
     {
		global $i_frontpage_bulletin, $image_path, $LAYOUT_SKIN;
		
		$f = new libgroups($this->db_prefix."c$course_id");
		$sql  = "SELECT count(bulletin_id) FROM ".$this->db_prefix."c".$course_id.".bulletin WHERE (readflag is null OR readflag not like '%;$user_id;%')";
		# Eric Yip (20090824): New filtering condition in eClass40
		$sql  .= "and IsDraft = '0' and IsTemp = '0'";
		$sql .= ($memberType=="G" || $memberType=="S") ? " AND forum_id NOT IN (".$f->returnFunctionIDs("B",$user_id).")" : "";
		$counta = $this->returnArray($sql,1);
		$count = $counta[0][0];
		
		if ($count != 0)
		{			
			$x  = "<span class=\"indextabclassicon\" >\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_bulletin'>{$count} \n";
			$x .= "</span>\n";					
		} else {
			$x  = "<span class=\"indextabclassiconoff\" >\n";
			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_bulletin'>{$count} \n";
			$x .= "</span>\n";					
		}
		
		return $x;
     }
     
    function GET_MODULE_OBJ_ARR()
    {
        global $plugin, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path;
        ### wordings
        global $ip20TopMenu, $i_adminmenu_eclass, $i_eClass_Admin_AssessmentReport, $i_frontpage_menu_eclass_mgt, $i_frontpage_eclass_eclass_dir, $i_frontpage_eclass_eclass_my, $i_eClass_Admin_Settings, $i_eClass_Admin_Stats, $i_eClass_Admin_Individual, $i_eClass_Admin_Shared_Files, $i_frontpage_eclass_eclass_children;
                        
        global $UserID, $CurrentPageArr, $intranet_root, $eclass40_httppath, $sys_custom, $intranet_session_language, $Lang;
        
        # Check user access right (eClass Management)
        include_once($PATH_WRT_ROOT."includes/libuser.php");
        include_once($PATH_WRT_ROOT."includes/libaccess.php");
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
        $lu = new libuser($UserID);
        $la = new libaccess($UserID);
        $la->retrieveAccessEClass();
        
        $params = '?roomType='.$this->roomType;
        if($this->roomType == 13){
        	$CurrentPageArr['WWSeLearningProject'] = 1;
        } else {
        	$CurrentPageArr['eClass'] = 1;
        	$params = '';
        }
        
        # Current Page Information init
        $PageDir	= 0;
        $PageManagement = 0;
        $PageMyeClass	= 0;
        $PageMyChildreneClass = 0;		
        $PageSettings	= 0;
        $PageStatistics	= 0;
        $PageShareArea	= 0;
        $PageAssessmentReportRank = 0;
        $PageAssessmentReportProgress = 0;
        
        switch ($CurrentPage) {
        	case "PageMyeClass":
            	$PageMyeClass = 1;
            	break;
        	case "PageMyChildreneClass":
            	$PageMyChildreneClass = 1;
            	break;				
	        case "PageDir":
            	$PageDir = 1;
            	break;
            case "PageManagement":
            	$PageManagement = 1;
            	break;
          	case "PageSettings":
          		$PageSettings = 1;
          		break;
          	case "PageStatistics":
          		$PageStatistics = 1;
         		 break;
	       	case "PageShareArea":
          		$PageShareArea = 1;
          		break;
			case "PageAssessmentReport":
				$PageAssessmentReport = 1;
          		break;
          	case "PageAssessmentReportRank":
          		$PageAssessmentReportRank = 1;
          		break;
          	case "PageAssessmentReportProgress":
          		$PageAssessmentReportProgress = 1;
          		break;		
        }

        # Menu information
        if($this->myeClass)
        	$MenuArr["MyeClass"] = array($i_frontpage_eclass_eclass_my, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/index.php".$params, $PageMyeClass, "$image_path/$LAYOUT_SKIN/eclass/icon_myeclass.gif");
   		
		if($this->myChildreneClass && $lu->RecordType==3)
        	$MenuArr["MyChildreneClass"] = array($i_frontpage_eclass_eclass_children, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/children.php".$params, $PageMyChildreneClass, "$image_path/$LAYOUT_SKIN/eclass/icon_myeclass.gif");			
			         
        if($this->eClassDirectory)
        	$MenuArr["Directory"] = array($i_frontpage_eclass_eclass_dir, $PATH_WRT_ROOT."home/eLearning/eclass/directory.php", $PageDir, "$image_path/$LAYOUT_SKIN/eclass/icon_directory.gif");

        # Eric Yip (20090714): add session checking from role setting
        //is teacher && Setting Allow teacher to manage 
        //OR is Admin in eLearning-eClass
  		if($this->eClassManagement && (($lu->RecordType==1 && $la->isAccessEClassMgt()) || $_SESSION['SSV_USER_ACCESS']['eLearning-eClass']))
        	$MenuArr["Management"] = array($i_frontpage_menu_eclass_mgt, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/organize/".$params, $PageManagement, "$image_path/$LAYOUT_SKIN/eclass/icon_management.gif");
        
        if($this->eClassSettings && $_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
          	$MenuArr["Settings"] = array($i_eClass_Admin_Settings, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/settings/", $PageSettings, "$image_path/$LAYOUT_SKIN/eclass/icon_preference.gif");
        
        if($this->eClassStatistics && $_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
          	$MenuArr["Statistics"] = array($i_eClass_Admin_Stats, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/stats/usage_files/", $PageStatistics, "$image_path/$LAYOUT_SKIN/eclass/icon_statistics.gif");
     
	if($this->eClassAssessmentReport && $_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] && $sys_custom['eClass']['individualAssessmentReport'] )
          	$MenuArr["AssessmentReport"] = array($i_eClass_Admin_AssessmentReport, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/assessment_report/markbook.php", $PageIndividual, "$image_path/$LAYOUT_SKIN/eclass/icon_statistics.gif");
      
      	//  if($this->eClassLfAssessmentReport  && $sys_custom['LangFlow_eClass_Assessment_Report']){ // allowing teacher and admin to view report
        if($this->eClassLfAssessmentReport && ($lu->RecordType==2 && $lu->RecordStatus==1) && $sys_custom['LangFlow_eClass_Assessment_Report']){ //allowing only student to view report
        		$MenuArr["LfAssessmentReport"] = array($i_eClass_Admin_AssessmentReport, "", $PageAssessmentReport, "$image_path/$LAYOUT_SKIN/eclass/icon_statistics.gif");
				$MenuArr["LfAssessmentReport"]["Child"]["Ranking"] = array($Lang['eClass']['LfAssessmentReport']['Ranking'], $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/assessment_report_langflow/rank/weekly.php", $PageAssessmentReportRank);
				$MenuArr["LfAssessmentReport"]["Child"]["LearningProgress"] = array($Lang['eClass']['LfAssessmentReport']['Progress'], $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/assessment_report_langflow/progress/week.php", $PageAssessmentReportProgress);
        }
	
        if(($this->teacherSharingArea && $_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])||($sys_custom['project']['NCS']==true && $_SESSION['UserType']==1 && $_SESSION['isTeaching']==1))                
          	$MenuArr["ShareArea"] = array($i_eClass_Admin_Shared_Files, $PATH_WRT_ROOT."home/eLearning".$this->eclass_folder."/share_files/", $PageShareArea, "$image_path/$LAYOUT_SKIN/eclass/icon_directory.gif");

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eClass";'."\n";
        $js.= '</script>'."\n";

        # module information
        $MODULE_OBJ['title'] = $this->getModuleTitle().$js;
        $MODULE_OBJ['title_css'] = "menu_opened";
        $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eclass.gif";
        //$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eLearning/eclass/index.php";
        $MODULE_OBJ['menu'] = $MenuArr;
		
        return $MODULE_OBJ;
    }
        
    function getModuleTitle(){
    	global $Lang, $ip20TopMenu;
    	$x = '';
    	switch($this->roomType){
    		case '13':	$x = $Lang['wws']['eLearningProject']; break;
    		default:	$x = $ip20TopMenu['eClass']; break;
    	}
    	return $x;
    }
	
        # /home/eclass/directory.php      
	function displayEClassDirectory12($directory)
        {
          global $courseSubjectCategory,$PATH_WRT_ROOT,$eclass_db,$Lang, $sys_custom;		  
         
			
          $x .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>\n";
          $x .= "<tr class='tablerow2'>\n";
		  $i = 0;
		  foreach($directory as $ckey=>$dir){
			 $x .= ($i%3==0) ? "</tr>\n<tr><td colspan='3' class='tablerow1' height='5'></td></tr>\n<tr class='tablerow2'>\n" : "";
			   $x .= "<td width='30%' class='tabletext' valign='top'>\n";
               $x .= "<p><b>".$dir['Category']['catName']."</b><br>\n";
              $x .= $this->displayEClassDirectorySubject($dir);
               $x .= "<br></td>\n";
			 $i++;
		  }

		  if($sys_custom['eClass']['EnableLinkToSubject']){
		  	$subjectIdSQL = 'AND
				(
					SubjectID is null
				or
					SubjectID = 0
				)';
		  }
		  
		   $sql = "select 
		   		count(*) 
		   from 
		   		{$eclass_db}.course 
		   where 
		   		RoomType='0' 
		   AND 
			   (
				   (
				   		SubjectGroupID is null 
				   or
				   		SubjectGroupID = '' 
				   or 
				   		SubjectGroupID = 0
				   )
				   {$subjectIdSQL}
			   )
		   ";
		   $result = $this->returnVector($sql);
		   
			$x .= ($i%3==0) ? "</tr>\n<tr><td colspan='3' class='tablerow1' height='5'></td></tr>\n<tr class='tablerow2'>\n" : "";
		    $x .= "<td width='30%' class='tabletext' valign='top'>\n";
			$x .= "<p><b><a href='directory.php?SubjectID=Unclassified&catID=Other' class='tablelink'>".$Lang['eclass']['directory']['notCategorized']." (".$result[0].")</a></b><br>\n";			
			$x .= "<br></td>\n";
			$i++;
			
          /*for($i=0; $i<sizeof($courseSubjectCategory); $i++){
               $cate_id = $courseSubjectCategory[$i][0];
               $cate_name = trim($courseSubjectCategory[$i][1]);
               $x .= ($i%3==0) ? "</tr>\n<tr><td colspan='3' class='tablerow1' height='5'></td></tr>\n<tr class='tablerow2'>\n" : "";
               $x .= "<td width='30%' class='tabletext' valign='top'>\n";
               $x .= "<p><b>$cate_name</b><br>\n";
               $x .= $this->displayEClassDirectorySubject($cate_id);
               $x .= "<br></td>\n";
          }*/

          for($i=$i%3; ($i<3 and $i>0); $i++)
          	$x .= "<td width='30%'></td>\n";
          
          $x .= "</tr>\n";
          $x .= "</table>\n";
          return $x;
     }        
     
    // function displayEClassDirectorySubject($CategoryID){
	function displayEClassDirectorySubject($Category){
          global $courseSubject, $image_path, $LAYOUT_SKIN, $sys_custom;
          
		  if (!empty($Category['Subject'])){
			  foreach($Category['Subject'] as $skey=>$subject){
				   $subj_id = $skey;
				   $subj_name = trim($subject['SubjectName']);
				   $cate_id = $Category['Category']['LearningCategoryID'];
				   
				   # Get Number of Distinct course_id instead of Number of Subject Group  
				   $current_course_id = array();
				   if(is_array($Category['SubjectGroup'][$skey]) && count($Category['SubjectGroup'][$skey])){
					   foreach($Category['SubjectGroup'][$skey] as $SubjectGroupID=>$SubjectGroup_ary){
					   		if(!in_array($SubjectGroup_ary['course_id'], $current_course_id)){
					   			$current_course_id[] = $SubjectGroup_ary['course_id'];
					   		}
					   }
				   }
				   
				   #### Get Number of Distinct course_id instead of Number of Subject START ####
				   if($sys_custom['eClass']['EnableLinkToSubject']){
					   foreach((array)$Category['SubjectLink'][$skey] as $SubjectID=>$courseId){
					   		if(!in_array($courseId, $current_course_id)){
					   			$current_course_id[] = $courseId;
					   		}
					   }
				   }
				   #### Get Number of Distinct course_id instead of Number of Subject END ####
				   
				   //$num = sizeof($Category['SubjectGroup'][$skey]);
				   $num = count($current_course_id);
				   $x .= "<img src='{$image_path}/{$LAYOUT_SKIN}/dir_c.gif' border='0' hspace='5'>";
				   $x .= ($num==0) ? "$subj_name ($num)" : "<a href='directory.php?SubjectID={$subj_id}&catID={$cate_id}' class='tablelink'>$subj_name ($num)</a>";
				   $x .= "<br>\n";
			  }
		  }
		  
		  
          /*for($i=0; $i<sizeof($courseSubject); $i++){
               $subj_id = $courseSubject[$i][0];
               $subj_name = trim($courseSubject[$i][1]);
               $cate_id = $courseSubject[$i][2];
               $num = sizeof($this->eClassSubjectCourses($subj_id));
               if($cate_id==$CategoryID){
                    $x .= "<img src='{$image_path}/{$LAYOUT_SKIN}/dir_c.gif' border='0' hspace='5'>";
                    $x .= ($num==0) ? "$subj_name ($num)" : "<a href='directory.php?SubjectID=$subj_id' class='tablelink'>$subj_name ($num)</a>";
                    $x .= "<br>\n";
               }
          }*/
          return $x;
     }
     
	 function getEClassUserIDUserCourseID($course_id){
			global $eclass_db,$intranet_db;
          $this->db = $this->eclass_db;
          $db_name = $this->eclass_db; 
          $fieldname  = "a.course_id, a.course_code, a.course_name, a.course_desc, a.is_guest, a.no_users, a.rights_guest, ";
          $fieldname .= "b.user_id, b.memberType, b.lastused, b.user_course_id ,b.status, b.UserID ";
          $sql = "SELECT $fieldname FROM {$eclass_db}.course AS a Left join
					( select b.user_id, b.memberType, b.lastused, b.user_course_id, b.status, u.UserID,b.course_id				
					from 
						{$eclass_db}.user_course as b inner join {$intranet_db}.INTRANET_USER as u on
						b.user_email = u.UserEmail
						and UserID = {$_SESSION['UserID']}
					) AS b on
					a.course_id = b.course_id
		  WHERE (b.status is NULL OR b.status NOT IN ('deleted')) 
		  AND a.course_id in ('$course_id') 
		  GROUP BY a.course_id ORDER BY a.course_code, a.course_name";
          return $this->returnArray($sql);
     } 
	 	 
     function displayEClassDirectoryView12($SubjectID,$directory,$catID)
     {
          global $courseSubjectCategory, $courseSubject, $linterface, $image_path, $LAYOUT_SKIN;
          global $i_eClassCourseCode, $i_eClassCourseName, $i_eClassCourseDesc, $i_eClassIsGuest, $i_eClassNumUsers,$Lang;
          global $PATH_WRT_ROOT, $UserID;

					# Get a list of user course
					/*include_once($PATH_WRT_ROOT."includes/libuser.php");
					$lu = new libuser($UserID);
					$user_course_temp = $this->returnEClassUserIDCourseIDStandard($lu->UserEmail);*/
					include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
					$scm = new subject_class_mapping();
					$allcourses = Array();
					if ($SubjectID == 'Unclassified'){
						$allcourses = $scm->Get_Unclassified_CourseID();
					}
					else
						$allcourses = $scm->Get_Related_CourseID($SubjectID); 
					$all_course_id = implode("','",$allcourses); //echo $all_course_id;
					$user_course_temp = $this->getEClassUserIDUserCourseID($all_course_id);
					
					for($i=0; $i<count($user_course_temp); $i++)
					{
						$user_course[$user_course_temp[$i]['course_id']] = $user_course_temp[$i]['user_course_id'];
					}
				//	unset($user_course_temp); 
        //  if($SubjectID<>""){
          //     $row = $this->eClassSubjectCoursesVer2($allcourses);

		// ec3.0
		//$SubjectID = $this->getCourseSubjectIndex($courseSubject, $SubjectID);
		//$CategoryID = $this->getCourseSubjectCategoryIndex($courseSubjectCategory, $courseSubject[$SubjectID][2]);
		//$subj_name = trim($courseSubject[$SubjectID][1]);
		//$cate_name = trim($courseSubjectCategory[$CategoryID][1]);
		if ($catID != 'Other'){
			$cate_name = trim($directory[$catID]['Category']['catName']);
			$subj_name = trim($directory[$catID]['Subject'][$SubjectID]['SubjectName']);
		}
		// eof ec3.0
                
               $x .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>\n";
               $x .= "<tr>";
               $x .= "<td colspan='5'>". ($catID != 'Other'? $linterface->GET_NAVIGATION2($cate_name ." &gt; ". $subj_name) : $linterface->GET_NAVIGATION2($Lang['eclass']['directory']['notCategorized']))."</td>";
               $x .= "</tr>";
               $x .= "<tr>";
               $x .= "<td class='tablebluetop tabletopnolink' width='110'><b>$i_eClassCourseCode</b></td>\n";
               $x .= "<td class='tablebluetop tabletopnolink' width='207'><b>$i_eClassCourseName</b></td>\n";
               $x .= "<td class='tablebluetop tabletopnolink' width='150'><b>$i_eClassCourseDesc</b></td>\n";
               $x .= "<td class='tablebluetop tabletopnolink' width='120'><b>$i_eClassIsGuest</b></td>\n";
               $x .= "<td class='tablebluetop tabletopnolink' width='70'><b>$i_eClassNumUsers</b></td>\n";
               $x .= "</tr>";
			   $row = $user_course_temp; 
               for($i=0; $i<sizeof($row); $i++){
                    $course_id = $row[$i][0];
                    $course_code = $row[$i][1];
                    $course_name = intranet_wordwrap($row[$i][2],20,"\n",1);
                    $course_desc = ($row[$i][3]==""?"--":$row[$i][3]);
                    $is_guest = $row[$i][4];
                    $no_users = $row[$i][5];
					$existUserID =$row[$i]['UserID'];					
					$rights_guest = trim($row[$i][6]);		/*20091019: Sandy */
					
                    $x .= "<tr class='tablebluerow".($i%2==0?1:2)."'>\n";
                    $x .= "<td class='tabletext'>$course_code</td>\n";
                    # link to classroom if the user is a member of it
                    $temp = (is_array($user_course) && array_key_exists($course_id, $user_course)) ? "<td class='tabletext'><a href='javascript:fe_eclass(".$user_course[$course_id].")' class=\"tablelink\">$course_name</a></td>\n" : "<td class='tabletext'>$course_name</td>\n";
					//$x .= $temp;
					$x .= ($no_users==0||$existUserID!=$_SESSION['UserID']?"<td class='tabletext'>".$course_name."</td>": $temp);
                    $x .= "<td class='tabletext'>$course_desc</td>\n";
                    //$x .= ($is_guest=="yes") ? "<td class='tabletext'><a href='javascript:fe_eclass_guest($course_id)'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_guest_allow.gif' border='0'></a></td>\n" : "<td class='tabletext'>--</td>\n";
                    /*Sandy : 20091019*/
                    $x .= ($is_guest=="yes" && $rights_guest != "ec3.1") ? "<td class='tabletext'><a href='javascript:fe_eclass_guest($course_id)'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_guest_allow.gif' border='0'></a></td>\n" : "<td class='tabletext'>--</td>\n";
                    $x .= "<td class='tabletext'>$no_users</td>\n";
                    $x .= "</tr>\n";
               }
               $x .= "<tr><td class='dotline' colspan='5'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1' /></td></tr>";
               $x .= "</table><br />";
         // }
          return $x;
     }
     
     function displayUserEClass($UserEmail,$main="")
     {
		global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
		global $i_status_suspended, $image_path, $LAYOUT_SKIN;
		
		$row = $this->returnEClassUserIDCourseIDStandard($UserEmail);

		if(sizeof($row)==0)
		{
			$x .= "
					<tr>
						<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</td>
					</tr>
					";			
		}
		else
		{
                	$x .= "<tr>\n";
			for($i=0; $i<sizeof($row); $i++)
			{
				$course_id = $row[$i][0];
				$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
				$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
				$user_id = $row[$i][3];
				$memberType = $row[$i][4];
				$lastused = $row[$i][5];
				$user_course_id = $row[$i][6];
				$status = $row[$i][7]; 
				
				$whatsnew = $this->displayEClassWhatIsNew($course_id, $user_id, $memberType, 1);
				$whatsnew2 = $this->checkEClassWhatIsNew($course_id, $user_id, $memberType);
				
                                $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";
                                
                                if ($status == "suspended")
                                {
                                	$class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eclass/icon_eclass.gif\" >";
                                        $eClass_title = "$course_code - $course_name ($memberType - <i>$i_status_suspended</i>)";
				}
                                else
                                {
                                        if ($whatsnew2)
                		            $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eclass/icon_eclass_new.gif\" >";
                		        else
                		            $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eclass/icon_eclass.gif\" >";
                                            
					$eClass_title = "<a href=\"javascript:fe_eclass('{$user_course_id}')\" class=\"tabletoplink\">$course_code - $course_name ($memberType) </a>";
                                }
                                
                                $x .= "<td align='center' width='50%' valign='top'>". $this->MyeClassTable($class_icon, $eClass_title, $whatsnew, $lastlogin) ."</td>";
                                $x .= ($i%2==1) ? "</tr>\n<tr>\n" : "";
			}
                        $x .= "</tr>\n";
		}
                
		return $x;
     }
     
	function MyeClassTable($class_icon, $eClass_title, $whatsnew, $lastlogin)
        {
		global $image_path, $LAYOUT_SKIN;
                
		$x = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                $x .= "<tr>";
                $x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>";
                $x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
                $x .= "<tr>";
                $x .= "<td width='19'>$class_icon</td>";
                $x .= "<td>$eClass_title</td>";
                $x .= "</tr>";
                $x .= "</table></td>";
                $x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>";
                $x .= "</tr>";
                $x .= "<tr>";
                $x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif' width='10' height='11'></td>";
                $x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
                $x .= "<tr>";
                $x .= "<td>";
                $x .= $whatsnew;
                $x .="</td>";
                $x .= "</tr>";
                $x .= "<tr>";
                $x .= "<td align='right' class='tabletext'>$lastlogin</td>";
                $x .= "</tr>";
                $x .= "</table></td>";
                $x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif' width='12' height='12'></td>";
                $x .= "</tr>";
                $x .= "<tr>";
                $x .= "<td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board07.gif' width='10' height='10'></td>";
                $x .= "<td height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif' width='10' height='10'></td>";
                $x .= "<td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif' width='12' height='10'></td>";
                $x .= "</tr>";
                $x .= "</table>";
                
                return $x;
        
        }
		
	function checkeSpecialRoomtAccessPermission($withMsg=true){
		global $plugin;
		if($this->roomType == '13' && $plugin['WWS_eLearningProject']){
			return true;
		} else {
			if($withMsg){
				echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
				echo '<tr><td colspan="100%" align="center">You don\'t have right to access this page.</td></tr>';
				echo '</table>';
			}
			return false; 
		}
	}
}
?>
