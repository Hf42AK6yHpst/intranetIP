<?php 

class libcurdlog extends libdb{
	
	public function addlog($module, $actionType, $tableName, $keyName, $keyValue, $before="", $after="", $query=""){
		global $UserID;
		if(strstr($query, "HashedPass")){
			$query = preg_replace("/(HashedPass=.+?,)/","", $query);
		}
		$sql = "INSERT INTO ACTION_LOG (Module, ActionType, AffectTable,TableKeyName,TableKeyValue, DataBefore, DataAfter, query, InputBy, InputDate)
				VALUES ('".$module."','".$actionType."','".$tableName."','".$keyName."','".$keyValue."','".$before."','".$after."','".addslashes($query)."','".$UserID."', NOW())";
		$logged = $this->db_db_query($sql);
		if(!$logged)debug_r($sql);
		return true;
	}
	
	public function updateLog($logID, $key, $value){
		$sql = "UPDATE ACTION_LOG SET ".$key."='".$value."' WHERE LogID='".$logID."'";
		$this->db_db_query($sql);
	}
	
	public function getLogSql($module=""){
		$Module = "";
		switch($module){
			case "user":
				$Module = "User Settings";
				break;
			case "group":
				$Module = "User Group";
				break;
			default:
				$module = "";
				break;
		}
		$sql = "SELECT Module, ActionType, REPLACE(DataAfter,';','<br>') as DataAfter, CONCAT(InputDate, '<br> By: ', INTRANET_USER.ChineseName, ' ', INTRANET_USER.EnglishName, '<br>(UserID: ', ACTION_LOG.InputBy,')') as input
				FROM ACTION_LOG
				INNER JOIN INTRANET_USER ON ACTION_LOG.InputBy=INTRANET_USER.UserID ";
		$sql .= ($module=="")?"":" WHERE Module='".$Module."'";
		return $sql;
	}
}
?>