<?php
# using:   anna

################ Change Log
#
#   Date:   2018-11-12 Cameron
#           - add function custDisplayUserEClass_CEM()
#
#	Date:	2017-12-18 Cameron [ip.2.5.9.1.1]
#			- add function custDisplayUserEClass_Oaks()
#	
#	Date:	2017-11-07 Paul	[ip.2.5.9.1.1]
#			- modified returnSurvey(), handle the situation of L&T
#
#	Date:	2017-09-18 Bill	[ip.2.5.8.10.1]		[2017-0908-1248-12235]
#			- add function getClassTeacherApplyLeaveTodayPendingRecord(), to get pending records of teaching classes only
#
#	Date:	2017-08-24 Paul
#			- add function classroom_list_appview()
#			- add function getClassLatestActivity()
#
#	Date:	2017-03-08 Bill [ip.2.5.8.4.1]		[2017-0306-0923-10236]
#			- modified getUnsignedCircularCount(), to apply old logic to unsigned circular number display (count circular not passed deadline only)	($sys_custom['eCircular']['IconUnsignedCountApplyOldLogic'])
#
#	Date:	2017-01-24 Paul [ip.2.5.8.3.1]
#			- modified returnEClassUserIDCourseID_CCEAP(), fix the order of classroom display
#
#	Date:	2016-12-01 Bill [ip.2.5.8.1.1]		[2016-1130-0910-56236]
#			- modified getUnsignedCircularCount(), to hide eCircular Icon in portal page if eCircular is disabled
#
#	Date:	2016-10-17 Bill [ip.2.5.7.10.1]		[2016-0928-0903-29236]
#			- modified getUnsignedCircularCount(), to fix returning incorrect number of unsigned circular
#
#	Date:	2016-09-19 Paul [ip.2.5.7.10.1]
#			- modified function custDisplayUserEClass_CCEAP(), EAPeClassTableDisplay(), EAPeClassTableNews() for Centennial App view
#
#	Date:	2016-09-13 Ivan [ip.2.5.7.10.1] [X103187]
#			- modified getParentNoticeUnsignedCount(), getStudentNoticeUnsignedCount() to cater notice start date end date with time improvement
#
#	Date:	2016-09-07 Paul
#			- modify function displayUseriTextbookClass() to handle session variables in PHP 5.4
#
#	Date:	2016-05-05 Paul
#			- add function custDisplayUserEClass_CCEAP()
#			- add function EAPeClassTableDisplay()
#			- add function getEAPeClassTableIconId()
#			- add function returnEClassUserIDCourseID_CCEAP()
#			- add function EAPeClassTableNews()
#
#	Date:	2015-12-09 Cameron
#			- modify returnEClassUserIDCourseIDStandard() to retrieve course_desc
#			- add function geteClassLogo()
#			- add function custDisplayUserEClass_Amway()
#
#	Date:	2015-09-24 Ivan [K86138] [ip.2.5.6.10.1.0]
#			modified function getApplyLeaveTodayPendingRecord() to get all pending records
#
#	Date:	2015-08-13	Omas
#			added custDisplayUserEClass_HKUGAC(), getSubjectClassMapping() for HKUGAC Portal Cust  
#
#	Date:	2015-01-21	Omas
#			Improved Repair system Logo's number including both processing and pending record
#
#	Date:	2014-08-07	Charles Ma
#			modify displayUseriTextbookClass() - pas for parent
#
#	Date:	2014-02-21	Ivan
#			added function getApplyLeaveTodayPendingRecord() to count apply leave record
#
#	Date:	2013-03-07	Thomas
#			Modified function getgetAssessmentStatusCount(), replace a slow query with 4 separate queries and array operations	
#
#	Date:	2012-11-30	YatWoon
#			update getStudentNoticeUnsignedCount(), getParentNoticeUnsignedCount()
#			count with late sign notice			
#
#	Date:	2012-11-05  Thomas
#			added function hasPoemsAndSongsClass() for poems and songs checking
#
#	Date:	2012-10-18	Ivan
#			added socument routing function getDrUserCurrentInvolvedEntryCount()
#
#	Date:	2012-09-05	YatWoon
#			update returnSurvey(), add GroupID checking (not null) to prevnet abnormal GroupID=NULL data
#
#	Date:	2012-08-21 (Siuwan)
#			add displayChildrenEClass() for parent viewing Children's eClass
#
#	Date:	2012-07-14 (Jason)
#			modified returnEClassUserIDCourseIDStandard(), displayUserEClass()
#
#	Date:	2012-04-25 (YatWoon)
#			modified getParentNoticeUnsignedCount(), getStudentNoticeUnsignedCount() for flag sql query [Case#2012-0424-0918-32073]
#
#	Date: 	2012-02-29 (Jason)
#			modified getgetAssessmentStatusCount() to ignore assessments which are from lesson plan by fromlesson
#
#	Date:	2011-12-13 (Henry Chow)
#			modified getStudentNoticeUnsignedCount(), getParentNoticeUnsignedCount()
#			do not count the eNotice which is belongs to customization report [2011-1213-1154-33066]
#
#	Date:	2011-11-17 	Jason
#			add get_lesson_session(), session_gc() for the powerlesson use
#			to provide a quick access of lesson for student
#
#	Date:	2011-05-04	YatWoon
#			update returnTVinfo(), fixed: incorrect data retrieve
#
#	Date: 	2011-04-08 Jason
#			modify getgetAssessmentStatusCount() to improve sql performance 
#			for massive datam it could handle properly than not using 'UNION ALL'
#	Date:	2010-09-10	YatWoon
#			update getUnsignedCircularCount(), add circular record status = 1
#
########################

class libportal extends libdb
{
	var $session_timeout = 2700; 		// for powerlesson use only
	
	function libportal()	
	{	    
		 $this->libdb();
	}

	#######################################################################################
	# Mail related
	#######################################################################################
	# check any new mails [ campus mail]
	function returnNumNewMessage($UserID)
	{
		$sql = "SELECT COUNT(CampusMailID) FROM INTRANET_CAMPUSMAIL WHERE RecordType = 2 AND RecordStatus IS NULL AND CampusMailFromID IS NOT NULL AND Subject IS NOT NULL AND UserID = '$UserID'";
		$result = $this->returnVector($sql,1);
		return $result[0];
	}
     
	# check any new mails [ iMail v1.2]
	function returnNumNewMessage_iMail($UserID)
	{
		$sql = "SELECT COUNT(CampusMailID) FROM INTRANET_CAMPUSMAIL WHERE UserFolderID = 2 AND RecordStatus IS NULL AND Subject IS NOT NULL AND Deleted != 1 AND UserID = '$UserID'";
		$result = $this->returnVector($sql,1);
		return $result[0];
	}
	
	# Connect to mail server
	# Store the connection resource variable in $this->connection
	function openInbox($UserLogin, $UserPassword)
	{
		global $webmail_info;
		global $intranet_inbox_connect_failed;
		$UserLogin = strtolower($UserLogin);
		if ($webmail_info['mail_server_type'] == 'qmail')
		{
		   $login_string = "$UserLogin@".$this->mailaddr_domain;
		}
		else if ($this->login_type == "email")
		{
		   $login_string = "$UserLogin@".$this->mailaddr_domain;
		}
		else $login_string = $UserLogin;
		$inbox = @imap_open("{".$this->host.":".$this->mail_server_port."}",$login_string,$UserPassword);
		
		if (!$inbox)
		{
		    $intranet_inbox_connect_failed = true;
		    return false;
		}
		$this->inbox = $inbox;
		return $this->inbox;
	}
	#######################################################################################
	# Mail related -- End
	#######################################################################################
	
	#######################################################################################
	# eNotice related
	#######################################################################################
	function getParentNoticeUnsignedCount()
    {
	     global $UserID, $sys_custom;
	     # Grab children ids
	     $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
	     $children = $this->returnVector($sql);
	     if (sizeof($children)==0) return "";
		 
	     include("libnotice.php");
	     $lnotice = new libnotice();
	     # not allow late sign
	     if(!$lnotice->isLateSignAllow)
	     {
	     	//$date_str = " AND a.DateEnd >= CURDATE() ";
	     	$date_str = " AND UNIX_TIMESTAMP(a.DateEnd) >= UNIX_TIMESTAMP(NOW()) ";
     	 }
	      
	     $child_list = implode(",",$children);
	     $sql = "SELECT COUNT(c.NoticeReplyID)
	             FROM INTRANET_NOTICE_REPLY as c
	                  LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
	                  LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
	             WHERE c.StudentID IN ($child_list) 
	             AND UNIX_TIMESTAMP(a.DateStart) <= UNIX_TIMESTAMP(NOW()) $date_str
	             AND c.RecordStatus = 0
	                   AND a.RecordStatus = 1 AND a.IsDeleted=0
	              and (a.Module is NULL or a.Module='DISCIPLINE' or a.Module='DISCIPLINE_STUDENTAWARDREPORT' or a.Module='STUDENTATTENDANCE') and TargetType='P'
	             ";
	             
	             /*
	             ### don't know what's the following purpose, will check this if sun kei request problem ###
	     if($sys_custom['skss']['StudentAwardReport']) {
			//$sql .= " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";	
			$sql .= " AND (a.Module!='DISCIPLINE_STUDENTAWARDREPORT' or a.Module is NULL)";    
		}
		*/
	     $replies = $this->returnVector($sql);
	     
	     ## count on "current paymen notice"
	     $sql = "SELECT COUNT(c.NoticeReplyID)
	             FROM INTRANET_NOTICE_REPLY as c
	                  LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
	                  LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
	             WHERE c.StudentID IN ($child_list) 
	             AND UNIX_TIMESTAMP(a.DateStart) <= UNIX_TIMESTAMP(NOW()) AND UNIX_TIMESTAMP(a.DateEnd) >= UNIX_TIMESTAMP(NOW())
	             AND c.RecordStatus = 0
	                   AND a.RecordStatus = 1 AND a.IsDeleted=0
	              and a.Module='Payment' and TargetType='P'
	             ";
	     $replies2 = $this->returnVector($sql);
	     
	     return $replies[0]+$replies2[0];
    }
    
    function getStudentNoticeUnsignedCount()
    {
         global $UserID, $sys_custom;

		 include("libnotice.php");
	     $lnotice = new libnotice();
	     # not allow late sign
	     if(!$lnotice->isLateSignAllow)
	     {
	     	//$date_str = " AND a.DateEnd >= CURDATE() ";
	     	$date_str = " AND UNIX_TIMESTAMP(a.DateEnd) >= UNIX_TIMESTAMP(CURDATE()) ";
     	 }
     	 
         $sql = "SELECT COUNT(c.NoticeReplyID)
                 FROM INTRANET_NOTICE_REPLY as c
                      LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                 WHERE c.StudentID = '$UserID' 
                 AND UNIX_TIMESTAMP(a.DateStart) <= UNIX_TIMESTAMP(NOW()) $date_str
                 AND c.RecordStatus = 0
                  AND a.RecordStatus in (1) AND a.IsDeleted=0 
                  and TargetType='S'
                 "; 
                 /*
         if($sys_custom['skss']['StudentAwardReport']) {
			//$sql .= " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";	
			$sql .= " AND (a.Module!='DISCIPLINE_STUDENTAWARDREPORT' or a.Module is NULL)";    
		}
		*/
		 
         $replies = $this->returnVector($sql);
         return $replies[0];
    }
	#######################################################################################
	# eNotice related -- End
	#######################################################################################
	
	#######################################################################################
	# Circular related 
	#######################################################################################
	function getUnsignedCircularCount($targetUserID="", $Ended = 0)
    {
    	 global $sys_custom;
    	 
         if ($targetUserID == "")
         {
             global $UserID;
             $targetUserID = $UserID;
         }
         if ($targetUserID == "")
         {
             return 0;
         }
         
         global $PATH_WRT_ROOT;
		 include_once($PATH_WRT_ROOT."includes/libcircular.php");
		 $lcircular = new libcircular();
		 
		 // [2016-1130-0910-56236] hide eCircular icon if eCircular is disabled 
		 if($lcircular->disabled)
		 {
		 	return 0;
		 }
		 
         // [2016-0928-0903-29236] fixed: cannot check if eCircular allow late signing or not
         // [2017-0306-0923-10236] display unsigned circular count (not pass deadline only) even allow late sign ($sys_custom['eCircular']['IconUnsignedCountApplyOldLogic'])
         if ($lcircular->isLateSignAllow && !$sys_custom['eCircular']['IconUnsignedCountApplyOldLogic'])
         {
         }
         else
         {
             $conds = " AND b.DateEnd >= CURDATE()";
         }

         if($Ended)
         {
                 $conds = " AND b.DateEnd >= CURDATE()";
         }

         $sql = "SELECT 
         			COUNT(a.CircularReplyID)
				FROM 
					INTRANET_CIRCULAR_REPLY as a
					LEFT OUTER JOIN INTRANET_CIRCULAR as b ON a.CircularID = b.CircularID
				WHERE 
					a.UserID = $targetUserID AND a.RecordStatus != 2 AND
					b.DateStart <= CURDATE() and b.RecordStatus=1 $conds";
         $temp = $this->returnVector($sql);
         return $temp[0]+0;
    }
	#######################################################################################
	# Circular related -- End
	#######################################################################################        
	
	
	#######################################################################################
	# Group Survey related
	#######################################################################################        
	/*
	function returnSurvey()
	{
		global $UserID;
		$date_conds = " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
		$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID'";
		$groups = $this->returnVector($sql);
		if (sizeof($groups)!=0) $group_list = implode(",",$groups);
		$sql = "SELECT a.SurveyID FROM INTRANET_SURVEY as a
		        LEFT OUTER JOIN INTRANET_GROUPSURVEY as b ON a.SurveyID = b.SurveyID
		        WHERE b.SurveyID IS NULL AND a.RecordStatus = 1 $date_conds";
		$idschool = $this->returnVector($sql);
		if ($group_list != "")
		{
		    $sql = "SELECT 
		    			DISTINCT a.SurveyID 
		            FROM 
		            	INTRANET_SURVEY as a, 
		            	INTRANET_GROUPSURVEY as b,
		            	INTRANET_SURVEYANSWER as c
		            WHERE 
		            	a.SurveyID = b.SurveyID AND 
		            	b.GroupID IN ($group_list) AND 
		            	a.RecordStatus = 1  
		            	$date_conds
		            	AND c.SurveyID=a.SurveyID
		            ";
		            
		    $idgroup = $this->returnVector($sql);
		    $overall = array_merge($idschool,$idgroup);
		}
		else
		{
		    $overall = $idschool;
		}
		if (sizeof($overall)==0) 
		{
				return array();
		}
		
		$list = implode(",",$overall);
		$namefield = getNameFieldWithClassNumberByLang("c.");
		$sql = "SELECT a.SurveyID, a.Title,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),
		        IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$namefield),
		        d.Title, a.RecordType FROM INTRANET_SURVEY as a
		        LEFT OUTER JOIN INTRANET_SURVEYANSWER as b ON a.SurveyID = b.SurveyID AND b.UserID = $UserID
		        LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.PosterID
		        LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.OwnerGroupID
		        WHERE a.SurveyID IN ($list) AND b.SurveyID IS NULL
		        ORDER BY a.DateEnd";
		$result = $this->returnArray($sql,7);
		return $result;
	}
	*/
	function returnSurvey()
	{
		global $UserID, $Lang, $sys_custom;
		
		$date_conds = " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
		
		# retrieve WHOLE SCHOOL survey
		$sql = "SELECT a.SurveyID FROM INTRANET_SURVEY as a
		        LEFT OUTER JOIN INTRANET_GROUPSURVEY as b ON a.SurveyID = b.SurveyID
		        WHERE b.SurveyID IS NULL AND a.RecordStatus = 1 $date_conds";
		if($sys_custom['iPf']['learnAndTeachReport']){
			$sql .= "  AND a.QuestionType <> 'LT'";
		}       
		$idschool = $this->returnVector($sql);
		
		# retrieve GROUP survey
		$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID' and GroupID is not NULL";
		$groups = $this->returnVector($sql);
		if (sizeof($groups)!=0) 
		{
			$group_list = implode(",",$groups);
		
		    $sql = "SELECT DISTINCT a.SurveyID FROM INTRANET_SURVEY as a, INTRANET_GROUPSURVEY as b
		            WHERE a.SurveyID = b.SurveyID AND b.GroupID IN ($group_list) AND a.RecordStatus = 1  $date_conds";
		    $idgroup = $this->returnVector($sql);
		    $overall = array_merge($idschool,$idgroup);
		}
		else
		{
		    $overall = $idschool;
		}
		
		# retrieve SUBJECT GROUP survey
		if($sys_custom['iPf']['learnAndTeachReport']){
			$sql = "SELECT SubjectGroupID FROM SUBJECT_TERM_CLASS_USER WHERE UserID = '$UserID'";
			$groups = $this->returnVector($sql);
			if (sizeof($groups)!=0) 
			{
				$group_list = implode(",",$groups);
			
			    $sql = "SELECT DISTINCT a.SurveyID FROM INTRANET_SURVEY as a, INTRANET_SUBJECTGROUPSURVEY as b
			            WHERE a.SurveyID = b.SurveyID AND b.SubjectGroupID IN ($group_list) AND a.RecordStatus = 1  $date_conds";
			    
			    $idgroup = $this->returnVector($sql);
			    $overall = array_merge($overall,$idgroup);
			}
			else
			{
			    $overall = $overall;
			}
		}
		
		if (sizeof($overall)==0) return array();
		
		$list = implode(",",$overall);
		$namefield = getNameFieldWithClassNumberByLang("c.");
		$sql ="set @c=0";
		$this->db_db_query($sql);
		
		$sql = "SELECT 
					DATE_FORMAT(a.DateStart,'%Y-%m-%d') as StartDate,
					DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as EndDate,
					concat('<a href=\"javascript:openSurvey(', a.SurveyID,', ', @c := @c + 1 ,');\">', a.Title, '</a>'),
			        IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$namefield),
			        '". $Lang['eSurvey']['NotAnswered'] ."',
			        a.SurveyID as SurveyID
		        FROM 
			        INTRANET_SURVEY as a
			        LEFT OUTER JOIN INTRANET_SURVEYANSWER as b ON a.SurveyID = b.SurveyID AND b.UserID = $UserID
			        LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.PosterID
		        WHERE 
		        	a.SurveyID IN ($list) AND 
		        	b.SurveyID IS NULL
		        ";

		//LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.OwnerGroupID
		// ORDER BY a.DateEnd";
			return $this->returnArray($sql);
	}
	#######################################################################################
	# Group Survey related -- End
	#######################################################################################        
	
	#######################################################################################
	# Polling
	#######################################################################################        
	function returnUnPollingList($UserID)
	{
		$GroupPollingIDs = $this->db_sub_select("SELECT PollingID FROM INTRANET_GROUPPOLLING");
		$GroupPollingIDs = $GroupPollingIDs==""? "''" : $GroupPollingIDs;
		
		$sql = "SELECT 
					a.PollingID, b.UserID
				FROM 
					INTRANET_POLLING AS a 
					LEFT OUTER JOIN INTRANET_POLLINGRESULT AS b ON (a.PollingID = b.PollingID AND b.UserID = $UserID)
				WHERE 
					a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()
					AND a.PollingID NOT IN ($GroupPollingIDs)
					AND b.UserID IS NULL ";
		return $this->returnArray($sql, 2);
	}
    #######################################################################################
	# Polling -- End
	#######################################################################################        
	
    #######################################################################################
	# eNews
	#######################################################################################
	function getWaitingAnnouncement($targetUserID)
	{
	    $sql = "SELECT count(*) FROM INTRANET_ANNOUNCEMENT_APPROVAL WHERE UserID = '$targetUserID' and RecordStatus = 2";
		$temp = $this->returnVector($sql);
		return $temp[0]+0;
	}
    #######################################################################################
	# eNews -- End
	#######################################################################################
	
	#######################################################################################
	# CampusTV related
	#######################################################################################
	function returnTVinfo()
	{
		global $intranet_root, $_SESSION;
          if ($_SESSION['campustv_portal_content']=="")
          {
          	$live_portal_file = get_file_content ("$intranet_root/file/campustv_portal.txt");
          	$_SESSION['campustv_portal_content'] = (trim($live_portal_file)=="")?"NULL_CONTENT":$live_portal_file;
          } else
          {
          	$live_portal_file = ($_SESSION['campustv_portal_content']=="NULL_CONTENT") ? "":$_SESSION['campustv_portal_content'];
          }
		
		$x = array();
// 		if (is_file($live_portal_file))
		if(!empty($live_portal_file))
		{
// 			$file_content = get_file_content($live_portal_file);
			$file_content = $live_portal_file;
			$data = explode("\n",$file_content);
 			list(	$live_show,
					$live_width,
					$live_height
				) = $data;
		}
		$x['live_show'] = $live_show;
		$x['live_width'] = $live_width ? $live_width : 320;
		$x['live_height'] = $live_height ? $live_height : 250;
		return $x;
	}
	
	function returnLiveURL()
	{
		global $intranet_root;
		$live_file = "$intranet_root/file/campustv_live.txt";
		
		if (is_file($live_file))
			return trim(get_file_content($live_file));
		else
			return "";	
	}
	
	function retrieveClipInfo($ClipID='')
    {
         $sql = "SELECT Title, Description,ClipURL, Recommended,isFile,UserID, RecordType, RecordStatus, IsSecure FROM INTRANET_TV_MOVIECLIP";
         if($ClipID)
	         $sql .= " WHERE ClipID = $ClipID";
	     else
	     {
			$sql .= " order by DateInput desc limit 1";
	     }
         $result = $this->returnArray($sql,9);
         return $result[0];
    }
        
	function returnChannels()
    {
		$sql = "SELECT ChannelID, Title FROM INTRANET_TV_CHANNEL ORDER BY DisplayOrder";
		return $this->returnArray($sql,2);
    }
    
    function returnProgramList()
    {
	    global $intranet_root;
		$programme_file = "$intranet_root/file/campustv_programme.txt";    
		 
		$programmeList = array();
		if (is_file($programme_file))
		{
			$file_content = get_file_content($programme_file);
			$data = explode("\n",$file_content);
			for ($i=0; $i<sizeof($data); $i++)
			{
				$list[] = explode(":::",$data[$i]);
			}
			$programmeList = $list;
		}
		
		return $programmeList;
    }
    
   
	#######################################################################################
	# CampusTV related [End]
	#######################################################################################
	
	#######################################################################################
	# eClass Panel related
	#######################################################################################
		
	function returnEClassUserIDCourseIDStandard($Email, $roomType=0){
    
    global $eclass_db;
    
    $fieldname  = "a.course_id, a.course_code, a.course_name, ";
    $fieldname .= "b.user_id, b.memberType, b.lastused, b.user_course_id ,b.status, a.course_desc";
    $sql = "SELECT $fieldname FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b WHERE a.course_id = b.course_id AND b.user_email = '$Email' AND a.RoomType='$roomType' AND (b.status is NULL OR b.status NOT IN ('deleted')) GROUP BY a.course_id ORDER BY a.course_code, a.course_name";

    return $this->returnArray($sql);
  }
	function returnEClassUserIDCourseIDiTextbook($Email)
	{
	  	global $eclass_db;
	  	
	  	    $fieldname  = "a.course_id, a.course_code, a.course_name, ";
	    	$fieldname .= "b.user_id, b.memberType, b.lastused, b.user_course_id ,b.status";
	    	$sql = "SELECT $fieldname FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b WHERE a.course_id = b.course_id AND b.user_email = '$Email' AND a.RoomType=7 AND (b.status is NULL OR b.status NOT IN ('deleted')) GROUP BY a.course_id ORDER BY a.course_code, a.course_name";
				
	    return $this->returnArray($sql);	
	    	
	  }
  function displayUserEClass($UserEmail, $rightpanel=0, $roomType=0){
    
    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
  	global $i_status_suspended, $image_path, $LAYOUT_SKIN;
  	
    $row = $this->returnEClassUserIDCourseIDStandard($UserEmail, $roomType);
  
    if(sizeof($row)==0)
  	{		
        if($rightpanel){
          $x .= "
    			<tr>
    				<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
    				<td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
    				<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
    			</tr>
    			";	
        }
        else{
          $x .= "<tr>
        					<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</td>
        				</tr>
    				";	
        }		
  	}
  	else
  	{
      $x .= ($rightpanel)? "":"<tr>\n";
  		for($i=0; $i<sizeof($row); $i++)
  		{
  			$course_id = $row[$i][0];
  			$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
  			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
  			$user_id = $row[$i][3];
  			$memberType = $row[$i][4];
  			$lastused = $row[$i][5];
  			$user_course_id = $row[$i][6];
  			$status = $row[$i][7]; 
  			
  			# get notes count / display
  			list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);			
  			# get assessment count / display
  			list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);			
  			# get forum count / display
  			list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
        	# get announcement count / display
  			list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
  			
  			
        if($rightpanel)
          $whatsnew = "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" >";
  			else
          $whatsnew = "<table border='0' cellspacing='0' cellpadding='6'>";
  			
        $whatsnew .= "<tr>";
        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$notes_display."</td>":"";
        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$ass_display."</td>":"";
        $whatsnew .= "<td align=\"center\" >".$forum_display."</td>";
        $whatsnew .= "<td align=\"center\" >".$annc_display."</td>";
        $whatsnew .= "</tr>";
        $whatsnew .= "</table>";
  																
        if (($count_notes == 0 || $memberType !='S') && ($count_assessment==0 || $memberType !='S') && ($count_forum==0) && ($count_announcement==0))
          $whatsnew2 = false;
        else $whatsnew2 = true;
        
        $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";
        
        if($rightpanel) 
          $x .= $this->MyeClassPanelDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $lastused, $whatsnew, $whatsnew2);
        else 
          $x .= $this->MyeClassTableDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $lastused, $whatsnew, $whatsnew2);
      }
  		
      $x .= ($rightpanel)?"":"</tr>\n";
  	}
  	
  	return $x;
  
  }

    function displayUserEClassInElearningSection($UserEmail, $roomType=0)
    {
        global $UserID;
        global $intranet_root, $image_path, $LAYOUT_SKIN, $plugin;
        global $Lang, $i_status_suspended, $i_frontpage_notes, $i_frontpage_assessment, $i_frontpage_bulletin, $i_frontpage_announcement;

        $courses = $this->returnEClassUserIDCourseIDStandard($UserEmail, $roomType);

        if(count($courses) == 0){
            include_once 'libuser.php';
            include_once 'libaccess.php';
            $lu = new libuser($UserID);
            $la = new libaccess($UserID);
            $la->retrieveAccessEClass();

            $html = '<div class="index-remark">';
            if (
                ($lu->teaching && $la->isAccessEClassMgtCourse()) ||
                (!$lu->teaching && $lu->RecordType == 1 && $la->isAccessEClassNTMgtCourse()) ||
                ($_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] && !$plugin["Disable_eClassroom"])
            ) {
                include_once 'libeclass2007a.php';
                $leclass = new libeclass2007();
                $createClassroomUrl = "/home/eLearning" . $leclass->eclass_folder . "/organize/new.php";

                $html .= '<div class="add-classroom"></div>
                          <p>' . $Lang['eLearningSection']['NotEnrolledInAnyClassroom'] . '</p>
                          <input type="button" class="formbutton_v30" value="' . $Lang['eLearningSection']['CreateClassroom'] . '" onclick="location.href=\'' . $createClassroomUrl . '\';">';
            } else {
                $html .= '<div class="add-classroom-student"></div>
                          <p>' . $Lang['eLearningSection']['NotEnrolledInAnyClassroom'] . '</p>';
            }
            $html .= '</div>';
        } else {
            $html = '<ul class="indexlist-container">';

            foreach($courses as $course){
                $courseId     = $course[0];
                $courseCode   = intranet_wordwrap($course[1],30,"\n",1);
                $courseName   = intranet_wordwrap($course[2],30,"\n",1);
                $courseUserId = $course[3];
                $memberType   = $course[4];
                $lastUsed     = $course[5];
                $userCourseId = $course[6];
                $status       = $course[7];

                $iconPath = "{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list";

                # get notes count / display
                list($countNotes, $notesDisplay) = $this->returnNotesCount($courseId, $courseUserId, $memberType);
                $notesDisplay = $memberType=='S'? '<span class="indextabclassicon"><img src="'.$iconPath.'/icon_econtent.gif" border="0" align="absmiddle" title="'.$i_frontpage_notes.'">'.$countNotes.'</span>' : '';

                # get assessment count / display
                list($countAssessment, $assDisplay) = $this->returnAssessmentCount($courseId, $courseUserId, $memberType);
                $assDisplay = $memberType=='S'? '<span class="indextabclassicon"><img src="'.$iconPath.'/icon_assessment.gif" border="0" align="absmiddle" title="'.$i_frontpage_assessment.'">'.$countAssessment.'</span>' : '';

                # get forum count / display
                list($countForum, $forumDisplay) = $this->returnBulletinCount($courseId, $courseUserId, $memberType);
                $forumDisplay = '<span class="indextabclassicon"><img src="'.$iconPath.'/icon_forum.gif" border="0" align="absmiddle" title="'.$i_frontpage_bulletin.'">'.$countForum.'</span>';

                # get announcement count / display
                list($countAnnouncement, $anncDisplay) = $this->returnAnnouncementCount($courseId, $courseUserId);
                $anncDisplay = '<span class="indextabclassicon"><img src="'.$iconPath.'/icon_annou.gif" border="0" align="absmiddle" title="'.$i_frontpage_announcement.'">'.$countAnnouncement.'</span>';

                if(
                    ($countNotes == 0 || $memberType != 'S') &&
                    ($countAssessment == 0 || $memberType != 'S') &&
                    $countForum == 0 &&
                    $countAnnouncement==0
                ){
                    $newAlert = '';
                } else {
                    $newAlert = '<span class="new-alert"></span>';
                }

                $statusDisplay = $status == "suspended"? " - <i>{$i_status_suspended}</i>" : '';

                $html .= <<<HTML
<li class="indexlist-item">
        <a href="javascript:fe_eclass({$userCourseId})">
            <span class="indexlist-title">{$courseCode} - {$courseName} ({$memberType}{$statusDisplay}){$newAlert}</span>
            <div class="indexnewsbottom">
                {$notesDisplay}
                {$assDisplay}
                {$forumDisplay}
                {$anncDisplay}
            </div>
        </a>
    </li>
HTML;
            }
            $html .= '</ul>';
        }

        return $html;
    }

   function displayChildrenEClass($UserID,$childID){
    
    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
  	global $i_status_suspended, $image_path, $LAYOUT_SKIN;
  	global $intranet_db, $eclass_prefix,$lo;
	include_once("libaccess.php");
	$la = new libaccess();
	$la->retrieveAccessEClass(true);
	$enable_children_access = $la->isAccessChildrenEClassMgt();
	$filecontent = trim(get_file_content ($lo->filepath."/files/children_eclass.txt"));
	$child_class = ($filecontent=="")? array(): explode(",",$filecontent);

	$sql = "SELECT UserEmail FROM {$intranet_db}.INTRANET_USER WHERE UserID = '".$childID."'";
	$result = $this->returnVector($sql);
	$ChildEmail = $result[0];
    $row = $this->returnEClassUserIDCourseIDStandard($ChildEmail);
    
    if(sizeof($row)==0)
  	{		
         $x .= "<tr>
        			<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</td>
        		</tr>
    	";		
  	}
  	else
  	{
      	$x .= ($rightpanel)? "":"<tr>\n";
      	$cnt = 0;
  		for($i=0; $i<sizeof($row); $i++)
  		{
  			$course_id = $row[$i][0];
  			$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
  			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
  			$user_id = $row[$i][3];
  			$memberType = $row[$i][4];
  			$lastused = $row[$i][5];
  			$user_course_id = $row[$i][6];
  			$status = $row[$i][7]; 
  			
  			# get notes count / display
  			list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);			
  			# get assessment count / display
  			list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);			
  			# get forum count / display
  			list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
        # get announcement count / display
  			list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
  			
  			
        if($rightpanel)
          $whatsnew = "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" >";
  			else
          $whatsnew = "<table border='0' cellspacing='0' cellpadding='6'>";
  			
        $whatsnew .= "<tr>";
        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$notes_display."</td>":"";
        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$ass_display."</td>":"";
        $whatsnew .= "<td align=\"center\" >".$forum_display."</td>";
        $whatsnew .= "<td align=\"center\" >".$annc_display."</td>";
        $whatsnew .= "</tr>";
        $whatsnew .= "</table>";
  																
        if (($count_notes == 0 || $memberType !='S') && ($count_assessment==0 || $memberType !='S') && ($count_forum==0) && ($count_announcement==0))
          $whatsnew2 = false;
        else $whatsnew2 = true;
        
        $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";
		
        if($enable_children_access || in_array($course_id,$child_class)){
			$x .= $this->MyeClassTableDisplay($cnt, $status, $course_code, $course_name, $memberType, $user_course_id, $lastused, $whatsnew, $whatsnew2, 1);
        	$cnt++;
        }
      }
  		
      $x .= ($rightpanel)?"":"</tr>\n";
  	}
  	
  	return $x;
  
  } 
  function displayUseriTextbookClass($UserEmail,$rightpanel)//20140807-parent-pas
  {    
    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
  	global $i_status_suspended, $image_path, $LAYOUT_SKIN;
  	global $iTextbook_Book;
  	
  	global $isParent,$plugin;
  	
  	if($iTextbook_Book == null){
  		$iTextbook_Book = $_SESSION['iTextbook_Book'];
  	}
  	
  	$row = array();
  	$course_id_arr = array();
  	if(is_array($UserEmail)){
  		foreach($UserEmail as $key=>$value){  		
	    	$results = $this->returnEClassUserIDCourseIDiTextbook($value);
	    	foreach($results as $key=>$result){
	    		if(!in_array($result["course_id"], $course_id_arr)){
	    			$course_code = $result["course_code"];
	    			if($isParent && (($course_code == "poemsandsongs" && !$plugin['pas_parent']) 
	    			|| ($course_code == "ambook" && !$plugin['ambook_parent'])
	    			|| ($course_code != "ambook" && $course_code != "poemsandsongs") )){
	    				
	    			}else{
			    		array_push($row, $result);
			    		array_push($course_id_arr, $result["course_id"]);	    				
	    			}
		    	}	   
	    	}	    	 	
	  	}
  	}else{
  		$row = $this->returnEClassUserIDCourseIDiTextbook($UserEmail);
  	} 	
    
    if(sizeof($row)==0)
  	{		
        if($rightpanel){
          $x .= "
    			<div class=\"iTextbook_list indextabclassiconoff\" >
    				".$i_no_record_exists_msg."
        			</a></div>
    			";	
    	}
        else{
          $x .= "<tr>
        					<td width=\"100%\" class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</td>
        				</tr>
    				";	
        }		
  	}
  	else
  	{
      $x .= ($rightpanel)? "":"<tr>\n";
  		for($i=0; $i<sizeof($row); $i++)
  		{
  			$course_id = $row[$i][0];
  			$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
  			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
  			$user_id = $row[$i][3];
  			$memberType = $row[$i][4];
  			$lastused = $row[$i][5];
  			$user_course_id = $row[$i][6];
  			$status = $row[$i][7]; 
  			
  			# get notes count / display
  			list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);			
  			# get assessment count / display
  			list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);			
  			# get forum count / display
  			list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
        # get announcement count / display
  			list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
  			
  			
        if($rightpanel)
          $whatsnew = "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" >";
  			else
          $whatsnew = "<table border='0' cellspacing='0' cellpadding='6'>";
  			
        $whatsnew .= "<tr>";
        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$notes_display."</td>":"";
        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$ass_display."</td>":"";
        $whatsnew .= "<td align=\"center\" >".$forum_display."</td>";
        $whatsnew .= "<td align=\"center\" >".$annc_display."</td>";
        $whatsnew .= "</tr>";
        $whatsnew .= "</table>";
  																
        if (($count_notes == 0 || $memberType !='S') && ($count_assessment==0 || $memberType !='S') && ($count_forum==0) && ($count_announcement==0))
          $whatsnew2 = false;
        else $whatsnew2 = true;
        
        $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";
        
        
        if($rightpanel)
        {
        	/*
        	$x = '<div class="iTextbook_list">
    				<a href="javascript:fe_eclass(\''.$user_course_id.'\')">
        			<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/itextbook/'.$row[$i]['course_code'].'/logo_'.$row[$i]['course_code'].'_s.png" />
        			<h1>'.$row[$i]['course_name'].'</h1>
        			</a></div>';
        	*/
        	if(is_array($iTextbook_Book[$row[$i]['course_code']]) && count($iTextbook_Book[$row[$i]['course_code']])>0){
	        	foreach($iTextbook_Book[$row[$i]['course_code']] as $BookID=>$BookInfo){
	        		if(!$isParent){
		        		$x .= '<div class="iTextbook_list">
				    				<a href="javascript:fe_itextbook(\''.$user_course_id.'\', \''.$BookID.'\')">
				        				<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/itextbook/'.$row[$i]['course_code'].'/logo_'.$row[$i]['course_code'].'_s.png" />
				        				<h1>'.$BookInfo['BookName'].'</h1>
				        			</a>
							   </div>';
	        		}else{
	        			if(($row[$i]['course_code'] == "ambook" && $plugin['ambook_parent']) || ($row[$i]['course_code'] == "poemsandsongs" && $plugin['pas_parent']))
		        		$x .= '<div class="iTextbook_list">
				    				<a href="javascript:fe_itextbook2(\''.$user_course_id.'\', \''.$BookID.'\', \''.$BookInfo["children_UserID"].'\')">
				        				<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/itextbook/'.$row[$i]['course_code'].'/logo_'.$row[$i]['course_code'].'_s.png" />
				        				<h1>'.$BookInfo['BookName'].'</h1>
				        			</a>
							   </div>';	        			
	        		}
	        	}
        	}
        	
        	/* Temporary add poems and songs 
        	if($row[$i]['course_code']=='poemsandsongs'){
        		$x .= "<div class=\"iTextbook_list\">
			    			<a href=\"javascript:javascript:fe_itextbook('$user_course_id')\">
								<img src=\"$image_path/$LAYOUT_SKIN/itextbook/".$row[$i]['course_code']."/logo_poem_s.png\" />
			        			<h1>Poems and Songs</h1>
							</a>
					   </div>";
        	}
        	*/
        } 
        else 
          $x .= $this->MyeClassTableDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $lastused, $whatsnew, $whatsnew2);
      }
  		
      $x .= ($rightpanel)?"":"</tr>\n";
  	}
  	
  	return $x;
  
  
  }

    function displayUserPL2InElearningSection($UserEmail)
    {
        $courses = $this->returnEClassUserIDCourseIDStandard($UserEmail, 0);

        if(count($courses) > 0){
            $html = '';
        } else {
            $html = '<ul class="indexlist-container">';

            foreach($courses as $course) {
                $courseId     = $course[0];
                $courseCode   = intranet_wordwrap($course[1], 30, "\n", 1);
                $courseName   = intranet_wordwrap($course[2], 30, "\n", 1);

                $html .= <<<HTML
<li class="indexlist-item">
    <a href="javascript:loginPowerLesson2({$courseId})">
        <span class="indexlist-title">{$courseCode} - {$courseName}</span>
        <!--<span class="new-alert"></span>-->
    </a>
</li>
HTML;
            }
            $html .= '</ul>';
        }

        return $html;
    }
  
  function returnNotesCount($course_id, $user_id,$memberType)
  {
  		global $i_frontpage_notes,$image_path, $LAYOUT_SKIN;
  		global $eclass_prefix,$eclass40_filepath;
  		
  		$course_db = $eclass_prefix."c$course_id";
      $f = new libgroups($course_db);
  		//if ($memberType=="G" || $memberType=="S")
  		if ($memberType=="S")
  		{
  			$notes_id_blocked = $f->returnFunctionIDs("NOTE",$user_id);

  			# get notes
  			$notesArr = $this->getNotes($course_db, $memberType, $notes_id_blocked);
	
  			$count = 0;
  			for ($i=0; $i<sizeof($notesArr); $i++)
  			{
  				$readflag = $notesArr[$i][8];
  				if (!strstr($readflag, ";".$user_id.";"))
  				{
  					$count ++;
  				}
  			}
  		} 
  		else
  		{
  			$sql = "SELECT COUNT(*) FROM ".$course_db.".notes WHERE status = '1' AND (readflag is null OR readflag not like '%;".$user_id.";%') ";		
  			$row = $this->returnVector($sql);
  			$count = (int) $row[0];
  		}
  		
  		if ($count != 0)
  		{		 	
  		 	$x  = "<span class=\"indextabclassicon\" >\n";
  			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_econtent.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_notes'>{$count} \n";
  			$x .= "</span>\n";
  		} else {
  		 	$x  = "<span class=\"indextabclassiconoff\" >\n";
  			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_econtent_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_notes'>{$count} \n";
  			$x .= "</span>\n";
  		}
  		
  		return array($count, $x);  
  
  }
  
  
  function returnAssessmentCount($course_id, $user_id, $memberType)
  {
      global $i_frontpage_assessment, $eclass40_filepath, $image_path, $LAYOUT_SKIN;
      global $eclass_prefix;
      
      $count = 0;
      
      if ($memberType=="S" && $user_id!='')
      {            
        # GET not submitted assessment
        // $sql = $this->getAssessmentStatusCountSQL($course_id, $user_id);
        // $row = $this->returnArray($sql);
		// $count = $row[0][0];
		$count = $this->getgetAssessmentStatusCount($course_id, $user_id);
      }
      
      if ($count != 0)
      {              
  	 		$x  = "<span class=\"indextabclassicon\" >\n";
  			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_assessment.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_assessment'>{$count} \n";
  			$x .= "</span>\n";              
      } else {
  	 		$x  = "<span class=\"indextabclassiconoff\" >\n";
  			$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_assessment_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_assessment'>{$count} \n";
  			$x .= "</span>\n";              	          
      }
      
      return array($count, $x);
  }
  
  function returnBulletinCount($course_id, $user_id, $memberType)
  {
    global $i_frontpage_bulletin, $image_path, $LAYOUT_SKIN;
    global $eclass_prefix;
    
    $f = new libgroups($eclass_prefix."c$course_id");
    $sql  = "SELECT count(bulletin_id) FROM ".$eclass_prefix."c".$course_id.".bulletin WHERE (readflag is null OR readflag not like '%;$user_id;%')";
    # Eric Yip (20090824): New filtering condition in eClass40
    $sql .= "and IsDraft = '0' and IsTemp = '0'";
    $sql .= ($memberType=="G" || $memberType=="S") ? " AND forum_id NOT IN (".$f->returnFunctionIDs("B",$user_id).")" : "";
    $row = $this->returnArray($sql);
    $count = $row[0][0];
    
    if ($count != 0)
    {			
      $x  = "<span class=\"indextabclassicon\" >\n";
      $x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_bulletin'>{$count} \n";
      $x .= "</span>\n";					
    } else {
      $x  = "<span class=\"indextabclassiconoff\" >\n";
      $x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_forum_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_bulletin'>{$count} \n";
      $x .= "</span>\n";					
    }
    
    return array($count, $x);
  }
  function returnAnnouncementCount($course_id, $user_id)
  {
      global $i_frontpage_announcement, $image_path, $LAYOUT_SKIN;
      global $eclass_prefix;
      
      $sql = "SELECT count(announcement_id) FROM ".$eclass_prefix."c".$course_id.".announcement WHERE (readflag is null OR readflag not like '%;$user_id;%') AND (Date_Start <= NOW() AND Date_End > NOW())";
      $row = $this->returnArray($sql,1);
      $count = $row[0][0];
      if ($count != 0)
      {              
      	$x  = "<span class=\"indextabclassicon\" >\n";
      	$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_annou.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_announcement'>{$count} \n";
      	$x .= "</span>\n";              
      } else {
      	$x  = "<span class=\"indextabclassiconoff\" >\n";
      	$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_list/icon_annou_off.gif\" border=\"0\" align=\"absmiddle\" title='$i_frontpage_announcement'>{$count} \n";
      	$x .= "</span>\n";              	          
      }
      
      return array($count, $x);
  }
   
  function MyeClassTableDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $lastused, $whatsnew, $whatsnew2, $isParent=0)
  {
    global $i_frontpage_eclass_lastlogin,$i_status_suspended;
    global $image_path,$LAYOUT_SKIN;
    
    $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";
                                  
    if ($status == "suspended")
    {
      $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eclass/icon_eclass.gif\" >";
      $eClass_title = "$course_code - $course_name ($memberType - <i>$i_status_suspended</i>)";
    }
    else
    {
      if ($whatsnew2)
        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eclass/icon_eclass_new.gif\" >";
      else
        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/eclass/icon_eclass.gif\" >";
      if ($isParent)
        $eClass_title = "<a href=\"javascript:fe_eclass2('{$user_course_id}')\" class=\"tabletoplink\">$course_code - $course_name ($memberType) </a>";
      else
        $eClass_title = "<a href=\"javascript:fe_eclass('{$user_course_id}')\" class=\"tabletoplink\">$course_code - $course_name ($memberType) </a>";
             
      
    }
    
    //$x .= "<td align='center' width='50%' valign='top'>". $this->MyeClassTable($class_icon, $eClass_title, $whatsnew, $lastlogin) ."</td>";
    $x .= "<td align='center' width='50%' valign='top'>";
    
    # eclass table
    $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
    $x .= "<tr>";
    $x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>";
    $x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
    $x .= "<tr>";
    $x .= "<td width='19'>$class_icon</td>";
    $x .= "<td>$eClass_title</td>";
    $x .= "</tr>";
    $x .= "</table></td>";
    $x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>";
    $x .= "</tr>";
    $x .= "<tr>";
    $x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif' width='10' height='11'></td>";
    $x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
    $x .= "<tr>";
    $x .= "<td>";
    $x .= $whatsnew;
    $x .="</td>";
    $x .= "</tr>";
    $x .= "<tr>";
    $x .= "<td align='right' class='tabletext'>$lastlogin</td>";
    $x .= "</tr>";
    $x .= "</table></td>";
    $x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif' width='12' height='12'></td>";
    $x .= "</tr>";
    $x .= "<tr>";
    $x .= "<td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board07.gif' width='10' height='10'></td>";
    $x .= "<td height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif' width='10' height='10'></td>";
    $x .= "<td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif' width='12' height='10'></td>";
    $x .= "</tr>";
    $x .= "</table>";  
    
    $x .= "</td>";
    $x .= ($i%2==1) ? "</tr>\n<tr>\n" : "";
    
    return $x;
  }
  
  function MyeClassPanelDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $lastused, $whatsnew, $whatsnew2)
  {
    global $i_status_suspended;
    global $image_path,$LAYOUT_SKIN;
    
    	        
    if ($i%2==0)
      $RowClass = " class=\"indextabwhiterow\" ";
    else 
      $RowClass = " class=\"indextabbluerow\" ";
  
    $x .= "<tr><td $RowClass >";
    $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
  		        
    if ($status == "suspended"){
      $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
      
      $x .= "<tr>";
      $x .= "<td width=\"19\">{$class_icon}</td>";    		        
      $x .= "<td wrap=true>$course_code - $course_name ($memberType - <i>$i_status_suspended</i>) </td>";																
      $x .= "</tr>";										        
    } else {
      
      if ($whatsnew2)
        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class_new.gif\" >";
      else		        
        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
      
      $x .= "<tr>";
      $x .= "<td width=\"19\">{$class_icon}</td>";
  		$x .= "<td wrap=true><a href=\"javascript:fe_eclass('{$user_course_id}')\" class=\"indextabclasslist\">$course_code - $course_name ($memberType) </a></td>";																
  		$x .= "</tr>";										        			        
    }
    
    $x .= "<tr>";
    $x .= "<td >&nbsp;</td>";
    $x .= "<td >{$whatsnew}</td>";																
    $x .= "</tr>";										        
    
    $x .= "</table> \n";
    $x .= "</td></tr> \n";  
    
    return $x;
  
  }
  
  function getNotes($course_db, $memberType, $idList=0){
		global $ck_user_id, $ck_memberType, $image_path;

		if (trim($idList)=="")
			return false;		

		$sql = "SELECT a_no FROM {$course_db}.notes WHERE notes_id IN ($idList) AND b_no=0 ";
		$row = $this->returnArray($sql);
		$a_noList = "0";
		for ($i=0; $i<sizeof($row); $i++)
		{
			$a_noList .= ",".$row[$i][0];
		}
		// get notes
		$fieldname = "if(locate(';$ck_user_id;',IFNULL(readflag,''))=0, CONCAT(title,' <img src=$image_path/new.gif hspace=2 border=0>'),title)";
		$sql = "SELECT notes_id, a_no, b_no, c_no, d_no, e_no, $fieldname, url, readflag, status, modified, title, description, starttime, endtime FROM {$course_db}.notes WHERE status='1' AND a_no NOT IN ($a_noList) ORDER BY a_no, b_no, c_no, d_no, e_no";
    
    
		$memberType = ($ck_memberType!="") ? $ck_memberType : $memberType;
		$time_now = date("Y-m-d H:i:s");
		$index =0;
		
		$row = $this->returnArray($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
        $notes_id = $row[$i][0];
				$a_no = $row[$i][1];
				$b_no = $row[$i][2];
				$c_no = $row[$i][3];
				$d_no = $row[$i][4];
				$e_no = $row[$i][5];
				$title = $row[$i][6];
				$url = trim($row[$i][7]);
				$readflag = $row[$i][8];
				$status = $row[$i][9];
				$inputdate = $row[$i][10];
				$title_pure = $row[$i][11];
				$description = $row[$i][12];
				$starttime = $row[$i][13];
				$endtime = $row[$i][14];
				$record_type = $row[$i][15];
				# if < start time || if > end time
				if ( ($starttime!="" && $starttime!="0000-00-00 00:00:00" && $starttime>$time_now && ($memberType=="S" || $memberType=="G") && $ck_room_type!="iPORTFOLIO")
					|| ($endtime!="" && $endtime!="0000-00-00 00:00:00" && $endtime<$time_now && ($memberType=="S" || $memberType=="G") && $ck_room_type!="iPORTFOLIO") )
				{
					$flag_not_show[$a_no][$b_no][$c_no][$d_no][$e_no] = true;
				# if parent node is not shown
				} elseif (!($flag_not_show[$a_no][0][0][0][0] || $flag_not_show[$a_no][$b_no][0][0][0]
					|| $flag_not_show[$a_no][$b_no][$c_no][0][0] || $flag_not_show[$a_no][$b_no][$c_no][$d_no][0]))
				{
					$notesArr[$index++] = array($notes_id, $a_no, $b_no, $c_no, $d_no, $e_no, $title, $url, $readflag, $status, $inputdate, $title_pure, $description, $starttime, $endtime, $record_type);
				}
    }
		
    return $notesArr;
	}
	
	function getgetAssessmentStatusCount($course_id, $user_id){
		global $eclass_prefix;
		$course_db = $eclass_prefix."c$course_id";
		$allAssess = Array();
		$sql = "select 1 from {$course_db}.usermaster u
					where u.membertype = 'S' AND u.status IS NULL AND u.user_id='{$user_id}' ";
		$result = $this->returnArray($sql);
		if($user_id!=''&&count($result)>0){
			/*
			# Old Query - no longer use since 2013-03-07
			$sql = "SELECT a.assessment_id
					FROM {$course_db}.assessment a 
					LEFT JOIN {$course_db}.grouping_function gf ON 
					gf.function_id = a.assessment_id AND gf.function_type = 'A'
					LEFT JOIN {$course_db}.user_group ug ON 
					ug.group_id = gf.group_id AND ug.group_id <> 0 
					WHERE
					a.status <> 1 and a.fromlesson is null 
					AND (
					 (gf.function_id IS NULL AND a.handinby = 0) 
					 OR
					  (
					 (gf.user_id = {$user_id})
					 OR
					 (ug.user_id = {$user_id})
					 )
					)
					group by a.assessment_id
					";
			$allAssess = $this->returnVector($sql);
			*/
			
			# Get all Normal Assessment
			$sql = "SELECT
						a.assessment_id
					FROM
						{$course_db}.assessment AS a
					WHERE
						a.status<>1 AND a.fromlesson IS NULL AND a.handinby = 0";
			$normal_assessment = $this->returnVector($sql);
			
			# Get all assessment with specific targets
			$sql = "SELECT
						DISTINCT a.assessment_id
					FROM
						{$course_db}.assessment AS a INNER JOIN
						{$course_db}.grouping_function AS gf ON gf.function_type = 'A' AND a.assessment_id = gf.function_id
					WHERE
						a.status<>1 AND a.fromlesson IS NULL";
			$specific_targets_assessment = $this->returnVector($sql);
			
			# Get all assessment with specific targets that are assigned to the user
			$sql = "SELECT
						DISTINCT a.assessment_id
					FROM
						{$course_db}.assessment AS a INNER JOIN
						{$course_db}.grouping_function AS gf ON gf.function_type = 'A' AND a.assessment_id = gf.function_id
					WHERE
						a.status<>1 AND a.fromlesson IS NULL AND gf.user_id = {$user_id}";
			$user_involve_assessment = $this->returnVector($sql);
			
			# Get all assessment with specific targets that are assigned to the group which involve the user
			$sql = "SELECT
						DISTINCT a.assessment_id
					FROM
						{$course_db}.assessment AS a INNER JOIN
						{$course_db}.grouping_function AS gf ON gf.function_type = 'A' AND a.assessment_id = gf.function_id INNER JOIN
						{$course_db}.user_group AS ug ON gf.group_id = ug.group_id
					WHERE
						a.status<>1 AND a.fromlesson IS NULL AND ug.user_id = {$user_id}";
			$user_group_involve_assessment = $this->returnVector($sql);
			
			# Get all assessment with no specific targets
			$no_specific_targets_assessment = array_diff($normal_assessment, $specific_targets_assessment);
			
			# Get all assessment that assigned to the user
			$allAssess = array_unique(array_merge($no_specific_targets_assessment, $user_involve_assessment, $user_group_involve_assessment));
			sort($allAssess);
			
			if (count($allAssess)>0){
				$assessmentSql = implode(',',$allAssess);

				/*
				# Old Query - no longer use since 201104-08
				$sql = "SELECT afm.assessment_id FROM {$course_db}.assessment_final_mark afm 
						 inner join {$course_db}.assessment as a on
						 afm.assessment_id in ($assessmentSql) and afm.assessment_id = a.assessment_id
						 LEFT JOIN {$course_db}.grouping_function gf ON gf.function_id = a.assessment_id AND gf.function_type = 'A'
						where (
						  (a.handinby = 1 AND afm.group_id = gf.group_id)
						  OR
						  (a.handinby = 0 AND afm.user_id = {$user_id})
						 ) and IFNULL(afm.mark, '') <> '' AND afm.is_final = 1 
						 group by afm.assessment_id";
				*/
				$sql = "(
							SELECT afm.assessment_id 
							FROM {$course_db}.assessment_final_mark afm
							inner join {$course_db}.assessment as a on
								afm.assessment_id in ($assessmentSql) and 
								afm.assessment_id = a.assessment_id and a.handinby = 1 
							LEFT JOIN {$course_db}.grouping_function gf ON 
							    gf.function_id = a.assessment_id AND gf.function_type = 'A' and 
							    afm.group_id = gf.group_id 
							where 
							    IFNULL(afm.mark, '') <> '' AND afm.is_final = 1
							group by afm.assessment_id
						)
						UNION ALL
						(
							SELECT afm.assessment_id 
							FROM {$course_db}.assessment_final_mark afm
							inner join {$course_db}.assessment as a on
								afm.assessment_id in ($assessmentSql) and 
								afm.assessment_id = a.assessment_id and a.handinby = 0 
							LEFT JOIN {$course_db}.grouping_function gf ON 
							    gf.function_id = a.assessment_id AND gf.function_type = 'A'
							where 
							    IFNULL(afm.mark, '') <> '' AND afm.is_final = 1 and 
							    afm.user_id = {$user_id} 
							group by afm.assessment_id
						) ";
				$finalMarkAssess = $this->returnVector($sql);

				/*
				# Old Query - no longer use since 201104-08
				$sql = "SELECT p.assessment_id FROM {$course_db}.phase p
						 JOIN {$course_db}.task t ON t.phase_id = p.phase_id
						 JOIN {$course_db}.handin h ON h.assignment_id = t.task_id
						 inner join {$course_db}.assessment as a on
						 p.assessment_id in ($assessmentSql) and p.assessment_id = a.assessment_id
						 LEFT JOIN {$course_db}.grouping_function gf ON gf.function_id = a.assessment_id AND gf.function_type = 'A'
						 where 
						 (
						  (a.handinby = 1 AND h.group_id = gf.group_id)
						  OR
						  (a.handinby = 0 AND h.user_id = {$user_id})
						 )
						 AND (h.status='L' OR h.status='LR' OR h.status='D' OR h.status='DR') 
						 group by p.assessment_id";
				*/
				$sql = "(
							SELECT p.assessment_id 
							FROM {$course_db}.phase p
							JOIN {$course_db}.task t ON t.phase_id = p.phase_id
							JOIN {$course_db}.handin h ON 
							    h.assignment_id = t.task_id and 
							    (h.status='L' OR h.status='LR' OR h.status='D' OR h.status='DR')
							inner join {$course_db}.assessment as a on
							    p.assessment_id in ($assessmentSql) and 
							    p.assessment_id = a.assessment_id and 
							    a.handinby = 1 
							LEFT JOIN {$course_db}.grouping_function gf ON 
							    gf.function_id = a.assessment_id AND gf.function_type = 'A' and 
							    h.group_id = gf.group_id 
							group by p.assessment_id
						) 
						UNION ALL
						(
							SELECT p.assessment_id 
							FROM {$course_db}.phase p
							JOIN {$course_db}.task t ON t.phase_id = p.phase_id
							JOIN {$course_db}.handin h ON 
							    h.assignment_id = t.task_id and 
							    (h.status='L' OR h.status='LR' OR h.status='D' OR h.status='DR') and 
							    h.user_id = {$user_id} 
							inner join {$course_db}.assessment as a on
							    p.assessment_id in ($assessmentSql) and 
							    p.assessment_id = a.assessment_id and 
							    a.handinby = 0
							LEFT JOIN {$course_db}.grouping_function gf ON 
								gf.function_id = a.assessment_id AND gf.function_type = 'A'
							group by p.assessment_id
						) ";
				$phaseAssess = $this->returnVector($sql); 
				
				$allAssess = array_diff($allAssess,$finalMarkAssess);
				$allAssess = array_diff($allAssess,$phaseAssess);
			}
		}
		return count($allAssess);
	}
	
	function getAssessmentStatusCountSQL($course_id, $user_id)
	{
    global $eclass_prefix;
    
    $course_db = $eclass_prefix."c$course_id";
    if($user_id!='')
    {
      $sql = "SELECT
      	(SELECT COUNT(DISTINCT a.assessment_id) FROM {$course_db}.assessment a
      		LEFT JOIN {$course_db}.grouping_function gf ON gf.function_id = a.assessment_id AND gf.function_type = 'A'
      		LEFT JOIN {$course_db}.user_group ug ON ug.group_id = gf.group_id AND ug.group_id <> 0
      		WHERE
      		a.status <> 1 -- status 0: draft
      		AND (
      			(gf.function_id IS NULL AND a.handinby = 0) -- handinby 0: individual
      			OR
      			(gf.user_id = u.user_id)
      			OR
      			(ug.user_id = u.user_id)
      		)
      		AND NOT EXISTS (
      			SELECT 1 FROM {$course_db}.assessment_final_mark afm
      			WHERE afm.assessment_id = a.assessment_id
      			AND (
      				(a.handinby = 1 AND afm.group_id = gf.group_id) -- handinby 1: group
      				OR
      				(a.handinby = 0 AND afm.user_id = u.user_id)
      			)
      			AND IFNULL(afm.mark, '') <> '' AND afm.is_final = 1
      		)
      		AND NOT EXISTS (
      			SELECT 1 FROM {$course_db}.phase p
      			JOIN {$course_db}.task t ON t.phase_id = p.phase_id
      			JOIN {$course_db}.handin h ON h.assignment_id = t.task_id
      			WHERE p.assessment_id = a.assessment_id
      			AND (
      				(a.handinby = 1 AND h.group_id = gf.group_id) -- handinby 1: group
      				OR
      				(a.handinby = 0 AND h.user_id = u.user_id)
      			)
      			AND (h.status='L' OR h.status='LR' OR h.status='D' OR h.status='DR')
      		)
      	) AS num_of_not_yet_started_assessments
        FROM {$course_db}.usermaster u
        WHERE u.membertype = 'S' AND u.status IS NULL AND u.user_id='$user_id'";
    }
    
    return $sql;
  }
	#######################################################################################
	# eClass Panel [End]
	#######################################################################################
	
	#########################################################################################
	# Repair System [Start]
	#########################################################################################
	function isRepairSystemMgmtGroupMember($parUserID="")
	{
		if($parUserID!="") {
			$sql = "SELECT COUNT(*) FROM REPAIR_SYSTEM_GROUP_MEMBER WHERE UserID=$parUserID";	
			$result = $this->returnVector($sql);
			
			return $result[0];
		}
	}
	
	function RepairSystemPendingCount()
	{
		global $UserID;
		
		if($_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'])
		{	//20150121 following repair system index.php logic 
			// do not include those record with a deleted REQUEST_SUMMARY
			$sql = "select count(*) 
					from REPAIR_SYSTEM_RECORDS as c
					INNER JOIN REPAIR_SYSTEM_REQUEST_SUMMARY as e on (e.Title=c.Title and e.CategoryID=c.CategoryID  )
					where 
						c.RecordStatus IN (1 ,4) AND e.RecordStatus = 1";
		}
		else 
		{
			$sql = "
				select 
					count(c.RecordID)
				from 
					REPAIR_SYSTEM_GROUP_MEMBER as a
					left join REPAIR_SYSTEM_CATEGORY as b on (b.GroupID = a.GroupID)
					left join REPAIR_SYSTEM_RECORDS as c on (c.CategoryID = b.CategoryID)
					INNER JOIN REPAIR_SYSTEM_REQUEST_SUMMARY as e on (e.Title=c.Title and e.CategoryID=c.CategoryID  )
				where 
					a.UserID = $UserID and
					c.RecordStatus IN (1 ,4) and
					e.RecordStatus = 1
			";
		}
		
		$result = $this->returnVector($sql);
		return $result[0];
	}
	#########################################################################################
	# Repair System [End]
	#########################################################################################
	
	function hasiTextbookClass()
 	{
 		global $eclass_db;
 		$sql =  "SELECT COUNT(*) FROM ".$eclass_db.".course where roomtype=7";
 		$count_obj = $this->returnVector($sql);
 		return $count_obj[0];	
 	}
 	
 	function hasPoemsAndSongsClass()
 	{
 		global $eclass_db;
 		$sql = "SELECT COUNT(*) FROM ".$eclass_db.".course where roomtype=7 AND course_code='poemsandsongs'";	
 		$count_obj = $this->returnVector($sql);
 		return $count_obj[0];	
 	}
 	
 	#########################################################################################
	# PowerLesson [Start]
	#########################################################################################
    
    function get_lesson_session($course_id){
		global $intranet_db, $eclass_prefix;
		$this->session_gc($course_id);		
		$db =$eclass_prefix."c".$course_id;	
		$sql = "select 
					s.session_id, s.lesson_plan_id, s.teacher_id, s.std_notify_file, s.tea_notify_file, 
					s.course_id, s.scoreboard_table, s.last_visited, s.no_student, s.login_stud, 
					s.lesson_started, s.teacher_in_lesson, s.subject_id, s.lesson_title, ".getNameFieldByLang2('iu.')." as username 
				from {$db}.lesson_session as s 
				inner join {$db}.usermaster as u on 
					s.teacher_id = u.user_id 
				inner join {$intranet_db}.INTRANET_USER as iu on 
					iu.UserEmail = u.user_email 
				where 
					unix_timestamp() - s.last_visited <= ".$this->session_timeout." and 
					s.course_id = '".$course_id."'";
		return $this->returnArray($sql); 
	}
	
	function session_gc($course_id = ''){
		global $eclass_prefix;
		$course_id = empty($course_id)?$_SESSION['ck_course_id']:$course_id;
		$db = $eclass_prefix."c".$course_id;
		$sql= "select session_id,scoreboard_table, std_notify_file, tea_notify_file from {$db}.lesson_session where unix_timestamp() - last_visited > ".$this->session_timeout;
		$rubblish = $this->returnArray($sql);
		if (count($rubblish) > 0){
			$sub_sql = "";
			foreach ($rubblish as $r){
				$sub_sql .= "'".$r['session_id']."',";
				if (file_exists($this->notify_file_path.$r['std_notify_file'])){
					unlink($this->notify_file_path.$r['std_notify_file']);
				}
				if (file_exists($this->notify_file_path.$r['tea_notify_file'])){
					unlink($this->notify_file_path.$r['tea_notify_file']);
				}
				$sql = "Replace into {$db}.scoreboard_history(plan_section_id, teacher_id, title, section_type, function_id, duration, is_using, finished, waiting, sequence_no, freeze,  dirty, topic_title, serializedObj, message, attachment, subject, plan_id, lesson_tool) 
					select plan_section_id, '".$r['teacher_id']."', title, section_type, function_id, duration, is_using, finished, waiting, sequence_no, freeze,  dirty, topic_title, serializedObj, message, attachment, subject, '".$r['lesson_plan_id']."', lesson_tool
					from {$db}.".$r['scoreboard_table'];
				$this->db_db_query($sql);
				
				$sql ="drop table if exists {$db}.".$r['scoreboard_table'];
				$this->db_db_query($sql);
				
				$r['duration'] = empty($r['duration'])?0:$r['duration'];
				$sql = "update {$db}.lesson_plan 
					set avail_candidate = '".mysql_real_escape_string($r['avail_candidate'])."',
					real_duration = ifnull(real_duration,0) + ".$r['duration'].",
					is_used = 1,
					real_lesson_date = if(real_lesson_date = '0000-00-00 00:00:00', '".$r['start_date']."', real_lesson_date)
					where plan_id = '".$r['lesson_plan_id']."'";
				$this->db_db_query($sql);
				
				$sql ="drop table if exists {$db}.".$r['scoreboard_table'];
				$this->db_db_query($sql);
			}			
			$sub_sql = rtrim($sub_sql,',');
			$sql = "delete from {$db}.lesson_session where session_id in ($sub_sql)";
			$this->db_db_query($sql);
		}
	}
 	
 	#########################################################################################
	# PowerLesson [End]
	#########################################################################################
	
	#########################################################################################
	# Document Routing [Start]
	#########################################################################################
	function getDrUserCurrentInvolvedEntryCount() {
		global $docRoutingConfig;
		
		$targetUserId = $_SESSION['UserID'];
		
		// get document info
		$sql = "Select
						e.DocumentID
				From
						INTRANET_DR_ENTRY as e
						Inner Join INTRANET_DR_ROUTES as r On (e.DocumentID = r.DocumentID)
						Left Outer Join INTRANET_DR_ROUTE_TARGET idrt ON (r.RouteID = idrt.RouteID And idrt.UserID = '".$targetUserId."')
						Left Outer Join INTRANET_DR_STAR as star On (e.DocumentID = star.DocumentID And star.UserID = '".$targetUserId."')
				Where
						e.RecordStatus = '".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']."'
						And (	
								e.UserID = '".$targetUserId."'
								Or r.UserID = '".$targetUserId."'
								Or idrt.UserID = '".$targetUserId."' 
							)
				Group By
						e.DocumentID
				";
		$documentInfoAry = $this->returnResultSet($sql);
		$documentIdAry = Get_Array_By_Key($documentInfoAry, 'DocumentID');
		$numOfDocument = count($documentIdAry);
		
		// get route info
		$sql = "Select
						DocumentID,
						RouteID,
						EffectiveType,
						EffectiveStartDate,
						EffectiveEndDate
				From 
						INTRANET_DR_ROUTES 
				Where 
						RecordStatus='".$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Active']."' 
						And DocumentID IN ('".implode("','", (array)$documentIdAry)."')
				ORDER BY
						DisplayOrder, RouteID
				";
		$routeInfoAry = $this->returnResultSet($sql);
		$routeIdAry = Get_Array_By_Key($routeInfoAry, 'RouteID');
		$routeInfoAssoAry = BuildMultiKeyAssoc($routeInfoAry, 'DocumentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		
		// get user route info
		$sql = "SELECT  
					RouteID,
					UserID,
					RecordStatus
				FROM INTRANET_DR_ROUTE_TARGET 
				WHERE 	RouteID IN (".implode(",", (array)$routeIdAry).") 
						AND AccessRight = '".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']."'
				ORDER BY RouteID";
		$userTargetRouteAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), array('RouteID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		
		
		$involvedEntryAry = array();
		for ($i=0; $i<$numOfDocument; $i++) {
			$_documentId = $documentIdAry[$i];
			$_routeInfoAry = (array)$routeInfoAssoAry[$_documentId];
			$_numOfRoute = count($_routeInfoAry);
			
			$_foundPrevRouteFinished = false;
			for ($j=$_numOfRoute-1; $j>=0; $j--) {
				$__routeId = $_routeInfoAry[$j]['RouteID'];
				$__effectiveType = $_routeInfoAry[$j]['EffectiveType'];
				$__effectiveStartDate = $_routeInfoAry[$j]['EffectiveStartDate'];
				$__effectiveEndDate = $_routeInfoAry[$j]['EffectiveEndDate'];
				
				$__routeTargetUserAssoAry = BuildMultiKeyAssoc((array)$userTargetRouteAssoAry[$__routeId], 'UserID');
				$__targetUserRouteStatus = $__routeTargetUserAssoAry[$targetUserId]['RecordStatus'];
				
				// check if user is in this route
				if (isset($__routeTargetUserAssoAry[$targetUserId]) && $__targetUserRouteStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
					// pending records
				}
				else {
					continue;
				}
				
				if ($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']) {
					// route by period => check route date range
					$__effectiveStartDateTs = strtotime($__effectiveStartDate);
					$__effectiveEndDateTs = strtotime($__effectiveEndDate);
					$__curTs = strtotime('now');
					
					if ($__effectiveStartDateTs <= $__curTs && $__curTs <= $__effectiveEndDateTs) {
						$involvedEntryAry[$_documentId][$__routeId] = true;
					}
				}
				else if ($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished']) {
					if (!$_foundPrevRouteFinished) {
						if ($j == 0) {
							// all routes are not finished => should be in first route
							$involvedEntryAry[$_documentId][$__routeId] = true;
						}
						else {
							// check if the previous route are all finished => if so, this is the current route
							$__previousRouteId = $_routeInfoAry[$j-1]['RouteID'];
							$__routeTargetUserStatusAry = Get_Array_By_Key((array)$userTargetRouteAssoAry[$__previousRouteId], 'RecordStatus');
							$__numOfStatus = count($__routeTargetUserStatusAry);
							
							if ($__numOfStatus == 0) {
								$__isAllCompleted = false;
							}
							else {
								$__isAllCompleted = true;
								for ($k=0; $k<$__numOfStatus; $k++) {
									$___recordStatus = $__routeTargetUserStatusAry[$k];
									
									if ($___recordStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
										$__isAllCompleted = false;
										break;
									}
								}
							}
							
							
							if ($__isAllCompleted) {
								$involvedEntryAry[$_documentId][$__routeId] = true;
								$_foundPrevRouteFinished = true;
							}
						}
					}
				}
			}
		}
		
		return count((array)$involvedEntryAry);
	}
	#########################################################################################
	# Document Routing [End]
	#########################################################################################
	
	#########################################################################################
	# Apply Leave (app) [Start]
	#########################################################################################
	function getApplyLeaveTodayPendingRecord() {
		$sql = "SELECT
						COUNT(*) as recordNum
				FROM
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						INNER JOIN YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID)
						INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
				WHERE
						r.IsDeleted = '0'
						/* And Date(r.InputDate) = Date(now()) */
						AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						AND r.ApprovalStatus = '0' ";
		$recordAry = $this->returnResultSet($sql);
		return $recordAry[0]["recordNum"];
	}
	
	function getClassTeacherApplyLeaveTodayPendingRecord($YearClassList) {
		$sql = "SELECT
						COUNT(*) as recordNum
				FROM
						CARD_STUDENT_APPLY_LEAVE_RECORD as r
						INNER JOIN YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID)
						INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
				WHERE
						r.IsDeleted = '0'
						/* And Date(r.InputDate) = Date(now()) */
						AND yc.YearClassID IN ('".implode("', '", (array)$YearClassList)."')
						AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						AND r.ApprovalStatus = '0' ";
		$recordAry = $this->returnResultSet($sql);
		return $recordAry[0]["recordNum"];
	}
	#########################################################################################
	# Apply Leave (app) [End]
	#########################################################################################


    #####################################
    # Reprint Card (app) Logo [Start]
    #####################################

    function getReprintCardPendingRecord() {
        $sql = "SELECT
						COUNT(*) as recordNum
				FROM
						REPRINT_CARD_RECORD as r
						INNER JOIN YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID)
						INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
				WHERE
                        yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						AND r.CardStatus = '0' ";
        $recordAry = $this->returnResultSet($sql);
        return $recordAry[0]["recordNum"];
    }

    #####################################
    # Reprint Card (app) Logo [End]
    #####################################

	#########################################################################################
	# Document Routing [End]
	#########################################################################################
	
	#########################################################################################
	# HKUGAC Portal Cust [Start]
	#########################################################################################
	function getSubjectClassMapping($course_idArr){
		global $intranet_session_language;
		$NameField = $intranet_session_language=='b5'?'Chi':'Eng';
		$Name2Field = $intranet_session_language=='b5'?'CH':'EN';
		$Name3Field = $intranet_session_language=='b5'?'B5':'EN';
		$sql = "select  /*lc.LearningCategoryID,
					 lc.Name{$NameField} as catName,*/
					 s.{$Name2Field}_DES as SubjectName,
					 /*s.RecordID,
					 c.ClassTitle{$Name3Field} as SubjectGroupName,
					 c.SubjectGroupID,*/
					 c.course_id
				from LEARNING_CATEGORY as lc Left join ASSESSMENT_SUBJECT as s on
					s.RecordStatus = 1 AND
					(s.CMP_CODEID IS NULL OR s.CMP_CODEID = '') AND
					lc.LearningCategoryID = s.LearningCategoryID
				Left join SUBJECT_TERM as t on
					t.SubjectID = s.RecordID
				Left join SUBJECT_TERM_CLASS as c on
					c.SubjectGroupID = t.SubjectGroupID and
					c.course_id is not null and 
					c.course_id <> 0
				where
					lc.RecordStatus = 1
					and c.course_id IN ('".implode("','",(array)$course_idArr)."')
				order by lc.DisplayOrder ASC, s.DisplayOrder asc, SubjectName ASC,c.ClassCode asc";
		$sqlResult = $this->returnArray($sql); 

		return $sqlResult;
	}
	
	function custDisplayUserEClass_HKUGAC($UserEmail, $rightpanel=0, $roomType=0){
		global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
	  	global $i_status_suspended, $image_path, $LAYOUT_SKIN;
	    
	    $row = $this->returnEClassUserIDCourseIDStandard($UserEmail, $roomType);
	    $countRow = count($row);
    
	    if($countRow==0){
	    	$x .= "<tr>
    				<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
    				<td width=\"90%\" class=\"indextabclassiconoff\" align=\"center\" >$i_no_record_exists_msg</td>
    				<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
    			</tr>";
	    }
	    else{
	    	
	    	// Get Subect Data
	    	$course_idArr = Get_Array_By_Key($row,'course_id');
		    $mapping = $this->getSubjectClassMapping($course_idArr);
		    
		    foreach( (array)$mapping as $key=>$Arr){
		    	$mapping[$key]['Order'] = $key;
		    }
		    $mappingAssoc = BuildMultiKeyAssoc($mapping,'course_id');
		    
		    // Get Order from $mappingAssoc (Subject)
		    foreach( (array)$row as $key=>$rowInfo){
		    	if($mappingAssoc[$rowInfo[0]]['Order'] != '' || $mappingAssoc[$rowInfo[0]]['Order']>-1){
		    		$row[$key]['Order'] = $mappingAssoc[$rowInfo[0]]['Order'];
		    	}
		    	else{
		    		$row[$key]['Order'] = 'Other';		    		
		    	}
		    }
		    
		    // Re-Sort $row
	    	sortByColumn3($row,'Order','course_code','course_name');
	    	
	    	$x = '';
	    	for($i=0; $i<$countRow; $i++){
		    	$course_id = $row[$i][0];
	  			$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
	  			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
	  			$user_id = $row[$i][3];
	  			$memberType = $row[$i][4];
	  			$lastused = $row[$i][5];
	  			$user_course_id = $row[$i][6];
	  			$status = $row[$i][7]; 
	  			$subjectName = (empty($mappingAssoc[$course_id])?'Others' : $mappingAssoc[$course_id]['SubjectName']);
	  			# get notes count / display
	  			list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);			
	  			# get assessment count / display
	  			list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);			
	  			# get forum count / display
	  			list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
	        	# get announcement count / display
	  			list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
	  			
	  			
  				 if ($i%2==0)
			      $RowClass = " class=\"indextabwhiterow\" ";
			    else 
			      $RowClass = " class=\"indextabbluerow\" ";
			   	
		        $whatsnew = "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" >";
		  			
		  			
		        $whatsnew .= "<tr>";
		        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$notes_display."</td>":"";
		        $whatsnew .= ($memberType =='S')?"<td align=\"center\" >".$ass_display."</td>":"";
		        $whatsnew .= "<td align=\"center\" >".$forum_display."</td>";
		        $whatsnew .= "<td align=\"center\" >".$annc_display."</td>";
		        $whatsnew .= "</tr>";
		        $whatsnew .= "</table>";
		  																
		        if (($count_notes == 0 || $memberType !='S') && ($count_assessment==0 || $memberType !='S') && ($count_forum==0) && ($count_announcement==0))
		          $whatsnew2 = false;
		        else $whatsnew2 = true;
		        
		        $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";
			  	
			  	if( ($i+1)%5 == 1){
			    $x .= "<tr>";
			  	}
			  	
				$x .= "<td width=\"20%\" style=\"vertical-align: top;\">";
				$x .= "<table width='100%' height='100%'><tr><td class=\"indexnewslist\">$subjectName</td></tr><tr><td $RowClass>";
			    $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			  		        
			    if ($status == "suspended"){
			      $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
			      
			      $x .= "<tr>";
			      $x .= "<td width=\"19\">{$class_icon}</td>";    		        
			      $x .= "<td wrap=true>$course_code - $course_name ($memberType - <i>$i_status_suspended</i>) </td>";																
			      $x .= "</tr>";										        
			    } else {
			      
			      if ($whatsnew2)
			        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class_new.gif\" >";
			      else		        
			        $class_icon = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/icon_class.gif\" >";
			      
			      $x .= "<tr>";
			      $x .= "<td width=\"19\">{$class_icon}</td>";
			  		$x .= "<td wrap=true><a href=\"javascript:fe_eclass('{$user_course_id}')\" class=\"indextabclasslist\">$course_code - $course_name ($memberType) </a></td>";																
			  		$x .= "</tr>";										        			        
			    }
			    
			    $x .= "<tr>";
			    $x .= "<td >&nbsp;</td>";
			    $x .= "<td >{$whatsnew}</td>";																
			    $x .= "</tr>";										        
			    
			    $x .= "</table> \n";
			    $x .= "</td>";
			    $x .= "</tr>";
			    $x .= "</table>";
			    $x .= "</td>";
			    if($countRow < 5 && ($i== ($countRow-1)) ){
			    	$count = 0;
			    	for($j = $i; $j<4 ; $j++){
			    		$count++;
			    		$x .= '<td width="20%">&nbsp;</td>';
			    		if($count > 4){
			    			break;
			    		}
			    	}
			    }
			    if( ($i+1)%5 == 0){
					$x .= "</tr> \n";
			    }
	    	}
	    }
	    return $x;
	}
	#########################################################################################
	# HKUGAC Portal Cust  [End]
	#########################################################################################
	

	#########################################################################################
	# Amway Portal Cust  [Start]
	#########################################################################################
	
  	function custDisplayUserEClass_Amway($UserEmail, $roomType=0){
    
	    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
	  	global $i_status_suspended, $image_path, $LAYOUT_SKIN, $Lang;
  	
	    $row = $this->returnEClassUserIDCourseIDStandard($UserEmail, $roomType);
  	
  		$x = '';
	    if(sizeof($row)==0)
	  	{		
	        $x .= "	<div class=\"portal_classroom_list\">
       					<div class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</div>
       				</div>";	
	  	}
	  	else
	  	{
	  		for($i=0,$iMax=count($row); $i<$iMax; $i++)
	  		{
	  			$course_id = $row[$i][0];
	  			$course_code = intranet_wordwrap($row[$i][1],40,"\n",1);
	  			$course_name = intranet_wordwrap($row[$i][2],40,"\n",1);
	  			$user_id = $row[$i][3];
	  			$memberType = $row[$i][4];
	  			$lastused = $row[$i][5];
	  			$user_course_id = $row[$i][6];
	  			$status = $row[$i][7]; 
	  			$course_desc = intranet_wordwrap($row[$i][8],40,"\n",1);
	  			
	  			# get notes count / display
	  			list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);			
	  			# get assessment count / display
	  			list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);			
	  			# get forum count / display
	  			list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
	        	# get announcement count / display
	  			list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
	  			
	  			// overwrite display
	  			$notes_display = "<span class=\"".($count_notes?"":"icon_dim ")."icon_econtent\">{$count_notes}</span>";
	  			$ass_display = "<span class=\"".($count_assessment?"":"icon_dim ")."icon_assessment\">{$count_assessment}</span>";
	  			$forum_display = "<span class=\"".($count_forum?"":"icon_dim ")."icon_forum\">{$count_forum}</span>";
	  			$annc_display = "<span class=\"".($count_announcement?"":"icon_dim ")."icon_annoucement\">{$count_announcement}</span>";

				$x .= "	<div class=\"portal_classroom_list\">	  			
	                    	<div class=\"classroom_group\">
								<div class=\"classroom_photo\"><a href=\"javascript:fe_eclass('{$user_course_id}')\"><img src=\"".($this->geteClassLogo($course_id))."\"/></a></div>
	                            <div class=\"classroom_name\"><a href=\"javascript:fe_eclass('{$user_course_id}')\">{$course_code} - {$course_name}</a><em>{$course_desc}</em></div>
	                            <p class=\"spacer\"></p>
							</div>
	                        <div class=\"classroom_icon_group\">
								{$notes_display}
								{$ass_display}
								{$forum_display}
								{$annc_display}
							</div>
	                        <div class=\"classroom_bottom\">                        
	                   			<span class=\"icon_lastvisit\">{$i_frontpage_eclass_lastlogin}: {$lastused}</span>
	                            <input name=\"submit1\" type=\"button\" value=\"".$Lang['Portal']['Course']['Enter']."\" class=\"portal_classroom_button\" onClick=\"javascript:fe_eclass('{$user_course_id}')\"/>
	                            <p class=\"spacer\"></p>
	                        </div>
						</div>";
	  		}	  			
	  	}
	  	return $x;
	}
	#########################################################################################
	# Amway Portal Cust  [End]
	#########################################################################################

	#########################################################################################
	# Oaks Portal Cust  [Start]
	#########################################################################################
	
  	function custDisplayUserEClass_Oaks($UserEmail, $roomType=0){
  		return $this->custDisplayUserEClass_Amway($UserEmail, $roomType);
	}
	#########################################################################################
	# Oaks Portal Cust  [End]
	#########################################################################################

	#########################################################################################
	# CEM_Macau Portal Cust  [Start]
	#########################################################################################
	
	function custDisplayUserEClass_CEM($UserEmail, $roomType=0){
	    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
	    global $i_status_suspended, $image_path, $LAYOUT_SKIN, $Lang;
	    
	    $row = $this->returnEClassUserIDCourseIDStandard($UserEmail, $roomType);
	    
	    $x = '';
	    if(sizeof($row)==0)
	    {
	        $x .= "	<div class=\"portal_classroom_list\">
	        <div class=\"indextabclassiconoff\" align='center'><br /><br /><br /><br /><br />$i_no_record_exists_msg</div>
	        </div>";
	    }
	    else
	    {
	        for($i=0,$iMax=count($row); $i<$iMax; $i++)
	        {
	            $course_id = $row[$i][0];
	            $course_code = intranet_wordwrap($row[$i][1],40,"\n",1);
	            $course_name = intranet_wordwrap($row[$i][2],40,"\n",1);
	            $user_id = $row[$i][3];
	            $memberType = $row[$i][4];
	            $lastused = $row[$i][5];
	            $user_course_id = $row[$i][6];
	            $status = $row[$i][7];
	            $course_desc = intranet_wordwrap($row[$i][8],40,"\n",1);
	            
	            # get notes count / display
	            list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);
	            # get assessment count / display
	            list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);
	            # get forum count / display
	            list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
	            # get announcement count / display
	            list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
	            
	            // overwrite display
	            $notes_display = "<span class=\"".($count_notes?"":"icon_dim ")."icon_econtent\">{$count_notes}</span>";
	            $ass_display = "<span class=\"".($count_assessment?"":"icon_dim ")."icon_assessment\">{$count_assessment}</span>";
	            $forum_display = "<span class=\"".($count_forum?"":"icon_dim ")."icon_forum\">{$count_forum}</span>";
	            $annc_display = "<span class=\"".($count_announcement?"":"icon_dim ")."icon_annoucement\">{$count_announcement}</span>";
	            
	            $x .= "	<div class=\"portal_classroom_list\">
    	            <div class=\"classroom_group\">
        	            <div class=\"classroom_photo\"><a href=\"javascript:fe_eclass('{$user_course_id}')\"><img src=\"".($this->geteClassLogo($course_id))."\"/></a></div>
                        <div class=\"classroom_info\">
        	                <div class=\"classroom_name\"><a href=\"javascript:fe_eclass('{$user_course_id}')\">{$course_code} - {$course_name}</a><em>{$course_desc}</em></div>
            	            <div class=\"classroom_icon_group\">
                	            {$notes_display}
                	            {$ass_display}
                	            {$forum_display}
                	            {$annc_display}
            	            </div>
    	                    <div class=\"classroom_bottom\">
    	                        <span class=\"icon_lastvisit\">{$i_frontpage_eclass_lastlogin}: {$lastused}</span>
    	                        <input name=\"submit1\" type=\"button\" value=\"".$Lang['Portal']['Course']['Enter']."\" class=\"portal_classroom_button\" onClick=\"javascript:fe_eclass('{$user_course_id}')\"/>
    	                    </div>
    	                </div>
    	            </div>
                </div>";
	        }
	    }
	    return $x;
	}
	#########################################################################################
	# CEM_Macau Portal Cust  [End]
	#########################################################################################
	
	#########################################################################################
	# Centennial College EAP Portal Cust  [Start]
	#########################################################################################
	function returnEClassUserIDCourseID_CCEAP($Email, $roomType=0){
    
    global $eclass_db, $sys_custom;
    if($sys_custom['project']['centennialcollege']){
		include_once("../cc_eap/settings.php");
	}    
    
    if($sys_custom['centennialcollege']['dev']['on']){
		$cond = "AND a.course_code LIKE '%EAP_%'";
		$code_prefix = "EAP_";
	}else{
		$cond = "";
		$code_prefix = "";
	}
    
    $fieldname  = "a.course_id, a.course_code, a.course_name, ";
    $fieldname .= "b.user_id, b.memberType, b.lastused, b.user_course_id ,b.status, a.course_desc";
    $sql = "SELECT $fieldname FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b WHERE a.course_id = b.course_id AND b.user_email = '$Email' AND a.RoomType='$roomType' AND (b.status is NULL OR b.status NOT IN ('deleted')) $cond GROUP BY a.course_id ORDER BY a.course_id";

    $unorderedList = $this->returnArray($sql);
    $orderedList = array();
    
    $order = array(
    		$code_prefix."S",
    		$code_prefix."L",
    		$code_prefix."R",
    		$code_prefix."W",
    		$code_prefix."G",
    		$code_prefix."V",
    		$code_prefix."P",
    );
    for($i=0; $i < count($unorderedList); $i++){
    	$position = array_search($unorderedList[$i]['course_code'], $order);
    	$orderedList[$position] = $unorderedList[$i];
    }
	
    ksort($orderedList, SORT_NUMERIC);
    
    return $orderedList;
  }
	
	function custDisplayUserEClass_CCEAP($UserEmail, $roomType=0, $isMobile=false){
    
    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
  	global $i_status_suspended, $image_path, $LAYOUT_SKIN;
  	
    $row = $this->returnEClassUserIDCourseID_CCEAP($UserEmail, $roomType);

			
  		for($i=0; $i<sizeof($row); $i++)
  		{
  			$course_id = $row[$i][0];
  			$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
  			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
  			$user_id = $row[$i][3];
  			$memberType = $row[$i][4];
  			$lastused = $row[$i][5];
  			$user_course_id = $row[$i][6];
  			$status = $row[$i][7]; 
  			
  			# get notes count / display
  			list($count_notes, $notes_display) = $this->returnNotesCount($course_id, $user_id,$memberType);			
  			# get assessment count / display
  			list($count_assessment, $ass_display) = $this->returnAssessmentCount($course_id, $user_id,$memberType);			
  			# get forum count / display
  			list($count_forum, $forum_display) = $this->returnBulletinCount($course_id, $user_id,$memberType);
        	# get announcement count / display
  			list($count_announcement, $annc_display) = $this->returnAnnouncementCount($course_id, $user_id,$memberType);
  			
  																			
        if (($count_notes == 0 || $memberType !='S') && ($count_assessment==0 || $memberType !='S') && ($count_forum==0) && ($count_announcement==0))
          $whatsnew2 = false;
        else $whatsnew2 = true;
        
        $lastlogin = $lastused ? $i_frontpage_eclass_lastlogin. " : " . $lastused : "&nbsp;";

        $x .= $this->EAPeClassTableDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $isMobile);
      }
  		

  	
  	return $x;
  
  }
  
  function getEAPeClassTableIconId($eClass_title){
  	$icon_id = 0;
  	switch(trim($eClass_title)){
    	case 'Academic Writing':
    		$icon_id=1;
    		break;
    	case 'Academic Presentation':
    		$icon_id=2;
    		break;
    	case 'Grammar':
    		$icon_id=3;
    		break;
    	case 'Academic Vocabulary':
    		$icon_id=4;
    		break;
    	case 'Academic Reading':
    		$icon_id=5;
    		break;
    	case 'Academic Listening':
    		$icon_id=6;
    		break;
    	case 'Academic Speaking':
    		$icon_id=7;
    		break;						
    }
    return $icon_id;
  }
  
  function EAPeClassTableDisplay($i, $status, $course_code, $course_name, $memberType, $user_course_id, $isMobile=false)
  {
    global $i_frontpage_eclass_lastlogin,$i_status_suspended;
    global $image_path,$LAYOUT_SKIN;
    
    $eClass_title = $course_name;
    
    $icon_id = $this->getEAPeClassTableIconId($eClass_title);
    if($isMobile){
    	$x .= '<a href="/home/eLearning/login.php?uc_id='.$user_course_id.'" class="list-group-item">
					<div class="classroom-icon"><icon class="course-icon t'.$icon_id.'"/></div><div class="word"><vert>'.$eClass_title.'</vert></div><div class="word arrow"><vert><span class="glyphicon glyphicon-menu-right"></span></vert></div>
				</a>';
    }else{
    	
    
		$x .= '<div class="icon_obj t'.$icon_id.'" onclick="javascript:fe_eclass('.$user_course_id.')">
					<div class="word">
						<div>'.$eClass_title.'</div>
					</div>
					<div class="icon">
						<div class="bg"></div>
						<div class="pic"></div>
					</div>
				</div>';
    } 
    
    
    return $x;
  }
  
  function EAPeClassTableNews($UserEmail='', $roomType=0, $isMobile=false){
  	global $eclass_db, $sys_custom, $eclass_prefix, $eclass40_filepath, $isTeaching;
  	$cfg_eclass_lib_path =  $eclass40_filepath."/src/includes/php";
  	include_once($eclass40_filepath."/src/includes/php/lib-groups.php");
  	include_once($eclass40_filepath."/src/includes/php/lib-note.php");

  	$x .= '';
  	$Year = date("Y");
  	$month = date("m");
  	$startYear = $Year;
  	if($month < 9){
  		$startYear = intval($Year) - 1;
  		$semStart = $startYear."-09-01 00:00:00";
  	}else{
  		$semStart = $Year."-09-01 00:00:00";
  	}
  	$dateOfThisMonth = $Year."-".$month."-01 00:00:00";	
	
	//$monthInBetween = (($Year - $startYear) * 12) + ($month - 9);
	$monthInBetween = 2;

	for($i=0; $i <= $monthInBetween; $i++){
		$displayMonth = date("F", strtotime($dateOfThisMonth." -".$i." month"));
		$considerMonth = date("m", strtotime($dateOfThisMonth." -".$i." month"));
		if($i==0){
			$nextMonth = date("m", strtotime($dateOfThisMonth." +1 month"));
		}else{
			$nextMonth = date("m", strtotime($dateOfThisMonth." -".($i-1)." month"));
		}		
		$dateOfMonth = $Year."-".$considerMonth."-01 00:00:00";
		$dateOfNextMonth = 	$Year."-".$nextMonth."-01 00:00:00";
		$eclassCourse = $this->returnEClassUserIDCourseID_CCEAP($UserEmail, $roomType);
		$newsArr = array();
		foreach($eclassCourse as $course){
			$course_id = $course[0];
			$course_name = intranet_wordwrap($course[2],30,"\n",1);
			$memberType = $course[4];
			$user_course_id = $course[6];
			$course_db = $eclass_prefix."c".$course_id;
			$sql = "SELECT user_id FROM ".$course_db.".usermaster WHERE intranet_user_id='".$_SESSION['UserID']."'";
			$userID = $this->returnVector($sql);
			$lg = new libgroups($course_db);
			$notes_id_blocked = $lg->returnFunctionIDs("NOTE",$userID[0]);
			$assessment_id_blocked = $lg->returnFunctionIDs("A",$userID[0]);
			if($memberType == 'S'){
				$sub_query_assessment = "  AND assessment_id NOT IN (".$assessment_id_blocked.") ";
				$sub_query_notes = "  AND notes_id NOT IN (".$notes_id_blocked.") ";
			}else{
				$sub_query_assessment = '';
				$sub_query_notes = '';
			}
			    	
			$sql = "SELECT 'A' as Type, assessment_id, '--' as notes_id, title, '".$user_course_id."' as uc_id FROM ".$course_db.".assessment WHERE (startdate>= '".$dateOfMonth."' AND startdate< '".$dateOfNextMonth."' AND status='2') $sub_query_assessment
					UNION
					SELECT 'NOTE' as Type, '--' as assessment_id, notes_id, title, '".$user_course_id."' as uc_id FROM ".$course_db.".notes WHERE ((starttime >= '".$dateOfMonth."' AND starttime < '".$dateOfNextMonth."') OR (starttime='0000-00-00 00:00:00' AND inputdate>= '".$dateOfMonth."' AND inputdate< '".$dateOfNextMonth."')) $sub_query_notes AND status='1';";

			$newList = $this->returnArray($sql);
			if(empty($newList))continue;

			$newsArr[$course_name] = $newList;					
		}
		if(empty($newsArr))	continue;
		if($isMobile){
			$x .= '<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">'.$displayMonth.'</h3>
					</div>';
			$x .= ' <div class="panel-body">
						<div class="list-group">';
		}else{
			if($x != '')$x .= '<hr/>';
			$x .= '			<div class="subtitle">'.$displayMonth.'</div>';
		}
		foreach($newsArr as $name=>$list){
			$icon_id = $this->getEAPeClassTableIconId($name);
			foreach($list as $item){
				if($item['Type']=='NOTE'){
					$path = "src/econtent/econtent.php?nid=".$item['notes_id'];
				}else if ($item['Type']=='A'){
					if($isTeaching){
						$path = "src/assessment/assessment_new2.php?assessment_id=".$item['assessment_id'];
					}else{
						$path = "src/assessment/std_assessment_detail2.php?assessment_id=".$item['assessment_id'];
					}
				}
				if($isMobile){
					$x .= '<a href="/home/eLearning/login.php?uc_id='.$item['uc_id'].'&jumpback=directEntry&redirectPath='.$path.'" class="list-group-item">
								<h4 class="list-group-item-heading">'.$name.'<icon class="course-icon-s t'.$icon_id.'"/></h4>
								<p class="list-group-item-text">'.$item['title'].'</p>
							</a>';
				}else{
					$x .= '<hr/>';
					$x .='<div class="item_obj t'.$icon_id.'">
								<div class="icon"><div></div></div>
								<div class="ititle" onclick="window.open(\'/home/eLearning/login.php?uc_id='.$item['uc_id'].'&jumpback=directEntry&redirectPath='.$path.'\', \'_blank\')">'.$item['title'].'</div>
							</div>';
				}
			}		
		}
		if($isMobile){
			$x .= '		</div>
					</div>
				</div>';
		}			
	}
  	
  	$dateOfNextMonth = $Year."-".date("m",  strtotime('+1 month'))."-01 00:00:00";
  	$monthDisplay = date("F");
  	$sql = "";
  	
  	
  	
  	return $x;
  }
  
	#########################################################################################
	# Centennial College EAP Portal Cust  [End]
	#########################################################################################

	#########################################################################################
	# Classroom @ Student App Classroom List Cust  [Start]
	#########################################################################################
	function classroom_list_appview($UserEmail, $roomType=0){
    
	    global $i_no_record_exists_msg, $i_frontpage_eclass_courses, $i_frontpage_eclass_lastlogin, $i_frontpage_eclass_whatsnews;
	  	global $i_status_suspended, $image_path, $LAYOUT_SKIN,$eclass_db, $Lang;
	  	
	    $fieldname  = "a.course_id, a.course_code, a.course_name, ";
	    $fieldname .= "b.user_id, b.memberType, b.lastused, b.user_course_id ,b.status, a.course_desc";
	    $sql = "SELECT $fieldname FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b WHERE a.course_id = b.course_id AND b.user_email = '$UserEmail' AND a.RoomType='$roomType' AND (b.status is NULL OR b.status NOT IN ('deleted'))  GROUP BY a.course_id ORDER BY a.course_id";
	
	    $row = $this->returnArray($sql);
		
		if(empty($row)){
			$x .= '<div class="panel panel-default">
						<div class="panel-heading">
							<div class="classroom-icon"></div>
							<h3 class="panel-title">'.$Lang['eclass']['warning']['noClassEnrolled'].'</h3>
						</div>
						
					</div>';
			return $x;
		}else{		
		  	for($i=0; $i<sizeof($row); $i++){
		  		$course_id = $row[$i][0];
		  		$course_code = intranet_wordwrap($row[$i][1],30,"\n",1);
		  		$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
		  		$user_id = $row[$i][3];
		  		$memberType = $row[$i][4];
		  		$lastused = $row[$i][5];
		  		$user_course_id = $row[$i][6];
		  		$status = $row[$i][7]; 
		  		
		
		        $eClass_title = $course_name;
	    
	    		$icon = $this->geteClassLogo($course_id);
	    		$icon = (strstr($icon,"eclass_logo"))?"":$icon;
	    		$path = "src/assessment/assessment.php?from_app=1";
				$path = '/home/eLearning/login.php?uc_id='.$user_course_id.'&jumpback=directEntry&redirectPath='.$path;
	/*
	    		$x .= '<a href="/home/eLearning/login.php?uc_id='.$user_course_id.'" class="list-group-item">
						<div class="classroom-icon"><icon class="course-icon t'.$icon_id.'"/></div><div class="word"><vert>'.$eClass_title.'</vert></div><div class="word arrow"><vert><span class="glyphicon glyphicon-menu-right"></span></vert></div>
					</a>';
	*/				
				$x .= '<div class="panel panel-default">
						<div class="panel-heading">
							<div class="classroom-icon"><icon class="course-icon-s" style="background: url('.$icon.'); background-size: 33px 33px; "></icon></div>
							<h3 class="panel-title">'.$course_name.'</h3>
						</div>
						<div class="panel-body">
							<div class="list-group">';
				$x .= $this->getClassLatestActivity($course_id, $user_course_id, $user_id);											
				$x .= '	</div>
							<div class="btn-enterClassroom">
								<a href="'.$path.'">'.$Lang['General']['More'].' <span class="glyphicon glyphicon-menu-right"></span></a>
							</div>
						</div>
					</div>';	
	
		     }
		}	
	  	return $x;
	  
	}
	
	function getClassLatestActivity($course_id, $user_course_id, $ck_user_id){
		global $eclass_db,$eclass_prefix, $eclass40_filepath, $i_frontpage_assessment,$Lang;
		include_once($eclass40_filepath."/src/includes/php/lib-groups.php");
		$course_db = $eclass_prefix."c".$course_id;
		$lg = new libgroups($course_db);
		# Get Specific Group Assessment related to the user
		$grpFtnIDArr = $lg->Get_Group_FunctionID('A', $ck_user_id);
		$assessmentid_list .= (count($grpFtnIDArr) > 0) ? implode(",", $grpFtnIDArr) : '';

		# Get Specific Individual Assessment related to the user
		$indiFtnIDArr = $lg->Get_Individual_FunctionID('A', $ck_user_id);
		$assessmentid_list .= ($assessmentid_list != '' && count($indiFtnIDArr) > 0) ? ',' : '';
		$assessmentid_list .= (count($indiFtnIDArr) > 0) ? implode(",", $indiFtnIDArr) : '';
		
		# Get Individual Assessment without assign specific individual, ie. all related students
		$allIndiFtnIDArr = $lg->Get_All_Individual_FunctionID('A');
		$assessmentid_list .= ($assessmentid_list != '' && count($allIndiFtnIDArr) > 0) ? ',' : '';
		$assessmentid_list .= (count($allIndiFtnIDArr) > 0) ? implode(",", $allIndiFtnIDArr) : '';
		
		$assessmentid_list = ($assessmentid_list != '') ? $assessmentid_list : "''";
		
		$sql = "SELECT assessment_id, title, enddate 
				FROM {$course_db}.assessment 
				WHERE enddate > NOW() AND startdate <= NOW() AND assessment_id IN (".$assessmentid_list.") AND status=2 
				ORDER BY enddate DESC LIMIT 3";
		$row = $this->returnArray($sql);
		foreach($row as $i=>$v){
			$path = "src/assessment/std_assessment_detail2.php?assessment_id=".$v['assessment_id'];
			$path = '/home/eLearning/login.php?uc_id='.$user_course_id.'&jumpback=directEntry&redirectPath='.$path;
			$x .= ' <a href="'.$path.'" class="list-group-item">
						<h4 class="list-group-item-heading">'.$i_frontpage_assessment.': '.$v['title'].'</h4>
						<p class="list-group-item-text">'.$Lang['General']['EndDate'].': '.$v['enddate'].'</p>
					</a>';
		}
		return $x;
	}
	#########################################################################################
	# Classroom @ Student App Classroom List Cust [End]
	#########################################################################################

	## return eClass logo by course_id
	function geteClassLogo($course_id) {
		global $eclass_db,$eclass_prefix,$eclass40_filepath,$eclass40_httppath;
		
		if($course_id==$eclass_db){
			$cid = $eclass_db;
		} else {
			$cid = $eclass_prefix."c".$course_id;
		}
		if(file_exists($eclass40_filepath."/files/".$cid."/logo.jpg")){
			$logo = "http://".$eclass40_httppath."files/".$cid."/logo.jpg";			// reference path: IP25 root dir
		}else{	
			$logo = "http://".$eclass40_httppath."images/top/eclass_logo.png";
		}
		return $logo;		
	}
}
?>