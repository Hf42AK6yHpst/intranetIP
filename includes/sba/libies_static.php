<?php
/*******************************************
*	modification log
*		2015-06-24 Siuwan
*			- modified function getStageMarkingCriteria() to get chinese and english title
*/

// STATIC FUNCIONT MAY HAVE PROBLEM, PLEASE CHECK
/**********************************************************************
 * Eric Yip (20100622):
 * - Used in
 *   - /home/eLearning/ies/admin/management/marking/scheme_student_final.php
 *   - /home/eLearning/ies/admin/management/marking/scheme_stage_final.php
 *   - /home/eLearning/ies/admin/management/marking/scheme_task.php
 *   - /home/eLearning/ies/admin/management/marking/scheme_score_export.php
 * - All static functions, no need to init an object
 **********************************************************************/

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
//include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
class libies_static {

  public static function getSchemeStudentArr($scheme_id)
  {
    global $intranet_db;

    $ldb = new libdb();
    $sql = "SELECT schemestudent.UserID, ".getNameFieldByLang2("iu.")." ";
    $sql .= "FROM {$intranet_db}.IES_SCHEME_STUDENT schemestudent ";
    $sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserID = schemestudent.UserID ";
    $sql .= "WHERE schemestudent.SchemeID = '{$scheme_id}' ";
    $sql .= "ORDER BY iu.ClassName, iu.ClassNumber";
    $returnArr = $ldb->returnArray($sql);

    return $returnArr;
  }

  public static function getSchemeTeacherType($parSchemeID, $parUserID)
  {
    global $intranet_db;

    $ldb = new libdb();
    $sql = "SELECT TeacherType ";
    $sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER ";
    $sql .= "WHERE SchemeID = '{$parSchemeID}' AND UserID = '{$parUserID}'";
    $returnArray = $ldb->returnVector($sql);

    return $returnArray;
  }

  public static function isStageAllowed($parStageID, $parStudentID)
  {
    global $intranet_db, $ies_cfg;

    $ldb = new libdb();

    $sql = "SELECT Approval ";
    $sql .= "FROM {$intranet_db}.IES_STAGE_STUDENT ";
    $sql .= "WHERE StageID = '{$parStageID}' AND UserID = '{$parStudentID}'";
    $stageStatus = current($ldb->returnVector($sql));

    return ($stageStatus == $ies_cfg["DB_IES_STAGE_STUDENT_Approval"]["approved"]);
  }

  public static function getNextTask($parTaskID)
  {
    global $intranet_db;

    $ldb = new libdb();

    $sql = "SELECT StageID, Sequence FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$parTaskID}'";
    list($stageID, $task_seq) = current($ldb->returnArray($sql));

    $sql = "SELECT TaskID ";
    $sql .= "FROM {$intranet_db}.IES_TASK ";
    $sql .= "WHERE StageID = '{$stageID}' AND Sequence > '{$task_seq}' AND Enable = 1 ";
    $sql .= "ORDER BY Sequence ";
    $sql .= "LIMIT 1";
    $nextTaskID = current($ldb->returnVector($sql));

    return $nextTaskID;
  }

  public static function isTaskNeedAllowance($parTaskID)
  {
    global $intranet_db;

    $ldb = new libdb();

    $sql = "SELECT Approval ";
    $sql .= "FROM {$intranet_db}.IES_TASK ";
    $sql .= "WHERE TaskID = '{$parTaskID}'";
    $taskApproval = current($ldb->returnVector($sql));

    return ($taskApproval == 1);
  }

  public static function isTaskAllowed($parTaskID, $parStudentID)
  {
    global $intranet_db, $ies_cfg;

    $ldb = new libdb();

    $sql = "SELECT Approved ";
    $sql .= "FROM {$intranet_db}.IES_TASK_STUDENT ";
    $sql .= "WHERE TaskID = '{$parTaskID}' AND UserID = '{$parStudentID}'";
    $taskStatus = current($ldb->returnVector($sql));

    return ($taskStatus == $ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"]);
  }

  public static function getBatchCommentArr($batchID){
    global $intranet_db;

    $ldb = new libdb();

    $sql = "SELECT comment.Comment, DATE_FORMAT(comment.DateInput, '%d-%m-%Y') AS DateInput, comment.BatchCommentID, ".getNameFieldByLang("iu.")." AS CommentTeacher ";
    $sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_COMMENT AS comment ";
    $sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserID = comment.InputBy ";
    $sql .= "WHERE comment.BatchID = '{$batchID}' order by comment.DateInput DESC";
    $batchCommentArr = $ldb->returnArray($sql);

    return $batchCommentArr;
  }

  public static function getStageFinalHandinArr($stage_id, $user_id)
  {
    global $intranet_db;

    $ldb = new libdb();
    $sql = "SELECT batch.BatchID, DATE_FORMAT(batch.SubmissionDate, '%Y-%m-%d') AS BatchSubmissionDate, batch.BatchStatus, handin_file.FileID, handin_file.FileName, handin_file.FileHashName, DATE_FORMAT(handin_file.DateInput, '%Y-%m-%d') AS UploadDate ";
    $sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH batch ";
    $sql .= "LEFT JOIN {$intranet_db}.IES_STAGE_HANDIN_FILE handin_file ";
    $sql .= "ON batch.BatchID = handin_file.BatchID ";
    $sql .= "WHERE batch.StageID = '{$stage_id}' AND batch.UserID = '{$user_id}' ";
    $sql .= "ORDER BY batch.DateModified DESC";

    $stageHandinArr = $ldb->returnArray($sql);

    $returnArr = array();
    for($i=0, $i_max=count($stageHandinArr); $i<$i_max; $i++)
    {
      $t_batch_id = $stageHandinArr[$i]["BatchID"];
      $t_batch_status = $stageHandinArr[$i]["BatchStatus"];
      $t_batch_submissiondate = $stageHandinArr[$i]["BatchSubmissionDate"];
      $t_file_id = $stageHandinArr[$i]["FileID"];
      $t_file_name = $stageHandinArr[$i]["FileName"];
      $t_file_hashname = $stageHandinArr[$i]["FileHashName"];
      $t_file_date = $stageHandinArr[$i]["UploadDate"];

      if($t_file_id != "")
      {
        $returnArr[$t_batch_id]["Files"][] =  array(
                                                "FileID" => $t_file_id,
                                                "FileName" => $t_file_name,
                                                "FileHashName" => $t_file_hashname,
                                                "FileDate" => $t_file_date
                                              );
      }
      $returnArr[$t_batch_id]["StageID"] = $stage_id;
      $returnArr[$t_batch_id]["UserID"] = $user_id;
      $returnArr[$t_batch_id]["BatchStatus"] = $t_batch_status;
      $returnArr[$t_batch_id]["BatchSubmissionDate"] = $t_batch_submissiondate;
    }

    return $returnArr;
  }

  /**
	*
	* @owner : ERIC (20101209)
	*
	*/
  public static function getMarkingCriteria(){
    global $intranet_db;

    $ldb = new libdb();
    $sql = "SELECT imc.MarkCriteriaID, ".Get_Lang_Selection("imc.TitleChi", "imc.TitleEng")." as CRITERIATITLE ";
    $sql .= "FROM {$intranet_db}.IES_COMBO_MARKING_CRITERIA imc ";
    $sql .= "ORDER BY imc.defaultSequence";
    $returnArr = $ldb->returnArray($sql);

    return $returnArr;
  }

  /**
	*
	* @owner : ERIC (20101209)
	*
	*/
  public static function getStageMarkingCriteria($schemeID, $parStageIDs=array())
  {
    global $intranet_db, $ies_cfg;

    $stageIDstr = is_array($parStageIDs) ? implode(", ", $parStageIDs) : $parStageIDs;

    $ldb = new libdb();
    $sql = "SELECT imc.MarkCriteriaID, ".Get_Lang_Selection("imc.TitleChi", "imc.TitleEng")." as CRITERIATITLE, imc.TitleChi, imc.TitleEng,";
    $sql .= "ismc.task_rubric_id AS TASK_RUBRIC_ID, ";   // No task rubric id if no stage ID is set
    $sql .= "ismc.MaxScore, ismc.Weight, ";
    $sql .= "ismc.StageMarkCriteriaID, ismc.RecordStatus ";
    $sql .= "FROM {$intranet_db}.IES_COMBO_MARKING_CRITERIA imc ";
    $sql .= "INNER JOIN {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ON (ismc.MarkCriteriaID = imc.MarkCriteriaID) ";
    $sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ON ismc.StageID = stage.StageID ";
    $sql .= "WHERE stage.SchemeID = '{$schemeID}' ";
    $sql .= ($stageIDstr == "") ? "" : "AND ismc.StageID IN ({$stageIDstr}) ";
    $sql .= "ORDER BY imc.defaultSequence";
    $returnArr = $ldb->returnArray($sql);


    return $returnArr;
	}

  public static function getCurrentMark($ParAssignmentID, $ParAssignmentType, $ParMarkCriteria)
  {
    global $intranet_db;
     // To Select the max(DateModified), so limit 1
    $ldb = new libdb();
    $sql = "SELECT Score ";
    $sql .= "FROM {$intranet_db}.IES_MARKING ";
    $sql .= "WHERE AssignmentID = '{$ParAssignmentID}' AND AssignmentType = '{$ParAssignmentType}' AND MarkCriteria = '{$ParMarkCriteria}' ";
    $sql .= "ORDER BY DateModified DESC ";
    $sql .= "LIMIT 1";

    $returnVal = current($ldb->returnVector($sql));



    return $returnVal;
  }

  public static function getLastMarker($ParAssignmentID, $ParAssignmentType)
  {
    global $intranet_db;
    // To Select the max(DateModified), so limit 1
    $ldb = new libdb();
    $sql = "SELECT m.MarkerID, ".getNameFieldByLang("iu.")." AS MarkerName, DATE_FORMAT(m.DateModified, '%Y-%m-%d') AS LastModified ";
    $sql .= "FROM {$intranet_db}.IES_MARKING m ";
    $sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON m.MarkerID = iu.UserID ";
    $sql .= "WHERE m.AssignmentID = '{$ParAssignmentID}' AND m.AssignmentType = '{$ParAssignmentType}' ";
    $sql .= "ORDER BY m.DateModified DESC ";
    $sql .= "LIMIT 1";

    $returnArr = current($ldb->returnArray($sql));

    return $returnArr;
  }

  public static function getBatchStrArr($stage_detail_arr, $batchArr, $markingCriteriaArr, $rubricDetailArr, $teacherTypeArr=array(), $ParSchemeLang='b5', $SchemeType=''){
    global $ies_cfg, $Lang;

    $lastHandin = true;
    // IES admin or assess teacher of scheme
    $isAssessTeacher = ($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || in_array($ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"], $teacherTypeArr));
    $stageMaxScore = $stage_detail_arr["MaxScore"];
	$SchemeID = $stage_detail_arr["SchemeID"];

//debug_r($stage_detail_arr);
    $returnStrArr = array();
    if(count($batchArr) > 0)
    {
		$libIES = new libies();
      foreach($batchArr AS $batchID => $batchDetailArr)
      {

        $t_batch_submissiondate = empty($batchDetailArr["BatchSubmissionDate"]) ? "--" : $batchDetailArr["BatchSubmissionDate"];
        $t_batch_stage_id = $batchDetailArr["StageID"];


		$taskDetails = array();
		if(is_numeric($t_batch_stage_id)){
			$taskDetails = $libIES->GET_TASK_ARR($t_batch_stage_id);
		}

        $t_batch_user_id = $batchDetailArr["UserID"];

        /**************************************************
         * Student handin Cell
         **************************************************/

        $returnStr = "<td class=\"student_ans\">";
        $returnStr .= "<ul class=\"attachment_list\">";

        $batchStatus = $batchDetailArr["BatchStatus"];

        $answerEncodeNo = base64_encode($batchID);

        $batchFileArr = $batchDetailArr["Files"];
        $fileListStr = "";
        if(count($batchFileArr) > 0)
        {
          for($i=0, $i_max=count($batchFileArr); $i<$i_max; $i++)
          {
            $fileID = $batchFileArr[$i]["FileID"];
            $fileName = $batchFileArr[$i]["FileName"];
            $fileHashName = $batchFileArr[$i]["FileHashName"];
            $fileDate = $batchFileArr[$i]["FileDate"];

            $fileListStr .= "<li><a href=\"index.php?mod=admin&task=download&fhn={$fileHashName}\">{$fileName}</a><br /><span class=\"update_record\">({$fileDate})</span></li>";
          }
        }
        else
        {
          $fileListStr .= "<span class=\"update_record\">{$Lang['IES']['SubmittingFilesDeleted']}</span>";
        }
        $returnStr .= ($fileListStr == "") ? "--" : $fileListStr;
        $returnStr .= "</ul>";
        $returnStr .= "</td>";

        /**************************************************
         * Comment Cell
         **************************************************/
        $commentArr = self::getBatchCommentArr($batchID);
        for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
        {
          $t_comment = nl2br($commentArr[$j]["Comment"]);
          $t_inputdate = $commentArr[$j]["DateInput"];
          $t_teacher = $commentArr[$j]["CommentTeacher"];

          $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
          $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:void(0);\" onclick=\"DeleteBatchComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
		      $commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
         // $commentStr .= "<input type=\"button\" value=\"{$Lang['IES']['Delete2']}\" onmouseout=\"this.className='formsmallbutton'\" onmouseover=\"this.className='formsmallbuttonon'\" class=\"formsmallbutton\" onclick=\"DeleteComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" style=\"float: right;\">";
          $commentStr .= "<div class=\"edit_top\"></div>";
        }

				############# Start: The sliding selection of category
				$h_categoryListing = self::getCommentCategoryListing($answerEncodeNo,$ParSchemeLang, $SchemeType);
				############# End: The sliding selection of category

        $returnStr .= "<td class=\"teacher_comment\">";
        $returnStr .= ($lastHandin) ? "<div class=\"commentbox_btn\"><a href=\"javascript:;\" name=\"addComment\">".$Lang['IES']['NewComment']."</a> </div>" : "&nbsp;";
       // $comment_block = "<span style=\"padding-top: 2px; float: left;\"></span>";
        $returnStr .= <<<HTML
<form action="stage_comment_update.php">
<div name="comment_box" class="IES_comment_box" style="display:none">
  <span class="commentbank_btn commentbank_btn1"  answerEncodeNo={$answerEncodeNo}><a href="javascript: void(0);">{$Lang['IES']['CommentBank']}</a></span>
  <span style="float:right; padding-top:2px"><a href="javascript:;" name="closeComment">{$Lang['IES']['Close']}</a></span>
  <span class="commentbank_btn commentbank_btn2"><a href="javascript:;">{$Lang['IES']['CommentCategory']}</a><br/></span>
  <div style="position:absolute; margin-left: 60px; margin-top: 4px; display:none; background-color:#F8F8F8;" class="categoryList">{$h_categoryListing}</div>
  <div class="clear"></div>
  <div class="IES_select_commentbank" style="display:none">

  </div>
  <textarea name="{$answerEncodeNo}" id="text_{$answerEncodeNo}" class="IES_comment_textarea" style="width:99%" rows="6"></textarea>
  <input style="float:right" onClick="addBatchComment(this)" type="button" class="formsmallbutton "
	 onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="{$Lang['IES']['SubmitRequest']}"
	onmouseover="this.className='formbuttonon'" />
</div>

<input type="hidden" name="stage_id" id="stage_id" value="{$t_batch_stage_id}" />
<input type="hidden" name="StudentID" id="StudentID" value="{$t_batch_user_id}" />
<input type="hidden" name="mod" value="ajax" />
<input type="hidden" name="task" value="ajax_admin" />
<input type="hidden" name="ajaxAction" value="AddStageComment" />

</form>
<div class="clear"></div>
HTML;

        $returnStr .= "<div class=\"IES_comment_string\">".$commentStr."</div>";
        $returnStr .= "</td>";

        /**************************************************
         * Marking Cell
         **************************************************/

        $returnStr .= "<td class=\"teacher_comment\">";
        //$returnStr .= ($batchStatus != $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"]) ? "<div class=\"rubricsbox_btn\"><a href=\"javascript:;\" name=\"addMarking\"><!--{{-->給予評分<!--}}--></a> </div>" : "&nbsp;";
        //$returnStr .= ($lastHandin) ? "<div class=\"rubricsbox_btn\"><a href=\"javascript:;\" name=\"addMarking\"><!--{{-->給予評分<!--}}--></a> </div>" : "&nbsp;";

        switch($batchStatus)
        {
          case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]:
            $returnStr .= ($isAssessTeacher) ? "<input style=\"float:right\" name=\"redo\" type=\"button\" batch_id=\"{$batchID}\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['Redo']}\" />" : "";
            //$returnStr .= "<div class=\"commentbox_btn\"> <a href=\"javascript:;\" name=\"redo\" batch_id=\"{$batchID}\">".$Lang['IES']['Redo']."</a> </div>";
            //$returnStr .= ($batchStatus == $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]) ? "<div class=\"commentbox_btn\"> <a href=\"javascript:;\" name=\"approve\" batch_id=\"{$batchID}\">".$Lang['IES']['Approve']."</a> </div>" : "{{BATCH APPROVED}}";
            break;
          case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]:
            $returnStr .= "<span class=\"IES_redo\">{$Lang['IES']['RedoSent']}</span>";
            break;
          case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["approved"]:
            $returnStr .= "{{BATCH APPROVED}}";
            break;
        }

        foreach($markingCriteriaArr AS $markingCriteria)
        {
          $_criteria_code = $markingCriteria["MarkCriteriaID"];
          $_criteria_name = $markingCriteria["CRITERIATITLE"];
          $_criteria_task_rubric_id = $markingCriteria["TASK_RUBRIC_ID"];
          $_criteria_weight = $markingCriteria["Weight"];
          $_criteria_maxScore = $markingCriteria["MaxScore"];
          $_criteria_status = $markingCriteria["RecordStatus"];

			/*
		  if($_criteria_maxScore=="") {
		  	$html_mark_criteria = "<tr><td colspan=100%><font color='red'>".$Lang['SBA']['AlertMsg']['MarkingCriteriaNotComplete']."</font></td></tr>";
		  } else {
			*/
	          $_html_rubric_table = "";
	          $_html_th = "";
	          $_html_td = "";

	          if(!empty($_criteria_task_rubric_id))
	          {
	            $_rubric_detail_arr = $rubricDetailArr[$_criteria_task_rubric_id];

	            for($j=0, $j_max=count($_rubric_detail_arr); $j<$j_max; $j++)
	            {
	              $_rubric_score = $_rubric_detail_arr[$j]["score"];
	              $_rubric_score_from = $_rubric_detail_arr[$j]["score_from"];
	              $_rubric_score_to = $_rubric_detail_arr[$j]["score_to"];
	              $_rubric_desc = $_rubric_detail_arr[$j]["description"];

	              $_html_th .= "<th title=\"{$_rubric_desc}\" nowrap>".$_rubric_score."</th>";
	              $_html_td .= "<td style=\"border-top-width:0px\" nowrap>".$_rubric_score_from." - ".$_rubric_score_to."</td>";
	            }
	            $_html_rubric_table .= "<table border=\"1\">";
	            $_html_rubric_table .= empty($_html_th) ? "" : "<tr>{$_html_th}</tr>";
	            $_html_rubric_table .= empty($_html_td) ? "": "<tr>{$_html_td}</tr>";
	            $_html_rubric_table .= "</table>";

	            unset($_th_arr);
	            unset($_td_arr);
	          }
	          //$displayOverall = 1;

	          $_assignment_mark = self::getCurrentMark($batchID, $ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"], $_criteria_code);

	          switch($_criteria_code)
	          {
	            case MARKING_CRITERIA_OVERALL:
	            //echo $displayOverall.'/';
	            	if($stageMaxScore=="") {
	            		//$displayOverall = 0;
	            		$html_mark_overall .= '<tr><td colspan="100%"><div class="tabletextrequire">'.$Lang['SBA']['AlertMsg']['MarkingCriteriaNotComplete'].'</div></td></tr>';
	            		continue;
	            	}

	              $html_mark_overall .= "<tr>";
	              //$html_mark_criteria .= "<td colspan=\"2\" align=\"right\"><em>Result</em></td>";
	              $html_mark_overall .= "<td colspan=\"2\" align=\"right\"><em>{$Lang['IES']['TotalMark']}</em></td>";
	              $html_mark_overall .= "<td class=\"rubrics_student_col_sum rubrics_student_col_sum_group\">";

	              $html_mark_overall .= ($isAssessTeacher && $lastHandin) ? "<input type=\"text\" size=\"3\" maxlength=\"3\" name=\"mark[{$answerEncodeNo}][{$_criteria_code}]\" value=\"{$_assignment_mark}\" maxScore=\"{$stageMaxScore}\" READONLY />" : $_assignment_mark;
	              $html_mark_overall .= " / {$stageMaxScore}";
	              $html_mark_overall .= "</td>";
	              $html_mark_overall .= "</tr>";
	              break;
	            default:

	            	if($_criteria_status==0) continue;

	            	//$displayOverall = ($_assignment_mark=="") ? 0 : 1;
	            	//$displayOverall = 1;

					$html_taskMark_Details = '';
					if($_criteria_code == $ies_cfg["marking_criteria"]["progress"][0]){
					  $html_taskMark_Details = "<div><a href=\"index.php?mod=admin&task=task_score_thick_box&KeepThis=true&stage_id=$t_batch_stage_id&StudentID=$t_batch_user_id&TB_iframe=true&height=400&width=700\" class=\"thickbox view_step\" title=\"{$Lang['IES']['StudentInputData3']}\">&nbsp;</a></div>";
					}
	              $html_mark_criteria .= "<tr class=\"rubrics_critera_row\">";
	              $html_mark_criteria .= "<td>{$_criteria_name}{$html_taskMark_Details}<br /></td>";
	              $html_mark_criteria .= "<td><em>".$_criteria_weight."</em></td>";
	              //$html_mark_criteria .= "<td>{$_html_rubric_table}</td>";
	              $html_mark_criteria .= "<td class=\"rubrics_student_col\">";
	              $html_mark_criteria .= "<table cellspacing=\"0\" cellpadding=\"0\">";
	              $html_mark_criteria .= "<tr>";
	              $html_mark_criteria .= "<td style=\"border-top-width:0px\" nowrap>";
	              $html_mark_criteria .= ($isAssessTeacher && $lastHandin && $stageMaxScore!="") ? "<input type=\"text\" size=\"1\" maxlength=\"3\" name=\"mark[{$answerEncodeNo}][{$_criteria_code}]\" weight=\"{$_criteria_weight}\" maxScore=\"{$_criteria_maxScore}\" value=\"{$_assignment_mark}\" />" : ($_assignment_mark=="" ? "-" : $_assignment_mark);
	              $html_mark_criteria .= " / {$_criteria_maxScore}";

	              //echo $_assignment_mark.'/';
	              //$displayOverall = ($_assignment_mark=="") ? 0 : 1;

								if($isAssessTeacher && $lastHandin)
			          {
			          	if(!empty($_criteria_task_rubric_id))
			          	{
				            $_rubric_detail_arr = $rubricDetailArr[$_criteria_task_rubric_id];

				            for($j=0, $j_max=count($_rubric_detail_arr); $j<$j_max; $j++)
				            {
				              $_rubric_score_from = $_rubric_detail_arr[$j]["score_from"];
				              $_rubric_score_to = $_rubric_detail_arr[$j]["score_to"];

				              $html_mark_criteria .= "<span name=\"range[{$answerEncodeNo}][]\" from=\"{$_rubric_score_from}\" to=\"{$_rubric_score_to}\" style=\"display:none\"></span>";
				            }
				          }
				          else{
										$html_mark_criteria .= "<span name=\"range[{$answerEncodeNo}][]\" from=\"0\" to=\"{$_criteria_maxScore}\" style=\"display:none\"></span>";
									}
			          }

	              $html_mark_criteria .= "</td>";
	              $html_mark_criteria .= "</tr>";
	              $html_mark_criteria .= ($_html_rubric_table == "") ? "" : "<tr><td style=\"border-top-width:0px\">{$_html_rubric_table}</td></tr>";
	              $html_mark_criteria .= "</table>";
	              //$html_mark_criteria .= "&nbsp;</td>";
	              $html_mark_criteria .= "</td>";
	              $html_mark_criteria .= "</tr>";
	              break;
	          }
		  //}
        }

        $lastMarkerArr = self::getLastMarker($batchID, $ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]);
        $markerName = $lastMarkerArr["MarkerName"];
        $lastModifiedDate = $lastMarkerArr["LastModified"];
        $html_latest_modified = empty($lastMarkerArr) ? "<br /><span class=\"update_record\" style=\"float:left\"></span>" : "<br /><span class=\"update_record\" style=\"float:left\">({$lastModifiedDate} {$markerName})</span>";



	        if($stageMaxScore!="") {
		        $html_give_mark_button = ($isAssessTeacher && $lastHandin) ? "<input style=\"float:right; margin-left:10px\" name=\"giveMark\" type=\"button\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['Submit']}\" onmouseover=\"this.className='formbuttonon'\" />" : "";
	        }


        if($batchStatus != $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"])
        {
          $returnStr .= <<<HTML
<form>
<div name="marking_box" class="IES_rubrics_box">
  <!--<span style="float:right; padding-top:2px"><a href="javascript:;" name="closeMarking">{$Lang['IES']['Close']}</a></span>-->
  <div class="clear"></div>

  <table class="rubrics_table">
    <col class="col_critera"/>

    <tr>
      <td><u>{$Lang['IES']['Criteria']}</u></td>
      <td width="60"><u>{$Lang['IES']['Weight']}</u></td>
      <td><u>{$Lang['IES']['Mark']}</u></td>
    </tr>
    {$html_mark_criteria}
    {$html_mark_overall}
  </table>
  {$html_latest_modified}
  {$html_give_mark_button}
  <div class="edit_top"></div>

</div>
<input type="hidden" name="AssignmentType" value="{$ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]}" />
<input type="hidden" name="stage_id" value="{$t_batch_stage_id}" />
<input type="hidden" name="StudentID" value="{$t_batch_user_id}" />
<input type="hidden" name="scheme_id" value="{$SchemeID}" />
<input type="hidden" name="ajaxAction" value="StageMarkUpdate" />
<input type="hidden" name="mod" value="ajax" />
<input type="hidden" name="task" value="ajax_admin" />
</form>
HTML;
        }
        else
        {
          $returnStr .= "<span class=\"update_record\">{$Lang['IES']['SubmittingNoMarking']}</span>";
        }

        $returnStr .= "</td>";

        $returnStrArr[] = $returnStr;

        unset($commentStr);
        unset($html_mark_criteria);
        unset($html_mark_overall);
        $lastHandin = false;
      }
    }
    else
    {
      $returnStr = "<td class=\"student_ans\">--</td>";
      $returnStr .= "<td class=\"teacher_comment\">&nbsp;</td>";
      $returnStr .= "<td class=\"teacher_comment\">";
      //$returnStr .= "<div class=\"rubricsbox_btn\"> <a href=\"#\">新增評量表</a> </div><div class=\"commentbox_btn\"> <a href=\"#\">新增評語</a> </div>
      $returnStr .= "&nbsp;</td>";

      $returnStrArr[] = $returnStr;
    }

    return $returnStrArr;
  }

  public static function getTaskCommentArr($SnapshotID){
    global $intranet_db;

    $ldb = new libdb();

    $sql = "SELECT comment.Comment, DATE_FORMAT(comment.DateInput, '%d-%m-%Y') AS DateInput, ".getNameFieldByLang("iu.")." AS CommentTeacher, comment.SnapshotCommentID FROM {$intranet_db}.IES_TASK_HANDIN_COMMENT AS comment INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserID = comment.InputBy WHERE comment.SnapshotAnswerID = '{$SnapshotID}' ORDER BY comment.DateInput DESC";
    $snapshotCommentArr = $ldb->returnArray($sql);
  //echo $sql.'<br><br>';
    return $snapshotCommentArr;
  }

   public static function getTaskScoreArr($SnapshotID){
    global $intranet_db;

    $ldb = new libdb();

    $sql = "SELECT score.score, DATE_FORMAT(score.DateInput, '%d-%m-%Y') AS DateInput, ".getNameFieldByLang("iu.")." AS scoreTeacher, score.SnapshotScoreID FROM {$intranet_db}.IES_TASK_HANDIN_SCORE AS score INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserID = score.InputBy WHERE score.SnapshotAnswerID = '{$SnapshotID}' ORDER BY score.DateInput DESC";


    $snapshotCommentArr = $ldb->returnArray($sql);

    return $snapshotCommentArr;
  }


  public static function getTaskAnswerStrArr($answerArr, $teacherTypeArr=array(), $stageID="",$ParSchemeLang='b5', $SchemeType=''){
    global $ies_cfg, $Lang;

    $lastHandin = true;
    // IES admin or assess teacher of scheme
    $isAssessTeacher = ($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || in_array($ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"], $teacherTypeArr));
    $objIES = new libies();
  	$comment_bank_arr = $objIES->getCommentBank('',$SchemeType);
    $c_no = 0;

    $returnStrArr = array();
    if(count($answerArr) > 0)
    {
      for($i=0, $i_max=count($answerArr); $i<$i_max; $i++)
      {
        $snapshotID = $answerArr[$i]["SnapshotAnswerID"];
        $taskID = $answerArr[$i]["TaskID"];
        $studentID = $answerArr[$i]["UserID"];

        /**************************************************
         * Student handin Cell
         **************************************************/
        $answerEncodeNo = base64_encode($snapshotID."_".$taskID."_".$studentID);
      //  debug_r($answerEncodeNo);


        $answer = $objIES->stringfilter($answerArr[$i]["Answer"]);
//        debug_r(($answerArr[$i]["Answer"]));

        $answerDate = $answerArr[$i]["AnswerDate"];
		    switch(strtoupper($answerArr[$i]["QuestionType"]))
		    {
          case "TABLE":
            ##################################################################################
            ## here is a set of temp. code that used to handle the table display, should be modified later,
            ## and is trying to get the max. step id inside a task to get the table column fields
            ##################################################################################
            $libdb = new libdb();
            $sql = "SELECT MAX(STEPID) FROM IES_STEP WHERE TASKID = $taskID";
            $stepId = current($libdb->returnVector($sql));
            $answer = $objIES->getQuestionAnswer_Table($answer, $stepId);
            break;
          case "SURVEY:EDIT":
//          debug_r($answer);
            $survey_arr = $objIES->parseSurveyMapString($answer);


            $answer = self::getTeacherViewSurveyAnswer($survey_arr, strtoupper($answerArr[$i]["QuestionType"]), $studentID, $taskID);
            break;
          case "SURVEY:COLLECT":
            $survey_arr = $objIES->getStudentStageSurvey($stageID, $studentID);
            $answer = self::getTeacherViewSurveyAnswer($survey_arr, strtoupper($answerArr[$i]["QuestionType"]), $studentID, $taskID);
            break;
          case "SURVEY:ANALYSIS":
            $survey_arr = $objIES->getStudentStageSurvey($stageID, $studentID);
            $answer = self::getTeacherViewSurveyAnswer($survey_arr, strtoupper($answerArr[$i]["QuestionType"]), $studentID, $taskID);
            break;
        }

        $returnStr = "<td class=\"student_ans\">".$answer."<br /><span class=\"update_record\">(".$answerDate.")</span></td>";

        /**************************************************
         * Comment Cell
         **************************************************/
//        $comment_bank = "<ul>";

//		############################# Start: Handling display B5 / EN Title
//		global $intranet_session_language;
//		switch(strtolower($intranet_session_language)) {
//			default:
//			case "b5":
//			$commentInLang = '';
//			break;
//			case "en":
//			$commentInLang = 'EN';
//			break;
//		}
//		############################# End: Handling display B5 / EN Title
//        foreach($comment_bank_arr as $value){
//          $comment_bank .= "<li onclick=\"add_from_commentbank(this,'h_commentbank_$c_no', 'text_{$answerEncodeNo}')\">".nl2br($value["Comment"])."</li><input type=\"hidden\" id=\"h_commentbank_$c_no\" VALUE=\"".$value["Comment"]."\">";
//          $c_no++;
//        }
//        unset($value);
//        $comment_bank .= "</ul>";

        $commentStr = "";
        //echo $snapshotID.'/';
        if(!empty($snapshotID))
        {
          $commentArr = self::getTaskCommentArr($snapshotID);
          for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
          {
            $t_comment = nl2br(htmlspecialchars($commentArr[$j]["Comment"]));
            $t_inputdate = $commentArr[$j]["DateInput"];
            $t_teacher = $commentArr[$j]["CommentTeacher"];


            $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
            $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:void(0);\" onclick=\"DeleteComment('".$commentArr[$j]["SnapshotCommentID"]."', $snapshotID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
            $commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
            $commentStr .= "<div class=\"edit_top\"></div>";

          }
        }

		############# Start: The sliding selection of category
		$h_categoryListing = self::getCommentCategoryListing($answerEncodeNo,$ParSchemeLang,$SchemeType);
		############# End: The sliding selection of category

        $returnStr .= "<td class=\"teacher_comment\">";

        $returnStr .= ($lastHandin) ? "<div class=\"commentbox_btn\"> <a href=\"javascript:;\" name=\"addComment\">".$Lang['IES']['NewComment']."</a> </div>" : "&nbsp;";


        $returnStr .= <<<HTML
<form action="task_comment_update.php">
<div name="comment_box" class="IES_comment_box" style="display:none;">

  <span class="commentbank_btn commentbank_btn1" answerEncodeNo={$answerEncodeNo}><a href="javascript:;">{$Lang['IES']['CommentBank']}</a></span>
  <span class="commentbank_btn commentbank_btn2"><a href="javascript:;">{$Lang['IES']['CommentCategory']}</a><br/></span>
  <div style="position:absolute; margin-left: 60px; margin-top: 4px; display:none; background-color:#F8F8F8;" class="categoryList">{$h_categoryListing}</div>

  <span style="float:right; padding-top:2px"><a href="javascript:;" name="closeComment">{$Lang['IES']['Close']}</a></span>
  <div class="clear"></div>
  <div class="IES_select_commentbank" style="display:none">

  </div>
  <textarea class="IES_comment_textarea" name="{$answerEncodeNo}" id="text_{$answerEncodeNo}" style="width:99%" rows="6"></textarea>
  <input style="float:right" onClick="addComment(this)" type="button" class="formsmallbutton "
	 onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="{$Lang['IES']['Submit']}"
	onmouseover="this.className='formbuttonon'" />
</div>
<input type="hidden" name="mod" id="mod" value="ajax" />
<input type="hidden" name="task" id="task" value="ajax_admin" />
<input type="hidden" name="ajaxAction" id="ajaxAction" value="AddComment">

</form>
<div class="clear"></div>
HTML;
		    $returnStr .= "<div class=\"IES_comment_string\">".$commentStr."</div>";
        $returnStr .= "</td>";


///////////////////////////////
// Start For Mark / Score Handling //
///////////////////////////////


		$scoreStr = '';
		$t_score = '';
		$t_SnapshotScoreID = '';
		if(empty($snapshotID)){
			// do nothing
		}else{
			$scoreArr = libies_static::getTaskScoreArr($snapshotID);

			if(is_array($scoreArr) && count($scoreArr) == 1){
				//suppose return one array only
				$scoreArr = current($scoreArr);
				$t_score = $scoreArr['score'];
				$t_inputdate = $scoreArr['DateInput'];
				$t_teacher = $scoreArr['scoreTeacher'];
				$t_SnapshotScoreID = $scoreArr['SnapshotScoreID'];

				$scoreStr .= "<span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span>";
			}

		}

$html_taskScoreUI = <<<HTML1
	<form action = "task_score_update.php">
		<div class="commentbox_btn">{$Lang['IES']['Mark']} : <input type = "text" maxlength="3" size = "3" name="{$answerEncodeNo}" id="text_score_{$answerEncodeNo}" value="{$t_score}"></div>
	  <input style="float:right" onClick="addTaskScore(this)" type="button" class="formsmallbutton "
	 onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="{$Lang['IES']['Submit']}"
	onmouseover="this.className='formbuttonon'" />
	<input type="hidden" name="mod" id="mod" value="ajax" />
	<input type="hidden" name="task" id="task" value="ajax_admin" />
	<input type="hidden" name="ajaxAction" id="ajaxAction" value="AddMark">
	</form>
HTML1;


		$returnStr .= "<td class=\"teacher_comment\">";
//		$returnStr .= ($lastHandin)? $html_taskScoreUI : ' &nbsp;';

		if($lastHandin){
			$returnStr .= $html_taskScoreUI;
		}else{
			if(!empty($t_SnapshotScoreID)){
				$returnStr .= '<br/>Mark : '.$t_score.'<br/>&nbsp;';
			}
		}

		$returnStr .= "<div class=\"IES_score_string\"><div style=\"width:100%; float:left;\">".$scoreStr."</div></div>";
		$returnStr .= "</td>";

///////////////////////////////
// End For Mark / Score Handling //
///////////////////////////////

        $returnStrArr[] = $returnStr;
        unset($commentArr);
        unset($html_mark_criteria);

        $lastHandin  = false;
      }
    }
    else
    {
      $returnStr = "<td class=\"student_ans\">{$Lang['IES']['NotYetAnswered']}</td>";
      $returnStr .= "<td class=\"teacher_comment\">";
      //$returnStr .= "<div class=\"rubricsbox_btn\"> <a href=\"#\">新增評量表</a> </div><div class=\"commentbox_btn\"> <a href=\"#\">新增評語</a> </div>
      $returnStr .= "&nbsp;</td>";

	  //For Mark Handling
	  $returnStr .= "<td class=\"teacher_comment\">&nbsp;</td>";


      $returnStrArr[] = $returnStr;
    }

    return $returnStrArr;
  }

	/**
	 * @author maxwong
	 * @param String AnswerEncodNo - base64_encode($snapshotID."_".$taskID."_".$studentID) / base64_encode($batchID) or sth else used
	 * @return String -HTML a ul listing of category titles
	 */
	public function getCommentCategoryListing($ParAnswerEncodeNo,$ParLanguage='b5',$SchemeType="") {
		switch(strtolower($ParLanguage)) {
			default:
			case "b5":
			$commentInLang = '';
			break;
			case "en":
			$commentInLang = 'EN';
			break;
		}
		$libies = new libies();
		############# Start: The sliding selection of category
		$categoryListDetail = $libies->getCommentCategoryDetails_Lite("",$SchemeType);
		if (is_array($categoryListDetail) && count($categoryListDetail)>0) {
			$h_categoryListing .= '<ul style="list-style: none; margin:0; padding-left:0;">';
			foreach($categoryListDetail as $key => $element) {
				if (empty($element["TITLE".$commentInLang])) {
					continue;
				}
				$h_categoryListing .= '<li answerEncodeNo='.$ParAnswerEncodeNo.' onClick="commentShow($(this).parents(\'div[class=IES_comment_box]:first\').children(\'.IES_select_commentbank\'),$(this).attr(\'answerEncodeNo\'),'.$element["CATEGORYID"].');"><a href="javascript:;">'.$element["TITLE".$commentInLang].'</a></li>';
			}
			$h_categoryListing .= '</ul>';
		}
		############# End: The sliding selection of category
		return $h_categoryListing;
	}

    public static function getIESRurbicClassRoomID()
  {
		global $ies_cfg, $sba_cfg;

//		$classRoomID = getEClassRoomID($ies_cfg["IESClassRoomType"]);
		$classRoomID = getEClassRoomID($sba_cfg["IES_COMBO_ClassRoomType"]);

		return $classRoomID;
  }

	/**
	* RETURN A STRING OF ID with format 1) 1 or 2) 2, 23, 34, 45 DEPEND ON PASSING PARAMETER,
	*           MAINLY HANDLE FOR SQL STATEMENT WITH INTEGER
    *				select * from table WHERE ID = 2
	*			OR
	*				select * from table where ID IN (2,23,34)
	* @owner  : Fai (20101013)
	* @param  : "A INTEGER" OR  "ARRAY OF INTEGER"
	* @return : A INTEGER OR A STRING OF INTEGER
	*/
  public function concatSQLIntValue($ParInteger)
  {
		$idString = '';
		if($ParInteger != NULL || (trim($ParInteger) != "")){
			//Pass integer $ParInteger with value or not empty
			if(is_array($ParInteger)){
				$idString = implode(',',$ParInteger);
			}else{
				$idString = $ParInteger;
			}
		}
		return $idString;
  }


  private static function getTeacherViewSurveyAnswer($parSurveyArr, $parQuestionType, $parStudentID="", $parTaskID=""){
    global $ies_cfg;

    $returnStr = "";
	$returnFinalStr = '';
    if(is_array($parSurveyArr))
    {
		foreach($parSurveyArr AS $_survey_type => $_surveyAnswered){

			foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $survey_type_detail){
				if(is_array($_surveyAnswered) && count($_surveyAnswered) > 0 && ($_survey_type == $survey_type_detail[0])){
					$objIES = new libies();
			        $survey_arr = $objIES->getSurveyDetails($_surveyAnswered);
					$returnStr = '';
					$answerCount  = 0;
					for($i =0,$i_max = sizeof($survey_arr);$i< $i_max;$i++){
						$answerCount  = $i +1;
						$html_survey_title = $survey_arr[$i]['Title'];
						$survey_id = $survey_arr[$i]['SurveyID'];
						switch($parQuestionType){
						  case "SURVEY:EDIT":
							$_key = base64_encode("SurveyID={$survey_id}&StudentID={$parStudentID}&TaskID={$parTaskID}");
							$_link = "/home/eLearning/ies/survey/viewQue.php?key={$_key}";
							break;
						  case "SURVEY:COLLECT":
							if ($survey_type_detail[0]==1) {
								$FromPage = "Collect:Survey";
							} else if ($survey_type_detail[0]==2) {
								$FromPage = "Collect:Interview";
							} else if ($survey_type_detail[0]==3) {
								$FromPage = "Collect:Observe";
							}
							$_key = base64_encode("SurveyID={$survey_id}&StudentID={$parStudentID}&TaskID={$parTaskID}&FromPage=$FromPage");
							$_parStr = "key={$_key}";
							$_link = "/home/eLearning/ies/survey/management.php?{$_parStr}";

							$objIES_survey = new libies_survey();
							$survey_answer = $objIES_survey->getSurveyAnswer($survey_id);
							$survey_answer_num = count($survey_answer);

							$html_survey_title .= " ({$survey_answer_num})";
							break;
						  case "SURVEY:ANALYSIS":
							$_key = base64_encode("SurveyID=".$survey_id."&StudentID=".$parStudentID."&TaskID=".$parTaskID);
							$_parStr = "key={$_key}";
							$_link = "/home/eLearning/ies/survey/questionnaireDisplay.php?{$_parStr}";
							break;
						}
//			            $returnStr .= "{$survey_type_detail[1]} : <a href=\"javascript:newWindow('{$_link}', 10)\">{$html_survey_title}</a><br />";
			            $returnStr .= $answerCount.") <a href=\"javascript:newWindow('{$_link}', 10)\">{$html_survey_title}</a><br />";
					}
					$returnFinalStr .= '<table border = "0" cellpadding="0" cellspacing="0" class=""><tr ><td width="40%">'.$survey_type_detail[1].' : </td><td>'.$returnStr.'</td></tr></table>';
				}
			}

		}
	}
	return $returnFinalStr;
  }

  public static function calculateSchemeMark($parSchemeID, $parStudentIDs=""){
    global $intranet_db, $ies_cfg, $sba_cfg;

    $ldb = new libdb();

	# check whether Maring Criteria setting is completed
    include_once("libSba.php");
    $sba_libSba = new libSba();
    $IsMarkingCrieteriaComplete = $sba_libSba->IsMarkingCriteriaComplete($parSchemeID);
    if($IsMarkingCrieteriaComplete!=$sba_cfg['MarkingCriteriaCompleteStatus']['Complete']) {
    	//return;
    }


    $studentID_str = empty($parStudentIDs) ? "" : (is_array($parStudentIDs) ? implode(", ", $parStudentIDs) : $parStudentIDs);




	if(empty($studentID_str)){
		$conds = "";
		$conds2 = "";
	}else{
		$conds = " AND stagestudent.UserID IN ({$studentID_str}) ";
		$conds2 = " AND ies_ss.UserID in({$studentID_str}) ";
	}


		$sql = "UPDATE {$intranet_db}.IES_SCHEME_STUDENT ies_ss ";
		$sql .= "LEFT JOIN (";
		$sql .= "SELECT stagestudent.UserID, (SUM(IFNULL(stagestudent.Score, 0) / stage.MaxScore * stage.Weight) / SUM(stage.Weight)) * scheme.MaxScore AS Score ";
		$sql .= "FROM {$intranet_db}.IES_STAGE stage ";
		$sql .= "INNER JOIN {$intranet_db}.IES_SCHEME scheme ON (stage.SchemeID = scheme.SchemeID AND scheme.RecordStatus=".$ies_cfg["recordStatus_approved"].") ";
		$sql .= "LEFT JOIN {$intranet_db}.IES_STAGE_STUDENT stagestudent ON stage.StageID = stagestudent.StageID ";
		$sql .= "WHERE stage.SchemeID = '{$parSchemeID}' AND stage.RecordStatus=".$ies_cfg["recordStatus_approved"];
		$sql .= $conds;
		$sql .= " GROUP BY stagestudent.UserID, stage.SchemeID";
		$sql .= ") AS t_sm ";
		$sql .= "ON ies_ss.UserID = t_sm.UserID ";
		$sql .= "SET ies_ss.Score = ROUND(IFNULL(t_sm.Score, 0),2) ";
		$sql .= "WHERE ies_ss.SchemeID = '{$parSchemeID}'";
		$sql .= $conds2;

    $ldb->db_db_query($sql);
	}

  public static function calculateStageMark($parStageID, $parStudentIDs=""){
    global $intranet_db, $ies_cfg, $sba_cfg;

	# check whether Maring Criteria setting is completed
	include_once("libSba.php");
    $sba_libSba = new libSba();
    $sql = "SELECT SchemeID FROM IES_STAGE WHERE StageID='$parStageID'";
    $thisSchemeID = $sba_libSba->returnVector($sql);
    $IsMarkingCrieteriaComplete = $sba_libSba->IsMarkingCriteriaComplete($thisSchemeID[0]);
    if($IsMarkingCrieteriaComplete!=$sba_cfg['MarkingCriteriaCompleteStatus']['Complete']) {
    	return;
    }

    $ldb = new libdb();

    $studentID_str = empty($parStudentIDs) ? "" : (is_array($parStudentIDs) ? implode(", ", $parStudentIDs) : $parStudentIDs);

	if(trim($studentID_str)== ""){
		$conds ="";
		$conds2 = "";
		$conds3 = "";
	}else{
		$conds = " AND ishb.UserID IN ({$studentID_str}) ";
		$conds2 = " AND t_sm.UserID IN ({$studentID_str}) ";
		$conds3 = " AND t_sm.UserID IN ({$studentID_str}) ";
	}

	/*
	$sql = "SELECT ishb.USERID , ishb.StageID, ishb.BatchID, {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]} AS AssignmentType, SUM(im.Score / ismc.MaxScore * ismc.Weight) as SumScore, SUM(ismc.Weight) as SumWeight, stage.MaxScore as StageMaxScore, ROUND(SUM(im.Score / ismc.MaxScore * ismc.Weight) / SUM(ismc.Weight) * stage.MaxScore, 2) AS UpdatedScore ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ishb ";
		$sql .= "INNER JOIN {$intranet_db}.IES_MARKING im ON ishb.BatchID = im.AssignmentID AND im.AssignmentType = {$ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]} ";
		$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ON (im.StageMarkCriteriaID = ismc.StageMarkCriteriaID AND ismc.RecordStatus=".$ies_cfg["recordStatus_publish"].") ";
		$sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ON (ishb.StageID = stage.StageID AND stage.RecordStatus=".$ies_cfg["recordStatus_publish"].") ";
		$sql .= "WHERE ishb.BatchStatus = {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]} AND stage.StageID = {$parStageID} ";
		$sql .= "and im.MarkCriteria <> ".MARKING_CRITERIA_OVERALL;
		$sql .= $conds;
		$sql .= " GROUP BY ishb.USERID ,ishb.StageID,ishb.BatchID ";
		$a = $ldb->returnArray($sql);
		echo $sql;
		debug_pr($a);
		//echo $sql;
	*/


        //exit;
		$sql = "UPDATE {$intranet_db}.IES_MARKING im ";
		$sql .= "LEFT JOIN (";
		$sql .= "SELECT ishb.USERID , ishb.StageID, ishb.BatchID, {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]} AS AssignmentType, ROUND(((SUM((im.Score / ismc.MaxScore) * ismc.Weight)) / SUM(ismc.Weight)) * stage.MaxScore, 2) AS UpdatedScore ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ishb ";
		$sql .= "INNER JOIN {$intranet_db}.IES_MARKING im ON ishb.BatchID = im.AssignmentID AND im.AssignmentType = {$ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]} ";
		$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ON (im.StageMarkCriteriaID = ismc.StageMarkCriteriaID AND ismc.RecordStatus=".$ies_cfg["recordStatus_publish"].") ";
		$sql .= "INNER JOIN {$intranet_db}.IES_STAGE stage ON (ishb.StageID = stage.StageID AND stage.RecordStatus=".$ies_cfg["recordStatus_publish"].") ";
		$sql .= "WHERE ishb.BatchStatus = {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]} AND stage.StageID = '{$parStageID}' ";
		$sql .= "and im.MarkCriteria <> ".MARKING_CRITERIA_OVERALL." /*im.MarkCriteria  <> MARKING_CRITERIA_OVERALL because should not count mark with type is MARKING_CRITERIA_OVERALL again*/ ";
		$sql .= $conds;
		$sql .= " GROUP BY ishb.USERID ,ishb.StageID,ishb.BatchID ";
		$sql .= ") AS t_sm ";
		$sql .= "ON im.AssignmentID = t_sm.BatchID AND im.AssignmentType = t_sm.AssignmentType ";
		$sql .= "SET im.Score = t_sm.UpdatedScore ";
		$sql .= "WHERE t_sm.StageID = '{$parStageID}' AND im.MarkCriteria = ".MARKING_CRITERIA_OVERALL;
		$sql .= $conds2;

    $a = $ldb->db_db_query($sql);
    //echo $sql.'<br><br>';

    $sql = "UPDATE {$intranet_db}.IES_STAGE_STUDENT iss ";
    $sql .= "LEFT JOIN (";
		$sql .= "SELECT ishb.StageID, ishb.UserID, im.Score ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ishb ";
		$sql .= "INNER JOIN {$intranet_db}.IES_MARKING im ON ishb.BatchID = im.AssignmentID AND im.AssignmentType = {$ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]} ";
		$sql .= "WHERE ishb.BatchStatus = {$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]} AND ishb.StageID = '{$parStageID}' AND im.MarkCriteria = ".MARKING_CRITERIA_OVERALL." ";
		$sql .= $conds;
		$sql .= "GROUP BY ishb.UserID, ishb.StageID HAVING MAX(ishb.DateModified)";
		$sql .= ") AS t_sm ";
		$sql .= "ON iss.StageID = t_sm.StageID AND iss.UserID = t_sm.UserID ";
		$sql .= "SET iss.Score = t_sm.Score ";
		$sql .= "WHERE iss.StageID = {$parStageID}";
		$sql .= $conds3;
		$b = $ldb->db_db_query($sql);
		//echo $a.'/'.$b;
	//echo $sql."<br>";

	}
}

?>