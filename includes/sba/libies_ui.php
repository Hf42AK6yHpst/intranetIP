<?php
/** [Modification Log] Modifying By: Paul
 * *******************************************
 * 2011-01-25 Thomas
 *  - modified function getQuestionCollectTableDisplay(), go to page "questionnaire_discovery_list.php" instead of "management.php" when clicking on manage button in stage 3
 * 2011-01-13 Thomas
 *  - add function getSurveyMappingInfoDisplay()
 * 2011-01-13 Ivan
 * 	- modified GET_MANAGEMENT_BROAD_TAB_MENU(), added "Discovery List" tab for $FromTask == "Collect" and in Stage 3
 * 	- modified getQuestionCollectTableDisplay(), added "IsStage3" para when calling the new window pop up
 * *******************************************
 */

class libies_ui {

  public static function GET_STEP_CHAIN($parStepArr, $parCurrentStepID="", $parShowStep=true)
  {
    global $Lang;
  
    $step_style = $parShowStep ? "style=\"display:block\"" : "style=\"display:none\"";

    $returnStr = "<div class=\"task_steps\" {$step_style}>";
    $returnStr .= "<ul class=\"line\"><span>{$Lang['IES']['Step']}</span>";
    for($i=0; $i<count($parStepArr); $i++)
    {
      $t_step_id = $parStepArr[$i]["StepID"];
      $t_step_no = $parStepArr[$i]["StepNo"];
      $t_step_title = $parStepArr[$i]["Title"];
      $t_step_seq = $parStepArr[$i]["Sequence"];
      
    
      $returnStr .= "<li class=\"circle\">";
      $returnStr .= ($t_step_id == $parCurrentStepID) ? "<a class=\"current\" href=\"javascript:;\">" : "<a href=\"javascript:doStep({$t_step_id})\">";
      $returnStr .= $t_step_seq."</a></li>";
      $returnStr .= (next($parStepArr) === false) ? "" : "<li class=\"spacer\"></li>";
    }
    $returnStr .= "</ul>";
    $returnStr .= "</div>";
    
    return $returnStr;
  }
  
  public static function GET_SCHEME_SELECTION($parSchemeArr, $parCurrentSchemeID, $IsIESTeacher=false)
  {  	
    $returnStr = "<div class=\"scheme_current\">";
    $returnStr .= "<ul class=\"scheme_option\" style=\"display: none;\">";

    for($i=0; $i<count($parSchemeArr); $i++)
    {
      $t_scheme_id = $parSchemeArr[$i]["SchemeID"];
      $t_scheme_title = $parSchemeArr[$i]["Title"];
      
      if($parCurrentSchemeID == $t_scheme_id)
      {
        $currentSchemeTitle = $t_scheme_title;
      }
      else
      {
      	if ($IsIESTeacher) {
      		// do nothing
      	} else {
        	$returnStr .= "<li><a href=\"javascript:doScheme(".$t_scheme_id.")\">".$t_scheme_title."</a></li>";
      	}
      }
    }
    $returnStr .= "</ul>";
    
    $returnStr .= $currentSchemeTitle;
    $returnStr .= "</div>";
  
    return $returnStr;
  }

	public static function GET_STAGE_SELECTION($stageArr,$currentStageID,$currentSchemeID)
	{
    global $Lang;
	
		$returnStr = "<ul class=\"stage\">\n";
		for($i = 0, $count = sizeof($stageArr); $i < $count; $i++)
		{
			$_stageId = $stageArr[$i]["StageID"];
			$_stageTitle = $stageArr[$i]["Title"];
			$_stageSequence = $stageArr[$i]["Sequence"];
			$_stage = "{$Lang['IES']['Stage']}{$_stageSequence}";

			$_stageCssClass = "s{$_stageSequence}";

			$cssClass = ($_stageId == $currentStageID) ? "class = \"current\"" :"" ;

			$_link = "javascript: goToStage({$currentSchemeID},{$_stageId})";

			$returnStr .= "<li {$cssClass}><a href = \"{$_link}\" class=\"{$_stageCssClass}\">{$_stageTitle}</a></li>\n";

			if($i != $count-1)
			{
				//display this line for not last element
				$returnStr .= "<li class=\"line\"></li>\n";
			}
		}
		$returnStr  .= "</ul>";

		return $returnStr;

	}

	/**
	* Generate HTML FUNCTION TAB 
	*
	* @param : ARRAY $tabArray ARRAY TO STORE ALL THE TAB NEED TO DISPLAY
  * @param : STRING $currentTagFunction CURRENT USING FUNCTION, for display active tag	
	* @return : String HTML FUNCITON TAB
	* 
	*/
	public static function GET_TAB_MENU($tabArray, $currentTagFunction="")
	{

		$x = "";
		$x .= "<div class=\"shadetabs\">";
		$x .= "<ul>";

		if(is_array($tabArray) > 0)
		{
			foreach ($tabArray as $thisTabFunction => $thisDetails)
			{

				$ClassStyle = ($thisTabFunction == $currentTagFunction) ? "selected" : "";								

				$Link = $thisDetails[0];   // DATA STRUCTURE the first element is the Link					
				$Title = $thisDetails[1];  // DATA STRUCTURE the second element is the Module title
			
				$x .= "<li class=\"".$ClassStyle."\"><a href=\"".$Link."\"><strong>".$Title."</strong></a></li>";
			}
		}
		$x .= "</ul>";
		$x .= "</div>";

		return $x;
	}
	
	public static function GET_TASK_BLOCK_CONTENT($parTaskBlockInfoArr)
	{
    global $Lang;
   
    // Preload data
    $task_id = $parTaskBlockInfoArr["task_id"];
    $title = $parTaskBlockInfoArr["title"];
    $step_arr = $parTaskBlockInfoArr["step_arr"];
    $is_task_enable = $parTaskBlockInfoArr["is_task_enable"];
    $is_task_open = $parTaskBlockInfoArr["is_task_open"];
    $is_task_start = $parTaskBlockInfoArr["is_task_start"];
	
    // HTML content preparation
    if($is_task_enable)
    {
      $html_title_style = $is_task_open ? "task_open" : "task_close";
      $html_desc_style = ($is_task_open && $is_task_start) ? "style=\"display:block\"" : "style=\"display:none\"";
      $html_step_chain = ($is_task_open && $is_task_start) ? self::GET_STEP_CHAIN($step_arr, "", $is_task_start) : "";
     
      $returnStr = "<div class=\"task_title {$html_title_style}\"><span>{$title}</span></div>";
      $returnStr .= ($is_task_open ? "" : "<span class=\"content_grey_text\">(".$Lang['IES']['NotReleased'].")</span>");
      $returnStr .= "<a href=\"javascript:doLoadDescription({$task_id});\" class=\"show_intro\" {$html_desc_style}><span>".$Lang['IES']['Introduction']."</span></a>";
      
      $returnStr .= $html_step_chain;
      
      $returnStr .= "<div class=\"clear\"></div>";
      $returnStr .= "<div id=\"task_content_{$task_id}\">";
      
      // If task or any steps in task have not been done
      if($is_task_open)
      {
        if($is_task_start)
        {
          $returnStr .= self::GET_TASK_CONTENT($parTaskBlockInfoArr);
        }
        else
        {
          $returnStr .= self::GET_TASK_DESCRIPTION($parTaskBlockInfoArr, false);
        }
      }
  
      $returnStr .= "</div>";
    }

    return $returnStr;
  }
  
  private static function GET_TASK_CONTENT($parTaskBlockInfoArr)
  {
    global $Lang, $ies_cfg,$PATH_WRT_ROOT;
    $task_id = $parTaskBlockInfoArr["task_id"];
    $stage_id = $parTaskBlockInfoArr["stage_id"];
	$student_uID = $parTaskBlockInfoArr["studentID"];
    $description = $parTaskBlockInfoArr["description"];
    $instant_edit = $parTaskBlockInfoArr["instant_edit"];
    $task_question_type = $parTaskBlockInfoArr["task_question_type"];
    $task_answer = $parTaskBlockInfoArr["task_answer"];
    $step_id = $parTaskBlockInfoArr["first_step_id"];
    $task_input_date = $parTaskBlockInfoArr["task_input_date"];
    $task_modified_date = $parTaskBlockInfoArr["task_modified_date"];


    // Display task content block
    if($task_answer !== false)
    {
    	switch(strtoupper($task_question_type)) {
    		default:
			    // Display task content
  	      $html_task_content = nl2br($task_answer);
  	      //	$html_task_content = '<div class="icon_stu_edit">'.nl2br($task_answer).'</div>';
  	      	$html_edit_button = ($instant_edit) ? "<a name=\"edit_task_btn\" class=\"task_edit\" task_id=\"{$task_id}\"><span>{$Lang['IES']['Edit']}</span></a>" : "";
          break;
    		case "TABLE":
  		    $libies = new libies();
  		    $parentIds = current($libies->getParentsIds("TASK",$task_id));
  		    $stepNoSaveToTask = $libies->getMaxStepNoSaveToTask($task_id);
  		    # $_question[1] is get from here
  		    $_question[1] = array();
  		    include_once($PATH_WRT_ROOT."/home/eLearning/ies/coursework/templates/default/{$parentIds["STAGE_SEQ"]}/{$stepNoSaveToTask}/model.php");
  
  		    $question_tr_fields = "<tr><th>#</th><th>".implode("</th><th>",$_question[1])."</th><th>&nbsp;</th></tr>";

      		$task_answer = addslashes(html_entity_decode($task_answer));
      		$sizeOfQuestionHeader = count($_question[1]);
      		$colSpan = $sizeOfQuestionHeader+2;
      		$questionInput = "";
      		for($i=1;$i<$sizeOfQuestionHeader;$i++) {
      			$questionInput .= "<td><input name=\"content1\" class=\"input_long\" type=\"text\" /></td>";
      		}  
          $html_task_content = <<<HTMLEND
<form name="formx" id="formx">
	<div class="IES_task_form"> <!-- IES_task_form start -->
    <table class="common_table_list IES_form_table" id="addWorkTable">
	    <col class="" />
	    <col class="field_title" />
	    <thead>
        {$question_tr_fields}
      </thead>
	    <tbody>
        <tr id="blankRow" style="display: none">
          <td class="num_check">#</td>
          <td><span class="row_content"><input name="content1" type="text" id="textfield2" class=""/></span></td>
  		    {$questionInput}
  		    <td class="tool_col_min" nowrap><input name="submit2" type="button" class="formbutton save" onmouseover="this.className='formbuttonon save'" onmouseout="this.className='formbutton save'" value=" {$Lang['IES']['Save']} "/>&nbsp;&nbsp;<input name="submit2" type="button" class="formsubbutton removeRow cancel" onmouseover="this.className='formsubbuttonon removeRow cancel'" onmouseout="this.className='formsubbutton removeRow cancel'" value=" {$Lang['IES']['Cancel']} "/></td>
        </tr>
        <tr id="addRowTr"><td colspan="{$colSpan}" style="text-align:right" ><a href="javascript: void(0);" class="add_row" id="addRow">{$Lang['IES']['AddRow']}</a></td></tr>
	    </tbody>
	  </table>
    <input type="hidden" value="" name="task_content" />
    <input name="task_id" value="{$task_id}" type="hidden">
    <input name="task_type" value="{$task_question_type}" type="hidden">
	</div>
</form>
<script>
  initializeAnswerArray("{$task_answer}");
	initializeResultTable();
</script>
HTMLEND;
          break;
        case "SURVEY:EDIT":
		      $html_thead = "";
			  $html_tdata = "";
				
			$objIES = new libies();
			$SurveyDetailArray = $objIES->getTastAllSurveyDetail($stage_id, $student_uID, $task_id);
			$SurveyDetails = $objIES->handleSurveyURLDetails($SurveyDetailArray);

			 $html_task_content = self::getQuestionnaireTableDisplayWithGuide($SurveyDetails,$ies_cfg['Questionnaire']['SurveyAction']['EDIT'],$ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['FRONT']);

          break;
          case "SURVEY:COLLECT":

//          	$html_task_content =  self::getQuestionCollectTableDisplay($parTaskBlockInfoArr["surveydetailarray"]);
			$objIES = new libies();
			$SurveyDetailArray = $objIES->getTastAllSurveyDetail($stage_id, $student_uID, $task_id);
			$SurveyDetails = $objIES->handleSurveyURLDetails($SurveyDetailArray);

			 $html_task_content = self::getQuestionnaireTableDisplayWithGuide($SurveyDetails,$ies_cfg['Questionnaire']['SurveyAction']['COLLECT'],$ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['FRONT']);

          break;
          case "SURVEY:ANALYSIS":
			$objIES = new libies();
			$SurveyDetailArray = $objIES->getTastAllSurveyDetail($stage_id, $student_uID, $task_id);
			$SurveyDetails = $objIES->handleSurveyURLDetails($SurveyDetailArray);

			 $html_task_content = self::getQuestionnaireTableDisplayWithGuide($SurveyDetails,$ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS'],$ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['FRONT']);

//          	$html_task_content =  self::getQuestionnaireAnalysisTableDisplay($parTaskBlockInfoArr["surveydetailarray"]);
          break;

    	}
    }
        
    // Check if the teacher comment exist or not!!!
//    DEBUG_R($parTaskBlockInfoArr["is_teacher_comment"]);

    if(!empty($parTaskBlockInfoArr["is_teacher_comment"]) || !empty($parTaskBlockInfoArr['is_teacher_score_on_task']))
    {
		$teach_comment_date  = '';
		if(empty($parTaskBlockInfoArr["is_teacher_comment"]["DateInput"])) {
			//do nothing
		}else{
			$teach_comment_date   = '('.$parTaskBlockInfoArr["is_teacher_comment"]["DateInput"].')';
		}
      $html_teacher_comment = "<div class=\"teacher_comment\"><a href=\"javascript:doGetComment({$task_id})\">{$Lang['IES']['TeachersFeedback']}</a><span style=\"font-size:9px; color:grey\">{$teach_comment_date}</span></div><!-- if have new comment, <a> add class \"new\"-->";
    }
    else
    {
      $html_teacher_comment = "<div class=\"teacher_comment\" style=\"display:none;\"><a href=\"javascript:doGetComment({$task_id})\">{$Lang['IES']['TeachersFeedback']}</a></div><!-- if have new comment, <a> add class \"new\"-->";
    }
         
    $returnStr .= <<<HTML
<!-- ############ task_ans_box start ##############  -->
<div class="IES_ans_box" style="display:block"> <!-- IES_ans_box, control show hide off ans box -->
  <div class="writing_area_min task_wrap">
  <!--<div class="writing_area_min task_wrap icon_stu_edit">-->
  	<div class="q_top"><div><div></div></div></div>
  	<div class="q_left">
  		<div class="q_right">
		  	<div class="icon_stu_edit">
			    {$html_task_content}
			    <div style="width:100%">
			      {$html_edit_button}&nbsp;
			      <div class="task_index_edit" style="display:none"> <!-- ###27-05-2010 -->
			        <a href="javascript:doStep({$step_id})" class="gotosteps"><span>{$Lang['IES']['PleaseGuideMeToWrite']}</span></a> <!-- ###27-05-2010 -->
			        <div class="h-spacer">&nbsp;</div> <!-- ###27-05-2010 -->
			        <a href="javascript:doEditTask({$task_id})" class="instant_write" style="float:right"><span>{$Lang['IES']['InstantEdit']}</span></a> <!-- ###27-05-2010 -->
			      </div> <!-- ### 27-05-2010 -->
			    </div>
		    	<div class="clear"></div>
		    </div>
		</div>
	</div>
	<div style="clear:both"></div>
	<div class="q_bottom"><div><div></div></div></div>
  </div>        
{$html_teacher_comment}  
  <div class="edit_date">{$Lang['IES']['SubmitDate']} : {$task_input_date} | {$Lang['IES']['LastModifiedDate']} : {$task_modified_date}</div>
  <div class="clear"></div>
  <div style="display:none;" id="Comment_block_{$task_id}" class="IES_commentbox"></div> 
</div> <!-- IES_ans_box, control show hide off ans box ### END ###-->    
HTML;

    return $returnStr;
  }
  
  public static function GET_TASK_DESCRIPTION($parTaskBlockInfoArr, $showCloseBtn)
  {
  	global $Lang;
    $task_id = $parTaskBlockInfoArr["task_id"];
    $description = $parTaskBlockInfoArr["description"];
    $step_id = $parTaskBlockInfoArr["first_step_id"];
    
    // Display description block
    if($showCloseBtn) {
      $returnStr .= <<<HTML
<div class="task_intro_box" style=""> <!--task_intro_box start-->
  <div class="task_intro_top"><div></div></div>
  <div class="task_intro_left"><div class="task_intro_right">
    <div class="task_intro_content">
      <a href="javascript:doLoadTaskBlock({$task_id});" class="intro_close" title="{$Lang['IES']['Close']}">&nbsp;</a> <!--close btn show when sth was written -->
      <span>{$description}</span>
      
    </div>
  </div></div>
  <div class="clear"></div>
  <div class="task_intro_bottom"><div></div></div>
</div>
HTML;
    }
    else {
      $instant_edit = $parTaskBlockInfoArr["instant_edit"];
      
      if($instant_edit)
      {
        $op_btn = "<a href=\"javascript:doEditTask({$task_id})\" class=\"instant_write\"><span>{$Lang['IES']['InstantEdit']}</span></a>";
        $op_btn .= "<a href=\"javascript:doStep({$step_id})\" class=\"gotosteps\"><span>{$Lang['IES']['PleaseGuideMeToWrite']}</span></a>";
      }
      else
      {
        $op_btn = "<a href=\"javascript:doStep({$step_id})\" class=\"gotosteps\" style=\"float:none; margin:0 auto; width:200px\"><span>{$Lang['IES']['PleaseGuideMeToWrite']}</span></a>";
      }
    
      $returnStr .= <<<HTML
<div class="task_intro_box" style=""> <!--task_intro_box start-->
  <div class="task_intro_top"><div></div></div>
  <div class="task_intro_left"><div class="task_intro_right">
    <div class="task_intro_content">
      <a href="javascript:;" class="intro_close" style="display:none">&nbsp;</a> <!--close btn show when sth was written -->
      <span>{$description}</span>
      
      <div class="task_intro_buttons">
        {$op_btn}
      </div>
      <div class="clear"></div>
    </div>
  </div></div>
  <div class="clear"></div>
  <div class="task_intro_bottom"><div></div></div>
</div>
HTML;
    }
 
    return $returnStr;
  }
  
	/**
	* Generate HTML FUNCTION TAB 
	*
	* @param : ARRAY $tabArray ARRAY TO STORE ALL THE TAB NEED TO DISPLAY
  * @param : STRING $currentTagFunction CURRENT USING FUNCTION, for display active tag	
	* @return : String HTML FUNCITON TAB
	* 
	*/
	public static function GET_SURVEY_TAB_MENU($tabArray, $currentTagFunction="")
	{

		$x = "";
		$x .= "<div class=\"watermark\"></div>";
		$x .= "<ul class=\"q_tab\">";

		if(is_array($tabArray) > 0)
		{
			foreach ($tabArray as $thisTabFunction => $thisDetails)
			{

				$ClassStyle = ($thisTabFunction == $currentTagFunction) ? "current" : "";								

				$Link = $thisDetails[0];   // DATA STRUCTURE the first element is the Link					
				$Title = $thisDetails[1];  // DATA STRUCTURE the second element is the Module title
			
				$x .= "<li class=\"".$ClassStyle."\"><a href=\"".$Link."\">".$Title."</a></li>";
			}
		}
		$x .= "</ul>";

		return $x;
	} 

		public static function getQuestionCollectInstruction($questionType,$stage = 0){
			global $ies_cfg, $Lang;

			//DON'T SHOW THE MAIL INSTRUCTION WHEN STAGE 3 , HARDCODE 3
			if($stage != 3){
				$strMailMsg = $Lang['IES']['SurveyTable']['string1'].'<br/>';
			}
			$html = '';
			switch($questionType){
				case $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]:
				$html = <<<HTML
					{$strMailMsg}
					{$Lang['IES']['SurveyTable']['string2']}
HTML;

				break;

				case $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]:
				$html = <<<HTML
					{$Lang['IES']['SurveyTable']['string5']}<br />
					<br/>
					<b>{$Lang['IES']['SurveyTable']['string6']}</b>
					<ol>
						<li>{$Lang['IES']['SurveyTable']['string7']}</li>
						<li>{$Lang['IES']['SurveyTable']['string8']}</li>
						<li>{$Lang['IES']['SurveyTable']['string9']}</li>
						<li>{$Lang['IES']['SurveyTable']['string10']}</li>
						<li>{$Lang['IES']['SurveyTable']['string11']}</li>
					</ol>
HTML;
				break;				

				case $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]:
				$html = <<<HTML
				{$Lang['IES']['SurveyTable']['string12']}
HTML;
				break;
			}
			return $html;
		}
	

	public static function getQuestionCollectTableDisplay($ParSurveyDetailArray,$ParIsStage3=false,$ParStage3TaskID=0){
		global $Lang;
		$html_survey = " -- ";
		$html_interview = " -- ";	
		$html_observation = " -- ";
		
		$taskID = $ParSurveyDetailArray["TaskID"];
		$SurveyDetail = $ParSurveyDetailArray["Survey"];
		$InterviewDetail = $ParSurveyDetailArray["Interview"];
		$ObservationDetail = $ParSurveyDetailArray["Observation"];
		$StudentID = $ParSurveyDetailArray["UserID"];
		
		$URLStage3Num = ($ParIsStage3)? 1 : 0;

		if(!empty($SurveyDetail)){
			$SurveyID = $SurveyDetail["SurveyID"];
			$Survey_title = $SurveyDetail['Title']; 
			$code_key = "SurveyID={$SurveyID}&task_id=$taskID";
			$encode_SurveyID = base64_encode($SurveyID);
			$key = base64_encode($code_key);
			
		
			if ($ParIsStage3) {
				$URLStage3Num=1;
				$composeEmailLink = "";
				$surveyDetailLink = "questionnaireGroupingList.php";
			} else {
				$URLStage3Num=0;
				$composeEmailLink = "<p><a href=\"/home/eLearning/ies/survey/mail/compose_email.php?KeepThis=true&key=$key&amp;TB_iframe=true&amp;height=450&amp;width=730\"  class=\"send_email thickbox\">{$Lang['IES']['SurveyTable']['string3']}</a></p>&nbsp;";
				$surveyDetailLink = "management.php";
			}
			
			$html_survey = "<p><a href=\"javascript:void(0)\" onClick=\"newWindow('/home/eLearning/ies/survey/SurveyPreview.php?SurveyID={$encode_SurveyID}',95)\">{$Survey_title}</a></p>";

			if(!$ParIsStage3){  //not stage 3 ,ie is stage 2
				$html_survey .= "{$Lang['IES']['SurveyTable']['string1']}<br />";
			}
			$html_survey .= "{$Lang['IES']['SurveyTable']['string2']}";
			//$html_survey .= "<p><a href=\"/home/eLearning/ies/survey/mail/edit.php?KeepThis=true&key=$key&amp;TB_iframe=true&amp;height=450&amp;width=730\"  class=\"send_email thickbox\">發送電郵</a></p>&nbsp;";
			$html_survey .= $composeEmailLink;
			$manage_content_1 = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('/home/eLearning/ies/survey/{$surveyDetailLink}?key=".base64_encode("SurveyID=$SurveyID&StudentID=$StudentID&TaskID=$taskID&IsStage3=$URLStage3Num&Stage3TaskID=$ParStage3TaskID&FromPage=Collect:Survey")."',95)\" value=\" {$Lang['IES']['SurveyTable']['string4']} \" />";
			//$html_survey .= "<p><a href=\"javascript:void(0);\"  class=\"send_email\">管理</a></p>";
			
		}
		if(!empty($InterviewDetail)){
			$popup_page = $ParIsStage3? "questionnaire_discovery_list.php" : "management.php";
			$Interview_title = $InterviewDetail['Title']; 
			$InterviewID = $InterviewDetail["SurveyID"];
			$encode_InterviewID = base64_encode($InterviewID);
			$html_interview = "<p><a href=\"javascript:void(0)\" onClick=\"newWindow('/home/eLearning/ies/survey/SurveyPreview.php?SurveyID={$encode_InterviewID}',95)\">{$Interview_title}</a></p>";
			$html_interview .= "{$Lang['IES']['SurveyTable']['string5']}<br /><br /><b>{$Lang['IES']['SurveyTable']['string6']}</b><ol><li>{$Lang['IES']['SurveyTable']['string7']}</li><li>{$Lang['IES']['SurveyTable']['string8']}</li><li>{$Lang['IES']['SurveyTable']['string9']}</li><li>{$Lang['IES']['SurveyTable']['string10']}</li><li>{$Lang['IES']['SurveyTable']['string11']}</li>";
			//$html_interview .= "<p><a href=\"#\" class=\"task_demo\">範例</a></p>";
			$manage_content_2 = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('/home/eLearning/ies/survey/$popup_page?key=".base64_encode("SurveyID=$InterviewID&FromPage=Collect:Interview&IsStage3=$URLStage3Num")."',95)\" value=\" {$Lang['IES']['SurveyTable']['string4']} \" />";
		}
		if(!empty($ObservationDetail)){
			$popup_page = $ParIsStage3? "questionnaire_discovery_list.php" : "management.php";
			$Observation_title = $ObservationDetail['Title']; 
			$ObservationID = $ObservationDetail["SurveyID"];
			$encode_Observation = base64_encode($ObservationID);
			
			$html_observation = "<p><a href=\"javascript:void(0)\" onClick=\"newWindow('/home/eLearning/ies/survey/SurveyPreview.php?SurveyID={$encode_Observation}',95)\">{$Observation_title}</a></p>";
			$html_observation .= $Lang['IES']['SurveyTable']['string12'];
			$manage_content_3 = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('/home/eLearning/ies/survey/$popup_page?key=".base64_encode("SurveyID=$ObservationID&FromPage=Collect:Observe&IsStage3=$URLStage3Num")."',95)\" value=\" {$Lang['IES']['SurveyTable']['string4']} \" />";

		}
		
		$html = <<<HTML
		    <table class="common_table_list IES_q_table">
      <tr>
        <th>{$Lang['IES']['SurveyTable']['string13']}</th>
        <th>{$Lang['IES']['SurveyTable']['string14']}</th>
        <th>{$Lang['IES']['SurveyTable']['string15']}</th>
      </tr>
      <tr>
        <td width=33%>{$html_survey}</td>
        <td width=33%>{$html_interview}</td>
        <td>{$html_observation}</td>
      </tr>
      <tr class="btn_row">
        <td>{$manage_content_1}</td>
        <td>{$manage_content_2}</td>
        <td>{$manage_content_3}</td>
      </tr>
    </table>
HTML;
		
	
		return $html;
	}
	
	public static function getQuestionnaireAnalysisTableDisplay($ParTaskAllSurveyDetail) {
		global $Lang;
		$key1 = base64_encode("SurveyID=".$ParTaskAllSurveyDetail["Survey"]["SurveyID"]."&StudentID=".$ParTaskAllSurveyDetail["UserID"]."&TaskID=".$ParTaskAllSurveyDetail["TaskID"]."&FromPage=Analyze:Survey");
		
		$survey_link_1 = "../survey/questionnaireGroupingList.php?key=$key1";

		$libies = new libies();
		$surveyCountAndDate = $libies->getValidSurveyCountAndLastModifiedDate($ParTaskAllSurveyDetail["Survey"]["SurveyID"]);
		$survey_count = $surveyCountAndDate["COUNT"];
		$survey_date = empty($surveyCountAndDate["DATE"])?"--":$surveyCountAndDate["DATE"];
//		debug_r($surveyCountAndDate);

		$interviewCountAndDate = $libies->getValidSurveyCountAndLastModifiedDate($ParTaskAllSurveyDetail["Interview"]["SurveyID"]);
		$interview_count = $interviewCountAndDate["COUNT"];
		$interview_date = empty($interviewCountAndDate["DATE"])?"--":$interviewCountAndDate["DATE"];
//		debug_r($interviewCountAndDate);
		
		$observationCountAndDate = $libies->getValidSurveyCountAndLastModifiedDate($ParTaskAllSurveyDetail["Observation"]["SurveyID"]);
		$observation_count = $observationCountAndDate["COUNT"];
		$observation_date = empty($observationCountAndDate["DATE"])?"--":$observationCountAndDate["DATE"];

		
//		DEBUG_R($ParTaskAllSurveyDetail);
		$survey_title_1 = $ParTaskAllSurveyDetail["Survey"]["Title"];
		$survey_title_2 = $ParTaskAllSurveyDetail["Interview"]["Title"];
		$survey_title_3 = $ParTaskAllSurveyDetail["Observation"]["Title"];
		if (!empty($survey_title_1)) {
			$encode_SurveyID = base64_encode($ParTaskAllSurveyDetail["Survey"]["SurveyID"]);
			$box_content_1 = "<p><a href=\"javascript:void(0)\" onClick=\"newWindow('/home/eLearning/ies/survey/SurveyPreview.php?SurveyID={$encode_SurveyID}',95)\">{$survey_title_1}</a></p><p>{$Lang['IES']['Finished']} : {$survey_count}{$Lang['IES']['Copy1']}</p><div class=\"edit_bottom\"> <span> {$Lang['IES']['LastModifiedDate']} : {$survey_date}</span></div>";
			$manage_content_1 = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('{$survey_link_1}', 95)\" value=\" {$Lang['IES']['ManageInformation']} \" />";
		} else {
			$box_content_1 = "--";
			$manage_content_1 = "<br />";
		}
		if (!empty($survey_title_2)) {
			$encode_SurveyID = base64_encode($ParTaskAllSurveyDetail["Interview"]["SurveyID"]);
			$box_content_2 = "<p><a href=\"javascript:void(0)\" onClick=\"newWindow('/home/eLearning/ies/survey/SurveyPreview.php?SurveyID={$encode_SurveyID}',95)\">{$survey_title_2}</a></p><p>{$Lang['IES']['Finished']} : {$interview_count}{$Lang['IES']['Copy1']}</p><div class=\"edit_bottom\"> <span> {$Lang['IES']['LastModifiedDate']} : {$interview_date}</span></div>";
			//$manage_content_2 = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('{$survey_link_2}', 95)\" value=\" 管理資料 \" />";
			$manage_content_2 = "<br />";
		} else {
			$box_content_2 = "--";
			$manage_content_2 = "<br />";
		}
		if (!empty($survey_title_3)) {
			$encode_SurveyID = base64_encode($ParTaskAllSurveyDetail["Observation"]["SurveyID"]);
			$box_content_3 = "<p><a href=\"javascript:void(0)\" onClick=\"newWindow('/home/eLearning/ies/survey/SurveyPreview.php?SurveyID={$encode_SurveyID}',95)\">{$survey_title_3}</a></p><p>{$Lang['IES']['Finished']} : {$observation_count}{$Lang['IES']['Copy1']}</p><div class=\"edit_bottom\"> <span> {$Lang['IES']['LastModifiedDate']} : {$observation_date}</span></div>";
			//$manage_content_3 = "<input name=\"submit2\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('{$survey_link_3}', 95)\" value=\" 管理資料 \" />";
			$manage_content_3 = "<br />";
		} else {
			$box_content_3 = "--";
			$manage_content_3 = "<br />";
		}
		
		$html = <<<HTMLEND
    <table class="common_table_list IES_q_table">
      <tr>
        <th style="width:33%">{$Lang['IES']['Questionnaire']}</th>
        <th>{$Lang['IES']['Interview']}</th>
        <th style="width:33%">{$Lang['IES']['Observe']}</th>
      </tr>
      <tr>
        <td>{$box_content_1}</td>
        <td>{$box_content_2}</td>
        <td>{$box_content_3}</td>
      </tr>
      <tr class="btn_row">
        <td>{$manage_content_1}</td>
        <td>{$manage_content_2}</td>
        <td>{$manage_content_3}</td>
      </tr>
    </table><br />
HTMLEND;
		return $html;
	}
	
	/**
	* Generate Survey Edit Table 
	*
	* @param : ARRAY $parBtnLinkArr Associative Array, survey type against edit link
	* @param : ARRAY $parFormCreatedArr Associative Array, boolean indicating whether survey is created	
	* @return : String $html resulting table
	* 
	*/

	public static function getQuestionnaireEditTableDisplay($parBtnArr, $parFormCreatedArr){
    global $ies_cfg, $Lang;
    
    $html_thead = "";
    $html_tbtncell = "";
    
    foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $qTypeDetail)
    {
      $_qTypeCode = $qTypeDetail[0];
      $_qTypeName = $qTypeDetail[1];
      $_tbtnLinkArr = $parBtnArr[$_qTypeCode];
      $_tFormCreated = $parFormCreatedArr[$_qTypeCode];
      
      $html_thead .= ($_tFormCreated) ? "<th>{$_qTypeName} <span class=\"green\">({$Lang['IES']['SurveyCreated']})</span></th>" : "<th>{$_qTypeName}</th>";
      
      if(empty($_tbtnLinkArr))
      {
        $html_tbtncell .= "<td>&nbsp;</td>";
      }
      else
      {
        $_tbtnLink = $_tbtnLinkArr[0];
        $_tbtnText = $_tbtnLinkArr[1];
      
        $html_tbtncell .= "<td>123 <input type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onClick=\"newWindow('{$_tbtnLink}', 10)\" value=\"{$_tbtnText}\" /> 456</td>";
      }
    }
    
    $html = <<<HTML
<table class="common_table_list IES_q_table">
  <tr>
    {$html_thead}
  </tr>
  <tr>
    <td width=30%>{$Lang['IES']['SurveyTable']['string16']}<br /><br /><a href="tips/stage2_E1_02.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string17']}</a><br /><br /><a href="tips/stage2_E1_03.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string18']}</a></td>
    <td width=36%>{$Lang['IES']['SurveyTable']['string19']}<br />{$Lang['IES']['SurveyTable']['string20']}<br/><a href="tips/stage2_E1_01.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string21']}</a><br /><br /><br />{$Lang['IES']['SurveyTable']['string22']}<br /><a href="tips/stage2_E1_00.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string23']}</a><br /><br /></td>
    <td>{$Lang['IES']['SurveyTable']['string24']}<br /><br />{$Lang['IES']['SurveyTable']['string25']}</p><p></td>
  </tr>
  <tr class="btn_row">
    {$html_tbtncell}
  </tr>
</table>
HTML;

    return $html;
  }
  
  /** GET questionnaire instruction
    * @param : String $questionCode , The request QUESTION CODE 
	* @return : String $html the requested questionnaire instruction
	*/

	public static function getQuestionnaireEditInstruction($surveyType){
		global $ies_cfg, $Lang;
		
		$html = '';
		switch($surveyType){
			case $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]:
				$html = <<<HTML
					{$Lang['IES']['SurveyTable']['string16']}<br /><br /><a href="tips/stage2_E1_02.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string17']}</a><br /><br /><a href="tips/stage2_E1_03.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string18']}</a>
HTML;
			break;
			case $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]:
				$html = <<<HTML
					{$Lang['IES']['SurveyTable']['string19']}<br />{$Lang['IES']['SurveyTable']['string20']}<br/><a href="tips/stage2_E1_01.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string21']}</a><br /><br /><br />{$Lang['IES']['SurveyTable']['string22']}<br /><a href="tips/stage2_E1_00.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo">{$Lang['IES']['SurveyTable']['string23']}</a><br /><br />
HTML;
			break;
			case $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]:
				$html = <<<HTML
					{$Lang['IES']['SurveyTable']['string24']}<br /><br />{$Lang['IES']['SurveyTable']['string25']}</p><p>
HTML;
			break;
		}
		return $html;
	}

	function getQuestionnaireInstruction($surveyType , $action,$stage = 0){

				global $ies_cfg;

				switch($action){
					case $ies_cfg['Questionnaire']['SurveyAction']['EDIT']:
							$html = self::getQuestionnaireEditInstruction($surveyType);
					break;
					case $ies_cfg['Questionnaire']['SurveyAction']['COLLECT']:
							//$html = '<font size="50" color="Green">COLLECT</font>';
							$html = self::getQuestionCollectInstruction($surveyType,$stage);
								
					break;
					case $ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']:
//							$html = '<font size="50" color="Green">ANALYSIS</font>';
							$html = '';
					break;
				}
				return $html;
	}


	public static function getQuestionnaireTableDisplay($parSurveyDetails){
		global $ies_cfg, $Lang;
		$taskID = $parSurveyDetails['TaskID'];
		$student_UserID = $parSurveyDetails['UserID'];

		foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $qTypeDetail){

			$_qTypeCodeNo = $qTypeDetail[0];
			$_qTypeName = $qTypeDetail[1];
			$_qTypeCode = $qTypeDetail[2];
			
			$_qQuestionInside = $parSurveyDetails[$_qTypeCode];

			$_qTypeCount = count($_qQuestionInside);

			$_html_all_questionInfo = '';
			$_htmlTmp = '';

			for($i = 0;$i <$_qTypeCount;$i++){
				$_htmlReceiveSurveyCount = '';

				$_sSurveyID			= $_qQuestionInside[$i]['SurveyID'];
				$_sTitle			= $_qQuestionInside[$i]['Title'];
				$_sDateModified		= $_qQuestionInside[$i]['DateModified'];
				$_sDateInput		= $_qQuestionInside[$i]['DateInput'];

				$_sViewURL = $_qQuestionInside[$i]['ViewURL'];
				$_sEditURL = $_qQuestionInside[$i]['EditURL'];
				$_sMailURL = $_qQuestionInside[$i]['MailURL'];
				$_sCollectManagementURL = $_qQuestionInside[$i]['CollectManagementURL'];
				$_sAnalysisManagementURL = $_qQuestionInside[$i]['AnalysisManagementURL'];

				//DISPLAY MAIL URL ONLY FOR ACTION = "COLLECT" AND SURVERY TYPE IS "SURVEY"
				$_html_mailURL = '';
				if($action == $ies_cfg['Questionnaire']['SurveyAction']['COLLECT'] && $_qTypeCode == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][2]){
					$_html_mailURL = '<a href = "'.$_sMailURL.'" class="mail_dim thickbox" title ="'.$Lang['IES']['SurveyTable']['string3'].'"></a>';
				}
			}
		}

		return 'faifai<br/>';
	}
	/**
	* general UI display for questionnaire table inside "STEP" (with guide)
	*
	* @param : ARRAY $parSurveyDetails Associative Array, details
	* @param : String $action , ACTION FOR THE DISPLAY (EDIT , COLLECT , ANALYSIS FOR THE UI)
	* @param : String $location , THIS UI DISPLAY LOCATION ('F' --> FRONT (ALL STEP ANSWER OVERVIEW), 'I' ---> INSIDE A STEP);
	* @param : INT $stage , OPTIONAL , the current STAGE to call this function, mainly to control the UI display option
	* @return : String $html resulting table
	* 
	*/

	public static function getQuestionnaireTableDisplayWithGuide($parSurveyDetails,$action,$location , $stage = 0){
		global $ies_cfg, $Lang;
//debug_r($parSurveyDetails);	

		if(!in_array($location,$ies_cfg['Questionnaire']['AnswerTableDisplayLocation'])){
			//FOR PROGRAMMING SAFE , SUPPOSE $location SHOULD BE IN $ies_cfg['Questionnaire']['AnswerTableDisplayLocation']
			return '<!-- error [action not find]-->';
		}

		$html_thead = "";
		$html_InsideSurveyTypeHead = '';
		$html_FrontSurveyTypeHead = '';
		$html_FrontAnswerSection = '';

		$taskID = $parSurveyDetails['TaskID'];
		$student_UserID = $parSurveyDetails['UserID'];

		foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $qTypeDetail){

			$_qTypeCodeNo = $qTypeDetail[0];
			$_qTypeName = $qTypeDetail[1];
			$_qTypeCode = $qTypeDetail[2];
			
			$_qQuestionInside = $parSurveyDetails[$_qTypeCode];

			$_qTypeCount = count($_qQuestionInside);

			$_html_all_questionInfo = '';
			$_htmlTmp = '';

			for($i = 0;$i <$_qTypeCount;$i++){
				$_htmlReceiveSurveyCount = '';
//debug_r($_qQuestionInside[$i]);
				$_sSurveyID			= $_qQuestionInside[$i]['SurveyID'];
				$_sTitle			= $_qQuestionInside[$i]['Title'];
				$_sDateModified		= $_qQuestionInside[$i]['DateModified'];
				$_sDateInput		= $_qQuestionInside[$i]['DateInput'];

				$_sViewURL = $_qQuestionInside[$i]['ViewURL'];
				$_sEditURL = $_qQuestionInside[$i]['EditURL'];
				$_sMailURL = $_qQuestionInside[$i]['MailURL'];
				$_sCollectManagementURL = $_qQuestionInside[$i]['CollectManagementURL'];
				$_sAnalysisManagementURL = $_qQuestionInside[$i]['AnalysisManagementURL'];
				$_sSurveyPreviewURL = $_qQuestionInside[$i]['SurveyPreviewURL'];

				//DISPLAY MAIL URL ONLY FOR ACTION = "COLLECT" AND SURVERY TYPE IS "SURVEY"
				$_html_mailURL = '';
				if($action == $ies_cfg['Questionnaire']['SurveyAction']['COLLECT'] && $_qTypeCode == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][2]){
					$_html_mailURL = '<a href = "'.$_sMailURL.'" class="mail_dim thickbox" title ="'.$Lang['IES']['SurveyTable']['string3'].'"></a>';
				}

				if($stage == 3){
					//IN STAGE 3, USER NOT HAVE THE RIGHT TO SEND MAIL ANY MORE
					$_html_mailURL = '';
				}

				//TITLE URL DEPEND ON THE ACTION (edit , collect..)
				switch($action){
					case $ies_cfg['Questionnaire']['SurveyAction']['EDIT']:
						$_html_TitleURL = $_sEditURL;
						break;
					case $ies_cfg['Questionnaire']['SurveyAction']['COLLECT']:
						$_html_TitleURL = $_sCollectManagementURL;

						if($stage == 3){
							$_sViewURL = $_sSurveyPreviewURL;  // WITH STAGE 3 , ALTHOUGH IN COLLECTION, USER SHOULD NOT HAVE THE RIGHT TO EDIT THE QUESTIONNAIRE, SUCH THAT IT IS PREVIEW ONLY

						}
						break;
					case $ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']:
						$_html_TitleURL = $_sAnalysisManagementURL;
						$_sViewURL = $_sSurveyPreviewURL;  // IN ANALYSIS , USER SHOULD NOT HAVE THE RIGHT TO EDIT THE QUESTIONNAIRE, SUCH THAT IT IS PREVIEW ONLY
						break;
				}
				
				if($action == $ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']){
					$libies = new libies();
					$surveyCountAndDate = $libies->getValidSurveyCountAndLastModifiedDate($_sSurveyID);
					$survey_count = $surveyCountAndDate["COUNT"];
					$survey_date = empty($surveyCountAndDate["DATE"])?"--":$surveyCountAndDate["DATE"];
					$_htmlReceiveSurveyCount = '<span class="task_date">'.$Lang['IES']['Finished'].' : '.$survey_count.' '.$Lang['IES']['Copy1'].'</span>';
				}


				$_htmlQuestionTitle = 	'<a href="'.$_html_TitleURL.'" title="'.$Lang['IES']['Manage'].'" target="_blank" class="task_form">'.$_sTitle.'</a>';
				if(($action == $ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']) && ($_qTypeCodeNo != $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0])){
					$_htmlQuestionTitle = '<span class="task_form">'.$_sTitle.'</span>';
				}

				$_html_all_questionInfo .= <<<HTMLTMP
				<p>
					{$_htmlQuestionTitle}
					<div class="table_row_tool">
						{$_html_mailURL}
						<a href="{$_sSurveyPreviewURL}" class="view_dim" title="{$Lang['IES']['View2']}" target="_blank"></a>						
						<!--a href="#" class="delete_dim" title="Delete"></a-->
					</div>
					{$_htmlReceiveSurveyCount}
					<span class="task_date">{$Lang['IES']['CreationDate']} : {$_sDateInput}</span>
				</p>
HTMLTMP;

			}

			$html_InsideSurveyTypeHead .= '<a href = "javascript:void(0);" class="surveyType"  id="sType_'.$_qTypeCodeNo.'" ><span>'.$_qTypeName.'</span></a>';

			if($_qTypeCount > 0){
				//Display the title while count > 0
				$html_FrontSurveyTypeHead .= '<th>'.$_qTypeName.'</th>';
			}

			$questionnaireInstruction = self::getQuestionnaireInstruction($_qTypeCodeNo,$action,$stage);

			$_html_colspan = 'colspan="2"';

			//////////////////////////////////////////////////////
			// ANSWER SECTION (DISPLAY IN BOTH FRONT AND INSIDE)//
			//////////////////////////////////////////////////////
			//PLEASE NOTE, THIS LAYOUT SECTION SHARE IN BOTH FRONT AND INSIDE
			$_answerSection = <<<ANSWERSECTION
				<td>
					<p>{$Lang['IES']['SurveyCreated']} : {$_qTypeCount} {$Lang['IES']['Copy1']}</p>
					{$_html_all_questionInfo}
				</td>			
ANSWERSECTION;

			if($_qTypeCount == 0){
				//IF $_qTypeCount == 0 , STILL SHOW THE NO OF ANSWER IN "ANALYSIS" , 
				//i.e reset $_answerSection and $_html_colspan if $action = "COLLECT" or "EDIT"
				if($action != $ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']){
					$_answerSection = '';
					$_html_colspan = '';
				}
			}else{
				$html_FrontAnswerSection .= $_answerSection;
			}

			if($location == $ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['INSIDE']){
				/////////////////////////
				// INSTRUCTION SECTION //
				/////////////////////////
				if($action == $ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']){
					$_instructionSection  = '';
				}else{
					$_instructionSection  = '<td>'.$questionnaireInstruction.'</td>';
				}

				/////////////////////
				// NEW BTN SECTION //
				/////////////////////
				$_newBtn = '';
				if($action == $ies_cfg['Questionnaire']['SurveyAction']['EDIT']){
					$_newBtn = <<<newBTN
					<tr class="btn_row">
					 <td {$_html_colspan}>
						<input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="newWindow('/home/eLearning/ies/survey/createForm.php?TaskID={$taskID}&SurveyType={$_qTypeCodeNo}',10)" value="{$Lang['IES']['Create']}" /></td>
					</tr>
newBTN;
				}


				$htmlInside .= <<<HTML3
					<div id = "area_sType_{$_qTypeCodeNo}" style="display:none;">
						<table class="common_table_list IES_task_form_table">
						<col width="50%"><col />
							<tr>
								{$_instructionSection}
								{$_answerSection}
							</tr>
						
							{$_newBtn}

						</table>
					</div>\n
HTML3;
			}
		}


		//RETURN LAYOUT DEPEND ON $location VALUE
		if($location == $ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['INSIDE']){
			$html4 = <<<HTML4
				<div class="task_form_btn">
					{$html_InsideSurveyTypeHead}
				</div>
				<br class="clear">
HTML4;
			return $html4.$htmlInside;
		}else{
			$html4 = <<<HTML4
                <table class="common_table_list IES_q_table" >
				<tr>
					{$html_FrontSurveyTypeHead}
				</tr>
				<tr>
					{$html_FrontAnswerSection}
					</tr>
				</table>
HTML4;
			return $html4;
		}		
  }



  	public static function getSurveyAnsweredRespondentTable($ParList, $ParIS_IES_STUDENT, $ParoriginalKey,$Survey_type){


  		global $Lang, $ies_cfg;

		switch($Survey_type){
			case $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]:
				$h_dateCaption = $Lang['IES']['inputdateInterview'];
			break;
			case $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]:
				$h_dateCaption = $Lang['IES']['inputdateObserve'];
			break;
			default:
				$h_dateCaption = $Lang['IES']['inputdateSurvey'];
			break;
		}
		$html_table = "<table class=\"common_table_list\">";
		$html_table .= "<thead><tr><th class=\"num_check\">#</th><th>{$Lang['IES']['Respondent']}</th>";
		$html_table .= "<th>{$h_dateCaption}</th><th>{$Lang['IES']['question_is_valid']}</th>";
		if($ParIS_IES_STUDENT && !empty($ParList)){
			$html_table .= "<th>&nbsp;</th>";
		}
		$html_table .= "</tr></thead><tbody>";
		
		if(empty($ParList)){
			$html_table .= "<tr><td colspan = '4' style='text-align:center;'>{$Lang['IES']['NoRecordAtThisMoment']}</td></tr>";
		}
		else{
			for($i = 0; $i < sizeof($ParList); $i++){
				$count = $i + 1;
				$html_table .= "<tr>";
				$html_table .= "<td>{$count}</td>";
				if($ParList[$i]["RespondentNature"] == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
					$html_table .= "<td>{$ParList[$i]["StudentName"]} </td>";
				}
				else{
					$html_table .= "<td>{$ParList[$i]["Respondent"]} </td>";
				}
				
				$html_table .= "<td>{$ParList[$i]["InputDate"]}</td>";
				if($ParList[$i]["Status"]){
					$html_table .= " <td><span class=\"valid\">{$Lang['IES']['is_valid']}</span></td>";
				}
				else{
					$html_table .= "<td><span class=\"invalid\">{$Lang['IES']['is_invalid']}</span></td>";
				}
				if($ParIS_IES_STUDENT){
					$html_table .= "<td class=\"sub_row\"><div class=\"table_row_tool\">";
					
					//$key = base64_encode("SurveyID={$ParList[$i]["SurveyID"]}&Respondent={$ParList[$i]["Respondent"]}&isedit=1");
					if($ParList[$i]["RespondentNature"] != $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
						$respondent_key = $ParList[$i]["Respondent"];
					}
					else{
						$respondent_key = $ParList[$i]["UserID"];
					}
//					$html_table .= "<a href=\"result.php?key={$ParoriginalKey}&Respondent={$respondent_key}&respondentNature={$ParList[$i]["RespondentNature"]}&isedit=1\" class=\"edit_dim\" title=\"Edit\"></a>";
					$html_table .= "<a href=\"index.php?mod=survey&task=result&key={$ParoriginalKey}&Respondent={$respondent_key}&respondentNature={$ParList[$i]["RespondentNature"]}&isedit=1\" class=\"edit_dim\" title=\"Edit\"></a>";
					$encode_ansid = base64_encode($ParList[$i]["AnswerID"]);
					$html_table .= "<a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"Delete\" onclick=\"javascript:DoDelete('{$encode_ansid}')\"></a></div></td>";
				}
				$html_table .= "</tr>";
			}
		}
		
		
		$html_table .= "</tbody></table>";
		
		return $html_table;
	}
	
	public static function getSurveyDiscoveryTable($ParList, $ParIS_IES_STUDENT, $ParoriginalKey){
  		global $Lang, $ies_cfg;
		$html_table = "<table class=\"common_table_list\">";
		$html_table .= "<thead><tr><th class=\"num_check\">#</th><th>{$Lang['IES']['Respondent']}</th>";
		$html_table .= "<th>{$Lang['IES']['inputdate']}</th><th>{$Lang['IES']['question_is_valid']}</th>";
		if($ParIS_IES_STUDENT && !empty($ParList)){
			$html_table .= "<th>&nbsp;</th>";
		}
		$html_table .= "</tr></thead><tbody>";
		
		if(empty($ParList)){
			$html_table .= "<tr><td colspan = '4' style='text-align:center;'>{$Lang['IES']['NoRecordAtThisMoment']}</td></tr>";
		}
		else{
			for($i = 0; $i < sizeof($ParList); $i++){
				$count = $i + 1;
				$html_table .= "<tr>";
				$html_table .= "<td>{$count}</td>";
				if($ParList[$i]["RespondentNature"] == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
					$html_table .= "<td>{$ParList[$i]["StudentName"]} </td>";
				}
				else{
					$html_table .= "<td>{$ParList[$i]["Respondent"]} </td>";
				}
				
				$html_table .= "<td>{$ParList[$i]["InputDate"]}</td>";
				if($ParIS_IES_STUDENT){
					$html_table .= "<td class=\"sub_row\"><div class=\"table_row_tool\">";
					
					//$key = base64_encode("SurveyID={$ParList[$i]["SurveyID"]}&Respondent={$ParList[$i]["Respondent"]}&isedit=1");
					if($ParList[$i]["RespondentNature"] != $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
						$respondent_key = $ParList[$i]["Respondent"];
					}
					else{
						$respondent_key = $ParList[$i]["UserID"];
					}
					
					# Delete Icon
					$encode_ansid = base64_encode($ParList[$i]["AnswerID"]);
					$html_table .= "<a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"Delete\" onclick=\"javascript:DoDelete('{$encode_ansid}')\"></a></div></td>";
				}
				$html_table .= "</tr>";
			}
		}
		
		
		$html_table .= "</tbody></table>";
		
		return $html_table;
	}
	
		/**
		* Generate HTML FUNCTION TAB FOR THE ANALYSIS BROAD
		*
		* @param : STRING $currentTag CURRENT USING FUNCTION, for display active tag	
		* @param : STRING $ParIsIESStudent true if is ies student, vice versa
		* @param String $originalKey - encoded URI
		* @param String $ParFromPage - specify where the page comes from e.g. Collect:Survey,Collect:Interview
		* @return : String HTML FUNCITON TAB
		* 
		*/
		static public function GET_MANAGEMENT_BROAD_TAB_MENU($currentTag,$ParIsIESStudent,$originalKey,$ParFromPage) {
			global $Lang;

			if (empty($ParOtherParamentersArray)) {
				$ParOtherParamentersArray = array();
			}
			list($FromTask,$FromModule) = explode(":",$ParFromPage);
			$FromTask = strtolower($FromTask);
			$FromModule = strtolower($FromModule);

			//$manageAnswerCaption = (strtolower($FromModule) == strtolower("observe"))? $Lang['IES']['manage_answered_questionaireType2']: $Lang['IES']['manage_answered_questionaire'];
			$manageAnswerCaption = $Lang['IES']['manage_answered_questionaire'];
			if($FromModule=="observe"){
				$manageAnswerCaption = $Lang['IES']['manage_answered_observation'];
			}
			$decodedURIs = libies_survey::breakEncodedURI($originalKey);
			
//			if ($FromTask == "Collect") {
			if ($FromTask == "collect") {
				if (!$decodedURIs["IsStage3"]) {
					//CONFIG the tab element for this broad

//					$tab_array["ManageCompleteQuestionnaire"] = array("management.php?key=$originalKey",$manageAnswerCaption,"");
					$tab_array["ManageCompleteQuestionnaire"] = array("index.php?mod=survey&task=management&key=$originalKey",$manageAnswerCaption,"");
				}
				else {
					$tab_array["ManageCompleteQuestionnaire"] = array("javascript:void();", $manageAnswerCaption, "onClick=\"goManagement()\"");
				}
			}
//			if ($FromTask == "Collect"&&$FromModule=="Survey") {
			if ($FromTask == "collect"&&$FromModule=="survey") {
				
				if (!$decodedURIs["IsStage3"]) {
//					$tab_array["EmailList"] = array("email_list.php?key=$originalKey",$Lang['IES']['EmailList'],"");
					$tab_array["EmailList"] = array("index.php?mod=survey&task=email_list&key=$originalKey",$Lang['IES']['EmailList'],"");
				}	
				$tab_array["Overall"] = array("#",$Lang['IES']['Overall'],"onClick=\"goDisplay()\"");	
				/*
				if($ParIsIESStudent)
				{
					$tab_array["Grouping"] = array("#",$Lang['IES']['Grouping'],"onClick=\"goGrouping()\"");
				}
				*/
			}
			if ($FromTask == "collect"&&$FromModule=="survey" 
				|| $FromTask == "analyze"&&$FromModule=="survey" ) {
				$tab_array["GroupingList"] = array("#",$Lang['IES']['GroupingList'],"onClick=\"goGroupingList()\"");
				}
			if ($decodedURIs["IsStage3"] && (($FromTask == "collect" && $FromModule=="interview") || ($FromTask == "collect" && $FromModule=="observe")))
			{
				$tab_array["GroupingList"] = array("#", $Lang['IES']['answered_questionaire_discovery'], "onClick=\"goDiscoveryList()\"");
			}
				
				
				
			$html = self::GET_BROAD_TAB_MENU($tab_array,$currentTag);
			return $html;
		}
		
		
		
		/**
		* Generate HTML FUNCTION TAB FOR THE BROAD
		*
		* @param : ARRAY $tabArray ARRAY TO STORE ALL THE TAB NEED TO DISPLAY
		* @param : STRING $currentTagFunction CURRENT USING FUNCTION, for display active tag	
		* @return : String HTML FUNCITON TAB
		* 
		*/
		static public function GET_BROAD_TAB_MENU($tabArray, $currentTagFunction="")
		{		
				$x = '<div class="watermark"></div>';
				$x .= "<ul class=\"q_tab\">";
				if(is_array($tabArray) > 0)
				{
					foreach ($tabArray as $thisTabFunction => $thisDetails)
					{
		
						$ClassStyle = ($thisTabFunction == $currentTagFunction) ? "current" : "";								
		
						$Link = $thisDetails[0];   // DATA STRUCTURE the first element is the Link					
						$Title = $thisDetails[1];  // DATA STRUCTURE the second element is the Module title
						$OtherAttr = $thisDetails[2]; // DATA STRUCTURE the third element is the Link others attribute 			
						$x .= "<li class =\"{$ClassStyle}\"><a href = \"{$Link}\" {$OtherAttr}>{$Title}</a></li>";
					}
				}
				$x .= "</ul>";
				return $x;
		}
		
		static public function GetMCAnsDisplay($ParArry){
			$x = "";
			
			for($i = 0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++){
				if($i == $ParArry["ANSWER"] && $ParArry["ANSWER"] != ""){
					$x .= "<INPUT type='radio' CHECKED DISABLED />".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
				}
				else{
					$x .= "<INPUT type='radio' DISABLED />".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
				}
			}
			return $x;
		}
		
		static public function GetMPAnsDisplay($ParArry){
			global $ies_cfg;
			$x = "";
			$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);
			for($i = 0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++){
				if($AnsArry[$i] == 1){
					$x .= "<INPUT type='checkbox' CHECKED DISABLED />".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
				}
				else{
					$x .= "<INPUT type='checkbox' DISABLED />".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
				}
			}
			return $x;
		}
		
		static public function GetShortAnsDisplay($ParArry){
			global $ies_cfg;
			$x = $ParArry["ANSWER"];
			return $x;
		}
		
		static public function GetLongAnsDisplay($ParArry){
			global $ies_cfg;
			$x = "<div>".nl2br($ParArry["ANSWER"])."</div>";
			return $x;
		}
		
		static public function GetTableLikertAnsDisplay($ParArry){
			global $ies_cfg;
			$x = "";
			$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);
			
			$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" ><tr><td></td>";
		        
		    for($i=0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++)
			{
				$x .= "<td>".$ParArry["QUESTION"]["OPTIONS"][$i]."</td>";
			}
			$x .= "</tr>";
			for($i = 0; $i < $ParArry["QUESTION"]["QUESTION_NUM"]; $i++){
				$x .= "<tr>";
				$x .= "<td>".$ParArry["QUESTION"]["QUESTION_QUESTIONS"][$i]."</td>";
				for($j = 0; $j < $ParArry["QUESTION"]["OPTION_NUM"]; $j++){
		//			DEBUG_R($AnsArry[$i]);
					if($AnsArry[$i] == $j && $AnsArry[$i] != ""){
						$x .= "<td><INPUT type='radio' CHECKED DISABLED /></td>";
					}
					else{
						$x .= "<td><INPUT type='radio' DISABLED /></td>";
					}
				}
				$x .= "</tr>";
		
			}
			$x .= "</table>";
			return $x;
		}
		
		static public function GetTableInputAnsDisplay($ParArry){
			global $ies_cfg;
			$x = "";
			$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);
			$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" ><tr><td></td>";
		        
		    for($i=0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++)
			{
				$x .= "<td>".$ParArry["QUESTION"]["OPTIONS"][$i]."</td>";
			}
			$x .= "</tr>";
			for($i = 0; $i < $ParArry["QUESTION"]["QUESTION_NUM"]; $i++){
				$x .= "<tr>";
				$x .= "<td>".$ParArry["QUESTION"]["QUESTION_QUESTIONS"][$i]."</td>";
				for($j = 0; $j < $ParArry["QUESTION"]["OPTION_NUM"]; $j++){
					$Ans_num = $i * $ParArry["QUESTION"]["OPTION_NUM"] + $j; 
					$x .= "<td>".$AnsArry[$Ans_num]."</td>";
				}
				$x .= "</tr>";
		
			}
			$x .= "</table>";
			return $x;
		}
		
		static public function getSurveyResultTable($ParAnsArry){
			global $ies_cfg;
			if(!empty($ParAnsArry)){
				$count = 1;
				foreach($ParAnsArry as $value){
					$HTML_ANS .= "<div class=\"ies_q_box\"><table class=\"form_table\">";
					$HTML_ANS .= "<tr><td class=\"q_title\">".$count.". ".$value["QUESTION"]["TITLE"]."</td></tr><tr><td>";
					switch($value["QUESTION"]["TYPE"]){
			
						case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
							
							$HTML_ANS .= self::GetMCAnsDisplay($value);
							break;
						case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
							
							$HTML_ANS .= self::GetMPAnsDisplay($value);
							break;
						case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
							
							$HTML_ANS .= self::GetShortAnsDisplay($value);
							break;
						case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
							
							$HTML_ANS .= self::GetLongAnsDisplay($value);
							break;
						case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
						
							$HTML_ANS .= self::GetTableLikertAnsDisplay($value);
							break;
						case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:
							
							$HTML_ANS .= self::GetTableInputAnsDisplay($value);
							
							break;
							
					}
					$HTML_ANS .= "</td></tr></table></div>";
					$count++;
				}
			}
			return $HTML_ANS;
		}
	static function getCloseButton() {
		global $Lang;
		return "<input name=\"submit2\" onclick=\"javascript:window.open('','_parent','');window.close();\" type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" value=\" ".$Lang['IES']['Close']." \" />";
	}
	static function getMsgRecordUpdated() {
		global $Lang;
		return "<div class=\"alert_message\"><span class=\"success\">".$Lang['IES']['AlertMsg']['update']."</span></div>";
	}
	static function getMsgRecordFail() {
		global $Lang;
		return "<div class=\"alert_message\"><span class=\"fail\">".$Lang['IES']['AlertMsg']["update_failed"]."</span></div>";
	}
	
	static function getSurveyMappingInfoDisplay($Survey_info_arr) {
		global $Lang;
		
//		debug_r($Lang['IES']['SurveyTable']['string13']);
//				debug_r($Lang['IES']['SurveyTable']['string14']);
//				debug_r($Lang['IES']['SurveyTable']['string15']);
		$Survey_info_arr_keys = array_keys($Survey_info_arr);
		$html_title_arr = array($Survey_info_arr_keys[0]=>$Lang['IES']['SurveyTable']['string13'],
								$Survey_info_arr_keys[1]=>$Lang['IES']['SurveyTable']['string14'],
								$Survey_info_arr_keys[2]=>$Lang['IES']['SurveyTable']['string15']);
		$html_str = "";
		
		foreach($Survey_info_arr as $key=>$Survey_info){
//debug_r($Survey_info);
//debug_r(count($Survey_info));
			for($i = 0, $i_max = count($Survey_info); $i < $i_max; $i++){
				$_details = $Survey_info[$i];
				if(count($_details["Mapping"]) > 0){
					$html_str .= "<span class=\"prev_answer\">".$html_title_arr[$key]." - ".$_details["Title"]."</span>
							  <div class=\"answer_area\">
	    						<table class=\"answer common_table_list\">
	      							<col width=\"200\">
	      							<col>";
					for($j=0,$j_max = count($_details["Mapping"]); $j < $j_max; $j++){
						$html_str .= "	<tr>
										<td>".($j+1)." ) ".$_details["Mapping"][$j]["MappingTitle"]."</td>
										<td class=\"student_ans\">".nl2br($_details["Mapping"][$j]["Comment"])."&nbsp;</td>
									</tr>";
					}
					$html_str .= "</table></div><br/>";
				}
			}

			/*
			if(count($Survey_info["Mapping"])){
				$html_str .= "<span class=\"prev_answer\">".$html_title_arr[$key]." - ".$Survey_info["Title"]."</span>
							  <div class=\"answer_area\">
	    						<table class=\"answer common_table_list\">
	      							<col width=\"200\">
	      							<col>";
				for($i=0;$i<count($Survey_info["Mapping"]);$i++){
					$html_str .= "	<tr>
										<td>".($i+1)." ) ".$Survey_info["Mapping"][$i]["MappingTitle"]."</td>
										<td class=\"student_ans\">".nl2br($Survey_info["Mapping"][$i]["Comment"])."</td>
									</tr>";
				}
				$html_str .= "</table></div><br/>";
			}*/
		}
		
		return $html_str;
	}
	public static function getTextareaDisplayWithTitle($ParTitle, $ParTextareaHTML) {
		$html = <<<HTML
			<h4 class="stu_edit"><span class="task_instruction_text">{$ParTitle}</span></h4>
			{$ParTextareaHTML}
HTML;
		return $html;
	}
	public static function getSurveyBodyClass($ParSurveyType) {
		global $ies_cfg;
		
		switch ($ParSurveyType) {
			case $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]:
				$html_ies_survey_body_class = 'ies_q';
				break;
			case $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]:
				$html_ies_survey_body_class = 'ies_i';
				break;
			case $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]:
				$html_ies_survey_body_class = 'ies_f';
				break;
			default:
				$html_ies_survey_body_class = 'ies_q';
		}
		
		return $html_ies_survey_body_class;
	}
}

?>