<?php
//editing : 
include_once($intranet_root."/includes/sba/libSbaStepHandin.php");

class libSbaStepHandinFactory{
	public function __construct() {
		
    }
   
    public static function createStepHandin($parTaskID, $StepHandinType, $parStepID=0, $parQuestion='', $parAnswerId=0){
    	global $sba_cfg, $intranet_root;

		$surveyEditIncludeFile = $intranet_root.'/includes/sba/libSbaStepHandinQuestionnaireEDIT.php';

		$surveyAnalysisIncludeFile = $intranet_root.'/includes/sba/libSbaStepHandinQuestionnaireANALYSIS.php';

    	switch($StepHandinType){

    		case $sba_cfg['DB_IES_STEP_QuestionType']['longQuestion'][0]:
				$libFile = $intranet_root.'/includes/sba/libSbaStepHandinLONGQUESTION.php';
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinLONGQUESTION($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;
		
		case $sba_cfg['DB_IES_STEP_QuestionType']['selfEvaluation'][0]:
				$libFile = $intranet_root.'/includes/sba/libSbaStepHandinSelfEvaluation.php';
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinSelfEvaluation($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;
		
		case $sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'][0]:
				$libFile = $intranet_root.'/includes/sba/libSbaStepHandinTablePlanner.php';
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinTablePlanner($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		case $sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'][0]:
				$libFile = $surveyEditIncludeFile;
				if(file_exists($libFile)){

					include_once($libFile);
					return new libSbaStepHandinSurveyEDIT($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		case $sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'][0]:
				$libFile = $surveyEditIncludeFile;
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinObserveEDIT($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		case $sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'][0]:
				$libFile = $surveyEditIncludeFile;
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinInterviewEDIT($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		case $sba_cfg['DB_IES_STEP_QuestionType']['surveyAnalysis'][0]:
    			$libFile = $surveyAnalysisIncludeFile;
		
				if(file_exists($libFile)){
					include_once($libFile);

					return new libSbaStepHandinSurveyANALYSIS($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		case $sba_cfg['DB_IES_STEP_QuestionType']['observeAnalysis'][0]:
    			$libFile = $surveyAnalysisIncludeFile;
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinObserveANALYSIS($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		case $sba_cfg['DB_IES_STEP_QuestionType']['interviewAnalysis'][0]:
    			$libFile = $surveyAnalysisIncludeFile;
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinInterviewANALYSIS($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;
    		
    		case $sba_cfg['DB_IES_STEP_QuestionType']['powerConcept'][0]:
    			$libFile = $intranet_root.'/includes/sba/libSbaStepHandinPOWERCONCEPT.php';
				if(file_exists($libFile)){
					include_once($libFile);
					return new libSbaStepHandinPOWERCONCEPT($parTaskID, $parStepID, $parQuestion, $parAnswerId);
				}else{
					return null;
				}
    			break;

    		default :
    			return null;
    	}
    }
}

?>