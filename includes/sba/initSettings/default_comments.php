<?php
$temp = <<<EOT
Stage 1
##擬題##Formulation of Title
:-B5
--研究題目很有創意。(+)
--擬訂研究題目時展現心思。(+)
--研究題目的設計很清晰。(+)
--研究題目基本上是可行的。(+)
--研究題目深淺程度適中。(+)
--研究題目能展示通識科的相關概念。(+)
--研究題目內概念定義含糊。(-)
--研究題目太空泛。(-)
--研究題目探究範圍太大。(-)
--研究題目要探究的內容太多。(-)
--研究純粹只涉及個人興趣，研究意義不大。(-)
--研究題目涉及的研究對象難以接觸。(-)
:-EN
--The title is very innovative. (+)  
--Your thoughtfulness is demonstrated in the title. (+)  
--The design of the title is very clear. (+)  
--The study is basically feasible. (+)  
--The difficulty level of the study is optimal. (+)  
--The title can demonstrate related concepts covered in Liberal Studies. (+)  
--Definitions of the title concepts are vague. (-)  
--The title is too vague. (-)  
--The scope of the study is too wide. (-)  
--The study covers too much content. (-)  
--The study is based on personal interests only and is not meaningful. (-)  
--The research objects required for the study are inaccessible.(-)  

##擬題建議##Recommendations for formulating a title
:-B5
--需要搜集更多有關研究題目的資料。
--需要把研究題目涉及的範圍收窄。
--應考慮是否能在指定時間內完成研究。
--應考慮是否有足夠資源進行研究。
--需要選擇可接觸的研究對象。
:-EN
--(You should) search for more information about the study.
--(You should) narrow the scope of the study.
--(You should consider) whether the study can be completed in a specified time.
--(You should consider) whether the resources are enough for the study.
--(You should) choose accessible research objects.
##題目範圍和定義##Scope and definition of title
:-B5
--題目富有原創性。(+)
--題目探究範圍適中。(+)
--題目的關鍵字定義清晰。(+)
--題目原創性略為不足。(-)
--題目所涉及的範圍太廣，搜集資料時會有困難。(-)
--題目的關鍵字定義欠清晰。(-)
:-EN
--The title is originative. (+)
--The scope of the title is optimal. (+)
--The key words of the title are well-defined. (+)
--The title is not originative enough. (-)
--The scope covered in the study is too large for data collection.(-)
--The key words of the title are not well-defined. (-)

##探究切入點##Perspectives/Entry points of the study
:-B5
--題目能引發跨單元和多角度探究。(+)
--題目中的字眼中肯，沒有價值觀判斷。(+)
--題目未能引發跨單元和多角度探究。(-)
--題目中的字眼含有價值觀判斷，不夠中肯。(-)
:-EN
--The title can initiate a cross-module and multi-perspective enquiry. (+)
--The wording of the title is objective and does not imply pre-occupied value judgement. (+)
--The title does not facilitate a cross-module and multi-perspective enquiry. (-)
--The wording of the title implies pre-occupied value judgement, not objective enough. (-)

##相關概念的應用##Application of related concepts
:-B5
--題目能應用合適的相關概念。(+)
--能清楚給予相關概念清楚定義。(+)
--題目能包含通識科的相關概念。(+)
--題目未能應用合適的相關概念。(-)
--未能清楚給予相關概念清楚定義。(-)
--題目未有包含通識科的相關概念。(-)

:-EN
--Appropriate related concepts are applied to the study.(+)
--The related concepts are well-defined.(+)
--The title includes the related concepts in Liberal Studies.(+)
--Appropriate related concepts are not applied to the study.(-)
--The related concepts are not well-defined. (-)
--The title does not include the related concepts in Liberal Studies. (-)

##研究動機/目的##Research motivation/objective
:-B5
--研究符合通識科課程精神，有研究價值。(+)
--研究目的清晰。(+)
--研究目的與研究題目很配合。(+)
--研究不符合通識科課程精神，沒有研究價值。(-)
--研究目的不夠清晰。(-)
--研究只局限於個人興趣。(-)
--研究目的與研究題目並不配合。(-)
:-EN
--The study is worthy for its attachment to the Liberal Studies curriculum. (+)  
--The research objective is clear. (+)
--The research objective and the title are related. (+)
--The study is not worthy since it has no attachment to the Liberal Studies curriculum. (-)
--The research objective is not specific enough. (-)
--The study is solely confined to personal interests. (-)  
--The research objective and the title are unrelated. (-)

##焦點問題##Focus questions
:-B5
--焦點問題非常具體清晰。(+)
--焦點問題頗為具體清晰。(+)
--焦點問題能協助達到研究目的。(+)
--焦點問題能回應研究題目。(+)
--部分焦點問題欠清晰。(-)
--焦點問題欠清晰。(-)
--焦點問題未能協助達到研究目的。(-)
--部分焦點問題未能回應研究目的。(-)
--焦點問題未能回應研究目的。(-)
--部分焦點問題與研究目的沒有關連。(-)
--焦點問題與研究目的沒有關連。(-)
--部分焦點問題偏離了研究目的。(-)
--焦點問題偏離了研究目的。(-)
:-EN
--The focus questions are very concrete and specific. (+)
--The focus questions are quite concrete and specific. (+)  
--The focus questions can help to serve the research objective. (+)
--The focus questions can address the research title. (+)
--Some focus questions are not specific enough. (-)
--The focus questions are not specific enough. (-)  
--The focus questions cannot help to serve the research objectives. (-)
--Some focus questions cannot address the research objectives. (-)
--The focus questions cannot address the research objectives. (-)
--Some focus questions and the research objectives are unrelated.(-)
--The focus questions and the research objectives are unrelated.(-) 
--Some focus questions deviate from the research objectives. (-)
--The focus questions deviate from the research objectives. (-)

##文獻探討##Literature review##
:-B5
--能引述適當的文獻資料。(+)
--文獻資料來源多元化。(+)
--文獻資料全面和充足。(+)
--未能引述適當的文獻資料。(-)
--引述了不適當的文獻資料。(-)
--文獻資料來源不夠多元化。(-)
--文獻資料欠全面和充足。(+)
--大部分參考資料來自網上討論區、維基百科等，權威性不高。(-)
:-EN
--Appropriate literature is cited. (+)
--Sources of the literature are diversified. (+)
--The literature is comprehensive and sufficient. (+)  
--Appropriate literature cannot be cited. (-)
--Inappropriate literature is cited. (-)
--Sources of the literature are not diversified enough. (-)
--The literature is not comprehensive and sufficient enough. (+)
--Most of the references come from the Internet and Wikipedia which are not quite authoritative. (-)

##工作計劃##Work plan
:-B5
--設定的工作計劃很適當。(+)
--工作計劃基本上可行。(+)
--設定的工作計劃很具體。(+)
--工作計劃的時間分配恰當。(+)
--設定的工作計劃能切合研究目的。(+)
--設定的工作計劃並不適當。(-)
--工作計劃並不可行。(-)
--設定的工作計劃未夠具體。(-)
--工作計劃的時間分配並不恰當。(-)
--設定的工作計劃並未能切合研究目的。(-)
--工作計劃遺漏了一些重要項目。(-)
:-EN
--The work plan is set appropriately. (+)
--The work plan is basically feasible. (+)
--The work plan is very specific. (+)
--The time allocation of the work plan is appropriate. (+)
--The work plan can meet the research objectives. (+)
--The work plan is not set appropriately. (-)
--The work plan is not feasible. (-)
--The work plan is not specific enough. (-)
--The time allocation of the work plan is inappropriate. (-)
--The work plan cannot meet the research objectives. (-)  
--Some important processes are missed out in the work plan.(-)

##反思##Reflection
:-B5
--反思內容具體。(+)
--在探究過程中顯示高度反思能力。(+)
--在探究過程中顯示一般程度的反思能力。(+)
--在探究過程中能持續地作自我反思。(+)
--反思內容流於空泛。(-)
--在探究過程中顯示低度反思能力。(-)
--在探究過程中未能作自我反思。(-)
:-EN
--The reflection is specific. (+)  
--A high level of competence in the reflection on the enquiry is shown. (+)
--A moderate level of competence in the reflection on the enquiry is shown. (+)
--Continuous reflection is demonstrated in the course of study. (+)
--The reflection is too superficial. (-)
--A low level of competence in the reflection on the enquiry is shown. (-)
--Reflection is not demonstrated in the course of study. (-)

##獨立思考##Independent thinking
:-B5
--能使用與議題相關的資料，並考慮其準確性。(+)
--能在議題中聯繫概念和知識。(+)
--能建立合理的論點。(+)
--能提供不同觀點和見解。(+)
--能比較與議題相關的不同角度。(+)
--未能使用與議題相關的資料，並考慮其準確性。(-)
--未能在議題中聯繫概念和知識。(-)
--未能建立合理的論點。(-)
--未能提供不同觀點和見解。(-)
--未能比較與議題相關的不同角度。(-)
:-EN
--Ability to include information which is relevant to the issue concerned and to consider its accuracy is shown. (+)
--Ability to relate concepts and knowledge to the issue concerned is shown. (+)
--Ability to make reasoned argument is shown. (+)
--Ability to provide ideas and viewpoints is shown. (+)
--Ability to identify and compare multiple perspectives of the issue concerned is shown. (+)
--Ability to include information which is relevant to the issue concerned and to consider its accuracy is not shown. (-)
--Ability to relate concepts and knowledge to the issue concerned is not shown. (-)
--Ability to make reasoned argument is not shown. (-)
--Ability to provide ideas and viewpoints is not shown. (-)
--Ability to identify and compare multiple perspectives of the issue concerned is not shown. (-)

##溝通##Communication
:-B5
--使用有效的溝通媒介和方法去傳達資料。(+)
--能清晰和有組織地溝通。(+)
--能與其他人交換意見和資訊。(+)
--未能清晰和有組織地溝通。(-)
--沒有使用有效的溝通媒介和方法去傳達資料。(-)
--未能與其他人交換意見和資訊。(-)
:-EN
--Effective means of communication is employed to pass on information.  (+)
--Communication is clear and organised. (+)
--Ability to exchange ideas and information with others is shown. (+)
--Communication is not clear and organised. (-)
--No effective means of communication is employed to pass on information. (-)
--Ability to exchange ideas and information with others is not shown. (-)
##努力與付出##Effort
:-B5
--有良好的時間與資源的管理。(+)
--表現出主動解決問題和不斷改善的積極態度。(+)
--能主動提出問題及尋找支援。(+)
--時間與資源的管理能力不足。(-)
--未表現出主動解決問題和不斷改善的積極態度。(-)
--未能主動提出問題及尋找支援。(-)
:-EN
--Good management of time and resources is demonstrated. (+)
--Positive attitudes towards problem solving and self-improvement are displayed. (+)
--Eagerness in asking questions and seeking assistance is demonstrated. (+)
--Ability of management of time and resources is lacked. (-)
--Positive attitudes towards problem solving and self-improvement are not displayed. (-)
--Eagerness in asking questions and seeking assistance is not demonstrated. (-)

Stage 2
##研究方法##Research method
:-B5
--研究方法很具體。(+)
--研究方法可行。(+)
--研究方法尚算可行。(+)
--研究方法可以切合題目所需。 (+)
--研究方法欠具體。(-)
--研究方法可行性不大。(-)
--研究方法未能切合題目所需。 (+)
:-EN
--The research method is very specific. (+)
--The research method is workable. (+)
--The research method is quite workable. (+)
--The research method meets the requirements of the study. (+)
--The research method is not specific enough (-)  
--The research method is most likely unworkable. (-)
--The research method does not meet the requirements of the study. (+)
##運用的工具##Research tool
:-B5
--能以合適的研究工具搜集完整及有用的資料。(+)
--以頗合適的研究工具搜集完整及有用的資料。(+)
--所選的研究工具能收集題目及焦點問題所需的關鍵資料。(+)
--所選的研究工具數量足夠。(+)
--未能以合適的研究工具搜集完整及有用的資料。(-)
--所選的研究工具未能收集題目及焦點問題所需的關鍵資料。(-)
--所選的研究工具數量並不足夠。(-)
:-EN
--Ability to deploy comprehensive and highly useful data collected with appropriate research tool(s) is shown. (+)
--Ability to deploy fairly comprehensive and useful data collected with appropriate research tool(s) is shown. (+)
--The research tool selected enables collection of data relevant to the title and focus questions. (+)
--The quantity of the research tools selected is sufficient. (+)
--Inappropriate research tool(s) is/are used to deploy comprehensive and useful data collected. (-)
--The research tool selected does not enable collection of data relevant to the title and focus questions. (-)
--The quantity of the research tools selected is insufficient. (-)
##處理數據##Data management
:-B5
--分析資料的方法恰當，能得出重要的探究結果。(+)
--分析資料的方法尚算恰當，得出頗為重要的探究結果。(+)
--能把搜集的數據妥善歸類和整合。(+)
--整合後的數據能回應焦點問題。(+)
--分析資料的方法不恰當，得出並不重要的探究結果。(-)
--未能把搜集的數據妥善歸類和整合。(-)
--整合後的數據未能回應焦點問題。(-)
:-EN
--Appropriate methods are used to produce significant findings. (+)
--Fairly appropriate methods are used to produce significant findings. (+)
--The data collected are categorised and integrated properly. (+)
--The data collected can address the focus questions after integration. (+)
--Inappropriate methods are used to produce significant findings. (-)
--The data collected are not categorised and integrated properly. (-)
--The data cannot address the focus questions after integration. (-)
##研究所得##Research outcome
:-B5
--能使用有效的方法 （例如：圖表）來展示研究所得。(+)
--能有系統和準確地表達研究所得。(+)
--大致上能交代研究所得，然而表達方式和組織方面有待改善。(+)
--未能使用有效的方法來展示研究所得。 (-)
--未能有系統和準確地表達研究所得。 (-)
:-EN
--Effective means (e.g. graph) is adopted to present the research outcome. (+)
--The research outcome is presented systematically and precisely. (+)
--The research outcome is fairly presented, but improvement is needed in the presentation mode and organisation. (+)
--Effective means is not adopted to present the research outcome. (-)
--The research outcome is not presented systematically and precisely. (-)
##研究困難/限制##Limitations / Difficulties of research
:-B5
--能清楚預計研究過程中的困難和限制，並在研究方法和工具作出相應的改善。(+)
--能清楚指出研究過程中的困難和限制，並提出合理的解決方法。(+)
--能清楚指出研究過程中的困難和限制，但未能提出合理的解決方法。(+)
--未能清楚預計研究過程中的困難和限制，並在研究方法和工具作出相應的改善。(-)
--未能清楚指出研究過程中的困難和限制，並提出合理的解決方法。(-)
:-EN
--Ability to foresee the limitations and difficulties of research in the course of study is shown and corresponding improvement in the research method and tools is provided. (+)
--Limitations and difficulties of research in the course of study are clearly stated and reasonable solutions are provided. (+)
--Limitations and difficulties of research in the course of study are clearly stated but reasonable solutions are not provided. (+)
--Ability to foresee the limitations and difficulties of research in the course of study is not shown and corresponding improvement in the research method and tools is not provided. (-)
--Limitations and difficulties of research in the course of study are not clearly stated and reasonable solutions are not provided. (-)




EOT;
?>