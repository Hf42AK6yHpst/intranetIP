<?php

abstract class libSbaTaskHandin {
	
	protected $objDb;
	protected $eclass_db;
	protected $intranet_db;
	
	private $AnswerID;
	private $SchemeID;
	private $StageID;
	private $TaskID;
	private $UserID;
	private $Answer;
	private $QuestionType;
	private $DateInput;
	private $InputBy;
	private $DateModified;
	private $ModifyBy;
	
	public function __construct($parStageID, $parTaskID=0, $parAnswerId=null) {
		global $eclass_db,$intranet_db;
		
		$this->objDB = new libdb();
		
		$this->eclass_db   = $eclass_db;
		$this->intranet_db = $intranet_db;

		if($parAnswerId != null){
			$this->setAnswerID(intval($parAnswerId));
			$this->loadDataFromStorage();
		}
		
		$this->setStageID(intval($parStageID));

		$this->loadSchemeIDStageIDFromStorage();
			
		if($parTaskID != 0){
			$this->setTaskID(intval($parTaskID));
		}
    }
    
    abstract public function setStudentAnswer($val);
    
    abstract public function getDisplayAnswer();
    
    abstract public function getDisplayAnswerInput();
    
    abstract public function getExportAnswer();
    
    public function setAnswerID($val){
    	$this->AnswerID = $val;
    }
    
    public function getAnswerID(){
    	return $this->AnswerID;
    }
    
    public function setSchemeID($val){
    	$this->SchemeID = $val;
    }
    
    public function getSchemeID(){
    	return $this->SchemeID;
    }
    
    public function setStageID($val){
    	$this->StageID = $val;
    }
    
    public function getStageID(){
    	return $this->StageID;
    }
    
    public function setTaskID($val){
    	$this->TaskID = $val;
    }
    
    public function getTaskID(){
    	return $this->TaskID;
    }
    
    public function setUserID($val){
    	$this->UserID = $val;
    }
    
    public function getUserID(){
    	return $this->UserID;
    }

	/* Remark : This method is 'protected' as the answer should be handled by abstract public function setStudentAnswer() first */
    protected function setAnswer($val){
    	$this->Answer = $val;
    }
    
    /* Remark : This method is 'protected' as the answer should be handled by abstract public function getDisplayAnswer() first */
    protected function getAnswer(){
    	return $this->Answer;
    }
    
    /* Remark : This method is 'protected' as the QuestionType should be handled by the constructor of the child class */
    protected function setQuestionType($val){
    	$this->QuestionType = $val;
    }
    
    public function getQuestionType(){
    	return $this->QuestionType;
    }
    
    public function setDateInput($val){
    	$this->DateInput = $val;
    }
    
    public function getDateInput(){
    	return $this->DateInput;
    }
    
    public function setInputBy($val){
    	$this->InputBy = $val;
    }
    
    public function getInputBy(){
    	return $this->InputBy;
    }
    
    public function setDateModified($val){
    	$this->DateModified = $val;
    }
    
    public function getDateModified(){
    	return $this->DateModified;
    }
    
    public function setModifyBy($val){
    	$this->ModifyBy = $val;
    }
    
    public function getModifyBy(){
    	return $this->ModifyBy;
    }
    
    public function save(){
		
		if($this->getAnswerID() != null && intval($this->getAnswerID()) > 0){
			$recordID = $this->updateRecord();
		}
		else {
			$recordID = $this->newRecord();
		}

		$this->setAnswerID($recordID);
		
		return $recordID;
	}
	
	private function newRecord(){
		
		$DataArr = array();

		$DataArr["TaskID"]		 = $this->objDB->pack_value($this->getTaskID(), 	  "int");
		$DataArr["UserID"]		 = $this->objDB->pack_value($this->getUserID(), 	  "int");
		$DataArr["Answer"]		 = $this->objDB->pack_value($this->getAnswer(), 	  "str");
		$DataArr["QuestionType"] = $this->objDB->pack_value($this->getQuestionType(), "str");
		$DataArr["DateInput"]	 = $this->objDB->pack_value($this->getDateInput(), 	  "date");
		$DataArr["InputBy"]		 = $this->objDB->pack_value($this->getInputBy(), 	  "int");
		$DataArr["DateModified"] = $this->objDB->pack_value($this->getDateModified(), "date");
		$DataArr["ModifyBy"]	 = $this->objDB->pack_value($this->getModifyBy(), 	  "int");

		$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];


		$sql = 'Insert Into '.$this->intranet_db.'.IES_TASK_HANDIN ('.$fieldStr.') Values ('.$valueStr.')';

		$success = $this->objDB->db_db_query($sql);
								
		$recordID = $this->objDB->db_insert_id();

		$this->setAnswerID($recordID);
					
		$this->loadDataFromStorage();

		return $this->getAnswerID();

	}
	
	private function updateRecord(){
		
		$DataArr = array();

		$DataArr["TaskID"]		 = $this->objDB->pack_value($this->getTaskID(), 	  "int");
		$DataArr["UserID"]		 = $this->objDB->pack_value($this->getUserID(), 	  "int");
		$DataArr["Answer"]		 = $this->objDB->pack_value($this->getAnswer(), 	  "str");
		$DataArr["QuestionType"] = $this->objDB->pack_value($this->getQuestionType(), "str");
		$DataArr["DateModified"] = $this->objDB->pack_value($this->getDateModified(), "date");
		$DataArr["ModifyBy"]	 = $this->objDB->pack_value($this->getModifyBy(), 	  "int");
		
		$updateDetails = "";

		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);
		
		$sql = 'Update '.$this->intranet_db.'.IES_TASK_HANDIN Set '.$updateDetails.' Where AnswerID = '.$this->getAnswerID();
		
		$this->objDB->db_db_query($sql);
		
		return $this->getAnswerID();
	}
    
    private function loadDataFromStorage(){
		
		$sql = 'Select
					AnswerID , TaskID , UserID , Answer , QuestionType , DateInput , InputBy, DateModified, ModifyBy 
				From 
					'.$this->intranet_db.'.IES_TASK_HANDIN
				Where 
					AnswerID = '.$this->getAnswerID();

		$result = $this->objDB->returnResultSet($sql);

		
		if(is_array($result) && count($result) == 1){
			$result = current($result);

            $this->setTaskID($result['TaskID']);
            $this->setUserID($result['UserID']);
            $this->setAnswer($result['Answer']);
            $this->setQuestionType($result['QuestionType']);
			$this->setDateInput($result['DateInput']);
			$this->setInputBy($result['InputBy']);
			$this->setDateModified($result['DateModified']);
			$this->setModifyBy($result['ModifyBy']);
		}
		else{
			return null;
		}
	}
	
	private function loadSchemeIDStageIDFromStorage(){
		$sba_libSba = new libSba();		
		$result = $sba_libSba->getSchemeStructureByStageID($this->getStageID());

		if(is_array($result) && count($result)==1){
			$result = current($result);
			
			$this->setSchemeID($result['SchemeID']);
			$this->setStageID($result['StageID']);
		}
		else{
			return null;
		}

	}
}

?>