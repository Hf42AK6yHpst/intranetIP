<?php
# Remark: ==>Bookmark_1: Limiting the sync besides b5
//# using:  Max
class libies_xml_sync {
	function SyncScheme() {
		global $env,$intranet_root;
		#	Read XML
		$xmlFile = $intranet_root."/home/eLearning/ies/admin/settings/scheme.default.xml";
		$XMLString = get_file_content($xmlFile);
		$SimpleXMLElement = new SimpleXMLElement($XMLString);
		if (strtoupper($SimpleXMLElement[type]) == "IES") {
			#	Load Schemes Info type=ies
			$schemesDetail = libies::GetAllSchemesVersion();
//			$schemesDetail = $this->Fai_GetAllSchemesVersion();
			$this->writeLog("Total existing schemes: ".count($schemesDetail));

			$this->writeLog("XML Version: [{$SimpleXMLElement[version]}]");
			$this->writeLog("-------------------------------------------");
			$res = array();
			#	foreach schemes info						|__~~~--------- UPDATE SCHEME ---------~~~__|
			foreach($schemesDetail as $key => $schemeDetail) {
				$schemeLang = $schemeDetail["LANGUAGE"];
				$this->writeLog("this is the lang: $schemeLang");
				$this->writeLog("");
//				if ($schemeDetail["SCHEMEID"] <= 262) {
//					continue;
//				}
			#		Check the version the same with current schemes
			#		if the version are the same
				$this->writeLog("##########################################################################");
				$this->writeLog("Start Process -> [SchemeID: {$schemeDetail["SCHEMEID"]}; Current Version: {$schemeDetail["VERSION"]}] - {$schemeDetail["TITLE"]}");
				$this->writeLog("##########################################################################");
				if ($schemeDetail["VERSION"] == $SimpleXMLElement[version]) {
			#			abort update this scheme
					$this->writeLog("The version is already the newest!");
					$this->writeLog("");
			#		else
				} else {
			#			update this scheme
			#			foreach xml stages					|__~~~--------- UPDATE STAGE ---------~~~__|

					foreach($SimpleXMLElement->stage as $scheme_key => $stage) {
			#				Check the stage availability
						$this->writeLog("----> Processing [Stage Sequence: {$stage->sequence}; Title: {$stage->title}]");
						if (strtoupper($env)=="DEV") $stage[availability] = "ALL";	//	Control the update power of development
						switch (strtoupper($stage[availability])) {
			#				default OR if availiablity == "development"
							default:
							case "DEVELOPMENT":
								$this->writeLog("----> This is a developing stage and no update!");
								break;
			#				if availiablity == "all"
							case "ALL":
			#					update the stage
								$stage_title = $stage->title->$schemeLang;
								$stage_sequence = $stage->sequence;
								##############Bookmark_1
								//if ($stage_sequence>2 && $schemeLang!="b5") break 2;

								//for dev testing backup relate table in dev only
//								$isDev = true;
								if($isDev){
									$this->backUPSchemeTable();
								}
								$stageResult = $this->updateStage($schemeDetail["SCHEMEID"], $stage_sequence, $stage_title);
								$res["stage_update"][] = $stageResult;
								if (!$stageResult) {
									break 3;	#~ break the switch, next stage update and break from current scheme update
								}

								$stage_tasks = $stage->tasks[0];
			#					foreach xml tasks			|__~~~--------- UPDATE TASK ---------~~~__|
								foreach($stage_tasks as $stage_key => $task) {
			#						update the task
									$task_code = $task->code;
									$task_title = $task->title->$schemeLang;
									$task_sequence = $task->sequence;
									$task_description = $task->description->$schemeLang;
									$task_instantedit = $task->instantedit;
									$task_instantedit = ($task_instantedit == "") ? 1 : $task_instantedit;  //fai
									$this->writeLog("--------> Processing [Task Sequence: {$task->sequence}; Title: {$task->title}]");
									$taskResult = $this->updateTask($schemeDetail["SCHEMEID"],$stage_sequence,$task_sequence,$task_code,$task_title,$task_description,$task_instantedit);
									$res["task_update"][] = $taskResult;
									if (!$taskResult) {
										break 4;	#~ break next task update, the switch, next stage update and break from current scheme update
									}

									$task_steps = $task->steps[0];
			#						foreach xml steps		|__~~~--------- UPDATE STEP ---------~~~__|
									foreach($task_steps as $task_key => $step) {
			#							update the step (with handling extra fields like savetotask)
										$step_no = $step->no;
										$step_sequence = $step->sequence;
										$step_title = $step->title->$schemeLang;
										$step_savetotask = $step->savetotask;

										$this->writeLog("----------------> Processing [Step Sequence: {$step->sequence}; Title: {$step->title}]");
										$stepResult = $this->updateStep($schemeDetail["SCHEMEID"],$stage_sequence,$task_sequence,$step_sequence,$step_no,$step_title,$step_savetotask,$task_code);
										$res["step_update"][] = $stepResult;
										if (!$stepResult) {
											break 5;	#~ break next step update, next task update, the switch, next stage update and break from current scheme update
										}
									}
								}
								break;
						}

					}
			#			update scheme version
					$schemeResult = $this->updateScheme($schemeDetail["SCHEMEID"],$SimpleXMLElement[version]);
					$res["scheme_update"][] = $schemeResult;
					if (!$schemeResult) {
						break;	#~ break from next scheme update
					}
					$this->writeLog("--------------------------------------------------------------\n");
				}
			}
			$finalUpdateResult = true;
			if (count($res)>0) {
				foreach($res as $resKey => $resValueArray) {
					if (in_array(false,$resValueArray)) {
						$finalUpdateResult = false;
						break;
					}
				}
			} else {
				$finalUpdateResult = false;
			}


			##########################################################
			##	Final result return
			$returnResult = $res;	//	Return Updated IDs if success
			if ($finalUpdateResult) {
				$this->writeLog("[[[[[[[[[[Start verifying data]]]]]]]]]]");
			#		Verify Data
				$verifyResult = $this->verifyData($SimpleXMLElement, $res);
			#		if verified
				if ($verifyResult) {
			#			show success
					$this->writeLog("Verification Success.....");
			#		else
				} else {
			#			dump error
					$this->writeLog("Verification Failed.....Data mismatch during data verification....");
					$returnResult = false;
				}

				$this->writeLog("[[[[[[[[[[End verifying data]]]]]]]]]]");
			} else {
				$returnResult = false;
				$this->writeLog("Error occurred during the process of updating....Or all scheme are already the newest!");
			}
		} else {
			$this->writeLog("The type of the XML should be ies....");
			$returnResult = false;
		}
		//	Return Updated IDs if success
		return $returnResult;
		##########################################################
	}


	########################## All below are misc functions ############################
	function verifyData($ParSimpleXMLElement, $resultIds) {
		global $env,$libdb;
		$result = false;

		################################################
		##	Verify
		foreach($resultIds["scheme_update"] as $schemeKey => $schemeId) {
			$schemeLang = "b5";
			$sql = "SELECT LANGUAGE FROM IES_SCHEME WHERE SCHEMEID = '$schemeId'";
			$schemeLang = current($libdb->returnVector($sql));
			foreach($ParSimpleXMLElement->stage as $stageKey => $stage) {
				if (strtoupper($env)=="DEV") $stage[availability] = "ALL";	//	Control the update power of development
				switch (strtoupper($stage[availability])) {
					default:
					case "DEVELOPMENT":
						break;
					case "ALL":
						$stage_sequence = $stage->sequence;
						$stage_title = $stage->title->$schemeLang;
						##############Bookmark_1
						if ($stage_sequence>1 && $schemeLang!="b5") break 2;

						$stage_tasks = $stage->tasks[0];
						foreach($stage_tasks as $taskKey => $task) {
							$task_sequence = $task->sequence;
							$task_code = $task->code;
							$task_title = $task->title->$schemeLang;
							$task_description = $task->description->$schemeLang;
							$task_instantedit = $task->instantedit;
							$task_instantedit = ($task_instantedit == "") ? 1 : $task_instantedit;  //fai
							$task_steps = $task->steps[0];
							foreach($task_steps as $stepKey => $step) {
								$step_sequence = $step->sequence;
								$step_no = $step->no;
								$step_title = $step->title->$schemeLang;
								$step_savetotask = $step->savetotask;
								$sql = "
									SELECT
										STEP.STEPID
									FROM
										IES_STEP STEP
										INNER JOIN IES_TASK TASK
										ON STEP.TASKID = TASK.TASKID
										INNER JOIN IES_STAGE STAGE
										ON TASK.STAGEID = STAGE.STAGEID
										INNER JOIN IES_SCHEME SCHEME
										ON STAGE.SCHEMEID = SCHEME.SCHEMEID
									WHERE
										SCHEME.SCHEMEID = '$schemeId'
										AND STAGE.SEQUENCE = '$stage_sequence'
										AND TASK.SEQUENCE = '$task_sequence'
										AND STEP.SEQUENCE = '$step_sequence'
										AND STAGE.TITLE = '$stage_title'
										AND TASK.CODE = '$task_code' AND TASK.TITLE = '$task_title' AND TASK.DESCRIPTION = '$task_description' AND TASK.INSTANTEDIT = '$task_instantedit'
										AND STEP.STEPNO = '$step_no' AND STEP.TITLE = '$step_title' AND STEP.SAVETOTASK = '$step_savetotask'
									";
								$foundResult = current($libdb->returnVector($sql));
								if ($foundResult) {
									$this->writeLog("Matched[StepID: $foundResult; StepTitle: $step_title] - Scheme[ID: $schemeId] > Stage[Sequence: $stage_sequence] > Task[Sequence: $task_sequence] > Step[Sequence: $step_sequence]");
								} else {
									$this->writeLog("Missed[StepTitle: $step_title] - Scheme[ID: $schemeId] > Stage[Sequence: $stage_sequence] > Task[Sequence: $task_sequence] > Step[Sequence: $step_sequence]");
									$this->SQLLog($sql);;
								}
								$res[] = $foundResult;
							}
						}
						break;
				}
			}
	//		$res [] = false;

			if (isset($res) && in_array(false,$res)) {
				$result = false;
			} else {
				$result = true;
			}
		}
		################################################
		return $result;
	}
	function SQLLog($ParSQLDisplay) {

		$this->writeLog("*****************************************\n");
		$this->writeLog($ParSQLDisplay);
		if (mysql_error()) {
			$this->writeLog("");
			$this->writeLog("Update error:\n");
			$this->writeLog(mysql_error());
			$this->writeLog("");
		}
		$this->writeLog("*****************************************\n");
		$this->writeLog("");

	}
	function updateScheme($ParSchemeId, $ParSchemeVersion) {
		global $libdb, $intranet_db;
		$result = false;
		$sql = "UPDATE {$intranet_db}.IES_SCHEME SET VERSION = '$ParSchemeVersion' WHERE SCHEMEID = '$ParSchemeId'";
		$updateResult = $libdb->db_db_query($sql);
		if ($updateResult) {
			$result = $ParSchemeId;
		}
		$this->SQLLog($sql);
		return $result;
	}
	function getStageIdBySchemeIdAndStageSeq($ParSchemeId, $ParStageSequence) {
		global $libdb, $intranet_db;
		$sql = "SELECT STAGE.STAGEID FROM {$intranet_db}.IES_STAGE STAGE
				INNER JOIN IES_SCHEME SCHEME
					ON STAGE.SCHEMEID = SCHEME.SCHEMEID
				WHERE SCHEME.SCHEMEID = '$ParSchemeId' AND STAGE.SEQUENCE = '$ParStageSequence'";
		$targetStageId = current($libdb->returnVector($sql));
		return $targetStageId;
	}
	function getTaskIdBySchemeIdAndStageSeqAndTaskSeq($ParSchemeId, $ParStageSequence, $ParTaskSequence) {
		global $libdb, $intranet_db;
		$sql = "SELECT TASK.TASKID FROM {$intranet_db}.IES_TASK TASK
				INNER JOIN IES_STAGE STAGE
					ON TASK.STAGEID = STAGE.STAGEID
				INNER JOIN IES_SCHEME SCHEME
					ON STAGE.SCHEMEID = SCHEME.SCHEMEID
				WHERE SCHEME.SCHEMEID = '$ParSchemeId'
					AND STAGE.SEQUENCE = '$ParStageSequence'
					AND TASK.SEQUENCE = '$ParTaskSequence'";
		$targetTaskId = current($libdb->returnVector($sql));
	//	debug_r($sql);
		return $targetTaskId;
	}
	function getTaskIdBySchemeIdAndStageSeqAndTaskCode($ParSchemeId, $ParStageSequence, $ParTaskCode) {
		global $libdb, $intranet_db;
		$sql = "SELECT TASK.TASKID FROM {$intranet_db}.IES_TASK TASK
				INNER JOIN IES_STAGE STAGE
					ON TASK.STAGEID = STAGE.STAGEID
				INNER JOIN IES_SCHEME SCHEME
					ON STAGE.SCHEMEID = SCHEME.SCHEMEID
				WHERE SCHEME.SCHEMEID = '$ParSchemeId'
					AND STAGE.SEQUENCE = '$ParStageSequence'
					AND TASK.CODE = '$ParTaskCode'";
		$targetTaskId = current($libdb->returnVector($sql));
		return $targetTaskId;
	}
	function updateStage($ParSchemeId, $ParStageSequence, $ParStageTitle) {
		global $libdb, $intranet_db, $UserID;
		$result = false;

		$targetStageId = $this->getStageIdBySchemeIdAndStageSeq($ParSchemeId, $ParStageSequence);

		#####################################################################
		##	Just put the fields that you need to update over here
		$updateFieldsArray[] = "TITLE = '{$ParStageTitle}'";
		$updateFieldsString = implode(",",$updateFieldsArray);
		#####################################################################
	//	if ($targetStageId==32) {
	//		$updateFieldsString .= "....p";
	//	}
		if ($targetStageId) {
			$sql = "UPDATE {$intranet_db}.IES_STAGE
					SET $updateFieldsString
					WHERE STAGEID = $targetStageId";
			$updateResultId = $targetStageId;
		} else {
			$this->writeLog("NEW STAGE");
			$sql = "INSERT INTO {$intranet_db}.IES_STAGE
					SET $updateFieldsString,
					SCHEMEID = '$ParSchemeId',
					SEQUENCE = '$ParStageSequence',
					DATEINPUT = NOW()";
		}
		$result = $libdb->db_db_query($sql);
		$this->SQLLog($sql);
		###################################################
		##	Success Update/Insert return update id / insert id
		$result = $this->chooseIdToShow($result,$updateResultId);
		###################################################
		return $result;
	}
	function updateTask($ParSchemeId, $ParStageSequence, $ParTaskSequence, $ParTaskCode, $ParTaskTitle, $ParTaskDescription,$ParInstantEdit) {
		global $libdb, $intranet_db, $UserID;
		$result = false;
	//echo "----->>111";
		$targetTaskId = $this->getTaskIdBySchemeIdAndStageSeqAndTaskCode($ParSchemeId, $ParStageSequence, $ParTaskCode);
	//	debug_r($sql);
		#####################################################################
		##	Just put the fields that you need to update over here
	//	$updateFieldsArray[] = "CODE = '{$ParTaskCode}'";
		$updateFieldsArray[] = "SEQUENCE = $ParTaskSequence";
		$updateFieldsArray[] = "TITLE = '{$ParTaskTitle}'";
		$updateFieldsArray[] = "DESCRIPTION = '{$ParTaskDescription}'";
		$updateFieldsArray[] = "INSTANTEDIT = '{$ParInstantEdit}'";
		$updateFieldsString = implode(",",$updateFieldsArray);
		#####################################################################

		if ($targetTaskId) {
			$sql = "UPDATE {$intranet_db}.IES_TASK
					SET $updateFieldsString
					WHERE TASKID = '$targetTaskId'";
			$updateResultId = $targetTaskId;
		} else {
			$stageId = $this->getStageIdBySchemeIdAndStageSeq($ParSchemeId, $ParStageSequence);
			$this->writeLog("NEW TASK");
			/*
			$sql = "INSERT INTO {$intranet_db}.IES_TASK
					SET CODE = '{$ParTaskCode}',$updateFieldsString,
					STAGEID = $stageId,
					DATEINPUT = NOW(),
					INPUTBY = $UserID,
					MODIFYBY = $UserID,
					APPROVAL = 1";
			*/
			$sql = "INSERT INTO {$intranet_db}.IES_TASK
					SET CODE = '{$ParTaskCode}',$updateFieldsString,
					STAGEID = '$stageId',
					DATEINPUT = NOW(),
					APPROVAL = 1";

		}
		$result = $libdb->db_db_query($sql);
		$this->SQLLog($sql);
		###################################################
		##	Success Update/Insert return update id / insert id
		$result = $this->chooseIdToShow($result,$updateResultId);
		###################################################
		return $result;
	}
	function updateStep($ParSchemeId, $ParStageSequence, $ParTaskSequence, $ParStepSequence, $ParStepNo, $ParStepTitle, $ParStepSaveToTask,$ParTaskCode) {
		global $libdb, $intranet_db, $UserID;
		$result = false;

		$sql = "SELECT STEP.STEPID FROM {$intranet_db}.IES_STEP STEP
				INNER JOIN IES_TASK TASK
					ON STEP.TASKID = TASK.TASKID
				INNER JOIN IES_STAGE STAGE
					ON TASK.STAGEID = STAGE.STAGEID
				INNER JOIN IES_SCHEME SCHEME
					ON STAGE.SCHEMEID = SCHEME.SCHEMEID
				WHERE SCHEME.SCHEMEID = '$ParSchemeId'
					AND STAGE.SEQUENCE = '$ParStageSequence'
					AND TASK.CODE = '$ParTaskCode'
					AND STEP.SEQUENCE = '$ParStepSequence'";

		$targetStepId = current($libdb->returnVector($sql));

		#####################################################################
		##	Just put the fields that you need to update over here
		$updateFieldsArray[] = "STEPNO = '{$ParStepNo}'";
		$updateFieldsArray[] = "TITLE = '{$ParStepTitle}'";
		$updateFieldsArray[] = "SAVETOTASK = '{$ParStepSaveToTask}'";
		$updateFieldsString = implode(",",$updateFieldsArray);
		#####################################################################

		if ($targetStepId) {
			$sql = "UPDATE {$intranet_db}.IES_STEP
					SET $updateFieldsString
					WHERE STEPID = '$targetStepId'";
			$updateResultId = $targetStepId;
		} else {
	//		echo "---->$ParSchemeId,$ParStageSequence,$ParTaskSequence";
			$taskId = $this->getTaskIdBySchemeIdAndStageSeqAndTaskCode($ParSchemeId, $ParStageSequence, $ParTaskCode);
			$this->writeLog("NEW STEP");
			/*
			$sql = "INSERT INTO {$intranet_db}.IES_STEP
					SET $updateFieldsString,
					TASKID = $taskId,
					SEQUENCE = $ParStepSequence,
					DATEINPUT = NOW(),
					INPUTBY = $UserID,
					MODIFYBY = $UserID";
			*/
			$sql = "INSERT INTO {$intranet_db}.IES_STEP
					SET $updateFieldsString,
					TASKID = '$taskId',
					SEQUENCE = '$ParStepSequence',
					DATEINPUT = NOW()";
		}
		$result = $libdb->db_db_query($sql);
		$this->SQLLog($sql);
		###################################################
		##	Success Update/Insert return update id / insert id
		$result = $this->chooseIdToShow($result,$updateResultId);
		###################################################
		return $result;
	}
	function chooseIdToShow($ParResult, $ParUpdateResultId) {
		if ($ParResult){
			if(!isset($ParUpdateResultId)) {	//	success insert
				$ParResult = mysql_insert_id();
			} else {	//
				$ParResult = $ParUpdateResultId;
			}
		}
		return $ParResult;
	}
	function writeLog($ParContent, $ParShowToScreen=true) {
		global $intranet_root;
		$ParContent .= "\n";
		$today = date("Ymd");
		if ($ParShowToScreen) {
			echo $ParContent."<br/>";
		}

		$logPath = $intranet_root."/file/ies/log";
		if(!file_exists($logPath))
		{
		  mkdir($logPath, 0777, true);
		}
		$logFile = "$logPath/scheme_sync_log_$today.log";
		error_log($ParContent, 3, $logFile);
	}
	function Fai_GetAllSchemesVersion() {
		$libdb11 = new libdb();
		$sql = "SELECT SCHEMEID, TITLE, VERSION FROM {$intranet_db}.IES_SCHEME where SchemeID in(181,182,184,210)";
		$returnArray = $libdb11->returnArray($sql);
		return $returnArray;
	}
	function backUPSchemeTable()
	{
		global $intranet_db;
		$libdb11 = new libdb();
		$aryTable = array();
		$aryTable[] = "IES_SCHEME";
		$aryTable[] = "IES_STAGE";
		$aryTable[] = "IES_TASK";
		$aryTable[] = "IES_STEP";

		$_now = date("YmdGis");

		for($i = 0, $j = sizeof($aryTable); $i< $j; $i++)
		{
			$_table = $aryTable[$i];
			$_newTable = $_table."_bak_{$_now}";
			$sql = "CREATE TABLE  {$intranet_db}.{$_newTable} select * from {$intranet_db}.{$_table}";
			$libdb11->db_db_query($sql);
		}
	}
}
?>