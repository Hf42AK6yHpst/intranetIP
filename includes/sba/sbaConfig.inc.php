<?php
/** [Modification Log] Modifying By:
 * 
 * 2012-08-14 Mick:
 * New config for self evaluation questions
 */

//SBA config start here



$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO'] = 'SBA_IES';

$sba_cfg["IES_COMBO_ClassRoomType"] = 8; // IES with class room backup , room type = 5

$sba_cfg['SBA_EXPORT_STUDENTHANDIN_ROOT_PATH']= '/tmp/sba_export/';
$sba_cfg['SBA_STUDENTHANDIN_ROOT_PATH'] = '/file/sba/scheme/';
$sba_cfg['SBA_SURVEY_CHART_ROOT_PATH'] = '/file/sba/tmp_chart/';	// survey chart directory

$sba_cfg['DB_IES_TASK_InstantEdit']['InstantEdit'] = 1;
$sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'] = 2;
$sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit'] = 3; // Both Instant Edit and Step Edit is selected

$sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea'] = "textarea";

//$sba_cfg['DB_IES_STEP_QuestionType']['longquestion'] = "longquestion";

/* Important Note : key of array $sba_cfg['DB_IES_STEP_QuestionType'] should be the same with first element, 
 * e.g. For Question Type XXXXX : $sba_cfg['DB_IES_STEP_QuestionType']['XXXXX'] =array('XXXXX', "caption long quest"); */
$sba_cfg['DB_IES_STEP_QuestionType']['longQuestion'] = array('longQuestion', $Lang['SBA']['LongQuestion']);

$sba_cfg['DB_IES_STEP_QuestionType']['selfEvaluation'] = array('selfEvaluation', $Lang['SBA']['selfEvaluation']);
$sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'] = array('tablePlanner', $Lang['SBA']['tablePlanner']);

$sba_cfg['DB_IES_STEP_QuestionType']['powerConcept'] = array('powerConcept', $Lang['SBA']['ConceptMap']);

//SURVEY edit question
$sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'] = array('surveyEdit', $Lang['IES']['Survey']);
$sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'] = array('observeEdit', $Lang['IES']['Observe']);
$sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'] = array('interviewEdit', $Lang['IES']['Interview']);

/*
$sba_cfg['DB_IES_STEP_QuestionType']['surveyedit'] = "surveyEdit";
$sba_cfg['DB_IES_STEP_QuestionType']['observeedit'] = "observeEdit";
$sba_cfg['DB_IES_STEP_QuestionType']['interviewedit'] = "interviewEdit";
*/
//SURVEY analysis question
$sba_cfg['DB_IES_STEP_QuestionType']['surveyAnalysis'] = array('surveyAnalysis', $Lang['SBA']['Survey']['SurveyQuestionAnslysisTypeName']);
$sba_cfg['DB_IES_STEP_QuestionType']['observeAnalysis'] = array('observeAnalysis', $Lang['SBA']['Survey']['ObserverQuestionInputTypeName']);
$sba_cfg['DB_IES_STEP_QuestionType']['interviewAnalysis'] = array('interviewAnalysis', $Lang['SBA']['Survey']['InterviewQuestionInputTypeName']);


// SURVEY chart setting
$sba_cfg['SBA']['SurveyChart']['X_Legend_Min'] = 0.5;
$sba_cfg['SBA']['SurveyChart']['X_Legend_Max']['Normal'] = 2;
$sba_cfg['SBA']['SurveyChart']['X_Legend_Max']['Matrix'] = 3;
$sba_cfg['SBA']['SurveyChart']['ChartWidth'] = 400;
$sba_cfg['SBA']['SurveyChart']['ChartHeight'] = 400;
$sba_cfg['SBA']['SurveyChart']['CombineChartWidth'] = 400;
$sba_cfg['SBA']['SurveyChart']['CombineChartHeight'] = 400;





//$ies_cfg['bibi']['chibook'] = array('author' => $Lang['IES']['chibook']['author'],
//$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["editScheme"] = array($ies_cfg["ADMIN_SETTING_PATH"]."/scheme_edit.php", $Lang['IES']['MemberTitle'] );
$sba_cfg['SBA_SUPPORT_LANG'] = array('b5' =>$Lang['IES']['SchemeLang']["b5"],'en' =>$Lang['IES']['SchemeLang']["en"]);


//store the attribute for each SBA supported subject
//Please add one more element to "$sba_cfg['SBA']['SubjectProvided']" if one subject is added to SBA
$sba_cfg['SBA']['SubjectProvided']['IES'] = array(
										'lang' => array('b5','en'), //support/provided lang
									);

# Define the key of navigation ary which is supplied to function getNavigation() in libSba_ui.php 
$sba_cfg['SBA_NAVIGATION_ARY']['Caption'] = "Caption";
$sba_cfg['SBA_NAVIGATION_ARY']['Link'] = "Link";



/***********************************************/
/** since this SBA module copy from IES , so some of the config variable use the original one $ies_cfg
/***********************************************/


//GLODBAL varibale to all IES file , for the top menu with "eLearning" is active while under IES module
$CurrentPageArr['IES'] = 1;
$CurrentPageArr['SBA'] = 1;
/*
// temporary access right checking
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"]) {
	header("Location: /");
	exit;
}
*/

////////////////////////////////////////////////////////////////////////////////////////////////
// !!!! This variable $ies_cfg["IESSchemeSupportLang"] has been DEPRECATED (fai) !!!          //
// Please use $sba_cfg['SBA_SUPPORT_LANG']													  //
////////////////////////////////////////////////////////////////////////////////////////////////
$ies_cfg["IESSchemeSupportLang"] = array("b5","en");

$ies_cfg["IESClassRoomType"] = $sba_cfg["IES_COMBO_ClassRoomType"]; // IES with class room backup , equal to IES_COMBO_ClassRoomType
$ies_cfg["moduleCode"] = "ies";  // this value should be actually match with table INTRANET_MODULE.Code



define("IES_CFG_IES_MODULE_PATH", "/home/eLearning/ies");
$ies_cfg["ADMIN_SETTING_PATH"] = IES_CFG_IES_MODULE_PATH."/admin/settings";

$ies_cfg["DefaultInterface"] = "sba_admin_default.html";
$ies_cfg["DefaultFrontEndInterface"] = "ies_frontend_default.html";
$ies_cfg["PreviewFrontEndInterface"] = "ies_frontend_preview.html";
$ies_cfg["ThickboxFrontEndInterface"] = "sba_thickbox_default.html";

//$ies_cfg["DB_[TABLENAME]_[TABLEFIELD]"]["[MEANNING]"] = [VALUE];
$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"] = 1;
$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"] = 2;

$ies_cfg["DB_IES_TASK_STUDENT_DETAILS_Status"]["Suspend"] = 0;
$ies_cfg["DB_IES_TASK_STUDENT_DETAILS_Status"]["Open"] = 1;

define("STATUS_SUBMITTING", 1);
define("STATUS_SUBMITTED", 2);
define("STATUS_APPROVED", 3);
define("STATUS_TEACHER_READ", 4);
define("STATUS_REDO", 5);

######
# -	a function getBatchStatusLangByConfigNum() in libies can use this number to return a lang
# 	for DB_IES_STAGE_HANDIN_BATCH_BatchStatus and DB_IES_STAGE_STUDENT_BatchHandinStatus
######
$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"] = STATUS_SUBMITTING;
$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"] = STATUS_SUBMITTED;
$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["approved"] = STATUS_APPROVED;
$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["teacher_read"] = STATUS_TEACHER_READ;  // teacher has opened student batch listing but no action
$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"] = STATUS_REDO;

$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitting"] = STATUS_SUBMITTING;
$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitted"] = STATUS_SUBMITTED;
$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["approved"] = STATUS_APPROVED;
$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["teacher_read"] = STATUS_TEACHER_READ;  // teacher has opened student batch listing but no action
$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["redo"] = STATUS_REDO;

$ies_cfg["DB_IES_STAGE_STUDENT_Approval"]["approved"] = STATUS_APPROVED;

$ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"] = STATUS_APPROVED;

$ies_cfg["new_set_question_answer_sperator"] = "@~@";  //eg <tr>
$ies_cfg["question_answer_sperator"] = "#~#";  //eg <td>
$ies_cfg["comment_answer_sperator"] = "%~%";  //eg <th>
$ies_cfg["answer_within_question_sperator"] = ",";  
$ies_cfg["handin_status_identifier"] = "{HANDIN_STATUS}";  //eg <td>
$ies_cfg["html_open"] = "{[{";  //for bibi, to handle < in html. replace {[{ to < 
$ies_cfg["html_close"] = "}]}";

## IES_FAQ ##
# RecordStatus
$ies_cfg["faq_recordStatus_approved"] = 1;	# approved
$ies_cfg["faq_recordStatus_rejected"] = 0;	# rejected
# UserType
$ies_cfg["faq_userType_student"] = 1;		# question asked by student
$ies_cfg["faq_userType_teacher"] = 2;		# question asked by teacher
# AnswerStatus
$ies_cfg["faq_replied"] = 1;				# replied
$ies_cfg["faq_not_yet_replied"] = 2;		# not yet replied
# Answer Assign Type
$ies_cfg["faq_assignType_to_all"] = 0;				# answer to all schemes
$ies_cfg["faq_assignType_to_more_scheme"] = 1;		# answer to more schemes
$ies_cfg["faq_assignType_to_student"] = 2;			# answer to student

#########################
### GENERAL VARIABLES ###
#########################
$ies_cfg["recordStatus_approved"] = 1;	# approved
$ies_cfg["recordStatus_rejected"] = 0;	# rejected

$ies_cfg["recordStatus_publish"] = 1;		# Publish
$ies_cfg["recordStatus_notpublish"] = 0;	# Not Publish


## TAB CONFIG ##

//$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["editScheme"] = array($ies_cfg["ADMIN_SETTING_PATH"]."/scheme_edit.php", $Lang['IES']['MemberTitle'] );
$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["editScheme"] = array("index.php?mod=admin&task=settings_scheme_edit", $Lang['IES']['MemberTitle'] );
//$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["editFlow"] = array($ies_cfg["ADMIN_SETTING_PATH"]."/scheme_step_edit.php", $Lang['IES']['StepSettingTitle']);
$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["editSetUp"] = array("index.php?mod=admin&task=scheme_edit_details", $Lang['IES']['DetailSetupTitle']);

//$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["rubricSetUp"] = array($ies_cfg["ADMIN_SETTING_PATH"]."/scheme_rubric_details.php", $Lang['IES']['MarkingSettings']);
//if($plugin['IES_ENV'] == "DEV"){ open this module in 20110902
//$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["docExportSetUp"] = array($ies_cfg["ADMIN_SETTING_PATH"]."/scheme_export_section.php", $Lang['IES']['WordDocSettings']);

//$ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"]["schemeMoveStudent"] = array($ies_cfg["ADMIN_SETTING_PATH"]."/scheme_move_student.php", $Lang['IES']['SchemeMoveStudent']);

//}

## Marking Criteria ##
## Must in order ##

# Greatest number of tinyint(1), MarkCriteria field type of IES_MARKING
# MARKING_CRITERIA_OVERALL from 1 to 20 is reserved for other unknown default marking value
define("MARKING_CRITERIA_OVERALL", 1);

//TEMPORARY OPEN for marking (before rubric) , may remove later (FAI 20101019)
# Format array(DB_value, Display_title, Weight)
$ies_cfg["marking_criteria"]["progress"] = array(21, $Lang['IES']['Progress'], 50);
$ies_cfg["marking_criteria"]["result"] = array(22, $Lang['IES']['SchoolWork'], 50);
$ies_cfg["marking_criteria"]["overall_result"] = array(MARKING_CRITERIA_OVERALL, "Result");

$ies_cfg["DB_IES_MARKING_AssignmentType"]["scheme"] = 1;
$ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"] = 2;
$ies_cfg["DB_IES_MARKING_AssignmentType"]["task"] = 3;

$sba_cfg['MarkingCriteriaCompleteStatus']['Complete'] = 1;
$sba_cfg['MarkingCriteriaCompleteStatus']['NotComplete'] = 0;
$sba_cfg['MarkingCriteriaCompleteStatus']['NoStage'] = 2;

$sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore'] = 999999;


## Worksheet ##
$ies_cfg['HandinFileByTeacher'] = 1;
$ies_cfg['HandinFileByStudent'] = 2;
$ies_cfg['WorksheetFlag_New'] = 1;
$ies_cfg['WorksheetFlag_Edit'] = 2;

## Word Document Section Type ##
$ies_cfg['DocSectionType']['section'] = "SectionID";
$ies_cfg['DocSectionType']['task'] = "TaskID";

## classroom underneath ##
$ies_cfg['DB_course_RoomType'] = $ies_cfg["IESClassRoomType"];
$ies_cfg['DB_user_course_user_email']["rubric_manager"] = "ies_t01@rubric.manager";

## Start: Survey Email ##
$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"] = "SEND_STATUS";
$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["DONE_SURVEY"] = "DONE_SURVEY";
$ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"]]["NOT_SENT"] = 0;
$ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["SEND_STATUS"]]["SENT"] = 1;
$ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["DONE_SURVEY"]]["NOT_DONE"] = 0;
$ies_cfg['DB_IES_SURVEY_EMAIL_MailList'][$ies_cfg['DB_IES_SURVEY_EMAIL_MailList']["Element"]["DONE_SURVEY"]]["DONE"] = 1;

$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['NOT_SENT'] = 0;
$ies_cfg['DB_IES_SURVEY_EMAIL_Status__Lang'][$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['NOT_SENT']] = $Lang['IES']['NotSent'];
$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['SENT'] = 1;
$ies_cfg['DB_IES_SURVEY_EMAIL_Status__Lang'][$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['SENT']] = $Lang['IES']['Sent'];  
$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE'] = 2;
$ies_cfg['DB_IES_SURVEY_EMAIL_Status__Lang'][$ies_cfg['DB_IES_SURVEY_EMAIL_Status']['DONE']] = $Lang['IES']['Done'];


$ies_cfg["SenderEmail"] = $BroadlearningClientName;
$ies_cfg["SurveyEmailCSVHeader"] = array("Email");
$ies_cfg["CSV_Delimiter"] = "\t";

$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL'] = 1;
$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL'] = 2;
$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['Manual'] = 3;

$ies_cfg['DB_IES_DEFAULT_SURVEY_QUESTION']['B5'] = 1;
$ies_cfg['DB_IES_DEFAULT_SURVEY_QUESTION']['EN'] = 2;


//define("ies_cfg_ERR_MSG_TYPE_DIFF_FIELD_NUM",0);
//define("ies_cfg_ERR_MSG_TYPE_FIELD_MISS",1);
//
//$ies_cfg["CSV_ErrorMessage"][ies_cfg_ERR_MSG_TYPE_DIFF_FIELD_NUM] = "ABC";
//$ies_cfg["CSV_ErrorMessage"][ies_cfg_ERR_MSG_TYPE_FIELD_MISS] = "ABC";


$ies_cfg['SurveyEmail']['MailType']['iMail'] = 1;
$ies_cfg['SurveyEmail']['MailType']['GammaMail'] = 2;


## End: Survey Email ##


## Start: Questionnaire ##
$ies_cfg["Questionnaire"]["Delimiter"]["Question"] = "#QUE#";
$ies_cfg["Questionnaire"]["Delimiter"]["Answer"] = "#ANS#";
$ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"] = ",";
$ies_cfg["Questionnaire"]["Delimiter"]["Question_Title"] = "||";
$ies_cfg["Questionnaire"]["Delimiter"]["Question_Option"] = "#OPT#";

// Question Type Definition
// Format: [{Question code (to DB)}] = array({Question Type JS Library Name}, {Display Name})
$ies_cfg["Questionnaire"]["QuestionType"]["MC"] = 2;
$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"] = 3;
$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"] = 4;
$ies_cfg["Questionnaire"]["QuestionType"]["FillLong"] = 5;
$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"] = 6;
//$ies_cfg["Questionnaire"]["QuestionType"]["Table"] = 7;

/* should be deleted 2010825
$ies_cfg["Questionnaire"]["QuestionType"][$ies_cfg["Questionnaire"]["QuestionType"]["MC"]] = array("question_mc_single", "MC (Single)");
$ies_cfg["Questionnaire"]["QuestionType"][$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]] = array("question_mc_multi", "MC (Multiple)");
$ies_cfg["Questionnaire"]["QuestionType"][$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]] = array("question_fillin_short", "Fill-in (Short)");
$ies_cfg["Questionnaire"]["QuestionType"][$ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]] = array("question_fillin_long", "Fill-in (Long)");
$ies_cfg["Questionnaire"]["QuestionType"][$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]] = array("question_table_likert", "Likert Scale");
$ies_cfg["Questionnaire"]["QuestionType"][$ies_cfg["Questionnaire"]["QuestionType"]["Table"]] = array("question_table_input", "Table-like Input");
*/

$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["MC"]] = array("question_mc_single", $Lang['IES']['MC']);
$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]] = array("question_mc_multi", $Lang['IES']['MultiMC']);
$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]] = array("question_fillin_short", $Lang['IES']['FillShort']);
$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]] = array("question_fillin_long", $Lang['IES']['FillLong']);
$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]] = array("question_table_likert", $Lang['IES']['LikertScale']);
// $ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["Table"]] = array("question_table_input", "Table-like Input");

$ies_cfg["Questionnaire"]["ValidTypeForAnalyze"] = array(
														$ies_cfg["Questionnaire"]["QuestionType"]["MC"],
														$ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"],
														$ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]
														);

$ies_cfg["Questionnaire"]["SurveyType"]["Survey"] = array(1, $Lang['IES']['Survey'],'Survey');
$ies_cfg["Questionnaire"]["SurveyType"]["Interview"] = array(2, $Lang['IES']['Interview'],'Interview');
$ies_cfg["Questionnaire"]["SurveyType"]["Observe"] = array(3, $Lang['IES']['Observe'],'Observation');

$ies_cfg['Questionnaire']['SurveyAction']['EDIT']		= 'EDIT';
$ies_cfg['Questionnaire']['SurveyAction']['COLLECT']	= 'COLLECT';
$ies_cfg['Questionnaire']['SurveyAction']['ANALYSIS']	= 'ANALYSIS';

//QUESTIONNAIRE  ANSWER DISPLAY LOCATION ('F' --> FRONT (ALL STEP ANSWER OVERVIEW), 'I' ---> INSIDE A STEP, FROM A model.php);
$ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['FRONT']		= 'F';
$ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['INSIDE']		= 'I';

/*
$ies_cfg["Questionnaire"]["SurveyType"]["Survey"] = array('Survey', $Lang['IES']['Survey']);
$ies_cfg["Questionnaire"]["SurveyType"]["Interview"] = array('Interview', $Lang['IES']['Interview']);
$ies_cfg["Questionnaire"]["SurveyType"]["Observe"] = array('Observation', $Lang['IES']['Observe']);
*/

$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Invalid'] = 0;
$ies_cfg['DB_IES_SURVEY_ANSWER_status']['Valid'] = 1;

$ies_cfg['DB_IES_SURVEY_MAPPING_XMAPPING']['Survey_Comment'] = 'S_C';

$ies_cfg['Questionnaire']['isDefaultQuestion'] = 1;  //those question is created by system (default build in once installed IES), not by user
$ies_cfg['Questionnaire']['isNotDefaultQuestion'] = 0; //user self created question

## End: Questionnaire ##

/*******************************
*        old bibi HERE!        *
********************************/
 	     
$ies_cfg['bibi']['chibook'] = array('author' => $Lang['IES']['chibook']['author'],
									'translator' => $Lang['IES']['chibook']['translator'],
									'editor' => $Lang['IES']['chibook']['editor'],
									'year' => $Lang['IES']['chibook']['year'],
									'bkname' => $Lang['IES']['chibook']['bkname'],
									'articlename' => $Lang['IES']['chibook']['articlename'],
									'edition' => $Lang['IES']['chibook']['edition'],
									'location' => $Lang['IES']['chibook']['location'],
									'pub' => $Lang['IES']['chibook']['pub'],
									'remark' => $Lang['IES']['chibook']['remark']
									);
$ies_cfg['bibi']['chinew'] = array('author' => $Lang['IES']['chinew']['author'],
									'date' => $Lang['IES']['chinew']['date'], 
									'bkname' => $Lang['IES']['chinew']['bkname'],
									'articlename' => $Lang['IES']['chinew']['articlename'],
									'fnum' => $Lang['IES']['chinew']['fnum'],
									'pnum' => $Lang['IES']['chinew']['pnum'],
									'remark' => $Lang['IES']['chinew']['remark']
									);
$ies_cfg['bibi']['chinet'] = array('author' => $Lang['IES']['chinet']['author'],
									'year' => $Lang['IES']['chinet']['year'],
									'bkname' => $Lang['IES']['chinet']['bkname'],
									'articlename' => $Lang['IES']['chinet']['articlename'],
									'date' => $Lang['IES']['chinet']['date'],
									'website' => $Lang['IES']['chinet']['website'],
									'remark' => $Lang['IES']['chinet']['remark']
									);
$ies_cfg['bibi']['engbook'] = array('author' => $Lang['IES']['engbook']['author'],
									'year' => $Lang['IES']['engbook']['year'],
									'articlename' => $Lang['IES']['engbook']['articlename'],
									'editor' => $Lang['IES']['engbook']['editor'],
									'title' => $Lang['IES']['engbook']['title'],
									'edition' => $Lang['IES']['engbook']['edition'],
									'pnum' => $Lang['IES']['engbook']['pnum'],
									'location' => $Lang['IES']['engbook']['location'],
									'pub' => $Lang['IES']['engbook']['pub'],
									'remark' => $Lang['IES']['engbook']['remark']
									);
$ies_cfg['bibi']['engnew'] = array('author' => $Lang['IES']['engnew']['author'],
									'year' => $Lang['IES']['engnew']['year'],
									'articlename' => $Lang['IES']['engnew']['articlename'],
									'journal' => $Lang['IES']['engnew']['journal'],
									'vnum' => $Lang['IES']['engnew']['vnum'],
									'inum' => $Lang['IES']['engnew']['inum'],
									'pnum' => $Lang['IES']['engnew']['pnum'],
									'remark' => $Lang['IES']['engnew']['remark']
									);
$ies_cfg['bibi']['engnet'] = array('author' => $Lang['IES']['engnet']['author'],
									'date' => $Lang['IES']['engnet']['date'],
									'title' => $Lang['IES']['engnet']['title'],
									'bdate' => $Lang['IES']['engnet']['bdate'],
									'website' => $Lang['IES']['engnet']['website'],
									'remark' => $Lang['IES']['engnet']['remark']
									);
$ies_cfg['bibi']['manual'] =array('manual' => $Lang['IES']['manual']['manual']);


//$ies_cfg['bibi']['chi'] = array('chibook', 'chinew', 'chinet', 'manual');							
//$ies_cfg['bibi']['eng'] = array('engbook', 'engnew','engnet');

/***********old bibi END *************/



/*******************************
*        new bibi HERE!        *
********************************/

$ies_cfg['bibi']['chibook_new'] = array('authorChi' => $Lang['IES']['chibook_new']['authorChi'],
									'authorEng' => $Lang['IES']['chibook_new']['authorEng'],
									'translator' => $Lang['IES']['chibook_new']['translator'],
									'editor' => $Lang['IES']['chibook_new']['editor'],
									'year' => $Lang['IES']['chibook_new']['year'],
									'bkname' => $Lang['IES']['chibook_new']['bkname'],
									'articlename' => $Lang['IES']['chibook_new']['articlename'],
									'edition' => $Lang['IES']['chibook_new']['edition'],
									'pnum' => $Lang['IES']['chibook_new']['pnum'],
									'location' => $Lang['IES']['chibook_new']['location'],
									'pub' => $Lang['IES']['chibook_new']['pub'],
									'remark' => $Lang['IES']['chibook_new']['remark'],
									'IsEditedBook' => $Lang['IES']['chibook_new']['IsEditedBook'],
									'Yes' => $Lang['IES']['chi']['Yes'],
									'No' => $Lang['IES']['chi']['No']
									);
$ies_cfg['bibi']['chireport_new'] = array('author' => $Lang['IES']['chireport_new']['author'],
									'organization' => $Lang['IES']['chireport_new']['organization'],
									'year' => $Lang['IES']['chireport_new']['year'],
									'month' => $Lang['IES']['chireport_new']['month'],
									'day' => $Lang['IES']['chireport_new']['day'],
									'title' => $Lang['IES']['chireport_new']['title'],
									'remark' => $Lang['IES']['chireport_new']['remark'],
									);
									
$ies_cfg['bibi']['chijournals_new'] = array('author' => $Lang['IES']['chijournals_new']['author'],
									'year' => $Lang['IES']['chijournals_new']['year'], 
									'bkname' => $Lang['IES']['chijournals_new']['bkname'],
									'articlename' => $Lang['IES']['chijournals_new']['articlename'],
									'gnum' => $Lang['IES']['chijournals_new']['gnum'],
									'fnum' => $Lang['IES']['chijournals_new']['fnum'],
									'pnum' => $Lang['IES']['chijournals_new']['pnum'],
									'remark' => $Lang['IES']['chijournals_new']['remark']
									);
									
$ies_cfg['bibi']['chimagazine_new'] = array('author' => $Lang['IES']['chimagazine_new']['author'],
									'year' => $Lang['IES']['chimagazine_new']['year'],
									'month' => $Lang['IES']['chimagazine_new']['month'], 
								    'day' => $Lang['IES']['chimagazine_new']['day'], 
									'bkname' => $Lang['IES']['chimagazine_new']['bkname'],
									'articlename' => $Lang['IES']['chimagazine_new']['articlename'],
									'fnum' => $Lang['IES']['chimagazine_new']['fnum'],
									'pnum' => $Lang['IES']['chimagazine_new']['pnum'],
									'remark' => $Lang['IES']['chimagazine_new']['remark']
									);
$ies_cfg['bibi']['chinew_new'] = array('author' => $Lang['IES']['chinew_new']['author'],
									'date' => $Lang['IES']['chinew_new']['date'], 
									'bkname' => $Lang['IES']['chinew_new']['bkname'],
									'articlename' => $Lang['IES']['chinew_new']['articlename'],
									'pnum' => $Lang['IES']['chinew_new']['pnum'],
									'remark' => $Lang['IES']['chinew_new']['remark']
									);
$ies_cfg['bibi']['chinet_new'] = array('author' => $Lang['IES']['chinet_new']['author'],
									'year' => $Lang['IES']['chinet_new']['year'],
									'organization' => $Lang['IES']['chinet_new']['organization'],
									'articlename' => $Lang['IES']['chinet_new']['articlename'],
									'date' => $Lang['IES']['chinet_new']['date'],
									'website' => $Lang['IES']['chinet_new']['website'],
									'remark' => $Lang['IES']['chinet_new']['remark']
									);
$ies_cfg['bibi']['engbook_new'] = array('bookAuthor' => $Lang['IES']['engbook_new']['bookAuthor'],
									'bookTitle' => $Lang['IES']['engbook_new']['bookTitle'],
									'chapterAuthor' => $Lang['IES']['engbook_new']['chapterAuthor'],
									'chapterTitle' => $Lang['IES']['engbook_new']['chapterTitle'],
									'bookEditor' => $Lang['IES']['engbook_new']['bookEditor'],
									'year' => $Lang['IES']['engbook_new']['year'],								
									'titleWork' => $Lang['IES']['engbook_new']['titleWork'],
									'edition' => $Lang['IES']['engbook_new']['edition'],
									'pnum' => $Lang['IES']['engbook_new']['pnum'],
									'location' => $Lang['IES']['engbook_new']['location'],
									'pub' => $Lang['IES']['engbook_new']['pub'],
									'remark' => $Lang['IES']['engbook_new']['remark'],
									'IsEditedBook' => $Lang['IES']['engbook_new']['IsEditedBook'],
									'Yes' => $Lang['IES']['eng']['Yes'],
									'No' => $Lang['IES']['eng']['No']
									);
$ies_cfg['bibi']['engreport_new'] = array('author' => $Lang['IES']['engreport_new']['author'],
									'organization' => $Lang['IES']['engreport_new']['organization'],
									'year' => $Lang['IES']['engreport_new']['year'],
									'title' => $Lang['IES']['engreport_new']['title'],
									'location' => $Lang['IES']['engreport_new']['location'],
									'pub' => $Lang['IES']['engreport_new']['pub'],
									'remark' => $Lang['IES']['engreport_new']['remark']
								
									);
$ies_cfg['bibi']['engjournals_new'] = array('author' => $Lang['IES']['engjournals_new']['author'],
									'year' => $Lang['IES']['engjournals_new']['year'],
									'articlename' => $Lang['IES']['engjournals_new']['articlename'],
									'journal' => $Lang['IES']['engjournals_new']['journal'],
									'vnum' => $Lang['IES']['engjournals_new']['vnum'],
									'inum' => $Lang['IES']['engjournals_new']['inum'],
									'pnum' => $Lang['IES']['engjournals_new']['pnum'],
									'remark' => $Lang['IES']['engjournals_new']['remark']
									);
									
$ies_cfg['bibi']['engmagazine_new'] = array('author' => $Lang['IES']['engmagazine_new']['author'],
									'year' => $Lang['IES']['engmagazine_new']['year'],
									'month' => $Lang['IES']['engmagazine_new']['month'],
									'day' => $Lang['IES']['engmagazine_new']['day'],
									'articlename' => $Lang['IES']['engmagazine_new']['articlename'],
									'magazine' => $Lang['IES']['engmagazine_new']['magazine'],
									'vnum' => $Lang['IES']['engmagazine_new']['vnum'],
									'inum' => $Lang['IES']['engmagazine_new']['inum'],
									'pnum' => $Lang['IES']['engmagazine_new']['pnum'],
									'remark' => $Lang['IES']['engmagazine_new']['remark'],
									); 
$ies_cfg['bibi']['engnew_new'] = array('author' => $Lang['IES']['engnew_new']['author'],
									'date' => $Lang['IES']['engnew_new']['date'],
									'articlename' => $Lang['IES']['engnew_new']['articlename'],
									'newspaper' => $Lang['IES']['engnew_new']['newspaper'],
									'pnum' => $Lang['IES']['engnew_new']['pnum'],
									'remark' => $Lang['IES']['engnew_new']['remark'],
									);
$ies_cfg['bibi']['engnet_new'] = array('author' => $Lang['IES']['engnet_new']['author'],
									'organization' => $Lang['IES']['engreport_new']['organization'],
									'year' => $Lang['IES']['engnet_new']['year'],
									'month' => $Lang['IES']['engnet_new']['month'],
									'day' => $Lang['IES']['engnet_new']['day'],
									'title' => $Lang['IES']['engnet_new']['title'],
									'bdate' => $Lang['IES']['engnet_new']['bdate'],
									'website' => $Lang['IES']['engnet_new']['website'],
									'remark' => $Lang['IES']['engnet_new']['remark']
									);
$ies_cfg['bibi']['engother_new'] = array('other' => $Lang['IES']['engother_new']['other']);
$ies_cfg['bibi']['chiother_new'] = array('other' => $Lang['IES']['chiother_new']['other']);


$ies_cfg['bibi']['manual'] =array('manual' => $Lang['IES']['manual']['manual']);


//it is for adding the old data
//$ies_cfg['bibi']['chi'] = array('chibook', 'chinew', 'chinet', 'manual','chibook_new', 'chireport_new', 'chijournals_new', 'chimagazine_new','chinew_new','chinet_new','chiother_new');							
//$ies_cfg['bibi']['eng'] = array('engbook', 'engnew','engnet','engbook_new', 'engreport_new','engjournals_new','engmagazine_new','engnew_new','engnet_new','engother_new');


$ies_cfg['bibi']['chi'] = array('oldDataChi','chibook_new', 'chireport_new', 'chijournals_new', 'chimagazine_new','chinew_new','chinet_new','chiother_new');							
$ies_cfg['bibi']['eng'] = array('oldDataEng','engbook_new', 'engreport_new','engjournals_new','engmagazine_new','engnew_new','engnet_new','engother_new');


/***********new bibi END *************/

$ies_cfg['selfEvaluation']['positiveQuestions']=array(1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,0);


/////////////////////////////////////////
//should not have any new line below this
/////////////////////////////////////////
?>