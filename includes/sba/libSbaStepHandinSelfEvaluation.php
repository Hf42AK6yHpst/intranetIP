<?php
 /**
 * 2012-08-22 Mick
 *	Implement getExportDisplay()
 *
 * */
class libSbaStepHandinSelfEvaluation extends libSbaStepHandin{
	
	private $QuestionTitle;
	private $numOfAnswers;
	private $numOfQuestions;
	
	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg, $Lang;
		
		parent::__construct($parTaskID, $parStepID, $parAnswerId);
		
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['selfEvaluation'][0]);
		$this->setQuestionTitle($sba_cfg['DB_IES_STEP_QuestionType']['selfEvaluation'][1]);
		$this->setNumOfAnswers(4);
		$this->setNumOfQuestions(sizeof($Lang['IES']['selfEvaluation']["choice"]));
		
	}
	
	public function getCaption(){
		global $sba_cfg;
		
		return $sba_cfg['DB_IES_STEP_QuestionType']['selfEvaluation'][1];
	}
	
	/* This function should receive r_stdAnswer[num] defined in getQuestionDisplay() */
	public function setStudentAnswer($stdAnswer_ary){
		
		global $ies_cfg;
		
		$this->setAnswer(implode($ies_cfg["answer_within_question_sperator"],array_keys($stdAnswer_ary)).$ies_cfg["question_answer_sperator"].implode($ies_cfg["answer_within_question_sperator"],$stdAnswer_ary));
		
		if (sizeof($stdAnswer_ary)==$this->numOfQuestions){
			$mark=0;
			foreach ($stdAnswer_ary as $num=>$value){
				$mark = ($ies_cfg['selfEvaluation']['positiveQuestions'][$num-1]==1)? $mark+$value : $mark-$value;
			}
			$this->setAnswerActualValue($mark);

		}
		
	}
	
	public function getDisplayAnswer(){
		
		return $this->generateComment();
	}
	
	public function getDisplayAnswerForMarking(){

		return $this->generateComment();

	}
	
	public function getExportDisplay(){
		
		$title = '<h3>'.$this->getQuestionTitle().'</h3>';
		return array($this->getQuestionType()=>$title.$this->generateEvaluationForm());
		
	}
	
	public function getQuestionDisplay(){
		global $sba_allowStudentSubmitHandin, $Lang;
		$_task_id 	= $this->getTaskID()?   $this->getTaskID()   : 0;
		$_step_id 	= $this->getStepID()?   $this->getStepID()   : 0;
		$_answer_id = $this->getAnswerID()? $this->getAnswerID() : 0;
		
		$from_reset = $form_submit = 'alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')';
		if($sba_allowStudentSubmitHandin){
			$form_submit= "step_stdAnswer_handin_form_submit({$_task_id}, {$_step_id}, {$_answer_id})";
			$from_reset= "step_stdAnswer_handin_form_reset({$_step_id})";
			$editable=true;
		}else{
			$editable=false;
		}


		$QuestionHTML = "<h4 class=\"icon_stu_edit\" style=\"margin-top:30px\">
				<span class=\"task_instruction_text\">".$this->getQuestionTitle()."</span>
				</h4>
				".$this->generateEvaluationForm($editable)."
				<div style=\"text-align:center; margin-top:10px\">
					<input type=\"button\" value=\"".$Lang['IES']['Save2']."\" onclick=\"{$form_submit}\" onmouseout=\"this.className=&quot;formbutton&quot;\" onmouseover=\"this.className=&quot;formbuttonon&quot;\" class=\"formbutton\" name=\"submit2\">  
					<input type=\"button\" value=\"".$Lang['IES']['Reset']."\" onclick=\"{$from_reset}\" onmouseout=\"this.className=&quot;formbutton&quot;\" onmouseover=\"this.className=&quot;formbuttonon&quot;\" class=\"formbutton\">  
				
				</div>
				<div class=\"edit_bottom\"><span>".$Lang['IES']['LastModifiedDate']." : ".($this->getDateModified()? $this->getDateModified():'--')."</span></div>";
		return $QuestionHTML;
	}
	
	public function getEditQuestionDisplay(){
		
		global $sba_cfg;
				
		$QuestionHTML = "
                                <script>function step_tool_validate_fail(){return false;}</script>
				<div class=\"IES_task_form\">
				<h4 class=\"icon_stu_edit\"><strong>".$this->getQuestionTitle()."</strong></h4>
				".$this->generateEvaluationForm()."
				<input type=\"hidden\" id=\"r_step_QuestionType\" name=\"r_step_QuestionType\" value=\"".($this->getQuestionType())."\"></div>";
		return $QuestionHTML;
	}
	
	private function generateComment(){
		global $Lang;
		$mark=$this->getAnswerActualValue();
	
		if (!$mark){
			return "";
		}else if ($mark<30){
			return $Lang['IES']['selfEvaluation']["comment"]["1"];
		}else if ($mark<40){
			return $Lang['IES']['selfEvaluation']["comment"]["2"];
		}else if ($mark<50){
			return $Lang['IES']['selfEvaluation']["comment"]["3"];
		}else{
			return $Lang['IES']['selfEvaluation']["comment"]["4"];
		} 
	}
	private function generateEvaluationForm($editable=false){
		global $Lang,$ies_cfg;
		
		$disabled=($editable)? "" : "disabled";
		
		list($questions, $answers)=explode($ies_cfg["question_answer_sperator"],$this->getAnswer());
		$answer_array=array_combine(explode($ies_cfg["answer_within_question_sperator"],$questions),explode($ies_cfg["answer_within_question_sperator"],$answers));
		//build an array which has keys of answered questions and values of answers
		
		$comment=$this->generateComment();

		$html = "<table class=\"about_stage estimate_table\">
		<tr class=\"question\"><td>&nbsp;</td><td>&nbsp;</td><td class=\"option\">".$Lang['IES']['selfEvaluation']["string"]["12"]
		."</td><td class=\"option\">&nbsp;</td><td class=\"option\">&nbsp;</td><td class=\"option\">".$Lang['IES']['selfEvaluation']["string"]["13"]
		."</td></tr>";
		
		foreach ($Lang['IES']['selfEvaluation']["choice"] as $num=>$choice)
		{
			
			$html .= "<tr class=\"question\">";
			$html .= "<td>$num</td>";
			$html .= "<td>$choice</td>";
			
			for($j = 1; $j <= $this->numOfAnswers;$j++)
			{
				$checked= ($answer_array[$num]==$j)? 'checked' : '';		
				$html .= "<td class=\"option\"><input name=\"r_stdAnswer[$num]\" type=\"radio\" value=\"".$j."\" $disabled $checked/></td>";
			}
			$html .= "</tr>\n";
		}
		$html .= "<tr class=\"question\"><td>&nbsp;</td><td>$comment</td><td>&nbsp;</td><td>&nbsp;<td>&nbsp;</td><td>&nbsp;</td>&nbsp;</td></tr></table>";
		
		return $html;
	}
	
	public function getQuestionStr(){
		$QuestionStr = $this->getQuestionTitle();
		
		return $QuestionStr;
	}
	
	private function setNumOfAnswers($val){
		$this->numOfAnswers=$val;
	}
	
	private function setNumOfQuestions($val){
		$this->numOfQuestions=$val;
	}
	private function setQuestionTitle($val){
		$this->QuestionTitle=$val;
	} 
	
	private function getQuestionTitle(){
		return $this->QuestionTitle;
	}
	
}

?>