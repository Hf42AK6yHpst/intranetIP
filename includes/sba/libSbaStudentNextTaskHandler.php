<?php
class libSbaStudentNextTaskHandler{
	private static $instance;
	private static $task_student_ary;
	
	private function __construct($par_user_id){
		global $ies_cfg;
		
		$db_obj = new libdb();
		
		$sql = "SELECT
					TaskID
				FROM
					IES_TASK_STUDENT WHERE UserID = $par_user_id AND
					Approved = ".$ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"];
		
		self::$task_student_ary = $db_obj->returnVector($sql);
	}
	
	public static function getInstance($par_user_id){
		if(!is_object(self::$instance)){
			self::$instance = new libSbaStudentNextTaskHandler($par_user_id);
		}
		
		return self::$instance;
	}
	
	public function isTaskOpen(task $TaskObj){
		global $ies_cfg;
		
		$result = false;
		
		$result = !$TaskObj->getApproval() || (is_array(self::$task_student_ary) && count(self::$task_student_ary)>0 && in_array($TaskObj->getTaskID(), self::$task_student_ary));
		
		$result = $TaskObj->getEnable()? $result : false;
		
		return $result;
	}
}

?>