<?
/*******************************************
*	Editing by Paul
*	Modification Log:
* 		2016-08-15 (Paul)
* 			- hide Mr Yeung schema (IES scheme 2017) by Sep 1
*		2016-07-18 Paul
*			- modified function GET_SCHEME_ARR() to display public scheme for default only
*		2015-06-24 Siuwan
*			- modified function GetDefaultMarkingCriteria() to get chinese and english title
*/
include_once("$intranet_root/includes/liblog.php");

class libSba extends libdb {

	private $Module;

	function libSba() {
		global $sba_cfg;
		$this->libdb();
		$this->Module = $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO'];
	}

	public function GET_MODULE_OBJ_ARR(){

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $CurrentPage, $CurrentPageArr;
		global $top_menu_mode,$ies_lang, $Lang,$sba_cfg;
		global $plugin, $UserID;

		// Logo link
		$MODULE_OBJ['root_path'] = "/home/eLearning/sba/";

		// Current Page Information init
		$PageManagement = 0;
		$PageSettings = 0;

		switch ($CurrentPage) {
			case "StudentProgress":
				$PageManagement = 1;
				$PageManagement_StudentProgress = 1;
				break;
			case "FAQ":
				$PageManagement = 1;
				$PageManagement_FAQ = 1;
				break;
			case "Worksheet":
				$PageManagement = 1;
				$PageManagement_Worksheet = 1;
				break;
			case "SchemeSettings":
				$PageSettings = 1;
				$PageSettings_SchemeSettings = 1;
				break;
			case "DefaultSurveyQuestion":
        $PageSettings = 1;
				$PageSettings_DefaultSurveyQuestion = 1;
				break;
			case "CommentBank":
				$PageSettings = 1;
				$PageSettings_CommentBank = 1;
				break;
			case "UploadSettings":
				$PageSettings = 1;
				$PageSettings_UploadSettings = 1;
				break;
			case "RubricSettings":
				$PageSettings = 1;
				$PageSettings_RubricSettings = 1;
				break;
		}

		// Menu information
		//$MenuArr["Management"] = array($ies_lang['Management'], $PATH_WRT_ROOT."home/eLearning/ies/index.php", $PageStatistics);

		$MenuArr["Management"] = array($ies_lang['Management'], "", $PageManagement);
//		$MenuArr["Management"]["Child"]["StudentProgress"] = array($ies_lang['StudentProgress'], $PATH_WRT_ROOT."home/eLearning/ies/admin/index.php", $PageManagement_StudentProgress);


		if($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"] || $_SESSION["SSV_PRIVILEGE"]["SBA"]["isTeacher"]) {
			$MenuArr["Management"]["Child"]["StudentProgress"] = array($ies_lang['StudentProgress'], $PATH_WRT_ROOT."home/eLearning/sba/?clearCoo=1&mod=admin&task=schemeProgress", $PageManagement_StudentProgress);
			$MenuArr["Management"]["Child"]["Worksheet"] = array($Lang['IES']['Worksheet'], $PATH_WRT_ROOT."home/eLearning/sba/?clearCoo=1&mod=worksheet&task=index", $PageManagement_Worksheet);
			if(strtoupper($plugin['IES_ENV']) == "DEV"){
				$MenuArr["Management"]["Child"]["FAQ"] = array($Lang['IES']['FAQ'], $PATH_WRT_ROOT."home/eLearning/sba/?mod=faq&task=index", $PageManagement_FAQ);
			}
		}

		if($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"]) {
			$MenuArr["Settings"] = array($ies_lang['Settings'], "", $PageSettings);
			$MenuArr["Settings"]["Child"]["SchemeSettings"] = array($Lang['IES']['Scheme'], $PATH_WRT_ROOT."home/eLearning/sba/index.php?mod=admin&task=settings_scheme_index", $PageSettings_SchemeSettings);
			$MenuArr["Settings"]["Child"]["CommentBank"] = array($Lang['IES']['CommentBank'], $PATH_WRT_ROOT."home/eLearning/sba/index.php?mod=commentbank&task=listCategory", $PageSettings_CommentBank);

			//ModuleCode='sba_ies'
			$moduleCode = $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO'];

			$moduleCode_encode=getEncryptedText($moduleCode);

			$MenuArr["Settings"]["Child"]["StudentLicense"] = array($ies_lang['StudentLicense'], "javascript:newWindow('/home/moduleLicense/module_license.php?ModuleCode_e={$moduleCode_encode}','31')", $PageManagement_StudentLicense);


//			$MenuArr["Settings"]["Child"]["CommentBank"] = array($Lang['IES']['CommentBank'], $PATH_WRT_ROOT."home/eLearning/ies/admin/management/commentbank/", $PageSettings_CommentBank);
			$MenuArr["Settings"]["Child"]["UploadSettings"] = array($Lang['IES']['UploadSettings'], "javascript: var uploadSettingWin = newWindow('".$PATH_WRT_ROOT."ipfiles/iportal_file_settings.php?moduleCode=".$moduleCode."','6');", $PageSettings_UploadSettings);

			//for tempraory "DEV" 20100910
//
        $MenuArr["Settings"]["Child"]["DefaultSurveyQuestion"] = array($Lang['IES']['DefaultSurveyQuestion'], $PATH_WRT_ROOT."home/eLearning/sba/index.php?mod=survey&task=setQuestion", $PageSettings_DefaultSurveyQuestion);
//		if(strtoupper($plugin['IES_ENV']) == "DEV"){
				$MenuArr["Settings"]["Child"]["Rubric"] = array($Lang['IES']['RubricSetting'], "javascript: var uploadSettingWin = newWindow('/home/eLearning/sba/LoginEclass.php','31');", $PageSettings_RubricSettings);
//			}
		}
        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass Reading Scheme";'."\n";
        $js.= '</script>'."\n";

		// module information
		$MODULE_OBJ['title'] = $ies_lang['IES_TITLE'].$js;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_IES_setting.png";
		$MODULE_OBJ['menu'] = $MenuArr;

		return $MODULE_OBJ;
	}

	public function isSBA_Teacher($thisUserID)
	{
		/* the field "SchemeType" has the value "IES_COMBO", "IES_HISTORY", "IES_ENGLISH", etc... so currently return all non-IES Schemes
		*/

		global $plugin;

		if(count($plugin['SBA']) > 0) {
			$conds = " AND UPPER(s.SchemeType) != '".strtoupper("IES")."'";
		}

		$sql = "SELECT COUNT(*) FROM IES_SCHEME_TEACHER it INNER JOIN IES_SCHEME s ON (s.SchemeID=it.SchemeID $conds) WHERE it.UserID='$thisUserID'";
		$result = $this->returnVector($sql);
		$returnValue = ($result[0]==0) ? 0 : 1;
		return $returnValue;
	}

	/**
	 * Check whether the user (Student) already assigned to any one of the scheme
	 * for access right checking
	 * @return INT, 1 : true; 0 : false
	 */
	public function isSBA_Student($thisUserID)
	{
		/* the field "SchemeType" has the value "IES_COMBO", "IES_HISTORY", "IES_ENGLISH", etc... so currently return all non-IES Schemes
		*/
		global $plugin;

		if(count($plugin['SBA']) > 0) {
			$conds = " AND UPPER(s.SchemeType) != '".strtoupper("IES")."'";
		}

		$sql = "SELECT COUNT(*) FROM IES_SCHEME_STUDENT ss INNER JOIN IES_SCHEME s ON (ss.SchemeID=s.SchemeID $conds) WHERE UserID='$thisUserID'";

		$result = $this->returnVector($sql);
		$returnValue = ($result[0]==0) ? 0 : 1;
		return $returnValue;
	}


	public function GET_STAGE_ARR($SchemeID, $allowEdit=0){
	    global $intranet_db, $ies_cfg;

	    if(!$allowEdit) {
	    	$cond = " AND RecordStatus=".$ies_cfg["recordStatus_publish"];
	    }

	    $sql =  "
	              SELECT
	                StageID,
	                Title,
						Sequence,
						DATE_format(Deadline, '%Y-%m-%d') AS Deadline,
						Weight,
						MaxScore
	              FROM
	                {$intranet_db}.IES_STAGE
	              WHERE
	                SchemeID = '{$SchemeID}'
					$cond
	              ORDER BY
	                Sequence
	            ";
	    $returnArr = $this->returnArray($sql);

	    return $returnArr;
	  }

	function DisplayStage($SchemeID, $selectedStageID="")
	{
		global $Lang, $scheme_id, $sba_allow_edit;

		$stageAry = $this->GET_STAGE_ARR($SchemeID, $sba_allow_edit);

		$delim = '';
		for($i=0, $i_max=count($stageAry); $i<$i_max; $i++) {
			list($_stage_id, $_title, $_sequence, $_deadline, $_weight, $_maxScore) = $stageAry[$i];

			$currentClass = ($selectedStageID == $_stage_id) ? ' class="current"' : '';

			$a = ($i%5)+1;
			$x .= $delim.'<div class="stage_menu s'.$a.'"><a href="index.php?mod=content&task=stageInfo&scheme_id='.$SchemeID.'&stage_id='.$_stage_id.'" '.$currentClass.'><span>'.$_title.'</span></a></div>';

			$delim = '<div class="arrow"></div>';
		}
		//$x .= $delim.'<div class="stage_menu s1"><a href="#stage_id='.$_stage_id.'" '.$currentClass.'><span>xxx</span></a></div>';

		if($sba_allow_edit) {
			$x .= '<div class="stage_menu s_add"><a href="index.php?mod=content&task=stageNew&scheme_id='.$scheme_id.'" title="'.$Lang['SBA']['AddStage'].'"><span>&nbsp;</span></a></div>';
		}

		return $x;
	}

	public function GET_SCHEME_ARR($isDefaultScheme=""){
	    global $intranet_db, $sba_cfg, $ies_cfg;
		global $sba_thisStudentID, $sba_thisMemberType;

		//handle a teacher login , show his / her scheme only
		$teacherSQLQuery = "";
		$teacherCriteria = "";
		$extraCriteria = "";
		$distinct = "";
		if($isDefaultScheme!=''){
			$extraCriteria .= "and DefaultSchemeCode";
			//DefaultSchemeCode represents the scheme is a default scheme or not, for all custom scheme, the scheme code is 0
			$extraCriteria .= $isDefaultScheme>0 ? ">0":"=0";
			// only display "Published" scheme
			$extraCriteria .= $isDefaultScheme>0 ?" AND IES_SCHEME.RecordStatus=".$ies_cfg["recordStatus_publish"]:'';
			$isSetShowPublicOnly = true;
			if(time() >= mktime(0, 0, 0, 9, 1, 2016)){
				$extraCriteria .= " AND (IES_SCHEME.DefaultSchemeCode NOT IN (5))";
			}
		}
		if($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"]){
			//check not a IES Admin login
			//do nothing
		}else{
			if($sba_thisMemberType==USERTYPE_STAFF) {
				$extraSQLQuery = " LEFT JOIN {$intranet_db}.IES_SCHEME_TEACHER ies_st ON ies_st.SchemeID = IES_SCHEME.SchemeID ";
				$extraCriteria .= " and ies_st.UserID = '{$sba_thisStudentID}' ";  // the teacher id is equal to this teacher login
			$distinct = " distinct "; // to avoid duplicate scheme display while a teacher is both a teaching teacher and marking teacher (left join) IES_SCHEME_TEACHER
			} else if($sba_thisMemberType==USERTYPE_STUDENT) {
				$extraSQLQuery = " LEFT JOIN {$intranet_db}.IES_SCHEME_STUDENT ies_ss ON ies_ss.SchemeID = IES_SCHEME.SchemeID ";
				$extraCriteria .= " and ies_ss.UserID = '{$sba_thisStudentID}' ";  // the teacher id is equal to this teacher login
			$distinct = " distinct "; // to avoid duplicate scheme display while a teacher is both a teaching teacher and marking teacher (left join) IES_SCHEME_TEACHER
				if(!isset($isSetShowPublicOnly)||!$isSetShowPublicOnly){
					// only display "Published" scheme
					$extraCriteria .= " AND IES_SCHEME.RecordStatus=".$ies_cfg["recordStatus_publish"];
				}
			}

		}
	    $sql =  "
	              SELECT
			  	    {$distinct}
	                IES_SCHEME.SchemeID as 'SchemeID',
	                Title,
					IES_SCHEME.RecordStatus
	              FROM
	                {$intranet_db}.IES_SCHEME
				    {$extraSQLQuery}
				  where SchemeType = '".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."'
			  	    {$extraCriteria}
				  ORDER BY Title
	            ";
	    $returnArr = $this->returnArray($sql);

	    return $returnArr;
	}

	public function DisplaySchemeMenuByUser($thisUserID="", $selectedSchemeID="", $isDefaultScheme="")
	{
		global $Lang, $Lang;

		// return associated array, with SchemeID and Title
		// select from db
		/* parameter in $returnAry
		 * 0 => Scheme ID
		 * 1 => Scheme Title
		 * 2 => is published ? 1 = yes, 0 = no
		 */

		$schemeAry = $this->GET_SCHEME_ARR($isDefaultScheme);
		//debug_pr($schemeAry);
		//$selectedSchemeID = $scheme_id;

		if($selectedSchemeID=="") {
			$thisSelectedOption = "-- ".$Lang['SBA']['PleaseSelectScheme']." --\n";
		}


		// display slide-menu base on the values in $returnAry
		for($i=0, $i_max=count($schemeAry); $i<$i_max; $i++) {
			list($_scheme_id, $_title, $_isPublished) = $schemeAry[$i];
			//$_isPublished = 1;	// temporary set to 1

			$lockMsg = $_isPublished==1 ? '' : '<span class="scheme_lock" title="'.$Lang['IES']['RecordStatusNotPublish'].'">&nbsp;</span>';

			if($selectedSchemeID != "" && $_scheme_id == $selectedSchemeID) {
				$thisSelectedOption = $_title.$lockMsg;
			} else {
				$t_scheme_preview = $isDefaultScheme>0 ? "&isPreview=0" : "";
				$thisOption .= '<li><a href="index.php?mod=content&task=scheme_list&scheme_id='.$_scheme_id.$t_scheme_preview.'">'.$_title.$lockMsg.'</a></li>'."\n";
			}
		}

		if($thisOption != "") {
			$thisOption = '<ul class="scheme_option">
				                '.$thisOption.'
			                </ul>';
		}

		$x = '
				<div class="scheme_grp">
					<div class="scheme_head"></div>
					<div class="scheme_current">

		                '.$thisOption.'

	              	'.$thisSelectedOption.'
					</div>
					<a href="#" class="scheme_select" title="'.$Lang['IES']['ChooseOtherScheme'].'">&nbsp;</a>
            	</div>
		';

		return $x;
	}

	function IsEditable($thisUserID)
	{
		global $_SESSION;

		/* about to edit SBA Scheme/Stage/Task/Step if $thisUserID is SBA Admin
		 * true => editable, false => not editable
		 */

		if($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"] || $_SESSION["SSV_PRIVILEGE"]["SBA"]["isTeacher"]) {
			return 1;
		} else {
			return 0;
		}
	}
	/*
	static public function getSchemeLangRadio($ParFromPage="new",$ParLanguage="") {
		global $Lang;
		//HANDLE THE SCHEME XML LANG
		$schemeConfigXML = libSba::openXMLContent("scheme.default.xml");
		$schemeSupportLang = $schemeConfigXML[lang];
		$h_schemeTitle = $schemeConfigXML[title];

		//split the lang to a array
		$supportLangAry = explode("/",$schemeSupportLang);

		$h_RadioLang = "";
		$checked = "";

		//	Handle the default lang purchased
		$purchasedLang = self::getPurchaseLang();

		// Construct the html for selection of scheme lang
		for($i = 0,$i_max = sizeof($supportLangAry); $i  < $i_max;$i++)
		{
			$_tmpLang = $supportLangAry[$i];
			$_tmpID = "iesLang_".$_tmpLang;
			$_tmpLangCaption = $Lang['IES']['SchemeLang'][$_tmpLang];

			switch(strtoupper($ParFromPage)) {
				default:
				case "NEW":
					$checked = ($i == 0 )?" checked ":""; // set default checked the first value
					break;
				case "EDIT":
					if ($_tmpLang==$ParLanguage){
						$checked = " checked ";
					} else {
						$checked = "";
					}
					break;
			}
			if (in_array($_tmpLang,$purchasedLang)) {
				// Enabled
				$inputElementName = "selectedLang";
				$inputDisabled = "";
				$titleDisplay = "";
			} else {
				// Disabled
				$inputElementName = "";
				$inputDisabled = "disabled=\"disabled\"";
				$checked = "";
				$titleDisplay = "title='{$Lang['IES']['WantToUseThisLangContactUs']}'";
			}

			$h_RadioLang .= "<input type=\"radio\"  name=\"$inputElementName\" value=\"{$_tmpLang}\" id=\"{$_tmpID}\" {$checked} $inputDisabled ><label for=\"{$_tmpID}\" $titleDisplay>{$_tmpLangCaption}</label>&nbsp;\n";

		}
		return $h_RadioLang;
	}
	*/
	public function openXMLContent($file)
	{
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');
		$xml = null;

		if(file_exists($file)){
			$xmlstr = get_file_content("scheme.default.xml");
			$xml = new SimpleXMLElement($xmlstr);
		}
		return $xml;
	}

	static public function getPurchaseLang() {
		global $plugin;
		$defaultLangToUse = "b5";
		if (!is_array($plugin['IES_Lang_Purchased']) || !isset($plugin['IES_Lang_Purchased'])) {
			$purchasedLang = array($defaultLangToUse);
		} else {
			$purchasedLang = $plugin['IES_Lang_Purchased'];
		}
		return $purchasedLang;
	}

	/* function getUniqID() is used to generate unique id for Code field in Table IES_TASK */
	public function getUniqID(){
		return uniqid();
	}

	public function getMaxStageSequence($thisSchemeID)
	{
		$sql = "SELECT Max(Sequence) FROM IES_STAGE WHERE SchemeID='$thisSchemeID'";
		$result = $this->returnVector($sql);
		return 	$result[0];
	}

	public function getAnswerIDByTaskIDUserID($parTaskID, $parUserID){

		$sql = "SELECT AnswerID FROM IES_TASK_HANDIN WHERE TaskID = '$parTaskID' AND UserID = '$parUserID'";
		$AnswerID = current($this->returnVector($sql));

		return $AnswerID;
	}

	public function getAnswerIDByStepIDUserID($parStepID, $parUserID){

		$sql = "SELECT AnswerID FROM IES_QUESTION_HANDIN WHERE StepID = '$parStepID' AND UserID = '$parUserID'";
		$AnswerID = current($this->returnVector($sql));
		return $AnswerID;
	}

	public function GetDefaultMarkingCriteria()
	{
		global $intranet_db;

		$sql = "SELECT MarkCriteriaID, TitleChi, TitleEng, ".Get_Lang_Selection("TitleChi", "TitleEng")." as CRITERIATITLE, 0 as TASK_RUBRIC_ID,defaultMaxScore as MaxScore, defaultWeight as Weight, NULL as StageMarkCriteriaID FROM {$intranet_db}.IES_COMBO_MARKING_CRITERIA WHERE isDefault=1 ORDER BY defaultSequence";
		return $this->returnArray($sql);

	}
	public function getSchemeLangRadio($checkedVal = '',$subject = 'IES'){
		global $Lang,$sba_cfg;
		$h_RadioLang = "";
		$checked = "";

		//get all of the lang that the subject support
		$subjectSupportedLang = $this->getSystemSupportedSBASubjectLang($subject = 'IES');

		//get client purchased lang
		$purchasedLang = $this->getClientPurchasedSBASubjectLang($subject = 'IES');

		for($i=0,$i_max = count($subjectSupportedLang); $i <$i_max; $i++){
			$_tmpLang = $subjectSupportedLang[$i];
			$_tmpDivID = 'lang_'.$_tmpLang;
			$_tmpLangCaption = $Lang['IES']['SchemeLang'][$_tmpLang];

			$_checked = ($_tmpLang == strtolower($checkedVal)) ? ' checked ': '';

			//for new a scheme ,
			//default the first one is checked
			//cond: 1) _checked is empty after the first checking  must be '' , ($_checked) == '' and
			//2) this is the first lang display

			$_checked = (trim($_checked) == '' && $i == 0 )? ' checked ': $_checked;

			$h_RadioLang .= "<input type=\"radio\"  name=\"r_schemeLang\" value=\"{$_tmpLang}\" id=\"{$_tmpDivID}\" {$_checked} ><label for=\"{$_tmpDivID}\">{$_tmpLangCaption}</label>&nbsp;\n";
		}
		return $h_RadioLang;

	}

	public function getClientPurchasedSBASubjectLang($subject = 'IES') {
		global $plugin;
		$purchasedLang = NULL;

		$subject = strtoupper($subject);

		//plugin format
		//SUBJECT must be capital letter
		//IES --> $plugin['SBA']['IES_Lang_Purchased'] = array('en','b5');
		//MATHS --> $plugin['SBA']['MATHS_Lang_Purchased'] = array('en','b5');

		$langPurchaseFormat = $subject.'_Lang_Purchased';
		if (is_array($plugin['SBA'][$langPurchaseFormat]) && isset($plugin['SBA'][$langPurchaseFormat])) {
			$purchasedLang = $plugin['SBA'][$langPurchaseFormat];
		}
		return $purchasedLang;
	}
	public function getSystemSupportedSBASubjectLang($subject = 'IES'){
		global $sba_cfg;

		$returnLang = NULL;
		if(is_array($sba_cfg['SBA']['SubjectProvided'][$subject])){
			$returnLang = $sba_cfg['SBA']['SubjectProvided'][$subject]['lang'];
		}

		return $returnLang;


	}

	/**
	* Return Scheme , stage , task , step ID and Title by a given Step ID
	* @owner : Fai (20120710)
	* @param : Integer $stepID, the Step ID
	* @return : array , with returnResultSet Format
	*
	*/

	public function getStepStructureByStepID($stepID){
		global $intranet_db;
		$sql = "select
					scheme.SchemeID as 'SchemeID',
					scheme.Title as 'SchemeTitle',
					stage.StageID as 'StageID' ,
					stage.Title as 'StageTitle' ,
					task.TaskID as 'TaskID',
					task.Title as 'TaskTitle',
					step.StepID as 'StepID' ,
					step.Title as 'StepTitle'
				from
					{$intranet_db}.IES_STEP as step
				inner join
					{$intranet_db}.IES_TASK as task on step.TaskID = task.TaskID
				inner join
					{$intranet_db}.IES_STAGE as stage on task.StageID = stage.StageID
				inner join
					{$intranet_db}.IES_SCHEME as scheme on stage.SchemeID = scheme.SchemeID
				where  step.StepID = '{$stepID}'";

		$objDB = new libDB();
		return $objDB->returnResultSet($sql);

	}


	public function getSchemeStructureByTaskID($taskID){
		global $intranet_db;
		$sql = "select
					scheme.SchemeID as 'SchemeID',
					scheme.Title as 'SchemeTitle',
					stage.StageID as 'StageID' ,
					stage.Title as 'StageTitle' ,
					task.TaskID as 'TaskID',
					task.Title as 'TaskTitle'
				from
					{$intranet_db}.IES_TASK as task
				inner join
					{$intranet_db}.IES_STAGE as stage on task.StageID = stage.StageID
				inner join
					{$intranet_db}.IES_SCHEME as scheme on stage.SchemeID = scheme.SchemeID
				where  task.TaskID = '{$taskID}'";

		$objDB = new libDB();
		return $objDB->returnResultSet($sql);

	}


	public function getSchemeStructureByStageID($stageID){
		global $intranet_db;
		$sql = "select
					scheme.SchemeID as 'SchemeID',
					scheme.Title as 'SchemeTitle',
					stage.StageID as 'StageID' ,
					stage.Title as 'StageTitle'
				from
					{$intranet_db}.IES_STAGE as stage
				inner join
					{$intranet_db}.IES_SCHEME as scheme on stage.SchemeID = scheme.SchemeID
				where  stage.StageID = '{$stageID}'";

		$objDB = new libDB();
		return $objDB->returnResultSet($sql);

	}


	public function insertEmptyPowerConceptRecord($parUserID){
		global $sba_cfg;

		$sql = "INSERT INTO
					TOOLS_POWERCONCEPT
					(UserID, ModuleCode, InputDate, ModifiedDate)
				VALUES
					($parUserID, '".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."', NOW(), NOW())";
		$this->db_db_query($sql);
		$PowerConceptID = $this->db_insert_id();

		return $PowerConceptID;
	}

	public function deletePowerConceptRecords($parPowerConceptID_ary){
		if(is_array($parPowerConceptID_ary) && count($parPowerConceptID_ary)){
			$sql = "DELETE FROM TOOLS_POWERCONCEPT WHERE PowerConceptID IN (".implode(',',$parPowerConceptID_ary).")";
			$this->db_db_query($sql);
		}
	}

	function filename_safe($name) {
			$except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', ' ');
			return str_replace($except, '_', $name);
	}
	function trimOptions($ParOption) {
		return substr($ParOption,strpos($ParOption,"[")+1,strpos($ParOption,"]")-strpos($ParOption,"[")-1);
	}

	public function deleteSchemes($SchemeID_ary){
		if(is_array($SchemeID_ary) && count($SchemeID_ary)>0){
			$sql = "SELECT StageID FROM IES_STAGE WHERE SchemeID IN (".implode(',', $SchemeID_ary).")";
			$StageID_ary = $this->returnVector($sql);

			$sql = "SELECT SchemeID, Title FROM IES_SCHEME WHERE SchemeID IN (".implode(',', $SchemeID_ary).")";
			$_tempSchemeAry = $this->returnArray($sql);

			# delete SCHEME
			$this->deleteStages($StageID_ary);

			$sql = "DELETE FROM IES_SCHEME WHERE SchemeID IN (".implode(',', $SchemeID_ary).")";
			$this->db_db_query($sql);

			# deletion log
			$delim = "";
			$liblog = new liblog();
			$logDetails = "The following SBA Scheme(s) have been deleted : <br>";
			for($a=0, $a_max=count($_tempSchemeAry); $a<$a_max; $a++) {
				list($_scheme_id, $_scheme_title) = $_tempSchemeAry[$a];
				$logDetails .= $delim.$_scheme_title." (SchemeID : ".$_scheme_id.")";
				$delim = ", ";
			}

			$liblog->INSERT_LOG($this->Module, $Section='SCHEME', $RecordDetail=$logDetails, $TableName='IES_SCHEME', $RecodID='');
		}
	}

	public function deleteStages($StageID_ary)
	{
		global $sba_cfg;

		if(is_array($StageID_ary) && count($StageID_ary)>0){
			$sql = "SELECT TaskID FROM IES_TASK WHERE StageID IN (".implode(',', $StageID_ary).")";
			$TaskID_ary = $this->returnVector($sql);

			# delete corresponding tasks
			$this->deleteTasks($TaskID_ary);

			# get existing seqeunce of stage
			$sql = "SELECT DISTINCT scheme.SchemeID as SchemeID, stage.Title, stage.StageID FROM IES_STAGE stage INNER JOIN IES_SCHEME scheme ON (scheme.SchemeID=stage.SchemeID) WHERE StageID IN (".implode(',', $StageID_ary).")";
			$_tempSchemeAry = $this->returnArray($sql);
			$AssociateAry = BuildMultiKeyAssoc($_tempSchemeAry, "SchemeID", array("Title","StageID"));

			# Scheme ID affected (because need to reorder STAGE)
			$AffectedSchemeAry = array_keys($AssociateAry);

			# delete STAGE
			$sql = "DELETE FROM IES_STAGE WHERE StageID IN (".implode(',', $StageID_ary).")";
			$this->db_db_query($sql);

			# Re-order Stage within Scheme
			$this->Reorder_Stage($AffectedSchemeAry);

			# deletion log
			$delim = "";
			$liblog = new liblog();
			$logDetails = "The following SBA Stage(s) have been deleted : <br>";
			for($a=0, $a_max=count($_tempSchemeAry); $a<$a_max; $a++) {
				list($_scheme_id, $_stage_title, $_stage_id) = $_tempSchemeAry[$a];
				$logDetails .= $delim.$_stage_title." (StageID : ".$_stage_id.")";
				$delim = ", ";
			}
			$liblog->INSERT_LOG($this->Module, $Section='STAGE', $RecordDetail=$logDetails, $TableName='IES_STAGE', $RecodID='');


		}
	}

	public function deleteTasks($TaskID_ary){
		if(is_array($TaskID_ary) && count($TaskID_ary)>0){
			$sql = "SELECT StepID FROM IES_STEP WHERE TaskID IN (".implode(',', $TaskID_ary).")";
			$StepID_ary = $this->returnVector($sql);

			# delete corresponding steps
			$this->deleteSteps($StepID_ary);

			# get existing seqeunce of stage
			$sql = "SELECT DISTINCT stage.StageID as StageID, task.Title, task.TaskID, task.Attachments FROM IES_TASK task INNER JOIN IES_STAGE stage ON (stage.StageID=task.StageID) WHERE TaskID IN (".implode(',', $TaskID_ary).")";
			$_tempStageAry = $this->returnArray($sql);
			$AssociateAry = BuildMultiKeyAssoc($_tempStageAry, "StageID", array("Title","TaskID"));

			# Scheme ID affected (because need to reorder STAGE)
			$AffectedStageAry = array_keys($AssociateAry);

			# delete TASK
			$sql = "DELETE FROM IES_TASK WHERE TaskID IN (".implode(',', $TaskID_ary).")";
			$this->db_db_query($sql);

			# Re-order Task with Stage
			$this->Reorder_Task($AffectedStageAry);

			# add deletion log
			$delim = "";
			$liblog = new liblog();
			$logDetails = "The following SBA Task(s) have been deleted : <br>";
			for($a=0, $a_max=count($_tempStageAry); $a<$a_max; $a++) {
				list($_task_id, $_task_title, $_task_Attachment) = $_tempStageAry[$a];
				$this->removeAttachment($_task_Attachment);
				$logDetails .= $delim.$_task_title." ($_task_id)";
				$delim = ", ";
			}
			$liblog->INSERT_LOG($this->Module, $Section='TASK', $RecordDetail=$logDetails, $TableName='IES_TASK', $RecodID='');
		}
	}

	public function deleteSteps($StepID_ary){
		if(is_array($StepID_ary) && count($StepID_ary)>0){
			$sql = "SELECT StepID, Title, Attachments FROM IES_STEP WHERE StepID IN (".implode(',', $StepID_ary).")";
			$thisTempStepAry = $this->returnArray($sql);

			# delete corresponding handins
			$this->deleteStepHandins($StepID_ary);

			# get existing seqeunce of step
			$sql = "SELECT DISTINCT task.TaskID as TaskID, step.Title, step.StepID FROM IES_STEP step INNER JOIN IES_TASK task ON (task.TaskID=step.TaskID) WHERE StepID IN (".implode(',', $StepID_ary).")";
			$_tempStepAry = $this->returnArray($sql);
			$AssociateAry = BuildMultiKeyAssoc($_tempStepAry, "TaskID", array("Title","StepID"));

			# Task ID affected (because need to reorder STEP)
			$AffectedTaskAry = array_keys($AssociateAry);

			# delete "STEP"
			$sql = "DELETE FROM IES_STEP WHERE StepID IN (".implode(',', $StepID_ary).")";
			$this->db_db_query($sql);

			# Re-order Step within Task
			$this->Reorder_Step($AffectedTaskAry);


			# add deletion log
			$delim = "";
			$liblog = new liblog();
			$logDetails = "The following SBA Step(s) have been deleted : <br>";
			for($a=0, $a_max=count($thisTempStepAry); $a<$a_max; $a++) {
				list($_step_id, $_step_title, $_step_attachment) = $thisTempStepAry[$a];
				$logDetails .= $delim.$_step_title." ($_step_id)";

				$this->removeAttachment($_step_attachment);
				$delim = ", ";
			}
			$liblog->INSERT_LOG($this->Module, $Section='STEP', $RecordDetail=$logDetails, $TableName='IES_STEP', $RecodID='');
		}
	}

	public function deleteStepHandins($StepID_ary){
		if(is_array($StepID_ary) && count($StepID_ary)>0){
			$sql = "DELETE FROM IES_QUESTION_HANDIN WHERE StepID IN (".implode(',', $StepID_ary).")";
			$this->db_db_query($sql);

		}

	}

	private function Reorder_Stage($SchemeIdAry)
	{
		if(!is_array($SchemeIdAry)) {
			$thisSchemeIdAry = array($SchemeIdAry);
		} else {
			$thisSchemeIdAry = $SchemeIdAry;
		}

		if(count($thisSchemeIdAry) > 0) {
			$thisSchemeIdAry = array_unique($thisSchemeIdAry);		// DISTINCT SchemeID

			for($i=0, $i_max=count($thisSchemeIdAry); $i<$i_max; $i++) {
				$_thisSchemeID = $thisSchemeIdAry[$i];

				// get all existing stages of scheme
				$sql = "SELECT StageID FROM IES_STAGE WHERE SchemeID='$_thisSchemeID' ORDER BY Sequence";
				$_tempStageAry = $this->returnVector($sql);

				for($j=0, $j_max=count($_tempStageAry); $j<$j_max; $j++) {
					$_stage_id = $_tempStageAry[$j];
					$newSequence = $j+1;

					$sql = "UPDATE IES_STAGE SET Sequence='$newSequence' WHERE StageID='$_stage_id'";
					$x = $this->db_db_query($sql);
				}
			}
		}
	}

	private function Reorder_Task($StageIdAry)
	{
		if(!is_array($StageIdAry)) {
			$thisStageIdAry = array($StageIdAry);
		} else {
			$thisStageIdAry = $StageIdAry;
		}

		if(count($thisStageIdAry) > 0) {
			$thisStageIdAry = array_unique($thisStageIdAry);		// DISTINCT StageID

			for($i=0, $i_max=count($thisStageIdAry); $i<$i_max; $i++) {
				$_thisStageID = $thisStageIdAry[$i];

				// get all existing Task of Stage
				$sql = "SELECT TaskID FROM IES_TASK WHERE StageID='$_thisStageID' ORDER BY Sequence";
				$_tempTaskAry = $this->returnVector($sql);

				for($j=0, $j_max=count($_tempTaskAry); $j<$j_max; $j++) {
					$_task_id = $_tempTaskAry[$j];
					$newSequence = $j+1;

					$sql = "UPDATE IES_TASK SET Sequence='$newSequence' WHERE TaskID='$_task_id'";
					$x = $this->db_db_query($sql);
				}
			}
		}
	}

	private function Reorder_Step($TaskIdAry)
	{
		if(!is_array($TaskIdAry)) {
			$thisTaskIdAry = array($TaskIdAry);
		} else {
			$thisTaskIdAry = $TaskIdAry;
		}


		if(count($thisTaskIdAry) > 0) {
			$thisTaskIdAry = array_unique($thisTaskIdAry);		// DISTINCT TaskID

			for($i=0, $i_max=count($thisTaskIdAry); $i<$i_max; $i++) {
				$_thisTaskID = $thisTaskIdAry[$i];

				// get all existing Step of Task
				$sql = "SELECT StepID FROM IES_STEP WHERE TaskID='$_thisTaskID' ORDER BY Sequence";
				$_tempStepAry = $this->returnVector($sql);

				for($j=0, $j_max=count($_tempStepAry); $j<$j_max; $j++) {
					$_step_id = $_tempStepAry[$j];
					$newSequence = $j+1;

					$sql = "UPDATE IES_STEP SET Sequence='$newSequence' WHERE StepID='$_step_id'";
					$x = $this->db_db_query($sql);
				}
			}
		}
	}

	public function IsMarkingCriteriaComplete($scheme_id)
	{
		global $sba_cfg;

		include_once("libies.php");
		$objIES = new libies();
		$result = $objIES->GET_STAGE_ARR($scheme_id);

		$noOfStage = count($result);

		$IsCompleteStatus = $sba_cfg['MarkingCriteriaCompleteStatus']['Complete'];

		if($noOfStage==0) {		// no stage
			$IsCompleteStatus = $sba_cfg['MarkingCriteriaCompleteStatus']['NoStage'];
		} else {
			for($i=0; $i<$noOfStage; $i++) {
				$thisWeight = trim($result[$i]['Weight']);
				$thisMaxScore = trim($result[$i]['MaxScore']);
				if($thisWeight=="" || $thisMaxScore=="") {		// marking criteria setting not complete
					$IsCompleteStatus = $sba_cfg['MarkingCriteriaCompleteStatus']['NotComplete'];
					break;
				}
			}
		}
		return $IsCompleteStatus;
	}

	public function IsStageMarkingCriteriaComplete($stage_id)
	{
		global $sba_cfg;

		include_once("libies.php");
		$objIES = new libies();
		$result = $objIES->GET_STAGE_DETAIL($stage_id);
		debug_pr($result);

	}

	public function AllowAccessScheme($ParSchemeID, $ParStudentID)
	{
		global $ies_cfg;
		// student can only access published scheme
		$sql = "SELECT COUNT(student.SchemeStudentID) FROM IES_SCHEME_STUDENT student INNER JOIN IES_SCHEME scheme ON (scheme.SchemeID=student.SchemeID AND scheme.RecordStatus=".$ies_cfg["recordStatus_publish"].") WHERE student.SchemeID='$ParSchemeID' AND student.UserID='$ParStudentID'";
		$result = $this->returnVector($sql);

		return ($result[0]==0) ? 0 : 1;

	}


	public function getAttachmentDetails ($attachment_ids){
		global $intranet_db;

		$sql = "SELECT *
			FROM {$intranet_db}.IES_FILE WHERE FileID in ($attachment_ids)";

		return $this->returnArray($sql);
	}

	public function addAttachment($ParFolderPath,$ParFileName,$ParFileHashName,$ParInputBy) {

		global $intranet_db;

		$ParFileName = addslashes($ParFileName);
		$sql = "INSERT INTO {$intranet_db}.IES_FILE
			SET

				FOLDERPATH = '$ParFolderPath',
				FILENAME = '$ParFileName',
				FILEHASHNAME = '$ParFileHashName',
				DATEINPUT = NOW(),
				INPUTBY = '$ParInputBy'";

		$result = $this->db_db_query($sql);
		if ($result) {
			$result = mysql_insert_id();
		}
		return $result;
	}

	public function getTaskAttachmentDetails_byFileHashName($ParFileHashName) {
		global $intranet_db;
		$sql = "SELECT FILEID,FILENAME,FOLDERPATH,FILEHASHNAME,TASKID,DATEINPUT,INPUTBY,DATEMODIFIED,MODIFYBY
FROM {$intranet_db}.SBA_TASK_FILE WHERE FILEHASHNAME = '$ParFileHashName'";

		return $this->returnArray($sql);
	}
	public function removeAttachment($file_ids) {

		global $intranet_root;

		$files = $this->getAttachmentDetails($file_ids);
		$libfilesystem 	= new libfilesystem();

		foreach ($files as $file){

		    $file_path = $intranet_root.$file['FolderPath'].$file['FileHashName'];
		    $libfilesystem->file_remove($file_path);

		}
		global $intranet_db;
		$sql = "DELETE
		FROM {$intranet_db}.IES_FILE WHERE FileID in ($file_id)";
		return $this->db_db_query($sql);
	}

	public function insertStageMarkingCriteria($ParMarkingArr){
		global $intranet_db;

		if ($ParMarkingArr['task_rubric_id'] == '') {
			$ParMarkingArr['task_rubric_id'] = 'null';
		}
		if ($ParMarkingArr['MaxScore'] == '') {
			$ParMarkingArr['MaxScore'] = 'null';
		}
		if ($ParMarkingArr['Weight'] == '') {
			$ParMarkingArr['Weight'] = 'null';
		}

		$sql = "INSERT INTO {$intranet_db}.IES_STAGE_MARKING_CRITERIA (StageID,MarkCriteriaID,task_rubric_id,MaxScore,Weight,RecordStatus,DateInput,InputBy,ModifyBy) ";
		$sql .= "VALUES ('{$ParMarkingArr['StageID']}','{$ParMarkingArr['MarkCriteriaID']}',{$ParMarkingArr['task_rubric_id']},{$ParMarkingArr['MaxScore']},{$ParMarkingArr['Weight']},{$ParMarkingArr['RecordStatus']},{$ParMarkingArr['DateInput']},'{$ParMarkingArr['InputBy']}','{$ParMarkingArr['ModifyBy']}'); ";
		$ForDebug=$this->db_db_query($sql);

		if($ForDebug==false)
		{
			return false;
		}

		$returnResult = $this->db_insert_id();
		return $returnResult;
	}

}

?>