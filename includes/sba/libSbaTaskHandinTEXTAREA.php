<?php

class libSbaTaskHandinTEXTAREA extends libSbaTaskHandin{
	
	public function __construct($parStageID, $parTaskID = 0, $parAnswerId = null) {
		global $sba_cfg;
		
		parent::__construct($parStageID, $parTaskID, $parAnswerId);
		$this->setQuestionType($sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']);
	}
	
	public function setStudentAnswer($val){
		$this->setAnswer($val);
	}
	
	public function getDisplayAnswer(){
		return $this->getAnswer();
	}
	
	public function getDisplayAnswerInput(){
		# Get HTML editor for Introduction
		/*
		$oFCKeditor = new FCKeditor('r_Answer_'.$this->getTaskID(), '100%', 200, '', 'Basic2_SBA');
		$oFCKeditor->Value = $this->getAnswer();
		
		$html .= $oFCKeditor->Create2()."<textarea name='r_Answer' style='display:none'></textarea>";
		*/
		$html = getCkEditor('r_Answer_'.$this->getTaskID(), $this->getAnswer(), "sba", 200, '100%');
		$html .= "<textarea name='r_Answer' style='display:none'></textarea>";
		$html .= "<script>
				CKEDITOR.on('instanceReady',
				   function( evt ) {
				   		var oEditor = evt.editor;
				    	evt.editor.Name = evt.editor.name;
				    	FCKeditor_OnComplete(oEditor);
				   }
				);
				</script>";
		return $html;
	}
	
	public function getExportAnswer(){
            
                
		return preg_replace('`src="(/file/sba/[^"]+)"`','src="http://'.$_SERVER['SERVER_ADDR'].':'.$_SERVER['SERVER_PORT'].'$1"',$this->getAnswer());
	}
}

?>