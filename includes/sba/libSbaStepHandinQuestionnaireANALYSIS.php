<?php

/**
 * 2012-08-22 Mick ----EDITING----
 * 	Implement getExportDisplay()
 *
 *
 */

include_once("libies_survey.php");

abstract class libSbaStepHandinQuestionnaireANALYSIS extends libSbaStepHandin{
	
	private $questionStr;
	private $questionTypeForEdit; // store use with edit type for this analysis

	private $selfQuestionDataStructure; // abstract variable

	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;

		parent::__construct($parTaskID, $parStepID, $parAnswerId);

		//prepare a question str to store in t: IES_STEP.Question

		$this->setSelfQuestionDataStructure($parQuestion);

		$this->tranformQuestionStr($parQuestion);


	}
	public function getSelfQuestionDataStructure(){//abstract function
		return $this->selfQuestionDataStructure;
	}


	//$this->getSelfQuestionDataStructure() --> eg. array [12], [13],[14] stepid which tool is set as survey question edit
	public function setSelfQuestionDataStructure($question){ // abstract function //transfer to 
		$this->selfQuestionDataStructure = '';
		if(is_array($question)){
			$this->selfQuestionDataStructure = $question;
		}else{
			if(trim($question) != ''){
				$this->selfQuestionDataStructure = explode(',',$question);
			}
		}	
	}
	private function tranformQuestionStr($question){
		$this->questionStr = '';
		if(is_array($question)){

			$this->questionStr = implode(',',$question);
		}
	}

	public function getCaption(){
		return "Survey ANALYSIS";
	}

	public function setQuestionTypeForEdit($val){
		$this->questionTypeForEdit = $val;
	}

	public function getQuestionTypeForEdit(){
		return $this->questionTypeForEdit;
	}

	public function setStudentAnswer($SurveyID_ary){
		global $ies_cfg;
		
		$this->setAnswer(implode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $SurveyID_ary));
	}
	
	public function getDisplayAnswer(){
		/* For Survet, it should not contains any Student Answer, i.e. this method should not do anything */
		return null;
	}
	
	public function getDisplayAnswerForMarking(){
		global $ies_cfg; 

		$objSurvey = new libies_survey();

		$answerid   = $this->getAnswerID();
		$SurveyCode = $this->getQuestionnaireCode();


		$answer = $this->getRawAnswer();
		$surveyIdAry = array();
		$h_studentAns = '';
		if($answer != ''){
			//explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$_answerVal);
			$surveyIdAry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answer);

			if(is_array($surveyIdAry) && count($surveyIdAry) > 0){
				$_surveyAry = $objSurvey->getStudentSurveyBySurveyId($surveyIdAry);

				for($s = 0, $s_max = count($_surveyAry);$s < $s_max;$s++){
					$_surveyID = $_surveyAry[$s]['SurveyID'];

					$_surveyTitle = $_surveyAry[$s]['Title'];
					
					$_key = $this->getQuestionnaireKey($_surveyID);
					
					$h_studentAns .= intval($s+1).') <a href="javascript:void(0)" onclick="newWindow(\'/home/eLearning/sba/?mod=survey&task=management&key='.$_key.'\',10)" title="'.$Lang['IES']['View'].'">'.$_surveyTitle.'</a><br/>';
					$h_studentAns .= "\n";

				}
			}

		}
		return $h_studentAns;
	}
	
	public function getExportDisplay(){//same in all types
		
		global $ies_cfg, $Lang;
		
		$objSurvey = new libies_survey();
		$objStepMapper = new SbaStepMapper();
		
		$objSteps = $objStepMapper->findByStepIds($this->getSelfQuestionDataStructure());
		$surveyIdAry=array();
                
                //var_dump($objSteps);
                $analysis=array('images'=>array());
		$num=0;
                
		foreach ($objSteps as $objStep){
                    
                        $thisStepID = $this->getStepID();
                        $objStepID = $objStep->getStepID();
                        
			$surveyIdAry=$this->getSurveyIDs($objStepID);
			
                        foreach ($surveyIdAry as $surveyId){
			    
                                if ($surveyId==0) continue;
                                $surveyDetails=current($objSurvey->getSurveyDetails($surveyId));
                                
                                $title=(++$num).'-'.$surveyId.'-['.$surveyDetails["Title"].']';
                                
                                list($analysis[$title],$image_paths)=$objSurvey->genAnalysisDOC($surveyId);//ask objSurvey to gen Question in word format
                                //var_dump($_SESSION['SurveyIDAry'],$_SESSION['ExportPathAry'][$stepID][$surveyId],$image_paths);die();
                                if (!empty($_SESSION['ExportPathAry'][$thisStepID][$surveyId])){
				   //debug_r(array_values($_SESSION['ExportPathAry'][$thisStepID][$surveyId]));debug_r($image_paths);
                                        $analysis['images']+=array_combine($_SESSION['ExportPathAry'][$thisStepID][$surveyId],$image_paths);//session contains uploaded temp graphs
                                }
                        }
                }

		//debug_r($analysis['images']);
		return array('analysis'=>$analysis);
	}
	
	public function getSurveyIDs($_stepID){
		global $sba_thisStudentID,$ies_cfg;
		
		$objIES = new libies();
		$_answerDetails = $objIES->GET_CURRENT_STEP_ANSWER($_stepID,$sba_thisStudentID,$this->getQuestionTypeForEdit());
		
		if(!is_array($_answerDetails) || count($_answerDetails) != 1) return array();
		
		$_answerDetails = current($_answerDetails);
		$_answerVal = $_answerDetails['Answer'];
		if ($_answerVal=='') return array();
		
		return explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$_answerVal);
		
	}
	
	public function getQuestionDisplay(){

		global $sba_thisStudentID,$ies_cfg,$Lang,$sba_thisSchemeLang,$sba_allowStudentSubmitHandin;
		$totalHandinQuestionForEdit = 0;//store student total number of question setting
		$totalHandinQuestionForAnalysis = 0;//store student total number of question analysis
		$objSurvey = new libies_survey();
		$SurveyType = $this->getQuestionnaireType();
		$SurveyCode = $this->getQuestionnaireCode();

		$objStepMapper = new SbaStepMapper();

		//$this->getSelfQuestionDataStructure() --> stepid which tool is set as survey question edit
		$objSteps = $objStepMapper->findByStepIds($this->getSelfQuestionDataStructure());
		
		$answer = $this->getRawAnswer();
		$answerSurveyIdAry = array();
		$answerSurveyIdAry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answer);
		
		$h_studentAns  = '';
		for($i = 0,$i_max = count($objSteps);$i < $i_max; $i++){
			$_objStep = $objSteps[$i];
			$_stepID = $_objStep->getStepID();
			$_QuestionType = $_objStep->getQuestionType();
			$_stepTitle = $_objStep->getTitle();
			
			$h_studentAns .= '<br class="clear"><br class="clear">'.$Lang['IES']['Step_upper'].' '.$_stepTitle.':';

			//according to the DB t: IES_QUESTION_HANDIN UNIQUE KEY `StepUserQuestion` (`StepID`,`UserID`,`QuestionCode`), suppose return one array only
			
			$_surveyIDAry = $this->getSurveyIDs($_stepID);
			
			if(is_array($_surveyIDAry) && count($_surveyIDAry) > 0){
				$_surveyAry = $objSurvey->getStudentSurveyBySurveyId($_surveyIDAry,$orderBy= '  order by s.DateModified desc , s.title');

				$h_studentAns .= "<table class='sub_form_table'>";
				$totalHandinQuestionForEdit = count($_surveyAry);
				for( $s = 0; $s < $totalHandinQuestionForEdit; $s++ ) {

					$_surveyID = $_surveyAry[$s]['SurveyID'];
					$_DateModified = $_surveyAry[$s]['DateModified'];
					$_surveyTitle = $_surveyAry[$s]['Title'];

					$_key = 'SurveyID='.$_surveyID.'&StudentID='.$sba_thisStudentID.'&FromPage='.$ies_cfg['Questionnaire']['SurveyAction']['COLLECT'].':'.$SurveyCode;
					$_key = base64_encode($_key);
					
					$h_studentAns .= "<tr>
										  <td><a href='javascript:void(0)' onclick='newWindow(\"/home/eLearning/sba/?mod=survey&task=management&key=".$_key."\",10)' class='task_form' title='".$Lang['IES']['View']."'>{$_surveyTitle}</a></td>";
					
					if(in_array($_surveyID, $answerSurveyIdAry)){ 
						$totalHandinQuestionForAnalysis = $totalHandinQuestionForAnalysis +1;
						$h_studentAns .= "<td><span class=\"object_edit_date\">(".$Lang['IES']['LastModifiedDate']." : {$_DateModified})</span></td>
										  <td><span class='object_edit_date'>(".$Lang['IES']['Submitted'].")</span></td>
						  <td><div class='table_row_tool'><a title='".$Lang['IES']['Delete2']."' class='delete' href='javascript:void(0)' onclick='deleteSurveyAnalysis(".$this->getTaskID().", ".$this->getStepID().", ".$this->getAnswerID().", {$_surveyID})'></a></div></td>";
					}
					else{
						$h_studentAns .= "
										  <td><span class=\"object_edit_date\">(".$Lang['IES']['LastModifiedDate']." : {$_DateModified})</span></td>
										  <td>
												<input 
												type='button' value='".$Lang['IES']['Submit']."' 
												onclick='analysysSubmitAsHandin(".$this->getTaskID().", ".$this->getStepID().", {$_surveyID}, \"".$this->getQuestionType()."\")' 
												onmouseout='this.className=\"formbutton\"' onmouseover='this.className=\"formbuttonon\"' class='formbuttonon' name='submit2'>
										  </td>
										  <td>&nbsp;</td>";
					}
					$h_studentAns .= "</tr>";
				}
				
				$h_studentAns .= "</table>";
			}else{
				//else skip to next loop
				$h_studentAns  .= '--';
				continue; 
			}
		}

		$totalHandinQuestionForEdit = ($totalHandinQuestionForEdit == '') ? 0 : $totalHandinQuestionForEdit;
		$totalHandinQuestionForAnalysis = ($totalHandinQuestionForAnalysis == '') ? 0: $totalHandinQuestionForAnalysis;


		$h_submitStatusNumber = "已製作 {$totalHandinQuestionForEdit} 份 及 呈交 {$totalHandinQuestionForAnalysis} 份";
		if(strtolower($sba_thisSchemeLang) == 'en'){
			$h_submitStatusNumber = $Lang['IES']['SurveyCreated']." {$totalHandinQuestionForEdit} and ".$Lang['IES']['Submitted']." {$totalHandinQuestionForAnalysis} ";
		}

if($sba_allowStudentSubmitHandin){
	//do nothing, no need to handle the preview Input
}else{
		$_key = $this->getQuestionnaireKey($_surveyID);
		$_onClickAction = "newWindow('/home/eLearning/sba/?mod=survey&task=management&key={$_key}',10)";
		$h_previewStudentInput = "<div class=\"stage_left_tool\"><a href=\"javascript:void(0)\" onclick=\"{$_onClickAction}\" class=\"add\" title=\"{$Lang['IES']['View']}\"><span>".$this->getQuestionTypeName()."</span></a></div>";
}

		$QuestionHTML = "<div class=\"IES_task_form\" style=\"margin-top:30px\">
                        	<h4 class=\"icon_stu_edit\"><strong>".$this->getQuestionTypeName()."</strong></h4>
                        	<span class=\"guide\">{$h_submitStatusNumber}</span>
                        	<div class=\"wrapper\">
								{$h_studentAns}
						
                            	<br class=\"clear\">
								{$h_previewStudentInput}
								<br class=\"clear\">
                        	</div>
                		 </div>";

		return $QuestionHTML;
	}
	public function getQuestionnaireKey($_surveyID){
            return base64_encode("SurveyID=".$_surveyID."&StudentID=".$this->getUserID()."&TaskID=".$this->getTaskID()."&FromPage=".$ies_cfg['Questionnaire']['SurveyAction']['COLLECT'].":".$this->getQuestionnaireCode());	
        }
        
	public function getEditQuestionDisplay(){


		global $intranet_root,$Lang,$ies_cfg;
		include_once($intranet_root."/includes/sba/libSbaStep.php");
		$SurveyCode = $this->getQuestionnaireCode();

		$taskID    = $this->getTaskID();
		$stageID   = $this->getStageID();
		$id_suffix = $this->getStepID()? $this->getStepID() : 'new';
		
		$linterface = new interface_html();
				
		$objStepMapper = new SbaStepMapper();
		$objSteps = $objStepMapper->findByStageId($stageID);
		$h_subTable = '';

		$surveyFoundForEdit = 0;
		for($i = 0,$i_max = count($objSteps);$i < $i_max; $i++){
			$_objStep = $objSteps[$i];
			$_QuestionType = $_objStep->getQuestionType();

			if($_QuestionType == $this->getQuestionTypeForEdit()){
				$surveyFoundForEdit = $surveyFoundForEdit +1;
				$_Title = $_objStep->getTitle();
				$_StepID = $_objStep->getStepID();

				$_checked = '';
				if(is_array($this->getSelfQuestionDataStructure())){
					$_checked = in_array($_objStep->getStepID(),$this->getSelfQuestionDataStructure())? ' checked ': '' ;
				}

				$h_subTable .= '<tr><td><input type="checkbox" name="r_step_Question[]" value ="'.$_StepID.'" id="surveyStepID_'.$_StepID.'" '.$_checked.'/><label for="surveyStepID_'.$_StepID.'"> '.$Lang['IES']['Step_upper'].' : '.$_Title.'</label></td></tr>'."\n";
			}
		}

		if($surveyFoundForEdit >0 ){
						$h_subTable = '<table class="sub_form_table">'.$h_subTable.'</table>';
		}else{
						$h_subTable = '<table class="sub_form_table"><tr><td><font color="red">'.$Lang['SBA']['Survey']['SurveyQuestionForAnalysisIsEmpty'].'</font></td></tr></table>';
		}


		$_key = $this->getQuestionnaireKey($_surveyID);					
		$_onClickAction = "newWindow('/home/eLearning/sba/?mod=survey&task=management&key={$_key}',10)";
		$h_previewStudentInput = "<div class=\"stage_left_tool\"><a href=\"javascript:void(0)\" onclick=\"{$_onClickAction}\" class=\"add\" title=\"{$Lang['IES']['View']}\"><span>".$this->getQuestionTypeName()."</span></a></div>";

		$t_QuestionCode = $this->getQuestionnaireCode();
		$t_QuestionCode = $t_QuestionCode=='Observation'?'Observe': $t_QuestionCode;

		$QuestionHTML = "<script>
							function step_tool_validate_fail(){
								if($('[id^=\"surveyStepID_\"][name=\"r_step_Question[]\"]:checked').length==0){

									$('#step_Question_surveyStepID_{$id_suffix}_ErrorDiv').show();
									return true;
								}

								return false;
							}
						 </script>
						 <div class=\"IES_task_form\">
                        	<h4 class=\"icon_stu_edit\"><strong>".$this->getQuestionTypeName()."</strong></h4>
                        	<span class=\"guide\">{$Lang['IES']['SurveyCreated']}:0</span>
                        	<div class=\"wrapper\">
                        		{$Lang['IES'][$t_QuestionCode]}{$Lang['SBA']['Survey']['SelectOneItemForAnalysisFromDifferentTool']}
								{$h_subTable}								
								{$h_previewStudentInput}
								<br class=\"clear\">
                        	</div>
                		 </div>
						 ".$linterface->Get_Form_Warning_Msg("step_Question_surveyStepID_{$id_suffix}_ErrorDiv", "{$Lang['SBA']['Survey']['SelectOneItemForSurveyAnalysis']}", 'WarningDiv')."
						 <input type=\"hidden\" id=\"r_step_QuestionType\" name=\"r_step_QuestionType\" value=\"".($this->getQuestionType())."\">";
						 
		return $QuestionHTML;
	}
	
	public function getQuestionStr(){
		return $this->questionStr;
	}

	public function getRawAnswer(){

		return $this->getAnswer();
	}
	//return 0, 1, 2
	abstract public function getQuestionnaireType();
	abstract public function getQuestionTypeName();
	//return Survey , Interview,Observation
	abstract public function getQuestionnaireCode();



}


class libSbaStepHandinSurveyANALYSIS extends libSbaStepHandinQuestionnaireANALYSIS {

	public function __construct($parTaskID,$parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;

		parent::__construct( $parTaskID,$parStepID, $parQuestion, $parAnswerId);

		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['surveyAnalysis'][0]);
		$this->setQuestionTypeForEdit($sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'][0]);
	}

	public function getQuestionnaireType(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0];

	}

	public function getQuestionnaireCode(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][2];

	}
	public function getQuestionTypeName(){ // this function should be abstract to parent "libSbaStepHandin" not only libSbaStepHandinQuestionnaireEDIT, need to discuss with thomas
		global $sba_cfg;
		return $sba_cfg['DB_IES_STEP_QuestionType']['surveyAnalysis'][1];
	}
	
}

class libSbaStepHandinInterviewANALYSIS extends libSbaStepHandinQuestionnaireANALYSIS {

	public function __construct($parTaskID,$parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;
		parent::__construct($parTaskID,$parStepID, $parQuestion, $parAnswerId);
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['interviewAnalysis'][0]);
		$this->setQuestionTypeForEdit($sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'][0]);
	}

	public function getQuestionnaireType(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0];

	}

	public function getQuestionnaireCode(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][2];

	}
	public function getQuestionTypeName(){ // this function should be abstract to parent "libSbaStepHandin" not only libSbaStepHandinQuestionnaireEDIT, need to discuss with thomas
		global $sba_cfg;
		return $sba_cfg['DB_IES_STEP_QuestionType']['interviewAnalysis'][1];
	}
}

class libSbaStepHandinObserveANALYSIS extends libSbaStepHandinQuestionnaireANALYSIS {

	public function __construct($parTaskID,$parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;
		parent::__construct($parTaskID,$parStepID, $parQuestion, $parAnswerId);
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['observeAnalysis'][0]);
		$this->setQuestionTypeForEdit($sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'][0]);
	}

	public function getQuestionnaireType(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0];

	}

	public function getQuestionnaireCode(){
		global $ies_cfg;
		return $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][2];

	}

	public function getQuestionTypeName(){ // this function should be abstract to parent "libSbaStepHandin" not only libSbaStepHandinQuestionnaireEDIT, need to discuss with thomas
		global $sba_cfg;
		return $sba_cfg['DB_IES_STEP_QuestionType']['observeAnalysis'][1];
	}

}



?>