<?php
# using : thomas

class libSbaStepHandinPOWERCONCEPT extends libSbaStepHandin{
	
	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg;
		
		parent::__construct($parTaskID,$parStepID, $parAnswerId);
		
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['powerConcept'][0]);
	}
	
	public function getCaption(){
		global $sba_cfg;
		
		return $sba_cfg['DB_IES_STEP_QuestionType']['powerConcept'][1];
	}
	
	public function setStudentAnswer($PowerConceptID_ary){
		global $ies_cfg;
		
		$this->setAnswer(implode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $PowerConceptID_ary));
	}
	
	public function getDisplayAnswer(){
		return $this->getAnswer();
	}
	
	public function getDisplayAnswerForMarking(){
		$PowerConcept_ary = $this->getPowerConceptDataByIds($this->getPowerConceptIdAry());
		$totalHandin  	  = count($PowerConcept_ary);
		
		$_scheme_id = $this->getSchemeID()? $this->getSchemeID() : 0;
		$_stage_id  = $this->getStageID()?  $this->getStageID()  : 0;
		$_task_id 	= $this->getTaskID()?   $this->getTaskID()   : 0;
		$_step_id 	= $this->getStepID()?   $this->getStepID()   : 0;
		$_answer_id = $this->getAnswerID()? $this->getAnswerID() : 0;
		
		for($i=0; $i<$totalHandin; $i++){
			$PowerConceptID = $PowerConcept_ary[$i]['PowerConceptID'];
			$ModifiedDate   = $PowerConcept_ary[$i]['ModifiedDate'];
			$MarkingHTML  .= "<p><a title=\"View\" class=\"task_conceptmap\" href=\"javascript:void(0)\" onclick=\"viewPowerConcept($PowerConceptID)\">conceptmap.epc ($ModifiedDate)</a></p>";
		}
		
		return $MarkingHTML; 
	}
	
	public function getExportDisplay(){
		return null;
	}
	public function getQuestionDisplay(){
		global $sba_allowStudentSubmitHandin, $Lang,$sba_thisSchemeLang;
		
		$PowerConcept_ary = $this->getPowerConceptDataByIds($this->getPowerConceptIdAry());
		$totalHandin  	  = count($PowerConcept_ary);
		
		$_scheme_id = $this->getSchemeID()? $this->getSchemeID() : 0;
		$_stage_id  = $this->getStageID()?  $this->getStageID()  : 0;
		$_task_id 	= $this->getTaskID()?   $this->getTaskID()   : 0;
		$_step_id 	= $this->getStepID()?   $this->getStepID()   : 0;
		$_answer_id = $this->getAnswerID()? $this->getAnswerID() : 0;
		
		for($i=0; $i<$totalHandin; $i++){
			$PowerConceptID = $PowerConcept_ary[$i]['PowerConceptID'];
			$ModifiedDate   = $PowerConcept_ary[$i]['ModifiedDate'];
			$h_studentAns  .= "<tr id='sba_step_handin_powerconcept_$PowerConceptID'>
									<td>
										<a title=\"View\" class=\"task_conceptmap\" href=\"javascript:void(0)\" onclick=\"editPowerConcept($_scheme_id, $_stage_id, $_task_id, $_step_id, $_answer_id, $PowerConceptID)\">Conceptmap</a>
										<input type=\"hidden\" name=\"r_stdAnswer[]\" value=\"$PowerConceptID\">
									</td>
                            	  	<td><span class=\"object_edit_date\">({$Lang['IES']['LastModifiedDate']} : $ModifiedDate)</span></td>
                            	  	<td><div class=\"table_row_tool\"><a title=\"{$Lang['IES']['Delete2']}\" class=\"delete\" href=\"javascript:void(0)\" onclick=\"deletePowerConcept($_task_id, $_step_id, $_answer_id, $PowerConceptID)\"></a></div></td>
							   </tr>"; 
		}

		
		$thisJsAction = 'alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')';
		if($sba_allowStudentSubmitHandin){
			$thisJsAction = "editPowerConcept({$_scheme_id}, {$_stage_id}, {$_task_id}, {$_step_id}, {$_answer_id}, 0)";
		}

		$h_captionNoOfHandin = (strtolower($sba_thisSchemeLang) == 'en') ? '' : $Lang['IES']['Copy1'];

		$QuestionHTML = "<div class=\"IES_task_form\" style=\"margin-top:30px\">
                        	<h4 class=\"icon_stu_edit\"><strong>{$Lang['SBA']['ConceptMap']}</strong></h4>
                        	<span class=\"guide\">{$Lang['SBA']['ConceptMapCaption']['Created']}: {$totalHandin} {$h_captionNoOfHandin}</span>
                        	<div class=\"wrapper\">
								<table class=\"sub_form_table\">
                            		{$h_studentAns}
                                	<tr>
										<td>
											<div class=\"stage_left_tool\">
												<a title=\"{$Lang['SBA']['ConceptMapCaption']['newConcept']}\" class=\"add\" href=\"javascript:void(0)\" onclick=\"{$thisJsAction}\"><span>{$Lang['SBA']['ConceptMapCaption']['newConcept']}</span></a>
											</div>
										</td>
                                  		<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								</table>
                        	</div>
                		 </div>";

		return $QuestionHTML;
	}
	
	public function getEditQuestionDisplay(){
		global $Lang, $sba_allowStudentSubmitHandin;
		
		$thisJsAction = 'alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')';
		if($sba_allowStudentSubmitHandin){
			$thisJsAction = "viewPowerConcept(0)";
		}

		$h_captionNoOfHandin = (strtolower($sba_thisSchemeLang) == 'en') ? '' : $Lang['IES']['Copy1'];

		$QuestionHTML = "<div class=\"IES_task_form\">
                        	<h4 class=\"icon_stu_edit\"><strong>{$Lang['SBA']['ConceptMap']}</strong></h4>
                        	<span class=\"guide\">{$Lang['SBA']['ConceptMapCaption']['Created']}: 0 {$h_captionNoOfHandin}</span>
                        	<div class=\"wrapper\">
								<div class=\"stage_left_tool\">
									<a title=\"{$Lang['SBA']['ConceptMapCaption']['newConcept']}\" class=\"add\" href=\"javascript:void(0)\" onclick=\"".$thisJsAction."\"><span>{$Lang['SBA']['ConceptMapCaption']['newConcept']}</span></a>
								</div>
                            	<br class=\"clear\">
                        	</div>
                		 </div>
						 <input type=\"hidden\" id=\"r_step_QuestionType\" name=\"r_step_QuestionType\" value=\"".($this->getQuestionType())."\">";
		return $QuestionHTML;
	}
	
	public function getQuestionStr(){
		return null;
	}
	
	public function getRawAnswer(){//should be an abstract function to libSbaStepHandin
		return $this->getAnswer();
	}
	
	private function getPowerConceptIdAry(){
		global $ies_cfg;
		
		$PowerConceptID_ary = array();
		
		if($this->getAnswer()){
			$PowerConceptID_ary = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $this->getAnswer());
		}
		
		return $PowerConceptID_ary;
	}
	
	private function getPowerConceptDataByIds($PowerConceptID_ary){
		$result = array();
		
		if(is_array($PowerConceptID_ary) && count($PowerConceptID_ary)>0){
			$sql = "SELECT
						*
					FROM
						TOOLS_POWERCONCEPT
					WHERE
						PowerConceptID IN (".implode(',', $PowerConceptID_ary).")
					ORDER BY
						ModifiedDate";
			$result = $this->objDB->returnResultSet($sql);
		}
		
		return $result;
	}
}

?>