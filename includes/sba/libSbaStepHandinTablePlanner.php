<?php
 /**
 * 2012-08-22 Mick
 *	Implement getExportDisplay()
 *
 * */
class libSbaStepHandinTablePlanner extends libSbaStepHandin{
	
	private $QuestionTitle;
	private $Question;
	private $columnNum;
	
	public function __construct($parTaskID, $parStepID=0, $parQuestion='', $parAnswerId=0) {
		global $sba_cfg, $Lang;
		
		parent::__construct($parTaskID, $parStepID, $parAnswerId);
		
		$this->setQuestionType($sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'][0]);
		$this->setQuestionTitle($sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'][1]);
		$this->setQuestion($parQuestion);
	}
	
	public function getCaption(){
		global $sba_cfg;
		
		return $sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'][1];
	}
	
	/* This function should receive r_stdAnswer[num] defined in getQuestionDisplay() */
	public function setStudentAnswer($stdAnswer_ary){
		
		global $ies_cfg;
	
		$rows=array_chunk($stdAnswer_ary,$this->columnNum);//chunk answers by no of columns
		
		$table_temp=array();
		foreach ($rows as $row){
			$table_temp[]=implode($ies_cfg["comment_answer_sperator"],$row);
		}
		$this->setAnswer(implode($ies_cfg["question_answer_sperator"],$table_temp));
		
	}
	
	public function getDisplayAnswer(){
		
		return $this->getDisplayAnswerForMarking();
	}
	
	public function getDisplayAnswerForMarking(){
		
		global $ies_cfg;
		
		$answer=$this->getAnswer();
		
		if (!$answer){return "";}
		
		$html = "<table></tr><th>#</th>";//question head
		foreach ($this->getQuestion() as $value){
			$html.='<th>'.htmlspecialchars(stripslashes($value)).'</th>';
		}
		$html.="</tr>";
		
		$rows=explode($ies_cfg["question_answer_sperator"],$answer);
		foreach ($rows as $num=>$row){
			$answers=explode($ies_cfg["comment_answer_sperator"],$row);
			$html.='<tr><td>'.($num+1).'</td>';
			for ($i=0;$i<$this->columnNum;$i++){
				$html.='<td>'.htmlspecialchars(stripslashes($answers[$i])).'</td>';// exploded answer
			}
			$html.='</tr>';
		}
		
		$html.='</table>';
		
		return $html;

	}
	
	public function getExportDisplay(){
		
		$title = '<h3>'.$this->getQuestionTitle().'</h3>';
		return array($this->getQuestionType()=>$title.$this->getDisplayAnswer());
		
	}
	
	public function getQuestionDisplay(){
		global $sba_allowStudentSubmitHandin, $Lang, $ies_cfg;
		
		$_task_id 	= $this->getTaskID()?   $this->getTaskID()   : 0;
		$_step_id 	= $this->getStepID()?   $this->getStepID()   : 0;
		$_answer_id = $this->getAnswerID()? $this->getAnswerID() : 0;
		
		//default: show alert
		$form_editRow = $form_deleteRow = $form_addRow = $from_reset = $form_submit = 'alert(\''.$Lang['SBA']['StudentClickThisToSubmitHandin'].'\')';
		if($sba_allowStudentSubmitHandin){//student actions
			$form_submit=	"step_stdAnswer_handin_form_submit({$_task_id}, {$_step_id}, {$_answer_id})";
			$from_reset= 	"step_stdAnswer_handin_form_reset({$_step_id});tablePlanner_resetRow();";
			$form_addRow=	'tablePlanner_addRow()';
			$form_deleteRow='tablePlanner_deleteRow(this)';
			$form_editRow=	"tablePlanner_editRow()";
		}
		
		$answer_th = "";
		foreach ($this->getQuestion() as $value){//question head
			$answer_th.='<th>'.htmlspecialchars(stripslashes($value)).'</th>';
		}
		
		$rows=explode($ies_cfg["question_answer_sperator"],$this->getAnswer());
		$rowNum=sizeof($rows);
		$answer_tr="";
		foreach ($rows as $num=>$row){
			$answers=explode($ies_cfg["comment_answer_sperator"],$row);
			$answer_tr.='<tr><td>'.($num+1).'</td>';
			for ($i=0;$i<$this->columnNum;$i++){//answer text boxes
				$answer_tr.='<td><input type="text" style="width:98%;" name="r_stdAnswer[]" value="'.htmlspecialchars(stripslashes($answers[$i])).'"/></td>';
			}
			$answer_tr.='<td><a class="edit_row" onclick="'.$form_editRow.'" title="Edit" href="javascript: void(0);"></a><a class="delete_row" onclick="'.$form_deleteRow.'" title="Delete" href="javascript: void(0);"></a></td></tr>';
		}
		$script = "<script>
			tablePlanner_columnNum=$this->columnNum;
			tablePlanner_rowNum=$rowNum;
			$('#tablePlanner_display tbody').sortable({
				helper: tablePlanner_uiHelper,
				stop: tablePlanner_reNumberRows
				});
			</script>";//jqueryui sortable init
		
		return "<h4 class=\"icon_stu_edit\" style=\"margin-top:30px\">
				<span class=\"task_instruction_text\">".$this->getQuestionTitle()."</span>
			</h4>
			<table class=\"common_table_list IES_form_table\" id=\"tablePlanner_display\">
				<colgroup><col width=5%/></colgroup>
				<thead><tr><th>#</th>$answer_th<th></th></tr></thead>
				<tbody>$answer_tr</tbody>
				<tfoot><tr><td colspan=\"".($this->columnNum+2)."\" style=\"text-align:right\" ><a href=\"javascript: void(0);\" class=\"add_row\" onclick=\"$form_addRow\">".$Lang['IES']['AddRow']."</a></td></tr></tfoot>
			</table>
			<span id=\"tablePlanner_error\" class=\"WarningDiv tabletextrequire\"></span>
			<div style=\"text-align:center; margin-top:10px\">
				<input type=\"button\" value=\"".$Lang['IES']['Save2']."\" onclick=\"{$form_submit}\" onmouseout=\"this.className=&quot;formbutton&quot;\" onmouseover=\"this.className=&quot;formbuttonon&quot;\" class=\"formbutton\" name=\"submit2\">  
				<input type=\"button\" value=\"".$Lang['IES']['Reset']."\" onclick=\"{$from_reset}\" onmouseout=\"this.className=&quot;formbutton&quot;\" onmouseover=\"this.className=&quot;formbuttonon&quot;\" class=\"formbutton\">  
			</div>
			<div class=\"edit_bottom\"><span>".$Lang['IES']['LastModifiedDate']." : ".($this->getDateModified()? $this->getDateModified():'--')."</span></div>
			$script";
		
	}
	
	public function getEditQuestionDisplay(){
		
		global $sba_cfg,$Lang,$ies_cfg;

		$changeColumnWarning = ($this->getStepID())? ' <span class="tabletextrequire">'.$Lang['SBA']['ChangetablePlannerWarning'].'</span>' : ''; //show alert in js if step exists

		$answer_th="";
		foreach ($this->getQuestion() as $value){//question text boxes
			$answer_th.='<th><input type="text" style="width:80%;" name="r_step_Question[]" value="'.htmlspecialchars(stripslashes($value)).'"/><a class="delete_row" onclick="tablePlanner_deleteColumn(this)" title="Delete" href="javascript: void(0);"></a></th>';
		}
		
		return "
                        <script>function step_tool_validate_fail(){return false;}</script>
                        <div class=\"IES_task_form\">
			<h4 class=\"icon_stu_edit\"><strong>".$this->getQuestionTitle()."</strong>$changeColumnWarning</h4>
			<table class=\"common_table_list IES_form_table\" id=\"tablePlanner_edit\">
			<thead><tr><th width=2%>#</th>$answer_th<th width=2% id=\"addRowTh\"><a href=\"javascript: void(0);\" class=\"add_row\" title=\"Add Column\" onclick=\"tablePlanner_addColumn()\"></a></th></tr></thead></table>
			<input type=\"hidden\" id=\"r_step_QuestionType\" name=\"r_step_QuestionType\" value=\"".$this->getQuestionType()."\"></div><span id=\"tablePlanner_error\" class=\"WarningDiv tabletextrequire\"></span>";
		
	}

	private function getQuestion(){
		return $this->Question;
	}
	
	private function setQuestion($val){
		
		global $ies_cfg;

		$val=(is_array($val))? $val: explode($ies_cfg["comment_answer_sperator"],$val);//may give back 2 types of answers
		$this->columnNum=sizeof($val);
		$this->Question=$val;
	}
	
	public function getQuestionStr(){
		
		global $ies_cfg;
		
		return implode($ies_cfg["comment_answer_sperator"],$this->getQuestion());
	}
		
	private function setQuestionTitle($val){
		$this->QuestionTitle=$val;
	} 
	
	private function getQuestionTitle(){
		return $this->QuestionTitle;
	}
	
}

?>