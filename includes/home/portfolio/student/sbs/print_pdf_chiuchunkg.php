<?php
// Using: Pun

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
iportfolio_auth("SPTA");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpf-sbs.php");
include_once($PATH_WRT_ROOT."includes/lib-growth-scheme.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

intranet_opendb();

######## Init START ########
$lpf = new libpf_sbs();

$assignment_id = IntegerSafe($assignment_id);
$phase_id = IntegerSafe($phase_id);

if($ck_memberType == "P")
{
	$user_id = $lpf->getCourseUserID($ck_current_children_id);
}

if($user_id=="")
{
	$user_id = $lpf->getCourseUserID($UserID);
}
######## Init END ########


######## Access Right START ########
$lpf->CHECK_ACCESS_IPORTFOLIO($PATH_WRT_ROOT);
$lpf->ACCESS_CONTROL("growth_scheme");


$lgs = new growth_scheme();

if (!in_array($user_id, $lgs->getSchemeUsers($assignment_id))) {
	No_Access_Right_Pop_Up();
}
######## Access Right END ########


######## Student Info START ########
$StudentIDArr = array();
$StudentIDArr[] = $user_id;
$UserArr = $lgs->getHandedinUserInfo($StudentIDArr);

$nameSql = getNameFieldByLang2("iu.");
$sql = "SELECT 
	{$nameSql} as Name,
	iu.ClassName,
	iu.ClassNumber
FROM 
	{$lpf->course_db}.usermaster um
INNER JOIN 
	{$intranet_db}.INTRANET_USER iu
ON 
	um.user_email = iu.UserEmail
WHERE
	um.user_id = '{$user_id}'
	";
$rs = 	$lpf->returnResultSET($sql);
$user = $rs[0];

$studentName = $user['Name'];
$studentClass = $user['ClassName'];
$studentNumber = $user['ClassNumber'];
######## Student Info END ########


######## Get data START ########
$phase_obj = $lgs->getPhaseInfo($phase_id);
$phase_obj = $phase_obj[0];

$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
######## Get data END ########


######## Decode Ansersheet START ########
$answersheetArr = explode('#QUE#', $phase_obj['answersheet']);
array_shift($answersheetArr);
$questionArr = array();
$categoryArr = array();
$subcategoryArr = array();

$lastCategoryId = -1;
$lastSubCategoryId = -1;
for($i=0,$iMax=count($answersheetArr);$i<$iMax;$i++){
	$_questions = explode('||', $answersheetArr[$i]);
	$typeNum = explode(',', $_questions[0]); // $typeNum[0]: Question Type , $typeNum[>0]: Number of options
	$title = $_questions[1];
	$options = $_questions[2];
	
	if($typeNum[0] == '8'){ // Category
		$lastCategoryId++;
		$categoryArr[$lastCategoryId] = $title;
		continue;
	}else if($typeNum[0] == '6'){ // Sub-Category, Options
		$lastSubCategoryId++;
		$subcategoryArr[$lastSubCategoryId] = $title;
		$optionsArr = explode('#OPT#', $options);
		array_shift($optionsArr); // Remove the first(empty) item
		
		$itmArr = array();
		$optArr = array();
		for($j=0, $jMax=count($optionsArr);$j<$jMax;$j++){ // item
			if( ($j) < $typeNum[1]){
				$itmArr[] = $optionsArr[$j];
			}else{
				$optArr[] = $optionsArr[$j];
			}
		}
		
		$questionArr[$lastCategoryId][$lastSubCategoryId] = array(
			'items' => $itmArr,
			'options' => $optArr,
		);
	}
}
######## Decode Ansersheet END ########


######## Decode Anser START ########
$_answerArr = explode('#ANS#', $handin_obj['answer']);
array_shift($_answerArr);
$answerArr = array();

$lastCategoryId = -1;
$lastSubCategoryId = -1;
for($i=0,$iMax=count($_answerArr);$i<$iMax;$i++){
	if($_answerArr[$i] == ''){
		$lastCategoryId++;
		continue;
	}else{
		$lastSubCategoryId++;
		
		if(strcasecmp($_answerArr[$i], 'NULL') == 0){
			$optionCount = count($questionArr[$lastCategoryId][$lastSubCategoryId]['items']);
			for($j=0;$j<$optionCount;$j++){
				$answerArr[$lastCategoryId][$lastSubCategoryId][] = -1;
			}
		}else{
			$answerArr[$lastCategoryId][$lastSubCategoryId] = explode(',', $_answerArr[$i]);
		}
	}
}
######## Decode Anser END ########


######## PDF Settings START ########
$margin_top = '7';
$mPDF = new mPDF('','A4',0,'',15,15,5,5);
$mPDF->mirrorMargins = 1;
$mPDF->SetColumns(2);

$header = "
	學生姓名：<span style=\"text-decoration: underline;\">&nbsp;{$studentName}&nbsp;</span>&nbsp;&nbsp;&nbsp;
	班別：<span style=\"text-decoration: underline;\">&nbsp;{$studentClass}&nbsp;</span>&nbsp;&nbsp;&nbsp;
	學號：<span style=\"text-decoration: underline;\">&nbsp;{$studentNumber}&nbsp;</span>
";
$mPDF->SetHTMLHeader("<div style=\"width:100%; text-align:center;\">{$header}</div>",'E');
$mPDF->SetHTMLHeader("<div style=\"width:100%; text-align:center;\">{$header}</div>",'O');

$footer = '(1) 未能做到&nbsp;&nbsp;&nbsp;(2) 偶爾能做到&nbsp;&nbsp;&nbsp;(3) 間中能做到&nbsp;&nbsp;&nbsp;(4) 經常能做到&nbsp;&nbsp;&nbsp;(5) 完全做到';

$mPDF->SetHTMLFooter("
	<div style=\"width:90%; text-align:center;float:left;\">{$footer}</div>
	<span style=\"float:right\">第 {PAGENO} 頁</span>
",'O');
$mPDF->SetHTMLFooter("
	<div style=\"width:90%; text-align:center;float:left;\">{$footer}</div>
	<span style=\"float:right\">第 {PAGENO} 頁</span>
",'E');
######## PDF Settings END ########


######## Output START ########
$row = -1;
$rowPerPage = 39;
$hasNewColumn = false;

ob_start();
?>
<style>
	body{
		font-family: msjh !important;
	}
	.alignLeft{
		width: 80mm;
	}
	
	.categoryRow{
		border-bottom: 2 solid black;
	}
	.category{
		font-weight: bold;
	}
	
	.subCategoryRow{
		width: 100%;
		padding-left: 3mm;
		border-bottom: 1 solid black;
	}
	
	.itemRow{
		padding-left: 6mm;
		border-bottom: 1 solid black;
	}
</style>

<br />
<?php 
foreach ($questionArr as $CategoryId=>$d1){ 
	if($row++ == $rowPerPage){
		echo '<columnbreak /><br /><br />';
		$row = 1;
		$hasNewColumn = true;
	}else{
		echo '<br />';
		$row++;
	}
?>

	<!-- ######## Category START ######## -->
	<table class="categoryRow">
		<tr>
			<td class="alignLeft category"><?=$categoryArr[$CategoryId]?></td>
			<td class="alignRight" style="white-space: nowrap">表現</td>
		</tr>
	</table>
	<!-- ######## Category END ######## -->

	<!-- ######## SubCategory START ######## -->
	<?php 
		foreach ($d1 as $SubCategoryId=>$question){ 
			if($row++ == $rowPerPage){
				echo '<columnbreak /><br /><br />';
				$row = 1;
				$hasNewColumn = true;
			}
			$items = $question['items'];
			$options = $question['options'];
	?>
		<table class="subCategoryRow">
			<tr>
				<td>
					<div><?=$subcategoryArr[$SubCategoryId]?></div>
				</td>
			</tr>
		</table>
	<!-- ######## SubCategory END ######## -->
	
		<?php 
		foreach ($items as $index=>$item){ 
			if($row++ == $rowPerPage){
				echo '<columnbreak /><br /><br />';
				$row = 1;
				$hasNewColumn = true;
			}
		?>
		<!-- ######## Item START ######## -->
			<table class="itemRow">
				<tr>
					<td class="alignLeft"><?=$item?></td>
					<td class="alignRight">
					<?php 
					if($answerArr[$CategoryId][$SubCategoryId][$index] > -1){
						echo ($answerArr[$CategoryId][$SubCategoryId][$index] + 1);
					}else{
						echo '';
					}
					?>
					</td>
				</tr>
			</table>
		<!-- ######## Item END ######## -->
		
		<?php 
		} 
		?>
	<?php 
	} 
	?>
<?php 
} 

if(!$hasNewColumn){ // If don't echo these <br .>, the rows will balanced. 
	for($i=0;$i<$rowPerPage;$i++){
		echo '<br />&nbsp;';
	}
}

?>



<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->Output();


exit;
####################### END ########################
?>

........................THE CODE BELOW ARE OLD CODE AND WILL NOT USE......................



<style>
	div{
		width: 100%;
		border: 1;
	}
	.alignLeft{
		color:blue;
		float:left;
		width: 70%;
	}
	.alignRight{
		color:red;
		float:right;
		width: 20%;
	}
</style>

<?php foreach ($questionArr as $CategoryId=>$d1){ ?>

	<!-- ######## Category START ######## -->
	<div class="categoryRow">
		<div class="alignLeft"><?=$categoryArr[$CategoryId]?></div>
		<div class="alignRight">abc</div>
	</div>
	<!-- ######## Category END ######## -->

	<!-- ######## SubCategory START ######## -->
	<?php 
		foreach ($d1 as $SubCategoryId=>$question){ 
			$items = $question['items'];
			$options = $question['options'];
	?>
		<div class="subCategoryRow">
			<div><?=$subcategoryArr[$SubCategoryId]?></div>
		</div>
	<!-- ######## SubCategory END ######## -->
	
		<?php foreach ($items as $index=>$item){ ?>
		<!-- ######## Item START ######## -->
			<div class="itemRow">
				<div class="alignLeft"><?=$item?></div>
				<div class="alignRight">
					<?php 
					if($answerArr[$CategoryId][$SubCategoryId][$index] > -1){
						echo ($answerArr[$CategoryId][$SubCategoryId][$index] + 1);
					}else{
						echo '';
					}
					?>
				</div>
			</div>
		<!-- ######## Item END ######## -->
		
		<?php } ?>
	<?php } ?>
<?php } ?>

<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->Output();


exit;
####################### END ########################
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style>
		.subCatRow > td > span{
			color:red;
		}
	</style>
</head>
<body>
<?php foreach ($questionArr as $CategoryId=>$d1){ ?>
	<table border="1" style="width: 100%">
		<tr>
			<th>
				<span><?=$categoryArr[$CategoryId]?></span>
			</th>
			<th style="width: 15mm">
				<span>&nbsp;</span>
			</th>
		</tr>
		
		<?php 
		foreach ($d1 as $SubCategoryId=>$question){ 
			$items = $question['items'];
			$options = $question['options'];
		?>
			<tr class="subCatRow">
				<td>
					<span><?=$subcategoryArr[$SubCategoryId]?></span>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<?php foreach ($items as $index=>$item){ ?>
				<tr class="itemRow">
					<td>
						<span><?=$item?></span>
					</td>
					<td>
						<span>
							<?php 
							if($answerArr[$CategoryId][$SubCategoryId][$index] > -1){
								echo ($answerArr[$CategoryId][$SubCategoryId][$index] + 1);
							}else{
								echo '';
							}
							?>
						</span>
					</td>
				</tr>
			<?php } ?>
		<?php 
		} 
		?>
	</table>
<?php } ?>
</body>
</html>
<?php
$html = ob_get_clean();
$mPDF->WriteHTML($html);
$mPDF->Output();
?>