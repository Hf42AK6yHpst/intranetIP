<?php
#  Editing by 

/*****************************************************
 * Modification Log
 * 
 * 	2017-07-07	(Bill)	[2016-0822-1721-00164]
 * 	- modified getExtraInfoDBTable(), getExtraInfoDBTableSql(), to handle School Chop when $eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop'] is set
 * 
 *****************************************************/
 
if (!defined("LIBREPORTCARD_TEACHER_EXTRA_INFO"))         // Preprocessor directives
{
	define("LIBREPORTCARD_TEACHER_EXTRA_INFO", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	class libreportcard_teacherExtraInfo extends libdbobject {
		private $lreportcard;
		
		private $recordId;
		private $userId;
		private $signaturePath;
		private $schoolChopPath;
		private $teacherTitle;
		private $dateInput;
		private $inputBy;
		private $dateModified;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			$this->lreportcard = new libreportcard();
			parent::__construct($this->lreportcard->DBName.'.RC_TEACHER_EXTRA_INFO', 'RecordID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setRecordId($val) {
			$this->recordId = $val;
		}
		public function getRecordId() {
			return $this->recordId;
		}
		
		public function setUserId($val) {
			$this->userId = $val;
		}
		public function getUserId() {
			return $this->userId;
		}
		
		public function setSignaturePath($val) {
			$this->signaturePath = $val;
		}
		public function getSignaturePath() {
			return $this->signaturePath;
		}
		
		public function setSchoolChopPath($val) {
			$this->schoolChopPath = $val;
		}
		public function getSchoolChopPath() {
			return $this->schoolChopPath;
		}
		
		public function setTeacherTitle($val) {
			$this->teacherTitle = $val;
		}
		public function getTeacherTitle() {
			return $this->teacherTitle;
		}
		
		public function setDateInput($val) {
			$this->dateInput = $val;
		}
		public function getDateInput() {
			return $this->dateInput;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setDateModified($val) {
			$this->dateModified = $val;
		}
		public function getDateModified() {
			return $this->dateModified;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('SignatureID', 'int', 'setSignatureId', 'getSignatureId');
			$fieldMappingAry[] = array('UserID', 'int', 'setUserId', 'getUserId');
			$fieldMappingAry[] = array('SignaturePath', 'str', 'setSignaturePath', 'getSignaturePath');
			$fieldMappingAry[] = array('SchoolChopPath', 'str', 'setSchoolChopPath', 'getSchoolChopPath');
			$fieldMappingAry[] = array('TeacherTitle', 'str', 'setTeacherTitle', 'getTeacherTitle');
			$fieldMappingAry[] = array('DateInput', 'date', 'setDateInput', 'getDateInput');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('DateModified', 'date', 'setDateModified', 'getDateModified');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		public function newRecordBeforeHandling() {
			$this->setDateInput('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setDateModified('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		
		
		public function returnSignatureImage($width='') {
			return $this->returnImage($this->getSignaturePath(), $width);
		}
		
		public function returnSchoolChopImage($width='') {
			return $this->returnImage($this->getSchoolChopPath(), $width);
		}
		
		private function returnImage($link, $width) {
			$image = '';
			if ($link=='') {
				$image = '&nbsp;';
			}
			else {
				if ($width=='') {
					$width = '150px';
				}
				
				$ts = strtotime(date('Y-m-d H:i:s'));
				$image = '<img src="'.$link.'?ts='.$ts.'" width="'.$width.'">';
			}
			return $image;
		}
		
		public function deleteSignatureImage() {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			$successAry['deleteImage'] = $this->deleteImage($this->getSignaturePath());
			
			$this->setSignaturePath('');
			$successAry['savePath'] = $this->save();
			
			return in_array(false, $successAry)? false : true;
		}
		
		public function deleteSchoolChopImage() {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			$successAry['deleteImage'] = $this->deleteImage($this->getSchoolChopPath());
			
			$this->setSchoolChopPath('');
			$successAry['savePath'] = $this->save();
			
			return in_array(false, $successAry)? false : true;
		}
		
		private function deleteImage($path) {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			if ($path == '') {
				$successAry['noLink'] = true;
			}
			else {
				include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
				$fs = new libfilesystem();
				
				$orignialPath = $intranet_root.$path;
				$newPath = $intranet_root.$path.'.'.date('Ymd_His').'.'.$_SESSION['UserID'];
				$successAry['renameFile'] = $fs->file_rename($orignialPath, $newPath);
			}
			
			return in_array(false, $successAry)? false : true;
		}
		
		
		public function uploadSignatureImage($fileLocation, $fileName) {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			$path = $this->uploadImage($fileLocation, $fileName, 'imageSignature');
			if ($path == ''){
				$successAry['uploadImage'] = false;
			}
			else {
				$successAry['uploadImage'] = true;
				
				$this->setSignaturePath($path);
				$successAry['savePath'] = $this->save();
			}
			
			return in_array(false, $successAry)? false : true;
		}
		
		public function uploadSchoolChopImage($fileLocation, $fileName) {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			$path = $this->uploadImage($fileLocation, $fileName, 'imageSchoolChop');
			if ($path == ''){
				$successAry['uploadImage'] = false;
			}
			else {
				$successAry['uploadImage'] = true;
				
				$this->setSchoolChopPath($path);
				$successAry['savePath'] = $this->save();
			}
			
			return in_array(false, $successAry)? false : true;
		}
		
		private function uploadImage($fileLocation, $fileName, $folderName) {
			global $PATH_WRT_ROOT, $intranet_root;
			
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$fs = new libfilesystem();
				
			$successAry = array();
			
			### Create Folder
			$dbFolderPathPrefix = '/file/reportcard2008/'.$this->lreportcard->schoolYear;
			$fullFolderPathPrefix = $intranet_root.$dbFolderPathPrefix;
			if (!file_exists($fullFolderPathPrefix)) {
				$successAry['createFolder1'] = $fs->folder_new($fullFolderPathPrefix);
			}
			$dbFolderPathPrefix .= '/'.$folderName;
			$fullFolderPathPrefix .= '/'.$folderName;
			if (!file_exists($fullFolderPathPrefix)) {
				$successAry['createFolder2'] = $fs->folder_new($fullFolderPathPrefix);
			}
			
			$ext = get_file_ext($fileName);
			$newFileName = $this->getUserId().$ext;
			$successAry['copyFile'] = $fs->file_copy($fileLocation, $fullFolderPathPrefix.'/'.$newFileName);
			
			return in_array(false, $successAry)? '' : $dbFolderPathPrefix.'/'.$newFileName;
		}
		
		public function returnInfoByTeacherId($teacherIdAry) {
			$RC_TEACHER_EXTRA_INFO = $this->getTableName();
			$sql = "Select
							RecordID
					From
							$RC_TEACHER_EXTRA_INFO
					Where
							UserID In ('".implode("','", (array)$teacherIdAry)."')
					";
			return $this->objDb->returnResultSet($sql);
		}
		
		public function getExtraInfoDBTable($field='',$order='',$page='',$Keyword='', $FilterSignature='', $FilterSchoolChop='') {
			global $PATH_WRT_ROOT, $Lang, $eReportCard, $page_size, $ck_reportcard_settings_teacherExtraInfo_page_size, $eRCTemplateSetting;
			
			include_once($PATH_WRT_ROOT."includes/libdbtable.php");
			include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
			
			$field = ($field=='')? 0 : $field;
			$order = ($order=='')? 1 : $order;
			$page = ($page=='')? 1 : $page;
	
			if (isset($ck_reportcard_settings_teacherExtraInfo_page_size) && $ck_reportcard_settings_teacherExtraInfo_page_size != "") {
				$page_size = $ck_reportcard_settings_teacherExtraInfo_page_size;
			}
			$li = new libdbtable2007($field,$order,$page);
			
			$sql = $this->getExtraInfoDBTableSql($Keyword, $FilterSignature, $FilterSchoolChop);
			
			$li->sql = $sql;
			$li->IsColOff = "IP25_table";
			
			$li->field_array = array("UserLogin", "NameField", "SignatureLink", "SchoolChopLink");
			$li->column_array = array(0,0);
			$li->wrap_array = array(0,0);
			
			$pos = 0;
			$li->column_list .= "<th width='30px' class='tabletoplink'>#</td>\n";
			$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['AccountMgmt']['LoginID'])."</td>\n";
			$li->column_list .= "<th>".$li->column_IP25($pos++, $Lang['AccountMgmt']['UserName'])."</td>\n";
			$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $eReportCard['JobTitle'])."</td>\n";
			$li->column_list .= "<th width='150px'>". $Lang['AccountMgmt']['Signature']."</td>\n";
			if($eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
				$pos--;
			}
			else {
				$li->column_list .= "<th width='150px'>". $eReportCard['Template']['SchoolChop']."</td>\n";
			}
			$li->column_list .= "<th width='30px'>".$li->check("userIdAry[]")."</td>\n";
			$li->no_col = $pos+4;
		
			$x .= $li->display();

			$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
			$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
			$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
			$x .= '<input type="hidden" name="page_size_change" value="" />';
			$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
		
			return $x;
				
		}
		
		private function getExtraInfoDBTableSql($Keyword='', $FilterSignature='', $FilterSchoolChop='') {
			global $eRCTemplateSetting;
			
			//$NameField = getNameFieldByLang("iu.");
			$NameField = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');
			
			if(trim($Keyword)!='') {
				$Keyword = $this->lreportcard->Get_Safe_Sql_Like_Query($Keyword);
				$cond_Keyword = " 
					AND 
					(
						iu.UserLogin LIKE '%$Keyword%'
						Or iu.EnglishName LIKE '%$Keyword%'
						Or iu.ChineseName LIKE '%$Keyword%' 
						Or tei.TeacherTitle LIKE '%$Keyword%'
					)
				";
			}
			
			if($FilterSignature != '') {
				if ($FilterSignature == 'uploaded') {
					$cond_FilterSignature = " AND (tei.SignaturePath IS NOT NULL AND TRIM(tei.SignaturePath) <> '') ";
				}
				else if ($FilterSignature == 'notUploaded') {
					$cond_FilterSignature = " AND (tei.SignaturePath IS NULL OR TRIM(tei.SignaturePath) = '') ";
				}
			}
			
			if($FilterSchoolChop != '' && !$eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
				if ($FilterSchoolChop == 'uploaded') {
					$cond_FilterSchoolChop = " AND (tei.SchoolChopPath IS NOT NULL AND TRIM(tei.SchoolChopPath) <> '') ";
				}
				else if ($FilterSchoolChop == 'notUploaded') {
					$cond_FilterSchoolChop = " AND (tei.SchoolChopPath IS NULL OR TRIM(tei.SchoolChopPath) = '') ";
				}
			}
			
			$SchoolChopField = "";
			if(!$eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
				$SchoolChopField = "IF(tei.SchoolChopPath IS NULL OR TRIM(tei.SchoolChopPath) = '',
										'&nbsp;',
										CONCAT('<img src=\"',tei.SchoolChopPath,'?ts=', CURRENT_TIMESTAMP(), '\" width=\"150px\">') 
									) AS SchoolChopPath,";
			}
			
			$RC_TEACHER_EXTRA_INFO = $this->getTableName();
			$sql = "SELECT
						CONCAT('<a class=\"tablelink\" href=\"edit.php?teacherId=', iu.UserID, '\">', iu.UserLogin, '</a>') as UserLoginLink,
						$NameField AS NameField,
						tei.TeacherTitle,
						IF(tei.SignaturePath IS NULL OR TRIM(tei.SignaturePath) = '',
							'&nbsp;',
							CONCAT('<img src=\"',tei.SignaturePath,'?ts=', CURRENT_TIMESTAMP(), '\" width=\"150px\">') 
						) AS SignaturePath,
						$SchoolChopField
						CONCAT('<input type=\"checkbox\" class=\"userIdSel\" name=\"userIdAry[]\" value=\"',iu.UserID,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\">') AS CheckBox,
						iu.UserLogin
					FROM
						INTRANET_USER iu
						LEFT OUTER JOIN $RC_TEACHER_EXTRA_INFO tei ON iu.UserID = tei.UserID
					WHERE
						iu.RecordType = '".USERTYPE_STAFF."'
						AND iu.RecordStatus = 1
						$cond_Keyword
						$cond_FilterSignature
						$cond_FilterSchoolChop
					";
			return $sql;
		}
	} // end of class libreportcard_extrainfo
} // end of Preprocessor directives
?>