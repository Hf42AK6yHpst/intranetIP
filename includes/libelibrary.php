<?php
// using by:
// * Please save in utf8 *
/**
 * Log:
 *
 * 2019-09-25 (Cameron)
 *  - modify GET_RECOMMEND_BOOK(), allow parent to view recommend books because kis parent need such function [case #F171071]
 * 2018-12-11 (Pun) [ip.2.5.10.1.1]
 *  - Modified getBookInformation(), GET_BOOK_OVERVIEW_SQL(), GET_BOOK_DETAILS(), getBookDetail(), getBookInformation(),
 *      getFieldName(), getNormalSearch(), displayBookDetail(), printSearchInput(), added ISBN field
 * 2018-12-03 (Cameron)
 *  - don't show expired ebooks in GET_RECOMMEND_BOOK() [case #C152289]
 * 2018-09-27 (Cameron)
 *  - add function updateBookReview() [case #W139304]
 * 2018-01-30 (Cameron)
 * 	- retrieve ItemSeriesNum in GET_PHYSICAL_BOOK_INFO() [case #L130405]
 * 2018-01-23 (Cameron)
 * 	- add functions used in iPortfolio, e.g. getStudentReadingSummaryUI()
 * 2017-12-12 (Cameron)
 * 	- add BookCategoryType in SYNC_PHYSICAL_BOOK_TO_ELIB() [case #F130026]
 * 2017-04-06 (Cameron)
 * 	- fix bug of apostrophe problem: apply Get_Safe_Sql_Query to returnTagIDByTagName() and insertTag()
 * 		limitation: tag string cannot contain comma as it's used as seperator
 * 	- modify returnPopularTags() to support apostrophy
 * 2017-03-10 (Cameron)
 *  - add function isSchooleBook(), modify isPurchasedeBook() so that it returns true if there's quotation or isSchooleBook) = true [case#S114395]
 * 2017-01-25 (Cameron)
 * 	- sync AccountDate to INTRANET_ELIB_BOOK in SYNC_PHYSICAL_BOOK_TO_ELIB()
 * 2017-01-16 (Cameron)
 * 	- add function isPurchasedeBook()
 * 2016-11-11 (Cameron)
 * 	- add function removeRecommendByBookID()
 * 2016-10-24 (Cameron)
 *	- add parameter $dateRange to GET_RECOMMEND_BOOK()
 * 2016-09-27 (Cameron)
 * 	- don't set user filter if UserID is empty in GET_RECOMMEND_BOOK(), to support opac
 * 2016-09-23 (Cameron)
 * 	- add parameter offset and with_total to GET_RECOMMEND_BOOK()
 *  - add parameter $publish to getBookIDByBookTitle()
 * 2016-09-20 (Cameron)
 * 	- add function removeRecommend(), getBookRecommendByID()
 * 2016-09-15 (Cameron)
 * 	- break to run at most 1000 records each time in SYNC_PHYSICAL_BOOK_TO_ELIB
 * 2016-07-08 (Cameron)
 *  - fix bug: correct field name from "RelevantSubject" to "Subject" in SYNC_PHYSICAL_BOOK_TO_ELIB()
 * 2016-06-28 (Cameron)
 *  - fix bug: apply pack_value function to Description field in addBookRecommend() and editBookRecommend() to support back slash
 * 2016-06-27 (Cameron)
 *  - add function getReviewByReviewID()
 * 2016-05-20 (Cameron)
 * 	- add parameter bookType(ebook/physical) in GET_RECOMMEND_BOOK(), to support new eLib+ portal
 * 2016-01-21 Paul
 * 	- add Customisation for internal site to display books without quotations
 * 2015-12-08 Paul
 * 	- add function getExpiredBookIDArr(), getActiveBookID()
 * 	- modified function newPrintBookTable(), GET_RECOMMEND_BOOK()
 * 2015-10-07 Cameron
 *	- add function getBookIDByBookTitle() and isBookTagExist()
 * 2015-09-15 Cameron
 * 	- add return value in function SYNC_BOOK_CATEGORY_TO_ELIB()
 * 2015-06-19 Cameron
 * 	- concate English and Chinese Book Category Name if $sys_custom['eLibraryPlus']['BookCategorySyncTwoLang'] = true in
 * 		SYNC_BOOK_CATEGORY_TO_ELIB() and SYNC_PHYSICAL_BOOK_TO_ELIB()
 * 2015-05-20 Cameron
 * 	- add function SYNC_BOOK_CATEGORY_TO_ELIB()
 * 2015-02-11 Henry
 * 	- Modified SYNC_PHYSICAL_BOOK_TO_ELIB() to add SubTitle field on the table and update publish = 0 if No of copy = 0
 * 2015-02-10 Ryan
 * 	- ej DM# 535 fixed Fail to save review with single quote addBookReview();
 * 2015-01-28 Henry
 * 	- Modified SYNC_PHYSICAL_BOOK_TO_ELIB() to search book category with type
 * 2014-11-20 Cameron
 * 	- Add return value to AssignTagToBooks()
 * 2013-11-07 Siuwan
 * - modify GET_PHYSICAL_BOOK_INFO() to get details of available and borrowed book items
 * 2013-10-16 Charles Ma
 * - edit GET_BOOK_OVERVIEW_SQL() for blank first page
 * 2013-08-23 Siuwan
 * - edit SYNC_PHYSICAL_BOOK_TO_ELIB() to combine CallNum and CallNum2 from LMS to eLibrary CallNumber
 * 2013-08-23 Jason
 * - edit SYNC_PHYSICAL_BOOK_TO_ELIB() to sync Series, ACNOList field from LMS to eLibrary
 * 2013-02-07 Charles Ma
 * - modify displayBookDetail to hidden the width applied on the preview image of book
 * 2013-01-28 CharlesMa
 * - modify GET_BOOK_DETAILS to retrieve ForceToOnePageMode & OnePageModeWidth
 * 2012-09-19 Yuen
 *  - distinguish eBook and physical book on BookID
 * 2012-04-25 Yuen
 *  - added Tag related functions
 * 2012-04-03 Carlos
 *  - added RelevantSubject to getBookInformation(), ADD_BOOK_INFO(), UPDATE_BOOK_INFO2()
 * 2012-03-12 Carlos
 *  - modified GET_BOOK_DETAILS(), left join INTRANET_EBOOK_BOOK, get fields ImageBook and ImageWidth
 *
 * 2012-02-27 yuen
 * 	define book formats and update it using SET_BOOK_FORMAT()
 *
 * 2010-07-28 Sandy:
 * 	- GET_RECOMMEND_BOOK(), update sql to refine search
 * 2010-1-20 Sandy : displayRecordListMenu() , new navigation style
 */

define("BOOK_FORMAT_EPUB2", "ePub2");
define("BOOK_FORMAT_EPUB3", "ePub3");
define("BOOK_FORMAT_IMAGE", "Image");

include_once($intranet_root."/includes/libfilesystem.php");
include_once($intranet_root."/includes/libuser.php");
class elibrary extends libdb
{
	/*
		For book conversion
	*/
	var $breaker;												// Chapter breaker
	var $FileName;											// Input file (.txt)
	var $Content;												// Content of the text file
	var $RowArr;												// Attay of page content
	var $BookTitle2;											// The title on each page
	var $IndexPage;											// The table of content page index
	var $FirstPage;											// The starting page index
	var $ChapterArr;											// Store the chapter information
	var $ChapterPrepageArr;								// Store the introduction information
	var $ChapterPageArr;									// Store the chapter to page index
	var $PageContentArr;									// Store the page information
	var $FileNameXML;										// Output content file (.xml)
	var $FileNameChapterXML;							// Output chapter file (.xml)
	var $BookType;											// Type of input books
	var $BottomLineSkip;									// Number of bottom lines to be skipped
	var $CheckIndent;										// Check the idents
	var $TitleIndenifier;										// Symbols for title
	var $SubTitleIndenifier;									// Symbols for sub title
	var $FootNotesType;									//Foot notes type
	var $FootNotesStr;										//Foot notes symbol
	var $FootNotesIndenifier;								// Symbols for foot notes

	var $Default_Copyright_CUP;							// cup copyright template
	var $Default_Copyright_Field_CUP;					// the number of field input in cup copyright template

	var $physical_book_init_value;
	var $physical_book_type_code;

	/*
	*	Constructor
	*/
	function elibrary($InputFile="", $InputType="")
	{
		$this->libdb();

		$this->breaker = "";
		if ($InputFile == "")
		{
			$this->FileName = "a64-1.txt";
		}
		else
		{
			$this->FileName = $InputFile;
		}
		if ($InputType == "")
		{
			$this->BookType = "1";
		}
		else
		{
			$this->BookType = $InputType;
		}

		$this->IndexPage = 4;
		$this->FirstPage = 1;
		$this->FileNameXML = "output.xml";
		$this->BottomLineSkip = 0;
		$this->CheckIndent = true;

		$this->FootNotesType = "1";
		$this->FootNotesStr = "　　 　";

		$this->TitleIndenifier = "***title***";
		$this->SubTitleIndenifier = "***sub_title***";
		$this->FootNotesIndenifier = "***foot_notes***";

		$this->ChapterArr = array();
		$this->ChapterPrepageArr = array();
		$this->ChapterPageArr = array();
		$this->PageContentArr = array();

		# to distinguish eBook and physical books
		$this->physical_book_init_value = 10000000;
		$this->physical_book_type_code = "physical";

	}
	function dbConnectionUTF8()
	{
		mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");
	}
	function dbConnectionLatin1()
	{
		mysql_query("set character_set_database='latin1'");
		mysql_query("set names latin1");
	}

	function SET_BOOK_FORMAT($BookID, $BookFormat)
	{
		$sql = "UPDATE INTRANET_ELIB_BOOK SET BookFormat='{$BookFormat}' WHERE BookID={$BookID} ";
		return $this->db_db_query($sql);
	}

	function GET_COPYRIGHT_TEMPLATE($source="cup")
	{
		if($source == "cup")
		{
			$this->Default_Copyright_Field_CUP = 5;

			$this->Default_Copyright_CUP .= "<b>CAMBRIDGE UNIVERSITY PRESS</b><br />";
			$this->Default_Copyright_CUP .= "Cambridge, New York, Melbourne, Madrid, Cape Town, Singapore, <br />S&#227;o Paulo, Delhi<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "Cambridge University Press<br />";
			$this->Default_Copyright_CUP .= "10 Hoe Chiang Road, #08-01/02, Keppel Towers, Singapore 089315<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "www.cambridge.org<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "Information on and purchase of this title: www.cambridge.org/<b>[JS1]</b><b>[/JS1]</b><br />or contact <u>hongkong@cambridge.org</u><br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "First published in print format <b>[JS2]</b><b>[/JS2]</b><br />";
			$this->Default_Copyright_CUP .= "&#169; Cambridge University Press <b>[JS3]</b><b>[/JS3]</b><br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "This publication is in copyright. Subject to statutory exception and to the provisions of relevant collective licensing agreements, no reproduction of any part may take place without the written permission of Cambridge University Press.<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "This electronic edition first published 2009<br />";
			$this->Default_Copyright_CUP .= "&#169; Cambridge University Press and BroadLearning Education 2009<br />";
			$this->Default_Copyright_CUP .= "<br />";
			$this->Default_Copyright_CUP .= "ISBN <b>[JS4]</b><b>[/JS4]</b>  (paperback)<br />";
			$this->Default_Copyright_CUP .= "ISBN <b>[JS5]</b><b>[/JS5]</b>  (paperback plus audio CD pack)<br />";

			return $this->Default_Copyright_CUP;
		}

		return "";
	}


	/*
	*	Set indent checking
	*/
	function CHECK_IDENT($Option)
	{
		$this->CheckIndent = $Option;
	}


	/*
	*	Set the book type
	*/
	function SET_BOOK_TYPE($InputType)
	{
		$this->BookType = $InputType;
	}

	/*
	*	Set the footnotes type
	*/
	function SET_FOOTNOTES_TYPE($InputType="",$InputStr="")
	{
		$this->FootNotesType = $InputType;
		$this->FootNotesStr = $InputStr;
	}

	/*
	*	Set the page title
	*/
	function SET_SUB_TITLE($PageTitle)
	{
		$this->BookTitle2 = $PageTitle;
	}

	/*
	*	Set the first page index
	*/
	function SET_FIRST_PAGE($FirstPage)
	{
		$this->FirstPage = $FirstPage;
	}


	/*
	*	Set the table of content index
	*/
	function SET_INDEX_PAGE($IndexPage)
	{
		$this->IndexPage = $IndexPage;
	}


	/*
	*	Set the file to be input
	*/
	function SET_INPUT_FILE($InputFile)
	{
		$this->FileName = $InputFile;
	}


	/*
	*	Set the file to be output
	*/
	function SET_OUTPUT_FILE($OutputFile)
	{
		$this->FileNameXML = $OutputFile;
	}


	/*
	*	Convert the text file into array
	*/
	function DO_CONVERSION()
	{
		$this->Content = file_get_contents($this->FileName);
		$this->RowArr = explode($this->breaker,$this->Content);

		for ($i=0;$i<count($this->RowArr);$i++)
		{

			$PageArr = explode("\n",$this->RowArr[$i]);
			if ($i==$this->IndexPage)
			{
				if ($this->BookTitle2=="")
				{
					$this->BookTitle2 = $PageArr[1];
				}
				$ChapterCnt = 0;

				for ($j=0;$j<count($PageArr);$j++)
				{
					$pos1 = strpos($PageArr[$j],".....");
					if ($pos1 === false)
					{
						continue;
					}
					$pos2 = strrpos($PageArr[$j],".....");
					if ($pos2 === false)
					{
						continue;
					}

					$ChapterName = substr($PageArr[$j],0,$pos1);
					$ChapterName = trim($ChapterName);
					$ChapterPage = substr($PageArr[$j],$pos2+1);
					$ChapterPage = trim($ChapterPage);

/*
					if ($ChapterCnt == 0)
					{
						$this->FirstPage = intval($ChapterPage);
					}
*/
					if (substr($ChapterName,0,strlen("　　"))=="　　")
					{
						$IsChapter = "0";
					}
					else
					{
						$IsChapter = "1";
					}
					$this->ChapterArr[] = array($ChapterName,$ChapterPage,$this->IndexPage+$this->FirstPage+intval($ChapterPage)-1,$IsChapter);
					$this->ChapterPageArr[] = $this->IndexPage+$this->FirstPage+intval($ChapterPage)-1;

					$ChapterCnt++;
				}
			}
			else if (($this->FirstPage != 0) && ($i > $this->IndexPage) && ($i < ($this->FirstPage+$this->IndexPage)))
			{
				for ($j=0;$j<count($PageArr);$j++)
				{
					$pos1 = strpos($PageArr[$j],".....");
					if ($pos1 === false)
					{
						continue;
					}
					$pos2 = strrpos($PageArr[$j],".....");
					if ($pos2 === false)
					{
						continue;
					}
					$ChapterName = substr($PageArr[$j],0,$pos1);
					$ChapterName = trim($ChapterName);
					$ChapterPage = substr($PageArr[$j],$pos2+1);
					$ChapterPage = trim($ChapterPage);

					if (substr($ChapterName,0,strlen("　　"))=="　　")
					{
						$IsChapter = "0";
					}
					else
					{
						$IsChapter = "1";
					}
					$this->ChapterArr[] = array($ChapterName,$ChapterPage,$this->IndexPage+$this->FirstPage+intval($ChapterPage)-1,$IsChapter);
					$this->ChapterPageArr[] = $this->IndexPage+$this->FirstPage+intval($ChapterPage)-1;

					$ChapterCnt++;
				}
			}
			else if ($i >= ($this->FirstPage+$this->IndexPage))
			{
				if (is_array($this->ChapterPageArr) && in_array($i,$this->ChapterPageArr))
				{
					$TmpChapterID = $this->CHAPTER_INDEX($i);
					$TmpChapterIDTitleArr = array();
					$TmpChapterIDSubTitleArr = array();


					$IsMultiChapter = false;
					if ($TmpChapterID && count($TmpChapterID)>1)
					{
						$IsMultiChapter = true;
						for ($j=0;$j <count($TmpChapterID);$j++)
						{
							if ($this->ChapterArr[$TmpChapterID[$j]][3] == "1")
								$TmpChapterIDTitleArr[] = trim(str_replace("　　","",$this->ChapterArr[$TmpChapterID[$j]][0]));
							else
								$TmpChapterIDSubTitleArr[] = trim(str_replace("　　","",$this->ChapterArr[$TmpChapterID[$j]][0]));
						}
					}


					$ContentStr = "";
					$FoundContent = false;
					$IsFootprint = false;

					for ($j=0;$j <count($PageArr);$j++)
					{
						if ($IsMultiChapter)
						{
							if ((trim($PageArr[$j]) !="") && count($TmpChapterIDTitleArr)>0 && in_array(trim($PageArr[$j]),$TmpChapterIDTitleArr))
							{
								$FoundContent = true;
								$ContentStr .= $this->TitleIndenifier.$PageArr[$j]."\n";
								continue;
							}
							else if ((trim($PageArr[$j]) !="") && count($TmpChapterIDSubTitleArr)>0 && in_array(trim($PageArr[$j]),$TmpChapterIDSubTitleArr))
							{
								$FoundContent = true;
								$ContentStr .= $this->SubTitleIndenifier.$PageArr[$j]."\n";
								continue;
							}
						}
						else
						{
							if (trim($PageArr[$j]) == trim($this->ChapterArr[$TmpChapterID][0]))
							{
								$FoundContent = true;
								$ContentStr .= $this->TitleIndenifier.$PageArr[$j]."\n";
								continue;
							}
						}

						if ($this->FootNotesType=="1")
						{
							if  ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) ||
								(trim(substr($PageArr[$j],0,strlen($this->FootNotesStr))) == trim($this->FootNotesStr)))
							{
								$IsFootprint = true;
							}
						}
						else if ($this->FootNotesType=="2")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}
						}
						else if ($this->FootNotesType=="3")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}
						}

						if ($FoundContent)
						{
							if (($PageArr[$j] == "") && ($ContentStr==""))
							{
								continue;
							}
							else if (trim($PageArr[$j]) == trim($this->BookTitle2))
							{
								continue;
							}
							else
							{
								if ($IsFootprint)
								{
									$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
								}
								else
									$ContentStr .= $PageArr[$j]."\n";
							}
						}
					}


					if (trim($ContentStr) =="")
					{
					$IsFootprint = false;
					for ($j=0;$j <count($PageArr);$j++)
					{
						if ($this->FootNotesType=="1")
						{
							if (substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr)
							{
								$IsFootprint = true;
							}
						}
						else if ($this->FootNotesType=="2")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}
						}
						else if ($this->FootNotesType=="3")
						{
							if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
							{
								$IsFootprint = true;
							}
						}

						if (($PageArr[$j] == "") && ($ContentStr==""))
						{
							continue;
						}
						else if (trim($PageArr[$j]) == trim($this->BookTitle2))
						{
							continue;
						}
						else
						{
							//$ContentStr .= $PageArr[$j]."\n";
							if ($IsFootprint)
							{
								$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
							}
							else
								$ContentStr .= $PageArr[$j]."\n";
						}
					}
					}

					if (trim($ContentStr) !="")
					{
						if ($IsMultiChapter)
							$this->PageContentArr[] = array($ContentStr,$TmpChapterID,$this->ChapterArr[$TmpChapterID[0]][0]);
						else
							$this->PageContentArr[] = array($ContentStr,$TmpChapterID,$this->ChapterArr[$TmpChapterID][0]);
					}

				}
				else
				{
					$ContentStr = "";
					$FoundContent = false;

					if (($this->FootNotesType=="1") || ($this->FootNotesType=="2"))
					{
						$IsFootprint = false;
					}

					for ($j=0;$j <count($PageArr);$j++)
					{
						if (trim($PageArr[$j]) == trim($this->BookTitle2))
						{
							$FoundContent = true;
							continue;
						}
						if ($FoundContent)
						{
							if (($PageArr[$j] == "") && ($ContentStr==""))
							{
								continue;
							} else {
								if ($this->FootNotesType=="1")
								{
									if (substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr)
									{
										$IsFootprint = true;
									}
								}
								else if ($this->FootNotesType=="2")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}
								}
								else if ($this->FootNotesType=="3")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}
								}

								if ($IsFootprint)
								{
									$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
								} else {
									$ContentStr .= $PageArr[$j]."\n";
								}
							}
						}
					}
					if (trim($ContentStr) =="")
					{
					$IsFootprint = false;
					for ($j=0;$j <count($PageArr);$j++)
					{
							if (($PageArr[$j] == "") && ($ContentStr==""))
							{
								continue;
							} else {
								if ($this->FootNotesType=="1")
								{
									if (substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr)
									{
										$IsFootprint = true;
									}
								}
								else if ($this->FootNotesType=="2")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}
								}
								else if ($this->FootNotesType=="3")
								{
									if ((substr($PageArr[$j],0,strlen($this->FootNotesStr)) == $this->FootNotesStr) && (trim($PageArr[$j-1])==""))
									{
										$IsFootprint = true;
									}
								}

								if ($IsFootprint)
								{
									$ContentStr .= $this->FootNotesIndenifier.$PageArr[$j]."\n";
								} else {
									$ContentStr .= $PageArr[$j]."\n";
								}
							}
					}
					}

					if (trim($ContentStr) !="")
					{
						$this->PageContentArr[] = array($ContentStr,$TmpChapterID,"");
					}
				}
			}
			else
			{
				$ContentStr = trim($this->RowArr[$i]);

				$TmpStr = "";
				$PageArr = explode("\n",$this->RowArr[$i]);
				for ($j=0;$j<count($PageArr);$j++)
				{
					if ((trim($PageArr[$j]) != trim($this->BookTitle2)) && (trim($this->BookTitle2)!=""))
					{
						$TmpStr .= $PageArr[$j]."\n";
					}
				}
				$this->ChapterPrepageArr[] = $TmpStr;
			}
		}
	} // end function do convertion

	/*
	*	Convert & output the file
	*/
	function DO_CONVERSION_CUP($ParArr="")
	{
		$xml_contents = $ParArr["xml_contents"];
		$xml_parser = $ParArr["xml_parser"];
		$Location3 = $ParArr["location_orig"];
		$Location4 = $ParArr["location_tmp_convert"];

		$xml_contents = str_replace("<emphasis>","&lt;emphasis&gt;",$xml_contents);
		$xml_contents = str_replace('<emphasis role="bold">',"&lt;emphasis&gt;",$xml_contents);
		$xml_contents = str_replace("</emphasis>","&lt;/emphasis&gt;",$xml_contents);
		$xml_contents = str_replace("<glossterm>","&lt;superscript&gt;",$xml_contents);
		$xml_contents = str_replace("</glossterm>","&lt;/superscript&gt;",$xml_contents);
		$xml_contents = str_replace("<superscript>","&lt;superscript&gt;",$xml_contents);
		$xml_contents = str_replace("</superscript>","&lt;/superscript&gt;",$xml_contents);
		$xml_contents = str_replace("<glosssee>","&lt;glosssee&gt;",$xml_contents);
		$xml_contents = str_replace("</glosssee>","&lt;/glosssee&gt;",$xml_contents);

		xml_parse_into_struct($xml_parser, $xml_contents, $temp_arr_vals);
		xml_parser_free($xml_parser);

		$Level = 0;
		$OpenArr = array();
		$OutputArr = array();
		$TmpArr = array();
		$PrevLevel = -1;
		$MaxLevel = -1;
		$CurrentChapter = 0;

		$OpenTag = "";
		$IsTagOpen = false;
		$ChapterCnt = 0;
		$PageCnt = 0;
		$ChapterPageCnt = 0;

		$IsPart = false;
		$InPartTab = false;
		$BufferPartLabel = "";
		$InChapterTab = false;

		$AllCdata = "";

		foreach($temp_arr_vals as $key=>$value)
		{
			if($value['type'] == "open")
			{
				if ($value['tag'] == "PART")
				{
					$IsPart = true;
					$InPartTab = true;
					//echo "<pre>open part tab, set InPartTab = true</pre>";
					//var_dump($InPartTab);
					$BufferPartLabel = $value['attributes']['LABEL'];
				}
				else if ($value['tag'] == "CHAPTER")
				{
					$InPartTab = false;
					$InChapterTab = true;
					//echo "<pre>open chapter tab, set InPartTab = false</pre>";
					//var_dump($InPartTab);
					$CurrentChapter = $value['attributes']['LABEL'];
					$ChapterCnt++;
					$PageCnt++;
					$ChapterPageCnt = 1;
				}
				else if ($value['tag'] == "PARA")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"cut");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "MSGMAIN")
				{
					$TmpArr = array("class"=>"text","val"=>"----------------------------------------------------------------------","page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"email");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "MSGTEXT")
				{
					$TmpArr = array("class"=>"text","val"=>"----------------------------------------------------------------------","page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"email");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
			}
			//start - handle the content after <cut /> within <para>bah bah bah <cut />bah bah bah</para>
			////////////////////////////////////////////////////////////////////////////////////////////////////
			else if ($value['type'] == "cdata")
			{
				if ($value['tag'] == "PARA")
				{
					$AllCdata .= $value['value']; //add all the text segments together until close tag
				}
			}
			else if ($value['type'] == "close")
			{
				if ($value['tag'] == "CHAPTER")
				{
					if($IsPart == true){
						$OutputArr[$CurrentChapter]["part_title"] = trim($BufferPartLabel);
						$IsPart = false;
						$BufferPartLabel = "";
					}else{
						$OutputArr[$CurrentChapter]["part_title"] = "null";
					}
				}
				else if ($value['tag'] == "PARA")
				{
					$TmpArr = array("class"=>"text","val"=>trim($AllCdata),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"tail");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
					$AllCdata = ""; //reset
				}
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////
			//end - handle the content after <cut /> within <para>bah bah bah <cut />bah bah bah</para>
			else if ($value['type'] == "complete")
			{
				//if ($value['tag'] == "PARTTITLE")
				//{
				//	if ($value["level"] == 3)
				//	{
				//		$BufferPartLabel = trim($BufferPartLabel)."  ".trim($value['value']);
				//	}
				//}
				if ($value['tag'] == "TITLE")
				{
					if($InPartTab == true){
						$BufferPartLabel = trim($BufferPartLabel)."  ".trim($value['value']);
						$InPartTab = false;
						//echo "<pre>complete title tab, set InPartTab = false</pre>";
						//var_dump($InPartTab);
					}else if($InChapterTab == true){
						//$OutputArr[$CurrentChapter]["title"] = "<bold>".trim($CurrentChapter)." </bold>"." ".trim($value['value']);
						$OutputArr[$CurrentChapter]["title"] = trim($CurrentChapter)."   ".trim($value['value']);
						$OutputArr[$CurrentChapter]["chapter_id"] = trim($ChapterCnt);
						$InChapterTab = false;
					}
				}
				else if ($value['tag'] == "PARA")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "PARAMETER")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"email");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "GLOSSENTRY")
				{
					$TmpArr = array("class"=>"text","val"=>trim($value['value']),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
					$TmpArr = array("class"=>"text","val"=>"","page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "GRAPHIC")
				{
					$TmpArr = array("class"=>"image","val"=>$value['attributes']['FILEREF'],"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"normal");
					$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
				}
				else if ($value['tag'] == "BREAK")
				{
					$ChapterPageCnt++;
					$PageCnt++;
				}
				else if ($value['tag'] == "CUT")
				{
					if($AllCdata != "") { //there is a long paragraph spread 3 pages!
						$TmpArr = array("class"=>"text","val"=>trim($AllCdata),"page_id"=>trim($PageCnt),"chapter_page_id"=>trim($ChapterPageCnt),"justify"=>"middle");
						$OutputArr[$CurrentChapter]["para"][] = $TmpArr;
						$AllCdata = ""; //reset
					}
					$ChapterPageCnt++;
					$PageCnt++;
				}
				else
				{}
			}
		} // end for loop

		//echo "<pre>";
		//var_dump($temp_arr_vals);
		//var_dump($OutputArr);
		//echo "</pre>";
		//die();

		$imageFolder = array("images/", "Images/");
		$outputFile = "output.xml";
		$chapterFile = "chapter.xml";
		$currentPage = 0;

		$x = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$y = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$y .= "<chapter>\n";

		$x .= "<body>\n";
		if (is_array($OutputArr) && (count($OutputArr)>0))
		{
			foreach ($OutputArr as $Key => $Value)
			{
				$TmpParaArr = $Value["para"];
				if (count($TmpParaArr)>0)
				{
					for ($i=0;$i<count($TmpParaArr);$i++)
					{
						if ($TmpParaArr[$i]["page_id"] != $currentPage)
						{
							$x .= "<a id=\"page_".trim($TmpParaArr[$i]["page_id"])."\" ></a>\n";
							if ($i==0) //first page of the chapter
							{
								if($Value["part_title"] != "null"){
									$x .= "<p class=\"part_title\">". trim($Value["part_title"])."</p>\n";
								}
								$x .= "<p class=\"chapter_title\">". trim($Value["title"])."</p>\n";
								if ($TmpParaArr[$i]["class"] == "text")
								{
									if($TmpParaArr[$i]["justify"] == "cut")
									{
										$x .= "<p class=\"noindent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else
									{
										$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
								}
								else if ($TmpParaArr[$i]["class"] == "image")
								{

									$x .= "<p class=\"image1\"><img src=\"".str_replace($imageFolder, "image/",trim($TmpParaArr[$i]["val"]))."\" alt=\"Image\"></img></p>\n";
								}

								if($Value["part_title"] != "null"){
									$modifyCid = -1*$Value["chapter_id"];
									$y .= "<title page=\"".$TmpParaArr[$i]["page_id"]."\" cid=\"".trim($modifyCid)."\" sid=\"0\" isChapter=\"2\" >".strip_tags(trim($Value["part_title"]))."</title>\n";
								}
								$y .= "<title page=\"".$TmpParaArr[$i]["page_id"]."\" cid=\"".trim($Value["chapter_id"])."\" sid=\"0\" isChapter=\"1\" >".strip_tags(trim($Value["title"]))."</title>\n";
							}
							else
							{
								if ($TmpParaArr[$i]["class"] == "text")
								{
									if($TmpParaArr[$i]["justify"] == "cut")
									{
										$x .= "<p class=\"indent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else if($TmpParaArr[$i]["justify"] == "tail")
									{
										$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else if($TmpParaArr[$i]["justify"] == "middle")
									{
										$x .= "<p class=\"noindent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else if($TmpParaArr[$i]["justify"] == "email")
									{
										$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
									else
									{
										$x .= "<p class=\"indent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
									}
								}
								else if ($TmpParaArr[$i]["class"] == "image")
								{
									$x .= "<p class=\"image1\"><img src=\"".str_replace($imageFolder, "image/",trim($TmpParaArr[$i]["val"]))."\" alt=\"Image\"></img></p>\n";
								}
							}
							$currentPage = $TmpParaArr[$i]["page_id"];
					}	// end if currentpage id
					else
					{
						if ($TmpParaArr[$i]["class"] == "text")
						{
							if($TmpParaArr[$i]["justify"] == "cut")
							{
								$x .= "<p class=\"indent\" cut=\"true\">". trim($TmpParaArr[$i]["val"])."</p>\n";
							}
							else if($TmpParaArr[$i]["justify"] == "email")
							{
								$x .= "<p class=\"noindent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
							}
							else
							{
								$x .= "<p class=\"indent\">". trim($TmpParaArr[$i]["val"])."</p>\n";
							}
						}
						else if ($TmpParaArr[$i]["class"] == "image")
						{
							$x .= "<p class=\"image1\"><img src=\"".str_replace($imageFolder, "image/",trim($TmpParaArr[$i]["val"]))."\" alt=\"Image\"></img></p>\n";
						}
					}
				} // end for each row
			} // end if each data
		} // end for each OutputArr
		} // end if outputArr > 0
		$x .= "</body>\n";
		$y .= "</chapter>\n";

		$x = str_replace("<glosssee>","",$x);
		$x = str_replace("</glosssee>","",$x);
		$x = str_replace("<glossterm>","&lt;superscript&gt;",$x);
		$x = str_replace("</glossterm>","&lt;/superscript&gt;",$x);
		//$x = str_replace("<glossterm>","&lt;font size=&apos;20&apos; face=&apos;GG Superscript&apos;&gt;", $x);
		//$x = str_replace("</glossterm>","&lt;/font&gt;",$x);
		$x = str_replace("<superscript>","&lt;superscript&gt;",$x);
		$x = str_replace("</superscript>","&lt;/superscript&gt;",$x);
		$x = str_replace("<bold>","&lt;b&gt;",$x);
		$x = str_replace("</bold>","&lt;/b&gt;",$x);
		$x = str_replace("<emphasis>","&lt;i&gt;",$x);
		$x = str_replace("</emphasis>","&lt;/i&gt;",$x);
		$x = str_replace('<p class="indent">* * *</p>', '<p class="indent-center">* * *</p>', $x);

		$handle = fopen($Location4."/".$outputFile, "w");
		fwrite($handle, $x);
		fclose($handle);

		$handle2 = fopen($Location4."/".$chapterFile, "w");
		fwrite($handle2, $y);
		fclose($handle2);
	} // end functio DO CONVERSION CUP

	/*
	*	Return the chapter id of a page
	*/
	function CHAPTER_INDEX($page)
	{
		$TmpArr = array();

		for ($i=0;$i<count($this->ChapterPageArr);$i++)
		{
			if (intval($this->ChapterPageArr[$i]) == intval($page))
			{

				$TmpArr[] = $i;

				//return $i;
				//break;
			}
		}

		if (count($TmpArr)==0)
		{
			return -1;
		}
		else if (count($TmpArr)==1)
		{
			return $TmpArr[0];
		}
		else
		{
			return $TmpArr;
		}

	}


	/*
	*	Convert the array into XML
	*/
	function OUTPUT_XML($ParPrePageArr="",$ParPageArr="",$OutputFile="Output.xml",$ImagePath="")
	{
		$ImageCnt = 0;
		$ImageCnt2 = 0;

		$ImageArr = $this->GET_IMAGE_ARRAY($ImagePath);

		$x = "";
		$x .= "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$x .= "<body>\n";
		for ($i=0;$i<count($ParPrePageArr);$i++)
		{

			$x .= "<a id=\"prepage_".($i+1)."\"  cid=\"-1\" ></a>\n";
			$ParPrePageArr[$i] = trim($ParPrePageArr[$i]);
			$CurLineContent = strtolower(trim($ParPrePageArr[$i]));

			if (($CurLineContent=="[image here]") || ($CurLineContent=="image") ||
				($CurLineContent=="image here") || ($CurLineContent=="[image here] ")
				 || (strstr($CurLineContent,"image here")) )
			{
				$ImageCnt++;

				$TmpImgName = "pre".$ImageCnt;
				if ($ImageArr[$TmpImgName] == "1")
				{
					$TmpImageName = "image/pre".$ImageCnt.".jpg";
					$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
				}
				else
				{
					for ($j=0;$j<$ImageArr[$TmpImgName];$j++)
					{
						$TmpImageName = "image/pre".$ImageCnt."_".($j+1).".jpg";
						$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
					}
				}
			}
			else
			{
				$TmpPageArr = explode("\n",$ParPrePageArr[$i]);
				// hard code to search pre heading
				//$tmpIntro = iconv("big5","utf-8//IGNORE", "");
				$tmpIntro = "";

				for ($j=0;$j<count($TmpPageArr);$j++)
				{
					if (trim($TmpPageArr[$j] != ""))
					{
						//$TmpStr = iconv("big5","utf-8//IGNORE",trim($TmpPageArr[$j]));
						$TmpStr = trim($TmpPageArr[$j]);

						if($j == 0 && $TmpStr == $tmpIntro)
						$x .= "<p class=\"into1_title\">".$TmpStr."</p>\n";
						else
						$x .= "<p class=\"into1\">".$TmpStr."</p>\n";
					}
				}
			}
		}
		for ($i=0;$i<count($ParPageArr);$i++)
		{
			$TmpPageID = $i+1;
			$TmpCurPage = $this->IndexPage+$this->FirstPage+$i;

			$TmpCh1 = $this->CHAPTER_INDEX($TmpCurPage);
			if (is_array($TmpCh1))
			{
				if ($TmpCh1 != -1)
				{
					$TmpCurPage2 = $TmpCh1[0];
					$IsStart = 1;
				}
				else
				{
					$IsStart = 0;
				}
			}
			else
			{
				if ($TmpCh1 != -1)
				{
					$TmpCurPage2 = $TmpCh1;
					$IsStart = 1;
				}
				else
				{
					$IsStart = 0;
				}
			}

			$x .= "<a id=\"page_".$TmpPageID."\" cid=\"".$TmpCurPage2."\" IsChapterStart=\"".$IsStart."\" ></a>\n";

			if ((trim($ParPageArr[$i][0])=="[image here]") || (trim($ParPageArr[$i][0])=="image") || (trim($ParPageArr[$i][0])=="image here"))
			{
				//$ImageCnt++;
				$TmpImageName = "image/p".$TmpPageID.".jpg";
				$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
			}
			else
			{
				/*
				if (trim($ParPageArr[$i][2]) != "")
				{
					$TmpStr = iconv("big5","utf-8//IGNORE",trim($ParPageArr[$i][2]));

					$x .= "<p class=\"chapter_title\">".$TmpStr."</p>\n";
				}
				*/
				$TmpPageArr = explode("\n", trim($ParPageArr[$i][0]));
				if ((trim($TmpPageArr[0])=="[image here]") || (trim($TmpPageArr[0])=="image") || (trim($TmpPageArr[0])=="image here"))
				{
					//$ImageCnt++;
					$TmpImageName = "image/p".$TmpPageID.".jpg";
					$x .= "<p class=\"image1\"><img src=\"{$TmpImageName}\" alt=\"Image\"></img></p>\n";
					continue;
				}
				$StillBlankBefore = true;
				for ($j=0;$j<count($TmpPageArr);$j++)
				{

					if ($j == count($TmpPageArr)-1)
					{
						/*
						if ((trim($TmpPageArr[$j]) != "") && (!is_nan(ceil(trim($TmpPageArr[$j])))) )
						{
							continue;
						}
						*/

						if ((trim($TmpPageArr[$j]) != "") && (strlen(trim($TmpPageArr[$j]))<=5) &&
							(trim($TmpPageArr[$j]) >="1")  && (trim($TmpPageArr[$j]) <="9999") )
						{

							continue;
						}

					}
//echo trim($TmpPageArr[$j])."<br />";
					if ((trim($TmpPageArr[$j]) != "") || (!$StillBlankBefore))
					{
						if (substr($TmpPageArr[$j],0,strlen($this->TitleIndenifier)) == $this->TitleIndenifier)
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim(substr($TmpPageArr[$j],strlen($this->TitleIndenifier))));
							$TmpStr = trim(substr($TmpPageArr[$j],strlen($this->TitleIndenifier)));
							$x .= "<p class=\"chapter_title\">".$TmpStr."</p>\n";
						}
						else if (substr($TmpPageArr[$j],0,strlen($this->SubTitleIndenifier)) == $this->SubTitleIndenifier)
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim(substr($TmpPageArr[$j],strlen($this->SubTitleIndenifier))));
							$TmpStr = trim(substr($TmpPageArr[$j],strlen($this->SubTitleIndenifier)));
							$x .= "<p class=\"sub_chapter_title\">".$TmpStr."</p>\n";
						}
						else if ((substr($TmpPageArr[$j],0,strlen($this->FootNotesIndenifier)) == $this->FootNotesIndenifier)
								&& ($this->FootNotesType!=""))
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim(substr($TmpPageArr[$j],strlen($this->FootNotesIndenifier))));
							$TmpStr = trim(substr($TmpPageArr[$j],strlen($this->FootNotesIndenifier)));
							$x .= "<p class=\"foot_notes\">".$TmpStr."</p>\n";
						}
						else
						{
							//$TmpStr = iconv("big5","utf-8//IGNORE",trim($TmpPageArr[$j]));
							$TmpStr = trim($TmpPageArr[$j]);
							if (!$this->CheckIndent)
							{
								$x .= "<p class=\"noindent\">".$TmpStr."</p>\n";
							}
							else
							{
//For checking
//echo $StillBlankBefore." | ".trim($ParPageArr[$i][2])." | ".trim($TmpPageArr[$j-1])." | ".trim($TmpStr)."<br />";
								if (($StillBlankBefore && (trim($ParPageArr[$i][2]) != "")) ||
									((!$StillBlankBefore) && (trim($TmpPageArr[$j-1]) == "") && (trim($TmpStr)!="")))
								{
									$x .= "<p class=\"indent\">".$TmpStr."</p>\n";
								}
								else
								{
									$x .= "<p class=\"noindent\">".$TmpStr."</p>\n";
								}
							}
						}
						$StillBlankBefore = false;
					}
				}
			}
//For checking
//die();
		}
		$x .= "</body>\n";


		$handle = fopen($OutputFile, "w");
		fwrite($handle, $x." \n");
		fclose($handle);

		return $x;
	}


	/*
	*	Convert the array into XML
	*/
	function OUTPUT_CHAPTER_XML($ParChapterArr="",$OutputFile="Chapter.xml")
	{
		$SubChapterCnt = 0;
		$CurChapter = 0;
		$CurPage = 0;

		$x = "";

		$x .= "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">\n";
		$x .= "<chapter>\n";
		for ($i=0;$i<count($ParChapterArr);$i++)
		{

			$TmpPage = $ParChapterArr[$i][1];
			$TmpTitle = $ParChapterArr[$i][0];
			if (substr($TmpTitle,0,strlen("　　"))=="　　")
			{
				$SubChapterCnt++;

				$TmpTitle = substr($TmpTitle,strlen("　　"));
				//$TmpTitle = iconv("big5","utf-8//IGNORE",trim($TmpTitle));
				$TmpTitle = trim($TmpTitle);

				$x .= "<title page=\"".$TmpPage."\" cid=\"".$CurChapter."\" sid=\"".$SubChapterCnt."\" isChapter=\"0\" >{$TmpTitle}</title>";
			}
			else
			{
				$CurChapter++;
				$SubChapterCnt = 0;

				//$TmpTitle = iconv("big5","utf-8//IGNORE",trim($TmpTitle));
				$TmpTitle = trim($TmpTitle);

				$x .= "<title page=\"".$TmpPage."\" cid=\"".$CurChapter."\" sid=\"".$SubChapterCnt."\" isChapter=\"1\" >{$TmpTitle}</title>";
			}
		}
		$x .= "</chapter>\n";

		$handle = fopen($OutputFile, "w");
		fwrite($handle, $x." \n");
		fclose($handle);

		return $x;
	}


	function MY_ECLASS_TABLE($class_icon, $RmTitle)
	{
		global $image_path, $LAYOUT_SKIN;

		$x  = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board01.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='26'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board02.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td width='19'>$class_icon</td>";
		$x .= "<td>$RmTitle</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board03.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='26'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board04.gif' width='10' height='11'></td>";
		$x .= "<td background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board05.gif'><table width='95%' border='0' cellpadding='5' cellspacing='0'>";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= $whatsnew;
		$x .="</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align='right' class='tabletext'>$lastlogin</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width='12' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board06.gif' width='12' height='12'></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width='10' height='10'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board07.gif' width='10' height='10'></td>";
		$x .= "<td height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board08.gif' width='10' height='10'></td>";
		$x .= "<td width='12' height='10' background='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/class_board09.gif' width='12' height='10'></td>";
		$x .= "</tr>";
		$x .= "</table>";

		return $x;
	}


	/*
	* Get MODULE_OBJ array
	*/
	function GET_MODULE_OBJ_ARR()
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path;
		### wordings
		global $eLib;
		global $CurrentPageArr;
		global $ip20TopMenu, $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;

		$CurrentPageArr['eLibrary'] = 1;

        switch ($CurrentPage)
        {
        	case "PageConvertBook":
        		$PageAdmin = 1;
            	$PageConvertBook = 1;
            	break;
        	case "PageImportBook":
        		$PageAdmin = 1;
            	$PageImportBook = 1;
            	break;
        	case "PageContentManage":
        		$PageAdmin = 1;
            	$PageContentManage = 1;
            	break;

        	case "PageContentView":
        		$PageAdmin = 1;
            	$PageContentView = 1;
            	break;

        }


		$MenuArr["Admin"] 	= array($eLib['ManageBook']["Title"],"", $PageAdmin);
		/*
		$MenuArr["Admin"]["Child"]["ConvertBook"] = array($eLib['ManageBook']["ConvertBook"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/convert_book.php", $PageConvertBook);
		$MenuArr["Admin"]["Child"]["ImportBook"] = array($eLib['ManageBook']["ImportBook"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/import_book.php", $PageImportBook);
		$MenuArr["Admin"]["Child"]["ContentManage"] = array($eLib['ManageBook']["ContentManage"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/index.php", $PageContentManage);
		$MenuArr["Admin"]["Child"]["ContentView"] = array($eLib['ManageBook']["ContentView"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/view.php", $PageContentView);
		*/
		$MenuArr["Admin"]["Child"]["ConvertBook"] = array($eLib['ManageBook']["ConvertBook"]);
		$MenuArr["Admin"]["Child"]["ImportBook"] = array($eLib['ManageBook']["ImportBook"], "", $PageImportBook);
		$MenuArr["Admin"]["Child"]["ContentManage"] = array($eLib['ManageBook']["ContentManage"], $PATH_WRT_ROOT."home/eLearning/elibrary/admin/index.php", $PageContentManage);
		$MenuArr["Admin"]["Child"]["ContentView"] = array($eLib['ManageBook']["ContentView"], "", $PageContentView);


		# module information
		$MODULE_OBJ['title_css']= "menu_opened";
		$MODULE_OBJ['title'] = $ip20TopMenu['eLibrary'];
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eLibrary.gif";
		$MODULE_OBJ['root_path'] = "#";
		$MODULE_OBJ['menu'] = $MenuArr;

		return $MODULE_OBJ;
	}

	function GET_MODULE_OBJ_ARR2()
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path;
		### wordings
		global $eLib;
		global $CurrentPageArr;
		global $ip20TopMenu, $access2readingrm, $access2elprm, $access2ssrm, $access2specialrm;

		$CurrentPageArr['eLibrary'] = 1;
		$CurrentPageArr['eLib'] = 1;

		# module information
		$MODULE_OBJ['title'] = $ip20TopMenu['eLibrary'];
		$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eLibrary.gif";
		$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eclass/index_elib.php";
		//$MODULE_OBJ['menu'] = $MenuArr;

		return $MODULE_OBJ;
	}

    function getPageTabTitle(){
        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eLibrary";'."\n";
        $js.= '</script>'."\n";

        return $js;
    }

	function HAS_RIGHT($ParArr="")
	{
		return true;
	}

	function IMPORT_BOOK_INFO($InputFile,$ParArr="")
	{
		$Type = $ParArr["Type"];
		if ($Type=="")
		{
			$Type="sys";
		}
		$AuthorArray = array();

		if ($InputFile != "")
		{
			$row = 1;
			$handle = fopen($InputFile, "r");
			if ($handle)
			{
				while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE)
				{
					if ($row !=1)
					{
					    $TmpTitle = trim($data[0]);
					    $TmpDocument = trim($data[1]);
					    $TmpAuthor = trim($data[2]);
					    $TmpPublisher = trim($data[3]);
					    $TmpPreface = trim($data[4]);
					    $TmpAuthorDesc = trim($data[5]);

					    if (trim($AuthorArray[$TmpAuthor]) == "")
					    {
						    $TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);
						    $AuthorArray[$TmpAuthor] = $TmpAuthorID;
					    }
					    else
					    {
						    $TmpAuthorID = $AuthorArray[$TmpAuthor];
					    }

					    $TmpArr = array();
					    $TmpArr["Title"] =  $TmpTitle;
					    $TmpArr["Author"] =  $TmpAuthor;
					    $TmpArr["AuthorID"] =  $TmpAuthorID;
					    $TmpArr["Publisher"] =  $TmpPublisher;
					    $TmpArr["Preface"] =  $TmpPreface;
					    $TmpArr["Document"] =  $TmpDocument;
					    $TmpArr["InputBy"] =  $Type;
						$TmpArr["Source"]  = $ParArr["Source"];
						$TmpArr["Language"]  = $ParArr["Language"];
						$TmpArr["Category"]  = $ParArr["Category"];


					    if ($TmpDocument != "")
					    {
							$this->UPDATE_BOOK_INFO($TmpArr);
						}
					}
				    $row++;
				}
				fclose($handle);
			}
		}
	} // end function

	function IMPORT_BOOK_INFO_FROM_CUP($InputFile,$ParArr="")
	{
		//echo "<pre>IMPORT_BOOK_INFO_FROM_CUP is called</pre>";

		$Type = $ParArr["Type"];
		if ($Type=="")
		{
			$Type="sys";
		}
		$AuthorArray = array();

		//echo "<pre>InputFile is";
		//var_dump($InputFile);
		//echo "</pre>";

		if ($InputFile != "")
		{
			$row = 1;
			$handle = fopen($InputFile, "r");

			if ($handle)
			{
				while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE)
				{
					//echo "<pre>data is";
					//var_dump($data);
					//echo "</pre>";

					//echo "<pre>row is";
					//var_dump($row);
					//echo "</pre>";

					if ($row != 1)
					{
						$TmpIndexNumber = trim($data[0]);
					    $TmpTitle = trim($data[1]);
					    $TmpDocument = trim($data[2]);
					    $TmpISBN = trim($data[3]);
					    $TmpAuthor = trim($data[4]);
					    $TmpPublisher = trim($data[5]);
					    $TmpSubCategory = trim($data[6]);
					    $TmpLevel = trim($data[7]);
					    $TmpAdultContent = trim($data[8]);
					    $TmpPreface = trim($data[9]);
					    $TmpAuthorDesc = "";

					     //debug_r($data);

					    if (trim($AuthorArray[$TmpAuthor]) == "")
					    {
						    //$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);
						    $AuthorArray[$TmpAuthor] = $TmpAuthorID;
					    }
					    else
					    {
						    $TmpAuthorID = $AuthorArray[$TmpAuthor];
					    }

					    if($ParArr["Category"] == "")
					    $TmpCategory = "Readers";
					    else
					    $TmpCategory = $ParArr["Category"];

					    if($TmpLevel != "")
					    $TmpLevel = $TmpLevel[0];

					    if($TmpSubCategory != "")
					    $TmpSubCategory = ucwords(strtolower($TmpSubCategory));


					    $TmpArr = array();
					    $TmpArr["Title"] =  htmlspecialchars($TmpTitle, ENT_QUOTES);
					    $TmpArr["Author"] =  htmlspecialchars($TmpAuthor, ENT_QUOTES);
					    $TmpArr["AuthorID"] =  $TmpAuthorID;
					    $TmpArr["Publisher"] =  $TmpPublisher;
					    $TmpArr["Preface"] =  htmlspecialchars($TmpPreface, ENT_QUOTES);
					    $TmpArr["Document"] =  htmlspecialchars($TmpDocument, ENT_QUOTES);
					    $TmpArr["InputBy"] =  $Type;
						$TmpArr["Source"]  = $ParArr["Source"];
						$TmpArr["Language"]  = $ParArr["Language"];
						$TmpArr["Category"]  = $TmpCategory;
						$TmpArr["SubCategory"] = $TmpSubCategory;
						$TmpArr["ISBN"] = $TmpISBN;
						$TmpArr["AdultContent"] = $TmpAdultContent;
						$TmpArr["Level"] = $TmpLevel;

 						//echo "<pre>TmpArr is";
						//var_dump($TmpArr);
						//echo "</pre>";
						//echo "</pre>TmpDocument is";
						//var_dump($TmpDocument);
						//echo "</pre>";
						//debug_r($TmpArr);

					    if ($TmpDocument != "")
					    {
						   // debug_r($TmpArr);
							$this->UPDATE_BOOK_INFO($TmpArr);
						}
					}
				    $row++;
				}
				fclose($handle);
			}
		}
		//die();
	} // end function

	function UPDATE_BOOK_PUBLISH($ParArr)
	{
		$BookIDArr = $ParArr["BookID"];

		$BookIDField .= "BookID = '".$BookIDArr[0]."'";

		for($i = 1; $i < count($BookIDArr); $i++)
		{
			$BookIDField .= " OR BookID = '".$BookIDArr[$i]."'";
		}

		$sql = "
		UPDATE
		INTRANET_ELIB_BOOK
		SET
		Publish='".$ParArr["Publish"]."'
		WHERE
		$BookIDField
		";

		$this->db_db_query($sql);
	} // end funciton UPDATE BOOK PUBLISH

	function UPDATE_BOOK_INFO($ParArr)
	{
		$Sql1 =  "
						SELECT
							BookID
						FROM
							INTRANET_ELIB_BOOK
						WHERE
							Document = '".$ParArr["Document"]."'
							AND InputBy = '".$ParArr["InputBy"]."'
					";
		$ReturnArr = $this->returnVector($Sql1);

		if (count($ReturnArr)>0)
		{
		//Edit currrent record
			$BookID = $ReturnArr[0];
			if (($ParArr["Title"] != "") && ($ParArr["Document"]!=""))
			{
				$Sql1 =  "
								UPDATE
									INTRANET_ELIB_BOOK
								SET
									Title='".$ParArr["Title"]."',
									Author='".$ParArr["Author"]."',
									AuthorID='".$ParArr["AuthorID"]."',
									Publisher='".$ParArr["Publisher"]."',
									Preface='".$ParArr["Preface"]."',
									Source='".$ParArr["Source"]."',
									Language='".$ParArr["Language"]."',
									Level='".$ParArr["Level"]."',
									Category='".$ParArr["Category"]."'
								WHERE
									BookID='{$BookID}'
							";	// Charles Ma 20131125-elibplus-oldbook  DateModified=now(),
				$this->db_db_query($Sql1);
			}
		}
		else
		{
		//Insert new record
			if (($ParArr["Title"] != "") && ($ParArr["Document"]!=""))
			{
				$Sql1 =  "
								INSERT INTO
									INTRANET_ELIB_BOOK
									(Title,Author,AuthorID,
									Publisher,Preface,Document,
									InputBy,DateModified,Source,Language,Level,
									Category, SubCategory, ISBN,
									AdultContent)
								VALUES
									('".$ParArr["Title"]."','".$ParArr["Author"]."','".$ParArr["AuthorID"]."',
									 '".$ParArr["Publisher"]."','".$ParArr["Preface"]."','".$ParArr["Document"]."',
									 '".$ParArr["InputBy"]."',now(),'".$ParArr["Source"]."','".$ParArr["Language"]."','".$ParArr["Level"]."',
									 '".$ParArr["Category"]."', '".$ParArr["SubCategory"]."', '".$ParArr["ISBN"]."',
									 '".$ParArr["AdultContent"]."')
							";

				$this->db_db_query($Sql1);
				$BookID = $this->db_insert_id();
			}
		}

		return $BookID;
	}


	/*
	*	edit_book_info
	*/
	function UPDATE_BOOK_INFO2($ParArr)
	{
		$BookID = $ParArr["BookID"];
		$Type = $ParArr["Type"];
		if ($ParArr["Title"] != "")
		{
			$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($ParArr["Author"],$description,$Type);
			if ($TmpAuthorID != $ParArr["AuthorID"])
			{
				$AuthorSQL = " Author='".$ParArr["Author"]."', AuthorID='".$TmpAuthorID."', ";
			}

			$Sql1 =  "
							UPDATE
								INTRANET_ELIB_BOOK
							SET
								Title='".$ParArr["Title"]."',
								$AuthorSQL
								SeriesEditor='".$ParArr["SeriesEditor"]."',
								Publisher='".$ParArr["Publisher"]."',
								Preface='".$ParArr["Preface"]."',
								Source='".$ParArr["Source"]."',
								Language='".$ParArr["Language"]."',
								Level='".$ParArr["Level"]."',
								ISBN='".$ParArr["ISBN"]."',
								SubCategory='".$ParArr["SubCategory"]."',
								Category='".$ParArr["Category"]."',
								RelevantSubject='".$ParArr["RelevantSubject"]."',
								Publish='".$ParArr["Publish"]."',
								Copyright='".$ParArr["Copyright"]."',
								AdultContent='".$ParArr["AdultContent"]."',
								FirstPublished='".$ParArr["FirstPublished"]."',
								ePublisher='".$ParArr["ePublisher"]."',
								CopyrightYear='".$ParArr["CopyrightYear"]."',
								CopyrightStatement='".$ParArr["CopyrightStatement"]."'
							WHERE
								BookID='{$BookID}'
						";// Charles Ma 20131125-elibplus-oldbook  DateModified=now(),

			$this->db_db_query($Sql1);
		}

		return $BookID;
	}



	function UPDATE_BOOK_AUTHOR($Author,$Description="",$Type)
	{
		$Sql1 =  "
						SELECT
							AuthorID
						FROM
							INTRANET_ELIB_BOOK_AUTHOR
						WHERE
							Author = '".trim($Author)."'
							AND InputBy='{$Type}'
					";
		$ReturnArr = $this->returnVector($Sql1);

		if (count($ReturnArr)>0)
		{
		//Edit currrent record
			$AuthorID = $ReturnArr[0];
			if (trim($Description) != "")
			{
				$Sql1 =  "
								UPDATE
									INTRANET_ELIB_BOOK_AUTHOR
								SET
									Description='".trim($Description)."',
									DateModified=now()
								WHERE
									AuthorID='{$AuthorID}'
							";
				$this->db_db_query($Sql1);
			}
		}
		else
		{
		//Insert new record
			$Sql1 =  "
							INSERT INTO
								INTRANET_ELIB_BOOK_AUTHOR
								(Author,Description,InputBy,DateModified)
							VALUES
								('".trim($Author)."','".trim($Description)."','{$Type}',now())
						";
			$this->db_db_query($Sql1);
			$AuthorID = $this->db_insert_id();
		}

		return $AuthorID;
	}


	function UPDATE_BOOK_AUTHORID($BookID,$AuthorID)
	{
		$Sql1 =  "
						UPDATE
							INTRANET_ELIB_BOOK_AUTHOR
						SET
							AuthorID='{$AuthorID}'
						WHERE
							BookID='{$AuthorID}'
					";
		$this->db_db_query($Sql1);

		return $AuthorID;
	}


	function GET_BOOK_OVERVIEW_SQL($ParArr="")
	{
		global $eLib, $Lang;
		global $i_QB_LangSelect,$i_QB_LangSelectChinese,$i_QB_LangSelectEnglish;

		// Field SQL
		$SourceField = "CASE Source ";
		if (is_array($eLib['Source']) && count($eLib['Source'])>0)
		{
		foreach($eLib['Source'] as $Key => $Value)
		{

			$SourceField .= " WHEN {$Key} THEN '{$Value} ' ";
		}
		}
		$SourceField .= " ELSE '' END ";

		# ".$eLib['Source']["cup"]." -> CUP to shorten the length
		//$SourceField = "IF (Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."')";
		$SourceField = " CASE Source WHEN 'green' THEN '".$eLib['Source']["green"]."'" .
				" WHEN 'cup' THEN '".$eLib['Source']["cup"]."'" .
						"  WHEN 'breakthrough' THEN '".$eLib['Source']["breakthrough"]."'" .
								"  WHEN 'hkcrown' THEN '".$eLib['Source']["hkcrown"]."'" .
										"  WHEN 'enrichculture' THEN '".$eLib['Source']["enrichculture"]."'" .
												"  WHEN 'rightman' THEN '".$eLib['Source']["rightman"]."' ELSE '--' END ";
		$LanguageField = "IF (Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."')";

		//Conditions
		$Cond = " WHERE 1 ";
		if ($ParArr["AuthorID"] != "")
		{
			$Cond .= " AND AuthorID='".$ParArr["AuthorID"]."' ";
		}
		if ($ParArr["Language"] != "")
		{
			$Cond .= " AND Language='".$ParArr["Language"]."' ";
		}
		if ($ParArr["Source"] != "")
		{
			$Cond .= " AND Source='".$ParArr["Source"]."' ";
		}
		if ($ParArr["Keyword"]!="")
		{
			$Cond .= " AND (Title LIKE '%".$ParArr["Keyword"]."%') ";
		}

		if ($ParArr["Link1"] != "")
		{
			$TitleField = "CONCAT('<a href=\"".$ParArr["Link1"]."?BookID=',BookID,' \" class=\"tablelink\"><img src=\"/images/icon_edit.gif\" border=\"0\" align=\"absmiddle\"  /> ',Title,'</a> <br /><a href=\"javascript:newWindow(\'/home/eLearning/elibrary/book_detail.php?BookID=',BookID,'\', 21) \" class=\"tablelink\"><img src=\"/images/2009a/eLibrary/icon_book.png\" border=\"0\" align=\"absmiddle\" /> [".$Lang['Btn']['Preview']."]</a>'),";
		} else
		{
			$TitleField = "Title,";
		}
		$AuthorField = "CONCAT('<a href=\"edit_book_author.php?BookID=',BookID,'&AuthorID=',AuthorID,' \" class=\"tablelink\">',Author,'</a>'),";

		//$PublishField = "IF (Publish='1','".$eLib["html"]["publish"]."', '".$eLib["html"]["unpublish"]."'),";
		$PublishField = "IF (Publish='1','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."'),";

		// 2013-10-16 Charles Ma
		if ($ParArr["IsFirstPageBlank"] != "")
		{
			$IsFirstPageBlankField = "IF (IsFirstPageBlank='1','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."'),";
		}
		// 2013-10-16 Charles Ma END

		// 20140224-twupdate1
		if ($ParArr["IsRightToLeft"] != "")
		{
			$IsRightToLeftField = "IF (IsRightToLeft='1','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."'),";
		}

		$Sql = "  SELECT
						SUBSTRING((BookID+100000000), 2), ISBN, {$TitleField}{$AuthorField} Publisher,{$SourceField},{$LanguageField}, DateModified,{$PublishField}{$IsFirstPageBlankField}{$IsRightToLeftField} CONCAT('<input type=\"checkbox\" name=\"BookID[]\" value=\"',BookID,'\" />')
					FROM
						INTRANET_ELIB_BOOK
					$Cond	AND BookID<".$this->physical_book_init_value."
				  ";
		// 20140224-twupdate1 END
		return  $Sql;
	}

	function GET_ALL_AUTHOR_ARRAY()
	{
		$Sql = "
					SELECT
						DISTINCT AuthorID, Author
					FROM
						INTRANET_ELIB_BOOK
				  ";
		$ReturnArr = $this->returnArray($Sql,2);

		$ReturnArr2 = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			//$ReturnArr2[] = array($ReturnArr[$i][0],iconv("UTF-8", "big5"."//IGNORE", $ReturnArr[$i][1]));
			$ReturnArr2[] = array($ReturnArr[$i][0],$ReturnArr[$i][1]);
		}
		return $ReturnArr2;
	}

	function GET_ALL_SOURCE_ARRAY()
	{
		global $eLib;

		$Sql = "
					SELECT
						DISTINCT Source
					FROM
						INTRANET_ELIB_BOOK
				  ";
		$ReturnArr = $this->returnVector($Sql);

		$ReturnArr2 = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			if ($eLib['Source'][$ReturnArr[$i]]!="")
				$ReturnArr2[] = array($ReturnArr[$i],$eLib['Source'][$ReturnArr[$i]]);
		}

		return $ReturnArr2;
	}

	function GET_BOOK_DETAILS($BookID)
	{
		global $eLib;

		// 2013-1-28 Charles Ma - modify the sql - add retrieve ForceToOnePageMode & OnePageModeWidth

		$Sql = "
					SELECT
						a.Title, a.Author, a.AuthorID, a.Publisher, a.Preface, a.Document, a.InputBy, a.DateModified, a.Source, a.Language, a.AdultContent,
						count(DISTINCT b.PageID) AS TotalPage, a.Category, a.Level, a.SeriesEditor, a.SubCategory, a.Publish, a.Copyright,  a.FirstPublished, a.ePublisher, a.CopyrightYear, a.CopyrightStatement, " .
								"c.ImageBook, c.ImageWidth, c.ForceToOnePageMode, c.OnePageModeWidth, c.InputFormat, a.ISBN
					FROM
						INTRANET_ELIB_BOOK a
							LEFT JOIN
						INTRANET_ELIB_BOOK_PAGE b
							ON a.BookID = b.BookID
						LEFT JOIN INTRANET_EBOOK_BOOK c
							ON c.BookID = a.BookID
					WHERE
						a.BookID='{$BookID}'
					GROUP BY
						 a.BookID
				  ";
		$ReturnArr = $this->returnArray($Sql,17);

		return $ReturnArr[0];
	}


	function GET_IMAGE_ARRAY($ImagePath="")
	{
		$TmpArray = array();
		if ($handle = opendir($ImagePath))
		{
		    while (false !== ($file = readdir($handle)))
		    {
				if ($file != "." && $file != "..")
		        {
			        list($filename,$extension) = explode(".",$file);
			        list($pid,$pcnt) = explode("_",$filename);

			        if ($TmpArray[$pid]=="")
			        {
				        $TmpArray[$pid] = 1;
			        }
			        else
			        {
				        $TmpArray[$pid]++;
			        }

		            //echo "$file\n";

		        }
		    }
		    closedir($handle);
		}

		return $TmpArray;
	}

	function IS_SCHOOL_ADMIN()
	{
		return true;
	}

	function IS_SYSTEM_ADMIN()
	{
		return true;
	}

	function GET_AUTHOR_DETAILS($AuthorID="")
	{
		if ($AuthorID != "")
		{
			$Cond = " WHERE AuthorID = '{$AuthorID}' ";
		}
		$Sql = "
					SELECT
						AuthorID, Author, Description, InputBy
					FROM
						INTRANET_ELIB_BOOK_AUTHOR
					$Cond
				  ";
		$ReturnArr = $this->returnArray($Sql,3);

		return $ReturnArr;
	}


	function GET_BOOK_PAGE_TYPE_ARRAY($BookID)
	{
		$Sql = "
					SELECT
						PageType, count(BookPageID) As PageTotal
					FROM
						INTRANET_ELIB_BOOK_PAGE
					WHERE
						BookID = '{$BookID}'
					GROUP BY
						PageType
				  ";
		$ReturnArr = $this->returnArray($Sql,2);
		$ReturnArr2 = array();
		for ($i=0;$i<count($ReturnArr);$i++)
		{
			$ReturnArr2[$ReturnArr[$i]["PageType"]] = $ReturnArr[$i]["PageTotal"];
		}

		return $ReturnArr2;
	}

	// 2013-11-14 Charles Ma 		2013-11-14-deleteallbookcomment
	// This function is for remove particular book review or all book review of particular book
	function REMOVE_BOOK_REVIEW($BookID, $ReviewID)
	{

		if($ReviewID>0 && $ReviewID!="")
		{
			$sql = "
				DELETE FROM
				INTRANET_ELIB_BOOK_REVIEW_HELPFUL
				WHERE
            	ReviewID = ".$ReviewID;
    		$this->db_db_query($sql);

			$sql = "
				DELETE FROM
				INTRANET_ELIB_BOOK_REVIEW
				WHERE
            	ReviewID = ".$ReviewID;

			return $this->db_db_query($sql);
		}
		else if ($BookID>0 && $BookID!=""){

			$sql = "
				DELETE FROM
				INTRANET_ELIB_BOOK_REVIEW_HELPFUL
				WHERE
            	BookID = ".$BookID;
    		$this->db_db_query($sql);

			$sql = "
				DELETE FROM
				INTRANET_ELIB_BOOK_REVIEW
				WHERE
            	BookID = ".$BookID;

			return $this->db_db_query($sql);
		}
		 else
		{
			return false;
		}
	}


	function REMOVE_BOOK_INFO($BookIDArr="")
	{

		global $intranet_root, $PATH_WRT_ROOT;

		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


		$lo = new libfilesystem();



		if(count($BookIDArr) > 0)
		{
			$con = "";

			for($i = 0; $i < count($BookIDArr); $i++)
			{
				$sql = "
					DELETE FROM
					INTRANET_ELIB_BOOK
					WHERE
	            	BookID = ".$BookIDArr[$i];

				$this->db_db_query($sql);

				# remove files
				$book_folder = $intranet_root."/file/elibrary/content/".$BookIDArr[$i];
				$lo->folder_remove_recursive($book_folder);
			}
		}
	} // end function remove book info



	function SYNC_PHYSICAL_BOOK_TO_ELIB($PhysicalBookID="", $DisplayCounting=false)
	{
		global $PATH_WRT_ROOT, $intranet_root, $sys_custom;

		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
		include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

		$counts["UPDATE"] = 0;
		$counts["NEW"] = 0;
		$counts["DELETE"] = 0;

		$physical_book_typecode = $this->physical_book_type_code;

		$libms = new liblms();
		$elib_install = new elibrary_install();

		$li = new libfilesystem();
		$LangID = $li->file_read($intranet_root."/file/language.txt");
		$LangID += 0;
		if ($LangID==0)
		{
			$LangUsed = "en";
		} elseif ($LangID==1)
		{
			$LangUsed = "b5";
		} elseif ($LangID==2)
		{
			$LangUsed = "gb";
		}

		$limit_upper = 1000;
		if ($PhysicalBookID!="")
		{
			$cond = " AND lb.BookID IN ({$PhysicalBookID}) ";
			$maxLoop = 1;
		}
		else {
			$sql = "SELECT COUNT(*) AS CNT from LIBMS_BOOK";
			$rs = $libms->returnResultSet($sql);
			if (count($rs) > 0) {
				$no_of_rec = $rs[0]['CNT'];
				$maxLoop = ceil($no_of_rec / $limit_upper);
			}
			else {
				$maxLoop = 10000;
			}
		}
		$limit_i = 0;
		$retrieve_done = false;
		$rows = array();

		# from LMS
		$sql_books = "SELECT
							lb.BookID, lb.NoOfCopy, lb.BookTitle, lb.BookSubTitle, lb.Language, lb.Publisher, lb.Subject, lb.ResponsibilityBy1,
							lb.Introduction, lb.CoverImage, lb.OpenBorrow, lbc.DescriptionEn AS CategoryEng,
							lbc.DescriptionChi  AS CategoryChi, lb.BookCategoryCode, lb.RecordStatus,
							lb.Series, lb.CallNum, lb.CallNum2, lb.ISBN, bu.AccountDate, lbc.BookCategoryType
						FROM LIBMS_BOOK AS lb
						LEFT JOIN LIBMS_BOOK_CATEGORY AS lbc on lbc.BookCategoryCode=lb.BookCategoryCode
							AND lbc.BookCategoryType IN (IFNULL((SELECT lbl2.BookCategoryType FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookLanguageCode = lb.Language),1))
						LEFT JOIN (SELECT MIN(bu.CreationDate) AS AccountDate, bu.BookID
									FROM LIBMS_BOOK_UNIQUE bu
									WHERE bu.RecordStatus<>'DELETE'
									AND bu.CreationDate IS NOT NULL
									AND bu.CreationDate <>'0000-00-00 00:00:00' GROUP BY bu.BookID) AS bu ON bu.BookID=lb.BookID
						WHERE
							lb.BookID>0 " . $cond . "
						ORDER BY lb.BookID ASC";

		while (!$retrieve_done && $limit_i<$maxLoop)
		{
			$sql_limit_start = $limit_i * $limit_upper;
			$sql = $sql_books . " limit {$sql_limit_start}, {$limit_upper}";

			$rows = $libms->returnResultSet($sql);

			$limit_i++;
			$retrieve_done = (sizeof($rows)<$limit_upper);

			$book_id_array = array();
			if (count($rows) > 0) {
				foreach($rows as $row) {
					$book_id_array[] = $row['BookID'];
				}
			}

			# get ACNO into any associated array by BookID
			$sql = "select ACNO, BookID from LIBMS_BOOK_UNIQUE
					where ACNO is not null ".
					((count($book_id_array)>0) ? " and BookID in ('". implode("','",$book_id_array)."') " : "") ."
					order by BookID ";
			$result1 = $libms->returnResultSet($sql);

			unset($book_id_array);
			$acnoMapping = array();
			if(count($result1) > 0){
				for($k=0 ; $k<count($result1) ; $k++){
					$acnoMapping[$result1[$k]['BookID']][] = $result1[$k]['ACNO'];
				}
			}
			unset($result1);

			for ($i=0, $iMax=count($rows); $i<$iMax; $i++)
			{
				$bookObj = $rows[$i];

				$eBookID = $this->physical_book_init_value + $bookObj["BookID"];

				# set acnoList
				if(isset($acnoMapping[$bookObj["BookID"]]) && ($acnoMapping[$bookObj["BookID"]]) > 0){
					# separator: |=|
					$acnoList = implode("|=|", $acnoMapping[$bookObj["BookID"]]);
				} else {
					$acnoList = '';
				}

				$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK WHERE BookID=".$eBookID;

				$eBookRows = $this->returnVector($sql);

				$categoryName = "";
				if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
					$categoryName = trim($bookObj['BookCategoryCode']) . " - ". trim($bookObj['CategoryEng']) . " - ". trim($bookObj['CategoryChi']);
				}
				else {
					if ($LangUsed=="en" && trim($bookObj['CategoryEng'])!="")
					{
						$categoryName = trim($bookObj['BookCategoryCode']) . " - ". trim($bookObj['CategoryEng']);
					} elseif (trim($bookObj['CategoryChi'])!="")
					{
						$categoryName = trim($bookObj['BookCategoryCode']) . " - ". trim($bookObj['CategoryChi']);
					}
				}

				$callNumber = $this->GetCallNumber($bookObj['CallNum'],$bookObj['CallNum2']);

				# if exist, update
				if (strtoupper($bookObj["RecordStatus"])=="DELETE")
				{
					$this->REMOVE_PHYSICAL_BOOK($bookObj["BookID"]);
					$counts["DELETE"] ++;
				} elseif (sizeof($eBookRows)>0)
				{
					$sql = "UPDATE INTRANET_ELIB_BOOK SET
							 Title='".addslashes($bookObj['BookTitle'])."',
							 SubTitle='".addslashes($bookObj['BookSubTitle'])."',
			                 Author= '".addslashes($bookObj['ResponsibilityBy1'])."',
			                 Category= '".addslashes($categoryName)."',
			                 CallNumber= '".addslashes($callNumber)."',
			                 SubCategory= '".addslashes($bookObj['SubCategory'])."',
			                 Preface= '".addslashes($bookObj['Introduction'])."',
							 RelevantSubject= '".addslashes($bookObj['Subject'])."',
			                 Publisher= '".addslashes($bookObj['Publisher'])."',
			                 Language= '".addslashes($bookObj['Language'])."',
			                 ISBN= '".addslashes($bookObj['ISBN'])."',
			                 Publish= '".addslashes($bookObj['OpenBorrow'] && $bookObj['NoOfCopy'] > 0?1:0)."',
			                 BookFormat= '{$physical_book_typecode}',
							 SeriesEditor= '".addslashes($bookObj['Series'])."',
							 ACNOList = '".$acnoList."',
							 AccountDate = '".addslashes($bookObj['AccountDate'])."',
							 BookCategoryType = '".addslashes($bookObj['BookCategoryType'])."'
			                WHERE BookID=".$eBookID;	// Charles Ma 20131125-elibplus-oldbook   DateModified= now()
			         $counts["UPDATE"]++;
				} else
				{
					# else insert
					$sql = "
						INSERT INTO
						INTRANET_ELIB_BOOK
						(BookID, Title, SubTitle, Author, Category, SubCategory, Preface, RelevantSubject, Publisher, Language, ISBN, Publish, BookFormat, SeriesEditor, ACNOList, AccountDate, BookCategoryType, DateModified)
						VALUES
		  				(
		                  $eBookID,
		                  '".addslashes($bookObj['BookTitle'])."',
						  '".addslashes($bookObj['BookSubTitle'])."',
		                  '".addslashes($bookObj['ResponsibilityBy1'])."',
		                  '".addslashes($categoryName)."',
		                  '".addslashes($bookObj['SubCategory'])."',
		                  '".addslashes($bookObj['Introduction'])."',
						  '".addslashes($bookObj['Subject'])."',
		                  '".addslashes($bookObj['Publisher'])."',
		                  '".addslashes($bookObj['Language'])."',
		                  '".addslashes($bookObj['ISBN'])."',
		                  '".addslashes($bookObj['OpenBorrow'] && $bookObj['NoOfCopy'] > 0?1:0)."',
		                  '{$physical_book_typecode}',
						  '".addslashes($bookObj['Series'])."',
						  '".$acnoList."',
						  '".addslashes($bookObj['AccountDate'])."',
						  '".addslashes($bookObj['BookCategoryType'])."',
		                  NOW())
						 ";
					$counts["NEW"] ++;
				}

				$this->db_db_query($sql);

				# tags (category)
				$sql = " SELECT DISTINCT lt.TagName FROM LIBMS_BOOK_TAG AS lbt, LIBMS_TAG AS lt WHERE lbt.BookID=".$bookObj["BookID"]." " .
						" AND lt.TagID=lbt.TagID ORDER by TagName ASC ";
				$tagRows = $libms->returnVector($sql);
				if (sizeof($tagRows)>0)
				{
					$BookTags = implode(",", $tagRows);
				} else
				{
					$BookTags = "";
				}
				$this->UPDATE_BOOK_TAGS($BookTags, $eBookID);

				# book cover TBD - Yuen

				# enable
				if ($bookObj['OpenBorrow'] && strtoupper($bookObj["RecordStatus"])!="DELETE")
					$elib_install->enable_site_book_license($eBookID, 0);
				else
					$elib_install->disable_site_book_license($eBookID);
				unset($bookObj);
			}	// end for $bookObj
			unset($rows);
			unset($acnoMapping);

		}	// end while loop

		if ($DisplayCounting)
		{
			debug_r($counts);
		}

	}


	function REMOVE_PHYSICAL_BOOK($PhysicalBookID)
	{
		global $PATH_WRT_ROOT, $intranet_root;

		include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

		if ($PhysicalBookID!="" && $PhysicalBookID>0)
		{
			$elib_install = new elibrary_install();

			$eBookID = $this->physical_book_init_value + $PhysicalBookID;

			$elib_install->disable_site_book_license($eBookID);

			$sql = "DELETE FROM INTRANET_ELIB_BOOK WHERE BookID='".$eBookID."'";

			return $this->db_db_query($sql);
		}
	}


	function GET_PHYSICAL_BOOK_INFO($eBookID)
	{
		global $PATH_WRT_ROOT, $intranet_root, $intranet_session_language;

		include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
		$libms = new liblms();

		/*
		 * no of copies,
		 * no of copies available to loan
		 *
		 */
		 $physicalBookID = $eBookID - $this->physical_book_init_value;
		 if ($physicalBookID!=""  && $physicalBookID>0)
		 {
		 	$BookRows = $libms->GET_BOOK_INFO($physicalBookID);

		 	##Get Location
		 	$LocationAry = $libms->GET_LOCATION_INFO();
		 	if ($intranet_session_language=="en")
 			{
 				$location_field = "DescriptionEn";
 			} else {
 				$location_field = "DescriptionChi";
 			}
 			$LocationAry = BuildMultiKeyAssoc($LocationAry, 'LocationCode', $location_field);
		 	if (trim($BookRows[0]['LocationCode'])!="")
		 	{
		 		/*$sql = "SELECT DescriptionEn, DescriptionChi FROM LIBMS_LOCATION WHERE LocationCode='".addslashes($BookRows[0]['LocationCode'])."'";
		 		$locationRows = $libms->returnArray($sql);
		 		if (sizeof($locationRows)>0)
		 		{
		 			if ($intranet_session_language=="en")
		 			{
		 				$BookRows[0]["Location"] = $locationRows[0]["DescriptionEn"];
		 			} else {
		 				$BookRows[0]["Location"] = $locationRows[0]["DescriptionChi"];
		 			}
		 		}*/
		 		$BookRows[0]["Location"] = $LocationAry[$BookRows[0]['LocationCode']][$location_field];
		 	}
		 	$BookItems = $libms->GET_BARCODE($physicalBookID);
		 	$BookItems = sortByColumn($BookItems,'ACNO');
		 	foreach($BookItems as $_itemAry){
		 		if($_itemAry['RecordStatus']=='NORMAL'||$_itemAry['RecordStatus']=='SHELVING'||$_itemAry['RecordStatus']=='RESERVED'){

		 			$_location =$LocationAry[$_itemAry['LocationCode']][$location_field];

		 			$BookRows[0]['AvailableItem'][$_location][] = array("ACNO"=>$_itemAry['ACNO'],
																		"status"=>$_itemAry['RecordStatus'],
																		"ItemSeriesNum"=>$_itemAry['ItemSeriesNum']);
		 		}else if($_itemAry['RecordStatus']=='BORROWED'){
		 			$BorrowLog = current($libms->GET_BORROW_LOG($physicalBookID,$_itemAry['UniqueID']));
		 			$BookRows[0]['BorrowedItem'][] = array("ACNO"=>$_itemAry['ACNO'],
															"duedate"=>$BorrowLog['DueDate'],
															"ItemSeriesNum"=>$BorrowLog['ItemSeriesNum']);
		 		}
		 	}
		 	//debug_r($BookRows[0]);
		 	return $BookRows[0];

		 	/*
		 	 * Language, Subject, Edition, PublishYear, LocationCode
		 	 * NoOfCopy, NoOfCopyAvailable
		 	 */
		 } else
		 {
		 	die ("Oooops... no book is found in Library Management System");
		 }
	}



	function ADD_BOOK_INFO($ParArr="")
	{
		$Type = $ParArr['InputBy'];
		$TmpAuthorDesc = "";
		$TmpAuthor = $ParArr['Author'];

		$TmpAuthorID = $this->UPDATE_BOOK_AUTHOR($TmpAuthor,$TmpAuthorDesc,$Type);

		# get the last book ID
		$sql = "SELECT max(bookID) from INTRANET_ELIB_BOOK where bookID<".$this->physical_book_init_value;
		$row = $this->returnVector($sql);
		$BookIDNew = $row[0] + 1;

		if ($BookIDNew<$this->physical_book_init_value && $BookIDNew>0)
		{
			$sql = "
					INSERT INTO
					INTRANET_ELIB_BOOK
					(BookID, Title, Author, AuthorID, SeriesEditor, Category, SubCategory, RelevantSubject, Level, AdultContent, Publisher, Source, Language, Preface, Copyright, InputBy, Publish, FirstPublished, ePublisher, CopyrightYear, CopyrightStatement, DateModified)
					VALUES
	  				(
	                  $BookIDNew,
	                  '".$ParArr['Title']."',
	                  '".$ParArr['Author']."',
	                  '".$TmpAuthorID."',
	                  '".$ParArr['SeriesEditor']."',
	                  '".$ParArr['Category']."',
	                  '".$ParArr['SubCategory']."',
					  '".$ParArr['RelevantSubject']."',
					  '".$ParArr['Level']."',
					  '".$ParArr['AdultContent']."',
	                  '".$ParArr['Publisher']."',
	                  '".$ParArr['Source']."',
	                  '".$ParArr['Language']."',
	                  '".$ParArr['Preface']."',
					  '".$ParArr['Copyright']."',
	                  '".$ParArr['InputBy']."',
	                  '".$ParArr['Publish']."','".$ParArr['FirstPublished']."','".$ParArr['ePublisher']."','".$ParArr['CopyrightYear']."','".$ParArr['CopyrightStatement']."',
	                  NOW())
					 ";

			$this->db_db_query($sql);

			//return $this->db_insert_id();
			return $BookIDNew;
		} else
		{
			die("The number of eBooks exceeds the limit (".$this->physical_book_init_value.")!");
		}
	} // end function add book info

	function ADD_BOOK_CHAPTER($BookID, $ChapterInfo)
	{
		$Sql =  "
  						INSERT INTO
  							INTRANET_ELIB_BOOK_CHAPTER
  							 (BookID,
                  ChapterID,
                  SubChapterID,
                  Title,
                  OrigPageID,
                  IsChapter,
                  DateModified)
  						VALUES
  						  ('".$BookID."',
                  '".$ChapterInfo['ChapterID']."',
                  '".$ChapterInfo['SubChapterID']."',
                  '".$ChapterInfo['Title']."',
                  '".$ChapterInfo['OrigPageID']."',
                  '".$ChapterInfo['IsChapter']."',
                  NOW())
					   ";
    $this->db_db_query($Sql);
	}

	function DELETE_BOOK_CHAPTER($BookID, $BookChapterID = "")
	{
    $Sql = "
						DELETE FROM
							INTRANET_ELIB_BOOK_CHAPTER
            WHERE
              BookID = '".$BookID."'
				   ";
		if($BookChapterID != "")
		  $Sql .= "
                AND
                  BookChapterID = '".$BookChapterID."'
              ";

		$this->db_db_query($Sql);
  }

	function IS_CHAPTER_START($BookID, $PageID)
	{
		$Sql = "
					SELECT
						IsChapterStart
					FROM
						INTRANET_ELIB_BOOK_PAGE
					WHERE
						BookID = '".$BookID."' AND
						PageID = '".$PageID."'
				  ";
		$ReturnVal = $this->returnVector($Sql);

		return $ReturnVal[0];
	}

	function GET_CHAPTER_INDEXES($BookID)
	{
		$Sql = "
					SELECT DISTINCT
						ChapterID, Title
					FROM
						INTRANET_ELIB_BOOK_CHAPTER
					WHERE
						BookID = '".$BookID."'
				  ";
		$ReturnArray = $this->returnArray($Sql);

		return $ReturnArray;
	}

	function TEST_UPDATE_CAT()
	{
		$Sql = "
					SELECT
						Category
					FROM
						INTRANET_ELIB_BOOK
					WHERE
						BookID = '2'
				  ";
		$ReturnArr = $this->returnVector($Sql);
		$TmpCat = $ReturnArr[0];

		$Sql = "
					UPDATE
						INTRANET_ELIB_BOOK
					SET
						Category = '".$TmpCat."'
					WHERE
						(BookID != '2') AND (BookID != '383')
				  ";
		$this->db_db_query($Sql);
	}

	function printLeftMenu() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$x  = "<div id=\"eLib_logo\" style=\"position: absolute; z-index: 2; left: 0px; top: -18px;\"  >";
		$x .= "<table width=\"120\" height=\"110\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
		$x .= "<tbody><tr>";

		$x .= "<td valign=\"middle\" background=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icalendar/logo_bg_01.gif\" align=\"center\">";
		$x .= "<a href='".$PATH_WRT_ROOT."home/eLearning/elibrary/'><img width=\"110\" height=\"100\" src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/leftmenu/icon_eLibrary.gif\"/></a>";
		$x .= "</td>";

		$x .= "<td valign=\"bottom\">";
		$x .= "<img width=\"33\" height=\"110\" src=\"".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icalendar/logo_bg_02.gif\"/>";
		$x .= "</td>";

		$x .= "</tr>";
		$x .= "</tbody></table>";
		$x .= "</div>";

		return $x;
	}

	function GET_MOST_ACTIVE_REVIEWERS($num=10)
	{
		$sql = "SELECT
			   a.UserID, count(a.UserID) countNum, b.ChineseName, b.FirstName, b.LastName, b.ClassName, b.ClassNumber, b.EnglishName
			   FROM
			   INTRANET_ELIB_BOOK_REVIEW a, INTRANET_USER b
			   WHERE
			   a.UserID = b.UserID
			   GROUP BY
			   UserID
			   ORDER BY
			   countNum desc
			   Limit $num";

		$ReturnArray = $this->returnArray($sql);

		return $ReturnArray;
	} // end function get most active reviewers

	function printMostActiveReviewers($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;

		$lang = $_SESSION["intranet_session_language"];

		$x = "<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">";

		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			//$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];

			if($ClassName != "")
			$ClassName = "(".$ClassName.")";

			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;

				$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
				$lang1 = $UserName_b5;

				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}

			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;

			if(trim($UserName) == "")
			$UserName = "--";

			$rank = $i + 1;

			if($rank <= 5)
			$rankBG = "top_no_".$rank."a.gif";
			else
			$rankBG = "top_no_b.gif";

		$x .= "<tr>";
		$x .= "<td width=\"35\">";
		$x .= "<table width=\"31\" height=\"30\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/".$rankBG."\" class=\"tabletopnolink\">".$rank."</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "<td>";
		$x .= "<table border=\"0\" cellspacing=\"4\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td valign=\"top\">&nbsp;<a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('elib_top_reviewers.php?ReviewerID=$currUserID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\" class=\"eLibrary_top10_link1\">".$UserName." ".$ClassName."</a></td>";
		$x .= "<td valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td width=\"2\" height=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_01.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "<td height=\"2\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_02.gif\" width=\"20\" height=\"2\"></td>";
		$x .= "<td width=\"2\" height=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_03.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"2\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_04.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"eLibrary_top_review_no\">".$Count."</td>";
		$x .= "<td background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_06.gif\" width=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_06.gif\" width=\"2\" height=\"2\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"2\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_07.gif\" width=\"2\" height=\"5\"></td>";
		$x .= "<td height=\"5\" align=\"left\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_08b.gif\" width=\"7\" height=\"5\"></td>";
		$x .= "<td width=\"2\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/most_review_bubble_09.gif\" width=\"2\" height=\"5\"></td>";
		$x .= "</tr>";
		$x .= "	</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		}
		$x .= "</table>";
		return $x;
	} // end function print Most active reviewers

	function GET_MOST_USEFUL_REVIEWS($num=10)
	{

		$sql = "SELECT
			   a.BookID, c.Title, a.ReviewID, d.UserID,
			   count(a.ReviewID) countNum,
			   b.ChineseName, b.EnglishName, b.FirstName, b.LastName, b.ClassName, b.ClassNumber, c.BookFormat, c.IsTLF
			   FROM
				   INTRANET_ELIB_BOOK_REVIEW_HELPFUL a
			   LEFT OUTER JOIN
				   INTRANET_ELIB_BOOK_REVIEW d
			   ON
				   a.ReviewID = d.ReviewID
			   INNER JOIN
				   INTRANET_USER b
			   ON
				   d.UserID = b.UserID
			   INNER JOIN
				   INTRANET_ELIB_BOOK c
			   ON
				   c.BookID = a.BookID
			   WHERE
			   a.Choose = 1
			   AND c.Publish = 1
			   GROUP BY
			   a.ReviewID
			   ORDER BY
			   countNum desc
			   Limit $num";

			   //debug_r($sql);

		$ReturnArray = $this->returnArray($sql);

		return $ReturnArray;
	} // end function get most useful review

	function printMostUsefulReviews($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;

		//hdebug_r($data);

		$lang = $_SESSION["intranet_session_language"];

		$x = "<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">";

		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$ReviewID = $data[$i]["ReviewID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en2 = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];
			//$BookName = iconv("UTF-8","BIG-5",$data[$i]["Title"]);
			$BookName = $data[$i]["Title"];

			if($ClassName != "")
			{
				if($ClassNumber != "")
				$ClassName = "(".$ClassName."-".$ClassNumber.")";
				else
				$ClassName = "(".$ClassName.")";
			}

			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;

			$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
			$lang1 = $UserName_b5;

				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}

			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;

			if(trim($UserName) == "")
			$UserName = "--";

			$rank = $i + 1;

			if($rank <= 5)
			$rankBG = "top_no_".$rank."b.gif";
			else
			$rankBG = "top_no_b.gif";

			$x .= "<tr>";
			$x .= "<td width=\"35\">";
			$x .= "<table width=\"31\" height=\"30\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$x .= "<tr>";
			$x .= "<td align=\"center\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/".$rankBG."\" class=\"tabletopnolink\">".$rank."</td>";
			$x .= "</tr>";
			$x .= "</table>";
			$x .= "</td>";
			$x .= "<td>&nbsp;<a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('elib_top_reviews.php?ReviewID=$ReviewID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\" class=\"eLibrary_top10_link2\">".$BookName."</a></td>";
			$x .= "<td class=\"eLibrary_top10_link2_stu_name\">".$UserName."  ".$ClassName."</td>";
			$x .= "</tr>";

		}
		$x .= "</table>";
		return $x;
	} // end function print Most active reviewers

	/*
	 * 	$bookType: 	ebook		-- BookID < 10000000
	 * 				physical	-- BookID >= 10000000
	 * 				$dateRange	-- 0 - accumulated, 1 - this week
	 */
	function GET_RECOMMEND_BOOK($numLimit=1,$bookType='',$offset=0,$with_total=false,$dateRange=0)
	{
		//AND a.RecommendGroup LIKE '%d.ClassLevelID%'
		//AND b.ClassLevel = d.LevelName
		//AND b.UserID = '{$currUserID}'
		//INTRANET_ELIB_BOOK_RECOMMEND a, INTRANET_USER b, INTRANET_ELIB_BOOK c, INTRANET_CLASS d

		$currUserID = $_SESSION["UserID"];

		if($this->IS_TEACHER() || $this->IS_ADMIN() || empty($currUserID)){
			$con = "";
			$par1 = "";
			$par2 = "";
		}else{
			$AcademicYearID = Get_Current_Academic_Year_ID();
			$con = "INNER JOIN YEAR_CLASS d ON d.AcademicYearID='{$AcademicYearID}' AND (
					(
					CONCAT('|',a.RecommendGroup,'|')  LIKE CONCAT('%|',d.YearID,'|%')
				  	)
					OR
					CONCAT('|',a.RecommendGroup,'|') LIKE CONCAT('%|',-1,'|%')
				)";
            if ($_SESSION['UserType'] == USERTYPE_PARENT) {     // kis parent need to use sch function
                $con .= " INNER JOIN INTRANET_PARENTRELATION r ON r.ParentID='{$currUserID}' ";
                $con .= " INNER JOIN INTRANET_USER b ON b.UserID=r.StudentID AND b.ClassName = d.ClassTitleEN ";
            }
            else {      // USERTYPE_STUDENT
                $con .= " INNER JOIN INTRANET_USER b ON b.UserID = '{$currUserID}' AND b.ClassName = d.ClassTitleEN ";
            }

			$par1 = "d.ClassTitleEN as ClassTitleEN, ";
			$par2 = "d.YearID as YearID, ";
/*			$con = "AND b.ClassName = d.ClassTitleEN"; */
		}

		if ($bookType == $this->physical_book_type_code) {
			$bookTypeCond = "AND c.BookID>=".$this->physical_book_init_value;
		}
		else if ($bookType == 'ebook') {
			$bookTypeCond = "AND c.BookID<".$this->physical_book_init_value;
		}
		else {
			$bookTypeCond = "";
		}

//		$con .= "AND c.BookID = i.BookID";
//		$con .= "AND i.QuotationID = q.QuotationID";
//		$con .= "AND (" .
//				"q.PeriodTo = '0000-00-00 00:00:00' OR q.PeriodTo > NOW()" .
//				")";
		$expireBook = $this->getExpiredBookIDArr();
		if(!empty($expireBook)){
//			$exCond_a = (!strpos($_GET['action'], 'Portal'))?((isset($_GET['action']))?'':' AND a.BookID NOT IN ('.$expireBook.') '):' AND a.BookID NOT IN ('.$expireBook.') ';
		    $exCond_a = ' AND a.BookID NOT IN ('.$expireBook.') ';
		}else{
			$exCond_a = '';
		}

		if ($dateRange == 1) {
			$dateRangeCond = " AND a.DateModified between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())-1 DAY) and NOW()";
		}
		else {
			$dateRangeCond = "";
		}
		$sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT
				a.BookID as BookID, c.Title as Title, a.Description as Description, $par1 a.RecommendGroup as RecommendGroup, $par2 c.BookFormat as BookFormat, c.IsTLF as IsTLF
				FROM
				INTRANET_ELIB_BOOK c
				INNER JOIN INTRANET_ELIB_BOOK_RECOMMEND a ON c.BookID = a.BookID
				$con
				WHERE c.Publish = 1
				$exCond_a
				$bookTypeCond
				$dateRangeCond
				GROUP BY
				a.BookID
				ORDER BY
				a.RecommendOrder ASC, a.DateModified DESC";
		if ($numLimit != -1) {
			$sql .= " Limit $offset,$numLimit";
		}

/*		$sql = "SELECT DISTINCT
				a.BookID as BookID, c.Title as Title, a.Description as Description, d.ClassTitleEN as ClassTitleEN, a.RecommendGroup as RecommendGroup, d.YearID as YearID, c.BookFormat as BookFormat, c.IsTLF as IsTLF
				FROM
				INTRANET_ELIB_BOOK_RECOMMEND a, INTRANET_ELIB_BOOK c, INTRANET_USER b, YEAR_CLASS d
				WHERE
				a.BookID = c.BookID
				$con
				$exCond_a
				AND b.UserID = '{$currUserID}'
				AND
				(
					(
					CONCAT('|',a.RecommendGroup,'|')  LIKE CONCAT('%|',d.YearID,'|%')
				  	)
					OR
					CONCAT('|',a.RecommendGroup,'|') LIKE CONCAT('%|',-1,'|%')
				)
				AND c.Publish = 1
				GROUP BY
				a.BookID
				ORDER BY
				a.RecommendOrder ASC, a.DateModified DESC
				Limit 1000"	;
*/
		// debug_r($sql);

		$ReturnArray = $this->returnArray($sql);
		$total = current($this->returnVector('select found_rows()'));

		return $with_total ? array($total, $ReturnArray) : $ReturnArray;

	} // end function get recommend book

	function GET_BOOK_CATEGORY($ParArr="")
	{
		$Language = $ParArr["Language"];

		if($Language == "eng" || $Language == "chi")
		$con = "AND Language='".$Language."'";
		else
		$con = "";

		$sql = "
				SELECT DISTINCT
				Category
				FROM
				INTRANET_ELIB_BOOK
				WHERE 1
				AND Publish = 1
				$con
				";

		$ReturnArray = $this->returnArray($sql);
		return $ReturnArray;
	} // end function get book category

	function getBookLanguage($ParArr="")
	{
		$BookID = $ParArr["BookID"];

		$sql = "
		SELECT
		Language
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		BookID = {$BookID}
		";

		$ReturnArray = $this->returnArray($sql);
		return $ReturnArray;
	} // end function get book language

	function ADD_READING_HISTORY_RECORD($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["UserID"];

		$sql = "
		INSERT INTO
		INTRANET_ELIB_BOOK_HISTORY
		(UserID,BookID,DateModified)
		VALUES
			('".$currUserID."','".$BookID."', now())
		";

		$this->db_db_query($sql);
	} // end function add reading history record

	function ADD_BOOK_HIT_RATE($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		/*
		$sql = "
		SELECT
		HitRate
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		BookID = {$BookID}
		";

		$ReturnArray = $this->returnArray($sql);

		$HitRate = $ReturnArray[0]["HitRate"];

		if($HitRate == "" || $HitRate == NULL)
			$HitRate = 0;

		$HitRate += 1;

		$sql = "
				UPDATE
				INTRANET_ELIB_BOOK
				SET
				HitRate = {$HitRate}
				WHERE
				BookID = {$BookID}
			  ";
		*/

		$sql = "
				UPDATE
					INTRANET_ELIB_BOOK
				SET
					HitRate = IF(HitRate IS NULL, 1, HitRate+1)
				WHERE
					BookID = {$BookID}
			  ";

	    $this->db_db_query($sql);
	} // end function add book hit rate

	function GET_WEEKLY_HIT_BOOK($numLimit=1)
	{
		$today = date("Y-m-d G:i:s");
		$lastweek = date('Y-m-d G:i:s', mktime(date("G"), date("i"), date("s"), date("m"),   date("d")-7,   date("Y"))); // 2009-04-04 12:00:00

		$start_date = $lastweek;
		$end_date = $today;

		$sql = "SELECT
			   a.BookID, c.Title, count(a.BookID) countNum, c.BookFormat, c.IsTLF
			   FROM
			   INTRANET_ELIB_BOOK_HISTORY a, INTRANET_ELIB_BOOK c
			   WHERE
			   a.BookID = c.BookID
			   AND c.Publish = 1
			   AND
			   (a.DateModified BETWEEN '$start_date' AND '$end_date')
			   GROUP BY
			   a.BookID
			   ORDER BY
			   countNum desc
			   Limit $numLimit";

		$ReturnArray = $this->returnArray($sql);

		//if(count($ReturnArray) <= 0)
		//$ReturnArray = $this->GET_HIT_BOOK($numLimit=1);

		return $ReturnArray;
	} // end function get weekly hit book

	function GET_HIT_BOOK($numLimit=1)
	{
		$sql = "SELECT
			   BookID, Title, BookFormat, IsTLF
			   FROM
			   INTRANET_ELIB_BOOK
			   WHERE
			   Publish = 1
			   AND HitRate >= 1
			   ORDER BY
			   HitRate desc
			   Limit $numLimit";

		$ReturnArray = $this->returnArray($sql);

		return $ReturnArray;
	} // end function get hit book


	function ADD_BOOK_COVER($BookID, $filepath)
	{
		global $intranet_root, $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libimage.php");
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

		$filepath_abs = $intranet_root.$filepath;
		if (is_file($filepath_abs))
		{
			$imagesize = getimagesize($filepath_abs);

			$lo = new libfilesystem();
			$image_obj = new SimpleImage();

			# copy and resize
			$photo_des = $intranet_root."/file/elibrary/content/".$BookID."/image";
			$lo->createFolder($photo_des);
			if ($imagesize[1]>220 && $image_obj->IsGDReady())
			{
			   $image_obj->load($filepath_abs);
			   $image_obj->resizeToMax(163, 220);
			   $image_obj->save($photo_des."/cover.jpg");

			} else
			{
				$lo->file_copy($filepath_abs, $photo_des);
			}

		}

	}

	function printBookTable($data="", $image_path="", $LAYOUT_SKIN="", $NumOfBook="", $type="", $eLib=""){
		global $PATH_WRT_ROOT;

		//debug_r($data);

		if($NumOfBook == 4)
		{
			$NumOfBook = 2;
			$width = 100;
			$height = 135;
		}
		else if($NumOfBook == 9)
		{
			$NumOfBook = 3;
			$width = 45;
			$height = 65;
		}
		else
		{
			$NumOfBook = 1;
			$width = 200;
			$height = 285;
		}

		$count = 0;

		// check if recommend table
		if(count($data) <= 0 && $type == "recommend")
		{
			$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr><td align=\"center\" class=\"tabletext\">";
			$x .= "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
			$x .= $eLib["html"]["no_recommend_book"];
			$x .= "</td></tr>";
			$x .= "</table>";

			return $x;
		}

		$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		for($i = 0; $i < $NumOfBook ; $i++)
		{
			$x .= "<tr>";
			for($j = 0; $j < $NumOfBook ; $j++)
			{
				//$BookName = iconv("UTF-8","BIG-5",$data[$count]["Title"]);
				$BookName = $data[$count]["Title"];
				$BookID = $data[$count]["BookID"];
				$Description =  $data[$count]["Description"];

				if($BookID != "")
				{
					$BookCover = "/file/elibrary/content/".$BookID."/image/cover.jpg";
				}
				else
				{
					$BookCover = $image_path."/".$LAYOUT_SKIN."/10x10.gif";
				}

				$count++;

				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td>";
				if($type == "recommend")
				{
					if($BookID != "")
					$x .= "<div id=\"book_border\"><a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\" onMouseover=\"MM_showHideLayers('recommend_detail_$count','','show')\" onMouseout=\"MM_showHideLayers('recommend_detail_$count','','hide')\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
					else
					{
						//$x .= "<div id=\"book_border\"><a href=\"javascript:void()\" onMouseover=\"MM_showHideLayers('recommend_detail_$count','','show')\" onMouseout=\"MM_showHideLayers('recommend_detail_$count','','hide')\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
						$x .= "<div><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></div>";
					}
				}
				else
				{
					if($BookID != "")
						$x .= "<div id=\"book_border\"><a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
					else
					{
						//$x .= "<div id=\"book_border\"><a href=\"javascript:void()\"><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></a></div>";
						$x .= "<div><span><img src=\"".$BookCover."\" width=\"".$width."\" height=\"".$height."\" border=\"0\"></span></div>";
					}
				}
				$x .= "</td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td>";
				if($type == "recommend")
				{
					$x .= "<td align=\"left\" valign=\"top\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"1\" height=\"20\"><br>";
					$x .= "<div id=\"recommend_detail_$count\" style=\"position:absolute; width: 120px; z-index:1; visibility: hidden;\"><table  border=\"0\" cellspacing=\"0\" cellpadding=\"2\" class=\"eLibrary_recomm_detail\">";
					$x .= "<tr>";
					$x .= "<td nowrap class=\"eLibrary_title_sub_heading\"> ".$eLib["html"]["recommended_reason"]."</td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td class=\"tabletext\">".$Description."</td>";
					$x .= "</tr></table></div></td>";
				}
				$x .= "</tr><tr><td>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$x .= "<tr>";
				$x .= "<td align=\"center\" class=\"tabletext\"><a href=\"javascript:void()\" class=\"eLibrary_booktitle_normal\">".$BookName."</a></td>";
				$x .= "</tr></table></td></tr></table></td>";
			} // end for cell
			$x .= "</tr>";
		} // end for row
		$x .= "</table>";


		return $x;
	} // end function print table

	function getSourceFullName($Source="")
	{
		if($Source == "cup")
		$Source = "Cambridge University Press";
		else if($Source == "green")
		$Source = "GREEN APPLE MEDIA LIMITED";

		return $Source;
	} // end function get source full name

	function GET_USER_BOOK_SETTING($ParArr="")
	{
		// admin acc
		$currUserID = 0;
		//$currUserID = $ParArr["UserID"];

		$Sql = "
				SELECT
					DisplayReviewer,DisplayReview,DisplayRecommendBook,DisplayWeeklyHitBook,DisplayHitBook
				FROM
					INTRANET_ELIB_BOOK_SETTINGS
				WHERE
					UserID = '{$currUserID}'
			  ";

		$ReturnArr = $this->returnArray($Sql, 5);

		if(count($ReturnArr) <= 0)
		{
			$this->UPDATE_USER_BOOK_SETTING($ParArr);

		$Sql = "
				SELECT
					DisplayReviewer,DisplayReview,DisplayRecommendBook,DisplayWeeklyHitBook,DisplayHitBook
				FROM
					INTRANET_ELIB_BOOK_SETTINGS
				WHERE
					UserID = '{$currUserID}'
			  ";

		$ReturnArr = $this->returnArray($Sql, 5);
		}

		return $ReturnArr;
	} // end function get user book setting

	function UPDATE_USER_BOOK_SETTING($ParArr="")
	{
		// admin acc setting
		$currUserID = 0;
		//$currUserID = $ParArr["UserID"];

		$Sql = "
				SELECT
					UserID
				FROM
					INTRANET_ELIB_BOOK_SETTINGS
				WHERE
					UserID = '{$currUserID}'
			  ";

		$ReturnArr = $this->returnArray($Sql, 1);

		if(count($ReturnArr) <= 0)
		{
			$ParArr["UserID"] = 0;
			$ParArr["DisplayReviewer"] = 10;
			$ParArr["DisplayReview"] = 10;
			$ParArr["RecommendBook"] = 4;
			$ParArr["WeeklyHitBook"] = 1;
			$ParArr["HitBook"] = 9;

			$Sql1 =  "
					INSERT INTO
							INTRANET_ELIB_BOOK_SETTINGS
							(UserID,DisplayReviewer,DisplayReview,DisplayRecommendBook,DisplayWeeklyHitBook,DisplayHitBook)
					VALUES
						('".$ParArr["UserID"]."','".$ParArr["DisplayReviewer"]."','".$ParArr["DisplayReview"]."',
						'".$ParArr["RecommendBook"]."','".$ParArr["WeeklyHitBook"]."','".$ParArr["HitBook"]."')
					";
				$this->db_db_query($Sql1);
				//$returnID = $this->db_insert_id();
		}
		else
		{
			$Sql1 =  "
					UPDATE
						INTRANET_ELIB_BOOK_SETTINGS
					SET
						DisplayReviewer = '".$ParArr["DisplayReviewer"]."',
						DisplayReview = '".$ParArr["DisplayReview"]."',
						DisplayRecommendBook = '".$ParArr["RecommendBook"]."',
						DisplayWeeklyHitBook = '".$ParArr["WeeklyHitBook"]."',
						DisplayHitBook = '".$ParArr["HitBook"]."'
					WHERE
						UserID = '{$currUserID}'
					";

			$this->db_db_query($Sql1);
		}
	} // end function update user book setting

	function printSettingBookTable($tableName="", $image_path="", $LAYOUT_SKIN="", $NumOfBook=""){
		global $PATH_WRT_ROOT, $eLib;

		$BookName = $eLib["html"]["book_name"];
		$BookCover = $eLib["html"]["book_cover"];

		if($NumOfBook == 4)
		{
			$NumOfBook = 2;
			$width = 100;
			$height = 135;
		}
		else if($NumOfBook == 9)
		{
			$NumOfBook = 3;
			$width = 45;
			$height = 65;
			$BookName = $eLib["html"]["book_name"]."<br><br>";
		}
		else
		{
			$NumOfBook = 1;
			$width = 200;
			$height = 285;
		}

		$x = "<table name=\"$tableName\" id=\"$tableName\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		for($i = 0; $i < $NumOfBook ; $i++)
		{
			$x .= "<tr>";
			for($j = 0; $j < $NumOfBook ; $j++)
			{
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\">";
				$x .= "<tr>";
				$x .= "<td>";
				$x .= "<td width=\"".$width."\" height=\"".$height."\" align=\"center\" bgcolor=\"#C5E0FC\" class=\"tabletextremark\">".$BookCover."</td>";
				$x .= "</td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "</tr>";
				//$x .= "<tr><td>";
				//$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"0\">";
				//$x .= "<tr>";
				//$x .= "<td align=\"center\" class=\"tabletext\"><span class=\"eLibrary_booktitle_normal\">".$BookName."</span></td>";
				//$x .= "</tr>";
				//$x .= "</table>";
				//$x .= "</td></tr>";
				$x .= "</table>";
				$x .= "</td>";
			} // end for cell
			$x .= "</tr>";
		} // end for row
		$x .= "</table>";

		return $x;

	} // end function print setting book table

	function n_sizeof($arrData){

		$count = 0;
		if (is_array($arrData))
		{
			while (list($key, $value) = each($arrData))
			{
				if (is_int($key))
				{
					$count++;
				}
			}
		} else
		{
			$count = sizeof($arrData);
		}
		return $count;
	} // end function n_sizeof

	// convert PHP array to JS array (2D) -- copy from /home/eclass30/eclass30/src/includes/php/lib.php
	function ConvertToJSArray($arrData, $jArrName, $fromDB = 0){

		$ReturnStr = "<SCRIPT LANGUAGE=\"JavaScript\">\n";
		$ReturnStr .= "var ".$jArrName." = new Array(".sizeof($arrData).");\n";

		for ($i = 0; $i < sizeof($arrData); $i++)
		{
			$ReturnStr .= $jArrName."[".$i."] = new Array(".$this->n_sizeof($arrData[$i]).");\n";

			for ($j = 0; $j < $this->n_sizeof($arrData[$i]); $j++)
			{
				if (sizeof($arrData[$i]) > 1)
				{
					if ($fromDB)
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", $arrData[$i][$j])."\";\n";
					} else
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", htmlspecialchars($arrData[$i][$j]))."\";\n";
					}
				} else
				{
					if ($fromDB)
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", $arrData[$i])."\";\n";
					} else
					{
						$ReturnStr .= $jArrName."[".$i."][".$j."] = \"".str_replace("'", "&#96;", htmlspecialchars($arrData[$i]))."\";\n";
					}
				}
			}
		}

		$ReturnStr .= "</SCRIPT>\n";

		return $ReturnStr;
	} // end function convert to js array

	function displayPresetListAuthorInput($title="Author")
	{
		global $linterface;
		$sql = "SELECT Distinct Author FROM INTRANET_ELIB_BOOK ORDER BY Author";
		$returnArr = $this->returnArray($sql,1);

		//for($i = 0; $i < count($returnArr); $i++)
		//{
		//	$returnArr[$i][0] = iconv("UTF-8","BIG-5",$returnArr[$i][0]);
		//}

		$x .= $this->ConvertToJSArray($returnArr, "arrA", 1);
		$x .= $linterface->GET_LIST("form1", "arrA", $title);

		return $x;
	} // end function display preset list input

	function displayPresetListSourceInput($title="Source")
	{
		global $linterface;
		$sql = "SELECT Distinct Source FROM INTRANET_ELIB_BOOK ORDER BY Source";
		$returnArr = $this->returnArray($sql,1);

		//for($i = 0; $i < count($returnArr); $i++)
		//{
		//	$returnArr[$i][0] = iconv("UTF-8","BIG-5",$returnArr[$i][0]);
		//}

		$x .= $this->ConvertToJSArray($returnArr, "arrS", 1);
		$x .= $linterface->GET_LIST("form1", "arrS", $title);

		return $x;
	} // end function display preset list input

	function displayCategorySelect($sel="selCategory", $eLib="")
	{
		$sql = "SELECT Distinct CONCAT(Category, ' -> ', SubCategory) FROM INTRANET_ELIB_BOOK ORDER BY Category ASC, SubCategory ASC";
		$category = $this->returnArray($sql,1);

		$IsCatShowBefore = array();

		$x = "<select name=".$sel.">";
		$x .= "<option>".$eLib["html"]["all_category"]."</option>";

		for($i = 0; $i < count($category); $i++)
		{
			//$x .= "<option>".iconv("UTF-8","BIG-5",$category[$i][0])."</option>";
			if (trim($category[$i][0])!="->")
			{
				$arrTmp = explode(" -> ", $category[$i][0]);
				$ThisCat = trim($arrTmp[0]);
				if (!$IsCatShowBefore[$ThisCat])
				{
					$x .= "<option>".$ThisCat."</option>";
					$IsCatShowBefore[$ThisCat] = true;
				}
				$x .= "<option>".$category[$i][0]."</option>";
			}
		}

		$x .= "</select>";

		return $x;
	} // end function display Category select

	function displayLevelSelect($sel="selLevel", $eLib="")
	{
		$sql = "SELECT Distinct Level FROM INTRANET_ELIB_BOOK ORDER BY Level";
		$returnArr = $this->returnArray($sql,1);

		$x = "<select name=".$sel.">";
		$x .= "<option>".$eLib["html"]["all_level"]."</option>";

		for($i = 0; $i < count($returnArr); $i++)
		{
			if(trim($returnArr[$i][0]) != "")
			//$x .= "<option>".iconv("UTF-8","BIG-5",$returnArr[$i][0])."</option>";
			$x .= "<option>".$returnArr[$i][0]."</option>";
		}

		$x .= "</select>";

		return $x;
	} // end function display Category select

	// advance search
	function getBookDetail($ParArr="")
	{
		$BookTitle = $ParArr["BookTitle"];
		$Author = $ParArr["Author"];
		$Source = $ParArr["Source"];
		$Category = $ParArr["Category"];
		$Level = $ParArr["Level"];
		$ISBN = $ParArr["ISBN"];
		$Tag = $ParArr["Tag"];

		$startdate = $ParArr["startdate"];
		$enddate = $ParArr["enddate"];
		$checkWorksheets = $ParArr["checkWorksheets"];
		$sortField = $ParArr["sortField"];
		$sortOrder = $ParArr["sortFieldOrder"];

		//$DisplayNumPage = $ParArr["DisplayNumPage"];
		//$limit = "Limit {$DisplayNumPage}";

		$con1 = "";
		$con2 = "";
		$con3 = "";
		$con4 = "";
		$con5 = "";
		$con6 = "";
		$con7 = "";
		$con8 = "";
		$con9 = "";

		if($BookTitle != "")
		{
			//$BookTitle = iconv("big-5", "utf-8", trim($BookTitle));
			$BookTitle = trim($BookTitle);
			$con1 = "AND b.Title like '%{$BookTitle}%'";
		}

		if($Author != "")
		{
			//$Author = iconv("big-5", "utf-8", trim($Author));
			$Author = trim($Author);
			$con2 = "AND b.Author like '%{$Author}%'";
		}

		if($Source != "")
		{
			//$Source = iconv("big-5", "utf-8", trim($Source));
			$Source = trim($Source);
			$con3 = "AND b.Source like '%{$Source}%'";
		}

		if($Category != $ParArr["all_category"])
		{
			//$Category = iconv("big-5", "utf-8", $Category);
			$arrTmp = explode(" -> ", $Category);
			if (sizeof($arrTmp)==2)
			{
				$ThisCat = trim($arrTmp[0]);
				$ThisSubCat = trim($arrTmp[1]);
				$con4 = "AND b.Category = '{$ThisCat}' AND b.SubCategory = '{$ThisSubCat}' ";
			} else
			{
				$con4 = "AND b.Category = '{$Category}'";
			}
		}

		if($Level != $ParArr["all_level"])
		{
			//$Level = iconv("big-5", "utf-8", $Level);
			$con5 = "AND b.Level = '{$Level}'";
		}

		if($checkWorksheets != $ParArr["checkWorksheets"])
		{
			$con6 = "";
		}

		if($startdate != "" && $enddate != "")
		{
			$con7 = "AND b.DateModified BETWEEN '$startdate' AND '$enddate'";
		}


		if ($ISBN!="")
		{
			$con8 = " AND b.ISBN like '%{$ISBN}%' ";
		}

		if ($Tag!="")
		{
			$con9 = " AND t.TagName like '%{$Tag}%' ";
		}

		$conOB = "ORDER BY b.{$sortField} {$sortOrder}";


		$sql = "
		SELECT
		DISTINCT b.BookID, b.Title, b.Author, b.Publisher, b.Category, b.SubCategory, b.Source, b.Level, b.BookFormat, b.IsTLF, b.ISBN
		FROM
		INTRANET_ELIB_BOOK AS b LEFT JOIN INTRANET_ELIB_BOOK_TAG AS bt ON bt.BookID=b.BookID LEFT JOIN INTRANET_ELIB_TAG AS t ON t.TagID=bt.TagID
		WHERE b.Publish = 1
		$con1
		$con2
		$con3
		$con4
		$con5
		$con6
		$con7
		$con8
		$con9
		$conOB
		";

		$returnArr = $this->returnArray($sql);
		return $returnArr;
	} // end function get book detail

	function removeMyFavourite($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["currUserID"];

		$sql = "DELETE FROM INTRANET_ELIB_BOOK_MY_FAVOURITES WHERE BookID = {$BookID} AND UserID = {$currUserID}";

		return $this->db_db_query($sql);
	} // end function remove my favourite

	function getBookNotesTitle_ByUserID($ParArr="")
	{
		$currUserID = $ParArr["currUserID"];

		$sql = "
		SELECT
		DISTINCT (a.Title), a.BookID
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		";

		$returnArr = $this->returnArray($sql, 2);

		return $returnArr;
	} // end function get book notes title by userID

	function getBookNotesCategory_ByUserID($ParArr="")
	{
		$currUserID = $ParArr["currUserID"];

		$sql = "
		SELECT
		DISTINCT (b.Category)
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		";

		$returnArr = $this->returnArray($sql, 1);

		return $returnArr;
	} // end function get book notes title by userID

	function addMyFavourite($ParArr="")
	{
		$id = "";

		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["currUserID"];

		// check exist
		$sql = "
		SELECT
			FavouriteID
		FROM
			INTRANET_ELIB_BOOK_MY_FAVOURITES
		WHERE
			BookID = {$BookID}
		AND
			UserID = {$currUserID}
		";


		$returnArr = $this->returnArray($sql, 1);

		$is_fave = $this->IS_MY_FAVOURITE_BOOK($ParArr);

		//if(count($returnArr) <= 0)
		if(!$is_fave)
		{
		$sql = "
		INSERT INTO INTRANET_ELIB_BOOK_MY_FAVOURITES
			(BookID, UserID, DateModified)
		VALUES
			('".$BookID."', '".$currUserID."', now())
		";
		echo $sql;
		$this->db_db_query($sql);
		$id = $this->db_insert_id();
		}

		return $id;
	} // end funciton add my favourite

	function getCountSubCategory($ParArr="")
	{
	$sql = "
		SELECT
		Language,
		Category, SubCategory,
		Count(SubCategory) as count_subcategory
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		GROUP BY
		category, SubCategory
		ORDER BY category
		";

		$returnArr = $this->returnArray($sql, 4);
		return $returnArr;
	} // end function get Count SubCategory

	function getCountCategory($ParArr="")
	{
	$sql = "
		SELECT
		Language,
		Category,
		Count(Category) as count_category
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		GROUP BY
		Category
		";

		$returnArr = $this->returnArray($sql, 4);

		/* attempt to map language to "chi" or "eng"
		for ($i=0; $i<sizeof($returnArr); $i++)
		{
			if ($returnArr[$i]["Language"]!="chi" && $returnArr[$i]["Language"]!="eng")
			{
				if (str_lang($returnArr[$i]["Language"])=="CHI")
				{
					$returnArr[$i]["Language"] = "chi";
					$returnArr[$i][0] = "chi";
				} else
				{
					$returnArr[$i]["Language"] = "eng";
					$returnArr[$i][0] = "eng";

				}
			}
		}
		*/

		return $returnArr;
	} // end function get Count Category


	function getAllBookCatalogue()
	{
		$sql = "
		SELECT
		BookID, Category, SubCategory, Language
		FROM
		INTRANET_ELIB_BOOK
		WHERE
		Publish = 1
		";
		$returnArr = $this->returnArray($sql, 4);
		return $returnArr;
	} // end function get all book catalogue

	function getBookInformation($ParArr="")
	{
		$BookID = IntegerSafe($ParArr["BookID"]);

		$sql = "
		SELECT
		    BookID, Title, Author, Publisher, Category, SubCategory, RelevantSubject, Source, Level, DateModified, Preface, AdultContent, Publish, IsTLF, BookFormat, ISBN
		FROM
		    INTRANET_ELIB_BOOK
		WHERE
		    BookID = '".$BookID."'
		";

		//debug_r($sql);

		$returnArr = $this->returnArray($sql, 11);
		return $returnArr;
	} // end function get book informaiton by ID


	function getFieldName($selectSearchType)
	{
		$x = "";
		switch($selectSearchType)
		{
			case 0:
			$x = "Title";
			break;

			case 1:
			$x = "Author";
			break;

			case 2:
			$x = "Publisher";
			break;

			case 4:
			$x = "TAG";
			break;

			case 5:
			$x = "ISBN";
			break;

			default:
			break;
		}
		return $x;
	} // end function get field name

	function getNormalSearch($ParArr="")
	{
    	$selectSearchType = $ParArr["selectSearchType"];

    	$selectSearchType = $this->getFieldName($selectSearchType);

		//$keywords = iconv("big-5","utf-8", $ParArr["keywords"]);
		$keywords = $ParArr["keywords"];

		$con1 = "";
		if ($selectSearchType == "TAG")
		{
			$sql = "
			SELECT
			b.BookID, b.Title, b.Author, b.Publisher, b.Category, b.SubCategory, b.Source, b.Level, b.BookFormat, b.IsTLF, b.ISBN
			FROM
			INTRANET_ELIB_BOOK AS b, INTRANET_ELIB_BOOK_TAG AS bt, INTRANET_ELIB_TAG AS t
			WHERE b.Publish = 1 AND t.TagName LIKE '%$keywords%' AND bt.TagID=t.TagID AND bt.BookID=b.BookID
			";
		} else
		{
			if($selectSearchType != "")
			{
				$con1 .= " AND b.{$selectSearchType} LIKE '%$keywords%'";
			}
			else
			{
				$con1 .= "AND (";
				$con1 .= " b.Title LIKE '%$keywords%'";
				$con1 .= " OR b.Author LIKE '%$keywords%'";
				$con1 .= " OR b.Publisher LIKE '%$keywords%'";
				$con1 .= ")";
			}

			/*
			if($BookTitle != "")
			{
				//$BookTitle = iconv("big-5", "utf-8", $BookTitle);
				$con1 = "AND b.Title = '{$BookTitle}'";
			}
			*/

			$sql = "
			SELECT
			b.BookID, b.Title, b.Author, b.Publisher, b.Category, b.SubCategory, b.Source, b.Level, b.BookFormat, b.IsTLF, b.ISBN
			FROM
			INTRANET_ELIB_BOOK AS b
			WHERE b.Publish = 1
			$con1
			";
		}

		$returnArr = $this->returnArray($sql, 8);
		return $returnArr;
	} // end function get normal search


	function getBookQueryList($ParArr="", $sql="")
	{
		$sortFieldArray = $ParArr["sortFieldArray"];
		$numField = $ParArr["sortFieldArray"];

		$returnArr = $this->returnArray($sql, $numField);

		//debug_r($returnArr);

		return $returnArr;
	} // end function get book query list

	function displayTableHeading($ParArr="", $eLib="")
	{

		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$CurrentPageNum = $ParArr["CurrentPageNum"];
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	$totolRecord = $ParArr["totalNum"];

    	if($CurrentPageNum * $DisplayNumPage > $totolRecord)
		{
			$CurrentPageNum = ceil($totolRecord / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}

    	$pageDisplay = $ParArr["pageDisplay"];
    	if($pageDisplay == "none")
    	return "";

		//if($totolRecord > 0)
		//$ReportMsg = $eLib["html"]["record"]." 1 - ".$totolRecord.", ".$eLib["html"]["total"]." ".$totolRecord;
		//else
		//$ReportMsg = $eLib["html"]["record"]." 0, ".$eLib["html"]["total"]." 0";

		if($totolRecord > 0){
			$startRec = 1 + ($CurrentPageNum - 1) * $DisplayNumPage;
			//if($startRec > $totolRecord){
			//	$CurrentPageNum = 1;
			//	$startRec = 1;
			//}

			$endRec = $startRec + $DisplayNumPage - 1;
			if($endRec > $totolRecord)
			$endRec = $totolRecord;

			//$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".($endRec - $startRec + 1);
			$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".$totolRecord;
		}else{
			$RecordMsg = $eLib["html"]["record"]." 0, ".$eLib["html"]["total"]." 0";
		}

		//echo("\n CurrentPageNum: ");
		//var_dump($CurrentPageNum);
		//echo("\n DisplayNumPage: ");
		//var_dump($DisplayNumPage);
		//echo("\n startRec: ");
		//var_dump($startRec);
		//echo("\n endRec: ");
		//var_dump($endRec);
		//echo("\n totolRecord: ");
		//var_dump($totolRecord);


		$xh1 = "<select name=\"selPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		//$xh2 = "<select name=\"selPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		$xh3 = "<select name=\"selDisplayPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		//$xh4 = "<select name=\"selDisplayPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";

		$totalPage = ceil($totolRecord / $DisplayNumPage);

		if($totalPage <= 0)
		$totalPage = 1;

		for($n = 1; $n <= $totalPage; $n++)
		{
			if($n == $CurrentPageNum)
			$xl .= "<option value=\"$n\" selected>$n</option>";
			else
			$xl .= "<option value=\"$n\">$n</option>";
		}
		$xt = "</select>";

		$selectPageTop = $xh1.$xl.$xt;
		//$selectPageBottom = $xh2.$xl.$xt;

		if(5 == $DisplayNumPage){
			$yl .= "<option value=\"5\" selected>5</option>";
		}else{
			$yl .= "<option value=\"5\">5</option>";
		}
		for($n = 10; $n <= 100; $n+=10)
		{
			if($n == $DisplayNumPage)
			$yl .= "<option value=\"$n\" selected>$n</option>";
			else
			$yl .= "<option value=\"$n\">$n</option>";
		}

		$selectDisplayPageTop = $xh3.$yl.$xt;
		//$selectDisplayPageBottom = $xh4.$yl.$xt;

		$t  = "";
		$t .= "<tr><td class=\"tablebottom\">";
		$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
		$t .= "<td align=\"left\" class=\"tabletext\">".$RecordMsg."</td>";
		$t .= "<td align=\"right\">";

		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";

		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$t .= "<tr align=\"center\" valign=\"middle\">";
		$t .= "<td><a href=\"javascript:void()\" onClick=\"prevPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp','','".$image_path."/".$LAYOUT_SKIN."/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_prev_off.gif\" name=\"prevp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp\"></a> <span class=\"tabletext\">".$eLib["html"]["page"]."</span></td>";
		$t .= "<td class=\"tabletext\">".$selectPageTop."</td>";
		$t .= "<td><a href=\"javascript:void()\" onClick=\"nextPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp','','".$image_path."/".$LAYOUT_SKIN."/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_next_off.gif\" name=\"nextp\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp\"></a></td>";
		$t .= "</tr></table>";
		$t .= "</td>";
		$t .= "<td>&nbsp;<img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\"></td>";
		$t .= "<td>";

		$t .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"tabletext\"><tr>";
		$t .= "<td>".$eLib["html"]["display2"]."</td>";
		$t .= "<td>".$selectDisplayPageTop."</td>";
		$t .= "<td>/ ".$eLib["html"]["page"]." </td>";
		$t .= "</tr></table>";

		$t .= "</td></tr></table>";

		$t .= "</td></tr></table>";
		$t .= "</td></tr>";

		return $t;
	} // end function display Table heading

	function displayTableTail($ParArr="", $eLib="")
	{

		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
    	$CurrentPageNum = $ParArr["CurrentPageNum"];
    	$DisplayNumPage = $ParArr["DisplayNumPage"];
    	$totolRecord = $ParArr["totalNum"];

    	$pageDisplay = $ParArr["pageDisplay"];
    	if($pageDisplay == "none")
    	return "";

		//if($totolRecord > 0)
		//$ReportMsg = $eLib["html"]["report"]." 1 - ".$totolRecord.", ".$eLib["html"]["total"]." ".$totolRecord;
		//else
		//$ReportMsg = $eLib["html"]["report"]." 0, ".$eLib["html"]["total"]." 0";

		if($totolRecord > 0){
			$startRec = 1 + ($CurrentPageNum - 1) * $DisplayNumPage;
			//if($startRec > $totolRecord){
			//	$CurrentPageNum = 1;
			//	$startRec = 1;
			//}

			$endRec = $startRec + $DisplayNumPage - 1;
			if($endRec > $totolRecord)
			$endRec = $totolRecord;

			//if($endRec == 0)
			//{
			//	$startRec = 0;
			//	$diffRec = 0;
			//}
			//else
			//$diffRec = $endRec - $startRec + 1;

			//$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".$diffRec;
			$RecordMsg = $eLib["html"]["record"]." ".$startRec." - ".$endRec.", ".$eLib["html"]["total"]." ".$totolRecord;
		}else{
			$RecordMsg = $eLib["html"]["record"]." 0, ".$eLib["html"]["total"]." 0";
		}

		//$xh1 = "<select name=\"selPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		$xh2 = "<select name=\"selPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		//$xh3 = "<select name=\"selDisplayPageTop\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";
		$xh4 = "<select name=\"selDisplayPageBottom\" class=\"formtextbox\" onChange=\"sortFunction(this);\">";

		$totalPage = ceil($totolRecord / $DisplayNumPage);

		if($totalPage <= 0)
		$totalPage = 1;

		for($n = 1; $n <= $totalPage; $n++)
		{
			if($n == $CurrentPageNum)
			$xl .= "<option value=\"$n\" selected>$n</option>";
			else
			$xl .= "<option value=\"$n\">$n</option>";
		}
		$xt = "</select>";

		//$selectPageTop = $xh1.$xl.$xt;
		$selectPageBottom = $xh2.$xl.$xt;

		if(5 == $DisplayNumPage){
			$yl .= "<option value=\"5\" selected>5</option>";
		}else{
			$yl .= "<option value=\"5\">5</option>";
		}
		for($n = 10; $n <= 100; $n+=10)
		{
			if($n == $DisplayNumPage)
			$yl .= "<option value=\"$n\" selected>$n</option>";
			else
			$yl .= "<option value=\"$n\">$n</option>";
		}

		//$selectDisplayPageTop = $xh3.$yl.$xt;
		$selectDisplayPageBottom = $xh4.$yl.$xt;

		$t .= "<tr><td class=\"tablebottom\">";
		$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
		$t .= "<td align=\"left\" class=\"tabletext\">".$RecordMsg."</td>";
		$t .= "<td align=\"right\">";
		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
		$t .= "<td>";
		$t .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$t .= "<tr align=\"center\" valign=\"middle\">";
		$t .= "<td><a href=\"javascript:void()\" onClick=\"prevPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('prevp1','','".$image_path."/".$LAYOUT_SKIN."/icon_prev_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_prev_off.gif\" name=\"prevp1\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"prevp1\"></a> <span class=\"tabletext\">".$eLib["html"]["page"]." </span></td>";
		$t .= "<td class=\"tabletext\">".$selectPageBottom."</td>";
		$t .= "<td><a href=\"javascript:void()\" onClick=\"nextPage();\" class=\"tablebottomlink\" onMouseOver=\"MM_swapImage('nextp1','','".$image_path."/".$LAYOUT_SKIN."/icon_next_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_next_off.gif\" name=\"nextp1\" width=\"11\" height=\"10\" border=\"0\" align=\"absmiddle\" id=\"nextp1\"></a></td>";
		$t .= "</tr>";
		$t .= "</table>";
		$t .= "</td>";
		$t .= "<td>&nbsp;<img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\"></td>";
		$t .= "<td>";
		$t .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"tabletext\">";
		$t .= "<tr>";
		$t .= "<td>".$eLib["html"]["display2"]."</td>";
		$t .= "<td>".$selectDisplayPageBottom."</td>";
		$t .= "<td>/ ".$eLib["html"]["page"]." </td>";
		$t .= "</tr>";
		$t .= "</table>";
		$t .= "</td></tr></table>";
		$t .= "</td></tr></table>";
		$t .= "</td></tr>";

		return $t;
	} // end function display table tail

	function displayBookDetail($ParArr="", $eLib="", $sql="", $ShowOtherLinks=true, $SortingLinks=true)
	{
		global $page_size, $intranet_root, $PATH_WRT_ROOT, $objInstall, $Lang, $eLib_plus, $LAYOUT_SKIN;

		if (!isset($objInstall))
		{
			include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
			$objInstall 	= new elibrary_install();
		}
		// display mode 1 - full detail
		// display mode 2 - list mode
		// display mode 3 - review mode

		//debug_r($ParArr);
		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$DisplayMode = $ParArr["DisplayMode"];
		$ReviewDisplayMode = $ParArr["ReviewDisplayMode"];
		if ($ReviewDisplayMode=="")
		{
			$ReviewDisplayMode = 1;
		}
    	$CurrentPageNum = $ParArr["CurrentPageNum"];

    	$DisplayNumPage = $ParArr["DisplayNumPage"];

    	if($DisplayNumPage == "")
    	{
    		$DisplayNumPage = $page_size;
    		$ParArr["DisplayNumPage"] = $DisplayNumPage;
		}

    	$curUserID = $ParArr["currUserID"];

    	// for sorting
    	$sortField = $ParArr["sortField"];
    	$sortFieldOrder = $ParArr["sortFieldOrder"];
    	$sortFieldArr = $ParArr["sortFieldArray"];

    	// for normal search
    	$selectSearchType = $ParArr["selectSearchType"];
		$keywords = $ParArr["keywords"];

		$x = "";

		if($sql == "")
		{
			if($selectSearchType == "" && $keywords == "")
				$returnArr = $this->getBookDetail($ParArr);
			else
				$returnArr = $this->getNormalSearch($ParArr);
		}
		else
		{
			$returnArr = $this->getBookQueryList($ParArr, $sql);
		}

// 		debug_r($returnArr);

		$totalNum = count($returnArr);

		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}

		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;
		$endIndex = $startIndex + $DisplayNumPage;

		if($endIndex > $totalNum)
		$endIndex = $totalNum;

		$ParArr["totalNum"] = $totalNum;

		if($DisplayMode == 1)
		{
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= $this->displayTableHeading($ParArr, $eLib);

			//$AllTags = $this->returnAvailableTag(true, false);

			if($totalNum > 0)
			{
				for($i = $startIndex; $i < $endIndex; $i++)
				{
					$BookID = $returnArr[$i]["BookID"];
					//$Title = iconv("utf-8","big-5",$returnArr[$i]["Title"]);
					//$Author = iconv("utf-8","big-5",$returnArr[$i]["Author"]);
					//$Source = iconv("utf-8","big-5",$returnArr[$i]["Source"]);
					//$Category = iconv("utf-8","big-5",$returnArr[$i]["Category"]);
					//$SubCategory = iconv("utf-8","big-5",$returnArr[$i]["SubCategory"]);
					//$Publisher = iconv("utf-8","big-5",$returnArr[$i]["Publisher"]);
					$Title = $returnArr[$i]["Title"];
					$Author = $returnArr[$i]["Author"];
					$Source = $returnArr[$i]["Source"];
					$Category = $returnArr[$i]["Category"];
					$SubCategory = $returnArr[$i]["SubCategory"];
					$Publisher = $returnArr[$i]["Publisher"];
					$Level = $returnArr[$i]["Level"];
					$IsTLF = $returnArr[$i]["IsTLF"];
					$BookFormat = $returnArr[$i]["BookFormat"];
					$ISBN = $returnArr[$i]["ISBN"];

					$Source = $this->getSourceFullName($Source);

					if ($Level == "")
						$Level = "--";

					$tmpParArr["BookID"] = $BookID;
					$reviewArr = $this->getReview($tmpParArr);
					$reviewNum = count($reviewArr);

					$sumRating = 0;
					for($c = 0; $c < $reviewNum ; $c++)
					{
						$sumRating += $reviewArr[$c]["Rating"];
					}
					if($reviewNum != 0)
					$rating = ceil($sumRating / $reviewNum);
					else
					$rating = 0;

					if (file_exists($intranet_root."/file/elibrary/content/{$BookID}/image/cover.jpg"))
					{
						$coverFile = "/file/elibrary/content/".$BookID."/image/cover.jpg";
					} else
					{
						$coverFile = "{$image_path}/{$LAYOUT_SKIN}/eLibrary/cover_no_image.jpg";
					}
					$x .= "<tr><td>";
					$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";
					$x .= "<tr>\n";
					$x .= "<td width=\"110\" align=\"center\" valign=\"top\">\n";
					$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
					$x .= "<tr>\n";
					$x .= "<td><div id=\"book_border\"><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "COVER")."\" title=\"".$objInstall->get_book_action_title("COVER")."\" ><img src=\"".$coverFile."\"  height=\"135\" border=\"0\" /></a></div></td>\n"; // Charles Ma 2013-1-31 --- width=\"100\"
					$x .= "</tr>\n";
					$x .= "</table>\n";
					$x .= "<a href=\"eLibrary_popular_bookdetail.htm\" class=\"contenttool\"></a>";
					$x .= "</td>";
					$x .= "<td align=\"left\" valign=\"top\">";
					$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["title"]."</td>";
					$x .= "<td width=\"10\" align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\"><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\" title=\"".$objInstall->get_book_action_title("TITLE")."\" ><strong>".$Title."</strong></a></span></td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["author"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					if ($ShowOtherLinks)
						$x .= "<td align=\"left\" class=\"tabletext\"><a href=\"elib_advanced_search_result.php?selectSearchType=1&keywords=".$Author."\" class=\"eLibrary_commonlink\"><strong>".$Author."</strong></a></td>";
					else
						$x .= "<td align=\"left\" class=\"tabletext\">".$Author."</td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["publisher"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					if ($ShowOtherLinks)
						$x .= "<td align=\"left\" class=\"tabletext\"><a href=\"elib_advanced_search_result.php?selectSearchType=2&keywords=".$Publisher."\" class=\"eLibrary_commonlink\">".$Publisher."</a></td>";
					else
						$x .= "<td align=\"left\" class=\"tabletext\">".$Publisher."</td>";
					$x .= "</tr>";
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["category"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";

					if ($Category!="" && $SubCategory!="")
					{
						$Categorys = $Category." -> ". $SubCategory;
					} elseif ($Category!="")
					{
						$Categorys = $Category;
					} elseif ($SubCategory!="")
					{
						$Categorys = $SubCategory;
					} else
					{
						$Categorys = "--";
					}

					//elib_catalogue_detail.php??BookLanguage=eng&Category=Readers&SubCategory=Human%20Interest
					if ($ShowOtherLinks)
						$x .= "<td align=\"left\" class=\"tabletext\"><a href=\"elib_catalogue_detail.php?Category=".urlencode($Category)."&SubCategory=".urlencode($SubCategory)."\" class=\"eLibrary_commonlink\">".$Categorys."</a></td>";
					else
						$x .= "<td align=\"left\" class=\"tabletext\">".$Categorys."</td>";
					$x .= "</tr>";
					/*
					 $x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["source"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><span class=\"eLibrary_commonlink\">".$Source."</span></td>";
					$x .= "</tr>";
					*/
					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["level"]."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\">".$Level."</td>";
					$x .= "</tr>";

					if($ISBN){
    					$x .= "<tr>";
    					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$eLib["html"]["ISBN"]."</td>";
    					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
    					$x .= "<td align=\"left\" class=\"tabletext\">".$ISBN."</td>";
    					$x .= "</tr>";
					}

					$tagName = $this->returnTagNameByBook($BookID);
					$BookTags = $this->displayTags($tagName, true);

					$x .= "<tr>";
					$x .= "<td width=\"110\" align=\"left\" class=\"tabletext\">".$Lang['StudentRegistry']['Tag']."</td>";
					$x .= "<td align=\"left\" class=\"tabletext\"><b>:</b></td>";
					$x .= "<td align=\"left\" class=\"tabletext\">".$BookTags."</td>";
					$x .= "</tr>";
					$x .= "</table>";
					$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";

					//// draw star ////////////////////////////////////////////////////////////////////////////////////////
					$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
					for ($star = 1; $star <= 5; $star++)
					{
						if($star <= $rating)
						{
						$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_on.gif\" width=\"15\" height=\"20\">";
						}
						else
						{
						if($star <= $rating + 0.5)
						$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_half.gif\" width=\"15\" height=\"20\">";
						else
						$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_off.gif\" width=\"15\" height=\"20\">";
						}
					} // end for loop star
					$x .= "</td></tr></table>";
					////////////////////////////////////////////////////////////////////////////////////////////////////////

					$x .= "</td>";
					$x .= "<td>";
					$x .= "<a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\" class=\"contenttool\">(".$reviewNum." <img src=\"$image_path/$LAYOUT_SKIN/eLibrary/icon_comment.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">)</a>";
					$x .= "</td></tr></table>";
					$x .= "</td></tr></table>";
					$x .= "</td></tr>";

					if($i < $endIndex - 1)
					{
					$x .= "<tr>";
					$x .= "<td height=\"5\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"5\"></td>";
					$x .= "</tr>";
					}
				} // end for loop row
			} // end if check total record nun
			else
			{
				$x .= "<tr class=\"tablerow1\">";
				$x .= "<td class=\"tabletext\" align=\"center\">".$eLib["html"]["no_record"]."</td>";
				$x .= "</tr>";
			}
		} // end if display mode = 1
		else if ($DisplayMode == 2)
		{
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= "<tr><td class=\"tablebottom\">";
			$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$t .= "<tr class=\"tabletop\">";

			for($s = 0; $s < count($sortFieldArr) ; $s++)
			{
				# don't show source
				if ($s!=5)
				{
					$value = $sortFieldArr[$s]["value"];
					$text = $sortFieldArr[$s]["text"];

					if($sortField == $value && $sortFieldOrder == "ASC")
					$order = "a";
					else
					$order = "d";

					if ($SortingLinks)
					{
						$t .= "<td><a href=\"javascript:void()\" class=\"tabletoplink\" onClick=\"sortOrder('".$value."');\"";

						if($sortField == $value)
						{
						$t .= " onMouseOver=\"MM_swapImage('sort".$s."','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_on.gif',1)\"";
						$t .= " onMouseOut=\"MM_swapImgRestore()\">".$text."<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_off.gif\"";
						$t .= " name=\"sort".$s."\" width=\"13\" height=\"13\" border=\"0\" align=\"absmiddle\" id=\"sort".$s."\">";
						}
						else
						$t .= ">".$text;

						$t .= "</a></td>";
					} else
					{
						$t .= "<td>".$text."</td>";
					}
				}

			} // end for loop sort
			$t .= "</tr>";

			if($totalNum > 0)
			{
				for($i = $startIndex; $i < $endIndex; $i++)
				{
					$BookID = $returnArr[$i]["BookID"];
					$BookFormat = $returnArr[$i]["BookFormat"];
					$IsTLF = $returnArr[$i]["IsTLF"];

					$row = "";
					for($k = 0; $k < count($sortFieldArr); $k++)
					{
						$value = $sortFieldArr[$k]["value"];
						//$row[$k] = iconv("utf-8","big-5",$returnArr[$i][$value]);
						$row[$k] = $returnArr[$i][$value];

						if($value == "UserName")
							$row[$k] = $returnArr[$i][$value];

						if($value == "Source")
						{
							# $row[$k] = $this->getSourceFullName($row[$k]);
						}
					}

					if($i % 2 == 0)
						$x .= "<tr class=\"tablerow1\">";
					else
						$x .= "<tr class=\"tablerow2\">";

					for($k = 0; $k < count($sortFieldArr); $k++)
					{
						$width = $sortFieldArr[$k]["width"];
						$value = $sortFieldArr[$k]["value"];

						if($width == "")
						$width = floor(100 / count($sortFieldArr))."%";

						# don't show source
						if ($k != 5)
						{
							if ($k < 2 && $value == "Title")
								$x .= "<td width=".$width."><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "COVER")."\" title=\"".$objInstall->get_book_action_title("COVER")."\" ><img src=\"/images/".$LAYOUT_SKIN."/eLibrary/icon_book.png\" border=\"0\" align=\"absmiddle\" /></a> <a href=\"javascript:void()\" class=\"tablelink\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\" title=\"".$objInstall->get_book_action_title("TITLE")."\" >".$row[$k]."</a></td>";
							else
							$x .= "<td width=".$width." class=\"tabletext\">".$row[$k]."</td>";
						}
					}

					$x .= "</tr>";
				} // end for loop row
			} // end if check total record num
			else
			{
				$x .= "<tr class=\"tablerow1\">";

				for($n = 0; $n < count($sortFieldArr) ; $n++)
				{
					if($n == floor(count($sortFieldArr) / 2))
					$x .= "<td class=\"tabletext\">".$eLib["html"]["no_record"]."</td>";
					else
					$x .= "<td class=\"tabletext\">&nbsp;</td>";
				}
				$x .= "</tr>";
			}
				$x .= "</table>";
				$x .= "</td></tr>";
		} // end display mode = 2
		else if($DisplayMode == 3)
		{
			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= $this->displayTableHeading($ParArr, $eLib);

			$x = "";
			$x = "<tr><td align='center'>";

			if($totalNum > 0)
			{
			for($i = $startIndex; $i < $endIndex; $i++)
			{
				$tmpUserID =  $returnArr[$i]["UserID"];
				$BookID = $returnArr[$i]["BookID"];
				$ReviewID = $returnArr[$i]["ReviewID"];
				//$Content = iconv("utf-8","big-5",$returnArr[$i]["Content"]);
				$Content = $returnArr[$i]["Content"];
				$IsTLF = $returnArr[$i]["IsTLF"];
				$BookFormat = $returnArr[$i]["BookFormat"];
				// remove the tag
				$Content = BadWordFilter(strip_tags($Content));

				$Rating = $returnArr[$i]["Rating"];
				$Date = $returnArr[$i]["Date"];
				//$Title = iconv("utf-8","big-5",$returnArr[$i]["Title"]);
				$Title = $returnArr[$i]["Title"];
				$coverFile = (($BookID != "" && file_exists("../file/elibrary/content/".$BookID."/image/cover.jpg"))&&($objInstall->check_if_readable($BookID)))? "/file/elibrary/content/".$BookID."/image/cover.jpg" : $image_path."/".$LAYOUT_SKIN."/eLibrary/cover_no_image.jpg";

				$isAnswer = $this->getHelpfulCheck($ReviewID);
				$helpfulArr = $this->getHelpfulVote($ReviewID);
				$numTotalVote = $helpfulArr["total"];
				$numUsefulVote = $helpfulArr["yesNum"];
			 	$helpfulStr = $this->getHelpfulMessage($eLib, $numTotalVote, $numUsefulVote);


			 	# new design - review, book cover

				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td valign=\"top\">";

				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>";
				$x .= "<td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>";
				$x .= "<td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04.gif\">&nbsp;</td>";
				$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td align=\"right\" class=\"tabletext forumtablerow\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr><td align=\"left\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td class=\"tabletextremark\">".$eLib["html"]["rating"].":</td>";

				//// draw star ////////////////////////////////////////////////////////////////////////////////////////
				for ($star = 1; $star <= 5; $star++)
				{
					if($star <= $Rating)
					{
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_on.gif\" width=\"15\" height=\"20\"></td>";
					}
					else
					{
					if($star <= $Rating + 0.5)
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_half.gif\" width=\"15\" height=\"20\"></td>";
					else
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_off.gif\" width=\"15\" height=\"20\"></td>";
					}
				} // end for loop star
				////////////////////////////////////////////////////////////////////////////////////////////////////////

				$x .= "</tr></table></td>";
				$x .= "<td align=\"right\" class=\"tabletext\">".$Date."</td>";
				$x .= "</tr></table></td></tr>";
				$x .= "<tr><td class=\"tabletext\">".nl2br($Content)."</td></tr>";
				$x .= "<tr>";
				$x .= "<td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td height=\"5\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td height=\"10\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
				$x .= "<tr>";
				$x .= "<td class=\"tabletext\">".$helpfulStr."</td>";

				if($curUserID != $tmpUserID && $isAnswer == false)
				{
					$x .= "<td align=\"right\" class=\"tabletext\">";
					$x .= $eLib["html"]["was_this_review_helpful"]."<br />";
					$x .= "<input name=\"yes_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview('yes','".$ReviewID."','".$BookID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["yes"]."\">&nbsp;";
					$x .= "<input name=\"no_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview('no','".$ReviewID."','".$BookID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["no"]."\">";
					$x .= "</td>";
				} else
					$x .= "<td align=\"right\">&nbsp;</td>";

				$x .= "</tr></table></td>";
				$x .= "</tr></table></td>";


				$x .= "<td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_06.gif\">&nbsp;</td>";

				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>";
				$x .= "<td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "<td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "</tr></table>";

				$x .= "</td><td valign=\"top\" width=\"100\" >";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr><td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				if($objInstall->check_if_readable($BookID)){
					$x .= "<td><div id=\"book_border\"><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "COVER")."\" title=\"".$objInstall->get_book_action_title("COVER")."\" ><span><img src=\"".$coverFile."\" width=\"100\" height=\"135\" border=\"0\"></span></a></div></td>";
				}else{
					$x .= "<td><div id=\"book_border\"><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\"title=\"".$eLib_plus["html"]["expiredBook"]["onCoverRemark"]."\" ><span><img src=\"".$coverFile."\" width=\"100\" height=\"135\" border=\"0\"></span></a></div></td>";
				}
				$x .= "</tr></table></td></tr>";
				$x .= "<tr><td>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				if($objInstall->check_if_readable($BookID)){
					$x .= "<tr><td align=\"center\" class=\"tabletext\"><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\" title=\"".$objInstall->get_book_action_title("TITLE")."\" class=\"eLibrary_commonlink\">".$Title."</a></td></tr>";
				}else{
					$x .= "<tr><td align=\"center\" class=\"tabletext\"><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\" title=\"".$eLib_plus["html"]["expiredBook"]["onCoverRemark"]."\" class=\"eLibrary_commonlink\">".$Title."</a><br>".$eLib_plus["html"]["expiredBook"]["onCoverRemark"]."</td></tr>";
				}
				$x .= "</table></td></tr>";
				$x .= "</table>";

				$x .= "</td></tr></table><br />";

			/*
			 	# old design - book cover, review
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td width=\"100\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr><td align=\"center\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td><div id=\"book_border\"><a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=".$BookID."','bookdetail','scrollbars=yes')\"><span><img src=\"".$coverFile."\" width=\"100\" height=\"135\" border=\"0\"></span></a></div></td>";
				$x .= "</tr></table></td></tr>";
				$x .= "<tr><td>";
				$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$x .= "<tr><td align=\"center\" class=\"tabletext\"><a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=".$BookID."','bookdetail','scrollbars=yes')\" class=\"eLibrary_commonlink\">".$Title."</a></td></tr>";
				$x .= "</table></td></tr>";
				$x .= "</table></td>";
				$x .= "<td align=\"left\" valign=\"top\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>";
				$x .= "<td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>";
				$x .= "<td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04.gif\">&nbsp;</td>";
				$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td align=\"right\" class=\"tabletext forumtablerow\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr><td align=\"left\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td class=\"tabletextremark\">".$eLib["html"]["rating"].":</td>";

				//// draw star ////////////////////////////////////////////////////////////////////////////////////////
				for ($star = 1; $star <= 5; $star++)
				{
					if($star <= $Rating)
					{
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_on.gif\" width=\"15\" height=\"20\"></td>";
					}
					else
					{
					if($star <= $Rating + 0.5)
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_half.gif\" width=\"15\" height=\"20\"></td>";
					else
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_off.gif\" width=\"15\" height=\"20\"></td>";
					}
				} // end for loop star
				////////////////////////////////////////////////////////////////////////////////////////////////////////

				$x .= "</tr></table></td>";
				$x .= "<td align=\"right\" class=\"tabletext\">".$Date."</td>";
				$x .= "</tr></table></td></tr>";
				$x .= "<tr><td class=\"tabletext\">".$Content."</td></tr>";
				$x .= "<tr>";
				$x .= "<td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td height=\"5\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td height=\"10\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
				$x .= "<tr>";
				$x .= "<td class=\"tabletext\">".$helpfulStr."</td>";

				if($curUserID != $tmpUserID && $isAnswer == false)
				{
					$x .= "<td align=\"right\" class=\"tabletext\">";
					$x .= $eLib["html"]["was_this_review_helpful"]."&nbsp;";
					$x .= "<input name=\"yes_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview('yes','".$ReviewID."','".$BookID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["yes"]."\">&nbsp;";
					$x .= "<input name=\"no_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview('no','".$ReviewID."','".$BookID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["no"]."\">";
					$x .= "</td>";
				}
				else
				$x .= "<td align=\"right\">&nbsp;</td>";

				$x .= "</tr></table></td>";
				$x .= "</tr></table></td>";


				$x .= "<td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_06.gif\">&nbsp;</td>";

				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>";
				$x .= "<td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "<td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "</tr></table></td></tr></table><br />";
			*/
			} // end for loop
		}// if check total num < 0
			else
			{
				$x .= "<tr class=\"tablerow1\">";
				$x .= "<td class=\"tabletext\" align=\"center\">".$eLib["html"]["no_record"]."</td>";
				$x .= "</tr>";
			}
			$x .= "</td></tr>";
		} // end display mode = 3 for review table
		else if ($DisplayMode == 4)
		{
		} // End displaymode 4 -> class book summary
		else
		{
			$t = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= $this->displayTableHeading($ParArr, $eLib);
		} // no display mode select


	$z = $this->displayTableTail($ParArr, $eLib);

	$z .= "</table>";

	$tmpArr["DisplayNumPage"] = $DisplayNumPage;
	$tmpArr["contentHtml"] = $t.$x.$z;
	$tmpArr["totalRecord"] = count($returnArr);
	return $tmpArr;
} // end function display Book Detail

function displayClassSummaryDetail($ParArr="", $eLib="", $sql="")
	{
		global $page_size;

		$image_path = $ParArr["image_path"];
		$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
		$DisplayMode = $ParArr["DisplayMode"];
    	$CurrentPageNum = $ParArr["CurrentPageNum"];

    	$DisplayNumPage = $ParArr["DisplayNumPage"];

    	if($DisplayNumPage == "")
    	{
    		$DisplayNumPage = $page_size;
    		$ParArr["DisplayNumPage"] = $DisplayNumPage;
		}

    	$curUserID = $ParArr["currUserID"];

    	// for sorting
    	$sortField = $ParArr["sortField"];
    	$sortFieldOrder = $ParArr["sortFieldOrder"];
    	$sortFieldArr = $ParArr["sortFieldArray"];

    	// for normal search
    	$selectSearchType = $ParArr["selectSearchType"];
		$keywords = $ParArr["keywords"];

		$x = "";

		if($sql == "")
		{
			if($selectSearchType == "" && $keywords == "")
			$returnArr = $this->getBookDetail($ParArr);
			else
			$returnArr = $this->getNormalSearch($ParArr);
		}
		else
		{
			$returnArr = $this->getBookQueryList($ParArr, $sql);
		}

//		debug_r($returnArr);

		$totalNum = count($returnArr);

		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}

		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;
		$endIndex = $startIndex + $DisplayNumPage;

		if($endIndex > $totalNum)
		$endIndex = $totalNum;

		$ParArr["totalNum"] = $totalNum;

		$sortPageBy = ($sortFieldOrder)? 0 : 1;

			$t = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			$t .= "<tr><td class=\"tablebottom\">";
			$t .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$t .= "<tr class=\"tabletop\">";

			for($s = 0; $s < count($sortFieldArr) ; $s++)
			{
				$value = $sortFieldArr[$s]["value"];
				$text = $sortFieldArr[$s]["text"];

				if($sortField == $value && $sortFieldOrder == 0)
					$order = "a";
				else
					$order = "d";


					$t .= "<td";
				$t .= ($s > 1)? ' colspan= "2" ' : '';
				$t .= ">";
				if( $s <2)
					$t .= "<a href=\"javascript:void()\" class=\"tabletoplink\" onClick=\"sortPage(".$sortPageBy.", ".$s.", document.form1);\"";

				if($sortField == $value && $s< 2)
				{
				$t .= " onMouseOver=\"MM_swapImage('sort".$s."','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_on.gif',1)\"";
				$t .= " onMouseOut=\"MM_swapImgRestore()\">".$text."<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_off.gif\"";
				$t .= " name=\"sort".$s."\" width=\"13\" height=\"13\" border=\"0\" align=\"absmiddle\" id=\"sort".$s."\">";
				}
				else{
					$t .= ($s <2)? ">".$text: $text ;
				}

				if( $s <2)
					$t .= "</a>";

				$t .= "</td>";

			} // end for loop sort
				$t .= "</tr>";
				$t .= "<tr class=\"tabletop\">";
			$sortFieldArr2 = $ParArr["sortFieldArray2"];

			for($s = 0; $s < count($sortFieldArr2) ; $s++)
				{
					$value = $sortFieldArr2[$s]["value"];
					$text = $sortFieldArr2[$s]["text"];

					if($sortField == $value && $sortFieldOrder == 0)
					$order = "a";
					else
					$order = "d";

					$t .= ( $s >1)? "<td class='subtitle'>" : "<td>";
					if( $s >1)
						$t .= "<a href=\"javascript:void()\" class=\"tabletoplink\" onClick=\"sortPage(".$sortPageBy.", ".$s.", document.form1);\"";

					if($sortField == $value && $s > 1)
					{
					$t .= " onMouseOver=\"MM_swapImage('sort".$s."','','".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_on.gif',1)\"";
					$t .= " onMouseOut=\"MM_swapImgRestore()\">".$text."<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_sort_".$order."_off.gif\"";
					$t .= " name=\"sort".$s."\" width=\"13\" height=\"13\" border=\"0\" align=\"absmiddle\" id=\"sort".$s."\">";
					}
					else
					$t .= ($s > 1)? ">".$text: $text ;



					if( $s >1)
						$t .= "</a>";

					$t .= "</td>";

				} // end for loop sort
					$t .= "</tr>";

			if($totalNum > 0)
			{
				for($i = $startIndex; $i < $endIndex; $i++)
				{
					$BookID = $returnArr[$i]["BookID"];
					$YearClassID = $returnArr[$i]["YearClassID"];

					$row = "";
					for($k = 0; $k < count($sortFieldArr2); $k++)
					{
						$value = $sortFieldArr2[$k]["value"];
						//$row[$k] = iconv("utf-8","big-5",$returnArr[$i][$value]);
						$row[$k] = $returnArr[$i][$value];

						if($value == "UserName")
						$row[$k] = $returnArr[$i][$value];

						if($value == "Source")
						$row[$k] = $this->getSourceFullName($row[$k]);
					}

					if($i % 2 == 0)
					$x .= "<tr class=\"tablerow1\">";
					else
					$x .= "<tr class=\"tablerow2\">";

					for($k = 0; $k < count($sortFieldArr2); $k++)
					{
						$width = $sortFieldArr2[$k]["width"];
						$value = $sortFieldArr2[$k]["value"];

						if($width == "")
						$width = floor(100 / count($sortFieldArr2))."%";

						if($k == 0 && $value == "Title")
							$x .= "<td width=".$width."><a href=\"javascript:void()\" class=\"tablelink\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\">".$row[$k]."</a></td>";
						else if($value == "ClassName")
							$x .= "<td width=".$width." class=\"tabletext\"><a href=\"elib_public_students_summary.php?YearClassID=".($YearClassID)."&ClassName=".($row[$k])."\">".$row[$k]."</a></td>";
						else
							$x .= "<td width=".$width." class=\"tabletext\">".$row[$k]."</td>";
					}

					$x .= "</tr>";
				} // end for loop row
			} // end if check total record num
			else
			{
				$x .= "<tr class=\"tablerow1\">";

				for($n = 0; $n < count($sortFieldArr) ; $n++)
				{
					if($n == floor(count($sortFieldArr) / 2))
					$x .= "<td class=\"tabletext\">".$eLib["html"]["no_record"]."</td>";
					else
					$x .= "<td class=\"tabletext\">&nbsp;</td>";
				}
				$x .= "</tr>";
			}
				$x .= "</table>";
				$x .= "</td></tr>";
		$z = $this->displayTableTail($ParArr, $eLib);

	$z .= "</table>";

	$tmpArr["DisplayNumPage"] = $DisplayNumPage;
	$tmpArr["contentHtml"] = $t.$x.$z;
	$tmpArr["totalRecord"] = count($returnArr);
	return $tmpArr;
}


function printSearchInput($ParArr="", $eLib="")
{
	global $selectSearchType, $Lang;
	$selectedArr = Array();

	$image_path = $ParArr["image_path"];
	$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
	$isSearch = $_SESSION["elib"]["is_search"];
	$searchWord = $_SESSION["elib"]["search_word"];
	if ($selectSearchType!="")
	{
		 $_SESSION["elib"]["is_search_select"] = $selectSearchType;
	}
	$is_search_select = $_SESSION["elib"]["is_search_select"];

	$selectedArr[$is_search_select] = "selected";
	if (!strstr($_SERVER["SCRIPT_NAME"], "/elib_advanced_search_result.php"))
	{
		$isSearch = 0;
		//debug($_SERVER["SCRIPT_NAME"]);
	}
	if($isSearch == 1)
	{
		$className = "tabletext";
		$checkInput = "";
		$searchTxt = $searchWord;
	}
	else
	{
		$className = "tabletextremark";
		$checkInput = "checkInputText(this);";
		$searchTxt = $eLib["html"]["search_input"];
	}


	$x = "";
	$x .= "<form name=\"searchForm\" action=\"elib_advanced_search_result.php\" method=\"post\" onSubmit=\"return checkSearchForm(this);\">";
	$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"30\" align=\"right\" >";
	$x .= "<tr>";
	$x .= "<td width=\"25\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\"></td>";
	$x .= "<td align=\"left\" >";
	$x .= "<select name=\"selectSearchType\" >";
	$x .= "<option value=\"0\" ".$selectedArr[0].">".$eLib['html']['book_title']."</option>";
	$x .= "<option value=\"1\" ".$selectedArr[1].">".$eLib["html"]["author"]."</option>";
	$x .= "<option value=\"2\" ".$selectedArr[2].">".$eLib["html"]["publisher"]."</option>";
	$x .= "<option value=\"3\" ".$selectedArr[3].">".$eLib["html"]["keywords"]."</option>";
	$x .= "<option value=\"4\" ".$selectedArr[4].">".$Lang['StudentRegistry']['Tag']."</option>";
	$x .= "<option value=\"5\" ".$selectedArr[5].">".$eLib["html"]["ISBN"]."</option>";
	$x .= "</select>";
	$x .= "&nbsp;<input onFocus=\"".$checkInput."\" name=\"keywords\" type=\"text\" class=\"".$className."\" value=\"".$searchTxt."\" size=\"45\" /></td>";
	$x .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\">";
	//$x .= "<input type=\"button\"  class=\"formbutton_v30 print_hide\" value=\"".$eLib["html"]["search"]."\"   />";
	$x .= "<input type=\"button\" class=\"formsubbutton\" onClick=\"this.form.submit()\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["search"]."\">";
	$x .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\">| <img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"10\"><a href=\"elib_advanced_search_form.php\" class=\"contenttool\">{$eLib['html']['advance_search']}</a></td>";
	$x .= "</tr>";
	$x .= "</table>";
	$x .= "<input type=\"hidden\" name=\"isSearch\" value=\"".$isSearch."\" />";
	$x .= "<input type=\"hidden\" name=\"isSearchSelect\" value=\"".$is_search_select."\" />";
	$x .= "</form>";

	return $x;
} // end function print search input

	function getReview($ParArr="")
	{
    	$BookID = $ParArr["BookID"];

		$sql = "
		SELECT
		a.ReviewID, a.UserID, a.Rating, a.Content, a.DateModified,
		b.EnglishName, b.ChineseName, b.ClassName, b.ClassNumber
		FROM
		INTRANET_ELIB_BOOK_REVIEW a, INTRANET_USER b
		WHERE
		a.BookID = '{$BookID}'
		AND a.UserID = b.UserID
		ORDER BY a.ReviewID DESC
		";

		//debug_r($sql);

		$returnArr = $this->returnArray($sql, 5);

		//debug_r($returnArr);

		return $returnArr;
	} // end function get review

	function addBookReview($ParArr="")
	{
		$Rating = $ParArr["Rating"];
		$Content = htmlspecialchars($ParArr["Content"]);
		$BookID = $ParArr["BookID"];
		$currUserID = $_SESSION["UserID"];

		//$Content = iconv("BIG-5", "UTF-8", $Content);

		$sql = "
		INSERT INTO
		INTRANET_ELIB_BOOK_REVIEW
		(BookID, UserID, Rating, Content, DateModified)
		VALUES
		('".$BookID."', '".$currUserID."', '".$Rating."', ".$this->pack_value($Content,'str').", now())
		";

		$this->db_db_query($sql);
		$id = $this->db_insert_id();

		return $id;
	} // end function add book review

	function addBookRecommend($ParArr="")
	{
		$Description = $ParArr["Description"];
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["currUserID"];
		//$ClassLevelIDArr = $ParArr["ClassLevelID"];
		$RecommendGroup = $ParArr["ClassLevelID"];
		/*$x = "";

		for($i = 0; $i < count($ClassLevelIDArr); $i++)
		{
			$sep = "|";
			$x .= $sep.$ClassLevelIDArr[$i];
		}
		$x .= "|";

		$RecommendGroup = $x;*/

		$sql = "
		INSERT INTO
		INTRANET_ELIB_BOOK_RECOMMEND
		(BookID, UserID, Description, RecommendGroup, DateModified)
		VALUES
		('".$BookID."', '".$currUserID."', ".$this->pack_value($Description).", '".$RecommendGroup."', now())
		";

		if($this->db_db_query($sql))
			return $this->db_insert_id();
		else
			return;
	} // end function add book recommend

	function getBookRecommend($ParArr="")
	{
		$BookID = $ParArr["BookID"];

		$sql = "
		SELECT
		Description, RecommendGroup
		FROM
		INTRANET_ELIB_BOOK_RECOMMEND
		WHERE
		BookID = '".$BookID."'
		";

		$returnArr = $this->returnArray($sql, 2);

		return $returnArr;
	} // end function get book recommend

	function editBookRecommend($ParArr="")
	{
		$Description = $ParArr["Description"];
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["currUserID"];
		//$ClassLevelIDArr = $ParArr["ClassLevelID"];
		$RecommendGroup = $ParArr["ClassLevelID"];
		/*$x = "";

		for($i = 0; $i < count($ClassLevelIDArr); $i++)
		{
			$sep = "|";
			$x .= $sep.$ClassLevelIDArr[$i];
		}
		$x .= "|";

		$RecommendGroup = $x;*/
		if($RecommendGroup == ""){
			$sql = " DELETE FROM INTRANET_ELIB_BOOK_RECOMMEND " .		//20140326-deletebookrecommend
					" WHERE BookID = '".$BookID."' ";
		}else{
			$sql = "
			UPDATE
				INTRANET_ELIB_BOOK_RECOMMEND
			SET
				UserID = '".$currUserID."',
				Description = ".$this->pack_value($Description).",
				RecommendGroup = '".$RecommendGroup."',
				DateModified = now()
			WHERE
				BookID = '".$BookID."'
				LIMIT 1
			";
		}

		return $this->db_db_query($sql);
	} // end function edit book recommend

	function addBookReviewHelpful($ParArr="")
	{
		$ReviewID 	= $ParArr["ReviewID"];
		$BookID 	= $ParArr["BookID"];
		$Choose 	= $ParArr["Choose"];
		$currUserID 	= $_SESSION["UserID"];

		if($BookID != "" && $ReviewID != "" && $Choose != "" && $currUserID != "" && !$this->getHelpfulCheck($ReviewID))
		{

			$sql = "
				INSERT INTO
				INTRANET_ELIB_BOOK_REVIEW_HELPFUL
				(BookID, UserID, Choose, DateModified, ReviewID)
				VALUES
				('".$BookID."', '".$currUserID."', '".$Choose."', now(), '".$ReviewID."')
			";

			$this->db_db_query($sql);
			return $this->db_insert_id();
		}

		return;
	} // end function add book review

	function getHelpfulCheck($ReviewID="")
	{
		$curUserID = $_SESSION["UserID"];

		$sql = "
		SELECT
		ReviewCommentID
		FROM
		INTRANET_ELIB_BOOK_REVIEW_HELPFUL
		WHERE
		ReviewID = '{$ReviewID}'
		AND UserID = '{$curUserID}'
		";

		//debug_r($sql);

		$returnArr = $this->returnArray($sql, 1);

		//debug_r($returnArr);
		if(count($returnArr) > 0)
		return true;
		else
		return false;
	} // end function get helpful check

	function getHelpfulVote($ReviewID="")
	{
		$sql = "
		SELECT
		ReviewCommentID, Choose, UserID
		FROM
		INTRANET_ELIB_BOOK_REVIEW_HELPFUL
		WHERE
		ReviewID = '{$ReviewID}'
		GROUP BY
		UserID
		";

		$returnArr = $this->returnArray($sql, 3);

		$tmpArr["total"] = count($returnArr);

		$countYes = 0;
		for($i = 0; $i < count($returnArr); $i++)
		{
			if($returnArr[$i]["Choose"] == "1")
			$countYes++;
		}
		$tmpArr["yesNum"] = $countYes;

		return $tmpArr;
	} // end function get helpful vote

	function GET_CLASSNAME()
	{
		//$sql = "SELECT a.ClassID,a.ClassName FROM INTRANET_CLASS AS a WHERE a.RecordStatus=1 ORDER BY a.ClassName";

		$yid = Get_Current_Academic_Year_ID();
		$sql = "
			SELECT
				YearClassID, ClassTitleEN
			FROM
				YEAR_CLASS
			WHERE
				AcademicYearID = '{$yid}'
			ORDER BY
				YearID
			ASC
		";
		$returnArr = $this->returnArray($sql,2);

		//var_dump($returnArr);
		return $returnArr;
	} // end function get className

	function GET_STUDENT_NAME_BY_CLASS($ParArr="")
	{
		$ClassName = $ParArr["ClassName"];

		$sql = "
		SELECT
		UserID, EnglishName, ChineseName
		FROM
		INTRANET_USER
		WHERE
		ClassName = '{$ClassName}'
		ORDER BY
		EnglishName ASC, ChineseName ASC
		";

		$returnArr = $this->returnArray($sql, 3);

		return $returnArr;
	} // end function get student name by class

	function GET_STUDENT_NAME_BY_ID($ParArr="")
	{
		$StudentID = $ParArr["StudentID"];

		$sql = "
		SELECT
		ClassNumber, EnglishName, ChineseName
		FROM
		INTRANET_USER
		WHERE
		UserID = '{$StudentID}'
		";

		$returnArr = $this->returnArray($sql, 3);

		return $returnArr;
	} // end function get student name by ID

 	function displayReviewTableHeading($ParArr="", $eLib="")
 	{
    	$returnArr = $this->getReview($ParArr);

    	$x = "";

    	if($returnArr != "")
    	{
	    	$ParArr["totalNum"] = count($returnArr);
	 		$x = $this->displayTableHeading($ParArr, $eLib);
 		}

	 	return $x;
 	} // end function print review table

 	function displayReviewTable($ParArr="", $eLib="", $sql="")
 	{
	 	global $intranet_root, $objInstall, $Lang, $LAYOUT_SKIN;

	 	$image_path 	= $ParArr["image_path"];
		$LAYOUT_SKIN 	= $ParArr["LAYOUT_SKIN"];
		$curUserID 		= $ParArr["currUserID"];
		$CurrentPageNum = $ParArr["CurrentPageNum"];
    	$DisplayNumPage = $ParArr["DisplayNumPage"];

    	$IsAdmin = $this->IS_ADMIN_USER($_SESSION["UserID"]);

    	if($sql == "")
	 		$returnArr = $this->getReview($ParArr);
	 	else
	 		$returnArr = $this->getBookQueryList($ParArr, $sql);

	 	$ParArr["totalNum"] = count($returnArr);

	 	$totalNum = count($returnArr);

		if($CurrentPageNum * $DisplayNumPage > $totalNum)
		{
			$CurrentPageNum = ceil($totalNum / $DisplayNumPage);
			$ParArr["CurrentPageNum"] = $CurrentPageNum;
		}

		$startIndex = ($CurrentPageNum - 1) * $DisplayNumPage;

		if($startIndex < 0)
		$startIndex = 0;

		$endIndex = $startIndex + $DisplayNumPage;

		if($endIndex > $totalNum || $totalNum == 0)
		$endIndex = $totalNum;

	 	//debug_r($returnArr);

	 	$x = "";

	 	if($returnArr != "")
	 	{
		 	for($i = $startIndex; $i < $endIndex; $i++)
		 	{
			 	$currUserID = $returnArr[$i]["UserID"];

			 	$li = new libuser($currUserID);
				$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
				if ($li->PhotoLink !="")
				{
					if (is_file($intranet_root.$li->PhotoLink))
					{
						$photo_link = $li->PhotoLink;
					}
				}

				//$headImg = "images/cimg/ecomm/basketball_club/student/Chan Siu Man.jpg";
				$headImg = "$photo_link";

				$ReviewID = $returnArr[$i]["ReviewID"];
				$EnglishName = $returnArr[$i]["EnglishName"];
				$ChineseName = $returnArr[$i]["ChineseName"];
				$ClassName = $returnArr[$i]["ClassName"];
				$ClassNumber = $returnArr[$i]["ClassNumber"];

				//debug_r($_SESSION);
				if($_SESSION["intranet_session_language"] == "b5")
				{
					$UserName1 = $ChineseName;
					$UserName2 = $EnglishName;
				}
				else
				{
					$UserName1 = $EnglishName;
					$UserName2 = $ChineseName;
				}


				$UserName 	= ($UserName1 != "")? $UserName1 : $UserName2;
				$nameStr 	= ($ClassName != "")? $UserName." (".$ClassName."-".$ClassNumber.")" : $UserName;

				$date 		= $returnArr[$i]["DateModified"];
				$content 	= BadWordFilter(strip_tags($returnArr[$i]["Content"]));
				$tmpUserID 	= $returnArr[$i]["UserID"];
				$rating 	= $returnArr[$i]["Rating"];

				$isAnswer		= $this->getHelpfulCheck($ReviewID);
				$voteArr 		= $this->getHelpfulVote($ReviewID);
				$numTotalVote 	= $voteArr["total"];
				$numUsefulVote 	= $voteArr["yesNum"];
				$helpfulStr 	= $this->getHelpfulMessage($eLib, $numTotalVote, $numUsefulVote);

				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td width=\"100\">";
				$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
				$x .= "<tr><td width=\"120\" align=\"center\">";
				$x .= "<table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><td>";
				$x .= "<div id=\"div\"><span><img src=\"".$headImg."\" width=\"54\" height=\"70\" border=\"0\"></span> </div></td>";
				$x .= "</tr></table>";
				$x .= "</td></tr>";
				$x .= "<tr><td width=\"120\" align=\"center\" class=\"tabletext\">".$nameStr."</td></tr>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "<td align=\"left\" valign=\"top\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td width=\"25\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>";
				$x .= "<td height=\"7\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>";
				$x .= "<td width=\"9\" height=\"7\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>";
				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" valign=\"top\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_04b.gif\" width=\"25\" height=\"40\"></td>";
				$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				$x .= "<tr>";
				$x .= "<td align=\"right\" class=\"tabletext forumtablerow\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
				$x .= "<td align=\"left\">";
				$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
				$x .= "<td class=\"tabletextremark\" >".$eLib["html"]["rating"].":</td>";

				for($j = 1 ; $j <= 5; $j++)
				{
					if($j <= $rating)
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_on.gif\" width=\"15\" height=\"20\"></td>";
					else
					$x .= "<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/star_off.gif\" width=\"15\" height=\"20\"></td>";
				}

				$x .= "</tr></table>";
				$x .= "</td>";

				// 2013-11-20 Charles Ma 2013-11-14-deleteallbookcomment
				$remove_by_admin = ($IsAdmin) ? "<a href='javascript:removeReview($ReviewID)' title='".$eLib["html"]["Delete"]."'><img src=\"/images/".$LAYOUT_SKIN."/icon_delete.gif\" border='\"0\" />'</a>" : "";

				$x .= "<td align=\"right\" class=\"tabletext\">".$date. " {$remove_by_admin} </td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= "</td>";
				$x .= "</tr>";
				$x .= "<tr><td class=\"tabletext\">".nl2br($content)."</td></tr>";
				$x .= "<tr><td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"10\"></td></tr>";
				$x .= "<tr><td height=\"5\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"5\"></td></tr>";
				$x .= "<tr><td height=\"10\" class=\"tabletext\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";
				$x .= "<td class=\"tabletext\" id='helpful_display_".$ReviewID."'>".$helpfulStr."</td>";
				$x .= "<td align=\"right\" class=\"tabletext\" id='helpful_vote_panel_".$ReviewID."'>";

				if($curUserID != $tmpUserID && $isAnswer == false)
				{
					$x .= $eLib["html"]["was_this_review_helpful"]."&nbsp;";
					$x .= "<input name=\"yes_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview(1,'".$ReviewID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["yes"]."\">&nbsp;";
					$x .= "<input name=\"no_btn\" type=\"button\" class=\"formsubbutton\" onClick=\"voteReview(0,'".$ReviewID."'); \"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" value=\"".$eLib["html"]["no"]."\">";
				}

				$x .= "</td></tr></table>";
				$x .= "</td></tr></table>";
				$x .= "</td>";

				/*Edited - <img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/ecomm/forum_bubble_re_06.gif\" width=\"9\" height=\"80\" border='none' />*/
				$x .= "<td width=\"9\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_06.gif\">&nbsp;</td>";

				$x .= "</tr>";
				$x .= "<tr>";
				$x .= "<td width=\"25\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>";
				$x .= "<td height=\"10\" background=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "<td width=\"9\" height=\"10\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>";
				$x .= "</tr></table>";
				$x .= "</td></tr></table>";
				$x .= "<br />";
	 		} // end for loop
	 	}

	 	//$t = $this->displayReviewTableHeading($ParArr, $eLib);
	 	if ($totalNum>0)
	 	{
	 		$y = $this->displayTableTail($ParArr, $eLib);
	 	}

	 	$script_x = "<script>
	 	function removeReview(ReviewID)
	 	{
	 		if (confirm('".$Lang['SysMgr']['RemoveReview']."'))
	 		{
	 			self.location = 'remove_review.php?BookID=".$ParArr["BookID"]."&ReviewID='+ReviewID;
	 		}
	 	}
	 	</script>";
	 	$tmpArr["content"] = $x.$y.$script_x;
	 	$tmpArr["totalRecord"] = $totalNum;

	 	return $tmpArr;
 	} // end function display review table

 	function displayRecordListMenu($ParArr="", $eLib="")
 	{
	 	global $UserType,$UserID;

		/*2010-01-20 : New navigation style */
			$image_path = $ParArr["image_path"];
			$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
			$currUserID = $ParArr["currUserID"];

		 	$x .= "";
			$x .= "<div id=\"list1\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_history.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reading_history"]."</a></p></div>";
			## If user is student , display "My Book" Menu tab
			if($UserType == 2)
				$x .= "<div id=\"list7\"><p><a href=\"javascript:void()\">"."<a href=\"elib_my_books.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_books"]."</a></p></div>";

			$x .= "<div id=\"list2\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_favourite.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_favourites"]."</a></p></div>";
			$x .= "<div id=\"list3\"><p><a href=\"javascript:void()\">"."<a href=\"elib_public_students_summary_review.php?myUserID=".$UserID."\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reviews"]."</a></p></div>";
			$x .= "<div id=\"list4\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_mynotes.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_notes"]."</a></p></div>";

			$x .= "<div id=\"list5\"><p><a href=\"javascript:void()\">"."<a href=\"elib_public_all_reviews.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["all_reviews_2"]."</a></p></div>";

			if($this->IS_TEACHER() || $this->IS_ADMIN())
			{
			$x .= "<div id=\"list6\"><p><a href=\"javascript:void()\">"."<a href=\"elib_public_class_summary.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["class_summary"]."</a></p></div>";
			}

			return $x;
 	} // end function display record list menu


 	function newDisplayRecordListMenu($ParArr="", $eLib="")
	 	{
		 	$image_path = $ParArr["image_path"];
			$LAYOUT_SKIN = $ParArr["LAYOUT_SKIN"];
			$currUserID = $_SESSION["UserID"];

		 	$x .= "";
			$x .= "<div id=\"list1\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_history.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reading_history"]."</a></p></div>";
			$x .= "<div id=\"list2\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_favourite.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_favourites"]."</a></p></div>";
			$x .= "<div id=\"list3\"><p><a href=\"javascript:void()\">"."<a href=\"elib_public_students_summary_review.php?myUserID=".$currUserID."\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_reviews"]."</a></p></div>";
			$x .= "<div id=\"list4\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_mynotes.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["my_notes"]."</a></p></div>";



			//modified by Addam (2008.11.06) - delete the icons and categories
			///////////////////////////////////////////////////////////////////
			//$x .= "</table>";
			//$x .= "</td>";
			//$x .= "<td background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/myrecord_board_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"16\" height=\"110\"></td>";
			//$x .= "</tr>";
			//$x .= "<tr>";
			//$x .= "<td width=\"12\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/myrecord_board_ex_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"12\" height=\"4\"></td>";
			//$x .= "<td valign=\"middle\" background=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/myrecord_board_05.gif\">";
			//$x .= "<span class=\"eLibrary_myrecord_list_title\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_public.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">".$eLib["html"]["public"]."</span>";
			//$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			///////////////////////////////////////////////////////////////////

			$x .= "<div id=\"list5\"><p><a href=\"javascript:void()\">"."<a href=\"elib_public_all_reviews.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["all_reviews_2"]."</a></p></div>";


			if($this->IS_TEACHER() || $this->IS_ADMIN())
			{
			$x .= "<div id=\"list6\"><p><a href=\"javascript:void()\">"."<a href=\"elib_record_mynotes.php\" class=\"eLibrary_myrecord_list\">".$eLib["html"]["student_summary"]."</a></p></div>";

			}

			return $x;
	 	} // end function display record list menu


 	function getHelpfulMessage($eLib="", $numTotalVote=0, $numUsefulVote=0)
 	{
	 	$x = ($numTotalVote>0) ? "<strong>".$numUsefulVote."</strong> ".$eLib["html"]["of"]." ".$numTotalVote." ".$eLib["html"]["people_found_this_review_helpful"] : "&nbsp;";
	 	return $x;
 	} // end function

 	/*
	* Check if the user is guest
	*/
	function IS_GUEST($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_reportcard_usertype'];
		}

		$ReturnVal = ($ParUserType=="GUEST");

		return $ReturnVal;
	}

	/*
	* Check if the user is student
	*/
	function IS_STUDENT($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_elibrary_usertype'];
		}

		$ReturnVal = ($ParUserType=="STUDENT");

		return $ReturnVal;

	}

	/*
	* Check if the user is teacher
	*/
	function IS_TEACHER($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_elibrary_usertype'];
		}

		$ReturnVal = ($ParUserType=="TEACHER");

		return $ReturnVal;

	}

	/*
	* Check if the user is system admin
	*/
	function IS_ADMIN($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_elibrary_usertype'];
		}

		$ReturnVal = ($ParUserType=="ADMIN");

		return $ReturnVal;
	}

	function GET_ADMIN_USER()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/elibrary/admin_user.txt"));

		return $AdminUser;
	}


	function IS_ADMIN_USER($ParUserID)
	{
		global $intranet_root;

		$AdminUser = $this->GET_ADMIN_USER();

		$IsAdmin = 0;
		if(!empty($AdminUser))
		{
			$AdminArray = explode(",", $AdminUser);
			$IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
		}

		return $IsAdmin;
	}

	function IS_MY_FAVOURITE_BOOK($ParArr="")
	{
		$BookID = $ParArr["BookID"];
		$currUserID = $ParArr["currUserID"];

		$sql = "
				SELECT BookID
				FROM
				INTRANET_ELIB_BOOK_MY_FAVOURITES
				WHERE
				BookID = {$BookID} AND UserID = {$currUserID}
			   ";

		$returnArr = $this->returnArray($sql, 1);

		if(count($returnArr) > 0)
		return true;
		else
		return false;

	} // end function


 	//Modified by Josephine
	function newPrintMostActiveReviewers($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;

		$lang = $_SESSION["intranet_session_language"];

		//$x = "<table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">";

		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			//$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];

			if($ClassName != "")
			$ClassName = "(".$ClassName.")";

			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;

				$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
				$lang1 = $UserName_b5;

				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}

			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;

			if(trim($UserName) == "")
			$UserName = "--";

			$rank = $i + 1;

			if($rank <= 5)
			$rankBG = "top_no_".$rank."a.gif";
			else
			$rankBG = "top_no_b.gif";

			/*
			if($rank == 1){
				$rank = $rank."st";
			}else if($rank == 2){
				$rank = $rank."nd";
			}else if($rank == 3){
				$rank = $rank."rd";
			}else {
				$rank = $rank."th";
			}*/

			$x .= "<div class=\"left01\">" .
						"<p class=\"text01\">" .
						"<strong>&nbsp;".$rank." &nbsp;"."</strong>" .
						"<a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('elib_top_reviewers.php?ReviewerID=$currUserID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\">".$UserName." ".$ClassName."</a>" .
						"</p>" .
					"</div>";

			$x .= "<div class=\"review_icon\">" .
						"<label title=\"No.of Review\">".
						"<p>".$Count."</p>".
						"</label>".
					"</div>";

			$x .= "<div class=\"line\"></div>";

		}

		return $x;
	} // end function print Most active reviewers

	//Josephine

	//Modified by Josephine 2009/10/29
	function newPrintBookTable($data="", $image_path="", $LAYOUT_SKIN="", $NumOfBook="", $type="", $eLib="")
	{
		global $PATH_WRT_ROOT, $objInstall;

		if (!isset($objInstall))
		{
			include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
			$objInstall 	= new elibrary_install();
		}

		# Init variables
		$count = 0;
		$countRowBlock = 0;



		# Calculate the book image dimensions
		switch($NumOfBook){
			case 4:
				$width 		= ($type=="recommend")? 80 : 120;
				$height 	= ($type=="recommend")? 100: 155;
				$maxRow 	= 2;
				break;
			case 9:
				$width 		= ($type=="recommend")? 40 : 80;
				$height 	= ($type=="recommend")? 60: 100;
				$maxRow 	= 3;
				break;
			case 'na':
				$width 		= 150;
				$height 	= 225;
				$maxRow 	= 4;
				$NumOfBook = count($data);
				break;
			default:
				$width 		= 200;
				$height 	= 285;
				$maxRow 	= 1;
				break;
		}
		# Number of books per row

		$height_div = $height + 30;

		$tdWidth = round(100/$maxRow);

		# check if recommend table
		if(count($data) <= 0 && $type == "recommend")
		{
			$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			$x .= "<tr><td align=\"center\" class=\"tabletext\">";
			$x .= "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
			$x .= $eLib["html"]["no_recommend_book"];
			$x .= "</td></tr>";
			$x .= "</table>";

			return $x;
		}

		# Generate Book display
		$x .= '<table width="100%" border="0"><tr>';

		for($j = 0; $j < $NumOfBook ; $j++)
		{
			$BookName 		= $data[$count]["Title"];
			$BookID 		= $data[$count]["BookID"];
			$Description 	= $data[$count]["Description"];
			$IsTLF 			= $data[$count]["IsTLF"];
			$BookFormat 	= $data[$count]["BookFormat"];
			$count++;
			$bookPath 		= "/file/elibrary/content/".$BookID."/image/cover.jpg";

			# Set Book Cover Image path
			$BookCover = (($BookID != "" && file_exists("../../..".$bookPath))&&($objInstall->check_if_readable($BookID)))? $bookPath : $image_path."/".$LAYOUT_SKIN."/eLibrary/cover_no_image.jpg";

			# HTML BookImage + Name

			$x .= '<td width = "'.$tdWidth.'%" align="center" >';
			if($BookID != ""){
				if($type == "recommend")
				{
					//$eLib["action"]['open_book']

					$x .= "
							<label title='$BookName'>
							<a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('book_detail.php?BookID=$BookID','bookdetail','scrollbars=yes')\" onMouseover=\"MM_showHideLayers('recommend_detail_$count','','show'); MM_showRecommendedBook($j)\" onMouseout=\"MM_showHideLayers('recommend_detail_$count','','hide')\" ><img src=\"".$BookCover."\"  height=\"".$height."\" border=\"0\"></a>
							</label>
						   ";
					// Charles Ma 2013-1-31 --- width=\"".$width."\"
				}else if($type == "Readable"){
					$x .= "<div id=\"book_border\" style='width:".$width."px; height:{$height_div}px;  padding:10px;'><a href=\"javascript:void()\" class=\"book_border_href\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "COVER")."\" title=\"".$objInstall->get_book_action_title("COVER")."\" ><img src=\"".$BookCover."\"  height=\"".$height."\" style=\"border: 1px solid #AAAAAA;\" ></a><br/><span><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\" title=\"".$objInstall->get_book_action_title("TITLE")."\" >".$BookName."</a></span></div>";
					// Charles Ma 2013-1-31 --- width=\"".$width."\"
				}else{
					$x .= "<div style='width:100%; height:{$height_div}px; text-align:center;'><a href=\"javascript:void()\" class=\"book_border_href\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "COVER")."\" title=\"".$objInstall->get_book_action_title("COVER")."\" ><img  src=\"".$BookCover."\"  height=\"".$height."\" style=\"border: 1px solid #AAAAAA;\" /></a><br /><div style=\"margin-top:-16px; width:100%; height:10px; text-align:center;\" ><a href=\"javascript:void()\" onClick=\"".$objInstall->get_book_reader_link($BookID, $BookFormat, $IsTLF, $_SESSION['UserID'], "TITLE")."\" title=\"".$objInstall->get_book_action_title("TITLE")."\" >".$BookName."</a></div>";
					// Charles Ma 2013-1-31 --- width=\"".$width."\"
				}
			}
			$x .= '</center></td>';


			$countRowBlock++;
			# Next line
			if($countRowBlock >= $maxRow){
				$countRowBlock = 0;
				$x .= '</tr><tr>';
			}
		} // end for cell


		#Close table row if not Already CLOSED
		if($countRowBlock != 0){
			$x .= '</tr>';
		}
		$x .= "</table>";

		return $x;
	} // end function print table by Josephine


	function printReadableBooks(){

	}

	//modified by Josephine
	function newPrintMostUsefulReviews($data="", $image_path="", $LAYOUT_SKIN=""){
		global $PATH_WRT_ROOT;

		//hdebug_r($data);

		$lang = $_SESSION["intranet_session_language"];

		for($i = 0; $i < count($data) ; $i++)
		{
			$currUserID = $data[$i]["UserID"];
			$ReviewID = $data[$i]["ReviewID"];
			$Count = $data[$i]["countNum"];
			$UserName_b5 = $data[$i]["ChineseName"];
			$UserName_en = $data[$i]["FirstName"]." ".$data[$i]["LastName"];
			$UserName_en2 = $data[$i]["EnglishName"];
			$ClassName = $data[$i]["ClassName"];
			$ClassNumber = $data[$i]["ClassNumber"];
			//$BookName = iconv("UTF-8","BIG-5",$data[$i]["Title"]);
			$BookName = $data[$i]["Title"];

			if($ClassName != "")
			{
				if($ClassNumber != "")
				$ClassName = "(".$ClassName."-".$ClassNumber.")";
				else
				$ClassName = "(".$ClassName.")";
			}

			if($lang == "en")
			{
				if(trim($UserName_en) != "")
					$lang1 = $UserName_en;
				else
					$lang1 = $UserName_en2;

			$lang2 = $UserName_b5;
			}
			else if($lang == "b5")
			{
			$lang1 = $UserName_b5;

				if(trim($UserName_en) != "")
					$lang2 = $UserName_en;
				else
					$lang2 = $UserName_en2;
			}

			if($lang1 == "" || $lang1 == NULL || trim($lang1) == "")
			$UserName = $lang2;
			else
			$UserName = $lang1;

			if(trim($UserName) == "")
			$UserName = "--";

			$rank = $i + 1;

			if($rank <= 5)
			$rankBG = "top_no_".$rank."b.gif";
			else
			$rankBG = "top_no_b.gif";

			/*
			if($rank == 1){
				$rank = $rank."st";
			}else if($rank == 2){
				$rank = $rank."nd";
			}else if($rank == 3){
				$rank = $rank."rd";
			}else if($rank == 4 || $rank == 5){
				$rank = $rank."th";
			}*/


			$x .= "";
			$x .= "<div class=\"left02\">" .
						"<p class=\"text01\">" .
							"<strong>&nbsp;".$rank." &nbsp;"."</strong><a href=\"javascript:void()\" onClick=\"MM_openBrWindowFull('elib_top_reviews.php?ReviewID=$ReviewID&Ranking=".($i+1)."','review_detail','scrollbars=yes')\">".$BookName."</a>" .
						"</p>" .
					"</div>".

					"<div class=\"right\">" .
						"<span class=\"text02\">".$UserName."  ".$ClassName."</span>" .
					"</div>".

					"<div class=\"line\"></div>";

		}

		return $x;
	} // end function print Most active reviewers

	function Get_List_Read_History($BookID){
		global $lang;

		$selName1 = "CONCAT('<a href=\"elib_public_students_read_summary.php?UserID=', a.UserID ,'&ClassName=', a.ClassName,'\">' , if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), a.ChineseName , a.EnglishName), '</a>') as UserName";
		$selName2 = "CONCAT('<a href=\"elib_public_students_read_summary.php?UserID=', a.UserID ,'&ClassName=', a.ClassName,'\">' , if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), a.EnglishName, a.ChineseName), '</a>') as UserName";
		$selName = ($lang == "b5")? $selName1 : $selName2;

		$sql = "SELECT
					$selName,
					a.UserID
				FROM
					INTRANET_USER as a
				INNER JOIN
					INTRANET_ELIB_BOOK_HISTORY as h
				ON
					a.UserID = h.UserID
				WHERE
					h.BookID = ".$BookID;
		echo htmlspecialchars($sql);
		return $this->db_db_query($sql);
	}

	function Get_Book_Hits($BookID)
	{
		$sql = "SELECT COUNT(*) FROM INTRANET_ELIB_BOOK_HISTORY WHERE BookID=". $BookID;
		//$sql = "SELECT HitRate FROM INTRANET_ELIB_BOOK WHERE BookID=". $BookID;
		$rows = $this->returnVector($sql);

		return $rows[0];
	}

	/**
	 * Get the number of helpful review count UI
	 * E.g   0 of 0 people found this Review helpful
	 */
	function getBookReviewHelpfulCountUI($ReviewID){
		global $eLib;

		$voteArr = $this->getHelpfulVote($ReviewID);
		return $this->getHelpfulMessage($eLib, $voteArr["total"], $voteArr["yesNum"]);
	}

	function returnAvailableTag($assocAry=0, $assocAryByID=true)
	{

		$sql = "SELECT TagID, TagName FROM INTRANET_ELIB_TAG ORDER BY TagName ASC";
		$result = $this->returnArray($sql);

		if ($assocAry==1)
		{
			$newAry = array();
			for($i=0; $i<sizeof($result); $i++)
			{
				list($id, $name) = $result[$i];
				if ($assocAryByID)
				{
					$newAry[$id] = $name;
				} else
				{
					$newAry[$name] = $id;
				}
			}
			return $newAry;
		} else
		{
			return $result;
		}
	}

	function returnTagNameByBook($BookID) {
		$sql = "SELECT t.TagName FROM INTRANET_ELIB_TAG t, INTRANET_ELIB_BOOK_TAG bt WHERE bt.BookID='".$BookID."' AND t.TagID=bt.TagID";
		return $this->returnVector($sql);
	}

	function returnTagNameByID($TagID) {
		$sql = "SELECT t.TagName FROM INTRANET_ELIB_TAG t WHERE t.TagID='".$TagID."' ";
		$rows = $this->returnVector($sql);
		return $rows[0];
	}

	function returnPopularTags($Total=-1, $keyword="", $SortByTagName=false)
	{
		$order_cond = ($SortByTagName)?"t.TagName ASC, TotalCount DESC":"TotalCount DESC, t.TagName ASC";
		$sql = "SELECT t.TagName, t.TagID, COUNT(bt.TagID) AS TotalCount FROM INTRANET_ELIB_TAG t LEFT JOIN INTRANET_ELIB_BOOK_TAG bt ON t.TagID=bt.TagID LEFT JOIN INTRANET_ELIB_BOOK AS b ON bt.BookID=b.BookID WHERE b.publish=1 AND b.BookID<'".$this->physical_book_init_value."' ";
		if (trim($keyword)!="")
		{
			$unconverted_keyword_sql = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));
			$converted_keyword_sql = special_sql_str($keyword);
			if ($unconverted_keyword_sql == $converted_keyword_sql){
				$sql .= " AND t.TagName LIKE '%$unconverted_keyword_sql%'";
			}
			else {
				$sql .= " AND (t.TagName LIKE '%$unconverted_keyword_sql%' OR t.TagName LIKE '%$converted_keyword_sql%')";
			}
		}
		$sql .= " GROUP BY t.TagID ORDER BY ".$order_cond." ";
		if ($Total>0)
		{
			$sql .= " LIMIT {$Total} ";
		}

		return $this->returnResultSet($sql);
	}


	function returnTagIDByTagName($tagName='')
	{
		if($tagName!='')
		{
			$tagName = trim($tagName);
			$tagAry = explode(',', $tagName);
			$tempAry = array();

			foreach($tagAry as $tag)
			{
				$tag = trim($tag);
				if ($tag!="")
				{
					$sql = "SELECT TagID FROM INTRANET_ELIB_TAG WHERE TagName='".$this->Get_Safe_Sql_Query($tag)."'";
					$result = $this->returnVector($sql);
					if($result[0]!="") {	# can retrieve TagID from existing data
						$tempAry[] = $result[0];
					} else {				# cannot retrieve TagID, then create a new tag
						$tagid = $this->insertTag($tag);
						$tempAry[] = $tagid;
					}
				}
			}

			return $tempAry;

		}
	}

	function insertTag($TagName)
	{
		global $UserID;

		$TagName = trim($TagName);
		if ($TagName!="")
		{
			$sql = "INSERT INTO INTRANET_ELIB_TAG (TagName, DateInput, InputBy) VALUES ('".$this->Get_Safe_Sql_Query($TagName)."', NOW(), '$UserID')";
			$this->db_db_query($sql);
			$tagid = $this->db_insert_id();

			return $tagid;
		}
	}


	/*
	function displayTags($tagName, $AllTags, $IsLink=false)
	{
		$BookTags = "";

		if(is_array($tagName) && sizeof($tagName)>0)
		{
			if ($IsLink && is_array($AllTags) && sizeof($AllTags)>0)
			{
				for ($i=0; $i<sizeof($tagName); $i++)
				{
					if ($AllTags[$tagName[$i]]>0)
					{
						$BookTags .= (($BookTags!="") ? ", " : "" ) . "<a href=\"elib_list_by_tag.php?TagID=".$AllTags[$tagName[$i]]."\">".$tagName[$i]."</a>";
					}
				}
			} else
			{
				$BookTags = implode(', ', $tagName);
			}
		} else
		{
			$BookTags = "--";
		}
		return $BookTags;
	}
	*/


	function displayTags($tagName, $IsLink=false)
	{
		$BookTags = "";

		if(is_array($tagName) && sizeof($tagName)>0)
		{
			if ($IsLink)
			{
				for ($i=0; $i<sizeof($tagName); $i++)
				{
					$BookTags .= (($BookTags!="") ? ", " : "" ) . "<a href=\"elib_advanced_search_result.php?selectSearchType=4&keywords=".$tagName[$i]."\">".$tagName[$i]."</a>";
				}
			} else
			{
				$BookTags = implode(', ', $tagName);
			}
		} else
		{
			$BookTags = "--";
		}
		return $BookTags;
	}


	function UPDATE_BOOK_TAGS($BookTags, $BookID)
	{
		global $UserID;

		$TagIDArray = $this->returnTagIDByTagName($BookTags);

		# loading existing tags
		$sql = "SELECT TagID FROM INTRANET_ELIB_BOOK_TAG WHERE BookID='".$BookID."'";
		$PreTagIDArray = $this->returnVector($sql);

		# compare and remove old tag(s)
		for ($i=0; $i<sizeof($PreTagIDArray); $i++)
		{
			$IsNeeded = false;
			for ($j=0; $j<sizeof($TagIDArray); $j++)
			{
				if ($TagIDArray[$j]==$PreTagIDArray[$i] && $TagIDArray[$j]!="" && $TagIDArray[$j]>0)
				{
					$IsNeeded = true;
					$TagsKeep[$TagIDArray[$j]] = true;
					break;
				}
			}

			if (!$IsNeeded)
			{
				# delete from DB
				$sql = "DELETE FROM INTRANET_ELIB_BOOK_TAG WHERE BookID='".$BookID."' AND TagID='".$PreTagIDArray[$i]."'";
				$this->db_db_query($sql);
			}
		}

		# add new tags
		if (is_array($TagIDArray) && sizeof($TagIDArray)>0)
		{
			# add or update
			for ($i=0; $i<sizeof($TagIDArray); $i++)
			{
				$tagID = $TagIDArray[$i];

				# add new only
				if (!$TagsKeep[$tagID])
				{
					$sql = "INSERT INTO INTRANET_ELIB_BOOK_TAG (BookID, TagID, DateInput, InputBy) VALUES ('".$BookID."', '".$tagID."', now(), '".$UserID."') ";
					$this->db_db_query($sql);
				}
			}
		}
	}

	function AssignTagToBooks($TagID, $BookIDSelected)
	{
		global $UserID;
		$ret = array();
		if ($TagID!="" && $TagID>0)
		{
			for ($i=0; $i<sizeof($BookIDSelected); $i++)
			{
				$BookID = $BookIDSelected[$i];
				if ($BookID!="" && $BookID>0)
				{
					$sql = "INSERT INTO INTRANET_ELIB_BOOK_TAG (BookID, TagID, DateInput, InputBy) VALUES ('".$BookID."', '".$TagID."', now(), '".$UserID."') ";
					$ret[] = $this->db_db_query($sql);
				}
			}
		}
		return (in_array(false,$ret) ? false : true);
	}


	function DeleteTag($TagID)
	{
		if ($TagID!="" && $TagID>0)
		{
			$sql = "DELETE FROM INTRANET_ELIB_BOOK_TAG WHERE TagID='{$TagID}' ";
			$this->db_db_query($sql);

			$sql = "DELETE FROM INTRANET_ELIB_TAG WHERE TagID='{$TagID}' ";
			$this->db_db_query($sql);
		}
	}


	function DisplayTagMenu()
	{
		global $Lang, $eLib, $LAYOUT_SKIN;

		$TagArr = $this->returnPopularTags(10);

		if (sizeof($TagArr)>0)
		{
			$TagsHtml = '<link rel="stylesheet" href="/templates/'.$LAYOUT_SKIN.'/css/reading_scheme.css" type="text/css" /><!-- eLibrary  tag Layer Begin -->
	                                <div class="reading_board" id="resource_tag" >
	                            <div class="reading_board_top_left" ><div class="reading_board_top_right" >
	                                                <h1>'.$Lang['StudentRegistry']['Tag'].'</h1>
	                            </div></div>

	                                            <div class="reading_board_left"><div class="reading_board_right">
											<div class="tag_list">';

			for ($i=0; $i<sizeof($TagArr); $i++)
			{
				$TagObj = $TagArr[$i];
				$TagsHtml .= "<a href=\"elib_advanced_search_result.php?selectSearchType=4&keywords=".$TagObj["TagName"]."\" class=\"tag_normal\"><font size=\"-1\">".$TagObj["TagName"]." <!--(".$TagObj["TotalCount"].")--></font></a><p class=\"spacer\"></p>";
			}

	        $TagsHtml .= '
	                                                   </div>
	                                                   <p class="spacer"></p>
	                                        <div id="elib_cata_listall"><a class="elib_cata_listall" href="elib_tag_list.php">'.$eLib["html"]["list_all"].'</a></div>
	                                            </div></div>

	                            <div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>

	                               </div>
	                                <!-- eLibrary tag End -->';

			return $TagsHtml;
		}
	}


	function ListAllTagsInTable($keyword)
	{
		global $eLib, $Lang;

		$Tags = $this->returnPopularTags(-1, $keyword);
		$returnRX = '<table class="common_table_list">
                              <thead>
                                <tr>
                                  <th class="num_check">#</th>
                                  <th>'.$Lang['StudentRegistry']['Tag'].'</th>
                                  <th>'.$eLib["html"]["num_of_books"].'</th>
                                  <th>&nbsp;</th>
                                </tr></thead><tbody>';
		for ($i=0; $i<sizeof($Tags); $i++)
		{
			$TagObj = $Tags[$i];
        	$returnRX .= '<tr>
                                  <td>'.($i+1).'</td>
                                  <td >'.$TagObj["TagName"].'</td>
                                  <td>'.$TagObj["TotalCount"].'</td>
                                  <td class="sub_row"><div class="table_row_tool"><a href="elib_setting_tags_new.php?TagID='.$TagObj["TagID"].'" class="edit_dim" title="'.$eLib["html"]['Edit'].'"></a><a href="javascript:JSDeleteTag('.$TagObj["TagID"].')" class="delete_dim" title="'.$eLib["html"]['Delete'].'"></a></div></td>
                                </tr>';
		}
        $returnRX .= '
                              </tbody>
                            </table>';

		return $returnRX;
	}

	function ListAllTagsInPage()
	{
	    global $LAYOUT_SKIN;
		$Tags = $this->returnPopularTags($Total=-1, $keyword="", $SortByTagName=true);

		$item_per_row = 4;

		$returnRX = "<table width='90%' align='center' cellpadding='5' cellspacing='10' border='0' >\n";
		for ($i=0; $i<sizeof($Tags); $i++)
		{
			$TagObj = $Tags[$i];
			if ($i%$item_per_row==0)
			{
				$returnRX .= ($i==0) ? "<tr>" : "</tr><tr>\n";
			}
			$returnRX .= "<td class=\"eLibrary_cat_list\" align=\"center\"><!--<img src=\"/images/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif\" width=\"10\" height=\"20\" border=\"0\" align=\"absmiddle\" />--><a href=\"elib_advanced_search_result.php?selectSearchType=4&keywords=".$TagObj["TagName"]."\">".$TagObj["TagName"]." (".$TagObj["TotalCount"].") </a></td>";
            if ($i==sizeof($Tags)-1)
            {
            	$colspan = $item_per_row - ($i%$item_per_row);
            	if ($colspan>0)
            	{
            		$returnRX .= "<td colspan=\"".$colspan."\">&nbsp;</td>";
            	}
            }
		}
		$returnRX .= "</table>";

		return $returnRX;
	}


	function DisplayTagBookSelections($TagID, $IsSelected, $ObjName)
	{
		if ($IsSelected)
		{
			# find the books added
			$sql = "SELECT b.Title, b.Author, b.BookID, b.Category, b.SubCategory FROM INTRANET_ELIB_BOOK AS b, INTRANET_ELIB_BOOK_TAG AS bt where b.Publish=1 AND b.BookID=bt.BookID AND b.BookID<'".$this->physical_book_init_value."' AND bt.TagID='".$TagID."' ORDER BY b.Category ASC, b.SubCategory ASC, b.Title ASC ";
		} else
		{
			$sql = "SELECT b.BookID FROM INTRANET_ELIB_BOOK AS b, INTRANET_ELIB_BOOK_TAG AS bt where b.Publish=1 AND b.BookID=bt.BookID AND b.BookID<'".$this->physical_book_init_value."' AND bt.TagID='".$TagID."' ";
			$rows = $this->returnVector($sql);

			if (sizeof($rows)>0)
			{
				$book_exclude = implode(",",$rows);
				$cond = " AND BookID NOT IN ({$book_exclude}) ";
			}
			# find the books available to be added
			$sql = "SELECT Title, Author, BookID, Category, SubCategory FROM INTRANET_ELIB_BOOK where Publish=1 {$cond} AND BookID<'".$this->physical_book_init_value."' ORDER BY Category ASC, SubCategory ASC, Title ASC ";
		}

		$rows = $this->returnResultSet($sql);

		$IsCategoryList = array();
		$returnRX = "<select name='$ObjName' id='$ObjName' size='20' multiple>";
		for ($i=0; $i<sizeof($rows); $i++)
		{
			$BookObj = $rows[$i];
			if (!$IsCategoryList[$BookObj["Category"]][$BookObj["SubCategory"]])
			{
				if ($i>0)
				{
					$returnRX .= "</optgroup>\n";
				}
				$returnRX .= "<optgroup label=\"".$BookObj["Category"]." -> ".$BookObj["SubCategory"]."\">\n";
				$IsCategoryList[$BookObj["Category"]][$BookObj["SubCategory"]] = true;
			}
			$returnRX .= "<option value='".$BookObj["BookID"]."'>".$BookObj["Title"]." (".$BookObj["Author"].")</option>\n";
			if ($i==sizeof($rows)-1)
			{
				$returnRX .= "</optgroup><optgroup label=\"*****************************\"></optgroup>\n";
			}
		}
		$returnRX .= "</select>";


		return $returnRX;
	}

	function GetCategories()
	{
		global $plugin, $SchoolBasedCategory, $SchoolBasedShow;

		/*
		$catListArr["chi"][0]["name"] = "category name";
		$catListArr["chi"][0]["subname"][0] = "sub-category name";
		*/
		$sql = "SELECT DISTINCT Category, SubCategory FROM INTRANET_ELIB_BOOK WHERE publish=1 order by Category ASC, SubCategory ASC";
		$rows = $this->returnResultSet($sql);

		$IsOldCategory = array();
		$catListArr = array();

		$sub_index = 0;
		$chi_index = -1;
		$eng_index = -1;
		$sub_index_school = 0;
		$chi_index_school = -1;
		$eng_index_school = -1;

		for ($i=0; $i<sizeof($rows); $i++)
		{
			$Category = trim($rows[$i]["Category"]);
			$SubCategory = trim($rows[$i]["SubCategory"]);
			if ($Category!="" && $SubCategory!="")
			{
				//debug($Category, str_lang($Category));
				$langNow = (str_lang($Category)=="ENG") ? "eng" : "chi";
				if ($SchoolBasedCategory[$Category])
				{
					if ($SchoolBasedShow[$Category])
					{
						# school-based category
						if (!$IsOldCategory[$Category])
						{
							$sub_index_school = 0;
							${$langNow."_index_school"} ++;
							$catListArrSchool[$langNow][${$langNow."_index_school"}]["name"] = $Category;
							$IsOldCategory[$Category] = true;
						}
						$catListArrSchool[$langNow][${$langNow."_index_school"}]["subname"][$sub_index_school] = $SubCategory;
						$sub_index_school ++;
					}
				} else
				{
					# standard category
					if (!$IsOldCategory[$Category])
					{
						$sub_index = 0;
						${$langNow."_index"} ++;
						$catListArr[$langNow][${$langNow."_index"}]["name"] = $Category;
						$IsOldCategory[$Category] = true;
					}
					$catListArr[$langNow][${$langNow."_index"}]["subname"][$sub_index] = $SubCategory;
					$sub_index ++;
				}
			}
		}

		if (is_array($catListArrSchool) && sizeof($catListArrSchool)>0)
		{
			foreach ($catListArrSchool AS $langNow => $carArray)
			{
				for ($i=0; $i<sizeof($carArray); $i++)
				{
					$lang_index = sizeof($catListArr[$langNow]);
					$catListArr[$langNow][$lang_index]["name"] = $carArray[$i]["name"];
					for ($j=0; $j<sizeof($carArray[$i]["subname"]); $j++)
					{
						$catListArr[$langNow][$lang_index]["subname"][$j] = $carArray[$i]["subname"][$j];
					}
				}
			}
		}

		return $catListArr;
	}

	function GetCallNumber($call1,$call2){
		return trim($call1." ".$call2);
	}

	function UPDATE_BOOK_BLANKFIRSTPAGE($ParArr)
	{
		$BookIDArr = $ParArr["BookID"];

		$BookIDField .= "BookID = '".$BookIDArr[0]."'";

		for($i = 1; $i < count($BookIDArr); $i++)
		{
			$BookIDField .= " OR BookID = '".$BookIDArr[$i]."'";
		}

		$sql = "
		UPDATE
		INTRANET_ELIB_BOOK
		SET
		IsFirstPageBlank='".$ParArr["BLANKFIRSTPAGE"]."'
		WHERE
		$BookIDField
		";

		$this->db_db_query($sql);
	} // end funciton UPDATE_BOOK_BLANKFIRSTPAGE

	function UPDATE_BOOK_UNBLANKFIRSTPAGE($ParArr)
	{
		$BookIDArr = $ParArr["BookID"];

		$BookIDField .= "BookID = '".$BookIDArr[0]."'";

		for($i = 1; $i < count($BookIDArr); $i++)
		{
			$BookIDField .= " OR BookID = '".$BookIDArr[$i]."'";
		}

		$sql = "
		UPDATE
		INTRANET_ELIB_BOOK
		SET
		IsFirstPageBlank='".$ParArr["UNBLANKFIRSTPAGE"]."'
		WHERE
		$BookIDField
		";

		$this->db_db_query($sql);
	} // end funciton UPDATE_BOOK_UNBLANKFIRSTPAGE


	function UPDATE_BOOK_RIGHTTOLEFT($ParArr)
	{
		$BookIDArr = $ParArr["BookID"];

		$BookIDField .= "BookID = '".$BookIDArr[0]."'";

		for($i = 1; $i < count($BookIDArr); $i++)
		{
			$BookIDField .= " OR BookID = '".$BookIDArr[$i]."'";
		}

		$sql = "
		UPDATE
		INTRANET_ELIB_BOOK
		SET
		IsRightToLeft='".$ParArr["ISRIGHTTOLEFT"]."'
		WHERE
		$BookIDField
		";

		$this->db_db_query($sql);
	} // end funciton UPDATE_BOOK_BLANKFIRSTPAGE


	function SYNC_BOOK_CATEGORY_TO_ELIB($newCategory)
	{
		global $PATH_WRT_ROOT, $intranet_root, $sys_custom;

		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

		$libms = new liblms();
		$li = new libfilesystem();
		$LangID = $li->file_read($intranet_root."/file/language.txt");
		$LangID += 0;
		if ($LangID==0)
		{
			$LangUsed = "en";
		} elseif ($LangID==1)
		{
			$LangUsed = "b5";
		} elseif ($LangID==2)
		{
			$LangUsed = "gb";
		}

		$newCategoryName = "";
		if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
			$newCategoryName = trim($newCategory['BookCategoryCode']) . " - ". trim($newCategory['DescriptionEn']) . " - ". trim($newCategory['DescriptionChi']);
		}
		else {
			if ($LangUsed=="en" && trim($newCategory['DescriptionEn'])!="")
			{
				$newCategoryName = trim($newCategory['BookCategoryCode']) . " - ". trim($newCategory['DescriptionEn']);
			} elseif (trim($newCategory['DescriptionChi'])!="")
			{
				$newCategoryName = trim($newCategory['BookCategoryCode']) . " - ". trim($newCategory['DescriptionChi']);
			}
		}

		# from LMS
		$sql = "SELECT
					lb.BookID
				FROM
					LIBMS_BOOK AS lb
				INNER JOIN
					LIBMS_BOOK_CATEGORY AS lbc on lbc.BookCategoryCode=lb.BookCategoryCode
				WHERE
					lb.BookID>0
				AND
					lb.BookCategoryCode='".$newCategory['BookCategoryCode']."'
				AND
					lbc.BookCategoryType = (IFNULL((SELECT lbl2.BookCategoryType FROM LIBMS_BOOK_LANGUAGE lbl2 WHERE lbl2.BookLanguageCode = lb.Language),1))
				AND lbc.BookCategoryType = '".$newCategory['BookCategoryType']."'
				ORDER BY
					lb.BookID ASC";
		$rows = $libms->returnVector($sql);

		$ret = array();
		$eBookIDAry = array();
		for ($i=0,$iMax=count($rows); $i<$iMax; $i++)
		{
			$bookID = $rows[$i];
			$eBookIDAry[] = $this->physical_book_init_value + $bookID;
		}
		if (count($eBookIDAry) > 0) {
			$sql = "UPDATE INTRANET_ELIB_BOOK SET
	                 Category= '".addslashes($newCategoryName)."'
	                WHERE BookID in (".implode(',',$eBookIDAry).")";
	        $ret[] = $this->db_db_query($sql);
		}
		return in_array(false,$ret) ? false : true;
	}

	// return BookID by BookTitle, return empty string if not found
	function getBookIDByBookTitle($bookTitle, $publish='') {
		$cond = ($publish != '') ? " AND Publish='".$publish."'" : "";
		$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK WHERE Title='$bookTitle'".$cond;
		$rs = $this->returnVector($sql);
		return count($rs) > 0 ? $rs[0] : '';
	}

	// check if book tag exist by bookID and tagID, return true if exist and false otherwise
	function isBookTagExist($bookID, $tagID) {
		$sql = "SELECT BookID FROM INTRANET_ELIB_BOOK_TAG WHERE BookID='$bookID' AND TagID='$tagID'";
		$rs = $this->returnVector($sql);
		return count($rs) > 0 ? true : false;
	}

	// retrieve list of Book that is expired
	function getExpiredBookIDArr($isArr=false){
		global $sys_custom;
		if($sys_custom['ebook']['disableRemovalEbook']){
			return array();
		}else{
			$activeListArr = $this->getActiveBookID();
			if($activeListArr){
				$activeList = implode(",", $activeListArr);
			}else{
				$activeList = '';
			}
			if($activeList == ''){
				$sql = "SELECT BookID FROM INTRANET_EBOOK_BOOK WHERE BookID NOT IN
						('$activeList')"; //empty string
			}else{
				$sql = "SELECT BookID FROM INTRANET_EBOOK_BOOK WHERE BookID NOT IN
						($activeList)";
			}
			$rs = $this->returnVector($sql);
			if($isArr){
				return $rs;
			}
			if($rs)
				$IDlist = implode(",",$rs);
			else
				$IDlist = '';
		}
		return $IDlist;
	}

	// retrieve list of Book that is active
	function getActiveBookID(){
		global $sys_custom;
		if($sys_custom['ebook']['disableRemovalEbook']){
			return "";
		}
		$sql = "SELECT i.BookID FROM INTRANET_ELIB_BOOK_INSTALLATION i INNER JOIN INTRANET_ELIB_BOOK_QUOTATION q
				ON i.QuotationID=q.QuotationID AND  (q.PeriodTo > NOW() OR q.PeriodTo = '0000-00-00 00:00:00') AND q.IsActivated='1'
				WHERE i.IsEnable='1' OR IsCompulsory='1'  ORDER BY i.BookID";
		$rs = $this->returnVector($sql);
		return $rs;
	}

	function getReviewByReviewID($ReviewID) {
		$sql = "SELECT * FROM INTRANET_ELIB_BOOK_REVIEW where ReviewID='{$ReviewID}'";
		$rs = $this->returnResultSet($sql);

		return $rs;
	}

	// $RecommendID can be array or string
	function removeRecommend($RecommendID='')
	{
		if (empty($RecommendID)) {
			return false;
		}

		if (is_array($RecommendID)) {
			$cond = "RecommendID IN ('" . implode("','",$RecommendID)."')";
		}
		else {
			$cond = "RecommendID = '".$RecommendID."'";
		}

		$sql = " DELETE FROM INTRANET_ELIB_BOOK_RECOMMEND WHERE " . $cond;
		$result = $this->db_db_query($sql);

		return $result;
	} // end function removeRecommend

	function getBookRecommendByID($ParArr="")
	{
		$RecommendID = $ParArr["RecommendID"];

		$sql = "
		SELECT
		BookID, Description, RecommendGroup
		FROM
		INTRANET_ELIB_BOOK_RECOMMEND
		WHERE
		RecommendID = '".$RecommendID."'
		";

		$returnArr = $this->returnResultSet($sql);

		return current($returnArr);
	} // end function getBookRecommendByID

	// $BookID can be array or string
	function removeRecommendByBookID($BookID='')
	{
		if (empty($BookID)) {
			return false;
		}

		if (is_array($BookID)) {
			$cond = "BookID IN ('" . implode("','",$BookID)."')";
		}
		else {
			$cond = "BookID = '".$BookID."'";
		}

		$sql = " DELETE FROM INTRANET_ELIB_BOOK_RECOMMEND WHERE " . $cond;
		$result = $this->db_db_query($sql);

		return $result;
	} // end function removeRecommend

	// check if a client has ever purchased eBook or not
	function isPurchasedeBook($excludeExpire=false,$isActivated=1){
		$sql = "SELECT QuotationID FROM INTRANET_ELIB_BOOK_QUOTATION WHERE 1";
		if ($isActivated) {
			$sql .= " AND IsActivated='1'";
		}
		if ($excludeExpire) {
			$sql .= " AND (PeriodTo > NOW() OR PeriodTo = '0000-00-00 00:00:00')";
		}
		$sql .= " ORDER BY QuotationID DESC LIMIT 1";
		$rs = $this->returnResultSet($sql);
		if (count($rs) > 0) {
			$ret = true;
		}
		else {
			$ret = $this->isSchooleBook();
		}
		return $ret;
	}

	function isSchooleBook($publish=''){
		$schooleBookID = "761, 764, 767, 768, 769, 770, 771, 773, 775, 776, 777, 778, 779, 780, 781, 782, 783, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 952, 957, 958, 1010, 1011, 1012, 1019, 1096, 1560, 1561, 1562, 1563, 1695, 1699, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 2350";

		$sql  = "SELECT BookID FROM INTRANET_ELIB_BOOK WHERE BookID IN ({$schooleBookID})";
		if ($publish != '') {
			$sql .= " AND Publish='{$publish}'";
		}
		$sql .= "LIMIT 1";
		$rs = $this->returnResultSet($sql);
		return count($rs) > 0 ? true : false;
	}

	#############################################
	## start of functions used in iPortfolio
	## iPortfolio > Student Account > Class Directory > {Class Name} > {Student Name} > Reading Records

	function getYearClassList($StudentID, $AcademicYearID='')
	{
		$cond = ($AcademicYearID) ? " AND ay.AcademicYearID='".$AcademicYearID."'" : "";

		$yearName = Get_Lang_Selection('YearNameB5','YearNameEN');
		$className = Get_Lang_Selection('ClassTitleB5','ClassTitleEN');

		$sql = "SELECT 		ay.AcademicYearID,
							ay.{$yearName} as YearName,
							yc.{$className} as ClassName
				FROM
							YEAR_CLASS_USER ycu
				INNER JOIN
							YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
				INNER JOIN
							ACADEMIC_YEAR ay ON ay.AcademicYearID=yc.AcademicYearID
				WHERE
							ycu.UserID='{$StudentID}' {$cond}
				ORDER BY	ay.YearNameEN DESC";
		$rs = $this->returnArray($sql);

		return $rs;
	}

	function getReadCountEBookByYear($StudentID, $AcademicYearID) {
		global $eclass_prefix;

		$ret = array();
		$yearTotal = 0;
		$semesters = getSemesters($AcademicYearID, $ReturnAsso=0, $ParLang='');
		if (count($semesters)) {
			foreach((array)$semesters as $semester) {
				$sql = "SELECT
								COUNT(DISTINCT BookID) AS NumOfBooks
						FROM
								INTRANET_ELIB_BOOK_HISTORY
						WHERE
								UserID='{$StudentID}'
						AND
								DateModified>='".$semester['TermStart']."'
						AND
								DateModified<='".$semester['TermEnd']."'";

				$rs = $this->returnResultSet($sql);
				$ret[$semester['YearTermID']] = $rs[0]['NumOfBooks'];
				$yearTotal += $rs[0]['NumOfBooks'];
			}
		}
		$ret['Total'] = $yearTotal;

		return $ret;
	}

	function getReadCountEBook($StudentID, $AcademicYearID=''){
		global $eclass_prefix;
		$libmsDB = $eclass_prefix."eClass_LIBMS";

		$ret = array();
		if ($AcademicYearID == '') {
			$yearClassList = $this->getYearClassList($StudentID, $AcademicYearID);
			foreach((array)$yearClassList as $year){
				$academicYearID = $year['AcademicYearID'];
				$ret[$academicYearID] = $this->getReadCountEBookByYear($StudentID, $academicYearID);
			}
		}
		else {
			$ret[$AcademicYearID] = $this->getReadCountEBookByYear($StudentID, $AcademicYearID);
		}
		return $ret;
	}

	function getReadCountPBookByYear($StudentID, $AcademicYearID) {
		global $eclass_prefix;
		$libmsDB = $eclass_prefix."eClass_LIBMS";

		$ret = array();
		$yearTotal = 0;
		$semesters = getSemesters($AcademicYearID, $ReturnAsso=0, $ParLang='');
		if (count($semesters)) {
			foreach((array)$semesters as $semester) {
				$sql = "SELECT
								COUNT(DISTINCT BookID) AS NumOfBooks
						FROM
								{$libmsDB}.LIBMS_BORROW_LOG
						WHERE
								UserID='{$StudentID}'
						AND
								BorrowTime>='".$semester['TermStart']."'
						AND
								BorrowTime<='".$semester['TermEnd']."'";

				$rs = $this->returnResultSet($sql);
				$ret[$semester['YearTermID']] = $rs[0]['NumOfBooks'];
				$yearTotal += $rs[0]['NumOfBooks'];
			}
		}
		$ret['Total'] = $yearTotal;

		return $ret;
	}

	function getReadCountPBook($StudentID, $AcademicYearID=''){
		global $eclass_prefix;
		$libmsDB = $eclass_prefix."eClass_LIBMS";

		$ret = array();
		if ($AcademicYearID == '') {
			$yearClassList = $this->getYearClassList($StudentID, $AcademicYearID);
			foreach((array)$yearClassList as $year){
				$academicYearID = $year['AcademicYearID'];
				$ret[$academicYearID] = $this->getReadCountPBookByYear($StudentID, $academicYearID);
			}
		}
		else {
			$ret[$AcademicYearID] = $this->getReadCountPBookByYear($StudentID, $AcademicYearID);
		}
		return $ret;
	}

 	function getStudentReadingSummaryUI($StudentID, $ChooseYear=''){
 		global $Lang;

 		$yearClassList = $this->getYearClassList($StudentID, $ChooseYear);
 		$eBookRead = $this->getReadCountEBook($StudentID, $ChooseYear);
 		$pBookRead = $this->getReadCountPBook($StudentID, $ChooseYear);

 		$x = "<table width=100% border=0 cellpadding=4 cellspacing=0 bgcolor=#CCCCCC>\n";
			$x .= "<tr class='tabletop'>\n";
 				$x .= "<td class='tabletopnolink' style='width:23%; text-align:left; font-weight:bold;'>".$Lang['General']['SchoolYear']."</td>";
 				$x .= "<td class='tabletopnolink' style='width:24%; text-align:left; font-weight:bold;'>".$Lang['Header']['Menu']['Class']."</td>";
 				$x .= "<td class='tabletopnolink' style='width:23%; text-align:left; font-weight:bold;'>".$Lang['General']['Term']."</td>";
 				$x .= "<td class='tabletopnolink' style='width:15%; text-align:center; font-weight:bold;'>".$Lang["libms"]["portal"]["pBooks"]."</td>";
 				$x .= "<td class='tabletopnolink' style='width:15%; text-align:center; font-weight:bold;'>".$Lang["libms"]["portal"]["eBooks"]."</td>";
 			$x .= "</tr>\n";

 		if (!empty($yearClassList)) {
 			$num = 0;
 			$eBookTotal = 0;
 			$pBookTotal = 0;

 			foreach((array)$yearClassList as $year=>$yearClass) {
 				$academicYearID = $yearClass['AcademicYearID'];
 				$yearName = $yearClass['YearName'];
 				$className = $yearClass['ClassName'];

 				$semesters = getSemesters($academicYearID);
 				$semesters['Total'] = $Lang['General']['WholeYear'];

 				$currYear = "";
 				foreach((array)$semesters as $k=>$v) {
					if($year!==$currYear)
					{
						if ($num%2!=0) {
							$bclass = "class=tablerow2";
							$tcolor = "tablerow_total2";
						}
						else {
							$bclass = "class=tablerow1";
							$tcolor = "tablerow_total";
						}
						$x .= "<tr $bclass>\n";
						$x .= "<td class=tabletext>$yearName</td>\n";
						$x .= "<td class=tabletext>$className</td>\n";
						$num++;
						$currYear = $year;
					}
					else
					{
						$x .= "<tr $bclass>\n";
						$x .= "<td class=tabletext>&nbsp;</td>\n";
						$x .= "<td class=tabletext>&nbsp;</td>\n";
					}

					if ($k == 'Total') {
						$x .= "<td align='center' class='tabletext tablerow_underline $tcolor' style='font-weight: bold'><a href='#' onClick='jVIEW_DETAIL(\"reading_records\", \"AcademicYearID=$academicYearID\")' class='tablelink'>$v</a></td>\n";
						$x .= "<td align='center' class='tabletext tablerow_underline $tcolor' style='font-weight: bold'>".$pBookRead[$academicYearID][$k]."</td>\n";
						$x .= "<td align='center' class='tabletext tablerow_underline $tcolor' style='font-weight: bold'>".$eBookRead[$academicYearID][$k]."</td>\n";
						$eBookTotal += $eBookRead[$academicYearID][$k];
						$pBookTotal += $pBookRead[$academicYearID][$k];
					}
					else {
						$x .= "<td align='center' class='tabletext tablerow_underline'><a href='#' onClick='jVIEW_DETAIL(\"reading_records\", \"SemesterID=$k\")' class='tablelink'>$v</a></td>\n";
						$x .= "<td align='center' class='tabletext tablerow_underline'>".$pBookRead[$academicYearID][$k]."</td>\n";
						$x .= "<td align='center' class='tabletext tablerow_underline'>".$eBookRead[$academicYearID][$k]."</td>\n";
					}
					$x .= "</tr>\n";

 				}		// end semesters
 			}		// end yearClassList

 			// sum total
			$bcolor = "class=tablebluebottom";
			$x .= "<tr $bcolor>\n";
				$x .= "<td class=tabletext style='font-weight:bold;'>".$Lang['General']['Total']."</td>\n";
				$x .= "<td class=tabletext>&nbsp;</td>\n";
				$x .= "<td class=tabletext>&nbsp;</td>\n";
				$x .= "<td class=tabletext style='font-weight:bold;' align='center'>".$pBookTotal."</td>\n";
				$x .= "<td class=tabletext style='font-weight:bold;' align='center'>".$eBookTotal."</td>\n";
			$x .= "</tr>\n";

 		}		// not empty
 		else {
			$x .= "<tr class=tablerow1 valign='middle'><td height='100' colspan='5' align='center' class=\"tabletext td_no_record\">".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
 		}
 		$x .= "</table>";

 		return $x;
 	}

	/*
	 * 	Note: need to retrieve Book Title from LIBMS_BOOK because some books maybe lost after borrow, but still need to count
	 */
	function getStudentReadingDetails($para) {
		global $Lang, $eclass_prefix;
		$libmsDB = $eclass_prefix."eClass_LIBMS";

		if ($para['Scope'] == 'Year' ) {
			$sem_p = "CASE ";
			$sem_e = $sem_p;
			foreach((array)$para['Terms'] as $term) {
				$sem_p .= "WHEN r.BorrowTime>='".$term['TermStart']."' AND r.BorrowTime<='".$term['TermEnd']."' THEN '".$term['TermName']."' ";
				$sem_e .= "WHEN r.DateModified>='".$term['TermStart']."' AND r.DateModified<='".$term['TermEnd']."' THEN '".$term['TermName']."' ";
			}
			$sem_p .= "END AS Semester, ";
			$sem_e .= "END AS Semester, ";
		}
		else {
			$sem_p = "'".$para['Terms']."' AS Semester, ";
			$sem_e = $sem_p;
		}
		$sql = "CREATE TEMPORARY TABLE TEMP_READING_LIST (
 					BookID int(11),
					Title text,
					Category varchar(100) default NULL,
 					BookType varchar(100),
					Semester varchar(100),
					ActionDate datetime default NULL,
					Index IdxBookID(BookID),
					Index IdxSemester(Semester),
					Index IdxActionDate(ActionDate)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		$this->db_db_query($sql);

		$sql = "INSERT INTO
							TEMP_READING_LIST
 				SELECT 		b.BookID,
							bk.BookTitle as Title,
							b.Category,
							IF(b.BookID>".$this->physical_book_init_value.",'".$Lang["libms"]["portal"]["pBooks"]."','".$Lang["libms"]["portal"]["eBooks"]."') as BookType,
							{$sem_p}
							r.BorrowTime as ActionDate
				FROM
							{$libmsDB}.LIBMS_BORROW_LOG r
				INNER JOIN
							{$libmsDB}.LIBMS_BOOK bk ON bk.BookID=r.BookID
				LEFT JOIN
							INTRANET_ELIB_BOOK b ON b.BookID=r.BookID+".$this->physical_book_init_value."
				WHERE
							r.UserID='".$para['StudentID']."'
					AND 	r.BorrowTime>='".$para['StartDate']."'
					AND 	r.BorrowTime<='".$para['EndDate']."'
			UNION
				SELECT 		b.BookID,
							b.Title,
							b.Category,
							IF(b.BookID>".$this->physical_book_init_value.",'".$Lang["libms"]["portal"]["pBooks"]."','".$Lang["libms"]["portal"]["eBooks"]."') as BookType,
							{$sem_e}
							r.DateModified as ActionDate
				FROM
							INTRANET_ELIB_BOOK b
				INNER JOIN
							INTRANET_ELIB_BOOK_HISTORY r ON r.BookID=b.BookID
				WHERE
							r.UserID='".$para['StudentID']."'
					AND 	r.DateModified>='".$para['StartDate']."'
					AND 	r.DateModified<='".$para['EndDate']."'
				ORDER BY 	ActionDate, BookID";

		$this->db_db_query($sql);

		$sql = "SELECT
						BookID,
						Title,
						Category,
						BookType,
						Semester,
						MIN(ActionDate) as ActionDate
				FROM
						TEMP_READING_LIST
				GROUP BY BookID, Semester
				ORDER BY ActionDate";
		$rs = $this->returnResultSet($sql);
		if (count($rs)) {
			$rs2 = array();
			foreach((array)$rs as $k=>$v) {
				unset($rs2);
				$sql = "SELECT
								ActionDate
						FROM
								TEMP_READING_LIST
						WHERE
								BookID='".$v['BookID']."'
						AND
								Semester='".$v['Semester']."'
						ORDER BY ActionDate";
				$rs2 = $this->returnVector($sql);
				$rs[$k]['ActionDates'] = $rs2;
			}
		}

		$sql = "DROP TABLE TEMP_READING_LIST";
		$this->db_db_query($sql);

		return $rs;
	}

	/*
	 * 	$para = array(	'StudentID'	=> {$dtudentID},
	 * 					'StartDate'	=> {$startDate},
	 * 					'EndDate' 	=> {$endDate},
	 * 					'Scope'		=> 'Year'/'Semester',
	 * 					'Terms'		=> {$Terms})
	 */

	function getStudentReadingDetailsUI($para) {
		global $Lang, $sys_custom, $intranet_session_language;

		$result = $this->getStudentReadingDetails($para);

		if ($para['Scope'] == 'Year') {
			$width['Title'] = '25';
			$width['Category'] = '25';
			$width['BookType'] = '15';
			$width['Term'] = '14';
			$width['ActionDate'] = '18';
			$colSpan = 6;
		}
		else {
			$width['Title'] = '30';
			$width['Category'] = '30';
			$width['BookType'] = '16';
			$width['Term'] = '0';
			$width['ActionDate'] = '20';
			$colSpan = 5;
		}
		$x = "<table width='100%' border='0' cellpadding='10' cellspacing='0'>\n";
			$x .= "<tr class='tabletop'>\n";
				$x .= "<th class='tbheading' height='25' nowrap align='center'><span class=\"tabletoplink\">#</span></td>\n";
				$x .= "<th class='tabletopnolink' style='width:".$width['Title']."%; text-align:left;'>".$Lang['libms']['bookmanagement']['bookTitle']."</th>\n";
				$x .= "<th class='tabletopnolink' style='width:".$width['Category']."%; text-align:left;'>".$Lang["libms"]["iPortfolio"]["BookCategory"]."</th>\n";
				$x .= "<th class='tabletopnolink' style='width:".$width['BookType']."%; text-align:left;'>".$Lang["libms"]["iPortfolio"]["PhysicalOrEBook"]."</th>\n";
				if ($para['Scope'] == 'Year') {
					$x .= "<th class='tabletopnolink' style='width:".$width['Term']."%; text-align:left;'>".$Lang['General']['Term']."</th>\n";
				}
				$x .= "<th class='tabletopnolink' style='width:".$width['ActionDate']."%; text-align:left;'>".$Lang["libms"]["iPortfolio"]["BorrowReadDate"]."</th>\n";
			$x .= "</tr>\n";

		if (count($result)) {
			foreach((array)$result as $k=>$rs) {
				if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
					$catTitle = explode('-',$rs['Category']);
					if (count($catTitle) > 2) {
						if ($intranet_session_language == 'en') {
							$dispCategory = $catTitle[0] . ' -' . $catTitle[1];
						}
						else {
							$dispCategory = $catTitle[0] . ' -' . $catTitle[2];
						}
					}
					else {
						$dispCategory = $rs['Category'];
					}
				}
				else {
					$dispCategory = $rs['Category'];
				}

				$bclass = ($k%2!=0) ? "bgcolor=#F3F3F3 class=tabletext" : "bgcolor=#FFFFFF class=tabletext";
				$x .= "<tr $bclass>\n";
					$x .= "<td class=tabletext>".($k+1)."</td>\n";
					$x .= "<td class=tabletext>".$rs['Title']."</td>\n";
					$x .= "<td class=tabletext>".$dispCategory."</td>\n";
					$x .= "<td class=tabletext>".$rs['BookType']."</td>\n";
					if ($para['Scope'] == 'Year') {
						$x .= "<td class=tabletext>".$rs['Semester']."</td>\n";
					}
					$x .= "<td class=tabletext>".$rs['ActionDate']."</td>\n";
				$x .= "</tr>\n";

				$iMax = count($rs['ActionDates']);
				if ($iMax > 1) {
					for($i=1; $i<$iMax; $i++) {		// 1st one already print in above
						$x .= "<tr $bclass>\n";
							$x .= "<td class=tabletext>&nbsp;</td>\n";
							$x .= "<td class=tabletext>&nbsp;</td>\n";
							$x .= "<td class=tabletext>&nbsp;</td>\n";
							$x .= "<td class=tabletext>&nbsp;</td>\n";
							if ($para['Scope'] == 'Year') {
								$x .= "<td class=tabletext>&nbsp;</td>\n";
							}
							$x .= "<td class=tabletext>".$rs['ActionDates'][$i]."</td>\n";
						$x .= "</tr>\n";
					}
				}
			}
		}
		else {
			$x .= "<tr class=tablerow1 valign='middle'><td height='100' colspan='".$colSpan."' align='center' class=tabletext>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
		}
		$x .= "</table>";

		return $x;
	}

 	## end of functions used in iPortfolio
 	#############################################


	function updateBookReview($parArr="")
	{
	    if (is_array($parArr)) {
    	    $reviewID = $parArr["ReviewID"];
    	    $rating = $parArr["Rating"];
    	    $content = htmlspecialchars($parArr["Content"]);
    	    $currUserID = $_SESSION["UserID"];
    	    $sql = "UPDATE INTRANET_ELIB_BOOK_REVIEW SET
                    Rating='".$rating."',
                    Content=".$this->pack_value($content,'str').",
                    DateModified=NOW(),
                    LastModifiedBy='".$currUserID."'
                    WHERE ReviewID='".$reviewID."'";
    	    $result = $this->db_db_query($sql);
	    }
	    else {
	        $result = false;
	    }
	    return $result;
	}

} // end class

?>