<?php 
// Using:
/*
 *  2020-03-03 Cameron
 *      - modify displayCell() to adapt the SENType format so that SenType containing sub-SenType can also contain text [case #D178788]
 *       
 *  2018-04-13 Cameron
 *      - create this file
 */
if (!defined("LIBSENCASETYPETABLE_DEFINED"))                     // Preprocessor directive
{
	define("LIBSENCASETYPETABLE_DEFINED", true);
	class libSenCaseTypeTable extends libdbtable2007
	{	

	    private $senCaseTypeAssoc;
	    private $senCaseSubTypeAssoc;
		
		function __construct($field, $order, $pageNo){
			$this->libdbtable2007($field, $order, $pageNo);
			$this->getSENType();
		}
		
		function getSENType() {
		    global $intranet_root;
		    include_once ($intranet_root . "/includes/eGuidance/libguidance.php");
		    $libguidance = new libguidance();
		    $sen_case_type = $libguidance->getAllSENCaseType();
		    $senCaseTypeAssoc = BuildMultiKeyAssoc($sen_case_type, array(
		        'Code'
		    ), $IncludedDBField = array(
		        'Name'
		    ), $SingleValue = 1);
		    $sen_case_subtype = $libguidance->getAllSENCaseSubType();
		    $senCaseSubTypeAssoc = BuildMultiKeyAssoc($sen_case_subtype, array(
		        'TypeCode',
		        'Code'
		    ), $IncludedDBField = array(
		        'Name'
		    ), $SingleValue = 1);
		    $this->senCaseTypeAssoc = $senCaseTypeAssoc;
		    $this->senCaseSubTypeAssoc = $senCaseSubTypeAssoc;
		}
		
		function displayCell($i,$data, $css="", $other=""){
			global $Lang, $objMedical, $medicalPath;
			if($i == 2){ // SEN case type
                $senNameAry = array();
			    $senTypeAry = explode('^~', $data);
			    foreach ((array) $senTypeAry as $k => $v) {
			        if (strpos($v, '^:') !== false) {
			            list ($senType, $senItemStr) = explode('^:', $v);
			            $senTypeAry[$k] = $senType;
			            $pos = strpos($senItemStr, '^@');
			            $senItems = $senItemStr;
			            if ($pos !== false) {
			                $senItems = substr($senItemStr, 0, $pos);
			                $senItemText = substr($senItemStr,$pos+2);
			            }
			            else {
			                $senItemText = '';
			            }

			            $subItemName = $this->senCaseTypeAssoc[$senType];
			            if ($senItemText) {
			                $subItemName .= '-'.$senItemText;
			            }
			            
			            if (strpos($senItems, '^#') !== false) {
                            $subItemName .= '(';
                            $subItemNameAry = array();                            
                            $senItemAry = explode('^#', $senItems);
                            foreach((array)$senItemAry as $_subItemCode) {
                                $subItemNameAry[] = $this->senCaseSubTypeAssoc[$senType][$_subItemCode];
			                }
			                $subItemName .= implode(', ',$subItemNameAry) . ')';
			            } else {
			                if ($this->senCaseTypeAssoc[$senType] && $this->senCaseSubTypeAssoc[$senType][$senItems]) {
			                    $subItemName .= '('.$this->senCaseSubTypeAssoc[$senType][$senItems].')';
			                }
			                else {
			                    $subItemName .= '('.$senItems.')';
			                }
			            }
			            $senNameAry[] = $subItemName;
			        }
			        else if (strpos($v, '^@') !== false) {
			            list ($senType, $itemText) = explode('^@', $v);
			            $subItemName = $this->senCaseTypeAssoc[$senType];
			            if ($itemText) {
			                $subItemName .= '-'.$itemText;
			            }
			            $senNameAry[] = $subItemName;
			        }			        
			        else {
			            $senNameAry[] = $this->senCaseTypeAssoc[$v];
			        }
			    }
			    $data = implode(', ',$senNameAry);
			    
			}
			return parent::displayCell($i,$data, $css, $other);
		}
		
		
	}
}
?>