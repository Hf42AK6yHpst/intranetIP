<?php
// Using: 
/*
 * Note: must include libguidance.php & libinterface and initialize $libguidance and $linterface object before calling functions that contains $libguidance and $linterface respectively
 *
 * 2020-05-13 Cameron
 * - add function getSENCaseTypeBySequence() and getSENCaseSubTypeBySequence() [case #M151032]
 *
 * 2020-04-23 Cameron
 * - add function getAllSENCaseType() [case #E150107]
 * 
 * 2019-11-18 Cameron
 * - add function getAllSENCaseTypeFilter() [case #Y173401]
 * 
 * 2019-08-13 Cameron
 * - show attachment for view mode in getSENServiceActivityTableRow() [case #M158130]
 * 
 * 2018-09-12 Cameron
 * - fix: use double quote to avoid js error when content contain apostrophe in getAdvancedClassLessonEdit()
 * 
 * 2018-05-21 Cameron
 * - add function getAdjustTypeBySequence(), getAdjustItemBySequence(), getServiceTypeBySequence(), getServiceItemBySequence()
 *
 * 2018-04-11 Cameron
 * - add function getSelectSenSatus();
 *
 * 2017-11-07 Cameron
 * - modify getSelectClassWithWholeForm() to support ej
 *
 * 2017-10-25 Cameron
 * - add follow-up advice in getClassTeacherCommentTable()
 *
 * 2017-09-11 Cameron
 * - modify parameter of getStudentNameListWClassNumberByClassName() in getTransferFrom()
 *
 * 2017-07-28 Cameron
 * - add function getConfidentialViewerSelection(), getGroupCategoryList()
 *
 * 2017-07-24 Cameron
 * - add WithText field to getNewSupportItem()
 *
 * 2017-07-20 Cameron
 * - Teacher name selection list show in two group: teaching staff and non-teaching staff. apply in getTransferFrom()
 *
 * 2017-07-18 Cameron
 * - add getTeacherTypeSelection() and modify getTeacherSelection() so that it can choose teaching / non-teaching staff
 *
 * 2017-06-29 Cameron
 * - add label for radio button item so that clicking lable is the same as clicking the radio button in getNewAdjustmentItem(), getTransferFrom()
 * - modify getAdvancedClassLessonEdit(), getTherapyActivityEdit(), getSENServiceActivityEdit() to add StartDate in the interface
 *
 * 2017-06-26 Pun
 * - Added getSelectClassWithWholeForm()
 *
 * 2017-05-19 Cameron
 * fix bug: disable button after submit to avoid adding duplicate record
 *
 * 2017-02-16 Cameron
 * - create this file
 */
if (! defined("LIBGUIDANCE_UI_DEFINED")) // Preprocessor directives
{
    define("LIBGUIDANCE_UI_DEFINED", true);

    class libguidance_ui extends interface_html
    {

        // -- Start of Class
        public function libguidance_ui()
        {
            parent::interface_html();
        }

        function getSelectClassWithWholeForm($tags, $selected = "", $academicYearID = "", $displayForm = true, $displayAllForm = false)
        {
            global $PATH_WRT_ROOT, $Lang, $i_general_all, $i_general_please_select, $junior_mck;
            
            $pleaseSelect = ($pleaseSelect == "") ? $i_general_please_select : $pleaseSelect;
            $academicYearID = ($academicYearID == "") ? Get_Current_Academic_Year_ID() : $academicYearID;
            
            if ($junior_mck) {
                include_once ($PATH_WRT_ROOT . "includes/libclass.php");
                $lclass = new libclass();
                $levelList = $lclass->getLevelList();
                $classList = $lclass->getClassList();
                $allClasses = BuildMultiKeyAssoc($classList, array(
                    'ClassLevelID',
                    'ClassID'
                ), array(
                    'ClassName'
                ));
                
                // ####### Get SelectOption START ########
                $allSelectOption = array();
                $allSelectOption[] = array(
                    '#',
                    " -- {$pleaseSelect} -- "
                );
                
                if ($displayAllForm) {
                    $allSelectOption[] = array(
                        '0',
                        $Lang['eGuidance']['AllClass']
                    );
                }
                
                foreach ($levelList as $levelID => $levelName) {
                    if (! count($allClasses[$levelID])) {
                        continue;
                    }
                    
                    if ($displayForm) {
                        $allSelectOption[] = array(
                            $levelID,
                            "{$i_general_all} {$levelName}"
                        );
                    }
                    
                    foreach ($allClasses[$levelID] as $classID => $levelClass) {
                        $className = $levelClass['ClassName'];
                        $allSelectOption[] = array(
                            "{$levelID}:{$classID}",
                            "&nbsp;&nbsp;&nbsp;{$className}"
                        );
                    }
                }
                // ####### Get SelectOption END ########
            } else {
                include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
                
                // ####### Get Year START ########
                $objYear = new Year();
                $allYear = $objYear->Get_All_Year_List();
                // ####### Get Year END ########
                
                // ####### Get YearClass START ########
                $objYearClass = new year_class();
                $rs = $objYearClass->Get_All_Class_List($academicYearID);
                
                $allYearClass = array();
                foreach ($rs as $r) {
                    $allYearClass[$r['YearID']][] = $r;
                }
                // ####### Get YearClass END ########
                
                // ####### Get SelectOption START ########
                $allSelectOption = array();
                $allSelectOption[] = array(
                    '#',
                    " -- {$pleaseSelect} -- "
                );
                
                if ($displayAllForm) {
                    $allSelectOption[] = array(
                        '0',
                        $Lang['eGuidance']['AllClass']
                    );
                }
                
                foreach ($allYear as $year) {
                    $yearId = $year['YearID'];
                    if (! count($allYearClass[$yearId])) {
                        continue;
                    }
                    $yearName = $year['YearName'];
                    
                    if ($displayForm) {
                        $allSelectOption[] = array(
                            $yearId,
                            "{$i_general_all} {$yearName}"
                        );
                    }
                    
                    foreach ($allYearClass[$yearId] as $yearClass) {
                        $yearClassId = $yearClass['YearClassID'];
                        $yearClassName = $yearClass['ClassTitle'];
                        $allSelectOption[] = array(
                            "{$yearId}:{$yearClassId}",
                            "&nbsp;&nbsp;&nbsp;{$yearClassName}"
                        );
                    }
                }
                // ####### Get SelectOption END ########
            }
            return getSelectByArray($data = $allSelectOption, $tags, $selected, $all = 0, $noFirst = 1, $FirstTitle = "", $ParQuoteValue = 1);
        }

        // three columns: selected students, buttons, student to be selected
        public function getStudentSelection($selectedStudent = array())
        {
            global $PATH_WRT_ROOT, $Lang, $libguidance;
            
            include_once ($PATH_WRT_ROOT . "includes/libclass.php");
            
            $lclass = new libclass();
            $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
            $nrStudent = count($selectedStudent);
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=StudentID[] id=StudentID style="min-width:200px; height:171px;" multiple>';
            $StudentID = array();
            for ($i = 0; $i < $nrStudent; $i ++) {
                $studentID = $selectedStudent[$i]['UserID'];
                $StudentID[] = $studentID;
                $studentName = $selectedStudent[$i]['Name'];
                $x .= '<option value="' . $studentID . '">' . $studentName . '</option>';
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $this->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableStudentID[]'],this.form.elements['StudentID[]']);return false;", "submit11") . "<br /><br />";
            $x .= $this->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['StudentID[]'],this.form.elements['AvailableStudentID[]']);return false;", "submit12");
            $x .= '</td>';
            
            $x .= '<td>';
            $x .= $classSelection . '<br>';
            $x .= '<span id="StudentNameSpan"><select name=AvailableStudentID[] id=AvailableStudentID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        public function getTeacherTypeSelection($selectedType = '', $id = 'TeacherType')
        {
            global $Lang;
            
            $x = '<select name=' . $id . ' id=' . $id . '>';
            $x .= '<option value="" ' . (($selectedType == '') ? 'SELECTED' : '') . '>-- ' . $Lang['Btn']['Select'] . ' --</option>';
            $x .= '<option value="1" ' . (($selectedType == 1) ? 'SELECTED' : '') . '>' . $Lang['Identity']['TeachingStaff'] . '</option>';
            $x .= '<option value="0" ' . (($selectedType === 0) ? 'SELECTED' : '') . '>' . $Lang['Identity']['NonTeachingStaff'] . '</option>';
            $x .= '</select>';
            
            return $x;
        }

        // three columns: selected teachers, buttons, teacher to be selected
        public function getTeacherSelection($selectedTeacher = array())
        {
            global $Lang, $libguidance;
            
            if (is_array($selectedTeacher)) {
                $nrTeacher = count($selectedTeacher);
            } else {
                $nrTeacher = 0;
            }
            $selectedTeacherID = array();
            $teacherTypeSelection = $this->getTeacherTypeSelection();
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=TeacherID[] id=TeacherID style="min-width:200px; height:171px;" multiple>';
            for ($i = 0; $i < $nrTeacher; $i ++) {
                $teacherID = $selectedTeacher[$i]['UserID'];
                $teacherName = $selectedTeacher[$i]['Name'];
                $x .= '<option value="' . $teacherID . '">' . $teacherName . '</option>';
                $selectedTeacherID[] = $teacherID;
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $this->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableTeacherID[]'],this.form.elements['TeacherID[]']);return false;", "submit11") . "<br /><br />";
            $x .= $this->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['TeacherID[]'],this.form.elements['AvailableTeacherID[]']);return false;", "submit12");
            $x .= '</td>';
            
            $x .= '<td>';
            $x .= $teacherTypeSelection . '<br>';
            $x .= '<span id="TeacherSelectionSpan"><select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        // three columns: selected confidentialViewer, buttons, confidentialViewer to be selected
        public function getConfidentialViewerSelection($selectedConfidentialViewer = array())
        {
            global $Lang, $libguidance;
            
            if (is_array($selectedConfidentialViewer)) {
                $nrConfidentialViewer = count($selectedConfidentialViewer);
            } else {
                $nrConfidentialViewer = 0;
            }
            $selectedConfidentialViewerID = array();
            // $teacherTypeSelection = $this->getTeacherTypeSelection('','ConfidentialViewerType');
            $groupCategorySelection = $this->getGroupCategoryList('ConfidentialGroupCategory');
            
            $x = '
				<table class="no_bottom_border">
					<tr>
						<td>
							<select name=ConfidentialViewerID[] id=ConfidentialViewerID style="min-width:200px; height:192px;" multiple>';
            for ($i = 0; $i < $nrConfidentialViewer; $i ++) {
                $confidentialViewerID = $selectedConfidentialViewer[$i]['UserID'];
                $confidentialViewerName = $selectedConfidentialViewer[$i]['Name'];
                $x .= '<option value="' . $confidentialViewerID . '">' . $confidentialViewerName . '</option>';
                $selectedConfidentialViewerID[] = $confidentialViewerID;
            }
            $x .= '		</td>';
            
            $x .= '<td align=center>';
            
            $x .= $this->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableConfidentialViewerID[]'],this.form.elements['ConfidentialViewerID[]']);return false;", "submit21") . "<br /><br />";
            $x .= $this->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['ConfidentialViewerID[]'],this.form.elements['AvailableConfidentialViewerID[]']);return false;", "submit22");
            $x .= '</td>';
            
            $x .= '<td>';
            $x .= $groupCategorySelection . '<br>';
            $x .= '<select name="ConfidentialGroup" id="ConfidentialGroup" style="display:none"></select><br>';
            $x .= '<span id="ConfidentialViewerSelectionSpan"><select name=AvailableConfidentialViewerID[] id=AvailableConfidentialViewerID style="min-width:200px; height:156px;" multiple>';
            $x .= '</select></span>';
            $x .= '</td>';
            
            $x .= '	</tr>										
				</table>';
            return $x;
        }

        public function getAttachmentUploadForm($target, $recordID)
        {
            global $PATH_WRT_ROOT;
            
            ob_start();
            ?>
<form
	style="display: none; float: left; margin-top: 15px; position: relative; top: -166px; left: 460px;"
	class="attach_form" target="upload_frame"
	action="<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax_file_handling.php?action=uploadAttachment&callback=window.parent.loadAttachment"
	method="post" enctype="multipart/form-data">
	<input class="upload_button" id="attachment" name="attachment"
		type="file" /> <input type=hidden id="BatchUploadTime"
		name="BatchUploadTime" value=""> <input type=hidden id="Target"
		name="Target" value="<?=$target?>"> <input type=hidden name="RecordID"
		id="RecordID" value="<?=$recordID?>">
</form>
<iframe name="upload_frame" style="display: none;"></iframe>
<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        // $mode - view / edit
        public function getAttachmentLayout($target = '', $recordID = '', $mode = 'edit', $current_attachment_id = 'current_attachment')
        {
            global $Lang, $libguidance;
            $a = '<div id="' . $current_attachment_id . '" ' . ($recordID ? '' : 'style="display:none"') . '>';
            if ($recordID) {
                $rs = $libguidance->geteGuidanceAttachment('', $target, $recordID);
                $a .= $this->geteGuidanceAttachFileList($rs, false, $mode);
            }
            $a .= '</div>';
            
            $AttachBtn = $this->GET_SMALL_BTN($Lang['eGuidance']['AddAttachment'], 'button', '', 'BtnAddAttachment');
            $x = '<tr>';
            $x .= '<td valign="top" nowrap="nowrap" class="field_title" width="20%" >' . $Lang['eGuidance']['Attachment'] . '</td>';
            $x .= '<td class="tabletext">';
            $x .= $a;
            $x .= $mode == 'edit' ? '<div id="add_attachment">' . $AttachBtn . '</div>' : '';
            $x .= '</td>';
            $x .= '</tr>';
            return $x;
        }

        public function geteGuidanceAttachFile($row, $script = false, $mode = 'edit')
        {
            global $Lang, $file_path, $PATH_WRT_ROOT, $libguidance;
            
            $eg_path = $libguidance->eg_path ? $libguidance->eg_path : "$file_path/file/eGuidance/";
            $fileSize = (round(filesize($eg_path . $row['EncodeFileName']) / 1024, 2));
            
            $x = '<input type="checkbox" id="FileID[]" name="FileID[]" value="' . $row['FileID'] . '" checked style="visiblity:hidden; display:none;">';
            $x .= '<a class="tabletool" href="/home/download_attachment.php?target_e=' . getEncryptedText($eg_path . $row['EncodeFileName']) . '&filename_e=' . getEncryptedText($row['OriginalFileName']) . '">';
            $x .= (($script == true) ? str_replace("'", "\'", $row['OriginalFileName']) : $row['OriginalFileName']) . ' (' . $fileSize . 'K)';
            $x .= '</a>';
            if ($mode == 'edit') {
                if ($script == true) {
                    $x .= ' [<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\\\'' . $row['FileID'] . '\\\')">' . $Lang['eGuidance']['RemoveAttachment'] . '</a>]';
                } else {
                    $x .= ' [<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\'' . $row['FileID'] . '\')">' . $Lang['eGuidance']['RemoveAttachment'] . '</a>]';
                }
                $x .= '<input type="hidden" id="FileSize[]" name="FileSize[]" value="' . $fileSize . '">';
            }
            return $x;
        }

        public function geteGuidanceAttachFileList($attachList, $script = false, $mode = 'edit')
        {
            $x = '';
            if (count($attachList) > 0) {
                for ($i = 0, $iMax = count($attachList); $i < $iMax; $i ++) {
                    $row = $attachList[$i];
                    $x .= $this->geteGuidanceAttachFile($row, $script, $mode);
                    if ($i < $iMax - 1) {
                        $x .= '<br>';
                    }
                }
            } else {
                if ($mode == 'view') {
                    $x .= '-';
                }
            }
            return $x;
        }

        /*
         * $data = array([0]=>array( ['UserID'] => 1,
         * ['Name'] => 'demo user 1')
         * [1]=>array( ['UserID'] => 2,
         * ['Name'] => 'demo user 2')...)
         * return user name with line break
         */
        public function getUserNameList($data)
        {
            $user_list = array();
            for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
                $user_list[] = $data[$i]['Name'];
            }
            return implode("<br>", (array) $user_list);
        }

        // # advanced_class
        public function getAdvancedClassLessonTable($classID, $student, $layoutMode = 'edit')
        {
            global $Lang, $libguidance;
            
            $lesson = $libguidance->getAdvancedClassLesson($classID);
            $nrLesson = count($lesson);
            
            $x = '';
            $x .= '<table id="LessonTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
            $x .= '<thead>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<th style="width:25px;">#</th>' . "\n";
            $x .= '<th style="width:10%;">' . $Lang['General']['Date'] . '</th>' . "\n";
            $x .= '<th>' . $Lang['eGuidance']['advanced_class']['TeacherComment'] . '</th>' . "\n";
            if ($layoutMode == 'edit') {
                $x .= '<th style="width:25px;"><input type="checkbox" id="CheckAllLesson" onclick="Check_All_Options_By_Class(\'LessonChk\', this.checked);"></th>' . "\n";
            }
            $x .= '</tr>' . "\n";
            $x .= '</thead>' . "\n";
            
            $x .= '<tbody>' . "\n";
            // lesson content
            for ($i = 0; $i < $nrLesson; $i ++) {
                $_rowNum = $i + 1;
                $_lesson = $lesson[$i];
                $x .= $this->getAdvancedClassLessonTableRow($_rowNum, $_lesson, $student, $layoutMode);
            }
            
            // no record display row
            $displayPara = ($nrLesson == 0) ? '' : 'display:none;';
            $x .= '<tr id="noRecordTr" style="' . $displayPara . '"><td colspan="' . ($layoutMode == 'edit' ? '4' : '3') . '" style="text-align:center;">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
            $x .= '</tbody>' . "\n";
            $x .= '</table>' . "\n";
            
            return $x;
        }

        public function getAdvancedClassLessonTableRow($rowNr, $lesson, $student, $layoutMode)
        {
            global $libguidance, $Lang;
            
            $lessonID = $lesson['LessonID'];
            $lessonDate = $lesson['LessonDate'];
            
            if ($lessonID) {
                $lesson = $libguidance->getAdvancedClassLessonResult($lessonID);
                $lessonResult = BuildMultiKeyAssoc($lesson, 'StudentID');
            } else {
                $lessonResult = array();
            }
            $checkboxId = '';
            $checkbox = $this->Get_Checkbox('LessonChk_' . $rowNr, 'LessonChk[]', $lessonID, $isChecked = 0, $Class = "LessonChk", $Display = "", $Onclick = "Uncheck_SelectAll('CheckAllLesson', this.checked);");
            
            $x = '';
            $x .= '<tr id="LessonTr_' . $rowNr . '" class="LessonTr">' . "\n";
            $x .= '<td><div id="RowNumDiv_' . $rowNr . '">' . $rowNr . '</div></td>' . "\n";
            $x .= '<td width="12%">' . "\n";
            
            if ($layoutMode == 'edit') {
                $x .= '<a href="#TB_inline?height=500&width=800&inlineId=FakeLayer" onclick="edit_lesson(\'' . $lessonID . '\'); return false;" title="' . $Lang['eGuidance']['advanced_class']['EditLesson'] . '">' . "\n";
                $x .= '<div id="LessonID_' . $lessonID . '" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);">' . $lessonDate . '</div></a>' . "\n";
            } else {
                $x .= $lessonDate;
            }
            
            $x .= '</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<table class="edit_table_list_v30">' . "\n";
            foreach ((array) $student as $k => $v) {
                $x .= '<tr>' . "\n";
                $x .= '<td>' . $v['Name'] . '</td>' . "\n";
                $x .= '<td width="80%">' . ($lessonResult[$k]['Result'] ? nl2br($lessonResult[$k]['Result']) : '-') . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
            $x .= '</table>' . "\n";
            $x .= '</td>' . "\n";
            if ($layoutMode == 'edit') {
                $x .= '<td>' . $checkbox . '</td>' . "\n";
            }
            $x .= '</tr>' . "\n";
            
            return $x;
        }

        public function getAdvancedClassLessonEdit($classID, $lessonID, $student, $action)
        {
            global $libguidance, $Lang, $i_invalid_date, $PATH_WRT_ROOT;
            
            $today = date('Y-m-d');
            $advancedClass = $libguidance->getAdvancedClass($classID);
            if (count($advancedClass) == 1) {
                $startDate = $advancedClass[0]['StartDate'];
            } else {
                $startDate = $today; // default today
            }
            
            if ($lessonID) {
                $lesson = $libguidance->getAdvancedClassLesson($lessonID);
                if (count($lesson) == 1) {
                    $lessonDate = $lesson[0]['LessonDate'];
                } else {
                    $lessonDate = $startDate;
                }
                $lessonResult = $libguidance->getAdvancedClassLessonResult($lessonID);
                $lessonResult = BuildMultiKeyAssoc($lessonResult, 'StudentID');
            } else {
                $lessonResult = array();
                $lessonDate = $startDate;
            }
            
            ob_start();
            ?>
<form name="form2" id="form2" method="post">
	<table align="center" class="form_table_v30">
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Date']?></td>
			<td class="tabletext"><?=$this->GET_DATE_PICKER("LessonDate", $lessonDate)?></td>

		</tr>
			
		<? foreach((array)$student as $k=>$v):?>
				<tr>
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$v['Name']?></td>
			<td><textarea class="LessonResult" name="LessonInfoAry[<?=$k?>]"
					id="LessonInfoAry[<?=$k?>]" cols="80" rows="2" wrap="virtual"
					onfocus="this.rows=10;"><?=$lessonResult[$k]['Result']?></textarea></td>
		</tr>
		<? endforeach;?>
		
				<?=$this->getAttachmentLayout('AdvancedClassLesson',$lessonID)?>
				
			</table>

	<br>
	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
				<p class="spacer"></p>
	</div>
			<?=$this->GET_HIDDEN_INPUT('ClassID', 'ClassID', $classID)?>
			<?=$this->GET_HIDDEN_INPUT('LessonID', 'LessonID', $lessonID)?>
			
			</form>

<?=$this->getAttachmentUploadForm('AdvancedClassLesson',$lessonID)?>

<script>
					
			$().ready( function(){
				$("#submitBtn_tb").click(function(){
					var empty = true;
					$("input.actionBtn").attr("disabled", "disabled");
					$(".LessonResult").each(function(){
						if ($.trim($(this).val())!="") {
							empty = false;
						}
					});
					if (empty) {
						alert("<?=$Lang['eGuidance']['advanced_class']['Warning']['InputTeacherComment']?>");
						$("input.actionBtn").attr("disabled", "");
						return;
					}
				 	else if (!check_date(document.getElementById('LessonDate'),"<?=$i_invalid_date; ?>")) {
				 		$("input.actionBtn").attr("disabled", "");
				 		return false;
				 	}
				 	else if (compareDate($('#LessonDate').val(),'<?=$startDate?>')<0) {
				 		alert('<?=$Lang['eGuidance']['Warning']['InvalidDateEarly']?>');
				 		$("input.actionBtn").attr("disabled", "");
				 		return false;
				 	}
					else {
						$.ajax({
							dataType: "json",
							type: "POST",
							url: "ajax_update.php?action=<?=$action?>",
							data : $("#form2").serialize(),		  
							success: update_lesson_list,
							error: show_ajax_error
						});
					}
					js_Hide_ThickBox();
				});
				
				<?=$this->getAttachmentRdyJS()?>
			});
			
			<?=$this->getAttachmentOtherJS()?>
			
			</script>
<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        public function getAttachmentRdyJS()
        {
            ob_start();
            ?>
		    $('#BtnAddAttachment').click(function(){
				if (navigator.userAgent.match(/msie/i)){
					$('#attachment').replaceWith($('#attachment').clone(true))
				    $('.attach_form').show();
				}
				else{
				    $('#attachment').click();
				}
				return false;
		    });
			
			// upload attachment
		    $('#attachment').change(function(){
				if ($('#attachment').val() != '') {
					if (navigator.userAgent.match(/msie/i)){
					    $('.attach_form').hide();
					}
					$(this).parent().submit();
					$(this).val('');	// empty it for next upload
				}
		    });
		    
			if ((navigator.userAgent.match(/Trident/i)) || (navigator.userAgent.match(/Edge/i))){
				var obj = document.getElementById('attachment'); 
			    obj.onchange = function() {
			    	if ($('#attachment').val() != '') {
						$(this).parent().submit();
						$(this).val('');	// empty it for next upload
			    	}
			    }
			}
<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        public function getAttachmentOtherJS()
        {
            global $Lang, $PATH_WRT_ROOT;
            ob_start();
            ?>
			function loadAttachment(attachment_layout, batchUploadTime){
				$('#current_attachment').show().html(attachment_layout);
				if (batchUploadTime != '') {
					$('#BatchUploadTime').val(batchUploadTime);
				}
			}
			
			function FileDeleteSubmit(fileID) {
				conf = confirm('<?=$Lang['eGuidance']['Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
				if (conf) {
					$.ajax({
						type: "POST",
						url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax_file_handling.php',
						data : {
							'action': 'removeAttachment',
							'FileID': fileID,
							'BatchUploadTime': $('#BatchUploadTime').val(),
							'Target': $('#Target').val(),
							'RecordID': $('#RecordID').val()
						},		  
						success: function(attachment_layout){
							loadAttachment(attachment_layout,'');
						},
						error: show_ajax_error
					});
				}
			}
<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        // # therapy
        public function getTherapyActivityTable($therapyID, $student, $layoutMode = 'edit')
        {
            global $Lang, $libguidance;
            
            $activity = $libguidance->getTherapyActivity($therapyID);
            $nrActivity = count($activity);
            
            $x = '';
            $x .= '<table id="ActivityTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
            $x .= '<thead>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<th style="width:25px;">#</th>' . "\n";
            $x .= '<th style="width:10%;">' . $Lang['General']['Date'] . '</th>' . "\n";
            $x .= '<th style="width:15%;">' . $Lang['eGuidance']['therapy']['ActivityName'] . '</th>' . "\n";
            $x .= '<th style="width:15%;">' . $Lang['eGuidance']['Remark'] . '</th>' . "\n";
            $x .= '<th>' . $Lang['eGuidance']['therapy']['ContentAndResult'] . '</th>' . "\n";
            if ($layoutMode == 'edit') {
                $x .= '<th style="width:25px;"><input type="checkbox" id="CheckAllActivity" onclick="Check_All_Options_By_Class(\'ActivityChk\', this.checked);"></th>' . "\n";
            }
            $x .= '</tr>' . "\n";
            $x .= '</thead>' . "\n";
            
            $h_hiddenField = '';
            $x .= '<tbody>' . "\n";
            // activity content
            for ($i = 0; $i < $nrActivity; $i ++) {
                $_rowNum = $i + 1;
                $_activity = $activity[$i];
                $_activityID = $activity[$i]['ActivityID'];
                $x .= $this->getTherapyActivityTableRow($_rowNum, $_activity, $student, $layoutMode);
            }
            
            // no record display row
            $displayPara = ($nrActivity == 0) ? '' : 'display:none;';
            $x .= '<tr id="noRecordTr" style="' . $displayPara . '"><td colspan="' . ($layoutMode == 'edit' ? '6' : '5') . '" style="text-align:center;">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
            $x .= '</tbody>' . "\n";
            $x .= '</table>' . "\n";
            
            return $x;
        }

        public function getTherapyActivityTableRow($rowNr, $_activity, $student, $layoutMode)
        {
            global $libguidance, $Lang;
            
            $activityID = $_activity['ActivityID'];
            $activityDate = $_activity['ActivityDate'];
            $activityName = $_activity['ActivityName'];
            $remark = $_activity['Remark'];
            
            if ($activityID) {
                $activity = $libguidance->getTherapyActivityResult($activityID);
                $activityResult = BuildMultiKeyAssoc($activity, 'StudentID');
            } else {
                $activityResult = array();
            }
            $checkboxId = '';
            $checkbox = $this->Get_Checkbox('ActivityChk_' . $rowNr, 'ActivityChk[]', $activityID, $isChecked = 0, $Class = "ActivityChk", $Display = "", $Onclick = "Uncheck_SelectAll('CheckAllActivity', this.checked);");
            
            $x = '';
            $x .= '<tr id="ActivityTr_' . $rowNr . '" class="ActivityTr">' . "\n";
            $x .= '<td><div id="RowNumDiv_' . $rowNr . '">' . $rowNr . '</div></td>' . "\n";
            $x .= '<td width="12%">' . "\n";
            
            if ($layoutMode == 'edit') {
                $x .= '<a href="#TB_inline?height=500&width=800&inlineId=FakeLayer" onclick="edit_activity(\'' . $activityID . '\'); return false;" title="' . $Lang['eGuidance']['therapy']['EditdActivity'] . '">' . "\n";
                $x .= '<div id="LessonID_' . $activityID . '" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);">' . $activityDate . '</div></a>' . "\n";
            } else {
                $x .= $activityDate;
            }
            
            $x .= '</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= ($activityName ? $activityName : '-') . "\n";
            $x .= '</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= ($remark ? nl2br($remark) : '-') . "\n";
            $x .= '</td>' . "\n";
            
            $x .= '<td>' . "\n";
            $x .= '<table class="edit_table_list_v30">' . "\n";
            foreach ((array) $student as $k => $v) {
                $x .= '<tr>' . "\n";
                $x .= '<td>' . $v['Name'] . '</td>' . "\n";
                $x .= '<td width="80%">' . ($activityResult[$k]['Result'] ? nl2br($activityResult[$k]['Result']) : '-') . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
            $x .= '</table>' . "\n";
            $x .= '</td>' . "\n";
            if ($layoutMode == 'edit') {
                $x .= '<td>' . $checkbox . '</td>' . "\n";
            }
            $x .= '</tr>' . "\n";
            
            return $x;
        }

        public function getTherapyActivityEdit($therapyID, $activityID, $student, $action)
        {
            global $libguidance, $Lang, $i_invalid_date;
            
            $today = date('Y-m-d');
            $therapy = $libguidance->getTherapy($therapyID);
            
            if (count($therapy) == 1) {
                $startDate = $therapy[0]['StartDate'];
            } else {
                $startDate = $today; // default today
            }
            
            if ($activityID) {
                $rs_activity = $libguidance->getTherapyActivity($therapyID, $activityID);
                if (count($rs_activity) == 1) {
                    $rs_activity = current($rs_activity);
                    $activityDate = $rs_activity['ActivityDate'];
                } else {
                    $activityDate = $startDate;
                }
                $activity = $libguidance->getTherapyActivityResult($activityID);
                $activityResult = BuildMultiKeyAssoc($activity, 'StudentID');
            } else {
                $rs_activity = array();
                $activityResult = array();
            }
            
            ob_start();
            ?>
<form name="form2" id="form2" method="post">
	<table align="center" class="form_table_v30">
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Date']?></td>
			<td class="tabletext"><?=$this->GET_DATE_PICKER("ActivityDate", $activityDate)?></td>
		</tr>

		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['therapy']['ActivityName']?></td>
			<td><?=$this->GET_TEXTBOX('ActivityName', 'ActivityName', $rs_activity['ActivityName'])?></td>
		</tr>

		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Remark']?></td>
			<td><textarea name="Remark" id="Remark" cols="80" rows="2"
					wrap="virtual" onfocus="this.rows=5;"><?=$rs_activity['Remark']?></textarea></td>
		</tr>
			
		<? foreach((array)$student as $k=>$v):?>
				<tr>
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$v['Name']?></td>
			<td><textarea class="ActivityResult" name="ActivityInfoAry[<?=$k?>]"
					id="ActivityInfoAry[<?=$k?>]" cols="80" rows="2" wrap="virtual"
					onfocus="this.rows=10;"><?=$activityResult[$k]['Result']?></textarea></td>
		</tr>
		<? endforeach;?>
		
				<?=$this->getAttachmentLayout('TherapyActivity',$activityID)?>
				
			</table>

	<br>
	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
				<p class="spacer"></p>
	</div>
			<?=$this->GET_HIDDEN_INPUT('TherapyID', 'TherapyID', $therapyID)?>
			<?=$this->GET_HIDDEN_INPUT('ActivityID', 'ActivityID', $activityID)?>
			
			</form>

<?=$this->getAttachmentUploadForm('TherapyActivity',$activityID)?>

<script>
			$().ready( function(){
				$("#submitBtn_tb").click(function(){
					var empty = true;
					$("input.actionBtn").attr("disabled", "disabled");
					$(".ActivityResult").each(function(){
						if ($.trim($(this).val())!="") {
							empty = false;
						}
					});
					if ($("#ActivityName").val()!="" || $("#Remark").val()!="") {
						empty = false;
					}
					if (empty) {
						alert('<?=$Lang['eGuidance']['therapy']['Warning']['InputAtLeastOne']?>');
						$("input.actionBtn").attr("disabled", "");
						return;
					}
				 	else if (!check_date(document.getElementById('ActivityDate'),"<?=$i_invalid_date; ?>")) {
				 		$("input.actionBtn").attr("disabled", "");
				 		return false;
				 	}
				 	else if (compareDate($('#ActivityDate').val(),'<?=$startDate?>')<0) {
				 		alert('<?=$Lang['eGuidance']['Warning']['InvalidDateEarly']?>');
				 		$("input.actionBtn").attr("disabled", "");
				 		return false;
				 	}
					else {
						$.ajax({
							dataType: "json",
							type: "POST",
							url: "ajax_update.php?action=<?=$action?>",
							data : $("#form2").serialize(),		  
							success: update_activity_list,
							error: show_ajax_error
						});
					}
					js_Hide_ThickBox();
				});
				
				<?=$this->getAttachmentRdyJS()?>			
			});
			
			<?=$this->getAttachmentOtherJS()?>
			
			</script>
<?
            $x = ob_get_contents();
            ob_end_clean();
            
            return $x;
        }

        // # SEN Service
        public function getSENServiceActivityTable($serviceID, $student, $layoutMode = 'edit')
        {
            global $Lang, $libguidance;
            
            $activity = $libguidance->getSENServiceActivity($serviceID);
            $nrActivity = count($activity);
            
            $x = '';
            $x .= '<table id="ActivityTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
            $x .= '<thead>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<th style="width:25px;">#</th>' . "\n";
            $x .= '<th style="width:10%;">' . $Lang['General']['Date'] . '</th>' . "\n";
            $x .= '<th>' . $Lang['eGuidance']['sen_service']['ExtraInfo'] . '</th>' . "\n";
            if ($layoutMode == 'edit') {
                $x .= '<th style="width:25px;"><input type="checkbox" id="CheckAllActivity" onclick="Check_All_Options_By_Class(\'ActivityChk\', this.checked);"></th>' . "\n";
            }
            $x .= '</tr>' . "\n";
            $x .= '</thead>' . "\n";
            
            $x .= '<tbody>' . "\n";
            // activity content
            for ($i = 0; $i < $nrActivity; $i ++) {
                $_rowNum = $i + 1;
                $_activity = $activity[$i];
                $x .= $this->getSENServiceActivityTableRow($_rowNum, $_activity, $student, $layoutMode);
            }
            
            // no record display row
            $displayPara = ($nrActivity == 0) ? '' : 'display:none;';
            $x .= '<tr id="noRecordTr" style="' . $displayPara . '"><td colspan="' . ($layoutMode == 'edit' ? '4' : '3') . '" style="text-align:center;">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
            $x .= '</tbody>' . "\n";
            $x .= '</table>' . "\n";
            
            return $x;
        }

        public function getSENServiceActivityTableRow($rowNr, $_activity, $student, $layoutMode)
        {
            global $libguidance, $Lang;
            
            $activityID = $_activity['ActivityID'];
            $activityDate = $_activity['ActivityDate'];
            
            if ($activityID) {
                $activity = $libguidance->getSENServiceActivityResult($activityID);
                $activityResult = BuildMultiKeyAssoc($activity, 'StudentID');
            } else {
                $activityResult = array();
            }
            $checkboxId = '';
            $checkbox = $this->Get_Checkbox('ActivityChk_' . $rowNr, 'ActivityChk[]', $activityID, $isChecked = 0, $Class = "ActivityChk", $Display = "", $Onclick = "Uncheck_SelectAll('CheckAllActivity', this.checked);");
            
            $x = '';
            $x .= '<tr id="ActivityTr_' . $rowNr . '" class="ActivityTr">' . "\n";
            $x .= '<td><div id="RowNumDiv_' . $rowNr . '">' . $rowNr . '</div></td>' . "\n";
            $x .= '<td width="12%">' . "\n";
            
            if ($layoutMode == 'edit') {
                $x .= '<a href="#TB_inline?height=500&width=800&inlineId=FakeLayer" onclick="edit_activity(\'' . $activityID . '\'); return false;" title="' . $Lang['eGuidance']['sen_service']['EditActivity'] . '">' . "\n";
                $x .= '<div id="ActivityID_' . $activityID . '" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);">' . $activityDate . '</div></a>' . "\n";
            } else {
                $x .= $activityDate;
            }
            
            $x .= '</td>' . "\n";
            
            $x .= '<td>' . "\n";
            $x .= '<table class="edit_table_list_v30">' . "\n";
            foreach ((array) $student as $k => $v) {
                $x .= '<tr>' . "\n";
                $x .= '<td>' . $v['Name'] . '</td>' . "\n";
                $x .= '<td width="80%">' . ($activityResult[$k]['Result'] ? nl2br($activityResult[$k]['Result']) : '-') . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
            
            if ($layoutMode == 'view') {
                $attachment = $libguidance->geteGuidanceAttachment('', 'SENServiceActivity',$activityID);
                if (count($attachment)) {
                    $x .= $this->getAttachmentLayout('SENServiceActivity',$activityID,'view','view_current_attachment') . "\n";
                }
            }
            
            $x .= '</table>' . "\n";
            $x .= '</td>' . "\n";
            if ($layoutMode == 'edit') {
                $x .= '<td>' . $checkbox . '</td>' . "\n";
            }
            $x .= '</tr>' . "\n";
            
            return $x;
        }

        public function getSENServiceActivityEdit($serviceID, $activityID, $student, $action)
        {
            global $libguidance, $Lang, $i_invalid_date;
            
            $today = date('Y-m-d');
            $senService = $libguidance->getSENService($serviceID);
            if (count($senService) == 1) {
                $startDate = $senService[0]['StartDate'];
            } else {
                $startDate = $today; // default today
            }
            
            if ($activityID) {
                $senActivity = $libguidance->getSENServiceActivity($serviceID, $activityID);
                if (count($senActivity) == 1) {
                    $activityDate = $senActivity[0]['ActivityDate'];
                } else {
                    $activityDate = $startDate;
                }
                
                $activity = $libguidance->getSENServiceActivityResult($activityID);
                $activityResult = BuildMultiKeyAssoc($activity, 'StudentID');
            } else {
                $activityResult = array();
            }
            
            ob_start();
            ?>

<form name="form2" id="form2" method="post">
	<table align="center" class="form_table_v30">
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Date']?></td>
			<td class="tabletext"><?=$this->GET_DATE_PICKER("ActivityDate", $activityDate)?></td>

		</tr>

		<? foreach((array)$student as $k=>$v):?>			
				<tr>
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$v['Name']?></td>
			<td><textarea class="ActivityResult" name="ActivityInfoAry[<?=$k?>]"
					id="ActivityInfoAry[<?=$k?>]" cols="80" rows="2" wrap="virtual"
					onfocus="this.rows=10;"><?=$activityResult[$k]['Result']?></textarea></td>
		</tr>
		<? endforeach;?>
		
				<?=$this->getAttachmentLayout('SENServiceActivity',$activityID)?>
				
			</table>

	<br>
	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
				<p class="spacer"></p>
	</div>
			
			<?=$this->GET_HIDDEN_INPUT('ServiceID', 'ServiceID', $serviceID)?>
			<?=$this->GET_HIDDEN_INPUT('ActivityID', 'ActivityID', $activityID)?>
			
			</form>

<?=$this->getAttachmentUploadForm('SENServiceActivity',$activityID)?>

<script>
			$().ready( function(){
				$("#submitBtn_tb").click(function(){
					var empty = true;
					$("input.actionBtn").attr("disabled", "disabled");
					$(".ActivityResult").each(function(){
						if ($.trim($(this).val())!="") {
							empty = false;
						}
					});
					if (empty) {
						alert('<?=$Lang['eGuidance']['sen_service']['Warning']['InputExtraInfo']?>');
						$("input.actionBtn").attr("disabled", "");
						return;
					}
				 	else if (!check_date(document.getElementById('ActivityDate'),"<?=$i_invalid_date?>")) {
				 		$("input.actionBtn").attr("disabled", "");
				 		return false;
				 	}
				 	else if (compareDate($('#ActivityDate').val(),'<?=$startDate?>')<0) {
				 		alert('<?=$Lang['eGuidance']['Warning']['InvalidDateEarly']?>');
				 		$("input.actionBtn").attr("disabled", "");
				 		return false;
				 	}
					
					else {
						$.ajax({
							dataType: "json",
							type: "POST",
							url: "ajax_update.php?action=<?=$action?>",
							data : $("#form2").serialize(),		  
							success: update_activity_list,
							error: show_ajax_error
						});
						
						
					}
					js_Hide_ThickBox();
				});
				
				<?=$this->getAttachmentRdyJS()?>			
			});
			
			<?=$this->getAttachmentOtherJS()?>
			
			</script>
<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        // $teacherType: -1 -> all teacher, 1 -> teaching teacher, 0 -> non-teaching staff
        public function getTeacherList($name, $selectedVal, $display, $teacherType = '-1', $disabled = '')
        {
            global $libguidance;
            $teacherAry = $libguidance->getTeacher($teacherType);
            $teacherSelection = getSelectByArray($teacherAry, "name='$name' id='$name' style='display:$display' $disabled", $selectedVal);
            return $teacherSelection;
        }

        public function getTransferFrom($rs = array(), $disabled = '')
        {
            global $Lang, $lclass, $libguidance;
            
            $transfer_from = $libguidance->getGuidanceSettingItem('Transfer', 'TransferFrom');
            
            $x = '';
            $i = 0;
            foreach ((array) $transfer_from as $k => $v) {
                $x .= $i > 0 ? '<br>' : '';
                switch ($k) {
                    case 'DisciplineTeacher':
                        $x .= '<input type="radio" name="TransferFromType" id="DisciplineTeacher" ' . $disabled . ' value="DisciplineTeacher" ' . (($rs['TransferFromType'] == 'DisciplineTeacher') ? 'checked' : '') . '>';
                        $x .= '<label for="DisciplineTeacher">' . $v . '</label>';
                        $display = ($rs['TransferFromType'] == 'DisciplineTeacher') ? '' : 'none';
                        
                        $teachingStaffAry = $libguidance->getTeacher($teacherType = '1');
                        $teachingStaffAry = build_assoc_array($teachingStaffAry);
                        $nonTeachingStaffAry = $libguidance->getTeacher($teacherType = '0');
                        $nonTeachingStaffAry = build_assoc_array($nonTeachingStaffAry);
                        $data = array(
                            $Lang['Identity']['TeachingStaff'] => $teachingStaffAry,
                            $Lang['Identity']['NonTeachingStaff'] => $nonTeachingStaffAry
                        );
                        $x .= getSelectByAssoArray($data, "name='FromDisciplineTeacher' id='FromDisciplineTeacher' style='display:$display' $disabled", $rs['TransferFrom']);
                        break;
                    
                    case 'Self':
                        $x .= '<input type="radio" name="TransferFromType" id="Self" ' . $disabled . ' value="Self" ' . (($rs['TransferFromType'] == 'Self') ? 'checked' : '') . '>';
                        $x .= '<label for="Self">' . $v . '</label>';
                        break;
                    
                    case 'Teacher':
                        $x .= '<input type="radio" name="TransferFromType" id="Teacher" ' . $disabled . ' value="Teacher" ' . (($rs['TransferFromType'] == 'Teacher') ? 'checked' : '') . '>';
                        $x .= '<label for="Teacher">' . $v . '</label>';
                        $display = ($rs['TransferFromType'] == 'Teacher') ? '' : 'none';
                        $x .= '&nbsp;' . $this->getTeacherList('FromTeacher', $rs['TransferFrom'], $display, 1, $disabled); // teaching teacher
                        break;
                    
                    case 'SocialWorker':
                        $x .= '<input type="radio" name="TransferFromType" id="SocialWorker" ' . $disabled . ' value="SocialWorker" ' . (($rs['TransferFromType'] == 'SocialWorker') ? 'checked' : '') . '>';
                        $x .= '<label for="SocialWorker">' . $v . '</label>';
                        $display = ($rs['TransferFromType'] == 'SocialWorker') ? '' : 'none';
                        $x .= '&nbsp;' . $this->getTeacherList('FromSocialWorker', $rs['TransferFrom'], $display, 0, $disabled); // non-teaching staff
                        break;
                    
                    case 'Classmate':
                        $x .= '<input type="radio" name="TransferFromType" id="Classmate" ' . $disabled . ' value="Classmate" ' . (($rs['TransferFromType'] == 'Classmate') ? 'checked' : '') . '>';
                        $x .= '<label for="Classmate">' . $v . '</label>';
                        
                        if ($rs['TransferFromType'] == 'Classmate') {
                            $display = '';
                            $rs_class_name = $libguidance->getClassByStudentID($rs['TransferFrom']);
                            if (! empty($rs_class_name)) {
                                $classSelection = $lclass->getSelectClass("name='FromClassName' id='FromClassName' $disabled ", $rs_class_name);
                                $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name);
                                $studentSelection = getSelectByArray($studentSelection, "name='FromStudentID' $disabled", $rs['TransferFrom']);
                            } else {
                                $classSelection = $lclass->getSelectClass("name='FromClassName' id='FromClassName' ");
                                $studentSelection = getSelectByArray(array(), "name='FromStudentID'");
                            }
                        } else {
                            $display = 'none';
                            $classSelection = $lclass->getSelectClass("name='FromClassName' id='FromClassName' ");
                            $studentSelection = getSelectByArray(array(), "name='FromStudentID'");
                        }
                        $x .= '&nbsp;&nbsp;&nbsp;<span id="FromStudentSpan" style="display:' . $display . '"><span>' . $Lang['eGuidance']['Class'] . '</span>&nbsp;' . $classSelection . '&nbsp;<span>' . $Lang['eGuidance']['StudentName'];
                        $x .= '</span>&nbsp;<span id="FromStudentNameSpan">' . $studentSelection . '</span></span>';
                        break;
                    
                    default:
                        $x .= '<input type="radio" name="TransferFromType" id="' . $k . '" ' . $disabled . '  value="' . $k . '" ' . (($rs['TransferFromType'] == "$k") ? 'checked' : '') . '>';
                        $x .= '<label for="' . $k . '">' . $v . '</label>';
                        $x .= '&nbsp;<input type="text" name="From' . $k . '" id="From' . $k . '" ' . $disabled . '  value="' . (($rs['TransferFromType'] == "$k") ? intranet_htmlspecialchars($rs['TransferFrom']) : '') . '" style="width:60%; display:' . (($rs['TransferFromType'] == "$k") ? '' : 'none') . '">';
                        break;
                }
                $i ++;
            }
            
            return $x;
        }

        public function getTransferFromText($rs = array())
        {
            global $Lang, $PATH_WRT_ROOT, $libguidance;
            
            include_once ($PATH_WRT_ROOT . "includes/libuser.php");
            
            $transfer_from = $libguidance->getGuidanceSettingItem('Transfer', 'TransferFrom');
            $transferFromType = $rs['TransferFromType'];
            $ret = '';
            
            switch ($transferFromType) {
                case 'DisciplineTeacher':
                    $teacher = new libuser($rs['TransferFrom']);
                    $teacherName = Get_Lang_Selection($teacher->ChineseName, $teacher->EnglishName);
                    $ret = $transfer_from[$transferFromType] . ": {$teacherName}";
                    break;
                case 'Self':
                    $ret = $transfer_from[$transferFromType];
                    break;
                case 'Teacher':
                    $teacher = new libuser($rs['TransferFrom']);
                    $teacherName = Get_Lang_Selection($teacher->ChineseName, $teacher->EnglishName);
                    $ret = $transfer_from[$transferFromType] . ": {$teacherName}";
                    break;
                case 'SocialWorker':
                    $teacher = new libuser($rs['TransferFrom']);
                    $teacherName = Get_Lang_Selection($teacher->ChineseName, $teacher->EnglishName);
                    $ret = $transfer_from[$transferFromType] . ": {$teacherName}";
                    break;
                case 'Classmate':
                    $student = new libuser($rs['TransferFrom']);
                    $studentName = $student->getNameWithClassNumber($rs['TransferFrom']);
                    $ret = $transfer_from[$transferFromType] . ": {$studentName}";
                    break;
                case '': // not input
                    $ret = '';
                    break;
                default:
                    $ret = $transfer_from[$transferFromType] . ": {$rs['TransferFrom']}";
                    break;
            }
            return $ret;
        }

        public function getTransferToText($rs = array())
        {
            global $Lang, $PATH_WRT_ROOT, $libguidance;
            
            include_once ($PATH_WRT_ROOT . "includes/libuser.php");
            
            $transfer_to = $libguidance->getGuidanceSettingItem('Transfer', 'TransferTo');
            $transferToType = $rs['TransferToType'];
            $ret = '';
            
            switch ($transferToType) {
                case 'InSocialWorker':
                    $teacher = new libuser($rs['TransferTo']);
                    $teacherName = Get_Lang_Selection($teacher->ChineseName, $teacher->EnglishName);
                    $ret = $transfer_to[$transferToType] . ": {$teacherName}";
                    break;
                case '': // not input
                    $ret = '';
                    break;
                default:
                    $ret = $transfer_to[$transferToType] . ": {$rs['TransferTo']}";
                    break;
            }
            return $ret;
        }

        public function getTransferTo($rs = array(), $disabled = '')
        {
            global $Lang, $lclass, $libguidance;
            
            $nonTeachingStaffAry = $libguidance->getTeacher($teacherType = '0');
            $nonTeachingStaffAry = build_assoc_array($nonTeachingStaffAry);
            
            $transfer_to = $libguidance->getGuidanceSettingItem('Transfer', 'TransferTo');
            
            $x = '';
            $i = 0;
            foreach ((array) $transfer_to as $k => $v) {
                $x .= $i > 0 ? '<br>' : '';
                switch ($k) {
                    case 'InSocialWorker':
                        $x .= '<input type="radio" name="TransferToType" id="' . $k . '" ' . $disabled . '  value="' . $k . '" ' . (($rs['TransferToType'] == "$k") ? 'checked' : '') . '>';
                        $x .= '<label for="' . $k . '">' . $v . '</label>';
                        $display = ($rs['TransferToType'] == 'InSocialWorker') ? '' : 'none';
                        $x .= '&nbsp;' . $this->getTeacherList('ToInSocialWorker', $rs['TransferTo'], $display, 0, $disabled); // non-teaching staff
                        break;
                    default:
                        $x .= '<input type="radio" name="TransferToType" id="' . $k . '" ' . $disabled . '  value="' . $k . '" ' . (($rs['TransferToType'] == "$k") ? 'checked' : '') . '>';
                        $x .= '<label for="' . $k . '">' . $v . '</label>';
                        $x .= '&nbsp;<input type="text" name="To' . $k . '" id="To' . $k . '" ' . $disabled . '  value="' . (($rs['TransferToType'] == "$k") ? intranet_htmlspecialchars($rs['TransferTo']) : '') . '" style="width:60%; display:' . (($rs['TransferToType'] == "$k") ? '' : 'none') . '">';
                        break;
                }
                $i ++;
            }
            
            return $x;
        }

        public function getSubTag($pages_arr)
        {
            for ($i = 0; $i < sizeof($pages_arr); $i ++) {
                list ($page_title, $page_href, $select) = $pages_arr[$i];
                $page_title = trim($page_title);
                $page_href = trim($page_href);
                
                if ($page_title != "") {
                    $rx .= "<li";
                    $rx .= ($select == 1) ? " class='selected'>" : ">";
                    if ($page_href != '') {
                        $rx .= "<a href='" . $page_href . "'><strong>" . $page_title . "</strong></a>";
                    } else {
                        $rx .= $page_title;
                    }
                    $rx .= "</li>";
                }
            }
            
            return $rx;
        }

        public function getNewAdjustmentItem($typeID)
        {
            global $Lang, $i_general_required_field, $image_path, $LAYOUT_SKIN;
            
            ob_start();
            ?>
<form name="form2" id="form2" method="post">
	<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td>
				<table align="center" class="form_table_v30">
					<tr valign="top">
						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['AdjustItemChineseName']?>
									<span class="tabletextrequire">*</span></td>
						<td class="tabletext"><input type="text" name="ChineseName"
							id="ChineseName" class="textboxtext" value=""></td>
					</tr>
					<tr valign="top">
						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['AdjustItemEnglishName']?>
									<span class="tabletextrequire">*</span></td>
						<td class="tabletext"><input type="text" name="EnglishName"
							id="EnglishName" class="textboxtext" value=""></td>
					</tr>

					<tr valign="top">
						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['WithText']?>
									</td>
						<td class="tabletext"><input type="radio" name="WithText"
							id="WithTextYes" value="1"><label for="WithTextYes"><?=$Lang['General']['Yes']?></label><br>
							<input type="radio" name="WithText" id="WithTextNo" value="0"><label
							for="WithTextNo"><?=$Lang['General']['No']?></label></td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
						<td width="80%">&nbsp;</td>
					</tr>
				</table>

			</td>
		</tr>
		<tr>
			<td height="1" class="dotline"><img
				src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
				height="1"></td>
		</tr>
		<tr>
			<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><span>
										<?=$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
										<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
									</span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br> <input type=hidden id="TypeID" name="TypeID" value="<?=$typeID?>">
</form>

<script>
			$().ready( function(){
				$("#submitBtn_tb").click(function(){
					$('input.actionBtn').attr('disabled', 'disabled');
					if ($('#ChineseName').val() == '') {
						alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemChineseName']?>');
						$('input.actionBtn').attr('disabled', '');
						return;	
					}

					if ($('#EnglishName').val() == '') {
						alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemEnglishName']?>');
						$('input.actionBtn').attr('disabled', '');
						return;	
					}
					
					$.ajax({
						dataType: "json",
						type: "POST",
						url: "ajax_update.php?action=add_adjustment_item",
						data : $("#form2").serialize(),		  
						success: update_adjust_item_list,
						error: show_ajax_error
					});
					
					js_Hide_ThickBox();
				});			
			});
		</script>

<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        public function getNewSupportItem($typeID)
        {
            global $Lang, $i_general_required_field, $image_path, $LAYOUT_SKIN;
            
            ob_start();
            ?>
<form name="form2" id="form2" method="post">
	<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td>
				<table align="center" class="form_table_v30">
					<tr valign="top">
						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['Support']['ChineseName']?>
									<span class="tabletextrequire">*</span></td>
						<td class="tabletext"><input type="text" name="ChineseName"
							id="ChineseName" class="textboxtext" value=""></td>
					</tr>
					<tr valign="top">
						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['Support']['EnglishName']?>
									<span class="tabletextrequire">*</span></td>
						<td class="tabletext"><input type="text" name="EnglishName"
							id="EnglishName" class="textboxtext" value=""></td>
					</tr>
					<tr valign="top">
						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['WithText']?>
									</td>
						<td class="tabletext"><input type="radio" name="WithText"
							id="WithTextYes" value="1"><label for="WithTextYes"><?=$Lang['General']['Yes']?></label><br>
							<input type="radio" name="WithText" id="WithTextNo" value="0"><label
							for="WithTextNo"><?=$Lang['General']['No']?></label></td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
						<td width="80%">&nbsp;</td>
					</tr>
				</table>

			</td>
		</tr>
		<tr>
			<td height="1" class="dotline"><img
				src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
				height="1"></td>
		</tr>
		<tr>
			<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><span>
										<?=$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
										<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
									</span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br> <input type=hidden id="TypeID" name="TypeID" value="<?=$typeID?>">
</form>

<script>
			$().ready( function(){
				$("#submitBtn_tb").click(function(){
					$('input.actionBtn').attr('disabled', 'disabled');
					if ($('#ChineseName').val() == '') {
						alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputSupportChineseName']?>');
						$('input.actionBtn').attr('disabled', '');
						return;	
					}

					if ($('#EnglishName').val() == '') {
						alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputSupportEnglishName']?>');
						$('input.actionBtn').attr('disabled', '');
						return;	
					}
					
					$.ajax({
						dataType: "json",
						type: "POST",
						url: "ajax_update.php?action=add_support_item",
						data : $("#form2").serialize(),		  
						success: update_support_item_list,
						error: show_ajax_error
					});
					
					js_Hide_ThickBox();
				});			
			});
		</script>

<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        public function getGroupCategoryList($groupCatID = "GroupCategory")
        {
            global $Lang, $libguidance;
            
            $groupCategory = $libguidance->getGroupCategoryList();
            ob_start();
            ?>
<select name="<?=$groupCatID?>" id="<?=$groupCatID?>">
	<option value="">-- <?=$Lang['General']['PleaseSelect']?> --</option>
	<optgroup label="<?=$Lang['Identity']['Staff']?>">
		<option value="TeachingStaff"><?=$Lang['Identity']['TeachingStaff']?></option>
		<option value="NonTeachingStaff"><?=$Lang['Identity']['NonTeachingStaff']?></option>
	</optgroup>

	<optgroup label="<?=$Lang['Group']['GroupCategory']?>">
				<? foreach((array)$groupCategory as $cat):?>
					<option value="<?=$cat['GroupCategoryID']?>"><?=$cat['CategoryName']?></option>
				<? endforeach;?>
				</optgroup>
</select>
<?
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        public function getNotifyColleagueSelection($recordID, $recordType, $actionType = 'update')
        {
            global $libguidance, $Lang, $PATH_WRT_ROOT;
            
            $groupCategorySelection = $this->getGroupCategoryList('NotifierGroupCategory');
            
            ob_start();
            ?>
<form name="form2" id="form2" method="post">
	<table align="center" class="form_table_v30">
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['SelectColleague']?></td>

			<td class="tabletext">
				<table class="no_bottom_border">
					<tr>
						<td><select name=NotifierID[] id=NotifierID
							style="min-width: 200px; height: 192px;" multiple>'; </td>

						<td align=center>
									<?=$this->GET_BTN("<< ".$Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableNotifierID[]'],this.form.elements['NotifierID[]']);return false;", "submit31")?> <br />
						<br />
									<?=$this->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['NotifierID[]'],this.form.elements['AvailableNotifierID[]']);return false;", "submit32")?>
								 </td>

						<td>
								 	<?=$groupCategorySelection?><br> <select name="NotifierGroup"
							id="NotifierGroup" style="display: none"></select><br> <span
							id="NotifierSelectionSpan"><select name=AvailableNotifierID[]
								id=AvailableNotifierID style="min-width: 200px; height: 156px;"
								multiple>
							</select></span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<br>
	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
				<p class="spacer"></p>
	</div>
			<?=$this->GET_HIDDEN_INPUT('RecordID', 'RecordID', $recordID)?>
			<?=$this->GET_HIDDEN_INPUT('RecordType', 'RecordType', $recordType)?>
			
			</form>

<script>
			$().ready( function(){
				var isLoading = true;
				var loadingImg = '<?=$this->Get_Ajax_Loading_Image() ?>';
				
				$("#submitBtn_tb").click(function(){
					var empty = true;
					$("input.actionBtn").attr("disabled", "disabled");
					
			 		if ($('#NotifierID option').length == 0) {
					    alert('<?=$Lang['eGuidance']['Warning']['SelectNotifier']?>');
					    $("input.actionBtn").attr("disabled", "");
					    return false;
			 		} 
					else {
						var ary = [];
						$('#NotifierID option').attr('selected',true);
						$('#NotifierID option:selected').each(function(){
							ary.push($(this).val());
						});
						$('#hidNotifierID').val(ary);
						
						$('#submitMode').val('submitAndNotify');
						$('#form1').submit();
						
//						$.ajax({
//							dataType: "json",
//							type: "POST",
//							url: "../ajax/ajax.php?action="+"<?=($actionType == 'update' ? 'notifyUpdate' : 'notifyReferral')?>",
//							data : $("#form2").serialize(),		  
//							success: show_notify_message,
//							error: show_ajax_error
//						});
					}
//					$("input.actionBtn").attr("disabled", "");
//					js_Hide_ThickBox();
				});
							
				$('#NotifierGroupCategory').change(function(){
					$('#NotifierID').attr('checked',true);
					isLoading = true;
					$('#NotifierSelectionSpan').html(loadingImg);
					var excludeNotifierID = [];
					var i = 0;
					$('#NotifierID option').each(function() {
						if ($.trim($(this).val()) != '') {
							excludeNotifierID[i++] = $(this).val();
						}
					});
			
					if ($('#NotifierGroupCategory').val() == '') {
						$('#AvailableNotifierID option').remove();
						$('#NotifierGroup option').remove();
						$('#NotifierGroup').css('display','none');
						isLoading = false;
						var blankOptionList = '<select name=AvailableNotifierID[] id=AvailableNotifierID style="min-width:200px; height:156px;" multiple>';
						blankOptionList += '</select>';
						$('#NotifierSelectionSpan').html(blankOptionList);			
					}
					else if ($('#NotifierGroupCategory').val() == 'TeachingStaff' || $('#NotifierGroupCategory').val() == 'NonTeachingStaff') {
						$('#NotifierGroup option').remove();
						$('#NotifierGroup').css('display','none');
						
						$.ajax({
							dataType: "json",
							type: "POST",
							url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
							data : {
								'action': 'getNotifier',
								'NotifierGroupCategory': $('#NotifierGroupCategory').val(),
								'ExcludeNotifierID[]':excludeNotifierID
							},		  
							success: update_available_notifier_list,
							error: show_ajax_error
						});
					}
					else {			
			 			$.ajax({
							dataType: "json",
							type: "POST",
							url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
							data : {
								'action': 'getNotifierGroup',
								'NotifierGroupCategory': $('#NotifierGroupCategory').val(),
								'isLoading': isLoading
							},		  
							success: update_notifier_group_list,
							error: show_ajax_error
						});
					}		
				});
				
			});
			
			function changeNotifierGroup(isLoading) {
				var loadingImg = '<?=$this->Get_Ajax_Loading_Image() ?>';	
				$('#NotifierID').attr('checked',true);
				isLoading = true;
				$('#NotifierSelectionSpan').html(loadingImg);
				var excludeNotifierID = [];
				var i = 0;
				$('#NotifierID option').each(function() {
					if ($.trim($(this).val()) != '') {
						excludeNotifierID[i++] = $(this).val();
					}
				});
			
				if ($('#NotifierGroup').val() == '') {
					$('#AvailableNotifierID option').remove();
					isLoading = false;
					var blankOptionList = '<select name=AvailableNotifierID[] id=AvailableNotifierID style="min-width:200px; height:156px;" multiple>';
					blankOptionList += '</select>';
					$('#NotifierSelectionSpan').html(blankOptionList);			
				}
				else {			
					$.ajax({
						dataType: "json",
						type: "POST",
						url: '<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eGuidance/ajax/ajax.php',
						data : {
							'action': 'getNotifierGroupMember',
							'NotifierGroup': $('#NotifierGroup').val(),
							'ExcludeNotifierID[]': excludeNotifierID
						},		  
						success: update_available_notifier_list,
						error: show_ajax_error
					});
				}		
			}
			
			function update_available_notifier_list(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
					$('#NotifierSelectionSpan').html(ajaxReturn.html);
					isLoading = false;
				}
			}
			
			function update_notifier_group_list(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
					var blankOptionList = '<select name=AvailableNotifierID[] id=AvailableNotifierID style="min-width:200px; height:156px;" multiple>';
					blankOptionList += '</select>';
					$('#NotifierSelectionSpan').html(blankOptionList);
					$('#NotifierGroup').replaceWith(ajaxReturn.html);
					isLoading = false;
				}
			}
			
//			function show_notify_message() {
//				Get_Return_Message("1|=|<?=($actionType == 'update' ? $Lang['eGuidance']['AlreadyNotifyUpdate'] : $Lang['eGuidance']['AlreadyNotifyReferral'])?>");
//			}
			</script>
<?
            $x = ob_get_contents();
            ob_end_clean();
            
            return $x;
        }

        public function getHistoryPushMessage($recordID, $recordType)
        {
            global $libguidance, $Lang;
            
            $notify = $libguidance->getGuidancePushMessage($recordID, $recordType);
            $rs1 = BuildMultiKeyAssoc($notify, 'NotifyMessageID', array(
                'NotifyDateTime',
                'NotifyUser'
            ));
            $rs2 = BuildMultiKeyAssoc($notify, array(
                'NotifyMessageID',
                'TargetID'
            ), array(
                'NotifyTo',
                'MessageStatus'
            ));
            $i = 1;
            ob_start();
            ?>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table align="center" class="common_table_list_v30">
				<tr valign="top">
					<th>#</th>
					<th width="24%"><?=$Lang['General']['CreatedBy']?></th>
					<th width="24%"><?=$Lang['AppNotifyMessage']['SendTime']?></th>
					<th width="25%"><?=$Lang['eGuidance']['PushMessage']['SendToStaff']?></th>
					<th width="25%"><?=$Lang['General']['Status']?></th>
				</tr>
					<?
            
if (count($rs1) > 0) :
                foreach ((array) $rs1 as $notifyID => $nf) :
                    $rowSpan = count($rs2[$notifyID]);
                    $j = 0;
                    ?>
							<tr valign="top">
					<td rowspan="<?=$rowSpan?>"><?=$i++?></td>
					<td rowspan="<?=$rowSpan?>"><?=$nf['NotifyUser']?></td>
					<td rowspan="<?=$rowSpan?>"><?=$nf['NotifyDateTime']?></td>
								<?
                    
foreach ((array) $rs2[$notifyID] as $t) :
                        if ($j > 0) {
                            echo '<tr>';
                        }
                        ?>
										<td width="25%"><?=$t['NotifyTo']?></td>
					<td width="25%"><?=$libguidance->getPushMessageStatusDisplay($t['MessageStatus'])?></td>
										
								<?
                        
if ($j > 0) {
                            echo '</tr>';
                        }
                        $j ++;
                        ?>		
								<?endforeach;?>
							</tr>
						<?endforeach;?>
					<?else:?>
							<tr id="noRecordTr">
					<td colspan="5" style="text-align: center;"><?=$Lang['General']['NoRecordAtThisMoment']?></td>
				</tr>
					<?endif;?>
						</table>

		</td>
	</tr>
</table>

<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
					<?=$this->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", "closeBtn_tb", "", "0", "", "actionBtn")?>
				<p class="spacer"></p>
</div>

<?
            $x = ob_get_contents();
            ob_end_clean();
            
            return $x;
        }

        public function getClassTeacherCommentTable($meetingID, $yearClassID = '', $view = false, $yearName = '', $className = '')
        {
            global $Lang, $libguidance, $linterface;
            
            if (($yearClassID) || (! empty($yearName) && ! empty($className))) {
                $comments = $libguidance->getClassTeacherComments($meetingID, $yearClassID, $yearName, $className);
            } else {
                $comments = array();
            }
            ob_start();
            
            ?>
<table align="center" class="common_table_list_v30 " id="ClassStudent">
	<tr valign="top">
		<th width='1' class='tabletoplink'>#</th>
		<th width='13%'><?=$Lang['eGuidance']['ctmeeting']['StudentRegNo']?></th>
		<th width='5%'><?=$Lang['General']['ClassNumber']?></th>
		<th width='15%'><?=$Lang['eGuidance']['ctmeeting']['StudentName']?></th>
		<th width='40%'><?=$Lang['eGuidance']['ctmeeting']['Comments']?></th>
		<th width='25%'><?=$Lang['eGuidance']['ctmeeting']['Followup']?></th>
	</tr>
			<? foreach((array)$comments as $k=>$com):?>
				<tr>
		<td><?=($k+1)?></td>
		<td><?=$com['WebSAMSRegNo']?></td>
		<td><?=$com['ClassNumber']?></td>
		<td><?=$com['StudentName']?></td>
		<td class="tabletext">
						<? if ($view): ?>
							<?=($com['Comment'] ? nl2br($com['Comment']) : '-')?>
						<? else:?>
							<textarea class="tabletext "
				name="Comment[<?=$com['StudentID']?>]"
				id="Comment[<?=$com['StudentID']?>]" cols="40" rows="2"
				wrap="virtual" onfocus="this.rows=10;"><?=$com['Comment']?></textarea>
						<? endif;?>
					</td>
		<td class="tabletext">
						<? if ($view): ?>
							<?=($com['Followup'] ? nl2br($com['Followup']) : '-')?>
						<? else:?>
							<textarea class="tabletext "
				name="Followup[<?=$com['StudentID']?>]"
				id="Followup[<?=$com['StudentID']?>]" cols="20" rows="2"
				wrap="virtual" onfocus="this.rows=10;"><?=$com['Followup']?></textarea>
						<? endif;?>
					</td>
	</tr>
			<? endforeach;?>
				
			</table>
<?
            $x = ob_get_contents();
            ob_end_clean();
            
            return $x;
        }

        public function getAcademicYearByClassName($className)
        {
            global $Lang, $libguidance, $linterface;
            
            $academicYearList = $libguidance->getAcademicYearByClassName($className);
            $i = 0;
            ob_start();
            
            foreach ((array) $academicYearList as $k => $v) :
                ?>
			<?=($i++>0 ? "<br>" : "")?>
<input type="checkbox" name="AcademicYearID[]"
	id="AcademicYearID_<?=$k?>" value="<?=$k?>" checked>
<label for="AcademicYearID_<?=$k?>"><?=$v?></label>
<?
            endforeach
            ;
            
            $x = ob_get_contents();
            ob_end_clean();
            
            return $x;
        }

        // for ej only
        public function getSelectAcademicYear($select = '', $tag = '')
        {
            global $Lang, $libguidance, $linterface;
            
            $academicYearList = $libguidance->getAcademicYearList();
            
            $academicYearNameAry = array();
            if (count($academicYearList)) {
                foreach ((array) $academicYearList as $ay) {
                    $academicYearNameAry[] = array(
                        $ay['YearName'],
                        $ay['YearName']
                    );
                }
            }
            $yearSelection = getSelectByArray($academicYearNameAry, $tag, $select, $all = 0, $noFirst = 1);
            return $yearSelection;
        }

        // for ej only
        public function getSelectHistoryClass($academicYearName, $tag = '', $select = '', $all = 0, $noFirst = 0, $FirstTitle = "")
        {
            global $Lang, $libguidance, $linterface;
            
            $classList = $libguidance->getPastClassList($academicYearName);
            
            $classNameAry = array();
            if (count($classList)) {
                foreach ((array) $classList as $c) {
                    $classNameAry[] = array(
                        $c['ClassName'],
                        $c['ClassName']
                    );
                }
            }
            $classSelection = getSelectByArray($classNameAry, $tag, $select, $all = 0, $noFirst = 0, $FirstTitle = "");
            return $classSelection;
        }

        public function getSelectSenSatus($select = '', $tag = '')
        {
            global $Lang;
            
            $senStatusAry = array();
            $senStatusAry[] = array(
                '1',
                $Lang['eGuidance']['sen']['Confirm']
            );
            $senStatusAry[] = array(
                '0',
                $Lang['eGuidance']['sen']['NotConfirm']
            );
            $senStatusAry[] = array(
                '2',
                $Lang['eGuidance']['sen']['Quit']
            );
            
            $senStatusSelection = getSelectByArray($senStatusAry, $tag, $select, $all=1);
            return $senStatusSelection;
        }
        
        public function getAdjustTypeBySequence()
        {
            global $Lang, $libguidance, $linterface;
            
            $adjustTypeAry = $libguidance->getSENAdjustType();
            
            $x = '';
            $x .= '<table id="AdjustTypeTable" class="common_table_list">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th class="row_content" width="40%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['AdjustTypeChineseName'] . '</th>';
            $x .= '<th class="row_content" width="40%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['AdjustTypeEnglishName'] . '</th>';
            $x .= '<th class="row_content" align="left">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';
            
            $x .= '<tbody>';
            if (count($adjustTypeAry)) {
                foreach ((array) $adjustTypeAry as $_record) {
                    $recordID = $_record['TypeID'];
                    $englishName = $_record['EnglishName'];
                    $chineseName = $_record['ChineseName'];
                    $seqNo = $_record['SeqNo'];
                    
                    $x .= '<tr id="AdjustTypeRow_' . $recordID . '">';
                    $x .= '<td align="left"><div>' . $chineseName . '</div></td>';
                    $x .= '<td align="left"><div>' . $englishName . '</div></td>';
                    $x .= "<td class='Dragable' align='left'>";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['eGuidance']['settings']['MoveToReorder']);
                    $x .= '</td>';
                    $x .= "<input type='hidden' name='AdjustTypeID_" . $recordID . "' value='$recordID'>";
                    $x .= "</tr>";
                }
            }
            $x .= '</tbody>';
            $x .= "</table>";
            
            return $x;
        }
        
        public function getAdjustItemBySequence($typeID)
        {
            global $Lang, $libguidance, $linterface;
            
            $adjustItemAry = $libguidance->getSENAdjustItem($typeID);
            
            $x = '';
            $x .= '<table id="AdjustItemTable" class="common_table_list">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th class="row_content" width="40%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['AdjustItemChineseName'] . '</th>';
            $x .= '<th class="row_content" width="40%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['AdjustItemEnglishName'] . '</th>';
            $x .= '<th class="row_content" align="left">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';
            
            $x .= '<tbody>';
            if (count($adjustItemAry)) {
                foreach ((array) $adjustItemAry as $_record) {
                    $recordID = $_record['ItemID'];
                    $englishName = $_record['EnglishName'];
                    $chineseName = $_record['ChineseName'];
                    $seqNo = $_record['SeqNo'];
                    
                    $x .= '<tr id="AdjustItemRow_' . $recordID . '">';
                    $x .= '<td align="left"><div>' . $chineseName . '</div></td>';
                    $x .= '<td align="left"><div>' . $englishName . '</div></td>';
                    $x .= "<td class='Dragable' align='left'>";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['eGuidance']['settings']['MoveToReorder']);
                    $x .= '</td>';
                    $x .= "<input type='hidden' name='AdjustItemID_" . $recordID . "' value='$recordID'>";
                    $x .= "</tr>";
                }
            }
            $x .= '</tbody>';
            $x .= "</table>";
            
            return $x;
        }
        
        public function getServiceTypeBySequence()
        {
            global $Lang, $libguidance, $linterface;
            
            $serviceTypeAry = $libguidance->getServiceType();
            
            $x = '';
            $x .= '<table id="ServiceTypeTable" class="common_table_list">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th class="row_content" width="32%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['ServiceTypeChineseName'] . '</th>';
            $x .= '<th class="row_content" width="32%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['ServiceTypeEnglishName'] . '</th>';
            $x .= '<th class="row_content" width="21%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['ServiceScope'] . '</th>';
            $x .= '<th class="row_content" align="left">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';
            
            $x .= '<tbody>';
            if (count($serviceTypeAry)) {
                foreach ((array) $serviceTypeAry as $_record) {
                    $recordID = $_record['TypeID'];
                    $englishName = $_record['EnglishName'];
                    $chineseName = $_record['ChineseName'];
                    $type = ($_record['Type'] == 'Study') ? $Lang['eGuidance']['settings']['FineTune']['SupportType']['Study'] : $Lang['eGuidance']['settings']['FineTune']['SupportType']['Other'];
                    $seqNo = $_record['SeqNo'];
                    
                    $x .= '<tr id="ServiceTypeRow_' . $recordID . '">';
                    $x .= '<td align="left"><div>' . $chineseName . '</div></td>';
                    $x .= '<td align="left"><div>' . $englishName . '</div></td>';
                    $x .= '<td align="left"><div>' . $type . '</div></td>';
                    $x .= "<td class='Dragable' align='left'>";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['eGuidance']['settings']['MoveToReorder']);
                    $x .= '</td>';
                    $x .= "<input type='hidden' name='ServiceTypeID_" . $recordID . "' value='$recordID'>";
                    $x .= "</tr>";
                }
            }
            $x .= '</tbody>';
            $x .= "</table>";
            
            return $x;
        }
        
        public function getServiceItemBySequence($typeID)
        {
            global $Lang, $libguidance, $linterface;
            
            $serviceItemAry = $libguidance->getSENSupportService($typeID);
            
            $x = '';
            $x .= '<table id="ServiceItemTable" class="common_table_list">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th class="row_content" width="40%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['Support']['ChineseName'] . '</th>';
            $x .= '<th class="row_content" width="40%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['Support']['EnglishName'] . '</th>';
            $x .= '<th class="row_content" align="left">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';
            
            $x .= '<tbody>';
            if (count($serviceItemAry)) {
                foreach ((array) $serviceItemAry as $_record) {
                    $recordID = $_record['ServiceID'];
                    $englishName = $_record['EnglishName'];
                    $chineseName = $_record['ChineseName'];
                    $seqNo = $_record['SeqNo'];
                    
                    $x .= '<tr id="ServiceItemRow_' . $recordID . '">';
                    $x .= '<td align="left"><div>' . $chineseName . '</div></td>';
                    $x .= '<td align="left"><div>' . $englishName . '</div></td>';
                    $x .= "<td class='Dragable' align='left'>";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['eGuidance']['settings']['MoveToReorder']);
                    $x .= '</td>';
                    $x .= "<input type='hidden' name='ServiceItemID_" . $recordID . "' value='$recordID'>";
                    $x .= "</tr>";
                }
            }
            $x .= '</tbody>';
            $x .= "</table>";
            
            return $x;
        }
        
        public function getAllSENCaseType($SenCaseTypeFilterAry='', $SenCaseSubTypeFilterAry='')
        {
            global $libguidance;
            
            $senCaseTypeAry = $libguidance->getAllSENCaseType();
            $senCaseSubTypeAry = $libguidance->getAllSENCaseSubType();
            $senCaseTypeAssoc = BuildMultiKeyAssoc($senCaseTypeAry, array('Code'), array('Name'));
            $senCaseSubTypeAssoc = BuildMultiKeyAssoc($senCaseSubTypeAry, array('TypeCode', 'Code'), array('Name'),1);
            
            $senTypeList = '';
            foreach ((array) $senCaseTypeAssoc as $code=>$nameAry) {
                $checkedSenCaseTypeStr = in_array($code, (array)$SenCaseTypeFilterAry) ? ' checked' : '';
                
                $senTypeList .= '<tr><td><input type="checkbox" class="SenCaseTypeFilter" name="SenCaseTypeFilter[]" id="SenCaseTypeFilter_'.$code.'" value="'.$code.'"'.$checkedSenCaseTypeStr.'><label for="SenCaseTypeFilter_'.$code.'">'.$nameAry['Name'].'</label></td></tr>'."\n";
                if (isset($senCaseSubTypeAssoc[$code])) {
                    foreach ((array) $senCaseSubTypeAssoc[$code] as $subCaseTypeCode => $subCaseTypeName) {
                        $checkedSenCaseSubTypeStr = in_array($subCaseTypeCode, (array)$SenCaseSubTypeFilterAry) ? ' checked' : '';
                        $senTypeList .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="SenCaseSubTypeFilter" name="SenCaseSubTypeFilter[]" id="SenCaseSubTypeFilter_'.$subCaseTypeCode.'" value="'.$subCaseTypeCode.'"'.$checkedSenCaseSubTypeStr.'><label for="SenCaseSubTypeFilter_'.$subCaseTypeCode.'">'.$subCaseTypeName.'</label></td</tr>'."\n";
                    }
                }
            }
            return $senTypeList;
        }
        
        public function getAllSENCaseTypeFilter($LogicalType='and', $SenCaseTypeFilterAry='', $SenCaseSubTypeFilterAry='')
        {
            global $Lang, $i_general_required_field, $image_path, $LAYOUT_SKIN;
            
            $senTypeList = $this->getAllSENCaseType($SenCaseTypeFilterAry, $SenCaseSubTypeFilterAry);
            
            ob_start();
?>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table align="center" class="form_table_v30">
                        	<tr valign="top">
        						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eGuidance']['sen_case']['FilterSelection'];?></td>
        						<td class="tabletext">
                                    <input type="radio" name="LogicalType" id="LogicalAnd" value="and" <?php if ($LogicalType=='and') echo ' checked';?>><label for="LogicalAnd"><?php echo $Lang['eGuidance']['sen_case']['Filter']['LogicAnd'];?></label><br>
                                    <input type="radio" name="LogicalType" id="LogicalOr" value="or" <?php if ($LogicalType=='or') echo ' checked';?>><label for="LogicalOr"><?php echo $Lang['eGuidance']['sen_case']['Filter']['LogicOr'];?></label>
        						</td>
    						</tr>
                        	<tr valign="top">
        						<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eGuidance']['sen']['SENType'];?></td>
        						<td class="tabletext">
        							<table border="0">
										<?php echo $senTypeList;?>        							
        							</table>
        						</td>
    						</tr>
						</table>
        			</td>
        		</tr>
						
        		<tr>
        			<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
        		</tr>
        		<tr>
        			<td align="right">
        				<table width="100%" border="0" cellspacing="0" cellpadding="0">
        					<tr>
        						<td align="center">
        							<span>
    									<?=$this->GET_ACTION_BTN($Lang['Btn']['Search'], "button", "", "searchBtn_tb", "", "0", "", "actionBtn")?>
    									<?=$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "", "cancelBtn_tb", "", "0", "", "")?>
    								</span>
    							</td>
        					</tr>
        				</table>
        			</td>
        		</tr>
        	</table>

<?php
            $x = ob_get_contents();
            ob_end_clean();
            return $x;
        }

        public function getSENCaseTypeBySequence()
        {
            global $Lang, $libguidance, $linterface, $intranet_session_language;

            $senCaseTypeAry = $libguidance->getAllSENCaseType();

            if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
                $nameFieldAry = array('ChineseName','EnglishName');
            }
            else {
                $nameFieldAry = array('EnglishName','ChineseName');
            }

            $x = '';
            $x .= '<table id="SenCaseTypeTable" class="common_table_list">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th class="row_content" width="10%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['CaseTypeCode']. '</th>';
            $x .= '<th class="row_content" width="30%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['CaseType'.$nameFieldAry[0]]. '</th>';
            $x .= '<th class="row_content" width="30%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['CaseType'.$nameFieldAry[1]]. '</th>';
            $x .= '<th class="row_content" width="10%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['WithText']. '</th>';
            $x .= '<th class="row_content" align="left">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';

            $x .= '<tbody>';
            if (count($senCaseTypeAry)) {
                foreach ((array) $senCaseTypeAry as $_record) {
                    $recordID = $_record['SettingID'];
                    $code = $_record['Code'];
                    $englishName = $_record['EnglishName'];
                    $chineseName = $_record['ChineseName'];
                    $isWithText = $_record['IsWithText'] ? $Lang['General']['Yes'] : $Lang['General']['No'];

                    $x .= '<tr id="SenCaseTypeRow_' . $recordID . '">';
                    $x .= '<td align="left"><div>' . $code . '</div></td>';
                    $x .= '<td align="left"><div>' . $chineseName . '</div></td>';
                    $x .= '<td align="left"><div>' . $englishName . '</div></td>';
                    $x .= '<td align="left"><div>' . $isWithText . '</div></td>';
                    $x .= "<td class='Dragable' align='left'>";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['eGuidance']['settings']['MoveToReorder']);
                    $x .= '</td>';
                    $x .= "<input type='hidden' name='SenCaseTypeID_" . $recordID . "' value='$recordID'>";
                    $x .= "</tr>";
                }
            }
            $x .= '</tbody>';
            $x .= "</table>";

            return $x;
        }


        # $settingID is SENTypeID
        public function getSENCaseSubTypeBySequence($settingID)
        {
            global $Lang, $libguidance, $linterface, $intranet_session_language;

            $senCaseSubTypeAry = $libguidance->getAllSENCaseSubType('', $settingID);

            if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
                $nameFieldAry = array('ChineseName','EnglishName');
            }
            else {
                $nameFieldAry = array('EnglishName','ChineseName');
            }

            $x = '';
            $x .= '<table id="SenCaseSubTypeTable" class="common_table_list">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th class="row_content" width="10%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['CaseTypeCode']. '</th>';
            $x .= '<th class="row_content" width="42%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['CaseSubType'.$nameFieldAry[0]]. '</th>';
            $x .= '<th class="row_content" width="42%" align="left">' . $Lang['eGuidance']['settings']['FineTune']['CaseSubType'.$nameFieldAry[1]]. '</th>';
            $x .= '<th class="row_content" align="left">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';

            $x .= '<tbody>';
            if (count($senCaseSubTypeAry)) {
                foreach ((array) $senCaseSubTypeAry as $_record) {
                    $recordID = $_record['SettingID'];
                    $code = $_record['Code'];
                    $englishName = $_record['EnglishName'];
                    $chineseName = $_record['ChineseName'];

                    $x .= '<tr id="SenCaseSubTypeRow_' . $recordID . '">';
                    $x .= '<td align="left"><div>' . $code . '</div></td>';
                    $x .= '<td align="left"><div>' . $chineseName . '</div></td>';
                    $x .= '<td align="left"><div>' . $englishName . '</div></td>';
                    $x .= "<td class='Dragable' align='left'>";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['eGuidance']['settings']['MoveToReorder']);
                    $x .= '</td>';
                    $x .= "<input type='hidden' name='SenCaseSubTypeID_" . $recordID . "' value='$recordID'>";
                    $x .= "</tr>";
                }
            }
            $x .= '</tbody>';
            $x .= "</table>";

            return $x;
        }

        // -- End of Class
    }
}

?>