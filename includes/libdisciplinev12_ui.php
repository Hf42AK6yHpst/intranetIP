<?php
#  Editing by : Bill

##### Change Log [Start] #####
#
#	Date	:	2014-11-10 (Bill)
#	Detail	:	modified AP_Class_Report_Form(), allow to select modified date - $sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']
#
#	Date	:	2012-07-10 (Henry Chow)
#	Detail	:	added Get_Merit_Type_Select(), Get_WSCSS_Individual_Form(), Get_WSCSS_Form_Setting(), Get_WSCSS_Conduct_Grade_Setting_Form(), Get_All_AP_Item_Display
#				Conduct Grade [CRM : 2012-0221-1557-57098]
#
#	Date	:	2012-07-03 (Henry Chow)
#	Detail	:	modified Get_ApprovalGroup_Right_Index(), revise the UI of approval rights
#
#	Date	:	2012-04-16 (Henry Chow)
#	Detail	:	modified Get_ApprovalGroup_User_Index(), Get_Settings_ApprovalGroup_UI(), added Display_AP_SubMenu(), Get_ApprovalGroup_Right_Index()
#				assign AP category to approval group setting for approval
#
#	Date	:	2012-01-18 (Henry Chow)
#	Detail	:	modified AP_Class_Report_Form(), have option to count "unreleased" records [CRM No. : 2011-1111-1402-34132]
#
#	Date	:	2011-01-03 (Henry Chow)
#	Detail	:	modified AP_Class_Report_Form(), have option to count "wavied" records [CRM No. : 2010-0414-1608]
#
###### Change Log [End] ######

include_once("libdisciplinev12.php");

class libdisciplinev12_ui extends interface_html {

	function libdisciplinev12_ui(){
// 		parent::interface_html();
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '';
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>'."\n";
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/copypaste.js"></script>'."\n";

		$x .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/reportcard_rubrics.css" type="text/css" media="screen" />'."\n";
		$x .= '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />'."\n";
				
		return $x;
	}
	
	function Get_Settings_CurriculumPool_UI()
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN;
		
		$ldiscipline = new libdisciplinev12();
		
// 		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
// 		include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
		
// 		$scm_ui = new subject_class_mapping_ui();
// 		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		### Subject Selection
// 		$OnChange = "js_Changed_Subject_Selection(this.value);";
// 		$SubjectSelection = $scm_ui->Get_Subject_Selection('SubjectID', $SubjectID, $OnChange, $noFirst=1, $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=0);
		
		$x = '';
// 		$x .= '<br />'."\n";
		$x .= '<div id="DIV_ListTable">';
// 		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="FollowUpListTable" CLASS="common_table_list_v30">'."\n";
			$x .= '<col align="left" style="width: 45%;">'."\n";
			$x .= '<col align="left" style="width: 45%;">'."\n";
			$x .= '<col align="left" style="width: 10%;">'."\n";
				##### table header
				$x .= '<thead><tr>'."\n";
					$x .= '<th>'. $Lang['eDiscipline']['FollowUp'] .'</th>'."\n";
					$x .= '<th>'. $Lang['SysMgr']['FormClassMapping']['InUse'] .' / '. $Lang['SysMgr']['FormClassMapping']['NotInUse'] .'</th>'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
				$x .= '</tr></thead>'."\n";
				$x .= '<tbody>';
				##### table content
				$flist = $ldiscipline->returnFollowUpList();
// 				$x .= '<tr><td><div id="TableContentDiv"></div></td></tr>'."\n";
				foreach($flist as $k=>$d)
				{
					list($thisActionID, $thisTitle, $thisStatus) = $d;
					$x .= '<tr id="'.$thisActionID.'">';
					$x .= '<td>'. $this->Get_Thickbox_Edit_Div("ActionID_".$thisActionID, "ActionID_".$thisActionID, "jEditCode", $thisTitle) ;
					$x .= $this->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_".$thisActionID). '</td>';
					
					# in use / not in use
					if($thisStatus==1)
					{
						$StatusDisplay = "<a href='#' onClick='click_toggle($thisActionID);'><img src='". $image_path ."/".$LAYOUT_SKIN."/icon_function_on.gif' width='15' height='15' align='absmiddle' border='0' /></a> ".  $Lang['SysMgr']['FormClassMapping']['InUse'];
					}
					else
					{
						$StatusDisplay = "<a href='#' onClick='click_toggle($thisActionID);'><img src='". $image_path ."/".$LAYOUT_SKIN."/icon_function_off.gif' width='15' height='15' align='absmiddle' border='0' /></a> ".  $Lang['SysMgr']['FormClassMapping']['NotInUse'];
					}
					
					$x .= '<td>'. $StatusDisplay .'</td>';
					
					$x .= '<td class="Dragable" align="left">';
					$x .= $this->GET_LNK_MOVE("#", $Lang['Btn']['Move']);
					$x .= $this->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "js_Delete_Action('$thisActionID'); return false;");
					$x .= '	</td>';
				
					$x .= '</tr>'."\n";
				}
				##### add row
				$x .= '
					<tr id="AddRoleRow">
						<td colspan="3">
							<div class="table_row_tool row_content_tool">
								<a href="#" class="add_dim" title="'. $Lang['Btn']['New'] .'" onclick="Add_Form_Row(); return false;"></a>
							</div>
						</td>
					';
			$x .= '</tbody>';
		
			$x .= '</table>'."\n";
			$x .= '</div>';
// 		$x .= '</form>'."\n";
		return $x;
	}
	
	function Get_Settings_ApprovalGroup_UI($keyword="", $initial=1)
	{
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN;
		
		$ldiscipline = new libdisciplinev12();
				
		if($initial==1) {
			$x = '<div class="content_top_tool" id="SearchInputLayer">
		          <div class="Conntent_search">
		          	<input name="Keyword" id="Keyword" type="text" onkeyup="Check_Go_Search(event);"/>
		          </div>
	          	  <br style="clear:both" />
         	  </div>
         	  <div class="content_top_tool">
			  </div>';
		}
		$x .= '<div id="ApprovalGroupListLayer">';

			$x .= '<table id="FollowUpListTable" CLASS="common_table_list_v30">'."\n";
			$x .= '<col align="left" style="width: 35%;">'."\n";
			$x .= '<col align="left" style="width: 35%;">'."\n";
			$x .= '<col align="left" style="width: 20%;">'."\n";
			$x .= '<col align="left" style="width: 10%;">'."\n";
				##### table header
				$x .= '<thead><tr>'."\n";
					$x .= '<th>'. $Lang['eDiscipline']['GroupName'] .'</th>'."\n";
					$x .= '<th>'. $Lang['eDiscipline']['GroupDescription'] .'</th>'."\n";
					$x .= '<th>'. $Lang['eDiscipline']['NoOfMembers'] .'</th>'."\n";					
					$x .= '<th>&nbsp;</th>'."\n";
				$x .= '</tr></thead>'."\n";
				$x .= '<tbody>';
				##### table content
				$flist = $ldiscipline->returnApprovalGroupList($keyword);

				for($i=0; $i<sizeof($flist); $i++)
				{
					list($thisGroupID, $thisTitle, $thisDescription, $thisNoOfMember) = $flist[$i];
					$x .= '<tr id="'.$thisGroupID.'">';
					$x .= '<td><a href="approvalGroup_rights.php?GroupID='.$thisGroupID.'">'.$thisTitle.'</a></td>';					
					$x .= '<td>'. $thisDescription .'&nbsp;</td>';
					$x .= '<td><a href="approvalGroup_detail.php?GroupID='.$thisGroupID.'">'. $thisNoOfMember .'</a></td>';
					
					$x .= '<td align="left">';
					$x .= '<div class="table_row_tool"><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="approvalGroup_detail.php?GroupID='.$thisGroupID.'" /><a title="'.$Lang['Btn']['Delete'].'" class="delete_dim" href="javascript:void(0);" onclick="Delete_ApprovalGroup('.$thisGroupID.')" /></div>';
					$x .= '	</td>';
				
					$x .= '</tr>'."\n";
				}
				##### add row
				$x .= '
					<tr id="AddRoleRow">
						<td colspan="4">
							<div class="table_row_tool row_content_tool"><a title="'.$Lang['StaffAttendance']['AddGroup'].'" class="add" href="approvalGroup_detail.php" /></div>
						</td>
					';
			$x .= '</tbody>';
		
			$x .= '</table>'."\n";
			$x .= '</div>';
		return $x;
	}	
	
	function Get_ApprovalGroup_User_Index($GroupID, $xmsg="")
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $linterface, $button_new, $button_submit, $button_cancel;
		
		$ldiscipline = new libdisciplinev12();
		
		if($GroupID != null && trim($GroupID) != '' && $GroupID > 0 )
		{
			$ResultArr = $ldiscipline->Get_ApprovalGroup_Info($GroupID);
			$GroupTitle = $ResultArr[0]['Name'];
			$GroupDescription = $ResultArr[0]['Description'];
			$LastUpdateDate = $ResultArr[0]['DateModified'];
		}else
		{
			$GroupTitle = "";
			$GroupDescription = "";
			$LastUpdateDate = "";
		}
		
		//$x .= '<form name="form1" id="form1" action="approvalGroup_update.php" method="post" onSubmit="return checkForm()">';
		$x .= $this->Include_JS_CSS();
		$x .= "<div align='right'>".$linterface->GET_SYS_MSG($xmsg)."</div>";
		$x .= '
			  <div class="navigation">
					<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
					<a href="approvalGroup.php">'.$Lang['AccountMgmt']['Settings']['GroupList'].'</a>
					<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
          <span id="GroupTitleNavLayer">'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><br />
			  	<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />
			  </div>
<form name="form1" id="form1" action="approvalGroup_update.php" method="post" onSubmit="return checkForm()">
        <div class="detail_title_box">

					<span style="width:100%">
						<span style="width:10%">'.$Lang['AccountMgmt']['Settings']['GroupName'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupTitle" onmouseover=Show_Edit_Icon("GroupTitleEditIcon") onmouseout=Hide_Edit_Icon("GroupTitleEditIcon") class="jEditInput" >'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupTitleEditIcon" class="edit_dim" onClick="javascript:jsClickEdit();"></a></span>
							<input type="hidden" id="GroupTitle" name="GroupTitle" value="'.htmlspecialchars($GroupTitle,ENT_QUOTES).'" />
						</strong>
					</span>
					<div id="GroupTitleWarningLayer" style="display:none; color:red;" ></div>
					<br style="clear: both;" />
					<span style="width:100%">
		                <span style="width:10%">'.$Lang['AccountMgmt']['Settings']['Description'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupDescription" onmouseover=Show_Edit_Icon("GroupDescEditIcon") onmouseout=Hide_Edit_Icon("GroupDescEditIcon") class="jEditInput2" >'.htmlspecialchars($GroupDescription,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupDescEditIcon" class="edit_dim" onClick="javascript:jsClickEdit2();"></a></span>
							<input type="hidden" id="GroupDescription" name="GroupDescription" value="'.htmlspecialchars($GroupDescription,ENT_QUOTES).'" />
						</strong>
					</span>
          			<br style="clear: both;"/>
				</div>
</form>
              <p style="clear: both;"/>&nbsp;<p>
			';
	if(isset($GroupID) && $GroupID!="") {
		/*
		$x .= '
              <div class="shadetabs">
                <ul>
                  <li class="selected"><a>'.$Lang['StudentRegistry']['UserList'].'</a></li>
                </ul>
              </div>';
		*/
		$x .= $this->Display_AP_SubMenu($GroupID,$selected=1);
		$x .= '<div class="Conntent_tool"><a title="'.$Lang['StaffAttendance']['AddUser'].'" href="approval_group_user_add.php?GroupID='.$GroupID.'">'.$button_new.'</a></div>';
		
					
		$x .= '		
                <div class="Conntent_search" id="SearchInputLayer">
                    <input type="text" name="Keyword" id="Keyword" onkeyup="Check_Go_Search(event);" />
                </div>
                <br style="clear: both;"/>
              </div>
			  <input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />';
 
		$x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">
				  <tbody>
					<tr>
				    <td valign="bottom"><div class="table_filter"/></td>
				    <td valign="bottom"><div class="common_table_tool"> <a title="'.$Lang['Btn']['Delete'].'" class="tool_delete" href="javascript:void(0);" onclick="Delete_Member();" />'.$Lang['Btn']['Delete'].'</a></div></td>
				  	</tr>
				  </tbody>
				</table>';
       $x .= '      <div class="table_board" id="UserListLayer">';		
		$x .= $this->Get_ApprovalGroup_Users_List($GroupID);
				    
		if($LastUpdateDate != '') {
			$x .= '<br><p class="spacer"/>';
	   	    $x .= '<div class="edit_bottom" style="width:100%;">';
			$x .= '<span>'.$Lang['StaffAttendance']['LastUpdateOn'].$LastUpdateDate.'</span>';
			$x .= '</div>';
		}
		} else {
			$x .= '<div class="edit_bottom">'.
				$linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()")."&nbsp;".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='approvalGroup.php'")
				.'</div>';
		}
		$x .= '<p class="spacer"/>
		  	</div>
            <br/>
            <br/>';
        //$x .= '</form>';
		
		return $x;
	}	
	
	function Get_ApprovalGroup_Users_List($GroupID, $Keyword="")
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $i_no_record_exists_msg;
		
		$ldiscipline = new libdisciplinev12();
		
		$UserList = $ldiscipline->Get_ApprovalGroup_Users($GroupID, $Keyword);
		
		$x .= '
				<table class="common_table_list">	
				  <thead>
				    <tr>
				      <th class="num_check" width="1">#</th>
				      <th width="60%">'.$Lang['StaffAttendance']['StaffName'].'</th>
				      <th width="40%">'.$Lang['StaffAttendance']['UserType'].'</th>
				      <th class="num_check" width="1"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'StaffID[]\',this.checked)" /></th>
				      </tr>
				  </thead>
				  <tbody>';
		for($i=0;$i<sizeof($UserList);$i++)
		{
			$x .= '<tr>
				      <td>'.($i+1).'</td>
				      <td>'.$UserList[$i]['UserName'].'</td>
				      <td>'.($UserList[$i]['Teaching']==1?$Lang['Identity']['TeachingStaff']:$Lang['Identity']['NonTeachingStaff']).'</td>
				      <td><input type="checkbox" id="StaffID[]" name="StaffID[]" value="'.$UserList[$i]['UserID'].'" /></td>
				   </tr>';
		}
		if(sizeof($UserList)==0) {
			$x .= '<tr><td colspan="4" height="40" align="center">'.$i_no_record_exists_msg.'</td></tr>';	
		}
		   
		$x .= '</tbody>
			  </table>';
		return $x;
	}
	
	function AP_Class_Report_Form($startdate, $enddate, $meritType=1, $targetClass='', $include_waive=0, $include_unreleased=0, $dateSearchType=0)
	{
		global $ldiscipline, $linterface, $iDiscipline, $Lang, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select, $i_general_class, $i_Merit_Award, $i_Merit_Punishment, $button_view, $i_alert_pleaseselect, $i_Discipline_System_Reports_Include_Waived_Record;
		global $sys_custom;
		global $i_general_record_date;
		
		$select_class = $ldiscipline->getSelectClassWithWholeForm("name='targetClass'", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);
				
		$x ="
				<SCRIPT LANGUAGE=\"JavaScript\">
				function reset_innerHtml()
				{
					document.getElementById('div_DateEnd_err_msg').innerHTML = \"\";
					document.getElementById('div_Class_err_msg').innerHTML = \"\";
				}
				
				function checkForm()
				{
					//// Reset div innerHtml
					reset_innerHtml();
					
					var obj = document.form1;
					var error_no=0;
					var focus_field='';
									
					if (compareDate(obj.startdate.value, obj.enddate.value) > 0) 
					{
						document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color=\"red\">". $Lang['SysMgr']['SchoolCalendar']['JSWarning']['EndDateLaterThanStartDate'] ."</font>';
						error_no++;
						if(focus_field==\"\")	focus_field = \"startdate\";
					}
					
					if(obj.targetClass.value=='#')
					{
						document.getElementById('div_Class_err_msg').innerHTML = '<font color=\"red\">". $i_alert_pleaseselect ." ". $i_general_class ."</font>';
						error_no++;
						if(focus_field==\"\")	focus_field = \"targetClass\";
					}
					
					if(error_no>0)
					{
						eval(\"obj.\" + focus_field +\".focus();\");
						return false;
					}
					else
					{
						return true;
					}
				}
				</SCRIPT>
				
				<form name=\"form1\" action=\"result.php\" method=\"POST\" onSubmit=\"return checkForm();\">
				
				<div class=\"this_form\">
				<table class=\"form_table_v30\">
				<tr>
					<td class=\"field_title\"><span class=\"tabletextrequire\">*</span>". $Lang['eDiscipline']['DateRange'] ."</td>
					<td>". $linterface->GET_DATE_PICKER("startdate",$startdate) . $i_To . $linterface->GET_DATE_PICKER("enddate",$enddate) ."<br><span id=\"div_DateEnd_err_msg\"></span></td>
				</tr>
				". 
				($sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']? 
				"<tr>
					<td class=\"field_title\">".$Lang['eDiscipline']['APReportCust']['DateRangeType']."</td>
					<td>
						". $linterface->Get_Radio_Button("dateSearchType1", "dateSearchType", "0", ($dateSearchType==0? 1: 0) , "", $i_general_record_date) ."
						". $linterface->Get_Radio_Button("dateSearchType2", "dateSearchType", "1", ($dateSearchType==1? 1: 0) , "", $Lang['eDiscipline']['APReportCust']['ModifiedDate']) ."
					</td>
				</tr>" : "")."
				<tr>
					<td class=\"field_title\"><span class=\"tabletextrequire\">*</span>". $i_general_class ."</td>
					<td>". $select_class ."<br><span id=\"div_Class_err_msg\"></span></td>
				</tr>
				<tr>
					<td class=\"field_title\">". $iDiscipline['RecordType2'] ."</td>
					<td>
						". $linterface->Get_Radio_Button("meritType1","meritType","1", ($meritType==1? 1: 0) ,"", $i_Merit_Award) ."
						". $linterface->Get_Radio_Button("meritType2","meritType","-1", ($meritType==-1? 1: 0) ,"",$i_Merit_Punishment) ."
					</td>
				</tr>";
	
		$x .= "		
				<tr>
					<td>&nbsp;</td>
					<td>
						<input type='checkbox' name='include_waive' id='include_waive' value='1' ";
		$x .= ($include_waive ? " checked" : "");
		$x .= "><label for='include_waive'>".$i_Discipline_System_Reports_Include_Waived_Record."</label>
						<br>
						<input type='checkbox' name='include_unreleased' id='include_unreleased' value='1' ";
		$x .= ($include_unreleased ? " checked" : "");
		$x .= "><label for='include_unreleased'>".$Lang['eDiscipline']['IncludeUnreleasedRecords']."</label>
					</td>
				</tr>";

		$x .= "		
				</table>
				
				<div class=\"edit_bottom_v30\">
				<p class=\"spacer\"></p>
					". $linterface->GET_ACTION_BTN($button_view, "submit") ."
				<p class=\"spacer\"></p>
				</div>
				</div>
				
				</form>	
		";
		
		return $x;
	}
	
	function Display_AP_SubMenu($GroupID="", $selected=1)
	{
		global $Lang;
		$str .= '
			<div class="shadetabs">
                <ul>';
		# option 1
		$clsName = ($selected==1) ? " selected" : "";
		$href = ($clsName=="") ? ' href="approvalGroup_detail.php?GroupID='.$GroupID.'"' : '';
		$str .= ' <li class="'.$clsName.'"><a '.$href.'>'.$Lang['eDiscipline']['UserList'].'</a></li>';
		if(isset($GroupID) && $GroupID!="") {
			# option 2
			$clsName = ($selected==2) ? " selected" : "";
			$href = ($clsName=="") ? ' href="approvalGroup_rights.php?GroupID='.$GroupID.'"' : '';
			$str .= ' <li class="'.$clsName.'"><a '.$href.'>'.$Lang['eDiscipline']['Rights'].'</a></li>';
		}
        $str .= '</ul>
              </div>';
		return $str;
	}
	
	function Get_ApprovalGroup_Right_Index($GroupID, $xmsg="")
	{
		global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $Lang, $linterface, $button_new, $button_submit, $button_cancel;
		global $i_Merit_Award, $i_Merit_Punishment;
		
		$ldiscipline = new libdisciplinev12();
		
		if($GroupID != null && trim($GroupID) != '' && $GroupID > 0 )
		{
			$ResultArr = $ldiscipline->Get_ApprovalGroup_Info($GroupID);
			$GroupTitle = $ResultArr[0]['Name'];
			$GroupDescription = $ResultArr[0]['Description'];
			$LastUpdateDate = $ResultArr[0]['DateModified'];
		}else
		{
			$GroupTitle = "";
			$GroupDescription = "";
			$LastUpdateDate = "";
		}
		
		//$x .= '<form name="form1" id="form1" action="approvalGroup_update.php" method="post" onSubmit="return checkForm()">';
		$x .= $this->Include_JS_CSS();
		$x .= "<div align='right'>".$linterface->GET_SYS_MSG($xmsg)."</div>";
		$x .= '
			  <div class="navigation">
					<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
					<a href="approvalGroup.php">'.$Lang['AccountMgmt']['Settings']['GroupList'].'</a>
					<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>
          <span id="GroupTitleNavLayer">'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><br />
			  	<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />
			  </div>
<form name="form1" id="form1" action="approvalGroup_update.php" method="post" onSubmit="return checkForm()">
        <div class="detail_title_box">

					<span style="width:100%">
						<span style="width:10%">'.$Lang['AccountMgmt']['Settings']['GroupName'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupTitle" onmouseover=Show_Edit_Icon("GroupTitleEditIcon") onmouseout=Hide_Edit_Icon("GroupTitleEditIcon") class="jEditInput" >'.htmlspecialchars($GroupTitle,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupTitleEditIcon" class="edit_dim" onClick="javascript:jsClickEdit();"></a></span>
							<input type="hidden" id="GroupTitle" name="GroupTitle" value="'.htmlspecialchars($GroupTitle,ENT_QUOTES).'" />
						</strong>
					</span>
					<div id="GroupTitleWarningLayer" style="display:none; color:red;" ></div>
					<br style="clear: both;" />
					<span style="width:100%">
		                <span style="width:10%">'.$Lang['AccountMgmt']['Settings']['Description'].': </span>
						<strong>
							<span style="text-align:left;" id="EditableGroupDescription" onmouseover=Show_Edit_Icon("GroupDescEditIcon") onmouseout=Hide_Edit_Icon("GroupDescEditIcon") class="jEditInput2" >'.htmlspecialchars($GroupDescription,ENT_QUOTES).'</span><span class="table_row_tool"><a id="GroupDescEditIcon" class="edit_dim" onClick="javascript:jsClickEdit2();"></a></span>
							<input type="hidden" id="GroupDescription" name="GroupDescription" value="'.htmlspecialchars($GroupDescription,ENT_QUOTES).'" />
						</strong>
					</span>
          			<br style="clear: both;"/>
				</div>
</form>
              <p style="clear: both;"/>&nbsp;<p>
			';
		
			
			$x .= $this->Display_AP_SubMenu($GroupID,$selected=2);
			
			$Rights = $ldiscipline->GET_APPROVAL_GROUP_RIGHT_ID($GroupID);
			
			$x .= '<br>';
		
			$x .= '<form name="AccessRightForm" id="AccessRightForm" method="POST" action="approvalGroup_rights_update.php" onSubmit="return goSubmit();">';
			
			include_once("libinterface.php");
			$linterface = new interface_html();
			
			$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
			$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);
			
			### prepare the selected and de-select array of merit/demerit
			// for merit
			for($a=0, $a_max=count($merit_cats); $a<$a_max; $a++) {
				list($thisCatId, $thisCatName) = $merit_cats[$a];
				if(count($Rights)==0 || !in_array($thisCatId, $Rights)) {
					$merit_deselected_ary[] = array($thisCatId, $thisCatName);	
				} else {
					$merit_selected_ary[] = array($thisCatId, $thisCatName);	
				}
			}
			
			for($a=0, $a_max=count($demerit_cats); $a<$a_max; $a++) {
				list($thisCatId, $thisCatName) = $demerit_cats[$a];
				if(count($Rights)==0 || !in_array($thisCatId, $Rights)) {
					$demerit_deselected_ary[] = array($thisCatId, $thisCatName);	
				} else {
					$demerit_selected_ary[] = array($thisCatId, $thisCatName);	
				}
			}
			
			
			//$meritSelect = getSelectByArray($merit_cats, 'name="meritCategory[]" id="meritCategory" multiple size="10"', $Rights, 0, 1);
			//$demeritSelect = getSelectByArray($demerit_cats, 'name="demeritCategory[]" id="demeritCategory" multiple size="10"', $Rights, 0, 1);
			
			// Merit Select Menu
			$meritSelect = getSelectByArray($merit_selected_ary, 'name="meritCategory[]" id="meritCategory" multiple size="10"', array(), 0, 1);
			// Merit Deselect Menu
			$meritDeselect = getSelectByArray($merit_deselected_ary, 'name="meritDeselectCategory[]" id="meritDeselectCategory" multiple size="10"', array(), 0, 1);
			// Demerit Select Menu
			$demeritSelect = getSelectByArray($demerit_selected_ary, 'name="demeritCategory[]" id="demeritCategory" multiple size="10"', array(), 0, 1);
			// Demerit Deselect Menu
			$demeritDeselect = getSelectByArray($demerit_deselected_ary, 'name="demeritDeselectCategory[]" id="demeritDeselectCategory" multiple size="10"', array(), 0, 1);
			
			
			$x .= '<div class="table_board">
					<table class="form_table_v30">
					<tr>
						<td valign="top" width="25%" nowrap="nowrap" class="field_title">
							&nbsp;
						</td>
						<td class="field_title">'.$Lang['eDiscipline']['CategorySelected'].'</td>
						<td>&nbsp;</td>
						<td class="field_title">&nbsp;</td>
					</tr>
					<tr>
						<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ediscipline/icon_merit.gif">'.$i_Merit_Award.'</td>
						<td class="tabletext" valign="top">
							'.$meritSelect.'
							<br><br>
						</td>
						<td>'.
						$linterface->GET_BTN($Lang['Btn']['AddTo'], "button",'checkOptionTransfer(this.form.elements[\'meritDeselectCategory\'],this.form.elements[\'meritCategory\']);return false;')
						.'<br><br>'.
						$linterface->GET_BTN($Lang['Btn']['DeleteTo'], "button",'checkOptionTransfer(this.form.elements[\'meritCategory\'],this.form.elements[\'meritDeselectCategory\']);return false;')
						.'</td>
						<td class="tabletext" valign="top">
							'.$meritDeselect.'
						</td>
					</tr>
					<tr>
						<td>
							<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ediscipline/icon_demerit.gif">'.$i_Merit_Punishment.'
						</td>
						<td>
							'.$demeritSelect.'
							<br><br>
						</td>
						<td>'.
						$linterface->GET_BTN($Lang['Btn']['AddTo'], "button",'checkOptionTransfer(this.form.elements[\'demeritDeselectCategory\'],this.form.elements[\'demeritCategory\']);return false;')
						.'<br><br>'.
						$linterface->GET_BTN($Lang['Btn']['DeleteTo'], "button",'checkOptionTransfer(this.form.elements[\'demeritCategory\'],this.form.elements[\'demeritDeselectCategory\']);return false;')
						.'</td>
						<td class="tabletext" valign="top">
							'.$demeritDeselect.'
						</td>
					</tr>
					</table>
				</div>
				';

			//$x .= '<span class="tabletextremark">'.$Lang['StaffAttendance']['LastUpdateOn'].$LastUpdateDate.'</span>';
			$x .= '<br style:clear:both>';
			$x .= '<br style:clear:both>';
			
			$x .= '<div class="edit_bottom">'.
				$linterface->GET_ACTION_BTN($button_submit, "submit", "")."&nbsp;".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='approvalGroup.php'")
				.'</div>';
		$x .= $linterface->GET_HIDDEN_INPUT('GroupID', 'GroupID', $GroupID);
		$x .= '</form>';
		
		$x .= '<p class="spacer"/>
		  	</div>
            <br/>
            <br/>';
        //$x .= '</form>';
		
		return $x;
	}
	
	function Get_WSCSS_Conduct_Grade_Setting_Form($isViewOnly=0)
	{
		// for display of whole table 
		global $Lang, $image_path, $LAYOUT_SKIN;
		
		include_once("form_class_manage.php");
		
		$fcm = new form_class_manage();
		$forms = $fcm->Get_Form_List($GetMappedClassInfo=false, $ActiveOnly=1);

		// table content of each form (loop)
		for($a=0, $a_max=count($forms); $a<$a_max; $a++) {
			list($_YearId, $_YearName) = $forms[$a];
			
			// for display of grade setting of each form (one row for one form)
			$thisFormSetting = $this->Get_WSCSS_Form_Setting($_YearId, $isNewSetting=0, $isViewOnly);
			
			$form_content .= '
							<tr >
								<td width="15%" style="background-color:#FFFFFF">';
								
			if(!strpos($thisFormSetting,$Lang['General']['EmptySymbol'])) {
				//$form_content .= ' <a href="javascript:displayFormSetting('.$_YearId.');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/addon_tools/close_layer.gif" valign="absmiddle" border="0"></a>';
				$form_content .= '<span class="row_link_tool">
								<a href="javascript:displayFormSetting('.$_YearId.');" id="Link_'.$_YearId.'" class="zoom_in">'.$_YearName.'</a>';
			} else {
				$form_content .= $_YearName;	
			}
			$form_content .= '</td>
								<td width="80%" height="40">'.$thisFormSetting.'</td>
								<td width="5%"><div class="table_row_tool row_content_tool">
<a href="#TB_inline?height=550&width=800&inlineId=FakeLayer;" onClick="AddSetting('.$_YearId.');" class="add thickbox" title="'.$Lang['SysMgr']['CycleDay']['AddNewSettings'].'" id="editLink2">&nbsp;</a>
								</div>
								</td>
							</tr>
			';
		}
		$form_content .= '
							
		';


		// main table content
		$table_content = '
			<div id="mainTableContent">
			<table class="common_table_list_v30 edit_table_list_v30">
				<thead>
					<tr>
						<th width="15%">'.$Lang['General']['Form'].'</th>
						<th width="85%" colspan="2">'.$Lang['SysMgr']['FormClassMapping']['FormSetting'].'</th>
					</tr>
				</thead>
				<tbody>
					'.$form_content.'			
				</tbody>
			</table>
			</div>
			';

		return $table_content;
		
	}	
	
	function Get_WSCSS_Form_Setting($thisYearID="", $IsNewSetting=0, $isViewOnly=0)
	{
		global $Lang;
		
		$ldiscipline = new libdisciplinev12();
		// get each grade and sequence of Form
		$GradeAry = $ldiscipline->Get_WSCSS_Form_Setting_By_YearID($thisYearID);
		
		$returnTable = '<div id="FormDiv_'.$thisYearID.'" style="display:none">';
		
		// display for existing setting
		// loop for each grade setting of this specific form
		for($a=0, $a_max=count($GradeAry); $a<$a_max; $a++) {
			list($_YearGradeID, $_YearID, $_GradeChar, $_Sequence) = $GradeAry[$a];
			
			// loop to those 5 settings of this grade
			$thisSetting = $this->Get_WSCSS_Individual_Form($_YearGradeID, $thisYearID, $isViewOnly);
			
			$returnTable .= '<table class="common_table_list_v30 edit_table_list_v30">';
			
			$returnTable .= '<tr class="edit_table_head_bulk">';
			$returnTable .= '	<th width="90%">'.$Lang['General']['Grade'];
			if($isViewOnly) {
				$returnTable .= ' : '.$_GradeChar;	
			} else {
				$returnTable .= ' <input type="text" name="grade" id="grade" value="'.$_GradeChar.'" size="4">';
			}
			$returnTable .= '&nbsp;&nbsp;|&nbsp;&nbsp;'.$Lang['General']['Sequence'];
			if($isViewOnly) {
				$returnTable .= ' : '.$_Sequence;	
			} else {
				$returnTable .= ' <input type="text" name="sequence" id="sequence" value="'.$_Sequence.'" size="4">';
			}
			$returnTable .= '</th>';
			$returnTable .= '	<th width="10%">
						<div class="table_row_tool row_content_tool">
						<a href="#TB_inline?height=550&width=800&inlineId=FakeLayer;" onClick="EditSetting('.$_YearGradeID.');" class="edit thickbox" title="'.$Lang['Btn']['Edit'].'" id="editLink2">&nbsp;</a>

						<a title="'.$Lang['Btn']['Remove'].'" class="delete" href="javascript:;" onClick="RemoveSetting('.$_YearGradeID.')" /></a></div></th>';
			$returnTable .= '</tr>';
			
			for($b=0, $b_max=count($thisSetting); $b<$b_max; $b++) {
			
				$returnTable .= '<tr>';
				$returnTable .= '	<td colspan="100%">'.$thisSetting[$b].'</td>';
				$returnTable .= '</tr>';
				
			}
			
			$returnTable .= '</table>';	
			$returnTable .= '<br />';
		}
		
		// add new setting
		if($IsNewSetting) {
			$returnTable = '<div id="FormDiv_'.$thisYearID.'">';
		
			// loop to those 5 settings of this grade
			$thisSetting = $this->Get_WSCSS_Individual_Form();
			
			$returnTable .= '<br />';
			$returnTable .= '<table class="common_table_list_v30 edit_table_list_v30">';
			
			$returnTable .= '<tr class="edit_table_head_bulk">';
			$returnTable .= '	<th>'.$Lang['General']['Grade'].' <input type="text" name="grade" id="grade" value="" size="4">';
			$returnTable .= '&nbsp;&nbsp;|&nbsp;&nbsp;'.$Lang['General']['Sequence'].' <input type="text" name="sequence" id="sequence" value="" size="4"></th>';
			$returnTable .= '</tr>';
			
			for($b=0, $b_max=count($thisSetting); $b<$b_max; $b++) {
			
				$returnTable .= '<tr>';
				$returnTable .= '	<td colspan="100%">'.$thisSetting[$b].'</td>';
				$returnTable .= '</tr>';
				
			}
			
			$returnTable .= '</table>';	
			$returnTable .= '<br />';
			
			
		} else if(count($GradeAry)==0) {	// neither "new setting" nor with "existing setting"
			$returnTable .= $Lang['General']['EmptySymbol'];
		}
		
		$returnTable .= '</div>';
		
		if(!$IsNewSetting) {
			$returnTable .= '<div id="FormPreloadDiv_'.$thisYearID.'" style="display:inline;">'.(count($GradeAry)==0 ? $Lang['General']['EmptySymbol'] : '<a href="javascript:displayFormSetting('.$thisYearID.');">'.count($GradeAry).$Lang['eDiscipline']['WSCSS_Conduct_Grade_Total_Setting'].'</a>').'</div>';
		}
		
		return $returnTable;
	}
	
	function Get_WSCSS_Individual_Form($YearGradeID="", $YearID="", $isViewOnly=0)
	{
		global $Lang;
		
		$ldiscipline = new libdisciplinev12();
		$settingRawAry = $ldiscipline->Get_WSCSS_Individual_Form_Setting($YearGradeID);
		$settingAry = BuildMultiKeyAssoc($settingRawAry, "Sequence");
		
		for($a=1, $a_max=count($Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting']); $a<=$a_max; $a++) {
			
			if($isViewOnly && $settingAry[$a]['RecordStatus'] != 1) {
				continue;	
			}
			
			$thisNameId = ($YearGradeID=="") ? $a : $YearGradeID;
			$_thisSettingID = $settingAry[$a]['SettingID'];
			
			if($a==1 || $a==2) {
				$thisMeritType = '';
				
				if($isViewOnly) {
					$thisAmt = $settingAry[$a]['CriteriaValue'];
				} else {
					
					$thisAmt = '<input type="text" name="CriteriaValue['.$a.']" id="CriteriaValue['.$a.']" size="4" value="'.$settingAry[$a]['CriteriaValue'].'">';
				}
				$thisValue = str_replace("##INPUT_VALUE##", $thisAmt, $Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][$a]);
				
			} else {
				$arySelect = explode("#", $settingAry[$a]['CriteriaValue']);
				
				$meritArySelect = $arySelect[0];
				
				//$returnType = ($isViewOnly) ? "ViewOnly" : "Select";
				$returnType = "Select";
				
				
				$thisMeritType = $this->Get_All_AP_Item_Display(explode(',',$meritArySelect), $name="ItemID[".$a."][]", $id="ItemID[".$a."][]", $otherTag=" multiple size=10", $isViewOnly);
				
				if($isViewOnly) {
					$thisAmt =	$arySelect[1];
				} else {
					$thisAmt = '<input type="text" name="CriteriaValue['.$a.']" id="CriteriaValue['.$a.']" size="4" value="'.$arySelect[1].'">';
				}
				
				$thisValue = str_replace("##INPUT_VALUE##", $thisAmt, $Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'][$a]);
			}
			$thisChecked = ($settingAry[$a]['RecordStatus']) ? " checked" : "";
			
			if($YearGradeID=="") {
				$table_content[] .= '
					<input type="checkbox" name="optionInUse['.$thisNameId.']" id="optionInUse_'.$a.'" value="1" onClick="DisplayOption(this.checked, \'OptionDiv_'.$a.'\')"> <label for="">'.$thisValue.'</label>
					<br />'.
					($isViewOnly ? $thisMeritType : '<div id="OptionDiv_'.$a.'" style="display:none;">
					'.$thisMeritType.
					'</div>');
						
			} else {
				$thisDisabled = ($isViewOnly) ? " disabled" : "";
				$thisDisplay = $thisChecked ? "inline" : "none";
				
				$table_content[] .= '
					<input type="checkbox" name="optionInUse['.$a.']" id="optionInUse['.$a.']" value="1" '.$thisChecked.' '.$thisDisabled.' onClick="DisplayOption(this.checked, \'OptionDiv_'.$a.'\')"> <label for="">'.$thisValue.'</label>
					<br />'.
					($isViewOnly ? $thisMeritType : '<div id="OptionDiv_'.$a.'" style="display:'.$thisDisplay.';">
					'.$thisMeritType.
					'</div>');				
			}
		}
		
		
		return $table_content;	
	}
	
	function Get_Merit_Type_Select($selectMeritType=array(), $returnType="checkbox", $delimitor="", $name="", $id="", $otherTag="", $isViewOnly=0)
	{
		global $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit;
		include_once("libstudentprofile.php");
		$lstudentprofile = new libstudentprofile();
		
		$defaultMeritType[1] = $i_Merit_Merit;
		$defaultMeritType[2] = $i_Merit_MinorCredit;
		$defaultMeritType[3] = $i_Merit_MajorCredit;
		$defaultMeritType[4] = $i_Merit_SuperCredit;
		$defaultMeritType[5] = $i_Merit_UltraCredit;
			
		if (!$lstudentprofile->is_merit_disabled) {	
			$meritType[] = array(1, $i_Merit_Merit); 
		}
		if (!$lstudentprofile->is_min_merit_disabled) {	
			$meritType[] = array(2, $i_Merit_MinorCredit); 
		}
		if (!$lstudentprofile->is_maj_merit_disabled) {	
			$meritType[] = array(3, $i_Merit_MajorCredit); 
		}
		if (!$lstudentprofile->is_sup_merit_disabled) {	
			$meritType[] = array(4, $i_Merit_SuperCredit); 
		}
		if (!$lstudentprofile->is_ult_merit_disabled) {	
			$meritType[] = array(5, $i_Merit_UltraCredit); 
		}
		
		if($delimitor=="") $delimitor = "<br />";
		
		$thisReadOnly = ($isViewOnly) ? " READONLY" : "";
		
		switch($returnType) {
			case "ViewOnly" :
					$delim = "";		
					for($a=0, $a_max=count($selectMeritType); $a<$a_max; $a++) {
						if($selectMeritType[$a] != "") {
							$returnMerit .= $delim.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- '.$defaultMeritType[$selectMeritType[$a]];
							$delim = "<br />";
						}	
					}
					
					break;
					
			case "checkbox"	:
					
			
					for($a=0, $a_max=count($meritType); $a<$a_max; $a++) {
						list($_meritTypeId, $_meritTypeName) = $meritType[$a];
						$thisChecked = in_array($_meritTypeId, $selectMeritType) ? " checked" : "";
						$returnMerit .= "<input type='checkbox' name='".$name."' value='".$_meritTypeId."' id='".$id."' ".$thisChecked." ".$otherTag." ".$thisReadOnly.">".$_meritTypeName.$delimitor;	
					}
				
					break;
					
			case "Select"	:
			
					$returnMerit = '<select name="'.$name.'" id="'.$id.'" '.$otherTag.'>';
					for($a=0, $a_max=count($meritType); $a<$a_max; $a++) {
						list($_meritTypeId, $_meritTypeName) = $meritType[$a];
						$thisChecked = in_array($_meritTypeId, $selectMeritType) ? " selected" : "";
						$returnMerit .= "<option value='".$_meritTypeId."'".$thisChecked.">".$_meritTypeName."</option>";	
					}
					$returnMerit .= '</select>';
			
					break;
					
			default			:
					break;	
		}
		
		return $returnMerit;
	}
	
	function Get_All_AP_Item_Display($selectedApItem=array(), $name="", $id="", $otherTag="", $isViewOnly=false, $firstOption="", $recordType=1)
	{
		$ldiscipline = new libdisciplinev12();
		
		//$apItemInfo = $ldiscipline->getAllAPItemInfo();
		
		$sql = "SELECT CatID, CategoryName FROM DISCIPLINE_MERIT_ITEM_CATEGORY WHERE RecordType='$recordType' AND (RecordStatus IS NULL OR RecordStatus!=-1) ORDER BY DisplayOrder";
		$ApCategory = $ldiscipline->returnArray($sql);
		
		$returnValue = '<select name="'.$name.'" id="'.$id.'" '.$otherTag.'>';
		
		if($firstOption!=""){
			$returnValue .= '<option value="">'.$firstOption.'</option>';
		}
		
		for($a=0, $a_max=count($ApCategory); $a<$a_max; $a++) {
			list($_catid, $_catname) = $ApCategory[$a];
			
			$returnValue .= '<optgroup label="'.$_catname.'">';
			
			$sql = "SELECT ItemID, CONCAT(ItemCode,' - ',ItemName) as ItemName FROM DISCIPLINE_MERIT_ITEM WHERE CatID='$_catid' AND (RecordStatus is null OR RecordStatus!=-1) ORDER BY ItemCode, ItemName";
			$ApItem = $ldiscipline->returnArray($sql);
			
			for($b=0, $b_max=count($ApItem); $b<$b_max; $b++) {
				list($_itemid, $_itemname) = $ApItem[$b];
				
				$selected = (in_array($_itemid, $selectedApItem)) ? " selected" : "";
					
				$returnValue .= '<option value="'.$_itemid.'" '.$selected.'>'.$_itemname.'</option>';
				
				if($selected!="") {
					$returnText .= '&nbsp;&nbsp;&nbsp;&nbsp;- '.$_itemname.'<br>';
					//$returnText .= '<li>'.$_itemname.'</li>';
				}
			}
			
			$returnValue .= '</optgroup>';
				
		}
		$returnValue .= '</select>';
		
		//$returnText = '<ul>'.$returnText.'</ul>';
		
		return ($isViewOnly) ? $returnText : $returnValue;
	}
	
		
}

?>
