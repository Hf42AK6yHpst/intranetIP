<?php
// editing by 
 
/********************
 * change log:
 * 2018-05-08 Siuwan [ip.2.5.9.5.1]: added serverPublicPath
 * 2015-03-16 Ivan [ip.2.5.6.5.1.0]: created this file
 ********************/
include_once($intranet_root.'/includes/FlippedChannels/FlippedChannelsConfig.inc.php');

if (!defined("LIBFLIPPEDCHANNELS_DEFINED")) {
	define("LIBFLIPPEDCHANNELS_DEFINED", true);
	
	class libFlippedChannels extends libdb {
		var $ModuleTitle;
		
		function libFlippedChannels() {
			$this->libdb();
			$this->ModuleTitle = 'FlippedChannels';
		}
		
		function getCurCentralServerUrl() {
			global $FlippedChannelsConfig;
			
			return $FlippedChannelsConfig['serverPath'];
		}
		
		function getServerPublicPath() {
			global $FlippedChannelsConfig;
			
			return $FlippedChannelsConfig['serverPublicPath'];
		}
	}
}
?>