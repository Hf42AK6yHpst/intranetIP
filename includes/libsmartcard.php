<?php
// using by 

###############################################
#   2020-10-08 (Ray):    Modified GET_MODULE_OBJ_ARR, add earlyleave
#   2019-12-18 (Ray):    Modified GET_MODULE_OBJ_ARR, add KIS remove Attendance, Payment
#	2017-11-22 (Carlos): Modified GET_MODULE_OBJ_ARR(), added checking on group attendance admin to display take attendance page.
# 	2016-10-18 (Carlos): Added checking on [disallow non-teaching staff taking attendance]. 
#	2013-08-23 (Carlos): Modified GET_MODULE_OBJ_ARR(), if only have lesson attendance, only show [Take Attenance]
#	2012-08-30 (Carlos): Moved Staff Attendance [Daily Attendance Record] [Doctor Certificate] to eService > Staff Attendance
#	2012-08-23 (Carlos): GET_MODULE_OBJ_ARR() added Staff Attendance - Daily Attendance Record, Doctor Certificate
#
#	Date:	2011-09-15	YatWoon
#			updated GET_MODULE_OBJ_ARR(), add "Receipt Settings" in iSmartcard
#
###############################################

include_once("libadminjob.php");

if (!defined("LIBSMARTCARD_DEFINED"))                     // Preprocessor directive
{
define("LIBSMARTCARD_DEFINED", true);

class libsmartcard extends libdb
{
	function libsmartcard(){
	}

  /*
  * Get MODULE_OBJ array
  */
  function GET_MODULE_OBJ_ARR()
  {
  	include_once('libuser.php');
  	include_once("libcardstudentattend2.php");
  	
    global $plugin, $PATH_WRT_ROOT, $i_Discipline_System, $CurrentPage, $iDiscipline, $LAYOUT_SKIN, $image_path;
    ### wordings
    global $i_Attendance, $i_Profile_Attendance, $ip20TopMenu, $i_Payment, $i_Payment_AccountBalance, $i_Payment_PaymentRecord, $i_Payment_TransactionType_Payment;
    global $i_Payment_ValueAddedRecord, $i_StudentAttendance_Frontend_menu_TakeAttendance, $i_StudentAttendance_Menu_OtherFeatures_Reminder, $OverallRecord, $i_Payment_Transfer;
    global $i_Payment_Menu_PhotoCopier_Quota_Record, $i_Payment_Menu_PhotoCopier_Quota_Purchase, $i_StaffAttendance_SelfAttendance, $module_version;
    global $UserID, $CurrentPageArr, $special_option, $Lang, $sys_custom, $i_SmartCard_DailyOperation_Preset_Absence, $i_SmartCard_DailyOperation_ViewEarlyLeaveStatus;
    
    $lu 	= new libuser($UserID);
    $ladminjob = new libadminjob($UserID);
    $lcardattend = new libcardstudentattend2();
    
    $CurrentPageArr['iPCard'] = 1;
    
    # Current Page Information init
    $PageOverall = 0;
    
    $PageAttendance = 0;
    $PageTakeAttendance = 0;
    $PageAttendanceRecord = 0;
    $PageReminderRecord = 0;
    
    $PagePayment = 0;
    $PagePaymentBalance = 0;
    $PagePaymentRecord = 0;
    $PagePaymentAddValue = 0;
    $PagePaymentTransfer = 0;
    
    $PagePhotocopierViewQuota = 0;
    $PagePhotocopierPurchaseQuota = 0;
    
    $PageStaffOwnAttdenance = 0;

    $PagePresetAbsence = 0;
    $PageEarlyLeave = 0;
    switch ($CurrentPage) {
    	case "PageOverall":
        $PageOverall = 1;
        break;
		case "PageEarlyLeave":
			$PageAttendance = 1;
			$PageEarlyLeave = 1;
			break;
	    case "PageAttendance":
        $PageAttendance = 1;
        $PageAttendanceRecord = 1;
        break;
			case "PageTakeAttendance":
        $PageAttendance = 1;
        $PageTakeAttendance = 1;
        break;
      case "PageClassFloorPlanSetup":
        $PageAttendance = 1;
        $PageClassFloorPlanSetup = 1;
        break;
			case "PageReminderRecord":
        $PageAttendance = 1;
        $PageReminderRecord = 1;
        break;
                                
			case "PagePaymentBalance":
	      $PagePayment = 1;
	      $PagePaymentBalance = 1;
	      break;
			case "PagePaymentRecord":
        $PagePayment = 1;
        $PagePaymentRecord = 1;
        break;
			case "PagePaymentAddValue":
        $PagePayment = 1;
        $PagePaymentAddValue = 1;
        break;
        
        case "PageReceiptSettings":
        $PagePayment = 1;
        $PageReceiptSettings = 1;
        break;
        
			case "PagePaymentTransfer":
        $PagePayment = 1;
        $PagePaymentTransfer = 1;
        break;
			case "PagePhotocopierViewQuota":
				$PagePayment = 1;
				$PagePhotocopierViewQuota = 1;
				break;
			case "PagePhotocopierPurchaseQuota":
				$PagePayment = 1;
				$PagePhotocopierPurchaseQuota = 1;
				break;
			case "PageStaffOwnAttdenancePage":
				$PageStaffOwnAttendance = 1;
				break;
			case "PageStaffOwnAttendance":
				$PageStaffOwnAttendance = 1;
				$PageStaffAttendance = 1;
				break;
			case "PageStaffOwnRoster":
				$PageStaffOwnAttendance = 1;
				$PageStaffRoster = 1;
				break;
			case "PagePresetAbsence":
				$PagePresetAbsence = 1;
				break;
		/*	case "PageStaffDailyAttendanceRecord":
				$PageStaffOwnAttendance = 1;
				$PageStaffDailyAttendanceRecord = 1;
			break;
			case "PageStaffDoctorCertificate":
				$PageStaffOwnAttendance = 1;
				$PageStaffDoctorCertificate = 1;
			break; */
		}

    # Menu information
    if($lu->isStudent())
    $MenuArr["Overall"] = array($OverallRecord, $PATH_WRT_ROOT."home/smartcard/overall.php", $PageOverall, "$image_path/$LAYOUT_SKIN/smartcard/icon_overall.gif");
    
    if($plugin['attendancestudent'])
    {
    	$isGroupAdmin = false;
		if(method_exists($lcardattend,'getAttendanceGroupRecords')){
			$attendance_groups = $lcardattend->getAttendanceGroupRecords(array());
			$responsible_user_groups = $lcardattend->getAttendanceGroupAdminUsers(array('GroupID'=>Get_Array_By_Key($attendance_groups,'GroupID'),'UserID'=>$_SESSION['UserID']));
			$isGroupAdmin = count($responsible_user_groups)>0;
		}
    	
      $MenuArr["Attendance"] = array($i_Attendance, "", $PageAttendance, "$image_path/$LAYOUT_SKIN/smartcard/icon_accountbalance.gif");
      if($lu->isTeacherStaff() || $ladminjob->isSmartAttendenceAdmin() || $isGroupAdmin)	{
      	if(!($lcardattend->DisallowNonTeachingStaffTakeAttendance && $lu->isTeacherStaff() && $lu->teaching!=1)) {
			if ($sys_custom['StudentAttendance']['HideSmartCardTakeAttendance']) {

			} else {
				$MenuArr["Attendance"]["Child"]["TakeAttendance"] = array($i_StudentAttendance_Frontend_menu_TakeAttendance, $PATH_WRT_ROOT . "home/smartcard/attendance/take_attendance.php", $PageTakeAttendance, "$image_path/$LAYOUT_SKIN/smartcard/icon_takeattendance.gif");
			}
			$MenuArr["Attendance"]["Child"]["PageEarlyLeave"] = array($i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, $PATH_WRT_ROOT . "home/smartcard/attendance/early_leave.php", $PageEarlyLeave, "$image_path/$LAYOUT_SKIN/smartcard/icon_takeattendance.gif");
		}
      	if ($plugin['attendancelesson']) {
      		$MenuArr["Attendance"]["Child"]["ClassAttendFloorPlan"] = array($Lang['LessonAttendance']['ClassFloorPlanSetup'], $PATH_WRT_ROOT."home/smartcard/attendance/class_sitting_plan.php", $PageClassFloorPlanSetup, "$image_path/$LAYOUT_SKIN/smartcard/icon_sitting_plan.gif");
      	}
      }
      
      if ($lu->isTeacherStaff()) 
      	$MenuArr["Attendance"]["Child"]["AttendanceRecord"] = array($i_Profile_Attendance, $PATH_WRT_ROOT."home/smartcard/attendance/attendance_record/search_report.php", $PageAttendanceRecord, "$image_path/$LAYOUT_SKIN/smartcard/icon_checkstatus.gif");
      else 
      	$MenuArr["Attendance"]["Child"]["AttendanceRecord"] = array($i_Profile_Attendance, $PATH_WRT_ROOT."home/smartcard/attendance/attendance_record/attendance_list.php", $PageAttendanceRecord, "$image_path/$LAYOUT_SKIN/smartcard/icon_checkstatus.gif");
      if($lu->isTeacherStaff())	$MenuArr["Attendance"]["Child"]["ReminderRecord"] = array($i_StudentAttendance_Menu_OtherFeatures_Reminder, $PATH_WRT_ROOT."home/smartcard/attendance/reminder.php", $PageReminderRecord, "$image_path/$LAYOUT_SKIN/smartcard/icon_reiminderrecord.gif");

      if($lu->isTeacherStaff()) {
		  if($lcardattend->AllowClassTeacherManagePresetStudentAbsence == 1) {
			  include_once("libteaching.php");
			  $lteaching = new libteaching($UserID);
			  $TeachingClassList = $lteaching->returnTeacherClass($UserID);
			  if (count($TeachingClassList) > 0) {
				  $MenuArr["Attendance"]["Child"]["PresetAbsence"] = array($i_SmartCard_DailyOperation_Preset_Absence, $PATH_WRT_ROOT . "home/smartcard/attendance/preset_absence/new.php", $PagePresetAbsence, "$image_path/$LAYOUT_SKIN/smartcard/icon_checkstatus.gif");
			  }
		  }
	  }
    }else if($plugin['attendancelesson']) {
		$MenuArr["Attendance"] = array($i_Attendance, "", $PageAttendance, "$image_path/$LAYOUT_SKIN/smartcard/icon_accountbalance.gif");
		if($lu->isTeacherStaff() || $ladminjob->isSmartAttendenceAdmin())	{
			if($sys_custom['StudentAttendance']['HideSmartCardTakeAttendance']) {

			} else {
				$MenuArr["Attendance"]["Child"]["TakeAttendance"] = array($i_StudentAttendance_Frontend_menu_TakeAttendance, $PATH_WRT_ROOT . "home/smartcard/attendance/take_attendance.php", $PageTakeAttendance, "$image_path/$LAYOUT_SKIN/smartcard/icon_takeattendance.gif");
			}
		}
	}
                
    if($plugin['payment'])
    {
      $MenuArr["Payment"] = array($i_Payment_TransactionType_Payment, "", $PagePayment, "$image_path/$LAYOUT_SKIN/smartcard/icon_payment.gif");
      $MenuArr["Payment"]["Child"]["PaymentBalance"] = array($i_Payment_AccountBalance, $PATH_WRT_ROOT."home/smartcard/payment/paymentinfo.php", $PagePaymentBalance, "$image_path/$LAYOUT_SKIN/smartcard/icon_accountbalance.gif");
      if($lu->isStudent() or $lu->isParent()) {
      	$MenuArr["Payment"]["Child"]["PaymentRecord"] = array($i_Payment_PaymentRecord, $PATH_WRT_ROOT."home/smartcard/payment/payment_item.php", $PagePaymentRecord, "$image_path/$LAYOUT_SKIN/smartcard/icon_paymentrecord.gif");
      	$MenuArr["Payment"]["Child"]["PaymentAddValue"] = array($i_Payment_ValueAddedRecord, $PATH_WRT_ROOT."home/smartcard/payment/payment_addvalue.php", $PagePaymentAddValue, "$image_path/$LAYOUT_SKIN/smartcard/icon_addvalue.gif");
      }
      if($lu->isParent() && !$special_option['disable_children_transfer'])
      {
      	$MenuArr["Payment"]["Child"]["PaymentTransfer"] = array($i_Payment_Transfer, $PATH_WRT_ROOT."home/smartcard/payment/payment_transfer.php", $PagePaymentTransfer, "$image_path/$LAYOUT_SKIN/smartcard/icon_transfer.gif");
      }
      
      ### Added on 11/10/2007 by Ronald ###
	    if($plugin['payment_printing_quota'])
	    {
	      if(($lu->isTeacherStaff()) || ($lu->isStudent()))
	      {
	      	$MenuArr["Payment"]["Child"]["PagePhotocopierViewQuota"] = array($i_Payment_Menu_PhotoCopier_Quota_Record, $PATH_WRT_ROOT."home/smartcard/payment/payment_photocopier_quota.php", $PagePhotocopierViewQuota, "$image_path/$LAYOUT_SKIN/smartcard/icons_photocopy.gif");
	      	$MenuArr["Payment"]["Child"]["PagePhotocopierPurchaseQuota"] = array($i_Payment_Menu_PhotoCopier_Quota_Purchase, $PATH_WRT_ROOT."home/smartcard/payment/payment_purchase_quota.php", $PagePhotocopierPurchaseQuota, "$image_path/$LAYOUT_SKIN/smartcard/icons_photocopy_purchase.gif");
	    	}
	    }
	    
		### Added on 2011-09-15
		if($lu->isStudent()) 
		{
			include_once('libgeneralsettings.php');
			$GeneralSetting = new libgeneralsettings();
			$Settings = $GeneralSetting->Get_General_Setting('ePayment');
			if($Settings['AllowStudentChooseReceipt'])
			{
				$MenuArr["Payment"]["Child"]["ReceiptSettings"] = array($Lang['ePayment']['ReceiptSettings'], $PATH_WRT_ROOT."home/smartcard/payment/receipt_settings.php", $PageReceiptSettings);
			}
		}
	}
		
		if ($plugin['ePOS'] && $plugin['payment'] && ($lu->isStudent() || $lu->isParent())) {
			include_once($PATH_WRT_ROOT.'includes/libfamily.php');
			$libfamily = new libfamily();
			if ($lu->isStudent() || ($lu->isParent() && sizeof($libfamily->returnChildrens($_SESSION['UserID'])) > 0)) {
				$MenuArr["POS"] = array($Lang['Header']['Menu']['ePOS'], "",($CurrentPageArr['ePOS'] == 1));
	      $MenuArr["POS"]["Child"]["ReportHealthPersonal"] = array($Lang['ePOS']['HealthPersonalReport'], $PATH_WRT_ROOT."home/smartcard/pos/health_personal/index.php",(is_array($CurrentPage) && $CurrentPage['ReportHealthPersonal'] == 1));
	    }
		}

		// added on 20080326 by kenneth for staff attendance 2.0
		if ($module_version['StaffAttendance'] == 2.0 && $lu->isTeacherStaff() && $lu->needAttendance()) {
			$MenuArr["StaffAttendance"] = array($i_StaffAttendance_SelfAttendance, $PATH_WRT_ROOT."home/smartcard/StaffAttendance/OwnAttendance.php", $PageStaffOwnAttdenance, "$image_path/$LAYOUT_SKIN/smartcard/icon_personalattend.gif");
		}
		
		if (($module_version['StaffAttendance'] == 3.0 && $lu->isTeacherStaff()) || $sys_custom["StaffAttendance"]["SSORedirect"]) {
			include_once('libstaffattend3.php');
			$StaffAttend3 = new libstaffattend3();
		
			$StaffAttend3->Index_Page_Process();
			$MenuArr["StaffAttendance"] = array($i_StaffAttendance_SelfAttendance, '', $PageStaffOwnAttendance, "$image_path/$LAYOUT_SKIN/smartcard/icon_personalattend.gif");
			if ($sys_custom["StaffAttendance"]["SSORedirect"]) {
				$MenuArr["StaffAttendance"]["Child"]["Attendance"] = array($Lang['StaffAttendance']['Attendance'], $PATH_WRT_ROOT."home/smartcard/StaffAttendance/sso_attendance_record.php", $PageStaffAttendance);
				$MenuArr["StaffAttendance"]["Child"]["Roster"] = array($Lang['StaffAttendance']['Roster'], $PATH_WRT_ROOT."home/smartcard/StaffAttendance/sso_self_roster.php", $PageStaffRoster);
			}
			else {
				$MenuArr["StaffAttendance"]["Child"]["Attendance"] = array($Lang['StaffAttendance']['Attendance'], $PATH_WRT_ROOT."home/smartcard/StaffAttendance/attendance_record.php", $PageStaffAttendance);
				$MenuArr["StaffAttendance"]["Child"]["Roster"] = array($Lang['StaffAttendance']['Roster'], $PATH_WRT_ROOT."home/smartcard/StaffAttendance/self_roster.php", $PageStaffRoster);
			/*	if($sys_custom['StaffAttendance']['DailyAttendanceRecord'])
					$MenuArr["StaffAttendance"]["Child"]["DailyAttendanceRecord"] = array($Lang['StaffAttendance']['DailyAttendanceRecord'], $PATH_WRT_ROOT."home/smartcard/StaffAttendance/daily_attendance_record.php", $PageStaffDailyAttendanceRecord);
				if($sys_custom['StaffAttendance']['DoctorCertificate'])
					$MenuArr["StaffAttendance"]["Child"]["DoctorCertificate"] = array($Lang['StaffAttendance']['DoctorCertificate'], $PATH_WRT_ROOT."home/smartcard/StaffAttendance/doctor_certificate.php", $PageStaffDoctorCertificate);
			*/
			}
		}

      # change page web title
      $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
      $js.= 'document.title="eClass iSmartCard";'."\n";
      $js.= '</script>'."\n";

    # module information
    $MODULE_OBJ['title'] = $ip20TopMenu['iSmartCard'].$js;
    $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_smartcard.gif";
    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/smartcard/index.php";
    $MODULE_OBJ['title_css'] = "menu_opened";

	  if($_SESSION["platform"]=="KIS") {
		  unset($MenuArr["Attendance"]);
		  unset($MenuArr["Payment"]);
		  $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/smartcard/StaffAttendance/attendance_record.php";
	  }

    $MODULE_OBJ['menu'] = $MenuArr;

    return $MODULE_OBJ;
	}

}
}        // End of directive
?>
