<?php # Page Modifying By: 
/********************** Change Log **********************
#   2020-08-24 (Henry) : Modified Build_Mime_Text(), added para $isFromApp to use html content
#   2020-08-14 (Ray)    : modified Output_To_Browser safari
#   2020-06-22 (Tommy) : modified Get_Mail_Content_UI(), remove <a> tag if user cannot send email
#	2020-05-18 (Sam) : Remove file path value stored in ImageFileID to avoid WAF blocking
#                      Modified FileDeleteSubmit() to avoid WAF blocking by removing MailBody submission 
#	2020-02-03 (Sam) : Url escape the password before calling the osapi, modified open_account(), change_password()
#	2019-07-12 (Carlos) : fixed Replace_Links_Without_Anchor_Tag($html), if the hyperlink begins with www. not http, prepend http:// to the hyperlink for the value in <a href="">
#	2019-06-10 (Carlos) : added Replace_Links_Without_Anchor_Tag($html), applied to HTML_Transform($document). 
#	2019-05-31 (Carlos) : modified MIME_Decode($str) use alternative charset CP949 for charset ks_c_5601-1987.
#	2019-05-23 (Carlos) : modified HTML_Transform($document) to remove <textarea*></textarea> and <input*> elements from the html, as it breaks html editor and google forms use submit button with name "submit" that overwritten the form.submit() function that cause cannot submit the form. 
#	2019-01-17 (Carlos) : modified Get_Header_Flag($MsgNo,$Flag) to Get_Header_Flag($MsgNo,$Flag, $Options=0) allow use UID for setting $Options=FT_UID.
#						  modified Move_To_Trash($folderName, $uid), Move_Mail($sourceFolder, $uid, $destinationFolder), SetMailReceiptTrashMark($ParFolderName,$ParUID,$ParAction="",$ParType="",$ParUserID="") to avoid convert UID to MsgNo. Use UID is better than MsgNo because UID would not change but MsgNo may change by sorting. 
#	2018-08-14 (Carlos) : $sys_custom['iMailPlus']['EDSReportSpam'] - modified Get_Mail_Content_UI(), added Report Spam button to move current email to EDS_ReportSpam folder for analysis.
#	2018-08-08 (Carlos) : modified Get_Mail_Content_UI() and Get_Compose_Mail_Form(), apply cleanHtmlJavascript($html) to remove all script tags and onXXX event handlers.
#	2017-12-21 (Carlos) : modified Output_To_Browser(), detect Edge browser and urlencode filename.
#	2017-11-15 (Carlos) : modified Replace_Link($matches) to remove &gt; at the end of an url. 
#	2017-09-21 (Carlos) : improved BadWordFilter($Text), use str_replace() instead of preg_replace() to avoid pattern error and cause many error logs.
#	2017-07-27 (Ivan)	: added getUserId(), getUserEmail(), getUserPassword() for ASTRI app development
#	2017-06-09 (Carlos) : $sys_custom['iMailPlus']['CKEditor'] modified Get_Compose_Mail_Form(), changed to use CKEditor.
#	2017-03-13 (Carlos) : modified displayQuotaBar(), added javascript click handler on quota bar to recalculate used quota.
#	2016-09-21 (Carlos) : modified Get_Compose_Mail_Form(), fix forward from iMail Archive from email address format issue.
#	2016-06-13 (Carlos) : Modified Search_Mail(), fixed advanced search of attachment issue. 
#	2016-04-12 (Carlos) : Added htmlencodeEmailAddressAngleBrackets(), modified Get_Compose_Mail_Form(), to remove < > in content wrapped an email address to avoid it being treated as html tags and cause content after it missing.
#	2016-03-02 (Carlos) : Modified replaceQuotesForDisplayName(), remove double quotes, backward slashes, single quotes and &quot; in display name.
#	2016-01-29 (Carlos) : Modified Recursive_Check_Attachment($part), determine have attachment by checking disposition equals attachment or inline. 
# 	2015-11-20 (Carlos) : Remove reference to ImapCache object at __destruct() in order to let php release the momery.
#	2015-10-30 (Carlos) : Modified Get_Compose_Mail_Form(), if have reply-to header, add it to recipient list. 
#	2015-10-12 (Carlos) : Modified SetTotalQuota(), do not check the string result from osapi which is not accurate. 
#	2015-10-02 (Carlos) : Modified Get_Compose_Mail_Form(), remove < and > (which would become html tags and cause content missing) from recipient names for REPLY, REPLY ALL and FORWARD action. 
#	2015-09-29 (Carlos) : if $sys_custom['iMailPlus']['MonitorPerformance'] on, start timer in imap_gamma constructor, and do error log in destructor.
#	2015-09-17 (Carlos) : Modified GET_MODULE_OBJ_ARR(), use flag $sys_custom['iMailPlus']['DisableAutoForward'] to control availability of auto forwarding. 
#	2015-07-23 (Carlos) : Added osapi function recalculateQuota($targetEmail) to remove maildirsize file to force recalculate used quota.
#	2015-06-09 (Carlos) : Modified InsertMailReceiptByMailAddresses(), split insert values into 50 per chunk. 
# 	2015-04-27 (Carlos) : Modified GET_MODULE_OBJ_ARR(), hide auto reply preference setting for student type and with flag $sys_custom['iMailPlus']['DisableStudentAutoReply']
#	2015-04-17 (Carlos) : Added new methods to validate recipient input: splitEmailStringIntoArray(), isEmailAddressInString(), inQuotedExpression(), hasNonEscapedQuote(), splitNameEmailEntry(), replaceQuotesForDisplayName(), removeQuotesForDisplayName()
#	2015-04-13 (Carlos) : Modified Get_Compose_Mail_Form(), handle reply/reply all emails from iMail Archive.
#	2015-04-01 (Carlos) : Added removeQuotaAndSharedMailBoxMember()
# 	2015-01-13 (Carlos) : Modified HTML_Transform(), remove <base></base> tags
#	2014-11-05 (Carlos) : [ip2.5.5.12.1] Modified getImapCacheAgent(), pass email and password to imap_cache_agent constructor, not use session
# 	2014-10-27 (Carlos) : [ip2.5.5.10.1] Modified Get_Mail_Content_UI(), Get_Mail_Top_Bar(), added base64 encoded folder name in javascript functions and url links
#	2014-10-14 (Carlos) : [ip2.5.5.10.1] Modified Get_Compose_Mail_Form() make notification checkbox default checked, use flag $sys_custom['iMailPlusNotCheckNotify'] to make it unchecked
#	2014-10-03 (Carlos) : Modified Search_Mail(), change imap_search() charset all to UTF-8, seems only keywords are required to change different charsets for seraching
#	2014-09-29 (Carlos) : Modified GetMailReceipts(), display student class name and class number, sort by read time in descending order
#	2014-09-23 (Carlos) : Modified Get_Compose_Mail_Form(), remove < and > from sender email when forwarding iMail archive email because it becomes true html tag
#	2014-09-17 (Carlos) : Modified Get_Mail_Content_UI(), base64_encode() Folder name that pass to Document Routing
#	2014-09-15 (Carlos) : Modified Check_Any_Mail_Attachment(), use Recursive_Check_Attachment() to count if have any attachment in any parts
#	2014-08-26 (Carlos) : Modified readSpamCheck(), turnOnSpamCheck(), turnOffSpamCheck(), readVirusCheck(), turnOnVirusCheck(), turnOffVirusCheck(), added policy paramater to api call
#							Copy setSpamControlSetting($enable_custom), retriveSpamControlSetting(), setSpamControlLevel($spam_level), retriveSpamControlLevel(), readSpamScore() from libwebmail.php
#	2014-07-04 (Carlos) : Modified open_account() and change_password(), added parameter pwd for encrypted password, removed parameter newPassword
#	2014-07-03 (Carlos) : Modified Get_MailBox_Password() to use new approach to retrieve shared mailbox password
#	2014-06-20 (Carlos) : Modified Get_AddressBook_Select_Toolbar(), simplify recipient selection to [External Contacts] and [Internal Contacts]
#	2014-06-18 (Carlos) : Modified Search_Mail(), add from date and to date conditions for simple search
#	2014-06-10 (Carlos) : Modified MIME_Decode(), add charset case "x-unknown"  
#	2014-03-25 (Carlos) : Modified Search_Mail(), imap_search() specify charset/encoding to search more precisely
#	2014-03-14 (Carlos) : Modified executePost(), added special config for https post to use $SYS_CONFIG['Mail']['SSLPort']
#	2014-02-25 (Carlos) : Modified MIME_Decode(), use iconv() to do BIG5-HKSCS convertion 
#	2014-02-21 (Carlos) : Modified getMailRecord() to reuse StructureArray
#   2013-12-16 (Carlos) : Added Change_Preference_Mailbox() 
#   2013-12-12 (Carlos) : Added osapi changeAccountName()
#   2013-12-04 (Carlos) : Added getQuotaAlertSetting() and setQuotaAlertSetting()
#	2013-12-03 (Carlos) : Modified getMoveToSelection(), change onchange event to use jquery.alerts to avoid iPad dialog hang problem
#   2013-11-28 (Carlos) : Added Set_AVAS_Rules()
#	2013-11-01 (Carlos) : modified Get_Compose_Mail_Form(), if have Reply-To header, [Reply]/[Reply All] will send to this email
# 	2013-10-30 (Cameron): 1. add function: GetSQL_ParentEmail4JS(), GetSQL_ParentEmail4All(),GetSQL_ParentEmail4FormClass(),GetSQL_ParentEmail4SubjectGroup()
#						  2. modified 	GetAllParentEmailByUserIdentity, GetAllSameFormParentEmailByUserIdentity, GetAllSameClassParentEmailByUserIdentity, 
#										GetAllSameSubjectParentEmailByUserIdentity, GetAllSameSubjectGroupParentEmailByUserIdentity
#							 to use INTRANET_IMAIL_PREFERENCE.ForwardedEmail instead of INTRANET_USER.ImapUserEmail for sending email to parent if teacher login
#
# 	2013-09-26 (Carlos) : modified HTML_Transform(), add ";" to allowed character in url regular expression pattern 
#	2013-09-13 (Carlos) : modified Get_Compose_Mail_Form(), if $sys_custom['iMailPlusUseStandardUploadMethod'], force use standard file upload method; Reorder tabindex.
#	2013-06-05 (Carlos) : modified Get_Compose_Mail_Form(), IE7 or below use traditional file upload method
#	2013-05-24 (Carlos) : modified Get_Mail_Content_UI(), use jquery .ready() to load mail content from textarea to <td>, prevent problem of HTML content break system UI
#	2013-05-23 (Carlos) : modified Get_Compose_Mail_Form(), use textarea to store temporary mail content for onComplete load to FCKEditor
#	2013-04-19 (Carlos) : modified Get_Mail_Content_UI(), display BCC for Sent copy
#	2013-04-09 (Carlos) : modified Get_Compose_Mail_Form() to use plupload for uploading attachment and image 
#	2013-03-28 (Carlos) : replace word [View Message Source]
#	2013-03-25 (Carlos) : modified MIME_Decode() - use GB2312,GBK for gb18030 to do charset convertion
# 	2013-03-12 (Carlos) : modified addGroupBlockExternal() and removeGroupBlockExternal() split email array into 10 per chunk because too long will fail to post data
#	2013-03-01 (Carlos) : added formatDatetimeString() to format datetime string to ensure strtotime() can handle it
#	2013-02-14 [Carlos] : modified GET_MODULE_OBJ_ARR(), display number of unseen mails at document title
#						  modified Get_Sort_Index(), add parameter useUID to get UID instead of msg number
#	2013-01-11 [Carlos] : modified Get_Mail_Content_UI(), added Create Routing Document function for email with attachment
#	2012-12-18 [Carlos] : modified Get_Compose_Mail_Form(), fixed wording Android
#	2012-09-07 [Carlos] : modified Get_Compose_Mail_Form(), remove checking on ignoring self email for [Reply] and [Reply All]
#	2012-09-06 [Carlos] : modified Get_Mail_Content_UI(), added [Upload files to Digital Archive] button
#	2012-08-16 [Carlos] : added [Download all attachments] button to Get_Mail_Content_UI()
#	2012-08-14 [Carlos] : modified displayQuotaBar(), if unlimited quota, do not display quota bar
#	2012-06-14 [Carlos] : Modified displayQuotaBar(), if fail to get quota info via IMAP command, try osapi
#	2012-06-13 [Carlos] : Pass parameter iMail to delete_account() -> removeAccount()
#	2012-06-06 [Carlos] : Break down inner query into several queries for functions like GetAll%ByUserIdentity()
# 	2012-05-25 [Carlos] : Added delete log functions Log_Delete_Action() and Clean_Delete_Log(), modified Clear_Folder() 
#	2012-04-27 [Carlos] : Modified Output_To_Browser(), for IE replace %20 and %25 to original char
#   2012-04-19 [Carlos] : Modified Output_To_Browser() to cater Chinese file name for each type of browsers
# 	2012-04-17 [Carlos] : Modified displayQuotaBar(), display quota precise to 2 floating point numbers 
#	2012-04-10 [Carlos] : Added helper functions FormatRecipientOption() and ExtractEmailsFromString() for usage in select recipient
#	2011-12-30 [Carlos] : Modified Search_Mail() to use default search charset UTF-8 and BIG5, if $SYS_CONFIG['Mail']['SearchGBMail'] is on to include GB2312 and GBK
#	2011-12-05 [Carlos] : Modified Get_Quota_Info_List() make array key userlogin lowercase
#	2011-11-17 [Carlos] : Fixed getMailList() such that it can function correctly
#	2011-10-19 [Carlos] : Modified getIndentityGroupAccessQuota() and setIndentityGroupAccessQuota() added alumni type
#	2011-10-03 [Carlos] : Modified Get_Compose_Mail_Form(), added max attachment size remind msg
#	2011-09-09 [Carlos] : Modified Get_Compose_Mail_Form(), default check Notification if $sys_custom['iMailPlusDefaultCheckNotify']
#	2011-08-19 [Yuen] : Support iPad/Andriod
#	2011-08-10 [Carlos] : Added optional parameter $UserEmail to Get_Preference() which useful for force set sender display name
#	2011-07-25 [Carlos] : Modified Collect_Batch_Removal_Mails() to adopt search from cached mails
# 	2011-07-14 [Carlos] : Added Search_Cached_Mail(), modified Get_Gmail_Mode_Mail_Top_Bar() to enable faster search by cache
#	2011-07-05 [Carlos] : modified constructor, added enable cache mail checking
#	2011-06-16 [Carlos] : added flag CacheMailToDB, functions Check_New_Mails_For_Cached_Mails(),Remove_Not_Exist_Cached_Mails(),Add_Cached_Mails(),Get_Mail_Data_For_Cache(),Manage_Cached_Mail().
#	2011-05-31 [Carlos] : fix MIME_Decode() try to guess the correct encoding
#	2011-04-26 [Carlos] : Modified Get_Prev_Next_UID() to manipulate data using UID, eliminate unnecessary commands to convert MsgNo to UID;
#						  Get_Folder_Structure() reuse cached folder list to eliminate repeated imap command to get folders;
#	2011-04-21 [Carlos] : Added Remove_Batch_Removal_Settings(), modified Get_Batch_Removal_Settings(), Set_Batch_Removal_Settings(), Collect_Batch_Removal_Mails(),
#						  Batch_Removal_Expunge_Mails() and Batch_Removal_Restore_Mails() .
#	2011-04-15 [Carlos] : Added Split_Text_Into_Name_Email_Array(), modified Get_AddressBook_Select_Toolbar() to fix internal mode address selection
#	2011-04-15 [Carlos] : Modified Build_Mime_Text() added global var XeClassMailID
#	2011-04-14 [Carlos]	: Added attribute message_cache, modified GetMailPriority(), added Get_Mail_Receipt_Readed_Counting_Info()
#	2011-04-13 [Carlos] : (1) remove count unseen mails of reportSpam folder and reportNonSpam folder in GET_MODULE_OBJ_ARR()
						  (2) modified Get_Header_Flag() and eliminate repeated function call of Get_Header_Flag() in other functions
						  (3) remove call of GetMailPriority() to get important flag in Get_Mail_Content_UI(), can get the flag from getMailRecord() instead
#	2011-04-11 [Carlos] : modified Get_Mail_Content_UI() to hide Reply, Reply all and compose link to external mail if mail account is not allowed to send internet mail 
#	2011-04-06 [Carlos] : modified AccessInternetMail() added checking on INTRANET_SYSTEM_ACCESS 
#	2011-04-04 [Carlos] : Added default folder $SYS_CONFIG['Mail']['SystemMailBox']['HiddenFolder'] at constructor
#	2011-03-25 (Carlos) : add Batch_Removal_Expunge_Mails() and Batch_Removal_Restore_Mails()
#	2011-03-21 (Carlos)	: add Get_Batch_Removal_Settings(), Set_Batch_Removal_Settings() and Collect_Batch_Removal_Mails()
#	2011-02-21 (Carlos) : Fix HTML_Transform() hyperlink regular expression
#	2011-02-17 (Carlos) : Fix Get_Compose_Mail_Form() sometimes RE/FW will miss content
#	2011-02-15 (Carlos) : Fix forward mail from iMail Archive attachments problem 
#	2011-02-08 (Carlos) : Added Set_Batch_Quota(); Modified Get_Compose_Mail_Form() to use html_editor_imailplus
#	2011-02-07 (Carlos) : Added menu item Preference > IMAP to GET_MODULE_OBJ_ARR()
#	2011-01-25 (Carlos)	: Added function Get_Cached_Search_Result() and Set_Cached_Search_Result()
#	2011-01-18 (Carlos) : Remove iMail 2.0 check webmail from GET_MODULE_OBJ_ARR
#	2010-12-31 (Carlos) : Remove form tags from mail body in HTML_Transform()
#	2010-12-15 (Carlos) : Added ComposeTime to prevent multiple compose mail page deleting uploaded attchments and inline contents
#	2010-12-09 (Carlos) : Changed to use email as ID to get/set preference
#	2010-12-01 (Carlos) : modified able to use signature for corresponding shared mail box 
#	2010-11-18 (Carlos) : modify Get_Quota_Info_List() to use new api GetSingleQuota
#	2010-11-17 (Carlos) : add Get_Mail_Print_Version()
#	2010-11-11 (Carlos) : replace all datetime string which with timezone UT to UTC
#	2010-10-06 (Carlos) : add ChangeDomainSpamScore() 
#
#	Date	:	2010-09-24 [Carlos]
#	Details :	add attribute internal_mail_only, getInternalMailOnly() and setInternalMailOnly() 
#
#	Date	:	2010-09-14 [Carlos]
#	Details :	modify Get_Gmail_Mode_Mail_Top_Bar() and Get_Mail_Top_Bar() to get header info instead of whole mail content
#
#	Date	:	2010-09-01 [Carlos]
#	Details	:	add function Get_Quota_Info_List() to get all accounts' quota info
#
#	Date	:	2010-08-16 [Carlos]
#	Detail	:	add embed inline images function to compose mail
#
#	Date	:	2010-07-12 [Marcus]
#	Detail	:	add admin console realted functions function RetrieveGeneralSetting(),
#			getDayInTrashSpam(), getExternMailBanList(),getSendMailBanList(),
#			getInternetMailAccess(),getIndentityGroupAccessQuota(),getBadWordList()
#
#	Date	:	2010-06-11 [Yuen]
#	Detail	:	improve some UI in email view page
#
#
 ******************* End Of Change Log *******************/

if (!defined("LIBIMAPGAMMA_DEFINED"))                     // Preprocessor directive
{
define("LIBIMAPGAMMA_DEFINED", true);

# RecordType #
define("TYPE_TEACHER",			1);
define("TYPE_STUDENT",			2);
define("TYPE_PARENT",			3);
define("TYPE_ALUMNI",			4);
	
//include_once("osaccount.php");
include_once("libhttpclient.php");

//include_once("$intranet_root/includes/libwebmail.php");

//For checking sendmail
include_once("lib.php");
include_once("libdb.php");
include_once("libuser.php");

//include_once("common_function.php");
include_once("libfilesystem.php");
include_once("imap_cache_agent.php");
//include_once("$intranet_root/includes/libaccess.php");

//include_once("PHPMailer/PHPMailer.php");
include_once("$intranet_root/includes/libosaccount.php");

function Get_Timer() {
	$time_now = '';
	$mtime = microtime();
	$mtime = explode(" ",$mtime);
	$mtime = $mtime[1] + $mtime[0];
	$time_now = $mtime;
	return $time_now;
}

//class imap_gamma extends osaccount
class imap_gamma extends libosaccount
{

  var $has_webmail;
  var $type;
  var $host;
  var $folderDelimiter;
  var $mailaddr_domain;
  var $mailSystemAction;
  var $message_format;
  var $mail_server_port;

  var $inbox;

 var $embed_part_ids;

  var $times;

  var $parsed_part;          # Variable for parse_message
  var $message_charset;      # Variable for retrieve_message
  var $is_html_message;      # Variable for storing the flag of HTML message
  var $mime_type;
  var $mime_encoding;
        var $CurUserAddress;
        var $CurUserName;
        var $CurUserPassword;
        var $RootPrefix;
        var $MimeLineBreak;
        var $login_type;
        var $DefaultFolderList;
        var $FolderPrefix;
        var $ExtMailFlag; 
        var $user_file_path;
        var $AllFolderList;

    	var $mail_api_url;         # Separated mail server API
		var $ConnectionStatus;
		var $AllowSendMail;
		var $days_in_trash;
		var $days_in_spam;
		var $bad_word_list;
		var $internet_mail_access;
		var $ban_ext_mail_list;
		var $ban_send_mail_list;
		var $access_right; # array($teacher,$student,$parent)
		var $default_quota; # array($teacher,$student,$parent)
		var $internal_mail_only; #array($teacher,$student,$parent)
		var $quota_alert; # alert when quota used over this percentage 
			
		var $IMapCache=false; # IMapCache Agent
		var $message_cache; # cache the result of imap_socket->Get_Message_Cache_By_Batch()
		var $GmailMode = false; // use GMail IMAP Service
		var $CacheMailToDB = false; // switch to control cache mails to DB for search
		
      ### Constructor:
      ### Param: skipAutoLogin - not open imap connection if true, targetLogin/targetPassword - use this login information, if it is set, otherwise use session variables
      function imap_gamma($skipAutoLogin=false, $targetLogin = "", $targetPassword="",$isHalfOpen=false)
      {
               $this->libosaccount();
			   //$this->ConnectionStatus = true;

               /// Temp development use
               //$targetLogin = "roundcube";
               //$targetPassword = "roundcubeeclass";
               //$targetLogin = "tguser3@tg-a.broadlearning.com";
               //$targetPassword = "eclasstguser3";

               // for alpha
               /*$targetLogin = "roundcube";
               //$targetPassword = "roundcubeeclass";*/

               // for beta
               /*$targetLogin = "roundcube";
               //$targetPassword = "roundcubeeclass";*/
               // temp mailbox open, should remove later(20080425)
                                                         //$this->Temp_Init();

               global $plugin,$webmail_SystemType,$webmail_info,$SYS_CONFIG,$UserID,$PATH_WRT_ROOT,$file_path, $sys_custom;

			   if($sys_custom['iMailPlus']['MonitorPerformance']){
			   		$this->Start_Timer();
			   }

               #### Kenneth Wong (20080531): Fixed iMail configuration for ESF project
               $this->has_webmail = true;
               $this->type = 3;
				
				
			########### Add Default Folder #########
			if(!isset($SYS_CONFIG['Mail']['SystemMailBox']['HiddenFolder']))
			{
				$SYS_CONFIG['Mail']['SystemMailBox']['HiddenFolder'] = "Hidden";
			}
			##### End adding Default Folder ####### 
               #######
               #### Configuration from config file
//               if ($_SESSION['UserType'] == "2") {
//               	 $this->FolderPrefix = $SYS_CONFIG['StudMail']['FolderPrefix'];
//               	 $this->folderDelimiter = ($SYS_CONFIG['StudMail']['FolderDelimiter']==""?".":$SYS_CONFIG['StudMail']['FolderDelimiter']);
//	               $this->RootPrefix = ($SYS_CONFIG['StudMail']['FolderPrefix'] == "")? "":$SYS_CONFIG['StudMail']['FolderPrefix'].$this->folderDelimiter;
//	               $this->MimeLineBreak = $SYS_CONFIG['StudMail']['MimeLineBreak'];
//	               $this->host = $SYS_CONFIG['StudMail']['ServerIP'];
//	               $this->mail_server_port = $SYS_CONFIG['StudMail']['Port'].'/ssl';
//	               $this->mailaddr_domain = $SYS_CONFIG['StudMail']['UserNameSubfix'];                # Default mail domain only, domain name should be passed together with login name in ESF
//	               $GmailAPI = new gmail_api_agent();
//	    					 $GmailAPI->Enable_IMap_Setting();
//	    					 
//               }
//               else {
			   if($_SESSION['SSV_LOGIN_EMAIL'] == 'gamma_t1@broadlearning.com' 
			   		|| $_SESSION['SSV_LOGIN_EMAIL']=='gammademo@g1.broadlearning.com')
               {
               		$this->GmailMode = true;
               		$SYS_CONFIG['StudMail']['MimeLineBreak'] = "\r\n";
					$SYS_CONFIG['StudMail']['ServerIP'] = "imap.gmail.com"; // gmail
					$SYS_CONFIG['StudMail']['Port'] = "993";
					$SYS_CONFIG['StudMail']['SystemMailBox'] = array("inbox" => "INBOX", "SentFolder" => "[Gmail]/Sent Mail", "DraftFolder" => "[Gmail]/Drafts", "SpamFolder" => "[Gmail]/Spam", "TrashFolder" => "[Gmail]/Trash");
					$SYS_CONFIG['StudMail']['FolderDelimiter'] = "/";
					$SYS_CONFIG['StudMail']['FolderPrefix'] = "";
					$SYS_CONFIG['StudMail']['ServerType'] = "gmail"; //imap or exchange or gmail
					$SYS_CONFIG['Mail']['SystemMailBox'] = $SYS_CONFIG['StudMail']['SystemMailBox'];
					
					$this->FolderPrefix = $SYS_CONFIG['StudMail']['FolderPrefix'];
               	 	$this->folderDelimiter = ($SYS_CONFIG['StudMail']['FolderDelimiter']==""?".":$SYS_CONFIG['StudMail']['FolderDelimiter']);
	               $this->RootPrefix = ($SYS_CONFIG['StudMail']['FolderPrefix'] == "")? "":$SYS_CONFIG['StudMail']['FolderPrefix'].$this->folderDelimiter;
	               $this->MimeLineBreak = $SYS_CONFIG['StudMail']['MimeLineBreak'];
	               $this->host = $SYS_CONFIG['StudMail']['ServerIP'];
	               $this->mail_server_port = $SYS_CONFIG['StudMail']['Port'].'/ssl';
	               $this->mailaddr_domain = $SYS_CONFIG['StudMail']['UserNameSubfix'];
	               
	               $_SESSION['SSV_EMAIL_LOGIN'] = "gammademo@g1.broadlearning.com";
	               $_SESSION['SSV_EMAIL_PASSWORD'] = "gamma123";
	               $_SESSION['SSV_LOGIN_EMAIL'] = "gammademo@g1.broadlearning.com";
	               //$this->CurUserName = $_SESSION['SSV_EMAIL_LOGIN'];
	               //$this->CurUserPassword = $_SESSION['SSV_EMAIL_PASSWORD'];
	               //$this->CurUserAddress = $_SESSION['SSV_LOGIN_EMAIL'];
               }else
               {
	               $this->FolderPrefix = $SYS_CONFIG['Mail']['FolderPrefix'];
	               $this->folderDelimiter = ($SYS_CONFIG['Mail']['FolderDelimiter']==""?".":$SYS_CONFIG['Mail']['FolderDelimiter']);
	               $this->RootPrefix = ($SYS_CONFIG['Mail']['FolderPrefix'] == "")? "":$SYS_CONFIG['Mail']['FolderPrefix'].$this->folderDelimiter;
	               $this->MimeLineBreak = $SYS_CONFIG['Mail']['MimeLineBreak'];
	               $this->host = $SYS_CONFIG['Mail']['ServerIP'];
	               $this->mail_server_port = $SYS_CONFIG['Mail']['Port'];
	               $this->mailaddr_domain = $SYS_CONFIG['Mail']['UserNameSubfix'];                # Default mail domain only, domain name should be passed together with login name in ESF
	               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
	               $this->actype = "postfix";
               }
	             //  $this->host = "backup.broadlearning.com";
	    					 
//               }
               
//               if ($webmail_info['host'] == "")
//               {
//                   $this->host = $HTTP_SERVER_VARS['SERVER_ADDR'];
//               }
//               else
//               {
//                   $this->host = $webmail_info['host'];
//               }
               //$this->setRemoteAPIHost($this->host,$webmail_info['api_port']);
               

				//debug_pr($this);
               ### API path
               $this->mail_api_url['OpenAccount'] = "/osapi/openaccount.php";
               $this->mail_api_url['CheckAccountExist'] = "/osapi/checkaccount.php";
               
               $this->mail_api_url['GetAliasInfo'] = "/osapi/getAliasInfo.php";
               $this->mail_api_url['GetAliasList'] = "/osapi/getAliasList.php";
               $this->mail_api_url['AddAlias'] = "/osapi/addAlias.php";
               $this->mail_api_url['RemoveAlias'] = "/osapi/removeAlias.php";

               $this->mail_api_url['AddAutoReply'] = "/osapi/addAutoreply.php";
               $this->mail_api_url['ReadAutoReply'] = "/osapi/readAutoreply.php";
               $this->mail_api_url['RemoveAutoReply'] = "/osapi/removeAutoreply.php";
			   $this->mail_api_url['AddAliasAutoReply'] = "/osapi/addAliasAutoreply.php";
			   $this->mail_api_url['RemoveAliasAutoReply'] = "/osapi/removeAliasAutoreply.php";

               $this->mail_api_url['AddBlackList'] = "/osapi/addBlackList.php";
               $this->mail_api_url['ReadBlackList'] = "/osapi/readBlackList.php";
               $this->mail_api_url['RemoveBlackListRecord'] = "/osapi/removeBlackListRecord.php";

               $this->mail_api_url['AddWhiteList'] = "/osapi/addWhiteList.php";
               $this->mail_api_url['ReadWhiteList'] = "/osapi/readWhiteList.php";
               $this->mail_api_url['RemoveWhiteList'] = "/osapi/removeWhiteListRecord.php";

               $this->mail_api_url['BypassSpamCheck'] = "/osapi/bypassSpamCheck.php";
               $this->mail_api_url['BypassVirusCheck'] = "/osapi/bypassVirusCheck.php";
               $this->mail_api_url['ReadSpamCheck'] = "/osapi/readSpamStatus.php";
               $this->mail_api_url['ReadVirusCheck'] = "/osapi/readVirusStatus.php";
               
               $this->mail_api_url['SetMailForward'] = "/osapi/setMailForward.php";
               $this->mail_api_url['GetMailForward'] = "/osapi/getMailForward.php";
               $this->mail_api_url['RemoveMailForward'] = "/osapi/removeMailForward.php";
              
               $this->mail_api_url['AddGroupBlockExternal'] = "/osapi/addGroupBlockExternalEmail.php";
               $this->mail_api_url['RemoveGroupBlockExternal'] = "/osapi/removeGroupBlockExternalEmail.php";
               
               $this->mail_api_url['SearchEmail'] = "/osapi/searchEmail.php";
               $this->mail_api_url['ChangeTrashDay'] = "/osapi/changeTrashDay.php";
               $this->mail_api_url['ChangeSpamDay'] = "/osapi/changeSpamDay.php";
               
				$this->mail_api_url['SetTotalQuota'] = "/osapi/setTotalQuota.php";
				$this->mail_api_url['GetQuotaTable'] = "/osapi/getQuotaTable.php";
				
				$this->mail_api_url['ChangeDomainSpamScore'] = "/osapi/changeDomainSpamScore.php";
				$this->mail_api_url['ReadSpamScore'] = "/osapi/getSpamScore.php";
				
				$this->mail_api_url['GetSingleQuota'] = "/osapi/getSingleQuota.php";
				$this->mail_api_url['SetBatchQuota'] = "/osapi/setBatchQuota.php";
				
				$this->mail_api_url['SetAvasRules'] = "/osapi/setAvasRules.php"; // post rules and scores
				$this->mail_api_url['ChangeAccountName'] = "/osapi/changeAccountName.php"; // change account email
				$this->mail_api_url['RecalculateQuota'] = "/osapi/recalculateQuota.php"; // remove /home/domains/[domain]/[userlogin]/Maildir/maildirsize to force recalculating the used quota
//				$targetLogin = "iconnect@backup.broadlearning.com";
//				$targetPassword = "BL2009mail";
				
				
				
               $this->RetrieveGeneralSetting();
				
               # Login info
               if ($targetLogin != "")
               {
                   $this->CurUserAddress = $targetLogin;
                   $this->CurUserPassword = $targetPassword;
               }
               else
               {
                   /*$this->CurUserName = $_SESSION['SSV_LOGIN_LOGIN'];
                   $this->CurUserPassword = $_SESSION['SSV_LOGIN_PASSWORD'];
                   $this->CurUserAddress = ($_SESSION['SSV_LOGIN_EMAIL']==""?($this->CurUserName."@".$this->mailaddr_domain):$_SESSION['SSV_LOGIN_EMAIL']);*/
                   // changed to entertain the mailbox switch function
                   if($_SESSION['SSV_EMAIL_LOGIN']!=''&&$_SESSION['SSV_EMAIL_PASSWORD']!=''&&$_SESSION['SSV_LOGIN_EMAIL']!='')
                   {
	                   $this->CurUserName = $_SESSION['SSV_EMAIL_LOGIN'];
	                   $this->CurUserPassword = $_SESSION['SSV_EMAIL_PASSWORD'];
	                   $this->CurUserAddress = $_SESSION['SSV_LOGIN_EMAIL'];
               		}
					else
					{
						$this->ConnectionStatus = false;
						return;				
					}                       
                                      
//                   if ($this->CurUserAddress == 'tony.chan@mail22.esf.edu.hk' || 
//                   		 $this->CurUserAddress == 'testing3@esfcentre.edu.hk' || 
//                   		 $this->CurUserAddress == 'testing1@esfcentre.edu.hk' || 
//                   		 $this->CurUserAddress == 'testing2@esfcentre.edu.hk' || 
//                   		 $this->CurUserAddress == 'tgcestest1@esfcentre.edu.hk' || 
//	    								 $this->CurUserAddress == 'tgcestest2@esfcentre.edu.hk' || 
//	    								 $this->CurUserAddress == 'tgcestest3@esfcentre.edu.hk') {
//                   	$this->host = $SYS_CONFIG['ExchangeServerIP'];
//                   }
               }
               
               
               
               //$inbox = imap_open("{backup.broadlearning.com:143/novalidate-cert}","iconnect@backup.broadlearning.com","BL2009mail",OP_HALFOPEN);
				//var_dump($inbox);
							
							// check if the email used to connect is valid for mail server or not
							$tempAddress = explode('@',$this->CurUserAddress);
							$Domain = $tempAddress[1];
							//$this->ExtMailFlag = ($Domain != $this->mailaddr_domain) && ($this->CurUserAddress != 'tgcestest1@esfcentre.edu.hk') && ($this->CurUserAddress != 'tgcestest2@esfcentre.edu.hk') && ($this->CurUserAddress != 'tgcestest3@esfcentre.edu.hk');
							
							 //ad hoc flow to handle exchange server type 2
//               if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && $SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && $this->CurUserAddress != "tgcestest1@esfcentre.edu.hk" && $this->CurUserAddress != 'testing2@esfcentre.edu.hk' && $this->CurUserAddress != 'testing3@esfcentre.edu.hk' && $this->CurUserAddress != 'testing1@esfcentre.edu.hk')
//               {
//	                $this->CurUserName = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@")); //get the username before '@' as the login to exchange server
//               }	
				if($SYS_CONFIG['Mail']['UsernameLoginWithoutDomain'] === true){
					if(stristr($this->CurUserAddress,"@")) $this->CurUserAddress = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@"));
				}
               # Retrieve General Setting
				
				####### Enable cache mail checking #######
				if(isset($SYS_CONFIG['Mail']['CacheMail']) && $SYS_CONFIG['Mail']['CacheMail']==true)
				{
					if(isset($SYS_CONFIG['Mail']['CacheSync']) && $SYS_CONFIG['Mail']['CacheSync']==true){
						
					}else if(!isset($_SESSION['SSV_GAMMA_USE_CACHE'])){
						include_once("libdb.php");
						$ldb = new libdb();
						$sql = "SELECT SyncStatus FROM TEMP_MAIL_CACHED_MAILS_STATUS WHERE UserEmail = '".$this->CurUserAddress."' ";
						$TempSyncStatus = $ldb->returnVector($sql);
						if($TempSyncStatus[0]=='C'){
							$_SESSION['SSV_GAMMA_USE_CACHE'] = 1;
						}else{
							$_SESSION['SSV_GAMMA_USE_CACHE'] = 0;
						}
					}
					
					$TempEnableCacheUsers = isset($SYS_CONFIG['Mail']['EnableCacheUser'])?$SYS_CONFIG['Mail']['EnableCacheUser']:array();
					$EnableCache = (in_array($_SESSION['SSV_LOGIN_EMAIL'],$TempEnableCacheUsers) || $_SESSION['SSV_GAMMA_USE_CACHE']==1 || $SYS_CONFIG['Mail']['CacheSync']==true);
					$this->CacheMailToDB = isset($SYS_CONFIG['Mail']['CacheMail'])? ($SYS_CONFIG['Mail']['CacheMail'] && $EnableCache):false;
				}
				####### End of enable cache mail checking #######
				
				//debug_pr($_SESSION);
               if (!$skipAutoLogin && !$this->ExtMailFlag)
               {
                    $open_inbox_success = $this->openInbox($this->CurUserAddress, $this->CurUserPassword,'', $isHalfOpen);				
					if(!$open_inbox_success) // if connection fail, try one more attempt
						$this->openInbox($this->CurUserAddress, $this->CurUserPassword,'', $isHalfOpen);
					
					if ($this->inbox)
                    {
//                    	if ($_SESSION['SSV_USER_TYPE'] == "2") {
//												// create default folder list
//	                      $DefaultFolderList = $SYS_CONFIG['StudMail']['SystemMailBox'];
//	                      foreach ($DefaultFolderList as $Key=> $Val) {
//	                      	if ($Val != $SYS_CONFIG['StudMail']['FolderPrefix']) {
//	                        	$Result[$Val.'Create'] = $this->createMailFolder($SYS_CONFIG['StudMail']['FolderPrefix'],$Val);
//	                          $DefaultFolderList[$Key] = $this->RootPrefix.$Val;
//	                       	}
//	                      }
//	                      /*for ($i=0; $i < sizeof($DefaultFolderList); $i++) {
//	                           if ($DefaultFolderList[$i] != $SYS_CONFIG['Mail']['FolderPrefix']) {
//	                               $Result[$DefaultFolderList[$i].'Create'] = $this->createMailFolder($SYS_CONFIG['Mail']['FolderPrefix'],$DefaultFolderList[$i]);
//	                               $DefaultFolderList[$i] = $this->RootPrefix.$DefaultFolderList[$i];
//	                           }
//	                      }*/
//											}
//											else {
	                      // create default folder list
	                      $DefaultFolderList = $SYS_CONFIG['Mail']['SystemMailBox'];
	                      
	                      if(!isset($_SESSION['SSV_GAMMA_INIT_FOLDER'])){
		                      foreach ($DefaultFolderList as $Key=> $Val){
		                      	if ($Val != $SYS_CONFIG['Mail']['FolderPrefix']){
		                      		$Result[$Val.'Create'] = $this->createMailFolder($SYS_CONFIG['Mail']['FolderPrefix'],$Val);
		                        	$DefaultFolderList[$Key] = $this->RootPrefix.$Val;
		                       	}
		                      }
	                      	  $_SESSION['SSV_GAMMA_INIT_FOLDER']=1;
	                      }else{
	                      	  foreach ($DefaultFolderList as $Key=> $Val){
		                      	if ($Val != $SYS_CONFIG['Mail']['FolderPrefix']){
		                        	$DefaultFolderList[$Key] = $this->RootPrefix.$Val;
		                       	}
		                      }
	                      }
	                      
	                      //$this->AllFolderList = $this->getMailFolders();
	                      //$this->my_debug_var = $Result;
	                      /*for ($i=0; $i < sizeof($DefaultFolderList); $i++) {
	                           if ($DefaultFolderList[$i] != $SYS_CONFIG['Mail']['FolderPrefix']) {
	                               $Result[$DefaultFolderList[$i].'Create'] = $this->createMailFolder($SYS_CONFIG['Mail']['FolderPrefix'],$DefaultFolderList[$i]);
	                               $DefaultFolderList[$i] = $this->RootPrefix.$DefaultFolderList[$i];
	                           }
	                      }*/
//                    	}
						$this->ConnectionStatus = true;
						
						# create folder to store user file
						
						$personal_path = "/file/gamma_mail/u$UserID";
						
						if(!is_dir($file_path.$personal_path))
						{
							$lfs = new libfilesystem();
							$lfs->folder_new($file_path.$personal_path, 0755, true);
						}
						$this->user_file_path = $PATH_WRT_ROOT."file/gamma_mail/u$UserID";
						//$this->user_file_full_path = $file_path.$personal_path;
                    }
					else 
					{
						$this->ConnectionStatus = false;
						return;	
					}
                    $this->DefaultFolderList = $DefaultFolderList;
                    
                    
                    $this->InboxFolder = $DefaultFolderList['inbox'];
                    $this->SentFolder = $DefaultFolderList['SentFolder'];
                    $this->DraftFolder = $DefaultFolderList['DraftFolder'];
                    $this->SpamFolder = $DefaultFolderList['SpamFolder'];
                    $this->TrashFolder = $DefaultFolderList['TrashFolder'];
                    $this->reportSpamFolder = $DefaultFolderList['reportSpamFolder'];
                    $this->reportNonSpamFolder = $DefaultFolderList['reportNonSpamFolder'];
                    $this->HiddenFolder = $DefaultFolderList['HiddenFolder'];
                    if($sys_custom['iMailPlus']['EDSReportSpam']){
                    	$this->EDSReportSpamFolder = $this->RootPrefix.'EDS_ReportSpam';
                    }
                    
                        // create new preference record if record doesn't exists
                        //intranet_opendb();
                      //  $this->New_Preference($this->CurUserAddress);
               }
               else
               {
                   $this->inbox = null;
               }
               
      } // End of constructor
		
		function __destruct()
		{
			global $sys_custom;
			
			//apache_note('eclass_user', $this->CurUserAddress);
			
			unset($this->inbox);
			if($this->ImapCache){
				$this->ImapCache->Close_Connect();
			}
			$this->unsetImapCacheAgent();
			//if($this->inbox) $this->close();
			
			if($sys_custom['iMailPlus']['MonitorPerformance']){
				$elapsed_ms = $this->Stop_Timer();
				$time_limit = isset($sys_custom['iMailPlus']['MonitorPerformanceTimeLimit'])? $sys_custom['iMailPlus']['MonitorPerformanceTimeLimit']: 5;
				$memory_limit = isset($sys_custom['iMailPlus']['MonitorPerformanceMemoryLimit'])? $sys_custom['iMailPlus']['MonitorPerformanceMemoryLimit'] : 40;
				$error_msg = '';
				if($elapsed_ms > $time_limit * 1000){
					$error_msg .= $this->CurUserAddress.' '.$_SERVER['PHP_SELF'].' consumed time '.$elapsed_ms.' milliseconds.';
				}
				$consumed_bytes = memory_get_usage(true);
				if($consumed_bytes > $memory_limit * 1024 * 1024){
					$error_msg .= $this->CurUserAddress.' '.$_SERVER['PHP_SELF'].' consumed memory: '.$consumed_bytes." bytes.";
				}
				if($error_msg != ''){
					error_log($error_msg);
					//debug_r($error_msg);
				}
			}
		}
		
		##### hardcoded for ASTRI dev first [start] #####
		function getUserId() {
			return 2;
		}
		
		function getUserEmail() {
			return 'astridev2@astridev2.eclasscloud.hk';
		}
		
		function getUserPassword() {
			return 'vh5gGPsQ';
		}
		##### hardcoded for ASTRI dev first [end] #####
		
		function unsetImapCacheAgent()
		{
			if($this->ImapCache){
				unset($this->ImapCache);
				$this->ImapCache = false;
			}
		}
		
		function getImapCacheAgent()
		{
			if($this->IMapCache===false)
				$this->IMapCache = new imap_cache_agent($this->CurUserAddress, $this->CurUserPassword);
			
			return $this->IMapCache;
		}
		
		function getAllFolderList()
		{
			if(!isset($this->AllFolderList) || sizeof($this->AllFolderList)==0){
            	$this->AllFolderList = $this->getMailFolders();
			}
			return $this->AllFolderList;
		}
//      function open_account($UserLogin, $UserPassword, $quota="")
//      {
//               global $webmail_info;
//               return $this->openAccount($UserLogin,$UserPassword);       # Call parent method
//      }

		function open_account ($ParUserLogin, $NewPassword)
        {
                Global $SYS_CONFIG;

					
                $login = $ParUserLogin;
                if (!$this->checkSecret())
                {
                     return false;
                }
				
                $success = true;
                //$path = $this->api_url['RemoveAutoreply']."?login=".urlencode($login);
				$path = $this->api_url['OpenAccount'];
                $post_data = "loginID=".urlencode($login);
                //$post_data .= "&newPassword=".urlencode($NewPassword);
                $post_data .= "&pwd=".urlencode(base64_encode($this->encrypt($NewPassword)));
                $post_data .= "&module=iMail";

                if ($this->actype != "")
                {
                    $post_data .= "&actype=".$this->actype;
                }
				$post_data .= "&secret=".urlencode($this->secret);


                $response = $this->getWebPage($path."?".$post_data);

                //$response = $this->executePost($path, $post_data);
                $success = $success && ($response[0]=="1");
	             
	            if ($success)
                {
                    return true;
                }
                else return false;
        }
        
        /******************** admin console related functions start ***********************/
        function RetrieveGeneralSetting()
        {
        	$this->getDayInTrashSpam();
        	$this->getExternMailBanList();
        	$this->getSendMailBanList();
        	$this->getInternetMailAccess();
        	$this->getIndentityGroupAccessQuota();
        	$this->getInternalMailOnly();
        	$this->getBadWordList();
        }
        
        function getDayInTrashSpam()
        {
    		global $intranet_root;
    		$li = new libfilesystem();
			$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/days_in_trash_spam.txt";
		
			list($days_in_trash,$days_in_spam) = explode(",",trim(get_file_content($file)));
			
			$this->days_in_trash = $days_in_trash;
			$this->days_in_spam = $days_in_spam;
        }
        
        function setDayInTrashSpam($days_in_trash= '',$days_in_spam = '')
        {
    		global $intranet_root;
    		$li = new libfilesystem();
			$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/days_in_trash_spam.txt";
		
			$this->days_in_trash = trim($days_in_trash)!=''?$days_in_trash:$this->days_in_trash;
			$this->days_in_spam = trim($days_in_spam)!=''?$days_in_spam:$this->days_in_spam;
			
			write_file_content($this->days_in_trash.",".$this->days_in_spam,$file);
        }
        /*
        function getDayInTrashSpam()
        {
        	global $intranet_root;
    		$li = new libfilesystem();
			$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file_junk = $location."/changeJunkDay.txt";
			$file_trash = $location."/changeTrashDay.txt";
			
			list($empty_trash,$days_in_trash) = explode(",",trim(get_file_content($file_trash)));
			list($empty_junk,$days_in_junk) = explode(",",trim(get_file_content($file_junk)));
			
			$this->days_in_trash = $days_in_trash;
			$this->days_in_spam = $days_in_junk;
        }
        
        function setDayInTrashSpam($days_in_trash= '',$days_in_junk = '')
        {
    		global $intranet_root;
    		$li = new libfilesystem();
			$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file_junk = $location."/changeJunkDay.txt";
			$file_trash = $location."/changeTrashDay.txt";
		
			$this->days_in_trash = trim($days_in_trash)!=''?$days_in_trash:$this->days_in_trash;
			$this->days_in_spam = trim($days_in_junk)!=''?$days_in_junk:$this->days_in_spam;
			
			write_file_content("1,".$this->days_in_trash,$file_trash);
			write_file_content("2,".$this->days_in_spam,$file_junk);
        }
        */
        function getExternMailBanList()
        {
    		global $intranet_root;
    		
    		$this->ban_ext_mail_list = get_file_content("$intranet_root/file/gamma_mail_banlist.txt");
        }
        
        function setExternMailBanList($final_ban_list)
        {
    		global $intranet_root;
    		$li = new libfilesystem();
    		
    		$file_content = implode("\r\n",(array)$final_ban_list);
			$li->file_write($file_content, $intranet_root."/file/gamma_mail_banlist.txt");	
			
			$this->ban_ext_mail_list  = $file_content;
			
        }
        
        function getSendMailBanList()
        {
    		global $intranet_root;
    		
    		$this->ban_send_mail_list = get_file_content("$intranet_root/file/campusmail_banlist.txt");
        }
        
        function setSendMailBanList($final_ban_list)
        {
    		global $intranet_root;
    		$li = new libfilesystem();
    		
    		//$file_content = implode("\r\n",(array)$final_ban_list);
			$li->file_write($final_ban_list, $intranet_root."/file/campusmail_banlist.txt");	
			
			$this->ban_send_mail_list  = $final_ban_list;
			
        }
        
        function getInternetMailAccess()
        {
    		global $intranet_root;
    		$li = new libfilesystem();
    		
    		$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/internet_mail_access.txt";
			
			if(file_exists($file)) 
				$enable = get_file_content($file);
			else
				$enable = 1; // default true;

    		$this->internet_mail_access = $enable;
        }
        
        function setInternetMailAccess($enable)
        {
    		global $intranet_root;
    		$li = new libfilesystem();
    		
    		if($this->internet_mail_access != $enable)
    		{
	    		$location = $intranet_root."/file/gamma_mail";
				$li->folder_new($location);
				$file = $location."/internet_mail_access.txt";
				$file_content = $enable;
				$li->file_write($file_content, $file);
					
	    		$this->internet_mail_access = $enable;
    		}
        }
        
        function getIndentityGroupAccessQuota()
        {
    		global $intranet_root, $special_feature;
    		$li = new libfilesystem();
			$file_content = $li->file_read($intranet_root."/file/campusmail_set.txt");
			
			if ($file_content == "")
			{
			    $access_right = array(1,1,1);
			    $userquota = array(10,10,10);
			    if($special_feature['alumni']){
			    	$access_right[] = 1;
			    	$userquota[] = 10;
			    }
			}
			else
			{
			    unset($campusmail_set_line);
			    $campusmail_set_line = array();
			
			    $content = explode("\n", $file_content);
			    $campusmail_set_line[] = explode(":",$content[0]);
			    $campusmail_set_line[] = explode(":",$content[1]);
			    //$campusmail_set_line[] = split(":",$content[2]); //skip alumni
			    $campusmail_set_line[] = explode(":",$content[3]);
			    if($special_feature['alumni']) $campusmail_set_line[] = explode(":",$content[2]); // Alumni UserType is 4
			    $access_right = array($campusmail_set_line[0][0],$campusmail_set_line[1][0],$campusmail_set_line[2][0]);
			    if($special_feature['alumni']) $access_right[] = $campusmail_set_line[3][0];
			    for ($i=0; $i<sizeof($campusmail_set_line); $i++)
			    {
			         $userquota[] = ( $campusmail_set_line[$i][1]!=""? $campusmail_set_line[$i][1]:10);
			    }
			}
			$this->access_right = $access_right;
			$this->default_quota = $userquota;
        }
        
        function setIndentityGroupAccessQuota($access_right,$default_quota)
        {
    		global $intranet_root;
    		$li = new libfilesystem();
    		
			$file_content = "";
			$file_content .= $access_right[0].":".$default_quota[0]."\n"; // staff
			$file_content .= $access_right[1].":".$default_quota[1]."\n"; // student
			//$file_content .= "0:0\n"; // alumni
			$file_content .= $access_right[3].":".$default_quota[3]."\n"; // alumni
			$file_content .= $access_right[2].":".$default_quota[2]."\n"; // parent
			
			# echo $file_content;
			$li->file_write($file_content, $intranet_root."/file/campusmail_set.txt");
			
			$this->access_right = $access_right;
			$this->default_quota = $default_quota;
        }
        
        function getInternalMailOnly()
        {
        	global $intranet_root;
        	$li = new libfilesystem();
        	$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/internal_mail_only.txt";
			
			if(file_exists($file)){ 
				$file_content = get_file_content($file);
				$internal = explode(',',$file_content);
			}
			else
				$internal = array(0,0,0); // default false;
			
    		$this->internal_mail_only = $internal;
        }
        
        function setInternalMailOnly($internal_only)
        {
        	global $intranet_root;
        	$li = new libfilesystem();
        	
        	$file_content = $internal_only[0].",".$internal_only[1].",".$internal_only[2];
        	$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/internal_mail_only.txt";
			$li->file_write($file_content, $file);
			
			$this->internal_mail_only = $internal_only;
        }
        
        function getQuotaAlertSetting()
        {
        	global $intranet_root;
        	$li = new libfilesystem();
        	$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/quota_alert.txt";
			
			if(file_exists($file)){ 
				$quota_alert = trim(get_file_content($file));
				if($quota_alert == ""){
					$quota_alert = "0.00";
				}
			}else{
				$quota_alert = "0.00";
			}
    		$this->quota_alert = $quota_alert;
    		return $this->quota_alert;
        }
        
        function setQuotaAlertSetting($quota_alert)
        {
        	global $intranet_root;
        	$li = new libfilesystem();
        	
        	if($quota_alert == ""){
        		$quota_alert = "0";
        	}
        	$quota_alert = sprintf("%.2f",$quota_alert);
        	$file_content = $quota_alert;
        	$location = $intranet_root."/file/gamma_mail";
			$li->folder_new($location);
			$file = $location."/quota_alert.txt";
			$success = $li->file_write($file_content, $file);
			
			$this->quota_alert = $quota_alert;
			return $success;
        }
        
        function getBadWordList()
        {
    		global $intranet_root;

			$base_dir = "$intranet_root/file/templates/";
			if (!is_dir($base_dir))
			{
			     $lf->folder_new($base_dir);
			}
			
			$target_file = "$base_dir"."mail_badwords.txt";
			
			$this->bad_word_list = get_file_content($target_file);        	
        }
        
        function setBadWordList($data)
        {
    		global $intranet_root;

			$lf = new libfilesystem();
			$base_dir = "$intranet_root/file/templates/";
			if (!is_dir($base_dir))
			{
			     $lf->folder_new($base_dir);
			}
			
			$target_file = "$base_dir"."mail_badwords.txt";
			write_file_content($data,$target_file);
			
			$this->bad_word_list = $data;        	
        }
        /******************** admin console related functions end ***********************/
        
        
        function SetTotalQuota ($ParUserLogin, $Quota, $ParUserID = '', $isAdd = 0)
        {
                Global $SYS_CONFIG;
				$Quota = max(0, intval($Quota));
				if($ParUserID!='')
				{
					$ExistQuota = $this->getUserImailGammaQuota($ParUserID);
					if($isAdd==1) 
					{
						$Quota = $ExistQuota+$Quota;
						$isUpdated = true;
					}
					else
					{
						$isUpdated = $ExistQuota!=$Quota;
					}
				}
				else
					$isUpdated = true;
				
				if($isUpdated)
				{	
	                $login = $ParUserLogin;
	                if (!$this->checkSecret())
	                {
	                     return false;
	                }
					
	                $success = true;
	
					$path = $this->mail_api_url['SetTotalQuota'];
	                $post_data = "loginID=".urlencode($login);
	                $post_data .= "&quota=$Quota";
					$post_data .= "&module=iMail";
	                if ($this->actype != "")
	                {
	                    $post_data .= "&actype=".$this->actype;
	                }
					$post_data .= "&secret=".urlencode($this->secret);
	
	                $response = $this->getWebPage($path."?".$post_data);
	                
	                //$success = $success && ($response=="Quota reset successfully");
		            if ($success)
	                {
	                	if($ParUserID!='')
	                	{
	                		$this->setIMapUserEmailQuota($ParUserID,$Quota);
	                	}
	                    return true;
	                }
	                else return false;
				}
	            else
	            	return true;
	             	
        }
        
	    function delete_account ($UserLogin)
	    {
	    	return $this->removeAccount($UserLogin,"iMail");            # Call parent method
	    }

//      function change_password ($UserLogin, $NewPassword)
//      {
//      	
//               return $this->changePassword($UserLogin,$NewPassword,"imail_gamma");  # Call parent method
//      }
		function change_password ($ParUserLogin='', $NewPassword)
        {
                Global $SYS_CONFIG;

					
                $login = !empty($ParUserLogin)?$ParUserLogin:$this->CurUserAddress;
                if (!$this->checkSecret())
                {
                     return false;
                }
				
                $success = true;
                //$path = $this->api_url['RemoveAutoreply']."?login=".urlencode($login);
				$path = $this->api_url['ChangePassword'];
                $post_data = "loginID=".urlencode($login);
                //$post_data .= "&newPassword=".urlencode($NewPassword);
                $post_data .= "&pwd=".urlencode(base64_encode($this->encrypt($NewPassword)));
                $post_data .= "&module=iMail";

                if ($this->actype != "")
                {
                    $post_data .= "&actype=".$this->actype;
                }
				$post_data .= "&secret=".urlencode($this->secret);

                $response = $this->getWebPage($path."?".$post_data);
                
                //$response = $this->executePost($path, $post_data);
                $success = $success && ($response[0]=="1");
	                
	                
                if ($success)
                {
//                    $db = new database();
//                        $sp = "exec Update_MailPreference_AutoReply @MailBoxName='" . $this->CurUserAddress . "', ";
//                        $sp .= "@AutoReplyMessage='" . $db->Get_Safe_Sql_Query($ReplyMessage) . "', ";
//                        $sp .= "@EnableAutoReply=false";
//                        $db->db_db_query($sp);
//
                    return true;
                }
                else return false;
        }

//      function is_user_exist($UserLogin)
//      {
//               return $this->isAccountExist($UserLogin);  # Call parent method
//      }

		function is_user_exist ($ParUserLogin='')
        {
                Global $SYS_CONFIG;

					
                $login = $ParUserLogin;
                if (!$this->checkSecret())
                {
                     return false;
                }
				
                $success = true;
                //$path = $this->api_url['RemoveAutoreply']."?login=".urlencode($login);
				$path = $this->mail_api_url['CheckAccountExist'];
                $post_data = "loginID=".urlencode($login);
                $post_data .= "&module=iMail";

                if ($this->actype != "")
                {
                    $post_data .= "&actype=".$this->actype;
                }
				$post_data .= "&secret=".urlencode($this->secret);

                $response = $this->getWebPage($path."?".$post_data);

                //$response = $this->executePost($path, $post_data);
                $success = $success && ($response[0]=="1");
	                  
                if ($success)
                {
                    return true;
                }
                else return false;
        }
	  
	  # login : email address
	  # quota : set to be (MB)
	  function set_total_quota($login, $usertype, $quota='')
	  {
			   global $webmail_info, $SYS_CONFIG;
			   $isvalid = false;
			   switch ($usertype){
					case 'T':	
						$quota = $SYS_CONFIG['mail']['MaxQuotaOfStaffMailBox']; 
						$isvalid = true;
						break;
					case 'S':
						$quota = '';
						$isvalid = true;
						break;
					/* Other usertype could be added in below */
					default:	
						$quota = ''; 
						break;
			   }
			   if(!$isvalid){
					return false;
			   } else {
					return ($quota != '') ? $this->setTotalQuota($login, $quota) : true;       # Call parent method
			   }
	  }

      # Functions for Type 3 Only
      # ------------------------------------

      function encodeFolderName($folderName)
      {
              return $this->Conver_Encoding($folderName, "UTF7-IMAP", "UTF-8");
      }

      function decodeFolderName($folderName)
      {
              return $this->Conver_Encoding($folderName, "UTF-8", "UTF7-IMAP");
              //big5 way of retrieving the folder name
      }

      # Connect to mail server
      # Store the connection resource variable in $this->connection
      function openInbox($UserLogin, $UserPassword,$tarGetfolder='', $isHalfOpen=false)
      {
               global $webmail_info;
               global $intranet_inbox_connect_failed;
               global $SYS_CONFIG;
               global $targetFolder;
             
               $UserLogin = strtolower($UserLogin);
               //if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && $SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && $this->CurUserAddress != "tgcestest1@esfcentre.edu.hk")
               if($SYS_CONFIG['Mail']['UsernameLoginWithoutDomain'] === true)
               {
               		if(stristr($UserLogin,"@"))
						$login_string = substr($UserLogin, 0, strpos($UserLogin, "@"));
					else
						$login_string = $UserLogin;
               }
               else
               {
                    //$login_string = $this->attachDomain($UserLogin);
                    $login_string =  $UserLogin;
               }
               
               $mailbox = $this->getMailServerRef().($targetFolder!=''?$this->encodeFolderName($targetFolder):"");
			   
			   //echo $mailbox.'<br>';
               if ($isHalfOpen)
               {
                   $inbox = @imap_open($mailbox,$login_string,$UserPassword,OP_HALFOPEN);
               }
               else
               {
                   $inbox = @imap_open($mailbox,$login_string,$UserPassword);
               }
			
               if (!$inbox)
               {
                    $intranet_inbox_connect_failed = true;
                    return false;
               }
               $this->inbox = $inbox;
               return $this->inbox;
      }

      # Close the stream
      function close()
      {
               if ($this->inbox){
                   @imap_close($this->inbox,CL_EXPUNGE);
                   unset($this->inbox);
               }
      }

      # Remove Mail in msgno
      function removeMail($msgno)
      {
               if ($this->inbox)
               {
                   return imap_delete($this->inbox,$msgno);
               }
               else return false;
      }

      # return number of mails in the inbox
      function Check_Mail_Count($Folder)
      {
              imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($Folder));
                                return imap_num_msg($this->inbox);
      }

      /* Old IP 2.0 functions */

      # Return mail information if the inbox is not empty
      # Attachment needs to copy to path if any
      # Remove the mail immediately
      #
      # Param:
      # $quota - Try to get most mails if quota is enough. (You can refer space with mails or attachment only, just depends which is easy to get)
      #          If no limit , $quota == "UNLIMIT";
      #
      # Return : false if quota not enough
      #          "empty" if the inbox is empty
      #          array of (subject,message,sender address, receiver addresses(To),receiver addresses (cc),receiver addresses (bcc)  , attachment path, date of receive)
      #
      # Attachment path can be get by $this->getNewAttachmentPath()
      # You can write the files using function write_file_content($content,$filepath)
      # Not used as use checkmailOne
      function checkMail($quota)
      {
               $this->embed_part_ids = array();
               $lf = new libfilesystem();
               global $file_path;
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;

               reset($overview);
               while (list($key,$val) = each($overview))
               {
                      $this->embed_part_ids = array();
                      $msg_no = imap_msgno($this->inbox, $val->uid);      # Messge Sequence Number

                      # Grab Structure of the mail
                      $structure = imap_fetchstructure($this->inbox, $val->uid, FT_UID);
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $val->uid, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             $i++;
                      }

                      # Subject
                      $subject_decoded = imap_mime_header_decode($val->subject);
                      $subject = trim($subject_decoded[0]->text);
                      if ($subject == "")
                      {
                          $subject = "[Unknown]";
                      }

                      # Size (in Kbytes)
                      $size = number_format ($val->size / 1024, 1);
                      # Check Quota enough

                      $used += $size;
                      if ($quota != 0 && $quota < $used)
                      {
                          return $mail;
                      }

                      # Grab information
                      $headerinfo = imap_headerinfo($this->inbox, $msg_no);
                      $temp = imap_mime_header_decode($headerinfo->fromaddress);
                      $fromaddress = "";
                      $delim = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $fromaddress .= $delim.$temp[$i]->text;
                           $delim = " ";
                      }
                      $temp = imap_mime_header_decode($headerinfo->toaddress);
                      $delim = "";
                      $toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      $temp = imap_mime_header_decode($headerinfo->reply_toaddress);
                      $delim = "";
                      $reply_toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $reply_toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      if (isset($headerinfo->ccaddress))
                      {
                          $temp = imap_mime_header_decode($headerinfo->ccaddress);
                          $delim = "";
                          $ccaddress = "";
                          for ($i=0; $i<sizeof($temp); $i++)
                          {
                               $ccaddress .= $delim.$temp[$i]->text;
                               $delim = ",";
                          }
                      }
                      //$dateReceive = date('Y-m-d H:i:s',strtotime(str_ireplace("UT","UTC",$val->date)));
					  $dateReceive = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($val->date)));
					  
                      # Attachment Path
                      $attachment_path = $this->getNewAttachmentPath();
                      $actual_file_path = $file_path."/file/mail/".$attachment_path;

                      # Message Content
                      $message = $this->parseMessageBody($this->inbox, $val->uid, $structure,'',$actual_file_path);
                      $filtered_message = $this->removeHTMLHeader($message);
                      $isHTML = ($filtered_message != $message);

                      # Get Attachment Size
                      if (is_dir($actual_file_path))
                      {
                          $temp = $lf->folder_size($actual_file_path);
                          $attachment_size = $temp[0]/1024;
                      }
                      else $attachment_size = 0;
                      $mail[] = array($val->uid,$subject,$filtered_message,$isHTML,$attachment_size,$fromaddress,$toaddress,$ccaddress,$dateReceive,$attachment_path);

               }
               return $mail;
      }
      # Return ONE and ONLY ONE mail information if the inbox is not empty
      # Same as checkMail()
      function checkOneMail($quota)
      {
               $this->embed_part_ids = array();
               $lf = new libfilesystem();
               global $file_path;
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return false;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;

               reset($overview);
               while (list($key,$val) = each($overview))
               {
                      $msg_no = imap_msgno($this->inbox, $val->uid);      # Messge Sequence Number

                      # Grab Structure of the mail
                      $structure = imap_fetchstructure($this->inbox, $msg_no, FT_UID);
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $msg_no, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             /*
                             $j = 1;
                             while( imap_fetchbody( $this->inbox, $msg_no, "$i.$j" ) ){
                                    $structure->parts[$i-1]->parts[$j-1] = imap_bodystruct( $this->inbox, $msg_no, "$i.$j" );
                                    $j++;
                             }
                             */


                             $i++;
                      }
                      /*
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $val->uid, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             $i++;
                      }
*/
                      # Subject
                      $subject_decoded = imap_mime_header_decode($val->subject);
                      $subject = trim($subject_decoded[0]->text);
                      if ($subject == "")
                      {
                          $subject = "[Unknown]";
                      }

                      # Size (in Kbytes)
                      $size = number_format ($val->size / 1024, 1);
                      # Check Quota enough

                      $used += $size;
                      if ($quota != 0 && $quota < $used)
                      {
                          return $mail;
                      }

                      # Grab information
                      $headerinfo = imap_headerinfo($this->inbox, $msg_no);
                      $temp = imap_mime_header_decode($headerinfo->fromaddress);
                      $fromaddress = "";
                      $delim = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $fromaddress .= $delim.$temp[$i]->text;
                           $delim = " ";
                      }
                      $temp = imap_mime_header_decode($headerinfo->toaddress);
                      $delim = "";
                      $toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      $temp = imap_mime_header_decode($headerinfo->reply_toaddress);
                      $delim = "";
                      $reply_toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $reply_toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      if (isset($headerinfo->ccaddress))
                      {
                          $temp = imap_mime_header_decode($headerinfo->ccaddress);
                          $delim = "";
                          $ccaddress = "";
                          for ($i=0; $i<sizeof($temp); $i++)
                          {
                               $ccaddress .= $delim.$temp[$i]->text;
                               $delim = ",";
                          }
                      }
                      //$dateReceive = date('Y-m-d H:i:s',strtotime(str_ireplace("UT","UTC",$val->date)));
					  $dateReceive = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($val->date)));
					  
                      # Attachment Path
                      $attachment_path = $this->getNewAttachmentPath();
                      $actual_file_path = $file_path."/file/mail/".$attachment_path;

                      # Message Content
                      $message = $this->parseMessageBody($this->inbox, $val->uid, $structure,'',$actual_file_path);
                      $filtered_message = $this->removeHTMLHeader($message);
                      $isHTML = ($filtered_message != $message);

                      # Get Attachment Size
                      if (is_dir($actual_file_path))
                      {
                          $temp = $lf->folder_size($actual_file_path);
                          $attachment_size = $temp[0]/1024;
                      }
                      else $attachment_size = 0;
                      $mail = array($val->uid,$subject,$filtered_message,$isHTML,$attachment_size,$fromaddress,$toaddress,$ccaddress,$dateReceive,$attachment_path);
                      return $mail;

               }
               return false;
      }

      # Send external mails
      # Mail Subject: $subject
      # Mail Content: $message
      # To: $receiver_to (array)
      # CC: $receiver_cc (array)
      # Bcc: $receiver_bcc (array)
      # Attachment: All files under $attachment_path (no sub-directory)
      # Return true if send is successful
      function sendMail_old($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="")
      {
               # Content-type of attachment
               $type="application/octet-stream";

               $priority = ($IsImportant==1? 1: 3);

               # receiving email addresses
               $numTo = sizeof($receiver_to);
               $numCC = sizeof($receiver_cc);
               $numBCC = sizeof($receiver_bcc);
               if ($numTo + $numCC + $numBCC == 0) return false;

               $to = ($numTo != 0)? implode(", ",$receiver_to) : "";
               $cc = ($numCC != 0)? implode(", ",$receiver_cc) : "";
               $bcc = ($numBCC != 0)? implode(", ",$receiver_bcc) : "";

               # Mail Header
               $headers='';
               $files = $this->listfiles($attachment_path);
               $isMulti = (sizeof($files)!=0);

               $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
               if ($isMulti)
               {
                   $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
                   $headers .= "X-Priority: $priority".$this->MimeLineBreak;
                   $headers .= "Content-Type: multipart/mixed; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
               }
               $headers .= "From: $from".$this->MimeLineBreak;
               if ($cc != "")
                   $headers .= "cc: $cc".$this->MimeLineBreak; // CC to
               if ($bcc != "")
                   $headers .= "bcc: $bcc".$this->MimeLineBreak; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com

               if ($isMulti)
               {
                   $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
                   $mime .= "".$this->MimeLineBreak;
                   $mime .= "--" . $mime_boundary . "".$this->MimeLineBreak;

                   # Message Body
                   $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
                   $mime .= "Content-Type: text/plain; charset=\"iso-8859-1\"".$this->MimeLineBreak;
                   $mime .= "".$this->MimeLineBreak;
               }
               $mime .= $message . $this->MimeLineBreak.$this->MimeLineBreak;
               if ($isMulti)
               {
                   $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
               }

               # Embed attachment files
               for ($i=0; $i<sizeof($files); $i++)
               {
                    $target = $files[$i];
                    $data = Get_File_Content("$attachment_path/$target");
                    $fname = addslashes($target);

                    $mime .= "Content-Type: $type;".$this->MimeLineBreak;
                    $mime .= "\tname=\"$fname\"".$this->MimeLineBreak;
                    $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                    $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
                    $mime .= "\tfilename=\"$fname\"".$this->MimeLineBreak.$this->MimeLineBreak;
                    $mime .= base64_encode($data);
                    $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                    $mime .= "--" . $mime_boundary;

                    # Different if end of email
                    if ($i==sizeof($files)-1)
                        $mime .= "--".$this->MimeLineBreak;
                    else
                        $mime .= $this->MimeLineBreak;
               }
               $result = mail($to, $subject, $mime, $headers);
               return $result;
      }

      # New Method
      # Send external mails
      # Mail Subject: $subject
      # Mail Content: $message
      # To: $receiver_to (array)
      # CC: $receiver_cc (array)
      # Bcc: $receiver_bcc (array)
      # Attachment: All files under $attachment_path (no sub-directory)
      # Return true if send is successful
      function sendMail($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="",$mail_return_path="",$reply_address="")
      {
                      global $intranet_session_language,$intranet_default_lang_set;

               # Content-type of attachment
               $type="application/octet-stream";

               $priority = ($IsImportant==1? 1: 3);

               # receiving email addresses
               $numTo = sizeof($receiver_to);
               $numCC = sizeof($receiver_cc);
               $numBCC = sizeof($receiver_bcc);
               if ($numTo + $numCC + $numBCC == 0) return false;

               $to = ($numTo != 0)? implode(", ",$receiver_to) : "";
               $cc = ($numCC != 0)? implode(", ",$receiver_cc) : "";
               $bcc = ($numBCC != 0)? implode(", ",$receiver_bcc) : "";

               # Mail Header
               $headers='';
               $files = $this->listfiles($attachment_path);
               global $intranet_session_language;
               $charset = (($intranet_session_language=="gb")?"gb2312":"big5");

               #$charset = (isBig5($message)?"big5":"iso-8859-1");
               #$isHTMLMessage = $this->isHTMLContent($message);
               global $special_feature;
               $isHTMLMessage = ($special_feature['imail_richtext']);

               $isMulti = (sizeof($files)!=0);

               $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
               $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part
               if ($isMulti)
               {
                   $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
                   $headers .= "X-Priority: $priority".$this->MimeLineBreak;
                   $headers .= "Content-Type: multipart/mixed; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
               }
               else if ($isHTMLMessage)         # HTML message w/o attachments
               {
                   $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
                   $headers .= "X-Priority: $priority".$this->MimeLineBreak;
                   $headers .= "Content-Type: multipart/alternative; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
               }

               $headers .= "From: $from".$this->MimeLineBreak;
               if ($cc != "")
                   $headers .= "cc: $cc".$this->MimeLineBreak; // CC to
               if ($bcc != "")
                   $headers .= "bcc: $bcc".$this->MimeLineBreak; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com
               if ($reply_address != "")
               {
                   $headers .= "Reply-To: $reply_address".$this->MimeLineBreak;
               }

               if ($isMulti || $isHTMLMessage)
               {
                   $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
                   $mime .= $this->MimeLineBreak;
                   $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
               }
               if (!$isMulti)  # No attachments
               {
                    if ($isHTMLMessage)
                    {
                        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
                        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                        $mime .= $this->MimeLineBreak;
                        $text_message = Remove_HTML_Tags($message);
                        #$encoded_text_message = QuotedPrintableEncode($message);
                        $encoded_text_message = chunk_split(base64_encode($text_message));
                        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
                        # HTML part
                        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
                        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                        $mime .= $this->MimeLineBreak;
                        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
                        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
                        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
                        $mime .= $this->MimeLineBreak;
                    }
                    else
                    {
                        $mime .= $message;
                    }
               }
               else
               {
                   if ($isHTMLMessage)
                   {
                       $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
                       $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
                       #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                       $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;
                       #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= chunk_split(base64_encode(Remove_HTML_Tags($message))). $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
                       # HTML part
                       $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
                       #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
                       $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;
                       #$mime .= QuotedPrintableEncode($message);
                       $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
                       $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary.$this->MimeLineBreak;

                   }
                   else
                   {
                       # Message Body
                       $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
                       $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
                       $mime .= $this->MimeLineBreak;

                       $mime .= $message;
                       $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                       $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
                   }
                                $filename_encoding = "=?".strtoupper($charset)."?B?";

                   # Embed attachment files
                   for ($i=0; $i<sizeof($files); $i++)
                   {
                        $target = $files[$i];
                        $data = Get_File_Content("$attachment_path/$target");
                        $fname = addslashes($target);

                        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
                        //$mime .= "\tname=\"$fname\"".$this->MimeLineBreak;
                        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
                        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
                        //$mime .= "\tfilename=\"$fname\"\r\n\r\n";
                        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= chunk_split(base64_encode($data));
                        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
                        $mime .= "--" . $mime_boundary;

                        # Different if end of email
                        if ($i==sizeof($files)-1)
                            $mime .= "--".$this->MimeLineBreak;
                        else
                            $mime .= $this->MimeLineBreak;
                   }

               }

               #global $intranet_root;
               #write_file_content($mime, "$intranet_root/file/outmail.txt");
               if ($mail_return_path!="")
               {
                   $result = mail($to, $subject, $mime, $headers, "-f $mail_return_path");
               }
               else
               {
                   $result = mail($to, $subject, $mime, $headers);
               }
               return $result;
      }

      # ---------------------------------------
      # Supporting functions

      # get a random path
      function getNewAttachmentPath()
      {
               $path = session_id().".".substr( md5(uniqid(rand(),1)),10 );
               global $UserID;
               if ($UserID == "")
               {
                   return $path;
               }
               else
               {
                   global $intranet_root;
                   if (!is_dir("$intranet_root/file/mail/u$UserID"))
                   {
                        mkdir("$intranet_root/file/mail/u$UserID",0777);
                   }
                   $path = "u$UserID/$path";
                   return $path;
               }
      }

      # Return all filenames under directory $path (sub-directories will be ignored)
      # For Debian version, is_dir will not return correct value if the directory is just removed
      # therefore, need to add @ to suppress error message and return null array if error occurs
      function listfiles($path)
      {
               clearstatcache();
               if (!is_dir($path)) return array();
               $d = @dir($path);
               if (!$d) return array();
               while($entry = $d->read())
               {
                     $filepath = $path."/".$entry;
                     if (is_file($filepath))
                     {
                         $files[] = $entry;
                     }
               }
               $d->close();
               return $files;
      }
      # Modify significant HTML tag that will affect the whole document outlook
      function removeHTMLHeader($str)
      {
               $pos = strpos($str,"<body");
               if ($pos===false)
               {
                   $pos = strpos($str,"<BODY");
               }
               if ($pos!==false)
               {
                   $str = substr($str,$pos);
               }
               $epos = strpos($str,"</body>");
               if ($epos===false)
               {
                   $epos = strpos($str,"</BODY>");
               }
               if ($epos!==false)
               {
                   $str = substr($str,0,$epos+7);
               }

               $str = str_replace("<BODY","<xbody",$str);
               $str = str_replace("<body","<xbody",$str);
               $str = str_replace("</BODY>","</xbody>",$str);
               $str = str_replace("</body>","</xbody>",$str);
               $str = str_replace("<HTML","<xhtml",$str);
               $str = str_replace("<html","<xhtml",$str);
               $str = str_replace("</HTML>","</xhtml>",$str);
               $str = str_replace("</html>","</xhtml>",$str);
               $str = str_replace("<META","<xmeta",$str);
               $str = str_replace("<meta","<xmeta",$str);

               return $str;

      }

      # Convert Mail message
      function convertMessage($str)
      {
               $coded = str_replace("=\r\n","",$str);
               if ($coded == $str)
               {
                   return $str;
               }
               else
               {
                  $result = quoted_printable_decode($coded);
                  return $result;
               }
      }

      // This function recursively analyze and output the contents of the mail message.
      function parseMessageBody($imap_stream, $msg_number, $structure, $part_number, $attachment_path)
      {
               #echo "part: $part_number";
               #print_r($structure);
               // Split the $part_number. For example, 1.2.3 will be split into 1.2.(major) and 3(minor)
               if (strlen($part_number) == 0)
               {
                   $major_part_number = '';
                   $minor_part_number = 1;
               }
               elseif (strlen($part_number) == 1)
               {
                   $major_part_number = "$part_number.";        // be aware of the 'dot' at the end.
                   $minor_part_number = 0;
               }
               else
               {
                   $major_part_number = substr($part_number, 0, strlen($part_number)-1);
                   $minor_part_number = intval(substr($part_number, -1));
               }

               // Determine whether it is a leaf node. Perform recursion if not a leaf node.
               // NOTE: For "subtype = alternative", it is most likely generated by email clients
               // such as outlook express for sending rich text message and plain text message
               // at the same time. So only in this case no recursion is nessary.
               // NOTE: For "type = message",
               $parts = isset($structure->parts)?$structure->parts:'';

               if (is_array($parts) && ($structure->ifsubtype) && strtolower($structure->subtype) != 'alternative')
               {
                   foreach ($parts as $part)
                   {
                            $body = $this->parseMessageBody($imap_stream, $msg_number, $part, "$major_part_number".$minor_part_number,$attachment_path);
                            $minor_part_number++;

                            if ($part->encoding==0 && (strtolower($part->disposition)=="attachment" || $part->ifdisposition==1))
                            {
                                $part->encoding = 4;
                            }

                            $body = $this->decode_part($body, $part->encoding);
                            $message_body .= $this->manage_part($body,$part,$attachment_path);
                            if ($part->id != "")
                            {
                                $temp = imap_mime_header_decode($part->parameters[0]->value);
                                $t_filename = $temp[0]->text;
                                $temp_str = substr($part->id,1,strlen($part->id)-2);
                                $this->embed_part_ids[] = array($temp_str,$t_filename);
                            }
                   }
                   return $message_body;
               }


               // See whether it is an 'alternative' message. Output the content and return if so.
               #
               if (($structure->ifsubtype) && strtolower($structure->subtype) == 'alternative' && is_array($structure->parts))
               {
                    // Load user preferences
                    $message_format = $this->message_format;
                    if ($message_format != 'html') $message_format = 'plain';

                    // Read the message according to the preferences of the user
                    foreach ($structure->parts as $part)
                    {
                             if (strtolower($part->subtype) == $message_format)
                             {
                                 $body = $this->readMessageBody($imap_stream, $msg_number, $major_part_number.$minor_part_number);
                                 if ($part->encoding==0 && (strtolower($part->disposition)=="attachment" || $part->ifdisposition==1))
                                 {
                                     $part->encoding = 4;
                                 }
                                 $body = $this->decode_part($body, $part->encoding, $part->subtype);
                                 $message_body .= $this->manage_part($body,$part,$attachment_path);
                             }
                             $minor_part_number++;
                    }

                    return $message_body;
               }
               else if (($structure->ifsubtype) && (strtolower($structure->subtype) == 'alternative'))
               {
                    // Load user preferences
                    $message_format = $this->message_format;
                    if ($message_format != 'html') $message_format = 'plain';
                    if (true || strtolower($structure->subtype) == $message_format)
                    {
                        #$body = $this->readMessageBody($imap_stream, $msg_number,$structure, $major_part_number.$minor_part_number, $attachment_path);
                        $body = $this->readMessageBody($imap_stream, $msg_number, $part_number);
                        if ($structure->encoding==0 && (strtolower($structure->disposition)=="attachment" || $structure->ifdisposition==1))
                        {
                            $structure->encoding = 4;
                        }
                        $body = $this->decode_part($body, $structure->encoding,$structure->subtype);
                        $message_body .= $this->manage_part($body,$structure,$attachment_path);
                        $message_body = $this->handle_nested_message($message_body);
                    }
                    $minor_part_number++;
                    return $message_body;
               }
               else
               {
                   if ( strtolower($structure->subtype)=="related")
                   {
                   }
               }


               // Something other than those mentioned above at the leaf node.
               $body = $this->readMessageBody($imap_stream, $msg_number, $part_number);
               #$body = $this->handle_mixed_message($body);

               # Perform encoding
               $body = $this->decode_part($body, $structure->encoding, $structure->subtype);
               $message_body .= $this->manage_part($body,$structure,$attachment_path);
               return $message_body;
      }

      // This function opens the message. This function is a helper of parseMessageBody().
      function readMessageBody ($imap_stream, $msg_number, $part_number)
      {
               // If $part_number is empty, it assume the message does not have multiple parts
               // and just throw them to screen.
               echo "Read body $part_number<br>\n";
               if (empty($part_number) || $part_number === "1.0")
               {
                   $temp_msg  =imap_body($imap_stream, $msg_number, FT_UID);
                   echo $temp_msg;
                   return $temp_msg;
               }
               else
               {
                   $temp_msg = imap_fetchbody($imap_stream, $msg_number, $part_number, FT_UID);
                   echo $temp_msg;
                   return $temp_msg;
               }
      }

      # Decode message part
      function decode_part($body, $encoding, $subtype="")
      {
               if ($encoding=="")
               {
                   $lower_subtype = trim(strtolower($subtype));
                   if ($lower_subtype=='plain' || $lower_subtype=='html' || $lower_subtype=='')
                   {
                       return $body;
                   }
                   else
                   {
                       return quoted_printable_decode($body);
                   }
               }
               switch ($encoding)
               {
                       case 0:
                            #$body = $this->convertMessage($body);
                            break;
                       case 1:
                            break;
                       case 2:
                       case 3:
                            $body = base64_decode ($body); break;
                       case 4:
                            $body = quoted_printable_decode ($body); break;
                       default:
                            $body = quoted_printable_decode ($body); break;
               }

               return $body;
      }

      # Copy the content to file if this part is attachment
      function manage_part($body, $structure, $attachment_path)
      {
               if ($body == "") {
               return ""; }
               #print_r($structure);
               if (strtolower($structure->disposition)=="attachment" || $structure->ifdisposition==1 || $structure->ifid==1 || ($structure->type!=0 && $structure->type!=1 &&$structure->type!=2))
               {
                   $file_content = $body;

                   if (!is_dir($attachment_path))
                   {
                        mkdir($attachment_path,0777);
                   }
                   # Get Filename

                   if ($structure->ifdparameters==1)
                   {
                       $temp = imap_mime_header_decode($structure->dparameters[0]->value);
                   }
                   else if ($structure->ifparameters==1)
                   {
                       $temp = imap_mime_header_decode($structure->parameters[0]->value);
                   }
                   $filename = $temp[0]->text;
                   if ($filename == "")
                   {
                       $filename = "unknown";
                   }
                   $target_file = $attachment_path."/".$filename; #$structure->parameters[0]->value;
                   write_file_content($file_content,$target_file);
                   return "";                       # Empty String (not affect message body)
               }
               else
               {
                   return $body;
               }
      }
      function handle_nested_message($msg)
      {

               $is_quoted_printable = false;
               $msg = trim($msg);
               if (strpos($msg,"Content-Transfer-Encoding: quoted-printable\r\n")!==false)
               {
                   $is_quoted_printable = true;
               }
               else
               {
               }

               $firstnl = strpos($msg,"\r\n");
               if ($firstnl === false) return $msg;
               $bound_str = trim(substr($msg,0,$firstnl));

               if ($bound_str == "") return $msg;
               if (strlen($bound_str)<=70)    # RFC 1521 standard
               {
                   $left_str = substr($msg,$firstnl+2);
                   $alt_bpos = strpos($left_str,$bound_str);     # begin of alternative part boundary
                   $alt_bendpos = $alt_bpos + strlen($bound_str);  # end of alternative part boundary
                   #$temp = substr($left_str, $alt_bpos, strlen($bound_str));

                   if ($alt_bpos===false || ($left_str[$alt_bendpos]!="\r" && $left_str[$alt_bendpos]!="\n" ) )
                   {
                       return $msg;
                   }
                   else
                   {
                       $left_str2 = substr($left_str,$alt_bendpos);
                       $pos = strpos($left_str2,"\r\n\r\n");
                       if ($pos === false) return $msg;
                       $left_str3 = trim(substr($left_str2,$pos));
                       $end_boundary = substr($left_str3,0-strlen($bound_str)-2);
                       if ($end_boundary == "$bound_str--")
                       {
                           $left_str3 = trim(substr($left_str3,0,strlen($left_str3)-strlen($bound_str)-2));
                       }
                       if ($is_quoted_printable)
                       {
                           $pos_3d = strpos($left_str3,"=3D");
                           if ($pos_3d === false)
                           {
                           }
                           else
                           {
                               $left_str3 = quoted_printable_decode($left_str3);
                           }
                       }

                       return $left_str3;
                   }
               }
               else
               {
                   return $msg;
               }
      }

      # Supporting function
      function isHTMLContent($content)
      {
               $filtered_content = Remove_HTML_Tags($content);
               return ($filtered_content != $content);
      }


      ##########################################################################################
      # New Mail retrieving function
      # Param: Quota : quota in Mbytes, num : number of emails to be retrieved
      function checkMail2($quota, $num=0)
      {
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes
               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);
               if (!is_array($overview)) return;
               if ($num==0)
               {
                   $num = sizeof($overview);
               }
               unset($result_mail);
               for ($i=0; $i < sizeof($overview) && $i < $num; $i++)
               {
                    $mail_msgno = $overview[$i]->msgno;
                    /* Fix error of retriving multiple address in To: */
                    $text_header = imap_fetchheader($this->inbox, $mail_msgno);
                    $raw_header = imap_rfc822_parse_headers($text_header);
                    /* */

                    $header = imap_header($this->inbox, $mail_msgno);

                    $from = $header->fromaddress;
                    #$to = $header->toaddress;
                    # Special function
                    $to = $raw_header->toaddress;
                    $subject = $header->Subject;


                    $elements = imap_mime_header_decode($from);
                    unset($subtmp);
                    for($k=0;$k<sizeof($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $fromname = $subtmp; # From Address to return

                    $elements=imap_mime_header_decode($subject);
                    unset($subtmp);
                    for($k=0;$k<count($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $subject = $subtmp; # Subject to return

                    $elements=imap_mime_header_decode($to);
                    unset($subtmp);
                    for($k=0;$k<count($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $showto = $subtmp;       # To address to return
                    #var_dump($header->toaddress);
                    #var_dump($showto);

                    $elements=imap_mime_header_decode($header->ccaddress);
                    unset($subtmp);
                    for($k=0;$k<count($elements);$k++) {
                        $subtmp .= $elements[$k]->text;
                    }
                    $showcc = $subtmp;       # CC address to return

                    /*
                    $this_offset = $tzoffset * 3600;
                    $date = @Date("r", $header->udate+$this_offset);      # Date of email
                    */
                    //$dateReceive = date('Y-m-d H:i:s',strtotime(str_ireplace("UT","UTC",$header->date)));
					$dateReceive = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($header->date)));
					
                    $structure = imap_fetchstructure($this->inbox, $mail_msgno);
                    unset($this->parsed_part);
                    $this->parse_message($structure);
                    $attachments = $this->grab_attach($this->parsed_part);

                    # Handle attachments
                    unset($attach_parts);
                    unset($embed_cids);
                    $total_attach_size = 0;
                    for ($k=0; $k<sizeof($attachments); $k++)
                    {
                         $pid = $attachments[$k]["partnum"];
                         $attach_filename = $attachments[$k]["filename"];
                         $attach_type = $attachments[$k]["type"];
                         $attach_size = $attachments[$k]["size"];
                         $attach_encoding = $attachments[$k]["encoding"];
                         $attach_cid = $attachments[$k]["cid"];

                         $elements=imap_mime_header_decode($attach_filename);

                         unset($subtmp);
                         for($f_i=0;$f_i<count($elements);$f_i++) {
                                 $subtmp .= $elements[$f_i]->text;
                         }
                         $attach_filename = $subtmp;


                         $attach_content = $this->retrieve_attachment_content($this->inbox, $mail_msgno, $pid);
                         $decoded_content = $this->mime_decode_msg($attach_content,$this->mime_encoding[$attach_encoding]);

                         unset($attach_entry);
                         $attach_entry['filename'] = $attach_filename;
                         $attach_entry['type'] = $attach_type;
                         $attach_entry['size'] = $attach_size;
                         $attach_entry['cid'] = $attach_cid;
                         $attach_entry['content'] = $decoded_content;
                         $attach_parts[] = $attach_entry;
                         $embed_cids[] = array($attach_entry['cid'],$attach_entry['filename']);
                         $total_attach_size += $attach_size;
                    }

                    # Check Quota enough
                    //$size = number_format($total_attach_size/1024,1);
                    $size = round($total_attach_size/1024,1);
                    $used_quota += $size;
                    /*
                    if ($quota != 0 && $quota < $used_quota)
                    {
                        return $result_mail;
                    }
                                        */
                                        # updated on 2007-08-27
                    if ($quota < $used_quota)
                    {
                        return $result_mail;
                    }

                    # Retrieve Message Content
                    $message = $this->retrieve_message($this->inbox, $mail_msgno, $this->parsed_part);
                    # $this->message_charset, $this->is_html_message
                    $result_mail[] = array($mail_msgno, $subject,$message,$total_attach_size,
                                           $fromname, $showto, $showcc, $dateReceive, $attach_parts,
                                           $embed_cids, $this->message_charset, $this->is_html_message);

               }
               return $result_mail;

        }
        function retrieve_message($inbox, $uid, $partArr)
        {
			$this->is_html_message = -1;
			$viewpart = 0;
			$totalviewed = 0;
			$result_message = "";
			while ($partArr[$viewpart] != "")
			{
				$p = $partArr[$viewpart];

				#if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) || (strtolower($p["disposition"]) == "inline")) && strtolower($p["disposition"])!='attachment')
				if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) ) && strtolower($p["disposition"])!='attachment')
				{
					$htmltransform = false;
					$this->is_html_message = -1;
					$this->message_charset = $p["charset"];

					if ($p["type"] == 0 && strtolower($p["subtype"]) == "plain")
					{
					   $transform = true;
					}
					if (strtolower($p["subtype"]) == "html")
					{
					    $htmltransform = true;
					    $this->is_html_message = 1;
                    }

					/*Fix for Inaccurate Part number finding*/
					$tmp = $p["partnum"];
					while (strlen($new = str_replace("..",".",$tmp)) < strlen($tmp))
                        $tmp = $new;

					$myBod = imap_fetchbody($inbox, $uid, (string)($tmp));

					$myBod = $this->mime_decode_msg($myBod, $this->mime_encoding[$partArr[$viewpart]["encoding"]]);
					#echo "<br>================<br>$uid:<br>";
					#echo "encoding: ".$partArr[$viewpart]["encoding"];
					#print_r($partArr);
					#echo $myBod;


					if ($transform)
					{
						$htmltransform = true;
						if (!$htmltransform)
						{
							if ($fwd == false)
							{
								$myBod = "<code class='dispmsg'>".$myBod."</code>";
								$myBod = ereg_replace("\n","<br>",$myBod);
							}
						}
						$myBod = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i","$1http://$2",    $myBod);
						$myBod = preg_replace("/([\w]+:\/\/[\w-?%:&;#~=\.\/\@]+[\w\/])/i","<A TARGET=\"_blank\" HREF=\"$1\">$1</A>", $myBod);

						//Old Pattern Finder
						//$myBod = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3}))/i","<A    HREF=\"compose.php?to=$1\">$1</A>",$myBod);
						$myBod= eregi_replace('(<)?([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})(>)?','\\1<a href="compose.php?to=\\2">\\2</a>\\4', $myBod);
						$myBod = ereg_replace("\n","<br>",$myBod);
					}

					if ($htmltransform)
					{
						/*** GET RID OF EXCESS ITEMS IN HTML EMAILS ***/
						#$myBod = preg_replace ("/(<\/?)(\w+)([^>]*>)/e", "'\\1'.strtoupper('\\2').'\\3'", $myBod);
						$myBod = str_replace ("<HTML>", "", $myBod);
						$myBod = str_replace ("</HTML>", "", $myBod);
						$myBod = preg_replace ("'<HEAD[^>]*?>.*?</HEAD>'si", "", $myBod);
						$myBod = preg_replace ("'<SCRIPT[^>]*?>.*?</SCRIPT>'si", "", $myBod);
						$myBod = preg_replace ("'<STYLE[^>]*?>.*?</STYLE>'si", "", $myBod);
						$myBod = preg_replace("'<BODY([^>]*?>)'si", "<DIV$1", $myBod);
						$myBod = str_replace ("</BODY>", "</DIV>", $myBod);
						#$myBod = preg_replace ('/cid:(.*?)("|;|\s|\\\|\')/e', "'view_img.php?mailbox=$mailbox&id=$id&cid='.urlencode('\\1').'\\2'", $myBod);
						#echo "<!-- checkbody\n";
						#echo $myBod;
						#echo "--->";
						$myBod = preg_replace ('/background=(.*?)(\'|"|;|\s|\\\)/e', "'style=\"background-image: url('.'\\1'.');\"'", $myBod);
						$myBod= str_replace("mailto:", "compose.php?targetemail=",$myBod);
						#$myBod = stripslashes($myBod);
						$myBod = str_replace("\\\"","\"",$myBod);
						   
					}

					   /* View Attachment part
					   if ($totalviewed > 0 && $fwd == false)
					   {
							echo "<hr><br>\n";
							echo "<table width=100% cellpadding=3 cellspacing=0 border=0><tr bgcolor='#cccccc'><td>";
							//echo "<a href='view_img?mailbox=".$mailbox."&id=".$id."&pid=".$p["partnum"]."' target='_blank'><b>View Part: ".$tmp."</b></a></td>";
							echo "<b>Viewing Part: ".$tmp."</b></td>";
							echo "<td align=right><small>Content-Type: <b>".strtolower($type[$p["type"]]."/".$p["subtype"])."</b></small></td></tr></table>";

					   }
					if ($totalviewed > 0 && $fwd == 'true')
					{
							echo "\n";
							return;
					}
					echo $myBod;
					if ($fwd == 'true')
							echo "\n";
					else echo "<br>";
					*/
					$result_message .= $myBod;

					$transform = false;
					$totalviewed++;
				}

                $viewpart++;
            }
            return $result_message;
        }

      //to retrieve the plain text message from the mail, if any
      //can't find any reusable material at the moment...

      //encoding issues not handled yet
      function retrieve_textmessage($inbox, $mail_msgno, $structure)
      {
		  $returnStr = "";

		  if (strtolower($structure->subtype) == "plain") //text only email
		  {
				  $returnStr = imap_body($inbox, $mail_msgno);
		  }
		  else
		  {
					$parts = $structure->parts;

					for($i = 0; $i < count($parts); ++$i)
					{
							if (strtolower($parts[$i]->subtype) == "plain" || strtolower($parts[$i]->subtype) == "text") //anymore criteria to find the text message?
							{
									$returnStr = $this->mime_decode_msg(imap_fetchbody($inbox, $mail_msgno, ($i+1)),$this->mime_encoding[$parts[$i]->encoding]);
							}
					}
			}
		  
			return $returnStr;
      }

      function mime_decode_msg($msg, $encoding)
      {
               //Decode Msg Body if needed....
               if ($encoding == "base64")
                   $msg = base64_decode($msg);
               else if ($encoding == "quoted-printable")
                    $msg = quoted_printable_decode($msg);
               else if ($encoding == "8bit")
                    $msg = quoted_printable_decode(imap_8bit($msg));
               else { }

               return $msg;
      }
      function retrieve_attachment_content($inbox, $uid, $pid)
      {
               /*Fix for Inaccurate Part number finding*/
               while (strlen($new = str_replace("..",".",$pid)) < strlen($pid))
                      $pid = $new;
               $attachment = imap_fetchbody($inbox, $uid, $pid);
               return $attachment;
      }
      function parse_message($obj,$prefix="")
      {
               if (strtolower($obj->subtype) == "alternative")
               {
                   $objArr = $this->grab_alt($obj, $onlyText);
                   $obj = $objArr['part'];

                   if ($prefix =="")
                       $prefix = substr($objArr['pid'], 1);
                   else
                       $prefix = $prefix.$objArr['pid'];
               }
               else
               {
                   if (sizeof($obj->parts) > 0)
                       foreach ($obj->parts as $count=>$p)
                           $this->parse_part($p, $prefix.($count+1));
               }

               if ($prefix[strlen($prefix)-1] == ".")
                   $prefix = substr($prefix, 0, strlen($prefix)-1);
               if ($prefix[0] == ".")
                   $prefix = substr($prefix, 1);

               if (sizeof($obj->parts) == 0 && is_integer($obj->type))
                   $this->create_partArray($obj, $prefix);
      }
      function grab_alt($obj, $text=false)
      {
               for ($i = sizeof($obj->parts) - 1; $i > -1 ; $i--)
               {
                    if (strtolower($obj->parts[$i]->subtype) == "html" && $text == false)
                    {
                        $ret['pid'] = "." . ($i+1);
                        $ret['part'] = $obj->parts[$i];
                        return $ret;
                    }
                    if (strtolower($obj->parts[$i]->subtype) == "plain")
                    {
                        $ret['pid'] = "." . ($i+1);
                        $ret['part'] = $obj->parts[$i];
                        return $ret;
                    }
               }
      }
      function parse_part($obj, $partno)
      {
               if ($obj->subtype == "ALTERNATIVE")
               {
                   $objArr = $this->grab_alt($obj);
                   $obj = $objArr['part'];
                   $partno = $partno.$objArr['pid'];
               }
               else
               {
                   if ($obj->type == TYPEMESSAGE)
                       $this->parse_message($obj->parts[0], $partno.".");
                   else if (sizeof($obj->parts) > 0)
                        foreach ($obj->parts as $count=>$p)
                            $this->parse_part($p, $partno.".".($count+1));
               }
               if ($partno[strlen($partno)-1] == ".")
                   $partno = substr($partno, 0, strlen($prefix)-1);
               if ($partno[0] == ".")
                   $partno = substr($partno, 1);

               if (sizeof($obj->parts) == 0 && is_integer($obj->type))
                   $this->create_partArray($obj, $partno);


      }

      function create_partArray($obj, $partno)
      {

               $k = sizeof($this->parsed_part);
               if ($partno == "")
                   $partno = 1;

               $this->parsed_part[$k]["disposition"] = $obj->disposition;
               $this->parsed_part[$k]["partnum"] = $partno;
               $this->parsed_part[$k]["description"] = $obj->description;
               $this->parsed_part[$k]["type"] = $obj->type;
               $this->parsed_part[$k]["subtype"] = $obj->subtype;
               $this->parsed_part[$k]["encoding"] = $obj->encoding;
               $this->parsed_part[$k]["size"] = $obj->bytes;

               if ($obj->ifid == 1)
               {
                   $tmp = str_replace ("<", "", $obj->id);
                   $this->parsed_part[$k]["cid"] = str_replace (">", "", $tmp);
               }

               if ($obj->ifdparameters == 1)
               {
                   $params = $obj->dparameters;
                   foreach ($params as $p)
                   {
                        if(strtolower($p->attribute) == "filename")
                        {
                                $this->parsed_part[$k]["filename"] = $p->value;
                                break;
                        }
                   }
               }
               else if ($obj->ifparameters == 1 && $obj->ifdparameters == 0)
               {
                    $params = $obj->parameters;
                    foreach ($params as $p)
                    {
                        if(strtolower($p->attribute) == "name")
                        {
                                $this->parsed_part[$k]["filename"] = $p->value;
                        }
                        if(strtolower($p->attribute) == "charset")
                        {
                                $this->parsed_part[$k]["charset"] = $p->value;
                        }

                    }
               }
               else {}
      }

      function grab_attach($partArr)
      {
               $k = 0;
               for ($i=0; $i < sizeof($partArr); $i++)
               {
                    $p = $partArr[$i];
                    if (strtolower($p["disposition"]) == "attachment" || $p["type"] >= 3)
                    {
                        if ($p["filename"] != "")
                        {
                                $retArr[$k] = $p;
                                $k++;
                        }
                    }

               }

               return $retArr;

      }


        /*
        * Get MODULE_OBJ array
        */
//        function GET_MODULE_OBJ_ARR()
//        {
//
//                global $PATH_WRT_ROOT, $i_Discipline_System, $CurrentPage, $iDiscipline, $LAYOUT_SKIN, $i_CampusMail_New_iMail;
//                global $image_path;
//
//                global $i_CampusMail_New_CheckMail, $i_CampusMail_New_Inbox, $i_CampusMail_New_AddressBook;
//                global $i_CampusMail_Internal_Recipient_Group, $i_CampusMail_External_Recipient, $i_CampusMail_External_Recipient_Group;
//                global $i_CampusMail_New_Outbox, $i_CampusMail_New_Draft, $i_CampusMail_New_Trash;
//                global $i_CampusMail_New_Settings, $i_CampusMail_New_FolderManager, $i_CampusMail_New_ComposeMail;
//                global $i_CampusMail_New_Mail_Search, $i_CampusMail_New_Settings_PersonalSetting;
//                global $i_CampusMail_New_Settings_Signature;
//                global $unreadInboxNo, $folders, $FolderID, $UserID, $i_CampusMail_New_Settings_EmailForwarding;
//                global $special_option,  $iNewCampusMail;
//
//                $anc = $special_option['no_anchor'] ?"":"#anc";
//
//                //Check has webmail or not
//                $lwebmail = new libwebmail();
//                if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
//                {
//                    include_once("../../includes/libsystemaccess.php");
//                    $lsysaccess = new libsystemaccess($UserID);
//                    if ($lsysaccess->hasMailRight())
//                    {
//                        $noWebmail = false;
//                    }
//                    else
//                    {
//                        $noWebmail = true;
//                    }
//                }
//                else
//                {
//                    $noWebmail = true;
//                }
//
//                # Current Page Information init
//                $PageComposeMail = 0;
//
//                $PageCheckMail = 0;
//                $PageCheckMail_Inbox = 0;
//                $PageCheckMail_Outbox = 0;
//                $PageCheckMail_Draft = 0;
//                $PageCheckMail_Trash = 0;
//                $PageCheckMail_FolderManager = 0;
//
//                $PageAddressBook = 0;
//                $PageAddressBook_InternalReceipientGroup = 0;
//                $PageAddressBook_ExternalReceipientGroup = 0;
//                $PageAddressBook_ExternalReceipient = 0;
//
//                $PageSearch = 0;
//
//                $PageSettings = 0;
//                $PageSettings_PersonalSetting = 0;
//                $PageSettings_Signature = 0;
//
//                switch ($CurrentPage) {
//                        case "PageComposeMail":
//                                $PageComposeMail = 1;
//                                break;
//
//                        case "PageCheckMail_Inbox":
//                                $PageCheckMail = 1;
//                                $PageCheckMail_Inbox = 1;
//                                break;
//                        case "PageCheckMail_Outbox":
//                                $PageCheckMail = 1;
//                                $PageCheckMail_Outbox = 1;
//                                break;
//                        case "PageCheckMail_Draft":
//                                $PageCheckMail = 1;
//                                $PageCheckMail_Draft = 1;
//                                break;
//                        case "PageCheckMail_Trash":
//                                $PageCheckMail = 1;
//                                $PageCheckMail_Trash = 1;
//                                break;
//                        case "PageCheckMail_FolderManager":
//                                $PageCheckMail = 1;
//                                $PageCheckMail_FolderManager = 1;
//                                break;
//
//                        case "PageAddressBook_InternalReceipientGroup":
//                                $PageAddressBook = 1;
//                                $PageAddressBook_InternalReceipientGroup = 1;
//                                break;
//
//                        case "PageAddressBook_ExternalReceipientGroup":
//                                $PageAddressBook = 1;
//                                $PageAddressBook_ExternalReceipientGroup = 1;
//                                break;
//
//                        case "PageAddressBook_ExternalReceipient":
//                                $PageAddressBook = 1;
//                                $PageAddressBook_ExternalReceipient = 1;
//                                break;
//
//                        case "PageSearch":
//                                $PageSearch = 1;
//                                break;
//
//                        case "PageSettings_PersonalSetting":
//                                $PageSettings = 1;
//                                $PageSettings_PersonalSetting = 1;
//                                break;
//                        case "PageSettings_Signature":
//                                $PageSettings = 1;
//                                $PageSettings_Signature = 1;
//                                break;
//                        case "PageSettings_EmailForwarding":
//                                $PageSettings = 1;
//                                $PageSettings_EmailForwarding = 1;
//                                break;
//                }
//
//                /*
//                structure:
//                $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
//                $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
//                */
//
//                /*
//
//                */
//
//                ### No of new email (to be set)
//                $unreadInboxNo = $iNewCampusMail;
//                if ($unreadInboxNo != "")
//                {
//                        $NoOfNewMail = $unreadInboxNo;
//                } else {
//                        $NoOfNewMail = 0;
//                }
//                if ($NoOfNewMail > 0) $NewMailText = " (".$NoOfNewMail.")";
//
//                # Menu information
//                if (auth_sendmail())
//                                {
//                        $MenuArr["ComposeMail"] = array($i_CampusMail_New_ComposeMail, $PATH_WRT_ROOT."home/imail/compose.php", $PageComposeMail, "$image_path/2007a/iMail/icon_compose.gif");
//                    }
//
//                $MenuArr["CheckMail"] = array($i_CampusMail_New_CheckMail, $PATH_WRT_ROOT."home/imail/checkmail.php", $PageCheckMail, "$image_path/2007a/iMail/icon_check_mail.gif");
//                $MenuArr["CheckMail"]["Child"]["Inbox"] = array($i_CampusMail_New_Inbox.$NewMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=2", $PageCheckMail_Inbox, "$image_path/2007a/iMail/icon_inbox.gif");
//                $MenuArr["CheckMail"]["Child"]["Outbox"] = array($i_CampusMail_New_Outbox, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=0", $PageCheckMail_Outbox, "$image_path/2007a/iMail/icon_outbox.gif");
//                $MenuArr["CheckMail"]["Child"]["Draft"] = array($i_CampusMail_New_Draft, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=1", $PageCheckMail_Draft, "$image_path/2007a/iMail/icon_draft.gif");
//                $MenuArr["CheckMail"]["Child"]["Trash"] = array($i_CampusMail_New_Trash, $PATH_WRT_ROOT."home/imail/trash.php", $PageCheckMail_Trash, "$image_path/2007a/iMail/btn_trash.gif");
//                $MenuArr["CheckMail"]["Child"]["FolderManager"] = array($i_CampusMail_New_FolderManager, $PATH_WRT_ROOT."home/imail/folder.php", $PageCheckMail_FolderManager, "$image_path/2007a/iMail/icon_folder_open.gif");
//
//                ### Child Folder Array (to be set)
//                /*
//                structure:
//                $ChildFolderArr[] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
//                */
//
//                //$ChildFolderArr[] = array($i_CampusMail_New_Draft, $PATH_WRT_ROOT."home/imail/", 0, "$image_path/2007a/iMail/icon_folder_close.gif");
//                //$ChildFolderArr[] = array($i_CampusMail_New_Trash, $PATH_WRT_ROOT."home/imail/", 1, "$image_path/2007a/iMail/icon_folder_close.gif");
//
//                if (is_array($folders) && count($folders) >0)
//                {
//                        for ($i=0;$i<count($folders);$i++)
//                        {
//                                if (($folders[$i][0] != 0) && ($folders[$i][0] != 1) && ($folders[$i][0] != 2) && ($folders[$i][0] != -1))
//                                {
//                                        if ($FolderID == $folders[$i][0] && $FolderID != "")
//                                        {
//                                                $ChildFolderArr[] = array($folders[$i][1], $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 1, "$image_path/2007a/iMail/icon_folder_close.gif");
//                                        } else {
//                                                $ChildFolderArr[] = array($folders[$i][1], $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 0, "$image_path/2007a/iMail/icon_folder_close.gif");
//                                        }
//                                }
//                        }
//                }
//
//
//                $MenuArr["CheckMail"]["Child"]["FolderManager"]["child"] = $ChildFolderArr;
//
//                $MenuArr["AddressBook"] = array($i_CampusMail_New_AddressBook, "", $PageAddressBook, "$image_path/2007a/iMail/icon_address_book.gif");
//                $MenuArr["AddressBook"]["Child"]["InternalRecipientGroup"] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=0&TabID=1$anc", $PageAddressBook_InternalReceipientGroup, "$image_path/2007a/iMail/icon_address_ingroup.gif");
//                if(!$noWebmail)
//                {
//                        $MenuArr["AddressBook"]["Child"]["ExternalRecipientGroup"] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=1&TabID=3$anc", $PageAddressBook_ExternalReceipientGroup, "$image_path/2007a/iMail/icon_address_exgroup.gif");
//                        $MenuArr["AddressBook"]["Child"]["ExternalRecipient"] = array($i_CampusMail_External_Recipient, $PATH_WRT_ROOT."home/imail/addressbook.php", $PageAddressBook_ExternalReceipient, "$image_path/2007a/iMail/icon_address_ex.gif");
//                }
//
//                $MenuArr["Search"] = array($i_CampusMail_New_Mail_Search, $PATH_WRT_ROOT."home/imail/search.php", $PageSearch, "$image_path/2007a/iMail/btn_view_source.gif");
//
//                $MenuArr["Settings"] = array($i_CampusMail_New_Settings, "", $PageSettings, "$image_path/2007a/iMail/icon_preference.gif");
//                $MenuArr["Settings"]["Child"]["PersonalSetting"] = array($i_CampusMail_New_Settings_PersonalSetting, $PATH_WRT_ROOT."home/imail/pref.php", $PageSettings_PersonalSetting);
//                $MenuArr["Settings"]["Child"]["Signature"] = array($i_CampusMail_New_Settings_Signature, $PATH_WRT_ROOT."home/imail/pref_signature.php", $PageSettings_Signature);
//                if (!$noWebmail)
//                {
//                        $MenuArr["Settings"]["Child"]["EmailForwarding"] = array($i_CampusMail_New_Settings_EmailForwarding, $PATH_WRT_ROOT."home/imail/pref_email_forwarding.php", $PageSettings_EmailForwarding);
//                }
//
//
//                # module information
//                $MODULE_OBJ['title'] = $i_CampusMail_New_iMail;
//                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_iMail.gif";
//                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/imail/index.php";
//                //$MODULE_OBJ['menu'] = array();
//                $MODULE_OBJ['menu'] = $MenuArr;
//
//                return $MODULE_OBJ;
//        }
        
        function GET_MODULE_OBJ_ARR()
        {

                global $PATH_WRT_ROOT, $i_Discipline_System, $CurrentPage, $iDiscipline, $LAYOUT_SKIN, $i_CampusMail_New_iMail;
                global $image_path;

                global $i_CampusMail_New_CheckMail, $i_CampusMail_New_Inbox, $i_CampusMail_New_AddressBook;
                global $i_CampusMail_Internal_Recipient_Group, $i_CampusMail_External_Recipient, $i_CampusMail_External_Recipient_Group;
                global $i_CampusMail_New_Outbox, $i_CampusMail_New_Draft, $i_CampusMail_New_Trash;
                global $i_CampusMail_New_Settings, $i_CampusMail_New_FolderManager, $i_CampusMail_New_ComposeMail;
                global $i_CampusMail_New_Mail_Search, $i_CampusMail_New_Settings_PersonalSetting;
                global $i_CampusMail_New_Settings_Signature;
                global $unreadInboxNo, $folders, $FolderID, $UserID, $i_CampusMail_New_Settings_EmailForwarding;
                global $special_option,  $iNewCampusMail;
                global $i_CampusMail_New_Settings_AutoReply, $i_CampusMail_New_Settings_POP3, $i_CampusMail_New_Settings_EmailRules;
                global $i_spam, $webmail_info, $sys_custom, $special_feature;
                global $Lang, $Folder;

                
                $anc = $special_option['no_anchor'] ?"":"#anc";
                /*
                //Check has webmail or not
                $lwebmail = new libwebmail();
                if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
                {
                    include_once("../../includes/libsystemaccess.php");
                    $lsysaccess = new libsystemaccess($UserID);
                    if ($lsysaccess->hasMailRight())
                    {
                        $noWebmail = false;
                    }
                    else
                    {
                        $noWebmail = true;
                    }
                }
                else
                {
                    $noWebmail = true;
                }
				*/
                # Current Page Information init
                $PageComposeMail = 0;

                $PageCheckMail = 0;
                $PageCheckMail_Inbox = 0;
                $PageCheckMail_Outbox = 0;
                $PageCheckMail_Draft = 0;
                $PageCheckMail_Trash = 0;
                $PageCheckMail_FolderManager = 0;

                $PageAddressBook = 0;
                $PageAddressBook_InternalReceipientGroup = 0;
                $PageAddressBook_ExternalReceipientGroup = 0;
                $PageAddressBook_ExternalReceipient = 0;

                $PageSearch = 0;

                $PageSettings = 0;
                $PageSettings_PersonalSetting = 0;
                $PageSettings_Signature = 0;
				
                switch ($CurrentPage) {
                        case "PageComposeMail":
                                $PageComposeMail = 1;
                                break;

                        case "PageCheckMail_Inbox":
                                $PageCheckMail = 1;
                                $PageCheckMail_Inbox = 1;
                                break;
                        case "PageCheckMail_Outbox":
                                $PageCheckMail = 1;
                                $PageCheckMail_Outbox = 1;
                                break;
                        case "PageCheckMail_Draft":
                                $PageCheckMail = 1;
                                $PageCheckMail_Draft = 1;
                                break;
                        case "PageCheckMail_Trash":
                                $PageCheckMail = 1;
                                $PageCheckMail_Trash = 1;
                                break;
                        case "PageCheckMail_Spam":
                                $PageCheckMail = 1;
                                $PageCheckMail_Spam = 1;
                                break;
                        case "PageCheckMail_reportSpam":
                                $PageCheckMail = 1;
                                $PageCheckMail_reportSpam = 1;
                                break;
                        case "PageCheckMail_reportNonSpam":
                                $PageCheckMail = 1;
                                $PageCheckMail_reportNonSpam = 1;
                                break;
                                
                        case "PageSearch":
                                $PageSearch = 1;
                                break;
                                
                        case "PageCheckMail_FolderManager":
                                $PageCheckMail = 1;
                                $PageCheckMail_FolderManager = 1;
                                break;

                        case "PageAddressBook_InternalReceipientGroup":
                                $PageAddressBook = 1;
                                $PageAddressBook_InternalReceipientGroup = 1;
                                $PageAddressBook_ExternalReceipientGroup = 1;
                                break;

                        case "PageAddressBook_ExternalReceipientGroup":
                                $PageAddressBook = 1;
                                $PageAddressBook_ExternalReceipientGroup = 1;
                                break;

                        case "PageAddressBook_ExternalReceipient":
                                $PageAddressBook = 1;
                                $PageAddressBook_ExternalReceipient = 1;
                                break;
						
                        case "PageSearch":
                                $PageSearch = 1;
                                break;

                        case "PageSettings_PersonalSetting":
                                $PageSettings = 1;
                                $PageSettings_PersonalSetting = 1;
                                break;
                        case "PageSettings_Signature":
                                $PageSettings = 1;
                                $PageSettings_Signature = 1;
                                break;
                        case "PageSettings_EmailForwarding":
                                $PageSettings = 1;
                                $PageSettings_EmailForwarding = 1;
                                break;
						case "PageSettings_POP3":
                                $PageSettings = 1;
                                $PageSettings_POP3 = 1;
                                break;
						case "PageSettings_AutoReply":
                                $PageSettings = 1;
                                $PageSettings_AutoReply = 1;
                                break;
						case "PageSettings_EmailRules":
								$PageSettings = 1;
                                $PageSettings_EmailRules = 1;
                                break;
                       case "PageSettings_IMAP":
                       			$PageSettings = 1;
                       			$PageSettings_IMAP = 1;
                       			break;
                }

                /*
                structure:
                $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
                */

                /*

                */

                ### No of new email (to be set)
                /* 
                ### old version - only show the new mail no.
                $unreadInboxNo = $iNewCampusMail;
                if ($unreadInboxNo != "")
                {
                        $NoOfNewMail = $unreadInboxNo;
                } else {
                        $NoOfNewMail = 0;
                }
                if ($NoOfNewMail > 0) $NewMailText = " (".$NoOfNewMail.")";
                */
                
                $li = new libdb();
//                ### New version - show no of unread mail in Inbox
//                $NewMailText = "";
//                
//                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '2' AND RECORDSTATUS IS NULL AND DELETED = 0";
//                $arr_result = $li->returnVector($sql);
//                
//                if(sizeof($arr_result)>0)
//                {
//	                $NewMailNo = $arr_result[0];
//                }else{
//	                $NewMailNo = 0;
//                }
//                if ($NewMailNo > 0) $NewMailText = " (".$NewMailNo.")";
//                
//                ### New version - show no of unread mail in Draft
//                $NewDraftMailText = "";
//                
//                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '1' AND Deleted <> 1 AND RECORDSTATUS IS NULL";
//                $arr_result = $li->returnVector($sql);
//                
//                if(sizeof($arr_result)>0)
//                {
//	                $NewDraftMailNo = $arr_result[0];
//                }else{
//	                $NewDraftMailNo = 0;
//                }
//                if ($NewDraftMailNo > 0) $NewDraftMailText  = " (".$NewDraftMailNo.")";
//                
//                ### New version - show no of unread mail in Trash
//                $NewTrashMailText = "";
//                
//                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND DELETED = 1 AND RECORDSTATUS IS NULL";
//                $arr_result = $li->returnVector($sql);
//                
//                if(sizeof($arr_result)>0)
//                {
//	                $NewTrashMailNo = $arr_result[0];
//                }else{
//	                $NewTrashMailNo = 0;
//                }
//                if ($NewTrashMailNo > 0) $NewTrashMailText  = " (".$NewTrashMailNo.")";
//                
//                ### Show unread mail no. in SPAM folder (if $webmail_info['bl_spam'] = ture)
//                $NewSpamMailNo = "";
//                if($webmail_info['bl_spam'])
//                {
//	                $li = new libdb();
//	                
//	                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '-2' AND Deleted <> 1 AND RECORDSTATUS IS NULL";
//	                $arr_result = $li->returnVector($sql);
//	                
//	                if(sizeof($arr_result)>0)
//	                {
//		                $NewSpamMailNo = $arr_result[0];
//	                }else{
//		                $NewSpamMailNo = 0;
//	                }
//	                
//	                if($NewSpamMailNo > 0) $NewSpamMailText = " (".$NewSpamMailNo.")";
//                }

                   
                //20100812 
                if(!$this->GmailMode){
                $NewMailText = $this->Get_Unseen_Mail_Number($this->InboxFolder);
                $NewDraftMailText = $this->Get_Unseen_Mail_Number($this->DraftFolder);
                $NewTrashMailText = $this->Get_Unseen_Mail_Number($this->TrashFolder);
                $NewSpamMailText = $this->Get_Unseen_Mail_Number($this->SpamFolder);
                //$NewreportSpamMailText = $this->Get_Unseen_Mail_Number($this->reportSpamFolder);
                //$NewreportNonSpamSpamMailText = $this->Get_Unseen_Mail_Number($this->reportNonSpamFolder);
                }
                # Menu information
                if (auth_sendmail())
                {
                    $MenuArr["ComposeMail"] = array($i_CampusMail_New_ComposeMail, $PATH_WRT_ROOT."home/imail_gamma/compose_email.php", $PageComposeMail, "$image_path/{$LAYOUT_SKIN}/iMail/icon_compose.gif");
                }
				
				//20100812
                $MenuArr["CheckMail"] = array($i_CampusMail_New_CheckMail, $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php", $PageCheckMail, "$image_path/{$LAYOUT_SKIN}/iMail/icon_check_mail.gif");
                $MenuArr["CheckMail"]["Child"]["Inbox"] = array($Lang['Gamma']['SystemFolderName']["INBOX"].$NewMailText, $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php?Folder=".$this->InboxFolder, $PageCheckMail_Inbox, "$image_path/{$LAYOUT_SKIN}/iMail/icon_inbox.gif");
                $MenuArr["CheckMail"]["Child"]["Outbox"] = array($Lang['Gamma']['SystemFolderName']["Sent"], $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php?Folder=".$this->SentFolder, $PageCheckMail_Outbox, "$image_path/{$LAYOUT_SKIN}/iMail/icon_outbox.gif");
                $MenuArr["CheckMail"]["Child"]["Draft"] = array($Lang['Gamma']['SystemFolderName']["Drafts"].$NewDraftMailText, $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php?Folder=".$this->DraftFolder, $PageCheckMail_Draft, "$image_path/{$LAYOUT_SKIN}/iMail/icon_draft.gif");
                $MenuArr["CheckMail"]["Child"]["Spam"] = array($Lang['Gamma']['SystemFolderName']['Junk'].$NewSpamMailText, $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php?Folder=".$this->SpamFolder, $PageCheckMail_Spam, "$image_path/$LAYOUT_SKIN/iMail/icon_spam.gif");
                $MenuArr["CheckMail"]["Child"]["Trash"] = array($Lang['Gamma']['SystemFolderName']["Trash"].$NewTrashMailText, $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php?Folder=".$this->TrashFolder, $PageCheckMail_Trash, "$image_path/{$LAYOUT_SKIN}/iMail/btn_trash.gif");

				### Folder Management
                $MenuArr["CheckMail"]["Child"]["FolderManager"] = array($i_CampusMail_New_FolderManager, $PATH_WRT_ROOT."home/imail_gamma/folder.php", $PageCheckMail_FolderManager, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_open.gif");
                
		            ### build Child Folder Ary
		            $folders = $this->Get_Folder_Structure(true,false);		            
		            for($i=0;$i<sizeof($folders);$i++)
		            {
		            	$PageCheckMail_PersonalFolder = $Folder == $folders[$i][1]?1:0;
				     	if(!$this->GmailMode) $NewFolderMailText = $this->Get_Unseen_Mail_Number($folders[$i][1]);
			            $temp = get_wrap_content(IMap_Decode_Folder_Name($folders[$i][0]),12," ",1);
	                    $ChildFolderArr[] = array($temp.$NewFolderMailText, $PATH_WRT_ROOT."home/imail_gamma/viewfolder.php?Folder=".urlencode($folders[$i][1]), $PageCheckMail_PersonalFolder, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");
		            }
		            
		            $MenuArr["CheckMail"]["Child"]["FolderManager"]["child"] = $ChildFolderArr;
		            
                ### Child Folder Array (to be set)
                /*
                structure:
                $ChildFolderArr[] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                */

                $MenuArr["AddressBook"] = array($i_CampusMail_New_AddressBook, "", $PageAddressBook, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_book.gif");
//                $MenuArr["AddressBook"]["Child"]["InternalRecipientGroup"] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=0&TabID=1$anc", $PageAddressBook_InternalReceipientGroup, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_ingroup.gif");
//                if(!$noWebmail)
//                {
                        //$MenuArr["AddressBook"]["Child"]["ExternalRecipientGroup"] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=1&TabID=3$anc", $PageAddressBook_ExternalReceipientGroup, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_exgroup.gif");
                        //$MenuArr["AddressBook"]["Child"]["ExternalRecipient"] = array($i_CampusMail_External_Recipient, $PATH_WRT_ROOT."home/imail_gamma/addressbook.php", $PageAddressBook_ExternalReceipient, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif");
                        $MenuArr["AddressBook"]["Child"]["ExternalRecipientGroup"] = array($Lang['Gamma']['RecipientGroup'], $PATH_WRT_ROOT."home/imail_gamma/groupalias.php?aliastype=1&TabID=3$anc", $PageAddressBook_ExternalReceipientGroup, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_exgroup.gif");
                        $MenuArr["AddressBook"]["Child"]["ExternalRecipient"] = array($Lang['Gamma']['Recipient'], $PATH_WRT_ROOT."home/imail_gamma/addressbook.php", $PageAddressBook_ExternalReceipient, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif");
//                }
//
                $MenuArr["Search"] = array($Lang['Gamma']['AdvancedSearch'], $PATH_WRT_ROOT."home/imail_gamma/advance_search.php", $PageSearch, "$image_path/{$LAYOUT_SKIN}/iMail/btn_view_source.gif");

//
                $MenuArr["Settings"] = array($i_CampusMail_New_Settings, "", $PageSettings, "$image_path/{$LAYOUT_SKIN}/iMail/icon_preference.gif");
//                
                $MenuArr["Settings"]["Child"]["PersonalSetting"] = array($i_CampusMail_New_Settings_PersonalSetting, $PATH_WRT_ROOT."home/imail_gamma/pref.php", $PageSettings_PersonalSetting);
                $MenuArr["Settings"]["Child"]["Signature"] = array($i_CampusMail_New_Settings_Signature, $PATH_WRT_ROOT."home/imail_gamma/pref_signature.php", $PageSettings_Signature);
//                if (!$noWebmail)
//                {
					if(!$sys_custom['iMailPlus']['DisableAutoForward']){
                        $MenuArr["Settings"]["Child"]["EmailForwarding"] = array($i_CampusMail_New_Settings_EmailForwarding, $PATH_WRT_ROOT."home/imail_gamma/pref_email_forwarding.php", $PageSettings_EmailForwarding);
					}
//                        $MenuArr["Settings"]["Child"]["POP3"] = array($i_CampusMail_New_Settings_POP3, $PATH_WRT_ROOT."home/imail/pref_pop3.php", $PageSettings_POP3);
//	                    if($webmail_info['actype'] == "postfix")
//		                {
						if(!($_SESSION['UserType']==USERTYPE_STUDENT && $sys_custom['iMailPlus']['DisableStudentAutoReply'])){
	                        $MenuArr["Settings"]["Child"]["AutoReply"] = array($i_CampusMail_New_Settings_AutoReply, $PATH_WRT_ROOT."home/imail_gamma/pref_auto_reply.php", $PageSettings_AutoReply);
						}
//	                    }
//	                    if($sys_custom['iMail_MailRule']){
//	                    	$MenuArr["Settings"]["Child"]["EmailRules"] = array($i_CampusMail_New_Settings_EmailRules, $PATH_WRT_ROOT."home/imail/pref_email_rules.php", $PageSettings_EmailRules);
//                    	}
//                }
//              
				$MenuArr["Settings"]["Child"]["IMAP"] = array("IMAP", $PATH_WRT_ROOT."home/imail_gamma/pref_imap.php", $PageSettings_IMAP);
				
				# Display number of unseen mails at document title
				$js = '<script type="text/JavaScript" language="JavaScript">'."\n";
				$js.= 'document.title="'.($NewMailText!=''?$NewMailText.' ':'').'eClass iMail plus";'."\n";
				$js.= '</script>'."\n";
				
				# imail archive
				global $SYS_CONFIG;
				//if($SYS_CONFIG['Mail']['hide_imail']===true)
				if($special_feature['imail']===true)
					$MenuArr["Archive"] = array($Lang['Gamma']['iMailArchive'], "javascript:newWindow('".$PATH_WRT_ROOT."home/imail/viewfolder.php',32);", "", "");
 
                # module information
                $MODULE_OBJ['title'] = str_replace("plus", "<i>plus</i>", $Lang['Gamma']['iMailGamma']).$js;
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_iMail.gif";
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/imail_gamma/index.php";
                //$MODULE_OBJ['menu'] = array();
                $MODULE_OBJ['menu'] = $MenuArr;

                return $MODULE_OBJ;
        }

        function getMailList()
        {
               //$mailCount = $this->checkMailCount();
               $mailCount = imap_num_msg($this->inbox);
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;
               //reset($overview);
               return $overview;

        }
        function getMailServerRef()
        {
        	return "{".$this->host.":".$this->mail_server_port."}";
					//return "{".$this->host.":".$this->mail_server_port."/ssl}";
        }

        function getMailFolders()
        {
			$mail_string_ref = $this->getMailServerRef();
			$length = strlen($mail_string_ref);

			$folders = imap_getmailboxes($this->inbox, $mail_string_ref,"*");
			$result = array();
			$i = 0;
			while (list($key, $value) = each($folders))
			{
				$temp = substr($value->name, $length);
				$result[IMap_Decode_Folder_Name($this->decodeFolderName($temp))] = $this->decodeFolderName($temp);

			}

			uksort($result,strcasecmp);
			//usort($result,"String_To_Lower_Array_Cmp");
			
			return array_values($result);
        }

        /* ------ Get Mail Folder List by level, by kenneth chung 20080423 ------ */
        function Get_Folder_List_In_Path($MailFolder="",$AllGet=false,$Recursive=false) {
                global $SYS_CONFIG;
                if ($MailFolder == "") $MailFolder = $this->encodeFolderName($this->FolderPrefix);
                else $MailFolder = $this->encodeFolderName($MailFolder);

                $mail_string_ref = $this->getMailServerRef();
                $length = strlen($mail_string_ref);
                if (!$AllGet) {
                        if (!$Recursive)
                                $folders = imap_getsubscribed($this->inbox, $mail_string_ref.$MailFolder,"%");
                        else
                                $folders = imap_getsubscribed($this->inbox, $mail_string_ref.$MailFolder,"*");
                }
                else {
                        if (!$Recursive)
                                $folders = imap_getmailboxes($this->inbox, $mail_string_ref.$MailFolder,"%");
                        else
                                $folders = imap_getmailboxes($this->inbox,$mail_string_ref.$MailFolder,"*");
                }
                $result = array();

                if ($folders !== false) {
                        for ($i=0; $i< sizeof($folders); $i++) {

                                $temp = substr($folders[$i]->name, $length);
                                if (!in_array($temp,$this->DefaultFolderList)) {
                                        $result[] = $this->decodeFolderName($temp);
                                }
                        }
                }

                sort($result,SORT_STRING);
                //usort($result,"String_To_Lower_Array_Cmp");

                return $result;
        }

        /* ----- Move to Folder, by kenneth chung 20080423 ------ */
        function Go_To_Folder($MailFolder=""){
                return imap_reopen($this->inbox, $this->getMailServerRef().$this->encodeFolderName($MailFolder));
        }

        /* Unused
        function getMailFolderList()
    {
            ### like $this->getMailFolders()
        }
        */
/* To-do: */

  /* Generate a MIME format of email, for save (Draft, sent) and send email */
  /* Param: Subject, email message, from address, to, cc, bcc, attachment path , Is important, mail return path
     Return: mime text
  */
        // Edited by kenneth chung at 20080430 to adopt new attachment structure
  function buildMimeText($subject,$message,$from,$receiver_to=array(),$receiver_cc=array(),$receiver_bcc=array(),$AttachList=array(),$IsImportant="",$DraftFlag=false, $Perference="") {
    /*  mainly follow the logic of sendmail()
            check charset, how about UTF-8 encoding?
    */

    # Content-type of attachment
    $type="application/octet-stream";

    $priority = ($IsImportant==1)? 1: 3;
   	/*echo '<pre>';
    var_dump($receiver_to);
    echo sizeof($receiver_to);
    echo '</pre>';*/
    # receiving email addresses -  To, Cc & Bcc fields
	$FinalReceiverTo = array();
	$FinalReceiverCc = array();
	$FinalReceiverBcc = array();	
    for ($i=0; $i< sizeof($receiver_to); $i++) {
		if (trim($receiver_to[$i]) != "") {
			$FinalReceiverTo[] = $receiver_to[$i];
		}
    }
    for ($i=0; $i< sizeof($receiver_cc); $i++) {
		if (trim($receiver_cc[$i]) != "") {
			$FinalReceiverCc[] = $receiver_cc[$i];
		}
    }
    for ($i=0; $i< sizeof($receiver_bcc); $i++) {
		if (trim($receiver_bcc[$i]) != "") {
			$FinalReceiverBcc[] = $receiver_bcc[$i];
		}
    }
    $numTo = sizeof($FinalReceiverTo);
    $numCC = sizeof($FinalReceiverCc);
    $numBCC = sizeof($FinalReceiverBcc);

	# Check Any Email is added in those Address Fields
    if (!$DraftFlag) {
		if ($numTo + $numCC + $numBCC == 0) return false;
	}

    $to  = ($numTo != 0) ? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverTo))) : "";
    $cc  = ($numCC != 0) ? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverCc))) : "";
    $bcc = ($numBCC != 0) ? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverBcc))) : "";

    # Mail Header - Preparation
    if ($Perference != "") {
		// have peference
		$fromName = (trim($Perference[0]["DisplayName"]) == "") ? substr($from,0,stripos($from,"@")) : $Perference[0]["DisplayName"];
		$ReplyAddress = (trim($Perference[0]["ReplyEmail"]) != "") ? $Perference[0]["ReplyEmail"] : $from;
		$HeaderFrom .= "From: \"$fromName\" <$from>".$this->MimeLineBreak;
		
		$ReturnMsg['From'] = trim('"'.$fromName.'" <'.$from.'>');
		$ReturnMsg['SortFrom'] = str_replace('"','',$ReturnMsg['From']);
	}
	else {
		// for auto mail, e.g password reset
		$HeaderFrom .= 'From: '.$from.$this->MimeLineBreak;
		$ReplyAddress = $from;
	}

	// Attachement Array
    for ($i=0; $i< sizeof($AttachList); $i++) {
		$files[] = $AttachList[$i][2];
		$fileRealName[] = $AttachList[$i][1];
    }

    //global $intranet_session_language;
    $charset = "UTF-8";
    //$charset = (($intranet_session_language=="gb")?"gb2312":"big5");

    #$charset = (isBig5($message)?"big5":"iso-8859-1");
    #$isHTMLMessage = $this->isHTMLContent($message);
    //global $special_feature;
    $isHTMLMessage = 1; //($special_feature['imail_richtext']);

    $isMulti = (sizeof($files)!=0);

    $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
    $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part

	# Mail Source Content
	$headers = '';
    if ($isMulti)
    {
        $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
		$headers .= "X-Priority: $priority".$this->MimeLineBreak;
		$headers .= "Content-Type: multipart/mixed; ";
		$headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    else if ($isHTMLMessage)         # HTML message w/o attachments
    {
		$headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
		$headers .= "X-Priority: $priority".$this->MimeLineBreak;
		$headers .= "Content-Type: multipart/alternative; ";
		$headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    if ($Perference == "")
        $headers .= $HeaderFrom;

    $headers .= "Date: " . date("r") .$this->MimeLineBreak;
    # Commented by Kenneth Wong (20080612)
    #$headers .= "Return-Path: $HeaderFrom ".$this->MimeLineBreak;

    if ($ReplyAddress != "")
    {
        $headers .= "Reply-To: $ReplyAddress".$this->MimeLineBreak;
    }
	//$headers .= "Message-ID: <".time()."@inc.broadlearner.com>\r\n";
	//$headers .= "X-Mailer: PHP ".phpversion()."\r\n";
	//$headers .= "X-Sender: $from\r\n";

	//echo $headers;

	//die;

    if ($isMulti || $isHTMLMessage)
    {
        $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
    }

	if (!$isMulti) { # No attachments
		if ($isHTMLMessage) {
			$text_message = Remove_HTML_Tags($message);
	        #$encoded_text_message = QuotedPrintableEncode($message);
	        $encoded_text_message = chunk_split(base64_encode($text_message));
		
            $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
        }
	    else {
			$mime .= $message;
	    }
    }
    else {
        if ($isHTMLMessage) {
            $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode(Remove_HTML_Tags($message))). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. "".$this->MimeLineBreak;
        }
		else {
            # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;

	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
        }
        $filename_encoding = "=?".strtoupper($charset)."?B?";

		# Embed attachment files
		for ($i=0; $i<sizeof($files); $i++)
		{
            //$target = $files[$i];
	        $data = Get_File_Content($files[$i]);
	        $fname = addslashes($fileRealName[$i]);

	        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
	        //$mime .= "\tname=\"$fname\"\r\n";
	        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
	        //$mime .= "\tfilename=\"$fname\"\r\n\r\n";
	        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($data));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary;

	        # Different if end of email
	        if ($i==sizeof($files)-1)
	            $mime .= "--".$this->MimeLineBreak;
	        else
	            $mime .= $this->MimeLineBreak;
        }
    }

	$ReturnMsg['to'] = str_replace('\"','"',$to);
	$ReturnMsg['cc'] = str_replace('\"','"',$cc);
	$ReturnMsg['bcc'] = str_replace('\"','"',$bcc);
	$ReturnMsg['subject'] = (trim($subject) == "")? "No Subject":$this->Get_Mail_Format($subject,"UTF-8");
	$ReturnMsg['headers'] = $headers;
	$ReturnMsg['body'] = $mime;

	// Kenneth Wong (20080612) Added element headers2 for mail()
	$ReturnMsg['headers2'] = $headers;
	if ($cc != "")
	{
		$ReturnMsg['headers2'] .= "cc: $cc".$this->MimeLineBreak;
	}
	if ($bcc != "")
	{
		$ReturnMsg['headers2'] .= "bcc: $bcc".$this->MimeLineBreak;
	}
	$ReturnMsg['headers2'] .= $HeaderFrom . $this->MimeLineBreak;

	$headers .= $HeaderFrom;
	# Commented by Kenneth Wong (20080612): return path set in mail() -f option
	#$headers .= "Return-Path: $HeaderFrom ".$this->MimeLineBreak;
	$headers .= "To: $to".$this->MimeLineBreak;
	$headers .= "cc: $cc".$this->MimeLineBreak;
	$headers .= "bcc: $bcc".$this->MimeLineBreak;
	$headers .= "Subject: ".$this->Get_Mail_Format($subject,"UTF-8").$this->MimeLineBreak;
	$ReturnMsg['FullMimeMsg'] = $headers.$mime;
    
    $ReturnMsg['SortTo'] = trim(str_replace('"','',$ReturnMsg['to']));
    $ReturnMsg['Boundary'] = $mime_boundary;

    return $ReturnMsg;
  }

        /*
          Save an email entry to SENT Folder
          Param : mail - mime text
          Return : Successful - true / false
        */
        //edited by kenneth chung at 20080502, will change later for folder structure
        function saveSentCopy($mail)
        {
                $Result["AppendFlag"] = imap_append($this->inbox, $this->getMailServerRef().$this->DefaultFolderList["SentFolder"], $mail);
                $uid = $this->Get_Last_Msg_UID($this->DefaultFolderList["SentFolder"]);
                $Result["SetReadFlag"] = imap_setflag_full($this->inbox,$uid,'\\Seen',ST_UID);
				
				if($Result["AppendFlag"] && $this->CacheMailToDB == true){
        			$this->Manage_Cached_Mail("add",$this->SentFolder,$uid);
				}
								/*echo '<pre>';
								var_dump($mail);
								echo '</pre>';
								echo '<pre>';
								var_dump($Result);
								echo '</pre>';
								die;*/
                return $Result["AppendFlag"];
        }

        /*
          Save an email entry to DRAFT Folder
          Param : mail - mime text
          Return : Successful - true / false
        */
        //edited by kenneth chung at 20080506, will change later for folder structure
        function saveDraftCopy($mail)
        {
            $result = imap_append($this->inbox, $this->getMailServerRef().$this->DefaultFolderList["DraftFolder"], $mail);
        	if($result && $this->CacheMailToDB == true){
        		$Uid = $this->Get_Last_Msg_UID($this->DraftFolder);
        		$this->Manage_Cached_Mail("add",$this->DraftFolder,$Uid);
        	}
        	return $result;
        }

        /*
          Read the email record
          Param : uid - UID of the mail
          Return : Array()
                     - email UID
                     - email subject
                     - email content (text)
                     - email (HTML alternative)
                     - From (decoded)
                     - To (decoded)
                     - CC (decoded)
                     - Date of the email
                     - charset encoding
                     - attachment array()
                       - part number/ID String
                       - attachment filename
                       - attachment content type
                       - attachment size
                       - attachment encoding
                       - attachment cid (for embedded image)

        */
		
		function checkUID($folderName, $uid, $PartNumber='') {
		# By Michael Cheung 2009-10-14 to solve F5 (refresh) at draft after auto-save
			$mailbox = $this->inbox;
			imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
			$overview = imap_fetch_overview($mailbox, $uid, FT_UID); // to get the msgno for the following use
			if ($overview == false) {
				return false;
			} else {
				return true;
			}
		}
      function getMailRecord($folderName, $uid, $PartNumber="")
      {
        /* refer to $this->checkmail2() imap_bodystruct() and imap_body() */
        //main follow the flow of checkmail2()
        $mailbox = $this->inbox;
        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
		// echo '$folderName : '.$folderName.'<br>';
		// echo '$uid : '.$uid.'<br>';
		// echo '$PartNumber : '.$PartNumber.'<br>';
        $overview = imap_fetch_overview($mailbox, $uid, FT_UID); // to get the msgno for the following use
        if ($overview === false) return null;
		
        $mail_msgno  = $overview[0]->msgno;
        $text_header = imap_fetchheader($this->inbox, $mail_msgno); 
        $raw_header  = imap_rfc822_parse_headers($text_header);
        $header 	 = imap_header($this->inbox, $mail_msgno);
		
        $from 		 = $header->fromaddress;
        $fromDetail  = $header->from;
		$to 		 = $raw_header->toaddress;
		$toDetail 	 = $raw_header->to;
		$cc 		 = $raw_header->ccaddress;
		$ccDetail 	 = $raw_header->cc;
		$bcc 		 = $raw_header->bccaddress;
		$bccDetail 	 = $raw_header->bcc;
        $subject 	 = $header->Subject;
        $flagged 	 = $header->Flagged;
		
		# Another Approach
		/*
		$mail_msgno = @imap_msgno($mailbox, $uid);		# Get Message Number of Mail by UID
		if ($mail_msgno === false) return null;
		$header = imap_header($this->inbox, $mail_msgno);
		
		$from 		 = $header->fromaddress;
		$fromDetail  = $header->from;
		$to 		 = $header->toaddress;
		$toDetail 	 = $header->to;
		$cc 		 = $header->ccaddress;
		$ccDetail 	 = $header->cc;
		$bcc 		 = $header->bccaddress;
		$bccDetail 	 = $header->bcc;
		$subject 	 = $header->Subject;
		*/

		# From Address
        $fromname = $this->MIME_Decode($from);

		# Subject 
        $subject = $this->MIME_Decode($subject);

		# To Address
        $showto = $this->MIME_Decode($to);

		# Cc Address
        $showcc = $this->MIME_Decode($cc);

		# Bcc Address
        $showbcc = $this->MIME_Decode($bcc);

		# Arrival Date
        $dateReceive = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($header->date)));

		# Body Structure
        //$structure = imap_fetchstructure($mailbox, $mail_msgno);
				//echo '<pre>';
				//debug_pr($structure);
				//echo '</pre>';
		//if($this->IMapCache===false)
		//		$this->IMapCache = new imap_cache_agent();
		$this->getImapCacheAgent();
		
				if (trim($PartNumber) != "") {
					$MailDetail = $this->IMapCache->Get_Attach_Message_Detail($folderName,$uid,$PartNumber);
					$fromname = $MailDetail["From"];
					$subject = $MailDetail["Subject"];
					$showto = $MailDetail["To"];
					$showcc = $MailDetail["Cc"];
					$showbcc = $MailDetail["Bcc"];
					$Attachment_List = $MailDetail['AttachList'];
					$message = $this->IMapCache->Get_Message_Text($folderName,$uid,"",$PartNumber);
					$RelatedInlineList = $this->IMapCache->Get_Related_Inline_Info($folderName,$uid,"",$PartNumber);
				}
				else {
					$EncodedFolder = IMap_Encode_Folder_Name($folderName);
					$StructureArray = $this->IMapCache->Get_Message_Structure($uid,$EncodedFolder);
					$message = $this->IMapCache->Get_Message_Text($folderName,$uid,$StructureArray);
					//$StructureArray = $this->IMapCache->Get_Message_Structure($uid,$EncodedFolder);
					$Attachment_List = $this->IMapCache->Get_Attachment_List($StructureArray,$folderName,$uid);
					$RelatedInlineList = $this->IMapCache->Get_Related_Inline_Info($folderName,$uid,$StructureArray,$PartNumber);
					//debug_r($PartNumber);
					//debug_r($StructureArray);
					//debug_r($message);
				}
				//$this->IMapCache->Close_Connect();
        /*unset($this->parsed_part);
        $this->parse_message($structure);
        $attachments = $this->grab_attach($this->parsed_part);*/
		//echo "<pre>";var_dump($attachments);echo "</pre>";die();
		
        # Handle attachments
        /*unset($attach_parts);
        unset($embed_cids);
        $total_attach_size = 0;
        for ($k=0; $k<sizeof($attachments); $k++)
        {
			$pid 			   = $attachments[$k]["partnum"];
			$attach_filename = $attachments[$k]["filename"];
			$attach_type 	   = $attachments[$k]["type"];
			$attach_subtype  = $attachments[$k]["subtype"];
			$attach_size 	   = $attachments[$k]["size"];
			$attach_encoding = $attachments[$k]["encoding"];
			$attach_cid 	   = $attachments[$k]["cid"];

			$elements = imap_mime_header_decode($attach_filename);

			unset($subtmp);
			for($f_i=0;$f_i<count($elements);$f_i++) {
				$subtmp .= $elements[$f_i]->text;
			}
			$attach_filename = $subtmp;

			$attach_content = $this->retrieve_attachment_content($mailbox, $mail_msgno, $pid);
			$decoded_content = $this->mime_decode_msg($attach_content,$this->mime_encoding[$attach_encoding]);

			unset($attach_entry);
			$attach_entry['partid'] = $pid;
			$attach_entry['filename'] = $attach_filename;
			$attach_entry['type'] = $this->mime_type[$attach_type];
			$attach_entry['subtype'] = $attach_subtype;
			$attach_entry['size'] = $attach_size;
			$attach_entry['cid'] = $attach_cid;
			$attach_entry['content'] = $decoded_content;
			$attach_parts[] = $attach_entry;
			$embed_cids[] = array($attach_entry['cid'],$attach_entry['filename']);
			$total_attach_size += $attach_size;
        }*/

        //$message 	  = $this->retrieve_message($mailbox, $mail_msgno, $this->parsed_part);
        //$text_message = $this->retrieve_textmessage($mailbox, $mail_msgno, $structure);
        
    //    $Priority 	  = $this->Get_Header_Flag($mail_msgno,'X-Priority');
    //    $eClassMailID 	  = $this->Get_Header_Flag($mail_msgno,'X-eClass-MailID');
    //    $MessageID = $this->Get_Header_Flag($mail_msgno,'Message-ID');
		$header_flags = $this->Get_Header_Flag($mail_msgno,'');
		$Priority = $header_flags['x-priority'];
		$eClassMailID = $header_flags['x-eclass-mailid'];
		$MessageID = $header_flags['message-id'];
		
        $returnArray = array();
        $returnArray[0]["msgno"] = $mail_msgno;
        $returnArray[0]["uid"] = $uid;
        $returnArray[0]["Priority"] = ($Priority == 1)? $Priority:0;
        $returnArray[0]["subject"] = $subject;
        //$returnArray[0]["message"] = $this->Conver_Encoding($message,"UTF-8",$this->message_charset);
        //$returnArray[0]["text_message"] = $this->Conver_Encoding($text_message,"UTF-8",$this->message_charset);
        $returnArray[0]["message"] = $message;
        $returnArray[0]["text_message"] = $message;
        $returnArray[0]["total_attach_size"] = $total_attach_size;
        $returnArray[0]["fromname"] = $fromname;
        $returnArray[0]["showto"] = $showto;
        $returnArray[0]["showcc"] = $showcc;
        $returnArray[0]["showbcc"] = $showbcc;
        $returnArray[0]["toDetail"] = $toDetail;
        $returnArray[0]["fromDetail"] = $fromDetail;
        $returnArray[0]["ccDetail"] = $ccDetail;
        $returnArray[0]["bccDetail"] = $bccDetail;
        $returnArray[0]["dateReceive"] = $dateReceive;
        $returnArray[0]["ReceiveTimeStamp"] = strtotime($this->formatDatetimeString($header->date));
        $returnArray[0]["attach_parts"] = $Attachment_List;
        $returnArray[0]["embed_cids"] = $embed_cids;
        //$returnArray[0]["message_charset"] = $this->message_charset;
        //$returnArray[0]["is_html_message"] = $this->is_html_message;
        $returnArray[0]["message_charset"] = 'UTF8';
        $returnArray[0]["is_html_message"] = true;
        $returnArray[0]["flagged"] = $flagged;
        $returnArray[0]["rawHeader"] = $raw_header;
        $returnArray[0]["header"] = $header;
		$returnArray[0]["eClassMailID"] = $eClassMailID;
		$returnArray[0]["MessageID"] = $MessageID;
		$returnArray[0]["RelatedInlineList"] = $RelatedInlineList;
		$returnArray[0]["header_flags"] = $header_flags;
		
        return $returnArray;
                                  /*
                                          array($mail_msgno, $subject,$message, $total_attach_size,
                          $fromname, $showto, $showcc, $dateReceive, $attach_parts,
                          $embed_cids, $this->message_charset, $this->is_html_message);
                                  */
      }

        /*
          Check Folder name is valid (not INBOX, DRAFT, SENT, SPAM and special characters)
          Param: folderName
          Return: True / False
        */
        function isValidMailFolderName($folderName)
        {
                        return !(substr_count($folderName, '"') || substr_count($folderName, "\\") ||
                                substr_count($folderName, $this->folderDelimiter) || ($folderName == '') ||
                                (strtoupper($folderName) == "INBOX") || (strtoupper($folderName) == "DRAFT") ||
                                (strtoupper($folderName) == "SENT") || (strtoupper($folderName) == "SPAM"));

                        //need to put those predefined names into config?
        }

        /*
          Create Folder
          Param: folderName
          Return: True / False
        */
        function createMailFolder($parentFolder, $folderName, $debug=0)
        {
        	$parentFolder = ($parentFolder == "")? $this->FolderPrefix:$parentFolder;
                //$folderName - in UTF-8 format
            $newFolder = $folderName;
          if ($parentFolder != "")
          {
	            $newFolder = $parentFolder . $this->folderDelimiter . $folderName;
	        }
	        if($debug==1)
	        {
				debug_pr($this->encodeFolderName($this->getMailServerRef().$newFolder));
				die;
	        }
	        if (imap_createmailbox($this->inbox, $this->encodeFolderName($this->getMailServerRef().$newFolder))) {
	                if (imap_subscribe($this->inbox, $this->encodeFolderName($this->getMailServerRef().$newFolder))) {
	                        $Result = true;
	                }
	        }
	        else {
	                $Result = false;
	        }

                                        return $Result;
        }

        /*
          Remove Folder
          Param: folderName
          Return: True / False
        */
        function Delete_Mail_Folder($folderName){
          global $SYS_CONFIG;

                imap_close($this->inbox);
                $mailbox_prefix = $this->getMailServerRef();
                #$this->inbox = imap_open($mailbox_prefix.$this->encodeFolderName($folderName),$this->CurUserAddress,$this->CurUserPassword,OP_HALFOPEN);
                $this->inbox = $this->openInbox($this->CurUserAddress, $this->CurUserPassword, $folderName, true);

                $SubFolderList = $this->Get_Folder_List_In_Path($folderName,true,true);
                for ($i=0;$i < sizeof($SubFolderList); $i++) {
                    $result[$SubFolderList[$i]."UnSubscribe"] = imap_unsubscribe($this->inbox,$mailbox_prefix.$this->encodeFolderName($SubFolderList[$i]));
                    imap_deletemailbox($this->inbox, $mailbox_prefix.$this->encodeFolderName($SubFolderList[$i]));
                }
								
								if (!in_array($folderName,$SubFolderList)) {
                	$result[$folderName."UnSubscribe"] = imap_unsubscribe($this->inbox, $mailbox_prefix.$this->encodeFolderName($folderName));
                	imap_deletemailbox($this->inbox, $mailbox_prefix.$this->encodeFolderName($folderName));
                }
								
								imap_close($this->inbox);
				if(!in_array(false,$result) && $this->CacheMailToDB == true){
					$this->Manage_Cached_Mail("remove",$folderName,-1);
				}
				
                return !in_array(false,$result);
        }

        function expungeMailFolder($folderName)
        {
                $mailbox = $this->inbox;
                        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
                        return imap_expunge($mailbox);
        }

        function Rename_Mail_Folder($sourceFolder, $newFolder)
        {
			$TempPath = explode($this->folderDelimiter, $sourceFolder);
			$FolderPath = "";
			for ($i=0; $i< (sizeof($TempPath)-1); $i++){
				$FolderPath .= $TempPath[$i].$this->folderDelimiter;
			}
			$newFolder = $FolderPath.$newFolder;

			$mailbox_prefix = $this->getMailServerRef();
			
			if (imap_unsubscribe ($this->inbox, $this->encodeFolderName($mailbox_prefix.$sourceFolder))) {
				$SubFolderList = $this->Get_Folder_List_In_Path($sourceFolder,true,true);
				for($i=0; $i< sizeof($SubFolderList); $i++) {
					$Result[] = imap_unsubscribe($this->inbox, $this->encodeFolderName($mailbox_prefix.$SubFolderList[$i]));
				}
				if (imap_renamemailbox($this->inbox, $mailbox_prefix.$this->encodeFolderName($sourceFolder), $mailbox_prefix.$this->encodeFolderName($newFolder))) {
					if (imap_subscribe($this->inbox, $this->encodeFolderName($mailbox_prefix.$newFolder))) {
						$SubFolderList = $this->Get_Folder_List_In_Path($newFolder,true,true);
						for($i=0; $i< sizeof($SubFolderList); $i++) {
							$Result[] = imap_subscribe($this->inbox, $this->encodeFolderName($mailbox_prefix.$SubFolderList[$i]));
						}
					}
				} else {
					$Result['RenameFolder'] = false;
				}
			}

			if (!$Result) $Result = array();
			if(!in_array(false,$Result) && $this->CacheMailToDB == true){
				$DataArr['Folder'] = $newFolder;
				$this->Manage_Cached_Mail("update",$sourceFolder,-1,$DataArr);
			}
			
			return !in_array(false,$Result);
        }

        // create at 20080506, by kenneth chung
        function Move_To_Trash($folderName, $uid) {
                $mailbox = $this->inbox;
                imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
				
				// Process Receipt records for sent mail copy
			//	$message_id= trim($this->Get_Header_Flag($this->Get_MessageID($uid),'message-id'));// comment out on 2011-04-13
				//$header_flags = $this->Get_Header_Flag($this->Get_MessageID($uid),'');
				$header_flags = $this->Get_Header_Flag($uid,'',FT_UID);
				$message_id = trim($header_flags['message-id']);
				if($message_id=='') // no message-id means is sent mail copy
				{
					if($folderName != $this->DefaultFolderList['TrashFolder'])
					{
						// Mark the TrashDate of Trashed mail
						$this->SetMailReceiptTrashMark($folderName,$uid,"DELETE","1");
					}else
					{
						// Delete receipt records
					//	$eClassMailID = trim($this->Get_Header_Flag($this->Get_MessageID($uid),'x-eclass-mailid'));// comment out on 2011-04-13
		                $eClassMailID = $header_flags['x-eclass-mailid'];
		                if($eClassMailID != ''){
		                	$this->DeleteMailReceipts($eClassMailID);// delete all receipt records
		                }
					}
				}
				
                if ($folderName != $this->RootPrefix."Trash"){
                	$Result['MoveResult'] = imap_mail_move($mailbox,$uid,$this->encodeFolderName($this->RootPrefix."Trash"),CP_UID);
                }
                else{
                	$Result['MoveResult'] = imap_delete($mailbox,$uid,FT_UID);
                }

                $Result['DeleteResult'] = imap_expunge($mailbox);
                
                if(!in_array(false,$Result) && $this->CacheMailToDB == true){
                	if($folderName != $this->TrashFolder)
                	{
	                	$moved_uid = $this->Get_Last_Msg_UID($this->TrashFolder);
	                	$DataArr['Folder'] = $this->TrashFolder;
	                	$DataArr['UID'] = $moved_uid;
	        			$this->Manage_Cached_Mail("update",$folderName,$uid,$DataArr);
                	}else{
                		$this->Manage_Cached_Mail("remove",$folderName,$uid);
                	}
                }
                
                return (!in_array(false,$Result));
        }

                        // edited at 20080506, by kenneth chung
      function deleteMail($folderName, $uid) {
            /* $this->removeMail() may do the same thing already */
            /* not tested with multi level folder */
            $mailbox = $this->inbox;
            imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

            $Result['DeleteResult'] = imap_delete($mailbox,$uid,FT_UID);

            $Result['ExpungeResult'] = imap_expunge($mailbox);
			
			if(!in_array(false,$Result) && $this->CacheMailToDB == true){
				$this->Manage_Cached_Mail("remove",$folderName,$uid);
			}
			
            return (!in_array(false,$Result));
      }

	// create by kenneth chung to undelete a mail
	function Undelete_Mail($folderName, $uid) {
		/* $this->removeMail() may do the same thing already */
		/* not tested with multi level folder */
		$mailbox = $this->inbox;
		imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
		return imap_clearflag_full($mailbox, $uid, '\\DELETED', ST_UID);
	}
	
	// By Michael - Set Unreadflag to mail
	function unreadMail($folderName, $uid) {
		$mailbox = $this->inbox;
		imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
		$result = imap_clearflag_full($mailbox, $uid, '\\SEEN', ST_UID);
		if($result && $this->CacheMailToDB == true){
			$DataArr['Seen'] = 0;
			$this->Manage_Cached_Mail("update",$folderName,$uid,$DataArr);
		}
		return $result;
	}
	
	// By Michael - Set Readflag to mail
	function readMail($folderName, $uid) {
		$mailbox = $this->inbox;
		imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
		$result = imap_setflag_full($mailbox, $uid, '\\SEEN', ST_UID);
		if($result && $this->CacheMailToDB == true){
			$DataArr['Seen'] = 1;
			$this->Manage_Cached_Mail("update",$folderName,$uid,$DataArr);
		}
		return  $result;
	}
        /*
          List the emails in the folder
          Param: folderName - listing the folder
                 startMsgNumber - retrieve email starting from startMsgNumber
                 count - how many to return
                 sort - sorting criteria
          Return: Array()
                    - Subject
                    - From
                    - To
                    - Date
                    - UID
                    - size
                    - msgno
                    - recent
                    - answered
                    - deleted
                    - seen
                    - [Pls check whether reply / forward / reply all]
        */
        function listMailInFolder($folderName, $startMsgNumber, $count, $sort="DateSort", $reverse=0, $Keyword="", $SearchField=array(), $FromDate="", $ToDate="")
        {
        	global $Lang;
        	//$time_start = microtime(true);
          /*echo $Keyword.'<br>';
          var_dump($SearchField).'<br>';*/
          /*echo $FromDate.'<br>';
          echo $ToDate.'<br>';
          echo $folderName.'<br>';*/
          /* refer to imap_fetch_overview(), imap_sort() */
          // Fetch an overview for all messages in INBOX
          //$startMsgNumber: start from 1
          //
          //e.g. listMailInFolder("INBOX", 1, 20, SORTDATE, 1);

                                        //$timestart = time();
          $mailbox = $this->inbox;
          imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
          //echo "imap reopen: " . (microtime(true) - $time_start) . "<br>\n";
          
          //echo "imap_reopen used ".$timespend =  time() - $timestart;

          //$timestart = time();
          //check the max msg number in the mailbox
          //$mailcount = imap_check($mailbox);
          //$MaxMsgNo = imap_num_msg($mailbox);
          
          //$FetchStartNumber = (($MaxMsgNo - ($startMsgNumber+$count)) < 0)? 1:($MaxMsgNo - ($startMsgNumber+$count) + 1);
	  
	  /*
          $FetchStartNumber = (($MaxMsgNo - ($startMsgNumber+$count)) < 0)? 1:($MaxMsgNo - ($startMsgNumber + $count) + 2);
          $FetchEndNumber = $MaxMsgNo - $startMsgNumber + 1;
          echo "MaxMsgNo: " . $MaxMsgNo . "<br>\n";
          echo "startMsgNumber: " . $startMsgNumber . "<br>\n";
          echo "count: " . $count . "<br>\n";
          echo "FetchStartNumber: " . $FetchStartNumber . "<br>\n";
          echo "FetchEndNumber: " . $FetchEndNumber . "<br>\n";
	  */
          $sortIndex = imap_sort($mailbox, SORTDATE, 1);
          //echo "imap sort: " . (microtime(true) - $time_start) . "<br>\n";
	  /*echo '<pre>';
	  var_dump($sortIndex);
	  echo '</pre>';
	  */
          //echo "imap_sort used ".$timespend =  time() - $timestart;

          //check do not exceed the max msg number, otherwise it'll return nothing
          $adjustedCount = ($startMsgNumber+$count-1 > sizeof($sortIndex)) ? sizeof($sortIndex) - $startMsgNumber + 1 : $count;

          //$sortIndex = imap_sort($mailbox, SORTDATE, $reverse);
          $msgList = array_slice($sortIndex, $startMsgNumber-1, $adjustedCount);
          for ($i=0; $i< sizeof($msgList); $i++) {
          	//$MsgFolderArray[] = array($folderName, $msgList[$i]);
          	$OverviewArray[] = $this->Get_Mail_In_Folder($folderName,$msgList[$i]);
          }
          	
          //$OverviewArray = $this->Get_Mail_Overview($MsgFolderArray);
          //echo "get mail overview: " . (microtime(true) - $time_start) . "<br>\n";
          
          /*//echo sizeof($sortIndex).'<br>';
          //echo $startMsgNumber.' '.$adjustedCount.'<br>';
          //echo sizeof($msgList).'<br>';
          //echo '<pre>'; var_dump($sortIndex); echo '</pre>';
          //echo '<pre>'; var_dump($msgList); echo '</pre>';
          //$result = imap_fetch_overview($mailbox, implode("," , $sortIndex), 0);
          //$timestart = time();
          $result = imap_fetch_overview($mailbox, implode("," , $msgList), 0);
          //$result = imap_fetch_overview($mailbox, $FetchStartNumber.":".$FetchEndNumber, 0);
          //echo $FetchStartNumber.":".$FetchEndNumber."<br>";
          //echo "imap_fetch_overview used ".$timespend =  time() - $timestart;
          //$result = imap_fetch_overview($mailbox, "1",0);
          //echo sizeof($result);

          //$timestart = time();
          if (sizeof($result) != 0) {
            for ($i=0; $i< sizeof($result); $i++) {
              $TempObj = $result[$i];
              unset($DisplayFrom);
              $FromElement = imap_mime_header_decode($TempObj->from);
              for ($j=0; $j< sizeof($FromElement); $j++) {
                                                                $DisplayFrom .= $this->Conver_Encoding($FromElement[$j]->text,"UTF-8", $FromElement[$j]->charset);
                      if ($j==0) $DisplayFrom .= " ";
              }
              $OverviewArray[$i]['From'] = $DisplayFrom;

              unset($DisplayTo);
              $ToElement = imap_mime_header_decode($TempObj->to);
              if (sizeof($ToElement) == 0) {
              	$header = imap_fetchheader($mailbox, $TempObj->msgno);
              	$RawHeader = imap_rfc822_parse_headers($header);
              	$ToElement = $RawHeader->to;
              	
              	if (sizeof($ToElement) > 0) {
              		if (!isset($ToElement[0]->personal)) $DisplayName = $ToElement[0]->mailbox."@".$ToElement[0]->host;
									else {
										$DisplayObject = imap_mime_header_decode(imap_utf8($ToElement[0]->personal));
										$DisplayName = $DisplayObject[0]->text;
									}
              		
					        $OverviewArray[$i]['To'] = $DisplayName."...";
              		//$OverviewArray[$i]['To'] = $Lang['email']['MultiReceiver'];
              	}
              	else {
              		$OverviewArray[$i]['To'] = $Lang['email']['NoReceiver'];
              	}
							}
							else {
	              for ($j=0; $j< sizeof($ToElement); $j++) {
	                $DisplayTo .= $this->Conver_Encoding($ToElement[$j]->text,"UTF-8", $ToElement[$j]->charset);
	                if ($j==0) $DisplayTo .= " ";
	              }
	              $OverviewArray[$i]['To'] = $DisplayTo;
	            }
              

              // trim the mini second for the date
              $Date = substr($TempObj->date, 0, -5);
              $OverviewArray[$i]['DateReceived'] = $Date;
              $TempDate = explode(',',$TempObj->date);
              $DayOfWeek = $TempDate[0];
              $DateTime = explode(' ',trim($TempDate[1]));
              list($Day,$Month,$Year,$Time,$Zone) = $DateTime;
              list($Hour,$Min,$Sec) = explode(':',$Time);
              $SortTime = mktime($Hour,$Min,$Sec,Get_Month_Number($Month),$Day,$Year);
              $OverviewArray[$i]['DateSort'] = $SortTime;

              // Get Priority
              $MailPriority = $this->Get_Header_Flag($TempObj->msgno,"x-priority");
              $OverviewArray[$i]['Priority'] = $MailPriority;

              // replace NO SUBJECT if subject is empty
              $SubjectObj = imap_mime_header_decode(imap_utf8($TempObj->subject));
              unset($subtmp);
              for($k=0;$k<count($SubjectObj);$k++) {
                                                                $subtmp .= $SubjectObj[$k]->text;
                                                        }
              $Subject = ($subtmp == "")? "NO SUBJECT":$subtmp;
              $OverviewArray[$i]['Subject'] = $Subject;

              // check if mail had attachment
              $ContentType = $this->Get_Header_Flag($TempObj->msgno,'Content-Type');
              $OverviewArray[$i]['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType) == 0)? false:true;
              //$OverviewArray[$i]['HaveAttachment'] = false;
              //$MailStructure = $this->Get_Mail_Structure($folderName,$TempObj->uid);
              //$MailStructure = imap_fetchstructure($this->inbox,$TempObj->uid,FT_UID);
              //$MailParts = $MailStructure->parts;
              //for ($j=0; $j< sizeof($MailParts); $j++) {
              //        if ($MailParts[$j]->disposition == "attachment") {
              //                $OverviewArray[$i]['HaveAttachment'] = true;
              //                break;
              //        }
              //}

              // Get UID
              $OverviewArray[$i]['UID'] = $TempObj->uid;

              // Get Mail size
              $OverviewArray[$i]['Size'] = round(($TempObj->size/1024),2);

              // Get Flag
              $OverviewArray[$i]["recent"] = $TempObj->recent;
              $OverviewArray[$i]["flagged"] = $TempObj->flagged;
              $OverviewArray[$i]["answered"] = $TempObj->answered;
              $OverviewArray[$i]["deleted"] = $TempObj->deleted;
              $OverviewArray[$i]["seen"] = $TempObj->seen;
              $OverviewArray[$i]["draft"] = $TempObj->draft;

              // Get Folder Name
              $OverviewArray[$i]["Folder"] = $folderName;*/

              /*// Get Mail Body if needed
              if (in_array('Body',$SearchField)) {
                      $structure = imap_fetchstructure($mailbox, $TempObj->msgno);
                      $OverviewArray[$i]["Body"] = $this->retrieve_textmessage($mailbox, $TempObj->msgno, $structure);
              }

              if (in_array('Cc',$SearchField)) {
                      $header = imap_header($mailbox, $TempObj->msgno);
                      $elements=imap_mime_header_decode(imap_utf8($header->ccaddress));
                      unset($subtmp);
                      for($k=0;$k<count($elements);$k++) {
                              $subtmp .= $elements[$k]->text;
                                                                                        }
                      $OverviewArray[$i]["Cc"] = $subtmp;
              }
                                                }
                                                //echo '<pre>';
                                                //var_dump($OverviewArray);
                                                //echo '</pre>';

                                                // is Search action
                                                if (($Keyword != "" && sizeof($SearchField) > 0) || ($FromDate!="" && $ToDate!="")) {
                                                  $FromYear = substr($FromDate,0,4);
                                                  $FromMonth = substr($FromDate,5,2);
                                                  $FromDay = substr($FromDate,8,2);
                                                  $ToYear = substr($ToDate,0,4);
                                                  $ToMonth = substr($ToDate,5,2);
                                                  $ToDay = substr($ToDate,8,2);

                                                  $FromTime = mktime(0,0,0,$FromMonth,$FromDay,$FromYear);
                                                  $ToTime = mktime(0,0,0,$ToMonth,$ToDay,$ToYear);
                                                  for ($i=0; $i< sizeof($OverviewArray); $i++) {
                                                          $found = false;
                                                          if ($Keyword != "" && sizeof($SearchField) > 0) {
                                                                  for ($j=0; $j< sizeof($SearchField); $j++) {
                                                                          if (stristr($OverviewArray[$i][$SearchField[$j]],$Keyword) != false) {
                                                                                  $found = true;
                                                                                  break;
                                                                          }
                                                                  }
                                                          }
                                                          if ($FromDate!="" && $ToDate!="") {
                                                                  $found = ($FromTime < $OverviewArray[$i]['DateSort'] && $OverviewArray[$i]['DateSort'] < $ToTime)? true:false;
                                                          }

                                                          if ($found) $FinalArray[] = $OverviewArray[$i];
                                                  }
                                                  if (!is_array($FinalArray)) {
                                                          $FinalArray = array();
                                                  }
                                                }
                                            
                                              else {*/
					if (sizeof($OverviewArray) > 0) {
            // Sort the result array
            switch ($sort) {
                    case "DateSort":
                            $RealSort = "DateSort";
                            break;
                    case "From":
                            $RealSort = "From";
                            break;
                    case "Subject":
                            $RealSort = "Subject";
                            break;
                    case "Size":
                            $RealSort = "Size";
                            break;
                    case "To":
                            $RealSort = "To";
                            break;
                    default:
                            $RealSort = "DateSort";
                            break;
            }
						
            foreach ($OverviewArray as $pos =>  $val)
                    $tmp_array[$pos] = $val[$RealSort];

            if ($reverse == 0)
                    uasort($tmp_array,"String_To_Lower_Array_Cmp");
            else
                    uasort($tmp_array,"String_To_Lower_Array_Cmp_Reverse");
            //echo "asort: " . (microtime(true) - $time_start) . "<br>\n";

            // display however you want
            foreach ($tmp_array as $pos =>  $val) {
           	 	$FinalArray[] = $OverviewArray[$pos];
           	}
			//echo "final array: " . (microtime(true) - $time_start) . "<br>\n";

            //$FinalArray = array_slice($FinalArray, $startMsgNumber-1, $adjustedCount);
            //}
          }
          else {
                  $FinalArray = array();
          }
                  /*if (count($result) > 0) {
                  $sortedResult = array(count($result));
                  for($i=0; $i < count($result); $i++)
                  {
                    $sortedResult[array_search($result[$i]->msgno, $msgList)] = $result[$i];
                  }
                  }*/

          //echo "message process time used ".$timespend =  time() - $timestart;
          return $FinalArray;
        }

        /*
          Get decoded mail part (attachment)
          Param: folderName, UID, part number
          Return: part content
        */
        function getMailPart($folderName, $uid, $partNumber)
        {
                        /* To-DO: */
                        $msgno = -1;
                        $mailbox = $this->inbox;
                        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
                        $mailcount = imap_check($mailbox);
                        $mails = imap_fetch_overview($mailbox, "1:".$mailcount->Nmsgs, 0);

                        for($i=0; $i < count($mails); $i++)
                        {
                                if ($mails[$i]->uid == $uid) $msgno = $mails[$i]->msgno;
                        }

                        if ($msgno != -1)
                        {
                                $structure = imap_fetchstructure($mailbox, $msgno); //have to use msg no here, instead of $uid
                                $parts = $structure->parts;

                                return $parts[$partNumber];
                        }
                        else
                        {
                                return null;
                        }
                        //type
                        //encoding
                        //size
                        //disposition
                        //
                        //
                        //

        }

                        // edit by kenneth chung at 20080507
      function Move_Mail($sourceFolder, $uid, $destinationFolder) {
        $mailbox = $this->inbox;
        
	      imap_reopen($mailbox,$this->getMailServerRef().$this->encodeFolderName($sourceFolder));
	      
	      // Process receipt records for sent mail copy
	      //$message_id = trim($this->Get_Header_Flag($this->Get_MessageID($uid),'message-id'));
	      $message_id = trim($this->Get_Header_Flag($uid,'message-id',FT_UID));
	      if($message_id=='')// no message-id means is sent mail copy
	      {
		      if($destinationFolder!=$this->DefaultFolderList['TrashFolder'] && $destinationFolder != $this->DefaultFolderList['SpamFolder']){
			  	// Unmark the TrashDate of Trash and Junk mail
				$this->SetMailReceiptTrashMark($sourceFolder,$uid,"");
			  }else if($destinationFolder==$this->DefaultFolderList['TrashFolder'] || $destinationFolder == $this->DefaultFolderList['SpamFolder']){
				// Mark the TrashDate of Trash and Junk mail
				$type = $destinationFolder==$this->DefaultFolderList['TrashFolder']?"1":"2";// 1= Trash; 2 = Spam
				$this->SetMailReceiptTrashMark($sourceFolder,$uid,"DELETE",$type);
			  }
	      }
	      
	      $Result['MoveResult'] = imap_mail_move($mailbox,$uid,$this->encodeFolderName($destinationFolder),CP_UID);
	      $Result['DeleteResult'] = imap_expunge($mailbox);
			
			if(!in_array(false,$Result) && $this->CacheMailToDB == true){
				$moved_uid = $this->Get_Last_Msg_UID($destinationFolder);
				$DataArr['Folder'] = $destinationFolder;
				$DataArr['UID'] = $moved_uid;
				$DataArr['Deleted'] = 0;
        		$this->Manage_Cached_Mail("update",$sourceFolder,$uid,$DataArr);
			}
		
	      return (!in_array(false,$Result));
      }

		function Copy_Mail($sourceFolder, $uid, $destinationFolder) {
        $mailbox = $this->inbox;
        
	      imap_reopen($mailbox,$this->getMailServerRef().$this->encodeFolderName($sourceFolder));
	
	      $Result['CopyResult'] = imap_mail_copy($mailbox,$uid,$this->encodeFolderName($destinationFolder),CP_UID);
			
		  if(!in_array(false,$Result) && $this->CacheMailToDB == true){
		  		$copy_uid = $this->Get_Last_Msg_UID($destinationFolder);
        		$this->Manage_Cached_Mail("add",$destinationFolder,$copy_uid);
		  }
			
	      return (!in_array(false,$Result));
      }
      
        /* Return whole email with raw header */
        function getMailRawHeader($folderName, $uid)
        {
                $mailbox = $this->inbox;
                        imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

                $header = imap_fetchheader($mailbox, $uid, FT_UID);
                           $returnObj = imap_rfc822_parse_headers($header);
                           return $returnObj;
        }

        /*
          Search email on mail folder
          Param: folderName, criteria
          Return: Array() of entries
                    - Array()
                      - FolderName, UID, Subject, From, To
        */
    
	# Parameters: folder list, from, to, cc, subject, dateFrom, dateEnd
	# Advance Search will search all input parameters with AND condition
	# Simple Search will search 3 parameters From, To, Subject only with OR condition
	### refer to imap_search(). Cross folder may be done by PHP algorithm
    function Search_Mail($folderName, $key_from="", $key_to="", $key_cc="", $key_bcc="", $key_subject="", $key_dateFrom="", $key_dateTo="", $key_body="", $key_attachment="", $IsAdvanceSearch=1,$seen="ALL", $SearchInfo=array()){
	    global $SYS_CONFIG;
	    
	    $searchCriteria = "";
	    $mailMsgNos = array();
	    
	    //$searchCriteria .= 'CHARSET "iso-8859-1" ';
	    /*$SearchEncoding = array("big5","UTF-8","iso-8859-1");

	    for ($i=0; $i< sizeof($SearchEncoding); $i++) {
	    	if ($SearchEncoding[$i] == "UTF-8") {
		    	$Subject = $key_subject;
		    	$Content = $key_content;
		    	$From = $key_from;
		    	$To = $key_recipient;
		    }
		    else {
		    	$Subject = mb_convert_encoding($key_subject,$SearchEncoding[$i],"UTF-8");
		    	$Content = mb_convert_encoding($key_content,$SearchEncoding[$i],"UTF-8");
		    	$From = mb_convert_encoding($key_from,$SearchEncoding[$i],"UTF-8");
		    	$To = mb_convert_encoding($key_recipient,$SearchEncoding[$i],"UTF-8");
		    }
	    	
	    	if ($key_subject != "") $searchCriteria .= 'SUBJECT "' . $Subject . '" ';
		    //if ($key_content != "") $searchCriteria .= '(BODY "' . $Content . '") ';
		    //if ($key_from != "") $searchCriteria .= '(FROM "' . $From . '") ';
		    //if ($key_recipient != "") $searchCriteria .= '(TO "' . $To . '") ';
		    //if ($key_attachmentFilename != "") $searchCriteria .= '(SUBJECT "' . $key_attachmentFilename . '") ';
		    //if ($key_dateFrom != "") $searchCriteria .= '(SINCE "' . $key_dateFrom . '") ';
		    //if ($key_dateTo != "") $searchCriteria .= '(BEFORE "' . $key_dateTo . '") ';
		    
		    $mailbox = $this->inbox;
	    	imap_reopen($mailbox,$this->getMailServerRef().$this->encodeFolderName($folderName));
	    	
	    	$ResultMsgList = imap_search($mailbox,$searchCriteria,0,$SearchEncoding[$i]);
	    	echo $Subject.'<br>';
	    	echo '<pre>';
	    	var_dump($ResultMsgList);
	    	echo '</pre>';
	    	if ($ResultMsgList !== FALSE) 
	    		$mailMsgNos = array_merge($mailMsgNos,$ResultMsgList);
	    }
	    
	    echo sizeof($mailMsgNos).'<br>';
	    $mailMsgNos = array_unique($mailMsgNos);
	    echo sizeof($mailMsgNos).'<br>';
	    die;*/
		
		# get the imap object
	    $mailbox = $this->inbox;
		# reopen the imap obj to current mailbox
	    imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
	    
	    # skip searching if no mail in mailbox
	    if(imap_num_msg($mailbox)==0)
	    	return null;
		
		# Imap Search 
		if($IsAdvanceSearch == 1){
			## Advance Search - AND condition
			$dateFrom = ($key_dateFrom != "") ? $this->Get_Date_Of_Imap_Search($key_dateFrom) : '';
			// BEFORE DATE need to +1 day
			$dateTo   = ($key_dateTo != "") ? $this->Get_Date_Of_Imap_Search(date("Y-m-d",strtotime($key_dateTo)+86400)) : '';

			//$encoding_arr = array( "UTF-8",  "BIG5", "GB2312","GBK");
			$encoding_arr = array( "UTF-8",  "BIG5");
			if($SYS_CONFIG['Mail']['SearchGBMail']){
				$encoding_arr[] = "GB2312";
				$encoding_arr[] = "GBK";
			}
			foreach($encoding_arr as $thisEncoding)
			{
				$searchCriteria = '';
				## search fields do not need to be encoded
				$searchCriteria .= ($key_dateFrom != "") ? 'SINCE "'.$dateFrom.'" ' : '';
				$searchCriteria .= ($key_dateTo != "") ? 'BEFORE "'.$dateTo.'" ' : '';
				
				## search fields need to be encoded								
				$searchCriteria .= ($key_subject != "") ? 'SUBJECT "'.Convert_Encoding($key_subject,$thisEncoding,"UTF-8").'" ' : '';
				$searchCriteria .= ($key_from != "") ? 'FROM "'.Convert_Encoding($key_from,$thisEncoding,"UTF-8").'" ' : '';
				$searchCriteria .= ($key_to != "") ? 'TO "'.Convert_Encoding($key_to,$thisEncoding,"UTF-8") .'" ' : '';
				$searchCriteria .= ($key_cc != "") ? 'CC "'.Convert_Encoding($key_cc,$thisEncoding,"UTF-8") .'" ' : '';
				$searchCriteria .= ($key_bcc != "") ? 'BCC "'.Convert_Encoding($key_bcc,$thisEncoding,"UTF-8") .'" ' : '';
				$searchCriteria .= ($key_body != "") ? 'BODY "'.Convert_Encoding($key_body,$thisEncoding,"UTF-8").'" ' : '';
				$searchCriteria .= ($seen != "") ? $seen : '';
				
				if (trim($searchCriteria) != '') {
					//$Temp1 = imap_search($mailbox, $searchCriteria, SE_FREE, $thisEncoding);
					$Temp1 = imap_search($mailbox, $searchCriteria, SE_FREE, "UTF-8");
					if(!is_array($Temp1)){
						$Temp1 = array();
					}
				}
				
				if (trim($key_attachment) != "") {
					$Temp2 = $this->Search_Attachment(Convert_Encoding($key_attachment,$thisEncoding,"UTF-8"));
					if(!is_array($Temp2)){
						$Temp2 = array();
					}
				}
				
				if (is_array($Temp1) && is_array($Temp2)) {
					//$Temp = array_merge($Temp1,$Temp2);
					$Temp = array_values(array_intersect($Temp2, $Temp1));
				} else if (is_array($Temp1) && !is_array($Temp2)) {
					$Temp = $Temp1;
				} else if (!is_array($Temp1) && is_array($Temp2)) {
					$Temp = $Temp2;
				}
				
				if(is_array($Temp))
				{
					$mailMsgNos = array_merge($mailMsgNos,$Temp);
				}
			}
			
				
			$mailMsgNos = ($mailMsgNos != false) ? array_unique($mailMsgNos) : false;
			
		} else {
			## Simple Search - OR condition
			
			// search by using different encoding of search value
			//$encoding_arr = array( "UTF-8", "ASCII", "Windows-1252", "ISO-8859-1", "BIG5", "GB2312","GBK");
			
			if(mb_detect_encoding($key_subject)=="ASCII")
				$encoding_arr = array("UTF-8");
			else{
				//$encoding_arr = array("UTF-8", "BIG5", "GB2312", "GBK");
				$encoding_arr = array( "UTF-8",  "BIG5");
				if($SYS_CONFIG['Mail']['SearchGBMail']){
					$encoding_arr[] = "GB2312";
					$encoding_arr[] = "GBK";
				}
			}
			
			$dateFrom = ($key_dateFrom != "") ? $this->Get_Date_Of_Imap_Search($key_dateFrom) : '';
			// BEFORE DATE need to +1 day
			//$dateTo   = ($key_dateTo != "") ? $this->Get_Date_Of_Imap_Search(date("Y-m-d",strtotime($key_dateTo)+86400)) : '';
			$dateTo   = ($key_dateTo != "") ? $this->Get_Date_Of_Imap_Search($key_dateTo) : '';
			
			foreach($encoding_arr as $thisEncoding)
			{
				# Subject 
				if(trim($key_subject)!=''){
					$subject_crit = 'SUBJECT "'.Convert_Encoding($key_subject,$thisEncoding,"UTF-8").'" '; 
					$subject_crit .= $seen; 
					
					$subject_crit .= ($key_dateFrom != "") ? ' SINCE "'.$dateFrom.'" ' : '';
					$subject_crit .= ($key_dateTo != "") ? ' BEFORE "'.$dateTo.'" ' : '';
					
					//$Temp = imap_search($mailbox, $subject_crit, SE_FREE, $thisEncoding);
					$Temp = imap_search($mailbox, $subject_crit, SE_FREE, "UTF-8");
					if ($Temp !== false) $mailMsgNos = array_merge($mailMsgNos, $Temp);
				}
				# From
				if(trim($key_from)!=''){
					$from_crit = 'FROM "'.Convert_Encoding($key_from,$thisEncoding,"UTF-8").'" ';
					$from_crit .= $seen; 
					
					$from_crit .= ($key_dateFrom != "") ? ' SINCE "'.$dateFrom.'" ' : '';
					$from_crit .= ($key_dateTo != "") ? ' BEFORE "'.$dateTo.'" ' : '';
					
					//$Temp = imap_search($mailbox, $from_crit, SE_FREE, $thisEncoding);
					$Temp = imap_search($mailbox, $from_crit, SE_FREE, "UTF-8");
					if ($Temp !== false) $mailMsgNos = array_merge($mailMsgNos, $Temp);
				}
				# To
				if(trim($key_to)!=''){
					$to_crit = 'TO "'.Convert_Encoding($key_to,$thisEncoding,"UTF-8").'" ';
					$to_crit .= $seen; 
					
					$to_crit .= ($key_dateFrom != "") ? ' SINCE "'.$dateFrom.'" ' : '';
					$to_crit .= ($key_dateTo != "") ? ' BEFORE "'.$dateTo.'" ' : '';
					
					//$Temp = imap_search($mailbox, $to_crit, SE_FREE, $thisEncoding);
					$Temp = imap_search($mailbox, $to_crit, SE_FREE, "UTF-8");
					if ($Temp !== false) $mailMsgNos = array_merge($mailMsgNos,$Temp);
				}
			}
			# remove duplicate 
			$mailMsgNos = array_unique($mailMsgNos);
			
		}
		
		
	    //echo 'Total Messages Found: '.sizeof($mailMsgNos).'<br>';
	    //echo 'Over All used Time for '.$folderName.': '. (time() - $OverAllTime).'<br><br>';
	    /*
	    echo "Criteria: $searchCriteria <br>\n";
	    echo 'UTF-8: '.sizeof($mailMsgNos).'<br>';
	    $mailMsgNos = imap_search($mailbox,$searchCriteria,0,'big5');
	    echo 'BIG5: '.sizeof($mailMsgNos).'<br>';
	    echo 'Used Time: '. (time() - $time); die;
		*/
	    
	
		if ($mailMsgNos === false){ 
			return null; 
		}

		foreach ( $mailMsgNos as $key => $value){
			$ReturnArray[] = array($folderName, $value);
		}
		
	    return $ReturnArray;
    }
    
    function Get_Mail_Overview($MsgNoArray) {
    	for ($i=0; $i< sizeof($MsgNoArray); $i++) {
    		if ($MsgNoArray[$i][0] != $MsgNoArray[$i-1][0] || $i == 0) {
    			$this->Go_To_Folder($MsgNoArray[$i][0]);
    		}
    		
    		$result = imap_fetch_overview($this->inbox, $MsgNoArray[$i][1]);
    		
    		$TempObj = $result[0];
        unset($DisplayFrom);
        $FromElement = imap_mime_header_decode($TempObj->from);
        for ($j=0; $j< sizeof($FromElement); $j++) {
           $DisplayFrom .= $this->Conver_Encoding($FromElement[$j]->text,"UTF-8", $FromElement[$j]->charset);
           if ($j==0) $DisplayFrom .= " ";
        }
        $OverviewArray[$i]['From'] = $DisplayFrom;

        unset($DisplayTo);
        $ToElement = imap_mime_header_decode($TempObj->to);
        /*echo '<pre>';
      	var_dump($ToElement);
      	echo '</pre>';die;*/
        if (sizeof($ToElement) == 0) {
        	$header = imap_fetchheader($this->inbox, $TempObj->msgno);
        	$RawHeader = imap_rfc822_parse_headers($header);
        	$ToElement = $RawHeader->to;
        	
        	if (sizeof($ToElement) > 0) {
        		if (!isset($ToElement[0]->personal)) $DisplayName = $ToElement[0]->mailbox."@".$ToElement[0]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($ToElement[0]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
        		
		        $OverviewArray[$i]['To'] = $DisplayName."...";
        		//$OverviewArray[$i]['To'] = $Lang['email']['MultiReceiver'];
        	}
        	else {
        		$OverviewArray[$i]['To'] = $Lang['email']['NoReceiver'];
        	}
				}
				else {
          for ($j=0; $j< sizeof($ToElement); $j++) {
            $DisplayTo .= $this->Conver_Encoding($ToElement[$j]->text,"UTF-8", $ToElement[$j]->charset);
            if ($j==0) $DisplayTo .= " ";
          }
          $OverviewArray[$i]['To'] = $DisplayTo;
        }
        

        // trim the mini second for the date
        $Date = substr($TempObj->date, 0, -5);
        $OverviewArray[$i]['DateReceived'] = $Date;
        $TempDate = explode(',',$TempObj->date);
        $DayOfWeek = $TempDate[0];
        $DateTime = explode(' ',trim($TempDate[1]));
        list($Day,$Month,$Year,$Time,$Zone) = $DateTime;
        list($Hour,$Min,$Sec) = explode(':',$Time);
        $SortTime = mktime($Hour,$Min,$Sec,Get_Month_Number($Month),$Day,$Year);
        $OverviewArray[$i]['DateSort'] = $SortTime;

        // Get Priority
        $header_flags = $this->Get_Header_Flag($TempObj->msgno,"");
    //    $MailPriority = $this->Get_Header_Flag($TempObj->msgno,"x-priority");
    	$MailPriority = $header_flags['x-priority'];
        $OverviewArray[$i]['Priority'] = $MailPriority;

        // replace NO SUBJECT if subject is empty
        $SubjectObj = imap_mime_header_decode(imap_utf8($TempObj->subject));
        unset($subtmp);
        for($k=0;$k<count($SubjectObj);$k++) {
        	$subtmp .= $SubjectObj[$k]->text;
        }
        $Subject = ($subtmp == "")? "NO SUBJECT":$subtmp;
        $OverviewArray[$i]['Subject'] = $Subject;
        //$OverviewArray[$i]['Subject'] = $TempObj->subject;

        // check if mail had attachment
    //    $ContentType = $this->Get_Header_Flag($TempObj->msgno,'Content-Type');
    	$ContentType = $header_flags['content-type'];
        $OverviewArray[$i]['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType) == 0)? false:true;
        //$OverviewArray[$i]['HaveAttachment'] = false;
        //$MailStructure = $this->Get_Mail_Structure($folderName,$TempObj->uid);
        /*$MailStructure = imap_fetchstructure($this->inbox,$TempObj->uid,FT_UID);
        $MailParts = $MailStructure->parts;
        for ($j=0; $j< sizeof($MailParts); $j++) {
                if ($MailParts[$j]->disposition == "attachment") {
                        $OverviewArray[$i]['HaveAttachment'] = true;
                        break;
                }
        }*/

        // Get UID
        $OverviewArray[$i]['UID'] = $TempObj->uid;

        // Get Mail size
        $OverviewArray[$i]['Size'] = round(($TempObj->size/1024),2);

        // Get Flag
        $OverviewArray[$i]["recent"] = $TempObj->recent;
        $OverviewArray[$i]["flagged"] = $TempObj->flagged;
        $OverviewArray[$i]["answered"] = $TempObj->answered;
        $OverviewArray[$i]["deleted"] = $TempObj->deleted;
        $OverviewArray[$i]["seen"] = $TempObj->seen;
        $OverviewArray[$i]["draft"] = $TempObj->draft;

        // Get Folder Name
        $OverviewArray[$i]["Folder"] = $MsgNoArray[$i][0];
    	}
	    
	    return $OverviewArray;
    }
/*
        function checkMailFolderStatus($folderName)
        {

                $status = @imap_status($this->inbox, $this->getMailServerRef().imap_utf7_encode($folderName), SA_ALL);
                $msg = "";
                    if ($status) {
                        $msg .= "Mailbox: '$folderName' has the following status:<br />\n";
                        $msg .= "Messages:   " . $status->messages    . "<br />\n";
                        $msg .= "Recent:     " . $status->recent      . "<br />\n";
                        $msg .= "Unseen:     " . $status->unseen      . "<br />\n";
                        $msg .= "UIDnext:    " . $status->uidnext     . "<br />\n";
                        $msg .= "UIDvalidity:" . $status->uidvalidity . "<br />\n";
                    }

                    return $msg;
        }
        */

                                function getMailDisplayFolder($FullPath) {
                                	global $Lang;
                                		if (strrpos($FullPath,".") !== false) {
                                            $FolderDisplayName = substr($FullPath,strrpos($FullPath,".")+1, strlen($FullPath)-1);
                                                
                                        }
                                        else {
                                                $FolderDisplayName = $FullPath;
                                        }
                                        if(in_array($FullPath,$this->DefaultFolderList))
                                        	$FolderDisplayName = $Lang['Gamma']['SystemFolderName'][$FolderDisplayName];
                                        	
                                        return $FolderDisplayName;
                                }
								
                                // get folder Structure for UI use
                                function Get_Folder_Structure($Shift=false,$IncludeDefaultFolder=true,$printLevelSpaces=0) {
                                        Global $Lang, $SYS_CONFIG;
										
										//if(isset($this->AllFolderList) && sizeof($this->AllFolderList)>0){
										//	$FolderList = $this->AllFolderList;
										//}else{
                                        //	$FolderList = $this->getMailFolders();
                                        //	$this->AllFolderList = $FolderList;
										//}
										$FolderList = $this->getAllFolderList();
                                        //sort($FolderList,SORT_STRING);
                                        //usort($FolderList,"String_To_Lower_Array_Cmp");
                                        /*echo '<pre>';
                                        var_dump($FolderList);
                                        echo '</pre>';*/

                                        $FolderArray[] = array($Lang['email']['MoveToFolder'],'');
                                        if ($IncludeDefaultFolder) {
											// please default folder list at top
											$FolderArray[] = array($Lang['email']['DefaultFolderList'],'Default','LabelGroupStart');
											foreach ($this->DefaultFolderList as $Key => $Val) {
													$FolderArray[] = array($this->getMailDisplayFolder($Val),$Val);
											}
											$FolderArray[] = array('','','LabelGroupEnd');
											$FolderArray[] = array($Lang['email']['PersonalFolderList'],'Personal','LabelGroupStart');
										}

                                        for ($i=0; $i< sizeof($FolderList); $i++) {
                                            // exclude all default folder
											$defaultFolderList = $this->DefaultFolderList;
											foreach ($defaultFolderList as $Key => $Val) {
												if ($FolderList[$i] == $this->FolderPrefix) {
													continue 2;
												}
												//if (stristr($FolderList[$i],$defaultFolderList[$j]) !== FALSE && $defaultFolderList[$j] != $SYS_CONFIG['Mail']['FolderPrefix']) {
												if ($FolderList[$i] == $Val && $Val != $this->FolderPrefix) {
													continue 2;
												}
											}

											$Level = substr_count($FolderList[$i],$this->folderDelimiter);
											if($printLevelSpaces==1)
											{
												$LevelSpaces = "";
												for ($j=0; $j< $Level; $j++) {
	                                                $LevelSpaces .= "&nbsp;&nbsp;";
												}
											}
											$FolderDisplayName = $this->getMailDisplayFolder($FolderList[$i]);

											$FolderArray[] = array($LevelSpaces.$FolderDisplayName,$FolderList[$i]);
                                        }

																				if ($IncludeDefaultFolder) {
                                          $FolderArray[] = array('','','LabelGroupEnd');
                                        }
                                        if ($Shift) {
                                        	array_shift($FolderArray);
                                        }
										
                                        return $FolderArray;
                                }

                                // function to convert plain text subject to mail server's format
/* commented by Marcus, use imap_8bit/quote_printable_encode/=?UTF-8?Q?.....?= to encode chinese characters seems to be more difficult to decode, 
 * changed to encode by base64 method below
                              function Get_Mail_Format($string, $encoding='UTF-8') {

                            $string = str_replace(" ", "_", trim($string)) ;
                            // We need to delete "=\r\n" produced by imap_8bit() and replace '?'
                            
                            $string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;

                            // Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
                            $string = chunk_split($string, 73);
                            
                            // We also have to remove last unneeded \r\n :
                            $string = substr($string, 0, strlen($string)-2);
                            // replace newlines with encoding text "=?UTF ..."
                            $string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?Q?", $string) ;

                            $encoded_string =  '=?'.$encoding.'?Q?'.$string.'?=';
                            
                           
							
                            return $encoded_string;
                                }
*/
						 function Get_Mail_Format($string, $encoding='UTF-8') {

                            //$string = str_replace(" ", "_", trim($string)) ;
                            // We need to delete "=\r\n" produced by imap_8bit() and replace '?'
                            $string = (base64_encode($string));
                            //$string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;

                            // Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
                            $string = chunk_split($string, 72);
                            
                            // We also have to remove last unneeded \r\n :
                            $string = substr($string, 0, strlen($string)-2);
                            // replace newlines with encoding text "=?UTF ..."
                            $string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?B?", $string) ;

                            $encoded_string =  '=?'.$encoding.'?B?'.$string.'?=';
                            
                            //debug_pr(imap_mime_header_decode($encoded_string));
							
                            return $encoded_string;
                                }
                                // Get structure of a mail
                                function Get_Mail_Structure($Folder,$UID) {
                                        imap_reopen($this->inbox, $this->getMailServerRef().$this->encodeFolderName($Folder));

                                        return imap_fetchstructure($this->inbox,$UID,FT_UID);
                                }

        function Send_Mail_By_PHPMailer($To=array(),$Cc=array(),$Bcc=array(),$Subject="",$Body="",$AttachList=array()) {
                /* ----- PHP Mailer ----- */
                $lmailer = new PHPMailer();
                $lmailer->IsSMTP();
                $lmailer->Host = $this->host;
                $lmailer->SMTPAuth = true;
                $lmailer->Username = $this->CurUserAddress;
                $lmailer->Password = $this->CurUserPassword;

                $lmailer->From = $this->CurUserAddress;
                $lmailer->FromName = '';

                // config reply-to
                $lmailer->AddReplyTo($this->CurUserAddress);

                // config To
                for ($i=0; $i< sizeof($To); $i++) {
                        $lmailer->AddAddress($To[$i]);
                }

                // config CC
                for ($i=0; $i< sizeof($Cc); $i++) {
                        $lmailer->AddCC($Cc[$i]);
                }

                // config BCC
                for ($i=0; $i< sizeof($Bcc); $i++) {
                        $lmailer->AddBCC($Bcc[$i]);
                }

                // add attachment
                for ($i=0; $i< sizeof($AttachList); $i++) {
                        $lmailer->AddAttachment($AttachList[$i][2],$AttachList[$i][1]);
                }

                $lmailer->Subject = $Subject;
                $lmailer->Body = $Body;
                $lmailer->AltBody = $Body;
                $lmailer->IsHTML(true);

                return $lmailer->Send();
        }

        //get Mail Importance Flag/ content type
        function Get_Header_Flag($MsgNo,$Flag, $Options=0) {
			$Flag = strtolower($Flag);
			$TextHeader = imap_fetchheader($this->inbox, $MsgNo, $Options);
							
			$TempHeader = trim(str_replace(array("\r\n", "\n\t", "\n "),array("\n", ' ', ' '), $TextHeader));
			$TempHeader = explode("\n" , $TempHeader);
			for ($i=0; $i< sizeof($TempHeader); $i++) {
				$pos = strpos($TempHeader[$i], ':');
				if ($pos > 0) {
					$field = strtolower(substr($TempHeader[$i], 0, $pos));
					if (!strstr($field,' ')) { /* valid field */
						$value = trim(substr($TempHeader[$i], $pos+1));
						switch($field) {
							case 'x-priority':
								$aMsg['x-priority'] = ($value) ? (int) $value: 3;
								break;
							case 'priority':
							case 'importance':
								if (!isset($aMsg['x-priority'])) {
									$aPrio = preg_split('/\w/',trim($value));
									$sPrio = strtolower(array_shift($aPrio));
									if  (is_numeric($sPrio)) {
										$iPrio = (int) $sPrio;
									} elseif ( $sPrio == 'non-urgent' || $sPrio == 'low' ) {
										$iPrio = 3;
									} elseif ( $sPrio == 'urgent' || $sPrio == 'high' ) {
										$iPrio = 1;
									} else {
										// default is normal priority
										$iPrio = 3;
									}
									$aMsg['x-priority'] = $iPrio;
								}
								break;
							case 'subject':
								$aMsg['subject'] = imap_mime_header_decode(imap_utf8($value));
								break;
							default:
								break;
							case 'content-type':
								$aMsg['content-type'] = $value;
								break;
							case 'x-eclass-mailid':
								$aMsg['x-eclass-mailid'] = $value;
								break;
							case 'message-id':
								$aMsg['message-id'] = $value;
								break;
							case 'x-msmail-priority':
								$aMsg['x-msmail-priority'] = $value;
								break;
						}
					}
				}
			}
			
			if($Flag!='')
				return $aMsg[$Flag];
			else
				return $aMsg;
        }

        // Get the number of unseen message
        function Get_Unseen_Mail_Number($Folder, $forDisplay=1) {
        		$Folder = str_replace('\\','\\\\',$Folder);
        		$Folder = str_replace('"','\"',$Folder);
//                $status = @imap_status($this->inbox,$this->getMailServerRef().$Folder,SA_UNSEEN);
				//if($this->IMapCache===false)
				//	$this->IMapCache = new imap_cache_agent();
				$this->getImapCacheAgent();
				$NumberOfUnseen = $this->IMapCache->Get_Unseen_In_Folder($this->encodeFolderName($Folder));
//              $NumberOfUnseen = ($status != false)? $status->unseen:$status;
                $DisplayUnseen = ($NumberOfUnseen == false || $NumberOfUnseen == 0)? "": "(".$NumberOfUnseen.")";
				
				if($forDisplay)
                	return $DisplayUnseen;
                else
                	return 	$NumberOfUnseen;
        }

        function Get_Last_Msg_UID($Folder) {
                $mailbox = $this->inbox;
                imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($Folder));

                //check the max msg number in the mailbox
                $mailcount = imap_check($mailbox);
                if($mailcount->Nmsgs > 0)
                	$UID = imap_uid($mailbox,$mailcount->Nmsgs);
                else
                	$UID = "";
                return $UID;
        }

                                /* ------ Start of Mail Preference -------- */
                                function Check_Preference_Exists() {
                                        $db = new database();
                                        $sql = 'Select
                                                                                count(1)
                                                                        From
                                                                                MAIL_PREFERENCE
                                                                        Where
                                                                                MailBoxName = \''.$this->CurUserAddress.'\'';
                                        $Result = $db->returnVector($sql);

                                        if ($Result[0] > 0) {
                                                return true;
                                        }
                                        else {
                                                return false;
                                        }
                                }

        function setMailForward($targetEmail, $keepCopy, $EmailList)
        {
                GLOBAL $SYS_CONFIG;

                #$login = $this->CurUserName;
                $login = $this->CurUserAddress;

                if (!$this->checkSecret())
                {
                     return false;
                }
				$path = $this->mail_api_url['SetMailForward'];
				$targetEmail = stripslashes($targetEmail);
                
				$success = true;
                for ($i = 0; $i < count($EmailList); ++$i)
                {
                	$login = strtolower($EmailList[$i]);
					//$response = $this->getWebPage($path."?");
                	//$post_data = "loginID=".urlencode($login)."&targetEmail=".urlencode($targetEmail)."&keepCopy=".urlencode($keepCopy)."&secret=".urlencode($this->secret);
                	$post_data = "loginID=".urlencode($login)."&targetEmail=".urlencode($targetEmail)."&keepCopy=".($keepCopy?"1":"0")."&secret=".urlencode($this->secret);
                	if ($this->actype != "")
                	{
                	    $post_data .= "&actype=".$this->actype;
                	}
                	$response = $this->getWebPage($path."?".$post_data);
                	
//                	$response = $this->executePost($path, $post_data);
//                	debug_pr($response);
                	$success = $success && ($response=="Mail forward set successfully");
            	}

                if ($success)
                {
//                    $db = new database();
//                    $sp = "exec Update_MailPreference_Forward @MailBoxName='" . $this->CurUserAddress . "', ";
//                    $sp .= "@ForwardedEmail='" . $targetEmail . "', ";
//                    $sp .= "@ForwardKeepCopy=" . ($keepCopy ? "1" : "0");
//
//                    $db->db_db_query($sp);

                    return true;
                }
                else return false;
        }

        function removeMailForward($EmailList)
        {
            Global $SYS_CONFIG;

            #$login = $this->CurUserName;
            $login = $this->CurUserAddress;
            if (!$this->checkSecret())
            {
                 return false;
            }
                
            $success = true;
            for ($i = 0; $i < count($EmailList); ++$i)
            {
	            $login = strtolower($EmailList[$i]);

            	$path = $this->mail_api_url['RemoveMailForward']."?loginID=".urlencode($login)."&secret=".urlencode($this->secret);
               	if ($this->actype != "")
               	{
	            	$path .= "&actype=".$this->actype;
               	}
               	$response = $this->getWebPage($path);
               	$success = $success && ($response[0]=="1");
            }
            
            if ($success)
            {
//                    $db = new database();
//                        $sp = "exec Update_MailPreference_Forward @MailBoxName='" . $this->CurUserAddress . "', ";
//                        $sp .= "@ForwardedEmail='', ";
//                        $sp .= "@ForwardKeepCopy=0";
//                        $db->db_db_query($sp);

                    return true;
            }
            else return false;
        }

		function addGroupBlockExternal($targetEmailArr)
        {
                GLOBAL $SYS_CONFIG;

                if (!$this->checkSecret())
                {
                     return false;
                }
				$path = $this->mail_api_url['AddGroupBlockExternal'];
				$domain = $SYS_CONFIG['Mail']['UserNameSubfix'];
				
				$email_chunks = array_chunk($targetEmailArr,10);
				$numOfChunk = count($email_chunks);
				$success = true;
				for($i=0;$i<$numOfChunk;$i++) {
					$targetEmail = implode(",",$email_chunks[$i]);
					$targetEmail = stripslashes($targetEmail);
	                
	            	$post_data = "loginID=".urlencode($targetEmail)."&domain=".urlencode($domain);
	            	
	            	if ($this->actype != "")
	            	{
	            	    $post_data .= "&actype=".$this->actype;
	            	}
	            	$response = $this->getWebPage($path."?".$post_data);
	            	$success = $success && ($response[0]== "1");
				}				
				
                if ($success)
                {
                    return true;
                }
                else return false;
        }

        function removeGroupBlockExternal($targetEmailArr)
        {
                GLOBAL $SYS_CONFIG;

                if (!$this->checkSecret())
                {
                     return false;
                }
				$path = $this->mail_api_url['RemoveGroupBlockExternal'];
				$domain = $SYS_CONFIG['Mail']['UserNameSubfix'];
				
				$email_chunks = array_chunk($targetEmailArr,10);
				$numOfChunk = count($email_chunks);
				$success = true;
				for($i=0;$i<$numOfChunk;$i++) {
					$targetEmail = implode(",",$email_chunks[$i]);
					$targetEmail = stripslashes($targetEmail);
	                
	            	$post_data = "loginID=".urlencode($targetEmail);
	            	
	            	if ($this->actype != "")
	            	{
	            	    $post_data .= "&actype=".$this->actype;
	            	}
	            	
	            	$response = $this->getWebPage($path."?".$post_data);
	            	$success = $success && ($response[0]== "1");
				}
				
                if ($success)
                {
                    return true;
                }
                else return false;
        }
        
//        function searchEamil($keyword)
//        {
//                GLOBAL $SYS_CONFIG;
//
//                if (!$this->checkSecret())
//                {
//                     return false;
//                }
//                
//				$path = $this->mail_api_url['SearchEmail'];
//				$keyword = stripslashes($keyword);
//                
//            	$post_data = "keyword=".urlencode($keyword);
//            	
//            	if ($this->actype != "")
//            	{
//            	    $post_data .= "&actype=".$this->actype;
//            	}
//            	debug($path."?".$post_data);
//            	$response =$this->getWebPage($path."?".$post_data);
//            	debug_pr($response);
//            	$success = $response[0]== 1;
//
//                if ($success)
//                {
//                    return true;
//                }
//                else return false;
//        }

		
		function changeTrashDay($trashday)
        {
                GLOBAL $SYS_CONFIG;

                if (!$this->checkSecret())
                {
                     return false;
                }
                
				$path = $this->mail_api_url['ChangeTrashDay'];
				
            	$post_data = "day=".$trashday;
            	
            	$response =$this->getWebPage($path."?".$post_data);
            	$success = $response[0]== 1;

                if ($success)
                {
                    return true;
                }
                else return false;
        }
        
        function changeSpamDay($spamday)
        {
                GLOBAL $SYS_CONFIG;

                if (!$this->checkSecret())
                {
                     return false;
                }
                
				$path = $this->mail_api_url['ChangeSpamDay'];
				
            	$post_data = "day=".$spamday;
            	
            	$response =$this->getWebPage($path."?".$post_data);
            	$success = $response[0]== 1;

                if ($success)
                {
                    return true;
                }
                else return false;
        }
        #### Get .forward content
        #### Return : array ($keepCopy, $forwarded_email)
        /*function getMailForward()
        {
                //get from DB, do not use the api
                $db = new database();
                $sp = "exec Get_MailPreference_ByMailBoxName @MailBoxName='" . $this->CurUserAddress . "'";
                   $ReturnContent = $db->returnArray($sp);

                        //$db->Close();
                        return $ReturnContent;

        }*/
			
        function setAutoReply($ReplyMessage, $EmailList)
        {
                 GLOBAL $SYS_CONFIG;

                 $login = $this->CurUserAddress;

                 if (!$this->checkSecret())
                 {
                      return false;
                 }
                 $ReplyMessage = stripslashes($ReplyMessage);
                 
                 $success = true;
                 for ($i = 0; $i < count($EmailList); ++$i)
                 {
					 $path = ($i == 0) ? $this->mail_api_url['AddAutoReply'] : $this->mail_api_url['AddAliasAutoReply'];
	                 $login = strtolower($EmailList[$i]);
	                 $post_data  = "login=".urlencode($EmailList[0]);
					 $post_data .= ($i == 0) ? "" : "&alias=".urlencode($login);
					 $post_data .= "&text=".urlencode($ReplyMessage);
					 
	                 if ($this->actype != "")
	                 {
	                     $post_data .= "&actype=".$this->actype;
	                 }
	                 if($this->secret != "")
	                 	$post_data .= "&secret=".urlencode($this->secret);
	                 
	                 //$response = $this->executePost($path, $post_data);
	                 $response = $this->getWebPage($path."?".$post_data);
	                 
	                 $success = $success && ($response[0]=="1");
             	 }

                if ($success)
                {
//                        $db = new database();
//                        $sp = "exec Update_MailPreference_AutoReply @MailBoxName='" . $this->CurUserAddress . "', ";
//                        $sp .= "@AutoReplyMessage='" . $db->Get_Safe_Sql_Query($ReplyMessage) . "', ";
//                        $sp .= "@EnableAutoReply=true";
//
//                        $db->db_db_query($sp);

                        return true;
                }
                else return false;
        }
        
 		function readAutoReply($Email)
        {
                 GLOBAL $SYS_CONFIG;

                 $login = $this->CurUserAddress;

                 if (!$this->checkSecret())
                 {
                      return false;
                 }
//                 $ReplyMessage = stripslashes($ReplyMessage);
                 
                 $success = true;
                 
                 $path = $this->mail_api_url['ReadAutoReply'];
                 $post_data  = "login=".urlencode($Email);
                 
                 if ($this->actype != "")
                 {
                     $post_data .= "&actype=".$this->actype;
                 }
                 if($this->secret != "")
	                 $post_data .= "&secret=".urlencode($this->secret);
	                 
                 $response = $this->executePost($path, $post_data);
                 
                 $success = $success && ($response[0]=="1");
                 
//                 for ($i = 0; $i < count($EmailList); ++$i)
//                 {
//					 $path = ($i == 0) ? $this->mail_api_url['AddAutoReply'] : $this->mail_api_url['AddAliasAutoReply'];
//	                 $login = strtolower($EmailList[$i]);
//	                 $post_data  = "login=".urlencode($EmailList[0]);
//					 $post_data .= ($i == 0) ? "" : "&alias=".urlencode($login);
//					 $post_data .= "&text=".urlencode($ReplyMessage);
//					 
//	                 if ($this->actype != "")
//	                 {
//	                     $post_data .= "&actype=".$this->actype;
//	                 }
//	                 $response = $this->executePost($path, $post_data);
//	                 
//	                 $success = $success && ($response[0]=="1");
//             	 }

                if ($success)
                {
//                        $db = new database();
//                        $sp = "exec Update_MailPreference_AutoReply @MailBoxName='" . $this->CurUserAddress . "', ";
//                        $sp .= "@AutoReplyMessage='" . $db->Get_Safe_Sql_Query($ReplyMessage) . "', ";
//                        $sp .= "@EnableAutoReply=true";
//
//                        $db->db_db_query($sp);

                        return true;
                }
                else return false;
        }
        
        function removeAutoReply($ReplyMessage, $EmailList)
        {
                Global $SYS_CONFIG;

                $login = $this->CurUserAddress;
                if (!$this->checkSecret())
                {
                     return false;
                }
				
                $success = true;
				for ($i = count($EmailList)-1; $i >= 0; --$i)
                {
	                $login = strtolower($EmailList[$i]);
	                //$path = $this->api_url['RemoveAutoreply']."?login=".urlencode($login);
					$path = ($i == 0) ? $this->mail_api_url['RemoveAutoReply'] : $this->mail_api_url['RemoveAliasAutoReply'];
	                $login = strtolower($EmailList[$i]);
					$post_data = "login=".urlencode($EmailList[0]);
					$post_data .= ($i == 0) ? "" : "&alias=".urlencode($login);
	
	                if ($this->actype != "")
	                {
	                    $post_data .= "&actype=".$this->actype;
	                }

	                $response = $this->getWebPage($path."?".$post_data);
	                
	                //$response = $this->executePost($path, $post_data);
	                $success = $success && ($response[0]=="1");
	                
				}
	                
                if ($success)
                {
//                    $db = new database();
//                        $sp = "exec Update_MailPreference_AutoReply @MailBoxName='" . $this->CurUserAddress . "', ";
//                        $sp .= "@AutoReplyMessage='" . $db->Get_Safe_Sql_Query($ReplyMessage) . "', ";
//                        $sp .= "@EnableAutoReply=false";
//                        $db->db_db_query($sp);
//
                    return true;
                }
                else return false;
        }

        function Get_Preference($UserEmail='')
        {
                /*$db = new database();
                $sp = "exec Get_MailPreference_ByMailBoxName @MailBoxName = '" . $this->CurUserAddress. "'";
                $ReturnContent = $db->returnArray($sp);
                        //$db->Close();
                return $ReturnContent;*/
			global $UserID;
			$li = new libdb();
			/*
			$sql = "
				SELECT
					DisplayName,
					ReplyEmail,
					Signature,
					ForwardedEmail,
					ForwardKeepCopy,
					DaysInSpam,
					DaysInTrash,
					SkipCheckEmail,
					AutoReplyTxt,
					AutoSaveInterval,
					GmailMode
				FROM 
					INTRANET_IMAIL_PREFERENCE
				WHERE
					UserID = $UserID
				";
			*/
			$sql = "
				SELECT
					DisplayName,
					ReplyEmail,
					Signature,
					ForwardedEmail,
					ForwardKeepCopy,
					DaysInSpam,
					DaysInTrash,
					SkipCheckEmail,
					AutoReplyTxt,
					AutoSaveInterval,
					GmailMode 
				FROM 
					MAIL_PREFERENCE 
				WHERE ";
			if($UserEmail != '')
				$sql .= " MailBoxName = '".$UserEmail."' ";
			else
				$sql .= " MailBoxName = '".$this->CurUserAddress."' ";
			
			$returnAry = $li->returnArray($sql);
			$finalReturnAry = sizeof($returnAry)>0?$returnAry[0]:array();
			return $finalReturnAry;
        }

        function Update_Preference($UpdateArr)
        {
         	$libdb = new libdb();
         	global $UserID;
         	
         	foreach((array)$UpdateArr as $UpdateField => $UpdateValue)
         	{
         		$UpdateStrArr[] = 	"$UpdateField = '$UpdateValue'";
         		$InsertFieldArr[] = $UpdateField;
         		$InsertValueArr[] = $UpdateValue;
         	}
         	
         	$UpdateStr = implode(",",$UpdateStrArr);
         	$InsertFieldStr = implode(",",$InsertFieldArr);
         	$InsertValueStr = implode(",",$InsertValueArr);
         	/*
         	$sql = "
				UPDATE 
					INTRANET_IMAIL_PREFERENCE
				SET
					$UpdateStr,
					DateModified = NOW()
				WHERE
					UserID = $UserID
			";
			*/
			$sql = "
				UPDATE 
					MAIL_PREFERENCE
				SET
					$UpdateStr,
					DateModified = NOW()
				WHERE
					MailBoxName = '".$this->CurUserAddress."'
			";
			$Result = $libdb->db_db_query($sql);
			
			if(mysql_affected_rows()==0)
			{
				/*
				$sql = "
					INSERT INTO
						INTRANET_IMAIL_PREFERENCE
							(UserID,$InsertFieldStr,DateInput)
						VALUES
							($UserID,$InsertValueStr,NOW())
				";*/
				$sql = "INSERT INTO
							MAIL_PREFERENCE
							(MailBoxName,$InsertFieldStr,DateInput)
						VALUES
							('".$this->CurUserAddress."',$InsertValueStr,NOW())";
				$Result = $libdb->db_db_query($sql);
			}
			
         	return $Result;
		}

        function New_Preference($MailBoxName,$UserID="") {

                $db = new database();

                $Result = $this->Check_Preference_Exists($MailBoxName);

                if (!$Result) {
                        if ($UserID == "") $UserID = $_SESSION['SSV_USERID'];
                                                $user = new user($UserID);
                                                if ($MailBoxName == $user->Email) {
                                $sql = 'Insert Into MAIL_PREFERENCE
                                                                        (
                                                                        MailBoxName,
                                                                        UserID,
                                                                        DisplayName
                                                                        )
                                                                Values
                                                                        (
                                                                        \''.$MailBoxName.'\',
                                                                        \''.$UserID.'\',
                                                                        \''.$user->Surname.','.' '.$user->PrefName.'\'
                                                                        )
                                                                ';
                                $Result = $db->db_db_query($sql);
                        }
                        else {
                                $sql = 'Insert Into MAIL_PREFERENCE
                                                                        (
                                                                        MailBoxName
                                                                        )
                                                                Values
                                                                        (
                                                                        \''.$MailBoxName.'\'
                                                                        )
                                                                ';
                                $Result = $db->db_db_query($sql);
                        }
                }

                return $Result;
        }
        
        function Change_Preference_Mailbox($oldMailBoxName,$newMailBoxName)
        {
        	$libdb = new libdb();
        	$sql = "UPDATE MAIL_PREFERENCE
					SET MailBoxName='$newMailBoxName',DateModified = NOW()
					WHERE MailBoxName = '$oldMailBoxName'";
			return $libdb->db_db_query($sql);
        }
                                /* ------ End of Mail Preference -------- */

  // get prev/ next message UID for Read mail navigation buttons
  function Get_Prev_Next_UID($folderName, $sort="", $reverse=0, $CurUID) {
    $mailbox = $this->inbox;
    imap_reopen($mailbox, $this->getMailServerRef().$this->encodeFolderName($folderName));
	
	switch ($sort){
		case "SORTARRIVAL": 
			$UIDList = imap_sort($mailbox, SORTARRIVAL , $reverse, SE_UID);
			break;
		case "SORTFROM":
			$UIDList = imap_sort($mailbox, SORTFROM , $reverse, SE_UID);
			break;
		case "SORTSUBJECT":
			$UIDList = imap_sort($mailbox, SORTSUBJECT , $reverse, SE_UID);
			break;
		case "SORTSIZE":
			$UIDList = imap_sort($mailbox, SORTSIZE , $reverse, SE_UID);
			break;
		default:
			$UIDList = imap_sort($mailbox, SORTARRIVAL , $reverse, SE_UID);
			break;
	}
	
	for($i=0; $i<sizeof($UIDList) ; $i++) {
		if ($UIDList[$i] == $CurUID) {
			$CurIndex = $i;
			break;
		}
	}
	
	$PrevUID = $UIDList[$CurIndex+1];
	$NextUID = $UIDList[$CurIndex-1];
	
	if ($PrevUID != '')
		$ReturnVal['PrevUID'] = $PrevUID;
    else
        $ReturnVal['PrevUID'] = false;
	
    if ($NextUID != '')
        $ReturnVal['NextUID'] = $NextUID;
    else
        $ReturnVal['NextUID'] = false;
            
    return $ReturnVal;
  }

	// Get result of Simple / Advance search
	function Get_Search_Mail_List($FromMsg=1, $ToMsg=50, $SortField="", $Reverse=1, $SearchField=array(), $SearchInfo=array(), $IsAdvanceSearch=1,$seen) {
		if (sizeof($SearchInfo['Folder']) == 0) {
			//if(isset($this->AllFolderList) && sizeof($this->AllFolderList)>0){
			//	$FolderList = $this->AllFolderList;
			//}else{
            //	$FolderList = $this->getMailFolders();
            //	$this->AllFolderList = $FolderList;
			//}
			$FolderList = $this->getAllFolderList();
		} else {
				$FolderList = $SearchInfo['Folder'];
		}
		
		//sort($FolderList,SORT_STRING);

		$mailbox = $this->inbox;
		$MessageIDs = array();
		for ($i=0; $i<sizeof($FolderList) ; $i++) {
			if($IsAdvanceSearch == 1){
				$Temp = $this->Search_Mail($FolderList[$i], $SearchInfo['From'], $SearchInfo['To'], $SearchInfo['Cc'], $SearchInfo['Bcc'], $SearchInfo['Subject'], $SearchInfo['FromDate'], $SearchInfo['ToDate'], $SearchInfo['Body'], $SearchInfo['Attachment'],1,$seen,$SearchInfo);
			} else {
				$Temp = $this->Search_Mail($FolderList[$i], $SearchInfo['keyword'], $SearchInfo['keyword'], '', '', $SearchInfo['keyword'],  $SearchInfo['FromDate'], $SearchInfo['ToDate'], '', '', 0,$seen,$SearchInfo);
			}
			
			# merge array result if multi-folder selection is allowed
			if (!is_null($Temp)) {
				$MessageIDs = array_merge($MessageIDs, $Temp);	
			}
		}

		# Get the Range of Mails
		//$FetchMsgNo = array_slice($MessageIDs, $FromMsg-1, $ToMsg);		// Extract mails from the range of user settings
		$FetchMsgNo = $MessageIDs;									// Use all mails
		
		
		# Get the header info of each Mails		
		for ($i=0; $i< sizeof($FetchMsgNo) ; $i++) {
			# reopen imap stream to specific folder(mailbox)
			if ($FetchMsgNo[$i][0] != $FetchMsgNo[$i-1][0] || $i == 0) {
				$this->Go_To_Folder($FetchMsgNo[$i][0]);		
			}
			$OverviewArray[] = $this->Get_Mail_In_Folder($FetchMsgNo[$i][0], $FetchMsgNo[$i][1]);
		}

		if (sizeof($OverviewArray) > 0){
			$FinalArray = $OverviewArray;
		} else {
			$FinalArray = array();
		}

		/*
		*	No sorting is applied in Advance Search since 2008-11-10
		*/
		/*
		# Sort the Mail by SortField
		if (sizeof($OverviewArray) > 0) {
			// Sort the result array
			switch ($SortField) {
				case "DateSort": $RealSort = "DateSort"; break;
				case "From": 	 $RealSort = "From"; 	 break;
				case "Subject":  $RealSort = "Subject";  break;
				case "Size": 	 $RealSort = "Size"; 	 break;
				case "To": 		 $RealSort = "To"; 		 break;
				case "Folder":   $RealSort = "Folder"; 	 break;
				default: 		 $RealSort = "DateSort"; break;
			}

			foreach ($OverviewArray as $pos =>  $val){
				$tmp_array[$pos] = $val[$RealSort];
			}

			if ($Reverse == 0){
				uasort($tmp_array, "String_To_Lower_Array_Cmp");
			} else {
				uasort($tmp_array, "String_To_Lower_Array_Cmp_Reverse");
			}

			// display however you want
			foreach ($tmp_array as $pos =>  $val){
				$FinalArray[] = $OverviewArray[$pos]; // [$i][$j][From]
			}

			$FinalArray = array_slice($FinalArray, $FromMsg-1, $ToMsg);
		}
		else {
			$FinalArray = array();
		}
		*/
		return array($FinalArray, sizeof($MessageIDs));
	}

        function Clear_Folder($FolderName) {
                imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));
                $MaxMsgNum = imap_num_msg($this->inbox);

                imap_setflag_full($this->inbox,'1:'.$MaxMsgNum,'\\Deleted');

                $result = imap_expunge($this->inbox);
				
				$this->Clean_Delete_Log();
				$log = "Emptied folder $FolderName with $MaxMsgNum mails";
				$this->Log_Delete_Action($log);
				
                return $result;
        }

        function Set_Answered($FolderName,$UID) {
                imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));

                $result = imap_setflag_full($this->inbox,$UID,'\\Answered',ST_UID);
				if($result && $this->CacheMailToDB == true){
					$DataArr['Answered'] = 1;
					$this->Manage_Cached_Mail("update",$FolderName,$UID,$DataArr);
				}
				
                return $result;
        }

		function Set_Forwarded($FolderName,$UID) {
               // imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));
			//global $IMapCache;
			//if($this->IMapCache===false)
			//	$this->IMapCache = new imap_cache_agent();
			$this->getImapCacheAgent();
			$result = $this->IMapCache->Set_Fowarded_Flag($FolderName,$UID);
			if($result && $this->CacheMailToDB == true){
				$DataArr['Forwarded'] = 1;
				$this->Manage_Cached_Mail("update",$FolderName,$UID,$DataArr);
			}
			
            return $result;
        }

        function Set_Flagged($FolderName,$UID) {
                imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));

                $result = imap_setflag_full($this->inbox,$UID,'\\Flagged',ST_UID);
				
				if($result && $this->CacheMailToDB == true){
					$DataArr['Flagged'] = 1;
					$this->Manage_Cached_Mail("update",$FolderName,$UID,$DataArr);
				}
				
                return $result;
        }
        
        function Clear_Flagged($FolderName,$UID) {
                imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));

                $result = imap_clearflag_full($this->inbox,$UID,'\\Flagged',ST_UID);
				if($result && $this->CacheMailToDB == true){
					$DataArr['Flagged'] = 0;
					$this->Manage_Cached_Mail("update",$FolderName,$UID,$DataArr);
				}
                return $result;
        }
        
        function Clear_Expired_Mail($FolderName) {
                if ($FolderName == $this->RootPrefix."Trash" || $FolderName == $this->RootPrefix."Spam") {
                        $Perference = $this->Get_Preference($_SESSION['SSV_USERID']);

                        if ($FolderName == $this->RootPrefix."Trash") $ExpirePeriod = $Perference[0][10];
                        else $ExpirePeriod = $Perference[0][9];

                        if ($ExpirePeriod == 0) return true;

                        imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($FolderName));
                        $NumberOfMessage = imap_num_msg($this->inbox);
                        $MailToRemove = $this->listMailInFolder($FolderName, 1, $NumberOfMessage);

                        $DateToClear = time() - ($ExpirePeriod * 24 * 60 * 60);
                        if (sizeof($MailToRemove) > 0) {
                                if ($MailToRemove[0]['DateSort'] < $DateToClear) {
                                        for ($i=0; $i< sizeof($MailToRemove); $i++) {
                                                if ($MailToRemove[$i]['DateSort'] < $DateToClear)
                                                        $UIDRemoveList[] = $MailToRemove[$i]['UID'];
                                        }

                                        imap_delete($this->inbox,implode(",",$UIDRemoveList),FT_UID);

                                        return imap_expunge($this->inbox);
                                }
                                else return true;
                        }
                        else return true;
                }
                else return false;
        }

      #### New functions for ASAV by BL
      function readBlackList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadBlackList']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response == "")
               {
                   return "";
               }
               else
               {
                   $array = explode(";",$response);
                   return $array;
               }
      }
      function addBlackList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['AddBlackList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeBlackList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['RemoveBlackListRecord']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function readWhiteList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadWhiteList']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response == "")
               {
                   return "";
               }
               else
               {
                   $array = explode(";",$response);
                   return $array;
               }
      }
      function addWhiteList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['AddWhiteList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeWhiteList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['RemoveWhiteList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }

      function readSpamCheck()
      {
      	  global $webmail_info;
      	  
           if (!$this->checkSecret())
           {
                return false;
           }
           if($this->retriveSpamControlSetting() == 1){
           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
           } else {
                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
           }
           
           //$path = $this->mail_api_url['ReadSpamCheck']."?actype=".$this->actype."&secret=".urlencode($this->secret);
           $path = $this->mail_api_url['ReadSpamCheck']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
           $response = $this->getWebPage($path);
           return $response;
      }
      function turnOnSpamCheck()
      {
      		global $webmail_info;
               if (!$this->checkSecret())
               {
                    return false;
               }
               
               if($this->retriveSpamControlSetting() == 1){
	           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
	           } else {
	                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
	           }
               
               //$path = $this->mail_api_url['BypassSpamCheck']."?onOff=Y&actype=".$this->actype."&secret=".urlencode($this->secret);
               $path = $this->mail_api_url['BypassSpamCheck']."?onOff=N&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function turnOffSpamCheck()
      {
      		global $webmail_info;
               if (!$this->checkSecret())
               {
                    return false;
               }
               
               if($this->retriveSpamControlSetting() == 1){
	           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
	           } else {
	                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
	           }
               
               //$path = $this->mail_api_url['BypassSpamCheck']."?onOff=N&actype=".$this->actype."&secret=".urlencode($this->secret);
               $path = $this->mail_api_url['BypassSpamCheck']."?onOff=Y&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      
      function changeDomainSpamScore($score)
      {
      		if (!$this->checkSecret())
           	{
                return false;
           	}
           	$path = $this->mail_api_url['ChangeDomainSpamScore']."?domain=".$this->mailaddr_domain."&actype=".$this->actype."&score=".$score."&secret=".urlencode($this->secret);
      		$response = $this->getWebPage($path);
      		if($response[0]=="1"){
      			return true;
      		}else{
      			return false;	
      		}
      }
      
      function readVirusCheck()
      {
      		global $webmail_info;
               if (!$this->checkSecret())
               {
                    return false;
               }
               if($this->retriveSpamControlSetting() == 1){
	           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
	           } else {
	                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
	           }
               //$path = $this->mail_api_url['ReadVirusCheck']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $path = $this->mail_api_url['ReadVirusCheck']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               return $response;
      }
      function turnOnVirusCheck()
      {
      		global $webmail_info;
               if (!$this->checkSecret())
               {
                    return false;
               }
               if($this->retriveSpamControlSetting() == 1){
	           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
	           } else {
	                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
	           }
               
               //$path = $this->mail_api_url['BypassVirusCheck']."?onOff=Y&actype=".$this->actype."&secret=".urlencode($this->secret);
               $path = $this->mail_api_url['BypassVirusCheck']."?onOff=N&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function turnOffVirusCheck()
      {
      		global $webmail_info;
               if (!$this->checkSecret())
               {
                    return false;
               }
               if($this->retriveSpamControlSetting() == 1){
	           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
	           } else {
	                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
	           }
               //$path = $this->mail_api_url['BypassVirusCheck']."?onOff=N&actype=".$this->actype."&secret=".urlencode($this->secret);
               $path = $this->mail_api_url['BypassVirusCheck']."?onOff=Y&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
	  
	  
	  # Copy from libwebmail 
      # set use default spam level($webmail_info['bl_spam_control_level']) or custom spam level ##
      function setSpamControlSetting($enable_custom)
      {
	      global $webmail_info, $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");
	      
	      $lf = new libfilesystem();
	      $location = $intranet_root."/file/";
	      $file = $location."setting_enable_custom_spam_control.txt";
	      $success = $lf->file_write($enable_custom, $file);
      }
      
      # Copy from libwebmail
      ## retrive spam level is using default spam level($webmail_info['bl_spam_control_level']) or custom spam level ##
      function retriveSpamControlSetting()
      {
	      global $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");
	      	      
	      $lf = new libfilesystem();
	      $SpamControlSetting = trim($lf->file_read($intranet_root."/file/setting_enable_custom_spam_control.txt"));
	
	      return $SpamControlSetting;
      }
      
      #Copy from libwebmail
      ## set the custom spam level ##
      function setSpamControlLevel($spam_level)
      {
	      global $webmail_info, $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");

	      $lf = new libfilesystem();
	      $location = $intranet_root."/file/";
	      $file = $location."setting_spam_control_level.txt";
	      $success = $lf->file_write($spam_level, $file);
      }
      
      # Copy from libwebmail
      function retriveSpamControlLevel()
      {
	      global $webmail_info, $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");
	      
	      $lf = new libfilesystem();
	      
	      //if($this->readSpamCheck() == "Y"){
		      //echo $this->retriveSpamControlSetting();
		      if($this->retriveSpamControlSetting() == 1)
		      {
			      $SpamControlLevel = trim($lf->file_read($intranet_root."/file/setting_spam_control_level.txt"));
		      }
		      else
		      {
			      $SpamControlLevel = $webmail_info['bl_spam_control_level'];
		      }
	      //}
	      return $SpamControlLevel;
      }
      
	  # Copy from libwebmail
	  function readSpamScore()
      {
	      global $webmail_info;
	      
           if (!$this->checkSecret())
           {
                return false;
           }
           
           if($this->retriveSpamControlSetting() == 1){
           		$policy = isset($webmail_info['custom_spam_control_level'])? $webmail_info['custom_spam_control_level'] : 8;
           } else {
                $policy = isset($webmail_info['bl_spam_control_level'])? $webmail_info['bl_spam_control_level'] : 3;
           }
                          
           $path = $this->mail_api_url['ReadSpamScore']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
           $response = $this->getWebPage($path);
           
           return $response;
      }
	  
      ## Alias functions
      function addAlias($newAlias, $loginname)
      {
      	global $SYS_CONFIG;
      	
               if (!$this->checkSecret())
               {
                    return false;
               }

               $loginname = $this->attachDomain($loginname);
               //echo $loginname.'<br>';
               $newAlias = $this->attachDomain($newAlias);
               //echo $newAlias.'<br>';
               $path = $this->mail_api_url['AddAlias'];
               $post_data = "login=".$newAlias."&recipient=".$loginname."&actype=".$this->actype."&secret=".urlencode($this->secret);
               //$response = $this->executePost($path, $post_data);
               
               //1. found cannot call the api using executePost, use the old method first
               //2. urlencoded parameter is not supported by api
               $path = $this->mail_api_url['AddAlias']."?".$post_data;
               //echo $SYS_CONFIG['Mail']['APIHost'].':'.$SYS_CONFIG['Mail']['APIPort'].$path.'<br>';
               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }

      }
      function removeAlias($loginname)
      {
      	global $SYS_CONFIG;
               if (!$this->checkSecret())
               {
                    return false;
               }

               $loginname = $this->attachDomain($loginname);
               $path = $this->mail_api_url['RemoveAlias'];
               $post_data = "login=".$loginname."&actype=".$this->actype."&secret=".urlencode($this->secret);
               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
               $response = $this->getWebPage($path."?".$post_data);
               //$response = $this->executePost($path, $post_data);
               echo $SYS_CONFIG['Mail']['APIHost'].':'.$SYS_CONFIG['Mail']['APIPort'].$path.'<br>';
               echo $path."?".$post_data;
               var_dump($response);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }

      # Return array( alias 1, alias 2, alias 3, ......)
      function getAliasInfo($loginname)
      {
      	global $SYS_CONFIG;
               if (!$this->checkSecret())
               {
                    return false;
               }

               $loginname = $this->attachDomain($loginname);
               $path = $this->mail_api_url['GetAliasInfo'];
               $post_data = "login=".$loginname."&actype=".$this->actype."&secret=".urlencode($this->secret);
               $this->setRemoteAPIHost($SYS_CONFIG['Mail']['APIHost'], $SYS_CONFIG['Mail']['APIPort']);
               $response = $this->getWebPage($path."?".$post_data);
               //$response = $this->executePost($path, $post_data);
               if ($response == "")
               {
                   return array();
               }
               $entries = explode(",",$response);
               return $entries;
      }

      # Return array (
      #               array (primary email, alias1, alias2, alias3, .....)
      # )
      function getAliasList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }

               $path = $this->mail_api_url['GetAliasList'];
               $post_data = "actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->executePost($path, $post_data);
               if ($response == "")
               {
                   return array();
               }

               # Create array information
               $result = array();
               $entries = explode(",",$response);
               for ($i=0; $i<sizeof($entries); $i++)
               {
                    if ($entries[$i]=="")
                    {
                        $temp = explode(";", $entries[$i]);
                        if (sizeof($temp)<2)          # Skip error row
                        {

                        }
                        else
                        {
                            $result[] = $temp;
                        }

                    }
               }

               return $result;
      }

      # Supporting function
      function executePost($action_url, $post_data)
      {
               # Try to connect to service host
               global $SYS_CONFIG;
               if($SYS_CONFIG['Mail']['SSLPort'] != ''){
               		$lhttp = new libhttpclient($this->remote_server, $SYS_CONFIG['Mail']['SSLPort']);
               }else{
                	$lhttp = new libhttpclient($this->remote_server, $this->remote_server_port);
               }
                if (!$lhttp->connect())    # Exit if failed to connect
                return false;
                $lhttp->set_path($action_url);
                $lhttp->post_data = $post_data;
                $response = $lhttp->send_request();
                
                $pos = strpos($response,"\r\n\r\n");
                $response = substr($response,$pos+4);

                return $response;
        }
		
		// Get n quota info by input n loginID
		function Get_Single_Quota($loginID)
		{
			if (!$this->checkSecret())
            {
            	return false;
            }
            
            if(is_array($loginID)){
            	$postLoginID = implode(",",$loginID);
            }else{
            	$postLoginID = $loginID;
            }
            
            $path = $this->mail_api_url['GetSingleQuota'];
            $post_data = "loginID=".$postLoginID."&actype=".$this->actype."&secret=".urlencode($this->secret);
            $response = $this->executePost($path, $post_data);
            
            return $response;
		}
		
		function Set_Batch_Quota($loginID,$quota)
		{
			if (!$this->checkSecret())
            {
            	return false;
            }
            $postLoginID = implode(",",(array)$loginID);
            $postQuota = implode(",",(array)$quota);
            
            $path = $this->mail_api_url['SetBatchQuota'];
            $post_data = "loginID=".$postLoginID."&quota=".$postQuota."&actype=".$this->actype."&secret=".urlencode($this->secret);
            $response = $this->executePost($path, $post_data);
            
            return $response;
		}
      ###### End of new functions


        function Conver_Encoding($OriginText, $TargetEncode, $OriginEncode)
        {
                if ($OriginEncode == 'default')
                {
                        $ReturnX = mb_convert_encoding($OriginText, $TargetEncode);
                } else
                {
                        if ($OriginEncode!="" && !strstr($OriginEncode, "unknown"))
                        {
                                $ReturnX = mb_convert_encoding($OriginText,$TargetEncode,$OriginEncode);
                        } else
                        {
                                $ReturnX = $OriginText;
                        }
                }

                return $ReturnX;
        }

        function Close_Connect() {
                return imap_close($this->inbox);
        }

        function Get_UID($MessageID) {
                return @imap_uid($this->inbox,$MessageID);
        }

        function Get_MessageID($UID) {
                return @imap_msgno($this->inbox,$UID);
        }
        
        /*
          List the emails in the folder
          Param: folderName - listing the folder
                 startMsgNumber - retrieve email starting from startMsgNumber
                 count - how many to return
                 sort - sorting criteria
          Return: Array()
                    - Subject
                    - From
                    - To
                    - Date
                    - UID
                    - size
                    - msgno
                    - recent
                    - answered
                    - deleted
                    - seen
                    - [Pls check whether reply / forward / reply all]
        */
        function Get_Mail_In_Folder($folderName, $MsgNumber)
        {
        	global $Lang;

			//imap_reopen($this->inbox, $this->getMailServerRef().$this->encodeFolderName($folderName));

			$TempObj   = imap_headerinfo($this->inbox, $MsgNumber);
			$RawHeader = imap_fetchheader($this->inbox, $MsgNumber);

			# From Field
			$fromList = $TempObj->from;
			for ($j=0; $j<1 ; $j++) {
				if (!isset($fromList[$j]->personal)) {
					if($fromList[$j]->mailbox && $fromList[$j]->host)
						$DisplayName = $fromList[$j]->mailbox."@".$fromList[$j]->host;
				} else {
//					$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$j]->personal));
//					$DisplayName = $DisplayObject[0]->text;
					$DisplayName = $this->MIME_Decode($fromList[$j]->personal);
				}
					
				$Address = $fromList[$j]->mailbox."@".$fromList[$j]->host;
				$OverviewArray['From'] .= $DisplayName;
			}
			
			# To Field
			$toList = $TempObj->to;
			if (sizeof($toList) > 0) {
				for ($j=0; $j< 1; $j++) {
					if (!isset($toList[$j]->personal)) {
						$DisplayName = $toList[$j]->mailbox."@".$toList[$j]->host;
					} else {
//						$DisplayObject = imap_mime_header_decode(imap_utf8($toList[$j]->personal));
//						$DisplayName = $DisplayObject[0]->text;
						$DisplayName = $this->MIME_Decode($toList[$j]->personal);
					}
					
					$Address = $toList[$j]->mailbox."@".$toList[$j]->host;
					$OverviewArray['To'] .= $DisplayName;
				}
				if (sizeof($toList) > 1) {
					$OverviewArray['To'] .= '...';
				}
			} else {
				$OverviewArray['To'] = $Lang['email']['NoReceiver'];
			}
					
					/*$ccList = $TempObj->cc;
          for ($j=0; $j< sizeof($ccList); $j++) {
						if (!isset($ccList[$j]->personal)) $DisplayName = $ccList[$j]->mailbox."@".$ccList[$j]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($ccList[$j]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
						
						$Address = $ccList[$j]->mailbox."@".$ccList[$j]->host;
						
						$OverviewArray['Cc'] .= $DisplayName." ".$Address;
						$OverviewArray['Cc'] .= ";";
					}
					
					$bccList = $TempObj->bcc;
          for ($j=0; $j< sizeof($bccList); $j++) {
						if (!isset($bccList[$j]->personal)) $DisplayName = $bccList[$j]->mailbox."@".$bccList[$j]->host;
						else {
							$DisplayObject = imap_mime_header_decode(imap_utf8($bccList[$j]->personal));
							$DisplayName = $DisplayObject[0]->text;
						}
						
						$Address = $bccList[$j]->mailbox."@".$bccList[$j]->host;
						
						$OverviewArray['Bcc'] .= $DisplayName." ".$Address;
						$OverviewArray['Bcc'] .= ";";
					}*/
					
			$Date = substr($TempObj->date, 0, -5);
        	$OverviewArray['DateReceived'] = $Date;
			$OverviewArray['DateSort'] = strtotime($this->formatDatetimeString($TempObj->date));
			
			$HeaderValues = $this->Get_Header_Value($RawHeader,"");
			# Get Priority
			//$MailPriority = $this->Get_Header_Value($RawHeader,"x-priority");
			$MailPriority = $HeaderValues['x-priority'];
			$OverviewArray['Priority'] = ($MailPriority == 1) ? $MailPriority : 0;
			
			$OverviewArray['Message-Id'] = $HeaderValues['message-id'];
			$OverviewArray['X-eClass-MailID'] = $HeaderValues['x-eclass-mailid'];
			# Subject
//			$SubjectObj = imap_mime_header_decode(imap_utf8($TempObj->subject));
//			unset($subtmp);
//			if(count($SubjectObj) > 0){
//				for($k=0 ; $k<count($SubjectObj) ; $k++) {
//					$subtmp .= $SubjectObj[$k]->text;
//				}
//			}
			unset($subtmp);
			$subtmp = $this->MIME_Decode($TempObj->subject);
			$Subject = ($subtmp == "") ? "NO SUBJECT":  $subtmp;$OverviewArray['Subject'] = $Subject;
			
			# Attachment 
	        //$ContentType = $this->Get_Header_Value($RawHeader,'Content-Type');
	        $ContentType = $HeaderValues['content-type'];
	        $OverviewArray['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType) == 0)? false:true;
			
					
			# UID
			$OverviewArray['UID'] = $this->Get_UID($TempObj->Msgno);

			# Mail size
			$OverviewArray['Size'] = round(($TempObj->Size/1024), 2);

			# Other Flag
			$OverviewArray["recent"]   = ($TempObj->Recent == "R" || $TempObj->Recent == "N") ? true : false;
			$OverviewArray["flagged"]  = ($TempObj->Flagged == "F") ? true : false;
			$OverviewArray["answered"] = ($TempObj->Answered == "A") ? true : false;
			$OverviewArray["deleted"]  = ($TempObj->Deleted == "D") ? true : false;
			$OverviewArray["seen"]     = ($TempObj->Unseen == "U" || $TempObj->Recent == "N") ? false : true;
			$OverviewArray["draft"]    = ($TempObj->Draft == "X") ? true : false;

			# Folder Name
			$OverviewArray["Folder"] = $folderName;
					
			// Get Mail Body
			/*unset($this->parsed_part);
			$this->parse_message($MailStructure);
			$text_message = $this->retrieve_textmessage($this->inbox, $TempObj->Msgno, $MailStructure);

			if ($text_message == "") {
				$OverviewArray["Body"] = Remove_HTML_Tags($this->retrieve_message($this->inbox, $TempObj->Msgno, $this->parsed_part));
			}
			else {
			$OverviewArray["Body"] = $text_message;
			}*/
					  
			return $OverviewArray;
        }
        
      	function Get_Header_Value($RawHeader, $Flag) {
      		$Flag = strtolower($Flag);
			$TextHeader = $RawHeader;
					
			$TempHeader = trim(str_replace(array("\r\n", "\n\t", "\n "), array("\n", ' ', ' '), $TextHeader));
			$TempHeader = explode("\n" , $TempHeader);
			
			//echo "<pre>";var_dump($TempHeader);echo "</pre>";
			
			for ($i=0; $i< sizeof($TempHeader); $i++) {
                $pos = strpos($TempHeader[$i], ':');
				if ($pos > 0) {
					$field = strtolower(substr($TempHeader[$i], 0, $pos));
					if (!strstr($field,' ')) { /* valid field */
						$value = trim(substr($TempHeader[$i], $pos+1));
						switch($field) {
							case 'from':
								$aMsg['from'] = ($value != "") ? $value : "";
								break;
							case 'to':
								$aMsg['to'] = ($value != "") ? $value : "";
								break;
							case 'date':
								$aMsg['date'] = ($value != "") ? $value : "";
								break;
							case 'x-priority':
								$aMsg['x-priority'] = ($value) ? (int) $value: 3;
								break;
							case 'priority':
							case 'importance':
								if (!isset($aMsg['x-priority'])) {
									$aPrio = preg_split('/\w/',trim($value));
									$sPrio = strtolower(array_shift($aPrio));
									if  (is_numeric($sPrio)) {
										$iPrio = (int) $sPrio;
									} elseif ( $sPrio == 'non-urgent' || $sPrio == 'low' ) {
										$iPrio = 3;
									} elseif ( $sPrio == 'urgent' || $sPrio == 'high' ) {
										$iPrio = 1;
									} else {
										// default is normal priority
										$iPrio = 3;
									}
									$aMsg['x-priority'] = $iPrio;
								}
								break;
							case 'subject':
								$aMsg['subject'] = ($value != "") ? imap_mime_header_decode(imap_utf8($value)) : "";
								break;
							case 'content-type':
								$aMsg['content-type'] =( $value != "") ? $value : "";
								break;
							case 'x-eclass-mailid':
								$aMsg['x-eclass-mailid'] = $value;
								break;
							case 'message-id':
								$aMsg['message-id'] = $value;
								break;
							default:
								break;
						}
					}
				}
			}
			if($Flag!='')
				return $aMsg[$Flag];
			else
				return $aMsg;
      	}
		
	function Build_Mime_Text($subject,$message,$from,$receiver_to=array(),$receiver_cc=array(),$receiver_bcc=array(),$AttachList=array(),$IsImportant="",$DraftFlag=false, $Perference="", $RelatedFileList=array(), $isFromApp=false) {
  	global $PartNumber, $AttachPartNumber, $UID, $TNEFFile, $PathRelative;
  	global $Folder, $SYS_CONFIG, $XeClassMailID;//, $IMapCache;
	//if($this->IMapCache===false)
	//	$this->IMapCache = new imap_cache_agent();
	$this->getImapCacheAgent();
	$ReturnMsg = $this->IMapCache->Build_Mime_Text($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$AttachList,$IsImportant,$DraftFlag, $Perference, $RelatedFileList, $isFromApp);
	
	return $ReturnMsg;
  }
  
		
	function Build_Mime_Text_backup($subject,$message,$from,$receiver_to=array(),$receiver_cc=array(),$receiver_bcc=array(),$AttachList=array(),$IsImportant="",$DraftFlag=false, $Perference="") {
  	global $PartNumber, $AttachPartNumber, $UID, $TNEFFile, $PathRelative;
  	global $Folder, $SYS_CONFIG;
  	//if($this->IMapCache===false)
  	//	$this->IMapCache = new imap_cache_agent();
		$this->getImapCacheAgent();
		
		$TextMessage = html_entity_decode(html2txt(str_replace(array("<br>","<hr>"),array(chr(10),"______________________".chr(10)),$message))); 

		if ($UID != "") {
			$InlineFileList = $this->IMapCache->Get_Related_Inline_Info($Folder,$UID,"",$PartNumber);
		}
		else {
			$InlineFileList = false;
		}
    # Content-type of attachment
    $type="application/octet-stream";

    $priority = ($IsImportant==1)? 1: 3;
   	
    # receiving email addresses
    for ($i=0; $i< sizeof($receiver_to); $i++) {
      if (trim($receiver_to[$i]) != "") {
      	$FinalReceiverTo[] = $receiver_to[$i];
      }
    }
    for ($i=0; $i< sizeof($receiver_cc); $i++) {
      if (trim($receiver_cc[$i]) != "") {
        $FinalReceiverCc[] = $receiver_cc[$i];
      }
    }
    for ($i=0; $i< sizeof($receiver_bcc); $i++) {
      if (trim($receiver_bcc[$i]) != "") {
        $FinalReceiverBcc[] = $receiver_bcc[$i];
      }
    }
    $numTo = sizeof($FinalReceiverTo);
    $numCC = sizeof($FinalReceiverCc);
    $numBCC = sizeof($FinalReceiverBcc);
    
    if (!$DraftFlag) {
    	if ($numTo + $numCC + $numBCC == 0) return false;
    }

    $to = ($numTo != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverTo))) : "";
    $cc = ($numCC != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverCc))) : "";
    $bcc = ($numBCC != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverBcc))) : "";
    
    # Mail Header
    $headers='';
    // have peference
    if ($Perference!="") {
    	$fromName = (trim($Perference[0]["DisplayName"]) == "")? substr($from,0,stripos($from,"@")):$Perference[0]["DisplayName"];
      $ReplyAddress = (trim($Perference[0]["ReplyEmail"]) != "")? $Perference[0]["ReplyEmail"]:$from;
      $HeaderFrom .= "From: \"$fromName\" <$from>".$this->MimeLineBreak;

      $ReturnMsg['From'] = trim('"'.$fromName.'" <'.$from.'>');
			$ReturnMsg['SortFrom'] = str_replace('"','',$ReturnMsg['From']);
    }
    // for auto mail, e.g password reset
    else {
    	$HeaderFrom .= 'From: '.$from.$this->MimeLineBreak;
      $ReplyAddress = $from;
    }

    for ($i=0; $i< sizeof($AttachList); $i++) {
      $files[] = $AttachList[$i][2];
      $fileRealName[] = $AttachList[$i][1];
    }

    $charset = "UTF-8";

    $isHTMLMessage = 1;
		// check is multipart message
    $isMulti = (sizeof($files) > 0 || sizeof($AttachPartNumber) > 0 || $InlineFileList !== false || sizeof($TNEFFile) > 0);
    // check is mixed message
    $isMixed = (sizeof($files) > 0 || sizeof($AttachPartNumber) > 0 || sizeof($TNEFFile) > 0);
    // check is related message
    $isRelated = ($InlineFileList !== false);

    $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
    $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part
		
		$headers .= "MIME-Version: 1.0".$this->MimeLineBreak;	
    if ($isMulti) {
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      if ($isMixed) 
      	$headers .= "Content-Type: multipart/mixed; ";
      else
      	$headers .= "Content-Type: multipart/related; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    else if ($isHTMLMessage)         # HTML message w/o attachments
    {
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      $headers .= "Content-Type: multipart/alternative; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    if ($Perference == "")
			$headers .= $HeaderFrom;

    $headers .= "Date: " . date("r") .$this->MimeLineBreak;


    if ($ReplyAddress != "") {
    	$headers .= "Reply-To: $ReplyAddress".$this->MimeLineBreak;
    }

    if ($isMulti || $isHTMLMessage) {
	    $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
	    $mime .= $this->MimeLineBreak;
	    $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
    }

    if (!$isMulti) { # No attachments/ related
    	if ($isHTMLMessage) {
        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$encoded_text_message = QuotedPrintableEncode($message);
        $encoded_text_message = chunk_split(base64_encode($TextMessage));
        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
        # HTML part
        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
      }
      else {
      	$mime .= $message;
      }
    }
    else {
    	if ($isMixed && $isRelated) {
    		$mime_boundary_related = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Related
    		$mime_boundary_alternative = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Alternative
    		$mime .= "Content-Type: multipart/related; boundary=\"$mime_boundary_related\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary_related . $this->MimeLineBreak;
        
        /*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';*/
        
        for ($i=0; $i < sizeof($InlineFileList); $i++) {
        	if (stristr($message,$InlineFileList[$i]['FilePath']) !== FALSE) {
	      		$message = str_replace($InlineFileList[$i]['FilePath'],$InlineFileList[$i]['FileContentID'],$message);
	      		$FinalInlineList[] = $InlineFileList[$i];
	      	}
	      }
				
				/*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';
				die;*/
	      if ($isHTMLMessage) {
	        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary_alternative\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($TextMessage)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary_alternative . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_related.$this->MimeLineBreak;
	      }
	      else {
	        # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	
	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_related. $this->MimeLineBreak;
	      }
	      $filename_encoding = "=?".strtoupper($charset)."?B?";
	      for ($i=0; $i < sizeof($FinalInlineList); $i++) {
	      	$AttachmentContent = $this->IMapCache->Get_Attachment($FinalInlineList[$i]['PartNumber'],$UID,$Folder);

	     		$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
		     	$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-ID: '.$FinalInlineList[$i]['RealContentID'].$this->MimeLineBreak;
					$mime .= $this->MimeLineBreak;
		        
		      $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
		     	$mime .= $this->MimeLineBreak;
		     	$mime .= $this->MimeLineBreak;
		      if ($i != (sizeof($FinalInlineList)-1)) {
		      	$mime .= "--" . $mime_boundary_related. $this->MimeLineBreak;
		      }
		      else {
		      	$mime .= "--" . $mime_boundary_related. "--" .$this->MimeLineBreak;
		      	$mime .= $this->MimeLineBreak;
		      }
	      }
	      $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
	    }
    	else if ($isRelated) {
    		//$mime_boundary_related = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Related
    		$mime_boundary_alternative = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Alternative
    		/*$mime .= "Content-Type: multipart/related; boundary=\"$mime_boundary_related\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary_related . $this->MimeLineBreak;*/
        
        /*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';*/
        
        for ($i=0; $i < sizeof($InlineFileList); $i++) {
        	if (stristr($message,$InlineFileList[$i]['FilePath']) !== FALSE) {
	      		$message = str_replace($InlineFileList[$i]['FilePath'],$InlineFileList[$i]['FileContentID'],$message);
	      		$FinalInlineList[] = $InlineFileList[$i];
	      	}
	      }
				
				/*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';
				die;*/
	      if ($isHTMLMessage) {
	        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary_alternative\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($TextMessage)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary_alternative . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary.$this->MimeLineBreak;
	      }
	      else {
	        # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	
	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
	      }
	      $filename_encoding = "=?".strtoupper($charset)."?B?";
	      for ($i=0; $i < sizeof($FinalInlineList); $i++) {
	      	$AttachmentContent = $this->IMapCache->Get_Attachment($FinalInlineList[$i]['PartNumber'],$UID,$Folder);

	     		$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
		     	$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-ID: '.$FinalInlineList[$i]['RealContentID'].$this->MimeLineBreak;
					$mime .= $this->MimeLineBreak;
		        
		      $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
		     	$mime .= $this->MimeLineBreak;
		     	$mime .= $this->MimeLineBreak;
		      if ($i != (sizeof($FinalInlineList)-1)) {
		      	$mime .= "--" . $mime_boundary. $this->MimeLineBreak;
		      }
		      else {
		      	$mime .= "--" . $mime_boundary. "--" .$this->MimeLineBreak;
		      	$mime .= $this->MimeLineBreak;
		      }
	      }
	    }  
	    else {
	    	if ($isHTMLMessage) {
	        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($TextMessage)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. "".$this->MimeLineBreak;
	      }
	      else {
	        # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	
	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
	      }
	      $filename_encoding = "=?".strtoupper($charset)."?B?";
	    }
			
      // handle user's upload attachment
      for ($i=0; $i<sizeof($files); $i++) {
        //$target = $files[$i];
        $data = Get_File_Content($files[$i]);
        $fname = addslashes($fileRealName[$i]);

        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= chunk_split(base64_encode($data));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;

        # Different if end of email
        if ($i==sizeof($files)-1 && sizeof($AttachPartNumber) == 0 && sizeof($TNEFFile) == 0)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak;
     	}
     	
     	// handle forward message attachment
     	for ($i=0; $i< sizeof($AttachPartNumber); $i++) {
     		$AttachmentContent = $this->IMapCache->Get_Attachment($AttachPartNumber[$i],$UID,$Folder);

     		if ($AttachmentContent['MimeType'] == "message/rfc822") {
		     	$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: 7bit'.$this->MimeLineBreak;
					$mime .= 'Content-Disposition: attachment'.$this->MimeLineBreak.$this->MimeLineBreak;
					
					$mime .= $AttachmentContent['Content'];
				}
				else {
					$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
	     		$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-Disposition: attachment;'.$this->MimeLineBreak;
	        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        
	        $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
				}     		
				
				$mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;
        
        if ($i==sizeof($AttachPartNumber)-1 && sizeof($TNEFFile) == 0)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak; 
     	}
     	
     	// handle tnef attachment
     	$lfs = new file_system();
			$personal_path = $PathRelative."src/module/email/view_mail_folder/".$SYS_CONFIG['SchoolCode']."/u".$_SESSION['SSV_USERID']."/tnef/";
     	for ($i=0; $i< sizeof($TNEFFile); $i++) {
     		$FilePath = $personal_path.$TNEFFile[$i];
     		$FileName = $TNEFFile[$i];
     		$Content = $lfs->file_read($FilePath);
     		$MimeType = $lfs->getMIMEType($FilePath);
				
				$mime .= 'Content-Type: '.$MimeType.';'.$this->MimeLineBreak;
     		$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($FileName))."?=\"".$this->MimeLineBreak;
				$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
				$mime .= 'Content-Disposition: attachment;'.$this->MimeLineBreak;
        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($FileName))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
        
        $mime .= chunk_split(base64_encode($Content));
				
				$mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;
        
        if ($i==sizeof($TNEFFile)-1)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak; 
     	}
		}

    $ReturnMsg['to'] = str_replace('\"','"',$to);
    $ReturnMsg['cc'] = str_replace('\"','"',$cc);
    $ReturnMsg['bcc'] = str_replace('\"','"',$bcc);
    $ReturnMsg['subject'] = (trim($subject) == "")? "No Subject":$this->Get_Mail_Format($subject,"UTF-8");
    $ReturnMsg['headers'] = $headers;
    $ReturnMsg['body'] = $mime;
    //echo $mime; die;

    // Kenneth Wong (20080612) Added element headers2 for mail()
    $ReturnMsg['headers2'] = $headers;
    if ($cc != "")
    {
        $ReturnMsg['headers2'] .= "cc: $cc".$this->MimeLineBreak;
    }
    if ($bcc != "")
    {
        $ReturnMsg['headers2'] .= "bcc: $bcc".$this->MimeLineBreak;
    }
    $ReturnMsg['headers2'] .= $HeaderFrom . $this->MimeLineBreak;

    $headers .= $HeaderFrom;
    $headers .= "To: $to".$this->MimeLineBreak;
    $headers .= "cc: $cc".$this->MimeLineBreak;
    $headers .= "bcc: $bcc".$this->MimeLineBreak;
    $headers .= "Subject: ".$this->Get_Mail_Format($subject,"UTF-8").$this->MimeLineBreak;

    $ReturnMsg['FullMimeMsg'] = $headers.$mime;
    
    $ReturnMsg['SortTo'] = trim(str_replace('"','',$ReturnMsg['to']));
    $ReturnMsg['Boundary'] = $mime_boundary;
		
		//$this->IMapCache->Close_Connect();
    return $ReturnMsg;
  }
	  
	#####################################################################
	function Get_Mail_Attach_File_Map_List($MessageUID){
		$db = new database();
		$returnArr = array();
		$sql = "SELECT 
					FileID, UserID, UploadTime, DraftMailUID,
					CAST(CAST(OriginalFileName AS varbinary(max)) AS image) as OriginalFileName,
					EncodeFileName 
				FROM 
					MAIL_ATTACH_FILE_MAP 
				WHERE 
					UserID = ".$_SESSION['SSV_USERID']." AND 
					( 
						(DraftMailUID IS NULL AND OrgDraftMailUID = '".$MessageUID."') OR 
						 DraftMailUID = '".$MessageUID."'
					)
				";
		$returnArr = $db->returnArray($sql, 6);
		return $returnArr;
	}
	
	# Remove the Unlink Attached Files which may created in before forwarded mail 
	# by providing a UID of mail 
	# if mail is newly composed , MessageUID is NULL
	# if mail is compose forward mail , MessageUID is +ve Integer & MailAction is Forward
	# if mail is draft mail , MessageUID is +ve Integer & MailAction is NULL
	function Remove_Unlink_Attach_File($MessageUID="", $MailAction="", $ComposeDatetime=""){
		global $SYS_CONFIG, $UserID, $file_path;
		$FileList = '';
		$FileArr = array();
		
		$fs = new libfilesystem();
		$db  = new libdb();
		
		# Get File List from DB
		$sub_sql_1 = ($MailAction == "Forward" && $MessageUID != "") ? "= $MessageUID" : "IS NULL";
		$sub_sql_2 = ($MailAction != "Forward" && $MessageUID != "") ? "= $MessageUID" : "IS NULL";
		$sql = "SELECT 
					FileID, EncodeFileName FROM MAIL_ATTACH_FILE_MAP 
				WHERE 
					UserID = ".$UserID." AND 
					DraftMailUID $sub_sql_2 AND 
					OrgDraftMailUID $sub_sql_1";
		if($ComposeDatetime != "") $sql .= " AND DATE_FORMAT(ComposeTime,'%Y-%m-%d %H:%i:%s') = '$ComposeDatetime' ";
		$FileArr = $db->returnArray($sql, 2);
		
		//$personal_path = $SYS_CONFIG['sys_user_file']."/file/mail/u".$_SESSION['SSV_USERID'];
		$personal_path = "$file_path/file/gamma_mail/u$UserID";
		
		if (!is_dir($personal_path))
		{
			$fs->folder_new($personal_path);
		}
		
		if(count($FileArr) > 0){
			# delete the file in file system
			for($i=0; $i<sizeof($FileArr) ; $i++){
				list($FileID, $FileName) = $FileArr[$i];
				$loc = $personal_path."/".$FileName;
				$fs->file_remove($loc);
				
				$FileList .= (($i != 0) ? ', ' : '').$FileID;
			}
			# delete the file in db
			if($FileList != ""){
				$sql = "DELETE FROM MAIL_ATTACH_FILE_MAP WHERE 
						UserID = ".$UserID." AND FileID IN ($FileList)";
				$db->db_db_query($sql);
			}
		}
	}
	
	#############################################################################################
	# Get the valid format of date before passing to the imap_search
	function Get_Date_Of_Imap_Search($date){
		$returnStr = '';
		if($date != ""){
			$tmp = explode("-", $date);
			$month = $this->Get_Month_Short_Form($tmp[1]); 
			$returnStr = ((int)$tmp[2]).'-'.$month.'-'.$tmp[0];
		}
		return $returnStr;
	}
	
	function Get_Month_Short_Form($MonthNum) {
		$month = (int)$MonthNum;
		switch ($month) {
			case 1: $ReturnVal = 'Jan'; break;
			case 2: $ReturnVal = 'Feb'; break;
			case 3: $ReturnVal = 'Mar'; break;
			case 4: $ReturnVal = 'Apr'; break;
			case 5: $ReturnVal = 'May'; break;
			case 6: $ReturnVal = 'Jun'; break;
			case 7:	$ReturnVal = 'Jul'; break;
			case 8: $ReturnVal = 'Aug'; break;
			case 9: $ReturnVal = 'Sep'; break;
			case 10: $ReturnVal = 'Oct'; break;
			case 11: $ReturnVal = 'Nov'; break;
			case 12: $ReturnVal = 'Dec'; break;
			default: $ReturnVal = 'Jan'; break;
		}
		return $ReturnVal;
	}
	
	function Get_All_Folder_List() {
	  Global $Lang, $SYS_CONFIG;
	  	//if(isset($this->AllFolderList) && sizeof($this->AllFolderList)>0){
		//	$FolderList = $this->AllFolderList;
		//}else{
        //	$FolderList = $this->getMailFolders();
        //	$this->AllFolderList = $FolderList;
		//}
	  	$FolderList = $this->getAllFolderList();
		$Result = array();
		for ($i=0; $i< sizeof($FolderList); $i++) {
			$Result[] = $FolderList[$i]['FolderFullPath'];
		}
	  	
	  return $Result;
	}
	
	# Check the mail whether it contains attachment or not
		function Check_Any_Mail_Attachment($msgno){
		# Get the mail structure in object
		$struct = imap_fetchstructure($this->inbox, $msgno); 
		//debug_pr($struct);
		$part_cnt = $this->Recursive_Check_Attachment($struct);
		return $part_cnt > 0;
		/*
		# FIND OUT IF MULTIPARTs
		$Status = false;
		if(count($struct->parts) > 1 ){     
			$Y = 0; 
			while($Y < count($struct->parts)){ 
				# if this is an attachment! 
				$ctr=0;
				while($ctr < count($struct->parts[$Y]->parameters))
				{
					if(is_array($struct->parts[$Y]->parameters))
					{
						if(in_array(strtolower($struct->parts[$Y]->parameters[$ctr]->attribute),array("name","filename"))){ 
							$Status = true;
							$ctr = count($struct->parts[$Y]->parameters);
						}
					}
					$ctr++;
				} 
				
//				if( $struct->parts[$Y]->ifdisposition == true and $struct->parts[$Y]->ifdparameters == true ){ 
//					$Status = true;
//				} 
				$Y = ($Status == true) ? count($struct->parts) : $Y + 1;    
			} 
		} 
		return $Status;
		*/
	}
	
	function Recursive_Check_Attachment($part)
	{
		$part_cnt = 0;
		if(is_array($part->parameters)){
			for($i=0;$i<count($part->parameters);$i++)
			{
				if(in_array(strtolower($part->parameters[$i]->attribute),array("name","filename"))){ 
					$part_cnt += 1;
				}
			} 
		}
		if($part->ifdisposition == '1'){
			if(in_array(strtolower($part->disposition), array('attachment','inline'))){
				$part_cnt += 1;
			}
		}
		if(is_array($part->parts) && count($part->parts)>0){
			for($i=0;$i<count($part->parts);$i++){
				$part_cnt += $this->Recursive_Check_Attachment($part->parts[$i]);
			}
		}
		return $part_cnt;
	}
	
	function Scan_Mail_Headers($Folder=''){
		 imap_reopen($this->inbox,$this->getMailServerRef().$this->encodeFolderName($Folder));
		 return imap_headers($this->inbox);
	}
	
	function Search_Attachment($SearchStr=''){
		$IMapCheck = imap_check($this->inbox);
		$Temp = false;
		if (trim($SearchStr) != '' ) {
			$Temp = array();
			for ($MailIdx = 1; $MailIdx <= $IMapCheck->Nmsgs; $MailIdx++) {
				$MailStruct[$MailIdx] = imap_fetchstructure($this->inbox, $MailIdx);
				// echo count($MailStruct[$MailIdx]->parts).'<br>';
				if (count($MailStruct[$MailIdx]->parts) > 1) {
					foreach($MailStruct[$MailIdx]->parts as $Key => $curPart) {
						if ($curPart->ifdisposition == true && $curPart->ifdparameters == true) {
							// debug_r($curPart->dparameters);
							// echo count($curPart->dparameters).'<br>';
							if (stripos(imap_utf8($curPart->dparameters[0]->value),$SearchStr) !== false) {
								// echo $IMap->Get_UID($MailIdx).' : '.$curPart->dparameters[0]->value.'<br>';
								$Temp[] = $MailIdx;
							}
						}
					}
				}
			}
		}
		return $Temp;
	}

	# following added by Marcus 
	
	# Copy from imap_gamma_ui.php
	function Get_Sort_Index($IMap=NULL, $imapInfo=array(), $search="", $seen="ALL", $useUID=0){
		
		//$lui->logTime['Sort_Mail'] = Get_Timer();
		//Write_Message_Log('Gamma', "Sort_Begin: ".date("H:i:s") . " " . microtime());
		
		$array = array();
		$SE_UID = $useUID? SE_UID : 0;
		if($IMap != NULL){			
			//Folder, message or search?
			if ($imapInfo['displayMode'] == "folder" OR $imapInfo['displayMode'] == "message") {
				//$array=imap_sort($imap, $sort, $reverse, 1) ; did not seem to work, so need to do it manually!
				if ($imapInfo['sort'] == "SORTARRIVAL") {
					$array = imap_sort($IMap->inbox, SORTARRIVAL, $imapInfo['reverse'],$SE_UID,$seen) ;
				}
				else if ($imapInfo['sort'] == "SORTSUBJECT") {
					$array = imap_sort($IMap->inbox, SORTSUBJECT, $imapInfo['reverse'],$SE_UID,$seen) ;
				}
				else if ($imapInfo['sort'] == "SORTFROM") {
					$array = imap_sort($IMap->inbox, SORTFROM, $imapInfo['reverse'],$SE_UID,$seen) ;
				}
				else if ($imapInfo['sort'] == "SORTSIZE") {
					$array = imap_sort($IMap->inbox, SORTSIZE, $imapInfo['reverse'],$SE_UID,$seen) ;
				}
				else if ($imapInfo['sort'] == "SORTTO") {
					$array = imap_sort($IMap->inbox, SORTTO, $imapInfo['reverse'],$SE_UID,$seen) ;
				}
				else {
					$array=imap_sort($IMap->inbox, SORTARRIVAL, $imapInfo['reverse'],$SE_UID,$seen) ;
				}
			}
			else if ($imapInfo['displayMode'] == "search") {
				if (!($search == "")) {
					if($useUID) {
						$array = imap_search($IMap->inbox,'SUBJECT "' . $search . '"', SE_UID) ;
					}else{
						$array = imap_search($IMap->inbox,'SUBJECT "' . $search . '"') ;
					}
				}
			}
		} 
		//Write_Message_Log('Gamma', "Sort Mail: ".(Get_Timer() - $lui->logTime['Sort_Mail']));
		//Write_Message_Log('Gamma', "Sort_End: ".date("H:i:s") . " " . microtime());
		return $array;
	}	  
	
	function displayMsg($Folder, $Uid, $PartNumber='')
	{
		global $Lang,$CurUserType;
		
		$x='<table width="100%" cellspacing="0" cellpadding="0" border="0" id="MailDisplayTable">'; 
        	$x.='<tbody>';
				//$x.= $EmailDisplayContentRow = $this->Get_Mail_Content_UI($EmailContent[0],$Folder);
				if(trim($PartNumber)=='')
					$x.= $this->Get_Mail_Top_Bar($Folder, $Uid);
				$x.= $this->Get_Mail_Content_UI($Folder, $Uid,$PartNumber);
			$x.='</tbody>';
		$x.='</table>';		
		
		
		return $x;
	}
	
//	function Get_Mail_Content($thisEmailContent,$Folder,$LoadFromAjax=0)
//	{
//		global $Lang;
//		
//		if($LoadFromAjax==1)
//		{
//			$display_gmailtopbar = 'style="display:none"';
//			$display_gmaildesc = 'style="display:block"';
//			$display_gmailcontent = 'style="display:none"';
//		}
//		else 
//		{
//			$display_gmailtopbar = 'style="display:none"';
//			$display_gmaildesc = 'style="display:none"';
//			$display_gmailcontent = 'style="display:block"';
//		}
////		else if($GmailMode==0 && $GmailMode!='')
////		{
////			$display_gmailtopbar = 'style="display:none"';
////			$display_gmaildesc = 'style="display:block"';
////			$display_gmailcontent = 'style="display:none"';
////		}
////		else // original email
//		
//		$thisUid =  $thisEmailContent["uid"];
//		$thisFolder =  $thisEmailContent["Folder"]?$thisEmailContent["Folder"]:$Folder;
//		
//		// prepare data for top mail description
//		// sender name
//		$fromDetail = $thisEmailContent['fromDetail'][0];
//		$emailaddr = $fromDetail->mailbox."@".$fromDetail->host;
//		$DisplaySender = $this->MIME_Decode($fromDetail->personal);
//		$DisplaySender = $this->Format_Email_Display_Name($DisplaySender);
//		$DisplayTopSender= $DisplaySender?$DisplaySender:$emailaddr;
//		
//		$text_message = html2txt($thisEmailContent['message']);
//		$MailDesc = mb_strlen($text_message)>65?(mb_substr($text_message,0,65,mb_detect_encoding($text_message))."..."):$text_message;
//		
//		//------ mail top desc bar start -----------
//		$mailcss = "gmailcontent$thisUid";
//		$maildesccss = "gmaildesc$thisUid";		
//		$x.='<tr class="gmailtopbar" '.$display_gmailtopbar.'  onclick="showhidecontent(\''.$thisUid.'\',\''.$thisFolder.'\',this) ">'; 
//			$x.='<td colspan="2" style="border-top:1px solid #eeeeee;">';
//				$x .= '<table cellpadding=2 cellspacing=4 border=0 width="100%">';
//				$x.='<tr style="cursor:pointer">'; 
//					$x.='<td class="tabletool" width="150px">'.$DisplayTopSender.':</td>';
//					$x.='<td class="tabletextremark '.$maildesccss.'" ><span '.$display_gmaildesc.'>'.$MailDesc.'</span></td>';
//					$x.='<td class="'.$maildesccss.'" align="right" nowrap><span '.$display_gmaildesc.'>'.$thisEmailContent['dateReceive'].'</span></td>';
//				$x .= '</table>';
//			$x .='</td>'; 
//		$x.='</tr>'; 
//		//------ mail top desc bar end -----------
//	
//		//if(!$LoadFromAjax)
//		{
//			// ------- mail tool bar start ------------					
//			$x.='<tr>'; 
//				$x.='<td colspan="2">';
//					$x.='<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="'.$mailcss.'" bgcolor="#eeeeee" '.$display_gmailcontent.'>'; 
//		          		$x.='<tbody><tr>'; 
//		            		$x.='<td>'; 
//		                		$x.='<table cellspacing="0" cellpadding="2" border="0">'; 
//		                      	$x.='<tbody>';
//		                      		$x.='<tr> ';
//		                      		# Reply
//		                        	$x.='<td> ';
//		                        	$x.='<a class="contenttool" href="javascript:void(0);" onclick="SubmitMailAction(\'Reply\','.$thisUid.',\''.$thisFolder.'\')">'; 
//		                        	$x.='<img height="20" width="20" border="0" align="absmiddle" src="/images/2009a/iMail/btn_reply.gif">'; 
//		                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="reply">'.$Lang["Gamma"]["Reply"].'</span>'; 
//		                        	$x.='</a>'; 
//		                        	$x.='</td>'; 
//		                        	$x.='<td><img src="/images/2009a/10x10.gif"></td>'; 
//		                        	# reply all
//		                        	$x.='<td>'; 
//		                        	$x.='<a class="contenttool" href="javascript:void(0);" onclick="SubmitMailAction(\'ReplyAll\','.$thisUid.',\''.$thisFolder.'\')">'; 
//		                        	$x.='<img height="20" width="20" border="0" align="absmiddle" src="/images/2009a/iMail/btn_reply_all.gif">'; 
//		                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="replyall">'.$Lang["Gamma"]["ReplyAll"].'</span>'; 
//		                        	$x.='</a>'; 
//		                        	$x.='</td>'; 
//		                        	$x.='<td><img src="/images/2009a/10x10.gif"></td>'; 
//		                        	# Forward
//		                        	$x.='<td>'; 
//		                        	$x.='<p>'; 
//		                        	$x.='<a class="contenttool" href="javascript:void(0);" onclick="SubmitMailAction(\'Forward\','.$thisUid.',\''.$thisFolder.'\')">'; 
//		                        	$x.='<img height="20" width="20" border="0" align="absmiddle" src="/images/2009a/iMail/btn_forward.gif">'; 
//		                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="forward">'.$Lang["Gamma"]["Forward"].'</span>';  
//		                        	$x.='</a>'; 
//		                        	$x.='</p>'; 
//		                        	$x.='</td>'; 
//		                        	$x.='<td><img src="/images/2009a/10x10.gif"></td>'; 
//		                        	# Remove
//		                        	$x.='<td>'; 
//		                        	$x.='<p>'; 
//		                        	$x.='<a class="contenttool" href="remove.php?Folder='.$thisFolder.'&Uid[]='.$thisUid.'&page_from=viewmail.php">'; 
//		                        	$x.='<img height="20" width="20" border="0" align="absmiddle" src="/images/2009a/iMail/btn_trash.gif">';  
//		                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="delete">'.$Lang["Gamma"]["Remove"].'</span>'; 
//		                        	$x.='</a>'; 
//		                      	$x.='</p></td></tr>'; 
//		                  		$x.='</tbody></table>'; 
//				    		$x.='</td>'; 
//				    		#view email source 
//				    		$x.='<td align="right">'; 
//								$x.='<table cellspacing="0" cellpadding="2" border="0">'; 
//								$x.='<tbody><tr>'; 
//									$x.='<td><img src="/images/2009a/10x10.gif"></td>'; 
//									$x.='<td>'; 
//									$x.='<p><a class="contenttool" href="javascript:viewMessageSource('.$thisUid.',\''.$thisFolder.'\')">'; 
//									$x.='<img height="20" width="20" border="0" align="absmiddle" src="/images/2009a/iMail/btn_view_source.gif">'; 
//									$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="view_source">View Message Source</span>'; 
//									$x.='</a>'; 
//									$x.='</p>'; 
//									$x.='</td>';  
//								$x.='</tr>'; 
//								$x.='</tbody></table>'; 
//				   			$x.='</td>'; 
//				  		$x.='</tr>'; 
//						$x.='</tbody></table>';
//				$x.='</td>';
//			$x.='</tr>'; 
//			// ------- mail tool bar end ------------
//			
//			$x.='<tr>'; 
//				$x.='<td colspan="2">';
//					//------ mail header start -----------
//					$x.='<table width="100%" cellspacing="0" cellpadding="5" border="0" class="'.$mailcss.'" '.$display_gmailcontent.'>';
//						$x.='<tr>'; 
//							$x.='<td colspan="2">'; 
//							$x.='<table width="100%" cellspacing="0" cellpadding="5" border="0" >'; 
//							$x.='<tbody>';
//		
//							# date
//							$x.='<tr>'; 
//					            $x.='<td width="120" align="left" class="tabletext"><span class="iMailComposefieldtitle" id="date">'.$Lang['Gamma']['Date'].'</span></td>'; 
//					            $x.='<td width="4" align="left" class="tabletext"><span class="iMailComposefieldtitle"> :</span></td>'; 
//					            $x.='<td class="tabletext">'.$thisEmailContent['dateReceive'].'</td>'; 
//							$x.='</tr>'; 
//		
//							# sender
//							$sender = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplaySender).'&to='.$emailaddr.'">'.$DisplaySender.' &lt;'.$emailaddr.'&gt;</a>';
//					      	$x.='<tr>';
//					        	$x.='<td align="left" class="tabletext"><span id="sender" class="iMailComposefieldtitle">'.$Lang['Gamma']['Sender'].'</span></td>';			
//					        	$x.='<td align="left" class="tabletext"><span class="iMailComposefieldtitle">:</span></td>';	
//					        	$x.='<td class="tabletext"><span class="tabletext">'.$sender.'</span></td>';			
//					      	$x.='</tr>';
//					      	
//					      	# to
//							if(!empty($thisEmailContent['toDetail']))
//					      	{
//					      		$toAccountAry = $toAccountShowArr = $toAccountHideArr = array();
//						      	$toDetail = $thisEmailContent['toDetail'];
//						      	for($i=0;$i<sizeof($toDetail);$i++)
//						      	{
//									$emailaddr = $toDetail[$i]->mailbox."@".$toDetail[$i]->host;
//									$DisplayName = $this->MIME_Decode($toDetail[$i]->personal);
//									$toAccountAry[] = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplayName).'&to='.$emailaddr.'">'.$DisplayName.' &lt;'.$emailaddr.'&gt;</a>';
//						      	}
//						      	if(count($toAccountAry)>8)
//						      	{
//						      		for($i=0;$i<count($toAccountAry);$i++)
//						      		{
//						      			if($i<4)
//						      				$toAccountShowArr[] =  $toAccountAry[$i];
//						      			else
//						      				$toAccountHideArr[] = $toAccountAry[$i];
//						      		}
//						      		$toAccountShow = implode(", ",$toAccountShowArr).'<span id="ShowTo'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideTo'.$thisUid.'\').show(); $(\'#ShowTo'.$thisUid.'\').hide();">[Show Details]</a></span>';
//						      		$toAccountHide = '<span id="HideTo'.$thisUid.'" style="display:none;">,'.implode(", ",$toAccountHideArr).'</span>';
//						      		$toAccount = $toAccountShow.$toAccountHide;
//						      		
//						      	}else if(count($toAccountAry)>1)
//						      		$toAccount = implode(", ",$toAccountAry);
//						      	else
//						      		$toAccount = $toAccountAry[0];
//						      
//						      	
//						      	$x.='<tr>';
//						        	$x.='<td align="left" class="tabletext"><span id="sender" class="iMailComposefieldtitle">'.$Lang['Gamma']['To'].'</span></td>';			
//						        	$x.='<td align="left" class="tabletext"><span class="iMailComposefieldtitle">:</span></td>';	
//						        	$x.='<td class="tabletext"><span class="tabletext">'.$toAccount.'</span></td>';			
//						      	$x.='</tr>';
//					      	}
//					      	
//					      	# cc
//					      	if(!empty($thisEmailContent['ccDetail']))
//					      	{
//					      		$ccAccountAry = $ccAccountShowArr = $ccAccountHideArr = array();
//						      	$ccDetail = $thisEmailContent['ccDetail'];
//						      	for($i=0;$i<sizeof($ccDetail);$i++)
//						      	{
//									$emailaddr = $ccDetail[$i]->mailbox."@".$ccDetail[$i]->host;
//									$DisplayName =$this->MIME_Decode($ccDetail[$i]->personal);
//									$ccAccountAry[] = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplayName).'&to='.$emailaddr.'">'.$DisplayName.' &lt;'.$emailaddr.'&gt;</a>';
//						      	}
//		
//						      	if(count($ccAccountAry)>8)
//						      	{
//						      		for($i=0;$i<count($ccAccountAry);$i++)
//						      		{
//						      			if($i<5)
//						      				$ccAccountShowArr[] =  $ccAccountAry[$i];
//						      			else
//						      				$ccAccountHideArr[] = $ccAccountAry[$i];
//						      		}
//						      		$ccAccountShow = implode(", ",$ccAccountShowArr).'<span id="ShowCc'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideCc'.$thisUid.'\').show(); $(\'#ShowCc'.$thisUid.'\').hide();">[Show Details]</a></span>';
//						      		$ccAccountHide = '<span id="HideCc'.$thisUid.'" style="display:none;">,'.implode(", ",$ccAccountHideArr).'</span>';
//						      		$ccAccount = $ccAccountShow.$ccAccountHide;
//						      		
//						      	}else if(count($ccAccountAry)>1)
//						      		$ccAccount = implode(", ",$ccAccountAry);
//						      	else
//						      		$ccAccount = $ccAccountAry[0];
//						      	$x.='<tr>';
//						        	$x.='<td align="left" class="tabletext"><span id="sender" class="iMailComposefieldtitle">'.$Lang['Gamma']['cc'].'</span></td>';			
//						        	$x.='<td align="left" class="tabletext"><span class="iMailComposefieldtitle">:</span></td>';	
//						        	$x.='<td class="tabletext"><span class="tabletext">'.$ccAccount.'</span></td>';			
//						      	$x.='</tr>';
//					      	}
//		  	   				//$x.='</tr>';
//		        	  	    #Subject
//		  	        	  	$Subject = $thisEmailContent['subject']?$thisEmailContent['subject']:"No Subject";
//							
//			              	$x.='<tr>';			
//			                	$x.='<td align="left" class="tabletext"><span id="subject" class="iMailComposefieldtitle">'.$Lang['Gamma']['Subject'].'</span></td>';			
//			                	$x.='<td align="left" class="tabletext"><span class="iMailComposefieldtitle">:</span></td>';			
//			                	$x.='<td class="tabletext"><span class="tabletext">'.htmlspecialchars($Subject).'</span></td>';			
//			              	$x.='</tr>';			
//			              	
//			              	#Attachment
//			              	$file='';
//		  	        	  	$Attachment = $thisEmailContent["attach_parts"];
//		  	        	  	
//							if (!(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
//								
//								
//								// short list	
//								for ($i=0; $i< sizeof($Attachment); $i++) {	
//									
//									if (!$Attachment[$i]['IsTNEF'] ) {
//										if ($Attachment[$i]['AttachType'] == "File")
//											$file .= '<a class="tabletool" href="cache_view_attachment.php?PartNumber='.$Attachment[$i]['PartID']."&Folder=".urlencode($thisFolder).'&MessageID='.$thisUid.'">';
//										else
//											$file .= '<a class="tabletool" href="javascript:window.open(\'viewmail.php?PartNumber='.$Attachment[$i]['PartID'].'&Folder='.urlencode($thisFolder).'&uid='.$thisUid.'\');">';
//									}
//									else {
//										$file .= '<a class="tabletool" href="'.$Attachment[$i]['FilePath'].'" target="_blank" >';
//									}
//									$file .= '<img hspace="2" vspace="2" border="0" align="absmiddle" src="/images/file.gif">';
//									$file .= $Attachment[$i]['FileName'];
//									$file .= '</a>';
//									$file .= "<br> ";
//									
//								}
//								$x .= '	<tr>';
//			                    	$x .= '<td align="left" class="tabletext" valign=top><span id="attachment" class="iMailComposefieldtitle">'.$Lang["Gamma"]["Attachment"].'</span></td>';
//			                    	$x .= '<td align="left" class="tabletext" valign=top><span class="iMailComposefieldtitle">:</span></td>';
//			                    	$x .= '<td class="tabletext"><span class="tabletext">'.$file;
//									$x .= '</span></td>';
//			                  	$x .= '</tr>';
//				                $x .= '	<tr>';
//									$x .= '<td align="left" class="tabletext" valign=top colspan=3><a class="tabletool" href="javascript:void(0)"; onclick="window.open(\'view_all_image.php?&Folder='.urlencode($thisFolder).'&uid='.$thisUid.'\');">test</a></td>';
//								$x .= '</tr>';  	
//							}
//							
//						$x.='</tbody></table>'; 			
//		                $x.='</td>'; 			
//		            	$x.='</tr>'; 											
//						//------ mail header end -----------
//						
//						//------ mail message start ------------
//						# message
//						$x.='<tr>';
//							$x.='<td colspan="2">';
//							$x.='<table width="100%" cellspacing="0" cellpadding="10" border="0">';
//							$x.='<tbody><tr>';
//								$x.='<td>';
//									$x.= $thisEmailContent['message'];
//								$x.='</td>';
//							$x.='</tr>';
//							$x.='</tbody></table>';
//							$x.='</td>';
//						$x.='</tr>';
//						//------ mail message end ------------
//					$x.='</table>';
//				$x.='</td>';
//			$x.='</tr>';
//		}
//		
//		return $x;
//	}
	/*
	function Get_Gmail_Mode_Mail_Top_Bar($Folder, $Uid, $PartNumber,$LoadFromAjax='')
	{
		$EmailContent = $this->getMailRecord($Folder, $Uid, $PartNumber);
		$EmailContent[0]["Folder"] = $Folder;
		# search for mails with similar title
		# trim RE:/FW:/FWD:
		$trimed_subject = $this->TrimSubjectPrefix($EmailContent[0]['subject']);
		
		//20100812
		$FolderList = array($this->InboxFolder,$this->SentFolder);
//		#search sent and inbox
//		if(in_array($Folder,$FolderList) && !empty($trimed_subject) && strtolower($trimed_subject)!="no subject")
//		{
			if($trimed_subject!='')
			{
				$SearchResult = array();
				for ($i=0; $i<sizeof($FolderList) ; $i++) 
				{
					$Temp = $this->Search_Mail($FolderList[$i], '', '', '' ,'',$trimed_subject, '', '', '', '',0,ALL);
					if(is_array($Temp))
						$SearchResult = array_merge($SearchResult,$Temp);
				}
			}

			//$thisDate=$EmailContent[0]['dateReceive'];
			foreach((array)$SearchResult as $tmp=>$mailinfo)
			{
				list($Mailbox,$MailNo) = $mailinfo;
				if($lastMailbox != $Mailbox || empty($lastMailbox))
				{
					$this->Go_To_Folder($Mailbox);
					$lastMailbox = $Mailbox;
				}
				$thisUid = $this->Get_UID($MailNo);
				
				$thisContent = $this->getMailRecord($Mailbox, $thisUid);
				
				if($this->TrimSubjectPrefix($thisContent[0]['subject'])==$trimed_subject && ($thisContent[0]['uid']!=$Uid || $Mailbox != $Folder))
				{
					$thisContent[0]["Folder"] = $Mailbox;
					$RelatedMail[] = $thisContent[0];
				}
				
			}
			
			if(is_array($RelatedMail))
				$AllMail = $RelatedMail;
			
			$AllMail[] = $EmailContent[0];
			sortByColumn2($AllMail,"dateReceive");
			
			foreach((array) $AllMail as $thisEmailContent)
			{
				if($thisEmailContent["uid"]==$Uid  && $thisEmailContent["Folder"]==$Folder)
					$x .= "|=Current_Email_Delimiter=|";
				else
					$x .= $this->Get_Mail_Top_Bar($thisEmailContent["Folder"],$thisEmailContent["uid"],$LoadFromAjax);
			}// end loop all mail

			return $x;
	}
	
	function Get_Mail_Top_Bar($Folder, $Uid, $LoadFromAjax=0)
	{
		global $Lang, $image_path,$LAYOUT_SKIN ;
		
		if($LoadFromAjax==1)
			$display_gmaildesc = 'style="display:block"';
		else
			$display_gmaildesc = 'style="display:none"';
		
		$thisContent = $this->getMailRecord($Folder, $Uid);
		$thisEmailContent = $thisContent[0];
		
		$thisUid =  $thisEmailContent["uid"];
		$thisFolder =  $thisEmailContent["Folder"]?$thisEmailContent["Folder"]:$Folder;
		
		// prepare data for top mail description
		// sender name
		$fromDetail = $thisEmailContent['fromDetail'][0];
		$emailaddr = $fromDetail->mailbox."@".$fromDetail->host;
		$DisplaySender = $this->MIME_Decode($fromDetail->personal);
		$DisplaySender = $this->Format_Email_Display_Name($DisplaySender);
		$DisplayTopSender= $DisplaySender?$DisplaySender:$emailaddr;
		
		$text_message = html2txt($thisEmailContent['message']);
		$MailDesc = mb_strlen($text_message)>65?(mb_substr($text_message,0,65,mb_detect_encoding($text_message))."..."):$text_message;
		
		//------ mail top desc bar start -----------
		$mailcss = "gmailcontent$thisUid";
		$maildesccss = "gmaildesc$thisUid";	
		//20100812
		$senderclass = $Folder==$this->SentFolder?"indexcalendartoday":"tabletool";	
		
		$x.='<tr class="gmailtopbar" name="gmailtopbar'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" style="display:none"  onclick="showhidecontent(\''.$thisUid.'\',\''.$thisFolder.'\',this) ">'; 
			$x.='<td colspan="2" style="border-top:1px solid #9999BB;">';
				$x .= '<table cellpadding=2 cellspacing=4 border=0 width="100%">';
					$x .= '<tr style="cursor:pointer">'; 
						$x.='<td class="'.$senderclass.'" width="150px" nowrap>'.$DisplayTopSender.':</td>';
						$x.='<td class="tabletextremark '.$maildesccss.'" name="gmaildesc'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'"><span '.$display_gmaildesc.'>'.$MailDesc.'</span></td>';
						$x.='<td class="'.$maildesccss.'" name="gmaildesc'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" align="right" nowrap><span '.$display_gmaildesc.'>'.$thisEmailContent['dateReceive'].'</span></td>';
						$x.='<tr id="loadingrow'.$thisUid.'" name="loadingrow'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" style="display:none">';
						 $x.='<td colspan="3" class="tableremarks">Loading...<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/indicator.gif" /></td>';
					$x.='</tr>';
				$x .= '</table>';
			$x .='</td>';
			$x .= '<input type="hidden" name="hTopBarFolder[]" value="'.$thisFolder.'" />';
			$x .= '<input type="hidden" name="hTopBarUid[]" value="'.$thisUid.'" />';
		$x.='</tr>'; 
 
		//------ mail top desc bar end -----------
		
		return $x;
	}
	*/
	// Get Mail header info instead of get whole mail content
	function Get_Gmail_Mode_Mail_Top_Bar($Folder, $Uid, $PartNumber,$LoadFromAjax='')
	{
		$EmailHeader = $this->getMailRawHeader($Folder, $Uid);
		$EmailContent[0]['uid'] = $Uid;
		$EmailContent[0]['Folder'] = $Folder;
		$EmailContent[0]['subject'] = $this->MIME_Decode($EmailHeader->subject);
		$EmailContent[0]['dateReceive'] = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($EmailHeader->date)));
		# search for mails with similar title
		# trim RE:/FW:/FWD:
		$trimed_subject = $this->TrimSubjectPrefix($EmailContent[0]['subject']);
		
		//20100812
		$FolderList = array($this->InboxFolder,$this->SentFolder);
//		#search sent and inbox
//		if(in_array($Folder,$FolderList) && !empty($trimed_subject) && strtolower($trimed_subject)!="no subject")
//		{
			if($trimed_subject!='')
			{
				$SearchResult = array();
				if($this->CacheMailToDB == true){ // cache mode
					$SearchInfo = array('Subject'=>$trimed_subject,
										'Folder'=>$this->getAllFolderList);
					$SearchResult = $this->Search_Cached_Mail($SearchInfo);
				}else{ // normal mode
					for ($i=0; $i<sizeof($FolderList) ; $i++) 
					{
						$Temp = $this->Search_Mail($FolderList[$i], '', '', '' ,'',$trimed_subject, '', '', '', '',0,'ALL');
						if(is_array($Temp))
							$SearchResult = array_merge($SearchResult,$Temp);
					}
				}
			}
			
			if($this->CacheMailToDB == true){
				$x = '';
				for($i=0;$i<sizeof($SearchResult);$i++)
				{
					$Mailbox = $SearchResult[$i]['Folder'];
					$MailUid = $SearchResult[$i]['UID'];
					if($this->TrimSubjectPrefix($SearchResult[$i]['Subject'])==$trimed_subject)
					{
						if($MailUid==$Uid && $Mailbox==$Folder)
							$x .= "|=Current_Email_Delimiter=|";
						else
							$x .= $this->Get_Mail_Top_Bar($Mailbox,$MailUid,$LoadFromAjax);
					}
				}
			}else{
				//$thisDate=$EmailContent[0]['dateReceive'];
				foreach((array)$SearchResult as $tmp=>$mailinfo)
				{
					list($Mailbox,$MailNo) = $mailinfo;
					if($lastMailbox != $Mailbox || empty($lastMailbox))
					{
						$this->Go_To_Folder($Mailbox);
						$lastMailbox = $Mailbox;
					}
					$thisUid = $this->Get_UID($MailNo);
					
					//$thisContent = $this->getMailRecord($Mailbox, $thisUid);
					$thisHeader = $this->getMailRawHeader($Mailbox, $thisUid);
					$thisContent[0]['subject'] = $this->MIME_Decode($thisHeader->subject);
					$thisContent[0]['Folder'] = $Mailbox;
					$thisContent[0]['uid'] = $thisUid;
					$thisContent[0]['dateReceive'] = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($thisHeader->date)));
					
					if($this->TrimSubjectPrefix($thisContent[0]['subject'])==$trimed_subject && ($thisContent[0]['uid']!=$Uid || $Mailbox != $Folder))
					{
						//$thisContent[0]["Folder"] = $Mailbox;
						$RelatedMail[] = $thisContent[0];
					}
					
				}
				
				if(is_array($RelatedMail))
					$AllMail = $RelatedMail;
				
				$AllMail[] = $EmailContent[0];
				sortByColumn2($AllMail,"dateReceive");
			
				foreach((array) $AllMail as $thisEmailContent)
				{
					if($thisEmailContent["uid"]==$Uid  && $thisEmailContent["Folder"]==$Folder)
						$x .= "|=Current_Email_Delimiter=|";
					else
						$x .= $this->Get_Mail_Top_Bar($thisEmailContent["Folder"],$thisEmailContent["uid"],$LoadFromAjax);
				}// end loop all mail
			}
			
			return $x;
	}
	// Get header info instead of whole mail
	function Get_Mail_Top_Bar($Folder, $Uid, $LoadFromAjax=0)
	{
		global $Lang, $image_path,$LAYOUT_SKIN;
		
		if($LoadFromAjax==1)
			$display_gmaildesc = 'style="display:block"';
		else
			$display_gmaildesc = 'style="display:none"';
		
		$thisContent = $this->getMailRawHeader($Folder, $Uid);
		
		$thisUid = $Uid;
		$thisFolder = $Folder;
		
		// prepare data for top mail description
		// sender name
		$fromDetail = $thisContent->sender[0];
		$emailaddr = $fromDetail->mailbox."@".$fromDetail->host;
		$DisplaySender = $this->MIME_Decode($fromDetail->personal);
		$DisplaySender = $this->Format_Email_Display_Name($DisplaySender);
		$DisplayTopSender= $DisplaySender?$DisplaySender:$emailaddr;
		
		$text_message = $this->MIME_Decode($thisContent->subject);
		$MailDesc = mb_strlen($text_message)>65?(mb_substr($text_message,0,65,mb_detect_encoding($text_message))."..."):$text_message;
		
		$dateReceive = date('Y-m-d H:i:s',strtotime($this->formatDatetimeString($thisContent->date)));
		
		//------ mail top desc bar start -----------
		$mailcss = "gmailcontent$thisUid";
		$maildesccss = "gmaildesc$thisUid";	
		//20100812
		$senderclass = $Folder==$this->SentFolder?"indexcalendartoday":"tabletool";	
		
		$x.='<tr class="gmailtopbar" name="gmailtopbar'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" style="display:none"  onclick="showhidecontent(\''.$thisUid.'\',\''.base64_encode($thisFolder).'\',this) ">'; 
			$x.='<td colspan="2" style="border-top:1px solid #9999BB;">';
				$x .= '<table cellpadding=2 cellspacing=4 border=0 width="100%">';
					$x .= '<tr style="cursor:pointer">'; 
						$x.='<td class="'.$senderclass.'" width="150px" nowrap>'.$DisplayTopSender.':</td>';
						$x.='<td class="tabletextremark '.$maildesccss.'" name="gmaildesc'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'"><span '.$display_gmaildesc.'>'.$MailDesc.'</span></td>';
						$x.='<td class="'.$maildesccss.'" name="gmaildesc'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" align="right" nowrap><span '.$display_gmaildesc.'>'.$dateReceive.' ('.$Lang['SysMgr']['Homework']['WeekDay'][(date("w",strtotime($dateReceive)))].')</span></td>';
						$x.='<tr id="loadingrow'.$thisUid.'" name="loadingrow'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" style="display:none">';
						 $x.='<td colspan="3" class="tableremarks">Loading...<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/indicator.gif" /></td>';
					$x.='</tr>';
				$x .= '</table>';
			$x .='</td>';
			$x .= '<input type="hidden" name="hTopBarFolder[]" value="'.urlencode($thisFolder).'" />';
			$x .= '<input type="hidden" name="hTopBarUid[]" value="'.$thisUid.'" />';
		$x.='</tr>'; 
 
		//------ mail top desc bar end -----------
		
		return $x;
	}
	
	function Get_Mail_Content_UI($Folder, $Uid, $PartNumber='')
	{
		global $Lang, $LAYOUT_SKIN, $image_path, $webmail_info, $plugin, $PATH_WRT_ROOT, $sys_custom, $SYS_CONFIG, $intranet_root, $intranet_session_language, $sort, $reverse, $FromSearch, $pageNo, $field;
		
		//$display_gmailcontent = 'style="display:block"';
		$thisContent = $this->getMailRecord($Folder, $Uid, $PartNumber);
		$thisEmailContent = $thisContent[0];
		
		$thisUid =  $thisEmailContent["uid"];
		$thisFolder =  $thisEmailContent["Folder"]?$thisEmailContent["Folder"]:$Folder;
		
		$header_flags = $thisEmailContent['header_flags'];
		# important flag
		//$MailPriority = $this->GetMailPriority($thisUid,$Folder,$isUID=1); //comment out on 2011-04-13
		
		// prepare data for top mail description
		// sender name
		$fromDetail = $thisEmailContent['fromDetail'][0];
		$emailaddr = $fromDetail->mailbox."@".$fromDetail->host;
		$DisplaySender = str_replace(array('<','>'),array('&lt;','&gt;'),$this->MIME_Decode($fromDetail->personal));
		$DisplaySender = $this->Format_Email_Display_Name($DisplaySender);
		$DisplayTopSender= $DisplaySender?$DisplaySender:$emailaddr;
		
		$text_message = html2txt($thisEmailContent['message']);
		
//		$MailDesc = mb_strlen($text_message)>65?(mb_substr($text_message,0,65,mb_detect_encoding($text_message))."..."):$text_message;
		$mailcss = "gmailcontent$thisUid";
		$maildesccss = "gmaildesc$thisUid";		
		
		$isInternetMailEnabled = $this->AccessInternetMail();
		
		// ------- mail tool bar start ------------
			$x.='<tr>'; 
				$x.='<td colspan="2">';
					$x.='<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="'.$mailcss.'" name="gmailcontent'.$thisUid.'_'.htmlspecialchars($thisFolder, ENT_QUOTES).'" bgcolor="#eeeeee" '.$display_gmailcontent.' >'; 
		          		$x.='<tbody><tr>'; 
		            		$x.='<td>'; 
		                		$x.='<table cellspacing="0" cellpadding="2" border="0" bgcolor="#EEEEEE">';
		                      	$x.='<tbody>';
		                      		$x.='<tr> ';
                      				# flagged /star
									$starPath = trim($thisEmailContent["flagged"])?"icon_star_on.gif":"icon_star_off.gif";
									$starIcon = "<img class='mailflag' align='absmiddle' src='$image_path/$LAYOUT_SKIN/$starPath' border='0' style='cursor:pointer' onclick='setFlag(\"".base64_encode($thisFolder)."\",\"$Uid\", this)'/>";
		                        	$x.='<td>'.$starIcon.'</td>';
                      				# important flag
									//if(trim($MailPriority[$thisUid]["ImportantFlag"]=="true")) // comment out on 2011-04-13
									if($header_flags['x-priority'] == 1 || $header_flags['x-msmail-priority'] == 'High')
									{
										$ImportantIcon = "<img align='absmiddle' src='$image_path/$LAYOUT_SKIN/iMail/icon_important.gif' border='0' />";
		                        		$x.='<td>'.$ImportantIcon.'</td>';
									} 
									if(auth_sendmail())
		                      		{
		                        		$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>'; 
		                        		if(!(!$isInternetMailEnabled && strcasecmp($fromDetail->host,$SYS_CONFIG['Mail']['UserNameSubfix'])!=0))
		                        		{
			                      			# Reply
				                        	$x.='<td> ';
				                        	$x.='<a class="contenttool" href="javascript:void(0);" onclick="SubmitMailAction(\'Reply\','.$thisUid.',\''.base64_encode($thisFolder).'\')">'; 
				                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="reply"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_reply.gif">'; 
				                        	$x.=''.$Lang["Gamma"]["Reply"].'</span>'; 
				                        	$x.='</a>'; 
				                        	$x.='</td>'; 
				                        	$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>'; 
				                        	# reply all
				                        	$x.='<td>'; 
				                        	$x.='<a class="contenttool" href="javascript:void(0);" onclick="SubmitMailAction(\'ReplyAll\','.$thisUid.',\''.base64_encode($thisFolder).'\')">'; 
				                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="replyall"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_reply_all.gif">'; 
				                        	$x.=''.$Lang["Gamma"]["ReplyAll"].'</span>'; 
				                        	$x.='</a>'; 
				                        	$x.='</td>'; 		                        		
				                        	$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>';
		                        		} 
				                        	# Forward
				                        	$x.='<td>'; 
				                        	$x.='<a class="contenttool" href="javascript:void(0);" onclick="SubmitMailAction(\'Forward\','.$thisUid.',\''.base64_encode($thisFolder).'\')">'; 
				                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="forward"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_forward.gif">'; 
				                        	$x.=''.$Lang["Gamma"]["Forward"].'</span>';  
				                        	$x.='</a>'; 
				                        	$x.='</td>'; 
				                        	$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>'; 
		                      		}
		                        	# Remove
		                        	$x.='<td>'; 
		                        	$x.='<a class="contenttool" href="remove.php?Folder_b='.base64_encode($thisFolder).'&Uid[]='.$thisUid.'&page_from=viewmail.php&sort='.$sort.'&reverse='.$reverse.'&FromSearch='.$FromSearch.'&pageNo='.$pageNo.'&field='.$field.'" onclick="if(!confirm(\''.$Lang['Gamma']['ConfirmMsg']['ViewMail']['removeMail'].'\')) return false;">'; 
		                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="delete"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_trash.gif">';  
		                        	$x.=''.$Lang["Gamma"]["Remove"].'</span>'; 
		                        	$x.='</a>'; 
		                        	$x.='</td>'; 
		                        	$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>'; 
		                        	# ReportSpam
		                        	//20100812
			                        if($webmail_info['bl_spam']==true){
			                        	$BtnName = $Folder==$this->SpamFolder?$Lang['Gamma']['reportNonSpam']:$Lang['Gamma']['reportSpam'];
			                        	$ReportSpamMsg = $Folder==$this->SpamFolder?$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportNonSpam']:$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportSpam'];		                        	
			                        	$x.='<td>'; 
			                        	$x.='<a class="contenttool" href="report_spam.php?Folder_b='.base64_encode($thisFolder).'&Uid[]='.$thisUid.'&page_from=viewmail.php&sort='.$sort.'&reverse='.$reverse.'&FromSearch='.$FromSearch.'&pageNo='.$pageNo.'&field='.$field.'" onclick="if(!confirm(\''.$ReportSpamMsg.'\')) return false;">'; 
			                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="reportSpam"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_spam.gif">';  
			                        	$x.=''.$BtnName.'</span>'; 
			                        	$x.='</a>'; 
			                        	$x.='</td>'; 
			                        	$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>';
			                        }
			                        if($sys_custom['iMailPlus']['EDSReportSpam'] && $Folder != $this->EDSReportSpamFolder){
			                        	$BtnName = $Lang['Gamma']['reportSpam'];
			                        	$ReportSpamMsg = str_replace('<!--FOLDER-->','EDS_ReportSpam', $Lang['Gamma']['ConfirmMsg']['ViewMail']['reportSpamToFolder']);		                        	
			                        	$x.='<td>'; 
			                        	$x.='<a class="contenttool" href="eds_report_spam.php?Folder_b='.base64_encode($thisFolder).'&Uid[]='.$thisUid.'&page_from=viewmail.php&sort='.$sort.'&reverse='.$reverse.'&FromSearch='.$FromSearch.'&pageNo='.$pageNo.'&field='.$field.'" onclick="if(!confirm(\''.$ReportSpamMsg.'\')) return false;">'; 
			                        	$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="reportSpam"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_spam.gif">';  
			                        	$x.=''.$BtnName.'</span>'; 
			                        	$x.='</a>'; 
			                        	$x.='</td>'; 
			                        	$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>';
			                        } 
		                        	# Move to 
		                        	//debug_pr("move_email.php?Uid[]=$thisUid&Folder=$thisFolder");
		                        	$MoveTo = $this->getMoveToSelection("targetFolder",$Folder,"moveEmail(this)");
		                        	$x.='<td>'; 
		                        		$x.=$MoveTo;
		                      	$x.='</td></tr>'; 
		                  		$x.='</tbody></table>'; 
				    		$x.='</td>'; 
				    		#view email source 
				    		$x.='<td align="right" >'; 
								$x.='<table cellspacing="0" cellpadding="2" border="0">'; 
								$x.='<tbody><tr>'; 
									$x.='<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif"></td>'; 
									$x.='<td>'; 
									$x.='<a class="contenttool" href="javascript:viewMessageSource('.$thisUid.',\''.base64_encode($thisFolder).'\')">'; 
									$x.='<span onmouseout="this.className=\'contenttool\'" onmouseover="this.className=\'contenttool_over\'" class="contenttool" id="view_source"><img height="20" width="20" border="0" align="absmiddle" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_view_source.gif">'; 
									$x.=$Lang['Gamma']['ViewMessageSource'].'</span>'; 
									$x.='</a>'; 
									$x.='</td>';
									$x.='<td><div class="Conntent_tool"><a href="javascript:newWindow(\''.$PATH_WRT_ROOT.'home/imail_gamma/print.php?uid='.$Uid.'&Folder='.base64_encode($thisFolder).'&PartNumber='.$PartNumber.'\',10);" class="print">'.$Lang['Btn']['Print'].'</a></div></td>';
									//$x.='<td><div class="Conntent_tool">'.$linterface->GET_LNK_PRINT("javascript:newWindow('".$PATH_WRT_ROOT."home/imail_gamma/print.php?uid=$Uid&Folder=$Folder&PartNumber=$PartNumber',10);","","","","",1).'</div></td>';  
								$x.='</tr>'; 
								$x.='</tbody></table>'; 
				   			$x.='</td>'; 
				  		$x.='</tr>'; 
						$x.='</tbody></table>';
				$x.='</td>';
			$x.='</tr>'; 
			// ------- mail tool bar end ------------
			
			$x.='<tr>'; 
				$x.='<td colspan="2">';
					//------ mail header start -----------
					$x.='<table width="100%" cellspacing="0" cellpadding="5" style="border-bottom:solid 1px #9999BB;"  border="0" class="'.$mailcss.'" name="gmailcontent'.$thisUid.'_'.htmlspecialchars($thisFolder,ENT_QUOTES).'" '.$display_gmailcontent.'>';
						$x.='<tr>'; 
							$x.='<td colspan="2">'; 
							$x.='<table width="100%" cellspacing="0" cellpadding="5" border="0" style="border-bottom:dotted 1px #BBBBCC;" bgcolor="#d2eff5" >';
							$x.='<tbody>';
		
							# date
							$x.='<tr>'; 
					            $x.='<td width="120" align="left" class="tabletext"><span id="date">'.$Lang['Gamma']['Date'].'</span></td>'; 
					            $x.='<td width="4" align="left" class="tabletext"><span > :</span></td>'; 
					            $x.='<td class="tabletext">'.$thisEmailContent['dateReceive'].' ('.$Lang['SysMgr']['Homework']['WeekDay'][(date("w",strtotime($thisEmailContent['dateReceive'])))].')</td>'; 
							$x.='</tr>'; 
		
							# sender
							if(!$isInternetMailEnabled && strcasecmp($fromDetail->host,$SYS_CONFIG['Mail']['UserNameSubfix'])!=0){
								$sender = $this->removeQuotesForDisplayName($DisplaySender).' &lt;'.$emailaddr.'&gt;';
							}else{
							    if(auth_sendmail()){
								    $sender = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplaySender).'&to='.$emailaddr.'">'.$this->removeQuotesForDisplayName($DisplaySender).' &lt;'.$emailaddr.'&gt;</a>';
							    }else{
							        $sender = $this->removeQuotesForDisplayName($DisplaySender).' &lt;'.$emailaddr.'&gt;';
							    }
							}
					      	$x.='<tr>';
					        	$x.='<td align="left" class="tabletext"><span id="sender" >'.$Lang['Gamma']['Sender'].'</span></td>';			
					        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
					        	$x.='<td class="tabletext"><span class="tabletext">'.$sender.'</span></td>';			
					      	$x.='</tr>';
					      	
					      	# to
							if(!empty($thisEmailContent['toDetail']))
					      	{
					      		$toAccountAry = $toAccountShowArr = $toAccountHideArr = array();
						      	$toDetail = $thisEmailContent['toDetail'];
						      	for($i=0;$i<sizeof($toDetail);$i++)
						      	{
									$emailaddr = $toDetail[$i]->mailbox."@".$toDetail[$i]->host;
									$DisplayName = str_replace(array('<','>'),array('&lt;','&gt;'),$this->MIME_Decode($toDetail[$i]->personal));
									if(!$isInternetMailEnabled && strcasecmp($toDetail[$i]->host,$SYS_CONFIG['Mail']['UserNameSubfix'])!=0){
										$toAccountAry[] = $this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;';
						      	    }else{
						      	        if(auth_sendmail()){
									   	   $toAccountAry[] = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplayName).'&to='.$emailaddr.'">'.$this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;</a>';
						      	        }else{
						      	           $toAccountAry[] = $this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;';
						      	        }
									}
								}
						      	if(count($toAccountAry)>8)
						      	{
						      		for($i=0;$i<count($toAccountAry);$i++)
						      		{
						      			if($i<4)
						      				$toAccountShowArr[] =  $toAccountAry[$i];
						      			else
						      				$toAccountHideArr[] = $toAccountAry[$i];
						      		}
						      		$toAccountShow = implode(", ",$toAccountShowArr).'<span id="ShowTo'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideTo'.$thisUid.'\').show(); $(\'#ShowTo'.$thisUid.'\').hide();">[Show Details]</a></span>';
						      		$toAccountHide = '<span id="HideTo'.$thisUid.'" style="display:none;">,'.implode(", ",$toAccountHideArr).'</span>';
						      		$toAccount = $toAccountShow.$toAccountHide;
						      		
						      	}else if(count($toAccountAry)>1)
						      		$toAccount = implode(", ",$toAccountAry);
						      	else
						      		$toAccount = $toAccountAry[0];
						      
						      	
						      	$x.='<tr>';
						        	$x.='<td align="left" class="tabletext"><span id="sender" >'.$Lang['Gamma']['To'].'</span></td>';			
						        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
						        	$x.='<td class="tabletext"><span class="tabletext">'.$toAccount.'</span></td>';			
						      	$x.='</tr>';
					      	}
					      	
					      	# cc
					      	if(!empty($thisEmailContent['ccDetail']))
					      	{
					      		$ccAccountAry = $ccAccountShowArr = $ccAccountHideArr = array();
						      	$ccDetail = $thisEmailContent['ccDetail'];
						      	for($i=0;$i<sizeof($ccDetail);$i++)
						      	{
									$emailaddr = $ccDetail[$i]->mailbox."@".$ccDetail[$i]->host;
									$DisplayName = str_replace(array('<','>'),array('&lt;','&gt;'),$this->MIME_Decode($ccDetail[$i]->personal));
									if(!$isInternetMailEnabled && strcasecmp($ccDetail[$i]->host,$SYS_CONFIG['Mail']['UserNameSubfix'])!=0){
										$ccAccountAry[] = $this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;';
									}else{
									    if(auth_sendmail()){
										  $ccAccountAry[] = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplayName).'&to='.$emailaddr.'">'.$this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;</a>';
									    }else{
									      $ccAccountAry[] = $this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;';
									    }
									}
						      	}
								
						      	if(count($ccAccountAry)>8)
						      	{
						      		for($i=0;$i<count($ccAccountAry);$i++)
						      		{
						      			if($i<5)
						      				$ccAccountShowArr[] =  $ccAccountAry[$i];
						      			else
						      				$ccAccountHideArr[] = $ccAccountAry[$i];
						      		}
						      		$ccAccountShow = implode(", ",$ccAccountShowArr).'<span id="ShowCc'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideCc'.$thisUid.'\').show(); $(\'#ShowCc'.$thisUid.'\').hide();">[Show Details]</a></span>';
						      		$ccAccountHide = '<span id="HideCc'.$thisUid.'" style="display:none;">,'.implode(", ",$ccAccountHideArr).'</span>';
						      		$ccAccount = $ccAccountShow.$ccAccountHide;
						      		
						      	}else if(count($ccAccountAry)>1)
						      		$ccAccount = implode(", ",$ccAccountAry);
						      	else
						      		$ccAccount = $ccAccountAry[0];
						      	$x.='<tr>';
						        	$x.='<td align="left" class="tabletext"><span id="sender">'.$Lang['Gamma']['cc'].'</span></td>';			
						        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
						        	$x.='<td class="tabletext"><span class="tabletext">'.$ccAccount.'</span></td>';			
						      	$x.='</tr>';
					      	}
					      	
					      	# bcc
					      	if(!empty($thisEmailContent['bccDetail']))
					      	{
					      		$bccAccountAry = $bccAccountShowArr = $bccAccountHideArr = array();
						      	$bccDetail = $thisEmailContent['bccDetail'];
						      	for($i=0;$i<sizeof($bccDetail);$i++)
						      	{
									$emailaddr = $bccDetail[$i]->mailbox."@".$bccDetail[$i]->host;
									$DisplayName = str_replace(array('<','>'),array('&lt;','&gt;'),$this->MIME_Decode($bccDetail[$i]->personal));
									if(!$isInternetMailEnabled && strcasecmp($bccDetail[$i]->host,$SYS_CONFIG['Mail']['UserNameSubfix'])!=0){
										$bccAccountAry[] = $this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;';
									}else{
									    if(auth_sendmail()){
										  $bccAccountAry[] = '<a class="tabletool" title="'.$emailaddr.'" href="compose_email.php?person='.urlencode($DisplayName).'&to='.$emailaddr.'">'.$this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;</a>';
									    }else{
									      $bccAccountAry[] = $this->removeQuotesForDisplayName($DisplayName).' &lt;'.$emailaddr.'&gt;</a>';
									    }
									}
						      	}
								
						      	if(count($bccAccountAry)>8)
						      	{
						      		for($i=0;$i<count($bccAccountAry);$i++)
						      		{
						      			if($i<5)
						      				$bccAccountShowArr[] = $bccAccountAry[$i];
						      			else
						      				$bccAccountHideArr[] = $bccAccountAry[$i];
						      		}
						      		$bccAccountShow = implode(", ",$bccAccountShowArr).'<span id="ShowBcc'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideBcc'.$thisUid.'\').show(); $(\'#ShowBcc'.$thisUid.'\').hide();">[Show Details]</a></span>';
						      		$bccAccountHide = '<span id="HideBcc'.$thisUid.'" style="display:none;">,'.implode(", ",$bccAccountHideArr).'</span>';
						      		$bccAccount = $bccAccountShow.$bccAccountHide;
						      		
						      	}else if(count($bccAccountAry)>1)
						      		$bccAccount = implode(", ",$bccAccountAry);
						      	else
						      		$bccAccount = $bccAccountAry[0];
						      	$x.='<tr>';
						        	$x.='<td align="left" class="tabletext"><span id="sender">'.$Lang['Gamma']['bcc'].'</span></td>';			
						        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
						        	$x.='<td class="tabletext"><span class="tabletext">'.$bccAccount.'</span></td>';			
						      	$x.='</tr>';
					      	}
					      	
		  	   				//$x.='</tr>';
		        	  	    #Subject
		  	        	  	$Subject = $thisEmailContent['subject']?$thisEmailContent['subject']:"No Subject";
							$Subject = $this->BadWordFilter($Subject);
							
			              	$x.='<tr>';			
			                	$x.='<td align="left" class="tabletext"><span id="subject" >'.$Lang['Gamma']['Subject'].'</span></td>';			
			                	$x.='<td align="left" class="tabletext"><span >:</span></td>';			
			                	$x.='<td class="tabletext"><font size="+1">'.htmlspecialchars($Subject).'</font></td>';			
			              	$x.='</tr>';			
			              	
			              	#Attachment
			              	$file='';
		  	        	  	$Attachment = $thisEmailContent["attach_parts"];
		  	        	  	
							if (!(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
								
								
								// short list	
								$num_of_img=0;
								$num_of_files = 0;
								for ($i=0; $i< sizeof($Attachment); $i++) {	
									
									if (!$Attachment[$i]['IsTNEF'] ) {
										if ($Attachment[$i]['AttachType'] == "File")
											$file .= '<a class="tabletool" href="cache_view_attachment.php?PartNumber='.$Attachment[$i]['PartID']."&Folder=".urlencode($thisFolder).'&MessageID='.$thisUid.'">';
										else
											$file .= '<a class="tabletool" href="javascript:void(0);" onclick="window.open(\'viewmail.php?PartNumber='.$Attachment[$i]['PartID'].'&Folder='.urlencode($thisFolder).'&uid='.$thisUid.'\');">';
									}
									else {
										$file .= '<a class="tabletool" href="'.$Attachment[$i]['FilePath'].'" target="_blank" >';
									}
									$file .= '<img hspace="2" vspace="2" border="0" align="absmiddle" src="/images/file.gif">';
									$file .= $Attachment[$i]['FileName'];
									$file .= '</a>';
									$file .= ' ('.$Attachment[$i]['FileSize'].'Kb)';
									$file .= "<br> ";
									
									$filename = basename($Attachment[$i]['FileName']);
                					$fileext =  strtolower(substr($filename, strrpos($filename,".")));
									if(in_array($fileext,array(".jpg",".jpeg",".gif",".png",".tif",".bmp")))
										$num_of_img++;
									$num_of_files++;
								}
			                  	if($num_of_img>0)
									$file .= '[<a class="tabletool" href="javascript:void(0)"; onclick="window.open(\'view_all_image.php?&Folder='.urlencode($thisFolder).'&uid='.$thisUid.'\');">'.$Lang['Gamma']['ViewAllImage'].'</a>]&nbsp;';
								
								if($num_of_files>1){
									$file .= '[<a class="tabletool" href="download_all_attachments.php?Folder='.urlencode($thisFolder).'&uid='.$thisUid.'" target="_blank">'.$Lang['Gamma']['DownloadAllAttachment'].'</a>]&nbsp;';
								}
								
								if($num_of_files > 0 && $plugin['digital_archive'] && $_SESSION['UserType']==USERTYPE_STAFF) {
									include_once("libdigitalarchive_moduleupload.php");
									$ldamu = new libdigitalarchive_moduleupload();
									if($ldamu->User_Can_Archive_File())
										$file .= '[<a class="tabletool thickbox" href="#TB_inline?height='.$ldamu->TBHeight.'&width='.$ldamu->TBWidth.'&inlineId=FakeLayer" onclick="GetUploadFileToDA_TBForm(\''.urlencode($thisFolder).'\',\''.$thisUid.'\');" title="'.$Lang['DigitalArchiveModuleUpload']['Archive'].'">'.$Lang['DigitalArchiveModuleUpload']['Archive'].'</a>]';
								}
								
								if($num_of_files > 0 && $plugin['DocRouting'] && $_SESSION['UserType'] == USERTYPE_STAFF) {
									global $docRoutingConfig;
									include_once("liburlparahandler.php");
									include_once("DocRouting/docRoutingConfig.inc.php");
									include_once("DocRouting/libDocRouting.php");
									
									$ldocrouting = new libDocRouting();
									$pe = $ldocrouting->getEncryptedParameter('management','edit_doc',array('createDocFromModule'=>'imailplus', 'Folder'=>base64_encode($Folder), 'Uid'=>$Uid) );
									$doc_routing_httppath = '/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe='.$pe;
									//$doc_routing_httppath = '/home/eAdmin/ResourcesMgmt/DocRouting/index.php?p=management/||edit_doc||createDocFromModule-imailplus/Folder-'.$Folder.'/Uid-'.$Uid.'/';
									$file .= '[<a class="tabletool" href="'.$doc_routing_httppath.'" title="'.$Lang['Gamma']['CreateRoutingDocument'].'">'.$Lang['Gamma']['CreateRoutingDocument'].'</a>]';
								}
								
								$x .= '	<tr>';
			                    	$x .= '<td align="left" class="tabletext" valign=top><span id="attachment" >'.$Lang["Gamma"]["Attachment"].'</span></td>';
			                    	$x .= '<td align="left" class="tabletext" valign=top><span >:</span></td>';
			                    	$x .= '<td class="tabletext"><span class="tabletext">'.$file;
									$x .= '</span></td>';
			                  	$x .= '</tr>';
							}
							
						$x.='</tbody></table>'; 			
		                $x.='</td>'; 			
		            	$x.='</tr>'; 											
						//------ mail header end -----------
						//echo "<PRE>";print_r($thisEmailContent['RelatedInlineList']);echo "</PRE>";
						//------ mail message start ------------
						# message
						
						$tmp_id = md5(uniqid(mt_rand(), 1));
						
						$Message = $this->BadWordFilter($thisEmailContent['message']);
						$x.='<tr>';
							$x.='<td colspan="2">';
							$x.='<table width="100%" cellspacing="0" cellpadding="10" border="0">';
							$x.='<tbody><tr>';
								$x.='<td id="TdMessage'.$tmp_id.'" class="message">';
									$x.= cleanHtmlJavascript($Message);
									//$x.= $Message;
									/*
									$x.='<textarea id="TaMessage'.$tmp_id.'" style="display:none;">'.$Message.'</textarea>';
									$x .= '<script type="text/javascript" language="JavaScript">'."\n";
										$x .= '$(document).ready(function(){ $("td#TdMessage'.$tmp_id.'").html($("textarea#TaMessage'.$tmp_id.'").val());});';
									$x .= '</script>'."\n";
									*/
								$x.='</td>';
							$x.='</tr>';
							$x.='</tbody></table>';
							$x.='</td>';
						$x.='</tr>';
						//------ mail message end ------------
						
					//---- Track Mail Recipients Status for Internal Mail ----
					if(trim($thisEmailContent['eClassMailID'])!='')
					{
						if(trim($thisEmailContent['MessageID']) == '')
						{
							// This is the sent mail copy
							// Display Recipient Status
							$x .= $this->DisplayMailRecipientStatus(trim($thisEmailContent['eClassMailID']));
						}else
						{
							// Mark this Mail as Readed
							$this->UpdateMailReceipt($thisEmailContent['eClassMailID'],$_SESSION['UserID']);
						}
					}
					//---- End of Track Mail Recipients Status for Internal Mail ----
					
					$x.='</table>';
				$x.='</td>';
			$x.='</tr>';
		
		if($thisEmailContent['header']->Unseen == 'U'){
			$this->readMail($Folder,$Uid);
		}
		
		return $x;
	}
	
	function Get_Compose_Mail_Form($Mail="", $Action="", $Folder="", $Signature="", $mailList=Array(), $eventID="",$CampusMailID="", $internalUseArr=array()) {
		global $SYS_CONFIG, $Lang,  $linterface, $sys_custom;
		global $personal_path, $PATH_WRT_ROOT,$image_path;
		global $UserID, $LAYOUT_SKIN,$CurUserType , $MessageID,$DraftUID, $ComposeDatetime, $userBrowser;
		global $i_frontpage_campusmail_icon_important2_2007,$i_frontpage_campusmail_important,
			   $i_frontpage_campusmail_icon_notification2_2007,$i_frontpage_campusmail_notification,
			   $i_CampusMail_New_NotificationInternalOnly;
		################# New to re-design the logic for READ Mail Content #################
//		$user = new user($_SESSION['SSV_USERID']);
//		$AliasList = $user->Get_User_Alias_List();
//		for ($i=0; $i<sizeof($AliasList) ; $i++) {
//			$CheckMailList[] = $AliasList[$i][0];
//		}
//		$CheckMailList[] = $user->Email;

		#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
		##################### TEMPORARILY assign $CheckMailList ########################
		$user = new libuser($UserID);
		$CheckMailList[] = $user->ImapUserEmail;
		#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
		
		// echo '########## Debug ##########'.'<br>';
		// echo '$Mail : '.$Mail[0]['UID'].'<br>';
		// echo '########### End ###########'.'<br>';
		$is_below_IE8 = ($userBrowser->browsertype == "MSIE" && intval($userBrowser->version) < 8) || $sys_custom['iMailPlusUseStandardUploadMethod'];
		$is_mobile_tablet_platform = ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod");
		
		# process data for Reply/ ReplyAll/ Forward
		if($CampusMailID=='') // general
		{
			$added_from_email = array();
			$fromList = $Mail[0]['fromDetail'];
			if(isset($Mail[0]['header']->reply_to) && sizeof($Mail[0]['header']->reply_to)>0){
				$header_reply_to = $Mail[0]['header']->reply_to;
				$fromList = array_merge($fromList, $header_reply_to);
			}
			$name_of_from = '';
			for ($i=0; $i<sizeof($fromList) ; $i++) {
				if(in_array($fromList[$i]->mailbox."@".$fromList[$i]->host, $added_from_email)) continue;		
				if (trim($fromList[$i]->personal) != "") {
					$DisplayName = $this->MIME_Decode($fromList[$i]->personal);
					$DisplayName = $this->Format_Email_Display_Name($DisplayName);
					$DisplayName = str_replace(array('<','>'),'', $DisplayName);
					$name_of_from = $DisplayName;
					//stristr($DisplayName,"@")? $DisplayName = '"'.stripslashes($DisplayName).'"':$DisplayName = stripslashes($DisplayName);
					$MailBodyFrom .= $DisplayName." &lt; ".$fromList[$i]->mailbox."@".$fromList[$i]->host." &gt;; ";
				//	if (!in_array($fromList[$i]->mailbox."@".$fromList[$i]->host,$CheckMailList)) 
					$fromAddress .= $DisplayName." &lt;".$fromList[$i]->mailbox."@".$fromList[$i]->host."&gt;; ";
				}else if($name_of_from != ''){
					$MailBodyFrom .= $name_of_from." &lt; ".$fromList[$i]->mailbox."@".$fromList[$i]->host." &gt;; ";
					$fromAddress .= $name_of_from." &lt;".$fromList[$i]->mailbox."@".$fromList[$i]->host."&gt;; ";
				}else {
					$MailBodyFrom .= $fromList[$i]->mailbox."@".$fromList[$i]->host."; ";
				//	if (!in_array($fromList[$i]->mailbox."@".$fromList[$i]->host,$CheckMailList)) 
					$fromAddress .= $fromList[$i]->mailbox."@".$fromList[$i]->host."; ";
				}
				$added_from_email[] = $fromList[$i]->mailbox."@".$fromList[$i]->host;
			}
			
			# To Field
			$toList = $Mail[0]['toDetail'];
			
			for ($i=0; $i< sizeof($toList); $i++) {			
				if (trim($toList[$i]->personal) != "") {				
					$DisplayName = $this->MIME_Decode($toList[$i]->personal);
					$DisplayName = $this->Format_Email_Display_Name($DisplayName);
					$DisplayName = str_replace(array('<','>'),'', $DisplayName);
					//stristr($DisplayName,"@")? $DisplayName = '"'.$DisplayName.'"':$DisplayName = stripslashes($DisplayName);
					$MailBodyTo .= $DisplayName." &lt; ".$toList[$i]->mailbox."@".$toList[$i]->host." &gt;; ";
				//	if (!in_array($toList[$i]->mailbox."@".$toList[$i]->host,$CheckMailList)) 
						$toAddress .= $DisplayName." &lt;".$toList[$i]->mailbox."@".$toList[$i]->host."&gt;; ";
				}
				else {
					$MailBodyTo .= " ".$toList[$i]->mailbox."@".$toList[$i]->host."; ";
				//	if (!in_array($toList[$i]->mailbox."@".$toList[$i]->host,$CheckMailList)) 
						$toAddress .= " ".$toList[$i]->mailbox."@".$toList[$i]->host."; ";
				}
			}
			
			# Cc Field
			$ccList = $Mail[0]['ccDetail'];
			for ($i=0; $i< sizeof($ccList); $i++) {
				if (trim($ccList[$i]->personal) != "") {
					$DisplayName = $this->MIME_Decode($ccList[$i]->personal);
					$DisplayName = $this->Format_Email_Display_Name($DisplayName);
					$DisplayName = str_replace(array('<','>'),'', $DisplayName);
					//stristr($DisplayName,"@")? $DisplayName = '"'.stripslashes($DisplayName).'"':$DisplayName = stripslashes($DisplayName);
					$MailBodyCc .= $DisplayName." &lt; ".$ccList[$i]->mailbox."@".$ccList[$i]->host." &gt;; ";
				//	if (!in_array($ccList[$i]->mailbox."@".$ccList[$i]->host,$CheckMailList)) 
						$ccAddress .= $DisplayName." &lt;".$ccList[$i]->mailbox."@".$ccList[$i]->host."&gt;; ";
				}
				else {
					$MailBodyCc .= " ".$ccList[$i]->mailbox."@".$ccList[$i]->host."; ";
				//	if (!in_array($ccList[$i]->mailbox."@".$ccList[$i]->host,$CheckMailList)) 
						$ccAddress .= " ".$ccList[$i]->mailbox."@".$ccList[$i]->host."; ";
				}
			}
			
			# Bcc Field
			$bccList = $Mail[0]['bccDetail'];
			for ($i=0; $i< sizeof($bccList); $i++) {
				if (trim($ccList[$i]->personal) != "") {
					$DisplayName = $this->MIME_Decode($bccList[$i]->personal);
					$DisplayName = $this->Format_Email_Display_Name($DisplayName);
					$DisplayName = str_replace(array('<','>'),'', $DisplayName);
					$MailBodyBcc .= $DisplayName." &lt; ".$bccList[$i]->mailbox."@".$bccList[$i]->host." &gt;; ";
					$bccAddress .= $DisplayName." &lt;".$bccList[$i]->mailbox."@".$bccList[$i]->host."&gt;; ";
				}
				else {
					$MailBodyBcc .= " ".$bccList[$i]->mailbox."@".$bccList[$i]->host."; ";
					$bccAddress .= " ".$bccList[$i]->mailbox."@".$bccList[$i]->host."; ";
				}
			}
			
			$Subject 	  = htmlspecialchars($this->BadWordFilter($this->MIME_Decode($Mail[0]['subject'])));
			$Body 		  = $this->htmlencodeEmailAddressAngleBrackets($this->BadWordFilter($Mail[0]['message']));
			$Priority 	  = ($Mail[0]['Priority'] == 1) ? true : false;
			$DateReceived = $Mail[0]['dateReceive'];
			$AttachList = $Mail[0]['attach_parts'];
			
		}
		else // from iMail Archive
		{
			include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
			$lcampusmail = new libcampusmail($CampusMailID);
		    if ($lcampusmail->mailType==2)         # External
		    {
		        $mail_senderemail = (str_replace(array("<",">","&lt;","&gt;"),array("&amp;lt;","&amp;gt;","&amp;lt;","&amp;gt;"),$lcampusmail->SenderEmail));
		        $fromAddress = str_replace(array("<",">"),array("&lt;","&gt;"),$lcampusmail->SenderEmail).";";
		        $MailBodyFrom = $mail_senderemail;
		    }
		    else
		    {
		        $namefield = getNameFieldWithClassNumberByLang();
		        $sql = "SELECT UserID,$namefield, UserEmail, ImapUserEmail FROM INTRANET_USER WHERE UserID = '".$lcampusmail->SenderID."'";
		        $temp = $lcampusmail->returnArray($sql,4);
		        list($mail_senderid,$mail_sendername, $mail_useremail, $mail_imapemail) = $temp[0];
		        if($mail_imapemail != ''){
		        	$mail_senderemail = $this->Format_Email_Display_Name($mail_sendername)." &amp;lt;".$mail_imapemail."&amp;gt;;";
		        	$fromAddress = $this->Format_Email_Display_Name($mail_sendername)." &lt;".$mail_imapemail."&gt;;";
		        }else if($mail_useremail != ''){
		        	$mail_senderemail = $this->Format_Email_Display_Name($mail_sendername)." &amp;lt;".$mail_useremail."&amp;gt;;";
		        	$fromAddress = $this->Format_Email_Display_Name($mail_sendername)." &lt;".$mail_useremail."&gt;;";
		        }
		        //$MailBodyFrom = $mail_sendername;
		        $MailBodyFrom = $mail_senderemail;
		    }
		    
		    $Subject = $lcampusmail->Subject;
		    $Body = $this->htmlencodeEmailAddressAngleBrackets($lcampusmail->Message);
		    $DateReceived = $lcampusmail->DateInput;
		    $toAddress = "";
		    $ccAddress = "";
		    $bccAddress = "";
		    if($Action == "ReplyAll")
		    {
		    	$namefield = getNameFieldWithClassNumberByLang();
			    if(trim($lcampusmail->RecipientID) != ''){
			    	$userid_csv = str_replace("U","",trim($lcampusmail->RecipientID));
			    	$sql = "SELECT $namefield as UserName, IF(TRIM(ImapUserEmail)!='', ImapUserEmail, UserEmail) as UserEmail FROM INTRANET_USER WHERE UserID IN (".$userid_csv.") AND (TRIM(ImapUserEmail)!='' OR TRIM(UserEmail)!='')";
			    	$internal_to_address = $lcampusmail->returnResultSet($sql);
			    	for($i=0;$i<count($internal_to_address);$i++)
			    	{
			    		$toAddress .= $this->Format_Email_Display_Name($internal_to_address[$i]['UserName'])." &lt;".$internal_to_address[$i]['UserEmail']."&gt;;";
			    	}
			    }
			    if(trim($lcampusmail->InternalCC) != ''){
			    	$userid_csv = str_replace("U","",trim($lcampusmail->InternalCC));
			    	$sql = "SELECT $namefield as UserName, IF(TRIM(ImapUserEmail)!='', ImapUserEmail, UserEmail) as UserEmail FROM INTRANET_USER WHERE UserID IN (".$userid_csv.") AND (TRIM(ImapUserEmail)!='' OR TRIM(UserEmail)!='')";
			    	$internal_cc_address = $lcampusmail->returnResultSet($sql);
			    	for($i=0;$i<count($internal_cc_address);$i++)
			    	{
			    		$ccAddress .= $this->Format_Email_Display_Name($internal_cc_address[$i]['UserName'])." &lt;".$internal_cc_address[$i]['UserEmail']."&gt;;";
			    	}
			    }
		    }
		    
		    $toAddress .= intranet_htmlspecialchars($lcampusmail->ExternalTo);
		    $ccAddress .= intranet_htmlspecialchars($lcampusmail->ExternalCc);
		    $bccAddress .= intranet_htmlspecialchars($lcampusmail->ExternalBcc);
		    
		    $CampusMailAttachList = $this->Handle_CampusMail_Attachment($lcampusmail->Attachment,$ComposeDatetime);
		}
		
		$regexp = '/^\s*(re\s*:{1}\s*|fwd?\s*:{1}\s*){1}/i'; 
		$trimed_subject = trim(preg_replace($regexp, '', $Subject));
		################# New to re-design the logic for READ Mail Content #################
		
		// setup the email field by Draft/ Reply/ ReplyAll/ Forward
		
		switch ($Action) {
			case "Reply":
				$FinalTo = $fromAddress;
				$MailInfo = '<br>
							 <hr>
							 <b>From:</b> '.$MailBodyFrom.'<br>
							 <b>Sent:</b> '.$DateReceived.'<br>';
				$MailInfo .= ($MailBodyTo != "")? '<b>To:</b> '.$MailBodyTo.'<br>':'';
				$MailInfo .= ($MailBodyCc != "")? '<b>Cc:</b> '.$MailBodyCc.'<br>':'';
				$MailInfo .= '<b>Subject:</b> '.$Subject.'<br><br><br>';
				$Subject = "RE: ".$trimed_subject;
				$FinalBody = $MailInfo.$Body;
				break;
			case "ReplyAll":
				$FinalTo = $fromAddress.$toAddress;
				$FinalCc = $ccAddress;
				$MailInfo = '<br>
							 <hr>
							 <b>From:</b> '.$MailBodyFrom.'<br>
							 <b>Sent:</b> '.$DateReceived.'<br>';
				$MailInfo .= ($MailBodyTo != "")? '<b>To:</b> '.$MailBodyTo.'<br>':'';
				$MailInfo .= ($MailBodyCc != "")? '<b>Cc:</b> '.$MailBodyCc.'<br>':'';
				$MailInfo .= '<b>Subject:</b> '.$Subject.'<br><br><br>';
				$Subject = "RE: ".$trimed_subject;
				$FinalBody = $MailInfo.$Body;
				break;
			case "Forward":
				$MailInfo = '<br>
							 <hr>
							 <b>From:</b> '.$MailBodyFrom.'<br>
							 <b>Sent:</b> '.$DateReceived.'<br>';
				$MailInfo .= ($MailBodyTo != "")? '<b>To:</b> '.$MailBodyTo.'<br>':'';
				$MailInfo .= ($MailBodyCc != "")? '<b>Cc:</b> '.$MailBodyCc.'<br>':'';
				$MailInfo .= '<b>Subject:</b> '.$Subject.'<br><br><br>';
				$Subject = "FW: ".$trimed_subject;
				$FinalBody = $MailInfo.$Body;
//				// process attachment
//				if (sizeof($AttachList) > 0) {
//					$AttachmentDisplayList = $this->Get_List_Open();
//					for ($i=0; $i< sizeof($AttachList); $i++) {
//						$FileSize = $AttachList[$i]['FileSize'];
//						
//						if (!$AttachList[$i]['IsTNEF']) {
//							$AttachDisplay = $this->Get_Input_CheckBox("AttachPartNumber[]","AttachPartNumber[]",$AttachList[$i]['PartID'],true,'');
//							if ($AttachList[$i]['AttachType'] == "File") 
//								$AttachDisplay .= $this->Get_HyperLink_Open($PathRelative."src/module/email/cache_view_attachment.php?PartNumber=".$AttachList[$i]['PartID']."&Folder=".urlencode($Folder)."&MessageID=".$MessageID);
//							else 
//								$AttachDisplay .= $this->Get_HyperLink_Open("#",'onclick="window.open(\''.$PathRelative.'src/module/email/gamma/read_email.php?PartNumber='.$AttachList[$i]['PartID'].'&Folder='.urlencode($Folder).'&uid='.$MessageID.'&NoButton=1\');" class="imail_entry_read_link"');
//						}
//						else {
//							$AttachDisplay = $this->Get_Input_CheckBox("TNEFFile[]","TNEFFile[]",$AttachList[$i]['FileName'],true,'');
//							$AttachDisplay .= $this->Get_HyperLink_Open($AttachList[$i]['FilePath'],'target="_blank" class="imail_entry_read_link"');
//						}
//						$AttachDisplay .= $AttachList[$i]['FileName'].'('.$FileSize.'K)';
//						$AttachDisplay .= $this->Get_HyperLink_Close();
//						$AttachDisplay .= $this->Get_Input_Hidden("AttachPartSize[]", "AttachPartSize[]", $FileSize);
//						$AttachDisplay .= $this->Get_Br();
//						$AttachmentDisplayList .= $AttachDisplay;
//					}
//					$AttachmentDisplayList .= $this->Get_List_Close();
//				}

				// process attachment
				if($CampusMailID!='') // Archive
				{
					$CampusMailAttachmentList = '<ul>';
					for ($i=0; $i< sizeof($CampusMailAttachList); $i++) {
						$FileSize = (round(filesize($personal_path."/".$CampusMailAttachList[$i][5])/1024,2));
							$AttachDisplay  = '<input type="checkbox" id="FileID[]" name="FileID[]" value="'.$CampusMailAttachList[$i][0].'" checked style="visiblity:hidden; display:none;">'."\n";
							$AttachDisplay .= '<a class="tabletool" href="view_compose_attachment.php?FileID='.$CampusMailAttachList[$i][0].'">'."\n";
							$AttachDisplay .= str_replace("'","\'",$CampusMailAttachList[$i][4]).'('.$FileSize.'K)'."\n";
							$AttachDisplay .= '</a>'."\n";
							$AttachDisplay .= '[<a class="tabletool" href="javascript:void(0);" onclick="FileDeleteSubmit(\''.$CampusMailAttachList[$i][0].'\')">'.$Lang['Gamma']['RemoveAttachment'].'</a>]'."\n";
							$AttachDisplay .= '<input type="hidden" id="FileSize[]" name="FileSize[]" value="'.$FileSize.'">'."\n";
							$AttachDisplay .= '<br>';
						$CampusMailAttachmentList .= $AttachDisplay;
					}
					$CampusMailAttachmentList .= '</ul>';
				}
				else if (sizeof($AttachList) > 0) 
				{
					$AttachmentDisplayList = '<ul>';
					for ($i=0; $i< sizeof($AttachList); $i++) {
						$FileSize = $AttachList[$i]['FileSize'];
						//if($AttachList[$i]['Disposition'] == 'inline') continue;
						if (!$AttachList[$i]['IsTNEF']) {
							//$AttachDisplay = $this->Get_Input_CheckBox("AttachPartNumber[]","AttachPartNumber[]",$AttachList[$i]['PartID'],true,'');
							$AttachDisplay = '<input type="checkbox" id="AttachPartNumber[]" name="AttachPartNumber[]" value="'.$AttachList[$i]['PartID'].'" checked>';
							if ($AttachList[$i]['AttachType'] == "File"){ 
									$AttachDisplay .= '<a href="'.$PATH_WRT_ROOT.'home/imail_gamma/cache_view_attachment.php?PartNumber='.$AttachList[$i]['PartID'].'&Folder='.urlencode($Folder).'&MessageID='.$MessageID.'" class="tabletool">';
							}else{ 
								$AttachDisplay .= '<a href="javascript:void(0);" onclick="window.open(\''.$PATH_WRT_ROOT.'src/module/email/gamma/read_email.php?PartNumber='.$AttachList[$i]['PartID'].'&Folder='.urlencode($Folder).'&uid='.$MessageID.'&NoButton=1\');" class="tabletool">';
							}
						}
						else {
							$AttachDisplay = '<input type="checkbox" id="TNEFFile[]" name="TNEFFile[]" value="'.$AttachList[$i]['FileName'].'" checked>';
							$AttachDisplay .= '<a href="'.$AttachList[$i]['FilePath'].'" target="_blank" class="tabletool">';
						}
						$AttachDisplay .= $AttachList[$i]['FileName'].'('.$FileSize.'K)';
						$AttachDisplay .= '</a>';
						$AttachDisplay .= '<input type="hidden" id="AttachPartSize[]" name="AttachPartSize[]" value="'.$FileSize.'">';
						$AttachDisplay .= '<br>';
						$AttachmentDisplayList .= $AttachDisplay;
					}
					$AttachmentDisplayList .= '</ul>';
				}
				break;
			default:
				$FinalTo = $toAddress;
				$FinalCc = $ccAddress;
				$FinalBcc = $bccAddress;
				$FinalBody = $Body;
				// process attachment
				if (sizeof($AttachList) > 0) {
					$AttachmentDisplayList = '<ul>';
					for ($i=0; $i< sizeof($AttachList); $i++) {
						$FileSize = $AttachList[$i]['FileSize'];
						//if($AttachList[$i]['Disposition'] == 'inline') continue;
						if (!$AttachList[$i]['IsTNEF']) {
							$AttachDisplay = '<input type="checkbox" id="AttachPartNumber[]" name="AttachPartNumber[]" value="'.$AttachList[$i]['PartID'].'" checked>';

							if ($AttachList[$i]['AttachType'] == "File"){
								$AttachDisplay .= '<a href="'.$PATH_WRT_ROOT.'home/imail_gamma/cache_view_attachment.php?PartNumber='.$AttachList[$i]['PartID'].'&Folder='.urlencode($Folder).'&MessageID='.$MessageID.'" class="tabletool">';
							}else {
								$AttachDisplay .= '<a href="javascript:void(0);" onclick="window.open(\''.$PATH_WRT_ROOT.'home/imail_gamma/read_email.php?PartNumber='.$AttachList[$i]['PartID'].'&Folder='.urlencode($Folder).'&uid='.$MessageID.'&NoButton=1\');" class="tabletool">';
							}
						}
						else {
							$AttachDisplay = '<input type="checkbox" id="TNEFFile[]" name="TNEFFile[]" value="'.$AttachList[$i]['FileName'].'" checked>';
							$AttachDisplay .= '<a href="'.$AttachList[$i]['FilePath'].'" target="_blank" class="tabletool">';

						}
						$AttachDisplay .= $AttachList[$i]['FileName'].'('.$FileSize.'K)';
						$AttachDisplay .= '</a>';
						$AttachDisplay .= '<input type="hidden" id="AttachPartSize[]" name="AttachPartSize[]" value="'.$FileSize.'">';
						$AttachDisplay .= '<br>';
						$AttachmentDisplayList .= $AttachDisplay;
					}
					$AttachmentDisplayList .= '</ul>';
				}
				
//				// process attachment
//				if (sizeof($AttachList) > 0) {
//					$AttachmentDisplayList = $this->Get_List_Open();
//					for ($i=0; $i< sizeof($AttachList); $i++) {
//						$FileSize = $AttachList[$i]['FileSize'];
//						
//						if (!$AttachList[$i]['IsTNEF']) {
//							$AttachDisplay = $this->Get_Input_CheckBox("AttachPartNumber[]","AttachPartNumber[]",$AttachList[$i]['PartID'],true,'');
//							if ($AttachList[$i]['AttachType'] == "File") 
//								$AttachDisplay .= $this->Get_HyperLink_Open($PathRelative."src/module/email/cache_view_attachment.php?PartNumber=".$AttachList[$i]['PartID']."&Folder=".urlencode($Folder)."&MessageID=".$MessageID);
//							else 
//								$AttachDisplay .= $this->Get_HyperLink_Open("#",'onclick="window.open(\''.$PathRelative.'src/module/email/gamma/read_email.php?PartNumber='.$AttachList[$i]['PartID'].'&Folder='.urlencode($Folder).'&uid='.$MessageID.'&NoButton=1\');" class="imail_entry_read_link"');
//						}
//						else {
//							$AttachDisplay = $this->Get_Input_CheckBox("TNEFFile[]","TNEFFile[]",$AttachList[$i]['FileName'],true,'');
//							$AttachDisplay .= $this->Get_HyperLink_Open($AttachList[$i]['FilePath'],'target="_blank" class="imail_entry_read_link"');
//						}
//						$AttachDisplay .= $AttachList[$i]['FileName'].'('.$FileSize.'K)';
//						$AttachDisplay .= $this->Get_HyperLink_Close();
//						$AttachDisplay .= $this->Get_Input_Hidden("AttachPartSize[]", "AttachPartSize[]", $FileSize);
//						$AttachDisplay .= $this->Get_Br();
//						$AttachmentDisplayList .= $AttachDisplay;
//					}
//					$AttachmentDisplayList .= $this->Get_List_Close();
//				}
				break;
		}
		/*
		// Use shared mail box Signature
		if($_SESSION['SSV_LOGIN_EMAIL']!=$user->ImapUserEmail){
			$signature_sql = "SELECT Signature FROM MAIL_SHARED_MAILBOX WHERE MailBoxName = '".$_SESSION['SSV_LOGIN_EMAIL']."' ";
			$signature_res = $user->returnVector($signature_sql);
			if(trim($signature_res[0])!="") $Signature = $signature_res[0];
		}
		*/

		//global $userBrowser;
		if (!$sys_custom['iMailPlus']['CKEditor'] && ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod"))
		{
			if ($DraftUID)
			{
				#  use drafted contents
				$FinalBody = str_replace("<br />", "", trim(strip_tags($FinalBody)));
			} else
			{
				if ($Signature!=strip_tags($Signature))
				{
					$Signature = strip_tags(convert_line_breaks($Signature));
				}
				$BodyPlain = str_replace("\n", "\n> ", trim(strip_tags(convert_line_breaks($FinalBody))));
				$FinalBody = "";
				if (trim($Signature)!="")
				{
					$FinalBody .= "\n\n".$Signature;
				}
				if (trim($BodyPlain)!="")
				{
					$FinalBody .= "\n\n\n".$BodyPlain;
				}
			}
		} else 
		{
			if ($Signature==strip_tags($Signature))
			{
				$Signature = nl2br($Signature);
			}
			$FinalBody = ($DraftUID?"":($Signature=="" && $FinalBody!=""?"<br />":$Signature)).$FinalBody;
		}
		// end process		
		
		if (!empty($mailList)){
			$Subject = "Invitaion to New Event";
			$FinalBody = "You are invited to join......<br><br>Please Click the link below to view details.<br>";
			$link = "http".($_SERVER["SERVER_PORT"]==443?"s":"")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80 && $_SERVER["SERVER_PORT"]!=443?":".$_SERVER["SERVER_PORT"]:"")."/home/iCalendar/new_event.php?editEventID=event".$eventID."&calViewPeriod=&CalType=";
			$FinalBody .= '<a href="'.$link.'">'.$link."</a>";
		}
		if (!empty($mailList)){
			foreach($mailList as $addr){
				$FinalTo .= $addr["email"]."; ";
			}
			
		}
		
		//---------------- for internal use ----------------
		if(!empty($internalUseArr) && isset($internalUseArr['internalUse']))
		{
			$Subject = $internalUseArr['internalSubject'];
			if(trim($internalUseArr['internalBody'])!=""){
				if($internalUseArr['internalEnc']){
					$internal_msg = base64_decode($internalUseArr['internalBody']);
				}else{
					$internal_msg = $internalUseArr['internalBody'];
				}
				$internal_use_from_charset = mb_detect_encoding($internal_msg, 'BIG5, GB2312, GBK, UTF-8, ASCII, US-ASCII');
				if(!in_array(strtolower($internal_use_from_charset),array('utf-8','utf8'))){
					$FinalBody = @mb_convert_encoding($internal_msg,"UTF-8",$internal_use_from_charset);
					//$FinalBody = iconv($internal_use_from_charset,"UTF-8"."//TRANSLIT",$internalUseArr['internalBody']);
				}else
				{
					$FinalBody = $internal_msg;
				}
			}
			$FinalTo = $internalUseArr['internalTo'];
			$FinalCc = $internalUseArr['internalCc'];
			$FinalBcc = $internalUseArr['internalBcc'];
			$selectedMailBox = $internalUseArr['internalSender'];
		}
		//---------------- end internal use ----------------
		
		#################### prepare array for autocomplete #########################
		$AddressBookArr = array();
		$InternalArr = array();
		$libdb = new libdb();
		
		### General User - Can only search email address from their own external address book
		$AddressBookSql = "
						SELECT 
							AddressID, TargetName, CONCAT(TargetName,' &lt;',TargetAddress,'&gt;') 
						FROM 
							INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
						WHERE 
							OwnerID = $UserID
						";
		$AddressBookArr = $libdb->returnArray($AddressBookSql,3); 
		
		### If the user is a staff/teacher, they can also search the email address by typing the login name of the target user
		if($CurUserType == 1)
		{
			$InternalSql = "SELECT 
								UserID, EnglishName, CONCAT(EnglishName, IF(RecordType = 1, '', IF(ClassName = '' OR ClassName IS NULL, '', CONCAT(' (',ClassName,'-',ClassNumber,')'))) ,' &lt;',UserLogin,'@','".$lwebmail->mailaddr_domain."','&gt;') 
							FROM 
								INTRANET_USER 
							WHERE
								RecordType = 1 AND RecordStatus = 1
							ORDER BY RecordType, IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName";
			$InternalArr = $libdb->returnArray($InternalSql,3);
		}
		$TotalArr = array_merge($AddressBookArr,$InternalArr);
		
		for ($countli = 0; $countli < sizeof($TotalArr); $countli++) {
			($countli == 0) ? $liList = "<li class=\"\" style=\"display: none;\">".$TotalArr[$countli][2]."</li>\n" : $liList = "<li style=\"display: none;\">".$TotalArr[$countli][2]."</li>\n";
			$liArr .= "\"".addslashes(str_replace("&#039;","'",str_replace("&quot;", "\"", str_replace("&gt;", ">", str_replace("&lt;", "<", str_replace("&amp;","&",$TotalArr[$countli][2]))))))."\"";
			($countli == (sizeof($TotalArr)-1)) ? $liArr .= "" : $liArr .= ",\n";
		}
		#################### prepare array for autocomplete end ########################
		
		# UI Start
		$x .= '<table width="100%" cellspacing="0" cellpadding="5" border="0" align="center">';
			$x .= '<tbody>';
		
		$addccdisplay = !$FinalCc?"":"none";
		$addbccdisplay = !$FinalBcc?"":"none";
		$delimiterdisplay = (!$FinalCc&&!$FinalBcc)?"":"none";
		
		### From Email Address is selectable if have Shared Mail Box ###
		$selectedMailBox = trim($selectedMailBox)!=''?$selectedMailBox:$_SESSION['SSV_EMAIL_LOGIN'];
		$SharedMail = $this->GetUserSharedMailBox($UserID);
		if(sizeof($SharedMail)>0)
		{
			$MailBoxList[] = array($user->ImapUserEmail,$user->ImapUserEmail);
			for($i=0; $i<sizeof($SharedMail);$i++)
			{
				$MailBoxList[] = array($SharedMail[$i]["MailBoxName"],$SharedMail[$i]["MailBoxName"]);
			}
			$FromSharedMailBoxSelection = getSelectByArray($MailBoxList,"name='FromSharedMailBox' id='FromSharedMailBox' ",$selectedMailBox,0,1);
			$x .= '<tr>';
			$x .= '<td class="formfieldtitle" width="20%" >'.$Lang['Gamma']['Sender'].'</td>';
			$x .= '<td class="tabletext">'.$FromSharedMailBoxSelection.'</td>';
			$x .= '</tr>';
		}
		### -------------------------------------------------------- ###
					
		#to field
			$toArray = array();
				$x .= '<tr>';
					$x .= '<td class="formfieldtitle" width="20%">'.$Lang['Gamma']['To'].'</td>';
					$x .= '<td class="tabletext">';
						$x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">';
							$x .= '<tr>';
								$x .= '<td>';
									$x .= $this->Get_AddressBook_Select_Toolbar("ToAddress",$FinalTo);
								$x .= '</td>';
								$x .= '<td nowrap="nowrap" width="50%">';
									$x .= '<span id="addccrow" style="display:'.$addccdisplay.'"><a class="tabletool" href="javascript:void(0);" onclick="showCc(\'cc\')">'.$Lang["Gamma"]["AddCc"].'</a></span>';
									//$x .= '<span id="delimiter" style="display:'.$delimiterdisplay.'"> | </span>';
									$x .= '<br /><span id="addbccrow" style="display:'.$addbccdisplay.'"><a class="tabletool" href="javascript:void(0);" onclick="showCc(\'bcc\')">'.$Lang["Gamma"]["AddBcc"].'</a></span>';	
								$x .= '</td>';
							$x .= '</tr>';
						$x .= '</table>';
					$x .= '</td>';
				$x .= '</tr>';
		#cc field
			$ccdisplay = $FinalCc?"":"none";
				$x .= '<tr id="ccrow" style="display:'.$ccdisplay.'">';
					$x .= '<td class="formfieldtitle" width="20%">'.$Lang['Gamma']['cc'].'</td>';
					$x .= '<td class="tabletext">';
						//$x .= $linterface->GET_TEXTAREA("CcAddress", $FinalCc, 65, 5,"",0,' oninput="$(this).keydown();" ');
						$x .= $this->Get_AddressBook_Select_Toolbar("CcAddress",$FinalCc);
					$x .= '</td>';
				$x .= '</tr>';
		
		#bcc field
			$bccdisplay = $FinalBcc?"":"none";
				$x .= '<tr id="bccrow" style="display:'.$bccdisplay.'">';
					$x .= '<td class="formfieldtitle" width="20%">'.$Lang['Gamma']['bcc'].'</td>';
					$x .= '<td class="tabletext">';
						//$x .= $linterface->GET_TEXTAREA("BccAddress", $FinalBcc, 65, 5,"",0,' oninput="$(this).keydown();" ');
						$x .= $this->Get_AddressBook_Select_Toolbar("BccAddress",$FinalBcc);
					$x .= '</td>';
				$x .= '</tr>';		
//		#add bcc
//				$addbccdisplay = !$FinalBcc?"":"none";
//				$x .= '<tr id="addbccrow" style="display:'.$addbccdisplay.'">';
//					$x .= '<td class="formfieldtitle" width="20%">&nbsp;</td>';
//					$x .= '<td class="formfieldtitle"><a class="tabletool" href="javascript:void(0);" onclick="showBcc()">'.$Lang["Gamma"]["AddBcc"].'</a></td>';
//				$x .= '</tr>';							
		#Subject field
				$x .= '<tr>';
					$x .= '<td class="formfieldtitle" width="20%" >'.$Lang['Gamma']['Subject'].'</td>';
					$x .= '<td class="tabletext"><input class="textboxtext" type="text" name="Subject" id="Subject" size="55" maxlength="255" value="'.$Subject.'"  tabindex="2"/></td>';
				$x .= '</tr>';
		
		# Inline Images Part
			$InlineImagePart = '';
			$ImageIFrame = $linterface->Get_IFrame("0","ImageListRefresh","ImageListRefresh","no","0","0","","0","0",'style="visibility:visible;"');
		//	$AddImageBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['AddImage'], "button", "AddImageImport();","submit3","");
		//	$AddImageBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['ShowUploadImageWidget'], "button", "displayUploadImagePanel('DivUploadImagePanel','BtnShowUploadImagePanel');","BtnShowUploadImagePanel","");
		if($is_below_IE8) {	
			$AddImageBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['AddImage'], "button", "AddImageImport();","BtnAddImage"," tabindex=\"4\" ");
		}else{
			$AddImageBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['AddImage'], "button", "","BtnAddImage"," tabindex=\"4\" ");
		}	
			$ImagePart .= '<span name="mail_form_inline_image">';
				$ImagePart .= '<span id="mail_form_label_content_whole3" name="mail_form_label_content_whole3">';
					$ImagePart .= '<div id="add_inline_image" name="add_inline_image">';
						$ImagePart .= '<span id="MailImageList" name="MailImageList">';// For storing uploaded image file data
			$related_inline_list = $Mail[0]["RelatedInlineList"];
			if(sizeof($related_inline_list)>0){
				for($i=0;$i<sizeof($related_inline_list);$i++){
					//$ImagePart .= '<input type="hidden" name="ImageFileSize[]" value="'.round(filesize($PATH_WRT_ROOT."file/gamma_mail/u$UserID".$related_inline_list[$i]['FilePath'])/1024,2).'" />';
					//$ImagePart .= '<input type="hidden" name="ImageFileID[]" value="'.$related_inline_list[$i]['FileContentID'].'" />';
					//$ImagePart .= '<input type="hidden" name="ImageFileID[]" value="'."/".$PATH_WRT_ROOT."file/gamma_mail/u$UserID".$related_inline_list[$i]['FilePath'].'" />';
					if(file_exists($related_inline_list[$i]['FilePath'])){
						$ImagePart .= '<input type="hidden" name="ImageFileSize[]" value="'.round(filesize($related_inline_list[$i]['FilePath'])/1024,2).'" />';
// 						$ImagePart .= '<input type="hidden" name="ImageFileID[]" value="'."/".$related_inline_list[$i]['FilePath'].'" />';
// 						WAF workaround
						$ImagePart .= '<input type="hidden" name="ImageFileID[]" value="'.$related_inline_list[$i]['FileContentID'].'" />';
					}
				}
			}
						$ImagePart .= '</span>';
						$ImagePart .= '<span id="ImageFileInputList" name="ImageFileInputList"></span>';// For browse buttons
					$ImagePart .= '</div>';
				$ImagePart .= '</span>';
			if(!$is_mobile_tablet_platform) {	
				$ImagePart .= '<div id="ImageLinkDiv" name="ImageLinkDiv">'.$AddImageBtn.'<span class="tabletextremark"> ( '.$Lang['Gamma']['InsertImageToContent'].' )</span>'.'</div>';// Add One More Image Button
			}
			$ImagePart .= '</span>';
			$InlineImagePart .= '<tr>';
					//$InlineImagePart .= '<td class="formfieldtitle" width="20%" ><img border="0" align="absmiddle" alt="Attachment" src="/images/'.$LAYOUT_SKIN.'/iMail/icon_attachment.gif">'.$Lang['Gamma']['InsertImageToContent'].'</td>';
					$InlineImagePart .= '<td  class="tabletext">';
						$InlineImagePart .= $ImageIFrame;
						$InlineImagePart .= '<span id="ImageListIFrame" name="ImageListIFrame" ></span>';
						$InlineImagePart .= '<div id="DivUploadImagePanel"></div>'."\n";
						$InlineImagePart .= $ImagePart;
					$InlineImagePart .='</td>';
			$InlineImagePart .= '</tr>';
			$InlineImagePart .= '<input type="hidden" name="ImageInputNumber" id="ImageInputNumber" value="" />';
			$InlineImagePart .= '<script>
								   <!--
								   var ImageFileCount=0;
								   function AddImageImport(){
										var Obj = document.getElementById("ImageFileInputList");
										Obj.innerHTML += \'<div id="ImageFile\'+ImageFileCount+\'"><input type="file" name="userfile_image\'+ImageFileCount+\'" id="userfile_image\'+ImageFileCount+\'" onchange="ImageFileAttachSubmit(\'+ImageFileCount+\')" class="textbox" style="width:200px" tabindex="4" /><a href="javascript:void(0)" class="tabletool" onclick="DeleteImageImport(\'+ImageFileCount+\')" tabindex="4" />'.$Lang['Btn']['Remove'].'</a></div>\';
										document.getElementById("userfile_image"+ImageFileCount).focus();
										ImageFileCount++;
										if(bindClickEventToElements){
											bindClickEventToElements();
										}
								   }
								   function ImageFileAttachSubmit(ImageFileNumber){
											var ImageIframeList = document.getElementById("ImageListIFrame");
											ImageIframeList.innerHTML += \'<iframe width="0" height="0" name="ImageFileOperation\'+ImageFileNumber+\'" id="ImageFileOperation\'+ImageFileNumber+\'" src="" style="visibility:hidden;" />\';
											var ImageInputNumber = document.getElementById("ImageInputNumber");
											ImageInputNumber.value=ImageFileNumber;
											
											var Obj = document.getElementById("NewMailForm");
											Obj.target = "ImageFileOperation"+ImageFileNumber; 
											Obj.encoding = "multipart/form-data";
											Obj.action = "upload_image.php";
											Obj.method= "POST";
											Obj.submit();
											
											var ImageFileList = document.getElementById("ImageFile"+ImageFileNumber);
											var filenamestart = document.getElementById("userfile_image"+ImageFileNumber).value.lastIndexOf("\\\");
											var filename = document.getElementById("userfile_image"+ImageFileNumber).value.substring(filenamestart+1)
											ImageFileList.innerHTML = \'"\' + filename + \'" '.$Lang['Gamma']['Uploading'].'...<br>\';
								   }
								   function DeleteImageImport(ImageFileNumber){
											$(\'div#ImageFile\'+ImageFileNumber).remove();
								   }
								   //-->
								   </script>';
		
				$FinalBody = cleanHtmlJavascript(trim($FinalBody));
				
				//$x .= '<tr style="display:none"><td colspan="2"><div id="MailBodyContent" style="display:none">'.$FinalBody.'</div></td></tr>';
				$x .= '<tr style="display:none"><td colspan="2"><textarea id="MailBodyContent" style="display:none">'.str_replace(array('&lt;','&gt;'),array('&amp;lt;','&amp;gt;'),$FinalBody).'</textarea></td></tr>';
		# HTML Editor
				$x .= '<tr>';
					$x .= '<td class="formfieldtitle" width="20%">'.$Lang['Gamma']['Message'].'</td>';
					$x .= '<td class="tabletext">';
						$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
							$x .= '<tr>';
								$x .= '<td>';
									$x .= '<br style="clear:both">';
									
									# Components size
									$msg_box_width = "100%";
									$msg_box_height = 500;
									$obj_name = "MailBody";
									$editor_width = $msg_box_width;
									$editor_height = $msg_box_height;
									
									if($sys_custom['iMailPlus']['CKEditor']){
										$FinalBody = str_replace(array('&lt;','&gt;'),array('&amp;lt;','&amp;gt;'),$FinalBody);
										$x .= getCkEditor("MailBody", $FinalBody, "default",$editor_height, $editor_width);
									}else{
									//global $userBrowser;
										if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
										{
											$x .= "<textarea name='MailBody' id='MailBody' cols='75%' rows='20'>".(($FinalBody!="" && !$DraftUID)?"\n\n\n":"").$FinalBody."</textarea>";
										} else
										{
											$FinalBody = str_replace(array('&lt;','&gt;'),array('&amp;lt;','&amp;gt;'),$FinalBody);
											include_once($PATH_WRT_ROOT.'templates/html_editor_imailplus/fckeditor.php');
											$oFCKeditor = new FCKeditor('MailBody',$editor_width,$editor_height);
											$oFCKeditor->Value = $FinalBody;
											$oFCKeditor->OtherIframeTagInfo = "tabindex='3'";
											$x .= $oFCKeditor->Create2();
										}
									}
								$x .= '</td>';
							$x .= '</tr>';
							$x .= $InlineImagePart;
						$x .= '</table>';
						$x.= '<input type="checkbox" name="ImportantFlag" id="CheckImportant" value="1" tabindex="5">';
						$x.= '<label for="CheckImportant" >';
							$x.= $i_frontpage_campusmail_icon_important2_2007." ".$i_frontpage_campusmail_important;
						$x.= '</label>';
						//$notifyChecked = $sys_custom['iMailPlusDefaultCheckNotify']? 'checked="checked"':'';
						$notifyChecked = $sys_custom['iMailPlusNotCheckNotify']?'':'checked="checked"';
						$x.= '<input type="checkbox" name="IsNotification" value="1" id="CheckNotification" '.$notifyChecked.'  tabindex="6" />';
						$x.= '<label for="CheckNotification" >';
						$x.= $i_frontpage_campusmail_icon_notification2_2007.$i_frontpage_campusmail_notification;
						$x.= '<span class="tabletextremark"> ( '.$i_CampusMail_New_NotificationInternalOnly.' )</span>';
					$x .='</td>';
				$x .= '</tr>';
		
			//$x .= $InlineImagePart;
			
			# attachment part
				# hidden Iframe to process attachment list
				$IFrame .= $linterface->Get_IFrame("0","AttachListRefresh","AttachListRefresh","no","0","0","","0","0",'style="visibility:visible;"');

				$AttachIcon = '<img border="0" align="absmiddle" alt="Attachment" src="/images/'.$LAYOUT_SKIN.'/iMail/icon_attachment.gif">';
			if($is_mobile_tablet_platform || $is_below_IE8) {	
				$AttachBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['AddAttachment'], "button", "AddFileImport();Show_Attach_List_Link('none')","BtnAddAttachment"," tabindex=\"7\" ");
				//$AttachBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['ShowUploadAttachmentWidget'],'button','displayUploadPanel(\'DivUploadPanel\',\'BtnShowUploadPanel\');','BtnShowUploadPanel','');
			}else{	
				$AttachBtn = $linterface->GET_SMALL_BTN($Lang['Gamma']['AddAttachment'],'button','','BtnAddAttachment',' tabindex="7" ');
			}
				//$AttachPart .= '<span id="mail_form_attach" name="mail_form_attach">';
				$AttachPart .= '<span name="mail_form_attach">';
					$AttachPart .= '<span id="mail_form_label_content_whole2" name="mail_form_label_content_whole2">';
						$AttachPart .= '<div id="add_attachment" name="add_attachment">';
							$AttachPart .= '<span id="MailAttachmentList" name="MailAttachmentList">';
								$AttachPart .= $AttachmentDisplayList;
							$AttachPart .= '</span>';
							$AttachPart .= '<span id="AttachmentList" name="AttachmentList">'.$CampusMailAttachmentList.'</span>';
							$AttachPart .= '<span id="FileInputList" name="FileInputList"></span>';
						$AttachPart .= '</div>';
					$AttachPart .= '</span>';
					$AttachPart .= '<div id="AttachLinkDiv" name="AttachLinkDiv">'.$AttachBtn.'</div>';
				$AttachPart .= '</span>';
				$AttachPart .= '
						<script>
						<!--
						var FileCount=0;
						function AddFileImport() {
							var Obj = document.getElementById("FileInputList");
							
							Obj.innerHTML += \'<span id="File\'+FileCount+\'"><input type="file" name="userfile\'+FileCount+\'" id="userfile\'+FileCount+\'" onchange="FileAttachSubmit(\'+FileCount+\')" class="textbox" style="width:400px" tabindex="7"/> <a href="javascript:void(0)" class="tabletool" onclick="FileInputDelete(\'+FileCount+\');Show_Attach_List_Link(\\\'block\\\')" tabindex="7" />'.$Lang['Gamma']['RemoveAttachment'].'</a></span>\';
							document.getElementById("userfile"+FileCount).focus();							
							FileCount++;
							if(bindClickEventToElements){
								bindClickEventToElements();
							}
							
						}
						
						function FileAttachSubmit(FileNumber) {
							var AttachIframeList = document.getElementById("AttachListIFrame");
							AttachIframeList.innerHTML += \'<iframe width="0" height="0" name="FileOperation\'+FileNumber+\'" id="FileOperation\'+FileNumber+\'" src="" style="visibility:hidden;" />\';
							var FileInputNumber = document.getElementById("FileInputNumber");
							FileInputNumber.value=FileNumber;
		
							var Obj = document.getElementById("NewMailForm");
							Obj.target = "FileOperation"+FileNumber; 
							Obj.encoding = "multipart/form-data";
							Obj.action = "attach_update.php";
							Obj.method= "POST";
							Obj.submit();
							
							var FileList = document.getElementById("File"+FileNumber);
							var filenamestart = document.getElementById("userfile"+FileNumber).value.lastIndexOf("\\\");
							var filename = document.getElementById("userfile"+FileNumber).value.substring(filenamestart+1)
							FileList.innerHTML = \'"\' + filename + \'" uploading...<br>\';
						}
						
						function FileDeleteSubmit(FileID) {
							Obj = document.getElementById("NewMailForm");
							Obj.target = "AttachListRefresh"; 
							Obj.action = "attach_remove.php?FileID="+FileID;
                            // WAF workaround
                            $("#MailBody").attr("disabled", "disabled");
							Obj.submit();
                            $("#MailBody").removeAttr("disabled");
						}
						
						function FileInputDelete(FileNumber) {
							var FileList = document.getElementById("File"+FileNumber);
							FileList.innerHTML = \'\';
						}
						//-->
						</script>
						';
				
				if(isset($SYS_CONFIG['Mail']['MaxAttachmentSize'])){
					$MaxSizeRemark = '<span class="tabletextremark">( ';
					$MaxSizeRemark .= str_replace("<!--Size-->",sprintf("%.2f",$SYS_CONFIG['Mail']['MaxAttachmentSize']/1024)."MB", $Lang['Gamma']['RemindMsg']['MaxAttachmentSize']);
					$MaxSizeRemark .= ' )</span>';
				}
				$x .= '<tr>';
					$x .= '<td class="formfieldtitle" width="20%" >'.$AttachIcon.$Lang['Gamma']['Attachment'].'</td>';
					$x .= '<td class="tabletext">';
						$x .= $IFrame;
						$x .= '<span id="AttachListIFrame" name="AttachListIFrame" ></span>';
						$x .= $AttachPart;
						$x .= '<div id="DivUploadPanel" '.(($is_mobile_tablet_platform || $is_below_IE8)?'style="display:none;"':'').'></div>'."\n";
						$x .= $MaxSizeRemark;
					$x .='</td>';
				$x .= '</tr>';
								
			$x .= '</tbody>';
		$x .= '</table>';
					
		return $x;
	}
	
	function Handle_CampusMail_Attachment($Attachment,$ComposeDatetime='')
	{
		    global $file_path,$PATH_WRT_ROOT,$UserID;
		    include_once($PATH_WRT_ROOT."includes/libfiletable.php");
		    $source = "$file_path/file/mail/".$Attachment;
	        $lfiletable_source = new libfiletable("", $source, 0, 0, "");
	        $preset_files = $lfiletable_source->files;
	        $lfs = new libfilesystem();
	        $ldb = new libdb();
	        
	        $personal_path = "$file_path/file/gamma_mail/u$UserID";
	        foreach ($preset_files as $thisfile) 
	        {
	        	if (!is_dir($personal_path))
				{
				    $lfs->folder_new($personal_path);
				}
				
				$FileName = stripslashes($thisfile[0]);
				$TempName = "$source/".$thisfile[0];
//				$lfs->file_copy("$source/".$thisfile[0],$path."/$new_file_name");
				## Main
				
				if ($FileName != "") {
					$sql = 'Insert into MAIL_ATTACH_FILE_MAP 
								(UserID, UploadTime, OriginalFileName, ComposeTime) 
							Values 
								('.$UserID.',NOW(),\''.$ldb->Get_Safe_Sql_Query($FileName).'\',\''.$ComposeDatetime.'\')
						 ';
						 
					$ldb->db_db_query($sql);
					$EncodeName = $ldb->db_insert_id();
				}
				
				$des = "$personal_path/".$EncodeName;
				
				if (!is_file($TempName))
				{
					$sql = 'Delete From MAIL_ATTACH_FILE_MAP 
							Where FileID = \''.$EncodeName.'\' 
							';
					$ldb->db_db_query($sql);
				}
				else {
				  if(strpos($FileName, ".")==0){
				  }
				  else{
				  	$lfs->lfs_copy($TempName, $des);
				  	$FileNumber++;
				  }
				}
	        }
	        if ($FileNumber > 0) 
	        {
				// Database setup
				$sql = "
						SELECT 
							FileID, 
							UserID, 
							UploadTime,
							DraftMailUID,
							OriginalFileName,
							EncodeFileName 
						FROM MAIL_ATTACH_FILE_MAP 
						WHERE 
							UserID = '".$UserID."' AND 
							(
								(DraftMailUID IS NULL AND OrgDraftMailUID IS NULL) OR 
								DraftMailUID = '$MessageUID' OR 
								(DraftMailUID IS NULL AND OrgDraftMailUID = '$MessageUID')
							)";
				if($ComposeDatetime != '')
					$sql .= "AND DATE_FORMAT(ComposeTime,'%Y-%m-%d %H:%i:%s') = '$ComposeDatetime' ";
									
					$AttachList = $ldb->returnArray($sql,6);
					
	        }
	        return $AttachList;
	}
	
	function Get_AddressBook_Select_Toolbar($textfieldName,$FinalTo,$Mode="")
	{
		global $intranet_root,$image_path,$LAYOUT_SKIN,$i_CampusMail_New_SelectFromAlias,$Lang,$i_frontpage_campusmail_choose,$i_frontpage_campusmail_remove,$linterface;
		
		if($this->AccessInternetMail())
		{
			$textareadisplay = " display:block ";
		}
		else
		{
			$textareadisplay = " display:none ";
			$toArray = $this->Split_Text_Into_Name_Email_Array($FinalTo);
			$selection = getSelectByArray($toArray," id=\"".$textfieldName."Select\" name=\"".$textfieldName."Select\" multiple size=5 style='width:350px; '",0,0,1);
		}
		$x .= $linterface->GET_TEXTAREA($textfieldName, $FinalTo, 65, 5,"",0,' oninput="$(this).keydown();" tabindex="1" style="'.$textareadisplay.'"');
		$x .= $selection;
		
		if (!empty($Mode)) {
			$uriMode = "&mode=".strtoupper($Mode);
		}
		
		$x .= '<table border="0" cellspacing="0" cellpadding="2">';
			$x .= '<tr>';
				if($this->AccessInternetMail())
				{
					if(file_exists($intranet_root.'/home/imail_gamma/choose_ex/external_contacts.php')){
						$x .= '<td>';
						$x .= '<a href="javascript:newWindow(\'/home/imail_gamma/choose_ex/external_contacts.php?fieldname='.$textfieldName.$uriMode.'\',9)" class=\'iMailsubject\' >';
						$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif" alt="'.$Lang['Gamma']['ExternalContacts'].'" width="20" height="20" border="0" align="absmiddle" />';
						$x .= $Lang['Gamma']['ExternalContacts'];
						$x .= '</a>';
						$x .= '</td>';
					}else{
						$x .= '<td>';
						$x .= '<a href="javascript:newWindow(\'/home/imail_gamma/choose_ex/alias_ex.php?fieldname='.$textfieldName.$uriMode.'\',9)" class=\'iMailsubject\' >';
						$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif" alt="'.$Lang['Gamma']['SelectFromExternalRecipientGroup'].'" width="20" height="20" border="0" align="absmiddle" />';
						$x .= $Lang['Gamma']['SelectFromExternalRecipientGroup'];
						$x .= '</a>';
						$x .= '</td>';
						$x .= '<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" /></td>';
						$x .= '<td>';
						$x .= '<a href="javascript:newWindow(\'/home/imail_gamma/choose_ex/index.php?fieldname='.$textfieldName.$uriMode.'\',9)" class=\'iMailsubject\' >';
						$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif" alt="'.$Lang['Gamma']['SelectFromExternalRecipient'].'" width="20" height="20" border="0" align="absmiddle" />';
						$x .= $Lang['Gamma']['SelectFromExternalRecipient'];
						$x .= '</a>';
						$x .= '</td>';
						$x .= '<td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" /></td>';
					}
				}
				
				if(file_exists($intranet_root.'/home/imail_gamma/choose_ex/external_contacts.php')){
					$x .= '<td>';
					$x .= '<a href="javascript:newWindow(\'/home/imail_gamma/choose_ex/internal_recipient.php?fieldname='.$textfieldName.$uriMode.'\',9)" class=\'iMailsubject\' >';
					$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif" alt="'.$Lang['Gamma']['InternalContacts'].'" width="20" height="20" border="0" align="absmiddle" />';
					$x .= $Lang['Gamma']['InternalContacts'];
					$x .= '</a>';
					$x .= '</td>';
				}else{
					$x .= '<td>';
					$x .= '<a href="javascript:newWindow(\'/home/imail_gamma/choose/alias_in.php?fieldname='.$textfieldName.$uriMode.'\',9)" class=\'iMailsubject\' >';
					$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif" alt="'.$Lang['Gamma']['SelectFromInternalRecipientGroup'].'" width="20" height="20" border="0" align="absmiddle" />';
					$x .= $Lang['Gamma']['SelectFromInternalRecipientGroup'];
					$x .= '</a>';
					$x .= '</td>';
					
					$x .= '<td>';
					$x .= '<a href="javascript:newWindow(\'/home/imail_gamma/choose_ex/internal_recipient.php?fieldname='.$textfieldName.$uriMode.'\',9)" class=\'iMailsubject\' >';
					$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif" alt="'.$Lang['Gamma']['SelectFromInternalRecipient'].'" width="20" height="20" border="0" align="absmiddle" />';
					$x .= $Lang['Gamma']['SelectFromInternalRecipient'];
					$x .= '</a>';
					$x .= '</td>';
				}
				
				if(!$this->AccessInternetMail())
				{
					$x .= '<td>';
						$x .= '<a href="javascript:void(0)" onclick="RemoveEmailInSelection(\''.$textfieldName.'\')" class=\'iMailsubject\' >';
						$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/btn_delete_selected.gif" alt="'.$i_frontpage_campusmail_remove.'" width="20" height="20" border="0" align="absmiddle" />';
						$x .= $i_frontpage_campusmail_remove;
						$x .= '</a>';
					$x .= '</td>';
				}				
			$x .= '</tr>';
		$x .= '</table>';
		
		return $x;
	}
	
	function Gen_Mail_Attach_File_Map_List($MessageID){
		global $Lang, $personal_path;
		$AttachList = array();
		$returnStr = '';
		
		# Get the Mail Attach File Map List from database
		$AttachList = $this->Get_Mail_Attach_File_Map_List($MessageID);
		if(count($AttachList) > 0){
			for ($i=0; $i< sizeof($AttachList); $i++) {
				$FileSize = (round(filesize($personal_path."/".$AttachList[$i][5])/1024,2));
				
				$returnStr .= '<input type="checkbox" id="FileID[]" name="FileID[]" value="'.$AttachList[$i][0].'" checked style="visiblity:hidden; display:none;">';
				$returnStr .= '<a href="view_compose_attachment.php?FileID='.$AttachList[$i][0].'">';				
					$returnStr .= str_replace("'","\'",UTF8_From_DB_Handler($AttachList[$i][4])).'('.$FileSize.'K)';
				$returnStr .= '</a>';
				$returnStr .= '[<a href="javascript:void(0)" onclick="FileDeleteSubmit(\\\''.$AttachList[$i][0].'\\\')">';				
					$returnStr .= $Lang['Gamma']['RemoveAttachment'];
				$returnStr .= '</a>]';
				$returnStr .= '<input type="hidden" id="FileSize[]" name="FileSize[]" value="'.$FileSize.'">';
				$returnStr .= '<br>';
			}
		}
		return $returnStr;
	}
	
	function displayQuotaBar()
	{
		//to be done
		global $i_Campusquota_used,$LAYOUT_SKIN,$image_path, $Lang;
		
		//if($this->IMapCache===false)
		//	$this->IMapCache = new imap_cache_agent();
		$this->getImapCacheAgent();
		
		$Quota = $this->IMapCache->Check_Quota();
		$used = $Quota["UsedQuota"];
		$total = $Quota["TotalQuota"];
		if(round($used)==0 || round($total)==0 || $total==''){ // maybe fail to get quota via IMAP command, try osapi
			$tmp_used = $this->getUsedQuota($_SESSION['SSV_LOGIN_EMAIL'],"iMail");
			$tmp_total = $this->getTotalQuota($_SESSION['SSV_LOGIN_EMAIL'],"iMail");
			$used = round($tmp_used / 1024 / 1024,2);
			$total = round($tmp_total,2);
		}
		if($total == 0){
			$is_unlimited = true;
			$total = $Lang['Gamma']['Unlimited'];
		}
		$pused = $total==0?0.00:round($used/$total*100,2);
		$pused = sprintf("%.2f",$pused);
		$storage   = "<style type='text/css'> \n";
		//$storage  .= "<!--";
		$storage  .= "#UsageLayer {";
		$storage  .= "	position:absolute;";
		$storage  .= "	width:150px;";
		$storage  .= "	height:14x;";
		$storage  .= "	z-index:2;";
		$storage  .= "}"; 
		//$storage  .= "-->";
		$storage  .= "</style>";
		
		$storage .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' class='imailusage' > \n";
		$storage .= "<tr > \n";			
		$storage .= "<td style='vertical-align:bottom'> \n";
		$storage .= "<div id='UsageLayer' > \n";                                    
		$storage .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' > \n";                                      
		$storage .= "<tr> \n";
		$storage .= "<td align='center' class='imailusagetext' >{$pused}%</td> \n";                                          
		$storage .= "</tr> \n";                                            
		$storage .= "</table> \n";
		$storage .= "</div> \n";                                              
		$storage .= "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/usage_bar.gif' width='$pused%' height='14' align='absmiddle'  ></td> \n";
		$storage .= "</tr> \n";                                            
		$storage .= "</table> \n";
		
		$x  = "";
		$x .= "<table border='0' cellspacing='0' cellpadding='0' style='padding-right:3px'>";
		$x .= "<tr>";
		//$x .= "<td class='tabletext' nowrap>$i_Campusquota_used: ".number_format($used,0)." / ".number_format($total,0)." MB&nbsp;</td>";                              
		$x .= "<td class='tabletext' nowrap>$i_Campusquota_used: ".$used." / ".$total." MB&nbsp;</td>"; 
		if(!$is_unlimited){
			$x .= "<td >".$storage."</td>";                              
		}
		$x .= "</tr> \n";                                            
		$x .= "</table> \n";
		
		if($_SERVER['SCRIPT_NAME'] != '/home/imail_gamma/compose_email.php')
		{
			$x .= '<script type="text/javascript" language="javascript">'."\n";
			$x .= '$(document).ready(function(){'."\n";
			$x .= '$("#UsageLayer").click(function(){$.get("recalculate_quota.php",function(returnData){window.location.reload();});});'."\n";
			$x .= '});'."\n";
			$x .= '</script>'."\n";
		}
		return $x;
		
	}
	
	function getMoveToSelection($ID_Name, $excludeFolder='', $onchange='')
	{
		global $button_moveto,$i_CampusMail_New_alert_moveto;
		
		$folders = $this->Get_Folder_Structure(true);
		foreach((array)$folders as $thisfolder)
		{
			list($FolderName, $FolderPath) =  $thisfolder;
			
			if(in_array($FolderPath,array($this->DefaultFolderList["reportSpamFolder"],$this->DefaultFolderList["reportNonSpamFolder"])))
				continue;
			if($FolderPath!=$excludeFolder && !empty($FolderName))
			{
				$folderlist[] = array($FolderPath,IMap_Decode_Folder_Name($FolderName));
			} 
		}
		if($onchange==''){
			//$onchange = "if (this.selectedIndex != 0) {checkAlert(document.form1,'Uid[]','move_email.php','$i_CampusMail_New_alert_moveto'); this.selectedIndex=0}";
			$onchange = "MoveMailTo(this);";
		}
		$MoveTo = getSelectByArray($folderlist,"name='$ID_Name' onchange=\"$onchange\" ","-999",0,0,$button_moveto);
		
		return 	$MoveTo;	
	}
	
	function getFolderMultiSelection($ID,$Name,$selected=array())
	{
		global $linterface,$button_select_all;
		
		$folders = $this->Get_Folder_Structure(true);
		foreach((array)$folders as $thisfolder)
		{
			list($FolderName, $FolderPath) =  $thisfolder;
			if(!empty($FolderName))
			{
				$folderlist[] = array($FolderPath,IMap_Decode_Folder_Name($FolderName));
			} 
		}

		$selection = getSelectByArray($folderlist,"name='$Name' id='$ID' multiple size='10'",$selected,0,1);
		$SelectAllBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll('$ID'); return false;");
		
		$html .= '<table border="0">';
			$html .= '<tr>';
				$html .= '<td>'.$selection.'</td>';
				$html .= '<td valign="bottom">'.$SelectAllBtn.'</td>';
			$html .= '</tr>';
		$html .= '</table>';
		
		return $html;
	}
	
	function getAddressBookDetail($OwnerID)
	{
		$libdb = new libdb();
		$AddressBookSql = "
				SELECT 
					AddressID,  TargetName, TargetAddress 
				FROM 
					INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
				WHERE 
					OwnerID = $OwnerID
				";

		$AddressBookArr = $libdb->returnArray($AddressBookSql,3); 
		
		return $AddressBookArr;
	}
	
	
	function SwitchMailBox()
	{
		global $UserID;
		
		$selectedMailBox = $_SESSION['SSV_EMAIL_LOGIN'];
		$luser = new libuser($UserID);
		$SharedMail = $this->GetUserSharedMailBox($UserID);
		
		$MailBoxList[] = array($luser->ImapUserEmail,$luser->ImapUserEmail);
		for($i=0; $i<sizeof($SharedMail);$i++)
		{
			$MailBoxList[] = array($SharedMail[$i]["MailBoxName"],$SharedMail[$i]["MailBoxName"]);
		}
		$onchange = ' onchange="this.form.submit()" ';
		
		$selection = getSelectByArray($MailBoxList,"name='SwitchTo' id='SwitchTo' $onchange",$selectedMailBox,0,1);
		$html ='<form name="SwitchMailBoxForm" method="POST" action="switch_mailbox.php" style="margin:0; padding:0">';
		$html .=$selection;
		$html .='</form>';
		
		return $html;
		
	}
	
	function GetUserSharedMailBox($ParUserID='')
	{
		global $UserID;
		if(!$ParUserID)
			$ParUserID = $UserID;
			
		$libdb = new libdb();	
		$sql = "
			SELECT 
				a.MailBoxID, 
				a.MailBoxName, 
				a.MailBoxPassword, 
				a.MailBoxType  
			FROM 
				MAIL_SHARED_MAILBOX a 
				INNER JOIN  MAIL_SHARED_MAILBOX_MEMBER b ON a.MailBoxID = b.MailBoxID		
			WHERE
				UserID = '$ParUserID'
				";
				
		$result = $libdb->returnArray($sql);
		
		return $result;
		
	}
	
	function Get_MailBox_Password($MailBoxName)
	{
		global $UserID;
		$libdb = new libdb();	
		$luser = new libuser($UserID);
		
		if($MailBoxName == $luser->ImapUserEmail)
		{
			return $_SESSION['eclass_session_password'];
		}
		else
		{
			include_once("libpwm.php");
			
			$libpwm = new libpwm();
			$sql = "SELECT MailBoxID FROM MAIL_SHARED_MAILBOX WHERE MailBoxName='$MailBoxName'";
			$rs = $libdb->returnVector($sql);
			$mid = $rs[0];
			if($mid != '' && $mid > 0){
				$midToPw = $libpwm->getShareMailboxData(array($rs[0]));
				return $midToPw[$mid];
			}
			return '';
			/*
			$sql = "
				SELECT 
					MailBoxPassword
				FROM 
					MAIL_SHARED_MAILBOX  						
				WHERE
					MailBoxName = '$MailBoxName'
					";
					
			$result = $libdb->returnVector($sql);
		
			return $result[0];
			*/
		}
	}
	
	function MIME_Decode($str)
	{
		$elements = imap_mime_header_decode($str);
		for($k=0 ; $k<count($elements) ; $k++) {
			if(trim($elements[$k]->text) != '')
			{
				$lc_charset = strtolower($elements[$k]->charset);
				if($lc_charset=="utf-8"){
					$tmp .= $elements[$k]->text;
				}else if(in_array($lc_charset, array("utf-8","default","x-unknown","unknown")) || stristr($elements[$k]->charset,"unknown")){
					$charset = mb_detect_encoding($elements[$k]->text, 'UTF-8, BIG5, GB2312, GBK, ASCII');
					if($charset != "" && strtolower($charset) != 'utf-8')
						$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$charset);
					else
						$tmp .= $elements[$k]->text;
				}else if($lc_charset=="gb2312" || $lc_charset=="gb18030"){
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","GB2312,GBK");
				}else if($lc_charset == "big5-hkscs"){
					$tmp .= @iconv($lc_charset,"UTF-8",$elements[$k]->text);
				}else if($lc_charset == "ks_c_5601-1987"){
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","CP949");
				}
				else{ 
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$elements[$k]->charset);
				}
			}
		}	
		
		return $tmp;
	}
	
	function GetMailFlags($MailIDs, $Folder)
	{
		//if($this->IMapCache===false) 
		//	$this->IMapCache = new imap_cache_agent();
		$this->getImapCacheAgent();
		if(is_array($MailIDs))
			$UIDs = implode(",",$MailIDs);
		else
			$UIDs = $this->IMapCache->Get_UID($MailIDs);
		
		$Flags = $this->IMapCache->Get_Message_Status($UIDs,IMap_Encode_Folder_Name($Folder),true,false);
		
		return $Flags;
	}

	function GetMailPriority($MailIDs, $Folder, $isUid=0)
	{
		//if($this->IMapCache===false)
		//	$this->IMapCache = new imap_cache_agent();
		$this->getImapCacheAgent();
		if(empty($MailIDs)) return false;
		$From = min((array)$MailIDs);
		$To = max((array)$MailIDs);
		$this->IMapCache->Go_To_Folder(IMap_Encode_Folder_Name($Folder));
		$Result = $this->IMapCache->Get_Message_Cache_By_Batch($From,$To,$isUid);
		$this->message_cache = $Result;
		$Key = $isUid==1?"UID":"MsgNo";
		$Result = BuildMultiKeyAssoc($Result,$Key,"ImportantFlag");
		return $Result;
	}
		
	function TitleToolBar()
	{
		$x = "<table border=0 cellspacing=0 cellpadding=0>";
			$x .= "<tr>";
				$x .= "<td align='right' style=\"vertical-align: bottom;\">".$this->displayQuotaBar()."</td>";
				$x .= "<td align='right' style=\"vertical-align: bottom;\" nowrap>".$this->SwitchMailBox()."</td>";
			$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	}
	
	function TrimSubjectPrefix($ParSubject)
	{
		$regexp = '/^\s*(re\s*:{1}\s*|fwd?\s*:{1}\s*)*/i'; 
		$trimed_subject = trim(preg_replace($regexp, '', $ParSubject));
		
		return $trimed_subject;
	}
	
	function replaceQuotesForDisplayName($s)
	{
		//return str_replace(array('"','&quot;'),array('\"','\"'),$s);
		return str_replace(array('"','&quot;','\\',"'"),'',$s);
		/*
		$t = str_replace('&quot;','\"',$s);
		$l = strlen($t);
		$o = '';
		for($i=0;$i<$l;$i++){
			if($t[$i] == '"' && $i>=0 && $t[$i-1]!='\\'){
				$o .= '\\'.$t[$i];
			}else{
				$o .= $t[$i];
			}
		}
		return $o;
		*/
	}
	
	function removeQuotesForDisplayName($s)
	{
		$o = '';
		$l = strlen($s);
		for($i=0;$i<$l;$i++){
			if($s[$i] == '\\' && $i+1 < $l && $s[$i+1]=='"'){
				
			}else{
				$o .= $s[$i];
			}
		}
		return $o;
	}
	
	// get email list for internal user email (internal_recipient.php) modified from libwebmail.php (start)
	function GetAllTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", ImapUserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
			}else{
				# Staff
				$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", ImapUserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
			}
		} else if($TargetUserType == 2) {
			# Student 
			$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", ImapUserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
		} else if($TargetUserType == 3) {
			# Parent 
			$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", ImapUserEmail FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
		} else{
			## Unknown identity
			$js_all_teacher = "";
		}
		
		$result = $li->returnArray($all_teacher_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_teacher = $js_all_teacher."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_teacher = $js_all_teacher.$final_email."; ";
			}
		}else{
			$js_all_teacher = "";
		}
		
		return $js_all_teacher;
	}
	
	function GetAllSameFormTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$yearClassIdSql = "SELECT DISTINCT c.YearClassID 
									FROM YEAR_CLASS_TEACHER AS a 
									INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
									INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) 
									WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
				$yearClassIds = $li->returnVector($yearClassIdSql);
				$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
				
				$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
								FROM INTRANET_USER AS a 
								INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
								WHERE a.RecordStatus = 1 AND b.YearClassID IN ($yearClassIdCsv)
									AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
								ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

			}else{
				# Staff
				$js_all_form_teacher = "";
			}
		}else if($TargetUserType == 2){
			# Student
			$yearClassIdSql = "SELECT DISTINCT c.YearClassID 
								FROM YEAR_CLASS_TEACHER AS a 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) 
								WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
						FROM INTRANET_USER AS a 
						INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
						WHERE a.RecordStatus = 1 
							AND b.YearClassID IN ($yearClassIdCsv)
							AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
						ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else if($TargetUserType == 3){
			# Parent
			$yearClassIdSql = "SELECT DISTINCT c.YearClassID 
								FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) 
								WHERE relation.ParentID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
						FROM INTRANET_USER AS a 
						INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
						WHERE a.RecordStatus = 1 
							AND b.YearClassID IN ($yearClassIdCsv) 
							AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
						ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			## Unknown identity
			$js_all_form_teacher = "";
		}
		
		$result = $li->returnArray($form_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_form_teacher = $js_all_form_teacher."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_form_teacher = $js_all_form_teacher.$final_email."; ";
			}
		}else{
			$js_all_form_teacher = "";
		}
		
		return $js_all_form_teacher;
	}
	
	function GetAllSameClassTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$yearClassIdSql = "SELECT DISTINCT b.YearClassID 
								FROM YEAR_CLASS_TEACHER AS a 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN YEAR AS c ON (b.YearID = c.YearID) 
								WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID'";
				$yearClassIds = $li->returnVector($yearClassIdSql);
				$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
				
				$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
							WHERE a.RecordStatus = 1 
								AND b.YearClassID IN ($yearClassIdCsv)
								AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

			}else{
				#Staff
				$class_sql = "";
			}
		}else if($TargetUserType == 2){
			# Student
			$yearClassIdSql = "SELECT DISTINCT b.YearClassID 
								FROM YEAR_CLASS_USER AS a 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			
			$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
						  	FROM INTRANET_USER AS a 
							INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
							WHERE a.RecordStatus = 1 
								AND b.YearClassID IN ($yearClassIdCsv)
							 	AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else if($TargetUserType == 3){
			# Parent
			$yearClassIdSql = "SELECT DISTINCT b.YearClassID 
								FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								WHERE relation.ParentID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
							WHERE a.RecordStatus = 1 
								AND b.YearClassID IN ($yearClassIdCsv)
								AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			# Identity Unknown
			$class_sql = "";
		}
		
		$result = $li->returnArray($class_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_class_teacher = $js_all_class_teacher."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_class_teacher = $js_all_class_teacher."$final_email"."; ";
			}
		}else{
			$js_all_class_teacher = "";
		}
		
		return $js_all_class_teacher;
	}
	
	function GetAllSameSubjectTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID 
										FROM SUBJECT_TERM_CLASS_TEACHER AS a 
										INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
										INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
										WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
				$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
				$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
				$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
								FROM INTRANET_USER AS a 
								INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
								WHERE a.RecordStatus = 1 
									AND b.SubjectGroupID IN ($subjectGroupIdCsv)
									AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
								ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

			}else{
				# Staff
				$subject_sql = "";
			}
		}else if($TargetUserType == 2){
			# Student
			$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
							WHERE a.RecordStatus = 1 
								AND b.SubjectGroupID IN ($subjectGroupIdCsv)
								AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else if($TargetUserType == 3){
			# Parent
			$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
							WHERE a.RecordStatus = 1 
								AND b.SubjectGroupID IN ($subjectGroupIdCsv) 
								AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			# Identity Unknown
			$subject_sql = "";
		}
		
		$result = $li->returnArray($subject_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_subject_teacher = $js_all_subject_teacher."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_subject_teacher = $js_all_subject_teacher."$final_email"."; ";
			}
		}else{
			$js_all_subject_teacher = "";
		}
		
		return $js_all_subject_teacher;
	}
	
	function GetAllSameSubjectGroupTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_TEACHER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
				$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
				$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
				
				$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
										FROM INTRANET_USER AS a 
										INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
										WHERE a.RecordStatus = 1 
											AND b.SubjectGroupID IN ($subjectGroupIdCsv) 
											AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
										ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

			}else{
				# Staff
				$subject_group_sql = "";
			}
		} else if($TargetUserType == 2) {
			# Student
			$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID 
								FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
								WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
									FROM INTRANET_USER AS a 
									INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
									WHERE a.RecordStatus = 1 
										AND b.SubjectGroupID IN ($subjectGroupIdCsv)
										 AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
									ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		} else if($TargetUserType == 3) {
			# Parent
			$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", ImapUserEmail 
									FROM INTRANET_USER AS a 
									INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) 
									WHERE a.RecordStatus = 1 
										AND b.SubjectGroupID IN ($subjectGroupIdCsv)
										AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
									ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		} else {
			# identity unknown
			$subject_group_sql = "";
		}
		
		$result = $li->returnArray($subject_group_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_subject_group_teacher = $js_all_subject_group_teacher."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_subject_group_teacher = $js_all_subject_group_teacher."$final_email"."; ";
			}
		}else{
			$js_all_subject_group_teacher = "";
		}
		
		return $js_all_subject_group_teacher;
	}
	
	function GetAllStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		$all_student_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang().", ImapUserEmail FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND (ClassName != '' OR ClassName != NULL) AND (ClassNumber != '' OR ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
		$result = $li->returnArray($all_student_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_student = $js_all_student."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_student = $js_all_student."$final_email"."; ";
			}
		}else{
			$js_all_student = "";
		}
				
		return $js_all_student;
	}
	
	function GetAllSameFormStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$yearClassIdSql = "SELECT DISTINCT c.YearClassID 
									FROM YEAR_CLASS_TEACHER AS a 
									INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
									INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) 
									WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
				$yearClassIds = $li->returnVector($yearClassIdSql);
				$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
				$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", ImapUserEmail 
							FROM YEAR_CLASS_USER AS a 
							INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
							WHERE b.RecordStatus = 1 
								AND a.YearClassID IN ($yearClassIdCsv)
								 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

			} else {
				# Staff
				$form_sql = "";
			}
		}else if($TargetUserType == 2){
			$yearClassIdSql = "SELECT DISTINCT c.YearClassID 
								FROM YEAR_CLASS_USER AS a 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) 
								WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", ImapUserEmail 
						FROM YEAR_CLASS_USER AS a 
						INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
						WHERE b.RecordStatus = 1 
							AND a.YearClassID IN ($yearClassIdCsv) 
							 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
						ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";	

		}else if($TargetUserType == 3){
			$yearClassIdSql = "SELECT DISTINCT c.YearClassID 
								FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
								INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) 
								WHERE relation.ParentID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", ImapUserEmail 
						FROM YEAR_CLASS_USER AS a 
						INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
						WHERE b.RecordStatus = 1 
							AND a.YearClassID IN ($yearClassIdCsv)
							AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '')
						ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

		}else{
			$form_sql = "";
		}
		
		$result = $li->returnArray($form_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_form_student = $js_all_same_form_student."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_form_student = $js_all_same_form_student."$final_email"."; ";
			}
		}else{
			$js_all_same_form_student = "";
		}
		
		return $js_all_same_form_student;
	}
	
	function GetAllSameClassStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$yearClassIdSql = "SELECT DISTINCT a.YearClassID 
									FROM YEAR_CLASS AS a 
									INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) 
									WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$yearClassIds = $li->returnVector($yearClassIdSql);
				$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
				$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", ImapUserEmail 
								FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE b.RecordStatus = 1 
									AND a.YearClassID IN ($yearClassIdCsv)
									 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
								ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

			}else{
				# staff
				$class_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$yearClassIdSql = "SELECT DISTINCT a.YearClassID 
								FROM YEAR_CLASS AS a 
								INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
								WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", ImapUserEmail 
							FROM YEAR_CLASS_USER AS a 
							INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
							WHERE b.RecordStatus = 1 
								AND a.YearClassID IN ($yearClassIdCsv) 
								 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
							ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

		}else if($TargetUserType == 3){
			# parent
			$yearClassIdSql = "SELECT DISTINCT a.YearClassID 
								FROM YEAR_CLASS AS a 
								INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) 
								WHERE relation.ParentID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
			$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", ImapUserEmail 
						FROM YEAR_CLASS_USER AS a 
						INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
						WHERE b.RecordStatus = 1 
							AND a.YearClassID IN ($yearClassIdCsv)
							 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (IMapUserEmail IS NOT NULL AND IMapUserEmail<> '') 
						ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

		}else{
			# identity unknown
			$class_sql = "";
		}
		
		$result = $li->returnArray($class_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_class_student = $js_all_same_class_student."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_class_student = $js_all_same_class_student."$final_email"."; ";
			}
		}else{
			$js_all_same_class_student = "";
		}
		
		return $js_all_same_class_student;
	}
	
	function GetAllSameSubjectStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_TEACHER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
				$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
				$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
				$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", b.ImapUserEmail 
								FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE b.RecordStatus = 1 
									AND a.SubjectGroupID IN ($subjectGroupIdCsv)
									AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail <> '')
								ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

			}else{
				# staff
				$subject_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", b.ImapUserEmail 
							FROM SUBJECT_TERM_CLASS_USER AS a 
							INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
							WHERE b.RecordStatus = 1 
								AND a.SubjectGroupID IN ($subjectGroupIdCsv)
								 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail <> '') 
							 ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

		}else if($TargetUserType == 3){
			# parent
			$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", b.ImapUserEmail 
							FROM SUBJECT_TERM_CLASS_USER AS a 
							INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
							WHERE b.RecordStatus = 1 
								AND a.SubjectGroupID IN ($subjectGroupIdCsv)
							 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail <> '')
							 ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

		}else{
			# identity unknown
			$subject_sql = "";
		}
		
		$result = $li->returnArray($subject_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_student = $js_all_same_subject_student."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_subject_student = $js_all_same_subject_student."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_student = "";
		}
		
		return $js_all_same_subject_student;
	}
	
	function GetAllSameSubjectGroupStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID 
										FROM SUBJECT_TERM_CLASS_TEACHER AS a 
										INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
										WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
				$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
				$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
				$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", b.ImapUserEmail 
										FROM SUBJECT_TERM_CLASS_USER AS a 
										INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
										WHERE b.RecordStatus = 1 
											AND a.SubjectGroupID IN ($subjectGroupIdCsv)
											 AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail <> '') 
										 ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

			}else{
				# staff
				$subject_group_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID 
								FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
								WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", b.ImapUserEmail 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE b.RecordStatus = 1 
										AND a.SubjectGroupID IN ($subjectGroupIdCsv)
										AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL)  AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail <> '')
									ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";

		}else if($TargetUserType == 3){
			# parent
			$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID 
								FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
								INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
								WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", b.ImapUserEmail 
									FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE b.RecordStatus = 1 
										AND a.SubjectGroupID IN ($subjectGroupIdCsv)
										AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) AND (b.IMapUserEmail IS NOT NULL AND b.IMapUserEmail <> '') 
									 ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";					 

		}else{
			# identity unknown
			$subject_group_sql = "";
		}
		
		$result = $li->returnArray($subject_group_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_group_student = $js_all_same_subject_group_student."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_subject_group_student = $js_all_same_subject_group_student."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_group_student = "";
		}
		
		return $js_all_same_subject_group_student;
	}
	
	function GetAllParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		global $sys_custom,$imail_forwarded_email_splitter;
		
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		$name_field = getParentNameWithStudentInfo("a.","c.");
		
		$studentIdSql = "SELECT DISTINCT StudentID 
							FROM INTRANET_USER AS a 
							INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
							WHERE a.RecordType = 2 AND a.RecordStatus = 1";
		$studentIds = $li->returnVector($studentIdSql);
		$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
		
		if ($sys_custom['iMailSendToParent'] == true)
		{
//			$all_sql = "select 12 , '(S1L-14) luckychan\'s Parent (cabug2)','mfkhoe@broadlearning.com**cameronlau@broadlearning.com'";

			$all_sql = $this->GetSQL_ParentEmail4JS($name_field,$studentIdCsv);
						
		}
		else
		{
			$all_sql = "(SELECT b.ParentID, $name_field , c.ImapUserEmail 
						FROM INTRANET_USER AS a 
						INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
						INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
						WHERE c.RecordStatus = 1 
							AND b.StudentID IN ($studentIdCsv)
						 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
						ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$result = $li->returnArray($all_sql,3);
//print $all_sql;
//error_log("all_sql [".$all_sql."]   <----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/aaa.txt");
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_parent = $js_all_parent."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_parent = $js_all_parent."$final_email"."; ";
			}
		}else{
			$js_all_parent = "";
		}
		
		return $js_all_parent;
	}
	
	function GetAllSameFormParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		global $sys_custom;
		
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$yearIdSql = "SELECT DISTINCT a.YearID 
							FROM YEAR_CLASS AS a 
							INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) 
							WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
							
				$yearIds = $li->returnVector($yearIdSql);
				$yearIdCsv = count($yearIds)>0? implode(",",$yearIds) : "-1";
				if($yearIdCsv != "-1"){
					$yearClassIdSql = "SELECT a.YearClassID 
										FROM YEAR_CLASS AS a 
										WHERE a.YearID IN ($yearIdCsv)
											AND a.AcademicYearID = $CurrentAcademicYearID";
					$yearClassIds = $li->returnVector($yearClassIdSql);
					$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds): "-1";
				}else{
					$yearClassIdCsv = "-1";
				}
				if($yearClassIdCsv != "-1"){
					$studentIdSql = "SELECT a.UserID 
									FROM YEAR_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE a.YearClassID IN ($yearClassIdCsv)";
					$studentIds = $li->returnVector($studentIdSql);
					$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
				}else{
					$studentIdCsv = "-1";
				}

				if ($sys_custom['iMailSendToParent'] == true)
				{

					$form_sql = $this->GetSQL_ParentEmail4JS($name_field,$studentIdCsv,$distinct="DISTINCT");
//print $form_sql;											
				}
				else
				{					
					$form_sql = "(SELECT DISTINCT a.UserID, $name_field, c.ImapUserEmail 
								FROM INTRANET_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
								INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
								WHERE c.RecordStatus = 1 
									AND b.StudentID IN ($studentIdCsv)
									AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
								ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
				}

			}else{
				# staff
				$form_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_fieImapUserEmailrm_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
											FROM INTRANET_USER AS a 
											INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
											INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
											WHERE c.RecordStatus = 1 
												AND b.StudentID IN 
													(SELECT a.UserID 
													FROM YEAR_CLASS_USER AS a 
													INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
													WHERE a.YearClassID IN 
														(SELECT a.YearClassID 
														FROM YEAR_CLASS AS a 
														WHERE a.YearID IN 
															(SELECT DISTINCT a.YearID 
															FROM YEAR_CLASS AS a 
															INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
															WHERE b.UserID = $uid AND a.AcademicYearID = $CurrentAcademicYearID)
														 AND a.AcademicYearID = $CurrentAcademicYearID)
													)
												  AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
											ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$name_field = getParentNameWithStudentInfo("a.","c.");
			// get YearID
			$yearIdSql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a 
							INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
							INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) 
							WHERE relation.ParentID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$yearIds = $li->returnVector($yearIdSql);
			$yearIdCsv = count($yearIds)>0? implode(",",$yearIds) : "-1";
			// get YearClassID
			if($yearIdCsv != "-1"){
				$yearClassIdSql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($yearIdCsv) AND AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$yearClassIds = $li->returnVector($yearClassIdSql);
				$yearClassIdCsv = count($yearClassIds)>0? implode(",",$yearClassIds) : "-1";
			}else{
				$yearClassIdCsv = "-1";
			}
			// get StudentID
			if($yearClassIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE a.YearClassID IN ($yearClassIdCsv) ";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0? implode(",",$studentIds) : "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$form_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
						FROM INTRANET_USER AS a 
						INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
						INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
						WHERE c.RecordStatus = 1 
							AND b.StudentID IN ($studentIdCsv)
							AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
						ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			# identity unknown
			$form_sql = "";
		}
//		$form_sql = "select 12 , '(S1L-14) luckychan\'s Parent (cabug2)','mfkhoe@broadlearning.com**cameronlau@broadlearning.com'";		
		$result = $li->returnArray($form_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_form_parent = $js_all_same_form_parent."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_form_parent = $js_all_same_form_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_form_parent = "";
		}
		
		return $js_all_same_form_parent;
	}
	
	function GetAllSameClassParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		global $sys_custom;
		
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$yearClassIdSql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a 
									INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) 
									WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$yearClassIds = $li->returnVector($yearClassIdSql);
				$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds) : "-1";
				if($yearClassIdCsv != "-1"){
					$studentIdSql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE a.YearClassID IN ($yearClassIdCsv)";
					$studentIds = $li->returnVector($studentIdSql);
					$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
				}else{
					$studentIdCsv = "-1";
				}
				
				if ($sys_custom['iMailSendToParent'] == true)
				{


					$class_sql = $this->GetSQL_ParentEmail4JS($name_field,$studentIdCsv,$distinct="DISTINCT");
//print $class_sql;											
					
				}
				else
				{				
					$class_sql = "(SELECT DISTINCT a.UserID, $name_field, c.ImapUserEmail 
								FROM INTRANET_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
								INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
								WHERE c.RecordStatus = 1 
									AND b.StudentID IN ($studentIdCsv)
									AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
								ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
				}				

			}else{
				# staff
				$class_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$yearClassIdSql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a 
								INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
								WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds): "-1";
			if($yearClassIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE a.YearClassID IN ($yearClassIdCsv)";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds): "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$class_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
							INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
							WHERE c.RecordStatus = 1 AND b.StudentID IN ($studentIdCsv) 
								AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			/*
			$class_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
							INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
							WHERE c.RecordStatus = 1 AND b.StudentID IN 
								(SELECT a.UserID FROM YEAR_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE a.YearClassID IN 
									(SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a 
									INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
									WHERE b.UserID = $uid AND a.AcademicYearID = $CurrentAcademicYearID)
								) 
								AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)"; */
		}else if($TargetUserType == 3){
			# parent
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$yearClassIdSql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a 
								INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) 
								INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) 
								WHERE relation.ParentID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$yearClassIds = $li->returnVector($yearClassIdSql);
			$yearClassIdCsv = count($yearClassIds)>0 ? implode(",",$yearClassIds): "-1";
			if($yearClassIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE a.YearClassID IN ($yearClassIdCsv)";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds): "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$class_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
							INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
							WHERE c.RecordStatus = 1 
								AND b.StudentID IN ($studentIdCsv)
								 AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			# identity unknown
			$class_sql = "";
		}
//		$class_sql = "select 12 , '(S1L-14) luckychan\'s Parent (cabug2)','mfkhoe@broadlearning.com**cameronlau@broadlearning.com'";				
		$result = $li->returnArray($class_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_class_parent = $js_all_same_class_parent."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_class_parent = $js_all_same_class_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_class_parent = "";
		}
		
		return $js_all_same_class_parent;
	}
	
	function GetAllSameSubjectParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		global $sys_custom;
		
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'"; 
				$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
				$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
				if($subjectGroupIdCsv != "-1"){
					$studentIdSql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a 
										INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
										WHERE a.SubjectGroupID IN ($subjectGroupIdCsv)";
					$studentIds = $li->returnVector($studentIdSql);
					$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
				}else{
					$studentIdCsv = "-1";
				}
				
				if ($sys_custom['iMailSendToParent'] == true)
				{
					$subject_sql = $this->GetSQL_ParentEmail4JS($name_field,$studentIdCsv,$distinct="DISTINCT");
				}
				else
				{					
					$subject_sql = "(SELECT DISTINCT a.UserID, $name_field, c.ImapUserEmail 
									FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
									INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
									WHERE c.RecordStatus = 1 
										AND b.StudentID IN ($studentIdCsv)
										AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') 
									ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
				}

			}else{
				# staff
				$subject_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'"; 
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			if($subjectGroupIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE a.SubjectGroupID IN ($subjectGroupIdCsv)";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
							FROM INTRANET_USER AS a 
							INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
							INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
							WHERE c.RecordStatus = 1 
								AND b.StudentID IN ($studentIdCsv)
							  	AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '')
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else if($TargetUserType == 3){
			# parnet
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$subjectGroupIdSql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
									WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'"; 
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			if($subjectGroupIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE a.SubjectGroupID IN ($subjectGroupIdCsv)";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail FROM INTRANET_USER AS a 
							INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
							INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
							WHERE c.RecordStatus = 1 
								AND b.StudentID IN ($studentIdCsv)
								AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail<> '') 
							ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			# identity unknown
			$subject_sql = "";
		}
		
		$result = $li->returnArray($subject_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_parent = $js_all_same_subject_parent."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_subject_parent = $js_all_same_subject_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_parent = "";
		}
		
		return $js_all_same_subject_parent;
	}
	
	function GetAllSameSubjectGroupParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		global $sys_custom;
		
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'"; 
				$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
				$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
				if($subjectGroupIdCsv != "-1"){
					$studentIdSql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
									WHERE a.SubjectGroupID IN ($subjectGroupIdCsv)";
					$studentIds = $li->returnVector($studentIdSql);
					$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
				}else{
					$studentIdCsv = "-1";
				}

				if ($sys_custom['iMailSendToParent'] == true)
				{
					$subject_group_sql = $this->GetSQL_ParentEmail4JS($name_field,$studentIdCsv,$distinct="DISTINCT");
				}
				else
				{					
					$subject_group_sql = "(SELECT DISTINCT a.UserID, $name_field, c.ImapUserEmail FROM INTRANET_USER AS a 
											INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
											INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
											WHERE c.RecordStatus = 1 
												AND b.StudentID IN ($studentIdCsv)
												AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
											ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
				}

			}else{
				# staff
				$subject_group_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
								WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'"; 
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			if($subjectGroupIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE a.SubjectGroupID IN ($subjectGroupIdCsv)";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail 
								FROM INTRANET_USER AS a 
								INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
								INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
								WHERE c.RecordStatus = 1 
									AND b.StudentID IN ($studentIdCsv) 
									AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
								 ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else if($TargetUserType == 3){
			# parent
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$subjectGroupIdSql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a 
									INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) 
									INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
									WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID'"; 
			$subjectGroupIds = $li->returnVector($subjectGroupIdSql);
			$subjectGroupIdCsv = count($subjectGroupIds)>0 ? implode(",",$subjectGroupIds) : "-1";
			if($subjectGroupIdCsv != "-1"){
				$studentIdSql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a 
								INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
								WHERE a.SubjectGroupID IN ($subjectGroupIdCsv)";
				$studentIds = $li->returnVector($studentIdSql);
				$studentIdCsv = count($studentIds)>0 ? implode(",",$studentIds) : "-1";
			}else{
				$studentIdCsv = "-1";
			}
			$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field, c.ImapUserEmail FROM INTRANET_USER AS a 
									INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
									INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) 
									WHERE c.RecordStatus = 1 
										AND b.StudentID IN ($studentIdCsv)
										AND (c.IMapUserEmail IS NOT NULL AND c.IMapUserEmail <> '') 
									ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		}else{
			# identity unknown
			$subject_group_sql = "";
		}
		
		$result = $li->returnArray($subject_group_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_group_parent = $js_all_same_subject_group_parent."$uemail"."; ";\
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_same_subject_group_parent = $js_all_same_subject_group_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_group_parent = "";
		}
		
		return $js_all_same_subject_group_parent;
	}
	
	function GetAllGroupEmail($TargetGroupID,$ToTeacher,$ToStaff,$ToStudent,$ToParent,$ToAlumni=0)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		
		if($ToTeacher == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
		}
		if($ToStaff == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
		}
		if($ToStudent == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 2) ";
		}
		if($ToParent == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 3) ";
		}
		if($ToAlumni == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 4) ";
		}
		
		if($cond != "")
			$final_cond = " AND ( $cond ) ";

		
		$sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", a.ImapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID) $final_cond  AND (a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '') ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
		
		$result = $li->returnArray($sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_group_email = $js_all_group_email."$uemail"."; ";
				$final_email = '"'.$this->replaceQuotesForDisplayName($uname).'"'." <".$uemail.">";
				$js_all_group_email = $js_all_group_email."$final_email"."; ";
			}
		}else{
			$js_all_group_email = "";
		}
		
		return $js_all_group_email;
	}
	// get email list for internal user email (internal_recipient.php) modified from libwebmail.php (end)
	
	function Format_Email_Display_Name($DisplayName)
	{
		if(trim($DisplayName)=='')
			return $DisplayName;
		
		$DisplayName = trim(stripslashes($DisplayName));
		
//		if(stristr($DisplayName,"@")||stristr($DisplayName,";")||stristr($DisplayName,","))
//		{
			$first_char = $DisplayName[0];
			$last_char = $DisplayName[strlen($DisplayName)-1];
			if($first_char == '"' && $last_char == '"' ) // no need to add double quote
			{
				$DisplayName = '"'.$this->replaceQuotesForDisplayName(substr($DisplayName,1,strlen($DisplayName)-2)).'"';
			}else if($first_char == "'"  && $last_char == "'" ){ // need to replace single quote to double , to avoid checking error
				//$DisplayName[0] = $DisplayName[strlen($DisplayName)-1] = '"';
				$DisplayName = '"'.$this->replaceQuotesForDisplayName(substr($DisplayName,1,strlen($DisplayName)-2)).'"';
			}else{
				$DisplayName = '"'.$this->replaceQuotesForDisplayName($DisplayName).'"';
			}
			
//		}
		
		return $DisplayName;
	}
	
	function IsToday($date)
	{
		$start = strtotime(Date("Y-m-d"));
		$end = $start + 60*60*24;
		
		return (strtotime($date)>=$start && strtotime($date)<$end);
	}
	
	function BadWordFilter($Text)
	{
		global $intranet_root;
		
//		$lf = new libfilesystem();
//		$base_dir = "$intranet_root/file/templates/";
//		if (!is_dir($base_dir))
//		{
//		     $lf->folder_new($base_dir);
//		}
//		
//		$target_file = "$base_dir"."mail_badwords.txt";
//		$data = get_file_content($target_file);
		$data = $this->bad_word_list;
		
		$phrase = explode("\n",$data);
		for($i=0; $i<count($phrase); $i++)
		{
			if(trim($phrase[$i])!=''){
				//$bad_word_preg[] = "/".trim($phrase[$i])."/i";
				$bad_word_preg[] = trim($phrase[$i]);
			}		
		}
		
		return str_replace((array)$bad_word_preg,"***",$Text);
	}
	
	function setIMapUserEmailQuota($ParUserID,$Quota)
	{
		
		$libdb = new libdb();
			
		$sql = "UPDATE 
					INTRANET_CAMPUSMAIL_USERQUOTA
				SET
					GammaQuota = '$Quota'
				WHERE
					UserID = '$ParUserID'";
		
		$result =  $libdb->db_db_query($sql);
			
		if(!$result || $libdb->db_affected_rows()!=1)
		{
			$sql = "
				INSERT INTO 
					INTRANET_CAMPUSMAIL_USERQUOTA 
					(UserID, GammaQuota) 
					VALUES 
					('$ParUserID', '$Quota')";
					
			$result =  $libdb->db_db_query($sql);
		}

		return $result;
			
	}
	
	function getUserImailGammaQuota($uid)
	{
		$libdb = new libdb();
		
		$sql = "
			SELECT
				GammaQuota
			FROM 
				INTRANET_CAMPUSMAIL_USERQUOTA
			WHERE 
				UserID = '$uid'
		";
		
		$result = $libdb->returnVector($sql);

		return $result[0];
	}
	
	function AccessInternetMail($ParUserID='')
	{
		global $UserID, $intranet_root;
		
		$ParUserID = $ParUserID!=''?$ParUserID:$UserID;
//		if(file_exists("$intranet_root/file/gamma_mail/internet_mail_access.txt"))
//		{
//			$internet_mail_access = trim(get_file_content("$intranet_root/file/gamma_mail/internet_mail_access.txt"));
//			if($internet_mail_access==0)
//				return false;
//		}
		if(!$this->internet_mail_access)
			return false;
		
		//$gamma_ban_list = get_file_content("$intranet_root/file/gamma_mail_banlist.txt");
		$gamma_ban_list = $this->ban_ext_mail_list;
		$gamma_ban_arr = explode("\r\n",$gamma_ban_list);
		
//		$mail_user_login = $_SESSION['SSV_LOGIN_EMAIL'];
        $mail_user_login = $this->CurUserAddress;
		if(stristr($mail_user_login,"@"))
			$mail_user_login = substr($mail_user_login, 0, strpos($mail_user_login, "@"));
		if(in_array($mail_user_login,$gamma_ban_arr))
			return false;
		
		$libdb = new libdb();
		
		$sql = "
			SELECT
				u.UserLogin, u.RecordType, a.ACL  
			FROM 
				INTRANET_USER as u 
				LEFT JOIN INTRANET_SYSTEM_ACCESS as a ON a.UserID = u.UserID AND a.ACL IN (1,3) 
			WHERE 
				u.UserID = '$ParUserID'
		";
		
		$result = $libdb->returnArray($sql);
		
		if($result[0]['UserLogin'] != $mail_user_login)// using shared mail box
			return true;
		
		if($this->internet_mail_access && $this->internal_mail_only[$result[0]['RecordType']-1]==1)
			return false;
		
		if(!($result[0]['ACL']==1 || $result[0]['ACL']==3))
			return false;
		
		return !in_array($result[0]['UserLogin'],$gamma_ban_arr);
		//return !stristr($gamma_ban_list,$result[0]['UserLogin']);
	}
	
	function CheckIdentityPermission($ParUserType='')
	{
		global 	$UserType;
		
		$ParUserType = $ParUserType!=''?$ParUserType:$UserType;
		
		$AccessQuota = $this->getEmailDefaultQuota($ParUserType);
		return $AccessQuota[0]==1;
		
	}	
	
	function getEmailDefaultQuota($ParUserType='')
	{
		//global $intranet_root;
		
		$access = $this->access_right;
		$quota = $this->default_quota;
		
		$EmailQuota = array();

	    $EmailQuota[TYPE_TEACHER] = array($access[0],$quota[0]);
	    $EmailQuota[TYPE_STUDENT] = array($access[1],$quota[1]);
	    //$EmailQuota[TYPE_ALUMNI] = array(0,0); // tmp assign, not use in this moment
	    $EmailQuota[TYPE_PARENT] = array($access[2],$quota[2]);
		$EmailQuota[TYPE_ALUMNI] = array($access[3],$quota[3]);
		if($ParUserType=='')
			return $EmailQuota;
		else
			return $EmailQuota[$ParUserType];
	}
	
	function InsertMailReceiptByMailAddresses($ParMailID, $ParMailAddresses)
	{
		if(is_array($ParMailAddresses) && sizeof($ParMailAddresses)>0)
		{
			$libdb = new libdb();
			$mail_addresses = array();
			foreach($ParMailAddresses as $key => $mail_address)
				$mail_addresses[] = "'".$mail_address."'";
			
			$sql = "SELECT UserID 
					FROM INTRANET_USER 
					WHERE ImapUserEmail IN (".implode(",",$mail_addresses).")";
			$user_id_vec = $libdb->returnVector($sql);
			
			if(sizeof($user_id_vec)>0)
			{
				$result = true;
				$chunks = array_chunk($user_id_vec,50);
				$chunk_size = count($chunks);
				for($k=0;$k<$chunk_size;$k++)
				{
					$insert_values = array();
					$tmp_user_id_vec = $chunks[$k];
					for($i=0;$i<sizeof($tmp_user_id_vec);$i++)
						$insert_values[] = "('$ParMailID','".$tmp_user_id_vec[$i]."','0',NULL,NOW(),NOW())";
					
					$sql = "INSERT INTO INTRANET_IMAIL_RECEIPT_STATUS
								(MailID,UserID,Status,TrashDate,DateInput,DateModified)
							VALUES
								".implode(",",$insert_values);
					$result = $libdb->db_db_query($sql) && $result;
				}
				return $result;
			}
		}
		return false;
	}
	
	function UpdateMailReceipt($ParMailID, $ParUserID)
	{
		$libdb = new libdb();
		
		$sql = "SELECT RecordID, Status 
				FROM INTRANET_IMAIL_RECEIPT_STATUS 
				WHERE MailID = '$ParMailID' AND UserID = '$ParUserID' ";
		$record = $libdb->returnArray($sql);
		if(sizeof($record)>0 && $record[0]['Status'] != 1)
		{
			$sql = "UPDATE INTRANET_IMAIL_RECEIPT_STATUS 
					SET Status = '1', DateModified = NOW()
					WHERE MailID = '$ParMailID' AND UserID = '$ParUserID' ";
			
			return $libdb->db_db_query($sql);
		}
		return false;
	}
	
	function GetMailReceipts($ParMailID)
	{
		$libdb = new libdb();
		
		$NameField = getNameFieldWithClassNumberByLang("u.");
		
		$sql = "SELECT 
					$NameField as UserName,
					u.ImapUserEmail as MailAddress,
					r.Status,
					DATE_FORMAT(r.DateModified,'%Y-%m-%d %H:%i:%s') as DateModified 
				FROM 
					INTRANET_USER as u 
					INNER JOIN INTRANET_IMAIL_RECEIPT_STATUS as r 
						ON r.UserID = u.UserID AND r.MailID = '$ParMailID'  
				WHERE 
					r.MailID = '$ParMailID' 
				ORDER BY r.DateModified DESC,u.ClassName,u.ClassNumber+0,UserName";
		
		return $libdb->returnArray($sql);
	}
	
	function DeleteMailReceipts($ParMailID,$ParUserID='')
	{
		$libdb = new libdb();
		
		$sql = "DELETE FROM INTRANET_IMAIL_RECEIPT_STATUS WHERE MailID = '$ParMailID' ";
		if(trim($ParUserID) != '') $sql .= " AND UserID = '$ParUserID' ";
		
		return $libdb->db_db_query($sql);
	}
	
	function SetMailReceiptTrashMark($ParFolderName,$ParUID,$ParAction="",$ParType="",$ParUserID="")
	{
		//$eClassMailID = trim($this->Get_Header_Flag($this->Get_MessageID($ParUID),'x-eclass-mailid'));
		$eClassMailID = trim($this->Get_Header_Flag($ParUID,'x-eclass-mailid',FT_UID));
		
		if($eClassMailID != '')
		{
			$libdb = new libdb();
			if($ParAction=="DELETE")
				$trash_date = "NOW()";
			else 
				$trash_date = "NULL";
			
			$record_type = (trim($ParType)=='')?'NULL':$ParType;
			
			$sql = "UPDATE INTRANET_IMAIL_RECEIPT_STATUS 
					SET TrashDate = $trash_date, RecordType = '$record_type'   
					WHERE MailID = '$eClassMailID' ";
					
			if(trim($ParUserID)!='') $sql .= " AND UserID = '".$ParUserID."' ";
			return $result = $libdb->db_db_query($sql);
		}
		return false;
	}
	
	function DisplayMailRecipientStatus($ParMailID)
	{
		global $image_path,$LAYOUT_SKIN, $Lang;
		
		$receipts = $this->GetMailReceipts($ParMailID);
		if(sizeof($receipts)==0) return '';
		
		$total_readed = 0; $total_unreaded = 0; $r = '';
		for($i=0;$i<sizeof($receipts);$i++)
		{
			list($name,$mail_address,$status,$date_modified) = $receipts[$i];
			if ($i%2==0)
			{
       			$r .= '<tr class="tabletext">';
			} else {
				$r .= '<tr class="tablegreenrow2 tabletext">';
			}
			$r .= '<td nowrap="nowrap">'.$name.' &lt;'.$mail_address.'&gt;</td>';
			if(trim($status)==1){
				$total_readed++;
				$r .= '<td align="left">&nbsp;</td>';
				$r .= '<td align="left">'.$date_modified.'&nbsp;<img border="0" align="absmiddle" title="'.$Lang['Gamma']['UnseenStatus']['SEEN'].'" alt="'.$Lang['Gamma']['UnseenStatus']['SEEN'].'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_open_mail.gif"></td>';
			}
			else{
				$total_unreaded++;
				$r .= '<td align="left"><img border="0" align="absmiddle" title="'.$Lang['Gamma']['UnseenStatus']['UNSEEN'].'" alt="'.$Lang['Gamma']['UnseenStatus']['UNSEEN'].'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/iMail/icon_unread_mail.gif"></td>';
				$r .= '<td align="left">&nbsp;</td>';
			}
			$r .= '</tr>';
		}
		
		$x = '<tr>
				<td align="center">
					<table cellspacing="0" cellpadding="3" border="0" width="100%">
						<tbody>
							<tr class="tablegreenbottom">
								<td width="50%"><span class="tabletext">'.$Lang['Gamma']['Recipient'].'</span></td>
								<td align="right" width="25%" class="tabletext" style="text-align:left">'.$Lang['Gamma']['UnseenStatus']['UNSEEN'].'('.$total_unreaded.')</td>
								<td align="right" width="25%" class="tabletext" style="text-align:left">'.$Lang['Gamma']['UnseenStatus']['SEEN'].'('.$total_readed.')</td>
							</tr>';				
		$x .= $r;
		$x .= '			</tbody>
					</table>
				</td>
			</tr>';
		
		return $x;
	}
	
	function Get_Gmail_Mode_Mails_Content($FolderArray, $UidArray)
	{
		$x = ''; 
		$delimiter = '|=Mail_Content_Delimiter=|';
		for($i=0;$i<sizeof($UidArray);$i++)
		{
			if($i>0) $x .= $delimiter;
			$x .= $this->Get_Mail_Content_UI(trim(urldecode(stripslashes($FolderArray[$i]))),$UidArray[$i]);
		}
		return $x;
	}
	
	function Extract_Header_Email($EmailList)
	{
		for ($j=0; $j<sizeof($EmailList) ; $j++) {
			if (!isset($EmailList[$j]->personal)) {
				if($EmailList[$j]->mailbox && $EmailList[$j]->host)
					$DisplayName = $EmailList[$j]->mailbox."@".$EmailList[$j]->host;
			} else {
//					$DisplayObject = imap_mime_header_decode(imap_utf8($fromList[$j]->personal));
//					$DisplayName = $DisplayObject[0]->text;
				$DisplayName = $this->MIME_Decode($EmailList[$j]->personal);
			}
				
			$Address = $EmailList[$j]->mailbox."@".$EmailList[$j]->host;
			$DisplayName;
			$ReturnArr[] = array($DisplayName,$Address);
		} 
		
		return $ReturnArr;
	}
	/*
	# Get all existing gamma mail account quota info
	# Get back format : $login,$used,$soft,$hard
	function get_quota_table()
	{
	  		global $SYS_CONFIG;
	  		$path = $this->mail_api_url['GetQuotaTable'];
	  		if ($this->actype != "")
	    	{
	    		$path .= "?actype=".$this->actype;
	    	}
	    	$response = $this->getWebPage($path);
	        if ($response[0]=="0") return false;
	        else
	        {
	            $temp = explode("###",$response);
	            for ($i=0; $i<sizeof($temp); $i++)
	            {
	            	$temp_array = explode(",",$temp[$i]);
	                //$result[] = $temp_array;
	                $at_pos = stripos($temp_array[0],"@",0);
	                if($at_pos>=0){
	                	$user_login = substr($temp_array[0],0,$at_pos)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	                }else{
	                	$user_login = $temp_array[0]."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	                }
	                $quota_total = $temp_array[2];
	                $quota_used = $temp_array[1];
	                if($this->actype=="postfix"){
	                	$quota_total = round($quota_total/1024/1024,2);
	                	$quota_used = round($quota_used/1024/1024,2);
	                }
	                $temp2=array('UserLogin'=>$user_login,
								'AccountExist'=>true,
								'QuotaUsed'=>$quota_used,
								'QuotaTotal'=>$quota_total);
					$result[]=$temp2;
					$result[$user_login]=$temp2;
	            }
	            return $result;
	        }
	}
	
	# Very slow, temporary use
	function Get_Quota_Info_List($ParUserLogins=array())
	{
		$result = array();
		for($i=0;$i<sizeof($ParUserLogins);$i++){
			$user_login = $ParUserLogins[$i];
			$user_exist = $this->is_user_exist($user_login);
			$quota_used = 0;
			$quota_total = 0;
			if($user_exist){
				$quota_total = $this->getTotalQuota($user_login,"iMail");
				$quota_used = $this->getUsedQuota($user_login,"iMail");
				if($this->actype=="postfix") $quota_used = round($quota_used/1024/1024,2);
			}
			$temp=array('UserLogin'=>$user_login,
						'AccountExist'=>$user_exist,
						'QuotaUsed'=>$quota_used,
						'QuotaTotal'=>$quota_total);
			$result[]=$temp;
			$result[$user_login]=$temp;
		}
		return $result;
	}
	*/
	function Get_Quota_Info_List($ParUserLogins=array())
	{
		$result = array();
		$res_text = $this->Get_Single_Quota($ParUserLogins);
		if($res_text != false){
			$UserLogins = explode("<BR>",$res_text);
			for($i=0;$i<sizeof($UserLogins);$i++){
				$info = explode(",",$UserLogins[$i]);// [userlogin, quota total(MB), quota used(MB)]
				if(sizeof($info)!=3) continue;
				$user_exist = true;
				$user_login = strtolower($info[0]);
				$quota_used = $info[2];
				$quota_total = $info[1];
				
				$temp=array('UserLogin'=>$user_login,
							'AccountExist'=>$user_exist,
							'QuotaUsed'=>$quota_used,
							'QuotaTotal'=>$quota_total);
				$result[]=$temp;
				$result[$user_login]=$temp;
			}
		}
		return $result;
	}
	
	function Get_Mail_Print_Version($Folder, $Uid, $PartNumber='')
	{
		global $Lang, $LAYOUT_SKIN, $image_path;
		
		//$display_gmailcontent = 'style="display:block"';
		$thisContent = $this->getMailRecord($Folder, $Uid, $PartNumber);
		$thisEmailContent = $thisContent[0];
		
		$thisUid =  $thisEmailContent["uid"];
		$thisFolder =  $thisEmailContent["Folder"]?$thisEmailContent["Folder"]:$Folder;
		
		# important flag
		$MailPriority = $this->GetMailPriority($thisUid,$Folder,$isUID=1);
		
		// prepare data for top mail description
		// sender name
		$fromDetail = $thisEmailContent['fromDetail'][0];
		$emailaddr = $fromDetail->mailbox."@".$fromDetail->host;
		$DisplaySender = $this->MIME_Decode($fromDetail->personal);
		$DisplaySender = $this->Format_Email_Display_Name($DisplaySender);
		$DisplayTopSender= $DisplaySender?$DisplaySender:$emailaddr;
		
		$x = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
				<tbody>';
		
		$x.='<tr>'; 
				$x.='<td colspan="2">';
					//------ mail header start -----------
					$x.='<table width="100%" cellspacing="0" cellpadding="5"  border="0" >';
						$x.='<tr>'; 
							$x.='<td colspan="2">'; 
							$x.='<table width="100%" cellspacing="0" cellpadding="5" border="0" >'; 
							$x.='<tbody>';
		
							# date
							$x.='<tr>'; 
					            $x.='<td width="120" align="left" class="tabletext"><span id="date">'.$Lang['Gamma']['Date'].'</span></td>'; 
					            $x.='<td width="4" align="left" class="tabletext"><span > :</span></td>'; 
					            $x.='<td class="tabletext">'.$thisEmailContent['dateReceive'].'</td>'; 
							$x.='</tr>'; 
		
							# sender
							$sender = $DisplaySender.' &lt;'.$emailaddr.'&gt;';
					      	$x.='<tr>';
					        	$x.='<td align="left" class="tabletext"><span id="sender" >'.$Lang['Gamma']['Sender'].'</span></td>';			
					        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
					        	$x.='<td class="tabletext"><span class="tabletext">'.$sender.'</span></td>';			
					      	$x.='</tr>';
					      	
					      	# to
							if(!empty($thisEmailContent['toDetail']))
					      	{
					      		$toAccountAry = $toAccountShowArr = $toAccountHideArr = array();
						      	$toDetail = $thisEmailContent['toDetail'];
						      	for($i=0;$i<sizeof($toDetail);$i++)
						      	{
									$emailaddr = $toDetail[$i]->mailbox."@".$toDetail[$i]->host;
									$DisplayName = $this->MIME_Decode($toDetail[$i]->personal);
									$toAccountAry[] = $DisplayName.' &lt;'.$emailaddr.'&gt;';
						      	}
						      	/*if(count($toAccountAry)>8)
						      	{
						      		for($i=0;$i<count($toAccountAry);$i++)
						      		{
						      			if($i<4)
						      				$toAccountShowArr[] =  $toAccountAry[$i];
						      			else
						      				$toAccountHideArr[] = $toAccountAry[$i];
						      		}
						      		$toAccountShow = implode(", ",$toAccountShowArr).'<span id="ShowTo'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideTo'.$thisUid.'\').show(); $(\'#ShowTo'.$thisUid.'\').hide();">[Show Details]</a></span>';
						      		$toAccountHide = '<span id="HideTo'.$thisUid.'" style="display:none;">,'.implode(", ",$toAccountHideArr).'</span>';
						      		$toAccount = $toAccountShow.$toAccountHide;
						      		
						      	}else */ 
						      	if(count($toAccountAry)>1)
						      		$toAccount = implode(", ",$toAccountAry);
						      	else
						      		$toAccount = $toAccountAry[0];
						      
						      	
						      	$x.='<tr>';
						        	$x.='<td align="left" class="tabletext"><span id="sender" >'.$Lang['Gamma']['To'].'</span></td>';			
						        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
						        	$x.='<td class="tabletext"><span class="tabletext">'.$toAccount.'</span></td>';			
						      	$x.='</tr>';
					      	}
					      	
					      	# cc
					      	if(!empty($thisEmailContent['ccDetail']))
					      	{
					      		$ccAccountAry = $ccAccountShowArr = $ccAccountHideArr = array();
						      	$ccDetail = $thisEmailContent['ccDetail'];
						      	for($i=0;$i<sizeof($ccDetail);$i++)
						      	{
									$emailaddr = $ccDetail[$i]->mailbox."@".$ccDetail[$i]->host;
									$DisplayName =$this->MIME_Decode($ccDetail[$i]->personal);
									$ccAccountAry[] = $DisplayName.' &lt;'.$emailaddr.'&gt;';
						      	}
								/*
						      	if(count($ccAccountAry)>8)
						      	{
						      		for($i=0;$i<count($ccAccountAry);$i++)
						      		{
						      			if($i<5)
						      				$ccAccountShowArr[] =  $ccAccountAry[$i];
						      			else
						      				$ccAccountHideArr[] = $ccAccountAry[$i];
						      		}
						      		$ccAccountShow = implode(", ",$ccAccountShowArr).'<span id="ShowCc'.$thisUid.'">...<a class="tabletool" href="javascript:void(0); " onclick="$(\'#HideCc'.$thisUid.'\').show(); $(\'#ShowCc'.$thisUid.'\').hide();">[Show Details]</a></span>';
						      		$ccAccountHide = '<span id="HideCc'.$thisUid.'" style="display:none;">,'.implode(", ",$ccAccountHideArr).'</span>';
						      		$ccAccount = $ccAccountShow.$ccAccountHide;
						      		
						      	}else */
						      	if(count($ccAccountAry)>1)
						      		$ccAccount = implode(", ",$ccAccountAry);
						      	else
						      		$ccAccount = $ccAccountAry[0];
						      	$x.='<tr>';
						        	$x.='<td align="left" class="tabletext"><span id="sender">'.$Lang['Gamma']['cc'].'</span></td>';			
						        	$x.='<td align="left" class="tabletext"><span >:</span></td>';	
						        	$x.='<td class="tabletext"><span class="tabletext">'.$ccAccount.'</span></td>';			
						      	$x.='</tr>';
					      	}
		  	   				//$x.='</tr>';
		        	  	    #Subject
		  	        	  	$Subject = $thisEmailContent['subject']?$thisEmailContent['subject']:"No Subject";
							$Subject = $this->BadWordFilter($Subject);
							
			              	$x.='<tr>';			
			                	$x.='<td align="left" class="tabletext"><span id="subject" >'.$Lang['Gamma']['Subject'].'</span></td>';			
			                	$x.='<td align="left" class="tabletext"><span >:</span></td>';			
			                	$x.='<td class="tabletext"><font size="+1">'.htmlspecialchars($Subject).'</font></td>';			
			              	$x.='</tr>';			
			              	
			              	#Attachment
			              	$file='';
		  	        	  	$Attachment = $thisEmailContent["attach_parts"];
		  	        	  	
							if (!(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
								// short list	
								for ($i=0; $i< sizeof($Attachment); $i++) {	
									$file .= '<img hspace="2" vspace="2" border="0" align="absmiddle" src="/images/file.gif">';
									$file .= $Attachment[$i]['FileName'];
									$file .= ' ('.$Attachment[$i]['FileSize'].'Kb)';
									$file .= "<br> ";
								}
								
								$x .= '	<tr>';
			                    	$x .= '<td align="left" class="tabletext" valign=top><span id="attachment" >'.$Lang["Gamma"]["Attachment"].'</span></td>';
			                    	$x .= '<td align="left" class="tabletext" valign=top><span >:</span></td>';
			                    	$x .= '<td class="tabletext"><span class="tabletext">'.$file;
									$x .= '</span></td>';
			                  	$x .= '</tr>';
							}
							
						$x.='</tbody></table>'; 			
		                $x.='</td>'; 			
		            	$x.='</tr>'; 											
						//------ mail header end -----------
						
						//------ mail message start ------------
						# message
						
						$Message = $this->BadWordFilter($thisEmailContent['message']);
						$x.='<tr>';
							$x.='<td colspan="2">';
							$x.='<table width="100%" cellspacing="0" cellpadding="10" border="1">';
							$x.='<tbody><tr>';
								$x.='<td>';
									$x.= $Message;
								$x.='</td>';
							$x.='</tr>';
							$x.='</tbody></table>';
							$x.='</td>';
						$x.='</tr>';
						//------ mail message end ------------
					$x.='</table>';
				$x.='</td>';
			$x.='</tr>';
		
		$x .= '</tbody>
			</table>';
		
		return $x;
	}
	
	/*
	 * UID delimiter : commma ,
	 * Folders delimiter : forward slash /
	 */
	function Get_Cached_Search_Result()
	{
		global $intranet_root;
		$li = new libfilesystem();
		$location = $intranet_root."/file/gamma_mail";
		$li->folder_new($location);
		$location .= "/u".$_SESSION['UserID'];
		$li->folder_new($location);
		$file = $location."/search.txt";
		
		list($uids,$folders,$search_fields) = explode("\n",trim(get_file_content($file)));
		$SearchFieldsArray = array();
		if($search_fields != ""){
			$tmp_arr = explode("&",$search_fields);
			for($i=0;$i<count($tmp_arr);$i++){
				$key_value = explode("=",$tmp_arr[$i]);
				$SearchFieldsArray[] = array($key_value[0] => rawurldecode($key_value[1]));
			}
		}
		
		$result[] = explode(",",$uids);
		$result[] = explode("/",$folders);
		$result[] = $SearchFieldsArray;
		
		return $result;
	}
	/*
	 * $SearchedUID : array
	 * $SearchedFolder : array
	 */
	function Set_Cached_Search_Result($SearchedUID,$SearchedFolder,$SearchFields=array())
	{
		global $intranet_root;
    	$li = new libfilesystem();
		$location = $intranet_root."/file/gamma_mail";
		$li->folder_new($location);
		$location .= "/u".$_SESSION['UserID'];
		$li->folder_new($location);
		$file = $location."/search.txt";
		
		$search_fields = "";
		if(count($SearchFields)>0){
			$separator = "";
			for($i=0;$i<count($SearchFields);$i++){
				if(count($SearchFields[$i])>0){
					foreach($SearchFields[$i] as $FieldName => $FieldValue){
						$search_fields .= $separator.$FieldName."=".rawurlencode($FieldValue);
						$separator = "&";
					}
				}
			}
		}
		
		write_file_content(implode(",",(array)$SearchedUID)."\n".implode("/",$SearchedFolder)."\n".$search_fields,$file);
	}
	
	function Get_Batch_Removal_Settings($RecordID="")
	{
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT."includes/libdb.php");
		include_once("libdb.php");
		$li = new libdb();
    	
    	if($RecordID!="")
			$record_cond = " AND RecordID = '$RecordID' ";
    	$sql = "SELECT 
					RecordID, StartDate, EndDate, SubjectKeyword, Sender 
				FROM 
					MAIL_REMOVAL_RULES 
				WHERE 
					RecordStatus = 1 
					$record_cond 
				ORDER BY StartDate ";
		
		$result = $li->returnArray($sql);
		return $result;
	}
	
	function Set_Batch_Removal_Settings($DataArray)
	{
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT."includes/libdb.php");
		include_once("libdb.php");
		$li = new libdb();
		
		$RecordID = trim($DataArray['RecordID']);
		$StartDate = trim($DataArray['StartDate']);
		$EndDate = trim($DataArray['EndDate']);
		$SubjectKeyword = $li->Get_Safe_Sql_Query(trim($DataArray['SubjectKeyword']));
		$Sender = $li->Get_Safe_Sql_Query(trim($DataArray['Sender']));
		
		if($RecordID==""){
			$sql = "INSERT INTO MAIL_REMOVAL_RULES 
						(StartDate,EndDate,SubjectKeyword,Sender,RecordStatus,DateInput,DateModified)
					VALUES('$StartDate','$EndDate','$SubjectKeyword','$Sender','1',NOW(),NOW())";
		}else{
			$sql = "UPDATE MAIL_REMOVAL_RULES SET
						StartDate = '$StartDate',
						EndDate = '$EndDate',
						SubjectKeyword = '$SubjectKeyword',
						Sender = '$Sender',
						RecordStatus = '1',
						DateModified = NOW() 
					WHERE RecordID = '$RecordID' ";
		}	
		
		return $li->db_db_query($sql);
	}
	
	function Remove_Batch_Removal_Settings($RecordID)
	{
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT."includes/libdb.php");
		include_once("libdb.php");
		$li = new libdb();
		
		$sql = "UPDATE MAIL_REMOVAL_RULES SET RecordStatus = 0
				WHERE RecordID IN (".implode(",",(array)$RecordID).") ";
		return $li->db_db_query($sql);
	}
	
	function Collect_Batch_Removal_Mails($recordIdAry='')
	{
		include_once("libdb.php");
		$li = new libdb();
		/*
		$sql = "SELECT 
					r.RecordID, r.StartDate, DATE_ADD(r.EndDate,INTERVAL 1 DAY) as EndDate, r.Sender, r.SubjectKeyword, COUNT(lg.RecordID) as TotalOfLog 
				FROM MAIL_REMOVAL_RULES as r 
				LEFT JOIN MAIL_REMOVAL_LOG as lg ON r.RecordID = lg.RuleID AND lg.UserEmail = '".$this->CurUserAddress."' AND r.DateModified <= lg.DateModified
				WHERE 
					r.RecordStatus = 1 
				GROUP BY r.RecordID HAVING TotalOfLog = 0
				ORDER BY r.StartDate "; */
		$EndDate_field = $this->CacheMailToDB == true?"r.EndDate":"DATE_ADD(r.EndDate,INTERVAL 1 DAY) as EndDate";
		// Get removal rules that are edited within recent two months
		$cond = " AND UNIX_TIMESTAMP(r.DateModified) > (UNIX_TIMESTAMP()-5184000) ";
		if($recordIdAry != '' && count($recordIdAry)>0){
			$cond = " AND r.RecordID IN ('".implode("','",$recordIdAry)."') ";
		}
		$sql = "SELECT 
					r.RecordID, r.StartDate, $EndDate_field, r.Sender, r.SubjectKeyword
				FROM MAIL_REMOVAL_RULES as r 
				WHERE 
					r.RecordStatus = 1 $cond 
				ORDER BY r.StartDate ";
		$rules = $li->returnArray($sql);
		
		$rules_size = sizeof($rules);
		if($rules_size==0){
			## No need to perform search, set flag and return immediately
			$_SESSION['SSV_GAMMA_BATCH_REMOVAL_STATE'] = 1;
			return false;
		}
		
		$all_folders = $this->getAllFolderList();
		$search_folders = array();
		for($i=0;$i<sizeof($all_folders);$i++){
			if($all_folders[$i]==$this->reportSpamFolder
			   || $all_folders[$i]==$this->reportNonSpamFolder
			   || $all_folders[$i]==$this->HiddenFolder)
			 continue; // Skip all hidden folders
			$search_folders[] = $all_folders[$i];
		}
		
		$FLUSH_DB_SIZE = 50;
		$values = array();
		$success = array();
		for($k=0;$k<$rules_size;$k++)
		{
			list($rule_id,$start_date,$end_date,$sender,$subject_keyword) = $rules[$k];
			if($this->CacheMailToDB == true) // search from database
			{
				$SearchInfo = array("FromDate" => $start_date,"ToDate" => $end_date, "From" => $sender, "Subject" => $subject_keyword);
				$mail_list = $this->Search_Cached_Mail($SearchInfo);
				$cur_folder = '';
				for($j=0;$j<sizeof($mail_list);$j++){
					if($mail_list[$j]['Folder'] != $cur_folder){
						$change_folder_success = $this->Go_To_Folder($mail_list[$j]['Folder']);
						if(!$change_folder_success) continue;
						$cur_folder = $mail_list[$j]['Folder'];
					}
					$mailbox = $this->inbox;
					$MailUID = $mail_list[$j]['UID'];
					$MailFolder = $mail_list[$j]['Folder'];
					$ArrivalDate = $mail_list[$j]['MailDate'];
					$Sender = $mail_list[$j]['From'];
					$Subject = $mail_list[$j]['Subject'];
					$MessageID = $mail_list[$j]['MessageID'];
					$MoveSuccess = imap_mail_move($mailbox,$MailUID,$this->encodeFolderName($this->HiddenFolder),CP_UID);
		      		if($MoveSuccess)
					{
						// Original folder name and hidden folder UID are saved to DB for later reference
						$HiddenMailUID = $this->Get_Last_Msg_UID($this->HiddenFolder);// this will change folder
						$cur_folder = $this->HiddenFolder;
						$values[] = "('$rule_id','".$this->CurUserAddress."','$HiddenMailUID','".$li->Get_Safe_Sql_Query($MailFolder)."','$ArrivalDate','".$li->Get_Safe_Sql_Query($Sender)."','".$li->Get_Safe_Sql_Query($Subject)."','".$li->Get_Safe_Sql_Query($MessageID)."','2',NOW(),NOW())";
						if($this->CacheMailToDB == true){
							$DataArr = array();
							$DataArr['Folder'] = $this->HiddenFolder;
							$DataArr['UID'] = $HiddenMailUID;
							$DataArr['Deleted'] = 0;
			        		$this->Manage_Cached_Mail("update",$MailFolder,$MailUID,$DataArr);
						}
					}
					if($cur_folder != $mail_list[$j]['Folder']){
						if($this->Go_To_Folder($mail_list[$j]['Folder'])){
							$mailbox = $this->inbox;
							imap_expunge($mailbox);
							$cur_folder = $mail_list[$j]['Folder'];
						}
					}
					if(sizeof($values)>=$FLUSH_DB_SIZE){
						$sql = "INSERT INTO MAIL_REMOVAL_LOG (RuleID,UserEmail,MailUID,MailBox,ArrivalDate,FromEmail,Subject,MessageID,RecordStatus,DateInput,DateModified)
								VALUES ".implode(",",$values);
						$success[] = $li->db_db_query($sql);
						unset($values);
						$values = array();
					}
				}
			}else // search from mail server
			{
				for($i=0;$i<sizeof($search_folders);$i++){
					// Go to folder and search mails
					$change_folder_success = $this->Go_To_Folder($search_folders[$i]);
					if(!$change_folder_success) continue;
					$msgnos = $this->Search_Mail($search_folders[$i], $sender, "", "", "", $subject_keyword, $start_date, $end_date);
					for($j=0;$j<sizeof($msgnos);$j++){
						$change_folder_success = $this->Go_To_Folder($msgnos[$j][0]); // $msgnos[$j][0] is folder name; $msgnos[$j][1] is sequence number
						if(!$change_folder_success) continue;
						$mailbox = $this->inbox;
						$CurOverviewArray = $this->Get_Mail_In_Folder($msgnos[$j][0], $msgnos[$j][1]);// Get overview header info
						$MailUID = $CurOverviewArray['UID'];
						$MailFolder = $CurOverviewArray['Folder'];
						$ArrivalDate = date("Y-m-d H:i:s",strtotime($this->formatDatetimeString($CurOverviewArray['DateReceived'])));
						$Sender = $CurOverviewArray['From'];
						$Subject = $CurOverviewArray['Subject'];
						$MessageID = $CurOverviewArray['Message-Id']!=''?$CurOverviewArray['Message-Id']:$CurOverviewArray['X-eClass-MailID'];
						//if(imap_delete($this->inbox,$MailUID,FT_UID)){
						$MoveSuccess = false;
						$DeleteSuccess = false;
						// Move mail from searched folder to Hidden Folder
						$MoveSuccess = imap_mail_move($mailbox,$MailUID,$this->encodeFolderName($this->HiddenFolder),CP_UID);
			      		if($MoveSuccess)
						{
							// Original folder name and hidden folder UID are saved to DB for later reference
							$HiddenMailUID = $this->Get_Last_Msg_UID($this->HiddenFolder);// this will change folder
							$values[] = "('$rule_id','".$this->CurUserAddress."','$HiddenMailUID','".$li->Get_Safe_Sql_Query($MailFolder)."','$ArrivalDate','".$li->Get_Safe_Sql_Query($Sender)."','".$li->Get_Safe_Sql_Query($Subject)."','".$li->Get_Safe_Sql_Query($MessageID)."','2',NOW(),NOW())";
							if($this->CacheMailToDB == true){
								$DataArr = array();
								$DataArr['Folder'] = $this->HiddenFolder;
								$DataArr['UID'] = $HiddenMailUID;
								$DataArr['Deleted'] = 0;
				        		$this->Manage_Cached_Mail("update",$MailFolder,$MailUID,$DataArr);
							}
						}
						if(sizeof($values)>=$FLUSH_DB_SIZE){
							$sql = "INSERT INTO MAIL_REMOVAL_LOG (RuleID,UserEmail,MailUID,MailBox,ArrivalDate,FromEmail,Subject,MessageID,RecordStatus,DateInput,DateModified)
									VALUES ".implode(",",$values);
							$success[] = $li->db_db_query($sql);
							unset($values);
							$values = array();
						}
					}
					if(sizeof($msgnos)>0){
						if($this->Go_To_Folder($search_folders[$i])){
							$mailbox = $this->inbox;
							imap_expunge($mailbox);
						}
					}
				}
			}
		}
		
		if(sizeof($values)>0){
			$sql = "INSERT INTO MAIL_REMOVAL_LOG (RuleID,UserEmail,MailUID,MailBox,ArrivalDate,FromEmail,Subject,MessageID,RecordStatus,DateInput,DateModified)
					VALUES ".implode(",",$values);
			$success[] = $li->db_db_query($sql);
		}
		## Completed search and set flag
		$_SESSION['SSV_GAMMA_BATCH_REMOVAL_STATE'] = 1;
		return !in_array(false,$success);
	}
	
	function Batch_Removal_Expunge_Mails($recordIdAry='')
	{
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT."includes/libdb.php");
		include_once("libdb.php");
		$li = new libdb();
		
		if($recordIdAry != '' && count($recordIdAry)>0){
			$cond = " AND RecordID IN ('".implode("','",$recordIdAry)."') ";
		}
		$sql = "SELECT 
					RecordID, MailUID, MailBox, MessageID  
				FROM MAIL_REMOVAL_LOG 
				WHERE UserEmail = '".$this->CurUserAddress."' 
					AND (RecordStatus = '1' OR RecordStatus = '0') AND RecordType <> 1 $cond
				ORDER BY MailBox 
				";
		$data_list = $li->returnArray($sql);
		
		$num_records = sizeof($data_list);
		
		$success_array=array();
		if($num_records>0){
			$this->Go_To_Folder($this->HiddenFolder);
		}else{
			return false;
		}
		$mailbox = $this->inbox;
		for($i=0;$i<$num_records;$i++){
			list($record_id, $mail_uid, $mail_box, $message_id) = $data_list[$i];
			//$MoveSuccess = imap_mail_move($mailbox,$mail_uid,$this->encodeFolderName($mail_box),CP_UID);
			$mailbody = imap_fetchbody ($mailbox,$mail_uid,'',FT_UID);
			if(trim($mailbody)!=''){
				// Save the whole mail for removal history log
				//$sql = "UPDATE MAIL_REMOVAL_LOG SET RawMessageSource = '".$li->Get_Safe_Sql_Query($mailbody)."',DateModified=NOW() WHERE RecordID = '$record_id' ";
				$sql = "INSERT IGNORE INTO MAIL_REMOVAL_MESSAGE_LOG (MessageID,RawMessageSource,DateInput) VALUES('".$li->Get_Safe_Sql_Query($message_id)."','".$li->Get_Safe_Sql_Query($mailbody)."',NOW())";
				$SaveMailSuccess = $li->db_db_query($sql);
				if($SaveMailSuccess)
				{
					$DeleteSuccess = imap_delete($mailbox,$mail_uid,FT_UID);
					if($DeleteSuccess){
						$success_array[] = $record_id;
						if($this->CacheMailToDB == true){
							$this->Manage_Cached_Mail("remove",$mail_box,$mail_uid);
						}
					}
				}
			}
		}
		$UpdateDBSuccess = false;
		if(sizeof($success_array)>0){
			imap_expunge($mailbox);
			$sql = "UPDATE MAIL_REMOVAL_LOG SET RecordType = '1', DateModified = NOW() WHERE RecordID IN (".implode(",",$success_array).")";
			$UpdateDBSuccess = $li->db_db_query($sql);
		}
		
		return $UpdateDBSuccess;
	}
	
	function Batch_Removal_Restore_Mails($recordIdAry='')
	{
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT."includes/libdb.php");
		include_once("libdb.php");
		$li = new libdb();
		
		if($recordIdAry != '' && count($recordIdAry)>0){
			$cond = " AND RecordID IN ('".implode("','",$recordIdAry)."') ";
		}
		$sql = "SELECT 
					RecordID, MailUID, MailBox 
				FROM MAIL_REMOVAL_LOG 
				WHERE UserEmail = '".$this->CurUserAddress."' 
					AND RecordStatus = '3' AND RecordType <> '1' $cond 
				ORDER BY MailBox 
				";
		$data_list = $li->returnArray($sql);
		
		$num_records = sizeof($data_list);
		
		$success_array=array();
		if($num_records>0){
			$this->Go_To_Folder($this->HiddenFolder);
		}else{
			return false;
		}
		$mailbox = $this->inbox;
		
		$RestoreArray = array();
		for($i=0;$i<$num_records;$i++){
			list($record_id, $mail_uid, $mail_box) = $data_list[$i];
			// Move mail from Hidden Folder to original folder
			$MoveSuccess = imap_mail_move($mailbox,$mail_uid,$this->encodeFolderName($mail_box),CP_UID);
			if($MoveSuccess){
				$RestoreArray[] = $record_id;
				if($this->CacheMailToDB == true){
					$moved_uid = $this->Get_Last_Msg_UID($mail_box);
					$DataArr = array();
					$DataArr['Folder'] = $mail_box;
					$DataArr['UID'] = $moved_uid;
					$DataArr['Deleted'] = 0;
	        		$this->Manage_Cached_Mail("update",$this->HiddenFolder,$mail_uid,$DataArr);
	        		$this->Go_To_Folder($this->HiddenFolder);
				}
			}
		}
		$UpdateDBSuccess = false;
		if(sizeof($RestoreArray)>0){
			// Expunge Hidden Folder
			imap_expunge($mailbox);
			// Because mails are restored, thus log records can be deleted
			$sql = "DELETE FROM MAIL_REMOVAL_LOG WHERE RecordID IN (".implode(",",$RestoreArray).")";
			//$sql = "UPDATE MAIL_REMOVAL_LOG SET RecordType = '1', DateModified = NOW() WHERE RecordID IN (".implode(",",$RestoreArray).")";
			$UpdateDBSuccess = $li->db_db_query($sql);
		}
		
		return $UpdateDBSuccess;
	}
	
	/*
	 * @Param $XeClassMailID: array of single quoted X-eClass-MailID
	 * @Return Associative array with X-eClass-MailID as key, value as [X-eClass-MailID,NumOfReaded,Total]
	 */
	function Get_Mail_Receipt_Readed_Counting_Info($XeClassMailID=array())
	{
		//global $PATH_WRT_ROOT;
		//include_once($PATH_WRT_ROOT."includes/libdb.php");
		include_once("libdb.php");
		$li = new libdb();
		
		$XeClassMailID = (array)$XeClassMailID;
		if(sizeof($XeClassMailID)>0){
			$cond = " AND MailID IN (".implode(",",$XeClassMailID).")";
		}
		
		$sql = "SELECT MailID, SUM(Status) as NumOfReaded, COUNT(UserID) as Total 
				FROM INTRANET_IMAIL_RECEIPT_STATUS 
				WHERE 1 $cond  
				GROUP BY MailID";
		
		$result = $li->returnArray($sql);
		$assoc_result = array();
		for($i=0;$i<sizeof($result);$i++){
			$assoc_result[$result[$i]['MailID']] = $result[$i];
		}
		return $assoc_result;
	}
	
	/* Helper function - split mail list in semi-colon separated text form into array, 
	 * e.g. "student1"<student1@school.edu.hk>;"student2"<student2@school.edu.hk>;...
	 * becomes [[0=>student1@school.edu.hk,1=>"student1"],[0=>student2@school.edu.hk,1=>"student2"],[...]]
	 * */
	function Split_Text_Into_Name_Email_Array($MailListText)
	{
		$mail_list_text = trim($MailListText);
		$mail_array = array();// in 0=>value 1=>string pair
		if ($mail_list_text != '') {
			$mail_list_text = htmlspecialchars_decode($mail_list_text);
			$PreArray = explode(";",$mail_list_text);
			if(sizeof($PreArray)>0){
				foreach ($PreArray as $Key => $ToEmail) {
					$ToEmail = trim($ToEmail);
					if ($ToEmail != '') {
						if(strstr($ToEmail,"<") && strstr($ToEmail,">")){
							$EmailAddr = substr($ToEmail,strrpos($ToEmail,"<")+1,strrpos($ToEmail,">")-strrpos($ToEmail,"<")-1);
							$DisplayName = substr($ToEmail,0,strrpos($ToEmail,"<"));
						}else{
							$EmailAddr = $ToEmail;
							$DisplayName = $ToEmail;
						}
						$DisplayName = str_replace('\"','',$DisplayName);
						$mail_array[] = array($EmailAddr,htmlspecialchars($DisplayName,ENT_QUOTES));
					}
				}
			}
		}
		return $mail_array;
	}
	
	// Quickly compare highest mail UID and cached UID for a folder or list of folder
	function Check_New_Mails_For_Cached_Mails($Folder)
	{
		//global $PATH_WRT_ROOT;
		include_once("libdb.php");
		$li = new libdb();
		
		$FolderList = (array)$Folder;
		$FolderSqlList = array();
		for($i=0;$i<sizeof($FolderList);$i++){
			$FolderSqlList[] = $li->Get_Safe_Sql_Query($FolderList[$i]);
		}
		$FolderUIDAssoc = array();
		
		$sql = "SELECT Folder,IFNULL(MAX(UID),0) as MaxUID FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$this->CurUserAddress."' ";
		if($Folder!="")
			$sql .= " AND Folder IN ('".implode("','",$FolderSqlList)."') ";
		$sql .= " GROUP BY Folder ";
		$result = $li->returnArray($sql);
		for($i=0;$i<sizeof($result);$i++){
			$FolderUIDAssoc[$result[$i]['Folder']]['ClientMaxUID'] = $result[$i]['MaxUID'];
		}
		
		for($i=0;$i<sizeof($FolderList);$i++){
			$uid = $this->Get_Last_Msg_UID($FolderList[$i]);
			if(trim($uid)=="") $uid = 0;
			$FolderUIDAssoc[$FolderList[$i]]['ServerMaxUID'] = $uid;
		}
		
		return $FolderUIDAssoc;
	}
	
	function Remove_Not_Exist_Cached_Mails($Folder)
	{
		//global $PATH_WRT_ROOT;
		include_once("libdb.php");
		$li = new libdb();
		
		$Result = array();
		$FolderList = (array)$Folder;
		$FolderSqlList = array();
		for($i=0;$i<sizeof($FolderList);$i++){
			$FolderSqlList[] = $li->Get_Safe_Sql_Query($FolderList[$i]);
		}
		$DBFolderUIDAssoc = array();
		$DBFolderList = array();
		
		// find cached mails in db classified by folder
		$sql = "SELECT Folder, GROUP_CONCAT(UID SEPARATOR ',') as UID_List FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$this->CurUserAddress."' AND Folder IN ('".implode("','",$FolderList)."') GROUP BY Folder ";
		$result = $li->returnArray($sql);
		for($i=0;$i<sizeof($result);$i++){
			$DBFolderUIDAssoc[$result[$i]['Folder']] = explode(",",$result[$i]['UID_List']);
			$DBFolderList[] = $result[$i]['Folder'];
		}
		
		$FolderUIDAssoc = array();
		$FinalFolderUIDToRemove = array();
		// find cached mails to be removed in each folder
		for($i=0;$i<sizeof($FolderList);$i++){
			$folder = $FolderList[$i];
			if($this->Go_To_Folder($folder)){
				$num_msg = imap_num_msg($this->inbox);
				$overview_array = imap_fetch_overview($this->inbox,"1:$num_msg");
				$FolderUIDAssoc[$folder] = array();
				for($j=0;$j<sizeof($overview_array);$j++){
					$FolderUIDAssoc[$folder][] = $overview_array[$j]->uid;
				}
			}
			
			if(isset($DBFolderUIDAssoc[$folder]) && sizeof($FolderUIDAssoc[$folder])==0){
				$FinalFolderUIDToRemove[$folder] = $FolderUIDAssoc[$folder];
			}else if(isset($DBFolderUIDAssoc[$folder])){
				$UIDToRemove = array_values(array_diff($DBFolderUIDAssoc[$folder],$FolderUIDAssoc[$folder]));
				$FinalFolderUIDToRemove[$folder] = $UIDToRemove;
			}
			
			if(sizeof($FinalFolderUIDToRemove[$folder])>0){
				$sql = "DELETE FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$this->CurUserAddress."' AND Folder = '".$FolderSqlList[$i]."' AND UID IN (".implode(",",$FinalFolderUIDToRemove[$folder]).") ";
				$Result['RemoveMails_'.$folder] = $li->db_db_query($sql);
			}
		}
		
		// remove cached mails in folder
		$FoldersToRemove = array_values(array_diff($DBFolderList,$FolderList));
		if(sizeof($FoldersToRemove)>0){
			$sql = "DELETE FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$this->CurUserAddress."' AND Folder IN ('".implode("','",$FoldersToRemove)."') ";
			$Result['RemoveFolders'] = $li->db_db_query($sql);
		}
		
		return !in_array(false,$Result);
	}
	/* @Params: $DataArr[Folder] = array('StartUID'=>N,'EndUID'=>N) */
	function Add_Cached_Mails($DataArr,$ScreenOutput='')
	{
		//global $PATH_WRT_ROOT;
		include_once("libdb.php");
		$li = new libdb();
		$this->getImapCacheAgent();// init imap cache agent
		
		$timeout_period = 300; // 300 second = 5 minute
		$last_time = time();
		$sizePerRound = 20;
		$overview_size = 100; // overview array size limit per round 
		$thisEmail = $this->CurUserAddress;
		$debug = false;
		//if($thisEmail == 'carlos_s1@broadlearning.com') $debug = true;
		$Result = array();
		foreach($DataArr as $folder=>$uid_arr){
			$start_uid = $uid_arr['StartUID'];
			$end_uid = $uid_arr['EndUID'];
			$current_time = time();
			if(($current_time - $last_time) > $timeout_period){
				$this->openInbox($this->CurUserAddress, $this->CurUserPassword,$folder);
				$this->Go_To_Folder($folder);
				$this->IMapCache = new imap_cache_agent($this->CurUserAddress, $this->CurUserPassword);
				$this->IMapCache->Go_To_Folder($folder);
				$last_time = $current_time;
				if($debug){ echo "Reconnected at #1 on $current_time<br>";ob_flush();flush();}
			}
			if($debug){ echo "Folder = $folder; Start UID = $start_uid; End UID = $end_uid;<br>";ob_flush();flush();}
			if(($end_uid > $start_uid) && $this->Go_To_Folder($folder) && $this->IMapCache->Go_To_Folder($folder)){
				//$this->IMapCache->Go_To_Folder($folder);
				$thisFolder = $li->Get_Safe_Sql_Query($folder);
				$max_msgno = imap_msgno($this->inbox, $end_uid);
				for($temp_uid = $start_uid+1;$temp_uid<=$end_uid;$temp_uid++){
					$min_msgno = imap_msgno($this->inbox, $temp_uid);
					if($min_msgno != false) break;
				}
				$start_msgno = $min_msgno;
				$end_msgno = min($min_msgno+$overview_size,$max_msgno);
				do
				{
					if($debug){ echo "Folder = $folder; Start Msgno = $start_msgno; End Msgno = $end_msgno;<br>";ob_flush();flush();}
					//$overview_array = imap_fetch_overview($this->inbox,"$min_msgno:$max_msgno");
					$current_time = time();
					if(($current_time - $last_time) > $timeout_period){
						$this->openInbox($this->CurUserAddress, $this->CurUserPassword,$folder);
						$this->Go_To_Folder($folder);
						$this->IMapCache = new imap_cache_agent($this->CurUserAddress, $this->CurUserPassword);
						$this->IMapCache->Go_To_Folder($folder);
						$last_time = $current_time;
						if($debug){ echo "Reconnected at #2 on $current_time<br>";ob_flush();flush();}
					}
					unset($overview_array);
					$overview_array = imap_fetch_overview($this->inbox,"$start_msgno:$end_msgno");
					if(sizeof($overview_array)>0){
						unset($insert_values);
						$insert_values = array();
						foreach($overview_array as $k => $overview)
						{
							
							$current_time = time();
							if(($current_time - $last_time) > $timeout_period){
								$this->openInbox($this->CurUserAddress, $this->CurUserPassword,$folder);
								$this->Go_To_Folder($folder);
								$this->IMapCache = new imap_cache_agent($this->CurUserAddress, $this->CurUserPassword);
								$this->IMapCache->Go_To_Folder($folder);
								$last_time = $current_time;
								if($debug){ echo "Reconnected at #3 on $current_time<br>";ob_flush();flush();}
							}
							
							$thisUID = $overview->uid;
							unset($thisMailData);
							$thisMailData = $this->Get_Mail_Data_For_Cache($folder,$thisUID);
							if(count($thisMailData)>0){
								$insert_values[] = "('".implode("','",$thisMailData)."',NOW(),NOW())";
							}
							if(count($insert_values)>=$sizePerRound){
								$sql = "INSERT IGNORE INTO MAIL_CACHED_MAILS (UserEmail,Folder,UID,MailDate,MailTimestamp,MailDateString,SenderName,SenderEmail,ToRecipient,CcRecipient,BccRecipient,Subject,MessageID,IsImportant,Answered,Forwarded,Flagged,Deleted,Recent,Seen,Draft,Attachment,Message,MailSize,DateInput,DateModified) 
										VALUES ".implode(",",$insert_values)." ";
								$Result[$folder."_PART".$k] = $li->db_db_query($sql);
								unset($insert_values);
								$insert_values = array();
								if($debug){ echo "Insert to DB #1<br>";ob_flush();flush();}
								if($ScreenOutput != ''){echo $ScreenOutput;ob_flush();flush();}
							}
							usleep(10000);
							//$insert_values[] = "('$thisEmail','$thisFolder','$thisUID','$thisDate','$thisDateTimestamp','$thisDateStr','$thisSenderName','$thisSenderEmail','$thisTo','$thisCc','$thisBcc','$thisSubject','$thisMessageID','$thisIsImportant','$thisAnswered','$thisForwarded','$thisFlagged','$thisDeleted','$thisRecent','$thisSeen','$thisDraft','$thisAttachment','$thisMessage','$thisSize',NOW(),NOW())";
						}
						$size = sizeof($insert_values);
						/*if($size > 0){
							$splitted = array();
							if($size > $sizePerRound){
								$splitted =	array_chunk($insert_values,$sizePerRound);
							}else{
								$splitted[] = $insert_values;
							}
							foreach($splitted as $split_key => $split_values){
								$sql = "INSERT IGNORE INTO MAIL_CACHED_MAILS (UserEmail,Folder,UID,MailDate,MailTimestamp,MailDateString,SenderName,SenderEmail,ToRecipient,CcRecipient,BccRecipient,Subject,MessageID,IsImportant,Answered,Forwarded,Flagged,Deleted,Recent,Seen,Draft,Attachment,Message,MailSize,DateInput,DateModified) 
										VALUES ".implode(",",$split_values)." ";
								$Result[$folder."_PART".$split_key] = $li->db_db_query($sql);
							}
						}*/
						if($size > 0){
							$sql = "INSERT IGNORE INTO MAIL_CACHED_MAILS (UserEmail,Folder,UID,MailDate,MailTimestamp,MailDateString,SenderName,SenderEmail,ToRecipient,CcRecipient,BccRecipient,Subject,MessageID,IsImportant,Answered,Forwarded,Flagged,Deleted,Recent,Seen,Draft,Attachment,Message,MailSize,DateInput,DateModified) 
									VALUES ".implode(",",$insert_values)." ";
							$Result[$folder."_PART_FINAL"] = $li->db_db_query($sql);
							if($debug){ echo "Insert to DB #2<br>";ob_flush();flush();}
							if($ScreenOutput != ''){echo $ScreenOutput;ob_flush();flush();}
						}
					}
					$start_msgno = $end_msgno+1;
					$end_msgno = min($end_msgno+$overview_size,$max_msgno);
					usleep(10000);
				}while($start_msgno<=$end_msgno);
				if($debug){ echo "Leave folder $folder<br>";ob_flush();flush();}
			}
			usleep(5000);
		}
		if($debug){ echo "END $thisEmail<br>";ob_flush();flush();}
		
		return !in_array(false,$Result);
	}
	
	function Get_Mail_Data_For_Cache($Folder,$Uid)
	{
		//global $PATH_WRT_ROOT;
		include_once("libdb.php");
		$li = new libdb();
		$this->getImapCacheAgent();// init imap cache agent
		
		$thisEmail = $this->CurUserAddress;
		$thisFolder = $li->Get_Safe_Sql_Query($Folder);
		$thisUID = $Uid;
		$thisMailInfo = $this->getMailRecord($Folder, $thisUID);
		$flags = $this->IMapCache->Get_Message_Status($thisUID,$Folder,false, true);
		$thisFlags = $flags[$thisUID];
		$thisMail = $thisMailInfo[0];
		$thisSubject = $li->Get_Safe_Sql_Query($this->MIME_Decode( $thisMail['subject']));
		$thisDate = $thisMail['dateReceive'];
		$thisDateTimestamp = $thisMail['ReceiveTimeStamp'];
		$thisDateStr = $li->Get_Safe_Sql_Query($thisMail['header']->date);
		// sender
		$thisSenderName = $li->Get_Safe_Sql_Query($thisMail['fromname']);
		$thisSenderEmail = '';
		if(isset($thisMail['fromDetail'][0])){
			$fromObj = $thisMail['fromDetail'][0];
			$thisSenderEmail .= $fromObj->mailbox."@".$fromObj->host;
		}
		$thisSenderEmail = $li->Get_Safe_Sql_Query($thisSenderEmail);
		// to recipients
		$thisTo = "";
		if(isset($thisMail['toDetail']))
		{
			foreach($thisMail['toDetail'] as $key => $obj){
				if($thisTo!="") $thisTo .= ",";
				if(isset($obj->personal))
					$thisTo .= ($this->MIME_Decode($obj->personal)."<".$obj->mailbox."@".$obj->host.">");
				else
					$thisTo .= $obj->mailbox."@".$obj->host;
			}
		}
		$thisTo = $li->Get_Safe_Sql_Query($thisTo);
		// cc recipients
		$thisCc = "";
		if(isset($thisMail['ccDetail']))
		{
			foreach($thisMail['ccDetail'] as $key => $obj){
				if($thisCc!="") $thisCc .= ",";
				if(isset($obj->personal))
					$thisCc .= ($this->MIME_Decode($obj->personal)."<".$obj->mailbox."@".$obj->host.">");
				else
					$thisCc .= $obj->mailbox."@".$obj->host;
			}
		}
		$thisCc = $li->Get_Safe_Sql_Query($thisCc);
		// bcc recipients
		$thisBcc = "";
		if(isset($thisMail['bccDetail']))
		{
			foreach($thisMail['bccDetail'] as $key => $obj){
				if($thisBcc!="") $thisBcc .= ",";
				if(isset($obj->personal))
					$thisBcc .= ($this->MIME_Decode($obj->personal)."<".$obj->mailbox."@".$obj->host.">");
				else
					$thisBcc .= $obj->mailbox."@".$obj->host;
			}
		}
		$thisBcc = $li->Get_Safe_Sql_Query($thisBcc);
		$thisMessageID = $thisMail['MessageID']==''?$thisMail['eClassMailID']:$thisMail['MessageID'];
		$thisMessageID = $li->Get_Safe_Sql_Query($thisMessageID);
		
		// message body
		$thisMessage = $li->Get_Safe_Sql_Query($thisMail['message']);
		
		// attachments
		$thisAttachment = '';
		$attachDelim = '';
		if(isset($thisMail['attach_parts']) && sizeof($thisMail['attach_parts'])>0){
			foreach($thisMail['attach_parts'] as $attach_ind => $attach_detail){
				$thisAttachment .= $attachDelim.$attach_detail['FileName'];
				$attachDelim = '|';
			}
		}
		$thisAttachment = $li->Get_Safe_Sql_Query($thisAttachment);
		//debug_pr($thisMail['header']);
		if($thisMail['header']->Unseen == 'U'){
			imap_clearflag_full($this->inbox, $thisUID, '\\SEEN', ST_UID);
			$thisSeen = 0;
		}else{
			$thisSeen = $thisFlags['seen'];
		}
		
		$thisIsImportant = $thisMail['Priority'];
		$thisAnswered = $thisFlags['answered'];
		$thisForwarded = $thisFlags['forwarded'];
		$thisFlagged = $thisFlags['flagged'];
		$thisDeleted = $thisFlags['deleted'];
		$thisRecent = $thisFlags['recent'];
		$thisDraft = $thisFlags['draft'];
		
		$thisSize = sprintf("%.2f",$thisMail['header']->Size / 1024); // in KB
		
		$returnData = array();
		$returnData['UserEmail'] = $thisEmail;
		$returnData['Folder'] = $thisFolder;
		$returnData['UID'] = $thisUID;
		$returnData['MailDate'] = $thisDate;
		$returnData['MailTimestamp'] = $thisDateTimestamp;
		$returnData['MailDateString'] = $thisDateStr;
		$returnData['SenderName'] = $thisSenderName;
		$returnData['SenderEmail'] = $thisSenderEmail;
		$returnData['ToRecipient'] = $thisTo;
		$returnData['CcRecipient'] = $thisCc;
		$returnData['BccRecipient'] = $thisBcc;
		$returnData['Subject'] = $thisSubject;
		$returnData['MessageID'] = $thisMessageID;
		$returnData['IsImportant'] = $thisIsImportant;
		$returnData['Answered'] = $thisAnswered;
		$returnData['Forwarded'] = $thisForwarded;
		$returnData['Flagged'] = $thisFlagged;
		$returnData['Deleted'] = $thisDeleted;
		$returnData['Recent'] = $thisRecent;
		$returnData['Seen'] = $thisSeen;
		$returnData['Draft'] = $thisDraft;
		$returnData['Attachment'] = $thisAttachment;
		$returnData['Message'] = $thisMessage;
		$returnData['MailSize'] = $thisSize;
		return $returnData;
	}
	
	// Can [add new mail to cache db] / [update existing cached mail] / [remove cached mail]
	function Manage_Cached_Mail($Action,$Folder,$Uid,$DataArr=array())
	{
		//global $PATH_WRT_ROOT;
		include_once("libdb.php");
		$li = new libdb();
		
		$Result = false;
		$Action = strtolower($Action);
		if($Action == 'add')
		{
			// Find the mail data
			$thisMailData = $this->Get_Mail_Data_For_Cache($Folder,$Uid);
			
			$fields = "";
			$values = "";
			$delim = "";
			foreach($thisMailData as $field => $value){
				$fields .= $delim.$field;
				$values .= $delim."'".$value."' ";
				$delim = ",";
			}
			$fields .= ",DateInput,DateModified";
			$values .= ",NOW(),NOW()";
 			$sql = "INSERT INTO MAIL_CACHED_MAILS ($fields) VALUES ($values)";
			$Result = $li->db_db_query($sql);
		}elseif($Action == 'update'){
			$sql = "UPDATE MAIL_CACHED_MAILS SET ";
			$delim = "";
			foreach($DataArr as $key => $val)
			{
				$sql .= $delim." $key = '$val' ";
				$delim = ",";
			}
			$sql .= ",DateModified = NOW() ";
			$sql .= " WHERE UserEmail = '".$this->CurUserAddress."' AND Folder = '".$li->Get_Safe_Sql_Query($Folder)."' ";
			if($Uid > 0) $sql .= " AND UID = '$Uid' ";
			$Result = $li->db_db_query($sql);
		}elseif($Action == 'remove'){
			$sql = "DELETE FROM MAIL_CACHED_MAILS WHERE UserEmail = '".$this->CurUserAddress."' AND Folder = '".$li->Get_Safe_Sql_Query($Folder)."' ";
			if($Uid > 0) $sql .= " AND UID = '$Uid' ";
			$Result = $li->db_db_query($sql);
		}
		
		return $Result;
	}
	
	function Search_Cached_Mail($SearchInfo)
	{
		include_once("libdb.php");
		$li = new libdb();
		
		$sql_fields = "IF(SenderName <> '',SenderName,SenderEmail) as 'From',
						Subject,
						MailDate,
						MailTimestamp,
						Folder,
						MailSize,
						UID,
						ToRecipient,
						CcRecipient,
						BccRecipient,
						MailDateString as DateReceived,
						MessageID,
						Recent as recent,
						Flagged as flagged,
						Answered as answered,
						Deleted as deleted,
						Seen as seen,
						Draft as draft,
						IsImportant as Priority,
						Attachment ";
		if($SearchInfo['GetBody']) // get only if really needed because message body may be very large and too consume memory
			$sql_fields .= ",Message ";
		
		$SkipFolders = array();
		$SkipFolders[] = $this->reportSpamFolder;
		$SkipFolders[] = $this->reportNonSpamFolder;
		$SkipFolders[] = $this->HiddenFolder;
		
		$sql_cond = "WHERE UserEmail = '".$this->CurUserAddress."' AND Folder NOT IN ('".implode("','",$SkipFolders)."') ";
		if(sizeof($SearchInfo['Folder'])>0){
			$sql_cond .= " AND Folder IN ('".implode("','",$SearchInfo['Folder'])."') ";
		}
		// search by specific fields
		if($SearchInfo['FromDate']!=''){
			$sql_cond .= " AND MailDate >= '".$SearchInfo['FromDate']." 00:00:00' ";
		}
		if($SearchInfo['ToDate']!=''){
			$sql_cond .= " AND MailDate <= '".$SearchInfo['ToDate']." 23:59:59' ";
		}
		if($SearchInfo['From']!=''){
			$QueryFrom = $li->Get_Safe_Sql_Like_Query($SearchInfo['From']);
			$sql_cond .= " AND (SenderName LIKE '%".$QueryFrom."%' 
							OR SenderEmail LIKE '%".$QueryFrom."%') ";
		}
		if($SearchInfo['Subject']!=''){
			$QuerySubject = $li->Get_Safe_Sql_Like_Query($SearchInfo['Subject']);
			$sql_cond .= " AND Subject LIKE '%".$QuerySubject."%' ";
		}
		if($SearchInfo['To']!=''){
			$QueryTo = $li->Get_Safe_Sql_Like_Query($SearchInfo['To']);
			$sql_cond .= " AND ToRecipient LIKE '%".$QueryTo."%' ";
		}
		if($SearchInfo['Cc']!=''){
			$QueryCc = $li->Get_Safe_Sql_Like_Query($SearchInfo['Cc']);
			$sql_cond .= " AND CcRecipient LIKE '%".$QueryCc."%' ";
		}
		if($SearchInfo['Bcc']!=''){
			$QueryBcc = $li->Get_Safe_Sql_Like_Query($SearchInfo['Bcc']);
			$sql_cond .= " AND BccRecipient LIKE '%".$QueryBcc."%' ";
		}
		if($SearchInfo['Flagged']){
			$sql_cond .= " AND Flagged = 1 ";
		}
		if($SearchInfo['Attachment']!=''){
			$QueryAttachment = $li->Get_Safe_Sql_Like_Query($SearchInfo['Attachment']);
			$sql_cond .= " AND Attachment LIKE '%".$QueryAttachment."%' ";
		}
		if($SearchInfo['Body']!=''){
			$QueryBody = $li->Get_Safe_Sql_Like_Query($SearchInfo['Body']);
			$sql_cond .= " AND Message LIKE '%".$QueryBody."%' ";
		}
		
		// search by keyword
		if($SearchInfo['keyword']!=''){
			$QueryKeyword = $li->Get_Safe_Sql_Like_Query($SearchInfo['keyword']);
			$sql_cond .= " AND (MailDate LIKE '%".$QueryKeyword."%' 
								OR SenderName LIKE '%".$QueryKeyword."%' 
								OR SenderEmail LIKE '%".$QueryKeyword."%' 
								OR Subject LIKE '%".$QueryKeyword."%' 
								OR ToRecipient LIKE '%".$QueryKeyword."%' 
								OR CcRecipient LIKE '%".$QueryKeyword."%' 
								OR BccRecipient LIKE '%".$QueryKeyword."%' 
								OR Attachment LIKE '%".$QueryKeyword."%' 
								OR Message LIKE '%".$QueryKeyword."%' 
							)";
		}
		
		$sql = "SELECT ";
		$sql .= $sql_fields;
		$sql .= " FROM MAIL_CACHED_MAILS ";
		$sql .= $sql_cond;
		if($SearchInfo['SortFields']!='')
			$sql .= "ORDER BY ".$SearchInfo['SortFields']." ";
		else
			$sql .= " ORDER BY MailDate,Folder ";
		
		return $li->returnArray($sql);
	}
	
	##### Debug use only #####
	public static $statictimestart = 0;
	function Start_Timer() {
		//global $timestart;
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		self::$statictimestart = $mtime;
		return true;
	}
	
	function Stop_Timer($precision=5) {
		//global $timestart;
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		$timeend = $mtime;
		$timetotal = $timeend-self::$statictimestart;
		$scripttime = number_format($timetotal,$precision);
		return $scripttime;
	}
	##### ----------- #####
	
	function FormatRecipientOption($str)
	{
		$output = str_replace(array('<','>','"'),array('&lt;','&gt;','&quot;'),$str);
		return $output;
	}
	
	// $str is in pattern [ "name1"<email@domain.com>; "name2" <email2@domain.com>; noname@domain.com ]
	function ExtractEmailsFromString($str)
	{
		/*
		$str = str_replace(array('&lt;','&gt;','&quot;'),array('<','>','"'),$str);
		$arr = explode(";",$str);
		$emails = array();
		for($i=0;$i<count($arr);$i++){
			$tmp_email = trim($arr[$i]);
			if(strpos($tmp_email,"<") !== FALSE){
				if(preg_match("/<(.+\@.+)>/",$tmp_email,$matches)){
					if(count($matches)>1){
						$emails[] = trim($matches[1]);
					}
				}
			}else if(strpos($tmp_email,"@") !== FALSE){
				$emails[] = $tmp_email;
			}
		}
		return $emails;
		*/
		$str = str_replace(array('&lt;','&gt;','&quot;'),array('<','>','"'),$str);
		$ary = $this->splitEmailStringIntoArray($str,';',true);
		$valid_emails = $ary[2];
		$emails = array();
		$email_count = count($valid_emails);
		for($i=0;$i<$email_count;$i++){
			$emails[] = trim($valid_emails[$i][1]);
		}
		return $emails;
	}
	
	function Log_Delete_Action($LogContent)
	{
		$libdb = new libdb();
		
		$log_content = $libdb->Get_Safe_Sql_Query($LogContent);
		$sql = "INSERT INTO MODULE_RECORD_DELETE_LOG (Module,RecordDetail,LogDate,LogBy) VALUES('iMail plus','".$log_content."',NOW(),'".$_SESSION['UserID']."')";
		$success = $libdb->db_db_query($sql);
		
		return $success;
	}
	
	function Clean_Delete_Log($OlderThanSeconds=5184000) // about 2 month of seconds
	{
		$libdb = new libdb();
		
		$date = date("Y-m-d 00:00:00",time() - $OlderThanSeconds);
		$sql = "DELETE FROM MODULE_RECORD_DELETE_LOG WHERE Module='iMail plus' AND LogBy='".$_SESSION['UserID']."' AND LogDate < '".$date."' ";
		
		return $libdb->db_db_query($sql);
	}
	
	// format a datetime string to follow RFC822 standard and more php friendly
	function formatDatetimeString($datetime)
	{
		$return_datetime = str_replace("UT","UTC",$datetime); // UT would make strtotime() fail
		// remove ( and ) which would make strtotime() fail
		$matches = array();
		if(preg_match('/^.+(\([^\(\)]+\)).*$/',$return_datetime,$matches)){
			$numOfMatch = count($matches);
			if($numOfMatch>1) {
				for($i=0;$i<$numOfMatch;$i++) {
					$match = $matches[$i];
					$replace = str_replace(array('(',')'),'',$match);
					$return_datetime = str_replace($match,$replace,$return_datetime);
				}
			}
		}
		return $return_datetime;
	}
	
	// Return sql string for retrieving parent info
	// alias: 	a - Student
	// 			c - Parent
	function GetSQL_ParentEmail4JS($name_field,$studentIdCsv,$distinct="")
	{
		global $imail_forwarded_email_splitter;
		$id_field = ($distinct == "") ? "b.ParentID" : "a.UserID";
		$sql = "(SELECT $distinct $id_field, $name_field , replace(p.ForwardedEmail,',','$imail_forwarded_email_splitter') as ForwardedEmail 
					FROM INTRANET_USER AS a 
					INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
					INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID)
					INNER JOIN INTRANET_IMAIL_PREFERENCE p ON c.UserID=p.UserID
					WHERE c.RecordStatus = 1 
						AND b.StudentID IN ($studentIdCsv)
					 AND (p.ForwardedEmail IS NOT NULL AND p.ForwardedEmail <> '') 
					ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		return $sql;		
	}
	
	// Return sql string for retrieving parent info
	// alias: 	a - Parent
	// 			c - Student
	function GetSQL_ParentEmail4All($NameField,$SortField)
	{
		global $imail_forwarded_email_splitter;
		$sql = "SELECT DISTINCT a.UserID as UserID, $NameField as UserName, 
						replace(p.ForwardedEmail,',','$imail_forwarded_email_splitter') as UserEmail,
					 	$SortField as SortField  
				FROM 
				  	INTRANET_USER AS a 
					INNER JOIN INTRANET_IMAIL_PREFERENCE p ON a.UserID=p.UserID
				    LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) 
				    LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) 
				WHERE a.RecordType = 3 
				    AND a.RecordStatus = 1 
					AND (p.ForwardedEmail IS NOT NULL AND p.ForwardedEmail <> '') ";
		return $sql;		
	}

	// Return sql string for retrieving parent info for form / class level
	// outmost alias:	a - Student 
	// 					c - Parent   
	function GetSQL_ParentEmail4FormClass($name_field,$sort_field,$UserID,$CurrentAcademicYearID,$identity="Student",$level="form")
	{
		global $imail_forwarded_email_splitter;
		switch($identity)
		{
			case "Parent":
				$year_class_table = "YEAR_CLASS_USER";
				$join_table = "INNER JOIN INTRANET_PARENTRELATION AS r ON (b.UserID = r.StudentID)";
				$filter_field_name = "r.ParentID";
				break;
			case "Student":
				$year_class_table = "YEAR_CLASS_USER";
				$join_table = "";
				$filter_field_name = "b.UserID";
				break;
			case "Teaching":
				$year_class_table = "YEAR_CLASS_TEACHER";
				$join_table = "";
				$filter_field_name = "b.UserID";
				break;
		}
		 
		if ($level == "form")
		{
			$sub_query = "SELECT a.YearClassID 
				            FROM 
				                  YEAR_CLASS AS a 
				            WHERE a.YearID IN 
				                  (SELECT DISTINCT a.YearID 
				                    FROM 
				                          YEAR_CLASS AS a 
				                          INNER JOIN $year_class_table AS b ON (a.YearClassID = b.YearClassID)
										  $join_table 
				                    WHERE $filter_field_name = '$UserID' 
				                      AND a.AcademicYearID = '$CurrentAcademicYearID') 
				              AND a.AcademicYearID = '$CurrentAcademicYearID'";
		}
		else		// class level
		{
			$sub_query = "SELECT DISTINCT a.YearClassID 
				            FROM 
				                  YEAR_CLASS AS a 
				                  INNER JOIN $year_class_table AS b ON (a.YearClassID = b.YearClassID) 
				            WHERE $filter_field_name = '$UserID' 
				              AND a.AcademicYearID = '$CurrentAcademicYearID'";			
		}
		
		$sql = "SELECT DISTINCT c.UserID as UserID, 
					$name_field as UserName, 
					replace(p.ForwardedEmail,',','$imail_forwarded_email_splitter') as UserEmail, 
					$sort_field as SortField 
				FROM 
					INTRANET_USER AS a					
					INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
					INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID)
					INNER JOIN INTRANET_IMAIL_PREFERENCE p ON c.UserID=p.UserID 
				WHERE c.RecordStatus = 1 
				AND b.StudentID IN 
				  (SELECT a.UserID 
				    FROM 
				          YEAR_CLASS_USER AS a 
				          INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
				    WHERE a.YearClassID IN 
				          ($sub_query)
				  )  
				AND (p.ForwardedEmail IS NOT NULL AND p.ForwardedEmail <> '') ";
		return $sql;		
	}	
	
	// Return sql string for retrieving parent info for subject / subject group
	// outmost alias:	a - Student 
	// 					c - Parent   
	function GetSQL_ParentEmail4SubjectGroup($name_field,$sort_field,$UserID,$CurrentTermID,$level="subject")
	{
		global $imail_forwarded_email_splitter;
		 
		if ($level == "subject")
		{
			$sub_query = "SELECT DISTINCT c.SubjectGroupID 
					        FROM 
					              SUBJECT_TERM_CLASS_TEACHER AS a 
					              INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
					              INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) 
					        WHERE a.UserID = '$UserID' 
					          AND b.YearTermID = '$CurrentTermID' 
					          AND c.YearTermID = '$CurrentTermID'";
		}
		else		// subject group
		{
			$sub_query = "SELECT DISTINCT b.SubjectGroupID 
					        FROM 
					              SUBJECT_TERM_CLASS_TEACHER AS a 
					              INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) 
					        WHERE a.UserID = '$UserID' 
					          AND b.YearTermID = '$CurrentTermID'";			
		}
		
		$sql = "SELECT DISTINCT c.UserID as UserID, 
					$name_field as UserName, 
					replace(p.ForwardedEmail,',','$imail_forwarded_email_splitter') as UserEmail, 
					$sort_field as SortField 
				FROM 
					INTRANET_USER AS a					
					INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) 
					INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID)
					INNER JOIN INTRANET_IMAIL_PREFERENCE p ON c.UserID=p.UserID 
				WHERE c.RecordStatus = 1 
				AND b.StudentID IN 
				  (SELECT a.UserID 
				    FROM 
				          SUBJECT_TERM_CLASS_USER AS a 
				          INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) 
				    WHERE a.SubjectGroupID IN 
				          ($sub_query)
				  )  
				AND (p.ForwardedEmail IS NOT NULL AND p.ForwardedEmail <> '') ";
		return $sql;		
	}	
	
	function Set_AVAS_Rules($type)
	{
		$libdb = new libdb();
		
		$type = strtolower($type);
		$sql = "SELECT Rule,Score FROM MAIL_AVAS_RULES WHERE Type='$type' ORDER BY Score DESC";
		$records = $libdb->returnResultSet($sql);
		$record_count = count($records);
		
		//$post_string = "actype=".$this->actype;
		$postData = array();
		for($i=0;$i<$record_count;$i++){
			//$post_string .= "&"."Rule[$i]=".$records[$i]['Rule']."&Score[$i]=".$records[$i]['Score'];
			$postData['Rule['.$i.']'] = $records[$i]['Rule'];
			$postData['Score['.$i.']'] = $records[$i]['Score'];
		}
		
		if($this->actype != ""){
			$postData['actype'] = $this->actype;
		}
		$postData['Type'] = $type;
		
		$path = $this->mail_api_url['SetAvasRules'];
		$url = "http://".$this->remote_server .
                                  ($this->remote_server_port!=""&&$this->remote_server_port!=80?
                                   ":".$this->remote_server_port : "")
                                  .$path;
		
		//$url = "http://192.168.0.146:31002/home/imail_gamma_test/debug_set_avas_rules.php";
		
		//parse_str($post_string, $postData);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
	function changeAccountName($ParOldAccount, $ParNewAccount)
    {
        global $SYS_CONFIG;
		
        if (!$this->checkSecret())
        {
             return false;
        }
		
		$oldAccount = $ParOldAccount;
		$newAccount = $ParNewAccount;
		
        $success = true;
		$path = $this->mail_api_url['ChangeAccountName'];
        $post_data = "oldAccount=".urlencode($oldAccount);
        $post_data .= "&newAccount=".urlencode($newAccount);
        $post_data .= "&module=iMail";
		
        if ($this->actype != "")
        {
            $post_data .= "&actype=".$this->actype;
        }
		$post_data .= "&secret=".urlencode($this->secret);

        $response = $this->getWebPage($path."?".$post_data);
        $success = $response=="1";
           
        if ($success)
        {
            return true;
        }else{
        	return false;
        }
    }
    
    function removeQuotaAndSharedMailBoxMember($TargetUserID)
    {
    	global $intranet_root;
    	include_once($intranet_root."/includes/libdb.php");
    	$li = new libdb();
    	$resultAry = array();
    	$sql = "DELETE FROM INTRANET_CAMPUSMAIL_USERQUOTA WHERE UserID='$TargetUserID'";
    	$resultAry[] = $li->db_db_query($sql);
    	$sql = "DELETE FROM MAIL_SHARED_MAILBOX_MEMBER WHERE UserID='$TargetUserID'";
    	$resultAry[] = $li->db_db_query($sql);
    	return !in_array(false,$resultAry);
    }
    
    /* 
     * @usage: use to split a recipient string into separated name email entry, set $getNameEmailAry to true to get separated name and email info
     * @param string $str : input recipient string e.g. "Alice" <alice@abc.com>; Bob <bob@bl.com>; cat@abc.com; 'David, Chan' <davidchan@abc.com.hk>;
     * @param string $sep : recipient separator, commonly use ; or ,
     * @param bool $getNameEmailAry : if true, return result with 2=>valid_name_email_list, 3=>invalid_name_email_list
     * @return array[0=>valid_list, 1=>invalid_list, 2=>valid_name_email_list, 3=>invalid_name_email_list]
     * @remark: depends on isEmailAddressInString(), inQuotedExpression(), hasNonEscapedQuote(), splitNameEmailEntry()
     */
	function splitEmailStringIntoArray($str, $sep=';', $getNameEmailAry=false)
	{
		$valid_emails = array();
		$invalid_emails = array();
		$in = $str;
		$pos = -1;
		$str_left = "";
	
		while(strlen($in) > 0) // input string still has characters, continue process
		{
			if($str_left == "")
			{
				$pos = strpos($in, $sep, $pos+1); // find string before the first separator
				if($pos === false){ // no separator found
					if($this->isEmailAddressInString($in) && !$this->inQuotedExpression($in) && !$this->hasNonEscapedQuote($in)) // check the whole string is email
					{
						$valid_emails[] = trim($in);
					}else{
						$invalid_emails[] = trim($in);
					}
					$in = "";
					$str_left = "";
					$pos = -1;
				}else{
					$str_left = substr($in, 0, $pos);
				}
			}
			if($this->isEmailAddressInString($str_left) && !$this->inQuotedExpression($str_left))
			{
				// it is a email, get it
				if($this->hasNonEscapedQuote($str_left))
				{
					$invalid_emails[] = trim($str_left);
				}else{
					$valid_emails[] = trim($str_left);
				}
				$in = substr($in, $pos+1); // skip the left string and go to get next email
				$str_left = "";
				$pos = -1;
			}else{
				// it is not an email, get next separator position
				$pos = strpos($in, $sep, $pos+1);
				if($pos === false) // no more separator found
				{
					$in = trim($in);
					if($in != '')
					{
						if($this->isEmailAddressInString($in) && !$this->inQuotedExpression($in) && !$this->hasNonEscapedQuote($in)) // check the whole string is email
						{
							$valid_emails[] = $in;
						}else{
							$invalid_emails[] = $in;
						}
					}
					$in = ""; // no more input
					$str_left = "";
					$pos = -1;
				}else{
					$str_left = substr($in, 0, $pos);
				}
			}
		}
		
		if($getNameEmailAry)
		{
			$valid_array = array();
			$invalid_array = array();
			$valid_count = count($valid_emails);
			$invalid_count = count($invalid_emails);
			for($i=0;$i<$valid_count;$i++){
				$valid_array[] = $this->splitNameEmailEntry($valid_emails[$i]);
			}
			for($i=0;$i<$invalid_count;$i++){
				$invalid_array[] = $this->splitNameEmailEntry($invalid_emails[$i]);
			}
			return array($valid_emails, $invalid_emails, $valid_array, $invalid_array);
		}
		return array($valid_emails, $invalid_emails);
	}
	
	/*
	 * @usage: check if any valid email inside the string $str
	 */
	function isEmailAddressInString($str)
	{
		$strict = true;
		$email_pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z".($strict?"":"0-9-")."]{2,})$/i";
		
		$str = trim($str);
		$len = strlen($str);
		if($str[$len-1] == '>') // if end with >
		{
			// find < before >
			$lb_pos = strrpos($str, '<');
			if($lb_pos === false){
				return false;
			}
			// find string in between < and >
			$str_in_b = trim(substr($str, $lb_pos+1, $len-($lb_pos+1)-1));
			// if is email in < and >
			if(preg_match($email_pattern, $str_in_b))
			{
				return true;
			}else{
				// not email in < and >
				return false;
			}
		}else if(preg_match($email_pattern, $str)){ // no < >, check is email
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * @usage: check if a string is started with a double quote or any non-escaped double quote inbetween
	 */
	function inQuotedExpression($str)
	{
		$dq = '"';
		
		$str = trim($str);
		$len = strlen($str);
		$quote_count = 0;
		
		for($i=0;$i<$len;$i++)
		{
			if($str[$i] == $dq && ($i==0 || $str[$i-1] != '\\')){ // not escaped quote
				$quote_count++;
			}
		}
		if($quote_count > 0 && $quote_count % 2 !=0)
		{
			return true;
		}
		return false;
	}
	
	/*
	 * @usage: check if any non-escaped double quotes inside or has start quote but no close quote
	 */
	function hasNonEscapedQuote($str)
	{
		$dq = '"';
		
		$str = trim($str);
		$len = strlen($str);
		if($len == 0) return false;
		$invalid = false;
		$quote_count = 0; // not escaped quote count
		
		if($str[0] == '"'){ // has open quote
			$last_non_escape_quote_pos = strrpos($str, $dq); // find close quote
			if($last_non_escape_quote_pos !== false){ // has close quote
				$last_quote_pos = -1;
				for($i=1;$i<$len;$i++)
				{
					if($str[$i] == $dq && $str[$i-1] != '\\'){ // not escaped quote
						$quote_count++;
						if($i<$last_non_escape_quote_pos){
							$last_quote_pos = $i;
						}
					}
				}
				if($last_quote_pos > -1 && $last_quote_pos < $last_non_escape_quote_pos && $quote_count>0) // has non escaped quote in between, invalid
				{
					$invalid = true;
				}
			}else{ // has open quote but no close quote, invalid
				$invalid = true;
			}
		}else{// no open quote
			for($i=1;$i<$len;$i++)
			{
				if($str[$i] == $dq && $str[$i-1] != '\\'){ // not escaped quote
					$quote_count++;
				}
			}
			if($quote_count>0){
				$invalid = true;
			}
			
		}
		
		return $invalid;
	}
	
	/* 
	 * @usage: use to split a single recipient name email string
	 * @param string $entry: name + email
	 * ex1: "Alice" <alice@abc.com>
	 * ex2: Bob <bob@abc.com>
	 * ex3: 'Cat' <cat@abc.com>
	 * ex4: david@abc.com
	 * ex5: Eli's email <eli@abc.com>
	 * @return array(0=>name,1=>email)
	 * @remark: name would be empty string if only has email
	 */
	function splitNameEmailEntry($entry)
	{
		$entry = trim($entry);
		$len = strlen($entry);
		
		if($entry[$len-1] == '>')
		{
			$lb_pos = strrpos($entry, '<');
			if($lb_pos === false)
			{
				$name = "";
				$email = substr($entry,0,$len-2);
			}else{
				$name = trim(substr($entry,0,$lb_pos));
				$email = trim(substr($entry, $lb_pos+1, $len-($lb_pos+1)-1));
			}
		}else{
			$name = "";
			$email = $entry;
		}
		
		$name_len = strlen($name);
		if($name[0] == '"' && $name[$name_len-1] == '"')
		{
			$name = substr($name,1,$name_len-2);
		}
		
		$pair = array($name, $email);
		return $pair;
	}
	
	function recalculateQuota($targetEmail)
    {
	    global $SYS_CONFIG;
	
	    if (!$this->checkSecret())
	    {
	         return false;
	    }
		
	    $success = true;
		$path = $this->mail_api_url['RecalculateQuota'];
	    $post_data = "email=".urlencode($targetEmail);
	    $post_data .= "&module=iMail";
	
	    if ($this->actype != "")
	    {
	        $post_data .= "&actype=".$this->actype;
	    }
		$post_data .= "&secret=".urlencode($this->secret);
	    $response = $this->getWebPage($path."?".$post_data);
	    $success = ($response[0]=="1");
	    
	    return $success;
    }
    
    function htmlencodeEmailAddressAngleBrackets($text)
    {
    	$pattern = '/<\s*([^>:;"]*@[^>:;"]*)\s*>/i';
    	$text = preg_replace($pattern,'$1',$text);
    	$pattern = '/&lt;\s*([^>:;"]*@[^>:;"]*)\s*&gt;/i';
    	$text = preg_replace($pattern,'$1',$text);
    	
    	return $text;
    }
}
	
}        // End of directive

// following migrate from common_function.php from esf, put here TEMPORARILY
function IMap_Encode_Folder_Name($folderName)
{
	if(strtoupper(mb_detect_encoding($folderName))!="UTF7-IMAP")
        return Convert_Encoding($folderName, "UTF7-IMAP", "UTF-8");
    else
  		return $folderName;
}

function IMap_Decode_Folder_Name($folderName)
{
	if(strtoupper(mb_detect_encoding($folderName))!="UTF-8")
  		return Convert_Encoding($folderName, "UTF-8", "UTF7-IMAP");
  	else
  		return $folderName;
  //big5 way of retrieving the folder name
}

function Convert_Encoding($OriginText, $TargetEncode, $OriginEncode)
{
	
	    if ($OriginEncode == $TargetEncode)
        {
        	 $ReturnX = $OriginText;
        }
        else if ($OriginEncode == 'default')
        {
                $ReturnX = mb_convert_encoding($OriginText, $TargetEncode);
        } 
        else
        {
                if ($OriginEncode!="" && !strstr($OriginEncode, "unknown"))
                {
                        $ReturnX = mb_convert_encoding($OriginText,$TargetEncode,$OriginEncode);
                } else
                {
                        $ReturnX = $OriginText;
                }
        }

        return $ReturnX;
}

function HTML_Transform($document){
	$document = str_replace ("<HTML>", "", $document);
	$document = str_replace ("</HTML>", "", $document);
	$document = preg_replace("'<HTML([^>]*?>)'si", "", $document);
	$document = preg_replace ("'<HEAD[^>]*?>.*?</HEAD>'si", "", $document);
	$document = preg_replace ("'<SCRIPT[^>]*?>.*?</SCRIPT>'si", "", $document);
	$document = preg_replace ("'<STYLE[^>]*?>.*?</STYLE>'si", "", $document);
	$document = preg_replace("'<BASE[^>]*?/>'si", "", $document);
	$document = preg_replace("'<BODY([^>]*?>)'si", "<DIV$1", $document);
	$document = str_replace ("</BODY>", "</DIV>", $document);
	$document = preg_replace ("'<FORM[^>]*?>'si","",$document);
	$document = str_ireplace ("</FORM>","",$document);
	$document = preg_replace ("'<TEXTAREA[^>]*?>'si","",$document);
	$document = str_ireplace ("</TEXTAREA>","",$document);
	$document = preg_replace ("'<INPUT[^>]*?>'si","",$document);
	/*$document = preg_replace("'<A([^>]*?>)'si", "", $document);
	$document = preg_replace("'</A>'i", "", $document);*/
	$document = preg_replace ('/background=(.*?)(\'|"|;|\s|\\\)/e', "'style=\"background-image: url('.'\\1'.');\"'", $document);
	$document = str_replace("\\\"","\"",$document);
	//$document = str_replace("mailto:", "compose.php?targetemail=",$document);
	
	//$document = preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', '<a href="$1" >$1</a>', $document);

	//$LinkExp = '(.{0,6})?((((http|https)://)|(www\.))+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,6})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;%_\./-~-]*)?)(.{0,4})?';
//	$LinkExp = '(.{0,6})?((((http|https)://)|(www\.))+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,6})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;%_\./\?\=]*)?)(.{0,4})?';
//	$LinkExp='(.{0,1})?((https?://|www\.)+([-\w\.]+)+(:\d+)?([:/]([\w/_\.]*(\?\S+)?)?)*)(.{0,1})?';

	//$LinkExp='(.{0,1})?((https?://|www\.)+([-\w\.]+)+(:\d+)?([:/](([\w/_\-\.]*)(\?[^ <>]+)?)?)*)(.{0,1})?';// comment out on 2011-02-21
//	$LinkExp='(.{0,1})?((https?://|www\.)+([-\w\.]+)+(:\d+)?([:/](([\w~#%&;\'\+/_\-\.\)\(]*)(\?[^ <>"]+)?)?)*)(.{0,1})?';// fix on 2011-02-21    
//	$document = preg_replace_callback('@'.$LinkExp.'@i', 'Replace_Link', $document);
	$document = Replace_Links_Without_Anchor_Tag($document);

	//$NumOfMatches = preg_match_all('@('.$LinkExp.')@',$document,$Matches,PREG_OFFSET_CAPTURE);

	//$document = '<base target="_blank"/>'.$document.'<base target="_self"/>';
	//echo htmlspecialchars($document);
	return $document;
}

function Replace_Link($matches) {
//	echo '<pre>';
//	var_dump($matches);
//	echo '</pre>';
//debug_pr($matches);
	// the code below is to remove "&gt;" at the end of an url, because "&gt;" is valid in url regular expression pattern but "&gt;" should not be part of the url. 
	// e.g. html content that use brackets to wrap the url &lt;http://10.0.1.23/scanner/del.htm&gt; 
	$end_with_greaterthan_bracket = substr_compare($matches[2], '&gt;', -strlen('&gt;')) === 0;
	if($end_with_greaterthan_bracket){
		$matches[2] = str_replace('&gt;','',$matches[2]);
		$excess = '&gt;';
	}
	
	if(trim($matches[1])!="")
	{
		if($matches[1]=='"' || $matches[1]=="'" || $matches[count($matches)-1]=="'" || $matches[count($matches)-1]=='"')
		{
			return $matches[0];
		}else
		{
			if(stristr($matches[2], 'https://') || stristr($matches[2], 'http://'))
			{
				return $matches[1].'<a href="'.$matches[2].'" target="_blank">'.$matches[2].'</a>'.$excess.$matches[count($matches)-1];
			}else
			{
				return $matches[1].'<a href="http://'.$matches[2].'" target="_blank">'.$matches[2].'</a>'.$excess.$matches[count($matches)-1];
			}
		}
	}else
	{
		if(stristr($matches[2], 'http://') || stristr($matches[2], 'https://'))
		{
			return $matches[1].'<a href="'.$matches[2].'" target="_blank">'.$matches[2].'</a>'.$excess.$matches[count($matches)-1];
		}else
		{
			return $matches[1].'<a href="http://'.$matches[2].'" target="_blank">'.$matches[2].'</a>'.$excess.$matches[count($matches)-1];
		}
	}
}

function Replace_Links_Without_Anchor_Tag($html)
{
	$offset = 0;
	
	$matches = array();
	if(preg_match_all('@(https?://|www\.)+([-\w\.]+)+(:\d+)?([:/](([\w~#=%&;\'\+/_\-\.\)\(]*)(\?[^ <>"]+)?)?)*@i',$html,$matches)){
		//debug_pr($matches);
		$links = $matches[0];
		for($i=0;$i<count($links);$i++){
			$start_pos = strpos($html,$links[$i],$offset);
			if($start_pos !== false){
				$end_pos = $start_pos + strlen($links[$i]);
				$html_head_part = substr($html,0,$start_pos);
				$html_tail_part = substr($html,$end_pos);
				$front_char = $start_pos >= 0? $html[$start_pos-1] : '';
				$back_char = $end_pos < strlen($html)? $html[$end_pos] : '';
				$a_tag = substr($html,$end_pos,strlen('</a>'));
				if($front_char != '"' && $front_char != "'" && $front_char != '=' && $back_char != '"' && $back_char != "'" && strcasecmp($a_tag,'</a>')!=0){
					//debug_pr($links[$i]);
					$is_begin_with_http = strpos($links[$i],'http') === 0;
					$new_link = '<a href="'.(!$is_begin_with_http?'http://':'').$links[$i].'" target="_blank" data-link="">'.$links[$i].'</a>';
					$html = $html_head_part.$new_link.$html_tail_part;
					$offset = strlen($html_head_part.$new_link);
				}else{
					$offset = $end_pos;
				}
			}
		}
	}

	return $html;
}

/*
function Output_To_Browser($content, $filename,$content_type="")
{
         if ($content_type=="" || $content_type=="application/zip") //to handle IE download corrupted zip file problem
         {
             $content_type = "application/octet-stream";
         }
         while (@ob_end_clean());
         header("Pragma: public");
         header("Expires: 0"); // set expiration time
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Content-type: $content_type");
         header("Content-Length: ".strlen($content));
         header("Content-Disposition: attachment; filename=\"".$filename."\"");
         echo $content;

}
*/
function Output_To_Browser($content, $filename,$content_type="")
{
		if ($content_type=="" || $content_type=="application/zip")
		{
			$content_type = "application/octet-stream";
		}
		while (@ob_end_clean());
        $useragent = $_SERVER['HTTP_USER_AGENT'];

		if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = 'IE';
		} elseif(preg_match('|Edge/(\d+)|',$useragent,$matched)){
			$browser_version = $matched[1];
		    $browser= 'Edge';
		} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = 'Opera';
		} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = 'Firefox';
		} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
		    $browser_version=$matched[1];
		    $browser = 'Safari';
		} else {
			// browser not recognized!
		    $browser_version = 0;
		    $browser= 'other';
		}

		$encoded_filename = $filename;
        $encoded_filename = rawurlencode($filename);
		//$encoded_filename = str_replace("+", "%20", $encoded_filename);
		
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: $content_type"); 
		header("Content-Length: ".strlen($content));
		
		if ($browser == "IE")
		{
			$encoded_filename = str_replace(array("%20","%25"),array(" ","%"), $encoded_filename);
			header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
		}
		else if ($browser == "Firefox")
		{
			//header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			header('Content-Disposition: attachment; filename*=utf8\'\'' . $encoded_filename);
		}
		else if($browser == "Safari") // for Chrome
		{
			//header('Content-Disposition: attachment; filename="' . $filename . '"');
			header('Content-Disposition: attachment; filename="'.$encoded_filename.'"; filename*=utf-8\'\''.$encoded_filename);
		}
		else
		{
			header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
		}
		
		echo $content;
}

//function UTF8_To_DB_Handler($String="")
//{
//	// reference link
//	// http://209.85.175.104/search?q=cache:BB2ZWeisB9oJ:www.php.net/mssql+MS+SQL+2005+Express+UTF-8&hl=en&ct=clnk&cd=10
//	
//	$String_ucs2 = iconv('UTF-8', 'UCS-2LE', $String);
//
//  	// converting UCS-2 string into "binary" hexadecimal form
//    $String_arr = unpack('H*hex', $String_ucs2);
//
//    $String_hex = "0x{$String_arr['hex']}";
//    
//    $ReturnString = $String_hex;
//    
//	return $ReturnString;
//} // end function convert string before insert into database
//
//function UTF8_From_DB_Handler($String="")
//{
//	$ReturnString = iconv('UCS-2LE', 'UTF-8', $String);
//	
//	return $ReturnString;
//} // end function get string from database

function html2txt($document){
	$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
							 );
	$text = preg_replace($search, '', $document);
	return $text;
}

function Remove_Html_Comment($document) {
	$search = array('@<![\s\S]*?--[ \t\n\r]*>@');// Strip multi-line comments including CDATA
	$text = preg_replace($search, '', $document);
	return $text;
}

function Parse_Date_Time($ParDate, $ParID="", $ParNoComparison=false, $ParShowTime=true, $ParReturnType="normal", $TimeInString=true)
{
        global $Lang, $SYS_CONFIG;

        if ($ParDate=="")
        {
        	return;
        }

        # pls move to lang
        if ($ParID =="")
        {
	        $ParID2 = "PastDateDiv";
        }
        else
        {
	        $ParID2 = $ParID;
        }

        $ago['days'] = $Lang["TimeAgo"]['days'];
        $ago['hours'] = $Lang["TimeAgo"]['hours'];
        $ago['minutes'] = $Lang["TimeAgo"]['minutes'];
        $ago['seconds'] = $Lang["TimeAgo"]['seconds'];

        $after['days'] = $Lang["TimeAfter"]['days'];
        $after['hours'] = $Lang["TimeAfter"]['hours'];
        $after['minutes'] = $Lang["TimeAfter"]['minutes'];
        $after['seconds'] = $Lang["TimeAfter"]['seconds'];
				
				if ($TimeInString) 
					$ProcessTime = strtotime($ParDate);
				else 
					$ProcessTime = $ParDate;
					
        $diff = time() - $ProcessTime;
        //echo $diff.' '.$ParDate.'<br>';

        if ($diff > 0)
        {
                $Lang2 = $ago;
        } else
        {
                $Lang2 = $after;
                $diff = 0 - $diff;
        }


        if( $days = intval((floor($diff/86400))) ) $diff = $diff % 86400;
        if( $hours = intval((floor($diff/3600))) ) $diff = $diff % 3600;
        if( $minutes = intval((floor($diff/60))) ) $diff = $diff % 60;
        $seconds = intval( $diff );

        if ($days != "")
        {
                $DayTimeDifference = $days.$Lang2['days'];
        } else if ($hours != "")
        {
                $DayTimeDifference = $hours.$Lang2['hours'];
        } else if ($minutes != "")
        {
                $DayTimeDifference = $minutes.$Lang2['minutes'];
        } else
        {
                $DayTimeDifference = $seconds.$Lang2['seconds'];
        }

        if (date("Y", $ProcessTime)!=date("Y"))
        {
        	// previous years
        	$DateFormUsed = $SYS_CONFIG['DateFormat']['Full'];
        } elseif (date("dm", $ProcessTime)!=date("dm") || !$ParShowTime || $ParReturnType=="fulldisplay")
        {
        	// in current year and NOT today
        	$DateFormUsed = $SYS_CONFIG['DateFormat']['DayMonth'];
        	$DayMonthTimeSeparator = ",";
    	}

		$DateFormUsed .= ($ParShowTime) ? $DayMonthTimeSeparator." H:i" : "";
    	$DateShow = date($DateFormUsed, $ProcessTime);

		if ($ParReturnType=="normal" || $ParReturnType=="fulldisplay")
		{
	        if ($ParID2!="" || !$ParNoComparison)
	        {
	        	$ComparisonTitle = (!$ParNoComparison) ? "title=\"".$DayTimeDifference."\"" : "";
	        	$DeteID = ($ParID2!="") ? "id=\"".$ParID2."\"" : "";
	
	        	return "<span {$ComparisonTitle} {$DeteID} >".$DateShow."</span>";
	        } else
	        {
	        	return $DateShow;
	        }
	    } elseif ($ParReturnType=="difference")
	    {
	    	return $DayTimeDifference;
	    }
}

// Replace comma between mail recipients to semi-colon
function Replace_Display_Comma($AddressList) {
	$QuotePairs = array();
	$FirstDoubleQuote = "";
	$FirstSingleQuote = "";

	for ($i=0; $i< strlen($AddressList); $i++) {
		if ($AddressList[$i] == '"') {
			if ($FirstDoubleQuote === "") {
				$FirstDoubleQuote = $i;
			}
			else {
				$QuotePairs[] = array($FirstDoubleQuote,$i+1);
				
				if ($FirstSingleQuote > $FirstDoubleQuote && $FirstSingleQuote < $i && $FirstSingleQuote != "") {
					$FirstSingleQuote = "";
				}
				$FirstDoubleQuote = "";
			}
		}
		else if ($AddressList[$i] == "'") {
			if ($FirstSingleQuote === "") {
				$FirstSingleQuote = $i;
			}
			else {
				$QuotePairs[] = array($FirstSingleQuote,$i+1);
				
				if ($FirstDoubleQuote > $FirstSingleQuote && $FirstDoubleQuote < $i && $FirstDoubleQuote != "") {
					$FirstDoubleQuote = "";
				}
				$FirstSingleQuote = "";
			}
		}
	}
	
	for ($i=0; $i< sizeof($QuotePairs); $i++) {
		for ($j=$QuotePairs[$i][0]; $j< $QuotePairs[$i][1]; $j++) {
			if ($AddressList[$j] == ",") $AddressList[$j] = "|";
		}
	}
	
	$Result = explode(",",$AddressList);
	
	for ($i=0; $i< sizeof($Result); $i++) {
		$Result[$i] = str_replace("|",",",$Result[$i]);				
	}
	
	return implode(";",$Result);
}
?>
