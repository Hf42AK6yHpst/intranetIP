<?php
// modifying by:
/**
 * ******************
 * Change Log :
 * Date 2018-09-03 [Pun]
 * File Created
 *
 * ******************
 */
// error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);ini_set('display_errors', 1);
include_once("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/DynamicAdmissionFormSystem/DynamicAdmissionFormSystem.php");

use \AdmissionSystem\DynamicAdmissionFormSystem;

class admission_cust extends \AdmissionSystem\AdmissionCustBase
{

    const STUDENT_ACADEMIC_SCHOOL_COUNT = 1;

    const RELATIVES_COUNT = 2;

    public function __construct()
    {
        global $plugin;

        if ($plugin['eAdmission_devMode']) {
            error_reporting(E_ALL & ~E_NOTICE);
            ini_set('display_errors', 1);
            if ($_SERVER['HTTP_HOST'] == '192.168.0.171:31002') {
//                 $this->AdmissionFormSendEmail = false;
            }
        }

        parent::__construct();
        $this->init();
    }

    private function init()
    {
        /* #### FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER, array(
            $this,
            'genNextApplicationNumberFormat'
        ));
        /* #### FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER END #### */

        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function ($emailTitle, $applicationId) {
            global $admission_cfg;
            $result = $this->getPaymentResult('', '', '', '', $applicationId);

            $hasPaid = 0;
            if ($result) {
                foreach ($result as $aResult) {
                    if ($aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']) {
                        $hasPaid = 1;
                        break;
                    }
                }
            }
            if ($hasPaid) {
                return "新生入學申請 - 確認通知 (申請編號：{$applicationId}) New applications – confirmation notice (Application #: {$applicationId})";
            }
            return "付款查核 Verifying your payment";
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_TITLE END #### */

        /* #### FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC START #### */
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC, (function ($emailBcc, $applicationId) {
            global $admission_cfg;
            return array($admission_cfg['EmailBcc']);
        }));
        /* #### FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummySchoolInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummyRelativesInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array(
            $this,
            'insertDummyCustInfo'
        ));
        /* #### ACTION_APPLICANT_INSERT_DUMMY_INFO END #### */

        /* #### ACTION_APPLICANT_UPDATE_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array(
            $this,
            'updateDynamicField'
        ));
        /* #### ACTION_APPLICANT_UPDATE_INFO END #### */

        /* #### ACTION_APPLICANT_UPDATE_*_INFO START #### */
        $this->addAction(self::ACTION_APPLICANT_INFO, array(
            $this,
            'updateApplicantInfo'
        ));
        /* #### ACTION_APPLICANT_UPDATE_*_INFO END #### */

        /* #### FILTER_EXPORT_INTERVIEW_LIST START #### */
        $this->addFilter(self::FILTER_EXPORT_INTERVIEW_LIST, (function ($dataArray) {
            usort($dataArray, array($this, 'sortExportInterviewList'));
            return $dataArray;
        }));
        /* #### FILTER_EXPORT_INTERVIEW_LIST END #### */

        /* #### FILTER_EXPORT_APPLICANT_LIST START #### */
        $this->addFilter(self::FILTER_EXPORT_APPLICANT_LIST, array(
            $this,
            'filterExportApplicantList'
        ));
        /* #### FILTER_EXPORT_APPLICANT_LIST END #### */
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID)
    {
        $result = true;

        for ($i = 0; $i < self::STUDENT_ACADEMIC_SCHOOL_COUNT; $i++) {
            $sql = "INSERT INTO
                ADMISSION_STU_PREV_SCHOOL_INFO
            (
                ApplicationID,
                SchoolOrder,
                DateInput,
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";

            $result = $result && $this->db_db_query($sql);
        }

        if (!$result) {
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummyRelativesInfo($ApplicationID)
    {
        $result = true;

        for ($i = 0; $i < self::RELATIVES_COUNT; $i++) {
            $sql = "INSERT INTO
                ADMISSION_SIBLING
            (
                ApplicationID,
                SiblingOrder,
                DateInput,
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";
            $result = $result && $this->db_db_query($sql);
        }

        if (!$result) {
            throw new \Exception('Cannot insert dummy relatives info');
        }
        return true;
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummyCustInfo($ApplicationID)
    {
        global $admission_cfg;
        $result = $this->insertApplicationCustInfo(array('Code' => 'Remarks'), $ApplicationID);
        $result = $result && $this->insertApplicationCustInfo(array(
                'Code' => 'AdmitStatus',
                'Value' => $admission_cfg['AdmitStatus']['NotUpdated']
            ), $ApplicationID);
        $result = $result && $this->insertApplicationCustInfo(array(
                'Code' => 'InterviewStatus',
                'Value' => $admission_cfg['InterviewStatus']['NotUpdated']
            ), $ApplicationID);

        if (!$result) {
            throw new \Exception('Cannot insert dummy cust info');
        }
        return true;
    }

    public function genNextApplicationNumberFormat($nextApplicationId, $schoolYearID)
    {
        global $admission_cfg;
        $yearStart = date('Y', getStartOfAcademicYear('', $this->schoolYearID));

        #### Get next count START ####
        $sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '{$admission_cfg['SchoolCode']}{$yearStart}%' ORDER BY RecordID DESC LIMIT 1";
        $rs = $this->returnVector($sql);

        if (count($rs)) {
            $idArr = explode('-', $rs[0]);
            $lastCount = (int)$idArr[3];
        } else {
            $lastCount = 0;
        }
        $nextCount = str_pad($lastCount + 1, 4, "0", STR_PAD_LEFT);
        #### Get next count END ####

        #### Get data START ####
        $classLevelName = $this->classLevelAry[$_REQUEST['sus_status']];

        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        $fields = $dafs->getLastActiveAcademicYearFields();
        foreach ($fields as $field) {
            $table = $field['OtherAttributeArr']['DbTableName'];
            $_field = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
            $fieldName = $_field[0];

            if ($table == 'ADMISSION_STU_INFO' && $fieldName == 'DOB') {
                $dobYear = $_REQUEST["field_{$field['FieldID']}_year"];
                $dobMonth = str_pad($_REQUEST["field_{$field['FieldID']}_month"], 2, '0', STR_PAD_LEFT);
                $dobDay = str_pad($_REQUEST["field_{$field['FieldID']}_day"], 2, '0', STR_PAD_LEFT);
                $dob = "{$dobYear}-{$dobMonth}-{$dobDay}";
                break;
            }
        }

        $schoolStartDate = "{$yearStart}-09-01";
        $datetime1 = new DateTime($dob);
        $datetime2 = new DateTime($schoolStartDate);
        $interval = $datetime1->diff($datetime2);
        $diffYear = $interval->format('%y');
        $diffMonth = $interval->format('%m');
        $diffDay = $interval->format('%d');

        if ($classLevelName == 'K1') {
            $diffYear -= 1;
        } elseif ($classLevelName == 'K2') {
            $diffYear -= 2;
        } elseif ($classLevelName == 'K3') {
            $diffYear -= 3;
        }

        if (($diffYear >= 3) || ($diffYear >= 2 && $diffMonth > 8) || ($diffYear == 2 && $diffMonth == 8 && $diffDay > 0)) {
            $dobCode = 'S';
        } elseif (($diffYear < 2) || ($diffYear == 2 && $diffMonth == 0 && $diffDay == 0)) {
            $dobCode = 'U';
        } else {
            $dobCode = 'N';
        }
        #### Get data END ####

//         debug_r($dob);
//         debug_r($dobCode);
//         debug_r($interval->format('Diff: %y年%m月%d日'));
//         exit;
        $nextApplicationNumber = "{$admission_cfg['SchoolCode']}{$yearStart}-{$classLevelName}-{$dobCode}{$dobMonth}-{$nextCount}";
        return $nextApplicationNumber;
    }

    protected function updateDynamicField($Data, $ApplicationID, $groupId = null)
    {
        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        if ($groupId) {
            $fields = $dafs->getFieldsByGroupId($groupId, 1);
        } else {
            $fields = $dafs->getLastActiveAcademicYearFields();
        }
        $result = true;

        $updateArr = array();
        foreach ($fields as $field) {
            $value = $Data["field_" . $field['FieldID']];
            $table = $field['OtherAttributeArr']['DbTableName'];
            $_field = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
            $fieldName = $_field[0];
            $sqlWhere = $field['ExtraArr']['sqlWhere'];

            if (!$table || !$fieldName || $field['ExtraArr']['skipSaveDb']) {
                continue;
            }

            #### Save sql START ####
            switch ($field['Type']) {
                case DynamicAdmissionFormSystem::FIELD_TYPE_TEXT:
                case DynamicAdmissionFormSystem::FIELD_TYPE_TEXTAREA:
                case DynamicAdmissionFormSystem::FIELD_TYPE_RADIO:
                case DynamicAdmissionFormSystem::FIELD_TYPE_CHECKBOX:
                case DynamicAdmissionFormSystem::FIELD_TYPE_SELECT:
                    $value = intranet_htmlspecialchars($value);
                    break;
                case DynamicAdmissionFormSystem::FIELD_TYPE_DATE:
                    $year = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_year"]), 2, '0', STR_PAD_LEFT);
                    $month = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_month"]), 2, '0', STR_PAD_LEFT);
                    $day = str_pad(IntegerSafe($Data["field_{$field['FieldID']}_day"]), 2, '0', STR_PAD_LEFT);
                    $value = "{$year}-{$month}-{$day}";
                    break;
                case DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR:
                    $value = array_values(array_filter((array)$value));
                    for ($i = 0; $i < 3; $i++) {
                        $v = IntegerSafe($value[$i]);
                        $fieldName = 'ApplyDayType' . ($i + 1);
                        $updateArr[$table][$sqlWhere][] = "{$fieldName}='{$v}'";
                    }
                    continue 2; // Skip default add sql logic
                case DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY:
                    continue 2;
                case DynamicAdmissionFormSystem::FIELD_TYPE_CUSTOM_FIELD:
                    $_fieldName = $field['ExtraArr']['field'];
                    switch ($_fieldName) {
                        case 'brother_sister_1_schoolDuration':
                        case 'brother_sister_2_schoolDuration':
                        case 'previousSchoolYear':
                            $start = IntegerSafe($Data["field_{$field['FieldID']}_start"]);
                            $end = IntegerSafe($Data["field_{$field['FieldID']}_end"]);
                            $value = "{$start} - {$end}";
                            break;
                        case 'previousSchoolClass':
                            $start = intranet_htmlspecialchars($Data["field_{$field['FieldID']}_start"]);
                            $end = intranet_htmlspecialchars($Data["field_{$field['FieldID']}_end"]);
                            $value = "{$start} - {$end}";
                            break;
                    }
                    break;

            }
            #### Save sql END ####

            $updateArr[$table][$sqlWhere][] = "{$fieldName}='{$value}'";
        }

        foreach ($updateArr as $table => $d1) {
            foreach ($d1 as $cond => $updateFields) {
                $updateFieldSql = implode(',', $updateFields);
                $sql = "UPDATE
                    {$table}
                SET
                    {$updateFieldSql}
                WHERE
                   ApplicationID='{$ApplicationID}'
                ";
                if ($cond) {
                    $sql .= " AND {$cond}";
                }
                $result = $result && $this->db_db_query($sql);
            }
        }
        return $result;
    }

    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID)
    {
        $result = true;

        if ($type == 'remarks') {
            $result = $this->updateApplicationStatus($Data, $ApplicationID);
        } elseif ($type == 'creative_interview_follow_up') {
            $result = $this->updateApplicationInterviewFollowUp($Data, $ApplicationID);
        } else {
            $typeArr = explode('_', $type);
            $result = $this->updateDynamicField($Data, $ApplicationID, $typeArr[1]);
        }

        if (!$result) {
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }

    function updateApplicationStatusByIds($applicationIds, $status)
    {
        $result = true;
        if (isset($_REQUEST['status']) && $_REQUEST['status']) {
            $result = parent::updateApplicationStatusByIds($applicationIds, $status);
        }

        $sql = "SELECT ApplicationID  FROM ADMISSION_OTHERS_INFO WHERE RecordID IN (" . $applicationIds . ")";
        $ApplicationIdArr = $this->returnVector($sql);

        $Data = array();
        if (isset($_REQUEST['applicantInterviewStatus'])) {
            $Data['applicantInterviewStatus'] = $_REQUEST['applicantInterviewStatus'];
        }
        if (isset($_REQUEST['RegisterEmail'])) {
            $Data['RegisterEmail'] = $_REQUEST['RegisterEmail'];
            $Data['RegisterEmailDate_year'] = date('Y');
            $Data['RegisterEmailDate_month'] = date('m');
            $Data['RegisterEmailDate_day'] = date('d');
        }
        if (isset($_REQUEST['AdmitStatus'])) {
            $Data['AdmitStatus'] = $_REQUEST['AdmitStatus'];
        }
        if (isset($_REQUEST['RatingClass'])) {
            $Data['RatingClass'] = $_REQUEST['RatingClass'];
        }
        if (isset($_REQUEST['PaidReservedFee'])) {
            $Data['PaidReservedFee'] = $_REQUEST['PaidReservedFee'];
        }
        if (isset($_REQUEST['PaidRegistrationCertificate'])) {
            $Data['PaidRegistrationCertificate'] = $_REQUEST['PaidRegistrationCertificate'];
        }
        if (isset($_REQUEST['BookFeeEmail'])) {
            $Data['BookFeeEmail'] = $_REQUEST['BookFeeEmail'];
            $Data['BookFeeEmailDate_year'] = date('Y');
            $Data['BookFeeEmailDate_month'] = date('m');
            $Data['BookFeeEmailDate_day'] = date('d');
        }
        if (isset($_REQUEST['PaidBookFee'])) {
            $Data['PaidBookFee'] = $_REQUEST['PaidBookFee'];
        }
        if (isset($_REQUEST['OpeningNoticeEmail'])) {
            $Data['OpeningNoticeEmail'] = $_REQUEST['OpeningNoticeEmail'];
            $Data['OpeningNoticeEmailDate_year'] = date('Y');
            $Data['OpeningNoticeEmailDate_month'] = date('m');
            $Data['OpeningNoticeEmailDate_day'] = date('d');
        }
        if (isset($_REQUEST['PaidTuitionFee'])) {
            $Data['PaidTuitionFee'] = $_REQUEST['PaidTuitionFee'];
        }
        if (isset($_REQUEST['GiveUpOffer'])) {
            $Data['GiveUpOffer'] = $_REQUEST['GiveUpOffer'];
        }
        foreach ((array)$ApplicationIdArr as $applicationId) {
            $result = $result && $this->updateApplicationInterviewFollowUp($Data, $applicationId);
        }

        return $result;
    }

    /**
     * Portal - update interview follow up
     */
    function updateApplicationInterviewFollowUp($Data, $ApplicationID)
    {
        extract($Data);
        $RegisterEmailDate_month = str_pad($RegisterEmailDate_month, 2, '0', STR_PAD_LEFT);
        $RegisterEmailDate_day = str_pad($RegisterEmailDate_day, 2, '0', STR_PAD_LEFT);
        $BookFeeEmailDate_month = str_pad($BookFeeEmailDate_month, 2, '0', STR_PAD_LEFT);
        $BookFeeEmailDate_day = str_pad($BookFeeEmailDate_day, 2, '0', STR_PAD_LEFT);
        $OpeningNoticeEmailDate_month = str_pad($OpeningNoticeEmailDate_month, 2, '0', STR_PAD_LEFT);
        $OpeningNoticeEmailDate_day = str_pad($OpeningNoticeEmailDate_day, 2, '0', STR_PAD_LEFT);

        $result = array();
        $updateArr = array();

        if (isset($Data['applicantInterviewStatus'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'InterviewStatus',
                'Value' => $applicantInterviewStatus,
            );
        }
        if (isset($Data['RegisterEmail'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'RegisterEmail',
                'Value' => $RegisterEmail,
            );

            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'RegisterEmailDate',
                'Value' => "{$RegisterEmailDate_year}-{$RegisterEmailDate_month}-{$RegisterEmailDate_day}",
            );
        }
        if (isset($Data['AdmitStatus'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'AdmitStatus',
                'Value' => $AdmitStatus,
            );
        }
        if (isset($Data['RatingClass'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'RatingClass',
                'Value' => $RatingClass,
            );
        }
        if (isset($Data['PaidReservedFee'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidReservedFee',
                'Value' => $PaidReservedFee,
            );
        }
        if (isset($Data['PaidRegistrationCertificate'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidRegistrationCertificate',
                'Value' => $PaidRegistrationCertificate,
            );
        }
        if (isset($Data['BookFeeEmail'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'BookFeeEmail',
                'Value' => $BookFeeEmail,
            );
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'BookFeeEmailDate',
                'Value' => "{$BookFeeEmailDate_year}-{$BookFeeEmailDate_month}-{$BookFeeEmailDate_day}",
            );
        }
        if (isset($Data['PaidBookFee'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidBookFee',
                'Value' => $PaidBookFee,
            );
        }
        if (isset($Data['OpeningNoticeEmail'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'OpeningNoticeEmail',
                'Value' => $OpeningNoticeEmail,
            );
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'OpeningNoticeEmailDate',
                'Value' => "{$OpeningNoticeEmailDate_year}-{$OpeningNoticeEmailDate_month}-{$OpeningNoticeEmailDate_day}",
            );
        }
        if (isset($Data['PaidTuitionFee'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'PaidTuitionFee',
                'Value' => $PaidTuitionFee,
            );
        }
        if (isset($Data['GiveUpOffer'])) {
            $updateArr[] = array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'GiveUpOffer',
                'Value' => $GiveUpOffer,
            );
        }

        foreach ($updateArr as $data) {
            $result[] = $this->updateApplicationCustInfo($data);
        }

        return (!in_array(false, $result, true));
    }

    function getApplicationDetails($schoolYearID, $data = array())
    {
        global $admission_cfg;
        extract($data);

        $sort = $sortby ? "$sortby $order" : "application_id";
        $limit = $page ? " LIMIT " . (($page - 1) * $amount) . ", $amount" : "";
        $cond = !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= $status ? " AND s.Status='" . $status . "'" : "";
        if ($status == $admission_cfg['Status']['waitingforinterview']) {
            if ($interviewStatus == 1) {
                $cond .= " AND s.isNotified = 1";
            } else {
                $cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
            }
        }

        if ($paymentStatus == $admission_cfg['PaymentStatus']['OnlinePayment']) {
            $paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
            $cond .= " AND pi.ApplicationID IS NOT NULL  ";
        } else if ($paymentStatus == $admission_cfg['PaymentStatus']['OtherPayment']) {
            $paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
            $cond .= " AND pi.ApplicationID IS NULL  ";
        }

        if (!empty($keyword)) {
            $cond .= "
				AND (
                    stu.EnglishName LIKE '%" . $keyword . "%'
                    OR stu.ChineseName LIKE '%" . $keyword . "%'
                    OR stu.ApplicationID LIKE '%" . $keyword . "%'
                    OR pg.EnglishName LIKE '%" . $keyword . "%'
                    OR pg.ChineseName LIKE '%" . $keyword . "%'
                    OR pg.Mobile LIKE '%" . $keyword . "%'
                    OR stu.BirthCertNo LIKE '%" . $keyword . "%'
                    OR stu.HomeTelNo LIKE '%" . $keyword . "%'
				)
			";
        }

        $from_table = "
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			$paymentStatus_cond
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'
			" . $cond . "

    	";
        $sql = "SELECT DISTINCT o.ApplicationID " . $from_table;
        $applicationIDAry = $this->returnVector($sql);
        $applicationIdSql = implode("','", $applicationIDAry);

        //// Filter cust info START ////
        $sql = "SELECT
                ApplicationID,
                Code,
                Value
            FROM
                ADMISSION_CUST_INFO aci
            WHERE
                ApplicationID IN ('{$applicationIdSql}')
            ";
        $rs = $this->returnResultSet($sql);
        $applicationStatusArr = BuildMultiKeyAssoc($rs, array('ApplicationID', 'Code'), array('Value'), true);

        $applicationIDAry = array();
        foreach ($applicationStatusArr as $ApplicationID => $applicationStatus) {
            $isValid = true;

            if (isset($_GET['applicantInterviewStatus']) && $_GET['applicantInterviewStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['InterviewStatus'] == $_GET['applicantInterviewStatus']);
            }
            if (isset($_GET['RegisterEmail']) && $_GET['RegisterEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['RegisterEmail'] == $_GET['RegisterEmail']);
            }
            if (isset($_GET['AdmitStatus']) && $_GET['AdmitStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['AdmitStatus'] == $_GET['AdmitStatus']);
            }
            if (isset($_GET['RatingClass']) && $_GET['RatingClass'] !== '') {
                $isValid = $isValid && ($applicationStatus['RatingClass'] == $_GET['RatingClass']);
            }
            if (isset($_GET['PaidReservedFee']) && $_GET['PaidReservedFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidReservedFee'] == $_GET['PaidReservedFee']);
            }
            if (isset($_GET['PaidRegistrationCertificate']) && $_GET['PaidRegistrationCertificate'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidRegistrationCertificate'] == $_GET['PaidRegistrationCertificate']);
            }
            if (isset($_GET['BookFeeEmail']) && $_GET['BookFeeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['BookFeeEmail'] == $_GET['BookFeeEmail']);
            }
            if (isset($_GET['PaidBookFee']) && $_GET['PaidBookFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidBookFee'] == $_GET['PaidBookFee']);
            }
            if (isset($_GET['OpeningNoticeEmail']) && $_GET['OpeningNoticeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['OpeningNoticeEmail'] == $_GET['OpeningNoticeEmail']);
            }
            if (isset($_GET['PaidTuitionFee']) && $_GET['PaidTuitionFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidTuitionFee'] == $_GET['PaidTuitionFee']);
            }
            if (isset($_GET['GiveUpOffer']) && $_GET['GiveUpOffer'] !== '') {
                $isValid = $isValid && ($applicationStatus['GiveUpOffer'] == $_GET['GiveUpOffer']);
            }
            if ($isValid) {
                $applicationIDAry[] = $ApplicationID;
            }
        }
        $applicationIdSql = implode("','", $applicationIDAry);
        //// Filter cust info END ////


//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
        $sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			" . getNameFieldByLang2("stu.") . " AS student_name,
     			" . getNameFieldByLang2("pg.") . " AS parent_name,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				stu.BirthCertNo AS student_hkid,
				CASE
     	";
        FOREACH ($admission_cfg['Status'] as $_key => $_status) {//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status, s.DateInput	" . $from_table . " AND o.ApplicationID IN ('{$applicationIdSql}') ORDER BY $sort, FIELD(pg.PG_TYPE, 'F', 'M', 'G') ";

        $applicationAry = $this->returnArray($sql);

        //// Cust field START ////
        for ($i = 0, $iMax = count($applicationAry); $i < $iMax; $i++) {
            $application_id = $applicationAry[$i]['application_id'];
            $applicationAry[$i]['Cust_RatingClass'] = $applicationStatusArr[$application_id]['RatingClass'];
            $applicationAry[$i]['Cust_InterviewStatus'] = $applicationStatusArr[$application_id]['InterviewStatus'];
            $applicationAry[$i]['Cust_AdmitStatus'] = $applicationStatusArr[$application_id]['AdmitStatus'];
        }
        //// Cust field END ////

        return array(count($applicationIDAry), $applicationAry);
    }

    function getInterviewListAry($recordID = '', $date = '', $startTime = '', $endTime = '', $keyword = '', $order = '', $sortby = '', $round = 1)
    {
        global $admission_cfg;

        //$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

        if ($recordID != '') {
            $cond = " AND i.RecordID IN ('" . implode("','", (array)($recordID)) . "') ";
        }
        if ($date != '') {
            $cond .= ' AND i.Date >= \'' . $date . '\' ';
        }
        if ($startTime != '') {
            $cond .= ' AND i.StartTime >= \'' . $startTime . '\' ';
        }
        if ($endTime != '') {
            $cond .= ' AND i.EndTime <= \'' . $endTime . '\' ';
        }
        if ($keyword != '') {
            $search_cond = ' AND (i.Date LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.StartTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.EndTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\')';
        }
        $sort = $sortby ? "$sortby $order" : "application_id";

//		if(!empty($keyword)){
//			$cond .= "
//				AND (
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'
//				)
//			";
//		}

        $from_table = "
			FROM
				ADMISSION_INTERVIEW_SETTING AS i
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID" . ($round > 1 ? $round : '') . "
			INNER JOIN
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			WHERE
				1
			" . $cond . "
			order by " . $sort . "
    	";
        $sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			" . getNameFieldByLang2("stu.") . " AS student_name,
     			" . getNameFieldByLang2("pg.") . " AS parent_name,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE
     	";
        FOREACH ($admission_cfg['Status'] as $_key => $_status) {//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status	" . $from_table . " ";
        //debug_r($sql);
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function sortExportInterviewList($a, $b)
    {
        if ($a[1] != $b[1]) {
            return strcmp($a[1], $b[1]);
        } elseif ($a[2] != $b[2]) {
            return strcmp($a[2], $b[2]);
        } elseif ($a[3] != $b[3]) {
            return strcmp($a[3], $b[3]);
        } elseif ($a[4] != $b[4]) {
            return strcmp($a[4], $b[4]);
        }

        $aApplicationIdArr = explode('-', $a[6]);
        $bApplicationIdArr = explode('-', $b[6]);
        $aCode = str_replace(array('S', 'N', 'U'), array('1', '2', '3'), $aApplicationIdArr[2]);
        $bCode = str_replace(array('S', 'N', 'U'), array('1', '2', '3'), $bApplicationIdArr[2]);
        return strcmp($aCode, $bCode);
    }

    function getExportInterviewResultHeaderData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $selectStatus = '', $selectInterviewStatus, $selectInterviewRound = '', $selectFormQuestionID = '', $selectFormAnswerID = '', $interviewDate = '')
    {
        global $kis_lang;
        list($header, $data) = parent::getExportInterviewResultHeaderData($schoolYearID, $classLevelID, $applicationID, $recordID, $selectStatus, $selectInterviewStatus, $selectInterviewRound, $selectFormQuestionID, $selectFormAnswerID, $interviewDate);

        array_unshift($header[0], '');
        array_splice($header[1], 3, 0, $kis_lang['Admission']['CREATIVE']['applyFor']);

        $applicationIdArr = array_map('trim', Get_Array_By_Key($data, 1));
        $applicationIdSql = implode("','", $applicationIdArr);
        $sql = "SELECT
            ApplicationID,
            ApplyDayType1,
            ApplyDayType2,
            ApplyDayType3
        FROM
            ADMISSION_OTHERS_INFO
        WHERE
            ApplicationID IN ('{$applicationIdSql}')";
        $rs = $this->returnResultSet($sql);
        $applyDayTypeArr = BuildMultiKeyAssoc($rs, array('ApplicationID'));

        for ($i = 0, $iMax = count($data); $i < $iMax; $i++) {
            $applicationId = $data[$i][1] = trim($data[$i][1]);
            $applyDayTypeStr1 = $kis_lang['Admission']['TimeSlot'][$applyDayTypeArr[$applicationId]['ApplyDayType1']];
            $applyDayTypeStr1 = $applyDayTypeStr1 ? $applyDayTypeStr1 : '--';
            $applyDayTypeStr2 = $kis_lang['Admission']['TimeSlot'][$applyDayTypeArr[$applicationId]['ApplyDayType2']];
            $applyDayTypeStr2 = $applyDayTypeStr2 ? $applyDayTypeStr2 : '--';
            $applyDayTypeStr3 = $kis_lang['Admission']['TimeSlot'][$applyDayTypeArr[$applicationId]['ApplyDayType3']];
            $applyDayTypeStr3 = $applyDayTypeStr3 ? $applyDayTypeStr3 : '--';
            $applyDayTypeStr = "1){$applyDayTypeStr1}";
            $applyDayTypeStr .= " 2){$applyDayTypeStr2}";
            $applyDayTypeStr .= " 3){$applyDayTypeStr3}";
            array_splice($data[$i], 3, 0, $applyDayTypeStr);
        }

        return array($header, $data);
    }

    function hasApplicationSetting()
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '" . $this->schoolYearID . "' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
        return current($this->returnVector($sql));
    }

    function checkImportDataForImportAdmissionHeader($csv_header, $lang = '')
    {
        //$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
        $file_format = $this->getExportHeaderEDB();
        $file_format = $file_format[1];
        # check csv header
        $format_wrong = false;

        for ($i = 0; $i < sizeof($file_format); $i++) {
            if ($csv_header[$i] != $file_format[$i]) {
                $format_wrong = true;
                break;
            }
        }

        return $format_wrong;
    }

    public function returnPresetCodeAndNameArr($code_type = "")
    {
        $sql = "select Code, " . Get_Lang_Selection("NameChi", "NameEng") . "  from PRESET_CODE_OPTION where CodeType='" . $code_type . "'";
        $result = $this->returnArray($sql);
        return $result;
    }

    function checkImportDataForImportAdmission($data)
    {
        global $kis_lang, $admission_cfg;
        $resultArr = array();
        $i = 0;

        #### Get duplcate application id START ####
        $ids = array();
        foreach ($data as $aData) {
            $ids[] = trim($aData[0]);
        }
        $ids = array_filter($ids);

        $idSql = implode("','", $ids);
        $sql = "SELECT
		    ASI.ApplicationID,
		    ASI.BirthCertNo
		FROM
		    ADMISSION_STU_INFO ASI
		INNER JOIN
		    ADMISSION_OTHERS_INFO AOI
	    ON
	        ASI.ApplicationID = AOI.ApplicationID
        AND
            AOI.ApplyYear = '" . $this->getNextSchoolYearID() . "'
		WHERE
		    ASI.ApplicationID IN ('{$idSql}')
		";
        $rs = $this->returnResultSet($sql);

        $currentApplicationIdArr = Get_Array_By_Key($rs, 'ApplicationID');
        $applicationIdBirthCertMapping = BuildMultiKeyAssoc($rs, array('ApplicationID'), array('BirthCertNo'), $SingleValue = 1);
        #### Get duplcate application id END ####

        foreach ($data as $aData) {
            //valid ApplicationID
            if ($aData[0]) {
                $resultArr[$i]['validApplicationID'] = in_array($aData[0], $currentApplicationIdArr);
            } else {
                $resultArr[$i]['validApplicationID'] = true;
            }

            //valid E.Name
            $aData[1] = str_replace(',', '&#44;', $aData[1]);
            $resultArr[$i]['validEName'] = true;
            if ($aData[1] == '') {
                $resultArr[$i]['validEName'] = false;
            }

            //valid C.Name
            $aData[2] = str_replace(',', '&#44;', $aData[2]);
            $resultArr[$i]['validCName'] = true;
            if ($aData[2] == '') {
                $resultArr[$i]['validCName'] = false;
            }

            //valid Birth Date
            $aData[3] = getDefaultDateFormat($aData[3]);

            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[3])) {
                list($year, $month, $day) = explode('-', $aData[3]);
                $resultArr[$i]['validBirthDate'] = checkdate($month, $day, $year);
            } else {
                $resultArr[$i]['validBirthDate'] = false;
            }

            //valid Gender
            if (strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['M'])) {
                $aData[4] = 'M';
                $data[$i][4] = 'M';
            } else if (strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['F'])) {
                $aData[4] = 'F';
                $data[$i][4] = 'F';
            }
            if (strtoupper($aData[4]) == 'M' || strtoupper($aData[4]) == 'F') {

                $resultArr[$i]['validGender'] = true;
            } else {
                $resultArr[$i]['validGender'] = false;
            }

            //valid birth cert
            $aData[5] = str_replace(array('(', ')'), array('', ''), $aData[5]);
            $resultArr[$i]['validBirthCertNoFormat'] = true;
            if (!preg_match('/^[a-zA-Z][0-9A]{7}$/', $aData[5])) {
                $resultArr[$i]['validBirthCertNoFormat'] = false;
            }

            $resultArr[$i]['validBirthCertNoDuplicate'] = true;
            if (
                $aData[5] != $applicationIdBirthCertMapping[$aData[0]] &&
                $this->hasBirthCertNumber($aData[5], '')
            ) {
                $resultArr[$i]['validBirthCertNoDuplicate'] = false;
            }

            //valid STRN
            $resultArr[$i]['validSTRN'] = true;
            /*if($aData[6] == ''){
                $resultArr[$i]['validSTRN'] = false;
            }*/

            //valid Current School
            $resultArr[$i]['validPrimarySchool'] = true;
            if ($aData[7] == '') {
                $resultArr[$i]['validPrimarySchool'] = false;
            }

            //valid Parent/Guardian
            $resultArr[$i]['validParentGuardian'] = true;
            /*if($aData[8] == ''){
                $resultArr[$i]['validParentGuardian'] = false;
            }*/

            //valid email address
            $resultArr[$i]['validEmail'] = true;
            if ($aData[9] && !preg_match('/\S+@\S+\.\S+/', $aData[9])) {
                $resultArr[$i]['validEmail'] = false;
            }

            $resultArr[$i]['validData'] = $aData[1];
            $i++;
        }
        $result = $resultArr;

        //for printing the error message
        $errCount = 0;

        $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Admission']['englishname'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
        $i = 1;
        foreach ($result as $aResult) {
            //developing
            $hasError = false;
            $errorMag = '';
            if (!$aResult['validApplicationID']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
            } elseif (!$aResult['validEName']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['msg']['enterenglishname'];
            } elseif (!$aResult['validCName']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['msg']['enterchinesename'];
            } elseif (!$aResult['validBirthDate']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['HKUGAPS']['msg']['importInvalidDateOfBirth'];
            } elseif (!$aResult['validGender']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidWordOfGender'];
            } elseif (!$aResult['validBirthCertNoFormat']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertNo'];
            } elseif (!$aResult['validBirthCertNoDuplicate']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['duplicateHKID'];
            } elseif (!$aResult['validSTRN']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterSTRN'];
            } elseif (!$aResult['validPrimarySchool']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterCurrentSchool'];
            } elseif (!$aResult['validParentGuardian']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['UCCKE']['msg']['enterParentName'];
            } elseif (!$aResult['validEmail']) {
                $hasError = true;
                $errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress'];
            }

            //print the error msg to the client
            if ($hasError) {
                $errCount++;
                $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validData'] . '</td>
					<td><font color="red">';
                $x .= $errorMag;
                $x .= '</font></td></tr>';
            }

            $i++;
        }
        $x .= '</tbody></table>';
        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
    }

    function importDataForImportAdmission($data)
    {
        global $kis_lang, $admission_cfg, $classLevelID;
        $resultArr = array();
        array_shift($data);

        $_REQUEST['sus_status'] = $classLevelID;
        foreach ($data as $aData) {
            global $UserID;


            $applicationNumber = ($aData[0]) ? $aData[0] : $this->newApplicationNumber2();

            //--- convert the text to key code [start]
            if (strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['M'])) {
                $aData[4] = 'M';
            } else if (strtoupper($aData[4]) == strtoupper($kis_lang['Admission']['genderType']['F'])) {
                $aData[4] = 'F';
            }
            $aData[5] = str_replace(array('(', ')'), array('', ''), $aData[5]);

            $data = array(
                'student_name_en' => addslashes($aData[1]),
                'student_name_b5' => addslashes($aData[2]),
                'dateofbirth' => getDefaultDateFormat($aData[3]),
                'gender' => addslashes($aData[4]),
                'StudentBirthCertNo' => addslashes($aData[5]),
                'BdRefNo' => addslashes($aData[6]),
                'LastSchool' => addslashes($aData[7]),
                'ParentName' => addslashes($aData[8]),
                'ParentEmail' => addslashes($aData[9]),
            );

            $result = $this->insertApplicationAllInfo($this, $data, $applicationNumber, !!($aData[0]));

            $resultArr[] = $result;
            //debug_pr($aData);
        }
        return $resultArr;
    }

    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '', $classLevelID = '')
    {
        global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;

        $headerArray = array();

        #### Get data START ####
        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        $groups = $dafs->getLastActiveAcademicYearGroups();

        $fields = array();
        foreach ($groups as $index => $group) {
            $_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
            $fields[$group['GroupID']] = array();

            foreach ($_fields as $field) {
                if ($field['Type'] != DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) {
                    $fields[$group['GroupID']][] = $field;
                }
            }
        }
        #### Get data END ####

        #### First row START ####
        $column = array();
        $column[] = "";
        $column[] = "";
        $column[] = "";

        foreach ($groups as $group) {
            $column[] = Get_Lang_Selection($group['TitleB5'], $group['TitleEn']);
            for ($i = 0, $iMax = count($fields[$group['GroupID']]) - 1; $i < $iMax; $i++) {
                $column[] = "";
            }
        }
        $column[] = $kis_lang['creative_interview_follow_up'];
        for ($i = 0; $i < 10; $i++) {
            $column[] = "";
        }
        $column[] = $kis_lang['remarks'];
        $headerArray[] = $column;
        #### First row END ####

        #### Second row START ####
        $column = array();
        $column[] = $kis_lang['Admission']['admissiondate'];
        $column[] = $kis_lang['applicationno'];
        $column[] = $kis_lang['Admission']['applyLevel'];

        foreach ($groups as $group) {
            foreach ($fields[$group['GroupID']] as $field) {
                $column[] = Get_Lang_Selection($field['TitleB5'], $field['TitleEn']);
            }
        }

        ## Interview follow up START ##
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'];
        $column[] = $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'];
        ## Interview follow up END ##

        ## Remarks START ##
        $column[] = $kis_lang['Admission']['applicationstatus'];
        $column[] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
        $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['receiptcode'] . ")";
        $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['date'] . ")";
        $column[] = $kis_lang['Admission']['applicationfee'] . " (" . $kis_lang['Admission']['handler'] . ")";
	    $column[] = $kis_lang['Admission']['interviewdate'] . " (1)";
	    $column[] = $kis_lang['Admission']['interviewdate'] . " (2)";
	    $column[] = $kis_lang['Admission']['interviewdate'] . " (3)";
        $column[] = $kis_lang['Admission']['isnotified'];
        $column[] = $kis_lang['Admission']['otherremarks'];
        ## Remarks END ##

        $headerArray[] = $column;
        #### Second row END ####

        return $headerArray;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        #### Get data START ####
        ## ApplicationID START ##
        if (!$applicationID) {
            $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
            $applicationID = $otherInfo['ApplicationID'];
        }
        ## ApplicationID END ##

        $allCustInfo = $this->getAllApplicationCustInfo($applicationID);
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
        $classLevel = $this->getClassLevel();

        ## Dynamic fields START ##
        $dafs = new DynamicAdmissionFormSystem($this->schoolYearID);
        $groups = $dafs->getLastActiveAcademicYearGroups();

        $fields = array();
        $data = array();
        foreach ($groups as $index => $group) {
            $_fields = $dafs->getFieldsByGroupId($group['GroupID'], 1);
            foreach ($_fields as $field) {
                if ($field['Type'] != DynamicAdmissionFormSystem::FIELD_TYPE_DUMMY && !$field['ExtraArr']['skipExport']) {
                    $fields[$group['GroupID']][] = $field;
                }
            }
            $data[$group['GroupID']] = $dafs->getApplicationDataByGroupId($group['GroupID'], $applicationID);
        }
        ## Dynamic fields END ##
        #### Get data END ####

        #### Pack data START ####
        $row = array();

        $row[] = substr($otherInfo['DateInput'], 0, -9);
        $row[] = $otherInfo['applicationID'];
        $row[] = $classLevel[$otherInfo['classLevelID']];

        foreach ($fields as $groupId => $d1) {
            foreach ($d1 as $field) {
                $dbTableName = $field['OtherAttributeArr']['DbTableName'];
                $_dbFieldName = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
                $dbFieldName = $_dbFieldName[0];
                $value = '';
                if (!isset($data[$groupId][$dbTableName][$dbFieldName])) {
                    $sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
                    $sqlWhereField = $sqlWhere[0];
                    $sqlWhereValue = trim($sqlWhere[1], " '");

                    foreach ((array)$data[$groupId][$dbTableName] as $v) {
                        if (isset($v[$sqlWhereField]) && $v[$sqlWhereField] == $sqlWhereValue) {
                            $value = $v[$dbFieldName];
                            break;
                        }
                    }
                } else {
                    $value = $data[$groupId][$dbTableName][$dbFieldName];
                }

                switch ($field['Type']) {
                    case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR:
                        $value = "1){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType1']]}";
                        if ($data[$groupId][$dbTableName]['ApplyDayType2']) {
                            $value .= "\n2){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType2']]}";
                        } else {
                            $value .= "\n2) --";
                        }
                        if ($data[$groupId][$dbTableName]['ApplyDayType3']) {
                            $value .= "\n3){$kis_lang['Admission']['TimeSlot'][$data[$groupId][$dbTableName]['ApplyDayType3']]}";
                        } else {
                            $value .= "\n3) --";
                        }
                        break;
                    case \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_RADIO:
                        foreach ($field['ExtraArr']['options'] as $optionInfo) {
                            if ($value == $optionInfo[0]) {
                                $value = Get_Lang_Selection($optionInfo[1], $optionInfo[2]);
                                break;
                            }
                        }
                        break;
                }
                $row[] = $value;
            }
        }

	    // ######## Get interview START ########
	    $interviewInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
	    $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '');
	    $interviewSettings = BuildMultiKeyAssoc($rs, 'RecordID');

	    $interview1 = $interviewSettings[$interviewInfo['InterviewSettingID']];
	    $interview2 = $interviewSettings[$interviewInfo['InterviewSettingID2']];
	    $interview3 = $interviewSettings[$interviewInfo['InterviewSettingID3']];

	    $interviewDateTime1 = $interviewDateTime2 = $interviewDateTime3 = '';
	    if($interview1) {
		    $interviewDateTime1 = $interview1['Date'];
		    $interviewDateTime1 .= ' ' . substr( $interview1['StartTime'], 0, 5 );
		    $interviewDateTime1 .= ' - ' . substr( $interview1['EndTime'], 0, 5 );
	    }
	    if($interview2) {
		    $interviewDateTime2 = $interview2['Date'];
		    $interviewDateTime2 .= ' ' . substr( $interview2['StartTime'], 0, 5 );
		    $interviewDateTime2 .= ' - ' . substr( $interview2['EndTime'], 0, 5 );
	    }
	    if($interview3) {
		    $interviewDateTime3 = $interview3['Date'];
		    $interviewDateTime3 .= ' ' . substr( $interview3['StartTime'], 0, 5 );
		    $interviewDateTime3 .= ' - ' . substr( $interview3['EndTime'], 0, 5 );
	    }
	    // ######## Get interview END ########

        ## Interview follow up START ##
        $interviewStatusArr = array_flip($admission_cfg['InterviewStatus']);
        $interviewStatus = $allCustInfo['InterviewStatus'][0]['Value'];
        $interviewStatus = ($interviewStatus) ? $interviewStatus : $admission_cfg['InterviewStatus']['NotUpdated'];
        $interviewStatus = $interviewStatusArr[$interviewStatus];
        $row[] = $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$interviewStatus];

        if ($allCustInfo['RegisterEmail'][0]['Value']) {
            $row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['RegisterEmailDate'][0]['Value']})";
        } else {
            $row[] = $kis_lang['Admission']['no'];
        }

        $admitStatusArr = array_flip($admission_cfg['AdmitStatus']);
        $admitStatus = $allCustInfo['AdmitStatus'][0]['Value'];
        $admitStatus = ($admitStatus) ? $admitStatus : $admission_cfg['AdmitStatus']['NotUpdated'];
        $admitStatus = $admitStatusArr[$admitStatus];
        $row[] = $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$admitStatus];

        $row[] = $kis_lang['Admission']['TimeSlot'][$allCustInfo['RatingClass'][0]['Value']];
        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidReservedFee'][0]['Value']];
        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidRegistrationCertificate'][0]['Value']];

        if ($allCustInfo['BookFeeEmail'][0]['Value']) {
            $row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['BookFeeEmailDate'][0]['Value']})";
        } else {
            $row[] = $kis_lang['Admission']['no'];
        }

        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidBookFee'][0]['Value']];

        if ($allCustInfo['OpeningNoticeEmail'][0]['Value']) {
            $row[] = "{$kis_lang['Admission']['yes']} ({$allCustInfo['OpeningNoticeEmailDate'][0]['Value']})";
        } else {
            $row[] = $kis_lang['Admission']['no'];
        }

        $row[] = $kis_lang['Admission']['CREATIVE']['Paid'][$allCustInfo['PaidTuitionFee'][0]['Value']];
        $row[] = ($allCustInfo['GiveUpOffer'][0]['Value']) ? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
        ## Interview follow up END ##

        ## Remarks START ##
        $row[] = $kis_lang['Admission']['Status'][$status['status']];
        $row[] = $kis_lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
        $row[] = $status['receiptID'];
        $row[] = $status['receiptdate'];
        $row[] = $status['handler'];
//        $row[] = $status['interviewdate'];
//        $row[] = $status['interviewlocation'];
	    $row[] = $interviewDateTime1;
	    $row[] = $interviewDateTime2;
	    $row[] = $interviewDateTime3;
	    $row[] = $kis_lang['Admission'][ $status['isnotified'] ];
	    $row[] = $status['remark'];
        ## Remarks END ##
        #### Pack data END ####

        return $row;
    }

    public function filterExportApplicantList($rows)
    {
        $applicationIdArr = Get_Array_By_Key($rows, 1);
        $applicationIdSql = implode("','", $applicationIdArr);

        //// Filter cust info START ////
        $sql = "SELECT
                ApplicationID,
                Code,
                Value
            FROM
                ADMISSION_CUST_INFO aci
            WHERE
                ApplicationID IN ('{$applicationIdSql}')
            ";
        $rs = $this->returnResultSet($sql);
        $applicationStatusArr = BuildMultiKeyAssoc($rs, array('ApplicationID', 'Code'), array('Value'), true);

        $filterApplicationIDAry = array();
        foreach ($applicationStatusArr as $ApplicationID => $applicationStatus) {
            $isValid = true;

            if (isset($_POST['applicantInterviewStatus']) && $_POST['applicantInterviewStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['InterviewStatus'] == $_POST['applicantInterviewStatus']);
            }
            if (isset($_POST['RegisterEmail']) && $_POST['RegisterEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['RegisterEmail'] == $_POST['RegisterEmail']);
            }
            if (isset($_POST['AdmitStatus']) && $_POST['AdmitStatus'] !== '') {
                $isValid = $isValid && ($applicationStatus['AdmitStatus'] == $_POST['AdmitStatus']);
            }
            if (isset($_POST['RatingClass']) && $_POST['RatingClass'] !== '') {
                $isValid = $isValid && ($applicationStatus['RatingClass'] == $_POST['RatingClass']);
            }
            if (isset($_POST['PaidReservedFee']) && $_POST['PaidReservedFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidReservedFee'] == $_POST['PaidReservedFee']);
            }
            if (isset($_POST['PaidRegistrationCertificate']) && $_POST['PaidRegistrationCertificate'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidRegistrationCertificate'] == $_POST['PaidRegistrationCertificate']);
            }
            if (isset($_POST['BookFeeEmail']) && $_POST['BookFeeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['BookFeeEmail'] == $_POST['BookFeeEmail']);
            }
            if (isset($_POST['PaidBookFee']) && $_POST['PaidBookFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidBookFee'] == $_POST['PaidBookFee']);
            }
            if (isset($_POST['OpeningNoticeEmail']) && $_POST['OpeningNoticeEmail'] !== '') {
                $isValid = $isValid && ($applicationStatus['OpeningNoticeEmail'] == $_POST['OpeningNoticeEmail']);
            }
            if (isset($_POST['PaidTuitionFee']) && $_POST['PaidTuitionFee'] !== '') {
                $isValid = $isValid && ($applicationStatus['PaidTuitionFee'] == $_POST['PaidTuitionFee']);
            }
            if (isset($_POST['GiveUpOffer']) && $_POST['GiveUpOffer'] !== '') {
                $isValid = $isValid && ($applicationStatus['GiveUpOffer'] == $_POST['GiveUpOffer']);
            }
            if ($isValid) {
                $filterApplicationIDAry[] = $ApplicationID;
            }
        }
        //// Filter cust info END ////

        //// Set result START ////
        $resultRow = array();
        foreach ($rows as $row){
            if(in_array($row[1], $filterApplicationIDAry)){
                $resultRow[] = $row;
            }
        }
        //// Set result END ////

        return $resultRow;
    }

    /**
     * Portal - export applicant details header
     */
    public function getExportHeaderEDB($schoolYearID = '', $classLevelID = '')
    {
        global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;

        $exportColumn = array();

        $exportColumn[0][] = 'Application No.';
        $exportColumn[0][] = 'E.Name';
        $exportColumn[0][] = 'C.Name';
        $exportColumn[0][] = 'Birth Date';
        $exportColumn[0][] = 'Sex';
        $exportColumn[0][] = 'ID No.';
        $exportColumn[0][] = 'STRN';
        $exportColumn[0][] = 'Primary School';
// 		$exportColumn[0][] = 'Name of Parent/Guardian';
// 		$exportColumn[0][] = 'Email';

        return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportDataEDB($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg, $Lang, $kis_lang;

        $studentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $studentInfo['applicationID'], '');
        $otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID, $classLevelID, $applicationID, $recordID);
        $custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);

        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));

        $birthcertno = $studentInfo['birthcertno'];
        $birthcertno = substr_replace($birthcertno, '(', strlen($birthcertno) - 1, 0) . ')'; // Add ( ) for the hkid

        $ExportArr = array();

        $ExportArr[] = $studentInfo['applicationID'];
        $ExportArr[] = $studentInfo['student_name_en'];
        $ExportArr[] = $studentInfo['student_name_b5'];
        $ExportArr[] = $studentInfo['dateofbirth'] > 0 ? date("j/n/Y", strtotime($studentInfo['dateofbirth'])) : '';
        $ExportArr[] = $studentInfo['gender'];
        $ExportArr[] = $studentInfo['birthcertno'] ? $birthcertno : '';
        $ExportArr[] = $custInfo['BD_Ref_Num'][0]['Value'];
        $ExportArr[] = $otherInfo2[0]['OthersPrevSchName'];
// 		$ExportArr[] = $parentInfo[0]['EnglishName'];
// 		$ExportArr[] = $parentInfo[0]['Email'];

        return $ExportArr;
    }

    function getExportDataForImportAccount($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $tabID = '')
    {
        global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;

        $studentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));
        $custInfo = $this->getAllApplicationCustInfo($studentInfo['applicationID']);

        $dataArray = array();
        if ($tabID == 2) {
            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
            $studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
            $dataArray[0] = array();
            $dataArray[0][] = ''; //UserLogin
            $dataArray[0][] = ''; //Password
            $dataArray[0][] = ''; //UserEmail
            $dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
            $dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
            $dataArray[0][] = ''; //NickName
            $dataArray[0][] = $studentInfo['gender']; //Gender
            $dataArray[0][] = ''; //Mobile
            $dataArray[0][] = ''; //Fax
            $dataArray[0][] = ''; //Barcode
            $dataArray[0][] = ''; //Remarks
            $dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
            $dataArray[0][] = (is_numeric($studentInfo['homeaddress']) ? $Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']] : $studentInfo['homeaddress']); //Address
            if ((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment'])) {
                $dataArray[0][] = ''; //CardID
                if ($sys_custom['SupplementarySmartCard']) {
                    $dataArray[0][] = ''; //CardID2
                    $dataArray[0][] = ''; //CardID3
                }
            }
            if ($special_feature['ava_hkid'])
                $dataArray[0][] = $studentInfo['birthcertno']; //HKID
            if ($special_feature['ava_strn'])
                $dataArray[0][] = $custInfo['BD_Ref_Num'][0]['Value']; //STRN
            if ($plugin['medical'])
                $dataArray[0][] = ''; //StayOverNight
            $dataArray[0][] = $studentInfo['county']; //Nationality
            $dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
            $dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
        } else if ($tabID == 3) {
            $hasParent = false;
            $dataCount = array();
            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
            for ($i = 0; $i < count($parentInfo); $i++) {
                if ($parentInfo[$i]['type'] == 'G' && !$hasParent) {
                    $dataArray[0] = array();
                    $dataArray[0][] = ''; //UserLogin
                    $dataArray[0][] = ''; //Password
                    $dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
                    $dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
                    $dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
                    $dataArray[0][] = ''; //Gender
                    $dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
                    $dataArray[0][] = $custInfo['Parent_Fax'][0]['Value']; //Fax
                    $dataArray[0][] = ''; //Barcode
                    $dataArray[0][] = ''; //Remarks
                    if ($special_feature['ava_hkid'])
                        $dataArray[0][] = ''; //HKID
                    $dataArray[0][] = ''; //StudentLogin1
                    $dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
                    $dataArray[0][] = ''; //StudentLogin2
                    $dataArray[0][] = ''; //StudentEngName2
                    $dataArray[0][] = ''; //StudentLogin3
                    $dataArray[0][] = ''; //StudentEngName3
                    //$hasParent = true;
                    $dataCount[0] = ($parentInfo[$i]['email'] ? 1 : 0) + ($parentInfo[$i]['parent_name_en'] ? 1 : 0) + ($parentInfo[$i]['parent_name_b5'] ? 1 : 0) + ($parentInfo[$i]['mobile'] ? 1 : 0);
                }
            }
            if ($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]) {
                $tempDataArray = $dataArray[0];
            } else if ($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]) {
                $tempDataArray = $dataArray[1];
            } else if ($dataCount[2] > 0) {
                $tempDataArray = $dataArray[2];
            }
            $dataArray = array();
            $dataArray[0] = $tempDataArray;
        }
        $ExportArr = $dataArray;

        return $ExportArr;
    }

    function getApplicantEmail($applicationId)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;

        $sql = "SELECT
					a.Email as Email
				FROM ADMISSION_STU_INFO as a
				WHERE a.ApplicationID = '" . trim($applicationId) . "'";
        $records = current($this->returnArray($sql));

        return $records['Email'];
    }

    public function preReplaceEmailVariables($text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1, $interviewDateTime2, $interviewDateTime3, $briefingdate, $briefinginfo, $langSpokenAtHome, $printEmailLink)
    {
        global $kis_lang;
        $sql = "SELECT
					ApplyLevel
				FROM 
				    ADMISSION_OTHERS_INFO
				WHERE 
				    ApplicationID = '{$applicationNo}'";
        $applyLevel = current($this->returnVector($sql));
        $applyLevel = $this->classLevelAry[$applyLevel];
        $rs = $this->getApplicationCustInfo($applicationNo, 'RatingClass', true);
        $ratingClass = $kis_lang['Admission']['TimeSlot'][$rs['Value']];
        $ratingClass_zh = $kis_lang['Admission']['TimeSlot_zh'][$rs['Value']];
        $ratingClass_en = $kis_lang['Admission']['TimeSlot_en'][$rs['Value']];

        $replaced_text = $text;
        $replaced_text = str_replace("[=AdmissionClass=]", $applyLevel, $replaced_text);
        $replaced_text = str_replace("[=AdmissionSession=]", $ratingClass, $replaced_text);
        $replaced_text = str_replace("[=AdmissionSessionChi=]", $ratingClass_zh, $replaced_text);
        $replaced_text = str_replace("[=AdmissionSessionEng=]", $ratingClass_en, $replaced_text);

        return $replaced_text;
    }

    public function sendMailToNewApplicant($applicationId, $subject, $message, $isResent = false)
    {
        if ($this->isInternalUse($_GET['token']) && !$isResent) {
            return true; // Skip send email for teacher apply
        }
        return parent::sendMailToNewApplicant($applicationId, $subject, $message);
    }

    public function sendMailToNewApplicantWithReceiver($applicationId, $subject, $message, $to_email)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
        include_once($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new libwebmail();

        $from = $libwebmail->GetWebmasterMailAddress();

        if ($subject == '') {
            $email_subject = "新生入學申請 - 確認通知 (申請編號：{$applicationId})";
        } else {
            $email_subject = $subject;
        }
        $email_message = $message;
        $sent_ok = true;
        if ($to_email != '' && intranet_validateEmail($to_email)) {
            $sent_ok = $libwebmail->sendMail($email_subject, $email_message, $from, array($to_email), array(), array(), "", $IsImportant = "", $mail_return_path = get_webmaster(), $reply_address = "", $isMulti = null, $nl2br = 0);
        } else {
            $sent_ok = false;
        }

        return $sent_ok;
    }


    function importInterviewInfoByArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds = array())
    {
        global $UserID, $admission_cfg;

        // -- insert to arragement log [start]
        $sql = "INSERT INTO ADMISSION_INTERVIEW_ARRANGEMENT_LOG
					(SchoolYearID, Round, StatusSelect, DateInput, InputBy, DateModified, ModifiedBy)
					Values
					('" . $selectSchoolYearID . "',
					'" . $round . "',
					'" . implode(",", $selectStatusArr) . "',
					NOW(),
					'" . $UserID . "',
					NOW(),
					'" . $UserID . "'
					)";
        $insertResult[] = $this->db_db_query($sql);
        // -- insert to arragement log [end]

        $insertResult[] = $this->updateApplicantArrangement($selectSchoolYearID, $selectStatusArr, $round, $classLevelIds);

        return !in_array(false, $insertResult);
    }

    function checkImportDataForImportInterview($data)
    {
        $resultArr = array();
        $i = 0;
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);
            //check date
            if ($aData[4] == '' && $aData[5] == '') {
                $validDate = true;
            } else if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4])) {
                list($year, $month, $day) = explode('-', $aData[4]);
                $validDate = checkdate($month, $day, $year);
            } else {
                $validDate = false;
            }

            //check time
            if ($aData[4] == '' && $aData[5] == '') {
                $validTime = true;
            } else if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5])) {
                $validTime = true;
            } else {
                $validTime = false;
            }
            $sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE
					trim(s.applicationID) = '" . trim($aData[0]) . "' AND trim(s.birthCertNo) = '" . trim($aData[3]) . "'
	    	";
            $result = $this->returnVector($sql);
            if ($result[0] == 0) {
                $resultArr[$i]['validData'] = $aData[0];
            } else
                $resultArr[$i]['validData'] = false;
            $resultArr[$i]['validDate'] = $validDate;
            $resultArr[$i]['validTime'] = $validTime;
            if (!$validDate || !$validTime)
                $resultArr[$i]['validData'] = $aData[0];
            $i++;
        }
        return $resultArr;
    }

    function importDataForImportInterview($data)
    {
        $resultArr = array();
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);

            $sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET
		   		InterviewDate = '" . $aData[4] . " " . $aData[5] . "',
				InterviewLocation = '" . $aData[6] . "',
				DateModified = NOW(),
		   		ModifiedBy = '" . $this->uid . "'
   				WHERE ApplicationID = '" . $aData[0] . "'
	    	";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;

        }
        return $resultArr;
    }

    function getExportDataForImportInterview($recordID, $schoolYearID = '', $selectStatus = '', $classLevelID = '')
    {
        global $admission_cfg;
        $cond = !empty($schoolYearID) ? " AND a.SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= !empty($selectStatus) ? " AND a.Status='" . $selectStatus . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				" . $cond . "
    	";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds = array())
    {
        $fieldSetting = "InterviewSettingID" . ($round > 1 ? $round : '');

        ######## Get interview settings START ########
        $sql = "SELECT
            AIS.ClassLevelID,
            AIS.RecordID,
            AIS.Quota,
            COUNT(AOI.RecordID) AS UsedQuota
        FROM
            ADMISSION_INTERVIEW_SETTING AIS
        LEFT JOIN
            ADMISSION_OTHERS_INFO AOI
        ON 
            AOI.{$fieldSetting} = AIS.RecordID
        WHERE
            AIS.SchoolYearID = '" . $selectSchoolYearID . "'
        AND
            AIS.Round = '" . $round . "'
        AND
            AIS.GroupName IS NOT NULL
        GROUP BY
            AIS.RecordID,
            AIS.Quota
        ORDER BY
            AIS.ClassLevelID DESC, AIS.Date, AIS.StartTime, AIS.EndTime, AIS.GroupName";
        $interviewRecordIDArr = $this->returnArray($sql);
        $interviewRecordMapping = BuildMultiKeyAssoc($interviewRecordIDArr, array('RecordID'));
        ######## Get interview settings END ########

        ######## Init interview time slot array START ########
        $timeSlotQuotaArr = array();
        foreach ($interviewRecordIDArr as $interviewInfo) {
            $remainsQuota = $interviewInfo['Quota'] - $interviewInfo['UsedQuota'];

            if ($interviewInfo['ClassLevelID']) {
                for ($i = 0; $i < $remainsQuota; $i++) {
                    $timeSlotQuotaArr[$interviewInfo['ClassLevelID']][] = $interviewInfo['RecordID'];
                }
            } else {
                $timeSlotQuotaArr['Others'][] = $interviewInfo['RecordID'];
            }
        }
        ######## Init interview time slot array END ########

        ######## Get applicant START ########
        $classLevelIdSql = implode("','", $classLevelIds);
        $status_cond = '';
        if (sizeof($selectStatusArr) > 0) {
            $status_cond .= " AND st.status in ('" . implode("','", $selectStatusArr) . "') ";
        }

        $sql = "Select
            s.DOB,
            o.ApplicationID,
            o.ApplyLevel as ClassLevel
        From
            ADMISSION_OTHERS_INFO as o
        LEFT JOIN
            ADMISSION_STU_INFO as s
        ON
            o.ApplicationID = s.ApplicationID
        LEFT JOIN
            ADMISSION_APPLICATION_STATUS as st
        ON
            o.ApplicationID = st.ApplicationID
        LEFT JOIN
            YEAR as y
        ON
            y.YearID = o.ApplyLevel
        where
            o.ApplyYear = '{$selectSchoolYearID}'
        AND 
            o.{$fieldSetting} = 0
        AND 
            o.ApplyLevel IN ('{$classLevelIdSql}')
            {$status_cond}";

        $allApplicant = $this->returnResultSet($sql);

        if (!$allApplicant) {
            return true;
        }
        ######## Get applicant END ########

        ######## Re-Order applicant START ########
        usort($allApplicant, function ($a, $b) {
            $aDob = substr($a['DOB'], 0, 7);
            $bDob = substr($b['DOB'], 0, 7);

            if ($aDob != $bDob) {
                return strcmp($aDob, $bDob);
            }

            return strcmp($a['ApplicationID'], $b['ApplicationID']);
        });
        ######## Re-Order applicant END ########

        ######## Save to DB START ########
        $isNotEnoughQuota = false;
        $result = array();
        foreach ($allApplicant as $applicant) {
            $classLevelId = $applicant['ClassLevel'];

            if (count($timeSlotQuotaArr[$classLevelId])) {
                $timeSlotId = array_shift($timeSlotQuotaArr[$classLevelId]);
            } elseif (count($timeSlotQuotaArr['Others'])) {
                $timeSlotId = array_shift($timeSlotQuotaArr['Others']);

                $interviewInfo = $interviewRecordMapping[$timeSlotId];
                $remainsQuota = $interviewInfo['Quota'] - $interviewInfo['UsedQuota'] - 1;
                for ($i = 0; $i < $remainsQuota; $i++) {
                    $timeSlotQuotaArr[$classLevelId][] = $timeSlotId;
                }
            } else {
                $timeSlotId = 0;
                foreach ($interviewRecordMapping as $recordId => $interviewInfo) {
                    if ($interviewInfo['ClassLevelID'] == $classLevelId) {
                        $timeSlotId = $recordId;
                    }
                }

                if ($timeSlotId == 0) {
                    $isNotEnoughQuota = true;
                }
            }
            $interviewRecordMapping[$timeSlotId]['UsedQuota']++;

            if ($interviewRecordMapping[$timeSlotId]['ClassLevelID'] == 0) {
                $interviewRecordMapping[$timeSlotId]['ClassLevelID'] = $classLevelId;
                $sql = "UPDATE 
                    ADMISSION_INTERVIEW_SETTING 
                SET 
                    ClassLevelID = '{$classLevelId}'
                WHERE 
                    RecordID = '{$timeSlotId}' ";
                $result[] = $this->db_db_query($sql);
            }

            $sql = "UPDATE 
                ADMISSION_OTHERS_INFO 
            Set 
                {$fieldSetting} = '{$timeSlotId}' 
            Where 
                ApplicationID = '{$applicant['ApplicationID']}' ";
            $result[] = $this->db_db_query($sql);
        }
        ######## Save to DB END ########

        return !in_array(false, $result);
    }

    function hasBirthCertNumber($birthCertNo, $applyLevel)
    {
        global $admission_cfg;

        $sql = "SELECT 
               COUNT(*) 
        FROM 
            ADMISSION_STU_INFO AS asi 
        INNER JOIN 
            ADMISSION_OTHERS_INFO AS aoi 
        ON
            asi.ApplicationID = aoi.ApplicationID
        INNER JOIN 
            ADMISSION_APPLICATION_STATUS AS aas 
        ON
            asi.ApplicationID = aas.ApplicationID
        WHERE 
            TRIM(asi.BirthCertNo) = '{$birthCertNo}'
        AND 
            aoi.ApplyYear = '" . $this->getNextSchoolYearID() . "' 
        AND 
            aas.STATUS <> '{$admission_cfg['Status']['CREATIVE_cancelled']}'";

        return current($this->returnVector($sql));
    }

    function hasToken($token)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '" . $token . "' ";
        return current($this->returnVector($sql));
    }

    function getApplicationStudentInfoCust($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = !empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
			SELECT
				s.RecordID,
     			s.ApplicationID applicationID,
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'
			" . $cond . "
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

        return $this->returnArray($sql);
    }

    function getApplicationRelativesInfoCust($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = !empty($applicationID) ? " AND r.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
			SELECT
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year OthersRelativeStudiedYear,
				r.Name OthersRelativeStudiedName,
     			r.ClassPosition OthersRelativeClassPosition,
				r.Relationship OthersRelativeRelationship
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '" . $schoolYearID . "'
			" . $cond . "
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

        return $this->returnArray($sql);
    }

    function uploadAttachment($type, $file, $destination, $randomFileName = "")
    {//The $randomFileName does not contain file extension
        global $admission_cfg, $intranet_root, $libkis;
        include_once($intranet_root . "/includes/libimage.php");
        $uploadSuccess = false;
        $ext = strtolower(getFileExtention($file['name']));

// 		if($type == "personal_photo"){
// 			return true;
// 		}
        if (!empty($file['image_data'])) {
            list($type, $data) = explode(';', $file['image_data']);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);

            $tmpfname = tempnam("/tmp", "admission_file_");
            $handle = fopen($tmpfname, "w");
            fwrite($handle, $data);
            fclose($handle);

            $file['tmp_name'] = $tmpfname;
        }
        if (!empty($file['tmp_name'])) {
            require_once($intranet_root . "/includes/admission/class.upload.php");
            $handle = new Upload($file['tmp_name']);
            if ($handle->uploaded) {
                $handle->Process($destination);
                if ($handle->processed) {
                    $uploadSuccess = true;
                    if ($type == "personal_photo") {
                        $image_obj = new SimpleImage();
                        $image_obj->load($handle->file_dst_pathname);
                        if ($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
                            $image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
                        else
                            $image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
                        //rename the file and then save

                        $image_obj->save($destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext, $image_obj->image_type);
                        unlink($handle->file_dst_pathname);
                    } else {
                        rename($handle->file_dst_pathname, $destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext);
                    }
                    //$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
                } else {
                    // one error occured
                    $uploadSuccess = false;
                }
                // we delete the temporary files
                $handle->Clean();
            }
        } else {
            if ($this->isInternalUse($_REQUEST['token'])) {
                return true;
            }
        }

        if (file_exists($file['tmp_name'])) {
            @unlink($file['tmp_name']);
        }
        return $uploadSuccess;
        //}
        //return true;
    }
} // End Class
