<?php
# modifying by: Henry

/********************
 * Log :
 * Date		2015-10-07 [Henry]
 * 			added internal use logic
 * Date		2015-06-30 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step, $ApplicationID=''){
		global $Lang, $sys_custom,$lac, $validForAdmission,$kis_lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token'] && $Step!=7){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'</center></h2>';
		}
		if($lac->isInternalUse($_REQUEST['token']) && ($Step!=7 || $lac->getTokenByApplicationNumber($ApplicationID)=='')){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'</center></h2>';
		}
				
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].'</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].'</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].'</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>'.$Lang['Admission']['confirmation'].'</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:90px">5 '.$Lang['Admission']['finish'].'</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage']; //Henry 20131107
		}
		
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token']){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'</center></h2>';
		}
		if($lac->isInternalUse($_GET['token'])){
			$previewnote .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].'</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm($ClassLevel);
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac, $sys_custom;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage']; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
         $x .= $Instruction;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN('開始填寫表格', "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->';								
							if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
							}
							else{
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
							}
						$x .= '</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm($ClassLevel = ''){
		global $libkis_admission, $Lang, $lac, $sys_custom;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		/*
		//disable to choose class when using central server
		$disable = '';
		if($ClassLevel)
			$disable = 'return false';
		*/
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])){
					$hasClassLevelApply = 1;
					$selected = '';
					/*
					if($key == $ClassLevel)
						$selected = "checked='checked'";
						*/
					
					//Henry added [20140808]
					$numOFQuotaLeft = $lac->NumOfQuotaLeft($key,$value['SchoolYearID']);
					$isFullApply = false;
					if($numOFQuotaLeft <= 0){
						$isFullApply = true;
					}
					$disable = '';
					if($isFullApply){
						$selected = 'disabled';
					}
					
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="'.$disable.'" '.$selected.'  />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].($isFullApply?' <span style="color:red">('.$Lang['Admission']['IsFull'].' Full)</span>':'').'<!--(Quota Left:'.$numOFQuotaLeft.')-->'.'</label> ';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025
		//$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .='<fieldset ><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_index','step_instruction')", "SubmitBtn", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
		//$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm($BirthCertNo = ""){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission, $sys_custom, $admission_cfg;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm(0,$BirthCertNo);
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span>'.$Lang['Admission']['munsang']['mandatoryfield'].'</span>';
		//$x .='<br/>「<span class="tabletextrequire">*</span>」are mandatory but if not applicable please fill in N.A.';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->';
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0, $BirthCertNo = ""){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg,$kis_lang;
		$this->IsConfirm = $IsConfirm;
		
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayType = $applicationSetting[$formData['sus_status']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
			$dayTypeOption = $this->getSelectedValueLang($formData['ApplyClass'],$admission_cfg['classtype']);
		}else{
			// from /kis/admission_form/ajax_get_class_selection.php
		}
		
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
				$x = '';
		$x .= '<h1>'.$Lang['Admission']['CHIUCHUNKG']['Wish'].'</h1>';
		$x .= '<table class="form_table" style="font-size: 13px">';
		$x .= '<tr>';
		$x .= '<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>';
		$x .= '<td><div id="DayTypeOption">'.$dayTypeOption.'</div></td>';
		$x .= '</tr>';
		$x .= '</table>';
		
		$x .= '<h1>'.$Lang['Admission']['studentInfo'].'</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['chinesename'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_StuChiName").'</td>
				<td class="field_title">'.$star.$Lang['Admission']['englishname'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_StuEngName").'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateofbirth'].'</td>
				<td>
					'.$this->getFormElement("FormStudentInfo_DOB").'</td>
				<td class="field_title">'.$star.$Lang['Admission']['gender'].'</td>
				<td>';
				$x .= $this->getFormElement("FormStudentInfo_StuGender");
				$x .='</td>
			</tr>
			<tr>	
               	<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['placeofbirth'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_POB").$this->getFormElement("FormStudentInfo_POB_Other").'</td>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['religion'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_Religion").$this->getFormElement("FormStudentInfo_Religion_Other").'</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['nativeplace'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_Province").$Lang['Admission']['province'].$this->getFormElement("FormStudentInfo_County").$Lang['Admission']['county'].'</td>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['church'].'</td>
				<td>'.$this->getFormElement("FormStudentInfo_Church").'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['CHIUCHUNKG']['IDtype'].'</td>
				<td nowrap colspan="3">'.$this->getFormElement("FormStudentInfo_StuIDType")
							.$this->getFormElement("FormStudentInfo_StuIDType_Other")
							.' '.$this->getFormElement("StudentBirthCertNo")
							.($IsConfirm? '' : ' '.$Lang['Admission']['mgf']['msg']['birthcertnohints'])
							
				.'</td>				
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['homenumber'].'</td>
				<td colspan="3">'.$this->getFormElement("FormStudentInfo_HomeTel").'</td>
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['contactnumber']
				.$this->getFormElement("disableFormStudentInfo_ContactTel")
				.'</td>
				<td colspan="3">'.$this->getFormElement("FormStudentInfo_ContactTel").'</td>
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$Lang['Admission']['CHIUCHUNKG']['Chi'].')'.'</td>
				<td colspan="3">'.$this->getFormElement("HomeAddressDistrict").$this->getFormElement("FormStudentInfo_HomeAddrChi").'</td>
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['homeaddress'].'('.$Lang['Admission']['CHIUCHUNKG']['Eng'].')'.'</td>
				<td colspan="3">'.$this->getFormElement("FormStudentInfo_HomeAddrEng" ).'</td>
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['contactaddress']
										.'('.$Lang['Admission']['CHIUCHUNKG']['Chi'].')'
										.$this->getFormElement("disableFormStudentInfo_ContactAddr")
										.'</td>
				<td colspan="3">'.$this->getFormElement("ContactAddressDistrict").$this->getFormElement("FormStudentInfo_ContactAddrChi").'</td>
			</tr>
			<tr>
				<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['contactaddress'].'('.$Lang['Admission']['CHIUCHUNKG']['Eng'].')'.'</td>
				<td colspan="3">'.$this->getFormElement("FormStudentInfo_ContactAddrEng").'</td>
			</tr>
		</table>';
		if($formData['disableCurrentSchool']){
			$hide_table = 'display:none;';
		}
		$x .= '<h1>'.$Lang['Admission']['CHIUCHUNKG']['CurrentSchoolInfo'].$this->getFormElement("disableCurrentSchool").'</h1>';
		$x .= '<table id="CurrentSchoolTable" class="form_table" style="font-size: 13px; '.$hide_table.'">
					<tr>
						<td class="field_title">'.$Lang['Admission']['CHIUCHUNKG']['CurrentSchoolSchoolName'].'</td>
						<td>'.$this->getFormElement("CurrentSchoolName").'</td>
						<td class="field_title">'.$Lang['Admission']['level'].'</td>
						<td>'.$this->getFormElement("CurrentSchoolClassLv").$this->getFormElement("CurrentSchoolDayType").'</td>
					</tr>
					<tr>
						<td class="field_title">'.$Lang['Admission']['homeaddress'].'</td>
						<td colspan="3">'.$this->getFormElement("CurrentSchoolAddr").'</td>
					</tr>
				</table>';
		return $x;
	}
	

	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang, $lac;
		$this->IsConfirm = $IsConfirm;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['PGInfo'].'</h1>
				<table class="form_table" style="font-size: 13px">
				<tr>
					<td class="field_title">'.$Lang['Admission']['relationship'].'</td>
					<td class="form_guardian_head"><center>'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['PG_Type']['F'].'</center></td>
					<td class="form_guardian_head"><center>'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['PG_Type']['M'].'</center></td>
					<td class="form_guardian_head"><center>'
						.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['PG_Type']['G'].' '
						.$this->getFormElement("G3Relationship")
						.$this->getFormElement("G3Gender")
					.'</center></td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['name'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1ChineseName","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2ChineseName","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3ChineseName","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['occupation'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1Occupation","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2Occupation","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3Occupation","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['CHIUCHUNKG']['worknumber'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1WorkNo","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2WorkNo","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3WorkNo","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['csm']['mobile'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1MobileNo","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2MobileNo","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3MobileNo","Parent").'</td>
				</tr>			
				<tr>
					<td class="field_title">'.$Lang['Admission']['csm']['email'].'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G1Email","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G2Email","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("G3Email","Parent").'</td>
				</tr>
		
			</table>';
			
		return $x;
	}
	function getOthersForm($IsConfirm=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		$this->IsConfirm = $IsConfirm;
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}

		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['otherInfo'].'</h1>';

		
		$x .= '<table id="dataTable1" class="form_table" style="font-size: 13px">';
		$x .= '<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['Family'].$Lang['Admission']['CHIUCHUNKG']['PleaseUseNumber'].'</td>
					<td id="StuFamilyTableCell">'
						.$this->getFormElement("NumElderBro").'&nbsp;兄&nbsp;'
						.$this->getFormElement("NumElderSis").'&nbsp;姊&nbsp;'
						.$this->getFormElement("NumYoungBro").'&nbsp;弟&nbsp;'
						.$this->getFormElement("NumYoungSis").'&nbsp;妹&nbsp;'
					.'</td>
				</tr>
				<tr>
					<td class="field_title">'.($lac->isInternalUse($_GET['token'])? '':$star).$Lang['Admission']['CHIUCHUNKG']['Twins'].'</td>
					<td>'.$this->getFormElement("IsTwins").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$this->getFormElement("TwinsIDNo").'</td>
				</tr>';
		$x .='</table>';
				
				
			for($i=2; $i<=4; $i++){
				if($IsConfirm && ($formData['OthersPrevSchYear'.$i] !="" || $formData['OthersPrevSchClass'.$i] !="" || $formData['OthersPrevSchName'.$i] !=""))
			$x .= '<tr>
					<td class="field_title" style="text-align:right">('.$i.')</td>
					<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersPrevSchYear'.$i].'</center>':'<input name="OthersPrevSchYear'.$i.'" type="text" id="OthersPrevSchYear'.$i.'" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersPrevSchClass'.$i].'</center>':'<input name="OthersPrevSchClass'.$i.'" type="text" id="OthersPrevSchClass'.$i.'" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersPrevSchName'.$i].'</center>':'<input name="OthersPrevSchName'.$i.'" type="text" id="OthersPrevSchName'.$i.'" class="textboxtext" />').'</td>
				</tr>';
			}

			$x .= '</table>';

			$x .= '<table id="dataTable2" class="form_table" style="font-size: 13px">
				<tr>
					<td>'.$Lang['Admission']['CHIUCHUNKG']['RelativeStudiedAtSchool'].'</td>
					<td class="form_guardian_head"><center>'.$Lang['Admission']['name'].'</center></td>
					<td class="form_guardian_head"><center>'.$Lang['Admission']['munsang']['RelationshipWithApplicant'].'</center></td>
					<td class="form_guardian_head"><center>'.$Lang['Admission']['class'].' / '.$Lang['Admission']['GradYear'].'</center></td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right">(1)</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedName1","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeRelationship1","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedYear1","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right">(2)</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedName2","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeRelationship2","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedYear2","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right">(3)</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedName3","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeRelationship3","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedYear3","Parent").'</td>
				</tr>
				<tr>
					<td class="field_title" style="text-align:right">(4)</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedName4","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeRelationship4","Parent").'</td>
					<td class="form_guardian_field">'.$this->getFormElement("OthersRelativeStudiedYear4","Parent").'</td>
				</tr>';
			
//			for($i=2; $i<=5; $i++){
//				if($IsConfirm && ($formData['OthersRelativeStudiedYear'.$i] !="" || $formData['OthersRelativeStudiedName'.$i] !="" || $formData['OthersRelativeClassPosition'.$i] !="" || $formData['OthersRelativeRelationship'.$i] !=""))
//				$x .= '<tr>
//						<td class="field_title" style="text-align:right">('.$i.')</td>
//						<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersRelativeStudiedName'.$i].'</center>':'<input name="OthersRelativeStudiedName'.$i.'" type="text" id="OthersRelativeStudiedName'.$i.'" class="textboxtext" />').'</td>
//						<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersRelativeRelationship'.$i].'</center>':'<input name="OthersRelativeRelationship'.$i.'" type="text" id="OthersRelativeRelationship'.$i.'" class="textboxtext" />').'</td>
//						<td class="form_guardian_field">'.($IsConfirm?'<center>'.$formData['OthersRelativeStudiedYear'.$i].'</center>':'<input name="OthersRelativeStudiedYear'.$i.'" type="text" id="OthersRelativeStudiedYear'.$i.'" class="textboxtext" />').'</td>
//					</tr>';
//			}

			$x .= '</table>';
//			if(!$IsConfirm)
//				$x .= '&nbsp;&nbsp;&nbsp;<input type="button" id="btn_addRow2" value="+" onClick="addRow(\'dataTable2\')" /> <input type="button" id="btn_deleteRow2" value="-" onClick="deleteRow(\'dataTable2\')" style="display:none" /><br/><br/>';
		
		return $x;
	}
	
	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		
		if(!$lac->isInternalUse($_GET['token'])){ // Require input only for External use
			$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		}
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1 style="font-size: 15px">'.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table" style="font-size: 15px">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)</span></td>
				</tr>';
		}
		
		$x .='<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].'</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/>').'
				</td>
			</tr>';
		
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.($i==0?$star:'').$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
				  </tr>';
		}
			
		/*$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';*/
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
				
			</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />

			</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7, $ApplicationID);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';
			if(!$sys_custom['KIS_Admission']['StaticApplicationPage'])
				$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			
			//add english version here...
			//$x .='<h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';
			//$x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			
		
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent || !$ApplicationID){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		if($ApplicationID){
			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);         
			//$x .='<br/><br/>';
			//$x .='Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>Application form preview<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);
			if(!$LastContent){
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
			$x .= '<br/>'.$LastContent;
		}
		else{
			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
			//$x .='<br/><br/>';
			//$x .='Admission is Not Completed. Please try to apply again!';
        }
		
		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';            
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['admissionQuotaFull'].'</h1>';
				//$LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['interviewQuotaFull'].'</h1>';
				//$LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	
	function getPDF_CSS(){
		$css = '
			<style>
			/*
			@media all {
			.page-break {display: none;}
			}
			@page {size: A4 portrait; margin:0.7cm 0 0 0;}
			*/
			@media print {
			.page-break {display: block; page-break-before: always;}
			html, body {width: 21cm;}
			}
			
			/*body {padding:0; margin:0; background-color: #fff; font-family: Arial, Helvetica, Verdana, sans-serif, "微軟正黑體", "新細明體", "細明體";color: #000000; line-height:1em; font-size:0.9em}*/
			body {padding:0; margin:0; background-color: #fff; font-family: msjh; line-height:1em; font-size:1.0em}
			
			.page_wrapper {width: 19.6cm; height: 28.3cm; margin: 0 auto;  /*background: yellow*/}
			
			/*header*/
			.header  {padding: 0; float: left; width: 19.6cm; height:2.5cm; }
			.logo {width: 80px; height: 87px; float: left; margin-right: 3mm;}
			.head1 {font-weight:bold; font-size: 1.6em;}
			.head2 {font-weight:bold; font-size: 1.2em;}
			/*.header .info {font-size: 0.65em;}*/
			.info { padding-left: 0.3em; font-size: 0.65em; float:right;}
			.school_name {padding:0.3em; float:right;}
			.form_no{float: right; color:red; font-size: 1.0em; padding:2mm;}
			
			/*form top*/
			.application_form_top {clear: both; width:100%;}
			/* -----form_table_class----*/
			table.application_form_table_class {border-collapse: collapse; border: 0; font-size: 1.1em; width: 19.6cm;}
			.application_form_table_class td {font-weight: normal;color: #000; text-align: left; padding: 2px 8px 2px 8px;font-size: 0.8em;}
			.application_form_table_class td.bggrey {background:#F6F6F6;}
			/*
			.application_form_table_class td span.small {font-size: 0.75em;}
			.application_form_table_class td span.small2 {font-size: 0.75em; font-weight: bold; border-bottom:1px solid #000; }
			.application_form_table_class td span.title {font-weight: bold; margin-top:3px; display:inline-block;}
			*/
			.application_form_table_class td.title2 {font-size: 1.7em; line-height: 1em; font-weight:bold; text-align:center;}
			/*
			.application_form_table_class td span.year {font-size: 0.7em;}
			*/
			
			.application_form_table_class td.alignright {text-align:right;}
			.application_form_table_class td.note {font-size: 0.8em; padding: 8px 2px 1px 2px; font-weight: bold;}
			
			.year {font-size: 0.72em;}
			.title_class {font-size: 0.85em; font-weight: bold; margin-top:3px; display:inline-block;}
			.small_class {font-size: 0.75em;}
			.small2_class {font-size: 0.75em; font-weight: bold; border-bottom:1px solid #000; }
			
			/*form grey box*/
			.application_form_block {clear: both; border: 1px solid #ccc; width: 100%; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
			.application_form_block_2nd {border-top:none; padding-top:2px;}
			.application_form_block_3rd {clear: both; border: 5px solid #f2f2f2; width: 100%; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;}
			
			/* -----form table information & parent----*/
			table.application_form_table {border-collapse: collapse; border: 0px; font-size: 0.8em; margin: 0px auto 8px auto; width: 18.8cm; color: #000; text-align: left;}
			table.application_form_table table{border-collapse: collapse;}
			.application_form_table td {border-bottom: 1px solid #666; padding: 1px 5px 2px 5px;}
			.application_form_table table td {padding: 2px;}
			/*
			.application_form_table td span.title {font-weight: bold; margin-top:5px; display:inline-block;}
			.application_form_table td span.text {margin-top:5px; display:inline-block;}
			.application_form_table td span.small {font-size: 0.75em;}
			.application_form_table td span.small2 {font-size: 0.75em; font-weight: bold;}
			*/
			.title {font-weight: bold; margin-top:5px; display:inline-block;}
			.text {margin-top:5px; display:inline-block;}
			.small {font-size: 0.85em;}
			.small2 {font-size: 0.85em; font-weight: bold;}
			
			.photo {width: 3.6cm; height: 4.8cm; border: 1px solid #000; display: table; margin: 10px 0px 0px 0px; text-align: center;}
			.photo_txt {text-align: center; height: 4.8cm; display: table-cell; vertical-align: middle;}
			
			/*check_box*/
			.check_box {width: 11px; height: 11px; border: 1px solid #666; display: inline-block; vertical-align: middle; margin:0 2px 2px 0;}
			.check_box {width: 11px; height: 11px;}
			/*radio_button*/
			.radio_button {margin:0 6px 0px 3px;}
			.radio_button_container {display: inline-block; vertical-align: middle; margin:0 2px 2px 0; width: 12px; height: 12px;}
			/*.radio_button_container img{width: 12px; height: 12px;}*/
			.radio_button {width: 11px; height: 11px;}
			
			/*underline*/
			.underline {border-bottom: 1px solid #666; color: #000; display: inline-block; padding: 0px 5px 2px 5px; height: 1em; text-align:left;}
			/*space*/
			.space {color: #000; display: inline-block;  padding: 0px 5px 2px 5px; height: 1em; text-align:center;}
			/*alignment*/
			.text_center{ text-align:center;}
			/*widths*/
			/*
			.w5{width:5px;}
			.w10{width:10px;}
			.w15{width:15px;}
			.w20{width:20px;}
			.w30{width:30px;}
			.w40{width:40px;}
			.w60{width:60px;}
			.w85{width:85px;}
			.w90{width:90px;}
			*/
			/*---border----*/
			td.border_r {border-right: 1px solid #666;}
			td.border_l {border-left: 1px solid #666;}
			/*td.border_blite {border-bottom: 1px solid #ccc;}*/
			/*td.border_bdot {border-bottom: 1px dotted #ccc;}*/
			td.border_rlite {border-right: 1px solid #ccc;}
			/*td.border_bnone {border-bottom: none;}
			td.border_none {border: none; }*/
			/*---padding----*/
			td.padding_none {padding:0;}
			
			td.profileform {height: 290;}
			</style>';
		
		return $css;
	}
	
	
	function getPDF_Page2(){
		$page2 = '<div class="page-break"></div>
					<!--PAGE 2-->
					<div class="page_wrapper">
					 <div class="header"></div>
					    
					<!--form student profile-->
					 <div class="application_form_block_3rd">
						<table class="application_form_table">
						<tr>
						    <td height="0" width="55" style="border:none;"></td>
							<td height="0" width="110" style="border:none;"></td>
							<td height="0" width="110" style="border:none;"></td>
							<td style="border:none;"></td>
						</tr>
						<tr>
							<td colspan="4" align="right" style="border:none;" class="border_none"><span class="small">（此欄只供學校填寫）</span></td>
						</tr>
						<tr>
							<td style="padding-top:5px;"><span class="title">級別</span></td>
							<td style="padding-top:5px;"><span class="title">班別</span></td>
							<td style="padding-top:5px;"><span class="title">班主任</span></td>
							<td style="padding-top:5px;"><span class="title">備註</span></td>
						</tr>
						<tr valign="top" height="290">
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_l border_rlite border_blite">幼兒班</td>
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_rlite border_blite"></td>
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_rlite border_blite"></td>
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_r border_blite"></td>
						</tr>
						<tr valign="top" height="290">
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_l border_rlite border_blite">低班</td>
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_rlite border_blite"></td>
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_rlite border_blite"></td>
							<td style="border-bottom: 1px solid #ccc;" valign="top" class="profileform border_r border_blite"></td>
						</tr>
						<tr valign="top" height="290">
							<td valign="top" class="profileform border_l border_rlite">高班</td>
							<td valign="top" class="profileform border_rlite"></td>
							<td valign="top" class="profileform border_rlite"></td>
							<td valign="top" class="profileform border_r"></td>
						</tr>
						<tr>
							<td colspan="4" style="border:none;" class="border_none"><span class="small2">&bull; 學生若中途轉班或退學，班主任請在備註一欄內註明原因及日期。</span></td>
						</tr>
						</table>
					 </div>
					<!--form student profile ends-->
					</div>
					<!--PAGE 2 ends-->
					
					</body>';
		return $page2;
	}
	
	function getPDFContent($schoolYearID,$applicationIDAry,$type=''){
		global $PATH_WRT_ROOT,$lac,$Lang,$admission_cfg;
		
		$Lang['General']['EmptySymbol'] = '---';
		# pdf images
		$logo_URL = $PATH_WRT_ROOT.'file/customization/chiuchunkg.eclass.hk/images/logo.png';
		$radioOn_URL = $PATH_WRT_ROOT.'file/customization/chiuchunkg.eclass.hk/images/radiobutton_on.png';
		$radioOff_URL = $PATH_WRT_ROOT.'file/customization/chiuchunkg.eclass.hk/images/radiobutton_off.png';
		$checkboxOn_URL = $PATH_WRT_ROOT.'file/customization/chiuchunkg.eclass.hk/images/checkbox_on.png';
		$checkboxOff_URL = $PATH_WRT_ROOT.'file/customization/chiuchunkg.eclass.hk/images/checkbox_off.png';
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		$margin_top = '7';
		$mpdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer); 
		$mpdf->mirrorMargins = 1;
		
		foreach((array)$applicationIDAry as $applicationID){
			
			# Student Info
			//$StuInfoArr = $lac->getApplicationStudentInfo($schoolYearID,'','','',$applicationID);
			$StuInfoArr = $lac->getApplicationStudentInfo($schoolYearID,'',$applicationID);
			$StuInfo = $StuInfoArr[0];		
			
			# Parent Info
			$GuardianInfoArr = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
			$GuardianInfo = array();
			foreach((array)$GuardianInfoArr as $_guradianInfoArr ){
				
				if($_guradianInfoArr['type'] == 'F'){
					$Gnum = 1;
				}
				else if($_guradianInfoArr['type'] == 'M'){
					$Gnum = 2;
				}
				else{
					$Gnum = 3;
					$GuardianInfo['G3Relationship'] = $_guradianInfoArr['Relationship'];
					$GuardianInfo['G3Gender'] = $_guradianInfoArr['Gender'];
				}
				$GuardianInfo['G'.$Gnum.'ChineseName'] = $_guradianInfoArr['parent_name_b5'];
				$GuardianInfo['G'.$Gnum.'Occupation'] = $_guradianInfoArr['occupation'];
				$GuardianInfo['G'.$Gnum.'WorkNo'] = $_guradianInfoArr['OfficeTelNo'];
				$GuardianInfo['G'.$Gnum.'MobileNo'] = $_guradianInfoArr['mobile'];
				$GuardianInfo['G'.$Gnum.'Email'] = $_guradianInfoArr['email'];	
			}
			
			# OtherInfo
			$othersInfoArr = $lac->getApplicationOthersInfo($schoolYearID,'',$applicationID);
			$othersInfo = $othersInfoArr[0];
			$studentInfoCust = $lac->getApplicationStudentInfoCust($schoolYearID,'',$applicationID);
			$currentSchool = $studentInfoCust[0];
			$relativesInfoCust = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID);
			$i = 1;
			
			foreach((array)$relativesInfoCust as $_infoArr){
				$relativesInfo['Name_'.$i] = $_infoArr['OthersRelativeStudiedName'];
				$relativesInfo['YearClass_'.$i] = $_infoArr['OthersRelativeStudiedYear'];
				$relativesInfo['Relation_'.$i] = $_infoArr['OthersRelativeRelationship'];
				$i++;
			}
			# Photo
			$attachmentArr = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
			$photoLink = $attachmentArr['personal_photo']['link'];
			
			#Apply Class
			$StuApplyClass_option1 = $checkboxOff_URL;
			$StuApplyClass_option2 = $checkboxOff_URL;
			$StuApplyClass_option3 = $checkboxOff_URL;
			switch($othersInfo['ApplyDayType1']){
				case $admission_cfg['classtype']['AM']:
				$StuApplyClass_option1 = $checkboxOn_URL;
				break;
				case $admission_cfg['classtype']['PM']:
				$StuApplyClass_option2 = $checkboxOn_URL;
				break;
				case $admission_cfg['classtype']['EitherOne']:
				$StuApplyClass_option3 = $checkboxOn_URL;
				break;
			}
			
			#Gender radio button
			$StuGender_option1 = $radioOff_URL;
			$StuGender_option2 = $radioOff_URL;
			switch($StuInfo['gender']){
				case 'M':
					$StuGender_option1 = $radioOn_URL;
				break;
				case 'F':
					$StuGender_option2 = $radioOn_URL;
				break;
				
			}
			
			# Birth Cert Type radio button
			$StuBirthCert_option1 = $radioOff_URL;
			$StuBirthCert_option2 = $radioOff_URL;
			$StuBirthCert_option3 = $radioOff_URL;
			switch($StuInfo['birthcerttype']){
				case $admission_cfg['BirthCertType']['hkBirthCert']:
					$StuBirthCert_option1 = $radioOn_URL;
					$StuBirthCert_Other = Get_String_Display('');
				break;
				case $admission_cfg['BirthCertType']['hkIdCard']:
					$StuBirthCert_option2 = $radioOn_URL;
					$StuBirthCert_Other = Get_String_Display('');
				break;
				case $admission_cfg['BirthCertType']['other']:
					$StuBirthCert_option3 = $radioOn_URL;
					$StuBirthCert_Other = $StuInfo['birthcerttypeother'];
				break;
			}
			
			# POB radio button
			$StuPOB_option1 = $radioOff_URL;
			$StuPOB_option2 = $radioOff_URL;
			$StuPOB_option3 = $radioOff_URL; #$radioOn_URL
			$StuPOB_option4 = $radioOff_URL;
			$StuPOB_Other = '';
			switch($StuInfo['placeofbirth']){
				case $admission_cfg['placeofbirth']['hk']:
					$StuPOB_option1 = $radioOn_URL;
					$StuPOB_Other = Get_String_Display('');
				break;
				case $admission_cfg['placeofbirth']['macau']:
					$StuPOB_option2 = $radioOn_URL;
					$StuPOB_Other = Get_String_Display('');
				break;
				case $admission_cfg['placeofbirth']['china']:
					$StuPOB_option3 = $radioOn_URL;
					$StuPOB_Other = Get_String_Display('');
				break;
				default:
					$StuPOB_option4 = $radioOn_URL;
					$StuPOB_Other = $StuInfo['PlaceOfBirthOther'];
				break;
			}
			
			# religion radio button
			$StuReligion_option1 = $radioOff_URL;
			$StuReligion_option2 = $radioOff_URL;
			$StuReligion_option3 = $radioOff_URL;
			switch($StuInfo['religion1']){
				case $admission_cfg['religion']['Christian']:
					$StuReligion_option1 = $radioOn_URL;
					$StuReligion_Other = Get_String_Display('');
				break;
				case $admission_cfg['religion']['Catholic']:
					$StuReligion_option2 = $radioOn_URL;
					$StuReligion_Other = Get_String_Display('');
				break;
				case $admission_cfg['religion']['other']:
					$StuReligion_option3 = $radioOn_URL;
					$StuReligion_Other = $StuInfo['ReligionOther'];
				break;
			}
	
			# Age
			$StuAgeArr = explode('|||',$StuInfo['age']);
			$StuAgeYear = $StuAgeArr[0];
			$StuAgeMonth = $StuAgeArr[1];
			
			# Contact Num
			$StuContact_Same = $radioOff_URL;
			$StuPhoneNoArr = explode('|||',$StuInfo['homephoneno']);
			$StuHomeNo = $StuPhoneNoArr[0];
			$StuContactNo = $StuPhoneNoArr[1];
			if($StuContactNo == ''){
				$StuContact_Same = $radioOn_URL;
			}
			
			# Address
			$StuAddress_Same = $radioOff_URL;
			if($StuInfo['ContactAddress'] == '' && $StuInfo['ContactAddressChi'] ==''){
				$StuAddress_Same = $radioOn_URL;
			} 
			
			#Address District
			$AddressDistrictArr = explode('|||',$StuInfo['AddressDistrict']);
			$HDistrict_option1 = $radioOff_URL;
			$HDistrict_option2 = $radioOff_URL;
			$HDistrict_option3 = $radioOff_URL;
			$CDistrict_option1 = $radioOff_URL;
			$CDistrict_option2 = $radioOff_URL;
			$CDistrict_option3 = $radioOff_URL;
			for($i=0;$i<2;$i++){
				if($i==0){
					$Prefix = 'H';
				}
				else if($i==1){
					$Prefix = 'C';
				}
				
				switch($AddressDistrictArr[$i]){
					case $admission_cfg['addressdistrict']['hk']:
					${$Prefix.'District_option1'} = $radioOn_URL;
					break;
					case $admission_cfg['addressdistrict']['kln']:
					${$Prefix.'District_option2'} = $radioOn_URL;
					break;
					case $admission_cfg['addressdistrict']['nt']:
					${$Prefix.'District_option3'} = $radioOn_URL;
					break;
				}
			}
			
			# Current School Class
			$StuCurrentClass_option1 = $radioOff_URL;
			$StuCurrentClass_option2 = $radioOff_URL;
			$StuCurrentClass_option3 = $radioOff_URL;
			if(!empty($currentSchool)){
				switch($currentSchool['ClassType']){
					case $admission_cfg['classtype2']['AM']:
					$StuCurrentClass_option1 = $radioOn_URL;
					break;
					case $admission_cfg['classtype2']['PM']:
					$StuCurrentClass_option2 = $radioOn_URL;
					break;
					case $admission_cfg['classtype2']['WholeDay']:
					$StuCurrentClass_option3 = $radioOn_URL;
					break;
				}
			}
			
			# guardianGenderM
			$guardianGenderM = $radioOff_URL;
			$guardianGenderF = $radioOff_URL;
			if($GuardianInfo['G3Gender'] =='M'){
				$guardianGenderM = $radioOn_URL;
			}else if($GuardianInfo['G3Gender'] =='F'){
				$guardianGenderF = $radioOn_URL;
			}
			
			#Relatives
			for($i=1 ; $i <5;$i++){
				
				for($j=1 ; $j <5;$j++){
					${'Relatives'.$i.'_Option'.$j} = $radioOff_URL;	
				}
			}
			for($i=1 ; $i <5;$i++){
				switch($relativesInfo['Relation_'.$i]){
					case $admission_cfg['relation']['F']:
						${'Relatives'.$i.'_Option1'} = $radioOn_URL;
					break;
					case $admission_cfg['relation']['M']:
						${'Relatives'.$i.'_Option2'} = $radioOn_URL;
					break;
					case $admission_cfg['relation']['B']:
						${'Relatives'.$i.'_Option3'} = $radioOn_URL;
					break;
					case $admission_cfg['relation']['S']:
						${'Relatives'.$i.'_Option4'} = $radioOn_URL;
					break;
				}
			}
			
			#Is Twins
			$isTwinsYes = $radioOff_URL;
			$isTwinsNo = $radioOff_URL;
			if($StuInfo['IsTwins']==1){
				$isTwinsYes = $radioOn_URL;
			}
			else{
				$isTwinsNo = $radioOn_URL;
			}
				
			############################################################### Page 1 Build Here ###########################################

			$page1 = '<body>
						<!--PAGE 1-->
						<div class="page_wrapper">
						
						<!--header area-->
						 <div class="header">
							<table style="border-collapse: collapse;">
								<tr>
									<td rowspan="4"><img src="'.$logo_URL.'" width="80px"/></div></td>
									<td width="	"></td>
									<td><span class="head1">聖馬提亞堂肖珍幼稚園</span></td>
								</tr>
								<tr>
									<td></td>
									<td><span class="head2">ST. MATTHIAS\' CHURCH CHIU CHUN KINDERGARTEN</span></td>
								</tr>
								<tr>
									<td></td>
									<td><span class="info">
										地址: <strong>新界元朗媽廟路九號</strong>&nbsp;&nbsp;&nbsp;
										電話: <strong>2479 4262</strong>&nbsp;&nbsp;&nbsp;        
										傳真: <strong>2479 4441</strong>&nbsp;&nbsp;&nbsp;       
										網址: <strong>http://www.chiuchunkg.edu.hk</strong>&nbsp;&nbsp;&nbsp;
										電子郵箱: <strong>info@chiuchunkg.edu.hk</strong></span>
									</td>
								</tr>
								<tr><td></td><td></td><td></td> </tr>
							</table>
						 </div>
						<!--header area ends-->
						
						 <div style="clear:both"></div>
						
						<!--form class-->  
						 <div class="application_form_top">
							<table class="application_form_table_class">
							<tr>
								<td style="border:none;" width="235"></td>
								<td style="border:none;"></td>
								<td style="border:none;" width="210"></td>
							</tr>
							<tr>
								<td class="bggrey"><span class="title_class">投考意願</span> <span class="small_class">(請在下列</span><span class="small2_class">剔選一項</span><span class="small_class">，塗改後需加簽作實)</span></td>
								<td rowspan="4" class="title2" align="center">幼兒班入學申請表<br /><span class="year">'
								.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'-'.
								date('Y',getEndOfAcademicYear('',$lac->getNextSchoolYearID())).'年度</span></td>
								<td class=" alignright"></td>
							</tr>
							<tr>
								<td class="bggrey"><img height="11" width="11" src="'.$StuApplyClass_option1.'"/>&nbsp;上午班</td>
								<td class=" alignright"></td>
							</tr>
							<tr>
								<td class="bggrey"><img height="11" width="11" src="'.$StuApplyClass_option2.'"/>&nbsp;下午班</td>
								<td class=" alignright"></span></td>
							</tr>
							<tr>
								<td class="bggrey"><img height="11" width="11" src="'.$StuApplyClass_option3.'"/>&nbsp;上午班或下午班均可</td>
								<td class=" alignright"></td>
							</tr>
							<tr>
								<td colspan="3" class="note">注意：請用正楷填寫，並在適當的<img class="radio_button" src="'.$radioOff_URL.'">內加上剔號</td>
							</tr>
							</table>
						 </div>
						<!--form class ends--> 
						 
						<!--form information--> 
						 <div class="application_form_block">
							<table class="application_form_table">
							<tr>
								<td width="130" style="border:none;"></td>
								<td width="220" style="border:none;"></td>
								<td style="border:none;"></td>
								<td width="160" style="border:none;"></td>
							</tr>
							<tr valign="top">
								<td colspan="3" style="border:none; padding-top:5px;" class="border_none"><span class="title">姓名</span></td>
								<td rowspan="8" valign="top" class="border_none" align="right" style="padding-top:10px;"><img width="160" src="'.$photoLink.'"/></td>
							</tr>
							<tr>
								<td colspan="3">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="border:none;" width="40"></td>
										<td style="border:none;" width="200"></td>
										<td style="border:none;" width="40"></td>
										<td style="border:none;"></td>
									</tr>
									<tr valign="top">
										<td valign="top" style="border:none;" class="border_none">中文：</td>
										<td valign="top" style="border:none;" class="border_none">'.(str_replace(',','',$StuInfo['student_name_b5'])).'</td>
										<td valign="top" style="border:none;" class="border_none">英文：</td>
										<td valign="top" style="border:none;" class="border_none">'.(str_replace(',',' ',$StuInfo['student_name_en'])).'</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="border:none; padding-top:5px;" class="border_none"><span class="title">身份證明文件</span></td>
							</tr>
							<tr>
								<td colspan="3">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="border:none;" width="50"></td>
										<td style="border:none;" width="120"></td>
										<td style="border:none;"></td>
										<td style="border:none;" width="205"></td>
									</tr>
									<tr>
										<td style="border:none;" width="50"></td>
										<td style="border:none;" width="120"></td>
										<td style="border:none;"></td>
										<td style="border:none;" width="205"></td>
									</tr>
									<tr>
										<td style="border:none;" class="border_none">種類：</td>
										<td style="border:none;" class="border_none"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuBirthCert_option1.'"/></span>香港出生證明書</span></td>
										<td style="border:none;" class="border_none">&nbsp;</td>
										<td style="border:none;" class="border_none">號碼：'.$StuInfo['birthcertno'].'</td>
									</tr>
									<tr>
										<td style="border:none;" class="border_none">&nbsp;</td>
										<td style="border:none;" class="border_none"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuBirthCert_option2.'"></span>香港身份證</span></td>
										<td colspan="2" style="border:none;" class="border_none"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuBirthCert_option3.'"></span>其他 （請註明：'.$StuBirthCert_Other.'）</span></td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style="border-bottom: none; padding-top:5px;" class="border_r border_bnone"><span class="title">性別</span></td>
								<td style="border-bottom: none; padding-top:5px;" class="border_r border_bnone"><span class="title">出生日期</span> <span class="small">（年/月/日）</span></td>
								<td style="border:none; padding-top:5px;" class="border_none">
									<span class="title">年歲</span>
									<span class="small">（計算至<span class="space w20">'.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'</span>年
									<span class="space w10">'.'9'.'</span>月）</span>
								</td>
							</tr>
							<tr height="25">
								<td class="border_r">
									<span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuGender_option1.'"></span>男</span>
									<span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuGender_option2.'"></span>女</span>
								</td>
								<td class="border_r" align="right">'.$StuInfo['dateofbirth'].'</td>
								<td align="right">
									<span class="space w15">'.$StuAgeYear.'</span>歲
									<span class="space w15">'.$StuAgeMonth.'</span>個月
								</td>
							</tr>
							<tr>
								<td colspan="2" style="border-bottom: none; padding-top:5px;" class="border_r border_bnone"><span class="title">出生地點</span></td>
								<td style="border:none; padding-top:5px;" class="border_none"><span class="title">籍貫</span></td>
							</tr>
							<tr height="25">
								<td colspan="2" class="border_r">
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuPOB_option1.'"></span>
										香港
									</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuPOB_option2.'"></span>
										澳門
									</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuPOB_option3.'"></span>
										中國
									</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuPOB_option4.'"></span>
										其他 （請註明：<span class="space w60">'.$StuPOB_Other.'</span>）
									</span>
								</td>
								<td align="right">
									<span class="space w30">'.$StuInfo['province'].'</span>省
									<span class="space w30">'.$StuInfo['county'].'</span>縣
								</td>
							</tr>
							<tr>
								<td colspan="2" style="border-bottom: none; padding-top:5px;" class="border_r border_bnone"><span class="title">宗教</span></td>
								<td colspan="2" style="border:none; padding-top:5px;" class="border_none"><span class="title">所屬教會</span></td>
							</tr>
							<tr height="25">
								<td colspan="2" class="border_r">
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuReligion_option1.'"></span>
										基督教
									</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuReligion_option2.'"></span>
										天主教
									</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuReligion_option3.'"></span>
										其他 （請註明：<span class="space w85">'.$StuReligion_Other.'</span>）
									</span>
								</td>
								<td colspan="2">'.$StuInfo['Church'].'</td>
							</tr>
							<tr>
								<td colspan="3" style="border-bottom: none; padding-top:5px;" class="border_r border_bnone"><span class="title">住址</span></td>
								<td style="border:none; padding-top:5px;" class="border_none"><span class="title">住址電話</span></td>
							</tr>
							<tr>
								<td colspan="3" class="border_r">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="border:none;" width="40"></td>
										<td style="border:none;" width="200"></td>
										<td style="border:none;" width="40"></td>
										<td style="border:none;" width="200"></td>
									</tr>
									<tr valign="top">
										<td valign="top" style="border:none;" class="border_none">中文：</td>
										<td valign="top" style="border:none;" class="border_none">'.$StuInfo['homeaddresschi'].'</td>
										<td valign="top" style="border:none;" class="border_none">英文：</td>
										<td valign="top" style="border:none;" class="border_none">'.$StuInfo['homeaddress'].'</td>
									</tr>
									<tr valign="top">
										<td style="border:none;" class="border_none"></td>
										<td colspan="3" valign="top" style="border:none;" class="border_none">
											<img class="radio_button" src="'.$HDistrict_option1.'">香港&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img class="radio_button" src="'.$HDistrict_option2.'">九龍&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img class="radio_button" src="'.$HDistrict_option3.'">新界
										</td>
									</tr>
									</table>
								</td>
								<td>'.$StuHomeNo.'</td>
							</tr>
							<tr>
								<td colspan="3" style="border-bottom: none; padding-top:5px;" class="border_r border_bnone">
									<span class="title">通訊地址</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuAddress_Same.'"></span>
										同上
									</span>
								</td>
								<td style="border:none; padding-top:5px;" class="border_none">
									<span class="title">聯絡電話</span>
									<span class="radio_button">
										<span class="radio_button_container"><img class="radio_button" src="'.$StuContact_Same.'"></span>
										同上
									</span>
								</td>
							</tr>
							<tr>
								<td colspan="3" class="border_r">
						        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="40" style="border:none;"></td>
										<td width="200" style="border:none;"></td>
										<td width="40" style="border:none;"></td>
										<td width="200" style="border:none;"></td>
									</tr>
									<tr valign="top">
										<td valign="top" style="border:none;" class="border_none">中文：</td>
										<td valign="top" style="border:none;" class="border_none">'.$StuInfo['ContactAddressChi'].'</td>
										<td valign="top" style="border:none;" class="border_none">英文：</td>
										<td valign="top" style="border:none;" class="border_none">'.$StuInfo['ContactAddress'].'</td>
									</tr>
									<tr valign="top">
										<td style="border:none;" class="border_none"></td>
										<td colspan="3" valign="top" style="border:none;" class="border_none">
											<img class="radio_button" src="'.$CDistrict_option1.'">香港&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img class="radio_button" src="'.$CDistrict_option2.'">九龍&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img class="radio_button" src="'.$CDistrict_option3.'">新界
										</td>
									</tr>
									</table>
						        </td>
								<td>'.$StuContactNo.'</td>
							</tr>
							<tr valign="bottom">
								<td colspan="3" style="border:none; padding-top:5px;" class="border_none"><span class="title">現時就讀學校資料</span></td>
								<td rowspan="2" align="right"><span class="radio_button">
									<span class="radio_button_container"><img class="radio_button" src="'.$StuCurrentClass_option1.'"></span>上午班</span>
									<br />
									<span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuCurrentClass_option2.'"></span>下午班</span>
									<br />級別：<span class="space w40">'.Get_String_Display($currentSchool['OthersPrevSchClass']).'</span>
									<span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$StuCurrentClass_option3.'"></span>全日班</span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="border:none;" width="40"></td>
										<td style="border:none;" width="200"></td>
										<td style="border:none;" width="40"></td>
										<td style="border:none;"></td>
									</tr>
									<tr valign="top">
										<td valign="top" style="border:none;" class="border_none">校名：</td>
										<td valign="top" style="border:none;" class="border_none">'.Get_String_Display($currentSchool['OthersPrevSchName']).'</td>
										<td valign="top" style="border:none;" class="border_none">地址：</td>
										<td valign="top" style="border:none;" class="border_none">'.Get_String_Display($currentSchool['SchoolAddress']).'</td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						 </div>
						<!--form information ends--> 
						
						<!--form parents-->     
						 <div class="application_form_block application_form_block_2nd"> 
							<table class="application_form_table">
						    <tr>
								<td style="border:none;"></td>
								<td style="border:none;" width="120"></td>
							</tr>
							<tr>
								<td colspan="2" style="border:none;" class="border_none padding_none">
									<table width="100%" border="0">
									<tr>
							            <td style="border:none;"></td>
										<td style="border:none;" width="200"></td>
										<td style="border:none;" width="200"></td>
										<td style="border:none;" width="3"></td>
										<td style="border:none;" width="85"></td>
										<td style="border:none;" width="85"></td>
									</tr>
									<tr>
										<td style="border:none;" class="border_none">&nbsp;</td>
										<td style="border:none; padding-top:5px;" class="border_none"><span class="title">父親</span></td>
										<td style="border:none; padding-top:5px;" class="border_none"><span class="title">母親</span></td>
										<td style="border:none;" class="border_none">&nbsp;</td>
										<td colspan="2" style="border:none; padding-top:5px;" class="border_none"><span class="title">監護人</span></td>
									</tr>
									<tr>
										<td align="right" style="border-bottom: 1px solid #ccc;" class="border_l border_rlite border_blite">姓名</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.$GuardianInfo['G1ChineseName'].'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_r border_blite"><span class="border_rlite border_blite">'.$GuardianInfo['G2ChineseName'].'</span></td>
										<td class="border_bdot" style="border-bottom: 1px dotted #ccc;">&nbsp;</td>
										<td style="border-bottom: 1px solid #ccc;" colspan="2" class="border_l border_r border_blite"><span class="border_r border_blite">'.$GuardianInfo['G3ChineseName'].'</span></td>
									</tr>
									<tr>
										<td style="border-bottom: 1px solid #ccc;" class="border_l border_rlite border_blite" align="right">職業</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.$GuardianInfo['G1Occupation'].'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_r border_blite">'.$GuardianInfo['G2Occupation'].'</td>
										<td class="border_bdot" style="border-bottom: 1px dotted #ccc;">&nbsp;</td>
										<td style="border-bottom: 1px solid #ccc;" colspan="2" class="border_l border_r border_blite"><span class="border_r border_blite">'.$GuardianInfo['G3Occupation'].'</span></td>
									</tr>
									<tr>
										<td style="border-bottom: 1px solid #ccc;" class="border_l border_rlite border_blite" align="right">辦事處電話</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.$GuardianInfo['G1WorkNo'].'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_r border_blite"><span class="border_rlite border_blite">'.$GuardianInfo['G2WorkNo'].'</span></td>
										<td class="border_bdot" style="border-bottom: 1px dotted #ccc;">&nbsp;</td>
										<td style="border-bottom: 1px solid #ccc;" colspan="2" class="border_l border_r border_blite"><span class="border_r border_blite">'.$GuardianInfo['G3WorkNo'].'</span></td>
									</tr>
									<tr>
										<td style="border-bottom: 1px solid #ccc;" class="border_l border_rlite border_blite" align="right">手提電話</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.$GuardianInfo['G1MobileNo'].'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_r border_blite"><span class="border_rlite border_blite">'.$GuardianInfo['G2MobileNo'].'</span></td>
										<td class="border_bdot" style="border-bottom: 1px dotted #ccc;">&nbsp;</td>
										<td style="border-bottom: 1px solid #ccc;" colspan="2" class="border_l border_r border_blite"><span class="border_r border_blite">'.$GuardianInfo['G3MobileNo'].'</span></td>
									</tr>
									<tr>
										<td class="border_l border_rlite" align="right">電郵</td>
										<td class="border_rlite"><span class="border_rlite border_blite">'.$GuardianInfo['G1Email'].'</span></td>
										<td class="border_r"><span class="border_rlite"><span class="border_rlite border_blite">'.$GuardianInfo['G2Email'].'</span></span></td>
										<td class="border_bdot" style="border-bottom: 1px dotted #ccc;">&nbsp;</td>
										<td colspan="2" class="border_l border_r border_blite"><span class="border_r border_blite">'.$GuardianInfo['G3Email'].'</span></td>
									</tr>
									<tr>
										<td colspan="3" style="border:none; padding-top:5px;" class="border_none"><span class="title">家庭狀況 </span><span class="small">（請以數字表示）</span></td>
										<td style="border:none;" class="border_none">&nbsp;</td>
										<td style="border-bottom: none; padding-top:5px;" class="border_l border_rlite border_bnone"><span class="title">關係</span></td>
										<td style="border-bottom: none;" class="border_r border_bnone"><span class="text">性別</span></td>
									</tr>
									<tr>
										<td style="border:none;" colspan="3" align="right">
											<span class="underline text_center w20">'.$othersInfo['EBrotherNo'].'</span>兄
											<span class="underline text_center w20">'.$othersInfo['ESisterNo'].'</span>姊
											<span class="underline text_center w20">'.$othersInfo['YBrotherNo'].'</span>弟
											<span class="underline text_center w20">'.$othersInfo['YSisterNo'].'</span>妹
											&nbsp;&nbsp;&nbsp;&nbsp;該生排行<span class="underline text_center w20">'.($othersInfo['EBrotherNo']+$othersInfo['ESisterNo']).'</span>
										</td>
										<td rowspan="2" style="border:none;" class="border_none">&nbsp;</td>
										<td rowspan="2" class="border_l border_rlite">'.$GuardianInfo['G3Relationship'].'</td>
										<td rowspan="2" class="border_r"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$guardianGenderM.'"></span>男</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$guardianGenderF.'"></span>女</span></td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;幼兒是雙胞/多胞胎? 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img class="radio_button" src="'.$isTwinsYes.'">是
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img class="radio_button" src="'.$isTwinsNo.'">否</td>
										<td  style="border-top: none;>
											
										</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="border:none;" class="border_none padding_none">
						        	<table width="100%">
						            <tr>
										<td style="border:none;" width="90"></td>
										<td style="border:none;" width="150"></td>
										<td style="border:none;" width="150"></td>
										<td style="border:none;" width="150"></td>
										<td style="border:none;" width="150"></td>
						      		</tr>
									<tr>
										<td style="border:none; padding-top:5px;" class="border_none" colspan="5"><span class="title">現正就讀或曾經就讀本校之直系親屬</span></td>
									</tr>
									<tr>
										<td style="border-bottom: 1px solid #ccc;" align="right" class="border_l border_rlite border_blite"> 姓名</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.Get_String_Display($relativesInfo['Name_1']).'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.Get_String_Display($relativesInfo['Name_2']).'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_rlite border_blite">'.Get_String_Display($relativesInfo['Name_3']).'</td>
										<td style="border-bottom: 1px solid #ccc;" class="border_r border_blite">'.Get_String_Display($relativesInfo['Name_4']).'</td>
									</tr>
									<tr>
										<td style="border-bottom: 1px solid #ccc;" align="right" class="border_l border_rlite border_blite">關係</td>
										<td style="border-bottom: 1px solid #ccc;" align="center" class="border_rlite border_blite"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives1_Option1.'"></span>父</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives1_Option2.'"></span>母</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives1_Option3.'"></span>兄</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives1_Option4.'"></span>姊</span></td>
										<td style="border-bottom: 1px solid #ccc;" align="center" class="border_rlite border_blite"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives2_Option1.'"></span>父</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives2_Option2.'"></span>母</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives2_Option3.'"></span>兄</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives2_Option4.'"></span>姊</span></td>
										<td style="border-bottom: 1px solid #ccc;" align="center" class="border_rlite border_blite"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives3_Option1.'"></span>父</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives3_Option2.'"></span>母</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives3_Option3.'"></span>兄</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives3_Option4.'"></span>姊</span></td>
										<td style="border-bottom: 1px solid #ccc;" align="center" class="border_r border_blite"><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives4_Option1.'"></span>父</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives4_Option2.'"></span>母</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives4_Option3.'"></span>兄</span><span class="radio_button"><span class="radio_button_container"><img class="radio_button" src="'.$Relatives4_Option4.'"></span>姊</span></td>
									</tr>
									<tr>
										<td align="right" class="border_l border_rlite">班別 / 畢業年份</td>
										<td class="border_rlite">'.Get_String_Display($relativesInfo['YearClass_1']).'</td>
										<td class="border_rlite">'.Get_String_Display($relativesInfo['YearClass_2']).'</td>
										<td class="border_rlite">'.Get_String_Display($relativesInfo['YearClass_3']).'</td>
										<td class="border_r">'.Get_String_Display($relativesInfo['YearClass_4']).'</td>
									</tr>
									</table>
								</td>
							</tr>
							<!--tr>
								<td colspan="2" style="border:none; padding-top:5px;" class="border_none"><span class="title">備註</span></td>
							</tr-->
							<tr>
								<td rowspan="2"></td>
								<td align="right" style="border:none; padding-top:5px;" class="border_none"><span class="title">填表日期</span> <span class="small">（年/月/日）</span></td>
							</tr>
							<tr>
								<td align="right">'.$StuInfo['DateInput'].'</td>
							</tr>
							</table>
						 </div>
						<!--form parent ends-->
						</div>
						<!--PAGE 1 ends-->';
			############################################################### Page 1 Build End ###########################################

			$AdmissionNumber = '<div style="position: absolute; top: 7mm; left: 159mm;"><font style="font-size: 0.9em;">新生會見證編號 :</font><font style="font-family: gg; color:red; font-size: 0.9em;">&#8470; '.$applicationID.'</font></div>';	
			$mpdf->WriteHTML($this->getPDF_CSS());
			$mpdf->WriteHTML($AdmissionNumber);
			$mpdf->WriteHTML($page1);
			$mpdf->WriteHTML($this->getPDF_Page2());
		
		}
		$mpdf->Output();
	}
	
	function getPrintPageContent($schoolYearID,$applicationID, $type=""){ //using $type="teacher" if the form is print from teacher
	
//		global $PATH_WRT_ROOT,$Lang,$kis_lang, $admission_cfg;
//		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
//		$lac = new admission_cust();
//		if($applicationID != ""){
//		//get student information
//		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
//		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
//		foreach($parentInfo as $aParent){
//			if($aParent['type'] == 'F'){
//				$fatherInfo = $aParent;
//			}
//			else if($aParent['type'] == 'M'){
//				$motherInfo = $aParent;
//			}
//			else if($aParent['type'] == 'G'){
//				$guardianInfo = $aParent;
//			}
//		}
//		
//		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
//		
//		//for the 2 new table
//		$studentInfoCust = $lac->getApplicationStudentInfoCust($schoolYearID,'',$applicationID);
//		$relativesInfoCust = $lac->getApplicationRelativesInfoCust($schoolYearID,'',$applicationID);
//		
//		if($_SESSION['UserType']==USERTYPE_STAFF){
//			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
//			if(!is_date_empty($remarkInfo['interviewdate'])){
//				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
//				list($y,$m,$d) = explode('-',$date);
//				if($hour>12){
//					$period = '下午';
//					$hour -= 12;
//				}elseif($hour<12){
//					$period = '上午';
//				}else{
//					$period = '中午';
//				}
//				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
//				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
//				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
//			}else{
//				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
//			上午／下午____時____分';
//			}
//		}
//		else{
//				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
//			上午／下午____時____分';
//		}
//		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
//		$personalPhotoPath = $attachmentList['personal_photo']['link'];
//		$classLevel = $lac->getClassLevel();
//		
////		debug_pr($studentInfo);
////		debug_pr($fatherInfo);
////		debug_pr($motherInfo);
////		debug_pr($guardianInfo);
////		debug_pr($othersInfo);
//		
//		for($i=1; $i<=3; $i++){
//				if($othersInfo['ApplyDayType'.$i] != 0){
//					//$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
//					$dayTypeOption .= "(選擇 ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
//				}
//			}
//		
//		foreach($admission_cfg['BirthCertType'] as $_key => $_type){
//			if($studentInfo['birthcerttype'] == $_type){
//				$birth_cert_type_selection .= ' (';
//				$birth_cert_type_selection .= $Lang['Admission']['BirthCertType'][$_key];
//				$birth_cert_type_selection .= ') ';
//				break;
//			}
//		}
//		
//		$stuNameArr_en = explode(',',$studentInfo['student_name_en']);
//		$stuNameArr_b5 = explode(',',$studentInfo['student_name_b5']);
//		
//		//Header of the page		
//		$x ='<div id="content">
//			<div class="top">
//			<div class="logo"><img src="/includes/admission/eclassk.munsang.edu.hk/images/logo01.png" width="115px"/></div>  
//			<span class="heading">
//			<em>
//			民生書院幼稚園<br />
//			Munsang College Kindergarten<br />
//			'.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'-'.
//					date('Y',getEndOfAcademicYear('',$lac->getNextSchoolYearID())).' 年度<br />
//			</em>
//			報名表<br />
//			Application Form
//			</span>
//			<span class="logo02"><img src="/includes/admission/eclassk.munsang.edu.hk/images/logo02.png" width="70px"/></span>
//			<div class="application_form_no_block">
//			<div class="chinese">報名表編號:</div>
//			<div class="eng">Application Form No.</div>
//			<div class="eng"><span class="underline" style="width:50px;">'.$othersInfo['applicationID'].'</span></div>
//			<div class="chinese">出生証明文件:</div>
//			<div class="eng">Birth Cert:</div>
//			<div class="eng"><span class="underline" style="width:100px;"></span></div>
//			<div class="chinese">備註:</div>
//			<div class="eng">Remark:</div>
//			<div class="eng"><span class="underline" style="width:110px;"></span></div>
//			<div class="school">(只供校方填寫)<br />School use only</div>
//			</div>
//			</div> <!--end_top-->
//			
//			<div style="clear:both"></div>
//			<div class="application_form_block">
//			<table class="application_form_table_class">
//			  <tr>
//			    <td width="65" rowspan="3" style="border:none; line-height:16px;">申請班級&nbsp;*<br/> Class<br/>Applied For</td>
//			    <td width="119" rowspan="3" class="title">幼兒班<br />Nursery Class</td>
//			    <td width="242"> 
//			    <span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Cantonese A.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
//				&nbsp;&nbsp;上午英粵班&nbsp;&nbsp; Eng / Cantonese A.M.    </td>
//			    <td width="329" rowspan="3"  style="border:none; border-left:1px solid #000000;">如申請的學習時段額滿，是&nbsp; 
//			    
//			    <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> / &nbsp;否&nbsp;  <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span><br />願意由學校重新編配。<br />If the session I have applied for is full,<br /> 
//			    I will &nbsp;
//			       <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'Y'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>  / &nbsp;will not &nbsp; <span class="check_box">'.($othersInfo['OthersIsConsiderAlternative'] == 'N'?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
//			      consider the alternative session.</td>
//			    </tr>
//			  <tr>
//			    <td >
//				<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Cantonese P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span> 
//			         &nbsp;&nbsp;下午英粵班&nbsp;&nbsp; Eng / Cantonese P.M.    </td>
//			    </tr>
//			  <tr>
//			    <td >
//				<span class="check_box">'.(strpos($classLevel[$othersInfo['classLevelID']],'Mandarin P.M.')?'<img src="/includes/admission/eclassk.munsang.edu.hk/images/checkbox.png"></img>':'').'</span>       
//			        &nbsp;&nbsp;下午英普班&nbsp;&nbsp; Eng / Mandarin P.M.    </td>
//			    </tr>
//			</table>
//			
//			
//			
//			<table class="application_form_table_information"> 
//			  <tr>
//			    <td width="84" rowspan="2" class="border_t_l_r_none" style="text-align:center;">姓名&nbsp;*<br />
//			      Name</td>
//			    <td width="58" class="border_t_r_none">中文<br />
//			      Chinese</td>
//			    <td colspan="3" class="border_t_l_r_none">'.$stuNameArr_b5[0].' '.$stuNameArr_b5[1].'</td>
//			    <td width="180" rowspan="6" class="border_t_r_b_none" ><div class="photo"><img src="'.$personalPhotoPath.'" style="height:100%;max-width: 4cm;max-height: 4.5cm" /><!--<span class="photo_txt">近照<br />Photo</span>--></div></td>
//			  </tr>
//			  <tr>
//			    <td class="border_t_r_none">英文<br />
//			      English</td>
//			    <td colspan="3" class="border_t_l_r_none">'.$stuNameArr_en[0].' '.$stuNameArr_en[1].'</td>
//			    </tr>
//			  <tr>
//			    <td colspan="2" class="border_t_l_r_none">出生日期&nbsp;*<br />Date of Birth</td>
//			    <td class="border_t_l_r_none">'.substr($studentInfo['dateofbirth'], 0, 4).' 年 '.substr($studentInfo['dateofbirth'], 5, 2).' 月 '.substr($studentInfo['dateofbirth'], 8, 2).' 日</td>
//			    <td width="68" colspan="-2"  class="border_t_r_none">性別&nbsp;*<br />
//			      Sex</td>
//			    <td width="140"  class="border_t_l_r_none">'.$Lang['Admission']['genderType'][$studentInfo['gender']].'<br/>'.$studentInfo['gender'].'</td>
//			  </tr>
//			  <tr>
//			    <td colspan="2" class="border_t_l_r_none">出生地點&nbsp;*<br />Place of Birth</td>
//			    <td class="border_t_l_r_none">'.$studentInfo['placeofbirth'].'</td>
//			    <td colspan="-2"  class="border_t_r_none">宗教<br />Religion</td>
//			    <td  class="border_t_l_r_none">'.$studentInfo['religion'].'</td>
//			  </tr>
//			  <tr>
//			    <td colspan="2" class="border_t_l_r_none">出生證明書號碼&nbsp;*<br />Birth Certificate Number</td>
//			    <td width="217" class="border_t_l_r_none">'.$studentInfo['birthcertno'].'</td>
//			    <td colspan="-2"  class="border_t_r_none">電話&nbsp;*<br />Telephone</td>
//			    <td  class="border_t_l_r_none">'.$studentInfo['homephoneno'].'</td>
//			  </tr>
//			  <tr>
//			    <td class="border_none">地址&nbsp;*<br />Address</td>
//			    <td colspan="4" class="border_none">'.$studentInfo['homeaddress'].'</td>
//			    </tr>
//			</table>
//			
//			
//			<table class="application_form_table_parents">
//			  <tr>
//			    <td width="145"  class="border_t_l_r_none">&nbsp;</td>
//			    <th width="272"  class="border_t_r_none">父親 Father</th>
//			    <th width="276"  class="border_t_r_none">母親 Mother</th>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">姓名&nbsp;*<br />Name
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['parent_name_b5'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['parent_name_b5'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">公司<br />Company
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['companyname'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['companyname'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">地址<br />Address
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['companyaddress'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['companyaddress'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">職業<br />Occupation
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['occupation'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['occupation'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">手提電話&nbsp;*<br />Mobile Phone No.
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['mobile'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['mobile'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">電郵地址<br />E-mail Address
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['email'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['email'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_t_l_r_none">教育程度<br />Level of Education
//			</td>
//			    <td class="border_t_r_none"><center>'.$fatherInfo['levelofeducation'].'&nbsp;</center></td>
//			    <td class="border_t_r_none"><center>'.$motherInfo['levelofeducation'].'&nbsp;</center></td>
//			  </tr>
//			  <tr>
//			    <td class="title border_none">學校名稱<br />Name of School
//			</td>
//			    <td class="border_t_r_b_none"><center>'.$fatherInfo['lastschool'].'&nbsp;</center></td>
//			    <td  class="border_t_r_b_none"><center>'.$motherInfo['lastschool'].'&nbsp;</center></td>
//			  </tr>
//			</table>
//			
//			<table  class="application_form_table_record">
//			  <tr>
//			    <td colspan="3" class="title border_t_l_r_none">幼兒過去入學記錄 Record of Previous Schooling of the Child*</td>
//			    </tr>
//			  <tr>
//			    <th width="159"  class="border_t_l_r_none">年份 Year</th>
//			    <th width="160" class="border_t_r_none">級別 Class</th>
//			    <th width="440"  class="border_t_r_none">學校名稱 Name of School</th>
//			    </tr>';
//			    
//			  	foreach($studentInfoCust as $aStudentInfoCust){
//					if(trim($aStudentInfoCust['OthersPrevSchYear']) != '' || trim($aStudentInfoCust['OthersPrevSchClass']) != '' || trim($aStudentInfoCust['OthersPrevSchName']) != '')
//					$x .= '<tr>
//							<td class="border_t_l_r_none"><center>'.$aStudentInfoCust['OthersPrevSchYear'].'&nbsp;</center></td>
//							<td class="border_t_r_none"><center>'.$aStudentInfoCust['OthersPrevSchClass'].'&nbsp;</center></td>
//							<td class="border_t_r_none"><center>'.$aStudentInfoCust['OthersPrevSchName'].'&nbsp;</center></td>
//						</tr>';
//				}
//				
//			$x.='</table>
//			
//			<table  class="application_form_table_record">
//			  <tr>
//			    <td colspan="4" class="title border_t_l_r_none" style="text-align:left;">請列出曾經或現正在本校就讀/工作的親屬資料 (如有)<br />
//			Please list all the relatives who have studying / working or having studied /worked at our College (if applicable)
//			</td>
//			    </tr>
//			  <tr>
//			    <th width="131" class="border_t_l_r_none">年份 Year</th>
//			    <th width="133"  class="border_t_r_none">姓名 Name</th>
//			    <th width="191"  class="border_t_r_none">就讀班別 / 職位<br />
//			      Class / Position
//			</th>
//			    <th width="300"  class="border_t_r_none">與申請人的關係<br />
//			      Relationship with the Applicant
//			</th>
//			  </tr>';
//			  
//				$hasRelativesInfoCust = 0;
//				foreach($relativesInfoCust as $aRelativesInfoCust){
//					if(trim($aRelativesInfoCust['OthersRelativeStudiedYear']) != '' || trim($aRelativesInfoCust['OthersRelativeStudiedName']) != '' || trim($aRelativesInfoCust['OthersRelativeClassPosition']) != '' || trim($aRelativesInfoCust['OthersRelativeRelationship']) != ''){
//						$x .= '<tr>
//							<td class="border_t_l_r_none"><center>'.$aRelativesInfoCust['OthersRelativeStudiedYear'].'</center></td>
//							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeStudiedName'].'</center></td>
//							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeClassPosition'].'</center></td>
//							<td class="border_t_r_none"><center>'.$aRelativesInfoCust['OthersRelativeRelationship'].'</center></td>
//						</tr>';
//						$hasRelativesInfoCust++;
//					}
//				}
//				if($hasRelativesInfoCust < 2){
//					if($hasRelativesInfoCust == 0){
//						$x .= '<tr>
//							    <td  class="border_t_l_r_none">&nbsp;</td>
//							    <td  class="border_t_r_none">&nbsp;</td>
//							    <td  class="border_t_r_none">&nbsp;</td>
//							    <td  class="border_t_r_none">&nbsp;</td>
//							  </tr>';
//					}
//					$x .= '<tr>
//						    <td  class="border_none">&nbsp;</td>
//						    <td  class="border_t_r_b_none">&nbsp;</td>
//						    <td  class="border_t_r_b_none">&nbsp;</td>
//						    <td  class="border_t_r_b_none">&nbsp;</td>
//						  </tr>';
//				}
//				 
//			$x.='</table>
//			
//			<div class="footer">
//			<span>
//			根據個人資料(私穩)條例，以上個人資料只用於報讀幼稚園，完成報名程序後，所有資料將會註銷。<br />
//			According to the Personal Data (Privacy) Ordinance, the above Personal data will be used for Kindergarten application only. All data will be written off after application procedure completed.</span>
//			
//			<span>
//			備註Note:
//			<ul>
//			<li>必須提供標示*的資料，否則申請將不能進行，並視作放棄論。<br />
//			   The * marked information are compulsory and must be provided in order for this application to be processed. Failure to provide such data will be treated as withdrawn.
//			</li>
//			<li>沒有標示*的資料為非必要資料，家長/申請人可自行決定提供與否。<br />
//			   Information not marked with * are optional. Parent/Applicant can decide whether or not to provide the information.
//			</li>
//			</ul>
//			</span>
//			</div>
//			</div><!--end_application_form_block-->
//			</div>';	
//			return $x;
//		}
//		else
//			return false;
	}
	
	function getPrintPageCss(){
//		return '<style type="text/css">
//			body { padding:0; margin:0; }
//			body{	font-family: "Times New Roman", Times, serif,"新細明體" "細明體", "微軟正黑體";color: #000000;}
//			#content{ width:21cm; height:29.7cm; background-color:#fff;  margin:0 auto;}
//			.top{ padding:0; float:left; width:21cm;}
//			.logo{width:115px; height:120px; float:left; margin-right:1.8cm; }
//			.logo02{width:80px; height:95px; float:left; }
//			/*--top--*/
//			.top .heading{float:left; margin-right:1.2cm; text-align:center; line-height:23px;font-size:1.2em;}
//			.top .heading em{font-size:1.1em; line-height:28px; font-style:normal}
//			/*--application_form_no_block--*/
//			.application_form_no_block{ float:right; padding:5px 10px 3px 10px; border:2px dotted #000000;}
//			.application_form_no_block .chinese{font-size:0.75em; clear:both; display:block;}
//			.application_form_no_block .eng{font-size:0.7em; float:left; margin-bottom:3px; line-height:15px;}
//			.application_form_no_block .school{font-size:0.65em; clear:both; display:block; text-align:center;}
//			.underline { border-bottom:1px solid #666;color:#000; float:left; line-height:15px; font-size:1.3em; padding:0px 5px 2px 5px; display:block; height:12px;}
//			.application_form_block{clear:both; border:1px solid #000; width:100%; margin-top:5px;}
//			/* -----form_table_class----*/
//			table.application_form_table_class{  border-collapse:collapse; border:1px solid #666; border-bottom:none; font-size:0.8em; margin:10px auto 0px auto; width:20.5cm; height:20px;  }
//			.application_form_table_class td {font-weight: normal;color: #000;text-align:left; line-height:15px;padding:2px 5px 3px 5px; height:15px;}
//			.application_form_table_class td.title {font-weight:bold; color: #000;text-align:center;}
//			.photo{border:1px dotted #000; width:4cm; height:4.5cm; display:block; margin:auto; text-align:center;}
//			.photo_txt{margin:60px auto; display: inline-block; width:50px; text-align:center; line-height:22px; font-size:1.2em;}
//			
//			/* -----form_table_information----*/
//			table.application_form_table_information{ border-collapse:collapse; border:1px solid #666; font-size:0.8em;  margin:0px auto 0px auto; width:20.5cm;}
//			.application_form_table_information th {background-color: #f5c567; font-weight: bold; color: #000;text-align:left;  padding:10px; border:1px solid #666; }
//			.application_form_table_information td {font-weight: normal;color: #000;text-align:left; line-height:15px; border:1px solid #666; padding:3px 5px 2px 5px;}
//			.application_form_table_information td.title {font-weight: normal; color: #000;text-align:center;}
//			
//			/*-----form_table_parents----*/
//			table.application_form_table_parents{ border-collapse:collapse; border:1px solid #666; font-size:0.8em;  margin:10px auto 0px auto; width:20.5cm;}
//			.application_form_table_parents th {background-color: #f5c567; font-weight: normal; color: #000;text-align:center;  padding:10px 5px 10px 5px; border:1px solid #666; background-color:#e2e2e2;}
//			.application_form_table_parents td {font-weight: normal;color: #000;text-align:left; line-height:15px; border:1px solid #666;  padding:3px 5px 2px 5px;}
//			.application_form_table_parents td.title {text-align:center; padding:3px 5px 2px 5px;}
//			
//			/*-----form_table_record----*/
//			table.application_form_table_record{ border-collapse:collapse; border:1px solid #666; font-size:0.9em;  margin:10px auto 5px auto; width:20.5cm;}
//			.application_form_table_record th {background-color: #e2e2e2; font-weight:normal; color: #000;text-align:center;  padding:2px 5px 2px 5px; border:1px solid #666;  line-height:15px; font-size:0.9em;}
//			.application_form_table_record td {font-weight: normal;color: #000;text-align:left; line-height:13px; border:1px solid #666;  padding:3px 5px 3px 5px; font-size:0.9em; height:20px;}
//			.application_form_table_record td.title {text-align:center; padding:3px 5px 2px 5px;  line-height:13px;}
//			
//			/*------check_box-----*/
//			.check_box{ width:11px; height:11px; border:1px solid #1f1f1f; display:inline-block; vertical-align:middle; }			
//
//			/*----footer---*/
//			.footer{font-weight: normal;color: #000;text-align:left; line-height:12px; width:20.5cm; font-size:0.7em;  margin:7px auto 7px auto;}
//			.footer span{ margin-bottom:7px; display:block; line-height:15px;}
//			.footer ul{ margin:0; padding-left:15px;}
//			.footer li{list-style:decimal; }
//			/*---border----*/
//			td.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
//			th.border_t_l_r_none{border-top:none;border-left:none;border-right:none;}
//			td.border_t_r_none{border-top:none;border-right:none;}
//			th.border_t_r_none{border-top:none;border-right:none;}
//			td.border_t_r_b_none{border-top:none;border-right:none;border-bottom:none;}
//			td.border_none{border:none;}
//			@media print
//			{    
//			    .print_hide, .print_hide *
//			    {
//			        display: none !important;
//			    }
//			}
//			</style>';
	}
	
	function getDisableCheckBox($targetElementID, $displayString){
		
		return '<input type="checkbox" id="disable'.$targetElementID.'" name="disable'.$targetElementID.'" value="1" onclick="disableNotApplicable(\''.$targetElementID.'\')"><label for="disable'.$targetElementID.'">'.$displayString.'</label>'; 
	}
	
	
	function getSelectByConfig($config_array,$select_name="",$selected_value=""){
		global $Lang,$kis_lang;
		foreach((array)$config_array as $_key => $value){
			
			$result_array[$value] = $Lang['Admission']['CHIUCHUNKG'][$_key];
			if($result_array[$value] == ''){
				$result_array[$value] = $kis_lang['Admission']['CHIUCHUNKG'][$_key];
			}
		}
		$PlsSelectLang = $Lang['Admission']['CHIUCHUNKG']['PleaseSelect'] == ''? $kis_lang['Admission']['CHIUCHUNKG']['PleaseSelect']:$Lang['Admission']['CHIUCHUNKG']['PleaseSelect'];
		//$tags, $selected="", $all=0, $noFirst=0, $FirstTitle="",
		return getSelectByAssoArray($result_array,'name="'.$select_name.'" id="'.str_replace('[]','',$select_name).'" onchange="showOtherTextField(\''.$select_name.'\',this.value)"',$selected_value,0,0,$PlsSelectLang);
	}
	
	var $IsConfirm;
	function getFormElement($ElementName,$Section=''){
		$IsConfirm = $this->IsConfirm;
		global $admission_cfg,$intranet_root;
		global $Lang;
		//include_once($intranet_root.'/includes/admission/chiuchunkg.eclass.hk/form_element.php');
		
		$formElementArray = array(
			"ApplyDayType" => array( $this->getSelectByConfig($admission_cfg['classtype'],'ApplyDayType') ),
			"FormStudentInfo_StuChiName" => array(
				'<table style="font-size: 13px">
				<tr><td width="30px">
				('.$Lang['Admission']['csm']['surname_b5'].')</td><td>'.'<input name="studentssurname_b5" type="text" id="studentssurname_b5" class="textboxtext" />'.'</td></tr>
				<tr><td width="30px">
				('.$Lang['Admission']['csm']['firstname_b5'].')</td><td>'.'<input name="studentsfirstname_b5" type="text" id="studentsfirstname_b5" class="textboxtext" />'.'</td></tr>
				</table>' 
			),
			"FormStudentInfo_StuEngName"  => array(
				'<table style="font-size: 13px">
				<tr><td width="30px">
				('.$Lang['Admission']['csm']['surname_en'].')</td><td>'.'<input name="studentssurname_en" type="text" id="studentssurname_en" class="textboxtext" />'.'</td></tr>
				<tr><td width="30px">
				('.$Lang['Admission']['csm']['firstname_en'].')</td><td>'.'<input name="studentsfirstname_en" type="text" id="studentsfirstname_en" class="textboxtext" />'.'</td></tr>
				</table>'
			),
			"FormStudentInfo_StuGender" => array($this->Get_Radio_Button('FormStudentInfo_StuGender1', 'FormStudentInfo_StuGender', 'M', '0','',$Lang['Admission']['genderType']['M'].' M').'&nbsp;&nbsp;'
												.$this->Get_Radio_Button('FormStudentInfo_StuGender2', 'FormStudentInfo_StuGender', 'F', '0','',$Lang['Admission']['genderType']['F'].' F')),
			"StudentBirthCertNo" => array( '<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px">' ),
			"FormStudentInfo_StuIDType"  => array(  $this->getSelectByConfig($admission_cfg['BirthCertType'],'FormStudentInfo_StuIDType')  ),
			"FormStudentInfo_StuIDType_Other" => array( '<span id="StuID_Other" style="display:none;">請註明:<input name="FormStudentInfo_StuIDType_Other" type="text" id="FormStudentInfo_StuIDType_Other" class="textboxtext" style="width:100px"></span>' ),
			"FormStudentInfo_DOB"  => array( '<input name="FormStudentInfo_DOB" type="text" id="FormStudentInfo_DOB" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)'),
			"FormStudentInfo_POB" => array( $this->getSelectByConfig($admission_cfg['placeofbirth'],'FormStudentInfo_POB') ),
			"FormStudentInfo_POB_Other" => array( '<span id="POB_Other" style="display:none;">請註明:'.'<input name="FormStudentInfo_POB_Other" type="text" id="FormStudentInfo_POB_Other" class="textboxtext" style="width:80px"></span>'),
			"FormStudentInfo_Province"  => array( '<input name="FormStudentInfo_Province" type="text" id="FormStudentInfo_Province" class="textboxtext" style="width:50px">' ),
			"FormStudentInfo_County" => array( '<input name="FormStudentInfo_County" type="text" id="FormStudentInfo_County" class="textboxtext" style="width:50px">' ),
			"FormStudentInfo_Religion"  => array( $this->getSelectByConfig($admission_cfg['religion'],'FormStudentInfo_Religion') ),
			"FormStudentInfo_Religion_Other" => array( '<span id="Religion_Other" style="display:none;">請註明:'.'<input name="FormStudentInfo_Religion_Other" type="text" id="FormStudentInfo_Religion_Other" class="textboxtext" style="width:80px"></span>' ),
			"FormStudentInfo_Church"  => array( '<input name="FormStudentInfo_Church" type="text" id="FormStudentInfo_Church" class="textboxtext">' ),
			"HomeAddressDistrict" => array ( $this->getSelectByConfig($admission_cfg['addressdistrict'],'HomeAddressDistrict') ),
			"ContactAddressDistrict" => array ( $this->getSelectByConfig($admission_cfg['addressdistrict'],'ContactAddressDistrict') ),
//			"HomeAddressDistrict" => array ($this->Get_Radio_Button('HomeAddressDistrict1', 'HomeAddressDistrict', $admission_cfg['addressdistrict']['hk'], '0','',$Lang['Admission']['CHIUCHUNKG']['hk']).'&nbsp;&nbsp;'
//											.$this->Get_Radio_Button('HomeAddressDistrict2', 'HomeAddressDistrict', $admission_cfg['addressdistrict']['kln'], '0','',$Lang['Admission']['CHIUCHUNKG']['kln'])
//											.$this->Get_Radio_Button('HomeAddressDistrict3', 'HomeAddressDistrict', $admission_cfg['addressdistrict']['nt'], '0','',$Lang['Admission']['CHIUCHUNKG']['nt'])),
//			"ContactAddressDistrict" => array ($this->Get_Radio_Button('ContactAddressDistrict1', 'ContactAddressDistrict', $admission_cfg['addressdistrict']['hk'], '0','',$Lang['Admission']['CHIUCHUNKG']['hk']).'&nbsp;&nbsp;'
//											.$this->Get_Radio_Button('ContactAddressDistrict2', 'ContactAddressDistrict', $admission_cfg['addressdistrict']['kln'], '0','',$Lang['Admission']['CHIUCHUNKG']['kln'])
//											.$this->Get_Radio_Button('ContactAddressDistrict3', 'ContactAddressDistrict', $admission_cfg['addressdistrict']['nt'], '0','',$Lang['Admission']['CHIUCHUNKG']['nt'])),
			"FormStudentInfo_HomeAddrChi" => array( '<input type="text" name="FormStudentInfo_HomeAddrChi" class="textboxtext" id="FormStudentInfo_HomeAddrChi">' ),
			"FormStudentInfo_HomeAddrEng"  => array( '<input type="text" name="FormStudentInfo_HomeAddrEng" class="textboxtext" id="FormStudentInfo_HomeAddrEng">' ),
			"FormStudentInfo_HomeTel" => array( '<input name="FormStudentInfo_HomeTel" type="text" id="FormStudentInfo_HomeTel" class="textboxtext" maxlength="8">' ),
			"FormStudentInfo_ContactAddrChi"  => array( '<input type="text" name="FormStudentInfo_ContactAddrChi" class="textboxtext" id="FormStudentInfo_ContactAddrChi">' ),
			"FormStudentInfo_ContactAddrEng" => array( '<input type="text" name="FormStudentInfo_ContactAddrEng" class="textboxtext" id="FormStudentInfo_ContactAddrEng">' ),
			"FormStudentInfo_ContactTel"  => array( '<input name="FormStudentInfo_ContactTel" type="text" id="FormStudentInfo_ContactTel" class="textboxtext" maxlength="8">' ),
			"disableFormStudentInfo_ContactTel" => array($this->getDisableCheckBox('FormStudentInfo_ContactTel',$Lang['Admission']['CHIUCHUNKG']['SameAsAbove'])),
			"disableFormStudentInfo_ContactAddr" => array($this->getDisableCheckBox('FormStudentInfo_ContactAddr',$Lang['Admission']['CHIUCHUNKG']['SameAsAbove'])),
			"disableCurrentSchool" => array($this->getDisableCheckBox('CurrentSchool',$Lang['Admission']['Nil'])),
			"CurrentSchoolName" => array( '<input name="CurrentSchoolName" type="text" id="CurrentSchoolName" class="textboxtext" >'),
			"CurrentSchoolAddr"  => array( '<input name="CurrentSchoolAddr" type="text" id="CurrentSchoolAddr" class="textboxtext" >'),
			"CurrentSchoolClassLv" => array( '<input name="CurrentSchoolClassLv" type="text" id="CurrentSchoolClassLv" class="textboxtext" >' ),
			"CurrentSchoolDayType" => array( $this->getSelectByConfig($admission_cfg['classtype2'],'CurrentSchoolDayType') ),
			"G1ChineseName"  => array( '<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" />'),
			"G1Occupation" => array( '<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />' ),
			"G1WorkNo"  => array( '<input name="G1WorkNo" type="text" id="G1WorkNo" class="textboxtext" maxlength="8" />'),
			"G1MobileNo" => array( '<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" maxlength="8" />' ),
			"G1Email"  => array( '<input name="G1Email" type="text" id="G1Email" class="textboxtext" />' ),
			"G2ChineseName" => array( '<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" />' ),
			"G2Occupation"  => array( '<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />' ),
			"G2WorkNo" => array( '<input name="G2WorkNo" type="text" id="G2WorkNo" class="textboxtext" maxlength="8" />' ),
			"G2MobileNo"  => array( '<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" maxlength="8" />' ),
			"G2Email" => array( '<input name="G2Email" type="text" id="G2Email" class="textboxtext" />' ),
			"G3ChineseName"  => array( '<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" />' ),
			"G3Occupation" => array( '<input name="G3Occupation" type="text" id="G3Occupation" class="textboxtext" />' ),
			"G3WorkNo"  => array( '<input name="G3WorkNo" type="text" id="G3WorkNo" class="textboxtext" maxlength="8" />'),
			"G3MobileNo" => array( '<input name="G3MobileNo" type="text" id="G3MobileNo" class="textboxtext" maxlength="8" />'),
			"G3Email"  => array( '<input name="G3Email" type="text" id="G3Email" class="textboxtext" />'),
			"G3Relationship" => array( '<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" style="width:20%;" />' ),
			"G3Gender"  => array( $this->Get_Radio_Button('G3Gender1', 'G3Gender', 'M', '0','',$Lang['Admission']['genderType']['M'].' M').'&nbsp;&nbsp;'.$this->Get_Radio_Button('G3Gender2', 'G3Gender', 'F', '0','',$Lang['Admission']['genderType']['F'].' F')),
			"NumElderBro" => array( '<input name="NumElderBro" type="text" id="NumElderBro" class="textboxtext" maxlength="1" style="width:20px">' ),
			"NumElderSis"  => array( '<input name="NumElderSis" type="text" id="NumElderSis" class="textboxtext" maxlength="1" style="width:20px">' ),
			"NumYoungBro" => array( '<input name="NumYoungBro" type="text" id="NumYoungBro" class="textboxtext" maxlength="1" style="width:20px">' ),
			"NumYoungSis"  => array( '<input name="NumYoungSis" type="text" id="NumYoungSis" class="textboxtext" maxlength="1" style="width:20px">' ),
			"StuRankInFamily" => array( '<input name="StuRankInFamily" type="text" id="StuRankInFamily" class="textboxtext" maxlength="1" style="width:20px">' ),
			"IsTwins" => array($this->Get_Radio_Button('IsTwins1', 'IsTwins', '1', '0','',$Lang['Admission']['yes']).'&nbsp;&nbsp;'
							  .$this->Get_Radio_Button('IsTwins2', 'IsTwins', '0', '0','',$Lang['Admission']['no'])),
			"TwinsIDNo" => array( $Lang['Admission']['CHIUCHUNKG']['isTwinsApplied'].'<input type ="text" id="TwinsIDNo" name="TwinsIDNo" />' ),
			"OthersRelativeStudiedName1" => array('<input name="OthersRelativeStudiedName1" type="text" id="OthersRelativeStudiedName1" class="textboxtext" />'),
			"OthersRelativeStudiedName2" => array('<input name="OthersRelativeStudiedName2" type="text" id="OthersRelativeStudiedName2" class="textboxtext" />'),
			"OthersRelativeStudiedName3" => array('<input name="OthersRelativeStudiedName3" type="text" id="OthersRelativeStudiedName3" class="textboxtext" />'),
			"OthersRelativeStudiedName4" => array('<input name="OthersRelativeStudiedName4" type="text" id="OthersRelativeStudiedName3" class="textboxtext" />'),
			"OthersRelativeRelationship1" => array( '<center>'.$this->getSelectByConfig($admission_cfg['relation'],'OthersRelativeRelationship1').'</center>' ),
			"OthersRelativeRelationship2" => array( '<center>'.$this->getSelectByConfig($admission_cfg['relation'],'OthersRelativeRelationship2').'</center>' ),
			"OthersRelativeRelationship3" => array( '<center>'.$this->getSelectByConfig($admission_cfg['relation'],'OthersRelativeRelationship3').'</center>' ),
			"OthersRelativeRelationship4" => array( '<center>'.$this->getSelectByConfig($admission_cfg['relation'],'OthersRelativeRelationship4').'</center>' ),
			"OthersRelativeStudiedYear1" => array('<input name="OthersRelativeStudiedYear1" type="text" id="OthersRelativeStudiedName1" class="textboxtext" />'),
			"OthersRelativeStudiedYear2" => array('<input name="OthersRelativeStudiedYear2" type="text" id="OthersRelativeStudiedName2" class="textboxtext" />'),
			"OthersRelativeStudiedYear3" => array('<input name="OthersRelativeStudiedYear3" type="text" id="OthersRelativeStudiedName3" class="textboxtext" />'),
			"OthersRelativeStudiedYear4" => array('<input name="OthersRelativeStudiedYear4" type="text" id="OthersRelativeStudiedName3" class="textboxtext" />')
		);
		$returnValue = '';
		if($IsConfirm){
			global $formData;
			//$returnValue = '(!--'.$ElementName.'--!)';
			
			if($Section == 'Parent'){
				
				switch($ElementName){
					case "OthersRelativeRelationship1":
					case "OthersRelativeRelationship2":
					case "OthersRelativeRelationship3":
					case "OthersRelativeRelationship4":
						if(in_array($formData[$ElementName], $admission_cfg['relation'])){
							$returnValue = '<center>'.$this->getSelectedValueLang($formData[$ElementName],$admission_cfg['relation']).'</center>';
						}
						else{
							$returnValue = '<center> -- </center>';
						}
					break;
					default:
						$returnValue = '<center>'.$formData[$ElementName].'</center>';
				}
			}
			else{
				switch($ElementName){
				case "FormStudentInfo_StuChiName":
					$returnValue = $formData['studentssurname_b5'].$formData['studentsfirstname_b5'];
				break;
				case "FormStudentInfo_StuEngName":
					$returnValue = $formData['studentssurname_en'].' '.$formData['studentsfirstname_en'];
				break;
				case "StudentBirthCertNo":
					$returnValue = ' ('.$formData[$ElementName].')';
				break;
				case "FormStudentInfo_StuIDType":
					if($formData[$ElementName] == $admission_cfg['BirthCertType']['other']){
						$returnValue = '';	
					}
					else{
						$returnValue = $this->getSelectedValueLang($formData[$ElementName],$admission_cfg['BirthCertType']);
					}
				break;
				case "FormStudentInfo_POB":
					if($formData[$ElementName] == $admission_cfg['placeofbirth']['other']){
						$returnValue = '';	
					}
					else{
						$returnValue = $this->getSelectedValueLang($formData[$ElementName],$admission_cfg['placeofbirth']);
					}
				break;
				case "FormStudentInfo_Religion":
					if($formData[$ElementName] == $admission_cfg['religion']['other']){
						$returnValue = '';	
					}
					else{
						$returnValue = $this->getSelectedValueLang($formData[$ElementName],$admission_cfg['religion']);
					}
				break;
				case "FormStudentInfo_ContactTel":
					if($formData['disableFormStudentInfo_ContactTel']){
						$returnValue = $Lang['Admission']['CHIUCHUNKG']['SameAsAbove'];	
					}
					else{
						$returnValue = $formData[$ElementName];
					}
				break;
				case "FormStudentInfo_ContactAddrChi":
					if($formData['disableFormStudentInfo_ContactAddr']){
						$returnValue = $Lang['Admission']['CHIUCHUNKG']['SameAsAbove'];	
					}
					else{
						$returnValue = $formData[$ElementName];
					}
				break;
				case "FormStudentInfo_ContactAddrEng":
					if($formData['disableFormStudentInfo_ContactAddr']){
						$returnValue = $Lang['Admission']['CHIUCHUNKG']['SameAsAbove'];	
					}
					else{
						$returnValue = $formData[$ElementName];
					}
				break;
				case "CurrentSchoolDayType":
					if($formData[$ElementName]!=''){
						$returnValue = ' ('.$this->getSelectedValueLang($formData[$ElementName],$admission_cfg['classtype2']).')';
					}
				break;
				case "IsTwins":
					if($formData[$ElementName] == 1){
						$returnValue = $Lang['Admission']['yes'];
					}else{
						$returnValue = $Lang['Admission']['no'];
					}
				break;
				case "TwinsIDNo":
					if($formData[$ElementName] == '' || $formData[$ElementName]== ' -- '){
						$returnValue = '';
					}
					else{
						$returnValue = '('.$formData[$ElementName].')';
					}
				break;
				case "disableFormStudentInfo_ContactTel":
				case "disableFormStudentInfo_ContactAddr":
					$returnValue = '';
				break;
				case "disableCurrentSchool":
					if($formData[$ElementName]){
						$returnValue = ' - '.$Lang['Admission']['Nil'];
					}
				break;
				case "G3Relationship":
					$returnValue = ' ('.$formData[$ElementName];
				break;
				case "G3Gender":
					$returnValue = '-'.$formData[$ElementName].')';
				break;
				default:
					$returnValue = $formData[$ElementName];
				}

			}
			
		}else{
			
			$returnValue = $formElementArray[$ElementName][0];				
			
		}
		return $returnValue ;
	}
	
	function getSelectedValueLang($selectedValue,$ConfigArr){
		global $Lang,$kis_lang;
				
		foreach($ConfigArr as $_key => $_val){
			if($selectedValue == $_val){
				
				$returnValue = $Lang['Admission']['CHIUCHUNKG'][$_key];
				if($returnValue == ''){
					$returnValue = $kis_lang['Admission']['CHIUCHUNKG'][$_key];
				}
				break;
			}
		}
		return $returnValue;
	}
}
?>