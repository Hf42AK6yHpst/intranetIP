<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">

var isInternalUse = <?=($lac->isInternalUse($_GET['token']))? 'true' : 'false'; ?>;

kis.datepicker('#FormStudentInfo_DOB');	

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>");
		window.onbeforeunload = '';
		form1.submit();
//		setTimeout(function(){timeUp=true;},10000);
//		if(confirm("<?=$Lang['Admission']['msg']['annonceautosubit']?>")){
			//clearTimeout(timer);
//			if(timeUp){
//				alert('<?=$Lang['Admission']['msg']['timeup']?>');
//				window.onbeforeunload = '';
//				window.location.href = 'submit_time_out.php';
//			}
//			else{
//				window.onbeforeunload = '';
//				form1.submit();
//			}
//		}
//		else{
//			alert('<?=$Lang['Admission']['msg']['timeup']?>');
//			window.onbeforeunload = '';
//			window.location.href = 'submit_time_out.php';
//		}
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	//For debugging only
	//return true;
	
	// borower version checking if browser is IE9 or below or not
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	//for email validation
	var re = /\S+@\S+\.\S+/;

	//allow file size checking if the  
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
		// Student Info
	if(form1.ApplyClass.value==''){
		return false;
	} else if(form1.studentssurname_b5.value==''){
		return false;
	} else if(form1.studentsfirstname_b5.value==''){
		return false;
	} else if(form1.studentssurname_en.value==''){
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		return false;
	} else if(!form1.FormStudentInfo_DOB.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		return false;
	} else if(dOBRange[0] !='' && form1.FormStudentInfo_DOB.value < dOBRange[0] || dOBRange[1] !='' && form1.FormStudentInfo_DOB.value > dOBRange[1]){
		return false;
	} 
	else if($('input:radio[name=FormStudentInfo_StuGender]:checked').val() == null){
		return false;
	} else if(form1.FormStudentInfo_POB.value==''){
		return false;
	} else if(form1.FormStudentInfo_POB.value==<?=$admission_cfg['placeofbirth']['other']?> && form1.FormStudentInfo_POB_Other.value.trim() == '' ){
		return false;
	} else if(form1.FormStudentInfo_Religion.value==''){
		return false;
	} else if(form1.FormStudentInfo_Religion.value==<?=$admission_cfg['religion']['other']?> && form1.FormStudentInfo_Religion_Other.value.trim() == '' ){
		return false;
//	} else if( form1.FormStudentInfo_Province.value.trim() == '' ){
//		return false;
//	} else if( form1.FormStudentInfo_County.value.trim() == '' ){
//		return false;
	} else if( form1.FormStudentInfo_Church.value.trim() == '' ){
		return false;
	} else if(form1.FormStudentInfo_StuIDType.value==''){
		return false;
	} else if(form1.FormStudentInfo_StuIDType.value==3 && form1.FormStudentInfo_StuIDType_Other.value==''){
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		return false;
	}
	if(form1.FormStudentInfo_StuIDType.value!=3){
		if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
		return false;
		}
	}
	if(checkBirthCertNo() > 0){
		return false;
	} else if(form1.FormStudentInfo_HomeTel.value==''){
		return false;
	} else if(!/^[0-9]{8}$/.test(form1.FormStudentInfo_HomeTel.value)){
		return false;
	}  
	// Phone Num
	if($('#disableFormStudentInfo_ContactTel:checked').length ==0){
		if(form1.FormStudentInfo_ContactTel.value==''){
			return false;
		} else if(!/^[0-9]{8}$/.test(form1.FormStudentInfo_ContactTel.value)){
			return false;
		}
	} 
	
	// Address
	if(form1.FormStudentInfo_HomeAddrChi.value==''){
		return false;
	} else if(form1.HomeAddressDistrict.value==''){
		return false;
	} else if(form1.FormStudentInfo_HomeAddrEng.value==''){
		return false;
	} 
	if($('#disableFormStudentInfo_ContactAddr:checked').length ==0){
		
		if(form1.FormStudentInfo_ContactAddrChi.value==''){
			return false;
		}
		else if(form1.ContactAddressDistrict.value==''){
		return false;
		}
		else if(form1.FormStudentInfo_ContactAddrEng.value==''){
			return false;
		}
	} 
	
	//Current School Info
	if($('#disableCurrentSchool:checked').length ==0){
		if(form1.CurrentSchoolName.value==''){
			return false;
		} else if(form1.CurrentSchoolClassLv.value==''){
			return false;
		} else if(form1.CurrentSchoolDayType.value==''){
			return false;
		} else if(form1.CurrentSchoolAddr.value==''){
			return false;
		}
	}
	
	if(form1.G3Relationship.value==''){
		return false;
	}
	else if(form1.G3ChineseName.value==''){
		return false;
	}
	else if(form1.G3Gender.value==''){
		return false;
	}
	else if(form1.G3Occupation.value==''){
		return false;
	}
	else if(form1.G3WorkNo.value==''){
		return false;
	}
	else if(form1.G3MobileNo.value==''){
		return false;
	}
	else if(!/^[0-9]{8}$/.test(form1.G3MobileNo.value)){
		return false;
	}
	else if(form1.G3Email.value==''){
		return false;
	}
	else if(form1.G3Email.value!='' && !re.test(form1.G3Email.value)){
		return false;
	}
	
	var regxNum = /[0-9]/;
	var checkFamilyFail = 0;
	//for other information
	$('#StuFamilyTableCell').find('input, select, textarea').each(function(){
		if( $(this).val() == '' || !regxNum.test($(this).val()) ){
			checkFamilyFail = 1;
			return false;
		}	
	});
	if(checkFamilyFail == 1){
		return false;
	}
	if(form1.IsTwins.value == ''){
		return false;
	}
	return true;
}

function check_docs_upload2(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	//allow file size checking if the  
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	
	if(form1.StudentPersonalPhoto.value==''){
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		return false;
	}
	var temp_count = (file_count > 1?1:file_count);
	for(var i=0;i<temp_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(otherFileVal==''){
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			return false;
		}
	}
	if(file_count > 1){
	for(var i=1;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(otherFileVal!=''){
			if(!isOldBrowser){
				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
			}
			if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
				return false;
			} else if(!isOldBrowser && otherFileSize > maxFileSize){
				return false;
			}
		}
	}
	}
	return true;
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
//		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
//	        alert("表格需以 Google Chrome、Firefox 或 Internet Explorer 10 或以上瀏覽器填寫。");
//	        return;
//	    }
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		//alert($("form").serialize());
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
			var chk_ary = $('input[type=checkbox]');
			var chk_count = chk_ary.length;
			for(var i=0;i<chk_count;i++)
			{
//				var chk_element = chk_ary.get(i);
//				if(chk_element.checked == false){
//					alert("Please tick I have read and understand the statement. I am ready to proceed application online.\n請剔選 我已閱讀並理解以上資料，已準備繼續進行網上報名程序。");
//					isValid = false;
//				}
			}
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
			   clearTimeout(timer);
			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();	 
		   
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_class_selection.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#DayTypeOption").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
		   
	}
	
	if(page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_confirm").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){

	document.getElementById('form1').target = '';
	document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	return confirm('<?=$Lang['Admission']['munsang']['msg']['suresubmit']?>');
}

function check_choose_class(form1) {

	<?if($sys_custom['KIS_Admission']['ICMS']['Settings']){?>
		var chk_ary = $('input[type=checkbox]');
		var chk_count = chk_ary.length;
		for(var i=0;i<chk_count;i++)
		{
			var chk_element = chk_ary.get(i);
			if(chk_element.checked == false){
				alert("<?=$Lang['Admission']['icms']['msg']['acknowledgement']?>");
				return false;
			}
		}
	<?}?>
	
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>\nPlease select Class.");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	
	//For debugging only
	//return true;
	
	// borower version checking if browser is IE9 or below or not
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	//for email validation
	var re = /\S+@\S+\.\S+/;

	//allow file size checking if the  
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		var studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	// Student Info
	if(form1.ApplyClass.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectApplyClass']?>");	
		form1.ApplyClass.focus();
		return false;
	} else if(form1.studentssurname_b5.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentssurname_b5']?>");	
		form1.studentssurname_b5.focus();
		return false;
	} else if(form1.studentsfirstname_b5.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentsfirstname_b5']?>");
		form1.studentsfirstname_b5.focus();
		return false;
	} else if(form1.studentssurname_en.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentssurname_en']?>");	
		form1.studentssurname_en.focus();
		return false;
	} else if(form1.studentsfirstname_en.value==''){
		alert("<?=$Lang['Admission']['csm']['msg']['studentsfirstname_en']?>");
		form1.studentsfirstname_en.focus();
		return false;
	} else if(!form1.FormStudentInfo_DOB.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.FormStudentInfo_DOB.value!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>");	
		}
		
		form1.FormStudentInfo_DOB.focus();
		return false;
	} else if(dOBRange[0] !='' && form1.FormStudentInfo_DOB.value < dOBRange[0] || dOBRange[1] !='' && form1.FormStudentInfo_DOB.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>");
		form1.FormStudentInfo_DOB.focus();
		return false;
	} 
	else if($('input:radio[name=FormStudentInfo_StuGender]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>");	
		form1.FormStudentInfo_StuGender[0].focus();
		return false;
	} else if(!isInternalUse && form1.FormStudentInfo_POB.value==''){
		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>");	
		form1.FormStudentInfo_POB.focus();
		return false;
	} else if(!isInternalUse && form1.FormStudentInfo_POB.value==<?=$admission_cfg['placeofbirth']['other']?> && form1.FormStudentInfo_POB_Other.value.trim() == '' ){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectPOB']?>");	
		form1.FormStudentInfo_POB_Other.focus();
		return false;
	} else if(!isInternalUse && form1.FormStudentInfo_Religion.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectReligion']?>");	
		form1.FormStudentInfo_Religion.focus();
		return false;
	} else if(!isInternalUse && form1.FormStudentInfo_Religion.value==<?=$admission_cfg['religion']['other']?> && form1.FormStudentInfo_Religion_Other.value.trim() == '' ){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterReligion']?>");	
		form1.FormStudentInfo_Religion_Other.focus();
		return false;
//	} else if( form1.FormStudentInfo_Province.value.trim() == '' ){
//		alert("<?=$Lang['Admission']['msg']['enternativeplace']?>");	
//		form1.FormStudentInfo_Province.focus();
//		return false;
//	} else if( form1.FormStudentInfo_County.value.trim() == '' ){
//		alert("<?=$Lang['Admission']['msg']['enternativeplace']?>");	
//		form1.FormStudentInfo_County.focus();
//		return false;
	} else if( !isInternalUse && form1.FormStudentInfo_Church.value.trim() == '' ){
		alert("<?=$Lang['Admission']['msg']['enterchurch'] ?>");	
		form1.FormStudentInfo_Church.focus();
		return false;
	} else if(form1.FormStudentInfo_StuIDType.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectIDType']?>");	
		form1.FormStudentInfo_StuIDType.focus();
		return false;
	} else if(form1.FormStudentInfo_StuIDType.value==3 && form1.FormStudentInfo_StuIDType_Other.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterIDType']?>");	
		form1.FormStudentInfo_StuIDType_Other.focus();
		return false;
	} else if(form1.StudentBirthCertNo.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
	}
	if(form1.FormStudentInfo_StuIDType.value!=3){
		if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['InvalidId']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
		}
	}
	if(checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['munsang']['msg']['duplicatebirthcertificatenumber']?>");	
		form1.StudentBirthCertNo.focus();
		return false;
	} else if(!isInternalUse && form1.FormStudentInfo_HomeTel.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enterphone']?>");	
		form1.FormStudentInfo_HomeTel.focus();
		return false;
	} else if(!isInternalUse && !/^[0-9]{8}$/.test(form1.FormStudentInfo_HomeTel.value)){
		alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>");
		form1.FormStudentInfo_HomeTel.focus();
		return false;
	}  
	// Phone Num
	if($('#disableFormStudentInfo_ContactTel:checked').length ==0){
		if(!isInternalUse && form1.FormStudentInfo_ContactTel.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enterphone']?>");	
			form1.FormStudentInfo_ContactTel.focus();
			return false;
		} else if(!isInternalUse && !/^[0-9]{8}$/.test(form1.FormStudentInfo_ContactTel.value)){
			alert("<?=$Lang['Admission']['munsang']['msg']['enternumber']?>");
			form1.FormStudentInfo_ContactTel.focus();
			return false;
		}
	} 
	
	// Address
	if(!isInternalUse && form1.FormStudentInfo_HomeAddrChi.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>");	
		form1.FormStudentInfo_HomeAddrChi.focus();
		return false;
	} else if(!isInternalUse && form1.HomeAddressDistrict.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectDistrict']?>");
		form1.HomeAddressDistrict.focus();
		return false;
	} else if(!isInternalUse && form1.FormStudentInfo_HomeAddrEng.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>");	
		form1.FormStudentInfo_HomeAddrEng.focus();
		return false;
	} 
	if($('#disableFormStudentInfo_ContactAddr:checked').length ==0){
		
		if(!isInternalUse && form1.FormStudentInfo_ContactAddrChi.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>");	
			form1.FormStudentInfo_ContactAddrChi.focus();
			return false;
		}
		else if(!isInternalUse && form1.ContactAddressDistrict.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectDistrict']?>");
		form1.ContactAddressDistrict.focus();
		return false;
		}
		else if(!isInternalUse && form1.FormStudentInfo_ContactAddrEng.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>");	
			form1.FormStudentInfo_ContactAddrEng.focus();
			return false;
		}
	} 
	
	//Current School Info
	if($('#disableCurrentSchool:checked').length ==0){
		if(!isInternalUse && form1.CurrentSchoolName.value==''){
			alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterCurrentSchoolInfo']?>");	
			form1.CurrentSchoolName.focus();
			return false;
		} else if(!isInternalUse && form1.CurrentSchoolClassLv.value==''){
			alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterCurrentSchoolInfo']?>");	
			form1.CurrentSchoolClassLv.focus();
			return false;
		} else if(!isInternalUse && form1.CurrentSchoolDayType.value==''){
			alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterCurrentSchoolInfo']?>");	
			form1.CurrentSchoolDayType.focus();
			return false;
		} else if(!isInternalUse && form1.CurrentSchoolAddr.value==''){
			alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterCurrentSchoolInfo']?>");	
			form1.CurrentSchoolAddr.focus();
			return false;
		}
	}
	
	if(!isInternalUse && form1.G3Relationship.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterGRelationsip']?>");
		form1.G3Relationship.focus();
		return false;
	}
	else if(!isInternalUse && form1.G3ChineseName.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterGName']?>");
		$('#G3ChineseName').focus();
		return false;
	}
	else if(!isInternalUse && form1.G3Gender.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectGGender']?>");
		$('#G3Gender1').focus();
		return false;
	}
	else if(!isInternalUse && form1.G3Occupation.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterGuardianInfo']?>");
		form1.G3Occupation.focus();
		return false;
	}
	else if(!isInternalUse && form1.G3WorkNo.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterGuardianWorkNo'] ?>");
		form1.G3WorkNo.focus();
		return false;
	}
	else if(!isInternalUse && form1.G3MobileNo.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['G3mobile'] ?>");
		form1.G3MobileNo.focus();
		return false;
	}
	else if(form1.G3MobileNo.value!='' && !/^[0-9]{8}$/.test(form1.G3MobileNo.value)){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['G3mobileformat']?>");
		form1.G3MobileNo.focus();
		return false;
	}
	else if(!isInternalUse && form1.G3Email.value==''){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['G3Email']?>");
		form1.G3Email.focus();
		return false;
	}
	else if(form1.G3Email.value!='' && !re.test(form1.G3Email.value)){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['InvalidG3Email']?>");
		form1.G3Email.focus();
		return false;
	}
	
	var regxNum = /[0-9]/;
	var checkFamilyFail = 0;
	//for other information
	$('#StuFamilyTableCell').find('input, select, textarea').each(function(){
		if(!isInternalUse && ($(this).val() == '' || !regxNum.test($(this).val()))){
			alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['EnterFamily']?>");
			$(this).focus();
			checkFamilyFail = 1;
			return false;
		}	
	});
	if(checkFamilyFail == 1){
		return false;
	}
	if(!isInternalUse && form1.IsTwins.value == ''){
		alert('<?=$Lang['Admission']['CHIUCHUNKG']['msg']['isTwins']?>');
		$('#IsTwins1').focus();
		return false;
	}
	
	
//	if($('input:radio[name=OthersIsConsiderAlternative]:checked').val() == null){
//		alert("<?=$Lang['Admission']['munsang']['msg']['IsConsiderAlternative']?>");	
//		form1.OthersIsConsiderAlternative[0].focus();
//		return false;
//	}
//	else if(form1.OthersPrevSchYear1.value==''){
//		alert("<?=$Lang['Admission']['munsang']['msg']['enterYear']?>");
//		form1.OthersPrevSchYear1.focus();
//		return false;
//	}
//	else if(form1.OthersPrevSchClass1.value==''){
//		alert("<?=$Lang['Admission']['munsang']['msg']['enterClass']?>");
//		form1.OthersPrevSchClass1.focus();
//		return false;
//	}
//	else if(form1.OthersPrevSchName1.value==''){
//		alert("<?=$Lang['Admission']['munsang']['msg']['enterNameOfSchool']?>");
//		form1.OthersPrevSchName1.focus();
//		return false;
//	}
	return true;	
}

function check_docs_upload(form1) {
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;
	
	//allow file size checking if the  
	var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
	if(!isOldBrowser){
		if(form1.StudentPersonalPhoto.files[0]){
		studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
		var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
		}
	}
	if(!isInternalUse && form1.StudentPersonalPhoto.value!=''){
	if(form1.StudentPersonalPhoto.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
		alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	} else if(!isOldBrowser && studentPhotoSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
		form1.StudentPersonalPhoto.focus();
		return false;
	}
	}
	var temp_count = (file_count > 1?1:file_count);
	for(var i=0;i<temp_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(!isInternalUse || otherFileVal!=''){
		if(otherFileVal==''){
			alert("<?=$Lang['Admission']['msg']['uploadfile']?>");
			file_element.focus();
			return false;
		} else if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>");
			file_element.focus();
			return false;
		} else if(!isOldBrowser && otherFileSize > maxFileSize){
			alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>");	
			file_element.focus();
			return false;
		}
		}
	}
	if(file_count > 1){
	for(var i=1;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		var otherFileVal = file_element.value;
		if(otherFileVal!=''){
			var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
			if(!isOldBrowser){
				otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
				var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
			}
			if(otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
				alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
				file_element.focus();
				return false;
			} else if(!isOldBrowser && otherFileSize > maxFileSize){
				alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
				file_element.focus();
				return false;
			}
		}
	}
	}

	return true;
}

function checkBirthCertNo(){
	
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount > 3){
			document.getElementById('btn_addRow1').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow1').style.display = '';
		}
		 
	}
	else{
		if(rowCount > 3){
			document.getElementById('btn_addRow2').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow2').style.display = '';
		}
	}
	
	if(rowCount <= 4){                            // limit the user from creating fields more than your limits
		var row = table.insertRow(rowCount);
		var newcell = row.insertCell(0);
		newcell.className = "field_title";
		newcell.style.textAlign ="right";
		newcell.innerHTML = '('+rowCount+')';
		if(tableID == 'dataTable1'){
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchYear'+rowCount+'" type="text" id="OthersPrevSchYear'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchClass'+rowCount+'" type="text" id="OthersPrevSchClass'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchName'+rowCount+'" type="text" id="OthersPrevSchName'+rowCount+'" class="textboxtext" />';
		}
		else{
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedName'+rowCount+'" type="text" id="OthersRelativeStudiedName'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeRelationship'+rowCount+'" type="text" id="OthersRelativeRelationship'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedYear'+rowCount+'" type="text" id="OthersRelativeStudiedYear'+rowCount+'" class="textboxtext" />';
		}
	}else{
		 //alert("Maximum Passenger per ticket is 5");
			   
	}
}

function disableNotApplicable(id){
	
	var disableCheckBoxId = 'disable'+id;
	
	switch(id){
		
		case 'FormStudentInfo_ContactAddr':
			if($('input#'+disableCheckBoxId).attr('checked')){
				$('select#ContactAddressDistrict').attr('disabled',true);
				$('input#FormStudentInfo_ContactAddrChi').attr('disabled',true);
				$('input#FormStudentInfo_ContactAddrEng').attr('disabled',true);
			}
			else{
				$('select#ContactAddressDistrict').removeAttr('disabled');
				$('input#FormStudentInfo_ContactAddrChi').removeAttr('disabled');
				$('input#FormStudentInfo_ContactAddrEng').removeAttr('disabled');
			}
		break;
		case 'CurrentSchool':
			if($('input#'+disableCheckBoxId).attr('checked')){
				$('table#CurrentSchoolTable').hide();
				$('table#CurrentSchoolTable').find('input, select, textarea').each(function(){
					
					$(this).attr('disabled',true);
				});
			}
			else{
				$('table#CurrentSchoolTable').show();
				$('table#CurrentSchoolTable').find('input, select, textarea').each(function(){
					$(this).removeAttr('disabled');
				});
			}
		break;
		default:
		if($('input#'+disableCheckBoxId).attr('checked')){
			$('input#'+id).attr('disabled',true);
		}
		else{
			$('input#'+id).removeAttr('disabled');
		}
	}
	
}

function showOtherTextField(id,value){

	switch(id){
		case 'FormStudentInfo_POB':
			if(value == 4){
				$('span#POB_Other').show();	
				$('input#FormStudentInfo_POB_Other').removeAttr('disabled');
			}
			else{
				$('span#POB_Other').hide();
				$('input#FormStudentInfo_POB_Other').attr('disabled',true);
			}
		break;
		case 'FormStudentInfo_Religion':
			if(value == 3){
				$('span#Religion_Other').show();	
				$('input#FormStudentInfo_Religion_Other').removeAttr('disabled');
			}
			else{
				$('span#Religion_Other').hide();
				$('input#FormStudentInfo_Religion_Other').attr('disabled',true);
			}
		break;
		case 'FormStudentInfo_StuIDType':
		if(value == 3){
				$('span#StuID_Other').show();	
				$('input#FormStudentInfo_StuIDType_Other').removeAttr('disabled');
			}
			else{
				$('span#StuID_Other').hide();
				$('input#FormStudentInfo_StuIDType_Other').attr('disabled',true);
			}
		break;
	}
    
}

function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount < 4){
			document.getElementById('btn_deleteRow1').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow1').style.display = '';
		}
	}
	else{
		if(rowCount < 4){
			document.getElementById('btn_deleteRow2').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow2').style.display = '';
		}
	}
	
	if(rowCount <= 2) {               // limit the user from removing all the fields
		//alert("Cannot Remove all the Passenger.");				
	}
	else{
		table.deleteRow(rowCount - 1);	
	}
}


</script>