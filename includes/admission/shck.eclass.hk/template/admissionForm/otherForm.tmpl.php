<h1>Other Information <?=$Lang['Admission']['otherInfo']?></h1>
<table class="form_table otherInformation" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:6%">
        <col style="width:16%">
        <col style="width:16%">
        <col style="width:16%">
        <col style="width:16%">
    </colgroup>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title" rowspan="5">
    		Is the applicant’s brother or sister studying in this school?
    		<br />
    		<?=$Lang['Admission']['SHCK']['SibilingsStudyingInThisSchool']?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['SibilingsStudyingInThisSchool'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
    			<input type="radio" value="1" id="SibilingsStudyingInThisSchoolY" name="SibilingsStudyingInThisSchool" <?=($allCustInfo['SibilingsStudyingInThisSchool'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="SibilingsStudyingInThisSchoolY"> Yes <?=$Lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="SibilingsStudyingInThisSchoolN" name="SibilingsStudyingInThisSchool" <?=($allCustInfo['SibilingsStudyingInThisSchool'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="SibilingsStudyingInThisSchoolN"> No <?=$Lang['Admission']['no']?></label>
    		<?php } ?>
    	</td>
    </tr>
    
    
    <?php if(!$IsConfirm || $formData['SibilingsStudyingInThisSchool']){ ?>
        <tr>
        	<td>&nbsp;</td>
        	<td class="form_guardian_head"><center>Name <?=$Lang['Admission']['name'] ?></center></td>
        	<td class="form_guardian_head"><center>Relationship <?=$Lang['Admission']['SHCK']['Relationship'] ?></center></td>
        	<td class="form_guardian_head" colspan="2"><center>Class <?=$Lang['Admission']['class'] ?></center></td>
        </tr>
        
        <?php for($i=0;$i<3;$i++){ ?>
            <tr>
            	<td class="form_guardian_field" style="text-align: right">
            		<?=$i+1 ?>)&nbsp;
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInThisSchool_Name'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInThisSchool_Name[]" type="text" id="SibilingsStudyingInThisSchool_Name<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInThisSchool_Name'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInThisSchool_Relationship'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInThisSchool_Relationship[]" type="text" id="SibilingsStudyingInThisSchool_Relationship<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInThisSchool_Relationship'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field" colspan="2">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInThisSchool_Class'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInThisSchool_Class[]" type="text" id="SibilingsStudyingInThisSchool_Class<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInThisSchool_Class'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            </tr>
        <?php } ?>
    <?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title" rowspan="5">
    		Is any parent or sibling a former student of this school?
    		<br />
    		<?=$Lang['Admission']['SHCK']['FormerStudentInThisSchool']?>
    	</td>
    	<td colspan="4">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['FormerStudentInThisSchool'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
    			<input type="radio" value="1" id="FormerStudentInThisSchoolY" name="FormerStudentInThisSchool" <?=($allCustInfo['FormerStudentInThisSchool'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="FormerStudentInThisSchoolY"> Yes <?=$Lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="FormerStudentInThisSchoolN" name="FormerStudentInThisSchool" <?=($allCustInfo['FormerStudentInThisSchool'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="FormerStudentInThisSchoolN"> No <?=$Lang['Admission']['no']?></label>
    		<?php } ?>
    	</td>
    </tr>
    
    <?php if(!$IsConfirm || $formData['FormerStudentInThisSchool']){ ?>
        <tr>
        	<td>&nbsp;</td>
        	<td class="form_guardian_head"><center>Name <?=$Lang['Admission']['name'] ?></center></td>
        	<td class="form_guardian_head"><center>Relationship <?=$Lang['Admission']['SHCK']['Relationship'] ?></center></td>
        	<td class="form_guardian_head" colspan="2"><center>Year of Graduation <?=$Lang['Admission']['SHCK']['YearOfGraduation'] ?></center></td>
        </tr>
        
        <?php for($i=0;$i<3;$i++){ ?>
            <tr>
            	<td class="form_guardian_field" style="text-align: right">
            		<?=$i+1 ?>)&nbsp;
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInThisSchool_Name'][$i];
            		}else{?>
            			<input name="FormerStudentInThisSchool_Name[]" type="text" id="FormerStudentInThisSchool_Name<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInThisSchool_Name'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInThisSchool_Relationship'][$i];
            		}else{?>
            			<input name="FormerStudentInThisSchool_Relationship[]" type="text" id="FormerStudentInThisSchool_Relationship<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInThisSchool_Relationship'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field" colspan="2">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInThisSchool_YearOfGraduation'][$i];
            		}else{?>
            			<input name="FormerStudentInThisSchool_YearOfGraduation[]" type="text" id="FormerStudentInThisSchool_YearOfGraduation<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInThisSchool_YearOfGraduation'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            </tr>
        <?php } ?>
    <?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title" rowspan="5">
    		Is the applicant’s brother or sister studying in any of our Canossian schools?
    		<br />
    		<?=$Lang['Admission']['SHCK']['SibilingsStudyingInOtherCanossianSchool']?>
    	</td>
    	<td colspan="4">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['SibilingsStudyingInOtherCanossianSchool'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
    			<input type="radio" value="1" id="SibilingsStudyingInOtherCanossianSchoolY" name="SibilingsStudyingInOtherCanossianSchool" <?=($allCustInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="SibilingsStudyingInOtherCanossianSchoolY"> Yes <?=$Lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="SibilingsStudyingInOtherCanossianSchoolN" name="SibilingsStudyingInOtherCanossianSchool" <?=($allCustInfo['SibilingsStudyingInOtherCanossianSchool'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="SibilingsStudyingInOtherCanossianSchoolN"> No <?=$Lang['Admission']['no']?></label>
    		<?php } ?>
    	</td>
    </tr>
    
    <?php if(!$IsConfirm || $formData['SibilingsStudyingInOtherCanossianSchool']){ ?>
        <tr>
        	<td>&nbsp;</td>
        	<td class="form_guardian_head"><center>Name <?=$Lang['Admission']['name'] ?></center></td>
        	<td class="form_guardian_head"><center>Relationship <?=$Lang['Admission']['SHCK']['Relationship'] ?></center></td>
        	<td class="form_guardian_head"><center>Class <?=$Lang['Admission']['class'] ?></center></td>
        	<td class="form_guardian_head"><center>Name of School <?=$Lang['Admission']['SHCK']['NameOfSchool'] ?></center></td>
        </tr>
        
        <?php for($i=0;$i<3;$i++){ ?>
            <tr>
            	<td class="form_guardian_field" style="text-align: right">
            		<?=$i+1 ?>)&nbsp;
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInOtherCanossianSchool_Name'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInOtherCanossianSchool_Name[]" type="text" id="SibilingsStudyingInOtherCanossianSchool_Name<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInOtherCanossianSchool_Name'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInOtherCanossianSchool_Relationship'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInOtherCanossianSchool_Relationship[]" type="text" id="SibilingsStudyingInOtherCanossianSchool_Relationship<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInOtherCanossianSchool_Relationship'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInOtherCanossianSchool_Class'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInOtherCanossianSchool_Class[]" type="text" id="SibilingsStudyingInOtherCanossianSchool_Class<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInOtherCanossianSchool_Class'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['SibilingsStudyingInOtherCanossianSchool_NameOfSchool'][$i];
            		}else{?>
            			<input name="SibilingsStudyingInOtherCanossianSchool_NameOfSchool[]" type="text" id="SibilingsStudyingInOtherCanossianSchool_NameOfSchool<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['SibilingsStudyingInOtherCanossianSchool_NameOfSchool'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            </tr>
        <?php } ?>
    <?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title" rowspan="5">
    		Is any parent or sibling a former student of any of our Canossian schools?
    		<br />
    		<?=$Lang['Admission']['SHCK']['FormerStudentInOtherCanossianSchool']?>
    	</td>
    	<td colspan="4">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['FormerStudentInOtherCanossianSchool'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
    			<input type="radio" value="1" id="FormerStudentInOtherCanossianSchoolY" name="FormerStudentInOtherCanossianSchool" <?=($allCustInfo['FormerStudentInOtherCanossianSchool'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="FormerStudentInOtherCanossianSchoolY"> Yes <?=$Lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="FormerStudentInOtherCanossianSchoolN" name="FormerStudentInOtherCanossianSchool" <?=($allCustInfo['FormerStudentInOtherCanossianSchool'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="FormerStudentInOtherCanossianSchoolN"> No <?=$Lang['Admission']['no']?></label>
    		<?php } ?>
    	</td>
    </tr>
    
    <?php if(!$IsConfirm || $formData['FormerStudentInOtherCanossianSchool']){ ?>
        <tr>
        	<td>&nbsp;</td>
        	<td class="form_guardian_head"><center>Name <?=$Lang['Admission']['name'] ?></center></td>
        	<td class="form_guardian_head"><center>Relationship <?=$Lang['Admission']['SHCK']['Relationship'] ?></center></td>
        	<td class="form_guardian_head"><center>Year of Graduation <?=$Lang['Admission']['SHCK']['YearOfGraduation'] ?></center></td>
        	<td class="form_guardian_head"><center>Name of School <?=$Lang['Admission']['SHCK']['NameOfSchool'] ?></center></td>
        </tr>
        
        <?php for($i=0;$i<3;$i++){ ?>
            <tr>
            	<td class="form_guardian_field" style="text-align: right">
            		<?=$i+1 ?>)&nbsp;
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInOtherCanossianSchool_Name'][$i];
            		}else{?>
            			<input name="FormerStudentInOtherCanossianSchool_Name[]" type="text" id="FormerStudentInOtherCanossianSchool_Name<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInOtherCanossianSchool_Name'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInOtherCanossianSchool_Relationship'][$i];
            		}else{?>
            			<input name="FormerStudentInOtherCanossianSchool_Relationship[]" type="text" id="FormerStudentInOtherCanossianSchool_Relationship<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInOtherCanossianSchool_Relationship'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInOtherCanossianSchool_YearOfGraduation'][$i];
            		}else{?>
            			<input name="FormerStudentInOtherCanossianSchool_YearOfGraduation[]" type="text" id="FormerStudentInOtherCanossianSchool_YearOfGraduation<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInOtherCanossianSchool_YearOfGraduation'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['FormerStudentInOtherCanossianSchool_NameOfSchool'][$i];
            		}else{?>
            			<input name="FormerStudentInOtherCanossianSchool_NameOfSchool[]" type="text" id="FormerStudentInOtherCanossianSchool_NameOfSchool<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['FormerStudentInOtherCanossianSchool_NameOfSchool'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            </tr>
        <?php } ?>
    <?php } ?>
</tbody>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title" rowspan="5">
    		Is any immediate family member an employee of any of our Canossian institutions?
    		<br />
    		<?=$Lang['Admission']['SHCK']['EmployeeOfCanossianInstitutions']?>
    	</td>
    	<td colspan="4">
    		<?php if($IsConfirm){ ?>
    			<?=($formData['EmployeeOfCanossianInstitutions'])? 'Yes '. $Lang['Admission']['yes']:'No ' . $Lang['Admission']['no']?>
    		<?php }else{ ?>
    			<input type="radio" value="1" id="EmployeeOfCanossianInstitutionsY" name="EmployeeOfCanossianInstitutions" <?=($allCustInfo['EmployeeOfCanossianInstitutions'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="EmployeeOfCanossianInstitutionsY"> Yes <?=$Lang['Admission']['yes']?></label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="EmployeeOfCanossianInstitutionsN" name="EmployeeOfCanossianInstitutions" <?=($allCustInfo['EmployeeOfCanossianInstitutions'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="EmployeeOfCanossianInstitutionsN"> No <?=$Lang['Admission']['no']?></label>
    		<?php } ?>
    	</td>
    </tr>
    
    <?php if(!$IsConfirm || $formData['EmployeeOfCanossianInstitutions']){ ?>
        <tr>
        	<td>&nbsp;</td>
        	<td class="form_guardian_head"><center>Name <?=$Lang['Admission']['name'] ?></center></td>
        	<td class="form_guardian_head"><center>Relationship <?=$Lang['Admission']['SHCK']['Relationship'] ?></center></td>
        	<td class="form_guardian_head" colspan="2"><center>Name of Our Institution <?=$Lang['Admission']['SHCK']['NameOfOurInstitution'] ?></center></td>
        </tr>
        
        <?php for($i=0;$i<3;$i++){ ?>
            <tr>
            	<td class="form_guardian_field" style="text-align: right">
            		<?=$i+1 ?>)&nbsp;
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['EmployeeOfCanossianInstitutions_Name'][$i];
            		}else{?>
            			<input name="EmployeeOfCanossianInstitutions_Name[]" type="text" id="EmployeeOfCanossianInstitutions_Name<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['EmployeeOfCanossianInstitutions_Name'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field">
            		<?php if($IsConfirm){
            		    echo $formData['EmployeeOfCanossianInstitutions_Relationship'][$i];
            		}else{?>
            			<input name="EmployeeOfCanossianInstitutions_Relationship[]" type="text" id="EmployeeOfCanossianInstitutions_Relationship<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['EmployeeOfCanossianInstitutions_Relationship'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            	
            	<td class="form_guardian_field" colspan="2">
            		<?php if($IsConfirm){
            		    echo $formData['EmployeeOfCanossianInstitutions_NameOfOurInstitution'][$i];
            		}else{?>
            			<input name="EmployeeOfCanossianInstitutions_NameOfOurInstitution[]" type="text" id="EmployeeOfCanossianInstitutions_NameOfOurInstitution<?=$i ?>" class="textboxtext" value="<?=$allCustInfo['EmployeeOfCanossianInstitutions_NameOfOurInstitution'][$i]['Value'] ?>"/>
            		<?php } ?>
            	</td>
            </tr>
        <?php } ?>
    <?php } ?>
</tbody>

</table>

<script>
$(function(){
	$('.otherInformation .otherQuestionRow input[type="radio"]').click(function(){
		var $tbody = $(this).closest('tbody');
		if($(this).val() == '0'){
			$tbody.find('tr:not(.otherQuestionRow)').hide().find('input').prop('disabled', true);
		}else{
			$tbody.find('tr:not(.otherQuestionRow)').show().find('input').prop('disabled', false);
		}
	});
	$('.otherInformation .otherQuestionRow input[type="radio"]:checked').click();
});
</script>