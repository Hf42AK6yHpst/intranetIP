<?php


######## Interview Announcement START ########
$viewInterviewArr = array(
    1 => ($basicSettings['firstInterviewAnnouncement'] > 0),
    2 => ($basicSettings['secondInterviewAnnouncement'] > 0),
    3 => ($basicSettings['thirdInterviewAnnouncement'] > 0),
);
$hasInterviewArr = array(
    1 => !!($result['Date']),
    2 => !!($result2['Date']),
    3 => !!($result3['Date']),
);

$templateIdArr = array();
for($i=1;$i<=3;$i++){
    foreach($emailTemplates as $emailTemplate){
        $templateClassLevels = explode(',', $emailTemplate['TemplateClassLevel']);
        if(!in_array($classLevelId, $templateClassLevels)){
            continue;
        }

        if($hasInterviewArr[$i]){
            if(
                !isset($templateIdArr[$i]['b5']) && 
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview"]['b5']
            ){
                $templateIdArr[$i]['b5'] = $emailTemplate['TemplateID'];
            }elseif(
                !isset($templateIdArr[$i]['en']) && 
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview"]['en']
            ){
                $templateIdArr[$i]['en'] = $emailTemplate['TemplateID'];
            }
        }else{
            if(
                !isset($templateIdArr[$i]['b5']) && 
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview_CannotArrange"]['b5']
            ){
                $templateIdArr[$i]['b5'] = $emailTemplate['TemplateID'];
            }elseif(
                !isset($templateIdArr[$i]['en']) && 
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview_CannotArrange"]['en']
            ){
                $templateIdArr[$i]['en'] = $emailTemplate['TemplateID'];
            }
        }
    }
}
######## Interview Announcement END ########


######## Application Status START ########
$viewApplicationStatus = ($basicSettings['applicationStatusAnnouncement'] > 0);
$applicationAdmitted = ($applicationStatus == $admission_cfg['Status']['hkugaps_admitted']);
$applicationNotAdmitted = ($applicationStatus == $admission_cfg['Status']['hkugaps_notadmitted']);
$applicationGoToSecondInterview = ($applicationStatus == $admission_cfg['Status']['gotosecondinterview']);
$applicationGoToThirdInterview = ($applicationStatus == $admission_cfg['Status']['gotothirdinterview']);

$zhApplicationStatusTemplateId = 0;
$enApplicationStatusTemplateId = 0;
foreach($emailTemplates as $emailTemplate){
    $templateClassLevels = explode(',', $emailTemplate['TemplateClassLevel']);
    if(!in_array($classLevelId, $templateClassLevels)){
        continue;
    }
    
    if($applicationAdmitted){
        if(
            !$zhApplicationStatusTemplateId && 
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['Admitted']['b5']
        ){
            $zhApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        }elseif(
            !$enApplicationStatusTemplateId && 
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['Admitted']['en']
        ){
            $enApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        }
    }else{
        if(
            !$zhApplicationStatusTemplateId && 
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['NotAdmitted']['b5']
        ){
            $zhApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        }elseif(
            !$enApplicationStatusTemplateId && 
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['NotAdmitted']['en']
        ){
            $enApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        }
    }
}

if($isFirstYear){
	$firstRoundStrB5 = "面試";
	$firstRoundStrEn = "Interview";
	$firstRoundStrEnLowercase = "interview";
	$secondRoundStrB5 = "面試";
	$secondRoundStrEn = "Interview";
	$secondRoundStrEnLowercase = "interview";
}
else{
	$firstRoundStrB5 = "筆試";
	$firstRoundStrEn = "Admission Test (Written)";
	$firstRoundStrEnLowercase = "admission test (written)";
	$secondRoundStrB5 = "面試及口試";
	$secondRoundStrEn = "Interview And Oral Test";
	$secondRoundStrEnLowercase = "interview and oral test";
}
######## Application Status END ########
?>
<?php if($viewInterviewArr[1]): ?>
	<tr>
		<td class="field_title">第一階段<?=$firstRoundStrB5?> First Round <?=$firstRoundStrEn?></td>
		
		<?php if($isFirstYear):?>
    		<td>
                <div style="font-size:16px; margin-bottom:10px;">
                	<img src="/images/kis/icon_reminder.png" style="float:left">
                	<div style="overflow:hidden; padding:2px 0 0 10px;">
                		家長務必<b><u>列印或下載</u></b>此<?=$firstRoundStrB5?>通知信以進行<?=$firstRoundStrB5?>登記
                	</div>
                </div>
                <div style="margin:0 0 30px 30px;">
                	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[1]['b5'], '') ?>" target="_blank">
                		→ 按此查閱第一階段<?=$firstRoundStrB5?>通知信 (中文版本)
                	</a>
                </div>
                <div style="font-size:16px; margin-bottom:10px;">
                    <img src="/images/kis/icon_reminder.png" style="float:left">
                    <div style="overflow:hidden; padding:2px 0 0 10px;">
                    	Parents MUST <b><u>print or download</u></b> the PDF file of this <?=$firstRoundStrEnLowercase?> notification letter for registration
                    </div>
                </div>
                <div style="margin:0 0 30px 30px;">
                	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[1]['en'], '') ?>" target="_blank">
                		→ Click to view 1st Round <?=$firstRoundStrEn?> Notification Letter (English Version)
                	</a>
                </div>
            </td>
        <?php else: ?>
    		<td>
                <div style="font-size:16px; margin-bottom:10px;">
                	<img src="/images/kis/icon_reminder.png" style="float:left">
                	<div style="overflow:hidden; padding:2px 0 0 10px;">
                		請按以下連結以查看申請插班生之遴選結果
                	</div>
                </div>
                <div style="margin:0 0 30px 30px;">
                	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[1]['b5'], '') ?>" target="_blank">
                		→ 按此查閱遴選結果 (中文版本)
                	</a>
                </div>
            </td>
        <?php endif;?>
	</tr>
<?php endif; ?>

<?php if($viewInterviewArr[2] && $hasInterviewArr[1]): ?>
	<tr>
		<td class="field_title">第二階段<?=$secondRoundStrB5?> Second Round <?=$secondRoundStrEn?></td>
		
		
		<?php if($isFirstYear):?>
    		<td>
                <div style="font-size:16px; margin-bottom:10px;">
                	<img src="/images/kis/icon_reminder.png" style="float:left">
                	<div style="overflow:hidden; padding:2px 0 0 10px;">
                		家長務必<b><u>列印或下載</u></b>此面試通知信以進行面試登記
                	</div>
                </div>
                <div style="margin:0 0 30px 30px;">
                	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[2]['b5'], '') ?>" target="_blank">
                		→ 按此查閱第二階段面試通知信 (中文版本)
                	</a>
                </div>
                <div style="font-size:16px; margin-bottom:10px;">
                    <img src="/images/kis/icon_reminder.png" style="float:left">
                    <div style="overflow:hidden; padding:2px 0 0 10px;">
                    	Parents MUST <b><u>print or download</u></b> the PDF file of this interview notification letter for registration
                    </div>
                </div>
                <div style="margin:0 0 30px 30px;">
                	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[2]['en'], '') ?>" target="_blank">
                		→ Click to view 2nd Round Interview Notification Letter (English Version)
                	</a>
                </div>
            </td>
        <?php else:?>
    		<td>
                <div style="font-size:16px; margin-bottom:10px;">
                	<img src="/images/kis/icon_reminder.png" style="float:left">
                	<div style="overflow:hidden; padding:2px 0 0 10px;">
                		家長務必<b><u>列印或下載</u></b>此<?=$secondRoundStrB5?>通知信以進行<?=$secondRoundStrB5?>登記
                	</div>
                </div>
                <div style="margin:0 0 30px 30px;">
                	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[2]['b5'], '') ?>" target="_blank">
                		→ 按此查閱第二階段<?=$secondRoundStrB5?>通知信 (中文版本)
                	</a>
                </div>
            </td>
        <?php endif;?>
	</tr>
<?php endif; ?>

<?php if($viewInterviewArr[3] && $hasInterviewArr[2]): ?>
	<tr>
		<td class="field_title">第三階段面試 Third Round Interview</td>
		
		<td>
            <div style="font-size:16px; margin-bottom:10px;">
            	<img src="/images/kis/icon_reminder.png" style="float:left">
            	<div style="overflow:hidden; padding:2px 0 0 10px;">
            		家長務必<b><u>列印或下載</u></b>此面試通知信以進行面試登記
            	</div>
            </div>
            <div style="margin:0 0 30px 30px;">
            	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[3]['b5'], '') ?>" target="_blank">
            		→ 按此查閱第三階段面試通知信 (中文版本)
            	</a>
            </div>
            <div style="font-size:16px; margin-bottom:10px;">
                <img src="/images/kis/icon_reminder.png" style="float:left">
                <div style="overflow:hidden; padding:2px 0 0 10px;">
                	Parents MUST <b><u>print or download</u></b> the PDF file of this interview notification letter for registration
                </div>
            </div>
            <div style="margin:0 0 30px 30px;">
            	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[3]['en'], '') ?>" target="_blank">
            		→ Click to view 3rd Round Interview Notification Letter (English Version)
            	</a>
            </div>
        </td>
	</tr>
<?php endif; ?>

<?php if($viewApplicationStatus && ($applicationAdmitted || ($isFirstYear && $applicationGoToThirdInterview) || (!$isFirstYear && $applicationGoToSecondInterview) )): ?>
	<tr>
		<td class="field_title">面試結果 Interview Result</td>
		
		<td>
            <div style="margin:0 0 30px 30px;">
            	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $zhApplicationStatusTemplateId, '') ?>" target="_blank">
            		→ 按此查閱面試結果 (中文版本)
            	</a>
            </div>
            <div style="margin:0 0 30px 30px;">
            	<a href="<?=$libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $enApplicationStatusTemplateId, '') ?>" target="_blank">
            		→ Click to view interview result (English Version)
            	</a>
            </div>
        </td>
	</tr>
<?php endif; ?>