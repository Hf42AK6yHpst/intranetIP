<h1><?=$Lang['Admission']['PGInfo']?> Parent Information</h1>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:35%">
        <col style="width:35%">
    </colgroup>
    <tr>
    	<td>
    		<span>如沒有資料，例如單親家庭，請輸入 '<font style="color:red;">沒有</font>'</span>
    		<br/>
    		<span>If no information, e.g. single parent family, fill in '<font style="color:red;">Nil</font>'</span>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['father'] ?> Father</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['mother'] ?> Mother</center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star.$Lang['Admission']['HKUGAPS']['nameChi'] ?> Name (Chinese)
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1ChineseName'];
    		}else{?>
    			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$parentInfoArr['F']['chinesename'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2ChineseName'];
    		}else{?>
    			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$parentInfoArr['M']['chinesename'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star.$Lang['Admission']['HKUGAPS']['nameEng'] ?> Name (English)
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1EnglishName'];
    		}else{?>
    			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$parentInfoArr['F']['englishname'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2EnglishName'];
    		}else{?>
    			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$parentInfoArr['M']['englishname'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['HKUGAPS']['contactNum']?> Contact Number
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1MobileNo'];
    		}else{?>
    			<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" value="<?=$parentInfoArr['F']['mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2MobileNo'];
    		}else{?>
    			<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" value="<?=$parentInfoArr['M']['mobile'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['email']?> E-mail
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Email'];
    		}else{?>
    			<input name="G1Email" type="text" id="G1Email" class="textboxtext" value="<?=$parentInfoArr['F']['email'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Email'];
    		}else{?>
    			<input name="G2Email" type="text" id="G2Email" class="textboxtext" value="<?=$parentInfoArr['M']['email'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['occupation']?> Occupation
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Occupation'];
    		}else{?>
    			<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" value="<?=$parentInfoArr['F']['occupation'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Occupation'];
    		}else{?>
    			<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" value="<?=$parentInfoArr['M']['occupation'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
    		<?=$Lang['Admission']['HKUGAPS']['nameInstitution']?> Name of Institution
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Institution'];
    		}else{?>
    			<input name="G1Institution" type="text" id="G1Institution" class="textboxtext" value="<?=$parentInfoArr['F']['companyname'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Institution'];
    		}else{?>
    			<input name="G2Institution" type="text" id="G2Institution" class="textboxtext" value="<?=$parentInfoArr['M']['companyname'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
</table>


<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['HKUGAPS']['guardianName']?><br /> Name of Legal Guardian (if applicable)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['G3Name']?>
		<?php }else{ ?>
			<input name="G3Name" type="text" id="G3Name" class="textboxtext" value="<?=$parentInfoArr['G']['chinesename']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['HKUGAPS']['guardianReleation']?><br /> Relationship with applicant child (if applicable)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['G3Relationship']?>
		<?php }else{ ?>
			<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext"  value="<?=$parentInfoArr['G']['relationship']?>"/>
		<?php } ?>
	</td>
</tr>
</table>
