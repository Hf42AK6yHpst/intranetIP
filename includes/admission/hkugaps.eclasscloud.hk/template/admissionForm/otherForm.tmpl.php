<h1><?=$Lang['Admission']['otherInfo']?> Other Information</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>
<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['memberOfHKUGA']?> <br />
		Are you a life member of HKUGA
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ 
		    if($formData['memberOfHKUGA']){
		        echo "{$Lang['Admission']['yes']} Yes, {$Lang['Admission']['HKUGAPS']['memberName']} Member's Name: {$formData['memberOfHKUGAName']}, {$Lang['Admission']['HKUGAPS']['memberNo']} Membership No.: {$formData['memberOfHKUGANo']}";
		    }else{
		        echo "{$Lang['Admission']['no']} No";
		    }
		}else{ ?>
    			<input type="radio" name="memberOfHKUGA" id="memberOfHKUGAY"value="1" <?=$allCustInfo['HKUGA_Member'][0]['Value']=='1'?'checked':'' ?>/>
    			<label for="memberOfHKUGAY"><?=$Lang['Admission']['yes'] ?> Yes</label>&nbsp;&nbsp;
    		
    			<label for="memberOfHKUGAName"><?=$Lang['Admission']['HKUGAPS']['memberName'] ?> Member's Name</label>&nbsp;
    			<input name="memberOfHKUGAName" type="text" id="memberOfHKUGAName" value="<?=$allCustInfo['HKUGA_Member_Name'][0]['Value'] ?>"/>&nbsp;&nbsp;
    			
    			<label for="memberOfHKUGANo"><?=$Lang['Admission']['HKUGAPS']['memberNo'] ?> Membership No.</label>&nbsp;
    			<input name="memberOfHKUGANo" type="text" id="memberOfHKUGANo" value="<?=$allCustInfo['HKUGA_Member_No'][0]['Value'] ?>"/>

    			<br/>
    			<input type="radio" name="memberOfHKUGA" id="memberOfHKUGAN" value="0" <?=$allCustInfo['HKUGA_Member'][0]['Value']=='1'?'':'checked' ?>/>
    			<label for="memberOfHKUGAN"><?=$Lang['Admission']['no'] ?> No</label>

		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=($lac->isInternalUse($_GET['token']))? '':$star ?>
		<?=$Lang['Admission']['HKUGAPS']['memberOfHKUGAFoundation']?> <br />
		Are you a member of HKUGA Education Foundation
	</td>
	<td colspan="3">
		<?php if($IsConfirm){
		    if($formData['memberOfHKUGAFoundation']){
		        echo "{$Lang['Admission']['yes']} Yes, {$Lang['Admission']['HKUGAPS']['memberName']} Member's Name: {$formData['memberOfHKUGAFoundationName']}";
		    }else{
		        echo "{$Lang['Admission']['no']} No";
		    }
		}else{ ?>

    			<input type="radio" name="memberOfHKUGAFoundation" id="memberOfHKUGAFoundationY"value="1" <?=$allCustInfo['HKUGA_Foundation_Member'][0]['Value']=='1'?'checked':'' ?>/>
    			<label for="memberOfHKUGAFoundationY"><?=$Lang['Admission']['yes'] ?> Yes</label>&nbsp;&nbsp;
    			
    			<label for="memberOfHKUGAFoundationName"><?=$Lang['Admission']['HKUGAPS']['memberName'] ?> Member's Name</label>&nbsp;
    			<input name="memberOfHKUGAFoundationName" type="text" id="memberOfHKUGAFoundationName" value="<?=$allCustInfo['HKUGA_Foundation_Member_Name'][0]['Value'] ?>"/>&nbsp;
				<br/>
    			<input type="radio" name="memberOfHKUGAFoundation" id="memberOfHKUGAFoundationN" value="0" <?=$allCustInfo['HKUGA_Foundation_Member'][0]['Value']=='1'?'':'checked' ?>/>
    			<label for="memberOfHKUGAFoundationN"><?=$Lang['Admission']['no'] ?> No</label>
		<?php } ?>
	</td>
</tr>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
	<tr>
		<td rowspan="3" class="field_title">
			<?=$Lang['Admission']['HKUGAPS']['siblingInformation'] ?><br />
			Other sibling(s) studying/studied in HKUGA Primary School
		</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['nameChi'] ?> Name (Chinese)</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['nameEng'] ?> Name (English)</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['currentClass']?> Current Class</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['graduationYear'] ?> Graduation Year</center></td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(1)</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeStudiedNameChi1'];
    		}else{?>
    			<input name="OthersRelativeStudiedNameChi1" type="text" id="OthersRelativeStudiedNameChi1" class="textboxtext" value="<?=$siblingsInfoArr[1]['name'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeStudiedNameEng1'];
    		}else{?>
    			<input name="OthersRelativeStudiedNameEng1" type="text" id="OthersRelativeStudiedNameEng1" class="textboxtext" value="<?=$siblingsInfoArr[1]['englishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php 
    		if($IsConfirm){
    		    echo ($formData['OthersRelativeClassPosition1'])? $formData['OthersRelativeClassPosition1'] : ' -- ';
    		}else{
    		    $checked = ($siblingsInfoArr[1]['year'] == '')? 'checked' : '';
		    ?>
		    	<input type="radio" name="EnableOthersRelativeSchoolInfo1" value="class1" <?=$checked ?> />
    		    <select name="OthersRelativeClassPosition1" id="OthersRelativeClassPosition1">
					<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
        		    <?php
        		    for($i=1;$i<=6;$i++){
            		    for($j='A';$j<='D';$j++){
            		        $loopClass = $i.$j;
            		        $selected = ($loopClass == $siblingsInfoArr[1]['classposition'])? 'selected' : '';
        	        ?>
    	        			<option value="<?=$loopClass ?>" <?=$selected ?>><?=$loopClass ?></option>
        	        <?php
            		    }
        		    }
        		    ?>
    		    </select>
    		<?php 
    		}
    		?>
		</td>
		<td class="form_guardian_field">
    		<?php 
    		if($IsConfirm){
    		    echo ($formData['OthersRelativeGraduationYear1'])? $formData['OthersRelativeGraduationYear1'] : ' -- ';
    		}else{
    		    $checked = ($siblingsInfoArr[1]['year'] != '')? 'checked' : '';
		    ?>
		    	<input type="radio" name="EnableOthersRelativeSchoolInfo1" value="year1" <?=$checked ?> />
    		    <select name="OthersRelativeGraduationYear1" id="OthersRelativeGraduationYear1">
					<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
        		    <?php
        		    for($i=2006;$i<$admission_year_start;$i++){
        		        $selected = ($i == $siblingsInfoArr[1]['year'])? 'selected' : '';
        	        ?>
    	        			<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
        	        <?php
        		    }
        		    ?>
    		    </select>
    		<?php 
    		} 
    		?>
		</td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(2)</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeStudiedNameChi2'];
    		}else{?>
    			<input name="OthersRelativeStudiedNameChi2" type="text" id="OthersRelativeStudiedNameChi2" class="textboxtext" value="<?=$siblingsInfoArr[2]['name'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeStudiedNameEng2'];
    		}else{?>
    			<input name="OthersRelativeStudiedNameEng2" type="text" id="OthersRelativeStudiedNameEng2" class="textboxtext" value="<?=$siblingsInfoArr[2]['englishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php 
    		if($IsConfirm){
    		    echo ($formData['OthersRelativeClassPosition2'])? $formData['OthersRelativeClassPosition2'] : ' -- ';
    		}else{
    		    $checked = ($siblingsInfoArr[2]['year'] == '')? 'checked' : '';
		    ?>
		    	<input type="radio" name="EnableOthersRelativeSchoolInfo2" value="class2" <?=$checked ?> />
    		    <select name="OthersRelativeClassPosition2" id="OthersRelativeClassPosition2">
					<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
        		    <?php
        		    for($i=1;$i<=6;$i++){
            		    for($j='A';$j<='D';$j++){
            		        $loopClass = $i.$j;
            		        $selected = ($loopClass == $siblingsInfoArr[2]['classposition'])? 'selected' : '';
        	        ?>
    	        			<option value="<?=$loopClass ?>" <?=$selected ?>><?=$loopClass ?></option>
        	        <?php
            		    }
        		    }
        		    ?>
    		    </select>
    		<?php 
    		}
    		?>
		</td>
		<td class="form_guardian_field">
    		<?php 
    		if($IsConfirm){
    		    echo ($formData['OthersRelativeGraduationYear2'])? $formData['OthersRelativeGraduationYear2'] : ' -- ';
    		}else{
    		    $checked = ($siblingsInfoArr[2]['year'] != '')? 'checked' : '';
		    ?>
		    	<input type="radio" name="EnableOthersRelativeSchoolInfo2" value="year2" <?=$checked ?> />
    		    <select name="OthersRelativeGraduationYear2" id="OthersRelativeGraduationYear2">
					<option value="" > - <?=$Lang['Admission']['pleaseSelect'] ?> Please select - </option>
        		    <?php
        		    for($i=2006;$i<$admission_year_start;$i++){
        		        $selected = ($i == $siblingsInfoArr[2]['year'])? 'selected' : '';
        	        ?>
    	        			<option value="<?=$i ?>" <?=$selected ?>><?=$i ?></option>
        	        <?php
        		    }
        		    ?>
    		    </select>
    		<?php 
    		} 
    		?>
		</td>
	</tr>
</table>

<table id="dataTable1" class="form_table" style="font-size: 13px">
	<colgroup>
    	<col width="30%">
    	<col width="auto">
    	<col width="13%">
    	<col width="13%">
    	<col width="10%">
    	<col width="9%">
    	<col width="20%">
    	<col width="5%">
	</colgroup>
	<tr>
		<td rowspan="3" class="field_title">
			<?=$Lang['Admission']['HKUGAPS']['siblingOther'] ?><br />
			Other Siblings (Remarks: Please indicate if applicant is a twins)
		</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['nameChi'] ?> Name (Chinese)</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['nameEng'] ?> Name (English)</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['dateofbirth']?> Date of Birth</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['twins'] ?> Twins</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['school'] ?> School/College</center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['HKUGAPS']['grades'] ?> Grade</center></td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(1)</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeNameChi1'];
    		}else{?>
    			<input name="OthersRelativeNameChi1" type="text" id="OthersRelativeNameChi1" class="textboxtext" value="<?=$siblingsInfoArr[3]['name'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeNameEng1'];
    		}else{?>
    			<input name="OthersRelativeNameEng1" type="text" id="OthersRelativeNameEng1" class="textboxtext" value="<?=$siblingsInfoArr[3]['englishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeBirth1'];
    		}else{?>
    			<input name="OthersRelativeBirth1" type="text" id="OthersRelativeBirth1" class="textboxtext" maxlength="10" size="15" value="<?=$siblingsInfoArr[3]['dob'] ?>">(YYYY-MM-DD)
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    if($formData['OthersRelativeNameChi1'] != ' -- '){
        		    if($formData['OthersRelativeTwin1']){
        		        echo "{$Lang['Admission']['yes']} Yes";
        		    }else{
        		        echo "{$Lang['Admission']['no']} No";
        		    }
    		    }else{
    		        echo " -- ";
    		    }
    		}else{?>
    			<input type="radio" value="1" id="OthersRelativeTwin1Y" name="OthersRelativeTwin1" <?=$siblingsInfoArr[3]['istwins']=="1" ?'checked':'' ?>>
    			<label for="OthersRelativeTwin1Y"> <?=$Lang['Admission']['yes']?> Yes</label><br/>
    			<input type="radio" value="0" id="OthersRelativeTwin1N" name="OthersRelativeTwin1" <?=$siblingsInfoArr[3]['istwins']=="0" ?'checked':'' ?>>
    			<label for="OthersRelativeTwin1N"> <?=$Lang['Admission']['no']?> No</label>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeSchool1'];
    		}else{?>
    			<input name="OthersRelativeSchool1" type="text" id="OthersRelativeSchool1" class="textboxtext" value="<?=$siblingsInfoArr[3]['schoolName'] ?>">
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeGrade1'];
    		}else{?>
    			<input name="OthersRelativeGrade1" type="text" id="OthersRelativeGrade1" class="textboxtext" value="<?=$siblingsInfoArr[3]['classposition'] ?>">
    		<?php } ?>
		</td>
	</tr>
	<tr>
		<td class="field_title" style="text-align:right;width:50px;">(2)</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeNameChi2'];
    		}else{?>
    			<input name="OthersRelativeNameChi2" type="text" id="OthersRelativeNameChi2" class="textboxtext" value="<?=$siblingsInfoArr[4]['name'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeNameEng2'];
    		}else{?>
    			<input name="OthersRelativeNameEng2" type="text" id="OthersRelativeNameEng2" class="textboxtext" value="<?=$siblingsInfoArr[4]['englishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeBirth2'];
    		}else{?>
    			<input name="OthersRelativeBirth2" type="text" id="OthersRelativeBirth2" class="textboxtext" maxlength="20" size="25" value="<?=$siblingsInfoArr[4]['dob'] ?>">(YYYY-MM-DD)
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    if($formData['OthersRelativeNameChi2'] != ' -- '){
        		    if($formData['OthersRelativeTwin2']){
        		        echo "{$Lang['Admission']['yes']} Yes";
        		    }else{
        		        echo "{$Lang['Admission']['no']} No";
        		    }
    		    }else{
    		        echo " -- ";
    		    }
    		}else{?>
    			<input type="radio" value="1" id="OthersRelativeTwin2Y" name="OthersRelativeTwin2" <?=$siblingsInfoArr[4]['istwins']=="1" ?'checked':'' ?>>
    			<label for="OthersRelativeTwin2Y" > <?=$Lang['Admission']['yes']?> Yes</label><br/>
    			<input type="radio" value="0" id="OthersRelativeTwin2N" name="OthersRelativeTwin2" <?=$siblingsInfoArr[4]['istwins']=="0" ?'checked':'' ?>>
    			<label for="OthersRelativeTwin2N"> <?=$Lang['Admission']['no']?> No</label>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeSchool2'];
    		}else{?>
    			<input name="OthersRelativeSchool2" type="text" id="OthersRelativeSchool2" class="textboxtext" value="<?=$siblingsInfoArr[4]['schoolName'] ?>">
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['OthersRelativeGrade2'];
    		}else{?>
    			<input name="OthersRelativeGrade2" type="text" id="OthersRelativeGrade2" class="textboxtext" value="<?=$siblingsInfoArr[4]['classposition'] ?>">
    		<?php } ?>
		</td>
	</tr>
</table>


<script>
$(function() {
	var focusPairArr = [
        {
            'radio': '#memberOfHKUGAY',
            'text': '#memberOfHKUGAName'
        },
        {
            'radio': '#memberOfHKUGAFoundationY',
            'text': '#memberOfHKUGAFoundationName'
        }
	];

	$.each(focusPairArr, function(i, focusPair){
    	$(focusPair['radio']).click(function(){
    		$(focusPair['text']).focus();
    	});
    	$(focusPair['text']).keyup(function(e){
    		var code = e.keyCode || e.which;
    	    if (code != '9') { // Tab
    			$(focusPair['radio']).attr('checked', 'checked');
    	    }
    	});
	});

	$('input[name^="EnableOthersRelativeSchoolInfo"]').change(function(){
		var selected = $(this).attr('checked');
		if(selected){
    		var value = $(this).val();
    		var $enableItem, $disableItem;
    
    		if(value == 'class1'){
    			$enableItem = $('#OthersRelativeClassPosition1');
    			$disableItem = $('#OthersRelativeGraduationYear1');
    		}else if(value == 'year1'){
    			$disableItem = $('#OthersRelativeClassPosition1');
    			$enableItem = $('#OthersRelativeGraduationYear1');
    		}else if(value == 'class2'){
    			$enableItem = $('#OthersRelativeClassPosition2');
    			$disableItem = $('#OthersRelativeGraduationYear2');
    		}else{
    			$disableItem = $('#OthersRelativeClassPosition2');
    			$enableItem = $('#OthersRelativeGraduationYear2');
    		}
    			
    		$enableItem.removeAttr('disabled');
    		$disableItem.attr('disabled', 'disabled').val('');
		}
	});
	$('input[name^="EnableOthersRelativeSchoolInfo"]').change();
});
</script>
