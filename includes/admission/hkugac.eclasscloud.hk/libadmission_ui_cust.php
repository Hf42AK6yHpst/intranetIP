<?php
// modifying by:
/**
 * Change Log:
 * 2018-09-04 Pun
 * - File Created
 */
include_once("{$intranet_root}/includes/admission/libadmission_ui_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionUiCustBase.class.php");

class admission_ui_cust extends \AdmissionSystem\AdmissionUiCustBase
{

    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    private function init()
    {
        global $IsUpdate, $lac;
        // ### FILTER_ADMISSION_FORM_INSTRUCTION_HTML START ####
        /*$this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml = str_replace('class="instructionSuggestBrowser"', 'class="instructionSuggestBrowser" style="display:none;"', $formHtml);
            return $formHtml;
        }));
        $this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, array(
            $this,
            'instructionPageSchoolSelection'
        ));*/
        // ### FILTER_ADMISSION_FORM_INSTRUCTION_HTML END ####

        // ### FILTER_ADMISSION_FORM_WIZARD_STEPS START ####
        $this->addFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS, (function ($stepArr) {
            global $Lang, $LangB5, $LangEn;
            array_splice($stepArr, 2, 0, array(
                array(
                    'id' => 'pageAcademicPerformanceInfo',
                    'title' => '<div>' . $LangB5['Admission']['HKUGAC_academicPerformance'] . '</div><div>' . $LangEn['Admission']['HKUGAC_academicPerformance'] . '</div>',
                ),
            ));

            return $stepArr;
        }));
        if (!$IsUpdate && $lac && !$lac->isInternalUse($_GET['token'])) {
            $this->addFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS, (function ($stepArr) {
                global $Lang, $LangB5, $LangEn;
                array_splice($stepArr, 5, 0, array(
                    array(
                        'id' => 'pagePayment',
                        'title' => '<div>' . $LangB5['Admission']['payment'] . '</div><div>' . $LangEn['Admission']['payment'] . '</div>',
                    ),
                ));

                return $stepArr;
            }));
        }
        // ### FILTER_ADMISSION_FORM_WIZARD_STEPS END ####

        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML START ####
        $this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml .= $this->getInstructionForm($BirthCertNo, $IsUpdate);
            $formHtml .= $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
            $formHtml .= $this->getParentForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getOtherForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getAcademicPerformanceForm($IsConfirm, $BirthCertNo, $IsUpdate);
            $formHtml .= $this->getDocUploadForm($IsConfirm, $IsUpdate);
//            $formHtml .= $this->getPaymentForm($IsConfirm, $IsUpdate);
            return $formHtml;
        }));
        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML END ####

        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML START ####
//        $this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function ($formHtml, $IsUpdate) {
//            $formHtml .= $this->getStudentForm($IsConfirm = 1, '', $IsUpdate);
//            $formHtml .= $this->getParentForm($IsConfirm = 1, $IsUpdate);
//            $formHtml .= $this->getOtherForm($IsConfirm = 1, $IsUpdate);
//            $formHtml .= $this->getDocUploadForm($IsConfirm = 1, $IsUpdate);
//            return $formHtml;
//        }));
        // ### FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML END ####

        // ### FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML START ####
        $this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
            $formHtml .= $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
            $formHtml .= $this->getParentForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getOtherForm($IsConfirm, $IsUpdate);
            $formHtml .= $this->getAcademicPerformanceForm($IsConfirm, $BirthCertNo, $IsUpdate);
            $formHtml .= $this->getDocUploadForm($IsConfirm, $IsUpdate);

            return $formHtml;
        }));
        // ### FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML END ####

        // ### FILTER_ADMISSION_FORM_FINISH_HTML START ####
//        $this->addFilter(self::FILTER_ADMISSION_FORM_FINISH_HTML, (function ($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate) {
//            $formHtml .= $this->getPaymentForm($IsConfirm, $IsUpdate);
//            $formHtml .= $this->getFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '');
//            
//            return $formHtml;
//        }));
        // ### FILTER_ADMISSION_FORM_FINISH_HTML END ####

        // ### FILTER_ADMISSION_TOP_MENU_TAB START ####
        // Also add lang to lang/kis/apps/lang_admission_*.php
        $this->addFilter(self::FILTER_ADMISSION_TOP_MENU_TAB, (function ($defaultTab) {
            $newTab = array(
                'hkugac_academic_performance'
            );
            array_splice($defaultTab, 2, 0, $newTab);
            return $defaultTab;
        }));
        // ### FILTER_ADMISSION_TOP_MENU_TAB START ####

        // ### FILTER_ADMISSION_UPDATE_FORM_EMAIL_APPLICANT START ####
        $this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_EMAIL_APPLICANT, (function ($x, $ApplicationID) {
            global $lac;
            if ($lac->IsUpdateExtraAttachmentPeriod()) {
                $x = $this->getExtraAttachmentEmailContent($ApplicationID);
            }
            return $x;
        }));
        // ### FILTER_ADMISSION_UPDATE_FORM_EMAIL_APPLICANT START ####
    }

    /**
     * Admission Form - create/edit form student part
     */
    protected function getInstructionForm($BirthCertNo = "", $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg, $sus_status, $LangB5, $LangEn;

        $sus_status = ($IsConfirm) ? $formData['sus_status'] : $sus_status;

        $allClassLevel = $lac->getClassLevel();
        $settings = $lac->getApplicationSetting();
        $application_setting = $settings[$sus_status];

        $Instruction = $application_setting['FirstPageContent'];

        if (!$Instruction) {
            $Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'];
        }

        @ob_start();
        include(__DIR__ . "/template/admissionForm/instructionForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission Form - create/edit form student part
     */
    protected function getStudentForm($IsConfirm = 0, $BirthCertNo = "", $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg, $sus_status, $LangB5, $LangEn;

        $pageId = $IsConfirm ? 'pageConfirmation' : 'pagePersonalInfo';

        $sus_status = ($IsConfirm) ? $formData['sus_status'] : $sus_status;

        $allClassLevel = $lac->getClassLevel();
        $settings = $lac->getApplicationSetting();
        $application_setting = $settings[$sus_status];

        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';

        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);

            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

            if (count($application_details) > 0) {
                $StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'], '', $application_details['ApplicationID']));
                $StudentPrevSchoolInfo = $lac->getApplicationPrevSchoolInfo($application_details['ApplyYear'], '', $application_details['ApplicationID']);

                $dobYear = substr($StudentInfo['dateofbirth'], 0, 4);
                $dobMonth = substr($StudentInfo['dateofbirth'], 5, 2);
                $dobDay = substr($StudentInfo['dateofbirth'], 8, 2);
                $BirthCertNo = $StudentInfo['BirthCertNo'];
            }

            // ## Apply Year START ##
            $classLevelID = $StudentInfo['classLevelID'];
            $classLevel = $allClassLevel[$classLevelID];
            // ## Apply Year END ##

            // ## Apply School START ##
            $applySchoolId = $allCustInfo['ApplySchool'][0]['Value'];
            $applySchoolCode = $admission_cfg['SchoolType'][$applySchoolId];
            // ## Apply School END ##
        }

        @ob_start();
        include(__DIR__ . "/template/admissionForm/studentForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission Form - create/edit form Academic Performance part
     */
    protected function getAcademicPerformanceForm($IsConfirm = 0, $BirthCertNo = "", $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg, $sus_status, $LangB5, $LangEn;

        $pageId = $IsConfirm ? 'pageConfirmation' : 'pagePersonalInfo';

        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';

        if ($IsUpdate) {
            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
        }

        @ob_start();
        include(__DIR__ . "/template/admissionForm/academicPerformanceForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission Form - create/edit form parent part
     */
    protected function getParentForm($IsConfirm = 0, $IsUpdate = 0)
    {
        global $formData, $Lang, $lac, $admission_cfg, $LangB5, $LangEn;

        $pageId = $IsConfirm ? 'pageConfirmation' : 'pagePersonalInfo';

        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';

        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

            $tmpParentInfoArr = $lac->getApplicationParentInfo($application_details['ApplyYear'], '', $_SESSION['KIS_ApplicationID']);
            $parentInfoArr = array();
            foreach ($tmpParentInfoArr as $parent) {
                foreach ($parent as $para => $info) {
                    $ParentInfo[$parent['type']][$para] = $info;
                }
            }
            // ### Parent Info END ####
        }
        $parentTypeArr = array(
            1 => 'F',
            2 => 'M',
            3 => 'G'
        );

        @ob_start();
        include("template/admissionForm/parentForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission Form - create/edit form other part
     */
    protected function getOtherForm($IsConfirm = 0, $IsUpdate = 0)
    {
        global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, $lac, $kis_lang, $kis_lang_b5, $kis_lang_en, $LangB5, $LangEn, $sus_status;
        $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';

        $pageId = $IsConfirm ? 'pageConfirmation' : 'pagePersonalInfo';

        $classLevel = $lac->getClassLevel($sus_status);
        $classLevel = $classLevel[$sus_status];

        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
            $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
            $RelativeInfo = $lac->getApplicationRelativesInfo($application_details['ApplyYear'], '', $_SESSION['KIS_ApplicationID']);
        }

        @ob_start();
        include("template/admissionForm/otherForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission Form - create/edit form other part
     */
    protected function getDocUploadForm($IsConfirm = 0, $IsUpdate = 0, $AcademicYearID = 0, $YearID = 0)
    {
        global $Lang, $LangB5, $LangEn, $intranet_session_language;
        global $tempFolderPath, $fileData, $formData, $admission_cfg, $lac, $sys_custom, $intranet_root, $application_setting, $sus_status;

        $YearID = $sus_status = ($IsConfirm) ? $formData['sus_status'] : $sus_status;
        $pageId = $IsConfirm ? 'pageConfirmation' : 'pageDocsUpload';

        if ($IsConfirm) {
            $YearID = $sus_status;
        }

        if ($IsUpdate) {
            $application_details = $lac->getApplicationResult($_REQUEST['InputStudentDateOfBirth'], $_REQUEST['InputStudentBirthCertNo'], '', $_REQUEST['InputApplicationID']);

            if (count($application_details) > 0) {
                $applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'], array(
                    'applicationID' => $application_details['ApplicationID']
                ));
                $YearID = $application_details['ApplyLevel'];
            }
            // ## Photo START ##
            $viewFilePath = (is_file($intranet_root . "/file/admission/" . $applicationAttachmentInfo[$application_details['ApplicationID']]['personal_photo']['attachment_link'][0]) ? ' <a href="../admission_form/download_attachment.php?type=personal_photo' ./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]*/
                '" target="_blank" >檢視已遞交的檔案 View submitted file</a><br/>' : '');
            // ## Photo END ##
        }
        $settings = $lac->getApplicationSetting();
        $application_setting = $settings[$YearID];

        // ## Attachments START ##
        $settings = $lac->getAttachmentSettings();
        $attachment_settings = array();
        foreach ($settings as $index => $setting) {
            if ($setting['ClassLevelStr']) {
                $classLevelArr = explode(',', $setting['ClassLevelStr']);

                if (in_array($YearID, $classLevelArr)) {
                    $attachment_settings[$index] = $setting;
                }
            } else {
                $attachment_settings[$index] = $setting;
            }
        }
        // ## Attachments END ##

        if (!$lac->isInternalUse($_GET['token']) && !$IsUpdate) {
            $star = $IsConfirm ? '' : '<font style="color:red;">*</font>';
        } else {
            $star = '';
        }

        @ob_start();
        include("template/admissionForm/docUploadForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission Form - create/edit form payment part
     */
    protected function getPaymentForm($IsConfirm = 0, $IsUpdate = 0, $AcademicYearID = 0, $YearID = 0)
    {
        global $Lang;
        global $tempFolderPath, $fileData, $formData, $admission_cfg, $lac, $sys_custom, $intranet_root, $intranet_session_language;


        @ob_start();
        include("template/admissionForm/paymentForm.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    /**
     * Admission form - create applicant finish page
     */
    public function getFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;

        $hasPaid = 0;
        $cancelled = 0;
        $result = $lac->getPaymentResult('', '', '', '', $ApplicationID);
        if ($result) {
            foreach ($result as $aResult) {
                if ($aResult['Status'] >= $admission_cfg['Status']['paymentsettled'] && $aResult['Status'] != $admission_cfg['Status']['cancelled']) {
                    $hasPaid = 1;
                    //$isSent = $aResult['EmailSent'];
                    $paymentId = $aResult['PaymentID'];
                }
                if ($aResult['Status'] == $admission_cfg['Status']['cancelled']) {
                    $cancelled = 1;
                }
            }
        }
        $sql = "SELECT AutoEmailSent FROM ADMISSION_APPLICATION_STATUS Where ApplicationID = '" . $ApplicationID . "' ";
        $isSent = current($lac->returnArray($sql));

        if ($hasPaid || $lac->isInternalUse($_REQUEST['token'])) {
            if ($isSent['AutoEmailSent'] <= 0/* && !($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID)=='')*/) {
                $applicationSetting = $lac->getApplicationSetting($schoolYearID);
                $EmailContent = $applicationSetting[$sus_status]['EmailContent'];
                $mail_content = $lauc->getFinishPageEmailContent($ApplicationID, $EmailContent, $schoolYearID);
                if (!$lac->isInternalUse($_REQUEST['token'])) {
                    $lac->sendMailToNewApplicant($ApplicationID, '', $mail_content);
                }
                $sql = "UPDATE ADMISSION_APPLICATION_STATUS Set  AutoEmailSent = '1' Where  ApplicationID = '" . $ApplicationID . "' ";
                $lac->db_db_query($sql);
            }
            // finish page
            if ($ApplicationID) {
                $printLink = $lac->getPrintLink($schoolYearID, $ApplicationID, '', $this->getAdmissionLang($ApplicationID));
                $applicantEmail = $lac->getApplicantEmail($ApplicationID);

                $msg = '<span>
							報名表已遞交，申請編號為 <span class="applicationNo">' . $ApplicationID . '</span>。
							<br><br>
							Your application form has been successfully submitted.<br>
							Your application number is <span class="applicationNo">' . $ApplicationID . '</span>.<br><br>
							<span class="button button-secondary" id="btnPrint" onclick="window.open(\'' . $lac->getPrintLink("", $ApplicationID) . '\',\'_blank\');">' . $Lang['Admission']['printsubmitform'] . ' <span>Print submitted form</span></span>
						</span>';
                $msg2 = '<div>
							另外，閣下將會收到確認電郵，請檢查閣下在申請表填寫的電郵為 ' . $lac->getApplicantEmail($ApplicationID) . '。如未收到該電郵，請與本校聯絡。
							<br><br>
							An acknowledgement will be sent to you via email service. Please ensure that the email address entered on the form is ' . $lac->getApplicantEmail($ApplicationID) . '. Please contact our school in case you fail to receive it.
						</div>';
                if (!$LastContent || !$ApplicationID) {
                    $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
                }
                $x .= <<<HTML
	            <article id="blkFinish">
					<section id="blkApplicationNo">
						<span class="graphicWithThemeColor">
							<span>
								<img src="/images/kis/eadmission/graphic_finish.png">
							</span>
						</span><span>
							{$msg}		
						</span>
					</section>
					<section class="form sheet" id="blkFinishMsg">
						<div>
							{$msg2}
						</div>
						<div class="remark">
							{$LastContent}
						</div>
					</section>
				</article>
HTML;
            } else {
                $x .= <<<HTML
		            <article id="blkFinish">
						<section id="blkApplicationNo">
							<span class="graphicWithThemeColor">
								<span>
									<img src="/images/kis/eadmission/graphic_finish.png">
								</span>
							</span><span>
								未能成功遞交申請。請重新嘗試申請或致電與本校聯絡。
								<br><br>
								Your application is rejected. Please try to apply again or contact our school.
							</span>
						</section>
					</article>
HTML;
            }
        } else {
            // payment page
            if ($cancelled) {
                $x .= <<<HTML
		            <article id="blkFinish">
						<section id="blkApplicationNo">
							<span class="graphicWithThemeColor">
								<span>
									<img src="/images/kis/eadmission/graphic_finish.png">
								</span>
							</span><span>
								未能成功遞交申請。請重新嘗試申請或致電與本校聯絡。
								<br><br>
								Your application is rejected. Please try to apply again or contact our school.
							</span>
						</section>
					</article>
HTML;
            } else if ($ApplicationID) {
                $x .= <<<HTML
				<div id="payment_page">
					<section class="graphicWithThemeColor">
						<span>
							<img src="/images/kis/eadmission/graphic_submission.png">
						</span>
					</section>
					<section id="lblSubmitInst">
						現在請進行報名費付款 ，以完成報名程序。
						<br>
						To finish the online application, please pay now.
					</section>
					<section id="blkPay">
						<div>報名費 Application fee </div>
						<div id="lblApplicationFee">HK$50</div>
						<span class="button button-primary" id="btnPay" onclick="$('#payment_page').hide();$('#paypal_page').show();">付款 Pay</span>
					</section>
					</div>
HTML;

                $x .= '<div id="paypal_page" style="display:none">';
                $hashedApplicationID = MD5($result[0]['ApplicationID']);
                $x .= <<<HTML
					<section class="center">
						<img src="/images/kis/eadmission/paypal.png">
					</section>
					<section id="blkPayment">
						<div id="lblPayment">
							現在前往 Paypal 付款。完成後，必須於付款頁面按
							<div>「返回 {$admission_cfg['paypal_name']}」</div>
							方能完成整個付款及報名程序。<br><br>
							After paying the fee using Paypal, you MUST click
							<div>“Back to {$admission_cfg['paypal_name']}”</div>
							to complete the whole procedure.
						</div>
						<div class="remark">
							備註：請允許 Chrome 顯示彈出式視窗，以進入 Paypal 系統付款，若允許後顯示不到相關付款介面，請再按此步驟內的「Pay with PayPal」按鈕來嘗試。<br>
							Remark: Please allow pop-up to Paypal if blocked by your Chrome browser. If you cannot find the corresponding payment form in Paypal, please clisk the Payment icon in this step again.
						</div>
					</section>
					<section id="blkPaypal">
						<!-- PayPal Logo -->
						<table border="0" cellpadding="10" cellspacing="0" align="center">
							<tbody><tr>
								<td align="center"><a href="javascript:void(0)" title="PayPal 如何運作" onclick="javascript:window.open('https://{$_SERVER['HTTP_HOST']}/kis/admission_paypal/redirect_to_paypal.php?token={$hashedApplicationID}','payment_page','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/en_AU/i/buttons/btn_paywith_primary_m.png" alt="付款用 PayPal"></a></td>
							</tr>
						</tbody></table>
						<!-- PayPal Logo -->
					</section>
HTML;
//				$x .= $this->getWizardStepsUI(8);
//				$x .='<div class="admission_complete_msg"><h1><span><font color="red">*</font> 現在前往 Paypal 付款，完成後，必須於付款頁面按<br/> "<font color="red">返回'.$admission_cfg['paypal_name'].'</font>" <br/>方能完成整個付款及報名程序。<br/><br/><font color="red">*</font> After paying the fee using Paypal, you MUST click<br/> "<font color="red">Back to '.$admission_cfg['paypal_name'].'</font>" <br/> to complete the whole procedure.<br/><br/>';
//				$x .='<a href="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/redirect_to_paypal.php?token='.MD5($result[0]['ApplicationID']).'" target="payment_page" onclick="$(\'#nextStepLink\').show()">
//						<image src="https://www.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" alt="PayPal － 更安全、更簡單的網上付款方式！">
//						
//						<span id="nextStepLink" style="display:none;color: inherit;">
//							<br />
//							如果已使用 PayPal 成功繳費，但仍停留在此，可按此跳到下一步驟。<br />
//							If you already settle the payment using PayPal but no update to this page, you can click here to proceed to next step.
//						</span>
//						
//						</a></span></h1><br />';         
//						
//				$x .= '</div>';
                $x .= '</div>';

            } else {
                $x .= ' <div class="admission_complete_msg">';
                $x .= '<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
                $x .= '</div></div>';
            }
        }

        return $x;
    }

    function getExtraAttachmentEmailContent($ApplicationID)
    {
        global $lac;

        $rs = $lac->getAttachmentSettings();

        $updateFileNameArr = array();
        foreach ($rs as $index => $r) {
            if (!$r['IsExtra']) {
                continue;
            }

            if ($_FILES["OtherFile{$index}"]['size']) {
                $updateFileNameArr[] = $_FILES["OtherFile{$index}"]['name'];
            }
        }
        if (!$updateFileNameArr) {
            $lac->AdmissionFormSendEmail = false;
            return '';
        }
        $updateFileNameHtml = implode('</li><li>', $updateFileNameArr);

        $x = <<<HTML
<div style="max-width: 500px;">
    <p>
        Dear applicant,
        <br/><br/>
        The supplementary information with file name<br/> 
        <ul>
             <li>{$updateFileNameHtml}</li>
        </ul>
        is well received. Thank you. 
        <br/><br/>
        Regards,
        <br/><br/>
        HKUGA College Admission Team
    </p>
</div>
<div style="max-width: 500px;margin-top: 3rem;">
    <p>
        敬啟者：
        <br/><br/>
        本組已收妥 閣下名為
        <ul>
             <li>{$updateFileNameHtml}</li>
        </ul>
        的補充資料。謝謝！
        <br/><br/>
        此致
        <br/>
        申請人
        <br/>
        <span style="float: right;">港大同學會書院收生組謹啟</span>
    </p>
</div>
HTML;

        return $x;
    }

    function getFinishPageEmailContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $paymentEmail = '')
    {
        global $PATH_WRT_ROOT, $Lang, $lac, $admission_cfg, $sys_custom;
        include_once($PATH_WRT_ROOT . "lang/admission_lang.b5.php");

//		if($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID)==''){
//			if($ApplicationID){
//				$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
//				$stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
////				$x .='報讀學校 Applied School：　'.GET_SCHOOL_NAME().'<br/>';
////				$x .='報讀年級 Applied Level：'.$lac->classLevelAry[$stuedentInfo[0]['classLevelID']];
////				$x .='<br/>學生姓名：'.$stuedentInfo[0]['student_name_b5'];
////				$x .='<br/>Student Name：'.$stuedentInfo[0]['student_name_en'].'<br/><br/>';
//				
//				$x .='閣下的小一郵遞報名申請表已收到。學童姓名為 '.$stuedentInfo[0]['student_name_b5'].'，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';         
//				$x .='Your application form has been received, name of child is '.$stuedentInfo[0]['student_name_en'].' and the application number is <u>'.$ApplicationID.'</u>.';
//				
//				$x .='<br/><br/><br/>學校將透過電郵通知閣下有關面見安排，相關資訊可於學校網頁瀏覽。<br/>';
//				$x .='You will be informed of arrangements for interview(s) via email. Please visit the school website for related information.';
//				
//				$x .='<br/><br/><br/>謝謝您的申請！<br/>';            
//				$x .='Thanks for your application!';
//				
//				if(!$LastContent){
//					$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//				}
////				$x .='<br/><br/><table border="0" cellpadding="0" cellspacing="0" class="form">
////						<tr>
////							<td align="center">
////								<span style="font-size:large">
////									<br />
////									<br />
////									<span style="font-size: 20;font-weight: bold;text-decoration: underline;">Important</span>
////									<br />
////									<br />
////									Applicants will be informed of arrangements for interviews via email. <br />
////									Please visit the school website for related information.
////									<br />
////									<br />
////									<br />
////									<br />
////									<span style="font-size: 20;font-weight: bold;text-decoration: underline;">注意</span>
////									<br />
////									<br />
////									學校將透過電郵通知申請人有關面見安排，相關資訊可於學校網頁瀏覽。
////									<br />
////									<br />
////									<br />
////									<br />
////									&nbsp;
////								</span>
////							</td>
////						</tr>
////					</table>';
//				
//				$x .= '<br/><br/>'.$LastContent;
//			}
//		}else{
        if ($ApplicationID) {
            $sql = "Select Status From ADMISSION_APPLICATION_STATUS Where ApplicationID = '" . $ApplicationID . "'";
            $result = current($lac->returnArray($sql));
            $paymentSettle = 0;
            if ($result['Status'] >= $admission_cfg['Status']['paymentsettled'] && $result['Status'] != $admission_cfg['Status']['cancelled']) {
                $paymentSettle = 1;
            }
            if (/*$paymentEmail || */ $paymentSettle == 0) {
                $schoolYearID = $schoolYearID ? $schoolYearID : $lac->schoolYearID;
                $stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID, '', $ApplicationID);

                $x .= '已收到閣下的申請資料，學童姓名為 ' . $stuedentInfo[0]['student_name_b5'] . '，待繳付報名費後會作實申請。如未繳付，可按以下連結進行。<br/>';
                $x .= 'Your application form has been received, name of child is ' . $stuedentInfo[0]['student_name_en'] . '. To pay or check payment status, click this hyperlink:';
                $x .= "<br/><a target='_blank' href='https://" . $_SERVER['HTTP_HOST'] . "/kis/admission_form2/finish.php?id=" . urlencode(getEncryptedText("ApplicationID=" . $ApplicationID . "&sus_status=" . $stuedentInfo[0]['classLevelID'] . "&SchoolYearID=" . $schoolYearID, $admission_cfg['FilePathKey'])) . "'>https://" . $_SERVER['HTTP_HOST'] . "/kis/admission_form2/finish.php?id=" . urlencode(getEncryptedText("ApplicationID=" . $ApplicationID . "&sus_status=" . $stuedentInfo[0]['classLevelID'] . "&SchoolYearID=" . $schoolYearID, $admission_cfg['FilePathKey'])) . "</a>";
            } else {

                $x .= '報名表已遞交，申請編號為 <u>' . $ApplicationID . '</u>。<br/>';
                $x .= 'Your application form has been successfully submitted and the application number is <u>' . $ApplicationID . '</u>.<br/>';

//			$x .='<br/><br/>注意：所有申請必須支付報名費才有效；如未繳付或想檢查繳付情況，可按以下連結：<br/>';
//			$x .='Attention: Application is valid only if the application fee is paid. To pay or check payment status, click this hyperlink:';            
//			$x .='<br/>http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID);            

                $x .= '<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
                $x .= 'Click this hyperlink to view your application form.';
                $x .= "<br/><a target='_blank' href='https://" . $_SERVER['HTTP_HOST'] . $lac->getPrintLink("", $ApplicationID) . "'>https://" . $_SERVER['HTTP_HOST'] . $lac->getPrintLink("", $ApplicationID) . "</a>";

//			$x .='<br/><br/><br/>如需要更改已遞交的申請資料，可於報名截止前按以下連結。<br/>';
//			$x .='Click this hyperlink to amend your application form before the application deadline.';
//			$x .="<br/><a target='_blank' href='https://hkugaps.eclasscloud.hk/kis/admission_form/index_edit.php'>https://hkugaps.eclasscloud.hk/kis/admission_form/index_edit.php</a>";

                $x .= '<br/><br/><br/>謝謝您使用網上報名服務！<br/>';
                $x .= 'Thanks for lodging your application online!';

//			if(!$LastContent){
//				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//			}
                $x .= '<br/><br/>' . $LastContent;
            }
        } else {
            $x .= '未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
            $x .= '<br/><br/>';
            $x .= 'Your application is rejected. Please try to apply again or contact our school.';
        }
//		}
//		if($ApplicationID){
//			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);         
//			$x .='<br/><br/>';
//			$x .='Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>Application form preview<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);
//			if(!$LastContent){
//				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//			}
//			$x .= '<br/>'.$LastContent;
//		}
//		else{
//			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
//			$x .='<br/><br/>';
//			$x .='Admission is Not Completed. Please try to apply again!';
//        }

        return $x;
    }

    function getTimeOutPageContent($ApplicationID = '', $LastContent = '')
    {
        global $Lang, $lac, $admission_cfg, $sys_custom;

        if ($ApplicationID) {
            $msg = '<span>
					報名表已遞交，申請編號為 <span class="applicationNo">' . $ApplicationID . '</span>。
					<br><br>
					Your application form has been successfully submitted.<br>
					Your application number is <span class="applicationNo">' . $ApplicationID . '</span>.<br><br>
					<span class="button button-secondary" id="btnPrint" onclick="window.open(\'' . $lac->getPrintLink("", $ApplicationID) . '\',\'_blank\');">' . $Lang['Admission']['printsubmitform'] . ' <span>Print submitted form</span></span>
				</span>';

            $x .= <<<HTML
		            <article id="blkFinish">
						<section id="blkApplicationNo">
							<span class="graphicWithThemeColor">
							<span>
								<img src="/images/kis/eadmission/graphic_finish.png">
							</span>
							</span><span>
								{$msg}		
							</span>
						</section>
					</article>
HTML;
        } else {
            $x .= <<<HTML
		            <article id="blkFinish">
						<section id="blkApplicationNo">
							<span>
								未能成功遞交申請。請重新嘗試申請或致電與本校聯絡。
								<br><br>
								Your application is rejected. Please try to apply again or contact our school.
							</span>
						</section>
					</article>
HTML;
        }

//        if (! $lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
//            $x .= '<div class="edit_bottom">
//					<input type="button" class="formsubbutton" onclick="location.href=\'' . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST'] . '\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
//				</div>
//				<p class="spacer"></p></div>';
//        } else {
//            $x .= '<div class="edit_bottom">
//					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
//				</div>
//				<p class="spacer"></p></div>';
//        }
        return $x;
    }

    function getQuotaFullPageContent($type = 'Admission', $LastContent = '')
    {
        global $Lang, $lac, $admission_cfg, $sys_custom;
        $x = '<div class="admission_board">';
        if (!$LastContent) {
            if ($type == 'Admission') {
                $LastContent = '<span>' . $Lang['Admission']['munsang']['msg']['admissionQuotaFull'];
                $LastContent .= '<br><br>Admission Quota is Full! Thanks for your support!</span>';
            } else
                if ($type == 'Interview') {
                    $LastContent = '<span>' . $Lang['Admission']['munsang']['msg']['interviewQuotaFull'];
                    $LastContent .= '<br><br>Interview Timeslot Quota is Full! Please try to apply again!</span>';
                } else {
                    $LastContent .= '<span>未能成功遞交申請。請重新嘗試申請或致電與本校聯絡。
									<br><br>
									Your application is rejected. Please try to apply again or contact our school.</span>';
                }
        }
        $x .= <<<HTML
		            <article id="blkFinish">
						<section id="blkApplicationNo">
							{$LastContent}
						</section>
					</article>
HTML;

//        if (! $lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
//            $x .= '<div class="edit_bottom">
//					<input type="button" class="formsubbutton" onclick="location.href=\'' . $admission_cfg['IntegratedCentralServer'] . '?af=' . $_SERVER['HTTP_HOST'] . '\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
//				</div>
//				<p class="spacer"></p></div>';
//        } else {
//            $x .= '<div class="edit_bottom">
//					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="' . $Lang['Admission']['finish'] . ' Finish" />
//				</div>
//				<p class="spacer"></p></div>';
//        }
        return $x;
    }

    function getPayPalButton($ApplicationID)
    {
        global $admission_cfg, $lac;
        $ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
        return '<form action="' . $admission_cfg['paypal_url'] . '" <!--onsubmit="checkPayment(\'' . $ApplicationID . '\');return false;"--> method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="' . $admission_cfg['hosted_button_id'] . '">
				<input type="hidden" name="return" value="https://' . $_SERVER['HTTP_HOST'] . '/kis/admission_paypal/payment_finish2.php" /> 
				    <input type="hidden" name="cancel_return" value="https://' . $_SERVER['HTTP_HOST'] . '/kis/admission_paypal/payment_finish2.php?cm=' . $ApplicationID . '" />
				    <input type="hidden" name="custom" value="' . $ApplicationID . '" />
					<input type="hidden" name="notify_url" value="https://' . $_SERVER['HTTP_HOST'] . '/kis/admission_paypal/payment_ipn.php" /> 
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();
				        
						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
    }

    function getInterviewResultContent($result, $result2, $result3)
    {
        if ($_REQUEST['ApplicationNo'] == '') {
            echo 0;
            exit();
        }
        global $libkis_admission, $lac, $admission_cfg, $kis_lang;

        $emailTemplates = $libkis_admission->getEmailTemplateRecords(1/* Only Active */);
        $basicSettings = $libkis_admission->getBasicSettings($libkis_admission->schoolYearID, array('firstInterviewAnnouncement', 'secondInterviewAnnouncement', 'thirdInterviewAnnouncement', 'applicationStatusAnnouncement'));

        $sql = "SELECT Status FROM ADMISSION_APPLICATION_STATUS WHERE ApplicationID = '" . $_REQUEST['ApplicationNo'] . "'";
        $applicationStatus = current($lac->returnArray($sql));
        $applicationStatus = $applicationStatus['Status'];

        $sql = "SELECT ApplyLevel FROM ADMISSION_OTHERS_INFO WHERE ApplicationID = '" . $_REQUEST['ApplicationNo'] . "'";
        $classLevelId = current($lac->returnArray($sql));
        $classLevelId = $classLevelId['ApplyLevel'];

        @ob_start();
        include("template/interview/interviewResult.tmpl.php");
        $x = ob_get_clean();

        return $x;
    }

    function getUpdateFinishPageContent($ApplicationID = '', $LastContent = '', $schoolYearID = '', $sus_status = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;
        // finish page

        if ($ApplicationID) {
            $msg = '<span>
						報名表資料已更改，申請編號為 <span class="applicationNo">' . $ApplicationID . '</span>。
						<br><br>
						Your application form has been successfully updated.<br>
						Your application number is <span class="applicationNo">' . $ApplicationID . '</span>.<br><br>
						<span class="button button-secondary" id="btnPrint" onclick="window.open(\'' . $lac->getPrintLink("", $ApplicationID) . '\',\'_blank\');">' . $Lang['Admission']['printsubmitform'] . ' <span>Print submitted form</span></span>
					</span>';
            $msg2 = '<div>
						另外，閣下將會收到確認電郵，請檢查閣下在申請表填寫的電郵為 ' . $lac->getApplicantEmail($ApplicationID) . '。如未收到該電郵，請與本校聯絡。
						<br><br>
						An acknowledgement will be sent to you via email service. Please ensure that the email address entered on the form is ' . $lac->getApplicantEmail($ApplicationID) . '. Please contact our school in case you fail to receive it.
					</div>';
            if (!$LastContent || !$ApplicationID) {
                $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
            }
            $x .= <<<HTML
            <article id="blkFinish">
				<section id="blkApplicationNo">
					<span class="graphicWithThemeColor">
						<span>
							<img src="/images/kis/eadmission/graphic_finish.png">
						</span>
					</span><span>
						{$msg}		
					</span>
				</section>
				<section class="form sheet" id="blkFinishMsg">
					<div>
						{$msg2}
					</div>
					<div class="remark">
						{$LastContent}
					</div>
				</section>
			</article>
HTML;
        } else {
            $x .= <<<HTML
		            <article id="blkFinish">
						<section id="blkApplicationNo">
							<span class="graphicWithThemeColor">
								<span>
									<img src="/images/kis/eadmission/graphic_finish.png">
								</span>
							</span><span>
								未能成功更改資料。請重新嘗試申請或致電與本校聯絡。
								<br><br>
								Information cannot be updated. Please try to apply again or contact our school.
							</span>
						</section>
					</article>
HTML;
        }

        return $x;
    }

    function getAfterUpdateFinishPageContent($ApplicationID = '', $LastContent = '')
    {
        global $Lang, $lac, $lauc, $admission_cfg, $sys_custom;

        $msg = '<span>
					申請編號為 <span class="applicationNo">' . $ApplicationID . '</span>。
					<br><br>
					Your application form has been successfully submitted.<br>
					Your application number is <span class="applicationNo">' . $ApplicationID . '</span>.<br><br>
					<span class="button button-secondary" id="btnPrint" onclick="window.open(\'' . $lac->getPrintLink("", $ApplicationID) . '\',\'_blank\');">' . $Lang['Admission']['printsubmitform'] . ' <span>Print submitted form</span></span>
				</span>';
        $msg1 = $this->getDocUploadForm(1, 1);

        $applicationStatus = current($lac->getApplicationStatus($lac->schoolYearID, '', $ApplicationID));

        if ($applicationStatus['interviewdate'] && $applicationStatus['interviewdate'] != '0000-00-00 00:00:00') {
            $interviewDateTime = explode(" ", $applicationStatus['interviewdate']);
            $msg2 .= '<div>';
            $msg2 .= '<h1 style="font-size: 15px">面試資料 Interview Information</h1>';
            $msg2 .= '<table class="form_table" style="font-size: 15px">';
            $msg2 .= '<tr>';
            $msg2 .= '<td class="field_title">面試日期 Interview Date</td>';
            $msg2 .= '<td>' . $interviewDateTime[0] . '</td>';
            $msg2 .= '</tr>';
            $msg2 .= '<td class="field_title">面試時間 Interview Time</td>';
            $msg2 .= '<td>' . substr($interviewDateTime[1], 0, -3) . '</td>';
            $msg2 .= '</tr>';
            $msg2 .= '<td class="field_title">面試地點 Interview Location</td>';
            $msg2 .= '<td>' . $applicationStatus['interviewlocation'] . '</td>';
            $msg2 .= '</tr>';
            $msg2 .= '</table>';
            $msg2 .= '</div>';
        }

        if (!$LastContent) {
            $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; // Henry 20131107
        }

        $x .= <<<HTML
	            <article id="blkFinish">
					<section id="blkApplicationNo">
						<span class="graphicWithThemeColor">
							<span>
								<img src="/images/kis/eadmission/graphic_finish.png">
							</span>
						</span><span>
							{$msg}		
						</span>
					</section>
					{$msg1}
					<section class="form sheet" id="blkFinishMsg">
						{$msg2}
						<div class="remark">
							{$LastContent}
						</div>
					</section>
				</article>
HTML;

        return $x;
    }

    public function getApplicationForm($BirthCertNo = "", $YearID = "", $currentStep = 2)
    {
        global $fileData, $formData, $tempFolderPath, $LangB5, $LangEn, $libkis_admission, $sys_custom, $admission_cfg, $lac;

        $x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, $html = '', $IsConfirm = false, $BirthCertNo, $IsUpdate = false, $YearID);
//    	$x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, $html = '', $IsUpdate = false, $YearID);

        return $x;
    }

    public function getApplicationUpdateForm($BirthCertNo = "", $YearID = "", $currentStep = 2)
    {
        global $fileData, $formData, $tempFolderPath, $LangB5, $LangEn, $libkis_admission, $sys_custom, $admission_cfg, $lac;

        global $isAllowUpdate, $isUpdateExtraAttachment;
        $isAllowUpdate = $lac->IsUpdatePeriod();
        $isUpdateExtraAttachment = $lac->IsUpdateExtraAttachmentPeriod();
        $x .= $this->applyFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, $html = '', $IsConfirm = false, $BirthCertNo, $IsUpdate = true, $YearID);
//    	$x .= $this->applyFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, $html = '', $IsUpdate = false, $YearID);

        return $x;
    }

    /**
     * Portal - print admission form
     */
    public function getPDFContent($schoolYearID, $applicationIDAry, $type = '')
    {
        global $PATH_WRT_ROOT, $lac, $admission_cfg, $setting_path_ip_rel, $plugin, $kis_lang;

        global $baseFilePath;
        $baseFilePath = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}";
        $baseFilePath = $this->applyFilter(self::FILTER_ADMISSION_PDF_BASE_PATH, $baseFilePath);

        $yearStart = date('Y', getStartOfAcademicYear('', $schoolYearID));
        $yearEnd = (((int)$yearStart) + 1);
        // ####### Init PDF START ########
        $templateHeaderPath = "{$baseFilePath}/pdf/header.php";
        $templatePath = "{$baseFilePath}/pdf/application_form.php";
        require_once($PATH_WRT_ROOT . "includes/mpdf/mpdf.php");

        $mpdf = new \mPDF($mode = '', $format = 'A4', $default_font_size = 0, $default_font = 'msjh', $marginLeft = 0, $marginRight = 0, $marginTop = 0, $marginBottom = 0/*,
            $marginHeader=9,
            $marginFooter=9,
            $orientation='P'*/
        );
        $mpdf->mirrorMargins = 1;
        // ####### Init PDF END ########

        // ####### Load header to PDF START ########
        ob_start();
        include($templateHeaderPath);
        $pageHeader = ob_get_clean();

        $mpdf->WriteHTML($pageHeader);
        // ####### Load header to PDF END ########

        // ####### Load data to PDF START ########
        global $applicationIndex, $applicationID;
        foreach ((array)$applicationIDAry as $applicationIndex => $applicationID) {

            // ### Load Template START ####
            ob_start();
            include($templatePath);
            $page1 = ob_get_clean();
            // ### Load Template END ####

            $mpdf->WriteHTML($page1);
        }
        // ####### Load data to PDF END ########

        $mpdf->Output();
    }


    public function getAdminModuleTab($module)
    {
        switch ($module) {
            case 'interview':
                return array('interviewarrangement', 'timeslotsettings', 'announcementsettings');
        }
        return array();
    }
} // End Class