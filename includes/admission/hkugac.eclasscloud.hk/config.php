<?php
//using:
/*
 * This page is for admission only. For general KIS config : kis/config.php
 */

$admission_cfg['PrintByPDF'] = 1;

//Please also define lang in admission_lang
$admission_cfg['Status'] = array();
$admission_cfg['Status']['pending'] = 1;
$admission_cfg['Status']['paymentsettled'] = 2;
$admission_cfg['Status']['waitingforinterview'] = 3;
$admission_cfg['Status']['confirmed'] = 4;
$admission_cfg['Status']['cancelled'] = 5;
$admission_cfg['Status']['waitlisted'] = 6;
$admission_cfg['Status']['HKUGAC_waitingforinterview_1'] = 7;
$admission_cfg['Status']['HKUGAC_completedforinterview_1'] = 8;
$admission_cfg['Status']['HKUGAC_absentforinterview_1'] = 9;
$admission_cfg['Status']['HKUGAC_waitingforinterview_2'] = 10;
$admission_cfg['Status']['HKUGAC_completedforinterview_2'] = 11;
$admission_cfg['Status']['HKUGAC_absentforinterview_2'] = 12;
$admission_cfg['Status']['HKUGAC_waitingforinterview_3'] = 13;
$admission_cfg['Status']['HKUGAC_completedforinterview_3'] = 14;
$admission_cfg['Status']['HKUGAC_absentforinterview_3'] = 15;
$admission_cfg['Status']['HKUGAC_registered'] = 16;
$admission_cfg['Status']['HKUGAC_rejectedoffer'] = 17;
$admission_cfg['Status']['HKUGAC_cancelledregistration'] = 18;
$admission_cfg['Status']['HKUGAC_notadmitted']	= 19;

$admission_cfg['PaymentStatus']['OnlinePayment'] = 1;
$admission_cfg['PaymentStatus']['OtherPayment'] = 2;

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;	
//	$admission_cfg['Status']['reservedstudent']	= 5;

//	$admission_cfg['BirthCertType'] = array();
//	$admission_cfg['BirthCertType']['hk']	= 1;
//	$admission_cfg['BirthCertType']['oversea']	= 2;
//	$admission_cfg['BirthCertType']['mainland']	= 3;
//	$admission_cfg['BirthCertType']['others']	= 4;
//	
//	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
//	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
//	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
//	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
//	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
//	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
//	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
//	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);		

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '港大同學會書院';
$admission_cfg['SchoolName']['en'] = 'HKUGA College';

$admission_cfg['themeStyle'] = 'Secondary';

$admission_cfg['Subjects'] = array(
    'Chinese',
    'English',
    'Mathematic',
    'Putonghua',
    'GeneralStudiesSocial',
    'Science',
    'Religious',
    'Computer',
    'Music',
    'VisualArts',
    'PhysicalEducation',
    'AverageScore',
    'Conduct',
    'AverageScoreInForm',
    'PositionInForm',
    'PositionInClass'
);

$admission_cfg['EmailTemplateType'] = array();
$admission_cfg['EmailTemplateType']['Normal'] = 0;
$admission_cfg['EmailTemplateType']['Round1_Interview']['b5'] = 1;
$admission_cfg['EmailTemplateType']['Round1_Interview']['en'] = 2;
$admission_cfg['EmailTemplateType']['Round1_Interview_CannotArrange']['b5'] = 3;
$admission_cfg['EmailTemplateType']['Round1_Interview_CannotArrange']['en'] = 4;
$admission_cfg['EmailTemplateType']['Round2_Interview']['b5'] = 5;
$admission_cfg['EmailTemplateType']['Round2_Interview']['en'] = 6;
$admission_cfg['EmailTemplateType']['Round2_Interview_CannotArrange']['b5'] = 7;
$admission_cfg['EmailTemplateType']['Round2_Interview_CannotArrange']['en'] = 8;
$admission_cfg['EmailTemplateType']['Round3_Interview']['b5'] = 9;
$admission_cfg['EmailTemplateType']['Round3_Interview']['en'] = 10;
$admission_cfg['EmailTemplateType']['Round3_Interview_CannotArrange']['b5'] = 11;
$admission_cfg['EmailTemplateType']['Round3_Interview_CannotArrange']['en'] = 12;
$admission_cfg['EmailTemplateType']['Admitted']['b5'] = 13;
$admission_cfg['EmailTemplateType']['Admitted']['en'] = 14;
$admission_cfg['EmailTemplateType']['NotAdmitted']['b5'] = 15;
$admission_cfg['EmailTemplateType']['NotAdmitted']['en'] = 16;

$admission_cfg['extra_attachment_period_edit_email'] = array(
//    'hpmak@g2.broadlearning.com',
//    'sienasze@broadlearning.com',
    'admission@hkugac.edu.hk',
);
// ####### Cust config END ########

if ($plugin['eAdmission_devMode']) {
    /* for paypal testing [start] */
    $admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
    $admission_cfg['paypal_signature'] = '';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = 'AF2015';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
    $admission_cfg['hosted_button_id'] = '6KGPWBMHHMVWY';
    $admission_cfg['paypal_name'] = "test facilitator's Test Store";
    /* for paypal testing [End] */
}else {
    /* for real paypal use [start] */
    $admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
    $admission_cfg['paypal_signature'] = 'G-fV6A8lapcxSE8FvG7Br8azGnzKGQiksdmL-mTHdhQUoF16TT_NcWAGbmq';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = 'AF2015';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
    $admission_cfg['hosted_button_id'] = '4A2589M6DSVHG';
    $admission_cfg['paypal_name'] = 'The Incorporated Management Committee of HKUGA College';
    /* for real paypal use [End] */
}

$admission_cfg['FilePath'] = $PATH_WRT_ROOT . "/file/admission/";
$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
$admission_cfg['DefaultLang'] = "b5";
$admission_cfg['IsBilingual'] = true;
$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
$admission_cfg['personal_photo_width'] = 200;
$admission_cfg['personal_photo_height'] = 260;
?>