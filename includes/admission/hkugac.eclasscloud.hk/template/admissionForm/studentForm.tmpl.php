<section id="studentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation"
         style="display: none">
    <div class="form-header marginB10">
        <?= $LangB5['Admission']['HKUGAC']['ApplicantInfo'] ?> <?= $LangEn['Admission']['HKUGAC']['ApplicantInfo'] ?>
    </div>
    <div class="sheet">

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentSTRN" id="StudentSTRN"
                           value="<?= $allCustInfo['StudentSTRN'][0]['Value'] ?>" required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['HKUGAC']['strn'] ?> <?= $LangEn['Admission']['HKUGAC']['strn'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['strn'] ?> <?= $LangEn['Admission']['HKUGAC']['strn'] ?></div>
				<div class="dataValue <?= $allCustInfo['StudentSTRN'][0]['Value'] ? '' : 'dataValue-empty' ?>"><?= $allCustInfo['StudentSTRN'][0]['Value'] ? $allCustInfo['StudentSTRN'][0]['Value'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput itemInput-half">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentChineseName" id="StudentChineseName"
                           value="<?= $StudentInfo['ChineseName'] ?>">
					<div class="textboxLabel"><?= $LangB5['Admission']['HKUGAPS']['chinesename'] ?> <?= $LangEn['Admission']['HKUGAPS']['chinesename'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAPS']['chinesename'] ?> <?= $LangEn['Admission']['HKUGAPS']['chinesename'] ?></div>
				<div class="dataValue <?= $StudentInfo['ChineseName'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['ChineseName'] ? $StudentInfo['ChineseName'] : '－－' ?></div>
			</span><span class="itemInput itemInput-half">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentEnglishName" id="StudentEnglishName"
                           value="<?= $StudentInfo['EnglishName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['HKUGAC']['englishname'] ?> <?= $LangEn['Admission']['HKUGAC']['englishname'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData itemData-half">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['englishname'] ?> <?= $LangEn['Admission']['HKUGAC']['englishname'] ?></div>
				<div class="dataValue <?= $StudentInfo['EnglishName'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['EnglishName'] ? $StudentInfo['EnglishName'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
            <div class="itemLabel <?= $lac->isInternalUse($_GET['token']) ? '' : 'requiredLabel' ?>"><?= $LangB5['Admission']['gender'] ?> <?= $LangEn['Admission']['gender'] ?></div>
            <span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="StudentGender_M" name="StudentGender"
                           value="M" <?= $lac->isInternalUse($_GET['token']) ? '' : 'required' ?> <?= $StudentInfo['Gender'] == "M" ? 'checked' : '' ?>>
					<label for="StudentGender_M"><?= $LangB5['Admission']['genderType']['M'] ?> <?= $LangEn['Admission']['genderType']['M'] ?></label>
				</span>
				<span>
					<input type="radio" id="StudentGender_F" name="StudentGender"
                           value="F" <?= $lac->isInternalUse($_GET['token']) ? '' : 'required' ?> <?= $StudentInfo['Gender'] == "F" ? 'checked' : '' ?>>
					<label for="StudentGender_F"><?= $LangB5['Admission']['genderType']['F'] ?> <?= $LangEn['Admission']['genderType']['F'] ?></label>
    				<div class="remark remark-warn hide"><?= $LangB5['Admission']['pleaseSelect'] ?> <?= $LangEn['Admission']['pleaseSelect'] ?></div>
				</span>
			</span>
            <div class="itemData">
                <div class="dataValue <?= $StudentInfo['Gender'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['Gender'] ? $StudentInfo['Gender'] : '－－' ?></div>
            </div>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentAge" id="StudentAge" value="<?= $StudentInfo['Age'] ?>" required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['Age'] ?> <?= $LangEn['Admission']['Age'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['Age'] ?> <?= $LangEn['Admission']['Age'] ?></div>
				<div class="dataValue <?= $StudentInfo['Age'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['Age'] ? $StudentInfo['Age'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentNationality" id="StudentNationality"
                           value="<?= $StudentInfo['Nationality'] ?>" required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['nationality'] ?> <?= $LangEn['Admission']['nationality'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['nationality'] ?> <?= $LangEn['Admission']['nationality'] ?></div>
				<div class="dataValue <?= $StudentInfo['Nationality'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['Nationality'] ? $StudentInfo['Nationality'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentBirthCertNo" id="StudentBirthCertNo" value="<?= $BirthCertNo ?>"
                           required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['HKUGAC']['birthcertno'] ?> <?= $LangEn['Admission']['HKUGAC']['birthcertno'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errStudentBirthCertNo"></div>
					<div class="remark">(<?= $LangB5['Admission']['BirthcertNoHints'] ?> <?= $LangEn['Admission']['BirthcertNoHints'] ?>)</div>
					<div class="remark" style="padding-left: 1rem;">
                        <?= $LangB5['Admission']['HKUGAC']['msg']['BirthCertNoHints'] ?>
                        <br/>
                        <?= $LangEn['Admission']['HKUGAC']['msg']['BirthCertNoHints'] ?>
                    </div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['birthcertno'] ?> <?= $LangEn['Admission']['HKUGAC']['birthcertno'] ?></div>
				<div class="dataValue <?= $BirthCertNo ? '' : 'dataValue-empty' ?>"><?= $BirthCertNo ? $BirthCertNo : '－－' ?></div>
			</span>
        </div>

        <div class="item">
            <div class="itemLabel requiredLabel"><?= $LangB5['Admission']['dateofbirth'] ?> <?= $LangEn['Admission']['dateofbirth'] ?></div>
            <span class="itemInput itemInput-selector">
				<span class="selector">
					<select id="StudentDateOfBirthYear" name="StudentDateOfBirthYear" class="dob dateYear">
						<option value=""
                                hidden><?= $LangB5['Admission']['year'] ?> <?= $LangEn['Admission']['year'] ?></option>
						<?php
                        if ($application_setting['DOBStart'] && $application_setting['DOBEnd']) {
                            $startYear = substr($application_setting['DOBStart'], 0, 4);
                            $endYear = substr($application_setting['DOBEnd'], 0, 4);
                        } else {
                            $startYear = date('Y') - 15;
                            $endYear = date('Y');
                        }
                        for ($i = $endYear; $i >= $startYear; $i--):
                            ?>
                            <option value="<?= $i ?>" <?= $dobYear == $i ? 'selected' : '' ?>><?= $i ?></option>
                        <?php
                        endfor;
                        ?>
					</select>
				</span><span class="selector">
					<select id="StudentDateOfBirthMonth" name="StudentDateOfBirthMonth" class="dob dateMonth">
						<option value=""
                                hidden><?= $LangB5['Admission']['month'] ?> <?= $LangEn['Admission']['month'] ?></option>
						<?php for ($i = 1; $i <= 12; $i++): ?>
                            <option value="<?= str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?= $dobMonth == $i ? 'selected' : '' ?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
                        <?php endfor; ?>
					</select>
				</span><span class="selector">
					<select id="StudentDateOfBirthDay" name="StudentDateOfBirthDay" class="dob dateDay">
						<option value=""
                                hidden><?= $LangB5['Admission']['day'] ?> <?= $LangEn['Admission']['day'] ?></option>
						<?php for ($i = 1; $i <= 31; $i++): ?>
                            <option value="<?= str_pad($i, 2, "0", STR_PAD_LEFT) ?>" <?= $dobDay == $i ? 'selected' : '' ?>><?= str_pad($i, 2, "0", STR_PAD_LEFT) ?></option>
                        <?php endfor; ?>
					</select>
				</span>
				<span>
					<div class="remark remark-warn hide"
                         id="errDobRequired"><?= $LangB5['Admission']['pleaseSelect'] ?> <?= $LangEn['Admission']['pleaseSelect'] ?></div>
					<div class="remark remark-warn hide"
                         id="errDobRange"><?= $LangB5['Admission']['FH']['msg']['invalidbdaydateformat'] ?> <?= $LangEn['Admission']['FH']['msg']['invalidbdaydateformat'] ?></div>
				</span>
			</span>
            <div class="itemData">
                <div class="dataValue <?= $dobYear && $dobMonth && $dobDay ? '' : 'dataValue-empty' ?>"><?= $dobYear && $dobMonth && $dobDay ? $dobYear . '-' . $dobMonth . '-' . $dobDay : '－－' ?></div>
            </div>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentPlaceOfBirth" id="StudentPlaceOfBirth"
                           value="<?= $StudentInfo['PlaceOfBirth'] ?>" <?= $lac->isInternalUse($_GET['token']) ? '' : 'required' ?>>
					<div class="textboxLabel <?= $lac->isInternalUse($_GET['token']) ? '' : 'requiredLabel' ?>"><?= $LangB5['Admission']['placeofbirth'] ?> <?= $LangEn['Admission']['placeofbirth'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['placeofbirth'] ?> <?= $LangEn['Admission']['placeofbirth'] ?></div>
				<div class="dataValue <?= $StudentInfo['PlaceOfBirth'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['PlaceOfBirth'] ? $StudentInfo['PlaceOfBirth'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentAddressChi" id="StudentAddressChi"
                           value="<?= $StudentInfo['AddressChi'] ?>" required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['HKUGAPS']['address'] ?>(<?= $LangB5['Admission']['HKUGAC']['Chinese'] ?>) <?= $LangEn['Admission']['HKUGAPS']['address'] ?>(<?= $LangEn['Admission']['HKUGAC']['Chinese'] ?>)</div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAPS']['address'] ?>(<?= $LangB5['Admission']['HKUGAC']['Chinese'] ?>) <?= $LangEn['Admission']['HKUGAPS']['address'] ?>(<?= $LangEn['Admission']['HKUGAC']['Chinese'] ?>)</div>
				<div class="dataValue <?= $StudentInfo['AddressChi'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['AddressChi'] ? $StudentInfo['AddressChi'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentAddress" id="StudentAddress" value="<?= $StudentInfo['Address'] ?>"
                           required>
					<div class="textboxLabel requiredLabel"><?= $LangB5['Admission']['HKUGAPS']['address'] ?>(<?= $LangB5['Admission']['HKUGAC']['English'] ?>) <?= $LangEn['Admission']['HKUGAPS']['address'] ?>(<?= $LangEn['Admission']['HKUGAC']['English'] ?>)</div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAPS']['address'] ?>(<?= $LangB5['Admission']['HKUGAC']['English'] ?>) <?= $LangEn['Admission']['HKUGAPS']['address'] ?>(<?= $LangEn['Admission']['HKUGAC']['English'] ?>)</div>
				<div class="dataValue <?= $StudentInfo['Address'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['Address'] ? $StudentInfo['Address'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentContactAddressChi" id="StudentContactAddressChi"
                           value="<?= $StudentInfo['ContactAddressChi'] ?>">
					<div class="textboxLabel"><?= $LangB5['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangB5['Admission']['HKUGAC']['Chinese'] ?>) <?= $LangEn['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangEn['Admission']['HKUGAC']['Chinese'] ?>)</div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
					<div class="remark">(<?= $LangB5['Admission']['HKUGAC']['contactaddressremark'] ?> <?= $LangEn['Admission']['HKUGAC']['contactaddressremark'] ?>)</div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangB5['Admission']['HKUGAC']['Chinese'] ?>) <?= $LangEn['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangEn['Admission']['HKUGAC']['Chinese'] ?>)</div>
				<div class="dataValue <?= $StudentInfo['ContactAddressChi'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['ContactAddressChi'] ? $StudentInfo['ContactAddressChi'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentContactAddress" id="StudentContactAddress"
                           value="<?= $StudentInfo['ContactAddress'] ?>">
					<div class="textboxLabel"><?= $LangB5['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangB5['Admission']['HKUGAC']['English'] ?>) <?= $LangEn['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangEn['Admission']['HKUGAC']['English'] ?>)</div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
					<div class="remark">(<?= $LangB5['Admission']['HKUGAC']['contactaddressremark'] ?> <?= $LangEn['Admission']['HKUGAC']['contactaddressremark'] ?>)</div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangB5['Admission']['HKUGAC']['English'] ?>) <?= $LangEn['Admission']['HKUGAC']['contactaddress'] ?>(<?= $LangEn['Admission']['HKUGAC']['English'] ?>)</div>
				<div class="dataValue <?= $StudentInfo['ContactAddress'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['ContactAddress'] ? $StudentInfo['ContactAddress'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentHomePhone" id="StudentHomePhone"
                           value="<?= $StudentInfo['HomeTelNo'] ?>">
					<div class="textboxLabel"><?= $LangB5['Admission']['HKUGAC']['HomeTelephone'] ?> <?= $LangEn['Admission']['HKUGAC']['HomeTelephone'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['HomeTelephone'] ?> <?= $LangEn['Admission']['HKUGAC']['HomeTelephone'] ?></div>
				<div class="dataValue <?= $StudentInfo['HomeTelNo'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['HomeTelNo'] ? $StudentInfo['HomeTelNo'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="StudentFax" id="StudentFax" value="<?= $StudentInfo['Fax'] ?>">
					<div class="textboxLabel"><?= $LangB5['Admission']['HKUGAC']['Fax'] ?> <?= $LangEn['Admission']['HKUGAC']['Fax'] ?></div>
					<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['Fax'] ?> <?= $LangEn['Admission']['HKUGAC']['Fax'] ?></div>
				<div class="dataValue <?= $StudentInfo['Fax'] ? '' : 'dataValue-empty' ?>"><?= $StudentInfo['Fax'] ? $StudentInfo['Fax'] : '－－' ?></div>
			</span>
        </div>

        <div class="item">
            <div class="itemLabel requiredLabel"><?= $LangB5['Admission']['HKUGAC']['CurrentSchoolAttend'] ?> <?= $LangEn['Admission']['HKUGAC']['CurrentSchoolAttend'] ?></div>
            <?php for ($i = 0; $i < 1; $i++) { ?>
                <span class="itemInput">
				<div class="itemInput-complex-order"><?= $i + 1 ?></div>
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool<?= $i ?>_Year" id="StudentSchool<?= $i ?>_Year"
                               value="<?= $StudentPrevSchoolInfo[$i]['Year'] ?>" <?= $i == 0 ? 'required' : '' ?>>
						<div class="textboxLabel <?= $i == 0 ? 'requiredLabel' : '' ?>"><?= $LangB5['Admission']['year'] ?> <?= $LangEn['Admission']['year'] ?></div>
						<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentSchool<?= $i ?>_Year"></div>
						<div class="remark">(YYYY-YYYY)</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool<?= $i ?>_Class" id="StudentSchool<?= $i ?>_Class"
                               value="<?= $StudentPrevSchoolInfo[$i]['Class'] ?>" <?= $i == 0 ? 'required' : '' ?>>
						<div class="textboxLabel <?= $i == 0 ? 'requiredLabel' : '' ?>"><?= $LangB5['Admission']['HKUGAC']['Class'] ?> <?= $LangEn['Admission']['HKUGAC']['Class'] ?></div>
						<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentSchool<?= $i ?>_Class"></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" class="inputselect" name="StudentSchool<?= $i ?>_Name"
                               id="StudentSchool<?= $i ?>_Name"
                               value="<?= $StudentPrevSchoolInfo[$i]['NameOfSchool'] ?>" <?= $i == 0 ? 'required' : '' ?>>
						<div class="textboxLabel <?= $i == 0 ? 'requiredLabel' : '' ?>"><?= $LangB5['Admission']['HKUGAC']['School'] ?> <?= $LangEn['Admission']['HKUGAC']['School'] ?></div>
						<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
                <span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?= $LangB5['Admission']['year'] ?> <?= $LangEn['Admission']['year'] ?></div>
						<div class="dataValue <?= $StudentPrevSchoolInfo[$i]['Year'] ? '' : 'dataValue-empty' ?>"><?= $StudentPrevSchoolInfo[$i]['Year'] ? $StudentPrevSchoolInfo[$i]['Year'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['Class'] ?> <?= $LangEn['Admission']['HKUGAC']['Class'] ?></div>
						<div class="dataValue <?= $StudentPrevSchoolInfo[$i]['Class'] ? '' : 'dataValue-empty' ?>"><?= $StudentPrevSchoolInfo[$i]['Class'] ? $StudentPrevSchoolInfo[$i]['Class'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['School'] ?> <?= $LangEn['Admission']['HKUGAC']['School'] ?></div>
						<div class="dataValue <?= $StudentPrevSchoolInfo[$i]['NameOfSchool'] ? '' : 'dataValue-empty' ?>"><?= $StudentPrevSchoolInfo[$i]['NameOfSchool'] ? $StudentPrevSchoolInfo[$i]['NameOfSchool'] : '－－' ?></div>
					</div>
				</div>
			</span>
            <? } ?>
        </div>
        <div class="item">
            <div class="itemLabel"><?= $LangB5['Admission']['HKUGAC']['LastSchoolAttend'] ?> <?= $LangEn['Admission']['HKUGAC']['LastSchoolAttend'] ?></div>
            <?php for ($i = 1; $i < $lac::STUDENT_ACADEMIC_SCHOOL_COUNT; $i++) { ?>
                <span class="itemInput">
				<div class="itemInput-complex-order"><?= $i + 1 ?></div>
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool<?= $i ?>_Year" id="StudentSchool<?= $i ?>_Year"
                               value="<?= $StudentPrevSchoolInfo[$i]['Year'] ?>" <?= $i == 0 ? 'required' : '' ?>>
						<div class="textboxLabel <?= $i == 0 ? 'requiredLabel' : '' ?>"><?= $LangB5['Admission']['year'] ?> <?= $LangEn['Admission']['year'] ?></div>
						<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentSchool<?= $i ?>_Year"></div>
						<div class="remark">(YYYY-YYYY)</div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="StudentSchool<?= $i ?>_Class" id="StudentSchool<?= $i ?>_Class"
                               value="<?= $StudentPrevSchoolInfo[$i]['Class'] ?>" <?= $i == 0 ? 'required' : '' ?>>
						<div class="textboxLabel <?= $i == 0 ? 'requiredLabel' : '' ?>"><?= $LangB5['Admission']['HKUGAC']['Class'] ?> <?= $LangEn['Admission']['HKUGAC']['Class'] ?></div>
						<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
						<div class="remark remark-warn hide" id="errStudentSchool<?= $i ?>_Class"></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" class="inputselect" name="StudentSchool<?= $i ?>_Name"
                               id="StudentSchool<?= $i ?>_Name"
                               value="<?= $StudentPrevSchoolInfo[$i]['NameOfSchool'] ?>" <?= $i == 0 ? 'required' : '' ?>>
						<div class="textboxLabel <?= $i == 0 ? 'requiredLabel' : '' ?>"><?= $LangB5['Admission']['HKUGAC']['School'] ?> <?= $LangEn['Admission']['HKUGAC']['School'] ?></div>
						<div class="remark remark-warn hide"><?= $LangB5['Admission']['required'] ?> <?= $LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
                <span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?= $LangB5['Admission']['year'] ?> <?= $LangEn['Admission']['year'] ?></div>
						<div class="dataValue <?= $StudentPrevSchoolInfo[$i]['Year'] ? '' : 'dataValue-empty' ?>"><?= $StudentPrevSchoolInfo[$i]['Year'] ? $StudentPrevSchoolInfo[$i]['Year'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['Class'] ?> <?= $LangEn['Admission']['HKUGAC']['Class'] ?></div>
						<div class="dataValue <?= $StudentPrevSchoolInfo[$i]['Class'] ? '' : 'dataValue-empty' ?>"><?= $StudentPrevSchoolInfo[$i]['Class'] ? $StudentPrevSchoolInfo[$i]['Class'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?= $LangB5['Admission']['HKUGAC']['School'] ?> <?= $LangEn['Admission']['HKUGAC']['School'] ?></div>
						<div class="dataValue <?= $StudentPrevSchoolInfo[$i]['NameOfSchool'] ? '' : 'dataValue-empty' ?>"><?= $StudentPrevSchoolInfo[$i]['NameOfSchool'] ? $StudentPrevSchoolInfo[$i]['NameOfSchool'] : '－－' ?></div>
					</div>
				</div>
			</span>
            <? } ?>
        </div>

        <div class="remark">
            * <?= $LangB5['Admission']['requiredFields'] ?> <?= $LangEn['Admission']['requiredFields'] ?></div>

    </div>
</section>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<script>
    $(function () {
        'use strict';

        $('#LastSchool').on('blur', function (e) {
            setTimeout(function () {
                $('#LastSchool').change();
            }, 1000);
        });

        $('#studentForm .itemInput').each(function () {
            var $itemInput = $(this),
                $inputText = $itemInput.find('input[type="text"]'),
                $inputRadio = $itemInput.find('input[type="radio"]'),
                $inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
                $inputDateYearSelect = $itemInput.find('select.dateYear'),
                $inputDateMonthSelect = $itemInput.find('select.dateMonth'),
                $inputDateDaySelect = $itemInput.find('select.dateDay'),
                $dataValue = $itemInput.next('.itemData').find('.dataValue');

            $inputText.on('change', function (e) {
                $($dataValue.get($inputText.index($(this)))).html($(this).val());
                $($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
                if (!$($dataValue.get($inputText.index($(this)))).html()) {
                    $($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
                    $($dataValue.get($inputText.index($(this)))).html('－－');
                }
            });

            $inputRadio.on('change', function (e) {
                $dataValue.html($(this).next('label').html());
                $dataValue.removeClass('dataValue-empty');
                if (!$dataValue.html()) {
                    $dataValue.html('－－');
                    $dataValue.addClass('dataValue-empty');
                }
            });

            $inputDateSelect.on('change', function (e) {
                $dataValue.html($inputDateYearSelect.val() + '-' + $inputDateMonthSelect.val() + '-' + $inputDateDaySelect.val());
                $dataValue.removeClass('dataValue-empty');
                if (!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()) {
                    $dataValue.html('－－');
                    $dataValue.addClass('dataValue-empty');
                }
            });


        });

        // autocomplete for inputselect fields
        $('.inputselect').each(function () {
            var this_id = $(this).attr('id');
            if ($(this).length > 0) {
                $(this).autocomplete(
                    "../admission_form/ajax_get_suggestions.php",
                    {
                        delay: 3,
                        minChars: 1,
                        matchContains: 1,
                        extraParams: {'field': this_id},
                        autoFill: false,
                        overflow_y: 'auto',
                        overflow_x: 'hidden',
                        maxHeight: '200px'
                    }
                );
            }
        });

        function checkBirthCertNo() {
            var values = $("#form1").serialize();
            var res = null;
            /* check the birth cert number is applied or not */
            $.ajax({
                url: "../admission_form/ajax_get_birth_cert_no.php",
                type: "post",
                data: values,
                async: false,
                success: function (data) {
                    //alert("debugging: The classlevel is updated!");
                    res = data;
                },
                error: function () {
                    //alert("failure");
                    $("#result").html('There is error while submit');
                }
            });
            return res;
        }

        /*
        hkid format:  A123456(7)
        A1234567
        AB123456(7)
        AB1234567
        */
        function check_hkid(hkid) {
            var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
            var ra = re.exec(hkid);

            if (ra != null) {
                var p1 = ra[1];
                var p2 = ra[2];
                var p3 = ra[4];
                var check_sum = 0;
                if (p1.length == 2) {
                    check_sum = (p1.charCodeAt(0) - 55) * 9 + (p1.charCodeAt(1) - 55) * 8;
                } else if (p1.length == 1) {
                    check_sum = 324 + (p1.charCodeAt(0) - 55) * 8;
                }

                check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
                var check_digit = 11 - (check_sum % 11);
                if (check_digit == '11') {
                    check_digit = 0;
                } else if (check_digit == '10') {
                    check_digit = 'A';
                }
                if (check_digit == p3) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        window.checkStudentForm = (function (lifecycle) {
            var isValid = true;
            var $studentForm = $('#studentForm');

            /******** Basic init START ********/
                //for YYYY validation
            var re = /^\d{4}-\d{4}$/;
            /******** Basic init END ********/

            /**** Check required START ****/
            isValid = isValid && checkInputRequired($studentForm);

            if (
                ($('#StudentDateOfBirthYear').val() == '') ||
                ($('#StudentDateOfBirthMonth').val() == '') ||
                ($('#StudentDateOfBirthDay').val() == '')
            ) {
                $('#errDobRequired').removeClass('hide');
                $('#StudentDateOfBirthYear').focus();
                isValid = false;
            }

            if (
                (
                    '<?=$application_setting['DOBStart']?>' != '' &&
                    $('#StudentDateOfBirthYear').val() + '-' + $('#StudentDateOfBirthMonth').val() + '-' + $('#StudentDateOfBirthDay').val() < '<?=$application_setting['DOBStart']?>'
                ) || (
                    '<?=$application_setting['DOBEnd']?>' != '' &&
                    $('#StudentDateOfBirthYear').val() + '-' + $('#StudentDateOfBirthMonth').val() + '-' + $('#StudentDateOfBirthDay').val() > '<?=$application_setting['DOBEnd']?>'
                )
            ) {
                if ($('#errDobRequired').hasClass('hide')) {
                    $('#errDobRange').removeClass('hide');
                    focusElement = $('#StudentDateOfBirthYear');
                }
                isValid = false;
            }

            if (!window.isUpdatePeriod && $('#StudentBirthCertNo').val().trim() != '' && (!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test($('#StudentBirthCertNo').val()) || !check_hkid($('#StudentBirthCertNo').val()))) {
                if ($('#StudentBirthCertNo').parent().find('.remark-warn').hasClass('hide')) {
                    $('#errStudentBirthCertNo').html('<?=$LangB5['Admission']['UCCKE']['msg']['invalidHKID']?> <?=$LangEn['Admission']['UCCKE']['msg']['invalidHKID']?>');
                    $('#errStudentBirthCertNo').removeClass('hide');
                    focusElement = $('#StudentBirthCertNo');
                }
                isValid = false;
            }

            if (!window.isUpdatePeriod && $('#StudentBirthCertNo').val().trim() != '' && checkBirthCertNo() > 0) {
                if ($('#StudentBirthCertNo').parent().find('.remark-warn').hasClass('hide')) {
                    $('#errStudentBirthCertNo').html('<?=$LangB5['Admission']['UCCKE']['msg']['duplicateHKID']?> <?=$LangEn['Admission']['UCCKE']['msg']['duplicateHKID']?>');
                    $('#errStudentBirthCertNo').removeClass('hide');
                    focusElement = $('#StudentBirthCertNo');
                }
                isValid = false;
            }

            if ($('#StudentSchool0_Year').val().trim() != '' && !re.test($('#StudentSchool0_Year').val().trim())) {
                if ($('#StudentSchool0_Year').parent().find('.remark-warn').hasClass('hide')) {
                    $('#errStudentSchool0_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
                    $('#errStudentSchool0_Year').removeClass('hide');
                    focusElement = $('#StudentSchool0_Year');
                }
                isValid = false;
            }

            if ($('#StudentSchool1_Year').val().trim() != '' && !re.test($('#StudentSchool1_Year').val().trim())) {
                if ($('#StudentSchool1_Year').parent().find('.remark-warn').hasClass('hide')) {
                    $('#errStudentSchool1_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
                    $('#errStudentSchool1_Year').removeClass('hide');
                    focusElement = $('#StudentSchool1_Year');
                }
                isValid = false;
            }

            if ($('#StudentSchool2_Year').val().trim() != '' && !re.test($('#StudentSchool2_Year').val().trim())) {
                if ($('#StudentSchool2_Year').parent().find('.remark-warn').hasClass('hide')) {
                    $('#errStudentSchool2_Year').html('<?=$Lang['Admission']['HKUGAC']['msg']['YearFormatRemark']?>');
                    $('#errStudentSchool2_Year').removeClass('hide');
                    focusElement = $('#StudentSchool2_Year');
                }
                isValid = false;
            }


            /**** Check required END ****/

            if (focusElement) {
                focusElement.focus();
            }

            return isValid;
        });

        window.validateFunc['pagePersonalInfo'].push(checkStudentForm);
    });
</script>