<section id="parentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation" style="display: none">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['HKUGAC']['PGInfo'] ?> <?=$LangEn['Admission']['HKUGAC']['PGInfo'] ?>
	</div>
	<div style="padding-left:20px">* <?=$LangB5['Admission']['HKUGAC']['msg']['NARemark'] ?><br/>* <?=$LangEn['Admission']['HKUGAC']['msg']['NARemark'] ?></div>
	<div class="sheet">
	<?php
		foreach($parentTypeArr as $index => $parentType){
	?><div class="item item-In3">
			<div class="text-inst">
				<?=$LangB5['Admission']['HKUGAC']['ParentInformation'][$parentType] ?> <?=$LangEn['Admission']['HKUGAC']['ParentInformation'][$parentType] ?>
			</div>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>EnglishName" id="G<?=$index?>EnglishName" value="<?=$ParentInfo[$parentType]['EnglishName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['englishname'] ?> <?=$LangEn['Admission']['HKUGAC']['englishname'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['englishname'] ?> <?=$LangEn['Admission']['HKUGAC']['englishname'] ?></div>
				<div class="dataValue <?=$ParentInfo[$parentType]['EnglishName'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo[$parentType]['EnglishName'] ? $ParentInfo[$parentType]['EnglishName'] : '－－'?></div>
			</span>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>ChineseName" id="G<?=$index?>ChineseName" value="<?=$ParentInfo[$parentType]['ChineseName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAPS']['chinesename'] ?> <?=$LangEn['Admission']['HKUGAPS']['chinesename'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAPS']['chinesename'] ?> <?=$LangEn['Admission']['HKUGAPS']['chinesename'] ?></div>
				<div class="dataValue <?=$ParentInfo[$parentType]['ChineseName'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo[$parentType]['ChineseName'] ? $ParentInfo[$parentType]['ChineseName'] : '－－'?></div>
			</span>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>JobTitle" id="G<?=$index?>JobTitle" value="<?=$ParentInfo[$parentType]['JobTitle'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['Occupation'] ?> <?=$LangEn['Admission']['HKUGAC']['Occupation'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Occupation'] ?> <?=$LangEn['Admission']['HKUGAC']['Occupation'] ?></div>
				<div class="dataValue <?=$ParentInfo[$parentType]['JobTitle'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo[$parentType]['JobTitle'] ? $ParentInfo[$parentType]['JobTitle'] : '－－'?></div>
			</span>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>Company" id="G<?=$index?>Company" value="<?=$ParentInfo[$parentType]['Company'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['Company'] ?> <?=$LangEn['Admission']['HKUGAC']['Company'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Company'] ?> <?=$LangEn['Admission']['HKUGAC']['Company'] ?></div>
				<div class="dataValue <?=$ParentInfo[$parentType]['Company'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo[$parentType]['Company'] ? $ParentInfo[$parentType]['Company'] : '－－'?></div>
			</span>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>OfficeTelNo" id="G<?=$index?>OfficeTelNo" value="<?=$ParentInfo[$parentType]['OfficeTelNo'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangB5['Admission']['HKUGAC']['Day']?>) <?=$LangEn['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangEn['Admission']['HKUGAC']['Day']?>)</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangB5['Admission']['HKUGAC']['Day']?>) <?=$LangEn['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangEn['Admission']['HKUGAC']['Day']?>)</div>
				<div class="dataValue <?=$ParentInfo[$parentType]['OfficeTelNo'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo[$parentType]['OfficeTelNo'] ? $ParentInfo[$parentType]['OfficeTelNo'] : '－－'?></div>
			</span>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>NightTelephone" id="G<?=$index?>NightTelephone" value="<?=$allCustInfo["G{$index}NightTelephone"][0]['Value'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangB5['Admission']['HKUGAC']['Night']?>) <?=$LangEn['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangEn['Admission']['HKUGAC']['Night']?>)</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangB5['Admission']['HKUGAC']['Night']?>) <?=$LangEn['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangEn['Admission']['HKUGAC']['Night']?>)</div>
				<div class="dataValue <?=$allCustInfo["G{$index}NightTelephone"][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo["G{$index}NightTelephone"][0]['Value'] ? $allCustInfo["G{$index}NightTelephone"][0]['Value'] : '－－'?></div>
			</span>

			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G<?=$index?>Mobile" id="G<?=$index?>Mobile" value="<?=$ParentInfo[$parentType]['Mobile'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangB5['Admission']['HKUGAC']['Mobile']?>) <?=$LangEn['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangEn['Admission']['HKUGAC']['Mobile']?>)</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangB5['Admission']['HKUGAC']['Mobile']?>) <?=$LangEn['Admission']['HKUGAC']['Telephone'] ?>(<?=$LangEn['Admission']['HKUGAC']['Mobile']?>)</div>
				<div class="dataValue <?=$ParentInfo[$parentType]['Mobile'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo[$parentType]['Mobile'] ? $ParentInfo[$parentType]['Mobile'] : '－－'?></div>
			</span>
		</div><?php }?>

		<div class="item">
			<div class="itemLabel requiredLabel"><?=$LangB5['Admission']['contactperson'] ?> <?=$LangEn['Admission']['contactperson'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="ContactPerson_Father" name="ContactPerson" value="Father" required <?=$allCustInfo['ContactPerson'][0]['Value'] == "Father"?'checked':'' ?>>
					<label for="ContactPerson_Father"><?=$LangB5['Admission']['HKUGAC']['father'] ?> <?=$LangEn['Admission']['HKUGAC']['father'] ?></label>
				</span>
				<span>
					<input type="radio" id="ContactPerson_Mother" name="ContactPerson" value="Mother" required <?=$allCustInfo['ContactPerson'][0]['Value'] == "Mother"?'checked':'' ?>>
					<label for="ContactPerson_Mother"><?=$LangB5['Admission']['HKUGAC']['mother'] ?> <?=$LangEn['Admission']['HKUGAC']['mother'] ?></label>
				</span>
				<span>
					<input type="radio" id="ContactPerson_Guardian" name="ContactPerson" value="Guardian" required <?=$allCustInfo['ContactPerson'][0]['Value'] == "Guardian"?'checked':'' ?>>
					<label for="ContactPerson_Guardian"><?=$LangB5['Admission']['HKUGAC']['guardian'] ?> <?=$LangEn['Admission']['HKUGAC']['guardian'] ?></label>
    				<div class="remark remark-warn hide"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue <?=$allCustInfo['ContactPerson'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['ContactPerson'][0]['Value'] == "Father"?$LangB5['Admission']['HKUGAC']['father'].' '.$LangEn['Admission']['HKUGAC']['father']:($allCustInfo['ContactPerson'][0]['Value'] == "Mother" ? $LangB5['Admission']['HKUGAC']['mother'].' '.$LangEn['Admission']['HKUGAC']['mother'] : ($allCustInfo['ContactPerson'][0]['Value'] == "Guardian" ? $LangB5['Admission']['HKUGAC']['guardian'].' '.$LangEn['Admission']['HKUGAC']['guardian'] : '－－'))?></div>
			</div>
		</div>

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="G1Email" id="G1Email" value="<?=$ParentInfo['F']['email'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['EmailAddress'] ?> <?=$LangEn['Admission']['HKUGAC']['EmailAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id='errG1Email'></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['EmailAddress'] ?> <?=$LangEn['Admission']['HKUGAC']['EmailAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['F']['email'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['F']['email'] ? $ParentInfo['F']['email'] : '－－'?></div>
			</span>
		</div>

        <div class="item">
            <div class="itemLabel requiredLabel"><?=$LangB5['Admission']['HKUGAC']['IsTwinsApplied'] ?><br/><?=$LangEn['Admission']['HKUGAC']['IsTwinsApplied'] ?></div>
            <span class="itemInput">
				<div class="icon icon-button fa-trash button-deleteComplex hide"></div>
				<div class="itemInput-complex">
					<div class="textbox-floatlabel textbox-In3">
						<span class="itemInput-choice">
							<span>
								<input type="radio" id="IsTwinsApplied_Y" name="IsTwinsApplied" value="Y" required <?=$allCustInfo['IsTwinsApplied'][0]['Value'] == "Y"?'checked':'' ?>>
								<label for="IsTwinsApplied_Y"><?=$LangB5['Admission']['yes'] ?> <?=$LangEn['Admission']['yes'] ?></label>
							</span>
							<span>
								<input type="radio" id="IsTwinsApplied_N" name="IsTwinsApplied" value="N" required <?=!$allCustInfo['IsTwinsApplied'][0]['Value'] || $allCustInfo['IsTwinsApplied'][0]['Value'] == "N"?'checked':'' ?>>
								<label for="IsTwinsApplied_N"><?=$LangB5['Admission']['no'] ?> <?=$LangEn['Admission']['no'] ?></label>
			    				<div class="remark remark-warn hide"><?=$LangB5['Admission']['pleaseSelect'] ?> <?=$LangEn['Admission']['pleaseSelect'] ?></div>
							</span>
						</span>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="TwinsAppliedName" id="TwinsAppliedName" value="<?=$allCustInfo['TwinsAppliedName'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['name'] ?> <?=$LangEn['Admission']['name'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
					<div class="textbox-floatlabel textbox-In3">
						<input type="text" name="TwinsAppliedSTRN" id="TwinsAppliedSTRN" value="<?=$allCustInfo['TwinsAppliedSTRN'][0]['Value'] ?>">
						<div class="textboxLabel"><?=$LangB5['Admission']['HKUGAC']['strn'] ?> <?=$LangEn['Admission']['HKUGAC']['strn'] ?></div>
						<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					</div>
				</div>
			</span>
            <span class="itemData">
				<div class="itemData-complex">
					<div class="itemData-complex-In3">
						<div class="dataValue"><?=$Lang['Admission']['no']?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$LangB5['Admission']['name'] ?> <?=$LangEn['Admission']['name'] ?></div>
						<div class="dataValue <?=$allCustInfo['TwinsAppliedName'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['TwinsAppliedName'][0]['Value'] ? $allCustInfo['TwinsAppliedName'][0]['Value'] : '－－' ?></div>
					</div>
					<div class="itemData-complex-In3">
						<div class="dataLabel"><?=$LangB5['Admission']['HKUGAC']['strn'] ?> <?=$LangEn['Admission']['HKUGAC']['strn'] ?></div>
						<div class="dataValue <?=$allCustInfo['TwinsAppliedSTRN'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['TwinsAppliedSTRN'][0]['Value'] ? $allCustInfo['TwinsAppliedSTRN'][0]['Value'] : '－－' ?></div>
					</div>
				</div>
			</span>
        </div>

		<div class="remark">* <?=$LangB5['Admission']['requiredFields'] ?> <?=$LangEn['Admission']['requiredFields'] ?></div>
	</div>
</section>

<script>
$(function(){
	'use strict';

	$('#parentForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		/*$inputText.on('change', function(e){
			$($dataValue.get($inputText.index($(this)))).html($(this).val());
			$($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
			if(!$($dataValue.get($inputText.index($(this)))).html()){
				$($dataValue.get($inputText.index($(this)))).html('－－');
				$($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
			}
		});*/
        $inputText.on('change', function(e){
            $($dataValue.get($(this).parent('.textbox-floatlabel').index())).html($(this).val());
            $($dataValue.get($(this).parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
            if(!$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html()){
                $($dataValue.get($(this).parent('.textbox-floatlabel').index())).html('－－');
                $($dataValue.get($(this).parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
            }
        });

		/*$inputRadio.on('change', function(e){
			$dataValue.html($inputRadio.next('label').html());
			$dataValue.removeClass('dataValue-empty');
			if(!$dataValue.html()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});*/
        $inputRadio.on('change', function(e){
            $($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html($(this).next('label').html());
            $($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
            if(!$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html()){
                $($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html('－－');
                $($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
            }
        });
	});

	window.checkParentForm = (function(lifecycle){
		var isValid = true;
		var $parentForm = $('#parentForm');

		/******** Basic init START ********/
		//for email validation
		var re = /\S+@\S+\.\S+/;
		/******** Basic init END ********/

		/**** Check required START ****/
		isValid = isValid && checkInputRequired($parentForm);

		if($('#G1Email').val().trim()!='' && !re.test($('#G1Email').val())){
			if($('#G1Email').parent().find('.remark-warn').hasClass('hide')){
				$('#errG1Email').html('<?=$LangB5['Admission']['icms']['msg']['invalidmailaddress']?> <?=$LangEn['Admission']['icms']['msg']['invalidmailaddress']?>');
				$('#errG1Email').removeClass('hide');
				focusElement = $('#G1Email');
			}
	    	isValid = false;
		}

//		if($('#ParentHomeTelNo').val().trim()!='' &&  !/^[0-9]{8}$/.test($('#ParentHomeTelNo').val())){
//			if($('#ParentHomeTelNo').parent().find('.remark-warn').hasClass('hide')){
//				$('#errParentHomeTelNo').html('<?=$LangB5['Admission']['msg']['invalidhomephoneformat']?> <?=$LangEn['Admission']['msg']['invalidhomephoneformat']?>');
//				$('#errParentHomeTelNo').removeClass('hide');
//				focusElement = $('#ParentHomeTelNo');
//			}	
//	    	isValid = false;
//		}
//		
//		if($('#ParentMobileNo').val().trim()!='' &&  !/^[0-9]{8}$/.test($('#ParentMobileNo').val())){
//			if($('#ParentMobileNo').parent().find('.remark-warn').hasClass('hide')){
//				$('#errParentMobileNo').html('<?=$LangB5['Admission']['msg']['invalidmobilephoneformat']?> <?=$LangEn['Admission']['msg']['invalidmobilephoneformat']?>');
//				$('#errParentMobileNo').removeClass('hide');
//				focusElement = $('#ParentMobileNo');
//			}	
//	    	isValid = false;
//		}

        if($('#IsTwinsApplied_Y').prop("checked") && $('#TwinsAppliedName').val() == ''){
            $('#TwinsAppliedName').parent().find('.remark-warn').removeClass('hide');
            focusElement = $('#TwinsAppliedName');
            isValid = false;
        }

        if($('#IsTwinsApplied_Y').prop("checked") && $('#TwinsAppliedSTRN').val() == ''){
            $('#TwinsAppliedSTRN').parent().find('.remark-warn').removeClass('hide');
            focusElement = $('#TwinsAppliedSTRN');
            isValid = false;
        }
        if($('#IsTwinsApplied_N').prop("checked")){
            $('#TwinsAppliedName, #TwinsAppliedSTRN').val('').change();
        }
		/**** Check required END ****/

		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkParentForm);
});
</script>