<?php
global $libkis_admission;

$allCustInfo = $libkis_admission->getAllApplicationCustInfo($kis_data['applicationID']);

?>
<style>
    .form_guardian_head, .form_guardian_field {
        text-align: center !important;
    }
</style>
<table class="form_table">
    <colgroup>
        <col style="width: 30%">
        <col style="width: 35%">
        <col style="width: 35%">
    </colgroup>
    <tr>
        <td>&nbsp;</td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAC']['P5FirstResult'] ?><br/>
				<?= $kis_lang['Admission']['HKUGAC']['P6FinalResult'] ?><br/>
				<?= $kis_lang['Admission']['HKUGAC']['S1FinalResult'] ?><br/>
				<?= $kis_lang['Admission']['HKUGAC']['S2FinalResult'] ?></center>
        </td>
        <td class="form_guardian_head">
            <center><?= $kis_lang['Admission']['HKUGAC']['P5FinalResult'] ?><br/>
				<?= $kis_lang['Admission']['HKUGAC']['S1FirstResult'] ?><br/>
				<?= $kis_lang['Admission']['HKUGAC']['S2FirstResult'] ?><br/>
				<?= $kis_lang['Admission']['HKUGAC']['S3FirstResult'] ?></center>
        </td>
    </tr>

    <?php foreach ($admission_cfg['Subjects'] as $subjectCode): ?>
        <tr>
            <td class="field_title">
                <?= $kis_lang['Admission']['HKUGAC']['Subjects'][$subjectCode] ?>
            </td>
            <td class="form_guardian_field">
                <input class="textboxtext" name="F5_Subject_<?= $subjectCode ?>" id="F5_Subject_<?= $subjectCode ?>"
                       value="<?= $allCustInfo["F5_Subject_{$subjectCode}"][0]['Value'] ?>">
            </td>
            <td class="form_guardian_field">
                <input class="textboxtext" name="F6_Subject_<?= $subjectCode ?>" id="F6_Subject_<?= $subjectCode ?>"
                       value="<?= $allCustInfo["F6_Subject_{$subjectCode}"][0]['Value'] ?>">
            </td>
        </tr>
    <?php endforeach; ?>

</table>