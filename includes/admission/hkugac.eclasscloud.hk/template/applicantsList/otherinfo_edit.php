<?php
global $libkis_admission;

$RelativeInfo = $libkis_admission->getApplicationRelativesInfo($schoolYearID, '', $applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>
<style>
.whereToKnowOptionDiv {
	margin-bottom: 5px;
}

.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>

<table class="form_table">
    <colgroup>
        <col style="width:40%">
        <col style="width:30%">
        <col style="width:30%">
    </colgroup>
    <tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['year'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['OtherLearningExperience'] ?></td>
	</tr>
	<?php for($i=0;$i<$libkis_admission::LEARNING_EXPERIENCE_COUNT;$i++){ ?>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['HKUGAC']['LearningExperience'] ?> (<?=$i+1?>)
		</td>
		<td>
			<input name="StudentExperience<?=$i?>_Year" type="text"
					id="StudentExperience<?=$i?>_Year" class="textboxtext"
					value="<?=$allCustInfo['StudentExperience'.$i.'_Year'][0]['Value']?>" />
		</td>
		<td>
			<input name="StudentExperience<?=$i?>_Name" type="text"
					id="StudentExperience<?=$i?>_Name" class="textboxtext"
					value="<?=$allCustInfo['StudentExperience'.$i.'_Name'][0]['Value']?>" />
		</td>
	</tr>
	<? }?>
</table>

<table class="form_table" style="font-size: 13px">
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAC']['WhyChooseSchool']?>
    	</td>
		<td>
			<textarea name="WhyChooseSchool" type="text"
				id="WhyChooseSchool" class="textboxtext" style="height: 100px; resize: vertical;"
			><?=$allCustInfo['WhyChooseSchool'][0]['Value']?></textarea>
    	</td>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAC']['ApplicantStrengthsWeaknesses']?>
    	</td>
		<td>
			<textarea name="ApplicantStrengthsWeaknesses" type="text"
				id="ApplicantStrengthsWeaknesses" class="textboxtext" style="height: 100px; resize: vertical;"
			><?=$allCustInfo['ApplicantStrengthsWeaknesses'][0]['Value']?></textarea>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAC']['IsParentsHKUGAMember']?>
    	</td>
		<td>
			<?=$libinterface->Get_Radio_Button('IsParentsHKUGAMember_Y', 'IsParentsHKUGAMember', 'Y', ($allCustInfo['IsParentsHKUGAMember'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    		<?=$libinterface->Get_Radio_Button('IsParentsHKUGAMember_N', 'IsParentsHKUGAMember', 'N', ($allCustInfo['IsParentsHKUGAMember'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			<br/><?=$kis_lang['Admission']['HKUGAC']['MemberName']?>: <input name="ParentsHKUGAMemberName" type="text"
				id="ParentsHKUGAMemberName" class="textboxtext"
				value="<?=$allCustInfo['ParentsHKUGAMemberName'][0]['Value']?>" />
			<br/><?=$kis_lang['Admission']['HKUGAC']['MembershipNo']?>: <input name="ParentsHKUGAMemberNumber" type="text"
				id="ParentsHKUGAMemberNumber" class="textboxtext"
				value="<?=$allCustInfo['ParentsHKUGAMemberNumber'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['HKUGAC']['IsParentsHKUGAEFMember']?>
    	</td>
		<td>
			<?=$libinterface->Get_Radio_Button('IsParentsHKUGAEFMember_Y', 'IsParentsHKUGAEFMember', 'Y', ($allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    		<?=$libinterface->Get_Radio_Button('IsParentsHKUGAEFMember_N', 'IsParentsHKUGAEFMember', 'N', ($allCustInfo['IsParentsHKUGAEFMember'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			<br/><?=$kis_lang['Admission']['HKUGAC']['MemberName']?>: <input name="ParentsHKUGAEFMemberName" type="text"
				id="ParentsHKUGAEFMemberName" class="textboxtext"
				value="<?=$allCustInfo['ParentsHKUGAEFMemberName'][0]['Value']?>" />
			<br/><?=$kis_lang['Admission']['HKUGAC']['MembershipNo']?>: <input name="ParentsHKUGAEFMemberNumber" type="text"
				id="ParentsHKUGAEFMemberNumber" class="textboxtext"
				value="<?=$allCustInfo['ParentsHKUGAEFMemberNumber'][0]['Value']?>" />
    	</td>
	</tr>
</table>	
	
<table class="form_table">
    <colgroup>
        <col style="width:20%">
        <col style="width:20%">
        <col style="width:20%">
        <col style="width:20%">
        <col style="width:20%">
    </colgroup>
    <tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['name'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['year'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['Class'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['HKUGAC']['Relationship'] ?></td>
	</tr>
	<?php for($i=0;$i<$libkis_admission::RELATIVES_COUNT;$i++){ ?>
	<tr>
		<td class="field_title">
			<?=$kis_lang['Admission']['HKUGAC']['SiblingInformation'] ?> (<?=$i+1?>)
		</td>
		<td>
			<input id="Relative<?=$i?>_Name" name="Relative<?=$i?>_Name"
				class="textboxtext" value="<?=$RelativeInfo[$i]['Name'] ?>" />
		</td>
		<td>
			<input id="Relative<?=$i?>_Year" name="Relative<?=$i?>_Year"
				class="textboxtext" value="<?=$RelativeInfo[$i]['Year'] ?>" />
		</td>
		<td>
			<input id="Relative<?=$i?>_Class" name="Relative<?=$i?>_Class"
				class="textboxtext" value="<?=$RelativeInfo[$i]['ClassPosition'] ?>" />
		</td>
		<td>
			<input id="Relative<?=$i?>_Relationship" name="Relative<?=$i?>_Relationship"
				class="textboxtext" value="<?=$RelativeInfo[$i]['Relationship'] ?>" />
		</td>
	</tr>
	<? }?>
</table>	

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsMMYY(str){
	return str.match(/^(0[123456789]|10|11|12)\/\d{2}$/);
}

function checkValidForm(){
	return true;
	if(
		$('#SpeechTherapyDate').val().trim()!='' &&
		!checkIsMMYY($('#SpeechTherapyDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#SpeechTherapyDate').focus();
		return false;
	}
	if(
		$('#OccupationalTherapyDate').val().trim()!='' &&
		!checkIsMMYY($('#OccupationalTherapyDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#OccupationalTherapyDate').focus();
		return false;
	}
	if(
		$('#PsychoEducationalAssessmentDate').val().trim()!='' &&
		!checkIsMMYY($('#PsychoEducationalAssessmentDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#PsychoEducationalAssessmentDate').focus();
		return false;
	}
	if(
		$('#PsychologicalAssessmentCounsellingDate').val().trim()!='' &&
		!checkIsMMYY($('#PsychologicalAssessmentCounsellingDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#PsychologicalAssessmentCounsellingDate').focus();
		return false;
	}
	if(
		$('#RemedialInstructionTutoringDate').val().trim()!='' &&
		!checkIsMMYY($('#RemedialInstructionTutoringDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#RemedialInstructionTutoringDate').focus();
		return false;
	}
	if(
		$('#IndividualizedProgramPlanDate').val().trim()!='' &&
		!checkIsMMYY($('#IndividualizedProgramPlanDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#IndividualizedProgramPlanDate').focus();
		return false;
	}
	if(
		$('#OtherFormalAssessmentDate').val().trim()!='' &&
		!checkIsMMYY($('#OtherFormalAssessmentDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#OtherFormalAssessmentDate').focus();
		return false;
	}
	return true;
}

</script>