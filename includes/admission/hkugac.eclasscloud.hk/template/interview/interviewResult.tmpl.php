<?php


######## Interview Announcement START ########
$viewInterviewArr = array(
    1 => ($basicSettings['firstInterviewAnnouncement'] > 0),
    2 => ($basicSettings['secondInterviewAnnouncement'] > 0),
    3 => ($basicSettings['thirdInterviewAnnouncement'] > 0),
);
$hasInterviewArr = array(
    1 => !!($result['Date']),
    2 => !!($result2['Date']),
    3 => !!($result3['Date']),
);

$templateIdArr = array();
for ($i = 1; $i <= 3; $i++) {
    foreach ($emailTemplates as $emailTemplate) {
        $templateClassLevels = explode(',', $emailTemplate['TemplateClassLevel']);
        if (!in_array($classLevelId, $templateClassLevels)) {
            continue;
        }

        if ($hasInterviewArr[$i]) {
            if (
                !isset($templateIdArr[$i]['b5']) &&
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview"]['b5']
            ) {
                $templateIdArr[$i]['b5'] = $emailTemplate['TemplateID'];
            } elseif (
                !isset($templateIdArr[$i]['en']) &&
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview"]['en']
            ) {
                $templateIdArr[$i]['en'] = $emailTemplate['TemplateID'];
            }
        } else {
            if (
                !isset($templateIdArr[$i]['b5']) &&
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview_CannotArrange"]['b5']
            ) {
                $templateIdArr[$i]['b5'] = $emailTemplate['TemplateID'];
            } elseif (
                !isset($templateIdArr[$i]['en']) &&
                $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']["Round{$i}_Interview_CannotArrange"]['en']
            ) {
                $templateIdArr[$i]['en'] = $emailTemplate['TemplateID'];
            }
        }
    }
}
######## Interview Announcement END ########


######## Application Status START ########
$viewApplicationStatus = ($basicSettings['applicationStatusAnnouncement'] > 0);
$applicationAdmitted = ($applicationStatus == $admission_cfg['Status']['confirmed']);
$applicationNotAdmitted = ($applicationStatus == $admission_cfg['Status']['HKUGAC_notadmitted']);

$zhApplicationStatusTemplateId = 0;
$enApplicationStatusTemplateId = 0;
foreach ($emailTemplates as $emailTemplate) {
    $templateClassLevels = explode(',', $emailTemplate['TemplateClassLevel']);
    if (!in_array($classLevelId, $templateClassLevels)) {
        continue;
    }

    if ($applicationAdmitted) {
        if (
            !$zhApplicationStatusTemplateId &&
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['Admitted']['b5']
        ) {
            $zhApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        } elseif (
            !$enApplicationStatusTemplateId &&
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['Admitted']['en']
        ) {
            $enApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        }
    } elseif ($applicationNotAdmitted) {
        if (
            !$zhApplicationStatusTemplateId &&
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['NotAdmitted']['b5']
        ) {
            $zhApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        } elseif (
            !$enApplicationStatusTemplateId &&
            $emailTemplate['TemplateType'] == $admission_cfg['EmailTemplateType']['NotAdmitted']['en']
        ) {
            $enApplicationStatusTemplateId = $emailTemplate['TemplateID'];
        }
    }
}
######## Application Status END ########

?>
    <style>
        .itemData table tr {
            line-height: 2rem;
        }
    </style>
<?php if ($viewInterviewArr[1]): ?>
    <div class="item">
        <div class="itemLabel">
            第一輪面試 First Round Interview
        </div>
        <div class="itemData">
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[1]['b5'], '') ?>">
                按此查看 (中文版本)
            </a>
            <br />
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[1]['en'], '') ?>">
                Click to view (English Version)
            </a>
        </div>
    </div>
<?php endif; ?>

<?php if ($viewInterviewArr[2] && $hasInterviewArr[1]): ?>
    <div class="item">
        <div class="itemLabel">
            第二輪面試 Second Round Interview
        </div>
        <div class="itemData">
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[2]['b5'], '') ?>">
                按此查看 (中文版本)
            </a>
            <br />
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[2]['en'], '') ?>">
                Click to view (English Version)
            </a>
        </div>
    </div>
<?php endif; ?>

<?php if ($viewInterviewArr[3] && $hasInterviewArr[2]): ?>
    <div class="item">
        <div class="itemLabel">
            第三輪面試 Third Round Interview
        </div>
        <div class="itemData">
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[3]['b5'], '') ?>">
                按此查看 (中文版本)
            </a>
            <br />
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result['OthersInfoRecordID'], $templateIdArr[3]['en'], '') ?>">
                Click to view (English Version)
            </a>
        </div>
    </div>
<?php endif; ?>



<?php
if ($viewApplicationStatus && ($applicationAdmitted || $applicationNotAdmitted)): ?>
    <div class="item">
        <div class="itemLabel">
            取錄結果 Application Result
        </div>
        <div class="itemData">
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result3['OthersInfoRecordID'], $zhApplicationStatusTemplateId, '') ?>">
                按此查看 (中文版本)
            </a>
            <br/>
            <a target="_blank"
               href="<?= $libkis_admission->getPrintLink('', $result3['OthersInfoRecordID'], $enApplicationStatusTemplateId, '') ?>">
                Click to view (English Version)
            </a>
        </div>
    </div>

<?php endif; ?>