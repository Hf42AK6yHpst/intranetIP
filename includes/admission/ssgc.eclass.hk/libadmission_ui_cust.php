<?php
# modifying by: Pun

/********************
 * Log :
 * Date		2017-08-24 [Pun]
 * 			Modified getUpdateIndexContent(), fixed lang
 * Date		2017-08-15 [Pun]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step, $ApplicationID=''){
		global $Lang, $sys_custom,$lac, $validForAdmission, $kis_lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token'] && $Step!=7){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'<br/>This is preview form. Any information will not be submit.</center></h2>';
		}
		if($lac->isInternalUse($_REQUEST['token']) && ($Step!=7 || $lac->getTokenByApplicationNumber($ApplicationID)=='')){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}
				
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].' Instruction</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].' Personal Info</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].' Docs Upload</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>'.$Lang['Admission']['confirmation'].' Confirmation</a></div>
					<!--<div class="'.$active_step7.'"><a><span>5</span>'.$Lang['Admission']['submission'].' Submission</a></div>-->
					<!--<div><a><span>6</span>'.$Lang['Admission']['payment'].' Payment</a></div>-->
					<div class="'.$active_step7.' last_step"><span style="width:90px">5 '.$Lang['Admission']['finish'].' Finish</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].'<br/>Welcome to our online application page!'; //Henry 20131107
		}
		
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && !$_GET['token']){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultpreviewpagemessage'] .'<br/>This is preview form. Any information will not be submit.</center></h2>';
		}
		if($lac->isInternalUse($_GET['token'])){
			$previewnote .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm($ClassLevel);
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac, $sys_custom;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'].'<br/>Welcome to our online application page!'; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
		$HKUGAPS_cust = '<br /><br /><table style="font-size:small;">
						<tr>
							<td width="5">&nbsp;</td>
							<td align="center" width="1%" rowspan="2"><input type="checkbox" name="Agree" id="Agree" value="1" style="transform: scale(2);  -webkit-transform: scale(2);"/></td>
							<td width="5">&nbsp;</td>
							<td><label for="Agree">本人同意上述有關條款及細則。</label></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><label for="Agree">I agree the terms and conditions as stated above.</label></td>
						</tr>
					</table>';
					
         $x .= $Instruction.$HKUGAPS_cust;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_instruction','step_index')", "backToIndex", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN('開始填寫表格 Begin Application', "button", "goto('step_instruction','step_input_form')", "goToForm", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->';								
							if(!$lac->isInternalUse($_REQUEST['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
							}
							else{
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
							}
						$x .= '</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm($ClassLevel = ''){
		global $libkis_admission, $Lang, $lac, $sys_custom;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		/*
		//disable to choose class when using central server
		$disable = '';
		if($ClassLevel)
			$disable = 'return false';
		*/
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			//$class_level_selection .= '<p>英語、粵語及普通話&nbsp;&nbsp;&nbsp;English, Cantonese and Mandarin</p>';
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate'] || ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate'])){
					$hasClassLevelApply = 1;
					$selected = '';
					
					if($key == $ClassLevel)
						$selected = "checked='checked'";
						
					
					//Henry added [20140808]
					$numOFQuotaLeft = $lac->NumOfQuotaLeft($key,$value['SchoolYearID']);
					$isFullApply = false;
					if($numOFQuotaLeft <= 0){
						$isFullApply = true;
					}
					$disable = '';
					if($isFullApply){
						$selected = 'disabled';
					}
					
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="'.$disable.'" '.$selected.'  />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].($isFullApply?' <span style="color:red">('.$Lang['Admission']['IsFull'].' Full)</span>':'').'<!--(Quota Left:'.$numOFQuotaLeft.')-->'.'</label> ';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'<br/>Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].' Warning</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'<br/>Application is not yet started / There is no class level for online application.<br/>If you have any enquiries please contact us.</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025
		//$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .='<fieldset ><legend>'.$Lang['Admission']['level'].' Level</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_index','step_instruction')", "goToInstruction", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
		$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm($BirthCertNo = ""){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission, $sys_custom, $admission_cfg, $lac;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm(0,$BirthCertNo);
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span>'.$Lang['Admission']['munsang']['mandatoryfield'].'</span>';
		$x .='<br/>「<span class="tabletextrequire">*</span>」are mandatory but if not applicable please fill in N.A.';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_input_form','step_instruction')", "backToInstruction", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_input_form','step_docs_upload')", "goToDocs", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->';
				if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0, $BirthCertNo = "", $IsUpdate=0){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg;

		$prevApplyClassLevel = array();

		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			
			$rs = $lac->getApplicationCustInfo($_SESSION['KIS_ApplicationID'], 'AppliedBeforeClass');
			$prevApplyClassLevel = Get_Array_By_Key($rs, 'Value');

			
			//debug_pr($formData);
			//debug_pr($application_details);
			if(count($application_details) > 0){
				$applicationStudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
				//debug_pr($applicationStudentInfo);
// 				$stuNameArr_en = explode(',',$applicationStudentInfo['student_name_en']);
// 				$stuNameArr_b5 = explode(',',$applicationStudentInfo['student_name_b5']);
			}
			## Current School Info START ##
			//$studentCustInfo = $lac-> getApplicationStudentInfoCust($application_details['ApplyYear'],$classLevelID='',$application_details['ApplicationID'],$recordID='');
			## Current School Info END ##
		}
		$allClassLevel = $lac->getClassLevel();
		
		$isBirthCertNo = 0;
		if(preg_match('/^[a-zA-Z][0-9]{6}[aA0-9]$/', $BirthCertNo)){
			$isBirthCertNo = 1;
		}
		

		$applicationSetting = $lac->getApplicationSetting($application_details['ApplyYear']);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		@ob_start();
		include("template/admissionForm/studentForm.tmpl.php");
		$x = ob_get_clean();
		
		if($IsUpdate){
			## Apply Year START ##
			$classLevelID = $applicationStudentInfo['classLevelID'];
			$allClassLevel = $lac->getClassLevel();
			$classLevel = $allClassLevel[$classLevelID];
			## Apply Year END ##
			
			$x .= '<h1>'.$Lang['Admission']['applyLevel'].' Student Information</h1>';
			$x .= '<table class="form_table" style="font-size: 13px"><tbody><tr>
				<td class="field_title">'.$Lang['Admission']['applyLevel'].' Student Information</td><td>'.$classLevel.'
				<input name="sus_status" type="text" id="sus_status" class="textboxtext" value="'.$classLevelID.'" hidden />		
				</td>
						
			</tr></tbody></table>';
			
		}
		return $x;
	}
	
	function getParentForm($IsConfirm=0, $IsUpdate=0){
		global $fileData, $formData, $Lang, $lac;
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

			$tmpParentInfoArr = $lac->getApplicationParentInfo($application_details['ApplyYear'],'',$_SESSION['KIS_ApplicationID']);
			$parentInfoArr = array();
			foreach($tmpParentInfoArr as $parent){
			    if($parent['type'] != 'M'){
			        $parentType = $parent['type'];
			    }
				foreach($parent as $para=>$info){
					$parentInfoArr[$parent['type']][$para] = $info;
				}
			}
			#### Parent Info END ####
		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		@ob_start();
		include("template/admissionForm/parentForm.tmpl.php");
		$x = ob_get_clean();
			
		return $x;
	}
	function getOthersForm($IsConfirm=0, $IsUpdate=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		$admission_year_start = substr($admission_year, 0, 4);
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
//		else{
//			foreach($dayTypeArr as $aDayType){
//	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
//	//			$dayTypeOption .=" ";
//				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
//			}
//			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
//		}
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			$StatusInfo = $lac->getApplicationStatus($application_details['ApplyYear'],'',$application_details['ApplicationID']);
			$applicationStudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
		}
		
		@ob_start();
		include("template/admissionForm/otherForm.tmpl.php");
		$x = ob_get_clean();
		
		return $x;
	}
	
	function getDocsUploadForm($IsConfirm=0, $IsUpdate=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac, $sys_custom, $formData,$intranet_root, $kis_lang;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			//debug_pr($application_details);
			if(count($application_details) > 0){
				$applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'],array('applicationID'=>$application_details['ApplicationID']));
				//debug_pr($applicationAttachmentInfo);
			}
			## Photo START ##
			$viewFilePath = (is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']]['personal_photo']['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['StudentPersonalPhoto'])?' <a href="download_attachment.php?type=personal_photo'./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]*/'" target="_blank" >檢視已遞交的檔案 View submitted file</a>':'');
			
			## Photo END ##
			
		}
		if(!$lac->isInternalUse($_GET['token'])){
			$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		}else{
			$star = '';
		}
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm && !$IsUpdate){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1 style="font-size: 15px">'.$Lang['Admission']['docsUpload'].' Documents Upload</h1>';
		}
		$x .='<table class="form_table" style="font-size: 15px">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)<br/>Document (image in JPEG/GIF/PNG/PDF format, file size less than : '.($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
		$x .='<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].' Personal Photo</td>
				<td>';

		if($IsConfirm){
			if($fileData['StudentPersonalPhoto']){
			    $x .= '<div id="StudentPersonalPhotoDiv">
    						<img src="#" style="max-width:600px" id="imgStudentPersonalPhoto"/>
    						<br/>
    					</div>';
		        $x .= stripslashes($fileData['StudentPersonalPhoto']);
		        $x .= '<a href="#" target="_blank" id="hrefStudentPersonalPhoto" style="display:none;"> ['.$kis_lang['preview'].']</a>';
			}
		}else{
		    $x .= '<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png" />';
		}
		$x .= $viewFilePath;
		/*$x .='<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].' Personal Photo</td>
				<td>'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png"/>').$viewFilePath.'</td></tr>';
		
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$attachment_name.'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile'.$i]):'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').(is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['OtherFile'.$i])?' <a href="download_attachment.php?type='.$attachment_settings[$i]['AttachmentName']./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]* /'" target="_blank" >檢視已遞交的檔案 View submitted file</a>':'').'</td>
				  </tr>';
		}*/
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
    		$x .= '<tr>
    			<td class="field_title">'.($i<0?$star:'').$attachment_name.'</td>
    			<td>';
    		if($IsConfirm){
				if($fileData['OtherFile'.$i]){
    				$x .= '<div id="divOtherFile'.$i.'">
    						<img src="#" style="max-width:600px" id="imgOtherFile'.$i.'"/>
    						<br/>
    					</div>';
					$x .= stripslashes($fileData['OtherFile'.$i]);
					$x .= '<a href="#" target="_blank" id="hrefOtherFile'.$i.'" style="display:none;"> ['.$kis_lang['preview'].']</a>';
				}
    		}else{
    				$x .= '<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />';
    		}
    				$x .= (is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['OtherFile'.$i])?' <a href="download_attachment.php?type='.$attachment_settings[$i]['AttachmentName'].'" target="_blank" >檢視已遞交的檔案 View submitted file</a>':'').'<br/>
    			</td>
    		</tr>';
		}
			
		
		if(!$IsConfirm && $IsUpdate){
			$x .='<tr>
					<td colspan="2">'.$star.'如不需更改文件檔案，則不用選擇檔案 if you no need to update document file, please make it blank</span></td>
				</tr>';
		}	
		/*$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';*/
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->';
			$x .= '</table>';
		if(!$IsConfirm && !$IsUpdate){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_docs_upload','step_input_form')", "backToForm", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_docs_upload','step_confirm')", "goToConfirm", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->';
				if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				
			$x.='</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	
	function stripslashesRecursive($formData){
		if($formData){
			foreach ($formData as $key=>$value) {
				if(is_array($value)){
					$formData[$key] = $this->stripslashesRecursive($value);
				}else{
					$formData[$key] = stripslashes($value);
					if($formData[$key] == ""){
						$formData[$key] =" -- ";
					}
				}
			}
		}
		return $formData;
	}
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;

		//remove the slashes of the special character
// 		if($formData){
// 			foreach ($formData as $key=>$value) {
// 				$formData[$key] = stripslashes($value);
// 				if($formData[$key] == ""){
// 					$formData[$key] =" -- ";
// 				}
// 			}
// 		}
		$formData = $this->stripslashesRecursive($formData);
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back']." Back", "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN("提交 Submit", "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
				if(!$lac->isInternalUse($_REQUEST['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
				else{
					$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				}
			$x.='</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7, $ApplicationID);
		if($ApplicationID){
			$x .=' <div class="admission_complete_msg"><h1><span style="display:inline">報名表已遞交，申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;</span><br/>';
			$x .='<span style="display:inline">Your application form has been successfully submitted.Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';

//			$x .='<br/><h1>現在請進行報名費付款 ，以完成報名程序。<br/>';            
//			$x .='To finish the online application, please pay now.<br/><br><input type="button" value="繳付 HK$100 報名費 Pay application fee HK$100" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'Payment Status\');"></input></h1>';            
//			
			$x .='<br/><h1><span>另外，閣下將會收到確認電郵，請檢查閣下在申請表填寫的電郵為 '.$lac->getApplicantEmail($ApplicationID).'。<br/>如未收到該電郵，請致電與本校聯絡。<br/>';            
			$x .='An acknowledgement will be sent to you via email service. Please ensure that the email address entered on the form is '.$lac->getApplicantEmail($ApplicationID).'. <br/>Please contact our school in case you fail to receive it.</span></h1>';            
			
//			$x .='<br/><h1><span>謝謝您使用網上報名服務！<br/>';            
//			$x .='Thank you for using our online application!</span></h1>';            
			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
//			
//			//add english version here...
//			$x .='<h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			
		
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>未能成功遞交申請。<span>請重新嘗試申請或致電與本校聯絡。</span></h1>';
            $x .='<h1>Your application is rejected.<span>Please try to apply again or contact our school.</span></h1>';
            
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent || !$ApplicationID){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		if((!$lac->isInternalUse($_GET['token']) || $lac->getTokenByApplicationNumber($ApplicationID)!='') && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{				
			$x .= '<div class="edit_bottom">';
				//$x .= '<input id="finish_page_payment_button" type="button" class="formsubbutton" value="繳付 HK$100 報名費 Pay application fee HK$100" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'_blank\');"></input>';
				$x .= '<input id="finish_page_finish_button" style="" type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />';
			$x .= '</div>';
			$x .= '<p class="spacer"></p></div>';
		}
		return $x;
	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID='', $paymentEmail=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");

		if($lac->isInternalUse($_GET['token']) && $lac->getTokenByApplicationNumber($ApplicationID)==''){
		    if($ApplicationID){
		        $schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;
		        $stuedentInfo = $lac->getApplicationStudentInfo($schoolYearID,'',$ApplicationID);
		        
		        $x .='閣下的郵遞報名申請表已收到。學童姓名為 '.$stuedentInfo[0]['student_name_b5'].'，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';
		        $x .='Your application form has been received, name of child is '.$stuedentInfo[0]['student_name_en'].' and the application number is <u>'.$ApplicationID.'</u>.';
		
		        $x .='<br/><br/><br/>學校將透過電郵通知閣下有關面見安排，相關資訊可於學校網頁瀏覽。<br/>';
		        $x .='You will be informed of arrangements for interview(s) via email. Please visit the school website for related information.';
		
		        $x .='<br/><br/><br/>謝謝您的申請！<br/>';
		        $x .='Thanks for your application!';
		
		        if(!$LastContent){
		            $LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		        }
		        $x .= '<br/><br/>'.$LastContent;
		    }
		}else{
    		if($ApplicationID){
    			$x .='報名表已遞交，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';         
    			$x .='Your application form has been successfully submitted and the application number is <u>'.$ApplicationID.'</u>.<br/>';
    			
    			$x .='<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
    			$x .='Click this hyperlink to view your application form.';
    			$x .="<br/><a target='_blank' href='http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."'>http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."</a>";
    			
    			$x .='<br/><br/><br/>謝謝您使用網上報名服務！<br/>';            
    			$x .='Thanks for lodging your application online!';
    			
    			$x .= '<br/><br/>'.$LastContent;
    		}else{
    			$x .='未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
    			$x .='<br/><br/>';
    			$x .='Your application is rejected. Please try to apply again or contact our school.';
            }
        }
        
		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';            
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['admissionQuotaFull'].'</h1>';
				$LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['interviewQuotaFull'].'</h1>';
				$LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}

	function getPDFContent($schoolYearID,$applicationIDAry,$type=''){
		global $PATH_WRT_ROOT,$lac,$Lang,$admission_cfg, $setting_path_ip_rel, $intranet_session_language;
	
		$Lang['General']['EmptySymbol'] = '---';
		$yearStart = date('Y',getStartOfAcademicYear('',$schoolYearID));
		$yearEnd = (((int)$yearStart)+1);
		$settings = $lac->getApplicationSetting($schoolYearID);

		$selectedSchoolYear = current(kis_utility::getAcademicYears(array('AcademicYearID'=>$schoolYearID)));
		$selectedSchoolYearHTML = $selectedSchoolYear['academic_year_name_'.$intranet_session_language];
		
		$allClassLevel = $lac->getClassLevel();
		
		######## Load Image Path to Variable START ########
		$imageNames = array(
			'checkbox','checked',
			'checkbox2','checked2', 
			'photo'
		);
		foreach($imageNames as $names){
		    global $$names;
			$$names = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}/images/{$names}.png";
		}
		$logo = "{$PATH_WRT_ROOT}file/customization/{$setting_path_ip_rel}/images/school_logo.png";
		######## Load Image Path to Variable END ########
	
		######## Init PDF START ########
		$templateHeaderPath = $PATH_WRT_ROOT.'file/customization/'.$setting_path_ip_rel.'/pdf/header.php';
		$templatePath = $PATH_WRT_ROOT.'file/customization/'.$setting_path_ip_rel.'/pdf/application_form.php';
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		
		$mpdf = new mPDF(
		    $mode='',
		    $format='A4',
		    $default_font_size=0,
		    $default_font='',
		    $marginLeft=1,
		    $marginRight=1,
		    $marginTop=5,
		    $marginBottom=3/*,
		    $marginHeader=9,
		    $marginFooter=9, 
		    $orientation='P'*/
	    );
		$mpdf->mirrorMargins = 1;
		######## Init PDF END ########
	
		######## Load header to PDF START ########
		ob_start();
		include($templateHeaderPath);
		$pageHeader = ob_get_clean();
		
		$mpdf->WriteHTML($pageHeader);
		######## Load header to PDF END ########
		
		
		######## Load data to PDF START ########
		global $allCustInfo; // For helper function
		foreach((array)$applicationIDAry as $index=>$applicationID){
			
			#### All Cust Info START ####
			$allCustInfo = $lac->getAllApplicationCustInfo($applicationID);
			#### All Cust Info END ####
				
			## Current School Info START ##
			/*$studentCustInfo = $lac-> getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID,$recordID='');
			
			foreach($studentCustInfo as $key=>$school){
				$studentCustInfo[$key]['Class'] = explode(' - ', $school['Class']);
				$studentCustInfo[$key]['Year'] = explode(' - ', $school['Year']);
			}*/
			## Current School Info END ##
			
			#### Student Info START ####
			$StuInfoArr = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
			

			## Photo START ##
			$attachmentArr = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
			$photoLink = $attachmentArr['personal_photo']['link'];
			$photoLink = ($photoLink)?$photoLink:$photo;
			## Photo END ##
			
			## Age Range START ##
			/*$ageRangeStart = $settings[ $StuInfoArr['classLevelID'] ]['DOBStart'];
			$ageRangeStart = explode('-',$ageRangeStart);
			$ageRangeStart = array_reverse($ageRangeStart);
			$ageRangeStart = implode('/',$ageRangeStart);
			
			$ageRangeEnd = $settings[ $StuInfoArr['classLevelID'] ]['DOBEnd'];
			$ageRangeEnd = explode('-',$ageRangeEnd);
			$ageRangeEnd = array_reverse($ageRangeEnd);
			$ageRangeEnd = implode('/',$ageRangeEnd);
			
			$ageRangeHTML = "{$ageRangeStart} - {$ageRangeEnd}";*/
			## Age Range END ##
			
			$StuInfoArr['DobArr'] = explode('-',$StuInfoArr['DOB']);

			## Prev applied class START ##
			$rs = $lac->getApplicationCustInfo($applicationID, 'AppliedBeforeClass');
			$prevApplyClassLevel = Get_Array_By_Key($rs, 'Value');
			## Prev applied class END ##
			#### Student Info END ####
			
			#### Parent Info START ####
			$tmpParentInfoArr = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
			$parentInfoArr = array();
			$parentType = '';
			foreach($tmpParentInfoArr as $parent){
			    $parentType = ($parent['type'] != 'M')? $parent['type'] : $parentType;
				foreach($parent as $para=>$info){
					$parentInfoArr[$parent['type']][$para] = $info;
				}
			}
			#### Parent Info END ####

				
			#### Load Template START ####
			ob_start();
			include($templatePath);
			$page1 = ob_get_clean();
			#### Load Template END ####
	
			$mpdf->WriteHTML($page1);
		}
		######## Load data to PDF END ########
	
// 				echo $pageHeader;
// 				echo $page1;
		$mpdf->Output();
	}
	
	function getPayPalButton($ApplicationID){
		global $admission_cfg, $lac;
		$ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
		return '<form action="'.$admission_cfg['paypal_url'].'" <!--onsubmit="checkPayment(\''.$ApplicationID.'\');return false;"--> method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="'.$admission_cfg['hosted_button_id'].'">
				<input type="hidden" name="return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php" /> 
				    <input type="hidden" name="cancel_return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php?cm='.$ApplicationID.'" />
				    <input type="hidden" name="custom" value="'.$ApplicationID.'" />
					<input type="hidden" name="notify_url" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_ipn.php" /> 
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();
				        
						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
	}
	
	function getUpdateWizardStepsUI($Step, $ApplicationID=''){
		global $Lang, $sys_custom,$lac, $validForAdmission, $kis_lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$active_step8 ="";
		$active_step9 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		$href_step8 ="";
		$href_step9 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_update_input_form\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		    case 8:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="completed-step";
		        $active_step8 ="active-step";
		        break;
		    case 9:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="completed-step";
		        $active_step8 ="completed-step";
		        $active_step9 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">';
		$x .='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultupdatepagemessage'] .'<br/>This page is for update applicaiton information.</center></h2>';
		if($lac->isInternalUse($_REQUEST['token']) && ($Step!=7 || $lac->getTokenByApplicationNumber($ApplicationID)=='')){
			$x .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}		
				$x .='<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<!--<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].' Instruction</a></div>-->
					<!--<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].' Personal Info</a></div>-->
					<div class="'.$active_step5.' first_step"><a '.$href_step5.'><span>1</span>'.'資料更改 Information Update</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>2</span>'.$Lang['Admission']['confirmation'].' Confirmation</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:90px">3 '.$Lang['Admission']['finish'].' Finish</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getUpdateIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac,$sys_custom,$validForAdmission;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage'].'<br/>Welcome to our online application page!'; //Henry 20131107
		}
		
		if($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && $lac->IsAfterUpdatePeriod()){
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultviewpagemessage'] .'<br/>This page is for view applicaiton information.</center></h2>';
		}
		else{
			$previewnote ='<h2 style="font-size:18px;color:red"><center>'.$Lang['Admission']['msg']['defaultupdatepagemessage'] .'<br/>This page is for update applicaiton information.</center></h2>';
		}
		if($lac->isInternalUse($_GET['token'])){
			$previewnote .='<h2 style="font-size:18px;color:red"><center>'.$kis_lang['remarks'] .'<br/>Internal Use</center></h2>';
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">'.$previewnote.'
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].' Online Application</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && $lac->IsUpdatePeriod()){
							$star = '<font style="color:red;">*</font>';
							
							//$x .='<form id="form1" name="form1">';
							$x .= '<table class="form_table" style="font-size: 13px">';
							$x .= '<tr>';
							$x .= '<td class="field_title">'.$star.'輸入申請編號 Application Number</td>';
							$x .= '<td><input style="width:200px" name="InputApplicationID" type="text" id="InputApplicationID" class="textboxtext" maxlength="16" size="8"/></td>';
							$x .= '</tr>';
							$x .= '<tr>';
							$x .= '<td class="field_title">'.$star.'輸入出生證明書號碼 Birth Certificate Number</td>';
							$x .= '<td><input style="width:200px" name="InputStudentBirthCertNo" type="text" id="InputStudentBirthCertNo" class="textboxtext" maxlength="64" size="8"/><br />不需要輸入括號，例如：A123456(7)，請輸入 "A1234567"。<br/>No need to enter the brackets. Eg. A123456(7), please enter "A1234567".</td>';
							$x .= '</tr>';
							$x .= '<tr>';
							$x .= '<td class="field_title">'.$star.'輸入出生日期 Date of Birth</td>';
							$x .= '<td><input style="width:200px" name="InputStudentDateOfBirth" type="text" id="InputStudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)</td>';
							$x .= '</tr>';
							$x .= '</table>';
							//$x .='</form>';
							
							$x .= '<div class="edit_bottom">
									'.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_index','step_update_input_form')", "goToForm", "", 0, "formbutton").'
								</div>
								<p class="spacer"></p>';
								
							$x .='<p class="spacer"></p><br/><span>建議使用 <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> 瀏覽器。</span>';
							$x .='<br/><span>Recommended to use <a href="http://www.google.com/chrome/browser/" target="download_chrome">Google Chrome</a> Browser.</span>';
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}
	
	function getInputFormPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		if($lac->IsUpdatePeriod()){
			//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
			//x .= '<div class="admission_board">';
			$x .= $this->getUpdateWizardStepsUI(5);
			
			$x .=$this->getStudentForm(0, $fileData['InputStudentBirthCertNo'], 1);
			$x .= $this->getParentForm(0,1);
			$x .= $this->getOthersForm(0,1);
			$x .= $this->getDocsUploadForm(0,1);
			$x .= '</div>
				<div class="edit_bottom">
					'.$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_update_input_form','step_index')", "backToIndex", "", 0, "formbutton").' '
					.$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_update_input_form','step_confirm')", "goToConfirm", "", 0, "formbutton")
					.'
					<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
					<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
					$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index_edit.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
				$x.='</div>
				<p class="spacer"></p>';
				//$x .='</form></div>';
		}
		else{
			$applicationSetting = $lac->getApplicationSetting($lac->schoolYearID);
			$applicationOthersInfo = current($lac->getApplicationOthersInfo($lac->schoolYearID,'',$fileData['InputApplicationID']));
			$lastContent = $applicationSetting[$applicationOthersInfo['classLevelID']]['LastPageContent'];
			$x .= $this->getAfterUpdateFinishPageContent($fileData['InputApplicationID'], $lastContent);
		}
		return $x;
	}
	
	function getUpdateConfirmPageContent(){
		global $Lang, $fileData, $formData, $sys_custom, $lac;
		
		//remove the slashes of the special character
		$formData = $this->stripslashesRecursive($formData);
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getUpdateWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1,1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back']." Back", "button", "goto('step_confirm','step_update_input_form')", "SubmitBtn", "id='backToForm'", 0, "formbutton").' '
				.$this->GET_ACTION_BTN("提交 Submit", "submit", "", "SubmitBtn", "id='goToSubmit'", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
				$x.='<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index_edit.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].' Cancel" />';
			$x.='</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getUpdateFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID='', $sus_status=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		// finish page	
		$x .= $this->getUpdateWizardStepsUI(7);
		if($ApplicationID){
			$x .=' <div class="admission_complete_msg"><h1>報名表資料已更改，申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/>';
			$x .='Your application form has been updated. Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></h1>';

//			$x .='<br/><h1>現在請進行報名費付款 ，以完成報名程序。<br/>';            
//			$x .='To finish the online application, please pay now.<br/><br><input type="button" value="繳付 HK$40 報名費 Pay application fee HK$40" onclick="window.open(\'http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID).'\',\'Payment Status\');"></input></h1>';            
//			
			$x .='<br/><h1><span>另外，閣下將會收到確認電郵，請檢查閣下在申請表填寫的電郵為 '.$lac->getApplicantEmail($ApplicationID).'。<br/>如未收到該電郵，請致電與本校聯絡。<br/>';            
			$x .='An acknowledgement will be sent to you via email service. Please ensure that the email address entered on the form is '.$lac->getApplicantEmail($ApplicationID).'. <br/>Please contact our school in case you fail to receive it.</span></h1>';            
			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
//			
//			//add english version here...
//			$x .='<h1>Admission is Completed.<span>Your application number is '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';
//			$x .='<h1><span>Notice of the application has been sent to your contact E-mail: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
			
		
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>未能成功更改資料。<span>請重新嘗試申請或致電與本校聯絡。</span></h1>';
            $x .='<h1>Information cannot be updated.<span>Please try to apply again or contact our school.</span></h1>';
            
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent || !$ApplicationID){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		
		
		return $x;
	}
	
	function getAfterUpdateFinishPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';

		$x .=' <div class="admission_complete_msg"><h1>申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/>';
		$x .='Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></h1></div>';
		
		$x .= '<div class="admission_board">';
		$x .= $this->getDocsUploadForm(1, 1);
		
		$applicationStatus = current($lac->getApplicationStatus($lac->schoolYearID,'',$ApplicationID));
		
		if($applicationStatus['interviewdate'] && $applicationStatus['interviewdate'] != '0000-00-00 00:00:00'){
			$interviewDateTime = explode(" ", $applicationStatus['interviewdate']);
			$x .='<h1 style="font-size: 15px">面試資料 Interview Information</h1>';
			$x .= '<table class="form_table" style="font-size: 15px">';
			$x .= '<tr>';
			$x .= '<td class="field_title">面試日期 Interview Date</td>';
			$x .= '<td>'.$interviewDateTime[0].'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試時間 Interview Time</td>';
			$x .= '<td>'.substr($interviewDateTime[1], 0, -3).'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試地點 Interview Location</td>';
			$x .= '<td>'.$applicationStatus['interviewlocation'].'</td>';
			$x .= '</tr>';
			$x .= '</table>';
		}
		$x .= '</div>';
		
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent;
		$x .= '</div>';
		
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		
		
		return $x;
	}
	
	function getUpdateFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		if($ApplicationID){
			$x .='報名表資料已更改，申請編號為 <u>'.$ApplicationID.'</u>。<br/>';         
			$x .='Your application form has been updated and the application number is <u>'.$ApplicationID.'</u>.<br/>';
			
//			$x .='<br/><br/>注意：所有申請必須支付報名費才有效；如未繳付或想檢查繳付情況，可按以下連結：<br/>';
//			$x .='Attention: Application is valid only if the application fee is paid. To pay or check payment status, click this hyperlink:';            
//			$x .='<br/>http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/index.php?token='.MD5($ApplicationID);            
			
			$x .='<br/><br/>如想查看已遞交的申請表，可按以下連結。 <br/>';
			$x .='Click this hyperlink to view your application form.';
			$x .="<br/><a target='_blank' href='http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."'>http://".$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID)."</a>";
			
			$x .='<br/><br/><br/>如需要更改已遞交的申請資料，可於報名截止前按以下連結。<br/>';
			$x .='Click this hyperlink to amend your application form before the application deadline.';
			$x .="<br/><a target='_blank' href='http://{$_SERVER['HTTP_HOST']}/kis/admission_form/index_edit.php'>http://{$_SERVER['HTTP_HOST']}/kis/admission_form/index_edit.php</a>";
			
			$x .='<br/><br/><br/>謝謝您使用網上報名服務！<br/>';            
			$x .='Thanks for lodging your application online!';
			
//			if(!$LastContent){
//				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//			}
			$x .= '<br/><br/>'.$LastContent;
			
		}
		else{
			$x .='未能成功遞交申請，請重新嘗試申請或致電與本校聯絡。';
			$x .='<br/><br/>';
			$x .='Your application is rejected. Please try to apply again or contact our school.';
        }
//		if($ApplicationID){
//			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);         
//			$x .='<br/><br/>';
//			$x .='Admission is Completed. Your application number is <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>Application form preview<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);
//			if(!$LastContent){
//				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//			}
//			$x .= '<br/>'.$LastContent;
//		}
//		else{
//			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
//			$x .='<br/><br/>';
//			$x .='Admission is Not Completed. Please try to apply again!';
//        }
		
		return $x;
	}
}
?>