<?php
	//using: Pun
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

	$admission_cfg['PrintByPDF'] = 1;

	//Please also define lang in admission_lang
	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['SSGC_printed']	= 3;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;
	$admission_cfg['Status']['SSGC_read']	= 8;


	$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
	$admission_cfg['PaymentStatus']['OtherPayment']	= 2;


	$admission_cfg['YearName'] = array();
	$admission_cfg['YearName']['K1']['zh'] = '幼兒班';
	$admission_cfg['YearName']['K1']['en'] = 'Nursery';
	$admission_cfg['YearName']['K2']['zh'] = '低班';
	$admission_cfg['YearName']['K2']['en'] = 'Lower';
	$admission_cfg['YearName']['K3']['zh'] = '高班';
	$admission_cfg['YearName']['K3']['en'] = 'Upper';

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;
//	$admission_cfg['Status']['reservedstudent']	= 5;

// 	$admission_cfg['BirthCertType'] = array();
// 	$admission_cfg['BirthCertType']['birthCert']	= 1;
// 	$admission_cfg['BirthCertType']['hkid']	= 2;
// 	$admission_cfg['BirthCertType']['others']	= 3;



    $admission_cfg['SessionChoice'] = array();
    $admission_cfg['SessionChoice'][0] = '上午班 AM';
    $admission_cfg['SessionChoice'][1] = '下午班 PM';
    $admission_cfg['SessionChoice'][2] = '上下午班均可 Either AM/PM';


    $admission_cfg['DistrictArea'] = array();
    $admission_cfg['DistrictArea'][0] = '香港島 Hong Kong Island';
    $admission_cfg['DistrictArea'][1] = '九龍 Kowloon';
    $admission_cfg['DistrictArea'][2] = '新界 New Territories';

    $admission_cfg['DistrictAddress'] = array();
    $admission_cfg['DistrictAddress'][0] = array();
    $admission_cfg['DistrictAddress'][1] = array();
    $admission_cfg['DistrictAddress'][2] = array();
    $admission_cfg['DistrictAddress'][0][0] = '中環 Central';
    $admission_cfg['DistrictAddress'][0][1] = '上環 Sheung Wan';
    $admission_cfg['DistrictAddress'][0][2] = '西營盤 Sai Ying Pun';
    $admission_cfg['DistrictAddress'][0][3] = '堅尼地城 Kennedy Town';
    $admission_cfg['DistrictAddress'][0][4] = '山頂 The Peak';
    $admission_cfg['DistrictAddress'][0][5] = '灣仔 Wan Chai';
    $admission_cfg['DistrictAddress'][0][6] = '銅鑼灣 Causeway Bay';
    $admission_cfg['DistrictAddress'][0][7] = '跑馬地 Happy Valley';
    $admission_cfg['DistrictAddress'][0][8] = '天后 Tin Hau';
    $admission_cfg['DistrictAddress'][0][9] = '大坑 Tai Hang';
    $admission_cfg['DistrictAddress'][0][10] = '渣甸山 Jardine\'s Lookout';
    $admission_cfg['DistrictAddress'][0][11] = '炮台山 Fortress Hill';
    $admission_cfg['DistrictAddress'][0][12] = '北角 North Point';
    $admission_cfg['DistrictAddress'][0][13] = '鰂魚涌 Quarry Bay';
    $admission_cfg['DistrictAddress'][0][14] = '太古 Taikoo';
    $admission_cfg['DistrictAddress'][0][15] = '鯉景灣 Lei King Wan';
    $admission_cfg['DistrictAddress'][0][16] = '西灣河 Sai Wan Ho';
    $admission_cfg['DistrictAddress'][0][17] = '筲箕灣 Shau Kei Wan';
    $admission_cfg['DistrictAddress'][0][18] = '杏花邨 Heng Fa Chuen';
    $admission_cfg['DistrictAddress'][0][19] = '柴灣 Chai Wan';
    $admission_cfg['DistrictAddress'][0][20] = '石澳 Shek O';
    $admission_cfg['DistrictAddress'][0][21] = '香港仔 Aberdeen';
    $admission_cfg['DistrictAddress'][0][22] = '鴨脷洲 Ap Lei Chau';
    $admission_cfg['DistrictAddress'][0][23] = '薄扶林 Pok Fu Lam';
    $admission_cfg['DistrictAddress'][0][24] = '黃竹坑 Wong Chuk Hang';
    $admission_cfg['DistrictAddress'][0][25] = '赤柱 Stanley';
    $admission_cfg['DistrictAddress'][0][26] = '淺水灣 Repulse Bay';
    $admission_cfg['DistrictAddress'][0][27] = '其他 Others';
    $admission_cfg['DistrictAddress'][1][0] = '九龍城 Kowloon City';
    $admission_cfg['DistrictAddress'][1][1] = '觀塘 Kwun Tong';
    $admission_cfg['DistrictAddress'][1][2] = '深水埗 Sham Shui Po';
    $admission_cfg['DistrictAddress'][1][3] = '黃大仙 Wong Tai Sin';
    $admission_cfg['DistrictAddress'][1][4] = '油麻地 Yau Ma Tei';
    $admission_cfg['DistrictAddress'][1][5] = '尖沙咀 Tsim Sha Tsui';
    $admission_cfg['DistrictAddress'][1][6] = '旺角 Mong Kok';
    $admission_cfg['DistrictAddress'][1][7] = '其他 Others';
    $admission_cfg['DistrictAddress'][2][0] = '離島區 Islands District';
    $admission_cfg['DistrictAddress'][2][1] = '葵青區 Kwai Tsing District';
    $admission_cfg['DistrictAddress'][2][2] = '北區 North District';
    $admission_cfg['DistrictAddress'][2][3] = '西貢區 Sai Kung District';
    $admission_cfg['DistrictAddress'][2][4] = '沙田區 Sha Tin District';
    $admission_cfg['DistrictAddress'][2][5] = '大埔區 Tai Po District';
    $admission_cfg['DistrictAddress'][2][6] = '荃灣區 Tsuen Wan District';
    $admission_cfg['DistrictAddress'][2][7] = '屯門區 Tuen Mun District';
    $admission_cfg['DistrictAddress'][2][8] = '元朗區 Yuen Long District';
    $admission_cfg['DistrictAddress'][2][9] = '其他 Others';

    $admission_cfg['DistrictMapping'] = array();
    $admission_cfg['DistrictMapping'][0] = array();
    $admission_cfg['DistrictMapping'][1] = array();
    $admission_cfg['DistrictMapping'][2] = array();
    $admission_cfg['DistrictMapping'][0][0] =
    $admission_cfg['DistrictMapping'][0][1] =
    $admission_cfg['DistrictMapping'][0][2] =
    $admission_cfg['DistrictMapping'][0][3] =
    $admission_cfg['DistrictMapping'][0][4] = 11; //中環、上環、西營盤、堅尼地城、山頂
    $admission_cfg['DistrictMapping'][0][5] =
    $admission_cfg['DistrictMapping'][0][6] =
    $admission_cfg['DistrictMapping'][0][7] =
    $admission_cfg['DistrictMapping'][0][8] =
    $admission_cfg['DistrictMapping'][0][9] =
    $admission_cfg['DistrictMapping'][0][10] = 12; //灣仔、銅鑼灣、跑馬地、天后、大坑、渣甸山
    $admission_cfg['DistrictMapping'][0][11] =
    $admission_cfg['DistrictMapping'][0][12] =
    $admission_cfg['DistrictMapping'][0][13] =
    $admission_cfg['DistrictMapping'][0][14] =
    $admission_cfg['DistrictMapping'][0][15] = 14; //炮台山、北角、鰂魚涌、太古、鯉景灣
    $admission_cfg['DistrictMapping'][0][16] =
    $admission_cfg['DistrictMapping'][0][17] =
    $admission_cfg['DistrictMapping'][0][18] =
    $admission_cfg['DistrictMapping'][0][19] =
    $admission_cfg['DistrictMapping'][0][20] = 16; //西灣河、筲箕灣、杏花邨、柴灣、石澳
    $admission_cfg['DistrictMapping'][0][21] =
    $admission_cfg['DistrictMapping'][0][22] =
    $admission_cfg['DistrictMapping'][0][23] =
    $admission_cfg['DistrictMapping'][0][24] =
    $admission_cfg['DistrictMapping'][0][25] =
    $admission_cfg['DistrictMapping'][0][26] = 18; //香港仔、鴨脷洲、薄扶林、黃竹坑、赤柱、淺水灣
    $admission_cfg['DistrictMapping'][0][27] = 0; //其他
    $admission_cfg['DistrictMapping'][1][0] =
    $admission_cfg['DistrictMapping'][1][1] =
    $admission_cfg['DistrictMapping'][1][2] =
    $admission_cfg['DistrictMapping'][1][3] =
    $admission_cfg['DistrictMapping'][1][4] =
    $admission_cfg['DistrictMapping'][1][5] =
    $admission_cfg['DistrictMapping'][1][6] =
    $admission_cfg['DistrictMapping'][1][7] = 19; //九龍
    $admission_cfg['DistrictMapping'][2][0] =
    $admission_cfg['DistrictMapping'][2][1] =
    $admission_cfg['DistrictMapping'][2][2] =
    $admission_cfg['DistrictMapping'][2][3] =
    $admission_cfg['DistrictMapping'][2][4] =
    $admission_cfg['DistrictMapping'][2][5] =
    $admission_cfg['DistrictMapping'][2][6] =
    $admission_cfg['DistrictMapping'][2][7] =
    $admission_cfg['DistrictMapping'][2][8] =
    $admission_cfg['DistrictMapping'][2][9] = 20; //新界





//	/* for paypal testing [start] */
//	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
////	$admission_cfg['paypal_to_email'] = '';
//	$admission_cfg['paypal_signature'] = '';
////	$admission_cfg['item_name'] = '';
////	$admission_cfg['item_number'] = 'AF2015';
////	$admission_cfg['amount'] = '40.00';
////	$admission_cfg['currency_code'] = 'HKD';
//	$admission_cfg['hosted_button_id'] = '';
//	$admission_cfg['paypal_name'] = '';
//	/* for paypal testing [End] */

	/* for real paypal use [start] * /
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = '';
	$admission_cfg['paypal_signature'] = '';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = '';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = '';
	$admission_cfg['paypal_name'] = '';
	/* for real paypal use [End] */

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	$admission_cfg['interview_arrangment']['interview_group_type'] = 'Room';
	$admission_cfg['interview_arrangment']['interview_group_name'] = array();

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}