<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}

.sessionChoice{
    display: inline-block;
    margin-right: 30px;
}
</style>

<h1>
    <?=$Lang['Admission']['studentInfo']?>
    Student Information
</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['chinesename']?> 
		Name in Chinese
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_b5']?>
		<?php }else{ ?>
			<input name="studentsname_b5" type="text" id="studentsname_b5" class="textboxtext" value="<?=$applicationStudentInfo['student_name_b5']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['englishname']?> (<?=$Lang['Admission']['UCCKE']['SameAsHKID']?>) <br />
		Name in English (same as on HKID Card)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_en']?>
		<?php }else{ ?>
			<input name="studentsname_en" type="text" id="studentsname_en" class="textboxtext"  value="<?=$applicationStudentInfo['student_name_en']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['dateofbirth']?>
		Date of Birth
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15" value="<?=$applicationStudentInfo['dateofbirth']?>"/>(YYYY-MM-DD)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['gender']?>
		Gender
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['StudentGender'])? ($Lang['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']) : ' -- ' ?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" <?=$applicationStudentInfo['gender'] == "M"?'checked':'' ?>>
			<label for="StudentGender1">M <?=$Lang['Admission']['genderType']['M']?></label>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender" <?=$applicationStudentInfo['gender'] == "F"?'checked':'' ?>>
			<label for="StudentGender2">F <?=$Lang['Admission']['genderType']['F']?></label>
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['SHCK']['birthCertNo']?> <font color="blue">(<?=$Lang['Admission']['SHCK']['birthCertNoHint'] ?>)</font>
		<br />
		Birth Cert No. <font color="blue">(Cannot be changed after submitted)</font>
	</td>
	<td>
		<?php 
		if($IsConfirm){
		    echo $formData['StudentBirthCertNo'];
		}else{ 
		?>
			<table>
				<tr>
        			<?php if($BirthCertNo){ ?>
        				<?php if($IsUpdate){ ?>
        					<input name="StudentBirthCertNo" value="<?=$BirthCertNo ?>" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none;width: 250px;"/>
        				<?php }else{ ?>
        					<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$BirthCertNo?>"/>
	            			<br/>
	            			<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
	            			 <br/>(eg：A123456(7)，please enter "A1234567")
        				<?php } ?>	
        			<?php }else{ ?>
            			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationStudentInfo['birthcertno'] ?>"/>
            			<br/>
            			<?=$Lang['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
            			 <br/>(eg：A123456(7)，please enter "A1234567")
        			<?php } ?>
        			</td>
				</tr>
			</table>

		<?php 
		} 
		?>
	</td>
	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['nationality']?>
		Nationality
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Nationality'];
		}else{
		?>
			<input id="Nationality" name="Nationality" class="textboxtext" value="<?=$applicationStudentInfo['Nationality'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['placeofbirth']?>
		Place of Birth
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['StudentPlaceOfBirth'];
		}else{
		?>
			<input id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" class="textboxtext" value="<?=$applicationStudentInfo['PlaceOfBirth'] ?>" />
		<?php
		}
		?>
	</td>
	
   	<td class="field_title">
		<?=$Lang['Admission']['SSGC']['religion']?>
		Religion
	</td>
	<td style="white-space: nowrap;">
		<?php
		if($IsConfirm){
            echo "{$formData['ReligionOther']}{$Lang['Admission']['SSGC']['religionShort']} {$formData['Church']}{$Lang['Admission']['SSGC']['church']}";
		}else{
		?>
			<input id="ReligionOther" name="ReligionOther" class="textboxtext" value="<?=$applicationStudentInfo['ReligionOther'] ?>" style="width: 120px;" />
			<?=$Lang['Admission']['SSGC']['religionShort'] ?>
			&nbsp;&nbsp;&nbsp;
			<input id="Church" name="Church" class="textboxtext" value="<?=$applicationStudentInfo['Church'] ?>" style="width: 120px;" />
			<?=$Lang['Admission']['SSGC']['church'] ?>
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['district']?>
		District
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo "{$admission_cfg['DistrictArea'][ $formData['AddressDistrict'] ]} ({$admission_cfg['DistrictAddress'][ $formData['AddressDistrict'] ][ $formData['AddressEstate'] ]})";
		}else{
		?>
			<select id="AddressDistrict" name="AddressDistrict">
				<?php
				foreach($admission_cfg['DistrictArea'] as $index => $address):
				    $selected = ($applicationStudentInfo['AddressDistrict'] == $index)? 'selected' : '';
				?>
					<option value="<?=$index ?>" <?=$selected ?> ><?=$address ?></option>
				<?php
				endforeach;
				?>
			</select>
			
			<?php foreach($admission_cfg['DistrictArea'] as $areaIndex => $address): ?>
    			<select id="AddressEstate<?=$areaIndex ?>" name="AddressEstate">
    				<?php
    				foreach($admission_cfg['DistrictAddress'][$areaIndex] as $addressIndex => $address):
    				    if(
    				        $applicationStudentInfo['AddressDistrict'] == $areaIndex && 
    				        $applicationStudentInfo['AddressEstate'] == $addressIndex
    				    ){
    				        $selected = 'selected';
    				    }else{
    				        $selected = '';
    				    }
    				?>
    					<option value="<?=$addressIndex ?>" <?=$selected ?> ><?=$address ?></option>
    				<?php
    				endforeach;
    				?>
    			</select>
			<?php endforeach; ?>
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['SSGC']['addressChinese']?>
		Address (Chinese)
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo nl2br($formData['AddressChi']);
		}else{
		?>
			<input id="AddressChi" name="AddressChi" class="textboxtext" value="<?=$applicationStudentInfo['AddressChi'] ?>" />
		<?php
		}
		?>
	</td>
	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['SSGC']['addressEnglish']?>
		Address (English)
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo nl2br($formData['Address']);
		}else{
		?>
			<input id="Address" name="Address" class="textboxtext" value="<?=$applicationStudentInfo['Address'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
		<?=$Lang['Admission']['SSGC']['presentSchool']?>
		Present School
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['LastSchool'];
		}else{
		?>
			<input id="LastSchool" name="LastSchool" class="textboxtext" value="<?=$applicationStudentInfo['LastSchool'] ?>" />
		<?php
		}
		?>
	</td>
	
   	<td class="field_title">
		<?=$Lang['Admission']['SSGC']['homeTel']?>
		Home Tel.
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['HomeTelNo'];
		}else{
		?>
			<input id="HomeTelNo" name="HomeTelNo" class="textboxtext" value="<?=$applicationStudentInfo['HomeTelNo'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['contactEmail']?> 
		Contact Email
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Email'];
		}else{
		?>
			<input name="Email" type="text" id="Email" class="textboxtext" value="<?=$applicationStudentInfo['Email'] ?>"/>
		<?php
		}
		?>
	</td>
	
	<?php
	if($IsConfirm){
	}else{
	?>
    	<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['SHCK']['ConfirmEmail']?> 
    		Re-enter Email
    	</td>
    	<td>
			<input name="EmailConfirm" type="text" id="EmailConfirm" class="textboxtext" value="<?=$applicationStudentInfo['Email'] ?>"/>
    	</td>
	<?php
	}
	?>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['SSGC']['desiredSession']?>
		Desired Session
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ 
		    $options = array();
		    for($i=1;$i<=3;$i++){
		        if($formData['OthersApplyDayType'.$i]){
		            $options[] = "{$Lang['Admission']['Option']} {$i} Option {$i}: {$Lang['Admission']['TimeSlot'][ $formData['OthersApplyDayType'.$i] ]}";
		        }
		    }
		    
		    if(count($options) > 1){
		        echo implode('<br />', $options);
		    }else{
		        echo $Lang['Admission']['TimeSlot'][ $formData['OthersApplyDayType1'] ];
		    }
		?>
		<?php }else if($IsUpdate){ ?>
		     <div id="DayTypeOption">
            	<input type="hidden" name="OthersApplyDayType[1]" value="<?=$applicationStudentInfo['ApplyDayType1'] ?>" />
            	<input type="hidden" name="OthersApplyDayType[2]" value="<?=$applicationStudentInfo['ApplyDayType2'] ?>" />
            	<input type="hidden" name="OthersApplyDayType[3]" value="<?=$applicationStudentInfo['ApplyDayType3'] ?>" />
            </div>
		<?php }else{?>
			<div id="DayTypeOption"></div>
		<?php } ?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['SSGC']['langSpoken']?>
		Can the student use Chinese as the learning medium
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=($formData['LangSpokenAtHome'])? "{$Lang['Admission']['yes3']} Yes":"{$Lang['Admission']['no3']} No"?>
		<?php }else{ ?>
			<input type="radio" value="1" id="LangSpokenAtHomeY" name="LangSpokenAtHome" <?=($applicationStudentInfo['LangSpokenAtHome'] == 1)?'checked':'' ?> />
			<label for="LangSpokenAtHomeY"><?=$Lang['Admission']['yes3']?> Yes</label>&nbsp;&nbsp;
			<input type="radio" value="0" id="LangSpokenAtHomeN" name="LangSpokenAtHome" <?=($applicationStudentInfo['LangSpokenAtHome'] == 0)?'checked':'' ?> />
			<label for="LangSpokenAtHomeN"><?=$Lang['Admission']['no3']?> No</label>
		<?php } ?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['SSGC']['appliedBefore']?>
		Has the student applied our kindergaten before
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
    		<?php if($formData['AppliedBefore']){ ?>
    			<?="{$Lang['Admission']['yes']} Yes"?>
    			(
        			<?php
        			$selectedClassLevel = array();
        			foreach((array)$formData['AppliedBeforeClass'] as $classLevelId){
        			    $selectedClassLevel[] = $allClassLevel[$classLevelId];
        			}
        			echo implode(', ', $selectedClassLevel);
        			?>
    			)
			<?php }else{ ?>
				<?="{$Lang['Admission']['no']} No" ?>
			<?php } ?>
		<?php }else{ ?>
    		<div>
    			<input type="radio" value="1" id="AppliedBeforeY" name="AppliedBefore" <?=($allCustInfo['AppliedBefore'][0]['Value'] == 1)?'checked':'' ?> />
    			<label for="AppliedBeforeY"><?=$Lang['Admission']['yes']?> Yes</label>&nbsp;&nbsp;
    			<input type="radio" value="0" id="AppliedBeforeN" name="AppliedBefore" <?=($allCustInfo['AppliedBefore'][0]['Value'] == 0)?'checked':'' ?> />
    			<label for="AppliedBeforeN"><?=$Lang['Admission']['no']?> No</label>
			</div>
			<div id="AppliedBeforeClassDiv">
				<?php
				foreach($allClassLevel as $classLevelId => $classLevel):
				    $checked = (in_array($classLevelId, $prevApplyClassLevel))? 'checked' : '';
				?>
        			<input type="checkbox" value="<?=$classLevelId ?>" id="AppliedBeforeClass<?=$classLevelId ?>" name="AppliedBeforeClass[]" <?=$checked ?> />
        			<label for="AppliedBeforeClass<?=$classLevelId ?>" style="margin-right: 10px;"><?=$classLevel ?></label>
				<?php
				endforeach;
				?>
			</div>
		<?php } ?>
	</td>
</tr>
</table>

<script>

$(function(){
	'use strict';
	
	function updateUI(){
		var addressDistrict = $('#AddressDistrict').val();
		$('[name="AddressEstate"]').hide().prop('disabled', true);
		$('#AddressEstate' + addressDistrict).show().prop('disabled', false);

		if($('[name="AppliedBefore"]:checked').val() == '1'){
			$('#AppliedBeforeClassDiv').show().find('input').prop('disabled', false);
		}else{
			$('#AppliedBeforeClassDiv').hide().find('input').prop('disabled', true);
		}
	}
	
	$('[name="AppliedBefore"]').change(updateUI);
	$('#AddressDistrict').change(updateUI);
	updateUI();
});
</script>