﻿<?php
// modifying by:
/**
 * Change Log:
 * 2020-04-15 Sam
 *  - Hide unselected item for session preference   
 * 
 * 2020-03-27 Sam
 *  - Fill in dynamic fields 
 *
 * 2020-03-26 Pun
 *  - File created
 */

//error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);

######## Image START ########
$imgs = array(
    'checkbox',
    'checked',
    'joyful_logo',
    'photo'
);
foreach ($imgs as $img) {
    $$img = "{$baseFilePath}/images/{$img}.png";
}
######## Image END ########

######## Get data START ########
#### Get student info START ####
$StudentInfo = current($lac->getApplicationStudentInfo($schoolYearID, '', $applicationID));
$homeAddrDistrict = $StudentInfo['AddressDistrict'];
$homeAddrRegion = $StudentInfo['Address'];

## Photo START ##
$attachmentArr = $lac->getAttachmentByApplicationID($schoolYearID, $applicationID);
$photoLink = $attachmentArr['personal_photo']['link'];
$photoLink = ($photoLink) ? $photoLink : $photo;
## Photo END ##

#### Get student info END ####

#### Get parent info START ####
$tmpParentInfoArr = $lac->getApplicationParentInfo($schoolYearID, '', $applicationID);
$parentInfoArr = array();
foreach ($tmpParentInfoArr as $parent) {
    foreach ($parent as $para => $info) {
        $ParentInfo[$parent['type']][$para] = $info;
    }
}
$MotherInfo = $ParentInfo['M'];
$FatherInfo = $ParentInfo['F'];
#### Get parent info END ####

#### Get prev school info START ####
$StudentPrevSchoolInfo = $lac->getApplicationPrevSchoolInfo($schoolYearID, '', $applicationID);
#### Get prev school info END ####

#### Get sibling info START ####
$SiblingInfo = $lac->getApplicationSibling($schoolYearID, '', $applicationID);
#### Get sibling info END ####

#### Get other info START ####
$OtherInfo = current($lac->getApplicationOthersInfo($schoolYearID, '', $applicationID));
$classLevelName = $allClassLevel[$OtherInfo['classLevelID']];
$applyDayType = array($OtherInfo['ApplyDayType1'], $OtherInfo['ApplyDayType2'], $OtherInfo['ApplyDayType3']);
#### Get other info END ####

$applicationSetting = $lac->getApplicationSetting($schoolYearID);
$dayType = $applicationSetting[$OtherInfo['classLevelID']]['DayType'];
$numberOfDayType = count(explode(',',$dayType));

#### Get cust info START ####
$campus = $lac->getApplicationCustInfo($applicationID, 'Campus');
$entryDate = date_format(date_create($lac->getApplicationCustInfo($applicationID, 'EntryDate', true)['Value']), 'd/m/Y');
$birthCertNo = $lac->getApplicationCustInfo($applicationID, 'BirthCertNo', true)['Value'];
$visa = $lac->getApplicationCustInfo($applicationID, 'Visa', true)['Value'];
$visa = $StudentInfo['BirthCertType'] == $admission_cfg['BirthCertType']['passport'] ? date_format(date_create($visa), 'd/m/Y') : '';
$mailAddrFlat = $lac->getApplicationCustInfo($applicationID, 'MailAddr_Flat', true)['Value'];
$mailAddrFloor = $lac->getApplicationCustInfo($applicationID, 'MailAddr_Floor', true)['Value'];
$mailAddrBlock = $lac->getApplicationCustInfo($applicationID, 'MailAddr_Block', true)['Value'];
$mailAddrEstate = $lac->getApplicationCustInfo($applicationID, 'MailAddr_Estate', true)['Value'];
$mailAddrStreet = $lac->getApplicationCustInfo($applicationID, 'MailAddr_Street', true)['Value'];
$mailAddrDistrict = $lac->getApplicationCustInfo($applicationID, 'MailAddr_District', true)['Value'];
$mailAddrRegion = $lac->getApplicationCustInfo($applicationID, 'MailAddr_Region', true)['Value'];
$motherContactPriority = $lac->getApplicationCustInfo($applicationID, 'M_ContactPriority', true)['Value'];
$fatherContactPriority = $lac->getApplicationCustInfo($applicationID, 'F_ContactPriority', true)['Value'];
$motherReligion = $lac->getApplicationCustInfo($applicationID, 'M_Religion', true)['Value'];
$fatherReligion = $lac->getApplicationCustInfo($applicationID, 'F_Religion', true)['Value'];
$motherOtherLang = $lac->getApplicationCustInfo($applicationID, 'M_OtherLang', true)['Value'];
$fatherOtherLang = $lac->getApplicationCustInfo($applicationID, 'F_OtherLang', true)['Value'];
$previouslyAttended = $lac->getApplicationCustInfo($applicationID, 'PreviouslyAttended', true)['Value'];
$preSchoolExperience = $lac->getApplicationCustInfo($applicationID, 'PreSchoolExperience', true)['Value'];
$siblingPosition = $lac->getApplicationCustInfo($applicationID, 'SiblingPosition', true)['Value'];
$numOfSibling = $lac->getApplicationCustInfo($applicationID, 'NumOfSibling', true)['Value'];
$physicalConcern = $lac->getApplicationCustInfo($applicationID, 'PhysicalConcern', true)['Value'];
$anyAllergies = $lac->getApplicationCustInfo($applicationID, 'AnyAllergies', true)['Value'];
$takeMedication = $lac->getApplicationCustInfo($applicationID, 'TakeMedication', true)['Value'];
$specialNeeds = $lac->getApplicationCustInfo($applicationID, 'SpecialNeeds', true)['Value'];
$whyWishToEnroll = $lac->getApplicationCustInfo($applicationID, 'WhyWishToEnroll', true)['Value'];
$expectationFromSchool = $lac->getApplicationCustInfo($applicationID, 'ExpectationFromSchool', true)['Value'];
$howInvolve = $lac->getApplicationCustInfo($applicationID, 'HowInvolve', true)['Value'];
$whyChoose = $lac->getApplicationCustInfo($applicationID, 'WhyChoose', true)['Value'];
$majorityTime = $lac->getApplicationCustInfo($applicationID, 'MajorityTime', true)['Value'];
$mainHomeLang = $lac->getApplicationCustInfo($applicationID, 'MainHomeLang', true)['Value'];
$howToKnow = $lac->getApplicationCustInfo($applicationID, 'HowToKnow', true)['Value'];
$schoolBus = $lac->getApplicationCustInfo($applicationID, 'SchoolBus', true)['Value'];
$contactName1 = $lac->getApplicationCustInfo($applicationID, 'ContactName1', true)['Value'];
$contactName2 = $lac->getApplicationCustInfo($applicationID, 'ContactName2', true)['Value'];
$contactRelationship1 = $lac->getApplicationCustInfo($applicationID, 'ContactRelationship1', true)['Value'];
$contactRelationship2 = $lac->getApplicationCustInfo($applicationID, 'ContactRelationship2', true)['Value'];
$contactNumber1 = $lac->getApplicationCustInfo($applicationID, 'ContactNumber1', true)['Value'];
$contactNumber2 = $lac->getApplicationCustInfo($applicationID, 'ContactNumber2', true)['Value'];
#### Get cust info END ####

if ($sys_custom['KIS_Admission']['PayPal']) {
    $paypalPaymentInfo = $lac->getPaymentResult('', '', '', $schoolYearID, $applicationID);
}
######## Get data END ########

######## Value to Display mapping START ########
$campusMap = array(
    'BD' => 'Belvedere Kindergarten 麗城校園',
    'TK' => 'Tsuen King Kindergarten 荃景校園',
    'ST' => 'Sha Tin Kindergarten 沙田校園',
    'YL' => 'Yuen Long Kindergarten 元朗校園',
    'TM' => 'Tuen Mun Kindergarten 屯門校園'
);

$applyDayMap = array(
    1 => 'AM Class 上午班',
    2 => 'PM Class 下午班',
    3 => 'AM class: 9:00am - 12:00pm, PM class: 1:30 - 4:30pm',
);

$districtMap = array(
    'Central and Western 中西區',
    'Eastern 東區',
    'Southern 南區',
    'Wan Chai 灣仔',
    'Sham Shui Po 深水埗',
    'Kowloon City 九龍城',
    'Kwun Tong 觀塘',
    'Wong Tai Sin 黃大仙',
    'Yau Tsim Mong 油尖旺',
    'Islands 離島區',
    'Kwai Tsing 葵青',
    'North 北區',
    'Sai Kung 西貢',
    'Sha Tin 沙田',
    'Tai Po 大埔',
    'Tsuen Wan 荃灣',
    'Tuen Mun 屯門',
    'Yuen Long 元朗'
);

$regionMap = array(
    'Hong Kong Island 香港島',
    'Kowloon 九龍',
    'New Territories 新界',
    'Islands 離島區'
);

$maritalMap = array(
    'S' => 'Single 單身',
    'M' => 'Married 已婚',
    'D' => 'Divorced 離婚',
    'E' => 'Separated 分居',
    'W' => 'Widowed 寡居',
);

$qualificationMap = array(
    'Postgraduate' => 'Postgraduate 碩士',
    'University' => 'University 大學',
    'Secondary' => 'Secondary 中學',
    'Primary' => 'Primary 小學',
    'Others' => 'Others 其他',
);

$whyChooseMap = array();
$whyChooseOpts = array('Location', 'Professionalism', 'WordOfMouth', 'Environment', 'Culture', 'Language', 'Curriculum', 'Educational');
foreach(preg_split('/\s*,\s*/', $whyChoose) as $choice) {
    if(in_array($choice, $whyChooseOpts))
        $whyChooseMap[$choice] = $choice;
    else
        $whyChooseMap['Others'] = $choice;
}

$majorityTimeMap = array();
$majorityTimeOpts = array('Mum', 'Dad', 'Grandparents', 'Guardian', 'Helper');
foreach(preg_split('/\s*,\s*/', $majorityTime) as $choice) {
    if(in_array($choice, $majorityTimeOpts))
        $majorityTimeMap[$choice] = $choice;
    else
        $majorityTimeMap['Others'] = $choice;
}

$mainHomeLangMap = array();
$mainHomeLangOpts = array('English', 'Putonghua', 'Cantonese');
foreach(preg_split('/\s*,\s*/', $mainHomeLang) as $choice) {
    if(in_array($choice, $mainHomeLangOpts))
        $mainHomeLangMap[$choice] = $choice;
    else
        $mainHomeLangMap['Others'] = $choice;
}

$howToKnowMap = array();
$howToKnowOpts = array('Website', 'Referred', 'Facebook', 'Search');
foreach(preg_split('/\s*,\s*/', $howToKnow) as $choice) {
    if(in_array($choice, $howToKnowOpts))
        $howToKnowMap[$choice] = $choice;
    else
        $howToKnowMap['Others'] = $choice;
}
######## Value to Display mapping END ########
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Joyful World International Nursery &amp; Kindergarten - Application Form 入學申請表</title>
</head>
<style>
@media print {
	html, body {width: 210mm;}
	.page-break	{display: block; height: 15mm; page-break-before: always; }
}
@page {size: A4 portrait; margin: 0mm 0 0 0;}
@-moz-document url-prefix(){ @page {margin:-3mm 0 0 -8mm;} td, li {line-height: 1em;}}

body {background-color: #fff; color:#000; font-family: "微軟正黑體", serif; font-size: 16px; line-height: 1.2em; margin:0;  padding:0;}

#content {background-color: #fff; margin: 0 auto; width: 210mm;}
div.page_wrapper {padding: 10mm;}

p {line-height: 1.3em; margin: 0; padding: 0;}
tr.control td {height: 0; line-height: 0; padding: 0;}
table {border-collapse: separate; border-spacing: 0; padding: 0; table-layout: fixed;} /*for IE*/
th:last-child {border-right: 1px solid #000;}
td.last-child {border-right: 1px solid #000;}
.checkbox {height: 3mm; margin-bottom: 0.5mm; vertical-align: middle; width: 3mm;}

/* Header start */
.header {width: 190mm;}
.header_ctrl {height: 0; padding: 0; margin: 0;}
.schLogo {vertical-align: top;}
.schLogo img {height: 15mm; width: auto;}
.barcode {height: 15mm; text-align: right; vertical-align: top;}
.barcode img {height: 10mm; width: auto;}
p.appNo {font-size: 0.75em; line-height: 0.75em; text-align: right;}
td.title {font-size: 1.125em; font-weight: bold; line-height: 1.125em; padding-bottom: 3mm; padding-top: 8mm; text-align: center;}
p.info {font-size: 0.75em; line-height: 1.5em;}
/* Header end */

.subtitle {font-size: 1em; font-weight: bold; line-height: 1.6em;}

/* Level and class start */
.tbl_lvClass {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; width: 190mm;}
.tbl_lvClass p {font-weight: bold; margin: 0;}
.tbl_lvClass td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 0.5mm 1mm;}
.tbl_lvClass td:last-child {border-right: 1px solid #000;}
.lvClass_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
.lvClass_check td {border-top: 0;}
.lvClass_check td:last-child {border-left: 0;}
.lvClass_check .checkbox {margin-top: 1mm;}
/* Level and class end */
	
/* Student info start */
.tbl_std {border: 1px solid #000; border-bottom: 0; font-size: 0.75em; line-height: 1.3em; margin-top: 1mm; width: 190mm;}
.tbl_std p {font-weight: bold; margin: 0;}
.tbl_std td {padding: 1mm;}
.std_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_std .std_fill {border-bottom: 1px solid #000; min-height: 8mm;}
.tbl_std .std_remark {height: 5mm;}
.tbl_std .photo {padding: 1.5mm 1mm; text-align: center; vertical-align: top;}
.photo img {height: auto; max-height: 49mm; max-width: 40mm; width: auto;}

.tbl_std2 {border: 1px solid #000; border-top: 0; font-size: 0.75em; line-height: 1.3em; margin-bottom: 5mm; padding: 0 0 2mm; width: 190mm;}
.tbl_std2 p {font-weight: bold; margin: 0;}
.tbl_std2 td {padding: 1mm;}
.std2_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_std2 .std2_fill {border-bottom: 1px solid #000; min-height: 8mm;}
/* Student info end */

/* Address start */
.tbl_address {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 3mm; margin-top: 1mm; width: 190mm;}
.tbl_address p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_address td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm;}
.tbl_address td:last-child {border-right: 1px solid #000;}
.address_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}

.tbl_mailAddress {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; width: 190mm;}
.tbl_mailAddress p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_mailAddress td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm;}
.tbl_mailAddress td:last-child {border-right: 1px solid #000;}
.mailAddress_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
/* Address end */

/* Parent start */
.tbl_parent {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; width: 190mm;}
.tbl_parent p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_parent td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm;}
.tbl_parent td:last-child {border-right: 1px solid #000;}
.parent_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
/* Parent end */

/* Other personal information start */
.tbl_other {font-size: 0.75em; line-height: 1.5em; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_other p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_other td {padding: 1mm; vertical-align: top;}
.other_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_other .other_fill {border-bottom: 1px solid #000; line-height: 1.3em; min-height: 8mm; vertical-align: middle;}
.other_space {height: 2mm;}
	
.tbl_preSch {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.3em; margin: 0 0 3mm 5mm; width: 185mm;}
.tbl_preSch p {font-weight: bold; margin: 0;}
.tbl_preSch td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm; text-align: center;}
.tbl_preSch td:last-child {border-right: 1px solid #000;}
.preSch_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}

.tbl_other2 {font-size: 0.75em; line-height: 1.5em; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_other2 p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_other2 td {padding: 1mm; vertical-align: top;}
.other2_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_other2 .other2_fill {border-bottom: 1px solid #000; line-height: 1.3em; min-height: 8mm; text-align: center; vertical-align: middle;}
	
.tbl_sibling {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.3em; margin: 2mm 0 5mm 5mm; width: 185mm;}
.tbl_sibling p {font-weight: bold; margin: 0;}
.tbl_sibling td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm; text-align: center;}
.tbl_sibling td:last-child {border-right: 1px solid #000;}
.sibling_ctrl td {border: 0; height: 0; padding: 0; margin: 0; padding: 0;}
.tbl_sibling .label {text-align: left;}
/* Other personal information end */

/* Health condition start */
.tbl_health {font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_health p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_health td {padding: 0.5mm 1mm;}
.tbl_health .health_no {vertical-align: top;}
.health_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_health .health_ans {line-height: 1.3em; height: 5mm; vertical-align: bottom;}
.tbl_health .health_fill {border-bottom: 1px solid #000; line-height: 1.3em; height: 5mm;}
.health_space {height: 2mm;}
/* Health condition end */

/* Parental expectations start */
.tbl_expect {font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_expect p {font-weight: bold; line-height: 1.3em; margin: 0;}
.tbl_expect td {padding: 0.5mm 1mm; vertical-align: top;}
.expect_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_expect .expect_no {vertical-align: top;}
.tbl_expect .expect_fill {border-bottom: 1px solid #000; line-height: 1.2em; height: 6mm; vertical-align: middle;}
.expect_space {height: 2mm;}
/* Parental expectations end */

/* Questionnaire start */
.tbl_q {font-size: 0.75em; line-height: 1.5em; margin-bottom: 5mm; margin-top: 1mm; padding: 1mm 0 0; width: 190mm;}
.tbl_q p {font-weight: bold; line-height: 1.3em; margin: 0; padding-bottom: 1mm;}
.tbl_q td {padding: 0.5mm 1mm;}
.q_ctrl td {height: 0; padding: 0; margin: 0;}
.tbl_q .q_no {vertical-align: top;}
.tbl_q .q_checkbox {height: 5mm;}
.tbl_q .checkbox {margin-top: 0.5mm;}
.tbl_q .q_fill {border-bottom: 1px solid #000; line-height: 1.2em; height: 5mm; vertical-align: middle;}
.q_space {height: 4mm;}
/* Questionnaire end */

/* Emergency contact start */
.tbl_contact {border-bottom: 1px solid #000; font-size: 0.75em; line-height: 1.2em; margin-top: 1mm; width: 190mm;}
.tbl_contact p {font-weight: bold; margin: 0;}
.tbl_contact td {border-top: 1px solid #000; border-left: 1px solid #000; padding: 1mm; text-align: center;}
.tbl_contact td:last-child {border-right: 1px solid #000;}
.contact_ctrl td {border: 0; height: 0; padding: 0; margin: 0;}
.tbl_contact .contact_label {text-align: left;}
/* Emergency contact end */
</style>
<body>
<?php if ($applicationIndex > 0) { ?>
    <div style="page-break-before: always;line-height: 0.1mm;height: 0.1mm;font-size: 0.1mm;">&nbsp;</div>
<?php } ?>

<div id="content">
    <div class="page_wrapper">
        <!-- Header starts -->
        <table cellpadding="0" cellspacing="0" class="header">
            <tr>
                <td class="header_ctrl" style="width: 130mm;"></td>
                <td class="header_ctrl" style="width: 60mm;"></td>
            </tr>
            <tr>
                <td class="schLogo" rowspan="2"><img style="height:15mm; width:auto;" src="<?=$joyful_logo?>" alt="logo" /></td>
                <td class="barcode"><img style="height: 10mm; width: auto;" src="barcode.php?barcode=<?=rawurlencode($applicationID)?>" alt="barcode" /></td>
            </tr>
            <tr>
                <td><p class="appNo">Application Number 申請編號: <?=$applicationID?></p></td>
            </tr>
            <tr>
                <td class="title" colspan="2">
                    <p>Online Admission Form 網上入學申請表</p>
                </td>
            </tr>
        </table>
        <!-- Header ends-->
        <!-- Level and class start -->
        <p class="subtitle" colspan="8">Section 1 : School Application 學校申請</p>
        <table cellpadding="0" cellspacing="0" class="tbl_lvClass">
             <tr class="lvClass_ctrl"> 
                <td style="width: 30mm;"></td>
                <td style="width: 50mm;"></td>
                <td style="width: 55mm;"></td>
                <td style="width: 30mm;"></td>
             </tr>
            <tr>
                <td style="padding-top: 7px" valign="top"><p>Campus 校園</p></td>
                <td colspan="3" class="last-child" style="padding-bottom: 5px;">
                	<span>1<sup>st</sup> Preference: <?=$campus[0]['Value'] ? $campusMap[$campus[0]['Value']] : '-'?></span><br />
                    <span>2<sup>nd</sup> Preference: <?=$campus[1]['Value'] ? $campusMap[$campus[1]['Value']] : '-'?></span><br />
                    <span>3<sup>rd</sup> Preference: <?=$campus[2]['Value'] ? $campusMap[$campus[2]['Value']] : '-'?></span>
                </td>
            </tr>
            <tr>
                <td><p>Level 級別</p></td>
                <td colspan="3" class="last-child"><span><?=$classLevelName?></span></td>
            </tr>
            <tr>
                <td style="padding-top: 7px" valign="top"><p>Session 上課時段</p></td>
                <td colspan="3" class="last-child" style="padding-bottom: 5px;">
                	<?if($numberOfDayType > 0){?>
                	<span style="<?=!$applyDayType[0] ? 'display:none' :''?>">1<sup>st</sup> Preference: <?=$applyDayType[0] ? $applyDayMap[$applyDayType[0]] : '-'?></span>
                	<?} if($numberOfDayType > 1){?>
                	<br /><span style="<?=!$applyDayType[1] ? 'display:none' :''?>">2<sup>nd</sup> Preference: <?=$applyDayType[1] ? $applyDayMap[$applyDayType[1]] : '-'?></span>
                	<?} if($numberOfDayType > 2){?>
                	<br /><span style="<?=!$applyDayType[2] ? 'display:none' :''?>">3<sup>rd</sup> Preference: <?=$applyDayType[2] ? $applyDayMap[$applyDayType[2]] : '-'?></span>
                	<?} ?>
                </td>
            </tr>
            <tr>
                <td><p>School Year 學年</p></td>
                <td><span><?=getAYNameByAyId($schoolYearID)?></span></td>
                <td><p>Intended Entry Date 擬入學日期 (DD/MM/YYYY)</p></td>
                <td class="last-child"><span><?=$entryDate?></span></td>
            </tr>
        </table>
        <!-- Level and class end -->

        <!-- Applicant information start -->
        <p class="subtitle">Section 2 : Personal Details of Student 學生的個人資料</p>
        <table cellpadding="0" cellspacing="0" class="tbl_std">
            <tr class="std_ctrl">
                <td style="width: 33mm;"></td>
                <td style="width: 5mm;"></td>
                <td style="width: 13mm;"></td>
                <td style="width: 5mm;"></td>
                <td style="width: 14mm;"></td>
                <td style="width: 2mm;"></td>
                <td style="width: 30mm;"></td>
                <td style="width: 20mm;"></td>
                <td style="width: 25mm;"></td>
                <td style="width: 43mm;"></td>
            </tr>
            <tr>
                <td colspan="9"><p>Name in English 英文姓名 (same as HKID or passport)</p></td>
                <td class="photo" rowspan="7"><img style="height:auto; max-height:49mm; max-width:40mm; width:auto;" src="<?=$photoLink?>" alt="photo" /></td>
            </tr>
            <tr>
                <td><p>Family Name<br />姓氏</p></td>
                <td class="std_fill" colspan="4"><?=$StudentInfo['EnglishSurname']?></td>
                <td rowspan="6">&nbsp;</td>
                <td><p>Given Name<br />名字</p></td>
                <td colspan="2" class="std_fill"><?=$StudentInfo['EnglishFirstName']?></td>
            </tr>
            <tr>
                <td><p>Preferred Name<br />常用英文姓名</p></td>
                <td class="std_fill" colspan="4"><?=$StudentInfo['EnglishName']?></td>
                <td><p>Chinese Name<br />中文姓名</p></td>
                <td class="std_fill" colspan="2"><?=$StudentInfo['ChineseName']?></td>
            </tr>
            <tr>
                <td><p>Gender<br />性別</p></td>
                <td><img src="<?=$StudentInfo['Gender'] == 'M' ? $checked : $checkbox?>" alt="checkbox" class="checkbox"></td>
                <td>M 男</td>
                <td><img src="<?=$StudentInfo['Gender'] == 'F' ? $checked : $checkbox?>" alt="checkbox" class="checkbox"></td>
                <td>F 女</td>
                <td>
                    <p>Nationality<br />國籍</p>
                </td>
                <td class="std_fill" colspan="2"><?=$StudentInfo['Nationality']?></td>
            </tr>
            <tr>
                <td><p>Date of Birth<br />出生日期 (DD/MM/YYYY)</p></td>
                <td class="std_fill" colspan="4"><?=date_format(date_create($StudentInfo['dateofbirth']), 'd/m/Y')?></td>
                <td><p>Country of Birth<br />出生地點</p></td>
                <td colspan="2" class="std_fill"><?=$StudentInfo['placeofbirth']?></td>
            </tr>
            <tr>
                <td><p>Birth Certificate No.<br />出生證明書號碼</p></td>
                <td class="std_fill" colspan="4"><?=$birthCertNo?></td>
                <td><p>Religion<br />宗教信仰</p></td>
                <td colspan="2" class="std_fill"><?=$StudentInfo['ReligionOther']?></td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="tbl_std2">
            <tr class="std2_ctrl">
                <td style="width: 10mm;"></td>
                <td style="width: 23mm;"></td>
                <td style="width: 10mm;"></td>
                <td style="width: 27mm;"></td>
                <td style="width: 2mm;"></td>
                <td style="width: 18mm;"></td>
                <td style="width: 27mm;"></td>
                <td style="width: 71mm;"></td>
                <td style="width: 2mm;"></td>
            </tr>
            <tr>
                <td colspan="2"><p>First Language<br />母語</p></td>
                <td class="std2_fill" colspan="2"><?=$StudentInfo['LangSpokenAtHome']?></td>
                <td></td>
                <td colspan="2"><p>Other Language(s) Spoken<br />其他語言</p></td>
                <td class="std2_fill"><?=$StudentInfo['LangSpokenOther']?></td>
            </tr>
            <tr>
                <td colspan="9"></td>
            </tr>
            <tr>
                <td colspan="8" style="padding-bottom: 0;"><p>Is your child a Hong Kong permanent resident?<br />貴子女為香港永久居民嗎？</p></td>
            </tr>
            <?php if($StudentInfo['BirthCertType'] == $admission_cfg['BirthCertType']['hkid']):?>
            <tr>
                <td><p>Yes<br/>是</p></td>
                <td colspan="2"><p>Hong Kong ID No.<br/>香港身份證號碼</p></td>
                <td class="std2_fill"><?=$StudentInfo['birthcertno']?></td>
            </tr>
            <?php else:?>
            <tr>
                <td><p>No<br/>否</p></td>
                <td colspan="2"><p>Passport No.<br/>護照號碼</p></td>
                <td class="std2_fill"><?=$StudentInfo['birthcertno']?></td>
                <td></td>
                <td colspan="2"><p>Dependent Visa Expiry Date<br/>受養人簽證到期日 (DD/MM/YYYY)</p></td>
                <td class="std2_fill"><?=$visa?></td>
            </tr>
            <?php endif;?>
        </table>
        <!-- Applicant information end -->

        <!-- Address starts -->
        <p class="subtitle">Section 3 : Address 地址</p>
        <p class="info">Home Address 住址</p>
        <table cellpadding="0" cellspacing="0" class="tbl_address">
            <tr class="address_ctrl">
                <td style="width: 30mm;"></td>
                <td style="width: 37mm;"></td>
                <td style="width: 30mm;"></td>
                <td style="width: 33mm;"></td>
                <td style="width: 30mm;"></td>
                <td style="width: 30mm;"></td>
            </tr>
            <tr>
                <td><p>Flat 單位</p></td>
                <td><?=$StudentInfo['AddressRoom']?></td>
                <td><p>Floor 樓層</p></td>
                <td><?=$StudentInfo['AddressFloor']?></td>
                <td><p>Block 座數</p></td>
                <td class="last-child"><?=$StudentInfo['AddressBlock']?></td>
            </tr>
            <tr>
                <td colspan="2"><p>Name of Building / Estate 大廈 / 屋苑名稱</p></td>
                <td colspan="4" class="last-child"><?=$StudentInfo['AddressEstate']?></td>
            </tr>
            <tr>
                <td colspan="2"><p>Number and Name of Street (or Village)<br/>門牌號數及街道 (或鄉村) 名稱</p></td>
                <td colspan="4" class="last-child"><?=$StudentInfo['AddressStreet']?></td>
            </tr>
            <tr>
                <td><p>District 地區</p></td>
                <td colspan="2"><?=$districtMap[$homeAddrDistrict];?></td>
                <td><p>Region 地域</p></td>
                <td colspan="2" class="last-child"><?=$regionMap[$homeAddrRegion]?></td>
            </tr>
        </table>
        <p class="info">Mailing Address (if different from home address) 郵寄地址（如與住址不同）</p>
        <table cellpadding="0" cellspacing="0" class="tbl_mailAddress">
            <tr class="mailAddress_ctrl">
                <td style="width: 30mm;"></td>
                <td style="width: 37mm;"></td>
                <td style="width: 30mm;"></td>
                <td style="width: 33mm;"></td>
                <td style="width: 30mm;"></td>
                <td style="width: 30mm;"></td>
            </tr>
            <tr>
            	<td colspan="2"><p>Flat / Floor / Block 單位 / 樓層 / 座數</p></td>
                <td colspan="4" class="last-child"><?=$mailAddrFlat?></td>
            </tr>
            <tr>
                <td colspan="2"><p>Name of Building / Estate 大廈 / 屋苑名稱</p></td>
                <td colspan="4" class="last-child"><?=$mailAddrEstate?></td>
            </tr>
            <tr>
                <td colspan="2"><p>Number and Name of Street (or Village)<br/>門牌號數及街道 (或鄉村) 名稱</p></td>
                <td colspan="4" class="last-child"><?=$mailAddrStreet?></td>
            </tr>
            <tr>
                <td><p>District 地區</p></td>
                <td colspan="2"><?=$districtMap[$mailAddrDistrict];?></td>
                <td><p>Region 地域</p></td>
                <td colspan="2" class="last-child"><?=$regionMap[$mailAddrRegion]?></td>
            </tr>
        </table>
        <!-- Address end -->

        <div class="page-break"></div>

        <!-- Parent Info starts -->
        <p class="subtitle">Section 4 : Parent / Guardian Information 家長 / 監護人資料</p>
        <table cellpadding="0" cellspacing="0" class="tbl_mailAddress">
            <tr class="mailAddress_ctrl">
                <td style="width: 50mm;"></td>
                <td style="width: 70mm;"></td>
                <td style="width: 70mm;"></td>
            </tr>
            <tr>
                <td><p>Parent Information</p></td>
                <td align="center"><p>Mother / Guardian<br />母親 / 監護人</p></td>
                <td align="center" class="last-child"><p>Father / Guardian<br />父親 / 監護人</p></td>
            </tr>
            <tr>
                <td><p>English Name 英文姓名<br />(same as HKID or passport)</p></td>
                <td><?=$MotherInfo['EnglishName']?></td>
                <td class="last-child"><?=$FatherInfo['EnglishName']?></td>
            </tr>
            <tr>
                <td><p>Chinese Name 中文姓名</p></td>
                <td><?=$MotherInfo['ChineseName']?></td>
                <td class="last-child"><?=$FatherInfo['ChineseName']?></td>
            </tr>
            <tr>
                <td><p>Marital Status 婚姻狀況</p></td>
                <td><?=$maritalMap[$MotherInfo['MaritalStatus']]?></td>
                <td class="last-child"><?=$maritalMap[$FatherInfo['MaritalStatus']]?></td>
            </tr>
            <tr>
                <td><p>Contact Priority 聯絡次序</p></td>
                <td><?=$motherContactPriority?></td>
                <td class="last-child"><?=$fatherContactPriority?></td>
            </tr>
            <tr>
                <td><p>Nationality 國籍</p></td>
                <td><?=$MotherInfo['Nationality']?></td>
                <td class="last-child"><?=$FatherInfo['Nationality']?></td>
            </tr>
            <tr>
                <td><p>HKID Card / Passport No.<br />香港身份證/護照號碼</p></td>
                <td><?=$MotherInfo['HKID']?></td>
                <td class="last-child"><?=$FatherInfo['HKID']?></td>
            </tr>
            <tr>
                <td><p>Religion 宗教信仰</p></td>
                <td><?=$motherReligion?></td>
                <td class="last-child"><?=$fatherReligion?></td>
            </tr>
            <tr>
                <td><p>First Language 母語</p></td>
                <td><?=$MotherInfo['NativeLanguage']?></td>
                <td class="last-child"><?=$FatherInfo['NativeLanguage']?></td>
            </tr>
            <tr>
                <td><p>Other Language(s) Spoken<br />其他語言</p></td>
                <td><?=$motherOtherLang?></td>
                <td class="last-child"><?=$fatherOtherLang?></td>
            </tr>
            <tr>
                <td><p>Academic Qualification<br />學歷程度</p></td>
                <td><?=$qualificationMap[$MotherInfo['LevelOfEducation']]?></td>
                <td class="last-child"><?=$qualificationMap[$FatherInfo['LevelOfEducation']]?></td>
            </tr>
            <tr>
                <td><p>Occupation 職業</p></td>
                <td><?=$MotherInfo['occupation']?></td>
                <td class="last-child"><?=$FatherInfo['occupation']?></td>
            </tr>
            <tr>
                <td><p>Company Name 公司名稱</p></td>
                <td><?=$MotherInfo['Company']?></td>
                <td class="last-child"><?=$FatherInfo['Company']?></td>
            </tr>
            <tr>
                <td><p>Company Address 公司地址</p></td>
                <td><?=$MotherInfo['OfficeAddress']?></td>
                <td class="last-child"><?=$FatherInfo['OfficeAddress']?></td>
            </tr>
            <tr>
                <td><p>Mobile No. 手提電話</p></td>
                <td><?=$MotherInfo['Mobile']?></td>
                <td class="last-child"><?=$FatherInfo['Mobile']?></td>
            </tr>
            <tr>
                <td><p>Home Phone No. 住宅電話號碼</p></td>
                <td><?=$MotherInfo['OfficeTelNo']?></td>
                <td class="last-child"><?=$FatherInfo['OfficeTelNo']?></td>
            </tr>
            <tr>
                <td><p>Email 電郵<br/>(Communication with the school office will mainly be in the form of email. Kindly ensure your email address is correct. 本校將以電郵作主要聯絡方法。請確保您的電郵輸入正確。)</p></td>
                <td><?=$MotherInfo['Email']?></td>
                <td class="last-child"><?=$FatherInfo['Email']?></td>
            </tr>
        </table>
        <!-- Parent Info end -->

        <div class="page-break"></div>

        <!-- Other personal information starts -->
        <p class="subtitle">Section 5 : Other Personal Information 其他個人資料</p>
        <table cellpadding="0" cellspacing="0" class="tbl_other">
            <tr class="other_ctrl">
                <td style="width: 5mm;"></td>
                <td style="width: 15mm;"></td>
                <td style="width: 30mm;"></td>
                <td style="width: 50mm;"></td>
                <td style="width: 5mm;"></td>
                <td style="width: 20mm;"></td>
                <td style="width: 20mm;"></td>
                <td style="width: 45mm;"></td>
            </tr>
            <tr>
                <td><p>1.</p></td>
                <td colspan="9">
                    <p>Has the applicant previously attended / applied for admission to Joyful World?<br/>幼兒是否曾就讀 / 申請入讀心怡幼稚園?</p>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <?php if(substr($previouslyAttended, 0, 2) == 'N;'):?>
                <td><p>No 沒有</p></td>
                <?php else:?>
                <td><p>Yes 有</p></td>
                <td><p>Class Level 級別</p></td>
                <?php $previouslyAttendedSplits = explode(';', $previouslyAttended);?>
                <td class="other_fill"><?=$previouslyAttendedSplits[1]?></td>
                <td>&nbsp;</td>
                <td><p>Year 年份</p></td>
                <td class="other_fill"><?=$previouslyAttendedSplits[2]?></td>
                <?php endif;?>
                <td>&nbsp;</td>
            </tr>
            <tr class="other_space">
                <td colspan="8"></td>
            </tr>
            <tr>
                <td><p>2.</p></td>
                <td colspan="9">
                    <p>Has the applicant previously attended any other playgroup or kindergarten?<br/>幼兒是否曾修讀任何親子班或幼稚園課程?</p>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <?php if($preSchoolExperience == 'Y'):?>
                <td><p>Yes 是</p></td>
                <?php else:?>
                <td><p>No 沒有</p></td>
                <?php endif;?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <?php if($preSchoolExperience == 'Y'):?>
        <table cellpadding="0" cellspacing="0" class="tbl_preSch">
            <tr class="preSch_ctrl">
                <td style="width: 70mm;"></td>
                <td style="width: 40mm;"></td>
                <td style="width: 35mm;"></td>
                <td style="width: 40mm;"></td>
            </tr>
            <tr>
                <td><p>Name of School<br />學校名稱</p></td>
                <td><p>Location of School<br />學校地區</p></td>
                <td><p>Level / Grade<br />班級</p></td>
                <td class="last-child"><p>Period of Stay<br />就讀時期</p></td>
            </tr>
            <?php for($i=0; $i<3; $i++):?>
            <?php if($i < count($StudentPrevSchoolInfo)):?>
            <tr>
                <td><?=$StudentPrevSchoolInfo[$i]['NameOfSchool']?></td>
                <td><?=$StudentPrevSchoolInfo[$i]['SchoolAddress']?></td>
                <td><?=$StudentPrevSchoolInfo[$i]['Year']?></td> 
                <td class="last-child"><?=date_format(date_create($StudentPrevSchoolInfo[$i]['StartDate']), 'm/Y')?> to <?=date_format(date_create($StudentPrevSchoolInfo[$i]['EndDate']), 'm/Y')?></td>
            </tr>
            <?php else:?>
            <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td class="last-child">-</td>
            </tr>
            <?php endif;?>
            <?php endfor;?>
        </table>
        <?php endif;?>
        <table cellpadding="0" cellspacing="0" class="tbl_other2">
            <tr class="other2_ctrl">
                <td style="width: 5mm;"></td>
                <td style="width: 15mm;"></td>
                <td style="width: 10mm;"></td>
                <td style="width: 5mm;"></td>
                <td style="width: 10mm;"></td>
                <td style="width: 145mm;"></td>
            </tr>
            <tr>
                <td><p>3.</p></td>
                <td colspan="5">
                    <p>Sibling Information 兄弟姊妹資料<br/>Applicant's Position in Family 幼兒在家中的排行</p>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><p>Position</p></td>
                <td class="other2_fill"><?=$siblingPosition?></td>
                <td>of</td>
                <td class="other2_fill"><?=$numOfSibling?></td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="tbl_sibling">
            <tr class="sibling_ctrl">
                <td style="width: 38mm;"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><p>Sibling<br />兄弟姊妹: 1</p></td>
                <td><p>Sibling<br />兄弟姊妹: 2</p></td>
                <td><p>Sibling<br />兄弟姊妹: 3</p></td>
                <td><p>Sibling<br />兄弟姊妹: 4</p></td>
                <td class="last-child"><p>Sibling<br />兄弟姊妹: 5</p></td>
            </tr>
            <tr>
                <td class="label"><p>English Name 英文姓名<br />(same as HKID or passport)</p></td>
                <?php
                    if(!$numOfSibling || $numOfSibling == 1){
                        $j = 0;
                    } else {
                        $j = $numOfSibling-1;
                    }
                    for($i=0; $i<5; $i++):
                ?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>"><?=$i<$j ? $SiblingInfo[$i]['EnglishName'] : '-'?></td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
            <tr>
                <td class="label"><p>Age 年齡</p></td>
                <?php for($i=0; $i<5; $i++):?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>"><?=$i<$j ? $SiblingInfo[$i]['Age'] : '-'?></td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
            <tr>
                <td class="label"><p>Birth Certificate / Passport No.<br />出生證明書/護照號碼</p></td>
                <?php for($i=0; $i<5; $i++):?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>"><?=$i<$j ? $SiblingInfo[$i]['BirthCertNo'] : '-'?></td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
            <tr>
                <td class="label"><p>Current School<br />現就讀學校</p></td>
                <?php for($i=0; $i<5; $i++):?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>"><?=$i<$j ? $SiblingInfo[$i]['CurrentSchool'] : '-'?></td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
            <tr>
                <td class="label"><p>Current Grade(s) Attending<br />就讀班級</p></td>
                <?php for($i=0; $i<5; $i++):?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>"><?=$i<$j ? $SiblingInfo[$i]['Year'] : '-'?></td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
            <tr>
                <td class="label"><p>Graduated from K3 at Joyful World?<br />於心怡天地高班畢業?</p></td>
                <?php for($i=0; $i<5; $i++):?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">
                	<?php if($i>=$j):?>
                	-
                	<?php elseif($SiblingInfo[$i]['GraduateSameSchool']==''):?>
                	<p>No 沒有</p>
                	<?php else:?>
                	<p>Yes 有<br />Year attended<br />就讀年份</p><?=$SiblingInfo[$i]['GraduateSameSchool']?>
                	<?php endif;?>
                </td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
            <tr>
                <td class="label"><p>Currently Applying to Joyful World?<br />一同報讀心怡天地?</p></td>
                <?php for($i=0; $i<5; $i++):?>
                <?php if($i<count($SiblingInfo)):?>
                <td class="<?= $i==4 ? 'last-child' : ''?>"><?=$i<$j ? ($SiblingInfo[$i]['ApplyingSameSchool'] == 'N' ? 'No 沒有' : 'Yes 有') : '-'?></td>
                <?php else:?>
                <td class="<?= $i==4 ? 'last-child' : ''?>">-</td>
                <?php endif;?>
                <?php endfor;?>
            </tr>
        </table>
        <!-- Other personal information end -->

        <div class="page-break"></div>

        <!-- Health condition starts -->
        <p class="subtitle">Section 6 : Health Condition 健康情況</p>
        <table cellpadding="0" cellspacing="0" class="tbl_health">
            <tr class="health_ctrl">
                <td style="width: 5mm;"></td>
                <td style="width: 45mm;"></td>
                <td style="width: 135mm;"></td>
            </tr>
            <tr>
                <td class="health_no"><p>1.</p></td>
                <td colspan="3">
                    <p>Does the applicant have any health or physical concern?<br/>幼兒是否有任何健康或身體上的特別關注?</p>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="health_ans" colspan="2"><p><?=substr($physicalConcern, 0, 2) == 'Y;' ? 'Yes 有' : 'No 沒有'?></p></td>
            </tr>
            <?php if(substr($physicalConcern, 0, 2) == 'Y;'):?>
            <tr>
                <td></td>
                <td><p>Please specify. 請明確說明。</p></td>
                <td class="health_fill"><?=substr($physicalConcern, 2)?></td>
            </tr>
            <?php endif;?>
            <tr class="health_space"><td colspan="4"></td></tr>
            <tr>
                <td class="health_no"><p>2.</p></td>
                <td colspan="2">
                    <p>Does the applicant have any allergies?<br/>幼兒是否有任何過敏情況?</p>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="health_ans" colspan="2"><p><?=substr($anyAllergies, 0, 2) == 'Y;' ? 'Yes 有' : 'No 沒有'?></p></td>
            </tr>
            <?php if(substr($anyAllergies, 0, 2) == 'Y;'):?>
            <tr>
                <td></td>
                <td><p>Please specify. 請明確說明。</p></td>
                <td class="health_fill"><?=substr($anyAllergies, 2)?></td>
            </tr>
            <?php endif;?>
            <tr>
                <td class="health_no"><p>3.</p></td>
                <td colspan="2">
                    <p>Does the applicant have to take medication on a regular basis?<br/>幼兒是否需要定時服用藥物?</p>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="health_ans" colspan="2"><p><?=substr($takeMedication, 0, 2) == 'Y;' ? 'Yes 有' : 'No 沒有'?></p></td>
            </tr>
            <?php if(substr($takeMedication, 0, 2) == 'Y;'):?>
            <tr>
                <td></td>
                <td><p>Please specify. 請明確說明。</p></td>
                <td class="health_fill"><?=substr($takeMedication, 2)?></td>
            </tr>
            <?php endif;?>
            <tr class="health_space"><td colspan="4"></td></tr>
            <tr>
                <td class="health_no"><p>4.</p></td>
                <td colspan="2">
                    <p>Does the applicant require special needs education?<br/>幼兒是否有任何特殊教育需要？</p>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="health_ans" colspan="2"><p><?=substr($specialNeeds, 0, 2) == 'Y;' ? 'Yes 有' : 'No 沒有'?></p></td>
            </tr>
            <?php if(substr($specialNeeds, 0, 2) == 'Y;'):?>
            <tr>
                <td></td>
                <td><p>Please specify. 請明確說明。</p></td>
                <td class="health_fill"><?=substr($specialNeeds, 2)?></td>
            </tr>
            <?php endif;?>
            <tr class="health_space"><td colspan="4"></td></tr>
        </table>
        <!-- Health condition end -->

        <!-- Parental expectations starts -->
        <p class="subtitle">Section 7 : Parental Expectations 父母期望</p>
        <table cellpadding="0" cellspacing="0" class="tbl_expect">
            <tr class="expect_ctrl">
                <td style="width: 5mm;"></td>
                <td style="width: 185mm;"></td>
            </tr>
            <tr>
                <td class="expect_no"><p>1.</p></td>
                <td><p>Why do you wish to enroll your child at Joyful World?<br/>為什麼您為幼兒選擇報讀心怡天地?</p></td>
            </tr>
            <tr>
                <td></td>
                <td class="expect_fill"><?=$whyWishToEnroll?></td>
            </tr>
            <tr class="expect_no"><td colspan="3"></td></tr>
            <tr>
                <td class="expect_no"><p>2.</p></td>
                <td><p>What are your expectations from the school?<br/>您對本校有什麼期望?</p></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="expect_fill"><?=$expectationFromSchool?></td>
                <td></td>
            </tr>
            <tr class="health_space"><td colspan="3"></td></tr>
            <tr>
                <td class="expect_no">3.</td>
                <td><p>How involved would you be in your child's education?<br/>您會怎樣參與孩子的學習?</p></td>
            </tr>
            <tr>
                <td></td>
                <td class="expect_fill"><?=$howInvolve?></td>
                <td></td>
            </tr>
        </table>
        <!-- Parental expectations end -->

        <div class="page-break"></div>

        <!-- Questionnaire start -->
        <p class="subtitle">Section 8 : Parents' Questionnaire 家長問卷</p>
        <p class="info">(You may choose more than one option with a &quot;&#10004;&quot; 可作多項選擇並加上 &quot;&#10004;&quot;)</p>
        <table cellpadding="0" cellspacing="0" class="tbl_q">
            <tr class="q_ctrl">
                <td style="width: 5mm;"></td>
                <td style="width: 5mm;"></td>
                <td style="width: 20mm;"></td>
                <td style="width: 38mm;"></td>
                <td style="width: 22mm;"></td>
                <td style="width: 29mm;"></td>
                <td style="width: 11mm;"></td>
                <td style="width: 60mm;"></td>
            </tr>
            <tr>
                <td class="q_no"><p>1.</p></td>
                <td colspan="7"><p>Why would you choose Joyful World International Nursery &amp; Kindergarten for your child?<br/>您為何選擇心怡天地國際幼兒園暨幼稚園？</p></td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Location']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Location 地點</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Professionalism']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Staff Professionalism and Attitude 職員之專業及態度</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['WordOfMouth']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Word of Mouth 口碑</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Environment']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">School Environment 學校環境</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Culture']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Culture and Atmosphere 文化與氣氛</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Language']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Language Environment 語言環境</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Curriculum']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Curriculum 課程</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Educational']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Educational Philosophy 教育理念</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($whyChooseMap['Others']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td>Others 其他</td>
                <td class="q_fill" colspan="2"><?=isset($whyChooseMap['Others']) ? $whyChooseMap['Others'] : ''?></td>
                <td colspan="3"></td>
            </tr>
            <tr class="q_space"><td colspan="8"></td></tr>
            <tr>
                <td class="q_no"><p>2.</p></td>
                <td colspan="7"><p>Who does your child spend the majority of their time with outside of school?<br/>在學校以外，孩子大部份時間由誰陪伴?</p></td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($majorityTimeMap['Mum']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Mum 母親</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($majorityTimeMap['Dad']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Dad 父親</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($majorityTimeMap['Grandparents']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Grandparents (外)祖父/母</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($majorityTimeMap['Guardian']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Guardian 監護人</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($majorityTimeMap['Helper']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Helper 家傭</td>
            </tr>
            <tr class="q_space"><td colspan="8"></td></tr>
            <tr>
                <td class="q_no"><p>3.</p></td>
                <td colspan="6"><p>What is the main language spoken at home?<br/>您在家中與孩子主要使用什麼語言？</p></td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($mainHomeLangMap['English']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">English 英語</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($mainHomeLangMap['Putonghua']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Putonghua 普通話</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($mainHomeLangMap['Cantonese']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Cantonese 廣東話</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($mainHomeLangMap['Others']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="2">Others (Please specify) 其他 (請列明):</td>
                <td class="q_fill" colspan="3"><?=isset($mainHomeLangMap['Others']) ? $mainHomeLangMap['Others'] : ''?></td>
                <td></td>
            </tr>
            <tr class="q_space"><td colspan="8"></td></tr>
            <tr>
                <td class="q_no"><p>4.</p></td>
                <td colspan="6"><p>How did you hear about our school?<br/>您從什麼途徑得悉本校的資料？</p></td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($howToKnowMap['Website']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">School Website 本校網頁</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($howToKnowMap['Referred']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Referred by Friends 朋友推介</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($howToKnowMap['Facebook']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Facebook 面書</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($howToKnowMap['Search']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="6">Search Engines 網上搜尋</td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=isset($howToKnowMap['Others']) ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td colspan="2">Others (Please specify) 其他 (請列明):</td>
                <td class="q_fill" colspan="3"><?=isset($howToKnowMap['Others']) ? $howToKnowMap['Others'] : ''?></td>
                <td></td>
            </tr>
            <tr class="q_space"><td colspan="4"></td></tr>
            <tr>
                <td class="q_no"><p>5.</p></td>
                <td colspan="5"><p>Do you require school bus service?<br/>您是否需要校車服務？</p></td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=substr($schoolBus, 0, 2) == 'Y;' ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td>Yes 需要</td>
                <td colspan="3">Indicate pick up and drop off point 請填寫上車及落車地點:</td>
                <td class="q_fill" colspan="2"><?=substr($schoolBus, 2)?></td>
            </tr>
            <tr class="q_checkbox">
                <td></td>
                <td><img src="<?=substr($schoolBus, 0, 2) == 'N;' ? $checked : $checkbox?>" alt="checked" class="checkbox"></td>
                <td>No 不需要</td>
                <td colspan="5"></td>
            </tr>
        </table>
        <!-- Questionnaire end -->

        <!-- Emergency contact start -->
        <p class="subtitle">Section 9 : Emergency Contact 緊急聯絡</p>
        <p class="info">Please provide the emergency contact of two people other than the parents of the applicant.<br />請提供兩位父母以外的緊急聯絡人資料。</p>
        <table cellpadding="0" cellspacing="0" class="tbl_contact">
            <tr class="contact_ctrl">
                <td style="width: 50mm;"></td>
                <td></td>
            </tr>
            <tr>
                <td class="contact_label"><p>Emergency Contact 緊急聯絡</p></td>
                <td class="last-child"><p>Contact No. 1</p></td>
            </tr>
            <tr>
                <td class="contact_label"><p>Name 姓名</p></td>
                <td class="last-child"><?=$contactName1?></td>
            </tr>
            <tr>
                <td class="contact_label"><p>Relationship 關係</p></td>
                <td class="last-child"><?=$contactRelationship1?></td>
            </tr>
            <tr>
                <td class="contact_label"><p>Contact No. 聯絡電話</p></td>
                <td class="last-child"><?=$contactNumber1?></td>
            </tr>
        </table>
        <!-- Emergency contact end -->
    </div>
</div>
</body>
</html>