<?php
global $json, $admission_cfg;


$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );

#### Get setting START ####
$schoolTypeLangArr = array();
foreach($admission_cfg['SchoolType'] as $type){
	$schoolTypeLangArr[$type] = $kis_lang['Admission']['JOYFUL']['SchoolType'][ $type ];
}

#### Get setting END ####
?>
<span class="itemInput itemInput-selector">
    <span class="selector">
        <select
                id="field_<?= $field['FieldID'] ?>_0"
                name="field_<?= $field['FieldID'] ?>[]"
                data-field="<?= $extraArr['field'] ?>"
        >
            <option value=''>-- <?= $kis_lang['Admission']['JOYFUL']['firstChoice'] ?> --</option>
            <?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                <option
                        value="<?= $type ?>"
			            <?= ( $type == $value[ 0 ] ) ? 'selected' : '' ?>
                >
	                <?= $kis_lang['Admission']['JOYFUL']['SchoolType'][ $type ] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </span>
    <span class="selector">
        <select
                id="field_<?= $field['FieldID'] ?>_1"
                name="field_<?= $field['FieldID'] ?>[]"
                data-field="<?= $extraArr['field'] ?>"
        >
            <option value=''>-- <?= $kis_lang['Admission']['JOYFUL']['secondChoice'] ?> --</option>
            <?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                <option
                        value="<?= $type ?>"
			            <?= ( $type == $value[ 1 ] ) ? 'selected' : '' ?>
                >
	                <?= $kis_lang['Admission']['JOYFUL']['SchoolType'][ $type ] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </span>
    <span class="selector">
        <select
                id="field_<?= $field['FieldID'] ?>_2"
                name="field_<?= $field['FieldID'] ?>[]"
                data-field="<?= $extraArr['field'] ?>"
        >
            <option value=''>-- <?= $kis_lang['Admission']['JOYFUL']['thirdChoice'] ?> --</option>
            <?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                <option value="<?= $type ?>"
	                    <?= ( $type == $value[ 2 ] ) ? 'selected' : '' ?>><?= $kis_lang['Admission']['JOYFUL']['SchoolType'][ $type ] ?></option>
            <?php endforeach; ?>
        </select>
    </span>

    <div style="">
    	<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
	    <div class="remark remark-warn option_repeat hide" style="clear:both;">選擇重覆 Duplicate selection</div>
    	<div class="remark remark-warn custom hide" style="clear:both;"></div>
    </div>
</span>
<script>
  $(function () {
    var slotLangArr = <?=$json->encode( $schoolTypeLangArr ) ?>;
    $('[name^="field_<?=$field['FieldID'] ?>"]').change(updateValue);

    function updateValue() {
      var choices = [];
		<?php for ($i = 0, $iMax = 3; $i < $iMax; $i ++): ?>
      var value = $('#field_<?=$field['FieldID'] ?>_<?=$i ?>').val();

      if (value) {
        choices.push(value);
      }
		<?php endfor; ?>


      var $field = $('#data_<?=$field['FieldID'] ?>');
      if (choices.length) {
        $field.removeClass('dataValue-empty');
      } else {
        $field.addClass('dataValue-empty');
      }

      $field.find('[class^="choice_"]').html('－－');
      $.each(choices, function (index, choice) {
        $field.find('.choice_' + index).html(slotLangArr[choice]);
      });
    }

    updateValue();
  });
</script>
