<?php
global $kis_lang;
$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$hasLabel   = ! ! ( $field['LabelB5'] . $field['LabelEn'] . $field['LabelGb'] );
$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );

#### Get setting START ####
$maxYear = ($extraArr['maxYear'])? $extraArr['maxYear'] : (int)date('Y') + 9;
$minYear = ($extraArr['minYear'])? $extraArr['minYear'] : ((int)date('Y')) - 1;
#### Get setting END ####

$valueArr = explode(';', $value);
$valueArr[2] = explode('-', $valueArr[2]);

?>

<span class="itemInput itemInput-choice">
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_Y"
                name="field_<?= $field['FieldID'] ?>"
                value="Y"
                <?= ($valueArr[0] === 'Y')? 'checked':''?>
        ><label for="field_<?= $field['FieldID'] ?>_Y">
	        <?=$kis_lang['Admission']['yes']?>
        </label>
	</span>
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_N"
                name="field_<?= $field['FieldID'] ?>"
                value="N"
                <?= ($valueArr[0] === 'N')? 'checked':''?>
        ><label for="field_<?= $field['FieldID'] ?>_N">
	        <?=$kis_lang['Admission']['no']?>
        </label>
	</span>
    <div style="">
        <div class="remark remark-warn required1 hide" style="clear:both;">
            <?= Get_Lang_Selection( '必須填寫', 'Required' ) ?>
        </div>
    </div>


    <span class="options_<?= $field['FieldID'] ?>_details Y" style="display: none;margin: 26px 0;">
        <div class="textbox-floatlabel <?= $cssClass ?>">
            <div class="textboxLabel ">
                <?= Get_Lang_Selection( '香港身份證號碼', 'Hong Kong ID No.' ) ?>
            </div>
            <input
                    type="text"
                    id="field_<?= $field['FieldID'] ?>_0"
                    class="field_<?= $field['FieldID'] ?>_option Y textboxtext"
                    name="field_<?= $field['FieldID'] ?>_option[]"
                    value="<?=$valueArr[1]?>"
            />
        </div>

        <div style="">
            <div class="remark remark-warn required2 hide" style="clear:both;">
                <?= Get_Lang_Selection( '必須填寫', 'Required' ) ?>
            </div>
            <div class="remark remark-warn hkid_duplicate hide" style="clear:both;">
                <?= Get_Lang_Selection( '出生證明書號碼已被使用！請輸入其他出生證明書號碼。', 'The Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.' ) ?>
            </div>
            <div class="remark remark-warn hkid email hide" style="clear:both;">
                <?= Get_Lang_Selection( '格式錯誤', 'Invalid format' ) ?>
            </div>
        </div>
    </span>

    <span class="options_<?= $field['FieldID'] ?>_details N" style="display: none;margin: 26px 0;">
        <div class="textbox-floatlabel <?= $cssClass ?>">
            <div class="textboxLabel ">
                <?= Get_Lang_Selection( '護照號碼', 'Passport No.' ) ?>
            </div>
            <input
                    type="text"
                    id="field_<?= $field['FieldID'] ?>_1"
                    class="field_<?= $field['FieldID'] ?>_option N textboxtext"
                    name="field_<?= $field['FieldID'] ?>_option[]"
                    value="<?=$valueArr[1]?>"
            />
        </div>

        <div style="">
            <div class="remark remark-warn required3 hide" style="clear:both;">
                <?= Get_Lang_Selection( '必須填寫', 'Required' ) ?>
            </div>
            <div class="remark remark-warn hkid_duplicate hide" style="clear:both;">
                <?= Get_Lang_Selection( '出生證明書號碼已被使用！請輸入其他出生證明書號碼。', 'The Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.' ) ?>
            </div>
        </div>
    </span>

    <div class="itemLabel options_<?= $field['FieldID'] ?>_details N" style="display: none;">
        <?= Get_Lang_Selection( '受養人簽證到期日', 'Dependent Visa Expiry Date' ) ?>
    </div>
    <span class="options_<?= $field['FieldID'] ?>_details N" style="display: none;">

        <span class="itemInput itemInput-selector" data-min-date="" data-max-date="">
            <span class="selector">
                <select
                        id="field_<?=$field['FieldID'] ?>_year"
                        name="field_<?=$field['FieldID'] ?>_year"
                        data-field="<?=$extraArr['field']?>"
                >
                    <option value='' <?=($isRequired)?'hidden':''?>>
                        <?= Get_Lang_Selection( '年', 'Year' ) ?>
                    </option>
                    <?php for($i=$minYear;$i<=$maxYear;$i++): ?>
                        <option value="<?=$i ?>" <?=($i == $valueArr[2][0])?'selected':'' ?>>
                            <?=$i ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </span><span class="selector">
                <select
                        id="field_<?=$field['FieldID'] ?>_month"
                        name="field_<?=$field['FieldID'] ?>_month"
                >
                    <option value='' <?=($isRequired)?'hidden':''?>>
                        <?= Get_Lang_Selection( '月', 'Month' ) ?>
                    </option>
                    <?php for($i=1;$i<=12;$i++): ?>
                        <option value="<?=$i ?>" <?=($i == $valueArr[2][1])?'selected':'' ?>><?=str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
                    <?php endfor; ?>
                </select>
            </span><span class="selector">
                <select
                        id="field_<?=$field['FieldID'] ?>_day"
                        name="field_<?=$field['FieldID'] ?>_day"
                >
                    <option value='' <?=($isRequired)?'hidden':''?>>
                        <?= Get_Lang_Selection( '日', 'Day' ) ?>
                    </option>
                    <?php for($i=1;$i<=31;$i++): ?>
                        <option value="<?=$i ?>" <?=($i == $valueArr[2][2])?'selected':'' ?>><?=str_pad($i, 2, '0', STR_PAD_LEFT); ?></option>
                    <?php endfor; ?>
                </select>
            </span>

            <div style="">
                <div class="remark remark-warn required4 hide" style="clear:both;">
                    <?= Get_Lang_Selection( '必須填寫', 'Required' ) ?>
                </div>
                <div class="remark remark-warn date hide" style="clear:both;">
                    <?= Get_Lang_Selection( '日期錯誤', 'Invalid date' ) ?>
                </div>
            </div>
        </span>
    </span>

</span>
<script>
  $(function () {
    var $field = $('[name="field_<?=$field['FieldID'] ?>"]');
    $field.click(updateValue);
    $('.field_<?=$field['FieldID'] ?>_option').change(updateValue);
    $('#field_<?=$field['FieldID'] ?>_year').change(updateValue);
    $('#field_<?=$field['FieldID'] ?>_month').change(updateValue);
    $('#field_<?=$field['FieldID'] ?>_day').change(updateValue);

    function updateValue() {
      var checkedVal = $field.filter(':checked').val();

      //// Update UI START ////
      $('.field_<?= $field['FieldID'] ?>_option').prop('disabled', true);
      $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).prop('disabled', false);
      $('.options_<?= $field['FieldID'] ?>_details').hide();
      $('.options_<?= $field['FieldID'] ?>_details.'+checkedVal).show().css('display', 'block')
      //// Update UI END ////

      //// Update value START ////
      var $data = $('#data_<?=$field['FieldID'] ?>');
      $data.find('.dataValue, .Y, .N').hide();

      if (checkedVal == '') {
        $data.find('.dataValue-empty').show();
      } else {
        $data.find('.dataValue-non-empty, .'+checkedVal).show();
        $data.find('.value-id').html($('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).val());

        var expiryStr = '';
        expiryStr += $('#field_<?=$field['FieldID'] ?>_year').val() + '-';
        expiryStr += ('0'+$('#field_<?=$field['FieldID'] ?>_month').val()).slice(-2) + '-';
        expiryStr += ('0'+$('#field_<?=$field['FieldID'] ?>_day').val()).slice(-2);
        $data.find('.value-expiry').html(expiryStr);
      }
      //// Update value END ////
    }

    updateValue();
  });
</script>
