<?php
global $lac;
global $lauc;

if($isEdit){
    $siblingInfoArr = $lac->getApplicationSibling($lac->schoolYearID, '', $ApplicationID);
    $custInfoArr = $lac->getAllApplicationCustInfo($ApplicationID);
}else{
	$siblingInfoArr = array();
	$custInfoArr = array();
	$custInfoArr['SiblingPosition'][0]['Value'] = 1;
	$custInfoArr['NumOfSibling'][0]['Value'] = 1;
}



?>
<div class="item displaySection display_pagePersonalInfo">
    <div class="itemLabel requiredLabel">
	    <?=$lauc->getLangStr(array(
		    'b5' => '幼兒在家中的排行',
		    'en' => 'Applicant\'s Position in Family',
	    )) ?>
    </div>
    <span class="itemInput itemInput-selector itemInput-warn">
	    <?=$lauc->getLangStr(array(
		    'b5' => '排行',
		    'en' => 'Position',
	    )) ?>
        <span class="selector" style="display: inline-block;">
            <select id="siblingPosition" name="siblingPosition">
                <?php for($i=1;$i<=6;$i++):?>
                    <option <?=($custInfoArr['SiblingPosition'][0]['Value'] == $i)?'selected':''?>><?=$i?></option>
                <?php endfor;?>
            </select>
        </span>
        /
        <span class="selector" style="display: inline-block;">
            <select id="numOfSibling" name="numOfSibling">
                <?php for($i=1;$i<=6;$i++):?>
                    <option <?=($custInfoArr['NumOfSibling'][0]['Value'] == $i)?'selected':''?>><?=$i?></option>
                <?php endfor;?>
            </select>
        </span>
    </span>
</div>
<div class="item" style="display: none;"></div>
<div class="item" style="display: none;"></div>


<?php for ( $i = 0; $i < 5; $i ++ ): ?><!--
--><div class="item item-In3 siblingContainer displaySection display_pagePersonalInfo" id="siblingContainer_<?=$i?>">
    <div class="text-inst">
	    <?=$lauc->getLangStr(array(
		    'b5' => '兄弟姊妹',
		    'en' => 'Sibling',
	    )) ?>
        : <?=$i+1?>
    </div>
    <span class="itemInput">
        <div class="textbox-floatlabel itemInput">
            <input
                    type="text"
                    id="siblingName_<?=$i?>"
                    name="siblingName_<?=$i?>"
                    class="siblingInput"
                    value="<?=$siblingInfoArr[$i]['EnglishName']?>"
            />
            <div class="textboxLabel requiredLabel">
                <?=$lauc->getLangStr(array(
                    'b5' => '英文姓名 (same as HKID or passport)',
                    'en' => 'Name in English',
                )) ?>
            </div>
            <div class="remark remark-warn required hide" style="clear:both;">
                <?=$lauc->getLangStr(array(
                    'b5' => '必須填寫。',
                    'en' => 'Required.',
                ))?>
            </div>
            <div class="remark remark-warn english hide" style="clear:both;">
                <?=$lauc->getLangStr(array(
                    'b5' => '必須是英文字',
                    'en' => 'Must be English character',
                ))?>
            </div>
        </div><div class="textbox-floatlabel itemInput">
            <input
                    type="text"
                    id="siblingAge_<?=$i?>"
                    name="siblingAge_<?=$i?>"
                    class="siblingInput"
                    value="<?=$siblingInfoArr[$i]['Age']?>"
            />
            <div class="textboxLabel requiredLabel">
                <?=$lauc->getLangStr(array(
	                'b5' => '年齡',
	                'en' => 'Age',
                )) ?>
            </div>
            <div class="remark remark-warn required hide" style="clear:both;">
                <?=$lauc->getLangStr(array(
	                'b5' => '必須填寫。',
	                'en' => 'Required.',
                ))?>
            </div>
            <div class="remark remark-warn postive_number hide" style="clear:both;">
                <?=$lauc->getLangStr(array(
                    'b5' => '必須是正整數',
                    'en' => 'Must be postive integer',
                ))?>
            </div>
        </div><div class="textbox-floatlabel itemInput">
            <input
                    type="text"
                    id="siblingBirthCert_<?=$i?>"
                    name="siblingBirthCert_<?=$i?>"
                    class="siblingInput"
                    value="<?=$siblingInfoArr[$i]['BirthCertNo']?>"
            />
            <div class="textboxLabel requiredLabel">
                <?=$lauc->getLangStr(array(
	                'b5' => '出生證明書/護照號碼',
	                'en' => 'Birth Certificate / Passport No.',
                )) ?>
            </div>
            <div class="remark remark-warn required hide" style="clear:both;">
                <?=$lauc->getLangStr(array(
	                'b5' => '必須填寫。',
	                'en' => 'Required.',
                ))?>
            </div>
        </div><div class="textbox-floatlabel itemInput">
            <input
                    type="text"
                    id="siblingCurrentSchool_<?=$i?>"
                    name="siblingCurrentSchool_<?=$i?>"
                    class="siblingInput"
                    value="<?=$siblingInfoArr[$i]['CurrentSchool']?>"
            />
            <div class="textboxLabel">
                <?=$lauc->getLangStr(array(
		            'b5' => '現就讀學校',
		            'en' => 'Current School',
	            ))?>
            </div>
        </div><div class="textbox-floatlabel itemInput">
            <input
                    type="text"
                    id="siblingCurrentGrade_<?=$i?>"
                    name="siblingCurrentGrade_<?=$i?>"
                    class="siblingInput"
                    value="<?=$siblingInfoArr[$i]['Year']?>"
            >
            <div class="textboxLabel">
                <?=$lauc->getLangStr(array(
	                'b5' => '就讀班級',
	                'en' => 'Current Grade(s) Attending',
                ))?>
            </div>
        </div><div class="textbox-floatlabel">
            <div class="itemLabel requiredLabel">
                <?=$lauc->getLangStr(array(
	                'b5' => '於心怡天地高班畢業?',
	                'en' => 'Graduated from K3 at Joyful World?',
                ))?>
            </div>
            <span class="itemInput itemInput-choice">
                <span>
                    <input
                            type="radio"
                            id="siblingSameSchoolGraduated_<?=$i?>_Y"
                            name="siblingSameSchoolGraduated_<?=$i?>"
                            value="Y"
                            class="siblingInput"
                            <?=($siblingInfoArr[$i]['GraduateSameSchool'] != '')?'checked':''?>
                    />
                    <label for="siblingSameSchoolGraduated_<?=$i?>_Y">
                        <?=$lauc->getLangStr(array(
                            'b5' => $LangB5['General']['Yes'],
                            'en' => $LangEn['General']['Yes'],
                        )) ?>
                    </label>
                </span>
                <span class="textbox-floatlabel">
                    <input
                            type="text"
                            id="siblingSameSchoolGraduatedYear_<?=$i?>"
                            name="siblingSameSchoolGraduatedYear_<?=$i?>"
                            class="siblingInput"
                            value="<?=$siblingInfoArr[$i]['GraduateSameSchool']?>"
                    />
                    <div class="textboxLabel requiredLabel">
                        <?=$lauc->getLangStr(array(
                            'b5' => '就讀年份',
                            'en' => 'Year attended',
                        )) ?>
                    </div>
                </span>
                <span>
                    <input
                            type="radio"
                            id="siblingSameSchoolGraduated_<?=$i?>_N"
                            name="siblingSameSchoolGraduated_<?=$i?>"
                            value="N"
                            class="siblingInput"
                            <?=($siblingInfoArr[$i]['GraduateSameSchool'] == '')?'checked':''?>
                    />
                    <label for="siblingSameSchoolGraduated_<?=$i?>_N">
                        <?=$lauc->getLangStr(array(
                            'b5' => $LangB5['General']['No'],
                            'en' => $LangEn['General']['No'],
                        )) ?>
                    </label>
                </span>
                <div class="remark remark-warn required hide" style="clear:both;">
                    <?=$lauc->getLangStr(array(
                        'b5' => '必須填寫。',
                        'en' => 'Required.',
                    ))?>
                </div>
            </span>
        </div><div class="textbox-floatlabel">
            <div class="itemLabel requiredLabel">
                <?=$lauc->getLangStr(array(
                    'b5' => '一同報讀心怡天地?',
                    'en' => 'Currently Applying to Joyful World together?',
                )) ?>
            </div>
            <span class="itemInput itemInput-choice">
                <span>
                    <input
                            type="radio"
                            id="siblingSameSchoolApply_<?=$i?>_Y"
                            name="siblingSameSchoolApply_<?=$i?>"
                            value="Y"
                            class="siblingInput"
                            <?=($siblingInfoArr[$i]['ApplyingSameSchool'] === 'Y')?'checked':''?>
                    />
                    <label for="siblingSameSchoolApply_<?=$i?>_Y">
                        <?=$lauc->getLangStr(array(
	                        'b5' => $LangB5['General']['Yes'],
	                        'en' => $LangEn['General']['Yes'],
                        )) ?>
                    </label>
                </span>
                <span>
                    <input
                            type="radio"
                            id="siblingSameSchoolApply_<?=$i?>_N"
                            name="siblingSameSchoolApply_<?=$i?>"
                            value="N"
                            class="siblingInput"
                            <?=($siblingInfoArr[$i]['ApplyingSameSchool'] === 'N')?'checked':''?>
                    />
                    <label for="siblingSameSchoolApply_<?=$i?>_N">
                        <?=$lauc->getLangStr(array(
	                        'b5' => $LangB5['General']['No'],
	                        'en' => $LangEn['General']['No'],
                        )) ?>
                    </label>
                </span>
                <div class="remark remark-warn required hide" style="clear:both;">
                    <?=$lauc->getLangStr(array(
                        'b5' => '必須填寫。',
                        'en' => 'Required.',
                    ))?>
                </div>
            </span>
        </div>
    </span>
</div><!--
--><?php endfor; ?>

<span class="itemData <?=$itemDataClass ?>"  id="data-siblingContainer">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
            'b5' => '幼兒在家中的排行',
            'en' => 'Applicant\'s Position in Family',
        )) ?>
	</div>
	<div class="dataValue">
        <?=$lauc->getLangStr(array(
	        'b5' => '排行',
	        'en' => 'Position',
        )) ?>:
        <span class="siblingPosition"></span>
    </div>

<div class="itemLabel" style="margin-top: 20px;"></div>
<?php for( $i = 0; $i < 5; $i ++ ): ?><!--
    --><div class="item item-In3 data-siblingContainer" id="data-siblingContainer_<?=$i?>">
        <div class="text-inst">
            <?=$lauc->getLangStr(array(
                'b5' => '兄弟姊妹',
                'en' => 'Sibling',
            )) ?>
            : <?=$i+1?>
        </div>
        <span class="itemData">
            <div class="textbox-floatlabel">
                <input
                        type="text"
                        class="itemData notEmpty dataValue"
                        id="data_siblingName_<?=$i?>"
                        value="<?=$siblingInfoArr[$i]['EnglishName']?>"
                        readonly
                />
                <div class="textboxLabel requiredLabel">
                    <?=$lauc->getLangStr(array(
                        'b5' => '英文姓名',
                        'en' => 'English Name',
                    )) ?>
                </div>
            </div><div class="textbox-floatlabel">
                <input
                        type="text"
                        class="itemData notEmpty dataValue"
                        id="data_siblingAge_<?=$i?>"
                        value="<?=$siblingInfoArr[$i]['Age']?>"
                        readonly
                />
                <div class="textboxLabel requiredLabel">
                    <?=$lauc->getLangStr(array(
	                    'b5' => '年齡',
	                    'en' => 'Age',
                    )) ?>
                </div>
            </div><div class="textbox-floatlabel">
                <input
                        type="text"
                        class="itemData notEmpty dataValue"
                        id="data_siblingBirthCert_<?=$i?>"
                        value="<?=$siblingInfoArr[$i]['BirthCertNo']?>"
                        readonly
                />
                <div class="textboxLabel requiredLabel">
                    <?=$lauc->getLangStr(array(
	                    'b5' => '出生證明書/護照號碼',
	                    'en' => 'Birth Certificate / Passport No.',
                    )) ?>
                </div>
            </div><div class="textbox-floatlabel">
                <input
                        type="text"
                        class="itemData notEmpty dataValue"
                        id="data_siblingCurrentSchool_<?=$i?>"
                        value="<?=$siblingInfoArr[$i]['CurrentSchool']?>"
                        readonly
                />
                <div class="textboxLabel">
                    <?=$lauc->getLangStr(array(
	                    'b5' => '現就讀學校',
	                    'en' => 'Current School',
                    )) ?>
                </div>
            </div><div class="textbox-floatlabel">
                <div class="itemLabel requiredLabel">
                    <?=$lauc->getLangStr(array(
	                    'b5' => '於心怡天地高班畢業',
	                    'en' => 'Graduated from K3 at Joyful World',
                    )) ?>
                </div>
                <span id="data_siblingSameSchoolGraduated_<?=$i?>_Y" class="dataValue">
                    <?=$lauc->getLangStr(array(
                        'b5' => $LangB5['General']['Yes'],
                        'en' => $LangEn['General']['Yes'],
                    )) ?>
                </span>
                <span id="data_siblingSameSchoolGraduated_<?=$i?>_N" class="dataValue">
                    <?=$lauc->getLangStr(array(
                        'b5' => $LangB5['General']['No'],
                        'en' => $LangEn['General']['No'],
                    )) ?>
                </span>

                <span class="textbox-floatlabel" style="display:block;margin-top: 10px;" id="data_siblingSameSchoolGraduated_<?=$i?>_Y_container">
                    <input
                            type="text"
                            class="itemData notEmpty dataValue"
                            id="data_siblingSameSchoolGraduatedYear_<?=$i?>"
                            value="<?=$siblingInfoArr[$i]['Year']?>"
                            readonly
                    />
                    <div class="textboxLabel requiredLabel">
                        <?=$lauc->getLangStr(array(
                            'b5' => '就讀年份',
                            'en' => 'Year attended',
                        )) ?>
                    </div>
                </span>
            </div><div class="textbox-floatlabel">
                <div class="itemLabel requiredLabel">
                    <?=$lauc->getLangStr(array(
                        'b5' => '一同報讀心怡天地',
                        'en' => 'Currently Applying to Joyful World',
                    )) ?>
                </div>
                <span id="data_siblingSameSchoolApply_<?=$i?>_Y" class="dataValue">
                    <?=$lauc->getLangStr(array(
                        'b5' => $LangB5['General']['Yes'],
                        'en' => $LangEn['General']['Yes'],
                    )) ?>
                </span>
                <span id="data_siblingSameSchoolApply_<?=$i?>_N" class="dataValue">
                    <?=$lauc->getLangStr(array(
                        'b5' => $LangB5['General']['No'],
                        'en' => $LangEn['General']['No'],
                    )) ?>
                </span>
            </div>
        </span>
    </div><!--
--><?php endfor;?>

</span><!--

-->
<script>
  $(function () {
    $('#numOfSibling').change(updateValue);

    $('#siblingPosition, #numOfSibling').change(function () {
      $('.siblingPosition').html($('#siblingPosition').val() + '/' + $('#numOfSibling').val());
    }).change();
    $('[id^="siblingSameSchoolApply_"], [id^="siblingSameSchoolGraduated_"], siblingInput').click(updateValue);
    $('.siblingInput').change(updateValue);

    function updateValue() {
      var numOfSibling = $('#numOfSibling').val();
      var siblingPosition = $('#siblingPosition').val();

      $('.siblingContainer, .data-siblingContainer').hide();
      for (var i = 0; i < numOfSibling - 1; i++) {
        $('#siblingContainer_' + i + ', #data-siblingContainer_' + i).show();
      }

      $('#siblingPosition').html('');
      for (var i = 0; i < numOfSibling; i++) {
      if(siblingPosition > numOfSibling && numOfSibling == (i + 1)){
      	$('#siblingPosition').append($('<option>').html(i + 1).attr('selected','selected'));
      }
      else if(siblingPosition == (i + 1)){
      	$('#siblingPosition').append($('<option>').html(i + 1).attr('selected','selected'));
      }
      else
      {
        $('#siblingPosition').append($('<option>').html(i + 1));
      }
      }

      $('[id^="siblingSameSchoolApply_"]:checked, [id^="siblingSameSchoolGraduated_"]:checked').each(function(){
        var id = $(this).attr('id');
        var name = $(this).attr('name');
        var value = $(this).val();
        $('[id^="data_'+name+'"]').hide();
        $('#data_'+id).show();

        if(value === 'Y'){
          $('#data_'+id+'_container').show();
        }else{
          $('#data_'+id+'_container').hide();
        }
      });

      if($(this).hasClass('siblingInput')){
        var id = $(this).attr('id');
        var value = $(this).val();
        $('#data_'+id).val(value);
      }
    }

    window.admissionStateMachine.onEnterPagePersonalInfo = function () {
      setTimeout(updateValue, 500);
    }
    window.admissionStateMachine.onEnterPageConfirmation = function () {
      setTimeout(function () {
        var $dataContainer = $('#data-siblingContainer');
        $dataContainer.find('.itemLabel').show();
      });
    }
  });
</script>
