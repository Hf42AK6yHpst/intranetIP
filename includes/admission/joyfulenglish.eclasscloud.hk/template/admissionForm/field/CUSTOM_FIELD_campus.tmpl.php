<?php
global $json, $admission_cfg;
global $lauc;


$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );

#### Get setting START ####
$schoolTypeLangArr = array();
foreach($admission_cfg['SchoolType'] as $type){
	$schoolTypeLangArr[$type] = $lauc->getLangStr(array(
        'b5' => $LangB5['Admission']['JOYFUL']['SchoolType'][ $type ],
        'en' => $LangEn['Admission']['JOYFUL']['SchoolType'][ $type ],
    ));
}
#### Get setting END ####
?>
<span class="itemInput itemInput-selector">
    <span class="selector">
        <select
                id="field_<?= $field['FieldID'] ?>_0"
                name="field_<?= $field['FieldID'] ?>[]"
                data-field="<?= $extraArr['field'] ?>"
        >
            <option value=''>
                --
                <?=$lauc->getLangStr(array(
                    'b5' => $LangB5['Admission']['JOYFUL']['firstChoice'],
                    'en' => $LangEn['Admission']['JOYFUL']['firstChoice'],
                ))?>
                --
            </option>
            <?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                <option
	                value="<?= $type ?>"
	                <?= ( $type == $value[ 0 ] ) ? 'selected' : '' ?>
                >
                    <?=$lauc->getLangStr(array(
	                    'b5' => $LangB5['Admission']['JOYFUL']['SchoolType'][ $type ],
	                    'en' => $LangEn['Admission']['JOYFUL']['SchoolType'][ $type ],
                    ))?>
                </option>
            <?php endforeach; ?>
        </select>
    </span>
    <span class="selector">
        <select
                id="field_<?= $field['FieldID'] ?>_1"
                name="field_<?= $field['FieldID'] ?>[]"
                data-field="<?= $extraArr['field'] ?>"
        >
            <option value=''>
                --
                <?=$lauc->getLangStr(array(
	                'b5' => $LangB5['Admission']['JOYFUL']['secondChoice'],
	                'en' => $LangEn['Admission']['JOYFUL']['secondChoice'],
                ))?>
                --
            </option>
            <?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                <option
	                value="<?= $type ?>"
	                <?= ( $type == $value[ 1 ] ) ? 'selected' : '' ?>
                >
                    <?=$lauc->getLangStr(array(
	                    'b5' => $LangB5['Admission']['JOYFUL']['SchoolType'][ $type ],
	                    'en' => $LangEn['Admission']['JOYFUL']['SchoolType'][ $type ],
                    ))?>
                </option>
            <?php endforeach; ?>
        </select>
    </span>
    <span class="selector">
        <select
                id="field_<?= $field['FieldID'] ?>_2"
                name="field_<?= $field['FieldID'] ?>[]"
                data-field="<?= $extraArr['field'] ?>"
        >
            <option value=''>
                --
                <?=$lauc->getLangStr(array(
	                'b5' => $LangB5['Admission']['JOYFUL']['thirdChoice'],
	                'en' => $LangEn['Admission']['JOYFUL']['thirdChoice'],
                ))?>
                --
            </option>
            <?php foreach ( $admission_cfg['SchoolType'] as $type ): ?>
                <option value="<?= $type ?>" <?= ( $type == $value[ 2 ] ) ? 'selected' : '' ?>>
                    <?=$lauc->getLangStr(array(
	                    'b5' => $LangB5['Admission']['JOYFUL']['SchoolType'][ $type ],
	                    'en' => $LangEn['Admission']['JOYFUL']['SchoolType'][ $type ],
                    ))?>
                </option>
            <?php endforeach; ?>
        </select>
    </span>

    <div style="">
        <div class="remark remark-warn required hide" style="clear:both;">
            <?=$lauc->getLangStr(array(
                'b5' => '必須填寫',
                'en' => 'Required',
            ))?>
        </div>
        <div class="remark remark-warn option_repeat hide" style="clear:both;">
            <?=$lauc->getLangStr(array(
                'b5' => '選擇重覆',
                'en' => 'Duplicate selection',
            ))?>
        </div>
    	<div class="remark remark-warn custom hide" style="clear:both;"></div>
    </div>
</span><!--

--><span class="itemData">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
            'b5' => $field['LabelB5'],
            'en' => $field['LabelEn'],
        ))?>
	</div>
	<div class="dataValue" id="data_<?= $field['FieldID'] ?>">
    	<?php for ( $i = 0, $iMax = 3; $i < $iMax; $i ++ ): ?>
            <div>
                <?=$lauc->getLangStr(array(
                    'b5' => "{$LangB5['Admission']['Option']} " . ($i+1),
                    'en' => "{$LangEn['Admission']['Option']} " . ($i+1),
                ))?>:
    			<span class="choice_<?= $i ?>"></span>
    		</div>
	    <?php endfor; ?>
	</div>
</span><!--

-->
<script>
  $(function () {
    var slotLangArr = <?=$json->encode( $schoolTypeLangArr ) ?>;
    $('[name^="field_<?=$field['FieldID'] ?>"]').change(updateValue);

    function updateValue() {
      var choices = [];
		<?php for ($i = 0, $iMax = 3; $i < $iMax; $i ++): ?>
      var value = $('#field_<?=$field['FieldID'] ?>_<?=$i ?>').val();

      if (value) {
        choices.push(value);
      }
		<?php endfor; ?>


      var $field = $('#data_<?=$field['FieldID'] ?>');
      if (choices.length) {
        $field.removeClass('dataValue-empty');
      } else {
        $field.addClass('dataValue-empty');
      }

      $field.find('[class^="choice_"]').html('－－');
      $.each(choices, function (index, choice) {
        $field.find('.choice_' + index).html(slotLangArr[choice]);
      });
    }

    updateValue();
  });
</script>
