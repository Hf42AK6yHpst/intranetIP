<?php
global $lauc, $admission_cfg;
$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$hasLabel   = ! ! ( $field['LabelB5'] . $field['LabelEn'] . $field['LabelGb'] );
$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );
$isReadonly = ! ! ( $data );

$extraArr['optionsY'] = (array) $extraArr['optionsY'];
$extraArr['optionsN'] = (array) $extraArr['optionsN'];


$value = '';
if ( $applicationData['ADMISSION_STU_INFO']['BirthCertType'] == $admission_cfg['BirthCertType']['hkid'] ) {
	$value .= 'Y;';
	$value .= $applicationData['ADMISSION_STU_INFO']['BirthCertNo'];
} else if ( $applicationData['ADMISSION_STU_INFO']['BirthCertType'] == $admission_cfg['BirthCertType']['passport'] ){
	$visa = '';
	foreach ( (array) $data as $d ) {
		if ( $d['Code'] === 'Visa' ) {
			$visa = $d['Value'];
		}
	}

	$value .= 'N;';
	$value .= $applicationData['ADMISSION_STU_INFO']['BirthCertNo'] . ';';
	$value .= $visa;
}

$valueArr    = explode( ';', $value );
$valueArr[2] = explode( '-', $valueArr[2] );


#### Get setting START ####
$maxYear = ( $extraArr['maxYear'] ) ? $extraArr['maxYear'] : (int) date( 'Y' ) + 9;
$minYear = ( $extraArr['minYear'] ) ? $extraArr['minYear'] : ( (int) date( 'Y' ) ) - 1;
#### Get setting END ####

if ( $isReadonly ):
	?>
    <span>
        <span class="itemData">
            <div class="dataLabel">
                <?= $field['TitleB5'] ?>
                <?= $field['TitleEn'] ?>
            </div>
        </span>
        <div class="dataValue dataValue-non-empty">
            <span>
                <?= $lauc->getLangStr( array(
                    'b5' => ( $valueArr[0] === 'Y' )? $LangB5['General']['Yes']:$LangB5['General']['No'],
                    'en' => ( $valueArr[0] === 'Y' )? $LangEn['General']['Yes']:$LangEn['General']['No'],
                ) ) ?>
            </span>
        </div>
        <div class="dataValue dataValue-non-empty">
            <span>
                <?= $lauc->getLangStr( array(
                    'b5' => ( $valueArr[0] === 'Y' )? '香港身份證號碼' : '護照號碼',
                    'en' => ( $valueArr[0] === 'Y' )? 'Hong Kong ID No.' : 'Passport No.',
                ) ) ?>:
            </span>
            <span class="value-id"><?= $valueArr[1] ?></span>
        </div>
        <?php if( $valueArr[0] !== 'Y' ):?>
        <div class="dataValue dataValue-non-empty">
            <span>
                <?= $lauc->getLangStr( array(
                    'b5' => '受養人簽證到期日',
                    'en' => 'Dependent Visa Expiry Date',
                ) ) ?>:
            </span>
            <span class="value-expiry"><?="{$visa}"?></span>
        </div>
        <?php endif;?>
    </span>
<?php
else:
	?>

    <span class="itemInput itemInput-choice">
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_Y"
                name="field_<?= $field['FieldID'] ?>"
                value="Y"
				<?= ( $valueArr[0] === 'Y' ) ? 'checked' : '' ?>
        ><label for="field_<?= $field['FieldID'] ?>_Y">
	        <?= $lauc->getLangStr( array(
		        'b5' => $LangB5['General']['Yes'],
		        'en' => $LangEn['General']['Yes'],
	        ) ) ?>
        </label>
	</span>
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_N"
                name="field_<?= $field['FieldID'] ?>"
                value="N"
				<?= ( $valueArr[0] === 'N' ) ? 'checked' : '' ?>
        ><label for="field_<?= $field['FieldID'] ?>_N">
	        <?= $lauc->getLangStr( array(
		        'b5' => $LangB5['General']['No'],
		        'en' => $LangEn['General']['No'],
	        ) ) ?>
        </label>
	</span>
    <div style="">
        <div class="remark remark-warn required1 hide" style="clear:both;">
            <?= $lauc->getLangStr( array(
	            'b5' => '必須填寫',
	            'en' => 'Required',
            ) ) ?>
        </div>
    </div>


    <span class="options_<?= $field['FieldID'] ?>_details Y" style="display: none;margin: 26px 0;">
        <div class="textbox-floatlabel <?= $cssClass ?>">
            <input
                    type="text"
                    id="field_<?= $field['FieldID'] ?>_0"
                    class="field_<?= $field['FieldID'] ?>_option Y"
                    name="field_<?= $field['FieldID'] ?>_option[]"
                    value="<?= $valueArr[1] ?>"
            />
            <div class="textboxLabel ">
                <?= $lauc->getLangStr( array(
	                'b5' => '香港身份證號碼',
	                'en' => 'Hong Kong ID No.',
                ) ) ?>
            </div>
            <div class="remark"><?= $lauc->getLangStr( array(
	                'b5' => '例如：A123456(7)，請輸入 "A1234567"',
	                'en' => 'eg：A123456(7), please enter "A1234567"',
                ) ) ?></div>
        </div>

        <div style="">
            <div class="remark remark-warn required2 hide" style="clear:both;">
                <?= $lauc->getLangStr( array(
	                'b5' => '必須填寫',
	                'en' => 'Required',
                ) ) ?>
            </div>
            <div class="remark remark-warn hkid_duplicate hide" style="clear:both;">
                <?= $lauc->getLangStr( array(
	                'b5' => '出生證明書號碼已被使用！請輸入其他出生證明書號碼。',
	                'en' => 'The Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.',
                ) ) ?>
            </div>
            <div class="remark remark-warn hkid email hide" style="clear:both;">
                <?= $lauc->getLangStr( array(
	                'b5' => '格式錯誤',
	                'en' => 'Invalid format',
                ) ) ?>
            </div>
        </div>
    </span>

    <span class="options_<?= $field['FieldID'] ?>_details N" style="display: none;margin: 26px 0;">
        <div class="textbox-floatlabel <?= $cssClass ?>">
            <input
                    type="text"
                    id="field_<?= $field['FieldID'] ?>_1"
                    class="field_<?= $field['FieldID'] ?>_option N"
                    name="field_<?= $field['FieldID'] ?>_option[]"
                    value="<?= $valueArr[1] ?>"
            />
            <div class="textboxLabel ">
                <?= $lauc->getLangStr( array(
	                'b5' => '護照號碼',
	                'en' => 'Passport No.',
                ) ) ?>
            </div>
        </div>

        <div style="">
            <div class="remark remark-warn required3 hide" style="clear:both;">
                <?= $lauc->getLangStr( array(
	                'b5' => '必須填寫',
	                'en' => 'Required',
                ) ) ?>
            </div>
            <div class="remark remark-warn hkid_duplicate hide" style="clear:both;">
                <?= $lauc->getLangStr( array(
	                'b5' => '出生證明書號碼已被使用！請輸入其他出生證明書號碼。',
	                'en' => 'The Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.',
                ) ) ?>
            </div>
        </div>
    </span>

    <div class="itemLabel options_<?= $field['FieldID'] ?>_details N" style="display: none;">
        <?= $lauc->getLangStr( array(
	        'b5' => '受養人簽證到期日',
	        'en' => 'Dependent Visa Expiry Date',
        ) ) ?>
    </div>
    <span class="options_<?= $field['FieldID'] ?>_details N" style="display: none;">

        <span class="itemInput itemInput-selector" data-min-date="" data-max-date="">
            <span class="selector">
                <select
                        id="field_<?= $field['FieldID'] ?>_year"
                        name="field_<?= $field['FieldID'] ?>_year"
                        data-field="<?= $extraArr['field'] ?>"
                >
                    <option value='' <?= ( $isRequired ) ? 'hidden' : '' ?>>
                        <?= $lauc->getLangStr( array(
	                        'b5' => '年',
	                        'en' => 'Year',
                        ) ) ?>
                    </option>
                    <?php for ( $i = $minYear; $i <= $maxYear; $i ++ ): ?>
                        <option value="<?= $i ?>" <?= ( $i == $valueArr[2][0] ) ? 'selected' : '' ?>>
                            <?= $i ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </span><span class="selector">
                <select
                        id="field_<?= $field['FieldID'] ?>_month"
                        name="field_<?= $field['FieldID'] ?>_month"
                >
                    <option value='' <?= ( $isRequired ) ? 'hidden' : '' ?>>
                        <?= $lauc->getLangStr( array(
	                        'b5' => '月',
	                        'en' => 'Month',
                        ) ) ?>
                    </option>
                    <?php for ( $i = 1; $i <= 12; $i ++ ): ?>
                        <option value="<?= $i ?>"
	                            <?= ( $i == $valueArr[2][1] ) ? 'selected' : '' ?>><?= str_pad( $i, 2, '0', STR_PAD_LEFT ); ?></option>
                    <?php endfor; ?>
                </select>
            </span><span class="selector">
                <select
                        id="field_<?= $field['FieldID'] ?>_day"
                        name="field_<?= $field['FieldID'] ?>_day"
                >
                    <option value='' <?= ( $isRequired ) ? 'hidden' : '' ?>>
                        <?= $lauc->getLangStr( array(
	                        'b5' => '日',
	                        'en' => 'Day',
                        ) ) ?>
                    </option>
                    <?php for ( $i = 1; $i <= 31; $i ++ ): ?>
                        <option value="<?= $i ?>"
	                            <?= ( $i == $valueArr[2][2] ) ? 'selected' : '' ?>><?= str_pad( $i, 2, '0', STR_PAD_LEFT ); ?></option>
                    <?php endfor; ?>
                </select>
            </span>

            <div style="">
                <div class="remark remark-warn required4 hide" style="clear:both;">
                    <?= $lauc->getLangStr( array(
	                    'b5' => '必須填寫',
	                    'en' => 'Required',
                    ) ) ?>
                </div>
                <div class="remark remark-warn date hide" style="clear:both;">
                    <?= $lauc->getLangStr( array(
	                    'b5' => '日期錯誤',
	                    'en' => 'Invalid date',
                    ) ) ?>
                </div>
            </div>
        </span>
    </span>

</span><!--

--><span class="itemData <?= $itemDataClass ?>" id="data_<?= $field['FieldID'] ?>">
	<div class="dataLabel">
		<?= $field['TitleB5'] ?>
		<?= $field['TitleEn'] ?>
	</div>
	<div class="dataValue dataValue-empty">－－</div>
	<div class="dataValue dataValue-non-empty">
        <span class="Y">
            <?= $lauc->getLangStr( array(
	            'b5' => $LangB5['General']['Yes'],
	            'en' => $LangEn['General']['Yes'],
            ) ) ?>
        </span>
        <span class="N">
            <?= $lauc->getLangStr( array(
	            'b5' => $LangB5['General']['No'],
	            'en' => $LangEn['General']['No'],
            ) ) ?>
        </span>
    </div>
	<div class="dataValue dataValue-non-empty">
        <span class="Y">
            <?= $lauc->getLangStr( array(
	            'b5' => '香港身份證號碼',
	            'en' => 'Hong Kong ID No.',
            ) ) ?>:
        </span>
        <span class="N">
            <?= $lauc->getLangStr( array(
	            'b5' => '護照號碼',
	            'en' => 'Passport No.',
            ) ) ?>:
        </span>
        <span class="value-id"></span>
    </div>
	<div class="dataValue dataValue-non-empty">
        <span class="N">
            <?= $lauc->getLangStr( array(
	            'b5' => '受養人簽證到期日',
	            'en' => 'Dependent Visa Expiry Date',
            ) ) ?>:
        </span>
        <span class="value-expiry N"></span>
    </div>
</span><!--

-->
    <script>
      $(function () {
        var $field = $('[name="field_<?=$field['FieldID'] ?>"]');
        $field.click(updateValue);
        $('.field_<?=$field['FieldID'] ?>_option').change(updateValue);
        $('#field_<?=$field['FieldID'] ?>_year').change(updateValue);
        $('#field_<?=$field['FieldID'] ?>_month').change(updateValue);
        $('#field_<?=$field['FieldID'] ?>_day').change(updateValue);

        function updateValue() {
          var checkedVal = $field.filter(':checked').val();

          //// Update UI START ////
          $('.field_<?= $field['FieldID'] ?>_option').prop('disabled', true);
          $('.field_<?= $field['FieldID'] ?>_option.' + checkedVal).prop('disabled', false);
          $('.options_<?= $field['FieldID'] ?>_details').hide();
          $('.options_<?= $field['FieldID'] ?>_details.' + checkedVal).show().css('display', 'block')
          //// Update UI END ////

          //// Update value START ////
          var $data = $('#data_<?=$field['FieldID'] ?>');
          $data.find('.dataValue, .Y, .N').hide();

          if (checkedVal == '') {
            $data.find('.dataValue-empty').show();
          } else {
            $data.find('.dataValue-non-empty, .' + checkedVal).show();
            $data.find('.value-id').html($('.field_<?= $field['FieldID'] ?>_option.' + checkedVal).val());

            var expiryStr = '';
            expiryStr += $('#field_<?=$field['FieldID'] ?>_year').val() + '-';
            expiryStr += ('0' + $('#field_<?=$field['FieldID'] ?>_month').val()).slice(-2) + '-';
            expiryStr += ('0' + $('#field_<?=$field['FieldID'] ?>_day').val()).slice(-2);
            $data.find('.value-expiry').html(expiryStr);
          }
          //// Update value END ////
        }

        updateValue();
      });
    </script>
<?php
endif;
