<?php
global $lauc;

$startYear = date('Y', getStartOfAcademicYear('', $this->schoolYearID));
$endYear = date('y', getStartOfAcademicYear('', $this->schoolYearID)) + 1;

?>
<div class="item">
	<div class="itemLabel">
		<?=$lauc->getLangStr(array(
			'b5' => $field['TitleB5'],
			'en' => $field['TitleEn'],
		))?>
	</div>
	<span class="itemInput">
		<span>
			<label><?=$startYear?>/<?=$endYear?></label>
		</span>
	</span>
</div>
<span class="itemData">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
	        'b5' => $field['TitleB5'],
	        'en' => $field['TitleEn'],
        ))?>
	</div>
	<div class="dataValue" id="data_<?= $field['FieldID'] ?>">
        <label><?=$startYear?>/<?=$endYear?></label>
	</div>
</span>
