<table class="common_table_list edit_table_list">
	<colgroup>
		<col nowrap="nowrap">
	</colgroup>
	<thead>
	<tr>
		<th width="20">&nbsp;</th>
		<th><?= $kis_lang['Admission']['JOYFUL']['Campas'] ?></th>
		<th><?= $kis_lang['interviewdate'] ?></th>
		<th><?= $kis_lang['timeslot'] ?> </th>
		<th>
			<? if ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room') { ?>
				<?= $kis_lang['interviewroom'] ?>
			<? } else { ?>
				<?= $kis_lang['sessiongroup'] ?>
			<? } ?>
		</th>
		<th><?= $kis_lang['quotaRemains'] ?></th>
		<th><?= $kis_lang['applied'] ?></th>
		<th><input type="checkbox" name="checkmaster"
		           onclick="(this.checked)?setChecked(1,this.form,'interviewAry[]'):setChecked(0,this.form,'interviewAry[]')">
		</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$idx = 0;
	foreach ($interviewSettingAry as $_classLevelId => $_classLevelRecord):
		//foreach($_classLevelAry as $_classLevelRecord){
		$idx++;
		$SchoolYearArr = current(kis_utility::getAcademicYears(array("AcademicYearID" => $_classLevelRecord['ClassLevelID'])));
		$formName = $SchoolYearArr['academic_year_name_' . $intranet_session_language];
		?>
		<tr>
			<td><?= $idx ?></td>
			<td><?= $kis_lang['Admission']['JOYFUL']['SchoolType'][$_classLevelRecord['Extra']] ?></td>
			<td><?= $_classLevelRecord['Date'] ?></td>
			<td><?= substr($_classLevelRecord['StartTime'], 0, -3) ?>
				~ <?= substr($_classLevelRecord['EndTime'], 0, -3) ?></td>
			<td><?= ($_classLevelRecord['GroupName'] ? $_classLevelRecord['GroupName'] : '--') ?></td>
			<td>
				<?= $_classLevelRecord['Quota'] ?>
				(<span style="<?= (($_classLevelRecord['Quota'] - $_classLevelRecord['Applied']) > 0) ? '' : 'color:red;'; ?>"><?= ($_classLevelRecord['Quota'] - $_classLevelRecord['Applied']) ?></span>)
			</td>
			<td><?= /*($_classLevelRecord['Applied'] > 0)?*/
				'<a href="#/apps/admission/interview/details/' . ($selectInterviewRound ? $selectInterviewRound : 1) . '/' . $_classLevelRecord['RecordID'] . '/">' . $_classLevelRecord['Applied'] . '</a>'/*:$_classLevelRecord['Applied']*/ ?>
				<input type="hidden" name="applied_<?= $_classLevelRecord['RecordID'] ?>"
				       id="applied_<?= $_classLevelRecord['RecordID'] ?>"
				       value="<?= $_classLevelRecord['Applied'] ?>">
			</td>
			<td>
				<?= $libinterface->Get_Checkbox('interview_' . $_classLevelRecord['RecordID'], 'interviewAry[]', $_classLevelRecord['RecordID'], '', $Class = '', $Display = '', $Onclick = "unset_checkall(this, document.getElementById('interview_list'));", $Disabled = '') ?>

				<?php if (count((array)$interviewListAry[$_classLevelRecord['RecordID']])): ?>
					<input type="hidden" id="applicant_ids_<?= $_classLevelRecord['RecordID'] ?>"
					       name="applicant_ids_<?= $_classLevelRecord['RecordID'] ?>"
					       value="<?= implode(',', (array)$interviewListAry[$_classLevelRecord['RecordID']]) ?>"/>
				<?php endif; ?>
			</td>
		</tr>
	<? //}
	endforeach;
	if ($idx == 0) {
		?>
		<tr>
			<td colspan="7" style="text-align:center;"><?= $kis_lang['norecord'] ?></td>
		</tr>
	<? } ?>
	</tbody>
</table>
