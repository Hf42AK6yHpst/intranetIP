<table class="common_table_list edit_table_list">
	<colgroup>
		<col nowrap="nowrap">
	</colgroup>
	<thead>
	<tr>
		<th width="10">&nbsp;</th>
		<th width="10%"><?= $kis_lang['Admission']['JOYFUL']['Campas'] ?></th>
		<th width="20%"><?= $kis_lang['date'] ?></th>
		<th width="40%"><?= $kis_lang['timeslot'] ?></th>
		<th width="5%"><?= $admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['numofroom'] : $kis_lang['numofgroup'] ?></th>
		<th width="5%"><?= $kis_lang['qouta'] ?></th>
		<th width="20%">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$x = '';
	if (sizeof($interviewArrangementRecords) > 0) {
		$index = 0;
		foreach ($interviewArrangementRecords as $key => $record) {
			$index++;
			$x .= '<tr id="row_' . $record['RecordID'] . '">';
			$x .= '<td class="Dragable">' . $index . '</td>';
			$x .= "<td class=\"Dragable\">{$kis_lang['Admission']['JOYFUL']['SchoolType'][$record['Extra']]}</td>";
			$x .= '<td class="Dragable"><a class="td_form_name" href="#/apps/admission/interview/timeslotsettings/' . $selectInterviewRound . '/edit/' . $record['RecordID'] . '">' . $record['Date'] . '</a></td>';

			$timeslotText = '';
			for ($i = 0; $i < sizeof($record['StartTime']); $i++) {
				$timeslotText .= substr($record['StartTime'][$i], 0, -3) . ' ~ ' . substr($record['EndTime'][$i], 0, -3) . '<br/>';
			}

			$x .= '<td class="Dragable">' . $timeslotText . '</td>';
			$x .= '<td class="Dragable">' . $record['NumOfGroup'] . '</td>';
			$x .= '<td class="Dragable">' . $record['Quota'] . '</td>';
			$x .= '<td class="Dragable">';
			$x .= '<div class="table_row_tool">';
			$x .= '<a title="' . $kis_lang['edit'] . '" class="edit_dim" href="#/apps/admission/interview/timeslotsettings/' . $selectInterviewRound . '/edit/' . $record['RecordID'] . '" ></a>';
			$x .= '<a title="' . $kis_lang['delete'] . '" class="delete_dim" href="javascript:void(0);"><input type="hidden" name="TemplateID[]" value="' . $record['RecordID'] . '" /></a>';
			$x .= '</div>';
			$x .= '</td>';
			$x .= '</tr>';
		}
	} else {
		$x .= '<tr><td colspan="6" style="text-align:center;">' . $kis_lang['norecord'] . '</td></tr>';
	}
	echo $x;
	?>
	</tbody>
</table>
