<?php
//using:
/*
 * This page is for admission only. For general KIS config : kis/config.php
 */


//Please also define lang in admission_lang
$admission_cfg['Status'] = array();
$admission_cfg['Status']['pending'] = 1;
$admission_cfg['Status']['paymentsettled'] = 2;
$admission_cfg['Status']['CREATIVE_approved'] = 8;
$admission_cfg['Status']['CREATIVE_datamissing'] = 9;
$admission_cfg['Status']['CREATIVE_cancelled'] = 11;
$admission_cfg['Status']['gotofirstinterview'] = 3;
$admission_cfg['Status']['gotosecondinterview'] = 6;
$admission_cfg['Status']['gotothirdinterview'] = 7;
$admission_cfg['Status']['confirmed'] = 4;
//     $admission_cfg['Status']['cancelled']	= 5;

$admission_cfg['PaymentStatus']['OnlinePayment'] = 1;
$admission_cfg['PaymentStatus']['OtherPayment'] = 2;

$admission_cfg['InterviewStatus']['AlreadyInterviewed'] = 1;
$admission_cfg['InterviewStatus']['Absent1'] = 2;
$admission_cfg['InterviewStatus']['Absent2'] = 3;
$admission_cfg['InterviewStatus']['Withdrawn'] = 4;
$admission_cfg['InterviewStatus']['NotUpdated'] = 5;

$admission_cfg['AdmitStatus']['Admitted'] = 1;
$admission_cfg['AdmitStatus']['WaitListed'] = 2;
$admission_cfg['AdmitStatus']['NotAdmitted'] = 3;
$admission_cfg['AdmitStatus']['NotUpdated'] = 4;


 	$admission_cfg['BirthCertType'] = array();
 	$admission_cfg['BirthCertType']['hkid']	= 1;
// 	$admission_cfg['BirthCertType']['oversea']	= 2;
// 	$admission_cfg['BirthCertType']['mainland']	= 3;
// 	$admission_cfg['BirthCertType']['others']	= 4;
 	$admission_cfg['BirthCertType']['passport']	= 5;

// ####### Cust config START ########
$admission_cfg['SchoolName']['en'] = '心怡天地國際幼兒園暨幼稚園';
$admission_cfg['SchoolName']['b5'] = 'Joyful World International Nursery & Kindergarten';
$admission_cfg['AdmissionSystemTitle']['en'] = '網上入學申請系統';
$admission_cfg['AdmissionSystemTitle']['b5'] = 'Online Admission System';

$admission_cfg['SchoolType'] = array();
$admission_cfg['SchoolType'][0] = 'BD';
$admission_cfg['SchoolType'][1] = 'TK';
$admission_cfg['SchoolType'][2] = 'ST';
$admission_cfg['SchoolType'][3] = 'YL';
$admission_cfg['SchoolType'][4] = 'TM';

$admission_cfg['District'] = array();
$admission_cfg['District'][] = '=地區 --=-- District';
$admission_cfg['District'][] = '0=中西區=Central and Western';
$admission_cfg['District'][] = '1=東區=Eastern';
$admission_cfg['District'][] = '2=南區=Southern';
$admission_cfg['District'][] = '3=灣仔=Wan Chai';
$admission_cfg['District'][] = '4=深水埗=Sham Shui Po';
$admission_cfg['District'][] = '5=九龍城=Kowloon City';
$admission_cfg['District'][] = '6=觀塘=Kwun Tong';
$admission_cfg['District'][] = '7=黃大仙=Wong Tai Sin';
$admission_cfg['District'][] = '8=油尖旺=Yau Tsim Mong';
$admission_cfg['District'][] = '9=離島區=Islands';
$admission_cfg['District'][] = '10=葵青=Kwai Tsing';
$admission_cfg['District'][] = '11=北區=North';
$admission_cfg['District'][] = '12=西貢=Sai Kung';
$admission_cfg['District'][] = '13=沙田=Sha Tin';
$admission_cfg['District'][] = '14=大埔=Tai Po';
$admission_cfg['District'][] = '15=荃灣=Tsuen Wan';
$admission_cfg['District'][] = '16=屯門=Tuen Mun';
$admission_cfg['District'][] = '17=元朗=Yuen Long';

$admission_cfg['Region'] = array();
$admission_cfg['Region'][] = '=地域 --=-- Region';
$admission_cfg['Region'][] = '0=香港島=Hong Kong Island';
$admission_cfg['Region'][] = '1=九龍=Kowloon';
$admission_cfg['Region'][] = '2=新界=New Territories';
//$admission_cfg['Region'][] = '3=離島區=Islands';


$admission_cfg['themeStyle'] = 'Kindergarten';
// 	$admission_cfg['themeStyle'] = 'Primary';
// 	$admission_cfg['themeStyle'] = 'Secondary';
// ####### Cust config END ########


/* for email [start] */
if ($plugin['eAdmission_devMode']) {
//    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
    $admission_cfg['EmailBcc'] = '';
}else{
    $admission_cfg['EmailBcc'] = '';
}
/* for email [end] */

/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    $admission_cfg['paypal_signature'] = 'W55x8ky_kk6d23a_YonKtmvDHBIbnQ-XCGFhUg4pg1KzlUN4aLjDBscZMni';
    $admission_cfg['hosted_button_id'] = '4MZKGPEXHK9NN';
    $admission_cfg['paypal_name'] = "Joyful English Limited Testing";
} else {
    $admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
    $admission_cfg['paypal_signature'] = 'JudOiac0tfqs7OwQbtXin3U4Iyktol8TvLimzZRXaE7DSjvx-cS_Nf6UW8O';
    $admission_cfg['hosted_button_id'] = '7L84FGJ7DC86G';
    $admission_cfg['paypal_name'] = 'Joyful English Limited';
}
/* for paypal [End] */


$admission_cfg['PrintByPDF'] = 1;
$admission_cfg['FilePath'] = $PATH_WRT_ROOT . "/file/admission/";
$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
$admission_cfg['DefaultLang'] = "b5";
$admission_cfg['Lang'] = array();
$admission_cfg['Lang'][1] = 'en';
$admission_cfg['Lang'][2] = 'b5';
//$admission_cfg['MultipleLang'] = count($admission_cfg['Lang']) > 1;
$admission_cfg['HideSelectLang'] = true;
$admission_cfg['IsBilingual'] = true;
$admission_cfg['maxUploadSize'] = 3; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
$admission_cfg['personal_photo_width'] = 200;
$admission_cfg['personal_photo_height'] = 260;

if ($plugin['eAdmission_devMode']) {
    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
} else {
    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
}
