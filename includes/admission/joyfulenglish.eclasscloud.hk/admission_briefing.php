<?php

class admission_briefing extends admission_briefing_base{
    function getBriefingChar($BriefingID){
        $briefing = $this->getBriefingSession($BriefingID);
        $briefingArr = $this->getAllBriefingSession($briefing['SchoolYearID']);
        $charArr = range('A', 'Z');
        foreach($briefingArr as $index=>$briefing){
            if($briefing['BriefingID'] != $BriefingID){
                continue;
            }
            return $charArr[$index];
        }
        return 'A';
    }
    
	function generateApplicantID($schoolYearID){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$schoolYearID)), -2);
		
		$sql = "SELECT 
			MAX(SUBSTR(ABAI.ApplicantID, 2)) 
		FROM 
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		AND
		    ABS.SchoolYearID = '{$schoolYearID}'";
		$rs = $this->returnVector($sql);
		
		if($rs[0] == null){
			return "{$yearStart}00001";
		}
		return ((int)$rs[0]) + 1;
	}

	function checkBriefingDuplicate($schoolYearID, $Data){
	    $sql = "SELECT 
	        * 
	    FROM
	        ADMISSION_BRIEFING_SETTING ABS
        INNER JOIN
	        ADMISSION_BRIEFING_APPLICANT_INFO ABAI
        ON
            ABS.BriefingID = ABAI.BriefingID
	    WHERE 
	        ABS.SchoolYearID='{$schoolYearID}' 
	    AND 
	        ABAI.Email='{$Data['Email']}'";
	    if($this->returnVector($sql)){
	        return true;
	    }
	    return false;
	}
    function insertBriefingApplicant($schoolYearID, $Data){
        $Briefing_ApplicantID = parent::insertBriefingApplicant($schoolYearID, $Data);
        
        $briefingChar = $this->getBriefingChar($Data['BriefingID']);
        
        $sql = "UPDATE
            ADMISSION_BRIEFING_APPLICANT_INFO
        SET
            DOB='{$_POST['StudentYearOfBirth']}-01-01',
            ApplicantID=CONCAT('{$briefingChar}', ApplicantID)
        WHERE
            Briefing_ApplicantID='{$Briefing_ApplicantID}'";
        $result = $this->db_db_query($sql);
        
        return $Briefing_ApplicantID;
    }

    function getBriefingSessionUsedQuota($BriefingID){ // Skip the quota checking
	    global $admission_cfg;
		$sql = "SELECT 
			SUM(SeatRequest) 
		FROM
			ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
			BriefingID = '{$BriefingID}'
		AND
			DateDeleted IS NULL
		AND
		    Status = '{$admission_cfg['BriefingStatus']['confirmed']}'
		";
		$rs = $this->returnVector($sql);
		$usedQuota = $rs[0];

		return $usedQuota;
	}
	
	function getAllBriefingSessionUsedQuota($schoolYearID = ''){ // Skip the quota checking
	    global $admission_cfg;
	    
		$rs = $this->getAllBriefingSession($schoolYearID);
		$BriefingIdArr = Get_Array_By_Key($rs, 'BriefingID');
		$BriefingIdSql = implode("','", $BriefingIdArr);

		$sql = "SELECT
		    BriefingID,
			SUM(SeatRequest) AS RemainsQuota
		FROM
		    ADMISSION_BRIEFING_APPLICANT_INFO
		WHERE
		    BriefingID IN ('{$BriefingIdSql}')
		AND
		    Status = '{$admission_cfg['BriefingStatus']['confirmed']}'
		GROUP BY
		    BriefingID
		";
		$rs = $this->returnResultSet($sql);
		
		$quotas = BuildMultiKeyAssoc($rs, array('BriefingID') , array('RemainsQuota'), 1);
		
		return $quotas;
	}


	function briefingApplicantLottery($schoolYearID, $data = ''){
	    global $admission_cfg, $libkis_admission;
		
	    $result = true;
	    $allBriefingSession = $this->getAllBriefingSession($schoolYearID);
		$BriefingIdArr = Get_Array_By_Key($allBriefingSession, 'BriefingID');
		$BriefingIdSql = implode("','", $BriefingIdArr);
	    
		######## Reset all applicant status to cancelled START ########
		$sql = "UPDATE 
		    ADMISSION_BRIEFING_APPLICANT_INFO 
		SET 
		    Status='{$admission_cfg['BriefingStatus']['cancelled']}'
	    WHERE
	        BriefingID IN ('{$BriefingIdSql}')
		";
		$result = $result && $this->db_db_query($sql);
		######## Reset all applicant status to cancelled END ########
		
		######## Get YearOfBirth of P1 admission settings START ########
		$p1YearOfBirthStart = '0000';
		$p1YearOfBirthEnd = '9999';
		$rs = $libkis_admission->getApplicationSetting($schoolYearID);
		foreach($rs as $classLevelId => $r){
		    if(strtoupper($r['ClassLevelName']) != 'P1'){
		        continue;
		    }
		    $p1YearOfBirthStart = substr($r['DOBStart'], 0, 4);
		    $p1YearOfBirthEnd = substr($r['DOBEnd'], 0, 4);
		}
		######## Get YearOfBirth of P1 admission settings END ########
		
		######## Get all applicant info START ########
		$sql = "SELECT 
    		Briefing_ApplicantID,
    		BriefingID, 
    		SeatRequest,
    		DOB
		FROM 
		    ADMISSION_BRIEFING_APPLICANT_INFO
	    WHERE
	        BriefingID IN ('{$BriefingIdSql}')";
		$rs = $this->returnResultSet($sql);
		
		foreach($rs as $index=>$r){
		    $yob = substr($r['DOB'], 0, 4);
		    $rs[$index]['isYearOfBirthMatch'] = ($p1YearOfBirthStart <= $yob && $yob <= $p1YearOfBirthEnd);
		}
		
		$briefingSessionApplicantMapping = BuildMultiKeyAssoc($rs, 'BriefingID', array(
		    'Briefing_ApplicantID', 'SeatRequest', 'isYearOfBirthMatch',
		), 0, 1);
		######## Get all applicant info END ########
		
		
		######## Lottery START ########
		$lotteryMapping = array();
		foreach($allBriefingSession as $briefingSession){
		    $briefingId = $briefingSession['BriefingID'];
		    $totalQuota = $briefingSession['TotalQuota'];
		    $remainsQuota = $totalQuota;
		    
		    #### Get priority array START ####
		    $firstPriorityApplicant = array();
		    $secondPriorityApplicant = array();
		    foreach((array)$briefingSessionApplicantMapping[$briefingId] as $applicant){
		        if($applicant['isYearOfBirthMatch']){
		            $firstPriorityApplicant[] = $applicant;
		        }else{
		            $secondPriorityApplicant[] = $applicant;
		        }
		    }
		    shuffle($firstPriorityApplicant);
		    shuffle($secondPriorityApplicant);
		    #### Get priority array END ####
		    
		    #### Add applicant to result START ####
		    foreach($firstPriorityApplicant as $applicant){
		        $lotteryMapping[$briefingId][] = $applicant['Briefing_ApplicantID'];
		        $remainsQuota -= $applicant['SeatRequest'];
		        
		        if($remainsQuota <= 0){
		            break;
		        }
		    }
		    
		    if($remainsQuota > 0){
    		    foreach($secondPriorityApplicant as $applicant){
    		        $lotteryMapping[$briefingId][] = $applicant['Briefing_ApplicantID'];
    		        $remainsQuota -= $applicant['SeatRequest'];
    		        
    		        if($remainsQuota <= 0){
    		            break;
    		        }
    		    }
		    }
		    #### Add applicant to result END ####
		    
		    #### Save result to DB START ####
		    foreach($lotteryMapping as $briefingId => $applicantIds){
		        $applicantIdSql = implode("','", (array)$applicantIds);
		        
		        $sql = "UPDATE
		            ADMISSION_BRIEFING_APPLICANT_INFO
		        SET
		            Status='{$admission_cfg['BriefingStatus']['confirmed']}'
		        WHERE
		            Briefing_ApplicantID IN ('{$applicantIdSql}')
		        ";
		        $result = $result && $this->db_db_query($sql);
		    }
		    #### Save result to DB END ####
		}
		######## Lottery END ########
		
		return $result;
	}
	


//	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = '')
//	{
//	    global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $Lang;
//	    include_once($intranet_root."/includes/libwebmail.php");
//	    $libwebmail = new libwebmail();
//	
//	    $from = $libwebmail->GetWebmasterMailAddress();
//	    $result = array();
//	
//	    ######## Get Applicant Info START ########
//	    $applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
//	    $Email = ($email)?$email:$applicant['Email'];
//	    $ApplicantID = $applicant['ApplicantID'];
//	    $DeleteRecordPassKey = $applicant['DeleteRecordPassKey'];
//	    ######## Get Applicant Info END ########
//	
//	    ######## Setup Email Content START ########
//	    if($subject == ''){
//	        $email_subject = "簡介會申請通知 Briefing Session Application Notification";
//	    }
//	    else{
//	        $email_subject = $subject;
//	    }
//	
//	    if($message == ''){
//	        if($Briefing_ApplicantID == 0){
//	            return true;
//	            /* Fail case no email * /
//	             $email_message = <<<EMAIL
//	             {$Lang['Admission']['msg']['applicationnotcomplete']}
//	             {$Lang['Admission']['msg']['tryagain']}
//	             <br /><br />
//	             Application is Not Completed. Please try to apply again!
//	             EMAIL;
//	             /* */
//	        }else{
//	            $protocol = (checkHttpsWebProtocol())? 'https':'http';
//	            $viewResultLink = "{$protocol}://{$_SERVER['HTTP_HOST']}/kis/admission_briefing/briefing_result.php";
//	            
//	            $email_message = <<<EMAIL
//					<font color="green">
//						報名已遞交，申請編號為
//					</font>
//					<font size="5">
//						<u>{$ApplicantID}</u>
//					</font>
//			
//					<br />
//					    按此查看報名結果：
//					<br />
//					{$viewResultLink}
//					<br /><br />
//					<font color="green">
//						Application is Completed.
//						Your application number is
//					</font>
//					<font size="5">
//						<u>{$ApplicantID}</u>
//					</font>
//					<br />
//					Click here to view application result:
//					<br />
//					{$viewResultLink}
//EMAIL;
//	        }
//	    }else{
//	        $email_message = $message;
//	    }
//	    ######## Setup Email Content END ########
//	
//	    $sent_ok = true;
//	    if($Email != '' && intranet_validateEmail($Email)){
//	        $sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($Email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
//	    }else{
//	        $sent_ok = false;
//	    }
//	    	
//	    return $sent_ok;
//	}
	
	
	
	
	
	
} // End Class