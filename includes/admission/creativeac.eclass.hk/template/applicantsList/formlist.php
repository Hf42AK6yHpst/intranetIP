<?php
//modifying by Pun
/**
 *  2019-08-26 Pun
 *  - File created
 */
global $kis_admission_school;

list($page, $amount, $total, $sortby, $order, $keyword) = $kis_data['PageBar'];

//////// Cust status START ////////
//// InterviewStatus START ////
$interviewStatusArr = array();
foreach ($admission_cfg['InterviewStatus'] as $key => $value) {
    $interviewStatusArr[] = array(
        $value, $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$key]
    );
}
$interviewStatusSelect = getSelectByArray(
    $interviewStatusArr,
    ' name="applicantInterviewStatus"',
    $applicantInterviewStatus,
    0,
    0,
    "- {$kis_lang['Admission']['pleaseSelect']} -"
);
//// InterviewStatus END ////


//// AdmitStatus START ////
$admitStatusArr = array();
foreach ($admission_cfg['AdmitStatus'] as $key => $value) {
    $admitStatusArr[] = array(
        $value, $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$key]
    );
}
$admitStatusSelect = getSelectByArray(
    $admitStatusArr,
    ' name="AdmitStatus"',
    $AdmitStatus,
    0,
    0,
    "- {$kis_lang['Admission']['pleaseSelect']} -"
);
//// AdmitStatus END ////

//// RatingClass START ////
$ratingClassSelect = getSelectByValueDiffName(
    array_keys($kis_lang['Admission']['TimeSlot']),
    array_values($kis_lang['Admission']['TimeSlot']),
    ' name="RatingClass"',
    $RatingClass,
    1,
    0,
    "- {$kis_lang['Admission']['pleaseSelect']} -"
);
//// RatingClass END ////
//////// Cust status END ////////

?>
<style type="text/css">
    .search_advanced {
        float: right;
        margin: 10px;
    }

    .advancedSearchDiv {
        display: block;
        background: #e4e4e4;
        padding: 10px;
    }

    .advancedSearchContent {
        display: flex;
        justify-content: center;
        flex-direction: row;
        flex-wrap: wrap;
    }

    .advancedSearchContent div {
        min-width: 30%;
        /*border: 1px solid red;*/
        padding: 5px;
        align-items: center;
        width: 410px;
    }

    .advancedSearchContent div select {
        min-width: 160px;
    }

    .advancedSearchContent div span:first-child {
        width: 200px;
        display: inline-block;
    }

    .advancedSearchContent div span:nth-child(2) {
        display: inline-block;
        width: 200px;
        vertical-align: top;
    }

    .advancedSearchContent div span:nth-child(2) * {
        width: 100%;
    }

    .Content_tool a.parent_btn {
        background-color: #e9eef2;
        border: 1px solid #CCCCCC;
        border-bottom: none;
        z-index: 999999;
        position: static;
        width: auto;
    }

    /**btn sub option**/
    .Conntent_tool .btn_option_layer {
        margin-top: -1px;
        padding: 0px;
        display: block;
        position: absolute;
        visibility: hidden;
        line-height: 18px;
        z-index: 99;
        float: left;
        background-color: #e9eef2;
        border: 1px solid #CCCCCC
    }

    .Conntent_tool .btn_option_layer a.sub_btn {
        clear: both;
        background: url(../../../images/2009a/btn_sub.gif) no-repeat 0px 0px;
    }

    /****/
    select[disabled] {
        color: lightgrey;
    }
</style>
<script type="text/javascript">
    window.currentSchool = '<?=preg_replace('/\..*/', '', $kis_admission_school)?>';
    kis.admission.application_form_init({
        selectatleastonerecord: '<?=$kis_lang['Admission']['msg']['selectatleastonerecord']?>',
        exportallrecordsornot: '<?=$kis_lang['Admission']['msg']['exportallrecordsornot']?>',
        printallrecordsornot: '<?=$kis_lang['Admission']['msg']['printallrecordsornot']?>',
        sendemailallrecordsornot: '<?=$kis_lang['Admission']['msg']['sendemailallrecordsornot']?>',
        importinterviewallrecordsornot: '<?=$kis_lang['Admission']['msg']['importinterviewallrecordsornot']?>',
        confirmresendnotificationemail: '<?=$kis_lang['msg']['confirmresendnotificationemail']?>',
        successresendnotificationemail: '<?=$kis_lang['msg']['successresendnotificationemail']?>',
        unsuccessresendnotificationemail: '<?=$kis_lang['msg']['unsuccessresendnotificationemail']?>'
    });

    function js_Clicked_Option_Layer(jsOptionLayerDivID, jsOptionLayerBtnID) {
        js_Hide_All_Option_Layer(jsOptionLayerDivID);
        js_ShowHide_Layer(jsOptionLayerDivID);

        if ($('div#' + jsOptionLayerDivID).css('visibility') == 'visible')
            $('a#' + jsOptionLayerBtnID).addClass('parent_btn');
        else
            $('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
    }

    function js_Clicked_Option_Layer_Button(jsOptionLayerDivID, jsOptionLayerBtnID) {
        MM_showHideLayers(jsOptionLayerDivID, '', 'hide');
        $('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
    }

    // var funcFilterParams = funcFilterParams || kis.setFilterParams;
    // kis.setFilterParams = (function () {
    //     $('#advancedSearchForm').submit();
    //     return false;
    // });
    $('#advancedSearchForm').submit(function (e) {
        e.preventDefault();
        $('[name="isAdvanceSearch"]').val(+$('.advancedSearchDiv').is(':visible'));
        // $('.filter_form').appendTo($(this).find('.otherFields'));
        // funcFilterParams($(this).serialize());
    });

    $('#application_status_form .toggleDisable').click(function () {
        $(this).closest('tr').find('select').prop('disabled', !$(this).prop('checked'));
    });
    $('#application_status_form select').prop('disabled', true);
    $('#application_status_form').off('submit').submit(function () {
        var applicationIds = $.map($("input[name='applicationAry\[\]']:checked"), function (n) {
            return n.value;
        }).join(',');

        $(this).find('[name="applicationIds"]').val(applicationIds);
        kis.showLoading();
        $.post('apps/admission/ajax.php?action=updateApplicationStatusByIds', $(this).serialize(), function (data) {
            parent.$.fancybox.close();
            kis.hideLoading().loadBoard();
        });
        return false;
    });
</script>
<div class="main_content">
    <?= $kis_data['NavigationBar'] ?>
    <p class="spacer"></p>
    <? if (!empty($warning_msg)): ?>
        <?= $warning_msg ?>
    <? endif; ?>
    <p class="spacer"></p>

    <input type="button" class="formsubbutton search_advanced" value="<?= $kis_lang['advanced'] ?>"
           onclick="$('.advancedSearchDiv').slideToggle()"/>
    <form class="filter_form">
        <div class="search">
            <input placeholder="<?= $kis_lang['search'] ?>" name="keyword" value="<?= $keyword ?>" type="text">
        </div>
    </form>

    <div class="Content_tool">

        <div class="btn_option" id="ExportDiv">

            <a onclick="js_Clicked_Option_Layer('export_option', 'btn_export');" id="btn_export"
               class="export option_layer" href="javascript:void(0);"><?= $kis_lang['export'] ?></a>
            <br style="clear: both;">
            <div onclick="js_Clicked_Option_Layer_Button('export_option', 'btn_export');" id="export_option"
                 class="btn_option_layer" style="visibility: hidden;">
                <a href="javascript:void(0);" onclick="js_Clicked_Option_Layer('export_option', 'btn_export');"
                   class="sub_btn tool_export"><?= $kis_lang['applicationformdata'] ?>&nbsp;</a>
                <a href="javascript:void(0);" onclick="js_Clicked_Option_Layer('export_option', 'btn_export');"
                   class="sub_btn tool_export_stu_acc"><?= $kis_lang['exportforimportstuacc'] ?>&nbsp;</a>
                <a href="javascript:void(0);" onclick="js_Clicked_Option_Layer('export_option', 'btn_export');"
                   class="sub_btn tool_export_prt_acc"><?= $kis_lang['exportforimportprtacc'] ?>&nbsp;</a>
            </div>
            &nbsp;
        </div>
        <div class="btn_option" id="ExportDiv">
            <a id="btn_import" class="tool_download_application_attachment export" href="javascript:void(0);">
                <?= $kis_lang['downloadapplicationattachment'] ?>
            </a>
        </div>
    </div>

    <p class="spacer"></p>
    <div class="advancedSearchDiv" style="display: <?= ($isAdvanceSearch) ? 'block' : 'none' ?>;">
        <form id="advancedSearchForm" class="advancedSearchForm filter_form">
            <input type="hidden" name="isAdvanceSearch" value="0"/>
            <div class="advancedSearchContent">
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'] ?></span>
                    <span><?= $interviewStatusSelect ?></span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'] ?></span>
                    <span>
                        <select name="RegisterEmail">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="1" <?= ($RegisterEmail === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['yes'] ?>
                            </option>
                            <option value="0" <?= ($RegisterEmail === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'] ?></span>
                    <span><?= $admitStatusSelect ?></span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?></span>
                    <span><?= $ratingClassSelect ?></span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'] ?></span>
                    <span>
                        <select name="PaidReservedFee">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="0" <?= ($PaidReservedFee === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1" <?= ($PaidReservedFee === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'] ?></span>
                    <span>
                        <select name="PaidRegistrationCertificate">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="0" <?= ($PaidRegistrationCertificate === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1" <?= ($PaidRegistrationCertificate === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'] ?></span>
                    <span>
                        <select name="BookFeeEmail">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="1" <?= ($BookFeeEmail === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['yes'] ?>
                            </option>
                            <option value="0" <?= ($BookFeeEmail === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'] ?></span>
                    <span>
                        <select name="PaidBookFee">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="0" <?= ($PaidBookFee === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1" <?= ($PaidBookFee === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'] ?></span>
                    <span>
                        <select name="OpeningNoticeEmail">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="1" <?= ($OpeningNoticeEmail === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['yes'] ?>
                            </option>
                            <option value="0" <?= ($OpeningNoticeEmail === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'] ?></span>
                    <span>
                        <select name="PaidTuitionFee">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="0" <?= ($PaidTuitionFee === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1" <?= ($PaidTuitionFee === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div>
                    <span><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'] ?></span>
                    <span>
                        <select name="GiveUpOffer">
                            <option value=""><?= "- {$kis_lang['Admission']['pleaseSelect']} -" ?></option>
                            <option value="1" <?= ($GiveUpOffer === '1') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['yes'] ?>
                            </option>
                            <option value="0" <?= ($GiveUpOffer === '0') ? 'selected' : '' ?>>
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </span>
                </div>
                <div><span>&nbsp;</span><span>&nbsp;</span></div>
                <div><span>&nbsp;</span><span>&nbsp;</span></div>
                <div><span>&nbsp;</span><span>&nbsp;</span></div>
            </div>

            <p class="spacer"></p>
            <div class="edit_bottom">
                <input type="submit" value="<?= $kis_lang['submit'] ?>" class="formbutton">
            </div>
            <div class="otherFields"></div>
        </form>
    </div>


    <p class="spacer"></p>
    <div class="table_board">

        <!------ Table header START ------>
        <div style="display: flex;justify-content: space-between;align-items: flex-end;">
            <div id="table_filter" style="padding-bottom: 3px;">
                <form class="filter_form">
                    <?= $classLevelSelection ?>
                    <?= $applicationStatus ?>
                    <?= $paymentStatus ?>
                    <input type="hidden" name="selectSchoolYearID" id="selectSchoolYearID"
                           value="<?= $schoolYearID ?>">
                </form>
            </div>
            <? if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission-interview-marker"]) { ?>
                <div class="common_table_tool common_table_tool_table">
                    <a href="#" class="tool_print tool_print_form">
                        <?= $kis_lang['print'] ?>
                    </a>
                </div>
            <? } else { ?>
                <div class="common_table_tool common_table_tool_table">
                    <a href="#" class="tool_email tool_resend_email">
                        <?= $kis_lang['sendnotificationemail'] ?>
                    </a>
                    <a href="#" class="tool_email tool_send_email">
                        <?= $kis_lang['sendemail'] ?>
                    </a>
                    <a href="#" class="tool_set_class">
                        <?= $kis_lang['changeclasslevel'] ?>
                    </a>
                    <a href="#" class="tool_set">
                        <?= $kis_lang['changestatus'] ?>
                    </a>
                    <a href="#" class="tool_print tool_print_form">
                        <?= $kis_lang['print'] ?>
                    </a>
                </div>
            <? } ?>
        </div>
        <p class="spacer"></p>
        <!------ Table header END ------>


        <form id="application_form" name="application_form" method="post">
            <div id="advancedSearch" style="display: none;"></div>
            <table class="common_table_list edit_table_list">
                <colgroup>
                    <col nowrap="nowrap">
                </colgroup>
                <thead>
                <tr>
                    <th width="20">#</th>
                    <th><? kis_ui::loadSortButton('application_id', 'applicationno', $sortby, $order) ?></th>
                    <th><? kis_ui::loadSortButton('student_name', 'studentname', $sortby, $order) ?></th>

                    <th><?= $kis_lang['parentorguardian'] ?></th>
                    <th><?= $kis_lang['phoneno'] ?></th>
                    <th><? kis_ui::loadSortButton('application_status', 'status', $sortby, $order) ?></th>
                    <th><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?></th>
                    <th><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'] ?></th>
                    <th><?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'] ?></th>
                    <th><? kis_ui::loadSortButton('DateInput', 'enrolmenttime', $sortby, $order) ?></th>
                    <th>
                        <input
                                type="checkbox"
                                name="checkmaster"
                                onclick="(this.checked)?setChecked(1,this.form,'applicationAry[]'):setChecked(0,this.form,'applicationAry[]')"
                        >
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($total > 0) {
                    $idx = $amount * ($page - 1);
                    foreach ($applicationDetails as $_applicationId => $_applicationDetailsAry) {
                        $idx++;
                        $_recordId = $_applicationDetailsAry['record_id'];
                        $_recordDateInput = $_applicationDetailsAry['DateInput'];
                        $_status = $_applicationDetailsAry['application_status'];
                        $_studentName = $_applicationDetailsAry['student_name'];
                        $_student_hkid = $_applicationDetailsAry['student_hkid'];
                        $_student_strn = $_applicationDetailsAry['student_strn'];
                        $_parentName = implode('<br/>', array_filter($_applicationDetailsAry['parent_name']));
                        $_parentPhone = implode('<br/>', array_filter($_applicationDetailsAry['parent_phone']));

                        $_record_CustRatingClass = $_applicationDetailsAry['Cust_RatingClass'];
                        $_record_Cust_InterviewStatus = $_applicationDetailsAry['Cust_InterviewStatus'];
                        $_record_Cust_AdmitStatus = $_applicationDetailsAry['Cust_AdmitStatus'];
                        $RatingClassLang = $kis_lang['Admission']['TimeSlot'][$_record_CustRatingClass];
                        $InterviewStatus = array_flip($admission_cfg['InterviewStatus']);
                        $InterviewStatus = $InterviewStatus[$_record_Cust_InterviewStatus];
                        $InterviewStatusLang = $kis_lang['Admission']['CREATIVE']['InterviewStatus'][$InterviewStatus];
                        $AdmitStatus = array_flip($admission_cfg['AdmitStatus']);
                        $AdmitStatus = $AdmitStatus[$_record_Cust_AdmitStatus];
                        $AdmitStatusLang = $kis_lang['Admission']['CREATIVE']['AdmitStatus'][$AdmitStatus];

                        $tr_css = '';
                        switch ($_status) {
                            case 'pending':
                                $tr_css = ' class="absent"';
                                break;
                            case 'waitingforinterview':
                                $tr_css = ' class="waiting"';
                                break;
                            case 'confirmed':
                                $tr_css = ' class="done"';
                                break;
                            case 'cancelled':
                                $tr_css = ' class="draft"';
                                break;
                        }
                        ?>
                        <tr<?= $tr_css ?>>
                            <td><?= $idx ?></td>
                            <td>
                                <a href="#/apps/admission/applicantslist/details/<?= $schoolYearID ?>/<?= $_recordId ?>/">
                                    <?= $_applicationId ?>
                                </a>
                            </td>
                            <td><?= $_studentName ?></td>

                            <td><?= $_parentName ? $_parentName : '--' ?></td>
                            <td nowrap="nowrap"><?= $_parentPhone ? $_parentPhone : '--' ?></td>
                            <td><?= $kis_lang['Admission']['Status'][$_status] ?></td>
                            <td><?= $RatingClassLang ? $RatingClassLang : '--' ?></td>
                            <td><?= $InterviewStatusLang ? $InterviewStatusLang : '--' ?></td>
                            <td><?= $AdmitStatusLang ? $AdmitStatusLang : '--' ?></td>
                            <td><?= $_recordDateInput ?></td>
                            <td>
                                <?= $libinterface->Get_Checkbox(
                                    'application_' . $_recordId,
                                    'applicationAry[]',
                                    $_recordId,
                                    '',
                                    $Class = '',
                                    $Display = '',
                                    $Onclick = "unset_checkall(this, document.getElementById('application_form'));",
                                    $Disabled = ''
                                ) ?>
                            </td>
                        </tr>
                    <?php }
                } else { //no record
                    ?>
                    <tr>
                        <td colspan="7" style="text-align:center;"><?= $kis_lang['norecord'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <input type="hidden" name="schoolYearID" id="schoolYearID" value="<?= $schoolYearID ?>">
            <input type="hidden" name="classLevelID" id="classLevelID" value="<?= $classLevelID ?>">
        </form>
        <p class="spacer"></p>
        <? kis_ui::loadPageBar($page, $amount, $total, array(10, 20, 50, 100, 500, 1000)) ?>
        <p class="spacer"></p><br>
    </div>
</div>

<!--FancyBox-->

<div id='edit_box' style="padding:5px;display:none;" class="pop_edit">
    <form id="application_status_form" method="post">
        <input type="hidden" name="applicationIds" value=""/>
        <div class="pop_title">
            <span><?= $kis_lang['changestatus'] ?></span>
        </div>
        <div class="table_board" style="height:330px">

            <table class="form_table">

                <tr>
                    <td width="1" class="field_title">
                        <input type="hidden" name="status" value=""/>
                        <input type="checkbox" class="toggleDisable" id="statusForm_applicationstatus"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_applicationstatus">
                            <?= $kis_lang['Admission']['applicationstatus'] ?>
                        </label>
                    </td>
                    <td width="70%"><?= $statusSelection ?></td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_InterviewStatus"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_InterviewStatus">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['InterviewStatus'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <?= getSelectByArray(
                            $interviewStatusArr,
                            ' name="applicantInterviewStatus"',
                            $applicantInterviewStatus,
                            0,
                            1
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_RegisterEmail"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_RegisterEmail">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RegisterEmail'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="RegisterEmail">
                            <option value="1">
                                <?= $kis_lang['Admission']['yes'] ?>
                                (<?= $kis_lang['Admission']['date'] ?>: <?= date('Y-m-d') ?>)
                            </option>
                            <option value="0">
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_AdmitStatus"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_AdmitStatus">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['AdmitStatus'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <?= getSelectByArray(
                            $admitStatusArr,
                            ' name="AdmitStatus"',
                            $AdmitStatus,
                            0,
                            1
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_RatingClass"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_RatingClass">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['RatingClass'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <?= getSelectByValueDiffName(
                            array_keys($kis_lang['Admission']['TimeSlot']),
                            array_values($kis_lang['Admission']['TimeSlot']),
                            ' name="RatingClass"',
                            $RatingClass,
                            1,
                            1
                        ); ?>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_PaidReservedFee"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_PaidReservedFee">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidReservedFee'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="PaidReservedFee">
                            <option value="0">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_PaidRegistrationCertificate"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_PaidRegistrationCertificate">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidRegistrationCertificate'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="PaidRegistrationCertificate">
                            <option value="0">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_BookFeeEmail"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_BookFeeEmail">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['BookFeeEmail'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="BookFeeEmail">
                            <option value="1">
                                <?= $kis_lang['Admission']['yes'] ?>
                                (<?= $kis_lang['Admission']['date'] ?>: <?= date('Y-m-d') ?>)
                            </option>
                            <option value="0">
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_PaidBookFee"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_PaidBookFee">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidBookFee'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="PaidBookFee">
                            <option value="0">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_OpeningNoticeEmail"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_OpeningNoticeEmail">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['OpeningNoticeEmail'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="OpeningNoticeEmail">
                            <option value="1">
                                <?= $kis_lang['Admission']['yes'] ?>
                                (<?= $kis_lang['Admission']['date'] ?>: <?= date('Y-m-d') ?>)
                            </option>
                            <option value="0">
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_PaidTuitionFee"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_PaidTuitionFee">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['PaidTuitionFee'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="PaidTuitionFee">
                            <option value="0">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][0] ?>
                            </option>
                            <option value="1">
                                <?= $kis_lang['Admission']['CREATIVE']['Paid'][1] ?>
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="1" class="field_title">
                        <input type="checkbox" class="toggleDisable" id="statusForm_GiveUpOffer"/>
                    </td>
                    <td width="30%" class="field_title">
                        <label for="statusForm_GiveUpOffer">
                            <?= $kis_lang['Admission']['CREATIVE']['InterviewFollowUp']['GiveUpOffer'] ?>
                        </label>
                    </td>
                    <td width="70%">
                        <select name="GiveUpOffer">
                            <option value="1">
                                <?= $kis_lang['Admission']['yes'] ?>
                            </option>
                            <option value="0">
                                <?= $kis_lang['Admission']['no'] ?>
                            </option>
                        </select>
                    </td>
                </tr>

            </table>
            <p class="spacer"></p>
        </div>
        <div class="edit_bottom">
            <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                   value="<?= $kis_lang['submit'] ?>"/>
            <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                   value="<?= $kis_lang['cancel'] ?>"/>
        </div>

    </form>
</div>

<div id='edit_class' style="padding:5px;display:none;" class="pop_edit">
    <form id="application_class_form" method="post">
        <div class="pop_title">
            <span><?= $kis_lang['changeclass'] ?></span>
        </div>
        <div class="table_board" style="height:330px">

            <table class="form_table">
                <tr>
                    <td width="30%" class="field_title"><?= $mustfillinsymbol ?><?= $kis_lang['form'] ?></td>
                    <td width="70%"><?= $classChangeLevelSelection ?></td>
                </tr>
            </table>
            <p class="spacer"></p>
        </div>
        <div class="edit_bottom">
            <input id="submitBtn" name="submitBtn" type="submit" class="formbutton"
                   value="<?= $kis_lang['submit'] ?>"/>
            <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();"
                   value="<?= $kis_lang['cancel'] ?>"/>
        </div>

    </form>
</div>