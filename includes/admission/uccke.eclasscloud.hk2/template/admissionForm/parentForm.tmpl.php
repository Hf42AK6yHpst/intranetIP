
<section id="parentForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['PGInfo'] ?> <?=$LangEn['Admission']['PGInfo'] ?>
	</div>
	<div class="sheet">
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentName" id="ParentName" value="<?=$ParentInfo['G']['EnglishName'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['UCCKE']['NameOfParentGuardian'] ?> <?=$LangEn['Admission']['UCCKE']['NameOfParentGuardian'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['NameOfParentGuardian'] ?> <?=$LangEn['Admission']['UCCKE']['NameOfParentGuardian'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['EnglishName'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['EnglishName'] ? $ParentInfo['G']['EnglishName'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentRelationship" id="ParentRelationship" value="<?=$ParentInfo['G']['Relationship'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['relationship'] ?> <?=$LangEn['Admission']['relationship'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['relationship'] ?> <?=$LangEn['Admission']['relationship'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Relationship'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Relationship'] ? $ParentInfo['G']['Relationship'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentReligion" id="ParentReligion" value="<?=$allCustInfo['Parent_Religion'][0]['Value'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['religion'] ?>(<?=$LangB5['Admission']['ifAny'] ?>) <?=$LangEn['Admission']['religion'] ?>(<?=$LangEn['Admission']['ifAny'] ?>)</div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['religion'] ?>(<?=$LangB5['Admission']['ifAny'] ?>) <?=$LangEn['Admission']['religion'] ?>(<?=$LangEn['Admission']['ifAny'] ?>)</div>
				<div class="dataValue <?=$allCustInfo['Parent_Religion'][0]['Value'] ? '' : 'dataValue-empty'?>"><?=$allCustInfo['Parent_Religion'][0]['Value'] ? $allCustInfo['Parent_Religion'][0]['Value'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentEmail" id="ParentEmail" value="<?=$ParentInfo['G']['Email'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['contactEmail'] ?> <?=$LangEn['Admission']['contactEmail'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errParentEmail"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['contactEmail'] ?> <?=$LangEn['Admission']['contactEmail'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Email'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Email'] ? $ParentInfo['G']['Email'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" id="ParentEmailConfirm" value="<?=$ParentInfo['G']['Email'] ?>" required>
					<div class="textboxLabel requiredLabel"><?=$LangB5['Admission']['contactEmailConfirm'] ?> <?=$LangEn['Admission']['contactEmailConfirm'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errParentEmailConfirm"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['contactEmailConfirm'] ?> <?=$LangEn['Admission']['contactEmailConfirm'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Email'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Email'] ? $ParentInfo['G']['Email'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentAddress" id="ParentAddress" value="<?=$ParentInfo['G']['OfficeAddress'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['UCCKE']['EngAddress'] ?> <?=$LangEn['Admission']['UCCKE']['EngAddress'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark">(<?=$LangB5['Admission']['UCCKE']['DifferentAddress'] ?> <?=$LangEn['Admission']['UCCKE']['DifferentAddress'] ?>)</div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['EngAddress'] ?> <?=$LangEn['Admission']['UCCKE']['EngAddress'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['OfficeAddress'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['OfficeAddress'] ? $ParentInfo['G']['OfficeAddress'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentHomeTelNo" id="ParentHomeTelNo" value="<?=$ParentInfo['G']['OfficeTelNo'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> maxlength="8">
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['phome'] ?> <?=$LangEn['Admission']['UCCKE']['phome'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errParentHomeTelNo"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['phome'] ?> <?=$LangEn['Admission']['UCCKE']['phome'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['OfficeTelNo'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['OfficeTelNo'] ? $ParentInfo['G']['OfficeTelNo'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentMobileNo" id="ParentMobileNo" value="<?=$ParentInfo['G']['Mobile'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?> maxlength="8">
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['UCCKE']['mobile'] ?> <?=$LangEn['Admission']['UCCKE']['mobile'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
					<div class="remark remark-warn hide" id="errParentMobileNo"></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['UCCKE']['mobile'] ?> <?=$LangEn['Admission']['UCCKE']['mobile'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['Mobile'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['Mobile'] ? $ParentInfo['G']['Mobile'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ParentOccupation" id="ParentOccupation" value="<?=$ParentInfo['G']['JobTitle'] ?>" <?=$lac->isInternalUse($_GET['token'])?'':'required'?>>
					<div class="textboxLabel <?=$lac->isInternalUse($_GET['token'])?'':'requiredLabel'?>"><?=$LangB5['Admission']['occupation'] ?> <?=$LangEn['Admission']['occupation'] ?></div>
					<div class="remark remark-warn hide"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['occupation'] ?> <?=$LangEn['Admission']['occupation'] ?></div>
				<div class="dataValue <?=$ParentInfo['G']['JobTitle'] ? '' : 'dataValue-empty'?>"><?=$ParentInfo['G']['JobTitle'] ? $ParentInfo['G']['JobTitle'] : '－－'?></div>
			</span>
		</div>
		
		<div class="remark">* <?=$LangB5['Admission']['requiredFields'] ?> <?=$LangEn['Admission']['requiredFields'] ?></div>
	</div>
</section>

<script>
$(function(){
	'use strict';
	
	$('#parentForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($inputText.index($(this)))).html($(this).val());
			$($dataValue.get($inputText.index($(this)))).removeClass('dataValue-empty');
			if(!$($dataValue.get($inputText.index($(this)))).html()){
				$($dataValue.get($inputText.index($(this)))).html('－－');
				$($dataValue.get($inputText.index($(this)))).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$dataValue.html($inputRadio.next('label').html());
			$dataValue.removeClass('dataValue-empty');
			if(!$dataValue.html()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});
	});
	
	window.checkParentForm = (function(lifecycle){
		var isValid = true;
		var $parentForm = $('#parentForm');

		/******** Basic init START ********/
		//for email validation
		var re = /\S+@\S+\.\S+/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($parentForm);
		
		if($('#ParentEmail').val().trim()!='' && !re.test($('#ParentEmail').val())){
			if($('#ParentEmail').parent().find('.remark-warn').hasClass('hide')){
				$('#errParentEmail').html('<?=$LangB5['Admission']['icms']['msg']['invalidmailaddress']?> <?=$LangEn['Admission']['icms']['msg']['invalidmailaddress']?>');
				$('#errParentEmail').removeClass('hide');
				focusElement = $('#ParentEmail');
			}	
	    	isValid = false;
		}
		
		if($('#ParentEmailConfirm').val().trim()!='' && $('#ParentEmailConfirm').val() != $('#ParentEmail').val()){
			if($('#ParentEmailConfirm').parent().find('.remark-warn').hasClass('hide')){
				$('#errParentEmailConfirm').html('<?=$LangB5['Admission']['msg']['contactEmailMismatch']?> <?=$LangEn['Admission']['msg']['contactEmailMismatch']?>');
				$('#errParentEmailConfirm').removeClass('hide');
				focusElement = $('#ParentEmailConfirm');
			}	
	    	isValid = false;
		}
		
		if($('#ParentHomeTelNo').val().trim()!='' &&  !/^[0-9]{8}$/.test($('#ParentHomeTelNo').val())){
			if($('#ParentHomeTelNo').parent().find('.remark-warn').hasClass('hide')){
				$('#errParentHomeTelNo').html('<?=$LangB5['Admission']['msg']['invalidhomephoneformat']?> <?=$LangEn['Admission']['msg']['invalidhomephoneformat']?>');
				$('#errParentHomeTelNo').removeClass('hide');
				focusElement = $('#ParentHomeTelNo');
			}	
	    	isValid = false;
		}
		
		if($('#ParentMobileNo').val().trim()!='' &&  !/^[0-9]{8}$/.test($('#ParentMobileNo').val())){
			if($('#ParentMobileNo').parent().find('.remark-warn').hasClass('hide')){
				$('#errParentMobileNo').html('<?=$LangB5['Admission']['msg']['invalidmobilephoneformat']?> <?=$LangEn['Admission']['msg']['invalidmobilephoneformat']?>');
				$('#errParentMobileNo').removeClass('hide');
				focusElement = $('#ParentMobileNo');
			}	
	    	isValid = false;
		}
		/**** Check required END ****/

		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkParentForm);
});
</script>