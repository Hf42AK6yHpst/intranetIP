<?php

global $libkis_admission;


$StudentInfo = $applicationInfo;

?>
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$StudentInfo['ChineseSurname']?><?=$StudentInfo['ChineseFirstName']?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['englishname'] ?>
			</td>
			<td>
				<?=$StudentInfo['EnglishSurname']?>, <?=$StudentInfo['EnglishFirstName']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $kis_lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['DOB']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][ $StudentInfo['Gender'] ]) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['PICLC']['birthCertNo'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['BirthCertNo']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['PICLC']['PlaceOfIssue'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['PlaceOfBirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['nationality'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Nationality']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['PICLC']['nativePlace'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['PlaceOfBirthOther']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['PICLC']['homeAddress'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Address']) ?>
			</td>
		</tr>
		<!--<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['PICLC']['registedResidence'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['PlaceOfBirth']) ?>
			</td>
		</tr>-->
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['contactEmail'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Email']) ?>
			</td>
		</tr>
	
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
                	for($i=0;$i<sizeof($attachmentSettings);$i++) {
                		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
                		$_filePath = $attachmentList[$attachment_name]['link'];
						if ($_filePath) {

						    $tempAttachmentName = '';
					        if($kis_lang['Admission']['PICLC']['documentUpload'][$StudentInfo['BirthCertTypeOther']][$i]){
					            $tempAttachmentName .= $kis_lang['Admission']['PICLC']['documentUpload'][$StudentInfo['BirthCertTypeOther']][$i];
					        }
						    $attachment_name = ($tempAttachmentName)?$tempAttachmentName:$attachment_name;
						     
							//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
							$attachmentAry[] = '<a href="' . $_filePath . '" target="_blank">' . $attachment_name . '</a>';
						}
                	}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>                                                                                 
	</tbody>
</table>