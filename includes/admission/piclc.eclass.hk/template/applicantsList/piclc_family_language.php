<?php

global $libkis_admission;

$StudentInfo = current($libkis_admission->getApplicationStudentInfo($schoolYearID,'',$applicationInfo['applicationID']));
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>

<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Student']?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Mother']?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Father']?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['PICLC']['FirstLanguage']?> </td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['StudentFirstLanguage'][0]['Value'] ) ?>
		</td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['MotherFirstLanguage'][0]['Value'] ) ?>
		</td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['FatherFirstLanguage'][0]['Value'] ) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['PICLC']['SecondLanguage']?> </td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['StudentSecondLanguage'][0]['Value'] ) ?>
		</td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['MotherSecondLanguage'][0]['Value'] ) ?>
		</td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['FatherSecondLanguage'][0]['Value'] ) ?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$kis_lang['Admission']['PICLC']['ThirdLanguage']?> </td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['StudentThirdLanguage'][0]['Value'] ) ?>
		</td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['MotherThirdLanguage'][0]['Value'] ) ?>
		</td>
		<td class="form_guardian_field">
		<?= kis_ui::displayTableField( $allCustInfo['FatherThirdLanguage'][0]['Value'] ) ?>
		</td>
	</tr>
</table>

<!--<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="20%">
    	<col width="30%">
    	<col width="20%">
    </colgroup>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['LanguageSpokenAtHome']?> 
    	</td>
    	<td colspan="3">
			<?= kis_ui::displayTableField( $StudentInfo['LangSpokenAtHome'] ) ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['StudentFirstLanguageIsEnglish']?> 
    	</td>
    	<td colspan="3">
			<?php
			if($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'Y'){
			    echo kis_ui::displayTableField( $kis_lang['Admission']['yes'] );
			}else if($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N'){
			    echo kis_ui::displayTableField( $kis_lang['Admission']['no'] );
			}else{
			    echo kis_ui::displayTableField( '' ); 
			}
    
		    $display = ($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N')? 'display: block;' : 'display: none;';
    		?>
    		<div id="StudentFirstLanguageIsEnglishDiv" style="<?=$display ?>">
    			
                <table class="form_table" style="font-size: 13px">
                    <colgroup>
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                        <col style="width:200px;">
                    </colgroup>
                	<tr>
                		<td class="form_guardian_head" rowspan="2"><center><?=$kis_lang['Admission']['PICLC']['SkillAreas'] ?></center></td>
                		<td class="form_guardian_head" colspan="5"><center><?=$kis_lang['Admission']['PICLC']['EnglishAssessmentOfProficiency'] ?></center></td>
                	</tr>
                	<tr>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['Advanced'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['UpperIntermediate'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['Intermediate'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['PreIntermediate'] ?></center></td>
                		<td class="form_guardian_head" style="border-radius: 0;"><center><?=$kis_lang['Admission']['PICLC']['Beginner'] ?></center></td>
                	</tr>
                	
                	<?php
                	$areas = array('Speaking', 'Listening', 'Reading', 'Writing');
                	foreach($areas as $area){
                	?>
                    	<tr>
                    		<td class="field_title"><?=$kis_lang['Admission']['PICLC'][$area] ?></td>
                    		<?php
                    		for($i=5;$i>=1;$i--){
                		        $checked = ($allCustInfo["StudentEnglishLevel_{$area}"][0]['Value'] == $i)?'checked':'';
                		        $disabled = ($checked)?'':'disabled';
                    		?>
                        		<td style="background:#fcfcfc">
                            		<label style="width:100%;height:100%;">
                            			<center>
                            				<input type="radio" <?=$checked ?> onclick="return false;" <?=$disabled ?> />
                            			</center>
                        			</label>
                        		</td>
                    		<?php
                    		}
                    		?>
                    	</tr>
                	<?php 
                	}
                	?>
                </table>
                				
    		</div>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['StudentHowLongLearnEnglish']?> 
    	</td>
    	<td colspan="3">
			<?= kis_ui::displayTableField( $allCustInfo['StudentHowLongLearnEnglish'][0]['Value'] ) ?>
    	</td>
    </tr>
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['StudentForeignLanguage']?> 
    	</td>
    	<td colspan="3">
    		<?php 
		    if(empty($allCustInfo["StudentForeignLanguage"][0]['Value'])){
		        echo ' -- ';
		    }else{
		        $studentForeignLanguageArr = explode(',', $allCustInfo["StudentForeignLanguage"][0]['Value']);
		        foreach($studentForeignLanguageArr as $studentForeignLanguageId){
		            $studentForeignLanguage = $admission_cfg['StudentForeignLanguage'][$studentForeignLanguageId];
		            $details = $allCustInfo["StudentForeignLanguage_{$studentForeignLanguage}Details"][0]['Value'];
            ?>
	            	<div>
	            		<span style="width: 80px;display: inline-block;">
	            			<?=$kis_lang['Admission']['PICLC']['StudentForeignLanguageType'][$studentForeignLanguage] ?>
	            		</span>
	            		
	            		<?php if($details){ ?>
	            			<span>( <?=$details ?> )</span>
	            		<?php } ?>
	            	</div>
            <?php
		        }
		    }
    		?>
    	</td>
    </tr>
</table>-->
