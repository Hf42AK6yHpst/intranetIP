<?php
# modifying by: 
/********************
 * 
 * Log :
 * Date		2018-01-26 [Pun]
 * 			File Created
 * 
 ********************/

// error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);
include_once("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");
 
class admission_cust extends \AdmissionSystem\AdmissionCustBase{

    /**** CONST START ****/
    const STUDENT_ACADEMIC_SCHOOL_COUNT = 3;
    const SIBLING_COUNT = 3;
    /**** CONST END ****/
    
    public function __construct(){
        global $plugin;
        
        if($plugin['eAdmission_devMode']){
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);ini_set('display_errors', 1);
            if($_SERVER['HTTP_HOST'] == '192.168.0.171:31002'){
                $this->AdmissionFormSendEmail = false;
            }
        }
        
        parent::__construct();
        $this->init();
    }
    
    private function init(){
        /**** FILTER_ADMISSION_FORM_EMAIL_TITLE START ****/
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function($emailTitle){
            return 'Princeton Sky Lake International School Admission Notification';
        }));
        /**** FILTER_ADMISSION_FORM_EMAIL_TITLE END ****/
        
        /**** ACTION_APPLICANT_INSERT_DUMMY_INFO START ****/
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array($this, 'insertDummySchoolInfo'));
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array($this, 'insertDummySiblingInfo'));
        /**** ACTION_APPLICANT_INSERT_DUMMY_INFO END ****/
        
        /**** ACTION_APPLICANT_UPDATE_INFO START ****/
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateStudentInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateParentInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateStudentAcademicBackgroundInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateFamilyLanguageProfileInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateSiblingInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateOtherInfo'));
        /**** ACTION_APPLICANT_UPDATE_INFO END ****/
        
        /**** ACTION_APPLICANT_UPDATE_*_INFO START ****/
        $this->addAction(self::ACTION_APPLICANT_INFO, array($this, 'updateApplicantInfo'));
        /**** ACTION_APPLICANT_UPDATE_*_INFO END ****/
    }
    
    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID){
        $result = true;

        for($i=0;$i<self::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){
            $sql = "INSERT INTO 
                ADMISSION_STU_PREV_SCHOOL_INFO 
            (
                ApplicationID, 
                SchoolOrder,
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";

            $result = $result && $this->db_db_query($sql);
        }
        
        if(!$result){
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySiblingInfo($ApplicationID){
        $result = true;

        for($i=0;$i<self::SIBLING_COUNT;$i++){
            $sql = "INSERT INTO 
                ADMISSION_SIBLING 
            (
                ApplicationID, 
                SiblingOrder,
                DateInput, 
                InputBy
            ) VALUES (
                '{$ApplicationID}',
                '{$i}',
                NOW(),
                '{$this->uid}'
            )";

            $result = $result && $this->db_db_query($sql);
        }
        
        if(!$result){
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentInfo($Data, $ApplicationID){
        $fieldArr = array(
            'ChineseSurname' => 'StudentChineseSurname',
            'ChineseFirstName' => 'StudentChineseFirstName',
            'EnglishSurname' => 'StudentEnglishSurname',
            'EnglishFirstName' => 'StudentEnglishFirstName',
            'DOB' => 'StudentDateOfBirth',
            'Gender' => 'StudentGender',
            'BirthCertTypeOther' => 'StudentBirthCertType',
            'BirthCertNo' => 'StudentBirthCertNo',
            'Nationality' => 'StudentCounty',
            'Address' => 'StudentAddress',
            'PlaceOfBirth' => 'StudentRegistedResidence',
            'PlaceOfBirthOther' => 'StudentNativePlace',
            'Email' => 'StudentEmail',
            'LangSpokenAtHome' => 'LangSpokenAtHome',
        );
        
        $updateSql = '';
        foreach($fieldArr as $dbField => $dataField){
            if(isset($Data[$dataField])){
                $updateSql .= "$dbField = '{$Data[$dataField]}',";
            }
        }
        
        #### Concat name START ####
        if($Data['StudentChineseSurname'] && $Data['StudentChineseFirstName']){
            $updateSql .= "ChineseName = '{$Data['StudentChineseSurname']}{$Data['StudentChineseFirstName']}',";
        }
        if($Data['StudentEnglishSurname'] && $Data['StudentEnglishFirstName']){
            $updateSql .= "EnglishName = '{$Data['StudentEnglishSurname']}, {$Data['StudentEnglishFirstName']}',";
        }
        #### Concat name END ####
        
        $sql = "UPDATE 
            ADMISSION_STU_INFO
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'";
        $result = $this->db_db_query($sql);
        
        if(!$result){
            throw new \Exception('Cannot update student info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateParentInfo($Data, $ApplicationID){
        $result = true;

        $pgType = array( 1=>'F', 2=>'M', 3=>'G' );
        
        /**** Update parent basic info START ****/
        $fieldArr = array(
            'EnglishSurname',
            'EnglishFirstName',
            'ChineseSurname',
            'ChineseFirstName',
            'Nationality',
            'Company',
            'JobPosition',
            'OfficeAddress',
            'OfficeTelNo',
            'Mobile',
            'Email',
        );
        
        foreach($pgType as $id => $type){
            $updateSQL = '';
            if($Data["PG_Type"] == $type){
            	$id = 1;
            }
            else{
            	$id = 0;
            }
            foreach($fieldArr as $field){
                if(isset($Data["G{$id}{$field}"])){
                    $updateSQL .= "{$field} = '{$Data["G{$id}{$field}"]}',";
                }
                else if($id == 0){
                	$updateSQL .= "{$field} = '',";
                }
            }

            #### Concat name START ####
            if($Data["G{$id}ChineseSurname"] && $Data["G{$id}ChineseFirstName"]){
                $updateSQL .= "ChineseName = '{$Data["G{$id}ChineseSurname"]}{$Data["G{$id}ChineseFirstName"]}',";
            }
            if($Data["G{$id}EnglishSurname"] && $Data["G{$id}EnglishFirstName"]){
                $updateSQL .= "EnglishName = '{$Data["G{$id}EnglishSurname"]}, {$Data["G{$id}EnglishFirstName"]}',";
            }
            #### Concat name END ####

            $sql = "UPDATE 
                ADMISSION_PG_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                PG_TYPE = '{$type}'";
            $result = $result && $this->db_db_query($sql);
        }
        /**** Update parent basic info END ****/
        
        /**** Update parent extra info START ****/
        foreach($pgType as $id => $type){
        	$orig_id = $id;
        	if($Data["PG_Type"] == $type){
            	$id = 1;
            }
            else{
            	$id = 0;
            	$Data["G{$id}NativePlace"] = '';
            	$Data["G{$id}WeChatID"] = '';
            }
            if(isset($Data["G{$id}NativePlace"])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => "G{$orig_id}NativePlace",
                    'Value' => $Data["G{$id}NativePlace"],
                ), $insertIfNotExists = true);
            }
            if(isset($Data["G{$id}WeChatID"])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => "G{$orig_id}WeChatID",
                    'Value' => $Data["G{$id}WeChatID"],
                ), $insertIfNotExists = true);
            }
        }
        /**** Update parent extra info END ****/
        
        /**** Update Cust info START ****/
        if(isset($Data['InvoiceTo'])){
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'InvoiceTo',
                'Value' => $Data['InvoiceTo'],
            ), $insertIfNotExists = true);
        }
        
        if(isset($Data['AnnualHouseholdIncome'])){
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'AnnualHouseholdIncome',
                'Value' => $Data['AnnualHouseholdIncome'],
            ), $insertIfNotExists = true);
        }
        /**** Update Cust info END ****/
        
        if(!$result){
            throw new \Exception('Cannot update parent info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentAcademicBackgroundInfo($Data, $ApplicationID){
        $result = true;

        /**** Update school basic info START ****/
        $fieldArr = array(
            'NameOfSchool' => 'Name',
            'SchoolAddress' => 'Location',
            'StartDate' => 'DatesAttended_From',
            'EndDate' => 'DatesAttended_To',
            'LangSpoken' => 'Language',
        );
        
        for($i=0;$i<self::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){
            $updateSQL = '';
            foreach($fieldArr as $dbField => $inputField){
                if(isset($Data["StudentSchool{$i}_{$inputField}"])){
                    $updateSQL .= "{$dbField} = '{$Data["StudentSchool{$i}_{$inputField}"]}',";
                }
            }
            
            $sql = "UPDATE
                ADMISSION_STU_PREV_SCHOOL_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                SchoolOrder = '{$i}'";
            $result = $result && $this->db_db_query($sql);
        }
        /**** Update school basic info END ****/
        
        /**** Update cust info START ****/
        $fieldArr = array(
            'ChildPlacedOutStandardGroup',
            'ChildPlacedOutStandardGroup_Details',
            'ChildSpecialClassTalent',
            'ChildSpecialClassTalent_Details',
            'ChildEducationalPsychologist',
            'ChildEducationalPsychologist_Details',
            'StudentFavouriteSubject',
            'StudentSuccessfulSubject',
            'StudentChallengingSubject',
        );
        foreach($fieldArr as $field){
            if(isset($Data[$field])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field],
                ), $insertIfNotExists = true);
            }
        }
        /**** Update cust info START ****/
    
        if(!$result){
            throw new \Exception('Cannot update student academic background info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateFamilyLanguageProfileInfo($Data, $ApplicationID){
        $result = true;

        $Data['StudentForeignLanguage'] = (array)$Data['StudentForeignLanguage'];
        if(!in_array(6, $Data['StudentForeignLanguage'])){
            $Data['StudentForeignLanguage_othersDetails'] = '';
        }
        $Data['StudentForeignLanguage'] = implode(',', $Data['StudentForeignLanguage']);
        
        $fieldArr = array(
            'StudentFirstLanguage',
            'StudentSecondLanguage',
            'StudentThirdLanguage',
            'MotherFirstLanguage',
            'MotherSecondLanguage',
            'MotherThirdLanguage',
            'FatherFirstLanguage',
            'FatherSecondLanguage',
            'FatherThirdLanguage',
            'StudentFirstLanguageIsEnglish',
            'StudentEnglishLevel_Speaking',
            'StudentEnglishLevel_Listening',
            'StudentEnglishLevel_Reading',
            'StudentEnglishLevel_Writing',
            'StudentHowLongLearnEnglish',
            'StudentForeignLanguage',
            'StudentForeignLanguage_othersDetails',
        );
        foreach($fieldArr as $field){
            if(isset($Data[$field])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field],
                ), $insertIfNotExists = true);
            }
        }
        
        if(!$result){
            throw new \Exception('Cannot update family language info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateSiblingInfo($Data, $ApplicationID){
        $result = true;

        $fieldArr = array(
            'EnglishName' => 'Name',
            'Gender' => 'Gender',
            'DOB' => 'DOB',
            'CurrentSchool' => 'CurrentSchool',
            'ApplyingSameSchool' => 'ApplyingSameSchool',
        );
        
        for($i=0;$i<self::SIBLING_COUNT;$i++){
            $updateSQL = '';
            foreach($fieldArr as $dbField=>$field){
                if(isset($Data["Sibling{$i}_{$field}"])){
                    $updateSQL .= "{$dbField} = '{$Data["Sibling{$i}_{$field}"]}',";
                }
            }
            
            $sql = "UPDATE
                ADMISSION_SIBLING
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                SiblingOrder = '{$i}'";
            $result = $result && $this->db_db_query($sql);
        }
        
        if(!$result){
            throw new \Exception('Cannot update sibling info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateOtherInfo($Data, $ApplicationID){
        $result = true;

        $Data['WhereToKnowPIS'] = (array)$Data['WhereToKnowPIS'];
        if(!in_array(6, $Data['WhereToKnowPIS'])){
            $Data['WhereToKnowPIS_MagazineDetails'] = '';
        }
        if(!in_array(7, $Data['WhereToKnowPIS'])){
            $Data['WhereToKnowPIS_EventDetails'] = '';
        }
        if(!in_array(8, $Data['WhereToKnowPIS'])){
            $Data['WhereToKnowPIS_OtherDetails'] = '';
        }
        $Data['WhereToKnowPIS'] = implode(',', $Data['WhereToKnowPIS']);
        
        $fieldArr = array(
            'SpecialHolidayCelebrate',
            'WhyChoosePIS',
            'WhereToKnowPIS',
            'WhereToKnowPIS_MagazineDetails',
            'WhereToKnowPIS_EventDetails',
            'WhereToKnowPIS_OtherDetails',
            'OtherInformation'
        );
        foreach($fieldArr as $field){
            if(isset($Data[$field])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field],
                ), $insertIfNotExists = true);
            }
        }
        
        if(isset($Data['lang'])){
            $sql = "UPDATE 
                ADMISSION_OTHERS_INFO 
            SET 
                Lang='{$Data['lang']}',
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE 
                ApplicationID = '{$ApplicationID}'
            ";
            $result = $result && $this->db_db_query($sql);
        }
        
        if(!$result){
            throw new \Exception('Cannot update other info');
        }
        return true;
    }


    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID){
        $result = true;
        
        if($type == 'studentinfo'){
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);
            
        }else if($type == 'parentinfo'){
            $result = $result && $this->updateParentInfo($Data, $ApplicationID);
            
        }else if($type == 'piclc_academic_background'){
            $result = $result && $this->updateStudentAcademicBackgroundInfo($Data, $ApplicationID);
            
        }else if($type == 'piclc_family_language'){
            $result = $result && $this->updateFamilyLanguageProfileInfo($Data, $ApplicationID);
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);
            
        }else if($type == 'otherinfo'){
            $result = $result && $this->updateSiblingInfo($Data, $ApplicationID);
            $result = $result && $this->updateOtherInfo($Data, $ApplicationID);
            
        }else if($type == 'remarks'){
            $result = $result && $this->updateApplicationStatus($Data, $ApplicationID);
            
        }else{
            $result = false;
        }

        if(!$result){
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }
    
    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '',$classLevelID = ''){
        global $kis_lang;
    
        $headerArray = array();
    
        $headerArray[] = $kis_lang['Admission']['admissiondate'];
        $headerArray[] = $kis_lang['applicationno'];
        $headerArray[] = $kis_lang['Admission']['applyLevel'];
        
        //for student info
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['englishname']} ({$kis_lang['Admission']['PICLC']['surname']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['englishname']} ({$kis_lang['Admission']['PICLC']['firstName']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['chinesename']} ({$kis_lang['Admission']['PICLC']['surname']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['chinesename']} ({$kis_lang['Admission']['PICLC']['firstName']})";
        $headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['PICLC']['birthCertNo'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['PICLC']['PlaceOfIssue'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['nationality'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['PICLC']['nativePlace'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['PICLC']['homeAddress'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['contactEmail'];
    
    
        //for parent info
        $headerArray['parentInfo'][] = "{$kis_lang['Admission']['englishname']} ({$kis_lang['Admission']['PICLC']['surname']})";
        $headerArray['parentInfo'][] = "{$kis_lang['Admission']['englishname']} ({$kis_lang['Admission']['PICLC']['firstName']})";
        $headerArray['parentInfo'][] = "{$kis_lang['Admission']['chinesename']} ({$kis_lang['Admission']['PICLC']['surname']})";
        $headerArray['parentInfo'][] = "{$kis_lang['Admission']['chinesename']} ({$kis_lang['Admission']['PICLC']['firstName']})";
        $headerArray['parentInfo'][] = $kis_lang['Admission']['nationality'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['PICLC']['nativePlace'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['PICLC']['employer'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['PICLC']['position'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['PICLC']['businessAddress'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['PICLC']['businessPhone'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['mobilephoneno'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['email'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['WeChatId'];
        $headerArray['parentInfo2'][] = $kis_lang['Admission']['PICLC']['invoiceTo'];
    
        
        //for academic background
        for($i=0;$i<self::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){
            $index = $i+1;
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['SchoolName'] . " ({$index})";
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['Location'];
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['DatesAttended'];
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['LanguageOfInstruction'];
        }
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['ChildPlacedOutStandardGroup'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['Details'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['ChildSpecialClassTalent'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['Details'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['ChildEducationalPsychologist'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['Details'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['IndicateStudentFavouriteSubject'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['IndicateStudentSuccessfulSubject'];
//        $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['PICLC']['IndicateStudentChallengingSubject'];
        
        
        //for language
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['FirstLanguage'].'('.$kis_lang['Admission']['PICLC']['Student'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['SecondLanguage'].'('.$kis_lang['Admission']['PICLC']['Student'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['ThirdLanguage'].'('.$kis_lang['Admission']['PICLC']['Student'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['FirstLanguage'].'('.$kis_lang['Admission']['PICLC']['Mother'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['SecondLanguage'].'('.$kis_lang['Admission']['PICLC']['Mother'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['ThirdLanguage'].'('.$kis_lang['Admission']['PICLC']['Mother'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['FirstLanguage'].'('.$kis_lang['Admission']['PICLC']['Father'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['SecondLanguage'].'('.$kis_lang['Admission']['PICLC']['Father'].')';
        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['ThirdLanguage'].'('.$kis_lang['Admission']['PICLC']['Father'].')';
//        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['LanguageSpokenAtHome'];
//        $headerArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC']['StudentFirstLanguageIsEnglish'];
//        
//        $headerArray['familyLanguageInfo2'][] = $kis_lang['Admission']['PICLC']['Speaking'];
//        $headerArray['familyLanguageInfo2'][] = $kis_lang['Admission']['PICLC']['Listening'];
//        $headerArray['familyLanguageInfo2'][] = $kis_lang['Admission']['PICLC']['Reading'];
//        $headerArray['familyLanguageInfo2'][] = $kis_lang['Admission']['PICLC']['Writing'];
//        $headerArray['familyLanguageInfo2'][] = $kis_lang['Admission']['PICLC']['StudentHowLongLearnEnglish'];
//        $headerArray['familyLanguageInfo2'][] = $kis_lang['Admission']['PICLC']['StudentForeignLanguage'];
        
    
        //for other info
        for($i=0;$i<self::SIBLING_COUNT;$i++){
            $index = $i+1;
            $headerArray['siblingInfo'][] = $kis_lang['Admission']['name'] . " ({$index})";
            $headerArray['siblingInfo'][] = $kis_lang['Admission']['gender'];
            $headerArray['siblingInfo'][] = $kis_lang['Admission']['dateofbirth'];
            $headerArray['siblingInfo'][] = $kis_lang['Admission']['PICLC']['CurrentSchool'];
//            $headerArray['siblingInfo'][] = $kis_lang['Admission']['PICLC']['ApplyingPIS'];
        }
//        $headerArray['otherInfo'][] = $kis_lang['Admission']['PICLC']['SpecialHolidayCelebrate'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['PICLC']['WhyChoosePIS'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['PICLC']['WhereToKnowPIS'];
        $headerArray['otherInfo'][] = $kis_lang['Admission']['otherInfo'];
        
    
        //for official use
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];// Omas
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (1)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (2)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (3)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
    
    
        ######## Header START ########
        #### Admission Info START ####
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        #### Admission Info END ####
    
        
        #### Student Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
        for($i=0; $i < count($headerArray['studentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
        #### Student Info END ####
    
        
        #### Parent Info START ####
        $exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['F']})";
        for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
    
        $exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['M']})";
        for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
    
        $exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['G']})";
        for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
        
        for($i=0; $i < count($headerArray['parentInfo2']); $i++){
            $exportColumn[0][] = "";
        }
        #### Parent Info END ####
        
        
        #### Academic Background Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['PICLC']['studentAcademicBackground'];
        for($i=0; $i < count($headerArray['academicBackgroundInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
        #### Academic Background Info END ####
    
        
        #### Family Language Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['PICLC']['FamilyLanguageProfile'];
        for($i=0; $i < count($headerArray['familyLanguageInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
        
        $exportColumn[0][] = $kis_lang['Admission']['PICLC']['EnglishAssessmentOfProficiency'];
        for($i=0; $i < count($headerArray['familyLanguageInfo2'])-1; $i++){
            $exportColumn[0][] = "";
        }
        #### Family Language Info END ####
        
        
        #### Other Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['PICLC']['SiblingInformation'];
         for($i=0; $i < count($headerArray['siblingInfo'])-1; $i++){
         $exportColumn[0][] = "";
        }
        $exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
         for($i=0; $i < count($headerArray['otherInfo'])-1; $i++){
         $exportColumn[0][] = "";
        }
        #### Other Info END ####
    
        #### Offical Use START ####
        $exportColumn[0][] = $kis_lang['remarks'];
        for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
            $exportColumn[0][] = "";
        }
        #### Offical Use END ####
    
        //sub header
        $exportColumn[1] = array_merge(
        array($headerArray[0],$headerArray[1],$headerArray[2]),
            $headerArray['studentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo2'],
            $headerArray['academicBackgroundInfo'],
            $headerArray['familyLanguageInfo'],
//            $headerArray['familyLanguageInfo2'],
            $headerArray['siblingInfo'],
            $headerArray['otherInfo'],
            $headerArray['officialUse']
        );
        
        return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
        global $admission_cfg, $Lang, $kis_lang;
    
        ######### Get data START ########
        #### Get student Info START ####
        $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
        #### Get student Info END ####
        
        #### Get parent Info START ####
        $parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
        $parentInfoArr = array();
        foreach($parentInfo as $parent){
            $ParentInfo[$parent['type']] = $parent;
        }
        #### Get parent Info END ####
    
        #### Get academic background START ####
        $StudentPrevSchoolInfo = $this->getApplicationPrevSchoolInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
        #### Get academic background END ####
        
        #### Get other info START ####
        $SiblingInfo = $this->getApplicationSibling($schoolYearID,$classLevelID,$applicationID,'',$recordID);
        
        $allCustInfo = $this->getAllApplicationCustInfo($StudentInfo['applicationID']);

        $allClassLevel = $this->getClassLevel();
        $classLevel = $this->getClassLevel();
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
        $status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
        #### Get other info END ####
        ######### Get data END ########
    
    
        $dataArray = array();

        ######### Export data START ########
        #### Basic Info START ####
        $dataArray[] = substr($otherInfo['DateInput'], 0, -9);
        $dataArray[] = $StudentInfo['applicationID'];
        $dataArray[] = $classLevel[$otherInfo['classLevelID']];
        #### Basic Info END ####
        
        
        #### Student Info START ####
        $dataArray['studentInfo'][] = $StudentInfo['EnglishSurname'];
        $dataArray['studentInfo'][] = $StudentInfo['EnglishFirstName'];
        $dataArray['studentInfo'][] = $StudentInfo['ChineseSurname'];
        $dataArray['studentInfo'][] = $StudentInfo['ChineseFirstName'];
        $dataArray['studentInfo'][] = ($StudentInfo['DOB'] == '0000-00-00')? '' : $StudentInfo['DOB'];
        $dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$StudentInfo['Gender']];
        $dataArray['studentInfo'][] = $StudentInfo['BirthCertNo'];
        $dataArray['studentInfo'][] = $StudentInfo['PlaceOfBirth'];
        $dataArray['studentInfo'][] = $StudentInfo['Nationality'];
        $dataArray['studentInfo'][] = $StudentInfo['PlaceOfBirthOther'];
        $dataArray['studentInfo'][] = $StudentInfo['Address'];
        $dataArray['studentInfo'][] = $StudentInfo['Email'];
        #### Student Info END ####
    
        
        #### Parent Info START ####
        $index = 1;
        foreach(array('F', 'M', 'G') as $type){
            $dataArray['parentInfo'][] = $ParentInfo[$type]['EnglishSurname'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['EnglishFirstName'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['ChineseSurname'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['ChineseFirstName'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Nationality'];
            $dataArray['parentInfo'][] = $allCustInfo["G{$index}NativePlace"][0]['Value'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Company'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['JobPosition'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['OfficeAddress'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['OfficeTelNo'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Mobile'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Email'];
            $dataArray['parentInfo'][] = $allCustInfo["G{$index}WeChatID"][0]['Value'];
            $index++;
        }
        $dataArray['parentInfo'][] = $allCustInfo['InvoiceTo'][0]['Value'];
        #### Parent Info END ####
        
        
        #### Academic Background Info START ####
        ## Basic info START ##
        for($i=0;$i<self::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){
            $dataArray['academicBackgroundInfo'][] = $StudentPrevSchoolInfo[$i]['NameOfSchool'];
            $dataArray['academicBackgroundInfo'][] = $StudentPrevSchoolInfo[$i]['SchoolAddress'];
            if($StudentPrevSchoolInfo[$i]['StartDate'] != '0000-00-00' && $StudentPrevSchoolInfo[$i]['EndDate'] != '0000-00-00'){
                $dataArray['academicBackgroundInfo'][] = "{$StudentPrevSchoolInfo[$i]['StartDate']} - {$StudentPrevSchoolInfo[$i]['EndDate']}";
            }
            else{
            	$dataArray['academicBackgroundInfo'][] = '';
            }
            $dataArray['academicBackgroundInfo'][] = $StudentPrevSchoolInfo[$i]['LangSpoken'];
        }
        ## Basic info END ##

//        ## ChildPlacedOutStandardGroup START ##
//        if($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'Y'){
//            $dataArray['academicBackgroundInfo'][] = $kis_lang['Admission']['yes2'];
//            
//            if($allCustInfo['ChildPlacedOutStandardGroup_Details'][0]['Value']){
//                $dataArray['academicBackgroundInfo'][] = $allCustInfo['ChildPlacedOutStandardGroup_Details'][0]['Value'];
//            }else{
//                $dataArray['academicBackgroundInfo'][] = '';
//            }
//        }else if($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'N'){
//            $dataArray['academicBackgroundInfo'][] = $kis_lang['Admission']['no2'];
//            $dataArray['academicBackgroundInfo'][] = '';
//        }else{
//            $dataArray['academicBackgroundInfo'][] = '';
//            $dataArray['academicBackgroundInfo'][] = '';
//        }
//        ## ChildPlacedOutStandardGroup END ##
//
//        ## ChildSpecialClassTalent START ##
//        if($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'Y'){
//            $dataArray['academicBackgroundInfo'][] = $kis_lang['Admission']['yes2'];
//            
//            if($allCustInfo['ChildSpecialClassTalent_Details'][0]['Value']){
//                $dataArray['academicBackgroundInfo'][] = $allCustInfo['ChildSpecialClassTalent_Details'][0]['Value'];
//            }else{
//                $dataArray['academicBackgroundInfo'][] = '';
//            }
//        }else if($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'N'){
//            $dataArray['academicBackgroundInfo'][] = $kis_lang['Admission']['no2'];
//            $dataArray['academicBackgroundInfo'][] = '';
//        }else{
//            $dataArray['academicBackgroundInfo'][] = '';
//            $dataArray['academicBackgroundInfo'][] = '';
//        }
//        ## ChildSpecialClassTalent END ##
//
//        ## ChildEducationalPsychologist START ##
//        if($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'Y'){
//            $dataArray['academicBackgroundInfo'][] = $kis_lang['Admission']['yes2'];
//            
//            if($allCustInfo['ChildEducationalPsychologist_Details'][0]['Value']){
//                $dataArray['academicBackgroundInfo'][] = $allCustInfo['ChildEducationalPsychologist_Details'][0]['Value'];
//            }else{
//                $dataArray['academicBackgroundInfo'][] = '';
//            }
//        }else if($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'N'){
//            $dataArray['academicBackgroundInfo'][] = $kis_lang['Admission']['no2'];
//            $dataArray['academicBackgroundInfo'][] = '';
//        }else{
//            $dataArray['academicBackgroundInfo'][] = '';
//            $dataArray['academicBackgroundInfo'][] = '';
//        }
//        ## ChildEducationalPsychologist END ##
//
//        ## Subjects START ##
//        $dataArray['academicBackgroundInfo'][] = $allCustInfo['StudentFavouriteSubject'][0]['Value'];
//        $dataArray['academicBackgroundInfo'][] = $allCustInfo['StudentSuccessfulSubject'][0]['Value'];
//        $dataArray['academicBackgroundInfo'][] = $allCustInfo['StudentChallengingSubject'][0]['Value'];
//        ## Subjects END ##
//        #### Academic Background Info END ####
    
        
        #### Family Language START ####
        ## Basic info START ##
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['StudentFirstLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['StudentSecondLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['StudentThirdLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['MotherFirstLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['MotherSecondLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['MotherThirdLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['FatherFirstLanguage'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['FatherSecondLanguages'][0]['Value'];
        $dataArray['academicBackgroundInfo'][] = $allCustInfo['FatherThirdLanguages'][0]['Value'];
//        $dataArray['academicBackgroundInfo'][] = $StudentInfo['LangSpokenAtHome'];
        ## Basic info END ##
        

//        ## English START ##
//        if($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'Y'){
//            $dataArray['familyLanguageInfo'][] = $kis_lang['Admission']['yes'];
//        }else if($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N'){
//            $dataArray['familyLanguageInfo'][] = $kis_lang['Admission']['no'];
//        }else{
//            $dataArray['familyLanguageInfo'][] = '';
//        }
//
//        $areas = array('Speaking', 'Listening', 'Reading', 'Writing');
//        $proficiency = array(
//            5 => 'Advanced',
//            4 => 'UpperIntermediate',
//            3 => 'Intermediate',
//            2 => 'PreIntermediate',
//            1 => 'Beginner',
//        );
//        foreach($areas as $area){
//            if($allCustInfo['StudentFirstLanguageIsEnglish'][0]['Value'] == 'N'){
//                $level = $allCustInfo["StudentEnglishLevel_{$area}"][0]['Value'];
//                $levelName = $proficiency[$level];
//                $dataArray['familyLanguageInfo'][] = $kis_lang['Admission']['PICLC'][$levelName];
//            }else{
//                $dataArray['familyLanguageInfo'][] = '';
//            }
//        }
//
//        $dataArray['familyLanguageInfo'][] = $allCustInfo['StudentHowLongLearnEnglish'][0]['Value'];
//        $dataArray['familyLanguageInfo'][] = $allCustInfo['StudentForeignLanguage'][0]['Value'];
//        ## English END ##
//        #### Family Language END ####
        
        
        #### Sibling Info START ####
        for($i=0;$i<self::SIBLING_COUNT;$i++){
            $dataArray['siblingInfo'][] = $SiblingInfo[$i]['EnglishName'];
            $dataArray['siblingInfo'][] = $kis_lang['Admission']['genderType'][ $SiblingInfo[$i]['Gender'] ];
            $dataArray['siblingInfo'][] = $SiblingInfo[$i]['DOB'];
            $dataArray['siblingInfo'][] = $SiblingInfo[$i]['CurrentSchool'];
//		    if($SiblingInfo[$i]['ApplyingSameSchool'] == 'Y'){
//		        $dataArray['siblingInfo'][] = $kis_lang['Admission']['yes'];
//		    }else if($SiblingInfo[$i]['ApplyingSameSchool'] == 'N'){
//		        $dataArray['siblingInfo'][] = $kis_lang['Admission']['no'];
//		    }else{
//		        $dataArray['siblingInfo'][] = '';
//		    }
        }
        #### Sibling Info END ####
        
        #### Other Info START ####
//        $dataArray['otherInfo'][] = $allCustInfo['SpecialHolidayCelebrate'][0]['Value'];
        $dataArray['otherInfo'][] = $allCustInfo['WhyChoosePIS'][0]['Value'];
        
        $whereToKnowArr = explode(',', $allCustInfo["WhereToKnowPIS"][0]['Value']);
        $whereToKnowStr = '';
        foreach($whereToKnowArr as $whereToKnowId){
            $whereToKnow = $admission_cfg['WhereToKnowPis'][$whereToKnowId];
            $details = $allCustInfo["WhereToKnowPIS_{$whereToKnow}Details"][0]['Value'];
            
            $whereToKnowStr .= $kis_lang['Admission']['PICLC']['KnowPIS'][$whereToKnow];
            if($details){
                $whereToKnowStr .= " ({$details})";
            }
            $whereToKnowStr .= ', ';
        }
        $whereToKnowStr = trim($whereToKnowStr, ', ');
        $dataArray['otherInfo'][] = $whereToKnowStr;
        $dataArray['otherInfo'][] = $allCustInfo['OtherInformation'][0]['Value'];
        #### Other Info END ####
    
    
        #### Official Use START ####
        $dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
        $dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
        $dataArray['officialUse'][] = $status['receiptID'];
        $dataArray['officialUse'][] = $status['receiptdate'];
        $dataArray['officialUse'][] = $status['handler'];
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
        $dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
        $dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
        $dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
        $dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
        $dataArray['officialUse'][] = $status['remark'];
        #### Official Use END ####
    
        $ExportArr = array_merge(
            array($dataArray[0],$dataArray[1],$dataArray[2]),
            $dataArray['studentInfo'],
            $dataArray['parentInfo'],
            $dataArray['academicBackgroundInfo'],
//            $dataArray['familyLanguageInfo'],
            $dataArray['siblingInfo'],
            $dataArray['otherInfo'],
            $dataArray['officialUse']
        );
        ######### Export data END ########

        return $ExportArr;
    }
    
    function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1){
		$status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND st.status in ('".implode("','",$selectStatusArr)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		
		$sql='Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" '.$status_cond.' order by y.YearName desc';
				
		$result = $this->returnArray($sql);
		
		$allApplicant = array();
		
		for($i=0; $i<sizeof($result); $i++){
			$allApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
		}
		
//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);
		
		$result = array();
		
		$TwinsAssignedApplicant = array();
		
//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
				$sql = "SELECT RecordID, Quota FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY Date, StartTime, GroupName";
				$interviewRecordIDArr = $this->returnArray($sql);
				
				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
					
					if(!$allApplicant){
						break;
					}
					
					$previousClassLevel = $allApplicant[0]['ClassLevel'];
					$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
					$result[] = $this->db_db_query($sql);
					
					for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
						$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
						$result[] = $this->db_db_query($sql);
						array_shift($allApplicant);
						if($previousClassLevel != $allApplicant[0]['ClassLevel']){
							break;
						}
					}
	
				}
//		}
		
		//handling twins swraping [start]
		$sql='Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID'.($round>1?$round:'').' as InterviewSettingID From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" AND s.IsTwinsApplied = "Y" order by y.YearName desc';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
			for($j=0; $j<sizeof($twinsResult); $j++){
				if($twinsResult[$i]['TwinsApplicationID'] == $twinsResult[$j]['BirthCertNo'] && $twinsResult[$i]['InterviewSettingID'] == $twinsResult[$j]['InterviewSettingID'] && !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)){
					
					$sql = 'Select RecordID, ClassLevelID, Date, GroupName From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" ';
					$originalSession = current($this->returnArray($sql));
					
					$sql = 'Select RecordID From ADMISSION_INTERVIEW_SETTING where ClassLevelID = "'.$originalSession['ClassLevelID'].'" AND Date = "'.$originalSession['Date'].'" '.$round_cond.' AND GroupName <> "'.$originalSession['GroupName'].'" ';
					$newSession = current($this->returnArray($sql));
					
					if($newSession){
						$sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" AND s.IsTwinsApplied <> "Y" ';
						$swapApplicant = current($this->returnArray($sql));
						if($swapApplicant){
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$originalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$j]['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$assignedTwins[] = $twinsResult[$j]['ApplicationID'];
						}
					}
				}
			}
		}
		
		//handling twins swraping [end]
		
		return !in_array(false,$result);
	}
} // End Class