<?php
// Editing by 
/*
 * Common functions, i.e. non-customized functions can implement in this base class
 * customized functions implement in inherited class, functions can be overwritten there
 *
 * 2019-11-18 (Pun) [175264] modified getExportInterviewResultHeaderData(), fixed cannot export data in search result
 * 2019-10-11 (Tommy) added getKnowUsBy()
 * 2019-05-07 (Pun) added getNextApplicationRecordId(), getPrevApplicationRecordId()
 * 2019-01-23 (Pun) modified updateApplicationSetting(), fixed cannot create new application
 * 2017-10-19 (Pun) modified function checkImportDataForImportInterviewSettings(), added check room/session exists
 * 2017-10-18 (Pun) modified function checkImportDataForImportInterviewApplicant(), added applicant duplicate checking display error
 * 2017-10-17 (Pun) modified function checkImportDataForImportInterviewApplicant(), added room checking display error
 * 2017-10-09 (Pun) added function checkImportDataForImportInterviewApplicant(), importDataForImportInterviewApplicant()
 * 2016-05-02 (Pun) added function checkBirthCertNo()
 * 2016-08-03 (Henry) added function getExportPaymentRecord()
 * 2015-12-16 (Henry) added function NumOfInterviewQuotaLeftByClassLevel()
 * 2015-11-09 (Henry) added function getInterviewListAry()
 * 2015-10-28 (Henry) modified getInterviewResult()
 * 2015-10-27 (Henry) modified getInterviewResult()
 * 2015-10-13 (Henry) added getApplicationResult()
 * 2015-09-25 (Henry) modified updateApplicationSetting() and added IsUpdatePeriod()
 * 2015-09-24 (Henry) modified importInterviewInfoByArrangement() and getApplicationSetting()
 * 2015-09-23 (Henry) modified getPaymentResult() to filter out no payment status record
 * 2015-09-15 (Henry) added getTokenByApplicationNumber()
 * 2015-09-15 (Henry) added isInternalUse()
 * 2015-09-09 (Henry) added getPaymentResult()
 * 2015-08-19 (Henry) added getExportInterviewResultHeaderData() <--[developing]
 * 2015-08-04 (Henry) added Get_Number_Selection()
 * 2015-07-31 (Henry) added Get_Alphabet_Selection()
 * 2015-07-28 (Pun) added parameter $insertIfNotExists for updateApplicationCustInfo()
 * 2015-07-27 (Pun) added function getAllApplicationCustInfo()
 * 2015-07-24 (Henry) added function getInterviewResult()
 * 2015-07-24 (Pun) added function insertApplicationCustInfo(), updateApplicationCustInfo(), deleteApplicationCustInfo(), getApplicationCustInfo()
 * 2015-07-21 (Henry) added function importInterviewInfoByArrangement()
 * 2015-01-20 (Henry) added updateApplicationClassLevelByIds(), getOtherInfoByToken()
 * 2014-09-01 (Henry) added updateOtherInterviewSettingID()
 */

class admission_cust_base extends libdb
{

    function admission_cust_base()
    {
        global $kis_lang; // switch $lang for IP/EJ/KIS
    }

    function getAttachmentSettings($filterIdList = array(), $filterName = array(), $excludeRecordID = array())
    {
        if (sizeof($filterName) > 0) {

            for ($i = 0; $i < sizeof($filterName); $i++) {
                if (trim($filterName[$i])) {
                    $filterName[$i] = $this->Get_Safe_Sql_Query($filterName[$i]);
                } else {
                    unset($filterName[$i]);
                }
            }
            $filterName = array_filter($filterName);
            $filterNameCond = " AND
			(
			    AttachmentName IN ('" . implode("','", (array)$filterName) . "')
	        OR
			    AttachmentName1 IN ('" . implode("','", (array)$filterName) . "')
	        OR
			    AttachmentName2 IN ('" . implode("','", (array)$filterName) . "')
	        OR
			    AttachmentName3 IN ('" . implode("','", (array)$filterName) . "')
		    )
	        ";
        }
        $filterIdCond = sizeof($filterIdList) > 0 || $filterIdList ? " AND RecordID IN (" . implode(",", (array)$filterIdList) . ") " : "";
        if (sizeof($excludeRecordID) > 0) {
            $filterIdCond .= " AND RecordID NOT IN (" . implode(",", $excludeRecordID) . ") ";
        }
        $sql = "SELECT * FROM ADMISSION_ATTACHMENT_SETTING WHERE 1 $filterIdCond $filterNameCond ORDER BY Sequence";

        $records = $this->returnArray($sql);
        return $records;
    }

    function updateAttachmentSetting($recordId, $attachments, $IsOptional, $YearClassIdArr, $IsExtra)
    {
        global $admission_cfg;

        if (is_array($attachments)) {
            $attachmentName = $this->Get_Safe_Sql_Query($attachments[0]);
            $attachmentName1 = $this->Get_Safe_Sql_Query($attachments[1]);
            $attachmentName2 = $this->Get_Safe_Sql_Query($attachments[2]);
            $attachmentName3 = $this->Get_Safe_Sql_Query($attachments[3]);
            if (!$attachmentName) {
                foreach ($admission_cfg['Lang'] as $index => $lang) {
                    if ($lang == 'en') {
                        $attachmentName = $this->Get_Safe_Sql_Query($attachments[$index]);
                    }
                }
            }
        } else {
            $attachmentName = $this->Get_Safe_Sql_Query($attachmentName);
            $attachmentName1 = '';
            $attachmentName2 = '';
            $attachmentName3 = '';
        }

        $isOptional = IntegerSafe($IsOptional);
        $IsExtra = IntegerSafe($IsExtra);

        if (count($YearClassIdArr) == count($this->classLevelAry)) {
            $YearClassIdStr = '';
        } else {
            $YearClassIdArr = IntegerSafe($YearClassIdArr);
            $YearClassIdStr = implode(',', $YearClassIdArr);
        }

        if (!empty($recordId) && $recordId > 0) {
            $sql = "UPDATE
			    ADMISSION_ATTACHMENT_SETTING
		    SET
			    AttachmentName='{$attachmentName}',
			    AttachmentName1='{$attachmentName1}',
			    AttachmentName2='{$attachmentName2}',
			    AttachmentName3='{$attachmentName3}',
			    IsOptional='{$isOptional}',
			    ClassLevelStr='{$YearClassIdStr}',
		        DateModified=NOW(),
		        ModifiedBy='{$this->uid}',
			    IsExtra='{$IsExtra}'
			WHERE
			    RecordID='{$recordId}'
			";
            $result = $this->db_db_query($sql);
        } else {
            $sql = "SELECT IFNULL(MAX(Sequence)+1,1) FROM ADMISSION_ATTACHMENT_SETTING";
            $sequenceRecord = $this->returnVector($sql);
            $sequence = $sequenceRecord[0];
            $sql = "INSERT INTO
			    ADMISSION_ATTACHMENT_SETTING
		    (
			    AttachmentName,
			    AttachmentName1,
			    AttachmentName2,
			    AttachmentName3,
			    Sequence,
			    IsOptional,
			    ClassLevelStr,
			    DateInput,
			    DateModified,
			    InputBy,
			    ModifiedBy,
		        IsExtra
		    ) VALUES (
			    '{$attachmentName}',
			    '{$attachmentName1}',
			    '{$attachmentName2}',
			    '{$attachmentName3}',
    			'{$sequence}',
			    '{$isOptional}',
			    '{$YearClassIdStr}',
    			NOW(),
    			NOW(),
    			'{$this->uid}',
			    '{$this->uid}',
                '{$IsExtra}'
	        )";
            $result = $this->db_db_query($sql);
        }
        return $result;
    }

    function removeAttachmentSetting($recordId)
    {
        $sql = "DELETE FROM ADMISSION_ATTACHMENT_SETTING WHERE RecordID IN (" . implode(",", (array)$recordId) . ")";
        $result = $this->db_db_query($sql);
        return $result;
    }

    function reorderAttachmentSettings($recordIdAry)
    {
        $result = array();
        for ($i = 0; $i < sizeof($recordIdAry); $i++) {
            $sql = "UPDATE ADMISSION_ATTACHMENT_SETTING SET Sequence='" . ($i + 1) . "' WHERE RecordID='" . $recordIdAry[$i] . "'";
            $result[] = $this->db_db_query($sql);
        }
        return !in_array(false, $result);
    }

    function getExportDataForImportInterview($recordID, $schoolYearID = '', $selectStatus = '', $classLevelID = '')
    {
        global $admission_cfg;
        $cond = !empty($schoolYearID) ? " AND a.SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= !empty($selectStatus) ? " AND a.Status='" . $selectStatus . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				" . $cond . "
    	";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }
    
    function getMandrillRecord($recordID, $schoolYearID = '', $byMonth=false)
    {
        global $admission_cfg;
        $cond = !empty($schoolYearID) ? " AND SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= !empty($recordID) ? " AND RecordID='" . $recordID . "'" : "";
        if($byMonth){
        	$cond .= " AND　MONTH(DateInput)　＝　MONTH(now()) ";
        }
        $sql = "
			SELECT
     			*
				ADMISSION_MANDRILL_RECORD
			WHERE 1
				" . $cond . "
    	";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

	function hasMandrillQuotaByMonth(){
		global $admission_cfg;
		
		$mandrillRecord = $this->getMandrillRecord('','',true);
		return count($mandrillRecord) < $admission_cfg['MandrillMonthlyQuota'];
	}

	function insertMandrillRecord($applicationID, $subject, $message)
    {
    	global $UserID;
    	
        $sql = "
			INSERT INTO ADMISSION_MANDRILL_RECORD
			(ApplicationID, Subject, Message, InputBy, DateInput)
			Values
			('" . $applicationID . "', '" . $this->Get_Safe_Sql_Query($subject) . "', '" . $this->Get_Safe_Sql_Query($message) . "', '" . $UserID . "', NOW())
    	";
        $result = $this->db_db_query($sql);
        
        return $result;
    }
    
    function getExportPaymentRecord($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg;
        $cond = !empty($schoolYearID) ? " AND a.SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= !empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= !empty($applicationID) ? " AND o.ApplicationID='" . $applicationID . "'" : "";
        $cond .= !empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";

        $sql = "
			SELECT
				y.YearName,
				o.ApplicationID,
				s.ChineseName,
				s.EnglishName,
				s.BirthCertNo,
				p.txn_id,
				p.receiver_email,
				p.mc_currency,
				p.mc_gross,
				p.inputdate,
				o.Remarks
			FROM
				ADMISSION_APPLICATION_STATUS a
			JOIN
				ADMISSION_OTHERS_INFO o ON o.ApplicationID = a.ApplicationID
			JOIN
				ADMISSION_STU_INFO s ON o.ApplicationID = s.ApplicationID
			JOIN
				YEAR y ON y.YearID = o.ApplyLevel
			LEFT JOIN
				ADMISSION_PAYMENT_INFO p ON o.ApplicationID = p.ApplicationID
			WHERE 1
				" . $cond . "
			";

        // $sql = "
        // SELECT
        // a.ApplicationID applicationID,
        // s.EnglishName englishName,
        // s.ChineseName chineseName,
        // s.BirthCertNo birthCertNo,
        // IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
        // IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime
        // FROM
        // ADMISSION_APPLICATION_STATUS a
        // INNER JOIN
        // ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
        // INNER JOIN
        // ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
        // WHERE 1
        // ".$cond."
        // ";
        $applicationAry = $this->returnArray($sql);
        return current($applicationAry);
    }
    // function getExportDataForImportInterview($recordID, $schoolYearID=''){
    // global $admission_cfg;
    // $cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
    // $cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
    // $sql = "
    // SELECT
    // a.ApplicationID applicationID,
    // s.EnglishName englishName,
    // s.ChineseName chineseName,
    // s.BirthCertNo birthCertNo,
    // IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
    // IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME(a.InterviewDate),'') As interviewtime
    // FROM
    // ADMISSION_APPLICATION_STATUS a
    // INNER JOIN
    // ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
    // INNER JOIN
    // ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
    // WHERE 1
    // ".$cond."
    // ";
    // $applicationAry = $this->returnArray($sql);
    // return current($applicationAry);
    // }
    function checkBirthCertNo($birthCertNo)
    {
        $birthCertNo = strtoupper($birthCertNo);
        $result = preg_match('/^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/', $birthCertNo, $ra);

        if ($result) {
            $p1 = $ra[1];
            $p2 = $ra[2];
            $p3 = $ra[4];
            $check_sum = 0;
            if (strlen($p1) == 2) {
                $check_sum = (ord($p1[0]) - 55) * 9 + (ord($p1[1]) - 55) * 8;
            } else
                if (strlen($p1) == 1) {
                    $check_sum = 324 + (ord($p1[0]) - 55) * 8;
                }

            $check_sum += ($p2[0]) * 7 + ($p2[1]) * 6 + ($p2[2]) * 5 + ($p2[3]) * 4 + ($p2[4]) * 3 + ($p2[5]) * 2;
            $check_digit = 11 - ($check_sum % 11);

            if ($check_digit == '11') {
                $check_digit = 0;
            } else
                if ($check_digit == '10') {
                    $check_digit = 'A';
                }

            if ($check_digit == $p3) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    function checkImportDataForImportInterview($data)
    {
        $resultArr = array();
        $i = 0;
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);
            // check date
            if ($aData[4] == '' && $aData[5] == '') {
                $validDate = true;
            } else
                if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4])) {
                    list ($year, $month, $day) = explode('-', $aData[4]);
                    $validDate = checkdate($month, $day, $year);
                } else {
                    $validDate = false;
                }

            // check time
            if ($aData[4] == '' && $aData[5] == '') {
                $validTime = true;
            } else
                if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5])) {
                    $validTime = true;
                } else {
                    $validTime = false;
                }
            $sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE
					trim(s.applicationID) = '" . trim($aData[0]) . "' AND trim(s.birthCertNo) = '" . trim($aData[3]) . "'
	    	";
            $result = $this->returnVector($sql);
            if ($result[0] == 0) {
                $resultArr[$i]['validData'] = $aData[0];
            } else
                $resultArr[$i]['validData'] = false;
            $resultArr[$i]['validDate'] = $validDate;
            $resultArr[$i]['validTime'] = $validTime;
            if (!$validDate || !$validTime)
                $resultArr[$i]['validData'] = $aData[0];
            $i++;
        }
        return $resultArr;
    }

    function importDataForImportInterview($data)
    {
        $resultArr = array();
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);

            $sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET
		   		InterviewDate = '" . $aData[4] . " " . $aData[5] . "',
				DateModified = NOW(),
		   		ModifiedBy = '" . $this->uid . "'
   				WHERE ApplicationID = '" . $aData[0] . "'
	    	";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;
        }
        return $resultArr;
    }

    function checkImportDataForImportInterviewSettings($data)
    {
        global $kis_lang, $admission_cfg;
        $resultArr = array();
        $i = 0;
        foreach ($data as $aData) {

            // check form
            $resultArr[$i]['validClassLevel'] = false;
            foreach ($this->classLevelAry as $_classLevelId => $_classLevelName) {
                if (strtoupper($aData[0]) == strtoupper($_classLevelName)) {
                    $resultArr[$i]['validClassLevel'] = true;
                    break;
                }
            }

            $aData[1] = getDefaultDateFormat($aData[1]);

            // check date
            if (trim($aData[1]) == '') {
                $resultArr[$i]['validInterviewDate'] = false;
            } else
                if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[1])) {
                    list ($year, $month, $day) = explode('-', $aData[1]);
                    $resultArr[$i]['validInterviewDate'] = checkdate($month, $day, $year);
                } else {
                    $resultArr[$i]['validInterviewDate'] = false;
                }

            // check time start
            if ($aData[2] == '') {
                $resultArr[$i]['validInterviewTimeStart'] = false;
            } else
                if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[2])) {
                    $resultArr[$i]['validInterviewTimeStart'] = true;
                } else {
                    $resultArr[$i]['validInterviewTimeStart'] = false;
                }

            // check time end
            if ($aData[3] == '') {
                $resultArr[$i]['validInterviewTimeEnd'] = false;
            } else
                if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[3])) {
                    $resultArr[$i]['validInterviewTimeEnd'] = true;
                } else {
                    $resultArr[$i]['validInterviewTimeEnd'] = false;
                }

            // check quota
            if ($aData[4] == '') {
                $resultArr[$i]['validInterviewQuota'] = false;
            } else
                if (is_numeric($aData[4]) && $aData[4] >= 0) {
                    $resultArr[$i]['validInterviewQuota'] = true;
                } else {
                    $resultArr[$i]['validInterviewQuota'] = false;
                }

            // check room/session
            if ($aData[5] == '') {
                $resultArr[$i]['validInterviewRoomSection'] = true;
            } else
                if ($admission_cfg['interview_arrangment']['interview_group_name']) {
                    $resultArr[$i]['validInterviewRoomSection'] = in_array($aData[5], $admission_cfg['interview_arrangment']['interview_group_name']);
                } else {
                    $resultArr[$i]['validInterviewRoomSection'] = preg_match("/^[a-zA-Z]$/", $aData[5]);
                }

            $resultArr[$i]['validDataClassLevel'] = $aData[0];
            $resultArr[$i]['validDataInterviewDate'] = $aData[1] . ' (' . $aData[2] . ' ~ ' . $aData[3] . ')';
            $i++;
        }
        // return $resultArr;
        $result = $resultArr;

        // for printing the error message
        $errCount = 0;

        $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['form'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['interviewdate'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
        $i = 1;
        foreach ($result as $aResult) {
            // developing
            $hasError = false;
            $errorMag = '';
            if (!$aResult['validClassLevel']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importClassLevelNotFound'];
            } else
                if (!$aResult['validInterviewDate']) {
                    $hasError = true;
                    $errorMag = $kis_lang['msg']['importInvalidInterviewDate'];
                } else
                    if (!$aResult['validInterviewTimeStart']) {
                        $hasError = true;
                        $errorMag = $kis_lang['msg']['importInvalidInterviewTimeStart'];
                    } else
                        if (!$aResult['validInterviewTimeEnd']) {
                            $hasError = true;
                            $errorMag = $kis_lang['msg']['importInvalidInterviewTimeEnd'];
                        } else
                            if (!$aResult['validInterviewQuota']) {
                                $hasError = true;
                                $errorMag = $kis_lang['msg']['importInvalidInterviewQuota'];
                            } else
                                if (!$aResult['validInterviewRoomSection']) {
                                    $hasError = true;
                                    if ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room') {
                                        $errorMag = $kis_lang['msg']['importInvalidInterviewSettingRoom'];
                                    } else {
                                        $errorMag = $kis_lang['msg']['importInvalidInterviewSettingSession'];
                                    }
                                }

            // print the error msg to the client
            if ($hasError) {
                $errCount++;
                $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validDataClassLevel'] . '</td>
					<td>' . $aResult['validDataInterviewDate'] . '</td>
					<td><font color="red">';
                $x .= $errorMag;
                $x .= '</font></td></tr>';
            }
            $i++;
        }
        $x .= '</tbody></table>';
        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
    }

    function importDataForImportInterviewSettings($schoolYearID, $data, $round = 1)
    {
        $resultArr = array();
        array_shift($data);
        foreach ($data as $aData) {
            $aData[1] = getDefaultDateFormat($aData[1]);

            foreach ($this->classLevelAry as $_classLevelId => $_classLevelName) {
                if (strtoupper($aData[0]) == strtoupper($_classLevelName)) {
                    $classLevelId = $_classLevelId;
                    break;
                }
            }

            $sql = "
				INSERT INTO ADMISSION_INTERVIEW_SETTING
				(SchoolYearID, ClassLevelID, Date, StartTime, EndTime, Quota, GroupName, Round, InputBy, DateModified)
				Values
				('" . $schoolYearID . "', '" . $classLevelId . "', '" . $aData[1] . "', '" . $aData[2] . "', '" . $aData[3] . ":59', '" . $aData[4] . "', '" . $aData[5] . "', '" . $round . "', '" . $this->uid . "', NOW())
	    	";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;
        }
        return $resultArr;
    }

    function checkImportDataForImportInterviewApplicant($data, $schoolYearID, $round = 1)
    {
        global $kis_lang, $admission_cfg;
        $resultArr = array();
        $i = 0;

        $sql = "SELECT
	        ApplicationID,
	        ApplyLevel
        FROM
	        ADMISSION_OTHERS_INFO O
        WHERE
	        ApplyYear = '{$schoolYearID}'";
        $rs = $this->returnResultSet($sql);
        $applicationLevelMapping = BuildMultiKeyAssoc($rs, array(
            'ApplicationID'
        ), array(
            'ApplyLevel'
        ), $SingleValue = 1);

        // call libkis_admission.php => getInterviewSettingAry()
        $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '', $round);
        $interviewSettings = array();
        $interviewQuota = array();
        foreach ($rs as $r) {
            $interviewSettings[$r['ClassLevelID']][$r['Date']][$r['StartTime']][$r['EndTime']][$r['GroupName']][] = $r;

            // $remainsQuota = $r['Quota'] - $r['Applied'];
            $remainsQuota = $r['Quota']; // Remove all data before import, no need to count old data
            $interviewQuota[$r['ClassLevelID']][$r['Date']][$r['StartTime']][$r['EndTime']][$r['GroupName']] += $remainsQuota;
        }

        $applicantIds = array();
        foreach ($data as $aData) {
            // check form
            $resultArr[$i]['validClassLevel'] = false;
            foreach ($this->classLevelAry as $_classLevelId => $_classLevelName) {
                if (strtoupper($aData[0]) == strtoupper($_classLevelName)) {
                    $resultArr[$i]['validClassLevel'] = true;
                    break;
                }
            }

            $aData[1] = getDefaultDateFormat($aData[1]);

            // check date
            if (trim($aData[1]) == '') {
                $resultArr[$i]['validInterviewDate'] = false;
            } else
                if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[1])) {
                    list ($year, $month, $day) = explode('-', $aData[1]);
                    $resultArr[$i]['validInterviewDate'] = checkdate($month, $day, $year);
                } else {
                    $resultArr[$i]['validInterviewDate'] = false;
                }

            // check time start
            if ($aData[2] == '') {
                $resultArr[$i]['validInterviewTimeStart'] = false;
            } else
                if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[2])) {
                    $resultArr[$i]['validInterviewTimeStart'] = true;
                } else {
                    $resultArr[$i]['validInterviewTimeStart'] = false;
                }

            // check time end
            if ($aData[3] == '') {
                $resultArr[$i]['validInterviewTimeEnd'] = false;
            } else
                if (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[3])) {
                    $resultArr[$i]['validInterviewTimeEnd'] = true;
                } else {
                    $resultArr[$i]['validInterviewTimeEnd'] = false;
                }

            // check room/session
            if ($aData[4] == '') {
                $resultArr[$i]['validInterviewRoomSession'] = false;
            } else
                if ($admission_cfg['interview_arrangment']['interview_group_name'] && !in_array($aData[4], $admission_cfg['interview_arrangment']['interview_group_name'])) {
                    $resultArr[$i]['validInterviewRoomSession'] = false;
                } else {

                    $resultArr[$i]['validInterviewRoomSession'] = true;
                }

            // check applicant
            if ($aData[6] == '') {
                $resultArr[$i]['validInterviewApplicant'] = false;
            } else
                if (!in_array($aData[6], array_keys($applicationLevelMapping))) {
                    $resultArr[$i]['validInterviewApplicant'] = false;
                } else
                    if (strtoupper($this->classLevelAry[$applicationLevelMapping[$aData[6]]]) != strtoupper($aData[0])) {
                        $resultArr[$i]['validInterviewApplicantWrongClassLevel'] = false;
                        $resultArr[$i]['validInterviewApplicant'] = true;
                    } else
                        if (in_array($aData[6], $applicantIds)) {
                            $resultArr[$i]['validInterviewApplicantWrongClassLevel'] = true;
                            $resultArr[$i]['validInterviewApplicant'] = true;
                            $resultArr[$i]['validInterviewApplicantDuplicate'] = false;
                        } else {
                            $resultArr[$i]['validInterviewApplicantWrongClassLevel'] = true;
                            $resultArr[$i]['validInterviewApplicant'] = true;
                            $resultArr[$i]['validInterviewApplicantDuplicate'] = true;
                        }
            $applicantIds[] = $aData[6];

            // ### Check Inteview Settings ####
            $classLevelId = $applicationLevelMapping[$aData[6]];
            $date = $aData[1];
            $startTime = date('H:i:00', strtotime($aData[2]));
            $endTime = date('H:i:59', strtotime($aData[3]));
            $roomSession = $aData[4];

            $settings = $interviewSettings[$classLevelId][$date][$startTime][$endTime][$roomSession];
            $quotaLeft = --$interviewQuota[$classLevelId][$date][$startTime][$endTime][$roomSession];

            if (!$settings) {
                if (isset($interviewSettings[$classLevelId][$date][$startTime][$endTime])) {
                    $resultArr[$i]['validInterviewSetting'] = true;
                    $resultArr[$i]['validInterviewSettingRoom'] = false;
                } else {
                    $resultArr[$i]['validInterviewSetting'] = false;
                    $resultArr[$i]['validInterviewSettingRoom'] = true;
                }
            } else
                if ($quotaLeft < 0) {
                    $resultArr[$i]['validInterviewSetting'] = true;
                    $resultArr[$i]['validInterviewSettingRoom'] = true;
                    $resultArr[$i]['validInterviewQuota'] = false;
                } else {
                    $resultArr[$i]['validInterviewSetting'] = true;
                    $resultArr[$i]['validInterviewSettingRoom'] = true;
                    $resultArr[$i]['validInterviewQuota'] = true;
                }
            // ### Check Inteview END ####

            $resultArr[$i]['validDataClassLevel'] = $aData[0];
            $resultArr[$i]['validDataInterviewDate'] = $aData[1] . ' (' . $aData[2] . ' ~ ' . $aData[3] . ')&nbsp;&nbsp;';
            $resultArr[$i]['validDataInterviewDate'] .= ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room') ? "{$kis_lang['interviewroom']}: {$aData[4]}" : "{$kis_lang['sessiongroup']}: {$aData[4]}";
            $i++;
        }
        // return $resultArr;
        $result = $resultArr;

        // for printing the error message
        $errCount = 0;

        $x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">' . $kis_lang['Row'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['form'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['interviewdate'] . '</th>
					<th class="tablebluetop tabletopnolink">' . $kis_lang['importRemarks'] . '</th>
				</tr>';
        $i = 1;
        foreach ($result as $aResult) {
            // developing
            $hasError = false;
            $errorMag = '';
            if (!$aResult['validClassLevel']) {
                $hasError = true;
                $errorMag = $kis_lang['msg']['importClassLevelNotFound'];
            } else
                if (!$aResult['validInterviewDate']) {
                    $hasError = true;
                    $errorMag = $kis_lang['msg']['importInvalidInterviewDate'];
                } else
                    if (!$aResult['validInterviewTimeStart']) {
                        $hasError = true;
                        $errorMag = $kis_lang['msg']['importInvalidInterviewTimeStart'];
                    } else
                        if (!$aResult['validInterviewTimeEnd']) {
                            $hasError = true;
                            $errorMag = $kis_lang['msg']['importInvalidInterviewTimeEnd'];
                        } else
                            if (!$aResult['validInterviewApplicant']) {
                                $hasError = true;
                                $errorMag = $kis_lang['msg']['importInvalidApplicant'];
                            } else
                                if (!$aResult['validInterviewApplicantWrongClassLevel']) {
                                    $hasError = true;
                                    $errorMag = $kis_lang['msg']['importInvalidApplicantClassLevel'];
                                } else
                                    if (!$aResult['validInterviewApplicantDuplicate']) {
                                        $hasError = true;
                                        $errorMag = $kis_lang['msg']['importDuplicateApplicant'];
                                    } else
                                        if (!$aResult['validInterviewSetting']) {
                                            $hasError = true;
                                            $errorMag = $kis_lang['msg']['importInvalidInterviewSetting'];
                                        } else
                                            if (!$aResult['validInterviewSettingRoom']) {
                                                $hasError = true;
                                                $errorMag = $kis_lang['msg']['importInvalidInterviewSettingRoom'];
                                            } else
                                                if (!$aResult['validInterviewQuota']) {
                                                    $hasError = true;
                                                    $errorMag = $kis_lang['msg']['importInvalidInterviewQuotaLeft'];
                                                }

            // print the error msg to the client
            if ($hasError) {
                $errCount++;
                $x .= '<tr class="step2">
					<td>' . $i . '</td>
					<td>' . $aResult['validDataClassLevel'] . '</td>
					<td>' . $aResult['validDataInterviewDate'] . '</td>
					<td><font color="red">';
                $x .= $errorMag;
                $x .= '</font></td></tr>';
            }
            $i++;
        }
        $x .= '</tbody></table>';
        return htmlspecialchars((count($data) - $errCount) . "," . $errCount . "," . $x);
    }

    function importDataForImportInterviewApplicant($schoolYearID, $data, $round = 1)
    {
        $resultArr = array();
        array_shift($data);

        // ### Remove all inteview record for that school year and round START ####
        $field = 'InterviewSettingID' . ($round > 1 ? $round : '');
        $sql = "UPDATE
	        ADMISSION_OTHERS_INFO
        SET
	        {$field} = '0'
        WHERE
            ApplyYear='{$schoolYearID}'
        ";
        $result = $this->db_db_query($sql);
        $resultArr[] = $result;
        // ### Remove all inteview record for that school year and round END ####

        // ### Get all class level START ####
        $sql = "SELECT
            ApplicationID,
            ApplyLevel
        FROM
            ADMISSION_OTHERS_INFO O
        WHERE
            ApplyYear = '{$schoolYearID}'";
        $rs = $this->returnResultSet($sql);
        $applicationLevelMapping = BuildMultiKeyAssoc($rs, array(
            'ApplicationID'
        ), array(
            'ApplyLevel'
        ), $SingleValue = 1);
        // ### Get all class level START ####

        // ### Get all interview settings START ####
        // call libkis_admission.php => getInterviewSettingAry()
        $rs = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, '', $round);
        $interviewSettings = array();
        $interviewQuota = array();
        foreach ($rs as $r) {
            $interviewSettings[strtoupper($r['ClassLevelID'])][$r['Date']][$r['StartTime']][$r['EndTime']][$r['GroupName']][] = $r;

            // $remainsQuota = $r['Quota'] - $r['Applied'];
            $remainsQuota = $r['Quota']; // Remove all data before import, no need to count old data
            $interviewQuota[$r['RecordID']] = $remainsQuota;
        }
        // ### Get all interview settings END ####

        // ### Update inteview record START ####
        foreach ($data as $aData) {
            $aData[1] = getDefaultDateFormat($aData[1]);

            $field = 'InterviewSettingID' . ($round > 1 ? $round : '');
            $classLevelId = strtoupper($applicationLevelMapping[$aData[6]]);
            $date = $aData[1];
            $startTime = date('H:i:00', strtotime($aData[2]));
            $endTime = date('H:i:59', strtotime($aData[3]));
            $roomSession = $aData[4];

            $settings = $interviewSettings[$classLevelId][$date][$startTime][$endTime][$roomSession];
            $settingId = 0;
            foreach ($settings as $setting) {
                $_settingId = $setting['RecordID'];
                if ($interviewQuota[$_settingId] > 0) {
                    $settingId = $_settingId;
                    $interviewQuota[$_settingId]--;
                    break;
                }
            }

            $sql = "UPDATE
                ADMISSION_OTHERS_INFO
            SET
                {$field} = '{$settingId}'
            WHERE
                ApplyYear = '{$schoolYearID}'
            AND
                ApplicationID = '{$aData[6]}'
            ";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;
        }
        // ### Update inteview record END ####

        return $resultArr;
    }

    function Get_Time_Selection($li, $ID_Name, $DefaultTime = '', $others_tab = '', $show_sec = false)
    {
        list ($hr, $min, $sec) = explode(":", $DefaultTime);
        if (!is_array($others_tab)) {
            $tmp = $others_tab;
            $others_tab = array();
            for ($i = 0; $i < 3; $i++)
                $others_tab[$i] = $tmp;
        }

        $time_sel[] = $li->Get_Time_Selection_Box($ID_Name . "_hour", "hour", $hr, $others_tab[0]);
        $time_sel[] = $li->Get_Time_Selection_Box($ID_Name . "_min", "min", $min, $others_tab[1]);
        $time_sel[] = $li->Get_Time_Selection_Box($ID_Name . "_sec", "sec", $sec, $others_tab[2]);

        return $time_sel[0] . ':' . $time_sel[1] . ($show_sec ? '' : '<div style="display:none">') . $time_sel[2] . ($show_sec ? '' : '</div>');
    }

    function Get_Number_Selection($ID_Name, $MinValue, $MaxValue, $SelectedValue = '', $Onchange = '', $noFirst = 0, $isAll = 0, $FirstTitle = '', $Disabled = 0, $OptionText = '')
    {
        global $Lang;

        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';

        $disabled = '';
        if ($Disabled == 1)
            $disabled = ' disabled ';

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $disabled;

        for ($i = $MinValue; $i <= $MaxValue; $i++)
            $selectArr[$i] = ($OptionText ? sprintf($OptionText, $i) : $i);

        if ($FirstTitle == '')
            $FirstTitle = ($isAll) ? $Lang['Btn']['All'] : $Lang['Btn']['Select'];

        $FirstTitle = Get_Selection_First_Title($FirstTitle);
        $NumberSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedValue, $isAll, $noFirst, $FirstTitle);

        return $NumberSelection;
    }

    function Get_Alphabet_Selection($ID_Name, $MinValue, $MaxValue, $SelectedValue = '', $Onchange = '', $noFirst = 0, $isAll = 0, $FirstTitle = '', $Disabled = 0)
    {
        global $Lang;

        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';

        $disabled = '';
        if ($Disabled == 1)
            $disabled = ' disabled ';

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $disabled;

        for ($i = ord($MinValue); $i <= ord($MaxValue); $i++)
            $selectArr[chr($i)] = chr($i);

        if ($FirstTitle == '')
            $FirstTitle = ($isAll) ? $Lang['Btn']['All'] : $Lang['Btn']['Select'];

        $FirstTitle = Get_Selection_First_Title($FirstTitle);
        $NumberSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedValue, $isAll, $noFirst, $FirstTitle);

        return $NumberSelection;
    }

    function hasToken($token)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '" . $token . "' ";
        return current($this->returnVector($sql));
    }

    function getOtherInfoByToken($token)
    {
        $sql = "SELECT ApplicationID, ApplyYear, ApplyLevel FROM ADMISSION_OTHERS_INFO WHERE Token = '" . $token . "' ";
        return current($this->returnArray($sql));
    }

    /*
     * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
     */
    public function sendMailToNewApplicant($applicationId, $subject, $message, $isEdit = false)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $sys_custom, $admission_cfg;
        include_once($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new libwebmail();

		if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
			$from = "no-reply@".$_SERVER['SERVER_NAME'];
		}
		else{
        	$from = $libwebmail->GetWebmasterMailAddress();
		}
        $inputby = $_SESSION['UserID'];
        $result = array();

        $sql = "SELECT
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					p.Email as Email
				FROM ADMISSION_OTHERS_INFO as f
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID
				LEFT JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=a.ApplicationID AND p.Email IS NOT NULL AND p.Email<>''
				LEFT JOIN ADMISSION_OTHERS_INFO as d ON d.ApplicationID=a.ApplicationID
				LEFT JOIN ADMISSION_APPLICATION_STATUS as b ON b.ApplicationID=a.ApplicationID
				WHERE f.ApplicationID = '" . trim($applicationId) . "' AND p.PG_TYPE = 'F'
				ORDER BY a.ApplicationID";
        $records = current($this->returnArray($sql));
        // debug_pr($applicationId);
        $to_email = $records['Email'];
        if ($subject == '') {
            $email_subject = "善明托兒所入學申請通知";
        } else {
            $email_subject = $subject;
        }
        $email_message = $message;
        $sent_ok = true;
        if ($to_email != '' && intranet_validateEmail($to_email)) {
        	
        	if($admission_cfg['EnableMandrill'] && $this->hasMandrillQuotaByMonth()){
        		$sent_ok = $this->sendMandrillMail($email_subject, $email_message, $from, array($to_email));
        		if($sent_ok){
        			$this->insertMandrillRecord($applicationId, $email_subject, $email_message);
        		}
        	}
        	else{
	            $sent_ok = $libwebmail->sendMail($email_subject, $email_message, $from, array(
	                $to_email
	            ), array(), array(), "", $IsImportant = "", $mail_return_path = ($sys_custom['KIS_Admission']['STANDARD']['Settings']?"no-reply@".$_SERVER['SERVER_NAME']:get_webmaster()), $reply_address = "", $isMulti = null, $nl2br = 0);
        	}
        } else {
            $sent_ok = false;
        }

        return $sent_ok;
    }

    function getApplicantEmail($applicationId)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;

        $sql = "SELECT
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					p.Email as Email
				FROM ADMISSION_OTHERS_INFO as f
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID
				LEFT JOIN ADMISSION_PG_INFO as p ON p.ApplicationID=a.ApplicationID AND p.Email IS NOT NULL AND p.Email<>''
				LEFT JOIN ADMISSION_OTHERS_INFO as d ON d.ApplicationID=a.ApplicationID
				LEFT JOIN ADMISSION_APPLICATION_STATUS as b ON b.ApplicationID=a.ApplicationID
				WHERE f.ApplicationID = '" . trim($applicationId) . "' AND p.PG_TYPE = 'F'
				ORDER BY a.ApplicationID";
        $records = current($this->returnArray($sql));

        return $records['Email'];
    }

    // copy from sinmeng -> libadmission_ui_cust.php
    function getApplicationSetting($schoolYearID = '')
    {
        $schoolYearID = $schoolYearID ? $schoolYearID : $this->schoolYearID;

        $sql = "
			SELECT
     			ClassLevelID,
			    IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d %H:%i'),'') As StartDate,
				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d %H:%i'),'') As EndDate,
				IF(DOBStart,DATE_FORMAT(DOBStart,'%Y-%m-%d'),'') As DOBStart,
				IF(DOBEnd,DATE_FORMAT(DOBEnd,'%Y-%m-%d'),'') As DOBEnd,
				IF(PreviewStartDate,DATE_FORMAT(PreviewStartDate,'%Y-%m-%d %H:%i'),'') As PreviewStartDate,
				IF(PreviewEndDate,DATE_FORMAT(PreviewEndDate,'%Y-%m-%d %H:%i'),'') As PreviewEndDate,
				IF(UpdateStartDate,DATE_FORMAT(UpdateStartDate,'%Y-%m-%d %H:%i'),'') As UpdateStartDate,
				IF(UpdateEndDate,DATE_FORMAT(UpdateEndDate,'%Y-%m-%d %H:%i'),'') As UpdateEndDate,
				IF(UpdateExtraAttachmentStartDate,DATE_FORMAT(UpdateExtraAttachmentStartDate,'%Y-%m-%d %H:%i'),'') As UpdateExtraAttachmentStartDate,
				IF(UpdateExtraAttachmentEndDate,DATE_FORMAT(UpdateExtraAttachmentEndDate,'%Y-%m-%d %H:%i'),'') As UpdateExtraAttachmentEndDate,
				Quota,
			    DayType,
			    FirstPageContent,
			    FirstPageContent1,
			    FirstPageContent2,
			    FirstPageContent3,
			    LastPageContent,
			    LastPageContent1,
			    LastPageContent2,
			    LastPageContent3,
				EmailContent,
				EmailContent1,
				EmailContent2,
				EmailContent3,
			    DateInput,
			    InputBy,
			    DateModified,
			    ModifiedBy,
				SchoolYearID,
    	        CustSetting,
				AllowInternalUse
			FROM
				ADMISSION_APPLICATION_SETTING
			WHERE
				SchoolYearID = '" . $schoolYearID . "'
    	";
        $setting = $this->returnArray($sql);
        $applicationSettingAry = BuildMultiKeyAssoc($setting, 'ClassLevelID');
        $applicationPeriodAry = array();
        foreach ($this->classLevelAry as $_classLevelId => $_classLevelName) {
            $_startdate = $applicationSettingAry[$_classLevelId]['StartDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['StartDate'] : '';
            $_enddate = $applicationSettingAry[$_classLevelId]['EndDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['EndDate'] : '';

            $_previewstartdate = $applicationSettingAry[$_classLevelId]['PreviewStartDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['PreviewStartDate'] : '';
            $_previewenddate = $applicationSettingAry[$_classLevelId]['PreviewEndDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['PreviewEndDate'] : '';

            $_updatestartdate = $applicationSettingAry[$_classLevelId]['UpdateStartDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['UpdateStartDate'] : '';
            $_updateenddate = $applicationSettingAry[$_classLevelId]['UpdateEndDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['UpdateEndDate'] : '';

            $_updateextraattachmentstartdate = $applicationSettingAry[$_classLevelId]['UpdateExtraAttachmentStartDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['UpdateExtraAttachmentStartDate'] : '';
            $_updateextraattachmentenddate = $applicationSettingAry[$_classLevelId]['UpdateExtraAttachmentEndDate'] != '0000-00-00 00:00' ? $applicationSettingAry[$_classLevelId]['UpdateExtraAttachmentEndDate'] : '';

            $applicationPeriodAry[$_classLevelId] = array(
                'ClassLevelName' => $_classLevelName,
                'StartDate' => $_startdate,
                'EndDate' => $_enddate,
                'PreviewStartDate' => $_previewstartdate,
                'PreviewEndDate' => $_previewenddate,
                'UpdateStartDate' => $_updatestartdate,
                'UpdateEndDate' => $_updateenddate,
                'UpdateExtraAttachmentStartDate' => $_updateextraattachmentstartdate,
                'UpdateExtraAttachmentEndDate' => $_updateextraattachmentenddate,
                'DOBStart' => $applicationSettingAry[$_classLevelId]['DOBStart'],
                'DOBEnd' => $applicationSettingAry[$_classLevelId]['DOBEnd'],
                'DayType' => $applicationSettingAry[$_classLevelId]['DayType'],
                'FirstPageContent' => $applicationSettingAry[$_classLevelId]['FirstPageContent'],
                'FirstPageContent1' => $applicationSettingAry[$_classLevelId]['FirstPageContent1'],
                'FirstPageContent2' => $applicationSettingAry[$_classLevelId]['FirstPageContent2'],
                'FirstPageContent3' => $applicationSettingAry[$_classLevelId]['FirstPageContent3'],
                'LastPageContent' => $applicationSettingAry[$_classLevelId]['LastPageContent'],
                'LastPageContent1' => $applicationSettingAry[$_classLevelId]['LastPageContent1'],
                'LastPageContent2' => $applicationSettingAry[$_classLevelId]['LastPageContent2'],
                'LastPageContent3' => $applicationSettingAry[$_classLevelId]['LastPageContent3'],
                'EmailContent' => $applicationSettingAry[$_classLevelId]['EmailContent'],
                'EmailContent1' => $applicationSettingAry[$_classLevelId]['EmailContent1'],
                'EmailContent2' => $applicationSettingAry[$_classLevelId]['EmailContent2'],
                'EmailContent3' => $applicationSettingAry[$_classLevelId]['EmailContent3'],
                'Quota' => $applicationSettingAry[$_classLevelId]['Quota'],
                'SchoolYearID' => $applicationSettingAry[$_classLevelId]['SchoolYearID'],
                'CustSetting' => $applicationSettingAry[$_classLevelId]['CustSetting'],
                'AllowInternalUse' => $applicationSettingAry[$_classLevelId]['AllowInternalUse']
            );
        }
        return $applicationPeriodAry;
    }

    // copy from sinmeng -> libadmission_ui_cust.php
    public function updateApplicationSetting($data)
    {
        extract($data);
        // Check exist setting
        $sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '" . $schoolYearID . "'	AND ClasslevelID = '" . $classLevelID . "'";
        $cnt = current($this->returnVector($sql));

        if ($cnt) { // update
            $sql = "UPDATE ADMISSION_APPLICATION_SETTING SET
		   				 StartDate = '" . $startDate . "',
		  				 EndDate = '" . $endDate . "',
 						 PreviewStartDate = '" . $previewStartDate . "',
		  				 PreviewEndDate = '" . $previewEndDate . "',
						 UpdateStartDate = '" . $updateStartDate . "',
		  				 UpdateEndDate = '" . $updateEndDate . "',
						 UpdateExtraAttachmentStartDate = '" . $updateExtraAttachmentStartDate . "',
		  				 UpdateExtraAttachmentEndDate = '" . $updateExtraAttachmentEndDate . "',
						 Quota = '" . $quota . "',
						 DOBStart = '" . $dOBStart . "',
		  				 DOBEnd = '" . $dOBEnd . "',
		   				 DayType = '" . $dayType . "',
		  				 FirstPageContent = '" . $firstPageContent . "',
		  				 FirstPageContent1 = '" . $firstPageContent1 . "',
		  				 FirstPageContent2 = '" . $firstPageContent2 . "',
		  				 FirstPageContent3 = '" . $firstPageContent3 . "',
		   				 LastPageContent = '" . $lastPageContent . "',
		   				 LastPageContent1 = '" . $lastPageContent1 . "',
		   				 LastPageContent2 = '" . $lastPageContent2 . "',
		   				 LastPageContent3 = '" . $lastPageContent3 . "',
						 EmailContent = '" . $emailContent . "',
						 EmailContent1 = '" . $emailContent1 . "',
						 EmailContent2 = '" . $emailContent2 . "',
						 EmailContent3 = '" . $emailContent3 . "',
						 CustSetting = '" . $custSettings . "',
		   				 DateModified = NOW(),
		   				 ModifiedBy = '" . $this->uid . "',
						 AllowInternalUse = '" . ($allowInternalUse ? $allowInternalUse : 0) . "'
   					WHERE SchoolYearID = '" . $schoolYearID . "'	AND ClasslevelID = '" . $classLevelID . "'";
        } else { // insert
            $sql = "INSERT INTO ADMISSION_APPLICATION_SETTING (
   						SchoolYearID,
   						ClassLevelID,
					    StartDate,
					    EndDate,
						PreviewStartDate,
		  				PreviewEndDate,
						UpdateStartDate,
						UpdateEndDate,
						UpdateExtraAttachmentStartDate,
						UpdateExtraAttachmentEndDate,
						Quota,
						DOBStart,
		  				DOBEnd,
					    DayType,
		  				FirstPageContent,
		  				FirstPageContent1,
		  				FirstPageContent2,
		  				FirstPageContent3,
		   				LastPageContent,
		   				LastPageContent1,
		   				LastPageContent2,
		   				LastPageContent3,
						EmailContent,
						EmailContent1,
						EmailContent2,
						EmailContent3,
						CustSetting,
					    DateInput,
					    InputBy,
					    DateModified,
					    ModifiedBy,
                        AllowInternalUse)
					VALUES (
					    '" . $schoolYearID . "',
					    '" . $classLevelID . "',
					    '" . $startDate . "',
					    '" . $endDate . "',
						'" . $previewStartDate . "',
						'" . $previewEndDate . "',
						'" . $updateStartDate . "',
						'" . $updateEndDate . "',
						'" . $updateExtraAttachmentStartDate . "',
						'" . $updateExtraAttachmentEndDate . "',
						'" . $quota . "',
						'" . $dOBStart . "',
						'" . $dOBEnd . "',
					    '" . $dayType . "',
					    '" . $firstPageContent . "',
					    '" . $firstPageContent1 . "',
					    '" . $firstPageContent2 . "',
					    '" . $firstPageContent3 . "',
					    '" . $lastPageContent . "',
					    '" . $lastPageContent1 . "',
					    '" . $lastPageContent2 . "',
					    '" . $lastPageContent3 . "',
						'" . $emailContent . "',
						'" . $emailContent1 . "',
						'" . $emailContent2 . "',
						'" . $emailContent3 . "',
						'" . $custSettings . "',
					    NOW(),
					    '" . $this->uid . "',
					     NOW(),
					    '" . $this->uid . "',
						'" . ($allowInternalUse ? $allowInternalUse : 0) . "')
			";
        }
        return $this->db_db_query($sql);
    }

    public function IsPreviewPeriod()
    {
        global $sys_custom;
        $application_setting = $this->getApplicationSetting();
        $allowToPreview = false;
        foreach ($application_setting as $key => $value) {
            if ($sys_custom['KIS_Admission']['PreviewFormMode'] && date('Y-m-d H:i') >= $value['PreviewStartDate'] && date('Y-m-d H:i') <= $value['PreviewEndDate']) {
                $allowToPreview = true;
                break;
            }
        }
        return $allowToPreview;
    }

    public function IsUpdatePeriod()
    {
        global $sys_custom;
        $application_setting = $this->getApplicationSetting();
        $allowToUpdate = false;
        foreach ($application_setting as $key => $value) {
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && date('Y-m-d H:i') >= $value['UpdateStartDate'] && date('Y-m-d H:i') <= $value['UpdateEndDate']) {
                $allowToUpdate = true;
                break;
            }
        }
        return $allowToUpdate;
    }

    public function IsUpdateExtraAttachmentPeriod()
    {
        global $sys_custom;
        $application_setting = $this->getApplicationSetting();
        $allowToUpdate = false;
        foreach ($application_setting as $key => $value) {
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && date('Y-m-d H:i') >= $value['UpdateExtraAttachmentStartDate'] && date('Y-m-d H:i') <= $value['UpdateExtraAttachmentEndDate']) {
                $allowToUpdate = true;
                break;
            }
        }
        return $allowToUpdate;
    }

    public function IsAfterUpdatePeriod()
    {
        global $sys_custom;
        $application_setting = $this->getApplicationSetting();
        $allowToUpdate = false;
        foreach ($application_setting as $key => $value) {
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && date('Y-m-d H:i') > $value['UpdateEndDate'] && $value['UpdateEndDate'] && $value['UpdateEndDate'] != "0000-00-00 00:00:00" && date('Y-m-d H:i') > $value['EndDate']) {
                $allowToUpdate = true;
                break;
            }
        }
        return $allowToUpdate;
    }

    public function NumOfQuotaLeft($ClassLevelID, $schoolYearID)
    {
        global $sys_custom;
        if (!$sys_custom['KIS_Admission']['ApplyQuotaSettings']/* || ($_SESSION["platform"]=="KIS" && $_SESSION["UserID"])*/)
            return 1000;

        $application_setting = $this->getApplicationSetting($schoolYearID);
        $numOfQuotaTotal = $application_setting[$ClassLevelID]['Quota'];
        $appliedTotal = count($this->getApplicationOthersInfo(($schoolYearID == '' ? $this->schoolYearID : $schoolYearID), $ClassLevelID));

        return $numOfQuotaTotal - $appliedTotal;
    }

    // check interview quota function here
    public function getInterviewSettingAry($recordID = '', $date = '', $startTime = '', $endTime = '', $keyword = '', $schoolYearID = '', $classLevelID = '')
    {
        global $admission_cfg, $Lang;
        // $schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

        if ($schoolYearID != '') {
            $cond .= ' AND i.SchoolYearID = \'' . $schoolYearID . '\' ';
        }
        if ($classLevelID != '') {
            $cond .= ' AND i.ClassLevelID = \'' . $classLevelID . '\' ';
        }
        if ($recordID != '') {
            $cond .= ' AND i.RecordID = \'' . $recordID . '\' ';
        }
        if ($date != '') {
            $cond .= ' AND i.Date >= \'' . $date . '\' ';
        }
        if ($startTime != '') {
            $cond .= ' AND i.StartTime >= \'' . $startTime . '\' ';
        }
        if ($endTime != '') {
            $cond .= ' AND i.EndTime <= \'' . $endTime . '\' ';
        }
        if ($keyword != '') {
            $search_cond = ' AND (i.Date LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.StartTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.EndTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\')';
        }
        $sql = 'SELECT i.RecordID as RecordID, i.SchoolYearID as SchoolYearID, i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.Quota as Quota, COUNT(o.InterviewSettingID) as Applied, i.GroupName as GroupName
				FROM ADMISSION_INTERVIEW_SETTING AS i
				LEFT JOIN ADMISSION_OTHERS_INFO AS o ON i.RecordID = o.InterviewSettingID
				WHERE 1 ' . $cond . ' ' . $search_cond . '
				GROUP BY i.RecordID
				ORDER BY ClassLevelID, Date, StartTime, EndTime';
        // debug_r($sql);
        $interviewAry = $this->returnArray($sql);

        return $interviewAry;
    }

    public function NumOfInterviewQuotaLeft($recordID)
    {
        global $sys_custom;
        if (!$sys_custom['KIS_Admission']['InterviewSettings'])
            return 1000;

        $interview_setting = current($this->getInterviewSettingAry($recordID));

        return $interview_setting['Quota'] - $interview_setting['Applied'];
    }

    public function NumOfInterviewQuotaLeftByClassLevel($schoolYearID, $classLevelID)
    {
        global $sys_custom;
        if (!$sys_custom['KIS_Admission']['InterviewSettings'])
            return 1000;

        $sql = "Select StartDate, EndDate
			  From ADMISSION_APPLICATION_SETTING
			  where SchoolYearID = '" . $schoolYearID . "' and ClassLevelID = '" . $classLevelID . "'";
        $result = current($this->returnArray($sql));
        if (!(/*date('Y-m-d H:i') >= $result['StartDate'] && */ date('Y-m-d H:i') <= $result['EndDate'])) {
            return 0;
        }

        $interview_setting = $this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, $classLevelID);

        $quotaLeft = 0;
        foreach ($interview_setting as $aInterview_setting) {
            $quotaLeft += $aInterview_setting['Quota'] - $aInterview_setting['Applied'];
        }

        return $quotaLeft;
    }

    public function updateOtherInterviewSettingID($recordID, $applicationID)
    {
        if ($this->NumOfInterviewQuotaLeft($recordID) > 0) {
            $sql = "UPDATE
					ADMISSION_OTHERS_INFO
				SET
					 InterviewSettingID =  '" . $recordID . "'
				WHERE
					ApplicationID = '" . $applicationID . "'";
            return $this->db_db_query($sql);
        } else {
            return false;
        }
    }

    function getInterviewSettingSelection($recordID = '', $name = 'InterviewSettingID', $firstTitle = true, $firstTitleText = '', $schoolYearID = '', $classLevelID = '')
    {
        global $kis_lang, $setting_path_ip_rel;
        $x = '<select name="' . $name . '" id="' . $name . '">';
        if ($setting_path_ip_rel == 'mosgraceful.eclass.hk') {
            $kis_lang['select'] = '選擇  Select';
        }
        $x .= ($firstTitle) ? '<option value="0"' . ($recordID == '' ? ' selected="selected"' : '') . '>- ' . ($firstTitleText != '' ? $firstTitleText : $kis_lang['select']) . ' -</option>' : '';
        foreach ($this->getInterviewSettingAry('', '', '', '', '', $schoolYearID, $classLevelID) as $_classLevelId => $_classLevelName) {
            if ($_classLevelName['Quota'] > $_classLevelName['Applied'] || $recordID == $_classLevelName['RecordID']) {
                $x .= '<option value="' . $_classLevelName['RecordID'] . '"' . ($recordID == $_classLevelName['RecordID'] ? ' selected="selected"' : '') . '>';
                $x .= $_classLevelName['Date'] . ' (' . substr($_classLevelName['StartTime'], 0, -3) . ' ~ ' . substr($_classLevelName['EndTime'], 0, -3) . ')';
                $x .= '</option>';
            }
        }
        $x .= '</select>';
        return $x;
    }

    function updateApplicationClassLevelByIds($applicationIds, $classlevel)
    {
        global $UserID;
        $sql = "
			UPDATE
				ADMISSION_OTHERS_INFO o
			SET
      			o.ApplyLevel = '" . $classlevel . "',
      			o.DateModified = NOW(),
      			o.ModifiedBy = '" . $UserID . "'
     		WHERE
				o.RecordID IN (" . $applicationIds . ")
    	";
        return $this->db_db_query($sql);
    }

    function hasApplicationNumber($applicationNo)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE ApplicationID = '" . $applicationNo . "'";
        return current($this->returnVector($sql));
    }

    function importInterviewSessionBySettings($round = 1)
    {
        global $UserID, $admission_cfg;

        //// Get settings START ////
        $sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota , s.StartTime, s.EndTime, s.Group
				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
				JOIN ADMISSION_INTERVIEW_ARRANGEMENT_SESSION as s ON a.RecordID = s.RecordID
				WHERE a.Round = '{$round}'
				ORDER BY a.Sequence, a.Date, s.StartTime";
        $rs = $this->returnArray($sql);
        //// Get settings END ////

        //// Delete applicant interview record START ////
        $sql = "UPDATE
            ADMISSION_INTERVIEW_SETTING as s
        JOIN
            ADMISSION_OTHERS_INFO as o
        ON
            s.RecordID = o.InterviewSettingID" . ($round > 1 ? $round : '') . "
		Set
            o.InterviewSettingID" . ($round > 1 ? $round : '') . " = '0'
		WHERE
            s.SchoolYearID = '{$this->schoolYearID}'
        AND
            s.Round = '{$round}'
        AND
            s.GroupName IS NOT NULL";

        $insertResult[] = $this->db_db_query($sql);
        //// Delete applicant interview record END ////

        //// Delete current interview session START ////
        $insertResult = array();
        $sql = "DELETE FROM ADMISSION_INTERVIEW_SETTING
				WHERE SchoolYearID = '{$this->schoolYearID}' AND Round = '{$round}' AND GroupName IS NOT NULL";
        $insertResult[] = $this->db_db_query($sql);
        //// Delete current interview session END ////

        //// Create interview session START ////
        foreach($rs as $r) {
            for ($j = 0; $j < $r['NumOfGroup']; $j++) {
                // chr($j+97)

                #### Get group START ####
                if ($r['Group']) { // User select groups
                    $userSelectedGroups = explode(',', $r['Group']);
                    $group = $userSelectedGroups[$j];
                } else { // System generate groups
                    if ($admission_cfg['interview_arrangment']['interview_group_name'][$j]) {
                        $group = $admission_cfg['interview_arrangment']['interview_group_name'][$j];
                    } else {
                        $group = strtoupper(chr($j + 97));
                    }
                }
                #### Get group END ####

                $sql = "INSERT INTO ADMISSION_INTERVIEW_SETTING
						(SchoolYearID, ClassLevelID, Date, StartTime, EndTime, Quota, GroupName, Round, InputBy, DateModified, ModifiedBy)
						Values
						('{$this->schoolYearID}',
						'0',
						'{$r['Date']}',
						'{$r['StartTime']}',
						'{$r['EndTime']}',
						'{$r['Quota']}',
						'{$group}',
						'{$round}',
						'{$UserID}',
						NOW(),
						'{$UserID}'
						)";
                $insertResult[] = $this->db_db_query($sql);
            }
        }
        //// Create interview session END ////

        return !in_array(false, $insertResult);
    }

    function importInterviewInfoByArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds = array())
    {
        global $UserID, $admission_cfg;

        $sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota , s.StartTime, s.EndTime, s.Group
				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
				JOIN ADMISSION_INTERVIEW_ARRANGEMENT_SESSION as s ON a.RecordID = s.RecordID
				WHERE a.Round = '" . $round . "'
				ORDER BY a.Sequence, a.Date, s.StartTime";

        $arrangmentRecord = $this->returnArray($sql);

        // -- add process application date here...
        $classLevelIdSql = '';
        if ($classLevelIds) {
            $classLevelIdSql = implode("','", (array)$classLevelIds);
            $classLevelIdSql = " AND o.ApplyLevel IN ('{$classLevelIdSql}')";
        }

        $sql = "UPDATE
            ADMISSION_INTERVIEW_SETTING as s
        JOIN
            ADMISSION_OTHERS_INFO as o
        ON
            s.RecordID = o.InterviewSettingID" . ($round > 1 ? $round : '') . "
            {$classLevelIdSql}
		Set
            o.InterviewSettingID" . ($round > 1 ? $round : '') . " = '0'
		WHERE
            s.SchoolYearID = '{$selectSchoolYearID}'
        AND
            s.Round = '{$round}'
        AND
            s.GroupName IS NOT NULL";

        $insertResult[] = $this->db_db_query($sql);

        $sql = "DELETE FROM ADMISSION_INTERVIEW_SETTING
				WHERE SchoolYearID = '" . $selectSchoolYearID . "' AND Round = '" . $round . "' AND GroupName IS NOT NULL";
        $insertResult[] = $this->db_db_query($sql);

        $insertResult = array();
        for ($i = 0; $i < sizeof($arrangmentRecord); $i++) {

            $classLevelID = '';

            for ($j = 0; $j < $arrangmentRecord[$i]['NumOfGroup']; $j++) {
                // chr($j+97)

                #### Get group START ####
                if ($arrangmentRecord[$i]['Group']) { // User select groups
                    $userSelectedGroups = explode(',', $arrangmentRecord[$i]['Group']);
                    $group = $userSelectedGroups[$j];
                } else { // System generate groups
                    if ($admission_cfg['interview_arrangment']['interview_group_name'][$j]) {
                        $group = $admission_cfg['interview_arrangment']['interview_group_name'][$j];
                    } else {
                        $group = strtoupper(chr($j + 97));
                    }
                }
                #### Get group END ####

                $sql = "INSERT INTO ADMISSION_INTERVIEW_SETTING
						(SchoolYearID, ClassLevelID, Date, StartTime, EndTime, Quota, GroupName, Round, InputBy, DateModified, ModifiedBy)
						Values
						('" . $selectSchoolYearID . "',
						'" . $classLevelID . "',
						'" . $arrangmentRecord[$i]['Date'] . "',
						'" . $arrangmentRecord[$i]['StartTime'] . "',
						'" . $arrangmentRecord[$i]['EndTime'] . "',
						'" . $arrangmentRecord[$i]['Quota'] . "',
						'" . $group . "',
						'" . $round . "',
						'" . $UserID . "',
						NOW(),
						'" . $UserID . "'
						)";
                $insertResult[] = $this->db_db_query($sql);
            }
        }

        // -- insert to arragement log [start]
        $sql = "INSERT INTO ADMISSION_INTERVIEW_ARRANGEMENT_LOG
					(SchoolYearID, Round, StatusSelect, DateInput, InputBy, DateModified, ModifiedBy)
					Values
					('" . $selectSchoolYearID . "',
					'" . $round . "',
					'" . implode(",", $selectStatusArr) . "',
					NOW(),
					'" . $UserID . "',
					NOW(),
					'" . $UserID . "'
					)";
        $insertResult[] = $this->db_db_query($sql);
        // -- insert to arragement log [end]

        $insertResult[] = $this->updateApplicantArrangement($selectSchoolYearID, $selectStatusArr, $round, $classLevelIds);

        return !in_array(false, $insertResult);
    }

    function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds = array())
    {
        // pls write the function in the cust file
        return false;
    }

    /**
     * Add record to ADMISSION_CUST_INFO
     * $Data => array(
     * Code: (Required.
     * The cust field name),
     * Value: (The field value),
     * Position: (The position of the field)
     * )
     */
    function insertApplicationCustInfo($Data, $ApplicationID)
    {
        global $UserID;
        extract($Data);

        // Check exist application
        if ($ApplicationID != "") {
            $sql = "INSERT INTO ADMISSION_CUST_INFO (
						ApplicationID,
						Code,
						Value,
						Position,
						InputBy,
						DateInput)
					VALUES (
						'" . $ApplicationID . "',
						'" . $Code . "',
						'" . $Value . "',
						'" . $Position . "',
						'" . $UserID . "',
						now())
			";
            $success = $this->db_db_query($sql);
            return $success;
        }
        return false;
    }

    function getApplicationCustInfo($ApplicationID, $Code, $isSingleRow = false)
    {
        $sql = "SELECT
			*
		FROM
			ADMISSION_CUST_INFO
		WHERE
			ApplicationID='{$ApplicationID}'
		AND
			Code='{$Code}'
		ORDER BY
			Position
		";
        $rs = $this->returnResultSet($sql);
        if ($isSingleRow) {
            $count = count($rs);
            if ($count == 0) {
                return array();
            } else
                if ($count == 1) {
                    return $rs[0];
                } else {
                    return false;
                }
        }
        return $rs;
    }

    function getAllApplicationCustInfo($ApplicationID)
    {
        $sql = "SELECT
			*
		FROM
			ADMISSION_CUST_INFO
		WHERE
			ApplicationID='{$ApplicationID}'
		ORDER BY
			Code, Position, RecordID
		";
        $rs = $this->returnResultSet($sql);

        $infoArr = array();
        foreach ($rs as $r) {
            $infoArr[$r['Code']][] = $r;
        }
        return $infoArr;
    }

    /**
     * Update the record in ADMISSION_CUST_INFO by ApplicationID and Code
     * $Data => array(
     * filterRecordId: (The Primary Key for filter),
     * filterApplicationId: (The ApplicationID for filter),
     * filterCode: (The cust field name for filter),
     * filterValue: (The field value for filter),
     * filterPosition: (The position of the field for filter),
     *
     * ApplicationId: (The ApplicationID for update)
     * Code: (The cust field name for update),
     * Value: (The field value for update),
     * Position: (The position of the field for update),
     * )
     */
    function updateApplicationCustInfo($Data, $insertIfNotExists = true)
    {
        global $UserID;
        extract($Data);

        $insertItem = array();
        $filterSQL = '';
        if (isset($filterRecordId)) {
            $filterSQL .= " AND RecordID = '{$filterRecordId}'";
        }
        if (isset($filterCode)) {
            $filterSQL .= " AND Code = '{$filterCode}'";
            $insertItem['Code'] = $filterCode;
        }
        if (isset($filterApplicationId)) {
            $filterSQL .= " AND ApplicationId = '{$filterApplicationId}'";
            $insertItem['ApplicationId'] = $filterApplicationId;
        }
        if (isset($filterValue)) {
            $filterSQL .= " AND Value = '{$filterValue}'";
            $insertItem['Value'] = $filterValue;
        }
        if (isset($filterPosition)) {
            $filterSQL .= " AND Position = '{$filterPosition}'";
            $insertItem['Position'] = $filterPosition;
        }

        $updateSQL = ',';
        if (isset($ApplicationId)) {
            $updateSQL .= "ApplicationId = '{$ApplicationId}',";
            $insertItem['ApplicationId'] = $ApplicationId;
        }
        if (isset($Code)) {
            $updateSQL .= "Code = '{$Code}',";
            $insertItem['Code'] = $Code;
        }
        if (isset($Value)) {
            $updateSQL .= "Value = '{$Value}',";
            $insertItem['Value'] = $Value;
        } else {
            // $updateSQL .= "Value = NULL,";
        }
        if (isset($Position)) {
            $updateSQL .= "Position = '{$Position}',";
            $insertItem['Position'] = $Position;
        } else {
            // $updateSQL .= "Position = NULL,";
        }
        $updateSQL = trim($updateSQL, ',');
        $insertField = implode(',', array_keys($insertItem));
        $insertValue = implode("','", array_values($insertItem));

        if ($insertIfNotExists) {
            $sql = "SELECT COUNT(*) FROM ADMISSION_CUST_INFO WHERE 1=1 {$filterSQL}";
            $count = $this->returnVector($sql);
            if ($count[0] == 0) {
                $sql = "INSERT INTO
					ADMISSION_CUST_INFO
				(
					{$insertField},
					InputBy,
					DateInput
				) VALUES (
					'{$insertValue}',
					'{$UserID}',
					now()
				)";
                $success = $this->db_db_query($sql);
                return $success;
            }
        }

        $sql = "UPDATE
			ADMISSION_CUST_INFO
		SET
			DateModified = NOW(),
			ModifiedBy = '" . $UserID . "',
			{$updateSQL}
		WHERE
			1=1
			{$filterSQL}
		";
        $success = $this->db_db_query($sql);
        return $success;
    }

    /**
     * Delete the record in ADMISSION_CUST_INFO by filter
     * $Data => array(
     * filterRecordId: (The Primary Key for filter),
     * filterApplicationId: (The ApplicationID for filter),
     * filterCode: (The cust field name for filter),
     * filterValue: (The field value for filter),
     * filterPosition: (The position of the field for filter),
     * )
     */
    function deleteApplicationCustInfo($Data)
    {
        extract($Data);

        $filterSQL = '';
        if (isset($filterRecordId)) {
            $filterSQL .= " AND RecordID = '{$filterRecordId}'";
        }
        if (isset($filterCode)) {
            $filterSQL .= " AND Code = '{$filterCode}'";
        }
        if (isset($filterApplicationId)) {
            $filterSQL .= " AND ApplicationID = '{$filterApplicationId}'";
        }
        if (isset($filterValue)) {
            $filterSQL .= " AND Value = '{$filterValue}'";
        }
        if (isset($filterPosition)) {
            $filterSQL .= " AND Position = '{$filterPosition}'";
        }

        $sql = "DELETE FROM
			ADMISSION_CUST_INFO
		WHERE
			1=1
			{$filterSQL}";
        $result = $this->db_db_query($sql);
        return $result;
    }

    public function getNextApplicationRecordId($currentApplicationRecordId)
    {
        $sql = "select ApplicationID, ApplyYear, ApplyLevel from ADMISSION_OTHERS_INFO where RecordID = '{$currentApplicationRecordId}'";
        $r = current($this->returnResultSet($sql));
        if (!$r) {
            return 0;
        }

        $sql = "select RecordID from ADMISSION_OTHERS_INFO where ApplyYear='{$r['ApplyYear']}' AND ApplyLevel='{$r['ApplyLevel']}' ORDER BY ApplicationID";
        $rs = $this->returnvector($sql);

        $index = array_search($currentApplicationRecordId, $rs);
        return isset($rs[$index + 1]) ? $rs[$index + 1] : 0;
    }

    public function getPrevApplicationRecordId($currentApplicationRecordId)
    {
        $sql = "select ApplicationID, ApplyYear, ApplyLevel from ADMISSION_OTHERS_INFO where RecordID = '{$currentApplicationRecordId}'";
        $r = current($this->returnResultSet($sql));
        if (!$r) {
            return 0;
        }

        $sql = "select RecordID from ADMISSION_OTHERS_INFO where ApplyYear='{$r['ApplyYear']}' AND ApplyLevel='{$r['ApplyLevel']}' ORDER BY ApplicationID";
        $rs = $this->returnvector($sql);

        $index = array_search($currentApplicationRecordId, $rs);
        return isset($rs[$index - 1]) ? $rs[$index - 1] : 0;
    }

    function getInterviewResult($StudentDateOfBirth, $StudentBirthCertNo, $SchoolYearID = '', $round = 1, $ApplicationID = '')
    {
        if ($SchoolYearID) {
            $cond = " AND o.ApplyYear = '" . $SchoolYearID . "' ";
        }
        if ($ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationID . "' ";
        }
        $sql = "SELECT o.ApplicationID, s.ChineseName, s.EnglishName, i.Date, i.StartTime, i.EndTime, i.GroupName, st.InterviewDate, st.InterviewLocation, o.ApplyLevel, i.RecordID
				FROM ADMISSION_STU_INFO as s
				JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID
				JOIN ADMISSION_APPLICATION_STATUS as st ON st.ApplicationID = o.ApplicationID
				LEFT JOIN ADMISSION_INTERVIEW_SETTING as i ON o.InterviewSettingID" . ($round > 1 ? $round : '') . " = i.RecordID
				WHERE s.DOB = '" . $StudentDateOfBirth . "' AND s.DOB <> '' AND (s.BirthCertNo = '" . strtoupper($StudentBirthCertNo) . "' OR replace(replace(replace(s.BirthCertNo,')',''),'(',''),' ','') = '" . strtoupper($StudentBirthCertNo) . "') AND s.BirthCertNo <> '' " . $cond . " ORDER BY o.ApplicationID desc";

        $result = current($this->returnArray($sql));

        if (!$result['ApplicationID']) {
            return 0;
        }

        // if(!$result['Date']){
        // return 'NotAssigned';
        // }

        return $result;
    }

    function getPaymentResult($ApplicationNo, $StudentBirthCertNo, $token, $SchoolYearID = '', $ApplicationID = '')
    {
        if ($token) {
            $cond .= " AND MD5(o.ApplicationID) = '" . $token . "' ";
        }
        if ($ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationID . "' ";
        }
        if (!$token && !$ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationNo . "' AND s.BirthCertNo = '" . strtoupper($StudentBirthCertNo) . "' ";
        }
        if ($SchoolYearID) {
            $cond .= " AND o.ApplyYear = '" . $SchoolYearID . "' ";
        }
        $sql = "SELECT o.ApplicationID, s.ChineseName, s.EnglishName, p.payment_status, p.receiver_email, p.mc_currency, p.mc_gross, p.mc_fee, p.payment_date, p.item_name, p.payer_email, p.txn_id, p.payer_id, p.receiver_id, a.Status, p.EmailSent, p.PaymentID
				FROM ADMISSION_STU_INFO as s
				JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID
				JOIN ADMISSION_APPLICATION_STATUS as a ON a.ApplicationID = o.ApplicationID
				LEFT JOIN ADMISSION_PAYMENT_INFO as p ON o.ApplicationID = p.ApplicationID
				WHERE 1 " . $cond . " ORDER BY o.ApplicationID, p.inputdate";

        $result = $this->returnArray($sql);

        if (!$result[0]['ApplicationID']) {
            return 0;
        }

        // if(!$result['Date']){
        // return 'NotAssigned';
        // }

        return $result;
    }
    
    function getAlipayPaymentResult($ApplicationNo, $StudentBirthCertNo, $token, $SchoolYearID = '', $ApplicationID = '')
    {
        if ($token) {
            $cond .= " AND MD5(o.ApplicationID) = '" . $token . "' ";
        }
        if ($ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationID . "' ";
        }
        if (!$token && !$ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationNo . "' AND s.BirthCertNo = '" . strtoupper($StudentBirthCertNo) . "' ";
        }
        if ($SchoolYearID) {
            $cond .= " AND o.ApplyYear = '" . $SchoolYearID . "' ";
        }
        $sql = "SELECT o.ApplicationID, s.ChineseName, s.EnglishName, p.RecordStatus, p.Amount, p.OutTradeNo, p.TradeNo, a.Status, p.DateModified
				FROM ADMISSION_STU_INFO as s
				JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID
				JOIN ADMISSION_APPLICATION_STATUS as a ON a.ApplicationID = o.ApplicationID
				LEFT JOIN ADMISSION_ALIPAY_TRANSACTION as p ON o.ApplicationID = p.ApplicationID
				WHERE 1 " . $cond . " ORDER BY o.ApplicationID, p.DateModified";

        $result = $this->returnArray($sql);

        if (!$result[0]['ApplicationID']) {
            return 0;
        }

        // if(!$result['Date']){
        // return 'NotAssigned';
        // }

        return $result;
    }

    function decodeMD5ApplicationID($ApplicationID)
    {
        if ($ApplicationID) {
            $sql = "SELECT ApplicationID
				FROM ADMISSION_OTHERS_INFO
				WHERE MD5(ApplicationID) = '" . $ApplicationID . "'";
            $result = current($this->returnArray($sql));
            return $result['ApplicationID'];
        } else {
            return false;
        }
    }

    function getExportInterviewResultHeaderData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $selectStatus = '', $selectInterviewStatus, $selectInterviewRound = '', $selectFormQuestionID = '', $selectFormAnswerID = '', $selectInterviewDate = '')
    {
        global $admission_cfg, $Lang, $kis_lang, $libkis_admission, $sys_custom;
        // debug_pr($recordID);
        // $status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
        // $studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
        // $parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
        $classLevel = $libkis_admission->getClassLevel();
        // $otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
        // $oldSchoolInfo = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
        // $siblingsInfo = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
        // $allCustInfo = $this->getAllApplicationCustInfo($status['applicationID']);
        //
        //
        // $student_name_en = explode(',',$studentInfo['student_name_en']);
        // $student_name_b5 = explode(',',$studentInfo['student_name_b5']);
        //
        // $dataArray = array();

        // ####### Student Info START ########
        /*
         * //$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
         * //$dataArray[] = $studentInfo['applicationID'];
         * $classLevel = $this->getClassLevel();
         * $dataArray['studentInfo'][] = $studentInfo['applicationID'];
         * $dataArray['studentInfo'][] = $classLevel[$otherInfo['classLevelID']];
         * $dataArray['studentInfo'][] = $student_name_en[0];
         * $dataArray['studentInfo'][] = $student_name_en[1];
         * $dataArray['studentInfo'][] = $student_name_b5[0];
         * $dataArray['studentInfo'][] = $student_name_b5[1];
         * $dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
         * $dataArray['studentInfo'][] = $studentInfo['county'];
         * $dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
         * $dataArray['studentInfo'][] = $studentInfo['placeofbirth'];
         * if($studentInfo['birthcerttype'] == $admission_cfg['BirthCertType']['hk']){
         * $dataArray['studentInfo'][] = $Lang['Admission']['BirthCertType']['hk2'];
         * }else if($studentInfo['birthcerttype'] == $admission_cfg['BirthCertType']['others']){
         * $dataArray['studentInfo'][] = $studentInfo['birthcerttypeother'];
         * }else{
         * $dataArray['studentInfo'][] = '';
         * }
         * $dataArray['studentInfo'][] = $studentInfo['birthcertno'];
         * $dataArray['studentInfo'][] = $studentInfo['homeaddress'];
         * $dataArray['studentInfo'][] = $studentInfo['contactaddress'];
         * $dataArray['studentInfo'][] = $studentInfo['homephoneno'];
         * $dataArray['studentInfo'][] = $studentInfo['religionOther'];
         * $dataArray['studentInfo'][] = $studentInfo['email'];
         * $dataArray['studentInfo'][] = ($studentInfo['istwinsapplied'])?'Y':'N';
         * $dataArray['studentInfo'][] = $oldSchoolInfo[0]['OthersPrevSchName'];
         * $dataArray['studentInfo'][] = $oldSchoolInfo[0]['OthersPrevSchClass'];
         * $dataArray['studentInfo'][] = $allCustInfo['Merit_Class'][0]['Value'];
         * $dataArray['studentInfo'][] = $allCustInfo['Merit_Form'][0]['Value'];
         * ## Talent START ##
         * $talentsStr = '';
         * foreach((array)$allCustInfo['Talents'] as $talent){
         * $talentsStr .= $talent['Value'] . "\n";
         * }
         * $dataArray['studentInfo'][] = trim($talentsStr);
         * ## Talent END ##
         *
         * ## Achievement START ##
         * $achievementStr = '';
         * foreach((array)$allCustInfo['Achievement'] as $achievement){
         * $achievementStr .= $achievement['Value'] . "\n";
         * }
         * $dataArray['studentInfo'][] = trim($achievementStr);
         * ## Achievement END ##
         *
         * ## Mother Tongue START ##
         * if($studentInfo['language'] == 1){
         * $dataArray['studentInfo'][] = $Lang['Admission']['RMKG']['cantonese'];
         * }else if($studentInfo['language'] == 1){
         * $dataArray['studentInfo'][] = $Lang['Admission']['RMKG']['eng'];
         * }else if($studentInfo['language'] == 1){
         * $dataArray['studentInfo'][] = $Lang['Admission']['RMKG']['pth'];
         * }else{
         * $dataArray['studentInfo'][] = '';
         * }
         * ## Mother Tongue END ##
         *
         * ## Fluency START ##
         * foreach((array)$allCustInfo['Fluency'] as $fluency){
         * ${$fluency['Value']} = $fluency['Position'];
         * }
         * $dataArray['studentInfo'][] = ${$admission_cfg['LanguageType'][1]};
         * $dataArray['studentInfo'][] = ${$admission_cfg['LanguageType'][2]};
         * $dataArray['studentInfo'][] = ${$admission_cfg['LanguageType'][3]};
         * ## Fluency END ##
         * ######## Student Info END ########
         *
         *
         * ######## Parent Info START ########
         * for($i=0;$i<count($parentInfo);$i++){
         * if($parentInfo[$i]['type'] == 'F'){
         * $dataArray['parentInfoF'][] = $parentInfo[$i]['parent_name_en'];
         * $dataArray['parentInfoF'][] = $parentInfo[$i]['parent_name_b5'];
         * $dataArray['parentInfoF'][] = $parentInfo[$i]['levelofeducation'];
         * $dataArray['parentInfoF'][] = $parentInfo[$i]['occupation'];
         * $dataArray['parentInfoF'][] = $parentInfo[$i]['companyaddress'];
         * $dataArray['parentInfoF'][] = $parentInfo[$i]['mobile'];
         * }
         * else if($parentInfo[$i]['type'] == 'M'){
         * $dataArray['parentInfoM'][] = $parentInfo[$i]['parent_name_en'];
         * $dataArray['parentInfoM'][] = $parentInfo[$i]['parent_name_b5'];
         * $dataArray['parentInfoM'][] = $parentInfo[$i]['levelofeducation'];
         * $dataArray['parentInfoM'][] = $parentInfo[$i]['occupation'];
         * $dataArray['parentInfoM'][] = $parentInfo[$i]['companyaddress'];
         * $dataArray['parentInfoM'][] = $parentInfo[$i]['mobile'];
         * }
         * else if($parentInfo[$i]['type'] == 'G'){
         * $dataArray['parentInfoG'][] = $parentInfo[$i]['parent_name_b5'];
         * $dataArray['parentInfoG'][] = $parentInfo[$i]['mobile'];
         * $dataArray['parentInfoG'][] = $parentInfo[$i]['relationship'];
         * }
         * }
         *
         * if(count($dataArray['parentInfoF']) == 0){
         * $dataArray['parentInfoF'] = array('','','','','','');
         * }
         * if(count($dataArray['parentInfoM']) == 0){
         * $dataArray['parentInfoM'] = array('','','','','','');
         * }
         * if(count($dataArray['parentInfoG']) == 0){
         * $dataArray['parentInfoG'] = array('','','');
         * }
         * ######## Parent Info END ########
         *
         *
         * ######## Referee Info START ########
         * $dataArray['referee'][] = $allCustInfo['Referee_Name'][0]['Value'];
         * foreach((array)$allCustInfo['Referee_Type'] as $type){
         * if($type['Value'] == $admission_cfg['RefereeType'][1]){
         * $hasCommittee = true;
         * }else if($type['Value'] == $admission_cfg['RefereeType'][2]){
         * $hasStaff = true;
         * }else if($type['Value'] == $admission_cfg['RefereeType'][3]){
         * $hasOther = true;
         * $otherType = $allCustInfo['Referee_Type_Other'][0]['Value'];
         * }
         * }
         * if($allCustInfo['Referee_Name'][0]['Value'] != ''){
         * $dataArray['referee'][] = ($hasCommittee)?'Y':'N';
         * $dataArray['referee'][] = ($hasStaff)?'Y':'N';
         * $dataArray['referee'][] = ($hasOther)?$otherType:'';
         * }else{
         * $dataArray['referee'][] = '';
         * $dataArray['referee'][] = '';
         * $dataArray['referee'][] = '';
         * }
         * ######## Referee Info END ########
         *
         * //for other info
         * for($i=0;$i<3;$i++){
         * $dataArray['siblingsInfo'][] = $siblingsInfo[$i]['OthersRelativeStudiedName'];
         * $dataArray['siblingsInfo'][] = $siblingsInfo[$i]['OthersRelativeClassPosition'];
         * $dataArray['siblingsInfo'][] = $siblingsInfo[$i]['OthersRelativeRelationship'];
         * }
         * //for official use
         * $dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
         * $dataArray['officialUse'][] = $status['receiptID'];
         * $dataArray['officialUse'][] = $status['receiptdate'];
         * $dataArray['officialUse'][] = $status['handler'];
         * $dataArray['officialUse'][] = $status['interviewdate'];
         * $dataArray['officialUse'][] = $status['interviewlocation'];
         * $dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
         * $dataArray['officialUse'][] = $status['remark'];
         *
         * $ExportArr = array_merge(
         * $dataArray['studentInfo'],
         * $dataArray['parentInfoM'],
         * $dataArray['parentInfoF'],
         * $dataArray['parentInfoG'],
         * $dataArray['referee'],
         * $dataArray['siblingsInfo'],
         * $dataArray['officialUse']
         * );
         */

        /*
         * if($q[2]=='search'){
         * $NavArr[] = array('applicantslist/',$selectedSchoolYear['academic_year_name_'.$intranet_session_language]);
         * $NavArr[] = array('',$kis_lang['search_result']);
         * }else{
         * $kis_data['classLevelID'] = $classLevelId = (in_array($q[2],$classLevelIdAry))?$q[2]:$classLevelIdAry[0];
         * $status = in_array($selectStatus,$admission_cfg['Status'])? $selectStatus:'';
         * $kis_data['interviewStatusSelection'] = ($status==$admission_cfg['Status']['waitingforinterview'])?$libkis_admission->getInterviewStatusSelection('selectInterviewStatus',$selectInterviewStatus):'';
         * $kis_data['classLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId);
         * $kis_data['classChangeLevelSelection'] = $libkis_admission->getClassLevelSelection($classLevelId,'selectChangeClassLevelID');
         * $kis_data['applicationStatus'] = $libkis_admission->getStatusSelection($status);
         * $selectedClassLevel = $libkis_admission->classLevelAry[$kis_data['classLevelID']];
         * }
         */

        $data = array(
            'classLevelID' => $classLevelID,
            'applicationID' => $recordID,
            'status' => $selectStatus,
            'interviewStatus' => $selectInterviewStatus,
            'keyword' => $keyword,
            'page' => $page,
            'amount' => $amount,
            'order' => $order,
            'sortby' => $sortby,
            'round' => $selectInterviewRound,
            'formQuestionID' => $selectFormQuestionID,
            'formAnswer' => $selectFormAnswerID,
	        'interviewDate' => $selectInterviewDate,
        );
        list ($total, $applicationDetails) = $libkis_admission->getInterviewFormReport($schoolYearID, $data);
        $count = count($applicationDetails);
        // debug_pr($applicationDetails);
        $kis_data['interviewQuestionAnswer'] = $libkis_admission->getInterviewFormQuestionAnswerAry();
        // debug_pr($kis_data['interviewQuestionAnswer']);

        // ####### Export Header START ########
        $exportColumn[0][] = '';
        $exportColumn[0][] = '';
        $exportColumn[0][] = '';
        $exportColumn[0][] = '';

        // for($i=0; $i<$kis_data['interviewQuestionAnswer']['maxNumOfInputBy']; $i++){
        // $exportColumn[0][] = $kis_lang['teacher'].' '.($i+1);
        // $exportColumn[0][] = '';
        // $exportColumn[0][] = '';
        // }
        // $exportColumn[0][] = '';

        $exportColumn[1][] = $kis_lang['Admission']['applyLevel'];
        $exportColumn[1][] = $kis_lang['applicationno'];
        $exportColumn[1][] = $kis_lang['studentname'];
        $exportColumn[1][] = $kis_lang['marks'];

        // for($i=0; $i<$kis_data['interviewQuestionAnswer']['maxNumOfInputBy']; $i++){
        // $exportColumn[1][] = 'Q1';
        // $exportColumn[1][] = 'Q2';
        // $exportColumn[1][] = 'Q3';
        // }

        // $exportColumn[1][] = $kis_lang['status'];
        // ####### Export Header END ########

        $kis_data['applicationDetails'] = array();
        $unsetapplication = array();
        for ($i = 0; $i < $count; $i++) {

            $_applicationId = (is_numeric($applicationDetails[$i]['application_id']) ? $applicationDetails[$i]['application_id'] . ' ' : $applicationDetails[$i]['application_id']);

            $kis_data['applicationDetails'][$_applicationId]['InterviewDate'] = $applicationDetails[$i]['InterviewDate'];
            $kis_data['applicationDetails'][$_applicationId]['student_name'] = $applicationDetails[$i]['student_name'];
            $kis_data['applicationDetails'][$_applicationId]['parent_name'][] = $applicationDetails[$i]['parent_name'];
            $kis_data['applicationDetails'][$_applicationId]['parent_phone'][] = $applicationDetails[$i]['parent_phone'];
            $kis_data['applicationDetails'][$_applicationId]['application_status'] = $applicationDetails[$i]['application_status'];
            $kis_data['applicationDetails'][$_applicationId]['record_id'] = $applicationDetails[$i]['record_id'];
            $kis_data['applicationDetails'][$_applicationId]['Marks'] = $applicationDetails[$i]['Marks'];
            $kis_data['applicationDetails'][$_applicationId]['QuestionAnswers'] = $kis_data['interviewQuestionAnswer'][$applicationDetails[$i]['record_id']][$applicationDetails[$i]['InterviewSettingID']];
            // if($applicationDetails[$i]['FormID'] && !$formIdForSelectionQuestion){
            // $formIdForSelectionQuestion = $applicationDetails[$i]['FormID'];
            // }
            // debug_pr($kis_data['applicationDetails'][$_applicationId]['QuestionAnswers']);
            if ($selectFormQuestionID != '' && $selectFormAnswerID != '') {
                $hasQuestion = false;
                if (!$kis_data['applicationDetails'][$_applicationId]['QuestionAnswers']) {
                    unset($kis_data['applicationDetails'][$_applicationId]);
                    if (!in_array($_applicationId, $unsetapplication)) {
                        $total--;
                    }
                    $unsetapplication[] = $_applicationId;
                } else {
                    foreach ($kis_data['applicationDetails'][$_applicationId]['QuestionAnswers'] as $aQestion) {
                        $j = 0;
                        foreach ($aQestion['QuestionsID'] as $_aQestion) {
                            if ($_aQestion == $selectFormQuestionID && $aQestion['Answers'][$j] == $selectFormAnswerID) {
                                $hasQuestion = true;
                            }
                            $j++;
                        }
                    }
                    if (!$hasQuestion) {
                        unset($kis_data['applicationDetails'][$_applicationId]);
                        if (!in_array($_applicationId, $unsetapplication)) {
                            $total--;
                        }
                        $unsetapplication[] = $_applicationId;
                    }
                }
            }
            if(!empty($selectInterviewDate) && $selectInterviewDate != $kis_data['applicationDetails'][$_applicationId]['InterviewDate']){
	            unset($kis_data['applicationDetails'][$_applicationId]);
	            if (!in_array($_applicationId, $unsetapplication)) {
		            $total--;
	            }
	            $unsetapplication[] = $_applicationId;
            }
        }
        $k = 0;


        $numOfQuestion = 0;
        $exportColumnAssigned = array();
        foreach ($kis_data['applicationDetails'] as $_applicationId => $_applicationDetailsAry) {
            // for question

            for ($i = 0; $i < $kis_data['interviewQuestionAnswer']['maxNumOfInputBy']; $i++) {
                // debug_pr($_applicationDetailsAry['QuestionAnswers']);
                if ($_applicationDetailsAry['QuestionAnswers']) {
                    $pos = 0;
                    foreach ($_applicationDetailsAry['QuestionAnswers'] as $key => $value) {
                        if ($i == $pos) {
                            for ($j = 0; $j < sizeof($_applicationDetailsAry['QuestionAnswers'][$key]['Questions']); $j++) {
                                if (!in_array(($_applicationDetailsAry['QuestionAnswers'][$key]['QuestionsID'][$j] . $i), $exportColumnAssigned)) {

                                    $exportColumnAssigned[] = $_applicationDetailsAry['QuestionAnswers'][$key]['QuestionsID'][$j] . $i;
                                    if ($j == 0) {
                                        $exportColumn[0][] = $kis_lang['teacher'] . ' ' . ($i + 1);
                                        $exportColumn[1][] = $kis_lang['marker'];
                                        $exportColumn[0][] = '';
                                        $numOfQuestion++;
                                        $exportColumnAssigned[] = '<!--Markers-->';
                                    } else {
                                        $exportColumn[0][] = '';
                                    }
                                    $exportColumn[1][] = $_applicationDetailsAry['QuestionAnswers'][$key]['Questions'][$j];
                                    $numOfQuestion++;
                                }
                            }
                            // $exportColumnAssigned =true;
                        }
                        $pos++;
                    }
                }
            }
        }
        foreach ($kis_data['applicationDetails'] as $_applicationId => $_applicationDetailsAry) {
            $_recordId = $_applicationDetailsAry['record_id'];
            $_interviewDate = $_applicationDetailsAry['InterviewDate'];
            $_status = $_applicationDetailsAry['application_status'];
            $_studentName = $_applicationDetailsAry['student_name'];
            $_parentName = implode('<br/>', array_filter($_applicationDetailsAry['parent_name']));
            $_parentPhone = implode('<br/>', array_filter($_applicationDetailsAry['parent_phone']));

            $tr_css = '';
            switch ($_status) {

                case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'waitingforinterview':
                    $tr_css = ' class="absent"';
                    break;
                case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'interviewed':
                    $tr_css = ' class="waiting"';
                    break;
                case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'admitted':
                    $tr_css = ' class="done"';
                    break;
                case $sys_custom['KIS_Admission']['MGF']['Settings'] && 'notadmitted':
                    $tr_css = ' class="draft"';
                    break;

                case 'pending':
                    $tr_css = ' class="absent"';
                    break;
                // case 'paymentsettled':$tr_css = '';break;
                case 'waitingforinterview':
                    $tr_css = ' class="waiting"';
                    break;
                case 'confirmed':
                    $tr_css = ' class="done"';
                    break;
                case 'cancelled':
                    $tr_css = ' class="draft"';
                    break;
            }

            $ExportArr[$k][] = $classLevel[$classLevelID];
            if($sys_custom['KIS_Admission']['CREATIVE']['Settings']){
                $ExportArr[$k][] = $_interviewDate;
            }
            $ExportArr[$k][] = $_applicationId;
            $ExportArr[$k][] = $_studentName;
            $ExportArr[$k][] = $_applicationDetailsAry['Marks'];

            // for answer
            $noAnswer = false;
            $numOfAssignAns = 0;
            // debug_pr($exportColumnAssigned);

            for ($i = 0; $i < $kis_data['interviewQuestionAnswer']['maxNumOfInputBy']; $i++) {
                if ($_applicationDetailsAry['QuestionAnswers']) {
                    foreach ($_applicationDetailsAry['QuestionAnswers'] as $key => $value) {
                        for ($j = 0; $j < sizeof($_applicationDetailsAry['QuestionAnswers'][$key]['Questions']); $j++) {
                            for ($m = 0; $m < sizeof($exportColumnAssigned); $m++) {
                                if (($_applicationDetailsAry['QuestionAnswers'][$key]['QuestionsID'][$j] . $i) != $exportColumnAssigned[$m]) {
                                    $ExportArr[$k][] = '';
                                    $numOfAssignAns++;
                                } else {
                                    break 4;
                                }
                            }
                        }
                    }
                }
            }

            for ($i = 0; $i < $kis_data['interviewQuestionAnswer']['maxNumOfInputBy']; $i++) {
                // $exportColumnAssigned = false;
                // debug_pr($_applicationDetailsAry['QuestionAnswers']);
                if ($_applicationDetailsAry['QuestionAnswers']) {
                    $pos = 0;
                    foreach ($_applicationDetailsAry['QuestionAnswers'] as $key => $value) {
                        // for($m=0; $m<sizeof($exportColumnAssigned); $m++){
                        // if(($_applicationDetailsAry['QuestionAnswers'][$key]['QuestionsID'][0].$i) != $exportColumnAssigned[$m]){
                        // $ExportArr[$k][] = '';
                        // }
                        // else{
                        // break;
                        // }
                        // }
                        if ($i == $pos) {
                            $ExportArr[$k][] = $_applicationDetailsAry['QuestionAnswers'][$key]['InputByName'];
                            $numOfAssignAns++;
                            for ($j = 0; $j < sizeof($_applicationDetailsAry['QuestionAnswers'][$key]['Questions']); $j++) {
                                $ExportArr[$k][] = $_applicationDetailsAry['QuestionAnswers'][$key]['Answers'][$j];
                                $numOfAssignAns++;
                            }
                            // $exportColumnAssigned =true;
                        }
                        $pos++;
                    }
                } else {
                    $noAnswer = true;
                }
            }
            // if($noAnswer){
            for ($m = 0; $m < ($numOfQuestion - $numOfAssignAns); $m++) {
                $ExportArr[$k][] = '';
            }
            // }

            $ExportArr[$k][] = $kis_lang['Admission']['Status'][$_status];
            $k++;
        }
        $exportColumn[1][] = $kis_lang['status'];
        // debug_pr($ExportArr);
        return array(
            $exportColumn,
            $ExportArr
        );
    }

    function isInternalUse($token = '')
    {
        if ($_SESSION["platform"] == "KIS" && $_SESSION["UserID"] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAdmission"] && $token == '') {
            return true;
        } else {
            return false;
        }
    }

    function getTokenByApplicationNumber($ApplicationID = '')
    {
        $sql = 'select Token from ADMISSION_OTHERS_INFO where ApplicationID = "' . $ApplicationID . '"';
        $result = current($this->returnArray($sql));
        return $result['Token'];
    }

    function getApplicationResult($StudentDateOfBirth, $StudentBirthCertNo, $SchoolYearID = '', $ApplicationID = '')
    {
        global $admission_cfg;

        if ($SchoolYearID) {
            $cond = " AND o.ApplyYear = '" . $SchoolYearID . "' ";
        }
        if ($ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationID . "' ";
        }

        $sql = "SELECT o.ApplicationID, o.ApplyYear
				FROM ADMISSION_STU_INFO as s
				JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID
				WHERE s.DOB = '" . $StudentDateOfBirth . "' AND BINARY s.BirthCertNo = '" . $StudentBirthCertNo . "' " . $cond . " ORDER BY o.ApplicationID desc";

        $result = current($this->returnArray($sql));

        if (!$result['ApplicationID']) {
            return 0;
        }

        $result2 = $this->getPaymentResult('', '', '', '', $result['ApplicationID']);
        if ($result2) {
            foreach ($result2 as $aResult) {
                if ($aResult['Status'] == $admission_cfg['Status']['cancelled']) {
                    return 0;
                }
            }
        }

        // if(!$result['Date']){
        // return 'NotAssigned';
        // }

        return $result;
    }

    function getInterviewListAry($recordID = '', $date = '', $startTime = '', $endTime = '', $keyword = '', $order = '', $sortby = '', $round = 1)
    {
        global $admission_cfg;

        // $schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

        if ($recordID != '') {
            $cond = " AND i.RecordID IN ('" . implode("','", (array)($recordID)) . "') ";
        }
        if ($date != '') {
            $cond .= ' AND i.Date >= \'' . $date . '\' ';
        }
        if ($startTime != '') {
            $cond .= ' AND i.StartTime >= \'' . $startTime . '\' ';
        }
        if ($endTime != '') {
            $cond .= ' AND i.EndTime <= \'' . $endTime . '\' ';
        }
        if ($keyword != '') {
            $search_cond = ' AND (i.Date LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.StartTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
							OR i.EndTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\')';
        }
        $sort = $sortby ? "$sortby $order" : "application_id";

        // if(!empty($keyword)){
        // $cond .= "
        // AND (
        // stu.EnglishName LIKE '%".$keyword."%'
        // OR stu.ChineseName LIKE '%".$keyword."%'
        // OR stu.ApplicationID LIKE '%".$keyword."%'
        // OR pg.EnglishName LIKE '%".$keyword."%'
        // OR pg.ChineseName LIKE '%".$keyword."%'
        // )
        // ";
        // }

        $from_table = "
			FROM
				ADMISSION_INTERVIEW_SETTING AS i
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID" . ($round > 1 ? $round : '') . "
			INNER JOIN
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			WHERE
				1
			" . $cond . "
			order by " . $sort . "
    	";
        $sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			" . getNameFieldByLang2("stu.") . " AS student_name,
     			" . getNameFieldByLang2("pg.") . " AS parent_name,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE
     	";
        FOREACH ($admission_cfg['Status'] as $_key => $_status) { // e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status	" . $from_table . " ";
        // debug_r($sql);
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function getClassLevelSelection($classLevelID = '', $name = 'selectClassLevelID', $firstTitle = false, $firstTitleText = '', $isMultiSelect = false, $otherTag = '')
    {
        global $kis_lang, $setting_path_ip_rel;

        if (!is_array($classLevelID)) {
            $classLevelID = (array)$classLevelID;
        }

        $x = '<select name="' . $name . '" id="' . $name . '" ' . ($isMultiSelect ? 'multiple' : '') . ' ' . $otherTag . ' >';
        if ($setting_path_ip_rel == 'ylsyk.eclass.hk') {
            $kis_lang['select'] = '選擇 Select';
        }
        $x .= ($firstTitle) ? '<option value=""' . (!$classLevelID ? ' selected="selected"' : '') . '>- ' . ($firstTitleText != '' ? $firstTitleText : $kis_lang['select']) . ' -</option>' : '';
        foreach ($this->classLevelAry as $_classLevelId => $_classLevelName) {
            $x .= '<option value="' . $_classLevelId . '"' . (in_array($_classLevelId, $classLevelID) ? ' selected="selected"' : '') . '>';
            $x .= $_classLevelName;
            $x .= '</option>';
        }
        $x .= '</select>';
        return $x;
    }

    public function getPaymentReportDetails($data = array())
    {
        global $admission_cfg;
        extract($data);

        $sort = $sortby ? "$sortby $order" : "date_input";
        $limit = $page ? " LIMIT " . (($page - 1) * $amount) . ", $amount" : "";

        $cond .= !empty($startDate) ? " AND o.DateInput>='" . $startDate . "' " : "";
        $cond .= !empty($endDate) ? " AND o.DateInput<='" . $endDate . "' " : "";

        if (!empty($keyword)) {
            $cond .= "
				AND (
					stu.EnglishName LIKE '%" . $keyword . "%'
					OR stu.ChineseName LIKE '%" . $keyword . "%'
					OR stu.ApplicationID LIKE '%" . $keyword . "%'
					OR pg.EnglishName LIKE '%" . $keyword . "%'
					OR pg.ChineseName LIKE '%" . $keyword . "%'
					OR pg.Mobile LIKE '%" . $keyword . "%'
					OR stu.BirthCertNo LIKE '%" . $keyword . "%'
				)
			";
        }

        $from_table = "
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO stu ON a.ApplicationID = stu.ApplicationID
			INNER JOIN
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				YEAR y ON y.YearID = o.ApplyLevel
			LEFT JOIN
				ADMISSION_PAYMENT_INFO p ON o.ApplicationID = p.ApplicationID AND payment_status = 'Completed'
			WHERE
				a.Status != '" . $admission_cfg['Status']['cancelled'] . "'
			" . $cond . "

    	";
        // a.Status >= '".$admission_cfg['Status']['paymentsettled']."'

        $sql = "SELECT DISTINCT o.ApplicationID " . $from_table;
        $applicationIDAry = $this->returnVector($sql);

        $sql = "
			SELECT
				o.DateInput AS date_input,
     			stu.ApplicationID AS application_id,
     			" . getNameFieldByLang2("stu.") . " AS student_name,
				y.YearName AS year_name,
				IF(p.mc_gross!='', p.mc_gross, '500.00') AS payment_amount,
				p.txn_id AS txn_id,
				a.Remark AS remarks,
     			" . getNameFieldByLang2("pg.") . " AS parent_name,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				o.ApplyYear AS apply_year,
				CASE
     	";
        foreach ($admission_cfg['Status'] as $_key => $_status) { // e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN a.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE a.Status END application_status,
				p.payment_status AS payment_status
					" . $from_table . " AND o.ApplicationID IN ('" . implode("','", $applicationIDAry) . "') ORDER BY $sort ";
        // debug_pr($sql);
        $applicationAry = $this->returnArray($sql);

        return array(
            count($applicationIDAry),
            $applicationAry
        );
    }

    function getKnowUsBy($appID){
        $sql = "SELECT KnowUsBy, KnowUsByOther FROM ADMISSION_OTHERS_INFO WHERE ApplicationID = '".$appID."'";
        return $this->returnResultSet($sql);
    }
    
    public function sendMandrillMail($email_subject, $email_message, $from, $to_emails, $cc_emails='', $bcc_emails='')
    {
        global $admission_cfg, $intranet_root;
        include_once($intranet_root . "/includes/mandrill/src/Mandrill.php");
        //debug_pr($from);die();
        $mandrill = new Mandrill($admission_cfg['MandrillApikey']);
        $message = new stdClass();
		$message->html = $email_message;
		$message->subject = $email_subject;
		//$message->from_email = "ts@broadlearning.com";
		$sPattern = '/([\w\s\'\"]+[\s]+)?(<)?(([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4}))?(>)?/';
		preg_match($sPattern,$from,$aMatch);
		$message->from_email = $aMatch[3];
		$message->from_name  = $aMatch[1];

		$email_to = array();
		for($i=0;$i<count($to_emails);$i++){
			$email_to[] = array("email" => $to_emails[$i]);
		}
		
		$message->to = $email_to;
		$message->track_opens = true;
		
		$response = $mandrill->messages->send($message);
		if($response[0]["status"] == "sent"){
			return true;
		}
		else{
			return false;
		}
    }
} // End Class

?>
