<?php
# modifying by: 
/**
 * Change Log:
 * 2018-01-24 Pun
 *  - File Created
 */

include_once("{$intranet_root}/includes/admission/libadmission_ui_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionUiCustBase.class.php");

class admission_ui_cust extends \AdmissionSystem\AdmissionUiCustBase{
    
	public function __construct(){
        global $plugin;
        
        if($plugin['eAdmission_devMode']){
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);ini_set('display_errors', 1);
        }
        
	    parent::__construct();
		$this->init();
	}
	
	private function init(){
	    /**** FILTER_ADMISSION_FORM_WIZARD_STEPS START ****/
		$this->addFilter(self::FILTER_ADMISSION_FORM_WIZARD_STEPS, (function($defaultArr){
		    global $Lang;
		    
		    $defaultArr[1]['title'] = $Lang['Admission']['PICLC']['personalInfo'];
		    $defaultArr[2]['title'] = $Lang['Admission']['PICLC']['docsUpload'];
		    
		    return $defaultArr;
		}));
	    /**** FILTER_ADMISSION_FORM_WIZARD_STEPS END ****/

		/**** FILTER_ADMISSION_FORM_STEP_PAGE_HTML START **** /
		$this->addFilter(self::FILTER_ADMISSION_FORM_STEP_PAGE_HTML, (function($defaultArr){
		    $additionalPage = array();
		    $additionalPage[] = array(
                'id' => 'step_payment',
                'func' => 'getPaymentPage',
                'args' => array(),
		    );
		    
		    array_splice($defaultArr, 4, 0, $additionalPage);
		    
		    return $defaultArr;
		}));
		/**** FILTER_ADMISSION_FORM_STEP_PAGE_HTML START ****/
		
	    /**** FILTER_ADMISSION_FORM_INSTRUCTION_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    global $LangB5, $kis_lang_b5;
		    $formHtml = str_replace($LangB5['Admission']['msg']['defaultpreviewpagemessage'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['msg']['defaultviewpagemessage'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['msg']['defaultupdatepagemessage'], '', $formHtml);
		    $formHtml = str_replace($kis_lang_b5['remarks'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['onlineApplication'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['IsFull'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['msg']['noclasslevelapply'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['warning'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['msg']['noclasslevelapply'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['Language'], '', $formHtml);
		    $formHtml = str_replace($LangB5['Admission']['level'], '', $formHtml);
		    $formHtml = str_replace('class="instructionLang"', 'class="instructionLang" style="display:none;"', $formHtml);
		    $formHtml = str_replace('class="instructionSuggestBrowser"', 'class="instructionSuggestBrowser" style="display:none;"', $formHtml);
		    return $formHtml;
		}));
	    /**** FILTER_ADMISSION_FORM_INSTRUCTION_HTML END ****/
	    
		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getParentForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getStudentAcademicBackgroundForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getFamilyLanguageProfileForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getOtherForm($IsConfirm, $IsUpdate);
		}));
		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_HTML END ****/
		
		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate){
		    return $formHtml . $this->getStudentForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate){
		    return $formHtml . $this->getParentForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate){
		    return $formHtml . $this->getStudentAcademicBackgroundForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate){
		    return $formHtml . $this->getFamilyLanguageProfileForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate){
		    return $formHtml . $this->getOtherForm($IsConfirm=1, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML, (function($formHtml, $IsUpdate){
		    return $formHtml . $this->getDocUploadForm($IsConfirm=1, $IsUpdate);
		}));
		/**** FILTER_ADMISSION_FORM_APPLICANT_FORM_CONFIRM_HTML END ****/
		
		/**** FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML START ****/
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getStudentForm($IsConfirm, $BirthCertNo, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getParentForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getStudentAcademicBackgroundForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getFamilyLanguageProfileForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getOtherForm($IsConfirm, $IsUpdate);
		}));
		$this->addFilter(self::FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML, (function($formHtml, $IsConfirm, $BirthCertNo, $IsUpdate){
		    return $formHtml . $this->getDocUploadForm($IsConfirm, $IsUpdate);
		}));
		/**** FILTER_ADMISSION_UPDATE_FORM_APPLICANT_FORM_HTML END ****/
		
		/**** FILTER_ADMISSION_TOP_MENU_TAB START ****/
		$this->addFilter(self::FILTER_ADMISSION_TOP_MENU_TAB, (function($defaultTab){
		    $newTab = array('piclc_academic_background', 'piclc_family_language');
		    array_splice($defaultTab, 2, 0, $newTab);
		    return $defaultTab;
		}));
		/**** FILTER_ADMISSION_TOP_MENU_TAB END ****/
		
	}
	
	/**
     * Admission form - First page Instruction to preview/create/view/update Applicants
     */
    public function getIndexContent($Instruction, $ClassLevel = "", $IsUpdate = false)
    {
        global $LangB5, $LangEn, $kis_lang_b5, $kis_lang_en, $lac, $sys_custom;
        
        /**
         * ** Instruction START ***
         */
        if (! $Instruction) {
            $Instruction = "{$LangB5['Admission']['msg']['defaultfirstpagemessage']}<br/>{$LangEn['Admission']['msg']['defaultfirstpagemessage']}";
        }
        /**
         * ** Instruction END ***
         */
        
        /**
         * ** Preview START ***
         */
        $previewStyle = 'display:none;';
        if (! $IsUpdate && ($_SESSION["platform"] != "KIS" || ! $_SESSION["UserID"]) && $sys_custom['KIS_Admission']['PreviewFormMode'] && $lac->IsPreviewPeriod() && ! $_GET['token']) {
            $previewStyle = '';
        }
        /**
         * ** Preview END ***
         */
        
        /**
         * ** View/Edit START ***
         */
        if ($IsUpdate) {
            $viewStyle = 'display:none;';
            $editStyle = '';
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && $lac->IsAfterUpdatePeriod()) {
                $viewStyle = '';
                $editStyle = 'display:none;';
            }
        } else {
            $viewStyle = $editStyle = 'display:none;';
        }
        /**
         * ** View/Edit END ***
         */
        
        /**
         * ** Remark START ***
         */
        $remarkStyle = 'display:none;';
        if ($lac->isInternalUse($_REQUEST['token'])) {
            $remarkStyle = '';
        }
        /**
         * ** Remark END ***
         */
        
        /**
         * ** Choose class START ***
         */
        $mainHTML = '';
        if ($IsUpdate) {
            if ($sys_custom['KIS_Admission']['ApplicantUpdateForm'] && ($lac->IsUpdatePeriod() || $lac->IsAfterUpdatePeriod())) {
                $mainHTML = $this->getApplicantCheckingForm();
            }
        } else {
            if ($lac->schoolYearID) {
                $mainHTML = $this->getChooseClassForm($ClassLevel);
            }
        }
        /**
         * ** Choose class END ***
         */
        
        $x = <<<HTML
            <div class="notice_paper">
                <h2 style="font-size:18px;color:red;{$previewStyle}">
                    <center>
                        {$LangB5['Admission']['msg']['defaultpreviewpagemessage']}
                        <br/>
                        {$LangEn['Admission']['msg']['defaultpreviewpagemessage']}
                    </center>
                </h2>
                
                <h2 style="font-size:18px;color:red;{$viewStyle}">
                    <center>
                        {$LangB5['Admission']['msg']['defaultviewpagemessage']}
                        <br/>
                        {$LangEn['Admission']['msg']['defaultviewpagemessage']}
                    </center>
                </h2>
                <h2 style="font-size:18px;color:red;{$editStyle}">
                    <center>
                        {$LangB5['Admission']['msg']['defaultupdatepagemessage']}
                        <br/>
                        {$LangEn['Admission']['msg']['defaultupdatepagemessage']}
                    </center>
                </h2>
    
                <h2 style="font-size:18px;color:red;{$remarkStyle}">
                    <center>
                        {$kis_lang_b5['remarks']}
                        <br/>
                        {$kis_lang_en['remarks']}
                    </center>
                </h2>
    
                <div class="notice_paper_top">
                    <div class="notice_paper_top_right">
                        <div class="notice_paper_top_bg">
                            <h1 class="notice_title"></h1>
                        </div>
                    </div>
                </div>
    
                <div class="notice_paper_content">
                    <div class="notice_paper_content_right">
                        <div class="notice_paper_content_bg">
                            <div class="notice_content">
                                <div class="admission_content">
                                    {$Instruction}
                                  </div>
                                  <p class="spacer"></p>
                              </div>

            <table style="font-size:16px;">
                <tr>
                    <td width="5">&nbsp;</td>
                    <td align="center" width="1%" rowspan="2"><input type="checkbox" name="Agree" id="Agree" value="1" style="transform: scale(2);  -webkit-transform: scale(2);"/></td>
                    <td width="5">&nbsp;</td>
                    <td><label for="Agree">{$LangEn['Admission']['AgreeTermsCondition']}.</label></td>
                </tr>
            </table>
            <p style="font-size:16px;">Please select the grade.</p>
                              {$mainHTML}
                          </div>
                      </div>
                  </div>
    
                  <div class="notice_paper_bottom">
                      <div class="notice_paper_bottom_right">
                          <div class="notice_paper_bottom_bg">
                          </div>
                      </div>
                  </div>
              </div>
HTML;
        
        $x = $this->applyFilter(self::FILTER_ADMISSION_FORM_INSTRUCTION_HTML, $html = $x, $Instruction, $ClassLevel, $IsUpdate);
        
        return $x;
    }
    
    /**
     * Admission form - second page Instruction for each class level
     */
    public function getInstructionContent($Instruction, $currentStep = 1)
    {
        global $Lang, $lac, $sys_custom;
        
        $x = $this->getWizardStepsUI($currentStep);
        
        /**
         * ** Instruction START ***
         */
        if (! $Instruction) {
            $Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage'];
        }
        $x .= $Instruction;
        /**
         * ** Instruction END ***
         */
        
        $x .= $agreeHTML;
        /**
         * ** Agree button END ***
         */
        
        /**
         * ** Step button START ***
         */
        $lastStepId = 'step_index';
        $currentStepId = 'step_instruction';
        $nextStepId = 'step_input_form';
        
        if ($lac->schoolYearID) {
            $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId, $nextBtnLang = $Lang['Admission']['BeginApplication']);
        }
        /**
         * ** Step button END ***
         */
        
        return $x;
    }
    
	/**
	 * Admission Form - create/edit form student part
	 */
	protected function getStudentForm($IsConfirm=0, $BirthCertNo = "", $IsUpdate=0){
		global $formData, $Lang, $lac, $admission_cfg;

		$allClassLevel = $lac->getClassLevel();
		$schoolArr = $lac->getSchoolBySchoolYearIdClassLevelId($this->schoolYearID,$_REQUEST['sus_status']);

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			
			if(count($application_details) > 0){
				$StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
			}
			
			## Apply Year START ##
			$classLevelID = $StudentInfo['classLevelID'];
			$classLevel = $allClassLevel[$classLevelID];
			## Apply Year END ##
			$schoolArr = $lac->getSchoolBySchoolYearIdClassLevelId($application_details['ApplyYear'],$classLevelID);
		}
		
		@ob_start();
		include(__DIR__ . "/template/admissionForm/studentForm.tmpl.php");
		$x = ob_get_clean();
		
		return $x;
	}
	
	/**
	 * Admission Form - create/edit form parent part
	 */
	protected function getParentForm($IsConfirm=0, $IsUpdate=0){
		global $formData, $Lang, $lac, $admission_cfg;

		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);

			$tmpParentInfoArr = $lac->getApplicationParentInfo($application_details['ApplyYear'],'',$_SESSION['KIS_ApplicationID']);
			$parentInfoArr = array();
			foreach($tmpParentInfoArr as $parent){
				foreach($parent as $para=>$info){
					$ParentInfo[$parent['type']][$para] = $info;
				}
			}
			#### Parent Info END ####
		}
		$parentTypeArr = array(1=>'F',2=>'M',3=>'G');
		
		@ob_start();
		include("template/admissionForm/parentForm.tmpl.php");
		$x = ob_get_clean();
			
		return $x;
	}
	
	/**
	 * Admission Form - create/edit form student academic background part
	 */
	protected function getStudentAcademicBackgroundForm($IsConfirm=0, $IsUpdate=0){
	    global $formData, $Lang, $lac, $admission_cfg;
	
	    $allClassLevel = $lac->getClassLevel();
	
	    $star = $IsConfirm?'':'<font style="color:red;">*</font>';
	
	    if($IsUpdate){
	        $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
	        	
	        $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
	        	
	        if(count($application_details) > 0){
	            $StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
	            
	            $StudentPrevSchoolInfo = $lac->getApplicationPrevSchoolInfo($schoolYearID=$application_details['ApplyYear'],$classLevelID='',$applicationID=$application_details['ApplicationID'],$recordID='');
	        }
	    }
	
	    @ob_start();
	    include(__DIR__ . "/template/admissionForm/studentAcademicBackgroundForm.tmpl.php");
	    $x = ob_get_clean();
	
	    return $x;
	}
	
	/**
	 * Admission Form - create/edit form family language profile part
	 */
	protected function getFamilyLanguageProfileForm($IsConfirm=0, $IsUpdate=0){
	    global $formData, $Lang, $lac, $admission_cfg;
	
	    $allClassLevel = $lac->getClassLevel();
	
	    $star = $IsConfirm?'':'<font style="color:red;">*</font>';
	
	    if($IsUpdate){
	        $application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
	        $allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
	        	
	        if(count($application_details) > 0){
	            $StudentInfo = current($lac->getApplicationStudentInfo($application_details['ApplyYear'],'',$application_details['ApplicationID']));
	        }
	    }
	    
	    @ob_start();
	    include(__DIR__ . "/template/admissionForm/familyLanguageProfile.tmpl.php");
	    $x = ob_get_clean();
	
	    return $x;
	}
	
	/**
	 * Admission Form - create/edit form other part
	 */
	protected function getOtherForm($IsConfirm=0, $IsUpdate=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang, $kis_lang_b5, $kis_lang_en;
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			$SiblingInfo = $lac->getApplicationSibling($schoolYearID=$application_details['ApplyYear'],$classLevelID='',$applicationID=$application_details['ApplicationID'],$recordID='');
		}
		
		@ob_start();
		include("template/admissionForm/otherForm.tmpl.php");
		$x = ob_get_clean();
		
		return $x;
	}
	
	
	/**
	 * Admission Form - create/edit form other part
	 */
	protected function getDocUploadForm($IsConfirm=0, $IsUpdate=0, $AcademicYearID = '', $ApplyFor=0){
	    global $Lang;
		global $tempFolderPath, $fileData, $admission_cfg, $lac, $sys_custom, $intranet_root;
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			
			if(count($application_details) > 0){
				$applicationAttachmentInfo = $lac->getApplicationAttachmentRecord($application_details['ApplyYear'],array('applicationID'=>$application_details['ApplicationID']));
				$ApplyFor = $application_details['ApplyLevel'];
			}
			## Photo START ##
			$viewFilePath = (is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']]['personal_photo']['attachment_link'][0]) && ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['StudentPersonalPhoto'])?' <a href="download_attachment.php?type=personal_photo'./*$admission_cfg['FilePath'].$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]*/'" target="_blank" >'.$Lang['Admission']['viewSubmittedFile'].'</a>':'');
			## Photo END ##
		}
		
		## Attachments START ##
		$settings = $lac->getAttachmentSettings();
		$attachment_settings = array();
		foreach ($settings as $index=>$setting){
		    if($setting['ClassLevelStr']){
		        $classLevelArr = explode(',', $setting['ClassLevelStr']);
		        
			    if(in_array($ApplyFor, $classLevelArr)){
			        $attachment_settings[] = $setting;
			    }
		    }else{
		        $attachment_settings[] = $setting;
		    }
		}
		$attachment_settings_count  = sizeof($attachment_settings);
		## Attachments END ##
		
		if(!$lac->isInternalUse($_GET['token']) && !$IsUpdate){
			$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		}else{
			$star = '';
		}
		
		@ob_start();
		include("template/admissionForm/docUploadForm.tmpl.php");
		$x = ob_get_clean();
		
		return $x;
	}
	
	/**
	 * Admission Form - create/edit form payment page
	 * /
	protected function getPaymentPage(){
		global $Lang;
		global $tempFolderPath, $fileData, $admission_cfg, $lac, $sys_custom, $intranet_root;
		
        $x = $this->getWizardStepsUI(4);
		
		if($IsUpdate){
			$application_details = $lac->getApplicationResult($_SESSION['KIS_StudentDateOfBirth'], $_SESSION['KIS_StudentBirthCertNo'], '', $_SESSION['KIS_ApplicationID']);
			$allCustInfo = $lac->getAllApplicationCustInfo($_SESSION['KIS_ApplicationID']);
			$StatusInfo = $lac->getApplicationStatus($application_details['ApplyYear'],'',$application_details['ApplicationID']);
		}
		
		@ob_start();
		include("template/admissionForm/paymentForm.tmpl.php");
		$x .= ob_get_clean();


		/**** Step button START **** /
		$lastStepId = 'step_docs_upload';
		$currentStepId = 'step_payment';
		$nextStepId = 'step_confirm';
		
	    $x .= $this->getStepButton($currentStepId, $nextStepId, $lastStepId);
		/**** Step button END **** /
		
		return $x;
	}/* */
	
	
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            $x .='<h1>Admission is Not Completed.<span>Please try to apply again!</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
//		if(!$LastContent){
//			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
//		}
//		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	function getQuotaFullPageContent($type='Admission', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
//		if($ApplicationID){
//			
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
////            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
////                           </p>';
////            $x .='<p>
////				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
////				</p>';            
//		}
//		else{
//			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
//            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
//		}
		if(!$LastContent){
			if($type == 'Admission'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['admissionQuotaFull'].'</h1>';
				$LastContent .= '<h1>Admission Quota is Full! Thanks for your support!</h1>';
			}else if($type == 'Interview'){
				$LastContent = '<div class="admission_complete_msg"><h1>'.$Lang['Admission']['munsang']['msg']['interviewQuotaFull'].'</h1>';
				$LastContent .= '<h1>Interview Timeslot Quota is Full! Please try to apply again!</h1>';
			}else{
				$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
			}
		}
		$x .= $LastContent.'</div>';
		$x .= '</div>';

		if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}

	
	function getPayPalButton($ApplicationID){
		global $admission_cfg, $lac;
		$ApplicationID = $lac->decodeMD5ApplicationID($ApplicationID);
		return '<form action="'.$admission_cfg['paypal_url'].'" <!--onsubmit="checkPayment(\''.$ApplicationID.'\');return false;"--> method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="'.$admission_cfg['hosted_button_id'].'">
				<input type="hidden" name="return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php" /> 
				    <input type="hidden" name="cancel_return" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_finish2.php?cm='.$ApplicationID.'" />
				    <input type="hidden" name="custom" value="'.$ApplicationID.'" />
					<input type="hidden" name="notify_url" value="http://'.$_SERVER['HTTP_HOST'].'/kis/admission_paypal/payment_ipn.php" /> 
				<input type="image" src="https://www.sandbox.paypal.com/zh_HK/HK/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal － 更安全、更簡單的網上付款方式！">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/zh_HK/i/scr/pixel.gif" width="1" height="1">
				</form>
				<script>
					function checkPayment(applicationID){
						var myWindow = window.open("", "paypal_payment");
				        myWindow.close();
				        
						$.ajax({
					       url: "ajax_check_payment_status.php",
					       type: "post",
					       data: { ApplicationNo: applicationID },
					       async: false,
					       success: function(data){
					           //alert("debugging: The classlevel is updated!");
					           if(data == 1){
					           	location.reload();
					           }
								else{
									this.submit();
								}
					       },
					       error:function(){
					           //alert("failure");
					           $("#result").html("There is error while submit");
					       }
					   });
					}
				</script>';
	}
	
	function getAfterUpdateFinishPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $lauc, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';

		$x .=' <div class="admission_complete_msg"><h1>申請編號為 '.$ApplicationID.'。&nbsp;&nbsp;<br/>';
		$x .='Your application number is '.$ApplicationID.'.&nbsp;&nbsp;<br><br><input type="button" value="'.$Lang['Admission']['printsubmitform'].' Print submitted form" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></h1></div>';
		
		$x .= '<div class="admission_board">';
		$x .= $this->getDocsUploadForm(1, 1);
		
		$applicationStatus = current($lac->getApplicationStatus($lac->schoolYearID,'',$ApplicationID));
		
		if($applicationStatus['interviewdate'] && $applicationStatus['interviewdate'] != '0000-00-00 00:00:00'){
			$interviewDateTime = explode(" ", $applicationStatus['interviewdate']);
			$x .='<h1 style="font-size: 15px">面試資料 Interview Information</h1>';
			$x .= '<table class="form_table" style="font-size: 15px">';
			$x .= '<tr>';
			$x .= '<td class="field_title">面試日期 Interview Date</td>';
			$x .= '<td>'.$interviewDateTime[0].'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試時間 Interview Time</td>';
			$x .= '<td>'.substr($interviewDateTime[1], 0, -3).'</td>';
			$x .= '</tr>';
			$x .= '<td class="field_title">面試地點 Interview Location</td>';
			$x .= '<td>'.$applicationStatus['interviewlocation'].'</td>';
			$x .= '</tr>';
			$x .= '</table>';
		}
		$x .= '</div>';
		
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent;
		$x .= '</div>';
		
			$x .= '<div class="edit_bottom">
					<input id="finish_page_finish_button" type="button" class="formsubbutton" onclick="location.href=\'index_edit.php\'" value="'.$Lang['Admission']['finish'].' Finish" />
				</div>
				<p class="spacer"></p></div>';
		
		
		return $x;
	}
	
	protected function getStepButton($currentStepId, $nextStepId, $lastStepId = '', $nextBtnLang = '', $isUpdate = false)
    {
        global $lac, $sys_custom;
        global $Lang;

        $backButton = '';
        $submitButton = '';
        
        if ($lastStepId) {
            $backButton = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('{$currentStepId}','{$lastStepId}')", "back_{$lastStepId}", "", 0, "formbutton");
        }

        $nextBtnLang = ($nextBtnLang) ? $nextBtnLang : $Lang['Btn']['Next'];

        if ($nextStepId) {
            $nextButton = $this->GET_ACTION_BTN($nextBtnLang, "button", "goto('{$currentStepId}','{$nextStepId}')", "go_{$nextStepId}", "", 0, "formbutton");
        } else {
            $nextButton = $this->GET_ACTION_BTN($nextBtnLang, "submit", "", "go_submit", "", 0, "formbutton");
        }
        
        if($currentStepId == 'step_input_form' || $currentStepId == 'step_docs_upload'){
        	$submitButton = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "go_submit", "", 0, "formbutton");
        }

        if ($lastStepId) {
            if ($isUpdate) {
                $cancelButton = '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index_edit.php\');return document.MM_returnValue" value="' . $Lang['Btn']['Cancel'] . '" />';
            } else {
                if (! $lac->isInternalUse($_REQUEST['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
                    $cancelButton = '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token=' . $_REQUEST['token'] . '\');return document.MM_returnValue" value="' . $Lang['Btn']['Cancel'] . '" />';
                } else {
                    $cancelButton = '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="' . $Lang['Btn']['Cancel'] . '" />';
                }
            }
        }

        $buttonHTML = <<<HTML
            <div class="edit_bottom">
                {$backButton}
                {$nextButton}
                {$submitButton}
                {$cancelButton}
            </div>
            <p class="spacer"></p>
HTML;
        return $buttonHTML;
    }
	
}
?>