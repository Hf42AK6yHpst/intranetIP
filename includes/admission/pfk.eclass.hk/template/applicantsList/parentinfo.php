<?php

global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$parentTypeArr = array(1=>'F',2=>'M',3=>'G');

?>
<style>
.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:70%">
        <!--<col style="width:23%">
        <col style="width:24%">-->
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<?if(trim($ParentInfo['F']['EnglishSurname']) != ''){$parentTypeArr = array(1=>'F');?>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['F'] ?></center></td>
		<?} if(trim($ParentInfo['M']['EnglishSurname']) != ''){$parentTypeArr = array(2=>'M');?>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['M'] ?></center></td>
		<?} if(trim($ParentInfo['G']['EnglishSurname']) != ''){$parentTypeArr = array(3=>'G');?>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['G'] ?></center></td>
		<?}?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['englishname'] ?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){
		    if($ParentInfo[$parentType]['EnglishSurname'] && $ParentInfo[$parentType]['EnglishFirstName']){
		        $name = "{$ParentInfo[$parentType]['EnglishSurname']}, {$ParentInfo[$parentType]['EnglishFirstName']}";
		    }else{
		        $name = '';
		    }
		?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $name ) ?>
        	</td>
    	<?php
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){
		    if($ParentInfo[$parentType]['ChineseSurname'] && $ParentInfo[$parentType]['ChineseFirstName']){
		        $name = "{$ParentInfo[$parentType]['ChineseSurname']}{$ParentInfo[$parentType]['ChineseFirstName']}";
		    }else{
		        $name = '';
		    }
		?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $name ) ?>
        	</td>
    	<?php
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['nationality'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Nationality'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['nativePlace'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["G{$index}NativePlace"][0]['Value'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['employer'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Company'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['position'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['JobPosition'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['businessAddress'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['OfficeAddress'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['businessPhone'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['OfficeTelNo'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['mobilephoneno'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Mobile'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['email'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Email'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['WeChatId'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $allCustInfo["G{$index}WeChatID"][0]['Value'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['invoiceTo'] ?>
		</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['InvoiceTo'][0]['Value'] ) ?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['AnnualHouseholdIncome'] ?>
		</td>
		<td>
			<?= kis_ui::displayTableField( $kis_lang['Admission']['PICLC']['AnnualHouseholdIncomeType'][ $allCustInfo['AnnualHouseholdIncome'][0]['Value'] ] ) ?>
    	</td>
	</tr>
	
</table>