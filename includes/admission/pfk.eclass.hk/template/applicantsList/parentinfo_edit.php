<?php

global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);

$star = $mustfillinsymbol;
$parentTypeArr = array(1=>'F',2=>'M',3=>'G');

?>
<style>
.col2div{
    display:inline-block;
    width:48%;
}
.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>

<table class="form_table parentInfo">
	<colgroup>
        <col style="width:30%">
        <col style="width:70%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head">
			<center>
				<?=$libinterface->Get_Radio_Button('PG_Type_F', 'PG_Type', 'F', (trim($ParentInfo['F']['EnglishSurname'])!=''), '', $kis_lang['Admission']['PG_Type']['F'])?>
	    		<?=$libinterface->Get_Radio_Button('PG_Type_M', 'PG_Type', 'M', (trim($ParentInfo['M']['EnglishSurname'])!=''), '', $kis_lang['Admission']['PG_Type']['M'])?>
	    		<?=$libinterface->Get_Radio_Button('PG_Type_G', 'PG_Type', 'G', (trim($ParentInfo['G']['EnglishSurname'])!=''), '', $kis_lang['Admission']['PG_Type']['G'])?>
    		</center></td>
		<!--<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['F'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['M'] ?></center></td>
		<td class="form_guardian_head"><center><?=$kis_lang['Admission']['PG_Type']['G'] ?></center></td>-->
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$kis_lang['Admission']['englishname'] ?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName_surname = "EnglishSurname";
		    $formName_surname = "G{$index}EnglishSurname";
		    $fieldName_firstName = "EnglishFirstName";
		    $formName_firstName = "G{$index}EnglishFirstName";
		?>
    		<td class="form_guardian_field">
    			<div class="col2div">
            		<label for="<?=$formName_surname ?>">
                		<?=$kis_lang['Admission']['PICLC']['surname'] ?>
            		</label>
        			<input name="<?=$formName_surname ?>" type="text" id="<?=$formName_surname ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_surname] ?>" data-parent="<?=$index ?>" />
        		</div>
        		&nbsp;
    			<div class="col2div">
            		<label for="<?=$formName_firstName ?>">
                		<?=$kis_lang['Admission']['PICLC']['firstName'] ?>
            		</label>
        			<input name="<?=$formName_firstName ?>" type="text" id="<?=$formName_firstName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_firstName] ?>" data-parent="<?=$index ?>" />
        		</div>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['chinesename'] ?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName_surname = "ChineseSurname";
		    $formName_surname = "G{$index}ChineseSurname";
		    $fieldName_firstName = "ChineseFirstName";
		    $formName_firstName = "G{$index}ChineseFirstName";
		?>
    		<td class="form_guardian_field">
    			<div class="col2div">
            		<label for="<?=$formName_surname ?>">
                		<?=$kis_lang['Admission']['PICLC']['surname'] ?>
            		</label>
        			<input name="<?=$formName_surname ?>" type="text" id="<?=$formName_surname ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_surname] ?>" data-parent="<?=$index ?>" />
        		</div>
        		&nbsp;
    			<div class="col2div">
            		<label for="<?=$formName_firstName ?>">
                		<?=$kis_lang['Admission']['PICLC']['firstName'] ?>
            		</label>
        			<input name="<?=$formName_firstName ?>" type="text" id="<?=$formName_firstName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_firstName] ?>" data-parent="<?=$index ?>" />
        		</div>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['nationality']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "Nationality";
		    $formName = "G{$index}Nationality";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['nativePlace']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    //$index = 1; 
		    $formName = "G{$index}NativePlace";
		?>
    		<td class="form_guardian_field">
    			<input name="G1NativePlace" type="text" id="G1NativePlace" class="textboxtext" value="<?=$allCustInfo[$formName][0]['Value'] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['employer']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "Company";
		    $formName = "G{$index}Company";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['position']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "JobPosition";
		    $formName = "G{$index}JobPosition";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['businessAddress']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "OfficeAddress";
		    $formName = "G{$index}OfficeAddress";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['businessPhone']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "OfficeTelNo";
		    $formName = "G{$index}OfficeTelNo";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$kis_lang['Admission']['mobilephoneno']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "Mobile";
		    $formName = "G{$index}Mobile";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['email']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    $index = 1; 
		    $fieldName = "Email";
		    $formName = "G{$index}Email";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['WeChatId']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
			if(trim($ParentInfo[$parentType]['EnglishSurname'])==''){
		    	continue;
		    }
		    //$index = 1; 
		    $fieldName = "WeChatID";
		    $formName = "G{$index}WeChatID";
		?>
    		<td class="form_guardian_field">
    			<input name="G1WeChatID" type="text" id="G1WeChatID" class="textboxtext" value="<?=$allCustInfo[$formName][0]['Value'] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['invoiceTo']?>
		</td>
		<td>
			<input name="InvoiceTo" type="text" id="InvoiceTo" class="textboxtext" value="<?=$allCustInfo['InvoiceTo'][0]['Value'] ?>" />
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['AnnualHouseholdIncome']?>
		</td>
		<td>
			<select id="AnnualHouseholdIncome" name="AnnualHouseholdIncome">
    			<?php 
    			foreach($admission_cfg['AnnualHouseholdIncome'] as $incomeType){ 
    			    $selected = ($allCustInfo['AnnualHouseholdIncome'][0]['Value'] == $incomeType)? ' selected' : '';
    			?>
    				<option value="<?=$incomeType ?>" <?=$selected ?> >
        				<?=$kis_lang['Admission']['PICLC']['AnnualHouseholdIncomeType'][$incomeType] ?>
    				</option>
				<?php 
    			} 
    			?>
			</select>
		</td>
	</tr>
	
	
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	return check_parent_form();
}


function check_parent_form(){
	//For debugging only
	//return true;
	
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;

	var form1 = applicant_form;
	var isValid = true;
	/******** Basic init END ********/
	
	
	/**** All empty START ****/
	var parentHasInfoArr = [];
	var _text = '';
	$('.parentInfo input').each(function(){
		if($(this).val().trim() != ''){
			parentHasInfoArr.push($(this).data('parent'));
		}
	});
	
	if(parentHasInfoArr.length == 0){
		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterAtLeastOneParent']?>");
		$('#G1EnglishSurname').focus();
		return false;
	}
	/**** All empty END ****/
	

	/**** Name START ****/	
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(EnglishSurname|EnglishFirstName)/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['HKUGAPS']['msg']['enterParentName']?>");
    		this.focus();
    		return isValid = false;
    	}
    	if(
    		!checkNaNull(this.value) &&
    		!checkIsEnglishCharacter(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['enterenglishcharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(ChineseSurname|ChineseFirstName)/);
    }).each(function(){
    	if(
			$.trim(this.value)!='' &&
    		!checkNaNull(this.value) &&
    		!checkIsChineseCharacter(this.value)
    	){
    		alert(" <?=$kis_lang['Admission']['msg']['enterchinesecharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name END ****/
	
	/**** Nationality START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dNationality/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert(" <?=$kis_lang['Admission']['msg']['enternationality']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Nationality START ****/
	
	/**** Native Place START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dNativePlace/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert("<?=$kis_lang['Admission']['PICLC']['msg']['enterNativePlace']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Native Place START ****/
	
	/**** Employer START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dCompany/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterEmployer']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Employer START ****/
	
	/**** Position START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dJobPosition/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterPosition']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Position START ****/
	
	/**** Business Address START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dOfficeAddress/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterBusinessAddress']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Business Address START ****/
	
	/**** Business Phone START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dOfficeTelNo/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert(" <?=$kis_lang['Admission']['PICLC']['msg']['enterBusinessPhone']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Business Phone START ****/
	
	/**** Mobile START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dMobile/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert(" <?=$kis_lang['Admission']['msg']['entermobilephoneno']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Mobile START ****/
	
	/**** Email START ****/
//	$('.parentInfo input').filter(function() {
//		var parent = $(this).data('parent');
//        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dEmail/);
//    }).each(function(){
//    	if($.trim(this.value)==''){
//    		alert(" <?=$kis_lang['Admission']['icms']['msg']['entermailaddress']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//
//    	if(!re.test($.trim(this.value))){
//    		alert(" <?=$kis_lang['Admission']['icms']['msg']['invalidmailaddress']?>");
//    		this.focus();
//    		return isValid = false;
//    	}
//    });
//	if(!isValid) return false;
	/**** Email START ****/

	return true;
}
</script>