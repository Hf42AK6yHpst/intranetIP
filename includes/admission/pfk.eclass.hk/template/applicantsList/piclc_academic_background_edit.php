<?php

global $libkis_admission;


$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,'',$applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>

<style>
.col2div{
    display:inline-block;
    width:48%;
}
.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:10px">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:405px">
        <col style="">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['SchoolName'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Location'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['DatesAttended'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['LanguageOfInstruction'] ?></td>
	</tr>
	
	<?php for($i=0;$i<$libkis_admission::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){ ?>
    	<tr>
    		<?php if($i == 0){ ?>
        		<td class="field_title"  rowspan="3">
           			<?=$kis_lang['Admission']['PICLC']['previousSchool'] ?>
        		</td>
    		<?php } ?>
    	
    		<td>
       			<?=$i+1?>)
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "StudentSchool{$i}_Name";
        		?>
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['NameOfSchool'] ?>"/>
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "StudentSchool{$i}_Location";
        		?>
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['SchoolAddress'] ?>"/>
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "StudentSchool{$i}_DatesAttended";
        		?>
        		<div class="col2div" style="width: 195px;">
        			<?=$kis_lang['Admission']['from'] ?>
        			<?=$LangEn['Admission']['from'] ?>:
    				<input 
        				type="text" 
        				class="textboxtext datepicker" 
        				placeholder="YYYY-MM-DD" 
        				maxlength="10" 
        				size="15"
        				style="width: 110px;"
        				 
        				id="<?=$formName ?>_From" 
        				name="<?=$formName ?>_From" 
        				value="<?=$StudentPrevSchoolInfo[$i]['StartDate'] ?>" 
    				/>
        		</div>
        		<div class="col2div" style="width: 195px;">
        			<?=$kis_lang['Admission']['to'] ?>
        			<?=$LangEn['Admission']['to'] ?>:
    				<input 
        				type="text" 
        				class="textboxtext datepicker" 
        				placeholder="YYYY-MM-DD" 
        				maxlength="10" 
        				size="15"
        				style="width: 110px;"
        				 
        				id="<?=$formName ?>_To" 
        				name="<?=$formName ?>_To" 
        				value="<?=$StudentPrevSchoolInfo[$i]['EndDate'] ?>" 
    				/>
        		</div>
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "StudentSchool{$i}_Language";
        		?>
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['LangSpoken'] ?>"/>
    		</td>
    	</tr>
	<?php } ?>
</table>

<!--<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="70%">
    </colgroup>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['ChildPlacedOutStandardGroup']?> 
    	</td>
    	<td>
    		<?php 
    		    $checkedY = ($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'Y')? 'checked' : '';
    		    $checkedN = ($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'N')? 'checked' : '';
    		?>
			<div>
    			<input type="radio" value="Y" id="ChildPlacedOutStandardGroup_Y" name="ChildPlacedOutStandardGroup" <?=$checkedY ?>>
    			<label for="ChildPlacedOutStandardGroup_Y"><?=$kis_lang['Admission']['yes2'] ?></label>
    			
    			<label for="ChildPlacedOutStandardGroup_Details" style="margin-left: 10px;"><?=$kis_lang['Admission']['PICLC']['Details'] ?> <?=$LangEn['Admission']['PICLC']['Details'] ?></label>
    			<input name="ChildPlacedOutStandardGroup_Details" type="text" id="ChildPlacedOutStandardGroup_Details" class="textboxtext" value="<?=$allCustInfo['ChildPlacedOutStandardGroup_Details'][0]['Value'] ?>" style="width: 50%;"/>
			</div>
			<div>
    			<input type="radio" value="N" id="ChildPlacedOutStandardGroup_N" name="ChildPlacedOutStandardGroup" <?=$checkedN ?>>
    			<label for="ChildPlacedOutStandardGroup_N"><?=$kis_lang['Admission']['no2'] ?></label>
			</div>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['ChildSpecialClassTalent']?> 
    	</td>
    	<td>
    		<?php 
    		    $checkedY = ($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'Y')? 'checked' : '';
    		    $checkedN = ($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'N')? 'checked' : '';
    		?>
			<div>
    			<input type="radio" value="Y" id="ChildSpecialClassTalent_Y" name="ChildSpecialClassTalent" <?=$checkedY ?>>
    			<label for="ChildSpecialClassTalent_Y"><?=$kis_lang['Admission']['yes2'] ?></label>
    			
    			<label for="ChildSpecialClassTalent_Details" style="margin-left: 10px;"><?=$kis_lang['Admission']['PICLC']['Details'] ?> <?=$LangEn['Admission']['PICLC']['Details'] ?></label>
    			<input name="ChildSpecialClassTalent_Details" type="text" id="ChildSpecialClassTalent_Details" class="textboxtext" value="<?=$allCustInfo['ChildSpecialClassTalent_Details'][0]['Value'] ?>" style="width: 50%;"/>
			</div>
			<div>
    			<input type="radio" value="N" id="ChildSpecialClassTalent_N" name="ChildSpecialClassTalent" <?=$checkedN ?>>
    			<label for="ChildSpecialClassTalent_N"><?=$kis_lang['Admission']['no2'] ?></label>
			</div>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['ChildEducationalPsychologist']?> 
    	</td>
    	<td>
    		<?php 
    		    $checkedY = ($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'Y')? 'checked' : '';
    		    $checkedN = ($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'N')? 'checked' : '';
    		?>
			<div>
    			<input type="radio" value="Y" id="ChildEducationalPsychologist_Y" name="ChildEducationalPsychologist" <?=$checkedY ?>>
    			<label for="ChildEducationalPsychologist_Y"><?=$kis_lang['Admission']['yes2'] ?></label>
    			
    			<label for="ChildEducationalPsychologist_Details" style="margin-left: 10px;">
        			<?=$kis_lang['Admission']['PICLC']['Details'] ?>
    			</label>
    			<input name="ChildEducationalPsychologist_Details" type="text" id="ChildEducationalPsychologist_Details" class="textboxtext" value="<?=$allCustInfo['ChildEducationalPsychologist_Details'][0]['Value'] ?>" style="width: 50%;"/>
			</div>
			<div>
    			<input type="radio" value="N" id="ChildEducationalPsychologist_N" name="ChildEducationalPsychologist" <?=$checkedN ?>>
    			<label for="ChildEducationalPsychologist_N"><?=$kis_lang['Admission']['no2'] ?></label>
			</div>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['IndicateStudentFavouriteSubject']?>
    	</td>
    	<td>
			<input name="StudentFavouriteSubject" type="text" id="StudentFavouriteSubject" class="textboxtext" value="<?=$allCustInfo['StudentFavouriteSubject'][0]['Value']?>"/>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['IndicateStudentSuccessfulSubject']?> 
    	</td>
    	<td>
			<input name="StudentSuccessfulSubject" type="text" id="StudentSuccessfulSubject" class="textboxtext" value="<?=$allCustInfo['StudentSuccessfulSubject'][0]['Value']?>"/>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['IndicateStudentChallengingSubject']?>  
    	</td>
    	<td>
			<input name="StudentChallengingSubject" type="text" id="StudentChallengingSubject" class="textboxtext" value="<?=$allCustInfo['StudentChallengingSubject'][0]['Value']?>"/>
    	</td>
    </tr>
</table>-->


<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){
	/**** ChildPlacedOutStandardGroup START ****/
	if($('#ChildPlacedOutStandardGroup_N').prop('checked')){
		 $('#ChildPlacedOutStandardGroup_Details').val('');
	}
	/**** ChildPlacedOutStandardGroup END ****/

	/**** ChildSpecialClassTalent START ****/
	if($('#ChildSpecialClassTalent_N').prop('checked')){
		 $('#ChildSpecialClassTalent_Details').val('');
	}
	/**** ChildSpecialClassTalent END ****/

	/**** ChildEducationalPsychologist START ****/
	if($('#ChildEducationalPsychologist_N').prop('checked')){
		 $('#ChildEducationalPsychologist_Details').val('');
	}
	/**** ChildEducationalPsychologist END ****/
	
	return true;
}

</script>