<?php

global $libkis_admission;

$SiblingInfo = $libkis_admission->getApplicationSibling($schoolYearID,'',$applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>
<style>
.whereToKnowOptionDiv{
    margin-bottom: 5px;
}

.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:10px">
        <col style="width:20%">
        <col style="width:125px">
        <col style="width:155px">
        <col style="">
        <!--<col style="width:205px">-->
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['name'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['gender'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['dateofbirth'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['CurrentSchool'] ?></td>
		<!--<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['ApplyingPIS'] ?></td>-->
	</tr>
	
	<?php for($i=0;$i<$libkis_admission::SIBLING_COUNT;$i++){ ?>
    	<tr>
    		<?php if($i == 0){ ?>
    			<td class="field_title" rowspan="3">
           			<?=$kis_lang['Admission']['PICLC']['SiblingInformation'] ?>
           			<?=$kis_langEn['Admission']['PICLC']['SiblingInformation'] ?>
        		</td>
    		<?php }?>
    	
    		<td>
       			<?=$i+1?>)
    		</td>
    		
    		<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $SiblingInfo[$i]['EnglishName'] ) ?>
    		</td>
    		
    		<td class="form_guardian_field">
    			<?php
    			    if($SiblingInfo[$i]['Gender'] == 'M'){
    			        echo kis_ui::displayTableField( $kis_lang['Admission']['genderType']['M'] );
    			    }else if($SiblingInfo[$i]['Gender'] == 'F'){
    			        echo kis_ui::displayTableField( $kis_lang['Admission']['genderType']['F'] );
    			    }else{
    			        echo kis_ui::displayTableField( '' );
    			    }
    			?>
    		</td>
    		
    		<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $SiblingInfo[$i]['DOB'] ) ?>
    		</td>
    		
    		<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $SiblingInfo[$i]['CurrentSchool'] ) ?>
    		</td>
    		
    		<!--<td class="form_guardian_field">
    			<?php
    			    if($SiblingInfo[$i]['ApplyingSameSchool'] == 'Y'){
    			        echo kis_ui::displayTableField( $kis_lang['Admission']['yes'] );
    			    }else if($SiblingInfo[$i]['ApplyingSameSchool'] == 'N'){
    			        echo kis_ui::displayTableField( $kis_lang['Admission']['no'] );
    			    }else{
    			        echo kis_ui::displayTableField( '' );
    			    }
    			?>
    		</td>-->
    	</tr>
	<?php } ?>
</table>


<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="70%">
    </colgroup>
    
    <!--<tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['SpecialHolidayCelebrate']?>
    	</td>
    	<td>
			<?= kis_ui::displayTableField( $allCustInfo['SpecialHolidayCelebrate'][0]['Value'] ) ?>
    	</td>
    </tr>-->
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PFK']['WhyChooseSchool']?>
    	</td>
    	<td>
			<?= kis_ui::displayTableField( $allCustInfo['WhyChoosePIS'][0]['Value'] ) ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PFK']['WhereToKnowSchool']?>
    	</td>
    	<td>
    		<?php 
		    if(empty($allCustInfo["WhereToKnowPIS"][0]['Value'])){
		        echo ' -- ';
		    }else{
		        $whereToKnowArr = explode(',', $allCustInfo["WhereToKnowPIS"][0]['Value']);
		        foreach($whereToKnowArr as $whereToKnowId){
		            $whereToKnow = $admission_cfg['WhereToKnowPis'][$whereToKnowId];
		            $details = $allCustInfo["WhereToKnowPIS_{$whereToKnow}Details"][0]['Value'];
            ?>
	            	<div>
	            		<span style="width: 200px;display: inline-block;">
	            			<?=$kis_lang['Admission']['PICLC']['KnowPIS'][$whereToKnow] ?>
	            			<?=$kis_langEn['Admission']['PICLC']['KnowPIS'][$whereToKnow] ?>
	            		</span>
	            		
	            		<?php if($details){ ?>
	            			<span>( <?=$details ?> )</span>
	            		<?php } ?>
	            	</div>
            <?php
		        }
		    }
    		?>
    	</td>
    </tr>
</table>
<table class="form_table" style="font-size: 13px">
	<colgroup>
        <col style="width:30%">
        <col style="width:70%">
    </colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['otherInfo']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['OtherInformation'][0]['Value'] ) ?>
		</td>	
	</tr>
</table>
<table class="form_table" style="font-size: 13px">
	<colgroup>
        <col style="width:30%">
        <col style="width:70%">
    </colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PFK']['school']?>
    	</td>
		<td>
    		<?php
    		    $firstChoice = $allCustInfo['SchoolFirstChoice'][0]['Value'];
    		    $secondChoice = $allCustInfo['SchoolSecondChoice'][0]['Value'];
    		    $thirdChoice = $allCustInfo['SchoolThirdChoice'][0]['Value'];
    	    ?>
	    	<label style="width: 120px;display: inline-block;">
	    		<?=$kis_lang['Admission']['PFK']['firstChoice'] ?>:
    		</label>
	    	<?= ($firstChoice)?$kis_lang['Admission']['PFK']['schoolType'][$firstChoice]:' -- ' ?>
	    	<br />
	    	
	    	<label style="width: 120px;display: inline-block;">
	    		<?=$kis_lang['Admission']['PFK']['secondChoice'] ?>:
    		</label>
	    	<?= ($secondChoice)?$kis_lang['Admission']['PFK']['schoolType'][$secondChoice]:' -- ' ?>
	    	<br />
	    	
	    	<label style="width: 120px;display: inline-block;">
	    		<?=$kis_lang['Admission']['PFK']['thirdChoice'] ?>:
	    	</label>
	    	<?= ($thirdChoice)?$kis_lang['Admission']['PFK']['schoolType'][$thirdChoice]:' -- ' ?>
		</td>	
	</tr>
</table>