<?php

global $libkis_admission;

$SiblingInfo = $libkis_admission->getApplicationSibling($schoolYearID,'',$applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$schoolArr = $libkis_admission->getSchoolBySchoolYearIdClassLevelId($applicationInfo['ApplyYear'],$applicationInfo['ApplyLevel']);
?>
<style>
.whereToKnowOptionDiv{
    margin-bottom: 5px;
}

.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:10px">
        <col style="width:20%">
        <col style="width:125px">
        <col style="width:155px">
        <col style="">
        <!--<col style="width:205px">-->
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['name'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['gender'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['dateofbirth'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['CurrentSchool'] ?></td>
		<!--<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['ApplyingPIS'] ?></td>-->
	</tr>
	
	<?php for($i=0;$i<$libkis_admission::SIBLING_COUNT;$i++){ ?>
    	<tr>
    		<?php if($i == 0){ ?>
    			<td class="field_title" rowspan="3">
           			<?=$kis_lang['Admission']['PICLC']['SiblingInformation'] ?>
        		</td>
    		<?php }?>
    	
    		<td>
       			<?=$i+1?>)
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		    $formName = "Sibling{$i}_Name";
        		?>
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$SiblingInfo[$i]['EnglishName'] ?>"/>
    		</td>
    		
    		<td class="form_guardian_field">
        		
        		<?php
        		    $checkedM = ($SiblingInfo[$i]['Gender'] == 'M')?'checked':'';
        		    $checkedF = ($SiblingInfo[$i]['Gender'] == 'F')?'checked':'';
        		?>
    			<input type="radio" value="M" id="Sibling<?=$i ?>_GenderM" name="Sibling<?=$i ?>_Gender" <?=$checkedM ?> />
    			<label for="Sibling<?=$i ?>_GenderM"><?=$kis_lang['Admission']['genderType']['M']?></label>
    			<input type="radio" value="F" id="Sibling<?=$i ?>_GenderF" name="Sibling<?=$i ?>_Gender" <?=$checkedF ?> >
    			<label for="Sibling<?=$i ?>_GenderF"><?=$kis_lang['Admission']['genderType']['F']?></label>
    		
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		    $formName = "Sibling{$i}_DOB";
        		?>
				<input 
    				type="text" 
    				class="textboxtext datepicker" 
    				placeholder="YYYY-MM-DD" 
    				maxlength="10" 
    				size="15"
    				style="width: 110px;"
    				 
    				id="<?=$formName ?>" 
    				name="<?=$formName ?>" 
    				value="<?=$SiblingInfo[$i]['DOB'] ?>" 
				/>
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "Sibling{$i}_CurrentSchool";
        		?>
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$SiblingInfo[$i]['CurrentSchool'] ?>"/>
    		</td>
    		
    		<!--<td class="form_guardian_field">
        		
            		<?php
            		    $checkedY = ($SiblingInfo[$i]['ApplyingSameSchool'] == 'Y')?'checked':'';
            		    $checkedN = ($SiblingInfo[$i]['ApplyingSameSchool'] == 'N')?'checked':'';
            		?>
        			<input type="radio" value="Y" id="Sibling<?=$i ?>_ApplyingSameSchoolY" name="Sibling<?=$i ?>_ApplyingSameSchool" <?=$checkedY ?> />
        			<label for="Sibling<?=$i ?>_ApplyingSameSchoolY"><?=$kis_lang['Admission']['yes'] ?> <?=$kis_langEn['Admission']['yes'] ?></label>
        			
        			&nbsp;
        			&nbsp;
        			
        			<input type="radio" value="N" id="Sibling<?=$i ?>_ApplyingSameSchoolN" name="Sibling<?=$i ?>_ApplyingSameSchool" <?=$checkedN ?> />
        			<label for="Sibling<?=$i ?>_ApplyingSameSchoolN"><?=$kis_lang['Admission']['no'] ?> <?=$kis_langEn['Admission']['no'] ?></label>
        		
    		</td>-->
    	</tr>
	<?php } ?>
</table>


<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="70%">
    </colgroup>
    
    <!--<tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['SpecialHolidayCelebrate']?>
    	</td>
    	<td>
			<input name="SpecialHolidayCelebrate" type="text" id="SpecialHolidayCelebrate" class="textboxtext" value="<?=$allCustInfo['SpecialHolidayCelebrate'][0]['Value']?>"/>
    	</td>
    </tr>-->
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PFK']['WhyChooseSchool']?>
    	</td>
    	<td>
			<input name="WhyChoosePIS" type="text" id="WhyChoosePIS" class="textboxtext" value="<?=$allCustInfo['WhyChoosePIS'][0]['Value']?>"/>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PFK']['WhereToKnowSchool']?>
    	</td>
    	<td>
    		<?php 
    	        $whereToKnowStr = $allCustInfo["WhereToKnowPIS"][0]['Value'];
    	        $whereToKnowArr = explode(',', $whereToKnowStr);
    		    foreach($admission_cfg['WhereToKnowPis'] as $value => $whereToKnow){ 
    		        $checked = (in_array($value, $whereToKnowArr))?'checked':'';
    	    ?>
        			<div class="whereToKnowOptionDiv">
            			<input type="checkbox" id="WhereToKnowPIS_<?=$whereToKnow ?>" name="WhereToKnowPIS[]" value="<?=$value ?>" <?=$checked ?> />
            			<label for="WhereToKnowPIS_<?=$whereToKnow ?>" style="margin-bottom: 5px;">
                			<?=$kis_lang['Admission']['PICLC']['KnowPIS'][$whereToKnow] ?>
                            <?=$kis_langEn['Admission']['PICLC']['KnowPIS'][$whereToKnow] ?>
                        </label>
                        
                        <?php if(in_array($whereToKnow, $admission_cfg['WhereToKnowPisDetails'])){ ?>
                            (
                                <label for="WhereToKnowPIS_<?=$whereToKnow ?>Details">
                                	<?=$kis_lang['Admission']['PICLC']['PleaseSpecify'] ?>
                                	<?=$kis_langEn['Admission']['PICLC']['PleaseSpecify'] ?>:
                            	</label>
                            	<input id="WhereToKnowPIS_<?=$whereToKnow ?>Details" name="WhereToKnowPIS_<?=$whereToKnow ?>Details" value="<?=$allCustInfo["WhereToKnowPIS_{$whereToKnow}Details"][0]['Value'] ?>" />
                            )
                        <?php } ?>
                    </div>
            <?php 
    		    }
    		?>
    	</td>
    </tr>
</table>
<table class="form_table" style="font-size: 13px">
	<colgroup>
        <col style="width:30%">
        <col style="width:70%">
    </colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['otherInfo']?>
    	</td>
		<td>
			<input name="OtherInformation" type="text" id="OtherInformation" class="textboxtext" value="<?=$allCustInfo['OtherInformation'][0]['Value'] ?>"/>
		</td>	
	</tr>
</table>
<table class="form_table" style="font-size: 13px">
	<colgroup>
        <col style="width:30%">
        <col style="width:70%">
    </colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['PFK']['school']?>
    	</td>
		<td>
			<div>
				<label for="schoolFirstChoice" style="width: 100px;display: inline-block;">
					<?=$kis_lang['Admission']['PFK']['firstChoice'] ?>
				</label>
				<select id="schoolFirstChoice" name="SchoolFirstChoice">
					<option value=""> - <?=$kis_lang['Admission']['pleaseSelect'] ?> - </option>
    				<?php 
    				foreach($schoolArr as $type): 
    				    $selected = ($allCustInfo['SchoolFirstChoice'][0]['Value'] == $type)? ' selected' : '';
    				?>
    					<option value="<?=$type ?>" <?=$selected ?>><?=$kis_lang['Admission']['PFK']['schoolType'][$type] ?></option>
					<?php 
					endforeach; 
					?>
				</select>
			</div>
			<div style="margin-top: 5px;">
				<label for="schoolSecondChoice" style="width: 100px;display: inline-block;">
					<?=$kis_lang['Admission']['PFK']['secondChoice'] ?>
				</label>
				<select id="schoolSecondChoice" name="SchoolSecondChoice">
					<option value=""> - <?=$kis_lang['Admission']['pleaseSelect'] ?> - </option>
    				<?php 
    				foreach($schoolArr as $type): 
    				    $selected = ($allCustInfo['SchoolSecondChoice'][0]['Value'] == $type)? ' selected' : '';
    				?>
    					<option value="<?=$type ?>" <?=$selected ?>><?=$kis_lang['Admission']['PFK']['schoolType'][$type] ?></option>
					<?php 
					endforeach; 
					?>
				</select>
			</div>
			<div style="margin-top: 5px;">
				<label for="schoolThirdChoice" style="width: 100px;display: inline-block;">
					<?=$kis_lang['Admission']['PFK']['thirdChoice'] ?>
				</label>
				<select id="schoolThirdChoice" name="SchoolThirdChoice">
					<option value=""> - <?=$kis_lang['Admission']['pleaseSelect'] ?> - </option>
    				<?php 
    				foreach($schoolArr as $type): 
    				    $selected = ($allCustInfo['SchoolThirdChoice'][0]['Value'] == $type)? ' selected' : '';
    				?>
    					<option value="<?=$type ?>" <?=$selected ?>><?=$kis_lang['Admission']['PFK']['schoolType'][$type] ?></option>
					<?php 
					endforeach; 
					?>
				</select>
			</div>
		</td>	
	</tr>
</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkValidForm(){
	return true;
}

</script>