<?php

global $libkis_admission;


$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,$classLevelID='',$applicationID=$applicationInfo['applicationID'],$recordID='');
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>
<style>
.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>
<table class="form_table">
    <colgroup>
        <col style="width:30%">
        <col style="width:10px">
        <col style="width:15%">
        <col style="width:15%">
        <col style="width:405px">
        <col style="">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['SchoolName'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['Location'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['DatesAttended'] ?></td>
		<td class="form_guardian_head"><?=$kis_lang['Admission']['PICLC']['LanguageOfInstruction'] ?></td>
	</tr>
	
	<?php for($i=0;$i<$libkis_admission::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){ ?>
    	<tr>
    		<?php if($i == 0){ ?>
        		<td class="field_title"  rowspan="3">
           			<?=$kis_lang['Admission']['PICLC']['previousSchool'] ?>
        		</td>
    		<?php } ?>
    	
    		<td>
       			<?=$i+1?>)
    		</td>
    		
    		<td class="form_guardian_field">
				<?= kis_ui::displayTableField( $StudentPrevSchoolInfo[$i]['NameOfSchool'] ) ?>
    		</td>
    		
    		<td class="form_guardian_field">
				<?= kis_ui::displayTableField( $StudentPrevSchoolInfo[$i]['SchoolAddress'] ) ?>
    		</td>
    		
    		<td class="form_guardian_field">
    			<?php
    			    $from = $StudentPrevSchoolInfo[$i]['StartDate'];
    			    $to = $StudentPrevSchoolInfo[$i]['EndDate'];
    			    if($from && $to){
    			        echo kis_ui::displayTableField( "$from ~ $to" );
    			    }else{
    			        echo kis_ui::displayTableField('');
    			    }
    			?>
    		</td>
    		
    		<td class="form_guardian_field">
				<?= kis_ui::displayTableField( $StudentPrevSchoolInfo[$i]['LangSpoken'] ) ?>
    		</td>
    	</tr>
	<?php } ?>
</table>


<!--<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="70%">
    </colgroup>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['ChildPlacedOutStandardGroup']?> 
    	</td>
    	<td>
    		<?php
    		    if($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'Y'){
    		        echo kis_ui::displayTableField( $kis_lang['Admission']['yes2'] );
    		        if($allCustInfo['ChildPlacedOutStandardGroup_Details'][0]['Value']){
        		        echo "&nbsp;&nbsp;&nbsp;( {$allCustInfo['ChildPlacedOutStandardGroup_Details'][0]['Value']} )";
    		        }
    		    }else if($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'N'){
    		        echo kis_ui::displayTableField( $kis_lang['Admission']['no2'] );
    		    }else{
    		        echo kis_ui::displayTableField( '' );
    		    }
    		?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['ChildSpecialClassTalent']?>
    	</td>
    	<td>
    		<?php
    		    if($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'Y'){
    		        echo kis_ui::displayTableField( $kis_lang['Admission']['yes2'] );
    		        if($allCustInfo['ChildSpecialClassTalent_Details'][0]['Value']){
        		        echo "&nbsp;&nbsp;&nbsp;( {$allCustInfo['ChildSpecialClassTalent_Details'][0]['Value']} )";
    		        }
    		    }else if($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'N'){
    		        echo kis_ui::displayTableField( $kis_lang['Admission']['no2'] );
    		    }else{
    		        echo kis_ui::displayTableField( '' );
    		    }
    		?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['ChildEducationalPsychologist']?>
    	</td>
    	<td>
    		<?php
    		    if($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'Y'){
    		        echo kis_ui::displayTableField( $kis_lang['Admission']['yes2'] );
    		        if($allCustInfo['ChildEducationalPsychologist_Details'][0]['Value']){
        		        echo "&nbsp;&nbsp;&nbsp;( {$allCustInfo['ChildEducationalPsychologist_Details'][0]['Value']} )";
    		        }
    		    }else if($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'N'){
    		        echo kis_ui::displayTableField( $kis_lang['Admission']['no2'] );
    		    }else{
    		        echo kis_ui::displayTableField( '' );
    		    }
    		?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['IndicateStudentFavouriteSubject']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField( $allCustInfo['StudentFavouriteSubject'][0]['Value'] ) ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['IndicateStudentSuccessfulSubject']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField( $allCustInfo['StudentSuccessfulSubject'][0]['Value'] ) ?>
    	</td>
    </tr>
    
    <tr>
    	<td class="field_title">
    		<?=$kis_lang['Admission']['PICLC']['IndicateStudentChallengingSubject']?>
    	</td>
    	<td>
    		<?= kis_ui::displayTableField( $allCustInfo['StudentChallengingSubject'][0]['Value'] ) ?>
    	</td>
    </tr>
</table>-->