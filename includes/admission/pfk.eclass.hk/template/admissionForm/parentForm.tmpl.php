<h1>
    <?=$Lang['Admission']['PICLC']['pgInformation']?>
</h1>
<table class="form_table parentInfo" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:70%">
        <!--<col style="width:23%">
        <col style="width:24%">-->
    </colgroup>
    <!--tr>
    	<td>
    		<span>If no information, e.g. single parent family, fill in '<font style="color:red;">N.A.</font>'</span>
    		<br/>
    		<span>如沒有資料，例如單親家庭，請輸入 '<font style="color:red;">N.A.</font>'</span>
    	</td>
    </tr-->
	<tr>
		<td class="field_title"><?=$star ?>Relationship</td>
		<td style="background:#f2f4d8">
			<?php if($IsConfirm){ ?>
    			<?=$Lang['Admission']['PG_Type'][$formData['PG_Type']] ?>
			<?php }else{ ?> 
				<input type="radio" value="F" id="PG_Type_F" name="PG_Type" <?=trim($ParentInfo['F']['EnglishSurname']) != ''?'checked':'' ?>>
				<label for="PG_Type_F"><?=$Lang['Admission']['PG_Type']['F']?></label>
				<input type="radio" value="M" id="PG_Type_M" name="PG_Type" <?=trim($ParentInfo['M']['EnglishSurname']) != ''?'checked':'' ?>>
				<label for="PG_Type_M"><?=$Lang['Admission']['PG_Type']['M']?></label>
				<input type="radio" value="G" id="PG_Type_G" name="PG_Type" <?=trim($ParentInfo['G']['EnglishSurname']) != ''?'checked':'' ?>>
				<label for="PG_Type_G"><?=$Lang['Admission']['PG_Type']['G']?></label>
			<?php } ?>
		</td>
		<!--<td class="form_guardian_field"><?=$Lang['Admission']['PG_Type']['M']?></td>
		<td class="form_guardian_field"><?=$Lang['Admission']['PG_Type']['G']?></td>-->
	</tr>
	
	
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['englishname']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_surname = "EnglishSurname";
		    $formName_surname = "G{$index}EnglishSurname";
		    $fieldName_firstName = "EnglishFirstName";
		    $formName_firstName = "G{$index}EnglishFirstName";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){ ?>
        			<?=$formData[$formName_surname] ?>, <?=$formData[$formName_firstName] ?>
				<?php }else{ ?> 
        			<div class="col2div">
                		<label for="<?=$formName_surname ?>">
                    		<?=$Lang['Admission']['PICLC']['surname'] ?>
                		</label>
            			<input name="<?=$formName_surname ?>" type="text" id="<?=$formName_surname ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_surname] ?>" data-parent="<?=$index ?>" />
            		</div>
            		&nbsp;
        			<div class="col2div">
                		<label for="<?=$formName_firstName ?>">
                    		<?=$Lang['Admission']['PICLC']['firstName'] ?>
                		</label>
            			<input name="<?=$formName_firstName ?>" type="text" id="<?=$formName_firstName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_firstName] ?>" data-parent="<?=$index ?>" />
            		</div>   			
				<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['chinesename']?> 
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_surname = "ChineseSurname";
		    $formName_surname = "G{$index}ChineseSurname";
		    $fieldName_firstName = "ChineseFirstName";
		    $formName_firstName = "G{$index}ChineseFirstName";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){ ?>
        			<?=$formData[$formName_surname] ?><?=$formData[$formName_firstName] ?>
				<?php }else{ ?> 
        			<div class="col2div">
                		<label for="<?=$formName_surname ?>">
                    		<?=$Lang['Admission']['PICLC']['surname'] ?>
                		</label>
            			<input name="<?=$formName_surname ?>" type="text" id="<?=$formName_surname ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_surname] ?>" data-parent="<?=$index ?>" />
            		</div>
            		&nbsp;
        			<div class="col2div">
                		<label for="<?=$formName_firstName ?>">
                    		<?=$Lang['Admission']['PICLC']['firstName'] ?>
                		</label>
            			<input name="<?=$formName_firstName ?>" type="text" id="<?=$formName_firstName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_firstName] ?>" data-parent="<?=$index ?>" />
            		</div>   			
				<?php } ?>
    		</td>
		<?php
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['nationality']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "Nationality";
		    $formName = "G{$index}Nationality";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['nativePlace']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $formName = "G{$index}NativePlace";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$allCustInfo[$formName][0]['Value'] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['employer']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "Company";
		    $formName = "G{$index}Company";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['position']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "JobPosition";
		    $formName = "G{$index}JobPosition";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['businessAddress']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "OfficeAddress";
		    $formName = "G{$index}OfficeAddress";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['businessPhone']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "OfficeTelNo";
		    $formName = "G{$index}OfficeTelNo";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		Mobile Phone Number
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "Mobile";
		    $formName = "G{$index}Mobile";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['email']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "Email";
		    $formName = "G{$index}Email";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['WeChatId']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "Email";
		    $formName = "G{$index}WeChatID";
		    $displayNone = '';
		    if($index > 1){
		    	//$displayNone = ';display:none';
		    	break;
		    }
		?>
    		<td class="td_g<?=$index ?>" style="background:#f2f4d8<?=$displayNone?>">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$allCustInfo[$formName][0]['Value'] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
		
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['AnnualHouseholdIncome']?>
		</td>
		<td>
    		<?php if($IsConfirm){
    		    echo $Lang['Admission']['PICLC']['AnnualHouseholdIncomeType'][ $formData['AnnualHouseholdIncome'] ];
    		}else{?>
    			<select id="AnnualHouseholdIncome" name="AnnualHouseholdIncome">
        			<?php 
        			foreach($admission_cfg['AnnualHouseholdIncome'] as $incomeType){ 
        			    $selected = ($allCustInfo['AnnualHouseholdIncome'][0]['Value'] == $incomeType)? ' selected' : '';
        			?>
        				<option value="<?=$incomeType ?>" <?=$selected ?> >
	        				<?=$Lang['Admission']['PICLC']['AnnualHouseholdIncomeType'][$incomeType] ?>
        				</option>
    				<?php 
        			} 
        			?>
    			</select>
    		<?php } ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['PICLC']['invoiceTo']?>
		</td>
		<td>
    		<?php if($IsConfirm){
    		    echo $formData['InvoiceTo'];
    		}else{?>
    			<input name="InvoiceTo" type="text" id="InvoiceTo" class="textboxtext" value="<?=$allCustInfo['InvoiceTo'][0]['Value'] ?>" />
    		<?php } ?>
		</td>
	</tr>
	
</table>
