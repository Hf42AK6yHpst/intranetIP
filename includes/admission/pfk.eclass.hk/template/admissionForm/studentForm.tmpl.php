<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}
.col2div{
    display:inline-block;
    /*width:48%;*/
}
@media (min-width: 768px) and (max-width: 991.98px) { 
    .col2div{
        width:100%;
    }
}

.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>

<table class="form_table" style="font-size: 13px">
    <colgroup>
    	<col width="30%">
    	<col width="20%">
    	<col width="30%">
    	<col width="20%">
    </colgroup>
    
    <tbody>
        <tr>	
           	<td class="field_title">
           		<?=$star ?>
        		<?=$Lang['Admission']['PFK']['school']?>
        	</td>
        	<td colspan="3">
        		<?php
        		if($IsConfirm){
        		    $firstChoice = $formData['SchoolFirstChoice'];
        		    $secondChoice = $formData['SchoolSecondChoice'];
        		    $thirdChoice = $formData['SchoolThirdChoice'];
    		    ?>
    		    	<label style="width: 120px;display: inline-block;">
        	    		<?=$Lang['Admission']['PFK']['firstChoice'] ?>:
        	    	</label>
        	    	<?= ($firstChoice != ' -- ')?$Lang['Admission']['PFK']['schoolType'][$firstChoice]:$firstChoice ?>
        	    	<br />
        	    	
    		    	<label style="width: 120px;display: inline-block;">
        	    		<?=$Lang['Admission']['PFK']['secondChoice'] ?>:
    	    		</label>
        	    	<?= ($secondChoice != ' -- ')?$Lang['Admission']['PFK']['schoolType'][$secondChoice]:$secondChoice ?>
        	    	<br />
        	    	
    		    	<label style="width: 120px;display: inline-block;">
        	    		<?=$Lang['Admission']['PFK']['thirdChoice'] ?>:
    	    		</label>
        	    	<?= ($thirdChoice != ' -- ')?$Lang['Admission']['PFK']['schoolType'][$thirdChoice]:$thirdChoice ?>
                <?php
        		}else{
        		?>
        			<div>
        				<label for="schoolFirstChoice" style="width: 100px;display: inline-block;">
        					<?=$Lang['Admission']['PFK']['firstChoice'] ?>
        				</label>
        				<select id="schoolFirstChoice" name="SchoolFirstChoice">
        					<option value=""> - <?=$Lang['Admission']['pleaseSelect'] ?> - </option>
            				<?php 
            				foreach((array)$schoolArr as $type): 
            				?>
            					<option value="<?=$type ?>"><?=$Lang['Admission']['PFK']['schoolType'][$type] ?></option>
        					<?php 
        					endforeach; 
        					?>
        				</select>
        			</div>
        			<div style="margin-top: 5px;">
        				<label for="schoolSecondChoice" style="width: 100px;display: inline-block;">
        					<?=$Lang['Admission']['PFK']['secondChoice'] ?>
        				</label>
        				<select id="schoolSecondChoice" name="SchoolSecondChoice">
        					<option value=""> - <?=$Lang['Admission']['pleaseSelect'] ?> - </option>
            				<?php 
            				foreach((array)$schoolArr as $type): 
            				?>
            					<option value="<?=$type ?>"><?=$Lang['Admission']['PFK']['schoolType'][$type] ?></option>
        					<?php 
        					endforeach; 
        					?>
        				</select>
        			</div>
        			<div style="margin-top: 5px;">
        				<label for="schoolThirdChoice" style="width: 100px;display: inline-block;">
        					<?=$Lang['Admission']['PFK']['thirdChoice'] ?>
        				</label>
        				<select id="schoolThirdChoice" name="SchoolThirdChoice">
        					<option value=""> - <?=$Lang['Admission']['pleaseSelect'] ?> - </option>
            				<?php 
            				foreach((array)$schoolArr as $type): 
            				?>
            					<option value="<?=$type ?>"><?=$Lang['Admission']['PFK']['schoolType'][$type] ?></option>
        					<?php 
        					endforeach; 
        					?>
        				</select>
        			</div>
        		<?php
        		}
        		?>
        	</td>
        </tr>
    </tbody>
</table>

<h1>
    <?=$Lang['Admission']['studentInfo']?>
</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['englishname']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentEnglishSurname']?>, <?=$formData['StudentEnglishFirstName']?>
		<?php }else{ ?>
    		<div class="col2div">
        		<label for="StudentEnglishSurname">
            		<?=$Lang['Admission']['PICLC']['surname'] ?>
        		</label>
    			<input name="StudentEnglishSurname" type="text" id="StudentEnglishSurname" class="textboxtext"  value="<?=$StudentInfo['EnglishSurname']?>"/>
    		</div>
    		&nbsp;
    		<div class="col2div">
        		<label for="StudentEnglishFirstName">
            		<?=$Lang['Admission']['PICLC']['firstName'] ?>
                </label>
    			<input name="StudentEnglishFirstName" type="text" id="StudentEnglishFirstName" class="textboxtext"  value="<?=$StudentInfo['EnglishFirstName']?>"/>
    		</div>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['chinesename']?> <font color="blue"><br/>(<?=$Lang['Admission']['inchinesecharacters'] ?>)</font>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentChineseSurname']?><?=$formData['StudentChineseFirstName']?>
		<?php }else{ ?>
    		<div class="col2div">
        		<label for="StudentChineseSurname">
            		<?=$Lang['Admission']['PICLC']['surname'] ?>
        		</label>
    			<input name="StudentChineseSurname" type="text" id="StudentChineseSurname" class="textboxtext"  value="<?=$StudentInfo['ChineseSurname']?>"/>
    		</div>
    		&nbsp;
    		<div class="col2div">
        		<label for="StudentChineseFirstName">
            		<?=$Lang['Admission']['PICLC']['firstName'] ?>
                </label>
    			<input name="StudentChineseFirstName" type="text" id="StudentChineseFirstName" class="textboxtext"  value="<?=$StudentInfo['ChineseFirstName']?>"/>
    		</div>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['dateofbirth']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input 
				type="text" 
				class="textboxtext datepicker" 
				placeholder="YYYY-MM-DD" 
				maxlength="10" 
				size="15"
				style="width: 110px;"
				 
				id="StudentDateOfBirth" 
				name="StudentDateOfBirth" 
				value="<?=$StudentInfo['DOB'] ?>" 
			/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$Lang['Admission']['gender']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['StudentGender'])? $Lang['Admission']['genderType'][$formData['StudentGender']] : ' -- ' ?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" <?=$StudentInfo['Gender'] == "M"?'checked':'' ?>>
			<label for="StudentGender1"><?=$Lang['Admission']['genderType']['M']?></label>
			<br/>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender" <?=$StudentInfo['Gender'] == "F"?'checked':'' ?>>
			<label for="StudentGender2"><?=$Lang['Admission']['genderType']['F']?></label>
		<?php } ?>
	</td>
</tr>


<tr>
	<td rowspan="2" class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['PICLC']['birthCertNo']?> <font color="blue"><br/>(<?=$Lang['Admission']['SHCK']['birthCertNoHint'] ?>)</font>
		<br/><br/>
		<?=$Lang['Admission']['PICLC']['PlaceOfIssue']?>
	</td>
	<td rowspan="2">
		<?php 
		if($IsConfirm){
		    echo $formData['StudentBirthCertNo'];
		    echo '<br /><br /><br/>';
		    echo $formData['StudentRegistedResidence'];
		}else{ 
		?>
			<table>
				<tr>
					<td>
            			<?php if($BirthCertNo){ ?>
            				<?php if($IsUpdate){ ?>
            					<input name="StudentBirthCertNo" value="<?=$BirthCertNo ?>" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none;"/>
            				<?php }else{ ?>
            					<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="" value="<?=$BirthCertNo?>"/>
            				<?php } ?>
            			<?php }else{ ?>
                			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="" value="<?=$StudentInfo['BirthCertNo'] ?>"/>
            			<?php } ?>
            			<br/><br/>
            			<?php
						if($IsConfirm){
				            echo $formData['StudentRegistedResidence'];
						}else{
						?>
							<input id="StudentRegistedResidence" name="StudentRegistedResidence" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirth'] ?>" />
						<?php
						}
						?>
        			</td>
				</tr>
			</table>

		<?php 
		} 
		?>
	</td>
	
   	<td class="field_title">
		<?=$Lang['Admission']['nationality']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['StudentCounty'];
		}else{
		?>
			<input id="StudentCounty" name="StudentCounty" class="textboxtext inputselect" value="<?=$StudentInfo['Nationality'] ?>" />
		<?php
		}
		?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['nativePlace']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentNativePlace'];
		}else{
		?>
			<input id="StudentNativePlace" name="StudentNativePlace" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirthOther'] ?>" />
		<?php
		}
		?>
	</td>
</tr>
<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['PICLC']['homeAddress']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentAddress'];
		}else{
		?>
			<input id="StudentAddress" name="StudentAddress" class="textboxtext" value="<?=$StudentInfo['Address'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<!--<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['PICLC']['registedResidence']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['StudentRegistedResidence'];
		}else{
		?>
			<input id="StudentRegistedResidence" name="StudentRegistedResidence" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirth'] ?>" />
		<?php
		}
		?>
	</td>
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['PICLC']['nativePlace']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentNativePlace'];
		}else{
		?>
			<input id="StudentNativePlace" name="StudentNativePlace" class="textboxtext" value="<?=$StudentInfo['PlaceOfBirthOther'] ?>" />
		<?php
		}
		?>
	</td>
</tr>-->

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['contactEmail']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentEmail'];
		}else{
		?>
			<input id="StudentEmail" name="StudentEmail" class="textboxtext" value="<?=$StudentInfo['Email'] ?>" />
		<?php
		}
		?>
	</td>
</tr>
</table>

<?php if($classLevelID){ ?>
	<h1>
    	<?=$Lang['Admission']['applyLevel'] ?>
	</h1>
	<table class="form_table" style="font-size: 13px">
		<tr>
			<td class="field_title">
				<?=$Lang['Admission']['applyLevel'] ?>
				<?=$LangEn['Admission']['applyLevel'] ?>
			</td>
			<td>
				<?=$classLevel ?>
				<input name="sus_status" type="hidden" id="sus_status" class="textboxtext" value="<?=$classLevelID ?>" />		
			</td>
		</tr>
	</table>
<?php } ?>

<script>
$(function(){
	'use strict';

	$('#StudentBirthCertType').change(function(){
		$('.docUpload').hide();
		$('.docUpload_' + $(this).val()).show();
	}).change();
});
</script>