<h1>
    <?=$Lang['Admission']['PICLC']['studentAcademicBackground']?>
</h1>
<table class="form_table" style="font-size: 13px">
    <colgroup>
        <col style="width:20%">
        <col style="width:10px">
        <col style="width:400px">
        <col style="width: 190px">
        <col style="width: 170px">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PFK']['school']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['DatesAttended']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PICLC']['LanguageOfInstruction']?></td>
	</tr>
	
	<?php for($i=0;$i<$lac::STUDENT_ACADEMIC_SCHOOL_COUNT;$i++){ ?>
    	<tr>
    		<?php if($i == 0){ ?>
        		<td class="field_title"  rowspan="3" style="width:20%">
           			<?=$Lang['Admission']['PICLC']['previousSchool'] ?>
        		</td>
    		<?php } ?>
    	
    		<td>
       			<?=$i+1?>)
    		</td>
    		
    		<td class="form_guardian_field" style="text-align:right !important">
    			<?=$Lang['Admission']['name']?>: 
        		<?php 
        		$formName = "StudentSchool{$i}_Name";
        		if($IsConfirm){
        		    echo $formData[$formName].'<br/>';
        		}else{
        		?>
        			<input style="width: 285px;" name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['NameOfSchool'] ?>"/><br/>
        		<?php 
        		} 
        		?>
        		<?=$Lang['Admission']['address']?>: 
        		<?php 
        		$formName = "StudentSchool{$i}_Location";
        		if($IsConfirm){
        		    echo $formData[$formName];
        		}else{
        		?>
        			<input style="width: 285px;" name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['SchoolAddress'] ?>"/>
        		<?php 
        		} 
        		?>
    		</td>
    		
    		<td class="form_guardian_field" style="text-align:right !important">
        		<?php 
        		$formName = "StudentSchool{$i}_DatesAttended";
        		if($IsConfirm){
        		    $from = $formData["{$formName}_From"];
        		    $to = $formData["{$formName}_To"];
        		    echo ($formData["{$formName}_From"] != ' -- ' && $formData["{$formName}_To"] != ' -- ')? "{$from} ~ {$to}" : ' -- ';
        		}else{
        		?>
            		<div class="col2div" style="width: 195px;">
            			<?=$Lang['Admission']['from'] ?>:
        				<input 
            				type="text" 
            				class="textboxtext datepicker" 
            				placeholder="YYYY-MM-DD" 
            				maxlength="10" 
            				size="15"
            				style="width: 110px;"
            				 
            				id="<?=$formName ?>_From" 
            				name="<?=$formName ?>_From" 
            				value="<?=$StudentPrevSchoolInfo[$i]['StartDate'] ?>" 
        				/>
            		</div>
            		<div class="col2div" style="width: 195px;">
            			<?=$Lang['Admission']['to'] ?>:
        				<input 
            				type="text" 
            				class="textboxtext datepicker" 
            				placeholder="YYYY-MM-DD" 
            				maxlength="10" 
            				size="15"
            				style="width: 110px;"
            				 
            				id="<?=$formName ?>_To" 
            				name="<?=$formName ?>_To" 
            				value="<?=$StudentPrevSchoolInfo[$i]['EndDate'] ?>" 
        				/>
            		</div>
        		<?php 
        		}
        		?>
    		</td>
    		
    		<td class="form_guardian_field">
        		<?php 
        		$formName = "StudentSchool{$i}_Language";
        		if($IsConfirm){
        		    echo $formData[$formName];
        		}else{
        		?>
        			<input style="max-width: 170px;" name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$StudentPrevSchoolInfo[$i]['LangSpoken'] ?>"/>
        		<?php 
        		} 
        		?>
    		</td>
    	</tr>
	<?php } ?>
</table>


<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="70%">
</colgroup>

<!--<tr>
	<td class="field_title">
		
		<?=$Lang['Admission']['PICLC']['ChildPlacedOutStandardGroup']?> 
		<br />
		<?=$LangEn['Admission']['PICLC']['ChildPlacedOutStandardGroup']?> 
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		    if($formData['ChildPlacedOutStandardGroup'] == 'Y'){
		        echo "{$Lang['Admission']['yes2']}<br />{$formData['ChildPlacedOutStandardGroup_Details']}";
		    }else if($formData['ChildPlacedOutStandardGroup'] == 'N'){
		        echo "{$Lang['Admission']['no2']}";
		    }else{
		        echo ' -- ';
		    }
		}else{ 
		    $checkedY = ($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'Y')? 'checked' : '';
		    $checkedN = ($allCustInfo['ChildPlacedOutStandardGroup'][0]['Value'] == 'N')? 'checked' : '';
		?>
			<div>
    			<input type="radio" value="Y" id="ChildPlacedOutStandardGroup_Y" name="ChildPlacedOutStandardGroup" <?=$checkedY ?>>
    			<label for="ChildPlacedOutStandardGroup_Y"><?=$Lang['Admission']['yes2'] ?></label>
    			
    			<label for="ChildPlacedOutStandardGroup_Details" style="margin-left: 10px;"><?=$Lang['Admission']['PICLC']['Details'] ?></label>
    			<input name="ChildPlacedOutStandardGroup_Details" type="text" id="ChildPlacedOutStandardGroup_Details" class="textboxtext" value="<?=$allCustInfo['ChildPlacedOutStandardGroup_Details'][0]['Value'] ?>" style="width: 80%;"/>
			</div>
			<div>
    			<input type="radio" value="N" id="ChildPlacedOutStandardGroup_N" name="ChildPlacedOutStandardGroup" <?=$checkedN ?>>
    			<label for="ChildPlacedOutStandardGroup_N"><?=$Lang['Admission']['no2'] ?></label>
			</div>
		<?php 
		} 
		?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['ChildSpecialClassTalent']?> 
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		    if($formData['ChildSpecialClassTalent'] == 'Y'){
		        echo "{$Lang['Admission']['yes2']}<br />{$formData['ChildSpecialClassTalent_Details']}";
		    }elseif($formData['ChildSpecialClassTalent'] == 'N'){
		        echo $Lang['Admission']['no2'];
		    }else{
		        echo ' -- ';
		    }
		}else{ 
		    $checkedY = ($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'Y')? 'checked' : '';
		    $checkedN = ($allCustInfo['ChildSpecialClassTalent'][0]['Value'] == 'N')? 'checked' : '';
		?>
			<div>
    			<input type="radio" value="Y" id="ChildSpecialClassTalent_Y" name="ChildSpecialClassTalent" <?=$checkedY ?>>
    			<label for="ChildSpecialClassTalent_Y"><?=$Lang['Admission']['yes2'] ?></label>
    			
    			<label for="ChildSpecialClassTalent_Details" style="margin-left: 10px;"><?=$Lang['Admission']['PICLC']['Details'] ?></label>
    			<input name="ChildSpecialClassTalent_Details" type="text" id="ChildSpecialClassTalent_Details" class="textboxtext" value="<?=$allCustInfo['ChildSpecialClassTalent_Details'][0]['Value'] ?>" style="width: 80%;"/>
			</div>
			<div>
    			<input type="radio" value="N" id="ChildSpecialClassTalent_N" name="ChildSpecialClassTalent" <?=$checkedN ?>>
    			<label for="ChildSpecialClassTalent_N"><?=$Lang['Admission']['no2'] ?></label>
			</div>
		<?php 
		} 
		?>
	</td>
</tr>

<tr>
	<td class="field_title">
		
		<?=$Lang['Admission']['PICLC']['ChildEducationalPsychologist']?> 
		<br />
		<?=$LangEn['Admission']['PICLC']['ChildEducationalPsychologist']?> 
	</td>
	<td>
		<?php 
		if($IsConfirm){ 
		    if($formData['ChildEducationalPsychologist'] == 'Y'){
		        echo "{$Lang['Admission']['yes2']}<br />{$formData['ChildEducationalPsychologist_Details']}";
		    }elseif($formData['ChildEducationalPsychologist'] == 'N'){
		        echo $Lang['Admission']['no2'];
		    }else{
		        echo ' -- ';
		    }
		}else{ 
		    $checkedY = ($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'Y')? 'checked' : '';
		    $checkedN = ($allCustInfo['ChildEducationalPsychologist'][0]['Value'] == 'N')? 'checked' : '';
		?>
			<div>
    			<input type="radio" value="Y" id="ChildEducationalPsychologist_Y" name="ChildEducationalPsychologist" <?=$checkedY ?>>
    			<label for="ChildEducationalPsychologist_Y"><?=$Lang['Admission']['yes2'] ?></label>
    			
    			<label for="ChildEducationalPsychologist_Details" style="margin-left: 10px;">
        			<?=$Lang['Admission']['PICLC']['Details'] ?>
    			</label>
    			<input name="ChildEducationalPsychologist_Details" type="text" id="ChildEducationalPsychologist_Details" class="textboxtext" value="<?=$allCustInfo['ChildEducationalPsychologist_Details'][0]['Value'] ?>" style="width: 80%;"/>
    			<label for="ChildEducationalPsychologist_Details" style="margin-left: 115px;">
        			(
        				<?=$Lang['Admission']['PICLC']['ChildEducationalPsychologistReport'] ?>
        			)
    			</label>
			</div>
			<div>
    			<input type="radio" value="N" id="ChildEducationalPsychologist_N" name="ChildEducationalPsychologist" <?=$checkedN ?>>
    			<label for="ChildEducationalPsychologist_N"><?=$Lang['Admission']['no2'] ?></label>
			</div>
		<?php 
		} 
		?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['IndicateStudentFavouriteSubject']?>  
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentFavouriteSubject']?>
		<?php }else{ ?>
			<input name="StudentFavouriteSubject" type="text" id="StudentFavouriteSubject" class="textboxtext" value="<?=$allCustInfo['StudentFavouriteSubject'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['IndicateStudentSuccessfulSubject']?> 
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentSuccessfulSubject']?>
		<?php }else{ ?>
			<input name="StudentSuccessfulSubject" type="text" id="StudentSuccessfulSubject" class="textboxtext" value="<?=$allCustInfo['StudentSuccessfulSubject'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['PICLC']['IndicateStudentChallengingSubject']?>  
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentChallengingSubject']?>
		<?php }else{ ?>
			<input name="StudentChallengingSubject" type="text" id="StudentChallengingSubject" class="textboxtext" value="<?=$allCustInfo['StudentChallengingSubject'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>-->
</table>