<?php
global $libkis_admission;

$SiblingInfo = $libkis_admission->getApplicationSibling($schoolYearID, '', $applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>
<style>
.whereToKnowOptionDiv {
	margin-bottom: 5px;
}

.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>

<table class="form_table" style="font-size: 13px">
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['howHearSchool']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['HowHearSchool'][0]['Value'] )?>
    	</td>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['haveParticipatedActivity']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['HaveParticipatedActivity'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeExpertise']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['DescribeExpertise'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['whyChooseSchool']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['WhyChooseSchool'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['hasSibling']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['HasSibling'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeChildPersonality']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['DescribeChildPersonality'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeChildRoutine']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['DescribeChildRoutine'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeChildRelationship']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['DescribeChildRelationship'][0]['Value'] )?>
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['medicalConcern']?>
    	</td>
		<td>
			<?= kis_ui::displayTableField( $allCustInfo['MedicalConcern'][0]['Value'] )?>
    	</td>
	</tr>
	
	<table class="form_table">
	    <colgroup>
	        <col style="width:40%">
	        <col style="width:30%">
	        <col style="width:30%">
	    </colgroup>
	    <tr>
			<td colspan="3" class="form_guardian_head"><?=$kis_lang['Admission']['FH']['hasFormalAssessment'] ?></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['speechTherapy'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsSpeechTherapy'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsSpeechTherapy'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['SpeechTherapyDate'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['occupationalTherapy'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsOccupationalTherapy'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsOccupationalTherapy'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['OccupationalTherapyDate'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['psychoEducationalAssessment'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['PsychoEducationalAssessmentDate'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['psychologicalAssessmentCounselling'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['remedialInstructionTutoring'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsRemedialInstructionTutoring'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsRemedialInstructionTutoring'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['RemedialInstructionTutoringDate'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['individualizedProgramPlan'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IndividualizedProgramPlanDate'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= kis_ui::displayTableField($allCustInfo['OtherFormalAssessmentName'][0]['Value'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "Y" ? $kis_lang['Admission']['yes'] : ($allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "N" ? $kis_lang['Admission']['no'] : ''))?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['OtherFormalAssessmentDate'][0]['Value'])?>
			</td>
		</tr>
	</table>
</table>