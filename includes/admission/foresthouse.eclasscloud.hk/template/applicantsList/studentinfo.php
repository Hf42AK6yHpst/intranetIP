<?php
global $libkis_admission;

$StudentInfo = $applicationInfo;
$StudentPrevSchoolInfo = $libkis_admission->getApplicationPrevSchoolInfo($schoolYearID,'',$applicationInfo['applicationID'],'');
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

$applySchoolId = $allCustInfo['ApplySchool'][0]['Value'];
$applySchoolType = $admission_cfg['SchoolType'][$applySchoolId];

foreach($StudentPrevSchoolInfo as $index => $school){
    if($school['StartDate'] == '0000-00-00'){
        $StudentPrevSchoolInfo[$index]['StartDate'] = '';
    }else{
        $StudentPrevSchoolInfo[$index]['StartDate'] = date("m/y", strtotime($school['StartDate']));
    }
    if($school['EndDate'] == '0000-00-00'){
        $StudentPrevSchoolInfo[$index]['EndDate'] = '';
    }else{
        $StudentPrevSchoolInfo[$index]['EndDate'] = date("m/y", strtotime($school['EndDate']));
    }
}
?>
<table class="form_table">
	<tbody>
		<tr>
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['childsName']?>
			</td>
			<td width="40%">
				<?=kis_ui::displayTableField($StudentInfo['EnglishSurname'])?>, <?=kis_ui::displayTableField($StudentInfo['EnglishFirstName'])?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin: 0px;">
					<img
						src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>" />
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['OtherName'] ?>
    					(<?=$kis_lang['Admission']['FH']['IncludingChineseName'] ?>)
			</td>
			<td>
				<?=kis_ui::displayTableField($StudentInfo['ChineseName'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['gender']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($kis_lang['Admission']['genderType'][ $StudentInfo['Gender'] ])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['FH']['dateofbirth']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['DOB'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['FH']['birthcertno']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['BirthCertNo'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['FH']['placeofbirth']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['PlaceOfBirth'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['nationality']?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Nationality'])?>
			</td>
		</tr>
	</table>
	<table class="form_table">
	    <colgroup>
	        <col style="width:30%">
	        <col style="width:23%">
	        <col style="width:23%">
	        <col style="width:24%">
	    </colgroup>
	    <tr>
			<td>&nbsp;</td>
			<td class="form_guardian_head"><?=$kis_lang['Admission']['FH']['first'] ?></td>
			<td class="form_guardian_head"><?=$kis_lang['Admission']['FH']['second'] ?></td>
			<td class="form_guardian_head"><?=$kis_lang['Admission']['FH']['third'] ?></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['langspokenathome'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['StudentFirstLanguage'][0]['Value'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['StudentSecondLanguage'][0]['Value'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['StudentThirdLanguage'][0]['Value'])?>
			</td>
		</tr>
	</table>
	<table class="form_table">
	    <colgroup>
	        <col style="width:30%">
	        <col style="width:23%">
	        <col style="width:23%">
	        <col style="width:24%">
	    </colgroup>
	    <tr>
			<td>&nbsp;</td>
			<td class="form_guardian_head"><?=$kis_lang['Admission']['FH']['schoolName'] ?></td>
			<td class="form_guardian_head"><?=$kis_lang['Admission']['from'] ?></td>
			<td class="form_guardian_head"><?=$kis_lang['Admission']['to'] ?></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['previousSchool'] ?> (1)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentPrevSchoolInfo[0]['NameOfSchool'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentPrevSchoolInfo[0]['StartDate'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentPrevSchoolInfo[0]['EndDate'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['previousSchool'] ?> (2)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentPrevSchoolInfo[1]['NameOfSchool'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentPrevSchoolInfo[1]['StartDate'])?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentPrevSchoolInfo[1]['EndDate'])?>
			</td>
		</tr>
	</table>
<table class="form_table">
	<tbody>
		<tr>
			<td width="30%" class="field_title">
				<?= $kis_lang['Admission']['FH']['enrolmentToCommence']?>
			</td>
			<td width="70%" colspan="2">
				<?= kis_ui::displayTableField($allCustInfo['StudentEnrolmentToCommence'][0]['Value'])?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['document']?>
			</td>
			<td colspan="2">
				<?php
    $attachmentAry = array();
    for ($i = 0; $i < sizeof($attachmentSettings); $i ++) {
        
        // #### Get attachment name START #### //
        $attachment_name = $attachmentSettings[$i]['AttachmentName'];
        
        // ## Multi-lang START ## //
        $attachmentNameDisplay = $attachment_name;
        if ($admission_cfg['MultipleLang']) {
            foreach ($admission_cfg['Lang'] as $index => $kis_lang) {
                if ($kis_lang == $intranet_session_language) {
                    $attachmentNameDisplay = $attachmentSettings[$i]["AttachmentName{$index}"];
                }
            }
        }
        // ## Multi-lang END ## //

        // ## Cust-lang START ## //
        if ($kis_lang['Admission']['PICLC']['documentUpload'][$StudentInfo['BirthCertTypeOther']][$i]) {
            $attachmentNameDisplay = $kis_lang['Admission']['PICLC']['documentUpload'][$StudentInfo['BirthCertTypeOther']][$i];
        }
        // ## Cust-lang END ## //
        // #### Get attachment name END #### //
        
        $_filePath = $attachmentList[$attachment_name]['link'];
        if ($_filePath) {
            // $attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$kis_lang['Admission'][$_type].'</a>';
            $attachmentAry[] = '<a href="' . $_filePath . '" target="_blank">' . $attachmentNameDisplay . '</a>';
        }
    }
    ?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>
	</tbody>
</table>