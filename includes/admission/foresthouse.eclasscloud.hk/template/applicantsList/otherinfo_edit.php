<?php
global $libkis_admission;

$SiblingInfo = $libkis_admission->getApplicationSibling($schoolYearID, '', $applicationInfo['applicationID']);
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);

?>
<style>
.whereToKnowOptionDiv {
	margin-bottom: 5px;
}

.form_guardian_head, .form_guardian_field {
	text-align: center !important;
}
</style>
<table class="form_table" style="font-size: 13px">
	<colgroup>
		<col width="30%">
		<col width="70%">
	</colgroup>
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['howHearSchool']?>
    	</td>
		<td>
			<input name="HowHearSchool" type="text"
				id="HowHearSchool" class="textboxtext"
				value="<?=$allCustInfo['HowHearSchool'][0]['Value']?>" />
    	</td>
	</tr>

	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['haveParticipatedActivity']?>
    	</td>
		<td>
			<input name="HaveParticipatedActivity" type="text"
				id="HaveParticipatedActivity" class="textboxtext"
				value="<?=$allCustInfo['HaveParticipatedActivity'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeExpertise']?>
    	</td>
		<td>
			<input name="DescribeExpertise" type="text"
				id="DescribeExpertise" class="textboxtext"
				value="<?=$allCustInfo['DescribeExpertise'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['whyChooseSchool']?>
    	</td>
		<td>
			<input name="WhyChooseSchool" type="text"
				id="WhyChooseSchool" class="textboxtext"
				value="<?=$allCustInfo['WhyChooseSchool'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['hasSibling']?>
    	</td>
		<td>
			<input name="HasSibling" type="text"
				id="HasSibling" class="textboxtext"
				value="<?=$allCustInfo['HasSibling'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeChildPersonality']?>
    	</td>
		<td>
			<input name="DescribeChildPersonality" type="text"
				id="DescribeChildPersonality" class="textboxtext"
				value="<?=$allCustInfo['DescribeChildPersonality'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeChildRoutine']?>
    	</td>
		<td>
			<input name="DescribeChildRoutine" type="text"
				id="DescribeChildRoutine" class="textboxtext"
				value="<?=$allCustInfo['DescribeChildRoutine'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['describeChildRelationship']?>
    	</td>
		<td>
			<input name="DescribeChildRelationship" type="text"
				id="DescribeChildRelationship" class="textboxtext"
				value="<?=$allCustInfo['DescribeChildRelationship'][0]['Value']?>" />
    	</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$kis_lang['Admission']['FH']['medicalConcern']?>
    	</td>
		<td>
			<input name="MedicalConcern" type="text"
				id="MedicalConcern" class="textboxtext"
				value="<?=$allCustInfo['MedicalConcern'][0]['Value']?>" />
    	</td>
	</tr>
	
	<table class="form_table">
	    <colgroup>
	        <col style="width:40%">
	        <col style="width:30%">
	        <col style="width:30%">
	    </colgroup>
	    <tr>
			<td colspan="3" class="form_guardian_head"><?=$kis_lang['Admission']['FH']['hasFormalAssessment'] ?></td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['speechTherapy'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsSpeechTherapy_Y', 'IsSpeechTherapy', 'Y', ($allCustInfo['IsSpeechTherapy'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsSpeechTherapy_N', 'IsSpeechTherapy', 'N', ($allCustInfo['IsSpeechTherapy'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="SpeechTherapyDate" type="text"
					id="SpeechTherapyDate" class="textboxtext"
					value="<?=$allCustInfo['SpeechTherapyDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['occupationalTherapy'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsOccupationalTherapy_Y', 'IsOccupationalTherapy', 'Y', ($allCustInfo['IsOccupationalTherapy'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsOccupationalTherapy_N', 'IsOccupationalTherapy', 'N', ($allCustInfo['IsOccupationalTherapy'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="OccupationalTherapyDate" type="text"
					id="OccupationalTherapyDate" class="textboxtext"
					value="<?=$allCustInfo['OccupationalTherapyDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['psychoEducationalAssessment'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsPsychoEducationalAssessment_Y', 'IsPsychoEducationalAssessment', 'Y', ($allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsPsychoEducationalAssessment_N', 'IsPsychoEducationalAssessment', 'N', ($allCustInfo['IsPsychoEducationalAssessment'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="PsychoEducationalAssessmentDate" type="text"
					id="PsychoEducationalAssessmentDate" class="textboxtext"
					value="<?=$allCustInfo['PsychoEducationalAssessmentDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['psychologicalAssessmentCounselling'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsPsychologicalAssessmentCounselling_Y', 'IsPsychologicalAssessmentCounselling', 'Y', ($allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsPsychologicalAssessmentCounselling_N', 'IsPsychologicalAssessmentCounselling', 'N', ($allCustInfo['IsPsychologicalAssessmentCounselling'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="PsychologicalAssessmentCounsellingDate" type="text"
					id="PsychologicalAssessmentCounsellingDate" class="textboxtext"
					value="<?=$allCustInfo['PsychologicalAssessmentCounsellingDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['remedialInstructionTutoring'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsRemedialInstructionTutoring_Y', 'IsRemedialInstructionTutoring', 'Y', ($allCustInfo['IsRemedialInstructionTutoring'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsRemedialInstructionTutoring_N', 'IsRemedialInstructionTutoring', 'N', ($allCustInfo['IsRemedialInstructionTutoring'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="RemedialInstructionTutoringDate" type="text"
					id="RemedialInstructionTutoringDate" class="textboxtext"
					value="<?=$allCustInfo['RemedialInstructionTutoringDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['FH']['individualizedProgramPlan'] ?>
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsIndividualizedProgramPlan_Y', 'IsIndividualizedProgramPlan', 'Y', ($allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsIndividualizedProgramPlan_N', 'IsIndividualizedProgramPlan', 'N', ($allCustInfo['IsIndividualizedProgramPlan'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="IndividualizedProgramPlanDate" type="text"
					id="IndividualizedProgramPlanDate" class="textboxtext"
					value="<?=$allCustInfo['IndividualizedProgramPlanDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<input name="OtherFormalAssessmentName" type="text"
					id="OtherFormalAssessmentName" class="textboxtext"
					value="<?=$allCustInfo['OtherFormalAssessmentName'][0]['Value']?>" />
			</td>
			<td>
				<?=$libinterface->Get_Radio_Button('IsOtherFormalAssessment_Y', 'IsOtherFormalAssessment', 'Y', ($allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "Y"), '', $kis_lang['Admission']['yes'])?>
    			<?=$libinterface->Get_Radio_Button('IsOtherFormalAssessment_N', 'IsOtherFormalAssessment', 'N', ($allCustInfo['IsOtherFormalAssessment'][0]['Value'] == "N"), '', $kis_lang['Admission']['no'])?>
			</td>
			<td>
				<input name="OtherFormalAssessmentDate" type="text"
					id="OtherFormalAssessmentDate" class="textboxtext"
					value="<?=$allCustInfo['OtherFormalAssessmentDate'][0]['Value']?>" /> (mm/yy)
			</td>
		</tr>
	</table>
</table>

<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsMMYY(str){
	return str.match(/^(0[123456789]|10|11|12)\/\d{2}$/);
}

function checkValidForm(){
	if(
		$('#SpeechTherapyDate').val().trim()!='' &&
		!checkIsMMYY($('#SpeechTherapyDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#SpeechTherapyDate').focus();
		return false;
	}
	if(
		$('#OccupationalTherapyDate').val().trim()!='' &&
		!checkIsMMYY($('#OccupationalTherapyDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#OccupationalTherapyDate').focus();
		return false;
	}
	if(
		$('#PsychoEducationalAssessmentDate').val().trim()!='' &&
		!checkIsMMYY($('#PsychoEducationalAssessmentDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#PsychoEducationalAssessmentDate').focus();
		return false;
	}
	if(
		$('#PsychologicalAssessmentCounsellingDate').val().trim()!='' &&
		!checkIsMMYY($('#PsychologicalAssessmentCounsellingDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#PsychologicalAssessmentCounsellingDate').focus();
		return false;
	}
	if(
		$('#RemedialInstructionTutoringDate').val().trim()!='' &&
		!checkIsMMYY($('#RemedialInstructionTutoringDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#RemedialInstructionTutoringDate').focus();
		return false;
	}
	if(
		$('#IndividualizedProgramPlanDate').val().trim()!='' &&
		!checkIsMMYY($('#IndividualizedProgramPlanDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#IndividualizedProgramPlanDate').focus();
		return false;
	}
	if(
		$('#OtherFormalAssessmentDate').val().trim()!='' &&
		!checkIsMMYY($('#OtherFormalAssessmentDate').val())
	){
		alert(" <?=$kis_lang['Admission']['FH']['msg']['dateYearFormatRemark']?>");	
		$('#OtherFormalAssessmentDate').focus();
		return false;
	}
	return true;
}

</script>