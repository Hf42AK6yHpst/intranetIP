<section id="docsForm" class="form displaySection display_pageDocsUpload display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$Lang['Admission']['document'] ?>
	</div>
	<div class="sheet">
		<div class="text-inst">
			<?=$Lang['Admission']['msg']['birthCertFormat'] ?>
			<?= ($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1') ?> MB
		</div>
		<div class="item">
    			<div class="itemLabel requiredLabel"><?=$Lang['Admission']['personalPhoto'] ?></div>
				<div class="uploadedFiles">
	    			<div class="uploadedFile" style="display:none">
						<span class="link"></span><span class="icon icon-button fa-trash"></span>
					</div>
				</div>
    			<div class="itemInput itemInput-file">
    				<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" data-multiple-caption="<?=$Lang['Admission']['fileSelected'] ?>" required accept=".gif, .jpeg, .jpg, .png" />
    				<label for="StudentPersonalPhoto"><?=$Lang['Admission']['selectFile'] ?></label>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					<div class="remark remark-warn errFile hide"></div>
    			</div>
    		</div>
		<?php
		foreach($attachment_settings as $index => $attachment_setting):
		    $labelClass = ($attachment_setting['IsOptional'])? '' : 'requiredLabel';
		    $required = ($attachment_setting['IsOptional'])? '' : 'required';
		    $accept = '.gif, .jpeg, .jpg, .png, .pdf';
		?>
    		<div class="item">
    			<div class="itemLabel <?=$labelClass ?>"><?=$attachment_setting['AttachmentName'] ?></div>
				<div class="uploadedFiles">
	    			<div class="uploadedFile" style="display:none">
						<span class="link"></span><span class="icon icon-button fa-trash"></span>
					</div>
				</div>
    			<div class="itemInput itemInput-file">
    				<input type="file" name="OtherFile<?=$index ?>" id="OtherFile<?=$index ?>" data-multiple-caption="<?=$Lang['Admission']['fileSelected'] ?>" <?=$required ?> accept="<?=$accept ?>" />
    				<label for="OtherFile<?=$index ?>"><?=$Lang['Admission']['selectFile'] ?></label>
					<div class="remark remark-warn hide"><?=$Lang['Admission']['required'] ?></div>
					<div class="remark remark-warn errFile hide"></div>
    			</div>
    		</div>
		<?php 
		endforeach;
		?>
		
		<div class="remark">* <?=$Lang['Admission']['requiredFields'] ?></div>
	</div>
</section>

<script>
$(function(){
	'use strict';
	
	$('.uploadedFile .fa-trash').each(function(){
		var $span = $(this),
			$input = $span.parent().parent().next('.itemInput-file').children('input');
			
			$span.on('click', function(e){
				$input.val("");
				$input.change();
			});
	});
	
	$('.itemInput-file input[type="file"]').each(function(){
		var $input = $(this),
			$uploadedFileDiv = $input.parent().prev('.uploadedFiles').children('.uploadedFile'),
			$label = $input.next('label'),
			$link = $uploadedFileDiv.children('.link'),
			linkVal = $link.html();

		$input.on('change', function(e){
			var fileName = '';

			if(this.files && this.files.length > 1)
				fileName = (this.getAttribute('data-multiple-caption') || '' ).replace('{count}', this.files.length );
			else if(e.target.value)
				fileName = e.target.value.split('\\').pop();

			if(fileName){
				//$label.hide();
				$uploadedFileDiv.show();
				$link.html(fileName);
			}
			else{
				//$label.show();
				$uploadedFileDiv.hide();
				$link.html(linkVal);
			}
		});
	});
	
	window.checkDocsForm = (function(lifecycle){
		var isValid = true;
		var $docsForm = $('#docsForm');
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($docsForm);

		var isUpdatePeriod =<?=($allowToUpdate)? 'true' : 'false'; ?>;
		var isTeacherInput =<?= ($lac->isInternalUse($_GET['token']))?1:0 ?>;
		var isOldBrowser = 0;
		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
			isOldBrowser = 1;
		}
	
		var maxFileSize = 1 * 1024 * 1024;
		<?if($admission_cfg['maxUploadSize'] > 0){?>
			maxFileSize =<?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
		<?}?>
		
		var $files = $('#StudentPersonalPhoto, input[type=file][name*=OtherFile]');

		$files.each(function(){
			var teacherIsOptional = !!$(this).data('teacher-is-optional');
			var isOptional = !!$(this).data('is-optional');
	
			if(isTeacherInput && teacherIsOptional){
				isOptional = true;
			}
	
			/**** File size START ****/
			var filesize = ($(this).val()=='')? 0 : $(this)[0].files[0].size;
			if(!isOldBrowser && filesize > maxFileSize){
				if($this.parent().find('.remark-warn .errFile').hasClass('hide')){
					$this.parent().find('.remark-warn .errFile').html('<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>');
					$this.parent().find('.remark-warn .errFile').removeClass('hide');
					focusElement = $this;
				}	
		    	isValid = false;
			}
			/**** File size END ****/
	
			/**** File format START **** /
			var _acceptFormatArr = $(this).attr('accept').split(',');
			var acceptFormatArr = [];
			$.each(_acceptFormatArr, function(){
				acceptFormatArr.push( $.trim(this).substr(1) );
			});
			
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, acceptFormatArr) == -1){
				if($this.parent().find('.remark-warn .errFile').hasClass('hide')){
					$this.parent().find('.remark-warn .errFile').html('<?=$Lang['Admission']['msg']['invalidfileformat']?>');
					$this.parent().find('.remark-warn .errFile').removeClass('hide');
					focusElement = $this;
				}	
		    	isValid = false;
			}
			/**** File format END ****/
		});

		return isValid;
	});

	window.validateFunc['pageDocsUpload'].push(checkDocsForm);
});
</script>