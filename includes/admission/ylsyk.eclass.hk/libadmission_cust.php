<?php
# modifying by: Henry

/********************
 * 
 * Log :
 * Date		2016-05-20 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_cust_base.php");
 
class admission_cust extends admission_cust_base{
	function admission_cust(){
		global $kis_lang, $UserID; //switch $lang for IP/EJ/KIS
		$this->libdb();
		$this->filepath = '/file/admission/';
		$this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
		$this->schoolYearID = $this->getNextSchoolYearID();
		$this->uid = $UserID;
		$this->classLevelAry = $this->getClassLevel();
	}
	
	function testing(){
		return "test";
	}
	function encrypt_attachment($file){
		list($filename,$ext) = explode('.',$file);
	    $timestamp = date("YmdHis");
	    return base64_encode($filename.'_'.$timestamp).'.'.$ext;
	}
	function getNextSchoolYearID(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		$SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField=array('AcademicYearID'),1);
		krsort($SchoolYearIDArr);
		
		$SchoolYearIDArr = array_values($SchoolYearIDArr);
		$currentSchoolYear = Get_Current_Academic_Year_ID();
		$key = array_search($currentSchoolYear, $SchoolYearIDArr);
		if($key>0){
    		return $SchoolYearIDArr[$key-1];
    	}else{
    		return false;
    	}
	}
	function insertApplicationAllInfo($libkis_admission, $Data, $ApplicationID, $isUpdate=0){
		extract($Data);
		$Success = array();
		if($ApplicationID != ""){
			
			if(!$isUpdate)
				$Success[] = $this->insertApplicationStatus($libkis_admission, $ApplicationID);
			
			$Success[] = $this->insertApplicationStudentInfo($Data, $ApplicationID, $isUpdate);			
			
			//Pack Parent Info 
			foreach($this->pg_type as $_key => $_pgType){
				if(!empty($ApplicationID)/*&&!empty(${'G'.($_key+1).'EnglishName'})*/){
					$parentInfoAry = array();
					$parentInfoAry['ApplicationID'] = $ApplicationID;
					$parentInfoAry['PG_TYPE'] = $_pgType;
					//$parentInfoAry['lsSingleParents'] = $IsSingleParent;
					//$parentInfoAry['HasFullTimeJob'] = $IsFullTime;
					//$parentInfoAry['IsFamilySpecialCase'] = $IsFamilySpecialCase;
					//$parentInfoAry['IsApplyFullDayCare'] = $IsApplyFullDayCare;
					//$parentInfoAry['IsLiveWithChild'] = ${'G'.($_key+1).'LiveWithChild'};
					//$parentInfoAry['Relationship'] = ${'G'.($_key+1).'Relationship'};
					$parentInfoAry['EnglishName'] = ${'G'.($_key+1).'EnglishName'};
					$parentInfoAry['ChineseName'] = ${'G'.($_key+1).'ChineseName'};
					//$parentInfoAry['HKID'] = ${'G'.($_key+1).'HKID'};
					$parentInfoAry['JobTitle'] = ${'G'.($_key+1).'Occupation'};
//					$parentInfoAry['Company'] = ${'G'.($_key+1).'CompanyName'};
					//$parentInfoAry['JobPosition'] = ${'G'.($_key+1).'JobPosition'};
//					$parentInfoAry['OfficeAddress'] = ${'G'.($_key+1).'CompanyAddress'};
					$parentInfoAry['OfficeTelNo'] = ${'G'.($_key+1).'CompanyNo'};
//					$parentInfoAry['Email'] = ${'G'.($_key+1).'Email'};
					//$parentInfoAry['Relationship'] = ($_pgType=='G'?$G3Relationship:$StudentRelationship);	
					$parentInfoAry['Mobile'] = ${'G'.($_key+1).'MobileNo'};	
//					$parentInfoAry['LevelOfEducation'] = ${'G'.($_key+1).'LevelOfEducation'};	
//					$parentInfoAry['LastSchool'] = ${'G'.($_key+1).'LastSchool'};	
					$Success[] = $this->insertApplicationParentInfo($parentInfoAry, $isUpdate);
				}
				
 			}
			/*if($G1EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 1);
				
			if($G2EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 2);
				
			if($G3EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 3);*/
			
//			$Success[] = $this->insertApplicationStudentInfoCust($Data, $ApplicationID);
			
			$Success[] = $this->insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate);
			
			$Success[] = $this->insertApplicationOthersInfo($Data, $ApplicationID, $isUpdate);
			
			if(in_array(false,$Success)){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	function insertApplicationStatus($libkis_admission, $ApplicationID){
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){

//   		}
//   		else{
   			//Henry: not yet finish
   			$sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
     					SchoolYearID,
     					ApplicationID,
					    Status,
					    DateInput)
					VALUES (
						'".$this->schoolYearID."',
						'".$ApplicationID."',
						'1',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	function updateApplicationStatus($data){
		global $UserID;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
				s.ReceiptID = '".$receiptID."',
     			s.ReceiptDate = '".$receiptdate."',
      			s.Handler = '".$handler."', 
      			s.InterviewDate = '".$interviewdate."',
				s.InterviewLocation = '".$interviewlocation."',
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."',
				o.InterviewSettingID =  '".$InterviewSettingID."',
				o.InterviewSettingID2 =  '".$InterviewSettingID2."',
				o.InterviewSettingID3 =  '".$InterviewSettingID3."'
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	return $this->db_db_query($sql);
	}  
	function updateApplicationStatusByIds($applicationIds,$status){
		global $UserID;
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.RecordID IN (".$applicationIds.")
    	";
    	return $this->db_db_query($sql);
	}	  	
   	function insertApplicationStudentInfo($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			if($isUpdate){
   				//Henry
   				$sql = "UPDATE 
					ADMISSION_STU_INFO 
				SET
					 ChineseName = '".$StudentName_b5."',
					 EnglishName = '".$StudentName_en."',
					 Gender = '".$StudentGender."',
					 DOB = '".$StudentDateOfBirth."',
					 PlaceOfBirth = '".$StudentPlaceOfBirth."',
					 HomeTelNo = '".$StudentHomePhoneNo."',
					 Address = '".$StudentHomeAddress_en."',
					 AddressChi = '".$StudentHomeAddress_b5."',
					 Email = '".$StudentEmail."',
					 ReligionOther = '".$religion."',
					 County =  '".$Nationality."',
				     DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'";
   			}
   			else{
	   			$sql = "INSERT INTO ADMISSION_STU_INFO (
							ApplicationID,
			   				 ChineseName,
			  				 EnglishName,   
			   				 Gender,
			  				 DOB,
			   				 BirthCertNo,
			   				 PlaceOfBirth,
							HomeTelNo,
							Address,
							AddressChi,
							Email,
							ReligionOther,
							DateInput,
							County
						) VALUES (
							'".$ApplicationID."',
							'".$StudentName_b5."',
							'".$StudentName_en."',
							'".$StudentGender."',
							'".$StudentDateOfBirth."',
							'".$StudentBirthCertNo."',
							'".$StudentPlaceOfBirth."',
							'".$StudentHomePhoneNo."',
							'".$StudentHomeAddress_en."',
							'".$StudentHomeAddress_b5."',
							'".$StudentEmail."',
							'".$religion."',
							now(),
							'".$Nationality."'
						)
				";
   			}
			return $this->db_db_query($sql);
   		}
   		return false;
    }
    
    function insertApplicationParentInfo($Data, $isUpdate=0){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
		
		if($isUpdate){
			foreach($Data as $_fieldname => $_fieldvalue){
				if($_fieldname == "ApplicationID"){
					$ApplicationID = $_fieldvalue;
				}
				else if($_fieldname == "PG_TYPE"){
					$PG_TYPE = $_fieldvalue;
				}
				else{
					$updateStr .= $_fieldname." = '".$_fieldvalue."',";
				}
			}
			//Henry
			$sql = "UPDATE 
					ADMISSION_PG_INFO 
				SET
					".$updateStr."
				    DateModified = NOW()  
				WHERE
					ApplicationID = '".$ApplicationID."'
				AND
					PG_TYPE = '".$PG_TYPE."'";
					
			if(($result = $this->db_db_query($sql)) && $this->db_affected_rows() == 0){
				$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
			}
			else{
				return $result;
			}
		}
		else{	
			$sql = "INSERT INTO ADMISSION_PG_INFO (
						".$fieldname."
						DateInput
					)VALUES (
						".$fieldvalue."
						NOW()
					)";
		}
		return $this->db_db_query($sql);
    }
     function updateApplicationParentInfo($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_PG_INFO SET
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		return $this->db_db_query($sql);
    }
    
     function updateApplicationStudentInfoCust($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_STU_PREV_SCHOOL_INFO
				SET 
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		$success[] = $this->db_db_query($sql);
				
		if($Data['Year'] == '' && $Data['Class'] =='' && $Data['NameOfSchool'] ==''){
			$sql = "DELETE FROM ADMISSION_STU_PREV_SCHOOL_INFO WHERE RecordID = '".$Data['RecordID']."'";
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
    }
    
    function updateApplicationRelativesInfoCust($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_RELATIVES_AT_SCH_INFO
				SET 
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		$success[] = $this->db_db_query($sql);
		if($Data['Year'] == '' && $Data['Name'] =='' && $Data['ClassPosition'] =='' && $Data['Relationship'] ==''){
			$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE RecordID = '".$Data['RecordID']."'";
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
    }
       
    function insertApplicationOthersInfo($Data, $ApplicationID, $isUpdate=0){    	
   		extract($Data);
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){
			
//   		}
//   		else{
   			//Henry: not yet finish
//   			$ApplyDayType1 = '';
//   			$ApplyDayType2 = '';
//   			$ApplyDayType3 = '';
//   			for($i=1;$i<=3;$i++){
//   				if(${'OthersApplyDayType'.$i} == 1){
//   					$ApplyDayType1 = $i;
//   				}
//   				else if(${'OthersApplyDayType'.$i} == 2){
//   					$ApplyDayType2 = $i;
//   				}
//   				else if(${'OthersApplyDayType'.$i} == 3){
//   					$ApplyDayType3 = $i;
//   				}
//   			}
			if($isUpdate){
   				//Henry
   				$sql = "UPDATE 
						ADMISSION_OTHERS_INFO 
					SET
						 ApplyDayType1 = '".$OthersApplyDayType1."',
						 ApplyDayType2 = '".$OthersApplyDayType2."',
						 ApplyDayType3 = '".$OthersApplyDayType3."',
						 BriefingApplicationNo = '".$OthersBriefingApplicationNumber."',
					     DateInput = NOW()  
					WHERE
						ApplicationID = '".$ApplicationID."'";
   			}
   			else{
				$sql = "UPDATE 
						ADMISSION_OTHERS_INFO 
					SET
						 ApplyYear = '".$this->schoolYearID."',
						 ApplyDayType1 = '".$OthersApplyDayType1."',
						 ApplyDayType2 = '".$OthersApplyDayType2."',
						 ApplyDayType3 = '".$OthersApplyDayType3."',
						 ApplyLevel = '".$sus_status."',
						 Token = '".$token."',
						 BriefingApplicationNo = '".$OthersBriefingApplicationNumber."',
					     DateInput = NOW(),
				 	 	 HTTP_USER_AGENT = '".addslashes($_SERVER['HTTP_USER_AGENT'])."' 
					WHERE
						ApplicationID = '".$ApplicationID."'";
   			}
			if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
   			
	   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
							ApplicationID,
						     ApplyYear,	   
						     ApplyDayType1,
						     ApplyDayType2,
						     ApplyDayType3,
						     ApplyLevel,
							 Token,
							 BriefingApplicationNo,
						     DateInput,
   						 	 HTTP_USER_AGENT
						     )
						VALUES (
							'".$ApplicationID."',
							'".$this->schoolYearID."',
							'".$OthersApplyDayType1."',
							'".$OthersApplyDayType2."',
							'".$OthersApplyDayType3."',
							'".$sus_status."',
							'".$token."',
							'".$OthersBriefingApplicationNumber."',
							now(),
							'".addslashes($_SERVER['HTTP_USER_AGENT'])."')
				";
				return $this->db_db_query($sql);
			}
			else 
				return true;
   		}
   		return false;
    }
    
    function insertApplicationStudentInfoCust($Data, $ApplicationID){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			for($i=1; $i <= 5; $i++){
   				if(${'OthersPrevSchYear'.$i} != '' || ${'OthersPrevSchClass'.$i} !='' || ${'OthersPrevSchName'.$i} !=''){
		   			$sql = "INSERT INTO ADMISSION_STU_PREV_SCHOOL_INFO (
								ApplicationID,
				   				Year,
				  				Class,   
				   				NameOfSchool,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".${'OthersPrevSchYear'.$i}."',
								'".${'OthersPrevSchClass'.$i}."',
								'".${'OthersPrevSchName'.$i}."',
								now())
					";
					$success[] = $this->db_db_query($sql);
   				}
   			}
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID applicationID,
     			s.Year OthersPrevSchYear,		    					
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName 
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

    	return $this->returnArray($sql);
	}	
    
    function insertApplicationRelativesInfoCust($Data, $ApplicationID, $isUpdate=0){
   		extract($Data);

   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			if($isUpdate){
   				//Henry
   				$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE ApplicationID = '".$ApplicationID."'";
   				$success[] = $this->db_db_query($sql);
   			}
   			for($i=1; $i <= 2; $i++){
   				if(${'OthersRefRelativeStudiedName'.$i} != '' || ${'OthersRefRelativeRelationship'.$i} !=''){
		   			$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				  				Name,   
				   				Type,
				   				Relationship,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".${'OthersRefRelativeStudiedName'.$i}."',
								'REF',
								'".${'OthersRefRelativeRelationship'.$i}."',
								now())
					";
					$success[] = $this->db_db_query($sql);
   				}
   			}
   			for($i=1; $i <= 2; $i++){
   				if(${'OthersExRelativeStudiedName'.$i} != '' || ${'OthersExRelativeRelationship'.$i} !='' || ${'OthersExRelativeStudiedYear'.$i} !=''){
		   			$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				  				Name,   
				   				Type,
				   				Year,
								Relationship,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".${'OthersExRelativeStudiedName'.$i}."',
								'EX',
								'".${'OthersExRelativeStudiedYear'.$i}."',
								'".${'OthersExRelativeRelationship'.$i}."',
								now())
					";
					$success[] = $this->db_db_query($sql);
   				}
   			}
   			for($i=1; $i <= 2; $i++){
   				if(${'OthersCurRelativeStudiedName'.$i} != '' || ${'OthersCurRelativeRelationship'.$i} !='' || ${'OthersCurRelativeStudiedYear'.$i} !=''){
		   			$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				  				Name,   
				   				Type,
				   				Year,
								Relationship,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".${'OthersCurRelativeStudiedName'.$i}."',
								'CUR',
								'".${'OthersCurRelativeStudiedYear'.$i}."',
								'".${'OthersCurRelativeRelationship'.$i}."',
								now())
					";
					$success[] = $this->db_db_query($sql);
   				}
   			}
   			for($i=1; $i <= 2; $i++){
   				if(${'OthersRelativeApplyName'.$i} != '' || ${'OthersRelativeApplyRelationship'.$i} !='' || ${'OthersRelativeApplyClass'.$i} !=''){
		   			$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				  				Name,   
				   				Type,
				   				ClassPosition,
								Relationship,
								BirthCertNo,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".${'OthersRelativeApplyName'.$i}."',
								'APPLY',
								'".${'OthersRelativeApplyClass'.$i}."',
								'".${'OthersRelativeApplyRelationship'.$i}."',
								'".${'OthersRelativeApplyBirthCertNo'.$i}."',
								now())
					";
					$success[] = $this->db_db_query($sql);
   				}
   			}
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function getApplicationRelativesInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$type=''){
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($type)?" AND r.Type='".$type."'":"";
		$sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
     			r.Year OthersRelativeStudiedYear,
				r.Name OthersRelativeStudiedName,		    					
     			r.ClassPosition OthersRelativeClassPosition,
				r.Relationship OthersRelativeRelationship,
				r.BirthCertNo OthersRelativeBirthCertNo,
				r.Type
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
    	";

    	return $this->returnArray($sql);
	}	
    
	function updateApplicationOtherInfo($data){    	
		global $UserID,$admission_cfg,$sys_custom;
   		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;

		$sql = "UPDATE 
					ADMISSION_OTHERS_INFO 
				SET ";
				
				$ApplyDayTypeAry = array();
				for($i=0;$i<3;$i++){
					if(!empty($ApplyDayType[$i])){
						$ApplyDayTypeAry[] = $ApplyDayType[$i];
					}
				} 
				for($i=0;$i<3;$i++){
					$sql .= "ApplyDayType".($i+1)." = '".$ApplyDayTypeAry[$i]."',";
				}     
		    $sql .= "BriefingApplicationNo = '".$OthersBriefingApplicationNumber."',
				     DateModified = NOW(),
				     ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$recordID."' AND ApplyYear = '".$schoolYearId."'";
			$success[] = $this->db_db_query($sql);
		
//		$success[] = $this->saveApplicationStudentInfoCust($data);
		
		$success[] = $this->saveApplicationRelativesInfoCust($data);
		
		return !in_array(false,$success);	
    }    
    function removeApplicationAttachment($data){
    	global $file_path;
    	extract($data);
    	if(empty($recordID)) return;
 		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";
    	$sql = "SELECT 
    				r.RecordID attachment_id,
    				r.AttachmentName attachment_name,
    				r.AttachmentType attachment_type
    			FROM 
    				ADMISSION_ATTACHMENT_RECORD r
    			INNER JOIN 
    				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
    			WHERE 
    				o.RecordID = '".$recordID."' 
    				".$cond."
    			";
    	$applicantAry = $this->returnArray($sql);
    
    	$Success = array();
    	for($i=0;$i<count($applicantAry);$i++){
    		$_attachmentName = $applicantAry[$i]['attachment_name'];
    		$_attachmentType = $applicantAry[$i]['attachment_type']=='personal_photo'? $applicantAry[$i]['attachment_type']:'other_files';
    		$_attachmentId = $applicantAry[$i]['attachment_id'];
    		$image_url = $this->filepath.$recordID.'/'.$_attachmentType.'/'.$_attachmentName;	
    		
    		if(file_exists($file_path.$image_url)){
    			unlink($file_path.$image_url);
    			$sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '".$_attachmentId."'";
    			
    			$Success[] = $this->db_db_query($sql);
    		}
    	}
    	if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }
    function saveApplicationAttachment($data){
    	global $UserID;
    	extract($data);
 		if(empty($recordID)) return;
    	$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '".$recordID."'";
    	$applicationID = current($this->returnVector($sql));
    	
    	if(!empty($applicationID)&&!empty($attachment_name)&&!empty($attachment_type)){
    		$result = $this->removeApplicationAttachment($data);
    		if($result){
	    		$sql = "INSERT INTO 
	    					ADMISSION_ATTACHMENT_RECORD 
	    						(ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy)
	    					VALUES
	    						('".$applicationID."','".$attachment_type."','".$attachment_name."',NOW(),'".$UserID."')
	    				";
	    		return $this->db_db_query($sql);
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
 	function updateApplicationStudentInfo($data, $isAdminUpdate = false){
 		global $kis_lang, $Lang;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
 		if($isAdminUpdate){
// 			$sql = "
//				UPDATE 
//					ADMISSION_STU_INFO stu 
//				INNER JOIN 
//					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
//				SET
//					stu.LastSchool = '".$lastschool."'
//				WHERE 
//					o.RecordID = '".$recordID."'	
//	    	";
 		}
 		else{
			$sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
				SET
	     			stu.ChineseName = '".$student_name_b5."',
	      			stu.EnglishName = '".$student_name_en."',
	      			stu.Gender = '".$gender."',
	      			stu.DOB = '".$dateofbirth."',
					stu.BirthCertType = '".$birthcerttype."',	
	      			stu.PlaceOfBirth = '".$placeofbirth."',
	      			stu.County = '".$nationality."',
	      			stu.HomeTelNo = '".$homephoneno."',
	      			stu.BirthCertNo = '".$birthcertno."' ,
	      			stu.Email = '".$email."',
	      			stu.ReligionOther = '".$religion."' ,
	      			stu.Address = '".$homeaddress."',
	      			stu.AddressChi = '".$homeaddresschi."'
				WHERE 
					o.RecordID = '".$recordID."'	
	    	";
 		}
    	return $this->db_db_query($sql);
	}    
 	
	function getApplicationAttachmentRecord($schoolYearID,$data=array()){
		extract($data);
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";		
		$sql = "
			SELECT
				 o.RecordID folder_id,
     			 r.RecordID,
			     r.ApplicationID applicationID,
			     r.AttachmentType attachment_type,
			     r.AttachmentName attachment_name,
			     r.DateInput dateinput,
			     r.InputBy inputby
			FROM
				ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID						
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			ORDER BY r.AttachmentType, r.RecordID desc
    	";
    	$result = $this->returnArray($sql);
    	$attachmentAry = array();
    	for($i=0;$i<count($result);$i++){
    		$_attachType = $result[$i]['attachment_type'];
     		$_attachName = $result[$i]['attachment_name'];   
     		$_applicationId = $result[$i]['applicationID'];  
     		$_folderId = $result[$i]['folder_id'];
     		$attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;		
     		$attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId.'/'.($_attachType=='personal_photo'?$_attachType:'other_files').'/'.$_attachName;	
     		
    	}
		return $attachmentAry;
	}
	function saveApplicationParentInfo($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
 		foreach($this->pg_type as $_key => $_pgType){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			$parentInfoAry['PG_TYPE'] = $_pgType;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			$parentInfoAry['ChineseName'] = $parent_name_b5[$_key];
			$parentInfoAry['JobTitle'] = $occupation[$_key];
			//$parentInfoAry['Company'] = $companyname[$_key];
			//$parentInfoAry['JobPosition'] = $jobposition[$_key];
			//$parentInfoAry['OfficeAddress'] = $companyaddress[$_key];
			$parentInfoAry['OfficeTelNo'] = $office[$_key];
			$parentInfoAry['Mobile'] = $mobile[$_key];	
			//$parentInfoAry['Email'] = $email[$_key];
			//$parentInfoAry['LevelOfEducation'] = $levelofeducation[$_key];
			//$parentInfoAry['LastSchool'] = $lastschool[$_key];
			//$parentInfoAry['HKID'] = $hkid[$_key];	
			if($parent_id[$_key]=='new'){
				$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
			}else{
				$parentInfoAry['RecordID'] = $parent_id[$_key];	
				$Success[] = $this->updateApplicationParentInfo($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationStudentInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		$Success = array();
 		
 		for($i=0; $i<count($OthersPrevSchYear); $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_cust_id[$i]=='new'){
				$parentInfoAry['OthersPrevSchYear'.($i+1)] = $OthersPrevSchYear[$i];
				$parentInfoAry['OthersPrevSchClass'.($i+1)] = $OthersPrevSchClass[$i];
				$parentInfoAry['OthersPrevSchName'.($i+1)] = $OthersPrevSchName[$i];
				$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry);
			}else{
				$parentInfoAry['Year'] = $OthersPrevSchYear[$i];
				$parentInfoAry['Class'] = $OthersPrevSchClass[$i];
				$parentInfoAry['NameOfSchool'] = $OthersPrevSchName[$i];
				$parentInfoAry['RecordID'] = $student_cust_id[$i];	
				$Success[] = $this->updateApplicationStudentInfoCust($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationRelativesInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
 		for($i=0; $i<2; $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_ref_cust_id[$i]=='new'){
				$parentInfoAry['OthersRefRelativeStudiedName'.($i+1)] = $OthersRefRelativeStudiedName[$i];
//				$parentInfoAry['OthersRelativeClassPosition'.($i+1)] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['OthersRefRelativeRelationship'.($i+1)] = $OthersRefRelativeRelationship[$i];
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry, $ApplicationID);
			}else{
				$parentInfoAry['Name'] = $OthersRefRelativeStudiedName[$i];
//				$parentInfoAry['ClassPosition'] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['Relationship'] = $OthersRefRelativeRelationship[$i];
				$parentInfoAry['RecordID'] = $student_ref_cust_id[$i];	
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
		}
		
 		for($i=0; $i<2; $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_ex_cust_id[$i]=='new'){
				$parentInfoAry['OthersExRelativeStudiedName'.($i+1)] = $OthersExRelativeStudiedName[$i];
//				$parentInfoAry['OthersRelativeClassPosition'.($i+1)] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['OthersExRelativeStudiedYear'.($i+1)] = $OthersExRelativeStudiedYear[$i];
				$parentInfoAry['OthersExRelativeRelationship'.($i+1)] = $OthersExRelativeRelationship[$i];
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry, $ApplicationID);
			}else{
				$parentInfoAry['Name'] = $OthersExRelativeStudiedName[$i];
//				$parentInfoAry['ClassPosition'] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['Year'] = $OthersExRelativeStudiedYear[$i];
				$parentInfoAry['Relationship'] = $OthersExRelativeRelationship[$i];
				$parentInfoAry['RecordID'] = $student_ex_cust_id[$i];	
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
		}
		
		for($i=0; $i<2; $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_cur_cust_id[$i]=='new'){
				$parentInfoAry['OthersCurRelativeStudiedName'.($i+1)] = $OthersCurRelativeStudiedName[$i];
//				$parentInfoAry['OthersRelativeClassPosition'.($i+1)] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['OthersCurRelativeStudiedYear'.($i+1)] = $OthersCurRelativeStudiedYear[$i];
				$parentInfoAry['OthersCurRelativeRelationship'.($i+1)] = $OthersCurRelativeRelationship[$i];
				
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry, $ApplicationID);
			}else{
				$parentInfoAry['Name'] = $OthersCurRelativeStudiedName[$i];
//				$parentInfoAry['ClassPosition'] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['Year'] = $OthersCurRelativeStudiedYear[$i];
				$parentInfoAry['Relationship'] = $OthersCurRelativeRelationship[$i];
				$parentInfoAry['RecordID'] = $student_cur_cust_id[$i];	
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
		}
		
		for($i=0; $i<2; $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($student_apply_cust_id[$i]=='new'){
				$parentInfoAry['OthersRelativeApplyName'.($i+1)] = $OthersRelativeApplyName[$i];
//				$parentInfoAry['OthersRelativeClassPosition'.($i+1)] = $OthersRelativeClassPosition[$i];
				$parentInfoAry['OthersRelativeApplyClass'.($i+1)] = $OthersRelativeApplyClass[$i];
				$parentInfoAry['OthersRelativeApplyRelationship'.($i+1)] = $OthersRelativeApplyRelationship[$i];
				$parentInfoAry['OthersRelativeApplyBirthCertNo'.($i+1)] = $OthersRelativeApplyBirthCertNo[$i];
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry, $ApplicationID);
			}else{
				$parentInfoAry['Name'] = $OthersRelativeApplyName[$i];
				$parentInfoAry['ClassPosition'] = $OthersRelativeApplyClass[$i];
//				$parentInfoAry['Year'] = $OthersRelativeStudiedYear[$i];
				$parentInfoAry['Relationship'] = $OthersRelativeApplyRelationship[$i];
				$parentInfoAry['BirthCertNo'] = $OthersRelativeApplyBirthCertNo[$i];
				$parentInfoAry['RecordID'] = $student_apply_cust_id[$i];	
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	   
	function getApplicationOthersInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND RecordID='".$recordID."'":"";

		$sql = "
			SELECT 
				RecordID,
				ApplicationID applicationID,
				ApplyYear schoolYearId,
				ApplyDayType1,
				ApplyDayType2,
				ApplyDayType3,
				ApplyLevel classLevelID,
				DateInput,
				DateModified,
				ModifiedBy,
				BriefingApplicationNo,
				InterviewSettingID,
				InterviewSettingID2,
				InterviewSettingID3
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	return $this->returnArray($sql);
	}		 
	function getApplicationParentInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND pg.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				pg.RecordID,
     			pg.ApplicationID applicationID,
     			pg.ChineseName parent_name_b5,
				pg.EnglishName parent_name_EN,
     			pg.PG_TYPE type,
     			pg.JobTitle occupation,
     			pg.Mobile mobile,
				pg.OfficeTelNo office_tel_no,
     			o.ApplyLevel classLevelID
			FROM
				ADMISSION_PG_INFO pg
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";

    	return $this->returnArray($sql);
	}	
	function getApplicationStatus($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			s.ApplicationID applicationID,
     			s.ReceiptID receiptID, 
			    IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
			    IF(s.Handler,".getNameFieldByLang2("iu.").",'') AS handler,
			    s.Handler handler_id,
				s.InterviewDate As interviewdate,
				s.InterviewLocation As interviewlocation,
			    s.Remark remark,CASE ";
		foreach($admission_cfg['Status'] as $_key => $_value){
				$sql .= "WHEN s.Status = ".$_value." THEN '".$_key."' ";
		}	    
		$sql .= " ELSE s.Status END status,";
		$sql .= "
			    CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
			    s.DateInput dateinput,
			    s.InputBy inputby,
			    s.DateModified datemodified,
			    s.ModifiedBy modifiedby,
			    o.ApplyLevel classLevelID
			FROM
				ADMISSION_APPLICATION_STATUS s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			LEFT JOIN
				INTRANET_USER iu ON s.Handler = iu.UserID
			WHERE
				s.SchoolYearID = '".$schoolYearID."'	
			".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

		//if($type == "personal_photo"){
			if (!empty($file)) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							if($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
								$image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
							else
								$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	function moveUploadedAttachment($tempFolderPath, $destFolderPath){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/personal_photo", $destFolderPath);
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/other_files", $destFolderPath);
		$uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);
		
		if(in_array(false, $uploadSuccess))
			return false;
		else
			return true;
	}
	//Siuwan 20131018 Copy from libstudentregistry.php, e.g.$this->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	public function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
		$result = $this->returnArray($sql);
		return getSelectByArray($result, "name=".$selection_name, $code_selected,0,1);
	}
	//Henry 20131018 Copy from libstudentregistry.php
	public function returnPresetCodeName($code_type="", $selected_code="")
	{
		$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
		$result = $this->returnVector($sql);
		return $result[0];
	}
	function displayWarningMsg($warning){
		global $kis_lang;
		$x = '
		 <fieldset class="warning_box">
			<legend>'.$kis_lang['warning'].'</legend>
			<ul>
				<li>'.$kis_lang['msg'][$warning].'</li>
			</ul>
		</fieldset>
		
		';
		return $x;
	}
		
	function insertAttachmentRecord($AttachmentType,$AttachmentName, $ApplicationID){
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
		   				 ApplicationID,
		  				 AttachmentType,   
		   				 AttachmentName,
		  				 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$AttachmentType."',
						'".$AttachmentName."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	//old method
	function newApplicationNumber($schoolYearID=""){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$defaultNo = "PL".$year."-a0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%' order by ApplicationID desc";
		$result = $this->returnArray($sql);
		
		if($result){			
			$num = substr($result[0]['ApplicationID'], -4);
			$num++;
			$num = sprintf("%04s", $num);
		}
		
		$newNo = $prefix.$num;
		
		return $newNo;
	}
	
	function newApplicationNumber2($schoolYearID="",$classLevelID=""){
		global $Lang;
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
//		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
//		if($yearStart == $yearEnd){
//			$year = $yearStart;
//		}
//		else{
//			$year = $yearStart;
//		}
		$classLevel = $this->getClassLevel();
		$classLevel = $classLevel[$classLevelID];
		//$levelKeyWord = $Lang['Admission']['munsang'][$classLevel]['keyword'];
//		if(strpos($classLevel,'6') || strpos($classLevel,'粵') || strpos($classLevel,'Cantonese'))
//			$levelKeyWord = $Lang['Admission']['munsang']['Cantonese']['keyword'];
//		else if(strpos($classLevel,'1') || strpos($classLevel,'普') || strpos($classLevel,'Mandarin'))
//			$levelKeyWord = $Lang['Admission']['munsang']['Mandarin']['keyword'];
//		else
//			$levelKeyWord = $classLevel;
		//csm14a0001
		$defaultNo = $yearStart."0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
		
		if($this->returnArray($sql)){
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( SUBSTRING( ApplicationID, '".(strlen($prefix)+1)."' ) , UNSIGNED ) ) +1, 4, '0')) 
					FROM ADMISSION_OTHERS_INFO 
					WHERE ApplicationID like '".$prefix."%'";
		}
		else{
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					Values ('".$defaultNo."')";
		}
			$result = $this->db_db_query($sql);
					
			$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
			$newNo = $this->returnArray($sql);
			return $newNo[0]['ApplicationID'];
	}
	function getAttachmentByApplicationID($schoolYearID,$applicationID){
		global $file_path;
		$attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID,array("applicationID"=>$applicationID));
		$attachment = array();
		foreach((array)$attachmentAry[$applicationID] as $_type => $_attachmentAry){
			$_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
			if(!empty($_thisAttachment)){
				$_thisAttachment = $this->filepath.$_thisAttachment; 
				if(file_exists($file_path.$_thisAttachment)){
					$attachment[$_type]['link'] = $_thisAttachment;
				}else{
					$attachment[$_type]['link'] = false;
				}
			}else{
				$attachment[$_type]['link'] = false;
			}
			
		}
		return $attachment;
	}
	
	public function getApplicationNumber($RecordIDAry){
		$sql = 'SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID IN ("'.implode('","',$RecordIDAry).'")';
		$result = $this->returnVector($sql);
		return $result;
	}
	
	public function getPrintLink($schoolYearID="", $applicationID , $type="", $lang=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?&id='.$id.'&form_lang='.$lang;
	}
 	function getClassLevel($ClassLevel=''){
 		global $intranet_root;
    	include_once($intranet_root."/includes/form_class_manage.php");
    	$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
    	$numOfForm = count($FormArr);
    	$classLevelName = array();
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisClassLevelID = $FormArr[$i]['YearID'];
			$thisLevelName = $FormArr[$i]['YearName'];
			$classLevelName[$thisClassLevelID] = $thisLevelName;
		}
		return $classLevelName;
    }	
	function getBasicSettings($schoolYearID='',$SettingNameAry=array()){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
    	$sql = "
			SELECT
			    SettingName,
			    SettingValue
			FROM
				ADMISSION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'	
    	";
    	if (sizeof($SettingNameAry) > 0)  {
			$sql .= " AND 
						SettingName in ('".implode("','",$SettingNameAry)."')";
		}
    	$setting = $this->returnArray($sql);
		for ($i=0; $i< sizeof($setting); $i++) {
			$Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
		}
		return $Return;    	
    }
   	function saveBasicSettings($schoolYearID='',$SettingNameValueAry=array()){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$this->Start_Trans();
		
		$SettingsNameAry = array_keys($SettingNameValueAry);
		
		$result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID,$SettingsNameAry);
		$result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID,$SettingNameValueAry);
		
		if(in_array(false, $result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	 
	function removeBasicSettings($schoolYearID,$SettingsNameAry){
		if(count($SettingsNameAry)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameAry)."'";
		
		$sql = "
			DELETE FROM
				ADMISSION_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
			AND SchoolYearID = '".$schoolYearID."'
		";
		
		return $this->db_db_query($sql);
	}
	
	function insertBasicSettings($schoolYearID,$SettingNameValueAry){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		
		foreach((array)$SettingNameValueAry as $_settingName => $_settingValue)
		{
			$InsertSqlArr[] = "('".$schoolYearID."','".$_settingName."','".$_settingValue."', '".$this->uid."', NOW(), '".$this->uid."', NOW())";
		}
		
		if(count($InsertSqlArr)>0)
		{	
			$InsertSql = implode(',',$InsertSqlArr);	
			
			$sql = "
				INSERT INTO	ADMISSION_SETTING
					(SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)	 
				VALUES
					$InsertSql
			";
			
			return $this->db_db_query($sql);
			
		}
		else
			return false;
		
			
	}
//    function getApplicationSetting($schoolYearID=''){
//    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
//
//    	$sql = "
//			SELECT
//     			ClassLevelID,
//			    IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d %H:%i'),'') As StartDate,
//				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d %H:%i'),'') As EndDate,
//				IF(DOBStart,DATE_FORMAT(DOBStart,'%Y-%m-%d'),'') As DOBStart,
//				IF(DOBEnd,DATE_FORMAT(DOBEnd,'%Y-%m-%d'),'') As DOBEnd,
//			    DayType,
//			    FirstPageContent,
//			    LastPageContent,
//			    DateInput,
//			    InputBy,
//			    DateModified,
//			    ModifiedBy
//			FROM
//				ADMISSION_APPLICATION_SETTING
//			WHERE
//				SchoolYearID = '".$schoolYearID."'
//    	";
//    	$setting = $this->returnArray($sql);
//    	$applicationSettingAry = BuildMultiKeyAssoc($setting, 'ClassLevelID');
//    	$applicationPeriodAry = array();
//    	foreach($this->classLevelAry as $_classLevelId => $_classLevelName){ 	
//    		$_startdate = $applicationSettingAry[$_classLevelId]['StartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['StartDate']:'';	
//    		$_enddate = $applicationSettingAry[$_classLevelId]['EndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['EndDate']:'';	
//    		
//    		$applicationPeriodAry[$_classLevelId] = array(
//    													'ClassLevelName'=>$_classLevelName,
//      													'StartDate'=>$_startdate,  	
//      													'EndDate'=>$_enddate,
//      													'DOBStart'=>$applicationSettingAry[$_classLevelId]['DOBStart'],
//      													'DOBEnd'=>$applicationSettingAry[$_classLevelId]['DOBEnd'],
//      													'DayType'=>$applicationSettingAry[$_classLevelId]['DayType'],   
//      													'FirstPageContent'=>$applicationSettingAry[$_classLevelId]['FirstPageContent'],  
//      													'LastPageContent'=>$applicationSettingAry[$_classLevelId]['LastPageContent']      													  																									
//    												);
//    	}
//    	return $applicationPeriodAry;    	
//    } 
//	public function updateApplicationSetting($data){
//   		extract($data);
//   		#Check exist setting
//   		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
//   		$cnt = current($this->returnVector($sql));
//
//   		if($cnt){//update
//   			$sql = "UPDATE ADMISSION_APPLICATION_SETTING SET 
//		   				 StartDate = '".$startDate."',
//		  				 EndDate = '".$endDate."', 
//						 DOBStart = '".$dOBStart."',
//		  				 DOBEnd = '".$dOBEnd."',  
//		   				 DayType = '".$dayType."',
//		  				 FirstPageContent = '".$firstPageContent."',   
//		   				 LastPageContent = '".$lastPageContent."',
//		   				 DateModified = NOW(),
//		   				 ModifiedBy = '".$this->uid."'
//   					WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
//   		}else{//insert
//   			$sql = "INSERT INTO ADMISSION_APPLICATION_SETTING (
//   						SchoolYearID,
//   						ClassLevelID,
//					    StartDate,
//					    EndDate,
//					    DayType,
//		  				FirstPageContent,   
//		   				LastPageContent,
//					    DateInput,
//					    InputBy,
//					    DateModified,
//					    ModifiedBy) 
//					VALUES (
//					    '".$schoolYearID."',
//					    '".$classLevelID."',
//					    '".$startDate."',
//					    '".$endDate."',	
//					    '".$dayType."',
//					    '".$firstPageContent."',
//					    '".$lastPageContent."',	
//					    NOW(),
//					    '".$this->uid."',
//					     NOW(),
//					    '".$this->uid."')
//			";
//   		}
//   		return $this->db_db_query($sql);
//    }    
    function getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$recordID=''){
		$cond = !empty($applicationID)?" AND stu.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($status)?" AND s.status='".$status."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			stu.ApplicationID applicationID,
     			o.ApplyLevel classLevelID,
     			o.ApplyYear schoolYearID,
     			stu.ChineseName student_name_b5,
      			stu.EnglishName student_name_en, 
      			stu.Gender gender,
				stu.BirthCertType birthcerttype,
      			IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,	
      			stu.PlaceOfBirth placeofbirth,
      			stu.County county,
      			stu.HomeTelNo homephoneno,
      			stu.BirthCertNo birthcertno,
      			stu.Email email,
      			stu.ReligionOther religionOther,
      			stu.Address homeaddress,
				stu.AddressChi homeaddresschi,		
     			".getNameFieldByLang2("stu.")." AS student_name
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID							
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	$studentInfoAry = $this->returnArray($sql);
		return $studentInfoAry;
	}
	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
		
		 if($custSelection=='HasSiblingsOrRelatives'){
			$custSelection_cond .= " left outer join ADMISSION_RELATIVES_AT_SCH_INFO as pi ON o.ApplicationID = pi.ApplicationID";
			$cond .= " AND pi.Name != '' AND pi.Type = 'EX' ";
		}
		
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'				
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			$custSelection_cond	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}
	
	function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1){
		global $admission_cfg;
		
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		
		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sort = $sortby? "$sortby $order":"application_id";
		
//		if(!empty($keyword)){
//			$cond .= "
//				AND ( 
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'					
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'				
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')."
			INNER JOIN 
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				1 	
			".$cond." 
			order by ".$sort."
    	";
    	$sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			stu.ChineseName AS student_name_b5,
				stu.EnglishName AS student_name_en,
				".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." ";
     	//debug_r($sql);
    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}
	   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function checkImportDataForImportAdmissionHeader($csv_header, $lang = ''){
		//$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
		$file_format = $this->getExportHeader();
		$file_format = $file_format[1];
		# check csv header
		$format_wrong = false;
		
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($csv_header[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		
		return $format_wrong;
	}
	
	public function returnPresetCodeAndNameArr($code_type="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			
			//valid Admission Date
			$aData[0] = getDefaultDateFormat($aData[0]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[0]) ) {
				list($year , $month , $day) = explode('-',$aData[0]);
		       	$resultArr[$i]['validAdmissionDate'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validAdmissionDate'] = false;
			}
			
			//valid Admission number
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[1])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validAdmissionNo'] = false;
			}
			else
				$resultArr[$i]['validAdmissionNo'] = true;
			
			//valid Gender
			if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
				$data[$i][5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
				$data[$i][5] = 'F';
			}
			if ( strtoupper($aData[5]) == 'M' || strtoupper($aData[5]) == 'F') {
				
		       	$resultArr[$i]['validGender'] = true;
			}
			else{
				$resultArr[$i]['validGender'] = false;
			}
			
			//valid Date of birth
			$aData[6] = getDefaultDateFormat($aData[6]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[6]) ) {
				list($year , $month , $day) = explode('-',$aData[6]);
		       	$resultArr[$i]['validDateOfBirth'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validDateOfBirth'] = false;
			}
			
			//valid age
			if (is_numeric($aData[7])) {
				$resultArr[$i]['validAge'] = true;
			}
			else{
				$resultArr[$i]['validAge'] = false;
			}
			
			//valid birth cert type
			$resultArr[$i]['validBirthCertType'] = false;
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$resultArr[$i]['validBirthCertType'] = true;
					break;
				}
			}
			
			//valid birth cert
//			if (preg_match('/^[a-zA-Z][0-9]{7}$/',$aData[8])) {
//				$resultArr[$i]['validBirthCertNo'] = true;
//			}
//			else{
//				$resultArr[$i]['validBirthCertNo'] = false;
//			}
			
			//valid email address
			if (preg_match('/\S+@\S+\.\S+/',$aData[14])) {
				$resultArr[$i]['validEmail'] = true;
			}
			else{
				$resultArr[$i]['validEmail'] = false;
			}
			//valid Religion
			$religionArr = $this->returnPresetCodeAndNameArr("RELIGION");
			foreach($religionArr as $aReligion){
				if (strtoupper($aReligion[1]) == strtoupper($aData[15])){
					$aData[15] = $aReligion[0];
					$data[$i][15] = $aReligion[0];
					$resultArr[$i]['validReligion'] = true;
					break;
				}
				else{
					$resultArr[$i]['validReligion'] = false;
				}
			}
			
			//valid Apply day type
			$resultArr[$i]['validApplyDayType'] = false;
			if($aData[26] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[26]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[26] = $j;
						$data[$i][26] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			if($aData[27] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[27] = $j;
						$data[$i][27] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			if($aData[28] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[28]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[28] = $j;
						$data[$i][28] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			//valid SiblingApplied
			$resultArr[$i]['validSiblingApplied'] = true;
			if(strtoupper($aData[29]) == strtoupper($kis_lang['Admission']['yes'])){
				if($aData[30] == ''){
					$resultArr[$i]['validSiblingApplied'] = false;
				}
			}
			
			//valid SiblingIsGrad
			$resultArr[$i]['validSiblingIsGrad'] = true;
			$resultArr[$i]['validSiblingIsGradInfo'] = true;
			if(strtoupper($aData[31]) == strtoupper($kis_lang['Admission']['yes'])){
				if(!is_numeric($aData[32])){
					$resultArr[$i]['validSiblingIsGrad'] = false;
				}
				if($aData[32] == 1 || $aData[32] == 2 || $aData[32] == 3){
					if($aData[33] =='' || $aData[34] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
				if($aData[32] == 2 || $aData[32] == 3){
					if($aData[35] =='' || $aData[36] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
				if($aData[32] == 3){
					if($aData[37] =='' || $aData[38] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
			}
			
			//--- Henry Added 20140827
//			//valid record status
//			foreach($admission_cfg['Status'] as $_key => $_status){
//				if (strtoupper($kis_lang['Admission']['Status'][$_key]) == strtoupper($aData[41])){
//					
//					$aData[41] = $_status;
//					$data[$i][41] = $_status;
//					$resultArr[$i]['validRecordStatus'] = true;
//					break;
//				}
//				else{
//					$resultArr[$i]['validRecordStatus'] = false;
//				}
//			}
//			
//			//valid Receipt Date
//			$aData[43] = getDefaultDateFormat($aData[43]);
//
//			if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[43]) ) {
//				list($year , $month , $day) = explode('-',$aData[43]);
//		       	$resultArr[$i]['validReceiptDate'] = checkdate($month , $day , $year);
//			}
//			else{
//				if($aData[43] != '')
//					$resultArr[$i]['validReceiptDate'] = false;
//				else
//					$resultArr[$i]['validReceiptDate'] = true;
//			}
//			
//			//valid is notified
//			$resultArr[$i]['validIsNotified'] = false;
//			if(strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['no'])){
//				$resultArr[$i]['validIsNotified'] = true;
//			}
			
			
			//------------------------------------------------------------------------------------------
//			$aData[4] = getDefaultDateFormat($aData[4]);
//			//check date
//			if ($aData[4] =='' && $aData[5] ==''){
//				$validDate = true;
//			}
//			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
//		       list($year , $month , $day) = explode('-',$aData[4]);
//		       $validDate = checkdate($month , $day , $year);
//		    } else {
//		       $validDate =  false;
//		    }
//
//		    //check time
//		    if ($aData[4] =='' && $aData[5] ==''){
//				$validTime = true;
//			}
//			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
//		       $validTime = true;
//		    } else {
//		       $validTime =  false;
//		    }
//			$sql = "
//				SELECT
//					COUNT(*)
//				FROM
//					ADMISSION_STU_INFO s
//				WHERE 
//					trim(s.applicationID) = '".trim($aData[1])."' AND trim(s.birthCertNo) = '".trim($aData[8])."'
//	    	";
//			$result = $this->returnVector($sql);
//			if($result[0] == 0){
//				$resultArr[$i]['validData'] = $aData[1];
//			}
//			else
//				$resultArr[$i]['validData'] = false;
//			$resultArr[$i]['validDate'] = $validDate;
//			$resultArr[$i]['validTime'] = $validTime;
//			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[1];
			$i++;
		}
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
			if(!$aResult['validAdmissionDate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidAdmissionDate'];
			}
			else if(!$aResult['validAdmissionNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
			}
			else if(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidWordOfGender'] ;
			}
			else if(!$aResult['validDateOfBirth']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidDateOfBirth'];
			}
			else if(!$aResult['validAge']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidNumberOfAge'] ;
			}
			else if(!$aResult['validBirthCertType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertType'];
			}
//			else if(!$aResult['validBirthCertNo']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertNo'];
//			}
			else if(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress'];
			}
			else if(!$aResult['validReligion']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidKeywordOfReligion'] ;
			}
			else if(!$aResult['validApplyDayType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidKeywordOfApplyDayType'] ;
			}
			else if(!$aResult['validSiblingApplied']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNameOfSiblingApplied'];
			}
			else if(!$aResult['validSiblingIsGrad']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNoOfGraduatedSibling'];
			}
			else if(!$aResult['validSiblingIsGradInfo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNameOfGraduatedSibling'];
			}
//			else if(!$aResult['validRecordStatus']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importEnterRecordStatus'];
//			}
//			else if(!$aResult['validReceiptDate']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importInvalidReceiptDate'];
//			}
//			else if(!$aResult['validIsNotified']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importEnterIsNotified'];
//			}
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.$i.'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			//-----------------------------------------------------------------------------------
//			if($aResult['validData']!=false){
//				$errCount++;
//			$x .= '<tr class="step2">
//					<td>'.$i.'</td>
//					<td>'.$aResult['validData'].'</td>
//					<td><font color="red">';
//			if(!$aResult['validDate']){
//				$x .= $kis_lang['invalidinterviewdateformat'];
//			}
//			else if(!$aResult['validTime']){
//				$x .= $kis_lang['invalidinterviewtimeformat'];
//			}
//			else
//				$x .= $kis_lang['invalidapplicationbirthno'];
//			$x .= '</font></td>
//				</tr>';
//			}
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	function importDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		array_shift($data);
		array_shift($data);
		foreach($data as $aData){
			global $UserID;
			//--- convert the text to key code [start]
		    if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
			}
			
			$religionArr = $this->returnPresetCodeAndNameArr("RELIGION");
			foreach($religionArr as $aReligion){
				if (strtoupper($aReligion[1]) == strtoupper($aData[15])){
					$aData[15] = $aReligion[0];
					break;
				}
			}
			
			//valid Apply day type
			if($aData[26] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[26]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[26] = $j;
						break;
					}
				}
			}
			
			if($aData[27] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[27] = $j;
						break;
					}
				}
			}
			
			if($aData[28] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[28]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[28] = $j;
						break;
					}
				}
			}
			
			//valid birth cert type
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$aData[8] =$_type;
					break;
				}
			}
			
			//--- Henry Added 20140827
			//valid record status
//			foreach($admission_cfg['Status'] as $_key => $_status){
//				if (strtoupper($kis_lang['Admission']['Status'][$_key]) == strtoupper($aData[41])){
//					$aData[41] = $_status;
//					break;
//				}
//			}
//			
//			//valid Receipt Date
//			$aData[43] = getDefaultDateFormat($aData[43]);
//			
//			//valid is notified
//
//			if(strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['no'])){
//				$aData[46] = (strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']));
//			}
			//--- convert the text to key code [end]
		    
		    $result = array();
		    
		    $sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
				SET
	     			stu.ChineseName = '".$aData[3]."',
	      			stu.EnglishName = '".$aData[4]."', 
	      			stu.Gender = '".$aData[5]."',
	      			stu.DOB = '".getDefaultDateFormat($aData[6])." 00:00:00',	
	      			stu.PlaceOfBirth = '".$aData[10]."',
	      			stu.Province = '".$aData[11]."',
	      			stu.HomeTelNo = '".$aData[12]."',
					stu.BirthCertType = '".$aData[8]."' ,
	      			stu.BirthCertNo = '".$aData[9]."' ,
	      			stu.Email = '".$aData[14]."',
	      			stu.ReligionCode = '".$aData[15]."' ,
	      			stu.Church = '".$aData[16]."',
	      			stu.LastSchool = '".$aData[39]."',
	      			stu.Address = '".$aData[13]."',
					stu.Age = '".$aData[7]."',
					stu.DateModified = NOW(),
				    stu.ModifiedBy = '".$UserID."',
					o.ApplyDayType1 = '".$aData[26]."',
					o.ApplyDayType2 = '".$aData[27]."',
					o.ApplyDayType3 = '".$aData[28]."',
					o.SiblingAppliedName = '".$aData[30]."',
					o.ExBSName = '".$aData[33]."',
					o.ExBSName2 = '".$aData[35]."',
					o.ExBSName3 = '".$aData[37]."',
					o.ExBSGradYear = '".$aData[34]."',
					o.ExBSGradYear2 = '".$aData[36]."',
					o.ExBSGradYear3 = '".$aData[38]."',
					o.Remarks =  '".$aData[40]."', 
				    o.DateModified = NOW(),
				    o.ModifiedBy = '".$UserID."'
				WHERE 
					o.ApplicationID = '".$aData[1]."'
	    	";
		    
		    $result[] = $this->db_db_query($sql);
		    
		    $sql = "
				UPDATE 
					ADMISSION_PG_INFO pg
		    	SET
	     			pg.ChineseName = '".$aData[17]."',
	     			pg.JobTitle = '".$aData[19]."',
	     			pg.HKID = '".$aData[18]."',
	     			pg.Mobile = '".$aData[20]."',
					pg.DateModified = NOW(),
					pg.ModifiedBy = '".$UserID."'
				WHERE 
					pg.ApplicationID = '".$aData[1]."' AND pg.PG_TYPE = 'F'
		   	";
		   	
		   	$result[] = $this->db_db_query($sql);
		   	
		   	$sql = "
				UPDATE 
					ADMISSION_PG_INFO pg
		    	SET
	     			pg.ChineseName = '".$aData[21]."',
	     			pg.JobTitle = '".$aData[22]."',
	     			pg.HKID = '".$aData[23]."',
	     			pg.Mobile = '".$aData[24]."',
					pg.DateModified = NOW(),
					pg.ModifiedBy = '".$UserID."'
				WHERE 
					pg.ApplicationID = '".$aData[1]."' AND pg.PG_TYPE = 'M'
		   	";
		   
		    $result[] = $this->db_db_query($sql);
		    
//		    $sql = "
//				UPDATE 
//					ADMISSION_APPLICATION_STATUS
//		    	SET
//	     			Status = '".$aData[41]."',
//					ReceiptID = '".$aData[42]."',
//					ReceiptDate = '".$aData[43]." 00:00:00',
//					Handler = '".$aData[44]."',
//					InterviewDate = '".$aData[45]."',
//					isNotified = '".$aData[46]."',
//					DateModified = NOW(),
//					ModifiedBy = '".$UserID."'
//				WHERE 
//					ApplicationID = '".$aData[1]."'
//		   	";
//		   
//		    $result[] = $this->db_db_query($sql);
		     
			$resultArr[] = !in_array(false, $result);
			//debug_pr($aData);
		}
		return $resultArr;
	}
	
	function getExportHeader($lang = ''){
		global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$headerArray = array();
		
		if($lang == 'en'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}else if($lang == 'b5'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}
		
		//for student info
		$headerArray[] = $kis_lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['applyLevel'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['chinesename'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['englishname'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['YLSYK']['birthcertno'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['TSUENWANBCKG']['nationality'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['religion'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['csm']['homephoneno'];
		$headerArray['studentInfo'][] = $kis_lang['Admission']['TSUENWANBCKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['TSUENWANBCKG']['homeaddress'].'('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')';
		$headerArray['studentInfo'][] = $kis_lang['Admission']['csm']['email'];
		$headerArray['studentInfo'][] = $Lang['Admission']['csm']['applyDayTypeShort'].'('.$Lang['Admission']['Option'].' 1)';
		$headerArray['studentInfo'][] = $Lang['Admission']['csm']['applyDayTypeShort'].'('.$Lang['Admission']['Option'].' 2)';
		$headerArray['studentInfo'][] = $Lang['Admission']['csm']['applyDayTypeShort'].'('.$Lang['Admission']['Option'].' 3)';
		
		//for parent info
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['name'].' ('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')';
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['name'].' ('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')';
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['occupation'];
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['csm']['mobile'];
		$headerArray['parentInfoF'][] = $kis_lang['Admission']['CHIUCHUNKG']['worknumber'];

		$headerArray['parentInfoM'][] = $kis_lang['Admission']['name'].' ('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')';
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['name'].' ('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')';
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['occupation'];
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['csm']['mobile'];
		$headerArray['parentInfoM'][] = $kis_lang['Admission']['CHIUCHUNKG']['worknumber'];

		$headerArray['parentInfoG'][] = $kis_lang['Admission']['name'].' ('.$kis_lang['Admission']['CHIUCHUNKG']['Chi'].')';
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['name'].' ('.$kis_lang['Admission']['CHIUCHUNKG']['Eng'].')';
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['occupation'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['csm']['mobile'];
		$headerArray['parentInfoG'][] = $kis_lang['Admission']['CHIUCHUNKG']['worknumber'];
		
		//for other info
		$headerArray['otherInfo1'][] = $kis_lang['Admission']['name'];
		$headerArray['otherInfo1'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'];
		
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['name'].'(1)';
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'].'(1)';
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['TSUENWANBCKG']['graduateYear'].'(1)';
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['name'].'(2)';
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'].'(2)';
		$headerArray['otherInfo2'][] = $kis_lang['Admission']['TSUENWANBCKG']['graduateYear'].'(2)';
		
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['name'].'(1)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'].'(1)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['YLSYK']['currentclass'].'(1)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['name'].'(2)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'].'(2)';
		$headerArray['otherInfo3'][] = $kis_lang['Admission']['YLSYK']['currentclass'].'(2)';
		
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['name'].'(1)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'].'(1)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['YLSYK']['ApplyClass'].'(1)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['YLSYK']['birthcertno'].'(1)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['name'].'(2)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['YLSYK']['RelationshipBtwApplicant'].'(2)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['YLSYK']['ApplyClass'].'(2)';
		$headerArray['otherInfo4'][] = $kis_lang['Admission']['YLSYK']['birthcertno'].'(2)';
		$headerArray['otherInfo'][] = $kis_lang['Admission']['YLSYK']['BriefingApplyInfo'];
		//for official use
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'];
//		$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
		$headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
		
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//student info header
		$exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
		for($i=0; $i < count($headerArray['studentInfo'])-2; $i++){
			$exportColumn[0][] = "";
		}
		
		//parent info header
		$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['F'].")";
		for($i=0; $i < count($headerArray['parentInfoF'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['M'].")";
		for($i=0; $i < count($headerArray['parentInfoM'])-1; $i++){
			$exportColumn[0][] = "";
		}
		$exportColumn[0][] = $Lang['Admission']['PGInfo']."(".$Lang['Admission']['PG_Type']['G'].")";
		for($i=0; $i < count($headerArray['parentInfoG'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['YLSYK']['RefBroSisInfo'];
		for($i=0; $i < count($headerArray['otherInfo1'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['YLSYK']['ExBroSisInfo'];
		for($i=0; $i < count($headerArray['otherInfo2'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['YLSYK']['CurBroSisInfo'];
		for($i=0; $i < count($headerArray['otherInfo3'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//other info header
		$exportColumn[0][] = $kis_lang['Admission']['YLSYK']['BroSisApplyInfo'];
		for($i=0; $i < count($headerArray['otherInfo4'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//official use header
		$exportColumn[0][] = $kis_lang['remarks'];
		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
			$exportColumn[0][] = "";
		}
		
		//sub header
		$exportColumn[1] = array_merge(array($headerArray[0],$headerArray[1]), $headerArray['studentInfo'], $headerArray['parentInfoF'], $headerArray['parentInfoM'], $headerArray['parentInfoG'], $headerArray['otherInfo1'], $headerArray['otherInfo2'], $headerArray['otherInfo3'], $headerArray['otherInfo4'], $headerArray['otherInfo'], $headerArray['officialUse']);
		//if($lang)
		//include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
		return $exportColumn;
	}
	
	function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg, $Lang, $kis_lang;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$relativesInfoCustRef = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID,'REF');
		$relativesInfoCustEx = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID,'EX');
		$relativesInfoCustCur = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID,'CUR');
		$relativesInfoCustApply = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID,'APPLY');
		
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$dataArray = array();
		
		//for student info
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$dataArray['studentInfo'][] = $classLevel[$otherInfo['classLevelID']];
		$dataArray['studentInfo'][] = $studentInfo['student_name_b5'];
		$dataArray['studentInfo'][] = $studentInfo['student_name_en'];
		$dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$studentInfo['gender']];
		$dataArray['studentInfo'][] = $studentInfo['dateofbirth'];
		$dataArray['studentInfo'][] = $studentInfo['placeofbirth'];
		$dataArray['studentInfo'][] = $studentInfo['birthcertno'];
		$dataArray['studentInfo'][] = $studentInfo['county'];
		$dataArray['studentInfo'][] = $studentInfo['religionOther'];
		$dataArray['studentInfo'][] = $studentInfo['homephoneno'];
		$dataArray['studentInfo'][] = $studentInfo['homeaddresschi'];
		$dataArray['studentInfo'][] = $studentInfo['homeaddress'];
		$dataArray['studentInfo'][] = $studentInfo['email'];
		$dataArray['studentInfo'][] = $Lang['Admission']['csm']['TimeSlot'][$otherInfo['ApplyDayType1']];
		$dataArray['studentInfo'][] = $Lang['Admission']['csm']['TimeSlot'][$otherInfo['ApplyDayType2']];
		$dataArray['studentInfo'][] = $Lang['Admission']['csm']['TimeSlot'][$otherInfo['ApplyDayType3']];
		
		//for parent info		
		for($i=0;$i<count($parentInfo);$i++){
			if($parentInfo[$i]['type'] == 'F'){
				$dataArray['parentInfoF'][] = $parentInfo[$i]['parent_name_b5'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['parent_name_en'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoF'][] = $parentInfo[$i]['office_tel_no'];
			}
			else if($parentInfo[$i]['type'] == 'M'){
				$dataArray['parentInfoM'][] = $parentInfo[$i]['parent_name_b5'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['parent_name_en'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoM'][] = $parentInfo[$i]['office_tel_no'];
			}
			else if($parentInfo[$i]['type'] == 'G'){
				$dataArray['parentInfoG'][] = $parentInfo[$i]['parent_name_b5'];
				$dataArray['parentInfoG'][] = $parentInfo[$i]['parent_name_en'];
				$dataArray['parentInfoG'][] = $parentInfo[$i]['occupation'];
				$dataArray['parentInfoG'][] = $parentInfo[$i]['mobile'];
				$dataArray['parentInfoG'][] = $parentInfo[$i]['office_tel_no'];
			}
		}
		
		if(count($dataArray['parentInfoF']) == 0){
			$dataArray['parentInfoF'] = array('','','');
		}
		if(count($dataArray['parentInfoM']) == 0){
			$dataArray['parentInfoM'] = array('','','');
		}
		if(count($dataArray['parentInfoG']) == 0){
			$dataArray['parentInfoG'] = array('','','');
		}
		
		//for other info
		for($i=0;$i<1;$i++){
			$dataArray['otherInfo1'][] = $relativesInfoCustRef[$i]['OthersRelativeStudiedName'];
			$dataArray['otherInfo1'][] = $relativesInfoCustRef[$i]['OthersRelativeRelationship'];
		}
		//for other info
		for($i=0;$i<2;$i++){
			$dataArray['otherInfo2'][] = $relativesInfoCustEx[$i]['OthersRelativeStudiedName'];
			$dataArray['otherInfo2'][] = $relativesInfoCustEx[$i]['OthersRelativeRelationship'];
			$dataArray['otherInfo2'][] = $relativesInfoCustEx[$i]['OthersRelativeStudiedYear'];
		}
		//for other info
		for($i=0;$i<2;$i++){
			$dataArray['otherInfo3'][] = $relativesInfoCustCur[$i]['OthersRelativeStudiedName'];
			$dataArray['otherInfo3'][] = $relativesInfoCustCur[$i]['OthersRelativeRelationship'];
			$dataArray['otherInfo3'][] = $relativesInfoCustCur[$i]['OthersRelativeStudiedYear'];
		}
		//for other info
		for($i=0;$i<2;$i++){
			$OthersRelativeApplyClass = $this->getClassLevel($relativesInfoCustApply[$i]['OthersRelativeClassPosition']);
			$OthersRelativeApplyClass = $OthersRelativeApplyClass[$relativesInfoCustApply[$i]['OthersRelativeClassPosition']];

			$dataArray['otherInfo4'][] = $relativesInfoCustApply[$i]['OthersRelativeStudiedName'];
			$dataArray['otherInfo4'][] = $relativesInfoCustApply[$i]['OthersRelativeRelationship'];
			$dataArray['otherInfo4'][] = $OthersRelativeApplyClass;
			$dataArray['otherInfo4'][] = $relativesInfoCustApply[$i]['OthersRelativeBirthCertNo'];
		}
		$dataArray['otherInfo'][] = $otherInfo['BriefingApplicationNo'];
		
		//for official use
		$dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
		$dataArray['officialUse'][] = $status['receiptID'];
		$dataArray['officialUse'][] = $status['receiptdate'];
		$dataArray['officialUse'][] = $status['handler'];
		$dataArray['officialUse'][] = $status['interviewdate'];
//		$dataArray['officialUse'][] = $status['interviewlocation'];
		$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
		$dataArray['officialUse'][] = $status['remark'];
		
		$ExportArr = array_merge(array($dataArray[0],$dataArray[1]),$dataArray['studentInfo'],$dataArray['parentInfoF'],$dataArray['parentInfoM'],$dataArray['parentInfoG'], $dataArray['otherInfo1'], $dataArray['otherInfo2'], $dataArray['otherInfo3'], $dataArray['otherInfo4'], $dataArray['otherInfo'], $dataArray['officialUse']);
		
		return $ExportArr;
	}
	function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$dataArray = array();
		
		if($tabID == 2){
//			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
//			$studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = ''; //Mobile
			$dataArray[0][] = ''; //Fax
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = ''; //Remarks
			$dataArray[0][] = $studentInfo['dateofbirth'];; //DOB
			$dataArray[0][] = (is_numeric($studentInfo['homeaddress'])?$Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']]:$studentInfo['homeaddress']); //Address
			if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
			{
				$dataArray[0][] = ''; //CardID
				if($sys_custom['SupplementarySmartCard']){
					$dataArray[0][] = ''; //CardID2
					$dataArray[0][] = ''; //CardID3
				}
			}
			if($special_feature['ava_hkid'])
				$dataArray[0][] = $studentInfo['birthcertno']; //HKID
			if($special_feature['ava_strn'])
				$dataArray[0][] = ''; //STRN
			if($plugin['medical'])
				$dataArray[0][] = ''; //StayOverNight
			$dataArray[0][] = $studentInfo['county']; //Nationality
			$dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
		}
		else if($tabID == 3){
			$hasParent = false;
			$dataCount = array();
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			for($i=0;$i<count($parentInfo);$i++){
				if($parentInfo[$i]['type'] == 'F' && !$hasParent){
					$dataArray[0] = array();
					$dataArray[0][] = ''; //UserLogin
					$dataArray[0][] = ''; //Password
					$dataArray[0][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[0][] = 'M'; //Gender
					$dataArray[0][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[0][] = ''; //Fax
					$dataArray[0][] = ''; //Barcode
					$dataArray[0][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[0][] = ''; //HKID
					$dataArray[0][] = ''; //StudentLogin1
					$dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[0][] = ''; //StudentLogin2
					$dataArray[0][] = ''; //StudentEngName2
					$dataArray[0][] = ''; //StudentLogin3
					$dataArray[0][] = ''; //StudentEngName3
					//$hasParent = true;
					
					$dataCount[0] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'M' && !$hasParent){
					$dataArray[1] = array();
					$dataArray[1][] = ''; //UserLogin
					$dataArray[1][] = ''; //Password
					$dataArray[1][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[1][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[1][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[1][] = 'F'; //Gender
					$dataArray[1][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[1][] = ''; //Fax
					$dataArray[1][] = ''; //Barcode
					$dataArray[1][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[1][] = ''; //HKID
					$dataArray[1][] = ''; //StudentLogin1
					$dataArray[1][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[1][] = ''; //StudentLogin2
					$dataArray[1][] = ''; //StudentEngName2
					$dataArray[1][] = ''; //StudentLogin3
					$dataArray[1][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[1] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'G' && !$hasParent){
					$dataArray[2] = array();
					$dataArray[2][] = ''; //UserLogin
					$dataArray[2][] = ''; //Password
					$dataArray[2][] = $parentInfo[$i]['email']; //UserEmail
					$dataArray[2][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[2][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[2][] = ''; //Gender
					$dataArray[2][] = $parentInfo[$i]['mobile']; //Mobile
					$dataArray[2][] = ''; //Fax
					$dataArray[2][] = ''; //Barcode
					$dataArray[2][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[2][] = ''; //HKID
					$dataArray[2][] = ''; //StudentLogin1
					$dataArray[2][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[2][] = ''; //StudentLogin2
					$dataArray[2][] = ''; //StudentEngName2
					$dataArray[2][] = ''; //StudentLogin3
					$dataArray[2][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[2] = ($parentInfo[$i]['email']?1:0)+($parentInfo[$i]['parent_name_en']?1:0)+($parentInfo[$i]['parent_name_b5']?1:0)+($parentInfo[$i]['mobile']?1:0);
				}
			}
			if($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]){
				$tempDataArray = $dataArray[0];
			}
			else if($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]){
				$tempDataArray = $dataArray[1];
			}
			else if($dataCount[2] > 0){
				$tempDataArray = $dataArray[2];
			}
			$dataArray = array();
			$dataArray[0] = $tempDataArray;
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."'";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();
		$inputby = $_SESSION['UserID'];
		$result = array();
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		if($subject == ''){
			$email_subject = "元朗三育幼稚園入學申請通知";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	public function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();

		if($subject == ''){
			$email_subject = "元朗三育幼稚園入學申請通知";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	
	function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
			//check date
			if ($aData[4] =='' && $aData[5] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
		       list($year , $month , $day) = explode('-',$aData[4]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[4] =='' && $aData[5] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."' AND trim(s.birthCertNo) = '".trim($aData[3])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[4] = getDefaultDateFormat($aData[4]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[4]." ".$aData[5]."',
				InterviewLocation = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1){
		$status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND st.status in ('".implode("','",$selectStatusArr)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		
		$sql='Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied, o.BriefingApplicationNo, r.Type, s.Address, s.AddressChi From ADMISSION_OTHERS_INFO as o 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel 
				LEFT JOIN ADMISSION_RELATIVES_AT_SCH_INFO as r ON r.ApplicationID = o.ApplicationID 
				where o.ApplyYear = "'.$selectSchoolYearID.'" '.$status_cond.' 
				group by o.ApplicationID 
				order by FIELD( r.Type,"APPLY","CUR","EX",NULL) desc, o.BriefingApplicationNo desc, y.YearName desc';
				
		$result = $this->returnArray($sql);
		
		$sundayMorningApplicant = array();
		$firstWeekdayMorningApplicant = array();
		$firstWeekdayAfternoonApplicant = array();
		$otherApplicant = array();
		$AssignedApplicant = array();
		
		for($i=0; $i<sizeof($result); $i++){
			if($result[$i]['BriefingApplicationNo'] != '' && !in_array($result[$i]['ApplicationID'], $AssignedApplicant)){
				$sundayMorningApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
				$AssignedApplicant[] = $result[$i]['ApplicationID'];
			}
			else if(($result[$i]['Type'] == 'APPLY' || $result[$i]['Type'] == 'CUR' || $result[$i]['Type'] == 'EX') && !in_array($result[$i]['ApplicationID'], $AssignedApplicant)){
				$firstWeekdayMorningApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
				$AssignedApplicant[] = $result[$i]['ApplicationID'];
			}
			else if((strpos(strtoupper($result[$i]['Address']), 'YUEN LONG') || strpos(strtoupper($result[$i]['Address']), 'TIN SHUI WAI') || strpos($result[$i]['AddressChi'], '元朗') || strpos($result[$i]['AddressChi'], '天水圍')) && !in_array($result[$i]['ApplicationID'], $AssignedApplicant)){
				$firstWeekdayAfternoonApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
				$AssignedApplicant[] = $result[$i]['ApplicationID'];
			}
			else{
				$otherApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
				$AssignedApplicant[] = $result[$i]['ApplicationID'];
			}
		}
		
//		$allApplicant = array();
//		
//		for($i=0; $i<sizeof($result); $i++){
//			$allApplicant[] = array('ApplicationID' => $result[$i]['ApplicationID'], 'ClassLevel' => $result[$i]['ApplyLevel']);
//		}
		
//		$sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
//				FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
//				ORDER BY a.Date";
//		
//		$arrangmentRecord = $this->returnArray($sql);
		
		$result = array();
		
		$TwinsAssignedApplicant = array();
		
		$sql = "SELECT RecordID, Quota, Date, StartTime, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL 
				AND Date = '2018-11-04' ORDER BY Date, StartTime, GroupName";
		$interviewRecordIDArr = $this->returnArray($sql);

		for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
			
			if(!$sundayMorningApplicant){
				break;
			}
//			if($interviewRecordIDArr[$j]['ClassLevelID'] && $interviewRecordIDArr[$j]['ClassLevelID'] != $sundayMorningApplicant[0]['ClassLevel']){
//				continue;
//			}
			$previousClassLevel = $sundayMorningApplicant[0]['ClassLevel'];
			$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
			$result[] = $this->db_db_query($sql);
			
			for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$sundayMorningApplicant[0]['ApplicationID']."' ";
				$result[] = $this->db_db_query($sql);
				array_shift($sundayMorningApplicant);
				if($previousClassLevel != $sundayMorningApplicant[0]['ClassLevel']){
					break;
				}
			}

		}
		
		$sql = "SELECT RecordID, Quota, Date, StartTime, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL 
				AND (Date >= '2018-11-06' AND Date <= '2018-11-09') AND StartTime >= '09:30:00' AND StartTime <= '10:30:00' ORDER BY Date, StartTime, GroupName";
		$interviewRecordIDArr = $this->returnArray($sql);

		for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
			
			if(!$firstWeekdayMorningApplicant){
				break;
			}
//			if($interviewRecordIDArr[$j]['ClassLevelID'] && $interviewRecordIDArr[$j]['ClassLevelID'] != $sundayMorningApplicant[0]['ClassLevel']){
//				continue;
//			}
			$previousClassLevel = $firstWeekdayMorningApplicant[0]['ClassLevel'];
			$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
			$result[] = $this->db_db_query($sql);
			
			for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$firstWeekdayMorningApplicant[0]['ApplicationID']."' ";
				$result[] = $this->db_db_query($sql);
				array_shift($firstWeekdayMorningApplicant);
				if($previousClassLevel != $firstWeekdayMorningApplicant[0]['ClassLevel']){
					break;
				}
			}

		}
		
		$sql = "SELECT RecordID, Quota, Date, StartTime, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL 
				AND (Date >= '2017-11-19' AND Date <= '2017-11-22') ORDER BY Date, StartTime, GroupName";
		$interviewRecordIDArr = $this->returnArray($sql);

		for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
			
			if(!$firstWeekdayAfternoonApplicant){
				break;
			}
//			if($interviewRecordIDArr[$j]['ClassLevelID'] && $interviewRecordIDArr[$j]['ClassLevelID'] != $sundayMorningApplicant[0]['ClassLevel']){
//				continue;
//			}
			$previousClassLevel = $firstWeekdayAfternoonApplicant[0]['ClassLevel'];
			$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
			$result[] = $this->db_db_query($sql);
			
			for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$firstWeekdayAfternoonApplicant[0]['ApplicationID']."' ";
				$result[] = $this->db_db_query($sql);
				array_shift($firstWeekdayAfternoonApplicant);
				if($previousClassLevel != $firstWeekdayAfternoonApplicant[0]['ClassLevel']){
					break;
				}
			}

		}
		
		$allApplicant = array_merge($sundayMorningApplicant, $firstWeekdayMorningApplicant, $firstWeekdayAfternoonApplicant, $otherApplicant);
		
		$sql = "SELECT RecordID, Quota, Date, StartTime, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL 
				ORDER BY FIELD( Date,'2017-11-12','2017-11-05') desc, Date, StartTime, GroupName";
		$interviewRecordIDArr = $this->returnArray($sql);

		for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
			
			if(!$allApplicant){
				break;
			}
			
			$sql = "select count(*) from ADMISSION_OTHERS_INFO where InterviewSettingID".($round>1?$round:"")." = ".$interviewRecordIDArr[$j]['RecordID'];
			$countApplicant = $this->returnArray($sql);
			$interviewRecordIDArr[$j]['Quota'] = $interviewRecordIDArr[$j]['Quota'] - $countApplicant[0][0];

			if($interviewRecordIDArr[$j]['Quota'] == 0 || $interviewRecordIDArr[$j]['ClassLevelID'] && $interviewRecordIDArr[$j]['ClassLevelID'] != $allApplicant[0]['ClassLevel']){
				continue;
			}

			$previousClassLevel = $allApplicant[0]['ClassLevel'];
			if(!$interviewRecordIDArr[$j]['ClassLevelID']){
				$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
				$result[] = $this->db_db_query($sql);
			}
			
			for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
				$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
				$result[] = $this->db_db_query($sql);
				array_shift($allApplicant);
				if($previousClassLevel != $allApplicant[0]['ClassLevel']){
					break;
				}
			}

		}
				
//		for($i=0; $i<sizeof($arrangmentRecord); $i++){		
//				$sql = "SELECT RecordID, Quota, Date, StartTime FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY Date, StartTime, GroupName";
//				$interviewRecordIDArr = $this->returnArray($sql);
				
//				$insertPosition = 1;
//				$newInterviewRecordIDArr = array();
//				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
//					if($interviewRecordIDArr[$j]['Date'] == '2016-06-13'){
//						array_splice( $interviewRecordIDArr, $insertPosition, 0, array($interviewRecordIDArr[$j])); 
//						unset($interviewRecordIDArr[$j]);
//						$insertPosition++;
//					}
//				}
				
//				for($j=0; $j<sizeof($interviewRecordIDArr); $j++){
//					
//					if(!$allApplicant){
//						break;
//					}
//					
//					$previousClassLevel = $allApplicant[0]['ClassLevel'];
//					$sql ="UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '".$previousClassLevel."' WHERE RecordID = '".$interviewRecordIDArr[$j]['RecordID']."' ";
//					$result[] = $this->db_db_query($sql);
//					
//					for($k=0; $k<$interviewRecordIDArr[$j]['Quota']; $k++){
//						$sql ="UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID".($round>1?$round:'')." = '".$interviewRecordIDArr[$j]['RecordID']."' Where ApplicationID = '".$allApplicant[0]['ApplicationID']."' ";
//						$result[] = $this->db_db_query($sql);
//						array_shift($allApplicant);
//						if($previousClassLevel != $allApplicant[0]['ClassLevel']){
//							break;
//						}
//					}
//	
//				}
//		}
		
		//handling twins swraping [start]
		$sql='Select o.ApplicationID, s.BirthCertNo, s.ChineseName, s.EnglishName, r.Name, o.ApplyLevel, r.ClassPosition, r.BirthCertNo as RefBirthCertNo, o.InterviewSettingID'.($round>1?$round:'').' as InterviewSettingID From ADMISSION_OTHERS_INFO as o 
				JOIN ADMISSION_RELATIVES_AT_SCH_INFO as r ON o.ApplicationID = r.ApplicationID 
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID 
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" AND r.Type = "APPLY" order by y.YearName desc';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
			for($j=0; $j<sizeof($twinsResult); $j++){
				if(/*(trim($twinsResult[$i]['Name']) == trim($twinsResult[$j]['ChineseName']) || trim($twinsResult[$i]['Name']) == trim($twinsResult[$j]['EnglishName']))*/trim($twinsResult[$i]['RefBirthCertNo']) == trim($twinsResult[$j]['BirthCertNo']) && $twinsResult[$i]['ClassPosition'] == $twinsResult[$j]['ApplyLevel'] && $twinsResult[$i]['InterviewSettingID'] != $twinsResult[$j]['InterviewSettingID'] && !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)){
					
					$sql = 'Select RecordID From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" ';
					$originalSession = current($this->returnArray($sql));
					
					$sql = 'Select RecordID From ADMISSION_INTERVIEW_SETTING where RecordID = "'.$twinsResult[$j]['InterviewSettingID'].'" ';
					$newSession = current($this->returnArray($sql));
					
					if($newSession){
						$sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID LEFT JOIN ADMISSION_RELATIVES_AT_SCH_INFO as r ON o.ApplicationID = r.ApplicationID where o.InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" AND r.ApplicationID IS NULL';
						$swapApplicant = current($this->returnArray($sql));
						if($swapApplicant){
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$originalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.($round>1?$round:'').' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$i]['ApplicationID'].'" ';
							$updateResult = $this->db_db_query($sql);
							$assignedTwins[] = $twinsResult[$j]['ApplicationID'];
						}
					}
				}
			}
		}
		
		//handling twins swraping [end]
		
		return !in_array(false,$result);
	}
	
	function getCustSelection($status='',$name="custSelection",$auto_submit=true,$isAll=true, $isMultiSelect=false){
		global $admission_cfg,$kis_lang;
    	$x = '<select name="'.$name.'" id="'.$name.'" '.($auto_submit?'class="auto_submit"':'').' '.($isMultiSelect?'multiple':'').'>'; 
    	$x .= ($isAll)?'<option value=""'.($status==''?' selected="selected"':'').'>'.$kis_lang['allstatus'].'</option>':'';
	
		$x .= '<option value="HasSiblingsOrRelatives"'.($status=='HasSiblingsOrRelatives'?' selected="selected"':'').'>';
		$x .= $kis_lang['Admission']['YLSYK']['ExBroSisInfo'];
		$x .= '</option>';

		$x .= '</select>'; 	
		return $x;
	}
}
?>