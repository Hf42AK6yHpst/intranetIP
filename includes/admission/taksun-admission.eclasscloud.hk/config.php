<?php
	//using: Pun
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

    ######## Basic config START ########
	$admission_cfg['Status'] = array(); //Please also define lang in admission_lang
	$admission_cfg['Status']['pending']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['waitingforinterview']	= 3;
	$admission_cfg['Status']['gotosecondinterview']	= 6;
	$admission_cfg['Status']['gotothirdinterview']	= 7;
	$admission_cfg['Status']['confirmed']	= 4;
	$admission_cfg['Status']['cancelled']	= 5;
	$admission_cfg['Status']['KTLMS_admitted']	= 8;
	$admission_cfg['Status']['KTLMS_reserve']	= 9;
	$admission_cfg['Status']['KTLMS_notadmitted']	= 10;

	$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
	$admission_cfg['PaymentStatus']['OtherPayment']	= 2;

	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['Lang'][0] = 'en';
	$admission_cfg['Lang'][1] = 'gb';
	$admission_cfg['Lang'][2] = 'b5';

	$admission_cfg['interview_arrangment']['interview_group_type'] = 'Room';
	$admission_cfg['interview_arrangment']['interview_group_name'] = array('201', '202', '204', '301', '302', '304', '305', '306', '307', '401', '402', '404', '405', '406', '407');
    ######## Basic config END ########



	######## Cust config START ########
	$admission_cfg['ReligionType']['Catholic'] = 'Catholic';
	$admission_cfg['ReligionType']['Other'] = 'Other';
	$admission_cfg['ReligionType']['None'] = 'None';
	$admission_cfg['SchoolNet']['Default'] = 'Default';
	$admission_cfg['SchoolNet']['Other'] = 'Other';
	$admission_cfg['Wish']['1st'] = 'FirstWish';
	$admission_cfg['Wish']['2nd'] = 'SecondWish';
	$admission_cfg['Wish']['3rd'] = 'ThirdWish';
	$admission_cfg['Wish']['Other'] = 'OtherWish';
	$admission_cfg['Wish']['DifferentDistrict'] = 'DifferentDistrict';
	$admission_cfg['Wish']['NoSelection'] = 'NoSelection';
	$admission_cfg['Wish']['NotJoinCentralAllocation'] = 'NotJoinCentralAllocation';
	######## Cust config END ########



	######## Fixed config START ########
	$admission_cfg['PrintByPDF'] = 1;
	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
	$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	if($plugin['eAdmission_devMode']){
    	$admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
    	$admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}
	######## Fixed config END ########