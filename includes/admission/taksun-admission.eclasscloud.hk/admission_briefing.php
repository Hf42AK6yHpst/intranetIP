<?php

class admission_briefing extends admission_briefing_base{
	
    function generateApplicantID($schoolYearID){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$schoolYearID)), -2);
		
		$sql = "SELECT 
			ABAI.ApplicantID 
		FROM 
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		AND
		(
			ABS.SchoolYearID = '{$schoolYearID}'
		AND
			ABAI.ApplicantID LIKE 'TED{$yearStart}%'
		)
		ORDER BY ABAI.Briefing_ApplicantID DESC LIMIT 1";
		$rs = $this->returnVector($sql);
		
		if($rs[0] == null){
			$lastApplicationId = "TED{$yearStart}000";
		}else{
		    $lastApplicationId = $rs[0];
		}
		

		preg_match('/TED(\d+)/', $lastApplicationId, $matches);
		$nextApplicationId = 'TED' . ($matches[1] + 1);
		
		return $nextApplicationId;
	}

	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = '')
	{
	    global $intranet_root, $kis_lang, $PATH_WRT_ROOT, $Lang;
	    include_once($intranet_root."/includes/libwebmail.php");
	    $libwebmail = new libwebmail();
	
	    $from = $libwebmail->GetWebmasterMailAddress();
	    $result = array();
	
	    ######## Get Applicant Info START ########
	    $applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
	    $Email = ($email)?$email:$applicant['Email'];
	    $ApplicantID = $applicant['ApplicantID'];
	    $DeleteRecordPassKey = $applicant['DeleteRecordPassKey'];
	    ######## Get Applicant Info END ########
	
	    ######## Setup Email Content START ########
	    if($subject == ''){
	        $email_subject = "簡介會申請通知 Briefing Session Application Notification";
	    }
	    else{
	        $email_subject = $subject;
	    }
	
	    if($message == ''){
	        if($Briefing_ApplicantID == 0){
	            return true;
	            /* Fail case no email * /
	             $email_message = <<<EMAIL
	             {$Lang['Admission']['msg']['applicationnotcomplete']}
	             {$Lang['Admission']['msg']['tryagain']}
	             <br /><br />
	             Application is Not Completed. Please try to apply again!
	             EMAIL;
	             /* */
	        }else{
	            $deleteRecordLink = $this->getApplicantDeleteLink($Briefing_ApplicantID, $DeleteRecordPassKey);
	            $email_message = <<<EMAIL
					<font color="green">
						德信體驗日2019的報名已遞交，申請編號為
					</font>
					<font size="5">
						<u>{$ApplicantID}</u>
					</font>
			
					<br />
					
					<u>注意事項︰</u>
					<ol>
					    <li>報名一經遞交，獲取申請編號後，即代表申請已被確認，家長<span style="color: red;">請勿</span>再次報名登記。</li>
					    <li>如本校發現重複報名，校方有權取消有關家長的參與資格而不作另行通知，敬請家長合作。</li>
					    <li>由於本校場地有限，每個家庭只限一位家長陪同孩子出席。</li>
					    <li>當天不設劃位，禮堂座位先到先得，如禮堂座位不足夠，校方會安排家長到雨天操場或課室觀看直播。</li>
					    <li>如有特別需要，請家長先致電學校（2367 3446）與李英超副校長聯絡。</li>
					    <li>如家長臨時有事未能參與，請務必登入以下網頁取消申請，讓其他家長參與。</li>
					    <li>活動於2019年9月7日（星期六）早上9:15開始，請家長準時報到。</li>
					</ol>
					
					<br />
					<span>如需要取消申請，請按此連結︰</span><br />
					{$deleteRecordLink}
EMAIL;
	        }
	    }else{
	        $email_message = $message;
	    }
	    ######## Setup Email Content END ########
	
	    $sent_ok = true;
	    if($Email != '' && intranet_validateEmail($Email)){
	        $sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($Email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
	    }else{
	        $sent_ok = false;
	    }
	    	
	    return $sent_ok;
	}
} // End Class