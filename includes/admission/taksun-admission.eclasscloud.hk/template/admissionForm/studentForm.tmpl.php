<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}
.col2div{
    display:inline-block;
    width:48%;
}
@media (min-width: 768px) and (max-width: 991.98px) {
    .col2div{
        width:100%;
    }
}

.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>

<h1>
    <?=$Lang['Admission']['studentInfo']?>
</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['chinesename']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentChineseName']?>
		<?php }else{ ?>
    		<input name="StudentChineseName" type="text" id="StudentChineseName" class="textboxtext"  value="<?=$StudentInfo['ChineseName']?>"/>
		<?php } ?>
	</td>

	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['englishname']?> <font color="blue">(<?=$Lang['Admission']['UCCKE']['SameAsHKID']?>)</font>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentEnglishName']?>
		<?php }else{ ?>
			<input name="StudentEnglishName" type="text" id="StudentEnglishName" class="textboxtext"  value="<?=$StudentInfo['EnglishName']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['dateofbirth']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input
				type="text"
				class="textboxtext datepicker"
				placeholder="YYYY-MM-DD"
				maxlength="10"
				size="15"
				style="width: 110px;"

				id="StudentDateOfBirth"
				name="StudentDateOfBirth"
				value="<?=$StudentInfo['DOB'] ?>"
			/>
		<?php } ?>
	</td>

	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['gender']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['genderType']['M']?> (<?=$Lang['Admission']['TSS']['GenderRemarks'] ?>)
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" checked>
			<label for="StudentGender1"><?=$Lang['Admission']['genderType']['M']?> (<?=$Lang['Admission']['TSS']['GenderRemarks'] ?>)</label>
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['BirthCertType']?> <font color="blue">(<?=$Lang['Admission']['SHCK']['birthCertNoHint'] ?>)</font>
	</td>
	<td>
		<?php
		if($IsConfirm){
		    echo $formData['StudentBirthCertNo'];
		}else{
		?>
			<table>
				<tr>
					<td>
            			<?php if($BirthCertNo){ ?>
            				<?php if($IsUpdate){ ?>
            					<input name="StudentBirthCertNo" value="<?=$BirthCertNo ?>" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none;"/>
            				<?php }else{ ?>
            					<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="" value="<?=$BirthCertNo?>"/>
            				<?php } ?>
            			<?php }else{ ?>
                			<?php if($StudentInfo['BirthCertNo']){ ?>
                    			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="border: none;" value="<?=$StudentInfo['BirthCertNo'] ?>" readonly/>
                			<?php }else{ ?>
                    			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30"/>
	            			<?php } ?>
            			<?php } ?>
        			</td>
				</tr>
			</table>

		<?php
		}
		?>
	</td>

   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['placeofbirth']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['StudentPlaceOfBirth'];
		}else{
		?>
			<input id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" class="textboxtext inputselect" value="<?=$StudentInfo['PlaceOfBirth'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['TSS']['homeAddress']?> (<?=$Lang['Admission']['Languages']['Chinese'] ?>)
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentAddressChi'];
		}else{
		?>
			<input id="StudentAddressChi" name="StudentAddressChi" class="textboxtext" value="<?=$StudentInfo['AddressChi'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['TSS']['homeAddress']?> (<?=$Lang['Admission']['Languages']['English'] ?>)
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentAddress'];
		}else{
		?>
			<input id="StudentAddress" name="StudentAddress" class="textboxtext" value="<?=$StudentInfo['Address'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['TSS']['religion']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
		    if($formData['Religion'] == $admission_cfg['ReligionType']['Catholic']){
        ?>
        		<?=$Lang['Admission']['TSS']['Catholic'] ?>
            	<br />
            	<?=$Lang['Admission']['TSS']['CertificateOfBaptism'] ?>:
            	<?=$formData['CertificateOfBaptism'] ?>
            	<br />
            	<?=$Lang['Admission']['TSS']['AffiliatedParish'] ?>:
            	<?=$formData['AffiliatedParish'] ?>
        <?php
            }elseif($formData['Religion'] == $admission_cfg['ReligionType']['Other']){
        ?>
        		<?=$formData['ReligionOther'] ?>
        <?php
            }else{
        ?>
        		(<?=$Lang['Admission']['TSS']['NoReligion'] ?>)
        <?php
            }
		}else{
		?>
    		<div>
    			<?php $checked = ($StudentInfo['Church'] == $admission_cfg['ReligionType']['Catholic'])? 'checked' : '' ?>
    			<input type="radio" id="Religion_Catholic" name="Religion" value="<?=$admission_cfg['ReligionType']['Catholic'] ?>" <?=$checked ?>/>
    			<label for="Religion_Catholic"><?=$Lang['Admission']['TSS']['Catholic'] ?></label>

    			<div id="ReligionCatholicDetails" style="margin: 5px 25px; display:none;">
        			<label for="CertificateOfBaptism"><?=$Lang['Admission']['TSS']['CertificateOfBaptism'] ?></label>
        			<input id="CertificateOfBaptism" name="CertificateOfBaptism" value="<?=$allCustInfo['CertificateOfBaptism'][0]['Value'] ?>" style="margin-bottom: 5px;" />
        			(<?=$Lang['Admission']['TSS']['AttachCertificateOfBaptism'] ?>)
        			<br />
        			<label for="AffiliatedParish"><?=$Lang['Admission']['TSS']['AffiliatedParish'] ?></label>
        			<input id="AffiliatedParish" name="AffiliatedParish" value="<?=$allCustInfo['AffiliatedParish'][0]['Value'] ?>" />
    			</div>
			</div>
    		<div>
    			<?php $checked = ($StudentInfo['Church'] == $admission_cfg['ReligionType']['Other'])? 'checked' : '' ?>
    			<input type="radio" id="Religion_Other" name="Religion" value="<?=$admission_cfg['ReligionType']['Other'] ?>" <?=$checked ?>/>
    			<label for="Religion_Other"><?=$Lang['Admission']['TSS']['Others'] ?></label>

    			(
        			<label for="ReligionOther"><?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>: </label>
        			<input id="ReligionOther" name="ReligionOther" value="<?=$StudentInfo['ReligionOther'] ?>" />
    			)
			</div>
    		<div>
    			<?php $checked = ($StudentInfo['Church'] == $admission_cfg['ReligionType']['None'])? 'checked' : '' ?>
    			<input type="radio" id="Religion_No" name="Religion" value="<?=$admission_cfg['ReligionType']['None'] ?>" <?=$checked ?>/>
    			<label for="Religion_No"><?=$Lang['Admission']['TSS']['NoReligion'] ?></label>
			</div>
		<?php
		}
		?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['Referrer'] ?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Referrer'];
		}else{
		?>
			<input id="Referrer" name="Referrer" class="textboxtext" value="<?=$allCustInfo['Referrer'][0]['Value'] ?>" />
		<?php
		}
		?>
	</td>

	<td class="field_title">
		<?=$Lang['Admission']['TSS']['Relationship'] ?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['ReferrerRelationship'];
		}else{
		?>
			<input id="ReferrerRelationship" name="ReferrerRelationship" class="textboxtext" value="<?=$allCustInfo['ReferrerRelationship'][0]['Value'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$Lang['Admission']['contactEmail']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['StudentEmail'];
		}else{
		?>
			<input id="StudentEmail" name="StudentEmail" class="textboxtext" value="<?=$StudentInfo['Email'] ?>" />
		<?php
		}
		?>
	</td>
</tr>
</table>

<?php if($classLevelID){ ?>
	<h1>
    	<?=$Lang['Admission']['applyLevel'] ?>
	</h1>
	<table class="form_table" style="font-size: 13px">
		<tr>
			<td class="field_title">
				<?=$Lang['Admission']['applyLevel'] ?>
				<?=$LangEn['Admission']['applyLevel'] ?>
			</td>
			<td>
				<?=$classLevel ?>
				<input name="sus_status" type="hidden" id="sus_status" class="textboxtext" value="<?=$classLevelID ?>" />
			</td>
		</tr>
	</table>
<?php } ?>

<script>
<?php if(!$IsConfirm){ ?>
    $(function(){
    	'use strict';

    	$('[name="Religion"]').change(function(){
    		if($(this).prop('checked') && $(this).val() == '<?=$admission_cfg['ReligionType']['Catholic'] ?>'){
    			$('#ReligionCatholicDetails').slideDown();
    		}else if($(this).prop('checked')){
    			$('#ReligionCatholicDetails').slideUp();
    		}
    	}).change();

    	$('#StudentEnglishName, #StudentAddress, #StudentBirthCertNo').on('keyup blur', function(){
    		$(this).val($(this).val().toUpperCase());
    	});

    	var lastStudentBirthCertNo='';
    	$('#StudentBirthCertNo').on('keyup blur', function(e){
        	var val = $(this).val().toUpperCase();
    		if(val != '' && !/^[A-Z]\d*(?:\d[A-Z])?$/.test(val)){
        		$(this).val(lastStudentBirthCertNo);
        		return false;
    		}
    		$(this).val(val);
    		lastStudentBirthCertNo = val;
    	});
    });
<?php } ?>

/**** Check form START ****/
function check_student_form(){
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	dOBRange = dOBRange || ['',''];
	/******** Basic init END ********/


	/**** Name START ****/
	if(
		!checkNaNull($('#StudentChineseName').val()) &&
		!checkIsChineseCharacter($('#StudentChineseName').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");
		$('#StudentChineseName').focus();
		return false;
	}

	if($('#StudentEnglishName').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>");
		$('#StudentEnglishName').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishName').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishName').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>");
		$('#StudentEnglishName').focus();
		return false;
	}
	/**** Name END ****/

	/**** DOB START ****/
	if( !$('#StudentDateOfBirth').val().match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/) ){
		if($('#StudentDateOfBirth').val()!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>");
		}else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>");
		}

		$('#StudentDateOfBirth').focus();
		return false;
	}
	if(
		(
			dOBRange[0] !='' &&
			$('#StudentDateOfBirth').val() < dOBRange[0]
		) || (
			dOBRange[1] !='' &&
			$('#StudentDateOfBirth').val() > dOBRange[1]
		)
	){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>");
		$('#StudentDateOfBirth').focus();
		return false;
	}
	/**** DOB END ****/

	/**** Gender START ****/
	if($('input[name=StudentGender]:checked').length == 0){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>");
		$('input[name=StudentGender]:first').focus();
		return false;
	}
	/**** Gender END ****/

	/**** Personal Identification START ****/
	if($('#StudentBirthCertNo').val().trim()==''){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterPersonalIdentificationNo']?>");
		$('#StudentBirthCertNo').focus();
		return false;
	}
// 	if(!check_hkid(form1.StudentBirthCertNo.value)){
//    	alert("<?=$Lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>");
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }

    if(!isUpdatePeriod && checkBirthCertNo() > 0){
    	alert("<?=$Lang['Admission']['PICLC']['msg']['duplicateBirthCertNo']?>");
    	form1.StudentBirthCertNo.focus();
    	return false;
    }
	/**** Personal Identification END ****/

	/**** PlaceOfBirth START ****/
	if($('#StudentPlaceOfBirth').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>");
		$('#StudentPlaceOfBirth').focus();
		return false;
	}
	/**** PlaceOfBirth END ****/

	/**** Address START ****/
	if($('#StudentAddress').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterhomeaddress']?>");
		$('#StudentAddress').focus();
		return false;
	}
	/**** Address END ****/

	/**** Religion START ****/
	if($('[name="Religion"]:checked').length == 0){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectReligion']?>");
		$('#Religion_Catholic').focus();
		return false;
	}

	if($('#Religion_Catholic:checked').length == 1){
    	if($('#CertificateOfBaptism').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterCertificateOfBaptism']?>");
    		$('#CertificateOfBaptism').focus();
    		return false;
    	}
    	if($('#AffiliatedParish').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterAffiliatedParish']?>");
    		$('#AffiliatedParish').focus();
    		return false;
    	}
	}
	if($('#Religion_Other:checked').length == 1){
    	if($('#ReligionOther').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterExtraInfo']?>");
    		$('#ReligionOther').focus();
    		return false;
    	}
	}
	/**** Religion END ****/

	/**** Referrer START ****/
	if($('#Referrer').val().trim()!='' && $('#ReferrerRelationship').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterrelationship']?>");
		$('#ReferrerRelationship').focus();
		return false;
	}
	/**** Referrer END ****/

	/**** Email START ****/
	if($('#StudentEmail').val().trim()==''){
		alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	if($('#StudentEmail').val().trim()!='' && !re.test($('#StudentEmail').val().trim())){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	/**** Email END ****/

	return true;
}
/**** Check form END ****/
</script>