<h1>
    <?=$Lang['Admission']['PGInfo']?>
</h1>
<table class="form_table parentInfo" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:24%">
    </colgroup>
    <!--tr>
    	<td>
    		<span>If no information, e.g. single parent family, fill in '<font style="color:red;">N.A.</font>'</span>
    		<br/>
    		<span>如沒有資料，例如單親家庭，請輸入 '<font style="color:red;">N.A.</font>'</span>
    	</td>
    </tr-->
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PG_Type']['F']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PG_Type']['M']?></td>
		<td class="form_guardian_head"><?=$Lang['Admission']['PG_Type']['G']?></td>
	</tr>
	
	
	<!--tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['englishname']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_name = "EnglishName";
		    $formName_name = "G{$index}EnglishName";
		?>
    		<td class="form_guardian_field">
        		<?php if($IsConfirm){ ?>
        			<?=$formData[$fieldName_name] ?>
				<?php }else{ ?>
        			<input name="<?=$formName_name ?>" type="text" id="<?=$formName_name ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_name] ?>" data-parent="<?=$index ?>" />
				<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr-->
	
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$Lang['Admission']['chinesename']?> 
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_name = "ChineseName";
		    $formName_name = "G{$index}ChineseName";
		?>
    		<td class="form_guardian_field">
        		<?php if($IsConfirm){ ?>
        			<?=$formData[$formName_name] ?>
				<?php }else{ ?>
        			<input name="<?=$formName_name ?>" type="text" id="<?=$formName_name ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_name] ?>" data-parent="<?=$index ?>" />
				<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['phoneno']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_mobile1 = "Mobile";
		    $formName_mobile1 = "G{$index}Mobile";
		    $fieldName_mobile2 = "OfficeTelNo";
		    $formName_mobile2 = "G{$index}OfficeTelNo";
		?>
    		<td class="form_guardian_field">
        		<?php if($IsConfirm){ ?>
        			<?=$formData[$formName_mobile1] ?>, <?=$formData[$formName_mobile2] ?>
				<?php }else{ ?> 
        			<div class="col2div">
                		<label for="<?=$formName_mobile1 ?>">
                    		(1)
                		</label>
            			<input name="<?=$formName_mobile1 ?>" type="text" id="<?=$formName_mobile1 ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_mobile1] ?>" data-parent="<?=$index ?>" />
            		</div>
            		&nbsp; 	
        			<div class="col2div">
                		<label for="<?=$formName_mobile2 ?>">
                    		(2)
                		</label>
            			<input name="<?=$formName_mobile2 ?>" type="text" id="<?=$formName_mobile2 ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_mobile2] ?>" data-parent="<?=$index ?>" />
            		</div>		
				<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['occupation']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $formName = "G{$index}Company";
		?>
    		<td class="form_guardian_field">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType]['Company'] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['TSS']['jobPosition']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "JobPosition";
		    $formName = "G{$index}JobPosition";
		?>
    		<td class="form_guardian_field">
        		<?php if($IsConfirm){
        		    echo $formData[$formName];
        		}else{?>
        			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
        		<?php } ?>
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['TSS']['Relationship']?>
		</td>
		
		<td class="form_guardian_field">
			<?=$Lang['Admission']['PG_Type']['F']?>
		</td>
		<td class="form_guardian_field">
			<?=$Lang['Admission']['PG_Type']['M']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3Relationship'];
    		}else{?>
				<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" value="<?=$ParentInfo['G']['Relationship'] ?>" data-parent="<?=$index ?>" />
    		<?php } ?>
		</td>
	</tr>
	
</table>

<script>
function check_parent_form(){
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	
	var isValid = true;
	/******** Basic init END ********/
	
	
	/**** All empty START ****/
	var parentHasInfoArr = [];
	var _text = '';
	$('.parentInfo input').each(function(){
		if($(this).val().trim() != ''){
			parentHasInfoArr.push($(this).data('parent'));
		}
	});
	
	if(parentHasInfoArr.length == 0){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterAtLeastOneParent']?>");
		$('#G1ChineseName').focus();
		return false;
	}
	/**** All empty END ****/
	

	/**** Name START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(ChineseName)/);
    }).each(function(){
    	if(
			$.trim(this.value)!='' &&
    		!checkNaNull(this.value) &&
    		!checkIsChineseCharacter(this.value)
    	){
    		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name END ****/
	
	/**** Mobile START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dMobile/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['msg']['entermobilephoneno']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Mobile START ****/
	
	/**** Occupation START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dCompany/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['msg']['enteroccupation']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Occupation START ****/
	
	/**** Position START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dJobPosition/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterJobPosition']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Position START ****/
	
	/**** Relationship START ****/
	if(parentHasInfoArr.indexOf(3) > -1){
    	if($('#G3Relationship').val().trim()==''){
    		alert("<?=$Lang['Admission']['msg']['enterrelationship']?>");	
    		$('#G3Relationship').focus();
    		return false;
    	}
	}
	/**** Relationship END ****/

	return true;
}
</script>