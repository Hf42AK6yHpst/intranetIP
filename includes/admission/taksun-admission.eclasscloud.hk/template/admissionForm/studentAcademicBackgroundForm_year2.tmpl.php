<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}
.col2div{
    display:inline-block;
    width:48%;
}
@media (min-width: 768px) and (max-width: 991.98px) { 
    .col2div{
        width:100%;
    }
}

.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>

<h1>
    <?=$Lang['Admission']['TSS']['AcademicRecord']?>
</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['CurrentSchoolName']?> 
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['CurrentSchoolName']?>
		<?php }else{ ?>
    		<input name="CurrentSchoolName" type="text" id="CurrentSchoolName" class="textboxtext"  value="<?=$StudentPrevSchoolInfo[0]['NameOfSchool']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['CurrentSchoolAddress']?> 
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['CurrentSchoolAddress']?>
		<?php }else{ ?>
    		<input name="CurrentSchoolAddress" type="text" id="CurrentSchoolAddress" class="textboxtext"  value="<?=$StudentPrevSchoolInfo[0]['SchoolAddress']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['ApplyDate']?> 
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['ApplyYear']?><?=$Lang['Admission']['TSS']['Year'] ?>
			<?=$formData['ApplyMonth']?><?=$Lang['Admission']['TSS']['Month'] ?>
		<?php }else{ ?>
			<label for="ApplyYear"><?=$Lang['Admission']['TSS']['Year'] ?></label>
    		<input name="ApplyYear" type="text" id="ApplyYear" size="5" maxlength="4" value="<?=$allCustInfo['ApplyYear'][0]['Value']?>"/>
			<label for="ApplyMonth"><?=$Lang['Admission']['TSS']['Month'] ?></label>
    		<input name="ApplyMonth" type="text" id="ApplyMonth" size="3" maxlength="2" value="<?=$allCustInfo['ApplyMonth'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['LastExamAttend']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['TSS']['overallYear'] ?>: 
			<?=$formData['LastExamAttend_Year']?>
			<br />
			<?=$Lang['Admission']['TSS']['overallClass'] ?>: 
			<?=$formData['LastExamAttend_Class']?>
		<?php }else{ ?>
			<label for="LastExamAttend_Year"><?=$Lang['Admission']['TSS']['overallYear'] ?></label>
    		<input name="LastExamAttend_Year" type="text" id="LastExamAttend_Year" size="4" maxlength="3" value="<?=$allCustInfo['LastExamAttend_Year'][0]['Value']?>"/>
    		
			<label for="LastExamAttend_Class"><?=$Lang['Admission']['TSS']['overallClass'] ?></label>
    		<input name="LastExamAttend_Class" type="text" id="LastExamAttend_Class" size="4" maxlength="3" value="<?=$allCustInfo['LastExamAttend_Class'][0]['Value']?>"/>
		<?php } ?>
	</td>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['TotalAttend']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['TSS']['overallYear'] ?>: 
			<?=$formData['LastExamTotalAttend_Year']?>
			<br />
			<?=$Lang['Admission']['TSS']['overallClass'] ?>: 
			<?=$formData['LastExamTotalAttend_Class']?>
		<?php }else{ ?>
			<label for="LastExamTotalAttend_Year"><?=$Lang['Admission']['TSS']['overallYear'] ?></label>
    		<input name="LastExamTotalAttend_Year" type="text" id="LastExamTotalAttend_Year" size="4" maxlength="3" value="<?=$allCustInfo['LastExamTotalAttend_Year'][0]['Value']?>"/>
    		
			<label for="LastExamTotalAttend_Class"><?=$Lang['Admission']['TSS']['overallClass'] ?></label>
    		<input name="LastExamTotalAttend_Class" type="text" id="LastExamTotalAttend_Class" size="4" maxlength="3" value="<?=$allCustInfo['LastExamTotalAttend_Class'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['AllSubjectScore']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$Lang['Admission']['TSS']['Subjects']['Chi'] ?>:
			<?=$formData['AllSubjectScore_Chi'] ?>
			<br />
			<?=$Lang['Admission']['TSS']['Subjects']['Eng'] ?>:
			<?=$formData['AllSubjectScore_Eng'] ?>
			<br />
			<?=$Lang['Admission']['TSS']['Subjects']['Math'] ?>:
			<?=$formData['AllSubjectScore_Math'] ?>
			<br />
			<?=$Lang['Admission']['TSS']['Subjects']['GeneralStudies'] ?>:
			<?=$formData['AllSubjectScore_Gs'] ?>
		<?php }else{ ?>
			<div>
    			<label for="AllSubjectScore_Chi"><?=$Lang['Admission']['TSS']['Subjects']['Chi'] ?></label>
    			<input name="AllSubjectScore_Chi" type="text" id="AllSubjectScore_Chi" size="5" value="<?=$allCustInfo['AllSubjectScore_Chi'][0]['Value']?>"/>
			</div>
			<div style="margin-top: 5px;">
    			<label for="AllSubjectScore_Eng"><?=$Lang['Admission']['TSS']['Subjects']['Eng'] ?></label>
    			<input name="AllSubjectScore_Eng" type="text" id="AllSubjectScore_Eng" size="5" value="<?=$allCustInfo['AllSubjectScore_Eng'][0]['Value']?>"/>
			</div>
			<div style="margin-top: 5px;">
    			<label for="AllSubjectScore_Math"><?=$Lang['Admission']['TSS']['Subjects']['Math'] ?></label>
    			<input name="AllSubjectScore_Math" type="text" id="AllSubjectScore_Math" size="5" value="<?=$allCustInfo['AllSubjectScore_Math'][0]['Value']?>"/>
			</div>
			<div style="margin-top: 5px;">
    			<label for="AllSubjectScore_Gs"><?=$Lang['Admission']['TSS']['Subjects']['GeneralStudies'] ?></label>
    			<input name="AllSubjectScore_Gs" type="text" id="AllSubjectScore_Gs" size="5" value="<?=$allCustInfo['AllSubjectScore_Gs'][0]['Value']?>"/>
			</div>
		<?php } ?>
	</td>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['Conduct']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['Conduct']?>
		<?php }else{ ?>
			<input name="Conduct" type="text" id="Conduct" class="textboxtext" value="<?=$allCustInfo['Conduct'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$Lang['Admission']['TSS']['Strengths']?>
	</td>
	<td colspan="3">
		<?php if($IsConfirm){ ?>
			<?=$formData['Strengths']?>
		<?php }else{ ?>
			<input name="Strengths" type="text" id="Strengths" class="textboxtext" value="<?=$allCustInfo['Strengths'][0]['Value']?>"/>
		<?php } ?>
	</td>
</tr>
</table>

<script>
function check_academic_background_form(){
	
	/**** CurrentSchoolName START ****/
	if($('#CurrentSchoolName').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolName']?>");	
		$('#CurrentSchoolName').focus();
		return false;
	}
	/**** CurrentSchoolName END ****/
	
	/**** CurrentSchoolAddress START ****/
	if($('#CurrentSchoolAddress').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterCurrentSchoolAddress']?>");	
		$('#CurrentSchoolAddress').focus();
		return false;
	}
	/**** CurrentSchoolAddress END ****/
	
	/**** ApplyDate START ****/
	if($('#ApplyYear').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterApplyDate']?>");	
		$('#ApplyYear').focus();
		return false;
	}
	if($('#ApplyMonth').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterApplyDate']?>");	
		$('#ApplyMonth').focus();
		return false;
	}
	/**** ApplyDate END ****/
	
	/**** LastExamAttend START ****/
	if($('#LastExamAttend_Year').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamAttend']?>");	
		$('#LastExamAttend_Year').focus();
		return false;
	}
	if($('#LastExamAttend_Class').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamAttend']?>");	
		$('#LastExamAttend_Class').focus();
		return false;
	}
	/**** LastExamAttend END ****/
	
	/**** LastExamTotalAttend START ****/
	if($('#LastExamTotalAttend_Year').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamTotalAttend']?>");	
		$('#LastExamTotalAttend_Year').focus();
		return false;
	}
	if($('#LastExamTotalAttend_Class').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterLastExamTotalAttend']?>");	
		$('#LastExamTotalAttend_Class').focus();
		return false;
	}
	/**** LastExamTotalAttend END ****/
	
	/**** AllSubjectScore START ****/
	if($('#AllSubjectScore_Chi').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");	
		$('#AllSubjectScore_Chi').focus();
		return false;
	}
	if($('#AllSubjectScore_Eng').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");	
		$('#AllSubjectScore_Eng').focus();
		return false;
	}
	if($('#AllSubjectScore_Math').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");	
		$('#AllSubjectScore_Math').focus();
		return false;
	}
	if($('#AllSubjectScore_Gs').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterScore']?>");	
		$('#AllSubjectScore_Gs').focus();
		return false;
	}
	/**** AllSubjectScore END ****/
	
	/**** Conduct START ****/
	if($('#Conduct').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterConduct']?>");	
		$('#Conduct').focus();
		return false;
	}
	/**** Conduct END ****/
	
	/**** Strengths START ****/
	if($('#Strengths').val().trim()==''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterStrengths']?>");	
		$('#Strengths').focus();
		return false;
	}
	/**** Strengths END ****/
	
	return true;
}
</script>