<?php

global $libkis_admission;

$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$Lang = $kis_lang;

#### Get Year START ####
$allClassLevel = $libkis_admission->getClassLevel();
$isFirstYear = (filter_var($allClassLevel[$applicationInfo['classLevelID']], FILTER_SANITIZE_NUMBER_INT) == 1);
#### Get Year END ####
?>

<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="70%">
</colgroup>

<?php if($isFirstYear): ?>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['TssExperienceDay']?> 
	</td>
	<td>
		<div>
			<?php $checked = ($allCustInfo['TssExperienceDay'][0]['Value'] != 'Y')? 'checked' : '' ?>
			<input type="radio" id="TssExperienceDay_N" name="TssExperienceDay" value="N" <?=$checked ?> />
			<label for="TssExperienceDay_N"><?=$Lang['Admission']['no'] ?></label>
		</div>
		<div>
			<?php $checked = ($allCustInfo['TssExperienceDay'][0]['Value'] == 'Y')? 'checked' : '' ?>
			<input type="radio" id="TssExperienceDay_Y" name="TssExperienceDay" value="Y" <?=$checked ?> />
			<label for="TssExperienceDay_Y"><?=$Lang['Admission']['yes'] ?></label>
			(
				<label for="TssExperienceDayNo"><?=$Lang['Admission']['TSS']['ExperienceDayNo'] ?>:</label> 
				<input name="TssExperienceDayNo" type="text" id="TssExperienceDayNo" value="<?=$allCustInfo['TssExperienceDayNo'][0]['Value']?>"/>
			)
		</div>
	</td>
</tr>
<?php endif; ?>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['EnterAcceptRemarks']?> 
	</td>
	<td>
		<input name="AcceptRemarks" type="text" id="AcceptRemarks" class="textboxtext" value="<?=$allCustInfo['AcceptRemarks'][0]['Value']?>"/>
	</td>
</tr>

</table>

<script>
function check_other_form(){

	/**** TssExperienceDayNo START ****/
	if($('#TssExperienceDay_Y:checked').length == 1 && $('#TssExperienceDayNo').val().trim() == ''){
		alert("<?=$Lang['Admission']['TSS']['msg']['enterExperienceDayNo']?>");	
		$('#TssExperienceDayNo').focus();
		return false;
	}else if($('[name="TssExperienceDay"]:checked').length == 0){
		$('#TssExperienceDay_N').click();
	}
	/**** TssExperienceDayNo END ****/
	
	return true;
}

$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(check_other_form()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	
	return false;
});
</script>