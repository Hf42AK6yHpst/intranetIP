<?php

global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);
$Lang = $kis_lang;
$star = $mustfillinsymbol;

$parentTypeArr = array(1=>'F',2=>'M',3=>'G');

?>
<style>
.col2div{
    display:inline-block;
    width:48%;
}
.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>

<table class="form_table parentInfo">
	<colgroup>
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
        <col style="width:25%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['F'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['M'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['G'] ?></center></td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star?>
    		<?=$Lang['Admission']['chinesename'] ?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_name = "ChineseName";
		    $formName_name = "G{$index}ChineseName";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName_name ?>" type="text" id="<?=$formName_name ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_name] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['phoneno']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName_mobile1 = "Mobile";
		    $formName_mobile1 = "G{$index}Mobile";
		    $fieldName_mobile2 = "OfficeTelNo";
		    $formName_mobile2 = "G{$index}OfficeTelNo";
		?>
    		<td class="form_guardian_field">
    			<div class="col2div">
            		<label for="<?=$formName_mobile1 ?>">
                		(1)
            		</label>
        			<input name="<?=$formName_mobile1 ?>" type="text" id="<?=$formName_mobile1 ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_mobile1] ?>" data-parent="<?=$index ?>" />
        		</div>
        		&nbsp; 	
    			<div class="col2div">
            		<label for="<?=$formName_mobile2 ?>">
                		(2)
            		</label>
        			<input name="<?=$formName_mobile2 ?>" type="text" id="<?=$formName_mobile2 ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName_mobile2] ?>" data-parent="<?=$index ?>" />
        		</div>		
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['occupation']?>
		</td>
		
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $formName = "G{$index}Company";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType]['Company'] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$Lang['Admission']['TSS']['jobPosition']?>
		</td>
		<?php 
		foreach($parentTypeArr as $index => $parentType){ 
		    $fieldName = "JobPosition";
		    $formName = "G{$index}JobPosition";
		?>
    		<td class="form_guardian_field">
    			<input name="<?=$formName ?>" type="text" id="<?=$formName ?>" class="textboxtext" value="<?=$ParentInfo[$parentType][$fieldName] ?>" data-parent="<?=$index ?>" />
    		</td>
		<?php 
		}
		?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['TSS']['Relationship']?>
		</td>
		
		<td class="form_guardian_field">
			<?=$Lang['Admission']['PG_Type']['F']?>
		</td>
		<td class="form_guardian_field">
			<?=$Lang['Admission']['PG_Type']['M']?>
		</td>
		<td class="form_guardian_field">
			<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" value="<?=$ParentInfo['G']['Relationship'] ?>" data-parent="<?=$index ?>" />
		</td>
	</tr>
	
	
	
</table>
<script>
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();
	
	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}
	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') || 
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	return check_parent_form();
}


function check_parent_form(){
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	
	var isValid = true;
	/******** Basic init END ********/
	
	
	/**** All empty START ****/
	var parentHasInfoArr = [];
	var _text = '';
	$('.parentInfo input').each(function(){
		if($(this).val().trim() != ''){
			parentHasInfoArr.push($(this).data('parent'));
		}
	});
	
	if(parentHasInfoArr.length == 0){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterAtLeastOneParent']?>");
		$('#G1ChineseName').focus();
		return false;
	}
	/**** All empty END ****/
	

	/**** Name START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\d(ChineseName)/);
    }).each(function(){
    	if(
			$.trim(this.value)!='' &&
    		!checkNaNull(this.value) &&
    		!checkIsChineseCharacter(this.value)
    	){
    		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");	
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Name END ****/
	
	/**** Mobile START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dMobile/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['msg']['entermobilephoneno']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Mobile START ****/
	
	/**** Occupation START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dCompany/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['msg']['enteroccupation']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Occupation START ****/
	
	/**** Position START ****/
	$('.parentInfo input').filter(function() {
		var parent = $(this).data('parent');
        return $.inArray(parent, parentHasInfoArr) > -1 && this.name.match(/G\dJobPosition/);
    }).each(function(){
    	if($.trim(this.value)==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterJobPosition']?>");
    		this.focus();
    		return isValid = false;
    	}
    });
	if(!isValid) return false;
	/**** Position START ****/
	
	/**** Relationship START ****/
	if(parentHasInfoArr.indexOf(3) > -1){
    	if($('#G3Relationship').val().trim()==''){
    		alert("<?=$Lang['Admission']['msg']['enterrelationship']?>");	
    		$('#G3Relationship').focus();
    		return false;
    	}
	}
	/**** Relationship END ****/

	return true;
}
</script>