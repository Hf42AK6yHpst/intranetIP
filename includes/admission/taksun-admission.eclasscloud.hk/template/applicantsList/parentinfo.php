<?php

global $libkis_admission;

$ParentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($ParentInfo['applicationID']);
$Lang = $kis_lang;

$parentTypeArr = array(1=>'F',2=>'M',3=>'G');

?>
<style>
.form_guardian_head, .form_guardian_field{
    text-align: center !important;
}
</style>
<table class="form_table">
	<colgroup>
        <col style="width:30%">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:24%">
    </colgroup>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['F'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['M'] ?></center></td>
		<td class="form_guardian_head"><center><?=$Lang['Admission']['PG_Type']['G'] ?></center></td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['chinesename'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['ChineseName'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['phoneno'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Mobile'] ) ?>, 
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['OfficeTelNo'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['occupation'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['Company'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['TSS']['jobPosition'] ?>
		</td>
		<?php foreach($parentTypeArr as $index => $parentType){ ?>
			<td class="form_guardian_field">
    			<?= kis_ui::displayTableField( $ParentInfo[$parentType]['JobPosition'] ) ?>
        	</td>
    	<?php } ?>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$Lang['Admission']['TSS']['Relationship'] ?>
		</td>
		<td class="form_guardian_field">
			<?=$Lang['Admission']['PG_Type']['F']?>
		</td>
		<td class="form_guardian_field">
			<?=$Lang['Admission']['PG_Type']['M']?>
		</td>
		<td class="form_guardian_field">
			<?= kis_ui::displayTableField( $ParentInfo['G']['Relationship'] ) ?>
    	</td>
	</tr>
	
</table>