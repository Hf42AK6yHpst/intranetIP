<?php

global $libkis_admission;

$allCustInfo = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$Lang = $kis_lang;

#### Get Year START ####
$isFirstYear = $libkis_admission->getIsFirstYear($applicationInfo['classLevelID']);
#### Get Year END ####
?>

<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="70%">
</colgroup>

<?php if($isFirstYear): ?>
<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['TssExperienceDay']?> 
	</td>
	<td>
		
		<?php 
		    if($allCustInfo['TssExperienceDay'][0]['Value'] == 'Y'){
		?>
				<?=$Lang['Admission']['yes'] ?>
				<br />
				<?=$Lang['Admission']['TSS']['ExperienceDayNo'] ?>:
				<?=$allCustInfo['TssExperienceDayNo'][0]['Value'] ?>
		<?php   
		    }else{
		        echo $Lang['Admission']['no'];
		    }
	    ?>
	</td>
</tr>
<?php endif; ?>

<tr>
	<td class="field_title">
		<?=$Lang['Admission']['TSS']['EnterAcceptRemarks']?> 
	</td>
	<td>
		<?= kis_ui::displayTableField($allCustInfo['AcceptRemarks'][0]['Value']) ?>
	</td>
</tr>

</table>