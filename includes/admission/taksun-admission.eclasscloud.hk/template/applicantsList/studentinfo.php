<?php

global $libkis_admission;


$StudentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($StudentInfo['applicationID']);
$Lang = $kis_lang;

?>
<table class="form_table">
	<tbody>
		<tr> 
			<td width="30%" class="field_title">
				<?= $Lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
				<?=$StudentInfo['ChineseName']?>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?= $attachmentList['personal_photo']['link'] ? $attachmentList['personal_photo']['link'] : $blankphoto ?>?_=<?= time() ?>"/>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['englishname'] ?>
			</td>
			<td>
				<?=$StudentInfo['EnglishName']?>
			</td>
		</tr>
		<tr>   
			<td class="field_title">
				<?= $Lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['DOB']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['gender'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($Lang['Admission']['genderType'][ $StudentInfo['Gender'] ]) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['PICLC']['birthCertNo'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['BirthCertNo']) ?>
			</td>
		</tr>
		
		
		
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['PlaceOfBirth']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['TSS']['homeAddress'] ?> (<?=$Lang['Admission']['Languages']['Chinese'] ?>)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['AddressChi']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['TSS']['homeAddress'] ?> (<?=$Lang['Admission']['Languages']['English'] ?>)
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Address']) ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$Lang['Admission']['TSS']['religion']?>
			</td>
			<td>
				<?php
				    if(!$StudentInfo['Church']){
				        echo '--';
				    }elseif($StudentInfo['Church'] == $admission_cfg['ReligionType']['Catholic']){
				        echo $Lang['Admission']['TSS']['Catholic'] . '<br />';
				        echo "{$Lang['Admission']['TSS']['CertificateOfBaptism']}: {$allCustInfo['CertificateOfBaptism'][0]['Value']}<br />";
				        echo "{$Lang['Admission']['TSS']['AffiliatedParish']}: {$allCustInfo['AffiliatedParish'][0]['Value']}";
				    }elseif($StudentInfo['Church'] == $admission_cfg['ReligionType']['Other']){
				        echo $StudentInfo['ReligionOther'];
				    }else{
				        echo "( {$Lang['Admission']['TSS']['NoReligion']} )";
				    }
				?>
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['TSS']['Referrer'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($allCustInfo['Referrer'][0]['Value']) ?>
				<?php if($allCustInfo['Referrer'][0]['Value']){ ?>
    				(
        				<?=$Lang['Admission']['TSS']['Relationship'] ?>:
        				<?=$allCustInfo['ReferrerRelationship'][0]['Value'] ?>
    				)
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['contactEmail'] ?>
			</td>
			<td>
				<?= kis_ui::displayTableField($StudentInfo['Email']) ?>
			</td>
		</tr>
	
		<tr>
			<td class="field_title">
				<?= $Lang['Admission']['document'] ?>
			</td>
			<td colspan="2">
				<?php
					$attachmentAry = array();
                	for($i=0;$i<sizeof($attachmentSettings);$i++) {
                		$attachment_name = $attachmentSettings[$i]['AttachmentName'];
                		$_filePath = $attachmentList[$attachment_name]['link'];
						if ($_filePath) {
							//$attachmentAry[] = '<a href="'.$_attachmentAry['link'].'" target="_blank">'.$Lang['Admission'][$_type].'</a>';
							$attachmentAry[] = '<a href="' . $_filePath . '" target="_blank">' . $attachment_name . '</a>';
						}
                	}
				?>
				<?= count($attachmentAry) == 0 ? '--' : implode('<br/>', $attachmentAry); ?>
			</td>
		</tr>                                                                                 
	</tbody>
</table>