<?php

global $libkis_admission;

$StudentInfo = $applicationInfo;
$allCustInfo = $libkis_admission->getAllApplicationCustInfo($StudentInfo['applicationID']);
$Lang = $kis_lang;
$star = $mustfillinsymbol;

?>
<style>
select:disabled{
    color: #ccc;
}
.col2div{
    display:inline-block;
    width:48%;
}
</style>
<table class="form_table">
	<colgroup>
		<col style="width: 30%;"/>
		<col style="width: 40%;"/>
		<col style="width: 30%;"/>
	</colgroup>
	<tbody>


		<!-- ######## Basic Information START ######## -->
		<tr>
			<td width="30%" class="field_title">
				<?= $Lang['Admission']['chinesename'] ?>
			</td>
			<td width="40%">
    			<input name="StudentChineseName" type="text" id="StudentChineseName" class="textboxtext"  value="<?=$StudentInfo['ChineseName']?>"/>
			</td>
			<td width="30%" rowspan="7" width="145">
				<div id="studentphoto" class="student_info" style="margin:0px;">
					<img src="<?=$attachmentList['personal_photo']['link']?$attachmentList['personal_photo']['link']:$blankphoto?>?_=<?=time()?>"/>
					<div class="mail_icon_form" style="position: absolute;top: -5px;right: -40px; <?=$attachmentList['personal_photo']['link']?'':'display:none;'?>">
						<a id = "btn_remove" href="#" class="btn_remove"></a>
					</div>
					<div class="text_remark" style="text-align:center;">
						<?=$Lang['Admission']['msg']['clicktouploadphoto']?>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $Lang['Admission']['englishname'] ?>
			</td>
			<td>
				<input name="StudentEnglishName" type="text" id="StudentEnglishName" class="textboxtext"  value="<?=$StudentInfo['EnglishName']?>"/>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $Lang['Admission']['dateofbirth'] ?>
			</td>
			<td>
			<input name="StudentDateOfBirth" id="StudentDateOfBirth" value="<?=$StudentInfo['dateofbirth']?>">&nbsp;
			<span class="text_remark"><?=$Lang['Admission']['DateFormat']?></span>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<?=$star?><?= $Lang['Admission']['gender'] ?>
			</td>
    		<td>
    			<input type="radio" value="M" id="StudentGender1" name="StudentGender" checked>
    			<label for="StudentGender1"><?=$Lang['Admission']['genderType']['M']?> (<?=$Lang['Admission']['TSS']['GenderRemarks'] ?>)</label>
    		</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?><?=$Lang['Admission']['TSS']['BirthCertType'] ?>
			</td>
			<td nowrap="" colspan="2">
    			<input name="StudentBirthCertNo" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$StudentInfo['birthcertno'] ?>"/>
			</td>
		</tr>
		<!-- ######## Basic Information END ######## -->

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['placeofbirth'] ?>
			</td>
			<td>
    			<input id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" class="textboxtext inputselect" value="<?=$StudentInfo['PlaceOfBirth'] ?>" />
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['TSS']['homeAddress']?> (<?=$Lang['Admission']['Languages']['Chinese'] ?>)
			</td>
			<td>
    			<input id="StudentAddressChi" name="StudentAddressChi" class="textboxtext" value="<?=$StudentInfo['AddressChi'] ?>" />
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['TSS']['homeAddress']?> (<?=$Lang['Admission']['Languages']['English'] ?>)
			</td>
			<td>
    			<input id="StudentAddress" name="StudentAddress" class="textboxtext" value="<?=$StudentInfo['Address'] ?>" />
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['TSS']['religion']?>
			</td>
			<td>
        		<div>
        			<?php $checked = ($StudentInfo['Church'] == $admission_cfg['ReligionType']['Catholic'])? 'checked' : '' ?>
        			<input type="radio" id="Religion_Catholic" name="Religion" value="<?=$admission_cfg['ReligionType']['Catholic'] ?>" <?=$checked ?> />
        			<label for="Religion_Catholic"><?=$Lang['Admission']['TSS']['Catholic'] ?></label>

        			<div id="ReligionCatholicDetails" style="margin: 5px 25px; display:none;">
            			<label for="CertificateOfBaptism"><?=$Lang['Admission']['TSS']['CertificateOfBaptism'] ?></label>
            			<input id="CertificateOfBaptism" name="CertificateOfBaptism" value="<?=$allCustInfo['CertificateOfBaptism'][0]['Value'] ?>" style="margin-bottom: 5px;" />
            			(<?=$Lang['Admission']['TSS']['AttachCertificateOfBaptism'] ?>)
            			<br />
            			<label for="AffiliatedParish"><?=$Lang['Admission']['TSS']['AffiliatedParish'] ?></label>
            			<input id="AffiliatedParish" name="AffiliatedParish" value="<?=$allCustInfo['AffiliatedParish'][0]['Value'] ?>" />
        			</div>
    			</div>
        		<div>
        			<?php $checked = ($StudentInfo['Church'] == $admission_cfg['ReligionType']['Other'])? 'checked' : '' ?>
        			<input type="radio" id="Religion_Other" name="Religion" value="<?=$admission_cfg['ReligionType']['Other'] ?>" <?=$checked ?> />
        			<label for="Religion_Other"><?=$Lang['Admission']['TSS']['Others'] ?></label>

        			(
            			<label for="ReligionOther"><?=$Lang['Admission']['TSS']['PleaseSpecify'] ?>: </label>
            			<input id="ReligionOther" name="ReligionOther" value="<?=$StudentInfo['ReligionOther'] ?>" />
        			)
    			</div>
        		<div>
        			<?php $checked = ($StudentInfo['Church'] == $admission_cfg['ReligionType']['None'])? 'checked' : '' ?>
        			<input type="radio" id="Religion_No" name="Religion" value="<?=$admission_cfg['ReligionType']['None'] ?>" <?=$checked ?> />
        			<label for="Religion_No"><?=$Lang['Admission']['TSS']['NoReligion'] ?></label>
    			</div>
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['TSS']['Referrer'] ?>
			</td>
			<td>
    			<input id="Referrer" name="Referrer" class="textboxtext" value="<?=$allCustInfo['Referrer'][0]['Value'] ?>" />
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['TSS']['Relationship'] ?>
			</td>
			<td>
    			<input id="ReferrerRelationship" name="ReferrerRelationship" class="textboxtext" value="<?=$allCustInfo['ReferrerRelationship'][0]['Value'] ?>" />
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$star?>
				<?=$Lang['Admission']['contactEmail'] ?>
			</td>
			<td>
    			<input id="StudentEmail" name="StudentEmail" class="textboxtext" value="<?=$StudentInfo['Email'] ?>" />
			</td>
		</tr>


		<!-- ######## Document START ######## -->
    	<?php
    	for($i=0;$i<sizeof($attachmentSettings);$i++) {

    	    #### Check ClassLevel START ####
    	    if($attachmentSettings[$i]['ClassLevelStr']){
    	        $classLevelArr = explode(',', $attachmentSettings[$i]['ClassLevelStr']);

    	        if(!in_array($StudentInfo['classLevelID'], $classLevelArr)){
    	            continue;
    	        }
    	    }
    	    #### Check ClassLevel END ####


    		$attachment_name = $attachmentSettings[$i]['AttachmentName'];

    		$_filePath = $attachmentList[$attachment_name]['link'];
    		if($_filePath){
    			$_spanDisplay = '';
    			$_buttonDisplay = ' style="display:none;"';
    		}else{
    			$_filePath = '';
    			$_spanDisplay = ' style="display:none;"';
    			$_buttonDisplay = '';
    		}

    		$_attachment = "<a href ='".$_filePath."' class='file_attachment' target='_blank'>". $Lang['view']."</a>";
    		$_attachment .= '<div class="table_row_tool"><a href="#" class="delete_dim" title="'.$Lang['delete'].'"></a></div>';
    	?>
    		<tr>
    			<td class="field_title">
    				<?=$attachment_name?>
    			</td>
    			<td id="<?=$attachment_name?>">
    				<span class="view_attachment" <?=$_spanDisplay?>><?=$_attachment?></span>
    				<input type="button" class="attachment_upload_btn formsmallbutton" value="<?=$Lang['Upload']?>"  id="uploader-<?=$attachment_name?>" <?=$_buttonDisplay?>/>
    			</td>
    		</tr>
    	<?php
    	}
    	?>
		<!-- ######## Document END ######## -->
	</tbody>
</table>

<script>
//// UI Releated START ////
$(function(){
	'use strict';

	$('[name="Religion"]').change(function(){
		if($(this).prop('checked') && $(this).val() == '<?=$admission_cfg['ReligionType']['Catholic'] ?>'){
			$('#ReligionCatholicDetails').slideDown();
		}else if($(this).prop('checked')){
			$('#ReligionCatholicDetails').slideUp();
		}
	}).change();

	$('#StudentEnglishName, #StudentAddress, #StudentBirthCertNo').on('keyup blur', function(){
		$(this).val($(this).val().toUpperCase());
	});

	var lastStudentBirthCertNo='';
	$('#StudentBirthCertNo').on('keyup blur', function(e){
    	var val = $(this).val().toUpperCase();
		if(val != '' && !/^[A-Z]\d*(?:\d[A-Z])?$/.test(val)){
    		$(this).val(lastStudentBirthCertNo);
    		return false;
		}
		$(this).val(val);
		lastStudentBirthCertNo = val;
	});
});
////UI Releated END ////

var dOBRange;
$('#applicant_form').unbind('submit').submit(function(e){
	e.preventDefault();

	var schoolYearId = $('#schoolYearId').val();
	var recordID = $('#recordID').val();
	var display = $('#display').val();
	var timeSlot = lang.timeslot.split(',');
	if(checkValidForm()){
		$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
		});
	}

	return false;
});

function checkIsChineseCharacter(str){
	return str.match(/^[\u3400-\u9FBF]*$/);
}

function checkIsEnglishCharacter(str){
	return str.match(/^[A-Za-z ,\-]*$/);
}

function checkNaNull(str){
	return (
    	($.trim(str).toLowerCase()=='沒有') ||
    	($.trim(str).toLowerCase()=='nil') ||
    	($.trim(str).toLowerCase()=='n.a.')
	);
}

function checkValidForm(){
	return check_student_form();
}


function check_student_form(){
	/******** Basic init START ********/
	//for email validation
	var re = /\S+@\S+\.\S+/;
	dOBRange = dOBRange || ['',''];
	/******** Basic init END ********/


	/**** Name START ****/
	if(
		!checkNaNull($('#StudentChineseName').val()) &&
		!checkIsChineseCharacter($('#StudentChineseName').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterchinesecharacter']?>");
		$('#StudentChineseName').focus();
		return false;
	}

	if($('#StudentEnglishName').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>");
		$('#StudentEnglishName').focus();
		return false;
	}
	if(
		!checkNaNull($('#StudentEnglishName').val()) &&
		!checkIsEnglishCharacter($('#StudentEnglishName').val())
	){
		alert("<?=$Lang['Admission']['msg']['enterenglishcharacter']?>");
		$('#StudentEnglishName').focus();
		return false;
	}
	/**** Name END ****/

	/**** DOB START ****/
	if( !$('#StudentDateOfBirth').val().match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/) ){
		if($('#StudentDateOfBirth').val()!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>");
		}else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>");
		}

		$('#StudentDateOfBirth').focus();
		return false;
	}
	if(
		(
			dOBRange[0] !='' &&
			$('#StudentDateOfBirth').val() < dOBRange[0]
		) || (
			dOBRange[1] !='' &&
			$('#StudentDateOfBirth').val() > dOBRange[1]
		)
	){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>");
		$('#StudentDateOfBirth').focus();
		return false;
	}
	/**** DOB END ****/

	/**** Gender START ****/
	if($('input[name=StudentGender]:checked').length == 0){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>");
		$('input[name=StudentGender]:first').focus();
		return false;
	}
	/**** Gender END ****/

	/**** Personal Identification START ****/
	if($('#StudentBirthCertNo').val().trim()==''){
		alert("<?=$Lang['Admission']['PICLC']['msg']['enterPersonalIdentificationNo']?>");
		$('#StudentBirthCertNo').focus();
		return false;
	}
// 	if(!check_hkid(form1.StudentBirthCertNo.value)){
//    	alert("<?=$Lang['Admission']['SHCK']['msg']['invalidBirthCertNo']?>");
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }

//     if(!isUpdatePeriod && checkBirthCertNo() > 0){
//    	alert("<?=$Lang['Admission']['PICLC']['msg']['duplicateBirthCertNo']?>");
//     	form1.StudentBirthCertNo.focus();
//     	return false;
//     }
	/**** Personal Identification END ****/

	/**** PlaceOfBirth START ****/
	if($('#StudentPlaceOfBirth').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>");
		$('#StudentPlaceOfBirth').focus();
		return false;
	}
	/**** PlaceOfBirth END ****/

	/**** Address START ****/
	if($('#StudentAddress').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterhomeaddress']?>");
		$('#StudentAddress').focus();
		return false;
	}
	/**** Address END ****/

	/**** Religion START ****/
	if($('[name="Religion"]:checked').length == 0){
		alert("<?=$Lang['Admission']['CHIUCHUNKG']['msg']['SelectReligion']?>");
		$('#Religion_Catholic').focus();
		return false;
	}

	if($('#Religion_Catholic:checked').length == 1){
    	if($('#CertificateOfBaptism').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterCertificateOfBaptism']?>");
    		$('#CertificateOfBaptism').focus();
    		return false;
    	}
    	if($('#AffiliatedParish').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterAffiliatedParish']?>");
    		$('#AffiliatedParish').focus();
    		return false;
    	}
	}
	if($('#Religion_Other:checked').length == 1){
    	if($('#ReligionOther').val().trim()==''){
    		alert("<?=$Lang['Admission']['TSS']['msg']['enterExtraInfo']?>");
    		$('#ReligionOther').focus();
    		return false;
    	}
	}
	/**** Religion END ****/

	/**** Referrer START ****/
	if($('#Referrer').val().trim()!='' && $('#ReferrerRelationship').val().trim()==''){
		alert("<?=$Lang['Admission']['msg']['enterrelationship']?>");
		$('#ReferrerRelationship').focus();
		return false;
	}
	/**** Referrer END ****/

	/**** Email START ****/
	if($('#StudentEmail').val().trim()==''){
		alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	if($('#StudentEmail').val().trim()!='' && !re.test($('#StudentEmail').val().trim())){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>");
		$('#StudentEmail').focus();
		return false;
	}
	/**** Email END ****/

	return true;
}
</script>