<?php
# modifying by:
/********************
 *
 * Log :
 * Date		2018-01-26 [Pun]
 * 			File Created
 *
 ********************/

error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);
include_once("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/ActionFilterQueueTrait.class.php");
include_once("{$intranet_root}/includes/admission/HelperClass/AdmissionSystem/AdmissionCustBase.class.php");

class admission_cust extends \AdmissionSystem\AdmissionCustBase{

    /**** CONST START ****/
    /**** CONST END ****/

    public function __construct(){
        global $plugin;

        if($plugin['eAdmission_devMode']){
            error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);
            if($_SERVER['HTTP_HOST'] == '192.168.0.171:31002'){
                $this->AdmissionFormSendEmail = false;
            }
        }

        parent::__construct();
        $this->init();
    }

    private function init(){
        /**** FILTER_ADMISSION_FORM_EMAIL_TITLE START ****/
        $this->addFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, (function($emailTitle){
            return 'Tak Sun School Admission Notification';
        }));
        /**** FILTER_ADMISSION_FORM_EMAIL_TITLE END ****/

        /**** FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER START ****/
        $this->addFilter(self::FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER, array($this, 'getNextApplicationNum'));
        /**** FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER END ****/

        /**** ACTION_APPLICANT_INSERT_DUMMY_INFO START ****/
        $this->addAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, array($this, 'insertDummySchoolInfo'));
        /**** ACTION_APPLICANT_INSERT_DUMMY_INFO END ****/

        /**** ACTION_APPLICANT_UPDATE_INFO START ****/
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateStudentInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateStudentAcademicBackgroundInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateParentInfo'));
        $this->addAction(self::ACTION_APPLICANT_UPDATE_INFO, array($this, 'updateOtherInfo'));
        /**** ACTION_APPLICANT_UPDATE_INFO END ****/

        /**** ACTION_APPLICANT_UPDATE_*_INFO START ****/
        $this->addAction(self::ACTION_APPLICANT_INFO, array($this, 'updateApplicantInfo'));
        /**** ACTION_APPLICANT_UPDATE_*_INFO END ****/
    }

    /**
     * Admission Form - before create applicant
     */
    protected function insertDummySchoolInfo($ApplicationID){
        $result = true;

        $sql = "INSERT INTO
            ADMISSION_STU_PREV_SCHOOL_INFO
        (
            ApplicationID,
            SchoolOrder,
            DateInput,
            InputBy
        ) VALUES (
            '{$ApplicationID}',
            '0',
            NOW(),
            '{$this->uid}'
        )";

        $result = $result && $this->db_db_query($sql);

        if(!$result){
            throw new \Exception('Cannot insert dummy stuent school info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentInfo($Data, $ApplicationID){
        global $admission_cfg;

        /**** Update basic info START ****/
        if($Data['Religion'] == $admission_cfg['ReligionType']['Catholic']){
            $Data['ReligionOther'] = '';
        }elseif($Data['Religion'] == $admission_cfg['ReligionType']['Other']){
            $Data['CertificateOfBaptism'] = '';
            $Data['AffiliatedParish'] = '';
        }else{
            $Data['ReligionOther'] = '';
            $Data['CertificateOfBaptism'] = '';
            $Data['AffiliatedParish'] = '';
        }

        $fieldArr = array(
            'ChineseName' => 'StudentChineseName',
            'EnglishName' => 'StudentEnglishName',
            'DOB' => 'StudentDateOfBirth',
            'Gender' => 'StudentGender',
            'BirthCertNo' => 'StudentBirthCertNo',
            'PlaceOfBirth' => 'StudentPlaceOfBirth',
            'Address' => 'StudentAddress',
            'AddressChi' => 'StudentAddressChi',
            'Church' => 'Religion',
            'ReligionOther' => 'ReligionOther',
            'Email' => 'StudentEmail',
        );

        $updateSql = '';
        foreach($fieldArr as $dbField => $dataField){
            if(isset($Data[$dataField])){
                $updateSql .= "$dbField = '{$Data[$dataField]}',";
            }
        }

        $sql = "UPDATE
            ADMISSION_STU_INFO
        SET
            {$updateSql}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'";
        $result = $this->db_db_query($sql);
        /**** Update basic info END ****/


        /**** Update Cust info START ****/
        if(isset($Data['CertificateOfBaptism'])){
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'CertificateOfBaptism',
                'Value' => $Data['CertificateOfBaptism'],
            ), $insertIfNotExists = true);
        }
        if(isset($Data['AffiliatedParish'])){
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'AffiliatedParish',
                'Value' => $Data['AffiliatedParish'],
            ), $insertIfNotExists = true);
        }
        if(isset($Data['Referrer'])){
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'Referrer',
                'Value' => $Data['Referrer'],
            ), $insertIfNotExists = true);
        }
        if(isset($Data['ReferrerRelationship'])){
            $result = $result && $this->updateApplicationCustInfo(array(
                'filterApplicationId' => $ApplicationID,
                'filterCode' => 'ReferrerRelationship',
                'Value' => $Data['ReferrerRelationship'],
            ), $insertIfNotExists = true);
        }
        /**** Update Cust info END ****/

        if(!$result){
            throw new \Exception('Cannot update student info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateParentInfo($Data, $ApplicationID){
        $result = true;

        $pgType = array( 1=>'F', 2=>'M', 3=>'G' );

        /**** Update parent basic info START ****/
        $fieldArr = array(
            'ChineseName' => 'ChineseName',
            'EnglishName' => 'ChineseName',
            'Mobile' => 'Mobile',
            'OfficeTelNo' => 'OfficeTelNo',
            'Company' => 'Company',
            'JobPosition' => 'JobPosition',
        );

        foreach($pgType as $id => $type){
            $updateSQL = '';
            foreach($fieldArr as $dbField => $dataField){
                if(isset($Data["G{$id}{$dataField}"])){
                    $updateSQL .= "{$dbField} = '{$Data["G{$id}{$dataField}"]}',";
                }
            }

            if($type == 'G'){
                $updateSQL .= "Relationship = '{$Data['G3Relationship']}',";
            }

            $sql = "UPDATE
                ADMISSION_PG_INFO
            SET
                {$updateSQL}
                DateModified = NOW(),
                ModifiedBy = '{$this->uid}'
            WHERE
                ApplicationID = '{$ApplicationID}'
            AND
                PG_TYPE = '{$type}'";
            $result = $result && $this->db_db_query($sql);
        }
        /**** Update parent basic info END ****/

        /**** Update Cust info START ****/
        /**** Update Cust info END ****/

        if(!$result){
            throw new \Exception('Cannot update parent info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateStudentAcademicBackgroundInfo($Data, $ApplicationID){
        global $admission_cfg;

        $result = true;

        /**** Get Apply Year START ****/
        $application_details = $this->getApplicationOthersInfo($this->schoolYearID, '', $ApplicationID);
        $allClassLevel = $this->getClassLevel();
        $isFirstYear = (filter_var($allClassLevel[$application_details[0]['ApplyLevel']], FILTER_SANITIZE_NUMBER_INT) == 1);
        /**** Get Apply Year END ****/

        /**** Update school basic info START ****/
        $fieldArr = array(
            'NameOfSchool' => 'CurrentSchoolName',
            'SchoolAddress' => 'CurrentSchoolAddress',
        );

        $updateSQL = '';
        foreach($fieldArr as $dbField => $inputField){
            if(isset($Data[$inputField])){
                $updateSQL .= "{$dbField} = '{$Data[$inputField]}',";
            }
        }

        $sql = "UPDATE
            ADMISSION_STU_PREV_SCHOOL_INFO
        SET
            {$updateSQL}
            DateModified = NOW(),
            ModifiedBy = '{$this->uid}'
        WHERE
            ApplicationID = '{$ApplicationID}'
        AND
            SchoolOrder = '0'";
        $result = $result && $this->db_db_query($sql);
        /**** Update school basic info END ****/

        /**** Update cust info START ****/
        if($isFirstYear){
            if($Data['SchoolNet'] == $admission_cfg['SchoolNet']['Default']){
                $Data['SchoolNetOtherDetails_District'] = '';
                $Data['SchoolNetOtherDetails_Net'] = '';
            }

            $fieldArr = array(
                'EdbApplicationNo',
                'OriginalSecondarySchoolName',
                'SchoolNet',
                'SchoolNetOtherDetails_District',
                'SchoolNetOtherDetails_Net',
                'ApplyOtherSchoolAtSameTime1',
                'ApplyOtherSchoolAtSameTime2',
                'SelfApplySchool',
                //'ApplySchoolPartB',
                'WishPartA',
                'WishPartB',
            );
        }else{
            $fieldArr = array(
                'ApplyYear',
                'ApplyMonth',
                'LastExamAttend_Year',
                'LastExamAttend_Class',
                'LastExamTotalAttend_Year',
                'LastExamTotalAttend_Class',
                'AllSubjectScore_Chi',
                'AllSubjectScore_Eng',
                'AllSubjectScore_Math',
                'AllSubjectScore_Gs',
                'Conduct',
                'Strengths',
            );
        }
        foreach($fieldArr as $field){
            if(isset($Data[$field])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field],
                ), $insertIfNotExists = true);
            }
        }
        /**** Update cust info START ****/

        if(!$result){
            throw new \Exception('Cannot update student academic background info');
        }
        return true;
    }

    /**
     * Admission Form - create/update applicant
     */
    protected function updateOtherInfo($Data, $ApplicationID){
        $result = true;

        if($Data['TssExperienceDay'] == 'N'){
            $Data['TssExperienceDayNo'] = '';
        }

        $fieldArr = array(
            'TssExperienceDay',
            'TssExperienceDayNo',
            'AcceptRemarks',
        );
        foreach($fieldArr as $field){
            if(isset($Data[$field])){
                $result = $result && $this->updateApplicationCustInfo(array(
                    'filterApplicationId' => $ApplicationID,
                    'filterCode' => $field,
                    'Value' => $Data[$field],
                ), $insertIfNotExists = true);
            }
        }

        if(!$result){
            throw new \Exception('Cannot update other info');
        }
        return true;
    }


    /**
     * Portal - update applicant
     */
    protected function updateApplicantInfo($type, $Data, $ApplicationID){
        $result = true;

        if($type == 'studentinfo'){
            $result = $result && $this->updateStudentInfo($Data, $ApplicationID);

        }else if($type == 'parentinfo'){
            $result = $result && $this->updateParentInfo($Data, $ApplicationID);

        }else if($type == 'tss_academic_background'){
            $result = $result && $this->updateStudentAcademicBackgroundInfo($Data, $ApplicationID);

        }else if($type == 'otherinfo'){
            $result = $result && $this->updateOtherInfo($Data, $ApplicationID);

        }else if($type == 'remarks'){
            $result = $result && $this->updateApplicationStatus($Data, $ApplicationID);

        }else{
            $result = false;
        }

        if(!$result){
            throw new \Exception('Cannot update applicant info');
        }
        return $result;
    }

    /**
     * Portal - export applicant details header
     */
    public function getExportHeader($schoolYearID = '',$classLevelID = ''){
        global $kis_lang;

        $isFirstYear = $this->getIsFirstYear($classLevelID);

        $headerArray = array();

        $headerArray[] = $kis_lang['Admission']['admissiondate'];
        $headerArray[] = $kis_lang['applicationno'];
        $headerArray[] = $kis_lang['Admission']['applyLevel'];

        //for student info
        $headerArray['studentInfo'][] = $kis_lang['Admission']['englishname'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['chinesename'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['dateofbirth'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['gender'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['TSS']['BirthCertType'];

        $headerArray['studentInfo'][] = $kis_lang['Admission']['placeofbirth'];
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['TSS']['homeAddress']} ({$kis_lang['Admission']['Languages']['Chinese']})";
        $headerArray['studentInfo'][] = "{$kis_lang['Admission']['TSS']['homeAddress']} ({$kis_lang['Admission']['Languages']['English']})";
        $headerArray['studentInfo'][] = $kis_lang['Admission']['TSS']['religion'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['TSS']['CertificateOfBaptism'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['TSS']['AffiliatedParish'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['TSS']['Referrer'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['TSS']['Relationship'];
        $headerArray['studentInfo'][] = $kis_lang['Admission']['contactEmail'];


        //for parent info
        $headerArray['parentInfo'][] = $kis_lang['Admission']['chinesename'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['phoneno'].' (1)';
        $headerArray['parentInfo'][] = $kis_lang['Admission']['phoneno'].' (2)';
        $headerArray['parentInfo'][] = $kis_lang['Admission']['occupation'];
        $headerArray['parentInfo'][] = $kis_lang['Admission']['TSS']['jobPosition'];
        $headerArray['parentInfo2'][] = $kis_lang['Admission']['TSS']['Relationship'];


        //for academic background
        $headerArray['academicBackgroundInfo'] = array();
        $headerArray['academicBackgroundInfo2'] = array();
        if($isFirstYear){
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['EdbApplicationNo'];
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['CurrentSchoolName'];
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['district'];
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['SchoolNetForAddress'];
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['ApplyOtherSchoolAtSameTime'].' (1)';
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['ApplyOtherSchoolAtSameTime'].' (2)';
            $headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['SelfApplySchool'];
            //$headerArray['academicBackgroundInfo'][] = $kis_lang['Admission']['TSS']['ApplySchoolPartB'];
            $headerArray['academicBackgroundInfo'][] = "{$kis_lang['Admission']['TSS']['Wish']} ({$kis_lang['Admission']['TSS']['PartA']})";
            $headerArray['academicBackgroundInfo'][] = "{$kis_lang['Admission']['TSS']['Wish']} ({$kis_lang['Admission']['TSS']['PartB']})";
        }else{
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['CurrentSchoolName'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['CurrentSchoolAddress'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['ApplyDate'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['overallYear'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['overallClass'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['Subjects']['Chi'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['Subjects']['Eng'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['Subjects']['Math'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['Subjects']['GeneralStudies'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['Conduct'];
            $headerArray['academicBackgroundInfo2'][] = $kis_lang['Admission']['TSS']['Strengths'];
        }


        //for other info
        if($isFirstYear){
            $headerArray['otherInfo'][] = $kis_lang['Admission']['TSS']['TssExperienceDay'];
            $headerArray['otherInfo'][] = $kis_lang['Admission']['TSS']['ExperienceDayNo'];
        }
        $headerArray['otherInfo'][] = $kis_lang['Admission']['TSS']['EnterAcceptRemarks'];


        //for official use
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['PaymentStatus']['PaymentMethod'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (1)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (2)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate']." (3)";
        $headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
        $headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];


        ######## Header START ########
        #### Admission Info START ####
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        $exportColumn[0][] = "";
        #### Admission Info END ####


        #### Student Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
        for($i=0; $i < count($headerArray['studentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }
        #### Student Info END ####


        #### Parent Info START ####
        $exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['F']})";
        for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }

        $exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['M']})";
        for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }

        $exportColumn[0][] =" {$kis_lang['Admission']['PGInfo']} ({$kis_lang['Admission']['PG_Type']['G']})";
        for($i=0; $i < count($headerArray['parentInfo'])-1; $i++){
            $exportColumn[0][] = "";
        }

        for($i=0; $i < count($headerArray['parentInfo2']); $i++){
            $exportColumn[0][] = "";
        }
        #### Parent Info END ####


        #### Academic Background Info START ####
        if($isFirstYear){
            $exportColumn[0][] = "{$kis_lang['Admission']['TSS']['AcademicRecord']} ({$kis_lang['Admission']['TSS']['AlternatePlaces']})";
            for($i=0; $i < count($headerArray['academicBackgroundInfo'])-1; $i++){
                $exportColumn[0][] = "";
            }
        }else{
            $exportColumn[0][] = "{$kis_lang['Admission']['TSS']['AcademicRecord']} ({$kis_lang['Admission']['TSS']['TransferStudents']})";
            $exportColumn[0][] = "";
            $exportColumn[0][] = "";
            $exportColumn[0][] = $kis_lang['Admission']['TSS']['LastExamAttend'];
            $exportColumn[0][] = "";
            $exportColumn[0][] = $kis_lang['Admission']['TSS']['AllSubjectScore'];
            $exportColumn[0][] = "";
            $exportColumn[0][] = "";
            $exportColumn[0][] = "";
            $exportColumn[0][] = "";
            $exportColumn[0][] = "";
        }
        #### Academic Background Info END ####


        #### Other Info START ####
        $exportColumn[0][] = $kis_lang['Admission']['otherInfo'];
         for($i=0; $i < count($headerArray['otherInfo'])-1; $i++){
         $exportColumn[0][] = "";
        }
        #### Other Info END ####

        #### Offical Use START ####
        $exportColumn[0][] = $kis_lang['remarks'];
        for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
            $exportColumn[0][] = "";
        }
        #### Offical Use END ####

        //sub header
        $exportColumn[1] = array_merge(
        array($headerArray[0],$headerArray[1],$headerArray[2]),
            $headerArray['studentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo'],
            $headerArray['parentInfo2'],
            $headerArray['academicBackgroundInfo'],
            $headerArray['academicBackgroundInfo2'],
            $headerArray['otherInfo'],
            $headerArray['officialUse']
        );

        return $exportColumn;
    }

    /**
     * Portal - export applicant details
     */
    public function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
        global $admission_cfg, $Lang, $kis_lang;

        $isFirstYear = $this->getIsFirstYear($classLevelID);

        ######### Get data START ########
        #### Get student Info START ####
        $StudentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
        #### Get student Info END ####

        #### Get parent Info START ####
        $parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
        $parentInfoArr = array();
        foreach($parentInfo as $parent){
            $ParentInfo[$parent['type']] = $parent;
        }
        #### Get parent Info END ####

        #### Get academic background START ####
        $StudentPrevSchoolInfo = $this->getApplicationPrevSchoolInfo($schoolYearID,$classLevelID,$StudentInfo['applicationID']);
        #### Get academic background END ####

        #### Get other info START ####
        $allCustInfo = $this->getAllApplicationCustInfo($StudentInfo['applicationID']);

        $allClassLevel = $this->getClassLevel();
        $classLevel = $this->getClassLevel();
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
        $status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
        #### Get other info END ####
        ######### Get data END ########


        $dataArray = array();

        ######### Export data START ########
        #### Basic Info START ####
        $dataArray[] = substr($otherInfo['DateInput'], 0, -9);
        $dataArray[] = $StudentInfo['applicationID'];
        $dataArray[] = $classLevel[$otherInfo['classLevelID']];
        #### Basic Info END ####


        #### Student Info START ####
        $dataArray['studentInfo'][] = $StudentInfo['EnglishName'];
        $dataArray['studentInfo'][] = $StudentInfo['ChineseName'];
        $dataArray['studentInfo'][] = ($StudentInfo['DOB'] == '0000-00-00')? '' : $StudentInfo['DOB'];
        $dataArray['studentInfo'][] = $Lang['Admission']['genderType'][$StudentInfo['Gender']];
        $dataArray['studentInfo'][] = $StudentInfo['BirthCertNo'];
        $dataArray['studentInfo'][] = $StudentInfo['PlaceOfBirth'];
        $dataArray['studentInfo'][] = $StudentInfo['AddressChi'];
        $dataArray['studentInfo'][] = $StudentInfo['Address'];
        if($StudentInfo['Church'] == $admission_cfg['ReligionType']['Catholic']){
            $dataArray['studentInfo'][] = $Lang['Admission']['TSS']['Catholic'];
            $dataArray['studentInfo'][] = $allCustInfo['CertificateOfBaptism'][0]['Value'];
            $dataArray['studentInfo'][] = $allCustInfo['AffiliatedParish'][0]['Value'];
        }elseif($StudentInfo['Church'] == $admission_cfg['ReligionType']['Other']){
            $dataArray['studentInfo'][] = $StudentInfo['ReligionOther'];
            $dataArray['studentInfo'][] = '';
            $dataArray['studentInfo'][] = '';
        }else{
            $dataArray['studentInfo'][] = $Lang['Admission']['TSS']['NoReligion'];
            $dataArray['studentInfo'][] = '';
            $dataArray['studentInfo'][] = '';
        }
        $dataArray['studentInfo'][] = $allCustInfo['Referrer'][0]['Value'];
        $dataArray['studentInfo'][] = $allCustInfo['ReferrerRelationship'][0]['Value'];
        $dataArray['studentInfo'][] = $StudentInfo['Email'];
        #### Student Info END ####


        #### Parent Info START ####
        foreach(array('F', 'M', 'G') as $type){
            $dataArray['parentInfo'][] = $ParentInfo[$type]['ChineseName'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Mobile'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['OfficeTelNo'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['Company'];
            $dataArray['parentInfo'][] = $ParentInfo[$type]['JobPosition'];
        }
        $dataArray['parentInfo'][] = $ParentInfo[$type]['Relationship'];
        #### Parent Info END ####


        #### Academic Background Info START ####
        if($isFirstYear){
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['EdbApplicationNo'][0]['Value'];
            $dataArray['academicBackgroundInfo'][] = ($StudentPrevSchoolInfo[0]['NameOfSchool'])?$StudentPrevSchoolInfo[0]['NameOfSchool']:$Lang['Admission']['TSS']['TakSunKindergarten'];
            $dataArray['academicBackgroundInfo'][] = $Lang['Admission']['TSS']['Districts'][ $StudentPrevSchoolInfo[0]['SchoolAddress'] ];
    	    if($allCustInfo['SchoolNet'][0]['Value'] == $admission_cfg['SchoolNet']['Default']){
    	        $dataArray['academicBackgroundInfo'][] = $Lang['Admission']['TSS']['SchoolNetForAddressDefault'];
    	    }else{
    	        $dataArray['academicBackgroundInfo'][] = "{$Lang['Admission']['TSS']['Districts'][ $allCustInfo['SchoolNetOtherDetails_District'][0]['Value'] ]} {$allCustInfo['SchoolNetOtherDetails_Net'][0]['Value']}{$Lang['Admission']['TSS']['Net']}";
    	    }
    	    $dataArray['academicBackgroundInfo'][] = $allCustInfo['ApplyOtherSchoolAtSameTime1'][0]['Value'];
    	    $dataArray['academicBackgroundInfo'][] = $allCustInfo['ApplyOtherSchoolAtSameTime2'][0]['Value'];
    	    $dataArray['academicBackgroundInfo'][] = ($allCustInfo['SelfApplySchool'][0]['Value'])? $allCustInfo['SelfApplySchool'][0]['Value'] : $Lang['Admission']['TSS']['SelfApplySchool_taksun'];
    	    //$dataArray['academicBackgroundInfo'][] = $allCustInfo['ApplySchoolPartB'][0]['Value'];
    	    $dataArray['academicBackgroundInfo'][] = $Lang['Admission']['TSS']['WishOption'][ $allCustInfo['WishPartA'][0]['Value'] ];
    	    $dataArray['academicBackgroundInfo'][] = $Lang['Admission']['TSS']['WishOption'][ $allCustInfo['WishPartB'][0]['Value'] ];
        }else{
            $dataArray['academicBackgroundInfo'][] = $StudentPrevSchoolInfo[0]['NameOfSchool'];
            $dataArray['academicBackgroundInfo'][] = $StudentPrevSchoolInfo[0]['SchoolAddress'];
            $dataArray['academicBackgroundInfo'][] = "{$allCustInfo['ApplyYear'][0]['Value']}{$Lang['Admission']['TSS']['Year']} {$allCustInfo['ApplyMonth'][0]['Value']}{$Lang['Admission']['TSS']['Month']}";
            $dataArray['academicBackgroundInfo'][] = "{$allCustInfo['LastExamAttend_Year'][0]['Value']} / {$allCustInfo['LastExamTotalAttend_Year'][0]['Value']}";
            $dataArray['academicBackgroundInfo'][] = "{$allCustInfo['LastExamAttend_Class'][0]['Value']} / {$allCustInfo['LastExamTotalAttend_Class'][0]['Value']}";
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['AllSubjectScore_Chi'][0]['Value'];
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['AllSubjectScore_Eng'][0]['Value'];
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['AllSubjectScore_Math'][0]['Value'];
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['AllSubjectScore_Gs'][0]['Value'];
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['Conduct'][0]['Value'];
            $dataArray['academicBackgroundInfo'][] = $allCustInfo['Strengths'][0]['Value'];
        }
        #### Academic Background Info END ####

        #### Other Info START ####
        if($isFirstYear){
            $dataArray['otherInfo'][] = ($allCustInfo['TssExperienceDay'][0]['Value'] == 'Y')? $Lang['Admission']['yes'] : $Lang['Admission']['no'];
            $dataArray['otherInfo'][] = $allCustInfo['TssExperienceDayNo'][0]['Value'];
        }
        $dataArray['otherInfo'][] = $allCustInfo['AcceptRemarks'][0]['Value'];
        #### Other Info END ####


        #### Official Use START ####
        $dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
        $dataArray['officialUse'][] = $Lang['Admission']['PaymentStatus'][$status['OnlinePayment']]; // Omas
        $dataArray['officialUse'][] = $status['receiptID'];
        $dataArray['officialUse'][] = $status['receiptdate'];
        $dataArray['officialUse'][] = $status['handler'];
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
        $dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
        $dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
        $dataArray['officialUse'][] = $interviewInfo?$interviewInfo['Date'].' ('.substr($interviewInfo['StartTime'], 0, -3).' ~ '.substr($interviewInfo['EndTime'], 0, -3).')'.($interviewInfo['GroupName']?' '.($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room'?$kis_lang['interviewroom']:$kis_lang['sessiongroup']).' '.$interviewInfo['GroupName']:''):'';
        $dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
        $dataArray['officialUse'][] = $status['remark'];
        #### Official Use END ####

        $ExportArr = array_merge(
            array($dataArray[0],$dataArray[1],$dataArray[2]),
            $dataArray['studentInfo'],
            $dataArray['parentInfo'],
            $dataArray['academicBackgroundInfo'],
            $dataArray['otherInfo'],
            $dataArray['officialUse']
        );
        ######### Export data END ########

        return $ExportArr;
    }


    public function getIsFirstYear($classLevelID = '')
    {
	    $allClassLevel = $this->getClassLevel();
	    $yearName = $allClassLevel[$classLevelID];
	    $isFirstYear = (
	        filter_var($yearName, FILTER_SANITIZE_NUMBER_INT) == 1 ||
	        strpos($yearName, '一') !== false
        );
	    return $isFirstYear;
    }

    public function getNextApplicationNum($nextApplicationId, $schoolYearId)
    {
        $classId = IntegerSafe($_REQUEST['sus_status']);
        $yearStart = date('y', getStartOfAcademicYear('', $this->schoolYearID));

        if($this->getIsFirstYear($classId)){
            $applicationIdFormat = "A{$yearStart}%";
            $lastApplicationId = "A{$yearStart}13020";
        }else{
            $applicationIdFormat = "B{$yearStart}%";
            $lastApplicationId = "B{$yearStart}0000";
        }


        $sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '{$applicationIdFormat}' ORDER BY RecordID DESC LIMIT 1";
        $rs = $this->returnVector($sql);
        $lastApplicationId = (count($rs)) ? $rs[0] : $lastApplicationId;

        preg_match('/([A|B])(\d+)/', $lastApplicationId, $matches);
        $nextApplicationId = $matches[1] . ($matches[2] + 1);

        return $nextApplicationId;
    }
















} // End Class