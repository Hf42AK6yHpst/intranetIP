<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<title>:: eClass KIS ::</title>
	<style type="text/css">
		.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
		.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
		.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
		.tg-table-plain td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
		.print_field_title { background: #EFEFEF}
		.print_field_title_main { background:#D7D7D7}
		.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
		.print_field_row1 { width:120px}
		.print_field_row2 { width:28px}
		.print_field_row3 { width:80px}
		.print_field_title_remark { background:#B9B9B9; width:290px;}
		.print_field_title_parent{ width:181px;}
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
		}
	</style>
</head>
<body>
<?php

$x = '<div class="input_form" style="width:720px;margin:auto;">';
		$x .= "<div style='float:right;padding-top:5px'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=180&height=40&format=PNG'></div>";
		$x .= '<h4 align="center" style="padding-top:45px">民生書院幼稚園<br/>Munsang College Kindergarten<br/>年度</h4>';
		//date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'-'.date('Y',getEndOfAcademicYear('',$lac->getNextSchoolYearID()))
		$x .= '<h4 align="center">報名表<br/>Application Form</h4>';
		
//		$interviewList = current($lac->getInterviewListAry($othersInfo['InterviewSettingID']));
//		$interviewDate .= $interviewList['Date'].' ('.substr($interviewList['StartTime'], 0, -3).' ~ '.substr($interviewList['EndTime'], 0, -3).')'; 	
		
		$x .='<div style="border: 2px solid;padding: 5px;width:720px;">';
		
		//student information
		$x .= '<table align="center" class="tg-table-plain">
				<tr>
				    <td colspan="3">
						<table>
							<tr>
								<td style="border:0">申請班級*<br/>Class Applied For</td>
								<td style="border:0"><b>幼兒班<br/>Nursery Class</b></td>
								<td style="border:0" nowrap>
									<input type="checkbox" onclick="return false"/> 上午英粵班 Eng / Cantonese A.M.<br/>
									<input type="checkbox" onclick="return false"/> 下午英粵班 Eng / Cantonese P.M.<br/>
									<input type="checkbox" onclick="return false"/> 下午英普班 Eng / Mandarin P.M.<br/>
								</td>
							</tr>
						</table>
					</td>
				    <td colspan="2">
						如申請的學習時段額滿，是 <input type="checkbox" onclick="return false"/> / 否 <input type="checkbox" onclick="return false"/> 願意由學校重新編配。<br/>
						If the session I have applied for is full, I will <input type="checkbox" onclick="return false"/> / will not <input type="checkbox" onclick="return false"/> consider the alternative session.
					</td>
			    </tr>
				<tr>
					<td rowspan="2" width="30px">姓名*<br/>Name</td>
					<td colspan="3"><table style="padding:0;margin:0;"><tr><td style="border:0">中文<br/>Chinese</td><td style="border:0"><!--Answer here--></td></tr></table></td>
					<td rowspan="6" width="150px"><img src="'.$personalPhotoPath.'" width="150px" /></td>
				</tr>
				<tr>
					<td colspan="3"><table><tr><td style="border:0;padding:0;margin:0;">英文<br/>English</td><td style="border:0"><!--Answer here--></td></tr></table></td>
				</tr>
				<tr>
					<td colspan="2"><table><tr><td style="border:0;padding:0;margin:0;">出生日期*<br/>Date of Birth</td><td style="border:0"><!--Answer here--></td></tr></table></td>
					<td colspan="2"><table><tr><td style="border:0;padding:0;margin:0;">性別*<br/>Sex</td><td style="border:0"><!--Answer here--></td></tr></table></td>
				</tr>
				<tr>
					<td colspan="2"><table><tr><td style="border:0;padding:0;margin:0;">出生地點*<br/>Place of Birth</td><td style="border:0"><!--Answer here--></td></tr></table></td>
					<td colspan="2"><table><tr><td style="border:0;padding:0;margin:0;">宗教*<br/>Religion</td><td style="border:0"><!--Answer here--></td></tr></table></td>
				</tr>
				<tr>
					<td colspan="2"><table><tr><td style="border:0;padding:0;margin:0;">出生證明書號碼*<br/>Birth Certificate Number</td><td style="border:0"><!--Answer here--></td></tr></table></td>
					<td colspan="2"><table><tr><td style="border:0;padding:0;margin:0;">電話*<br/>Telephone</td><td style="border:0"><!--Answer here--></td></tr></table></td>
				</tr>
				<tr>
					<td colspan="4"><table><tr><td style="border:0;padding:0;margin:0;">地址*<br/>Address</td><td style="border:0"><!--Answer here--></td></tr></table></td>
				</tr>
				</table>';
		
		//Parent information
		$x .= '<table align="center" class="tg-table-plain" style="margin-top: 10px;">
				<tr>
					<td></td>
					<td class ="print_field_title_remark">父親 Father</td>
					<td class ="print_field_title_remark">母親 Mother</td>
				</tr>
				<tr>
					<td>姓名*<br/>Name</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>公司<br/>Company</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>地址<br/>Address</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>職業<br/>Occupation</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>手提電話*<br/>Mobile Phone No.</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>電郵地址*<br/>E-mail Address</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>教育程度<br/>Level of Education</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>學校名稱<br/>Name of school</td>
					<td></td>
					<td></td>
				</tr>
				</table>';
				
		//other information		
		$x .= '<table align="center" class="tg-table-plain" style="margin-top: 10px;">
				<tr>
					<td colspan="3">幼兒過去入學記錄 Record of Previous Schooling of the Child*</td>
				</tr>
				<tr class ="print_field_title_remark">
					<td>年份 Year</td>
					<td>級別 Class</td>
					<td>學校名稱 Name of School</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				</table>';
		$x .= '<table align="center" class="tg-table-plain" style="margin-top: 10px;margin-bottom: 10px;">
				<tr>
					<td colspan="4">請列出曾經或現正在本校就讀/工作的親屬資料 (如有)<br/>Please list all the relatives who have studying / working or having studied / worked at our College (if applicable)</td>
				</tr>
				<tr class ="print_field_title_remark">
					<td>年份 Year</td>
					<td>姓名 Name</td>
					<td>就讀級別 / 職位<br/>Class / Position</td>
					<td>與申請人關係<br/>Relationship with the Applicant</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				</table>';
				
		//remarks
		$x .= '<div style="font-size: 12px;">
				根據個人資料(私穩)條例，以上個人資料只用於報讀幼稚園，完成報名程序後，所有資料將會註銷。<br/>
				According to the Personal Data (Privacy) Ordinance, the above Personal data will be used for Kindergarten application only. All data will be written off after application procedure completed.<br/><br/>
				備註 Note:<br/>
				1. 必須提供標示*的資料，否則申請將不能進行，並視作放棄論。<br/>
				The * marked information are compulsory and must be provided in order for this application to be processed. Failure to provide such data will be treated as withdrawn.<br/>
				2. 沒有標示*的資料為非必要資料，家長/申請人可自行決定提供與否。
				Information not marked with * are optional. Parent/Applicant can decide whether or not to provide the information.
				</div>';

		$x .='</div>';
		
//		$x .='<table width="100%">
//				<tr>
//					<td><span style="font-size: 13px">申請編號：'.$othersInfo['applicationID'].'</span></td>
//					<td><span style="font-size: 13px">面試日期：'.$interviewDate.'</span></td>
//					<td><span style="float:right;font-size: 12px">填表日期：'.substr($othersInfo['DateInput'], 0, -9).'</span></td>
//				</tr>
//			</table>';
//
//		$x .= '<table align="center" class="tg-table-plain">
//			  <tr>
//			    <td class="print_field_title print_field_row1">中文姓名</td>
//			    <td>'.$studentInfo['student_name_b5'].'</td>
//			    <td class="print_field_title">性別</td>
//			    <td>'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td>
//			    <td rowspan="5" colspan="2" width="100" align="center"><img src="'.$personalPhotoPath.'" width="100px" /><!--相片--></td>
//			  </tr>
//			  <tr class="tg-even">
//				<td class="print_field_title">英文姓名</td>
//			    <td>'.$studentInfo['student_name_en'].'</td>
//				<td class="print_field_title">籍貫</td>
//			    <td>'.$studentInfo['province'].'</td>
//			  </tr>
//			  <tr>
//				<td class="print_field_title">出生日期</td>
//				<td>'.substr($studentInfo['dateofbirth'], 0, 4).' 年 '.substr($studentInfo['dateofbirth'], 5, 2).' 月 '.substr($studentInfo['dateofbirth'], 8, 2).' 日</td>
//			    <td class="print_field_title">年齡</td>
//			    <td>'.$studentInfo['age'].'</td>
//			  </tr>
//			  <tr class="tg-even">
//			    <td class="print_field_title">出世證編號</td>
//			    <td>'.$birth_cert_type_selection.$studentInfo['birthcertno'].'</td>
//				<td class="print_field_title">出生地點</td>
//			    <td>'.$studentInfo['placeofbirth'].'</td>
//			  </tr>
//			  <tr class="tg-even">
//			    <td class="print_field_title">電郵</td>
//			    <td colspan="3">'.$studentInfo['email'].'</td>
//			  </tr>
//			  <tr>
//			    <td class="print_field_title">地址</td>
//			    <td colspan="5">'.$studentInfo['homeaddress'].'</td>
//			  </tr>
//			  <tr>
//				<td class="print_field_title">聯絡電話</td>
//			    <td>'.$studentInfo['homephoneno'].'</td>
//				<td class="print_field_title">宗教</td>
//			    <td>'.'</td>
//				<td class="print_field_title">所屬教會</td>
//			    <td>'.$studentInfo['church'].'</td>
//			  </tr>
//			  <tr>
//				<td rowspan="5" class="print_field_title">家長資料</td>
//			  </tr>
//			  <tr>
//			    <td colspan="3">父親姓名：'.$fatherInfo['parent_name_b5'].'</td>
//			    <td class="print_field_title">手提電話</td>
//				<td>'.$fatherInfo['mobile'].'</td>
//			  </tr>
//			  <tr>
//				<td colspan="3">身份證：'.$fatherInfo['hkid'].'</td>
//			  	<td class="print_field_title">職業</td>
//				<td>'.$fatherInfo['occupation'].'</td>
//			  </tr>
//			  <tr>
//			    <td colspan="3">母親姓名：'.$motherInfo['parent_name_b5'].'</td>
//			    <td class="print_field_title">手提電話</td>
//				<td>'.$motherInfo['mobile'].'</td>
//			  </tr>
//			  <tr>
//				<td colspan="3">身份證：'.$motherInfo['hkid'].'</td>
//			  	<td class="print_field_title">職業</td>
//				<td>'.$motherInfo['occupation'].'</td>
//			  </tr>
//			  
//			  <tr>
//				<td class="print_field_title">擬報讀班報</td>
//				<td colspan="5"><span class="input_content">'.$classLevel[$othersInfo['classLevelID']].'</span></td>
//			  </tr>
//			  <tr>
//			    <td class="print_field_title">班級</td>
//			    <td colspan="5">'.$dayTypeOption.'</td>
//			  </tr>';
//			  if($type != 'reply_note'){
//			  $x.='<tr>
//				<td class="print_field_title">&nbsp;</td>
//				<td colspan="5"><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['SiblingAppliedName']!=""?'checked':'').' />有兄弟姊妹同時報讀本園</span> 姓名：<span class="input_content">'.$othersInfo['SiblingAppliedName'].'</span></td>
//			  </tr>
//			  <tr>
//				<td class="print_field_title">&nbsp;</td>
//				<td colspan="5">
//					<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />兄姐是本園畢業生</span>&nbsp;&nbsp;&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />1位</span>&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />2位</span>&nbsp;<span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ExBSName']!=""?'checked':'').' />3位</span><br/>
//					<span class="input_content">姓名：<span>'.$othersInfo['ExBSName'].'</span> (畢業年份：<span>'.$othersInfo['ExBSGradYear'].'</span>)&nbsp;&nbsp;姓名：<span>'.$othersInfo['ExBSName2'].'</span> (畢業年份：<span>'.$othersInfo['ExBSGradYear2'].'</span>)&nbsp;&nbsp;姓名：<span>'.$othersInfo['ExBSName3'].'</span> (畢業年份：<span>'.$othersInfo['ExBSGradYear3'].'</span>)</span>
//				</td>
//			  </tr>
//			  <tr>
//				<td class="print_field_title">由何校轉來</td>
//				<td colspan="5">
//					<span class="input_content">'.$studentInfo['lastschool'].'</span>
//				</td>
//			  </tr>
//			  <tr>
//				<td class="print_field_title">備註</td>
//				<td colspan="5">
//					<span class="input_content">'.$othersInfo['Remarks'].'</span>
//				</td>
//			  </tr>
//			</table>
//			<br/>
//			<table align="center" class="tg-table-plain">
//			  <tr><td colspan="5" class="print_field_title"><center>學 校 專 用</center></td></tr>
//			  <tr>
//				<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['ApplyFee']!=""?'checked':'').' />報名費</span></td>
//				<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['RegisterFee']!=""?'checked':'').' />註冊費</span></td>
//			 	<td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['BookFee']!=""?'checked':'').' />書簿雜費</span></td>
//			    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['UniformsFee']!=""?'checked':'').' />校服/書包</span></td>
//			    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($othersInfo['BookFee']!=""?'checked':'').' />學券</span></td>
//			    <!--<td>入學日期<br/><span class="input_content">___________</span></td>-->
//			 </tr>
//			';
//			  }
//			$x .= '</table></div>';
			echo $x;

?>
</body>
</html>