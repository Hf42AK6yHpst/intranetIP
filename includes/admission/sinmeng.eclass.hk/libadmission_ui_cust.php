<?php
# modifying by: Henry

/********************
 * Log :
 * Date		2014-07-25 [Henry]
 * 			add function getFinishPageEmailContent()
 * 
 * Date		2014-07-21 [YatWoon] 
 * 			failed to select Form option with IE8, checked that due to class "admission_select_class", temp remove for this class first
 * 
 * Date		2014-07-18 [YatWoon] [Case#F64448]
 * 			updated wordings / ui 
 * 
 * Date		2014-07-17 [YatWoon]
 * 			comment out the form selection "disabled" 
 * 
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_ui_cust_base
 * 			Modified getDocsUploadForm() to follow attachment settings
 * 			Moved getDocsUploadForm() to libadmission_ui_cust_base.php
 * 			
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step){
		global $Lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">
				<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].'</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].'</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].'</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>'.$Lang['Admission']['confirmation'].'</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:40px">'.$Lang['Admission']['finish'].'</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction, $ClassLevel = ""){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['csm']['msg']['defaultfirstpagemessage']; //Henry 20131107
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['csm']['onlineApplication'].'</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm($ClassLevel);
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac, $sys_custom;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage']; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
         $x .= $Instruction;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->';								
							if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
							}
							else{
								$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
							}
						$x .= '</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm($ClassLevel = ''){
		global $libkis_admission, $Lang, $lac;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		/*
		//disable to choose class when using central server
		$disable = '';
		if($ClassLevel)
			$disable = 'return false';
		*/
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d H:i') >= $value['StartDate'] && date('Y-m-d H:i') <= $value['EndDate']){
					$hasClassLevelApply = 1;
					$selected = '';
					/*
					if($key == $ClassLevel)
						$selected = "checked='checked'";
						*/
					if($value['DOBStart'] && $value['DOBEnd'])
						$displayDOBRange = '('.date("j/n/Y", strtotime($value['DOBStart'])).' '.$Lang['Admission']['to'].' '.date("j/n/Y", strtotime($value['DOBEnd'])).')';
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="'.$disable.'" '.$selected.'  />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].' '.$displayDOBRange.'</label><br/>';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025
		//$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .='<fieldset ><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';

		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Admission']['csm']['newApplication'], "button", "goto('step_index','step_instruction')", "SubmitBtn", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm($BirthCertNo = ""){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission, $sys_custom, $admission_cfg;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm(0,$BirthCertNo);
		$x .= $this->getParentForm();
		//$x .= $this->getOthersForm();
		$x .='<span class="text_remark">'.$Lang['Admission']['mandatoryfield'].'</span>';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->';
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0, $BirthCertNo = ""){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg, $kis_lang;
		
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1 style="font-size: 15px">'.$Lang['Admission']['csm']['studentInfo'].'</h1>
			<table class="form_table" style="font-size: 15px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['csm']['englishName'].'</td>
				<td>
					<table style="font-size: 15px">
				<tr><td width="20px">
				('.$Lang['Admission']['csm']['surname_en'].')</td><td>'.($IsConfirm?$formData['studentssurname_en']:'<input name="studentssurname_en" type="text" id="studentssurname_en" class="textboxtext" />').'</td></tr>
				<tr><td width="20px">
				('.$Lang['Admission']['csm']['firstname_en'].')</td><td>'.($IsConfirm?$formData['studentsfirstname_en']:'<input name="studentsfirstname_en" type="text" id="studentsfirstname_en" class="textboxtext" />').'</td></tr>
				</table>
					
				</td>
				<td class="field_title">'.$star.$Lang['Admission']['chinesename'].'</td>
				<td>
				<table style="font-size: 15px">
				<tr><td width="20px">
				('.$Lang['Admission']['csm']['surname_b5'].')</td><td>'.($IsConfirm?$formData['studentssurname_b5']:'<input name="studentssurname_b5" type="text" id="studentssurname_b5" class="textboxtext" />').'</td></tr>
				<tr><td width="20px">
				('.$Lang['Admission']['csm']['firstname_b5'].')</td><td>'.($IsConfirm?$formData['studentsfirstname_b5']:'<input name="studentsfirstname_b5" type="text" id="studentsfirstname_b5" class="textboxtext" />').'</td></tr>
				</table>		
				</td>
			</tr>
			<tr>
			<td class="field_title">'.$star.$Lang['Admission']['gender'].'</td>
				<td>';
				if($IsConfirm){
					$x .= $Lang['Admission']['genderType'][$formData['StudentGender']];
				}
				else{
					$x .=$this->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', '0','',$Lang['Admission']['genderType']['M']).'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', '0','',$Lang['Admission']['genderType']['F']);
				}
				
				$x .='</td>
				<td class="field_title">'.$star.$Lang['Admission']['csm']['CurBSName'].'</td>
				<td>';
//				if($IsConfirm){
//					$x .= $Lang['Admission'][$formData['StudentLastSchool']];
//				}
//				else{
//					$x .=$this->Get_Radio_Button('StudentLastSchool1', 'StudentLastSchool', 'yes', '0','',$Lang['Admission']['yes']).'&nbsp;&nbsp;'
//						.$this->Get_Radio_Button('StudentLastSchool2', 'StudentLastSchool', 'no', '0','',$Lang['Admission']['no']);
//				}
				if($IsConfirm){
					$x .= $Lang['Admission'][$formData['OthersCurBSName']];
				}
				else{
					$x .=$this->Get_Radio_Button('OthersCurBSName1', 'OthersCurBSName', 'yes', '0','',$Lang['Admission']['yes']).'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('OthersCurBSName2', 'OthersCurBSName', 'no', '0','',$Lang['Admission']['no']);
				}
			$x .='</td>
			</tr>
			<tr>
			<td class="field_title">'.$star.$Lang['Admission']['csm']['isTwins'].'</td>
				<td colspan="3">';
				if($IsConfirm){
					$x .= $formData['IsTwins']=="Y"?$Lang['Admission']['yes']:$Lang['Admission']['no'];
					if($formData['IsTwins']=="Y")
						$x .='<br/>( '.$Lang['Admission']['csm']['isTwinsApplied'].'<br/> '.($formData['IsTwinsApplied']=="Y"?$Lang['Admission']['yes2'].'(必須另外獨立填寫一張表格，其申請編號是 '.strtolower($formData['TwinsApplicationID']).'，如是第一個申請則不用填寫) ':$Lang['Admission']['no2']).')';
				}
				else{
					$x .=$this->Get_Radio_Button('IsTwins1', 'IsTwins', 'Y', '0','',$Lang['Admission']['yes'],"document.getElementById('IsTwinsApplied1').disabled = false;document.getElementById('IsTwinsApplied2').disabled = false;document.getElementById('TwinsApplicationID').disabled = false;").' '.$this->Get_Radio_Button('IsTwins2', 'IsTwins', 'N', '0','',$Lang['Admission']['no'],"document.getElementById('IsTwinsApplied1').disabled = true;document.getElementById('IsTwinsApplied2').disabled = true;document.getElementById('IsTwinsApplied1').checked = false;document.getElementById('IsTwinsApplied2').checked = false;document.getElementById('TwinsApplicationID').disabled = true;document.getElementById('TwinsApplicationID').value = '';");
					$x .='<br/>( '.$Lang['Admission']['csm']['isTwinsApplied'].'<br/> '.$this->Get_Radio_Button('IsTwinsApplied1', 'IsTwinsApplied', 'Y', '0','',$Lang['Admission']['yes2'].'(必須另外獨立填寫一張表格，其申請編號是 <input name="TwinsApplicationID" type="text" id="TwinsApplicationID" class="textboxtext" maxlength="10" size="10" style="width:100px" disabled />，如是第一個申請則不用填寫) ',"document.getElementById('TwinsApplicationID').disabled = false;",1).' '.$this->Get_Radio_Button('IsTwinsApplied2', 'IsTwinsApplied', 'N', '0','',$Lang['Admission']['no2'],"document.getElementById('TwinsApplicationID').disabled = true;document.getElementById('TwinsApplicationID').value = '';",1).')';
				}
			$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateofbirth'].'</td>
				<td>
				'.($IsConfirm?$formData['StudentDateOfBirth']:'<input style="width:80%" name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/><br/>(年年年年-月月-日日)').'</td>
				<!--<td class="field_title">'.$star.$Lang['Admission']['placeofbirth'].'</td>
				<td>'.($IsConfirm?$formData['StudentPlaceOfBirth']:'<input name="StudentPlaceOfBirth" type="text" id="StudentPlaceOfBirth" class="textboxtext" />').'</td>
			</tr>
			<tr>-->
				<td class="field_title">'.$star.$Lang['Admission']['csm']['birthcertno'].'</td>
				<td>'.($BirthCertNo?'<input name="StudentBirthCertNo" value="'.$BirthCertNo.'" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none"/>':($IsConfirm?$formData['StudentBirthCertNo']:'<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="8" size="8"/><br/>'.$Lang['Admission']['csm']['msg']['birthcertnohints'])).'</td>
				<!--<td class="field_title">'.$star.$Lang['Admission']['csm']['homephoneno'].'</td>
				<td >'.($IsConfirm?$formData['StudentHomePhoneNo']:'<input name="StudentHomePhoneNo" type="text" id="StudentHomePhoneNo" class="textboxtext" maxlength="8"/>').'</td>-->
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['csm']['homeaddress'].'</td>
				<td colspan="3">';
				if($IsConfirm){
					if(count($Lang['Admission']['csm']['AddressLocation']) != $formData['StudentHomeAddress']){
						$x .= $Lang['Admission']['csm']['AddressLocation'][$formData['StudentHomeAddress']];
					}
					else{
						$x .= $formData['StudentHomeAddressOthers'];
					}
					
				}
				else{
					for($i=1; $i<=count($Lang['Admission']['csm']['AddressLocation']); $i++){
						if($i == count($Lang['Admission']['csm']['AddressLocation'])){
							$x .= '<table><tr><td width="60px" style="padding:0px">'.$this->Get_Radio_Button('StudentHomeAddress'.$i, 'StudentHomeAddress', $i, '0','',$Lang['Admission']['csm']['AddressLocation'][$i]).'</td><td><input name="StudentHomeAddressOthers" type="text" id="StudentHomeAddressOthers" class="textboxtext" /></td></tr></table>';
						}else{
							$x .= $this->Get_Radio_Button('StudentHomeAddress'.$i, 'StudentHomeAddress', $i, '0','',$Lang['Admission']['csm']['AddressLocation'][$i]).'<br/>';
						}
					}
					//$x .= '<input type="text" name="StudentHomeAddress" class="textboxtext" id="StudentHomeAddress" />'.$this->GET_SELECTION_BOX($Lang['Admission']['csm']['AddressLocation'], '', '', 1);
				}
				$x.='</td>
			</tr>
			<!--<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].'</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/><br />
					<em>('.$Lang['Admission']['msg']['personalPhotoFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)</em>
					<br />').'
				</td>
			</tr>-->';
			
			$applicationSetting = $lac->getApplicationSetting();
			$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
			$dayTypeArr = explode(',',$dayType);
			
			$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['csm']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['csm']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
		else{
			foreach($dayTypeArr as $aDayType){
	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
	//			$dayTypeOption .=" ";
				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
			}
			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
		}
			if($IsConfirm){
			$x .='<tr style="display:none">
				<td class="field_title">'.$star.$Lang['Admission']['csm']['applyDayType'].'</td>
				<td>'.$classLevel.'</td>
				<td class="field_title">'.$Lang['Admission']['csm']['priorities'].'</td>
				<td>
				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
				
				
			</tr>';
			}
			else{			
			$x .='<tr style="display:none">
				<td class="field_title">'.$star.$Lang['Admission']['csm']['applyDayType'].'</td>
				<td colspan="3">
					<div id="DayTypeOption">'.$dayTypeOption.'</div>
				</td>
			</tr>';
			}
		if($_SESSION["platform"]=="KIS" && $_SESSION["UserID"]){
			$x .='<tr>
					<td class="field_title">'.$Lang['Admission']['csm']['ApplicationNo'].' (此輸入項目只限於管理時顯示，不輸入將會自動產生編號)</td>
					<td colspan="3">'.($IsConfirm?$formData['CustApplicationID']:'<input name="CustApplicationID" type="text" id="CustApplicationID" class="textboxtext" maxlength="20"/>').'</td>
				</tr>';
		}
		$x .='</table>';
		
		return $x;
	}
	
	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang, $lac;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1 style="font-size: 15px">'.$Lang['Admission']['csm']['PGInfo'].'</h1>
				<table class="form_table" style="font-size: 15px">
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['relationshipBetweenChild'].'</td>
					<td class="form_guardian_head">'.$this->Get_Radio_Button($IsConfirm?'tempRadio1':'StudentRelationship1', $IsConfirm?'tempRadio':'StudentRelationship', 'F', ($IsConfirm?($formData['StudentRelationship'] == 'F'?'1':'0'):'0'),'',$IsConfirm?'':$Lang['Admission']['PG_Type']['F'],$IsConfirm?'return false':'').' '.($IsConfirm?$Lang['Admission']['PG_Type']['F']:'').' </td>
					<td class="form_guardian_head">'.$this->Get_Radio_Button($IsConfirm?'tempRadio2':'StudentRelationship2', $IsConfirm?'tempRadio':'StudentRelationship', 'M', ($IsConfirm?($formData['StudentRelationship'] == 'M'?'1':'0'):'0'),'',$IsConfirm?'':$Lang['Admission']['PG_Type']['M'],$IsConfirm?'return false':'').' '.($IsConfirm?$Lang['Admission']['PG_Type']['M']:'').'</td>
					<td class="form_guardian_head">'.$this->Get_Radio_Button($IsConfirm?'tempRadio3':'StudentRelationship3', $IsConfirm?'tempRadio':'StudentRelationship', 'G', ($IsConfirm?($formData['StudentRelationship'] == 'G'?'1':'0'):'0'),'',$IsConfirm?'':$Lang['Admission']['csm']['PG_Type']['G'],$IsConfirm?'return false':'',1).' '.($IsConfirm?$Lang['Admission']['PG_Type']['G']:'').($IsConfirm?$formData['G3Relationship']:'<input name="G3Relationship" type="text" id="G3Relationship" class="textboxtext" style="width:40%;" disabled />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['parantenglishname'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1EnglishName']:'<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2EnglishName']:'<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3EnglishName']:'<input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext" style="width:60%" disabled />').' '.$Lang['Admission']['ifapplicable'].'</td>
				</tr>
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['parantchinesename'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1ChineseName']:'<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2ChineseName']:'<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3ChineseName']:'<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" style="width:60%" disabled />').' '.$Lang['Admission']['ifapplicable'].'</td>
				</tr>
				<!--<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['paranthkid'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G1HKID']:'<input name="G1HKID" type="text" id="G1HKID" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G2HKID']:'<input name="G2HKID" type="text" id="G2HKID" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3HKID']:'<input name="G3HKID" type="text" id="G3HKID" class="textboxtext" />').'</td>
				</tr>-->
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['parentlivewithchild'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?($formData['G1LiveWithChild']=="Y"?$Lang['Admission']['yes']:($formData['G1LiveWithChild']=="N"?$Lang['Admission']['no']:'--')):$this->Get_Checkbox('G1LiveWithChild1', 'G1LiveWithChild', 'Y', '0','',$Lang['Admission']['yes'], "document.getElementById('G1LiveWithChild2').checked = false;check_parent_live_with_child();").' '.$this->Get_Checkbox('G1LiveWithChild2', 'G1LiveWithChild', 'N', '0','',$Lang['Admission']['no'], "document.getElementById('G1LiveWithChild1').checked = false;check_parent_live_with_child();")).'</td>
					<td class="form_guardian_field">'.($IsConfirm?($formData['G2LiveWithChild']=="Y"?$Lang['Admission']['yes']:($formData['G2LiveWithChild']=="N"?$Lang['Admission']['no']:'--')):$this->Get_Checkbox('G2LiveWithChild1', 'G2LiveWithChild', 'Y', '0','',$Lang['Admission']['yes'], "document.getElementById('G2LiveWithChild2').checked = false;check_parent_live_with_child();").' '.$this->Get_Checkbox('G2LiveWithChild2', 'G2LiveWithChild', 'N', '0','',$Lang['Admission']['no'], "document.getElementById('G2LiveWithChild1').checked = false;check_parent_live_with_child();")).'</td>
					<td class="form_guardian_field">--<!--'.($IsConfirm?($formData['G3LiveWithChild']=="Y"?$Lang['Admission']['yes']:($formData['G3LiveWithChild']=="N"?$Lang['Admission']['no']:'--')):$this->Get_Checkbox('G3LiveWithChild1', 'G3LiveWithChild', 'Y', '0','',$Lang['Admission']['yes'], "document.getElementById('G3LiveWithChild2').checked = false;").' '.$this->Get_Checkbox('G3LiveWithChild2', 'G3LiveWithChild', 'N', '0','',$Lang['Admission']['no'], "document.getElementById('G3LiveWithChild1').checked = false;")).'--></td>
				</tr>
				<!--<tr>
				<td class="field_title">'.$star.$Lang['Admission']['csm']['IsFamilySpecialCase'].'</td>
				<td colspan="3">';
				if($IsConfirm){
					$x .= $formData['IsFamilySpecialCase']=="Y"?$Lang['Admission']['yes']:$Lang['Admission']['no'];
//					if($formData['IsSingleParent']=="Y")
//						$x .=' ( '.$Lang['Admission']['csm']['isFullTime'].' '.($formData['IsFullTime']=="Y"?$Lang['Admission']['yes']:$Lang['Admission']['no']).')';
					if($formData['IsFamilySpecialCase']=="Y")
						$x .=' ( '.$Lang['Admission']['csm']['IsApplyFullDayCare'].' '.($formData['IsApplyFullDayCare']=="Y"?$Lang['Admission']['yes']:$Lang['Admission']['no']).')';
				
				}
				else{
					$x .=$this->Get_Radio_Button('IsFamilySpecialCase1', 'IsFamilySpecialCase', 'Y', ($formData['IsFamilySpecialCase'] == 'Y'),'',$Lang['Admission']['yes']).' '.$this->Get_Radio_Button('IsFamilySpecialCase2', 'IsFamilySpecialCase', 'N', ($formData['IsFamilySpecialCase'] == 'N'),'',$Lang['Admission']['no']);
					//$x .=' ( '.$Lang['Admission']['csm']['isFullTime'].' '.$this->Get_Radio_Button('IsFullTime1', 'IsFullTime', 'Y', '0','',$Lang['Admission']['yes']).' '.$this->Get_Radio_Button('IsFullTime2', 'IsFullTime', 'N', '0','',$Lang['Admission']['no']).')';
					$x .=' ( '.$Lang['Admission']['csm']['IsApplyFullDayCare'].' '.$this->Get_Radio_Button('IsApplyFullDayCare1', 'IsApplyFullDayCare', 'Y', '0','',$Lang['Admission']['yes'],'$(\'#IsApplyFullDayCare1_temp\').prop(\'checked\',true)').' '.$this->Get_Radio_Button('IsApplyFullDayCare2', 'IsApplyFullDayCare', 'N', '0','',$Lang['Admission']['no'],'$(\'#IsApplyFullDayCare2_temp\').prop(\'checked\',true)').')';
				}
				$x .='</td>
				</tr>-->
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['mobile'].'</td>
					<td class="form_guardian_field">'.$this->Get_Radio_Button($IsConfirm?'tempStudentContactPerson1':'StudentContactPerson1', $IsConfirm?'tempStudentContactPerson':'StudentContactPerson', 'F', $IsConfirm?($formData['StudentContactPerson'] == 'F'):'','','',$IsConfirm?'return false':'').' '.($IsConfirm?$formData['G1Mobile']:'<input name="G1Mobile" type="text" id="G1Mobile" class="textboxtext" style="width:80%" maxlength="8"/>').'</td>
					<td class="form_guardian_field">'.$this->Get_Radio_Button($IsConfirm?'tempStudentContactPerson2':'StudentContactPerson2', $IsConfirm?'tempStudentContactPerson':'StudentContactPerson', 'M', $IsConfirm?($formData['StudentContactPerson'] == 'M'):'','','',$IsConfirm?'return false':'').' '.($IsConfirm?$formData['G2Mobile']:'<input name="G2Mobile" type="text" id="G2Mobile" class="textboxtext" style="width:80%" maxlength="8" />').'</td>
					<td class="form_guardian_field">'.$this->Get_Radio_Button($IsConfirm?'tempStudentContactPerson3':'StudentContactPerson3', $IsConfirm?'tempStudentContactPerson':'StudentContactPerson', 'G', $IsConfirm?($formData['StudentContactPerson'] == 'G'):'','','',$IsConfirm?'return false':'',1).' '.($IsConfirm?$formData['G3Mobile']:'<input name="G3Mobile" type="text" id="G3Mobile" class="textboxtext" style="width:80%" maxlength="8" disabled />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$star.$Lang['Admission']['csm']['email'].'</td>
					<td class="form_guardian_field" colspan="3">'.($IsConfirm?$formData['G1Email']:'<input name="G1Email" type="text" id="G1Email" class="textboxtext" />').'</td>
					<!--<td class="form_guardian_field">'.($IsConfirm?$formData['G2Email']:'<input name="G2Email" type="text" id="G2Email" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['G3Email']:'<input name="G3Email" type="text" id="G3Email" class="textboxtext" />').'</td>-->
				</tr>
				<tr>
					<td class="field_title">&nbsp;</td>
					<td class="form_guardian_field" colspan="3">'.$star.$Lang['Admission']['csm']['inputParentHint'].'</td>
				</tr>
			</table>';
//				<tr>
//				<td class="field_title">'.$star.str_replace('<!--AcademicYear-->',date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())),$Lang['Admission']['csm']['singleparent']).'</td>
//				<td colspan="3">';
//				if($IsConfirm){
//					$x .= $formData['IsSingleParent']=="Y"?$Lang['Admission']['yes']:($formData['IsSingleParent']=="N"?$Lang['Admission']['no'].'/ '.$Lang['Admission']['na']:$Lang['Admission']['na']);
////					if($formData['IsSingleParent']=="Y")
////						$x .=' ( '.$Lang['Admission']['csm']['isFullTime'].' '.($formData['IsFullTime']=="Y"?$Lang['Admission']['yes']:$Lang['Admission']['no']).')';
//					if($formData['IsSingleParent']=="Y")
//						$x .='<br/> ( '.$Lang['Admission']['csm']['IsApplyFullDayCare'].' '.($formData['IsApplyFullDayCare']=="Y"?$Lang['Admission']['agree']:$Lang['Admission']['disagree']).' )';
//				
//				}
//				else{
//					$x .=$this->Get_Radio_Button('IsSingleParent1', 'IsSingleParent', 'Y', '0','',$Lang['Admission']['yes'],"document.getElementById('IsApplyFullDayCare1').disabled = false;document.getElementById('IsApplyFullDayCare2').disabled = false;").'&nbsp;&nbsp;&nbsp;'.$this->Get_Radio_Button('IsSingleParent2', 'IsSingleParent', 'N', '0','',$Lang['Admission']['no'].'/ '.$Lang['Admission']['na'],"document.getElementById('IsApplyFullDayCare1').disabled = true;document.getElementById('IsApplyFullDayCare2').disabled = true;document.getElementById('IsApplyFullDayCare1').checked = false;document.getElementById('IsApplyFullDayCare2').checked = false;");
//					//$x .=' ( '.$Lang['Admission']['csm']['isFullTime'].' '.$this->Get_Radio_Button('IsFullTime1', 'IsFullTime', 'Y', '0','',$Lang['Admission']['yes']).' '.$this->Get_Radio_Button('IsFullTime2', 'IsFullTime', 'N', '0','',$Lang['Admission']['no']).')';
//					$x .='<br/> ( '.$Lang['Admission']['csm']['IsApplyFullDayCare'].' '.$this->Get_Radio_Button('IsApplyFullDayCare1', 'IsApplyFullDayCare', 'Y', '0','',$Lang['Admission']['agree'],'',1).'&nbsp;&nbsp;&nbsp;'.$this->Get_Radio_Button('IsApplyFullDayCare2', 'IsApplyFullDayCare', 'N', '0','',$Lang['Admission']['disagree'],'',1).' )';
//				}
//				$x .='</td>
//				</tr>
//			</table>';
			
		return $x;
	}
	
	function getOthersForm($IsConfirm=0){
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());
		//'<input name="OthersApplyYear" type="text" id="OthersApplyYear" class="" size="10" value="" maxlength="4"/>'
		//$formData['OthersApplyYear']
		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];
//			for($i=1; $i<=3; $i++){
//			if($formData['OthersApplyDayType'.$i] == 1)
//				$dayTypeOption1 = $kis_lang['Admission']['Option']." 1: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 2)
//				$dayTypeOption2 = $kis_lang['Admission']['Option']." 2: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			if($formData['OthersApplyDayType'.$i] == 3)
//				$dayTypeOption3 = $kis_lang['Admission']['Option']." 3: ".$Lang['Admission']['TimeSlot'][$i]." ";
//			}
			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}
//			$dayTypeOption .=$dayTypeOption1.$dayTypeOption2.$dayTypeOption3;
		}
		else{
			foreach($dayTypeArr as $aDayType){
	//			$dayTypeOption .= $this->Get_Radio_Button('OthersApplyDayType'.$aDayType, 'OthersApplyDayType', $aDayType, '0','',$Lang['Admission']['TimeSlot'][$aDayType]);
	//			$dayTypeOption .=" ";
				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
			}
			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['otherInfo'].'</h1>
			<table class="form_table" style="font-size: 15px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['familyStatus'].'</td>
				<td colspan="3"><!--<input name="OthersFamilyStatus" type="text" id="OthersFamilyStatus" class="textboxtext" />-->
				<table style="font-size: 15px">
					<tr>
						<td>'.$Lang['Admission']['elderBrother'].' ('.$Lang['Admission']['person'].')</td><td>'.$Lang['Admission']['elderSister'].' ('.$Lang['Admission']['person'].')</td><td>'.$Lang['Admission']['youngerBrother'].' ('.$Lang['Admission']['person'].')</td><td>'.$Lang['Admission']['youngerSister'].' ('.$Lang['Admission']['person'].')</td>
					</tr>
					<tr>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_EB']:$this->Get_Number_Selection('OthersFamilyStatus_EB', '0', '5','0')).'</td>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_ES']:$this->Get_Number_Selection('OthersFamilyStatus_ES', '0', '5','0')).'</td>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_YB']:$this->Get_Number_Selection('OthersFamilyStatus_YB', '0', '5','0')).'</td>
						<td>'.($IsConfirm?$formData['OthersFamilyStatus_YS']:$this->Get_Number_Selection('OthersFamilyStatus_YS', '0', '5','0')).'</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateOfEntry'].'</td>
				<td><table style="font-size: 15px">
					<tr><td width="40px">('.$Lang['General']['SchoolYear'].')</td><td>'.($IsConfirm?$admission_year:$admission_year).'</td></tr>
					<tr><td width="40px">('.$Lang['Admission']['month'].')</td><td>'.($IsConfirm?$formData['OthersApplyMonth']:$this->Get_Number_Selection('OthersApplyMonth', '1', '12')).'</td></tr>
					</table>
				<!--'.$this->GET_DATE_PICKER('OthersApplyDate').'--></td>
				<td class="field_title">'.$star.$Lang['Admission']['applyTerm'].'</td>
				<td><!--<input name="OthersApplyTerm" type="text" id="OthersApplyTerm" class="textboxtext" />-->';
				if($IsConfirm){
					//$x .= "Term ".$formData['OthersApplyTerm'];
					$x .=$Lang['Admission']['Term'][$formData['OthersApplyTerm']];
				}
				else{
					$x .=$this->Get_Radio_Button('OthersApplyTerm1', 'OthersApplyTerm', '1', '0','',$Lang['Admission']['Term'][1]).' '
					.$this->Get_Radio_Button('OthersApplyTerm2', 'OthersApplyTerm', '2', '0','',$Lang['Admission']['Term'][2]);
				}
			$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>
				<td '.($IsConfirm?'':'colspan="3"').'>
				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
				'.($IsConfirm?'<td class="field_title">'.$Lang['Admission']['applyLevel'].'</td><td>'.$classLevel.'</td>':'').'
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['ExBSName'].'</td>
				<td>
					<table style="font-size: 15px">
						<tr>
							<td width="40px">('.$Lang['General']['Name'].')</td>
							<td>
								'.($IsConfirm?$formData['OthersExBSName']:'<input name="OthersExBSName" type="text" id="OthersExBSName" class="textboxtext" />').'
							</td>
						</tr>
						<tr>
							<td width="40px">('.$Lang['Admission']['level'].')</td>
							<td>
								'.($IsConfirm?$formData['OthersExBSLevel']:'<input name="OthersExBSLevel" type="text" id="OthersExBSLevel" class="textboxtext" />').'
							</td>
						</tr>
					</table>
				</td>
				<td class="field_title">'.$Lang['Admission']['CurBSName'].'</td>
				<td>
					<table style="font-size: 15px">
						<tr>
							<td width="40px">('.$Lang['General']['Name'].')</td>
							<td>
								'.($IsConfirm?$formData['OthersCurBSName']:'<input name="OthersCurBSName" type="text" id="OthersCurBSName" class="textboxtext" />').'
							</td>
						</tr>
						<tr>
							<td width="40px">('.$Lang['Admission']['level'].')</td>
							<td>
								'.($IsConfirm?$formData['OthersCurBSLevel']:'<input name="OthersCurBSLevel" type="text" id="OthersCurBSLevel" class="textboxtext" />').'
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['needSchoolBus'].'</td>
				<td>';
				if($IsConfirm){
					$x .= $formData['OthersNeedSchoolBus']==1?$Lang['Admission']['need']:$Lang['Admission']['noneed'];
				}
				else{
					$x .='
					<input type="radio" name="OthersNeedSchoolBus" id="OthersNeedSchoolBus1" value="1" />					
					<label for="OthersNeedSchoolBus1" >'.$Lang['Admission']['need'].'</label>					
					<input type="radio" name="OthersNeedSchoolBus" id="OthersNeedSchoolBus2" value="0" />					
					<label for="OthersNeedSchoolBus2">'.$Lang['Admission']['noneed'].' </label>';
				}
				$x .='</td>
				<td class="field_title">'.$star.$Lang['Admission']['placeForTakingSchoolBus'].' ('.$Lang['Admission']['msg']['placeForTakingSchoolBusHints'].')</td>
				<td>'.($IsConfirm?$formData['OthersSchoolBusPlace']:'<input name="OthersSchoolBusPlace" type="text" id="OthersSchoolBusPlace" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['knowUsBy'].'</td>
				<td colspan="3"><!--<input name="OthersKnowUsBy" type="text" id="OthersKnowUsBy" class="textboxtext" />-->';
				if($IsConfirm){
					if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['mailleaflet']){
						$x .= $Lang['Admission']['mailLeaflet'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['newspaper']['index']){
						$x .= $Lang['Admission']['newspaper'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['introduced']['index']){
						$x .= $Lang['Admission']['introduced'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['ourwebsite']['index']){
						$x .= $Lang['Admission']['ourwebsite'];
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['otherwebsite']['index']){
						$x .= $Lang['Admission']['otherwebsite'].' ('.$formData['txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index']].')';
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['advertisement']['index']){
						$x .= $Lang['Admission']['advertisement'].' ('.$formData['txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index']].')';
					}
					else if($formData['OthersKnowUsBy']==$admission_cfg['KnowUsBy']['others']['index']){
						$x .= $Lang['Admission']['others'].' ('.$formData['txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index']].')';
					}
				}
				else{
					$x .=$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['mailleaflet']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['mailleaflet']['index'], '0','',$Lang['Admission']['mailleaflet']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['newspaper']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['newspaper']['index'], '0','',$Lang['Admission']['newspaper']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['introduced']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['introduced']['index'], '0','',$Lang['Admission']['introduced']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['ourwebsite']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['ourwebsite']['index'], '0','',$Lang['Admission']['ourwebsite']).' '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['otherwebsite']['index'], '0','',$Lang['Admission']['otherwebsite'].' ('.$Lang['General']['PlsSpecify'].'): ').'
					<input name="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index'].'" type="text" id="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['otherwebsite']['index'].'" class="textboxtext" style="width:100px;"/><br/> '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['advertisement']['index'], '0','',$Lang['Admission']['advertisement'].' ('.$Lang['General']['PlsSpecify'].'): ').'
					<input name="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index'].'" type="text" id="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['advertisement']['index'].'" class="textboxtext" style="width:100px;"/> '
					.$this->Get_Radio_Button('OthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index'], 'OthersKnowUsBy', $admission_cfg['KnowUsBy']['others']['index'], '0','',$Lang['Admission']['others'].' ('.$Lang['General']['PlsSpecify'].'): ').'
					<input name="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index'].'" type="text" id="txtOthersKnowUsBy'.$admission_cfg['KnowUsBy']['others']['index'].'" class="textboxtext" style="width:100px;"/>';
				}
				$x .='</td>
			</tr>
			</table>';
		
		return $x;
	}
	
	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg, $lac, $kis_lang, $sys_custom;
		
		$attachment_settings = $lac->getAttachmentSettings();
		$attachment_settings_count  = sizeof($attachment_settings);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		//$x = '<form name="form1" method="POST" action="confirm.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//$x .= '<div class="admission_board">';
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1 style="font-size: 15px">'.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table" style="font-size: 15px">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
		
		for($i=0;$i<$attachment_settings_count;$i++) {
			$attachment_name = $attachment_settings[$i]['AttachmentName'];
			$x .='<tr>
					<td class="field_title">'.$star.$attachment_name.'</td>
					<td>'.($IsConfirm?'<div id="divOtherFile'.$i.'"><img src="#" style="max-width:600px" id="imgOtherFile'.$i.'"/><br/></div>'.stripslashes($fileData['OtherFile'.$i]).'<a href="#" target="_blank" id="hrefOtherFile'.$i.'"> ['.$kis_lang['preview'].']</a>':'<input type="file" name="OtherFile'.$i.'" id="OtherFile'.$i.'" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'
					<br/></td>
				  </tr>';
		}
			
		/*$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';*/
			
		//$x .=$this->Get_Upload_Attachment_UI('form1', 'BirthCert', 'testing', '1');
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->';
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}
				else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}	
			$x .= '</div>
			<p class="spacer"></p>';
		//$x .='</form>';
		//$x .='</div>';
		}
		return $x;
	}
	
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData, $sys_custom;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		//$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->';
				if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php?token='.$_REQUEST['token'].'\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}else{
					$x .= '<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />';
				}
			$x .= '</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $Lang, $lac, $admission_cfg,$sys_custom;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['csm']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<br><input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';
			$x .='<h1><span>申請通知電郵已發送，請檢查閣下在申請表填寫的電郵: '.$lac->getApplicantEmail($ApplicationID).'</span></h1>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Admission']['finish'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{	
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Admission']['finish'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}
	
	function getFinishPageEmailContent($ApplicationID='', $LastContent='', $schoolYearID=''){
		global $PATH_WRT_ROOT,$Lang, $lac, $admission_cfg,$sys_custom;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		if($ApplicationID){
			$x .=$Lang['Admission']['msg']['admissioncomplete'].$Lang['Admission']['msg']['yourapplicationno'].' <u>'.$ApplicationID.'</u>&nbsp;&nbsp;<br>報名表預覽<br/><br/>http://'.$_SERVER['HTTP_HOST'].$lac->getPrintLink("", $ApplicationID);         
		}
		else{
			$x .=$Lang['Admission']['msg']['admissionnotcomplete'].$Lang['Admission']['msg']['tryagain'];
        }
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x = '<p style="font-size:medium">'.$x.'</font>';
		$x .= '<br/>'.$LastContent;

		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac, $admission_cfg, $sys_custom;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';
//            $x .='<p>'.$Lang['Admission']['msg']['processyourapplication'].' '.$Lang['Admission']['msg']['contactus'].'<br />
//                           </p>';
//            $x .='<p>
//				    '.sprintf($Lang['Admission']['msg']['clicktoprint'],'<!--<div class="Content_tool" style="display:inline">--><a class="print" target="_blank" href="'.$lac->getPrintLink("", $ApplicationID).'"><b><u>'.$Lang['Admission']['msg']['here'].'</u></b></a><!--</div>-->').'				
//				</p>';            
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
            //$x .='<p>'.$Lang['Admission']['msg']['contactus'].'<br /></p>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';

		if(($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\''.$admission_cfg['IntegratedCentralServer'].'?af='.$_SERVER['HTTP_HOST'].'\'" value="'.$Lang['Btn']['Back'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		else{
			$x .= '<div class="edit_bottom">
					<input type="button" class="formsubbutton" onclick="location.href=\'index.php\'" value="'.$Lang['Btn']['Back'].'" />
				</div>
				<p class="spacer"></p></div>';
		}
		return $x;
	}

	function getPrintPageContent($schoolYearID,$applicationID, $type=""){ //using $type="teacher" if the form is print from teacher
		global $PATH_WRT_ROOT,$Lang,$kis_lang;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		$lac = new admission_cust();
		if($applicationID != ""){
		//get student information
		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
		foreach($parentInfo as $aParent){
			if($aParent['type'] == 'F'){
				$fatherInfo = $aParent;
			}
			else if($aParent['type'] == 'M'){
				$motherInfo = $aParent;
			}
			else if($aParent['type'] == 'G'){
				$guardianInfo = $aParent;
			}
		}
		
		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
		if($_SESSION['UserType']==USERTYPE_STAFF){
			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
			if(!is_date_empty($remarkInfo['interviewdate'])){
				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
				list($y,$m,$d) = explode('-',$date);
				if($hour>12){
					$period = '下午';
					$hour -= 12;
				}elseif($hour<12){
					$period = '上午';
				}else{
					$period = '中午';
				}
				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
			}else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
			}
		}
		else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
		}
		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
		$personalPhotoPath = $attachmentList['personal_photo']['link'];
		$classLevel = $lac->getClassLevel();
		
//		debug_pr($studentInfo);
//		debug_pr($fatherInfo);
//		debug_pr($motherInfo);
//		debug_pr($guardianInfo);
//		debug_pr($othersInfo);
		$applicationSetting = $lac->getApplicationSetting();
		$dayType = $applicationSetting[$othersInfo['classLevelID']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		for($i=1; $i<=count($dayTypeArr); $i++){
				if($othersInfo['ApplyDayType'.$i] != 0){
					//$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
					if(count($dayTypeArr)>1){
						$dayTypeOption .= "(選擇 ".$i.") ";
					}
					$dayTypeOption .= "<u>".$Lang['Admission']['csm']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."</u>&nbsp;&nbsp;";
				}
			}
		
//		$x = '<div id="printOption" style="float: right"><table width="90%" align="center" class="print_hide" border="0">
//			<tr>
//				<td align="right">'.$this->GET_ACTION_BTN($kis_lang['print'], "button", "javascript:window.print();").'</td>
//			</tr>
//		</table></div>';
		
		//Header of the page
		$x = '<div class="input_form" style="width:720px;margin:auto;">';
		$x .='<table width="100%">';
		$x .= "<tr><td colspan='3'><div style='float:right;padding-top:5px;'><img src='barcode.php?barcode=".rawurlencode($othersInfo['applicationID'])."&width=160&height=40&format=PNG'></div></td></tr>";
		$x .='<tr><td width="27%">';
		$x .= '</td><td width="40%">';
		$x .= '<h3 align="center">善明托兒所</h3>';
//		$x .= '<h4 align="center"><u>入托報名表 ('.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'-'.
//		date('Y',getEndOfAcademicYear('',$lac->getNextSchoolYearID())).'年度適用)</u></h4>';
		$x .= '<h4 align="center"><u>報名表 ('.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).' 年度適用)</u></h4>';
		$x .= '</td><td width="33%">';
		$x .= '<table style="font: inherit; width:100%;background: #D7D7D7; border: 1px #555 solid; padding: 0px 5px; vertical-align: top; font-size: 12px;">
					<tr>
						<td>
							<b>托兒所專用</b>
							<br/><br/>
							收表日期: <u>'.substr($othersInfo['DateInput'], 0, -9).'</u>
							<br/><br/>
							輪候編號: <u>'.$othersInfo['applicationID'].'</u>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
				</table>';
		$x .='</td></tr></table>';
		
		$stuNameArr_en = explode(',',$studentInfo['student_name_en']);
		$stuNameArr_b5 = explode(',',$studentInfo['student_name_b5']);
				
		$x .= '<br/><table align="center" class="tg-table-plain">
			  <tr>
			    <td colspan="5" style="border-bottom:2px #555 solid; border-top:2px #555 solid" class="print_field_title_main"><b>嬰 幼 兒 個 人 資 料</b><div style="float:right">請於合適的<input type="checkbox" onclick="return false"/>上加上「√」號</div></td>
			  </tr>
			  <tr>
			    <td colspan="2">
				<div style="display:inline;float:left">
				外文姓名
				<br/>
				中文姓名
				</div>
				<div style="display:inline;float:left">
				: (姓)
				<br/>
				: (姓) 
				</div>
				<div style="display:inline;float:left">
				&nbsp;<u>'.$stuNameArr_en[0].'</u>
				<br/>
				&nbsp;<u>'.$stuNameArr_b5[0].'</u>
				</div>
				<div style="display:inline;float:left">
				&nbsp;(名)
				<br/>
				&nbsp;(名)
				</div>
				<div style="display:inline;float:left">
				&nbsp;<u>'.$stuNameArr_en[1].'</u>
				<br/>
				&nbsp;<u>'.$stuNameArr_b5[1].'</u>
				</div>
				</td>
			    <td>
				性 別: 
				<u>'.($studentInfo['gender']=='M'?'男':'女').'</u>
				</td>
			  </tr>
			  <tr>
			    <td colspan="2">出生日期: <u>'.$studentInfo['dateofbirth'].'</u></td>
			    <!--<td>出生地點: <u>'.$studentInfo['placeofbirth'].'</u></td>-->
			    <!--<td rowspan="6" width="100"><img src="'.$personalPhotoPath.'" width="150px" /></td>-->
				<!--<td rowspan="2"># 須附上嬰幼兒澳門居民身份證</td>-->
			  <!--</tr>
			 <tr>-->
			    <td>澳門居民身份證號碼: <u>'.$studentInfo['birthcertno'].'</u></td>
			    <!--<td>住址電話:<u>'.$studentInfo['homephoneno'].'</u></td>-->
			  </tr>
			<tr>
			    <td colspan="3"><center>#填妥的申請表須連同<u><b>嬰幼兒之澳門居民身份證副本</b></u>一併遞交</center></td>
			  </tr>
			<tr>
			    <td colspan="3">住址所屬區域: <!--(根據家庭住址所屬區域，請在選項處<input type="checkbox" onclick="return false"/>填上√)--> <br/><u>';
			    if(is_numeric($studentInfo['homeaddress'])){
					$x .= $Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']];
				}
				else{
					$x .= $studentInfo['homeaddress'];
				}
			    $x.='</u></td>
			  </tr>
			<tr>
			    <td colspan="3">嬰幼兒是否雙胞/多胞胎？ '.($studentInfo['IsTwins']=="Y"?'<u>'.$Lang['Admission']['yes'].'</u>'.'<br/>(如是，其雙胞/多胞胎之兄弟姊妹有否同時申請本期服務？ '.($studentInfo['IsTwinsApplied']=="Y"?'<u>'.$Lang['Admission']['yes2'].'(必須另外獨立填寫一張表格，其申請編號是 '.($studentInfo['TwinsApplicationID']?$studentInfo['TwinsApplicationID']:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;').'，如是第一個申請則不用填寫)</u> ':'<u>'.$Lang['Admission']['no2'].'</u>').' )':'<u>'.$Lang['Admission']['no'].'</u>').'</u>
				</td>
			  </tr>
			<tr>
			    <td colspan="3">嬰幼兒有其他兄弟姊妹曾/現入托本托兒所？ <u>'.($Lang['Admission'][$othersInfo['CurBSName']]).'</u>
				</td>
			  </tr>
			</table>
			<table align="center" class="tg-table-plain">
			<tr>
				<td colspan="3" style="border-bottom:2px #555 solid" class="print_field_title_main">
					<b>擬入托的組別 (出生時段)</b>
				</td>
			</tr>
			<tr>
				<td>
					<span class="input_content"><u>'.$classLevel[$othersInfo['classLevelID']].'</u></span>
				</td>
				<td>
					'.$dayTypeOption.'
				</td>
			</tr>
			<!--<tr>
				<td>
					嬰幼兒組 1 (3 至 12 個月)(只有全日托)
				</td>
				<td rowspan="3">
					備註:(請以 1,2,3 排列你的優先次序)<br/><br/>
					幼兒組3:(25 至 36 個月)<br/>
					全日托 上午組 下午組
				</td>
			</tr>
			<tr>
				<td>
					幼兒組 2 (13 至 24 個月)(暫時只有全日托)
				</td>
			
			</tr>
			<tr>
				<td>
					幼兒組 3 (25 至 36 個月)(提供全日托及半日托)
				</td>
			</tr>-->
			</table>
			<table align="center" class="tg-table-plain">
			<tr>
			    <td colspan="4" style="border-bottom:2px #555 solid" class="print_field_title_main"><b>申請人資料 (必須與嬰幼兒同住之父或母，如嬰幼兒不是與父/母同住，才填寫監護人資料) </b><div style="float:right">請於合適的<input type="checkbox" onclick="return false"/>上加上「√」號</div></td>
			 </tr> 
			<tr>
			    <td style="border-right:2px #555 solid"><span style="background:#EFEFEF;">申請人與嬰幼兒的關係</span></td>
			    <td><span style="background:#EFEFEF;"><input type="checkbox" onclick="return false" '.($fatherInfo['relationship'] == 'F'?'checked':'').' /> 父親</span></td>
				<td><span style="background:#EFEFEF;"><input type="checkbox" onclick="return false" '.($motherInfo['relationship'] == 'M'?'checked':'').' /> 母親</span></td>
				<td><span style="background:#EFEFEF;"><input type="checkbox" onclick="return false" '.($fatherInfo['relationship'] != 'F' && $fatherInfo['relationship'] != 'M'?'checked':'').' /> 監護人 <u>'.$guardianInfo['relationship'].'</u></span></td>
			  </tr>
			<tr>
			    <td style="border-right:2px #555 solid">父母/監護人外文姓名:</td>
			    <td><span class="input_content">'.$fatherInfo['parent_name_en'].'</span></td>
				<td><span class="input_content">'.$motherInfo['parent_name_en'].'</span></td>
				<td><span class="input_content">'.$guardianInfo['parent_name_en'].'</span> (如適用)</td>
			  </tr>
			<tr>
			    <td style="border-right:2px #555 solid">父母/監護人中文姓名:</td>
			    <td class="input_content"><span class="input_content">'.$fatherInfo['parent_name_b5'].'</span></td>
				<td class="input_content"><span class="input_content">'.$motherInfo['parent_name_b5'].'</span></td>
				<td class="input_content"><span class="input_content">'.$guardianInfo['parent_name_b5'].'</span> (如適用)</td>
			  </tr>
			<tr>
			    <td style="border-right:2px #555 solid">父母是否與嬰幼兒同住:</td>
			    <td class="input_content"><span class="input_content">'.($fatherInfo['IsLiveWithChild']=="Y"?$Lang['Admission']['yes']:($fatherInfo['IsLiveWithChild']?$Lang['Admission']['no']:'')).'</span></td>
				<td class="input_content"><span class="input_content">'.($motherInfo['IsLiveWithChild']=="Y"?$Lang['Admission']['yes']:($motherInfo['IsLiveWithChild']?$Lang['Admission']['no']:'')).'</span></td>
				<td class="input_content"><span class="input_content">--<!--'.($guardianInfo['IsLiveWithChild']=="Y"?$Lang['Admission']['yes']:($guardianInfo['IsLiveWithChild']?$Lang['Admission']['no']:'')).'</span>--></td>
			  </tr>
			<!--<tr>
			    <td style="border-right:2px #555 solid;border-bottom:2px #555 solid">因家庭有特殊情況未能照顧其嬰幼兒:</td>
			    <td style="border-bottom:2px #555 solid" colspan="3" class="input_content"><span class="input_content">'.($fatherInfo['IsFamilySpecialCase']=="Y"?$Lang['Admission']['yes'].' (如是，是否申請全日托服務？ '.($fatherInfo['IsApplyFullDayCare']=="Y"?$Lang['Admission']['yes']:$Lang['Admission']['no']).' )':$Lang['Admission']['no']).'</span></td>
			</tr>-->
			<tr>
			    <td style="border-right:2px #555 solid">手提電話:</td>
			    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($studentInfo['ContactPersonRelationship'] == 'F'?'checked':'').' />'.$fatherInfo['mobile'].'</span></td>
			    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($studentInfo['ContactPersonRelationship'] == 'M'?'checked':'').' />'.$motherInfo['mobile'].'</span></td>
			    <td><span class="input_content"><input type="checkbox" onclick="return false" '.($studentInfo['ContactPersonRelationship'] == 'G'?'checked':'').' />'.$guardianInfo['mobile'].'</span></td>
			  </tr>
			<tr>
			    <td style="border-right:2px #555 solid">電郵地址:</td>
			    <td colspan="3"><span class="input_content">'.$fatherInfo['email'].'</span></td>
				<!--<td><span class="input_content">'.$motherInfo['email'].'</span></td>
				<td><span class="input_content">'.$guardianInfo['email'].'</span></td>-->
			  </tr>
			<tr>
			    <td style="border-right:2px #555 solid;border-bottom:2px #555 solid">&nbsp;</td>
			    <td colspan="3" style="border-bottom:2px #555 solid">*請選擇一個指定的電郵及手提電話，以便閣下中籤後本托兒所職員能將相關文件傳送給家長。
				</td>
		  	</tr>
			<!--<tr>
			    <td style="border-right:2px #555 solid">嬰幼兒父母屬於單親<br/>人士或有特別情況<br>(必須先清楚細閱<u>'.date('Y',getStartOfAcademicYear('',$lac->getNextSchoolYearID())).'年<br/>度簡章，了解單親人士或<br/>有特別情況之定義</u>):</td>
			    <td colspan="3" class="input_content"><span class="input_content">'.($fatherInfo['lsSingleParents']=="Y"?'<u>'.$Lang['Admission']['yes'].'</u> ( 如是，若此申請在抽籤中中籤，本人同意提供所需証明資料予善明托兒所審核。本人明白並同意中籤不等於可正式入托。善明托兒所擁有最終審核權。 '.($fatherInfo['IsApplyFullDayCare']=="Y"?'<u>'.$Lang['Admission']['agree'].'</u>':'<u>'.$Lang['Admission']['disagree'].'</u>').' )':($fatherInfo['lsSingleParents']=="N"?'<u>'.$Lang['Admission']['no'].'/ '.$Lang['Admission']['na'].'</u>':'<u>'.$Lang['Admission']['na'].'</u>')).'</span></td>
			</tr>-->
			</table>';
			$x .= '</div>';
			return $x;
		}
		else
			return false;
	}
	function getPrintPageCss(){
		return '<style type="text/css">
.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
.tg-table-plain td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
.print_field_title { background: #EFEFEF}
.print_field_title_main { background:#D7D7D7}
.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
.print_field_row1 { width:120px}
.print_field_row2 { width:28px}
.print_field_row3 { width:80px}
.print_field_title_remark { background:#B9B9B9; width:290px;}
.print_field_title_parent{ width:181px;}
@media print
{    
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
</style>';
	}
}
?>