<section id="otherForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['otherInfo'] ?> <?=$LangEn['Admission']['otherInfo'] ?>
	</div>
	<div class="sheet">
		<div class="item">
		<?=$LangB5['Admission']['STCC']['activitiesAndAwards']?> <?=$LangEn['Admission']['STCC']['activitiesAndAwards']?>
			<span class="itemInput">
			<textarea rows="5" name="activitiesAndAwards" id="activitiesAndAwards"><?=$allCustInfo['Activity_And_Award'][0]['Value']?></textarea>
				
			</span><span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['STCC']['activitiesAndAwards'] ?> <?=$LangEn['Admission']['STCC']['religion'] ?></div>
				<div class="dataValue <?=$allCustInfo['Activity_And_Award'][0]['Value'] ? '' : 'dataValue-empty'?>" id="activitiesAndAwardsValue" style="overflow-wrap: break-word;"><?=$allCustInfo['Activity_And_Award'][0]['Value'] ? $allCustInfo['Activity_And_Award'][0]['Value'] : '－－'?></div>
			</span>
		</div>
		
		<div class="item" id="divOtherFormClassLevel">
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['applyLevel'] ?> <?=$LangEn['Admission']['applyLevel'] ?></div>
				<div class="dataValue"><?=$classLevel ?></div>
			</span>
		</div>
		</div>
</section>
	
<section id="siblingForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">		
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['STCC']['siblingForm'] ?> <?=$LangEn['Admission']['STCC']['siblingForm'] ?>
	</div>
	<div class="sheet">
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="RelativeStudent" id="RelativeStudent" value="<?=$relativeName ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['STCC']['RelativeAttending'] ?> <?=$LangEn['Admission']['STCC']['RelativeAttending'] ?></div>
					<div class="remark remark-warn hide" id="errRelativeStudent"></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['STCC']['RelativeAttending'] ?> <?=$LangEn['Admission']['STCC']['RelativeAttending'] ?></div>
				<div class="dataValue <?=$relativeName ? '' : 'dataValue-empty'?>"><?=$relativeName ? $relativeName : '－－' ?></div>
			</span>
		</div>

		<div class="item">
		<span class="itemInput">
			<span class="selector">
					<select id="RelativeClassAttend" name="RelativeClassAttend" class="classAttend">
						<option value="" hidden><?=$LangB5['Admission']['STCC']['ClassAttending'] ?> <?=$LangEn['Admission']['STCC']['ClassAttending'] ?></option>
						<?php for($i=1;$i<sizeof($class);$i++): ?>
							<option value="<?=$class[$i]?>" <?=$relativeClass == $class[$i] ? 'selected' : ''?>><?= $class[$i] ?></option>
						<?php endfor; ?>
					</select>
					<div class="remark remark-warn hide" id="errRelativeClassAttend"></div>
			</span>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['STCC']['ClassAttending'] ?> <?=$LangEn['Admission']['STCC']['ClassAttending'] ?></div>
				<div class="dataValue <?=$relativeClass ? '' : 'dataValue-empty'?>"><?=$relativeClass ? $relativeClass : '－－' ?></div>
			</span>
		</div>
	</div>
</section>	

<section id="sourceForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['STCC']['Source'] ?> <?=$LangEn['Admission']['STCC']['Source'] ?>
	</div>

	<div class="sheet">
		<div class="item">
				<span id="checkSource"class="itemInput itemInput-choice">
				<div class="textboxLabel"><?=$LangB5['Admission']['STCC']['Source'] ?> <?=$LangEn['Admission']['STCC']['Source'] ?>*</div><br><br>
				<?php 
				for($k = 0; $k < sizeof($sourceInfo); $k++){?>
				<?php 
				if($IsUpdate){
    				for($j = 0; $j <= sizeof($sourceRecord); $j++){
    				    if($sourceRecord[$j] == $sourceInfo[$k]["name"]){
    				        $checked = "checked";
    				        break;
    				    }else{
    				        $checked = "";
    				    }
    				}
				}
				?>
					<input type="checkbox" name="Source[]" id="Source[<?=$k?>]" value="<?=$sourceInfo[$k]["num"]?>" <?=$checked?>/>
					<label for="Source[<?=$k?>]" >	<?=$sourceInfo[$k]["letter"]?> <?=$sourceInfo[$k]["name"]?> </label> <br>
					<input type="hidden" name="SourceName[]" id="SourceName[<?=$k?>]" value="<?=$sourceInfo[$k]["name"]?>" />
				<?php } ?>
				<div class="textbox-floatlabel">					
					<div class="remark remark-warn hide" id="errorSource"><?=$LangB5['Admission']['required'] ?> <?=$LangEn['Admission']['required'] ?></div>
				</div>
				</span>
			<span class="itemData">

				<div class="dataLabel"><?=$LangB5['Admission']['STCC']['Source'] ?> <?=$LangEn['Admission']['STCC']['Source'] ?></div>
				<div class="dataValue <?=$source ? '' : 'dataValue-empty'?>" id="sourceValue"><?=$source ? $source : '－－' ?></div>
			</span>
		</div>
		<div class="remark">* <?=$LangB5['Admission']['requiredFields'] ?> <?=$LangEn['Admission']['requiredFields'] ?></div>
	</div>
</section>

<?if($lac->isInternalUse($_GET['token']) && !$IsUpdate){?>
<section id="feeForm" class="form displaySection display_pagePersonalInfo display_pageConfirmation">
	<div class="form-header marginB10">
		<?=$LangB5['Admission']['applicationfee'] ?> <?=$LangEn['Admission']['applicationfee'] ?>
	</div>
	<div class="sheet">

		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="BankName" id="BankName" value="<?=$StatusInfo[0]['FeeBankName'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['KTLMSKG']['bankName'] ?> <?=$LangEn['Admission']['KTLMSKG']['bankName'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['KTLMSKG']['bankName'] ?> <?=$LangEn['Admission']['KTLMSKG']['bankName'] ?></div>
				<div class="dataValue <?=$StatusInfo[0]['FeeBankName'] ? '' : 'dataValue-empty'?>"><?=$StatusInfo[0]['FeeBankName'] ? $StatusInfo[0]['FeeBankName'] : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<span class="itemInput">
				<div class="textbox-floatlabel">
					<input type="text" name="ChequeNo" id="ChequeNo" value="<?=$StatusInfo[0]['FeeChequeNo'] ?>">
					<div class="textboxLabel"><?=$LangB5['Admission']['KTLMSKG']['chequeNum'] ?> <?=$LangEn['Admission']['KTLMSKG']['chequeNum'] ?></div>
				</div>
			</span>
			<span class="itemData">
				<div class="dataLabel"><?=$LangB5['Admission']['KTLMSKG']['chequeNum'] ?> <?=$LangEn['Admission']['KTLMSKG']['chequeNum'] ?></div>
				<div class="dataValue <?=$StatusInfo[0]['FeeChequeNo'] ? '' : 'dataValue-empty'?>"><?=$StatusInfo[0]['FeeChequeNo'] ? $StatusInfo[0]['FeeBankName'] : '－－' ?></div>
			</span>
		</div>
		
		<div class="item">
			<div class="itemLabel"><?=$LangB5['Admission']['KTLMSKG']['Payed'] ?> <?=$LangEn['Admission']['KTLMSKG']['Payed'] ?></div>
			<span class="itemInput itemInput-choice">
				<span>
					<input type="radio" id="Payed_1" name="Payed" value="1" <?=$feestatus?'checked':'' ?>>
					<label for="Payed_1"><?=$LangB5['Admission']['yes'] ?> <?=$LangEn['Admission']['yes'] ?></label>
				</span>
				<span>
					<input type="radio" id="Payed_0" name="Payed" value="0" <?=!$feestatus?'checked':'' ?>>
					<label for="Payed_0"><?=$LangB5['Admission']['no'] ?> <?=$LangEn['Admission']['no'] ?></label>
				</span>
			</span>
			<div class="itemData">
				<div class="dataValue"><?=$feestatus ?$LangB5['Admission']['yes'].' '.$LangEn['Admission']['yes']:$LangB5['Admission']['no'].' '.$LangEn['Admission']['no']?></div>
			</div>
		</div>
		
	</div>
</section>
<?}?>

<script>
$(function(){
	'use strict';
	
	$('#otherForm .itemInput, #siblingForm .itemInput, #sourceForm .itemInput, #feeForm .itemInput').each(function(){
		var $itemInput = $(this),
			$inputText = $itemInput.find('input[type="text"]'),
			$inputRadio = $itemInput.find('input[type="radio"]'),
			$inputDateSelect = $itemInput.find('select.dateYear, select.dateMonth, select.dateDay'),
			$inputDateYearSelect = $itemInput.find('select.dateYear'),
			$inputDateMonthSelect = $itemInput.find('select.dateMonth'),
			$inputDateDaySelect = $itemInput.find('select.dateDay'),
			$inputClassAttend = $itemInput.find('select.classAttend'),
			$dataValue = $itemInput.next('.itemData').find('.dataValue');

		$inputText.on('change', function(e){
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html($(this).val());
			$($dataValue.get($(this).parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
		
		$inputRadio.on('change', function(e){
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html($(this).next('label').html());
			$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).removeClass('dataValue-empty');
			if(!$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html()){
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).html('－－');
				$($dataValue.get($(this).parent().parent().parent('.textbox-floatlabel').index())).addClass('dataValue-empty');
			}
		});
			
		$inputDateSelect.on('change', function(e){
			$dataValue.html($inputDateYearSelect.val()+'-'+$inputDateMonthSelect.val()+'-'+$inputDateDaySelect.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputDateYearSelect.val() || !$inputDateMonthSelect.val() || !$inputDateDaySelect.val()){
				$dataValue.html('－－');
				$dataValue.addClass('dataValue-empty');
			}
		});

		$inputClassAttend.on('change', function(e){
			$dataValue.html($inputClassAttend.val());
			$dataValue.removeClass('dataValue-empty');
			if(!$inputClassAttend.val()){
    			$dataValue.html('－－');
    			$dataValue.addClass('dataValue-empty');
			}
		});
		
		
	});

	$('#activitiesAndAwards').change(function(){
		$('#activitiesAndAwardsValue').html($('#activitiesAndAwards').val());
		$('#activitiesAndAwardsValue').removeClass('dataValue-empty');
	});
	
	window.checkOtherForm = (function(lifecycle){
		var isValid = true;
		var $otherForm = $('#otherForm');

		/******** Basic init START ********/
		//for mm/yy validation
		var re = /^(0[123456789]|10|11|12)\/\d{2}$/;
		/******** Basic init END ********/
		
		/**** Check required START ****/
		isValid = isValid && checkInputRequired($otherForm);
		
		if($('#RelativeStudent').val().trim()!=''){
			if($('#RelativeClassAttend').val().trim()==''){
				if($('#RelativeClassAttend').parent().find('.remark-warn').hasClass('hide')){
					$('#errRelativeClassAttend').html('<?=$LangB5['Admission']['UCCKE']['msg']['enterClassAttend']?> <?=$LangEn['Admission']['UCCKE']['msg']['enterClassAttend']?>');
					$('#errRelativeClassAttend').removeClass('hide');
					focusElement = $('#RelativeClassAttend');
				}	
		    	isValid = false;
			}
		}
		
		if(($('#RelativeClassAttend').val().trim()!='') && $('#RelativeStudent').val().trim()=='' ){
			if($('#RelativeStudent').parent().find('.remark-warn').hasClass('hide')){
				$('#errRelativeStudent').html("<?=$LangB5['Admission']['UCCKE']['msg']['enterRelativeName']?> <?=$LangEn['Admission']['UCCKE']['msg']['enterRelativeName']?>");
				$('#errRelativeStudent').removeClass('hide');
				focusElement = $('#RelativeStudent');
			}	
	    	isValid = false;
		}
		
		$('#errorSource').addClass('hide');
		if(!$('#checkSource').hasClass('notEmpty')){
    			$('#errorSource').removeClass('hide');
    			$("#Source\\[0\\]").focus();
    			isValid = false;
		}else{
			$("#sourceValue").removeClass('dataValue-empty');
			var sourceArray = new Array();
			var sourceName;
			for(var i = 0; i <= <?=sizeof($sourceInfo)?>; i++){
				if($("#Source\\["+i+"\\]").is(":checked")){
					sourceArray.push("<p>"+$("#SourceName\\["+i+"\\]").val()+"</p>");	
				}
			}

			sourceName = sourceArray.join(" ");
			$("#sourceValue").html(sourceName);
		}
		
		/**** Check required END ****/

		if(focusElement){
			focusElement.focus();
		}
		
		return isValid;
	});

	window.validateFunc['pagePersonalInfo'].push(checkOtherForm);
});
</script>