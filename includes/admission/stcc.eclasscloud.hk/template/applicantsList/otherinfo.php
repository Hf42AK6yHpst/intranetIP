<?php

global $libkis_admission;

#### Get Student Cust Info START ####
$applicationInfo['studentCustInfo'] = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
#### Get Student Cust Info START ####


#### Get Relatives Info START ####
$relativesInfo = $libkis_admission->getApplicationRelativesInfo($schoolYearID,'',$applicationInfo['applicationID'],'');
$otherInfoCust = $libkis_admission->getAllApplicationCustInfo($applicationInfo['applicationID']);
$knowUsBy = $libkis_admission->getKnowUsBy($applicationInfo['applicationID']);

#### Get Relatives Info END ####


?>
<table class="form_table">
	<colgroup>
		<col width="30%">
		<col width="20%">
		<col width="30%">
		<col width="20%">
	</colgroup>
	<tbody>                              
		<!--tr> 
			<td width="30%" class="field_title"><?=$kis_lang['form']?></td>
			<td width="70%"><?=kis_ui::displayTableField($classLevelAry[$applicationInfo['classLevelID']])?></td>
		</tr-->
		
<!-------------- Student Other Info START -------------->
<!--tr>
	<td class="field_title">
		<?=$kis_lang['Admission']['KTLMSKG']['CurrentStudySchool']?> (<?=$kis_lang['Admission']['class']?>)
	</td>
	<td>
		<span><?=kis_ui::displayTableField($currentSchoolInfo['OthersPrevSchName'])?> (<?=kis_ui::displayTableField($currentSchoolInfo['OthersPrevSchClass'])?>)</span>
	</td>
</tr-->
<!-------------- Student Other Info END -------------->


<!-------------- Siblings START -------------->
		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['STCC']['activitiesAndAwardsForm']  ?> 
			</td>
			<td>
				<?=kis_ui::displayTableField($otherInfoCust['Activity_And_Award'][0]['Value'])?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['STCC']['RelativeAttending'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($relativesInfo[0]['Name'])?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="field_title">
				<?= $kis_lang['Admission']['UCCKE']['ClassAttending'] ?>
			</td>
			<td>
				<?=kis_ui::displayTableField($relativesInfo[0]['ClassPosition'])?>
			</td>
		</tr>

		<tr>
			<td class="field_title">
				<?=$kis_lang['Admission']['STCC']['Source']  ?> 
			</td>
			<td>
				<?php 
    				if(sizeof($knowUsBy) > 0){
    				    for($i=0; $i <= sizeof($knowUsBy); $i++){
    				        $source .= "<p>".$knowUsBy[$i]."</p>";
    				    }
    				}
				?>
				<?=kis_ui::displayTableField($source)?>
			</td>
		</tr>
<!-------------- Siblings END -------------->

</tbody>
</table>
