<?php
	//using:
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

	$admission_cfg['PrintByPDF'] = 1;

	//Please also define lang in admission_lang
	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['STCC_pendingforpayment']	= 1;
	$admission_cfg['Status']['paymentsettled']	= 2;
	$admission_cfg['Status']['STCC_pending']	= 3;
	$admission_cfg['Status']['STCC_rejected']	= 11;
	$admission_cfg['Status']['STCC_firstroundannouncement']	= 6;
	$admission_cfg['Status']['STCC_secondroundannouncement']	= 7;
	$admission_cfg['Status']['STCC_reserve'] = 10;
	$admission_cfg['Status']['STCC_confirmed']	= 4;
	$admission_cfg['Status']['STCC_pendingforregistration']	= 8;
	$admission_cfg['Status']['STCC_registered'] = 9;
	$admission_cfg['Status']['cancelled']	= 5;
	
	$admission_cfg['StatusDisplayOnTable'][1] = 'STCC_pendingforpayment';
	$admission_cfg['StatusDisplayOnTable'][2] = 'paymentsettled';
	$admission_cfg['StatusDisplayOnTable'][3] = 'STCC_confirmed';

	$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
	$admission_cfg['PaymentStatus']['OtherPayment']	= 2;

//	$admission_cfg['Status'] = array();
//	$admission_cfg['Status']['waitingforinterview']	= 1;
//	$admission_cfg['Status']['interviewed']	= 2;
//	$admission_cfg['Status']['admitted']	= 3;
//	$admission_cfg['Status']['notadmitted']	= 4;
//	$admission_cfg['Status']['reservedstudent']	= 5;

//	$admission_cfg['BirthCertType'] = array();
//	$admission_cfg['BirthCertType']['hk']	= 1;
//	$admission_cfg['BirthCertType']['oversea']	= 2;
//	$admission_cfg['BirthCertType']['mainland']	= 3;
//	$admission_cfg['BirthCertType']['others']	= 4;
//
//	$admission_cfg['KnowUsBy']	= array(); //desc = true to show textbox
//	$admission_cfg['KnowUsBy']['mailleaflet']	= array('index'=>1,'desc'=>false);
//	$admission_cfg['KnowUsBy']['newspaper']		= array('index'=>2,'desc'=>false);
//	$admission_cfg['KnowUsBy']['introduced']	= array('index'=>3,'desc'=>false);
//	$admission_cfg['KnowUsBy']['ourwebsite']	= array('index'=>4,'desc'=>false);
//	$admission_cfg['KnowUsBy']['otherwebsite']	= array('index'=>5,'desc'=>true);
//	$admission_cfg['KnowUsBy']['advertisement']	= array('index'=>6,'desc'=>true);
//	$admission_cfg['KnowUsBy']['others']		= array('index'=>7,'desc'=>true);

	$admission_cfg['Class'] = array("", "1Love", "1Faith", "1Honesty", "1Hope", "2Love", "2Faith", "2Honesty", "2Hope",
	                        "3Love", "3Faith", "3Honesty", "3Hope","4Love", "4Faith", "4Honesty", "4Hope",
	                        "5Love", "5Faith", "5Honesty", "5Hope","6Love", "6Faith", "6Honesty", "6Hope"
	);
	
	$admission_cfg['KnowUsBy'] = array();
	$admission_cfg['KnowUsBy']['0'] = array("num" =>"A", "letter" =>"a.", "name" => "Newspaper / Magazine 報紙/雜誌");
	$admission_cfg['KnowUsBy']['1'] = array("num" =>"B", "letter" =>"b.", "name" =>"Education / Oversea Study 教育展覽");
	$admission_cfg['KnowUsBy']['2'] = array("num" =>"C", "letter" =>"c.", "name" =>"Internet 互聯網");
	$admission_cfg['KnowUsBy']['3'] = array("num" =>"D", "letter" =>"d.", "name" =>"Email 電郵");
	$admission_cfg['KnowUsBy']['4'] = array("num" =>"E", "letter" =>"e.", "name" =>"Outdoor advertisement 戶外廣告");
	$admission_cfg['KnowUsBy']['5'] = array("num" =>"F", "letter" =>"f.", "name" =>"Recruitment magazine 招聘雜誌");
	$admission_cfg['KnowUsBy']['6'] = array("num" =>"G", "letter" =>"g.", "name" =>"Relatives / Friends 親戚 / 朋友");
	$admission_cfg['KnowUsBy']['7'] = array("num" =>"H", "letter" =>"h.", "name" =>"Others 其他");
	
	// ####### Cust config START ########
	$admission_cfg['SchoolName']['b5'] = '基督教中國佈道會 聖道迦南書院';
	$admission_cfg['SchoolName']['en'] = 'ECF Saint Too Canaan College';

	$admission_cfg['themeStyle'] = 'Secondary';
	// ####### Cust config END ########

//	/* for paypal testing [start] */
//	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
////	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
//	$admission_cfg['paypal_signature'] = '3bG6NqSV1kcyFzTo-JaVohhxIdBgV0PhCYcJRBfa9riwMwVasUOirv_epIS';
////	$admission_cfg['item_name'] = 'Application Fee';
////	$admission_cfg['item_number'] = 'AF2015';
////	$admission_cfg['amount'] = '40.00';
////	$admission_cfg['currency_code'] = 'HKD';
//	$admission_cfg['hosted_button_id'] = 'UMUTAHK69BH8W';
//	$admission_cfg['paypal_name'] = 'BroadLearning Education (Asia) Ltd. (Testing)';
//	/* for paypal testing [End] */

	/* for real paypal use [start] */
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = 'yclai-facilitator@munsang.edu.hk';
	$admission_cfg['paypal_signature'] = 'QIxlU-tY_rF_0Mydpvz78jeb3P7gJJQeP_Swoz-mn68PbLeFkb1bBQHz0by';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = 'AF2015';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = 'S2MGC6ZVRBUPJ';
	$admission_cfg['paypal_name'] = 'BroadLearning Education (Asia) Ltd.';
	/* for real paypal use [End] */

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['IsBilingual'] = true;
	$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	if($plugin['eAdmission_devMode']){
	    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
	    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}