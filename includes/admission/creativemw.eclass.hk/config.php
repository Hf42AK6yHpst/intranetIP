<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(馬灣)';
$admission_cfg['SchoolName']['en'] .= ' (Ma Wan)';
$admission_cfg['SchoolCode'] = 'MW';
$admission_cfg['SchoolPhone'] = '2940 9228';
$admission_cfg['SchoolAddress']['b5'] = '新界馬灣珀麗路8號';
$admission_cfg['SchoolAddress']['en'] = '8 Pak Lai Road, Ma Wan, New Territories';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckmw.photo@gmail.com';
}
/* for email [end] */

/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = 'ZCC8XJJVJPNQA';
} else {
    $admission_cfg['paypal_signature'] = 'f_KVLTqyaiR9_mKnf1m6CUR2yy_i0P8Mhbc1cKl24Lfkk1Hz8tV9znIO0em';
    $admission_cfg['hosted_button_id'] = 'ETQRXNSS7V8B8';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Ma Wan)';
}
/* for paypal [End] */