<?php

class admission_briefing extends admission_briefing_base{
	
	public function sendMailToNewApplicant($Briefing_ApplicantID, $subject = '', $message = '', $email = ''){
		if($Briefing_ApplicantID == 0){
			return parent::sendMailToNewApplicant($Briefing_ApplicantID, $subject, $message, $email);
		}
		
		$applicant = $this->getBriefingApplicant($Briefing_ApplicantID);
		$ApplicantID = $applicant['ApplicantID'];
		$deleteRecordLink = $this->getApplicantDeleteLink($Briefing_ApplicantID, $applicant['DeleteRecordPassKey']);
		$BriefingID = $applicant['BriefingID'];
		$briefingSession = $this->getBriefingSession($BriefingID);
		
		#### Email Date START ####
		$_starttime = strtotime($briefingSession['BriefingStartDate']);
		$_endtime = strtotime($briefingSession['BriefingEndDate']);
		$_endDay = date('h:ia', $_endtime);
		## Chinese START ##
		$weekDayArr = array('日','一','二','三','四','五','六');
		$_weekDay = $weekDayArr[ date('w', $_starttime) ];
		$BriefingDateChi = date('d/m (%1) h:i-%2', $_starttime);
		$BriefingDateChi = str_replace('%1', $_weekDay, $BriefingDateChi);
		$BriefingDateChi = str_replace('%2', $_endDay, $BriefingDateChi);
		## Chinese END ##
		
		## English START ##
		$BriefingDateEng = date('d/m (l) h:i-%2', $_starttime);
		$BriefingDateEng = str_replace('%2', $_endDay, $BriefingDateEng);
		
		## English END ##
		#### Email Date END ####
		
		@ob_start();
?>
<pre>
<font color="green">申請完成。請記錄你的申請編號 <font size="5"><u><?=$ApplicantID?></u></font></font>

多謝家長報名參加本園簡介會，請注意下列事項
<ol>
<li>日期及時間： <?=$BriefingDateChi?> <?=$briefingSession['Title']?> </li>
<li>地點：荃灣浸信會幼稚園二樓禮堂（荃灣青山道99-113號）</li>
<li>簡介會為家長而設，恕不接待孩子，敬請見諒！</li>
<li>報名後恕不更改場次</li>
<li>請列印此確認信準時出席</li>
<li>如天文台於會前二小時懸掛（黑色暴雨警告、八號或以上颱風）訊號，簡介會將會延期，有關日期，請留意學校網頁。</li>
<li>如有查詢，請致電2413 3819</li>
</ol>
如需要刪除申請，請按此連結： <?=$deleteRecordLink?> 
</pre>
<br />
<pre>
<font color="green">Application is Completed.Your application number is <font size="5"><u><?=$ApplicantID?></u></font></font>

Thank you for enrolling our briefing sessions, please note:
<ol>
<li>Date and time: <?=$BriefingDateEng?> <?=$briefingSession['Title']?> </li>
<li>Venue: Hall, 2/F, Tsuen Wan Baptist Church Kindergarten ( 99-113 Castle Peak Road; Tsuen Wan.)</li>
<li>Briefing sessions are set for parents, we do not have baby-sit services. Please do not bring along with your children.</li>
<li>No change of sessions after confirming your enrollment. Be punctual please.</li>
<li>Please print out and bring along with your confirmation letter when attending.</li>
<li>In case no.8 typhoon signal or Black Rainstorm signal is hoisted within 2 hours of the starting time of a session will be postponed .New schedule will be posted in our kindergarten website.</li>
<li>Any enquiry needed please contact 2413 3819</li>
</ol>
If you want to cancel the application, click this link: <?=$deleteRecordLink?>
</pre>
<?php	
		$content = ob_get_clean();
		return parent::sendMailToNewApplicant($Briefing_ApplicantID, $subject, $message = $content, $email);
	}
	
} // End Class