<h1>
<?=$LangB5['Admission']['PGInfo']?>
Parent Information
</h1>
<table class="form_table parentInfo" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="width:23%">
        <col style="width:23%">
        <col style="width:24%">
    </colgroup>
    <tr>
    	<td>
    		<span>If no information, e.g. single parent family, fill in '<font style="color:red;">N.A.</font>'</span>
    		<br/>
    		<span>如沒有資料，例如單親家庭，請輸入 '<font style="color:red;">N.A.</font>'</span>
    	</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td class="form_guardian_head"><center><?="{$LangB5['Admission']['PG_Type']['F']} {$LangEn['Admission']['PG_Type']['F'] }"?></center></td>
		<td class="form_guardian_head"><center><?="{$LangB5['Admission']['PG_Type']['M']} {$LangEn['Admission']['PG_Type']['M'] }"?></center></td>
		<td class="form_guardian_head"><center><?="{$LangB5['Admission']['PG_Type']['G']} {$LangEn['Admission']['PG_Type']['G'] }"?></center></td>
	</tr>
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$LangB5['Admission']['chinesename']?> 
    		<?=$LangEn['Admission']['chinesename']?> 
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1ChineseName'];
    		}else{?>
    			<input name="G1ChineseName" type="text" id="G1ChineseName" class="textboxtext" value="<?=$parentInfoArr['F']['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2ChineseName'];
    		}else{?>
    			<input name="G2ChineseName" type="text" id="G2ChineseName" class="textboxtext" value="<?=$parentInfoArr['M']['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3ChineseName'];
    		}else{?>
    			<input name="G3ChineseName" type="text" id="G3ChineseName" class="textboxtext" value="<?=$parentInfoArr['G']['ChineseName'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
    		<?=$star ?>
    		<?=$LangB5['Admission']['englishname']?>
    		<?=$LangEn['Admission']['englishname']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1EnglishName'];
    		}else{?>
    			<input name="G1EnglishName" type="text" id="G1EnglishName" class="textboxtext" value="<?=$parentInfoArr['F']['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2EnglishName'];
    		}else{?>
    			<input name="G2EnglishName" type="text" id="G2EnglishName" class="textboxtext" value="<?=$parentInfoArr['M']['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3EnglishName'];
    		}else{?>
    			<input name="G3EnglishName" type="text" id="G3EnglishName" class="textboxtext" value="<?=$parentInfoArr['G']['EnglishName'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$LangB5['Admission']['phoneno']?>
    		<?=$LangEn['Admission']['phoneno']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Mobile'];
    		}else{?>
    			<input name="G1Mobile" type="text" id="G1Mobile" class="textboxtext" value="<?=$parentInfoArr['F']['Mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Mobile'];
    		}else{?>
    			<input name="G2Mobile" type="text" id="G2Mobile" class="textboxtext" value="<?=$parentInfoArr['M']['Mobile'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3Mobile'];
    		}else{?>
    			<input name="G3Mobile" type="text" id="G3Mobile" class="textboxtext" value="<?=$parentInfoArr['G']['Mobile'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$LangB5['Admission']['AOGKG']['nameOfOffice']?>
    		<?=$LangEn['Admission']['AOGKG']['nameOfOffice']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1Company'];
    		}else{?>
    			<input name="G1Company" type="text" id="G1Company" class="textboxtext" value="<?=$parentInfoArr['F']['Company'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2Company'];
    		}else{?>
    			<input name="G2Company" type="text" id="G2Company" class="textboxtext" value="<?=$parentInfoArr['M']['Company'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3Company'];
    		}else{?>
    			<input name="G3Company" type="text" id="G3Company" class="textboxtext" value="<?=$parentInfoArr['G']['Company'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
	
	<tr>
		<td class="field_title">
   			<?=$star ?>
    		<?=$LangB5['Admission']['AOGKG']['position']?>
    		<?=$LangEn['Admission']['AOGKG']['position']?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G1JobPosition'];
    		}else{?>
    			<input name="G1JobPosition" type="text" id="G1JobPosition" class="textboxtext" value="<?=$parentInfoArr['F']['JobPosition'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G2JobPosition'];
    		}else{?>
    			<input name="G2JobPosition" type="text" id="G2JobPosition" class="textboxtext" value="<?=$parentInfoArr['M']['JobPosition'] ?>"/>
    		<?php } ?>
		</td>
		<td class="form_guardian_field">
    		<?php if($IsConfirm){
    		    echo $formData['G3JobPosition'];
    		}else{?>
    			<input name="G3JobPosition" type="text" id="G3JobPosition" class="textboxtext" value="<?=$parentInfoArr['G']['JobPosition'] ?>"/>
    		<?php } ?>
		</td>
	</tr>
	
</table>
