<style>
.otherDiv{
    margin: 5px 0;
}
</style>
<h1>
    <?=$LangB5['Admission']['otherInfo']?>
    <?=$LangEn['Admission']['otherInfo']?>
</h1>


<table class="form_table otherInformation" style="font-size: 13px">
    <colgroup>
        <col style="width:30%">
        <col style="">
    </colgroup>

<tbody>
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$LangB5['Admission']['AOGKG']['currentStudySibiling']?>
    		<br />
    		<?=$LangEn['Admission']['AOGKG']['currentStudySibiling']?>
    	</td>
    	<td colspan="5">
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_Name">
    				<?=$LangB5['Admission']['name'] ?>
    				<?=$LangEn['Admission']['name'] ?>: 
    			</label>
        		<?php if($IsConfirm){ ?>
        			<?=$formData['CurrentStudySibiling_Name'] ?>
        		<?php }else{ ?>
        			<input 
            			id="CurrentStudySibiling_Name" 
            			name="CurrentStudySibiling_Name" 
            			class="textboxtext" 
            			style="width: 200px;"
            			value="<?=$allCustInfo['CurrentStudySibiling_Name'][0]['Value'] ?>" 
        			/>
    			<?php } ?>
    		</div>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_Class">
    				<?=$LangB5['Admission']['class'] ?>
    				<?=$LangEn['Admission']['class'] ?>: 
    			</label>
        		<?php if($IsConfirm){ ?>
        			<?=$formData['CurrentStudySibiling_Class'] ?>
        		<?php }else{ ?>
        			<input 
            			id="CurrentStudySibiling_Class" 
            			name="CurrentStudySibiling_Class" 
            			class="textboxtext" 
            			style="width: 200px;"
            			value="<?=$allCustInfo['CurrentStudySibiling_Class'][0]['Value'] ?>" 
        			/>
    			<?php } ?>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$LangB5['Admission']['AOGKG']['alunmusSibiling']?>
    		<br />
    		<?=$LangEn['Admission']['AOGKG']['alunmusSibiling']?>
    	</td>
    	<td colspan="5">
    		<div class="otherDiv">
    			<label for="AlunmusSibiling_Name">
    				<?=$LangB5['Admission']['name'] ?>
    				<?=$LangEn['Admission']['name'] ?>: 
    			</label>
        		<?php if($IsConfirm){ ?>
        			<?=$formData['AlunmusSibiling_Name'] ?>
        		<?php }else{ ?>
        			<input 
            			id="AlunmusSibiling_Name" 
            			name="AlunmusSibiling_Name" 
            			class="textboxtext" 
            			style="width: 200px;"
            			value="<?=$allCustInfo['AlunmusSibiling_Name'][0]['Value'] ?>" 
        			/>
    			<?php } ?>
    		</div>
    		<div class="otherDiv">
    			<label for="CurrentStudySibiling_GraduationYear">
    				<?=$LangB5['Admission']['GradYear'] ?>
    				<?=$LangEn['Admission']['GradYear'] ?>: 
    			</label>
        		<?php if($IsConfirm){ ?>
        			<?=$formData['CurrentStudySibiling_GraduationYear'] ?>
        		<?php }else{ ?>
        			<input 
            			id="CurrentStudySibiling_GraduationYear" 
            			name="CurrentStudySibiling_GraduationYear" 
            			class="textboxtext" 
            			style="width: 200px;"
            			value="<?=$allCustInfo['CurrentStudySibiling_GraduationYear'][0]['Value'] ?>" 
        			/>
    			<?php } ?>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$LangB5['Admission']['AOGKG']['memberOfFanling']?>
    		<br />
    		<?=$LangEn['Admission']['AOGKG']['memberOfFanling']?>
    	</td>
    	<td colspan="5">
    		<div class="otherDiv">
    			<label for="MemberOfFanling_Name">
    				<?=$LangB5['Admission']['name'] ?>
    				<?=$LangEn['Admission']['name'] ?>: 
    			</label>
        		<?php if($IsConfirm){ ?>
        			<?=$formData['MemberOfFanling_Name'] ?>
        		<?php }else{ ?>
        			<input 
            			id="MemberOfFanling_Name" 
            			name="MemberOfFanling_Name" 
            			class="textboxtext" 
            			style="width: 200px;"
            			value="<?=$allCustInfo['MemberOfFanling_Name'][0]['Value'] ?>" 
        			/>
    			<?php } ?>
    			&nbsp;
    			&nbsp;
    			(
    			<label for="MemberOfFanling_MembershipNumber">
    				<?=$LangB5['Admission']['AOGKG']['membershipNumber'] ?>
    				<?=$LangEn['Admission']['AOGKG']['membershipNumber'] ?>: 
    			</label>
        		<?php if($IsConfirm){ ?>
        			<?=$formData['MemberOfFanling_MembershipNumber'] ?>
        		<?php }else{ ?>
        			<input 
            			id="MemberOfFanling_MembershipNumber" 
            			name="MemberOfFanling_MembershipNumber" 
            			class="textboxtext" 
            			style="width: 200px;"
            			value="<?=$allCustInfo['MemberOfFanling_MembershipNumber'][0]['Value'] ?>" 
        			/>
    			<?php } ?>
    			)
    		</div>
    		<div class="otherDiv">
    			<label for="MemberOfFanling_Relationship">
    				<?=$LangB5['Admission']['AOGKG']['membershipReleationship'] ?>
    				<?=$LangEn['Admission']['AOGKG']['membershipReleationship'] ?>: 
    			</label>
        		<?php 
        		if($IsConfirm){ 
        		?>
        			<?=($formData['MemberOfFanling_Relationship'])?$LangB5['Admission']['PG_Type'][$formData['MemberOfFanling_Relationship']]:'--' ?>
        			<?=($formData['MemberOfFanling_Relationship'])?$LangEn['Admission']['PG_Type'][$formData['MemberOfFanling_Relationship']]:'' ?>
        		<?php 
        		}else{ 
        		    $relationshipFChecked = ($allCustInfo['MemberOfFanling_Relationship'][0]['Value'] == 'F')? 'checked' : '';
        		    $relationshipMChecked = ($allCustInfo['MemberOfFanling_Relationship'][0]['Value'] == 'M')? 'checked' : '';
        		?>
        			<input type="radio" id="MemberOfFanling_RelationshipF" name="MemberOfFanling_Relationship" value="F" <?=$relationshipFChecked ?> />
        			<label for="MemberOfFanling_RelationshipF">
        				<?=$LangB5['Admission']['PG_Type']['F'] ?>
        				<?=$LangEn['Admission']['PG_Type']['F'] ?>
        			</label>
        			&nbsp;
        			<input type="radio" id="MemberOfFanling_RelationshipM" name="MemberOfFanling_Relationship" value="M" <?=$relationshipMChecked ?> />
        			<label for="MemberOfFanling_RelationshipM">
        				<?=$LangB5['Admission']['PG_Type']['M'] ?>
        				<?=$LangEn['Admission']['PG_Type']['M'] ?>
        			</label>
    			<?php 
        		} 
        		?>
    		</div>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$LangB5['Admission']['AOGKG']['twinApply']?>
    		<?=$LangEn['Admission']['AOGKG']['twinApply']?>
    	</td>
    	<td colspan="5">
			<label for="TwinApply_Name">
				<?=$LangB5['Admission']['name'] ?>
				<?=$LangEn['Admission']['name'] ?>: 
			</label>
    		<?php if($IsConfirm){ ?>
    			<?=$formData['TwinApply_Name'] ?>
    		<?php }else{ ?>
    			<input 
        			id="TwinApply_Name" 
        			name="TwinApply_Name" 
        			class="textboxtext" 
        			style="width: 200px;"
        			value="<?=$allCustInfo['TwinApply_Name'][0]['Value'] ?>" 
    			/>
			<?php } ?>
    	</td>
    </tr>
    
    <tr class="otherQuestionRow">
       	<td class="field_title">
    		<?=$LangB5['Admission']['remarks']?>
    		<?=$LangEn['Admission']['remarks']?>
    	</td>
    	<td colspan="5">
    		<?php if($IsConfirm){ ?>
    			<?=$formData['Remarks'] ?>
    		<?php }else{ ?>
    			<input 
        			id="Remarks" 
        			name="Remarks" 
        			class="textboxtext" 
        			value="<?=$allCustInfo['Remarks'][0]['Value'] ?>" 
    			/>
			<?php } ?>
    	</td>
    </tr>
    
</tbody>

</table>

<script>
$(function(){
	'use strict';
	
	function updateUI(){
		var isNotApplicable = !!$('#partBNotApplicable:checked').length;
		$('.otherQuestionRow input, .otherQuestionRow select').prop('disabled', isNotApplicable);
		if(isNotApplicable){
			$('.otherQuestionRow label').addClass('disabled');
		}else{
			$('.otherQuestionRow label').removeClass('disabled');
		}

		$('.partBQuestion input:checked').each(function(index, element){
			var value = $(this).val();
			var $td = $(this).closest('td');
			var $detailsTd = $td.find('.partBDetails');
			if(value == '1'){
				$detailsTd.show();
				$detailsTd.find('input,select').prop('disabled', isNotApplicable);
			}else{
				$detailsTd.hide();
				$detailsTd.find('input,select').prop('disabled', true);
			}
		});

		if($('[name="SiblingSameSchoolGraduate_Type"]:checked').val() == '1'){
			$('#SiblingSameSchoolGraduate_NameClassDiv').hide().find('input,select').prop('disabled', true);
			$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').show().find('input,select').prop('disabled', isNotApplicable);
		}else{
			$('#SiblingSameSchoolGraduate_NameClassDiv').show().find('input,select').prop('disabled', isNotApplicable);
			$('#SiblingSameSchoolGraduate_NameYearOfGraduationDiv').hide().find('input,select').prop('disabled', true);
		}
	}
	$('#partBNotApplicable, .partBQuestion input').click(updateUI);
	$('[name="SiblingSameSchoolGraduate_Type"]').change(updateUI);
	updateUI();

});
</script>