<style>
select:disabled{
    color: #ccc;
}
textarea{
    height: 100px;
    resize: vertical;
}

.sessionChoice{
    display: inline-block;
    margin-right: 30px;
}
</style>

<h1>
    <?=$LangB5['Admission']['studentInfo']?>
    <?=$LangEn['Admission']['studentInfo']?>
</h1>
<table class="form_table" style="font-size: 13px">
<colgroup>
	<col width="30%">
	<col width="20%">
	<col width="30%">
	<col width="20%">
</colgroup>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$LangB5['Admission']['chinesename']?> 
		<?=$LangEn['Admission']['chinesename']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_b5']?>
		<?php }else{ ?>
			<input name="studentsname_b5" type="text" id="studentsname_b5" class="textboxtext" value="<?=$applicationStudentInfo['student_name_b5']?>"/>
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		<?=$LangB5['Admission']['englishname']?> (<?=$LangB5['Admission']['UCCKE']['SameAsHKID']?>) <br />
		<?=$LangEn['Admission']['englishname']?> (<?=$LangEn['Admission']['UCCKE']['SameAsHKID']?>)
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['studentsname_en']?>
		<?php }else{ ?>
			<input name="studentsname_en" type="text" id="studentsname_en" class="textboxtext"  value="<?=$applicationStudentInfo['student_name_en']?>"/>
		<?php } ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$LangB5['Admission']['dateofbirth']?>
		<?=$LangEn['Admission']['dateofbirth']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=$formData['StudentDateOfBirth']?>
		<?php }else{ ?>
			<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15" value="<?=$applicationStudentInfo['dateofbirth']?>"/>(YYYY-MM-DD)
		<?php } ?>
	</td>
	
	<td class="field_title">
		<?=$star ?>
		<?=$LangB5['Admission']['gender']?>
		<?=$LangEn['Admission']['gender']?>
	</td>
	<td>
		<?php if($IsConfirm){ ?>
			<?=($formData['StudentGender'])? ($LangB5['Admission']['genderType'][$formData['StudentGender']].' '.$formData['StudentGender']) : ' -- ' ?>
		<?php }else{ ?>
			<input type="radio" value="M" id="StudentGender1" name="StudentGender" <?=$applicationStudentInfo['gender'] == "M"?'checked':'' ?>>
			<label for="StudentGender1">M <?=$LangB5['Admission']['genderType']['M']?></label>
			<input type="radio" value="F" id="StudentGender2" name="StudentGender" <?=$applicationStudentInfo['gender'] == "F"?'checked':'' ?>>
			<label for="StudentGender2">F <?=$LangB5['Admission']['genderType']['F']?></label>
		<?php } ?>
	</td>
</tr>


<tr>
	<td class="field_title">
		<?=$star ?>
		<?=$LangB5['Admission']['SHCK']['birthCertNo']?> <font color="blue">(<?=$LangB5['Admission']['SHCK']['birthCertNoHint'] ?>)</font>
		<br />
		<?=$LangEn['Admission']['SHCK']['birthCertNo']?> <font color="blue">(<?=$LangEn['Admission']['SHCK']['birthCertNoHint'] ?>)</font>
	</td>
	<td>
		<?php 
		if($IsConfirm){
		    echo $formData['StudentBirthCertNo'];
		}else{ 
		?>
			<table>
				<tr>
        			<?php if($BirthCertNo){ ?>
        				<?php if($IsUpdate){ ?>
        					<input name="StudentBirthCertNo" value="<?=$BirthCertNo ?>" readonly type="text" id="StudentBirthCertNo" class="textboxtext" style="border: none;width: 250px;"/>
        				<?php }else{ ?>
        					<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$BirthCertNo?>"/>
	            			<br/>
	            			<?=$LangB5['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
	            			<br/>
	            			<?=$LangEn['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
        				<?php } ?>
        			<?php }else{ ?>
            			<input name="StudentBirthCertNo" type="text" id="StudentBirthCertNo" class="textboxtext" maxlength="30" size="30" style="width:150px" value="<?=$applicationStudentInfo['birthcertno'] ?>"/>
            			<br/>
            			<?=$LangB5['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
            			<br/>
            			<?=$LangEn['Admission']['HKUGAPS']['msg']['birthcertnohints'] ?>
        			<?php } ?>
        			</td>
				</tr>
			</table>

		<?php 
		} 
		?>
	</td>
	
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['langspokenathome']?>
		<?=$LangEn['Admission']['langspokenathome']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['LangSpokenAtHome'];
		}else{
		?>
			<input id="LangSpokenAtHome" name="LangSpokenAtHome" class="textboxtext" value="<?=$applicationStudentInfo['LangSpokenAtHome'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['placeofbirth']?>
		<?=$LangEn['Admission']['placeofbirth']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['StudentPlaceOfBirth'];
		}else{
		?>
			<input id="StudentPlaceOfBirth" name="StudentPlaceOfBirth" class="textboxtext" value="<?=$applicationStudentInfo['PlaceOfBirth'] ?>" />
		<?php
		}
		?>
	</td>
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['Age']?>
		<?=$LangEn['Admission']['Age']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Age'];
		}else{
		?>
			<input id="Age" name="Age" class="textboxtext" value="<?=$applicationStudentInfo['Age'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['AOGKG']['homeTel']?>
		<?=$LangEn['Admission']['AOGKG']['homeTel']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['HomeTelNo'];
		}else{
		?>
			<input id="HomeTelNo" name="HomeTelNo" class="textboxtext" value="<?=$applicationStudentInfo['HomeTelNo'] ?>" />
		<?php
		}
		?>
	</td>
	
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['mobilephoneno']?>
		<?=$LangEn['Admission']['mobilephoneno']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['MobileNo'];
		}else{
		?>
			<input id="MobileNo" name="MobileNo" class="textboxtext" value="<?=$applicationStudentInfo['MobileNo'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['email']?>
		<?=$LangEn['Admission']['email']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $formData['Email'];
		}else{
		?>
			<input id="Email" name="Email" class="textboxtext" value="<?=$applicationStudentInfo['Email'] ?>" />
		<?php
		}
		?>
	</td>
	
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['district']?>
		<?=$LangEn['Admission']['district']?>
	</td>
	<td>
		<?php
		if($IsConfirm){
            echo $admission_cfg['District'][$formData['AddressDistrict']];
		}else{
		    foreach($admission_cfg['District'] as $id => $val){
		        $checked = ($applicationStudentInfo['AddressDistrict'] == $id)? 'checked' : '';
		?>
			<input type="radio" id="AddressDistrict<?=$id ?>" name="AddressDistrict" value="<?=$id ?>" <?=$checked ?>/>
			<label for="AddressDistrict<?=$id ?>"><?=$val ?></label>
			<br />
		<?php
		    }
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['AOGKG']['address']?>
		<?=$LangEn['Admission']['AOGKG']['address']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            echo $formData['Address'];
		}else{
		?>
			<input id="Address" name="Address" class="textboxtext" value="<?=$applicationStudentInfo['Address'] ?>" />
		<?php
		}
		?>
	</td>
</tr>

<tr>	
   	<td class="field_title">
   		<?=$star ?>
		<?=$LangB5['Admission']['AOGKG']['sessionPreference']?>
		<?=$LangEn['Admission']['AOGKG']['sessionPreference']?>
	</td>
	<td colspan="3">
		<?php
		if($IsConfirm){
            if($formData['OthersApplyDayType1'] == '1'){
                $dayType2 = ($formData['OthersApplyDayType2'] == '2')? 'sessionPmCanSelect' : 'sessionPmCannotSelect';
            ?>
				<?=$LangB5['Admission']['AOGKG']['sessionPreferenceType'][1] ?>
				<span id="selectedClassLevel"><?=$classLevel ?></span>
				<?=$LangEn['Admission']['AOGKG']['sessionPreferenceType'][1] ?>
				<br />
				<br />
				
				<?=$LangB5['Admission']['AOGKG']['sessionPmHint'].$LangB5['Admission']['AOGKG'][$dayType2] ?>
				<br />
				<?=$LangEn['Admission']['AOGKG']['sessionPmHint'] ?>
				<?=$LangEn['Admission']['AOGKG'][$dayType2] ?>
            <?php
            }else{
                echo $LangB5['Admission']['AOGKG']['sessionPreferenceType'][2];
                echo $classLevel;
                echo $LangEn['Admission']['AOGKG']['sessionPreferenceType'][2];
            }
		}else{
		    $dayType1AmChecked = (isset($applicationStudentInfo['ApplyDayType1']) && $applicationStudentInfo['ApplyDayType1'] == 1)? 'checked' : '';
		    $dayType1PmChecked = (isset($applicationStudentInfo['ApplyDayType1']) && $applicationStudentInfo['ApplyDayType1'] == 2)? 'checked' : '';
		    $dayType2PmCanSelectChecked = (isset($applicationStudentInfo['ApplyDayType2']) && $applicationStudentInfo['ApplyDayType2'])? 'checked' : '';
		    $dayType2PmCannotSelectChecked = (isset($applicationStudentInfo['ApplyDayType2']) && !$applicationStudentInfo['ApplyDayType2'])? 'checked' : '';
		?>
			<div>
				<input type="radio" id="OthersApplyDayType1Am" name="OthersApplyDayType1" value="1" <?=$dayType1AmChecked ?> />
				<label for="OthersApplyDayType1Am">
					<?=$LangB5['Admission']['AOGKG']['sessionPreferenceType'][1] ?>
					<span id="selectedClassLevel"><?=$classLevel ?></span>
					<?=$LangEn['Admission']['AOGKG']['sessionPreferenceType'][1] ?>
				</label>
			</div>
			<div id="dayType2" style="margin-left: 27px;display: none;">
				<table>
					<tr>
						<td rowspan="2" style="width: 160px;">
    						<?=$star ?>
        					<?=$LangB5['Admission']['AOGKG']['sessionPmHint'] ?>
        					<br />
        					<?=$LangEn['Admission']['AOGKG']['sessionPmHint'] ?>
						</td>
						<td>
							<input type="radio" id="OthersApplyDayType2CanSelect" name="OthersApplyDayType2" value="2" <?=$dayType2PmCanSelectChecked ?> />
            				<label for="OthersApplyDayType2CanSelect">
            					<?=$LangB5['Admission']['AOGKG']['sessionPmCanSelect'] ?>
            					<br />
            					<?=$LangEn['Admission']['AOGKG']['sessionPmCanSelect'] ?>
            				</label>
						</td>
					</tr>
					<tr>
						<td>
							<input type="radio" id="OthersApplyDayType2CannotSelect" name="OthersApplyDayType2" value="" <?=$dayType2PmCannotSelectChecked ?> />
            				<label for="OthersApplyDayType2CannotSelect">
            					<?=$LangB5['Admission']['AOGKG']['sessionPmCannotSelect'] ?>
            					<br />
            					<?=$LangEn['Admission']['AOGKG']['sessionPmCannotSelect'] ?>
            				</label>
						</td>
					</tr>
				</table>
			</div>
			
			<div>
				<input type="radio" id="OthersApplyDayType1Pm" name="OthersApplyDayType1" value="2" <?=$dayType1PmChecked ?> />
				<label for="OthersApplyDayType1Pm">
					<?=$LangB5['Admission']['AOGKG']['sessionPreferenceType'][2] ?>
					<span id="selectedClassLevel"><?=$classLevel ?></span>
					<?=$LangEn['Admission']['AOGKG']['sessionPreferenceType'][2] ?>
				</label>
			</div>
		<?php
		}
		?>
	</td>
</tr>

</table>

<script>
$(function(){
	'use strict';

	function updateUI(){
		if($('input[name="OthersApplyDayType1"]:checked').val() == '1'){
			$('#dayType2').slideDown();
		}else{
			$('#dayType2').slideUp();
		}
	}

	$('input[name="OthersApplyDayType1"]').click(updateUI);
	updateUI();
});
</script>