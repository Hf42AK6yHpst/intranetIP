<?php
	//using: Pun
	/*
	 * This page is for admission only. For general KIS config : kis/config.php
	 */

	$admission_cfg['PrintByPDF'] = 1;

	//Please also define lang in admission_lang
// 	$admission_cfg['Status'] = array();
// 	$admission_cfg['Status']['pending']	= 1;
// 	$admission_cfg['Status']['paymentsettled']	= 2;
// 	$admission_cfg['Status']['waitingforinterview']	= 3;
// 	$admission_cfg['Status']['confirmed']	= 4;
// 	$admission_cfg['Status']['cancelled']	= 5;


	$admission_cfg['Status'] = array();
	$admission_cfg['Status']['AOGKG_waitingforinterview'] = 1;
	$admission_cfg['Status']['AOGKG_amconfirmed']	= 2;
	$admission_cfg['Status']['AOGKG_pmconfirmed']	= 3;
	$admission_cfg['Status']['AOGKG_reserve']		= 4;
	$admission_cfg['Status']['AOGKG_notadmitted']	= 5;
	$admission_cfg['Status']['AOGKG_absent']		= 6;

	$admission_cfg['StatusDisplayOnTable'][1] = 'AOGKG_waitingforinterview';
	$admission_cfg['StatusDisplayOnTable'][2] = 'AOGKG_amconfirmed';
	$admission_cfg['StatusDisplayOnTable'][3] = 'AOGKG_pmconfirmed';

	$admission_cfg['PaymentStatus']['OnlinePayment']	= 1;
	$admission_cfg['PaymentStatus']['OtherPayment']	= 2;


    $admission_cfg['District'] = array();
    $admission_cfg['District'][1] = '粉嶺 Fanling';
    $admission_cfg['District'][2] = '上水 Sheung Shui';
    $admission_cfg['District'][3] = '其他 Others';


//	/* for paypal testing [start] */
//	$admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
////	$admission_cfg['paypal_to_email'] = '';
//	$admission_cfg['paypal_signature'] = '';
////	$admission_cfg['item_name'] = '';
////	$admission_cfg['item_number'] = 'AF2015';
////	$admission_cfg['amount'] = '40.00';
////	$admission_cfg['currency_code'] = 'HKD';
//	$admission_cfg['hosted_button_id'] = '';
//	$admission_cfg['paypal_name'] = '';
//	/* for paypal testing [End] */

	/* for real paypal use [start] * /
	$admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//	$admission_cfg['paypal_to_email'] = '';
	$admission_cfg['paypal_signature'] = '';
//	$admission_cfg['item_name'] = 'Application Fee';
//	$admission_cfg['item_number'] = '';
//	$admission_cfg['amount'] = '40.00';
//	$admission_cfg['currency_code'] = 'HKD';
	$admission_cfg['hosted_button_id'] = '';
	$admission_cfg['paypal_name'] = '';
	/* for real paypal use [End] */

	$admission_cfg['FilePath']	= $PATH_WRT_ROOT."/file/admission/";
	$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
	$admission_cfg['DefaultLang'] = "b5";
	$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
	$admission_cfg['personal_photo_width'] = 200;
	$admission_cfg['personal_photo_height'] = 260;

	$admission_cfg['interview_arrangment']['interview_group_type'] = 'Room';
//	$admission_cfg['interview_arrangment']['interview_group_name'] = array('101', '102', '103', '104', '201', '202', '203', '204', '301', '302', '303', '304');

	if($plugin['eAdmission_devMode']){
    	$admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
	}else{
    	$admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
	}
?>