<!-- Using: Pun -->
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery.ui.datepicker-zh-HK.js"></script>
<script type="text/javascript" src="/templates/kis/js/config.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<style>
.ui-autocomplete {max-height: 200px;max-width: 200px;overflow-y: auto;overflow-x: hidden;font-size: 12px;font-family: Verdana, "微軟正黑體";}
.ui-autocomplete-category{font-style: italic;}
.ui-datepicker{font-size: 12px;width: 210px;font-family: Verdana, "微軟正黑體";}
.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {width:auto;}
.ui-selectable tr.ui-selecting td, .ui-selectable tr.ui-selected td{background-color: #fff7a3}
</style>
<script type="text/javascript">


	// autocomplete for inputselect fields
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "ajax_get_suggestions.php",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});


var isUpdatePeriod = <?=($allowToUpdate)? 'true' : 'false'; ?>;

kis.datepicker('#StudentDateOfBirth');	

if(isUpdatePeriod){
	kis.datepicker('#InputStudentDateOfBirth');
}

var dOBRange = new Array();

//--- added to disable the back button [start]
function preventBack() {
	window.onbeforeunload = '';
    window.history.forward();
    window.onbeforeunload = function (evt) {
	  var message = '<?=$Lang['Admission']['msg']['infolost']?>';
	  if (typeof evt == 'undefined') {
	    evt = window.event;
	  }
	  if (evt) {
	    evt.returnValue = message;
	  }
	  return message;
	}
}
window.onunload = function() {
    null;
};
var preventBackTimeout = setTimeout("preventBack()", 0);
//--- added to disable the back button [end]

var timer;
var timeUp = false;

function autoSubmit(form1){
	clearTimeout(timer);
	var isValid = true;
	isValid = check_choose_class2(form1);
	if(isValid)
		isValid = check_input_info2(form1);
	if(isValid)
		isValid = check_docs_upload2(form1);
	//alert('You used 3 seconds! The validation of the form: '+isValid);
	if(!isValid){
		alert('<?=$Lang['Admission']['msg']['timeup']?>\nThe time is up! Please apply again!');
		window.onbeforeunload = '';
		window.location.href = 'submit_time_out.php?sus_status='+$('input:radio[name=sus_status]:checked').val();
	}
	else{
		alert("<?=$Lang['Admission']['msg']['annonceautosubit']?>/nThe time is up!\nThe admission form will auto submit after pressing \'OK\'!");
		window.onbeforeunload = '';
		form1.submit();
	}
		
}

function check_choose_class2(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		return false;
	}
	 else  {
		return true;
	}
}

function check_input_info2(form1) {
	var _alert = window.alert;
	window.alert = function(a){ /*console.log(a);*/ };
	
	var isValid = check_input_info(form1);
	
	window.alert = _alert;
	return isValid;
}
function check_docs_upload2(form1) {
	var _alert = window.alert;
	window.alert = function(a){ /*console.log(a);*/ };
	
	var isValid = check_docs_upload(form1);
	
	window.alert = _alert;
	return isValid;
}

/*
hkid format:  A123456(7)
A1234567
AB123456(7)
AB1234567
*/
function check_hkid(hkid) {
// hkid = $.trim(hkid);
// hkid = hkid.replace(/\s/g, '');
// hkid = hkid.toUpperCase();
// $(":input[name='id_no']").val(hkid);

var re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
var ra = re.exec(hkid);

if (ra != null) {
	var p1 = ra[1];
	var p2 = ra[2];
	var p3 = ra[4];
	var check_sum = 0;
	if (p1.length == 2) {
		check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
	}
	else if (p1.length == 1){
		check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
	}

	check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
	var check_digit = 11 - (check_sum % 11);
	if (check_digit == '11') {
		check_digit = 0;
	}
	else if (check_digit == '10') {
		check_digit = 'A';
	}
	if (check_digit == p3 ) {
		return true;
	}
	else {
		return false;
	}
}
else {
	return false;
}
}

function goto(current,page){
	var isValid = true;
	if(page == 'step_instruction'){
//		if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
//	        alert("表格需以 Google Chrome、Firefox 或 Internet Explorer 10 或以上瀏覽器填寫。");
//	        return;
//	    }
		clearTimeout(timer);
		isValid = check_choose_class($("form")[0]);
	}
	else if(page == 'step_docs_upload'){
		//alert($("form").serialize());
		isValid = check_input_info($("form")[0]);
	}
	else if(page == 'step_confirm'){
		isValid = check_docs_upload($("form")[0]);
	}
	
	// Henry added [20151013]
	if(current == 'step_index' && page == 'step_update_input_form'){
		if(form1.InputApplicationID.value==''){
			alert("請輸入申請編號。\nPlease enter Application Number.");	
			form1.InputApplicationID.focus();
			return false;
		}
		else if(form1.InputStudentBirthCertNo.value==''){
			alert("<?=$Lang['Admission']['munsang']['msg']['enterbirthcertno']?>\nPlease enter Birth Certificate Number.");	
			form1.InputStudentBirthCertNo.focus();
			return false;
		}
// 		else if(!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.InputStudentBirthCertNo.value)){
//			alert("<?=$Lang['Admission']['munsang']['msg']['invalidbirthcertificatenumber']?>\nInvalid Birth Certificate Number.");	
// 			form1.InputStudentBirthCertNo.focus();
// 			return false;
// 		}
		else if(!form1.InputStudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
			if(form1.InputStudentDateOfBirth.value!=''){
				alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
			}
			else{
				alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
			}
			
			form1.InputStudentDateOfBirth.focus();
			return false;
		}
		
		<?if($lac->IsAfterUpdatePeriod()){?>
		clearTimeout(preventBackTimeout);
		window.onbeforeunload = '';
		<?}?>
		
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_valid_for_update_form.php",
		       type: "post",
		       data: values,
		       async: false,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#InputApplicationID").val('');
		           $("#InputStudentBirthCertNo").val('');
		           $("#InputStudentDateOfBirth").val('');
		           if(data==0){
		           		alert("申請編號，出生證明書號碼或出生日期不正確。\nIncorrect Application Number, Birth Certificate Number or Date of Birth.");	
						form1.InputStudentBirthCertNo.focus();
						isValid = false;
						return false;
		           }
//		           $("#applicationno").val(data);
//		           $("#divInterviewResult").html(data);
//		           document.getElementById(current).style.display = "none";
//				   document.getElementById(page).style.display = "";
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
		   
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_input_form.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           $("#step_update_input_form").html(data);
		           kis.datepicker('#StudentDateOfBirth');
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_update_input_form' && page == 'step_confirm'){
		/* get the birthday range of the form level */
		var values = $("#form1").serialize();
	   $.ajax({
	       url: "ajax_get_bday_range.php",
	       type: "post",
	       data: values,
	       async: false,
	       success: function(data){
	           //alert("debugging: The classlevel is updated!");
	           dOBRange = data.split(",");
	       },
	       error:function(){
	           //alert("failure");
	           $("#result").html('There is error while submit');
	       }
	   });
		isValid1 = true;//check_choose_class($("form")[0]);
		isValid2 = check_input_info($("form")[0]);
		isValid3 = check_docs_upload($("form")[0]);
		
		if(isValid1 && isValid2 && isValid3){
		/* Clear result div*/
		   $("#step_confirm").html('');
		
		   	var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_update_confirm.php?encoded=1",
		       type: "post",
		       data: values,
		       async: false,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           if(data == ''){
		           	isValid = false;
		           	alert("系統繁忙中，請再次按'下一步'。\nSystem busy. Please click 'Next' button again.");
		           }
		           var decoded = $("#step_confirm").html(data).text();
		           $("#step_confirm").html(decoded);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		           isValid = false;
		           alert("系統繁忙中，請再次按'下一步'。\nSystem busy. Please click 'Next' button again.");
		       }
		   });
		   }
		   else{
		   		return false;
		   }
	}
	
	if(isValid){
		document.getElementById(current).style.display = "none";
		document.getElementById(page).style.display = "";
	}
	
	if(current == 'step_index' && page == 'step_instruction'){
		/* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_instruction.php",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!");
		           $("#step_instruction").html(data);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });
	}
	
	if(current == 'step_instruction' && page == 'step_input_form'){
		   <?if ($_SESSION["platform"]!="KIS" || !$_SESSION["UserID"]){?>
//			   clearTimeout(timer);
//			   timer = setTimeout(function(){autoSubmit($("form")[0]);},1800000);
		   <?}?>
		   /* Clear result div*/
		   $("#DayTypeOption").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();	 
		   
		/* get the birthday range of the form level */
			   $.ajax({
			       url: "ajax_get_bday_range.php",
			       type: "post",
			       data: values,
			       success: function(data){
			           //alert("debugging: The classlevel is updated!");
			           dOBRange = data.split(",");
			       },
			       error:function(){
			           //alert("failure");
			           $("#result").html('There is error while submit');
			       }
			   });
			   window.scrollTo(0,0);
	}
	
	if(current != 'step_update_input_form' && page == 'step_confirm' && isValid){
		   
		   /* Clear result div*/
		   $("#step_confirm").html('');
		
		   /* Get some values from elements on the page: */
		   var values = $("#form1").serialize();
			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
			values+=studentPersonalPhoto;
			
	//		var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
	//		var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
	//		values+=otherFile;
	//		values+=otherFile1;
			
			var file_ary = $('input[type=file][name*=OtherFile]');
			var file_count = file_ary.length;
	
			for(var i=0;i<file_count;i++)
			{
				var file_element = file_ary.get(i);
				var otherFile = '&'+file_element.name+'='+file_element.value.replace(/^.*[\\\/]/, '');
				values+=otherFile;
			}
			
			/*Upload the temp file Henry modifying 20131028*/
//			document.getElementById('form1').target = 'upload_target';
//			document.getElementById('form1').action = 'upload.php';
//    		document.getElementById('form1').submit();
			
		   /* Send the data using post and put the results in a div */
		   $.ajax({
		       url: "ajax_get_confirm.php?encoded=1",
		       type: "post",
		       data: values,
		       success: function(data){
		           //alert("debugging: The classlevel is updated!"+values);
		           //$("#step_confirm").html(data);
		           var decoded = $("#step_confirm").html(data).text();
		           $("#step_confirm").html(decoded);
		       },
		       error:function(){
		           //alert("failure");
		           $("#result").html('There is error while submit');
		       }
		   });

	}
}

function submitForm(){
	document.getElementById('form1').target = '';
	if(!isUpdatePeriod)
		document.getElementById('form1').action = 'confirm_update.php';
	window.onbeforeunload = '';
	if(isUpdatePeriod)
		return confirm('<?=$Lang['Admission']['msg']['suresubmit']?>\nAre you sure you want to submit?');	
	return confirm('<?=$Lang['Admission']['munsang']['msg']['suresubmit']?>\nPlease check the information given again, it cannot edit after submitted.');
}

function check_choose_class(form1) {
	if($('input:radio[name=sus_status]:checked').val() == null){
		alert("<?=$Lang['Admission']['msg']['selectclass']?>\nPlease select Class.");
		if(form1.sus_status[0])
			form1.sus_status[0].focus();
		else
			form1.sus_status.focus();
		return false;
	}
	else  {
		return true;
	}
}

function check_input_info(form1) {
	//For debugging only
	//return true;
	
	var isTeacherInput = <?= ($lac->isInternalUse($_GET['token']))?1:0 ?>;
	
	// borower version checking if browser is IE9 or below or not
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	//for email validation
	var re = /\S+@\S+\.\S+/;

	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>

	/******** Student Info START ********/
	/**** Name START ****/
	if($.trim(form1.studentsname_b5.value)==''){
		alert("<?=$Lang['Admission']['msg']['enterchinesename']?>\nPlease enter Chinese Name.");	
		form1.studentsname_b5.focus();
		return false;
	}
	if($.trim(form1.studentsname_en.value)==''){
		alert("<?=$Lang['Admission']['msg']['enterenglishname']?>\nPlease enter English Name.");	
		form1.studentsname_en.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** DOB START ****/
	if(!form1.StudentDateOfBirth.value.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
		if(form1.StudentDateOfBirth.value!=''){
			alert("<?=$Lang['Admission']['msg']['invaliddateformat']?>\nInvalid Date Format");
		}
		else{
			alert("<?=$Lang['Admission']['msg']['enterdateofbirth']?>\nPlease enter Date of Birth.");	
		}
		
		form1.StudentDateOfBirth.focus();
		return false;
	} 
	if(dOBRange[0] !='' && form1.StudentDateOfBirth.value < dOBRange[0] || dOBRange[1] !='' && form1.StudentDateOfBirth.value > dOBRange[1]){
		alert("<?=$Lang['Admission']['msg']['invalidbdaydateformat']?>\nInvalid Birthday Range of Student");
		form1.StudentDateOfBirth.focus();
		return false;
	} 
	/**** DOB END ****/

	/**** Gender START ****/
	if(!isTeacherInput && $.trim($('input:radio[name=StudentGender]:checked').val())==''){
		alert("<?=$Lang['Admission']['msg']['selectgender']?>\nPlease select Gender.");	
		form1.StudentGender[0].focus();
		return false;
	}
	/**** Gender END ****/
	
	/**** Place of Birth START ****/
// 	if(!isTeacherInput && $.trim(form1.StudentPlaceOfBirth.value)==''){
//		alert("<?=$Lang['Admission']['msg']['enterplaceofbirth']?>\nPlease enter Place of Birth.");	
// 		form1.StudentPlaceOfBirth.focus();
// 		return false;
// 	} 
	/**** Place of Birth END ****/
	
	/**** Nationality START ****/
// 	if(!isTeacherInput && $.trim(form1.Nationality.value)==''){
//		alert("<?=$Lang['Admission']['icms']['msg']['enternationality']?>\nPlease Enter Nationality.");	
// 		form1.Nationality.focus();
// 		return false;
// 	} 
	/**** Nationality END ****/
	
	/**** HKID START ****/
	if(!isTeacherInput && $.trim(form1.StudentBirthCertNo.value) == ''){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterHKID']?>\nPlease Enter HKID Card No.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} 
	
	if(
		(
			!isTeacherInput && 
			!/^[a-zA-Z][0-9]{6}(a|A|[0-9])$/.test(form1.StudentBirthCertNo.value)
		)||(
			 $.trim(form1.StudentBirthCertNo.value) != '' &&
			!check_hkid(form1.StudentBirthCertNo.value)
		)
	){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['invalidHKID']?>\nInvalid HKID Card No.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} 
	if(!isTeacherInput && !isUpdatePeriod && checkBirthCertNo() > 0){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['duplicateHKID']?>\nThe HKID Card No. is used for admission! Please enter another HKID Card No.");	
		form1.StudentBirthCertNo.focus();
		return false;
	} 
	/**** HKID END ****/
	
	/**** B.D. Reference START ****/
	if(!isTeacherInput && form1.BdRefNo.value==''){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterBdReference']?>\nPlease Enter B.D.Student Reference No.");	
		form1.BdRefNo.focus();
		return false;
	} 
	/**** B.D. Reference END ****/
	
	/**** School Attending START ****/
	if(!isTeacherInput && form1.LastSchool.value==''){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLastSchool']?>\nPlease Enter School Attending.");	
		form1.LastSchool.focus();
		return false;
	} 
	/**** School Attending END ****/
	
	/**** Class Repeated START ****/
	/**** Class Repeated END ****/
	
	/**** Class Last Attended START ****/
	if(!isTeacherInput && form1.LastSchoolLevel.value==''){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterClassLastAttended']?>\nPlease Enter Class Last Attended.");	
		form1.LastSchoolLevel.focus();
		return false;
	} 
	/**** Class Last Attended END ****/
	
	/**** Elementary Chinese Student START ****/
	if(!isTeacherInput && $.trim($('input:radio[name=Elementary]:checked').val())==''){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['selectElementary']?>\nPlease Select Elementary Chinese Student.");	
		form1.ElementaryN.focus();
		return false;
	} 
	/**** Elementary Chinese Student END ****/
	
	/**** Address START ****/
	if(!isTeacherInput && form1.StudentHomeAddress.value==''){
		alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>\nPlease enter Address.");	
		form1.StudentHomeAddress.focus();
		return false;
	} 
// 	if(!isTeacherInput && form1.StudentHomeAddressChi.value==''){
//		alert("<?=$Lang['Admission']['munsang']['msg']['enteraddress']?>\nPlease enter Address.");	
// 		form1.StudentHomeAddressChi.focus();
// 		return false;
// 	} 
	/**** Address END ****/
	
	/**** Languages Spoken START **** /
	if(!isTeacherInput &&  $('[name="LangSpoken[]"]:checked').length == 0 ){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLangSpoken']?>\nPlease Enter Languages Spoken.");	
		form1.LangSpoken1.focus();
		return false;
	}
	if(!isTeacherInput &&  $('#LangSpokenOtherChk').attr('checked') == 'checked' ){
		if(form1.LangSpokenOther.value==''){
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLangSpoken']?>\nPlease Enter Languages Spoken.");
			form1.LangSpokenOther.focus();
			return false;
		} 
	}
	/**** Languages Spoken END ****/
	
	/**** Languages Written START **** /
// 	if(!isTeacherInput &&  $('[name="LangWritten[]"]:checked').length == 0 ){
//		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLangWritten']?>\nPlease Enter Languages Written.");	
// 		form1.LangWritten1.focus();
// 		return false;
// 	}
	if(!isTeacherInput &&  $('#LangWrittenOtherChk').attr('checked') == 'checked' ){
		if(form1.LangWrittenOther.value==''){
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLangWritten']?>\nPlease Enter Languages Written.");
			form1.LangWrittenOther.focus();
			return false;
		} 
	}
	/**** Languages Written END ****/
	
	/**** Extra-curricular START ****/
	/**** Extra-curricular END ****/
	
	/**** Church START ****/
	/**** Church END ****/
	
	/**** Church Activities START ****/
	/**** Church Activities END ****/
	
	/******** Student Info END ********/

	
	/******** Parent Info START ********/
	/**** Name START ****/
	if($.trim(form1.ParentName.value)==''){ //for parent info
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterParentName']?>\nPlease Enter Name of Parent/Guardian.");
		form1.ParentName.focus();
		return false;
	}
	/**** Name END ****/
	
	/**** Relationship START ****/
	if(!isTeacherInput && $.trim(form1.ParentRelationship.value)==''){ //for parent info
		alert("<?=$Lang['Admission']['msg']['enterrelationship']?>\nPlease enter Relationship.");
		form1.ParentRelationship.focus();
		return false;
	}
	/**** Relationship END ****/
	
	/**** Religion START ****/
	/**** Religion END ****/
	
	/**** Email START ****/
	if($.trim(form1.ParentEmail.value)==''){ //for parent info
		alert("<?=$Lang['Admission']['icms']['msg']['entermailaddress']?>\nPlease enter Email Address.");
		form1.ParentEmail.focus();
		return false;
	}
	if(!re.test(form1.ParentEmail.value)){
		alert("<?=$Lang['Admission']['icms']['msg']['invalidmailaddress']?>\nInvalid Email Format");
		form1.ParentEmail.focus();
		return false;
	}
	if($.trim(form1.ParentEmail.value)!='' && form1.ParentEmail.value != $('#ParentEmailConfirm').val()){
		alert("<?=$Lang['Admission']['msg']['contactEmailMismatch']?>\nConfirm Contact Email Mismatch.");
		form1.ParentEmailConfirm.focus();
		return false;
	}
	/**** Email END ****/
	
	/**** Home Phone START ****/
// 	if(!isTeacherInput && $.trim(form1.ParentHomeTelNo.value)==''){ //for parent info
//		alert("<?=$Lang['Admission']['csm']['msg']['enterstudenthomephoneno']?>\nPlease enter Home Phone No.");
// 		form1.ParentHomeTelNo.focus();
// 		return false;
// 	}
	if($.trim(form1.ParentHomeTelNo.value)!=''){
		if(!isTeacherInput && !/^[0-9]{8}$/.test(form1.ParentHomeTelNo.value)){
			alert("<?=$Lang['Admission']['msg']['invalidhomephoneformat']?>\nInvalid Home Phone Format.");
			form1.ParentHomeTelNo.focus();
			return false;
		}
	}
	/**** Home Phone END ****/
	
	/**** Fax START ****/
	/**** Fax END****/
	
	/**** Mobile Phone START ****/
	if(!isTeacherInput && $.trim(form1.ParentMobileNo.value)==''){ //for parent info
		alert("<?=$Lang['Admission']['icms']['msg']['entermobileno']?>\nPlease enter Mobile Phone No.");
		form1.ParentMobileNo.focus();
		return false;
	}
	if(!isTeacherInput && !/^[0-9]{8}$/.test(form1.ParentMobileNo.value)){
		alert("<?=$Lang['Admission']['msg']['invalidmobilephoneformat']?>\nInvalid Mobile Format.");
		form1.ParentMobileNo.focus();
		return false;
	}
	/**** Mobile Phone END ****/
	
	/**** Occupation START ****/
	if(!isTeacherInput && $.trim(form1.ParentOccupation.value)==''){ //for parent info
		alert("<?=$Lang['Admission']['msg']['enteroccupation']?>\nPlease Enter Occupation.");
		form1.ParentOccupation.focus();
		return false;
	}
	/**** Occupation END ****/
	
	/**** Employer START ****/
// 	if(!isTeacherInput && $.trim(form1.ParentEmployer.value)==''){ //for parent info
//		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterParentEmployerName']?>\nPlease Enter Name of Employer.");
// 		form1.ParentEmployer.focus();
// 		return false;
// 	}
	/**** Employer END ****/
	
	/**** Language Spoken START **** /
	if(!isTeacherInput &&  $('[name="ParentLangSpoken[]"]:checked').length == 0 ){
		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLangSpoken']?>\nPlease Enter Languages Spoken.");	
		form1.ParentLangSpoken1.focus();
		return false;
	}
	if(!isTeacherInput &&  $('#ParentLangSpokenOtherChk').attr('checked') == 'checked' ){
		if(form1.ParentLangSpokenOther.value==''){
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterLangSpoken']?>\nPlease Enter Languages Spoken.");
			form1.ParentLangSpokenOther.focus();
			return false;
		} 
	}
	/**** Language Spoken END ****/
	
	/**** Education START **** /
// 	if(!isTeacherInput && $.trim($('input:radio[name=ParentEducation]:checked').val())==''){
//		alert("<?=$Lang['Admission']['UCCKE']['msg']['enterParentEducation']?>\nPlease Enter Education.");	
// 		form1.ParentEducation1.focus();
// 		return false;
// 	}
	if(!isTeacherInput &&  $('#ParentEducationOtherChk').attr('checked') == 'checked' ){
		if(form1.ParentEducationOther.value==''){
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterParentEducation']?>\nPlease Enter Education.");
			form1.ParentEducationOther.focus();
			return false;
		} 
	}
	/**** Education END ****/
	/******** Parent Info END ********/
	
	
	/******** Other Info START ********/
	if(!isTeacherInput && form1.RelativeStudent.value != ''){
		if(form1.RelativeRelationship.value==''){
			alert("<?=$Lang['Admission']['msg']['enterrelationship']?>\nPlease Enter Relationship.");
			form1.RelativeRelationship.focus();
			return false;
		}
		if(form1.RelativeClassAttend.value==''){
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterClassAttend']?>\nPlease Enter Class / Attending.");
			form1.RelativeClassAttend.focus();
			return false;
		}
	}
	if(!isTeacherInput && form1.RelativeRelationship.value!='' || form1.RelativeClassAttend.value!=''){
		if(form1.RelativeStudent.value == ''){
			alert("<?=$Lang['Admission']['UCCKE']['msg']['enterRelativeName']?>\nPlease Enter Relatives\' name.");
			form1.RelativeStudent.focus();
			return false;
		}
	}
	/******** Other Info END ********/
	
	return true;	
}

function check_docs_upload(form1) {

	var isTeacherInput = <?= ($lac->isInternalUse($_GET['token']))?1:0 ?>;
	
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
	}
	
	<?if($admission_cfg['maxUploadSize'] > 0){?>
		var maxFileSize = <?=$admission_cfg['maxUploadSize'] * 1024 * 1024?>;
	<?}else{?>
		var maxFileSize = 1 * 1024 * 1024;
	<?}?>
	
	var file_ary = $('input[type=file][name*=OtherFile]');
	var file_count = file_ary.length;

	/******** Personal Photo START ********/
	if(!isUpdatePeriod && !isTeacherInput && form1.StudentPersonalPhoto.value==''){
		alert("<?=$Lang['Admission']['msg']['uploadPersonalPhoto']?>\nPlease upload a personal photo.");	
		form1.StudentPersonalPhoto.focus();
		return false;
	}
	/******** Personal Photo END ********/
	 
	 
	/******** File format START ********/
	if(form1.StudentPersonalPhoto.value!=''){
		var studentPhotoExt = form1.StudentPersonalPhoto.value.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			if(form1.StudentPersonalPhoto.files[0]){
			studentPhotoExt = form1.StudentPersonalPhoto.files[0].name.split('.').pop().toUpperCase();
			var studentPhotoSize = form1.StudentPersonalPhoto.files[0].size;
			}
		}
		if(studentPhotoExt !='JPG' && studentPhotoExt !='JPEG' && studentPhotoExt !='PNG' && studentPhotoExt !='GIF'){
			alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");	
			form1.StudentPersonalPhoto.focus();
			return false;
		} 
	}
	/******** File format END ********/
	
	/******** File size START ********/
	if(!isOldBrowser && studentPhotoSize > maxFileSize){
		alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
		form1.StudentPersonalPhoto.focus();
		return false;
	}
	/******** File size END ********/
	
	for(var i=0;i<file_count;i++)
	{
		var file_element = file_ary.get(i);
		
		var otherFileVal = file_element.value;
		var otherFileExt = otherFileVal.split('.').pop().toUpperCase();
		if(!isOldBrowser){
			otherFileExt = file_element.files.length>0? file_element.files[0].name.split('.').pop().toUpperCase() : '';
			var otherFileSize = file_element.files.length>0? file_element.files[0].size : 0;
		}
		if(!isUpdatePeriod || otherFileVal!=''){
			if(i == 0 && !isTeacherInput && otherFileVal==''){
				alert("<?=$Lang['Admission']['msg']['uploadfile']?>\nPlease upload file.");
				file_element.focus();
				return false;
			}
			
			if(otherFileVal!='' && otherFileExt !='JPG' && otherFileExt !='JPEG' && otherFileExt !='PNG' && otherFileExt !='GIF' && otherFileExt !='PDF'){
				alert("<?=$Lang['Admission']['msg']['invalidfileformat']?>\nInvalid File Format");
				file_element.focus();
				return false;
			} else if(otherFileVal!='' && !isOldBrowser && otherFileSize > maxFileSize){
				alert("<?=$Lang['Admission']['msg']['FileSizeExceedLimit']?>\nFile size exceeds limit.");	
				file_element.focus();
				return false;
			}
		}
	}
	return true;
}

function checkBirthCertNo(){
	var values = $("#form1").serialize();
	var res = null;
	/* check the birth cert number is applied or not */
   $.ajax({
       url: "ajax_get_birth_cert_no.php",
       type: "post",
       data: values,
       async: false,
       success: function(data){
           //alert("debugging: The classlevel is updated!");
            res = data;
       },
       error:function(){
           //alert("failure");
           $("#result").html('There is error while submit');
       }
   });
   return res;
}

//function checkInterviewQuotaLeft(){
//	var values = $("#form1").serialize();
//	var res = null;
//	/* Check the quota of interview timeslot */
//   $.ajax({
//       url: "ajax_check_num_of_interview_quota.php",
//       type: "post",
//       data: values,
//       async: false,
//       success: function(data){
//           //alert("debugging: The classlevel is updated!"+values);
//           //$("#step_confirm").html(data);
////           if(data <= 0){
////		           	alert("<?=$Lang['Admission']['msg']['interviewtimeslotisfull']?>");
////		           	return false;
//					res = data;
////		           }
//		       },
//		       error:function(){
//		           //alert("failure");
//           //$("#result").html('There is error while submit');
//       }
//   });
//   return res;	
//}

function addRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount > 4){
			document.getElementById('btn_addRow1').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow1').style.display = '';
		}
		 
	}
	else{
		if(rowCount > 4){
			document.getElementById('btn_addRow2').style.display = 'none';
		}
		else if(rowCount > 1){
			document.getElementById('btn_deleteRow2').style.display = '';
		}
	}
	
	if(rowCount <= 5){                            // limit the user from creating fields more than your limits
		var row = table.insertRow(rowCount);
		var newcell = row.insertCell(0);
		newcell.className = "field_title";
		newcell.style.textAlign ="right";
		newcell.innerHTML = '('+rowCount+')';
		if(tableID == 'dataTable1'){
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchYear'+rowCount+'" type="text" id="OthersPrevSchYear'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchClass'+rowCount+'" type="text" id="OthersPrevSchClass'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersPrevSchName'+rowCount+'" type="text" id="OthersPrevSchName'+rowCount+'" class="textboxtext" />';
		}
		else{
			newcell = row.insertCell(1);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedYear'+rowCount+'" type="text" id="OthersRelativeStudiedYear'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(2);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeStudiedName'+rowCount+'" type="text" id="OthersRelativeStudiedName'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(3);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeClassPosition'+rowCount+'" type="text" id="OthersRelativeClassPosition'+rowCount+'" class="textboxtext" />';
			newcell = row.insertCell(4);
			newcell.className = "form_guardian_field";
			newcell.innerHTML = '<input name="OthersRelativeRelationship'+rowCount+'" type="text" id="OthersRelativeRelationship'+rowCount+'" class="textboxtext" />';
		}
	}else{
		 //alert("Maximum Passenger per ticket is 5");
			   
	}
}

function deleteRow(tableID) {
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	
	if(tableID == 'dataTable1'){
		if(rowCount < 4){
			document.getElementById('btn_deleteRow1').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow1').style.display = '';
		}
	}
	else{
		if(rowCount < 4){
			document.getElementById('btn_deleteRow2').style.display = 'none';
		}
		else if(rowCount < 7){
			document.getElementById('btn_addRow2').style.display = '';
		}
	}
	
	if(rowCount <= 2) {               // limit the user from removing all the fields
		//alert("Cannot Remove all the Passenger.");				
	}
	else{
		table.deleteRow(rowCount - 1);	
	}
}

//function startUpload(){
//      document.getElementById('f1_upload_process').style.visibility = 'visible';
//      document.getElementById('f1_upload_form').style.visibility = 'hidden';
//      return true;
//}
//Henry modifying 20131028
//function stopUpload(temp_folder_name){
//		document.getElementById('tempFolderName').value = temp_folder_name;
//		
//		 var values = $("#form1").serialize();
//			var studentPersonalPhoto = '&StudentPersonalPhoto='+$("#StudentPersonalPhoto").val().replace(/^.*[\\\/]/, '');
//			var otherFile = '&OtherFile='+$("#OtherFile").val().replace(/^.*[\\\/]/, '');
//			var otherFile1 = '&OtherFile1='+$("#OtherFile1").val().replace(/^.*[\\\/]/, '');
//			values+=studentPersonalPhoto;
//			values+=otherFile;
//			values+=otherFile1;
//		$.ajax({
//		       url: "ajax_get_confirm.php",
//		       type: "post",
//		       data: values,
//		       success: function(data){
//		           //alert("debugging: The classlevel is updated!"+values);
//		           $("#step_confirm").html(data);
//		       },
//		       error:function(){
//		           //alert("failure");
//		           $("#result").html('There is error while submit');
//		       }
//		   });     
//      return true;  
//}
</script>