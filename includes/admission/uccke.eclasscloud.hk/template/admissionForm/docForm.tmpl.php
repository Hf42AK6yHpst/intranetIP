
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/thickbox.css">
<script type="text/javascript" src="/templates/jquery/thickbox-compressed.js"></script>
<script src="/templates/jquery/jquery.cropit.js" type="text/javascript" charset="utf-8"></script>


<?php if(!$IsConfirm && !$IsUpdate){ ?>
    <?=$this->getWizardStepsUI(5)?>
<?php }else{ ?>
	<h1 style="font-size: 15px"><?=$Lang['Admission']['docsUpload'] ?> Documents Upload</h1>
<?php } ?>

<table class="form_table" style="font-size: 15px">
    <?php if(!$IsConfirm){ ?>
    <tr>
		<td colspan="2">
			<span class="date_time">
    			<?=$Lang['Admission']['document'] ?>
				(
				<?=$Lang['Admission']['UCCKE']['msg']['birthCertFormat'] ?>
				<?= ($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1') ?> MB
				)
				<br/>
				Document 
				(
				image in JPEG/GIF/PNG format, file size less than : 
				<?=($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1') ?> MB
				)
			</span>
		</td>
	</tr>
    <?php } ?>
    <tr>
		<td class="field_title">
			<?=$star ?>
			<?=$Lang['Admission']['personalPhoto'] ?> Personal Photo
		</td>
		<td>
			<?php if($IsConfirm){ ?>
				<?=stripslashes($fileData['StudentPersonalPhoto']) ?>
			<?php }else{ ?>
				<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png"/>
			<?php } ?>
			<?=$viewFilePath ?>
		</td>
	</tr>
	
	<?php
	for($i=0;$i<$attachment_settings_count;$i++) {
	    $attachment_name = $attachment_settings[$i]['AttachmentName'];
	    
	    $accept = 'image/gif, image/jpeg, image/jpg, image/png, application/pdf';
	    $allowCrop = false;
	    if($i==0){
    	    $allowCrop = true;
    	    $accept = 'image/gif, image/jpeg, image/jpg, image/png';
	    }
	?>
		<tr>
			<td class="field_title">
				<?= ($i==0)? $star : '' ?>
				<?=$attachment_name ?>
			</td>
			<td>
				<?php 
				if($allowCrop){
				    $display = ($IsConfirm && $fileData['CropOtherFile'.$i])? '' : 'display:none';
			    ?>
    			    <img id="CropPreviewOtherFile<?=$i?>" src="<?=$fileData['CropOtherFile'.$i] ?>" style="margin-bottom:5px;width: 322px;height: 54mm;<?=$display ?>"/>
    			    <br id="CropPreviewNewLineOtherFile<?=$i?>" style="<?=$display ?>"/>
			    <?php
				}
				if($IsConfirm){ ?>
					<?=stripslashes($fileData['OtherFile'.$i]) ?>
				<?php }else{ ?>
					<input type="file" name="OtherFile<?=$i?>" id="OtherFile<?=$i?>" value="Add" class="OtherFile" accept="<?=$accept ?>" />
					<?php if($allowCrop){ ?>
    					<input type="button"  id="CropEditOtherFile<?=$i?>" data-target="OtherFile<?=$i?>" class="formbutton cropImage" value="<?=$Lang['Admission']['mgf']['cropPhoto'] ?>" style="margin-left: 5px;display:none;">
    					<input type="hidden" id="CropOtherFile<?=$i?>" name="CropOtherFile<?=$i?>" />
					<?php } ?>
				<?php } ?>
				
				<?php
				if(
				    is_file($intranet_root."/file/admission/".$applicationAttachmentInfo[$application_details['ApplicationID']][$attachment_settings[$i]['AttachmentName']]['attachment_link'][0]) && 
				    ($IsUpdate && !$IsConfirm || $IsUpdate && $IsConfirm && !$fileData['OtherFile'.$i])
		        ){
				?>
					<a href="download_attachment.php?type=<?=$attachment_settings[$i]['AttachmentName'] ?>" target="_blank" >檢視已遞交的檔案 View submitted file</a>
			    <?php 
				}
				?>
			    
		    </td>
	    </tr>
	<?php 
	}
	if(!$IsConfirm && $IsUpdate){
	?>
    	<tr>
    		<td colspan="2">
    			<?=$star ?>
				如不需更改文件檔案，則不用選擇檔案 if you no need to update document file, please make it blank
			</td>
    	</tr>
	<?php 
	}
	?>
</table>
		
<?php if(!$IsConfirm && !$IsUpdate){ ?>
	</div>
	<div class="edit_bottom">
		<?=$this->GET_ACTION_BTN($Lang['Btn']['Back'].' Back', "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton") ?>
		<?=$this->GET_ACTION_BTN($Lang['Btn']['Next'].' Next', "button", "goto('step_docs_upload','step_confirm')", "goToConfirm", "", 0, "formbutton") ?>
		
		<?php if(!$lac->isInternalUse($_GET['token']) && $sys_custom['KIS_Admission']['IntegratedCentralServer']){ ?>
			<input type="button" class="formsubbutton" onclick="MM_goToURL('parent','index.php?token=<?=$_REQUEST['token'] ?>');return document.MM_returnValue" value="<?=$Lang['Btn']['Cancel'] ?> Cancel" />
		<?php } ?>
		
		<input type="button" class="formsubbutton" onclick="MM_goToURL('parent','index.php');return document.MM_returnValue" value="<?=$Lang['Btn']['Cancel'] ?> Cancel" />
    </div>
	<p class="spacer"></p>
<?php } ?>



<style>
/**** Image Cropper START ****/
.cropit-preview {
    width: 322px;
    height: 54mm;
    margin: 25px auto;
}
.cropit-preview-container {
    border: 1px solid lightgrey;
}
.cropit-preview-image-container{
    cursor: move;
    border: 1px solid red;
}
input.cropit-image-input {
    display: none;
}
input.cropit-image-zoom-input {
    position: relative;
}
input.cropit-image-zoom-input[disabled] {
    cursor: not-allowed;
}
#image-cropper {
    overflow: hidden;
}
.image-control{
    text-align: center;
    padding-bottom: 5px;
}
.cropit-preview-background {
    opacity: .2;
}
.rotateBtn {
    font-size: 2em;
    color: #737373;
    cursor: pointer;
}
.select-image-btn{
    margin-top: 5px;
}
/**** Image Cropper END ****/

/**** Save START ****/
#saveControlBtn{
    margin-top: 10px;
    padding: 10px;
    text-align: center;
}
/**** Save END ****/
</style>
<div id="cropPhotoDiv" style="display:none;">
	<!-- This wraps the whole cropper -->
	<div id="image-cropper">
		<div class="cropit-preview-container">
    		<!-- This is where the preview image is displayed -->
			<div class="cropit-preview"></div>
		</div>
		
		<div class="image-control">
    		<!-- This range input controls zoom -->
    		<!-- You can add additional elements here, e.g. the image icons -->
    		<input type="range" class="cropit-image-zoom-input" />
    		
    		<!-- This is where user selects new image -->
    		<span class="rotateBtn rotate-ccw-btn"><i class="fa fa-rotate-left" aria-hidden="true"></i></span>
    		<span class="rotateBtn rotate-cw-btn"><i class="fa fa-rotate-right" aria-hidden="true"></i></span>
    		<input type="file" class="cropit-image-input" />
    		<!--button 
    			type="button"
    			class="formsubbutton select-image-btn"
    		>
    			<?=$Lang['Admission']['mgf']['uploadNewImage'] ?>
    		</button-->
		</div>
	</div>
	
	<div id="saveControlBtn" class="edit_bottom">
    	<button type="button" class="formbutton" id="saveCropImage">
    		<?=$Lang['Btn']['Confirm']?>
    	</button>
    	&nbsp;
    	<button type="button" class="formsubbutton" onclick="tb_remove()">
    		<?=$Lang['Btn']['Cancel']?>
    	</button>
	</div>
</div>


<script>
(function() {
	'use strict';

	$('.OtherFile').change(function(ev){
		var id = $(this).attr('id');
		if($('#CropEdit' + id).length == 0){
			return;
		}
		
		if(ev.target.files.length == 1){
    		var fr = new FileReader();
            fr.onload = function () {
        		$('#CropPreview' + id).attr('src', fr.result).show();
        		$('#Crop' + id).val(fr.result);
        		$('#CropPreviewNewLine' + id).show();
            }
            fr.readAsDataURL(ev.target.files[0]);
		}
		
		$('#Crop' + id).val('');

		var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
		if ($(this).val() == '' || $.inArray(ev.target.files[0]["type"], ValidImageTypes) == -1) {
			$('#CropEdit' + id).hide();
		}else{
			$('#CropEdit' + id).show();
		}
		$( '#CropPreview' + currentCropId ).hide();
	});
	
	
	var currentCropId = '';

	var $imageCropper = $('#image-cropper');
	$imageCropper.cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 25,
		smallImage: 'stretch',
		maxZoom: 1.5
	});

    $('.rotate-cw-btn').click(function() {
    	$('#image-cropper').cropit('rotateCW');
    });
    $('.rotate-ccw-btn').click(function() {
    	$('#image-cropper').cropit('rotateCCW');
    });


	$('.cropImage').click(function(e){
		currentCropId = $(this).data('target');
        if($('#Crop' + currentCropId).val()){
        	$imageCropper.cropit('imageSrc', $('#Crop' + currentCropId).val());
    		tb_show('','#TB_inline?height=360&width=380&inlineId=cropPhotoDiv&modal=true',null);
        }
		return false; // Prevent open default upload behaviour
	});
	$('#saveCropImage').click(function(){
		var newPhotoString = $imageCropper.cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true
        });
		$( '#Crop' + currentCropId ).val(newPhotoString);
		$( '#CropPreview' + currentCropId ).attr('src', newPhotoString).show();
		$('#CropPreviewNewLine' + currentCropId).show();
		
		tb_remove();
	});
})();
</script>