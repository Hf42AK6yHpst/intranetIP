﻿<?php

//error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);

######## Image START ########
$imgs = array(
    'logo',
    'photo',
);
foreach ($imgs as $img) {
    $$img = "{$baseFilePath}/images/{$img}.png";
}
######## Image END ########

######## Get data START ########
$basicSettings = $lac->getBasicSettings('99999', array (
						'schoolnamechi',
						'schoolnameeng',
						'schooladditionalinfo',
						'printformlabeltype',
						'applicationnoformatstartfrom',
						'remarksnamechi',
						'remarksnameeng'
					));
#### Get student info START ####
$StudentInfo = current($lac->getApplicationStudentInfo($schoolYearID, '', $applicationID));

## Photo START ##
$attachmentArr = $lac->getAttachmentByApplicationID($schoolYearID, $applicationID);
$photoLink = $attachmentArr['personal_photo']['link'];
$photoLink = ($photoLink) ? $photoLink : $photo;
## Photo END ##
#### Get student info END ####

#### Get parent info START ####
$tmpParentInfoArr = $lac->getApplicationParentInfo($schoolYearID, '', $applicationID);
$parentInfoArr = array();
foreach ($tmpParentInfoArr as $parent) {
    foreach ($parent as $para => $info) {
        $ParentInfo[$parent['type']][$para] = $info;
    }
}
#### Get parent info END ####

#### Get prev school info START ####
$StudentPrevSchoolInfo = $lac->getApplicationPrevSchoolInfo($schoolYearID, '', $applicationID);
#### Get prev school info END ####

#### Get sibling info START ####
$SiblingInfo = $lac->getApplicationSibling($schoolYearID, '', $applicationID);
#### Get sibling info END ####

#### Get other info START ####
$OtherInfo = $lac->getApplicationOthersInfo($schoolYearID, '', $applicationID);

$classLevelName = $allClassLevel[$OtherInfo[0]['classLevelID']];
#### Get other info END ####

#### Get cust info START ####
$remarks = $lac->getApplicationCustInfo($applicationID, 'Remarks', true);
$remarks = $remarks['Value'];
#### Get cust info END ####

if ($sys_custom['KIS_Admission']['PayPal']) {
    $paypalPaymentInfo = $lac->getPaymentResult('', '', '', $schoolYearID, $applicationID);
}

$yearStart = date('Y',getStartOfAcademicYear('',$schoolYearID));
$yearEnd = (((int)$yearStart)+1);
		
######## Get data END ########

$remarksnameeng = $basicSettings['remarksnameeng']?$basicSettings['remarksnameeng']:'Remarks';
$remarksnamechi = $basicSettings['remarksnamechi']?$basicSettings['remarksnamechi']:'備註';

?>
<body>
<?php if ($applicationIndex > 0) { ?>
    <div style="page-break-before: always;line-height: 0.1mm;height: 0.1mm;font-size: 0.1mm;">&nbsp;</div>
<?php } ?>

<div id="content">
    <div class="page_wrapper">
        <!-- Header starts -->
        <?php include(__DIR__ . "/content_header.php"); ?>
        <!-- Header ends-->
        <table cellpadding="0" cellspacing="0" class="tbl_formTitle">
            <tr>
            	<td width="130"></td>
                <td>
                    <span><?=$yearStart ?> - <?=$yearEnd?> Application Form for Admission<br/><?=$yearStart ?> - <?=$yearEnd?> 入學申請表</span>
                </td>
                <td class="barcode"><img src="<?=$basicSettings['printformlabeltype']=='qrcode'?'qrcode.php?encode':'barcode.php?barcode'?>=<?=rawurlencode($applicationID)?>" width="<?=($basicSettings['printformlabeltype']=='qrcode'?'70':'130')?>"/></td>
            </tr>
            <tr>
                <td class="regNo" colspan="3">
                    <span class="no">No. 申請編號︰</span><?= $applicationID ?>
                </td>
            </tr>
        </table>
        <!-- Student information starts -->
        <table cellpadding="0" cellspacing="0" class="tbl_form">
            <tr>
                <td class="tbl_col15"></td>
                <td class="tbl_col20"></td>
                <td class="tbl_col10"></td>
                <td class="tbl_col35"></td>
                <td class="tbl_col15"></td>
                <td class="tbl_col10"></td>
                <td class="tbl_col35"></td>
                <td class="tbl_col10"></td>
                <td class="tbl_col20"></td>
                <td class="tbl_col20"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <span>Name in Chinese 中文姓名</span>
                    <p><?= $StudentInfo['ChineseName'] ?></p>
                </td>
                <td colspan="2">
                    <span>Name in English 英文姓名</span>
                    <p><?= $StudentInfo['EnglishName'] ?></p>
                </td>
                <td colspan="2">
                    <span>Sex 性別</span>
                    <p><?= ($StudentInfo['Gender'] == 'F') ? 'Female 女' : 'Male 男' ?></p>
                </td>
                <td class="photo" colspan="3" rowspan="5" style="padding: 1mm;">
                    <img src="<?= $photoLink ?>" width="4cm" height="5cm"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span>Nationality 國籍</span>
                    <p><?= $StudentInfo['Nationality'] ?></p>
                </td>
                <td colspan="2">
                    <span>Date of Birth 出生日期</span>
                    <p><?= $StudentInfo['DOB'] ?></p>
                </td>
                <td colspan="2">
                    <span>Place of Birth 出生地點</span>
                    <p><?= $StudentInfo['PlaceOfBirth'] ?></p>
                </td>
            </tr>
            <tr>
            	<td colspan="7">
                    <span>Birth Certificate Number 出生證明書編號</span>
                    <p>(<?= ($StudentInfo['BirthCertType'] == $admission_cfg['BirthCertType']['hk2']?$langArr['en']['Admission']['BirthCertType']['hk2'].' '.$langArr['b5']['Admission']['BirthCertType']['hk2']:$langArr['en']['Admission']['BirthCertType']['others2'].' '.$langArr['b5']['Admission']['BirthCertType']['others2']) ?>) <?= $StudentInfo['BirthCertNo'] ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span>Ethnicity 種族</span><br/>
                    <p><?= $StudentInfo['Province'] ?></p>
                </td>
                <td colspan="2">
                    <span>First Language 母語</span>
                    <p><?= $StudentInfo['LangSpokenAtHome'] ?></p>
                </td>
                <td colspan="2">
                    <span>Other Language(s) used by Student 其他語言</span>
                    <p><?= $StudentInfo['LangSpokenOther'] ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span>Religion 宗教</span>
                    <p><?= $StudentInfo['ReligionOther'] ?></p>
                </td>
                <td colspan="4">
                    <span>Church 所屬教會</span>
                    <p><?= $StudentInfo['Church'] ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="5" rowspan="3">
                    <span>Address 住址<br/>(in Chinese)(中文)</span>
                    <p class="m_b"><?= $StudentInfo['AddressChi'] ?></p>
                    <br/>
                    <span>(in English)(英文)</span>
                    <p><?= $StudentInfo['Address'] ?></p>
                </td>
                <td colspan="2">
                    <span>Res. Tel. 住宅電話</span>
                    <p><?= $StudentInfo['HomeTelNo'] ?></p>
                </td>
                <td colspan="3">
                    <span>Fax No. 傳真號碼</span>
                    <p><?= $StudentInfo['Fax'] ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <span>E-mail 電郵</span>
                    <p class="email"><?= $StudentInfo['Email'] ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <span>Contact Name / Mobile No. 聯絡人及電話</span>
                    <p><?= $StudentInfo['ContactPerson'] ?> / <?= $StudentInfo['ContactPersonRelationship'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="center" colspan="6">
                    <span>Name(s) of Previous School(s) (if applicable) 以前就讀學校名稱 (如適用)</span>
                </td>
                <td class="center" colspan="2">
                    <span>Duration 就讀年份</span>
                </td>
                <td class="center" colspan="2">
                    <span>Class 班級</span>
                </td>
            </tr>
            <tr>
                <td class="center" colspan="6">
                    <p><?= $StudentPrevSchoolInfo[0]['NameOfSchool'] ?></p>
                </td>
                <td class="center" colspan="2">
                    <p><?= $StudentPrevSchoolInfo[0]['Year'] ?></p>
                </td>
                <td class="center" colspan="2">
                    <p><?= $StudentPrevSchoolInfo[0]['Class'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <span>Parents<br/>家長</span>
                </td>
                <td class="label" colspan="3">
                    <span>Name<br/>姓名</span>
                </td>
                <td class="label" colspan="2">
                    <span>Occupation<br/>職業</span>
                </td>
                <td class="label" colspan="2">
                    <span>Office Address<br/>辦事處地址</span>
                </td>
                <td class="label">
                    <span>Office Tel.<br/>辦事處電話</span>
                </td>
                <td class="label">
                    <span>Mobile Tel.<br/>手提電話</span>
                </td>
            </tr>
            <tr>
                <td class="center middle" rowspan="2">
                    <span>Father<br/>父親</span>
                </td>
                <td class="br_0 middle">
                    <span>In Chinese<br/>中文</span>
                </td>
                <td class="middle" colspan="2">
                    <p><?= $ParentInfo['F']['ChineseName'] ?></p>
                </td>
                <td class="middle" colspan="2" rowspan="2">
                    <p><?= $ParentInfo['F']['Company'] ?></p>
                </td>
                <td class="middle" colspan="2" rowspan="2">
                    <p class="m_0 middle"><?= $ParentInfo['F']['OfficeAddress'] ?> </p>
                </td>
                <td class="center middle" rowspan="2">
                    <p><?= $ParentInfo['F']['OfficeTelNo'] ?></p>
                </td>
                <td class="center middle" rowspan="2">
                    <p><?= $ParentInfo['F']['Mobile'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="bl_0 br_0 middle">
                    <span>In English<br/>英文</span>
                </td>
                <td colspan="2" class="middle">
                    <p><?= $ParentInfo['F']['EnglishName'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="center middle" rowspan="2">
                    <span>Mother<br/>母親</span>
                </td>
                <td class="br_0 middle">
                    <span>In Chinese<br/>中文</span>
                </td>
                <td class="middle" colspan="2">
                    <p><?= $ParentInfo['M']['ChineseName'] ?></p>
                </td>
                <td class="middle" colspan="2" rowspan="2">
                    <p><?= $ParentInfo['M']['Company'] ?></p>
                </td>
                <td class="middle" colspan="2" rowspan="2">
                    <p class="m_0 middle"><?= $ParentInfo['M']['OfficeAddress'] ?> </p>
                </td>
                <td class="center middle" rowspan="2">
                    <p><?= $ParentInfo['M']['OfficeTelNo'] ?></p>
                </td>
                <td class="center middle" rowspan="2">
                    <p><?= $ParentInfo['M']['Mobile'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="bl_0 br_0 middle">
                    <span>In English<br/>英文</span>
                </td>
                <td colspan="2" class="middle">
                    <p><?= $ParentInfo['M']['EnglishName'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="center middle" rowspan="2">
                    <span>Guardian<br/>監護人</span>
                </td>
                <td class="br_0 middle">
                    <span>In Chinese<br/>中文</span>
                </td>
                <td class="middle" colspan="2">
                    <p><?= $ParentInfo['G']['ChineseName'] ?></p>
                </td>
                <td class="middle" colspan="2" rowspan="2">
                    <p><?= $ParentInfo['G']['Company'] ?></p>
                </td>
                <td class="middle" colspan="2" rowspan="2">
                    <p class="m_0 middle"><?= $ParentInfo['G']['OfficeAddress'] ?> </p>
                </td>
                <td class="center middle" rowspan="2">
                    <p><?= $ParentInfo['G']['OfficeTelNo'] ?></p>
                </td>
                <td class="center middle" rowspan="2">
                    <p><?= $ParentInfo['G']['Mobile'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="bl_0 br_0 middle">
                    <span>In English<br/>英文</span>
                </td>
                <td colspan="2" class="middle">
                    <p><?= $ParentInfo['G']['EnglishName'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="label middle" colspan="6">
                    <span>Name(s) of Brother(s) and Sister(s)<br/>Studied / Currently Studying in Our School<br/>曾在 / 現在就讀啟本校之兄姊姓名</span>
                </td>
                <td class="label middle">
                    <span>Sex<br/>性別</span>
                </td>
                <td class="label middle" colspan="2">
                    <span>Duration<br/>就讀年份</span>
                </td>
                <td class="label middle">
                    <span>Class<br/>現讀級別</span>
                </td>
            </tr>
            <tr>
                <td class="center middle" colspan="6">
                    <p><?= $SiblingInfo[0]['ChineseName'] ?></p>
                </td>
                <td class="center middle">
                    <?php if ($SiblingInfo[0]['Gender'] == 'F'): ?>
                        <p>Female 女</p>
                    <?php elseif ($SiblingInfo[0]['Gender'] == 'M'): ?>
                        <p>Male 男</p>
                    <?php endif; ?>
                </td>
                <td class="center middle" colspan="2">
                    <p><?= $SiblingInfo[0]['Year'] ?></p>
                </td>
                <td class="center middle">
                    <p><?= $SiblingInfo[0]['Class'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="center middle" colspan="6">
                    <p><?= $SiblingInfo[1]['ChineseName'] ?></p>
                </td>
                <td class="center middle">
                    <?php if ($SiblingInfo[1]['Gender'] == 'F'): ?>
                        <p>Female 女</p>
                    <?php elseif ($SiblingInfo[1]['Gender'] == 'M'): ?>
                        <p>Male 男</p>
                    <?php endif; ?>
                </td>
                <td class="center middle" colspan="2">
                    <p><?= $SiblingInfo[1]['Year'] ?></p>
                </td>
                <td class="center middle">
                    <p><?= $SiblingInfo[1]['Class'] ?></p>
                </td>
            </tr>
            <tr>
                <td class="label" colspan="5">
                    <span>Applied For 現欲申請</span>
                </td>
                <td class="label" colspan="5">
                    <span><?=$remarksnameeng?> <?=$remarksnamechi?></span>
                </td>
            </tr>
            <tr>
                <td class="applied" colspan="5">
                    <?php if ($OtherInfo[0]['ApplyDayType1'] && !$OtherInfo[0]['ApplyDayType2'] && !$OtherInfo[0]['ApplyDayType3']): ?>
                        <p>
                            <?= $classLevelName ?>
                            <?= $langArr['en']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType1']] ?>
                            <?= $langArr['b5']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType1']] ?>
                        </p>
                    <?php else: ?>
                        <p>
                            First Choice
                            <?= $classLevelName ?>
                            <?= $langArr['en']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType1']] ?>
                            <br/>
                            第一志願
                            <?= $classLevelName ?>
                            <?= $langArr['b5']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType1']] ?>
                        </p>
                        <?php if ($OtherInfo[0]['ApplyDayType2']): ?>
                            <p>
                                Second Choice
                                <?= $classLevelName ?>
                                <?= $langArr['en']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType2']] ?>
                                <br/>
                                第二志願
                                <?= $classLevelName ?>
                                <?= $langArr['b5']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType2']] ?>
                            </p>
                        <?php endif; ?>
                        <?php if ($OtherInfo[0]['ApplyDayType3']): ?>
                            <p>
                                Third Choice
                                <?= $classLevelName ?>
                                <?= $langArr['en']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType3']] ?>
                                <br/>
                                第三志願
                                <?= $classLevelName ?>
                                <?= $langArr['b5']['Admission']['TimeSlot'][$OtherInfo[0]['ApplyDayType3']] ?>
                            </p>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
                <td class="remarks" colspan="5" style="height:120px">
                    <p><?= nl2br($remarks) ?></p>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>