<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>
<style>
.page-break	{display:block; page-break-before:always;}
.page-wrapper {height:29.7cm; width:21cm; margin:0 auto; border:1px solid #fff;}
.page-content {margin:1cm 2cm 0.5cm 2cm; height:28.2cm; position:relative;}

body {background-color: #fff; color:#000; font-family: Calibri, Arial, Helvetica, Verdana, sans-serif, "msjh"; font-size: 12px; line-height: 1.2em; margin:0;  padding:0;}

#content {background-color: #fff; margin: 0 auto; width: 210mm;}
div.page_wrapper {padding: 0;}

p {margin: 0; padding: 0.5mm;}
span {padding: 0.5mm 0;}
tr.control td {height: 0; line-height: 0; padding: 0;}
table {border-collapse: separate; border-spacing: 0; padding: 0; table-layout: fixed;} /*for IE*/
th {border: 1px solid #000; border-right: none; font-weight: normal; padding: 0.5mm 0.8mm; text-align: center;}
th:last-child {border-right: 1px solid #000;}

/* Header style start */
.header {background: #e4f0d9; border-bottom: 1mm solid #4cb134; padding: 5mm 10mm 3mm; width: 210mm;}
.h_col1 {height: 0; padding: 0; margin: 0; width: 6mm;}
.h_col2 {height: 0; padding: 0; margin: 0; width: 20mm;}
.h_col3 {height: 0; padding: 0; margin: 0; width: 7mm;}
.h_col4 {height: 0; padding: 0; margin: 0; width: 22mm;}
.h_col5 {height: 0; padding: 0; margin: 0; width: 20mm;}
.h_col6 {height: 0; padding: 0; margin: 0; width: 10mm;}
.h_col7 {height: 0; padding: 0; margin: 0; width: 20mm;}
.h_col8 {height: 0; padding: 0; margin: 0; width: 85mm;}
.schName_zh {border-bottom: 1px solid #000; font-size: 2em; font-weight: bold; height: 12mm; letter-spacing: 2mm; padding: 0 0 0 1mm; vertical-align: middle;}
.schName2_zh {border-bottom: 1px solid #000; font-size: 1.5em; font-weight: bold; height: 12mm; letter-spacing: 2mm; line-height: 1em; padding: 0 3mm;}
.campuses_zh {border-bottom: 1px solid #000; font-size: 1.5em; font-weight: bold; height: 12mm; padding: 0 1mm 0 0; text-align: right;}
.schName_en {font-size: 1.3em; font-weight: bold; height: 10mm; letter-spacing: 0.4mm; padding-left: 1mm;}
.schName2_en {font-size: 1.3em; font-weight: bold; height: 10mm; letter-spacing: 0.4mm; line-height: 1.1em; padding: 0 1mm 0 2mm; word-spacing: 5mm;}
.campuses_en {font-size: 1.3em; font-weight: bold; letter-spacing: 0.15mm; padding: 0 1mm 0 2mm; vertical-align: top;}
.schLogo {padding: 2mm 0 0 0; text-align: center; vertical-align: top;}
.schAddress {font-size: 1.1em; padding: 0 0 0 6mm; vertical-align: top;}
.schAddress p {margin:  0 0 2px;}
/* Header style end */

/* Title style start */
.tbl_formTitle {margin-top: 0mm; padding: 0 10mm; width: 210mm;}
.tbl_formTitle td {font-size: 1.5em; font-weight: bold; line-height: 1.2em; text-align: center;}
.tbl_formTitle .regNo {font-size: 1.2em; font-weight: normal; text-align: right;}
.tbl_formTitle .no {font-size: 0.5em;}
/* Title style end */

/* Form style start */
.tbl_form {border: 2px solid #000; margin: 1mm 10mm 0;}
.tbl_form .tbl_col10 {border: 0; height: 0; padding: 0; width: 10mm;}
.tbl_form .tbl_col15 {border: 0; height: 0; padding: 0; width: 15mm;}
.tbl_form .tbl_col20 {border: 0; height: 0; padding: 0; width: 20mm;}
.tbl_form .tbl_col35 {border: 0; height: 0; padding: 0; width: 35mm;}
.tbl_form .tbl_col40 {border: 0; height: 0; padding: 0; width: 40mm;}
.tbl_form td {border-right: 1px solid #000; border-top: 1px solid #000; padding: 1mm; vertical-align: top;}
.tbl_form td:first-child {border-left: 1px solid #000;}
.tbl_form td:first-child:last-child {border-left: 0;}
.tbl_form p {font-size: 1.2em; margin: 0; padding: 0.7mm 0;}
.tbl_form .photo {padding: 0; vertical-align: middle;}
.photo {text-align: center; vertical-align: middle;}
.photo img {height: auto; max-height: 49mm; max-width: 50mm; width: auto;}
.tbl_form .m_b {margin-bottom: 2mm;}
.tbl_form .email {word-break: break-all;}
.tbl_form td.center {padding: 1mm; text-align: center;}
.tbl_form td.middle {vertical-align: middle;}
.tbl_form td.label {border-top: 3px solid #000; text-align: center;}
.tbl_form td.bl_0 {border-left: 0;}
.tbl_form td.br_0 {border-right: 0;}
.tbl_form .applied {border-bottom: 1px solid #000; padding: 1.5mm 5mm;}
.applied p {line-height: 1.2em;}
.tbl_form .remarks {border-bottom: 1px solid #000; padding: 1.5mm 5mm;}
.remarks p {line-height: 1.2em;}
/* Form style end */
</style>