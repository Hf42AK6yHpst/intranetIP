
<table cellpadding="0" cellspacing="0" class="header" style="padding-top: 8mm;">
	<tr>
		<td class="h_col1"></td>
		<td class="h_col2"></td>
		<td class="h_col3"></td>
		<td class="h_col4"></td>
		<td class="h_col5"></td>
		<td class="h_col6"></td>
		<td class="h_col7"></td>
		<td class="h_col8"></td>
	</tr>
	<tr>
		<td rowspan="3">&nbsp;</td>
		<td class="schName_zh">啟思</td>
		<td class="schName2_zh" colspan="2">幼稚園<br />幼兒園</td>
		<td class="campuses_zh">(深灣軒)</td>
		<td>&nbsp;</td>
		<td class="schLogo center" rowspan="3">
			<img src="<?=$logo ?>" alt="logo" height="95" width="53" />
		</td>
		<td class="schAddress" rowspan="3">
			<p>香港鴨脷洲鴨脷洲徑 3 號深灣軒地下</p>
			<p>G/F., Sham Wan Towers, No. 3 Ap Lei Chau Drive, </p>
			<p>Ap Lei Chau, Hong Kong</p>
			<p>電話： 2873 2128</p>
			<p>傳真： 2873 2277</p>
			<p>網址： www.creative.edu.hk</p>
		</td>
	</tr>
	<tr>
		<td class="schName_en" colspan="2">CREATIVE</td>
		<td class="schName2_en" colspan="2">KINDERGARTEN<br />DAY NURSERY</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td class="campuses_en" colspan="2">(Sham Wan Towers)</td>
	</tr>
</table>