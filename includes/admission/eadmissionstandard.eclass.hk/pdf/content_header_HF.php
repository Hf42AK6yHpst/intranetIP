
<table cellpadding="0" cellspacing="0" class="header" style="padding-top: 8mm;">
	<tr>
		<td class="h_col1"></td>
		<td class="h_col2"></td>
		<td class="h_col3"></td>
		<td class="h_col4"></td>
		<td class="h_col5"></td>
		<td class="h_col6"></td>
		<td class="h_col7"></td>
		<td class="h_col8"></td>
	</tr>
	<tr>
		<td rowspan="3">&nbsp;</td>
		<td class="schName_zh">啟思</td>
		<td class="schName2_zh" colspan="2">幼稚園<br />幼兒園</td>
		<td class="campuses_zh">(杏花邨)</td>
		<td>&nbsp;</td>
		<td class="schLogo center" rowspan="3">
			<img src="<?=$logo ?>" alt="logo" height="95" width="53" />
		</td>
		<td class="schAddress" rowspan="3">
			<p>香港柴灣杏花邨盛泰道 100 號</p>
			<p>100 Shing Tai Road, Heng Fa Chuen, Hong Kong</p>
			<p></p>
			<p>電話： 2595 0638</p>
			<p>傳真： 2595 0633</p>
			<p>網址： www.creative.edu.hk</p>
		</td>
	</tr>
	<tr>
		<td class="schName_en" colspan="2">CREATIVE</td>
		<td class="schName2_en" colspan="2">KINDERGARTEN<br />DAY NURSERY</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td class="campuses_en" colspan="2">(Heng Fa Chuen)</td>
	</tr>
</table>