<?php
//using:
/*
 * This page is for admission only. For general KIS config : kis/config.php
 */


//Please also define lang in admission_lang
$admission_cfg['Status'] = array();
$admission_cfg['Status']['pending'] = 1;
$admission_cfg['Status']['paymentsettled'] = 2;
$admission_cfg['Status']['firstinterview'] = 3;
$admission_cfg['Status']['secondinterview'] = 6;
$admission_cfg['Status']['thirdinterview'] = 7;
$admission_cfg['Status']['absentforinterview'] = 9;
$admission_cfg['Status']['admitted'] = 4;
$admission_cfg['Status']['notadmitted'] = 8;
$admission_cfg['Status']['cancelled'] = 5;

$admission_cfg['PaymentStatus']['OnlinePayment'] = 1;
$admission_cfg['PaymentStatus']['OtherPayment'] = 2;

//$admission_cfg['InterviewStatus']['AlreadyInterviewed'] = 1;
//$admission_cfg['InterviewStatus']['Absent1'] = 2;
//$admission_cfg['InterviewStatus']['Absent2'] = 3;
//$admission_cfg['InterviewStatus']['Withdrawn'] = 4;
//$admission_cfg['InterviewStatus']['NotUpdated'] = 5;
//
$admission_cfg['StatusDisplayOnTable'][1] = 'pending';
$admission_cfg['StatusDisplayOnTable'][2] = 'paymentsettled';
$admission_cfg['StatusDisplayOnTable'][3] = 'admitted';
//$admission_cfg['AdmitStatus']['WaitListed'] = 2;
//$admission_cfg['AdmitStatus']['NotAdmitted'] = 3;
//$admission_cfg['AdmitStatus']['NotUpdated'] = 4;


 	$admission_cfg['BirthCertType'] = array();
 	$admission_cfg['BirthCertType']['hk2']	= 1;
 	$admission_cfg['BirthCertType']['others2']	= 2;

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '幼稚園';
$admission_cfg['SchoolName']['en'] = 'Kindergarten';
//$admission_cfg['SchoolCode'] = 'CK';
//$admission_cfg['SchoolPhone'] = '2336 1139';
//$admission_cfg['SchoolAddress']['b5'] = '九龍塘律倫街6號';
//$admission_cfg['SchoolAddress']['en'] = 'No.6, Rutland Quadrant, Kowloon Tong, Kowloon';

$admission_cfg['themeStyle'] = 'Kindergarten';
// 	$admission_cfg['themeStyle'] = 'Primary';
// 	$admission_cfg['themeStyle'] = 'Secondary';
// ####### Cust config END ########


/* for email [start] */
if ($plugin['eAdmission_devMode']) {
//    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
    $admission_cfg['EmailBcc'] = '';
}else{
//    $admission_cfg['EmailBcc'] = 'ckrq.photo@gmail.com';
}
/* for email [end] */

/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//    $admission_cfg['paypal_signature'] = '_i8XCVFFMmTJiYwYPjlKyVOyYj7s9pzirXjGK6XNmazwxmB-_2nqbrwYz7m';
//    $admission_cfg['hosted_button_id'] = 'D3PTYR6DFG45J';
//    $admission_cfg['paypal_name'] = "TESTING Creative Day Nursery";
} else {
    $admission_cfg['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
//    $admission_cfg['paypal_signature'] = 'eFuxJPDc_tfLo5QrpkuUy5GuxCc1lO2UNQx-0YWBaNsVR2tUjIQ_6CTbcUq';
//    $admission_cfg['hosted_button_id'] = '7JF3WF66BNCMJ';
//    $admission_cfg['paypal_name'] = 'Creative Day Nursery';
}
/* for paypal [End] */


$admission_cfg['PrintByPDF'] = 1;
$admission_cfg['FilePath'] = $PATH_WRT_ROOT . "/file/admission/";
$admission_cfg['FilePathKey'] = "KSb9jmFSXrHSfWSXy";
$admission_cfg['DefaultLang'] = "b5";
$admission_cfg['Lang'] = array();
$admission_cfg['Lang'][1] = 'en';
$admission_cfg['Lang'][2] = 'b5';
$admission_cfg['MultipleLang'] = count($admission_cfg['Lang']) > 1;
$admission_cfg['HideSelectLang'] = true;
$admission_cfg['IsBilingual'] = true;
$admission_cfg['maxUploadSize'] = 5; //in MB
//	$admission_cfg['maxSubmitForm'] = 6000; //for whole year
$admission_cfg['personal_photo_width'] = 200;
$admission_cfg['personal_photo_height'] = 260;

$admission_cfg['EnableMandrill'] = true;
$admission_cfg['MandrillApikey'] = 'X0-f0ZPKJKGaEyDs0EtJ5Q';
$admission_cfg['MandrillMonthlyQuota'] = 5000;

if ($plugin['eAdmission_devMode']) {
    $admission_cfg['IntegratedCentralServer'] = "http://192.168.0.146:31002/test/queue/";
} else {
    $admission_cfg['IntegratedCentralServer'] = "https://eadmission.eclasscloud.hk/";
}