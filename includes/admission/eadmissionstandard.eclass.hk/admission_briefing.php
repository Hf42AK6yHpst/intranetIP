<?php

class admission_briefing extends admission_briefing_base{
	function generateApplicantID($schoolYearID){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$schoolYearID)), -2);
		
		$sql = "SELECT 
			MAX(ABAI.ApplicantID) 
		FROM 
			ADMISSION_BRIEFING_SETTING ABS
		INNER JOIN
			ADMISSION_BRIEFING_APPLICANT_INFO ABAI
		ON
			ABS.BriefingID = ABAI.BriefingID
		AND
		(
			ABS.SchoolYearID = '{$schoolYearID}'
		OR
			ABAI.ApplicantID LIKE 'B{$yearStart}%'
		)";
		$rs = $this->returnVector($sql);
		
		if($rs[0] == null){
			return "B{$yearStart}0001";
		}
		return 'B'.(((int)substr($rs[0], 1)) + 1);
	}
} // End Class