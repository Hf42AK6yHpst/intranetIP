<?php
# modifying by: 

/********************
 * 
 * Log :
 * Date		2014-01-15 [Carlos]
 * 			Modified this class to extends from the base class admission_ui_cust_base
 * 			Modified getDocsUploadForm() to follow attachment settings
 * 			Moved getDocsUploadForm() to libadmission_ui_cust_base.php
 * 			
 * Date		2013-10-09 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_ui_cust_base.php");

class admission_ui_cust extends admission_ui_cust_base{
	public function __construct(){
		
	}
	
	function getWizardStepsUI($Step){
		global $Lang;
		
		$active_step1 ="";
		$active_step2 ="";
		$active_step3 ="";
		$active_step4 ="";
		$active_step5 ="";
		$active_step6 ="";
		$active_step7 ="";
		$href_step1 ="";
		$href_step2 ="";
		$href_step3 ="";
		$href_step4 ="";
		$href_step5 ="";
		$href_step6 ="";
		$href_step7 ="";
		
		switch ($Step) {
		    case 1:
		        $active_step1 ="active-step";
		        $href_step1 ='href="#"';
		        break;
		    case 2:
		    	$active_step1 ="completed-step";
		        $active_step2 ="active-step";
		        $href_step2 ='href="#"';
		        
		        break;
		    case 3:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="active-step";
		        $href_step3 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_instruction\', \'step_index\');"';
		        
		        break;
		    case 4:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="active-step";
		        $href_step4 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_input_form\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_input_form\', \'step_instruction\');"';
		       
		        break;
		    case 5:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="active-step";
		        $href_step5 ='href="#"';
		         $href_step2 ='href="javascript:goto(\'step_docs_upload\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_docs_upload\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_docs_upload\', \'step_input_form\');"';
		       
		        break;
		    case 6:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="active-step";
		        $href_step6 ='href="#"';
		        $href_step2 ='href="javascript:goto(\'step_confirm\', \'step_index\');"';
		        $href_step3 ='href="javascript:goto(\'step_confirm\', \'step_instruction\');"';
		        $href_step4 ='href="javascript:goto(\'step_confirm\', \'step_input_form\');"';
		        $href_step5 ='href="javascript:goto(\'step_confirm\', \'step_docs_upload\');"';
		        break;
		    case 7:
		    	$active_step1 ="completed-step";
		        $active_step2 ="completed-step";
		        $active_step3 ="completed-step";
		        $active_step4 ="completed-step";
		        $active_step5 ="completed-step";
		        $active_step6 ="completed-step";
		        $active_step7 ="last_step_completed";
		        break;
		}
		$x ='<div class="admission_board">
				<div class="wizard-steps">
					<!--<div class="'.$active_step1.'  first_step"><a href="#"><span>1</span>'.$Lang['Admission']['newApplication'].'</a></div>-->
					<!--<div class="'.$active_step2.' first_step"><a '.$href_step2.'><span>2</span>'.$Lang['Admission']['chooseClass'].'</a></div>-->
					<div class="'.$active_step3.' first_step"><a '.$href_step3.'><span>1</span>'.$Lang['Admission']['instruction'].'</a></div>
					<div class="'.$active_step4.'"><a '.$href_step4.'><span>2</span>'.$Lang['Admission']['personalInfo'].'</a></div>
					<div class="'.$active_step5.'"><a '.$href_step5.'><span>3</span>'.$Lang['Admission']['docsUpload'].'</a></div>
					<div class="'.$active_step6.'"><a '.$href_step6.'><span>4</span>'.$Lang['Admission']['confirmation'].'</a></div>
					<div class="'.$active_step7.' last_step"><span style="width:40px">'.$Lang['Admission']['finish'].'</span></div>
				</div>
				<p class="spacer"></p>';
		return $x;
	}
	
	function getIndexContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultfirstpagemessage']; //Henry 20131107
		}
		//$x = '<form name="form1" method="POST" action="choose_class.php">';
		$x .='<div class="notice_paper">
						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
                			<h1 class="notice_title">'.$Lang['Admission']['onlineApplication'].'</h1>
                		</div></div></div>
                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
                   		<div class="notice_content ">
                       		<div class="admission_content">
								'.$Instruction.'
                      		</div>';
                      		
//					if($libkis_admission->schoolYearID){
//						$x .='<div class="edit_bottom">
//								'.$this->GET_ACTION_BTN('New Application', "submit", "", "SubmitBtn", "", 0, "formbutton")
//								.'
//							</div>';
//					}
						
					$x .='<p class="spacer"></p>
                    	</div>';
                    	if($lac->schoolYearID){
							$x .= $this->getChooseClassForm();
                    	}
					$x .='</div></div></div>
                
                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
                </div></div></div></div>';
         
    	return $x;
	}

	function getInstructionContent($Instruction){
		global $Lang, $kis_lang, $libkis_admission, $lac;
//		$libkis = new kis('');
//		$libkis_admission = $libkis->loadApp('admission');
		if(!$Instruction){
			$Instruction = $Lang['Admission']['msg']['defaultinstructionpagemessage']; //Henry 20131107
		}
		$x = $this->getWizardStepsUI(3);
		//$x .= '<form name="form1" method="POST" action="input_info.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x .='<div class="notice_paper">
//						<div class="notice_paper_top"><div class="notice_paper_top_right"><div class="notice_paper_top_bg">
//                			<h1 class="notice_title">'.$Lang['Admission']['instruction'].'</h1>
//                		</div></div></div>
//                	<div class="notice_paper_content"><div class="notice_paper_content_right"><div class="notice_paper_content_bg">
//                   		<div class="notice_content ">
//                       		<div class="admission_content">
//                         		'.$Instruction.'
//                      		</div>';
         $x .= $Instruction;             		
					if($lac->schoolYearID){
						$x .='<div class="edit_bottom">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_instruction','step_index')", "SubmitBtn", "", 0, "formbutton").' '
								 .$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_instruction','step_input_form')", "SubmitBtn", "", 0, "formbutton")
								.'
								<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="New Application" />-->
								<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
								
							</div>';
					}
						
					$x .='<p class="spacer"></p>';
//                    $x .='</div>
//					</div></div></div>
//                
//                <div class="notice_paper_bottom"><div class="notice_paper_bottom_right"><div class="notice_paper_bottom_bg">
//                </div></div></div></div></div>';
				//$x .='</form>';
    	return $x;
	}
	
	function getChooseClassForm(){
		global $libkis_admission, $Lang, $lac;
		$class_level = $lac->getClassLevel();
		$application_setting = $lac->getApplicationSetting();
		
		$class_level_selection = "";
		//To get the class level which is available
		if($application_setting){
			$hasClassLevelApply = 0;
			foreach($application_setting as $key => $value){
				//debug_pr($value['StartDate']);
				if(date('Y-m-d') >= $value['StartDate'] && date('Y-m-d') <= $value['EndDate']){
					$hasClassLevelApply = 1;
					$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="" />
						<label for="status_'.$key.'">'.$value['ClassLevelName'].'</label>';
				}			
			}
			if($hasClassLevelApply == 0){
				$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
			}
		}
		else{ //Henry 20131107
			$class_level_selection .='<fieldset class="warning_box">
											<legend>'.$Lang['Admission']['warning'].'</legend>
											<ul>
												<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
											</ul>
										</fieldset>';
		}
		//$x = $this->getWizardStepsUI(2);
		//$x .= '<form name="form1" method="POST" action="instruction.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';
//		$x ='<table class="form_table">
//				  <tr>
//					<td class="field_title">'.$Lang['Admission']['class'].'</td>
//					<td >';
//					$x .= $class_level_selection;
////					<input type="radio" name="sus_status" value="1" id="status1" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status1">Nursery (K1)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status2" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status2">Lower (K2)</label><br />
////					<input type="radio" name="sus_status" value="1" id="status3" checked="checked" onclick="js_show_email_notification(this.value);" />
////					<label for="status3">Upper (K3)</label>
//					$x.='</td>
//				  </tr>
//				  <col class="field_title" />
//				  <col  class="field_c" />
//				</table>';
				
		//The new UI 20131025 comment it for the ICMS
		$x .='<fieldset class="admission_select_class"><legend>'.$Lang['Admission']['level'].'</legend><div class="admission_select_option">';
		$x .= $class_level_selection;		
		$x .='</div></fieldset>';
			
		$x .='<div class="edit_bottom">
				'.($hasClassLevelApply == 1?$this->GET_ACTION_BTN($Lang['Admission']['newApplication'], "button", "goto('step_index','step_input_form')", "SubmitBtn", "", 0, "formbutton"):'')
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Next" />-->
				<!--<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="Cancel" />-->
			</div>';
		$x .='<p class="spacer"></p>';
			//$x .= '</form></div>';
            return $x;
	}
	
	function getApplicationForm(){
		global $fileData, $formData, $tempFolderPath, $Lang, $libkis_admission;
		
		//$x = '<form name="form1" method="POST" action="docs_upload.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		
		//$x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(4);
		$x .= $this->getStudentForm();
		$x .= $this->getParentForm();
		$x .= $this->getOthersForm();
		$x .='<span class="text_remark">'.$Lang['General']['RequiredField'].'</span>';
		$x .= '</div>
			<div class="edit_bottom">


				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_input_form','step_instruction')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_input_form','step_docs_upload')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'choose_class.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
			</div>
			<p class="spacer"></p>';
		//$x .= '</form>';
		//$x .='</div>';
		return $x;
	}
	
	function getStudentForm($IsConfirm=0){
		global $fileData, $formData, $Lang, $religion_selection,$lac, $admission_cfg;
		
		$religion_selected = $lac->returnPresetCodeName("RELIGION", $formData['StudentReligion']);
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['icms']['personaldetails'].'</h1>
			<table class="form_table" style="font-size: 13px">
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['studentssurname'].'</td>
				<td>'.($IsConfirm?$formData['studentssurname']:'<input name="studentssurname" type="text" id="studentssurname" class="textboxtext" />').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['givennames'].'</td>
				<td>'.($IsConfirm?$formData['studentsfirstname']:'<input name="studentsfirstname" type="text" id="studentsfirstname" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['icms']['nameinchinese'].'</td>
				<td>'.($IsConfirm?$formData['nameinchinese']:'<input name="nameinchinese" type="text" id="nameinchinese" class="textboxtext" maxlength="10" size="15"/>').'</td>
				<td class="field_title">'.$star.$Lang['Admission']['gender'].'</td>
				<td>';
				if($IsConfirm){
					$x .= $Lang['Admission']['genderType'][$formData['StudentGender']];
				}
				else{
					$x .=$this->Get_Radio_Button('StudentGender1', 'StudentGender', 'M', '0','',$Lang['Admission']['genderType']['M']).'&nbsp;&nbsp;'
						.$this->Get_Radio_Button('StudentGender2', 'StudentGender', 'F', '0','',$Lang['Admission']['genderType']['F']);
				}
				
				$x .='</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateofbirth'].'</td>
				<td>
				'.($IsConfirm?$formData['StudentDateOfBirth']:'<input name="StudentDateOfBirth" type="text" id="StudentDateOfBirth" class="textboxtext" maxlength="10" size="15"/>(YYYY-MM-DD)').'</td>
				<td class="field_title">'.$Lang['Admission']['icms']['knowUsBy'].'</td>
				<td>'.($IsConfirm?$formData['knowUsBy']:'<input name="knowUsBy" type="text" id="knowUsBy" class="textboxtext" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['icms']['schoolcurrentlyattending'].'</td>
				<td colspan="2">'.($IsConfirm?$formData['schoolcurrentlyattending']:'<input name="schoolcurrentlyattending" type="text" id="schoolcurrentlyattending" class="textboxtext" />').'</td>
			</tr>	
			<tr>
				<td class="field_title">'.$Lang['Admission']['icms']['nameoffather'].'</td>
				<td>'.($IsConfirm?$formData['nameoffather']:'<input name="nameoffather" type="text" id="nameoffather" class="textboxtext" />').'</td>
				<td colspan="2">
					<table style="font-size: 13px">
						<tr><td class="field_title">'.$Lang['Admission']['icms']['mobilenumber'].'</td>
							<td>'.($IsConfirm?$formData['G1MobileNo']:'<input name="G1MobileNo" type="text" id="G1MobileNo" class="textboxtext" />').'</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['Admission']['icms']['occupation'].'</td>
						<td>'.($IsConfirm?$formData['G1Occupation']:'<input name="G1Occupation" type="text" id="G1Occupation" class="textboxtext" />').'</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['icms']['nameofmother'].'</td>
				<td>'.($IsConfirm?$formData['nameofmother']:'<input name="nameofmother" type="text" id="nameofmother" class="textboxtext" />').'</td>
				<td colspan="2">
					<table style="font-size: 13px" >
						<tr><td class="field_title">'.$Lang['Admission']['icms']['mobilenumber'].'</td>
							<td>'.($IsConfirm?$formData['G2MobileNo']:'<input name="G2MobileNo" type="text" id="G2MobileNo" class="textboxtext" />').'</td>
						</tr>
						<tr>
							<td class="field_title">'.$Lang['Admission']['icms']['occupation'].'</td>
						<td>'.($IsConfirm?$formData['G2Occupation']:'<input name="G2Occupation" type="text" id="G2Occupation" class="textboxtext" />').'</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="field_title">'.$Lang['Admission']['icms']['primaryemail'].'</td>
				<td >'.($IsConfirm?$formData['primaryemail']:'<input name="primaryemail" type="text" id="primaryemail" class="textboxtext" />').'</td>
				<td class="field_title">'.$Lang['Admission']['icms']['homenumber'].'</td>
				<td >'.($IsConfirm?$formData['homenumber']:'<input name="homenumber" type="text" id="homenumber" class="textboxtext" />').'</td>
			</tr>	
		
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['homeaddress'].'</td>
				<td colspan="3">'.($IsConfirm?$formData['homeaddress']:'<input type="text" name="homeaddress" class="textboxtext" id="homeaddress" />').'</td>
			</tr>
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['personalPhoto'].'</td>
				<td colspan="3">'.($IsConfirm?stripslashes($fileData['StudentPersonalPhoto']):'<input type="file" name="StudentPersonalPhoto" id="StudentPersonalPhoto" accept="image/gif, image/jpeg, image/jpg, image/png"/><br />
					<em>('.$Lang['Admission']['msg']['personalPhotoFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB)</em>
					<br />').'
				</td>
			</tr>
		</table>';
		
		return $x;
	}
	
	function getParentForm($IsConfirm=0){
		global $fileData, $formData, $Lang;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['icms']['persontoaccompany'].'</h1>
				<table class="form_table" style="font-size: 13px">
				<tr>
					<td>&nbsp;</td>
					<td class="form_guardian_head" style="text-align:center">1</td>
					<td class="form_guardian_head" style="text-align:center">2</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['name'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyname1']:'<input name="accompanyname1" type="text" id="accompanyname1" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyname2']:'<input name="accompanyname2" type="text" id="accompanyname2" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['relationship'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyrelationship1']:'<input name="accompanyrelationship1" type="text" id="accompanyrelationship1" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanyrelationship2']:'<input name="accompanyrelationship2" type="text" id="accompanyrelationship2" class="textboxtext" />').'</td>
				</tr>
				<tr>
					<td class="field_title">'.$Lang['Admission']['icms']['mobilenumber'].'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanymobile1']:'<input name="accompanymobile1" type="text" id="accompanymobile1" class="textboxtext" />').'</td>
					<td class="form_guardian_field">'.($IsConfirm?$formData['accompanymobile2']:'<input name="accompanymobile2" type="text" id="accompanymobile2" class="textboxtext" />').'</td>
				</tr>
				
			</table>';
			
		return $x;
	}
	
	function getOthersForm($IsConfirm=0){ //should be changed the method of selecting program
		global $admission_cfg, $Lang, $libkis_admission, $fileData, $formData, 	$lac, $kis_lang;
		
		$admission_year = getAcademicYearByAcademicYearID($lac->getNextSchoolYearID());

		$applicationSetting = $lac->getApplicationSetting();
		
		$dayType = $applicationSetting[$_REQUEST['hidden_class']]['DayType'];
		$dayTypeArr = explode(',',$dayType);
		
		$dayTypeOption="";
		
		if($IsConfirm){
			$classLevel = $lac->getClassLevel($formData['sus_status']);
			$classLevel = $classLevel[$formData['sus_status']];

			for($i=1; $i<=3; $i++){
				if($formData['OthersApplyDayType'.$i]){
					$dayTypeOption .= "(".$kis_lang['Admission']['Option']." ".$i.") ".($Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]?$Lang['Admission']['TimeSlot'][$formData['OthersApplyDayType'.$i]]:' -- ')." ";
				}
			}

		}
		else{
			foreach($dayTypeArr as $aDayType){
				$dayTypeOption .= $Lang['Admission']['TimeSlot'][$aDayType]." ".$this->Get_Number_Selection('OthersApplyDayType'.$aDayType, '1', count($dayTypeArr))." ";
			}
			 $dayTypeOption .='('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
		}
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		$x = '<h1>'.$Lang['Admission']['icms']['programselection'].'</h1>
			<table class="form_table" style="font-size: 13px">
			
			<tr>
				<td class="field_title">'.$star.$Lang['Admission']['dateOfEntry'].'</td>
				<td><table style="font-size: 13px">
					<tr><td width="40px">('.$Lang['General']['SchoolYear'].')</td><td>'.($IsConfirm?$admission_year:$admission_year).'</td></tr>
					<tr><td width="40px">('.$Lang['Admission']['month'].')</td><td>'.($IsConfirm?$formData['OthersApplyMonth']:$this->Get_Number_Selection('OthersApplyMonth', '1', '12')).'</td></tr>
					</table>
				<!--'.$this->GET_DATE_PICKER('OthersApplyDate').'--></td>
				<td class="field_title">'.$star.$Lang['Admission']['applyTerm'].'</td>
				<td>';
				if($IsConfirm){
					$x .=$Lang['Admission']['Term'][$formData['OthersApplyTerm']];
				}
				else{
					$x .=$this->Get_Radio_Button('OthersApplyTerm1', 'OthersApplyTerm', '1', '0','',$Lang['Admission']['Term'][1]).' '
					.$this->Get_Radio_Button('OthersApplyTerm2', 'OthersApplyTerm', '2', '0','',$Lang['Admission']['Term'][2]);
				}
			$x .='</td>
			</tr>
			<!--<tr>
				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>
				<td '.($IsConfirm?'':'colspan="3"').'>
				<div id="DayTypeOption">'.$dayTypeOption.'</div></td>
				'.($IsConfirm?'<td class="field_title">'.$Lang['Admission']['applyLevel'].'</td><td>'.$classLevel.'</td>':'').'
			</tr>-->';
			
			$class_level_selection = "";
			//To get the class level which is available
			if($applicationSetting){
				$hasClassLevelApply = 0;
				foreach($applicationSetting as $key => $value){
					//debug_pr($value['StartDate']);
					if(date('Y-m-d') >= $value['StartDate'] && date('Y-m-d') <= $value['EndDate']){
						$hasClassLevelApply = 1;
						$class_level_selection .= '<input type="radio" name="sus_status" value="'.$key.'" id="status_'.$key.'" onclick="" />
							<label for="status_'.$key.'">'.$value['ClassLevelName'].'</label>';
					}			
				}
				if($hasClassLevelApply == 0){
					$class_level_selection .='<fieldset class="warning_box">
												<legend>'.$Lang['Admission']['warning'].'</legend>
												<ul>
													<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
												</ul>
											</fieldset>';
				}
			}
			else{ //Henry 20131107
				$class_level_selection .='<fieldset class="warning_box">
												<legend>'.$Lang['Admission']['warning'].'</legend>
												<ul>
													<li>'.$Lang['Admission']['msg']['noclasslevelapply'].'</li>
												</ul>
											</fieldset>';
			}
			
			//loop the class level here...
			$dayTypeOption = '';
			foreach($applicationSetting as $key => $value){
				$classLevelNameAry[] = array($key,$value['ClassLevelName']);
			}
			for($i=0;$i<count($applicationSetting);$i++){
				$dayTypeOption .=$kis_lang['Admission']['Option'].' '.($i+1).' '.$this->GET_SELECTION_BOX($classLevelNameAry, "name='OthersApplyDayType".($i+1)."' id='OthersApplyDayType".($i+1)."' class='timeslotselection'",(count($applicationSetting)==1?'':$kis_lang['Admission']['Nil']));
				//if(($i+1)%3 == 0)
					$dayTypeOption .= '<br/>';
			}
			$dayTypeOption .='<br/>('.$Lang['Admission']['msg']['applyDayTypeHints'].')';
			$x .='<tr>
				<td class="field_title">'.$star.$Lang['Admission']['applyDayType'].'</td>
				<td '.($IsConfirm?'':'colspan="3"').'>'.$dayTypeOption.'</td></tr>';
			//loop the class level here...
			
			$x .='</table>';
		
		return $x;
	}
	/*
	function getDocsUploadForm($IsConfirm=0){
		global $tempFolderPath, $Lang, $fileData, $admission_cfg;
		
		$star = $IsConfirm?'':'<font style="color:red;">*</font>';
		
		if(!$IsConfirm){
		$x .= $this->getWizardStepsUI(5);
		}
		else{
			$x .='<h1>'.$Lang['Admission']['docsUpload'].'</h1>';
		}
		$x .='<table class="form_table">';
		if(!$IsConfirm){
			$x .='<tr>
					<td colspan="2">'.$Lang['Admission']['document'].' <span class="date_time">('.$Lang['Admission']['msg']['birthCertFormat'].($admission_cfg['maxUploadSize']?$admission_cfg['maxUploadSize']:'1').' MB) </span></td>
				</tr>';
		}
			
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['birthCert'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile']):'<input type="file" name="OtherFile" id="OtherFile" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		$x .='<tr>
					<td class="field_title">'.$star.$Lang['Admission']['immunisationRecord'].'</td>
					<td>'.($IsConfirm?stripslashes($fileData['OtherFile1']):'<input type="file" name="OtherFile1" id="OtherFile1" value="Add" class="" accept="image/gif, image/jpeg, image/jpg, image/png, application/pdf" />').'</td>
			</tr>';
		
		$x .='</td>
				</tr>
				<!--<tr>
					<td colspan="2">Admission Fee (<span class="acc_no">HKD$50</span>) </td>
				</tr>
				<tr>
					<td class="field_title">Payment Method</td>
					<td><label><span>
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					<img src="../../../images/icon_paypal.png" alt="" align="absmiddle" /> </span> <span class="selected">
					<input style="background:none; border:none;" type="radio" name="radio" id="radio" value="radio" />
					Bank Deposit </span> </label></td>
				</tr>
				<tr>
					<td class="field_title">XXX ?</td>
					<td>Please deposit to Broadlearning Education (Asia) Limited Standard Chartered Bank Account: <span class="acc_no">407-0-068474-3</span>, 
					and submit the  bank in receipt :<br />
					<input type="file" name="fileField" id="fileField" />
					<br />
					<em>(image in JPEG/GIF/PNG/PDF format, file size less than 10MB)</em>
					<br />
					</td>
				</tr>-->
			</table>';
		if(!$IsConfirm){
		$x .= '</div>
			<div class="edit_bottom">

				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_docs_upload','step_input_form')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "goto('step_docs_upload','step_confirm')", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'input_info.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'confirm.php\');return document.MM_returnValue" value="Next" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />
				
			</div>
			<p class="spacer"></p>';

		}
		return $x;
	}
	*/
	function getConfirmPageContent(){
		global $Lang, $fileData, $formData;
		
		//remove the slashes of the special character
		if($formData){
			foreach ($formData as $key=>$value) {
				$formData[$key] = stripslashes($value);
				if($formData[$key] == ""){
					$formData[$key] =" -- ";
				}
			}
		}
		
		//$x = '<form name="form1" method="POST" action="finish.php" onSubmit="return checkForm(this)" ENCTYPE="multipart/form-data">';     
		//x .= '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(6);
		
		$x .=$this->getStudentForm(1);
		$x .= $this->getParentForm(1);
		$x .= $this->getOthersForm(1);
		$x .= $this->getDocsUploadForm(1);
		$x .= '</div>
			<div class="edit_bottom">
				'.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goto('step_confirm','step_docs_upload')", "SubmitBtn", "", 0, "formbutton").' '
				.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "SubmitBtn", "", 0, "formbutton")
				.'
				<!--<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'docs_upload.php\');return document.MM_returnValue" value="Back" />
				<input type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'finish.php\');return document.MM_returnValue" value="Submit" />-->
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Cancel'].'" />

			</div>
			<p class="spacer"></p>';
			//$x .='</form></div>';
		return $x;
	}
	
	function getFinishPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac;
		$x = '<div class="admission_board">';
		$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';        
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		$x .= '<div class="edit_bottom">
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Admission']['finish'].'" />
			</div>
			<p class="spacer"></p></div>';
		return $x;
	}
	
	function getTimeOutPageContent($ApplicationID='', $LastContent=''){
		global $Lang, $lac;
		$x = '<div class="admission_board">';
		//$x .= $this->getWizardStepsUI(7);
		if($ApplicationID){
			
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissioncomplete'].'<span>'.$Lang['Admission']['msg']['yourapplicationno'].' '.$ApplicationID.'&nbsp;&nbsp;<input type="button" value="'.$Lang['Admission']['printsubmitform'].'" onclick="window.open(\''.$lac->getPrintLink("", $ApplicationID).'\',\'_blank\');"></input></span></h1>';           
		}
		else{
			$x .=' <div class="admission_complete_msg"><h1>'.$Lang['Admission']['msg']['admissionnotcomplete'] .'<span>'.$Lang['Admission']['msg']['tryagain'].'</span></h1>';
		}
		if(!$LastContent){
			$LastContent = $Lang['Admission']['msg']['defaultlastpagemessage']; //Henry 20131107
		}
		$x .= '<br/>'.$LastContent.'</div>';
		$x .= '</div>';
		$x .= '<div class="edit_bottom">
				<input type="button" class="formsubbutton" onclick="MM_goToURL(\'parent\',\'index.php\');return document.MM_returnValue" value="'.$Lang['Btn']['Back'].'" />
			</div>
			<p class="spacer"></p></div>';
		return $x;
	}

	function getPrintPageContent($schoolYearID,$applicationID, $type=""){ //using $type="teacher" if the form is print from teacher
		global $PATH_WRT_ROOT,$Lang,$kis_lang;
		include_once($PATH_WRT_ROOT."lang/admission_lang.b5.php");
		$lac = new admission_cust();
//		if($applicationID != ""){
		//get student information
		$studentInfo = current($lac->getApplicationStudentInfo($schoolYearID,'',$applicationID));
		$parentInfo = $lac->getApplicationParentInfo($schoolYearID,'',$applicationID);
		foreach($parentInfo as $aParent){
			if($aParent['type'] == 'F'){
				$fatherInfo = $aParent;
			}
			else if($aParent['type'] == 'M'){
				$motherInfo = $aParent;
			}
			else if($aParent['type'] == 'G'){
				$guardianInfo = $aParent;
			}
		}
		
		$othersInfo = current($lac->getApplicationOthersInfo($schoolYearID,'',$applicationID));
		if($_SESSION['UserType']==USERTYPE_STAFF){
			$remarkInfo = current($lac->getApplicationStatus($schoolYearID,'',$applicationID));
			if(!is_date_empty($remarkInfo['interviewdate'])){
				list($date,$hour,$min) = splitTime($remarkInfo['interviewdate']);
				list($y,$m,$d) = explode('-',$date);
				if($hour>12){
					$period = '下午';
					$hour -= 12;
				}elseif($hour<12){
					$period = '上午';
				}else{
					$period = '中午';
				}
				$hour = str_pad($hour,2,"0",STR_PAD_LEFT);
				$min = str_pad($hour,2,"0",STR_PAD_LEFT);
				$interviewdate = $y.'年'.$m.'月'.$d.'日<br/>'.$period.' '.$hour.' 時 '.$min.' 分';
			}else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
			}
		}
		else{
				$interviewdate = '＿＿＿＿年＿＿月＿＿日<br/>
			上午／下午____時____分';
		}
		$attachmentList = $lac->getAttachmentByApplicationID($schoolYearID,$applicationID);
		$personalPhotoPath = $attachmentList['personal_photo']['link'];
		$classLevel = $lac->getClassLevel();
		
//		debug_pr($studentInfo);
//		debug_pr($fatherInfo);
//		debug_pr($motherInfo);
//		debug_pr($guardianInfo);
//		debug_pr($othersInfo);
		
		for($i=1; $i<=3; $i++){
				if($othersInfo['ApplyDayType'.$i] != 0){
					//$dayTypeOption .= "(".$Lang['Admission']['Option']." ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
					$dayTypeOption .= "(選擇 ".$i.") ".$Lang['Admission']['TimeSlot'][$othersInfo['ApplyDayType'.$i]]."&nbsp;&nbsp;";
				}
			}
		
		//Header of the page
		$x = '<div class="input_form" style="width:720px;margin:auto;">';

$x .= '<h4 align="center">Island Children\'s Montessori House</h4>';

$x .= '<h5 align="center">International ◆ Preschool ◆ Education<br/>
		www.icms.edu.hk</h5>';

$x .= '<h3 align="center">Montessori Toddler Program (N1/N2SS/TOM)</h3>';

$x .= '<h3 align="center">2013-14 Application Form</h3>';

$x .= '<table style="width:720px;">
			<tr><td>';

$x .= '<table style="width:550px;" class="tg-table-plain">
			<tr>
				<td width="25%" style="padding:5px;vertical-align:middle;">For office use only Date received</td>
				<td width="25%" ><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">CSCI3250</td></tr><tr><td style="border:0">Class code</td></tr></table></td>
				<td width="25%" ><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">2000-01-01</td></tr><tr><td style="border:0">Start date</td></tr></table></td>
				<td width="25%" ><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">Testing...</td></tr><tr><td style="border:0">Notes</td></tr></table></td>
			</tr>
		</table>';
		
$x .='</td><td rowspan="2">
		<img src="smiley.gif" alt="personal photo" height="120px" width="90px">
	</td>';
	
$x .='</tr>
	<tr><td>';

$x .= '<table>
			<tr>
				<td><input type="checkbox" checked onclick="return false">Photographs</input></td>
				<td><input type="checkbox" checked onclick="return false">Application fee</input></td>
				<td><input type="checkbox" onclick="return false">Security Deposit</input></td>
				<td><input type="checkbox" onclick="return false">Postdated cheques:__________</input></td>
			</tr>
		</table>';

$x .='</td></tr></table>';

//$x .= '<h4><b>Section A</b> Personal Details</h4>';
$stuNameArr = explode(',',$studentInfo['student_name_en']);
$x .= '<div class="section_title"><b>Section A</b> Personal Details</div>';

$x .= '<table align="center" class="tg-table-plain">
		  <tr>
			<td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$stuNameArr[0].'</td></tr><tr><td style="border:0;font-size: 15px;">Student\'s surname</td></tr></table></td>
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$Lang['Admission']['genderType'][$studentInfo['gender']].'</td></tr><tr><td style="border:0">Gender</td></tr></table></td>
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['dateofbirth'].'</td></tr><tr><td style="border:0">Date of birth</td></tr></table></td>
		  </tr>
		  <tr class="tg-even">
		    <td colspan="3"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$stuNameArr[1].'</td></tr><tr><td style="border:0">Given names</td></tr></table></td>
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.($studentInfo['student_name_b5']?$studentInfo['student_name_b5']:'&nbsp;').'</td></tr><tr><td style="border:0">Name in Chinese (if applicable)</td></tr></table></td>
		  </tr>
		  <tr>
		    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.($studentInfo['lastschool']?$studentInfo['lastschool']:'&nbsp;').'</td></tr><tr><td style="border:0">School currently attending</td></tr></table></td>
		    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.($othersInfo['KnowUsByOther']?$othersInfo['KnowUsByOther']:'&nbsp;').'</td></tr><tr><td style="border:0">How did you hear about us?</td></tr></table></td>
		  </tr>
		  <tr class="tg-even">
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['nameoffather'].'</td></tr><tr><td style="border:0;font-size:15px">Name of father</td></tr></table></td>
		    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['mobile'].'</td></tr><tr><td style="border:0">Mobile number</td></tr></table></td>
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$fatherInfo['occupation'].'</td></tr><tr><td style="border:0">Occupation</td></tr></table></td>
		  </tr>
		  <tr>
		   <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['nameofmother'].'</td></tr><tr><td style="border:0;font-size:15px">Name of mother</td></tr></table></td>
		    <td colspan="2"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['mobile'].'</td></tr><tr><td style="border:0">Mobile number</td></tr></table></td>
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$motherInfo['occupation'].'</td></tr><tr><td style="border:0">Occupation</td></tr></table></td>
		  </tr>
		  <tr class="tg-even">
		    <td colspan="3"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['email'].'</td></tr><tr><td style="border:0">Primary email</td></tr></table></td>
		    <td><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['homephoneno'].'</td></tr><tr><td style="border:0">Home number</td></tr></table></td>
		  </tr>
		  <tr class="tg-even">
		    <td colspan="4"><table style="width:100%;margin:0;padding:0;border:0;"><tr><td class="answer" style="border:0">'.$studentInfo['homeaddress'].'</td></tr><tr><td style="border:0">Home address</td></tr></table></td>
		  </tr>
		</table>';

$x .= '<p>Person(s) to accompany your child to class: (only 1 adult per child is permitted to enter the class at any one time.)</p>';

$x .= '<table align="center" class="tg-table-plain tg-table-plain2">
			<tr>
				<td class="print_field_title_main"></td>
				<td class="print_field_title_main">Name</td>
				<td class="print_field_title_main">Relationship</td>
				<td class="print_field_title_main">Mobile number</td>
			</tr>
			<tr>
				<td>1</td>
				<td class="answer">'.$accompanyInfo['accompanyname1'].'</td>
				<td class="answer">'.$accompanyInfo['accompanyrelationship1'].'</td>
				<td class="answer">'.$accompanyInfo['accompanymobile1'].'</td>
			</tr>
			<tr>
				<td>2</td>
				<td>'.$accompanyInfo['accompanyname2'].'</td>
				<td>'.$accompanyInfo['accompanyrelationship2'].'</td>
				<td>'.$accompanyInfo['accompanymobile2'].'</td>
			</tr>		
		</table><br/>';

$x .= '<div class="section_title"><b>Section B</b> Program Selection</div>';

$x .= '<table align="center" class="tg-table-plain tg-table-plain2 center_text">
			<tr>
				<td class="print_field_title_main"></td>
				<td class="print_field_title_main">Program</td>
				<td class="print_field_title_main">Age</td>
				<td class="print_field_title_main">Minimum join time</td>
				<td class="print_field_title_main">Tuition Fee*</td>
			</tr>
			<tr>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td rowspan="2"><b>1.</b></td>
				<td><b>Toddler Playgroup (N1)</b></td>
				<td><b>18 to 24 months old</b></td>
				<td><b>36 classes</b></td>
				<td><b>$4,560* per instalment x 3 instalments</b></td>
			</tr>
			<tr style="page-break-after:always">
				<td><b>Adult supervision required</b><br/><br/>Toddler campus</td>
				<td style="text-align:left;"><u>Select preferred session</u><br/><br/>Admission for (mm/yy):<br/><br/><u><span class="answer">09/14</span></u></td>
				<td colspan="2" style="text-align:left;">
					<input type="checkbox" checked onclick="return false">9:00am - 10:30am Monday Wednesday Friday</input><br/>
					<input type="checkbox" onclick="return false">9:00am - 10:30am Tuesday Thursday Saturday</input><br/>
					<input type="checkbox" onclick="return false">11:00am - 12:30am Tuesday Thursday Saturday</input><br/>
				</td>
			</tr>
			<tr>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td rowspan="2"><b>2.</b></td>
				<td><b>Pre-Nursery Stepping Stone (PNSS)</b></td>
				<td><b>22 to 36 months old</b></td>
				<td><b>36 classes</b></td>
				<td><b>$4,800* per instalment x 3 instalments</b></td>
			</tr>
			<tr>
				<td><b>Limited adult supervision required</b><br/><br/>Toddler campus</td>
				<td style="text-align:left;"><u>One session only</u><br/><br/>Admission for (mm/yy):<br/><br/>____________</td>
				<td colspan="2" style="text-align:left;">
					<input type="checkbox" onclick="return false">11:00am - 1:00pm Monday Wednesday Friday</input>
				</td>
			</tr>
			<tr>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td rowspan="2"><b>3.</b></td>
				<td><b>Taste of Montessori (TOM)</b></td>
				<td><b>18 to 36 months old</b></td>
				<td><b>36 classes</b></td>
				<td><b>$4,560* per instalment x 3 instalments</b></td>
			</tr>
			<tr>
				<td><b>Adult supervision required</b><br/><br/>Toddler campus</td>
				<td style="text-align:left;"><u>One session only</u><br/><br/>Admission for (mm/yy):<br/><br/>____________</td>
				<td colspan="2" style="text-align:left;">
					<input type="checkbox" onclick="return false">3:00 - 4:30pm Monday Wednesday Friday</input>
				</td>
			</tr>
			<tr>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td rowspan="2"><b>4.</b></td>
				<td><b>Taste of Montessori (TOM Bilingual)</b></td>
				<td><b>18 to 36 months old</b></td>
				<td><b>12 classes</b></td>
				<td><b>$4,800* per instalment x 1 instalment</b></td>
			</tr>
			<tr>
				<td><b>Adult supervision required</b><br/><br/>Hong Kong East campus</td>
				<td style="text-align:left;"><u>One session only</u><br/><br/>Admission for (mm/yy):<br/><br/>____________</td>
				<td colspan="2" style="text-align:left;">
					<u>Check one or more boxes:</u><br/>
					<input type="checkbox" onclick="return false">Every Saturday 9:00am - 10:30am</input><br/>
					<input type="checkbox" onclick="return false">Every Saturday 11:00am - 12:30am</input>
				</td>
			</tr>
		</table>';

$x .= '<p>*Tuition fees remain unchanged until 31st August 2014, and will be subject to change 2014-15 academic year, beginning 1st September 2014.<br/>
		# Please submit a separate application form for Pre-Nursery Program, available from the school office.</p>';

$x .= '<div class="section_title"><b>Section C</b> Registration and Payment</div>';

$x .= '<h5><u>Return this application form along with payment, in person to the Main campus; or by post to:</u></h5>';

$x .= '<h5>Admissions<br/>Island Children\'s Montessori School,<br/>Units B&C, UG & LG floors<br/>16 Tin Hau Temple Road<br/>Hong Kong</h5>';

$x .= '<p>Please enclose the following with your application.</p>';

$x .= '<table>
			<tr>
				<td width="50%" style="border-bottom:1pt solid black;"><input type="checkbox" checked onclick="return false"><b>Toddler Playgroup (N1) or Taste of Montessori (TOM)</b></input></td>
				<td width="50%" style="border-bottom:1pt solid black;"><input type="checkbox" onclick="return false"><b>Pre-Nursery Steeping Stone (PNSS) or Taste of Montessori ( TOM Bilingual )</b></input></td>
			</tr>
			<tr>
				<td>
				- 1 passport size photo<br/>
				- Non-refundable Application Fee $500<br/>
				- Security Deposit $4560
				</td>
				<td>
				- 1 passport size photo<br/>
				- Non-refundable Application Fee $500<br/>
				- Security Deposit $4800
				</td>
			</tr>
		</table>';

$x .= '<p>Please write student name, program and session selected on the back of the cheque.<br/>
		<u>Cheque payable to: <b>Island Children\'s Montessori House</b></u></p>';

$x .= '<p>After submission of application, you will receive a Letter of Acceptance to confirm your child\'s placement and commencement date. Before the deadline started on the Letter of Acceptance, please send 3 post-dated cheques dated on the first day of each month, to cover tution fees for the 3 months that your child will be in the program.</p>';

$x .= '<table border="0">
			<tr>
				<td>Have questions?</td>
				<td>Toddler Campus: Contact us at 3188-9266, or by email <b><u>admin.park@icms.edu.hk</u></b></td>
			</tr>
			<tr>
				<td></td>
				<td>Hong Kong East Campus: Contact us at 3427-9100 or by email <b><u>admin.hktest@icms.edu.hk</u></b></td>
			</tr>
		</table>';

//$x .= '<h4><b>Section D</b> Terms of Enrolment</h4>';
//
//$x .= '<p>Enrolment and admission to Island Children\'s Montessori House (ICMH) are subject to the conditions outlined below.</p>';
//
//$x .= '<ol>
//			<li>Every child must be accompanied by one adult to all classes, and that adult must stay on ICMH premises while class is in session.</li>
//			<li>The minimum join time for each program is 36 classes.</li>
//			<li>Certificates will only be issued upon completion of the minimum 36 classes or more. A Certificate of Attendance will be issued to students who have completed 36 classes or more. A Certificate of Completion will be issued to students who have completed a minimum of 72 classes.</li>
//			<li>Tuition fees remain unchanged until 31st August 2014, and will be subject to change in the 2014-15 school year, beginning 1st September 2014.</li>
//			<li>All classes at ICMH continue to operate under these weather conditions:
//				<ul>
//					<li>Tropical Cyclone Signal 1 or 3;</li>
//					<li>Amber or red rainstorm warning.</li>
//				</ul>
//			</li>
//			<li>Class cancellations that are beyond the control of ICMH, such as Tropical Cyclone Signal 8 or above, black rain storm warning advice, EDB advice or other unforeseen circumstances will not result in refunds or makeup classes.</li>
//			<li>The commencement date can only be postponed ONCE within 2 months from the original commencement date. If after postponing the commencement date the perent chooses to withdraw from the program, neither the security deposit nor registration fee will be refunded.</li>
//			<li>Toddler Playgroup and Taste of Montessori Program (TOM) absences: Two (2) abseces (including illness or personal reasons) per month are allowed for these programs. Makeup classes must be taken within 30 days of absence. Due to class scheduling we cannot allow unused monthly allowable absent days be used or carried over to other months. There will be no fee refunds if makeup classes are not taken. It is the parent\'s responsibility to contact the teachers to arrange make-up classes.</li>		
//			<li>Stepping Stone Program absences: Two(2) absences (including illness or personal reasons) per month are allowed for Stepping Stone classes. Due to class scheduling we cannot allow unused monthly absent days be used or carried over to other months. Makeup classes will be taken at the end of the 36 classes. There will be no fee refunds if makeup classes are not taken. It is the parent\'s responsibility to contact the teachers to arrange make-up classes.</li>
//			<li></li>
//		</ol>';

$x .= '</div>';
			return $x;
//		}
//		else
//			return false;
	}
		function getPrintPageCss(){
		return '<style type="text/css">
.tg-left { text-align: left; } .tg-right { text-align: right; } .tg-center { text-align: center; }
.tg-bf { font-weight: bold; } .tg-it { font-style: italic; }
.tg-table-plain { border-collapse: collapse; border-spacing: 0; font-size: 70%; font: inherit; width:720px;}
.tg-table-plain td { border: 1px #555 solid; padding: 0px; vertical-align: top; font-size: 13px;}
.tg-table-plain2 td { border: 1px #555 solid; padding: 5px; vertical-align: top; font-size: 13px;}
.tg-table-plain .answer {font-size: 15px;font-weight: bold;}
.bottom_text td {vertical-align:bottom;text-align:left;height:50px;}
.center_text td {text-align:center;}
.print_field_title { background: #EFEFEF}
.print_field_title_main { background:#D7D7D7}
.input_content { background:#FFF; padding:1px 5px; margin-left:5px; margin-right:10px; border-radius:3px;}
.print_field_row1 { width:120px}
.print_field_row2 { width:28px}
.print_field_row3 { width:80px}
.print_field_title_remark { background:#B9B9B9; width:290px;}
.print_field_title_parent{ width:181px;}
div.section_title {background-color: #B9B9B9; width:710px; margin:20px 0px; padding:5px;}
@media print
{
    .print_hide, .print_hide *
    {
        display: none !important;
    }
}
p, table {font-size: 13px;}

</style>';
	}
}
?>