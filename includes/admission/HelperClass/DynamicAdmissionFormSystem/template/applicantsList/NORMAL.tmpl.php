<?php

?>
<?=$this->generateValidateJs($fields); ?>

<style>
.hide{
    display:none;
}
.itemInput .remark {
    margin-top: 5px;
}
.itemInput .remark.remark-warn {
    color: #dd2c00;
}
</style>
<table class="form_table">
	<tbody>
		<?php
		foreach($fields as $index=>$field):
    		if($field['ExtraArr']['skipDisplayInAdmin']){
    		    continue;
    		}

            $title = Get_Lang_Selection($field['TitleB5'], $field['TitleEn']);
            $dbTable = $field['OtherAttributeArr']['DbTableName'];
            $_dbField = explode(' ', $field['OtherAttributeArr']['DbFieldName']);
            $dbField = $_dbField[0];

            $data = $applicationData[$dbTable];

			if($dbTable === 'ADMISSION_CUST_INFO'){
				if(isset($field['ExtraArr']['code'])) {
					$_valueArr = array();
					foreach ( (array)$data as $d ) {
						if ( $d['Code'] === $field['ExtraArr']['code'] ) {
							$_valueArr[ (int) ( $d['Position'] ) ] = $d['Value'];
						}
					}

					if ( count( $_valueArr ) > 1 ) {
						$value = $_valueArr;
					} else {
						$value = current( $_valueArr );
					}
				}elseif(isset($field['ExtraArr']['sqlWhere'])){
					$sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
					$sqlWhereField = $sqlWhere[0];
					$sqlWhereValue = trim($sqlWhere[1], " '");

					foreach((array)$data as $v){
						if($v[$sqlWhereField] == $sqlWhereValue){
							$value = $v['Value'];
							break;
						}
					}
					
					if(isset($field['ExtraArr']['options'])){
						foreach((array)$field['ExtraArr']['options'] as $options){
							$value = str_replace($options[0], Get_Lang_Selection($options[1],$options[2]),$value);
						}
					}
				}
			}elseif(!isset($data[$dbTable]) && isset($field['ExtraArr']['sqlWhere']) && !isset($field['ExtraArr']['code'])){
				$sqlWhere = explode('=', $field['ExtraArr']['sqlWhere']);
				$sqlWhereField = $sqlWhere[0];
				$sqlWhereValue = trim($sqlWhere[1], " '");

				foreach((array)$data as $v){
					if($v[$sqlWhereField] == $sqlWhereValue){
						$value = $v[$dbTable];
						break;
					}
				}
			}else{
				$value = $data[$dbField];
			}

            if($isEdit) {
	            if($field['Type']=== \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR){
		            $value = array();
		            for($i=1;$i<=3;$i++){
			            if($data["ApplyDayType{$i}"]) {
				            $value[] = $data["ApplyDayType{$i}"];
			            }
		            }
	            }

	            $value = $kis_data['lauc']->applyFilter($kis_data['lauc']::FILTER_ADMISSION_PORTAL_EDIT_VALUE, $value, $field, $data);
            }else{
	            if($field['Type']=== \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_TYPE_APPLY_FOR){
		            $valueArr = array();
		            for($i=1;$i<=3;$i++){
			            if($data["ApplyDayType{$i}"]) {
				            $valueArr[] = $kis_lang['Admission']['TimeSlot'][ $data["ApplyDayType{$i}"] ];
			            }
		            }
		            $value = implode(', ', $valueArr);
	            }

            	$value = $kis_data['lauc']->applyFilter($kis_data['lauc']::FILTER_ADMISSION_PORTAL_DISPLAY_VALUE, $value, $field, $data);
            }
		?>
    		<tr>
    			<td class="field_title" width="30%" >
    				<?= $title ?>
    			</td>
    			<td>
        			<?php if($isEdit): ?>
        				<?=$this->generateAdminDetailsPageFieldHtml($field, $value, $applicationData) ?>
    				<?php else: ?>
        				<?=kis_ui::displayTableField($value) ?>
    				<?php endif; ?>
    			</td>
    		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>

<script>
  $('#applicant_form').unbind('submit').submit(function (e) {
    e.preventDefault();

    window.scrollTo(0, 0);
    var schoolYearId = $('#schoolYearId').val();
    var recordID = $('#recordID').val();
    var display = $('#display').val();
    var timeSlot = lang.timeslot.split(',');
    var data = $(this).serialize();
    var result = check_input_info();

    if (typeof (result) === 'boolean') {
      $.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function (success) {
        $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
      });
    } else {
      result.then(function () {
        $.post('apps/admission/ajax.php?action=updateApplicationInfo', data, function (success) {
          $.address.value('/apps/admission/applicantslist/details/' + schoolYearId + '/' + recordID + '/' + display + '&sysMsg=' + success);
        });
      });
    }

    return false;
  });
</script>
