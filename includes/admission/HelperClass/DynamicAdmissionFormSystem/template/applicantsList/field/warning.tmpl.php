
<div style="">
	<div class="remark remark-warn required hide" style="clear:both;">必須填寫 Required</div>
	<div class="remark remark-warn non_number hide" style="clear:both;">不可包含數字 Must not contains number</div>
	<div class="remark remark-warn number hide" style="clear:both;">必須是數字 Must be number</div>
	<div class="remark remark-warn integer hide" style="clear:both;">必須是整數 Must be integer</div>
	<div class="remark remark-warn postive_number hide" style="clear:both;">必須是正整數 Must be postive integer</div>
	<div class="remark remark-warn number_in_range hide" style="clear:both;">範圍必須由 {min} - {max} Must between {min} - {max}</div>
	<div class="remark remark-warn chinese hide" style="clear:both;">必須是中文字 Must be Chinese character</div>
	<div class="remark remark-warn has_chinese hide" style="clear:both;">必需包含中文字 Must contains Chinese character</div>
	<div class="remark remark-warn non_chinese hide" style="clear:both;">不可包含中文字 Must not contains Chinese character</div>
	<div class="remark remark-warn english hide" style="clear:both;">必須是英文字 Must be English character</div>
	<div class="remark remark-warn date hide" style="clear:both;">日期錯誤 Invalid date</div>
	<div class="remark remark-warn dob hide" style="clear:both;">出生日期與申請組別不符 Invalid Birthday Range of Student</div>
	<div class="remark remark-warn date_range hide" style="clear:both;">日期範圍不符 Invalid Date Range</div>
	<div class="remark remark-warn hkid email hide" style="clear:both;">格式錯誤 Invalid format</div>
	<div class="remark remark-warn hkid_duplicate hide" style="clear:both;">出生證明書號碼已被使用！請輸入其他出生證明書號碼。The Birth Certificate Number is used for admission! Please enter another Birth Certificate Number.</div>
	<div class="remark remark-warn equal hide" style="clear:both;">資料不相符 Data must be same</div>
	<div class="remark remark-warn not_complete hide" style="clear:both;">資料不完整 Data in-completed</div>
	<div class="remark remark-warn option_repeat hide" style="clear:both;">選擇重覆 Duplicate selection</div>
	<div class="remark remark-warn length hide" style="clear:both;">長度必需為 {length} Length must be {length}</div>
	<div class="remark remark-warn custom hide" style="clear:both;"></div>
</div>