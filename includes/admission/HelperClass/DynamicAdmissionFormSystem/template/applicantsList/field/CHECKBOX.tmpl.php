<?php
global $libkis_admission;

$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$libkis_admission->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get option START ####
$options = $extraArr['options'];
$hasOther = $extraArr['hasOther'];
#### Get option END ####


$valueArr = explode(', ', $value);
if($hasOther) {
	$checkboxValueArr = array();
	foreach ( $options as $index => $option ) {
		if ( in_array( $option[0], $valueArr ) ) {
			$checkboxValueArr[] = $option[0];
		}
	}

	$otherValueArr = array_diff($valueArr, $checkboxValueArr);
	$otherValue = implode( ', ', $otherValueArr );
}
?>
<span class="itemInput itemInput-choice">
    <?php foreach($options as $index=>$option):?>
    	<span>
    		<input
        		type="checkbox"
        		id="field_<?=$field['FieldID'] ?>_<?=$index ?>"
        		name="field_<?=$field['FieldID'] ?>[]"
                value="<?=$option[0] ?>"
                data-field="<?=$extraArr['field']?>"
                <?= (in_array($option[0], $valueArr))?'checked':'' ?>
            ><label for="field_<?=$field['FieldID'] ?>_<?=$index ?>">
                <?=Get_Lang_Selection($option[1],$option[2]) ?>
            </label>
    	</span>
	<?php endforeach; ?>

    <?php if($hasOther): ?>
	    <div style="width:80%;display: block;">
			<div class="textbox-floatlabel">
				<input
					type="text"
					id="field_<?=$field['FieldID'] ?>_other"
					name="field_<?=$field['FieldID'] ?>_other"
					value="<?=$otherValue ?>"
					data-original-value="<?=$otherValue ?>"
					data-field="<?=$extraArr['field']?>"
					class="<?=($otherValue)?'notEmpty':'' ?>"
				/>
				<div class="textboxLabel">
                <?=$kis_lang['General']['Others'] ?>
				</div>

			</div>
		</div>
    <?php endif; ?>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span><!--


--><script>
$(function(){
	$('[name="field_<?=$field['FieldID'] ?>[]"]').click(updateValue);
	$('[name="field_<?=$field['FieldID'] ?>_other"]').change(updateValue);

	function updateValue(){
		var valueArr = [];
        $('[name="field_<?=$field['FieldID'] ?>[]"]:checked').each(function(){
          valueArr.push($(this).next().html());
        });

        if(valueArr.length > 0){
          $('#data_<?=$field['FieldID'] ?>').removeClass('dataValue-empty').html(valueArr.join(', '))
          $('#data_<?=$field['FieldID'] ?>_other').html($('#field_<?=$field['FieldID'] ?>_other').val());
        }else{
          $('#data_<?=$field['FieldID'] ?>').addClass('dataValue-empty').html('－－');
        }
	}

	updateValue();
});
</script>
