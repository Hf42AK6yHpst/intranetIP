<?php
global $libkis_admission;

$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$libkis_admission->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get option START ####
$options = $extraArr['options'];
#### Get option END ####

?>
<span class="itemInput itemInput-choice">
    <?php foreach($options as $index=>$option):?>
    	<span>
    		<input
        		type="radio"
        		id="field_<?=$field['FieldID'] ?>_<?=$index ?>"
        		name="field_<?=$field['FieldID'] ?>"
                value="<?=$option[0] ?>"
                data-field="<?=$extraArr['field']?>"
                <?= ($option[0] == $value)?'checked':'' ?>
            ><label for="field_<?=$field['FieldID'] ?>_<?=$index ?>"> <?=$option[1] ?> <?=$option[2] ?></label>
    	</span>
	<?php endforeach; ?>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span>