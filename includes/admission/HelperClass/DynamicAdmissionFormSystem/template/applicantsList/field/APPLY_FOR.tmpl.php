<?php
global $json,$dayTypeArr, $libkis_admission;

$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$libkis_admission->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get setting START ####
$slotLangArr = array();
foreach($dayTypeArr as $dayType){
    $slotLangArr[$dayType] = $kis_lang['Admission']['TimeSlot'][$dayType];
}
#### Get setting END ####
?>
<span class="itemInput itemInput-selector">
    <?php for ($i = 0, $iMax=count($slotLangArr); $i < $iMax; $i++): ?><!--
        --><span class="selector">
    		<select
        		id="field_<?=$field['FieldID'] ?>_<?=$i ?>"
        		name="field_<?=$field['FieldID'] ?>[]"
        		data-field="<?=$extraArr['field']?>"
    		>
    			<option value='' >-- <?=$kis_lang['Admission']['Option'] ?> <?=$i+1 ?> --</option>
    			<?php foreach($slotLangArr as $type=>$slotLang): ?>
    				<option value="<?=$type ?>" <?=($type == $value[$i])?'selected':'' ?> ><?=$slotLang ?></option>
    			<?php endforeach; ?>
    		</select>
    	</span><!--
	--><?php endfor; ?>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span>