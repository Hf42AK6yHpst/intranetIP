<?php
global $lauc, $lac;
$OtherAttribute = $field['OtherAttributeArr'];
$width          = $OtherAttribute['Width'];
$extraArr       = $field['ExtraArr'];
$validation     = $field['ValidationArr'];

$hasLabel   = ! ! ( $field['LabelB5'] . $field['LabelEn'] . $field['LabelGb'] );
$isRequired = ( in_array( \AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation ) );
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

$extraArr['optionsY'] = (array) $extraArr['optionsY'];
$extraArr['optionsN'] = (array) $extraArr['optionsN'];

$valueArr = explode(';', $value);

?>

<span class="itemInput itemInput-choice">
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_Y"
                name="field_<?= $field['FieldID'] ?>"
                value="Y"
                <?= ($valueArr[0] === 'Y')? 'checked':''?>
        ><label for="field_<?= $field['FieldID'] ?>_Y">
	        <?=$lauc->getLangStr(array(
		        'b5' => $LangB5['General']['Yes'],
		        'en' => $LangEn['General']['Yes'],
	        )) ?>
        </label>
	</span>
	<span>
		<input
                type="radio"
                id="field_<?= $field['FieldID'] ?>_N"
                name="field_<?= $field['FieldID'] ?>"
                value="N"
                <?= ($valueArr[0] === 'N')? 'checked':''?>
        ><label for="field_<?= $field['FieldID'] ?>_N">
	        <?=$lauc->getLangStr(array(
		        'b5' => $LangB5['General']['No'],
		        'en' => $LangEn['General']['No'],
	        )) ?>
        </label>
	</span>

    <?php
    $countY = count( $extraArr['optionsY'] );
    foreach ( $extraArr['optionsY'] as $index=>$optionsY ):
	    $cssClass = ( $countY === 2 ) ? 'textbox-half' : "textbox-in{$countY}";
	    ?>
        <span class="options_<?= $field['FieldID'] ?>_details Y" style="display: none;">
            <div class="textbox-floatlabel <?= $cssClass ?>">
                <input
                        type="text"
                        id="field_<?= $field['FieldID'] ?>_<?=$index?>"
                        class="field_<?= $field['FieldID'] ?>_option Y"
                        name="field_<?= $field['FieldID'] ?>_option[]"
                        value="<?=$valueArr[$index+1]?>"
                />
                <div class="textboxLabel ">
                    <?=$lauc->getLangStr(array(
                        'b5' => $optionsY[0],
                        'en' => $optionsY[1],
                    )) ?>
                </div>
            </div>
        </span>
    <?php
    endforeach;

    $countN = count( $extraArr['optionsN'] );
    foreach ( $extraArr['optionsN'] as $index=>$optionsN ):
	    $cssClass = ( $countN === 2 ) ? 'textbox-half' : "textbox-in{$countN}";
	    ?>
        <span class="options_<?= $field['FieldID'] ?>_details N" style="display: none;">
            <div class="textbox-floatlabel <?= $cssClass ?>">
                <input
                        type="text"
                        id="field_<?= $field['FieldID'] ?>_<?=$index?>"
                        class="field_<?= $field['FieldID'] ?>_option N"
                        name="field_<?= $field['FieldID'] ?>_option[]"
                        value="<?=$valueArr[$index+1]?>"
                />
                <div class="textboxLabel ">
                    <?=$lauc->getLangStr(array(
	                    'b5' => $optionsN[0],
	                    'en' => $optionsN[1],
                    )) ?>
                </div>
            </div>
        </span>
    <?php
    endforeach;
    ?>

	<?php include( __DIR__ . '/warning.tmpl.php' ); ?>
</span><!--

--><span class="itemData <?= $itemDataClass ?>" id="data_<?= $field['FieldID'] ?>">
	<div class="dataLabel">
		<?= $field['TitleB5'] ?>
		<?= $field['TitleEn'] ?>
	</div>
	<div class="dataValue dataValue-empty">－－</div>
	<div class="dataValue dataValue-non-empty">
        <span class="Y">
            <?=$lauc->getLangStr(array(
                'b5' => $LangB5['General']['Yes'],
                'en' => $LangEn['General']['Yes'],
            )) ?>
        </span>
        <span class="N">
            <?=$lauc->getLangStr(array(
	            'b5' => $LangB5['General']['No'],
	            'en' => $LangEn['General']['No'],
            )) ?>
        </span>
        <div class="value"></div>
    </div>
</span><!--

-->
<script>
  $(function () {
    var $field = $('[name="field_<?=$field['FieldID'] ?>"]');
    $field.click(updateValue);
    $('.field_<?=$field['FieldID'] ?>_option').change(updateValue);

    function updateValue() {
      var checkedVal = $field.filter(':checked').val();

      //// Update UI START ////
      $('.field_<?= $field['FieldID'] ?>_option').prop('disabled', true);
      $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).prop('disabled', false);
      $('.options_<?= $field['FieldID'] ?>_details').hide();
      $('.options_<?= $field['FieldID'] ?>_details.'+checkedVal).show().css('display', 'block')
      //// Update UI END ////

      //// Update value START ////
      var $data = $('#data_<?=$field['FieldID'] ?>');
      $data.find('.dataValue, .Y, .N').hide();

      if (checkedVal == '') {
        $data.find('.dataValue-empty').show();
      } else {
        var valueArr = [];
        $('.field_<?= $field['FieldID'] ?>_option.'+checkedVal).each(function(){
          valueArr.push($(this).val());
        });
        $data.find('.dataValue-non-empty, .'+checkedVal).show();
        $data.find('.value').html(valueArr.join(', '));
      }
      //// Update value END ////
    }

    updateValue();
  });
</script>
