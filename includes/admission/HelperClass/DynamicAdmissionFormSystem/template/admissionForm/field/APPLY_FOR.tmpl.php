<?php
global $json,$dayTypeArr, $lauc, $lac;


$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get setting START ####
$slotLangArr = array();
foreach($dayTypeArr as $dayType){
    $slotLangArr[$dayType] = $lauc->getLangStr(array(
	    'b5' => $LangB5['Admission']['TimeSlot'][$dayType],
	    'en' => $LangEn['Admission']['TimeSlot'][$dayType],
    ));
}
#### Get setting END ####
?>
<span class="itemInput itemInput-selector">
    <?php for ($i = 0, $iMax=count($slotLangArr); $i < $iMax; $i++): ?><!--
        --><span class="selector">
    		<select
        		id="field_<?=$field['FieldID'] ?>_<?=$i ?>"
        		name="field_<?=$field['FieldID'] ?>[]"
        		data-field="<?=$extraArr['field']?>"
    		>
    			<option value='' >-- <?=$lauc->getLangStr(array(
                    'b5' => "{$LangB5['Admission']['Option']} " . ($i+1),
                    'en' => "{$LangEn['Admission']['Option']} " . ($i+1),
                )) ?> --</option>
    			<?php foreach($slotLangArr as $type=>$slotLang): ?>
    				<option value="<?=$type ?>" <?=($type == $value[$i])?'selected':'' ?>><?=$slotLang ?></option>
    			<?php endforeach; ?>
    		</select>
    	</span><!--
	--><?php endfor; ?>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span><!--

--><span class="itemData">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
	        'b5' => $field['LabelB5'],
	        'en' => $field['LabelEn'],
        )) ?>
        (<?=$classLevel?>)
	</div>
	<div class="dataValue" id="data_<?=$field['FieldID'] ?>">
    	<?php for ($i = 0, $iMax=count($slotLangArr); $i < $iMax; $i++): ?>
    		<div>
                <?=$lauc->getLangStr(array(
	                'b5' => "{$LangB5['Admission']['Option']} " . ($i+1),
	                'en' => "{$LangEn['Admission']['Option']} " . ($i+1),
                )) ?>:
    			<span class="choice_<?=$i ?>"></span>
    		</div>
    	<?php endfor; ?>
	</div>
</span><!--

--><script>
$(function(){
	var slotLangArr = <?=$json->encode($slotLangArr) ?>;
	$('[name^="field_<?=$field['FieldID'] ?>"]').change(updateValue);

	function updateValue(){
		var choices = [];
    	<?php for ($i = 0, $iMax=count($slotLangArr); $i < $iMax; $i++): ?>
    		var value = $('#field_<?=$field['FieldID'] ?>_<?=$i ?>').val();
    		if(value){
    			choices.push(value);
    		}
    	<?php endfor; ?>


		var $field = $('#data_<?=$field['FieldID'] ?>');
		if(choices.length){
			$field.removeClass('dataValue-empty');
		}else{
			$field.addClass('dataValue-empty');
		}

		$field.find('[class^="choice_"]').html('－－');
		$.each(choices, function(index, choice){
			$field.find('.choice_' + index).html(slotLangArr[choice]);
		});
	}

	updateValue();
});
</script>
