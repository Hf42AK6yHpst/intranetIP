<?php
global $lauc;
$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));

#### Get option START ####
$options = $extraArr['options'];
$hasOther = $extraArr['hasOther'];
#### Get option END ####

$valueArr = explode(', ', $value);
if($hasOther) {
	$checkboxValueArr = array();
	foreach ( $options as $index => $option ) {
		if ( in_array( $option[0], $valueArr ) ) {
			$checkboxValueArr[] = $option[0];
		}
	}

	$otherValueArr = array_diff($valueArr, $checkboxValueArr);
	$otherValue = implode( ', ', $otherValueArr );
}
?>
<span class="itemInput itemInput-choice">
    <?php foreach($options as $index=>$option):?>
    	<span>
    		<input
        		type="checkbox"
        		id="field_<?=$field['FieldID'] ?>_<?=$index ?>"
        		name="field_<?=$field['FieldID'] ?>[]"
                value="<?=$option[0] ?>"
                data-field="<?=$extraArr['field']?>"
                <?= (in_array($option[0], $valueArr))?'checked':'' ?>
            ><label for="field_<?=$field['FieldID'] ?>_<?=$index ?>">
                <?=$lauc->getLangStr(array(
                    'b5' => $option[1],
                    'en' => $option[2],
                )) ?>
            </label>
    	</span>
	<?php endforeach; ?>

    <?php if($hasOther): ?>
	    <div style="width:80%;display: block;">
			<div class="textbox-floatlabel">
				<input
					type="text"
					id="field_<?=$field['FieldID'] ?>_other"
					name="field_<?=$field['FieldID'] ?>_other"
					value="<?=$otherValue ?>"
					data-original-value="<?=$otherValue ?>"
					data-field="<?=$extraArr['field']?>"
					class="<?=($otherValue)?'notEmpty':'' ?>"
				/>
				<div class="textboxLabel">
                <?=$lauc->getLangStr(array(
	                'b5' => $LangB5['General']['Others'],
	                'en' => $LangEn['General']['Others'],
                )) ?>
				</div>

			</div>
		</div>
    <?php endif; ?>

	<?php include(__DIR__.'/warning.tmpl.php'); ?>
</span><!--

--><span class="itemData">
	<div class="dataLabel">
        <?=$lauc->getLangStr(array(
            'b5' => $field['LabelB5'],
            'en' => $field['LabelEn'],
        )) ?>
	</div>
	<div class="dataValue" id="data_<?=$field['FieldID'] ?>"></div>
	<div class="dataValue" id="data_<?=$field['FieldID'] ?>_other"></div>
</span><!--

--><script>
$(function(){
	$('[name="field_<?=$field['FieldID'] ?>[]"]').click(updateValue);
	$('[name="field_<?=$field['FieldID'] ?>_other"]').change(updateValue);

	function updateValue(){
		var valueArr = [];
        $('[name="field_<?=$field['FieldID'] ?>[]"]:checked').each(function(){
          valueArr.push($(this).next().html());
        });

        if(valueArr.length > 0){
          $('#data_<?=$field['FieldID'] ?>').removeClass('dataValue-empty').html(valueArr.join(', '))
          $('#data_<?=$field['FieldID'] ?>_other').html($('#field_<?=$field['FieldID'] ?>_other').val());
        }else{
          $('#data_<?=$field['FieldID'] ?>').addClass('dataValue-empty').html('－－');
        }
	}

	updateValue();
});
</script>
