<?php
global $admission_cfg;
global $lauc, $lac;

$OtherAttribute = $field['OtherAttributeArr'];
$width = $OtherAttribute['Width'];
$extraArr = $field['ExtraArr'];
$validation = $field['ValidationArr'];

$isRequired = (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED, $validation));
$isRequired = $isRequired || !$lac->isInternalUse($_GET['token']) && (in_array(\AdmissionSystem\DynamicAdmissionFormSystem::FIELD_VALIDATION_TYPE_REQUIRED_ONLY_STUDENT, $validation));

#### Get setting START ####
$birthCertTypeArr = array();
foreach($admission_cfg['BirthCertType'] as $key => $v){
	$birthCertTypeArr[$v] = $lauc->getLangStr(array(
        'b5' => $LangB5['Admission']['BirthCertType'][$key],
        'en' => $LangEn['Admission']['BirthCertType'][$key],
    ));
}
#### Get setting END ####
?>

<?php if($IsUpdate): ?>
    <span class="">
        <div class="dataLabel">
            <?=$lauc->getLangStr(array(
	            'b5' => $field['TitleB5'],
	            'en' => $field['TitleEn']
            ))?>
        </div>
        <div class="dataValue" id="data_<?=$field['FieldID'] ?>">
            <span class="<?=$v?>"><?=$birthCertTypeArr[$applicationData['ADMISSION_STU_INFO']['BirthCertType']]?></span>:
            <span class="value"><?=$value?></span>

            <input
                    type="hidden"
                    id="field_<?=$field['FieldID'] ?>_value"
                    name="field_<?=$field['FieldID'] ?>_value"
                    data-original-value="<?=$value ?>"
                    value="<?=$value ?>"
            />
            <input
                    type="hidden"
                    id="field_<?=$field['FieldID'] ?>_type"
                    name="field_<?=$field['FieldID'] ?>_type"
                    value="<?=$applicationData['ADMISSION_STU_INFO']['BirthCertType'] ?>"
            />
        </div>
    </span>
<?php else: ?>
    <div class="itemInput itemInput-selector"><span class="selector" style="
    padding-top: 12px;
    margin-right: 10px;
    ">
            <select id="field_<?=$field['FieldID'] ?>_type" name="field_<?=$field['FieldID'] ?>_type" data-field="">
    <!--                <option value="" hidden="">-- 選擇 Option --</option>-->
                <?php foreach($birthCertTypeArr as $v => $lang): ?>
                    <option value="<?=$v?>" <?= ($v == $applicationData['ADMISSION_STU_INFO']['BirthCertType'])?'selected':'' ?>><?=$lang?></option>
                <?php endforeach; ?>
            </select>
        </span><div class="textbox-floatlabel" style="
            width: 320px;
            display: inline-block;
            max-width: 100%;
        ">
            <input
                    type="text"
                    id="field_<?=$field['FieldID'] ?>_value"
                    name="field_<?=$field['FieldID'] ?>_value"
                    value="<?=$value?>"
					data-original-value="<?=$value ?>"
    				data-field="<?=$extraArr['field']?>"
                    class="empty"
            />
            <div class="textboxLabel requiredLabel">
                <?=$lauc->getLangStr(array(
                    'b5' => '號碼',
                    'en' => 'No.',
                ))?>
            </div>


            <div style="">
                <?php include( __DIR__ . '/warning.tmpl.php' ); ?>
            </div>
        </div>
    </div><!--

    --><span class="itemData">
        <div class="dataLabel">
            <?=$lauc->getLangStr(array(
                'b5' => $field['TitleB5'],
                'en' => $field['TitleEn']
            ))?>
        </div>
        <div class="dataValue" id="data_<?=$field['FieldID'] ?>">
            <?php foreach($birthCertTypeArr as $v => $lang): ?>
                <span class="<?=$v?>"><?=$lang?></span>
            <?php endforeach; ?>
            <span class="value"></span>
        </div>
    </span><!--

    --><script>
    $(function(){
        $('#field_<?=$field['FieldID'] ?>_type, #field_<?=$field['FieldID'] ?>_value').change(updateValue);

        function updateValue(){
          var $dataContainer = $('#data_<?=$field['FieldID'] ?>');
          var $valueContainer = $dataContainer.find('.value');

          $dataContainer.find('span').hide();
          $dataContainer.find('.'+$('#field_<?=$field['FieldID'] ?>_type').val()).show();
          $valueContainer.show();

            if($('#field_<?=$field['FieldID'] ?>_value').val() == ''){
              $dataContainer.addClass('dataValue-empty')
              $valueContainer.html('－－');
            }else{
              $dataContainer.removeClass('dataValue-empty');
              $valueContainer.html($('#field_<?=$field['FieldID'] ?>_value').val());
            }
        }

        updateValue();
    });
    </script>
<?php endif; ?>
