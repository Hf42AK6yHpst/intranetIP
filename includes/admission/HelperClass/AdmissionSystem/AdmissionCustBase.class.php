<?php
/**
 * Change Log:
 * 2020-03-28 Pun
 *  - Modified getNextApplicationNumber(), added data parameter
 * 2018-01-24 Pun
 *  - File Created
 */
namespace AdmissionSystem;

include_once ("{$intranet_root}/includes/admission/libadmission_cust_base.php");
include_once (__DIR__ . "/ActionFilterQueueTrait.class.php");

abstract class AdmissionCustBase extends \admission_cust_base
{
    use ActionFilterQueueTrait;

    public $AdmissionFormSendEmail = true;

    /**
     * ** Filter/Action START ***
     */
    const FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER = 'AdmissionForm_NextApplicationNumber';

    const FILTER_ADMISSION_FORM_EMAIL_TITLE = 'AdmissionForm_EmailTitle';
    const FILTER_ADMISSION_FORM_EMAIL_ADDRESS = 'AdmissionForm_EmailAddress';
    const FILTER_ADMISSION_FORM_EMAIL_ADDRESS_CC = 'AdmissionForm_EmailAddressCC';
    const FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC = 'AdmissionForm_EmailAddressBCC';

    const ACTION_APPLICANT_UPDATE_INFO = 'AdmissionForm_UpdateApplicationData';

    const ACTION_APPLICANT_INSERT_DUMMY_INFO = 'AdmissionForm_InsertDummyApplicationData';

    const FILTER_EXPORT_APPLICANT_LIST = 'Portal_ExportApplicantList';
    const FILTER_EXPORT_INTERVIEW_LIST = 'Portal_ExportInterviewList';
    const ACTION_APPLICANT_INFO = 'Portal_UpdateApplicationData';

    /**
     * ** Filter/Action END ***
     */
    public function __construct()
    {
        parent::__construct();

        global $kis_lang, $UserID; // switch $lang for IP/EJ/KIS
        $this->libdb();
        $this->filepath = '/file/admission/';
        $this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
        $this->schoolYearID = $this->getNextSchoolYearID();
        $this->uid = $UserID;
        $this->classLevelAry = $this->getClassLevel();
    }

    /**
     * Get the school year ID for admission
     */
    public function getNextSchoolYearID()
    {
        global $intranet_root;
        include_once ($intranet_root . "/includes/form_class_manage.php");
        $lfcm = new \form_class_manage();
        $SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence = 1, $excludeYearIDArr = array(), $noPastYear = 1, $pastAndCurrentYearOnly = 0, $excludeCurrentYear = 0);
        $SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField = array(
            'AcademicYearID'
        ), 1);
        krsort($SchoolYearIDArr);

        $SchoolYearIDArr = array_values($SchoolYearIDArr);
        $currentSchoolYear = Get_Current_Academic_Year_ID();
        $key = array_search($currentSchoolYear, $SchoolYearIDArr);
        if ($key > 0) {
            return $SchoolYearIDArr[$key - 1];
        } else {
            return false;
        }
    }

    public function getClassLevel($ClassLevel = '')
    {
        global $intranet_root;
        include_once ($intranet_root . "/includes/form_class_manage.php");
        $libYear = new \Year();
        $FormArr = $libYear->Get_All_Year_List();
        $numOfForm = count($FormArr);
        $classLevelName = array();
        for ($i = 0; $i < $numOfForm; $i ++) {
            $thisClassLevelID = $FormArr[$i]['YearID'];
            $thisLevelName = $FormArr[$i]['YearName'];
            $classLevelName[$thisClassLevelID] = $thisLevelName;
        }
        return $classLevelName;
    }

    /**
     * Admission Form - Before new applicant create status record
     */
    public function newApplicationNumber2($SchoolYearID, $Data)
    {
        global $sys_custom;

        $nextApplicationId = $this->getNextApplicationNumber($this->schoolYearID, $Data);

        $token = $_REQUEST['token'];
        if (! $this->isInternalUse($_REQUEST['token']) && $token == '' && ! $sys_custom['KIS_Admission']['IntegratedCentralServer']) {
            $token = 1;
        }
        $HTTP_USER_AGENT = addslashes($_SERVER['HTTP_USER_AGENT']);

        $sql = "INSERT INTO
            ADMISSION_OTHERS_INFO
        (
            ApplicationID,
            ApplyYear,
            ApplyLevel,
            ApplyDayType1,
            ApplyDayType2,
            ApplyDayType3,
            Token,
            DateInput,
            HTTP_USER_AGENT
        ) Values (
            '{$nextApplicationId}',
            '{$this->schoolYearID}',
            '{$_REQUEST['sus_status']}',
            '{$_REQUEST['OthersApplyDayType1']}',
            '{$_REQUEST['OthersApplyDayType2']}',
            '{$_REQUEST['OthersApplyDayType3']}',
            '{$token}',
            NOW(),
            '{$HTTP_USER_AGENT}'
        )";
        $result = $this->db_db_query($sql);

        return $nextApplicationId;
    }

    private function getNextApplicationNumber($schoolYearID = '', $Data = array())
    {
        $yearStart = ($Data['isImport']?'H':'').date('y', getStartOfAcademicYear('', $this->schoolYearID));

        $sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '{$yearStart}%' ORDER BY RecordID DESC LIMIT 1";
        $rs = $this->returnVector($sql);
        $lastApplicationId = (count($rs)) ? $rs[0] : "{$yearStart}0000";
        $nextApplicationId = $lastApplicationId + 1;

        $nextApplicationId = $this->applyFilter(self::FILTER_ADMISSION_FORM_NEXT_APPLICATION_NUMBER, $nextApplicationId, $this->schoolYearID, $Data);

        return $nextApplicationId;
    }

    /**
     * Admission Form - New/Edit applicant save all info
     */
    public function insertApplicationAllInfo($libkis_admission, $Data/* Equal to $_REQUEST */, $ApplicationID, $isUpdate = 0)
    {
        global $admission_cfg;

        $Success = array();
        if ($ApplicationID != "") {

            // ####### Create application status START ########
            if (! $isUpdate) {
                $Success[] = $this->insertApplicationStatus($ApplicationID);
                $Success[] = $this->insertDummyApplicationDetails($ApplicationID);
            }
            // ####### Create application status END ########

            // ####### Save application details START ########
            try {
                $this->doAction(self::ACTION_APPLICANT_UPDATE_INFO, $Data, $ApplicationID);
                $Success[] = true;
            } catch (\Exception $e) {
                $Success[] = false;

                global $plugin;
                if ($plugin['eAdmission_devMode']) {
                    debug_r($e);
                    exit();
                }
            }
            // ####### Save application details END ########

            if (in_array(false, $Success)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private function insertApplicationStatus($ApplicationID)
    {
        if (! $ApplicationID) {
            return false;
        }

        $sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
            SchoolYearID,
            ApplicationID,
            Status,
            DateInput
        ) VALUES (
            '" . $this->schoolYearID . "',
            '" . $ApplicationID . "',
            '1',
            now()
        )";
        return $this->db_db_query($sql);
    }

    private function insertDummyApplicationDetails($ApplicationID)
    {
        if (! $ApplicationID) {
            return false;
        }

        $insertResult = true;

        // ### Insert student info START ####
        $sql = "INSERT INTO ADMISSION_STU_INFO ( ApplicationID, DateInput ) VALUES ( '{$ApplicationID}', now() ) ";
        $insertResult = $insertResult && $this->db_db_query($sql);
        // ### Insert student info END ####

        // ### Insert parent info START ####
        $sql = "INSERT INTO ADMISSION_PG_INFO ( ApplicationID, PG_TYPE, DateInput ) VALUES ( '{$ApplicationID}', 'M', now() ) ";
        $insertResult = $insertResult && $this->db_db_query($sql);
        $sql = "INSERT INTO ADMISSION_PG_INFO ( ApplicationID, PG_TYPE, DateInput ) VALUES ( '{$ApplicationID}', 'F', now() ) ";
        $insertResult = $insertResult && $this->db_db_query($sql);
        $sql = "INSERT INTO ADMISSION_PG_INFO ( ApplicationID, PG_TYPE, DateInput ) VALUES ( '{$ApplicationID}', 'G', now() ) ";
        $insertResult = $insertResult && $this->db_db_query($sql);
        // ### Insert parent info END ####

        try {
            if ($insertResult) {
                $this->doAction(self::ACTION_APPLICANT_INSERT_DUMMY_INFO, $ApplicationID);
            }
        } catch (\Exception $e) {
            $insertResult = false;

            global $plugin;
            if ($plugin['eAdmission_devMode']) {
                debug_r($e);
                exit();
            }
        }

        return $insertResult;
    }

    /**
     * Admission Edit Form - Validate application exists
     */
    public function getApplicationResult($StudentDateOfBirth, $StudentBirthCertNo, $SchoolYearID = '', $ApplicationID = '')
    {
        global $admission_cfg;

        if ($SchoolYearID) {
            $cond = " AND o.ApplyYear = '" . $SchoolYearID . "' ";
        }
        if ($ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationID . "' ";
        }

        $sql = "SELECT o.ApplicationID, o.ApplyYear, o.ApplyLevel
                FROM ADMISSION_STU_INFO as s
                JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID
                WHERE s.DOB = '" . $StudentDateOfBirth . "' AND BINARY s.BirthCertNo = '" . $StudentBirthCertNo . "' " . $cond . " ORDER BY o.ApplicationID desc";

        $result = current($this->returnArray($sql));

        if (! $result['ApplicationID']) {
            return 0;
        }

        $result2 = $this->getPaymentResult('', '', '', '', $result['ApplicationID']);
        if ($result2) {
            foreach ($result2 as $aResult) {
                if ($aResult['Status'] == $admission_cfg['Status']['cancelled']) {
                    return 0;
                }
            }
        }

        // if(!$result['Date']){
        // return 'NotAssigned';
        // }

        return $result;
    }

    /**
     * Admission Form - finish send email
     */
    public function sendMailToNewApplicant($applicationId, $subject, $message, $isEdit=false)
    {
        global $admission_cfg, $intranet_root, $kis_lang, $PATH_WRT_ROOT, $sys_custom;
        include_once ($intranet_root . "/includes/libwebmail.php");
        $libwebmail = new \libwebmail();

        if($sys_custom['KIS_Admission']['STANDARD']['Settings']){
			$from = "no-reply@".$_SERVER['SERVER_NAME'];
		}
		else{
        	$from = $libwebmail->GetWebmasterMailAddress();
		}
        $inputby = $_SESSION['UserID'];
        $result = array();

        $sql = "SELECT
                    a.Email as Email
                FROM ADMISSION_STU_INFO as a
                WHERE a.ApplicationID = '" . trim($applicationId) . "'";
        $records = current($this->returnArray($sql));
        // debug_pr($applicationId);
        $to_email = $records['Email'];
        if ($subject == '') {
            $email_subject = "入學申請通知 Admission Notification";
        } else {
            $email_subject = $subject;
        }
        $email_subject = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_TITLE, $email_subject, $applicationId);
	    $to_email = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_ADDRESS, array($to_email), $applicationId);
        $cc_email = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_ADDRESS_CC, array(), $applicationId);
        $bcc_email = $this->applyFilter(self::FILTER_ADMISSION_FORM_EMAIL_ADDRESS_BCC, array(), $applicationId);

        $email_message = $message;
        if ($to_email) {
        	$isValid = true;
        	foreach((array)$to_email as $email){
		        $isValid = $isValid && intranet_validateEmail($email);
	        }

			if($isValid){
				if($admission_cfg['EnableMandrill'] && $this->hasMandrillQuotaByMonth()){
	        		$sent_ok = $this->sendMandrillMail(
		            	$email_subject,
			            $email_message,
			            $from,
			            (array)$to_email,
			            (array)$cc_email,
			            (array)$bcc_email
		            );
		            if($sent_ok){
	        			$sent_ok = $this->insertMandrillRecord($applicationId, $this->Get_Safe_Sql_Query($email_subject), $this->Get_Safe_Sql_Query($email_message));
	        		}
	        	}
	        	else{
	        		$sent_ok = $libwebmail->sendMail(
		            	$email_subject,
			            $email_message,
			            $from,
			            (array)$to_email,
			            (array)$cc_email,
			            (array)$bcc_email,
			            "",
			            $IsImportant = "",
			            $mail_return_path = get_webmaster(),
			            $reply_address = "",
			            $isMulti = null,
			            $nl2br = 0
		            );
	        	}
			}

        } else {
            $sent_ok = false;
        }

        return $sent_ok;
    }

    /**
     * Admission Form - get finish page print link
     */
    public function getPrintLink($schoolYearID = "", $applicationID, $type = "", $lang = '')
    {
        global $admission_cfg;
        $schoolYearID = $schoolYearID ? $schoolYearID : $this->schoolYearID;

        $param = '';
        $param .= "ApplicationID={$applicationID}";
        $param .= "&SchoolYearID={$schoolYearID}";
        $param .= "&Type={$type}";
        $param .= "&lang={$lang}";
        $id = urlencode(getEncryptedText($param, $admission_cfg['FilePathKey']));
        return '/kis/admission_form/print_form.php?id=' . $id;
    }

    /**
     * Get application student studied school
     */
    public function getApplicationPrevSchoolInfo($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = ! empty($applicationID) ? " AND s.ApplicationID='{$applicationID}'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='{$classLevelID}'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='{$recordID}'" : "";
        $sql = "
            SELECT
                s.*
            FROM
                ADMISSION_STU_PREV_SCHOOL_INFO s
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
            WHERE
                o.ApplyYear = '" . $schoolYearID . "'
            " . $cond . "
            AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='' OR s.SchoolAddress !='')
            ORDER BY
                s.SchoolOrder
        ";

        return $this->returnArray($sql);
    }

    /**
     * Get application student relatives
     */
    public function getApplicationRelativesInfo($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = ! empty($applicationID) ? " AND r.ApplicationID='{$applicationID}'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='{$classLevelID}'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='{$recordID}'" : "";
        $sql = "
            SELECT
                r.*
            FROM
                ADMISSION_RELATIVES_AT_SCH_INFO r
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
            WHERE
                o.ApplyYear = '" . $schoolYearID . "'
            " . $cond . "
            AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
        ";

        return $this->returnArray($sql);
    }

    /**
     * Get application student's sibling
     */
    public function getApplicationSibling($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = ! empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
            SELECT
                s.*
            FROM
                ADMISSION_SIBLING s
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
            WHERE
                o.ApplyYear = '{$schoolYearID}'
            " . $cond . "
            ORDER BY
                s.SiblingOrder
        ";

        return $this->returnArray($sql);
    }

    /**
     * Portal - update application info
     */
    public function updateApplicationInfo($type, $Data, $ApplicationID)
    {
        $result = true;

        try {
            $this->doAction(self::ACTION_APPLICANT_INFO, $type, $Data, $ApplicationID);
        } catch (\Exception $e) {
            $result = false;

            global $plugin;
            if ($plugin['eAdmission_devMode']) {
                debug_r($e);
                exit();
            }
        }
        return $result;
    }

    /**
     * Portal - get application student records in table view
     * $data => Array of filter (key-value pair):
     * - classLevelID
     * - applicationID
     * - status
     * - interviewStatus
     * - paymentStatus
     * - custSelection
     * - keyword
     * - page
     * - amount
     * - order
     * - sortby
     */
    function getApplicationDetails($schoolYearID, $data = array())
    {
        global $admission_cfg;
        extract($data);

        $sort = $sortby ? "$sortby $order" : "application_id";
        $sort .= ", FIELD(pg.PG_TYPE, 'F', 'M', 'G') ";
        $limit = $page ? " LIMIT " . (($page - 1) * $amount) . ", $amount" : "";
        $cond = ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= $status ? " AND s.Status='" . $status . "'" : "";
        if ($status == $admission_cfg['Status']['waitingforinterview']) {
            if ($interviewStatus == 1) {
                $cond .= " AND s.isNotified = 1";
            } else {
                $cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
            }
        }

        if ($paymentStatus == $admission_cfg['PaymentStatus']['OnlinePayment']) {
            $paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
            $cond .= " AND pi.ApplicationID IS NOT NULL  ";
        } elseif ($paymentStatus == $admission_cfg['PaymentStatus']['OtherPayment']) {
            $paymentStatus_cond .= " left outer join ADMISSION_PAYMENT_INFO as pi ON o.ApplicationID = pi.ApplicationID";
            $cond .= " AND pi.ApplicationID IS NULL  ";
        }

        if (! empty($keyword)) {
            $cond .= "
                AND (
                    stu.EnglishName LIKE '%" . $keyword . "%'
                    OR stu.ChineseName LIKE '%" . $keyword . "%'
                    OR stu.ApplicationID LIKE '%" . $keyword . "%'
                    OR pg.EnglishName LIKE '%" . $keyword . "%'
                    OR pg.ChineseName LIKE '%" . $keyword . "%'
                    OR pg.Mobile LIKE '%" . $keyword . "%'
                    OR stu.BirthCertNo LIKE '%" . $keyword . "%'
                    OR stu.HomeTelNo LIKE '%" . $keyword . "%'
                    OR stu.LangSpokenAtHome LIKE '%" . $keyword . "%'
                )
            ";
        }

        $from_table = "
        FROM
            ADMISSION_STU_INFO stu
        INNER JOIN
            ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
        INNER JOIN
            ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
        INNER JOIN
            ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
            $paymentStatus_cond
        WHERE
            o.ApplyYear = '" . $schoolYearID . "'
            " . $cond . "

        ";
        $sql = "SELECT DISTINCT o.ApplicationID " . $from_table;
        $applicationIDAry = $this->returnVector($sql);

        // $sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
        // $applicationIDCount = $this->returnVector($sql2);
        $sql = "
            SELECT
                o.RecordID AS record_id,
                 stu.ApplicationID AS application_id,
                 " . getNameFieldByLang2("stu.") . " AS student_name,
                 " . getNameFieldByLang2("pg.") . " AS parent_name,
                 IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
                stu.LangSpokenAtHome AS langspokenathome,
                CASE
         ";
        FOREACH ($admission_cfg['Status'] as $_key => $_status) { // e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status    " . $from_table . " AND o.ApplicationID IN ('" . implode("','", $applicationIDAry) . "') ORDER BY $sort ";

        $applicationAry = $this->returnArray($sql);

        return array(
            count($applicationIDAry),
            $applicationAry
        );
    }

    /**
     * Portal - update remarks
     */
    function updateApplicationStatus($Data, $ApplicationID)
    {
        extract($Data);
        if (empty($recordID) || empty($schoolYearId))
            return;
        if (! empty($interviewdate)) {
            $interviewdate .= " " . str_pad($interview_hour, 2, "0", STR_PAD_LEFT) . ":" . str_pad($interview_min, 2, "0", STR_PAD_LEFT) . ":" . str_pad($interview_sec, 2, "0", STR_PAD_LEFT);
        }
        if (empty($receiptID)) {
            $receiptdate = '';
            $handler = '';
        }
        $sql = "
            UPDATE
                ADMISSION_APPLICATION_STATUS s
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '" . $schoolYearId . "'
            SET
                s.ReceiptID = '" . $receiptID . "',
                 s.ReceiptDate = '" . $receiptdate . "',
                  s.Handler = '" . $handler . "',
                  s.InterviewDate = '" . $interviewdate . "',
                s.InterviewLocation = '" . $interviewlocation . "',
                  s.Remark = '" . $remark . "',
                  s.Status = '" . $status . "',
                  s.DateModified = NOW(),
                  s.ModifiedBy = '{$this->uid}',
                  s.isNotified = '" . $isnotified . "',
                o.InterviewSettingID =  '" . $InterviewSettingID . "',
                o.InterviewSettingID2 =  '" . $InterviewSettingID2 . "',
                o.InterviewSettingID3 =  '" . $InterviewSettingID3 . "',
                    s.FeeBankName = '{$FeeBankName}',
                    s.FeeChequeNo = '{$FeeChequeNo}'
                    WHERE
                    o.RecordID = '" . $recordID . "'
        ";
        return $this->db_db_query($sql);
    }

    /**
     * Portal - update application status in table view
     */
    function updateApplicationStatusByIds($applicationIds, $status)
    {
        $sql = "
            UPDATE
                ADMISSION_APPLICATION_STATUS s
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
            SET
                  s.Status = '" . $status . "',
                  s.DateModified = NOW(),
                  s.ModifiedBy = '{$this->uid}'
             WHERE
                o.RecordID IN (" . $applicationIds . ")
        ";

        return $this->db_db_query($sql);
    }

    /**
     * Portal - get admission settings
     */
    function getBasicSettings($schoolYearID = '', $SettingNameAry = array())
    {
        $schoolYearID = $schoolYearID ? $schoolYearID : $this->schoolYearID;
        $sql = "
            SELECT
                SettingName,
                SettingValue
            FROM
                ADMISSION_SETTING
            WHERE
                SchoolYearID = '" . $schoolYearID . "'
        ";
        if (sizeof($SettingNameAry) > 0) {
            $sql .= " AND
                        SettingName in ('" . implode("','", $SettingNameAry) . "')";
        }
        $setting = $this->returnArray($sql);
        for ($i = 0; $i < sizeof($setting); $i ++) {
            $Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
        }
        return $Return;
    }

    /**
     * Portal - update admission settings
     */
    function saveBasicSettings($schoolYearID = '', $SettingNameValueAry = array())
    {
        if (count($SettingNameValueAry) == 0 || ! is_array($SettingNameValueAry))
            return false;
        $schoolYearID = $schoolYearID ? $schoolYearID : $this->schoolYearID;
        $this->Start_Trans();

        $SettingsNameAry = array_keys($SettingNameValueAry);

        $result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID, $SettingsNameAry);
        $result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID, $SettingNameValueAry);

        if (in_array(false, $result)) {
            $this->RollBack_Trans();
            return false;
        }

        $this->Commit_Trans();
        return true;
    }
    
	/**
     * Portal - log admission settings
     */
    function logBasicSettings($schoolYearID, $SettingsNameAry)
    {
        if (count($SettingsNameAry) == 0)
            return false;

        $SettingsNameSql = "'" . implode("','", (array) $SettingsNameAry) . "'";

		$sql = "INSERT INTO ADMISSION_SETTING_HISTORY
            SELECT * from ADMISSION_SETTING WHERE SettingName IN (" . $SettingsNameSql . ") AND SchoolYearID = '" . $schoolYearID . "'";

		return $this->db_db_query($sql);
    }

    /**
     * Portal - remove admission settings
     */
    function removeBasicSettings($schoolYearID, $SettingsNameAry)
    {
        if (count($SettingsNameAry) == 0)
            return false;

        $SettingsNameSql = "'" . implode("','", (array) $SettingsNameAry) . "'";

        $sql = "
            DELETE FROM
                ADMISSION_SETTING
            WHERE
                SettingName IN (" . $SettingsNameSql . ")
            AND SchoolYearID = '" . $schoolYearID . "'
        ";

        return $this->db_db_query($sql);
    }

    /**
     * Portal - add admission settings
     */
    function insertBasicSettings($schoolYearID, $SettingNameValueAry)
    {
        if (count($SettingNameValueAry) == 0 || ! is_array($SettingNameValueAry))
            return false;

        foreach ((array) $SettingNameValueAry as $_settingName => $_settingValue) {
            $InsertSqlArr[] = "('" . $schoolYearID . "','" . $_settingName . "','" . $_settingValue . "', '" . $this->uid . "', NOW(), '" . $this->uid . "', NOW())";
        }

        if (count($InsertSqlArr) > 0) {
            $InsertSql = implode(',', $InsertSqlArr);

            $sql = "
            INSERT INTO    ADMISSION_SETTING
            (SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)
            VALUES
            $InsertSql
            ";

            return $this->db_db_query($sql);
        }
        return false;
    }

    /**
     * Portal - ???
     * /
     * function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
     * {
     * global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
     * include_once($intranet_root."/includes/libwebmail.php");
     * $libwebmail = new libwebmail();
     *
     * $from = $libwebmail->GetWebmasterMailAddress();
     *
     * if($subject == ''){
     * $email_subject = "聖士提反女子中學附屬幼稚園入學申請通知 St. Stephen's Girls' College Kindergarten Admission Notification";
     * }
     * else{
     * $email_subject = $subject;
     * }
     * $email_message = $message;
     * $sent_ok = true;
     * if($to_email != '' && intranet_validateEmail($to_email)){
     * $sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
     * }else{
     * $sent_ok = false;
     * }
     *
     * return $sent_ok;
     * }/*
     */

    /**
     * Portal - export application for import student/parnet
     */
    function getExportDataForImportAccount($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '', $tabID = '')
    {
        global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;

        $studentInfo = current($this->getApplicationStudentInfo($schoolYearID, $classLevelID, $applicationID, '', $recordID));
        $parentInfo = $this->getApplicationParentInfo($schoolYearID, $classLevelID, $applicationID, $recordID);
        $otherInfo = current($this->getApplicationOthersInfo($schoolYearID, $classLevelID, $applicationID, $recordID));
        $status = current($this->getApplicationStatus($schoolYearID, $classLevelID, $applicationID, $recordID));

        $dataArray = array();

        if ($tabID == 2) { // Student
            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
            $studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
            $dataArray[0] = array();
            $dataArray[0][] = ''; // UserLogin
            $dataArray[0][] = ''; // Password
            $dataArray[0][] = ''; // UserEmail
            $dataArray[0][] = $studentInfo['student_name_en']; // EnglishName
            $dataArray[0][] = $studentInfo['student_name_b5']; // ChineseName
            $dataArray[0][] = ''; // NickName
            $dataArray[0][] = $studentInfo['gender']; // Gender
            $dataArray[0][] = $studentInfo['homephoneno']; // Mobile
            $dataArray[0][] = ''; // Fax
            $dataArray[0][] = ''; // Barcode
            $dataArray[0][] = ''; // Remarks
            $dataArray[0][] = $studentInfo['dateofbirth'];
            ; // DOB
            $dataArray[0][] = $studentInfo['Address']; // Address
            if ((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment'])) {
                $dataArray[0][] = ''; // CardID
                if ($sys_custom['SupplementarySmartCard']) {
                    $dataArray[0][] = ''; // CardID2
                    $dataArray[0][] = ''; // CardID3
                }
            }
            if ($special_feature['ava_hkid'])
                $dataArray[0][] = $studentInfo['birthcertno']; // HKID
            if ($special_feature['ava_strn'])
                $dataArray[0][] = ''; // STRN
            if ($plugin['medical'])
                $dataArray[0][] = ''; // StayOverNight
            $dataArray[0][] = $studentInfo['Nationality']; // Nationality
            $dataArray[0][] = $studentInfo['PlaceOfBirth']; // PlaceOfBirth
            $dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); // AdmissionDate
        } elseif ($tabID == 3) { // Parent
            $hasParent = false;
            $dataCount = array();
            $studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
            for ($i = 0; $i < count($parentInfo); $i ++) {
                if ($parentInfo[$i]['type'] == 'F' && ! $hasParent) {
                    $dataArray[0] = array();
                    $dataArray[0][] = ''; // UserLogin
                    $dataArray[0][] = ''; // Password
                    $dataArray[0][] = $parentInfo[$i]['email']; // UserEmail
                    $dataArray[0][] = $parentInfo[$i]['parent_name_en']; // EnglishName
                    $dataArray[0][] = $parentInfo[$i]['parent_name_b5']; // ChineseName
                    $dataArray[0][] = 'M'; // Gender
                    $dataArray[0][] = $parentInfo[$i]['mobile']; // Mobile
                    $dataArray[0][] = ''; // Fax
                    $dataArray[0][] = ''; // Barcode
                    $dataArray[0][] = ''; // Remarks
                    if ($special_feature['ava_hkid'])
                        $dataArray[0][] = ''; // HKID
                    $dataArray[0][] = ''; // StudentLogin1
                    $dataArray[0][] = $studentInfo['student_name_en']; // StudentEngName1
                    $dataArray[0][] = ''; // StudentLogin2
                    $dataArray[0][] = ''; // StudentEngName2
                    $dataArray[0][] = ''; // StudentLogin3
                    $dataArray[0][] = ''; // StudentEngName3
                                          // $hasParent = true;

                    $dataCount[0] = ($parentInfo[$i]['email'] ? 1 : 0) + ($parentInfo[$i]['parent_name_en'] ? 1 : 0) + ($parentInfo[$i]['parent_name_b5'] ? 1 : 0) + ($parentInfo[$i]['mobile'] ? 1 : 0);
                } elseif ($parentInfo[$i]['type'] == 'M' && ! $hasParent) {
                    $dataArray[1] = array();
                    $dataArray[1][] = ''; // UserLogin
                    $dataArray[1][] = ''; // Password
                    $dataArray[1][] = $parentInfo[$i]['email']; // UserEmail
                    $dataArray[1][] = $parentInfo[$i]['parent_name_en']; // EnglishName
                    $dataArray[1][] = $parentInfo[$i]['parent_name_b5']; // ChineseName
                    $dataArray[1][] = 'F'; // Gender
                    $dataArray[1][] = $parentInfo[$i]['mobile']; // Mobile
                    $dataArray[1][] = ''; // Fax
                    $dataArray[1][] = ''; // Barcode
                    $dataArray[1][] = ''; // Remarks
                    if ($special_feature['ava_hkid'])
                        $dataArray[1][] = ''; // HKID
                    $dataArray[1][] = ''; // StudentLogin1
                    $dataArray[1][] = $studentInfo['student_name_en']; // StudentEngName1
                    $dataArray[1][] = ''; // StudentLogin2
                    $dataArray[1][] = ''; // StudentEngName2
                    $dataArray[1][] = ''; // StudentLogin3
                    $dataArray[1][] = ''; // StudentEngName3
                                          // $hasParent = true;
                    $dataCount[1] = ($parentInfo[$i]['email'] ? 1 : 0) + ($parentInfo[$i]['parent_name_en'] ? 1 : 0) + ($parentInfo[$i]['parent_name_b5'] ? 1 : 0) + ($parentInfo[$i]['mobile'] ? 1 : 0);
                } elseif ($parentInfo[$i]['type'] == 'G' && ! $hasParent) {
                    $dataArray[2] = array();
                    $dataArray[2][] = ''; // UserLogin
                    $dataArray[2][] = ''; // Password
                    $dataArray[2][] = $parentInfo[$i]['email']; // UserEmail
                    $dataArray[2][] = $parentInfo[$i]['parent_name_en']; // EnglishName
                    $dataArray[2][] = $parentInfo[$i]['parent_name_b5']; // ChineseName
                    $dataArray[2][] = ''; // Gender
                    $dataArray[2][] = $parentInfo[$i]['mobile']; // Mobile
                    $dataArray[2][] = ''; // Fax
                    $dataArray[2][] = ''; // Barcode
                    $dataArray[2][] = ''; // Remarks
                    if ($special_feature['ava_hkid'])
                        $dataArray[2][] = ''; // HKID
                    $dataArray[2][] = ''; // StudentLogin1
                    $dataArray[2][] = $studentInfo['student_name_en']; // StudentEngName1
                    $dataArray[2][] = ''; // StudentLogin2
                    $dataArray[2][] = ''; // StudentEngName2
                    $dataArray[2][] = ''; // StudentLogin3
                    $dataArray[2][] = ''; // StudentEngName3
                                          // $hasParent = true;
                    $dataCount[2] = ($parentInfo[$i]['email'] ? 1 : 0) + ($parentInfo[$i]['parent_name_en'] ? 1 : 0) + ($parentInfo[$i]['parent_name_b5'] ? 1 : 0) + ($parentInfo[$i]['mobile'] ? 1 : 0);
                }
            }
            if ($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]) {
                $tempDataArray = $dataArray[0];
            } elseif ($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]) {
                $tempDataArray = $dataArray[1];
            } elseif ($dataCount[2] > 0) {
                $tempDataArray = $dataArray[2];
            }
            $dataArray = array();
            $dataArray[0] = $tempDataArray;
        }
        $ExportArr = $dataArray;

        return $ExportArr;
    }

    abstract function getExportHeader($schoolYearID = '',$classLevelID = '');

    abstract function getExportData($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '');

    /**
     * Portal - get encrypted file name while upload
     */
    function encrypt_attachment($file)
    {
        list ($filename, $ext) = explode('.', $file);
        $timestamp = date("YmdHis");
        return base64_encode($filename . '_' . $timestamp) . '.' . $ext;
    }

    /**
     * Function below are copy from other school
     */
    function removeApplicationAttachment($data)
    {
        global $file_path;
        extract($data);
        if (empty($recordID))
            return;
        $cond .= ! empty($attachment_type) ? " AND r.AttachmentType='" . $attachment_type . "'" : "";
        $sql = "SELECT
                    r.RecordID attachment_id,
                    r.AttachmentName attachment_name,
                    r.AttachmentType attachment_type
                FROM
                    ADMISSION_ATTACHMENT_RECORD r
                INNER JOIN
                    ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
                WHERE
                    o.RecordID = '" . $recordID . "'
                    " . $cond . "
                ";
        $applicantAry = $this->returnArray($sql);

        $Success = array();
        for ($i = 0; $i < count($applicantAry); $i ++) {
            $_attachmentName = $applicantAry[$i]['attachment_name'];
            $_attachmentType = $applicantAry[$i]['attachment_type'] == 'personal_photo' ? $applicantAry[$i]['attachment_type'] : 'other_files';
            $_attachmentId = $applicantAry[$i]['attachment_id'];
            $image_url = $this->filepath . $recordID . '/' . $_attachmentType . '/' . $_attachmentName;

            if (file_exists($file_path . $image_url)) {
                unlink($file_path . $image_url);
                $sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '" . $_attachmentId . "'";

                $Success[] = $this->db_db_query($sql);
            }
        }
        if (in_array(false, $Success)) {
            return false;
        } else {
            return true;
        }
    }

    function saveApplicationAttachment($data)
    {
        global $UserID;
        extract($data);
        if (empty($recordID))
            return;
        $sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '" . $recordID . "'";
        $applicationID = current($this->returnVector($sql));

        if (! empty($applicationID) && ! empty($attachment_name) && ! empty($attachment_type)) {
            $result = $this->removeApplicationAttachment($data);
            if ($result) {
                $sql = "INSERT INTO
                            ADMISSION_ATTACHMENT_RECORD
                                (ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy, OriginalAttachmentName)
                            VALUES
                                ('" . $applicationID . "','" . $attachment_type . "','" . $attachment_name . "',NOW(),'" . $UserID . "','" . $original_attachment_name . "')
                        ";
                return $this->db_db_query($sql);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getApplicationAttachmentRecord($schoolYearID, $data = array())
    {
        extract($data);
        $cond = ! empty($applicationID) ? " AND r.ApplicationID='" . $applicationID . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= ! empty($attachment_type) ? " AND r.AttachmentType='" . $attachment_type . "'" : "";
        $sql = "
            SELECT
                 o.RecordID folder_id,
                  r.RecordID,
                 r.ApplicationID applicationID,
                 r.AttachmentType attachment_type,
                 r.AttachmentName attachment_name,
                 r.DateInput dateinput,
                 r.InputBy inputby,
                r.OriginalAttachmentName
            FROM
                ADMISSION_ATTACHMENT_RECORD r
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
            WHERE
                o.ApplyYear = '" . $schoolYearID . "'
            " . $cond . "
            ORDER BY r.AttachmentType, r.RecordID desc
        ";
        $result = $this->returnArray($sql);
        $attachmentAry = array();
        for ($i = 0; $i < count($result); $i ++) {
            $_attachType = $result[$i]['attachment_type'];
            $_originalAttachName = $result[$i]['OriginalAttachmentName'];
            $_attachName = $result[$i]['attachment_name'];
            $_applicationId = $result[$i]['applicationID'];
            $_folderId = $result[$i]['folder_id'];
            $attachmentAry[$_applicationId][$_attachType]['original_attachment_name'][] = $_originalAttachName;
            $attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;
            $attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId . '/' . ($_attachType == 'personal_photo' ? $_attachType : 'other_files') . '/' . $_attachName;
        }
        return $attachmentAry;
    }

    function getApplicationOthersInfo($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg;
        $cond = ! empty($applicationID) ? " AND ApplicationID='" . $applicationID . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($recordID) ? " AND RecordID='" . $recordID . "'" : "";

        $sql = "
            SELECT
                *,
                ApplicationID applicationID,
                ApplyYear schoolYearId,
                ApplyLevel classLevelID,
                IsConsiderAlternative OthersIsConsiderAlternative
            FROM
                ADMISSION_OTHERS_INFO
            WHERE
                ApplyYear = '" . $schoolYearID . "'
            " . $cond . "
        ";
        return $this->returnArray($sql);
    }

    function getApplicationParentInfo($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        $cond = ! empty($applicationID) ? " AND pg.ApplicationID='" . $applicationID . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
            SELECT
                pg.*,
                 pg.ApplicationID applicationID,
                 pg.EnglishName parent_name_en,
                 pg.ChineseName parent_name_b5,
                 pg.PG_TYPE type,
                pg.Company companyname,
                pg.OfficeAddress companyaddress,
                 pg.JobTitle occupation,
                 pg.Mobile mobile,
                pg.Email email,
                pg.LevelOfEducation levelofeducation,
                pg.LastSchool lastschool,
                pg.Relationship relationship,
                 o.ApplyLevel classLevelID
            FROM
                ADMISSION_PG_INFO pg
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
            WHERE
                o.ApplyYear = '" . $schoolYearID . "'
            " . $cond . "
        ";

        return $this->returnArray($sql);
    }

    function getApplicationStatus($schoolYearID, $classLevelID = '', $applicationID = '', $recordID = '')
    {
        global $admission_cfg;
        $cond = ! empty($applicationID) ? " AND s.ApplicationID='" . $applicationID . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
            SELECT
                 s.ApplicationID applicationID,
                 s.ReceiptID receiptID,
                IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
                IF(s.Handler," . getNameFieldByLang2("iu.") . ",'') AS handler,
                s.Handler handler_id,
                s.InterviewDate As interviewdate,
                s.InterviewLocation As interviewlocation,
                s.Remark remark,CASE ";
        foreach ($admission_cfg['Status'] as $_key => $_value) {
            $sql .= "WHEN s.Status = " . $_value . " THEN '" . ($_key) . "' ";
        }
        $sql .= " ELSE s.Status END status,";
        $sql .= "
                CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
                s.DateInput dateinput,
                s.InputBy inputby,
                s.DateModified datemodified,
                s.ModifiedBy modifiedby,
                o.ApplyLevel classLevelID,
                s.FeeBankName,
                s.FeeChequeNo,
                IF(pi.ApplicationID IS NOT NULL , 'OnlinePayment' , IF(s.Status=2, 'OtherPayment' , 'N/A')  ) as OnlinePayment,
                o.RecordID
            FROM
                ADMISSION_APPLICATION_STATUS s
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
            LEFT JOIN
                INTRANET_USER iu ON s.Handler = iu.UserID
            LEFT JOIN
                ADMISSION_PAYMENT_INFO pi ON o.ApplicationID = pi.ApplicationID and pi.payment_status = 'Completed'
            WHERE
                s.SchoolYearID = '" . $schoolYearID . "'
            " . $cond . "
        ";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function uploadAttachment($type, $file, $destination, $randomFileName = "")
    { // The $randomFileName does not contain file extension
        global $admission_cfg, $intranet_root, $libkis;
        include_once ($intranet_root . "/includes/libimage.php");
        $uploadSuccess = false;
        $ext = strtolower(getFileExtention($file['name']));

        // if($type == "personal_photo"){
        // return true;
        // }
        if (! empty($file['tmp_name'])) {
            require_once ($intranet_root . "/includes/admission/class.upload.php");
            $handle = new \Upload($file['tmp_name']);
            if ($handle->uploaded) {
                $handle->Process($destination);
                if ($handle->processed) {
                    $uploadSuccess = true;
                    if ($type == "personal_photo") {
                        /* Fix Image Orientation */
                        $fileExtension = strtolower($ext);
                        if (in_array($fileExtension, array(
                            'jpg',
                            'jpeg',
                            'gif',
                            'png'
                        ))) {
                            $imageInfo = getimagesize($handle->file_dst_pathname);

                            ini_set('memory_limit', '500M');

                            $exif = @exif_read_data($handle->file_dst_pathname);

                            switch ($fileExtension) {
                                case 'gif':
                                    $source = @imagecreatefromgif($handle->file_dst_pathname);
                                    break;
                                case 'png':
                                    $source = @imagecreatefrompng($handle->file_dst_pathname);
                                    break;
                                default:
                                    $source = @imagecreatefromjpeg($handle->file_dst_pathname);
                            }
                            if ($source) {
                                // rotate
                                if (! empty($exif['Orientation'])) {
                                    switch ($exif['Orientation']) {
                                        case 8:
                                            $source = imagerotate($source, 90, 0);
                                            $needUpdate = true;
                                            break;
                                        case 3:
                                            $source = imagerotate($source, 180, 0);
                                            $needUpdate = true;
                                            break;
                                        case 6:
                                            $source = imagerotate($source, - 90, 0);
                                            $needUpdate = true;
                                            break;
                                    }
                                }

                                if ($needUpdate) {
                                    unlink($handle->file_dst_pathname);
                                    switch ($fileExtension) {
                                        case 'gif':
                                            imagegif($source, $handle->file_dst_pathname);
                                            break;
                                        case 'png':
                                            imagepng($source, $handle->file_dst_pathname);
                                            break;
                                        default:
                                            imagejpeg($source, $handle->file_dst_pathname);
                                    }
                                }
                            }
                        }

                        $image_obj = new \SimpleImage();
                        $image_obj->load($handle->file_dst_pathname);
                        if ($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
                            $image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
                        else
                            $image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
                            // rename the file and then save

                        $image_obj->save($destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext, $image_obj->image_type);
                        unlink($handle->file_dst_pathname);
                    } else {
                        rename($handle->file_dst_pathname, $destination . "/" . ($randomFileName ? $randomFileName : $type) . "." . $ext);
                    }
                    // $cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
                } else {
                    // one error occured
                    $uploadSuccess = false;
                }
                // we delete the temporary files
                $handle->Clean();
            }
        } else {
            if ($this->isInternalUse($_REQUEST['token'])) {
                return true;
            }
        }
        return $uploadSuccess;
        // }
        // return true;
    }

    function moveUploadedAttachment($tempFolderPath, $destFolderPath)
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

        $lfs = new \libfilesystem();
        $uploadSuccess[] = $lfs->lfs_copy($tempFolderPath . "/personal_photo", $destFolderPath);
        $uploadSuccess[] = $lfs->lfs_copy($tempFolderPath . "/other_files", $destFolderPath);
        $uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);

        if (in_array(false, $uploadSuccess))
            return false;
        else
            return true;
    }

    public function displayPresetCodeSelection($code_type = "", $selection_name = "", $code_selected = "")
    {
        $sql = "select Code, " . Get_Lang_Selection("NameChi", "NameEng") . "  from PRESET_CODE_OPTION where CodeType='" . $code_type . "' and RecordStatus=1 order by DisplayOrder";
        $result = $this->returnArray($sql);
        return getSelectByArray($result, "name=" . $selection_name, $code_selected, 0, 1);
    }

    public function returnPresetCodeName($code_type = "", $selected_code = "")
    {
        $sql = "select " . Get_Lang_Selection("NameChi", "NameEng") . "  from PRESET_CODE_OPTION where CodeType='" . $code_type . "' and Code='" . $selected_code . "'";
        $result = $this->returnVector($sql);
        return $result[0];
    }

    function displayWarningMsg($warning)
    {
        global $kis_lang;
        $x = '
         <fieldset class="warning_box">
            <legend>' . $kis_lang['warning'] . '</legend>
            <ul>
                <li>' . $kis_lang['msg'][$warning] . '</li>
            </ul>
        </fieldset>

        ';
        return $x;
    }

    function insertAttachmentRecord($AttachmentType, $AttachmentName, $ApplicationID)
    {
        if ($ApplicationID != "") {
            $sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
                            ApplicationID,
                           AttachmentType,
                            AttachmentName,
                           DateInput)
                    VALUES (
                        '" . $ApplicationID . "',
                        '" . $AttachmentType . "',
                        '" . $AttachmentName . "',
                        now())
            ";
            return $this->db_db_query($sql);
        }
        return false;
    }

    function getAttachmentByApplicationID($schoolYearID, $applicationID)
    {
        global $file_path;
        $attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID, array(
            "applicationID" => $applicationID
        ));
        $attachment = array();
        foreach ((array) $attachmentAry[$applicationID] as $_type => $_attachmentAry) {
            $_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
            if (! empty($_thisAttachment)) {
                $_thisAttachment = $this->filepath . $_thisAttachment;
                if (file_exists($file_path . $_thisAttachment)) {
                    $attachment[$_type]['originalName'] = $attachmentAry[$applicationID][$_type]['original_attachment_name'];
                    $attachment[$_type]['link'] = $_thisAttachment;
                } else {
                    $attachment[$_type]['link'] = false;
                }
            } else {
                $attachment[$_type]['link'] = false;
            }
        }
        return $attachment;
    }

    public function getApplicationNumber($RecordIDAry)
    {
        $sql = 'SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID IN ("' . implode('","', $RecordIDAry) . '")';
        $result = $this->returnVector($sql);
        return $result;
    }

    function getApplicationStudentInfo($schoolYearID, $classLevelID = '', $applicationID = '', $status = '', $recordID = '')
    {
        $cond = ! empty($applicationID) ? " AND stu.ApplicationID='" . $applicationID . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $cond .= ! empty($status) ? " AND s.status='" . $status . "'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $sql = "
            SELECT
                 stu.ApplicationID applicationID,
                 o.ApplyLevel classLevelID,
                 o.ApplyYear schoolYearID,
                stu.*,
                 stu.ChineseName student_name_b5,
                  stu.EnglishName student_name_en,
                  stu.Gender gender,
                stu.BirthCertType birthcerttype,
                  IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,
                  stu.BirthCertNo birthcertno,
                  stu.Email email,
                 " . getNameFieldByLang2("stu.") . " AS student_name,
				stu.PlaceOfBirth placeofbirth,
				stu.County county,
      			stu.HomeTelNo homephoneno,
				stu.ReligionCode religion1,
      			stu.Church religion,
      			stu.LastSchool lastschool,
      			stu.Address homeaddress,
      			stu.AddressChi homeaddresschi,
				stu.Age age,
				stu.IsTwinsApplied istwinsapplied,
				stu.TwinsApplicationID twinsapplicationid,
                 o.ApplyDayType1,
                 o.ApplyDayType2,
                 o.ApplyDayType3
            FROM
                ADMISSION_STU_INFO stu
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
            INNER JOIN
                ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
            WHERE
                o.ApplyYear = '" . $schoolYearID . "'
            " . $cond . "
        ";
        $studentInfoAry = $this->returnArray($sql);
        return $studentInfoAry;
    }

    function getInterviewListAry($recordID = '', $date = '', $startTime = '', $endTime = '', $keyword = '', $order = '', $sortby = '', $round = 1)
    {
        global $admission_cfg;

        // $schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

        if ($recordID != '') {
            $cond = " AND i.RecordID IN ('" . implode("','", (array) ($recordID)) . "') ";
        }
        if ($date != '') {
            $cond .= ' AND i.Date >= \'' . $date . '\' ';
        }
        if ($startTime != '') {
            $cond .= ' AND i.StartTime >= \'' . $startTime . '\' ';
        }
        if ($endTime != '') {
            $cond .= ' AND i.EndTime <= \'' . $endTime . '\' ';
        }
        if ($keyword != '') {
            $search_cond = ' AND (i.Date LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
                            OR i.StartTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\'
                            OR i.EndTime LIKE \'%' . $this->Get_Safe_Sql_Like_Query($keyword) . '%\')';
        }
        $sort = $sortby ? "$sortby $order" : "application_id";

        $from_table = "
            FROM
                ADMISSION_INTERVIEW_SETTING AS i
            LEFT JOIN
                ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID" . ($round > 1 ? $round : '') . "
            INNER JOIN
                ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
            INNER JOIN
                ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
            INNER JOIN
                ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
            WHERE
                1
            " . $cond . "
            order by " . $sort . "
        ";
        $sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
                i.RecordID AS record_id, o.RecordID AS other_record_id,
                 stu.ApplicationID AS application_id,
                 stu.ChineseName AS student_name_b5,
                stu.EnglishName AS student_name_en,
                " . getNameFieldByLang2("stu.") . " AS student_name,
                 " . getNameFieldByLang2("pg.") . " AS parent_name,
                 IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
                CASE
         ";
        foreach ($admission_cfg['Status'] as $_key => $_status) { // e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
            $sql .= " WHEN s.Status = '" . $_status . "' THEN '" . $_key . "' ";
        }
        $sql .= " ELSE s.Status END application_status, stu.BirthCertNo, stu.TwinsApplicationID, stu.LangSpokenAtHome " . $from_table . " ";
        // debug_r($sql);
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function hasApplicationSetting()
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '" . $this->schoolYearID . "' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
        return current($this->returnVector($sql));
    }

    public function returnPresetCodeAndNameArr($code_type = "")
    {
        $sql = "select Code, " . Get_Lang_Selection("NameChi", "NameEng") . "  from PRESET_CODE_OPTION where CodeType='" . $code_type . "'";
        $result = $this->returnArray($sql);
        return $result;
    }

    function hasBirthCertNumber($birthCertNo, $applyLevel)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE aoi.ApplyLevel = '{$applyLevel}' AND TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '" . $this->getNextSchoolYearID() . "'";
        return current($this->returnVector($sql));
    }

    function hasToken($token)
    {
        $sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '" . $token . "' ";
        return current($this->returnVector($sql));
    }

    function getApplicantEmail($applicationId)
    {
        global $intranet_root, $kis_lang, $PATH_WRT_ROOT;

        $sql = "SELECT
                    a.Email as Email
                FROM ADMISSION_STU_INFO as a
                WHERE a.ApplicationID = '" . trim($applicationId) . "'";
        $records = current($this->returnArray($sql));

        return $records['Email'];
    }

    function checkImportDataForImportInterview($data)
    {
        $resultArr = array();
        $i = 0;
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);
            // check date
            if ($aData[4] == '' && $aData[5] == '') {
                $validDate = true;
            } elseif (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4])) {
                list ($year, $month, $day) = explode('-', $aData[4]);
                $validDate = checkdate($month, $day, $year);
            } else {
                $validDate = false;
            }

            // check time
            if ($aData[4] == '' && $aData[5] == '') {
                $validTime = true;
            } elseif (preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5])) {
                $validTime = true;
            } else {
                $validTime = false;
            }
            $sql = "
                SELECT
                    COUNT(*)
                FROM
                    ADMISSION_STU_INFO s
                WHERE
                    trim(s.applicationID) = '" . trim($aData[0]) . "' AND trim(s.birthCertNo) = '" . trim($aData[3]) . "'
            ";
            $result = $this->returnVector($sql);
            if ($result[0] == 0) {
                $resultArr[$i]['validData'] = $aData[0];
            } else
                $resultArr[$i]['validData'] = false;
            $resultArr[$i]['validDate'] = $validDate;
            $resultArr[$i]['validTime'] = $validTime;
            if (! $validDate || ! $validTime)
                $resultArr[$i]['validData'] = $aData[0];
            $i ++;
        }
        return $resultArr;
    }

    function importDataForImportInterview($data)
    {
        $resultArr = array();
        foreach ($data as $aData) {
            $aData[4] = getDefaultDateFormat($aData[4]);

            $sql = "
                UPDATE ADMISSION_APPLICATION_STATUS SET
                   InterviewDate = '" . $aData[4] . " " . $aData[5] . "',
                InterviewLocation = '" . $aData[6] . "',
                DateModified = NOW(),
                   ModifiedBy = '" . $this->uid . "'
                   WHERE ApplicationID = '" . $aData[0] . "'
            ";
            $result = $this->db_db_query($sql);
            $resultArr[] = $result;
        }
        return $resultArr;
    }

    function getExportDataForImportInterview($recordID, $schoolYearID = '', $selectStatus = '', $classLevelID = '')
    {
        global $admission_cfg;
        $cond = ! empty($schoolYearID) ? " AND a.SchoolYearID='" . $schoolYearID . "'" : "";
        $cond .= ! empty($recordID) ? " AND o.RecordID='" . $recordID . "'" : "";
        $cond .= ! empty($selectStatus) ? " AND a.Status='" . $selectStatus . "'" : "";
        $cond .= ! empty($classLevelID) ? " AND o.ApplyLevel='" . $classLevelID . "'" : "";
        $sql = "
            SELECT
                 a.ApplicationID applicationID,
                 s.EnglishName englishName,
                s.ChineseName chineseName,
                s.BirthCertNo birthCertNo,
                IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
                IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
                a.InterviewLocation interviewlocation
            FROM
                ADMISSION_APPLICATION_STATUS a
            INNER JOIN
                ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
            INNER JOIN
                ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
            WHERE 1
                " . $cond . "
        ";
        $applicationAry = $this->returnArray($sql);
        return $applicationAry;
    }

    function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1, $classLevelIds=array())
    {
        $status_cond = '';
        if (sizeof($selectStatusArr) > 0) {
            $status_cond .= " AND st.status in ('" . implode("','", $selectStatusArr) . "') ";
        }

        $round_cond = " AND Round = '" . $round . "' ";

        $sql = 'Select o.ApplicationID, o.ApplyLevel, y.YearName, s.DOB, s.IsTwinsApplied, s.LangSpokenAtHome From ADMISSION_OTHERS_INFO as o
                LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID
                LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID
                LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "' . $selectSchoolYearID . '" ' . $status_cond . ' order by FIELD(s.LangSpokenAtHome,"Cantonese","English","Putonghua") desc, y.YearName desc';

        $result = $this->returnArray($sql);

        $allApplicant = array();

        for ($i = 0; $i < sizeof($result); $i ++) {
            $allApplicant[] = array(
                'ApplicationID' => $result[$i]['ApplicationID'],
                'ClassLevel' => $result[$i]['ApplyLevel'],
                'LangSpokenAtHome' => $result[$i]['LangSpokenAtHome']
            );
        }

        // $sql = "SELECT a.RecordID, a.Date, a.NumOfGroup, a.Quota
        // FROM ADMISSION_INTERVIEW_ARRANGEMENT as a
        // ORDER BY a.Date";
        //
        // $arrangmentRecord = $this->returnArray($sql);

        $result = array();

        $TwinsAssignedApplicant = array();

        // for($i=0; $i<sizeof($arrangmentRecord); $i++){
        $sql = "SELECT RecordID, Quota, GroupName FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '" . $selectSchoolYearID . "' $round_cond AND (GroupName = '101' OR GroupName = '102') ORDER BY GroupName, Date, StartTime";
        $interviewRecordIDArr = $this->returnArray($sql);

        for ($j = 0; $j < sizeof($interviewRecordIDArr); $j ++) {

            if (! $allApplicant) {
                break;
            }

            $previousClassLevel = $allApplicant[0]['ClassLevel'];
            $previousInterviewLanguage = $allApplicant[0]['LangSpokenAtHome'];

            if ($allApplicant[0]['LangSpokenAtHome'] == 'Cantonese') {
                break;
            }
            if (($allApplicant[0]['LangSpokenAtHome'] == 'English' && $interviewRecordIDArr[$j]['GroupName'] != '102' || $allApplicant[0]['LangSpokenAtHome'] == 'Cantonese' && $interviewRecordIDArr[$j]['GroupName'] == '102')) {
                continue;
            }

            $sql = "UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '" . $previousClassLevel . "' WHERE RecordID = '" . $interviewRecordIDArr[$j]['RecordID'] . "' ";
            $result[] = $this->db_db_query($sql);

            for ($k = 0; $k < $interviewRecordIDArr[$j]['Quota']; $k ++) {
                $sql = "UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID" . ($round > 1 ? $round : '') . " = '" . $interviewRecordIDArr[$j]['RecordID'] . "' Where ApplicationID = '" . $allApplicant[0]['ApplicationID'] . "' ";
                $result[] = $this->db_db_query($sql);
                array_shift($allApplicant);

                if ($previousClassLevel != $allApplicant[0]['ClassLevel'] || $previousInterviewLanguage != $allApplicant[0]['LangSpokenAtHome']) {
                    break;
                }
            }
        }

        // $sql = "SELECT RecordID, Quota, GroupName, ClassLevelID FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '".$selectSchoolYearID."' $round_cond AND GroupName IS NOT NULL ORDER BY Date, StartTime, GroupName";
        $sql = "SELECT RecordID, Quota, GroupName FROM ADMISSION_INTERVIEW_SETTING WHERE SchoolYearID = '" . $selectSchoolYearID . "' $round_cond AND GroupName IS NOT NULL AND GroupName <> '101' AND GroupName <> '102' ORDER BY Date, StartTime, GroupName";
        $interviewRecordIDArr = $this->returnArray($sql);

        for ($j = 0; $j < sizeof($interviewRecordIDArr); $j ++) {

            if (! $allApplicant) {
                break;
            }

            // if($interviewRecordIDArr[$j]['ClassLevelID'] && $interviewRecordIDArr[$j]['ClassLevelID'] != $allApplicant[0]['ClassLevel']){
            // continue;
            // }

            $previousClassLevel = $allApplicant[0]['ClassLevel'];
            $previousInterviewLanguage = $allApplicant[0]['LangSpokenAtHome'];

            $sql = "UPDATE ADMISSION_INTERVIEW_SETTING SET ClassLevelID = '" . $previousClassLevel . "' WHERE RecordID = '" . $interviewRecordIDArr[$j]['RecordID'] . "' ";
            $result[] = $this->db_db_query($sql);

            for ($k = 0; $k < $interviewRecordIDArr[$j]['Quota']; $k ++) {
                $sql = "UPDATE ADMISSION_OTHERS_INFO Set InterviewSettingID" . ($round > 1 ? $round : '') . " = '" . $interviewRecordIDArr[$j]['RecordID'] . "' Where ApplicationID = '" . $allApplicant[0]['ApplicationID'] . "' ";
                $result[] = $this->db_db_query($sql);
                array_shift($allApplicant);
                if ($previousClassLevel != $allApplicant[0]['ClassLevel'] || $previousInterviewLanguage != $allApplicant[0]['LangSpokenAtHome']) {
                    break;
                }
            }
        }
        // }

        // handling twins swraping [start]
        $sql = 'Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID' . ($round > 1 ? $round : '') . ' as InterviewSettingID, s.LangSpokenAtHome From ADMISSION_OTHERS_INFO as o
                LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID
                LEFT JOIN ADMISSION_APPLICATION_STATUS as st ON o.ApplicationID = st.ApplicationID
                where o.ApplyYear = "' . $selectSchoolYearID . '" AND s.IsTwinsApplied = "1" ' . $status_cond . ' order by FIELD(s.LangSpokenAtHome,"Cantonese","English","Putonghua") desc, o.ApplicationID';

        $twinsResult = $this->returnArray($sql);

        $twinsArray = array();
        $assignedTwins = array();

        for ($i = 0; $i < sizeof($twinsResult); $i ++) {
            for ($j = 0; $j < sizeof($twinsResult); $j ++) {
                if (strtolower(trim($twinsResult[$i]['TwinsApplicationID'])) == strtolower(trim($twinsResult[$j]['BirthCertNo'])) && ! in_array($twinsResult[$j]['ApplicationID'], $assignedTwins)) {

                    $sql = 'Select RecordID, ClassLevelID, Date, GroupName, StartTime From ADMISSION_INTERVIEW_SETTING where RecordID = "' . $twinsResult[$i]['InterviewSettingID'] . '" ';
                    $originalSession = current($this->returnArray($sql));

                    $sql = 'Select RecordID, ClassLevelID, Date, GroupName, StartTime From ADMISSION_INTERVIEW_SETTING where RecordID = "' . $twinsResult[$j]['InterviewSettingID'] . '" ';
                    $twinsOriginalSession = current($this->returnArray($sql));

                    $sql = 'Select i.RecordID From ADMISSION_INTERVIEW_SETTING as i LEFT JOIN ADMISSION_OTHERS_INFO as o ON o.InterviewSettingID' . ($round > 1 ? $round : '') . ' = i.RecordID LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where i.ClassLevelID = "' . $originalSession['ClassLevelID'] . '" AND i.Date = "' . $originalSession['Date'] . '" AND i.StartTime = "' . $originalSession['StartTime'] . '" AND i.Round = "' . $round . '" AND i.GroupName <> "' . $originalSession['GroupName'] . '" AND i.RecordID <> "' . $twinsResult[$i]['InterviewSettingID'] . '" AND i.RecordID <> "' . $twinsResult[$j]['InterviewSettingID'] . '"  AND s.LangSpokenAtHome = "' . $twinsResult[$j]['LangSpokenAtHome'] . '"';
                    $newSession = current($this->returnArray($sql));

                    if ($newSession) {
                        $sql = 'Select o.ApplicationID From ADMISSION_OTHERS_INFO as o LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID where o.InterviewSettingID' . ($round > 1 ? $round : '') . ' = "' . $newSession['RecordID'] . '" AND s.IsTwinsApplied <> "1"';
                        $swapApplicant = current($this->returnArray($sql));
                        if ($swapApplicant) {
                            $sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID' . ($round > 1 ? $round : '') . ' = "' . $twinsOriginalSession['RecordID'] . '" where ApplicationID = "' . $swapApplicant['ApplicationID'] . '" ';
                            // debug_pr($sql);
                            $updateResult = $this->db_db_query($sql);
                            $sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID' . ($round > 1 ? $round : '') . ' = "' . $newSession['RecordID'] . '" where ApplicationID = "' . $twinsResult[$j]['ApplicationID'] . '" ';
                            // debug_pr($sql);
                            $updateResult = $this->db_db_query($sql);
                            $assignedTwins[] = $twinsResult[$j]['ApplicationID'];
                            $assignedTwins[] = $twinsResult[$i]['ApplicationID'];
                        }
                    }
                }
            }
        }

        // handling twins swraping [end]

        return ! in_array(false, $result);
    }

    function getInterviewTimeslotName($recordId, $round)
    {
        $interviewTimeslotName = '';

        $sql = 'Select Date, StartTime, SchoolYearID, Round From ADMISSION_INTERVIEW_SETTING where RecordID = "' . $recordId . '"';
        $result = current($this->returnArray($sql));

        $sql = 'Select distinct Date From ADMISSION_INTERVIEW_SETTING where SchoolYearID = "' . $result['SchoolYearID'] . '" AND Round = "' . $result['Round'] . '" order by Date';
        $result2 = $this->returnArray($sql);

        $sql = 'Select distinct StartTime From ADMISSION_INTERVIEW_SETTING where SchoolYearID = "' . $result['SchoolYearID'] . '" AND Round = "' . $result['Round'] . '" AND Date = "' . $result['Date'] . '" order by StartTime';
        $result3 = $this->returnArray($sql);

        for ($i = 0; $i < count($result2); $i ++) {
            if ($result2[$i]['Date'] == $result['Date']) {
                $interviewTimeslotName .= chr(($i + 65));
                break;
            }
        }

        for ($i = 0; $i < count($result3); $i ++) {
            if ($result3[$i]['StartTime'] == $result['StartTime']) {
                $interviewTimeslotName .= str_pad($i + 1, 2, '0', STR_PAD_LEFT);
                break;
            }
        }

        return $interviewTimeslotName;
    }

    function getInterviewResult($StudentDateOfBirth, $StudentBirthCertNo, $SchoolYearID = '', $round = 1, $ApplicationID = '')
    {
        if ($SchoolYearID) {
            $cond = " AND o.ApplyYear = '" . $SchoolYearID . "' ";
        }
        if ($ApplicationID) {
            $cond .= " AND o.ApplicationID = '" . $ApplicationID . "' ";
        }
        $sql = "SELECT o.ApplicationID, s.ChineseName, s.EnglishName, i.Date, i.StartTime, i.EndTime, i.GroupName, st.InterviewDate, st.InterviewLocation, o.ApplyLevel, i.RecordID, o.RecordID as OthersInfoRecordID
                FROM ADMISSION_STU_INFO as s
                JOIN ADMISSION_OTHERS_INFO as o ON s.ApplicationID = o.ApplicationID
                JOIN ADMISSION_APPLICATION_STATUS as st ON st.ApplicationID = o.ApplicationID
                LEFT JOIN ADMISSION_INTERVIEW_SETTING as i ON o.InterviewSettingID" . ($round > 1 ? $round : '') . " = i.RecordID
                WHERE st.Status <> 5 AND s.DOB = '" . $StudentDateOfBirth . "' AND s.DOB <> '' AND (s.BirthCertNo = '" . strtoupper($StudentBirthCertNo) . "' OR replace(replace(replace(s.BirthCertNo,')',''),'(',''),' ','') = '" . strtoupper($StudentBirthCertNo) . "') AND s.BirthCertNo <> '' " . $cond . " ORDER BY o.ApplicationID desc";

        $result = current($this->returnArray($sql));

        if (! $result['ApplicationID']) {
            return 0;
        }

        // if(!$result['Date']){
        // return 'NotAssigned';
        // }

        return $result;
    }
} // End Class
