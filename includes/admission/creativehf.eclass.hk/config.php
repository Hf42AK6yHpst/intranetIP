<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = '啟思幼稚園幼兒園(杏花邨)';
$admission_cfg['SchoolName']['en'] .= ' (Heng Fa Chuen)';
$admission_cfg['SchoolCode'] = 'HF';
$admission_cfg['SchoolPhone'] = '2595 0638';
$admission_cfg['SchoolAddress']['b5'] = '香港杏花邨盛泰道100號';
$admission_cfg['SchoolAddress']['en'] = '100 Shing Tai Road, Heng Fa Chuen, Hong Kong';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckhf.photo@gmail.com';
}
/* for email [end] */


/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = '9Q3ET7END2NA8';
} else {
    $admission_cfg['paypal_signature'] = '51fqkaqRHlaANJsazOr4cMeGMIGASIv4eNnVeeLXpxkj_-4zUyoG8_KYlAS';
    $admission_cfg['hosted_button_id'] = 'WZV2MUDP4DLA2';
    $admission_cfg['paypal_name'] = 'Creative Day Nursery (Heng Fa)';
}
/* for paypal [End] */