<?php
//using:
include(__DIR__ . '/../creativekt.eclass.hk/config.php');

// ####### Cust config START ########
$admission_cfg['SchoolName']['b5'] = ' 啟思幼稚園幼兒園(匯景)';
$admission_cfg['SchoolName']['en'] = 'Creative Kindergarten & Day Nursery(Sceneway)';
$admission_cfg['SchoolCode'] = 'SG';
$admission_cfg['SchoolPhone'] = '2717 8686 (KG) / 2717 8811 (DN)';
$admission_cfg['SchoolAddress']['b5'] = '九龍藍田茜發道匯景花園';
$admission_cfg['SchoolAddress']['en'] = 'Sceneway Garden, Sin Fat Road, Lam Tin, Kowloon';
// ####### Cust config END ########

/* for email [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['EmailBcc'] = 'hpmak@g2.broadlearning.com';
}else{
    $admission_cfg['EmailBcc'] = 'ckskg.photo@gmail.com';
}
/* for email [end] */


/* for paypal [start] */
if ($plugin['eAdmission_devMode']) {
    $admission_cfg['hosted_button_id'] = '5LYCPLYKG2XNU';
} else {
    $admission_cfg['paypal_signature'] = 'usFThwlfu4db24UN-mrgOtixWQZxgV7Q9RK9tMwG315cFahEZ5u0HP9hs9y';
    $admission_cfg['hosted_button_id'] = '8CYCYELRLAHKE';
    $admission_cfg['paypal_name'] = 'Creative Kindergarten & Day Nursery(Sceneway)';
}
/* for paypal [End] */