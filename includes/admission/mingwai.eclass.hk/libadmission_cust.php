<?php
# modifying by: 

/********************
 * 
 * Log :
 * Date		2015-08-25 (Henry) 
 * 			support interview round logic
 * Date 	2015-08-03 [Henry]
 * 			Added function updateApplicantArrangement()
 * Date		2015-06-30 [Henry]
 * 			File Created
 * 
 ********************/

include_once($intranet_root."/includes/admission/libadmission_cust_base.php");

class admission_cust extends admission_cust_base{
	function admission_cust(){
		global $kis_lang, $UserID; //switch $lang for IP/EJ/KIS
		
		$this->libdb();
		$this->filepath = '/file/admission/';
		$this->pg_type = array_keys($kis_lang['Admission']['PG_Type']);
		$this->schoolYearID = $this->getNextSchoolYearID();
		$this->uid = $UserID;
		$this->classLevelAry = $this->getClassLevel();
	}
	
	function testing(){
		return "test";
	}
	function encrypt_attachment($file){
		list($filename,$ext) = explode('.',$file);
	    $timestamp = date("YmdHis");
	    return base64_encode($filename.'_'.$timestamp).'.'.$ext;
	}
	function getNextSchoolYearID(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$SchoolYearArr = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		$SchoolYearIDArr = BuildMultiKeyAssoc($SchoolYearArr, 'AcademicYearStart', $IncludedDBField=array('AcademicYearID'),1);
		krsort($SchoolYearIDArr);
		
		$SchoolYearIDArr = array_values($SchoolYearIDArr);
		$currentSchoolYear = Get_Current_Academic_Year_ID();
		$key = array_search($currentSchoolYear, $SchoolYearIDArr);
		if($key>0){
			if($SchoolYearIDArr[$key-2] && $this->hasCurrentApplicationSetting($SchoolYearIDArr[$key-2]) > 0){
				return $SchoolYearIDArr[$key-2];
			}
    		return $SchoolYearIDArr[$key-1];
    	}else{
    		return false;
    	}
	}
	function getNextSchoolYearStart(){
		global $intranet_root;
		include_once($intranet_root."/includes/form_class_manage.php");
		$lfcm = new form_class_manage();
		$rs = $lfcm->Get_Academic_Year_List('', $OrderBySequence=1, $excludeYearIDArr=array(), $noPastYear=1, $pastAndCurrentYearOnly=0, $excludeCurrentYear=0);
		
		$nextAcademicYearId = $this->getNextSchoolYearID();
		foreach($rs as $r){
		    if($r['AcademicYearID'] == $nextAcademicYearId){
		        return $r['AcademicYearStart'];
		    }
		}
	}
	function insertApplicationAllInfo($libkis_admission, $Data, $ApplicationID){
	    global $admission_cfg;
		extract($Data);
	    
		$Success = array();
		if($ApplicationID != ""){

    		#### Admission Method START ####
    		if(!$this->isInternalUse($_REQUEST['token'])){
    		    $AdmissionMethod = $admission_cfg['admissionMethod']['Online'];
    		}
    		
    	    $Success[] = $this->insertApplicationCustInfo(array(
    	        'Code' => 'AdmissionMethod',
    	        'Value' => $AdmissionMethod
    	    ), $ApplicationID);
    	    $Success[] = $this->insertApplicationCustInfo(array(
    	        'Code' => 'MailCode',
    	        'Value' => $MailCode
    	    ), $ApplicationID);
    		#### Admission Method END ####
     		
    	    #### Fee START ####
    	    $feeValue = implode(';', (array)$_REQUEST['Fee']);
    	    $Success[] = $this->insertApplicationCustInfo(array(
    	        'Code' => 'Fee',
    	        'Value' => $feeValue
    	    ), $ApplicationID);
    	    #### Fee END ####
			
			
			$Success[] = $this->insertApplicationStatus($libkis_admission, $ApplicationID);
			
			$Success[] = $this->insertApplicationStudentInfo($Data, $ApplicationID);
			
			
			//Pack Parent Info 
			foreach($this->pg_type as $_key => $_pgType){
				if(!empty($ApplicationID)/*&&!empty(${'G'.($_key+1).'EnglishName'})*/){
					$parentInfoAry = array();
					$parentInfoAry['ApplicationID'] = $ApplicationID;
					$parentInfoAry['PG_TYPE'] = $_pgType;
					//$parentInfoAry['lsSingleParents'] = $IsSingleParent;
					//$parentInfoAry['HasFullTimeJob'] = $IsFullTime;
					//$parentInfoAry['IsFamilySpecialCase'] = $IsFamilySpecialCase;
					//$parentInfoAry['IsApplyFullDayCare'] = $IsApplyFullDayCare;
					//$parentInfoAry['IsLiveWithChild'] = ${'G'.($_key+1).'LiveWithChild'};
					//$parentInfoAry['Relationship'] = ${'G'.($_key+1).'Relationship'};
					//$parentInfoAry['EnglishName'] = ${'G'.($_key+1).'EnglishName'};
					$parentInfoAry['ChineseName'] = ${'G'.($_key+1).'ChineseName'};
					//$parentInfoAry['HKID'] = ${'G'.($_key+1).'HKID'};
					//$parentInfoAry['Nationality'] = ${'G'.($_key+1).'Nationality'};
					$parentInfoAry['JobTitle'] = ${'G'.($_key+1).'Occupation'};
					$parentInfoAry['Company'] = ${'G'.($_key+1).'CompanyName'};
					//$parentInfoAry['JobPosition'] = ${'G'.($_key+1).'JobPosition'};
					//$parentInfoAry['OfficeAddress'] = ${'G'.($_key+1).'CompanyAddress'};
					//$parentInfoAry['OfficeTelNo'] = ${'G'.($_key+1).'WorkNo'};
//					$parentInfoAry['Email'] = ${'G'.($_key+1).'Email'};
					$parentInfoAry['Mobile'] = ${'G'.($_key+1).'MobileNo'};	
					//$parentInfoAry['LevelOfEducation'] = ${'G'.($_key+1).'LevelOfEducation'};	
					//$parentInfoAry['LastSchool'] = ${'G'.($_key+1).'LastSchool'};	
					$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
				}
				
 			}
			/*if($G1EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 1);
				
			if($G2EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 2);
				
			if($G3EnglishName)
				$Success[] = $this->insertApplicationParentInfo($Data, $ApplicationID, 3);*/
			
			$Success[] = $this->insertApplicationStudentInfoCust($Data, $ApplicationID);
			
			$Success[] = $this->insertApplicationRelativesInfoCust($Data, $ApplicationID);
			
			$Success[] = $this->insertApplicationOthersInfo($Data, $ApplicationID);
			
			if(in_array(false,$Success)){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}
	
	function insertApplicationStatus($libkis_admission, $ApplicationID){
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){

//   		}
//   		else{
   			//Henry: not yet finish
   			$sql = "INSERT INTO ADMISSION_APPLICATION_STATUS (
     					SchoolYearID,
     					ApplicationID,
					    Status,
					    DateInput)
					VALUES (
						'".$this->schoolYearID."',
						'".$ApplicationID."',
						'".($this->isInternalUse($_REQUEST['token'])?'2':'1')."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	function updateApplicationStatus($data){
		global $UserID, $PATH_WRT_ROOT, $setting_path_ip_rel;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
 		$result = true;
 		
 		#### Briefing START ####
        include_once ("{$PATH_WRT_ROOT}includes/admission/libadmission_briefing_base.php");
        include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/admission_briefing.php");
        $objBreafing = new admission_briefing();
        
 		if(isset($data['briefingsession'])){
 		    $sql = "SELECT Briefing_ApplicantID, BriefingID FROM ADMISSION_BRIEFING_APPLICANT_INFO WHERE ApplicantID='{$ApplicationID}'";
 		    $rs = $this->returnResultSet($sql);
 		    
 		    if($briefingsession == '' || $BreafingReserveSeats == 0){
 		        $result = $objBreafing->deleteBriefingApplicant($rs[0]['Briefing_ApplicantID'], true);

 		        $sql = "UPDATE ADMISSION_APPLICATION_STATUS SET BriefingDate='', BriefingInfo='' WHERE ApplicationID='{$ApplicationID}'";
 		        $result = $result && $this->db_db_query($sql);
 		        
 		        $briefingdate = $briefinginformation = '';
 		    }else{
     		    if(count($rs) == 0){
     		        $StudentInfo = $this->getApplicationStudentInfo($schoolYearId,'',$ApplicationID);
     		        $StudentInfo = $StudentInfo[0];
     		        $ParentInfo = $this->getApplicationParentInfo($schoolYearId,'',$ApplicationID);
     		        $ParentInfo = BuildMultiKeyAssoc($ParentInfo[0], array('type'));
        		    $data = array(
        		        'BriefingID' => $briefingsession,
        		        'Email' => $StudentInfo['Email'],
        		        'ApplicationID' => $ApplicationID,
        		        'BirthCertNo' => $StudentInfo['birthcertno'],
        		        'ParentName' => trim("{$ParentInfo['F']['ChineseName']}, {$ParentInfo['M']['ChineseName']}", ', '),
        		        'StudentName' => $StudentInfo['student_name_b5'],
        		        'PhoneNo' => $StudentInfo['homephoneno'],
        		        'SeatRequest' => $BreafingReserveSeats,
        		    );
        		    $result = $objBreafing->insertBriefingApplicant($this->schoolYearID, $data);
     		    }else{
     		        $sql = "UPDATE ADMISSION_BRIEFING_APPLICANT_INFO SET BriefingID='{$briefingsession}', SeatRequest='{$BreafingReserveSeats}' WHERE ApplicantID='{$ApplicationID}'";
         		    $result = $this->db_db_query($sql);
     		    }
     		    
     		    $briefingSessionInfo = $objBreafing->getBriefingSession($rs[0]['BriefingID']);
     		    $briefingdate = $briefingSessionInfo['BriefingStartDate'];
     		    $briefinginformation = $briefingSessionInfo['Title'];
 		    }
 		}
 		#### Briefing END ####
 		
 		#### Admission Method START ####
	    $result = $result && $this->updateApplicationCustInfo(array(
	        'filterApplicationId' => $ApplicationID,
	        'filterCode' => 'AdmissionMethod',
	        'Value' => $AdmissionMethod
	    ));
	    if(isset($MailCode)){
    	    $result = $result && $this->updateApplicationCustInfo(array(
    	        'filterApplicationId' => $ApplicationID,
    	        'filterCode' => 'MailCode',
    	        'Value' => $MailCode
    	    ));
	    }
 		#### Admission Method END ####
 		
	    #### Fee START ####
	    $feeValue = implode(';', (array)$Fee);
	    $result = $result && $this->updateApplicationCustInfo(array(
	        'filterApplicationId' => $ApplicationID,
	        'filterCode' => 'Fee',
	        'Value' => $feeValue
	    ), 1);
	    #### Fee END ####
 		
 		if(!empty($interviewdate)){
 			$interviewdate .= " ".str_pad($interview_hour,2,"0",STR_PAD_LEFT).":".str_pad($interview_min,2,"0",STR_PAD_LEFT).":".str_pad($interview_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(!empty($briefingdate)){
 			//$briefingdate .= " ".str_pad($briefing_hour,2,"0",STR_PAD_LEFT).":".str_pad($briefing_min,2,"0",STR_PAD_LEFT).":".str_pad($briefing_sec,2,"0",STR_PAD_LEFT);
 		}
 		if(empty($receiptID)){
 			$receiptdate = '';
 			$handler = '';
 		}
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
			SET
				s.ReceiptID = '".$receiptID."',
     			s.ReceiptDate = '".$receiptdate."',
      			s.Handler = '".$handler."', 
      			s.InterviewDate = '".$interviewdate."',
				s.InterviewLocation = '".$interviewlocation."',
				s.BriefingDate = '".$briefingdate."',
				s.BriefingInfo = '".$briefinginformation."',
      			s.Remark = '".$remark."',	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."',
      			s.isNotified = '".$isnotified."',
				o.InterviewSettingID =  '".$InterviewSettingID."', 
				o.InterviewSettingID2 =  '".$InterviewSettingID2."',
				o.InterviewSettingID3 =  '".$InterviewSettingID3."'  
     		WHERE 
				o.RecordID = '".$recordID."'	
    	";
    	$result = $result && $this->db_db_query($sql);
    	return $result;
	}  
	function updateApplicationStatusByIds($applicationIds,$status){
		global $UserID;
		$sql = "
			UPDATE 
				ADMISSION_APPLICATION_STATUS s 
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID 
			SET	
      			s.Status = '".$status."',
      			s.DateModified = NOW(),
      			s.ModifiedBy = '".$UserID."'
     		WHERE 
				o.RecordID IN (".$applicationIds.")
    	";
    	return $this->db_db_query($sql);
	}	  	
   	function insertApplicationStudentInfo($Data, $ApplicationID){
   		extract($Data);
   		global $admission_cfg;
   		
   		#Check exist application
   		if($ApplicationID != ""){
   		    if($SpokenLanguage == $admission_cfg['lang']['others']){
   		        $SpokenLanguage = $SpokenLanguageOther;
   		    }
   		    
   		    if(!$twins){
   		        $twinsapplicationid = '';
   		    }
   		    
   		    #### Email Role START ####
   		    $this->insertApplicationCustInfo(array(
   		        'Code' => 'PrimaryEmailRole',
   		        'Value' => $PrimaryEmailRole
   		    ), $ApplicationID);
   		    $this->insertApplicationCustInfo(array(
   		        'Code' => 'SecondaryEmailRole',
   		        'Value' => $SecondaryEmailRole
   		    ), $ApplicationID);
   		    #### Email Role END ####
   		    
   			$sql = "INSERT INTO ADMISSION_STU_INFO (
						ApplicationID,
		   				 ChineseName,
		  				 EnglishName,   
		   				 Gender,
		  				 DOB,
		   				 BirthCertNo,
		   				 PlaceOfBirth,
						 Nationality,
						 HomeTelNo,
   						 Fax,
   			             AddressRoom,
   			             AddressFloor,
   			             AddressBlock,
   			             AddressBldg,
   			             AddressStreet,
   			             AddressDistrict,
   			             AddressEstate,
						 Email,
						 Email2,
						 LangSpokenAtHome,
   			             IsTwinsApplied,
   			             TwinsApplicationID,
   						 LastSchool,
   						 LastSchoolLevel,
						 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$studentsname_b5."',
						'".$studentsname_en."',
						'".$FormStudentInfo_StuGender."',
						'".$FormStudentInfo_DOB."',
						'".$StudentBirthCertNo."',
						'".$FormStudentInfo_POB."',
						'".$FormStudentInfo_Nationality."',
						'".$FormStudentInfo_HomeTel."',
						'".$FormStudentInfo_Fax."',
						'".$HomeAddrRoom."',
						'".$HomeAddrFloor."',
						'".$HomeAddrBlock."',
						'".$HomeAddrBlding."',
						'".$HomeAddrStreet."',
						'".$HomeAddrDistrict."',
						'".$HomeAddrRegion."',
						'".$PrimaryEmail."',
						'".$SecondaryEmail."',
						'".$SpokenLanguage."',
						'".$twins."',
						'".$twinsapplicationid."',
						'".$LastSchool."',
						'".$LastSchoolLevel."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
    }
    
    function insertApplicationParentInfo($Data){
		$fieldname = '';
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname.",";
			$fieldvalue .= "'".$_fieldvalue."',";
		}
	
		$sql = "INSERT INTO ADMISSION_PG_INFO (
					".$fieldname."
					DateInput
				)VALUES (
					".$fieldvalue."
					NOW()
				)";
		return $this->db_db_query($sql);
    }
     function updateApplicationParentInfo($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
		$sql = "UPDATE ADMISSION_PG_INFO SET
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		return $this->db_db_query($sql);
    }
    
     function updateApplicationStudentInfoCust($Data){
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		$sql = "UPDATE ADMISSION_STU_PREV_SCHOOL_INFO
				SET 
					".$fieldname."
					DateModified = NOW(),
					ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$Data['RecordID']."'
				";
		$success[] = $this->db_db_query($sql);
				
		if($Data['Year'] == '' && $Data['Class'] =='' && $Data['NameOfSchool'] ==''){
			$sql = "DELETE FROM ADMISSION_STU_PREV_SCHOOL_INFO WHERE RecordID = '".$Data['RecordID']."'";
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
    }
    
    function updateApplicationRelativesInfoCust($Data){
     	
     	global $UserID;
		$fieldvalue = '';
		foreach($Data as $_fieldname => $_fieldvalue){
			$fieldname .= $_fieldname." = '".$_fieldvalue."',";
		}
	
		if($Data['RecordID'] != 'new'){
    		$sql = "UPDATE ADMISSION_RELATIVES_AT_SCH_INFO
    				SET 
    					".$fieldname."
    					DateModified = NOW(),
    					ModifiedBy = '".$UserID."'
    				WHERE
    					RecordID = '".$Data['RecordID']."'
    				";
    		$success[] = $this->db_db_query($sql);
		}
				
		if($Data['Year'] == '' && $Data['Name'] =='' && $Data['ClassPosition'] =='' && $Data['Relationship'] ==''){
			$sql = "DELETE FROM ADMISSION_RELATIVES_AT_SCH_INFO WHERE RecordID = '".$Data['RecordID']."'";
			$success[] = $this->db_db_query($sql);
		}
		return !in_array(false, $success);
    }
       
    function insertApplicationOthersInfo($Data, $ApplicationID){    	
   		extract($Data);
//   		debug_pr($Data);
//   		debug_pr($StudentChiName);
   		#Check exist application
   		if($ApplicationID != ""){
   		    if(isset($Data['isJoinBriefing']) && $Data['isJoinBriefing'] == '0'){
   		        $Data['BreafingReserveSeats'] = '0';
   		    }
   		    
   		    $fieldArr = array(
   		        'BreafingReserveSeats',
   		    );
   		    foreach($fieldArr as $field){
   		        if(isset($Data[$field])){
   		            $this->updateApplicationCustInfo(array(
   		                'filterApplicationId' => $ApplicationID,
   		                'filterCode' => $field,
   		                'Value' => $Data[$field],
   		            ), $insertIfNotExists = true);
   		        }
   		    }

//   		}
//   		else{
   			//Henry: not yet finish
//   			$ApplyDayType1 = '';
//   			$ApplyDayType2 = '';
//   			$ApplyDayType3 = '';
//   			for($i=1;$i<=3;$i++){
//   				if(${'OthersApplyDayType'.$i} == 1){
//   					$ApplyDayType1 = $i;
//   				}
//   				else if(${'OthersApplyDayType'.$i} == 2){
//   					$ApplyDayType2 = $i;
//   				}
//   				else if(${'OthersApplyDayType'.$i} == 3){
//   					$ApplyDayType3 = $i;
//   				}
//   			}
			$sql = "UPDATE 
					ADMISSION_OTHERS_INFO 
				SET
					 ApplyYear = '".$this->schoolYearID."',
					 ApplyDayType1 = '".$ApplyClass."',
					 ApplyDayType1 = '".$OthersApplyDayType1."',
					 ApplyDayType2 = '".$OthersApplyDayType2."',
					 ApplyDayType3 = '".$OthersApplyDayType3."',
					 ApplyStartDay = '".$FormStudentInfo_Enroll_Date."',
					 ApplyLevel = '".$sus_status."',
					 ApplyProgram1 = '".$StudentProgram1."',
					 ApplyProgram2 = '".$StudentProgram2."',
					 IsConsiderAlternative = '".$OthersIsConsiderAlternative."',
					 Token = '".$token."',	
				     DateInput = NOW(),
				 	 HTTP_USER_AGENT = '".addslashes($_SERVER['HTTP_USER_AGENT'])."'  
				WHERE
					ApplicationID = '".$ApplicationID."'";
					
			if($this->db_db_query($sql) && $this->db_affected_rows() == 0){
   			
	   			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (
							 ApplicationID,
						     ApplyYear,	   
						     ApplyDayType1,
						     ApplyDayType2,
						     ApplyDayType3,
						     ApplyProgram1,
	   						 ApplyProgram2,
							 Token,
						     DateInput,
							 HTTP_USER_AGENT
						     )
						VALUES (
							'".$ApplicationID."',
							'".$this->schoolYearID."',
							'".$OthersApplyDayType1."',
							'".$OthersApplyDayType2."',
							'".$OthersApplyDayType3."',
							'".$StudentProgram1."',
							'".$StudentProgram2."',
							'".$OthersIsConsiderAlternative."',
							'".$token."',
							now(),
							'".addslashes($_SERVER['HTTP_USER_AGENT'])."')
				";
				return $this->db_db_query($sql);
				return false;
			}
			else 
				return true;
   		}
   		return false;
    }
    
    function insertApplicationStudentInfoCust($Data, $ApplicationID){
   		extract($Data);
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   			$sql = "INSERT INTO ADMISSION_STU_PREV_SCHOOL_INFO (
								ApplicationID,
				  				Class,
								ClassType,    
				   				NameOfSchool,
								SchoolAddress,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".$CurrentSchoolClassLv."',
								'".$CurrentSchoolDayType."',
								'".$CurrentSchoolName."',
								'".$CurrentSchoolAddr."',
								now())
					";
			$success[] = $this->db_db_query($sql);
   			
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function getApplicationStudentInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				s.RecordID,
     			s.ApplicationID applicationID,
     			s.Year OthersPrevSchYear,		    					
     			s.Class OthersPrevSchClass,
				s.NameOfSchool OthersPrevSchName,
				s.SchoolAddress ,
				s.ClassType
			FROM
				ADMISSION_STU_PREV_SCHOOL_INFO s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			AND (s.Year != '' OR s.Class !='' OR s.NameOfSchool !='')
    	";

    	return $this->returnArray($sql);
	}	
    
    function insertApplicationRelativesInfoCust($Data, $ApplicationID){
   		extract($Data);
   		
   		#Check exist application
   		if($ApplicationID != ""){
   			$success = array();
   				if($OthersExRelativeStudiedName != ''){
		   			$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				  				Name,
		   			            Year,
		   						Type,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".$OthersExRelativeStudiedName."',
								'".$OthersExRelativeStudiedYear."',
								'EX',
								now())
					";
					$success[] = $this->db_db_query($sql);
   				}
   				if($OthersRefRelativeStudiedName != ''){
   					$sql = "INSERT INTO ADMISSION_RELATIVES_AT_SCH_INFO (
								ApplicationID,
				  				Name,
   					            ClassPosition,
   								Type,
								DateInput)
							VALUES (
								'".$ApplicationID."',
								'".$OthersRefRelativeStudiedName."',
								'".$OthersRefRelativeStudiedClass."',
								'CUR',
								now())
					";
   					$success[] = $this->db_db_query($sql);
   				}
   				
   			return !in_array(false, $success);			
   		}
   		return false;
    }
    
    function getApplicationRelativesInfoCust($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$type=''){
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				r.RecordID,
     			r.ApplicationID applicationID,
				r.Year,
				r.Name,
				r.Gender,
				r.ClassPosition,
				r.Age 
			FROM
				ADMISSION_RELATIVES_AT_SCH_INFO r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'
			".$cond." 
			AND (r.Year != '' OR r.Name !='' OR r.ClassPosition !='' OR r.Relationship)
			AND r.Type ='".$type."'";

    	return $this->returnArray($sql);
	}	
    
	function updateApplicationOtherInfo($data){    	
		global $UserID,$admission_cfg,$sys_custom;
   		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;


		$sql = "UPDATE 
					ADMISSION_OTHERS_INFO 
				SET ";
				
				$ApplyDayTypeAry = array();
				for($i=0;$i<3;$i++){
					if(!empty($ApplyDayType[$i])){
						$ApplyDayTypeAry[] = $ApplyDayType[$i];
					}
				} 
				for($i=0;$i<3;$i++){
					$sql .= "ApplyDayType".($i+1)." = '".$ApplyDayTypeAry[$i]."',";
				}     
		    $sql .= "ApplyProgram1 = '".$StudentProgram1."',
		    		 ApplyProgram2 = '".$StudentProgram2."',
		    		 ApplyStartDay = '".$ApplyStartDay."',
					 CustQ1 = '".$IsKinderU."',
				     DateModified = NOW(),
				     ModifiedBy = '".$UserID."'
				WHERE
					RecordID = '".$recordID."' AND ApplyYear = '".$schoolYearId."'";
			$success[] = $this->db_db_query($sql);
		
		$success[] = $this->saveApplicationStudentInfoCust($data);
		
		$success[] = $this->saveApplicationRelativesInfoCust($data);
		
		return !in_array(false,$success);	
    }    
    function removeApplicationAttachment($data){
    	global $file_path;
    	extract($data);
    	if(empty($recordID)) return;
 		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";
    	$sql = "SELECT 
    				r.RecordID attachment_id,
    				r.AttachmentName attachment_name,
    				r.AttachmentType attachment_type
    			FROM 
    				ADMISSION_ATTACHMENT_RECORD r
    			INNER JOIN 
    				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID
    			WHERE 
    				o.RecordID = '".$recordID."' 
    				".$cond."
    			";
    	$applicantAry = $this->returnArray($sql);
    
    	$Success = array();
    	for($i=0;$i<count($applicantAry);$i++){
    		$_attachmentName = $applicantAry[$i]['attachment_name'];
    		$_attachmentType = $applicantAry[$i]['attachment_type']=='personal_photo'? $applicantAry[$i]['attachment_type']:'other_files';
    		$_attachmentId = $applicantAry[$i]['attachment_id'];
    		$image_url = $this->filepath.$recordID.'/'.$_attachmentType.'/'.$_attachmentName;	
    		
    		if(file_exists($file_path.$image_url)){
    			unlink($file_path.$image_url);
    			$sql = "DELETE FROM ADMISSION_ATTACHMENT_RECORD WHERE RecordID = '".$_attachmentId."'";
    			
    			$Success[] = $this->db_db_query($sql);
    		}
    	}
    	if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
    }
    function saveApplicationAttachment($data){
    	global $UserID;
    	extract($data);
 		if(empty($recordID)) return;
    	$sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID = '".$recordID."'";
    	$applicationID = current($this->returnVector($sql));
    	
    	if(!empty($applicationID)&&!empty($attachment_name)&&!empty($attachment_type)){
    		$result = $this->removeApplicationAttachment($data);
    		if($result){
	    		$sql = "INSERT INTO 
	    					ADMISSION_ATTACHMENT_RECORD 
	    						(ApplicationID,AttachmentType,AttachmentName,DateInput,InputBy)
	    					VALUES
	    						('".$applicationID."','".$attachment_type."','".$attachment_name."',NOW(),'".$UserID."')
	    				";
	    		return $this->db_db_query($sql);
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
 	function updateApplicationStudentInfo($data, $isAdminUpdate = false){
 		global $kis_lang, $Lang,$admission_cfg;
 		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		
//  		if($isAdminUpdate){
// 			$sql = "
//				UPDATE 
//					ADMISSION_STU_INFO stu 
//				INNER JOIN 
//					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
//				SET
//					stu.LastSchool = '".$lastschool."'
//				WHERE 
//					o.RecordID = '".$recordID."'	
//	    	";
//  		}
//  		else{

   		    #### Email Role START ####
   		    $this->updateApplicationCustInfo(array(
   		        'filterApplicationId' => $ApplicationID,
   		        'filterCode' => 'PrimaryEmailRole',
   		        'Value' => $PrimaryEmailRole
   		    ));
   		    $this->updateApplicationCustInfo(array(
   		        'filterApplicationId' => $ApplicationID,
   		        'filterCode' => 'SecondaryEmailRole',
   		        'Value' => $SecondaryEmailRole
   		    ));
   		    #### Email Role END ####

   		    if($SpokenLanguage == $admission_cfg['lang']['others']){
   		        $SpokenLanguage = $SpokenLanguageOther;
   		    }
	    	
   		    if(!$twins){
   		        $twinsapplicationid = '';
   		    }
	    	
			$sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID AND o.ApplyYear = '".$schoolYearId."'
				SET
	     			stu.ChineseName = '".$student_name_b5."',
	      			stu.EnglishName = '".$student_name_en."',
	      			stu.Gender = '".$gender."',
	      			stu.DOB = '".$dateofbirth."',
	      			stu.BirthCertNo = '".$birthcertno."' ,
	      			stu.PlaceOfBirth = '".$placeofbirth."',
	      			stu.Nationality = '".$nationality."',
	      			stu.HomeTelNo = '".$homephoneno."',
	      			stu.Fax = '".$fax."',
            		stu.AddressRoom = '".$HomeAddrRoom."',
            		stu.AddressFloor = '".$HomeAddrFloor."',
            		stu.AddressBlock = '".$HomeAddrBlock."',
            		stu.AddressBldg = '".$HomeAddrBlding."',
            		stu.AddressStreet = '".$HomeAddrStreet."',
            		stu.AddressDistrict = '".$HomeAddrDistrict."',
            		stu.AddressEstate = '".$HomeAddrRegion."',
					stu.Email = '".$email."',
				    stu.Email2 = '".$email2."',
			        stu.LangSpokenAtHome = '".$SpokenLanguage."',	
			        stu.IsTwinsApplied = '".$twins."',	
			        stu.TwinsApplicationID = '".$twinsapplicationid."',	
					stu.LastSchool = '".$lastschool."',
					stu.LastSchoolLevel = '".$lastschoollevel."'
				WHERE 
					o.RecordID = '".$recordID."'	
	    	";
//  		}

    	return $this->db_db_query($sql);
	}    
 	
	function getApplicationAttachmentRecord($schoolYearID,$data=array()){
		extract($data);
		$cond = !empty($applicationID)?" AND r.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($attachment_type)?" AND r.AttachmentType='".$attachment_type."'":"";		
		$sql = "
			SELECT
				 o.RecordID folder_id,
     			 r.RecordID,
			     r.ApplicationID applicationID,
			     r.AttachmentType attachment_type,
			     r.AttachmentName attachment_name,
			     r.DateInput dateinput,
			     r.InputBy inputby
			FROM
				ADMISSION_ATTACHMENT_RECORD r
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON r.ApplicationID = o.ApplicationID						
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
			ORDER BY r.AttachmentType, r.RecordID desc
    	";
    	$result = $this->returnArray($sql);
    	$attachmentAry = array();
    	for($i=0;$i<count($result);$i++){
    		$_attachType = $result[$i]['attachment_type'];
     		$_attachName = $result[$i]['attachment_name'];   
     		$_applicationId = $result[$i]['applicationID'];  
     		$_folderId = $result[$i]['folder_id'];
     		$attachmentAry[$_applicationId][$_attachType]['attachment_name'][] = $_attachName;		
     		$attachmentAry[$_applicationId][$_attachType]['attachment_link'][] = $_folderId.'/'.($_attachType=='personal_photo'?$_attachType:'other_files').'/'.$_attachName;	
     		
    	}
		return $attachmentAry;
	}
	function saveApplicationParentInfo($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];

 		foreach($this->pg_type as $_key => $_pgType){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			$parentInfoAry['PG_TYPE'] = $_pgType;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			$parentInfoAry['ChineseName'] = $parent_name_b5[$_key];
			//$parentInfoAry['Nationality'] = $nationality[$_key];
			$parentInfoAry['JobTitle'] = $occupation[$_key];
			$parentInfoAry['Company'] = $companyname[$_key];
			//$parentInfoAry['JobPosition'] = $jobposition[$_key];
//			$parentInfoAry['OfficeAddress'] = $companyaddress[$_key];
// 			$parentInfoAry['OfficeTelNo'] = $office[$_key];
			$parentInfoAry['Mobile'] = $mobile[$_key];	
//			$parentInfoAry['Email'] = $email[$_key];
//			$parentInfoAry['LevelOfEducation'] = $levelofeducation[$_key];
//			$parentInfoAry['LastSchool'] = $lastschool[$_key];
			//$parentInfoAry['HKID'] = $hkid[$_key];

			
			if($parent_id[$_key]=='new'){
				$Success[] = $this->insertApplicationParentInfo($parentInfoAry);
			}else{
				$parentInfoAry['RecordID'] = $parent_id[$_key];	
				$Success[] = $this->updateApplicationParentInfo($parentInfoAry);
			}
		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationStudentInfoCust($data){
		extract($data);
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
// 		for($i=0; $i<1; $i++){
 			$parentInfoAry = array();
			$parentInfoAry['ApplicationID'] = $ApplicationID;
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
//			if($student_cust_id[$i]=='new'){
//				$parentInfoAry['OthersPrevSchClass'.($i+1)] = $OthersPrevSchClass[$i];
//				$parentInfoAry['OthersPrevSchName'.($i+1)] = $OthersPrevSchName[$i];
//				$parentInfoAry['SchoolAddress'] = $SchoolAddress[$i];
//				$parentInfoAry['ClassType'] =  $ClassType;
//				$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry);
//			}else{
				if($student_cust_id[0] == 'new'){
					$parentInfoAry['CurrentSchoolClassLv'] = $OthersPrevSchClass[0];
					$parentInfoAry['CurrentSchoolName'] = $OthersPrevSchName[0];
					$parentInfoAry['CurrentSchoolAddr'] = $SchoolAddress[0];
					$parentInfoAry['CurrentSchoolDayType'] =  $ClassType;
					$Success[] = $this->insertApplicationStudentInfoCust($parentInfoAry,$parentInfoAry['ApplicationID']);
				}
				else{
					$parentInfoAry['Class'] = $OthersPrevSchClass[0];
					$parentInfoAry['NameOfSchool'] = $OthersPrevSchName[0];
					$parentInfoAry['SchoolAddress'] = $SchoolAddress[0];
					$parentInfoAry['ClassType'] =  $ClassType;
					$parentInfoAry['RecordID'] = $student_cust_id[0];	
					$Success[] = $this->updateApplicationStudentInfoCust($parentInfoAry);
				}
				
//			}
//		}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function saveApplicationRelativesInfoCust($data){
		extract($data);
		
 		if(empty($recordID)||empty($schoolYearId)) return;
 		$applicationStatus = current($this->getApplicationStatus($schoolYearId,$classLevelID='',$applicationID='',$recordID));
 		$ApplicationID = $applicationStatus['applicationID'];
 		
			//$parentInfoAry['Relationship'] = ($_pgType=='G')?$relationship:'';
			//$parentInfoAry['EnglishName'] = $parent_name_en[$_key];
			
			if($relatives_info_ex_id=='new'&& $OthersExName!=''){
     			$parentInfoAry = array();
    			$parentInfoAry['ApplicationID'] = $ApplicationID;
				$parentInfoAry['OthersExRelativeStudiedName'] = $OthersExName;
				$parentInfoAry['OthersExRelativeStudiedYear'] = $OthersExYear;
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry,$parentInfoAry['ApplicationID']);
			}else{
				$parentInfoAry['Name'] = $OthersExName;
				$parentInfoAry['Year'] = $OthersExYear;
				$parentInfoAry['ClassPosition'] = '';
				$parentInfoAry['Type'] = 'EX';
				$parentInfoAry['RecordID'] = $relatives_info_ex_id;
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
			
			if($relatives_info_cur_id=='new'&& $OthersCurName!=''){
     			$parentInfoAry = array();
    			$parentInfoAry['ApplicationID'] = $ApplicationID;
				$parentInfoAry['OthersRefRelativeStudiedName'] = $OthersCurName;
				$parentInfoAry['OthersRefRelativeStudiedClass'] = $OthersCurClass;
				$Success[] = $this->insertApplicationRelativesInfoCust($parentInfoAry,$parentInfoAry['ApplicationID']);
			}else{
				$parentInfoAry['Name'] = $OthersCurName;
				$parentInfoAry['Year'] = '';
				$parentInfoAry['ClassPosition'] = $OthersCurClass;
				$parentInfoAry['Type'] = 'CUR';
				$parentInfoAry['RecordID'] = $relatives_info_cur_id;
				$Success[] = $this->updateApplicationRelativesInfoCust($parentInfoAry);
			}
		if(in_array(false,$Success)){
			return false;
		}
		else{
			return true;
		}
	}
	   
	function getApplicationOthersInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND RecordID='".$recordID."'":"";

		$sql = "
			SELECT 
				RecordID,
				ApplicationID applicationID,
				ApplyYear schoolYearId,
				ApplyDayType1,
				ApplyDayType2,
				ApplyDayType3,
				ApplyProgram1,
				ApplyProgram2,
				ApplyStartDay,
				ApplyLevel classLevelID,
				IsConsiderAlternative,
				DateInput,
				DateModified,
				ModifiedBy,
				InterviewSettingID,
				InterviewSettingID2,
				InterviewSettingID3,
				SiblingAppliedName,
			 	NativeLanguage,
				CustQ1,
				Remarks,
				IsConsiderAlternative OthersIsConsiderAlternative
			FROM
				ADMISSION_OTHERS_INFO
			WHERE
				ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	return $this->returnArray($sql);
	}		 
	function getApplicationParentInfo($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		$cond = !empty($applicationID)?" AND pg.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT 
				pg.RecordID,
     			pg.ApplicationID applicationID,
     			pg.ChineseName ChineseName,
     			pg.PG_TYPE type,
				pg.Company Company,
     			pg.JobTitle JobTitle,
     			pg.Mobile Mobile,
     			o.ApplyLevel classLevelID
			FROM
				ADMISSION_PG_INFO pg
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON pg.ApplicationID = o.ApplicationID
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";

    	return $this->returnArray($sql);
	}	
	function getApplicationStatus($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg;
		$cond = !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$sql = "
			SELECT
     			s.ApplicationID applicationID,
     			s.ReceiptID receiptID, 
			    IF(s.ReceiptDate,DATE_FORMAT(s.ReceiptDate,'%Y-%m-%d'),'') As receiptdate,
			    IF(s.Handler,".getNameFieldByLang2("iu.").",'') AS handler,
			    s.Handler handler_id,
				s.InterviewDate As interviewdate,
				s.InterviewLocation As interviewlocation,
			    s.BriefingDate As briefingdate,
				s.BriefingInfo As briefinginfo,	
			    s.Remark remark,CASE ";
		foreach($admission_cfg['Status'] as $_key => $_value){
				$sql .= "WHEN s.Status = ".$_value." THEN '".strtolower($_key)."' ";
		}	    
		$sql .= " ELSE s.Status END status,";
		$sql .= "
			    CASE WHEN s.isNotified = 1 THEN 'yes' ELSE 'no' END isnotified,
			    s.DateInput dateinput,
			    s.InputBy inputby,
			    s.DateModified datemodified,
			    s.ModifiedBy modifiedby,
			    o.ApplyLevel classLevelID
			FROM
				ADMISSION_APPLICATION_STATUS s
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON s.ApplicationID = o.ApplicationID
			LEFT JOIN
				INTRANET_USER iu ON s.Handler = iu.UserID
			WHERE
				s.SchoolYearID = '".$schoolYearID."'	
			".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	
	function uploadAttachment($type, $file, $destination, $randomFileName=""){//The $randomFileName does not contain file extension
		global $admission_cfg, $intranet_root, $libkis;
		include_once($intranet_root."/includes/libimage.php");
		$uploadSuccess = false;
		$ext = strtolower(getFileExtention($file['name']));

		//if($type == "personal_photo"){
			if (!empty($file['tmp_name'])) {
				require_once($intranet_root."/includes/admission/class.upload.php");
				$handle = new Upload($file['tmp_name']);
				if ($handle->uploaded) {
					$handle->Process($destination);		
					if ($handle->processed) {
						$uploadSuccess = true;
						if($type == "personal_photo"){
							$image_obj = new SimpleImage();
							$image_obj->load($handle->file_dst_pathname);
							if($admission_cfg['personal_photo_width'] && $admission_cfg['personal_photo_height'])
								$image_obj->resizeToMax($admission_cfg['personal_photo_width'], $admission_cfg['personal_photo_height']);
							else
								$image_obj->resizeToMax(kis::$personal_photo_width, kis::$personal_photo_height);
							//rename the file and then save
							
							$image_obj->save($destination."/".($randomFileName?$randomFileName:$type).".".$ext, $image_obj->image_type);
							unlink($handle->file_dst_pathname);
						}
						else{
							rename($handle->file_dst_pathname, $destination."/".($randomFileName?$randomFileName:$type).".".$ext);
						}
						//$cover_image = str_replace($intranet_root, "", $handle->file_dst_pathname);
					} else {
						// one error occured
						$uploadSuccess = false;
					}		
					// we delete the temporary files
					$handle-> Clean();
				}		
			}else{
				if($this->isInternalUse($_REQUEST['token'])){
					return true;
				}
			}
			return $uploadSuccess;	
		//}
		//return true;
	}
	
	function moveUploadedAttachment($tempFolderPath, $destFolderPath){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/personal_photo", $destFolderPath);
		$uploadSuccess[] = $lfs->lfs_copy($tempFolderPath."/other_files", $destFolderPath);
		$uploadSuccess[] = $lfs->folder_remove_recursive($tempFolderPath);
		
		if(in_array(false, $uploadSuccess))
			return false;
		else
			return true;
	}
	//Siuwan 20131018 Copy from libstudentregistry.php, e.g.$this->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	public function displayPresetCodeSelection($code_type="", $selection_name="", $code_selected="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and RecordStatus=1 order by DisplayOrder";
		$result = $this->returnArray($sql);
		return getSelectByArray($result, "name=".$selection_name, $code_selected,0,1);
	}
	//Henry 20131018 Copy from libstudentregistry.php
	public function returnPresetCodeName($code_type="", $selected_code="")
	{
		$sql = "select ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."' and Code='". $selected_code ."'";
		$result = $this->returnVector($sql);
		return $result[0];
	}
	function displayWarningMsg($warning){
		global $kis_lang;
		$x = '
		 <fieldset class="warning_box">
			<legend>'.$kis_lang['warning'].'</legend>
			<ul>
				<li>'.$kis_lang['msg'][$warning].'</li>
			</ul>
		</fieldset>
		
		';
		return $x;
	}
		
	function insertAttachmentRecord($AttachmentType,$AttachmentName, $ApplicationID){
   		if($ApplicationID != ""){
   			$sql = "INSERT INTO ADMISSION_ATTACHMENT_RECORD (
		   				 ApplicationID,
		  				 AttachmentType,   
		   				 AttachmentName,
		  				 DateInput)
					VALUES (
						'".$ApplicationID."',
						'".$AttachmentType."',
						'".$AttachmentName."',
						now())
			";
			return $this->db_db_query($sql);
   		}
   		return false;
	}
	//old method
	function newApplicationNumber($schoolYearID=""){
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
		if($yearStart == $yearEnd){
			$year = $yearStart;
		}
		else{
			$year = $yearStart.$yearEnd;
		}
		$defaultNo = "PL".$year."-a0001";
		$prefix = substr($defaultNo, 0, -4);
		$num = substr($defaultNo, -4);

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%' order by ApplicationID desc";
		$result = $this->returnArray($sql);
		
		if($result){			
			$num = substr($result[0]['ApplicationID'], -4);
			$num++;
			$num = sprintf("%04s", $num);
		}
		
		$newNo = $prefix.$num;
		
		return $newNo;
	}
	
	function newApplicationNumber2($schoolYearID="",$classLevelID=""){
		global $Lang, $admission_cfg;
		$yearStart = substr(date('Y',getStartOfAcademicYear('',$this->schoolYearID)), -2);
//		$yearEnd = substr(date('Y',getEndOfAcademicYear('',$this->schoolYearID)), -2);
//		if($yearStart == $yearEnd){
//			$year = $yearStart;
//		}
//		else{
//			$year = $yearStart;
//		}
		//eg. csm1600001
//		$defaultNo = $yearStart."00001";
		if($this->isInternalUse($_REQUEST['token'])){
// 		    $prefix = 'NP'.$yearStart.'H1';
		    $prefix = 'NP'.$yearStart.'H5';
		}else{
		    $prefix = 'NP'.$yearStart.'E1';
		}
		$defaultNo = $prefix.'0001';
		//$prefix = substr($defaultNo, 0, -5);
		$num = substr($defaultNo, -5);
		
		$isTwins = $_POST["twins"] == 1? "T": "";

		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where ApplicationID like '".$prefix."%'";
		
		if($this->returnArray($sql)){
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					SELECT concat('".$prefix."',LPAD(MAX( CONVERT( SUBSTRING( ApplicationID, '".(strlen($prefix)+1)."' ) , UNSIGNED ) ) +1, 4, '0'), '".$isTwins."') 
					FROM ADMISSION_OTHERS_INFO 
					WHERE ApplicationID like '".$prefix."%'";
		}
		else{
			$sql = "INSERT INTO ADMISSION_OTHERS_INFO (ApplicationID) 
					Values ('".$defaultNo.$isTwins."')";
		}

		$result = $this->db_db_query($sql);
				
		$sql = "select ApplicationID from ADMISSION_OTHERS_INFO where RecordID = ".mysql_insert_id();
		$newNo = $this->returnArray($sql);

		return $newNo[0]['ApplicationID'];
	}
	
	function getAttachmentByApplicationID($schoolYearID,$applicationID){
		global $file_path;
		$attachmentAry = $this->getApplicationAttachmentRecord($schoolYearID,array("applicationID"=>$applicationID));
		
		$attachment = array();
		foreach((array)$attachmentAry[$applicationID] as $_type => $_attachmentAry){
			$_thisAttachment = $attachmentAry[$applicationID][$_type]['attachment_link'][0];
			if(!empty($_thisAttachment)){
				$_thisAttachment = $this->filepath.$_thisAttachment; 
				if(file_exists($file_path.$_thisAttachment)){
					$attachment[$_type]['link'] = $_thisAttachment;
				}else{
					$attachment[$_type]['link'] = false;
				}
			}else{
				$attachment[$_type]['link'] = false;
			}
			
		}
		return $attachment;
	}
	
	function getAttachmentSettings($filterIdList=array(),$filterName=array(),$excludeRecordID=array())
	{
		if(sizeof($filterName)>0){
			
			for($i=0;$i<sizeof($filterName);$i++){
				$filterName[$i] = $this->Get_Safe_Sql_Query($filterName[$i]);
			}
			$filterNameCond = " AND AttachmentName IN ('".implode("','",(array)$filterName)."') ";
		}
		$filterIdCond = sizeof($filterIdList)>0 || $filterIdList? " AND RecordID IN (".implode(",",(array)$filterIdList).") ": "";
		if(sizeof($excludeRecordID)>0){
			$filterIdCond .= " AND RecordID NOT IN (".implode(",",$excludeRecordID).") ";
		}
		$sql = "SELECT * FROM ADMISSION_ATTACHMENT_SETTING WHERE 1 $filterIdCond $filterNameCond ORDER BY Sequence";
		
		$records = $this->returnArray($sql);
		return $records;
	}
	
	public function getApplicationNumber($RecordIDAry){
		$sql = 'SELECT ApplicationID FROM ADMISSION_OTHERS_INFO WHERE RecordID IN ("'.implode('","',$RecordIDAry).'")';
		$result = $this->returnVector($sql);
		return $result;
	}
	
	public function getPrintLink($schoolYearID="", $applicationID , $type=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?id='.$id;
	}
	
	public function getPrintLink2($schoolYearID="", $applicationID , $type=""){
		global $admission_cfg;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$id = urlencode(getEncryptedText('ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type,$admission_cfg['FilePathKey']));
		//return '/kis/admission_form/print_form.php?ApplicationID='.$applicationID.'&SchoolYearID='.$schoolYearID.'&Type='.$type;
		return '/kis/admission_form/print_form.php?id='.$id;
	}
	
 	function getClassLevel($ClassLevel=''){
 		global $intranet_root;
    	include_once($intranet_root."/includes/form_class_manage.php");
    	$libYear = new Year();
		$FormArr = $libYear->Get_All_Year_List();
    	$numOfForm = count($FormArr);
    	$classLevelName = array();
		for ($i=0; $i<$numOfForm; $i++)
		{
			$thisClassLevelID = $FormArr[$i]['YearID'];
			$thisLevelName = $FormArr[$i]['YearName'];
			$classLevelName[$thisClassLevelID] = $thisLevelName;
		}
		return $classLevelName;
    }	
	function getBasicSettings($schoolYearID='',$SettingNameAry=array()){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
    	$sql = "
			SELECT
			    SettingName,
			    SettingValue
			FROM
				ADMISSION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'	
    	";
    	if (sizeof($SettingNameAry) > 0)  {
			$sql .= " AND 
						SettingName in ('".implode("','",$SettingNameAry)."')";
		}
    	$setting = $this->returnArray($sql);
		for ($i=0; $i< sizeof($setting); $i++) {
			$Return[$setting[$i]['SettingName']] = $setting[$i]['SettingValue'];
		}
		return $Return;    	
    }
   	function saveBasicSettings($schoolYearID='',$SettingNameValueAry=array()){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		$this->Start_Trans();
		
		$SettingsNameAry = array_keys($SettingNameValueAry);
		
		$result['remove_basic_settings'] = $this->removeBasicSettings($schoolYearID,$SettingsNameAry);
		$result['insert_basic_settings'] = $this->insertBasicSettings($schoolYearID,$SettingNameValueAry);
		
		if(in_array(false, $result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else
		{
			$this->Commit_Trans();
			return true;
		}
		
	}
	 
	function removeBasicSettings($schoolYearID,$SettingsNameAry){
		if(count($SettingsNameAry)==0) return false;
		
		$SettingsNameSql = "'".implode("','",(array)$SettingsNameAry)."'";
		
		$sql = "
			DELETE FROM
				ADMISSION_SETTING	 
			WHERE
				SettingName IN (".$SettingsNameSql.")
			AND SchoolYearID = '".$schoolYearID."'
		";
		
		return $this->db_db_query($sql);
	}
	
	function insertBasicSettings($schoolYearID,$SettingNameValueAry){
		if(count($SettingNameValueAry)==0 || !is_array($SettingNameValueAry)) return false;
		
		foreach((array)$SettingNameValueAry as $_settingName => $_settingValue)
		{
			$InsertSqlArr[] = "('".$schoolYearID."','".$_settingName."','".$_settingValue."', '".$this->uid."', NOW(), '".$this->uid."', NOW())";
		}
		
		if(count($InsertSqlArr)>0)
		{	
			$InsertSql = implode(',',$InsertSqlArr);	
			
			$sql = "
				INSERT INTO	ADMISSION_SETTING
					(SchoolYearID, SettingName, SettingValue, InputBy, DateInput, ModifiedBy, DateModified)	 
				VALUES
					$InsertSql
			";
			
			return $this->db_db_query($sql);
			
		}
		else
			return false;
		
			
	}
	
	function getApplicationSetting($schoolYearID=''){
    	$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;

    	$sql = "
			SELECT
     			ClassLevelID,
			    IF(StartDate,DATE_FORMAT(StartDate,'%Y-%m-%d %H:%i'),'') As StartDate,
				IF(EndDate,DATE_FORMAT(EndDate,'%Y-%m-%d %H:%i'),'') As EndDate,
				IF(DOBStart,DATE_FORMAT(DOBStart,'%Y-%m-%d'),'') As DOBStart,
				IF(DOBEnd,DATE_FORMAT(DOBEnd,'%Y-%m-%d'),'') As DOBEnd,
				IF(PreviewStartDate,DATE_FORMAT(PreviewStartDate,'%Y-%m-%d %H:%i'),'') As PreviewStartDate,
				IF(PreviewEndDate,DATE_FORMAT(PreviewEndDate,'%Y-%m-%d %H:%i'),'') As PreviewEndDate,
				IF(UpdateStartDate,DATE_FORMAT(UpdateStartDate,'%Y-%m-%d %H:%i'),'') As UpdateStartDate,
				IF(UpdateEndDate,DATE_FORMAT(UpdateEndDate,'%Y-%m-%d %H:%i'),'') As UpdateEndDate,
				Quota,
			    DayType,
			    FirstPageContent,
			    LastPageContent,
				EmailContent,
			    DateInput,
			    InputBy,
			    DateModified,
			    ModifiedBy,
				SchoolYearID,
				AllowInternalUse
			FROM
				ADMISSION_APPLICATION_SETTING
			WHERE
				SchoolYearID = '".$schoolYearID."'
    	";
    	$setting = $this->returnArray($sql);
    	$applicationSettingAry = BuildMultiKeyAssoc($setting, 'ClassLevelID');
    	$applicationPeriodAry = array();
    	foreach($this->classLevelAry as $_classLevelId => $_classLevelName){ 	
    		$_startdate = $applicationSettingAry[$_classLevelId]['StartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['StartDate']:'';	
    		$_enddate = $applicationSettingAry[$_classLevelId]['EndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['EndDate']:'';	
    		
    		$_previewstartdate = $applicationSettingAry[$_classLevelId]['PreviewStartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['PreviewStartDate']:'';	
    		$_previewenddate = $applicationSettingAry[$_classLevelId]['PreviewEndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['PreviewEndDate']:'';	
    		
    		$_updatestartdate = $applicationSettingAry[$_classLevelId]['UpdateStartDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['UpdateStartDate']:'';	
    		$_updateenddate = $applicationSettingAry[$_classLevelId]['UpdateEndDate']!='0000-00-00 00:00'?$applicationSettingAry[$_classLevelId]['UpdateEndDate']:'';
    		
    		$applicationPeriodAry[$_classLevelId] = array(
    													'ClassLevelName'=>$_classLevelName,
      													'StartDate'=>$_startdate,  	
      													'EndDate'=>$_enddate,
      													'PreviewStartDate'=>$_previewstartdate,  	
      													'PreviewEndDate'=>$_previewenddate,
      													'UpdateStartDate'=>$_updatestartdate,  	
      													'UpdateEndDate'=>$_updateenddate,
      													'DOBStart'=>$applicationSettingAry[$_classLevelId]['DOBStart'],
      													'DOBEnd'=>$applicationSettingAry[$_classLevelId]['DOBEnd'],
      													'DayType'=>$applicationSettingAry[$_classLevelId]['DayType'],   
      													'FirstPageContent'=>$applicationSettingAry[$_classLevelId]['FirstPageContent'],  
      													'LastPageContent'=>$applicationSettingAry[$_classLevelId]['LastPageContent'],
      													'EmailContent'=>$applicationSettingAry[$_classLevelId]['EmailContent'],
      													'Quota'=>$applicationSettingAry[$_classLevelId]['Quota'],
      													'SchoolYearID'=>$applicationSettingAry[$_classLevelId]['SchoolYearID'],
      													'AllowInternalUse'=>$applicationSettingAry[$_classLevelId]['AllowInternalUse']					  																									
    												);
    	}
    	return $applicationPeriodAry;    	
    }
    
    public function updateApplicationSetting($data){
   		extract($data);
   		#Check exist setting
   		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
   		$cnt = current($this->returnVector($sql));

   		if($cnt){//update
   			$sql = "UPDATE ADMISSION_APPLICATION_SETTING SET 
		   				 StartDate = '".$startDate."',
		  				 EndDate = '".$endDate."',
 						 PreviewStartDate = '".$previewStartDate."',
		  				 PreviewEndDate = '".$previewEndDate."',
						 UpdateStartDate = '".$updateStartDate."',
		  				 UpdateEndDate = '".$updateEndDate."',
						 Quota = '".$quota."',
						 DOBStart = '".$dOBStart."',
		  				 DOBEnd = '".$dOBEnd."',  
		   				 DayType = '".$dayType."',
		  				 FirstPageContent = '".$firstPageContent."',   
		   				 LastPageContent = '".$lastPageContent."',
						 EmailContent = '".$emailContent."',
		   				 DateModified = NOW(),
		   				 ModifiedBy = '".$this->uid."',
						 AllowInternalUse = '".($allowInternalUse?$allowInternalUse:0)."' 
   					WHERE SchoolYearID = '".$schoolYearID."'	AND ClasslevelID = '".$classLevelID."'";
   		}else{//insert
   			$sql = "INSERT INTO ADMISSION_APPLICATION_SETTING (
   						SchoolYearID,
   						ClassLevelID,
					    StartDate,
					    EndDate,
						PreviewStartDate,
		  				PreviewEndDate,
						UpdateStartDate,
						UpdateEndDate,
						Quota,
						DOBStart,
		  				DOBEnd,
					    DayType,
		  				FirstPageContent,   
		   				LastPageContent,
						EmailContent,
					    DateInput,
					    InputBy,
					    DateModified,
					    ModifiedBy,
						AllowInternalUse) 
					VALUES (
					    '".$schoolYearID."',
					    '".$classLevelID."',
					    '".$startDate."',
					    '".$endDate."',
						'".$previewStartDate."',
						'".$previewEndDate."',
						'".$updateStartDate."',
						'".$updateEndDate."',
						'".$quota."',
						'".$dOBStart."',
						'".$dOBEnd."',	
					    '".$dayType."',
					    '".$firstPageContent."',
					    '".$lastPageContent."',	
						'".$emailContent."',
					    NOW(),
					    '".$this->uid."',
					     NOW(),
					    '".$this->uid."',
						'".($allowInternalUse?$allowInternalUse:0)."')
			";
   		}
   		return $this->db_db_query($sql);
    }
    
    function getApplicationStudentInfo($schoolYearID,$classLevelID='',$applicationID='',$status='',$recordID=''){
		$cond = !empty($applicationID)?" AND stu.ApplicationID='".$applicationID."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";		
		$cond .= !empty($status)?" AND s.status='".$status."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";

		$sql = "
			SELECT
     			stu.ApplicationID applicationID,
     			o.ApplyLevel classLevelID,
     			o.ApplyYear schoolYearID,
     			stu.ChineseName student_name_b5,
      			stu.EnglishName student_name_en, 
      			stu.Gender gender,
      			IF(stu.DOB,DATE_FORMAT(stu.DOB,'%Y-%m-%d'),'') dateofbirth,	
      			stu.PlaceOfBirth placeofbirth,
      			stu.Nationality,
      			stu.HomeTelNo homephoneno,
				stu.Fax fax,
      			stu.BirthCertNo birthcertno,
        		stu.AddressRoom,
        		stu.AddressFloor,
        		stu.AddressBlock,
        		stu.AddressBldg,
        		stu.AddressStreet,
        		stu.AddressDistrict,
        		stu.AddressEstate,
     			stu.Email,
     			stu.Email2,
     			stu.LangSpokenAtHome,
		        stu.IsTwinsApplied,	
		        stu.TwinsApplicationID,	
				stu.LastSchool,
				stu.LastSchoolLevel,
				IF(stu.DateInput,DATE_FORMAT(stu.DateInput,'%Y-%m-%d'),'') DateInput
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID							
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond."
    	";
    	$studentInfoAry = $this->returnArray($sql);
		return $studentInfoAry;
	}
	function getApplicationDetails($schoolYearID,$data=array()){
		global $admission_cfg;
		extract($data);
		$sort = $sortby? "$sortby $order":"application_id";
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount": "";
		$cond = !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$cond .= !empty($applicationID)?" AND s.ApplicationID='".$applicationID."'":"";	
		$cond .= $status?" AND s.Status='".$status."'":"";
		if($status==$admission_cfg['Status']['waitingforinterview']){
			if($interviewStatus==1){
				$cond .= " AND s.isNotified = 1";
			}else{
				$cond .= " AND (s.isNotified = 0 OR s.isNotified IS NULL)";
			}
		}
		if(!empty($keyword)){
			$cond .= "
				AND ( 
					stu.EnglishName LIKE '%".$keyword."%'
					OR stu.ChineseName LIKE '%".$keyword."%'
					OR stu.ApplicationID LIKE '%".$keyword."%'					
					OR pg.EnglishName LIKE '%".$keyword."%'
					OR pg.ChineseName LIKE '%".$keyword."%'
					OR pg.Mobile LIKE '%".$keyword."%'
					OR stu.BirthCertNo LIKE '%".$keyword."%'
					OR CONCAT(LEFT(s.InterviewDate,length(s.InterviewDate)-3),' ',s.InterviewLocation) LIKE '%".$keyword."%'
					OR stu.HomeTelNo LIKE '%".$keyword."%'
                    OR stu.DOB LIKE '%".$keyword."%'
                    OR stu.Email LIKE '%".$keyword."%'
                    OR stu.Email2 LIKE '%".$keyword."%'
				)
			";
		}

     	$from_table = "		    
			FROM
				ADMISSION_STU_INFO stu
			INNER JOIN 
				ADMISSION_PG_INFO pg ON stu.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				o.ApplyYear = '".$schoolYearID."'	
			".$cond." 
			
    	";
    	$sql = "SELECT DISTINCT o.ApplicationID ".$from_table;
    	$applicationIDAry = $this->returnVector($sql);
//    	$sql2 = "SELECT DISTINCT o.ApplicationID ".$from_table;
//    	$applicationIDCount = $this->returnVector($sql2);
    	$sql = "
			SELECT
				o.RecordID AS record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." AND o.ApplicationID IN ('".implode("','",$applicationIDAry)."') ORDER BY $sort ";
     	
    	$applicationAry = $this->returnArray($sql);
    	
    	return array(count($applicationIDAry),$applicationAry);
	}
	
	function getInterviewListAry($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1){
		global $admission_cfg;
		
		//$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
		
		if($recordID != ''){
			$cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
		}
		if($date != ''){
			$cond .= ' AND i.Date >= \''.$date.'\' ';
		}
		if($startTime != ''){
			$cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
		}
		if($endTime != ''){
			$cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
		}
		if($keyword != ''){
			$search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\' 
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
		}
		$sort = $sortby? "$sortby $order":"application_id";
		
//		if(!empty($keyword)){
//			$cond .= "
//				AND ( 
//					stu.EnglishName LIKE '%".$keyword."%'
//					OR stu.ChineseName LIKE '%".$keyword."%'
//					OR stu.ApplicationID LIKE '%".$keyword."%'					
//					OR pg.EnglishName LIKE '%".$keyword."%'
//					OR pg.ChineseName LIKE '%".$keyword."%'				
//				)
//			";
//		}

     	$from_table = "
			FROM 
				ADMISSION_INTERVIEW_SETTING AS i		    
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')."
			INNER JOIN 
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN 
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID	
			WHERE
				1 	
			".$cond." 
			order by ".$sort."
    	";
    	$sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			".getNameFieldByLang2("stu.")." AS student_name,
     			".getNameFieldByLang2("pg.")." AS parent_name,     					
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE 
     	";
     	FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
     		$sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
     	}
     	$sql .= " ELSE s.Status END application_status	".$from_table." ";
     	//debug_r($sql);
    	$applicationAry = $this->returnArray($sql);
    	return $applicationAry;
	}
	   
	function hasApplicationSetting(){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$this->schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL";
		return current($this->returnVector($sql));	
	}
	
	function hasCurrentApplicationSetting($schoolYearID){
		$sql = "SELECT COUNT(*) FROM ADMISSION_APPLICATION_SETTING WHERE SchoolYearID = '".$schoolYearID."' AND StartDate IS NOT NULL AND EndDate IS NOT NULL AND EndDate >= NOW() AND StartDate <= NOW()";
		return current($this->returnVector($sql));	
	}
	
	function checkImportDataForImportAdmissionHeader($csv_header, $lang = ''){
		//$file_format = array("Application#","StudentEnglishName","StudrntChineseName","BirthCertNo","InterviewDate","InterviewTime");
		$file_format = $this->getExportHeader();
		$file_format = $file_format[1];
		# check csv header
		$format_wrong = false;
		
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($csv_header[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		
		return $format_wrong;
	}
	
	public function returnPresetCodeAndNameArr($code_type="")
	{
		$sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")."  from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function checkImportDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			
			//valid Admission Date
			$aData[0] = getDefaultDateFormat($aData[0]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[0]) ) {
				list($year , $month , $day) = explode('-',$aData[0]);
		       	$resultArr[$i]['validAdmissionDate'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validAdmissionDate'] = false;
			}
			
			//valid Admission number
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[1])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validAdmissionNo'] = false;
			}
			else
				$resultArr[$i]['validAdmissionNo'] = true;
			
			//valid Gender
			if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
				$data[$i][5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
				$data[$i][5] = 'F';
			}
			if ( strtoupper($aData[5]) == 'M' || strtoupper($aData[5]) == 'F') {
				
		       	$resultArr[$i]['validGender'] = true;
			}
			else{
				$resultArr[$i]['validGender'] = false;
			}
			
			//valid Date of birth
			$aData[6] = getDefaultDateFormat($aData[6]);

			if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[6]) ) {
				list($year , $month , $day) = explode('-',$aData[6]);
		       	$resultArr[$i]['validDateOfBirth'] = checkdate($month , $day , $year);
			}
			else{
				$resultArr[$i]['validDateOfBirth'] = false;
			}
			
			//valid age
			if (is_numeric($aData[7])) {
				$resultArr[$i]['validAge'] = true;
			}
			else{
				$resultArr[$i]['validAge'] = false;
			}
			
			//valid birth cert type
			$resultArr[$i]['validBirthCertType'] = false;
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$resultArr[$i]['validBirthCertType'] = true;
					break;
				}
			}
			
			//valid birth cert
//			if (preg_match('/^[a-zA-Z][0-9]{7}$/',$aData[8])) {
//				$resultArr[$i]['validBirthCertNo'] = true;
//			}
//			else{
//				$resultArr[$i]['validBirthCertNo'] = false;
//			}
			
			//valid email address
			if (preg_match('/\S+@\S+\.\S+/',$aData[14])) {
				$resultArr[$i]['validEmail'] = true;
			}
			else{
				$resultArr[$i]['validEmail'] = false;
			}
			//valid Religion
			$religionArr = $this->returnPresetCodeAndNameArr("RELIGION");
			foreach($religionArr as $aReligion){
				if (strtoupper($aReligion[1]) == strtoupper($aData[15])){
					$aData[15] = $aReligion[0];
					$data[$i][15] = $aReligion[0];
					$resultArr[$i]['validReligion'] = true;
					break;
				}
				else{
					$resultArr[$i]['validReligion'] = false;
				}
			}
			
			//valid Apply day type
			$resultArr[$i]['validApplyDayType'] = false;
			if($aData[26] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[26]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[26] = $j;
						$data[$i][26] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			if($aData[27] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[27] = $j;
						$data[$i][27] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			if($aData[28] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[28]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[28] = $j;
						$data[$i][28] = $j;
						$resultArr[$i]['validApplyDayType'] = true;
						break;
					}
					else{
						$resultArr[$i]['validApplyDayType'] = false;
					}
				}
			}
			else{
				$resultArr[$i]['validApplyDayType'] = true;
			}
			
			//valid SiblingApplied
			$resultArr[$i]['validSiblingApplied'] = true;
			if(strtoupper($aData[29]) == strtoupper($kis_lang['Admission']['yes'])){
				if($aData[30] == ''){
					$resultArr[$i]['validSiblingApplied'] = false;
				}
			}
			
			//valid SiblingIsGrad
			$resultArr[$i]['validSiblingIsGrad'] = true;
			$resultArr[$i]['validSiblingIsGradInfo'] = true;
			if(strtoupper($aData[31]) == strtoupper($kis_lang['Admission']['yes'])){
				if(!is_numeric($aData[32])){
					$resultArr[$i]['validSiblingIsGrad'] = false;
				}
				if($aData[32] == 1 || $aData[32] == 2 || $aData[32] == 3){
					if($aData[33] =='' || $aData[34] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
				if($aData[32] == 2 || $aData[32] == 3){
					if($aData[35] =='' || $aData[36] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
				if($aData[32] == 3){
					if($aData[37] =='' || $aData[38] ==''){
						$resultArr[$i]['validSiblingIsGradInfo'] = false;
					}
				}
			}
			
			//--- Henry Added 20140827
//			//valid record status
//			foreach($admission_cfg['Status'] as $_key => $_status){
//				if (strtoupper($kis_lang['Admission']['Status'][$_key]) == strtoupper($aData[41])){
//					
//					$aData[41] = $_status;
//					$data[$i][41] = $_status;
//					$resultArr[$i]['validRecordStatus'] = true;
//					break;
//				}
//				else{
//					$resultArr[$i]['validRecordStatus'] = false;
//				}
//			}
//			
//			//valid Receipt Date
//			$aData[43] = getDefaultDateFormat($aData[43]);
//
//			if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[43]) ) {
//				list($year , $month , $day) = explode('-',$aData[43]);
//		       	$resultArr[$i]['validReceiptDate'] = checkdate($month , $day , $year);
//			}
//			else{
//				if($aData[43] != '')
//					$resultArr[$i]['validReceiptDate'] = false;
//				else
//					$resultArr[$i]['validReceiptDate'] = true;
//			}
//			
//			//valid is notified
//			$resultArr[$i]['validIsNotified'] = false;
//			if(strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['no'])){
//				$resultArr[$i]['validIsNotified'] = true;
//			}
			
			
			//------------------------------------------------------------------------------------------
//			$aData[4] = getDefaultDateFormat($aData[4]);
//			//check date
//			if ($aData[4] =='' && $aData[5] ==''){
//				$validDate = true;
//			}
//			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
//		       list($year , $month , $day) = explode('-',$aData[4]);
//		       $validDate = checkdate($month , $day , $year);
//		    } else {
//		       $validDate =  false;
//		    }
//
//		    //check time
//		    if ($aData[4] =='' && $aData[5] ==''){
//				$validTime = true;
//			}
//			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
//		       $validTime = true;
//		    } else {
//		       $validTime =  false;
//		    }
//			$sql = "
//				SELECT
//					COUNT(*)
//				FROM
//					ADMISSION_STU_INFO s
//				WHERE 
//					trim(s.applicationID) = '".trim($aData[1])."' AND trim(s.birthCertNo) = '".trim($aData[8])."'
//	    	";
//			$result = $this->returnVector($sql);
//			if($result[0] == 0){
//				$resultArr[$i]['validData'] = $aData[1];
//			}
//			else
//				$resultArr[$i]['validData'] = false;
//			$resultArr[$i]['validDate'] = $validDate;
//			$resultArr[$i]['validTime'] = $validTime;
//			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[1];
			$i++;
		}
		$result = $resultArr;
		
		//for printing the error message
		$errCount = 0;
		
		$x .= '<table class="common_table_list"><tbody><tr class="step2">
					<th class="tablebluetop tabletopnolink">'.$kis_lang['Row'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['applicationno'].'</th>
					<th class="tablebluetop tabletopnolink">'.$kis_lang['importRemarks'].'</th>
				</tr>';
		$i = 1;
		foreach($result as $aResult){
			//developing
			$hasError = false;
			$errorMag = '';
			if(!$aResult['validAdmissionDate']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidAdmissionDate'];
			}
			else if(!$aResult['validAdmissionNo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importApplicationNoNotFound'];
			}
			else if(!$aResult['validGender']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidWordOfGender'] ;
			}
			else if(!$aResult['validDateOfBirth']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidDateOfBirth'];
			}
			else if(!$aResult['validAge']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidNumberOfAge'] ;
			}
			else if(!$aResult['validBirthCertType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertType'];
			}
//			else if(!$aResult['validBirthCertNo']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfBirthCertNo'];
//			}
			else if(!$aResult['validEmail']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidFormatOfEmailAddress'];
			}
			else if(!$aResult['validReligion']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidKeywordOfReligion'] ;
			}
			else if(!$aResult['validApplyDayType']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importInvalidKeywordOfApplyDayType'] ;
			}
			else if(!$aResult['validSiblingApplied']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNameOfSiblingApplied'];
			}
			else if(!$aResult['validSiblingIsGrad']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNoOfGraduatedSibling'];
			}
			else if(!$aResult['validSiblingIsGradInfo']){
				$hasError = true;
				$errorMag = $kis_lang['Admission']['mgf']['msg']['importEnterNameOfGraduatedSibling'];
			}
//			else if(!$aResult['validRecordStatus']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importEnterRecordStatus'];
//			}
//			else if(!$aResult['validReceiptDate']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importInvalidReceiptDate'];
//			}
//			else if(!$aResult['validIsNotified']){
//				$hasError = true;
//				$errorMag = $kis_lang['Admission']['msg']['importEnterIsNotified'];
//			}
			
			//print the error msg to the client
			if($hasError){
				$errCount++;
				$x .= '<tr class="step2">
					<td>'.$i.'</td>
					<td>'.$aResult['validData'].'</td>
					<td><font color="red">';
				$x .= $errorMag;
				$x .= '</font></td></tr>';
			}
			
			//-----------------------------------------------------------------------------------
//			if($aResult['validData']!=false){
//				$errCount++;
//			$x .= '<tr class="step2">
//					<td>'.$i.'</td>
//					<td>'.$aResult['validData'].'</td>
//					<td><font color="red">';
//			if(!$aResult['validDate']){
//				$x .= $kis_lang['invalidinterviewdateformat'];
//			}
//			else if(!$aResult['validTime']){
//				$x .= $kis_lang['invalidinterviewtimeformat'];
//			}
//			else
//				$x .= $kis_lang['invalidapplicationbirthno'];
//			$x .= '</font></td>
//				</tr>';
//			}
			$i++;
		}
		$x .= '</tbody></table>';
		return htmlspecialchars((count($data)-$errCount).",".$errCount.",".$x);
	}
	function importDataForImportAdmission($data){
		global $kis_lang, $admission_cfg;
		$resultArr = array();
		array_shift($data);
		array_shift($data);
		foreach($data as $aData){
			global $UserID;
			//--- convert the text to key code [start]
		    if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['M'])){
				$aData[5] = 'M';
			}
			else if(strtoupper($aData[5]) == strtoupper($kis_lang['Admission']['genderType']['F'])){
				$aData[5] = 'F';
			}
			
			$religionArr = $this->returnPresetCodeAndNameArr("RELIGION");
			foreach($religionArr as $aReligion){
				if (strtoupper($aReligion[1]) == strtoupper($aData[15])){
					$aData[15] = $aReligion[0];
					break;
				}
			}
			
			//valid Apply day type
			if($aData[26] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[26]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[26] = $j;
						break;
					}
				}
			}
			
			if($aData[27] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[27]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[27] = $j;
						break;
					}
				}
			}
			
			if($aData[28] != ''){
				for($j=1; $j<=count($kis_lang['Admission']['TimeSlot']); $j++ ){
					if(strtoupper($aData[28]) == strtoupper($kis_lang['Admission']['TimeSlot'][$j])){
						$aData[28] = $j;
						break;
					}
				}
			}
			
			//valid birth cert type
			foreach($admission_cfg['BirthCertType'] as $_key => $_type){
				if($aData[8] == $kis_lang['Admission']['BirthCertType'][$_key]){
					$aData[8] =$_type;
					break;
				}
			}
			
			//--- Henry Added 20140827
			//valid record status
//			foreach($admission_cfg['Status'] as $_key => $_status){
//				if (strtoupper($kis_lang['Admission']['Status'][$_key]) == strtoupper($aData[41])){
//					$aData[41] = $_status;
//					break;
//				}
//			}
//			
//			//valid Receipt Date
//			$aData[43] = getDefaultDateFormat($aData[43]);
//			
//			//valid is notified
//
//			if(strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']) || strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['no'])){
//				$aData[46] = (strtoupper($aData[46]) == strtoupper($kis_lang['Admission']['yes']));
//			}
			//--- convert the text to key code [end]
		    
		    $result = array();
		    
		    $sql = "
				UPDATE 
					ADMISSION_STU_INFO stu 
				INNER JOIN 
					ADMISSION_OTHERS_INFO o ON stu.ApplicationID = o.ApplicationID
				SET
	     			stu.ChineseName = '".$aData[3]."',
	      			stu.EnglishName = '".$aData[4]."', 
	      			stu.Gender = '".$aData[5]."',
	      			stu.DOB = '".getDefaultDateFormat($aData[6])." 00:00:00',	
	      			stu.PlaceOfBirth = '".$aData[10]."',
	      			stu.Province = '".$aData[11]."',
	      			stu.HomeTelNo = '".$aData[12]."',
					stu.BirthCertType = '".$aData[8]."' ,
	      			stu.BirthCertNo = '".$aData[9]."' ,
	      			stu.Email = '".$aData[14]."',
	      			stu.ReligionCode = '".$aData[15]."' ,
	      			stu.Church = '".$aData[16]."',
	      			stu.LastSchool = '".$aData[39]."',
	      			stu.Address = '".$aData[13]."',
					stu.Age = '".$aData[7]."',
					stu.DateModified = NOW(),
				    stu.ModifiedBy = '".$UserID."',
					o.ApplyDayType1 = '".$aData[26]."',
					o.ApplyDayType2 = '".$aData[27]."',
					o.ApplyDayType3 = '".$aData[28]."',
					o.SiblingAppliedName = '".$aData[30]."',
					o.ExBSName = '".$aData[33]."',
					o.ExBSName2 = '".$aData[35]."',
					o.ExBSName3 = '".$aData[37]."',
					o.ExBSGradYear = '".$aData[34]."',
					o.ExBSGradYear2 = '".$aData[36]."',
					o.ExBSGradYear3 = '".$aData[38]."',
					o.Remarks =  '".$aData[40]."', 
				    o.DateModified = NOW(),
				    o.ModifiedBy = '".$UserID."'
				WHERE 
					o.ApplicationID = '".$aData[1]."'
	    	";
		    
		    $result[] = $this->db_db_query($sql);
		    
		    $sql = "
				UPDATE 
					ADMISSION_PG_INFO pg
		    	SET
	     			pg.ChineseName = '".$aData[17]."',
	     			pg.JobTitle = '".$aData[19]."',
	     			pg.HKID = '".$aData[18]."',
	     			pg.Mobile = '".$aData[20]."',
					pg.DateModified = NOW(),
					pg.ModifiedBy = '".$UserID."'
				WHERE 
					pg.ApplicationID = '".$aData[1]."' AND pg.PG_TYPE = 'F'
		   	";
		   	
		   	$result[] = $this->db_db_query($sql);
		   	
		   	$sql = "
				UPDATE 
					ADMISSION_PG_INFO pg
		    	SET
	     			pg.ChineseName = '".$aData[21]."',
	     			pg.JobTitle = '".$aData[22]."',
	     			pg.HKID = '".$aData[23]."',
	     			pg.Mobile = '".$aData[24]."',
					pg.DateModified = NOW(),
					pg.ModifiedBy = '".$UserID."'
				WHERE 
					pg.ApplicationID = '".$aData[1]."' AND pg.PG_TYPE = 'M'
		   	";
		   
		    $result[] = $this->db_db_query($sql);
		    
//		    $sql = "
//				UPDATE 
//					ADMISSION_APPLICATION_STATUS
//		    	SET
//	     			Status = '".$aData[41]."',
//					ReceiptID = '".$aData[42]."',
//					ReceiptDate = '".$aData[43]." 00:00:00',
//					Handler = '".$aData[44]."',
//					InterviewDate = '".$aData[45]."',
//					isNotified = '".$aData[46]."',
//					DateModified = NOW(),
//					ModifiedBy = '".$UserID."'
//				WHERE 
//					ApplicationID = '".$aData[1]."'
//		   	";
//		   
//		    $result[] = $this->db_db_query($sql);
		     
			$resultArr[] = !in_array(false, $result);
			//debug_pr($aData);
		}
		return $resultArr;
	}
	
	function getExportHeader($lang = ''){
		global $kis_lang, $Lang, $intranet_root, $PATH_WRT_ROOT, $intranet_session_language;
		
		$isCustomField = is_array($_POST['customFields']);
		$customFields = (array)$_POST['customFields'];
		$headerArray = array();
		
		if($lang == 'en'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}else if($lang == 'b5'){
			$temp_intranet_session_language = $intranet_session_language;
			$intranet_session_language = $lang;
			include_once($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
			$intranet_session_language = $temp_intranet_session_language;
		}
		
		//for student info
		$headerArray[] = $kis_lang['Admission']['admissiondate'];
		$headerArray[] = $kis_lang['applicationno'];
		$headerArray[] = $kis_lang['Admission']['applyLevel'];
		
		$headerArray['studentInfo'] = array();
		$stuInfos = array(
			'student_info_chinesename' => $kis_lang['Admission']['chinesename'],
			'student_info_englishname' => $kis_lang['Admission']['englishname'],
			'student_info_dateofbirth' => $kis_lang['Admission']['dateofbirth'],
			'student_info_gender' => $kis_lang['Admission']['gender'],
			'student_info_birthcertno' => $kis_lang['Admission']['birthcertno'],
			'student_info_placeofbirth' => $kis_lang['Admission']['placeofbirth'],
			'student_info_nationality' => $kis_lang['Admission']['RMKG']['nationality'],
			'student_info_SpokenLanguage' => $kis_lang['Admission']['HKUGAPS']['SpokenLanguage'],
			'student_info_homephoneno' => $kis_lang['Admission']['homephoneno'],
			'student_info_homeaddress' => $kis_lang['Admission']['homeaddress'],
			'student_info_email' => $kis_lang['Admission']['MINGWAI']['email'],
			'student_info_email2' => $kis_lang['Admission']['MINGWAI']['email2'],
			'student_info_twins' => $kis_lang['Admission']['KTLMSKG']['twins'],
			'student_info_twinsID' => $kis_lang['Admission']['KTLMSKG']['twinsID'],
			'student_info_lastschool' => $kis_lang['Admission']['lastschool'],
			'student_info_lastschoollevel' => $kis_lang['Admission']['lastschoollevel'],
		);
		foreach($stuInfos as $key => $lang){
		    if(!$isCustomField || in_array($key, $customFields)){
		        $headerArray['studentInfo'][] = $lang;
		    }
		}
		
		//for parent info
		$headerArray['parentInfoF'] = array();
		$headerArray['parentInfoM'] = array();
		$headerArray['parentInfoG'] = array();
	    if(!$isCustomField || in_array('parent_info', $customFields)){
			$headerArray['parentInfoF'][] = $kis_lang['Admission']['RMKG']['name'];
			$headerArray['parentInfoF'][] = $kis_lang['Admission']['RMKG']['occupation'];
			$headerArray['parentInfoF'][] = $kis_lang['Admission']['RMKG']['company'];
			$headerArray['parentInfoF'][] = $kis_lang['Admission']['RMKG']['mobile'];
			
			$headerArray['parentInfoM'][] = $kis_lang['Admission']['RMKG']['name'];
			$headerArray['parentInfoM'][] = $kis_lang['Admission']['RMKG']['occupation'];
			$headerArray['parentInfoM'][] = $kis_lang['Admission']['RMKG']['company'];
			$headerArray['parentInfoM'][] = $kis_lang['Admission']['RMKG']['mobile'];
			
			$headerArray['parentInfoG'][] = $kis_lang['Admission']['name'];
			$headerArray['parentInfoG'][] = $kis_lang['Admission']['mgf']['occupation'];
			$headerArray['parentInfoG'][] = $kis_lang['Admission']['RMKG']['company'];
			$headerArray['parentInfoG'][] = $kis_lang['Admission']['csm']['mobile'];
	    }
	    
		//for other info
		$headerArray['otherInfo'] = array();
	    if(!$isCustomField || in_array('other_info', $customFields)){
			$headerArray['otherInfo'][] = $kis_lang['Admission']['Option'].'(1)';
			$headerArray['otherInfo'][] = $kis_lang['Admission']['Option'].'(2)';
			$headerArray['otherInfo'][] = $kis_lang['Admission']['Option'].'(1)';
			$headerArray['otherInfo'][] = $kis_lang['Admission']['Option'].'(2)';
			$headerArray['otherInfo'][] = $kis_lang['Admission']['Option'].'(3)';
			$headerArray['otherInfo'][] = $kis_lang['Admission']['ExBSName'];
			$headerArray['otherInfo'][] = $kis_lang['Admission']['yearOfGraduation'];
			$headerArray['otherInfo'][] = $kis_lang['Admission']['CurBSName'];
			$headerArray['otherInfo'][] = $kis_lang['Admission']['class'];
	    }
		//for official use
		$headerArray['officialUse'] = array();
	    if(!$isCustomField || in_array('remarks_info', $customFields)){
			$headerArray['officialUse'][] = $kis_lang['Admission']['applicationstatus'];
			$headerArray['officialUse'][] = $kis_lang['Admission']['MINGWAI']['admissionMethod'];
			$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['receiptcode'].")";
			$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['date'].")";
			$headerArray['officialUse'][] = $kis_lang['Admission']['applicationfee']." (".$kis_lang['Admission']['handler'].")";
//			$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'];
//			$headerArray['officialUse'][] = $kis_lang['Admission']['interviewlocation'];
			$headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (1)";
	        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (2)";
	        $headerArray['officialUse'][] = $kis_lang['Admission']['interviewdate'] . " (3)";
			$headerArray['officialUse'][] = $kis_lang['Admission']['briefing'];
	// 		$headerArray['officialUse'][] = $kis_lang['Admission']['briefingdate'];
	// 		$headerArray['officialUse'][] = $kis_lang['Admission']['briefinginformation'];		
			$headerArray['officialUse'][] = $kis_lang['Admission']['MINGWAI']['fee'];
			$headerArray['officialUse'][] = $kis_lang['Admission']['isnotified'];
			$headerArray['officialUse'][] = $kis_lang['Admission']['otherremarks'];
	    }
	    
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		$exportColumn[0][] = "";
		
		//student info header
		if(count($headerArray['studentInfo'])){
			$exportColumn[0][] = $kis_lang['Admission']['studentInfo'];
			for($i=0; $i < count($headerArray['studentInfo'])-1; $i++){
				$exportColumn[0][] = "";
			}
		}
		
		//parent info header
		if(count($headerArray['parentInfoF'])){
			$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['F'].")";
			for($i=0; $i < count($headerArray['parentInfoF'])-1; $i++){
				$exportColumn[0][] = "";
			}
		}
		if(count($headerArray['parentInfoM'])){
			$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['M'].")";
			for($i=0; $i < count($headerArray['parentInfoM'])-1; $i++){
				$exportColumn[0][] = "";
			}
		}
		if(count($headerArray['parentInfoG'])){
			$exportColumn[0][] = $kis_lang['Admission']['PGInfo']."(".$kis_lang['Admission']['PG_Type']['G'].")";
			for($i=0; $i < count($headerArray['parentInfoG'])-1; $i++){
				$exportColumn[0][] = "";
			}
		}
		if(count($headerArray['otherInfo'])){
			$exportColumn[0][] = $kis_lang['Admission']['applyTerm'];
			$exportColumn[0][] = "";
			$exportColumn[0][] = $kis_lang['Admission']['applyDayType'];
			$exportColumn[0][] = "";
			$exportColumn[0][] = "";
			$exportColumn[0][] = "";
			$exportColumn[0][] = "";
			$exportColumn[0][] = "";
			$exportColumn[0][] = "";
		}
		if(count($headerArray['officialUse'])){
			$exportColumn[0][] = $kis_lang['remarks'];
		}
		//other info header
// 		$exportColumn[0][] = $kis_lang['Admission']['RMKG']['SpokenLang'];
// 		for($i=0; $i < count($headerArray['otherInfo'])-1; $i++){
// 			$exportColumn[0][] = "";
// 		}
		
//		//other2 info header
//		$exportColumn[0][] = $kis_lang['Admission']['CHIUCHUNKG']['CurrentSchoolInfo'];
//		for($i=0; $i < count($headerArray['otherInfo2'])-1; $i++){
//			$exportColumn[0][] = "";
//		}
		
		//other3 info header
// 		$exportColumn[0][] = $kis_lang['Admission']['RMKG']['RelativeStudiedAtSchool'];
// 		for($i=0; $i < count($headerArray['otherInfo3'])-1; $i++){
// 			$exportColumn[0][] = "";
// 		}
		
		//official use header
// 		$exportColumn[0][] = $kis_lang['remarks'];
// 		for($i=0; $i < count($headerArray['officialUse'])-1; $i++){
// 			$exportColumn[0][] = "";
// 		}
	
		//sub header
		$exportColumn[1] = array_merge(array($headerArray[0],$headerArray[1],$headerArray[2]), $headerArray['studentInfo'], $headerArray['parentInfoF'], $headerArray['parentInfoM'],$headerArray['parentInfoG'], $headerArray['otherInfo'], $headerArray['officialUse']);
		//if($lang)
		//include($intranet_root."/lang/kis/apps/lang_admission_".$intranet_session_language.".php");
		return $exportColumn;
	}
	
	function getExportData($schoolYearID,$classLevelID='',$applicationID='',$recordID=''){
		global $admission_cfg, $Lang, $kis_lang, $lauc;
		
		$isCustomField = is_array($_POST['customFields']);
		$customFields = (array)$_POST['customFields'];
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		//$otherInfo2 = $this->getApplicationStudentInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfoEx = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID,'EX');
		$otherInfoCur = $this->getApplicationRelativesInfoCust($schoolYearID,$classLevelID,$applicationID,$recordID,'CUR');
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$student_name_en = explode(',',$studentInfo['student_name_en']);
		$student_name_b5 = explode(',',$studentInfo['student_name_b5']);
		
		$dataArray = array();
		
		//for student info
		$dataArray[] = substr($otherInfo['DateInput'], 0, -9);
		$dataArray[] = $studentInfo['applicationID'];
		$classLevel = $this->getClassLevel();
		$dataArray[] = $classLevel[$otherInfo['classLevelID']];
		
		$dataArray['studentInfo'] = array();
		$stuInfos = array(
		'student_info_chinesename' => $student_name_b5[0],
		'student_info_englishname' => $student_name_en[0],
		'student_info_dateofbirth' => $studentInfo['dateofbirth'],
		'student_info_gender' => $Lang['Admission']['genderType'][$studentInfo['gender']],
		'student_info_birthcertno' => $studentInfo['birthcertno'],
		'student_info_placeofbirth' => $studentInfo['placeofbirth'],
		'student_info_nationality' => $studentInfo['Nationality'],
		);
   		if(in_array($studentInfo['LangSpokenAtHome'], $admission_cfg['lang'])){
   		    foreach($admission_cfg['lang'] as $key => $langVal){
   		        if($langVal == $studentInfo['LangSpokenAtHome']){
   		            $stuInfos['student_info_SpokenLanguage'] = $kis_lang['Admission']['MINGWAI'][ $key ];
   		            break;
   		        }
   		    }
   		}else{
   		    $stuInfos['student_info_SpokenLanguage'] = $studentInfo['LangSpokenAtHome'];
   		}
		$stuInfos['student_info_homephoneno'] = $studentInfo['homephoneno'];
		
		$address = '';
		$_address = $studentInfo['AddressRoom'] . $studentInfo['AddressFloor'] . $studentInfo['AddressBlock'] . $studentInfo['AddressBldg'] . $studentInfo['AddressStreet'] . $studentInfo['AddressDistrict'];
	    if(preg_match("/[\x{4e00}-\x{9fa5}]+/u", $_address)){
	        if($studentInfo['AddressEstate'] != ''){
		        switch ($studentInfo['AddressEstate']){
		            case 1:
		                $address .= '香港';
		                break;
		            case 2:
		                $address .= '九龍';
		                break;
		            case 3:
		                $address .= '新界';
		                break;
		        }
	        }
	        if($studentInfo['AddressDistrict'] != ''){
	            $address .= "{$studentInfo['AddressDistrict']}";
	        }
	        if($studentInfo['AddressStreet'] != ''){
	            $address .= "{$studentInfo['AddressStreet']}";
	        }
	        if($studentInfo['AddressBldg'] != ''){
	            $address .= "{$studentInfo['AddressBldg']}";
	        }
	        if($studentInfo['AddressBlock'] != ''){
	            $address .= "{$studentInfo['AddressBlock']}座";
	        }
	        if($studentInfo['AddressFloor'] != ''){
	            $address .= "{$studentInfo['AddressFloor']}樓";
	        }
	        if($studentInfo['AddressRoom'] != ''){
	            $address .= "{$studentInfo['AddressRoom']}室";
	        }
	    }else{
	        if($studentInfo['AddressRoom'] != ''){
	            $address .= "Room {$studentInfo['AddressRoom']}, ";
	        }
	        if($studentInfo['AddressFloor'] != ''){
	            $address .= "Floor {$studentInfo['AddressFloor']}, ";
	        }
	        if($studentInfo['AddressBlock'] != ''){
	            $address .= "Block {$studentInfo['AddressBlock']}, ";
	        }
	        if($studentInfo['AddressBldg'] != ''){
	            $address .= "{$studentInfo['AddressBldg']}, ";
	        }
	        if($studentInfo['AddressStreet'] != ''){
	            $address .= "{$studentInfo['AddressStreet']}, ";
	        }
	        if($studentInfo['AddressDistrict'] != ''){
	            $address .= "{$studentInfo['AddressDistrict']}, ";
	        }
	        if($studentInfo['AddressEstate'] != ''){
		        switch ($studentInfo['AddressEstate']){
		            case 1:
		                $address .= 'Hong Kong';
		                break;
		            case 2:
		                $address .= 'Kowloon';
		                break;
		            case 3:
		                $address .= 'New Territories';
		                break;
		        }
	        }
	    }
		$stuInfos['student_info_homeaddress'] = $address;
   		
		$role = $this->getApplicationCustInfo($studentInfo['applicationID'], 'PrimaryEmailRole', 1);
   		$role = $role['Value'];
       	$stuInfos['student_info_email'] = "({$kis_lang['Admission']['PG_Type'][$role]}) {$studentInfo['Email']}";
   		
		$role = $this->getApplicationCustInfo($studentInfo['applicationID'], 'SecondaryEmailRole', 1);
   		$role = $role['Value'];
       	$stuInfos['student_info_email2'] = "({$kis_lang['Admission']['PG_Type'][$role]}) {$studentInfo['Email2']}";

       	$stuInfos['student_info_twins'] = ($studentInfo['IsTwinsApplied'])? $kis_lang['Admission']['yes'] : $kis_lang['Admission']['no'];
       	$stuInfos['student_info_twinsID'] = ($studentInfo['IsTwinsApplied'])? $studentInfo['TwinsApplicationID'] : '';
		$stuInfos['student_info_lastschool'] = $studentInfo['LastSchool'];
		$stuInfos['student_info_lastschoollevel'] = $studentInfo['LastSchoolLevel'];

		foreach($stuInfos as $key => $value){
		    if(!$isCustomField || in_array($key, $customFields)){
		        $dataArray['studentInfo'][] = $value;
		    }
		}
		
		//for parent info		
		$dataArray['parentInfoF'] = array();
		$dataArray['parentInfoM'] = array();
		$dataArray['parentInfoG'] = array();
	    if(!$isCustomField || in_array('parent_info', $customFields)){
			for($i=0;$i<count($parentInfo);$i++){
				if($parentInfo[$i]['type'] == 'F'){
					$dataArray['parentInfoF'][] = $parentInfo[$i]['ChineseName'];
					$dataArray['parentInfoF'][] = $parentInfo[$i]['JobTitle'];
					$dataArray['parentInfoF'][] = $parentInfo[$i]['Company'];
					$dataArray['parentInfoF'][] = $parentInfo[$i]['Mobile'];
					
				}
				else if($parentInfo[$i]['type'] == 'M'){
					$dataArray['parentInfoM'][] = $parentInfo[$i]['ChineseName'];
					$dataArray['parentInfoM'][] = $parentInfo[$i]['JobTitle'];
					$dataArray['parentInfoM'][] = $parentInfo[$i]['Company'];
					$dataArray['parentInfoM'][] = $parentInfo[$i]['Mobile'];
					
				}
				else if($parentInfo[$i]['type'] == 'G'){
					$dataArray['parentInfoG'][] = $parentInfo[$i]['ChineseName'];
					$dataArray['parentInfoG'][] = $parentInfo[$i]['JobTitle'];
					$dataArray['parentInfoG'][] = $parentInfo[$i]['Company'];
					$dataArray['parentInfoG'][] = $parentInfo[$i]['Mobile'];
				}
			}
			
			if(count($dataArray['parentInfoF']) == 0){
				$dataArray['parentInfoF'] = array('','','','','','','','');
			}
			if(count($dataArray['parentInfoM']) == 0){
				$dataArray['parentInfoM'] = array('','','','','','','','');
			}
			if(count($dataArray['parentInfoG']) == 0){
				$dataArray['parentInfoG'] = array('','','','','','','','');
			}
	    }
		
		//for other info
		$dataArray['otherInfo'] = array();
	    if(!$isCustomField || in_array('other_info', $customFields)){
			$dataArray['otherInfo'][] = $otherInfo['ApplyProgram1']== 1?'International Stream PN-K3 國際課程 PN-K3':($otherInfo['ApplyProgram1']== 2?'Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':'');
			$dataArray['otherInfo'][] = $otherInfo['ApplyProgram2']== 0?'Not Applicable 不適用':($otherInfo['ApplyProgram2']== 1?'International Stream PN-K3 國際課程 PN-K3':($otherInfo['ApplyProgram2']== 2?'Local Stream PN / Cambridge Stream K1 - K3 本地課程 PN / 劍橋英語課程 K1 - K3':''));
			$dataArray['otherInfo'][] = $otherInfo['ApplyDayType1']== 1?$kis_lang['Admission']['TimeSlot'][1]:($otherInfo['ApplyDayType1']== 2?$kis_lang['Admission']['TimeSlot'][2]:($otherInfo['ApplyDayType1']== 3?$kis_lang['Admission']['TimeSlot'][3]:''));
			$dataArray['otherInfo'][] = $otherInfo['ApplyDayType2']== 1?$kis_lang['Admission']['TimeSlot'][1]:($otherInfo['ApplyDayType2']== 2?$kis_lang['Admission']['TimeSlot'][2]:($otherInfo['ApplyDayType2']== 3?$kis_lang['Admission']['TimeSlot'][3]:''));
			$dataArray['otherInfo'][] = $otherInfo['ApplyDayType3']== 1?$kis_lang['Admission']['TimeSlot'][1]:($otherInfo['ApplyDayType3']== 2?$kis_lang['Admission']['TimeSlot'][2]:($otherInfo['ApplyDayType3']== 3?$kis_lang['Admission']['TimeSlot'][3]:''));
			$dataArray['otherInfo'][] = $otherInfoEx[0]['Name'];
			$dataArray['otherInfo'][] = $otherInfoEx[0]['Year'];
			$dataArray['otherInfo'][] = $otherInfoCur[0]['Name'];
			$dataArray['otherInfo'][] = $otherInfoCur[0]['ClassPosition'];
	    }
// 		for($i=0;$i<4;$i++){
// 			$dataArray['otherInfo3'][] = $otherInfo3[$i]['Year'];
// 			$dataArray['otherInfo3'][] = $otherInfo3[$i]['Name'];
// 			$dataArray['otherInfo3'][] = $otherInfo3[$i]['Gender'];
// 			$dataArray['otherInfo3'][] = $lauc->getPrevClassValueLang($otherInfo3[$i]['ClassPosition']);
// 			$dataArray['otherInfo3'][] = $otherInfo3[$i]['Year'];
// 		}
		//for official use
		$dataArray['officialUse'] = array();
	    if(!$isCustomField || in_array('remarks_info', $customFields)){
			$dataArray['officialUse'][] = $Lang['Admission']['Status'][$status['status']];
	
			#### Briefing START ####
		    global $PATH_WRT_ROOT, $setting_path_ip_rel;
		    
		    ######## Get briefing exists START ########
			include_once ("{$PATH_WRT_ROOT}includes/admission/libadmission_briefing_base.php");
			include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/admission_briefing.php");
			$objBreafing = new admission_briefing();
			
			$briefing = $objBreafing->getBriefingApplicantByApplicantID($studentInfo['applicationID']);
			$briefingSession = $objBreafing->getBriefingSession($briefing['BriefingID']);
			#### Briefing END ####
			
			#### Fee START ####
			$fee = $this->getApplicationCustInfo($studentInfo['applicationID'], 'Fee', 1);
			$fee = explode(';', $fee['Value']);
			
			$feeHtml = '';
			foreach($fee as $f){
			    $feeHtml .= "{$kis_lang['Admission']['MINGWAI']['feeType'][$f]}, ";
			}
			$feeHtml = trim($feeHtml, ', ');
			#### Fee END ####
			
			#### Admission Method START ####
			$admissionMethod = $this->getApplicationCustInfo($studentInfo['applicationID'], 'AdmissionMethod', 1);
			$admissionMethod = $admissionMethod['Value'];
			$mailCode = $this->getApplicationCustInfo($studentInfo['applicationID'], 'MailCode', 1);
			$mailCode = $mailCode['Value'];
			
			$admissionMethodLang = $kis_lang['Admission']['MINGWAI']['admissionMethodType'][$admissionMethod];
			if($mailCode){
			    $admissionMethodLang .= " ({$kis_lang['Admission']['MINGWAI']['MailCode']}: {$mailCode})";
			}
			$dataArray['officialUse'][] = $admissionMethodLang;
			#### Admission Method END ####
			
			$dataArray['officialUse'][] = $status['receiptID'];
			$dataArray['officialUse'][] = $status['receiptdate'];
			$dataArray['officialUse'][] = $status['handler'];
//			$dataArray['officialUse'][] = $status['interviewdate'];
//			$dataArray['officialUse'][] = $status['interviewlocation'];
			$interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID']));
	        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, - 3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, - 3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
	        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID2']));
	        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, - 3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, - 3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
	        $interviewInfo = current($this->getInterviewSettingAry($otherInfo['InterviewSettingID3']));
	        $dataArray['officialUse'][] = $interviewInfo ? $interviewInfo['Date'] . ' (' . substr($interviewInfo['StartTime'], 0, - 3) . ' ~ ' . substr($interviewInfo['EndTime'], 0, - 3) . ')' . ($interviewInfo['GroupName'] ? ' ' . ($admission_cfg['interview_arrangment']['interview_group_type'] == 'Room' ? $kis_lang['interviewroom'] : $kis_lang['sessiongroup']) . ' ' . $interviewInfo['GroupName'] : '') : '';
			if($briefing){
			    $dataArray['officialUse'][] = "{$briefingSession['Title']} ({$kis_lang['Admission']['requestSeat']}: {$briefing['SeatRequest']})";
			}else{
			    $dataArray['officialUse'][] = '';
			}
	// 		$dataArray['officialUse'][] = $status['briefingdate'];
	// 		$dataArray['officialUse'][] = $status['briefinginfo'];
			$dataArray['officialUse'][] = $feeHtml;
			$dataArray['officialUse'][] = $Lang['Admission'][$status['isnotified']];
			$dataArray['officialUse'][] = $status['remark'];
	    }
	    
		$ExportArr = array_merge(array($dataArray[0],$dataArray[1], $dataArray[2]),$dataArray['studentInfo'],$dataArray['parentInfoF'],$dataArray['parentInfoM'],$dataArray['parentInfoG'], $dataArray['otherInfo'], $dataArray['officialUse']);
		
		return $ExportArr;
	}
	function getExportDataForImportAccount($schoolYearID,$classLevelID='',$applicationID='',$recordID='',$tabID=''){
		global $admission_cfg, $Lang, $plugin, $special_feature, $sys_custom;
		
		$studentInfo = current($this->getApplicationStudentInfo($schoolYearID,$classLevelID,$applicationID,'',$recordID));
		$parentInfo = $this->getApplicationParentInfo($schoolYearID,$classLevelID,$applicationID,$recordID);
		$otherInfo = current($this->getApplicationOthersInfo($schoolYearID,$classLevelID,$applicationID,$recordID));
		$status = current($this->getApplicationStatus($schoolYearID,$classLevelID,$applicationID,$recordID));
		
		$dataArray = array();
		
		if($tabID == 2){
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			$studentInfo['student_name_b5'] = str_replace(",", "", $studentInfo['student_name_b5']);
			$dataArray[0] = array();
			$dataArray[0][] = ''; //UserLogin
			$dataArray[0][] = ''; //Password
			$dataArray[0][] = ''; //UserEmail
			$dataArray[0][] = $studentInfo['student_name_en']; //EnglishName
			$dataArray[0][] = $studentInfo['student_name_b5']; //ChineseName
			$dataArray[0][] = ''; //NickName
			$dataArray[0][] = $studentInfo['gender']; //Gender
			$dataArray[0][] = ''; //Mobile
			$dataArray[0][] = ''; //Fax
			$dataArray[0][] = ''; //Barcode
			$dataArray[0][] = ''; //Remarks
			$dataArray[0][] = $studentInfo['dateofbirth']; //DOB
			$dataArray[0][] = (is_numeric($studentInfo['homeaddress'])?$Lang['Admission']['csm']['AddressLocation'][$studentInfo['homeaddress']]:$studentInfo['homeaddress']); //Address
			if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))
			{
				$dataArray[0][] = ''; //CardID
				if($sys_custom['SupplementarySmartCard']){
					$dataArray[0][] = ''; //CardID2
					$dataArray[0][] = ''; //CardID3
				}
			}
			if($special_feature['ava_hkid'])
				$dataArray[0][] = $studentInfo['birthcertno']; //HKID
			if($special_feature['ava_strn'])
				$dataArray[0][] = ''; //STRN
			if($plugin['medical'])
				$dataArray[0][] = ''; //StayOverNight
			$dataArray[0][] = ''; //Nationality
			$dataArray[0][] = $studentInfo['placeofbirth']; //PlaceOfBirth
			$dataArray[0][] = substr($otherInfo['DateInput'], 0, 10); //AdmissionDate
		}
		else if($tabID == 3){
			$hasParent = false;
			$dataCount = array();
			$studentInfo['student_name_en'] = str_replace(",", " ", $studentInfo['student_name_en']);
			for($i=0;$i<count($parentInfo);$i++){
				if($parentInfo[$i]['type'] == 'F' && !$hasParent){
					$dataArray[0] = array();
					$dataArray[0][] = ''; //UserLogin
					$dataArray[0][] = ''; //Password
					$dataArray[0][] = $parentInfo[$i]['Email']; //UserEmail
					$dataArray[0][] = $parentInfo[$i]['ChineseName']; //EnglishName
					$dataArray[0][] = $parentInfo[$i]['ChineseName']; //ChineseName
//					$dataArray[0][] = $parentInfo[$i]['parent_name_en']; //EnglishName
//					$dataArray[0][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[0][] = 'M'; //Gender
					$dataArray[0][] = $parentInfo[$i]['Mobile']; //Mobile
					$dataArray[0][] = ''; //Fax
					$dataArray[0][] = ''; //Barcode
					$dataArray[0][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[0][] = ''; //HKID
					$dataArray[0][] = ''; //StudentLogin1
					$dataArray[0][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[0][] = ''; //StudentLogin2
					$dataArray[0][] = ''; //StudentEngName2
					$dataArray[0][] = ''; //StudentLogin3
					$dataArray[0][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[0] = ($parentInfo[$i]['Email']?1:0)+($parentInfo[$i]['ChineseName']?1:0)+($parentInfo[$i]['ChineseName']?1:0)+($parentInfo[$i]['Mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'M' && !$hasParent){
					$dataArray[1] = array();
					$dataArray[1][] = ''; //UserLogin
					$dataArray[1][] = ''; //Password
					$dataArray[1][] = $parentInfo[$i]['Email']; //UserEmail
					$dataArray[1][] = $parentInfo[$i]['ChineseName']; //EnglishName
					$dataArray[1][] = $parentInfo[$i]['ChineseName']; //ChineseName
					$dataArray[1][] = 'F'; //Gender
					$dataArray[1][] = $parentInfo[$i]['Mobile']; //Mobile
					$dataArray[1][] = ''; //Fax
					$dataArray[1][] = ''; //Barcode
					$dataArray[1][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[1][] = ''; //HKID
					$dataArray[1][] = ''; //StudentLogin1
					$dataArray[1][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[1][] = ''; //StudentLogin2
					$dataArray[1][] = ''; //StudentEngName2
					$dataArray[1][] = ''; //StudentLogin3
					$dataArray[1][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[1] = ($parentInfo[$i]['Email']?1:0)+($parentInfo[$i]['ChineseName']?1:0)+($parentInfo[$i]['ChineseName']?1:0)+($parentInfo[$i]['Mobile']?1:0);
				}
				else if($parentInfo[$i]['type'] == 'G' && !$hasParent){
					$dataArray[2] = array();
					$dataArray[2][] = ''; //UserLogin
					$dataArray[2][] = ''; //Password
					$dataArray[2][] = $parentInfo[$i]['Email']; //UserEmail
					$dataArray[2][] = $parentInfo[$i]['parent_name_en']; //EnglishName
					$dataArray[2][] = $parentInfo[$i]['parent_name_b5']; //ChineseName
					$dataArray[2][] = ''; //Gender
					$dataArray[2][] = $parentInfo[$i]['Mobile']; //Mobile
					$dataArray[2][] = ''; //Fax
					$dataArray[2][] = ''; //Barcode
					$dataArray[2][] = ''; //Remarks
					if($special_feature['ava_hkid'])
						$dataArray[2][] = ''; //HKID
					$dataArray[2][] = ''; //StudentLogin1
					$dataArray[2][] = $studentInfo['student_name_en']; //StudentEngName1
					$dataArray[2][] = ''; //StudentLogin2
					$dataArray[2][] = ''; //StudentEngName2
					$dataArray[2][] = ''; //StudentLogin3
					$dataArray[2][] = ''; //StudentEngName3
					//$hasParent = true;
					$dataCount[2] = ($parentInfo[$i]['Email']?1:0)+($parentInfo[$i]['ChineseName']?1:0)+($parentInfo[$i]['ChineseName']?1:0)+($parentInfo[$i]['Mobile']?1:0);
				}
			}
			
//			if($dataCount[0] > 0 && $dataCount[0] >= $dataCount[1] && $dataCount[0] >= $dataCount[2]){
//				$tempDataArray = $dataArray[0];
//			}
//			else if($dataCount[1] > 0 && $dataCount[1] >= $dataCount[0] && $dataCount[1] >= $dataCount[2]){
//				$tempDataArray = $dataArray[1];
//			}
//			else if($dataCount[2] > 0){
//				$tempDataArray = $dataArray[2];
//			}
			if($dataCount[1] > 0){
				$tempDataArray = $dataArray[1];
			}
			else{
				$tempDataArray = $dataArray[0];
			}
			$dataArray = array();
			$dataArray[0] = $tempDataArray;
		}
		$ExportArr = $dataArray;
		
		return $ExportArr;
	}
	function hasBirthCertNumber($birthCertNo, $applyLevel){
		$sql = "SELECT COUNT(*) FROM ADMISSION_STU_INFO AS asi JOIN ADMISSION_OTHERS_INFO AS aoi ON asi.ApplicationID = aoi.ApplicationID WHERE TRIM(asi.BirthCertNo) = '{$birthCertNo}' AND aoi.ApplyYear = '".$this->getNextSchoolYearID()."'";
		return current($this->returnVector($sql));
	}
	
	function hasToken($token){
		$sql = "SELECT COUNT(*) FROM ADMISSION_OTHERS_INFO WHERE Token = '".$token."' ";
		return current($this->returnVector($sql));
	}
	
	/*
	 * @param $sendTarget : 1 - send to all , 2 - send to those success, 3 - send to those failed, 4 - send to those have not acknowledged
	 */
	public function sendMailToNewApplicant($applicationId,$subject,$message, $isResend=false)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$webmasterMailAddress = explode("<",$libwebmail->GetWebmasterMailAddress());
		$from = "Ming Wai Int'l Kindergarten & Ming Wai Int'l Preschool(North Point) <".$webmasterMailAddress[1];
		$inputby = $_SESSION['UserID'];
		$result = array();
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		//debug_pr($applicationId);
		$to_email = $records['Email'];
		if($subject == ''){
			if($isResend){
				$email_subject = "[Reminder]Ming Wai Int'l Kindergarten & Ming Wai Int'l Preschool(North Point) Application Notification [溫馨提示]明慧國際幼稚園‧明慧國際幼兒園(北角)入學申請通知";
			}
			else{
				$email_subject = "Ming Wai Int'l Kindergarten & Ming Wai Int'l Preschool(North Point) Application Notification 明慧國際幼稚園‧明慧國際幼兒園(北角)入學申請通知";
			}
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	public function sendMailToNewApplicantWithReceiver($applicationId,$subject,$message, $to_email)
	{
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		include_once($intranet_root."/includes/libwebmail.php");
		$libwebmail = new libwebmail();
		
		$from = $libwebmail->GetWebmasterMailAddress();

		if($subject == ''){
			$email_subject = "Ming Wai Int'l Kindergarten & Ming Wai Int'l Preschool(North Point) Application Notification 明慧國際幼稚園‧明慧國際幼兒園(北角)入學申請通知";
		}
		else{
			$email_subject = $subject;
		}
		$email_message = $message;
			$sent_ok = true;
			if($to_email != '' && intranet_validateEmail($to_email)){
				$sent_ok = $libwebmail->sendMail($email_subject,$email_message,$from,array($to_email),array(),array(),"",$IsImportant="",$mail_return_path=get_webmaster(),$reply_address="",$isMulti=null,$nl2br=0);
			}else{
				$sent_ok = false;
			}
			
		return $sent_ok;
	}
	
	function getApplicantEmail($applicationId){
		global $intranet_root, $kis_lang, $PATH_WRT_ROOT;
		
		$sql = "SELECT 
					f.RecordID as UserID,
					a.ApplicationID as ApplicationNo,
					a.ChineseName,
					a.EnglishName,
					a.Email as Email
				FROM ADMISSION_OTHERS_INFO as f 
				INNER JOIN ADMISSION_STU_INFO as a ON a.ApplicationID=f.ApplicationID 
				WHERE f.ApplicationID = '".trim($applicationId)."'
				ORDER BY a.ApplicationID";
		$records = current($this->returnArray($sql));
		
		return $records['Email'];
	}
	
	function checkImportDataForImportInterview($data){
		$resultArr = array();
		$i=0;
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
			//check date
			if ($aData[4] =='' && $aData[5] ==''){
				$validDate = true;
			}
			else if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $aData[4]) ) {
		       list($year , $month , $day) = explode('-',$aData[4]);
		       $validDate = checkdate($month , $day , $year);
		    } else {
		       $validDate =  false;
		    }

		    //check time
		    if ($aData[4] =='' && $aData[5] ==''){
				$validTime = true;
			}
			else if ( preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $aData[5]) ) {
		       $validTime = true;
		    } else {
		       $validTime =  false;
		    }
			$sql = "
				SELECT
					COUNT(*)
				FROM
					ADMISSION_STU_INFO s
				WHERE 
					trim(s.applicationID) = '".trim($aData[0])."' AND trim(s.birthCertNo) = '".trim($aData[3])."'
	    	";
			$result = $this->returnVector($sql);
			if($result[0] == 0){
				$resultArr[$i]['validData'] = $aData[0];
			}
			else
				$resultArr[$i]['validData'] = false;
			$resultArr[$i]['validDate'] = $validDate;
			$resultArr[$i]['validTime'] = $validTime;
			if(!$validDate || !$validTime)
				$resultArr[$i]['validData'] = $aData[0];
			$i++;
		}
		return $resultArr;
	}
	
	function importDataForImportInterview($data){
		$resultArr = array();
		foreach($data as $aData){
		    $aData[4] = getDefaultDateFormat($aData[4]);
		    
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET 
		   		InterviewDate = '".$aData[4]." ".$aData[5]."',
				InterviewLocation = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
				$resultArr[] = $result;
			
		}
		return $resultArr;
	}
	
	function importDataForImportBriefing($data){
		$resultArr = array();
		foreach($data as $aData){
			$aData[4] = getDefaultDateFormat($aData[4]);
	
			$sql = "
				UPDATE ADMISSION_APPLICATION_STATUS SET
		   		BriefingDate = '".$aData[4]." ".$aData[5]."',
		   		BriefingInfo = '".$aData[6]."',
				DateModified = NOW(),
		   		ModifiedBy = '".$this->uid."'
   				WHERE ApplicationID = '".$aData[0]."'
	    	";
			$result = $this->db_db_query($sql);
			$resultArr[] = $result;
				
		}
		return $resultArr;
	}
	
	function getExportDataForImportInterview($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',DATE(a.InterviewDate),'') As interviewdate,
				IF(a.InterviewDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.InterviewDate,'%H:%i'),'') As interviewtime,
				a.InterviewLocation interviewlocation
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function getExportDataForImportBriefing($recordID, $schoolYearID='', $selectStatus='',$classLevelID=''){
		global $admission_cfg;
		$cond = !empty($schoolYearID)?" AND a.SchoolYearID='".$schoolYearID."'":"";
		$cond .= !empty($recordID)?" AND o.RecordID='".$recordID."'":"";
		$cond .= !empty($selectStatus)?" AND a.Status='".$selectStatus."'":"";
		$cond .= !empty($classLevelID)?" AND o.ApplyLevel='".$classLevelID."'":"";
		$sql = "
			SELECT
     			a.ApplicationID applicationID,
     			s.EnglishName englishName,
				s.ChineseName chineseName,
				s.BirthCertNo birthCertNo,
				IF(a.BriefingDate<>'0000-00-00 00:00:00',DATE(a.BriefingDate),'') As briefingdate,
				IF(a.BriefingDate<>'0000-00-00 00:00:00',TIME_FORMAT(a.BriefingDate,'%H:%i'),'') As briefingtime,
				a.BriefingInfo briefinginfo
			FROM
				ADMISSION_APPLICATION_STATUS a
			INNER JOIN
				ADMISSION_STU_INFO s ON a.ApplicationID = s.ApplicationID
			INNER JOIN
				ADMISSION_OTHERS_INFO o ON a.ApplicationID = o.ApplicationID
			WHERE 1
				".$cond."
    	";
		$applicationAry = $this->returnArray($sql);
		return $applicationAry;
	}
	
	function getLangByValue($selectedValue,$ConfigArr,$OtherValue="",$OtherLang=""){
		global $Lang,$kis_lang;
				
		foreach($ConfigArr as $_key => $_val){
			if($selectedValue == $_val){
				
				if(!empty($OtherValue) && $selectedValue == $OtherValue){
					$returnValue = $OtherLang;
				}
				else{
					$returnValue = $Lang['Admission']['CHIUCHUNKG'][$_key];
					if($returnValue == ''){
						$returnValue = $kis_lang['Admission']['CHIUCHUNKG'][$_key];
					}
					break;
				}
			}
			
		}
		return $returnValue;
	}
	
	
	function reorderApplicantForArrangement($a, $b){
	    #### IsTeacher START ####
	    $teacherCheckingArr = array('教師', '老師', 'teacher');
	    
	    $aJobTitle = strtolower("{$a['F_JobTitle']};{$a['M_JobTitle']};{$a['G_JobTitle']}");
	    $bJobTitle = strtolower("{$b['F_JobTitle']};{$b['M_JobTitle']};{$b['G_JobTitle']}");
	    
	    $aIsTeacher = false;
	    $bIsTeacher = false;
	    foreach($teacherCheckingArr as $checking){
	        if(strpos($aJobTitle, $checking) !== false){
	            $aIsTeacher = true;
	        }
	        if(strpos($bJobTitle, $checking) !== false){
	            $bIsTeacher = true;
	        }
	    }
	    
	    if($aIsTeacher && !$bIsTeacher){
	        return -1;
	    }elseif(!$aIsTeacher && $bIsTeacher){
	        return 1;
	    }
	    #### IsTeacher END ####
	    
	    #### YearName START ####
	    $teacherCheckingArr = array('k1', 'k2', 'k3');
	     
	    $aYearName = strtolower($a['YearName']);
	    $bYearName = strtolower($b['YearName']);
	     
	    $aIsK = false;
	    $bIsK = false;
	    foreach($teacherCheckingArr as $checking){
	        if(strpos($aYearName, $checking) !== false){
	            $aIsK = true;
	        }
	        if(strpos($bYearName, $checking) !== false){
	            $bIsK = true;
	        }
	    }
	    
	    if($aIsK && !$bIsK){
	        return -1;
	    }elseif(!$aIsK && $bIsK){
	        return 1;
	    }
	    #### YearName END ####
	    
	    #### Birthday START ####
	    $aBirthMonth = substr($a['DOB'], 5, 2);
	    $bBirthMonth = substr($b['DOB'], 5, 2);
	    if($aBirthMonth != $bBirthMonth){
	        return strcmp($aBirthMonth, $bBirthMonth);
	    }
	    #### Birthday END ####
	    
	    return 0;
	}
	
	function updateApplicantArrangement($selectSchoolYearID, $selectStatusArr = array(), $round = 1){
		$status_cond = '';
		if(sizeof($selectStatusArr) > 0){
			$status_cond .= " AND AAS.status in ('".implode("','",$selectStatusArr)."') ";
		}
		
		$round_cond = " AND Round = '".$round."' ";
		$roundSQL = ($round > 1)?$round:'';
		
		######## Get interview arrangement setting START ########
		$sql = "SELECT 
		    *
		FROM 
		    ADMISSION_INTERVIEW_SETTING
	    WHERE
		    SchoolYearID = '{$selectSchoolYearID}'
	    AND 
	        GroupName IS NOT NULL
		    {$round_cond} 
		ORDER BY 
		    Date, StartTime, GroupName";
		$rs = $this->returnArray($sql);
		
		### Ordering date by Saturday and Sunday first START ####
		$satSunDay = array();
		$normalDay = array();
		
		foreach($rs as $r){
		    if(date('N', strtotime($r['Date'])) >= 6){
		        $satSunDay[] = $r;
		    }else{
		        $normalDay[] = $r;
		    }
		}
		
		$interviewGroupArr = array_merge($satSunDay, $normalDay);
		### Ordering date by Saturday and Sunday first END ####
		######## Get interview arrangement setting END ########
		
		######## Get applicants START ########
		$sql = "SELECT
		    AOI.ApplicationID,
		    AOI.ApplyLevel,
		    Y.YearName,
		    ASI.DOB,
		    API_F.JobTitle AS F_JobTitle,
		    API_M.JobTitle AS M_JobTitle,
		    API_G.JobTitle AS G_JobTitle
	    FROM
		    ADMISSION_OTHERS_INFO AOI
		    
	    INNER JOIN
		    ADMISSION_STU_INFO ASI
	    ON
	        AOI.ApplicationID = ASI.ApplicationID
        INNER JOIN 
          YEAR as Y 
        ON 
          Y.YearID = AOI.ApplyLevel 
        INNER JOIN 
          ADMISSION_APPLICATION_STATUS as AAS 
        ON 
          AOI.ApplicationID = AAS.ApplicationID
	    LEFT JOIN
		    ADMISSION_PG_INFO API_F
	    ON
		    AOI.ApplicationID = API_F.ApplicationID
	    AND
		    API_F.PG_TYPE = 'F'
	    LEFT JOIN
		    ADMISSION_PG_INFO API_M
	    ON
		    AOI.ApplicationID = API_M.ApplicationID
	    AND
		    API_M.PG_TYPE = 'M'
	    LEFT JOIN
		    ADMISSION_PG_INFO API_G
	    ON
		    AOI.ApplicationID = API_G.ApplicationID
	    AND
		    API_G.PG_TYPE = 'G'
        WHERE
		    AOI.ApplyYear = '{$selectSchoolYearID}'
		    {$status_cond} 
	    ";
		$applicationRecordArr = $this->returnArray($sql);
		
		#### Get re-order applicant START ####
		usort($applicationRecordArr, array($this, 'reorderApplicantForArrangement'));
		#### Get re-order applicant END ####
		######## Get applicants END ########
		
		
		######## Add applicant to interview session START ########
		$applicationRecordIndex = 0;
		foreach($interviewGroupArr as $interviewGroup){
		    for($i=0; $i<$interviewGroup['Quota']; $i++){
    			$sql ="UPDATE 
    			    ADMISSION_OTHERS_INFO 
			    SET 
			        InterviewSettingID{$roundSQL} = '{$interviewGroup['RecordID']}' 
    			WHERE
    			    ApplicationID = '{$applicationRecordArr[$applicationRecordIndex++]['ApplicationID']}'
    			";
    			$result[] = $this->db_db_query($sql);
		    }
		}
		######## Add applicant to interview session END ########
		

		######## Handling twins swraping START ########
		$sql='Select o.ApplicationID, s.BirthCertNo, s.TwinsApplicationID, o.InterviewSettingID'.$roundSQL.' as InterviewSettingID From ADMISSION_OTHERS_INFO as o
				LEFT JOIN ADMISSION_STU_INFO as s ON o.ApplicationID = s.ApplicationID
				LEFT JOIN YEAR as y ON y.YearID = o.ApplyLevel where o.ApplyYear = "'.$selectSchoolYearID.'" AND s.IsTwinsApplied = "1" order by y.YearName desc';
		
		$twinsResult = $this->returnArray($sql);
		
		$twinsArray = array();
		$assignedTwins = array();
		
		for($i=0; $i<sizeof($twinsResult); $i++){
		    for($j=0; $j<sizeof($twinsResult); $j++){
		        if(
		            $twinsResult[$i]['TwinsApplicationID'] == $twinsResult[$j]['BirthCertNo'] && 
		            $twinsResult[$i]['InterviewSettingID'] != $twinsResult[$j]['InterviewSettingID'] && 
		            !in_array($twinsResult[$j]['ApplicationID'],$assignedTwins)
	            ){
		            $sql = 'Select 
		                RecordID
	                From 
		                ADMISSION_INTERVIEW_SETTING 
	                where 
		                RecordID = "'.$twinsResult[$i]['InterviewSettingID'].'" 
                    ';
		            $newSession = current($this->returnArray($sql));
		            
		            if($newSession){
    	                $sql = 'Select 
    	                    o.ApplicationID 
                        From 
    	                    ADMISSION_OTHERS_INFO as o 
                        LEFT JOIN 
    	                    ADMISSION_STU_INFO as s 
                        ON 
    	                    o.ApplicationID = s.ApplicationID 
                        where 
    	                    o.InterviewSettingID'.$roundSQL.' = "'.$newSession['RecordID'].'" 
                        AND 
                            s.IsTwinsApplied <> "1" 
                        ';
		                $swapApplicant = current($this->returnArray($sql));
		                
		                if($swapApplicant){
		                    $sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.$roundSQL.' = "'.$originalSession['RecordID'].'" where ApplicationID = "'.$swapApplicant['ApplicationID'].'" ';
		                    $updateResult = $this->db_db_query($sql);
		                    $sql = 'Update ADMISSION_OTHERS_INFO Set InterviewSettingID'.$roundSQL.' = "'.$newSession['RecordID'].'" where ApplicationID = "'.$twinsResult[$j]['ApplicationID'].'" ';
		                    $updateResult = $this->db_db_query($sql);
		                    $assignedTwins[] = $twinsResult[$j]['ApplicationID'];
		                }
		            }
		        }
		    }
		}
		######## Handling twins swraping END ########
		
		return !in_array(false,$result);
	}
	
	function autoApplyBriefing($ApplicationID){
	    global $PATH_WRT_ROOT, $setting_path_ip_rel;
	    
	    ######## Get briefing exists START ########
		include_once ("{$PATH_WRT_ROOT}includes/admission/libadmission_briefing_base.php");
		include_once ("{$PATH_WRT_ROOT}includes/admission/{$setting_path_ip_rel}/admission_briefing.php");
		$objBreafing = new admission_briefing();
		
		if(!$objBreafing->getBriefingApplicantByApplicantID($ApplicationID)){
		    
		    #### Get applicant info START ####
		    $StudentInfo = $this->getApplicationStudentInfo($this->schoolYearID,'',$ApplicationID);
		    $StudentInfo = $StudentInfo[0];
		    
		    $ParentInfo = $this->getApplicationParentInfo($this->schoolYearID,'',$ApplicationID);
		    $ParentInfo = BuildMultiKeyAssoc($ParentInfo, array('type'));
		    #### Get applicant info END ####
		    
		    #### Get briefing info START ####
		    $CustInfo = $this->getAllApplicationCustInfo($ApplicationID);
		    $BreafingReserveSeats = $CustInfo['BreafingReserveSeats'][0]['Value'];
		    #### Get briefing info END ####
		    
		    if($BreafingReserveSeats == 0){
		        return true;
		    }
		    
		    #### Get all briefing session START ####
		    $usedQuotas = $objBreafing->getAllBriefingSessionUsedQuota($this->schoolYearID);
		    $rs = $objBreafing->getActiveBriefingSession($this->schoolYearID);
		    $breafingTotalQuota = array();
		    foreach($rs as $r){
		        $breafingTotalQuota[$r['BriefingID']] = $r['TotalQuota'];
		    }
		    #### Get all briefing session END ####
		    
		    #### Find avaliable briefing session START ####
		    $BriefingID = 0;
	        foreach ((array)$breafingTotalQuota as $_BriefingID => $TotalQuota){
	            $remainsQuota = $TotalQuota - $usedQuotas[$_BriefingID];
	            if($remainsQuota >= $BreafingReserveSeats){
	                $BriefingID = $_BriefingID;
	                break;
	            }
	        }
		    #### Find avaliable briefing session END ####
		    
		    if($BriefingID == 0){
		        return false;
		    }
		    
		    $data = array(
		        'BriefingID' => $BriefingID,
		        'Email' => $StudentInfo['Email'],
		        'ApplicationID' => $ApplicationID,
		        'BirthCertNo' => $StudentInfo['birthcertno'],
		        'ParentName' => trim("{$ParentInfo['F']['ChineseName']}, {$ParentInfo['M']['ChineseName']}", ', '),
		        'StudentName' => $StudentInfo['student_name_b5'],
		        'PhoneNo' => $StudentInfo['homephoneno'],
		        'SeatRequest' => $BreafingReserveSeats,
		    );
		    $objBreafing->insertBriefingApplicant($this->schoolYearID, $data);
		    

		    $briefingSessionInfo = $objBreafing->getBriefingSession($BriefingID);
		    $briefingdate = $briefingSessionInfo['BriefingStartDate'];
		    $briefinginformation = $briefingSessionInfo['Title'];
		    $sql = "UPDATE
				ADMISSION_APPLICATION_STATUS
			SET
				BriefingDate = '{$briefingdate}',
				BriefingInfo = '{$briefinginformation}'
     		WHERE
				ApplicationID = '{$ApplicationID}'
			";
		    $result = $this->db_db_query($sql);
		}
	    ######## Get briefing exists END ########
	    
	    return true;
	}
	
	
	function postReplaceEmailVariables($text, $chineseName, $englishName, $applicationNo, $applicationStatus, $interviewDateTime, $interviewLocation, $interviewDateTime1='', $interviewDateTime2='', $interviewDateTime3='',$briefingdate='',$briefinginfo='', $langSpokenAtHome='', $printEmailLink=''){
	    global $PATH_WRT_ROOT, $setting_path_ip_rel;
	    
	    $protocol = (checkHttpsWebProtocol())? 'https':'http';
	    $replaced_text = $text;

		$replaced_text = str_replace("[=QrCode=]", '<img src="'.$protocol.'://'.$_SERVER['HTTP_HOST'].'/kis/admission_form/qrcode.php?encode='.$applicationNo.'" style="width: 30mm;" />', $replaced_text);
	    $replaced_text = str_replace("[=PrintInterviewSlip=]", $protocol.'://'.$_SERVER['HTTP_HOST'].$this->getPrintLink2("", $applicationNo , 'interview_form'), $replaced_text);
	    
	    return $replaced_text;
	}
	
	function getApplicationByInterviewSetting($interviewAry){
	    $sql = "SELECT ApplicationID FROM ADMISSION_OTHERS_INFO o INNER JOIN ADMISSION_INTERVIEW_SETTING i ON (i.RecordID = o.InterviewSettingID)  WHERE o.InterviewSettingID IN ('" . implode("','", $interviewAry) . "') ORDER BY i.Date, i.StartTime, i.EndTime, i.GroupName, i.RecordID, o.RecordID ";
	    $result = $this->returnVector($sql);
	    
	    return $result;
	}
	
	function getYearName(){
	    $yearStart = date('Y',getStartOfAcademicYear('',$this->schoolYearID));
	    $yearEnd = date('Y',getEndOfAcademicYear('',$this->schoolYearID));
	    
	    return array($yearStart, $yearEnd);
	}
	
	function getInterviewListAryCust($recordID='',  $date='', $startTime='', $endTime='', $keyword='', $order='', $sortby='', $round=1){
	    global $admission_cfg;
	    
	    //$schoolYearID = $schoolYearID?$schoolYearID:$this->schoolYearID;
	    
	    if($recordID != ''){
	        $cond = " AND i.RecordID IN ('".implode("','",(array)($recordID))."') ";
	    }
	    if($date != ''){
	        $cond .= ' AND i.Date >= \''.$date.'\' ';
	    }
	    if($startTime != ''){
	        $cond .= ' AND i.StartTime >= \''.$startTime.'\' ';
	    }
	    if($endTime != ''){
	        $cond .= ' AND i.EndTime <= \''.$endTime.'\' ';
	    }
	    if($keyword != ''){
	        $search_cond = ' AND (i.Date LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.StartTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\'
							OR i.EndTime LIKE \'%'.$this->Get_Safe_Sql_Like_Query($keyword).'%\')';
	    }
	    $sort = " i.Date, i.StartTime, record_id, other_record_id,
                                CASE GroupName 
                                WHEN 'Red 紅' THEN 1
                                WHEN 'Yellow 黃' THEN 2 
                                WHEN 'Green 綠' THEN 3
                                WHEN 'Blue 藍' THEN 4 
                                WHEN 'Orange 橙' THEN 5
                                ELSE 6
                                END, i.Round";
	    
	    //		if(!empty($keyword)){
	    //			$cond .= "
	    //				AND (
	    //					stu.EnglishName LIKE '%".$keyword."%'
	    //					OR stu.ChineseName LIKE '%".$keyword."%'
	    //					OR stu.ApplicationID LIKE '%".$keyword."%'
	    //					OR pg.EnglishName LIKE '%".$keyword."%'
	    //					OR pg.ChineseName LIKE '%".$keyword."%'
	    //				)
	    //			";
	    //		}
	    
	    $from_table = "
			FROM
				ADMISSION_INTERVIEW_SETTING AS i
			LEFT JOIN
				ADMISSION_OTHERS_INFO o ON i.RecordID = o.InterviewSettingID".($round>1?$round:'')."
			INNER JOIN
				ADMISSION_PG_INFO pg ON o.ApplicationID = pg.ApplicationID
			INNER JOIN
				ADMISSION_STU_INFO stu ON stu.ApplicationID = o.ApplicationID
			INNER JOIN
				ADMISSION_APPLICATION_STATUS s ON stu.ApplicationID = s.ApplicationID
			WHERE
				1
			".$cond."
			order by ".$sort."
    	";
	    $sql = "SELECT i.ClassLevelID as ClassLevelID, i.Date as Date, i.StartTime as StartTime, i.EndTime as EndTime, i.GroupName as GroupName, i.Quota as Quota,
				i.RecordID AS record_id, o.RecordID AS other_record_id,
     			stu.ApplicationID AS application_id,
     			stu.ChineseName AS ChineseName,
                stu.EnglishName AS EnglishName,
     			".getNameFieldByLang2("pg.")." AS parent_name,
                stu.DOB as dob, stu.Gender as gender, s.BriefingDate as briefing,
     			IF(pg.Mobile!='',pg.Mobile,pg.OfficeTelNo) AS parent_phone,
				CASE
     	";
	    FOREACH($admission_cfg['Status'] as $_key => $_status){//e.g. $admission_cfg['Status']['pending'] = 1, $_key = pending, $_status = 1
	        $sql .= " WHEN s.Status = '".$_status."' THEN '".$_key."' ";
	    }
	    $sql .= " ELSE s.Status END application_status	".$from_table." ";

	    //debug_r($sql);
	    $applicationAry = $this->returnArray($sql);
	    return $applicationAry;
	}
	
	function getClassLevelLetter($classLevel){
	    if(strpos($classLevel, "Pre-nursery") !== false){
	        $form = "P";
	    }elseif (strpos($classLevel, "K1") !== false){
	        $form = "N";
	    }elseif (strpos($classLevel, "K2") !== false){
	        $form = "L";
	    }elseif (strpos($classLevel, "K3") !== false){
	        $form = "U";
	    }else{
	        $form = "Z";
	    }
	    
	    return $form;
	}
	
	function getInterviewGroupSetting($interviewAry){
	    $sql = "SELECT RecordID, GroupName, ClassLevelID, Date, StartTime FROM ADMISSION_INTERVIEW_SETTING WHERE RecordID IN ('".implode("','", $interviewAry)."')";
	    $result = $this->returnArray($sql);
	    
	    $interviewGroupArr = array();
	    for($i = 0; $i < sizeof($result); $i++){
	        $recordID = $result[$i]["RecordID"];
	        $groupName = $result[$i]["GroupName"];
	        $classLevelID = $result[$i]["ClassLevelID"];
	        $date = $result[$i]["Date"];
	        $time = $result[$i]["StartTime"];
	        
	        if(!empty($interviewGroupArr)){
	            $count = 1;
	            for($k = 0; $k < sizeof($interviewGroupArr); $k++){
	                $interviewGroupArr[$i] = $result[$i];
	                if(in_array($groupName, $interviewGroupArr[$k]) && in_array($classLevelID, $interviewGroupArr[$k])
	                    && in_array($date, $interviewGroupArr[$k]) && in_array($time, $interviewGroupArr[$k])){
	                    $interviewGroupArr[$k]["num"] = $count++;
	                }
	            }
	        }else{
	            $interviewGroupArr[$i] = $result[$i];
	            $interviewGroupArr[$i]["num"] = 1;
	        }
	        $interviewGroup[$recordID]['RecordID'] = $recordID;
	        $interviewGroup[$recordID]['GroupName'] = $groupName;
	        $interviewGroup[$recordID]['ClassLevelID'] = $classLevelID;
	        $interviewGroup[$recordID]['num'] = $interviewGroupArr[$i]["num"];
	    }

	    return $interviewGroup;
	}
	
	function getInterviewGroupID($applicationId, $interviewGroupArr, $round='1'){
	    if($round == 1)
	        $settingID = "InterviewSettingID";
	    elseif($round == 2)
	        $settingID = "InterviewSettingID2";
	    elseif($round == 3)
	        $settingID = "InterviewSettingID3";
	    
	    $sql = "Select RecordID, ".$settingID." FROM ADMISSION_OTHERS_INFO WHERE ApplicationID = '".$applicationId."'";
	    $result = $this->returnArray($sql, 2);
	    
	    $groupPosition = "";
	    $color = "";
	    
	    for($i = 1; $i < sizeof($result[0]); $i++){
	        $recordID = $interviewGroupArr[$result[0][$i]]['RecordID'];
	        $groupName = $interviewGroupArr[$result[0][$i]]["GroupName"];
	        $classLevelID = $interviewGroupArr[$result[0][$i]]["ClassLevelID"];
	        $position = $interviewGroupArr[$result[0][$i]]["num"];
	        
	        if($result[0][$i] == $recordID){
	            $sql = "SELECT info.RecordID FROM ADMISSION_OTHERS_INFO as info
                        INNER JOIN ADMISSION_INTERVIEW_SETTING as setting ON (setting.RecordID = info.".$settingID.")
                        WHERE setting.RecordID = '".$recordID ."' AND setting.GroupName = '".$groupName."' ORDER BY RecordID";
	            $result2 = $this->returnVector($sql);
	            
	            $studentGroup = array_chunk($result2, 3);
	            if(sizeof($studentGroup) > 0){
// 	                $groupNum = (array_search($result[0][0], $result2)+1);
	                foreach($studentGroup as $num => $_id){
	                   if(FALSE !== array_search($result[0][0], $studentGroup[$num])){
	                       $groupNum = (array_search($result[0][0], $studentGroup[$num])+1);
	                       $groupPosition = $num+1;
	                   }
	                }
	                $color = substr($groupName, 0, 1);
	                
	                $interviewGroupID = $groupPosition.$color.$groupNum;
	            }
	        }
	    }
	    
	    return $interviewGroupID;
	}
	
	function getStudentAddress($applicationAry){
	    $sql = "SELECT s.RecordID, s.ApplicationID as application_id, s.ChineseName, s.EnglishName, s.AddressRoom, s.AddressFloor, s.AddressBlock, s.AddressBldg, s.AddressStreet, 
                s.AddressDistrict, s.AddressEstate FROM ADMISSION_STU_INFO s
                INNER JOIN ADMISSION_OTHERS_INFO o ON (s.ApplicationID = o.ApplicationID) WHERE o.RecordID IN ('".implode("','", $applicationAry)."')";
	    $result = $this->returnArray($sql);
	    
	    return $result;
	}
	
	
	
} // End Class
